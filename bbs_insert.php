<?
require_once("about_comedix.php");
require("webmail/config/config.php");
require_once("Cmx.php");
require_once("aclg_set.php");

if ($post_id == "") {
	$post_id = "0";
	$ret_url = "bbs_post_register.php";
} else {
	$ret_url = "bbs_register.php";
}
?>
<body>
<form name="items" action="<? echo($ret_url); ?>" method="post">
<input type="hidden" name="bbs_title" value="<? echo($bbs_title) ?>">
<input type="hidden" name="bbs" value="<? echo(h($bbs)) ?>">
<?
foreach ($filename as $tmp_filename) {
	echo("<input type=\"hidden\" name=\"filename[]\" value=\"$tmp_filename\">\n");
}
foreach ($file_id as $tmp_file_id) {
	echo("<input type=\"hidden\" name=\"file_id[]\" value=\"$tmp_file_id\">\n");
}
?>
<input type="hidden" name="session" value="<? echo($session) ?>">
<input type="hidden" name="theme_id" value="<? echo($theme_id) ?>">
<input type="hidden" name="post_id" value="<? echo($post_id); ?>">
<input type="hidden" name="sort" value="<? echo($sort) ?>">
<input type="hidden" name="show_content" value="<? echo($show_content) ?>">
<input type="hidden" name="back" value="t">
<input type="hidden" name="path" value="<? echo($path); ?>">
</form>
<?
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$auth_id = ($path == "2") ? 62 : 1;
$checkauth = check_authority($session, $auth_id, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// 入力チェック
if ($bbs_title == "") {
	echo("<script type=\"text/javascript\">alert('タイトルを入力してください。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if (strlen($bbs_title) > 100) {
	echo("<script type=\"text/javascript\">alert('タイトルが長すぎます。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if ($bbs == "") {
	echo("<script type=\"text/javascript\">alert('内容を入力してください。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}

// 添付ファイルの確認
if (!is_dir("bbs")) {
	mkdir("bbs", 0755);
}
if (!is_dir("bbs/tmp")) {
	mkdir("bbs/tmp", 0755);
}
for ($i = 0; $i < count($filename); $i++) {
	$tmp_file_id = $file_id[$i];
	$ext = strrchr($filename[$i], ".");

	$tmp_filename = "bbs/tmp/{$session}_{$tmp_file_id}{$ext}";
	if (!is_file($tmp_filename)) {
		echo("<script language=\"javascript\">alert('添付ファイルが削除された可能性があります。\\n再度添付してください。');</script>");
		echo("<script language=\"javascript\">document.items.submit();</script>");
		exit;
	}
}

// トランザクションを開始
pg_query($con, "begin");

// ログインユーザの職員IDを取得
$sql = "select emp_id, emp_lt_nm, emp_ft_nm, get_mail_login_id(emp_id) from empmst";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");
$emp_nm = pg_fetch_result($sel, 0, "emp_lt_nm") . pg_fetch_result($sel, 0, "emp_ft_nm");
$emp_login_id = pg_fetch_result($sel, 0, 3);

// 投稿IDを採番
$sql = "select max(bbs_id) from bbs where bbsthread_id = '$theme_id'";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$bbs_id = intval(pg_fetch_result($sel, 0, 0)) + 1;


$reg_date_time = date("YmdHi");
// 投稿情報を登録
$sql = "insert into bbs (emp_id, bbs_id, date, bbsthread_id, bbs_towhich_id, bbs_title, bbs) values (";
$content = array($emp_id, $bbs_id, $reg_date_time, $theme_id, $post_id, p($bbs_title), p($bbs));
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 添付ファイル情報を作成
$no = 1;
foreach ($filename as $tmp_filename) {
	$sql = "insert into bbsfile (bbsthread_id, bbs_id, bbsfile_no, bbsfile_name) values (";
	$content = array($theme_id, $bbs_id, $no, $tmp_filename);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$no++;
}

// トランザクションをコミット
pg_query($con, "commit");

// メール通知対象者取得
$mail_info = get_mail_info($con, $fname, $theme_id);

// テーマ名を取得
$sql = "select bbsthread_title from bbsthread";
$cond = "where bbsthread_id = $theme_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$bbsthread_title = pg_fetch_result($sel, 0, "bbsthread_title");

// データベース接続を閉じる
pg_close($con);

// メール通知
if (count($mail_info) > 0) {
	$mail_date_time = preg_replace("/(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})/", "$1/$2/$3 $4:$5", $reg_date_time);
	// メール内容編集
	$subject = "[CoMedix] 掲示板への投稿のお知らせ";

	$message = "下記の内容が登録されました。\n\n";
	$message .= "投稿者　：{$emp_nm}\n";
	$message .= "投稿日時：{$mail_date_time}\n";
	$message .= "テーマ　：{$bbsthread_title}\n";
	$message .= "タイトル：{$bbs_title}\n";
// 内容からタグを除く
	$detail = html_to_text_for_tinymce($bbs);
	$message .= "内容　　：\n{$detail}\n";
// 添付ファイル名
	if (count($filename) > 0) {
		$message .= "添付ファイル：あり\n";
	}
	$additional_headers = "From: ".mb_encode_mimeheader (mb_convert_encoding($emp_nm,"ISO-2022-JP","AUTO")) ." <$emp_login_id@$domain>";


	// メール送信
	foreach ($mail_info as $to_login_id => $tmp_emp_nm) {
		$to_addr = mb_encode_mimeheader (mb_convert_encoding($tmp_emp_nm,"ISO-2022-JP","AUTO")) ." <$to_login_id@$domain>";
		mb_send_mail($to_addr, $subject, $message, $additional_headers);
	}
}

// 添付ファイルの移動
for ($i = 0; $i < count($filename); $i++) {
	$tmp_file_id = $file_id[$i];
	$tmp_filename = $filename[$i];
	$tmp_fileno = $i + 1;
	$ext = strrchr($tmp_filename, ".");

	$tmp_filename = "bbs/tmp/{$session}_{$tmp_file_id}{$ext}";
	copy($tmp_filename, "bbs/{$theme_id}_{$bbs_id}_{$tmp_fileno}{$ext}");
}
foreach (glob("bbs/tmp/{$session}_*.*") as $tmpfile) {
	unlink($tmpfile);
}

// 投稿一覧画面に遷移
if ($path == "2") {
	echo("<script type=\"text/javascript\">location.href = 'bbs_admin_menu.php?session=$session&theme_id=$theme_id&sort=$sort&show_content=$show_content';</script>");
} else {
	echo("<script type=\"text/javascript\">location.href = 'bbsthread_menu.php?session=$session&theme_id=$theme_id&sort=$sort&show_content=$show_content';</script>");
}
?>
</body>
<?
// メール通知用情報取得、idをキーとして名前を設定した配列
function get_mail_info($con, $fname, $theme_id) {

	$mail_info = array();

	$sql = "select get_mail_login_id(b.emp_id) as mail_login_id, emp_lt_nm, emp_ft_nm from bbsmail b inner join empmst e on e.emp_id = b.emp_id";
	$cond = "where b.bbsthread_id = $theme_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$mail_login_id = $row["mail_login_id"];
		$mail_info["$mail_login_id"] = $row["emp_lt_nm"].$row["emp_ft_nm"];
	}
	
	return $mail_info;
}

// 以下の関数はファントルくんのhiyari_mail_input.iniからコピー。可能ならあとでまとめるべき
/**
 * HTML文字列をTEXTに変換します。(TinyMCE用)。
 * 注意：この変換は全てのHTMLを完全に変換するものではありません。
 * 
 * @param string $htmlstr ＨＴＭＬ文字列
 * @return string TEXT文字列
 */
function html_to_text_for_tinymce($htmlstr)
{
	$str = $htmlstr;
	
	//改行
	$str = str_replace("<br />","\n",$str);
	
	//改段落
	$str = str_replace("</p>","\n\n",$str);
	
	//テーブル
	$str = str_replace("</td>"," ",$str);
	$str = str_replace("</tr>","\n",$str);
	
	//列挙
	$str = str_replace("<li>"," ・",$str);
	$str = str_replace("</li>","\n",$str);
	$str = str_replace("</ul>","\n",$str);
	$str = str_replace("</ol>","\n",$str);
	
	//水平バー
	$str = str_replace("<hr />","\n----------\n",$str);
	
	//スペース
	$str = str_replace("&nbsp;"," ",$str);
	
	//特殊記号(「&」で始まり、小文字アルファベットが続き、「;」で終わる。)
	//ちなみに、「↓」等の記号文字は「&darr;」となっているため、この対象となる。
	$str = preg_replace("/&[a-z].*;/", "＊", $str);
	
	//インデントや位置指定などは無視。
	//顔画像の顔文字化もしない。
	//TinyMCEコマンド以外で入れられたタグは無視。
	
	//タグ抜き
	$str = strip_tags($str);
	
	return $str;
}
?>
