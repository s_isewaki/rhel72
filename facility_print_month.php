<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 設備予約 | 月（印刷）</title>
<?
require_once("about_comedix.php");
require("show_facility.ini");
require("facility_common.ini");
require("facility_month_common.ini");
require("holiday.php");
require("show_calendar_memo.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 11, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 表示範囲が渡されていない場合は「全て」に設定
if ($range == "") {
	$range = "all";
}

// タイムスタンプが渡されていない場合は当日日付のタイムスタンプを取得
if ($date == "") {
	$date = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
}

// データベースに接続
$con = connect2db($fname);

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// 参照／予約／管理可能なカテゴリ・設備一覧を配列で取得
$categories = get_referable_categories($con, $emp_id, $fname);

// オプション情報を取得
$default_info = get_default_info($con, $session, $fname);
$default_url = $default_info["url"];
$calendar_start = $default_info["calendar_start"];

// スタート曜日の設定
$start_wd = ($calendar_start == "1") ? 0 : 1;  // 0：日曜、1：月曜

// 前月・翌月のタイプスタンプを変数に格納
$last_month = get_last_month($date);
$next_month = get_next_month($date);
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
@media print {
	.noprint {visibility:hidden;}
}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="self.print();self.close();">
<center>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr bgcolor="#f6f9ff">
<td height="30"><span class="noprint"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示範囲&nbsp;&nbsp;<? show_range_string($con, $fname, $range); ?></font></span><img src="img/spacer.gif" width="180" height="1" alt=""><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><? show_date_string($date, $session, $range); ?></font></td>
<?
	echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">\n");
	$time_str = date("Y")."/".date("m")."/".date("d")." ".date("H").":".date("i");
	echo("作成日時：$time_str\n");
	echo("</font></td>\n");
?>
</tr>
</table>
<table width="100%" border="1" cellspacing="0" cellpadding="1">
<tr>
<?
for ($i = 0; $i < 7; $i++) {
	$tmp_wd = $start_wd + $i;
	if ($tmp_wd >= 7) {
		$tmp_wd -= 7;
	}
	switch ($tmp_wd) {
	case 0:
		$show_wd = "日";
		$bgcolor = " bgcolor=\"#fadede\"";
		break;
	case 1:
		$show_wd = "月";
		$bgcolor = "";
		break;
	case 2:
		$show_wd = "火";
		$bgcolor = "";
		break;
	case 3:
		$show_wd = "水";
		$bgcolor = "";
		break;
	case 4:
		$show_wd = "木";
		$bgcolor = "";
		break;
	case 5:
		$show_wd = "金";
		$bgcolor = "";
		break;
	case 6:
		$show_wd = "土";
		$bgcolor = " bgcolor=\"#defafa\"";
		break;
	}
	echo("<td width=\"13%\" align=\"center\"$bgcolor><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$show_wd</font></td>\n");
}
?>
</tr>
<?
$reservations = get_reservation($con, $categories, $range, $emp_id, $date, $start_wd, $session, $fname);
show_reservation($con, $categories, $range, $emp_id, $date, $start_wd, $session, $fname, $reservations);
?>
</table>
</center>
</body>
<? pg_close($con); ?>
</html>
<?
// 表示範囲を出力
function show_range_string($con, $fname, $range) {
	if ($range == "all") {
		$range_string = "全て";
	} else if (strpos($range, "-") === false) {
		$sql = "select fclcate_name from fclcate";
		$cond = "where fclcate_id = $range";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$range_string = "【" . pg_fetch_result($sel, 0, "fclcate_name") . "】";
	} else {
		list($cate_id, $facility_id) = explode("-", $range);
		$sql = "select facility_name from facility";
		$cond = "where fclcate_id = $cate_id and facility_id = $facility_id";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$range_string = "・" . pg_fetch_result($sel, 0, "facility_name");
	}

	echo($range_string);
}

// 選択日付を出力
function show_date_string($date, $session, $range) {
	$ym = date("Y/m", $date);
	echo("【{$ym}】");
}

// 予約状況を出力
function show_reservation($con, $categories, $range, $emp_id, $date, $start_wd, $session, $fname, $reservations) {
	list($selected_category_id, $selected_facility_id) = split("-", $range);

	// 不要なカテゴリー情報を削除
	if ($selected_category_id != "all") {
		$categories = array($selected_category_id => $categories[$selected_category_id]);
	}

	// 不要な設備情報を削除
	if ($selected_facility_id != "") {
		$categories[$selected_category_id]["facilities"] = array(
			$selected_facility_id => $categories[$selected_category_id]["facilities"][$selected_facility_id]
		);
	}

	// 当月1日〜末日の配列を作成
	$year = date("Y", $date);
	$month = date("m", $date);
	$arr_date = array();
	for ($i = 1; $i <= 31; $i++) {
		$day = substr("0" . $i, -2);
		if (checkdate($month, $day, $year)) {
			array_push($arr_date, "$year$month$day");
		} else {
			break;
		}
	}

	// 空白セル分の日付を配列の先頭に追加
	$first_day = mktime(0, 0, 0, $month, "01", $year);
	$empty = date("w", $first_day) - $start_wd;
	if ($empty < 0) {
		$empty += 7;
	}
	for ($i = 1; $i <= $empty; $i++) {
		array_unshift($arr_date, date("Ymd", strtotime("-$i days", $first_day)));
	}

	// 空白セル分の日付を配列の末尾に追加
	$day = substr($arr_date[count($arr_date) - 1], -2);
	$end_day = mktime(0, 0, 0, $month, $day, $year);
	$empty = 7 - (count($arr_date) % 7);
	if ($empty == 7) {
		$empty = 0;
	}
	for ($i = 1; $i <= $empty; $i++) {
		array_push($arr_date, date("Ymd", strtotime("$i days", $end_day)));
	}

	// 配列を週単位に分割
	$arr_week = array_chunk($arr_date, 7);

	// 「週表示」リンク出力用インデックスを求める
	$index = date("w", $date) - $start_wd;
	if ($index < 0) {
		$index += 7;
	}

	// 表示対象の設備IDを配列に格納
	$facility_ids = array();
	if ($selected_category_id == "all") {
		foreach ($categories as $tmp_category) {
			$facility_ids = array_merge(
				$facility_ids, array_keys($tmp_category["facilities"])
			);
		}
	} else if ($selected_facility_id == "") {
		$facility_ids = array_keys($categories["$selected_category_id"]["facilities"]);
	} else {
		$facility_ids[] = $selected_facility_id;
	}

	$start_date = "$year{$month}01";
	$end_date = "$year{$month}31";
	// カレンダーのメモを取得
	$arr_calendar_memo = get_calendar_memo($con, $fname, $start_date, $end_date);

	// 予定行を表示
	foreach ($arr_week as $arr_date) {
		echo("<tr height=\"80\">\n");

		// 「週表示」セルを出力（リンク先は処理日と同じ曜日）
		$tmp_year = substr($arr_date[$index], 0, 4);
		$tmp_month = substr($arr_date[$index], 4, 2);
		$tmp_day = substr($arr_date[$index], 6, 2);
		$tmp_date = mktime(0, 0, 0, $tmp_month, $tmp_day, $tmp_year);

		// 日付セルを出力
		foreach ($arr_date as $tmp_ymd) {
			$tmp_year = substr($tmp_ymd, 0, 4);
			$tmp_month = substr($tmp_ymd, 4, 2);
			$tmp_day = substr($tmp_ymd, 6, 2);
			$tmp_date = mktime(0, 0, 0, $tmp_month, $tmp_day, $tmp_year);
			$wd = date("w", $tmp_date);

			if ($tmp_ymd == date("Ymd")) {
				$bgcolor = " bgcolor=\"#ccffcc\"";
			} else {
				if ($wd == 6) {
					$bgcolor = " bgcolor=\"#defafa\"";
				} else if ($wd == 0) {
					$bgcolor = " bgcolor=\"#fadede\"";
				} else {
					$bgcolor = "";
				}
			}

			// 他の月の場合は空表示
			if ($tmp_month != $month) {
				echo("<td$bgcolor>&nbsp;</td>\n");

			// 当月の場合は詳細表示
			} else {
				$holiday_name = ktHolidayName($tmp_date);
				if ($tmp_ymd == date("Ymd")) {
					$bgcolor = " bgcolor=\"#ccffcc\"";
				} else if ($holiday_name != "") {
					$bgcolor = " bgcolor=\"#fadede\"";
				} else if ($arr_calendar_memo["{$tmp_ymd}_type"] == "5") {
					$bgcolor = " bgcolor=\"#defafa\"";
				}

				echo("<td$bgcolor valign=\"top\">\n");
				echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");

				$tmp_ymd = "$tmp_year$tmp_month$tmp_day";
				$tmp_day = intval($tmp_day);
				echo("<tr>\n");
				echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\"><b>$tmp_day</b></font></td>\n");
				echo("</tr>\n");

				// カレンダーのメモがある場合は設定する
				if ($arr_calendar_memo["$tmp_ymd"] != "") {
					if ($holiday_name == "") {
						$holiday_name = $arr_calendar_memo["$tmp_ymd"];
					} else {
						$holiday_name .= "<br>".$arr_calendar_memo["$tmp_ymd"];
					}
				}

				if ($holiday_name != "") {
					echo("<tr>\n");
					echo("<td colspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"red\">$holiday_name</font></td>\n");
					echo("</tr>\n");
				}

				// 予約情報配列を参照
				$tmp_reservation = $reservations["{$tmp_ymd}"];
				$rsv_count = count($tmp_reservation);

				for ($i = 0; $i < $rsv_count; $i++) {
					echo("<tr>\n");

					// ボーダーの設定
					if ($i == 0) {
						if ($rsv_count == 1) {
							$border = "";
						} else {
							$border = "border-bottom-style:none;";
						}
					} else if ($i < $rsv_count - 1) {
						$border = "border-top:silver dotted 1px;border-bottom-style:none;";
					} else {
						$border = "border-top:silver dotted 1px;";
					}

					$tmp_rsv_id = $tmp_reservation[$i]["reservation_id"];
					$tmp_rsv_emp_id = $tmp_reservation[$i]["emp_id"];
					if ($selected_facility_id != "") {
						$tmp_fcl_name = "";
					} else {
						$tmp_fcl_name = $tmp_reservation[$i]["facility_name"] . "<br>";
					}
					$tmp_s_time = $tmp_reservation[$i]["start_time"];
					$tmp_e_time = $tmp_reservation[$i]["end_time"];
					$tmp_title = $tmp_reservation[$i]["title"];
					$tmp_category_id = $tmp_reservation[$i]["fclcate_id"];
					$tmp_faclity_id = $tmp_reservation[$i]["facility_id"];
					$tmp_marker = $tmp_reservation[$i]["marker"];
					$tmp_fclhist_content_type = $tmp_reservation[$i]["fclhist_content_type"];
					$tmp_content = $tmp_reservation[$i]["content"];

					// テンプレートの場合はXML形式データから内容編集
					if ($tmp_fclhist_content_type == "2") {
						$tmp_content = get_text_from_xml($tmp_content);
					}

					if ($tmp_s_time != "") {
						$tmp_s_time = substr($tmp_s_time, 0, 2) . ":" . substr($tmp_s_time, 2, 2);
						$tmp_e_time = substr($tmp_e_time, 0, 2) . ":" . substr($tmp_e_time, 2, 2);
						$time_str = "$tmp_s_time-$tmp_e_time<br>";
					} else {
						$time_str = "<img src=\"img/icon/timeless.gif\" alt=\"時刻指定なし\" width=\"8\" height=\"8\">&nbsp;";
					}

					$style = get_style_by_marker($tmp_marker);

					$tmp_admin_flg = $categories["$tmp_category_id"]["facilities"]["$tmp_faclity_id"]["admin_flg"];
					if ($tmp_rsv_emp_id == $emp_id || $tmp_admin_flg) {
						$child = "facility_reservation_update.php?session=$session&rsv_id=$tmp_rsv_id&path=3&range=$range";
					} else {
						$child = "facility_reservation_detail.php?session=$session&rsv_id=$tmp_rsv_id&path=3&range=$range";
					}
					echo("<td colspan=\"2\" height=\"22\" valign=\"top\"$bgcolor style=\"$border padding:4px 0;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_fcl_name{$time_str}<span$style>" . h($tmp_title) . "</span>\n");

					// 予定内容を表示の場合
					$show_content = $categories["$tmp_category_id"]["show_content"];
					if ($show_content == "t") {
						echo("<br>" . preg_replace("/\r?\n/", "<br>", h($tmp_content)));
					}
					echo("</font></td>\n");
					echo("</tr>\n");
				}

				echo("</table>\n");
				echo("</td>\n");
			}
		}

		echo("</tr>\n");
	}
}
?>
