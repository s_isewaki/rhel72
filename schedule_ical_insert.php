<?php
$fname = $_SERVER["PHP_SELF"];

require_once("about_comedix.php");

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    js_login_exit();
}

// 権限のチェック
$checkauth = check_authority($session, 2, $fname);
if ($checkauth == "0") {
    js_login_exit();
}

// 入力チェック
$active         = $_REQUEST["active"]           == 1 ? "t" : "f";
$timeguide      = $_REQUEST["timeguide"]        == 1 ? "t" : "f";
$project        = $_REQUEST["project"]          == 1 ? "t" : "f";
$type_in_title  = $_REQUEST["type_in_title"]    == 1 ? "t" : "f";

if (preg_match("/\D/", $_REQUEST["before_days"])) {
    js_alert_exit('過去スケジュールの表示期間が不正です。');
    exit;
}
if (preg_match("/\D/", $_REQUEST["after_days"])) {
    js_alert_exit('過去スケジュールの表示期間が不正です。');
    exit;
}
if (empty($_REQUEST["public_str"])) {
    js_alert_exit('公開用文字列が設定されていません。');
    exit;
}
if ($_REQUEST["type"] < 1 or $_REQUEST["type"] > 3) {
    $_REQUEST["type"] = 1;
}

// DBへのコネクション作成
$con = connect2db($fname);

// SELECT
$sql = "SELECT * FROM schdical WHERE emp_id='{$_REQUEST["emp_id"]}'";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0) {
    pg_close($con);
    js_error_exit();
}

// トランザクションを開始
pg_query($con, "begin");

// UPDATE SQL
if (pg_num_rows($sel) > 0) {
    $set = array(
        "active"        => $active,
        "before_days"   => $_REQUEST["before_days"],
        "after_days"    => $_REQUEST["after_days"],
        "public_str"    => $_REQUEST["public_str"],
        "type"          => $_REQUEST["type"],
        "timeguide"     => $timeguide,
        "project"       => $project,
        "type_in_title" => $type_in_title,
    );
    $sql = "UPDATE schdical SET";
    $cond = "WHERE emp_id='{$_REQUEST["emp_id"]}'";
    $upd = update_set_table($con, $sql, array_keys($set), array_values($set), $cond, $fname);
    if ($upd == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        js_error_exit();
        exit;
    }
}

// INSERT SQL
else {
    $sql = "INSERT INTO schdical VALUES (";
    $content = array(
        $_REQUEST["emp_id"],
        $_REQUEST["public_str"],
        $active,
        $_REQUEST["before_days"],
        $_REQUEST["after_days"],
        $_REQUEST["type"],
        $timeguide,
        $project,
        $type_in_title,
    );
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        js_error_exit();
        exit;
    }
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// リダイレクト
header("Location: schedule_ical.php?session={$_REQUEST["session"]}&date={$_REQUEST["date"]}");
