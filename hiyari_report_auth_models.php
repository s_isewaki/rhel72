<?php
/**
 *  【ファントルくん権限用関数郡】
 * 
 * 
 *  作成者 ： システム開発部 神野 真彦
 *  作成日 ： 2011/08/10
 */

require_once("./about_postgres.php");
require_once("./hiyari_common.ini");
require_once("./get_values.php");
require_once("./hiyari_report_class.php");

/**
 *  送受信トレイで更新可能か取得するメソッド
 *  @param Object  $con             DBコネクション
 *  @param String  $session         セッションID
 *  @param String  $fname           実行ファイル名
 *  @param String  $report_id       報告書ID
 * 
 *  @return   $report_update_flg  boolean   true:更新可能、false:更新不可
 */
function getToreyUpdateAuth($con, $session, $fname, $report_id){
    
    // *** 変数宣言 ***
    $report_update_flg      = false;                                        // 更新フラグ ※念のためデフォルトはfalse
    $is_sm_emp              = is_sm_emp($session,$fname,$con);              // SM（GRM）であるかどうか確認
    $my_auth_list           = get_my_inci_auth_list($session,$fname,$con);  // 担当リスト
    
    // ** 一般ユーザの場合の処理 **
    if( empty($my_auth_list) ){
        $my_auth_list[0]['auth'] = 'GU';                                    // $my_auth_list に 一般ユーザの情報を記述
    }
    
    
    // *** 報告書情報取得 ***
    $rep_obj                = new hiyari_report_class($con, $fname);        // インスタンス生成                         
    $report_data            = $rep_obj->get_report($report_id);             // 報告書情報取得
    
    // *** 職員情報取得 ***
    $emp_id                 = get_emp_id($con,$session,$fname);             // 職員情報取得


    // *** 以下判定メインロジック ***
    
    // 担当がGRM（SM）である。または、報告書の登録者である。
    if ( $is_sm_emp || $emp_id === $report_data['registrant_id'] ) {
        
        // 更新可能
        $report_update_flg = true;
    }
    
    // 担当がGRM（SM）ではなく、登録者でもない。
    else {
        
        // 担当リストの数分ループ
        foreach ( $my_auth_list as $auth){
            
            $torey_update_flg = getToreyUpdateFlg($con);
            
            // 報告ファイルの権限に準じない場合、更新フラグをtrueにする。
            if ( $torey_update_flg === 'f' ) {
                
                // 更新アイコンを表示。担当範囲は特に見ない。（2011/08/11 現在の仕様）
                $report_update_flg = true;
            }
            
            // 送受信トレイ更新権限がfalseである（厳密に更新権限をチェックする）
            else {
                
                // 報告書の更新権限を確認
                $report_db_update_flg = is_report_db_update_able_report_post(
                            $session, 
                            $fname,
                            $con,
                            $report_data['registrant_class'],
                            $report_data['registrant_attribute'],
                            $report_data['registrant_dept'],
                            $report_data['registrant_room']
                        );
                
                // 報告書が更新可能な場合、trueを代入
                if ( $report_db_update_flg ) {
                    $report_update_flg = true;
                }
            }
        }
    }

    return $report_update_flg;
}

/**
 *  報告ファイルで更新可能か取得するメソッド
 *  @param Object  $con             DBコネクション
 *  @param String  $session         セッションID
 *  @param String  $fname           実行ファイル名
 *  @param String  $report_id       報告書ID
 * 
 *  @return   $report_update_flg  boolean   true:更新可能、false:更新不可
 */
function getReportFolderUpdateAuth($con, $session, $fname, $report_id){
    
    // *** 変数宣言 ***
    $report_update_flg      = false;                                        // 更新フラグ ※念のためデフォルトはfalse
    $is_sm_emp              = is_sm_emp($session,$fname,$con);              // SM（GRM）であるかどうか確認
    $my_auth_list           = get_my_inci_auth_list($session,$fname,$con);  // 担当リスト
    
    
    // *** 報告書情報取得 ***
    $rep_obj                = new hiyari_report_class($con, $fname);        // インスタンス生成                         
    $report_data            = $rep_obj->get_report($report_id);             // 報告書情報取得
    
    // *** 職員情報取得 ***
    $emp_id                 = get_emp_id($con,$session,$fname);             // 職員情報取得
    
    
    // *** 以下判定メインロジック ***
    
    // 担当がGRM（SM）である。または、報告書の登録者である。
    if ( $is_sm_emp || $emp_id === $report_data['registrant_id'] ) {
        
        // 更新可能、trueを代入。
        $report_update_flg = true;
    }
    
    // 担当がGRM（SM）ではなく、登録者でもない。
    else {
        
        // 担当リストの数分ループ
        foreach ( $my_auth_list as $auth){
            
            // 報告書の更新権限を確認
            $report_db_update_flg = is_report_db_update_able_report_post(
                        $session, 
                        $fname,
                        $con,
                        $report_data['registrant_class'],
                        $report_data['registrant_attribute'],
                        $report_data['registrant_dept'],
                        $report_data['registrant_room']
                    );
            
            // 報告書が更新可能な場合、trueを代入
            if ( $report_db_update_flg ) {
                $report_update_flg = true;
            }
        }
    }
    
    return $report_update_flg;
}

/**
 *  送受信トレイから編集可能か取得するメソッド
 *  @param Object  $con             DBコネクション
 *  @param String  $fname           実行ファイル名
 *  @param String  $auth            担当｛SM、RM、RA、SD、MD、HD、GU｝
 * 
 *  @return   $report_update_flg  boolean   true:更新可能、false:更新不可
 */
function getReportToreyUpdateFlg($con, $fname, $auth) {
    
    // SQL作成
    $sql  = "select auth, report_torey_update_flg from inci_auth_case";
    $cond = "where auth='{$auth}' ";
    
    // select文実行
    $sel = select_from_table($con, $sql, $cond, $fname);
    
    // エラーが無いか確認
    if ($sel == 0)
    {
        pg_close($con);
        echo("<script type='text/javascript' src='js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }
    
    // データベースの値を取得
    $result = pg_fetch_assoc($sel); 
    
    // 戻り値を設定
    return $result['report_torey_update_flg'];
}

/**
 *  報告ファイルの権限をDBから取得する
 *  @param  Object    $con             DBコネクション
 *  @param  String    $fname           実行ファイル名
 *  @param  String    $my_auth         担当｛SM、RM、RA、SD、MD、HD、GU｝
 * 
 *  @return Array     $array           DBから取得した値
 */
function get_inci_auth_case($con, $fname)
{
    
    $sql  = "select a.*, b.auth_rank, case when a.auth = 'GU' then '一般ユーザー' else b.auth_name end ";
    $sql .= "from inci_auth_case a left join inci_auth_mst b on a.auth = b.auth ";
    $cond = "order by auth_rank is not null, auth_rank asc";
    $sel = select_from_table($con, $sql, $cond, $fname);
    
    if ($sel == 0)
    {
        pg_close($con);
        echo("<script type='text/javascript' src='js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }
    
    $array = pg_fetch_all($sel);
    return $array;
}

/**
 *  報告ファイルの更新権限をDBに設定するメソッド
 *  @param  Object    $con                         DBコネクション                
 *  @param  String    $fname                       実行ファイル名
 *  @param  String    $auth                        担当｛SM、RM、RA、SD、MD、HD、GU｝
 *  @param  String    $report_db_use_flg           報告ファイル使用権限
 *  @param  String    $report_db_read_div          報告ファイル参照階層{0：すべて、1：第一階層、2：第二階層 ・・・}
 *  @param  String    $report_db_read_auth_flg     報告ファイル参照フラグ
 *  @param  String    $report_db_update_flg        報告ファイル更新フラグ
 *  @param  String    $report_db_update_div        報告ファイル更新階層{0：すべて、1：第一階層、2：第二階層 ・・・}
 * 
 * 
 *  @return boolean   $report_update_flg     
 *                      true:報告ファイルに順ずる、false:準じない（送受信トレイからは無条件に編集できる）
 */
function update_inci_auth_case(     $con,
                                    $fname, 
                                    $auth, 
                                    $report_db_use_flg, 
                                    $report_db_read_div, 
                                    $report_db_read_auth_flg, 
                                    $report_db_update_flg, 
                                    $report_db_update_div
)
{
    
    $sql = 'update inci_auth_case set';
    
    // 更新するカラム
    $set = array(
            'report_db_use_flg', 
            'report_db_read_div',
            'report_db_read_auth_flg',
            'report_db_update_flg',
            'report_db_update_div',
            );
    
    // カラムの値
    $setvalue = array(
            $report_db_use_flg,
            $report_db_read_div, 
            $report_db_read_auth_flg, 
            $report_db_update_flg, 
            $report_db_update_div,
            );
    // WHERE句
    $cond = "where auth = '{$auth}'";
    
    $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
    if ($upd == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}

/**
 *  送受信トレイからの報告書編集フラグを設定するメソッド
 *  @param  Object    $con                         DBコネクション                
 *  @param  String    $setting_torey_update_flg    実行ファイル名
 * 
 *  @return boolean   $report_update_flg     
 *                      true:報告ファイルに順ずる、false:準じない（送受信トレイからは無条件に編集できる）
 */
function setToreyUpdateFlg($con, $setting_torey_update_flg) {
    
    // チェックされていたら、$setting_torey_update_flgにtrueを代入
    if ( $setting_torey_update_flg == 't') {
        $report_torey_update_flg = 'true';
    }
    else {
        $report_torey_update_flg = 'false';
    }
    
    $sql = 'update inci_auth_case set';
    
    // 更新するカラム
    $set = array(
            'report_torey_update_flg',
            );
    
    // カラムの値
    $setvalue = array(
            $report_torey_update_flg,
            );
    
    // WHERE句 ※なし（全件更新）
    $cond = "";
    
    $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
    if ($upd == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}


/**
 *  DBから、送受信トレイの編集権限を取得するメソッド
 *  @param  Object    $con                         DBコネクション                
 * 
 *  @return String    $report_update_flg     
 *                      't':報告ファイルに順ずる、'f':準じない（送受信トレイからは無条件に編集できる）
 */
function getToreyUpdateFlg($con) {
    // SQL作成
    $sql  = "select report_torey_update_flg from inci_auth_case";
    $cond = "";
    
    // select文実行
    $sel = select_from_table($con, $sql, $cond, $fname);
    
    // エラーが無いか確認
    if ($sel == 0)
    {
        pg_close($con);
        echo("<script type='text/javascript' src='js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }
    
    // データベースの値を取得
    $result = pg_fetch_assoc($sel); 
    
    // 戻り値を設定
    return $result['report_torey_update_flg'];   
}

/**
 *  送信の際のあて先選択のボタンを表示するかしないか判定するメソッド
 *  @param Object  $con             DBコネクション
 *  @param String  $session         セッションID
 *  @param String  $fname           実行ファイル名
 * 
 *  @return   $report_update_flg  boolean   true:更新可能、false:更新不可
 */
function getAddressSearchBtnShowFlg($con, $session, $fname){
    
    // *** 変数宣言 ***
    $my_auth_list       = get_my_inci_auth_list($session,$fname,$con);  // 担当リスト
    $my_btn_show_flg    = getAuthBtnShowFlg($con, $my_auth_list);       // 表示するボタンのリスト
    
    
    // *** あて先選択のボタンを表示するか判定 ***
    // 「返信ボタン」および「転送ボタン」が非表示なら、あて先選択ボタンは表示しない。
    if ( $my_btn_show_flg['address_btn_show_flg'] == false) {
        $result = false ;
    }
    
    // どちらかが表示される設定なら、あて先選択ボタンを表示する。
    else{
        $result = true ;
    }
    
    return $result ;
}
function getListBtnShowFlg($con, $session, $fname){
    
    // *** 変数宣言 ***
    $my_auth_list       = get_my_inci_auth_list($session,$fname,$con);  // 担当リスト
    $my_btn_show_flg    = getAuthBtnShowFlg($con, $my_auth_list);       // 表示するボタンのリスト
    
    if ( $my_btn_show_flg['list_btn_show_flg'] == false) {
        $result = false ;
    }else{
        $result = true ;
    }
    
    return $result ;
}


/**
 *  自分が持っているボタン利用権限を取得するメソッド
 *  @param  Object    $con                         DBコネクション                
 *  @param  Array     $my_auth_list                DBコネクション                
 * 
 *  @return String    $my_btn_show_flg     
 *                      't':報告ファイルに順ずる、'f':準じない（送受信トレイからは無条件に編集できる）
 */
function getAuthBtnShowFlg($con, $my_auth_list) {
    
    // ボタン利用フラグ
    $my_btn_show_flg = array(
        'create_btn_show_flg'           => false,
        'return_btn_show_flg'           => false,
        'forward_btn_show_flg'          => false,
        'print_btn_show_flg'            => false,
        'excel_btn_show_flg'            => false,
        'collect_excel_btn_show_flg'    => false,
        'address_btn_show_flg'          => false,   
        'list_btn_show_flg'          => false,
    );
    
    $auth_str = "'GU'";
    
    foreach ( $my_auth_list as $my_auth) {
        $auth_str .= ", '{$my_auth['auth']}'";
    }  
    
    // SQL作成
    $sql  = "select * from inci_btn_show_flg ";
    $cond = "where auth IN({$auth_str})";
    
    // select文実行
    $sel = select_from_table($con, $sql, $cond, $fname);
    
    // エラーが無いか確認
    if ($sel == 0) {
        pg_close($con);
        echo("<script type='text/javascript' src='js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }
    
    // DBから取得した値をループでまわす。
    while ($row = pg_fetch_assoc($sel)) {
        
        // 't', 'f' を true, false に変換
        $row = array(
            'create_btn_show_flg'        => $row['create_btn_show_flg']         == 't' ? true : false,
            'return_btn_show_flg'        => $row['return_btn_show_flg']         == 't' ? true : false,
            'forward_btn_show_flg'       => $row['forward_btn_show_flg']        == 't' ? true : false,
            'print_btn_show_flg'         => $row['print_btn_show_flg']          == 't' ? true : false,
            'excel_btn_show_flg'         => $row['excel_btn_show_flg']          == 't' ? true : false,
            'collect_excel_btn_show_flg' => $row['collect_excel_btn_show_flg']  == 't' ? true : false,
            'address_btn_show_flg'       => $row['address_btn_show_flg']  == 't' ? true : false, 
            'list_btn_show_flg'          => $row['list_btn_show_flg']  == 't' ? true : false,
        );
        
        // ボタン権限の論理和を取得
        // 例）〜担当がRMと所属長が兼務の場合〜
        // RM転送可、部門長転送不可の場合、兼務しているユーザは転送可 （ RM転送権限：true or 所属長転送権限：false = RM・所属長兼務転送権限：true ）
        $my_btn_show_flg = array(
            'create_btn_show_flg'        =>  $my_btn_show_flg['create_btn_show_flg']        || $row['create_btn_show_flg'],
            'return_btn_show_flg'        =>  $my_btn_show_flg['return_btn_show_flg']        || $row['return_btn_show_flg'],
            'forward_btn_show_flg'       =>  $my_btn_show_flg['forward_btn_show_flg']       || $row['forward_btn_show_flg'],
            'print_btn_show_flg'         =>  $my_btn_show_flg['print_btn_show_flg']         || $row['print_btn_show_flg'],
            'excel_btn_show_flg'         =>  $my_btn_show_flg['excel_btn_show_flg']         || $row['excel_btn_show_flg'],
            'collect_excel_btn_show_flg' =>  $my_btn_show_flg['collect_excel_btn_show_flg'] || $row['collect_excel_btn_show_flg'],
            'address_btn_show_flg'       =>  $my_btn_show_flg['address_btn_show_flg']       || $row['address_btn_show_flg'],
            'list_btn_show_flg'       =>  $my_btn_show_flg['list_btn_show_flg']       || $row['list_btn_show_flg'],
        );
    }
    
    // 各ボタンの表示権限を取得
    return $my_btn_show_flg;   
}
?>