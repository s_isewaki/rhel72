<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務シフト作成 | 看護職員表</title>

<?
//ini_set("display_errors","1");
ini_set("max_execution_time", 0);
require_once("about_authority.php");
require_once("about_session.php");
require_once("about_postgres.php");

require_once("duty_shift_common_class.php");
require_once("duty_shift_report_common_class.php");
require_once("duty_shift_report1_common_class.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
//セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session,$fname);
if($session == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}
///-----------------------------------------------------------------------------
//DBコネクション
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
$obj_report = new duty_shift_report_common_class($con, $fname);
$obj_report1 = new duty_shift_report1_common_class($con, $fname);
///-----------------------------------------------------------------------------
//初期処理
///-----------------------------------------------------------------------------
    ///-----------------------------------------------------------------------------
    //初期値設定
    ///-----------------------------------------------------------------------------
    $sum_night_times = array(); //「夜勤専従者及び月１６時間以下」の合計
    $excel_flg = "";            //ＥＸＣＥＬ表示フラグ（１：ＥＸＣＥＬ表示）
    ///-----------------------------------------------------------------------------
    //現在日付の取得
    ///-----------------------------------------------------------------------------
    $date = getdate();
    $now_yyyy = $date["year"];
    $now_mm = $date["mon"];
    $now_dd = $date["mday"];
    ///-----------------------------------------------------------------------------
    //表示年／月設定
    ///-----------------------------------------------------------------------------
    if ($duty_yyyy == "") { $duty_yyyy = $now_yyyy; }
    if ($duty_mm == "") { $duty_mm = $now_mm; }
    $day_cnt = $obj->days_in_month($duty_yyyy, $duty_mm);
    ///-----------------------------------------------------------------------------
    //終了年月
    ///-----------------------------------------------------------------------------
    if ($night_start_day == "") {
        $night_start_day = 1;
    }
    $end_yyyy = (int)$duty_yyyy;
    $end_mm = (int)$duty_mm;
    $end_dd = (int)$day_cnt;
    if ($recomputation_flg == "2") {    //４週計算時
        $obj_report->setEndYmd4Week($night_start_day, $duty_yyyy, $duty_mm, $end_yyyy, $end_mm, $end_dd);
        $day_cnt = 28;
    } else {
        $night_start_day = 1;
    }
    //日またがりの時間を加算するため、１日前を設定
    if ((int)$night_start_day == 1) {
        $wk_start_yyyy = (int)$duty_yyyy;
        $wk_start_mm = (int)$duty_mm - 1;
        if ((int)$wk_start_mm < 1) {
            $wk_start_yyyy--;
            $wk_start_mm = 12;
        }
        $wk_start_dd = $obj->days_in_month($wk_start_yyyy, $wk_start_mm);
    } else {
        $wk_start_yyyy = (int)$duty_yyyy;
        $wk_start_mm = (int)$duty_mm;
        $wk_start_dd = (int)$night_start_day - 1;
    }
    ///-----------------------------------------------------------------------------
    // カレンダー(calendar)情報を取得
    ///-----------------------------------------------------------------------------
    //$start_date = sprintf("%04d%02d%02d",$duty_yyyy, $duty_mm, $night_start_day);
    $start_date = sprintf("%04d%02d%02d",$wk_start_yyyy, $wk_start_mm, $wk_start_dd);
    $end_date = sprintf("%04d%02d%02d",$end_yyyy, $end_mm, $end_dd);
    $calendar_array = $obj->get_calendar_array($start_date, $end_date);
    ///-----------------------------------------------------------------------------
    //指定月の全曜日を設定
    ///-----------------------------------------------------------------------------
    $week_array = array();
    for ($k=1; $k<=$day_cnt; $k++) {
        $p = $night_start_day + $k - 1;
        $tmp_date = mktime(0, 0, 0, $duty_mm, $p, $duty_yyyy);
        $week_array[$k]["name"] = $obj->get_weekday($tmp_date);
    }
    ///-----------------------------------------------------------------------------
    //施設基準情報（履歴）の取得
    ///-----------------------------------------------------------------------------
    $history_array = $obj->get_duty_shift_institution_history_array($standard_id, $duty_yyyy, $duty_mm);


    //$data = $obj->get_group_id_for_report($standard_id, $duty_yyyy, $duty_mm, $cond_add, $calendar_array, 1);
    //施設基準が合う病棟を全て表示する
$data = $obj->get_group_id_from_standard_id($standard_id);
?>

<!-- ************************************************************************ -->
<!-- JavaScript -->
<!-- ************************************************************************ -->
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
    ///-----------------------------------------------------------------------------
    //「ＥＸＣＥＬ作成」
    ///-----------------------------------------------------------------------------
    function makeExcel(group_id) {
        document.xlsForm1.group_id.value = group_id;
        document.xlsForm1.submit();
    }
</script>

<!-- ************************************************************************ -->
<!-- HTML -->
<!-- ************************************************************************ -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}

.non_list {border-collapse:collapse;}
.non_list td {border:#FFFFFF solid 1px;}

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
    <!-- ************************************************************************ -->
    <!-- 看護職員表 -->
    <!-- ************************************************************************ -->
    
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
        <tr height="32" bgcolor="#5279a5">
        <td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>看護職員表 病棟選択</b></font></td>
        <td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
        </tr>
    </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr height="32">
        <td>
        <input type="button" value="全病棟一括" onclick="makeExcel('0');">
        </td>
        </tr>
    </table>
<br>
    <form name="nurseForm1" method="post">
    <?
    for ($i=0; $i<count($data); $i++) {
        $group_id = $data[$i]["group_id"];
        $group_name = $data[$i]["group_name"];
        //js
        echo("<input type=\"button\" value=\"$group_name\" onclick=\"makeExcel('$group_id');\">");
//      echo("<a href=\"javascript:void(0);\" onclick=\"makeExcel('$group_id');\">");
//      echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$group_name</font></a>");
        echo("&nbsp;&nbsp;");
    }
/*
        ///-----------------------------------------------------------------------------
        // ＨＴＭＬ作成（表）
        ///-----------------------------------------------------------------------------
        echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"list\">\n");
            ///-----------------------------------------------------------------------------
            // ＨＴＭＬ作成（見出し）
            ///-----------------------------------------------------------------------------
            $btn_show_flg = "1";    //１：ボタン表示
            $wk = $obj_report1->showNurseTitle_1($btn_show_flg,
                                                $recomputation_flg,         //１ヵ月計算４週計算フラグ（１：１ヵ月計算、２：４週計算）
                                                $date_y1,                   //カレンダー用年
                                                $date_m1,                   //カレンダー用月
                                                $date_d1,                   //カレンダー用日
                                                $day_cnt);                  //日数
            echo($wk);
            ///-----------------------------------------------------------------------------
            // タイトル 
            ///-----------------------------------------------------------------------------
            $wk_array1 = $obj_report1->showNurseData_1($excel_flg, $week_array, $calendar_array, $day_cnt);
            echo($wk_array1["data"]);
            ///-----------------------------------------------------------------------------
            // データ 
            ///-----------------------------------------------------------------------------
            $wk_array2 = $obj_report1->showNurseData_2($excel_flg, $data_array, $day_cnt);
            for ($i=0; $i<count($data_array); $i++) {
                echo($wk_array2[$i]["data"]);
            }
            ///-----------------------------------------------------------------------------
            // 合計　日勤時間数
            // 合計　夜勤時間数
            ///-----------------------------------------------------------------------------
            $wk_array3 = $obj_report1->showNurseData_3($excel_flg, $data_array, $day_cnt, $wk_array2["sum_night_times"]);
            echo($wk_array3["data"]);
        echo("</table>\n");
        ///-----------------------------------------------------------------------------
        // ＨＴＭＬ作成（合計）
        ///-----------------------------------------------------------------------------
        echo("<table width=\"70%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"list\">\n");
            ///-----------------------------------------------------------------------------
            // 療養病棟入院基本料における夜勤体制の要件を満たすことの確認
            ///-----------------------------------------------------------------------------
            $wk_array4 = $obj_report1->showNurseData_4($excel_flg,
                                                    $day_cnt,
                                                    $sum_night_b,
                                                    $sum_night_c,
                                                    $sum_night_d,
                                                    $sum_night_e,
                                                    $sum_night_f,
                                                    $sum_night_g);
            echo($wk_array4["data"]);
        echo("</table>\n");
*/
    ?>
    </form>
    <!-- ************************************************************************ -->
    <!-- 他画面送信:Excel用フォーム -->
    <!-- ************************************************************************ -->
    <form name="xlsForm1" action="duty_shift_report_nurse1_excel.php" method="post">
        <?
            $obj_report->showHidden(
                                    $session,               //セッション
                                    $standard_id,           //施設基準ＩＤ
                                    $recomputation_flg,     //１ヵ月計算４週計算フラグ（１：１ヵ月計算、２：４週計算）
                                    $night_start_day,       //４週計算用（開始日）
                                    $hospital_name,         //保険医療機関名
                                    $ward_cnt,              //病棟数
                                    $sickbed_cnt,           //病床数
                                    $report_kbn,            //届出区分
                                    $report_kbn_name,       //届出区分名
                                    $report_patient_cnt,    //届出時入院患者数
                                    $hosp_patient_cnt,      //１日平均入院患者数
                                    $compute_start_yyyy,    //１日平均入院患者数 算出期間（開始年）
                                    $compute_start_mm,      //１日平均入院患者数 算出期間（開始月）
                                    $compute_end_yyyy,      //１日平均入院患者数 算出期間（終了年）
                                    $compute_end_mm,        //１日平均入院患者数 算出期間（終了月）
                                    $nurse_sumtime,             //勤務時間数（看護師）
                                    $sub_nurse_sumtime,         //勤務時間数（准看護師）
                                    $assistance_nurse_sumtime,  //勤務時間数（看護補助者）
                                    $nurse_cnt,             //看護師数
                                    $sub_nurse_cnt,         //准看護師数
                                    $assistance_nurse_cnt,  //看護補助者数
                                    $hospitalization_cnt,   //平均在院日数
                                    $ave_start_yyyy,        //平均在院日数 算出期間（開始年）
                                    $ave_start_mm,          //平均在院日数 算出期間（開始月）
                                    $ave_end_yyyy,          //平均在院日数 算出期間（終了年）
                                    $ave_end_mm,            //平均在院日数 算出期間（終了月）
                                    $prov_start_hh,         //夜勤時間帯 （開始時）
                                    $prov_start_mm,         //夜勤時間帯 （開始分）
                                    $prov_end_hh,           //夜勤時間帯 （終了時）
                                    $prov_end_mm,           //夜勤時間帯 （終了分）
                                    $duty_yyyy,             //対象年
                                    $duty_mm,               //対象月
                                    $date_y1,               //カレンダー用年
                                    $date_m1,               //カレンダー用月
                                    $date_d1,               //カレンダー用日
                                    $labor_cnt,             //常勤職員の週所定労働時間
                                    compact("create_yyyy", "create_mm", "create_dd"));  //作成年月日
        ?>
        <input type="hidden" name="sum_night_b" value="<? echo($sum_night_b); ?>">
        <input type="hidden" name="sum_night_c" value="<? echo($sum_night_c); ?>">
        <input type="hidden" name="sum_night_d" value="<? echo($sum_night_d); ?>">
        <input type="hidden" name="sum_night_e" value="<? echo($sum_night_e); ?>">
        <input type="hidden" name="sum_night_f" value="<? echo($sum_night_f); ?>">
        <input type="hidden" name="sum_night_g" value="<? echo($sum_night_g); ?>">
        <input type="hidden" name="group_id" value="">
        <input type="hidden" name="plan_result_flag" value="<? echo($plan_result_flag); ?>">
    </form>

<?
//print_r($wk_array2["sum_night_times"]);
?>

</body>
<? pg_close($con); ?>
</html>

