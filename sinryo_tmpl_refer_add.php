<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" action="sinryo_tmpl_refer.php" method="post">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="back" value="t">
</form>
<?
require("./about_session.php");
require("./about_authority.php");
require("sinryo_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"./js/showpage.js\"></script>");
	echo("<script language=\"javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 59, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"./js/showpage.js\"></script>");
	echo("<script language=\"javascript\">showLoginPage(window);</script>");
	exit;
}

// ファイルアップロードチェック
define(UPLOAD_ERR_OK, 0);
define(UPLOAD_ERR_INI_SIZE, 1);
define(UPLOAD_ERR_FORM_SIZE, 2);
define(UPLOAD_ERR_PARTIAL, 3);
define(UPLOAD_ERR_NO_FILE, 4);

$filename = $_FILES["template_file"]["name"];
if ($filename == "") {
	echo("<script language=\"javascript\">alert('ファイルを選択してください。');</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
}

if (strlen($filename) > 40) {
	echo("<script type=\"text/javascript\">alert('テンプレートファイル名が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

switch ($_FILES["template_file"]["error"]) {
case UPLOAD_ERR_INI_SIZE:
case UPLOAD_ERR_FORM_SIZE:
	echo("<script language=\"javascript\">alert('ファイルサイズが大きすぎます。');</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
case UPLOAD_ERR_PARTIAL:
case UPLOAD_ERR_NO_FILE:
	echo("<script language=\"javascript\">alert('アップロードに失敗しました。再度実行してください。');</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
}

// ファイル保存用ディレクトリがなければ作成
if (!is_dir("workflow")) mkdir("workflow", 0755);
if (!is_dir("workflow/tmp")) mkdir("workflow/tmp", 0755);

// 30分以上前に保存されたファイルを削除
foreach (glob("workflow/tmp/*.*") as $tmpfile) {
	if (time() - filemtime($tmpfile) >= 30 * 60) unlink($tmpfile);
}

// アップロードされたファイルを保存
$savefilename = "workflow/tmp/{$session}_{$filename}";
copy($_FILES["template_file"]["tmp_name"], $savefilename);


// 内容読み込み
if (!$fp = fopen($savefilename, "r")) {
	echo("<script language=\"javascript\">alert('ファイルのオープンができませんでした。');</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
}

$content = fread($fp, filesize($savefilename));
fclose($fp);

// 内容チェック
$chkarray = array("INSERT ", "INSERT	", "UPDATE ", "UPDATE	", "DELETE ", "DELETE	", "CONNECT2DB", "SELECT_FROM_ TABLE", "PG_CONNECT", "PG_EXEC", "<FORM");
$content_upper = strtoupper($content);
$sqlchkflg = true;
/*
foreach($chkarray as $chkstr){
	if(!(strpos($content_upper, $chkstr) === false)){
		$sqlchkflg = false;
		break;
	}
}

if($sqlchkflg == false){
	echo("<script language=\"javascript\">alert('指定できない文字列があります($chkstr)。');</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
}
*/
$tmpl_content = $content;

// テンプレートのバージョンの記述があれば1、なければ0
$template_version_description = (strpos($content, "COMEDIX_TEMPLATE_VERSION_DEFINITION") === false ? 0 : 1);

//==============================================================================
// 2010/01以前のバージョン
//==============================================================================
if (!$template_version_description){
	$tmpl_content = ereg_replace("<\? \/\/ XML.*\?>", "", $tmpl_content);

	$create_content = ereg_replace("^.*<\? // XML", "<? // XML", $content);
	// 文字コード変換　EUC -> UTF-8
	$create_content = mb_convert_encoding($create_content, "UTF-8", "EUC-JP");
	// XML作成用ファイル名
	$pos = strpos($tmpl_content, "xml_creator");
	//$pos = strrpos($tmpl_content, "<", $pos);
	$pos1 = strpos($tmpl_content, "create_", $pos);
	$pos2 = strpos($tmpl_content, "\"", $pos1);
	$create_filename = substr($tmpl_content, $pos1, $pos2 - $pos1);
	// XML作成用ファイル重複チェック
	//「create_*.php」をテンプレートファイルと見なす。
	$arr_filename = get_dir_file_list(".",FALSE,'^create_.*\.php$');
	for ($i=0; $i<count($arr_filename); $i++) {
		if ($create_filename == $arr_filename[$i]) {
			echo("<script type=\"text/javascript\">alert('同じ名前のXML作成用ファイルが登録されています。');</script>");
			echo("<script type=\"text/javascript\">document.items.submit();</script>");
			exit;
		}
	}
}


// 内容書込み
$fp1 = fopen($filename, "w");
if (!fwrite($fp1, $tmpl_content, 2000000)) {
	fclose($fp1);
	echo("<script language=\"javascript\">alert('ファイルの書き込みができませんでした。');</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
}
fclose($fp1);
chmod($filename, 0666);

//==============================================================================
// 2010/01以前のバージョン
//==============================================================================
if (!$template_version_description){
	// XML作成用コード書込み
	$fp2 = fopen($create_filename, "w");
	if (!fwrite($fp2, $create_content, 2000000)) {
		fclose($fp2);
		echo("<script language=\"javascript\">alert('ファイルの書き込みができませんでした。');</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}
	fclose($fp2);
	chmod($create_filename, 0666);
}

// 一覧画面に遷移
echo("<script type=\"text/javascript\">location.href='sinryo_tmpl_refer.php?session=$session';</script>");
?>
</body>
