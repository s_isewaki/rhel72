<?
function show_inpatient_list_common_javascript() {
?>
function showList() {
	var path;
	switch (document.listform.list_type.value) {
	case 'F':
		path = 'inpatient_waiting_search.php';
		break;
	case '1':
		path = 'inpatient_reserve_list.php';
		break;
	case '2':
		path = 'inpatient_list.php';
		break;
	case 'A':
		path = 'inpatient_date_list.php';
		break;
	case 'B':
		path = 'out_inpatient_date_list.php';
		break;
	case '3':
		path = 'out_inpatient_reserve_list.php';
		break;
	case '4':
		path = 'out_inpatient_3month_list.php';
		break;
	case 'C':
		path = 'inpatient_current_list.php';
		break;
	case 'D':
		path = 'inpatient_move_reserve_list.php';
		break;
	case 'E':
		path = 'inpatient_move_list.php';
		break;
	case '5':
		path = 'inpatient_cancel_list.php';
		break;
	case '6':
		path = 'inpatient_go_out_list.php';
		break;
	case '7':
		path = 'inpatient_stop_out_list.php';
		break;
	case '8':
		path = 'out_inpatient_list.php';
		break;
	}
	document.listform.action = path;
	document.listform.submit();
}

function addEmptyOption(box, value, text) {
	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	opt.selected = true;
	box.options[box.length] = opt;
	if (!box.multiple) {
		try {box.style.fontSize = 'auto';} catch (e) {}
		box.style.overflow = 'auto';
	}
}

function setListDateDisabled() {
	var list_year = document.listform.list_year;
	var list_month = document.listform.list_month;
	var list_day = document.listform.list_day;
	if (document.listform.list_type.value == '1' || document.listform.list_type.value == 'F') {
		if (list_year.options[list_year.options.length - 1].value != '0000') {
			addEmptyOption(list_year, '0000', '    ');
		}
		if (list_month.options[list_month.options.length - 1].value != '00') {
			addEmptyOption(list_month, '00', '  ');
		}
		if (list_day.options[list_day.options.length - 1].value != '00') {
			addEmptyOption(list_day, '00', '  ');
		}
	} else {
		if (list_year.options[list_year.options.length - 1].value == '0000') {
			if (list_year.value == '0000') list_year.value = '<? echo(date("Y")); ?>';
			list_year.options[list_year.options.length - 1] = null;
		}
		if (list_month.options[list_month.options.length - 1].value == '00') {
			if (list_month.value == '00') list_month.value = '<? echo(date("m")); ?>';
			list_month.options[list_month.options.length - 1] = null;
		}
		if (list_day.options[list_day.options.length - 1].value == '00') {
			if (list_day.value == '00') list_day.value = '<? echo(date("d")); ?>';
			list_day.options[list_day.options.length - 1] = null;
		}
	}

	var ymd1_filter_disabled;
	var ymd2_filter_disabled;
	switch (document.listform.list_type.value) {
	case '1':
	case 'A':
	case 'B':
	case 'E':
	case 'F':
		ymd1_filter_disabled = false;
		ymd2_filter_disabled = false;
		break;
	case 'C':
		ymd1_filter_disabled = false;
		ymd2_filter_disabled = true;
		break;
	default:
		ymd1_filter_disabled = true;
		ymd2_filter_disabled = true;
	}
	list_year.disabled = ymd1_filter_disabled;
	list_month.disabled = ymd1_filter_disabled;
	list_day.disabled = ymd1_filter_disabled;
	document.listform.list_year2.disabled = ymd2_filter_disabled;
	document.listform.list_month2.disabled = ymd2_filter_disabled;
	document.listform.list_day2.disabled = ymd2_filter_disabled;
	document.getElementById('ymd1_filter').style.display = (ymd1_filter_disabled) ? 'none' : '';
	document.getElementById('ymd1_filter_label').style.display = (ymd2_filter_disabled) ? 'none' : '';
	document.getElementById('ymd2_filter').style.display = (ymd2_filter_disabled) ? 'none' : '';

	var ward_filter_disabled = (document.listform.list_type.value == 'F');
	var sect_filter_disabled = ward_filter_disabled;
	document.listform.bldgwd.disabled = ward_filter_disabled;
	document.listform.list_sect_id.disabled = sect_filter_disabled;
	document.getElementById('ward_filter').style.display = (ward_filter_disabled) ? 'none' : '';
	document.getElementById('sect_filter').style.display = (sect_filter_disabled) ? 'none' : '';
}
<?
}

function show_inpatient_list_form($con, $sel_ward, $default_list_type, $default_bldgwd, $default_list_sect_id, $default_list_year, $default_list_month, $default_list_day, $default_list_year2, $default_list_month2, $default_list_day2, $session, $fname) {
	if ($default_list_year == "") {
		$default_list_year = date("Y");
	}
	if ($default_list_month == "") {
		$default_list_month = date("m");
	}
	if ($default_list_day == "") {
		$default_list_day = date("d");
	}
	if ($default_list_year2 == "") {
		$default_list_year2 = $default_list_year;
	}
	if ($default_list_month2 == "") {
		$default_list_month2 = $default_list_month;
	}
	if ($default_list_day2 == "") {
		$default_list_day2 = $default_list_day;
	}

	// ���Ųʰ��������
	$sql = "select sect_id, sect_nm from sectmst";
	$cond = "where sect_del_flg = 'f' order by sect_id";
	$sel_sect = select_from_table($con, $sql, $cond, $fname);
	if ($sel_sect == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

?>
<form name="listform" method="get">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
<td width="80"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">���԰���</font></td>
<td>
<select name="list_type" onchange="setListDateDisabled();">
<option value="F"<? if ($default_list_type == "F") {echo(" selected");} ?>>�Ե�����
<option value="1"<? if ($default_list_type == "1") {echo(" selected");} ?>>����ͽ�괵��
<option value="2"<? if ($default_list_type == "2") {echo(" selected");} ?>>��������
<option value="A"<? if ($default_list_type == "A") {echo(" selected");} ?>>������������
<option value="B"<? if ($default_list_type == "B") {echo(" selected");} ?>>�����ౡ����
<option value="3"<? if ($default_list_type == "3") {echo(" selected");} ?>>�ౡͽ�괵��
<option value="4"<? if ($default_list_type == "4") {echo(" selected");} ?>>3��������ౡ����
<option value="C"<? if ($default_list_type == "C") {echo(" selected");} ?>>�������ꥹ��
<option value="D"<? if ($default_list_type == "D") {echo(" selected");} ?>>ž��ͽ�괵��
<option value="E"<? if ($default_list_type == "E") {echo(" selected");} ?>>ž������
<option value="5"<? if ($default_list_type == "5") {echo(" selected");} ?>>����󥻥봵��
<option value="6"<? if ($default_list_type == "6") {echo(" selected");} ?>>���д���
<option value="7"<? if ($default_list_type == "7") {echo(" selected");} ?>>���񴵼�
<option value="8"<? if ($default_list_type == "8") {echo(" selected");} ?>>�ౡ����
</select>
</td>
</tr>
<tr id="ward_filter" style="display:none;">
<td><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">����</font></td>
<td>
<select name="bldgwd">
<option value="">���٤�
<?
while ($row = pg_fetch_array($sel_ward)) {
	$tmp_bldgwd = "{$row["bldg_cd"]}-{$row["ward_cd"]}";
	echo("<option value=\"$tmp_bldgwd\"");
	if ($tmp_bldgwd == $default_bldgwd) {
		echo(" selected");
	}
	echo(">{$row["ward_name"]}\n");
}
pg_result_seek($sel_ward, 0);
?>
</select>
</td>
</tr>
<tr id="sect_filter" style="display:none;">
<td><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">���Ų�</font></td>
<td>
<select name="list_sect_id">
<option value="">���٤�
<?
while ($row = pg_fetch_array($sel_sect)) {
	echo("<option value=\"{$row["sect_id"]}\"");
	if ($row["sect_id"] == $default_list_sect_id) {
		echo(" selected");
	}
	echo(">{$row["sect_nm"]}\n");
}
?>
</select>
</td>
</tr>
<tr id="ymd1_filter" style="display:none;">
<td><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">ǯ����<span id="ymd1_filter_label" style="display:none;">�ʼ���</span></font></td>
<td><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">
<select name="list_year"><? show_select_years_span(2004, date("Y") + 1, $default_list_year); ?></select>/<select name="list_month"><? show_select_months($default_list_month); ?></select>/<select name="list_day"><? show_select_days($default_list_day); ?></select>
</font></td>
</tr>
<tr id="ymd2_filter" style="display:none;">
<td><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">ǯ�����ʻ��</font></td>
<td><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">
<select name="list_year2"><? show_select_years_span(2004, date("Y") + 1, $default_list_year2); ?></select>/<select name="list_month2"><? show_select_months($default_list_month2); ?></select>/<select name="list_day2"><? show_select_days($default_list_day2); ?></select>
</font></td>
</tr>
<tr>
<td colspan="2" align="right"><input type="button" value="����" onclick="showList();"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
<?
}
?>
