<?php
require_once("about_comedix.php");
require_once("show_select_values.ini");
require_once("show_class_name.ini");
require_once("get_values.ini");
require_once("webmail_alias_functions.php");

$fname = $_SERVER["PHP_SELF"];

//勤務シフト作成の個人勤務条件からの場合を除く
if ($wherefrom != "1") { 
    // セッションのチェック
    $session = qualify_session($session, $fname);
    if ($session == "0") {
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
        exit;
    }
    
    // 権限のチェック
    $checkauth = check_authority($session, 18, $fname);
    if ($checkauth == "0") {
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
        exit;
    }
    
    // 職員登録権限を取得
    $reg_auth = check_authority($session, 19, $fname);
}
// 他部署兼務
$list_other_post_flg = array(
        "1" => "有り",
        "" => "無し"
        );

// データベースに接続
$con = connect2db($fname);

// 変更履歴を取得
if (!empty($_REQUEST["empcond_other_post_flg_history_id"])) {
    $empcond_other_post_flg_history_id = pg_escape_string($_REQUEST["empcond_other_post_flg_history_id"]);
    $sql = "SELECT * FROM empcond_other_post_flg_history WHERE empcond_other_post_flg_history_id=$empcond_other_post_flg_history_id";
    $sel = select_from_table($con, $sql, "", $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $hist = pg_fetch_assoc($sel);
}
else {
    $hist = array("histdate" => date('Y-m-d H:i:s'));
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 
<?
if ($wherefrom == "1") {
    echo("勤務シフト作成");
}
else {
    echo("職員登録");
}
?>
 | 他部署兼務履歴</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<?
//勤務シフト作成の個人勤務条件からの場合を除く
 if ($wherefrom != "1") { ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="employee_info_menu.php?session=<? echo($session); ?>"><img src="img/icon/b26.gif" width="32" height="32" border="0" alt="職員登録"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="employee_info_menu.php?session=<? echo($session); ?>"><b>職員登録</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_info_menu.php?session=<? echo($session); ?>&key1=<? echo(urlencode($key1)); ?>&key2=<? echo(urlencode($key2)); ?>&page=<? echo($page); ?>&view=<? echo($view); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">検索</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_list.php?session=<? echo($session); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_detail.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基本</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="employee_contact_setting.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">連絡先</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_condition_setting.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務条件</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#5279a5"><a href="employee_cond_op_history_form.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&empcond_other_post_flg_history_id=<? echo($empcond_other_post_flg_history_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>他部署兼務履歴</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_auth_setting.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_display_setting.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&view=<? echo($view); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示設定</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_menu_setting.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&view=<? echo($view); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メニュー設定</font></a></td>
<? if (webmail_alias_enabled()) { ?>
<td width="5">&nbsp;</td>
<td width="130" align="center" bgcolor="#bdd1e7"><a href="employee_alias_setting.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&view=<? echo($view); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メール転送設定</font></a></td>
<? } ?>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<? }
else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>他部署兼務履歴</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<? } ?>

<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<br />
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">■他部署兼務 変更履歴の追加/変更</font><br />
<br />
<form action="employee_cond_op_history_submit.php" method="post">
<table width="500" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
    <td width="30%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">他部署兼務</font></td>
    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?php
foreach ($list_other_post_flg as $item_id => $item_name) {
    printf('<label><input type="radio" name="other_post_flg" value="%s" %s>%s</label>', $item_id, ($item_id == $hist["other_post_flg"] ? 'checked' : ''), $item_name);
}
        ?>
    </font></td>
</tr>
<tr height="22">
    <td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">変更日時</font></td>
    <td>
        <input type="text" name="histdate" value="<?=substr($hist["histdate"],0,19)?>" size="25"><br />
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">　9999-99-99 99:99:99の形式で入力してください</font>
    </td>
</tr>
</table>

<p>
<button type="submit">登録</button>
</p>

<input type="hidden" name="emp_id" value="<? echo($emp_id); ?>">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="empcond_other_post_flg_history_id" value="<? echo($empcond_other_post_flg_history_id); ?>">
<input type="hidden" name="wherefrom" value="<?php eh($wherefrom); ?>">
</form>

</td>
</tr>
</table>

</body>
</html>
<? pg_close($con); ?>