<?php
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 23, $fname);
if ($auth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
foreach ($area_name_list as $i => $tmp_area_name) {
	if ($tmp_area_name == "") {
		echo("<script type=\"text/javascript\">alert('" . ($i + 1) . "行目のエリア名を入力してください。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}
	if (strlen($tmp_area_name) > 60) {
		echo("<script type=\"text/javascript\">alert('" . ($i + 1) . "行目のエリア名が長すぎます。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}
}
foreach ($order_no_list as $i => $tmp_order_no) {
	if (preg_match("/^\d{1,2}$/", $tmp_order_no) == 0) {
		echo("<script type=\"text/javascript\">alert('" . ($i + 1) . "行目の表示順を半角数字2桁以内で入力してください。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}
}

// エリア情報を配列に保存
$areas = array();
for ($i = 0, $j = count($area_id_list); $i < $j; $i++) {
	$areas[] = array(
		"area_id" => $area_id_list[$i],
		"del_flg" => in_array($area_id_list[$i], $del_area_id_list),
		"area_name" => $area_name_list[$i],
		"order_no" => $order_no_list[$i]
	);
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// エリア情報をループ
foreach ($areas as $tmp_area) {

	if (!$tmp_area["del_flg"]) {

		// エリア情報を更新
		$sql = "update area set";
		$set = array("area_name", "order_no");
		$setvalue = array($tmp_area["area_name"], $tmp_area["order_no"]);
		$cond = "where area_id = {$tmp_area["area_id"]}";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	} else {

		// 部門情報の該当エリア情報をnullに更新
		$sql = "update classmst set";
		$set = array("area_id");
		$setvalue = array(null);
		$cond = "where area_id = {$tmp_area["area_id"]}";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 当直・外来分担表の該当エリア情報をnullに更新
		$sql = "update allotfcl set";
		$set = array("area_id");
		$setvalue = array(null);
		$cond = "where area_id = {$tmp_area["area_id"]}";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// エリア情報を削除
		$sql = "delete from area";
		$cond = "where area_id = {$tmp_area["area_id"]}";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// エリア登録画面を再表示
echo("<script type=\"text/javascript\">location.href = 'area_register.php?session=$session';</script>");
?>
