<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix マスターメンテナンス | 職種一覧</title>
<?
require("about_authority.php");
require("about_session.php");
require("maintenance_menu_list.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 26, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 順番変更処理
if ($action == "up" || $action == "down") {
	pg_query($con, "begin");

	// 選択された職種の表示順を取得
	$sql = "select order_no from jobmst";
	$cond = "where job_id = $job_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$order_no = pg_fetch_result($sel, 0, "order_no");

	// 隣の職種を上げ下げ
	$diff = ($action == "up") ? -1 : 1;
	$sql = "update jobmst set";
	$set = array("order_no");
	$setvalue = array($order_no);
	$cond = "where order_no = " . ($order_no + $diff);
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// 選択職種を上げ下げ
	$sql = "update jobmst set";
	$set = array("order_no");
	$setvalue = array($order_no + $diff);
	$cond = "where job_id = $job_id";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	pg_query($con, "commit");
}

// 職種一覧を取得
$sql = "select job_id, job_nm, order_no, link_key from jobmst";
$cond = "where job_del_flg = 'f' order by order_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 職種一覧を配列に格納
$jobs = array();
$actual_order_no = 1;
while ($row = pg_fetch_array($sel)) {
	$jobs[] = array(
		"id" => $row["job_id"],
		"name" => $row["job_nm"],
		"actual_order_no" => $actual_order_no,
		"order_no" => $row["order_no"],
		"link_key" => $row["link_key"]        
	);
	$actual_order_no++;
}

// トランザクションを開始
pg_query($con, "begin");

// 並び順の再設定
foreach ($jobs as $tmp_job) {
	if ($tmp_job["actual_order_no"] == $tmp_job["order_no"]) {
		continue;
	}

	$sql = "update jobmst set";
	$set = array("order_no");
	$setvalue = array($tmp_job["actual_order_no"]);
	$cond = "where job_id = {$tmp_job["id"]}";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// トランザクションをコミット
pg_query($con, "commit");
?>
<script type="text/javascript">
function deleteJob() {
	if (confirm('削除してよろしいですか？')) {
		document.mainform.submit();
	}
}
</script>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="maintenance_menu.php?session=<? echo($session); ?>"><img src="img/icon/b27.gif" width="32" height="32" border="0" alt="マスターメンテナンス"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="maintenance_menu.php?session=<? echo($session); ?>"><b>マスターメンテナンス</b></a>&nbsp;&gt;&nbsp;<a href="job_menu.php?session=<? echo($session); ?>"><b>職種</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- left -->
<td width="160" valign="top">
<? write_menu_list($session, $fname) ?>
</td>
<!-- left -->
<!-- center -->
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->
<!-- right -->
<td valign="top">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="120" height="22" align="center" bgcolor="#5279a5"><a href="job_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>職種一覧</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="job_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職種登録</font></a></td>
<td align="right"><input type="button" value="削除" onclick="deleteJob();"></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<form name="mainform" action="job_delete.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr height="22" bgcolor="#f6f9ff">
<td width="28">&nbsp;</td>
<td width="40">&nbsp;</td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職種名</font></td>
</tr>
<?
for ($i = 0, $j = count($jobs) - 1; $i <= $j; $i++) {
	$tmp_job = $jobs[$i];
	$tmp_job_id = $tmp_job["id"];
	$tmp_job_name = $tmp_job["name"].($tmp_job["link_key"] ? " ({$tmp_job["link_key"]})" : "");

	if ($i == 0) {
		$tmp_img_html = "<img src=\"img/up-g.gif\" alt=\"\" width=\"17\" height=\"17\" style=\"vertical-align:middle;\">";
	} else {
		$tmp_img_html = "<a href=\"job_menu.php?session=$session&action=up&job_id=$tmp_job_id\"><img src=\"img/up.gif\" alt=\"\" width=\"17\" height=\"17\" border=\"0\" style=\"vertical-align:middle;\"></a>";
	}
	if ($i == $j) {
		$tmp_img_html .= "<img src=\"img/down-g.gif\" alt=\"\" width=\"17\" height=\"17\" style=\"vertical-align:middle;\">";
	} else {
		$tmp_img_html .= "<a href=\"job_menu.php?session=$session&action=down&job_id=$tmp_job_id\"><img src=\"img/down.gif\" alt=\"\" width=\"17\" height=\"17\" border=\"0\" style=\"vertical-align:middle;\"></a>";
	}

	echo("<tr height=\"22\">\n");
	echo("<td align=\"center\"><input type=\"checkbox\" name=\"trashbox[]\" value=\"$tmp_job_id\"></td>\n");
	echo("<td>$tmp_img_html</td>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"job_update.php?session=$session&job_id=$tmp_job_id\">$tmp_job_name</a></font></td>\n");
    echo("</tr>\n");
}
?>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
</td>
<!-- right -->
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
