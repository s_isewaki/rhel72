<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションをチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限をチェック
$checkauth = check_authority($session, 21, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 登録内容を編集
if ($hide_avg != "t") {
	$hide_avg = "f";
}

// データベースに接続
$con = connect2db($fname);

// 表示設定を更新
$sql = "update beddisp set";
$set = array("hide_avg");
$setvalue = array($hide_avg);
$cond = "";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// データベース接続を閉じる
pg_close($con);

// 元画面に戻る
echo "<script type=\"text/javascript\">location.href = 'bed_display_setting.php?session=$session';</script>";
?>
