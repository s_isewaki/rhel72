<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require_once("show_select_values.ini");
require_once("about_comedix.php");
require_once("label_by_profile_type.ini");




// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 79, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);
// メドレポート/ＰＯレポート(Problem Oriented)
$med_report_title = $_label_by_profile["MED_REPORT"][$profile_type];
?>
<title>CoMedix <?=$med_report_title?> | マスタ登録</title>

<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td, .inner td .list td {border:#E6B3D4 solid 1px;}
.inner td {border-style:none;}
.non_in_list {border-collapse:collapse;}
.non_in_list td {border:0px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<form name="mainform" action="fplusadm_mst_insert.php?session=<? echo($session); ?>" method="post">
<table width="720" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#E6B3D4">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>マスタ登録</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="720" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td bgcolor="#feeae9" width="120" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">マスタ管理コード</font></td>
<td style="border-right:none;"><input type="text" name="mst_cd" value="<?=h($mst_cd) ?>" maxlength="4" size="6" style="ime-mode: inactive;">
</td>
<td style="border-left:none;">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="red">※先頭を数字にして登録してください。（例：0001、1001）<br>
先頭が英字のコードをシステムで使用します。（例：A001、Z001）</font></td>
</tr>
<tr height="22">
<td bgcolor="#feeae9" width="120" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">マスタ名称</font></td>
<td colspan="2"><input type="text" name="mst_name" value="<?=h($mst_name) ?>" maxlength="40" size="46" style="ime-mode: active;"></td>
</tr>
<tr height="22">
<td bgcolor="#feeae9" width="120" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">内容</font></td>
<td colspan="2">
<textarea name="mst_content" style="ime-mode: active;width:100%" rows="3"><?=h($mst_content) ?></textarea>
<!--
<input type="text" name="mst_content" value="<? echo($mst_content); ?>" maxlength="100" size="112" style="ime-mode: active;">
-->
</td>
</tr>
</table>
<table width="720" border="0" cellspacing="0" cellpadding="2">
<tr>
<td height="22" align="right"><input type="submit" value="登録"></td>
</tr>
</table>
</form>
</center>
</body>
<? pg_close($con); ?>
</html>
