<?/*

画面パラメータ
	$session
		セッションID
		(必須)
	$pt_id
		患者ID
		(必須)
	$diag_div
		診療区分名称
		(必須)
	$tmpl_id
		テンプレートID
		フリー入力の場合は"non_tmpl"
		省略時はデフォルトテンプレート、なければ使用テンプレートのうちもっともtmpl_idの小さいものが対象となる。
	$s_date
		日付（YYYYMMDD）
		省略時はシステム日付。
	$offset
		読み飛ばす件数
		省略時は0
	$sct_id
		診療科ID(抽出条件)
	$problem_id
		プロブレムID(抽出条件)
	$creater_emp_id
		記載者の利用者ID(抽出条件)
*/

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css"/>
<?
require_once("about_comedix.php");
require("summary_tmpl_util.ini");
require_once("summary_common.ini");
require("get_values.ini");
require("label_by_profile_type.ini");
require_once("get_menu_label.ini");
require_once("show_select_values.ini");
require_once("yui_calendar_util.ini");

//==============================
//ページ名
//==============================
$fname = $PHP_SELF;

//==============================
//セッションのチェック
//==============================
$session = qualify_session($session,$fname);
if($session == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
// 権限チェック
//==============================
$summary = check_authority($session, 57, $fname);
if ($summary == "0")
{
	showLoginPage();
	exit;
}

//==============================
//DBコネクション
//==============================
$con = connect2db($fname);
if($con == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

//==============================
//使用中のテンプレート一覧情報を取得
//==============================

//SQL作成
$sql = "";
$sql .= " select tmplmst.* from ";
$sql .= " (";
$sql .= " select distinct tmpl_id from summary where ptif_id = '".$pt_id."' and diag_div = '".$diag_div."'";
$sql .= " ) use_tmpl";
$sql .= " left join tmplmst on use_tmpl.tmpl_id = tmplmst.tmpl_id";
$sql .= " order by tmpl_id";
$cond =  "";

//SQL実行
$sel_tmpl = select_from_table($con,$sql,$cond,$fname);
if($sel_tmpl == 0){
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

//検索結果件数
$sel_tmpl_num = pg_numrows($sel_tmpl);

//==============================
//対象の診療記録がない場合は表示しない
//==============================
if($sel_tmpl_num == 0){
	// 診療記録/記録
	$summary_record_title = $_label_by_profile["SUMMARY_RECORD"][$profile_type];
	?>
	<script language='javascript'>
	alert("「<?=str_replace("\"","\\\"",$diag_div) ?>」の<?=$summary_record_title?>は登録されていません。");
	window.close();
	</script>
	</head>
	<body>
	</body>
	</html>
	<?
	pg_close($con);
	exit;
}

//==============================
//使用中のテンプレート数を取得
//==============================
if($sel_tmpl_num > 0){
	$work_____last_tmpl_id = pg_fetch_result($sel_tmpl, $sel_tmpl_num - 1, "tmpl_id");
	if($work_____last_tmpl_id != "") {
		//診療記録は全部テンプレートを使用している
		$tmpl_count = $sel_tmpl_num;
	} else {
		//テンプレートを使用していない診療記録が存在する
		$tmpl_count = $sel_tmpl_num -1;
	}
} else {
	//診療記録がない。(※正常な画面遷移の場合はありえない)
	$tmpl_count = 0;
}

//==============================
//デフォルトテンプレートIDを取得 (表示対象テンプレートが未指定、かつ複数テンプレートの場合のみ)
//==============================
if(@$tmpl_id == "" && @$tmpl_count > 1) {
	//SQL作成
	$sql = "";
	$sql .= " select tmpl_div_link.tmpl_id from";
	$sql .= " (";
	$sql .= " select div_id from divmst where diag_div like '".$diag_div."'";
	$sql .= " ) target_div";
	$sql .= " left join tmpl_div_link on target_div.div_id = tmpl_div_link.div_id";
	$sql .= " and tmpl_div_link.default_flg = 't'";
	$cond =  "";

	//SQL実行
	$sel = select_from_table($con,$sql,$cond,$fname);
	if($sel == 0){
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	//デフォルトテンプレートIDの決定
	$sel_num = pg_numrows($sel);
	if($sel_num > 0) {
		$default_tmpl_id = pg_fetch_result($sel,0,"tmpl_id");
	} else {
		$default_tmpl_id = "";//診療記録区分に対してデフォルト指定なし(ありえない)
	}
}

//==============================
//表示対象テンプレートの決定
//==============================

//パラメータに指定されていた場合
if(@$tmpl_id != "") {
	if($tmpl_id == "non_tmpl") {
		//フリー入力
		$disp_tmpl_id = "";
	} else {
		//指定されたテンプレート
		$disp_tmpl_id = $tmpl_id;
	}
}
//テンプレートなしの場合
elseif($tmpl_count == 0) {
	//フリー入力
	$disp_tmpl_id = "";
}
//テンプレート１つだけの場合
elseif($tmpl_count == 1) {
	//唯一のテンプレート
	$disp_tmpl_id = pg_fetch_result($sel_tmpl, 0, "tmpl_id");
}
//デフォルトテンプレートが設定されている場合
elseif($default_tmpl_id != "") {
	//デフォルトのテンプレート
	$disp_tmpl_id = $default_tmpl_id;
}
//診療記録区分に対してデフォルト指定なしの場合 (ありえない)
else {
	//先頭のテンプレート
	$disp_tmpl_id = pg_fetch_result($sel_tmpl, 0, "tmpl_id");
}

$disp_tmpl_id_param = $disp_tmpl_id;
if($disp_tmpl_id_param == "") {
	$disp_tmpl_id_param = "non_tmpl";
}


//==============================
//表示対象テンプレートの情報を取得
//==============================
$disp_tmpl_file = "";
$disp_tmpl_name = "フリー入力";
for($i=0;$i<$sel_tmpl_num;$i++) {
	$work_____tmpl_id = pg_fetch_result($sel_tmpl, $i, "tmpl_id");
	if($work_____tmpl_id == $disp_tmpl_id) {
		$disp_tmpl_file = pg_fetch_result($sel_tmpl, $i, "tmpl_file");
		$disp_tmpl_name = pg_fetch_result($sel_tmpl, $i, "tmpl_name");
		break;
	}
}

//==============================
//フローシート表示判定(テンプレート定義を読み込む)
//==============================
if($disp_tmpl_file != "") {
	require_once($disp_tmpl_file);
	$is_flowsheet_usable_tmpl = function_exists("write_flowsheet_page");
} else {
	$is_flowsheet_usable_tmpl = false;
}

//==============================
//診療記録情報を取得
//==============================
//メモ：抽出条件は、画面内項目より選択されるため、必ず１件は存在する。

//==============================
//一覧表示する診療記録を特定
//==============================

//一覧表示件数
$list_disp_count = 4;

// 日付・読み飛ばし件数
$s_date = isset($_REQUEST["s_date"]) ? $_REQUEST["s_date"] : date("Ymd");
$offset = isset($_REQUEST["offset"]) ? $_REQUEST["offset"] : 0;

// SQL作成
if ($offset >= 0) {
	$sql = "select distinct * from summary where ptif_id = '$pt_id' and diag_div like '$diag_div'";
	if (@$disp_tmpl_id != "") {
		$sql .= " and tmpl_id = $disp_tmpl_id";
	} else {
		$sql .= " and tmpl_id is null ";
	}
	if (@$sct_id != "") {
		$sql .= " and sect_id = $sct_id";
	}
	if (@$problem_id != "") {
		$sql .= " and problem_id = $problem_id";
	}
	if (@$creater_emp_id != "") {
		$sql .= " and emp_id = '".$creater_emp_id."'";
	}
	$sql .= " and cre_date <= '$s_date' order by cre_date desc, summary_id desc offset $offset limit $list_disp_count";
} else {
	$sql = "select * from (select distinct * from summary where ptif_id = '$pt_id' and diag_div like '$diag_div'";
	if (@$disp_tmpl_id != "") {
		$sql .= " and tmpl_id = $disp_tmpl_id";
	} else {
		$sql .= " and tmpl_id is null ";
	}
	if (@$sct_id != "") {
		$sql .= " and sect_id = $sct_id";
	}
	if (@$problem_id != "") {
		$sql .= " and problem_id = $problem_id";
	}
	if (@$creater_emp_id != "") {
		$sql .= " and emp_id = '".$creater_emp_id."'";
	}
	$sql .= " and cre_date > '$s_date' order by cre_date, summary_id offset " . (($offset + $list_disp_count) * -1) . " limit $list_disp_count) t order by cre_date desc, summary_id desc";
}
$cond =  "";

// SQL実行
$sel_sum = select_from_table($con,$sql,$cond,$fname);
if($sel_sum == 0){
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}


//==========================================================================================
//HTML出力
//==========================================================================================
?>
<title>CoMedix <?=get_report_menu_label($con, $fname)?>｜フローシート</title>
<script type="text/javascript">

function load_action() {
	initcal();
	updateCal1();
}

var m_param_s_date = "<?=@$s_date?>";
var m_param_offset = "<?=@$offset?>";
var m_param_tmpl_id = "<?=@$disp_tmpl_id_param?>";
var m_param_sct_id = "<?=@$sct_id?>";
var m_param_problem_id = "<?=@$problem_id?>";
var m_param_creater_emp_id = "<?=@$creater_emp_id?>";

function clear_extract_params() {
	m_param_sct_id = "";
	m_param_problem_id = "";
	m_param_creater_emp_id = "";
}

function postback() {
	var url = "summary_flowsheet.php"
			+ "?session=<?=@$session?>"
			+ "&pt_id=<?=@$pt_id?>"
			+ "&diag_div=<?=urlencode(@$diag_div)?>"
			+ "&s_date=" + m_param_s_date
			+ "&offset=" + m_param_offset
			+ "&tmpl_id=" + m_param_tmpl_id
			+ "&sct_id=" + m_param_sct_id
			+ "&problem_id=" + m_param_problem_id
			+ "&creater_emp_id=" + m_param_creater_emp_id
			;
	location.href = url;
}

function changeDate() {
	var s_year  = document.getElementById('date_y1').value;
	var s_month = document.getElementById('date_m1').value;
	var s_day   = document.getElementById('date_d1').value;

	var year  = parseInt(s_year , 10);
	var month = parseInt(s_month, 10) - 1;
	var day   = parseInt(s_day  , 10);

	var date = new Date(year, month, day);
	if (date.getFullYear() != year || date.getMonth() != month || date.getDate() != day) {
		alert('日付が不正です。');
		return;
	}

	m_param_s_date = s_year.concat(s_month).concat(s_day);
	m_param_offset = "0";
	postback();
}
</script>
<?
write_yui_calendar_use_file_read_0_12_2();
write_yui_calendar_script2(1);
?>
</head>
<body onload="load_action();">
<div style="padding:4px">

<!-- ヘッダエリア -->
<?= summary_common_show_dialog_header("フローシート"); ?>


<? //本体 出力 ?>

<!-- 診療区分情報 START -->
<table class="list_table" style="margin-top:4px">
	<tr>
		<th align="center" width="100"><nobr><?=$_label_by_profile["SUMMARY_KUBUN"][$profile_type] ?></nobr></th>
		<td><nobr><?=h($diag_div)?></nobr></td>
	</tr>
</table>


<!-- 診療区分情報 END -->



<!-- テンプレート選択 START -->
<div style="background-color:#f4f4f4; padding-top:5px; padding-bottom:5px; margin-top:10px">
<table style="width:100%">
	<tr>
		<?
		for($i=0;$i<$sel_tmpl_num;$i++) {
			$sel_tmpl_id = pg_fetch_result($sel_tmpl,$i,"tmpl_id");
			$sel_tmpl_id_param = $sel_tmpl_id;
			$sel_tmpl_name = pg_fetch_result($sel_tmpl,$i,"tmpl_name");
			if($sel_tmpl_id == "") {
				$sel_tmpl_id_param = "non_tmpl";
				$sel_tmpl_name = "フリー入力";
			}
			$bg = ($sel_tmpl_id == $disp_tmpl_id) ? "background-color:#f8fdb7; border:1px solid #c5d506" : "";
		?>
				<td style="text-align:center; padding:2px; width:3%; <?=$bg?>">
					<nobr><a href="javascript:m_param_tmpl_id='<?=$sel_tmpl_id_param?>';clear_extract_params();postback();">
						<?= h($sel_tmpl_name) ?></a></nobr>
				</td>
				<td width="10">&nbsp</td>
		<? } ?>
		<!-- テンプレート選択 END -->

		<td width="50"></td>

		<!-- 日付選択 START -->
<?
$date_y1 = substr($s_date, 0, 4);
$date_m1 = substr($s_date, 4, 2);
$date_d1 = substr($s_date, 6, 2);
$sum_count = pg_num_rows($sel_sum);
?>
		<td nowrap>
<div style="float:left;"><select id="date_y1"><? show_select_years_span(min($date_y1, 1970), max($date_y1, date("Y") + 1), $date_y1, false); ?></select>/<select id="date_m1"><? show_select_months($date_m1, true); ?></select>/<select id="date_d1"><? show_select_days($date_d1, true); ?></select></div>
<div style="float:left;margin-left:2px;margin-right:3px;"><img src="img/calendar_link.gif" style="left:3px;z-index:1;cursor:pointer;" onclick="show_cal1();"><br><div id="cal1Container" style="position:absolute;display:none;z-index:10000;"></div></div>
<div style="float:left;"><input type="button" value="表示" onclick="changeDate();"></div>
<div style="float:left;margin-left:20px;"><a href="javascript:m_param_offset='<? echo($offset + $list_disp_count); ?>';postback();">＜前</a></div>
<div style="float:left;margin-left:20px;"><a href="javascript:m_param_offset='<? echo($offset - $list_disp_count); ?>';postback();">後＞</a></div>
		</td>
		<!-- 日付選択 END -->

	</tr>
</table>
</div>
<!-- 選択ヘッダー END -->

<?
if ($sum_count > 0) {
?>

<!-- 一覧 START -->
<table class="prop_table">



	<!-- 一覧:作成年月日 START -->
	<tr>
		<th align="center" width="150"><nobr>作成年月日</nobr></th>
		<?
			for ($i = $sum_count - 1; $i >= 0; $i--) {
				$cre_date = pg_fetch_result($sel_sum, $i, "cre_date");
				$disp_date = substr($cre_date,0,4)."年".((int)substr($cre_date,4,2))."月".((int)substr($cre_date,6,2))."日";
				echo '<th align="center"><nobr>'.$disp_date.'</nobr></th>';
			}
		?>
	</tr>
	<!-- 一覧:作成年月日 END -->



	<!-- 一覧:診療科 START -->
	<tr>
		<th align="center" width="150"><nobr><?=$_label_by_profile["SECTION"][$profile_type] ?></nobr></th>
		<?
		for ($i = $sum_count - 1; $i >= 0; $i--) {
			$line_sect_id = pg_fetch_result($sel_sum, $i, "sect_id");
			$line_sect_name = get_sect_name($line_sect_id,$con,$fname);
		?>
		<td align="left">
			<nobr>
				<a href="javascript:clear_extract_params();m_param_sct_id='<?=$line_sect_id?>';postback();">
				<?=h($line_sect_name)?>
				</a>
			</nobr>
		</td>
		<? } ?>
	</tr>
	<!-- 一覧:診療科 END -->



	<!-- 一覧:プロブレム START -->
	<tr>
		<th align="center" width="150"><nobr>プロブレム</nobr></th>
		<?
		for ($i = $sum_count - 1; $i >= 0; $i--) {
			$line_problem_id = pg_fetch_result($sel_sum, $i, "problem_id");
			$line_problem = pg_fetch_result($sel_sum, $i, "problem");
		?>
		<td align="left">
			<nobr>
			<a href="javascript:clear_extract_params();m_param_problem_id='<?=$line_problem_id?>';postback();">
			<?=h($line_problem)?>
			</a>
			</nobr>
		</td>
		<? } ?>
	</tr>
	<!-- 一覧:プロブレム END -->



	<!-- 一覧:記載者 START -->
	<tr>
		<th align="center" width="150"><nobr>記載者</nobr></th>
		<?
		for ($i = $sum_count - 1; $i >= 0; $i--) {
			$line_creater_emp_id = pg_fetch_result($sel_sum, $i, "emp_id");
			$line_creater_emp_name = get_emp_kanji_name($con,$line_creater_emp_id,$fname);
		?>
		<td align="left">
			<nobr>
			<a href="javascript:clear_extract_params();m_param_creater_emp_id='<?=$line_creater_emp_id?>';postback();">
			<?=h($line_creater_emp_name)?>
			</a>
			</nobr>
		</td>
		<? } ?>
	</tr>
	<!-- 一覧:記載者 END -->



	<!-- 一覧:記事重要度 START -->
	<tr>
		<th align="center" width="150"><nobr>記事重要度</nobr></th>
		<?
		for ($i = $sum_count - 1; $i >= 0; $i--) {
			$list_priority = pg_fetch_result($sel_sum, $i, "priority");
		?>
		<td align="left"><nobr><?=h($list_priority)?></nobr></td>
		<? } ?>
	</tr>
	<!-- 一覧:記事重要度 END -->



	<!-- 一覧:内容 START -->
<?
	if($is_flowsheet_usable_tmpl) { //フローシート対応テンプレートの場合
		//データXMLファイル配列を取得する。
		$xml_files = array();
		for ($i = $sum_count - 1; $i >= 0; $i--) {
			$creater_emp_id = pg_fetch_result($sel_sum, $i, "emp_id");
			$summary_id = pg_fetch_result($sel_sum, $i, "summary_id");
			$xml_file = get_tmpl_xml_file_name($creater_emp_id, $pt_id, $summary_id);
			$xml_files[] = $xml_file;
		}
		write_flowsheet_page($xml_files,count($xml_files));//テンプレートインターフェース関数より一覧を出力
	} else {//フローシート未対応の場合
	//テンプレート未対応の場合は内容のみを表示
?>
	<tr>
		<th align="center"><nobr>内容</nobr></th>
		<?
		for ($i = $sum_count - 1; $i >= 0; $i--) {
			$summary_id = pg_fetch_result($sel_sum, $i, "summary_id");
			$naiyo = get_summary_naiyo_html($con,$fname,$summary_id,$pt_id);
			if($disp_tmpl_id != "") {//テンプレートの場合
		?>
		<td align="left" valign="top">
			<?=$naiyo?>
		</td>
		<? } else {//フリー入力の場合 ?>
		<td align="left" valign="top"><?=$naiyo?></td>
		<? } ?>
<? } ?>
	</tr>
<? } ?>
<!-- 一覧:内容 END -->
</table>

<? } else { ?>
<p style="margin:0;">表示対象がありません。</p>
<? } ?>

<!-- 一覧 END -->
</div>
</body>
</html>
<?

pg_close($con);
?>
