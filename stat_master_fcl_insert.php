<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" action="stat_master_fcl_register.php" method="post">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="name" value="<? echo($name); ?>">
<input type="hidden" name="area_id" value="<? echo($area_id); ?>">
<input type="hidden" name="target_id_list1" value="<? echo($target_id_list1); ?>">
<?
if ($target_id_list1 != "") {
	$emp_ids = split(",", $target_id_list1);
} else {
	$emp_ids = array();
}
?>
<input type="hidden" name="back" value="t">
</form>
<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 52, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if ($name == "") {
	echo("<script type=\"text/javascript\">alert('施設名が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if (strlen($name) > 80) {
	echo("<script type=\"text/javascript\">alert('施設名が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
/*
if (!is_array($emp_ids)) {
	echo("<script type=\"text/javascript\">alert('統計入力可能職員が選択されていません。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
*/

if ($area_id == "") {$area_id = null;}

// データベースへ接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin transaction");

// 施設IDを採番
$sql = "select max(statfcl_id) from statfcl";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$id = intval(pg_fetch_result($sel, 0, 0)) + 1;

// 施設情報を登録
$sql = "insert into statfcl (statfcl_id, name, area_id) values (";
$content = array($id, $name, $area_id);
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 統計入力可能職員を登録
foreach ($emp_ids as $emp_id) {
	$sql = "insert into statemp (statfcl_id, emp_id) values (";
	$content = array($id, $emp_id);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 施設一覧画面に遷移
echo("<script type=\"text/javascript\">location.href = 'stat_master_fcl_list.php?session=$session'</script>");
?>
</body>
