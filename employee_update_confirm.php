<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" method="post" action="employee_detail.php">
<input type="hidden" name="personal_id" value="<? echo($personal_id); ?>">
<input type="hidden" name="lt_nm" value="<? echo($lt_nm); ?>">
<input type="hidden" name="ft_nm" value="<? echo($ft_nm); ?>">
<input type="hidden" name="lt_kana_nm" value="<? echo($lt_kana_nm); ?>">
<input type="hidden" name="ft_kana_nm" value="<? echo($ft_kana_nm); ?>">
<input type="hidden" name="id" value="<? echo($id); ?>">
<input type="hidden" name="pass" value="<? echo($pass); ?>">
<input type="hidden" name="mail_id" value="<? echo($mail_id); ?>">
<input type="hidden" name="sex" value="<? echo($sex); ?>">
<input type="hidden" name="birth_yr" value="<? echo($birth_yr); ?>">
<input type="hidden" name="birth_mon" value="<? echo($birth_mon); ?>">
<input type="hidden" name="birth_day" value="<? echo($birth_day); ?>">
<input type="hidden" name="entry_yr" value="<? echo($entry_yr); ?>">
<input type="hidden" name="entry_mon" value="<? echo($entry_mon); ?>">
<input type="hidden" name="entry_day" value="<? echo($entry_day); ?>">
<input type="hidden" name="retire_yr" value="<? echo($retire_yr); ?>">
<input type="hidden" name="retire_mon" value="<? echo($retire_mon); ?>">
<input type="hidden" name="retire_day" value="<? echo($retire_day); ?>">
<input type="hidden" name="suspend" value="<? echo($suspend); ?>">
<input type="hidden" name="cls" value="<? echo($cls); ?>">
<input type="hidden" name="atrb" value="<? echo($atrb); ?>">
<input type="hidden" name="dept" value="<? echo($dept); ?>">
<input type="hidden" name="room" value="<? echo($room); ?>">
<input type="hidden" name="status" value="<? echo($status); ?>">
<input type="hidden" name="job" value="<? echo($job); ?>">
<input type="hidden" name="group_id" value="<? echo($group_id); ?>">
<input type="hidden" name="idm" value="<? echo($idm); ?>">
<input type="hidden" name="aprv" value="<? echo($aprv); ?>">
<input type="hidden" name="webmail_quota" value="<? echo($webmail_quota); ?>">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="emp_id" value="<? echo($emp_id); ?>">
<input type="hidden" name="key1" value="<? echo($key1); ?>">
<input type="hidden" name="key2" value="<? echo($key2); ?>">
<input type="hidden" name="class_cond" value="<? echo($class_cond); ?>">
<input type="hidden" name="atrb_cond" value="<? echo($atrb_cond); ?>">
<input type="hidden" name="dept_cond" value="<? echo($dept_cond); ?>">
<input type="hidden" name="room_cond" value="<? echo($room_cond); ?>">
<input type="hidden" name="job_cond" value="<? echo($job_cond); ?>">
<input type="hidden" name="page" value="<? echo($page); ?>">
<input type="hidden" name="view" value="<? echo($view); ?>">
<input type="hidden" name="emp_o" value="<? echo($emp_o); ?>">
<input type="hidden" name="back" value="t">
<input type="hidden" name="updatephoto" value="<? echo($updatephoto); ?>">
<input type="hidden" name="clearphoto" value="<? echo($clearphoto); ?>">
<input type="hidden" name="profile" value="<? echo($profile); ?>">
<input type="hidden" name="concurrent" value="<? echo($concurrent); ?>">
<input type="hidden" name="emp_name_deadlink_flg" value="<? echo($emp_name_deadlink_flg); ?>">
<input type="hidden" name="emp_class_deadlink_flg" value="<? echo($emp_class_deadlink_flg); ?>">
<input type="hidden" name="emp_job_deadlink_flg" value="<? echo($emp_job_deadlink_flg); ?>">
<input type="hidden" name="emp_st_deadlink_flg" value="<? echo($emp_st_deadlink_flg); ?>">
<?
for ($i = 1; $i <= $concurrent; $i++) {
	$class_var_name = "o_cls$i";
	$atrb_var_name = "o_atrb$i";
	$dept_var_name = "o_dept$i";
	$room_var_name = "o_room$i";
	$status_var_name = "o_status$i";
?>
<input type="hidden" name="<? echo($class_var_name); ?>" value="<? echo($$class_var_name); ?>">
<input type="hidden" name="<? echo($atrb_var_name); ?>" value="<? echo($$atrb_var_name); ?>">
<input type="hidden" name="<? echo($dept_var_name); ?>" value="<? echo($$dept_var_name); ?>">
<input type="hidden" name="<? echo($room_var_name); ?>" value="<? echo($$room_var_name); ?>">
<input type="hidden" name="<? echo($status_var_name); ?>" value="<? echo($$status_var_name); ?>">
<?
}
?>
</form>
<?
//ファイルの読み込み
require_once("Cmx.php");
require_once("about_postgres.php");
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_validation.php");
require_once("show_class_name.ini");
require_once("get_values.ini");
require_once("passwd_change_common.ini");
require("employee_auth_common.php");
require_once("aclg_set.php");
require_once("webmail_quota_functions.php");

//ページ名
$fname = $PHP_SELF;

//セッションチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

//職員登録権限チェック
$reg_auth = check_authority($session,19,$fname);
if($reg_auth == "0"){
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

$arr_class_name = get_class_name_array($con, $fname);

// 環境設定情報を取得
$sql = "select site_id, use_cyrus, hide_passwd from config";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
$site_id = pg_fetch_result($sel, 0, "site_id");
$use_cyrus = pg_fetch_result($sel, 0, "use_cyrus");
$hide_passwd = pg_fetch_result($sel, 0, "hide_passwd");

// 現在のログインユーザのemp_idを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
$login_emp_id = pg_fetch_result($sel, 0, "emp_id");

// パスワードをマスクする設定の場合、かつ入力されたパスワードが「********」の場合
if ($hide_passwd == "t" && $pass == "********") {

	// 既存のパスワードが入力されたものとみなす
	$sql = "select emp_login_pass from login";
	$cond = "where emp_id = '$emp_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$pass = pg_fetch_result($sel, 0, "emp_login_pass");
}

//**********正規表現チェック**********
//----------名前情報----------
//苗字（漢字）null check
if($lt_nm ==""){
	echo("<script language=\"javascript\">alert(\"苗字（漢字）を入力してください。\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

//苗字（漢字）文字数チェック
if(strlen($lt_nm) > 20){
	echo("<script language=\"javascript\">alert(\"苗字（漢字）が長すぎます。\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

//名前（漢字）null check
if($ft_nm ==""){
	echo("<script language=\"javascript\">alert(\"名前（漢字）を入力してください。\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

//名前（漢字）文字数チェック
if(strlen($ft_nm) > 20){
	echo("<script language=\"javascript\">alert(\"名前（漢字）が長すぎます。\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

//苗字（かな）null check
if($lt_kana_nm ==""){
	echo("<script language=\"javascript\">alert(\"苗字（ひらがな）を入力してください。\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}
$lt_kana_nm = mb_convert_kana($lt_kana_nm, "HVc");

//苗字（かな）文字数チェック
if(strlen($lt_kana_nm) > 20){
	echo("<script language=\"javascript\">alert(\"苗字（ひらがな）が長すぎます。\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

//名前（かな）null check
if($ft_kana_nm ==""){
	echo("<script language=\"javascript\">alert(\"名前（ひらがな）を入力してください。\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}
$ft_kana_nm = mb_convert_kana($ft_kana_nm, "HVc");

//名前（かな）文字数チェック
if(strlen($ft_kana_nm) > 20){
	echo("<script language=\"javascript\">alert(\"名前（ひらがな）が長すぎます。\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

//ログインID null check
if($id ==""){
	echo("<script language=\"javascript\">alert(\"ログインIDを入力してください。\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

//ログインID 文字数チェック
if(strlen($id) > 20){
	echo("<script language=\"javascript\">alert(\"ログインIDが長すぎます。\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

// 文字種チェック
if ($site_id == "") {
	if (preg_match("/[^0-9a-z_-]/", $id) > 0) {
		echo("<script type=\"text/javascript\">alert(\"ログインIDに使える文字は半角の「0〜9」「a〜z」「_」「-」のみです。\");</script>\n");
		echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
		exit;
	}
} else {
	if (preg_match("/[^0-9a-zA-Z_-]/", $id) > 0) {
		echo("<script type=\"text/javascript\">alert(\"ログインIDに使える文字は半角の「0〜9」「a〜z」「A〜Z」「_」「-」のみです。\");</script>\n");
		echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
		exit;
	}
}
if (substr($id, 0, 1) == "-") {
	echo("<script type=\"text/javascript\">alert(\"ログインIDの先頭に「-」は使えません。\");</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

// 登録禁止アカウントチェック
if ($site_id == "") {
	if ($id == "cyrus" || $id == "postmaster" || $id == "root") {
		echo("<script language=\"javascript\">alert(\"「{$id}」はログインIDとして使用できません。\");</script>\n");
		echo("<script language=\"javascript\">document.items.submit();</script>\n");
		exit;
	}
}

//パスワード null check
if($pass ==""){
	echo("<script language=\"javascript\">alert(\"パスワードを入力してください。\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

//パスワード 文字数チェック
$pass_length = get_pass_length($con, $fname);
if(strlen($pass) > $pass_length){
	echo("<script language=\"javascript\">alert(\"パスワードが長すぎます。\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

// 文字種チェック
if (preg_match("/[^0-9a-zA-Z_-]/", $pass) > 0) {
	echo("<script language=\"javascript\">alert(\"パスワードに使える文字は半角の「0〜9」「a〜z」「A〜Z」「_」「-」のみです。\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}
if (substr($pass, 0, 1) == "-") {
	echo("<script type=\"text/javascript\">alert(\"パスワードの先頭に「-」は使えません。\");</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

// メールIDチェック
if ($site_id != "") {
	if ($mail_id == "") {
		echo("<script type=\"text/javascript\">alert(\"メールIDを入力してください。\");</script>\n");
		echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
		exit;
	}

	if (strlen($mail_id) > 20) {
		echo("<script type=\"text/javascript\">alert(\"メールIDが長すぎます。\");</script>\n");
		echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
		exit;
	}

	if ($use_cyrus == "t") {
		if (preg_match("/[^0-9a-z_-]/", $mail_id) > 0) {
			echo("<script type=\"text/javascript\">alert(\"メールIDに使える文字は半角の「0〜9」「a〜z」「_」「-」のみです。\");</script>\n");
			echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
			exit;
		}
	} else {
		if (preg_match("/[^0-9a-z_.-]/", $mail_id) > 0) {
			echo("<script type=\"text/javascript\">alert(\"メールIDに使える文字は半角の「0〜9」「a〜z」「_」「-」「.」のみです。\");</script>\n");
			echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
			exit;
		}
	}

	if (substr($mail_id, 0, 1) == "-") {
		echo("<script type=\"text/javascript\">alert(\"メールIDの先頭に「-」は使えません。\");</script>\n");
		echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
		exit;
	}
}

// ログインIDとパスワードが同じ場合
if ($id == $pass) {

	// 環境設定情報を取得
	$sql = "select weak_pwd_flg from config";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$weak_pwd_flg = pg_fetch_result($sel, 0, "weak_pwd_flg");

	// 許可しない設定の場合はエラーとする
	if ($weak_pwd_flg == "f") {
		echo("<script language=\"javascript\">alert('ログインIDと同じパスワードは使用できません。');</script>\n");
		echo("<script language=\"javascript\">document.items.submit();</script>\n");
		exit;
	}
}

//所属部門
if($cls =="0"){
	echo("<script language=\"javascript\">alert(\"所属{$arr_class_name[0]}を選択してください。\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

if ($birth_yr == "-" && $birth_mon == "-" && $birth_day == "-") {
	$birth = "";
} else if ($birth_yr != "-" && $birth_mon != "-" && $birth_day != "-") {
	if (!checkdate($birth_mon, $birth_day, $birth_yr)) {
		echo("<script type=\"text/javascript\">alert('生年月日が不正です。');</script>\n");
		echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
		exit;
	}
	$birth = "$birth_yr$birth_mon$birth_day";
} else {
	echo("<script type=\"text/javascript\">alert('生年月日が不正です。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

if ($entry_yr == "-" && $entry_mon == "-" && $entry_day == "-") {
	$entry = "";
} else if ($entry_yr != "-" && $entry_mon != "-" && $entry_day != "-") {
	if (!checkdate($entry_mon, $entry_day, $entry_yr)) {
		echo("<script type=\"text/javascript\">alert('入職日が不正です。');</script>\n");
		echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
		exit;
	}
	$entry = "$entry_yr$entry_mon$entry_day";
} else {
	echo("<script type=\"text/javascript\">alert('入職日が不正です。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

//所属課
if($atrb =="0"){
	echo("<script language=\"javascript\">alert(\"所属{$arr_class_name[1]}を選択してください。\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

//所属科
if($dept =="0"){
	echo("<script language=\"javascript\">alert(\"所属{$arr_class_name[2]}を選択してください。\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

// 所属室（null可とする）
if ($room == "0") {
	$room = null;
}

// ICカードIDm
if (strlen($idm) > 32) {
	echo("<script language=\"javascript\">alert('ICカードIDmが長すぎます。');</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}
if ($idm != "") {
	$sql = "select count(*) from empmst";
	$cond = "where emp_idm = '$idm' and exists (select * from authmst where authmst.emp_id = empmst.emp_id and emp_del_flg = 'f') and emp_id <> '$emp_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (intval(pg_fetch_result($sel, 0, 0)) > 0) {
		echo("<script language=\"javascript\">alert('入力されたICカードIDmは、すでに使われています。');</script>\n");
		echo("<script language=\"javascript\">document.items.submit();</script>\n");
		exit;
	}
}

// メールボックスの容量制限
$webmail_default_quota = webmail_quota_get_default();
if ($webmail_default_quota > 0) {
	if ($webmail_quota == "") {
		$webmail_quota = null;
		$webmail_actual_quota = $webmail_default_quota;
	} else if (preg_match("/[1-9][0-9]{0,2}\z/", $webmail_quota)) {
		$webmail_actual_quota = $webmail_quota;
	} else {
		echo("<script type=\"text/javascript\">alert('メールボックスの容量は、1〜999MBの間で入力してください。');</script>");
		echo("<script language=\"javascript\">document.items.submit();</script>\n");
		exit;
	}
} else {
	$webmail_quota = null;
	$webmail_actual_quota = null;
}

if ($retire_yr == "-" && $retire_mon == "-" && $retire_day == "-") {
	$retire = "";
} else if ($retire_yr != "-" && $retire_mon != "-" && $retire_day != "-") {
	if (!checkdate($retire_mon, $retire_day, $retire_yr)) {
		echo("<script type=\"text/javascript\">alert('退職日が不正です。');</script>\n");
		echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
		exit;
	}
	$retire = "$retire_yr$retire_mon$retire_day";
} else {
	echo("<script type=\"text/javascript\">alert('退職日が不正です。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

// 兼務所属
$concurrents = array();
for ($i = 1; $i <= $concurrent; $i++) {
	$class_var_name = "o_cls$i";
	$atrb_var_name = "o_atrb$i";
	$dept_var_name = "o_dept$i";
	$room_var_name = "o_room$i";
	$status_var_name = "o_status$i";

	if ($$class_var_name == "0") {
		echo("<script type=\"text/javascript\">alert('{$i}番目の兼務所属の所属{$arr_class_name[0]}を選択してください。');</script>\n");
		echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
		exit;
	}
	if ($$atrb_var_name == "0") {
		echo("<script type=\"text/javascript\">alert('{$i}番目の兼務所属の所属{$arr_class_name[1]}を選択してください。');</script>\n");
		echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
		exit;
	}
	if ($$dept_var_name == "0") {
		echo("<script type=\"text/javascript\">alert('{$i}番目の兼務所属の所属{$arr_class_name[2]}を選択してください。');</script>\n");
		echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
		exit;
	}
	if ($$room_var_name == 0) {
		$$room_var_name = null;
	}

	if ($$class_var_name == $cls && $$atrb_var_name == $atrb && $$dept_var_name == $dept && $$room_var_name == $room && $$status_var_name == $status) {
		continue;
	}

	$tmp_concurrent = array(
		"class" => $$class_var_name,
		"atrb" => $$atrb_var_name,
		"dept" => $$dept_var_name,
		"room" => $$room_var_name,
		"status" => $$status_var_name
	);

	if (in_array($tmp_concurrent, $concurrents)) {
		continue;
	}

	$concurrents[] = $tmp_concurrent;
}

if (count($concurrents) != $concurrent) {
	echo("<script type=\"text/javascript\">alert('所属が重複しています。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

//----------名前情報----------
$keywd = get_keyword($lt_kana_nm);

//----------Transaction begin----------
pg_query($con,"begin transaction");

// 職員ID重複チェック
$sql_dup_psn_id = "select count(*) from empmst";
$cond_dup_psn_id = "where emp_personal_id = '$personal_id' and emp_id <> '$emp_id'";
$sel_dup_psn_cnt = select_from_table($con, $sql_dup_psn_id, $cond_dup_psn_id, $fname);
if ($sel_dup_psn_cnt == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$dup_psn_cnt = pg_result($sel_dup_psn_cnt, 0, 0);
if ($dup_psn_cnt > 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script language=\"javascript\">alert(\"指定された職員IDは既に使用されています。\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

//----------ログインID重複チェック----------
$sql = "select count(*) from login";
$cond_dup_loginid_cnt = "where emp_login_id = '$id' and emp_id <> '$emp_id'";
$sel_dup_loginid_cnt = select_from_table($con, $sql, $cond_dup_loginid_cnt, $fname);
if ($sel_dup_loginid_cnt == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$dup_loginid_cnt = pg_result($sel_dup_loginid_cnt, 0, 0);
if ($dup_loginid_cnt > 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script language=\"javascript\">alert(\"指定されたログインIDは既に使用されています。\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

// メールID重複チェック
if ($site_id != "") {
	$sql = "select count(*) from login inner join authmst on login.emp_id = authmst.emp_id";
	$cond = "where login.emp_login_mail = '$mail_id' and authmst.emp_del_flg = 'f' and login.emp_id <> '$emp_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_result($sel, 0, 0) > 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('指定されたメールIDは既に使用されています。');</script>\n");
		echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
		exit;
	}
}

// 現在のログイン情報を取得
$sql = "select * from login";
$cond = "where emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$cur_login_id = pg_fetch_result($sel, 0, "emp_login_id");
$cur_login_pass = pg_fetch_result($sel, 0, "emp_login_pass");
$cur_login_mail = pg_fetch_result($sel, 0, "emp_login_mail");

//----------authmstテーブルを更新する----------
$suspend = ($suspend != "t") ? "f" : "t";

// 権限グループIDを更新
$group_id = ($group_id == "") ? null : $group_id;

$sql = "update authmst set";
if ($aprv == "t") {
	$set = array("emp_del_flg", "emp_aprv_auth", "emp_aprv_flg");
	$setvalue = array($suspend, "t", "t");
} else {
	$set = array("emp_aprv_auth");
	$setvalue = array("f");
	$set = array("emp_del_flg", "emp_aprv_auth");
	$setvalue = array($suspend, "f");
}
$cond = "where emp_id = '$emp_id'";
$up_auth = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($up_auth == 0) {
	pg_query($con, "rollback");
	pg_close($con);
//XX	mysql_close($con_mysql);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

require_once("webmail/config/config.php");
//XX require_once("community/mainfile.php");
//XX $con_mysql = mysql_connect(XOOPS_DB_HOST, XOOPS_DB_USER, XOOPS_DB_PASS);
//XX mysql_select_db(XOOPS_DB_NAME, $con_mysql);

// 利用停止がチェックされていない場合
if ($suspend != "t") {

	// メールボックス削除済みかどうか確認
	$sql = "select count(*) from maildel";
	$cond = "where emp_id = '$emp_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
//XX		mysql_close($con_mysql);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// 削除済みの場合
	if (intval(pg_fetch_result($sel, 0, 0)) > 0) {

		// メールボックスの再作成（ログインIDが数字のみでない場合）
		$account = ($site_id == "") ? $cur_login_id : "{$cur_login_mail}_{$site_id}";
		if (preg_match("/[a-z]/", $account) > 0 && $use_cyrus == "t") {
			$dir = getcwd();
			if ($imapServerAddress == "localhost" || $imapServerAddress == "127.0.0.1") {
				exec("/usr/bin/perl $dir/mboxadm/addmailbox $account $cur_login_pass 2>&1 1> /dev/null", $output, $exit_status);
				if ($exit_status !== 0) {
					exec("/usr/bin/expect -f $dir/expect/addmailbox.exp $account $cur_login_pass");
				}
			} else {
				exec("/usr/bin/expect -f $dir/expect/rsyncaddmailbox.exp $account $cur_login_pass $imapServerAddress $dir");
			}
		}

//XX		// コミュニティ権限・コミュニティ管理権限を取得
//XX		$sql = "select emp_cmt_flg, emp_cmtadm_flg from authmst";
//XX		$cond = "where emp_id = '$emp_id'";
//XX		$sel = select_from_table($con, $sql, $cond, $fname);
//XX		if ($sel == 0) {
//XX			pg_query($con, "rollback");
//XX			pg_close($con);
//XX			mysql_close($con_mysql);
//XX			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
//XX			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
//XX			exit;
//XX		}
//XX		$cmt = pg_fetch_result($sel, 0, "emp_cmt_flg");
//XX		$cmtadm = pg_fetch_result($sel, 0, "emp_cmtadm_flg");
//XX		// コミュニティ権限ありの場合
//XX		if ($cmt == "t") {
//XX
//XX			// XOOPSユーザとして登録されていたらデータを削除
//XX			$sql = "select uid from " . XOOPS_DB_PREFIX . "_users where uname = '$cur_login_id';";
//XX			if (!($sel_uid = mysql_query($sql, $con_mysql))) {
//XX				pg_query($con, "rollback");
//XX				pg_close($con);
//XX				mysql_close($con_mysql);
//XX				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
//XX				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
//XX				exit;
//XX			}
//XX			if (mysql_num_rows($sel_uid) > 0) {
//XX				$uid = mysql_result($sel_uid, 0, "uid");
//XX
//XX				$sql = "delete from " . XOOPS_DB_PREFIX . "_online where online_uid = $uid";
//XX				if (!mysql_query($sql, $con_mysql)) {
//XX					pg_query($con, "rollback");
//XX					pg_close($con);
//XX					mysql_close($con_mysql);
//XX					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
//XX					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
//XX					exit;
//XX				}
//XX
//XX				$sql = "delete from " . XOOPS_DB_PREFIX . "_groups_users_link where uid = $uid";
//XX				if (!mysql_query($sql, $con_mysql)) {
//XX					pg_query($con, "rollback");
//XX					pg_close($con);
//XX					mysql_close($con_mysql);
//XX					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
//XX					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
//XX					exit;
//XX				}
//XX
//XX				$sql = "delete from " . XOOPS_DB_PREFIX . "_users where uid = $uid";
//XX				if (!mysql_query($sql, $con_mysql)) {
//XX					pg_query($con, "rollback");
//XX					pg_close($con);
//XX					mysql_close($con_mysql);
//XX					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
//XX					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
//XX					exit;
//XX				}
//XX			}
//XX
//XX			// XOOPSユーザとして再登録
//XX			$rank = ($cmtadm == "t") ? 7 : 0;
//XX			$level = ($cmtadm == "t") ? 5 : 1;
//XX			$sql = "insert into " . XOOPS_DB_PREFIX . "_users (name, uname, email, user_regdate, user_viewemail, pass, rank, level, timezone_offset, notify_method, user_mailok) values ('$lt_nm $ft_nm', '$cur_login_id', '$cur_login_id@$domain', " . time() . ", 1, '" . md5($cur_login_pass) . "', $rank, $level, '9.0', 2, 0);";
//XX			if (!mysql_query($sql, $con_mysql)) {
//XX				pg_query($con, "rollback");
//XX				pg_close($con);
//XX				mysql_close($con_mysql);
//XX				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
//XX				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
//XX				exit;
//XX			}
//XX			$uid = mysql_insert_id($con_mysql);
//XX			$sql = "insert into " . XOOPS_DB_PREFIX . "_groups_users_link (groupid, uid) values (2, $uid)";
//XX			if (!mysql_query($sql, $con_mysql)) {
//XX				pg_query($con, "rollback");
//XX				pg_close($con);
//XX				mysql_close($con_mysql);
//XX				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
//XX				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
//XX				exit;
//XX			}
//XX			if ($cmtadm == "t") {
//XX				$sql = "insert into " . XOOPS_DB_PREFIX . "_groups_users_link (groupid, uid) values (1, $uid)";
//XX				if (!mysql_query($sql, $con_mysql)) {
//XX					pg_query($con, "rollback");
//XX					pg_close($con);
//XX					mysql_close($con_mysql);
//XX					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
//XX					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
//XX					exit;
//XX				}
//XX			}
//XX		}

		// メールボックス削除済み情報を削除
		$sql = "delete from maildel";
		$cond = "where emp_id = '$emp_id'";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
//XX			mysql_close($con_mysql);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
}

//----------loginテーブルを更新する----------
$sql = "update login set";
$set = array("emp_login_id", "emp_login_pass", "emp_login_mail");
$setvalue = array($id, $pass, $mail_id);
$cond = "where emp_id = '$emp_id'";
$up_login = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($up_login == 0) {
	pg_query($con, "rollback");
	pg_close($con);
//XX	mysql_close($con_mysql);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

$account = ($site_id == "") ? $id : "{$mail_id}_{$site_id}";
$cur_account = ($site_id == "") ? $cur_login_id : "{$cur_login_mail}_{$site_id}";

//----------optionテーブルを更新する----------
$sql = "update option set";
$set = array("webmail_quota");
$setvalue = array($webmail_quota);
$cond = "where emp_id = '$emp_id'";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
//XX	mysql_close($con_mysql);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

//---------- emp_relation ----------
if (version_compare(PHP_VERSION, '5.0.0') >= 0 && substr(pg_parameter_status($con, 'server_version'), 0, 1) >= 8) {
    $emp_name_deadlink_flg = $_POST['emp_name_deadlink_flg'] === 't' ? 't' : 'f';
    $emp_class_deadlink_flg = $_POST['emp_class_deadlink_flg'] === 't' ? 't' : 'f';
    $emp_job_deadlink_flg = $_POST['emp_job_deadlink_flg'] === 't' ? 't' : 'f';
    $emp_st_deadlink_flg = $_POST['emp_st_deadlink_flg'] === 't' ? 't' : 'f';
    $sql_rel = "SELECT update_emp_relation(";
    $content_rel = array($emp_id, $emp_name_deadlink_flg, $emp_class_deadlink_flg, $emp_job_deadlink_flg, $emp_st_deadlink_flg);
    $in_rel = insert_into_table($con, $sql_rel, $content_rel, $fname);
    if ($in_rel == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}

// 勤務シフト作成のリアルタイム同期対応
$sql = "select sync_flag from duty_shift_option";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$sync_flag = pg_fetch_result($sel, 0, "sync_flag");
if ($sync_flag == "t") {

	// 現在のシフトグループ情報
	$sql = "select s.group_id, g.pattern_id from duty_shift_staff s inner join duty_shift_group g on s.group_id = g.group_id";
	$cond = "where s.emp_id = '$emp_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
//XX		mysql_close($con_mysql);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$cur_group_id = pg_fetch_result($sel, 0, "group_id");
	$cur_pattern_id = pg_fetch_result($sel, 0, "pattern_id");

	// 新しいシフトグループ情報
	$sql = "select l.group_id, g.pattern_id from duty_shift_sync_link l inner join duty_shift_group g on l.group_id = g.group_id";
	$cond = "where l.class_id = $cls and l.atrb_id = $atrb and l.dept_id = $dept and l.room_id ";
	$cond .= ($room != "") ? " = $room" : " is null";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
//XX		mysql_close($con_mysql);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$new_group_id = pg_fetch_result($sel, 0, "group_id");
	$new_pattern_id = pg_fetch_result($sel, 0, "pattern_id");

	// シフトグループが変わる場合（シフトグループからの削除は考慮しない）
	if ($new_group_id != "" && $new_group_id != $cur_group_id) {

		// 別のグループに移動
		$sql = "delete from duty_shift_staff";
		$cond = "where emp_id = '$emp_id'";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
        //履歴作成
        if ($cur_group_id != "") {
            $sql = "insert into duty_shift_staff_history (emp_id, group_id, histdate) values(";
            $content = array($emp_id, $cur_group_id, date('Y-m-d H:i:s'));
            $ins = insert_into_table($con, $sql, $content, $fname);
            if ($ins == 0) {
                pg_query($con,"rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
        }

		$sql = "select count(*) from duty_shift_staff";
		$cond = "where group_id = '$new_group_id'";
		$sel_count = select_from_table($con, $sql, $cond, $fname);
		if ($sel_count == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$tmp_no = intval(pg_fetch_result($sel_count, 0, 0)) + 1;
		$sql = "insert into duty_shift_staff (group_id, emp_id, no) values (";
		$content = array($new_group_id, $emp_id, $tmp_no);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 勤務パターングループが変わらない場合、勤務条件をコピー
		if ($cur_pattern_id == $new_pattern_id) {
			$sql = "update duty_shift_staff_employment set";
			$set = array("group_id");
			$setvalue = array($new_group_id);
			$cond = "where group_id = '$cur_group_id' and emp_id = '$emp_id'";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			$sql = "update duty_shift_staff_employment_atdptn set";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}

		// 勤務パターングループが変わる場合、勤務条件を削除
		} else {
			$sql = "delete from duty_shift_staff_employment";
			$cond = "where emp_id = '$emp_id'";
			$del = delete_from_table($con, $sql, $cond, $fname);
			if ($del == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			$sql = "delete from duty_shift_staff_employment_atdptn";
			$del = delete_from_table($con, $sql, $cond, $fname);
			if ($del == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}
}

//----------empmstテーブルに挿入する----------
$set = array("emp_personal_id", "emp_class", "emp_attribute", "emp_dept", "emp_room", "emp_job", "emp_st", "emp_lt_nm", "emp_ft_nm", "emp_kn_lt_nm", "emp_kn_ft_nm", "emp_sex", "emp_birth", "emp_join", "emp_keywd", "emp_retire", "emp_idm", "emp_profile");
$setvalue = array($personal_id, $cls, $atrb, $dept, $room, $job, $status, $lt_nm, $ft_nm, $lt_kana_nm, $ft_kana_nm, $sex, $birth, $entry, $keywd, $retire, $idm, $profile);
$cond = "where emp_id = '$emp_id'";
$up_emp = update_set_table($con, "update empmst set", $set, $setvalue, $cond, $fname);
if ($up_emp == 0) {
	pg_query($con, "rollback");
	pg_close($con);
//XX	mysql_close($con_mysql);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 権限グループの更新
//XX auth_change_group_of_employee($con, $con_mysql, $emp_id, $group_id, $fname);
auth_change_group_of_employee($con, $emp_id, $group_id, $fname);

// 権限整合性・ライセンスのチェック
//XX auth_check_consistency($con, $con_mysql, $session, $fname, false);
auth_check_consistency($con, $session, $fname, false);

// 兼務所属レコードを全削除
$sql = "delete from concurrent";
$cond = "where emp_id = '$emp_id'";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 兼務所属レコードを作成
foreach ($concurrents as $tmp_concurrent) {
	$sql = "insert into concurrent (emp_id, emp_class, emp_attribute, emp_dept, emp_room, emp_st) values (";
	$content = array($emp_id, $tmp_concurrent["class"], $tmp_concurrent["atrb"], $tmp_concurrent["dept"], $tmp_concurrent["room"], $tmp_concurrent["status"]);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

//XX// XOOPSアカウントの更新
//XX $sql = "update " . XOOPS_DB_PREFIX . "_users set name = '$lt_nm $ft_nm', uname = '$id', email = '$id@$domain', pass = '" . md5($pass) . "' where uname = '$cur_login_id'";
//XX if (!mysql_query($sql, $con_mysql)) {
//XX	pg_query($con, "rollback");
//XX	pg_close($con);
//XX	mysql_close($con_mysql);
//XX	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
//XX	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
//XX	exit;
//XX }

// コミット処理
pg_query($con, "commit");
pg_close($con);
//XX mysql_close($con_mysql);

if ($use_cyrus == "t") {

	// ログインIDかパスワードが変わった場合、メールアカウント情報を更新
	if ($account != $cur_account || $pass !== $cur_login_pass) {
		$dir = getcwd();

		// ログインIDにアルファベットなし→アルファベットありの場合、メールアカウントを作成
		if (preg_match("/[a-z]/", $cur_account) == 0 && preg_match("/[a-z]/", $account) > 0) {
			if ($imapServerAddress == "localhost" || $imapServerAddress == "127.0.0.1") {
				exec("/usr/bin/perl $dir/mboxadm/addmailbox $account $pass 2>&1 1> /dev/null", $output, $exit_status);
				if ($exit_status !== 0) {
					exec("/usr/bin/expect -f $dir/expect/addmailbox.exp $account $pass");
				}
			} else {
				exec("/usr/bin/expect -f $dir/expect/rsyncaddmailbox.exp $account $pass $imapServerAddress $dir");
			}

		// ログインIDがアルファベットあり→アルファベットありの場合、メールアカウントを更新
		} else if (preg_match("/[a-z]/", $cur_account) > 0 && preg_match("/[a-z]/", $account) > 0) {
			if ($imapServerAddress == "localhost" || $imapServerAddress == "127.0.0.1") {
				exec("/usr/bin/perl $dir/mboxadm/updmailbox $cur_account $account $pass 2>&1 1> /dev/null", $output, $exit_status);
				if ($exit_status !== 0) {
					exec("/usr/bin/expect -f $dir/expect/updmailbox.exp $cur_account $account $pass");
				}
			} else {
				exec("/usr/bin/expect -f $dir/expect/rsyncupdmailbox.exp $cur_account $account $pass $imapServerAddress $dir");
			}

		// ログインIDがアルファベットあり→アルファベットなしの場合、メールアカウントを削除
		} else if (preg_match("/[a-z]/", $cur_account) > 0 && preg_match("/[a-z]/", $account) == 0) {
			if ($imapServerAddress == "localhost" || $imapServerAddress == "127.0.0.1") {
				exec("/usr/bin/perl $dir/mboxadm/delmailbox $cur_account 2>&1 1> /dev/null", $output, $exit_status);
				if ($exit_status !== 0) {
					exec("/usr/bin/expect -f $dir/expect/delmailbox.exp $cur_account");
				}
			} else {
				exec("/usr/bin/expect -f $dir/expect/rsyncdelmailbox.exp $cur_account $imapServerAddress $dir");
			}
		}
	}
}

// ウェブメール容量制限
if ($webmail_actual_quota > 0) {
	webmail_quota_change($account, $webmail_actual_quota);
}

// 写真関連処理
if ($updatephoto == "t" && $_FILES["photofile"]["error"] == 0) {
	if (!is_dir("profile")) {
		mkdir("profile", 0755);
	}
	copy($_FILES["photofile"]["tmp_name"], "profile/{$emp_id}.jpg");
} else if ($clearphoto == "t") {
	unlink("profile/{$emp_id}.jpg");
}

$url_key1 = urlencode($key1);
$url_key2 = urlencode($key2);
echo("<script type=\"text/javascript\">location.href = 'employee_detail.php?session=$session&emp_id=$emp_id&key1=$url_key1&key2=$url_key2&class_cond=$class_cond&atrb_cond=$atrb_cond&dept_cond=$dept_cond&room_cond=$room_cond&job_cond=$job_cond&page=$page&view=$view&emp_o=$emp_o';</script>");
?>
</body>
