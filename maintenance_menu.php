<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix マスターメンテナンス | マスターの概要</title>
<?
require_once("Cmx.php");
require_once("aclg_set.php");

require("about_session.php");
require("about_authority.php");
require("maintenance_menu_list.ini");
require("show_class_name.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$checkauth = check_authority($session, 16, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

$authority = get_authority($session, $fname);

// データベースに接続
$con = connect2db($fname);
// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

$arr_class_name = get_class_name_array($con, $fname);

// イントラメニュー名を取得
$sql = "select * from intramenu";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$intra_menu1 = pg_fetch_result($sel, 0, "menu1");
$intra_menu2_4 = pg_fetch_result($sel, 0, "menu2_4");
$intra_menu3 = pg_fetch_result($sel, 0, "menu3");
?>
<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="maintenance_menu.php?session=<? echo($session); ?>"><img src="img/icon/b27.gif" width="32" height="32" border="0" alt="マスターメンテナンス"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="./maintenance_menu.php?session=<? echo($session); ?>"><b>マスターメンテナンス</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- left -->
<td width="160" valign="top">
<? write_menu_list($session, $fname) ?>
</td>
<!-- left -->
<!-- center -->
<td width="5"><img src="./img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->
<!-- right -->
<td valign="top">
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="border3">
<tr>
<td height="22" colspan="2" bgcolor="#bdd1e7" class="spacing"><a href="./maintenance_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>各マスターの概要</b></font></a></td>
</tr>
<? if ($authority[37] == "t") { ?>
<tr valign="top">
<td width="160" height="22"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">組織プロフィール</font></td>
<td width=""><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病院様のプロフィールとして、医療機関名称、住所、病床数、保険医療機関コード、病院ロゴ画像、ホームページアドレス等の登録を行います。病院ロゴ画像は、表示タイプ設定をすることで、トップ画面の右上に画像表示することができます。</font></td>
</tr>
<? } ?>
<? if ($authority[23] == "t") { ?>
<tr valign="top">
<td width="160" height="22"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">組織</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">3階層または4階層で組織を登録できます。組織名称だけを登録してください。職員登録時に所属属性として利用します。</font></td>
</tr>
<? } ?>
<? if ($authority[26] == "t") { ?>
<tr valign="top">
<td width="160" height="22"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職種</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">医師、看護師、薬剤師などの職種を登録してください。この職種は、職員登録時の属性として使用されます。職種によって利用できる機能についての権限コントロールをします。</font></td>
</tr>
<? } ?>
<? if ($authority[27] == "t") { ?>
<tr valign="top">
<td width="160" height="22"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">役職</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">院長、副院長、事務長、看護師長などの役職を登録してください。この役職は、職員登録時の属性として使用されます。</font></td>
</tr>
<? } ?>
<? if ($authority[17] == "t") { ?>
<tr valign="top">
<td width="160" height="22"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カレンダー</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">通常勤務日の勤務時間、法定休日や病院の独自の休日設定などのカレンダー設定を行います。</font></td>
</tr>
<? } ?>
</table>
</td>
<!-- right -->
</tr>
</table>
</body>
</html>
