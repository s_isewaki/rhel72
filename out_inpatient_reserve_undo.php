<?
require_once("about_authority.php");
require_once("about_session.php");
require_once("inpatient_common.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$check_auth = check_authority($session, 14, $fname);
if ($check_auth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションの開始
pg_query($con, "begin");

// 退院予定日を取得
$sql = "select inpt_out_res_dt from inptmst";
$cond = "where ptif_id = '$ptif_id'";
$sel = select_from_table($con, $sql ,$cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$out_res_dt = pg_fetch_result($sel, 0, "inpt_out_res_dt");

// 「在院」に戻す
update_inpatient_condition($con, $ptif_id, $out_res_dt, null, "8", $session, $fname);

// トランザクションのコミット
//pg_query($con, "rollback");
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 患者一覧画面を再表示
switch ($list_type) {
case "3":
	echo("<script type=\"text/javascript\">location.href = 'out_inpatient_reserve_list.php?session=$session&bldgwd=$bldgwd&list_sect_id=$list_sect_id&order=$order&page=$page';</script>");
	break;
case "A":
	echo("<script type=\"text/javascript\">location.href = 'inpatient_date_list.php?session=$session&bldgwd=$bldgwd&list_sect_id=$list_sect_id&list_year=$list_year&list_month=$list_month&list_day=$list_day&list_year2=$list_year2&list_month2=$list_month2&list_day2=$list_day2&order=$order&page=$page';</script>");
	break;
}
?>
