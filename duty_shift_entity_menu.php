<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務シフト作成 | 管理 |シフトグループ一覧</title>

<?
///ini_set("display_errors","1");
require_once("about_authority.php");
require_once("about_session.php");
require_once("about_postgres.php");

require_once("duty_shift_common.ini");
require_once("duty_shift_manage_tab_common.ini");
require_once("duty_shift_common_class.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
//セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
///-----------------------------------------------------------------------------
// 権限のチェック
///-----------------------------------------------------------------------------
//管理画面用
$chk_flg = $obj->check_authority_Management($session, $fname);
if ($chk_flg == "") {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
//ユーザ画面用
$section_admin_auth = $obj->check_authority_user($session, $fname);

// 順番変更処理
if ($action == "up" || $action == "down") {
	pg_query($con, "begin");

	// 選択されたグループの表示順を取得
	$sql = "select order_no from duty_shift_group";
	$cond = "where group_id = '$group_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$order_no = pg_fetch_result($sel, 0, "order_no");

	// 隣のグループを上げ下げ
	$diff = ($action == "up") ? -1 : 1;
	$sql = "update duty_shift_group set";
	$set = array("order_no");
	$setvalue = array($order_no);
	$cond = "where order_no = " . ($order_no + $diff);
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// 選択グループを上げ下げ
	$sql = "update duty_shift_group set";
	$set = array("order_no");
	$setvalue = array($order_no + $diff);
	$cond = "where group_id = '$group_id'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	pg_query($con, "commit");
}

///-----------------------------------------------------------------------------
//初期値設定
///-----------------------------------------------------------------------------
	///-----------------------------------------------------------------------------
	//ＤＢ(stmst)より役職情報取得
	//ＤＢ(jobmst)より職種情報取得
	//ＤＢ(empmst)より職員情報を取得
	//ＤＢ(wktmgrp)より勤務パターン情報を取得
	///-----------------------------------------------------------------------------
	$data_st = $obj->get_stmst_array();
	$data_job = $obj->get_jobmst_array();
	$data_emp = $obj->get_empmst_array("");
	$data_wktmgrp = $obj->get_wktmgrp_array();
	///-----------------------------------------------------------------------------
	//勤務シフトグループ情報を取得
	///-----------------------------------------------------------------------------
	$data_array = $obj->get_duty_shift_group_array("", $data_emp, $data_wktmgrp);
	///-----------------------------------------------------------------------------
	//グループ毎のスタッフ情報を取得
	///-----------------------------------------------------------------------------
	for ($i=0; $i<count($data_array); $i++) {
		$staff_array = $obj->get_duty_shift_staff_array($data_array[$i]["group_id"], $data_st, $data_job, $data_emp);
		$data_array[$i]["staff_cnt"] = count($staff_array);
	}

	// 表示順用
	$line_count = count($data_array);

	$groups = array();
	$actual_order_no = 1;
	for ($i=0; $i<$line_count; $i++) {
		$groups[] = array(
			"id" => $data_array[$i]["group_id"],
			"name" => $data_array[$i]["group_name"],
			"actual_order_no" => $actual_order_no,
			"order_no" => $data_array[$i]["order_no"]
		);
		$actual_order_no++;
	}
	// トランザクションを開始
	pg_query($con, "begin");

	// 並び順の再設定
	foreach ($groups as $tmp_status) {
	// order_noが未設定の場合に再設定？
		if ($tmp_status["order_no"] != "") {
			continue;
		}

		$sql = "update duty_shift_group set";
		$set = array("order_no");
		$setvalue = array($tmp_status["actual_order_no"]);
		$cond = "where group_id = '{$tmp_status["id"]}'";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// トランザクションをコミット
	pg_query($con, "commit");
?>

<!-- ************************************************************************ -->
<!-- JavaScript -->
<!-- ************************************************************************ -->
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
	///-----------------------------------------------------------------------------
	//削除
	///-----------------------------------------------------------------------------
	function dataDelete(id, staff_cnt) {
		//指定グループに職員が存在するかチェック
		if (staff_cnt > 0) {
			alert('このグループには職員設定がされているため削除できません。職員削除後にもう一度処理を行ってください。');
		} else {
			//削除
			if (confirm('シフトグループ(病棟)を削除します。よろしいですか？')) {
				document.dutyshift.action="duty_shift_entity_group_delete_exe.php?session=<?=$session?>&group_id=" + id;
				document.dutyshift.submit();
			}
		}
	}

</script>

<!-- ************************************************************************ -->
<!-- HTML -->
<!-- ************************************************************************ -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
img {border-style:none;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
	<!-- ------------------------------------------------------------------------ -->
	<!-- 画面遷移／タブ -->
	<!-- ------------------------------------------------------------------------ -->
	<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
	<?
		// 画面遷移
		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
		show_manage_title($session, $section_admin_auth);			//duty_shift_common.ini
		echo("</table>\n");

		// タブ
		$arr_option = "";
		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
		show_manage_tab_menuitem($session, $fname, $arr_option);	//duty_shift_manage_tab_common.ini
		echo("</table>\n");

		// 下線
		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td bgcolor=\"#5279a5\"><img src=\"img/spacer.gif\" width\"1\" height=\"2\" alt=\"\"></td></tr></table><img src=\"img/spacer.gif\" width=\"1\" height=\"5\" alt=\"\"><br>\n");
	?>
	</td></tr></table>
	<!-- ------------------------------------------------------------------------ -->
	<!-- 一覧 -->
	<!-- ------------------------------------------------------------------------ -->
	<form name="dutyshift" method="post">
		<!-- ------------------------------------------------------------------------ -->
		<!-- 一覧（見出し） -->
		<!-- ------------------------------------------------------------------------ -->
		<table width="900" border="0" cellspacing="0" cellpadding="2" class="list">
		<tr height="22" bgcolor="#f6f9ff">
			<td width="5%" align="center" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示順</font></td>
			<? /*
			<td width="5%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">&nbsp;</font></td>
			*/ ?>
			<td width="25%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">シフトグループ(病棟)名</font></td>
			<td width="9%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">施設基準</font></td>
			<td width="12%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">シフト管理者<br>(責任者)</font></td>
			<td width="27%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">シフト管理者<br>(代行者)</font></td>
			<td width="16%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務パターングループ名</font></td>
			<td width="5%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">&nbsp;</font></td>
		</tr>
		<!-- ------------------------------------------------------------------------ -->
		<!-- 一覧（データ） -->
		<!-- ------------------------------------------------------------------------ -->
		<?
			for ($i=0; $i<count($data_array); $i++) {
				$wk_id = $data_array[$i]["group_id"];
				$wk1 = $data_array[$i]["group_name"];
				if ($data_array[$i]["standard_id"] == "") {
					$wk2 = "適用なし";
				} else {
					$wk2 = "施設基準" . $data_array[$i]["standard_id"];
				}
				$wk3 = $data_array[$i]["person_charge_name"];
				$wk4 = $data_array[$i]["caretaker_name"];
				for ($j=2; $j<=10; $j++) {
					$varname = "caretaker".$j."_name";
					if ($wk4 != "" && $data_array[$i]["$varname"] != "") {
						$wk4 .= "、";
					}
					$wk4 .= $data_array[$i]["$varname"];
				}
				$wk5 = $data_array[$i]["pattern_name"];
				// 一覧
				echo("<tr height=\"22\">\n");
				// 表示順
				echo("<td width=\"5%\" align=\"center\" nowrap>");
if($i != 0)
{
?>
	<a href="duty_shift_entity_menu.php?session=<?=$session?>&action=up&group_id=<?=$wk_id?>"><img src="img/up.gif" style="cursor: pointer;" alt=""></a>
<?
} else {
	echo("<img src=\"img/up-g.gif\" alt=\"\">");
}
?>
<?
if($i != $line_count-1)
{
?>
	<a href="duty_shift_entity_menu.php?session=<?=$session?>&action=down&group_id=<?=$wk_id?>"><img src="img/down.gif" style="cursor: pointer;" alt=""></a>
<?
} else {
	echo("<img src=\"img/down-g.gif\" alt=\"\"");
}
				echo("</td>\n");
//				echo("<td width=\"5%\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
//				echo("<a href=\"duty_shift_entity_group.php?session=$session&group_id=$wk_id\">編集</a>");
//				echo("</font></td>\n");

				if ($wk1 == "") { $wk1 = "＿"; } // "　"->"＿"
				if ($wk2 == "") { $wk2 = "　"; }
				if ($wk3 == "") { $wk3 = "　"; }
				if ($wk4 == "") { $wk4 = "　"; }
				if ($wk5 == "") { $wk5 = "　"; }
				echo("<td width=\"25%\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"duty_shift_entity_group.php?session=$session&group_id=$wk_id\">$wk1</a></font></td>\n");
				echo("<td width=\"9%\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk2</font></td>\n");
				echo("<td width=\"12%\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk3</font></td>\n");
				echo("<td width=\"27%\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk4</font></td>\n");
				echo("<td width=\"16%\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk5</font></td>\n");
				echo("<td width=\"5%\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">\n");
				$staff_cnt = $data_array[$i]["staff_cnt"];
				echo("<input type=\"button\" value=\"削除\" onclick=\"dataDelete('$wk_id',$staff_cnt);\">\n");
				echo("</font></td>\n");
				echo("</tr>\n");
			}
		?>
		</table>
		<input type="hidden" name="session" value="<? echo($session); ?>">
		<input type="hidden" name="is_postback" value="true">
		<input type="hidden" name="postback_mode" value="">
		<input type="hidden" name="change_line_no_after_line_no" value="">
		<input type="hidden" name="change_line_no_target_line_no" value="">
	</form>

</td>
</tr>
</table>
</body>

<? pg_close($con); ?>

</html>
