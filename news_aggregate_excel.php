<?php

ob_start();

require_once("Cmx.php");
require_once("about_comedix.php");
require_once("show_select_values.ini");
require_once("referer_common.ini");
require_once("get_values.ini");
require_once("show_class_name.ini");

$fname = $PHP_SELF;

// セッションのチェック

$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

//====================================
//権限チェック
//====================================
$wkfw = check_authority($session, 24, $fname);
if ($wkfw == "0") {
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}
// ワークフロー権限の取得

$con = connect2db($fname);

$stamp = time();
$date = date('YmdHis', $stamp);

$fileName = "お知らせ・回覧板集計結果_" . $date . ".xls";

if (preg_match('/MSIE/', $_SERVER["HTTP_USER_AGENT"])) { // MSIE
    $fileName = to_sjis($fileName);
}
else if (preg_match('/Firefox/', $_SERVER["HTTP_USER_AGENT"])) { // ff
    $fileName = to_utf8($fileName);
}
else {
    $fileName = rawurlencode(to_utf8($fileName));
}

ob_clean();
header("Content-Type: application/vnd.ms-excel");
header(sprintf('Content-Disposition: attachment; filename=%s', $fileName));
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0,pre-check=0');
header('Pragma: public');

$download_data = setData($fname, $con, $mrsa_sel_d1_y, $sel_vert_field_idx, $data_1_13_find_germ);

echo mb_convert_encoding(
    nl2br($download_data), 'sjis', mb_internal_encoding()
);

ob_end_flush();

pg_close($con);

function setData
($fname, $con, $sel_d1_y, $sel_vert_field_idx, $data_1_13_find_germ) {

    $arr_class_name = get_class_name_array($con, $fname);

    //エクセル出力用変数
    $dispSetdata = "";

    //出力用構造体
    $disp_data = array();

    $vert_field_ab_definision =
        $gokei_line = array();


    // 指定された日付
    //$sel_d1_y = (int)@$_REQUEST["sel_d1_y"];
    $www = $sel_d1_y;


    // 指定された縦軸項目のインデックス
    //$sel_vert_field_idx = @$_REQUEST["sel_vert_field_idx"]; // 選択中の縦軸項目インデックス番号(0〜)
    $idx = $sel_vert_field_idx; // 選択中の縦軸項目インデックス番号(0〜)

    if ($sel_vert_field_idx == "data_1_13_find_germ") {
        //縦軸の条件が"検出菌/微生物名"の場合は絞込み条件の"検出菌/微生物名"を検索条件にしない
        $germ_cond = "";

        //ヘッダにも表示しない
        $str_idx = "";
    }
    else {
        $germ_cond = "and data_1_13_find_germ = '$data_1_13_find_germ' ";
    }


    $data = "";
    // metaタグで文字コード設定必須（ない場合にはMacで文字化けするため）
    $data .= "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Shift_JIS\">";

    $data .= "<table border=1 >";

    $data .= "<tr align=\"center\">";
    $data .= "<td align=\"left\" colspan=\"14\" ><font size=\"4\">" . $str_disp . "</font></td>";
    $data .= "</tr>";

    $ag_end_year = $_REQUEST["ag_start_year"] + 1;
    if ($_REQUEST["ag_start_month"] == 1) {
        $ag_end_year = $ag_end_year - 1;
        $ag_end_month = 12;
    }
    else if ($_REQUEST["ag_start_month"] < 10) {
        $ag_end_month = "0" . ((int) $_REQUEST["ag_start_month"] - 1);
    }
    else {
        $ag_end_month = ((int) $_REQUEST["ag_start_month"] - 1);
    }

    //組織の各階層の名前を取得
    $sql = "select class_nm, atrb_nm, dept_nm, room_nm, class_cnt from classname";
    $cond = "";
    $sel = select_from_table($con, $sql, $cond, $fname);

    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    //マスターメンテナンスで登録した各階層の名称を取得
    $class_nm = pg_fetch_result($sel, 0, "class_nm");
    $atrb_nm = pg_fetch_result($sel, 0, "atrb_nm");
    $dept_nm = pg_fetch_result($sel, 0, "dept_nm");
    $class_cnt = pg_fetch_result($sel, 0, "class_cnt");
    if ($class_cnt == 4) {
        $room_nm = pg_fetch_result($sel, 0, "room_nm");
    }

    //20120326 中嶌慶
    //何のExcel出力か識別するための項目
    //ag_typeが0のときは施設別の集計結果が出力される。1のときは発信者別の集計結果が出力される。
    if ($_REQUEST["ag_type"] == 0) {
        switch ($_REQUEST["ag_facility_level"]) {
            case 1:
                $serch_nm = $class_nm;
                break;
            case 2:
                $serch_nm = $atrb_nm;
                break;
            case 3:
                $serch_nm = $dept_nm;
                break;
            case 4:
                $serch_nm = $room_nm;
                break;
        }
        $str_disp = "期間：" . $_REQUEST["ag_start_year"] . "年" . (int) $_REQUEST["ag_start_month"] . "月 〜 " . $ag_end_year . "年" . $ag_end_month . "月  　　施設別：" . $serch_nm;
    }
    else {
        $str_disp = "期間：" . $_REQUEST["ag_start_year"] . "年" . (int) $_REQUEST["ag_start_month"] . "月 〜 " . $ag_end_year . "年" . $ag_end_month . "月  　　発信者別";
    }


    $data = "";
    // metaタグで文字コード設定必須（ない場合にはMacで文字化けするため）
    $data .= "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Shift_JIS\">";

    $data .= "<table border=1 >";

    $data .= "<tr align=\"center\">";
    $data .= "<td align=\"left\" colspan=\"14\" ><font size=\"4\">" . $str_disp . "</font></td>";
    $data .= "</tr>";


    //ヘッダ行表示
    $serch_month = (int) $_REQUEST["ag_start_month"];
    $data .= "<tr align=\"center\">";
    if ($_REQUEST["ag_type"] == "1") {
        $data .= "<td bgcolor=\"#bdd1e7\" ><font size=\"4\">職員名</font></td>";
    }
    $data .= "<td bgcolor=\"#bdd1e7\" ><font size=\"4\">組織</font></td>";
    for ($i = 0; $i < 12; $i++) {
        if ($serch_month > 12) {
            $serch_month = 1;
        }
        $data .= "<td bgcolor=\"#bdd1e7\" ><font size=\"4\">" . $serch_month . "月</font></td>";
        $serch_month++;
    }
    $data .= "<td bgcolor=\"#bdd1e7\" ><font size=\"4\">合計</font></td>";
    $data .= "</tr>";

    //20120326 中嶌慶
    //集計の範囲を設定する。
    $ag_start_year = $_REQUEST["ag_start_year"];
    $ag_start_month = $_REQUEST["ag_start_month"];
    $start_year = $ag_start_year . $ag_start_month;
    if ($ag_start_month == 1) {
        $set_max_ym = $ag_start_year . "1201";
    }
    else {
        $set_max_ym = ($ag_start_year + 1) . ($ag_start_month - 1) . "01";
    }

    $ag_facility_level = $_REQUEST["ag_facility_level"];

    //20120326 中嶌慶
    //設定したでデータベースから抽出する
    //最初のif文で施設別か発信者別か区別する。施設別:0  発信者別:1
    //switch文でどの範囲を抽出するか識別する。$ag_facility_level=1:第一階層。2:第二階層。3:第三階層。4:第四階層
    if ($_REQUEST["ag_type"] == "0") {
        switch ($ag_facility_level) {
            case 1:
                $sql = "select * from classmst";
                $cond = "where class_del_flg = 'f' order by order_no";
                $sel = select_from_table($con, $sql, $cond, $fname);
                if ($sel == 0) {
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }
                $start_year = $ag_start_year . $ag_start_month . "01";
                if ($ag_start_month == 1) {
                    $set_max_ym = $ag_start_year . "1201";
                }
                else if ($ag_start_month < 10) {
                    $set_max_ym = ($ag_start_year + 1) . "0" . ($ag_start_month - 1) . "01";
                }
                else {
                    $set_max_ym = ($ag_start_year + 1) . ($ag_start_month - 1) . "01";
                }
                $sql2 = "select * from classmst left join
						(select count_data,  src_class_id, classmst.class_nm, ag_month from
						(select count(*) as count_data, src_class_id, ag_month  from
						(select src_class_id, news_date, substr(news_date, 5, 2) as ag_month from news where news_del_flg = 'f' ) wnews ";
                $cond2 = "where news_date between '" . $start_year . "' and '" . $set_max_ym . "'
						group by src_class_id,  ag_month  order by src_class_id
						) tbl_data
						left join classmst on tbl_data.src_class_id = classmst.class_id
						order by src_class_id, ag_month
						) wdept on classmst.class_id = wdept.src_class_id
						where class_del_flg = 'f'
						order by classmst.order_no";
                $sel2 = select_from_table($con, $sql2, $cond2, $fname);


                $wk_lp_cnt = 0;
                $wd_id = "";
                $arr_set = array();
                while ($row = pg_fetch_array($sel2)) {
                    //全体をループ
                    $arr_set[$row["class_id"]][$row["ag_month"]] = $row["count_data"];
                }

                //配列の中に月の値を入力
                $dis_month[0] = "";
                $dis_month[1] = "01";
                $dis_month[2] = "02";
                $dis_month[3] = "03";
                $dis_month[4] = "04";
                $dis_month[5] = "05";
                $dis_month[6] = "06";
                $dis_month[7] = "07";
                $dis_month[8] = "08";
                $dis_month[9] = "09";
                $dis_month[10] = "10";
                $dis_month[11] = "11";
                $dis_month[12] = "12";


                while ($row = pg_fetch_array($sel)) {
                    //施設分ループ
                    //施設名を表示
                    //月数ぶんループ
                    //$arr_set[$row[ループの施設ID]][ループの月数] = $row["count_data"];

                    $data .= "<tr align=\"center\">";
                    $facility_nm = $row["class_nm"];
                    $facility_id = $row["class_id"];
                    $data .= ("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$facility_nm</font></td>\n");
                    $serch_month = (int) $ag_start_month;
                    $all_count = 0;
                    for ($i = 1; $i < 13; $i++) {
                        if ($serch_month > 12) {
                            $serch_month = 1;
                        }

                        //20120326 中嶌慶
                        //$dis_month[$serch_month]月の中に入っている数字を$countに抽出する。
                        //$arr_set[$facility_id][$dis_month[$serch_month]]で各月のお知らせ数を取ることができる。
                        $count = $arr_set[$facility_id][$dis_month[$serch_month]];
                        if ($count != NULL) {
                            $data .= ("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . $arr_set[$facility_id][$dis_month[$serch_month]] . "</font></td>\n");
                        }
                        else {
                            $data .= ("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">0</font></td>\n");
                        }
                        $all_count += $arr_set[$facility_id][$dis_month[$serch_month]];
                        $serch_month++;
                    }

                    //月数ぶんループ終了
                    //施設分ループ終了
                    //合計数表示
                    $data .= ("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$all_count</font></td>\n");

                    $data .= "</tr>";
                }

                break;

            case 2:
                $sql = "select a.*, c.class_nm from atrbmst a left join classmst c on a.class_id = c.class_id";
                $cond = "where a.atrb_del_flg ='f' order by c.order_no,a.order_no";
                $sel = select_from_table($con, $sql, $cond, $fname);
                if ($sel == 0) {
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }
                $start_year = $ag_start_year . $ag_start_month . "01";
                if ($ag_start_month == 1) {
                    $set_max_ym = $ag_start_year . "1201";
                }
                else if ($ag_start_month < 10) {
                    $set_max_ym = ($ag_start_year + 1) . "0" . ($ag_start_month - 1) . "01";
                }
                else {
                    $set_max_ym = ($ag_start_year + 1) . ($ag_start_month - 1) . "01";
                }
                $sql2 = "select * from classmst,atrbmst left join
						(select count_data,  src_atrb_id, atrbmst.atrb_nm, ag_month from
						(select count(*) as count_data, src_atrb_id, ag_month  from
						(select src_atrb_id, news_date, substr(news_date, 5, 2) as ag_month from news where news_del_flg = 'f' ) wnews ";
                $cond2 = "where news_date between '" . $start_year . "' and '" . $set_max_ym . "'
						group by src_atrb_id,  ag_month  order by src_atrb_id
						) tbl_data
						left join atrbmst on tbl_data.src_atrb_id = atrbmst.class_id
						order by src_atrb_id, ag_month
						) wdept on atrbmst.atrb_id = wdept.src_atrb_id
						where atrb_del_flg = 'f' and class_del_flg = 'f' and classmst.class_id = atrbmst.class_id
						order by classmst.order_no,atrbmst.order_no";
                $sel2 = select_from_table($con, $sql2, $cond2, $fname);


                $wk_lp_cnt = 0;
                $wd_id = "";
                $arr_set = array();
                while ($row = pg_fetch_array($sel2)) {
                    //全体をループ
                    $arr_set[$row["atrb_id"]][$row["ag_month"]] = $row["count_data"];
                }

                $dis_month[0] = "";
                $dis_month[1] = "01";
                $dis_month[2] = "02";
                $dis_month[3] = "03";
                $dis_month[4] = "04";
                $dis_month[5] = "05";
                $dis_month[6] = "06";
                $dis_month[7] = "07";
                $dis_month[8] = "08";
                $dis_month[9] = "09";
                $dis_month[10] = "10";
                $dis_month[11] = "11";
                $dis_month[12] = "12";


                while ($row = pg_fetch_array($sel)) {
                    //施設分ループ
                    //施設名を表示
                    //月数ぶんループ
                    //$arr_set[$row[ループの施設ID]][ループの月数] = $row["count_data"];

                    $data .= "<tr align=\"center\">";
                    $facility_nm = $row["class_nm"] . "＞" . $row["atrb_nm"];
                    $facility_id = $row["atrb_id"];
                    $data .= ("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$facility_nm</font></td>\n");
                    $serch_month = (int) $ag_start_month;
                    $all_count = 0;
                    for ($i = 1; $i < 13; $i++) {
                        if ($serch_month > 12) {
                            $serch_month = 1;
                        }

                        $count = $arr_set[$facility_id][$dis_month[$serch_month]];
                        if ($count != NULL) {
                            $data .= ("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . $arr_set[$facility_id][$dis_month[$serch_month]] . "</font></td>\n");
                        }
                        else {
                            $data .= ("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">0</font></td>\n");
                        }
                        $all_count += $arr_set[$facility_id][$dis_month[$serch_month]];
                        $serch_month++;
                    }

                    //月数ぶんループ終了
                    //施設分ループ終了
                    //合計数表示
                    $data .= ("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$all_count</font></td>\n");

                    $data .= ("</tr>\n");
                }

                break;

            case 3:
                $sql = "select d.*, a.atrb_nm, c.class_nm from deptmst d left join atrbmst a on d.atrb_id = a.atrb_id left join classmst c on a.class_id = c.class_id";
                $cond = "where d.dept_del_flg = 'f' order by c.order_no,a.order_no,d.order_no";
                $sel = select_from_table($con, $sql, $cond, $fname);
                if ($sel == 0) {
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }
                $start_year = $ag_start_year . $ag_start_month . "01";
                if ($ag_start_month == 1) {
                    $set_max_ym = $ag_start_year . "1201";
                }
                else if ($ag_start_month < 10) {
                    $set_max_ym = ($ag_start_year + 1) . "0" . ($ag_start_month - 1) . "01";
                }
                else {
                    $set_max_ym = ($ag_start_year + 1) . ($ag_start_month - 1) . "01";
                }
                $sql2 = "select * from classmst,atrbmst,deptmst left join
						(select count_data,  src_dept_id, deptmst.dept_nm, ag_month from
						(select count(*) as count_data, src_dept_id, ag_month  from
						(select src_dept_id, news_date, substr(news_date, 5, 2) as ag_month from news where news_del_flg = 'f' ) wnews ";
                $cond2 = "where news_date between '" . $start_year . "' and '" . $set_max_ym . "'
						group by src_dept_id,  ag_month  order by src_dept_id
						) tbl_data
						left join deptmst on tbl_data.src_dept_id = deptmst.dept_id
						order by src_dept_id, ag_month
						) wdept on deptmst.dept_id = wdept.src_dept_id
						where dept_del_flg = 'f' and atrbmst.atrb_del_flg = 'f' and atrbmst.atrb_id = deptmst.atrb_id
						order by classmst.order_no,atrbmst.order_no,deptmst.order_no";
                $sel2 = select_from_table($con, $sql2, $cond2, $fname);


                $wk_lp_cnt = 0;
                $wd_id = "";
                $arr_set = array();
                while ($row = pg_fetch_array($sel2)) {
                    //全体をループ
                    $arr_set[$row["dept_id"]][$row["ag_month"]] = $row["count_data"];
                }

                $dis_month[0] = "";
                $dis_month[1] = "01";
                $dis_month[2] = "02";
                $dis_month[3] = "03";
                $dis_month[4] = "04";
                $dis_month[5] = "05";
                $dis_month[6] = "06";
                $dis_month[7] = "07";
                $dis_month[8] = "08";
                $dis_month[9] = "09";
                $dis_month[10] = "10";
                $dis_month[11] = "11";
                $dis_month[12] = "12";


                while ($row = pg_fetch_array($sel)) {
                    //施設分ループ
                    //施設名を表示
                    //月数ぶんループ
                    //$arr_set[$row[ループの施設ID]][ループの月数] = $row["count_data"];

                    $data .= ("<tr>\n");
                    $facility_nm = $row["class_nm"] . "＞" . $row["atrb_nm"] . "＞" . $row["dept_nm"];
                    $facility_id = $row["dept_id"];
                    $data .= ("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$facility_nm</font></td>\n");
                    $serch_month = (int) $ag_start_month;
                    $all_count = 0;
                    for ($i = 1; $i < 13; $i++) {
                        if ($serch_month > 12) {
                            $serch_month = 1;
                        }

                        $count = $arr_set[$facility_id][$dis_month[$serch_month]];
                        if ($count != NULL) {
                            $data .= ("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . $arr_set[$facility_id][$dis_month[$serch_month]] . "</font></td>\n");
                        }
                        else {
                            $data .= ("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">0</font></td>\n");
                        }
                        $all_count += $arr_set[$facility_id][$dis_month[$serch_month]];
                        $serch_month++;
                    }

                    //月数ぶんループ終了
                    //施設分ループ終了
                    //合計数表示
                    $data .= ("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$all_count</font></td>\n");

                    $data .= ("</tr>\n");
                }

                break;

            case 4:
                $sql = "select r.*, d.dept_nm, a.atrb_nm, c.class_nm from classroom r left join deptmst d on r.dept_id = d.dept_id left join atrbmst a on d.atrb_id = a.atrb_id left join classmst c on a.class_id = c.class_id";
                $cond = "where r.room_del_flg = 'f' order by c.order_no,a.order_no,d.order_no,r.order_no";
                $sel = select_from_table($con, $sql, $cond, $fname);
                if ($sel == 0) {
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }
                $start_year = $ag_start_year . $ag_start_month . "01";
                if ($ag_start_month == 1) {
                    $set_max_ym = $ag_start_year . "1201";
                }
                else if ($ag_start_month < 10) {
                    $set_max_ym = ($ag_start_year + 1) . "0" . ($ag_start_month - 1) . "01";
                }
                else {
                    $set_max_ym = ($ag_start_year + 1) . ($ag_start_month - 1) . "01";
                }
                $sql2 = "select * from deptmst,classroom left join
							(select count_data,  src_room_id, classroom.room_nm, ag_month from
							(select count(*) as count_data, src_room_id, ag_month  from
							(select src_room_id, news_date, substr(news_date, 5, 2) as ag_month from news where news_del_flg = 'f' ) wnews ";
                $cond2 = "where news_date between '" . $start_year . "' and '" . $set_max_ym . "'
							group by src_room_id,  ag_month  order by src_room_id
							) tbl_data
							left join classroom on tbl_data.src_room_id = classroom.room_id
							order by src_room_id, ag_month
							) wdept on classroom.room_id = wdept.src_room_id
							where room_del_flg = 'f' and deptmst.dept_del_flg = 'f' and deptmst.dept_id = classroom.dept_id
							order by classmst.order_no,atrbmst.order_no,deptmst.order_no,roommst.order_no";
                $sel2 = select_from_table($con, $sql2, $cond2, $fname);


                $wk_lp_cnt = 0;
                $wd_id = "";
                $arr_set = array();
                while ($row = pg_fetch_array($sel2)) {
                    //全体をループ
                    $arr_set[$row["room_id"]][$row["ag_month"]] = $row["count_data"];
                }

                $dis_month[0] = "";
                $dis_month[1] = "01";
                $dis_month[2] = "02";
                $dis_month[3] = "03";
                $dis_month[4] = "04";
                $dis_month[5] = "05";
                $dis_month[6] = "06";
                $dis_month[7] = "07";
                $dis_month[8] = "08";
                $dis_month[9] = "09";
                $dis_month[10] = "10";
                $dis_month[11] = "11";
                $dis_month[12] = "12";


                while ($row = pg_fetch_array($sel)) {
                    //施設分ループ
                    //施設名を表示
                    //月数ぶんループ
                    //$arr_set[$row[ループの施設ID]][ループの月数] = $row["count_data"];

                    $data .= ("<tr>\n");
                    $facility_nm = $row["class_nm"] . "＞" . $row["atrb_nm"] . "＞" . $row["dept_nm"] . "＞" . $row["room_nm"];
                    $facility_id = $row["room_id"];
                    $data .= ("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$facility_nm</font></td>\n");
                    $serch_month = (int) $ag_start_month;
                    $all_count = 0;
                    for ($i = 1; $i < 13; $i++) {
                        if ($serch_month > 12) {
                            $serch_month = 1;
                        }

                        $count = $arr_set[$facility_id][$dis_month[$serch_month]];
                        if ($count != NULL) {
                            $data .= ("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . $arr_set[$facility_id][$dis_month[$serch_month]] . "</font></td>\n");
                        }
                        else {
                            $data .= ("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">0</font></td>\n");
                        }
                        $all_count += $arr_set[$facility_id][$dis_month[$serch_month]];
                        $serch_month++;
                    }

                    //月数ぶんループ終了
                    //施設分ループ終了
                    //合計数表示
                    $data .= ("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$all_count</font></td>\n");

                    $data .= ("</tr>\n");
                }

                break;
        }
    }
    else if ($_REQUEST["ag_type"] == "1") {
        $sql = "select * from empmst";
        $cond = "order by emp_kn_lt_nm,emp_kn_ft_nm";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $start_year = $ag_start_year . $ag_start_month . "01";
        if ($ag_start_month == 1) {
            $set_max_ym = $ag_start_year . "1201";
        }
        else if ($ag_start_month < 10) {
            $set_max_ym = ($ag_start_year + 1) . "0" . ($ag_start_month - 1) . "01";
        }
        else {
            $set_max_ym = ($ag_start_year + 1) . ($ag_start_month - 1) . "01";
        }
        $sql2 = "select count_data, src_emp_id, ag_month, ref_empmst.emp_lt_nm, ref_empmst.emp_ft_nm, class_nm,atrb_nm,dept_nm, room_nm
				from(
				select count(*) as count_data, src_emp_id, ag_month from
				(select src_emp_id, news_date, substr(news_date, 5, 2) as ag_month from news where news_del_flg = 'f' ) wnews ";
        $cond2 = " where news_date between '" . $start_year . "' and '" . $set_max_ym . "' group by src_emp_id, ag_month
				) tbl_data
				inner join empmst ref_empmst on tbl_data.src_emp_id = ref_empmst.emp_id
				left join classmst on ref_empmst.emp_class = classmst.class_id
				left join atrbmst on ref_empmst.emp_attribute = atrbmst.atrb_id
				left join deptmst on ref_empmst.emp_dept = deptmst.dept_id
				left join classroom on ref_empmst.emp_room = classroom.room_id
				order by src_emp_id, ag_month";
        $sel2 = select_from_table($con, $sql2, $cond2, $fname);


        $wk_lp_cnt = 0;
        $wd_id = "";
        $arr_set = array();
        while ($row = pg_fetch_array($sel2)) {
            //全体をループ
            $arr_set[$row["src_emp_id"]][$row["ag_month"]] = $row["count_data"];
            $arr_set[$row["src_emp_id"]]["name"] = $row["emp_lt_nm"] . $row["emp_ft_nm"];
            if ($row["room_nm"] == "") {
                $arr_set[$row["src_emp_id"]]["dept"] = $row["class_nm"] . ">" . $row["atrb_nm"] . ">" . $row["dept_nm"];
            }
            else {
                $arr_set[$row["src_emp_id"]]["dept"] = $row["class_nm"] . ">" . $row["atrb_nm"] . ">" . $row["dept_nm"] . ">" . $row["room_nm"];
            }
        }

        $dis_month[0] = "";
        $dis_month[1] = "01";
        $dis_month[2] = "02";
        $dis_month[3] = "03";
        $dis_month[4] = "04";
        $dis_month[5] = "05";
        $dis_month[6] = "06";
        $dis_month[7] = "07";
        $dis_month[8] = "08";
        $dis_month[9] = "09";
        $dis_month[10] = "10";
        $dis_month[11] = "11";
        $dis_month[12] = "12";


        foreach ($arr_set as $key => $value) {
            //施設分ループ
            //施設名を表示
            //月数ぶんループ
            //$arr_set[$row[ループの施設ID]][ループの月数] = $row["count_data"];

            $data .= ("<tr>\n");
            $facility_nm = $value["name"];
            $dept_nm = $value["dept"];
            $data .= ("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$facility_nm</td><td align=\"right\">$dept_nm</font></td>\n");
            $serch_month = (int) $ag_start_month;
            $all_count = 0;
            for ($i = 1; $i < 13; $i++) {
                if ($serch_month > 12) {
                    $serch_month = 1;
                }

                $count = $arr_set[$key][$dis_month[$serch_month]];
                if ($count != NULL) {
                    $data .= ("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . $arr_set[$key][$dis_month[$serch_month]] . "</font></td>\n");
                }
                else {
                    $data .= ("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">0</font></td>\n");
                }
                $all_count += $arr_set[$key][$dis_month[$serch_month]];
                $serch_month++;
            }

            //月数ぶんループ終了
            //施設分ループ終了
            //合計数表示
            $data .= ("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$all_count</font></td>\n");

            $data .= ("</tr>\n");
        }
    }






    $data .= "</table>";

    return $data;
}
