<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");
require_once("hiyari_common.ini");

//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$hiyari = check_authority($session, 48, $fname);
if ($hiyari == "0")
{
	showLoginPage();
	exit;
}


//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//※削除は要望が上がるまで無しにする。(データケースが増えるから。) ※なお、以下は動作未確認。
//	
//	//==============================
//	//ポストバック時の処理
//	//==============================
//	if( $postback != "")
//	{
//		//==============================
//		//様式の削除
//		//==============================
//		
//		//SQLのin句
//		$in=" in (";
//		$count=count($trashbox);
//		for($i=0;$i<$count;$i++){
//			if(!$trashbox[$i]==""){
//				$in.="'$trashbox[$i]'";
//				if($i<($count-1)){
//					$in.=",";
//				}
//			}
//		}
//		$in.=")";
//		
//		//使用中チェック ･･･職種に割り当てられている様式は削除できない。
//		$tbl = "select * from inci_set_job_link";
//		$cond = " where inci_set_job_link.eis_no $in";
//		$sel = select_from_table($con,$tbl,$cond,$fname);
//		if($sel == 0){
//			pg_close($con);
//			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
//			echo("<script language='javascript'>showErrorPage(window);</script>");
//			pg_close($con);
//			exit;
//		}
//		$num = pg_numrows($sel);
//		if($num > 0)
//		{
//			echo("<script language='javascript'>alert(\"職種に割り当てられている様式は削除できません。\");</script>");
//			echo("<script language='javascript'>history.back();</script>");
//			pg_close($con);
//			exit;
//		}
//		
//	if(false)
//	{
//		//削除処理(論理削除)
//		$sql = "update inci_easyinput_set set";
//		$set = array("del_flag");
//		$setvalue = array("t");
//		$cond = "where inci_easyinput_set.eis_no = $in";
//		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
//		if ($upd == 0) {
//			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
//			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
//			pg_close($con);
//			exit;
//		}
//		
//	}
//	}






//==============================
//HTML出力
//==============================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$INCIDENT_TITLE?> | 様式一覧</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.tablednd.js"></script>
<script type="text/javascript">
$(function() {
	$('#eislist tbody').tableDnD({
		dragHandle: "dragHandle"
	});

	$('#eislist tbody tr').hover(function() {
		$(this.cells).css("cursor","move");
		$(this.cells[0]).addClass('showDragHandle');
		$(this.cells).css('background-color', 'gold');
	},

	function() {
		$(this.cells[0]).removeClass('showDragHandle');
		$(this.cells).css('background-color', 'white');
	});
});

$(function() {
	$('#upd').click(function() {
		$('#frm').attr("action","hiyari_easyinput_set_list_update.php");
		$('#frm').submit();
	});
});

function check_delete()
{
	//削除対照指定チェック
	var is_selected = false;
	var objs = document.getElementsByName("trashbox[]");
	for(var i = 0; i < objs.length; i++)
	{
		var obj = objs[i];
		if(obj.checked)
		{
			is_selected = true;
		}
	}
	if(!is_selected)
	{
		alert("チェックボックスをオンにしてください。");
		return;
	}
	
	//削除意思確認チェック
	if (!confirm("削除してよろしいですか？"))
	{
		return;
	}
	document.form1.submit();
	
}


</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

.list {border-collapse:collapse;}
.list td {border:#35B341 solid 1px;}

.in_list {border-collapse:collapse;}
.in_list td {border:#FFFFFF solid 0px;}

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">

<!-- BODY全体 START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<!-- ヘッダー START -->
<?
show_hiyari_header($session,$fname,true);
?>
<!-- ヘッダー END -->

<img src="img/spacer.gif" width="1" height="5" alt=""><br>


<!-- 本体 START -->
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>



<!-- 外の枠 START -->
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#FF0000">※ ドラッグ アンド ドロップで並び替えを行えます。並び替え後は「更新」ボタンをクリックしてください。</font>
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="600" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
<tr>
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td bgcolor="#F5FFE5">	



<!-- 送信領域 START-->
<form id="frm" name="form1" action="hiyari_easyinput_set_list.php" method="post">
<input type="hidden" name="postback" value="ture">
<input type="hidden" name="session" value="<?=$session?>">


<!-- 一覧 START-->
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list" id="eislist">
<thead>
<tr height="22">
<!--
	<td width="30" align="center" bgcolor="#DFFFDC">
		&nbsp
	</td>
-->
	<td width="70" align="center" bgcolor="#DFFFDC">
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">様式番号</font>
	</td>
	<td width="530" align="left" bgcolor="#DFFFDC">
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">様式名</font>
	</td>
</tr>
</thead>
<tbody>
<?

//様式一覧取得の取得
$sql = "select * from inci_easyinput_set where not del_flag order by order_no";
$sel_eis = select_from_table($con,$sql,"",$fname);

//診療区分ループ(一覧用)
$eis_count = pg_numrows($sel_eis);
for($i=0;$i<$eis_count;$i++)
{

	//職種情報
	$eis_id        = pg_fetch_result($sel_eis,$i,"eis_id");
	$eis_no        = pg_fetch_result($sel_eis,$i,"eis_no");
	$eis_name      = pg_fetch_result($sel_eis,$i,"eis_name");
	$eis_name_html = htmlspecialchars($eis_name);
?>

<tr height="22">
<!--
<td align="center" bgcolor="#FFFFFF">
	<input type="checkbox" name="trashbox[]" value="<?=$eis_no?>">
</td>
-->
<td class="dragHandle" align="center" bgcolor="#FFFFFF">
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="hidden" name="eis_id[]" value="<?=$eis_id?>"><?=$eis_no?></font>
</td>
<td class="dragHandle" align="left" bgcolor="#FFFFFF">
	<a href="hiyari_easyinput_set_update.php?session=<?=$session?>&eis_no=<?=$eis_no?>">
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$eis_name_html?></font>
	</a>
</td>
</tr>

<?
}
?>
</tbody>
</table>

<!-- 一覧 END -->
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><input id="upd" type="button" value="更新"></font></td>
</tr>
</table>

<!-- 送信ボタン START -->
<!--
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><input type="button" value="削除" onclick="return check_delete();"></font></td>
</tr>
</table>
-->
<!-- 送信ボタン END -->



</form>
<!-- 送信領域 END -->



</td>
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>
<!-- 外の枠 END -->

</td>
</tr>
</table>
<!-- 本体 END -->

<!-- フッター START -->
<?
show_hiyari_footer($session,$fname,true);
?>
<!-- フッター END -->

</td>
</tr>
</table>
<!-- BODY全体 END -->

</body>
</html>
<?



//==============================
//DBコネクション終了
//==============================
pg_close($con);
?>
