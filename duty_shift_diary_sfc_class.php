<?php
//管理日誌（SFC形式）用クラス
require_once("about_postgres.php");
require_once("date_utils.php");

class duty_shift_diary_sfc_class
{
    var $file_name;	// 呼び出し元ファイル名
    var $_db_con;	// DBコネクション
    
    /*************************************************************************/
    // コンストラクタ
    // @param object $con DBコネクション
    // @param string $fname 画面名
    /*************************************************************************/
    function duty_shift_diary_sfc_class($con, $fname)
    {
        // 呼び出し元ファイル名を保持
        $this->file_name = $fname;
        // DBコネクションを保持
        $this->_db_con = $con;
    }
    /**
     * SFC連携コード取得
     *
     * @return 勤務パターングループ_勤務パターン_事由をキーとした配列、値は連携コード
     *
     */
    function get_sfc_code() {
        
        $sql = "select ";
        $sql .= " pattern_id, atdptn_ptn_id, reason, sfc_code, font_name ";
        $sql .= " from duty_shift_pattern ";
        $cond = "order by pattern_id, order_no, atdptn_ptn_id, reason ";
		    $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		    if ($sel == 0) {
			    pg_close($this->_db_con);
            echo("<script type='text/javascript' src='./js/showpage.js'></script>");
            echo("<script language='javascript'>showErrorPage(window);</script>");
            exit;
        }
        $num = pg_numrows($sel);
        
        $arr_sfc_code = array();
        
        for ($i=0; $i<$num; $i++) {
            $pattern_id = pg_fetch_result($sel, $i, "pattern_id");
            $atdptn_ptn_id = pg_fetch_result($sel, $i, "atdptn_ptn_id");
            $reason = pg_fetch_result($sel, $i, "reason");
            $sfc_code = pg_fetch_result($sel, $i, "sfc_code");
            $reason = ($reason == "" || $reason == " ") ? "0" : $reason;
            $wk_key = $pattern_id."_".$atdptn_ptn_id."_".$reason;
            $arr_sfc_code[$wk_key] = $sfc_code;		
        }
        
        return $arr_sfc_code;
    }
    
    /**
     * SFC職員コード変換用ファイルを取得する
     *
     * @param mixed $csv_file_name ファイル名
     * @return mixed 職員IDをキーとした配列
     *
     */
    function get_convert_id($csv_file_name) {
        
        $arr_convert_id = array();
        
        $lines = file($csv_file_name);
        foreach ($lines as $line) {
            
            // 文字コードを変換
            $line = trim(mb_convert_encoding($line, "EUC-JP", "auto"));
            
            // EOFを削除
            $line = str_replace(chr(0x1A), "", $line);
            // 空行は無視
            if ($line == "") {
                continue;
            }
            // カンマで分割し配列へ設定
            $emp_data = split(",", $line);
            $emp_personal_id = $emp_data[0];
            $arr_convert_id[$emp_personal_id] = $emp_data[1];
        }
        return $arr_convert_id;
    }
    
    /**
     * シフトグループ情報を取得する
     *
     * @param mixed $group_id シフトグループID
     * @return mixed シフトグループ情報
     *
     */
    function get_group_info($group_id) {
        $sql = "select * from duty_shift_group ";
        $cond = "where group_id = '$group_id'  ";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
            echo("<script type='text/javascript' src='./js/showpage.js'></script>");
            echo("<script language='javascript'>showErrorPage(window);</script>");
            exit;
        }
        $group_array = array();
        $num = pg_numrows($sel);
        if ($num > 0) {
            $group_array = pg_fetch_array($sel);
        }
        if ($group_array["start_month_flg1"] == "") {$group_array["start_month_flg1"] = "1";}
        if ($group_array["start_day1"] == "" || $group_array["start_day1"] == "0") {$group_array["start_day1"] = "1";}
        if ($group_array["month_flg1"] == "" || $group_array["end_day1"] == "0") {$group_array["month_flg1"] = "1";} //終了日確認
        if ($group_array["end_day1"] == "" || $group_array["end_day1"] == "0") {  $group_array["end_day1"] = "99";}
        
        return $group_array;
    }
    
    /**
     * シフトグループ（病棟）、指定年月のSFCコード情報を取得
     * 月またがり対応
     *
     * @param mixed $group_id シフトグループID
     * @param mixed $start_yr 年
     * @param mixed $start_mon 月
     * @param mixed $arr_emp_info 職員情報（get_emp_info_for_sfcで取得）
     * @param mixed $group_array $group_idでマスタから検索した情報
     * @param mixed $atdbk_flg 実績予定フラグ "1":実績（デフォルト） "2":予定
     * @return mixed 職員、日別のSFCコードの配列
     *
     */
    function get_sfc_csv_data($group_id, $start_yr, $start_mon, $arr_emp_info, $group_array, $atdbk_flg = "1") {
        
        //職員設定のグループID
        $arr_main_group_id = array();
        $arr_main_group_id2 = array();
        $arr_personal_id = array();
        for ($i=0; $i<count($arr_emp_info); $i++) {
            $tmp_emp_id = $arr_emp_info[$i]["emp_id"];
            $arr_main_group_id[$tmp_emp_id] = $arr_emp_info[$i]["main_group_id"];
            $arr_main_group_id2[$tmp_emp_id] = $arr_emp_info[$i]["main_group_id2"];
            $arr_personal_id[$tmp_emp_id] = $arr_emp_info[$i]["emp_personal_id"];
        }
        
        $start_month_flg1 = $group_array["start_month_flg1"];
        $start_day1 = $group_array["start_day1"];
        $month_flg1 = $group_array["month_flg1"];
        $end_day1 = $group_array["end_day1"];
        
        //当月1日から当月末
        if ($start_month_flg1 == "1" &&
                $start_day1 == "1" &&
                $month_flg1 == "1") {
            $month_fromto_flg = "0";
        }
        //月またがり
        else {
            if ($start_month_flg1 == "0") {
                $month_fromto_flg = "1";
                
                //前月当月
                $target_year1 = $start_yr;
                $target_mon1 = intval($start_mon);
                
                $target_year2 = $start_yr;
                $target_mon2 = $start_mon+1;
                if ($target_mon2 > 12) {
                    $target_mon2 = 1;
                    $target_year2++;
                }
            }
            elseif ($month_flg1 == "2") {
                $month_fromto_flg = "2";
                //当月翌月
                $target_year1 = $start_yr;
                $target_mon1 = $start_mon-1;
                if ($target_mon1 < 1) {
                    $target_mon1 = 12;
                    $target_year1--;
                }
                
                $target_year2 = $start_yr;
                $target_mon2 = intval($start_mon);
            }
            $s_date = $start_yr.sprintf("%02d", $start_mon)."01"; //月の開始日
            $close_date = $start_yr.sprintf("%02d%02d", $start_mon, $end_day1); //締め日
            $close_next_date = $start_yr.sprintf("%02d%02d", $start_mon, $end_day1+1); //締め日翌日
            $e_date = date("Ymt", date_utils::to_timestamp_from_ymd($s_date)); //月の終了日
        }
        
        $arr_no = array();
        $cond_str = "";
        if ($month_fromto_flg != "0") {
            for ($i=0; $i<count($arr_emp_info); $i++) {
                $tmp_emp_id = $arr_emp_info[$i]["emp_id"];
                $arr_no[$tmp_emp_id] =  $arr_emp_info[$i]["no"];
                
                if ($cond_str != "") {
                    $cond_str .= " or ";
                }
                $cond_str .= " (a.emp_id = '{$arr_emp_info[$i]["emp_id"]}' and a.date >= '{$arr_emp_info[$i]["start_date"]}' and a.date <= '{$arr_emp_info[$i]["end_date"]}') ";
            }
            if ($cond_str != "") {
                $cond_str = " and ($cond_str) ";
            }
        }
        
        $arr_sfc_code = $this->get_sfc_code();
        $wk_mm = sprintf("%02d", $start_mon);
        $arr_data = array();
        
        $sql = "select ";
        if ($month_fromto_flg == "0") {
            $sql .= " b.no, ";
        }
        $sql .= " a.emp_id, a.date, substr(a.date, 7, 2) as dd, a.tmcd_group_id, a.pattern, a.reason, c.emp_personal_id, d.group_id as assist_group_id ";
        if ($atdbk_flg == "1") {
            $sql .= " from atdbkrslt a ";
        }
        else {
            $sql .= " from atdbk a ";
        }
        $sql .= " left join duty_shift_plan_assist d on d.emp_id = a.emp_id and d.duty_date = a.date ";
        if ($month_fromto_flg == "0") {
            $sql .= " inner join duty_shift_plan_staff b on b.emp_id = a.emp_id and b.group_id = '$group_id' and b.duty_yyyy||lpad(CAST(b.duty_mm AS varchar), 2, '0') = substr(a.date, 1, 6) ";
            $sql .= " left join empmst c on a.emp_id = c.emp_id ";
            $cond = "where substr(a.date, 1, 6) = '$start_yr$wk_mm' and a.pattern != '' ";
            $cond .= " order by b.no, a.date ";
        }
        else {
            $sql .= " left join empmst c on a.emp_id = c.emp_id ";
            $cond = "where a.pattern != '' $cond_str ";
            $cond .= " order by a.emp_id, a.date ";
        }
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
            echo("<script type='text/javascript' src='./js/showpage.js'></script>");
            echo("<script language='javascript'>showErrorPage(window);</script>");
            exit;
        }
        $num = pg_numrows($sel);
        
        for ($i=0; $i<$num; $i++) {
            $emp_id = pg_fetch_result($sel, $i, "emp_id");
            $dd = pg_fetch_result($sel, $i, "dd");
            $int_dd = intval($dd);
            //応援確認
            $assist_group_id = pg_fetch_result($sel, $i, "assist_group_id");
            //応援のグループIDが空白の場合は職員設定のグループを確認
            if ($assist_group_id == "") {
                //作成期間に対応した職員設定のシフトグループ
                //月またがりで締め日よりあとの場合
                if ($month_fromto_flg != "0" &&
                        $int_dd > $end_day1) {
                    $main_group_id = $arr_main_group_id2[$emp_id];
                }
                //月またがりなし、ありでも締め日以前の場合
                else {
                    $main_group_id = $arr_main_group_id[$emp_id];
                }
                if ($main_group_id != "" && $main_group_id != $group_id) {
                    continue;
                }
            }
            //応援のグループを確認
            else if ($assist_group_id != $group_id) {
                continue;
            }
            //月またがりなし
            if ($month_fromto_flg == "0") {
                $no = pg_fetch_result($sel, $i, "no");
            }
            else {
                $no = $arr_no[$emp_id];
            }
            $tmcd_group_id = pg_fetch_result($sel, $i, "tmcd_group_id");
            $pattern = pg_fetch_result($sel, $i, "pattern");
            $reason = pg_fetch_result($sel, $i, "reason");
            //休暇（事由なし）は34とする
            if ($pattern == "10" && ($reason == "" || $reason == " ")) {
                $reason = "34";
            }
            $reason = ($reason == "" || $reason == " " || $pattern != "10") ? "0" : $reason; //休暇以外は検索のため事由を0とする 20120127
            $wk_key = $tmcd_group_id."_".$pattern."_".$reason;
            //sfcコード
            $sfc_code = $arr_sfc_code[$wk_key];
            // 空白の場合は、"0000"とする
            if ($sfc_code == "") {
                $sfc_code = "0000";
            }
            $arr_data[$no-1][$int_dd] = $sfc_code;
            $arr_data[$no-1]["emp_personal_id"] = $arr_personal_id[$emp_id];
        }
        return $arr_data;
        
    }
    
    
    /**
     * シフトグループ（病棟）に指定年月時点で所属している職員の情報を取得(SFC連携用）
     * 月またがり対応
     *
     * @param mixed $group_id シフトグループID
     * @param mixed $duty_yyyy 年
     * @param mixed $duty_mm 月
     * @param mixed $group_array $group_idでマスタから検索した情報
     * @param mixed $history_get_flg "1":取得 "":取得しない
     * @return mixed 職員情報
     *
     */
    function get_emp_info_for_sfc($group_id, $duty_yyyy, $duty_mm, $group_array, $history_get_flg="") {
        
        $start_month_flg1 = $group_array["start_month_flg1"];
        $start_day1 = $group_array["start_day1"];
        $month_flg1 = $group_array["month_flg1"];
        $end_day1 = $group_array["end_day1"];
        
        //当月1日から当月末
        if ($start_month_flg1 == "1" &&
                $start_day1 == "1" &&
                $month_flg1 == "1") {
            $month_fromto_flg = "0";
        }
        //月またがり
        else {
            if ($start_month_flg1 == "0") {
                $month_fromto_flg = "1";
                
                //前月当月
                $target_year1 = $duty_yyyy;
                $target_mon1 = intval($duty_mm);
                
                $target_year2 = $duty_yyyy;
                $target_mon2 = $duty_mm+1;
                if ($target_mon2 > 12) {
                    $target_mon2 = 1;
                    $target_year2++;
                }
            }
            elseif ($month_flg1 == "2") {
                $month_fromto_flg = "2";
                //当月翌月
                $target_year1 = $duty_yyyy;
                $target_mon1 = $duty_mm-1;
                if ($target_mon1 < 1) {
                    $target_mon1 = 12;
                    $target_year1--;
                }
                
                $target_year2 = $duty_yyyy;
                $target_mon2 = intval($duty_mm);
            }
            $s_date = $duty_yyyy.sprintf("%02d", $duty_mm)."01"; //月の開始日
            $close_date = $duty_yyyy.sprintf("%02d%02d", $duty_mm, $end_day1); //締め日
            $close_next_date = $duty_yyyy.sprintf("%02d%02d", $duty_mm, $end_day1+1); //締め日翌日
            $e_date = date("Ymt", date_utils::to_timestamp_from_ymd($s_date)); //月の終了日
        }
        
        //当月1日から当月末
        if ($month_fromto_flg == "0") {
            $sql = "select ";
            $sql .= " a.emp_id, b.emp_personal_id, b.emp_lt_nm, b.emp_ft_nm, c.group_id as main_group_id ";
            $sql .= " , d.duty_form, e.link_key as job_link_key ";
            $sql .= " , f.link_key as st_link_key ";
            $sql .= " from duty_shift_plan_staff a ";
            $sql .= " left join empmst b on a.emp_id = b.emp_id ";
            $sql .= " left join duty_shift_staff c on c.emp_id = a.emp_id ";
            $sql .= " left join empcond d on d.emp_id = a.emp_id "; //勤務条件 
            $sql .= " left join jobmst e on e.job_id = b.emp_job ";
            $sql .= " left join stmst f on f.st_id = b.emp_st ";
            
            $cond = "where a.group_id = '$group_id' and a.duty_yyyy = $duty_yyyy and a.duty_mm = $duty_mm ";
            $cond .= " order by a.no ";
		    $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		    if ($sel == 0) {
			    pg_close($this->_db_con);
                echo("<script type='text/javascript' src='./js/showpage.js'></script>");
                echo("<script language='javascript'>showErrorPage(window);</script>");
                exit;
            }
            $num = pg_numrows($sel);
            
            $arr_emp_info = array();
            
            for ($i=0; $i<$num; $i++) {
                $emp_id = pg_fetch_result($sel, $i, "emp_id");
                $arr_emp_info[$i]["emp_id"] = pg_fetch_result($sel, $i, "emp_id");		
                $arr_emp_info[$i]["emp_personal_id"] = pg_fetch_result($sel, $i, "emp_personal_id");		
                $arr_emp_info[$i]["emp_lt_nm"] = pg_fetch_result($sel, $i, "emp_lt_nm");		
                $arr_emp_info[$i]["emp_ft_nm"] = pg_fetch_result($sel, $i, "emp_ft_nm");		
                $arr_emp_info[$i]["no"] = pg_fetch_result($sel, $i, "no");		
                $arr_emp_info[$i]["main_group_id"] = pg_fetch_result($sel, $i, "main_group_id");
                $wk_duty_form = pg_fetch_result($sel, $i, "duty_form"); //勤務条件、勤務形態
                if ($wk_duty_form == "") {
                    $wk_duty_form = "01";
                }
                //常勤"1"->"01", 非常勤"2"->"02", 短時間正職員"3"->"03"
                else {
                    $wk_duty_form = "0".$wk_duty_form;
                }
                $arr_emp_info[$i]["duty_form"] = $wk_duty_form;
                //職種
                $wk_job_link_key = pg_fetch_result($sel, $i, "job_link_key");
                if ($wk_job_link_key == "") {
                    $wk_job_link_key = "00";
                }
                $arr_emp_info[$i]["job_link_key"] = $wk_job_link_key;
                //役職
                $wk_st_link_key = pg_fetch_result($sel, $i, "st_link_key");
                if ($wk_st_link_key == "") {
                    $wk_st_link_key = "00";
                }
                $arr_emp_info[$i]["st_link_key"] = $wk_st_link_key;		
            }
            
        }
        //月またがり
        else {
            $sql = "select ";
            $sql .= "a.emp_id, a.duty_yyyy, a.duty_mm, a.no, b.emp_personal_id, b.emp_lt_nm, b.emp_ft_nm, c.group_id as main_group_id ";
            $sql .= " , d.duty_form, e.link_key as job_link_key ";
            $sql .= " , f.link_key as st_link_key ";
            $sql .= "from duty_shift_plan_staff a ";
            $sql .= " left join empmst b on a.emp_id = b.emp_id ";
            $sql .= " left join duty_shift_staff c on c.emp_id = a.emp_id ";
            $sql .= " left join empcond d on d.emp_id = a.emp_id "; //勤務条件 
            $sql .= " left join jobmst e on e.job_id = b.emp_job ";
            $sql .= " left join stmst f on f.st_id = b.emp_st ";
            $cond = "where a.group_id = '$group_id' and ((a.duty_yyyy = $target_year1 and a.duty_mm = $target_mon1) or (a.duty_yyyy = $target_year2 and a.duty_mm = $target_mon2)) ";
            $cond .= "order by a.duty_yyyy, a.duty_mm, a.no ";    
            
		    $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		    if ($sel == 0) {
			    pg_close($this->_db_con);
                echo("<script type='text/javascript' src='./js/showpage.js'></script>");
                echo("<script language='javascript'>showErrorPage(window);</script>");
                exit;
            }
            $arr_group_staff = array();
            $arr_staff = array();
            $arr_sort = array(); //並び順用
            $s_idx = 0;
            $arr_personal_id = array();
            $arr_emp_name = array();
            $arr_main_group_id = array();
            $arr_link_info = array();
            while( $row = pg_fetch_array($sel) ) {
                $tmp_emp_id = $row["emp_id"];
                $arr_staff[$tmp_emp_id] = 1;
                $arr_personal_id[$tmp_emp_id] = $row["emp_personal_id"];
                $arr_main_group_id[$tmp_emp_id] = $row["main_group_id"];
                $arr_link_info[$tmp_emp_id]["emp_lt_nm"] = $row["emp_lt_nm"];
                $arr_link_info[$tmp_emp_id]["emp_ft_nm"] = $row["emp_ft_nm"];
                
                $wk_duty_form = $row["duty_form"]; //勤務条件、勤務形態
                if ($wk_duty_form == "") {
                    $wk_duty_form = "01";
                }
                //常勤"1"->"01", 非常勤"2"->"02", 短時間正職員"3"->"03"
                else {
                    $wk_duty_form = "0".$wk_duty_form;
                }
                $arr_link_info[$tmp_emp_id]["duty_form"] = $wk_duty_form;
                //職種
                $wk_job_link_key = $row["job_link_key"];
                if ($wk_job_link_key == "") {
                    $wk_job_link_key = "00";
                }
                $arr_link_info[$tmp_emp_id]["job_link_key"] = $wk_job_link_key;
                //役職
                $wk_st_link_key = $row["st_link_key"];
                if ($wk_st_link_key == "") {
                    $wk_st_link_key ="00";
                }
                $arr_link_info[$tmp_emp_id]["st_link_key"] = $wk_st_link_key;

                $tmp_yyyy = $row["duty_yyyy"];
                $tmp_mm = $row["duty_mm"];
                $arr_group_staff[$tmp_emp_id][$tmp_yyyy][$tmp_mm] = 1;
                $tmp_yyyymm = $tmp_yyyy.sprintf("%02d", $tmp_mm);
                $tmp_no = $row["no"];
                $arr_sort[$s_idx]["emp_id"] = $tmp_emp_id;
                $arr_sort[$s_idx]["yyyymm"] = $tmp_yyyymm;
                $arr_sort[$s_idx]["no"] = $tmp_no;
                $s_idx++;
            }
            //表示順設定用
            $arr_no = array();
            //月調整、前月当月
            if ($month_fromto_flg == "1") {
                $target_yyyymm1 = $target_year1.sprintf("%02d", $target_mon1);
                $target_yyyymm2 = $target_year2.sprintf("%02d", $target_mon2);
            }
            //当月翌月
            else {
                $target_yyyymm1 = $target_year2.sprintf("%02d", $target_mon2);
                $target_yyyymm2 = $target_year1.sprintf("%02d", $target_mon1);
            }
            //当月分
            for ($i=0; $i<$s_idx; $i++) {
                if ($arr_sort[$i]["yyyymm"] == $target_yyyymm1) {
                    $tmp_emp_id = $arr_sort[$i]["emp_id"];
                    $arr_no[$tmp_emp_id] = $arr_sort[$i]["no"];
                    
                    $no_idx = $arr_sort[$i]["no"] - 1;
                    $arr_emp_info[$no_idx]["emp_id"] = $tmp_emp_id;
                    $arr_emp_info[$no_idx]["emp_personal_id"] = $arr_personal_id[$tmp_emp_id];
                    $arr_emp_info[$no_idx]["no"] = $arr_sort[$i]["no"];
                    $arr_emp_info[$no_idx]["main_group_id"] = $arr_main_group_id[$tmp_emp_id];
                    $arr_emp_info[$no_idx]["emp_lt_nm"] = $arr_link_info[$tmp_emp_id]["emp_lt_nm"];
                    $arr_emp_info[$no_idx]["emp_ft_nm"] = $arr_link_info[$tmp_emp_id]["emp_ft_nm"];
                    $arr_emp_info[$no_idx]["duty_form"] = $arr_link_info[$tmp_emp_id]["duty_form"];
                    $arr_emp_info[$no_idx]["job_link_key"] = $arr_link_info[$tmp_emp_id]["job_link_key"];
                    $arr_emp_info[$no_idx]["st_link_key"] = $arr_link_info[$tmp_emp_id]["st_link_key"];
                }
            }
            $cnt_no = count($arr_no);
            $e_idx = $cnt_no;
            //前月分または翌月分
            for ($i=0; $i<$s_idx; $i++) {
                if ($arr_sort[$i]["yyyymm"] == $target_yyyymm2) {
                    $tmp_emp_id = $arr_sort[$i]["emp_id"];
                    //当月分にない場合、追加
                    if ($arr_no[$tmp_emp_id] == "") {
                        
                        $arr_emp_info[$e_idx]["emp_id"] = $tmp_emp_id;
                        $arr_emp_info[$e_idx]["emp_personal_id"] = $arr_personal_id[$tmp_emp_id];
                        $arr_emp_info[$e_idx]["no"] = $e_idx + 1;
                        $arr_emp_info[$e_idx]["main_group_id"] = $arr_main_group_id[$tmp_emp_id];
                        $arr_emp_info[$e_idx]["emp_lt_nm"] = $arr_link_info[$tmp_emp_id]["emp_lt_nm"];
                        $arr_emp_info[$e_idx]["emp_ft_nm"] = $arr_link_info[$tmp_emp_id]["emp_ft_nm"];
                        $arr_emp_info[$e_idx]["duty_form"] = $arr_link_info[$tmp_emp_id]["duty_form"];
                        $arr_emp_info[$e_idx]["job_link_key"] = $arr_link_info[$tmp_emp_id]["job_link_key"];
                        $arr_emp_info[$e_idx]["st_link_key"] = $arr_link_info[$tmp_emp_id]["st_link_key"];
                        
                        $arr_no[$tmp_emp_id] = $e_idx + 1;
                        
                        $e_idx++;
                    }
                }
            }
            
            $cond_str = "";
            foreach ($arr_staff as $tmp_emp_id => $dummy) {
                
                //年月確認、前月当月、一ヶ月分
                if ($arr_group_staff[$tmp_emp_id][$target_year1][$target_mon1] == 1 &&
                        $arr_group_staff[$tmp_emp_id][$target_year2][$target_mon2] == 1) {
                    $tmp_start_date = $s_date;
                    $tmp_end_date = $e_date;
                }
                //前月、締め日までの前半分
                else if ($arr_group_staff[$tmp_emp_id][$target_year1][$target_mon1] == 1 &&
                        $arr_group_staff[$tmp_emp_id][$target_year2][$target_mon2] == "") {
                    $tmp_start_date = $s_date;
                    $tmp_end_date = $close_date;
                }
                //当月、締め日からの後半分
                else if ($arr_group_staff[$tmp_emp_id][$target_year1][$target_mon1] == "" &&
                        $arr_group_staff[$tmp_emp_id][$target_year2][$target_mon2] == 1) {
                    $tmp_start_date = $close_next_date;
                    $tmp_end_date = $e_date;
                }
                $idx = $arr_no[$tmp_emp_id] - 1;
                $arr_emp_info[$idx]["start_date"] = $tmp_start_date;
                $arr_emp_info[$idx]["end_date"] = $tmp_end_date;
            }
        }
        //職種等の履歴を取得
        if ($history_get_flg == "1") {
            $arr_emp_info = $this->get_empinfo_history($arr_emp_info, $duty_yyyy, $duty_mm);
        }
        if (count($arr_emp_info) > 0) {
            //シフトグループ履歴がある場合は設定
            $arr_group_id_history = $this->get_duty_shift_staff_history($duty_yyyy, $duty_mm, "01");
            for ($i=0; $i<count($arr_emp_info); $i++) {
                $wk_emp_id = $arr_emp_info[$i]["emp_id"];
                $arr_emp_info[$i]["main_group_id2"] = $arr_emp_info[$i]["main_group_id"]; //月またがりがある場合の後半部分用
                if ($arr_group_id_history[$wk_emp_id] != "") {
                    $arr_emp_info[$i]["main_group_id"] = $arr_group_id_history[$wk_emp_id];
                }
            }
            //月またがり対応
            if ($month_fromto_flg != "0") {
                $arr_group_id_history2 = $this->get_duty_shift_staff_history($duty_yyyy, $duty_mm, $end_day1+1);
                for ($i=0; $i<count($arr_emp_info); $i++) {
                    $wk_emp_id = $arr_emp_info[$i]["emp_id"];
                    if ($arr_group_id_history2[$wk_emp_id] != "") {
                        $arr_emp_info[$i]["main_group_id2"] = $arr_group_id_history2[$wk_emp_id];
                    }
                }
            }
        }
        return $arr_emp_info;
    }
    
    /**
     * 履歴情報取得
     *
     * @param mixed $arr_emp_info 職員情報
     * @param mixed $duty_yyyy 年
     * @param mixed $duty_mm 月
     * @return mixed 履歴情報上書き設定後の職員情報
     *
     */
    function get_empinfo_history($arr_emp_info, $duty_yyyy, $duty_mm) {
        
        $arr_emp_info_ret = $arr_emp_info;

        if (count($arr_emp_info) > 0) {
            
            //データ位置
            $arr_emp_idx = array();
            for ($i=0; $i<count($arr_emp_info); $i++) {
                $wk_emp_id = $arr_emp_info[$i]["emp_id"];
                $arr_emp_idx[$wk_emp_id] = $i;
            }
            
            $cond_emp_ids = "";
            for ($i=0; $i<count($arr_emp_info); $i++) {
                if ($cond_emp_ids != "") {
                    $cond_emp_ids .= ",";
                }
                $cond_emp_ids .= "'".$arr_emp_info[$i]["emp_id"]."'";
            }
            
            $start_date = $duty_yyyy.sprintf("%02d01", $duty_mm);
            $end_date = date("Ymt", mktime(0,0,0, $duty_mm, 1, $duty_yyyy));
            
            //基準日（1日）以降の履歴を取得し対象月の最新のデータを設定、未設定時は、呼出元のempmst,empcondの項目を使用
            $wk_ymd = $duty_yyyy.sprintf("-%02d-01 00:00:00", $duty_mm);
            //履歴には変更前のIDがレコードに登録されている
            $sql = "select a.emp_id, a.duty_form, a.histdate  from empcond_duty_form_history a ";
            $cond = " where a.histdate >= '$wk_ymd' "
                . " and a.emp_id in ($cond_emp_ids) "
                . " order by a.emp_id, a.histdate ";
            $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
            if ($sel == 0) {
                pg_close($this->_db_con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
            $arr_info = array();
            $mode_name = "duty_form";
            //取得
            while ($row = pg_fetch_array($sel)) {
                
                $emp_id = $row["emp_id"];
                //前ゼロ付編集
                $wk_duty_form = "0".$row[$mode_name];
                $wk_date = date("Ymd",strtotime($row["histdate"]));	//履歴日
                
                $arr_info[$emp_id][] = array($mode_name => $wk_duty_form, "histdate" => $wk_date);
            }
            //最新設定
            foreach ($arr_info as $emp_id => $arr_data) {
                $wk_ret_id = $this->get_new_id_in_term($arr_data, $mode_name, $start_date, $end_date);
                //未設定は置き換えない
                if ($wk_ret_id != "") {
                    $idx = $arr_emp_idx[$emp_id];
                    $arr_emp_info_ret[$idx][$mode_name] = $wk_ret_id;
                }
            }
            
            //職種
            $sql = "select a.emp_id, a.job_id, c.link_key as job_link_key, a.histdate from job_history a "
                . " left join jobmst c on c.job_id = a.job_id ";
            $cond = " where a.histdate >= '$wk_ymd' "
                . " and a.emp_id in ($cond_emp_ids) "
                . " order by a.emp_id, a.histdate ";
            $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
            if ($sel == 0) {
                pg_close($this->_db_con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
            $arr_info = array();
            $mode_name = "job_link_key";
            //取得
            while ($row = pg_fetch_array($sel)) {
                
                $emp_id = $row["emp_id"];

                $wk_job_link_key = $row[$mode_name];
                if ($wk_job_link_key == "") {
                    $wk_job_link_key = "00";
                }
                $wk_date = date("Ymd",strtotime($row["histdate"]));	//履歴日
                
                $arr_info[$emp_id][] = array($mode_name => $wk_job_link_key, "histdate" => $wk_date);
            }
            //最新設定
            foreach ($arr_info as $emp_id => $arr_data) {
                $wk_ret_id = $this->get_new_id_in_term($arr_data, $mode_name, $start_date, $end_date);
                //未設定は置き換えない
                if ($wk_ret_id != "") {
                    $idx = $arr_emp_idx[$emp_id];
                    $arr_emp_info_ret[$idx][$mode_name] = $wk_ret_id;
                }
            }
            
            //役職
            $sql = "select a.emp_id, a.st_id, c.link_key as st_link_key, a.histdate from st_history a "
                . " left join stmst c on c.st_id = a.st_id ";
            $cond = " where a.histdate >= '$wk_ymd' "
                . " and a.emp_id in ($cond_emp_ids) "
                . " order by a.emp_id, a.histdate ";
            $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
            if ($sel == 0) {
                pg_close($this->_db_con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
            $arr_info = array();
            $mode_name = "st_link_key";
            //取得
            while ($row = pg_fetch_array($sel)) {
                
                $emp_id = $row["emp_id"];
                
                $wk_st_link_key = $row[$mode_name];
                if ($wk_st_link_key == "") {
                    $wk_st_link_key = "00";
                }
                $wk_date = date("Ymd",strtotime($row["histdate"]));	//履歴日
                
                $arr_info[$emp_id][] = array($mode_name => $wk_st_link_key, "histdate" => $wk_date);
            }
            //最新設定
            foreach ($arr_info as $emp_id => $arr_data) {
                $wk_ret_id = $this->get_new_id_in_term($arr_data, $mode_name, $start_date, $end_date);
                //未設定は置き換えない
                if ($wk_ret_id != "") {
                    $idx = $arr_emp_idx[$emp_id];
                    $arr_emp_info_ret[$idx][$mode_name] = $wk_ret_id;
                }
            }
        }
        
        return $arr_emp_info_ret;
    }
    
    
    /**
     * 役職・職種・雇用勤務形態情報取得
     * 　期間内で最新状態のIDを返す、未設定の場合は呼び出し側で職員マスタの項目を使用する
     * 　duty_shift_common_class::get_show_name()を参考にしている
     * 
     * @param array  $arr_info      役職・職種・雇用勤務形態情報
     * @param string $mode_name     役職・職種・雇用勤務形態モード
     * @param string $start_date	 取得対象データの開始日
     * @param string $end_date		 取得対象データの終了日
     * @return string 取得情報
     *
     */
    function get_new_id_in_term($arr_info, $mode_name, $start_date, $end_date) {

        $max_key = max(array_keys($arr_info));	//最終履歴
        foreach ($arr_info as $value)
        {
            $alter_flag = FALSE;
            if($start_date <= $value["histdate"]  && $value["histdate"] <= $end_date)
            {
                $alter_flag = TRUE;
            }
            
            if ($start_date < $value["histdate"])
            {
                $id = $value[$mode_name];
                
                if ($alter_flag == FALSE)
                {
                    break;
                }
                
                if ( $arr_info[$max_key]["histdate"] == $value["histdate"])
                {
                    $id = "";	//最終履歴の場合、最新表示
                }
            }
        }
        
        return $id;
        
    }
    
    /**
     * シフトグループ履歴取得
     *
     * @param mixed $duty_yyyy 年
     * @param mixed $duty_mm 月
     * @param mixed $duty_dd 日
     * @return mixed $arr_group_id_history[emp_id]、職員IDをキーとした、シフトグループ履歴の配列
     *
     */
    function get_duty_shift_staff_history($duty_yyyy, $duty_mm, $duty_dd) {
    
        //基準日以降の最も近い履歴を取得し設定、ない場合は未設定となり、呼出元のduty_shift_staffのシフトグループを使用
        $wk_ymd = $duty_yyyy.sprintf("-%02d-%02d 00:00:00", $duty_mm, $duty_dd);
        //履歴には変更前のIDがレコードに登録されている
        $sql = "
                select a.emp_id, a.group_id from duty_shift_staff_history a
                inner join (
                select e.emp_id, min(e.duty_shift_staff_history_id) as min_id from duty_shift_staff_history e   
                where e.histdate >= '$wk_ymd' group by e.emp_id ) as b
                on a.emp_id = b.emp_id and a.duty_shift_staff_history_id = b.min_id order by a.emp_id
                ";
        
        $sel = select_from_table($this->_db_con, $sql, "", $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $arr_group_id_history = array();
        while ($row = pg_fetch_array($sel)) {
            $emp_id = $row["emp_id"];
            $arr_group_id_history[$emp_id] = $row["group_id"];
        }
        return $arr_group_id_history;
    }
}