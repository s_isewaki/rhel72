<?php
// settings.php
// 設定情報を格納するファイルです。

//年次休暇簿の文言を変更する場合の設定 
$kintai_vacation_paid_hol_title = "年次有給休暇簿";

//休暇簿を表示する場合の設定 "t" or "f" 初期値:"f"
$kintai_vacation_disp_flg = "f";

//端数処理 1:行わない 2:切り上げ 3:切捨て (月集計CSVの遅刻早退控除の時間が対象です)
$kintai_late_fraction = "2";
//端数処理の分（画面と同じコード）"A":5 "B":10 "1":15 "C":20 "2":30 "D":40 "E":45 "3":60
$kintai_late_fraction_min = "2";

//休暇申請テンプレートの表示設定
//時間有休 初期値:"f"
$kintai_paid_hol_hour_flg = "f";
//年次休暇簿表示ボタンの有無 初期値:"t"
$kintai_paid_hol_btn_disp_flg = "t";
//有休残の表示有無 初期値:"f"
$kintai_paid_remain_disp_flg = "f";
$kintai_paid_remain_title = "年次有給休暇残日数";

//日数を計算の表示有無 初期値:"f"
$kintai_calc_days_flg = "f";

//時間有給休暇として使用する所定労働時間
$paid_hol_spec_time = "0800";

?>