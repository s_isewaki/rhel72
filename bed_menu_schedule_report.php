<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 病床管理 | 入退院カレンダー | 病棟別空床状況報告</title>
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_ward_div.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 診療科登録権限を取得
$section_admin_auth = check_authority($session, 28, $fname);

// 病棟登録権限を取得
$ward_admin_auth = check_authority($session, 21, $fname);

// 表示期間の設定
$dates = array();
for ($i = 0; $i <= 6; $i++) {
	$dates[] = date("Ymd", strtotime("+$i days"));
}
$start_date = $dates[0];
$end_date = $dates[6];

// データベースに接続
$con = connect2db($fname);

// 入院患者リストを作成
$sql = "select i.bldg_cd, i.ward_cd, i.ptrm_room_no, p.ptif_sex, i.inpt_short_stay from inptmst i inner join ptifmst p on p.ptif_id = i.ptif_id";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$inpatients = array();
while ($row = pg_fetch_array($sel)) {
	$inpatients[$row["bldg_cd"]][$row["ward_cd"]][$row["ptrm_room_no"]][] = array(
		"sex" => $row["ptif_sex"], "short_stay" => $row["inpt_short_stay"]
	);
}

// 期間内在院患者リストを作成
$inpatients_0000 = array();
$sel = get_patient_list($con, $start_date, $fname);
while ($row = pg_fetch_array($sel)) {
	$tmp_out_tm = ($row["pt_type"] == "in") ? "9999" : $row["inpt_out_tm"];

	// 当日0時直後の移転確定情報を取得
	$sql_move = "select m.from_bldg_cd, m.from_ward_cd, m.from_ptrm_room_no from inptmove m";
	$cond_move = "where m.ptif_id = '{$row["ptif_id"]}' and m.move_cfm_flg = 't' and m.move_dt = '$start_date' and m.move_tm < '$tmp_out_tm' order by m.move_tm limit 1";
	$sel_move = select_from_table($con, $sql_move, $cond_move, $fname);
	if ($sel_move == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	if (pg_num_rows($sel_move) > 0) {
		$tmp_bldg_cd = pg_fetch_result($sel_move, 0, "from_bldg_cd");
		$tmp_ward_cd = pg_fetch_result($sel_move, 0, "from_ward_cd");
		$tmp_ptrm_room_no = pg_fetch_result($sel_move, 0, "from_ptrm_room_no");
	} else {
		$tmp_bldg_cd = $row["bldg_cd"];
		$tmp_ward_cd = $row["ward_cd"];
		$tmp_ptrm_room_no = $row["ptrm_room_no"];
	}
	$inpatients_0000[$tmp_bldg_cd][$tmp_ward_cd][$tmp_ptrm_room_no][] = array(
		"sex" => $row["ptif_sex"], "short_stay" => $row["inpt_short_stay"]
	);
}

// 病棟区分リストを作成
$ward_divs = get_ward_div($con, $fname, true);
foreach ($ward_divs as $ward_div => $ward_div_name) {
	$ward_divs[$ward_div] = array(
		"div_name" => $ward_div_name,
		"bed_max" => 0,
		"bed_cur" => 0,
		"wards" => array()
	);
}

// 病棟区分リストに病棟リストを追加
$sql = "select w.bldg_cd, w.ward_cd, w.ward_name, w.ward_type from wdmst w";
$cond = "where w.ward_del_flg = 'f' and exists (select * from bldgmst b where b.bldg_cd = w.bldg_cd and b.bldg_del_flg = 'f') order by w.bldg_cd, w.ward_cd";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
while ($row = pg_fetch_array($sel)) {
	if (!isset($ward_divs[$row["ward_type"]])) continue;
	$ward_divs[$row["ward_type"]]["wards"]["{$row["bldg_cd"]}-{$row["ward_cd"]}"] = array(
		"data_type" => "ward",
		"ward_name" => $row["ward_name"],
		"bed_max" => 0,
		"bed_cur" => 0,
		"ptrms" => array()
	);
}

// 病棟区分リストに病室リストを追加
$sql = "select r.bldg_cd, r.ward_cd, r.ptrm_room_no, w.ward_name, r.ptrm_name, w.ward_type, r.ward_type as ptrm_ward_type, r.ptrm_bed_max, r.ptrm_bed_cur, r.ptrm_type from ptrmmst r inner join wdmst w on w.bldg_cd = r.bldg_cd and w.ward_cd = r.ward_cd";
$cond = "where r.ptrm_del_flg = 'f' and w.ward_del_flg = 'f' and exists (select * from bldgmst b where b.bldg_cd = w.bldg_cd and b.bldg_del_flg = 'f') order by r.bldg_cd, r.ward_cd, r.ptrm_room_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
while ($row = pg_fetch_array($sel)) {
	$ward_type = $row["ward_type"];
	$ptrm_ward_type = $row["ptrm_ward_type"];
	$ptrm_bed_max = intval($row["ptrm_bed_max"]);
	$ptrm_bed_cur = intval($row["ptrm_bed_cur"]);

	// 病室に病棟区分が設定されていない場合
	if ($ptrm_ward_type == 0) {
		if (!isset($ward_divs[$ward_type])) continue;
		$key = "{$row["bldg_cd"]}-{$row["ward_cd"]}";
		$ward_divs[$ward_type]["wards"][$key]["ptrms"][$row["ptrm_room_no"]] = array(
			"bed_cur" => $ptrm_bed_cur,
			"ptrm_type" => $row["ptrm_type"]
		);
		$ward_divs[$ward_type]["wards"][$key]["bed_max"] += $ptrm_bed_max;
		$ward_divs[$ward_type]["wards"][$key]["bed_cur"] += $ptrm_bed_cur;
		$ward_divs[$ward_type]["bed_max"] += $ptrm_bed_max;
		$ward_divs[$ward_type]["bed_cur"] += $ptrm_bed_cur;

	// 病室に病棟区分が設定されている場合
	} else {
		if (!isset($ward_divs[$ptrm_ward_type])) continue;
		$ward_divs[$ptrm_ward_type]["wards"]["{$row["bldg_cd"]}-{$row["ward_cd"]}-{$row["ptrm_room_no"]}"] = array(
			"data_type" => "ptrm",
			"ward_name" => $row["ward_name"] . $row["ptrm_name"],
			"bed_max" => $ptrm_bed_max,
			"bed_cur" => $ptrm_bed_cur,
			"ptrm_type" => $row["ptrm_type"]
		);
		$ward_divs[$ptrm_ward_type]["bed_max"] += $ptrm_bed_max;
		$ward_divs[$ptrm_ward_type]["bed_cur"] += $ptrm_bed_cur;
	}
}

// 入院予定患者リストを作成
$sql = "select i.bldg_cd, i.ward_cd, i.ptrm_room_no, p.ptif_sex, i.inpt_short_stay, i.inpt_in_res_dt from inptres i inner join ptifmst p on p.ptif_id = i.ptif_id";
$cond = "where i.inpt_in_res_dt between '$start_date' and '$end_date'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$in_res_inpatients = array();
while ($row = pg_fetch_array($sel)) {
	$in_res_inpatients[$row["inpt_in_res_dt"]][$row["bldg_cd"]][$row["ward_cd"]][$row["ptrm_room_no"]][] = array(
		"sex" => $row["ptif_sex"],
		"short_stay" => $row["inpt_short_stay"]
	);
}

// 退院予定患者リストを作成
$sql = "select i.bldg_cd, i.ward_cd, i.ptrm_room_no, p.ptif_sex, i.inpt_short_stay, i.inpt_out_res_dt from inptmst i inner join ptifmst p on p.ptif_id = i.ptif_id";
$cond = "where i.inpt_out_res_dt between '$start_date' and '$end_date'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$out_res_inpatients = array();
while ($row = pg_fetch_array($sel)) {
	$out_res_inpatients[$row["inpt_out_res_dt"]][$row["bldg_cd"]][$row["ward_cd"]][$row["ptrm_room_no"]][] = array(
		"sex" => $row["ptif_sex"],
		"short_stay" => $row["inpt_short_stay"]
	);
}

// 転床予定リストを作成
$sql = "select move_dt, from_bldg_cd, from_ward_cd, from_ptrm_room_no, to_bldg_cd, to_ward_cd, to_ptrm_room_no from inptmove";
//$cond = "where move_dt between'$start_date' and '$end_date' and move_cfm_flg = 'f' and move_del_flg = 'f' and not (from_bldg_cd = to_bldg_cd and from_ward_cd = from_ptrm_room_no and to_ward_cd = to_ptrm_room_no)";
$cond = "where move_dt between'$start_date' and '$end_date' and move_cfm_flg = 'f' and move_del_flg = 'f'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$movings = array();
while ($row = pg_fetch_array($sel)) {
	$movings[$row["move_dt"]][] = array(
		"from_bldg_cd" => $row["from_bldg_cd"],
		"from_ward_cd" => $row["from_ward_cd"],
		"from_ptrm_room_no" => $row["from_ptrm_room_no"],
		"to_bldg_cd" => $row["to_bldg_cd"],
		"to_ward_cd" => $row["to_ward_cd"],
		"to_ptrm_room_no" => $row["to_ptrm_room_no"]
	);
}

// 備考リストを作成
$sql = "select note_key, note_val from bed_repnote";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$notes = array();
while ($row = pg_fetch_array($sel)) {
	$notes[$row["note_key"]] = $row["note_val"];
}

unset($sel);
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function exportExcel() {
	document.excelform.submit();
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.submenu {border-collapse:collapse;}
table.submenu td {border:#5279a5 solid 1px;}
table.block {border-collapse:collapse;}
table.block td {border:#5279a5 solid 1px;}
table.block td td {border-width:0;}
td.odd {background-color:white;}
td.even {background-color:#eeeeee;}
/*tr.sum {background-color:#f5edd6;}
tr.total {background-color:#def5d6;}*/
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="bed_menu.php?session=<? echo($session); ?>"><img src="img/icon/b17.gif" width="32" height="32" border="0" alt="病床管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="bed_menu.php?session=<? echo($session); ?>"><b>病床管理</b></a></font></td>
<? if ($section_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="entity_menu.php?session=<? echo($session); ?>&entity=1"><b>管理画面へ</b></a></font></td>
<? } else if ($ward_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="building_list.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="115" align="center" bgcolor="#5279a5"><a href="bed_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>病床管理トップ</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="ward_reference.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病棟照会</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="inpatient_reserve_register_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入退院登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="bedmake_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ベットメイク</font></a></td>
<td width="5">&nbsp;</td>
<td width="125" align="center" bgcolor="#bdd1e7"><a href="bed_rate_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床利用率管理</font></a>
</td>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#bdd1e7"><a href="bed_date_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">在院日数管理</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="bed_patient_export.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者データ抽出</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<!-- メニュー -->
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="submenu">
<tr height="22">
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu.php?session=<? echo($session); ?>">お知らせ</a></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_status.php?session=<? echo($session); ?>">病棟状況一覧表</a></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入退院カレンダー</font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_inpatient_search.php?session=<? echo($session); ?>">入院患者検索</a></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_vacant_search.php?session=<? echo($session); ?>">空床検索</a></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_occupation.php?session=<? echo($session); ?>">病床利用状況</a></font></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<!-- 入退院カレンダー -->
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="block">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr height="22" bgcolor="#bdd1e7">
<td width="45"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_schedule.php?session=<? echo($session); ?>">概要</a></font></td>
<td width="90"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_schedule_in.php?session=<? echo($session); ?>">入院予定表</a></font></td>
<td width="90"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_schedule_out.php?session=<? echo($session); ?>">退院予定表</a></font></td>
<td width="150"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>病棟別空床状況報告</b></font></td>
<td width="110"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_schedule_move.php?session=<? echo($session); ?>">院内移動状況</a></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_schedule_change.php?session=<? echo($session); ?>">転院者一覧</a></font></td>
</tr>
</table>
</td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="4" alt=""><br>

<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
<td align="left" style="padding-left:10px;"><a href="javascript:void(0);" onclick="window.scrollBy(99999, 0);">→</a></td>
<td align="right" style="padding-right:10px;"><a href="javascript:void(0);" onclick="window.scrollBy(-99999, 0);">←</a></td>
<tr>
</table>
<img src="img/spacer.gif" width="1" height="2" alt=""><br>

<table border="0" cellspacing="0" cellpadding="1">
<tr height="22">
<td><input type="button" value="Excel出力" onclick="exportExcel();"></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="2" alt=""><br>

<form name="mainform" action="bed_menu_schedule_report_update.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="block">
<tr height="22" bgcolor="#f6f9ff">
<td nowrap align="center" rowspan="5" colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床・<br>病棟別</font></td>
<td nowrap align="center" rowspan="5"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(format_vertical("病棟定床数")); ?></font></td>
<td nowrap align="center" rowspan="2" colspan="7"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">現在数</font></td>
<td nowrap align="center" rowspan="2" colspan="5"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">空床内訳（0時）</font></td>
<td nowrap align="center" rowspan="5"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(format_vertical("空床・現在")); ?></font></td>
<td rowspan="5"></td>
<? foreach ($dates as $date) { ?>
<td nowrap align="center" colspan="5"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(format_date($date)); ?></font></td>
<? } ?>
<td nowrap align="center" rowspan="2" colspan="5"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">合計</font></td>
<td nowrap align="center" rowspan="5"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(format_vertical("一週間後空床")); ?></font></td>
<td nowrap align="center" rowspan="5">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">備考</font>
</td>
<td nowrap align="center" rowspan="5"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病棟名</font></td>
</tr>
<tr height="22" bgcolor="#f6f9ff">
<? foreach ($dates as $date) { ?>
<td nowrap align="center" colspan="5"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(get_weekday($date)); ?></font></td>
<? } ?>
</tr>
<tr height="22" bgcolor="#f6f9ff">
<td nowrap align="center" colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">男性</font></td>
<td nowrap align="center" colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">女性</font></td>
<td nowrap align="center" colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">合計</font></td>
<td nowrap align="center" rowspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床<br>利用率</font></td>
<td nowrap align="center" rowspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(format_vertical("男性")); ?></font></td>
<td nowrap align="center" rowspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(format_vertical("女性")); ?></font></td>
<td nowrap align="center" rowspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(format_vertical("個室")); ?></font></td>
<td nowrap align="center" rowspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(format_vertical("二人部屋")); ?></font></td>
<td nowrap align="center" rowspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(format_vertical("合計")); ?></font></td>
<? for ($i = 1; $i <= 8; $i++) { ?>
<td nowrap align="center" colspan="5"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院（上段）<br>退院（下段）</font></td>
<? } ?>
</tr>
<tr height="22" bgcolor="#f6f9ff">
<? for ($i = 1; $i <= 3; $i++) { ?>
<td nowrap align="center" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">長期</font></td>
<td nowrap align="center" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">短期</font></td>
<? } ?>
<? for ($i = 1; $i <= 8; $i++) { ?>
<td nowrap align="center" colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">男性</font></td>
<td nowrap align="center" colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">女性</font></td>
<td nowrap align="center" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(format_vertical("転入出")); ?></font></td>
<? } ?>
</tr>
<tr height="22" bgcolor="#f6f9ff">
<? for ($i = 1; $i <= 16; $i++) { ?>
<td nowrap align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">長</font></td>
<td nowrap align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">短</font></td>
<? } ?>
</tr>
<?
$total_bed_max = 0;
$total_bed_cur = 0;
$total_male_long = 0;
$total_male_short = 0;
$total_female_long = 0;
$total_female_short = 0;
$total_vacancy_male = 0;
$total_vacancy_female = 0;
$total_vacancy_single = 0;
$total_vacancy_double = 0;
$total_vacancy_current = 0;
foreach ($dates as $date) {
	$total_in_res_male_long[$date] = 0;
	$total_in_res_male_short[$date] = 0;
	$total_in_res_female_long[$date] = 0;
	$total_in_res_female_short[$date] = 0;
	$total_in_res_moving[$date] = 0;
	$total_out_res_male_long[$date] = 0;
	$total_out_res_male_short[$date] = 0;
	$total_out_res_female_long[$date] = 0;
	$total_out_res_female_short[$date] = 0;
	$total_out_res_moving[$date] = 0;
}

foreach ($ward_divs as $ward_div => $ward_div_detail) {
	if ($ward_div_detail["bed_cur"] == 0) continue;

	$wards = $ward_div_detail["wards"];
	$ward_count = count($wards);

	// 病棟と病室が混在しているのでコード順に並べ替える
	uksort($wards, "sort_by_ward_key");

	$sum_male_long = 0;
	$sum_male_short = 0;
	$sum_female_long = 0;
	$sum_female_short = 0;
	$sum_vacancy_male = 0;
	$sum_vacancy_female = 0;
	$sum_vacancy_single = 0;
	$sum_vacancy_double = 0;
	$sum_vacancy_current = 0;
	foreach ($dates as $date) {
		$sum_in_res_male_long[$date] = 0;
		$sum_in_res_male_short[$date] = 0;
		$sum_in_res_female_long[$date] = 0;
		$sum_in_res_female_short[$date] = 0;
		$sum_in_res_moving[$date] = 0;
		$sum_out_res_male_long[$date] = 0;
		$sum_out_res_male_short[$date] = 0;
		$sum_out_res_female_long[$date] = 0;
		$sum_out_res_female_short[$date] = 0;
		$sum_out_res_moving[$date] = 0;
	}
?>
<tr height="22">
<td nowrap align="center" rowspan="<? echo($ward_count * 2); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(format_vertical($ward_div_detail["div_name"])); ?></font></td>
<?
	$first_ward = true;
	$td_class = "odd";
	foreach ($wards as $ward_key => $ward) {
		if (!$first_ward) {
?>
<tr height="22">
<?
		} else {
			$first_ward = false;
		}

		list($male_long, $male_short, $female_long, $female_short) = cale_inpatient($ward_key, $ward, $inpatients);
		$sum_male_long += $male_long;
		$sum_male_short += $male_short;
		$sum_female_long += $female_long;
		$sum_female_short += $female_short;

		list($vacancy_male, $vacancy_female, $vacancy_single, $vacancy_double) = calc_vacancy($ward_key, $ward, $inpatients_0000);
		$sum_vacancy_male += $vacancy_male;
		$sum_vacancy_female += $vacancy_female;
		$sum_vacancy_single += $vacancy_single;
		$sum_vacancy_double += $vacancy_double;

		$vacancy_current = calc_vacancy_current($ward_key, $ward, $inpatients);
		$sum_vacancy_current += $vacancy_current;

		foreach ($dates as $date) {
			list(
				$in_res_male_long[$date],
				$in_res_male_short[$date],
				$in_res_female_long[$date],
				$in_res_female_short[$date],
				$in_res_moving[$date],
				$out_res_male_long[$date],
				$out_res_male_short[$date],
				$out_res_female_long[$date],
				$out_res_female_short[$date],
				$out_res_moving[$date]
			) = calc_reservation($ward_key, $ward, $in_res_inpatients[$date], $out_res_inpatients[$date], $movings[$date]);

			$sum_in_res_male_long[$date] += $in_res_male_long[$date];
			$sum_in_res_male_short[$date] += $in_res_male_short[$date];
			$sum_in_res_female_long[$date] += $in_res_female_long[$date];
			$sum_in_res_female_short[$date] += $in_res_female_short[$date];
			$sum_in_res_moving[$date] += $in_res_moving[$date];
			$sum_out_res_male_long[$date] += $out_res_male_long[$date];
			$sum_out_res_male_short[$date] += $out_res_male_short[$date];
			$sum_out_res_female_long[$date] += $out_res_female_long[$date];
			$sum_out_res_female_short[$date] += $out_res_female_short[$date];
			$sum_out_res_moving[$date] += $out_res_moving[$date];
		}

		$vacancy = $vacancy_male + $vacancy_female + $vacancy_single + $vacancy_double;
		$week_in_res_male_long = array_sum($in_res_male_long);
		$week_in_res_male_short = array_sum($in_res_male_short);
		$week_in_res_female_long = array_sum($in_res_female_long);
		$week_in_res_female_short = array_sum($in_res_female_short);
		$week_in_res_moving = array_sum($in_res_moving);
		$week_out_res_male_long = array_sum($out_res_male_long);
		$week_out_res_male_short = array_sum($out_res_male_short);
		$week_out_res_female_long = array_sum($out_res_female_long);
		$week_out_res_female_short = array_sum($out_res_female_short);
		$week_out_res_moving = array_sum($out_res_moving);
?>
<td nowrap rowspan="2" class="<? echo($td_class); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($ward["ward_name"]); ?></font></td>
<td nowrap align="right" rowspan="2" class="<? echo($td_class); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(format_bed_count($ward["bed_max"], $ward["bed_cur"])); ?></font></td>
<td nowrap align="right" rowspan="2" class="<? echo($td_class); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? write_number_or_empty($male_long); ?></font></td>
<td nowrap align="right" rowspan="2" class="<? echo($td_class); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? write_number_or_empty($male_short); ?></font></td>
<td nowrap align="right" rowspan="2" class="<? echo($td_class); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? write_number_or_empty($female_long); ?></font></td>
<td nowrap align="right" rowspan="2" class="<? echo($td_class); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? write_number_or_empty($female_short); ?></font></td>
<td nowrap align="right" rowspan="2" class="<? echo($td_class); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($male_long + $female_long); ?></font></td>
<td nowrap align="right" rowspan="2" class="<? echo($td_class); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($male_short + $female_short); ?></font></td>
<td nowrap align="right" rowspan="2" class="<? echo($td_class); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(format_bed_rate($ward["bed_cur"], ($male_long + $female_long + $male_short + $female_short))); ?></font></td>
<td nowrap align="right" rowspan="2" class="<? echo($td_class); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? write_number_or_empty($vacancy_male); ?></font></td>
<td nowrap align="right" rowspan="2" class="<? echo($td_class); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? write_number_or_empty($vacancy_female); ?></font></td>
<td nowrap align="right" rowspan="2" class="<? echo($td_class); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? write_number_or_empty($vacancy_single); ?></font></td>
<td nowrap align="right" rowspan="2" class="<? echo($td_class); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? write_number_or_empty($vacancy_double); ?></font></td>
<td nowrap align="right" rowspan="2" class="<? echo($td_class); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($vacancy); ?></font></td>
<td nowrap align="right" rowspan="2" class="<? echo($td_class); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($vacancy_current); ?></font></td>
<td nowrap align="center" class="<? echo($td_class); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入</font></td>
<?
		foreach ($dates as $date) {
?>
<td nowrap align="right" class="<? echo($td_class); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? write_number_or_empty($in_res_male_long[$date]); ?></font></td>
<td nowrap align="right" class="<? echo($td_class); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? write_number_or_empty($in_res_male_short[$date]); ?></font></td>
<td nowrap align="right" class="<? echo($td_class); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? write_number_or_empty($in_res_female_long[$date]); ?></font></td>
<td nowrap align="right" class="<? echo($td_class); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? write_number_or_empty($in_res_female_short[$date]); ?></font></td>
<td nowrap align="right" class="<? echo($td_class); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? write_number_or_empty($in_res_moving[$date]); ?></font></td>
<?
		}
?>
<td nowrap align="right" class="<? echo($td_class); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? write_number_or_empty($week_in_res_male_long); ?></font></td>
<td nowrap align="right" class="<? echo($td_class); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? write_number_or_empty($week_in_res_male_short); ?></font></td>
<td nowrap align="right" class="<? echo($td_class); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? write_number_or_empty($week_in_res_female_long); ?></font></td>
<td nowrap align="right" class="<? echo($td_class); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? write_number_or_empty($week_in_res_female_short); ?></font></td>
<td nowrap align="right" class="<? echo($td_class); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? write_number_or_empty($week_in_res_moving); ?></font></td>
<td nowrap align="right" rowspan="2" class="<? echo($td_class); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($vacancy_current - ($week_in_res_male_long + $week_in_res_male_short + $week_in_res_female_long + $week_in_res_female_short + $week_in_res_moving) + ($week_out_res_male_long + $week_out_res_male_short + $week_out_res_female_long + $week_out_res_female_short + $week_out_res_moving)); ?></font></td>
<td nowrap class="<? echo($td_class); ?>">
<input type="button" value="更新" onclick="window.open('bed_menu_schedule_report_detail.php?session=<? echo($session); ?>&key=ward1_<? echo($ward_key); ?>', 'newwin', 'width=640,height=480,scrollbars=yes');">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($notes["ward1_$ward_key"]); ?></font>
</td>
<td nowrap rowspan="2" class="<? echo($td_class); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($ward["ward_name"]); ?></font></td>
</tr>
<tr height="22">
<td nowrap align="center" class="<? echo($td_class); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退</font></td>
<?
		foreach ($dates as $date) {
?>
<td nowrap align="right" class="<? echo($td_class); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? write_number_or_empty($out_res_male_long[$date]); ?></font></td>
<td nowrap align="right" class="<? echo($td_class); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? write_number_or_empty($out_res_male_short[$date]); ?></font></td>
<td nowrap align="right" class="<? echo($td_class); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? write_number_or_empty($out_res_female_long[$date]); ?></font></td>
<td nowrap align="right" class="<? echo($td_class); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? write_number_or_empty($out_res_female_short[$date]); ?></font></td>
<td nowrap align="right" class="<? echo($td_class); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? write_number_or_empty($out_res_moving[$date]); ?></font></td>
<?
		}
?>
<td nowrap align="right" class="<? echo($td_class); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? write_number_or_empty($week_out_res_male_long); ?></font></td>
<td nowrap align="right" class="<? echo($td_class); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? write_number_or_empty($week_out_res_male_short); ?></font></td>
<td nowrap align="right" class="<? echo($td_class); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? write_number_or_empty($week_out_res_female_long); ?></font></td>
<td nowrap align="right" class="<? echo($td_class); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? write_number_or_empty($week_out_res_female_short); ?></font></td>
<td nowrap align="right" class="<? echo($td_class); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? write_number_or_empty($week_out_res_moving); ?></font></td>
<td nowrap class="<? echo($td_class); ?>">
<input type="button" value="更新" onclick="window.open('bed_menu_schedule_report_detail.php?session=<? echo($session); ?>&key=ward2_<? echo($ward_key); ?>', 'newwin', 'width=640,height=480,scrollbars=yes');">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($notes["ward2_$ward_key"]); ?></font>
</td>
</tr>
<?
		$td_class = ($td_class != "odd") ? "odd" : "even";
	}

	$sum_vacancy = $sum_vacancy_male + $sum_vacancy_female + $sum_vacancy_single + $sum_vacancy_double;
	$week_sum_in_res_male_long = array_sum($sum_in_res_male_long);
	$week_sum_in_res_male_short = array_sum($sum_in_res_male_short);
	$week_sum_in_res_female_long = array_sum($sum_in_res_female_long);
	$week_sum_in_res_female_short = array_sum($sum_in_res_female_short);
	$week_sum_in_res_moving = array_sum($sum_in_res_moving);
	$week_sum_out_res_male_long = array_sum($sum_out_res_male_long);
	$week_sum_out_res_male_short = array_sum($sum_out_res_male_short);
	$week_sum_out_res_female_long = array_sum($sum_out_res_female_long);
	$week_sum_out_res_female_short = array_sum($sum_out_res_female_short);
	$week_sum_out_res_moving = array_sum($sum_out_res_moving);
?>
<tr height="22" class="sum">
<td nowrap align="center" colspan="2" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b>小計</b></font></td>
<td nowrap align="right" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? echo(format_bed_count($ward_div_detail["bed_max"], $ward_div_detail["bed_cur"])); ?></b></font></td>
<td nowrap align="right" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? echo($sum_male_long); ?></b></font></td>
<td nowrap align="right" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? echo($sum_male_short); ?></b></font></td>
<td nowrap align="right" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? echo($sum_female_long); ?></b></font></td>
<td nowrap align="right" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? echo($sum_female_short); ?></b></font></td>
<td nowrap align="right" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? echo($sum_male_long + $sum_female_long); ?></b></font></td>
<td nowrap align="right" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? echo($sum_male_short + $sum_female_short); ?></b></font></td>
<td nowrap align="right" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? echo(format_bed_rate($ward_div_detail["bed_cur"], ($sum_male_long + $sum_female_long + $sum_male_short + $sum_female_short))); ?></b></font></td>
<td nowrap align="right" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? echo($sum_vacancy_male); ?></b></font></td>
<td nowrap align="right" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? echo($sum_vacancy_female); ?></b></font></td>
<td nowrap align="right" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? echo($sum_vacancy_single); ?></b></font></td>
<td nowrap align="right" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? echo($sum_vacancy_double); ?></b></font></td>
<td nowrap align="right" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? echo($sum_vacancy); ?></b></font></td>
<td nowrap align="right" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? echo($sum_vacancy_current); ?></b></font></td>
<td nowrap align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b>入</b></font></td>
<?
		foreach ($dates as $date) {
?>
<td nowrap align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? write_number_or_empty($sum_in_res_male_long[$date]); ?></b></font></td>
<td nowrap align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? write_number_or_empty($sum_in_res_male_short[$date]); ?></b></font></td>
<td nowrap align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? write_number_or_empty($sum_in_res_female_long[$date]); ?></b></font></td>
<td nowrap align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? write_number_or_empty($sum_in_res_female_short[$date]); ?></b></font></td>
<td nowrap align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? write_number_or_empty($sum_in_res_moving[$date]); ?></b></font></td>
<?
		}
?>
<td nowrap align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? write_number_or_empty($week_sum_in_res_male_long); ?></b></font></td>
<td nowrap align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? write_number_or_empty($week_sum_in_res_male_short); ?></b></font></td>
<td nowrap align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? write_number_or_empty($week_sum_in_res_female_long); ?></b></font></td>
<td nowrap align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? write_number_or_empty($week_sum_in_res_female_short); ?></b></font></td>
<td nowrap align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? write_number_or_empty($week_sum_in_res_moving); ?></b></font></td>
<td nowrap align="right" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? echo($sum_vacancy_current - ($week_sum_in_res_male_long + $week_sum_in_res_male_short + $week_sum_in_res_female_long + $week_sum_in_res_female_short + $week_sum_in_res_moving) + ($week_sum_out_res_male_long + $week_sum_out_res_male_short + $week_sum_out_res_female_long + $week_sum_out_res_female_short + $week_sum_out_res_moving)); ?></b></font></td>
<td nowrap>
<input type="button" value="更新" onclick="window.open('bed_menu_schedule_report_detail.php?session=<? echo($session); ?>&key=sum1_<? echo($ward_div); ?>', 'newwin', 'width=640,height=480,scrollbars=yes');">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($notes["sum1_$ward_div"]); ?></font>
</td>
<td nowrap align="center" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b>小計</b></font></td>
</tr>
<tr height="22" class="sum">
<td nowrap align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b>退</b></font></td>
<?
		foreach ($dates as $date) {
?>
<td nowrap align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? write_number_or_empty($sum_out_res_male_long[$date]); ?></b></font></td>
<td nowrap align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? write_number_or_empty($sum_out_res_male_short[$date]); ?></b></font></td>
<td nowrap align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? write_number_or_empty($sum_out_res_female_long[$date]); ?></b></font></td>
<td nowrap align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? write_number_or_empty($sum_out_res_female_short[$date]); ?></b></font></td>
<td nowrap align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? write_number_or_empty($sum_out_res_moving[$date]); ?></b></font></td>
<?
		}
?>
<td nowrap align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? write_number_or_empty($week_sum_out_res_male_long); ?></b></font></td>
<td nowrap align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? write_number_or_empty($week_sum_out_res_male_short); ?></b></font></td>
<td nowrap align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? write_number_or_empty($week_sum_out_res_female_long); ?></b></font></td>
<td nowrap align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? write_number_or_empty($week_sum_out_res_female_short); ?></b></font></td>
<td nowrap align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? write_number_or_empty($week_sum_out_res_moving); ?></b></font></td>
<td nowrap>
<input type="button" value="更新" onclick="window.open('bed_menu_schedule_report_detail.php?session=<? echo($session); ?>&key=sum2_<? echo($ward_div); ?>', 'newwin', 'width=640,height=480,scrollbars=yes');">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($notes["sum2_$ward_div"]); ?></font>
</td>
</tr>
<?
	$total_bed_max += $ward_div_detail["bed_max"];
	$total_bed_cur += $ward_div_detail["bed_cur"];
	$total_male_long += $sum_male_long;
	$total_male_short += $sum_male_short;
	$total_female_long += $sum_female_long;
	$total_female_short += $sum_female_short;
	$total_vacancy_male += $sum_vacancy_male;
	$total_vacancy_female += $sum_vacancy_female;
	$total_vacancy_single += $sum_vacancy_single;
	$total_vacancy_double += $sum_vacancy_double;
	$total_vacancy_current += $sum_vacancy_current;
	foreach ($dates as $date) {
		$total_in_res_male_long[$date] += $sum_in_res_male_long[$date];
		$total_in_res_male_short[$date] += $sum_in_res_male_short[$date];
		$total_in_res_female_long[$date] += $sum_in_res_female_long[$date];
		$total_in_res_female_short[$date] += $sum_in_res_female_short[$date];
		$total_in_res_moving[$date] += $sum_in_res_moving[$date];
		$total_out_res_male_long[$date] += $sum_out_res_male_long[$date];
		$total_out_res_male_short[$date] += $sum_out_res_male_short[$date];
		$total_out_res_female_long[$date] += $sum_out_res_female_long[$date];
		$total_out_res_female_short[$date] += $sum_out_res_female_short[$date];
		$total_out_res_moving[$date] += $sum_out_res_moving[$date];
	}
}

$total_vacancy = $total_vacancy_male + $total_vacancy_female + $total_vacancy_single + $total_vacancy_double;
$week_total_in_res_male_long = array_sum($total_in_res_male_long);
$week_total_in_res_male_short = array_sum($total_in_res_male_short);
$week_total_in_res_female_long = array_sum($total_in_res_female_long);
$week_total_in_res_female_short = array_sum($total_in_res_female_short);
$week_total_in_res_moving = array_sum($total_in_res_moving);
$week_total_out_res_male_long = array_sum($total_out_res_male_long);
$week_total_out_res_male_short = array_sum($total_out_res_male_short);
$week_total_out_res_female_long = array_sum($total_out_res_female_long);
$week_total_out_res_female_short = array_sum($total_out_res_female_short);
$week_total_out_res_moving = array_sum($total_out_res_moving);
?>
<tr height="22" class="total">
<td nowrap align="center" colspan="2" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b>合計</b></font></td>
<td nowrap align="right" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? echo(format_bed_count($total_bed_max, $total_bed_cur)); ?></b></font></td>
<td nowrap align="right" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? echo($total_male_long); ?></b></font></td>
<td nowrap align="right" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? echo($total_male_short); ?></b></font></td>
<td nowrap align="right" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? echo($total_female_long); ?></b></font></td>
<td nowrap align="right" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? echo($total_female_short); ?></b></font></td>
<td nowrap align="right" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? echo($total_male_long + $total_female_long); ?></b></font></td>
<td nowrap align="right" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? echo($total_male_short + $total_female_short); ?></b></font></td>
<td nowrap align="right" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? echo(format_bed_rate($total_bed_cur, ($total_male_long + $total_female_long + $total_male_short + $total_female_short))); ?></b></font></td>
<td nowrap align="right" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? echo($total_vacancy_male); ?></b></font></td>
<td nowrap align="right" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? echo($total_vacancy_female); ?></b></font></td>
<td nowrap align="right" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? echo($total_vacancy_single); ?></b></font></td>
<td nowrap align="right" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? echo($total_vacancy_double); ?></b></font></td>
<td nowrap align="right" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? echo($total_vacancy); ?></b></font></td>
<td nowrap align="right" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? echo($total_vacancy_current); ?></b></font></td>
<td nowrap align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b>入計</b></font></td>
<?
foreach ($dates as $date) {
?>
<td nowrap align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? write_number_or_empty($total_in_res_male_long[$date]); ?></b></font></td>
<td nowrap align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? write_number_or_empty($total_in_res_male_short[$date]); ?></b></font></td>
<td nowrap align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? write_number_or_empty($total_in_res_female_long[$date]); ?></b></font></td>
<td nowrap align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? write_number_or_empty($total_in_res_female_short[$date]); ?></b></font></td>
<td nowrap align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? write_number_or_empty($total_in_res_moving[$date]); ?></b></font></td>
<?
}
?>
<td nowrap align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? write_number_or_empty($week_total_in_res_male_long); ?></b></font></td>
<td nowrap align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? write_number_or_empty($week_total_in_res_male_short); ?></b></font></td>
<td nowrap align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? write_number_or_empty($week_total_in_res_female_long); ?></b></font></td>
<td nowrap align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? write_number_or_empty($week_total_in_res_female_short); ?></b></font></td>
<td nowrap align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? write_number_or_empty($week_total_in_res_moving); ?></b></font></td>
<td nowrap align="right" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? echo($total_vacancy_current - ($week_total_in_res_male_long + $week_total_in_res_male_short + $week_total_in_res_female_long + $week_total_in_res_female_short + $week_total_in_res_moving) + ($week_total_out_res_male_long + $week_total_out_res_male_short + $week_total_out_res_female_long + $week_total_out_res_female_short + $week_total_out_res_moving)); ?></b></font></td>
<td nowrap>
<input type="button" value="更新" onclick="window.open('bed_menu_schedule_report_detail.php?session=<? echo($session); ?>&key=total1', 'newwin', 'width=640,height=480,scrollbars=yes');">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($notes["total1"]); ?></font>
</td>
<td nowrap align="center" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b>合計</b></font></td>
</tr>
<tr height="22" class="total">
<td nowrap align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b>退計</b></font></td>
<?
foreach ($dates as $date) {
?>
<td nowrap align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? write_number_or_empty($total_out_res_male_long[$date]); ?></b></font></td>
<td nowrap align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? write_number_or_empty($total_out_res_male_short[$date]); ?></b></font></td>
<td nowrap align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? write_number_or_empty($total_out_res_female_long[$date]); ?></b></font></td>
<td nowrap align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? write_number_or_empty($total_out_res_female_short[$date]); ?></b></font></td>
<td nowrap align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? write_number_or_empty($total_out_res_moving[$date]); ?></b></font></td>
<?
}
?>
<td nowrap align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? write_number_or_empty($week_total_out_res_male_long); ?></b></font></td>
<td nowrap align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? write_number_or_empty($week_total_out_res_male_short); ?></b></font></td>
<td nowrap align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? write_number_or_empty($week_total_out_res_female_long); ?></b></font></td>
<td nowrap align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? write_number_or_empty($week_total_out_res_female_short); ?></b></font></td>
<td nowrap align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b><? write_number_or_empty($week_total_out_res_moving); ?></b></font></td>
<td nowrap>
<input type="button" value="更新" onclick="window.open('bed_menu_schedule_report_detail.php?session=<? echo($session); ?>&key=total2', 'newwin', 'width=640,height=480,scrollbars=yes');">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($notes["total2"]); ?></font>
</td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
<br>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
<td align="left" style="padding-left:10px;"><a href="javascript:void(0);" onclick="window.scrollBy(99999, 0);">→</a></td>
<td align="right" style="padding-right:10px;"><a href="javascript:void(0);" onclick="window.scrollBy(-99999, 0);">←</a></td>
<tr>
</table>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
<form name="excelform" action="bed_menu_schedule_report_excel.php" method="get" target="excelframe">
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
<iframe name="excelframe" width="0" height="0" frameborder="0"></iframe>
</html>
<?
function get_patient_list($con, $start_date, $fname) {
	$sql = " select 'in' as pt_type, i.ptif_id, i.inpt_out_tm, i.bldg_cd, i.ward_cd, i.ptrm_room_no, p.ptif_sex, i.inpt_short_stay from inptmst i inner join wdmst w on w.bldg_cd = i.bldg_cd and w.ward_cd = i.ward_cd inner join ptrmmst r on r.bldg_cd = i.bldg_cd and r.ward_cd = i.ward_cd and r.ptrm_room_no = i.ptrm_room_no inner join ptifmst p on p.ptif_id = i.ptif_id where i.inpt_in_dt is not null and i.inpt_in_dt <= '$start_date' ";
	$sql .= " union select 'out' as pt_type, i.ptif_id, i.inpt_out_tm, i.bldg_cd, i.ward_cd, i.ptrm_room_no, p.ptif_sex, i.inpt_short_stay from inpthist i inner join wdmst w on w.bldg_cd = i.bldg_cd and w.ward_cd = i.ward_cd inner join ptrmmst r on r.bldg_cd = i.bldg_cd and r.ward_cd = i.ward_cd and r.ptrm_room_no = i.ptrm_room_no inner join ptifmst p on p.ptif_id = i.ptif_id where i.inpt_in_dt <= '$start_date' and i.inpt_out_dt >= '$start_date'";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	return $sel;
}

function get_weekday($date) {
	$year = substr($date, 0, 4);
	$month = substr($date, 4, 2);
	$day = substr($date, 6, 2);

	switch (date("w", mktime(0, 0, 0, $month, $day, $year))) {
	case "0":
		return "日";
	case "1":
		return "月";
	case "2":
		return "火";
	case "3":
		return "水";
	case "4":
		return "木";
	case "5":
		return "金";
	case "6":
		return "土";
	}
}

function format_date($date) {
	return preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $date);
}

function format_vertical($str) {
	$buf = array();
	for ($i = 0, $j = mb_strlen($str); $i < $j; $i++) {
		$buf[] = mb_substr($str, $i, 1);
	}
	return join("<br>", $buf);
}

function format_bed_count($max, $cur) {
	return ($max == $cur) ? $cur : "($max)<br>$cur";
}

function sort_by_ward_key($key1, $key2) {
	list($bldg_cd1, $ward_cd1, $ptrm_room_no1) = split("-", $key1);
	list($bldg_cd2, $ward_cd2, $ptrm_room_no2) = split("-", $key2);
	if (intval($bldg_cd1) < intval($bldg_cd2)) return -1;
	if (intval($bldg_cd1) > intval($bldg_cd2)) return 1;
	if (intval($ward_cd1) < intval($ward_cd2)) return -1;
	if (intval($ward_cd1) > intval($ward_cd2)) return 1;
	if (intval($ptrm_room_no1) < intval($ptrm_room_no2)) return -1;
	if (intval($ptrm_room_no1) > intval($ptrm_room_no2)) return 1;
	return 0;
}


function cale_inpatient($ward_key, $ward, $inpatients) {
	$male_long = 0;
	$male_short = 0;
	$female_long = 0;
	$female_short = 0;

	list($bldg_cd, $ward_cd, $ptrm_room_no_list) = resolve_ptrm_list($ward_key, $ward);

	foreach ($ptrm_room_no_list as $ptrm_room_no) {
		foreach ($inpatients[$bldg_cd][$ward_cd][$ptrm_room_no] as $inpatient) {

			// 男性の場合
			if ($inpatient["sex"] == "1") {
				if ($inpatient["short_stay"] == "t") {
					$male_short++;
				} else {
					$male_long++;
				}

			// 女性の場合
			} else if ($inpatient["sex"] == "2") {
				if ($inpatient["short_stay"] == "t") {
					$female_short++;
				} else {
					$female_long++;
				}
			}
		}
	}

	return array($male_long, $male_short, $female_long, $female_short);
}

function calc_vacancy($ward_key, $ward, $inpatients) {
	$vacancy_male = 0;
	$vacancy_female = 0;
	$vacancy_single = 0;
	$vacancy_double = 0;

	switch ($ward["data_type"]) {
	case "ward":
		list($bldg_cd, $ward_cd) = split("-", $ward_key);
		$ptrms = $ward["ptrms"];
		break;
	case "ptrm":
		list($bldg_cd, $ward_cd, $ptrm_room_no) = split("-", $ward_key);
		$ptrms[$ptrm_room_no] = $ward;
		break;
	}

	foreach ($ptrms as $ptrm_room_no => $ptrm) {
		$inpatiens_in_ptrm = $inpatients[$bldg_cd][$ward_cd][$ptrm_room_no];

		// 個室の場合
		if ($ptrm["ptrm_type"] == "2") {
			$vacancy_single += (1 - count($inpatiens_in_ptrm));

		// 二人部屋の場合
		} else if ($ptrm["bed_cur"] == 2) {
			$vacancy_double += (2 - count($inpatiens_in_ptrm));

		// 三人部屋以上の場合
		} else {

			// 男性部屋かどうか判定
			$male_count = 0;
			$female_count = 0;
			foreach ($inpatiens_in_ptrm as $inpatient) {
				switch ($inpatient["sex"]) {
				case "1":
					$male_count++;
					break;
				case "2":
					$female_count++;
					break;
				}
			}
			$is_male_room = ($male_count >= $female_count);

			if ($is_male_room) {
				$vacancy_male += ($ptrm["bed_cur"] - count($inpatiens_in_ptrm));
			} else {
				$vacancy_female += ($ptrm["bed_cur"] - count($inpatiens_in_ptrm));
			}
		}
	}

	return array($vacancy_male, $vacancy_female, $vacancy_single, $vacancy_double);
}

function calc_vacancy_current($ward_key, $ward, $inpatients) {
	$vacancy = 0;

	switch ($ward["data_type"]) {
	case "ward":
		list($bldg_cd, $ward_cd) = split("-", $ward_key);
		$ptrms = $ward["ptrms"];
		break;
	case "ptrm":
		list($bldg_cd, $ward_cd, $ptrm_room_no) = split("-", $ward_key);
		$ptrms[$ptrm_room_no] = $ward;
		break;
	}

	foreach ($ptrms as $ptrm_room_no => $ptrm) {
		$inpatiens_in_ptrm = $inpatients[$bldg_cd][$ward_cd][$ptrm_room_no];

		// 個室の場合
		if ($ptrm["ptrm_type"] == "2") {
			$vacancy += (1 - count($inpatiens_in_ptrm));

		// 二人部屋の場合
		} else if ($ptrm["bed_cur"] == 2) {
			$vacancy += (2 - count($inpatiens_in_ptrm));

		// 三人部屋以上の場合
		} else {
			$vacancy += ($ptrm["bed_cur"] - count($inpatiens_in_ptrm));
		}
	}

	return $vacancy;
}

function write_number_or_empty($num) {
	if ($num == 0) return;
	echo($num);
}

function format_bed_rate($bed_cur, $inpatient_count) {
	if ($bed_cur == 0 || $inpatient_count == 0) return "0.0%";
	return sprintf("%.1f", round(($inpatient_count / $bed_cur * 100), 1)) . "%";
}

function resolve_ptrm_list($ward_key, $ward, $permit_room_undec = false) {
	switch ($ward["data_type"]) {
	case "ward":
		list($bldg_cd, $ward_cd) = split("-", $ward_key);
		if ($permit_room_undec) {
			$ptrm_room_no_list = array_merge(array(0), array_keys($ward["ptrms"]));
		} else {
			$ptrm_room_no_list = array_keys($ward["ptrms"]);
		}
		break;
	case "ptrm":
		list($bldg_cd, $ward_cd, $ptrm_room_no) = split("-", $ward_key);
		$ptrm_room_no_list = array($ptrm_room_no);
		break;
	}
	return array($bldg_cd, $ward_cd, $ptrm_room_no_list);
}

function calc_reservation($ward_key, $ward, $in_res_inpatients, $out_res_inpatients, $movings) {
	$in_res_male_long = 0;
	$in_res_male_short = 0;
	$in_res_female_long = 0;
	$in_res_female_short = 0;
	$in_res_moving = 0;
	$out_res_male_long = 0;
	$out_res_male_short = 0;
	$out_res_female_long = 0;
	$out_res_female_short = 0;
	$out_res_moving = 0;

	list($bldg_cd, $ward_cd, $ptrm_room_no_list) = resolve_ptrm_list($ward_key, $ward, true);

	foreach ($ptrm_room_no_list as $ptrm_room_no) {
		foreach ($in_res_inpatients[$bldg_cd][$ward_cd][$ptrm_room_no] as $patient) {

			// 男性の場合
			if ($patient["sex"] == "1") {
				if ($patient["short_stay"] == "t") {
					$in_res_male_short++;
				} else {
					$in_res_male_long++;
				}

			// 女性の場合
			} else if ($patient["sex"] == "2") {
				if ($patient["short_stay"] == "t") {
					$in_res_female_short++;
				} else {
					$in_res_female_long++;
				}
			}
		}

		foreach ($out_res_inpatients[$bldg_cd][$ward_cd][$ptrm_room_no] as $patient) {

			// 男性の場合
			if ($patient["sex"] == "1") {
				if ($patient["short_stay"] == "t") {
					$out_res_male_short++;
				} else {
					$out_res_male_long++;
				}

			// 女性の場合
			} else if ($patient["sex"] == "2") {
				if ($patient["short_stay"] == "t") {
					$out_res_female_short++;
				} else {
					$out_res_female_long++;
				}
			}
		}

		foreach ($movings as $moving) {
			if ($bldg_cd == $moving["to_bldg_cd"] && $ward_cd == $moving["to_ward_cd"] && $ptrm_room_no == $moving["to_ptrm_room_no"]) {
				$in_res_moving++;
			}

			if ($bldg_cd == $moving["from_bldg_cd"] && $ward_cd == $moving["from_ward_cd"] && $ptrm_room_no == $moving["from_ptrm_room_no"]) {
				$out_res_moving++;
			}
		}
	}

	return array(
		$in_res_male_long,
		$in_res_male_short,
		$in_res_female_long,
		$in_res_female_short,
		$in_res_moving,
		$out_res_male_long,
		$out_res_male_short,
		$out_res_female_long,
		$out_res_female_short,
		$out_res_moving
	);
}
?>
