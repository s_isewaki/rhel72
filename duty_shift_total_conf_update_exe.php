<!-- ************************************************************************ -->
<!-- 勤務シフト作成｜管理 |勤務集計表設定「登録」 -->
<!-- ************************************************************************ -->

<?
require_once("about_session.php");
require_once("about_authority.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
// セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
//入力チェック
if ($delay_minute != "" && !preg_match("/^[0-9]*$/", $delay_minute)) {	
	echo("<script type=\"text/javascript\">alert('遅刻回数の分には数値を入力してください。');</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
}
if ($irrg_month_time != "" && !preg_match("/^[0-9]*$/", $irrg_month_time)) {	
	echo("<script type=\"text/javascript\">alert('変則労働月間時間には数値を入力してください。');</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
}
for ($i=0; $i<$data_cnt; $i++) {
	$wk = "tracnt_$i";
	if ($$wk != "" && !preg_match("/^[0-9]*$/", $$wk)) {	
		echo("<script type=\"text/javascript\">alert('交通費該当勤務パターンの回数には数値を入力してください。');</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}
	
}
if ($parttime_split_hour != "" && ($parttime_split_hour<=0 || $parttime_split_hour>=24)) {	
	echo("<script type=\"text/javascript\">alert('勤務時間の集計には1から23の数値を入力してください。');</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
}

///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
///-----------------------------------------------------------------------------
// トランザクションを開始
///-----------------------------------------------------------------------------
pg_query($con, "begin transaction");

//勤務集計表設定
$sql = "select * from duty_shift_total_config";
$cond = "where group_id = '$group_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$num = pg_numrows($sel);

if ($num == 0) {
	$sql = "insert into duty_shift_total_config (group_id, normal_overtime_flg, delay_minute, irrg_month_time, parttime_split_hour, hol_late_night_disp_flg, adjust_time_legal_hol_flg) values (";
	$content = array($group_id, $normal_overtime_flg, $delay_minute, $irrg_month_time, $parttime_split_hour, $hol_late_night_disp_flg, $adjust_time_legal_hol_flg);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	
} else {
	$sql = "update duty_shift_total_config set";
	$set = array("normal_overtime_flg", "delay_minute", "irrg_month_time", "parttime_split_hour", "hol_late_night_disp_flg", "adjust_time_legal_hol_flg");
	$setvalue = array($normal_overtime_flg, $delay_minute, $irrg_month_time, $parttime_split_hour, $hol_late_night_disp_flg, $adjust_time_legal_hol_flg);
	$cond = "where group_id = '$group_id'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	
}

//常勤表示勤務パターン
$sql = "delete from duty_shift_total_ptn_full";
$cond = "where group_id = '$group_id'";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

//登録
for($i=1; $i<=$data_cnt;$i++) {
	//値取得
	$wk1 = "ptn1_$i";

	if ($$wk1 != "1") {
		continue;
	}
	
	$sql = "insert into duty_shift_total_ptn_full (group_id, atdptn_ptn_id) values (";
	$content = array($group_id, $i);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

//休日出勤時間から除外する勤務パターン追加 20100722
$sql = "delete from duty_shift_total_hol_except_ptn";
$cond = "where group_id = '$group_id'";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

//登録
for($i=1; $i<=$data_cnt;$i++) {
	//値取得
	$wk1 = "ptn5_$i";
	
	if ($$wk1 != "1") {
		continue;
	}
	
	$sql = "insert into duty_shift_total_hol_except_ptn (group_id, atdptn_ptn_id) values (";
	$content = array($group_id, $i);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

//常勤交通費該当勤務パターン
//delete
$sql = "delete from duty_shift_total_tra_ptn_full";
$cond = "where group_id = '$group_id'";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}


//insert
for($i=1; $i<=$data_cnt;$i++) {
	//値取得
	$wk1 = "ptn2_$i";
	$wk2 = "tracnt_$i";
	
	if ($$wk1 != "1") {
		continue;
	}
	
	$sql = "insert into duty_shift_total_tra_ptn_full (group_id, atdptn_ptn_id, cnt) values (";
	$content = array($group_id, $i, $$wk2);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

//非常勤表示勤務パターン
$sql = "delete from duty_shift_total_ptn_part";
$cond = "where group_id = '$group_id'";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

//登録
for($i=1; $i<=$data_cnt;$i++) {
	//値取得
	$wk1 = "ptn3_$i";
	
	if ($$wk1 != "1") {
		continue;
	}
	
	$sql = "insert into duty_shift_total_ptn_part (group_id, atdptn_ptn_id) values (";
	$content = array($group_id, $i);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

//非常勤交通費該当勤務パターン
$sql = "delete from duty_shift_total_tra_ptn_part";
$cond = "where group_id = '$group_id'";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

//登録
for($i=1; $i<=$data_cnt;$i++) {
	//値取得
	$wk1 = "ptn4_$i";
	
	if ($$wk1 != "1") {
		continue;
	}
	
	$sql = "insert into duty_shift_total_tra_ptn_part (group_id, atdptn_ptn_id) values (";
	$content = array($group_id, $i);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

//常勤手当
$sql = "delete from duty_shift_total_allow_full";
$cond = "where group_id = '$group_id'";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

//登録
for($i=1; $i<=$allow_max;$i++) {
	//値取得
	$wk1 = "allow1_$i";
	
	if ($$wk1 == "1") {
		$wk_disp_flg = 't';
	} else {
		$wk_disp_flg = 'f';
	}
	
	$sql = "insert into duty_shift_total_allow_full (group_id, allow_id, disp_flg) values (";
	$content = array($group_id, $i, $wk_disp_flg);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

//非常勤手当
$sql = "delete from duty_shift_total_allow_part";
$cond = "where group_id = '$group_id'";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

//登録
for($i=1; $i<=$allow_max;$i++) {
	//値取得
	$wk1 = "allow2_$i";
	
	if ($$wk1 == "1") {
		$wk_disp_flg = 't';
	} else {
		$wk_disp_flg = 'f';
	}
	
	$sql = "insert into duty_shift_total_allow_part (group_id, allow_id, disp_flg) values (";
	$content = array($group_id, $i, $wk_disp_flg);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

///-----------------------------------------------------------------------------
// トランザクションをコミット
///-----------------------------------------------------------------------------
pg_query("commit");
///-----------------------------------------------------------------------------
// データベース接続を閉じる
///-----------------------------------------------------------------------------
pg_close($con);
///-----------------------------------------------------------------------------
// 遷移元画面を再表示
///-----------------------------------------------------------------------------
echo("<script type=\"text/javascript\">location.href = 'duty_shift_total_conf.php?session=$session&group_id=$group_id';</script>");
?>


