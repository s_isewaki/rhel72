<?
///*****************************************************************************
// 勤務シフト作成 | 看護協会版届出様式「ＥＸＣＥＬ」
///*****************************************************************************

ini_set("max_execution_time", 0);
ob_start();
require_once("about_authority.php");
require_once("about_session.php");
require_once("about_postgres.php");

require_once("duty_shift_common_class.php");
require_once("duty_shift_report_common_class.php");
require_once("duty_shift_report_associate_class.php");
require_once("duty_shift_report_associate2012_class.php");
require_once("duty_shift_menu_excel_5_workshop.php");

//debug

ini_set( "log_errors", "On" );
ini_set( "error_log", "/var/www/html/comedix/phpApl.log" );

///-----------------------------------------------------------------------------
//画面名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
//セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
//DBコネクション取得
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
$obj_report = new duty_shift_report_common_class($con, $fname);
///-----------------------------------------------------------------------------
// Excelオブジェクト生成、列名配列
///-----------------------------------------------------------------------------
$excelObj = new ExcelWorkShop();

///-----------------------------------------------------------------------------
//初期値設定
///-----------------------------------------------------------------------------
$sum_night_times = array();	//「夜勤専従者及び月１６時間以下」の合計
//	$excel_flg = "1";			//ＥＸＣＥＬ表示フラグ（１：ＥＸＣＥＬ表示）
//	$download_data = "";		//ＥＸＣＥＬデータ
///-----------------------------------------------------------------------------
//現在日付の取得
///-----------------------------------------------------------------------------
$date = getdate();
$now_yyyy = $date["year"];
$now_mm = $date["mon"];
$now_dd = $date["mday"];
///-----------------------------------------------------------------------------
//表示年／月設定
///-----------------------------------------------------------------------------
if ($duty_yyyy == "") { $duty_yyyy = $now_yyyy; }
if ($duty_mm == "") { $duty_mm = $now_mm; }
$day_cnt = $obj->days_in_month($duty_yyyy, $duty_mm);
///-----------------------------------------------------------------------------
//終了年月
///-----------------------------------------------------------------------------
if ($night_start_day == "") {
	$night_start_day = "1";
}
$end_yyyy = (int)$duty_yyyy;
$end_mm = (int)$duty_mm;
$end_dd = (int)$day_cnt;
if ($recomputation_flg == "2") {
	$wk_day = (int)$night_start_day + 27;
	$tmp_time = mktime(0, 0, 0, $duty_mm, $wk_day, $duty_yyyy);
	$tmp_date = date("Ymd", $tmp_time);
	$end_yyyy = (int)substr($tmp_date,0,4);
	$end_mm = (int)substr($tmp_date,4,2);
	$end_dd = (int)substr($tmp_date,6,2);
	$day_cnt = 28;
}
///-----------------------------------------------------------------------------
// カレンダー(calendar)情報を取得
///-----------------------------------------------------------------------------
$start_date = sprintf("%04d%02d%02d",$duty_yyyy, $duty_mm, $night_start_day);
$end_date = sprintf("%04d%02d%02d",$end_yyyy, $end_mm, $end_dd);
$calendar_array = $obj->get_calendar_array($start_date, $end_date);
///-----------------------------------------------------------------------------
//指定月の全曜日を設定
///-----------------------------------------------------------------------------
$week_array = array();
for ($k=1; $k<=$day_cnt; $k++) {
	$tmp_date = mktime(0, 0, 0, $duty_mm, $k, $duty_yyyy);
	$week_array[$k]["name"] = $obj->get_weekday($tmp_date);
}
///-----------------------------------------------------------------------------
//施設基準情報の取得
///-----------------------------------------------------------------------------
$standard_array = $obj->get_duty_shift_institution_history_array($standard_id, $duty_yyyy, $duty_mm);
///-----------------------------------------------------------------------------
// 様式９用　看護職員情報取得
///-----------------------------------------------------------------------------
$group_id = 0 ; // 全病棟
$data = $obj_report->get_report_array(
					$standard_id,
					$duty_yyyy,
					$duty_mm,
					$night_start_day,
					$end_yyyy,
					$end_mm,
					$end_dd,
					$day_cnt,
					$standard_array[0],
					$group_id,
					$plan_result_flag);

$data_array = $data["data_array"];
$data_sub_array = $data["data_sub_array"];

$nurse_cnt = $data["nurse_cnt"];			//看護師数
$sub_nurse_cnt = $data["sub_nurse_cnt"];		//准看護師数
$assistance_nurse_cnt = $data["assistance_nurse_cnt"];	//看護補助者数

$nurse_sumtime = $data["nurse_sumtime"];			//看護師総勤務時間
$sub_nurse_sumtime = $data["sub_nurse_sumtime"];		//准看護師総勤務時間
$assistance_nurse_sumtime = $data["assistance_nurse_sumtime"];	//看護補助者総勤務時間

$wk_duty_yyyymm = $duty_yyyy.sprintf("%02d", $duty_mm);

if ($wk_duty_yyyymm < "201204") { // 2012年対応
	$obj_ass = new duty_shift_report_associate_class($con, $fname);		// 2011年以前
}else{
	$obj_ass = new duty_shift_report_associate2012_class($con, $fname);	// 2012年対応
}

///-----------------------------------------------------------------------------
/// START
///-----------------------------------------------------------------------------

switch ($report_kbn) {
    case REPORT_KBN_CURE_BASIC1:
        $ReportType = 1; // 療養病棟入院基本料１
        $filename = "療養病棟入院基本料１.xls";
        break;
    case REPORT_KBN_CURE_BASIC2:
        $ReportType = 2; // 療養病棟入院基本料２
        $filename = "療養病棟入院基本料２.xls";
        break;
    case REPORT_KBN_SPECIFIC:
        $ReportType = 3; // 特定入院料
        $filename = "特定入院料.xls";
        break;
    case REPORT_KBN_SPECIFIC_PLUS:
        $ReportType = 4; // 特定入院料（看護職員＋看護補助者）
        $filename = "特定入院料（看護職員＋看護補助者）.xls";
        break;
    case REPORT_KBN_COMMUNITY_CARE:
    case REPORT_KBN_COMMUNITY_CARE2:
        $ReportType = 5; // 地域包括ケア 20140324
        $filename = "地域包括ケア病棟入院料.xls";
        break;
    default :
        $ReportType = 0; // 一般病棟入院基本料
        $filename = "一般病棟入院基本料.xls";
        break;
}

// 曜日生成
$week_array = array();
for ($k=1; $k<=$day_cnt; $k++) { 
	$wk_day = $k + $night_start_day - 1; 
	$tmp_date = mktime(0, 0, 0, $duty_mm, $wk_day, $duty_yyyy);//$k -> $wk_day 20150421
	$week_array[$k]["name"] = $obj->get_weekday($tmp_date);
	$week_array[$k]["day"] = date("j", $tmp_date);
}

// 共通
$obj_ass->publicHeader(
			$excelObj,	// ExcelObject
			$plan_result_flag, // 1予定 2実績 3予定+実績
			$_POST['create_yyyy'],	// 作成年月日
			$_POST['create_mm'] ,
			$_POST['create_dd'] ,
			$hospital_name,	// 保険医療機関名
			$ward_cnt ,	// 病棟数
			$sickbed_cnt,	// 病床数
            $report_kbn   //届出区分
);
// 帳票別
// 0:一般病棟様式、1:療養病棟様式１、2:療養病棟様式２
switch ($ReportType){
	case 0: // 一般病棟
		// $report_kbn_count対１入院基本料、920,925対策(duty_shift_common_class.php::get_report_kbn_array()を参照)
		switch ($report_kbn){
		case REPORT_KBN_BASIC20: $report_kbn_count = 20;break;
		case REPORT_KBN_BASIC25: $report_kbn_count = 25;break;
		default : $report_kbn_count = $report_kbn;
		}

		// 看護職員表、開始行位置
		if ($wk_duty_yyyymm < "201204") { // 2012年対応
			$start_row_number = 33;
		}else{
			$start_row_number = 41;
		}
		$arg_array = $obj_ass->ShowNurseData1(	// 看護職員表
				$excelObj,		// PHP-Excelオブジェクト
				$start_row_number,	// 表を開始する行位置
				$obj,			// 共通クラス
				$data_array,		// 看護師／准看護師情報
				$week_array,		// 曜日配列
				$day_cnt,		// 日数
				$kango_space_line	// 看護職員表、空白行数
		);

        $arg_array["report_kbn"] = $report_kbn; //20140326
        $arg_array = $obj_ass->ShowNurseSum01( $excelObj , $arg_array ); // 一般病棟様式　看護職員集計

		$arg_array = $obj_ass->ShowNurseData2(	// 看護補助者表
				$excelObj,		// PHP-Excelオブジェクト
				$start_row_number,	// 表を開始する行位置
				$obj,			// 共通クラス
				$data_sub_array,	// 看護補助者情報
				$week_array,		// 曜日配列
				$day_cnt,		// 日数
				$arg_array,		// 行位置情報
				$hojo_space_line	// 看護補助者表、空白行数
		);

		$arg_array = $obj_ass->ShowNurseSum02( $excelObj , $arg_array ); // 一般病棟様式　看護補助者集計

		$arg_array = $obj_ass->ShowNurseSum3( $excelObj , $arg_array ); // 一般病棟・療養病棟共通、月総夜勤時間別の看護職員数（夜勤従事者数）の分布

		if ($wk_duty_yyyymm >= "201204") { // 2012年対応
			$arg_array["hospital_name"] = $hospital_name;
			$arg_array["ward_cnt"]	    = $ward_cnt;
			$arg_array["sickbed_cnt"]   = $sickbed_cnt;

			$arg_array["acute_assistance_cnt"]	 = $acute_assistance_cnt;	// 急性期看護補助体制加算の届出区分
			$arg_array["night_acute_assistance_cnt"] = $night_acute_assistance_cnt;	// 夜間急性期看護補助体制加算の届出区分
			$arg_array["night_shift_add_flg"]	 = $night_shift_add_flg;	// 看護職員夜間配置加算の有無
		}
		$obj_ass->ShowData_0(
			$excelObj,		// PHP-Excelオブジェクト
			$obj,			// 共通クラス
//			$excel_flg,		//ＥＸＣＥＬ表示フラグ（１：ＥＸＣＥＬ表示）
//			$recomputation_flg,	//１ヵ月計算４週計算フラグ（１：１ヵ月計算、２：４週計算）
//			$data_array,		//看護師／准看護師情報
//			$data_sub_array,	//看護補助者情報
			$report_kbn,		//届出区分
			$report_kbn_name,	//届出区分名
			$report_patient_cnt,	//届出時入院患者数
			$hosp_patient_cnt,	//１日平均入院患者数
			$compute_start_yyyy,	//１日平均入院患者数 算出期間（開始年）
			$compute_start_mm,	//１日平均入院患者数 算出期間（開始月）
			$compute_end_yyyy,	//１日平均入院患者数 算出期間（終了年）
			$compute_end_mm,	//１日平均入院患者数 算出期間（終了月）
			$nurse_sumtime,		//勤務時間数（看護師）
			$sub_nurse_sumtime,	//勤務時間数（准看護師）
			$assistance_nurse_sumtime, //勤務時間数（看護補助者）
			$nurse_cnt,		//看護師数
			$sub_nurse_cnt,		//准看護師数
			$assistance_nurse_cnt,	//看護補助者数
			$hospitalization_cnt,	//平均在院日数
			$ave_start_yyyy,	//平均在院日数 算出期間（開始年）
			$ave_start_mm,		//平均在院日数 算出期間（開始月）
			$ave_end_yyyy,		//平均在院日数 算出期間（終了年）
			$ave_end_mm,		//平均在院日数 算出期間（終了月）
			$prov_start_hh,		//夜勤時間帯 （開始時）
			$prov_start_mm,		//夜勤時間帯 （開始分）
			$prov_end_hh,		//夜勤時間帯 （終了時）
			$prov_end_mm,		//夜勤時間帯 （終了分）
			$duty_yyyy,		//対象年
			$duty_mm,		//対象月
			$day_cnt,		//日数
//			$date_y1,		//カレンダー用年
//			$date_m1,		//カレンダー用月
//			$date_d1,		//カレンダー用日
			$labor_cnt,		//常勤職員の週所定労働時間
			$create_yyyy,		//作成年
			$create_mm,		//作成月
			$create_dd,		//作成日
			$nurse_staffing_flg,	//看護配置加算の有無
			$acute_nursing_flg,	//急性期看護補助加算の有無
			$acute_nursing_cnt,	//急性期看護補助加算の配置比率
			$nursing_assistance_flg,//看護補助加算の有無
			$nursing_assistance_cnt,//看護補助加算の配置比率
			$arg_array,		// 行位置情報
			$report_kbn_count	// 一般病棟、「n対１入院基本料金」の n の値
		);
		$arg_array["EndArea"] = $arg_array["GSumRow3"];
		$obj_ass->SetExcelSheet($excelObj,$arg_array); // エクセルシート調整

		break;
	case 1:	// 療養病棟１
	case 2: // 療養病棟２
		$start_row_number = 30;	// 看護職員表、開始行位置
		$arg_array = $obj_ass->ShowNurseData1(	// 看護職員表
				$excelObj,		// PHP-Excelオブジェクト
				$start_row_number,	// 表を開始する行位置
				$obj,			// 共通クラス
				$data_array,		// 看護師／准看護師情報
				$week_array,		// 曜日配列
				$day_cnt,		// 日数
				$kango_space_line	// 看護職員表、空白行数
		);
		$arg_array = $obj_ass->ShowNurseSum11( $excelObj , $arg_array ); // 療養病棟様式１　看護職員集計
		$arg_array = $obj_ass->ShowNurseData2(	// 看護補助者表
				$excelObj,		// PHP-Excelオブジェクト
				$start_row_number,	// 表を開始する行位置
				$obj,			// 共通クラス
				$data_sub_array,	// 看護補助者情報
				$week_array,		// 曜日配列
				$day_cnt,		// 日数
				$arg_array,		// 行位置情報
				$hojo_space_line	// 看護補助者表、空白行数
		);
		$arg_array = $obj_ass->ShowNurseSum12( $excelObj , $arg_array ); // 療養病棟様式１　看護補助者集計
		$arg_array = $obj_ass->ShowNurseSum3( $excelObj , $arg_array ); // 一般病棟・療養病棟共通、月総夜勤時間別の看護職員数（夜勤従事者数）の分布
		$obj_ass->ShowData_1(
			$excelObj,		// PHP-Excelオブジェクト
			$obj,			// 共通クラス
//			$excel_flg,		//ＥＸＣＥＬ表示フラグ（１：ＥＸＣＥＬ表示）
//			$recomputation_flg,	//１ヵ月計算４週計算フラグ（１：１ヵ月計算、２：４週計算）
//			$data_array,		//看護師／准看護師情報
//			$data_sub_array,	//看護補助者情報
//			$hospital_name,		//保険医療機関名
//			$ward_cnt,		//病棟数
//			$sickbed_cnt,		//病床数
			$report_kbn,		//届出区分
			$report_kbn_name,	//届出区分名
			$report_patient_cnt,	//届出時入院患者数
			$hosp_patient_cnt,	//１日平均入院患者数
			$compute_start_yyyy,	//１日平均入院患者数 算出期間（開始年）
			$compute_start_mm,	//１日平均入院患者数 算出期間（開始月）
			$compute_end_yyyy,	//１日平均入院患者数 算出期間（終了年）
			$compute_end_mm,	//１日平均入院患者数 算出期間（終了月）
			$nurse_sumtime,		//勤務時間数（看護師）
			$sub_nurse_sumtime,	//勤務時間数（准看護師）
			$assistance_nurse_sumtime, //勤務時間数（看護補助者）
			$nurse_cnt,		//看護師数
			$sub_nurse_cnt,		//准看護師数
			$assistance_nurse_cnt,	//看護補助者数
			$hospitalization_cnt,	//平均在院日数
			$ave_start_yyyy,	//平均在院日数 算出期間（開始年）
			$ave_start_mm,		//平均在院日数 算出期間（開始月）
			$ave_end_yyyy,		//平均在院日数 算出期間（終了年）
			$ave_end_mm,		//平均在院日数 算出期間（終了月）
			$prov_start_hh,		//夜勤時間帯 （開始時）
			$prov_start_mm,		//夜勤時間帯 （開始分）
			$prov_end_hh,		//夜勤時間帯 （終了時）
			$prov_end_mm,		//夜勤時間帯 （終了分）
//			$duty_yyyy,		//対象年
			$duty_mm,		//対象月
			$day_cnt,		//日数
//			$date_y1,		//カレンダー用年
//			$date_m1,		//カレンダー用月
//			$date_d1,		//カレンダー用日
			$labor_cnt,		//常勤職員の週所定労働時間
			$create_yyyy,		//作成年
			$create_mm,		//作成月
			$create_dd,		//作成日
			$nurse_staffing_flg,	//看護配置加算の有無
			$acute_nursing_flg,	//急性期看護補助加算の有無
			$acute_nursing_cnt,	//急性期看護補助加算の配置比率
			$nursing_assistance_flg,//看護補助加算の有無
			$nursing_assistance_cnt,//看護補助加算の配置比率
			$arg_array,		// 行位置情報
			$ReportType		// 1:療養病棟１、2:療養病棟２
		);
		$arg_array["EndArea"] = $arg_array["HMidSumRow2"] + 2;
		$obj_ass->SetExcelSheet($excelObj,$arg_array); // エクセルシート調整
		break;
	case 3: // 特定入院料
		$start_row_number = 29;	// 看護職員表、開始行位置
		$arg_array = $obj_ass->ShowNurseData1(	// 看護職員表
				$excelObj,		// PHP-Excelオブジェクト
				$start_row_number,	// 表を開始する行位置
				$obj,			// 共通クラス
				$data_array,		// 看護師／准看護師情報
				$week_array,		// 曜日配列
				$day_cnt,		// 日数
				$kango_space_line	// 看護職員表、空白行数
		);
		$arg_array = $obj_ass->ShowNurseSum11($excelObj, $arg_array, $ReportType); // 療養病棟様式１　看護職員集計
		$arg_array = $obj_ass->ShowNurseData2(	// 看護補助者表
				$excelObj,		// PHP-Excelオブジェクト
				$start_row_number,	// 表を開始する行位置
				$obj,			// 共通クラス
				$data_sub_array,	// 看護補助者情報
				$week_array,		// 曜日配列
				$day_cnt,		// 日数
				$arg_array,		// 行位置情報
				$hojo_space_line	// 看護補助者表、空白行数
		);
		$arg_array = $obj_ass->ShowNurseSum12($excelObj, $arg_array, $ReportType); // 療養病棟様式１　看護補助者集計
		$arg_array = $obj_ass->ShowNurseSum3( $excelObj , $arg_array ); // 一般病棟・療養病棟共通、月総夜勤時間別の看護職員数（夜勤従事者数）の分布
		$obj_ass->ShowData_3(
			$excelObj,		// PHP-Excelオブジェクト
			$obj,			// 共通クラス
			$report_kbn,		//届出区分
			$report_kbn_name,	//届出区分名
			$report_patient_cnt,	//届出時入院患者数
			$hosp_patient_cnt,	//１日平均入院患者数
			$compute_start_yyyy,	//１日平均入院患者数 算出期間（開始年）
			$compute_start_mm,	//１日平均入院患者数 算出期間（開始月）
			$compute_end_yyyy,	//１日平均入院患者数 算出期間（終了年）
			$compute_end_mm,	//１日平均入院患者数 算出期間（終了月）
			$nurse_sumtime,		//勤務時間数（看護師）
			$sub_nurse_sumtime,	//勤務時間数（准看護師）
			$assistance_nurse_sumtime, //勤務時間数（看護補助者）
			$nurse_cnt,		//看護師数
			$sub_nurse_cnt,		//准看護師数
			$assistance_nurse_cnt,	//看護補助者数
			$hospitalization_cnt,	//平均在院日数
			$ave_start_yyyy,	//平均在院日数 算出期間（開始年）
			$ave_start_mm,		//平均在院日数 算出期間（開始月）
			$ave_end_yyyy,		//平均在院日数 算出期間（終了年）
			$ave_end_mm,		//平均在院日数 算出期間（終了月）
			$prov_start_hh,		//夜勤時間帯 （開始時）
			$prov_start_mm,		//夜勤時間帯 （開始分）
			$prov_end_hh,		//夜勤時間帯 （終了時）
			$prov_end_mm,		//夜勤時間帯 （終了分）
			$duty_mm,		//対象月
			$day_cnt,		//日数
			$labor_cnt,		//常勤職員の週所定労働時間
			$create_yyyy,		//作成年
			$create_mm,		//作成月
			$create_dd,		//作成日
			$nurse_staffing_flg,	//看護配置加算の有無
			$acute_nursing_flg,	//急性期看護補助加算の有無
			$acute_nursing_cnt,	//急性期看護補助加算の配置比率
			$nursing_assistance_flg,//看護補助加算の有無
			$nursing_assistance_cnt,//看護補助加算の配置比率
			$specific_type,    //特定入院料種類
			$nurse_staffing_cnt,    //看護配置
			$assistance_staffing_cnt,     //看護補助配置
			$arg_array		// 行位置情報
		);
		$arg_array["EndArea"] = $arg_array["HMidSumRow2"] + 2;
		$obj_ass->SetExcelSheet($excelObj, $arg_array, '&L特定入院料届出計算用'); // エクセルシート調整
		break;
	case 4: // 特定入院料（看護職員＋看護補助者）
		$start_row_number = 32;	// 看護職員表、開始行位置
		$arg_array = $obj_ass->ShowNurseData1(	// 看護職員表
				$excelObj,		// PHP-Excelオブジェクト
				$start_row_number,	// 表を開始する行位置
				$obj,			// 共通クラス
				$data_array,		// 看護師／准看護師情報
				$week_array,		// 曜日配列
				$day_cnt,		// 日数
				$kango_space_line	// 看護職員表、空白行数
		);
		$arg_array = $obj_ass->ShowNurseSumSpecificPlus( $excelObj , $arg_array ); // 特定入院料（看護職員＋看護補助者）　看護職員集計
		$arg_array = $obj_ass->ShowNurseData2(	// 看護補助者表
				$excelObj,		// PHP-Excelオブジェクト
				$start_row_number,	// 表を開始する行位置
				$obj,			// 共通クラス
				$data_sub_array,	// 看護補助者情報
				$week_array,		// 曜日配列
				$day_cnt,		// 日数
				$arg_array,		// 行位置情報
				$hojo_space_line	// 看護補助者表、空白行数
		);
		$arg_array = $obj_ass->ShowNurseAssistanceSumSpecificPlus( $excelObj , $arg_array ); // 療養病棟様式１　看護補助者集計
		$arg_array = $obj_ass->ShowNurseSum3( $excelObj , $arg_array ); // 一般病棟・療養病棟共通、月総夜勤時間別の看護職員数（夜勤従事者数）の分布
		$obj_ass->ShowData_4(
			$excelObj,		// PHP-Excelオブジェクト
			$obj,			// 共通クラス
			$report_kbn,		//届出区分
			$report_kbn_name,	//届出区分名
			$report_patient_cnt,	//届出時入院患者数
			$hosp_patient_cnt,	//１日平均入院患者数
			$compute_start_yyyy,	//１日平均入院患者数 算出期間（開始年）
			$compute_start_mm,	//１日平均入院患者数 算出期間（開始月）
			$compute_end_yyyy,	//１日平均入院患者数 算出期間（終了年）
			$compute_end_mm,	//１日平均入院患者数 算出期間（終了月）
			$nurse_sumtime,		//勤務時間数（看護師）
			$sub_nurse_sumtime,	//勤務時間数（准看護師）
			$assistance_nurse_sumtime, //勤務時間数（看護補助者）
			$nurse_cnt,		//看護師数
			$sub_nurse_cnt,		//准看護師数
			$assistance_nurse_cnt,	//看護補助者数
			$hospitalization_cnt,	//平均在院日数
			$ave_start_yyyy,	//平均在院日数 算出期間（開始年）
			$ave_start_mm,		//平均在院日数 算出期間（開始月）
			$ave_end_yyyy,		//平均在院日数 算出期間（終了年）
			$ave_end_mm,		//平均在院日数 算出期間（終了月）
			$prov_start_hh,		//夜勤時間帯 （開始時）
			$prov_start_mm,		//夜勤時間帯 （開始分）
			$prov_end_hh,		//夜勤時間帯 （終了時）
			$prov_end_mm,		//夜勤時間帯 （終了分）
			$duty_mm,		//対象月
			$day_cnt,		//日数
			$labor_cnt,		//常勤職員の週所定労働時間
			$create_yyyy,		//作成年
			$create_mm,		//作成月
			$create_dd,		//作成日
			$nurse_staffing_flg,	//看護配置加算の有無
			$acute_nursing_flg,	//急性期看護補助加算の有無
			$acute_nursing_cnt,	//急性期看護補助加算の配置比率
			$nursing_assistance_flg,//看護補助加算の有無
			$nursing_assistance_cnt,//看護補助加算の配置比率
			$specific_type,    //特定入院料種類
			$nurse_staffing_cnt,    //看護配置
			$assistance_staffing_cnt,     //看護補助配置
			$arg_array		// 行位置情報
		);
		$arg_array["EndArea"] = $arg_array["HMidSumRow2"] + 2;
		$obj_ass->SetExcelSheet($excelObj, $arg_array, '&L特定入院料（看護職員＋看護補助者）届出計算用'); // エクセルシート調整
		break;
    case 5: // 地域包括ケア
        $report_kbn_count = $report_kbn - 900;
        // 看護職員表、開始行位置
        if ($wk_duty_yyyymm < "201204") { // 2012年対応
            $start_row_number = 33;
        }else{
            $start_row_number = 41;
        }
        $arg_array = $obj_ass->ShowNurseData1(	// 看護職員表
                $excelObj,		// PHP-Excelオブジェクト
                $start_row_number,	// 表を開始する行位置
                $obj,			// 共通クラス
                $data_array,		// 看護師／准看護師情報
                $week_array,		// 曜日配列
                $day_cnt,		// 日数
                $kango_space_line	// 看護職員表、空白行数
                );
        
        $arg_array["report_kbn"] = $report_kbn; //20140326
        $arg_array = $obj_ass->ShowNurseSum01( $excelObj , $arg_array ); // 一般病棟様式　看護職員集計

        $arg_array = $obj_ass->ShowNurseSum03( $excelObj , $arg_array ); // 地域包括ケア看護職員用 20140324

        $arg_array = $obj_ass->ShowNurseData2(	// 看護補助者表
                $excelObj,		// PHP-Excelオブジェクト
                $start_row_number,	// 表を開始する行位置
                $obj,			// 共通クラス
                $data_sub_array,	// 看護補助者情報
                $week_array,		// 曜日配列
                $day_cnt,		// 日数
                $arg_array,		// 行位置情報
                $hojo_space_line	// 看護補助者表、空白行数
                );

        $arg_array["nurse_staffing_flg"] = $nurse_staffing_flg;	 // 看護職員配置加算の有無
        $arg_array = $obj_ass->ShowNurseSum04( $excelObj , $arg_array ); // 地域包括ケア看護補助者用 20140324
        
        $arg_array = $obj_ass->ShowNurseSum3( $excelObj , $arg_array ); // 一般病棟・療養病棟共通、月総夜勤時間別の看護職員数（夜勤従事者数）の分布
        
        if ($wk_duty_yyyymm >= "201204") { // 2012年対応
            $arg_array["hospital_name"] = $hospital_name;
            $arg_array["ward_cnt"]	    = $ward_cnt;
            $arg_array["sickbed_cnt"]   = $sickbed_cnt;
            
            $arg_array["acute_assistance_cnt"]	 = $acute_assistance_cnt;	// 急性期看護補助体制加算の届出区分
            $arg_array["night_acute_assistance_cnt"] = $night_acute_assistance_cnt;	// 夜間急性期看護補助体制加算の届出区分
            $arg_array["night_shift_add_flg"]	 = $night_shift_add_flg;	// 看護職員夜間配置加算の有無
        }
        $obj_ass->ShowData_0(
                $excelObj,		// PHP-Excelオブジェクト
                $obj,			// 共通クラス
                //			$excel_flg,		//ＥＸＣＥＬ表示フラグ（１：ＥＸＣＥＬ表示）
                //			$recomputation_flg,	//１ヵ月計算４週計算フラグ（１：１ヵ月計算、２：４週計算）
                //			$data_array,		//看護師／准看護師情報
                //			$data_sub_array,	//看護補助者情報
                $report_kbn,		//届出区分
                $report_kbn_name,	//届出区分名
                $report_patient_cnt,	//届出時入院患者数
                $hosp_patient_cnt,	//１日平均入院患者数
                $compute_start_yyyy,	//１日平均入院患者数 算出期間（開始年）
                $compute_start_mm,	//１日平均入院患者数 算出期間（開始月）
                $compute_end_yyyy,	//１日平均入院患者数 算出期間（終了年）
                $compute_end_mm,	//１日平均入院患者数 算出期間（終了月）
                $nurse_sumtime,		//勤務時間数（看護師）
                $sub_nurse_sumtime,	//勤務時間数（准看護師）
                $assistance_nurse_sumtime, //勤務時間数（看護補助者）
                $nurse_cnt,		//看護師数
                $sub_nurse_cnt,		//准看護師数
                $assistance_nurse_cnt,	//看護補助者数
                $hospitalization_cnt,	//平均在院日数
                $ave_start_yyyy,	//平均在院日数 算出期間（開始年）
                $ave_start_mm,		//平均在院日数 算出期間（開始月）
                $ave_end_yyyy,		//平均在院日数 算出期間（終了年）
                $ave_end_mm,		//平均在院日数 算出期間（終了月）
                $prov_start_hh,		//夜勤時間帯 （開始時）
                $prov_start_mm,		//夜勤時間帯 （開始分）
                $prov_end_hh,		//夜勤時間帯 （終了時）
                $prov_end_mm,		//夜勤時間帯 （終了分）
                $duty_yyyy,		//対象年
                $duty_mm,		//対象月
                $day_cnt,		//日数
                //			$date_y1,		//カレンダー用年
                //			$date_m1,		//カレンダー用月
                //			$date_d1,		//カレンダー用日
                $labor_cnt,		//常勤職員の週所定労働時間
                $create_yyyy,		//作成年
                $create_mm,		//作成月
                $create_dd,		//作成日
                $nurse_staffing_flg,	//看護配置加算の有無
                $acute_nursing_flg,	//急性期看護補助加算の有無
                $acute_nursing_cnt,	//急性期看護補助加算の配置比率
                $nursing_assistance_flg,//看護補助加算の有無
                $nursing_assistance_cnt,//看護補助加算の配置比率
                $arg_array,		// 行位置情報
                $report_kbn_count	// 一般病棟、「n対１入院基本料金」の n の値
                );
        $arg_array["EndArea"] = $arg_array["GSumRow3"];
        $obj_ass->SetExcelSheet($excelObj, $arg_array, '&L地域包括ケア病棟入院料届出計算用'); // エクセルシート調整
        break;
}
///-----------------------------------------------------------------------------
// データEXCEL出力
///-----------------------------------------------------------------------------
ob_clean();

$ua = $_SERVER['HTTP_USER_AGENT'];
$filename = mb_convert_encoding($filename, 'UTF-8', mb_internal_encoding());

if(!preg_match("/safari/i",$ua)){
    header('Content-Type: application/octet-stream');
}
else {
    header('Content-Type: application/vnd.ms-excel');
}

if (preg_match("/MSIE/i", $ua)) {
    if (strlen(rawurlencode($filename)) > 21 * 3 * 3) {
        $filename = mb_convert_encoding($filename, "SJIS-win", "UTF-8");
        $filename = str_replace('#', '%23', $filename);
    }
    else {
        $filename = rawurlencode($filename);
    }
}
elseif (preg_match("/Trident/i", $ua)) {// IE11
    $filename = rawurlencode($filename);
}
elseif (preg_match("/chrome/i", $ua)) {// Google Chromeには『Safari』という字が含まれる
}
elseif (preg_match("/safari/i", $ua)) {// Safariでファイル名を指定しない場合
    $filename = "";
}
header('Content-Disposition: attachment; filename="'.$filename.'"');
header('Cache-Control: max-age=0');
ob_end_flush();
$excelObj->OutPut();

pg_close($con);

// program end.
