<?
require_once("class/Cmx/Model/SystemConfig.php");

function webmail_alias_key_exists() {
	$conf = new Cmx_SystemConfig();
	return ($conf->get('webmail.alias_enabled') != "");
}

function webmail_alias_enabled() {
	$conf = new Cmx_SystemConfig();
	return ($conf->get('webmail.alias_enabled') == "t");
}

function webmail_alias_enable() {
	$conf = new Cmx_SystemConfig();
	$conf->set('webmail.alias_enabled', "t");
}

function webmail_alias_disable() {
	$conf = new Cmx_SystemConfig();
	$conf->set('webmail.alias_enabled', "f");
}

function webmail_alias_truncate($con, $fname) {
	$sql = "delete from webmail_alias";
	$cond = "";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		return false;
	}
	return true;
}
