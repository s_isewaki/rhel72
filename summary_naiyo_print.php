<?/*

画面パラメータ
$session
$pt_id
$emp_id
$sum_id
$mode

*/
ob_start();
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");
require_once("label_by_profile_type.ini");
require_once("summary_tmpl_common.ini");
require_once("summary_tmpl_util.ini");
require_once("get_values_for_template.ini");
require_once("summary_common.ini");
require_once("show_kanja_joho.ini");
require_once("get_menu_label.ini");
ob_end_clean();

//====================================================================
//セッションチェック、権限チェック、DB接続
//====================================================================
$fname = $PHP_SELF;
$session = qualify_session($session,$fname);
$summary_check = @check_authority($session,57,$fname);
$con = @connect2db($fname);
if(!$session || !$summary_check || !$con){
	echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
	exit;
}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css"/>
<title><?=get_report_menu_label($con, $fname)?>｜内容印刷</title>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
</head>
<body style="padding:1px" onload="if(window.load_action){load_action();};
loadResizingTextArea();self.print();setTimeout('self.close();', 1000);" >


<?
// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

//summaryテーブルを検索
$sql =
	" select summary.*, sectmst.sect_nm from summary".
	" left join sectmst on sectmst.sect_id = summary.sect_id".
	" where summary_id='$sum_id' and ptif_id = '".pg_escape_string($pt_id)."'";
$sum_sel = select_from_table($con,$sql,"",$fname);


$cre_date = pg_result($sum_sel,0,"cre_date");
$tmpl_id = pg_result($sum_sel,0,"tmpl_id");
$xml_file = "";
if($tmpl_id != ""){
	$xml_file = $emp_id."_".$pt_id."_".$sum_id.".xml";
}


//========================================
// テンプレートの場合は、テンプレートの読み込み
//========================================
if($mode == 'template'){
	$sel = select_from_table($con,"select * from tmplmst where tmplmst.tmpl_id = '".pg_escape_string($tmpl_id)."'","",$fname);
	$tmpl_file = pg_result($sel,0,"tmpl_file");
	if(!file_exists($tmpl_file)) {
		echo("<script language=\"javascript\">alert(\"テンプレートファイルが正しくありません。管理者に連絡してください。\");close();</script>\n");
		exit;
	}
	require($tmpl_file);
}

//========================================
//FORMタグ外HTML出力
//========================================
tmpl_page_out_of_tmplform_html($session);
?>

<? //================================================= ?>
<? // ヘッダーの表示                                   ?>
<? //================================================= ?>
<? if($mode != 'template' || !(defined('TEMPLATE_HEADER_NON_PRINT') && TEMPLATE_HEADER_NON_PRINT == true)) {?>
<?   show_kanja_joho($con,$pt_id,$fname); ?>
	<table width="100%" class="prop_table" style="margin-top:4px">
	<tr>
		<th width="25%"><?=$_label_by_profile["SECTION"][$profile_type]?></th>
		<td colSpan="3"><?=pg_result($sum_sel,0,"sect_nm")?></td>
	</tr>
	<tr>
		<th>プロブレム</th>
		<td colSpan="3"><?=pg_result($sum_sel,0,"problem")?></td>
	</tr>
	<tr>
		<th width="25%"><?=$_label_by_profile["SUMMARY_RECORD_KUBUN"][$profile_type]?></th>
		<td width="30%"><?=pg_result($sum_sel,0,"diag_div")?></td>
		<th width="15%">作成日</th>
		<td width="30%"><?=substr($cre_date, 0, 4)."年".substr($cre_date, 4, 2)."月".substr($cre_date, 6, 2)."日"?></td>
	</tr>
	<tr>
		<th>記載者</th>
		<td><?=get_emp_kanji_name($con,$emp_id,$fname)?></td>
		<th>職種</th>
		<td><?=get_own_job_name($con,$emp_id,$fname)?></td>
	</tr>
	</table>
	<br>
<? } ?>




<? //================================================== ?>
<? // 内容情報の表示                                    ?>
<? //================================================== ?>
<form action="create_tmpl_xml.php" method="POST" name="tmplform">
<table border="0" style="width:100%"><tr><td>
<?
	if ($mode == "flowsheet"){
		$xml_file = get_tmpl_xml_file_name($emp_id, $pt_id, $sum_id);
		$tmpl_data_info = get_tmpl_data_info($xml_file);//テンプレートデータ情報を取得
		$tmpl_data_info = $tmpl_data_info[0]["child"];//階層0はテンプレートタイトルのため無視
		//階層1(大項目)に対して
		echo '<table class="prop_table">';
		for($i=0; $i<count($tmpl_data_info); $i++) {
			$name = (@$tmpl_data_info[$i]["attribute"]["disp"] != "" ? $tmpl_data_info[$i]["attribute"]["disp"] : $tmpl_data_info[$i]["name"]);
			$name = to_flowsheet_html_string($name);
			$value = convert_flowsheet_data_value_default(@$tmpl_data_info[$i]["child"]);
			echo '<tr><th style="width:20%; text-align:left; white-space:nowrap;">'.$name."</th><td>".$value."</td></tr>";
		}
		echo "</table>";
	}
	else if($mode == 'template'){
		$tmpl_param = array();
		$tmpl_param['mode'] = 'print';
		$tmpl_param['session'] = $session;
		$tmpl_param['login_emp_id'] = $emp_id;
		$tmpl_param['pt_id'] = $pt_id;
		$tmpl_param['summary_id'] = $sum_id;
		$tmpl_param['cre_date'] = $cre_date;
		write_tmpl_page($xml_file,$con,$fname,$tmpl_param);
		write_tmpl_page_data_script($xml_file,$con,$fname,$tmpl_param);
	} else {
		require_once("summary_tmpl_util.ini");
		$summary_naiyo1 = get_summary_naiyo_html($con,$fname,$sum_id,$pt_id);
		echo($summary_naiyo1);
	}
?>
</td></tr></table>
</form>
</body>
</html>
