<?php
require_once('about_comedix.php');
require_once("get_values.php");
require_once("hiyari_common.ini");
require_once("hiyari_item_timelag_setting_models.php");

// CmxSystemConfigクラス
require_once('Cmx.php');
require_once('class/Cmx/Model/SystemConfig.php');

//==============================
//初期処理
//==============================

// CmxSystemConfigインスタンス生成
$sysConf = new Cmx_SystemConfig();

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if ( $session == "0" ) {
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

$hiyari = check_authority($session, 48, $fname);
if ($hiyari == "0") {
    showLoginPage();
    exit;
}

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if ( $con == "0" ) {
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//==============================
//ポストバック時の処理
//==============================
if( $is_postback == "true" ) {
    //更新
    set_print_setting( $con, $fname, $patient_non_print, $mail_to_non_print, $patient_non_print_select, $patient_non_excel, $warning_sentence_print_flg, $warning_sentence );
	set_disp_setting( $con, $fname, $non_eis_report_default_disp_mode, $must_item_disp_mode, $mypage_news_disp, $news_all_disp, $mypage_header_color );
    setDispTimelagSetting( $con, $fname, $disp_report_timelag );
	
	// *** CmxSystemConfig Set ***
	// ファントルくん警告文章
	$sysConf->set('fantol.warning.message.text', $fantol_warning_message_text);
	
	// 警告文章マイページ表示フラグ
	$fantol_warning_mypage_flg = $fantol_warning_mypage_flg === null ? '0' : '1';
	$sysConf->set('fantol.warning.mypage.flg', $fantol_warning_mypage_flg);
	
	// 警告文章お知らせ表示フラグ
	$fantol_warning_news_flg = $fantol_warning_news_flg === null ? '0' : '1';
	$sysConf->set('fantol.warning.news.flg', $fantol_warning_news_flg);

    //　報告日編集可否フラグ
    $fantol_report_date_edit_flg = $fantol_report_date_edit_flg === null ? '0' : '1';
    $sysConf->set('fantol.report.date.edit.flg', $fantol_report_date_edit_flg);

    // 事例の詳細(時系列)のテンプレート入力フラグ
    $fantol_grpcode_4000_template_flg = $fantol_grpcode_4000_template_flg === null ? '0' : '1';
    $sysConf->set('fantol.grpcode.4000.template.flg', $fantol_grpcode_4000_template_flg);

    //　ユーザ画面の新デザインフラグ
    $sysConf->set('fantol.design.mode', $fantol_design_mode);

	// 報告書へのファイル添付フラグ
	$fantol_file_attach = $fantol_file_attach === null ? '0' : '1';
	$sysConf->set('fantol.report.file.attach', $fantol_file_attach);
	
    //評価予定期日の表示の有無
    $fantol_file_attach = $fantol_file_attach === null ? 'f' : 't';
    $sysConf->set('fantol.predetermined.eval', $fantol_predetermined_eval);

}

//==============================
//設定情報取得
//==============================
$print_setting = get_print_setting($con,$fname);
$patient_non_print          = $print_setting["patient_non_print_flg"]           == "t";
$patient_non_print_select   = $print_setting["patient_non_print_select_flg"]    == "t";
$patient_non_excel          = $print_setting["patient_non_excel_flg"]           == "t";
$mail_to_non_print          = $print_setting["mail_to_non_print_flg"]           == "t";
$warning_sentence_print_flg = $print_setting["warning_sentence_print_flg"]      == "t";
$warning_sentence           = $print_setting["warning_sentence"];


$disp_setting = get_disp_setting($con,$fname);
$non_eis_report_default_disp_mode   = $disp_setting["non_eis_report_default_disp_mode"];
$must_item_disp_mode                = $disp_setting["must_item_disp_mode"];
$mypage_news_disp                   = $disp_setting["mypage_news_disp"];
$news_all_disp                      = $disp_setting["news_all_disp"];
$mypage_header_color                = $disp_setting["mypage_header_color"];


// 発見時刻 〜 報告時刻のタイムラグを表示するかしないか。
$disp_report_timelag = getDispTimelagSetting($con, $fname);

// *** CmxSystemConfig Get ***

// ファントルくん警告文章
$fantol_warning_message_text     = h($sysConf->get('fantol.warning.message.text'));

// 警告文章マイページ表示フラグ
$fantol_warning_mypage_flg       = $sysConf->get('fantol.warning.mypage.flg');
$fantol_warning_mypage_flg       = $fantol_warning_mypage_flg === null ? '0' : $fantol_warning_mypage_flg ;

// 警告文章お知らせ表示フラグ
$fantol_warning_news_flg       = $sysConf->get('fantol.warning.news.flg');
$fantol_warning_news_flg       = $fantol_warning_news_flg === null ? '0' : $fantol_warning_news_flg ;

//　報告日編集可否フラグ
$fantol_report_date_edit_flg = $sysConf->get('fantol.report.date.edit.flg');
$fantol_report_date_edit_flg = $fantol_report_date_edit_flg === null ? '0' : $fantol_report_date_edit_flg;

// 事例の詳細(時系列)のテンプレート入力フラグ
$fantol_grpcode_4000_template_flg = $sysConf->get('fantol.grpcode.4000.template.flg');
$fantol_grpcode_4000_template_flg = $fantol_grpcode_4000_template_flg === null ? '0' : $fantol_grpcode_4000_template_flg;

// ユーザ画面の新デザインフラグ
$fantol_design_mode = $sysConf->get('fantol.design.mode');
$fantol_design_mode = $fantol_design_mode === null ? '1' : $fantol_design_mode;

//報告書へのファイル添付フラグ
$fantol_file_attach = $sysConf->get('fantol.report.file.attach');
$fantol_file_attach = $fantol_file_attach === null ? '0' : $fantol_file_attach;

//評価予定期日の表示の有無
$fantol_predetermined_eval = $sysConf->get('fantol.predetermined.eval');





//========================================================================================================================
//HTML出力
//========================================================================================================================

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$INCIDENT_TITLE?> | 印刷・表示設定</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript">


function load_action()
{
    script_patient_non_print_select();
}

function script_patient_non_print_select()
{
    var obj1 = document.getElementById("patient_non_print");
    var obj2 = document.getElementById("patient_non_print_select");
    obj1.disabled = obj2.checked;
}
// 警告文章を表示するをチェックしたときの処理
$(function() {
    $('#warning_sentence_print_flg').click(function(){
        if ( !$('#warning_sentence_print_flg').attr('checked') ) { 
            $('#warning_sentence').attr('disabled', 'disabled');
        }
        else {
            $('#warning_sentence').attr('disabled', '');
        }
    });
});

// 更新ボタンのクリックしたときの処理
$(function() {
    $('#btn_update').click(function(){
        if ( ($('#fantol_warning_mypage_flg').attr('checked') 
		    ||  $('#fantol_warning_mypage_flg').attr('checked'))
			 && ( $('#fantol_warning_message_text').val() == '')
			 ) { 
            alert('メッセージを表示する場合は、メッセージ内容を記載して下さい。');
        }
        else {
            $('#form1').submit() ;
        }
    });
});

// 「マイページに表示する」にチェックを入れたときの処理
$(function() {
    $('#fantol_warning_mypage_flg').click(function(){
        if ( $('#fantol_warning_mypage_flg').attr('checked') 
		  && $('#fantol_warning_message_text').val() == ''
		) { 
            alert('メッセージを表示する場合は、メッセージ内容を記載して下さい。');
        }
    });
});

// 「お知らせ画面に表示する」にチェックを入れたときの処理
$(function() {
    $('#fantol_warning_news_flg').click(function(){
        if ( $('#fantol_warning_news_flg').attr('checked') 
		  && $('#fantol_warning_message_text').val() == ''
		) { 
            alert('メッセージを表示する場合は、メッセージ内容を記載して下さい。');
        }
    });
});


</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

    .list {border-collapse:collapse;}
    .list td {border:#35B341 solid 1px;}

    .non_in_list {border-collapse:collapse;}
    .non_in_list td {border:0px;}

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="load_action()">
<form id="form1" name="form1" action="" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="is_postback" value="true">

<!-- BODY全体 START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<!-- ヘッダー START -->
<?
show_hiyari_header($session,$fname,true);
?>
<!-- ヘッダー END -->

<!-- メイン START -->
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
<td>


<!-- 外の枠 START -->
<table border="0" cellspacing="0" cellpadding="0" width="500">
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
<tr>
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td bgcolor="#F5FFE5">


<table border="0" cellspacing="1" cellpadding="0" class="list">
<tr>
<td width="200" bgcolor="#DFFFDC" align="center" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><B>出力設定</B></font>
</td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="1">

<tr>
<td width="100%" bgcolor="#F5FFE5" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="checkbox" id="patient_non_print" name="patient_non_print" value="t" <?if($patient_non_print == 't'){?>checked<?}?>>報告書印刷で患者ID、氏名を印刷しない
</font>
</td>
</tr>

<tr>
<td width="100%" bgcolor="#F5FFE5" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="checkbox" id="patient_non_print_select" name="patient_non_print_select" value="t" <?if($patient_non_print_select == 't'){?>checked<?}?> onclick="script_patient_non_print_select()" >印刷時に患者ID、氏名を印刷するかどうか指定する
</font>
</td>
</tr>

<tr>
<td width="100%" bgcolor="#F5FFE5" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="checkbox" name="patient_non_excel" value="t" <? if($patient_non_excel == 't'){?>checked<?}?> >Excel出力時に患者ID、患者氏名を出力しない
</font>
</td>
</tr>

<tr>
<td width="100%" bgcolor="#F5FFE5" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="checkbox" name="mail_to_non_print" value="t" <? if($mail_to_non_print == 't'){?>checked<?}?> >メール印刷でメールの宛先を印刷しない
</font>
</td>
</tr>

<tr>
<td width="100%" bgcolor="#F5FFE5" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="checkbox" name="warning_sentence_print_flg" id="warning_sentence_print_flg" value="t" <? if($warning_sentence_print_flg == 't'){?>checked<?}?> >印刷時に警告文章を表示する
</font>
</td>
</tr>

<tr>
<td width="100%" bgcolor="#F5FFE5" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
警告文：<input type="textbox" name="warning_sentence" id="warning_sentence" size="75" value="<?=$warning_sentence?>" <? if($warning_sentence_print_flg != 't'){?>disabled="disabled"<?}?>>
</font>
</td>
</tr>



</table>


<img src="img/spacer.gif" width="1" height="15" alt=""><br>


<table border="0" cellspacing="1" cellpadding="0" class="list">
<tr>
<td width="200" bgcolor="#DFFFDC" align="center" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><B>報告ファイルの初期表示の設定</B></font>
</td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
<td width="100%" bgcolor="#F5FFE5" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="non_eis_report_default_disp_mode" value="all" <?if($non_eis_report_default_disp_mode == "all"){?>checked<?}?> >全項目
<input type="radio" name="non_eis_report_default_disp_mode" value="eis" <?if($non_eis_report_default_disp_mode == "eis"){?>checked<?}?> >送信時の指定様式
</font>
</td>
</tr>

<tr>
<td width="100%" bgcolor="#F5FFE5" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="checkbox" name="disp_report_timelag" value="t" <? if ( $disp_report_timelag == 't' ){?>checked<?}?> >発見時間 〜 報告時間のタイムラグを表示する<br />
<span style="color:red">
※ タイムラグは発見時刻を入力しないと表示されません。<br />
※ 発見時刻は報告書様式＞管理項目変更＞発見時間帯・発生時間帯画面で<br />
「時間・分選択表示」を選択すると入力できます。
</span>
</font>
</td>
</tr>

</table>


<img src="img/spacer.gif" width="1" height="15" alt=""><br>

<table border="0" cellspacing="1" cellpadding="0" class="list">
<tr>
<td width="200" bgcolor="#DFFFDC" align="center" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><B>報告書必須項目表示の設定</B></font>
</td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
<td width="100%" bgcolor="#F5FFE5" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="must_item_disp_mode" value="1" <?if($must_item_disp_mode == "1"){?>checked<?}?> >＊マーク
<input type="radio" name="must_item_disp_mode" value="2" <?if($must_item_disp_mode == "2"){?>checked<?}?> >項目名の文字背景
</font>
</td>
</tr>
</table>



<img src="img/spacer.gif" width="1" height="15" alt=""><br>

<table border="0" cellspacing="1" cellpadding="0" class="list">
<tr>
<td width="200" bgcolor="#DFFFDC" align="center" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><B>お知らせの表示設定</B></font>
</td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
<td width="100%" bgcolor="#F5FFE5" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="checkbox" id="mypage_news_disp" name="mypage_news_disp" value="t" <?if($mypage_news_disp == 't'){?>checked<?}?>>マイページにお知らせの内容を表示する
</font>
</td>
</tr>
<tr>
<td width="100%" bgcolor="#F5FFE5" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="checkbox" id="news_all_disp" name="news_all_disp" value="t" <?if($news_all_disp == 't'){?>checked<?}?>>お知らせ画面にお知らせの内容の詳細を表示する
</font>
</td>
</tr>
<tr>
<td width="100%" bgcolor="#F5FFE5" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="checkbox" id="mypage_header_color" name="mypage_header_color" value="t" <?if($mypage_header_color == 't'){?>checked<?}?>>マイページでのお知らせ表示欄の項目エリアの色を変える
</font>
</td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="15" alt=""><br>

<table border="0" cellspacing="1" cellpadding="0" class="list">
<tr>
<td width="200" bgcolor="#DFFFDC" align="center" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><B>メッセージの表示設定</B></font>
</td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
<td width="100%" bgcolor="#F5FFE5" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
メッセージ内容<br />
<input type="textbox" name="fantol_warning_message_text" id="fantol_warning_message_text" size="83" value="<?=$fantol_warning_message_text?>">
</font>
</td>
</tr>
<tr>
<td width="100%" bgcolor="#F5FFE5" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="checkbox" id="fantol_warning_mypage_flg" name="fantol_warning_mypage_flg" value="t" <?if($fantol_warning_mypage_flg == '1'){?>checked<?}?>>マイページに表示する
</font>
</td>
</tr>
<tr>
<td width="100%" bgcolor="#F5FFE5" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="checkbox" id="fantol_warning_news_flg" name="fantol_warning_news_flg" value="t" <?if($fantol_warning_news_flg == '1'){?>checked<?}?>>お知らせ画面に表示する
</font>
</td>
</tr>
</table>


<img src="img/spacer.gif" width="1" height="15" alt=""><br>

<table border="0" cellspacing="1" cellpadding="0" class="list">
<tr>
<td width="200" bgcolor="#DFFFDC" align="center" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><B>報告書画面の入力設定</B></font>
</td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
    <td width="100%" bgcolor="#F5FFE5" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
    <input type="checkbox" id="fantol_report_date_edit_flg" name="fantol_report_date_edit_flg" value="t" <?if($fantol_report_date_edit_flg == '1'){?>checked<?}?>>報告日を変更不可にする
    </font>
    </td>
</tr>
<tr>
    <td width="100%" bgcolor="#F5FFE5" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
    <input type="checkbox" id="fantol_grpcode_4000_template_flg" name="fantol_grpcode_4000_template_flg" value="t" <?if($fantol_grpcode_4000_template_flg == '1'){?>checked<?}?>>事例の詳細(時系列)の入力をテンプレート方式にする
    </font>
    </td>
</tr>
<tr>
    <td width="100%" bgcolor="#F5FFE5" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
        <span style="color:red">
        ※ 報告書画面の入力設定は、報告書様式がスタイル１の場合は対象外となります。<br />
        </span>
    </font>
    </td>
</tr>
</table>

<img src="img/spacer.gif" width="1" height="5" alt=""><br><br>

<table border="0" cellspacing="1" cellpadding="0" class="list">
<tr>
<td width="200" bgcolor="#DFFFDC" align="center" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><B>評価予定期日を表示設定</B></font>
</td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
    <td width="100%" bgcolor="#F5FFE5" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
    <input type="checkbox" id="fantol_predetermined_eval" name="fantol_predetermined_eval" value="t" <?if($fantol_predetermined_eval == 't'){?>checked<?}?>>評価予定期日を表示する
    </font>
    </td>
</tr>
</table>

<img src="img/spacer.gif" width="1" height="5" alt=""><br><br>

<table border="0" cellspacing="1" cellpadding="0" class="list">
<tr>
<td width="200" bgcolor="#DFFFDC" align="center" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><B>ファイル添付</B></font>
</td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
    <td width="100%" bgcolor="#F5FFE5" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
    <input type="checkbox" id="fantol_file_attach" name="fantol_file_attach" value="t" <?if($fantol_file_attach == '1'){?>checked<?}?>>報告書にファイルを添付する
    </font>
    </td>
</tr>
</table>


<img src="img/spacer.gif" width="1" height="15" alt=""><br>

<table border="0" cellspacing="1" cellpadding="0" class="list">
<tr>
<td width="200" bgcolor="#DFFFDC" align="center" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><B>ユーザ画面のデザイン設定</B></font>
</td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
<td width="100%" bgcolor="#F5FFE5" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="fantol_design_mode" value="1" <?if($fantol_design_mode == "1"){?>checked<?}?> >旧デザインを利用する
<input type="radio" name="fantol_design_mode" value="2" <?if($fantol_design_mode == "2"){?>checked<?}?> >新デザインを利用する
</font>
</td>
</tr>
</table>

<img src="img/spacer.gif" width="1" height="5" alt=""><br>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
<td align="right">
<input id="btn_update" name="btn_update" type="button" value="更新">
</td>
</tr>
<tr>
<td align="left">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#FF0000">&lt;印刷設定の注意事項&gt;<BR>メール内容など、該当項目以外の自由入力欄に含まれる情報についてはこの設定の対象外となります。ご注意ください。</font>
</td>
</tr>
</table>


</td>
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>
<!-- 外の枠 END -->


</td>
</tr>
</table>
<!-- メイン END -->

<!-- フッター START -->
<?
show_hiyari_footer($session,$fname,true);
pg_close($con);
?>
<!-- フッター END -->

</td>
</tr>
</table>
<!-- BODY全体 END -->

</form>
</body>
</html>
