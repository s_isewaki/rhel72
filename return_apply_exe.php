<?
require("about_session.php");
require("about_authority.php");
require_once("about_postgres.php");
require_once("atdbk_workflow_common_class.php");

$fname = $PHP_SELF;

if ($session != "") {

	// セッションのチェック
	$session = qualify_session($session, $fname);
	if ($session == "0") {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
		exit;
	}
}

// 入力チェック
if ($reason_id == "other") {
	if ($reason == "") {
		echo("<script type=\"text/javascript\">alert('理由が入力されていません。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}
	if (strlen($reason) > 200) {
		echo("<script type=\"text/javascript\">alert('理由が長すぎます。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}
	$reason_id = null;
} else {
	$reason = "";
}

// データベースに接続
$con = connect2db($fname);

$obj = new atdbk_workflow_common_class($con, $fname);


// トランザクションを開始
pg_query($con, "begin");

// 申請IDを採番
$sql = "select max(apply_id) from rtnapply";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$apply_id = intval(pg_fetch_result($sel, 0, 0)) + 1;

// 申請情報を登録

// 申請確定情報取得
if($wkfw_approve_num != "")
{
    $rtncfm = $obj->get_apply_confirm_hierarchy($_POST);
}
else
{
    $rtncfm = null;
}

$sql = "insert into rtnapply (apply_id, emp_id, target_date, reason_id, reason, apply_time, apply_status, rtncfm, delete_flg) values (";
$content = array($apply_id, $emp_id, $date, $reason_id, $reason, date("YmdHis"), "0", $rtncfm, "f");

$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 承認登録
for($i=1; $i<=$approve_num; $i++)
{
	$varname = "regist_emp_id$i";
	$aprv_emp_id = ($$varname == "") ? null : $$varname;

	$varname = "apv_order$i";
	$apv_order = ($$varname == "") ? null : $$varname;

	$varname = "apv_sub_order$i";
	$apv_sub_order = ($$varname == "") ? null : $$varname;

	$varname = "multi_apv_flg$i";
	$multi_apv_flg = ($$varname == "") ? "f" : $$varname;

	$varname = "next_notice_div$i";
	$next_notice_div = ($$varname == "") ? null : $$varname;

    if($aprv_emp_id != "")
    {
		// 役職も登録する
		$sql = "select emp_st from empmst ";
		$cond = "where emp_id = '$aprv_emp_id'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$emp_st = pg_fetch_result($sel, 0, 0);
	
		// 承認者情報を登録
		$sql = "insert into rtnaprv (apply_id, aprv_no, aprv_emp_id, aprv_status, aprv_sub_no, multi_apv_flg, next_notice_div, emp_st) values (";
		$content = array($apply_id, $apv_order, $aprv_emp_id, "0", $apv_sub_order, $multi_apv_flg, $next_notice_div, $emp_st);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
    }
}


// 承認者候補登録
for($i=1; $i <= $approve_num; $i++)
{
	$varname = "apv_order$i";
	$apv_order = $$varname;

	$varname = "pst_approve_num$apv_order";
	$pst_approve_num = $$varname;

	for($j=1; $j<=$pst_approve_num; $j++)
	{
		$varname = "pst_emp_id$apv_order";
		$varname .= "_$j";
		$pst_emp_id = $$varname;

	
		$sql = "insert into rtnaprvemp (apply_id, aprv_no, person_no, emp_id, delete_flg) values (";
		$content = array($apply_id, $apv_order, $j, $pst_emp_id, 'f');
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
}

// 非同期・同期受信登録
$previous_apv_order = "";
$arr_apv = array();
$arr_apv_sub_order = array();
for($i=1; $i <= $approve_num; $i++)
{
	$varname = "apv_order$i";
	$apv_order = $$varname;

	$varname = "apv_sub_order$i";
	$apv_sub_order = ($$varname == "") ? null : $$varname;

	$varname = "multi_apv_flg$i";
	$multi_apv_flg = ($$varname == "") ? "f" : $$varname;

	$varname = "next_notice_div$i";
	$next_notice_div = ($$varname == "") ? null : $$varname;

	if($previous_apv_order != $apv_order)
	{
		$arr_apv_sub_order = array();
	}
	$arr_apv_sub_order[] = $apv_sub_order;
	$arr_apv[$apv_order] = array("multi_apv_flg" => $multi_apv_flg, "next_notice_div" => $next_notice_div, "apv_sub_order" => $arr_apv_sub_order);
	
	$previous_apv_order = $apv_order;
}

$arr_send_apv_sub_order = array();
foreach($arr_apv as $apv_order => $apv_info)
{
	$multi_apv_flg = $apv_info["multi_apv_flg"];
	$next_notice_div = $apv_info["next_notice_div"];
	$arr_apv_sub_order = $apv_info["apv_sub_order"];

	foreach($arr_send_apv_sub_order as $send_apv_order => $arr_send_apv_sub)
	{
		// 非同期通知
		if($arr_send_apv_sub != null)
		{
			foreach($arr_send_apv_sub as $send_apv_sub_order)
			{
				foreach($arr_apv_sub_order as $recv_apv_sub_order)
				{
					$sql = "insert into rtn_async_recv (apply_id, send_aprv_no, send_aprv_sub_no, recv_aprv_no, recv_aprv_sub_no, send_aprved_no, apv_show_flg) values (";
					$content = array($apply_id, $send_apv_order, $send_apv_sub_order, $apv_order, $recv_apv_sub_order, null, "f");
					$ins = insert_into_table($con, $sql, $content, $fname);
					if ($ins == 0) {
						pg_query($con, "rollback");
						pg_close($con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}
				}
			}
		}
		// 同期通知
		else
		{
			foreach($arr_apv_sub_order as $recv_apv_sub_order)
			{
				$sql = "insert into rtn_async_recv (apply_id, send_aprv_no, send_aprv_sub_no, recv_aprv_no, recv_aprv_sub_no, send_aprved_no, apv_show_flg) values (";
				$content = array($apply_id, $send_apv_order, null, $apv_order, $recv_apv_sub_order, null, "f");
				$ins = insert_into_table($con, $sql, $content, $fname);
				if ($ins == 0) {
					pg_query($con, "rollback");
					pg_close($con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
			}
		}

	}
	$arr_send_apv_sub_order = array();

	// 非同期通知の場合
	if($multi_apv_flg == "t" && $next_notice_div == "1" && count($arr_apv_sub_order) > 1)
	{
		$arr_send_apv_sub_order[$apv_order] = $arr_apv_sub_order;
	}
	// 同期通知または権限並列通知の場合
	else if($multi_apv_flg == "t" && $next_notice_div != "1" && count($arr_apv_sub_order) > 1)
	{
		$arr_send_apv_sub_order[$apv_order] = null;
	}
}


// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 画面を閉じる
if ($pnt_url == "") {
	echo("<script type=\"text/javascript\">alert('申請しました。');opener.document.loginform.id.focus();</script>");
} else {
	echo("<script type=\"text/javascript\">opener.location.href = '$pnt_url';</script>");
}
echo("<script type=\"text/javascript\">self.close();</script>");
?>
