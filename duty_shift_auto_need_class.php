<?php
//************************************************************************
// 勤務シフト作成　勤務シフト作成画面
// 自動シフト必要人数ＣＬＡＳＳ
//************************************************************************
require_once("about_postgres.php");

class duty_shift_auto_need_class
{
	/*************************************************************************/
	// コンストラクタ
	/*************************************************************************/
	function duty_shift_auto_need_class($group_id, $duty_y, $duty_m, $start_date, $end_date, $con, $fname)
	{
		// 呼び出し元ファイル名を保持
		$this->file_name = $fname;
		// DBコネクションを保持
		$this->_db_con = $con;

		// 必要人数テーブルを保持
		$this->_need_table = $this->_create_need_table($group_id, $duty_y, $duty_m, $start_date, $end_date);

		// インデックスのキャッシュを初期化
		$this->_idx_cache = array();
	}

	/*************************************************************************/
	// 必要人数テーブルの取得
	/*************************************************************************/
	function _create_need_table($group_id, $duty_y, $duty_m, $start_date, $end_date)
	{
		//-------------------------------------------------------------------
		// 日付データの取得
		//-------------------------------------------------------------------
		//SQL実行
		$sql = <<<_SQL_END_
SELECT
c1.*,
TO_CHAR(TO_DATE(c1.date,'YYYYMMDD') + interval '1 days','YYYYMMDD') AS next,
TO_CHAR(TO_DATE(c1.date,'YYYYMMDD') - interval '1 days','YYYYMMDD') AS prev,
DATE_PART('DOW',TO_DATE(c1.date,'YYYYMMDD')) AS week,
CASE
  WHEN c1.type='6' THEN 7
  WHEN c2.type='6' THEN 8
  ELSE DATE_PART('DOW',TO_DATE(c1.date,'YYYYMMDD'))
END AS week2
FROM calendar c1
LEFT JOIN calendar c2 ON c2.date=TO_CHAR(TO_DATE(c1.date,'YYYYMMDD') + interval '1 days','YYYYMMDD')
WHERE c1.date BETWEEN '$start_date' AND '$end_date' ORDER BY c1.date
_SQL_END_;
		$sel = select_from_table($this->_db_con, $sql, "", $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			echo("<!-- FROM calendar -->");
			exit;
		}
		//fetch
		$need_cnt = array();
		while ($row = pg_fetch_assoc($sel)){
			$row["cnt"] = "";
			$need_cnt[ $row["date"] ] = $row;
		}
		$this->_date_table = $need_cnt;

		//-------------------------------------------------------------------
		// 必要人数情報の取得
		// ※必要人数情報（カレンダー）は廃止したが、データが残っている可能性が
		//   あるため、duty_shift_need_cnt_calendar_jobの処理は残す
		//-------------------------------------------------------------------
		$need_table = array();

		//SQL実行
		$sql = "SELECT n.*,
				j1.job_nm as job1_name,
				j2.job_nm as job2_name,
				j3.job_nm as job3_name,
				CASE n.sex WHEN '1' THEN '男性' WHEN '2' THEN '女性' ELSE n.sex END as sex_name,
				t.team_name,
				p.atdptn_nm as ptn_name
				FROM duty_shift_need_cnt_calendar_job n
				JOIN duty_shift_group g USING (group_id)
				LEFT JOIN jobmst j1 on j1.job_id=n.job_id
				LEFT JOIN jobmst j2 on j2.job_id=n.job_id2
				LEFT JOIN jobmst j3 on j3.job_id=n.job_id3
				LEFT JOIN duty_shift_staff_team t on t.team_id=n.team
				LEFT JOIN atdptn p on p.atdptn_id=n.atdptn_ptn_id and p.group_id=g.pattern_id";
		$cond = <<<_COND_END_
WHERE n.group_id='$group_id'
AND duty_yyyy=$duty_y
AND duty_mm=$duty_m
ORDER BY atdptn_ptn_id
_COND_END_;
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			echo("<!-- FROM duty_shift_need_cnt_calendar_job -->");
			exit;
		}

		//count
		$count_job = pg_num_rows($sel);

		//-------------------------------------------------------------------
		// 必要人数情報（カレンダー）のデータがあれば、
		// カレンダーデータ(d_s_need_cnt_calendar)からテーブルを作成
		// ※必要人数情報（カレンダー）は廃止したが、データが残っている可能性が
		//   あるため処理は残す
		//-------------------------------------------------------------------
		if ( $count_job > 0 ) {

			// d_s_need_cnt_calendar_job fetch
			while ( $row = pg_fetch_assoc($sel) ) {
				$row["team"] = trim($row["team"]);
				$row["sex"] = trim($row["sex"]);
				$row["need_cnt"] = $need_cnt;

				// d_s_need_cnt_calendarからデータを取得
				$sql = <<<_SQL_END_
SELECT * FROM duty_shift_need_cnt_calendar
_SQL_END_;
				$cond = <<<_COND_END_
WHERE group_id='$group_id'
AND atdptn_ptn_id='{$row["atdptn_ptn_id"]}'
AND job_id='{$row["job_id"]}'
AND team='{$row["team"]}'
AND sex='{$row["sex"]}'
AND duty_date BETWEEN '$start_date' AND '$end_date'
_COND_END_;
				$sel2 = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
				if ($sel2 == 0) {
					pg_close($this->_db_con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					echo("<!-- FROM duty_shift_need_cnt_calendar -->");
					exit;
				}

				// d_s_need_cnt_calendar fetch
				while ( $row2 = pg_fetch_assoc($sel2) ) {
					$row["need_cnt"][ $row2["duty_date"] ]["cnt"] = $row2["need_cnt"] ;
				}

				$need_table[] = $row ;
			}
		}
		//-------------------------------------------------------------------
		// 必要人数情報（カレンダー）のデータがなければ、
		// 標準データ(d_s_need_cnt_standard)からテーブルを作成
		//-------------------------------------------------------------------
		else {
			// d_s_need_cnt_standardからデータを取得
			$sql = "SELECT n.*,
					j1.job_nm as job1_name,
					j2.job_nm as job2_name,
					j3.job_nm as job3_name,
					CASE n.sex WHEN '1' THEN '男性' WHEN '2' THEN '女性' ELSE n.sex END as sex_name,
					s.st_nm as st_name,
					t.team_name,
					p.atdptn_nm as ptn_name
					FROM duty_shift_need_cnt_standard n
					JOIN duty_shift_group g USING (group_id)
					LEFT JOIN jobmst j1 on j1.job_id=n.job_id
					LEFT JOIN jobmst j2 on j2.job_id=n.job_id2
					LEFT JOIN jobmst j3 on j3.job_id=n.job_id3
					LEFT JOIN stmst s on s.st_id=n.st_id
					LEFT JOIN duty_shift_staff_team t on t.team_id=n.team
					LEFT JOIN atdptn p on p.atdptn_id=n.atdptn_ptn_id and p.group_id=g.pattern_id
					WHERE n.group_id='$group_id' AND atdptn_ptn_id<>10 ORDER BY no"; // 条件追加、atdptn_ptn_id<>10 ... 2010.4.16
			$sel = select_from_table($this->_db_con, $sql, "", $this->file_name);
			if ($sel == 0) {
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				echo("<!-- FROM duty_shift_need_cnt_standard -->");
				exit;
			}

			// skill情報 2012.3.14追加▼
			// DBのスキル情報を読み込み$need_tableへスキル情報を追加する
			// 必要人数テーブル(DB)とスキルテーブル(DB)は別になっている
			// さらにスキルテーブル(DB)はNULL及びゼロデータが存在しないので$need_tableの「必要人数」件数分に合わせる
			// 2012.4.17単独処理duty_shift_need_cnt_skillテーブル廃止、duty_shift_need_cnt_standardへ統合
/*
			$skill_work = array();
			$sql2 = "SELECT no,need_skill FROM duty_shift_need_cnt_skill WHERE group_id='$group_id' ORDER BY no;";
			$skill_sel = select_from_table($this->_db_con, $sql2, "", $this->file_name);
			if ($skill_sel == 0) {
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				echo("<!-- FROM duty_shift_need_cnt_standard -->");
				exit;
			}
			$skill_count = pg_numrows($skill_sel);
			for($skc=0; $skc<$skill_count ; $skc++){
				$wsk = pg_result($skill_sel,$skc,"no");
				$skill_work[$wsk]['need_skill']  = pg_result($skill_sel,$skc,"need_skill");
			}
			// スキル情報ここまで▲
*/
			// d_s_need_cnt_standard fetch
			while ( $row = pg_fetch_assoc($sel) ) {
				$wk_need_cnt = $need_cnt;
				foreach($wk_need_cnt as $date => $need_row) {
					//平日
					$wk_need_cnt[$date]["cnt"] = $row["week_cnt"];

					//祝日
					if ($need_row["type"] == "6") {
						$wk_need_cnt[$date]["cnt"] = $row["holiday_cnt"];
					}

					//七曜日
					elseif ($need_row["week"] == "1") {
						$wk_need_cnt[$date]["cnt"] = $row["mon_cnt"];
					}
					elseif ($need_row["week"] == "2") {
						$wk_need_cnt[$date]["cnt"] = $row["tue_cnt"];
					}
					elseif ($need_row["week"] == "3") {
						$wk_need_cnt[$date]["cnt"] = $row["wed_cnt"];
					}
					elseif ($need_row["week"] == "4") {
						$wk_need_cnt[$date]["cnt"] = $row["thu_cnt"];
					}
					elseif ($need_row["week"] == "5") {
						$wk_need_cnt[$date]["cnt"] = $row["fri_cnt"];
					}
					elseif ($need_row["week"] == "6") {
						$wk_need_cnt[$date]["cnt"] = $row["sat_cnt"];
					}
					elseif ($need_row["week"] == "0") {
						$wk_need_cnt[$date]["cnt"] = $row["sun_cnt"];
					}
				}
				$row["need_cnt"] = $wk_need_cnt;
				$row["team"] = trim($row["team"]);
				$row["sex"] = trim($row["sex"]);
//				$wk_no = $row["no"]; // skill対応追加 2012.4.17廃止
//				$row["skill"] = $skill_work[$wk_no]['need_skill'];	// skill対応追加 2012.4.17廃止
				$row["skill"] = trim($row["need_skill"]);			// 2012.4.17追加
				$row["skill2"] = trim($row["need_skill2"]);			// 2014.1.21追加
				$need_table[] = $row;
			}
		}

		return $need_table;
	}

	/*************************************************************************/
	// 該当するneed_tableのインデックスを返す
	// 該当しなければnullを返す
	/*************************************************************************/
	function _search_index($atdptn_ptn_id, $job_id, $team_id, $sex, $st_id)
	{
		if ($this->_idx_cache[$atdptn_ptn_id][$job_id][$team_id][$sex][$st_id]) {
			return $this->_idx_cache[$atdptn_ptn_id][$job_id][$team_id][$sex][$st_id];
		}

		$idx = null;
		foreach ( $this->_need_table as $i => $need ) {
			//パターンが違う
			if ($atdptn_ptn_id != $need["atdptn_ptn_id"]){
				continue;
			}

			//職種が違う
			if ((!empty($need["job_id"]) or !empty($need["job_id2"]) or !empty($need["job_id3"])) and $job_id != $need["job_id"] and $job_id != $need["job_id2"] and $job_id != $need["job_id3"]) {
				continue;
			}

			//チームが違う
			if (!empty($need["team"]) and $team_id != $need["team"]) {
				continue;
			}

			//性別が違う
			if (!empty($need["sex"]) and $sex != $need["sex"]) {
				continue;
			}

			//役職が違う
			if (!empty($need["st_id"]) and $st_id != $need["st_id"]) {
				continue;
			}

			$idx = $i;
			break;
		}

		$this->_idx_cache[$atdptn_ptn_id][$job_id][$team_id][$sex][$st_id] = $idx;
		return $idx;
	}

	/*************************************************************************/
	// 現在のカウント
	/*************************************************************************/
	function count($date, $atdptn_ptn_id, $job_id, $team_id, $sex, $st_id)
	{
		$idx = $this->_search_index($atdptn_ptn_id, $job_id, $team_id, $sex, $st_id);
		if ( !is_null($idx) ) {
			return $this->_need_table[$idx]['need_cnt'][$date]['cnt'];
		}
	}

	/*************************************************************************/
	// 減算
	/*************************************************************************/
	function minus($date, $atdptn_ptn_id, $job_id, $team_id, $sex, $st_id)
	{
		$idx = $this->_search_index($atdptn_ptn_id, $job_id, $team_id, $sex, $st_id);
		if ( !is_null($idx) ) {
			$this->_need_table[$idx]['need_cnt'][$date]['cnt']--;
			return $this->_need_table[$idx]['need_cnt'][$date]['cnt'];
		}
	}

	/*************************************************************************/
	// 加算
	/*************************************************************************/
	function plus($date, $atdptn_ptn_id, $job_id, $team_id, $sex, $st_id)
	{
		$idx = $this->_search_index($atdptn_ptn_id, $job_id, $team_id, $sex, $st_id);
		if ( !is_null($idx) ) {
			$this->_need_table[$idx]['need_cnt'][$date]['cnt']++;
			return $this->_need_table[$idx]['need_cnt'][$date]['cnt'];
		}
	}

	/*************************************************************************/
	// 日付テーブル
	/*************************************************************************/
	function get_date_table()
	{
		return $this->_date_table;
	}

	/*************************************************************************/
	// 当日($date)の必要な勤務シフトを取得
	/*************************************************************************/
	function get_need_shift_array($date)
	{
		$shift_array = array();
		foreach ( $this->_need_table as $i => $row ) {
			if ( $row["need_cnt"][$date]["cnt"] > 0 ) {
				for ( $i=0; $i<$row["need_cnt"][$date]["cnt"]; $i++ ) {
					$shift_array[] = array(
							"atdptn_ptn_id" => $row["atdptn_ptn_id"],
							"job_id" => $row["job_id"],
							"job_id2" => $row["job_id2"],
							"job_id3" => $row["job_id3"],
							"team" => $row["team"],
							"sex" => $row["sex"],
							"st_id" => $row["st_id"],
							"skill" => $row["skill"], // skill対応追加
							"skill2" => $row["skill2"]  // skill対応追加
					);
				}
			}
		}
		return $shift_array;
	}

	/*************************************************************************/
	// 当日($date)の不足勤務シフトを取得
	/*************************************************************************/
	function get_less_shift($date)
	{
		$shift_array = array();
		foreach ( $this->_need_table as $i => $row ) {
			if ( $row["need_cnt"][$date]["cnt"] > 0 ) {

				$job_name = $row["job1_name"];
				$job_name .= !empty($row["job_id2"]) ? "・{$row["job2_name"]}" : "";
				$job_name .= !empty($row["job_id3"]) ? "・{$row["job3_name"]}" : "";
				$job_name .= !empty($row["st_id"])   ? "-{$row["st_name"]}" : "";
				$job_name .= !empty($row["team"])    ? "-{$row["team_name"]}" : "";
				$job_name .= !empty($row["sex"])     ? "-{$row["sex_name"]}" : "";

				$shift_array[] = array(
						"cnt" => $row["need_cnt"][$date]["cnt"],
						"atdptn_ptn_id" => $row["atdptn_ptn_id"],
						"job_id" => $row["job_id"],
						"job_id2" => $row["job_id2"],
						"job_id3" => $row["job_id3"],
						"team" => $row["team"],
						"sex" => $row["sex"],
						"st_id" => $row["st_id"],
						"ptn_name" => $row["ptn_name"],
						"job_name" => $job_name
						);
			}
		}
		return $shift_array;
	}

	/*************************************************************************/
	// 当日($date)の過剰勤務シフトを取得
	/*************************************************************************/
	function get_more_shift($date)
	{
		$shift_array = array();
		foreach ( $this->_need_table as $i => $row ) {
			if ( $row["need_cnt"][$date]["cnt"] < 0 ) {

				$job_name = $row["job1_name"];
				$job_name .= !empty($row["job_id2"]) ? "・{$row["job2_name"]}" : "";
				$job_name .= !empty($row["job_id3"]) ? "・{$row["job3_name"]}" : "";
				$job_name .= !empty($row["st_id"])   ? "-{$row["st_name"]}" : "";
				$job_name .= !empty($row["team"])    ? "-{$row["team_name"]}" : "";
				$job_name .= !empty($row["sex"])     ? "-{$row["sex_name"]}" : "";

				$shift_array[] = array(
						"cnt" => abs($row["need_cnt"][$date]["cnt"]),
						"atdptn_ptn_id" => $row["atdptn_ptn_id"],
						"job_id" => $row["job_id"],
						"job_id2" => $row["job_id2"],
						"job_id3" => $row["job_id3"],
						"team" => $row["team"],
						"sex" => $row["sex"],
						"st_id" => $row["st_id"],
						"ptn_name" => $row["ptn_name"],
						"job_name" => $job_name
						);
			}
		}
		return $shift_array;
	}
	/*************************************************************************/
	// 必要人数テーブル 2012.4.17
	/*************************************************************************/
	function get_need_table()
	{
		return $this->_need_table;
	}

	/*************************************************************************/
	// Debug: need_table_dump
	/*************************************************************************/
/*
	function debug_need_table_dump()
	{
		$table = $this->_need_table;
		error_log(date("\nY/m/d G:i:s\n"), 3, "log/autoshift.debug.log");

		//week
		error_log("                 week|", 3, "log/autoshift.debug.log");
		foreach ( $table[0]['need_cnt'] as $date => $row ) {
			error_log(sprintf("%2d|", $row['week']), 3, "log/autoshift.debug.log");
		}
		error_log("\n", 3, "log/autoshift.debug.log");

		//type
		error_log("                 type|", 3, "log/autoshift.debug.log");
		foreach ( $table[0]['need_cnt'] as $date => $row ) {
			error_log(sprintf("%2d|", $row['type']), 3, "log/autoshift.debug.log");
		}
		error_log("\n", 3, "log/autoshift.debug.log");

		//day
		error_log(" i|at|j1-j2-j3|t|s|st|", 3, "log/autoshift.debug.log");
		foreach ( $table[0]['need_cnt'] as $date => $row ) {
			error_log(sprintf("%2d|", substr($date,-2)), 3, "log/autoshift.debug.log");
		}
		error_log("\n", 3, "log/autoshift.debug.log");

		foreach ( $table as $i => $need ) {
			error_log(sprintf(
						"%2d|%2s|%2s-%2s-%2s|%1s|%1s|%2s|",
						$i,
						$need['atdptn_ptn_id'],
						$need['job_id'],
						$need['job_id2'],
						$need['job_id3'],
						$need['team'],
						$need['sex'],
						$need['st_id']
						), 3, "log/autoshift.debug.log");
			foreach ( $need['need_cnt'] as $date => $row ) {
				error_log(sprintf("%2d|", $row['cnt']), 3, "log/autoshift.debug.log");
			}
			error_log("\n", 3, "log/autoshift.debug.log");
		}
	}
*/
}