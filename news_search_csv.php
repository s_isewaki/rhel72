<?

ob_start();
require_once("about_session.php");
require_once("about_authority.php");
require_once("news_common.ini");
require_once("show_news_search.ini");
require_once("Cmx.php");
require_once("aclg_set.php");
ob_end_clean();

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 24, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

$csv_header = "登録日,カテゴリ,タイトル,職員ID,氏名,所属,職種,確認日時,コメント";
$csv_header = mb_convert_encoding($csv_header, "Shift_JIS", "EUC-JP");

// データベースに接続
$con = connect2db($fname);

$news_list = get_news_list($con, $fname);
$csv_body = get_csv_body($con, $news_list, $fname);

$file_name = "news.csv";
$csv = $csv_header . "\r\n" . $csv_body;
header("Content-Disposition: attachment; filename=$file_name");
header("Content-Type: application/octet-stream; name=$file_name");
header("Content-Length: " . strlen($csv));
echo($csv);

// アクセスログ（「内容」でファイル名を取得します。）
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"], $_POST, $con, $file_name);

// データベース接続を閉じる
pg_close($con);

function get_news_list($con, $fname) {
    $sql = "select n.news_id from news n inner join newscate c on n.news_add_category = c.newscate_id";
    $cond = get_news_search_condition() . " order by n.news_date desc, n.news_id desc";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    $news_ids = array();
    while ($row = pg_fetch_array($sel)) {
        $news_ids[] = $row["news_id"];
    }
    return $news_ids;
}

function get_csv_body($con, $news_list, $fname) {
    $buf = "";

    foreach ($news_list as $news_id) {

        // お知らせ情報の取得
        $sql = "select n.news_date, c.newscate_name, n.news_title, n.news_add_category, n.job_id as job_id1, n.st_id_cnt as st_id_cnt1, c.standard_cate_id, c.job_id as job_id2, c.st_id_cnt as st_id_cnt2, c.newscate_id, c.newscate_name from news n inner join newscate c on n.news_add_category = c.newscate_id";
        $cond = "where news_id = $news_id";
        $sel_news = select_from_table($con, $sql, $cond, $fname);
        if ($sel_news == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $news_date = pg_fetch_result($sel_news, 0, "news_date");
        $news_date = preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $news_date);
        $news_category_name = pg_fetch_result($sel_news, 0, "newscate_name");
        $news_category = pg_fetch_result($sel_news, 0, "news_add_category");
        $news_title = pg_fetch_result($sel_news, 0, "news_title");
        $standard_cate_id = pg_fetch_result($sel_news, 0, "standard_cate_id");
        $newscate_id = pg_fetch_result($sel_news, 0, "newscate_id");

        // 標準カテゴリの場合
        if ($news_category <= "3") {
            $st_id_cnt = pg_fetch_result($sel_news, 0, "st_id_cnt1");
            $job_id = pg_fetch_result($sel_news, 0, "job_id1");

        }
        // 追加カテゴリの場合
        else {
            $st_id_cnt = pg_fetch_result($sel_news, 0, "st_id_cnt2");
            $job_id = pg_fetch_result($sel_news, 0, "job_id2");
        }

        // 役職指定数がある場合条件追加
        $st_cond = "";
        if ($st_id_cnt > 0) {
            if ($news_category <= "3") {
                $newsst_info = get_newsst_info($con, $fname, $news_id);
            }
            else {
                $newsst_info = get_newscatest_info($con, $fname, $newscate_id);
            }
            $st_ids = join(",", array_keys($newsst_info));
            $st_cond .= " and (empmst.emp_st in ($st_ids) or empmst.emp_id in (select emp_id from concurrent where emp_st in ($st_ids)))";
        }

        // 回覧板
        if ($news_category == 10000) {
            $sql = "select empmst.emp_id, empmst.emp_personal_id, empmst.emp_lt_nm, empmst.emp_ft_nm, newscomment.update_time, newscomment.comment, newsref.ref_time, classmst.class_nm, atrbmst.atrb_nm, deptmst.dept_nm, classroom.room_nm, stmst.st_nm from newsref inner join empmst on newsref.emp_id = empmst.emp_id left join newscomment on newscomment.news_id = $news_id and newscomment.emp_id = empmst.emp_id left join classmst on classmst.class_id = empmst.emp_class left join atrbmst on atrbmst.atrb_id = empmst.emp_attribute left join deptmst on deptmst.dept_id = empmst.emp_dept left join classroom on classroom.room_id = empmst.emp_room left join stmst on stmst.st_id = empmst.emp_st";
            $cond = "where newsref.news_id = $news_id order by emp_kn_lt_nm, emp_kn_ft_nm ";
        }
        else {
            $sql = "select empmst.emp_id, empmst.emp_personal_id, empmst.emp_lt_nm, empmst.emp_ft_nm, (case when newscomment.update_time is null then newsref.ref_time else newscomment.update_time end) as update_time, newscomment.comment, newsref.ref_time, classmst.class_nm, atrbmst.atrb_nm, deptmst.dept_nm, classroom.room_nm, stmst.st_nm from newsref inner join empmst on newsref.emp_id = empmst.emp_id left join newscomment on newscomment.news_id = $news_id and newscomment.emp_id = empmst.emp_id left join classmst on classmst.class_id = empmst.emp_class left join atrbmst on atrbmst.atrb_id = empmst.emp_attribute left join deptmst on deptmst.dept_id = empmst.emp_dept left join classroom on classroom.room_id = empmst.emp_room left join stmst on stmst.st_id = empmst.emp_st";
            $cond = "where newsref.news_id = $news_id order by emp_kn_lt_nm, emp_kn_ft_nm ";
        }
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }

        while ($row = pg_fetch_array($sel)) {
            $tmp_emp_personal_id = $row["emp_personal_id"];

            $tmp_emp_lt_nm = $row["emp_lt_nm"];
            $tmp_emp_ft_nm = $row["emp_ft_nm"];
            $tmp_emp_name = "$tmp_emp_lt_nm $tmp_emp_ft_nm";

            $tmp_position = "{$row["class_nm"]}＞{$row["atrb_nm"]}＞{$row["dept_nm"]}";
            if ($row["room_nm"] != "") {
                $tmp_position .= "＞{$row["room_nm"]}";
            }

            $tmp_st_nm = $row["st_nm"];

            $tmp_ref_time = $row["update_time"];
            $tmp_ref_time = preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})$/", "$1/$2/$3 $4:$5", $tmp_ref_time);

            $comment = $row["comment"];

            // レコードの追加
            $buf .= format_csv_value($news_date) . ",";
            $buf .= format_csv_value($news_category_name) . ",";
            $buf .= format_csv_value($news_title) . ",";
            $buf .= format_csv_value($tmp_emp_personal_id) . ",";
            $buf .= format_csv_value($tmp_emp_name) . ",";
            $buf .= format_csv_value($tmp_position) . ",";
            $buf .= format_csv_value($tmp_st_nm) . ",";
            $buf .= format_csv_value($tmp_ref_time) . ",";
            $buf .= format_csv_value($comment);
            $buf .= "\r\n";
        }
    }

    return $buf;
}

function format_csv_value($value) {
    $buf = str_replace("\r", "", $value);
    $buf = str_replace("\n", "", $buf);
    if (strpos($buf, ",") !== false) {
        $buf = '"' . $buf . '"';
    }
    return mb_convert_encoding($buf, "Shift_JIS", "EUC-JP");
}
