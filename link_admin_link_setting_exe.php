<?php
/*************************************************************************
リンクライブラリ管理
  リンク名表示設定・更新
*************************************************************************/
require_once("Cmx.php");
require_once("aclg_set.php");
require_once("about_comedix.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック(リンクライブラリ管理[61])
$check_auth = check_authority($session, 61, $fname);
if ($check_auth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 登録情報を配列化
$links = array();
$link_ids = array();
for ($i = 0, $j = count($link_nm); $i < $j; $i++) {
	if ($link_nm[$i] == "") {
		continue;
	}

	$links[] = array(
			"link_id"	=> $link_id[$i],
			"link_type" => $link_type[$i],
			"link_nm"	=> p($link_nm[$i]),
			"link_url"	=> p($link_url[$i]),
			"link_note" => $link_note[$i],
			"note_flg"	=> ($note_flg[$i] == "t") ? "t" : "f",
			"show_flg1" => ($show_flg1[$i] == "t") ? "t" : "f",
			"icon_flg1" => ($show_flg1[$i] == "t") ? (($icon_flg1[$i]=="t") ? "t" : "f") : "f",
			"order1"	=> ($show_flg1[$i] == "t") ? max(intval($order1[$i]), 1) : null,
			"show_flg2" => ($show_flg2[$i] == "t") ? "t" : "f",
			"icon_flg2" => ($show_flg2[$i] == "t") ? (($icon_flg2[$i]=="t") ? "t" : "f") : "f",
			"order2"	=> ($show_flg2[$i] == "t") ? max(intval($order2[$i]), 1) : null,
			"show_flg3" => ($show_flg3[$i] == "t") ? "t" : "f",
			"icon_flg3" => ($show_flg3[$i] == "t") ? (($icon_flg3[$i]=="t") ? "t" : "f") : "f",
			"order3"	=> ($show_flg3[$i] == "t") ? max(intval($order3[$i]), 1) : null
			);
	
	if ($link_id[$i] != "") {
		$link_ids[] = $link_id[$i];
	}
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// トランザクションの開始
pg_query($con, "begin");

// 削除されたリンクIDを取得
$sql = "SELECT link_id FROM link";
$cond = "WHERE linkcate_id=$linkcate_id";
$cond .= (count($link_ids) > 0) ? " AND link_id NOT IN (" . implode(",", $link_ids) . ")" : "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$del_ids = array();
while ($row = pg_fetch_assoc($sel)) {
	$del_ids[] = $row['link_id'];
}

if (count($del_ids) > 0) {
	// 登録／更新対象リンクに所属しないリンク情報を削除（選択カテゴリのみ）
	$sql = "DELETE FROM link";
	$cond = "WHERE link_id IN (" . implode(",", $del_ids) . ")";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// 登録／更新対象リンクに所属しないパラメータ情報を削除（選択カテゴリのみ）
	$sql = "DELETE FROM link_param";
	$cond = "WHERE link_id IN (" . implode(",", $del_ids) . ")";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// リンク情報を登録
foreach ($links as $link) {
	
	// リンクID未採番なら登録処理を実行
	if ($link["link_id"] == "") {
		$sql = "select max(link_id) from link";
		$cond = "";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$link["link_id"] = intval(pg_fetch_result($sel, 0, 0)) + 1;
		
		$sql = <<<_SQL_END_
INSERT INTO link (
	link_id, 
	link_type, 
	link_nm, 
	link_url, 
	link_note, 
	note_flg, 
	linkcate_id, 
	show_flg1, 
	icon_flg1, 
	order1, 
	show_flg2, 
	icon_flg2, 
	order2, 
	show_flg3, 
	icon_flg3, 
	order3
) values (
_SQL_END_;
		$content = array(
				$link["link_id"], 
				$link["link_type"], 
				$link["link_nm"], 
				$link["link_url"], 
				$link["link_note"], 
				$link["note_flg"], 
				$linkcate_id, 
				$link["show_flg1"], 
				$link["icon_flg1"], 
				$link["order1"], 
				$link["show_flg2"], 
				$link["icon_flg2"], 
				$link["order2"], 
				$link["show_flg3"], 
				$link["icon_flg3"], 
				$link["order3"]
				);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		// リンクID採番済みなら更新処理を実行
	} else {
		$sql = "update link set";
		$set = array(
				"link_type", 
				"link_nm", 
				"link_url", 
				"link_note", 
				"note_flg", 
				"show_flg1", 
				"icon_flg1", 
				"order1", 
				"show_flg2", 
				"icon_flg2", 
				"order2", 
				"show_flg3", 
				"icon_flg3", 
				"order3"
				);
		$setvalue = array(
				$link["link_type"], 
				$link["link_nm"], 
				$link["link_url"], 
				$link["link_note"], 
				$link["note_flg"], 
				$link["show_flg1"], 
				$link["icon_flg1"], 
				$link["order1"], 
				$link["show_flg2"], 
				$link["icon_flg2"], 
				$link["order2"], 
				$link["show_flg3"], 
				$link["icon_flg3"], 
				$link["order3"]
				);
		$cond = "where link_id = {$link["link_id"]}";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
}

// トランザクションのコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);
?>
<script type="text/javascript">
location.href = 'link_admin_link_setting.php?session=<? eh($session);?>&linkcate_id=<? eh($linkcate_id);?>';
</script>