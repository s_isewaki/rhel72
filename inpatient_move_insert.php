<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<?
$ret_url = ($direct == "t") ? "inpatient_move_reserve_register.php" : "inpatient_move_register.php";
?>
<form name="items" method="post" action="<? echo($ret_url); ?>">
<input type="hidden" name="back" value="t">
<input type="hidden" name="enti" value="<? echo($enti); ?>">
<input type="hidden" name="ward" value="<? echo($ward); ?>">
<input type="hidden" name="ptrm" value="<? echo($ptrm); ?>">
<input type="hidden" name="bed" value="<? echo($bed); ?>">
<input type="hidden" name="mv_yr" value="<? echo($mv_yr); ?>">
<input type="hidden" name="mv_mon" value="<? echo($mv_mon); ?>">
<input type="hidden" name="mv_day" value="<? echo($mv_day); ?>">
<input type="hidden" name="mv_hr" value="<? echo($mv_hr); ?>">
<input type="hidden" name="mv_min" value="<? echo($mv_min); ?>">
<input type="hidden" name="sect" value="<? echo($sect); ?>">
<input type="hidden" name="doc" value="<? echo($doc); ?>">
<input type="hidden" name="nurse" value="<? echo($nurse); ?>">
<input type="hidden" name="reason" value="<? echo($reason); ?>">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="pt_id" value="<? echo($pt_id); ?>">
<input type="hidden" name="direct" value="<? echo($direct); ?>">
</form>
<?
require("about_session.php");
require("about_authority.php");
require("inpatient_common.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// チェック情報を取得
$sql = "select required1 from bedcheck";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$required1 = pg_fetch_result($sel, 0, "required1");

// 入力チェック
if (!checkdate($mv_mon, $mv_day, $mv_yr)) {
	echo("<script type=\"text/javascript\">alert('日付が不正です。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}
if ($required1 == "t" && ($nurse == "" || $nurse == 0)) {
	echo("<script type=\"text/javascript\">alert('担当看護師を選択してください。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

// 移転日時の編集
$date = "$mv_yr$mv_mon$mv_day";
$time = sprintf("%02d%02d", $mv_hr, $mv_min);
list($bldg_cd, $ward_cd) = split("-", $ward);

// トランザクションを開始
pg_query($con, "begin");

// 関連チェック
$sql = "select move_dt, move_tm from inptmove";
$cond = "where ptif_id = '$pt_id' and move_cfm_flg = 't' and move_del_flg = 'f' and (move_dt > '$date' or (move_dt = '$date' and move_tm >= '$time')) order by move_dt desc, move_tm desc";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if (pg_num_rows($sel) > 0) {
	pg_query($con, "rollback");
	pg_close($con);
	$selected_move_date = preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $date);
	$selected_move_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $time);
	$last_move_date = preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", pg_fetch_result($sel, 0, "move_dt"));
	$last_move_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", pg_fetch_result($sel, 0, "move_tm"));
	echo("<script type=\"text/javascript\">alert('指定の転床日時（{$selected_move_date} {$selected_move_time}）が\\n最新の転床日時（{$last_move_date} {$last_move_time}）を越えないため、登録できません。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}
$sql = "select inpt_res_flg, inpt_in_flg, inpt_out_res_flg, ptif_id from inptmst i";
$cond = "where bldg_cd = '$bldg_cd' and ward_cd = '$ward_cd' and ptrm_room_no = '$ptrm' and inpt_bed_no = '$bed'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if (pg_fetch_result($sel, 0, "inpt_res_flg") == "t") {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('指定された病床には入院予定患者が存在します。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}
if (pg_fetch_result($sel, 0, "inpt_in_flg") == "t" || pg_fetch_result($sel, 0, "inpt_out_res_flg") == "t") {
	$target_pt_id = pg_fetch_result($sel, 0, "ptif_id");

	$sql = "select move_dt, move_tm from inptmove";
	$cond = "where ptif_id = '$target_pt_id' and move_cfm_flg = 't' and move_del_flg = 'f' and (move_dt > '$date' or (move_dt = '$date' and move_tm >= '$time')) order by move_dt desc, move_tm desc";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0) {
		pg_query($con, "rollback");
		pg_close($con);
		$selected_move_date = preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $date);
		$selected_move_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $time);
		$last_move_date = preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", pg_fetch_result($sel, 0, "move_dt"));
		$last_move_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", pg_fetch_result($sel, 0, "move_tm"));
		echo("<script type=\"text/javascript\">alert('指定された病床には入院患者：{$target_pt_id}が存在します。\\n\\n指定の転床日時（{$selected_move_date} {$selected_move_time}）が、患者：{$target_pt_id}の\\n最新の転床日時（{$last_move_date} {$last_move_time}）を越えないため、登録できません。');</script>\n");
		echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
		exit;
	}

	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">");
	if (strcmp($target_pt_id, $pt_id) == 0) {
		echo("document.items.action = 'inpatient_move_change.php';");
	} else {
		echo("if (confirm('指定された病床には入院患者が存在します。\\nベッド交換しますか？')) {");
		echo("document.items.action = 'inpatient_move_change.php';");
		echo("}");
	}
	echo("document.items.submit();");
	echo("</script>\n");
	exit;
}
$sql = "select count(*) from inptmove";
$cond = "where to_bldg_cd = '$bldg_cd' and to_ward_cd = '$ward_cd' and to_ptrm_room_no = '$ptrm' and to_bed_no = '$bed' and move_cfm_flg = 'f' and move_del_flg = 'f' and ptif_id <> '$pt_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if (pg_fetch_result($sel, 0, 0) > 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('指定された病床には転床予定患者が存在します。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

// 未確定の移転情報を削除
$sql = "select move_dt from inptmove";
$cond = "where ptif_id = '$pt_id' and move_cfm_flg = 'f' and move_del_flg = 'f'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
while ($row = pg_fetch_array($sel)) {
	$sql = "delete from inptcond";
	$cond = "where ptif_id = '$pt_id' and date = '{$row["move_dt"]}' and hist_flg = 'f'";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}
$sql = "update inptmove set";
$set = array("move_del_flg");
$setvalue = array("t");
$cond = "where ptif_id = '$pt_id' and move_cfm_flg = 'f' and move_del_flg = 'f'";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 入院状況レコードを更新
update_inpatient_condition($con, $pt_id, $date, $time, "3", $session, $fname, true);

// 入院情報を取得
$sql = "select * from inptmst";
$cond = "where ptif_id = '$pt_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$from_enti_id = pg_fetch_result($sel, 0, "inpt_enti_id");
$from_bldg_cd = pg_fetch_result($sel, 0, "bldg_cd");
$from_ward_cd = pg_fetch_result($sel, 0, "ward_cd");
$from_ptrm_room_no = pg_fetch_result($sel, 0, "ptrm_room_no");
$from_bed_no = pg_fetch_result($sel, 0, "inpt_bed_no");
$inpt_arrival = pg_fetch_result($sel, 0, "inpt_arrival");
$from_sect_id = pg_fetch_result($sel, 0, "inpt_sect_id");
$from_dr_id = pg_fetch_result($sel, 0, "dr_id");
$from_nurse_id = pg_fetch_result($sel, 0, "nurse_id");
$inpt_lt_kn_nm = pg_fetch_result($sel, 0, "inpt_lt_kn_nm");
$inpt_ft_kn_nm = pg_fetch_result($sel, 0, "inpt_ft_kn_nm");
$inpt_lt_kj_nm = pg_fetch_result($sel, 0, "inpt_lt_kj_nm");
$inpt_ft_kj_nm = pg_fetch_result($sel, 0, "inpt_ft_kj_nm");
$inpt_keywd = pg_fetch_result($sel, 0, "inpt_keywd");
$inpt_vacant_flg = pg_fetch_result($sel, 0, "inpt_vacant_flg");
$inpt_res_flg = pg_fetch_result($sel, 0, "inpt_res_flg");
$inpt_in_dt = pg_fetch_result($sel, 0, "inpt_in_dt");
$inpt_in_flg = pg_fetch_result($sel, 0, "inpt_in_flg");
$inpt_out_dt = pg_fetch_result($sel, 0, "inpt_out_dt");
$inpt_out_flg = pg_fetch_result($sel, 0, "inpt_out_flg");
$inpt_out_res_dt = pg_fetch_result($sel, 0, "inpt_out_res_dt");
$inpt_out_res_flg = pg_fetch_result($sel, 0, "inpt_out_res_flg");
$inpt_insurance = pg_fetch_result($sel, 0, "inpt_insurance");
$inpt_insu_rate_rireki = pg_fetch_result($sel, 0, "inpt_insu_rate_rireki");
if ($inpt_insu_rate_rireki == "") {$inpt_insu_rate_rireki = null;}
$inpt_insu_rate_cd = pg_fetch_result($sel, 0, "inpt_insu_rate_cd");
$inpt_reduced = pg_fetch_result($sel, 0, "inpt_reduced");
if ($inpt_reduced == "") {$inpt_reduced = null;}
$inpt_impaired = pg_fetch_result($sel, 0, "inpt_impaired");
if ($inpt_impaired == "") {$inpt_impaired = null;}
$inpt_insured = pg_fetch_result($sel, 0, "inpt_insured");
$inpt_meet_flg = pg_fetch_result($sel, 0, "inpt_meet_flg");
$inpt_dcb_cls = pg_fetch_result($sel, 0, "inpt_dcb_cls");
$inpt_dcb_bas = pg_fetch_result($sel, 0, "inpt_dcb_bas");
$inpt_dcb_exp = pg_fetch_result($sel, 0, "inpt_dcb_exp");
$inpt_rhb_cls = pg_fetch_result($sel, 0, "inpt_rhb_cls");
$inpt_rhb_bas = pg_fetch_result($sel, 0, "inpt_rhb_bas");
$inpt_rhb_exp = pg_fetch_result($sel, 0, "inpt_rhb_exp");
$inpt_ecg_flg = pg_fetch_result($sel, 0, "inpt_ecg_flg");
$inpt_sample_blood = pg_fetch_result($sel, 0, "inpt_sample_blood");
$inpt_observe = pg_fetch_result($sel, 0, "inpt_observe");
$inpt_purpose_rireki = pg_fetch_result($sel, 0, "inpt_purpose_rireki");
if ($inpt_purpose_rireki == "") {$inpt_purpose_rireki = null;}
$inpt_purpose_cd = pg_fetch_result($sel, 0, "inpt_purpose_cd");
$inpt_purpose_content = pg_fetch_result($sel, 0, "inpt_purpose_content");
$inpt_short_stay = pg_fetch_result($sel, 0, "inpt_short_stay");
if ($inpt_short_stay == "") {$inpt_short_stay = null;}
$inpt_outpt_test = pg_fetch_result($sel, 0, "inpt_outpt_test");
if ($inpt_outpt_test == "") {$inpt_outpt_test = null;}
$inpt_diagnosis = pg_fetch_result($sel, 0, "inpt_diagnosis");
$inpt_nrs_obsv = pg_fetch_result($sel, 0, "inpt_nrs_obsv");
$inpt_free = pg_fetch_result($sel, 0, "inpt_free");
$inpt_special = pg_fetch_result($sel, 0, "inpt_special");
$inpt_disease = pg_fetch_result($sel, 0, "inpt_disease");
$inpt_patho_from = pg_fetch_result($sel, 0, "inpt_patho_from");
$inpt_patho_to = pg_fetch_result($sel, 0, "inpt_patho_to");
$inpt_up_dt = pg_fetch_result($sel, 0, "inpt_up_dt");
$inpt_up_tm = pg_fetch_result($sel, 0, "inpt_up_tm");
$inpt_op_no = pg_fetch_result($sel, 0, "inpt_op_no");
$inpt_in_tm = pg_fetch_result($sel, 0, "inpt_in_tm");
$inpt_out_tm = pg_fetch_result($sel, 0, "inpt_out_tm");
$inpt_out_res_tm = pg_fetch_result($sel, 0, "inpt_out_res_tm");
$inpt_in_res_dt_flg = pg_fetch_result($sel, 0, "inpt_in_res_dt_flg");
$inpt_in_res_dt = pg_fetch_result($sel, 0, "inpt_in_res_dt");
$inpt_in_res_tm = pg_fetch_result($sel, 0, "inpt_in_res_tm");
$inpt_in_res_ym = pg_fetch_result($sel, 0, "inpt_in_res_ym");
$inpt_in_res_td = pg_fetch_result($sel, 0, "inpt_in_res_td");
$inpt_ope_dt_flg = pg_fetch_result($sel, 0, "inpt_ope_dt_flg");
$inpt_ope_dt = pg_fetch_result($sel, 0, "inpt_ope_dt");
$inpt_ope_ym = pg_fetch_result($sel, 0, "inpt_ope_ym");
$inpt_ope_td = pg_fetch_result($sel, 0, "inpt_ope_td");
$inpt_emergency = pg_fetch_result($sel, 0, "inpt_emergency");
$inpt_in_plan_period = pg_fetch_result($sel, 0, "inpt_in_plan_period");
$inpt_in_way = pg_fetch_result($sel, 0, "inpt_in_way");
$inpt_nursing = pg_fetch_result($sel, 0, "inpt_nursing");
$inpt_staple_fd = pg_fetch_result($sel, 0, "inpt_staple_fd");
$inpt_fd_type = pg_fetch_result($sel, 0, "inpt_fd_type");
$inpt_fd_dtl = pg_fetch_result($sel, 0, "inpt_fd_dtl");
$inpt_wish_rm_rireki = pg_fetch_result($sel, 0, "inpt_wish_rm_rireki");
if ($inpt_wish_rm_rireki == "") {$inpt_wish_rm_rireki = null;}
$inpt_wish_rm_cd = pg_fetch_result($sel, 0, "inpt_wish_rm_cd");
$inpt_fd_start = pg_fetch_result($sel, 0, "inpt_fd_start");
$inpt_result = pg_fetch_result($sel, 0, "inpt_result");
$inpt_result_dtl = pg_fetch_result($sel, 0, "inpt_result_dtl");
$inpt_out_rsn_rireki = pg_fetch_result($sel, 0, "inpt_out_rsn_rireki");
if ($inpt_out_rsn_rireki == "") {$inpt_out_rsn_rireki = null;}
$inpt_out_rsn_cd = pg_fetch_result($sel, 0, "inpt_out_rsn_cd");
$inpt_out_pos_rireki = pg_fetch_result($sel, 0, "inpt_out_pos_rireki");
if ($inpt_out_pos_rireki == "") {$inpt_out_pos_rireki = null;}
$inpt_out_pos_cd = pg_fetch_result($sel, 0, "inpt_out_pos_cd");
$inpt_out_pos_dtl = pg_fetch_result($sel, 0, "inpt_out_pos_dtl");
$inpt_fd_end = pg_fetch_result($sel, 0, "inpt_fd_end");
$inpt_out_comment = pg_fetch_result($sel, 0, "inpt_out_comment");
$inpt_res_chg_psn = pg_fetch_result($sel, 0, "inpt_res_chg_psn");
$inpt_res_chg_psn_dtl = pg_fetch_result($sel, 0, "inpt_res_chg_psn_dtl");
$inpt_res_chg_rsn = pg_fetch_result($sel, 0, "inpt_res_chg_rsn");
$inpt_res_chg_ctt = pg_fetch_result($sel, 0, "inpt_res_chg_ctt");
$inpt_except_flg = pg_fetch_result($sel, 0, "inpt_except_flg");
$inpt_except_from_date1 = pg_fetch_result($sel, 0, "inpt_except_from_date1");
$inpt_except_to_date1 = pg_fetch_result($sel, 0, "inpt_except_to_date1");
$inpt_except_from_date2 = pg_fetch_result($sel, 0, "inpt_except_from_date2");
$inpt_except_to_date2 = pg_fetch_result($sel, 0, "inpt_except_to_date2");
$inpt_except_from_date3 = pg_fetch_result($sel, 0, "inpt_except_from_date3");
$inpt_except_to_date3 = pg_fetch_result($sel, 0, "inpt_except_to_date3");
$inpt_except_from_date4 = pg_fetch_result($sel, 0, "inpt_except_from_date4");
$inpt_except_to_date4 = pg_fetch_result($sel, 0, "inpt_except_to_date4");
$inpt_except_from_date5 = pg_fetch_result($sel, 0, "inpt_except_from_date5");
$inpt_except_to_date5 = pg_fetch_result($sel, 0, "inpt_except_to_date5");
$inpt_privacy_flg = pg_fetch_result($sel, 0, "inpt_privacy_flg");
$inpt_privacy_text = pg_fetch_result($sel, 0, "inpt_privacy_text");
$inpt_pre_div = pg_fetch_result($sel, 0, "inpt_pre_div");
$inpt_except_reason_id1 = pg_fetch_result($sel, 0, "inpt_except_reason_id1");
if ($inpt_except_reason_id1 == "") {$inpt_except_reason_id1 = null;}
$inpt_except_reason_id2 = pg_fetch_result($sel, 0, "inpt_except_reason_id2");
if ($inpt_except_reason_id2 == "") {$inpt_except_reason_id2 = null;}
$inpt_except_reason_id3 = pg_fetch_result($sel, 0, "inpt_except_reason_id3");
if ($inpt_except_reason_id3 == "") {$inpt_except_reason_id3 = null;}
$inpt_except_reason_id4 = pg_fetch_result($sel, 0, "inpt_except_reason_id4");
if ($inpt_except_reason_id4 == "") {$inpt_except_reason_id4 = null;}
$inpt_except_reason_id5 = pg_fetch_result($sel, 0, "inpt_except_reason_id5");
if ($inpt_except_reason_id5 == "") {$inpt_except_reason_id5 = null;}
$inpt_out_inst_cd = pg_fetch_result($sel, 0, "inpt_out_inst_cd");
$inpt_out_sect_rireki = pg_fetch_result($sel, 0, "inpt_out_sect_rireki");
if ($inpt_out_sect_rireki == "") {$inpt_out_sect_rireki = null;}
$inpt_out_sect_cd = pg_fetch_result($sel, 0, "inpt_out_sect_cd");
$inpt_out_doctor_no = pg_fetch_result($sel, 0, "inpt_out_doctor_no");
if ($inpt_out_doctor_no == "") {$inpt_out_doctor_no = null;}
$inpt_out_city = pg_fetch_result($sel, 0, "inpt_out_city");
$inpt_back_bldg_cd = pg_fetch_result($sel, 0, "inpt_back_bldg_cd");
if ($inpt_back_bldg_cd == "") {$inpt_back_bldg_cd = null;}
$inpt_back_ward_cd = pg_fetch_result($sel, 0, "inpt_back_ward_cd");
if ($inpt_back_ward_cd == "") {$inpt_back_ward_cd = null;}
$inpt_back_ptrm_room_no = pg_fetch_result($sel, 0, "inpt_back_ptrm_room_no");
if ($inpt_back_ptrm_room_no == "") {$inpt_back_ptrm_room_no = null;}
$inpt_back_bed_no = pg_fetch_result($sel, 0, "inpt_back_bed_no");
if ($inpt_back_bed_no == "") {$inpt_back_bed_no = null;}
$inpt_back_in_dt = pg_fetch_result($sel, 0, "inpt_back_in_dt");
if ($inpt_back_in_dt == "") {$inpt_back_in_dt = null;}
$inpt_back_in_tm = pg_fetch_result($sel, 0, "inpt_back_in_tm");
if ($inpt_back_in_tm == "") {$inpt_back_in_tm = null;}
$inpt_back_sect_id = pg_fetch_result($sel, 0, "inpt_back_sect_id");
if ($inpt_back_sect_id == "") {$inpt_back_sect_id = null;}
$inpt_back_dr_id = pg_fetch_result($sel, 0, "inpt_back_dr_id");
if ($inpt_back_dr_id == "") {$inpt_back_dr_id = null;}
$inpt_back_change_purpose = pg_fetch_result($sel, 0, "inpt_back_change_purpose");
$inpt_care_no = pg_fetch_result($sel, 0, "inpt_care_no");
$inpt_care_grd_rireki = pg_fetch_result($sel, 0, "inpt_care_grd_rireki");
if ($inpt_care_grd_rireki == "") {$inpt_care_grd_rireki = null;}
$inpt_care_grd_cd = pg_fetch_result($sel, 0, "inpt_care_grd_cd");
$inpt_care_apv = pg_fetch_result($sel, 0, "inpt_care_apv");
$inpt_care_from = pg_fetch_result($sel, 0, "inpt_care_from");
$inpt_care_to = pg_fetch_result($sel, 0, "inpt_care_to");
$inpt_dis_grd_rireki = pg_fetch_result($sel, 0, "inpt_dis_grd_rireki");
if ($inpt_dis_grd_rireki == "") {$inpt_dis_grd_rireki = null;}
$inpt_dis_grd_cd = pg_fetch_result($sel, 0, "inpt_dis_grd_cd");
$inpt_dis_type_rireki = pg_fetch_result($sel, 0, "inpt_dis_type_rireki");
if ($inpt_dis_type_rireki == "") {$inpt_dis_type_rireki = null;}
$inpt_dis_type_cd = pg_fetch_result($sel, 0, "inpt_dis_type_cd");
$inpt_spec_name = pg_fetch_result($sel, 0, "inpt_spec_name");
$inpt_spec_from = pg_fetch_result($sel, 0, "inpt_spec_from");
$inpt_spec_to = pg_fetch_result($sel, 0, "inpt_spec_to");
$inpt_intro_inst_cd = pg_fetch_result($sel, 0, "inpt_intro_inst_cd");
$inpt_intro_sect_rireki = pg_fetch_result($sel, 0, "inpt_intro_sect_rireki");
if ($inpt_intro_sect_rireki == "") {$inpt_intro_sect_rireki = null;}
$inpt_intro_sect_cd = pg_fetch_result($sel, 0, "inpt_intro_sect_cd");
$inpt_intro_doctor_no = pg_fetch_result($sel, 0, "inpt_intro_doctor_no");
if ($inpt_intro_doctor_no == "") {$inpt_intro_doctor_no = null;}
$inpt_hotline = pg_fetch_result($sel, 0, "inpt_hotline");
if ($inpt_hotline == "") {$inpt_hotline = null;}
$inpt_pr_inst_cd = pg_fetch_result($sel, 0, "inpt_pr_inst_cd");
$inpt_pr_sect_rireki = pg_fetch_result($sel, 0, "inpt_pr_sect_rireki");
if ($inpt_pr_sect_rireki == "") {$inpt_pr_sect_rireki = null;}
$inpt_pr_sect_cd = pg_fetch_result($sel, 0, "inpt_pr_sect_cd");
$inpt_pr_doctor_no = pg_fetch_result($sel, 0, "inpt_pr_doctor_no");
if ($inpt_pr_doctor_no == "") {$inpt_pr_doctor_no = null;}
$in_updated = pg_fetch_result($sel, 0, "in_updated");
$back_res_updated = pg_fetch_result($sel, 0, "back_res_updated");
$inpt_exemption = pg_fetch_result($sel, 0, "inpt_exemption");
if ($inpt_exemption == "") {$inpt_exemption = null;}

// 移転情報を作成
$sql = "insert into inptmove (ptif_id, from_enti_id, from_bldg_cd, from_ward_cd, from_ptrm_room_no, from_bed_no, from_sect_id, from_dr_id, from_nurse_id, to_enti_id, to_bldg_cd, to_ward_cd, to_ptrm_room_no, to_bed_no, to_sect_id, to_dr_id, to_nurse_id, move_dt, move_tm, move_reason, move_cfm_flg, move_del_flg, updated) values (";
$content = array($pt_id, $from_enti_id, $from_bldg_cd, $from_ward_cd, $from_ptrm_room_no, $from_bed_no, $from_sect_id, $from_dr_id, $from_nurse_id, $enti, $bldg_cd, $ward_cd, $ptrm, $bed, $sect, $doc, $nurse, $date, $time, $reason, "t", "f", date("YmdHis"));
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 移転先の病床情報を更新
$sql = "update inptmst set";
$set = array("ptif_id", "inpt_lt_kn_nm", "inpt_ft_kn_nm", "inpt_lt_kj_nm", "inpt_ft_kj_nm", "inpt_keywd", "inpt_vacant_flg", "inpt_res_flg", "inpt_in_dt", "inpt_in_flg", "inpt_out_dt", "inpt_out_flg", "inpt_out_res_dt", "inpt_out_res_flg", "inpt_enti_id", "inpt_sect_id", "dr_id", "nurse_id", "inpt_insurance", "inpt_meet_flg", "inpt_ecg_flg", "inpt_sample_blood", "inpt_observe", "inpt_purpose_rireki", "inpt_purpose_cd", "inpt_purpose_content", "inpt_diagnosis", "inpt_special", "inpt_disease", "inpt_up_dt", "inpt_up_tm", "inpt_op_no", "inpt_in_tm", "inpt_out_tm", "inpt_out_res_tm", "inpt_in_res_dt_flg", "inpt_in_res_dt", "inpt_in_res_tm", "inpt_in_res_ym", "inpt_in_res_td", "inpt_ope_dt_flg", "inpt_ope_dt", "inpt_ope_ym", "inpt_ope_td", "inpt_emergency", "inpt_in_plan_period", "inpt_in_way", "inpt_nursing", "inpt_staple_fd", "inpt_fd_type", "inpt_fd_dtl", "inpt_wish_rm_rireki", "inpt_wish_rm_cd", "inpt_fd_start", "inpt_result", "inpt_result_dtl", "inpt_out_pos_rireki", "inpt_out_pos_cd", "inpt_out_pos_dtl", "inpt_fd_end", "inpt_out_comment", "inpt_res_chg_psn", "inpt_res_chg_psn_dtl", "inpt_res_chg_rsn", "inpt_res_chg_ctt", "inpt_except_flg", "inpt_except_from_date1", "inpt_except_to_date1", "inpt_except_from_date2", "inpt_except_to_date2", "inpt_except_from_date3", "inpt_except_to_date3", "inpt_except_from_date4", "inpt_except_to_date4", "inpt_except_from_date5", "inpt_except_to_date5", "inpt_privacy_flg", "inpt_privacy_text", "inpt_except_reason_id1", "inpt_except_reason_id2", "inpt_except_reason_id3", "inpt_except_reason_id4", "inpt_except_reason_id5", "inpt_out_inst_cd", "inpt_out_sect_rireki", "inpt_out_sect_cd", "inpt_out_doctor_no", "inpt_out_city", "inpt_back_bldg_cd", "inpt_back_ward_cd", "inpt_back_ptrm_room_no", "inpt_back_bed_no", "inpt_back_in_dt", "inpt_back_in_tm", "inpt_back_sect_id", "inpt_back_dr_id", "inpt_back_change_purpose", "inpt_short_stay", "inpt_outpt_test", "inpt_insu_rate_rireki", "inpt_insu_rate_cd", "inpt_reduced", "inpt_impaired", "inpt_insured", "inpt_arrival", "inpt_patho_from", "inpt_patho_to", "inpt_out_rsn_rireki", "inpt_out_rsn_cd", "inpt_dcb_cls", "inpt_dcb_bas", "inpt_dcb_exp", "inpt_rhb_cls", "inpt_rhb_bas", "inpt_rhb_exp", "inpt_care_no", "inpt_care_grd_rireki", "inpt_care_grd_cd", "inpt_care_apv", "inpt_care_from", "inpt_care_to", "inpt_dis_grd_rireki", "inpt_dis_grd_cd", "inpt_dis_type_rireki", "inpt_dis_type_cd", "inpt_spec_name", "inpt_spec_from", "inpt_spec_to", "inpt_pre_div", "inpt_intro_inst_cd", "inpt_intro_sect_rireki", "inpt_intro_sect_cd", "inpt_intro_doctor_no", "inpt_hotline", "inpt_nrs_obsv", "inpt_free", "inpt_pr_inst_cd", "inpt_pr_sect_rireki", "inpt_pr_sect_cd", "inpt_pr_doctor_no", "in_updated", "back_res_updated", "inpt_exemption");
$setvalue = array($pt_id, $inpt_lt_kn_nm, $inpt_ft_kn_nm, $inpt_lt_kj_nm, $inpt_ft_kj_nm, $inpt_keywd, $inpt_vacant_flg, $inpt_res_flg, $inpt_in_dt, $inpt_in_flg, $inpt_out_dt, $inpt_out_flg, $inpt_out_res_dt, $inpt_out_res_flg, $enti, $sect, $doc, $nurse, $inpt_insurance, $inpt_meet_flg, $inpt_ecg_flg, $inpt_sample_blood, $inpt_observe, $inpt_purpose_rireki, $inpt_purpose_cd, $inpt_purpose_content, $inpt_diagnosis, $inpt_special, $inpt_disease, $inpt_up_dt, $inpt_up_tm, $inpt_op_no, $inpt_in_tm, $inpt_out_tm, $inpt_out_res_tm, $inpt_in_res_dt_flg, $inpt_in_res_dt, $inpt_in_res_tm, $inpt_in_res_ym, $inpt_in_res_td, $inpt_ope_dt_flg, $inpt_ope_dt, $inpt_ope_ym, $inpt_ope_td, $inpt_emergency, $inpt_in_plan_period, $inpt_in_way, $inpt_nursing, $inpt_staple_fd, $inpt_fd_type, $inpt_fd_dtl, $inpt_wish_rm_rireki, $inpt_wish_rm_cd, $inpt_fd_start, $inpt_result, $inpt_result_dtl, $inpt_out_pos_rireki, $inpt_out_pos_cd, $inpt_out_pos_dtl, $inpt_fd_end, $inpt_out_comment, $inpt_res_chg_psn, $inpt_res_chg_psn_dtl, $inpt_res_chg_rsn, $inpt_res_chg_ctt, $inpt_except_flg, $inpt_except_from_date1, $inpt_except_to_date1, $inpt_except_from_date2, $inpt_except_to_date2, $inpt_except_from_date3, $inpt_except_to_date3, $inpt_except_from_date4, $inpt_except_to_date4, $inpt_except_from_date5, $inpt_except_to_date5, $inpt_privacy_flg, $inpt_privacy_text, $inpt_except_reason_id1, $inpt_except_reason_id2, $inpt_except_reason_id3, $inpt_except_reason_id4, $inpt_except_reason_id5, $inpt_out_inst_cd, $inpt_out_sect_rireki, $inpt_out_sect_cd, $inpt_out_doctor_no, $inpt_out_city, $inpt_back_bldg_cd, $inpt_back_ward_cd, $inpt_back_ptrm_room_no, $inpt_back_bed_no, $inpt_back_in_dt, $inpt_back_in_tm, $inpt_back_sect_id, $inpt_back_dr_id, $inpt_back_change_purpose, $inpt_short_stay, $inpt_outpt_test, $inpt_insu_rate_rireki, $inpt_insu_rate_cd, $inpt_reduced, $inpt_impaired, $inpt_insured, $inpt_arrival, $inpt_patho_from, $inpt_patho_to, $inpt_out_rsn_rireki, $inpt_out_rsn_cd, $inpt_dcb_cls, $inpt_dcb_bas, $inpt_dcb_exp, $inpt_rhb_cls, $inpt_rhb_bas, $inpt_rhb_exp, $inpt_care_no, $inpt_care_grd_rireki, $inpt_care_grd_cd, $inpt_care_apv, $inpt_care_from, $inpt_care_to, $inpt_dis_grd_rireki, $inpt_dis_grd_cd, $inpt_dis_type_rireki, $inpt_dis_type_cd, $inpt_spec_name, $inpt_spec_from, $inpt_spec_to, $inpt_pre_div, $inpt_intro_inst_cd, $inpt_intro_sect_rireki, $inpt_intro_sect_cd, $inpt_intro_doctor_no, $inpt_hotline, $inpt_nrs_obsv, $inpt_free, $inpt_pr_inst_cd, $inpt_pr_sect_rireki, $inpt_pr_sect_cd, $inpt_pr_doctor_no, $in_updated, $back_res_updated, $inpt_exemption);
$cond = "where bldg_cd = '$bldg_cd' and ward_cd = '$ward_cd' and ptrm_room_no = '$ptrm' and inpt_bed_no = '$bed'";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 移転元を空床に更新
remove_inpatient_from_bed($con, $from_bldg_cd, $from_ward_cd, $from_ptrm_room_no, $from_bed_no, $fname);

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 親画面をリフレッシュし、自画面を閉じる
echo("<script type=\"text/javascript\">opener.location.reload(); self.close();</script>\n");
?>
</body>
