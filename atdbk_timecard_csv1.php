<?
ob_start();

require_once("Cmx.php");
require_once('Cmx/Model/SystemConfig.php');
require_once("about_comedix.php");
require_once("show_timecard_common.ini");
require_once("show_attendance_pattern.ini");
require_once("timecard_common_class.php");
require_once("date_utils.php");
require_once("atdbk_close_class.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
switch ($wherefrom) {
case "1":  // タイムカード入力画面より
	$auth_id = 5;
	break;
case "2":  // タイムカード修正画面より
	$auth_id = 42;
	break;
default:
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
	break;
}
$checkauth = check_authority($session, $auth_id, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);
// system_config
$conf = new Cmx_SystemConfig();
$duty_or_oncall_flg = $conf->get('timecard.duty_or_oncall_flg');
if ($duty_or_oncall_flg == "") {
	$duty_or_oncall_flg = "1";
}
$duty_str = ($duty_or_oncall_flg == "1") ? "当直" : "待機";


// ************ oose add start *****************
// 職員ID未指定の場合はログインユーザの職員IDを取得
if ($emp_id == "") {
	$sql = "select emp_id from session";
	$cond = "where session_id = '$session'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$emp_id = pg_fetch_result($sel, 0, "emp_id");
}
// 勤務条件テーブルより勤務形態（常勤、非常勤）を取得
$sql = "select duty_form from empcond";
$cond = "where emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$duty_form = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "duty_form") : "";
// ************ oose add end *****************

$atdbk_close_class = new atdbk_close_class($con, $fname);
$closing = $atdbk_close_class->get_empcond_closing($emp_id);
$closing_month_flg = $atdbk_close_class->closing_month_flg;

// 締め日が未登録の場合はエラーとする
if ($closing == "") {
	echo("<script type=\"text/javascript\">alert('タイムカード締め日が登録されていません。');</script>");
	exit;
}

if ($yyyymm != "") {
	$year = substr($yyyymm, 0, 4);
	$month = intval(substr($yyyymm, 4, 2));
	$month_set_flg = true; //月が指定された場合
} else {
	$year = date("Y");
	$month = date("n");
	$yyyymm = $year . date("m");
	$month_set_flg = false; //月が未指定の場合
}

$last_month_y = $year;
$last_month_m = $month - 1;
if ($last_month_m == 0) {
	$last_month_y--;
	$last_month_m = 12;
}
$last_yyyymm = $last_month_y . sprintf("%02d", $last_month_m);

$next_month_y = $year;
$next_month_m = $month + 1;
if ($next_month_m == 13) {
	$next_month_y++;
	$next_month_m = 1;
}
$next_yyyymm = $next_month_y . sprintf("%02d", $next_month_m);

// 開始日・締め日を算出
$calced = false;
switch ($closing) {
case "1":  // 1日
	$closing_day = 1;
	break;
case "2":  // 5日
	$closing_day = 5;
	break;
case "3":  // 10日
	$closing_day = 10;
	break;
case "4":  // 15日
	$closing_day = 15;
	break;
case "5":  // 20日
	$closing_day = 20;
	break;
case "6":  // 末日
	$start_year = $year;
	$start_month = $month;
	$start_day = 1;
	$end_year = $start_year;
	$end_month = $start_month;
	$end_day = days_in_month($end_year, $end_month);
	$calced = true;
	break;
}
if (!$calced) {
	$day = date("j");
	$start_day = $closing_day + 1;
	$end_day = $closing_day;

	//月が指定された場合
	if ($month_set_flg) {
		//当月〜翌月
		if ($closing_month_flg != "2") {
			$start_year = $year;
			$start_month = $month;
			
			$end_year = $year;
			$end_month = $month + 1;
			if ($end_month == 13) {
				$end_year++;
				$end_month = 1;
			}
		}
		//前月〜当月
		else {
			$start_year = $year;
			$start_month = $month - 1;
			if ($start_month == 0) {
				$start_year--;
				$start_month = 12;
			}
			
			$end_year = $year;
			$end_month = $month;
		}
			
	} else {
		if ($day <= $closing_day) {
			$start_year = $year;
			$start_month = $month - 1;
			if ($start_month == 0) {
				$start_year--;
				$start_month = 12;
			}
			
			$end_year = $year;
			$end_month = $month;
		} else {
			$start_year = $year;
			$start_month = $month;
			
			$end_year = $year;
			$end_month = $month + 1;
			if ($end_month == 13) {
				$end_year++;
				$end_month = 1;
			}
		}
	}
}
$start_date = sprintf("%04d%02d%02d", $start_year, $start_month, $start_day);
$end_date = sprintf("%04d%02d%02d", $end_year, $end_month, $end_day);

// 職員氏名を取得
$sql = "select emp_lt_nm, emp_ft_nm from empmst";
$cond = "where emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$emp_name = pg_fetch_result($sel, 0, "emp_lt_nm") . pg_fetch_result($sel, 0, "emp_ft_nm");

// 対象年月の締め状況を取得
//$tmp_yyyymm = substr($start_date, 0, 6);
//月度対応 20100415
$tmp_yyyymm = $year.sprintf("%02d", $month);
$sql = "select count(*) from atdbkclose";
$cond = "where emp_id = '$emp_id' and yyyymm = '$tmp_yyyymm'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$atdbk_closed = (pg_fetch_result($sel, 0, 0) > 0);

// 締め済みの場合、締めデータから表示対象期間を取得
if ($atdbk_closed) {
	$sql = "select min(date), max(date) from wktotald";
	$cond = "where emp_id = '$emp_id' and yyyymm = '$tmp_yyyymm'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$start_date = pg_fetch_result($sel, 0, 0);
	$end_date = pg_fetch_result($sel, 0, 1);

	$start_year = intval(substr($start_date, 0, 4));
	$start_month = intval(substr($start_date, 4, 2));
	$start_day = intval(substr($start_date, 6, 2));
	$end_year = intval(substr($end_date, 0, 4));
	$end_month = intval(substr($end_date, 4, 2));
	$end_day = intval(substr($end_date, 6, 2));
}

// 打刻情報をCSV形式で取得
$csv = get_attendance_book_csv($con, $emp_id, $emp_name, $start_date, $end_date, $fname, $atdbk_closed, $modify_flg);

// データベース接続を閉じる
pg_close($con);

// CSVを出力
$file_name = sprintf("%04d%02d", $year, $month) . ".csv";
ob_clean();
header("Content-Disposition: attachment; filename=$file_name");
header("Content-Type: application/octet-stream; name=$file_name");
header("Content-Length: " . strlen($csv));
echo($csv);
ob_end_flush();

//------------------------------------------------------------------------------
// 関数
//------------------------------------------------------------------------------

// 打刻情報をCSV形式で取得
function get_attendance_book_csv($con, $emp_id, $emp_name, $start_date, $end_date, $fname, $atdbk_closed, $modify_flg) {
	global $duty_str;

	$yyyymm = substr($start_date, 0, 6);
	// 手当情報取得
	$arr_allowance = get_timecard_allowance($con, $fname);

    // タイムカード設定情報
	$sql = "select * from timecard";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
    $night_workday_count = pg_fetch_result($sel, 0, "night_workday_count");
	$return_icon_flg = pg_fetch_result($sel, 0, "return_icon_flg");
	
	//翌日　所定終了時刻　滞留時間（分）追加 20110909
	$buf = "日付,曜日,種別,グループ,出勤予定,事由,{$duty_str},手当,前日,出勤,翌日,退勤,翌日,所定終了時刻,滞留時間（分）,日数,外出,復帰";
	if ($return_icon_flg != "2") {
		$buf .= ",退復,復退,退復,復退,退復,復退,退復,復退,退復,復退,退復,復退,退復,復退,退復,復退,退復,復退,退復,復退";
	} else {
		$buf .= ",呼出,呼退,呼出,呼退,呼出,呼退,呼出,呼退,呼出,呼退,呼出,呼退,呼出,呼退,呼出,呼退,呼出,呼退,呼出,呼退";
	}
	if ($modify_flg == "f") {
        $buf .= ",修正前前日,修正前出勤,修正前翌日,修正前退勤,修正前外出,修正前復帰";
		if ($return_icon_flg != "2") {
			$buf .= ",修正前退復,修正前復退,修正前退復,修正前復退,修正前退復,修正前復退,修正前退復,修正前復退,修正前退復,修正前復退,修正前退復,修正前復退,修正前退復,修正前復退,修正前退復,修正前復退,修正前退復,修正前復退,修正前退復,修正前復退";
		} else {
			$buf .= ",修正前呼出,修正前呼退,修正前呼出,修正前呼退,修正前呼出,修正前呼退,修正前呼出,修正前呼退,修正前呼出,修正前呼退,修正前呼出,修正前呼退,修正前呼出,修正前呼退,修正前呼出,修正前呼退,修正前呼出,修正前呼退,修正前呼出,修正前呼退";
		}
	}
	$buf .= "\r\n";

	$tmp_date = $start_date;

	//グループ毎のテーブルから勤務時間を取得する
	$timecard_common_class = new timecard_common_class($con, $fname, $emp_id, $tmp_date, $end_date);

    //個人別所定時間を優先するための対応 20120309
    $arr_empcond_officehours_ptn = $timecard_common_class->get_empcond_officehours_pattern();
    $arr_weekday_flg = $timecard_common_class->get_arr_weekday_flag($start_date, $end_date);
    $arr_empcond_officehours = $timecard_common_class->get_arr_empcond_officehours($emp_id, $start_date); //個人別勤務時間帯履歴対応 20130220
    
    // 全日付をループ
	while ($tmp_date <= $end_date) {
		$tmp_year = substr($tmp_date, 0, 4);
		$tmp_month = substr($tmp_date, 4, 2);
		$tmp_day = substr($tmp_date, 6, 2);

		// 締め未済の場合
		if (!$atdbk_closed) {

			// 処理日付の勤務日種別を取得
			$sql = "select type from calendar";
			$cond = "where date = '$tmp_date'";
			$sel = select_from_table($con, $sql, $cond, $fname);
			if ($sel == 0) {
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}
			$type = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "type") : "";

		// 締め済みの場合
		} else {

			// 処理日付の勤務日種別を取得
			$sql = "select type from wktotald";
			$cond = "where emp_id = '$emp_id' and yyyymm = '$yyyymm' and date = '$tmp_date'";
			$sel = select_from_table($con, $sql, $cond, $fname);
			if ($sel == 0) {
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}
			$type = pg_fetch_result($sel, 0, "type");
		}


		// 処理日付の勤務実績を取得
//		$sql = "select * from atdbkrslt";
		$sql =	"SELECT ".
			"atdbkrslt.*, ".
			"atdptn.atdptn_nm, ".
			"atdptn.workday_count, ".
			"atdptn.previous_day_flag as atdptn_previous_day_flag, ". //所定前日フラグ 20110909
			"atdptn.over_24hour_flag, ". //24時間超フラグ 20110909
			"wktmgrp.group_name, ".
			"atdbk_reason_mst.default_name, ".
			"atdbk_reason_mst.display_name ".
				"FROM ".
					"atdbkrslt  ".
						"LEFT JOIN wktmgrp ON atdbkrslt.tmcd_group_id = wktmgrp.group_id ".
						"LEFT JOIN atdptn ON atdbkrslt.tmcd_group_id = atdptn.group_id AND atdbkrslt.pattern = CAST(atdptn.atdptn_id AS varchar) ".
						"LEFT JOIN atdbk_reason_mst ON atdbkrslt.reason = atdbk_reason_mst.reason_id ";
		$cond = "where atdbkrslt.emp_id = '$emp_id' and atdbkrslt.date = '$tmp_date'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		if (pg_num_rows($sel) > 0) {
			$pattern = pg_fetch_result($sel, 0, "pattern");
			$reason = pg_fetch_result($sel, 0, "reason");
            $night_duty = pg_fetch_result($sel, 0, "night_duty");
			$allow_id = pg_fetch_result($sel, 0, "allow_id");
			$start_time = pg_fetch_result($sel, 0, "start_time");
			$out_time = pg_fetch_result($sel, 0, "out_time");
			$ret_time = pg_fetch_result($sel, 0, "ret_time");
			$end_time = pg_fetch_result($sel, 0, "end_time");

			for ($i = 1; $i <= 10; $i++) {
				$var_name = "o_start_time$i";
				$$var_name = pg_fetch_result($sel, 0, $var_name);
				$var_name = "o_end_time$i";
				$$var_name = pg_fetch_result($sel, 0, $var_name);
			}
			$workday_count = pg_fetch_result($sel, 0, "workday_count");
			$pattern_name = pg_fetch_result($sel, 0, "atdptn_nm");
			$tmcd_group_id = pg_fetch_result($sel, 0, "tmcd_group_id");
			$tmcd_group_name = pg_fetch_result($sel, 0, "group_name");
			$previous_day_flag = pg_fetch_result($sel, 0, "previous_day_flag");
			$next_day_flag = pg_fetch_result($sel, 0, "next_day_flag");
			$default_name = pg_fetch_result($sel, 0, "default_name");
			$display_name   = pg_fetch_result($sel, 0, "display_name");
			$reason_name = $display_name;
			if (empty($reason_name)){
				$reason_name = $default_name;
			}
			$atdptn_previous_day_flag   = pg_fetch_result($sel, 0, "atdptn_previous_day_flag");
			$over_24hour_flag   = pg_fetch_result($sel, 0, "over_24hour_flag");
		} else {
			$pattern = "";
			$reason = "";
            $night_duty = "";
			$allow_id = "";
			$start_time = "";
			$out_time = "";
			$ret_time = "";
			$end_time = "";
			for ($i = 1; $i <= 10; $i++) {
				$var_name = "o_start_time$i";
				$$var_name = "";
				$var_name = "o_end_time$i";
				$$var_name = "";
			}
			$workday_count = "";
			$pattern_name = "";
			$tmcd_group_id = "";
			$tmcd_group_name = "";
			$previous_day_flag = 0;
			$next_day_flag = 0;
			$reason_name = "";
			$atdptn_previous_day_flag   = "";
			$over_24hour_flag   = "";
		}

		//当直の有効判定
		$night_duty_flag = $timecard_common_class->is_valid_duty($night_duty, $tmp_date, $start_time, $previous_day_flag, $end_time, $next_day_flag, $tmcd_group_id);
		//滞留時間追加 退勤時刻−所定終了時刻  20110909
		$tmp_type = ($type != "2" && $type != "3") ? "1" : $type;
		$start2 = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "start2" );
		$end2 =   $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "end2" );
		$end2_next_day_flag = 0;
        //個人別所定時間を優先する場合 20120309
        if ($arr_empcond_officehours_ptn[$tmcd_group_id][$pattern] == 1) {
            $wk_weekday = $arr_weekday_flg[$tmp_date];
            $arr_empcond_officehours = $timecard_common_class->get_empcond_officehours($wk_weekday, $tmp_date); //個人別勤務時間帯履歴対応 20130220
            if ($arr_empcond_officehours["officehours2_start"] != "") {
                $start2 = $arr_empcond_officehours["officehours2_start"];
                $end2 = $arr_empcond_officehours["officehours2_end"];
            }
        }
        
        if ($end_time != "" && $end2 != "") {
			//所定終了翌日フラグ設定
			if (($start2 > $end2 && $atdptn_previous_day_flag != "1") || $over_24hour_flag == "1") {
				$end2_next_day_flag = 1;
			}
			//所定終了日時
			if ($end2_next_day_flag) {
				$end2_date_time = next_date($tmp_date).$end2;
			} else {
				$end2_date_time = $tmp_date.$end2;
			}
			//退勤日時
			if ($next_day_flag) {
				$end_date_time = next_date($tmp_date).$end_time;
			} else {
				$end_date_time = $tmp_date.$end_time;
			}
			$retention_time = date_utils::get_diff_minute($end_date_time ,$end2_date_time);;
		} else {
			$retention_time = "";
		}
		
		$buf .= "$tmp_year/$tmp_month/$tmp_day,";
		$buf .= get_weekday(to_timestamp($tmp_date)) . ",";

		// カレンダー名を取得
		$buf .= $timecard_common_class->get_calendarname($type, $tmp_date);
		$buf .= ",";
		$buf .= $tmcd_group_name . ",";
		$buf .= $pattern_name . ",";
		$buf .= $reason_name . ",";
		switch ($night_duty) {
		case "1":
			$buf .= "有り";
			break;
		case "2":
			$buf .= "無し";
			break;
		case "1":
			$buf .= "";
			break;
		}
		$buf .= ",";
		if ($allow_id != "")
		{
			foreach($arr_allowance as $allowance)
			{
				if($allow_id == $allowance["allow_id"])
				{
					$buf .= $allowance["allow_contents"];
					break;
				}
			}
		}

		// 前日になる場合があるフラグ
		$buf .= ",";
		if ($previous_day_flag == 1){
			$buf .= "前";
		}

		$buf .= ",";
		$buf .= preg_replace("/(\d{2})(\d{2})/", "$1:$2", $start_time) . ",";
		if ($next_day_flag == 1){
			$buf .= "翌,";
		} else {
			$buf .= ",";
		}
		$buf .= preg_replace("/(\d{2})(\d{2})/", "$1:$2", $end_time) . ",";
		//滞留時間追加20110909
		if ($end2_next_day_flag == 1){
			$buf .= "翌";
		}
		$buf .= ",";
		$buf .= $end2;
		$buf .= ",";
		if ($retention_time > 0) {
			$buf .= $retention_time;
		}
		$buf .= ",";
		
        // 日数
        $total_workday_count = "";  
        if($start_time != "")
        {
//	        $workday_count = $arr_atdptn_workday_count[$pattern];
	        if($workday_count != "")
	        {
	            $total_workday_count = $workday_count;
	        }
	
	        if($night_duty_flag)
	        {
				//曜日に対応したworkday_countを取得する
				$total_workday_count = $total_workday_count + $timecard_common_class->get_night_workday_count($tmp_date, $end_time, $next_day_flag, $tmcd_group_id);
	        }
        }
		else if ($workday_count == 0){
			//日数換算に0が指定された場合のみ0を入れる
			$total_workday_count = $workday_count;
		}
        $buf .= $total_workday_count . ",";

		$buf .= preg_replace("/(\d{2})(\d{2})/", "$1:$2", $out_time) . ",";
		$buf .= preg_replace("/(\d{2})(\d{2})/", "$1:$2", $ret_time);
		for ($i = 1; $i <= 10; $i++) {
			$var_name = "o_start_time$i";
			$buf .= "," . preg_replace("/(\d{2})(\d{2})/", "$1:$2", $$var_name);
			$var_name = "o_end_time$i";
			$buf .= "," . preg_replace("/(\d{2})(\d{2})/", "$1:$2", $$var_name);
		}

		// 修正申請が必要な場合
		if ($modify_flg == "f") {

			// 勤務時間修正申請情報を取得（否認済み以外）
			$sql = "select * from tmmdapply";
			$cond = "where emp_id = '$emp_id' and target_date = '$tmp_date' and not delete_flg and re_apply_id is null and apply_status <> '2'";
			$sel = select_from_table($con, $sql, $cond, $fname);
			if ($sel == 0) {
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			if (pg_num_rows($sel) > 0) {
				$b_start_time = pg_fetch_result($sel, 0, "b_start_time");
				$b_out_time = pg_fetch_result($sel, 0, "b_out_time");
				$b_ret_time = pg_fetch_result($sel, 0, "b_ret_time");
				$b_end_time = pg_fetch_result($sel, 0, "b_end_time");
				for ($i = 1; $i <= 10; $i++) {
					$var_name = "b_o_start_time$i";
					$$var_name = pg_fetch_result($sel, 0, $var_name);
					$var_name = "b_o_end_time$i";
					$$var_name = pg_fetch_result($sel, 0, $var_name);
				}
				$b_previous_day_flag = pg_fetch_result($sel, 0, "b_previous_day_flag");
				$b_next_day_flag = pg_fetch_result($sel, 0, "b_next_day_flag");
			} else {
				$b_start_time = "";
				$b_out_time = "";
				$b_ret_time = "";
				$b_end_time = "";
				for ($i = 1; $i <= 10; $i++) {
					$var_name = "b_o_start_time$i";
					$$var_name = "";
					$var_name = "b_o_end_time$i";
					$$var_name = "";
				}
				$b_previous_day_flag = 0;
				$b_next_day_flag = 0;
			}

			// 前日になる場合があるフラグ
			$buf .= ",";
			if ($b_previous_day_flag == 1){
				$buf .= "前";
			}

			$buf .= ",";
			$buf .= preg_replace("/(\d{2})(\d{2})/", "$1:$2", $b_start_time) . ",";
			if ($b_next_day_flag == 1){
				$buf .= "翌,";
			} else {
				$buf .= ",";
			}
			$buf .= preg_replace("/(\d{2})(\d{2})/", "$1:$2", $b_end_time) . ",";
			$buf .= preg_replace("/(\d{2})(\d{2})/", "$1:$2", $b_out_time) . ",";
			$buf .= preg_replace("/(\d{2})(\d{2})/", "$1:$2", $b_ret_time);
			for ($i = 1; $i <= 10; $i++) {
				$var_name = "b_o_start_time$i";
				$buf .= "," . preg_replace("/(\d{2})(\d{2})/", "$1:$2", $$var_name);
				$var_name = "b_o_end_time$i";
				$buf .= "," . preg_replace("/(\d{2})(\d{2})/", "$1:$2", $$var_name);
			}
		}

		$buf .= "\r\n";

		$tmp_date = next_date($tmp_date);
	}

	return mb_convert_encoding($buf, "SJIS", "EUC-JP");

}
?>
