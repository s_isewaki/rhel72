<?
ob_start();

require_once("about_comedix.php");
require_once("show_newsuser_refer_list.ini");
require_once("news_common.ini");
require_once("Cmx.php");
require_once("aclg_set.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
if ($admin_flg == '1') {
	$checkauth = check_authority($session, 24, $fname);
	if ($checkauth == "0") {
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
		exit;
	}
} else {
	$checkauth = check_authority($session, 46, $fname);
	if ($checkauth == "0") {
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
		exit;
	}
}

// データベースに接続
$con = connect2db($fname);

// 職員IDのカラム表示条件の初期値を取得
$default_emp_id_chk		= get_emp_id_read_status($con, $fname);

// 職員所属のカラム表示条件の初期値を取得
$default_emp_class_chk	= get_emp_class_read_status($con, $fname);

// 情報をCSV形式で取得
$csv = get_news_refer_csv($con, $news_id, $csv_flg, $default_emp_id_chk, $default_emp_class_chk, $admin_flg);

// CSVを出力
if ($csv_flg == "1") {
	$file_name = "enquet_total.csv";
} else {
	$file_name = "enquet_detail.csv";
}

// アクセスログ（「内容」でファイル名を取得します。）
$_GET['file_name']=$file_name;
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con,$_GET);

// データベース接続を閉じる
pg_close($con);

ob_clean();
header("Content-Disposition: attachment; filename=$file_name");
header("Content-Type: application/octet-stream; name=$file_name");
header("Content-Length: " . strlen($csv));
echo($csv);
ob_end_flush();


//------------------------------------------------------------------------------
// 関数
//------------------------------------------------------------------------------

// 情報をCSV形式で取得
function get_news_refer_csv($con, $news_id, $csv_flg, $default_emp_id_chk, $default_emp_class_chk, $admin_flg) {

	// 回答設定を取得
	$qa_cnt = 0;
	$question = array();
	$select_num = array();
	$multi_flg = array();
	$answer = array();
	$arr_answer_cnt = array();
	$arr_answer_item = array();
        $type = array();
        $free_format = array();

	$sql = "select question, select_num, multi_flg, news_title,type from newsenquet_q a inner join news b on b.news_id = a.news_id ";
	$cond = "where a.news_id = $news_id order by question_no";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$i = 1;
	while ($row = pg_fetch_array($sel)) {
		$question[$i] = $row["question"];
		$select_num[$i] = $row["select_num"];
		$multi_flg[$i] = $row["multi_flg"];
		$news_title[$i] = $row["news_title"];
                $type[$i] = $row["type"];
		$qa_cnt++;
		$i++;
	}

	for ($i=1; $i<=$qa_cnt; $i++) {
		$sql = "select answer from newsenquet_a";
		$cond = "where news_id = $news_id and question_no = $i order by item_no";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$j = 1;
		while ($row = pg_fetch_array($sel)) {
			$answer[$i][$j] = $row["answer"];
			$j++;
		}
	}
        //集計
	if ($csv_flg == "1") {
		// 回答数取得
		if ($qa_cnt > 0) {
			$sql = "select question_no, item_no,count(*) as cnt from newsanswer";
			$cond = "where news_id = $news_id group by question_no, item_no";
			$sel = select_from_table($con, $sql, $cond, $fname);
			if ($sel == 0) {
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			while ($row = pg_fetch_array($sel)) {
                            $arr_answer_cnt[$row["question_no"]][$row["item_no"]] = $row["cnt"];
			}
                        
                        $sql = "select question_no, item_no,count(*) as free_cnt from newsanswer";
                        $cond = "where news_id = $news_id and free_format !='' group by question_no, item_no";
                        $sel = select_from_table($con, $sql, $cond, $fname);
			if ($sel == 0) {
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			while ($row = pg_fetch_array($sel)) {
                              $free_format_cnt[$row["question_no"]][$row["item_no"]] = $row["free_cnt"];
			}
           
		}

		$buf = "";
		$buf .= "タイトル,{$news_title[1]}";
		$buf .= "\r\n";
		$buf .= "\r\n";
		for ($i=1; $i<=$qa_cnt; $i++) {
			$buf .= "質問{$i},{$question[$i]}";
			$buf .= "\r\n";
			$buf .= "項番";
                        if($type[$i]==0){
			for ($j=1; $j<=$select_num[$i]; $j++) {
				$buf .= ",{$j}";
			}
                        }else if($type[$i]==1){
                        $buf .= ",1";
                        }
			$buf .= "\r\n";
			$buf .= "内容";
                        if($type[$i]==0){
                            for ($j=1; $j<=$select_num[$i]; $j++) {
				$buf .= ",{$answer[$i][$j]}";
                            }
                        }else if($type[$i]==1){
                             $buf .= ",回答人数";
                        }
			$buf .= "\r\n";
			$buf .= "人数";
                        if($type[$i]==0){
                            for ($j=1; $j<=$select_num[$i]; $j++) {
				if ($arr_answer_cnt[$i][$j] == "") {
					$arr_answer_cnt[$i][$j] = 0;
				}
				$buf .= ",{$arr_answer_cnt[$i][$j]}";
                           }
			} else if($type[$i]==1){
                            	if ($free_format_cnt[$i]['1'] == "") {
					$free_format_cnt[$i]['1'] = 0;
				}
				$buf .= ",{$free_format_cnt[$i]['1']}";
                        }
			$buf .= "\r\n";
			$buf .= "\r\n";
		}
        //明細
	} else {
		// 回答者情報取得　$arr_answer_item[question_no][emp_id][item_no]
		if ($qa_cnt > 0) {
			$sql = "select question_no, emp_id, item_no,free_format from newsanswer";
			$cond = "where news_id = $news_id";
			$sel = select_from_table($con, $sql, $cond, $fname);
			if ($sel == 0) {
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			while ($row = pg_fetch_array($sel)) {
                                $free_format[$row["question_no"]][$row["emp_id"]]=$row["free_format"];
				$arr_answer_item[$row["question_no"]][$row["emp_id"]][$row["item_no"]] = "1";
			}
		}

		$buf = "";
		for ($i=1; $i<=$qa_cnt; $i++) {
			// 見出し

			$buf .= "質問番号";
			
			if ($default_emp_id_chk != 'f' || $admin_flg == '1') {
				$buf .= ",職員ID";
			}

			$buf .= ",氏名";

			if ($default_emp_class_chk != 'f' || $admin_flg == '1') { 
				$buf .= ",所属";
			}
                        if($type[$i]==0){
                            for ($j=1; $j<=$select_num[$i]; $j++) {
				$buf .= ",{$answer[$i][$j]}";
                            }
                        }else if($type[$i]==1){
                            $buf .= ",回答内容";
                        }
			$buf .= "\r\n";

			// 職員情報取得、職員ID（emp_personal_id）、氏名、所属（カナ順）
			$sql = "select a.emp_id, b.emp_personal_id, b.emp_lt_nm, b.emp_ft_nm, c.class_nm, d.atrb_nm, e.dept_nm, f.room_nm from (select distinct(emp_id) from newsanswer where news_id = $news_id and question_no = $i) a inner join empmst b on b.emp_id = a.emp_id  inner join classmst c on c.class_id = b.emp_class inner join atrbmst d on d.atrb_id = b.emp_attribute inner join deptmst e on e.dept_id = b.emp_dept left outer join classroom f on f.room_id = b.emp_room ";
			$cond = " order by b.emp_kn_lt_nm, b.emp_kn_ft_nm";
			$sel = select_from_table($con, $sql, $cond, $fname);
			if ($sel == 0) {
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			while ($row = pg_fetch_array($sel)) {

				$tmp_emp_id = $row["emp_id"];
				$tmp_emp_personal_id = $row["emp_personal_id"];
				$tmp_emp_nm = $row["emp_lt_nm"]." ".$row["emp_ft_nm"];
				$tmp_dept_nm = $row["class_nm"]." > ".$row["atrb_nm"]." > ".$row["dept_nm"];
				if ($row["room_nm"] != "") {
					$tmp_dept_nm .= $row["room_nm"];
				}
				// 職員データ
				$buf .= "{$i}";
				
				if ($default_emp_id_chk != 'f' || $admin_flg == '1') {
					$buf .= ",{$tmp_emp_personal_id}";
				}

				$buf .= ",{$tmp_emp_nm}";
				
				if ($default_emp_class_chk != 'f' || $admin_flg == '1') { 
					$buf .= ",{$tmp_dept_nm}";
				}

				// 選択肢
                                if($type[$i]==0){
                                    for ($j=1; $j<=$select_num[$i]; $j++) {
					if ($arr_answer_item[$i][$tmp_emp_id][$j] == "1") {
						$buf .= ",1";
					} else {
						$buf .= ",0";
					}
                                    }
                                }else if($type[$i]==1){
                                    $rep = str_replace("\r\n","",$free_format[$i][$tmp_emp_id]);
                                    $buf .= ",\"".$rep."\"";
                                }
				$buf .= "\r\n";
			}
			$buf .= "\r\n";
		}
	}
	return mb_convert_encoding($buf, "SJIS", "EUC-JP");

}
?>
