<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?php
//duty_shift_hol_remain.php
//繰越公休残日数表示調整画面
require_once("about_comedix.php");
require_once("duty_shift_common_class.php");
require_once("duty_shift_hol_remain_common_class.php");
require_once("show_timecard_common.ini");
require_once("timecard_bean.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
//セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session,$fname);
if($session == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}
///-----------------------------------------------------------------------------
//DBコネクション
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
//タイムカード情報の取得
$timecard_bean = new timecard_bean();
$timecard_bean->select($con, $fname);

///-----------------------------------------------------------------------------
// ログインユーザの職員ID・氏名を取得
///-----------------------------------------------------------------------------
$sql = "select emp_id, emp_lt_nm, emp_ft_nm from empmst";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showErrorPage(window);</script>");
    exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");
//繰越公休残用クラス
$obj_hol_remain = new duty_shift_hol_remain_common_class($con, $fname, $group_id, $emp_id);

//シフトグループ情報取得
$data_wktmgrp = $obj->get_wktmgrp_array();
$group_array = $obj->get_duty_shift_group_array($group_id, "", $data_wktmgrp);

$arr_data_emp = $obj->get_valid_empmst_array($emp_id, $duty_yyyy, $duty_mm);

$arr_staff_info = $obj->get_duty_shift_plan_staff_array($group_id, $duty_yyyy, $duty_mm, array(), array(), $arr_data_emp, "1", false);
//職員設定に登録されているスタッフ
$arr_main_staff_info = $obj->get_duty_shift_staff_array($group_id, array(), array(), array());
$plan_no_exist_flg = false;
if (count($arr_staff_info) == 0) {
    $plan_no_exist_flg = true;
    $arr_staff_info = $arr_main_staff_info;
   
}
else {
    //応援追加確認
    $ouen_flg = true; //true:応援表示する false:応援表示しない
    if ($ouen_flg) {
        $arr_main_staff_id = array();
        for ($i=0; $i<count($arr_main_staff_info); $i++) {
            $arr_main_staff_id[] = $arr_main_staff_info[$i]["emp_id"];
        }
        for ($i=0; $i<count($arr_staff_info); $i++) {
            $wk_staff_id = $arr_staff_info[$i]["id"];
            if (!in_array($wk_staff_id, $arr_main_staff_id)) {
                $arr_staff_info[$i]["other_flg"] = "1";
            }
            else {
                $arr_staff_info[$i]["other_flg"] = "";
            }
        }
    }
    else {
        $arr_staff_info = $arr_main_staff_info;
    }
}

$arr_remain = $obj_hol_remain->get_hol_remain($group_id, $duty_yyyy, $duty_mm);

$arr_remain_prev = $obj_hol_remain->get_hol_remain_prev($group_id, $duty_yyyy, $duty_mm);

//期間情報取得
$start_month_flg1 = $group_array[0]["start_month_flg1"];
$start_day1 = $group_array[0]["start_day1"];
$month_flg1 = $group_array[0]["month_flg1"];
$end_day1 = $group_array[0]["end_day1"];
if ($start_month_flg1 == "") {$start_month_flg1 = "1";}
if ($start_day1 == "" || $start_day1 == "0") {$start_day1 = "1";}
if ($month_flg1 == "" || $end_day1 == "0") {$month_flg1 = "1";}
if ($end_day1 == "" || $end_day1 == "0") {	$end_day1 = "99";}
$arr_date = $obj->get_term_date($duty_yyyy, $duty_mm, $start_month_flg1, $start_day1, $month_flg1, $end_day1);
$start_date = $arr_date[0];
$end_date = $arr_date[1];

$tablename = "atdbkrslt"; //atdbkrslt:実績 atdbk:予定
$arr_hol_cnt = $obj_hol_remain->get_hol_cnt($group_id, $duty_yyyy, $duty_mm, $start_date, $end_date, $tablename);
//実績入力画面の集計列から
$disp_hol_cnt_flg = false;
//warning_flg:前月残がないため警告画面として開いた状態を示すフラグ
if ($warning_flg == "" && $hol_cnt != "") {
    $disp_hol_cnt_flg = true;
    $arr_disp_hol_cnt = split("," , $hol_cnt);
}
//
$arr_legal_hol_cnt = array();
$arr_legal_hol_cnt_part = array();
$arr_holwk = get_timecard_holwk_day($con, $fname);
//
$arr_tmcd_group_id = $obj_hol_remain->get_tmcd_group_id($group_id, $duty_yyyy, $duty_mm);
$arr_tmcd_group = array();
foreach ($arr_tmcd_group_id as $staff_id => $staff_data) {
    $wk_tmcd_group_id = $staff_data["tmcd_group_id"];
    $arr_tmcd_group[$wk_tmcd_group_id] = $wk_tmcd_group_id;
}

//勤務パターングループ別の調整日数を確認する
$arr_std_hol_cnt = array();
$duty_form = "";
foreach ($arr_tmcd_group as $wk_tmcd_group_id => $dummy) {
    $arr_std_hol_cnt[$wk_tmcd_group_id] = get_legal_hol_cnt_month($con, $fname, $duty_yyyy, $duty_mm, $timecard_bean->closing, $timecard_bean->closing_month_flg, $wk_tmcd_group_id, $duty_form, $arr_legal_hol_cnt, $arr_legal_hol_cnt_part, $arr_holwk);
}

//月を確認
$prev_yyyymm=date("Ym", strtotime("-1 month"));
$next_yyyymm=date("Ym", strtotime("+1 month"));
$curr_yyyymm=date("Ym");
$today = date("Ymd");
$check_yyyymm = $duty_yyyy.sprintf("%02d", $duty_mm);
//前月以降なら更新可能とする（過去分は更新しない）
$past_flg = false;
if ($check_yyyymm < $prev_yyyymm) {
    $past_flg = true;
}

$group_name = $group_array[0]["group_name"];
$shift_menu_label = "勤務シフト作成";
$title = "繰越公休残日数調整";
?>
<title>CoMedix <? echo($shift_menu_label); ?> | <? echo($title); ?></title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function updateData() {
	document.mainform.action = "duty_shift_hol_remain_update_exe.php";
	document.mainform.submit();
}
</script>

<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/font.css">
<link rel="stylesheet" type="text/css" href="css/shift/shift.css">
<link rel="stylesheet" type="text/css" href="css/shift/results.css">
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
		<tr height="30" bgcolor="#5279a5">
		<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b><? echo($title); ?></b></font></td>
		<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
		</tr>
	</table>
		<form name="mainform" method="post">

<?php
echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\">");
echo("<b>".$group_name." ");
echo($duty_yyyy."年 ".$duty_mm."月 </b><br>\n");
echo("</font>\n");
$table_width="590";
//管理者、代行者の場合。 前月当月の時有効
$disabled = "";
if ($past_flg) {
    $disabled = " disabled";
}
?>
<table width="<?php echo $table_width; ?>" border="0">
<tr><td align="right">
		<input type="button" value="繰越公休残日数更新" onclick="updateData();" <?php echo $disabled; ?>>
        </td>
        </tr>
</table>
    <?
//
echo("<table width=\"{$table_width}\" class=\"list\">\n");
echo("<tr >\n");

echo("<td align=\"center\" width=\"200\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\">氏名</font></td>\n");
echo("<td align=\"center\" width=\"100\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\">前月繰越数</font></td>\n");
echo("<td align=\"center\" width=\"100\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\">所定公休数</font></td>\n");
echo("<td align=\"center\" width=\"100\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\">当月公休数</font></td>\n");
echo("<td align=\"center\" width=\"90\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\">繰越公休残</font></td>\n");
echo("</tr>\n");

for ($i=0; $i<count($arr_staff_info); $i++) {
    $staff_id = $arr_staff_info[$i]['id'];

    //所定公休数
    $wk_tmcd_group_id = $arr_tmcd_group_id[$staff_id]["tmcd_group_id"];
    $std_hol_cnt = $arr_std_hol_cnt[$wk_tmcd_group_id];
    
    //実績公休数 画面から取得した値
    if ($disp_hol_cnt_flg) {
        $wk_hol_cnt = $arr_disp_hol_cnt[$i];
    }
    else {
        //DBから取得
        $wk_hol_cnt = $arr_hol_cnt[$staff_id];
    }
    
    //当月残が未設定時は、計算する
    if ($arr_remain[$staff_id] == "") {
        //当月残＝前月残＋所定公休数−当月実績公休数
        $wk_remain = $arr_remain_prev[$staff_id] + $std_hol_cnt - $wk_hol_cnt;
        $arr_remain[$staff_id] = $wk_remain;
        $regist_flg = false;
    }
    else {
        $regist_flg = true;
    }
    
    echo("<tr height=\"22\">\n");
    $bgcolorstr = "";
    //氏名
    $color = ( $arr_staff_info[$i]["sex"] == 1 ) ? 'blue' : 'black';
    
    echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\" style=\"color:{$color}\">\n");
    echo($arr_staff_info[$i]['name']);
    echo("</font></td>\n");
    //前月繰越数
    echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\">\n");
    echo($arr_remain_prev[$staff_id]);
    echo("</font></td>\n");
    //所定公休数
    echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\">\n");
    echo($std_hol_cnt);
    echo("</font></td>\n");
    //当月公休数
    echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\">\n");
    echo($wk_hol_cnt);
    echo("</font></td>\n");
    //繰越公休残
    if ($arr_staff_info[$i]['other_flg'] == "1" || $past_flg) {
            $bgcolorstr = "bgcolor=\"lightgrey\"";
    }
    else {
        $bgcolorstr = ($regist_flg) ? "" : "bgcolor=\"#ff0000\"";
    }
    echo("<td $bgcolorstr width=\"60\" align=\"center\">\n");
    //応援の場合
    if ($arr_staff_info[$i]["other_flg"] == "1") {
        $readonly = " readOnly";
        $stylestr = "style=\"text-align:right; border:none;color:#808080;\"";
    }
    //過去月の場合
    else if ($past_flg) {
        $readonly = " readOnly";
        $stylestr = "style=\"text-align:right; border:none;color:#000000;\"";
    }
    else {
        $readonly = "";
        $stylestr = "style=\"text-align:right\"";
    }
    echo("<input type=\"text\" name=\"remain_{$i}\" id=\"remain_{$i}\" value=\"");
    echo($arr_remain[$staff_id]."\" size=\"5\" maxlength=\"5\" $readonly $stylestr>\n");
    echo("<input type=\"hidden\" name=\"staff_id_{$i}\" value=\"");
    echo($staff_id."\">\n");
    echo("<input type=\"hidden\" name=\"other_flg_{$i}\" id=\"other_flg_{$i}\" value=\"");
    echo($arr_staff_info[$i]['other_flg']."\">\n");
    echo("</td>\n");
    echo("</tr>\n");
}
echo("</table>\n");

?>
<table width="<?php echo $table_width; ?>" border="0">
<tr><td align="right">
		<input type="button" value="繰越公休残日数更新" onclick="updateData();" <?php echo $disabled; ?>>
        </td>
        </tr>
</table>
    <?
echo("<input type=\"hidden\" name=\"session\" value=\"$session\">\n");
echo("<input type=\"hidden\" name=\"duty_yyyy\" value=\"$duty_yyyy\">\n");
echo("<input type=\"hidden\" name=\"duty_mm\" value=\"$duty_mm\">\n");

echo("<input type=\"hidden\" name=\"group_id\" value=\"$group_id\">\n");
echo("<input type=\"hidden\" name=\"start_date\" value=\"$start_date\">\n");
echo("<input type=\"hidden\" name=\"end_date\" value=\"$end_date\">\n");
$data_cnt = count($arr_staff_info);
echo("<input type=\"hidden\" name=\"data_cnt\" value=\"$data_cnt\">\n");
echo("<input type=\"hidden\" name=\"hol_cnt\" value=\"$hol_cnt\">\n");
echo("<input type=\"hidden\" name=\"warning_flg\" value=\"$warning_flg\">\n");
?>
</form>
</body>

<? pg_close($con); ?>

</html>
