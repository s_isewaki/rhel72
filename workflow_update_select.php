<?
require_once("about_comedix.php");
require_once("application_workflow_common_class.php");
//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo ("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo ("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$wkfw = check_authority($session, 3, $fname);
if ($wkfw == "0") {
	echo ("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo ("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if ($con == "0") {
	echo ("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo ("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}


$obj = new application_workflow_common_class($con, $fname);
$sql = "select b.wkfw_id, b.wkfw_type, b.wkfw_title, c.wkfw_nm, b.wkfw_folder_id ".
		" from wkfwaliasmng a ".
		" left join wkfwmst b on b.wkfw_id = a.alias_wkfw_id ".
		" left join wkfwcatemst c on c.wkfw_type = b.wkfw_type ";
$cond = "where a.real_wkfw_id = $wkfw_id and delete_flg = 'f' order by b.wkfw_type, b.wkfw_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo ("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo ("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$arr_wkfw = array();
while ($row = pg_fetch_array($sel)) {
	if ($row["wkfw_folder_id"] != "") {
		$row["arr_folder"] = $obj->get_folder_path($row["wkfw_folder_id"]);
	}
	$arr_wkfw[] = $row;
}

?>

<?

//====================================
// HTML
//====================================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ワークフロー｜更新ワークフロー選択</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script src="js/prototype/dist/prototype.js"></script>
<script language="javascript">

function update() {
	if (window.opener.document.wkfw.alias_ids) {
		var alias_ids = getCheckedIds(document.wkfw.elements['alias_ids[]']);
		if (alias_ids.length > 0) {
			window.opener.document.wkfw.alias_ids.value = alias_ids.join(',');
		}
		window.opener.submitForm();
	}

	self.close();
}

function getCheckedIds(boxes) {
    var checked_ids = new Array();
    if (boxes) {
        if (!boxes.length) {
            if (boxes.checked) {
                checked_ids.push(boxes.value);
            }
        } else {
            for (var i = 0, j = boxes.length; i < j; i++) {
                if (boxes[i].checked) {
                    checked_ids.push(boxes[i].value);
                }
            }
        }
    }
    return checked_ids;
}
//チェックボックスONOFF
function checkAll() {

	stat = (document.wkfw.checkall.value == '解除') ? false : true;
	document.wkfw.checkall.value = (document.wkfw.checkall.value == '解除') ? '選択' : '解除';
	if (document.wkfw.elements['alias_ids[]'] != null) {
		var box_cnt = document.wkfw.elements['alias_ids[]'].length;
		if (box_cnt == undefined) {
			document.wkfw.elements['alias_ids[]'].checked = stat;
		} else {
			for (var i = 0, j = box_cnt; i < j; i++) {
				document.wkfw.elements['alias_ids[]'][i].checked = stat;
			}
		}
	}
}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
<!--
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}

table.block {border-collapse:collapse;}
table.block td {border:#5279a5 solid 1px;padding:1px;}
table.block td td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#5279a5 solid 0px;}
table.block_in td td {border-width:1;}
// -->
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="3" leftmargin="3" marginheight="3" marginwidth="3">
<form name="wkfw" action="workflow_update_select.php" method="post">

<img src="img/spacer.gif" width="1" height="3" alt=""><br>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>更新ワークフロー選択</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>

<img src="img/spacer.gif" width="1" height="3" alt=""><br>

<table width="100%" border="0" cellspacing="0" cellpadding="0">

<tr>
<td>
<table width="" border="0" cellspacing="0" cellpadding="0"  class="">
<tr height="22">
<td align="left">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" style="color:grey">選択したフォルダ・テンプレートの本文形式と本文(テキスト、テンプレート)を更新します。承認ルートなどの設定情報は更新されません。</font>
</td>
</tr>
</table>

<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
＜コピーされているフォルダ・テンプレート一覧＞
</font>
<table width="100%" border='0' cellspacing='0' cellpadding='2' class="list">
<tr bgcolor="#f6f9ff">
<td align="center" width="8%">
<input type="button" id="checkall" name="checkall" value="選択" onclick="checkAll();">
</td>
<td width="46%">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">フォルダ</font><BR>
</td>
<td width="46%">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ワークフロー名</font><BR>
</td>
</tr>
<? 
foreach ($arr_wkfw as $wkfw) {
?>
<tr>
<td align="center">
<input type="checkbox" id="alias_ids[]" name="alias_ids[]" value="<? echo($wkfw["wkfw_id"]); ?>">
</td>
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<? echo($wkfw["wkfw_nm"]);
foreach ($wkfw["arr_folder"] as $folder) {
	echo(" > ");
	echo($folder["name"]);
}
?></font><BR>
</td>
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($wkfw["wkfw_title"]); ?></font><BR>
</td>
</tr>
<? } ?>
</table>



<img src="img/spacer.gif" width="1" height="5" alt=""><br>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
<td align="right">
<input type="button" value="更新" onclick="update();">
</td>
</tr>
</table>
</td>
</tr>
</table>


</form>
</body>
</html>

<?
pg_close($con);
?>