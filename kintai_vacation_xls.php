<?php
ob_start();
require_once("about_session.php");
require_once("about_authority.php");

require_once './common_log_class.ini';
require_once './kintai/common/mdb_wrapper.php'; // MDB本体をWrap
require_once './kintai/common/mdb.php'; 		// DBアクセスユーティリティ
require_once './kintai/common/user_info.php';	// ユーザ情報
require_once './kintai/common/master_util.php';	// マスタ
require_once './kintai/common/exception.php';
require_once './kintai/common/date_util.php';
require_once './kintai/common/code_util.php';

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
// DBコネクション作成（既存用）
$con = connect2db($fname);

if($umode == "usr"){
	// 権限のチェック
	$checkauth = check_authority($session, 5, $fname);
	if ($checkauth == "0") {
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
		exit;
	}
}else if($umode == "adm"){
	// 勤務管理権限チェック
	$checkauth = check_authority($session, 42, $fname);
	if ($checkauth == "0") {
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showLoginPage(window);</script>");
		exit;
	}
}
elseif ($umode == "duty") {
    //シフト
    require_once("duty_shift_common_class.php");
    $obj = new duty_shift_common_class($con, $fname);
    //ユーザ画面用
    $chk_flg = $obj->check_authority_user($session, $fname);
    if ($chk_flg == "") {
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showLoginPage(window);</script>");
        exit;
    }
}
elseif ($umode == "apply") {
    ;
}
else{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$work_admin_auth = check_authority($session, 42, $fname);

if($flg == ""){
	$flg = "1";
}

//フォームデータ取得
$p_emp_id 	= $_REQUEST["p_emp_id"];
$date_y1	= $_REQUEST["date_y1"];
$date_m1	= $_REQUEST["date_m1"];
$date_d1	= $_REQUEST["date_d1"];
$date_y2	= $_REQUEST["date_y2"];
$date_m2	= $_REQUEST["date_m2"];
$date_d2	= $_REQUEST["date_d2"];

//データアクセス
try {

	MDBWrapper::init(); // おまじない
	$log = new common_log_class(basename(__FILE__));
	$db = new MDB($log); // ログはnullでも可。指定するとどのPHPからMDBにアクセスした際のエラーかわかる

	$db->beginTransaction(); // トランザクション開始
	
	//部署階層数
	$class_cnt = MasterUtil::get_organization_level_count();
	
	//職員ID・氏名を取得
	$ukey  = "";
	$uType = "";
    if($umode == "usr"){
        $ukey  = $session;
        $uType = "s";
    }else if($umode == "adm" || $umode == "duty" || $umode == "apply"){
		if($selEmpID != ""){
			$ukey  = $selEmpID;
			$uType = "u";
		}
	}
	$user_info  = UserInfo::get($ukey, $uType);
	$base_info  = $user_info->get_base_info();
	$job_info   = $user_info->get_job_info();
	$dept_info  = $user_info->get_dept_info();
	$croom_info = $user_info->get_class_room_info();
	
	//所属
	if($class_cnt == 4 && $croom_info->room_nm != ""){
		$shozoku = $croom_info->room_nm;
	}else{
		$shozoku = $dept_info->dept_nm;
	}
	
	//休暇申請テーブル
	if($base_info->emp_id != ""){
		$cnt = 0;
		$rData = array();
		//atdbkrslt　実績ベース
		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " kintai_hol.apply_date          AS ap_date, ";
		$sql .= " kintai_hol.apply_stat          AS ap_status, ";
		$sql .= " kintai_hol.kind_flag           AS hol_kind, ";
		$sql .= " kintai_hol.reason              AS hol_reason, ";
		$sql .= " kintai_hol.start_date          AS st_date, ";
		$sql .= " kintai_hol.end_date            AS ed_date, ";
		$sql .= " kintai_hol.start_time          AS st_time, ";
		$sql .= " kintai_hol.use_hour            AS use_h, ";
		$sql .= " kintai_hol.use_minute          AS use_m, ";
		$sql .= " kintai_hol.remark              AS hol_remark, ";
		$sql .= " atdbk_reason_mst.sequence      AS reason_seq, ";
		$sql .= " COALESCE( ";
		$sql .= "          NULLIF(atdbk_reason_mst.display_name, ''), ";
		$sql .= "          NULLIF(atdbk_reason_mst.default_name, '') ";
		$sql .= "         )                      AS disp_name, ";
		$sql .= " atdbkrslt.reason               AS hol_reason, ";
		$sql .= " atdbk_reason_mst.holiday_count AS hol_count ";
		$sql .= "FROM ";
		$sql .= " atdbkrslt ";
		$sql .= "LEFT JOIN ";
		$sql .= " atdbk_reason_mst ";
		$sql .= "ON ";
		$sql .= " atdbk_reason_mst.reason_id = atdbkrslt.reason ";
		$sql .= "LEFT JOIN ";
		$sql .= " kintai_hol ";
		$sql .= "ON ";
		$sql .= " kintai_hol.emp_id = atdbkrslt.emp_id AND ";
		$sql .= " kintai_hol.start_date <= atdbkrslt.date AND ";
		$sql .= " kintai_hol.end_date   >= atdbkrslt.date AND ";
		$sql .= " (kintai_hol.kind_flag = '5' OR ";
		$sql .= "  kintai_hol.kind_flag = '6' OR ";
		$sql .= "  kintai_hol.kind_flag = '99') ";
		$sql .= "WHERE ";
		$sql .= " atdbkrslt.emp_id = '".$base_info->emp_id."' AND ";
		$sql .= " atdbkrslt.date >= '".$date_y1.$date_m1.$date_d1."' AND ";
		$sql .= " atdbkrslt.date <= '".$date_y2.$date_m2.$date_d2."' AND ";
		$sql .= " NOT (atdbk_reason_mst.kind_flag = '') ";
		$sql .= "ORDER BY ";
		$sql .= " atdbkrslt.date ASC ";
//echo $sql."<br>";		
		$retRec = $db->find($sql);
//echo count($retRec)."<br>";		
		//$rData = array();
		if(count($retRec) > 0){
			//$cnt = 0;
			$totalJ = array();	//事由別累計用
			$totalS = array();	//種類別累計用
			for($i=0;$i<count($retRec);$i++){
//echo $retRec[$i]["st_date"]."<br>";		
				//日数
				$dateDiff = DateUtil::date_diff($retRec[$i]["st_date"], $retRec[$i]["ed_date"]);
				if($dateDiff > 0){
					for($j=1;$j<=$dateDiff;$j++){
						//日付
						$tmpArr = array();
						$tmpArr = DateUtil::split_ymd($retRec[$i]["st_date"]);
						$stDate = date("Ymd", mktime(0, 
										 			 0, 
										 			 0, 
										 			 intval($tmpArr["m"]), 
										 			 intval($tmpArr["d"]) + ($j - 1), 
										 			 intval($tmpArr["y"])
													 )
									  );
						//範囲外は終了
						if($stDate < $date_y1.$date_m1.$date_d1 || $stDate > $date_y2.$date_m2.$date_d2){
							//範囲外！　処理なし
							
						}else{
							if ($rData[$cnt]["ap_id"] == $apply_id) {
								$apply_id = $rData[$cnt]["ap_id"];
								continue;
							}

							//範囲内！　配列生成			  
							$rData[$cnt]["ymd"] = $stDate;	//Ymd
							$tmpArr = DateUtil::split_ymd($stDate);
							$rData[$cnt]["date"] = intval($tmpArr["m"])."/".intval($tmpArr["d"]);
							//曜日
							$rData[$cnt]["wday"] = DateUtil::day_of_week($stDate, true);
							//承認
							$rData[$cnt]["sts"] = CodeUtil::get_apply_status($retRec[$i]["ap_status"]);
							//事由
							$rData[$cnt]["hol_reason"] = $retRec[$i]["disp_name"];
							//詳細
							$rData[$cnt]["hol_remark"] = $retRec[$i]["hol_remark"];
							//日数
							$rData[$cnt]["daycnt"] = $retRec[$i]["hol_count"];
							//時間数
							$timeM = 0;
							if($retRec[$i]["st_time"] != ""){
								$rData[$cnt]["time"] = intval($retRec[$i]["use_h"]).":".$retRec[$i]["use_m"];
								$timeM = intval($retRec[$i]["use_h"]) * 60 + intval($retRec[$i]["use_m"]);
								//日数クリア
								$rData[$cnt]["daycnt"] = "";
							}
							//申請日
							$tmpArr = DateUtil::split_ymd($retRec[$i]["ap_date"]);
							$rData[$cnt]["ap_date"] = intval($tmpArr["m"])."/".intval($tmpArr["d"]);
							//特別・病気
							$rData[$cnt]["kind"] = CodeUtil::get_vacation_kind($retRec[$i]["hol_kind"]);
							
							//累計
							//事由
							if($retRec[$i]["ap_status"] == 1){
								$totalJ[$retRec[$i]["hol_reason"]]["seq"]   = $retRec[$i]["reason_seq"];
								$totalJ[$retRec[$i]["hol_reason"]]["name"]  = $retRec[$i]["disp_name"];
								$totalJ[$retRec[$i]["hol_reason"]]["day"]   = floatval($totalJ[$retRec[$i]["hol_reason"]]["day"]) + floatval($rData[$cnt]["daycnt"]);
								$totalJ[$retRec[$i]["hol_reason"]]["timeM"] = intval($totalJ[$retRec[$i]["hol_reason"]]["timeM"]) + $timeM;
								//種類
								$totalS[$retRec[$i]["hol_kind"]]["name"]  = CodeUtil::get_vacation_kind($retRec[$i]["hol_kind"], true);
								$totalS[$retRec[$i]["hol_kind"]]["day"]   = intval($totalS[$retRec[$i]["hol_kind"]]["day"]) + intval($rData[$cnt]["daycnt"]);
								$totalS[$retRec[$i]["hol_kind"]]["timeM"] = intval($totalS[$retRec[$i]["hol_kind"]]["timeM"]) + $timeM;
							}
							
							$cnt++;
						}
					}
				}
			}
		}
		//kintai_hol　申請ベース
		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " kintai_hol.apply_date          AS ap_date, ";
		$sql .= " kintai_hol.apply_stat          AS ap_status, ";
		$sql .= " kintai_hol.kind_flag           AS hol_kind, ";
		$sql .= " kintai_hol.reason              AS hol_reason, ";
		$sql .= " kintai_hol.start_date          AS st_date, ";
		$sql .= " kintai_hol.end_date            AS ed_date, ";
		$sql .= " kintai_hol.start_time          AS st_time, ";
		$sql .= " kintai_hol.use_hour            AS use_h, ";
		$sql .= " kintai_hol.use_minute          AS use_m, ";
		$sql .= " kintai_hol.remark              AS hol_remark, ";
		$sql .= " atdbk_reason_mst.sequence      AS reason_seq, ";
		$sql .= " COALESCE( ";
		$sql .= "          NULLIF(atdbk_reason_mst.display_name, ''), ";
		$sql .= "          NULLIF(atdbk_reason_mst.default_name, '') ";
		$sql .= "         )                      AS disp_name, ";
		$sql .= " atdbk_reason_mst.holiday_count AS hol_count ";
		$sql .= "FROM ";
		$sql .= " kintai_hol ";
		$sql .= "LEFT JOIN ";
		$sql .= " atdbk_reason_mst ";
		$sql .= "ON ";
		$sql .= " atdbk_reason_mst.reason_id = kintai_hol.reason ";
		$sql .= "WHERE ";
		$sql .= " kintai_hol.emp_id = '".$base_info->emp_id."' AND ";
		$sql .= " (kintai_hol.apply_stat = '0' OR ";
		$sql .= "  kintai_hol.apply_stat = '1') AND ";
		$sql .= " (kintai_hol.kind_flag = '5' OR ";
		$sql .= "  kintai_hol.kind_flag = '6' OR ";
		$sql .= "  kintai_hol.kind_flag = '99') AND ";
		$sql .= " ((kintai_hol.start_date >= '".$date_y1.$date_m1.$date_d1."' AND kintai_hol.start_date <= '".$date_y2.$date_m2.$date_d2."') OR ";
		$sql .= "  (kintai_hol.end_date   >= '".$date_y1.$date_m1.$date_d1."' AND kintai_hol.end_date   <= '".$date_y2.$date_m2.$date_d2."')) ";
		$sql .= "ORDER BY ";
		$sql .= " kintai_hol.start_date ASC ";
//echo $sql."<br>";		
		$retRec = $db->find($sql);
//echo count($retRec)."<br>";		
		if(count($retRec) > 0){
			$totalJ = array();	//事由別累計用
			$totalS = array();	//種類別累計用
			for($i=0;$i<count($retRec);$i++){
//echo $retRec[$i]["st_date"]."<br>";		
				//日数
				$dateDiff = DateUtil::date_diff($retRec[$i]["st_date"], $retRec[$i]["ed_date"]);
				if($dateDiff > 0){
					for($j=1;$j<=$dateDiff;$j++){
						//日付
						$tmpArr = array();
						$tmpArr = DateUtil::split_ymd($retRec[$i]["st_date"]);
						$stDate = date("Ymd", mktime(0, 
										 			 0, 
										 			 0, 
										 			 intval($tmpArr["m"]), 
										 			 intval($tmpArr["d"]) + ($j - 1), 
										 			 intval($tmpArr["y"])
													 )
									  );
						//範囲外は終了
						if($stDate < $date_y1.$date_m1.$date_d1 || $stDate > $date_y2.$date_m2.$date_d2){
							//範囲外！　処理なし
							
						}else{
							//存在する日付はスルー
							$chkFlg = 0;
							for($chkI=0;$chkI<count($rData);$chkI++){
								if($rData[$chkI]["ymd"] == $stDate){
									$chkFlg = 1;
									break;
								}
							}
							if($chkFlg == 1){
//echo $stDate.":スルー<br>";
								break;
							}
							//範囲内！　配列生成			  
							$rData[$cnt]["ymd"] = $stDate;	//Ymd
							$tmpArr = DateUtil::split_ymd($stDate);
							$rData[$cnt]["date"] = intval($tmpArr["m"])."/".intval($tmpArr["d"]);
							//曜日
							$rData[$cnt]["wday"] = DateUtil::day_of_week($stDate, true);
							//承認
							$rData[$cnt]["sts"] = CodeUtil::get_apply_status($retRec[$i]["ap_status"]);
							//事由
							$rData[$cnt]["hol_reason"] = $retRec[$i]["disp_name"];
							//詳細
							$rData[$cnt]["hol_remark"] = $retRec[$i]["hol_remark"];
							//日数
							$rData[$cnt]["daycnt"] = $retRec[$i]["hol_count"];
							//時間数
							$timeM = 0;
							if($retRec[$i]["st_time"] != ""){
								$rData[$cnt]["time"] = intval($retRec[$i]["use_h"]).":".$retRec[$i]["use_m"];
								$timeM = intval($retRec[$i]["use_h"]) * 60 + intval($retRec[$i]["use_m"]);
								//日数クリア
								$rData[$cnt]["daycnt"] = "";
							}
							//申請日
							$tmpArr = DateUtil::split_ymd($retRec[$i]["ap_date"]);
							$rData[$cnt]["ap_date"] = intval($tmpArr["m"])."/".intval($tmpArr["d"]);
							//特別・病気
							$rData[$cnt]["kind"] = CodeUtil::get_vacation_kind($retRec[$i]["hol_kind"]);
							
							//累計
							if($retRec[$i]["ap_status"] == 1){
								//事由
								$totalJ[$retRec[$i]["hol_reason"]]["seq"]   = $retRec[$i]["reason_seq"];
								$totalJ[$retRec[$i]["hol_reason"]]["name"]  = $retRec[$i]["disp_name"];
								$totalJ[$retRec[$i]["hol_reason"]]["day"]   = floatval($totalJ[$retRec[$i]["hol_reason"]]["day"]) + floatval($rData[$cnt]["daycnt"]);
								$totalJ[$retRec[$i]["hol_reason"]]["timeM"] = intval($totalJ[$retRec[$i]["hol_reason"]]["timeM"]) + $timeM;
								//種類
								$totalS[$retRec[$i]["hol_kind"]]["name"]  = CodeUtil::get_vacation_kind($retRec[$i]["hol_kind"], true);
								$totalS[$retRec[$i]["hol_kind"]]["day"]   = intval($totalS[$retRec[$i]["hol_kind"]]["day"]) + intval($rData[$cnt]["daycnt"]);
								$totalS[$retRec[$i]["hol_kind"]]["timeM"] = intval($totalS[$retRec[$i]["hol_kind"]]["timeM"]) + $timeM;
							}
							
							$cnt++;
						}
					}
				}
			}
		}
		
		//ソート
		foreach ($rData as $key => $rA){
		  $rS[$key] = $rA["ymd"];
		}
		array_multisort($rS,SORT_ASC, $rData);

		
	}

	$db->endTransaction(); // トランザクション終了
} catch (BusinessException $bex) {
	$message = $bex->getMessage();
	echo("<script type='text/javascript'>alert('".$message."');</script>");
	echo("<script language='javascript'>history.back();</script>");
	exit;
} catch (FatalException $fex) { 
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

//事由別累計配列操作
//ソート
foreach ($totalJ as $key => $arr){
  $seq[$key] = $arr["seq"];
}
array_multisort($seq,SORT_ASC, $totalJ);
//出力用配列へ格納
$arrJiyu  = array();
$arrJiyuD = array();
$cnt = 0;
foreach($totalJ as $key => $arr){
	$arrJiyu[$cnt] = $arr["name"];
	if(intval($arr["day"]) > 0){
		//$arrJiyu[$cnt] .= " ".$arr["day"];
		$arrJiyuD[$cnt] = $arr["day"];
	}
	if(intval($arr["timeM"]) > 0){
		$arrJiyuD[$cnt] .= "(".floor($arr["timeM"] / 60).":".sprintf("%02d",($arr["timeM"] % 60)).")";
	}
	$cnt++;
}
//種類別累計配列操作
//出力用配列へ格納
$arrShurui  = array();
$arrShuruiD = array();
$cnt = 0;
if($totalS[5]["day"] > 0 || $totalS[5]["timeM"] > 0){
	$arrShurui[$cnt] = $totalS[5]["name"];
	if(intval($totalS[5]["day"]) > 0){
		//$arrShurui[$cnt] .= " ".$totalS[5]["day"];
		$arrShuruiD[$cnt] = $totalS[5]["day"];
	}
	if(intval($totalS[5]["timeM"]) > 0){
		$arrShuruiD[$cnt] .= "(".floor($totalS[5]["timeM"] / 60).":".sprintf("%02d",($totalS[5]["timeM"] % 60)).")";
	}
	$cnt++;
}
if($totalS[6]["day"] > 0 || $totalS[6]["timeM"] > 0){
	$arrShurui[$cnt] = $totalS[6]["name"];
	if(intval($totalS[6]["day"]) > 0){
		//$arrShurui[$cnt] .= " ".$totalS[6]["day"];
		$arrShuruiD[$cnt] = $totalS[6]["day"];
	}
	if(intval($totalS[6]["timeM"]) > 0){
		$arrShuruiD[$cnt] .= "(".floor($totalS[6]["timeM"] / 60).":".sprintf("%02d",($totalS[6]["timeM"] % 60)).")";
	}
	$cnt++;
}
if($totalS[99]["day"] > 0 || $totalS[99]["timeM"] > 0){
	$arrShurui[$cnt] = $totalS[99]["name"];
	if(intval($totalS[99]["day"]) > 0){
		//$arrShurui[$cnt] .= " ".$totalS[7]["day"];
		$arrShuruiD[$cnt] = $totalS[99]["day"];
	}
	if(intval($totalS[99]["timeM"]) > 0){
		$arrShuruiD[$cnt] .= "(".floor($totalS[99]["timeM"] / 60).":".sprintf("%02d",($totalS[99]["timeM"] % 60)).")";
	}
	$cnt++;
}

//====================================
//	エクセル出力クラス
//====================================
require_once("./kintai/common/kintai_print_excel_class.php");
// Excelオブジェクト生成
$excelObj = new ExcelWorkShop();
// Excel列名。列名をloopで連続するときに使うと便利です。
$excelColumnName = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ','BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN');
// 用紙の向き。横方向の例
$excelObj->PageSetUp("virtical");
// 用紙サイズ
$excelObj->PaperSize( "A4" );
// 倍率と自動或いは利用者指定。利用者が指定して95%で印刷する場合
$excelObj->PageRatio( "USER" , "95" );
// 余白指定、左,右,上,下,ヘッダ,フッタ
$excelObj->SetMargin( "1.0" , "1,0", "1.5" , "1.5" , "0.5" , "0.5" );
// ヘッダー情報
$HeaderMessage = '&"ＭＳ ゴシック"&11&B休暇簿';
$HeaderMessage = $HeaderMessage . "&R".date("Y.n.j");
$excelObj->SetPrintHeader( $HeaderMessage );
//フッター
$strFooter  = '&C&P / &N';
$excelObj->SetPrintFooter($strFooter);
// シート名
$excelObj->SetSheetName(mb2("休暇簿"));
// 列幅
$colWdArr = array(5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5);
for($i=0;$i<count($colWdArr);$i++){
	$excelObj->SetColDim($excelColumnName[$i], $colWdArr[$i]);
}
//デフォルトフォント
$excelObj->setDefaultFont("ＭＳ ゴシック", 9, true);

//基本■■■■■■■■■■■■■■■■■■
//開始位置
$ROW_ST = 3;
//行のタイトル
$excelObj->setRowTitle(1, $ROW_ST);
//ウィンド枠固定
$excelObj->setPane(0,$ROW_ST+1);

//期間
$excelObj->CellMerge2("A1:B1", "", "", "");
$excelObj->SetFontBold();
$excelObj->SetValueJP2("A1", "対象期間：", "VCENTER HRIGHT", mb1("ＭＳ ゴシック"), 9);
$excelObj->CellMerge2("C1:I1", "", "", "");
$excelObj->SetFontBold();
$excelObj->SetValueJP2("C1", $date_y1."年".intval($date_m1)."月".intval($date_d1)."日〜".$date_y2."年".intval($date_m2)."月".intval($date_d2)."日", "VCENTER HLEFT", mb1("ＭＳ ゴシック"), 9);
//職員
$excelObj->CellMerge2("J1:K1", "", "", "");
$excelObj->SetFontBold();
$excelObj->SetValueJP2("J1", "所属：", "VCENTER HRIGHT", mb1("ＭＳ ゴシック"), 9);
$excelObj->CellMerge2("L1:U1", "", "", "");
$excelObj->SetFontBold();
$excelObj->SetValueJP2("L1", $shozoku, "VCENTER HLEFT", mb1("ＭＳ ゴシック"), 9);
$excelObj->CellMerge2("A2:B2", "", "", "");
$excelObj->SetFontBold();
$excelObj->SetValueJP2("A2", "職員ID：", "VCENTER HRIGHT", mb1("ＭＳ ゴシック"), 9);
$excelObj->CellMerge2("C2:F2", "", "", "");
$excelObj->SetFontBold();
$excelObj->SetValueJP2("C2", $base_info->emp_personal_id, "VCENTER HLEFT", mb1("ＭＳ ゴシック"), 9);
$excelObj->CellMerge2("G2:H2", "", "", "");
$excelObj->SetFontBold();
$excelObj->SetValueJP2("G2", "氏名：", "VCENTER HRIGHT", mb1("ＭＳ ゴシック"), 9);
$excelObj->CellMerge2("I2:U2", "", "", "");
$excelObj->SetFontBold();
$excelObj->SetValueJP2("I2", $base_info->emp_lt_nm." ".$base_info->emp_ft_nm."（".$job_info->job_nm."）", "VCENTER HLEFT", mb1("ＭＳ ゴシック"), 9);

if(MasterUtil::get_kintai_ptn() == 0){
//通常
	//現在行位置
	$y = $ROW_ST;

	//タイトル行
	$excelObj->CellMerge2("A".$y.":B".$y, "", "", "");
	$excelObj->SetValueJP2("A".$y, "日付", "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
	$excelObj->SetArea("C".$y); 
	$excelObj->SetValueJP2("C".$y, "曜日", "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
	$excelObj->CellMerge2("D".$y.":F".$y, "", "", "");
	$excelObj->SetValueJP2("D".$y, "事由", "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
	$excelObj->CellMerge2("G".$y.":H".$y, "", "", "");
	$excelObj->SetValueJP2("G".$y, "日数", "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);

	//リスト
	for($i=0;$i<count($rData);$i++){
		$y++;
		$excelObj->CellMerge2("A".$y.":B".$y, "", "", "");
		$excelObj->SetValueJP2("A".$y, $rData[$i]["date"], "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
		$excelObj->SetArea("C".$y); 
		$excelObj->SetValueJP2("C".$y, $rData[$i]["wday"], "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
		$excelObj->CellMerge2("D".$y.":F".$y, "", "", "");
		$excelObj->SetValueJP2("D".$y, $rData[$i]["hol_reason"], "VCENTER HLEFT", mb1("ＭＳ Ｐゴシック"), 9);
		$excelObj->CellMerge2("G".$y.":H".$y, "", "", "");
		$excelObj->SetValueJP2("G".$y, $rData[$i]["daycnt"], "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
	}
	//罫線
	$excelObj->SetArea("A".$ROW_ST.":H".$y); 
	$excelObj->SetBorder("THIN", "", "all");
	
}else{
//特殊
	//現在行位置
	$y = $ROW_ST;

	//タイトル行
	$excelObj->CellMerge2("A".$y.":B".$y, "", "", "");
	$excelObj->SetValueJP2("A".$y, "日付", "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
	$excelObj->SetArea("C".$y); 
	$excelObj->SetValueJP2("C".$y, "曜日", "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
	$excelObj->CellMerge2("D".$y.":E".$y, "", "", "");
	$excelObj->SetValueJP2("D".$y, "承認", "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
	$excelObj->CellMerge2("F".$y.":H".$y, "", "", "");
	$excelObj->SetValueJP2("F".$y, "事由", "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
	$excelObj->CellMerge2("I".$y.":M".$y, "", "", "");
	$excelObj->SetValueJP2("I".$y, "詳細", "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
	$excelObj->CellMerge2("N".$y.":O".$y, "", "", "");
	$excelObj->SetValueJP2("N".$y, "日数", "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
	$excelObj->CellMerge2("P".$y.":Q".$y, "", "", "");
	$excelObj->SetValueJP2("P".$y, "時間数", "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
	$excelObj->CellMerge2("R".$y.":S".$y, "", "", "");
	$excelObj->SetValueJP2("R".$y, "申請日", "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
	$excelObj->CellMerge2("T".$y.":U".$y, "", "", "");
	$excelObj->SetValueJP2("T".$y, "種類", "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
	
	//リスト
	for($i=0;$i<count($rData);$i++){
		$y++;
		$excelObj->CellMerge2("A".$y.":B".$y, "", "", "");
		$excelObj->SetValueJP2("A".$y, $rData[$i]["date"], "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
		$excelObj->SetArea("C".$y); 
		$excelObj->SetValueJP2("C".$y, $rData[$i]["wday"], "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
		$excelObj->CellMerge2("D".$y.":E".$y, "", "", "");
		$excelObj->SetValueJP2("D".$y, $rData[$i]["sts"], "VCENTER HLEFT", mb1("ＭＳ Ｐゴシック"), 9);
		$excelObj->CellMerge2("F".$y.":H".$y, "", "", "");
		$excelObj->SetValueJP2("F".$y, $rData[$i]["hol_reason"], "VCENTER HLEFT", mb1("ＭＳ Ｐゴシック"), 9);
		$excelObj->CellMerge2("I".$y.":M".$y, "", "", "");
		$excelObj->SetValueJP2("I".$y, $rData[$i]["hol_remark"], "VCENTER HLEFT", mb1("ＭＳ Ｐゴシック"), 9);
		$excelObj->CellMerge2("N".$y.":O".$y, "", "", "");
		$excelObj->SetValueJP2("N".$y, $rData[$i]["daycnt"], "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
		$excelObj->CellMerge2("P".$y.":Q".$y, "", "", "");
		$excelObj->SetValueJP2("P".$y, $rData[$i]["time"], "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
		$excelObj->CellMerge2("R".$y.":S".$y, "", "", "");
		$excelObj->SetValueJP2("R".$y, $rData[$i]["ap_date"], "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
		$excelObj->CellMerge2("T".$y.":U".$y, "", "", "");
		$excelObj->SetValueJP2("T".$y, $rData[$i]["kind"], "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
	}
	//罫線
	$excelObj->SetArea("A".$ROW_ST.":U".$y); 
	$excelObj->SetBorder("THIN", "", "all");
	
}

//累計
$posT = array("B", "G", "L", "Q");
$posD = array("E", "J", "O", "T");
$y++;
$excelObj->SetArea("A".$y); 
$excelObj->SetValueJP2("A".$y, "〔事由別累計〕", "VCENTER HLEFT", mb1("ＭＳ ゴシック"), 9);
$pos  = 0;
$y++;
for($i=0;$i<count($arrJiyu);$i++){
	if($pos > 3){
		$pos = 0;
		$y++;
	}
	$excelObj->SetArea($posT[$pos].$y); 
	$excelObj->SetValueJP2($posT[$pos].$y, $arrJiyu[$i], "VCENTER HLEFT", mb1("ＭＳ Ｐゴシック"), 9);
	$excelObj->SetArea($posD[$pos].$y); 
	$excelObj->SetValueJP2($posD[$pos].$y, $arrJiyuD[$i], "VCENTER HLEFT", mb1("ＭＳ ゴシック"), 9);
	$pos++;
}
if(MasterUtil::get_kintai_ptn() != 0){
	$pos  = 0;
	$y++;
	$excelObj->SetArea("A".$y); 
	$excelObj->SetValueJP2("A".$y, "〔種類別累計〕", "VCENTER HLEFT", mb1("ＭＳ ゴシック"), 9);
	$y++;
	for($i=0;$i<count($arrShurui);$i++){
		if($pos > 3){
			$pos = 0;
			$y++;
		}
		$excelObj->SetArea($posT[$pos].$y); 
		$excelObj->SetValueJP2($posT[$pos].$y, $arrShurui[$i], "VCENTER HLEFT", mb1("ＭＳ Ｐゴシック"), 9);
		$excelObj->SetArea($posD[$pos].$y); 
		$excelObj->SetValueJP2($posD[$pos].$y, $arrShuruiD[$i], "VCENTER HLEFT", mb1("ＭＳ ゴシック"), 9);
		$pos++;
	}
}
$excelObj->SetArea("A1"); 
// 出力ファイル名
$filename = "List_of_holiday.xls";
ob_clean();
// ダウンロード形式
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$filename);
header('Cache-Control: max-age=0');
$excelObj->OutPut();

function mb1($str){
    return mb_convert_encoding($str, "UTF-8", "EUC-JP");
}
function mb2($str){
	return mb_convert_encoding(mb_convert_encoding($str, "sjis-win", "eucJP-win"), "UTF-8", "sjis-win");
}
?>
