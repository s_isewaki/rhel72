<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 55, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if ($system == "") {
	echo("<script type=\"text/javascript\">alert('制度が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if (strlen($system) > 100) {
	echo("<script type=\"text/javascript\">alert('制度が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
for ($i = 1; $i <= 3; $i++) {
	if ($_FILES["file$i"]["name"] != "") {
		switch ($_FILES["file$i"]["error"]) {
		case 1:  // UPLOAD_ERR_INI_SIZE
		case 2:  // UPLOAD_ERR_FORM_SIZE
			echo("<script type=\"text/javascript\">alert('ファイルサイズが大きすぎます。');</script>");
			echo("<script type=\"text/javascript\">history.back();</script>");
			exit;
		case 3:  // UPLOAD_ERR_PARTIAL:
		case 4:  // UPLOAD_ERR_NO_FILE:
			echo("<script type=\"text/javascript\">alert('アップロードに失敗しました。再度実行してください。');</script>");
			echo("<script type=\"text/javascript\">history.back();</script>");
			exit;
		}
	}
}

// データベースに接続
$con = connect2db($fname);

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// 文書IDを採番
$sql = "select max(welfare_id) from welfare";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$welfare_id = intval(pg_fetch_result($sel, 0, 0)) + 1;

// 文書情報を登録
$sql = "insert into welfare (welfare_id, lifestage_id, system, description, update_time, emp_id) values (";
$content = array($welfare_id, $stage_id, $system, $description, date("YmdHis"), $emp_id);
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// データベース接続を閉じる
pg_close($con);

// 既存ファイルの削除
exec("rm -f " . getcwd() . "/intra/welfare/{$welfare_id}_*.*");

// ファイルを保存
for ($i = 1; $i <= 3; $i++) {
	if ($_FILES["file$i"]["name"] != "") {
		copy($_FILES["file$i"]["tmp_name"], "intra/welfare/{$welfare_id}_$i" . strrchr($_FILES["file$i"]["name"], "."));
	}
}

// 自画面を閉じ、親画面をリフレッシュ
echo("<script type=\"text/javascript\">self.close(); opener.location.href = 'intra_life_welfare.php?session=$session&default_stage_id=$stage_id';</script>");
?>
