<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>経営支援 | 表示設定</title>
<? require("about_authority.php"); ?>
<? require("about_session.php"); ?>
<? require("about_postgres.php"); ?>
<? require("./conf/sql.inf"); ?>
<? require("get_values.ini"); ?>
<?
require("label_by_profile_type.ini");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 経営支援権限チェック
$auth = check_authority($session,25,$fname);
if($auth == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//DBコネクションの作成
$con = connect2db($fname);

// 表示設定値の取得
$emp_id = get_emp_id($con, $session, $fname);
$sql = "select * from option";
$cond = "where emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$rate_visible = (pg_fetch_result($sel, 0, "biz_bed_rate") == "t");
$avg_rate_visible = (pg_fetch_result($sel, 0, "biz_bed_avg_rate") == "t");
$active_rate_visible = (pg_fetch_result($sel, 0, "biz_bed_actv_rate") == "t");
$avg_days_visible = (pg_fetch_result($sel, 0, "biz_bed_avg_days") == "t");
$simulation_visible = (pg_fetch_result($sel, 0, "biz_bed_sml_days") == "t");
// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 病床/居室
$bed_title = $_label_by_profile["BED"][$profile_type];

// 在院/在所
$stay_hospital_title = $_label_by_profile["STAY_HOSPITAL"][$profile_type];

?>
<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="biz_menu.php?session=<? echo($session); ?>"><img src="img/icon/b18.gif" width="32" height="32" border="0" alt="経営支援"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="biz_menu.php?session=<? echo($session); ?>"><b>経営支援</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="120" height="22" align="center" bgcolor="#bdd1e7"><a href="./biz_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">経営支援</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#5279a5"><a href="biz_option.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>表示設定</b></font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<form action="biz_option_confirm.php" method="post">
<table width="600" border="0" cellspacing="0" cellpadding="1">
<tr>
<td height="22"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$bed_title?>利用率：<input type="radio" name="rate" value="t"<? if ($rate_visible) {echo(" checked");} ?>>表示する&nbsp;<input type="radio" name="rate" value="f"<? if (!$rate_visible) {echo(" checked");} ?>>表示しない</font></td>
</tr>
<tr>
<td height="22"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">平均<?=$bed_title?>利用率（過去3か月）：<input type="radio" name="avg_rate" value="t"<? if ($avg_rate_visible) {echo(" checked");} ?>>表示する&nbsp;<input type="radio" name="avg_rate" value="f"<? if (!$avg_rate_visible) {echo(" checked");} ?>>表示しない</font></td>
</tr>
<tr>
<td height="22"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$bed_title?>稼動率（過去3か月）：<input type="radio" name="active_rate" value="t"<? if ($active_rate_visible) {echo(" checked");} ?>>表示する&nbsp;<input type="radio" name="active_rate" value="f"<? if (!$active_rate_visible) {echo(" checked");} ?>>表示しない</font></td>
</tr>
<tr>
<td height="22"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">平均<?=$stay_hospital_title?>日数（過去3か月）：<input type="radio" name="avg_days" value="t"<? if ($avg_days_visible) {echo(" checked");} ?>>表示する&nbsp;<input type="radio" name="avg_days" value="f"<? if (!$avg_days_visible) {echo(" checked");} ?>>表示しない</font></td>
</tr>
<tr>
<td height="22"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">平均<?=$stay_hospital_title?>日数（シミュレーション）：<input type="radio" name="simulation" value="t"<? if ($simulation_visible) {echo(" checked");} ?>>表示する&nbsp;<input type="radio" name="simulation" value="f"<? if (!$simulation_visible) {echo(" checked");} ?>>表示しない</font></td>
</tr>
<tr>
<td height="22" colspan="2" align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="emp_id" value="<? echo($emp_id); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
