<?
require_once("about_comedix.php");
require_once("show_select_values.ini");
require_once("yui_calendar_util.ini");
require_once("get_values.ini");
require_once("fplus_draft_template.ini");
require_once("fplus_common_class.php");
require_once("get_values_for_template.ini");
require_once("library_common.php");

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック
//====================================
$checkauth=check_authority($session,78,$fname);
if($checkauth=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}


//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$obj = new fplus_common_class($con, $fname);

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ファントルくん＋ | 報告印刷</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
<script type="text/javascript">
// テキストエリア行数の自動拡張
var ResizingTextArea = Class.create();

ResizingTextArea.prototype = {
    defaultRows: 1,

    initialize: function(field)
    {
        this.defaultRows = Math.max(field.rows, 1);
        this.resizeNeeded = this.resizeNeeded.bindAsEventListener(this);
        Event.observe(field, "click", this.resizeNeeded);
        Event.observe(field, "keyup", this.resizeNeeded);
    },

    resizeNeeded: function(event)
    {
        var t = Event.element(event);
        var lines = t.value.split('\n');
        var newRows = lines.length + 1;
        var oldRows = t.rows;
        for (var i = 0; i < lines.length; i++)
        {
            var line = lines[i];
            if (line.length >= t.cols) newRows += Math.floor(line.length / t.cols);
        }
        if (newRows > t.rows) t.rows = newRows;
        if (newRows < t.rows) t.rows = Math.max(this.defaultRows, newRows);
    }
}

// 全テキストエリアの行数変更
function resizeAllTextArea() {
	var objs = document.getElementsByTagName('textarea');
	for (var i = 0, j = objs.length; i < j; i++) {
	    var t = objs[i];
		var defaultRows = Math.max(t.rows, 1);
	    var lines = t.value.split('\n');
	    var newRows = lines.length + 1;
	    var oldRows = t.rows;
	    for (var k = 0; k < lines.length; k++)
	    {
	        var line = lines[k];
	        if (line.length >= t.cols) newRows += Math.floor(line.length / t.cols);
	    }
	    if (newRows > t.rows) t.rows = newRows;
	    if (newRows < t.rows) t.rows = Math.max(defaultRows, newRows);
	}
}
</script>
<?
	if($target_apply_id == "") {
		$target_apply_id = $apply_id;
	}


	// 申請・ワークフロー情報取得
	$arr_apply_wkfwmst = $obj->get_apply_wkfwmst($target_apply_id);
	$wkfw_content      = $arr_apply_wkfwmst[0]["wkfw_content"];
	$wkfw_content_type = $arr_apply_wkfwmst[0]["wkfw_content_type"];
	$apply_content     = $arr_apply_wkfwmst[0]["apply_content"];

/*
	// ワークフロー情報取得
	$sel_apply_wkfwmst = search_apply_wkfwmst($con, $fname, $target_apply_id);
	$wkfw_content = pg_fetch_result($sel_apply_wkfwmst, 0, "wkfw_content");
	$wkfw_content_type = pg_fetch_result($sel_apply_wkfwmst, 0, "wkfw_content_type");
	$apply_content = pg_fetch_result($sel_apply_wkfwmst, 0, "apply_content");
*/


	// 本文形式タイプのデフォルトを「テキスト」とする
	if ($wkfw_content_type == "") {$wkfw_content_type = "1";}

	// 形式をテキストからテンプレートに変更した場合の古いデータ対応
	// wkfw_content_typeは2:テンプレートだが、登録データがXMLでない場合、1:テキストとして処理する
	if ($wkfw_content_type == "2") {
		if (strpos($apply_content, "<?xml") === false) {
			$wkfw_content_type = "1";
		}
	}

	$num = 0;
	if ($wkfw_content_type == "2") {
		$pos = 0;
		while (1) {
			$pos = strpos($wkfw_content, 'show_cal', $pos);
			if ($pos === false) {
				break;
			} else {
				$num++;
			}
			$pos++;
		}
	}

	if ($num > 0) {
		// 外部ファイルを読み込む
		write_yui_calendar_use_file_read_0_12_2();
	}
	// カレンダー作成、関数出力
	write_yui_calendar_script2($num);
?>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

.list {border-collapse:collapse;}
.list td {border:#E6B3D4 solid 1px;}

table.block {border-collapse:collapse;}
table.block td {border:#E6B3D4 solid 1px;padding:1px;}
table.block td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#E6B3D4 solid 0px;}


table.block2 {border-collapse:collapse;}
table.block2 td {border:#E6B3D4 solid 1px;padding:1px;}
table.block2 td {border-width:1;}

table.block3 {border-collapse:collapse;}
table.block3 td {border:#E6B3D4 solid 0px;}
table.block3 td {border-width:0;}


p {margin:0;}

</style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initcal();if (window.OnloadSub) { OnloadSub(); };resizeAllTextArea();copyApproveOrders();">
<form name="apply" action="#" method="post">
<?
show_draft_template($con, $session, $fname, $target_apply_id, $apply_title, $content, $file_id, $filename, $back, $mode);
?>
    <table id="approve_orders" width="100%" border="0" cellspacing="0" cellpadding="2" class="block2">
    </table>
</form>
</body>
<script type="text/javascript">
function copyApproveOrders() {
	opener.parent.updateDOM();

	var self_table = document.getElementById('approve_orders');
	var opener_rows = opener.parent.document.getElementById('approve_orders').rows;
	for (var i = 0, j = opener_rows.length; i < j; i++) {
		var row = self_table.insertRow(-1);
		row.style.backgroundColor = opener_rows[i].style.backgroundColor;
		row.style.height = opener_rows[i].style.height;
		row.style.textAlign = 'center';

		var opener_cells = opener_rows[i].cells;
		for (var m = 0, n = opener_cells.length; m < n; m++) {
			var cell = row.insertCell(-1);
			cell.style.width = '20%';
			cell.innerHTML = opener_cells[m].innerHTML;
		}
	}
}

document.apply.apply_title.value=opener.parent.document.apply.apply_title.value;
setTimeout("self.print();self.close();", 100);
</script>
</html>
<?
pg_close($con);
?>
