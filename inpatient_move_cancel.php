<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// 最新の転床情報を取得
$sql = "select * from inptmove";
$cond = "where ptif_id = '$ptif_id' and move_del_flg = 'f' order by move_dt desc, move_tm desc limit 1";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$move_dt = pg_fetch_result($sel, 0, "move_dt");
$move_tm = pg_fetch_result($sel, 0, "move_tm");
$move_cfm_flg = pg_fetch_result($sel, 0, "move_cfm_flg");
$from_bldg_cd = pg_fetch_result($sel, 0, "from_bldg_cd");
$from_ward_cd = pg_fetch_result($sel, 0, "from_ward_cd");
$from_ptrm_room_no = pg_fetch_result($sel, 0, "from_ptrm_room_no");
$from_bed_no = pg_fetch_result($sel, 0, "from_bed_no");
$to_bldg_cd = pg_fetch_result($sel, 0, "to_bldg_cd");
$to_ward_cd = pg_fetch_result($sel, 0, "to_ward_cd");
$to_ptrm_room_no = pg_fetch_result($sel, 0, "to_ptrm_room_no");
$to_bed_no = pg_fetch_result($sel, 0, "to_bed_no");

// 最新の転床データでなければ取消不可
if ($move_dt > "$list_year2$list_month2$list_day2" || $move_cfm_flg == "f") {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('最新の転床データでないため取消できません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// 転床元の情報を取得
$sql = "select ptif_id from inptmst";
$cond = "where bldg_cd = '$from_bldg_cd' and ward_cd = '$from_ward_cd' and ptrm_room_no = '$from_ptrm_room_no' and inpt_bed_no = '$from_bed_no'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$from_ptif_id = pg_fetch_result($sel, 0, "ptif_id");

// 転床元が空床でも同一患者でもなければ取消不可
if ($from_ptif_id != "" && $from_ptif_id !== $ptif_id) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('転床元が空床でないため取消できません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

if ($from_ptif_id == "") {

	// 転床元の入院情報を一時退避
	$sql = "update inptmst set";
	$set = array("inpt_bed_no");
	$setvalue = array("XX");
	$cond = "where bldg_cd = '$from_bldg_cd' and ward_cd = '$from_ward_cd' and ptrm_room_no = '$from_ptrm_room_no' and inpt_bed_no = '$from_bed_no'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// 転床先の入院情報を転床元に変更
	$sql = "update inptmst set";
	$set = array("bldg_cd", "ward_cd", "ptrm_room_no", "inpt_bed_no");
	$setvalue = array($from_bldg_cd, $from_ward_cd, $from_ptrm_room_no, $from_bed_no);
	$cond = "where bldg_cd = '$to_bldg_cd' and ward_cd = '$to_ward_cd' and ptrm_room_no = '$to_ptrm_room_no' and inpt_bed_no = '$to_bed_no'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// 転床元の入院情報を転床先に変更
	$sql = "update inptmst set";
	$set = array("bldg_cd", "ward_cd", "ptrm_room_no", "inpt_bed_no");
	$setvalue = array($to_bldg_cd, $to_ward_cd, $to_ptrm_room_no, $to_bed_no);
	$cond = "where bldg_cd = '$from_bldg_cd' and ward_cd = '$from_ward_cd' and ptrm_room_no = '$from_ptrm_room_no' and inpt_bed_no = 'XX'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// 転床情報を削除
$sql = "update inptmove set";
$set = array("move_del_flg");
$setvalue = array("t");
$cond = "where ptif_id = '$ptif_id' and move_dt = '$move_dt' and move_tm = '$move_tm'";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 病床状況レコードを削除
$sql = "delete from inptcond";
$cond = "where ptif_id = '$ptif_id' and date = '$move_dt' and hist_flg = 'f'";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 一覧画面を再表示
echo "<script type=\"text/javascript\">location.href = 'inpatient_move_list.php?session=$session&bldgwd=$bldgwd&list_sect_id=$list_sect_id&list_year=$list_year&list_month=$list_month&list_day=$list_day&list_year2=$list_year2&list_month2=$list_month2&list_day2=$list_day2&order=$order';</script>";
