<?
//define("DEBUG", true);
define("DEBUG", false);

require_once("class/Cmx/Model/SystemConfig.php");

ob_start();

if (!isset($_POST["id"]) || (!isset($_POST["passwd"]) && !isset($_POST["systemid"]))) {
    ob_end_clean();
    show_form();
} else {
    $session_id = create_session($_POST["id"], $_POST["passwd"], $_POST["systemid"], $PHP_SELF);
    ob_end_clean();
    show_result($session_id);
}

function show_form() {
    header("Content-Type: text/html; charset=EUC-JP");
?>
<body>
<form method="POST" action="create_session.php">
<p>ID：<input type="text" name="id" value=""></p>
<p>パスワード：<input type="password" name="passwd" value=""></p>
<p>システムID：<input type="password" name="systemid" value=""></p>
<p><input type="submit" value="生成"></p>
</form>
</body>
<?
}

function create_session($id, $passwd, $systemid, $fname) {
    require_once("about_postgres.php");
	require_once('Cmx.php');
	require_once("l4pWClass.php");
	$lfp = new l4pWClass("FANTOL");
	$lfp->infoRst("FANTOL","testtest");
	
	
	//require_once('Cmx.php');
	require_once("aclg_set.php");
    $con = connect2db($fname);

    if ($systemid != "") {
        $sql = "select prf_org_cd from profile";
        $cond = "";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            if (DEBUG) write_log("sql failed, " . pg_last_error($con));
            pg_close($con);
            return "";
        }
        $org_cd = (pg_num_rows($sel) == 1) ? pg_fetch_result($sel, 0, "prf_org_cd") : "";
        if (strcmp($systemid, md5($org_cd)) == 0) {
            $sql = "select l.emp_login_pass from login l";
            $cond = "where l.emp_login_id = '$id' and exists (select * from authmst a where a.emp_id = l.emp_id and (not a.emp_del_flg))";
            $sel = select_from_table($con, $sql, $cond, $fname);
            if ($sel == 0) {
                if (DEBUG) write_log("sql failed, " . pg_last_error($con));
                pg_close($con);
                return "";
            }
            $passwd = (pg_num_rows($sel) == 1) ? pg_fetch_result($sel, 0, "emp_login_pass") : "";
        }
        else {
            pg_close($con);
            return "";
        }
    }

    $sql = "select login.emp_id from login";
    $cond = "where login.emp_login_id = '$id' and login.emp_login_pass = '$passwd' and exists (select * from authmst where authmst.emp_id = login.emp_id and authmst.emp_del_flg = 'f')";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        if (DEBUG) write_log("sql failed, " . pg_last_error($con));
        pg_close($con);
        return "";
    }
    if (pg_num_rows($sel) != 1) {
        if (DEBUG) write_log("employee count is " . pg_num_rows($sel));
        pg_close($con);
        return "";
    }

    $emp_id = pg_fetch_result($sel, 0, "emp_id");
    purge_session($con, $emp_id, $fname);
    $session_id = register_session($con, $emp_id, $fname);
	
	// アクセスログ
	aclg_regist($session_id, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);
	
    pg_close($con);

    if ($session_id == "") {
        if (DEBUG) write_log("generate failed");
        return "";
    }

    return $session_id;
}

function write_log($log) {
    $fp = fopen("create_session.log", "a");
    if (!$fp) return;
    fwrite($fp, "[". date("Y/m/d H:i:s") . "]" . $log . "\r\n");
    fclose($fp);
}

function show_result($session_id) {
    header("Content-Type: text/plain; charset=ASCII");
    if ($session_id != "") {
        echo($session_id);
    }
}

function register_session($con, $emp_id, $fname) {
    $hash_id = create_hash_id();
    if ($hash_id == "") {
        return "";
    }

    require_once("about_postgres.php");
    $sql = "insert into session (session_id, emp_id, session_made) values (";
    $content = array($hash_id, $emp_id, time());
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) {
        if (DEBUG) write_log("sql failed, " . pg_last_error($con));
        return "";
    }

    return $hash_id;
}

function purge_session($con, $emp_id, $fname) {
    $conf = new Cmx_SystemConfig();
    $purge_on_login = $conf->get('session.purge_on_login');
    if (!$purge_on_login) {
        return;
    }

    $sql = "delete from session";
    $cond = "where emp_id = '$emp_id' and session_made <= '" . strtotime("-3 days") . "'";
    delete_from_table($con, $sql, $cond, $fname);
    if ($del == 0) {
        if (DEBUG) write_log("sql failed, " . pg_last_error($con));
    }
}

function create_hash_id() {
    $hash_id = md5(uniqid(rand(), 1));
    if ($hash_id == "") {
        if (DEBUG) write_log("hash failed");
        return "";
    }
    return $hash_id;
}
