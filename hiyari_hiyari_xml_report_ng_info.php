<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");
require_once("hiyari_common.ini");
require_once("hiyari_hiyari_xml_util.ini");

//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$summary = check_authority($session, 47, $fname);
if ($summary == "0") {
	showLoginPage();
	exit;
}


//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
//ポストバック時の処理
//==============================

if($ispostback == "true")
{
	//==============================
	//フリー入力項目の更新
	//==============================
	

	//トランザクション開始
	pg_query($con,"begin transaction");
	
	//デリート
	$sql  = "delete from inci_hiyari_org_item";
	$cond = "where report_id = $report_id";
	$result = delete_from_table($con, $sql, $cond,  $fname);
	if ($result == 0){
		pg_query($con,"rollback");
		pg_close($con);
		showErrorPage();
		exit;
	}
	
	//インサート
	for($org_item_no = 1; $org_item_no <=10; $org_item_no++)
	{
		$free_value_param = "free_value_".$org_item_no;
		$free_value = $$free_value_param;
		if($free_value == "")
		{
			continue;
		}
		
		$sql = "insert into inci_hiyari_org_item values(";
		$registry_data = array(
			$report_id,$org_item_no,$free_value
		);
		$result = insert_into_table($con, $sql, $registry_data, $fname);
		if ($result == 0) {
			pg_query($con,"rollback");
			pg_close($db_con);
			showErrorPage();
			exit;
		}
	}

	//コミット
	pg_query($con, "commit");
}


//==============================
//項目一覧を取得
//==============================
	$sql  = " select * from";
	$sql .= " (";
	$sql .= " select distinct cate_code,grp_code,easy_item_code from inci_report_materials";
	$sql .= " ) i";
	$sql .= " natural inner join inci_easyinput_category_mst";
	$sql .= " natural inner join inci_easyinput_item_mst";
	$sql .= " natural inner join inci_easyinput_group_index";
	$sql .= " order by grp_num,easy_item_code";
	$sel = select_from_table($con,$sql,"",$fname);
	if($sel == 0){
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$item_list_data = pg_fetch_all($sel);
	
	
	//報告部署(9000)を先頭に追加
	$add_item_list_data = null;
	$add_item_list_data['grp_code'] = 9000;
	$add_item_list_data['easy_item_code'] = 9000;
	$add_item_list_data['cate_name'] = "ヘッダー項目";
	$add_item_list_data['easy_item_name'] = "報告部署";
	$add_item_list_data['cate_code'] = 1;
	array_unshift($item_list_data, $add_item_list_data);


//==============================
//ヒヤリハット出力情報を取得
//==============================
$hiyari_obj = new hiyari_hiyari_xml_util($con, $fname);
$mapping_info = $hiyari_obj->get_hiyari_item_mapping_info();
$org_item_setting_info = $hiyari_obj->get_org_item_setting_info();
$inci_profile_use_info = get_inci_profile($fname, $con);
$output_data_info = $hiyari_obj->get_hiyari_output_data_info($report_id,$hiyari_data_type,$mapping_info,$org_item_setting_info,$inci_profile_use_info);

$csv_data = $output_data_info['CSV_DATA'];
$ng_info = $output_data_info['NG_INFO'];
//$check_ng = $output_data_info['CHECK_NG'];

/*$ng_no_list = array();
foreach($ng_info as $ng_info1)
{
	foreach($ng_info1['hiyari_no'] as $ng_no)
	{
		if(!in_array($ng_no,$ng_no_list))
		{
			array_push($ng_no_list,$ng_no);
		}
	}
}

$has_report_item_ng = false;
foreach($ng_info as $ng_info1)
{
	if($ng_info1['grp_code'] != -1)
	{
		$has_report_item_ng = true;
		break;
	}
}

$has_report_item_ng = false;
foreach($ng_info as $ng_info1)
{
	if($ng_info1['grp_code'] != -1)
	{
		$has_report_item_ng = true;
		break;
	}
}*/




$title_info = get_no_title_info($hiyari_data_type,$org_item_setting_info);



$PAGE_TITLE = "ヒヤリ・ハット出力情報";
//======================================================================================================================================================
//HTML出力
//======================================================================================================================================================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$INCIDENT_TITLE?> | <?=$PAGE_TITLE?></title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">

	//リロードします。※注意：この関数は別画面から呼び出されます。
	function reload_page()
	{
		window.location.reload();
	}
	
	//レポート編集(レポートID固定/カテゴリ指定)
	function open_report_update(cate_code)
	{
		var url = "hiyari_easyinput.php?session=<?=$session?>&gamen_mode=cate_update&report_id=<?=$report_id?>&cate=" + cate_code;
		show_sub_window(url);
	}
	
	//子画面を表示
	function show_sub_window(url)
	{
		var h = window.screen.availHeight - 30;
		var w = window.screen.availWidth - 10;
		var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;
		
		window.open(url, '_blank',option);
	}
	
	//任意項目更新
	function update_free_value()
	{
		document.mainform.submit();
	}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

.list {border-collapse:collapse;}
.list td {border:#35B341 solid 1px;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#35B341 solid 0px;}
table.block_in td td {border-width:1;}

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<form name="mainform" action="hiyari_hiyari_report_ng_info.php" method="post">
<input type="hidden" name="ispostback" value="true">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="report_id" value="<?=$report_id?>">

<!-- BODY全体 START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<!-- ヘッダー START -->
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
<?
	show_hiyari_header_for_sub_window($PAGE_TITLE);
?>
</table>
<!-- ヘッダー END -->


</td></tr><tr><td>















<!-- エラーリスト START -->
<table width="1000" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
		<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
		<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
	</tr>
	<tr>
		<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
		<td bgcolor="#F5FFE5">


			<table border="0" cellspacing="0" cellpadding="2" class="list">
				<tr>
					<td width="150" align="center" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告書項目の入力エラー</font></td>
				</tr>
			</table>
			
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
				<tr>
					<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10" color="red">＊機種依存文字、固有名詞、個人情報等に対してはチェックを行いません。ご注意ください。</font></td>
				</tr>
			</table>			
<?
if($ng_info)
{
?>
			<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
				<tr>
					<td align="left" width="160" bgcolor="#DFFFDC">
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">カテゴリ</font>
					</td>
					<td align="left" width="160" bgcolor="#DFFFDC">
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">項目</font>
					</td>
					<td align="left" width="160" bgcolor="#DFFFDC">
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">エラー内容</font>
					</td>
				</tr>

<?
	foreach($item_list_data as $item_list_data1)
	{
		$g1 = $item_list_data1['grp_code'];
		$i1 = $item_list_data1['easy_item_code'];
		$cate_name = $item_list_data1['cate_name'];
		$easy_item_name = $item_list_data1['easy_item_name'];
		$cate_code = $item_list_data1['cate_code'];
		
		if($cate_code == 0)
		{
			$cate_code = 4;
		}
		
		foreach($ng_info as $ng_info1)
		{
			$g2 = $ng_info1['grp_code'];
			$i2 = $ng_info1['easy_item_code'];
			$message = $ng_info1['message'];
			$cate_name = $ng_info1['element'];
			$easy_item_name = $ng_info1['element_name'];
			if($g1 == $g2 && $i1 == $i2)
			{
?>
				<tr>
					<td align="left" bgcolor="#FFFFFF" width="200">
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
						<a href="javascript:open_report_update(<?=$cate_code?>)">
						<?=h($cate_name)?>
						</a>
						</font>
					</td>
					<td align="left" bgcolor="#FFFFFF" width="200">
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
						<?=h($easy_item_name)?>
						</font>
					</td>
					<td align="left" bgcolor="#FFFFFF">
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
						<?=h($message)?>
						</font>
					</td>
				</tr>
<?
			}
		}
	}
?>
			</table>
<?
}
else
{
?>
			<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
				<tr>
					<td align="left" width="100%" bgcolor="#FFFFFF">
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
						報告書項目に検出可能なエラーはありません。
						</font>
					</td>
				</tr>
			</table>
<?
}
?>


	<tr>
		<td><img src="img/r_3.gif" width="10" height="10"></td>
		<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
		<td><img src="img/r_4.gif" width="10" height="10"></td>
	</tr>
</table>
<!-- エラーリスト END -->





</td>
</tr>
</table>
<!-- BODY全体 END -->

</form>
</body>
</html>
<?
//==============================
//DBコネクション終了
//==============================
pg_close($con);




function get_no_title_info($hiyari_data_type,$org_item_setting_info)
{
	$no_title_width = null;
	$no_title_width[14] = 17;
	$no_title_width[34] = 28;
	$no_title_width[71] = 8;
	$no_title_width[82] = 3;
	$no_title_width[85] = 106;

//テスト確認用(発生要因詳細)
//	$no_title_width[85] = 2;
//	$no_title_width[87] = 2;
//	$no_title_width[89] = 2;
//	$no_title_width[91] = 3;
//	$no_title_width[94] = 3;
//	$no_title_width[97] = 5;
//	$no_title_width[102] = 4;
//	$no_title_width[106] = 7;
//	$no_title_width[113] = 5;
//	$no_title_width[118] = 8;
//	$no_title_width[126] = 6;
//	$no_title_width[133] = 8;
//	$no_title_width[141] = 6;
//	$no_title_width[147] = 9;
//	$no_title_width[156] = 10;
//	$no_title_width[166] = 8;
//	$no_title_width[174] = 8;
//	$no_title_width[182] = 4;
//	$no_title_width[186] = 4;

	$no_title_width[191] = 21;
	$no_title_width[217] = 3;
	$no_title_width[226] = 4;
	$no_title_width[232] = 2;
//	$no_title_width[237] = 10;
	
	$no_title = null;
	$no_title[1] = 'リザーブ';
	$no_title[2] = '施設内識別子';
	$no_title[3] = '記述区分';
	$no_title[4] = 'ファイル識別子';
	$no_title[5] = 'バージョン';
	$no_title[6] = '発生月';
	$no_title[7] = '発生曜日';
	$no_title[8] = '発生曜日区分';
	$no_title[9] = '発生時間帯';
	$no_title[10] = '発生場所';
	$no_title[11] = '発生場所その他';
	$no_title[12] = '患者性別';
	$no_title[13] = '患者年齢';
	$no_title[14] = '患者心身状態';
	$no_title[31] = '患者心身状態その他';
	$no_title[32] = '発見者';
	$no_title[33] = '発見者その他';
	$no_title[34] = '当事者職種';
	$no_title[62] = '当事者職種その他';
	$no_title[63] = '当事者職種経験(年)';
	$no_title[64] = '当事者職種経験(月)';
	$no_title[65] = '当事者部署配属(年)';
	$no_title[66] = '当事者部署配属(月)';
	$no_title[67] = '発生場面';
	$no_title[68] = '発生場面その他';
	$no_title[69] = '薬剤・製剤の種類';
	$no_title[70] = '薬剤・製剤の種類その他';
	$no_title[71] = '予備';
	$no_title[79] = 'ヒヤリハットの内容';
	$no_title[80] = 'ヒヤリハットの内容その他';
	$no_title[81] = '発生要因';

//テスト確認用(発生要因詳細)
//	$no_title[81] = '発生要因(連携)';

	$no_title[82] = '予備';
	$no_title[85] = '発生要因';

//テスト確認用(発生要因詳細)
//	$no_title[85] = '発生要因(確認)';
//	$no_title[87] = '発生要因(観察)';
//	$no_title[89] = '発生要因(判断)';
//	$no_title[91] = '発生要因(知識)';
//	$no_title[94] = '発生要因(技術)';
//	$no_title[97] = '発生要因(報告)';
//	$no_title[102] = '発生要因(身体)';
//	$no_title[106] = '発生要因(心理)';
//	$no_title[113] = '発生要因(システム)';
//	$no_title[118] = '発生要因(連携)';
//	$no_title[126] = '発生要因(記録記載)';
//	$no_title[132] = '発生要因(患者類似)';
//	$no_title[133] = '発生要因(勤務状況)';
//	$no_title[141] = '発生要因(環境)';
//	$no_title[147] = '発生要因(用具材料)';
//	$no_title[156] = '発生要因(薬剤)';
//	$no_title[166] = '発生要因(諸物品)';
//	$no_title[174] = '発生要因(施設設備)';
//	$no_title[182] = '発生要因(教育訓練)';
//	$no_title[186] = '発生要因(説明)';
//	$no_title[190] = '発生要因(他)';

	$no_title[191] = '発生要因その他';
	$no_title[212] = 'ヒヤリ・ハットの影響度';
	$no_title[213] = '備考';
	$no_title[214] = 'リザーブ';
	$no_title[215] = '医療用具の販売名';
	$no_title[216] = '医療用具の業者名';
	$no_title[217] = 'リザーブ';
	$no_title[220] = '関連した薬剤名';
	$no_title[221] = '関連した薬剤の剤型';
	$no_title[222] = '関連した薬剤の規格単位';
	$no_title[223] = '本来の薬剤名';
	$no_title[224] = '本来の薬剤の剤型';
	$no_title[225] = '本来の薬剤の規格単位';
	$no_title[226] = 'リザーブ';
	$no_title[230] = '諸物品の販売名';
	$no_title[231] = '諸物品の業者名';
	$no_title[232] = 'リザーブ';
	$no_title[234] = '具体的な内容';
	$no_title[235] = '背景・要因';
	$no_title[236] = '改善案';
	$no_title[237] = '任意項目1';
	$no_title[238] = '任意項目2';
	$no_title[239] = '任意項目3';
	$no_title[240] = '任意項目4';
	$no_title[241] = '任意項目5';
	$no_title[242] = '任意項目6';
	$no_title[243] = '任意項目7';
	$no_title[244] = '任意項目8';
	$no_title[245] = '任意項目9';
	$no_title[246] = '任意項目10';
	$no_title[247] = '関連診療科';
	$no_title[248] = '関連診療科その他';
	$no_title[249] = '内容';
	$no_title[250] = '内容その他';
	$no_title[251] = '医療用具の購入年';
	$no_title[252] = '諸物品の購入年';
	$no_title[253] = '関連した薬剤の業者名';
	$no_title[254] = '本来の薬剤の業者名';
	
	$info = null;
	$info['no_title'] = $no_title;
	$info['no_title_width'] = $no_title_width;
	$info['no_cate'] = get_no_cate_info($hiyari_data_type,$org_item_setting_info);
	return $info;
}




/**
 * ヒヤリ・ハット項目と報告書カテゴリのマッピングを返します。
 * 
 * @param integer $hiyari_data_type 出力タイプ
 * @param array $org_item_setting_info 任意項目の設定情報
 * @return array キー:ヒヤリ・ハット番号／値:カテゴリコード
 */
function get_no_cate_info($hiyari_data_type,$org_item_setting_info)
{
	$no_cate = null;
	
	if($hiyari_data_type != 3)
	{
		$no_cate[  6] = 1;
	}
	
	for($i =   7; $i <=  11; $i++){ $no_cate[$i] = 1; }
	
	if($hiyari_data_type != 3)
	{
		$no_cate[ 12] = 3;
		$no_cate[ 13] = 3;
	}
	
	for($i =  14; $i <=  31; $i++){ $no_cate[$i] = 3; }

	if($hiyari_data_type != 3)
	{
		$no_cate[ 32] = 2;
		$no_cate[ 33] = 2;
	}
	
	for($i =  34; $i <=  66; $i++){ $no_cate[$i] = 2; }
	
	$no_cate[ 67] = 4;
	
	$no_cate[ 69] = 4;
	
	if($hiyari_data_type != 3)
	{
		$no_cate[ 68] = 4;
		$no_cate[ 70] = 4;
		
		$no_cate[ 79] = 4;
		$no_cate[ 80] = 4;
		$no_cate[ 81] = 7;
		for($i =  85; $i <= 190; $i++){ $no_cate[$i] = 7; }
		for($i = 191; $i <= 211; $i++){ $no_cate[$i] = 7; }
		$no_cate[212] = 7;
		$no_cate[213] = 7;
	}
	
	if($hiyari_data_type != 1)
	{
		$no_cate[215] = 4;
		$no_cate[216] = 4;
		for($i = 220; $i <= 225; $i++){ $no_cate[$i] = 4; }
		$no_cate[230] = 4;
		$no_cate[231] = 4;
		$no_cate[234] = 5;
		$no_cate[235] = 6;
		$no_cate[236] = 6;

		for($i = 247; $i <= 254; $i++){ $no_cate[$i] = 4; }
	}
	
	//任意項目の定義
	foreach($org_item_setting_info as $org_item_setting_info1)
	{
		$org_item_no = $org_item_setting_info1["org_item_no"];
		$input_type = $org_item_setting_info1["input_type"];
		$auto_input_item = $org_item_setting_info1["auto_input_item"];
		$must_flg = $org_item_setting_info1["must_flg"];
		
		//自動設定の場合
		if($input_type == 1)
		{
			switch($auto_input_item)
			{
				case "incident_month"://発生月
					$no_cate[236 + $org_item_no] = 1;
					break;
				case "touzisha_class"://当事者所属部署
					$no_cate[236 + $org_item_no] = 2;
					break;
				case "report_class"://報告部署
					$no_cate[236 + $org_item_no] = 1;
					break;
				default:
					//カテゴリなし
					break;
			}
		}
	}
	
	return $no_cate;
}









?>

