<?
ob_start();

$fname = $PHP_SELF;

require_once('about_postgres.php');
$con = connect2db($fname);

switch ($_POST['type']) {
case '1':
	$text = apply_content_md5($con, $_POST['apply_id'], $fname);
	break;
}

pg_close($con);

ob_clean();

header('Content-Type:text/plain; charset=EUC-JP');
echo $text;

function apply_content_md5($con, $apply_id, $fname) {
	$sql = 'select apply_content from apply';
	$cond = 'where apply_id = ' . (int)$apply_id;
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0 || pg_num_rows($sel) == 0) return '';
	return md5(pg_fetch_result($sel, 0, 'apply_content'));
}
