<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>病床管理 | チェックリスト</title>
<? require("./about_session.php"); ?>
<? require("./about_authority.php"); ?>
<? require("./about_postgres.php"); ?>
<? require("./show_select_values.ini"); ?>
<? require("./get_values.ini"); ?>
<? require("./conf/sql.inf"); ?>
<? require("show_in_search.ini"); ?>
<?
//ページ名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//病床管理権限チェック
$wardreg = check_authority($session,14,$fname);
if($wardreg == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 診療科登録権限を取得
$section_admin_auth = check_authority($session, 28, $fname);

// 病棟登録権限を取得
$ward_admin_auth = check_authority($session, 21, $fname);

//DBへのコネクション作成
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 患者氏名の取得
$cond = "where ptif_id = '$pt_id' and ptif_del_flg = 'f'";
$sel = select_from_table($con,$SQL85,$cond,$fname);		//ptifmstから取得
if($sel == 0){
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$lt_kj_nm = pg_result($sel,0,"ptif_lt_kaj_nm");
$ft_kj_nm = pg_result($sel,0,"ptif_ft_kaj_nm");

// チェックリスト情報の取得
$cond = "where ptif_id = '$pt_id'";
$sel_q = select_from_table($con,$SQL125,$cond,$fname);
if($sel_q == 0){
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
if (pg_num_rows($sel_q) > 0) {
	$q1 = pg_result($sel_q,0,"inpt_q1");
	$q2 = pg_result($sel_q,0,"inpt_q2");
	$q3 = pg_result($sel_q,0,"inpt_q3");
	$q4_1 = pg_result($sel_q,0,"inpt_q4_1");
	$q4_2 = pg_result($sel_q,0,"inpt_q4_2");
	$q5 = pg_result($sel_q,0,"inpt_q5");
	$q6_1 = pg_result($sel_q,0,"inpt_q6_1");
	$q6_2 = pg_result($sel_q,0,"inpt_q6_2");
	$q7 = pg_result($sel_q,0,"inpt_q7");
	$q7_com = pg_result($sel_q,0,"inpt_q7_com");
	$q8_1 = pg_result($sel_q,0,"inpt_q8_1");
	$q8_2 = pg_result($sel_q,0,"inpt_q8_2");
}

$url_pt_nm = urlencode($pt_nm);

// 診療科情報、主治医情報を配列に格納
list($arr_sect, $arr_sectdr) = get_sectdr($con, $fname);
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function initPage() {
	sectOnChange2('<? echo($search_doc); ?>');
}

function sectOnChange2(doc) {
	var sect_id = document.search.search_sect.value;

	// 主治医セレクトボックスのオプションを全削除
	deleteAllOptions(document.search.search_doc);

	// 主治医セレクトボックスのオプションを作成
	addOption(document.search.search_doc, '0', 'すべて', doc);
<? foreach ($arr_sectdr as $sectdr) { ?>
	if (sect_id == '<? echo $sectdr["sect_id"]; ?>') {
	<? foreach($sectdr["drs"] as $dritem) { ?>
		addOption(document.search.search_doc, '<? echo $dritem["dr_id"]; ?>', '<? echo $dritem["dr_nm"]; ?>', doc);
	<? } ?>
	}
<? } ?>
}

function deleteAllOptions(box) {
	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}
}

function addOption(box, value, text, selected) {
	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	if (selected == value) {
		opt.selected = true;
	}
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="bed_menu.php?session=<? echo($session); ?>"><img src="img/icon/b17.gif" width="32" height="32" border="0" alt="病床管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="bed_menu.php?session=<? echo($session); ?>"><b>病床管理</b></a></font></td>
<? if ($section_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="entity_menu.php?session=<? echo($session); ?>&entity=1"><b>管理画面へ</b></a></font></td>
<? } else if ($ward_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="building_list.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="115" align="center" bgcolor="#5279a5"><a href="bed_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>病床管理トップ</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="ward_reference.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病棟照会</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="inpatient_reserve_register_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入退院登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="bedmake_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ベットメイク</font></a></td>
<td width="5">&nbsp;</td>
<td width="125" align="center" bgcolor="#bdd1e7"><a href="bed_rate_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床利用率管理</font></a>
</td>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#bdd1e7"><a href="bed_date_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">在院日数管理</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="bed_patient_export.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者データ抽出</font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="submenu">
<tr height="22">
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu.php?session=<? echo($session); ?>">お知らせ</a></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_status.php?session=<? echo($session); ?>">病棟状況一覧表</a></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_schedule.php?session=<? echo($session); ?>">入退院カレンダー</a></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院患者検索</font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_vacant_search.php?session=<? echo($session); ?>">空床検索</a></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_occupation.php?session=<? echo($session); ?>">病床利用状況</a></font></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<? write_search_form($pt_nm, $arr_sect, $search_sect, $session); ?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<!-- タブ2 -->
<? if ($path == "A") {  // 入院患者 ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="50" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_patient.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=A&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基本</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_sub.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=A&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基礎データ</font></a></td>
<td width="5">&nbsp;</td>
<td width="60" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_contact.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=A&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">連絡先</font></a></td>
<td width="5">&nbsp;</td>
<td width="50" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_claim.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=A&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">請求</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_detail.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=A&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院情報</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#5279a5"><a href="bed_menu_inpatient_check.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=A&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>チェックリスト</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_nutrition.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=A&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">栄養問診表</font></a></td>
<td width="5">&nbsp;</td>
<td width="170" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_exception.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=A&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">平均在院日数除外指定</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<? } else if ($path == "B") {  // 退院予定患者 ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="50" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_patient.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=B&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基本</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_sub.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=B&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基礎データ</font></a></td>
<td width="5">&nbsp;</td>
<td width="60" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_contact.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=B&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">連絡先</font></a></td>
<td width="5">&nbsp;</td>
<td width="50" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_claim.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=B&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">請求</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_detail.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=B&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院情報</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#5279a5"><a href="bed_menu_inpatient_check.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=B&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>チェックリスト</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_nutrition.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=B&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">栄養問診表</font></a></td>
<td width="5">&nbsp;</td>
<td width="170" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_exception.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=B&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">平均在院日数除外指定</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_out_reserve.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退院予定情報</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<? } ?>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<form name="inpt" action="inpatient_check_list_confirm.php?session=<? echo($session); ?>" method="post">
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="20%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者ID</font></td>
<td width="30%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($pt_id); ?></font></td>
<td width="20%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者氏名</font></td>
<td width="30%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo("$lt_kj_nm $ft_kj_nm"); ?></font></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<table width="800" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="50%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">問1）入院時書類（入院案内・契約書・食事について・ご案内）</font></td>
<td width="50%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="radio" name="q1" value="t" <? if($q1 == t){echo("checked");} ?>>渡した&nbsp;<input type="radio" name="q1" value="f" <? if($q1 == f){echo("checked");} ?>>未</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">問2）プロフィール用紙</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="radio" name="q2" value="t" <? if($q2 == t){echo("checked");} ?>>渡した&nbsp;<input type="radio" name="q2" value="f" <? if($q2 == f){echo("checked");} ?>>未</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">問3）入院案内（手続き、携帯品）</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="radio" name="q3" value="t" <? if($q3 == t){echo("checked");} ?>>説明済み&nbsp;<input type="radio" name="q3" value="f" <? if($q3 == f){echo("checked");} ?>>未</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">問4）承諾書</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="radio" name="q4_1" value="t" <? if($q4_1 == t){echo("checked");} ?>>有&nbsp;<input type="radio" name="q4_1" value="f" <? if($q4_1 == f){echo("checked");} ?>>無（<input type="radio" name="q4_2" value="t" <? if($q4_2 == t){echo("checked");} ?>>手渡し済&nbsp;<input type="radio" name="q4_2" value="f" <? if($q4_2 == f){echo("checked");} ?>>未）</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">問5）［手術・内視鏡］伝票の記入</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="radio" name="q5" value="t" <? if($q5 == t){echo("checked");} ?>>済&nbsp;<input type="radio" name="q5" value="f" <? if($q5 == f){echo("checked");} ?>>未</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">問6）麻酔科からのお願い</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="radio" name="q6_1" value="t" <? if($q6_1 == t){echo("checked");} ?>>有&nbsp;<input type="radio" name="q6_1" value="f" <? if($q6_1 == f){echo("checked");} ?>>無（<input type="radio" name="q6_2" value="t" <? if($q6_2 == t){echo("checked");} ?>>手渡し済&nbsp;<input type="radio" name="q6_2" value="f" <? if($q6_2 == f){echo("checked");} ?>>未）</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">問7）Dr.より［入院・手術・検査］の説明</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="radio" name="q7" value="t" <? if($q7 == t){echo("checked");} ?>>有（本人・家族：<input type="text" name="q7_com" value="<? echo($q7_com); ?>">）&nbsp;<input type="radio" name="q7" value="f" <? if($q7 == f){echo("checked");} ?>>無</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">問8）HIV感染検査のお願い</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="radio" name="q8_1" value="t" <? if($q8_1 == t){echo("checked");} ?>>有&nbsp;<input type="radio" name="q8_1" value="f" <? if($q8_1 == f){echo("checked");} ?>>無（<input type="radio" name="q8_2" value="t" <? if($q8_2 == t){echo("checked");} ?>>手渡し済&nbsp;<input type="radio" name="q8_2" value="f" <? if($q8_2 == f){echo("checked");} ?>>未）</font></td>
</tr>
</table>
<table width="800" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<input type="hidden" name="pt_id" value="<? echo($pt_id); ?>">
<input type="hidden" name="path" value="<? echo($path); ?>">
<input type="hidden" name="pt_nm" value="<? echo($pt_nm); ?>">
<input type="hidden" name="enti" value="1">
<input type="hidden" name="search_sect" value="<? echo($search_sect); ?>">
<input type="hidden" name="search_doc" value="<? echo($search_doc); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
