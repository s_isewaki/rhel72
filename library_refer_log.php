<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>文書管理 | 参照履歴</title>
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("referer_common.ini");
require_once("menu_common.ini");
require_once("library_common.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 32, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

function format_datetime($datetime) {
    if (strlen($datetime) == 14) {
        return preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})$/", "$1/$2/$3　$4:$5:$6", $datetime);
    } else {
        return preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $datetime);
    }
}

// 文書管理管理者権限を取得
$lib_admin_auth = check_authority($session, 63, $fname);

// デフォルト値の設定
if ($sort == "") {$sort = "1";}  // 並び順：「累積参照数の多い順」
if ($page == "") {$page = 1;}  // ページ数：1

// データベースに接続
$con = connect2db($fname);

// 遷移元の取得
$referer = get_referer($con, $session, "library", $fname);

// イントラメニュー情報を取得
$sql = "select * from intramenu";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$intra_menu2 = pg_fetch_result($sel, 0, "menu2");
$intra_menu2_3 = pg_fetch_result($sel, 0, "menu2_3");

// 使用可能な書庫一覧を取得
$available_archives = lib_available_archives($con, $fname, false);

// 最新文書一覧表示フラグを取得
$newlist_flg = lib_get_show_newlist_flg($con, $fname);
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.list {border-collapse:collapse;}
table.list td {border:#5279a5 solid 1px;}
table.list td td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="intra_menu.php?session=<? echo($session); ?>"><img src="img/icon/b38.gif" width="32" height="32" border="0" alt="イントラネット"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="intra_menu.php?session=<? echo($session); ?>"><b>イントラネット</b></a> &gt; <a href="intra_info.php?session=<? echo($session); ?>"><b><? echo($intra_menu2); ?></b></a> &gt; <a href="library_list_all.php?session=<? echo($session); ?>"><b><? echo($intra_menu2_3); ?></b></a> &gt; <a href="library_refer_log.php?session=<? echo($session); ?>"><b>参照履歴</b></a></font></td>
<? if ($lib_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="library_admin_menu.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="105" align="center" bgcolor="#bdd1e7"><a href="intra_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メインメニュー</font></a></td>
<td width="5">&nbsp;</td>
<td width="<? echo(get_tab_width($intra_menu2)); ?>" align="center" bgcolor="#bdd1e7"><a href="intra_info.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($intra_menu2); ?></font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_list_all.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文書一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文書登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_search.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文書検索</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#5279a5"><a href="library_refer_log.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>参照履歴</b></font></a></td>
<? if ($newlist_flg == 't') { ?>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_list_new.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><nobr>最新文書一覧</nobr></font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_option.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション</font></a></td>
<? } ?>
<td>&nbsp;</td>
</tr>
</table>
<? } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="library_list_all.php?session=<? echo($session); ?>"><img src="img/icon/b12.gif" width="32" height="32" border="0" alt="文書管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="library_list_all.php?session=<? echo($session); ?>"><b>文書管理</b></a> &gt; <a href="library_refer_log.php?session=<? echo($session); ?>"><b>参照履歴</b></a></font></td>
<? if ($lib_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="library_admin_menu.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_list_all.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文書一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文書登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_search.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文書検索</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#5279a5"><a href="library_refer_log.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>参照履歴</b></font></a></td>
<? if ($newlist_flg == 't') { ?>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_list_new.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><nobr>最新文書一覧</nobr></font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_option.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション</font></a></td>
<? } ?>
<td width="">&nbsp;</td>
</tr>
</table>
<? } ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="33%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_submenu_label("累積参照数の多い順", "1", $sort, $session); ?></font></td>
<td width="33%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_submenu_label("直近3ヶ月で参照数の多い順", "2", $sort, $session); ?></font></td>
<td width="33%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_submenu_label("直近1ヶ月で参照数の多い順", "3", $sort, $session); ?></font></td>
</tr>
<tr height="22">
<td width="33%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_submenu_label("累積参照数の少ない順", "4", $sort, $session); ?></font></td>
<td width="33%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_submenu_label("直近3ヶ月で参照数の少ない順", "5", $sort, $session); ?></font></td>
<td width="33%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_submenu_label("直近1ヶ月で参照数の少ない順", "6", $sort, $session); ?></font></td>
</tr>
</table>
<? show_library_list($con, $sort, $page, $available_archives, $session, $fname); ?>
</td>
</tr>
</table>
<form name="delform" action="library_delete.php" method="post">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="did[]" value="">
<input type="hidden" name="path" value="2">
<input type="hidden" name="sort" value="<? echo($sort); ?>">
<input type="hidden" name="page" value="<? echo($page); ?>">
</form>
<iframe name="download" width="0" height="0" frameborder="0"></iframe>
</body>
<? pg_close($con); ?>
</html>
<?
function show_submenu_label($label, $sort, $selected, $session) {
    if ($selected == $sort) {
        echo("<b>");
    } else {
        echo("<a href=\"library_refer_log.php?session=$session&sort=$sort\">");
    }
    echo($label);
    if ($selected == $sort) {
        echo("</b>");
    } else {
        echo("</a>");
    }
}

function show_library_list($con, $sort, $page, $available_archives, $session, $fname) {
    $limit = 15;

    // 文書数を取得（ページング処理のため）
    $sql_sel_doc = get_select_documents_sql($con, $session, $fname);
    $lib_count = lib_get_one($con, $fname, "select count(lib_id) from (select ".$sql_sel_doc.") libinfo");
    if ($lib_count == 0) return;

    if ($sort=="1" || $sort=="4") {  // 期間指定なし
        $sql = "select * from (select ".$sql_sel_doc.") libinfo";
    } else {  // 期間指定あり
        if ($sort=="2" || $sort=="5") {  // 直近3ヶ月
            $comp_time = date("YmdHi", strtotime("-3 months"));
        } else {
            $comp_time = date("YmdHi", strtotime("-1 month"));
        }
        $sql = "select libreflog.ref_count2, libinfo.* from (select ".$sql_sel_doc.") libinfo left join (select lib_id, count(*) as ref_count2 from libreflog where ref_date >= '$comp_time' group by lib_id) libreflog on libinfo.lib_id = libreflog.lib_id";
    }
    if ($sort=="1") $sql .= " order by libinfo.ref_count desc, libinfo.lib_up_date desc"; // 累積参照数の多い順
    if ($sort=="2") $sql .= " order by coalesce(libreflog.ref_count2, 0) desc, libinfo.lib_up_date desc"; // 直近3ヶ月で参照数の多い順
    if ($sort=="3") $sql .= " order by coalesce(libreflog.ref_count2, 0) desc, libinfo.lib_up_date desc"; // 直近1ヶ月で参照数の多い順
    if ($sort=="4") $sql .= " order by libinfo.ref_count, libinfo.lib_up_date"; // 累積参照数の少ない順
    if ($sort=="5") $sql .= " order by coalesce(libreflog.ref_count2, 0), libinfo.lib_up_date"; // 直近3ヶ月で参照数の少ない順
    if ($sort=="6") $sql .= " order by coalesce(libreflog.ref_count2, 0), libinfo.lib_up_date"; // 直近1ヶ月で参照数の少ない順
    $sel = select_from_table($con, $sql." offset " . ($limit * ($page - 1)) . " limit ".$limit, "", $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    // ページ番号の表示
    if ($lib_count > $limit) {
        echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"margin:5px 0 2px;\">\n");
        echo("<tr>\n");
        echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\">\n");

        if ($page > 1) {
            echo("<a href=\"$fname?session=$session&sort=$sort&page=" . ($page - 1) . "\">＜前</a>　\n");
        } else {
            echo("＜前　\n");
        }

        $page_count = ceil($lib_count / $limit);
        for ($i = 1; $i <= $page_count; $i++) {
            if ($i != $page) {
                echo("<a href=\"$fname?session=$session&sort=$sort&page=$i\">$i</a>\n");
            } else {
                echo("<b>$i</b>\n");
            }
        }

        if ($page < $page_count) {
            echo("　<a href=\"$fname?session=$session&sort=$sort&page=" . ($page + 1) . "\">次＞</a>\n");
        } else {
            echo("　次＞\n");
        }

        echo("</font></td>\n");
        echo("</tr>\n");

        echo("</table>\n");
    } else {
        echo("<img src=\"img/spacer.gif\" alt=\"\" width=\"1\" height=\"5\"><br>\n");
    }

    // 文書一覧の表示
    echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"list\">\n");
    echo("<tr height=\"22\" bgcolor=\"#f6f9ff\">\n");
    echo("<td width=\"23%\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">文書名</font></td>\n");
    echo("<td width=\"4%\" align=\"right\" style=\"padding-right:4px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><nobr>参照数</nobr></font></td>\n");
    echo("<td width=\"13%\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">更新日時</font></td>\n");
    echo("<td width=\"12%\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">登録者</font></td>\n");
    echo("<td width=\"38%\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">保存先</font></td>\n");
    echo("<td width=\"6%\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">参照</font></td>\n");
    echo("</tr>\n");
    while ($row = pg_fetch_array($sel)) {
        $tmp_lib_id = $row["lib_id"];
        $tmp_lib_nm = $row["lib_nm"];
        if ($sort == "1" || $sort == "4") {
            $tmp_ref_count =  intval($row["ref_count"]);
        } else {
            $tmp_ref_count =  intval($row["ref_count2"]);
        }
        $tmp_lib_up_date = format_datetime($row["lib_up_date"]);

		$emp_nm = lib_get_one($con, $fname, "select emp_lt_nm || ' ' || emp_ft_nm from empmst where emp_id = '".$row["emp_id"]."'");

        $tmp_lib_archive = get_lib_archive_label($row["lib_archive"]);
        $tmp_lib_cate_nm = $row["lib_cate_nm"];
        $tmp_folder_id = $row["folder_id"];
        $tmp_folder_path = lib_get_folder_path($con, $tmp_folder_id, $fname, $row["lib_archive"]);
        $tmp_lib_url = get_lib_url($row["lib_archive"], $row["lib_cate_id"], $tmp_lib_id, $row["lib_extension"], $session);

        echo("<tr height=\"22\" valign=\"top\">\n");
        echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_lib_nm</font></td>\n");
        echo("<td align=\"right\" style=\"padding-right:4px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
        echo($tmp_ref_count);
        echo("</font></td>\n");
        echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><nobr>$tmp_lib_up_date</nobr></font></td>\n");
        echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$emp_nm</font></td>\n");
        echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
        if (count($available_archives) > 1) {
            echo("$tmp_lib_archive &gt; ");
        }
        echo($tmp_lib_cate_nm);
        lib_write_folder_path($tmp_folder_path, true);
        echo("</font></td>\n");
        if (lib_get_filename_flg() > 0) {
            echo("<td align=\"center\"><input type=\"button\" value=\"参照\" onclick=\"location.href = '$tmp_lib_url';\"></td>\n");
        } else {
            echo("<td align=\"center\"><input type=\"button\" value=\"参照\" onclick=\"window.open('$tmp_lib_url');\"></td>\n");
        }
        echo("</tr>\n");
    }
    echo("</table>\n");
}

function get_lib_url($archive, $cate_id, $lib_id, $ext, $session) {
    $folders = array(
        "1" => "private",
        "2" => "all",
        "3" => "section",
        "4" => "project"
    );
    return "library_refer.php?s={$session}&i={$lib_id}&u=" . urlencode("docArchive/{$folders[$archive]}/cate{$cate_id}/document{$lib_id}.{$ext}");
}

function get_select_documents_sql($con, $session, $fname) {

    // ログインユーザの職員情報を取得
    $sql = "select emp_id, emp_class, emp_attribute, emp_dept, emp_st from empmst";
    $cond = "where emp_id = (select emp_id from session where session_id = '$session')";
    $sel_emp = select_from_table($con, $sql, $cond, $fname);
    if ($sel_emp == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $emp_id = pg_fetch_result($sel_emp, 0, "emp_id");
    $emp_class = pg_fetch_result($sel_emp, 0, "emp_class");
    $emp_atrb = pg_fetch_result($sel_emp, 0, "emp_attribute");
    $emp_dept = pg_fetch_result($sel_emp, 0, "emp_dept");
    $emp_st = pg_fetch_result($sel_emp, 0, "emp_st");

    // 参照可能な文書一覧を取得するSQLを返す
    // カテゴリとフォルダ別に権限を確認して取得するSQLをunionでつなげる
    // カテゴリ以下のファイル取得用
    $sql1 = "libinfo.*, libcate.lib_cate_nm from libinfo inner join (select * from libcate where libcate.libcate_del_flg = 'f') libcate on libinfo.lib_cate_id = libcate.lib_cate_id";
    $cond1 = "where libinfo.lib_delete_flag = 'f' ";
    $cond1 .= " and libinfo.folder_id is null ";
    // 部署、兼務所属






    $cond_info = " and (libinfo.lib_archive != '3' or (libinfo.lib_archive = '3' and (libinfo.private_flg = 'f' or (libinfo.private_flg = 't' and (libcate.lib_link_id = '$emp_class' or exists (select * from concurrent where concurrent.emp_id = '$emp_id' and to_char(concurrent.emp_class,'FM999999999999') = libcate.lib_link_id))))))";
    $cond_info .= " and (";
        $cond_info .= "(";
            $cond_info .= "(libinfo.ref_dept_flg = '1' or (libinfo.ref_dept_flg = '2' and exists (select * from librefdept where librefdept.lib_id = libinfo.lib_id and librefdept.class_id = $emp_class and librefdept.atrb_id = $emp_atrb and librefdept.dept_id = $emp_dept)))";
            $cond_info .= " and ";
            $cond_info .= "(libinfo.ref_st_flg = '1' or (libinfo.ref_st_flg = '2' and exists (select * from librefst where librefst.lib_id = libinfo.lib_id and librefst.st_id = $emp_st)))";
        $cond_info .= ")";
        // 兼務所属
        $cond_info .= " or ";
        $cond_info .= "(";
            $cond_info .= "(libinfo.ref_dept_flg = '1' or (libinfo.ref_dept_flg = '2' and exists (select * from concurrent, librefdept where  concurrent.emp_id = '$emp_id' and concurrent.emp_class = librefdept.class_id and concurrent.emp_attribute = librefdept.atrb_id and concurrent.emp_dept = librefdept.dept_id and librefdept.lib_id = libinfo.lib_id)))";
            $cond_info .= " and ";
            $cond_info .= "(libinfo.ref_st_flg = '1' or (libinfo.ref_st_flg = '2' and exists (select * from concurrent, librefst where concurrent.emp_id = '$emp_id' and librefst.st_id = concurrent.emp_st and librefst.lib_id = libinfo.lib_id)))";
        $cond_info .= ")";
        $cond_info .= " or ";
        $cond_info .= "exists (select * from librefemp where librefemp.lib_id = libinfo.lib_id and librefemp.emp_id = '$emp_id')";
        $cond_info .= " or ";
        $cond_info .= "(";
            $cond_info .= "(libinfo.upd_dept_flg = '1' or (libinfo.upd_dept_flg = '2' and exists (select * from libupddept where libupddept.lib_id = libinfo.lib_id and libupddept.class_id = $emp_class and libupddept.atrb_id = $emp_atrb and libupddept.dept_id = $emp_dept)))";
            $cond_info .= " and ";
            $cond_info .= "(libinfo.upd_st_flg = '1' or (libinfo.upd_st_flg = '2' and exists (select * from libupdst where libupdst.lib_id = libinfo.lib_id and libupdst.st_id = $emp_st)))";
        $cond_info .= ")";
        // 兼務所属
        $cond_info .= " or ";
        $cond_info .= "(";
            $cond_info .= "(libinfo.upd_dept_flg = '1' or (libinfo.upd_dept_flg = '2' and exists (select * from concurrent, libupddept where  concurrent.emp_id = '$emp_id' and concurrent.emp_class = libupddept.class_id and concurrent.emp_attribute = libupddept.atrb_id and concurrent.emp_dept = libupddept.dept_id and libupddept.lib_id = libinfo.lib_id)))";
            $cond_info .= " and ";
            $cond_info .= "(libinfo.upd_st_flg = '1' or (libinfo.upd_st_flg = '2' and exists (select * from concurrent, libupdst where concurrent.emp_id = '$emp_id' and libupdst.st_id = concurrent.emp_st and libupdst.lib_id = libinfo.lib_id)))";
        $cond_info .= ")";
        $cond_info .= " or ";
        $cond_info .= "exists (select * from libupdemp where libupdemp.lib_id = libinfo.lib_id and libupdemp.emp_id = '$emp_id')";
        $cond_info .= " or ";
        $cond_info .= " (libinfo.lib_archive = '1' and libcate.lib_link_id = '$emp_id')";
        $cond_info .= " or ";
        $cond_info .= " (libinfo.lib_archive = '4' and libinfo.private_flg = 't' and (exists (select * from project where project.pjt_response = '$emp_id' and project.pjt_delete_flag = 'f' and to_char(project.pjt_id,'FM999999999999') = libcate.lib_link_id) or exists (select * from promember where promember.emp_id = '$emp_id' and exists (select * from project where project.pjt_delete_flag = 'f' and to_char(project.pjt_id,'FM999999999999') = libcate.lib_link_id) and to_char(promember.pjt_id,'FM999999999999') = libcate.lib_link_id)))";
    $cond_info .= ") and exists (select * from libedition where exists (select * from (select libedition.base_lib_id, max(libedition.edition_no) as edition_no from libedition group by libedition.base_lib_id) latest_edition where latest_edition.base_lib_id = libedition.base_lib_id and latest_edition.edition_no = libedition.edition_no) and libedition.lib_id = libinfo.lib_id)";







    // カテゴリの権限
    $cond_cate = " and ((libcate.lib_archive != '3' or (libcate.lib_archive = '3' and (exists (select * from classmst where class_del_flg = 'f' and to_char(classmst.class_id,'FM999999999999') = libcate.lib_link_id) ";
    $cond_cate .= " and (libcate.private_flg = 'f' or (libcate.private_flg = 't' and (libcate.lib_link_id = '$emp_class' or exists (select * from concurrent where concurrent.emp_id = '$emp_id' and to_char(concurrent.emp_class,'FM999999999999') = libcate.lib_link_id) ))))))";
    $cond_cate .= " and ";
    $cond_cate .= " (";
        // 参照権限
        $cond_cate .= " (";
            $cond_cate .= " (libcate.ref_dept_flg = '1' or (libcate.ref_dept_flg = '2' and exists (select * from libcaterefdept where libcaterefdept.lib_cate_id = libcate.lib_cate_id and libcaterefdept.class_id = $emp_class and libcaterefdept.atrb_id = $emp_atrb and libcaterefdept.dept_id = $emp_dept)))";
            $cond_cate .= " and ";
            $cond_cate .= " (libcate.ref_st_flg = '1' or (libcate.ref_st_flg = '2' and exists (select * from libcaterefst where libcaterefst.lib_cate_id = libcate.lib_cate_id and libcaterefst.st_id = $emp_st)))";
        $cond_cate .= " )";
        // 兼務所属
//TODO 対処済み
        $cond_cate .= " or ";
        $cond_cate .= " (";
            $cond_cate .= " (libcate.ref_dept_flg = '1' or (libcate.ref_dept_flg = '2' and exists (select * from concurrent, libcaterefdept where  concurrent.emp_id = '$emp_id' and concurrent.emp_class = libcaterefdept.class_id and concurrent.emp_attribute = libcaterefdept.atrb_id and concurrent.emp_dept = libcaterefdept.dept_id and libcaterefdept.lib_cate_id = libcate.lib_cate_id)))";
            $cond_cate .= " and ";
            $cond_cate .= " (libcate.ref_st_flg = '1' or (libcate.ref_st_flg = '2' and exists (select * from concurrent, libcaterefst where concurrent.emp_id = '$emp_id' and libcaterefst.st_id = concurrent.emp_st and libcaterefst.lib_cate_id = libcate.lib_cate_id)))";
        $cond_cate .= " )";
        // 職員
        $cond_cate .= " or exists (select * from libcaterefemp where libcaterefemp.lib_cate_id = libcate.lib_cate_id and libcaterefemp.emp_id = '$emp_id')";
        // 更新権限
        $cond_cate .= " or (";
            $cond_cate .= " (libcate.upd_dept_flg = '1' or (libcate.upd_dept_flg = '2' and exists (select * from libcateupddept where libcateupddept.lib_cate_id = libcate.lib_cate_id and libcateupddept.class_id = $emp_class and libcateupddept.atrb_id = $emp_atrb and libcateupddept.dept_id = $emp_dept)))";
            $cond_cate .= " and ";
            $cond_cate .= " (libcate.upd_st_flg = '1' or (libcate.upd_st_flg = '2' and exists (select * from libcateupdst where libcateupdst.lib_cate_id = libcate.lib_cate_id and libcateupdst.st_id = $emp_st)))";
        $cond_cate .= " )";
        // 兼務所属
        $cond_cate .= " or ";
        $cond_cate .= " (";
            $cond_cate .= " (libcate.upd_dept_flg = '1' or (libcate.upd_dept_flg = '2' and exists (select * from concurrent, libcateupddept where  concurrent.emp_id = '$emp_id' and concurrent.emp_class = libcateupddept.class_id and concurrent.emp_attribute = libcateupddept.atrb_id and concurrent.emp_dept = libcateupddept.dept_id and libcateupddept.lib_cate_id = libcate.lib_cate_id)))";
            $cond_cate .= " and ";
            $cond_cate .= " (libcate.upd_st_flg = '1' or (libcate.upd_st_flg = '2' and exists (select * from concurrent, libcateupdst where concurrent.emp_id = '$emp_id' and libcateupdst.st_id = concurrent.emp_st and libcateupdst.lib_cate_id = libcate.lib_cate_id)))";
        $cond_cate .= " )";
        // 職員
        $cond_cate .= " or exists (select * from libcateupdemp where libcateupdemp.lib_cate_id = libcate.lib_cate_id and libcateupdemp.emp_id = '$emp_id')";
        // 個人
        $cond_cate .= " or ";
        $cond_cate .= " (libcate.lib_archive = '1' and libcate.lib_link_id = '$emp_id')";
    $cond_cate .= " )";
    // 委員会
    $cond_cate .= " and (libcate.lib_archive != '4' or (libcate.lib_archive = '4' and (exists (select * from project where pjt_delete_flag = 'f' and to_char(project.pjt_id,'FM999999999999') = libcate.lib_link_id) and (libcate.private_flg = 'f' or (libcate.private_flg = 't' and (exists (select * from project where project.pjt_response = '$emp_id') or exists (select * from promember where promember.emp_id = '$emp_id') ))) or exists (select * from libcateupdemp where libcateupdemp.lib_cate_id = libcate.lib_cate_id and libcateupdemp.emp_id = '$emp_id'))))";

    $cond_cate .= " ) ";






// フォルダ以下のファイル取得用
    $sql2 = " select libinfo.*, libcate.lib_cate_nm from libinfo inner join (select * from libcate where libcate.libcate_del_flg = 'f') libcate on libinfo.lib_cate_id = libcate.lib_cate_id";
    $sql2 .= " left join libfolder on libinfo.folder_id = libfolder.folder_id ";
    $cond2 = "where libinfo.lib_delete_flag = 'f' ";
    $cond2 .= " and libinfo.folder_id is not null ";







    // フォルダの権限
    $cond_folder = " and (libfolder.lib_archive != '3' or (libfolder.lib_archive = '3' and (libfolder.private_flg = 'f' or (libfolder.private_flg = 't' and (libcate.lib_link_id = '$emp_class' or exists (select * from concurrent where concurrent.emp_id = '$emp_id' and to_char(concurrent.emp_class,'FM999999999999') = libcate.lib_link_id) ))))) ";
    $cond_folder .= " and (";
        // 参照権限
        $cond_folder .= " ( ";
            $cond_folder .= " (libfolder.ref_dept_flg = '1' or (libfolder.ref_dept_flg = '2' and exists (select * from libfolderrefdept where libfolderrefdept.folder_id = libfolder.folder_id and libfolderrefdept.class_id = $emp_class and libfolderrefdept.atrb_id = $emp_atrb and libfolderrefdept.dept_id = $emp_dept))) ";
            $cond_folder .= " and ";
            $cond_folder .= " (libfolder.ref_st_flg = '1' or (libfolder.ref_st_flg = '2' and exists (select * from libfolderrefst where libfolderrefst.folder_id = libfolder.folder_id and libfolderrefst.st_id = $emp_st))) ";
        $cond_folder .= " )";
        // 兼務所属
        $cond_folder .= " or ";
        $cond_folder .= " (";
            $cond_folder .= " (libfolder.ref_dept_flg = '1' or (libfolder.ref_dept_flg = '2' and exists (select * from concurrent, libfolderrefdept where  concurrent.emp_id = '$emp_id' and concurrent.emp_class = libfolderrefdept.class_id and concurrent.emp_attribute = libfolderrefdept.atrb_id and concurrent.emp_dept = libfolderrefdept.dept_id and libfolderrefdept.folder_id = libfolder.folder_id))) ";
            $cond_folder .= " and ";
            $cond_folder .= " (libfolder.ref_st_flg = '1' or (libfolder.ref_st_flg = '2' and exists (select * from concurrent, libfolderrefst where concurrent.emp_id = '$emp_id' and libfolderrefst.st_id = concurrent.emp_st and libfolderrefst.folder_id = libfolder.folder_id))) ";
        $cond_folder .= " )";
        // 職員
        $cond_folder .= " or exists (select * from libfolderrefemp where libfolderrefemp.folder_id = libfolder.folder_id and libfolderrefemp.emp_id = '$emp_id') ";
        // 更新権限
        $cond_folder .= " or ";
        $cond_folder .= " (";
            $cond_folder .= "  (libfolder.upd_dept_flg = '1' or (libfolder.upd_dept_flg = '2' and exists (select * from libfolderupddept where libfolderupddept.folder_id = libfolder.folder_id and libfolderupddept.class_id = $emp_class and libfolderupddept.atrb_id = $emp_atrb and libfolderupddept.dept_id = $emp_dept))) ";
            $cond_folder .= " and ";
            $cond_folder .= " (libfolder.upd_st_flg = '1' or (libfolder.upd_st_flg = '2' and exists (select * from libfolderupdst where libfolderupdst.folder_id = libfolder.folder_id and libfolderupdst.st_id = $emp_st))) ";
        $cond_folder .= " )";
        // 兼務所属
        $cond_folder .= " or ";
        $cond_folder .= " (";
            $cond_folder .= " (libfolder.upd_dept_flg = '1' or (libfolder.upd_dept_flg = '2' and exists (select * from concurrent, libfolderupddept where  concurrent.emp_id = '$emp_id' and concurrent.emp_class = libfolderupddept.class_id and concurrent.emp_attribute = libfolderupddept.atrb_id and concurrent.emp_dept = libfolderupddept.dept_id and libfolderupddept.folder_id = libfolder.folder_id))) ";
            $cond_folder .= " and ";
            $cond_folder .= " (libfolder.upd_st_flg = '1' or (libfolder.upd_st_flg = '2' and exists (select * from concurrent, libfolderupdst where concurrent.emp_id = '$emp_id' and libfolderupdst.st_id = concurrent.emp_st and libfolderupdst.folder_id = libfolder.folder_id))) ";
        $cond_folder .= " )";
        // 職員
        $cond_folder .= " or exists (select * from libfolderupdemp where libfolderupdemp.folder_id = libfolder.folder_id and libfolderupdemp.emp_id = '$emp_id') ";
        // 個人
        $cond_folder .= " or ";
        $cond_folder .= " (libfolder.lib_archive = '1' and libcate.lib_link_id = '$emp_id')";
    $cond_folder .= " )";
    $cond_folder .= " and (libfolder.lib_archive != '4' or (libfolder.lib_archive = '4' and (libfolder.private_flg = 'f' or (libfolder.private_flg = 't' and (exists (select * from project where project.pjt_response = '$emp_id' and project.pjt_delete_flag = 'f' and to_char(project.pjt_id,'FM999999999999') = libcate.lib_link_id) or exists (select * from promember where promember.emp_id = '$emp_id' and exists (select * from project where project.pjt_delete_flag = 'f' and to_char(project.pjt_id,'FM999999999999') = libcate.lib_link_id) and to_char(promember.pjt_id,'FM999999999999') = libcate.lib_link_id))) or exists (select * from libfolderupdemp where libfolderupdemp.folder_id = libfolder.folder_id and libfolderupdemp.emp_id = '$emp_id'))))";

    return "$sql1 $cond1 $cond_info $cond_cate union $sql2 $cond2 $cond_info $cond_folder ";
}
?>
