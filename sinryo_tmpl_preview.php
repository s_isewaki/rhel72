<? $style2 = @$_REQUEST["style2"]; ?>
<? $session = @$_REQUEST["session"]; ?>
<? $filename = @$_REQUEST["filename"]; ?>


<? if ($style2){ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<? } else { ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<script type="text/javascript" src="js/fontsize.js"></script>
<style type="text/css">

.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}

table.block {border-collapse:collapse;}
table.block td {border:#5279a5 solid 1px;padding:1px;}
table.block td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#5279a5 solid 0px;}


table.block2 {border-collapse:collapse;}
table.block2 td {border:#5279a5 solid 1px;padding:1px;}
table.block2 td {border-width:1;}

table.block3 {border-collapse:collapse;}
table.block3 td {border:#5279a5 solid 0px;}
table.block3 td {border-width:0;}

p {margin:0;}
</style>
<? } ?>

<script type="text/javascript">
	function resizeIFrame(){}
	function adjustTextAreaHeight(){}
</script>


<?
require("about_session.php");
require("about_authority.php");
require("about_postgres.php");
require("show_select_values.ini");
require_once("summary_euc_special_char.ini");
require("label_by_profile_type.ini");
require_once("summary_tmpl_common.ini");
require("get_values.ini");
require_once("get_values_for_template.ini");
require_once("get_menu_label.ini");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$summary = check_authority($session,59,$fname);
if($summary == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
?>

<? $con = connect2db($fname);//ＤＢのコネクション作成 ?>
<? $profile_type = get_profile_type($con, $fname);// 組織タイプを取得 ?>
<? $med_report_title = get_report_menu_label($con, $fname); // メドレポート/ＰＯレポート(Problem Oriented) ?>


<title>CoMedix <? echo($med_report_title); ?>管理｜テンプレートプレビュー</title>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>

<? // テンプレート読み込み ?>
<? $tmplfilename = "workflow/tmp/{$session}_{$filename}"; ?>
<? if (is_file($tmplfilename)) require($tmplfilename); ?>

<? // カレンダー用JS出力処理 ?>
<? if(defined('TEMPLATE_CALENDAR_COUNT')) set_calendar_js(TEMPLATE_CALENDAR_COUNT); ?>

<? if(@$sansho_flg == false) { ?>
	<script type="text/javascript">
	set_window_size();
	function set_window_size() {
	<? if($window_size == 1) { ?>
		var w = 600;
		var h = 760;
		var l = 500;
		var t = 100;
	<? } else { ?>
		var h = window.screen.availHeight;
		var w = window.screen.availWidth;
		var l = 0;
		var t = 0;
	<? } ?>
		//1回だけではうまくいかないケースあり。
		window.moveTo(l, t);
		window.resizeTo(w, h);
		window.moveTo(l, t);
		window.resizeTo(w, h);
	}
	</script>
<? } ?>

</head>
<body style="padding:8px; margin:0; background-color:#ffc; background-image:url(img/tmpl_white_header.gif); background-repeat:repeat-x;"
 onload="
if(window.load_action){load_action();};
loadResizingTextArea();
<? if(defined('TEMPLATE_CALENDAR_COUNT')){ ?>initcal();<? } ?>
<? if(@$iframe != ""){ ?>
	if(window.parent.document.all('<?=$iframe ?>')) {
		var pfrm = window.parent.document.all('<?=$iframe ?>');
		var h = pfrm.contentWindow.document.body.scrollHeight;
		if(h < pfrm.contentWindow.document.body.offsetHeight) {
			h = pfrm.contentWindow.document.body.offsetHeight;
		}
	}
<? } ?>
">

<? //======================================== ?>
<? //閉じるヘッダーを表示                     ?>
<? //======================================== ?>
<? if(@$sansho_flg == false){ ?>

	<div style="border-bottom:1px solid #2e79be; border-top:1px solid #8ac8fb; margin-bottom:16px; <?=@$direct_registration ? "display:none" : ""?>">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr bgcolor="#4baaf8">
	<? if ($style2){ ?>
	<? } else { ?>
	<? } ?>
	<? if ($style2){ ?>
	<td style="font-size:14px; color:#fff; padding:4px; letter-spacing:1px"><b>テンプレートプレビュー</b></td>
	<? } else { ?>
	<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>テンプレートプレビュー</b></font></td>
	<? } ?>
	<td width="32" align="center" style="padding:2px;"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
	</tr>
	</table>
	</div>
<? } ?>



<? tmpl_page_out_of_tmplform_html($session); ?>

<form action="" method="post" name="tmplform" onsubmit="return false;">
<?
// テンプレート出力
if (is_file($tmplfilename)) {
	$tmpl_param = null;
	$tmpl_param['mode'] = 'preview';
	$tmpl_param['session'] = $session;
	$tmpl_param['login_emp_id'] = @$emp_id;
	$tmpl_param['pt_id'] = null;
	$tmpl_param['summary_id'] = null;
	write_tmpl_page("", $con, $fname, $tmpl_param);
}
?>
</form>
</body>
</html>
