<?

require_once("Cmx.php");
require_once("aclg_set.php");
require_once("about_comedix.php");

// ページ名
$fname = $PHP_SELF;

// セッションチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 組織登録権限チェック
$checkauth = check_authority($session, 23, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"], $_POST, $con);

// トランザクションの開始
pg_query($con, "begin transaction");

// 削除対象の文書カテゴリID
$cate_ids = array();

// チェックされた項目をループ
if (is_array($trashbox)) {
    foreach ($trashbox as $id) {

        // 部門の場合
        if (preg_match("/^(\d*)$/", $id, $matches) == 1) {
            $tmp_class_id = $matches[1];

            //部署の中にファイルが入っているか確認
            $sql = "select count(*) from libcate,libinfo,classmst";
            $cond = "where libcate.lib_archive = '3' and libcate.lib_cate_id = libinfo.lib_cate_id and libcate.lib_archive = libinfo.lib_archive";
            $cond .= " and to_number(libcate.lib_link_id,'000') = class_id and class_id = " . $tmp_class_id;
            $sel_lib = select_from_table($con, $sql, $cond, $fname);
            if ($sel_lib === '0') {
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
            echo pg_fetch_result($sel_lib, 0, "count");
            if (pg_fetch_result($sel_lib, 0, "count") != '0' and $del_flg != "t") {
                $sql = "select class_nm from classmst";
                $cond = "where class_id = $tmp_class_id";
                $sel = select_from_table($con, $sql, $cond, $fname);
                if ($sel == 0) {
                    pg_query($con, "rollback");
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }
                $tmp_class_nm = pg_fetch_result($sel, 0, "class_nm");

                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\">alert('「{$tmp_class_nm}」はファイルが存在するため削除できません');</script>");
                echo("<script type=\"text/javascript\">history.back();</script>");
                exit;
            }

            // 職員存在チェック
            $sql = "select count(*) from empmst";
            $cond = "where ((emp_class = $tmp_class_id) or (exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_class = $tmp_class_id))) and exists (select * from authmst where authmst.emp_id = empmst.emp_id and authmst.emp_del_flg = 'f')";
            $sel = select_from_table($con, $sql, $cond, $fname);
            if ($sel == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
            $tmp_emp_count = pg_fetch_result($sel, 0, 0);
            if ($tmp_emp_count > 0) {
                $sql = "select class_nm from classmst";
                $cond = "where class_id = $tmp_class_id";
                $sel = select_from_table($con, $sql, $cond, $fname);
                if ($sel == 0) {
                    pg_query($con, "rollback");
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }
                $tmp_class_nm = pg_fetch_result($sel, 0, "class_nm");

                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\">alert('「{$tmp_class_nm}」は職員情報が存在するため削除できません。');</script>");
                echo("<script type=\"text/javascript\">history.back();</script>");
                exit;
            }

            // 部門を論理削除
            $sql = "update classmst set";
            $set = array("class_del_flg", 'fantol_del_flg');
            $setvalue = array("t", 't');
            $cond = "where class_id = '$tmp_class_id'";
            $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
            if ($upd == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }

            // 課を論理削除
            $sql = "update atrbmst set";
            $set = array("atrb_del_flg", 'fantol_del_flg');
            $setvalue = array("t", 't');
            $cond = "where class_id = '$tmp_class_id'";
            $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
            if ($upd == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }

            // 科を論理削除
            $sql = "update deptmst set";
            $set = array("dept_del_flg", 'fantol_del_flg');
            $setvalue = array("t", 't');
            $cond = "where atrb_id in (select atrb_id from atrbmst where class_id = '$tmp_class_id')";
            $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
            if ($upd == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }

            // 室を論理削除
            $sql = "update classroom set";
            $set = array("room_del_flg", 'fantol_del_flg');
            $setvalue = array("t", 't');
            $cond = "where dept_id in (select dept_id from deptmst where atrb_id in (select atrb_id from atrbmst where class_id = '$tmp_class_id'))";
            $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
            if ($upd == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }

            // 勤務シフトとの同期設定を削除
            $sql = "delete from duty_shift_sync_link";
            $cond = "where class_id = $tmp_class_id";
            $del = delete_from_table($con, $sql, $cond, $fname);
            if ($del == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }

            // 文書カテゴリIDを取得
            $sql = "select lib_cate_id from libcate";
            $cond = "where lib_archive = '3' and lib_link_id = '$tmp_class_id'";
            $sel = select_from_table($con, $sql, $cond, $fname);
            if ($sel == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
            $tmp_cate_id = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, 0) : "";

            if ($tmp_cate_id != "") {
                $cate_ids[] = $tmp_cate_id;

                // ワークフローの保存先として使われていたら削除不可とする
                $sql = "select count(*) from wkfwmst";
                $cond = "where lib_archive = '3' and lib_cate_id = $tmp_cate_id and wkfw_del_flg = 'f'";
                $sel = select_from_table($con, $sql, $cond, $fname);
                if ($sel == 0) {
                    pg_query($con, "rollback");
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }
                if (intval(pg_fetch_result($sel, 0, 0)) > 0) {
                    $sql = "select class_nm from classmst";
                    $cond = "where class_id = $tmp_class_id";
                    $sel = select_from_table($con, $sql, $cond, $fname);
                    if ($sel == 0) {
                        pg_query($con, "rollback");
                        pg_close($con);
                        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                        exit;
                    }
                    $tmp_class_nm = pg_fetch_result($sel, 0, "class_nm");

                    pg_query($con, "rollback");
                    pg_close($con);
                    echo("<script type=\"text/javascript\">alert('「{$tmp_class_nm}」はワークフローの文書保存先として指定されているため削除できません。');</script>");
                    echo("<script type=\"text/javascript\">history.back();</script>");
                    exit;
                }

                // 文書カテゴリ情報を削除
                $sql = "delete from libcate";
                $cond = "where lib_cate_id = '$tmp_cate_id'";
                $del = delete_from_table($con, $sql, $cond, $fname);
                if ($del == 0) {
                    pg_query($con, "rollback");
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }

                // 文書ツリー情報を削除
                $sql = "delete from libtree";
                $cond = "where parent_id in (select folder_id from libfolder where lib_cate_id = '$tmp_cate_id') or child_id in (select folder_id from libfolder where lib_cate_id = '$tmp_cate_id')";
                $del = delete_from_table($con, $sql, $cond, $fname);
                if ($del == 0) {
                    pg_query($con, "rollback");
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }

                // 文書フォルダ情報を削除
                $sql = "delete from libfolder";
                $cond = "where lib_cate_id = '$tmp_cate_id'";
                $del = delete_from_table($con, $sql, $cond, $fname);
                if ($del == 0) {
                    pg_query($con, "rollback");
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }

                // 文書参照ログを削除
                $sql = "delete from libreflog";
                $cond = "where lib_id in (select lib_id from libinfo where lib_cate_id = $tmp_cate_id)";
                $del = delete_from_table($con, $sql, $cond, $fname);
                if ($del == 0) {
                    pg_query($con, "rollback");
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }

                // 文書情報を削除
                $sql = "delete from libinfo";
                $cond = "where lib_cate_id = '$tmp_cate_id'";
                $del = delete_from_table($con, $sql, $cond, $fname);
                if ($upd == 0) {
                    pg_query($con, "rollback");
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }
            }
        }
        // 課の場合
        else if (preg_match("/^(\d*)-(\d*)$/", $id, $matches) == 1) {
            $tmp_class_id = $matches[1];
            $tmp_atrb_id = $matches[2];

            // 職員存在チェック
            $sql = "select count(*) from empmst";
            $cond = "where ((emp_attribute = $tmp_atrb_id) or (exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_attribute = $tmp_atrb_id))) and exists (select * from authmst where authmst.emp_id = empmst.emp_id and authmst.emp_del_flg = 'f')";
            $sel = select_from_table($con, $sql, $cond, $fname);
            if ($sel == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
            $tmp_emp_count = pg_fetch_result($sel, 0, 0);
            if ($tmp_emp_count > 0) {
                $sql = "select atrb_nm from atrbmst";
                $cond = "where atrb_id = $tmp_atrb_id";
                $sel = select_from_table($con, $sql, $cond, $fname);
                if ($sel == 0) {
                    pg_query($con, "rollback");
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }
                $tmp_atrb_nm = pg_fetch_result($sel, 0, "atrb_nm");

                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\">alert('「{$tmp_atrb_nm}」は職員情報が存在するため削除できません。');</script>");
                echo("<script type=\"text/javascript\">history.back();</script>");
                exit;
            }

            // 課を論理削除
            $sql = "update atrbmst set";
            $set = array("atrb_del_flg");
            $setvalue = array("t");
            $cond = "where atrb_id = '$tmp_atrb_id'";
            $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
            if ($upd == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }

            // 科を論理削除
            $sql = "update deptmst set";
            $set = array("dept_del_flg");
            $setvalue = array("t");
            $cond = "where atrb_id = '$tmp_atrb_id'";
            $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
            if ($upd == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }

            // 室を論理削除
            $sql = "update classroom set";
            $set = array("room_del_flg");
            $setvalue = array("t");
            $cond = "where dept_id in (select dept_id from deptmst where atrb_id = '$tmp_atrb_id')";
            $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
            if ($upd == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }

            // 勤務シフトとの同期設定を削除
            $sql = "delete from duty_shift_sync_link";
            $cond = "where atrb_id = $tmp_atrb_id";
            $del = delete_from_table($con, $sql, $cond, $fname);
            if ($del == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
        }
        // 科の場合
        else if (preg_match("/^(\d*)-(\d*)-(\d*)$/", $id, $matches) == 1) {
            $tmp_atrb_id = $matches[2];
            $tmp_dept_id = $matches[3];

            // 職員存在チェック
            $sql = "select count(*) from empmst";
            $cond = "where ((emp_dept = $tmp_dept_id) or (exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_dept = $tmp_dept_id))) and exists (select * from authmst where authmst.emp_id = empmst.emp_id and authmst.emp_del_flg = 'f')";
            $sel = select_from_table($con, $sql, $cond, $fname);
            if ($sel == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
            $tmp_emp_count = pg_fetch_result($sel, 0, 0);
            if ($tmp_emp_count > 0) {
                $sql = "select dept_nm from deptmst";
                $cond = "where dept_id = $tmp_dept_id";
                $sel = select_from_table($con, $sql, $cond, $fname);
                if ($sel == 0) {
                    pg_query($con, "rollback");
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }
                $tmp_dept_nm = pg_fetch_result($sel, 0, "dept_nm");

                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\">alert('「{$tmp_dept_nm}」は職員情報が存在するため削除できません。');</script>");
                echo("<script type=\"text/javascript\">history.back();</script>");
                exit;
            }

            // 科を論理削除
            $sql = "update deptmst set";
            $set = array("dept_del_flg");
            $setvalue = array("t");
            $cond = "where dept_id = '$tmp_dept_id'";
            $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
            if ($upd == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }

            // 室を論理削除
            $sql = "update classroom set";
            $set = array("room_del_flg");
            $setvalue = array("t");
            $cond = "where dept_id = '$tmp_dept_id'";
            $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
            if ($upd == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }

            // 勤務シフトとの同期設定を削除
            $sql = "delete from duty_shift_sync_link";
            $cond = "where dept_id = $tmp_dept_id";
            $del = delete_from_table($con, $sql, $cond, $fname);
            if ($del == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
        }
        // 室の場合
        else if (preg_match("/^(\d*)-(\d*)-(\d*)-(\d*)$/", $id, $matches) == 1) {
            $tmp_dept_id = $matches[3];
            $tmp_room_id = $matches[4];

            // 職員情報の所属室をnullに更新
            $sql = "update empmst set";
            $set = array("emp_room");
            $setvalue = array(null);
            $cond = "where emp_room = '$tmp_room_id'";
            $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
            if ($upd == 0) {
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }

            // 室を論理削除
            $sql = "update classroom set";
            $set = array("room_del_flg");
            $setvalue = array("t");
            $cond = "where room_id = '$tmp_room_id'";
            $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
            if ($upd == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }

            // 勤務シフトとの同期設定を削除
            $sql = "delete from duty_shift_sync_link";
            $cond = "where room_id = $tmp_room_id";
            $del = delete_from_table($con, $sql, $cond, $fname);
            if ($del == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
        }
    }
}

// トランザクションのコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 文書を削除
$cwd = getcwd();
foreach ($cate_ids as $tmp_cate_id) {
    exec("rm -fr $cwd/docArchive/section/cate$tmp_cate_id");
}

// 組織一覧画面を再表示
echo("<script type=\"text/javascript\">location.href = 'class_menu.php?session=$session';</script>");