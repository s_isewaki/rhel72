<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("hiyari_el_common.ini");
require_once("hiyari_common.ini");

//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$auth_id = ($path != "3") ? 47 : 48;
if (check_authority($session, $auth_id, $fname) == "0")
{
	showLoginPage();
	exit;
}

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}



//==============================
// 第1版のコンテンツIDを取得
//==============================
$sql = "select base_lib_id from el_edition";
$cond = "where lib_id = $lib_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0)
{
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$base_lib_id = pg_fetch_result($sel, 0, "base_lib_id");



// ユーザ画面から呼ばれた場合
if ($path != "3")
{
	//==============================
	// ログインユーザの職員情報を取得
	//==============================
	$sql = "select emp_id, emp_class, emp_attribute, emp_dept, emp_st from empmst";
	$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0)
	{
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$emp_id = pg_fetch_result($sel, 0, "emp_id");
	$emp_class = pg_fetch_result($sel, 0, "emp_class");
	$emp_atrb = pg_fetch_result($sel, 0, "emp_attribute");
	$emp_dept = pg_fetch_result($sel, 0, "emp_dept");
	$emp_st = pg_fetch_result($sel, 0, "emp_st");

	//==============================
	// 参照可能分のコンテンツ履歴を取得
	//==============================
	
	$sql = "select el_info.lib_id, el_info.lib_up_date, el_info.lib_archive, el_info.lib_cate_id, el_info.lib_extension, (select el_edition.edition_no from el_edition where el_edition.lib_id = el_info.lib_id) as edition_no from el_info inner join (select * from el_cate where el_cate.libcate_del_flg = 'f') el_cate on el_info.lib_cate_id = el_cate.lib_cate_id";
	$cond = "where el_info.lib_delete_flag = 'f'";
	
	//参照・更新権限判定
	$cond .= " and";
	$cond .= " (";
		// 参照：所属
		$cond .= " (";
			$cond .= "(el_info.ref_dept_flg = '1' or (el_info.ref_dept_flg = '2' and exists (select * from el_ref_dept where el_ref_dept.lib_id = el_info.lib_id and el_ref_dept.class_id = $emp_class and el_ref_dept.atrb_id = $emp_atrb and el_ref_dept.dept_id = $emp_dept)))";
			$cond .= " and ";
			$cond .= "(el_info.ref_st_flg = '1' or (el_info.ref_st_flg = '2' and exists (select * from el_ref_st where el_ref_st.lib_id = el_info.lib_id and el_ref_st.st_id = $emp_st)))";
		$cond .= " )";
		// 参照：兼務所属
		$cond .= " or ";
		$cond .= "(";
			$cond .= "(el_info.ref_dept_flg = '1' or (el_info.ref_dept_flg = '2' and exists (select * from concurrent, el_ref_dept where  concurrent.emp_id = '$emp_id' and concurrent.emp_class = el_ref_dept.class_id and concurrent.emp_attribute = el_ref_dept.atrb_id and concurrent.emp_dept = el_ref_dept.dept_id and el_ref_dept.lib_id = el_info.lib_id)))";
			$cond .= " and ";
			$cond .= "(el_info.ref_st_flg = '1' or (el_info.ref_st_flg = '2' and exists (select * from concurrent, el_ref_st where concurrent.emp_id = '$emp_id' and el_ref_st.st_id = concurrent.emp_st and el_ref_st.lib_id = el_info.lib_id)))";
		$cond .= ")";
		// 参照：職員
		$cond .= " or";
		$cond .= " exists (select * from el_ref_emp where el_ref_emp.lib_id = el_info.lib_id and el_ref_emp.emp_id = '$emp_id')";
		// 更新：所属
		$cond .= " or ";
		$cond .= " (";
			$cond .= "(el_info.upd_dept_flg = '1' or (el_info.upd_dept_flg = '2' and exists (select * from el_upd_dept where el_upd_dept.lib_id = el_info.lib_id and el_upd_dept.class_id = $emp_class and el_upd_dept.atrb_id = $emp_atrb and el_upd_dept.dept_id = $emp_dept)))";
			$cond .= " and ";
			$cond .= "(el_info.upd_st_flg = '1' or (el_info.upd_st_flg = '2' and exists (select * from el_upd_st where el_upd_st.lib_id = el_info.lib_id and el_upd_st.st_id = $emp_st)))";
		$cond .= " )";
		// 更新：兼務所属
		$cond .= " or ";
		$cond .= "(";
			$cond .= "(el_info.upd_dept_flg = '1' or (el_info.upd_dept_flg = '2' and exists (select * from concurrent, el_upd_dept where  concurrent.emp_id = '$emp_id' and concurrent.emp_class = el_upd_dept.class_id and concurrent.emp_attribute = el_upd_dept.atrb_id and concurrent.emp_dept = el_upd_dept.dept_id and el_upd_dept.lib_id = el_info.lib_id)))";
			$cond .= " and ";
			$cond .= "(el_info.upd_st_flg = '1' or (el_info.upd_st_flg = '2' and exists (select * from concurrent, el_upd_st where concurrent.emp_id = '$emp_id' and el_upd_st.st_id = concurrent.emp_st and el_upd_st.lib_id = el_info.lib_id)))";
		$cond .= ")";
		// 更新：職員
		$cond .= " or ";
		$cond .= "exists (select * from el_upd_emp where el_upd_emp.lib_id = el_info.lib_id and el_upd_emp.emp_id = '$emp_id')";
	$cond .= ")";
	
	//元コンテンツが同じで、バージョンが呼び元と異なる(=最新ではない)判定
	$cond .= "and exists (select * from el_edition where el_edition.base_lib_id = $base_lib_id and el_edition.lib_id = el_info.lib_id) and el_info.lib_id <> $lib_id";
	
	$sel_ref_document = select_from_table($con, $sql, $cond, $fname);
	if ($sel_ref_document == 0)
	{
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	//==============================
	// 更新可能分のコンテンツ履歴を取得
	//==============================
	
	$sql = "select lib_id, lib_up_date, lib_archive, lib_cate_id, lib_extension, (select edition_no from el_edition where el_edition.lib_id = el_info.lib_id) as edition_no from el_info";
	$cond = "where lib_delete_flag = 'f'";
	
	//更新権限判定
	$cond .= " and";
	$cond .= " (";
		// 更新：所属
		$cond .= " (";
			$cond .= "(upd_dept_flg = '1' or (upd_dept_flg = '2' and exists (select * from el_upd_dept where el_upd_dept.lib_id = el_info.lib_id and el_upd_dept.class_id = $emp_class and el_upd_dept.atrb_id = $emp_atrb and el_upd_dept.dept_id = $emp_dept)))";
			$cond .= " and ";
			$cond .= "(upd_st_flg = '1' or (upd_st_flg = '2' and exists (select * from el_upd_st where el_upd_st.lib_id = el_info.lib_id and el_upd_st.st_id = $emp_st)))";
		$cond .= " )";
		// 更新：兼務所属
		$cond .= " or ";
		$cond .= "(";
			$cond .= "(el_info.upd_dept_flg = '1' or (el_info.upd_dept_flg = '2' and exists (select * from concurrent, el_upd_dept where  concurrent.emp_id = '$emp_id' and concurrent.emp_class = el_upd_dept.class_id and concurrent.emp_attribute = el_upd_dept.atrb_id and concurrent.emp_dept = el_upd_dept.dept_id and el_upd_dept.lib_id = el_info.lib_id)))";
			$cond .= " and ";
			$cond .= "(el_info.upd_st_flg = '1' or (el_info.upd_st_flg = '2' and exists (select * from concurrent, el_upd_st where concurrent.emp_id = '$emp_id' and el_upd_st.st_id = concurrent.emp_st and el_upd_st.lib_id = el_info.lib_id)))";
		$cond .= ")";
		// 更新：職員
		$cond .= " or";
		$cond .= " exists (select * from el_upd_emp where el_upd_emp.lib_id = el_info.lib_id and el_upd_emp.emp_id = '$emp_id')";
	$cond .= " )";
	
	//元コンテンツが同じで、バージョンが呼び元と異なる(=最新ではない)判定
	$cond .= " and exists (select * from el_edition where el_edition.base_lib_id = $base_lib_id and el_edition.lib_id = el_info.lib_id) and lib_id <> $lib_id";
	
	$sel_upd_document = select_from_table($con, $sql, $cond, $fname);
	if ($sel_upd_document == 0)
	{
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	//==============================
	// コンテンツ履歴を配列に格納
	//==============================
	
	//更新・参照のどちらかの権限があれば対象とする。
	$docs = array();
	while ($row = pg_fetch_array($sel_ref_document))
	{
		$docs[$row["edition_no"]] = array(
			"id" => $row["lib_id"],
			"path" => get_lib_path($row["lib_cate_id"], $row["lib_id"], $row["lib_extension"]),
			"modified" => $row["lib_up_date"]
			);
	}
	while ($row = pg_fetch_array($sel_upd_document))
	{
		$docs[$row["edition_no"]] = array(
			"id" => $row["lib_id"],
			"path" => get_lib_path($row["lib_cate_id"], $row["lib_id"], $row["lib_extension"]),
			"modified" => $row["lib_up_date"]
			);
	}
}
// 管理画面から呼ばれた場合
else
{
	//==============================
	// コンテンツ履歴を取得
	//==============================
	$sql = "select lib_id, lib_up_date, lib_archive, lib_cate_id, lib_extension, (select edition_no from el_edition where el_edition.lib_id = el_info.lib_id) as edition_no from el_info";
	$cond = "where lib_delete_flag = 'f' and exists (select * from el_edition where el_edition.base_lib_id = $base_lib_id and el_edition.lib_id = el_info.lib_id) and lib_id <> $lib_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0)
	{
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	//==============================
	// コンテンツ履歴を配列に格納
	//==============================
	$docs = array();
	while ($row = pg_fetch_array($sel))
	{
		$docs[$row["edition_no"]] = array(
			"id" => $row["lib_id"],
			"path" => get_lib_path($row["lib_cate_id"], $row["lib_id"], $row["lib_extension"]),
			"modified" => $row["lib_up_date"]
			);
	}
}




//画面名
$PAGE_TITLE = "履歴一覧";

//========================================================================================================================================================================================================
// HTML出力
//========================================================================================================================================================================================================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$INCIDENT_TITLE?> | <?=$PAGE_TITLE?></title>

<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
</style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>


<!-- ヘッダー START -->
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
<?
	show_hiyari_header_for_sub_window($PAGE_TITLE);
?>
</table>
<!-- ヘッダー END -->



</td></tr><tr><td>

<!-- 本体 START -->
<table border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
		<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
		<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
	</tr>
	<tr>
		<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
		<td bgcolor="#F5FFE5" align="center">




<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">

<tr height="22" bgcolor="#DFFFDC">
<td width="80" align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">版番号</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">更新日時</font></td>
<td width="60" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">参照</font></td>
</tr>

<?
krsort($docs);//版番号(キー)の降順に表示
foreach($docs as $i=>$tmp_doc)
{
	$tmp_doc = $docs[$i];
	if (strlen($tmp_doc["modified"]) == 14)
	{
		$modified = preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})$/", "$1/$2/$3 $4:$5:$6", $tmp_doc["modified"]);
	}
	else if (strlen($tmp_doc["modified"]) == 8)
	{
		$modified = preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $tmp_doc["modified"]);
	}
	else
	{
		$modified = "";
	}
	$tmp_lib_url = urlencode($tmp_doc["path"]);
	$tmp_btn = "<input type=\"button\" value=\"参照\" onclick=\"window.open('hiyari_el_refer.php?s=$session&i={$tmp_doc["id"]}&u=$tmp_lib_url');\">";

	echo("<tr height=\"22\" bgcolor=\"#FFFFFF\">\n");
	echo("<td align=\"right\" style=\"padding-right:6px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$i</font></td>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$modified</font></td>\n");
	echo("<td align=\"center\">$tmp_btn</td>\n");
	echo("</tr>\n");
}
?>
</table>





		</td>
		<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
	</tr>
	<tr>
		<td><img src="img/r_3.gif" width="10" height="10"></td>
		<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
		<td><img src="img/r_4.gif" width="10" height="10"></td>
	</tr>
</table>
<!-- 本体 END -->


</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
