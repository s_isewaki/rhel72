<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_select_values.ini");
require_once("get_values.ini");
require_once("inpatient_common.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$auth_id = ($subsys == "med") ? 57 : 14;
$checkauth = check_authority($session, $auth_id, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// データベースに接続
$con = connect2db($fname);

// 医療機関マスタを取得
$sql = "select a.mst_cd, a.mst_name, b.rireki_index, a.address1 from institemmst a left join (select mst_cd, max(rireki_index) as rireki_index from institemrireki group by mst_cd) b on b.mst_cd = a.mst_cd";
$cond = "order by a.mst_cd";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$insts = array();
while ($row = pg_fetch_array($sel)) {
    $insts[$row["mst_cd"]] = array(
        "name"   => $row["mst_name"],
        "rireki" => $row["rireki_index"],
        "city"   => $row["address1"]
    );
}

// 診療科マスタを取得（最新の履歴のみ）
$sql = "select a.* from institemrireki a inner join (select mst_cd, max(rireki_index) as rireki_index from institemrireki group by mst_cd) b on b.mst_cd = a.mst_cd and b.rireki_index = a.rireki_index";
$cond = "where a.disp_flg order by a.disp_order";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
while ($row = pg_fetch_array($sel)) {
    $insts[$row["mst_cd"]]["sects"][$row["item_cd"]]["name"] = $row["item_name"];
    for ($i = 1; $i <= 10; $i++) {
        if ($row["doctor_name$i"] == "" || $row["doctor_hide$i"] == "t") {
            continue;
        }
        $insts[$row["mst_cd"]]["sects"][$row["item_cd"]]["doctors"][$i] = $row["doctor_name$i"];
    }
}

// 診療科一覧の取得
$sql = "select sect_id, sect_nm from sectmst";
$cond = "where sect_del_flg = 'f' order by sect_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$sects = array();
while ($row = pg_fetch_array($sel)) {
    $sects[$row["sect_id"]] = array("name" => $row["sect_nm"]);
}

// 主治医一覧の取得
$sql = "select sect_id, dr_id, dr_nm from drmst";
$cond = "where dr_del_flg = 'f' order by dr_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
while ($row = pg_fetch_array($sel)) {
    if (!isset($sects[$row["sect_id"]])) {continue;}
    $sects[$row["sect_id"]]["doctors"][$row["dr_id"]] = $row["dr_nm"];
}
unset($sel);
pg_close($con);
?>
<title>CoMedix メドレポート｜紹介先情報検索<? if ($subsys != "med") { ?>登録<? } else { ?>参照<? } ?></title>
<script type="text/javascript" src="js/jquery/jquery-1.10.1.min.js"></script>
<script type="text/javascript" src="js/swapimg.js"></script>
<script type="text/javascript">
// 医療機関・施設検索子画面を開く
var childWin;
function openInstSearchWindow(callback) {
    childWin = window.open('bed_inst_search.php?session=<? echo $session; ?>&callback=' + callback, 'instsrch', 'width=640,height=480,scrollbars=yes');
}

function clearPrInst() {
    document.inpt.pr_inst_cd.value = '';
    if (document.inpt.pr_inst_name) {
        document.inpt.pr_inst_name.value = '';
    }
    document.inpt.pr_sect_rireki.value = '';
    if (document.inpt.pr_sect_cd) {
        clearOptions(document.inpt.pr_sect_cd);
    }
    if (document.inpt.pr_doctor_no) {
        clearOptions(document.inpt.pr_doctor_no);
    }
}

function selectPrInst(cd) {
    switch (cd) {
<? foreach ($insts as $tmp_inst_cd => $tmp_inst) { ?>
    case '<? echo $tmp_inst_cd; ?>':
        document.inpt.pr_inst_cd.value = '<? echo $tmp_inst_cd; ?>';
        if (document.inpt.pr_inst_name) {
            document.inpt.pr_inst_name.value = '<? echo $tmp_inst["name"]; ?>';
        }
        document.inpt.pr_sect_rireki.value = '<? echo $tmp_inst["rireki"]; ?>';
        break;
<? } ?>
    }

    setPrSectOptions();
}

function setPrSectOptions(defaultSectCd, defaultDoctorNo) {
    if (!document.inpt.pr_sect_cd) {
        return;
    }

    clearOptions(document.inpt.pr_sect_cd);

    var instCd = document.inpt.pr_inst_cd.value;
    if (!instCd) {
        return;
    }

    switch (instCd) {
<? foreach ($insts as $tmp_inst_cd => $tmp_inst) { ?>
    case '<? echo($tmp_inst_cd); ?>':
<? foreach ($tmp_inst["sects"] as $tmp_sect_cd => $tmp_sect) { ?>
        addOption(document.inpt.pr_sect_cd, '<? echo($tmp_sect_cd); ?>', '<? echo($tmp_sect["name"]); ?>', defaultSectCd);
<? } ?>
        break;
<? } ?>
    }

    setPrDoctorOptions(defaultDoctorNo);
}

function setPrDoctorOptions(defaultDoctorNo) {
    if (!document.inpt.pr_doctor_no) {
        return;
    }

    clearOptions(document.inpt.pr_doctor_no);

    var sectCd = document.inpt.pr_sect_cd.value;
    if (!sectCd) {
        return;
    }

    var instCd = document.inpt.pr_inst_cd.value;
<? foreach ($insts as $tmp_inst_cd => $tmp_inst) { ?>
    <? foreach ($tmp_inst["sects"] as $tmp_sect_cd => $tmp_sect) { ?>
    if (instCd == '<? echo($tmp_inst_cd); ?>' && sectCd == '<? echo($tmp_sect_cd); ?>') {
        <? foreach ($tmp_sect["doctors"] as $tmp_doctor_no => $tmp_doctor_name) { ?>
            addOption(document.inpt.pr_doctor_no, '<? echo($tmp_doctor_no); ?>', '<? echo($tmp_doctor_name); ?>', defaultDoctorNo);
        <? } ?>
    }
    <? } ?>
<? } ?>
}

function clearOptions(selectbox) {
    for (var i = selectbox.length - 1; i > 0; i--) {
        selectbox.options[i] = null;
    }
}

function addOption(box, value, text, selected) {
    var opt = document.createElement("option");
    opt.value = value;
    opt.text = text;
    if (selected == value) {
        opt.selected = true;
    }
    box.options[box.length] = opt;
    if (!box.multiple) {
        try {box.style.fontSize = 'auto';} catch (e) {}
        box.style.overflow = 'auto';
    }
}

// 検索した紹介先情報をテンプレート画面にセットする
function setReferInfo() {
    if (opener && !opener.closed && opener.selectDisease) {
        var inst_name   = $('#pr_inst_name').val();
        var sect_name   = $('#pr_sect_cd option:selected').text();
        var doctor_name = $('#pr_doctor_no option:selected').text();
        opener.setReferInfo(inst_name, sect_name, doctor_name);
    }
}

// ウィンドウクローズ時
$(window).unload(function(){
    if (childWin) {
        childWin.close();
    }
});
</script>
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<style type="text/css">
.info_table { width:100%; border-collapse:separate; border-spacing:1px; background-color:#5279a5; empty-cells:show; }
.info_table tr { background-color:#fff }
.info_table th { padding:3px 1px; font-weight:normal; background-color:#eff9fd }
.info_table td { padding:1px; vertical-align:middle }
</style>
</head>
<body>
<div style="padding:4px">
<div>
  <table class="dialog_titletable" style="background-color: #5279a5;"><tr>
    <th>紹介先情報検索</th>
      <td><a href="javascript:window.close()"><img src="img/icon/close.gif" alt="閉じる"
        onmouseover="swapimg(this,'img/icon/closeo.gif');" onmouseout="swapimg(this,'img/icon/close.gif');" /></a></td>
  </tr></table>
</div>
<form id="inpt" name="inpt">
<table class="info_table" cellspacing="1">
<tr>
  <td style="cursor: pointer;">紹介先医療機関名</td>
  <td style="cursor: pointer;">
    <input type="text" id="pr_inst_name" name="pr_inst_name"value="" size="40" disabled>
    <input type="button" value="検索" onclick="openInstSearchWindow('selectPrInst');">
    <input type="button" value="クリア" onclick="clearPrInst();">
  </td>
</tr>
<tr>
  <td style="cursor: pointer;">（科）</td>
  <td style="cursor: pointer;">
    <select id="pr_sect_cd" name="pr_sect_cd" onchange="setPrDoctorOptions();">
      <option value="">　　　　　</option>
    </select>
  </td>
</tr>
<tr>
  <td style="cursor: pointer;">担当医</td>
  <td style="cursor: pointer;">
    <select id="pr_doctor_no" name="pr_doctor_no">
      <option value="">　　　　　</option>
    </select>
  </td>
</tr>
</table>
<div style="padding-top: 5px; text-align: right;"><input type="button" value="決定" onclick="setReferInfo();"></div>
<input type="hidden" name="session" value="<? echo $session; ?>">
<input type="hidden" name="pt_id" value="<? echo $pt_id; ?>">
<input type="hidden" name="pr_inst_cd" value="<? echo $pr_inst_cd; ?>">
<input type="hidden" name="pr_sect_rireki" value="<? echo $pr_sect_rireki; ?>">
</form>
</div>
</body>
</html>
