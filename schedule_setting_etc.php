<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix スケジュール | その他</title>
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once('Cmx.php');
require_once('Cmx/Model/SystemConfig.php');
require("config_log_list.php");

$fname = $PHP_SELF;





// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 13, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// スケジュールの初期画面を取得
$sql = "select schedule1_default from option";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$schedule1_default = pg_fetch_result($sel, 0, "schedule1_default");
switch ($schedule1_default) {
	case "1":
		$default_url = "schedule_menu.php";
		break;
	case "2":
		$default_url = "schedule_week_menu.php";
		break;
	case "3":
		$default_url = "schedule_month_menu.php";
		break;
}

// ログ用system_config
$conf = new Cmx_SystemConfig();


if($aprv_ctrl_upd_flg =="1")
{
	$conf->set("schedule.aprv_ctrl", $aprv_ctrl);  // プロパティファイルを更新する
	$conf->set("schedule.span_default", $span_default);  // 時間指定の初期値を更新する 20141030TN
}

$aprv_ctrl = $conf->get("schedule.aprv_ctrl");
$span_default = $conf->get("schedule.span_default");

if($aprv_ctrl == "2")
{
	$str_chk1 = "";
	$str_chk2 = "checked";
	
}
else
{
	$str_chk2 = "";
	$str_chk1 = "checked";
}

?>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">

<!--ボーダー追加 20141030TN-->
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>

</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<? echo($default_url); ?>?session=<? echo($session); ?>"><img src="img/icon/b02.gif" width="32" height="32" border="0" alt="スケジュール"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<? echo($default_url); ?>?session=<? echo($session); ?>"><b>スケジュール</b></a> &gt; <a href="schedule_place_list.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="<? echo($default_url); ?>?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="100" align="center" bgcolor="#bdd1e7"><a href="schedule_place_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">行先一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="schedule_place_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">行先登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="schedule_type_update.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">種別</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="schedule_comgroup.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">共通グループ</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#5279a5"><a href="schedule_setting_etc.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>その他</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="schedule_admin_bulk_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括登録</font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<form name="mainform" action="schedule_setting_etc.php" method="post">

<script type="text/javascript">

	///-----------------------------------------------------------------------------
	//行／列ハイライト
	///-----------------------------------------------------------------------------
	function regist_start() 
	{
		document.mainform.aprv_ctrl_upd_flg.value = "1";
		document.mainform.submit();
	}

</script>

<!--ボーダー追加 20141030TN-->
<table height="27" width="600" border="0" cellspacing="0" cellpadding="2">
    <tr height="22">
        <td valign="bottom"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>予定の承認設定</b></font></td>
    </tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
    <tr height="22">
        <td width="140" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">予定の承認依頼</font></td>
        <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                <input type="radio" name="aprv_ctrl" value="1" <?php echo($str_chk1);?>>承認依頼する
                <input type="radio" name="aprv_ctrl" value="2" <?php echo($str_chk2);?>>承認依頼しない
        </font></td>
    </tr>
</table>

<!--時間指定の初期値設定 20141030TN-->
<table width="600" border="0" cellspacing="0" cellpadding="2">
    <tr height="22">
        <td valign="bottom"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>予定の追加画面</b></font></td>
    </tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
    <tr height="22">
        <td width="140" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時間指定の初期表示</font></td>
        <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
            <select name="span_default">
            <option value="" <?php if($span_default==""){echo "selected";};?>></option>
            <option value="30" <?php if($span_default=="30"){echo "selected";}?>>30分</option>
            <option value="60" <?php if($span_default=="60"){echo "selected";};?>>1時間</option>
            <option value="90" <?php if($span_default=="90"){echo "selected";};?>>1時間30分</option>
            <option value="120" <?php if($span_default=="120"){echo "selected";};?>>2時間</option>
            <option value="150" <?php if($span_default=="150"){echo "selected";};?>>2時間30分</option>
            <option value="180" <?php if($span_default=="180"){echo "selected";};?>>3時間</option>
            </select>
        </font></td>
    </tr>
</table>

<table
    <tr height="22" width="600" border="0" cellspacing="0" cellpadding="2">
        <td align="right"><input type="button" value="更新" onclick="regist_start();"></td>
    </tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="aprv_ctrl_upd_flg" value="">
</form>
</body>
<? pg_close($con); ?>
</html>
