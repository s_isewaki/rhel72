<?
require_once("Cmx.php");
require_once("aclg_set.php");
require("about_session.php");
require("about_authority.php");
require("hiyari_common.ini");

$fname=$PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 47, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if (!is_array($news_ids)) {
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('削除対象が選択されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// トランザクションを開始
pg_query($con, "begin");

// チェックされたお知らせをループ
foreach ($news_ids as $tmp_news_id) {

	// お知らせを削除
	$sql = "delete from inci_news";
	$cond = "where news_id = $tmp_news_id";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// お知らせ参照情報を削除
	$sql = "delete from inci_news_ref";
	$cond = "where news_id = $tmp_news_id";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// 添付ファイル情報を削除
	$sql = "delete from inci_newsfile";
	$cond = "where news_id = $tmp_news_id";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);




// 添付ファイルの削除
$newsfile_folder_name = get_newsfile_folder_name();
foreach ($news_ids as $tmp_news_id)
{
	foreach (glob("{$newsfile_folder_name}/{$tmp_news_id}_*.*") as $tmpfile)
	{
		unlink($tmpfile);
	}
}
// ユーザーの登録した一時ファイルを削除
foreach (glob("{$newsfile_folder_name}/tmp/{$session}_*.*") as $tmpfile)
{
	unlink($tmpfile);
}
// 30分以上前に保存された一時ファイルを削除
foreach (glob("{$newsfile_folder_name}/tmp/*.*") as $tmpfile)
{
	if (time() - filemtime($tmpfile) >= 30 * 60)
	{
		unlink($tmpfile);
	}
}


// お知らせ一覧画面に遷移
echo("<script type=\"text/javascript\">location.href = 'hiyari_news_menu.php?session=$session&page=$page';</script>");
?>
