<!-- ***************************************************************** -->
<!-- 勤務シフト作成　届出書添付書類画面 様式９ ＣＬＡＳＳ PHP Excel対応-->
<!-- 看護協会版届出様式「一般病棟・療養病棟１・２入院基本料金表」      -->
<!-- ***************************************************************** -->

<?
	//
	//
	//	末尾に行変数の説明があります
	//
	//
require_once("about_authority.php");
require_once("about_session.php");
require_once("about_postgres.php");

require_once("duty_shift_common_class.php");
require_once("duty_shift_time_common_class.php");

class duty_shift_report_associate_class
{
	var $file_name;	// 呼び出し元ファイル名
	var $_db_con;	// DBコネクション
	var $obj;	// 勤務シフト作成共通クラス
	var $obj_time;	// 勤務シフト作成 時刻 共通クラス
	var $excelColumnName = array(); // Excelカラム名配列
	var $MSPgothic;			// フォント指定用、MSP-ゴシック
	var $MSPmincho;			// フォント指定用、MSP-明朝
	/*************************************************************************/
	// コンストラクタ
	// @param object $con DBコネクション
	// @param string $fname 画面名
	/*************************************************************************/
	function duty_shift_report_associate_class($con, $fname)
	{
		// 呼び出し元ファイル名を保持
		$this->file_name = $fname;
		// DBコネクションを保持
		$this->_db_con = $con;
		// クラス new
		$this->obj = new duty_shift_common_class($con, $fname);
		$this->obj_time = new duty_shift_time_common_class($con, $fname);
		$this->excelColumnName = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ','BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN');
		$this->MSPgothic = mb_convert_encoding('ＭＳ Ｐゴシック','UTF-8','EUC-JP');
		$this->MSPmincho = mb_convert_encoding('ＭＳ Ｐ明朝','UTF-8','EUC-JP');
	}
	//////////////////////////////////////////////////
	//						//
	//	一般病棟・療養病棟１・２、共通部分	//
	//						//
	//////////////////////////////////////////////////
	function publicHeader(
			$excelObj,	// ExcelObject
			$plan_result_flag, // 1予定 2実績 3予定+実績
			$date_y1,	// 作成年月日
			$date_m1 ,
			$date_d1 ,
			$hospital_name,	// 保険医療機関名
			$ward_cnt ,	// 病棟数
			$sickbed_cnt	// 病床数
	){
		// Excel基本書式設定
		// 印刷
		$excelObj->SetSheetName("NS100");
		$excelObj->PaperSize("A3");
		$excelObj->PageSetUp("horizontial");
		$excelObj->PageRatio("USER","70");
		$excelObj->SetMargin( 1 , 0.5 , 1.5 , 1.5 , 0.5 , 0.5 ); // 左右上下頭足
		// 画面表示
		$excelObj->DisplayZoom( "65" ); // 65%表示
		$excelObj->SetGridline( "true" ); // 枠線表示
		
		// 列幅 --- 隠し列のAW-AZ列は最後に0行設定
		$excelObj->SetColDim("A",1.13);
		$excelObj->SetColDim("B",7.50);
		$excelObj->SetColDim("C",6.25);
		$excelObj->SetColDim("D",6.25);
		$excelObj->SetColDim("E",18);
		$excelObj->SetColDim("F",3.25);
		$excelObj->SetColDim("G",4);
		$excelObj->SetColDim("H",6.63);
		$excelObj->SetColDim("I",2.88);
		$excelObj->SetColDim("J",2.88);
		$excelObj->SetColDim("K",2.88);
		$excelObj->SetColDim("L",7.13);
		$excelObj->SetColDim("M",8.38);
		for($col=13; $col<44; $col++){ // N:AR列
			$excelObj->SetColDim($this->excelColumnName[$col],5);
		}
		$excelObj->SetColDim("AS",8.38);
		$excelObj->SetColDim("AT",8.38);
		$excelObj->SetColDim("AU",8.38);
		$excelObj->SetColDim("AV",8.38);
		
		// 帳票共通出力
		// 1行目
		$excelObj->SetRowDim(1,18.75);
		
		$excelObj->SetValueJP2("C1","入院基本料に係る届出書添付書類(様式９)","BOLD",$this->MSPgothic,14);
		$excelObj->SetValueJPwith("N1","作成年月日","");
		$excelObj->SetValuewith("Q1", $date_y1 , "");$excelObj->SetBorder("MEDIUM","outline");$excelObj->SetValueJPwith("R1","年","");
		$excelObj->SetValuewith("S1", $date_m1 , "");$excelObj->SetBorder("MEDIUM","outline");$excelObj->SetValueJPwith("T1","月","");
		$excelObj->SetValuewith("U1", $date_d1 , "");$excelObj->SetBorder("MEDIUM","outline");$excelObj->SetValueJPwith("V1","日","");
		
		$excelObj->SetArea("N1:V1");
		$excelObj->SetFont($this->MSPgothic,11); 
		
		switch ($plan_result_flag){
			case 2:$excelObj->SetValueJPwith("W1","(予定から作成)","");break;
			case 3:$excelObj->SetValueJPwith("W1","(実績と予定を組み合わせて作成 )","");break;
			default:;
		}
		
		// 2行目
		$excelObj->SetRowDim(2,9);
		// 3行目
		$excelObj->SetRowDim(3,18.75);
		
		$excelObj->SetValueJP2("E3","保険医療機関名","BOLD",$this->MSPgothic,14);
		$excelObj->CellMerge2("F3:K3" , "medium","outline", "");
		$excelObj->SetValueJP2("F3",$hospital_name,"BOLD",$this->MSPgothic,14);
		
		$excelObj->SetValueJPwith("M3","病棟数","");
		$excelObj->SetArea("N3:O3");
		$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->CellMergeReady();
		$excelObj->SetValueJPwith("N3", $ward_cnt ,"" );
		
		$excelObj->SetValueJPwith("R3","病床数","");
		$excelObj->SetArea("T3:U3");
		$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->CellMergeReady();
		$excelObj->SetValuewith("T3" , $sickbed_cnt );
		
		$excelObj->SetArea("M3:U3");
		$excelObj->SetFont($this->MSPgothic,11); 
		
		// 4行目
		$excelObj->SetRowDim(4,11.25);
	}
	
	//////////////////////////////////////////
	//					//
	// ------------ 看護職員表 ------------ //
	// 					//
	//////////////////////////////////////////
	function ShowNurseData1(
			$excelObj,		// PHP-Excelオブジェクト
			$start_row_number,	// 表を開始する行位置
			$obj,			// 共通クラス
			$data_array,		// 看護師／准看護師情報
			$week_array,		// 曜日配列
			$day_cnt,		// 日数
			$kango_space_line	// 看護職員表、空白行数
	){
		
		$arg_array = array();// 戻り値用
		
		// 追加する空白行にゴミを出さないよう、追加行分の内容をクリア
		if( $kango_space_line != 0 ){
			$add_line_count = count($data_array) + $kango_space_line;
			for ( $r=count($data_array); $r<$add_line_count; $r++){
				$data_array[$i]["job_name"] = "";
				$data_array[$i]["main_group_name"] = "";
				$data_array[$i]["staff_name"] = "";
				$data_array[$i]["duty_form"] = "";
				$data_array[$i]["other_post_flg"] = "";
				$data_array[$i]["night_duty_flg"] = "";
				for($k=1; $k<=$day_cnt; $k++) {
					$data_array[$i]["duty_time_$k"]= "";;
					$data_array[$i]["night_time_$k"]= "";;
				}
			}
		}
		
		$RowNumber = $start_row_number;
		$KangoRowTop = $start_row_number + 2; // 看護職員表開始のEXCEL行番号
		$MidashiRow1 = $KangoRowTop;
		$MidashiRow2 = $MidashiRow1 + 1;
		$MidashiRow3 = $MidashiRow1 + 2;
		$DayCntRow   = $KangoRowTop - 4;
		$LaborCntRow = $KangoRowTop - 3;
		// 看護職員表の見出し
		
		// 表名
		$excelObj->SetRowDim($RowNumber,18.75);
		$excelObj->SetValueJP2("B".$RowNumber,"【勤務計画表】","BOLD VCENTER",$this->MSPgothic ,16);
		
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,18.75);
		$excelObj->SetValueJP2("B".$RowNumber,"《看護職員表》","BOLD VCENTER",$this->MSPgothic ,14);
		
		// 行高さ設定
		$excelObj->SetRowDim($MidashiRow1,54);
		$excelObj->SetRowDim($MidashiRow2,13.5);
		$excelObj->SetRowDim($MidashiRow3,13.5);
		
		// 見出し
		$excelObj->SetValueJPwith("B".$MidashiRow1,"種別","BOLD");
		$excelObj->CellMerge("B".$MidashiRow1.":B".$MidashiRow3);

		$excelObj->SetValueJPwith("C".$MidashiRow1,"番号","BOLD");
		$excelObj->CellMerge("C".$MidashiRow1.":C".$MidashiRow3);

		$excelObj->SetValueJPwith("D".$MidashiRow1,"病棟名","BOLD");
		$excelObj->CellMerge("D".$MidashiRow1.":D".$MidashiRow3);

		$excelObj->SetValueJPwith("E".$MidashiRow1,"氏    名","BOLD");
		$excelObj->CellMerge("E".$MidashiRow1.":E".$MidashiRow3);

		$excelObj->SetValueJPwith("F".$MidashiRow1,"雇用・勤務形態","BOLD");
		$excelObj->CellMerge("F".$MidashiRow1.":H".$MidashiRow3);

		$excelObj->SetValueJPwith("I".$MidashiRow1,"夜勤の有無","BOLD");
		$excelObj->CellMerge("I".$MidashiRow1.":K".$MidashiRow3);

		$excelObj->SetValueJP2("L".$MidashiRow1,"夜勤従事者数への計上","WRAP",$this->MSPgothic ,10); 
		$excelObj->CellMerge("L".$MidashiRow1.":L".$MidashiRow3);

		$excelObj->CellMerge("M".$MidashiRow1.":M".$MidashiRow3);

		// 「日付別の勤務時間数」見出しのセルマージ領域。1ヶ月が31未満でも31日分のセルを用意する
		$excelObj->CellMerge2("N".$MidashiRow1.":AR".$MidashiRow1,"thin","bottom","");
		$excelObj->SetValueJP2("N".$MidashiRow1,"日付別の勤務時間数","BOLD VCENTER",$this->MSPgothic ,12); 
		
		// 日数
		for($c=1; $c<=$day_cnt; $c++){
			$cellArea=$this->excelColumnName[$c+12].$MidashiRow2;
			$excelObj->SetValueJPwith($cellArea , $c."日" , "");
		}
		// 曜日
		for($c=1; $c<=$day_cnt; $c++){
			$cellArea=$this->excelColumnName[$c+12]."".$MidashiRow3;
			$excelObj->SetValueJPwith($cellArea ,$week_array[$c]["name"]."曜" , "");
		}

		$excelObj->CellMerge("AS".$MidashiRow1.":AS".$MidashiRow3);
		$excelObj->SetValueJP2("AS".$MidashiRow1,"月勤務時間数（延べ時間数）","WRAP",$this->MSPgothic ,10); 

		$excelObj->CellMerge("AT".$MidashiRow1.":AT".$MidashiRow3);
		$excelObj->SetValueJP2("AT".$MidashiRow1,"（再掲）夜勤専従者及び月16時間以下の者の夜勤時間数","WRAP",$this->MSPgothic ,9); 

		$excelObj->CellMerge("AU".$MidashiRow1.":AV".$MidashiRow2);
		$excelObj->SetValueJP2("AU".$MidashiRow1,"職種別月勤務時間数","WRAP",$this->MSPgothic ,11); 

		// *** 隠し列 ***
		$excelObj->CellMerge("AW".$MidashiRow1.":AX".$MidashiRow2);
		$excelObj->SetValueJP2("AW".$MidashiRow1,"職種別\n常勤換算配置数","WRAP",$this->MSPgothic ,11); 

		// 見える列
		$excelObj->SetValueJPwith("AU".$MidashiRow3,"看護師","WRAP");
		$excelObj->SetValueJPwith("AV".$MidashiRow3,"准看護師","WRAP");

		// ** 隠し列 **
		$excelObj->SetValueJPwith("AW".$MidashiRow3,"看護師","WRAP");
		$excelObj->SetValueJPwith("AX".$MidashiRow3,"准看護師","WRAP");

		// AU-AXまとめて処理
		$excelObj->SetArea("AU".$MidashiRow3.":AX".$MidashiRow3);
		$excelObj->SetFont($this->MSPgothic ,11); 
		$excelObj->SetBorder2("double","","top");

		// AY-AZ(罫線無し)
		$excelObj->SetValueJPwith("AY".$MidashiRow3,"全体","");
		$excelObj->SetPosH("CENTER");
		$excelObj->SetValueJPwith("AZ".$MidashiRow3,"夜専","");
		$excelObj->SetPosH("CENTER");

		// B-AZまとめて
		$excelObj->SetArea("B".$MidashiRow1.":AZ".$MidashiRow3,$this->MSPgothic ,12); 
		$excelObj->SetPosV("CENTER");
		$excelObj->SetShrinkToFit();
		$excelObj->SetArea("B".$MidashiRow1.":L".$MidashiRow3,$this->MSPgothic ,12); 
		$excelObj->SetPosH("CENTER");

		//
		// データ行
		//
		$RowNumber = $KangoRowTop + 3;  // 行位置、一行ずつカウントして行く。2段表示のときは$RowNextを下段位置とする
		$KangoRowDataTop = $RowNumber ; // 看護職員表データ行開始のEXCEL行番号

		// 縦集計計算式用配列
		$VSumU = array(); // 上段
		$VSumL = array(); // 下段
		for($i=0; $i<51; $i++){ //0:A列、1:B列、・・・ 25:Z列・・・AZ列まで対応
			$VSumU[$i] = "=";
			$VSumL[$i] = "=";
		}
		if ( $kango_space_line != 0 ){
			$row_max = $add_line_count;
		}else{
			$row_max = count($data_array);
		}
		for($i=0; $i<$row_max; $i++) {
			$no = $i + 1;
			$RowNext = $RowNumber + 1;
			// 種別
			$excelObj->CellMerge("B".$RowNumber.":B".$RowNext);
			$excelObj->SetValueJPwith("B".$RowNumber, $data_array[$i]["job_name"],"");

			// 番号
			$excelObj->CellMerge("C".$RowNumber.":C".$RowNext);
			$excelObj->SetValuewith("C".$RowNumber, $no,"");

			// 病棟名
			$excelObj->CellMerge("D".$RowNumber.":D".$RowNext);
			$excelObj->SetValueJPwith("D".$RowNumber, $data_array[$i]["main_group_name"],"");

			// 氏名
			$excelObj->CellMerge("E".$RowNumber.":E".$RowNext);
			$excelObj->SetValueJPwith("E".$RowNumber, $data_array[$i]["staff_name"],"");

			// 明細行内の見出し
			$excelObj->SetValueJPwith("F".$RowNumber, "常勤","");
			$excelObj->SetValueJPwith("G".$RowNumber, "非常勤","");
			$excelObj->SetValueJPwith("H".$RowNumber, "他部署兼務","");
			$CellArea="F".$RowNumber.":H".$RowNumber;
			$excelObj->SetArea($CellArea);
			$excelObj->SetFont($this->MSPgothic ,8); 
			$excelObj->SetValueJPwith("I".$RowNumber, "有","");
			$excelObj->SetValueJPwith("J".$RowNumber, "無","");
			$excelObj->SetValueJPwith("K".$RowNumber, "夜専","");
			$CellArea="I".$RowNumber.":K".$RowNumber;
			$excelObj->SetArea($CellArea);
			$excelObj->SetFont($this->MSPgothic ,10); 
			$excelObj->SetValueJPwith("L".$RowNumber, "夜勤従事者","");
			$excelObj->SetFont($this->MSPgothic ,8); 
			$excelObj->SetValueJPwith("M".$RowNumber, "日勤時間数","");
			$excelObj->SetFont($this->MSPgothic ,10); 
			$excelObj->SetValueJPwith("M".$RowNext, "夜勤時間数","");
			$excelObj->SetFont($this->MSPgothic ,10); 

			// データ
			// duty_shift_report1_common_class.phpから流用
			//常勤／非常勤／他部署兼務
			//夜勤：有り／夜勤：無し／夜専
			$wk1 = '0';	// 常勤
			$wk2 = '0';	// 非常勤
			$wk3 = '0';	// 他部署兼務
//			$wk4 = "　";	// 夜勤有り→Excel計算式で求める
//			$wk5 = "　";	// 夜勤無し→Excel計算式で求める
			$wk6 = '0';	// 夜専
			if ($data_array[$i]["duty_form"] == "1") { $wk1 = "1"; }
			if ($data_array[$i]["duty_form"] == "2") { $wk2 = "1"; }
			if ($data_array[$i]["other_post_flg"] == "1") { $wk3 = "1"; }
			//夜専以外
			if ($data_array[$i]["night_duty_flg"] != "2") {
				//16時間超を夜勤有りとする→Excel計算式で求める
//				if (($wk_times_1 + $wk_times_2) > 16) {
//					$wk4 = "1";
//				} else {
//					$wk5 = "1";
//				}
			} else {
				//夜専
				$wk6 = "1";
			}
			$excelObj->SetValuewith("F".$RowNext, $wk1,"");
			$excelObj->SetValuewith("G".$RowNext, $wk2,"");
			$excelObj->SetValuewith("H".$RowNext, $wk3,"");
			$wk0 = $wk1 + $wk2 + $wk3;
			if( $wk0 == 0 ){
				$excelObj->SetArea("F".$RowNext.":H".$RowNext);
				$excelObj->SetCharColor("FFC0C0C0");
			}

			// =IF(AS39>16,1,0)
			$formulae = "=IF(AS".$RowNext.">16,1,0)";
			$excelObj->SetValuewith("I".$RowNext, $formulae,"");
			// =IF(AS39<=16,1,0)
			$formulae = "=IF(AS".$RowNext."<=16,1,0)";
			$excelObj->SetValuewith("J".$RowNext, $formulae,"");
			$excelObj->SetValuewith("K".$RowNext, $wk6,"");
			$CellArea="F".$RowNext.":L".$RowNext;
			$excelObj->SetArea($CellArea);
			$excelObj->SetFont($this->MSPgothic ,11); 

			// 縦集計の計算式
			$VSumL[5]  = $VSumL[5]."F".$RowNext."+";
			$VSumL[6]  = $VSumL[6]."G".$RowNext."+";
			$VSumL[7]  = $VSumL[7]."H".$RowNext."+";
			$VSumL[8]  = $VSumL[8]."I".$RowNext."+";
			$VSumL[9]  = $VSumL[9]."J".$RowNext."+";
			$VSumL[10] = $VSumL[10]."K".$RowNext."+";
			$VSumL[11] = $VSumL[11]."L".$RowNext."+";

			//夜勤従事者数
			//=IF(OR(K39=1, J39=1), 0, IF(AND(F39=1, G39=0, H39=0), 1, MIN(1,(AS38+AS39)/($M$32*$M$31/7))))
			//         "=IF(OR(K39=1          , J39          =1), 0, IF(AND(F39          =1, G39          =0, H39          =0), 1, MIN(1,(AS38+            AS39          )/($M$32*$M$31/7))))";
			$formulae = "=IF(OR(K".$RowNext."=1, J".$RowNext."=1), 0, IF(AND(F".$RowNext."=1, G".$RowNext."=0, H".$RowNext."=0), 1, MIN(1,(AS".$RowNumber."+AS".$RowNext.")/(\$M".$LaborCntRow."*\$M".$DayCntRow."/7))))";
			$excelObj->SetValuewith("L".$RowNext , $formulae, "");
//			$excelObj->SetFormatStyle("0.00");
			$excelObj->SetFormatStyle("[Red][<0.00](0.00) ;[Black][>=0]0.00 "); // L列

			// 日付別の勤務時間数（日勤時間数、夜勤時間数）duty_shift_report1_common_class.phpから流用
			// Excelなので1回のdayループで日勤(上段)と夜勤(下段)の両方を処理する
			$wk_times_1 = 0.0;
			$wk_times_2 = 0.0;
			for($k=1; $k<=$day_cnt; $k++) {
				$colPos = $k + 12; // N列から
				//////////////
				// 日勤時間 //
				//////////////
				$wk = $data_array[$i]["duty_time_$k"];
				if ($wk <= 0) {
					$wk = "　";
				}
				$CellArea = $this->excelColumnName[$colPos].$RowNumber; // 上段
				$excelObj->SetValuewith($CellArea , $wk, "");
				$VSumU[$colPos] = $VSumU[$colPos].$CellArea."+"; // 縦集計用の計算式

				//////////////
				// 夜勤時間 //
				//////////////
				$wk = $data_array[$i]["night_time_$k"];
				if ($wk <= 0) {
					$wk = "　";
				}
				$CellArea = $this->excelColumnName[$colPos].$RowNext; // 下段
				$excelObj->SetValuewith($CellArea , $wk, "");
				$VSumL[$colPos] = $VSumL[$colPos].$CellArea."+"; // 縦集計用の計算式
			}
			////////////
			// 行集計 //
			////////////
			// AS列
			$CellArea = "AS".$RowNumber; // 上段
			$formulae = "=SUM(N".$RowNumber.":AR".$RowNumber.")"; // =SUM(N38:AR38)
			$excelObj->SetValuewith($CellArea , $formulae, "");

			$CellArea = "AS".$RowNext; // 下段
			$formulae = "=SUM(N".$RowNext.":AR".$RowNext.")"; // =SUM(N39:AR39)
			$excelObj->SetValuewith($CellArea , $formulae, "");

			// 縦集計計算式
			$VSumU[44] = $VSumU[44]."AS".$RowNumber."+";
			$VSumL[44] = $VSumL[44]."AS".$RowNext."+";

			// AT列
			$CellArea = "AT".$RowNext; // 下段
			// =IF(OR(K39=1,AS39<=16),AS39,0)
			//           =IF(OR(K39          =1,AS39          <=16),AS39          ,0)
			$formulae = "=IF(OR(K".$RowNext."=1,AS".$RowNext."<=16),AS".$RowNext.",0)"; // =IF(OR(K39=1,AS39<=16),AS39,0)
			$excelObj->SetValuewith($CellArea , $formulae, "");
			// 縦集計計算式
			$VSumL[45] = $VSumL[45]."AT".$RowNext."+";

			// AU列上
			// =IF(B38="看護師",AS38,0)
			$formulae = "=IF(B".$RowNumber."=\"看護師\",AS".$RowNumber.",0)";
			$CellArea = "AU".$RowNumber;
			$excelObj->SetValueJPwith($CellArea , $formulae, "");

			// AU列下
			// =IF(B38="看護師",AS39,0)
			$formulae = "=IF(B".$RowNumber."=\"看護師\",AS".$RowNext.",0)";
			$CellArea = "AU".$RowNext;
			$excelObj->SetValueJPwith($CellArea , $formulae, "");

			// 縦集計計算式
			$VSumU[46] = $VSumU[46]."AU".$RowNumber."+";
			$VSumL[46] = $VSumL[46]."AU".$RowNext."+";

			// AV列上
			// =IF(B38="准看護師",AS38,0)
			$formulae = "=IF(B".$RowNumber."=\"准看護師\",AS".$RowNumber.",0)";
			$CellArea = "AV".$RowNumber;
			$excelObj->SetValueJPwith($CellArea , $formulae, "");

			// AV列下
			// =IF(B38="准看護師",AS39,0)
			$formulae = "=IF(B".$RowNumber."=\"准看護師\",AS".$RowNext.",0)";
			$CellArea = "AV".$RowNext;
			$excelObj->SetValueJPwith($CellArea , $formulae, "");

			// 縦集計計算式
			$VSumU[47] = $VSumU[47]."AV".$RowNumber."+";
			$VSumL[47] = $VSumL[47]."AV".$RowNext."+";

			// *** 隠し列 ***
			// AW列下
			$CellArea = "AW".$RowNext;
			//	     =IF($B38               = "看護師", IF(AND($F39             =1, $G39             =0, $H39             =0),1,MIN(1,ROUNDDOWN((AU38              +AU39            )/($M$32*$M$31/7),2))),0)
			$formulae = "=IF(\$B"."$RowNumber"."=\"看護師\",IF(AND(\$F"."$RowNext"."=1, \$G"."$RowNext"."=0, \$H"."$RowNext"."=0),1,MIN(1,ROUNDDOWN((AU"."$RowNumber"."+AU"."$RowNext".")/(\$M$LaborCntRow*\$M$DayCntRow/7),2))),0)";
			$excelObj->SetValueJPwith($CellArea , $formulae, "");
			$VSumL[48] = $VSumL[48]."AW".$RowNext."+";

			// AX列下
			$CellArea = "AX".$RowNext;
			//           =IF($B38               ="准看護師"  ,IF(AND($F39             =1, $G39             =0, $H39             =0),1,MIN(1,ROUNDDOWN((AV38              +AV39            )/($M$32  *$M$31  /7),2))),0)
			$formulae = "=IF(\$B"."$RowNumber"."=\"准看護師\",IF(AND(\$F"."$RowNext"."=1, \$G"."$RowNext"."=0, \$H"."$RowNext"."=0),1,MIN(1,ROUNDDOWN((AV"."$RowNumber"."+AV"."$RowNext".")/(\$M$LaborCntRow*\$M$DayCntRow/7),2))),0)";
			$excelObj->SetValueJPwith($CellArea , $formulae, "");
			$VSumL[49] = $VSumL[49]."AX".$RowNext."+";

			// AY列下
			$CellArea = "AY".$RowNext;
			//           =IF(AND(AS38              =0,AS39            =0), "-",AS39)
			$formulae = "=IF(AND(AS"."$RowNumber"."=0,AS"."$RowNext"."=0),\"-\",AS"."$RowNext".")";
			$excelObj->SetValueJPwith($CellArea , $formulae, "");

			// AZ列下
			$CellArea = "AZ".$RowNext;
			//           =IF(AND(AS38	       =0,AS39		  =0), "-", IF(K39	      =1,AS39,		   "-"))
			$formulae = "=IF(AND(AS"."$RowNumber"."=0,AS"."$RowNext"."=0),\"-\",IF(K"."$RowNext"."=1,AS"."$RowNext".",\"-\"))";
			$excelObj->SetValueJPwith($CellArea , $formulae, "");

			// 行間の横線
			$CellArea = "B".$RowNumber.":AX".$RowNumber; // 細い実線
			$excelObj->SetArea($CellArea);
			$excelObj->SetBorder2("thin","","top");
			$CellArea = "F".$RowNext.":AX".$RowNext; // 細い破線
			$excelObj->SetArea($CellArea);
			$excelObj->SetBorder2("dotted","","top");
			$RowNumber += 2;
		}
		//////////////////
		//    列集計    //
		//////////////////
		$KangoRowSmallSum = $RowNumber ; // 看護職員表小計のExcel行番号
		$RowNext = $RowNumber + 1;
		// 左見出し
		$excelObj->CellMerge("B".$RowNumber.":B".$RowNext);

		$excelObj->CellMerge("C".$RowNumber.":C".$RowNext);
		$CellArea = "C".$RowNumber;
		$excelObj->SetValueJPwith($CellArea, "小計" ,"LEFT");

		$excelObj->CellMerge("D".$RowNumber.":D".$RowNext);

		$excelObj->CellMerge("E".$RowNumber.":E".$RowNext);

		$name = count($data_array) ;
		$name = sprintf( '%003d' , $name );
		$name = "001 〜 ".$name;
		$CellArea = "E".$RowNumber;
		$excelObj->SetValueJPwith($CellArea, $name, "");

		// 明細行内の見出し
		$excelObj->SetValueJPwith("F".$RowNumber, "常勤","");
		$excelObj->SetValueJPwith("G".$RowNumber, "非常勤","");
		$excelObj->SetValueJPwith("H".$RowNumber, "他部署兼務","");
		$CellArea="F".$RowNumber.":H".$RowNumber;
		$excelObj->SetArea($CellArea);
		$excelObj->SetFont($this->MSPgothic ,8); 
		$excelObj->SetValueJPwith("I".$RowNumber, "有","");
		$excelObj->SetValueJPwith("J".$RowNumber, "無","");
		$excelObj->SetValueJPwith("K".$RowNumber, "夜専","");
		$CellArea="I".$RowNumber.":K".$RowNumber;
		$excelObj->SetArea($CellArea);
		$excelObj->SetFont($this->MSPgothic ,11); 
		$excelObj->SetValueJPwith("L".$RowNumber, "夜勤従事者","");
		$excelObj->SetFont($this->MSPgothic ,8); 
		$excelObj->SetValueJPwith("M".$RowNumber, "日勤時間数","");
		$excelObj->SetFont($this->MSPgothic ,10); 
		$excelObj->SetValueJPwith("M".$RowNext, "夜勤時間数","");
		$excelObj->SetFont($this->MSPgothic ,10); 

		for($k=5; $k<=11; $k++) { // F〜L列
			$VSumL[$k] = substr( $VSumL[$k] , 0 , strlen($VSumL[$k]) - 1 ); // 末尾の + を消す
			$CellArea = $this->excelColumnName[$k].$RowNext;
			$excelObj->SetValuewith($CellArea, $VSumL[$k], "");
		}
		$excelObj->SetFormatStyle("[Red][<0.00](0.00) ;[Black][>=0]0.00 "); // L列

		for($k=1; $k<=$day_cnt; $k++) { // 日毎
			$colPos = $k + 12; // N列から
			// 上段
			$VSumU[$colPos] = substr( $VSumU[$colPos] , 0 , strlen($VSumU[$colPos]) - 1 ); // 末尾の + を消す
			$CellArea = $this->excelColumnName[$colPos].$RowNumber;
			$excelObj->SetValuewith($CellArea, $VSumU[$colPos], "");
			// 下段
			$VSumL[$colPos] = substr( $VSumL[$colPos] , 0 , strlen($VSumL[$colPos]) - 1 ); // 末尾の + を消す
			$CellArea = $this->excelColumnName[$colPos].$RowNext;
			$excelObj->SetValuewith($CellArea, $VSumL[$colPos], "");
		}
		// AS列
		$colPos = 44 ;
		// 上段
		$VSumU[$colPos] = substr( $VSumU[$colPos] , 0 , strlen($VSumU[$colPos]) - 1 ); // 末尾の + を消す
		$CellArea = $this->excelColumnName[$colPos].$RowNumber;
		$excelObj->SetValuewith($CellArea, $VSumU[$colPos], "");
		// 下段
		$VSumL[$colPos] = substr( $VSumL[$colPos] , 0 , strlen($VSumL[$colPos]) - 1 ); // 末尾の + を消す
		$CellArea = $this->excelColumnName[$colPos].$RowNext;
		$excelObj->SetValuewith($CellArea, $VSumL[$colPos], "");
		// AT列、下段だけ
		$colPos = 45 ;
		// 下段
		$VSumL[$colPos] = substr( $VSumL[$colPos] , 0 , strlen($VSumL[$colPos]) - 1 ); // 末尾の + を消す
		$CellArea = $this->excelColumnName[$colPos].$RowNext;
		$excelObj->SetValuewith($CellArea, $VSumL[$colPos], "");

		// AU列
		$colPos = 46 ;
		// 上段
		$VSumU[$colPos] = substr( $VSumU[$colPos] , 0 , strlen($VSumU[$colPos]) - 1 ); // 末尾の + を消す
		$CellArea = $this->excelColumnName[$colPos].$RowNumber;
		$excelObj->SetValuewith($CellArea, $VSumU[$colPos], "");
		// 下段
		$VSumL[$colPos] = substr( $VSumL[$colPos] , 0 , strlen($VSumL[$colPos]) - 1 ); // 末尾の + を消す
		$CellArea = $this->excelColumnName[$colPos].$RowNext;
		$excelObj->SetValuewith($CellArea, $VSumL[$colPos], "");

		// AV列
		$colPos = 47 ;
		// 上段
		$VSumU[$colPos] = substr( $VSumU[$colPos] , 0 , strlen($VSumU[$colPos]) - 1 ); // 末尾の + を消す
		$CellArea = $this->excelColumnName[$colPos].$RowNumber;
		$excelObj->SetValuewith($CellArea, $VSumU[$colPos], "");
		// 下段
		$VSumL[$colPos] = substr( $VSumL[$colPos] , 0 , strlen($VSumL[$colPos]) - 1 ); // 末尾の + を消す
		$CellArea = $this->excelColumnName[$colPos].$RowNext;
		$excelObj->SetValuewith($CellArea, $VSumL[$colPos], "");

		// AW列(隠し列)下段のみ
		$colPos = 48 ;
		$VSumL[$colPos] = substr( $VSumL[$colPos] , 0 , strlen($VSumL[$colPos]) - 1 ); // 末尾の + を消す
		$CellArea = $this->excelColumnName[$colPos].$RowNext;
		$excelObj->SetValuewith($CellArea, $VSumL[$colPos], "");
		// AX列(隠し列)下段のみ
		$colPos = 49 ;
		$VSumL[$colPos] = substr( $VSumL[$colPos] , 0 , strlen($VSumL[$colPos]) - 1 ); // 末尾の + を消す
		$CellArea = $this->excelColumnName[$colPos].$RowNext;
		$excelObj->SetValuewith($CellArea, $VSumL[$colPos], "");

		// AY-AZ(隠し列)
		$excelObj->SetValueJP2("AY".$RowNext, "小計","",$this->MSPgothic ,11);
		$excelObj->SetValueJP2("AZ".$RowNext, "小計","",$this->MSPgothic ,11);

		// 行間の横線
		$CellArea = "B".$RowNumber.":AX".$RowNumber; // 細い実線
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("thin","","top");
		$CellArea = "F".$RowNext.":AX".$RowNext; // 細い破線
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("dotted","","top");

		$RowNumber += 2;
		$RowNext = $RowNumber + 1;
		// 列集計（小計）おわり。

		$KangoRowMax = $RowNumber - 1; // 看護職員表終了時のExcel行番号
		$KangoRowSum = $RowNumber ; // 看護職員表終了時のExcel行番号

		// B列のみ「小計」の行まで看護職員表まとめて設定
		// 罫線、表示位置
		$CellArea = "B".$KangoRowTop.":B".$KangoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("thin","","right");
		$excelObj->SetPosH("CENTER");

		$CellArea = "B".$KangoRowTop.":AX".$KangoRowMax; // 周囲をやや太い線で囲む。後で小計行の上下へ二重線を引く
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("medium","","outline");
		// C列より右は「計」行まで
		$KangoRowMax += 2;

		// フォントB-D列
		$CellArea = "B".$KangoRowTop.":D".$KangoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetFont($this->MSPgothic ,11); 
		// フォントE列
		$CellArea = "E".$KangoRowTop.":E".$KangoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetFont($this->MSPgothic ,12); 
		// フォントM列
		$CellArea = "M".$KangoRowTop.":M".$KangoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetFont($this->MSPgothic ,10); 
		// フォント、スタイル N-AX列
		$CellArea = "N".$KangoRowDataTop.":AX".$KangoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetFont($this->MSPgothic ,11); 
		$excelObj->SetFormatStyle("[Red][<0.00](0.00) ;[Black][>=0]0.00 "); 

		// 色指定 I-J列
		$CellArea = "I".$KangoRowDataTop.":J".$KangoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBackColor("CCFFCC"); // 薄い緑
		// 色指定 L列
		$CellArea = "L".$KangoRowDataTop.":L".$KangoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBackColor("CCFFCC"); // 薄い緑
		// 色指定 AS-AX列
		$CellArea = "AS".$KangoRowDataTop.":AX".$KangoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBackColor("CCFFCC"); // 薄い緑

		// 表示位置・縦横
		$CellArea = "C".$KangoRowDataTop.":C".$KangoRowMax; // 右よりはデータ行。見出しは中央
		$excelObj->SetArea($CellArea);
		$excelObj->SetPosH("RIGHT");

		$CellArea = "D".$KangoRowTop.":D".$KangoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetPosH("CENTER");

		$CellArea = "E".$KangoRowTop.":E".$KangoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetPosH("LEFT");

		// 「小計」行の横罫線引きなおして、背景黄色にする
		$beforRowU = $RowNumber-2;
		$beforRowL = $RowNext-2;
		$CellArea = "B".$beforRowU.":AX".$beforRowL;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("double","","top");
		$excelObj->SetBorder2("double","","bottom");
		$excelObj->SetBackColor("FFFF99"); // 薄い黄色

		// 「計」開始
		$excelObj->CellMerge("C".$RowNumber.":C".$RowNext);

		$excelObj->SetValueJP2("C".$RowNumber, "計" ,"LEFT",$this->MSPgothic ,11);

		$excelObj->CellMerge("D".$RowNumber.":D".$RowNext);

		$excelObj->CellMerge("E".$RowNumber.":E".$RowNext);

		// 明細行内の見出し
		$excelObj->SetValueJPwith("F".$RowNumber, "常勤","");
		$excelObj->SetValueJPwith("G".$RowNumber, "非常勤","");
		$excelObj->SetValueJPwith("H".$RowNumber, "他部署兼務","");
		$CellArea="F".$RowNumber.":H".$RowNumber;
		$excelObj->SetArea($CellArea);
		$excelObj->SetFont($this->MSPgothic ,8); 
		$excelObj->SetValueJPwith("I".$RowNumber, "有","");
		$excelObj->SetValueJPwith("J".$RowNumber, "無","");
		$excelObj->SetValueJPwith("K".$RowNumber, "夜専","");
		$CellArea="I".$RowNumber.":K".$RowNumber;
		$excelObj->SetArea($CellArea);
		$excelObj->SetFont($this->MSPgothic ,11); 
		$excelObj->SetValueJP2("L".$RowNumber, "夜勤従事者","",$this->MSPgothic ,8); 
		$excelObj->SetValueJP2("M".$RowNumber, "日勤時間数","",$this->MSPgothic ,10); 
		$excelObj->SetValueJP2("M".$RowNext, "夜勤時間数","",$this->MSPgothic ,10); 

		$beforRowU = $RowNumber-2;
		$beforRowL = $RowNext-2;
		for($k=5; $k<=11; $k++) { // F〜L列
			$colPos=$k;
			$CellArea = $this->excelColumnName[$colPos].$RowNext;
			$formulae = "=".$this->excelColumnName[$colPos].$beforRowL;
			$excelObj->SetValuewith($CellArea, $formulae, "");
		}
		$excelObj->SetFormatStyle("[Red][<0.00](0.00) ;[Black][>=0]0.00 "); // L列
		for($k=1; $k<=$day_cnt; $k++) { // 日毎
			$colPos = $k + 12; // N列から
			// 上段
			$CellArea = $this->excelColumnName[$colPos].$RowNumber;
			$formulae = "=".$this->excelColumnName[$colPos].$beforRowU;
			$excelObj->SetValuewith($CellArea, $formulae, "");
			// 下段
			$CellArea = $this->excelColumnName[$colPos].$RowNext;
			$formulae = "=".$this->excelColumnName[$colPos].$beforRowL;
			$excelObj->SetValuewith($CellArea, $formulae, "");
		}
		// AS列
		// 上段
		$colPos = 44 ;
		$CellArea = $this->excelColumnName[$colPos].$RowNumber;
		$formulae = "=".$this->excelColumnName[$colPos].$beforRowU;
		$excelObj->SetValuewith($CellArea, $formulae, "");
		// 下段
		$CellArea = $this->excelColumnName[$colPos].$RowNext;
		$formulae = "=".$this->excelColumnName[$colPos].$beforRowL;
		$excelObj->SetValuewith($CellArea, $formulae, "");
		// AT列
		$colPos = 45 ;
		// 下段のみ
		$CellArea = $this->excelColumnName[$colPos].$RowNext;
		$formulae = "=".$this->excelColumnName[$colPos].$beforRowL;
		$excelObj->SetValuewith($CellArea, $formulae, "");
		// AU列
		// 上段
		$colPos = 46 ;
		$CellArea = $this->excelColumnName[$colPos].$RowNumber;
		$formulae = "=".$this->excelColumnName[$colPos].$beforRowU;
		$excelObj->SetValuewith($CellArea, $formulae, "");
		// 下段
		$CellArea = $this->excelColumnName[$colPos].$RowNext;
		$formulae = "=".$this->excelColumnName[$colPos].$beforRowL;
		$excelObj->SetValuewith($CellArea, $formulae, "");
		// AV列
		// 上段
		$colPos = 47 ;
		$CellArea = $this->excelColumnName[$colPos].$RowNumber;
		$formulae = "=".$this->excelColumnName[$colPos].$beforRowU;
		$excelObj->SetValuewith($CellArea, $formulae, "");
		// 下段
		$CellArea = $this->excelColumnName[$colPos].$RowNext;
		$formulae = "=".$this->excelColumnName[$colPos].$beforRowL;
		$excelObj->SetValuewith($CellArea, $formulae, "");
		// AW列(隠し列)下段のみ
		$colPos = 48 ;
		$CellArea = $this->excelColumnName[$colPos].$RowNext;
		$formulae = "=".$this->excelColumnName[$colPos].$beforRowL;
		$excelObj->SetValuewith($CellArea, $formulae, "");
		// AX列(隠し列)下段のみ
		$colPos = 49 ;
		$CellArea = $this->excelColumnName[$colPos].$RowNext;
		$formulae = "=".$this->excelColumnName[$colPos].$beforRowL;
		$excelObj->SetValuewith($CellArea, $formulae, "");

		// AY-AZ(隠し列)
		$excelObj->SetValueJP2("AY".$RowNext, "計","",$this->MSPgothic ,11);

		$excelObj->SetValueJP2("AZ".$RowNext, "計","",$this->MSPgothic ,11);

		// 「計」行をまとめて設定。横線など
		$CellArea = "C".$RowNumber.":AR".$RowNext;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("hair","","vertical");
		$excelObj->SetBackColor("CCFFCC"); // 薄い緑
		$excelObj->SetBorder2("medium","","bottom");

		$CellArea = "F".$RowNumber.":AX".$RowNumber;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("dotted","","bottom");

		// 縦線まとめて
		$CellArea = "C".$KangoRowTop.":AX".$RowNext;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("thin","","left");
		$excelObj->SetBorder2("medium","","bottom");
		$excelObj->SetBorder2("medium","","right");
		$excelObj->SetBorder2("hair","","vertical"); // new

		// AU:AV、AW:AXのやや太い枠
		$CellArea = "AU".$KangoRowTop.":AV".$RowNext;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("medium","","left");
		$excelObj->SetBorder2("medium","","right");

		// AY:AZの背景色、灰色
		$wk = $KangoRowTop + 2;
		$CellArea = "AY".$wk.":AZ".$KangoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBackColor("C0C0C0"); // 灰色

		// 縮小して全体を表示する指定
		$CellArea = "E".$KangoRowDataTop.":AX".$KangoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetShrinkToFit();

		// 全体のセル内縦位置をCENTERにする
		$CellArea = "B".$KangoRowDataTop.":AX".$KangoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetPosV("CENTER");

		$arg_array["KangoRowTop"] = $KangoRowTop;
		$arg_array["KangoRowDataTop"] = $KangoRowDataTop;
		$arg_array["KangoRowSum"] = $KangoRowSum;
		$arg_array["RowNumber"] = $RowNumber;
		return $arg_array;
	}

	//////////////////////////////////
	//				//
	//	一般病棟・看護職員集計	//
	//				//
	//////////////////////////////////
	function ShowNurseSum01( $excelObj , $arg_array ){

		$RowNumber = $arg_array["RowNumber"];
		$KangoRowSum = $arg_array["KangoRowSum"];

		$KEI_RowU = $KangoRowSum;
		$KEI_RowL = $KangoRowSum+1;

		// 中間集計１行目
		$RowNumber += 3;
		$KMidSumRow1 = $RowNumber;
		$KMidSumRow2 = $KMidSumRow1 + 1;
		$KMidSumRow3 = $KMidSumRow1 + 2;

		$excelObj->SetRowDim($RowNumber,19.50);

		$excelObj->CellMerge2("C".$RowNumber.":H".$RowNumber,"medium","outline","");

		$excelObj->SetValueJP2("C".$RowNumber, "夜勤従事者数(夜勤ありの職員数)〔Ｂ〕", "BOLD VCENTER",$this->MSPgothic ,14); 

		$excelObj->CellMerge2("I".$RowNumber.":K".$RowNumber,"medium","outline","FFFFCC"); // もっと薄い黄色

		$CellArea = "I".$RowNumber;
		// =ROUNDDOWN(L241,1)
		$formulae = "=ROUNDDOWN(L".$KEI_RowL.",1)";
		$excelObj->SetValuewith($CellArea, $formulae, "BOLD");
		$excelObj->SetFont($this->MSPgothic ,12); 
		$excelObj->SetFormatStyle("0.00");
		$excelObj->SetPosV("CENTER");

		$excelObj->CellMerge("L".$RowNumber.":Q".$RowNumber);
		$excelObj->SetBorder2("medium","","outline");

		$excelObj->SetValueJP2("L".$RowNumber, "月延べ勤務時間数の計〔Ｃ〕", "BOLD VCENTER",$this->MSPgothic ,14); 

		$excelObj->CellMerge2("R".$RowNumber.":S".$RowNumber,"medium","outline","FFFFCC"); // もっと薄い黄色

		$CellArea = "R".$RowNumber;
		//=AS240+AS241
		$formulae = "=AS".$KEI_RowU."+AS".$KEI_RowL;
		$excelObj->SetValuewith($CellArea, $formulae, "BOLD");
		$excelObj->SetFont($this->MSPgothic ,12); 
		$excelObj->SetFormatStyle("0.00");
		$excelObj->SetPosV("CENTER");

		// 中間集計２行目
		$RowNumber ++;
		$KMidSumRow2 = $RowNumber;

		$excelObj->SetRowDim($RowNumber,21);

		$excelObj->CellMerge("C".$RowNumber.":H".$RowNumber);
		$excelObj->SetBorder2("medium","","outline");

		$excelObj->SetValueJP2("C".$RowNumber, "月延べ夜勤時間数　〔D−E〕", "BOLD VCENTER",$this->MSPgothic ,14); 

		//=R244-AE244
		$excelObj->CellMerge2("I".$RowNumber.":K".$RowNumber,"medium","outline","FFFFCC"); // もっと薄い黄色

		$RowBefore = $RowNumber - 1 ;
		$CellArea = "I".$RowNumber;
		$formulae = "=R".$RowNumber."-AE".$RowNumber;
		$excelObj->SetValuewith($CellArea, $formulae, "BOLD");
		$excelObj->SetFont($this->MSPgothic ,12); 
		$excelObj->SetFormatStyle("0.00");
		$excelObj->SetPosV("CENTER");

		$excelObj->CellMerge("L".$RowNumber.":Q".$RowNumber);
		$excelObj->SetBorder2("medium","","outline");

		$excelObj->SetValueJP2("L".$RowNumber, "月延べ夜勤時間数の計〔Ｄ〕", "BOLD VCENTER",$this->MSPgothic ,14); 

		$excelObj->CellMerge2("R".$RowNumber.":S".$RowNumber,"medium","outline","FFFFCC"); // もっと薄い黄色

		$CellArea = "R".$RowNumber;
		//=AS241
		$formulae = "=AS".$KEI_RowL;
		$excelObj->SetValuewith($CellArea, $formulae, "BOLD");
		$excelObj->SetFont($this->MSPgothic ,12); 
		$excelObj->SetFormatStyle("0.00");
		$excelObj->SetPosV("CENTER");

		$excelObj->CellMerge("T".$RowNumber.":AD".$RowNumber);
		$excelObj->SetBorder2("medium","","outline");

		$excelObj->SetValueJP2($CellArea = "T".$RowNumber, "夜勤専従者及び月16時間以下の者の夜勤時間数〔E〕", "BOLD VCENTER",$this->MSPgothic ,12); 

		$excelObj->CellMerge2("AE".$RowNumber.":AF".$RowNumber,"medium","outline","FFFFCC"); // もっと薄い黄色

		$CellArea = "AE".$RowNumber;
		$formulae = "=AT".$KEI_RowL;
		$excelObj->SetValuewith($CellArea, $formulae, "BOLD");
		$excelObj->SetFormatStyle("0.00");
		$excelObj->SetPosV("CENTER");

		// 中間集計３行目
		$RowNumber ++;
		$KMidSumRow3 = $RowNumber;

		$excelObj->SetRowDim($RowNumber,21.75);

		$excelObj->CellMerge("C".$RowNumber.":K".$RowNumber);
		$excelObj->SetBorder2("medium","","outline");

		$excelObj->SetValueJP2("C".$RowNumber, "1日看護配置数　　〔(A ／届出区分の数)×３〕", "BOLD VCENTER",$this->MSPgothic ,14); 

		$CellArea = "L".$RowNumber;
		$formulae = "=J15"; // <=★★要注意★★15の位置が可変である
		$excelObj->SetValuewith($CellArea, $formulae, "BOLD");
		$excelObj->SetFormatStyle("0.00");
		$excelObj->SetPosV("CENTER");
		$excelObj->SetBorder2("medium","","outline");
		$excelObj->SetBackColor("FFFFCC"); // もっと薄い黄色

		$excelObj->CellMerge("M".$RowNumber.":V".$RowNumber);
		$excelObj->SetBorder2("medium","","outline");

		$excelObj->SetValueJP2("M".$RowNumber, "月平均１日当たり看護配置数〔C／(日数×８）〕", "BOLD VCENTER",$this->MSPgothic ,14); 
		$excelObj->SetBorder2("medium","","outline");

		$excelObj->CellMerge2("W".$RowNumber.":X".$RowNumber,"medium","outline","");

		$CellArea = "W".$RowNumber;
		// =ROUNDDOWN(R243/(M31*8),1)
		$formulae = "=ROUNDDOWN(R".$KMidSumRow1."/(M31*8),1)";
		$excelObj->SetValuewith($CellArea, $formulae, "BOLD");
		$excelObj->SetFormatStyle("0.00");
		$excelObj->SetPosV("CENTER");
		$excelObj->SetBackColor("FFFFCC"); // もっと薄い黄色

		// 空行
		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber , 13.5);
		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber , 13.5);
		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber , 13.5);

		$arg_array["KMidSumRow1"] = $KMidSumRow1;
		$arg_array["KMidSumRow2"] = $KMidSumRow2;
		$arg_array["KMidSumRow3"] = $KMidSumRow3;
		$arg_array["RowNumber"] = $RowNumber;

		return $arg_array;
	}

	////////////////////////////////////////////
	//					  //
	// ++++++++++++ 看護補助者表 ++++++++++++ //
	// 					  //
	////////////////////////////////////////////
	function ShowNurseData2( // 看護職員表
				$excelObj,		// PHP-Excelオブジェクト
				$start_row_number,	// 表を開始する行位置
				$obj,			// 共通クラス
				$data_sub_array,	//看護補助者情報
				$week_array,		// 曜日配列
				$day_cnt,		// 日数
				$arg_array,		// 行位置情報
				$hojo_space_line )	// 看護補助者表、空白行数
	{

		// 追加する空白行にゴミを出さないよう、追加行分の内容をクリア
		if( $hojo_space_line != 0 ){
			$add_line_count = count($data_sub_array) + $hojo_space_line;
			for ( $r=count($data_sub_array); $r<$add_line_count; $r++){
				$data_sub_array[$i]["main_group_name"];
				$data_sub_array[$i]["staff_name"];
				$data_sub_array[$i]["duty_form"];
				$data_sub_array[$i]["other_post_flg"];
				$data_sub_array[$i]["night_duty_flg"];
				for($k=1; $k<=$day_cnt; $k++) {
					$data_sub_array[$i]["duty_time_$k"];
					$data_sub_array[$i]["night_time_$k"];
				}
			}
		}

		$KangoRowTop = $arg_array["KangoRowTop"];
		$DayCntRow   = $KangoRowTop - 4;
		$LaborCntRow = $KangoRowTop - 3;

		$RowNumber = $arg_array["RowNumber"];
		$RowNumber ++;

		$excelObj->SetRowDim($RowNumber , 18.75);
		$excelObj->SetValueJP2("C".$RowNumber,"《看護補助者表》","BOLD VCENTER",$this->MSPgothic ,14); 

		$RowNumber ++;

		$HojoRowTop  = $RowNumber;
		$MidashiRow1 = $HojoRowTop;
		$MidashiRow2 = $MidashiRow1 + 1;
		$MidashiRow3 = $MidashiRow1 + 2;

		// 行高さ設定
		$excelObj->SetRowDim($MidashiRow1,54);
		$excelObj->SetRowDim($MidashiRow2,13.5);
		$excelObj->SetRowDim($MidashiRow3,13.5);

		// 見出し
		$excelObj->SetValueJPwith("C".$MidashiRow1,"番号","BOLD");
		$excelObj->CellMerge("C".$MidashiRow1.":C".$MidashiRow3);
		$excelObj->SetPosH("CENTER");

		$excelObj->CellMerge("C".$MidashiRow1.":C".$MidashiRow3);
		$excelObj->SetPosH("CENTER");

		$excelObj->SetValueJPwith("D".$MidashiRow1,"病棟名","BOLD");
		$excelObj->CellMerge("D".$MidashiRow1.":D".$MidashiRow3);

		$excelObj->SetValueJPwith("E".$MidashiRow1,"氏    名","BOLD");
		$excelObj->CellMerge("E".$MidashiRow1.":E".$MidashiRow3);

		$excelObj->SetValueJPwith("F".$MidashiRow1,"雇用・勤務形態","BOLD");
		$excelObj->CellMerge("F".$MidashiRow1.":H".$MidashiRow3);

		$excelObj->SetValueJPwith("I".$MidashiRow1,"夜勤の有無","BOLD");
		$excelObj->CellMerge("I".$MidashiRow1.":K".$MidashiRow3);

		// まとめて
		$excelObj->SetArea("B".$MidashiRow1.":K".$MidashiRow1);
		$excelObj->SetFont($this->MSPgothic ,12); 
		$excelObj->SetPosV("CENTER");
		$excelObj->SetPosH("CENTER");

		$excelObj->SetValueJPwith("L".$MidashiRow1,"夜勤従事者数への計上","WRAP");
		$excelObj->SetFont($this->MSPgothic ,10); 
		$excelObj->CellMerge("L".$MidashiRow1.":L".$MidashiRow3);
		$excelObj->SetPosV("CENTER");
		$excelObj->SetPosH("CENTER");

		$excelObj->CellMerge("M".$MidashiRow1.":M".$MidashiRow3);

		// 「日付別の勤務時間数」見出しのセルマージ領域。1ヶ月が31未満でも31日分のセルを用意する
		$excelObj->CellMerge2("N".$MidashiRow1.":AR".$MidashiRow1,"thin","bottom","");
		$excelObj->SetValueJP2("N".$MidashiRow1,"日付別の勤務時間数","BOLD",$this->MSPgothic ,12); 
		$excelObj->SetPosV("CENTER");
		
		// 日数
		for($c=1; $c<=$day_cnt; $c++){
			$cellArea=$this->excelColumnName[$c+12].$MidashiRow2;
			$excelObj->SetValueJPwith($cellArea , $c."日" , "");
		}
		// 曜日
		for($c=1; $c<=$day_cnt; $c++){
			$cellArea=$this->excelColumnName[$c+12]."".$MidashiRow3;
			$excelObj->SetValueJPwith($cellArea ,$week_array[$c]["name"]."曜" , "");
		}

		$excelObj->CellMerge("AS".$MidashiRow1.":AS".$MidashiRow3);
		$excelObj->SetValueJP2("AS".$MidashiRow1,"月勤務時間数（延べ時間数）","WRAP",$this->MSPgothic ,10); 
		$excelObj->SetPosV("CENTER");

		$excelObj->CellMerge("AT".$MidashiRow1.":AT".$MidashiRow3);
		$excelObj->SetValueJP2("AT".$MidashiRow1,"（再掲）夜勤専従者及び月16時間以下の者の夜勤時間数","WRAP",$this->MSPgothic ,9); 
		$excelObj->SetPosV("CENTER");

		$excelObj->CellMerge("AU".$MidashiRow1.":AU".$MidashiRow3);
		$excelObj->SetValueJP2("AU".$MidashiRow1,"常勤換算配置数","WRAP",$this->MSPgothic ,11); 
		$excelObj->SetPosV("CENTER");

		// データ行
		$RowNumber = $HojoRowTop + 3;  // 行位置、一行ずつカウントして行く。2段表示のときは$RowNextを下段位置とする
		$HojoRowDataTop = $RowNumber ; // 看護職員表データ行開始のEXCEL行番号

		// 縦集計計算式用配列、リサイクル。初期化
		for($i=2; $i<51; $i++){ //0:A列、1:B列、・・・ 25:Z列・・・AZ列まで対応
			$VSumU[$i] = "=";
			$VSumL[$i] = "=";
		}
		if ( $hojo_space_line != 0 ){
			$row_max = $add_line_count;
		}else{
			$row_max = count($data_sub_array);
		}

		for($i=0; $i<$row_max; $i++) {
			$no = $i + 1;
			$RowNext = $RowNumber + 1;

			// 番号
			$excelObj->CellMerge("C".$RowNumber.":C".$RowNext);
			$excelObj->SetValuewith("C".$RowNumber, $no,"");

			// 病棟名
			$excelObj->CellMerge("D".$RowNumber.":D".$RowNext);
			$excelObj->SetValueJPwith("D".$RowNumber, $data_sub_array[$i]["main_group_name"],"");

			// 氏名
			$excelObj->CellMerge("E".$RowNumber.":E".$RowNext);
			$excelObj->SetValueJPwith("E".$RowNumber, $data_sub_array[$i]["staff_name"],"");

			// 明細行内の見出し   フォントの設定が細かくて泣ける・・・
			$excelObj->SetValueJPwith("F".$RowNumber, "常勤","");
			$excelObj->SetValueJPwith("G".$RowNumber, "非常勤","");
			$excelObj->SetValueJPwith("H".$RowNumber, "他部署兼務","");
			$CellArea="F".$RowNumber.":H".$RowNumber;
			$excelObj->SetArea($CellArea);
			$excelObj->SetFont($this->MSPgothic ,8); 
			$excelObj->SetValueJPwith("I".$RowNumber, "有","");
			$excelObj->SetValueJPwith("J".$RowNumber, "無","");
			$excelObj->SetValueJPwith("K".$RowNumber, "夜専","");
			$CellArea="I".$RowNumber.":K".$RowNumber;
			$excelObj->SetArea($CellArea);
			$excelObj->SetFont($this->MSPgothic ,10); 
			$excelObj->SetValueJP2("L".$RowNumber, "夜勤従事者","",$this->MSPgothic ,8); 
			$excelObj->SetValueJP2("M".$RowNumber, "日勤時間数","",$this->MSPgothic ,10); 
			$excelObj->SetValueJP2("M".$RowNext  , "夜勤時間数","",$this->MSPgothic ,10); 

			// データ
			// duty_shift_report1_common_class.phpから流用
			//常勤／非常勤／他部署兼務
			//夜勤：有り／夜勤：無し／夜専
			$wk1 = '0';	// 常勤
			$wk2 = '0';	// 非常勤
			$wk3 = '0';	// 他部署兼務
//			$wk4 = "　";	// 夜勤有り→Excel計算式で求める
//			$wk5 = "　";	// 夜勤無し→Excel計算式で求める
			$wk6 = '0';	// 夜専
			if ($data_sub_array[$i]["duty_form"] == "1") { $wk1 = "1"; }
			if ($data_sub_array[$i]["duty_form"] == "2") { $wk2 = "1"; }
			if ($data_sub_array[$i]["other_post_flg"] == "1") { $wk3 = "1"; }
			//夜専以外
			if ($data_sub_array[$i]["night_duty_flg"] != "2") {
//				//16時間超を夜勤有りとする→Excel計算式で求める
//				if (($wk_times_1 + $wk_times_2) > 16) {
//					$wk4 = "1";
//				} else {
//					$wk5 = "1";
//				}
			} else {
				//夜専
				$wk6 = "1";
			}
			$excelObj->SetValuewith("F".$RowNext, $wk1,"");
			$excelObj->SetValuewith("G".$RowNext, $wk2,"");
			$excelObj->SetValuewith("H".$RowNext, $wk3,"");
			$wk0 = $wk1 + $wk2 + $wk3;
			if( $wk0 == 0 ){
				$excelObj->SetArea("F".$RowNext.":H".$RowNext);
				$excelObj->SetCharColor("FFC0C0C0");
			}

			// =IF(AS254>16,1,0)
			$formulae = "=IF(AS".$RowNext.">16,1,0)" ;
			$excelObj->SetValuewith("I".$RowNext, $formulae,"");

			// =IF(AS254<=16,1,0)
			$formulae = "=IF(AS".$RowNext."<=16,1,0)" ;
			$excelObj->SetValuewith("J".$RowNext, $formulae,"");

			$excelObj->SetValuewith("K".$RowNext, $wk6,"");
			$CellArea="F".$RowNext.":M".$RowNext;
			$excelObj->SetArea($CellArea);
			$excelObj->SetFont($this->MSPgothic ,10); 

			// 縦集計の計算式
			$VSumL[5]  = $VSumL[5]."F".$RowNext."+";
			$VSumL[6]  = $VSumL[6]."G".$RowNext."+";
			$VSumL[7]  = $VSumL[7]."H".$RowNext."+";
			$VSumL[8]  = $VSumL[8]."I".$RowNext."+";
			$VSumL[9]  = $VSumL[9]."J".$RowNext."+";
			$VSumL[10] = $VSumL[10]."K".$RowNext."+";
			$VSumL[11] = $VSumL[11]."L".$RowNext."+";

			//夜勤従事者数
			//         "=IF(OR(K39=1          , J39          =1), 0, IF(AND(F39          =1, G39          =0, H39          =0), 1, MIN(1,(AS38+            AS39          )/($M$32*$M$31/7))))";
			$formulae = "=IF(OR(K".$RowNext."=1, J".$RowNext."=1), 0, IF(AND(F".$RowNext."=1, G".$RowNext."=0, H".$RowNext."=0), 1, MIN(1,(AS".$RowNumber."+AS".$RowNext.")/(\$M".$LaborCntRow."*\$M".$DayCntRow."/7))))";
			$excelObj->SetValuewith("L".$RowNext , $formulae, "");
			$excelObj->SetFormatStyle("[Red][<0.00](0.00) ;[Black][>=0]0.00 "); // L列のみ
			$excelObj->SetFont($this->MSPgothic ,11); 

			// 日付別の勤務時間数（日勤時間数、夜勤時間数）Excelなので1回のdayループで両方処理する
			for($k=1; $k<=$day_cnt; $k++) {
				$colPos = $k + 12; // N列から
				//////////////
				// 日勤時間 //
				//////////////
				$wk = $data_sub_array[$i]["duty_time_$k"];
				if ($wk <= 0) {
					$wk = "　";
				}
				$CellArea = $this->excelColumnName[$colPos].$RowNumber; // 上段
				$excelObj->SetValuewith($CellArea , $wk, "");
				$VSumU[$colPos] = $VSumU[$colPos].$CellArea."+"; // 縦集計用の計算式

				//////////////
				// 夜勤時間 //
				//////////////
				$wk = $data_sub_array[$i]["night_time_$k"];
				if ($wk <= 0) {
					$wk = "　";
				}
				$CellArea = $this->excelColumnName[$colPos].$RowNext; // 下段
				$excelObj->SetValuewith($CellArea , $wk, "");
				$VSumL[$colPos] = $VSumL[$colPos].$CellArea."+"; // 縦集計用の計算式
			}
			////////////
			// 行集計 //
			////////////
			// AS列
			$CellArea = "AS".$RowNumber; // 上段
			$formulae = "=SUM(N".$RowNumber.":AR".$RowNumber.")"; // =SUM(N38:AR38)
			$excelObj->SetValuewith($CellArea , $formulae, "");

			$CellArea = "AS".$RowNext; // 下段
			$formulae = "=SUM(N".$RowNext.":AR".$RowNext.")"; // =SUM(N39:AR39)
			$excelObj->SetValuewith($CellArea , $formulae, "");

			// 縦集計計算式
			$VSumU[44] = $VSumU[44]."AS".$RowNumber."+";
			$VSumL[44] = $VSumL[44]."AS".$RowNext."+";

			// AT列
			$CellArea = "AT".$RowNext; // 下段
			// =IF(OR(K39=1,AS39<=16),AS39,0)
			//           =IF(OR(K39          =1,AS39          <=16),AS39          ,0)
			$formulae = "=IF(OR(K".$RowNext."=1,AS".$RowNext."<=16),AS".$RowNext.",0)"; // =IF(OR(K39=1,AS39<=16),AS39,0)
			$excelObj->SetValuewith($CellArea , $formulae, "");
			// 縦集計計算式
			$VSumL[45] = $VSumL[45]."AT".$RowNext."+";

			// AU列上
			// 計算式無し

			// AU列下
 			// =IF(AND(F254=1, G254=0, H254=0),1,MIN(1,ROUNDDOWN((AS253+AS254)/($M$32*$M$31/7),2)))
			$formulae = "=IF(AND(F".$RowNext."=1, G".$RowNext."=0, H".$RowNext."=0),1,MIN(1,ROUNDDOWN((AS".$RowNumber."+AS".$RowNext.")/(\$M\$32*\$M\$31/7),2)))";
			$CellArea = "AU".$RowNext;
			$excelObj->SetValueJPwith($CellArea , $formulae, "");

			// 縦集計計算式
			$VSumU[46] = $VSumU[46]."AU".$RowNumber."+";
			$VSumL[46] = $VSumL[46]."AU".$RowNext."+";

			// AY列下
			$CellArea = "AY".$RowNext;
			//           =IF(AND(AS38              =0,AS39            =0), "-",AS39)
			$formulae = "=IF(AND(AS"."$RowNumber"."=0,AS"."$RowNext"."=0),\"-\",AS"."$RowNext".")";
			$excelObj->SetValueJPwith($CellArea , $formulae, "");

			// AZ列下
			$CellArea = "AZ".$RowNext;
			//           =IF(AND(AS38	       =0,AS39		  =0), "-", IF(K39	      =1,AS39,		   "-"))
			$formulae = "=IF(AND(AS"."$RowNumber"."=0,AS"."$RowNext"."=0),\"-\",IF(K"."$RowNext"."=1,AS"."$RowNext".",\"-\"))";
			$excelObj->SetValueJPwith($CellArea , $formulae, "");

			// 行間の横線
			$CellArea = "C".$RowNumber.":AU".$RowNumber; // 細い実線
			$excelObj->SetArea($CellArea);
			$excelObj->SetBorder2("thin","","top");
			$CellArea = "F".$RowNext.":AU".$RowNext; // 細い破線
			$excelObj->SetArea($CellArea);
			$excelObj->SetBorder2("dotted","","top");
			$RowNumber += 2;
		}
		//////////////////
		//    列集計    //
		//////////////////
		$HojoRowSmallSum = $RowNumber;
		$RowNext = $RowNumber + 1;
		// 左見出し
		$excelObj->CellMerge("C".$RowNumber.":C".$RowNext);
		$CellArea = "C".$RowNumber;
		$excelObj->SetValueJPwith($CellArea, "小計" ,"");
		$excelObj->SetPosH("LEFT");

		$excelObj->CellMerge("D".$RowNumber.":D".$RowNext);

		$excelObj->CellMerge("E".$RowNumber.":E".$RowNext);
		$name = count($data_array) ;
		$name = sprintf( '%003d' , $name );
		$name = "001 〜 ".$name;
		$CellArea = "E".$RowNumber;
		$excelObj->SetValueJPwith($CellArea, $name, "");

		// 小計・明細行内の見出し
		$excelObj->SetValueJPwith("F".$RowNumber, "常勤","");
		$excelObj->SetValueJPwith("G".$RowNumber, "非常勤","");
		$excelObj->SetValueJPwith("H".$RowNumber, "他部署兼務","");
		$CellArea="F".$RowNumber.":H".$RowNumber;
		$excelObj->SetArea($CellArea);
		$excelObj->SetFont($this->MSPgothic ,8); 
		$excelObj->SetValueJPwith("I".$RowNumber, "有","");
		$excelObj->SetValueJPwith("J".$RowNumber, "無","");
		$excelObj->SetValueJPwith("K".$RowNumber, "夜専","");
		$CellArea="I".$RowNumber.":K".$RowNumber;
		$excelObj->SetArea($CellArea);
		$excelObj->SetFont($this->MSPgothic ,11); 
		$excelObj->SetValueJP2("L".$RowNumber, "夜勤従事者","",$this->MSPgothic ,8); 
		$excelObj->SetValueJP2("M".$RowNumber, "日勤時間数","",$this->MSPgothic ,10); 
		$excelObj->SetValueJP2("M".$RowNext  , "夜勤時間数","",$this->MSPgothic ,10); 

		for($k=5; $k<=11; $k++) { // F〜L列
			$VSumL[$k] = substr( $VSumL[$k] , 0 , strlen($VSumL[$k]) - 1 ); // 末尾の + を消す
			$CellArea = $this->excelColumnName[$k].$RowNext;
			$excelObj->SetValuewith($CellArea, $VSumL[$k], "");
			$excelObj->SetFont($this->MSPgothic ,11); 
		}
		$excelObj->SetFont($this->MSPgothic ,11); 
		$excelObj->SetFormatStyle("[Red][<0.00](0.00) ;[Black][>=0]0.00 "); // L列のみ
		for($k=1; $k<=$day_cnt; $k++) { // 日毎
			$colPos = $k + 12; // N列から
			// 上段
			$VSumU[$colPos] = substr( $VSumU[$colPos] , 0 , strlen($VSumU[$colPos]) - 1 ); // 末尾の + を消す
			$CellArea = $this->excelColumnName[$colPos].$RowNumber;
			$excelObj->SetValuewith($CellArea, $VSumU[$colPos], "");
			// 下段
			$VSumL[$colPos] = substr( $VSumL[$colPos] , 0 , strlen($VSumL[$colPos]) - 1 ); // 末尾の + を消す
			$CellArea = $this->excelColumnName[$colPos].$RowNext;
			$excelObj->SetValuewith($CellArea, $VSumL[$colPos], "");
		}
		// AS列
		$colPos = 44 ;
		// 上段
		$VSumU[$colPos] = substr( $VSumU[$colPos] , 0 , strlen($VSumU[$colPos]) - 1 ); // 末尾の + を消す
		$CellArea = $this->excelColumnName[$colPos].$RowNumber;
		$excelObj->SetValuewith($CellArea, $VSumU[$colPos], "");
		// 下段
		$VSumL[$colPos] = substr( $VSumL[$colPos] , 0 , strlen($VSumL[$colPos]) - 1 ); // 末尾の + を消す
		$CellArea = $this->excelColumnName[$colPos].$RowNext;
		$excelObj->SetValuewith($CellArea, $VSumL[$colPos], "");
		// AT列、下段だけ
		$colPos = 45 ;
		// 下段
		$VSumL[$colPos] = substr( $VSumL[$colPos] , 0 , strlen($VSumL[$colPos]) - 1 ); // 末尾の + を消す
		$CellArea = $this->excelColumnName[$colPos].$RowNext;
		$excelObj->SetValuewith($CellArea, $VSumL[$colPos], "");

		// AU列
		$colPos = 46 ;
		// 上段
		$VSumU[$colPos] = substr( $VSumU[$colPos] , 0 , strlen($VSumU[$colPos]) - 1 ); // 末尾の + を消す
		$CellArea = $this->excelColumnName[$colPos].$RowNumber;
		$excelObj->SetValuewith($CellArea, $VSumU[$colPos], "");
		// 下段
		$VSumL[$colPos] = substr( $VSumL[$colPos] , 0 , strlen($VSumL[$colPos]) - 1 ); // 末尾の + を消す
		$CellArea = $this->excelColumnName[$colPos].$RowNext;
		$excelObj->SetValuewith($CellArea, $VSumL[$colPos], "");
		// 行間の横線
		$CellArea = "C".$RowNumber.":AU".$RowNumber; // 細い実線
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("thin","","top");
		$CellArea = "F".$RowNext.":AU".$RowNext; // 細い破線
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("dotted","","top");
		// 隠し行 AY-AZ
		$excelObj->SetValueJP2("AY".$RowNext, "小計","",$this->MSPgothic ,11);
		$excelObj->SetValueJP2("AZ".$RowNext, "小計","",$this->MSPgothic ,11);

		$RowNumber += 2;
		$RowNext = $RowNumber + 1;
		// 列集計（小計）おわり。

		$KangoRowMax = $RowNumber - 1; // 看護補助者表終了時のExcel行番号
		$HojoRowSum = $RowNumber;

		// 「計」行開始
		$KangoRowMax += 2;

		// フォントB-D列
		$CellArea = "B".$HojoRowTop.":D".$KangoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetFont($this->MSPgothic ,11); 
		// フォントE列
		$CellArea = "E".$HojoRowTop.":E".$KangoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetFont($this->MSPgothic ,12); 
		// フォントM列
		$CellArea = "M".$HojoRowTop.":M".$KangoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetFont($this->MSPgothic ,10); 
		// フォント、スタイル N-AX列
		$CellArea = "N".$HojoRowDataTop.":AX".$KangoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetFont($this->MSPgothic ,11); 
		$excelObj->SetFormatStyle("[Red][<0](0.00) ;[Black][>=0]0.00 ");

		// 色指定 I-J列
		$CellArea = "I".$HojoRowDataTop.":J".$KangoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBackColor("CCFFCC"); // 薄い緑
		// 色指定 L列
		$CellArea = "L".$HojoRowDataTop.":L".$KangoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBackColor("CCFFCC"); // 薄い緑
		// 色指定 AS-AX列
		$CellArea = "AS".$HojoRowDataTop.":AU".$KangoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBackColor("CCFFCC"); // 薄い緑

		// 表示位置・縦横
		$CellArea = "C".$HojoRowDataTop.":C".$KangoRowMax; // 右よりはデータ行。見出しは中央
		$excelObj->SetArea($CellArea);
		$excelObj->SetPosH("RIGHT");

		$CellArea = "D".$HojoRowTop.":D".$KangoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetPosH("CENTER");

		$CellArea = "E".$HojoRowTop.":E".$KangoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetPosH("LEFT");

		// 「小計」行の横罫線引きなおし、黄色
		$beforRowU = $RowNumber-2;
		$beforRowL = $RowNext-2;
		$CellArea = "C".$beforRowU.":AU".$beforRowL;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("double","","top");
		$excelObj->SetBorder2("double","","bottom");
		$excelObj->SetBackColor("FFFF99"); // 薄い黄色

		// 「計」開始
		$excelObj->CellMerge("C".$RowNumber.":C".$RowNext);
		$excelObj->SetValueJP2("C".$RowNumber, "計" ,"LEFT",$this->MSPgothic ,11); 

		$excelObj->CellMerge("D".$RowNumber.":D".$RowNext);

		$excelObj->CellMerge("E".$RowNumber.":E".$RowNext);

		// 明細行内の見出し
		$excelObj->SetValueJPwith("F".$RowNumber, "常勤","");
		$excelObj->SetValueJPwith("G".$RowNumber, "非常勤","");
		$excelObj->SetValueJPwith("H".$RowNumber, "他部署兼務","");
		$excelObj->SetValueJPwith("I".$RowNumber, "有","");
		$excelObj->SetValueJPwith("J".$RowNumber, "無","");
		$excelObj->SetValueJPwith("K".$RowNumber, "夜専","");
		$excelObj->SetValueJPwith("L".$RowNumber, "夜勤従事者","");
		$CellArea="F".$RowNumber.":L".$RowNumber;
		$excelObj->SetArea($CellArea);
		$excelObj->SetFont($this->MSPgothic ,8); 
		$excelObj->SetValueJP2("M".$RowNumber, "日勤時間数","",$this->MSPgothic ,10); 
		$excelObj->SetValueJP2("M".$RowNext, "夜勤時間数","",$this->MSPgothic ,10); 
		$beforRowU = $RowNumber-2;
		$beforRowL = $RowNext-2;
		for($k=5; $k<=11; $k++) { // F〜L列
			$colPos=$k;
			$CellArea = $this->excelColumnName[$colPos].$RowNext;
			$formulae = "=".$this->excelColumnName[$colPos].$beforRowL;
			$excelObj->SetValuewith($CellArea, $formulae, "");
			$excelObj->SetFont($this->MSPgothic ,11); 
		}
		$excelObj->SetFormatStyle("[Red][<0.00](0.00) ;[Black][>=0]0.00 "); // L列
		for($k=1; $k<=$day_cnt; $k++) { // 日毎
			$colPos = $k + 12; // N列から
			// 上段
			$CellArea = $this->excelColumnName[$colPos].$RowNumber;
			$formulae = "=".$this->excelColumnName[$colPos].$beforRowU;
			$excelObj->SetValuewith($CellArea, $formulae, "");
			// 下段
			$CellArea = $this->excelColumnName[$colPos].$RowNext;
			$formulae = "=".$this->excelColumnName[$colPos].$beforRowL;
			$excelObj->SetValuewith($CellArea, $formulae, "");
		}
		// AS列
		// 上段
		$colPos = 44 ;
		$CellArea = $this->excelColumnName[$colPos].$RowNumber;
		$formulae = "=".$this->excelColumnName[$colPos].$beforRowU;
		$excelObj->SetValuewith($CellArea, $formulae, "");
		// 下段
		$CellArea = $this->excelColumnName[$colPos].$RowNext;
		$formulae = "=".$this->excelColumnName[$colPos].$beforRowL;
		$excelObj->SetValuewith($CellArea, $formulae, "");
		// AT列
		$colPos = 45 ;
		// 下段のみ
		$CellArea = $this->excelColumnName[$colPos].$RowNext;
		$formulae = "=".$this->excelColumnName[$colPos].$beforRowL;
		$excelObj->SetValuewith($CellArea, $formulae, "");
		// AU列
		// 上段
		$colPos = 46 ;
		$CellArea = $this->excelColumnName[$colPos].$RowNumber;
		$formulae = "=".$this->excelColumnName[$colPos].$beforRowU;
		$excelObj->SetValuewith($CellArea, $formulae, "");
		// 下段
		$CellArea = $this->excelColumnName[$colPos].$RowNext;
		$formulae = "=".$this->excelColumnName[$colPos].$beforRowL;
		$excelObj->SetValuewith($CellArea, $formulae, "");

		// AY-AZ(隠し列)
		$CellArea="AY".$RowNext;
		$excelObj->SetArea($CellArea);
		$excelObj->SetFont($this->MSPgothic ,11); 
		$excelObj->SetValueJPwith($CellArea, "計","");

		$CellArea="AZ".$RowNext;
		$excelObj->SetArea($CellArea);
		$excelObj->SetFont($this->MSPgothic ,11); 
		$excelObj->SetValueJPwith($CellArea, "計","");

		// 「計」行をまとめて設定。横線など
		$CellArea = "C".$RowNumber.":AR".$RowNext;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBackColor("CCFFCC"); // 薄い緑

		$CellArea = "F".$RowNumber.":AU".$RowNumber;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("dotted","","bottom");

		$CellArea = "C".$HojoRowTop.":AU".$RowNext;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("thin","","left");
		$excelObj->SetBorder2("medium","","bottom");
		$excelObj->SetBorder2("hair","","vertical"); // new

		// やや太い枠で全体を囲み、左端(C列の左)は細い縦線を引きなおす
		$CellArea = "C".$HojoRowTop.":AU".$RowNext;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("medium","","outline");
		$excelObj->SetBorder2("thin","","left");

		$CellArea = "AU".$HojoRowTop.":AU".$RowNext;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("medium","","left");

		// AY:AZの背景色、灰色
		$wk = $HojoRowTop + 2;
		$CellArea = "AY".$wk.":AZ".$KangoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBackColor("C0C0C0"); // 灰色

		// 縮小して全体を表示する指定
		$CellArea = "F".$HojoRowDataTop.":AX".$KangoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetShrinkToFit();

		// 全体のセル内縦位置をCENTERにする
		$CellArea = "C".$HojoRowDataTop.":AX".$KangoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetPosV("CENTER");

		$H_KEI_RowU = $RowNumber;
		$H_KEI_RowL = $RowNumber+1;

		$RowNumber++;

		// 空行
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,13.5);

		$arg_array["RowNumber"] = $RowNumber;
		$arg_array["HojoRowDataTop"] = $HojoRowDataTop;
		$arg_array["HojoRowSum"] = $HojoRowSum;

		return $arg_array;
	}
	//////////////////////////////////
	//				//
	//   一般病棟・看護補助者集計   //
	//				//
	//////////////////////////////////
	function ShowNurseSum02( $excelObj , $arg_array ){

		$RowNumber = $arg_array["RowNumber"];
		$KMidSumRow1 = $arg_array["KMidSumRow1"];
		$HojoRowSum = $arg_array["HojoRowSum"];
		$H_KEI_RowU = $HojoRowSum;
		$H_KEI_RowL = $HojoRowSum+1;

		////////////////
		// 中間集計行 //
		////////////////
		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber,13.5);

		// コメント
		$excelObj->SetValueJP2("C".$RowNumber, "※急性期看護補助体制加算・看護補助加算等を届け出る場合", "VCENTER",$this->MSPgothic ,11); 

		// 中間集計１行目
		$RowNumber ++;
		$HMidSumRow1 = $RowNumber;
		$HMidSumRow2 = $HMidSumRow1 + 1;
		$excelObj->SetRowDim($RowNumber,18.75);

		$excelObj->CellMerge("C".$RowNumber.":J".$RowNumber);
		$excelObj->SetBorder2("medium","","outline");

		$excelObj->SetValueJP2("C".$RowNumber, "看護補助者のみの月延べ勤務時間数の計〔F〕", "BOLD VCENTER",$this->MSPgothic ,14); 

		$excelObj->CellMerge("K".$RowNumber.":L".$RowNumber);
		$excelObj->SetBorder2("medium","","outline");

		$CellArea = "K".$RowNumber;
		// =AS455+AS456
		$formulae = "=AS".$H_KEI_RowU."+AS".$H_KEI_RowL;
		$excelObj->SetValuewith($CellArea, $formulae, "BOLD");
		$excelObj->SetFont($this->MSPgothic ,12); 
		$excelObj->SetFormatStyle("0.00");
		$excelObj->SetPosV("CENTER");
		$excelObj->SetBackColor("FFFFCC"); // もっと薄い黄色

		$excelObj->CellMerge("M".$RowNumber.":AB".$RowNumber);
		$excelObj->SetBorder2("medium","","outline");

		$excelObj->SetValueJP2("M".$RowNumber, "みなし看護補助者の月延べ勤務時間数の計〔G〕　〔C - 1日看護配置数×８×日数〕", "BOLD VCENTER",$this->MSPgothic ,14); 

		$excelObj->CellMerge("AC".$RowNumber.":AD".$RowNumber);
		$excelObj->SetBorder2("medium","","outline");

		$CellArea = "AC".$RowNumber;
		// =MAX(R243-X15, 0)
		$formulae = "=MAX(R".$KMidSumRow1."-X15, 0)";
		$excelObj->SetValuewith($CellArea, $formulae, "BOLD");
		$excelObj->SetFont($this->MSPgothic ,12); 
		$excelObj->SetFormatStyle("0.00");
		$excelObj->SetPosV("CENTER");
		$excelObj->SetBackColor("FFFFCC"); // もっと薄い黄色

		// 中間集計２行目
		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber,18.75);

		$excelObj->CellMerge("C".$RowNumber.":J".$RowNumber);
		$excelObj->SetBorder2("medium","","outline");

		$excelObj->SetValueJP2("C".$RowNumber, "1日看護補助配置数　〔(A ／届出区分の数)×３〕", "BOLD VCENTER",$this->MSPgothic ,14); 

		$excelObj->CellMerge("K".$RowNumber.":L".$RowNumber);
		$excelObj->SetBorder2("medium","","outline");

		$CellArea = "K".$RowNumber;
		// =J29
		$formulae = "=J29"; // <=★★要注意★★29の位置が可変である
		$excelObj->SetValuewith($CellArea, $formulae, "BOLD");
		$excelObj->SetFont($this->MSPgothic ,12); 
		$excelObj->SetFormatStyle("0.00");
		$excelObj->SetPosV("CENTER");
		$excelObj->SetBackColor("FFFFCC"); // もっと薄い黄色

		$excelObj->CellMerge("M".$RowNumber.":AB".$RowNumber);
		$excelObj->SetBorder2("medium","","outline");

		$excelObj->SetValueJP2("M".$RowNumber, "月平均1日当たり看護補助者配置数　〔(F+G) ／ (日数×８) 〕", "BOLD VCENTER",$this->MSPgothic ,14); 

		$excelObj->CellMerge("AC".$RowNumber.":AD".$RowNumber);
		$excelObj->SetBorder2("medium","","outline");

		$CellArea = "AC".$RowNumber;
		// =ROUNDDOWN((K459+AC459)/(M31*8),1)
		$formulae = "=ROUNDDOWN((K".$HMidSumRow1."+AC".$HMidSumRow1.")/(M31*8),1)"; // <=★★要注意★★31の位置が可変である
		$excelObj->SetValuewith($CellArea, $formulae, "BOLD");
		$excelObj->SetFont($this->MSPgothic ,12); 
		$excelObj->SetFormatStyle("0.00");
		$excelObj->SetPosV("CENTER");
		$excelObj->SetBackColor("FFFFCC"); // もっと薄い黄色
		// 中間集計　おわり。

		// 空行3行
		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber,13.5);
		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber,13.5);
		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber,13.5);

		$arg_array["HMidSumRow1"] = $HMidSumRow1;
		$arg_array["HMidSumRow2"] = $HMidSumRow2;
		$arg_array["HMidSumRow3"] = 0;
		$arg_array["RowNumber"] = $RowNumber;

		return $arg_array;
	}

	//////////////////////////////////////////////////////
	//						    //
	// 月総夜勤時間別の看護職員数（夜勤従事者数）の分布 //
	//						    //
	// 一般病棟・療養病棟１、２、共通                   //
	//						    //
	//////////////////////////////////////////////////////
	function ShowNurseSum3( $excelObj , $arg_array ){

		$RowNumber = $arg_array["RowNumber"];
		$KMidSumRow1 = $arg_array["KMidSumRow1"];
		$KangoRowDataTop = $arg_array["KangoRowDataTop"];
		$KangoRowSum = $arg_array["KangoRowSum"];
		$HojoRowDataTop = $arg_array["HojoRowDataTop"];
		$HojoRowSum = $arg_array["HojoRowSum"];

		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber,18.75);

		$excelObj->SetValueJP2("L".$RowNumber, "参考", "BOLD HCENTER VCENTER",$this->MSPgothic ,12); 
		$excelObj->SetBorder2("medium","","outline");

		$excelObj->SetValueJP2("M".$RowNumber, "月総夜勤時間別の看護職員数（夜勤従事者数）の分布　　…「入院基本料等に関する実施状況報告書」用", "BOLD UNDER",$this->MSPgothic ,14); 

		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber,17.25);

		$excelObj->SetValueJP2("M".$RowNumber, "※夜勤専従者数は再掲として（　）に人数を記入のこと", "BOLD",$this->MSPmincho ,10); 

		// 空行
		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber,13.5);

		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber,21);

		// 行が前後するので要注意
		// L列〜AJ列のセル結合、3表まとめて処理
		for($k=11; $k<37; $k+=2){
			// 上下
			for($ss=0;$ss<3;$ss++){
				$stp = $ss * 4;
				$rwk = $RowNumber + 1 + $stp;
				$CellArea1 = $this->excelColumnName[$k].$rwk;
				$CellArea2 = $this->excelColumnName[$k+1].$rwk;
				$excelObj->CellMerge($CellArea1.":".$CellArea2);
			}
		}

		$excelObj->SetValueJP2("L".$RowNumber, "【看護職員】", "BOLD VCENTER",$this->MSPgothic ,11); 

		$RowNumber ++;  // 見出し行

		// L列〜AJ列の見出し設定、3表まとめてつける
		for($ss=0;$ss<3;$ss++){
			$stp = $ss * 4;
			$rwk = $RowNumber + $stp;
			$excelObj->SetValueJPwith( "L".$rwk, "夜勤時間数", "");
			$excelObj->SetValueJPwith( "N".$rwk, "16時間未満", "");
			$excelObj->SetValueJPwith( "P".$rwk, "16〜32時間", "");
			$excelObj->SetValueJPwith( "R".$rwk, "32〜48時間", "");
			$excelObj->SetValueJPwith( "T".$rwk, "48〜64時間", "");
			$excelObj->SetValueJPwith( "V".$rwk, "64〜72時間", "");
			$excelObj->SetValueJPwith( "X".$rwk, "72〜80時間", "");
			$excelObj->SetValueJPwith( "Z".$rwk, "80〜88時間", "");
			$excelObj->SetValueJPwith("AB".$rwk, "88〜96時間", "");
			$excelObj->SetValueJPwith("AD".$rwk, "96〜112時間", "");
			$excelObj->SetValueJPwith("AF".$rwk, "112〜144時間", "");
			$excelObj->SetValueJPwith("AH".$rwk, "144〜152時間", "");
			$excelObj->SetValueJPwith("AJ".$rwk, "152時間以上", "");

			$excelObj->SetArea("L".$rwk.":AJ".$rwk);
			$excelObj->SetPosV("CENTER"); 
			$excelObj->SetPosH("CENTER"); 
			$excelObj->SetFont($this->MSPgothic ,11); 
			$excelObj->SetShrinkToFit();
		}

		$RowNumber ++;
		$GSumRow1 = $RowNumber;
		$excelObj->SetRowDim($RowNumber,21);

		$CalcArrayL = array();
		$CalcArrayR = array();

		$excelObj->CellMerge("L".$RowNumber.":M".$RowNumber);

		$excelObj->SetValueJP2("L".$RowNumber, "夜勤従事者数", "VCENTER HCENTER",$this->MSPgothic ,11); 

		$CellArea1= $KangoRowDataTop - 1;
		$CellArea2= $KangoRowSum + 1;

		$CalcArrayL[0] = "=COUNTIF(\$AY$CellArea1:\$AY$CellArea2,\"<16\")";
		$CalcArrayL[1] = "=COUNTIF(\$AY$CellArea1:\$AY$CellArea2,\"<32\")-COUNTIF(\$AY$CellArea1:\$AY$CellArea2,\"<16\")";
		$CalcArrayL[2] = "=COUNTIF(\$AY$CellArea1:\$AY$CellArea2,\"<48\")-COUNTIF(\$AY$CellArea1:\$AY$CellArea2,\"<32\")";
		$CalcArrayL[3] = "=COUNTIF(\$AY$CellArea1:\$AY$CellArea2,\"<64\")-COUNTIF(\$AY$CellArea1:\$AY$CellArea2,\"<48\")";
		$CalcArrayL[4] = "=COUNTIF(\$AY$CellArea1:\$AY$CellArea2,\"<72\")-COUNTIF(\$AY$CellArea1:\$AY$CellArea2,\"<64\")";
		$CalcArrayL[5] = "=COUNTIF(\$AY$CellArea1:\$AY$CellArea2,\"<80\")-COUNTIF(\$AY$CellArea1:\$AY$CellArea2,\"<72\")";
		$CalcArrayL[6] = "=COUNTIF(\$AY$CellArea1:\$AY$CellArea2,\"<88\")-COUNTIF(\$AY$CellArea1:\$AY$CellArea2,\"<80\")";
		$CalcArrayL[7] = "=COUNTIF(\$AY$CellArea1:\$AY$CellArea2,\"<96\")-COUNTIF(\$AY$CellArea1:\$AY$CellArea2,\"<88\")";
		$CalcArrayL[8] = "=COUNTIF(\$AY$CellArea1:\$AY$CellArea2,\"<112\")-COUNTIF(\$AY$CellArea1:\$AY$CellArea2,\"<96\")";
		$CalcArrayL[9] = "=COUNTIF(\$AY$CellArea1:\$AY$CellArea2,\"<144\")-COUNTIF(\$AY$CellArea1:\$AY$CellArea2,\"<112\")";
		$CalcArrayL[10]= "=COUNTIF(\$AY$CellArea1:\$AY$CellArea2,\"<152\")-COUNTIF(\$AY$CellArea1:\$AY$CellArea2,\"<144\")";
		$CalcArrayL[11]= "=COUNTIF(\$AY$CellArea1:\$AY$CellArea2,\">=152\")";

		$CalcArrayR[0] = "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<16\")";
		$CalcArrayR[1] = "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<32\")-COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<16\")";
		$CalcArrayR[2] = "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<48\")-COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<32\")";
		$CalcArrayR[3] = "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<64\")-COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<48\")";
		$CalcArrayR[4] = "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<72\")-COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<64\")";
		$CalcArrayR[5] = "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<80\")-COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<72\")";
		$CalcArrayR[6] = "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<88\")-COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<80\")";
		$CalcArrayR[7] = "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<96\")-COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<88\")";
		$CalcArrayR[8] = "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<112\")-COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<96\")";
		$CalcArrayR[9] = "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<144\")-COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<112\")";
		$CalcArrayR[10]= "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<152\")-COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<144\")";
		$CalcArrayR[11]= "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\">=152\")";

		// N列〜AK列の計算式 【看護職員】
		$c=0;
		for($k=13; $k<=35; $k+=2){
			$CellArea = $this->excelColumnName[$k].$RowNumber;
			$excelObj->SetValuewith($CellArea, $CalcArrayL[$c], "");
			$excelObj->SetFont($this->MSPgothic ,11); 
			$excelObj->SetPosV("CENTER"); 
			$excelObj->SetPosH("RIGHT"); 

			$CellArea = $this->excelColumnName[$k+1].$RowNumber;
			$excelObj->SetValuewith($CellArea, $CalcArrayR[$c], "");
			$excelObj->SetFont($this->MSPgothic ,11); 
			$excelObj->SetPosV("CENTER"); 
			$excelObj->SetPosH("LEFT"); 
			$excelObj->SetFormatStyle("(0)");
			$excelObj->SetBorder2("thin","","right");

			$c++;
		}

		// 空行
		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber,13.5);

		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber,21);

		$excelObj->SetValueJP2("L".$RowNumber, "【看護補助者】", "BOLD VCENTER",$this->MSPgothic ,11); 

		$excelObj->CellMerge("L".$RowNumber.":M".$RowNumber);

		$RowNumber ++; // 見出し行
		$excelObj->SetRowDim($RowNumber,13.5);

		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber,21);
		$GSumRow2 = $RowNumber;

		$excelObj->CellMerge("L".$RowNumber.":M".$RowNumber);

		$excelObj->SetValueJP2("L".$RowNumber, "夜勤従事者数", "VCENTER HCENTER",$this->MSPgothic ,11); 

		$CellArea1= $HojoRowDataTop - 1;
		$CellArea2= $HojoRowSum + 1;

		$CalcArrayL[0] = "=COUNTIF(\$AY$CellArea1:\$AY$CellArea2,\"<16\")";
		$CalcArrayL[1] = "=COUNTIF(\$AY$CellArea1:\$AY$CellArea2,\"<32\")-COUNTIF(\$AY$CellArea1:\$AY$CellArea2,\"<16\")";
		$CalcArrayL[2] = "=COUNTIF(\$AY$CellArea1:\$AY$CellArea2,\"<48\")-COUNTIF(\$AY$CellArea1:\$AY$CellArea2,\"<32\")";
		$CalcArrayL[3] = "=COUNTIF(\$AY$CellArea1:\$AY$CellArea2,\"<64\")-COUNTIF(\$AY$CellArea1:\$AY$CellArea2,\"<48\")";
		$CalcArrayL[4] = "=COUNTIF(\$AY$CellArea1:\$AY$CellArea2,\"<72\")-COUNTIF(\$AY$CellArea1:\$AY$CellArea2,\"<64\")";
		$CalcArrayL[5] = "=COUNTIF(\$AY$CellArea1:\$AY$CellArea2,\"<80\")-COUNTIF(\$AY$CellArea1:\$AY$CellArea2,\"<72\")";
		$CalcArrayL[6] = "=COUNTIF(\$AY$CellArea1:\$AY$CellArea2,\"<88\")-COUNTIF(\$AY$CellArea1:\$AY$CellArea2,\"<80\")";
		$CalcArrayL[7] = "=COUNTIF(\$AY$CellArea1:\$AY$CellArea2,\"<96\")-COUNTIF(\$AY$CellArea1:\$AY$CellArea2,\"<88\")";
		$CalcArrayL[8] = "=COUNTIF(\$AY$CellArea1:\$AY$CellArea2,\"<112\")-COUNTIF(\$AY$CellArea1:\$AY$CellArea2,\"<96\")";
		$CalcArrayL[9] = "=COUNTIF(\$AY$CellArea1:\$AY$CellArea2,\"<144\")-COUNTIF(\$AY$CellArea1:\$AY$CellArea2,\"<112\")";
		$CalcArrayL[10]= "=COUNTIF(\$AY$CellArea1:\$AY$CellArea2,\"<152\")-COUNTIF(\$AY$CellArea1:\$AY$CellArea2,\"<144\")";
		$CalcArrayL[11]= "=COUNTIF(\$AY$CellArea1:\$AY$CellArea2,\">=152\")";

		$CalcArrayR[0] = "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<16\")";
		$CalcArrayR[1] = "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<32\")-COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<16\")";
		$CalcArrayR[2] = "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<48\")-COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<32\")";
		$CalcArrayR[3] = "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<64\")-COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<48\")";
		$CalcArrayR[4] = "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<72\")-COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<64\")";
		$CalcArrayR[5] = "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<80\")-COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<72\")";
		$CalcArrayR[6] = "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<88\")-COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<80\")";
		$CalcArrayR[7] = "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<96\")-COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<88\")";
		$CalcArrayR[8] = "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<112\")-COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<96\")";
		$CalcArrayR[9] = "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<144\")-COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<112\")";
		$CalcArrayR[10]= "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<152\")-COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<144\")";
		$CalcArrayR[11]= "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\">=152\")";

		// N列〜AK列の計算式【看護補助者】
		$c=0;
		for($k=13; $k<=35; $k+=2){
			$CellArea = $this->excelColumnName[$k].$RowNumber;
			$excelObj->SetValuewith($CellArea, $CalcArrayL[$c], "");
			$excelObj->SetFont($this->MSPgothic ,11); 
			$excelObj->SetPosV("CENTER"); 
			$excelObj->SetPosH("RIGHT"); 

			$CellArea = $this->excelColumnName[$k+1].$RowNumber;
			$excelObj->SetValuewith($CellArea, $CalcArrayR[$c], "");
			$excelObj->SetFont($this->MSPgothic ,11); 
			$excelObj->SetPosV("CENTER"); 
			$excelObj->SetPosH("LEFT"); 
			$excelObj->SetFormatStyle("(0)");
			$excelObj->SetBorder2("thin","","right");

			$c++;
		}
		// 空行
		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber,13.5);

		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber,21);

		$excelObj->SetValueJP2("L".$RowNumber, "【看護職員＋看護補助者】", "BOLD VCENTER",$this->MSPgothic ,11); 

		$excelObj->CellMerge("L".$RowNumber.":M".$RowNumber);

		$RowNumber ++; // 見出し行
		$excelObj->SetRowDim($RowNumber,19);

		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber,21);
		$GSumRow3 = $RowNumber;

		$excelObj->CellMerge("L".$RowNumber.":M".$RowNumber);

		$excelObj->SetValueJP2("L".$RowNumber, "夜勤従事者数", "VCENTER HCENTER",$this->MSPgothic ,11); 

		// N列〜AK列の計算式【看護職員＋看護補助者】
		$c=0;
		for($k=13; $k<=35; $k+=2){
			$CellArea = $this->excelColumnName[$k].$RowNumber;
			$formulae = "=".$this->excelColumnName[$k].$GSumRow1."+".$this->excelColumnName[$k].$GSumRow2;
			$excelObj->SetValuewith($CellArea, $formulae, "");
			$excelObj->SetFont($this->MSPgothic ,11); 
			$excelObj->SetPosV("CENTER"); 
			$excelObj->SetPosH("RIGHT"); 

			$wk=$k+1;
			$CellArea = $this->excelColumnName[$wk].$RowNumber;
			$formulae = "=".$this->excelColumnName[$wk].$GSumRow1."+".$this->excelColumnName[$wk].$GSumRow2;
			$excelObj->SetValuewith($CellArea, $formulae, "");
			$excelObj->SetFont($this->MSPgothic ,11); 
			$excelObj->SetPosV("CENTER"); 
			$excelObj->SetPosH("LEFT"); 
			$excelObj->SetFormatStyle("(0)");
			$excelObj->SetBorder2("thin","","right");

			$c++;
		}

		// 罫線
		$a = $GSumRow1 - 1;
		$b = $GSumRow1;
		$CellArea = "L".$a.":AK".$b;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("medium","","outline");
//		$excelObj->SetBorder2("thin","","vertical");
		$CellArea = "L".$b.":AK".$b;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("thin","","top");
		$excelObj->SetBackColor("CCFFCC"); // 薄い緑
		$CellArea = "L".$a.":M".$b;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("double","","right");
		$excelObj->SetBackColorCancel(); // 色外す

		$a = $GSumRow2 - 1;
		$b = $GSumRow2;
		$CellArea = "L".$a.":AK".$b;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("medium","","outline");
//		$excelObj->SetBorder2("thin","","vertical");
		$CellArea = "L".$b.":AK".$b;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBackColor("CCFFCC"); // 薄い緑
		$excelObj->SetBorder2("thin","","top");
		$CellArea = "L".$a.":M".$b;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("double","","right");
		$excelObj->SetBackColorCancel(); // 色外す

		$a = $GSumRow3 - 1;
		$b = $GSumRow3;
		$CellArea = "L".$a.":AK".$b;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("medium","","outline");
//		$excelObj->SetBorder2("thin","","vertical");
		$CellArea = "L".$b.":AK".$b;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBackColor("CCFFCC"); // 薄い緑
		$excelObj->SetBorder2("thin","","top");
		$CellArea = "L".$a.":M".$b;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("double","","right");
		$excelObj->SetBackColorCancel(); // 色外す

		$arg_array["GSumRow3"] = $GSumRow3;
		return $arg_array;
	}

	/////////////////////////////////////////////////////
	//						   //
	// 一般病棟 入院基本料に係る届出書添付書類(様式９) //
	//						   //
	/////////////////////////////////////////////////////
	function ShowData_0(
			$excelObj,		// PHP-Excelオブジェクト
			$obj,			// 共通クラス
//			$excel_flg,		//ＥＸＣＥＬ表示フラグ（１：ＥＸＣＥＬ表示）
//			$recomputation_flg,	//１ヵ月計算４週計算フラグ（１：１ヵ月計算、２：４週計算）
//			$data_array,		//看護師／准看護師情報
//			$data_sub_array,	//看護補助者情報
//			$hospital_name,		//保険医療機関名
//			$ward_cnt,		//病棟数
//			$sickbed_cnt,		//病床数
			$report_kbn,		//届出区分
			$report_kbn_name,	//届出区分名
			$report_patient_cnt,	//届出時入院患者数
			$hosp_patient_cnt,	//１日平均入院患者数
			$compute_start_yyyy,	//１日平均入院患者数 算出期間（開始年）
			$compute_start_mm,	//１日平均入院患者数 算出期間（開始月）
			$compute_end_yyyy,	//１日平均入院患者数 算出期間（終了年）
			$compute_end_mm,	//１日平均入院患者数 算出期間（終了月）
			$nurse_sumtime,		//勤務時間数（看護師）
			$sub_nurse_sumtime,	//勤務時間数（准看護師）
			$assistance_nurse_sumtime, //勤務時間数（看護補助者）
			$nurse_cnt,		//看護師数
			$sub_nurse_cnt,		//准看護師数
			$assistance_nurse_cnt,	//看護補助者数
			$hospitalization_cnt,	//平均在院日数
			$ave_start_yyyy,	//平均在院日数 算出期間（開始年）
			$ave_start_mm,		//平均在院日数 算出期間（開始月）
			$ave_end_yyyy,		//平均在院日数 算出期間（終了年）
			$ave_end_mm,		//平均在院日数 算出期間（終了月）
			$prov_start_hh,		//夜勤時間帯 （開始時）
			$prov_start_mm,		//夜勤時間帯 （開始分）
			$prov_end_hh,		//夜勤時間帯 （終了時）
			$prov_end_mm,		//夜勤時間帯 （終了分）
//			$duty_yyyy,		//対象年
			$duty_mm,		//対象月
			$day_cnt,		//日数
//			$date_y1,		//カレンダー用年
//			$date_m1,		//カレンダー用月
//			$date_d1,		//カレンダー用日
			$labor_cnt,		//常勤職員の週所定労働時間
			$create_yyyy,		//作成年
			$create_mm,		//作成月
			$create_dd,		//作成日
			$nurse_staffing_flg,	//看護配置加算の有無			
			$acute_nursing_flg,	//急性期看護補助加算の有無			
			$acute_nursing_cnt,	//急性期看護補助加算の配置比率			
			$nursing_assistance_flg,//看護補助加算の有無			
			$nursing_assistance_cnt, //看護補助加算の配置比率
			$arg_array,		// 行位置情報
			$report_kbn_count	// 一般病棟、n対１入院基本料金
		){
		$KangoRowDataTop= $arg_array["KangoRowDataTop"];
		$KangoRowSum	= $arg_array["KangoRowSum"];
		$RowNumber	= $arg_array["RowNumber"];
		$KMidSumRow1	= $arg_array["KMidSumRow1"];
		$KMidSumRow2	= $arg_array["KMidSumRow2"];
		$KMidSumRow3	= $arg_array["KMidSumRow3"];
		$HojoRowDataTop	= $arg_array["HojoRowDataTop"];
		$HojoRowSum	= $arg_array["HojoRowSum"];
		$HMidSumRow1	= $arg_array["HMidSumRow1"];
		$HMidSumRow2	= $arg_array["HMidSumRow2"];

		// 5行目
		$excelObj->SetRowDim(5,18.75);
		$excelObj->SetValueJPwith("E5","届出区分","BOLD");
		$excelObj->SetValuewith("H5", $report_kbn_count ,"BOLD");
		$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetValueJPwith("I5","対1入院基本料","BOLD");

		$excelObj->SetArea("E5:I5");
		$excelObj->SetFont($this->MSPgothic ,14); 
		
		$excelObj->SetValueJPwith("M5","届出時入院患者数","");
		$excelObj->CellMerge("P5:R5");
		$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetValuewith("P5", $report_patient_cnt ,"" );
		
		$excelObj->SetArea("M5:R5");
		$excelObj->SetFont($this->MSPgothic ,11); 

		// 6行目
		$excelObj->SetRowDim(6,13.50);
		// 7行目
		$excelObj->SetRowDim(7,19.50);

		$excelObj->SetValueJPwith("E7","看護配置加算の有無(該当に○)","BOLD");
		$excelObj->SetValueJPwith("L7"," 有　・　無 ","BOLD UNDER");
		$CellArea="L7"; // 有り
		if ( $nurse_staffing_flg == 0 ){ // 看護配置加算の有無
			$CellArea="M7"; // 無し
		}
		$excelObj->ImageDraw( "images/circle.gif" , 30 , "circle" , $CellArea); // ○で囲む

		$excelObj->SetArea("A1:Z5");
		$excelObj->SetPosV("CENTER");

		// 8行目
		$excelObj->SetRowDim(8,18.75);

		$excelObj->SetRowDim(8,19.50);
		$excelObj->SetValueJPwith("E8","急性期看護補助加算の届出区分","BOLD");
		$formulae = "=IF(L8=50, \"1\", IF(L8=75, \"2\", \"無\"))";
		$excelObj->SetValueJPwith("J8",$formulae,"BOLD");
		$excelObj->SetValueJPwith("K8","（","BOLD");
		$excelObj->SetValueJPwith("M8","対１　）","BOLD");
		$excelObj->SetArea("L8");
		$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetValuewith("L8",$acute_nursing_cnt,"BOLD");
		// 9行目
		$excelObj->SetRowDim(9,18.75);

		$excelObj->SetValueJPwith("E9","看護補助加算の届出区分","BOLD");
		$formulae = "=IF(L9=30, \"1\", IF(L9=50, \"2\", IF(L9=75, \"3\", \"無\")))";
		$excelObj->SetValueJPwith("J9",$formulae,"BOLD");
		$excelObj->SetValueJPwith("K9","（","BOLD");
		$excelObj->SetValueJPwith("M9","対１　）","BOLD");
		$excelObj->SetArea("L9");
		$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetValuewith("L9",$nursing_assistance_cnt,"BOLD");

		$excelObj->SetArea("E7:M9");
		$excelObj->SetFont($this->MSPgothic ,12); 

		// 10行目
		$excelObj->SetRowDim(10,13.50);
		// 11行目
		$excelObj->SetRowDim(11,9);
		// 12行目
		$excelObj->SetRowDim(12,18.75);

		$excelObj->SetValueJP2("E12","○１日平均入院患者数〔A〕","BOLD",$this->MSPgothic ,14); 

		$excelObj->CellMerge("I12:K12");
		$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetValuewith("I12",$hosp_patient_cnt,"BOLD");
		$excelObj->SetFont($this->MSPgothic ,12); 

		$excelObj->SetValueJP2("L12","人","BOLD",$this->MSPgothic ,11); 

		$excelObj->SetValueJPwith("M12","(算出期間)","BOLD");
		$excelObj->SetValueJPwith("P12","年","");
		$excelObj->SetValueJPwith("R12","月〜","");
		$excelObj->SetValueJPwith("T12","年","");
		$excelObj->SetValueJPwith("V12","月","");

		//１日平均入院患者数 算出期間
		$excelObj->SetValuewith("O12",$compute_start_yyyy,"");	// 開始年
		$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetValuewith("Q12",$compute_start_mm,"");	// 開始月
		$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetValuewith("S12",$compute_end_yyyy,"");	// 終了年
		$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetValuewith("U12",$compute_end_mm,"");	// 終了月
		$excelObj->SetBorder("MEDIUM","outline");

		$excelObj->SetArea("O12:V12");
		$excelObj->SetFont($this->MSPgothic ,11); 
		// 13行目
		$excelObj->SetRowDim(13,18.75);

		$excelObj->SetValueJP2("E13","※端数切上げ整数入力","",$this->MSPgothic ,11); 

		// 14行目
		$excelObj->SetRowDim(14,18.75);
		$excelObj->SetValueJP2("C14","�〃酳振傳影�当たり看護配置数","BOLD",$this->MSPgothic ,14); 

		$excelObj->SetValueJP2("L14","人(実績値)","BOLD",$this->MSPgothic ,14); 

		$excelObj->CellMerge("J14:K14");
		$excelObj->SetBorder("MEDIUM","outline");
		//          =ROUNDDOWN(W245,1)
		$formulae = "=ROUNDDOWN(W$KMidSumRow3,1)";
		$excelObj->SetValuewith("J14",$formulae,""); // ！！注意！！、「245」行数は後の職員数読み込み数により求める
		$excelObj->SetBackColor("CCFFCC"); // 薄い緑
		$excelObj->SetFont($this->MSPgothic ,12); 

		$excelObj->SetValueJPwith("W14","『月延べ勤務時間数』","BOLD");
		$excelObj->SetPosH("RIGHT");
		$excelObj->CellMerge2("X14:Y14","medium","outline","CCFFCC"); // 薄い緑
		//          =R243
		$formulae = "=R$KMidSumRow1";
		$excelObj->SetValuewith("X14",$formulae ,""); // ！！注意！！、「243」行数は後の職員数読み込み数により求める
		$excelObj->SetFormatStyle("0.00");
		$excelObj->SetValueJPwith("Z14","時間(実績値)","BOLD");
		$excelObj->SetArea("W14:Z14");
		$excelObj->SetFont($this->MSPgothic ,12); 
		// 15行目
		$excelObj->SetRowDim(15,18.75);

		$excelObj->SetValueJPwith("C15","1日看護配置数　　〔(A ／配置比率)×３〕※端数切上げ","");
		$excelObj->CellMerge2("J15:K15","medium","outline","FFFF99"); // 薄い黄色
		// =ROUNDUP((($I$12/$H$5)*3),0)
		$formulae = '=ROUNDUP((($I$12/$H$5)*3),0)'; // <--- ''で囲むときは $ を生かすとき
		$excelObj->SetValuewith("J15", $formulae , "");

		$excelObj->SetValueJPwith("L15","人(基準値)","");
		$excelObj->SetArea("C15:L15");
		$excelObj->SetFont($this->MSPgothic ,12); 

		$excelObj->SetValueJPwith("W15","※「1日平均看護配置数」を満たす「月延べ勤務時間数」","BOLD");
		$excelObj->SetPosH("RIGHT");

		$excelObj->CellMerge2("X15:Y15","medium","outline","FFFF99"); // 薄い黄色

		$excelObj->SetValuewith("X15","=\$J\$15*8*\$M\$31","");
		$excelObj->SetFormatStyle("0.00");

		$excelObj->SetValueJP2("Z15","時間(基準値)","",$this->MSPgothic ,11); 
		$excelObj->SetArea("W15:Z15");
		// 16行目
		$excelObj->SetRowDim(16,13.5);
		// 17行目
		$excelObj->SetRowDim(17,15);
		// 18行目
		$excelObj->SetRowDim(18,18.75);
		$excelObj->SetValueJP2("C18","��看護職員中の看護師の比率","BOLD",$this->MSPgothic ,14); 

		$excelObj->CellMerge2("I18:K18","medium","outline","CCFFCC"); // 薄い緑
		$formulae = "=ROUNDDOWN(IF((X18/X15)*100>100,100,(X18/X15)*100),1)";
		$excelObj->SetValuewith("I18" , $formulae ,"BOLD");
		$excelObj->SetFont($this->MSPgothic ,12); 

		$excelObj->SetValueJP2("L18","％","BOLD",$this->MSPgothic ,14); 

		$excelObj->SetValueJPwith("M18","看護要員の内訳","");

		$excelObj->SetValueJPwith("P18","看護師","");

		$wk = $KangoRowSum + 1;
		$formulae = "=AW$wk";
		$excelObj->SetValuewith("R18" , $formulae ,""); // !!!要注意!!!
		$excelObj->SetBackColor("CCFFCC"); // 薄い緑
		$excelObj->SetBorder("MEDIUM","outline");

		$excelObj->SetValueJPwith("T18","月間総勤務時間数","");

		$excelObj->CellMerge("X18:Y18");
		$excelObj->SetBorder("MEDIUM","outline");
		$wk = $KangoRowSum + 1;
		// =AU240+AU241
		$formulae = "=AU$KangoRowSum+AU$wk";
		$excelObj->SetValuewith( "X18",$formulae ,""); // !!!要注意!!!
		$excelObj->SetBackColor("CCFFCC"); // 薄い緑
		$excelObj->SetFormatStyle("0.00");

		$excelObj->SetValueJPwith("Z18","時間","");

		$excelObj->SetArea("M18:Z18");
		$excelObj->SetFont($this->MSPgothic ,11); 

		$formulae="=IF(OR(R18+R19=0,X18+X19=0),\"\", IF((X18+X19)/(R18+R19)>150, \"看護職員の確保定着対策が必要です\", IF((X18+X19)/(R18+R19)>130, \"もう少し余裕のある看護配置をしましょう\", \"ゆとりのある看護配置をされています\")))";
		$excelObj->SetValueJP2("AB18",$formulae,"BOLD",$this->MSPgothic ,14); 
		$excelObj->SetPosV("CENTER");

		// 19行目
		$excelObj->SetRowDim(19,15);

		$excelObj->SetValueJP2("C19","月平均1日当たり配置数","BOLD",$this->MSPgothic ,11); 

		$excelObj->SetValueJP2("H19","看護師","BOLD",$this->MSPgothic ,11); 

		$excelObj->CellMerge("I19:K19");
		$excelObj->SetBorder("MEDIUM","outline");
		$wk = $KangoRowSum + 1;
		// =ROUNDDOWN((AU240+AU241)/(M31*8),1)
		$formulae="=ROUNDDOWN((AU$KangoRowSum+AU$wk)/(M31*8),1)";
		$excelObj->SetValuewith( "I19",$formulae ,"BOLD"); // !!!要注意!!!
		$excelObj->SetFont($this->MSPgothic ,12); 
		$excelObj->SetBackColor("CCFFCC"); // 薄い緑

		$excelObj->SetValueJP2("L19","人","BOLD",$this->MSPgothic ,11); 

		$excelObj->SetValueJPwith("P19","准看護師","");

		$wk = $KangoRowSum + 1;
		// =AX241
		$formulae= "=AX$wk";
		$excelObj->SetValuewith("R19" , $formulae ,""); // !!!要注意!!!
		$excelObj->SetBackColor("CCFFCC"); // 薄い緑
		$excelObj->SetBorder("MEDIUM","outline");

		$excelObj->SetValueJPwith("T19","月間総勤務時間数","");

		$excelObj->CellMerge("X19:Y19");
		$excelObj->SetBorder("MEDIUM","outline");
		$wk = $KangoRowSum + 1;
		// =AV240+AV241
		$formulae= "=AV$KangoRowSum+AV$wk";
		$excelObj->SetValuewith( "X19",$formulae ,""); // !!!要注意!!!
		$excelObj->SetBackColor("CCFFCC"); // 薄い緑
		$excelObj->SetFormatStyle("0.00");

		$excelObj->SetValueJPwith("Z19","時間","");
		$excelObj->SetArea("P19:Z19");
		$excelObj->SetFont($this->MSPgothic ,11); 

		// 20行目
		$excelObj->SetRowDim(20,18.75);

		$excelObj->SetValueJPwith("P20","看護補助者","");

		$wk = $HojoRowSum + 1;
		// =AU456 
		$formulae = "=AU$wk";
		$excelObj->SetValuewith("R20" , $formulae ,""); // !!!要注意!!!
		$excelObj->SetBackColor("CCFFCC"); // 薄い緑
		$excelObj->SetBorder("MEDIUM","outline");

		$excelObj->SetValueJPwith("T20","月間総勤務時間数","");

		$excelObj->CellMerge("X20:Y20");
		$excelObj->SetBorder("MEDIUM","outline");

		$wk = $HojoRowSum + 1;
		// =AS455+AS456
		$formulae = '=AS'.$HojoRowSum.'+AS'.$wk;
		$excelObj->SetValuewith( "X20",$formulae ,""); // !!!要注意!!!
		$excelObj->SetBackColor("CCFFCC"); // 薄い緑
		$excelObj->SetFormatStyle("0.00");

		$excelObj->SetValueJPwith("Z20","時間","");

		$excelObj->SetArea("P20:Z20");
		$excelObj->SetFont($this->MSPgothic ,11); 

		// 21行目
		$excelObj->SetRowDim(21,11.25);
		// 22行目
		$excelObj->SetRowDim(22,18.75);
		$excelObj->SetValueJP2("C22","�Ｊ振兀澑‘�数 ※端数切上げ整数入力","BOLD",$this->MSPgothic ,14); 

		$excelObj->CellMerge("I22:K22");
		$excelObj->SetBorder("MEDIUM","outline");

		if ( $hospitalization_cnt == "" ){ $hospitalization_cnt=0;}
		$excelObj->SetValuewith( "I22", $hospitalization_cnt ,"");
		$excelObj->SetFont($this->MSPgothic ,12); 

		$excelObj->SetValueJP2("L22","日","BOLD",$this->MSPgothic ,14); 

		$excelObj->SetValueJPwith("M22","(算出期間)","BOLD");
		$excelObj->SetValueJPwith("P22","年","");
		$excelObj->SetValueJPwith("R22","月〜","");
		$excelObj->SetValueJPwith("T22","年","");
		$excelObj->SetValueJPwith("V22","月","");

		$excelObj->SetValuewith("O22",$ave_start_yyyy,"");	//平均在院日数 算出期間（開始年）
		$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetValuewith("Q22",$ave_start_mm,"");	//平均在院日数 算出期間（開始月）
		$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetValuewith("S22",$ave_end_yyyy,"");	//平均在院日数 算出期間（終了年）
		$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetValuewith("U22",$ave_end_mm,"");		//平均在院日数 算出期間（終了月）
		$excelObj->SetBorder("MEDIUM","outline");
		// 23行目
		$excelObj->SetRowDim(23,17.25);
		// 24行目
		$excelObj->SetRowDim(24,17.25);

		$excelObj->SetValueJP2("C24","�ぬ覿仍�間帯(16時間)","BOLD",$this->MSPgothic ,14); 

		$wk = " ".$prov_start_hh."時".$prov_start_mm."分 ";	//夜勤時間帯、開始時分

		$excelObj->SetValueJP2("H24", $wk ,"BOLD UNDER",$this->MSPgothic ,12); 

		$excelObj->SetValueJP2("L24", "〜" ,"BOLD",$this->MSPgothic ,14); 

		$wk   = " ".$prov_end_hh."時".$prov_end_mm."分 ";		//夜勤時間帯、終了時分
		$excelObj->SetValueJP2("M24", $wk ,"BOLD UNDER",$this->MSPgothic ,12); 

		// 25行目
		$excelObj->SetRowDim(25,13.5);
		// 26行目
		$excelObj->SetRowDim(26,18.75);

		$excelObj->SetValueJP2("C26","�シ酳振冖覿仍�間数〔(D-E)／B〕","BOLD",$this->MSPgothic ,14); 

		$excelObj->CellMerge("I26:K26");
		$excelObj->SetBorder("MEDIUM","outline");
		// =ROUNDDOWN(I244/I243,1)
		$formulae = "=ROUNDDOWN(I$KMidSumRow2/I$KMidSumRow1,1)";
		$excelObj->SetValuewith("I26",$formulae ,"");
		$excelObj->SetFont($this->MSPgothic ,12); 
		$excelObj->SetBackColor("CCFFCC"); // 薄い緑

		$excelObj->SetValueJP2("L26","時間","BOLD",$this->MSPgothic ,14); 

		$formulae = "=IF(ISNUMBER(I26), IF(I26>79.2, \"7対1特別入院基本料、10対1特別入院基本料の届出が必要となります。\", IF(I26>72, \"72時間を超過していますので、翌月には改善するようにしましょう。\", \"\")), \"\")";
		$excelObj->SetValueJPwith("M26",$formulae,"BOLD");

		// 27行目
		$excelObj->SetRowDim(27,13.50);
		// 28行目
		$excelObj->SetRowDim(28,18.75);

		$excelObj->SetValueJP2("C28","�Ψ酳振傳影�当たり看護補助者配置数","BOLD",$this->MSPgothic ,14); 

		$excelObj->CellMerge("J28:K28");
		$excelObj->SetBorder("MEDIUM","outline");

		// '=ROUNDDOWN(AC460,1)'
		$formulae = "=ROUNDDOWN(AC$HMidSumRow2,1)";
		$excelObj->SetValuewith("J28", $formulae , "");
		$excelObj->SetFont($this->MSPgothic ,12); 
		$excelObj->SetBackColor("CCFFCC"); // 薄い緑

		$excelObj->SetValueJP2("L28","人(実績値)","BOLD",$this->MSPgothic ,14); 

		$excelObj->SetValueJPwith("W28","『月延べ勤務時間数』","BOLD");
		$excelObj->SetPosH("RIGHT");
		$excelObj->CellMerge2("X28:Y28","medium","outline","CCFFCC"); // 薄い緑

		// '=K459+AC459'
		$formulae = "=K$HMidSumRow1+AC$HMidSumRow1";
		$excelObj->SetValuewith("X28", $formulae, ""); // ！！注意！！
		$excelObj->SetFormatStyle("0.00");

		$excelObj->SetValueJPwith("Z28","時間(実績値)","");
		$excelObj->SetArea("W28:Z28");
		$excelObj->SetFont($this->MSPgothic ,12);

		// 29行目
		$excelObj->SetRowDim(29,18.75);

		$excelObj->SetValueJP2("C29","1日看護補助者配置数　　〔(A ／配置比率)×３〕※端数切上げ","",$this->MSPgothic ,11); 

		$excelObj->CellMerge2("J29:K29","medium","outline","FFFF99"); // 薄い黄色
		// =ROUNDUP(($I$12/IF($H$5>=13, $L$9, $L$8)*3),0)
		$formulae="=ROUNDUP((\$I\$12/IF(\$H\$5>=13, \$L\$9, \$L\$8)*3),0)";
		$excelObj->SetValuewith("J29", $formulae, "BOLD");
		$excelObj->SetFont($this->MSPgothic ,12); 

		$excelObj->SetValueJPwith("L29","人(基準値)","");

		$excelObj->SetValueJPwith("W29","※「1日平均看護補助者配置数」を満たす「月延べ勤務時間数」","BOLD");
		$excelObj->SetPosH("RIGHT");
		$excelObj->SetFont($this->MSPgothic ,11); 

		$excelObj->CellMerge2("X29:Y29","medium","outline","FFFF99"); // 薄い黄色
		$excelObj->SetValuewith("X29",'=$J$29*8*$M$31',"BOLD"); // ！！注意！！
		$excelObj->SetFormatStyle("0.00");
		$excelObj->SetFont($this->MSPgothic ,12); 

		$excelObj->SetValueJP2("Z29","時間(基準値)","",$this->MSPgothic ,11); 
		// 30行目
		$excelObj->SetRowDim(30,15);
		// 31行目
		$excelObj->SetRowDim(31,18.75);

		$excelObj->SetValueJP2("F31","※今月の稼働日数","BOLD",$this->MSPgothic ,11); 

		$excelObj->SetValuewith("M31",$day_cnt,"BOLD");
		if ( $day_cnt <= 0 || $day_cnt == '' ){
			$excelObj->SetBackColor("FF0000"); // rgb 赤
		}
//		$excelObj->SetStyleConditional( 'lessThanOrEqual' , '0' , 'FFFF0000' ); // 2012.2.20現在、機能しない

		$excelObj->SetFont($this->MSPgothic ,12); 
		$excelObj->SetBorder("MEDIUM","outline");

		$excelObj->SetValueJP2("N31","日","",$this->MSPgothic ,11);	// 稼働日数
		// 32行目
		$excelObj->SetRowDim(32,18.75);

		$excelObj->SetValuewith("C32",$duty_mm,"BOLD");	//月
		$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetFont($this->MSPgothic ,14);

		$excelObj->SetValueJP2("D32","月","BOLD",$this->MSPgothic ,14);

		$excelObj->SetValueJP2("F32","※常勤職員の週所定労働時間","BOLD",$this->MSPgothic ,12);

		$excelObj->SetValuewith("M32",$labor_cnt,"BOLD");	//常勤職員の週所定労働時間
		if ( $labor_cnt <= 0 || $labor_cnt == '' ){
			$excelObj->SetBackColor("FF0000"); // rgb 赤
		}

//		$excelObj->SetStyleConditional( 'equal' , '0' , 'FFFF0000' ); // 2012.2.20現在、機能しない

		$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetFormatStyle("0.00");
		$excelObj->SetFont($this->MSPgothic ,12); 

		$excelObj->SetValueJP2("N32","時間","",$this->MSPgothic ,11);
		// 縦位置
		$excelObj->SetArea("A1:AZ32");
		$excelObj->SetPosV("CENTER");
	}
	///////////////////////////////////////////////////////////
	//							 //
	// 療養病棟１、２ 入院基本料に係る届出書添付書類(様式９) //
	//							 //
	///////////////////////////////////////////////////////////
	function ShowData_1(
			$excelObj,		// PHP-Excelオブジェクト
			$obj,			// 共通クラス
//			$excel_flg,		//ＥＸＣＥＬ表示フラグ（１：ＥＸＣＥＬ表示）
//			$recomputation_flg,	//１ヵ月計算４週計算フラグ（１：１ヵ月計算、２：４週計算）
//			$data_array,		//看護師／准看護師情報
//			$data_sub_array,	//看護補助者情報
//			$hospital_name,		//保険医療機関名
//			$ward_cnt,		//病棟数
//			$sickbed_cnt,		//病床数
			$report_kbn,		//届出区分
			$report_kbn_name,	//届出区分名
			$report_patient_cnt,	//届出時入院患者数
			$hosp_patient_cnt,	//１日平均入院患者数
			$compute_start_yyyy,	//１日平均入院患者数 算出期間（開始年）
			$compute_start_mm,	//１日平均入院患者数 算出期間（開始月）
			$compute_end_yyyy,	//１日平均入院患者数 算出期間（終了年）
			$compute_end_mm,	//１日平均入院患者数 算出期間（終了月）
			$nurse_sumtime,		//勤務時間数（看護師）
			$sub_nurse_sumtime,	//勤務時間数（准看護師）
			$assistance_nurse_sumtime, //勤務時間数（看護補助者）
			$nurse_cnt,		//看護師数
			$sub_nurse_cnt,		//准看護師数
			$assistance_nurse_cnt,	//看護補助者数
			$hospitalization_cnt,	//平均在院日数
			$ave_start_yyyy,	//平均在院日数 算出期間（開始年）
			$ave_start_mm,		//平均在院日数 算出期間（開始月）
			$ave_end_yyyy,		//平均在院日数 算出期間（終了年）
			$ave_end_mm,		//平均在院日数 算出期間（終了月）
			$prov_start_hh,		//夜勤時間帯 （開始時）
			$prov_start_mm,		//夜勤時間帯 （開始分）
			$prov_end_hh,		//夜勤時間帯 （終了時）
			$prov_end_mm,		//夜勤時間帯 （終了分）
//			$duty_yyyy,		//対象年
			$duty_mm,		//対象月
			$day_cnt,		//日数
//			$date_y1,		//カレンダー用年
//			$date_m1,		//カレンダー用月
//			$date_d1,		//カレンダー用日
			$labor_cnt,		//常勤職員の週所定労働時間
			$create_yyyy,		//作成年
			$create_mm,		//作成月
			$create_dd,		//作成日
			$nurse_staffing_flg,	//看護配置加算の有無
			$acute_nursing_flg,	//急性期看護補助加算の有無
			$acute_nursing_cnt,	//急性期看護補助加算の配置比率
			$nursing_assistance_flg,//看護補助加算の有無
			$nursing_assistance_cnt, //看護補助加算の配置比率
			$arg_array,		// 行位置情報
			$ReportType		// 1:療養病棟１、2:療養病棟２
		){
		$KangoRowDataTop= $arg_array["KangoRowDataTop"];
		$KangoRowSum	= $arg_array["KangoRowSum"];
		$KMidSumRow1	= $arg_array["KMidSumRow1"];
		$KMidSumRow2	= $arg_array["KMidSumRow2"];
		$KMidSumRow3	= $arg_array["KMidSumRow3"];
		$HojoRowDataTop	= $arg_array["HojoRowDataTop"];
		$HojoRowSum	= $arg_array["HojoRowSum"];
		$HMidSumRow1	= $arg_array["HMidSumRow1"];
		$HMidSumRow2	= $arg_array["HMidSumRow2"];
		$HMidSumRow3	= $arg_array["HMidSumRow3"];

/* debug 看護協会の雛形での行位置
		$KangoRowDataTop= 35;
		$KangoRowSum	= 237;
		$KMidSumRow1	= 240;
		$KMidSumRow2	= 241;
		$KMidSumRow3	= 242;
		$HojoRowDataTop	= 250;
		$HojoRowSum	= 452;
		$HMidSumRow1	= 456;
		$HMidSumRow2	= 457;
		$HMidSumRow3	= 458;
// debug end.
*/
		if ( $ReportType == 1 ){
			$calcUnit = 20;
		}else{
			$calcUnit = 25;
		}

		// 5行目
		$RowNumber=5;
		$excelObj->SetRowDim($RowNumber,18.75);
		$excelObj->SetValueJP2("E5","届出区分","BOLD",$this->MSPgothic ,14); 

		$excelObj->CellMerge2("F".$RowNumber.":L".$RowNumber,"medium","outline","CC99FF");

		$excelObj->SetValueJP2("F5", $report_kbn_name ,"BOLD",$this->MSPgothic ,12); 

		$excelObj->SetValueJP2("M5", "届出時入院患者数" ,"",$this->MSPgothic ,11); 

		$excelObj->CellMerge("P".$RowNumber.":R".$RowNumber);
		$excelObj->SetBorder2("medium","","outline");
		$excelObj->SetValueJPwith("P5", $report_patient_cnt ,"BOLD");

		// 6行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,13.5);

		// 7行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,19.5);

		if ( $ReportType == 1 ){
			$wk = "看護配置20対1以上・看護補助配置20対1以上　　看護職員中看護師比率20％以上　";
		}else{
			$wk = "看護配置25対1以上・看護補助配置25対1以上　　看護職員中看護師比率20％以上　看護要員(看護職員・看護補助者)の月平均夜勤時間数72時間以内";
		}
		$excelObj->CellMerge2("E".$RowNumber.":AB".$RowNumber,"medium","outline","FFFF99");
		$excelObj->SetValueJP2("E7", $wk ,"BOLD VPOS:CENTER",$this->MSPgothic ,12); 

		// 8行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,17.25);

		// 9行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,9);

		// 10行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,18.75);

		$excelObj->SetValueJP2("E10", "○１日平均入院患者数〔A〕" ,"BOLD",$this->MSPgothic ,14); 

		$excelObj->CellMerge("I".$RowNumber.":K".$RowNumber);
		$excelObj->SetBorder2("medium","","outline");

		$excelObj->SetValuewith("I10", $hosp_patient_cnt ,"BOLD");
		$excelObj->SetFont($this->MSPgothic ,12); 

		$excelObj->SetValuewith("L10", "人" ,"BOLD");
		$excelObj->SetFont($this->MSPgothic ,14); 

		$excelObj->SetValueJPwith("M10", "(算出期間）" ,"");

		$excelObj->SetValueJPwith("P10","年","");
		$excelObj->SetValueJPwith("R10","月〜","");
		$excelObj->SetValueJPwith("T10","年","");
		$excelObj->SetValueJPwith("V10","月","");

		//１日平均入院患者数 算出期間
		$excelObj->SetValuewith("O10",$compute_start_yyyy,"");	// 開始年
		$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetValuewith("Q10",$compute_start_mm,"");	// 開始月
		$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetValuewith("S10",$compute_end_yyyy,"");	// 終了年
		$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetValuewith("U10",$compute_end_mm,"");	// 終了月
		$excelObj->SetBorder("MEDIUM","outline");

		$CellArea = "M".$RowNumber.":U".$RowNumber;
		$excelObj->SetArea($CellArea);
		$excelObj->SetFont($this->MSPgothic ,11); 

		// 11行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,18.75);

		$excelObj->SetValueJP2("E11", "※端数切上げ整数入力" ,"",$this->MSPgothic ,11); 

		// 12行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,18.75);

		$excelObj->SetValueJP2("C12", "�〃酳振傳影�当たり看護配置数" ,"BOLD",$this->MSPgothic ,14); 
		$excelObj->CellMerge2("I".$RowNumber.":K".$RowNumber,"medium","outline","CCFFCC"); // 薄い緑
		// =ROUNDDOWN(W242,1)
		$formulae="=ROUNDDOWN(W$KMidSumRow3,1)";
		$excelObj->SetValuewith("I12", $formulae ,"BOLD");
		$excelObj->SetFont($this->MSPgothic ,12); 
		$excelObj->SetValueJP2("L12", "人(実績値）" ,"BOLD",$this->MSPgothic ,14); 

		$excelObj->SetValueJP2("Q12", "『月延べ勤務時間数』" ,"BOLD",$this->MSPgothic ,12); 
		$excelObj->CellMerge2("U".$RowNumber.":V".$RowNumber,"medium","outline","CCFFCC"); // 薄い緑
		// =R240
		$formulae="=R$KMidSumRow1";
		$excelObj->SetValuewith("U12", $formulae ,"BOLD");
		$excelObj->SetFont($this->MSPgothic ,12); 
		$excelObj->SetFormatStyle("0.00");

		$excelObj->SetValueJP2("W12", "時間(実績値)" ,"BOLD",$this->MSPgothic ,12); 

		// 13行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,18.75);

		$excelObj->SetValueJP2("C13", "1日看護配置数　　〔(A ／配置比率20)×３〕" ,"",$this->MSPgothic ,11); 
		$excelObj->CellMerge2("J".$RowNumber.":K".$RowNumber,"medium","outline","FFFF99"); // 薄い黄色

		$formulae="=ROUNDUP(((I10/$calcUnit)*3),0)";
		$excelObj->SetValuewith("J13", $formulae ,"BOLD");

		$excelObj->SetValueJP2("L13", "人" ,"",$this->MSPgothic ,12); 

		$excelObj->SetValueJP2("M13", "※「1日平均看護配置数」を満たす「月延べ勤務時間数」" ,"BOLD",$this->MSPgothic ,11); 
		$excelObj->CellMerge2("U".$RowNumber.":V".$RowNumber,"medium","outline","FFFF99"); // 薄い黄色

		$formulae="=J13*8*M28";
		$excelObj->SetValuewith("U13", $formulae ,"BOLD");
		$excelObj->SetFont($this->MSPgothic ,12); 
		$excelObj->SetFormatStyle("0.00");

		$excelObj->SetValueJP2("W13", "時間(基準値)" ,"",$this->MSPgothic ,11); 

		// 14行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,15);

		// 15行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,18.75);

		$excelObj->SetValueJP2("C15", "��-2 月平均１日当たり看護補助配置数" ,"BOLD",$this->MSPgothic ,14); 
		$excelObj->CellMerge2("J".$RowNumber.":K".$RowNumber,"medium","outline","CCFFCC"); // 薄い緑
		// =ROUNDDOWN(W458,1)
		$formulae="=ROUNDDOWN(W$HMidSumRow3,1)";
		$excelObj->SetValuewith("J15", $formulae ,"BOLD");
		$excelObj->SetFont($this->MSPgothic ,12); 
		$excelObj->SetFormatStyle("0.00");

		$excelObj->SetValueJP2("L15", "人(実績値）" ,"BOLD",$this->MSPgothic ,14); 

		$excelObj->SetValueJP2("Q15", "『月延べ勤務時間数』" ,"BOLD",$this->MSPgothic ,12); 
		$excelObj->CellMerge2("V".$RowNumber.":W".$RowNumber,"medium","outline","CCFFCC"); // 薄い緑
		// =R456
		$formulae="=R$HMidSumRow1";
		$excelObj->SetValuewith("V15", $formulae ,"BOLD");
		$excelObj->SetFont($this->MSPgothic ,12); 
		$excelObj->SetFormatStyle("0.00");

		$excelObj->SetValueJP2("X15", "時間(実績値)" ,"BOLD",$this->MSPgothic ,12); 

		// 16行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,18.75);

		$excelObj->SetValueJP2("C16", "1日看護補助配置数　　〔(A ／配置比率20)×３〕" ,"",$this->MSPgothic ,11); 
		$excelObj->CellMerge2("J".$RowNumber.":K".$RowNumber,"medium","outline","FFFF99"); // 薄い黄色

		$formulae="=ROUNDUP(((I10/$calcUnit)*3),0)";
		$excelObj->SetValuewith("J16", $formulae ,"BOLD");

		$excelObj->SetValueJP2("L16", "人" ,"",$this->MSPgothic ,12); 

		$excelObj->SetValueJP2("M16", "※「1日平均看護補助配置数」を満たす「月延べ勤務時間数」" ,"BOLD",$this->MSPgothic ,11); 
		$excelObj->CellMerge2("V".$RowNumber.":W".$RowNumber,"medium","outline","FFFF99"); // 薄い黄色

		$formulae="=J16*8*M28";
		$excelObj->SetValuewith("V16", $formulae ,"BOLD");
		$excelObj->SetFont($this->MSPgothic ,12); 
		$excelObj->SetFormatStyle("0.00");

		$excelObj->SetValueJP2("X16", "時間(基準値)" ,"",$this->MSPgothic ,11); 

		// 17行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber, 15);

		// 18行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber, 18.75);

		$excelObj->SetValueJP2("C18", "��看護職員中の看護師の比率" ,"BOLD",$this->MSPgothic ,14); 
		$excelObj->CellMerge2("I".$RowNumber.":K".$RowNumber,"medium","outline","CCFFCC"); // 薄い緑

		$formulae="=ROUNDDOWN(IF((X18/U13)*100>100,100,(X18/U13)*100),1)";
		$excelObj->SetValuewith("I18", $formulae ,"BOLD");
		$excelObj->SetFont($this->MSPgothic ,12); 
		$excelObj->SetFormatStyle("0.00");

		$excelObj->SetValueJP2("M$RowNumber", "看護要員の内訳" ,"",$this->MSPgothic ,11); 

		$excelObj->SetValueJP2("P$RowNumber", "看護師" ,"",$this->MSPgothic ,11); 

		$b = $KangoRowSum + 1;
		$formulae="=AW$b";
		$excelObj->SetValuewith("R$RowNumber", $formulae ,"");
		$excelObj->SetFont($this->MSPgothic ,11); 
		$excelObj->SetFormatStyle("0.0");
		$excelObj->SetBorder2("medium","","outline");
		$excelObj->SetBackColor("CCFFCC"); // 薄い緑

		$excelObj->SetValueJP2("S$RowNumber", "人" ,"",$this->MSPgothic ,11); 

		$excelObj->SetValueJP2("T$RowNumber", "月間総勤務時間数" ,"",$this->MSPgothic ,11); 
		$excelObj->CellMerge2("X".$RowNumber.":Y".$RowNumber,"medium","outline","CCFFCC"); // 薄い緑
		// =AU237+AU238
		$b = $KangoRowSum + 1;
		$formulae="=AU$KangoRowSum+AU$b";
		$excelObj->SetValuewith("X$RowNumber", $formulae ,"");
		$excelObj->SetFont($this->MSPgothic ,11); 
		$excelObj->SetFormatStyle("0.00");

		$excelObj->SetValueJP2("Z$RowNumber", "時間" ,"",$this->MSPgothic ,11); 

		$formulae = "=IF(OR(R18+R19=0,X18+X19=0),\"\", IF((X18+X19)/(R18+R19)>150, \"看護職員の確保定着対策が必要です\", IF((X18+X19)/(R18+R19)>130, \"もう少し余裕のある看護配置をしましょう\", \"ゆとりのある看護配置をされています\")))";
		$excelObj->SetValueJP2("AB$RowNumber", $formulae ,"BOLD",$this->MSPgothic,14);
		$excelObj->SetPosV("CENTER");

		// 19行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber, 18.75);
		$excelObj->SetValueJP2("P$RowNumber", "准看護師" ,"",$this->MSPgothic ,11); 

		$b = $KangoRowSum + 1;
		$formulae="=AX$b";
		$excelObj->SetValuewith("R$RowNumber", $formulae ,"");
		$excelObj->SetFont($this->MSPgothic ,11); 
		$excelObj->SetFormatStyle("0.0");
		$excelObj->SetBorder2("medium","","outline");
		$excelObj->SetBackColor("CCFFCC"); // 薄い緑

		$excelObj->SetValueJP2("S$RowNumber", "人" ,"",$this->MSPgothic ,11); 

		$excelObj->SetValueJP2("T$RowNumber", "月間総勤務時間数" ,"",$this->MSPgothic ,11); 

		$excelObj->CellMerge2("X".$RowNumber.":Y".$RowNumber,"medium","outline","CCFFCC"); // 薄い緑

		// =AV237+AV238
		$b = $KangoRowSum + 1;
		$formulae="=AV$KangoRowSum+AV$b";
		$excelObj->SetValuewith("X$RowNumber", $formulae ,"");
		$excelObj->SetFont($this->MSPgothic ,11); 
		$excelObj->SetFormatStyle("0.00");

		$excelObj->SetValueJP2("Z$RowNumber", "時間" ,"",$this->MSPgothic ,11); 

		// 20行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber, 18.75);
		$excelObj->SetValueJP2("P$RowNumber", "看護補助者" ,"",$this->MSPgothic ,11); 
		$excelObj->SetPosH("CENTER");

		$b = $HojoRowSum + 1;
		// =AU453
		$formulae="=AU$b";
		$excelObj->SetValuewith("R$RowNumber", $formulae ,"");
		$excelObj->SetFont($this->MSPgothic ,11); 
		$excelObj->SetFormatStyle("0.0");
		$excelObj->SetBorder2("medium","","outline");
		$excelObj->SetBackColor("CCFFCC"); // 薄い緑

		$excelObj->SetValueJP2("S$RowNumber", "人" ,"",$this->MSPgothic ,11); 

		$excelObj->SetValueJP2("T$RowNumber", "月間総勤務時間数" ,"",$this->MSPgothic ,11); 

		$excelObj->CellMerge2("X".$RowNumber.":Y".$RowNumber,"medium","outline","CCFFCC"); // 薄い緑

		// =AS452+AS453
		$b = $HojoRowSum + 1;
		$formulae="=AS$HojoRowSum+AS$b";
		$excelObj->SetValuewith("X$RowNumber", $formulae ,"");
		$excelObj->SetFont($this->MSPgothic ,11); 
		$excelObj->SetFormatStyle("0.00");

		$excelObj->SetValueJP2("Z$RowNumber", "時間" ,"",$this->MSPgothic ,11); 

		// 21行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber, 11.25);

		// 22行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber, 18.75);

		$excelObj->SetValueJP2("C$RowNumber", "�Ｊ振兀澑‘�数 ※端数切上げ整数入力" ,"",$this->MSPgothic ,14); 

		$excelObj->CellMerge("I".$RowNumber.":K".$RowNumber);
		$excelObj->SetBorder2("medium","","outline");

		$excelObj->SetValueJPwith("M$RowNumber", "(算出期間）" ,"");

		$excelObj->SetValueJPwith("P$RowNumber","年","");
		$excelObj->SetValueJPwith("R$RowNumber","月〜","");
		$excelObj->SetValueJPwith("T$RowNumber","年","");
		$excelObj->SetValueJPwith("V$RowNumber","月","");

		//１日平均入院患者数 算出期間
		$excelObj->SetArea("O$RowNumber");
		$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetArea("Q$RowNumber");
		$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetArea("S$RowNumber");
		$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetArea("U$RowNumber");
		$excelObj->SetBorder("MEDIUM","outline");

		$CellArea = "M".$RowNumber.":U".$RowNumber;
		$excelObj->SetArea($CellArea);
		$excelObj->SetFont($this->MSPgothic ,11); 

		$CellArea = "C".$RowNumber.":V".$RowNumber;
		$excelObj->SetArea($CellArea);

		$excelObj->SetMask('darkGrid'); // 網掛け

		// 23行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber, 17.25);

		// 24行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber, 17.25);

		$excelObj->SetValueJP2("C$RowNumber", "�ぬ覿仍�間帯(16時間)" ,"BOLD",$this->MSPgothic ,14); 

		$wk = " ".$prov_start_hh."時 ".$prov_start_mm."分　";
		$excelObj->SetValueJP2("H$RowNumber", $wk, "UNDER BOLD",$this->MSPgothic ,12); 

		$excelObj->SetValueJP2("L$RowNumber", "〜", "BOLD",$this->MSPgothic ,14); 

		$wk = " ".$prov_end_hh."時 ".$prov_end_mm."分　";
		$excelObj->SetValueJP2("M$RowNumber", $wk, "UNDER BOLD",$this->MSPgothic ,12); 

		// 25行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber, 13.50);

		// 26行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber, 18.75);

		$excelObj->SetValueJP2("C$RowNumber", "�ゴ埜醉廾�の月平均夜勤時間数〔(D-E)／B〕" ,"BOLD",$this->MSPgothic ,14); 

		$excelObj->CellMerge2("K".$RowNumber.":M".$RowNumber,"medium","outline","CCFFCC"); // 薄い緑

		// =ROUNDDOWN((I241+I457)/(I240+I456),1)
		$formulae="=ROUNDDOWN((I$KMidSumRow2+I$HMidSumRow2)/($KMidSumRow1+I$HMidSumRow1),1)";
		$excelObj->SetValuewith("K$RowNumber", $formulae ,"BOLD");
		$excelObj->SetFont($this->MSPgothic ,12); 

		$excelObj->SetValueJP2("O$RowNumber", "※看護職員・看護補助者の月平均夜勤時間数" ,"",$this->MSPgothic ,11); 

		$formulae="=IF(ISNUMBER(K26), IF(K26>72, \"72時間を超過していますので、看護職員の労務管理に留意しましょう。\", \"\"), \"\")";
		$excelObj->SetValuewith("X$RowNumber", $formulae ,"BOLD");
		$excelObj->SetFont($this->MSPgothic ,14); 

		// 27行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber, 13.50);

		// 28行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber, 18.75);

		$excelObj->SetValueJP2("F$RowNumber", "※今月の稼働日数" ,"BOLD",$this->MSPgothic ,12); 

		$excelObj->SetValueJP2("M$RowNumber", $day_cnt ,"BOLD",$this->MSPgothic ,12); 
		$excelObj->SetBorder2("medium","","outline");
		if ( $day_cnt <= 0 || $day_cnt == '' ){
			$excelObj->SetBackColor("FF0000"); // rgb 赤
		}
//		$excelObj->SetStyleConditional( 'lessThanOrEqual' , '0' , 'FFFF0000' ); // 2012.2.20現在、機能しない

		$excelObj->SetValueJP2("N$RowNumber", "日" ,"",$this->MSPgothic ,11); 

		// 29行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber, 18.75);

		$excelObj->SetValueJP2("C$RowNumber", $duty_mm ,"BOLD",$this->MSPgothic ,12); 
		$excelObj->SetBorder2("medium","","outline");
		$excelObj->SetValueJP2("D$RowNumber","月","BOLD",$this->MSPgothic ,14);

		$excelObj->SetValueJP2("F$RowNumber","※常勤職員の週所定労働時間","BOLD",$this->MSPgothic ,12);
		$excelObj->SetValuewith("M$RowNumber",$labor_cnt,"BOLD");	//常勤職員の週所定労働時間
		if ( $labor_cnt <= 0 || $labor_cnt == '' ){
			$excelObj->SetBackColor("FF0000"); // rgb 赤
		}

//		$excelObj->SetStyleConditional( 'equal' , '0' , 'FFFF0000' ); // 2012.2.20現在、機能しない

		$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetFormatStyle("0.00");
		$excelObj->SetFont($this->MSPgothic ,12); 

		$excelObj->SetValueJP2("N$RowNumber","時間","",$this->MSPgothic ,11);

		// 縦位置
		$excelObj->SetArea("A1:AZ29");
		$excelObj->SetPosV("CENTER");

	}
	//////////////////////////////////
	//				//
	//   療養病棟・看護職員集計	//
	//				//
	//////////////////////////////////
	function ShowNurseSum11( $excelObj , $arg_array ){

		$RowNumber = $arg_array["RowNumber"];
		$KangoRowSum = $arg_array["KangoRowSum"];

		$KEI_RowU = $KangoRowSum;
		$KEI_RowL = $KangoRowSum+1;

		// 中間集計１行目
		$RowNumber += 3;
		$KMidSumRow1 = $RowNumber;
		$KMidSumRow2 = $KMidSumRow1 + 1;
		$KMidSumRow3 = $KMidSumRow1 + 2;

		$excelObj->SetRowDim($RowNumber,19.50);

		$excelObj->CellMerge2("C".$RowNumber.":H".$RowNumber,"medium","outline","");

		$excelObj->SetValueJP2("C".$RowNumber, "夜勤従事者数(夜勤ありの職員数)〔Ｂ〕", "BOLD VCENTER",$this->MSPgothic ,14); 

		// =ROUNDDOWN(L241,1)
		$excelObj->CellMerge2("I".$RowNumber.":K".$RowNumber,"medium","outline","FFFFCC"); // もっと薄い黄色

		$CellArea = "I".$RowNumber;
		$formulae = "=ROUNDDOWN(L".$KEI_RowL.",1)";
		$excelObj->SetValuewith($CellArea, $formulae, "BOLD");
		$excelObj->SetFont($this->MSPgothic ,12); 
		$excelObj->SetFormatStyle("0.00");
		$excelObj->SetPosV("CENTER");

		$excelObj->CellMerge2("L".$RowNumber.":Q".$RowNumber,"medium","outline","");

		$excelObj->SetValueJP2("L".$RowNumber, "月延べ勤務時間数の計〔Ｃ〕", "BOLD VCENTER",$this->MSPgothic ,14); 

		$excelObj->CellMerge2("R".$RowNumber.":S".$RowNumber,"medium","outline","FFFFCC"); // もっと薄い黄色

		$CellArea = "R".$RowNumber;
		//=AS240+AS241
		$formulae = "=AS".$KEI_RowU."+AS".$KEI_RowL;
		$excelObj->SetValuewith($CellArea, $formulae, "BOLD");
		$excelObj->SetFont($this->MSPgothic ,12); 
		$excelObj->SetFormatStyle("0.00");
		$excelObj->SetPosV("CENTER");

		// 中間集計２行目
		$RowNumber ++;
		$KMidSumRow2 = $RowNumber;

		$excelObj->SetRowDim($RowNumber,21);

		$excelObj->CellMerge("C".$RowNumber.":H".$RowNumber);
		$excelObj->SetBorder2("medium","","outline");

		$excelObj->SetValueJP2("C".$RowNumber, "月延べ夜勤時間数　〔D−E〕", "BOLD VCENTER",$this->MSPgothic ,14); 

		//=R244-AE244
		$excelObj->CellMerge2("I".$RowNumber.":K".$RowNumber,"medium","outline","FFFFCC"); // もっと薄い黄色

		$RowBefore = $RowNumber - 1 ;
		$CellArea = "I".$RowNumber;
		$formulae = "=R".$RowNumber."-AE".$RowNumber;
		$excelObj->SetValuewith($CellArea, $formulae, "BOLD");
		$excelObj->SetFont($this->MSPgothic ,12); 
		$excelObj->SetFormatStyle("0.00");
		$excelObj->SetPosV("CENTER");

		$excelObj->CellMerge("L".$RowNumber.":Q".$RowNumber);
		$excelObj->SetBorder2("medium","","outline");

		$excelObj->SetValueJP2("L".$RowNumber, "月延べ夜勤時間数の計〔Ｄ〕", "BOLD VCENTER",$this->MSPgothic ,14); 

		$excelObj->CellMerge2("R".$RowNumber.":S".$RowNumber,"medium","outline","FFFFCC"); // もっと薄い黄色

		$CellArea = "R".$RowNumber;
		//=AS241
		$formulae = "=AS".$KEI_RowL;
		$excelObj->SetValuewith($CellArea, $formulae, "BOLD");
		$excelObj->SetFont($this->MSPgothic ,12); 
		$excelObj->SetFormatStyle("0.00");
		$excelObj->SetPosV("CENTER");

		$excelObj->CellMerge("T".$RowNumber.":AD".$RowNumber);
		$excelObj->SetBorder2("medium","","outline");

		$excelObj->SetValueJP2("T".$RowNumber, "夜勤専従者及び月16時間以下の者の夜勤時間数〔E〕", "BOLD VCENTER",$this->MSPgothic ,12); 

		$excelObj->CellMerge2("AE".$RowNumber.":AF".$RowNumber,"medium","outline","FFFFCC"); // もっと薄い黄色

		$CellArea = "AE".$RowNumber;
		$formulae = "=AT".$KEI_RowL;
		$excelObj->SetValuewith($CellArea, $formulae, "BOLD");
		$excelObj->SetFormatStyle("0.00");
		$excelObj->SetPosV("CENTER");

		// 中間集計３行目・療養病棟と一般病棟とで参照するセル行位置が異なる。L列とW列
		$RowNumber ++;
		$KMidSumRow3 = $RowNumber;

		$excelObj->SetRowDim($RowNumber,21.75);

		$excelObj->CellMerge("C".$RowNumber.":K".$RowNumber);
		$excelObj->SetBorder2("medium","","outline");

		$excelObj->SetValueJP2("C".$RowNumber, "1日看護配置数　　〔(A ／届出区分の数)×３〕", "BOLD VCENTER",$this->MSPgothic ,14); 

		$CellArea = "L".$RowNumber;
		$formulae = "=J13"; // <=要注意、一般と療養で参照セル異なる
		$excelObj->SetValuewith($CellArea, $formulae, "BOLD");
		$excelObj->SetFormatStyle("0.00");
		$excelObj->SetPosV("CENTER");
		$excelObj->SetBorder2("medium","","outline");
		$excelObj->SetBackColor("FFFFCC"); // もっと薄い黄色

		$excelObj->CellMerge("M".$RowNumber.":V".$RowNumber);
		$excelObj->SetBorder2("medium","","outline");

		$excelObj->SetValueJP2("M".$RowNumber, "月平均１日当たり看護配置数〔C／(日数×８）〕", "BOLD VCENTER",$this->MSPgothic ,14);
		$excelObj->SetBorder2("medium","","outline");

		$excelObj->CellMerge("W".$RowNumber.":X".$RowNumber);
		$excelObj->SetBorder2("medium","","outline");

		$CellArea = "W".$RowNumber;
		// =ROUNDDOWN(R240/(M28*8),1)
		$formulae = "=ROUNDDOWN(R".$KMidSumRow1."/(M28*8),1)";
		$excelObj->SetValuewith($CellArea, $formulae, "BOLD");
		$excelObj->SetFormatStyle("0.00");
		$excelObj->SetPosV("CENTER");
		$excelObj->SetBackColor("FFFFCC"); // もっと薄い黄色

		// 空行
		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber , 13.5);
		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber , 13.5);
		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber , 13.5);

		$arg_array["KMidSumRow1"] = $KMidSumRow1;
		$arg_array["KMidSumRow2"] = $KMidSumRow2;
		$arg_array["KMidSumRow3"] = $KMidSumRow3;
		$arg_array["RowNumber"] = $RowNumber;

		return $arg_array;
	}
	////////////////////////////////////
	//				  //
	//   療養病棟１・看護補助者集計   //
	//				  //
	////////////////////////////////////
	function ShowNurseSum12( $excelObj , $arg_array ){

		$RowNumber = $arg_array["RowNumber"];
		$KMidSumRow1 = $arg_array["KMidSumRow1"];
		$HojoRowSum = $arg_array["HojoRowSum"];

		////////////////
		// 中間集計行 //
		////////////////
		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber,13.5);

		// コメント
		$excelObj->SetValueJP2("C".$RowNumber, "※療養病棟入院基本料における夜勤体制の要件を満たすことの確認は、以下の欄で行ってください。", "VCENTER",$this->MSPgothic ,11); 

		// 中間集計１行目
		$RowNumber ++;
		$HMidSumRow1 = $RowNumber;
		$HMidSumRow2 = $HMidSumRow1 + 1;
		$HMidSumRow3 = $HMidSumRow1 + 2;
		$excelObj->SetRowDim($RowNumber,18.75);

		$excelObj->CellMerge("C".$RowNumber.":H".$RowNumber);
		$excelObj->SetBorder2("medium","","outline");

		$excelObj->SetValueJP2("C".$RowNumber, "夜勤従事者数(夜勤ありの職員数)〔Ｂ〕", "BOLD VCENTER",$this->MSPgothic ,14); 

		$excelObj->CellMerge2("I".$RowNumber.":K".$RowNumber,"medium","outline","FFFFCC"); // もっと薄い黄色

		$CellArea = "I".$RowNumber;
		$a = $HojoRowSum + 1;
		// =ROUNDDOWN(L453,1)
		$formulae = "=ROUNDDOWN(L$a,1)";
		$excelObj->SetValuewith($CellArea, $formulae, "BOLD");
		$excelObj->SetFont($this->MSPgothic ,12); 
		$excelObj->SetFormatStyle("0.00");
		$excelObj->SetPosV("CENTER");

		$excelObj->CellMerge("L".$RowNumber.":Q".$RowNumber);
		$excelObj->SetBorder2("medium","","outline");

		$excelObj->SetValueJP2("L".$RowNumber, "月延べ勤務時間数の計〔Ｃ〕", "BOLD VCENTER",$this->MSPgothic ,14); 

		$excelObj->CellMerge2("R".$RowNumber.":S".$RowNumber,"medium","outline","FFFFCC"); // もっと薄い黄色

		$CellArea = "R".$RowNumber;
		$a = $HojoRowSum + 1;
		// =AS452+AS453
		$formulae = "=AS$HojoRowSum+AS$a";
		$excelObj->SetValuewith($CellArea, $formulae, "BOLD");
		$excelObj->SetFont($this->MSPgothic ,12); 
		$excelObj->SetFormatStyle("0.00");
		$excelObj->SetPosV("CENTER");

		// 中間集計２行目
		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber,18.75);

		$excelObj->CellMerge("C".$RowNumber.":H".$RowNumber);
		$excelObj->SetBorder2("medium","","outline");

		$excelObj->SetValueJP2("C".$RowNumber, "月延べ夜勤時間数　〔D−E〕", "BOLD VCENTER",$this->MSPgothic ,14); 

		$excelObj->CellMerge2("I".$RowNumber.":K".$RowNumber,"medium","outline","FFFFCC"); // もっと薄い黄色

		$CellArea = "I".$RowNumber;
		$a = $HojoRowSum + 1;
		// =R457-AE457
		$formulae = "=R$HMidSumRow2-AE$HMidSumRow2";
		$excelObj->SetValuewith($CellArea, $formulae, "BOLD");
		$excelObj->SetFont($this->MSPgothic ,12); 
		$excelObj->SetFormatStyle("0.00");
		$excelObj->SetPosV("CENTER");

		$excelObj->CellMerge("L".$RowNumber.":Q".$RowNumber);
		$excelObj->SetBorder2("medium","","outline");

		$excelObj->SetValueJP2("L".$RowNumber, "月延べ夜勤時間数の計〔Ｄ〕", "BOLD VCENTER",$this->MSPgothic ,14); 

		$excelObj->CellMerge2("R".$RowNumber.":S".$RowNumber,"medium","outline","FFFFCC"); // もっと薄い黄色

		$CellArea = "R".$RowNumber;
		$a = $HojoRowSum + 1;
		// =AS453
		$formulae = "=AS$a";
		$excelObj->SetValuewith($CellArea, $formulae, "BOLD");
		$excelObj->SetFont($this->MSPgothic ,12); 
		$excelObj->SetFormatStyle("0.00");
		$excelObj->SetPosV("CENTER");

		$excelObj->CellMerge("T".$RowNumber.":AD".$RowNumber);
		$excelObj->SetBorder2("medium","","outline");

		$excelObj->SetValueJP2("T".$RowNumber, "夜勤専従者及び月16時間以下の者の夜勤時間数〔E〕", "BOLD VCENTER",$this->MSPgothic ,12); 

		$excelObj->CellMerge2("AE".$RowNumber.":AF".$RowNumber,"medium","outline","FFFFCC"); // もっと薄い黄色

		$CellArea = "AE".$RowNumber;
		$a = $HojoRowSum + 1;
		// =AT453
		$formulae = "=AT$a";
		$excelObj->SetValuewith($CellArea, $formulae, "BOLD");
		$excelObj->SetFont($this->MSPgothic ,12); 
		$excelObj->SetFormatStyle("0.00");
		$excelObj->SetPosV("CENTER");

		// 中間集計３行目
		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber,18.75);

		$excelObj->CellMerge("C".$RowNumber.":K".$RowNumber);
		$excelObj->SetBorder2("medium","","outline");

		$excelObj->SetValueJP2("C".$RowNumber, "1日看護補助配置数〔(A ／20)×３〕", "BOLD VCENTER",$this->MSPgothic ,14); 

		$CellArea = "L".$RowNumber;
		// =AS453
		$formulae = "=J16";
		$excelObj->SetValuewith($CellArea, $formulae, "BOLD");
		$excelObj->SetFont($this->MSPgothic ,12); 
		$excelObj->SetFormatStyle("0.00");
		$excelObj->SetPosV("CENTER");
		$excelObj->SetBorder2("medium","","outline");
		$excelObj->SetBackColor("FFFFCC"); // もっと薄い黄色

		$excelObj->CellMerge("M".$RowNumber.":V".$RowNumber);
		$excelObj->SetBorder2("medium","","outline");

		$excelObj->SetValueJP2("M".$RowNumber, "月平均１日当たり看護補助配置数〔C／(日数×８）〕", "BOLD VCENTER",$this->MSPgothic ,14); 

		$excelObj->CellMerge2("W".$RowNumber.":X".$RowNumber,"medium","outline","FFFFCC"); // もっと薄い黄色

		$CellArea = "W".$RowNumber;
		// =ROUNDDOWN(R456/(M28*8),1)
		$formulae = "=ROUNDDOWN(R$HMidSumRow1/(M28*8),1)";
		$excelObj->SetValuewith($CellArea, $formulae, "BOLD");
		$excelObj->SetFont($this->MSPgothic ,12); 
		$excelObj->SetFormatStyle("0.00");
		$excelObj->SetPosV("CENTER");

		// 空行3行
		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber,13.5);
		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber,13.5);
		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber,13.5);

		$arg_array["HMidSumRow1"] = $HMidSumRow1;
		$arg_array["HMidSumRow2"] = $HMidSumRow2;
		$arg_array["HMidSumRow3"] = $HMidSumRow3;
		$arg_array["RowNumber"] = $RowNumber;

		return $arg_array;
	}
	//////////////////////////
	//			//
	// エクセルシートの調整 //
	//			//
	//////////////////////////
	function SetExcelSheet($excelObj,$arg_array){
		// 非表示列を画面から消す
		$excelObj->SetValueJPwith("AW1","非表示列","wrap");
		$excelObj->SetValueJPwith("AX1","非表示列","wrap");
		$excelObj->SetValueJPwith("AY1","非表示列","wrap");
		$excelObj->SetValueJPwith("AZ1","非表示列","wrap");
		$excelObj->SetArea("AW1:AZ1");
		$excelObj->SetCharColor("FFFF0000"); // 赤文字 argvで指定、alpha値は機能しない
		$excelObj->SetBackColor("C0C0C0"); // 灰色
		$excelObj->SetFont($this->MSPgothic,11); 
		$excelObj->SetPosV("CENTER");
		
		$excelObj->SetColDim("AW",-0.62);
		$excelObj->SetColDim("AX",-0.62);
		$excelObj->SetColDim("AY",-0.62);
		$excelObj->SetColDim("AZ",-0.62);
		
		// 印刷範囲設定
		if ( $arg_array["HMidSumRow3"]  != 0 ){
			$EndArea = $arg_array["HMidSumRow3"];
		}else{
			$EndArea = $arg_array["HMidSumRow2"];
		}
		$excelObj->SetPritingArea("A1:AV".$EndArea);
		// 印刷ヘッダー設定
		$HeaderMessage = '&L作成　(社)日本看護協会　NS100';
		$HeaderMessage = $HeaderMessage . "&R平成２２年度診療報酬改定対応版\n更新：平成２３年１月";
		$excelObj->SetPrintHeader( $HeaderMessage );
		
		// エクセル画面を開いたときのカーソル位置を左上に設定
		$excelObj->SetArea("A1");
		$excelObj->SetPosV("");
	}
}
/* DEBUG情報

看護協会版届出様式
・一般病棟入院基本料表
・療養病棟入院基本料表１
・療養病棟入院基本料表２

(△印はブランクを示します)

  入院基本料の施設基準等に係る届出書添付書類(様式９)
  …………………
　……(中略)……
  …………………
 △…………△※今月の稼働日数△…………△[ ]日	 <-- $DayCntRow = $KangoRowTop - 4;
 [  ]月△…△※常勤職員の週所定労働時間△[ ]時間 <-- $LaborCntRow = $KangoRowTop - 3;
【勤務計画表】
《看護職員表》
+--------------------------
|見出し3行			<-- 見出しトップ行 $KangoRowTop
+--------------------------
|職員毎明細データ行		<-- データトップ行 $KangoRowDataTop
|
|	データ件数は count($data_array)
|
+--------------------------
|小計				<-- $KangoRowSmallSum (上段)
+-+------------------------
△|計				<-- $KangoRowSum (上段)
　+------------------------
(空行1行)
△…△[夜勤従事者……	<- $KMidSumRow1
△…△[月延べ夜勤……	<- $KMidSumRow2
△…△[1日看護配……	<- $KMidSumRow3

(空行3行)

《看護補助者表》
 +-------------------------
 |見出し3行			<-- 見出しトップ行 $HojoRowTop
 +-------------------------
 |職員毎明細データ行		<-- データトップ行 $HojoRowDataTop
 |
 |	データ件数は count($data_sub_array)
 |
 +-------------------------
 |小計				<-- $HojoRowSmallSum (上段)
 +-------------------------
 |計				<-- $HojoRowSum (上段)
 +-------------------------
(空行1行)
 ※……				左側の文言は一般病棟と療養病棟で異なります
 [……				<-- $HMidSumRow1
 [………			<-- $HMidSumRow2
 [………			<-- $HMidSumRow3 (一般病棟にはありません)

(空行3行)

△…△[参考][月総夜勤時間……
△……………△※夜勤専従者数は……
(空行1行)
△……………△【看護職員】
△……………△ (みだし)
△……………△ (計算式)			<-- $GSumRow1
(空行1行)
△……………△【看護補助者】
△……………△ (みだし)
△……………△ (計算式)			<-- $GSumRow2
(空行1行)
△……………△【看護職員＋看護補助者】
△……………△ (みだし)
△……………△ (計算式)			<-- $GSumRow3


・計算式・書式等は必ず看護協会の雛形と照合してください。
・罫線はなるべくまとめて範囲指定で引くようにしないと遅いです
・行位置はデータ件数から算出できますが実行数をカウントしています。

at 2012.如月吉日
*/
?>