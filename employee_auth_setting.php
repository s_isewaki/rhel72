<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 職員登録 | 権限設定</title>
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.ini");
require_once("label_by_profile_type.ini");
require_once("get_cas_title_name.ini");
require_once("webmail_alias_functions.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 職員参照権限チェック
$checkauth = check_authority($session, 18, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 職員登録権限を取得
$emp_reg_auth = check_authority($session, 19, $fname);

// データベースに接続
$con = connect2db($fname);

// 職員情報を取得
$emp_info = get_empmst($con, $emp_id, $fname);

// 職員の権限情報を取得
$sql = "select * from authmst";
$cond = "where emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$tmp_emp_auth = pg_fetch_array($sel, 0, PGSQL_ASSOC);
$emp_auth = array();
foreach ($tmp_emp_auth as $tmp_column => $tmp_value) {
    $tmp_column = preg_replace("/^emp_(.*_flg)$/", "$1", $tmp_column);
    $emp_auth[$tmp_column] = $tmp_value;
}
$group_id = $emp_auth["group_id"];

// 権限グループ一覧を配列に格納
$sql = "select * from authgroup";
$cond = "order by group_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$groups = array();
for ($i = 0, $j = pg_num_rows($sel); $i < $j; $i++) {
    $tmp_group_id = pg_fetch_result($sel, $i, "group_id");
    $groups[$tmp_group_id] = pg_fetch_array($sel, $i, PGSQL_ASSOC);
}

// 追加権限に絞り込む
if ($group_id != "") {
    foreach ($groups[$group_id] as $tmp_column => $tmp_value) {
        if (preg_match("/.*_flg$/", $tmp_column) > 0 && array_key_exists($tmp_column, $emp_auth) && $tmp_value == "t") {
            $emp_auth[$tmp_column] = "f";
        }
    }
}

// ライセンス情報の取得
$sql = "select * from license";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
for ($i = 1; $i <= 60; $i++) { // 2014/2/2 19から60に変更
    $var_name = "func$i";
    $$var_name = pg_fetch_result($sel, 0, "lcs_func$i");
}

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// イントラメニュー名を取得
$sql = "select * from intramenu";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$intra_menu1 = pg_fetch_result($sel, 0, "menu1");
// 当直・外来分担表
$intra_menu2_4 = pg_fetch_result($sel, 0, "menu2_4");
// 社会福祉法人の場合、「・外来」を削除
if ($profile_type == PROFILE_TYPE_WELFARE) {
    $intra_menu2_4 = mbereg_replace("・外来", "", $intra_menu2_4);
}
$intra_menu3 = pg_fetch_result($sel, 0, "menu3");

require_once("get_menu_label.ini");
$report_menu_label = get_report_menu_label($con, $fname);
$shift_menu_label = get_shift_menu_label($con, $fname);
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
var auths = [];
<?
foreach ($groups as $tmp_group_id => $tmp_group) {
    echo("auths[$tmp_group_id] = {");

    for ($i = 1; $j = count($tmp_group), $i <= $j; $i++) {
        list($tmp_column,  $tmp_value) = each($tmp_group);
        echo("'$tmp_column': '$tmp_value'");
        if ($i < $j) {
            echo(", ");
        }
    }

    echo("};\n");
}
?>

function initPage() {
    setPtRefAuth();
    setPtOutRefAuth();
    setPtDisRefAuth();
    setIntraAuth();
    setQAAuth();
//XX     setCmtAuth();
    setEmpRefAuth();
    setAmbAuth();
    setWardAuth();
    setInciAuth();
    setFPlusAuth();
    setManabuAuth();
    setMedAuth();
    setKViewAuth();
    setSchdAuth();
    setFclAuth();
    setLinkAuth();
    setBbsAuth();
    setAdbkAuth();
    setWebmlAuth();
    setShiftAuth();
    setShift2Auth();
    setCasAuth();
    setJinjiAuth();
    setLadderAuth();
    setCareerAuth();
    setJnlAuth();
    setNlcsAuth();
    setPjtAuth();
    setCAuthAuth();
    <? if ($func31=="t") { ?>setCCUsr1Auth();<? } ?>
    emphasizeAddables();
}

function setPtRefAuth() {
    setSubBox(document.employee.ptif, document.employee.ptreg.checked, '<? echo($func1); ?>');
}

function setPtOutRefAuth() {
    setSubBox(document.employee.inout, document.employee.outreg.checked, '<? echo($func1); ?>');
}

function setPtDisRefAuth() {
    setSubBox(document.employee.dishist, document.employee.disreg.checked, '<? echo($func1); ?>');
}

function setIntraAuth() {
    var check_flag = (
        document.employee.ymstat.checked ||
        document.employee.intram.checked ||
        document.employee.stat.checked ||
        document.employee.allot.checked ||
        document.employee.life.checked
    );
    setSubBox(document.employee.intra, check_flag, '<? echo($func4); ?>');
}

function setQAAuth() {
    setSubBox(document.employee.qa, document.employee.qaadm.checked, 't');
}

//XX function setCmtAuth() {
//XX     setSubBox(document.employee.cmt, document.employee.cmtadm.checked, 't');
//XX }

function setEmpRefAuth() {
    setSubBox(document.employee.empif, document.employee.reg.checked, 't');
}

function setAmbAuth() {
    setSubBox(document.employee.amb, document.employee.ambadm.checked, '<? echo($func15); ?>');
}

function setWardAuth() {
    if (document.employee.ward_reg) {
        setSubBox(document.employee.ward, document.employee.ward_reg.checked, '<? echo($func2); ?>');
    }
}

function setInciAuth() {
    setSubBox(document.employee.inci, document.employee.rm.checked, '<? echo($func5); ?>');
}

function setFPlusAuth() {
    setSubBox(document.employee.fplus, document.employee.fplusadm.checked, '<? echo($func13); ?>');
}

function setManabuAuth() {
    setSubBox(document.employee.manabu, document.employee.manabuadm.checked, '<? echo($func12); ?>');
}

function setMedAuth() {
    setSubBox(document.employee.med, document.employee.medadm.checked, '<? echo($func6); ?>');
}

function setKViewAuth() {
    setSubBox(document.employee.kview, document.employee.kviewadm.checked, '<? echo($func11); ?>');
}

function setSchdAuth() {
    setSubBox(document.employee.schd, document.employee.schdplc.checked, 't');
}

function setFclAuth() {
    setSubBox(document.employee.resv, document.employee.fcl.checked, 't');
}

function setLinkAuth() {
    setSubBox(document.employee.link, document.employee.linkadm.checked, 't');
}

function setBbsAuth() {
    setSubBox(document.employee.bbs, document.employee.bbsadm.checked, 't');
}

function setAdbkAuth() {
    setSubBox(document.employee.adbk, document.employee.adbkadm.checked, 't');
}

function setWebmlAuth() {
    setSubBox(document.employee.webml, document.employee.webmladm.checked, 't');
}

function setShiftAuth() {
    setSubBox(document.employee.shift, document.employee.shiftadm.checked, '<? echo($func9); ?>');
}

function setShift2Auth() {
    setSubBox(document.employee.shift2, document.employee.shift2adm.checked, '<? echo($func18); ?>');
}

function setCasAuth() {
    setSubBox(document.employee.cas, document.employee.casadm.checked, '<? echo($func8); ?>');
}

function setJinjiAuth() {
    setSubBox(document.employee.jinji, document.employee.jinjiadm.checked, '<? echo($func16); ?>');
}

function setLadderAuth() {
    setSubBox(document.employee.ladder, document.employee.ladderadm.checked, '<? echo($func17); ?>');
}

function setCareerAuth() {
    setSubBox(document.employee.career, document.employee.careeradm.checked, '<? echo($func19); ?>');
}

function setJnlAuth() {
    setSubBox(document.employee.jnl, document.employee.jnladm.checked, '<? echo($func14); ?>');
}

function setNlcsAuth() {
    setSubBox(document.employee.nlcs, document.employee.nlcsadm.checked, '<? echo($func10); ?>');
}

function setPjtAuth() {
    setSubBox(document.employee.pjt, document.employee.pjtadm.checked, 't');
}

function setCAuthAuth() {
    setSubBox(document.employee.pass, document.employee.cauth.checked, 't');
}
function setCCUsr1Auth() {
    setSubBox(document.employee.ccusr1, document.employee.ccadm1.checked, '<? echo($func31); ?>');
}

function setSubBox(sub_box, check_flag, sub_box_checkable) {
    if (check_flag) {
        sub_box.checked = true;
        sub_box.disabled = true;
    } else {
        sub_box.disabled = (sub_box_checkable != 't');
    }
}

function emphasizeAddables() {
    var group_id = document.employee.group_id.value;
    var paleLabelExists = false;

    var elements = document.getElementsByTagName('input');
    for (var i = 0, j = elements.length; i < j; i++) {
        if (elements[i].type != 'checkbox') {
            continue;
        }

        var box_name = elements[i].name;
        var cell = document.getElementById(box_name.concat('_cell'));
        if (!cell) {
            continue;
        }

        if (group_id == '' || auths[group_id][box_name.concat('_flg')] != 't') {
            cell.style.backgroundColor = 'white';
        } else {
            cell.style.backgroundColor = 'silver';
            paleLabelExists = true;
        }
    }

    document.getElementById('info').style.color = (paleLabelExists) ? 'black' : 'white';
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
.list td .inner {border-collapse:collapse;}
.list td .inner td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="employee_info_menu.php?session=<? echo($session); ?>"><img src="img/icon/b26.gif" width="32" height="32" border="0" alt="職員登録"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="employee_info_menu.php?session=<? echo($session); ?>"><b>職員登録</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_info_menu.php?session=<? echo($session); ?>&key1=<? echo(urlencode($key1)); ?>&key2=<? echo(urlencode($key2)); ?>&page=<? echo($page); ?>&view=<? echo($view); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">検索</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_list.php?session=<? echo($session); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&view=<? echo($view); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_detail.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&view=<? echo($view); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基本</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="employee_contact_setting.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&view=<? echo($view); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">連絡先</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_condition_setting.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&view=<? echo($view); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務条件</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#5279a5"><a href="employee_auth_setting.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&view=<? echo($view); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>権限</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_display_setting.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&view=<? echo($view); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示設定</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_menu_setting.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&view=<? echo($view); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メニュー設定</font></a></td>
<? if (webmail_alias_enabled()) { ?>
<td width="5">&nbsp;</td>
<td width="130" align="center" bgcolor="#bdd1e7"><a href="employee_alias_setting.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&view=<? echo($view); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メール転送設定</font></a></td>
<? } ?>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form name="employee" action="employee_auth_setting_confirm.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員ID</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($emp_info[1]); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">名前</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($emp_info[7] . " " . $emp_info[8]); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限グループ</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="group_id" onchange="emphasizeAddables();">
<option value="">　　　　　</option>
<?
foreach ($groups as $tmp_group_id => $tmp_group) {
    echo("<option value=\"$tmp_group_id\"");
    if ($tmp_group_id == $group_id) {
        echo(" selected");
    }
    echo(">{$tmp_group["group_nm"]}\n");
}
?>
</select><span id="info" style="color:white;margin-left:5px;">※背景が灰色の機能はグループで選択済みです。チェックは無視されます</span></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">追加権限</font></td>
<td>
<table border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>基本機能</b></font></td>
</tr>
</table>
<table width="50%" border="0" cellspacing="0" cellpadding="2" class="inner">
<tr height="22" bgcolor="#f6f9ff">
<td></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ユーザ機能</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">管理者機能</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ウェブメール</font></td>
<td align="center" id="webml_cell"><input name="webml" type="checkbox" value="t"<? if ($emp_auth["webml_flg"] == "t") {echo(" checked");} ?>></td>
<td align="center" id="webmladm_cell"><input name="webmladm" type="checkbox" value="t"<? if ($emp_auth["webmladm_flg"] == "t") {echo(" checked");} ?> onclick="setWebmlAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">スケジュール</font></td>
<td align="center" id="schd_cell"><input name="schd" type="checkbox" value="t"<? if ($emp_auth["schd_flg"] == "t") {echo(" checked");} ?>></td>
<td align="center" id="schdplc_cell"><input name="schdplc" type="checkbox" value="t"<? if ($emp_auth["schdplc_flg"] == "t") {echo(" checked");} ?> onclick="setSchdAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">委員会・WG</font></td>
<td align="center" id="pjt_cell"><input name="pjt" type="checkbox" value="t"<? if ($emp_auth["pjt_flg"] == "t") {echo(" checked");} ?>></td>
<td align="center" id="pjtadm_cell"><input name="pjtadm" type="checkbox" value="t"<? if ($emp_auth["pjtadm_flg"] == "t") {echo(" checked");} ?> onclick="setPjtAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ・回覧板</font></td>
<td align="center" id="newsuser_cell"><input name="newsuser" type="checkbox" value="t"<? if ($emp_auth["newsuser_flg"] == "t") {echo(" checked");} ?>></td>
<td align="center" id="news_cell"><input name="news" type="checkbox" value="t"<? if ($emp_auth["news_flg"] == "t") {echo(" checked");} ?>></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タスク</font></td>
<td align="center" id="work_cell"><input name="work" type="checkbox" value="t"<? if ($emp_auth["work_flg"] == "t") {echo(" checked");} ?>></td>
<td></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">伝言メモ</font></td>
<td align="center" id="phone_cell"><input name="phone" type="checkbox" value="t"<? if ($emp_auth["phone_flg"] == "t") {echo(" checked");} ?>></td>
<td></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">設備予約</font></td>
<td align="center" id="resv_cell"><input name="resv" type="checkbox" value="t"<? if ($emp_auth["resv_flg"] == "t") {echo(" checked");} ?>></td>
<td align="center" id="fcl_cell"><input name="fcl" type="checkbox" value="t"<? if ($emp_auth["fcl_flg"] == "t") {echo(" checked");} ?> onclick="setFclAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤表／勤務管理</font></td>
<td align="center" id="attdcd_cell"><input name="attdcd" type="checkbox" value="t"<? if ($emp_auth["attdcd_flg"] == "t") {echo(" checked");} ?>></td>
<td align="center" id="wkadm_cell"><input name="wkadm" type="checkbox" value="t"<? if ($emp_auth["wkadm_flg"] == "t") {echo(" checked");} ?>></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">決裁・申請／ワークフロー</font></td>
<td align="center" id="aprv_cell"><input name="aprv" type="checkbox" value="t"<? if ($emp_auth["aprv_flg"] == "t") {echo(" checked");} ?>></td>
<td align="center" id="wkflw_cell"><input name="wkflw" type="checkbox" value="t"<? if ($emp_auth["wkflw_flg"] == "t") {echo(" checked");} ?>></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ネットカンファレンス</font></td>
<td align="center" id="conf_cell"><input name="conf" type="checkbox" value="t"<? if ($emp_auth["conf_flg"] == "t") {echo(" checked");} ?>></td>
<td></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">掲示板・電子会議室</font></td>
<td align="center" id="bbs_cell"><input name="bbs" type="checkbox" value="t"<? if ($emp_auth["bbs_flg"] == "t") {echo(" checked");} ?>></td>
<td align="center" id="bbsadm_cell"><input name="bbsadm" type="checkbox" value="t"<? if ($emp_auth["bbsadm_flg"] == "t") {echo(" checked");} ?> onclick="setBbsAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">Q&amp;A</font></td>
<td align="center" id="qa_cell"><input name="qa" type="checkbox" value="t"<? if ($emp_auth["qa_flg"] == "t") {echo(" checked");} ?>></td>
<td align="center" id="qaadm_cell"><input name="qaadm" type="checkbox" value="t"<? if ($emp_auth["qaadm_flg"] == "t") {echo(" checked");} ?> onclick="setQAAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文書管理</font></td>
<td align="center" id="lib_cell"><input name="lib" type="checkbox" value="t"<? if ($emp_auth["lib_flg"] == "t") {echo(" checked");} ?>></td>
<td align="center" id="libadm_cell"><input name="libadm" type="checkbox" value="t"<? if ($emp_auth["libadm_flg"] == "t") {echo(" checked");} ?>></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">アドレス帳</font></td>
<td align="center" id="adbk_cell"><input name="adbk" type="checkbox" value="t"<? if ($emp_auth["adbk_flg"] == "t") {echo(" checked");} ?>></td>
<td align="center" id="adbkadm_cell"><input name="adbkadm" type="checkbox" value="t"<? if ($emp_auth["adbkadm_flg"] == "t") {echo(" checked");} ?> onclick="setAdbkAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">内線電話帳</font></td>
<td align="center" id="ext_cell"><input name="ext" type="checkbox" value="t"<? if ($emp_auth["ext_flg"] == "t") {echo(" checked");} ?>></td>
<td align="center" id="extadm_cell"><input name="extadm" type="checkbox" value="t"<? if ($emp_auth["extadm_flg"] == "t") {echo(" checked");} ?>></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">リンクライブラリ</font></td>
<td align="center" id="link_cell"><input name="link" type="checkbox" value="t"<? if ($emp_auth["link_flg"] == "t") {echo(" checked");} ?>></td>
<td align="center" id="linkadm_cell"><input name="linkadm" type="checkbox" value="t"<? if ($emp_auth["linkadm_flg"] == "t") {echo(" checked");} ?> onclick="setLinkAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">パスワード変更</font></td>
<td align="center" id="pass_cell"><input name="pass" type="checkbox" value="t"<? if ($emp_auth["pass_flg"] == "t") {echo(" checked");} ?>></td>
<td></td>
</tr>
<!-- tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">コミュニティサイト</font></td>
<td align="center" id="cmt_cell"><input name="cmt" type="checkbox" value="t"<? if ($emp_auth["cmt_flg"] == "t") {echo(" checked");} ?>></td>
<td align="center" id="cmtadm_cell"><input name="cmtadm" type="checkbox" value="t"<? if ($emp_auth["cmtadm_flg"] == "t") {echo(" checked");} ?> onclick="setCmtAuth();"></td>
</tr -->
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">備忘録</font></td>
<td align="center" id="memo_cell"><input name="memo" type="checkbox" value="t"<? if ($emp_auth["memo_flg"] == "t") {echo(" checked");} ?>></td>
<td></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">端末認証</font></td>
<td align="center" id="cauth_cell"><input name="cauth" type="checkbox" value="t"<? if ($emp_auth["cauth_flg"] == "t") {echo(" checked");} ?> onclick="setCAuthAuth();"></td>
<td></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="10"><br>
<table border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>管理機能</b></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="inner">
<tr height="22" bgcolor="#f6f9ff">
<td></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ユーザ機能</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">管理者機能</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員参照</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">組織プロフィール</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">組織</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職種</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">役職</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カレンダー</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 院内行事登録／法人内行事登録
echo($_label_by_profile["EVENT_REG"][$profile_type]);
?></font></td>
<td></td>
<td align="center" id="tmgd_cell"><input name="tmgd" type="checkbox" value="t"<? if ($emp_auth["tmgd_flg"] == "t") {echo(" checked");} ?>></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員登録</font></td>
<td></td>
<td align="center" id="reg_cell"><input name="reg" type="checkbox" value="t"<? if ($emp_auth["reg_flg"] == "t") {echo(" checked");} ?> onclick="setEmpRefAuth();"></td>
<td align="center" id="empif_cell"><input name="empif" type="checkbox" value="t"<? if ($emp_auth["empif_flg"] == "t") {echo(" checked");} ?>></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">マスターメンテナンス</font></td>
<td></td>
<td></td>
<td></td>
<td align="center" id="hspprf_cell"><input name="hspprf" type="checkbox" value="t"<? if ($emp_auth["hspprf_flg"] == "t") {echo(" checked");} ?>></td>
<td align="center" id="dept_cell"><input name="dept" type="checkbox" value="t"<? if ($emp_auth["dept_flg"] == "t") {echo(" checked");} ?>></td>
<td align="center" id="job_cell"><input name="job" type="checkbox" value="t"<? if ($emp_auth["job_flg"] == "t") {echo(" checked");} ?>></td>
<td align="center" id="status_cell"><input name="status" type="checkbox" value="t"<? if ($emp_auth["status_flg"] == "t") {echo(" checked");} ?>></td>
<td align="center" id="attditem_cell"><input name="attditem" type="checkbox" value="t"<? if ($emp_auth["attditem_flg"] == "t") {echo(" checked");} ?>></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">環境設定</font></td>
<td></td>
<td align="center" id="config_cell"><input name="config" type="checkbox" value="t"<? if ($emp_auth["config_flg"] == "t") {echo(" checked");} ?>></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ライセンス管理</font></td>
<td></td>
<td align="center" id="lcs_cell"><input name="lcs" type="checkbox" value="t"<? if ($emp_auth["lcs_flg"] == "t") {echo(" checked");} ?>></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">アクセスログ</font></td>
<td></td>
<td align="center" id="accesslog_cell"><input name="accesslog" type="checkbox" value="t"<? if ($emp_auth["accesslog_flg"] == "t") {echo(" checked");} ?>></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="10"><br>
<table border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>業務機能</b></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="inner">
<tr height="22" bgcolor="#f6f9ff">
<td></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 患者登録／利用者登録
echo($_label_by_profile["PATIENT_REG"][$profile_type]);
?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 患者基本情報／利用者基本情報
echo($_label_by_profile["PATIENT_INFO"][$profile_type]);
?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 来院登録／来所登録
echo($_label_by_profile["OUT_REG"][$profile_type]);
?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 入院来院履歴／入所来所履歴
echo($_label_by_profile["IN_OUT"][$profile_type]);
?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">プロブレムリスト</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">プロブレム</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">管理者権限</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 患者管理／利用者管理
echo($_label_by_profile["PATIENT_MANAGE"][$profile_type]);
?></font></td>
<td align="center" id="ptreg_cell"><input name="ptreg" type="checkbox" value="t"<? if ($emp_auth["ptreg_flg"] == "t") {echo(" checked");} ?><? if ($func1 == "f") {echo(" disabled");} ?> onclick="setPtRefAuth();"></td>
<td align="center" id="ptif_cell"><input name="ptif" type="checkbox" value="t"<? if ($emp_auth["ptif_flg"] == "t") {echo(" checked");} ?><? if ($func1 == "f") {echo(" disabled");} ?>></td>
<td align="center" id="outreg_cell"><input name="outreg" type="checkbox" value="t"<? if ($emp_auth["outreg_flg"] == "t") {echo(" checked");} ?><? if ($func1 == "f") {echo(" disabled");} ?> onclick="setPtOutRefAuth();"></td>
<td align="center" id="inout_cell"><input name="inout" type="checkbox" value="t"<? if ($emp_auth["inout_flg"] == "t") {echo(" checked");} ?><? if ($func1 == "f") {echo(" disabled");} ?>></td>
<td align="center" id="disreg_cell"><input name="disreg" type="checkbox" value="t"<? if ($emp_auth["disreg_flg"] == "t") {echo(" checked");} ?><? if ($func1 == "f") {echo(" disabled");} ?> onclick="setPtDisRefAuth();"></td>
<td align="center" id="dishist_cell"><input name="dishist" type="checkbox" value="t"<? if ($emp_auth["dishist_flg"] == "t") {echo(" checked");} ?><? if ($func1 == "f") {echo(" disabled");} ?>></td>
<td align="center" id="ptadm_cell"><input name="ptadm" type="checkbox" value="t"<? if ($emp_auth["ptadm_flg"] == "t") {echo(" checked");} ?><? if ($func1 == "f") {echo(" disabled");} ?>></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="8"><br>
<table width="50%" border="0" cellspacing="0" cellpadding="2" class="inner">
<tr height="22" bgcolor="#f6f9ff">
<td></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ユーザ機能</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">管理者機能</font></td>
</tr>





<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">検索ちゃん</font></td>
<td align="center" id="search_cell"><input name="search" type="checkbox" value="t"<? if ($emp_auth["search_flg"] == "t") {echo(" checked");} ?><? if ($func7 == "f") {echo(" disabled");} ?>></td>
<td></td>
</tr>



<? if ($func31=="t") { ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">スタッフ・ポートフォリオ</font></td>
<td align="center" id="ccusr1_cell"><input name="ccusr1" type="checkbox" value="t"<? if ($emp_auth["ccusr1_flg"] == "t") {echo(" checked");} ?><? if ($func31 == "f") {echo(" disabled");} ?>></td>
<td align="center" id="ccadm1_cell">
<input name="ccadm1" type="checkbox" value="t"<? if ($emp_auth["ccadm1_flg"] == "t") {echo(" checked");} ?><? if ($func31 == "f") {echo(" disabled");} ?> onclick="setCCUsr1Auth();">
</td>
</tr>
<? } ?>



<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">看護支援</font></td>
<td align="center" id="amb_cell"><input name="amb" type="checkbox" value="t"<? if ($emp_auth["amb_flg"] == "t") {echo(" checked");} ?><? if ($func15 == "f") {echo(" disabled");} ?>></td>
<td align="center" id="ambadm_cell">
<input name="ambadm" type="checkbox" value="t"<? if ($emp_auth["ambadm_flg"] == "t") {echo(" checked");} ?><? if ($func15 == "f") {echo(" disabled");} ?> onclick="setAmbAuth();">
</td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 病床管理
echo($_label_by_profile["BED_MANAGE"][$profile_type]);
?></font></td>
<td align="center" id="ward_cell"><input name="ward" type="checkbox" value="t"<? if ($emp_auth["ward_flg"] == "t") {echo(" checked");} ?><? if ($func2 == "f") {echo(" disabled");} ?>></td>
<td align="center" id="ward_reg_cell">
<? if ($profile_type != "2") { ?>
<input name="ward_reg" type="checkbox" value="t"<? if ($emp_auth["ward_reg_flg"] == "t") {echo(" checked");} ?><? if ($func2 == "f") {echo(" disabled");} ?> onclick="setWardAuth();">
<? } ?>
</td>
</tr>
<? if ($profile_type != "2") { ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 診療科／担当部門
echo($_label_by_profile["SECTION"][$profile_type]);
?></font></td>
<td></td>
<td align="center" id="entity_cell">
<input name="entity" type="checkbox" value="t"<? if ($emp_auth["entity_flg"] == "t") {echo(" checked");} ?><? if ($func2 == "f" && $func6 == "f") {echo(" disabled");} ?>>
</td>
</tr>
<? } ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">看護観察記録</font></td>
<td align="center" id="nlcs_cell"><input name="nlcs" type="checkbox" value="t"<? if ($emp_auth["nlcs_flg"] == "t") {echo(" checked");} ?><? if ($func10 == "f") {echo(" disabled");} ?>></td>
<td align="center" id="nlcsadm_cell"><input name="nlcsadm" type="checkbox" value="t"<? if ($emp_auth["nlcsadm_flg"] == "t") {echo(" checked");} ?><? if ($func10 == "f") {echo(" disabled");} ?> onclick="setNlcsAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ファントルくん</font></td>
<td align="center" id="inci_cell"><input name="inci" type="checkbox" value="t"<? if ($emp_auth["inci_flg"] == "t") {echo(" checked");} ?><? if ($func5 == "f") {echo(" disabled");} ?>></td>
<td align="center" id="rm_cell"><input name="rm" type="checkbox" value="t"<? if ($emp_auth["rm_flg"] == "t") {echo(" checked");} ?><? if ($func5 == "f") {echo(" disabled");} ?> onclick="setInciAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ファントルくん＋</font></td>
<td align="center" id="fplus_cell"><input name="fplus" type="checkbox" value="t"<? if ($emp_auth["fplus_flg"] == "t") {echo(" checked");} ?><? if ($func13 == "f") {echo(" disabled");} ?>></td>
<td align="center" id="fplusadm_cell"><input name="fplusadm" type="checkbox" value="t"<? if ($emp_auth["fplusadm_flg"] == "t") {echo(" checked");} ?><? if ($func13 == "f") {echo(" disabled");} ?> onclick="setFPlusAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">バリテス</font></td>
<td align="center" id="manabu_cell"><input name="manabu" type="checkbox" value="t"<? if ($emp_auth["manabu_flg"] == "t") {echo(" checked");} ?><? if ($func12 == "f") {echo(" disabled");} ?>></td>
<td align="center" id="manabuadm_cell"><input name="manabuadm" type="checkbox" value="t"<? if ($emp_auth["manabuadm_flg"] == "t") {echo(" checked");} ?><? if ($func12 == "f") {echo(" disabled");} ?> onclick="setManabuAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// メドレポート／POレポート
echo($report_menu_label);
?></font></td>
<td align="center" id="med_cell"><input name="med" type="checkbox" value="t"<? if ($emp_auth["med_flg"] == "t") {echo(" checked");} ?><? if ($func6 == "f") {echo(" disabled");} ?>></td>
<td align="center" id="medadm_cell"><input name="medadm" type="checkbox" value="t"<? if ($emp_auth["medadm_flg"] == "t") {echo(" checked");} ?><? if ($func6 == "f") {echo(" disabled");} ?> onclick="setMedAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カルテビューワー</font></td>
<td align="center" id="kview_cell"><input name="kview" type="checkbox" value="t"<? if ($emp_auth["kview_flg"] == "t") {echo(" checked");} ?><? if ($func11 == "f") {echo(" disabled");} ?>></td>
<td align="center" id="kviewadm_cell"><input name="kviewadm" type="checkbox" value="t"<? if ($emp_auth["kviewadm_flg"] == "t") {echo(" checked");} ?><? if ($func11 == "f") {echo(" disabled");} ?> onclick="setKViewAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">経営支援</font></td>
<td align="center" id="biz_cell"><input name="biz" type="checkbox" value="t"<? if ($emp_auth["biz_flg"] == "t") {echo(" checked");} ?><? if ($func3 == "f") {echo(" disabled");} ?>></td>
<td></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 勤務シフト作成
echo($shift_menu_label);
?></font></td>
<td align="center" id="shift_cell"><input name="shift" type="checkbox" value="t"<? if ($emp_auth["shift_flg"] == "t") {echo(" checked");} ?><? if ($func9 == "f") {echo(" disabled");} ?>></td>
<td align="center" id="shiftadm_cell"><input name="shiftadm" type="checkbox" value="t"<? if ($emp_auth["shiftadm_flg"] == "t") {echo(" checked");} ?><? if ($func9 == "f") {echo(" disabled");} ?> onclick="setShiftAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務表</font></td>
<td align="center" id="shift2_cell"><input name="shift2" type="checkbox" value="t"<? if ($emp_auth["shift2_flg"] == "t") {echo(" checked");} ?><? if ($func18 == "f") {echo(" disabled");} ?>></td>
<td align="center" id="shift2adm_cell"><input name="shift2adm" type="checkbox" value="t"<? if ($emp_auth["shift2adm_flg"] == "t") {echo(" checked");} ?><? if ($func18 == "f") {echo(" disabled");} ?> onclick="setShift2Auth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// CAS
echo(get_cas_title_name());
?></font></td>
<td align="center" id="cas_cell"><input name="cas" type="checkbox" value="t"<? if ($emp_auth["cas_flg"] == "t") {echo(" checked");} ?><? if ($func8 == "f") {echo(" disabled");} ?>></td>
<td align="center" id="casadm_cell"><input name="casadm" type="checkbox" value="t"<? if ($emp_auth["casadm_flg"] == "t") {echo(" checked");} ?><? if ($func8 == "f") {echo(" disabled");} ?> onclick="setCasAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">人事管理</font></td>
<td align="center" id="jinji_cell"><input name="jinji" type="checkbox" value="t"<? if ($emp_auth["jinji_flg"] == "t") {echo(" checked");} ?><? if ($func16 == "f") {echo(" disabled");} ?>></td>
<td align="center" id="jinjiadm_cell"><input name="jinjiadm" type="checkbox" value="t"<? if ($emp_auth["jinjiadm_flg"] == "t") {echo(" checked");} ?><? if ($func16 == "f") {echo(" disabled");} ?> onclick="setJinjiAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">クリニカルラダー</font></td>
<td align="center" id="ladder_cell"><input name="ladder" type="checkbox" value="t"<? if ($emp_auth["ladder_flg"] == "t") {echo(" checked");} ?><? if ($func17 == "f") {echo(" disabled");} ?>></td>
<td align="center" id="ladderadm_cell"><input name="ladderadm" type="checkbox" value="t"<? if ($emp_auth["ladderadm_flg"] == "t") {echo(" checked");} ?><? if ($func17 == "f") {echo(" disabled");} ?> onclick="setLadderAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">キャリア開発ラダー</font></td>
<td align="center" id="career_cell"><input name="career" type="checkbox" value="t"<? if ($emp_auth["career_flg"] == "t") {echo(" checked");} ?><? if ($func19 == "f") {echo(" disabled");} ?>></td>
<td align="center" id="careeradm_cell"><input name="careeradm" type="checkbox" value="t"<? if ($emp_auth["careeradm_flg"] == "t") {echo(" checked");} ?><? if ($func19 == "f") {echo(" disabled");} ?> onclick="setCareerAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日報・月報</font></td>
<td align="center" id="jnl_cell"><input name="jnl" type="checkbox" value="t"<? if ($emp_auth["jnl_flg"] == "t") {echo(" checked");} ?><? if ($func14 == "f") {echo(" disabled");} ?>></td>
<td align="center" id="jnladm_cell"><input name="jnladm" type="checkbox" value="t"<? if ($emp_auth["jnladm_flg"] == "t") {echo(" checked");} ?><? if ($func14 == "f") {echo(" disabled");} ?> onclick="setJnlAuth();"></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="8"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="inner">
<tr height="22" bgcolor="#f6f9ff">
<td></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ユーザ機能</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">月間・年間<? echo($intra_menu1); ?>参照</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">イントラメニュー</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 稼働状況統計
echo($intra_menu1);
?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 当直・外来分担表
echo($intra_menu2_4);
?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 私たちの生活サポート
echo($intra_menu3);
?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">イントラネット</font></td>
<td align="center" id="intra_cell"><input name="intra" type="checkbox" value="t"<? if ($emp_auth["intra_flg"] == "t") {echo(" checked");} ?><? if ($func4 == "f") {echo(" disabled");} ?>></td>
<td align="center" id="ymstat_cell"><input name="ymstat" type="checkbox" value="t"<? if ($emp_auth["ymstat_flg"] == "t") {echo(" checked");} ?><? if ($func4 == "f") {echo(" disabled");} ?> onclick="setIntraAuth();"></td>
<td align="center" id="intram_cell"><input name="intram" type="checkbox" value="t"<? if ($emp_auth["intram_flg"] == "t") {echo(" checked");} ?><? if ($func4 == "f") {echo(" disabled");} ?> onclick="setIntraAuth();"></td>
<td align="center" id="stat_cell"><input name="stat" type="checkbox" value="t"<? if ($emp_auth["stat_flg"] == "t") {echo(" checked");} ?><? if ($func4 == "f") {echo(" disabled");} ?> onclick="setIntraAuth();"></td>
<td align="center" id="allot_cell"><input name="allot" type="checkbox" value="t"<? if ($emp_auth["allot_flg"] == "t") {echo(" checked");} ?><? if ($func4 == "f") {echo(" disabled");} ?> onclick="setIntraAuth();"></td>
<td align="center" id="life_cell"><input name="life" type="checkbox" value="t"<? if ($emp_auth["life_flg"] == "t") {echo(" checked");} ?><? if ($func4 == "f") {echo(" disabled");} ?> onclick="setIntraAuth();"></td>
</tr>
</table>
<!--
attd -> attdcd
aprv_use -> aprv
pass_flg -> pass
dept_reg -> dept
job_flg -> job
status_flg -> status
entity_flg -> entity
-->
<img src="img/spacer.gif" alt="" width="1" height="4"><br>
</td>
</tr>
</table>
<? if ($emp_reg_auth == 1) { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<? } ?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="emp_id" value="<? echo($emp_id); ?>">
<input type="hidden" name="key1" value="<? echo($key1); ?>">
<input type="hidden" name="key2" value="<? echo($key2); ?>">
<input type="hidden" name="class_cond" value="<? echo($class_cond); ?>">
<input type="hidden" name="atrb_cond" value="<? echo($atrb_cond); ?>">
<input type="hidden" name="dept_cond" value="<? echo($dept_cond); ?>">
<input type="hidden" name="room_cond" value="<? echo($room_cond); ?>">
<input type="hidden" name="job_cond" value="<? echo($job_cond); ?>">
<input type="hidden" name="page" value="<? echo($page); ?>">
<input type="hidden" name="view" value="<? echo($view); ?>">
<input type="hidden" name="emp_o" value="<? echo($emp_o); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
