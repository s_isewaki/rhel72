<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix オプション設定 | メール転送設定</title>
<?
require_once("about_comedix.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	js_login_exit();
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 転送先リストを取得
$sql = "select alias from webmail_alias";
$cond = "where emp_id = (select emp_id from session where session_id = '$session') order by alias";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	js_error_exit();
}
$aliases = array();
while ($row = pg_fetch_array($sel)) {
	$aliases[] = $row["alias"];
}

// データベース接続を閉じる
pg_close($con);
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function deleteAlias(alias) {
	if (confirm(alias + ' を削除してもよろしいでしょうか？')) {
		document.delform.del_alias.value = alias;
		document.delform.submit();
	}
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="option_register.php?session=<? echo($session); ?>"><img src="img/icon/b15.gif" width="32" height="32" border="0" alt="オプション設定"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="option_register.php?session=<? echo($session); ?>"><b>オプション設定</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="120" align="center" bgcolor="#bdd1e7"><a href="option_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示設定</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="option_menu_setting.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メニュー設定</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="option_mygroup.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">マイグループ</font></a></td>
<td width="5">&nbsp;</td>
<td width="130" align="center" bgcolor="#5279a5"><a href="option_alias.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>メール転送設定</b></font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<form name="mainform" action="option_alias_update.php" method="post">
<p><input type="text" name="alias" value="" size="50" style="ime-mode:disabled"> <input type="submit" value="転送先アドレスの登録"></p>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
<? if (count($aliases) > 0) { ?>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td valign="bottom" colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>転送先アドレス</b></font></td>
</tr>
</table>
<form name="delform" action="option_alias_update.php" method="post">
<table width="500" border="0" cellspacing="0" cellpadding="2">
<? foreach ($aliases as $alias) { ?>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? eh($alias); ?></font></td>
<td><input type="button" value="アドレスを削除" onclick="deleteAlias('<? eh_jsparam($alias); ?>');"></td>
</tr>
<? } ?>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="del_alias" value="">
</form>
<? } ?>
</td>
</tr>
</table>
</body>
</html>
