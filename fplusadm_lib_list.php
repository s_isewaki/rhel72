<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ファントルくん＋ | 管理画面 | テンプレート一覧</title>
<?
require_once("about_comedix.php");
require_once("show_fplusadm_lib_list.ini");
require_once("fplusadm_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// ワークフロー権限のチェック
$checkauth = check_authority($session, 79, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function deleteWorkflow() {
	if (document.wkfw.elements['del_lib[]'] == undefined) {
		alert('削除可能なデータが存在しません。');
		return;
	}

	var checked = false;

	if (document.wkfw.elements['del_lib[]'] != undefined) {
		if (document.wkfw.elements['del_lib[]'].length == undefined) {
			if (document.wkfw.elements['del_lib[]'].checked) {
				checked = true;
			}
		} else {
			for (var i = 0, j = document.wkfw.elements['del_lib[]'].length; i < j; i++) {
				if (document.wkfw.elements['del_lib[]'][i].checked) {
					checked = true;
					break;
				}
			}
		}
	}

	if (!checked) {
		alert('削除対象が選択されていません。');
		return;
	}

	if (confirm('削除します。よろしいですか？')) {
		document.wkfw.submit();
	}
}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;border:#E6B3D4 solid 1px;}
.list td {border:#E6B3D4 solid 1px;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#E6B3D4 solid 0px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">


<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>


<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="32" height="32" class="spacing"><a href="fplus_menu.php?session=<? echo($session); ?>"><img src="img/icon/b47.gif" width="32" height="32" border="0" alt="ファントルくん＋"></a></td>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="fplus_menu.php?session=<? echo($session); ?>"><b>ファントルくん＋</b></a> &gt; <a href="fplusadm_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
		<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="fplus_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
	</tr>
</table>


<table style="width:100%; margin-top:4px"><tr>
<td style="vertical-align:top; width:120px"><?= fplusadm_common_show_admin_left_menu($session); ?></td>
<td style="vertical-align:top">

	<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom:2px solid #c488ae; margin-bottom:2px">
		<tr><?show_wkfw_menuitem($session, $fname, "");?></tr>
	</table>

	<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:6px">
		<tr><td align="right"><input type="button" value="削除" onclick="deleteWorkflow();"></td></tr>
	</table>


	<form name="wkfw" action="fplusadm_lib_delete.php" method="post">
		<? show_workflow_lib_list($con,$session,$fname); ?>
		<input type="hidden" name="session" value="<? echo($session); ?>">
	</form>

</td></tr></table>


</td></tr></table>


</body>
<? pg_close($con); ?>
</html>
