<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_session.php");
require("about_authority.php");
require("menu_common.ini");

define("MAX_ROWS", 20);

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 55, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 私たちの生活サポート設定権限の取得
$life_setting_auth = check_authority($session, 54, $fname);

if ($page == "") {$page = "1";}

// 文書フォルダがなければ作成
if (!is_dir("intra")) {mkdir("intra", 0755);}
if (!is_dir("intra/coupon")) {mkdir("intra/coupon", 0755);}

// データベースに接続
$con = connect2db($fname);

// 文書一覧を取得
$limit = MAX_ROWS;
$offset = ($page - 1) * MAX_ROWS;
$sql = "select coupon.coupon_id, coupon.title, coupon.update_time, empmst.emp_lt_nm, empmst.emp_ft_nm from coupon inner join empmst on coupon.emp_id = empmst.emp_id";
$cond = "order by coupon.update_time desc limit $limit offset $offset";
$sel_coupon = select_from_table($con, $sql, $cond, $fname);
if ($sel_coupon == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 文書件数を取得
$sql = "select count(*) from coupon";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$coupon_count = intval(pg_fetch_result($sel, 0, 0));

// 文書登録権限があるか確認
$sql = "select count(*) from lifeemp";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$is_admin = (intval(pg_fetch_result($sel, 0, 0)) > 0);

// イントラメニュー情報を取得
$sql = "select * from intramenu";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$menu3 = pg_fetch_result($sel, 0, "menu3");
$menu3_1 = pg_fetch_result($sel, 0, "menu3_1");
?>
<title>イントラネット | <? echo($menu3); ?> | <? echo($menu3_1); ?></title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function registCoupon() {
	window.open('intra_life_coupon_register.php?session=<? echo($session); ?>', 'newwin', 'width=640,height=480,scrollbars=yes');
}

function deleteCoupon() {
	if (confirm('削除してよろしいですか？')) {
		document.mainform.submit();
	}
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="intra_menu.php?session=<? echo($session); ?>"><img src="img/icon/b38.gif" width="32" height="32" border="0" alt="イントラネット"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="intra_menu.php?session=<? echo($session); ?>"><b>イントラネット</b></a> &gt; <a href="intra_life.php?session=<? echo($session); ?>"><b><? echo($menu3); ?></b></a> &gt; <a href="intra_life_coupon.php?session=<? echo($session); ?>"><b><? echo($menu3_1); ?></b></a></font></td>
<? if ($life_setting_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="life_master_menu.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<form name="mainform" action="intra_life_coupon_delete.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="105" align="center" bgcolor="#bdd1e7"><a href="intra_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メインメニュー</font></a></td>
<td width="5">&nbsp;</td>
<td width="<? echo(get_tab_width($menu3)); ?>" align="center" bgcolor="#bdd1e7"><a href="intra_life.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($menu3); ?></font></a></td>
<td width="5">&nbsp;</td>
<td width="<? echo(get_tab_width($menu3_1)); ?>" align="center" bgcolor="#5279a5"><a href="intra_life_coupon.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b><? echo($menu3_1); ?></b></font></a></td>
<?
if ($is_admin) {
	echo("<td align=\"right\">\n");
	echo("<input type=\"button\" value=\"登録\" onclick=\"registCoupon();\">\n");
	echo("<input type=\"button\" value=\"削除\" onclick=\"deleteCoupon();\">\n");
	echo("</td>\n");
} else {
	echo("<td>&nbsp;</td>\n");
}
?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff">
<?
if ($is_admin) {
	echo("<td width=\"5%\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">削除</font></td>\n");
}
?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイトル</font></td>
<td width="26%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">更新日時</font></td>
<td width="20%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録者</font></td>
<td width="7%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">参照</font></td>
</tr>
<?
while ($row = pg_fetch_array($sel_coupon)) {
	$coupon_id = $row["coupon_id"];
	$title = $row["title"];
	$update_time = preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})$/", "$1/$2/$3 $4:$5:$6", $row["update_time"]);
	$emp_nm = $row["emp_lt_nm"] . " " . $row["emp_ft_nm"];

	$paths = glob("intra/coupon/$coupon_id.*");
	$path = $paths[0];

	echo("<tr>\n");
	if ($is_admin) {
		echo("<td align=\"center\"><input type=\"checkbox\" name=\"coupon_ids[]\" value=\"$coupon_id\"></td>\n");
	}
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$title</font></td>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$update_time</font></td>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$emp_nm</font></td>\n");
	echo("<td align=\"center\"><input type=\"button\" value=\"参照\" onclick=\"window.open('$path');\"></td>\n");
	echo("</tr>\n");
}
?>
</table>
<?
if ($coupon_count > MAX_ROWS) {
	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\n");
	echo("<tr>\n");
	echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">\n");
	for ($i = 1; $i < $page; $i++) {
		echo("<a href=\"intra_life_coupon.php?session=$session&page=$i\">$i</a>\n");
	}
	echo("$page\n");
	for ($i = $page + 1, $j = ceil($coupon_count / MAX_ROWS); $i <= $j; $i++) {
		echo("<a href=\"intra_life_coupon.php?session=$session&page=$i\">$i</a>\n");
	}
	echo("</font></td>\n");
	echo("</tr>\n");
	echo("</table>\n");
}
?>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
