<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require_once("about_comedix.php");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// ワークフロー権限のチェック
$check_auth = check_authority($session, 79, $fname);
if ($check_auth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションの開始
pg_query($con, "begin");

// 削除対象をループ
$del_lib = (!is_array($del_lib)) ? array() : $del_lib;
foreach ($del_lib as $del_lib_id) {

	// ライブラリデータを論理削除
	$sql = "update fpluswkfwlib set";
	$set = array("wkfwlib_del_flg");
	$setvalue = array("t");
	$cond = "where wkfwlib_id = '$del_lib_id'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}


// トランザクションのコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);


// 一覧画面を再表示
echo("<script type=\"text/javascript\">location.href = 'fplusadm_lib_list.php?session=$session'</script>");

?>
