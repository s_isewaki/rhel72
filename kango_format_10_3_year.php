<?
require_once("about_comedix.php");
require_once("kango_common.ini");
require_once("kango_format_10_3_util.ini");
require_once("show_select_values.ini");


//==============================
//前処理
//==============================
// ページ名
$fname = $_SERVER["PHP_SELF"];

// セッションのチェック
$session = qualify_session($_REQUEST["session"], $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限チェック
$checkauth = check_authority($session, $AUTH_NO_KANGO_USER, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// パラメータ
$is_postback = isset($_REQUEST["is_postback"]) ? $_REQUEST["is_postback"] : null;
$standard_cd = isset($_REQUEST["standard_cd"]) ? $_REQUEST["standard_cd"] : 1;
$fiscal_year = isset($_REQUEST["fiscal_year"]) ? $_REQUEST["fiscal_year"] : null;

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if ($con == "0") {
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}


//==============================
//ポストバック時の処理
//==============================
if ($is_postback == "true") {
    if ($postback_mode == "save") {
        //==============================
        //パラメータ解析
        //==============================
        $save_date = null;
        for ($i = 0; $i < 12; $i++) {
            $p = "bed_count_{$i}";
            $bed_count = $$p;
            $p = "pt_count_{$i}";
            $pt_count = $$p;
            $p = "pt_7_1_count_{$i}";
            $pt_7_1_count = $$p;
            $p = "yyyy_{$i}";
            $yyyy = $$p;
            $p = "mm_{$i}";
            $mm = $$p;

            $save_date1 = null;
            $save_date1['yyyy'] = $yyyy;
            $save_date1['mm'] = $mm;
            $save_date1['bed_count'] = $bed_count;
            $save_date1['pt_count'] = $pt_count;
            $save_date1['pt_7_1_count'] = $pt_7_1_count;
            $save_date[] = $save_date1;
        }

        //==============================
        //入力チェック
        //==============================
        $is_ok = true;
        $error_message = "";

        foreach ($save_date as $save_date1) {
            $bed_count = $save_date1['bed_count'];
            $pt_count = $save_date1['pt_count'];
            $pt_7_1_count = $save_date1['pt_7_1_count'];


            if (!preg_match("/^[0-9]*$/", $bed_count)) {
                $error_message = "病床数は数値を入力してください。";
                $is_ok = false;
                break;
            }
            if (!preg_match("/^[0-9]*$/", $pt_count)) {
                $error_message = "入院患者数は数値を入力してください。";
                $is_ok = false;
                break;
            }
            if (!preg_match("/^[0-9]*$/", $pt_7_1_count)) {
                $error_message = "７対１条件患者数は数値を入力してください。";
                $is_ok = false;
                break;
            }

            if (
                !(
                ($bed_count == "" && $pt_count == "" && $pt_7_1_count == "") || ($bed_count != "" && $pt_count != "" && $pt_7_1_count != "")
                )
            ) {
                $error_message = "月単位で未入力の項目があります。";
                $is_ok = false;
                break;
            }
        }


        if (!$is_ok) {
            //==============================
            //エラーメッセージ表示
            //==============================
            ?>
            <script type="text/javascript">
                alert("<?=$error_message?>");
                history.back();
            </script>
            <?
            exit;
        }
        else {
            //==============================
            //トランザクション開始
            //==============================
            pg_query($con, "begin transaction");

            //==============================
            //更新：デリート
            //==============================
            $sql = "delete from nlcs_format_10_3 where fiscal_year='{$fiscal_year}' and standard_code={$standard_cd}";
            $result = pg_exec_with_log($con, $sql, $fname);
            if ($result == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }

            //==============================
            //更新：インサート
            //==============================
            foreach ($save_date as $save_date1) {
                $yyyy = $save_date1['yyyy'];
                $mm = $save_date1['mm'];
                $bed_count = $save_date1['bed_count'];
                $pt_count = $save_date1['pt_count'];
                $pt_7_1_count = $save_date1['pt_7_1_count'];

                if ($bed_count == "") {
                    $bed_count = "null";
                }
                if ($pt_count == "") {
                    $pt_count = "null";
                }
                if ($pt_7_1_count == "") {
                    $pt_7_1_count = "null";
                }


                $sql = "insert into nlcs_format_10_3(fiscal_year,yyyy,mm,bed_count,pt_count,pt_7_1_count,standard_code) ";
                $sql .= "values('{$fiscal_year}','{$yyyy}','{$mm}',{$bed_count},{$pt_count},{$pt_7_1_count},{$standard_cd})";
                $result = pg_exec_with_log($con, $sql, $fname);
                if ($result == 0) {
                    pg_query($con, "rollback");
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }
            }

            //==============================
            //コミット
            //==============================
            pg_query($con, "commit");
        }
    }
}

//==============================
//施設基準
//==============================
$standard_name = get_standard_name($con, $fname, $standard_cd);


//==============================
//表示データ取得
//==============================
$date_list = get_format_10_3_year_disp_data($con, $fname, $fiscal_year, $standard_cd);


//==============================
//その他
//==============================
//画面名
$PAGE_TITLE = "様式10の3_年度別報告書";

//========================================================================================================================================================================================================
// HTML出力
//========================================================================================================================================================================================================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$KANGO_TITLE?> | <?=$PAGE_TITLE?></title>

<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/popup.js"></script>
<script type="text/javascript">

//画面起動時の処理
function initPage()
{
	for(var i=0;i<12;i++)
	{
		edited(i);
	}
}


function date_changed()
{
	document.mainform.postback_mode.value = "date_changed";
	document.mainform.submit();
}

function save()
{
	document.mainform.postback_mode.value = "save";
	document.mainform.submit();
}


function edited(i)
{
	var pt_count_obj = document.getElementById("pt_count_"+i);
	var pt_7_1_count_obj = document.getElementById("pt_7_1_count_"+i);
	var pt_7_1_rate_obj = document.getElementById("pt_7_1_rate_"+i);

	var pt_count = pt_count_obj.value;
	var pt_7_1_count = pt_7_1_count_obj.value;


    if(pt_count == "" || pt_count.match(/[^0-9]/g))
    {
		pt_7_1_rate_obj.innerHTML = "";
		return;
    }
    if(pt_7_1_count == "" || pt_7_1_count.match(/[^0-9]/g))
    {
		pt_7_1_rate_obj.innerHTML = "";
		return;
    }

	var pt_7_1_rate = 0;
	if(pt_count != 0)
	{
		pt_7_1_rate = pt_7_1_count / pt_count * 100;
		pt_7_1_rate = (Math.round(pt_7_1_rate * 10)) /10;
	}
	pt_7_1_rate_obj.innerHTML = pt_7_1_rate+"%";
}

function goto_month_page(target_year,target_month)
{
	var url = "kango_format_10_3.php?session=<?=$session?>&standard_cd=<?=$standard_cd?>&target_date_y=" + target_year + "&target_date_m=" + target_month;
	location.href = url;
}

//EXCEL出力実効
function excel_download()
{
	document.xls.submit();
}


</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}

.list_in {border-collapse:collapse;}
.list_in td {border:#FFFFFF solid 0px;}
</style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<!-- ヘッダー START -->
<?
show_kango_header($session,$fname);
?>
<!-- ヘッダー END -->
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td>

<!-- 全体 START -->
<form name="mainform" action="<?=$fname?>" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="is_postback" value="true">
<input type="hidden" name="postback_mode" value="">




<!-- レイアウト START -->
<table width="800" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>



<!-- 表示条件 START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>

<td width="350">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">

<!-- 日付選択 START -->
年度指定：<select id='fiscal_year' name='fiscal_year' onchange="date_changed()">
<?
$date_y_min = 2008;
$date_y_max = get_fiscal_year(date("Ymd"));
for ($tmp_y = $date_y_min; $tmp_y <= $date_y_max; $tmp_y++)
{
	?>
	<option value="<?=$tmp_y?>" <?if($tmp_y == $fiscal_year){echo('selected');}?>><?=$tmp_y?></option>
	<?
}
?>
</select>年度
<!-- 日付選択 END -->

<!-- 施設基準 START -->
施設基準：<select id="standard_cd" name="standard_cd" onchange="date_changed()">
<option value="1" <? if ($standard_cd == 1) { echo "selected";}?>>7対1</option>
<option value="2" <? if ($standard_cd == 2) { echo "selected";}?>>10対1</option>
<option value="7" <? if ($standard_cd == 7) { echo "selected";}?>>地域包括ケア</option>
</select>
<!-- 施設基準 END -->

</font>
</td>


<td>
&nbsp;
</td>


<td align="right">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="button" value="EXCEL出力" onclick="excel_download()">
</font>
</td>

</tr>
</table>
<!-- 表示条件 END -->




</td>
</tr>

<tr><td><img src="img/spacer.gif" width="1" height="5" alt=""></td></tr>

<tr>
<td>





<!-- 一覧 START -->
<table width="100%" border='0' cellspacing='0' cellpadding='1' class="list">

<tr>
<td bgcolor='#bdd1e7' rowspan="2" align="center" width="140"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>届出<br>入院料</font></td>
<td bgcolor='#bdd1e7' rowspan="2" align="center" width="100"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>届出<br>病床数</font></td>
<td bgcolor='#bdd1e7' colspan="3" align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>入院患者の状況</font></td>
<td bgcolor='#bdd1e7' rowspan="2" align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>算出期間</font></td>
</tr>

<tr>
<td bgcolor='#bdd1e7' align="center" width="130"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>入院患者数</font></td>
<td bgcolor='#bdd1e7' align="center" width="130"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$standard_name?>条件患者数</font></td>
<td bgcolor='#bdd1e7' align="center" width="130"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>比率</font></td>
</tr>

<?
foreach($date_list as $i => $date_list1)
{
	?>
	<input type="hidden" name="yyyy_<?=$i?>" value="<?=$date_list1['yyyy']?>">
	<input type="hidden" name="mm_<?=$i?>" value="<?=$date_list1['mm']?>">
	<tr>
	<td align="center">
		<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
		<?=$standard_name?>入院基本料
		</font>
		</td>
	<td align="center">
		<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
		<input type="text" id="bed_count_<?=$i?>" name="bed_count_<?=$i?>" value="<?=$date_list1['bed_count']?>" maxlength="6" style="width:50px">床
		</font>
		</td>
	<td align="center">
		<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
		<input type="text" id="pt_count_<?=$i?>" name="pt_count_<?=$i?>" value="<?=$date_list1['pt_count']?>" maxlength="6" style="width:50px" onchange="edited('<?=$i?>')">
		</font>
		</td>
	<td align="center">
		<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
		<input type="text" id="pt_7_1_count_<?=$i?>" name="pt_7_1_count_<?=$i?>" value="<?=$date_list1['pt_7_1_count']?>" maxlength="6" style="width:50px" onchange="edited('<?=$i?>')">
		</font>
		</td>
	<td align="center">
		<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
		<span id="pt_7_1_rate_<?=$i?>"></span>
		</font>
		</td>
	<td align="center">
		<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
		<?
		if($date_list1['is_month_link_able'])
		{
			?>
			<a href="javascript:goto_month_page('<?=$date_list1['yyyy']?>','<?=$date_list1['mm']?>')">
			<?=$date_list1['date']?>
			</a>
			<?
		}
		else
		{
			?>
			<?=$date_list1['date']?>
			<?
		}
		?>
		</font>
		</td>
	</tr>
	<?
}
?>


</table>
<!-- 一覧 END -->



</td>
</tr>

<tr><td><img src="img/spacer.gif" width="1" height="5" alt=""></td></tr>

<tr>
<td>




<!-- 下部ボタン START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td align="right">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="button" value="更新" onclick="save()">
</font>
</td>
</tr>
</table>
<!-- 下部ボタン END -->


</td>
</tr>
</table>
<!-- レイアウト END -->


















</form>
<!-- 全体 END -->

</td>
</tr>
</table>



<!-- EXCEL表示用フォーム START -->
<form name="xls" action="kango_format_10_3_year_excel.php" method="post" target="download">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="fiscal_year" value="<?=$fiscal_year?>">
<input type="hidden" name="standard_cd" value="<?=$standard_cd?>">
</form>
<iframe name="download" width="0" height="0" frameborder="0"></iframe>
<!-- EXCEL表示用フォーム END -->




</body>
</html>
<?
pg_close($con);
?>
