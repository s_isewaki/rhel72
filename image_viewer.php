<?
//--------------------------------------------------------------------------------------------------
// 職員プロファイル画像のポップアップ表示ダイアログ画面
// ★★★このファイルは恐らく非利用
// ・内線電話帳４画面からしか呼ばれていなかった様子、
// ・内線電話帳自体も、はext/ext_image_viewer.phpを参照するようになっている。
//--------------------------------------------------------------------------------------------------
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 画像参照</title>
<?
require("about_session.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

$imagesize = getimagesize($src);
$img_width = $imagesize[0];
$img_height = $imagesize[1];
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function initPage() {
	var img = document.getElementById('mainimg');
	var img_src = '<? echo($src); ?>';
	var img_width = <? echo($img_width); ?>;
	var img_height = <? echo($img_height); ?>;
	var body_width = document.body.clientWidth - 15;
	var body_height = document.body.clientHeight - 50;

	for (var i = 100; i > 0; i--) {
		var tmp_width = Math.round(img_width * i / 100);
		var tmp_height = Math.round(img_height * i / 100);
		if (tmp_width <= body_width && tmp_height <= body_height) {
			img.width = tmp_width;
			img.height = tmp_height;
			img.src = img_src;
			break;
		}
	}
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="initPage();">
<table width="100%" cellpadding="0" cellspacing="0" border="0">
<tr>
<td align="right"><input type="button" value="閉じる" onclick="window.close();"></td>
</tr>
<tr>
<td align="center"><img id="mainimg" src="img/spacer.gif"></td>
</tr>
</table>
</body>
</html>
