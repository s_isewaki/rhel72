<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if ($ccl_psn == "") {
	echo("<script type=\"text/javascript\">alert('連絡者を選択してください。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションの開始
pg_query($con, "begin");

// 該当する入院予定情報を取得
$sql = "select *, to_number(inpt_bed_no, '00') as bed_no from inptres";
$cond = "where ptif_id = '$pt_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$bldg_cd = pg_fetch_result($sel, 0, "bldg_cd");
$ward_cd = pg_fetch_result($sel, 0, "ward_cd");
$ptrm_room_no = pg_fetch_result($sel, 0, "ptrm_room_no");
$inpt_lt_kn_nm = pg_fetch_result($sel, 0, "inpt_lt_kn_nm");
$inpt_ft_kn_nm = pg_fetch_result($sel, 0, "inpt_ft_kn_nm");
$inpt_lt_kj_nm = pg_fetch_result($sel, 0, "inpt_lt_kj_nm");
$inpt_ft_kj_nm = pg_fetch_result($sel, 0, "inpt_ft_kj_nm");
$inpt_keywd = pg_fetch_result($sel, 0, "inpt_keywd");
$inpt_arrival = pg_fetch_result($sel, 0, "inpt_arrival");
$inpt_enti_id = pg_fetch_result($sel, 0, "inpt_enti_id");
$inpt_sect_id = pg_fetch_result($sel, 0, "inpt_sect_id");
$dr_id = pg_fetch_result($sel, 0, "dr_id");
$nurse_id = pg_fetch_result($sel, 0, "nurse_id");
$inpt_sample_blood = pg_fetch_result($sel, 0, "inpt_sample_blood");
$inpt_purpose_rireki = pg_fetch_result($sel, 0, "inpt_purpose_rireki");
if ($inpt_purpose_rireki == "") {$inpt_purpose_rireki = null;}
$inpt_purpose_cd = pg_fetch_result($sel, 0, "inpt_purpose_cd");
$inpt_purpose_content = pg_fetch_result($sel, 0, "inpt_purpose_content");
$inpt_short_stay = pg_fetch_result($sel, 0, "inpt_short_stay");
if ($inpt_short_stay == "") {$inpt_short_stay = null;}
$inpt_outpt_test = pg_fetch_result($sel, 0, "inpt_outpt_test");
if ($inpt_outpt_test == "") {$inpt_outpt_test = null;}
$inpt_diagnosis = pg_fetch_result($sel, 0, "inpt_diagnosis");
$inpt_nrs_obsv = pg_fetch_result($sel, 0, "inpt_nrs_obsv");
$inpt_free = pg_fetch_result($sel, 0, "inpt_free");
$inpt_special = pg_fetch_result($sel, 0, "inpt_special");
$inpt_bed_no = pg_fetch_result($sel, 0, "inpt_bed_no");
$inpt_disease = pg_fetch_result($sel, 0, "inpt_disease");
$inpt_patho_from = pg_fetch_result($sel, 0, "inpt_patho_from");
$inpt_patho_to = pg_fetch_result($sel, 0, "inpt_patho_to");
$inpt_in_res_dt_flg = pg_fetch_result($sel, 0, "inpt_in_res_dt_flg");
$inpt_in_res_dt = pg_fetch_result($sel, 0, "inpt_in_res_dt");
$inpt_in_res_tm = pg_fetch_result($sel, 0, "inpt_in_res_tm");
$inpt_in_res_ym = pg_fetch_result($sel, 0, "inpt_in_res_ym");
$inpt_in_res_td = pg_fetch_result($sel, 0, "inpt_in_res_td");
$inpt_ope_dt_flg = pg_fetch_result($sel, 0, "inpt_ope_dt_flg");
$inpt_ope_dt = pg_fetch_result($sel, 0, "inpt_ope_dt");
$inpt_ope_ym = pg_fetch_result($sel, 0, "inpt_ope_ym");
$inpt_ope_td = pg_fetch_result($sel, 0, "inpt_ope_td");
$inpt_emergency = pg_fetch_result($sel, 0, "inpt_emergency");
$inpt_in_plan_period = pg_fetch_result($sel, 0, "inpt_in_plan_period");
$inpt_in_way = pg_fetch_result($sel, 0, "inpt_in_way");
$inpt_nursing = pg_fetch_result($sel, 0, "inpt_nursing");
$inpt_staple_fd = pg_fetch_result($sel, 0, "inpt_staple_fd");
$inpt_fd_type = pg_fetch_result($sel, 0, "inpt_fd_type");
$inpt_fd_dtl = pg_fetch_result($sel, 0, "inpt_fd_dtl");
$inpt_wish_rm_rireki = pg_fetch_result($sel, 0, "inpt_wish_rm_rireki");
if ($inpt_wish_rm_rireki == "") {$inpt_wish_rm_rireki = null;}
$inpt_wish_rm_cd = pg_fetch_result($sel, 0, "inpt_wish_rm_cd");
$inpt_fd_start = pg_fetch_result($sel, 0, "inpt_fd_start");
$inpt_res_chg_psn = pg_fetch_result($sel, 0, "inpt_res_chg_psn");
$inpt_res_chg_psn_dtl = pg_fetch_result($sel, 0, "inpt_res_chg_psn_dtl");
$inpt_res_chg_rsn = pg_fetch_result($sel, 0, "inpt_res_chg_rsn");
$inpt_res_chg_ctt = pg_fetch_result($sel, 0, "inpt_res_chg_ctt");
$inpt_insurance = pg_fetch_result($sel, 0, "inpt_insurance");
$inpt_insu_rate_rireki = pg_fetch_result($sel, 0, "inpt_insu_rate_rireki");
if ($inpt_insu_rate_rireki == "") {$inpt_insu_rate_rireki = null;}
$inpt_insu_rate_cd = pg_fetch_result($sel, 0, "inpt_insu_rate_cd");
$inpt_reduced = pg_fetch_result($sel, 0, "inpt_reduced");
if ($inpt_reduced == "") {$inpt_reduced = null;}
$inpt_impaired = pg_fetch_result($sel, 0, "inpt_impaired");
if ($inpt_impaired == "") {$inpt_impaired = null;}
$inpt_exemption = pg_fetch_result($sel, 0, "inpt_exemption");
if ($inpt_exemption == "") {$inpt_exemption = null;}
$inpt_insured = pg_fetch_result($sel, 0, "inpt_insured");
$inpt_observe = pg_fetch_result($sel, 0, "inpt_observe");
$inpt_meet_flg = pg_fetch_result($sel, 0, "inpt_meet_flg");
$inpt_dcb_cls = pg_fetch_result($sel, 0, "inpt_dcb_cls");
$inpt_dcb_bas = pg_fetch_result($sel, 0, "inpt_dcb_bas");
$inpt_dcb_exp = pg_fetch_result($sel, 0, "inpt_dcb_exp");
$inpt_rhb_cls = pg_fetch_result($sel, 0, "inpt_rhb_cls");
$inpt_rhb_bas = pg_fetch_result($sel, 0, "inpt_rhb_bas");
$inpt_rhb_exp = pg_fetch_result($sel, 0, "inpt_rhb_exp");
$inpt_privacy_flg = pg_fetch_result($sel, 0, "inpt_privacy_flg");
$inpt_privacy_text = pg_fetch_result($sel, 0, "inpt_privacy_text");
$inpt_pre_div = pg_fetch_result($sel, 0, "inpt_pre_div");
$inpt_care_no = pg_fetch_result($sel, 0, "inpt_care_no");
$inpt_care_grd_rireki = pg_fetch_result($sel, 0, "inpt_care_grd_rireki");
if ($inpt_care_grd_rireki == "") {$inpt_care_grd_rireki = null;}
$inpt_care_grd_cd = pg_fetch_result($sel, 0, "inpt_care_grd_cd");
$inpt_care_apv = pg_fetch_result($sel, 0, "inpt_care_apv");
$inpt_care_from = pg_fetch_result($sel, 0, "inpt_care_from");
$inpt_care_to = pg_fetch_result($sel, 0, "inpt_care_to");
$inpt_dis_grd_rireki = pg_fetch_result($sel, 0, "inpt_dis_grd_rireki");
if ($inpt_dis_grd_rireki == "") {$inpt_dis_grd_rireki = null;}
$inpt_dis_grd_cd = pg_fetch_result($sel, 0, "inpt_dis_grd_cd");
$inpt_dis_type_rireki = pg_fetch_result($sel, 0, "inpt_dis_type_rireki");
if ($inpt_dis_type_rireki == "") {$inpt_dis_type_rireki = null;}
$inpt_dis_type_cd = pg_fetch_result($sel, 0, "inpt_dis_type_cd");
$inpt_spec_name = pg_fetch_result($sel, 0, "inpt_spec_name");
$inpt_spec_from = pg_fetch_result($sel, 0, "inpt_spec_from");
$inpt_spec_to = pg_fetch_result($sel, 0, "inpt_spec_to");
$inpt_intro_inst_cd = pg_fetch_result($sel, 0, "inpt_intro_inst_cd");
$inpt_intro_sect_rireki = pg_fetch_result($sel, 0, "inpt_intro_sect_rireki");
if ($inpt_intro_sect_rireki == "") {$inpt_intro_sect_rireki = null;}
$inpt_intro_sect_cd = pg_fetch_result($sel, 0, "inpt_intro_sect_cd");
$inpt_intro_doctor_no = pg_fetch_result($sel, 0, "inpt_intro_doctor_no");
if ($inpt_intro_doctor_no == "") {$inpt_intro_doctor_no = null;}
$inpt_hotline = pg_fetch_result($sel, 0, "inpt_hotline");
if ($inpt_hotline == "") {$inpt_hotline = null;}

// 入院予定キャンセルIDの採番
$sql = "select max(inpt_res_ccl_id) from inptcancel";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$inpt_res_ccl_id = intval(pg_fetch_result($sel, 0, 0)) + 1;

// 入院予定キャンセル履歴の作成
if ($ccl_rsn_rireki == "" || $ccl_rsn_cd == "") {$ccl_rsn_rireki = null;}
$sql = "insert into inptcancel (inpt_res_ccl_id, ptif_id, bldg_cd, ward_cd, ptrm_room_no, inpt_lt_kn_nm, inpt_ft_kn_nm, inpt_lt_kj_nm, inpt_ft_kj_nm, inpt_keywd, inpt_enti_id, inpt_sect_id, dr_id, nurse_id, inpt_sample_blood, inpt_purpose_rireki, inpt_purpose_cd, inpt_purpose_content, inpt_diagnosis, inpt_special, inpt_bed_no, inpt_disease, inpt_in_res_dt_flg, inpt_in_res_dt, inpt_in_res_tm, inpt_in_res_ym, inpt_in_res_td, inpt_ope_dt_flg, inpt_ope_dt, inpt_ope_ym, inpt_ope_td, inpt_emergency, inpt_in_plan_period, inpt_in_way, inpt_nursing, inpt_staple_fd, inpt_fd_type, inpt_fd_dtl, inpt_wish_rm_rireki, inpt_wish_rm_cd, inpt_fd_start, inpt_res_chg_psn, inpt_res_chg_psn_dtl, inpt_res_chg_rsn, inpt_res_chg_ctt, inpt_res_ccl_psn, inpt_res_ccl_psn_dtl, inpt_res_ccl_rsn, inpt_insurance, inpt_observe, inpt_meet_flg, inpt_privacy_flg, inpt_privacy_text, inpt_short_stay, inpt_outpt_test, inpt_insu_rate_rireki, inpt_insu_rate_cd, inpt_reduced, inpt_impaired, inpt_insured, inpt_arrival, inpt_patho_from, inpt_patho_to, inpt_dcb_cls, inpt_dcb_bas, inpt_dcb_exp, inpt_rhb_cls, inpt_rhb_bas, inpt_rhb_exp, inpt_care_no, inpt_care_grd_rireki, inpt_care_grd_cd, inpt_care_apv, inpt_care_from, inpt_care_to, inpt_dis_grd_rireki, inpt_dis_grd_cd, inpt_dis_type_rireki, inpt_dis_type_cd, inpt_spec_name, inpt_spec_from, inpt_spec_to, inpt_pre_div, inpt_intro_inst_cd, inpt_intro_sect_rireki, inpt_intro_sect_cd, inpt_intro_doctor_no, inpt_hotline, inpt_nrs_obsv, inpt_free, inpt_res_ccl_rsn_rireki, inpt_res_ccl_rsn_cd, inpt_exemption) values (";
$content = array($inpt_res_ccl_id, $pt_id, $bldg_cd, $ward_cd, $ptrm_room_no, $inpt_lt_kn_nm, $inpt_ft_kn_nm, $inpt_lt_kj_nm, $inpt_ft_kj_nm, $inpt_keywd, $inpt_enti_id, $inpt_sect_id, $dr_id, $nurse_id, $inpt_sample_blood, $inpt_purpose_rireki, $inpt_purpose_cd, $inpt_purpose_content, $inpt_diagnosis, $inpt_special, $inpt_bed_no, $inpt_disease, $inpt_in_res_dt_flg, $inpt_in_res_dt, $inpt_in_res_tm, $inpt_in_res_ym, $inpt_in_res_td, $inpt_ope_dt_flg, $inpt_ope_dt, $inpt_ope_ym, $inpt_ope_td, $inpt_emergency, $inpt_in_plan_period, $inpt_in_way, $inpt_nursing, $inpt_staple_fd, $inpt_fd_type, $inpt_fd_dtl, $inpt_wish_rm_rireki, $inpt_wish_rm_cd, $inpt_fd_start, $inpt_res_chg_psn, $inpt_res_chg_psn_dtl, $inpt_res_chg_rsn, $inpt_res_chg_ctt, $ccl_psn, $ccl_psn_dtl, $ccl_rsn, $inpt_insurance, $inpt_observe, $inpt_meet_flg, $inpt_privacy_flg, $inpt_privacy_text, $inpt_short_stay, $inpt_outpt_test, $inpt_insu_rate_rireki, $inpt_insu_rate_cd, $inpt_reduced, $inpt_impaired, $inpt_insured, $inpt_arrival, $inpt_patho_from, $inpt_patho_to, $inpt_dcb_cls, $inpt_dcb_bas, $inpt_dcb_exp, $inpt_rhb_cls, $inpt_rhb_bas, $inpt_rhb_exp, $inpt_care_no, $inpt_care_grd_rireki, $inpt_care_grd_cd, $inpt_care_apv, $inpt_care_from, $inpt_care_to, $inpt_dis_grd_rireki, $inpt_dis_grd_cd, $inpt_dis_type_rireki, $inpt_dis_type_cd, $inpt_spec_name, $inpt_spec_from, $inpt_spec_to, $inpt_pre_div, $inpt_intro_inst_cd, $inpt_intro_sect_rireki, $inpt_intro_sect_cd, $inpt_intro_doctor_no, $inpt_hotline, $inpt_nrs_obsv, $inpt_free, $ccl_rsn_rireki, $ccl_rsn_cd, $inpt_exemption);
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 担当者情報をキャンセル履歴に移行
$sql = "select emp_id, order_no from inptopres";
$cond = "where ptif_id = '$pt_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
while ($row = pg_fetch_array($sel)) {
	$sql = "insert into inptopcancel (inpt_res_ccl_id, ptif_id, emp_id, order_no) values (";
	$content = array($inpt_res_ccl_id, $pt_id, $row["emp_id"], $row["order_no"]);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}
$sql = "delete from inptopres";
$cond = "where ptif_id = '$pt_id'";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 入院予定情報を削除
$sql = "delete from inptres";
$cond = "where ptif_id = '$pt_id'";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// トランザクションのコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 親画面をリフレッシュし、自画面を閉じる
echo "<script type=\"text/javascript\">opener.location.reload(); self.close();</script>";
?>
