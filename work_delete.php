<?
require_once("Cmx.php");
require_once("aclg_set.php");
require("about_authority.php");
require("about_session.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// タスク権限のチェック
$checkauth = check_authority($session, 30, $fname);
if ($ward == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if (!is_array($trashbox)) {
	echo("<script type=\"text/javascript\">alert('削除対象を選択してください。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// トランザクションの開始
pg_query($con, "begin");

foreach ($trashbox as $box_id) {
	list($tmp_pjt_id, $tmp_work_id) = split("-", $box_id);

	if ($tmp_pjt_id == "") {
		$sql = "delete from work";
		$cond = "where work_id = $tmp_work_id";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	} else {
		$sql = "delete from proworkmem";
		$cond = "where work_id = $tmp_work_id";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$sql = "delete from prowork";
		$cond = "where work_id = $tmp_work_id";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
}

// トランザクションのコミット
pg_query($con, "commit");

// タスク一覧画面を再表示
echo("<script type=\"text/javascript\">location.href = 'work_menu.php?session=$session';</script>");
?>
