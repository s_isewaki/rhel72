<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_authority.php");
require("about_session.php");
require("about_postgres.php");
require("referer_common.ini");
require("label_by_profile_type.ini");
require_once("sot_util.php");
require_once("get_menu_label.ini");

$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//診療科権限チェック
$dept = check_authority($session,28,$fname);
if($dept == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 病棟登録権限を取得
$ward_admin_auth = check_authority($session, 21, $fname);

// 患者管理管理者権限を取得
$patient_admin_auth = check_authority($session, 77, $fname);

// メドレポート管理権限を取得
$summary_admin_auth = check_authority($session, 59, $fname);

// 事業所IDの設定
$enti_id = 1;

//DBコネクションの作成
$con = connect2db($fname);



//==============================================================================
// もし登録・更新指定なら、更新する。（リダイレクトはしない）
//==============================================================================
if (@$_REQUEST["action"] == "regist"){

	$entries = array();
	foreach($_REQUEST as $key => $v){
		if (substr($key,0,8) != "chkroom_") continue;
		$id_list = explode("_", $key);
		if (count($id_list) != 5) continue;
		$entries[] = array(
			"bldg_cd" => (int)$id_list[1],
			"ward_cd" => (int)$id_list[2],
			"team_id" => (int)$id_list[3],
			"ptrm_room_no" => (int)$id_list[4]
		);
	}

	pg_query($con, "begin transaction");
	delete_from_table($con, "delete from sot_ward_team_relation", "", $fname);
	foreach ($entries as $idx => $row){
		$sql =
			" insert into sot_ward_team_relation (".
			" bldg_cd, ward_cd, ptrm_room_no, team_id".
			" ) values (".
			" " . $row["bldg_cd"].
			"," . $row["ward_cd"].
			"," . $row["ptrm_room_no"].
			"," . $row["team_id"].
			" )";
		update_set_table($con, $sql, array(), null, "", $fname);
	}
	pg_query($con, "commit");
}






// 遷移元の取得
$referer = get_referer($con, $session, "entity", $fname);
// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 診療科/担当部門
$section_title = $_label_by_profile["SECTION"][$profile_type];

// 病床管理/居室管理
$bed_manage_title = $_label_by_profile["BED_MANAGE"][$profile_type];

// 患者管理/利用者管理
$patient_manage_title = $_label_by_profile["PATIENT_MANAGE"][$profile_type];

// 病棟/施設
$ward_title = $_label_by_profile["WARD"][$profile_type];

// 診療区分/記録区分
$summary_kubun_title = $_label_by_profile["SUMMARY_KUBUN"][$profile_type];

// 担当医/担当者１
$doctor_title = $_label_by_profile["DOCTOR"][$profile_type];

// 担当看護師/担当者２
$nurse_title = $_label_by_profile["NURSE"][$profile_type];

// 担当看護師名/担当者２
$nurse_name_title = $_label_by_profile["NURSE_NAME"][$profile_type];

// メドレポート/ＰＯレポート(Problem Oriented)
$med_report_title = get_report_menu_label($con, $fname);
?>
<title>CoMedix マスターメンテナンス | <?=$nurse_title?>登録</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function clearEmployeeID() {
	document.nu.nu_emp_personal_id.value = '';
	document.nu.nu_emp_id.value = '';
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
label { cursor:pointer }
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<? if ($referer == "1") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="bed_menu.php?session=<? echo($session); ?>"><img src="img/icon/b17.gif" width="32" height="32" border="0" alt="<? echo ($bed_manage_title); ?>"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="bed_menu.php?session=<? echo($session); ?>"><b><? echo ($bed_manage_title); ?></b></a> &gt; <a href="entity_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a> &gt; <a href="entity_menu.php?session=<? echo($session); ?>"><b><? echo ($section_title); ?></b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="bed_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } else if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="patient_info_menu.php?session=<? echo($session); ?>"><img src="img/icon/b16.gif" width="32" height="32" border="0" alt="<? echo($patient_manage_title); ?>"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="patient_info_menu.php?session=<? echo($session); ?>"><b><? echo($patient_manage_title); ?></b></a> &gt; <a href="entity_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a> &gt; <a href="entity_menu.php?session=<? echo($session); ?>"><b><? echo ($section_title); ?></b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="patient_info_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } else if ($referer == "3") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="summary_menu.php?session=<? echo($session); ?>"><img src="img/icon/b57.gif" width="32" height="32" border="0" alt="<? echo($med_report_title); ?>"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="summary_menu.php?session=<? echo($session); ?>"><b><? echo($med_report_title); ?></b></a> &gt; <a href="entity_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a> &gt; <a href="entity_menu.php?session=<? echo($session); ?>"><b><? echo ($section_title); ?></b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="summary_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- left -->
<td width="120" valign="top">
<table width="120" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width="118" valign="top">
<table width="118" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#5279A5">
<td height="22" class="spacing" colspan="2"><b><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff">管理項目</font></b></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="entity_menu.php?session=<? echo($session); ?>"><? echo ($section_title); ?></a></font></td>
</tr>
<? if ($ward_admin_auth == "1" && $referer == "1") { ?>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="building_list.php?session=<? echo($session); ?>"><?=$ward_title?></a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_display_setting.php?session=<? echo($session); ?>">表示設定</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_check_setting.php?session=<? echo($session); ?>">管理項目設定</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_bulk_inpatient_register.php?session=<? echo($session); ?>">一括登録</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_master_menu.php?session=<? echo($session); ?>">マスタ管理</a></font></td>
</tr>
<? } ?>
<? if ($patient_admin_auth == "1" && $referer == "2") { ?>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_check_setting.php?session=<? echo($session); ?>">管理項目設定</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="patient_admin_menu.php?session=<? echo($session); ?>">オプション</a></font></td>
</tr>
<? } ?>
<? if ($summary_admin_auth == "1" && $referer == "3") { ?>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="summary_kanri_kubun.php?session=<? echo($session); ?>"><?=$summary_kubun_title?></a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="sum_ordsc_med_list.php?session=<? echo($session); ?>">処方・注射オーダ</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="summary_kanri_tmpl.php?session=<? echo($session); ?>">テンプレート</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="summary_mst.php?session=<? echo($session); ?>">マスタ管理</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="summary_access_log_daily.php?session=<? echo($session); ?>">アクセスログ</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="summary_kanri_option.php?session=<? echo($session); ?>">オプション</a></font></td>
</tr>

<? } ?>
</table>
</td>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
</td>
<!-- left -->
<!-- center -->
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->
<!-- right -->
<td valign="top">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="90" align="center" bgcolor="#bdd1e7"><a href="entity_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo ($section_title); ?>一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="doctor_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$doctor_title?>一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="nurse_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$nurse_title?>一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="section_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo ($section_title); ?>登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="doctor_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$doctor_title?>登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#5279a5"><a href="nurse_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b><?=$nurse_title?>登録</b></font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>


<img src="img/spacer.gif" alt="" width="1" height="3"><br>

<table border="0" cellspacing="0" cellpadding="6">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="nurse_register.php?session=<?=$session?>">診療科担当</a></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="nurse_ward_register.php?session=<?=$session?>">病棟担当</a></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="nurse_team_register.php?session=<?=$session?>">病棟担当(チーム)</a></font></td>
</tr>
</table>



<script type="text/javascript">
var selections = {};
var combobox = {};
function sel_changed(cmb_id, node, value){
	for(var i=1; i<=4; i++) combobox[i] = document.getElementById(cmb_id+"_"+i);<?// コンボ取得?>
	for(var i=4; i>=1; i--) if(node<i) combobox[i].options.length = 1;<? //下位層コンボをクリア ?>

	<? // 初期化用 自分コンボを再選択?>
	var cmb = combobox[node];
	if (node > 0 && cmb.value != value) {
		for(var i=0; i<cmb.options.length; i++) if (cmb.options[i].value==value) cmb.selectedIndex = i;
	}

	<? // 次階層をセット。最後の階層なら何もしない?>
	if (node >= 4) return;
	var idx = 1;
	var list = selections["node"+(node*1+1*1)];
	var cmb_next = combobox[node*1+1*1];
	for(var i in list){
		if (list[i].parent != value) continue;
		cmb_next.options.length = cmb_next.options.length + 1;
		cmb_next.options[idx].text  = list[i].name;
		cmb_next.options[idx].value = list[i].code;
		idx++;
	}
}
</script>
<?

$font = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';


// チームマスタ取得
$sel = $c_sot_util->select_from_table("select * from teammst order by team_id");
$teammst = array();
while ($row = pg_fetch_array($sel)) {
	$teammst[$row["team_id"]] = $row["team_nm"];
}


// 事業所と病棟、病室マスタ取得
$sql =
	" select".
	" b.bldg_cd, b.bldg_name, w.ward_cd, w.ward_name, p.ptrm_room_no, p.ptrm_name".
	" from bldgmst b".
	" inner join wdmst w on (w.bldg_cd = b.bldg_cd)".
	" inner join ptrmmst p on (p.bldg_cd = b.bldg_cd and p.ward_cd = w.ward_cd)".
	" where b.bldg_del_flg = 'f'".
	" and w.ward_del_flg = 'f'".
	" and p.ptrm_del_flg = 'f'".
	" order by b.bldg_cd, w.ward_cd, p.ptrm_room_no";
$places = array();
$sel = $c_sot_util->select_from_table($sql);
while ($row = pg_fetch_array($sel)){
	$places[$row["bldg_cd"]][$row["ward_cd"]][] = $row;
}


// リレーション済みデータの取得
$relations = array();
$sel = $c_sot_util->select_from_table("select * from sot_ward_team_relation");
while ($row = pg_fetch_array($sel)){
	$relations[$row["bldg_cd"]."_".$row["ward_cd"]."_".$row["team_id"]."_".$row["ptrm_room_no"]] = "exist!";
}


?>
<form name="frm" action="nurse_team_register.php" method="post">
	<input type="hidden" name="action" value="regist"/>
	<input type="hidden" name="session" value="<?=$session ?>"/>
	<table width="98%" border="0" cellspacing="0" cellpadding="3" class="list">
		<tr style="background-color:#f6f9ff">
			<td width="10%"><?=$font?>事業所</font></td>
			<td width="10%"><?=$font?>病棟</font></td>
			<td width="10%"><?=$font?>担当チーム</font></td>
			<td width="70%"><?=$font?>病室</font></td>
		</tr>

		<? foreach($places as $bldg_cd => $bldg_data){ ?><? // ループ１（事業所）開始 ?>
		<? $rowstart1 = 1; ?>
		<? foreach($bldg_data as $ward_cd => $rows){ ?><? // ループ２（病棟）開始 ?>
		<? $rowstart2 = 1; ?>
		<? foreach($teammst as $team_id => $team_nm){ ?><? // ループ３（チーム）開始 ?>
		<? $rowspan2 = count($teammst); ?>
		<? $rowspan1 = $rowspan2 * count($bldg_data); ?>

		<tr>
			<? // １列目：事業所マスタ -> 事業所名 ?>
			<? if ($rowstart1){ ?>
				<? $rowstart1 = 0; ?>
			<td rowspan="<?=$rowspan1?>" style="background-color:#f6f9ff"><?=$font?><?=$rows[0]["bldg_name"]?></font></td>
			<? } ?>

			<? // ２列目：病棟マスタ -> 病棟名 ?>
			<? if ($rowstart2){ ?>
				<? $rowstart2 = 0; ?>
			<td rowspan="<?=$rowspan2?>" style="background-color:#f6f9ff"><?=$font?><?=$rows[0]["ward_name"]?></font></td>
			<? } ?>

			<? // ３列目：チーム名 ?>
			<td style="background-color:#f6f9ff"><?=$font?><?=$team_nm?></font></td>

			<? // ４列目：ルーム番号のチェックボックス群 ?>
			<td><?=$font?>
				<? foreach($rows as $idx => $row){ ?>
					<? $key = $row["bldg_cd"]."_".$row["ward_cd"]."_".$team_id."_".$row["ptrm_room_no"]; ?>
					<label><input type="checkbox" name="chkroom_<?=$key?>" <?= @$relations[$key]?"checked":""?> /><?=$row["ptrm_name"]?></label>
				<? } ?>
			</font></td>

		</tr>

		<? } ?><? // ループ３（チーム）終了 ?>
		<? } ?><? // ループ２（病棟）終了 ?>
		<? } ?><? // ループ１（事業所）終了 ?>
	</table>

	<div style="width:98%; text-align:right; padding:2px"><input type="submit" value="登録"></div>
</form>




</td>
<!-- right -->
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
