<?

ob_start();

require_once("Cmx.php");
require_once("about_comedix.php");
require_once("library_common.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$check_auth = check_authority($session, 78, $fname);
if ($check_auth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

$ext = strrchr($filename, ".");
//file_flg 1:workflow 2:workflow/tmp 3:apply 4:apply/tmp
switch ($file_flg) {
    case 1:
        $file_path = "fplus/workflow/{$wkfw_id}_{$file_id}{$ext}";
        break;
    case 2:
        $file_path = "fplus/workflow/tmp/{$session}_{$file_id}{$ext}";
        break;
    case 3:
        $file_path = "fplus/apply/{$apply_id}_{$file_id}{$ext}";
        break;
    case 4:
        $file_path = "fplus/apply/tmp/{$session}_{$file_id}{$ext}";
        break;
}

// ダウンロード文書名の設定を取得（文書管理で設定されている内容を使用）
$filename_flg = lib_get_filename_flg();

// 登録文書名の場合
if ($filename_flg > 0) {
    if ($filename_flg == 1) { // MSIE
        $filename = to_sjis($filename);
    }
    else if ($filename_flg == 2) { // ff
        $filename = to_utf8($filename);
    }
    else {
        $filename = rawurlencode(to_utf8($filename));
    }

    ob_clean();
    header("Content-Disposition: attachment; filename=$filename");
    header("Content-Type: application/octet-stream; name=$filename");
    header("Content-Length: " . filesize($file_path));
    readfile($file_path);
    ob_end_flush();

// システム名の場合
}
else {
    $scheme = ($_SERVER["HTTPS"] == "") ? "http" : "https";
    $appdir = str_replace($_SERVER["DOCUMENT_ROOT"], "", $_SERVER["SCRIPT_FILENAME"]);
    $appdir = str_replace("fplusadm_workflow_attach_refer.php", "", $appdir);

    $file_url = "$scheme://" . $_SERVER['HTTP_HOST'] . $appdir . $file_path;
    ob_end_clean();
    header("Location: $file_url");
}