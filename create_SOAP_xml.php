<? // XML作成用コード。この行は変更しないでください。

$doc = domxml_new_doc("1.0");
$root = $doc->create_element("template");
$root = $doc->append_child($root);
$head = $doc->create_element("SOAP");
$head = $root->append_child($head);

//key:オブジェクト名 val:タイトル
$names['Textarea_S']='Subjective';
$names['Textarea_O']='Objective';
$names['Textarea_A']='Assessment';
$names['Textarea_P']='Plan';
$names['Textarea_N']='その他';

while(list ($key, $val) = each($names)) {
	$node = $doc->create_element($val);
	$node = $head->append_child($node);
	$node->set_attribute("main", $key);
	$node->append_child($doc->create_text_node(mb_convert_encoding($$key, "UTF-8", "EUC-JP")));
}

//XML文章
$text = $doc->dump_mem(true, "UTF-8");

?>