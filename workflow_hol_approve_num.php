<?php
require_once("about_comedix.php");
require_once("referer_common.ini");
require_once("workflow_common.ini");
require_once('Cmx.php');
require_once('Cmx/Model/SystemConfig.php');

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 3, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 遷移元の設定
if ($workflow != "") {
	set_referer($con, $session, "workflow", $workflow, $fname);
	$referer = $workflow;
} else {
	$referer = get_referer($con, $session, "workflow", $fname);
}

$conf = new Cmx_SystemConfig();
$hol_cfm = $conf->get('apply.hol.cfm');
//初期値、最終承認者の階層
if ($hol_cfm == "") {
	$hol_cfm = "2";
}
//年休残の計算対象 1:承認済みのみ 2:承認済みと申請中
$remain_count_flg = $conf->get('apply.hol.remain_count_flg');
//初期値
if ($remain_count_flg == "") {
	$remain_count_flg = "1";
}
//対象日数が年休残を超える申請時の動作  1:警告を出すが申請はできる 2:申請できない
$over_apply_flg = $conf->get('apply.hol.over_apply_flg');
//初期値
if ($over_apply_flg == "") {
	$over_apply_flg = "1";
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ワークフロー | 休暇申請</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/workflow/workflow.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($referer == "2") { ?>
<table id="header">
<tr>
<td class="icon"><a href="workflow_menu.php?session=<? echo($session); ?>"><img src="img/icon/b24.gif" width="32" height="32" border="0" alt="ワークフロー"></a></td>
<td><a href="workflow_menu.php?session=<?=$session?>"><b>ワークフロー</b></a></td>
</tr>
</table>
<? } else { ?>
<table id="header">
<tr>
<td class="icon"><a href="application_menu.php?session=<? echo($session); ?>"><img src="img/icon/b08.gif" width="32" height="32" border="0" alt="決裁・申請"></a></td>
<td>
	<a href="application_menu.php?session=<? echo($session); ?>"><b>決裁・申請</b></a> &gt;
	<a href="workflow_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a> &gt;
	<a href="workflow_notice_setting.php?session=<? echo($session); ?>"><b>オプション</b></a>
</td>
<td class="kanri"><a href="application_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></td>
</tr>
</table>
<? } ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td valign="top" width="120"><?show_wkfw_sidemenu($session);?></td>
<td width="5"><img src="img/spacer.gif" width="5" height="2" alt=""></td>
<td valign="top">

<? show_wkfw_menuitem($session, $fname, ""); ?>

<form action="workflow_hol_approve_num_update_exe.php" method="post">
<table width="660" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td align="right" bgcolor="#f6f9ff" width="50%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">休暇申請の確定階層</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<label><input type="radio" name="hol_cfm" value="1"<? if ($hol_cfm == "1") {echo(" checked");} ?>>1階層</label>
<label><input type="radio" name="hol_cfm" value="2"<? if ($hol_cfm == "2") {echo(" checked");} ?>>最終承認者の階層</label>
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff" width="50%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">年休残の計算対象</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<label><input type="radio" name="remain_count_flg" value="1"<? if ($remain_count_flg == "1") {echo(" checked");} ?>>承認済みのみ</label>
<label><input type="radio" name="remain_count_flg" value="2"<? if ($remain_count_flg == "2") {echo(" checked");} ?>>申請中と承認済み</label>
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff" width="50%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">対象日数が年休残を超える申請（付与がない場合も含む）</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<label><input type="radio" name="over_apply_flg" value="1"<? if ($over_apply_flg == "1") {echo(" checked");} ?>>警告を出すが申請はできる</label>
<label><input type="radio" name="over_apply_flg" value="2"<? if ($over_apply_flg == "2") {echo(" checked");} ?>>申請できない</label>
</font></td>
</tr>
</table>
<table width="660" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="更新"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
</td>
</tr>
</table>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
