<?
require_once("about_comedix.php");
require("get_values.ini");
require_once("Cmx.php");
require_once("aclg_set.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 2, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
$current_regex_encoding = mb_regex_encoding('EUC-JP');
if (mb_ereg_replace("[ 　]", "", $schd_title) === "") {
	echo("<script type=\"text/javascript\">alert('タイトルが入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
mb_regex_encoding($current_regex_encoding);

// 日付の処理
$date = mktime(0, 0, 0, $schd_start_mth, $schd_start_day, $schd_start_yrs);
$schd_start_yrs = date("Y", $date);
$schd_start_mth = date("m", $date);
$schd_start_day = date("d", $date);
$schd_date = "$schd_start_yrs$schd_start_mth$schd_start_day";

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// トランザクションの開始
pg_query($con, "begin transaction");

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_result($sel, 0, "emp_id");

// スケジュールIDを採番
$sql = "select schd_id from schdcnt2 for update";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$schd_id = intval(pg_result($sel, 0, "schd_id")) + 1;

// スケジュールID採番用レコードを更新
$sql = "update schdcnt2 set";
$set = array("schd_id");
$setvalue = array($schd_id);
$cond = "";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// スケジュール情報を登録
$sql = "insert into schdmst2 (schd_id, emp_id, schd_title, schd_plc, schd_type, schd_imprt, schd_start_date, schd_detail, schd_place_id, schd_reg_id, schd_status, marker, reg_time) values (";
$content = array($schd_id, $emp_id, p($schd_title), "", "1", "2", $schd_date, "", null, $emp_id, "1", "0", get_millitime());
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// トランザクションをコミット
pg_query($con, "commit");

// オプション設定情報を取得
$sql = "select schedule1_default from option";
$cond = "where emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$schedule1_default = pg_fetch_result($sel, 0, "schedule1_default");

// データベース接続を閉じる
pg_close($con);

// 画面遷移
switch ($schedule1_default) {
case "1":
	$schedule_url = "schedule_menu.php";
	break;
case "2":
	$schedule_url = "schedule_week_menu.php";
	break;
case "3":
	$schedule_url = "schedule_month_menu.php";
	break;
default:
	$schedule_url = "schedule_week_menu.php";
	break;
}
echo("<script type=\"text/javascript\">location.href = '$schedule_url?session=$session&date=$date';</script>");
?>
</body>
