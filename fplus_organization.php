<?
//デバッグ出力禁止
ini_set("display_errors","0"); //"0":メッセージ出力なし "1":メッセージ出力あり

//Comedixで使用。DB、システムに応じてデータ取得処理は変更してください。
require_once("about_comedix.php");

//画面名
$fname = $PHP_SELF;

//システム名
$INCIDENT_TITLE = "ファントルくんプラス";

if (!$con) $con = connect2db($fname);



require_once("show_class_name.ini");

// 組織名の取得
$arr_class_name = get_class_name_array($con, $fname);



//==============================
// （内部用）：部門一覧を取得する
//==============================

	// 部門一覧を取得
	$sql = "select * from classmst";
	$cond = "where class_del_flg = 'f' order by class_id";
	$sel_class = select_from_table($con, $sql, $cond, $fname);
	if ($sel_class == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	

//==============================
// （内部用）：課一覧を取得する
//==============================
	// 課一覧を取得
	$sql = "select * from atrbmst";
	$cond = "where atrb_del_flg = 'f' order by atrb_id";
	$sel_atrb = select_from_table($con, $sql, $cond, $fname);
	if ($sel_atrb == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
		exit;
	}
	

//==============================
// （内部用）：科一覧を取得する
//==============================
	// 科一覧を取得
	$sql = "select * from deptmst";
	$cond = "where dept_del_flg = 'f' order by dept_id";
	$sel_dept = select_from_table($con, $sql, $cond, $fname);
	if ($sel_dept == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
		exit;
	}




//==============================
// （内部用）：室一覧を取得する
//==============================
	// 室一覧を取得
	if ($arr_class_name["class_cnt"] == 4) {
		$sql = "select * from classroom";
		$cond = "where room_del_flg = 'f' order by room_id";
		$sel_room = select_from_table($con, $sql, $cond, $fname);
		if ($sel_room == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
			exit;
		}
	} else {
		$sel_room = null;
	}





//====================================================================================================
//HTML出力
//====================================================================================================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?echo $INCIDENT_TITLE ?> | 組織検索</title>
</head>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">

function setAttributeOptions(class_box, atrb_box, dept_box, room_box, atrb_id, dept_id, room_id) 
{
	delAllOptions(atrb_box);

	var class_id = class_box.value;
<? 
$wcnt = 0;
while ($row = pg_fetch_array($sel_atrb)) { 
	$ar_atrb[$wcnt]["atrb_id"] = $row["atrb_id"];
	$ar_atrb[$wcnt]["atrb_nm"] = $row["atrb_nm"];
	$wcnt++;
?>
	if (class_id == '<? echo $row["class_id"]; ?>') {
		addOption(atrb_box, '<? echo $row["atrb_id"]; ?>', '<? echo $row["atrb_nm"]; ?>', atrb_id);
	}
<? } ?>

	if (atrb_box.options.length == 0) {
		addOption(atrb_box, '0', '（未登録）');
	}
	
	setDepartmentOptions(atrb_box, dept_box, room_box, dept_id, room_id);
}


function delAllOptions(box) {
	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}
}

function addOption(box, value, text, selected) {
	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	if (selected == value) {
		opt.selected = true;
	}
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
}


function setDepartmentOptions(atrb_box, dept_box, room_box, dept_id, room_id) {
	delAllOptions(dept_box);

	var atrb_id = atrb_box.value;
<? 
$wcnt = 0;
while ($row = pg_fetch_array($sel_dept)) { 
	
	$ar_dept[$wcnt]["dept_id"] = $row["dept_id"];
	$ar_dept[$wcnt]["dept_nm"] = $row["dept_nm"];
	$wcnt++;
	
	
?>
	if (atrb_id == '<? echo $row["atrb_id"]; ?>') {
		addOption(dept_box, '<? echo $row["dept_id"]; ?>', '<? echo $row["dept_nm"]; ?>', dept_id);
	}
<? } ?>

	if (dept_box.options.length == 0) {
		addOption(dept_box, '0', '（未登録）');
	}

	setRoomOptions(dept_box, room_box, room_id);
}


function setRoomOptions(dept_box, room_box, room_id) {
	if (!room_box) {
		return;
	}

	delAllOptions(room_box);

	var dept_id = dept_box.value;
<? 
$wcnt = 0;
while ($row = pg_fetch_array($sel_room)) { 
	
	$ar_room[$wcnt]["room_id"] = $row["room_id"];
	$ar_room[$wcnt]["room_nm"] = $row["room_nm"];
	$wcnt++;
	
?>
	if (dept_id == '<? echo $row["dept_id"]; ?>') {
		if (room_box.options.length == 0) {
			addOption(room_box, '0', '　　　　　');
		}
		addOption(room_box, '<? echo $row["room_id"]; ?>', '<? echo $row["room_nm"]; ?>', room_id);
	}
<? } ?>

	if (room_box.options.length == 0) {
		addOption(room_box, '0', '（未登録）');
	}
	
}


</script>
<style type="text/css">
form {margin:0;}
table.list {border-collapse:collapse;}
table.list td {border:solid #35B341 1px;}
</style>
<style type="text/css">


.list {border-collapse:collapse;}
.list td {border:#E6B3D4 solid 1px;}
.list td td {border-width:0;}

table.block {border-collapse:collapse;}
table.block td {border:#E6B3D4 solid 1px;padding:1px;}
table.block td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#E6B3D4 solid 0px;}

table.block2 {border-collapse:collapse;}
table.block2 td {border:#E6B3D4 solid 1px;padding:1px;}
table.block2 td {border-width:1;}

table.block3 {border-collapse:collapse;}
table.block3 td {border:#E6B3D4 solid 0px;}
table.block3 td {border-width:0;}


p {margin:0;}
</style>

<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">


<center>

<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
<?
	show_hiyari_header_for_sub_window("組織検索");
?>
</table>


<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
<tr>
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td>

<form name="main_form" action="fplus_organization.php" method="post">


<table width="100%" border="0" cellspacing="0" cellpadding="2" class="block2">

<tr height="22">

	<td align="left" bgcolor="#f6f9ff">
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属<? echo($arr_class_name[0]); ?></font>
		<select name="cls" onchange="setAttributeOptions(this.form.cls, this.form.atrb, this.form.dept, this.form.room);">
			<option value=""></option>
			<?
			$wcnt = 0;
			while ($row = pg_fetch_array($sel_class)) 
			{
				
				$ar_cls[$wcnt]["cls_id"] = $row["class_id"];
				$ar_cls[$wcnt]["cls_nm"] = $row["class_nm"];
				$wcnt++;
	
				echo("<option value=\"{$row["class_id"]}\"");
				if ($row["class_id"] == $cls) {
					echo(" selected");
				}
				echo(">{$row["class_nm"]}</option>");
			}
			?>
		</select>

		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属<? echo($arr_class_name[1]); ?></font>
		<select name="atrb" onchange="setDepartmentOptions(this.form.atrb, this.form.dept, this.form.room);"></select>
		
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属<? echo($arr_class_name[2]); ?></font>
		<select name="dept" onchange="setRoomOptions(this.form.dept, this.form.room);"></select>
		
		
		<? if ($arr_class_name["class_cnt"] == 4) { ?>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属<? echo($arr_class_name[3]); ?></font>
			<select name="room"></select>
		<? } ?>
	</td>
</tr>


<tr height="22">
<td align="left" bgcolor="#F5FFE5" style="border-style:none;">
<input type="button" value="設定" onclick="setData();"></td>
</tr>
</table>

</form>

</td>
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>
</center>
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<img src="img/spacer.gif" width="1" height="1" alt="">
</td>
</tr>
</table>


</body>
</html>

<?
/**
 * インシデントレポートサブウィンドウ用のヘッダーを表示します。
 * 
 * @param string $title タイトル
 */
function show_hiyari_header_for_sub_window($title)
{
?>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr height="32" bgcolor="#FF99CC">
	<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b><?=$title?></b></font></td>
	<td>&nbsp</td>
	<td width="10">&nbsp</td>
	<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
	</tr>
	</table>
	<img src="img/spacer.gif" width="10" height="10" alt=""><br>
<?
}

?>

<script type="text/javascript">

function setData() 
{

	if(document.main_form.cls.value == "")
	{
		alert("組織を選択してください");
		return false;
	}


var array_cls = [];
<?
	for($wcnt = 0;$wcnt < count($ar_cls);$wcnt++)
	{
?>
		array_cls['<? echo($ar_cls[$wcnt]["cls_id"]); ?>'] = '<? echo($ar_cls[$wcnt]["cls_nm"]); ?>';
<?		
	}
?>
	if(array_cls[document.main_form.cls.value] == undefined)
	{
		opener.document.apply.<? echo($setarea1); ?>.value = "(未登録)";
	}
	else
	{
		opener.document.apply.<? echo($setarea1); ?>.value = array_cls[document.main_form.cls.value];
	}


var array_atrb = [];
<?
	for($wcnt = 0;$wcnt < count($ar_atrb);$wcnt++)
	{
?>
		array_atrb['<? echo($ar_atrb[$wcnt]["atrb_id"]); ?>'] = '<? echo($ar_atrb[$wcnt]["atrb_nm"]); ?>';
<?		
	}
?>

	if(array_atrb[document.main_form.atrb.value] == undefined)
	{
		opener.document.apply.<? echo($setarea2); ?>.value = "(未登録)";
	}
	else
	{
		opener.document.apply.<? echo($setarea2); ?>.value = array_atrb[document.main_form.atrb.value];
	}

var array_dept = [];
<?
	for($wcnt = 0;$wcnt < count($ar_dept);$wcnt++)
	{
?>
		array_dept['<? echo($ar_dept[$wcnt]["dept_id"]); ?>'] = '<? echo($ar_dept[$wcnt]["dept_nm"]); ?>';
<?		
	}
?>

			if(array_dept[document.main_form.dept.value] == undefined)
			{
				opener.document.apply.<? echo($setarea3); ?>.value = "(未登録)";
			}
			else
			{
				opener.document.apply.<? echo($setarea3); ?>.value = array_dept[document.main_form.dept.value];
			}

	<? if ($arr_class_name["class_cnt"] == 4) { ?>

		var array_room = [];
		<?
			for($wcnt = 0;$wcnt < count($ar_room);$wcnt++)
			{
		?>
				array_room['<? echo($ar_room[$wcnt]["room_id"]); ?>'] = '<? echo($ar_room[$wcnt]["room_nm"]); ?>';
		<?		
			}
		?>
		
			if(array_room[document.main_form.room.value] == undefined)
			{
				opener.document.apply.<? echo($setarea4); ?>.value = "(未登録)";
			}
			else
			{
				opener.document.apply.<? echo($setarea4); ?>.value = array_room[document.main_form.room.value];
			}
	<? } ?>



	//自画面を終了
	self.close();
}


</script>
