<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_select_values.ini");
require_once("show_class_name.ini");
require_once("hiyari_el_common.ini");
require_once("hiyari_common.ini");
require_once("Cmx.php");
require_once('Cmx/View/Smarty.php');

//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0")
{
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// 権限チェック
$auth_id = ($path != "3") ? 47 : 48;
if (check_authority($session, $auth_id, $fname) == "0")
{
    showLoginPage();
    exit;
}



//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0")
{
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//==================================================
//デザイン（1=旧デザイン、2=新デザイン）
//==================================================
$sysConf = new Cmx_SystemConfig();
$design_mode = $sysConf->get('fantol.design.mode');

//==============================
// ログインユーザの職員ID・所属部署IDを取得
//==============================
$sql = "select emp_id, emp_class, emp_attribute, emp_dept, emp_st, (emp_lt_nm || ' ' || emp_ft_nm) as emp_name from empmst";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel_emp = select_from_table($con, $sql, $cond, $fname);
if ($sel_emp == 0)
{
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$emp_id = pg_fetch_result($sel_emp, 0, "emp_id");
$emp_class = pg_fetch_result($sel_emp, 0, "emp_class");
$emp_atrb = pg_fetch_result($sel_emp, 0, "emp_attribute");
$emp_dept = pg_fetch_result($sel_emp, 0, "emp_dept");
$emp_st = pg_fetch_result($sel_emp, 0, "emp_st");
$emp_name = pg_fetch_result($sel_emp, 0, "emp_name");



//==============================
// 更新可能フラグの設定
//==============================

//管理画面の場合：常に更新可能
if ($path == "3")
{
    $upd_flg = true;
}
//ユーザー画面の場合：フォルダ・コンテンツ両方の更新権限が必要
else
{
    // フォルダの更新権限取得
    $cur_upd_flg = get_upd_flg($con, $fname, $a, $c, $f, $emp_id, $emp_class, $emp_atrb, $emp_dept, $emp_st);

    // コンテンツの更新権限取得
    $upd_flg = false;
    if ($cur_upd_flg == true)
    {
        $sql   = " select count(*) from el_info inner join (select * from el_cate where el_cate.libcate_del_flg = 'f') el_cate on el_info.lib_cate_id = el_cate.lib_cate_id";
        $cond  = " where lib_id = $lib_id";
        $cond .= " and";
        $cond .= " (";
            // 更新：所属
            $cond .= "(";
                $cond .= "(el_info.upd_dept_flg = '1' or (el_info.upd_dept_flg = '2' and exists (select * from el_upd_dept where el_upd_dept.lib_id = el_info.lib_id and el_upd_dept.class_id = $emp_class and el_upd_dept.atrb_id = $emp_atrb and el_upd_dept.dept_id = $emp_dept)))";
                $cond .= " and ";
                $cond .= "(el_info.upd_st_flg = '1' or (el_info.upd_st_flg = '2' and exists (select * from el_upd_st where el_upd_st.lib_id = el_info.lib_id and el_upd_st.st_id = $emp_st)))";
            $cond .= ")";
            // 更新：兼務所属
            $cond .= " or ";
            $cond .= "(";
                $cond .= "(el_info.upd_dept_flg = '1' or (el_info.upd_dept_flg = '2' and exists (select * from concurrent, el_upd_dept where  concurrent.emp_id = '$emp_id' and concurrent.emp_class = el_upd_dept.class_id and concurrent.emp_attribute = el_upd_dept.atrb_id and concurrent.emp_dept = el_upd_dept.dept_id and el_upd_dept.lib_id = el_info.lib_id)))";
                $cond .= " and ";
                $cond .= "(el_info.upd_st_flg = '1' or (el_info.upd_st_flg = '2' and exists (select * from concurrent, el_upd_st where concurrent.emp_id = '$emp_id' and el_upd_st.st_id = concurrent.emp_st and el_upd_st.lib_id = el_info.lib_id)))";
            $cond .= ")";
            // 更新：職員
            $cond .= " or";
            $cond .= " exists (select * from el_upd_emp where el_upd_emp.lib_id = el_info.lib_id and el_upd_emp.emp_id = '$emp_id')";
        $cond .= ")";

        $sel_upd_flg = select_from_table($con, $sql, $cond, $fname);
        if ($sel_upd_flg == 0)
        {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $upd_flg = (pg_fetch_result($sel_upd_flg, 0, 0) > 0);
    }
}


//==============================
//所属・役職一覧の取得
//==============================

// 部門一覧検索
$sql = "select class_id, class_nm from classmst";
$cond = "where class_del_flg = 'f' order by order_no";
$sel_class = select_from_table($con, $sql, $cond, $fname);
if ($sel_class == 0)
{
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// 課一覧検索
$sql = "select classmst.class_id, atrbmst.atrb_id, classmst.class_nm, atrbmst.atrb_nm from atrbmst inner join classmst on atrbmst.class_id = classmst.class_id";
$cond = "where classmst.class_del_flg = 'f' and atrbmst.atrb_del_flg = 'f' order by classmst.class_id, atrbmst.order_no";
$sel_atrb = select_from_table($con, $sql, $cond, $fname);
if ($sel_atrb == 0)
{
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// 科一覧検索
$sql = "select classmst.class_id, atrbmst.atrb_id, deptmst.dept_id, classmst.class_nm, atrbmst.atrb_nm, deptmst.dept_nm from deptmst inner join atrbmst on deptmst.atrb_id = atrbmst.atrb_id inner join classmst on atrbmst.class_id = classmst.class_id";
$cond = "where classmst.class_del_flg = 'f' and atrbmst.atrb_del_flg = 'f' and deptmst.dept_del_flg = 'f' order by classmst.class_id, atrbmst.atrb_id, deptmst.order_no";
$sel_dept = select_from_table($con, $sql, $cond, $fname);
if ($sel_dept == 0)
{
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// 役職一覧検索
$sql = "select st_id, st_nm from stmst";
$cond = "where st_del_flg = 'f' order by order_no";
$sel_st = select_from_table($con, $sql, $cond, $fname);
if ($sel_st == 0)
{
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

//科一覧配列作成
$dept_names = Array();
while ($row = pg_fetch_array($sel_dept))
{
    $tmp_class_id = $row["class_id"];
    $tmp_atrb_id = $row["atrb_id"];
    $tmp_dept_id = $row["dept_id"];
    $tmp_dept_nm = $row["dept_nm"];
    $dept_names["{$tmp_class_id}-{$tmp_atrb_id}-{$tmp_dept_id}"] = $tmp_dept_nm;
}
pg_result_seek($sel_dept, 0);



//==============================
// 組織名を取得
//==============================
$arr_class_name = get_class_name_array($con, $fname);





// コンテンツ情報を取得
$sql = "select el_info.*, el_cate.lib_cate_nm, classmst.class_nm from ((el_info inner join el_cate on el_info.lib_cate_id = el_cate.lib_cate_id) inner join empmst on el_info.emp_id = empmst.emp_id) inner join classmst on empmst.emp_class = classmst.class_id";
$cond = "where el_info.lib_id = '$lib_id'";
$sel_lib = select_from_table($con, $sql, $cond, $fname);
if ($sel_lib == 0)
{
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$reg_emp_id = pg_fetch_result($sel_lib, 0, "emp_id");
$reg_emp_name = get_emp_kanji_name($con, $reg_emp_id, $fname);
$reg_class_nm = pg_fetch_result($sel_lib, 0, "class_nm");
$upd_time = pg_fetch_result($sel_lib, 0, "lib_up_date");
if (strlen($upd_time) == 14)
{
    $upd_time = preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})$/", "$1/$2/$3 $4:$5:$6", $upd_time);
}
else
{
    $upd_time = preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $upd_time);
}
$ref_count = pg_fetch_result($sel_lib, 0, "ref_count");

// コンテンツファイルのパス
$document_path = get_lib_path(
    pg_fetch_result($sel_lib, 0, "lib_cate_id"),
    $lib_id,
    pg_fetch_result($sel_lib, 0, "lib_extension")
);
$lib_url = "hiyari_el_refer.php?s=$session&i=$lib_id&u=" . urlencode($document_path);

// 版番号を取得
$sql = "select edition_no from el_edition";
$cond = "where lib_id = $lib_id";
$sel_edition = select_from_table($con, $sql, $cond, $fname);
if ($sel_edition == 0)
{
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$edition_no = pg_fetch_result($sel_edition, 0, "edition_no");

// 初期表示時の設定
if (!is_array($ref_dept)) {$ref_dept = array();}
if (!is_array($ref_st)) {$ref_st = array();}
if (!is_array($upd_dept)) {$upd_dept = array();}
if (!is_array($upd_st)) {$upd_st = array();}
if ($back != "t")
{
    $archive = pg_fetch_result($sel_lib, 0, "lib_archive");
    $category = pg_fetch_result($sel_lib, 0, "lib_cate_id");
    $folder_id = pg_fetch_result($sel_lib, 0, "folder_id");
    $document_name = pg_fetch_result($sel_lib, 0, "lib_nm");
    $document_type = pg_fetch_result($sel_lib, 0, "lib_type");
    $keywd = pg_fetch_result($sel_lib, 0, "lib_keyword");
    $lib_no = pg_fetch_result($sel_lib, 0, "lib_no");
    $explain = pg_fetch_result($sel_lib, 0, "lib_summary");
    $ref_dept_st_flg = pg_fetch_result($sel_lib, 0, "ref_dept_st_flg");
    $ref_dept_flg = pg_fetch_result($sel_lib, 0, "ref_dept_flg");
    $ref_st_flg = pg_fetch_result($sel_lib, 0, "ref_st_flg");
    $upd_dept_st_flg = pg_fetch_result($sel_lib, 0, "upd_dept_st_flg");
    $upd_dept_flg = pg_fetch_result($sel_lib, 0, "upd_dept_flg");
    $upd_st_flg = pg_fetch_result($sel_lib, 0, "upd_st_flg");


    $sql = "select e.class_id, e.atrb_id, e.dept_id from el_ref_dept e left join classmst c on e.class_id = c.class_id left join atrbmst a on e.atrb_id = a.atrb_id left join deptmst d on e.dept_id = d.dept_id";
    $cond = "where e.lib_id = $lib_id order by c.order_no, a.order_no, d.order_no";
    $sel_ref_dept = select_from_table($con, $sql, $cond, $fname);
    if ($sel_ref_dept == 0)
    {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    while ($row = pg_fetch_array($sel_ref_dept))
    {
        $tmp_class_id = $row["class_id"];
        $tmp_atrb_id = $row["atrb_id"];
        $tmp_dept_id = $row["dept_id"];
        $ref_dept[] = "$tmp_class_id-$tmp_atrb_id-$tmp_dept_id";
    }

    $sql = "select st_id from el_ref_st";
    $cond = "where lib_id = $lib_id order by st_id";
    $sel_ref_st = select_from_table($con, $sql, $cond, $fname);
    if ($sel_ref_st == 0)
    {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    while ($row = pg_fetch_array($sel_ref_st))
    {
        $tmp_st_id = $row["st_id"];
        $ref_st[] = $tmp_st_id;
    }

    $sql = "select emp_id from el_ref_emp";
    $cond = "where lib_id = $lib_id order by emp_id";
    $sel_ref_emp = select_from_table($con, $sql, $cond, $fname);
    if ($sel_ref_emp == 0)
    {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $target_id_list1 = "";
    $is_first_flg = true;
    while ($row = pg_fetch_array($sel_ref_emp))
    {
        $tmp_emp_id = $row["emp_id"];
        if ($is_first_flg)
        {
            $is_first_flg = false;
        }
        else
        {
            $target_id_list1 .= ",";
        }
        $target_id_list1 .= $tmp_emp_id;
    }

    $sql = "select e.class_id, e.atrb_id, e.dept_id from el_upd_dept e left join classmst c on e.class_id = c.class_id left join atrbmst a on e.atrb_id = a.atrb_id left join deptmst d on e.dept_id = d.dept_id";
    $cond = "where e.lib_id = $lib_id order by c.order_no, a.order_no, d.order_no";
    $sel_upd_dept = select_from_table($con, $sql, $cond, $fname);
    if ($sel_upd_dept == 0)
    {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    while ($row = pg_fetch_array($sel_upd_dept))
    {
        $tmp_class_id = $row["class_id"];
        $tmp_atrb_id = $row["atrb_id"];
        $tmp_dept_id = $row["dept_id"];
        $upd_dept[] = "$tmp_class_id-$tmp_atrb_id-$tmp_dept_id";
    }

    $sql = "select st_id from el_upd_st";
    $cond = "where lib_id = $lib_id order by st_id";
    $sel_upd_st = select_from_table($con, $sql, $cond, $fname);
    if ($sel_upd_st == 0)
    {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    while ($row = pg_fetch_array($sel_upd_st))
    {
        $tmp_st_id = $row["st_id"];
        $upd_st[] = $tmp_st_id;
    }

    $sql = "select emp_id from el_upd_emp";
    $cond = "where lib_id = $lib_id order by emp_id";
    $sel_upd_emp = select_from_table($con, $sql, $cond, $fname);
    if ($sel_upd_emp == 0)
    {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $target_id_list2 = "";
    $is_first_flg = true;
    while ($row = pg_fetch_array($sel_upd_emp))
    {
        $tmp_emp_id = $row["emp_id"];
        if ($is_first_flg)
        {
            $is_first_flg = false;
        }
        else
        {
            $target_id_list2 .= ",";
        }
        $target_id_list2 .= $tmp_emp_id;
    }
}

// デフォルト値の設定
if ($manage_history == "") {$manage_history = "t";}
if ($upd_dept_flg != "1" && $upd_dept_flg != "2") {$upd_dept_flg = "1";}
if ($upd_st_flg != "1" && $upd_st_flg != "2") {$upd_st_flg = "1";}
if ($ref_dept_flg != "1" && $ref_dept_flg != "2") {$ref_dept_flg = "1";}
if ($ref_st_flg != "1" && $ref_st_flg != "2") {$ref_st_flg = "1";}


if ($ref_toggle_mode == "") {$ref_toggle_mode = "▼";}
$ref_toggle_display = ($ref_toggle_mode == "▼") ? "none" : "";
if ($upd_toggle_mode == "") {$upd_toggle_mode = "▼";}
$upd_toggle_display = ($upd_toggle_mode == "▼") ? "none" : "";

//==============================
// メンバー情報を配列に格納
//==============================
for ($list_idx=1; $list_idx<=2; $list_idx++)
{
    $arr_target["$list_idx"] = array();
    $target_id_list_name = "target_id_list$list_idx";
    $target_id_list = $$target_id_list_name;
    if ($target_id_list != "")
    {
        $arr_target_id = split(",", $target_id_list);
        for ($i = 0; $i < count($arr_target_id); $i++)
        {
            $tmp_emp_id = $arr_target_id[$i];
            $sql = "select (emp_lt_nm || ' ' || emp_ft_nm) as emp_name from empmst";
            $cond = "where emp_id = '$tmp_emp_id'";
            $sel = select_from_table($con, $sql, $cond, $fname);
            if ($sel == 0)
            {
                pg_close($con);
                echo("<script type='text/javascript' src='./js/showpage.js'></script>");
                echo("<script language='javascript'>showErrorPage(window);</script>");
            }
            $tmp_emp_name = pg_fetch_result($sel, 0, "emp_name");
            array_push($arr_target["$list_idx"], array("id" => $tmp_emp_id, "name" => $tmp_emp_name));
        }
    }
}

//====================================
//表示
//====================================
$smarty = new Cmx_View();

$smarty->assign("INCIDENT_TITLE", $INCIDENT_TITLE);
$smarty->assign("PAGE_TITLE", "コンテンツ更新");
$smarty->assign("session", $session);

$smarty->assign("lib_id", $lib_id);
$smarty->assign("a", $a);
$smarty->assign("c", $c);
$smarty->assign("f", $f);
$smarty->assign("o", $o);
$smarty->assign("path", $path);

//ファイルサイズの上限
$smarty->assign("upload_max_filesize", ini_get("upload_max_filesize"));

//実行ボタン
$smarty->assign("upd_flg", $upd_flg);
if (lib_get_filename_flg() > 0){
    $refer_onclick = "location.href='$lib_url';";
}
else{
    $refer_onclick = "window.open('$lib_url');";
}
$smarty->assign("refer_onclick", $refer_onclick);

//履歴管理
$smarty->assign("manage_history", $manage_history);

//コンテンツ名
$smarty->assign("document_name", h($document_name));

//コンテンツタイプ
$smarty->assign("document_type", $document_type);

//キーワード
$smarty->assign("keywd", h($keywd));

//コンテンツ番号
$smarty->assign("lib_no", h($lib_no));

//説明
$smarty->assign("explain", h($explain));

//保存先
$smarty->assign("cate_nm", lib_get_category_name($con, $category, $fname));
$smarty->assign("folder_path", lib_get_folder_path($con, $folder_id, $fname));
$smarty->assign("archive", $archive);
$smarty->assign("category", $category);
$smarty->assign("folder_id", $folder_id);

//登録者
$smarty->assign("reg_emp_name", $reg_emp_name);
$smarty->assign("reg_class_nm", $reg_class_nm);

//更新日時
$smarty->assign("upd_time", $upd_time);

//参照数
$smarty->assign("has_admin_auth", check_authority($session, 48, $fname) == "1");
$smarty->assign("ref_count", $ref_count);

//版番号
$smarty->assign("edition_no", $edition_no);

//------------------------------------
//参照可能範囲・更新可能範囲共通
//------------------------------------
//部署
$smarty->assign("sel_class", pg_fetch_all($sel_class));

$tmp_atrb = array();
foreach(pg_fetch_all($sel_atrb) as $atrb){
    $tmp_atrb[ $atrb['class_id'] ][ $atrb['atrb_id'] ] = $atrb['atrb_nm'];
}
$smarty->assign("sel_atrb", $tmp_atrb);

$tmp_dept = array();
foreach(pg_fetch_all($sel_dept) as $dept){
    $tmp_dept[ $dept['class_id'] ][ $dept['atrb_id'] ][ $dept['dept_id'] ] = $dept['dept_nm'];
}
$smarty->assign("sel_dept", $tmp_dept);

$smarty->assign("arr_class_name", $arr_class_name);
$smarty->assign("dept_names", $dept_names);

//役職
$smarty->assign("sel_st", pg_fetch_all($sel_st));

//職員
$smarty->assign("emp_id", $emp_id);
$smarty->assign("emp_name", $emp_name);
$smarty->assign("arr_target", $arr_target);

//------------------------------------
//参照可能範囲
//------------------------------------
$smarty->assign("ref_toggle_mode", $ref_toggle_mode);
$smarty->assign("ref_toggle_display", $ref_toggle_display);

//部署
$smarty->assign("ref_class_src", $ref_class_src);
$smarty->assign("ref_atrb_src", $ref_atrb_src);
$smarty->assign("ref_dept_st_flg", $ref_dept_st_flg);
$smarty->assign("ref_dept_flg", $ref_dept_flg);
$smarty->assign("ref_dept", $ref_dept);

//役職
$smarty->assign("ref_st_flg", $ref_st_flg);
$smarty->assign("ref_st", $ref_st);

//------------------------------------
//更新可能範囲
//------------------------------------
$smarty->assign("upd_toggle_mode", $upd_toggle_mode);
$smarty->assign("upd_toggle_display", $upd_toggle_display);

//部署
$smarty->assign("upd_class_src", $upd_class_src);
$smarty->assign("upd_atrb_src", $upd_atrb_src);
$smarty->assign("upd_dept_st_flg", $upd_dept_st_flg);
$smarty->assign("upd_dept_flg", $upd_dept_flg);
$smarty->assign("upd_dept", $upd_dept);

//役職
$smarty->assign("upd_st_flg", $upd_st_flg);
$smarty->assign("upd_st", $upd_st);

if ($design_mode == 1){
    $smarty->display("hiyari_el_update1.tpl");
}
else{
    $smarty->display("hiyari_el_update2.tpl");
}

pg_close($con);