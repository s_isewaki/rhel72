<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_session.php");
require("show_clock_in_common.ini");
require("get_values.ini");
require("timecard_common_class.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// セッションから職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// 勤務条件のパターン取得
$sql = "SELECT tmcd_group_id, atdptn_id, no_overtime FROM empcond";
$cond = "where emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if (pg_num_rows($sel) > 0) {
	$empcond_tmcd_group_id = pg_fetch_result($sel, 0, "tmcd_group_id");
	$empcond_atdptn_id     = pg_fetch_result($sel, 0, "atdptn_id");
	$no_overtime = pg_fetch_result($sel, 0, "no_overtime");	//残業管理をしないフラグ
}
else{
	$empcond_tmcd_group_id = null;
	$empcond_atdptn_id     = null;
	$no_overtime = "f";
}
//勤務条件の出勤パターングループがない場合、勤務シフト作成のグループ 20100622
if ($empcond_tmcd_group_id == "") {
	$sql = "select pattern_id from duty_shift_group a ";
	$cond = "where exists (select b.group_id from duty_shift_staff b where b.emp_id = '$emp_id' and a.group_id = b.group_id)";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0) {
		$empcond_tmcd_group_id = pg_fetch_result($sel, 0, "pattern_id");
	}
}

// 勤務実績レコードを取得
//$arr_result = get_atdbkrslt_array($con, $emp_id, $fname, ($wherefrom == "1"));
// 引数の日付から勤務実績レコードを取得
//$arr_result = get_atdbkrslt($con, $emp_id, $timecard_date, $fname);
$timecard_common_class = new timecard_common_class($con, $fname, $emp_id, date("Ymd", strtotime("-1 day")), date("Ymd", strtotime("1 day")));
$arr_result = $timecard_common_class->get_clock_in_atdbkrslt_array($con, $emp_id, $fname);

//翌日の勤務パターンの勤務開始時刻が前日であるフラグの設定を確認
//※フラグが前日の場合、実績のレコードは翌日へ登録する。
$tmp_previous_day_flag = $timecard_common_class->get_previous_day_flag($emp_id);
if ($tmp_previous_day_flag == "1") {
	$arr_result = $timecard_common_class->get_atdbkrslt_array($con, date("Ymd", strtotime("1 day")), $emp_id, $fname);
//	$previous_day_flag = 1;
	//実績の勤務パターンが未設定の場合、予定を取得 ※開始時刻が前日の不具合対応 20090910
	if ($arr_result["pattern"] == "") {
		$arr_result = $timecard_common_class->get_atdbk_array($con, date("Ymd", strtotime("1 day")), $emp_id, $fname);
	}
}

// 登録可能かどうかチェック
switch ($wherefrom) {
case "1":  // 出勤
	if ($arr_result["start_btn_time"] != "") {
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('出勤登録済みです。');</script>");
		echo("<script type=\"text/javascript\">location.href = 'main_menu.php?session=$session';</script>");
		exit;
	}
	$arr_result["start_time"] = date("Hi");
	$arr_result["start_btn_time"] = $arr_result["start_time"];
	$arr_result["start_date"] = date("Ymd");
	$arr_result["start_btn_date1"] = date("Ymd");
	$status = "1";
	break;
case "2":  // 外出
	if ($arr_result["start_time"] == "") {
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('出勤登録されていません。');</script>");
		echo("<script type=\"text/javascript\">location.href = 'main_menu.php?session=$session';</script>");
		exit;
	}
	if ($arr_result["out_time"] != "") {
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('外出登録済みです。');</script>");
		echo("<script type=\"text/javascript\">location.href = 'main_menu.php?session=$session';</script>");
		exit;
	}
	if ($arr_result["end_time"] != "") {
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('退勤登録済みです。');</script>");
		echo("<script type=\"text/javascript\">location.href = 'main_menu.php?session=$session';</script>");
		exit;
	}
	$arr_result["out_time"] = date("Hi");
	$status = "2";
	break;
case "3":  // 復帰
	if ($arr_result["out_time"] == "") {
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('外出登録されていません。');</script>");
		echo("<script type=\"text/javascript\">location.href = 'main_menu.php?session=$session';</script>");
		exit;
	}
	if ($arr_result["ret_time"] != "") {
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('復帰登録済みです。');</script>");
		echo("<script type=\"text/javascript\">location.href = 'main_menu.php?session=$session';</script>");
		exit;
	}
	$arr_result["ret_time"] = date("Hi");
	$status = "1";
	break;
case "4":  // 退勤
	if ($arr_result["start_time"] == "") {
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('出勤登録されていません。');</script>");
		echo("<script type=\"text/javascript\">location.href = 'main_menu.php?session=$session';</script>");
		exit;
	}
	if ($arr_result["out_time"] != "" && $arr_result["ret_time"] == "") {
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('復帰登録されていません。');</script>");
		echo("<script type=\"text/javascript\">location.href = 'main_menu.php?session=$session';</script>");
		exit;
	}
	if ($arr_result["end_btn_time"] != "") {
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('退勤登録済みです。');</script>");
		echo("<script type=\"text/javascript\">location.href = 'main_menu.php?session=$session';</script>");
		exit;
	}
	$arr_result["end_time"] = date("Hi");
	$arr_result["end_btn_time"] = $arr_result["end_time"];
	$arr_result["end_date"] = date("Ymd");
	$arr_result["end_btn_date1"] = date("Ymd");
	$status = "3";
	//会議・研修時間、時間帯画面で設定がある場合は、実績へ設定する
		if ($arr_result["meeting_start_time"] == "" && $arr_result["a_meeting_start_time"] != "") {
			$arr_result["meeting_start_time"] = $arr_result["a_meeting_start_time"];
		}
		if ($arr_result["meeting_end_time"] == "" && $arr_result["a_meeting_end_time"] != "") {
			$arr_result["meeting_end_time"] = $arr_result["a_meeting_end_time"];
		}
		break;
}

//実績のパターンが未設定または休暇の場合
if ($arr_result["pattern"] == "" || $arr_result["pattern"] == "10") {
	//予定の情報を、実績へ更新する
	$arr_atdbk = $timecard_common_class->get_atdbk_array($con, $arr_result["date"], $emp_id, $fname);

	$arr_result["pattern"]       = $arr_atdbk["pattern"];
	$arr_result["reason"]       = $arr_atdbk["reason"];
	$arr_result["night_duty"]       = $arr_atdbk["night_duty"];
	$arr_result["allow_id"]       = $arr_atdbk["allow_id"];
	$arr_result["tmcd_group_id"]       = $arr_atdbk["tmcd_group_id"];

}
// 月給者は出勤予定なしでの打刻を不可とする
// 時給、日給者も勤務条件にパターン指定がある場合使用する 20101129 is_monthly_emp($con, $emp_id, $fname) && 
if ($arr_result["pattern"] == "") {
	//勤務条件にパターン指定がある場合使用する
	if (empty($empcond_atdptn_id) == false){
		$arr_result["tmcd_group_id"] = $empcond_tmcd_group_id;
		$arr_result["pattern"] = $empcond_atdptn_id;
	}
	else{
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('出勤予定が登録されていません。');</script>");
		echo("<script type=\"text/javascript\">location.href = 'main_menu.php?session=$session';</script>");
		exit;
	}
}

// 勤務実績レコードをDELETE〜INSERT
$sql = "delete from atdbkrslt";
$cond = "where emp_id = '$emp_id' and date = '{$arr_result["date"]}'";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

//出勤日前日/翌日フラグの判定
$previous_day = date("Ymd", strtotime("-1 day"));
$next_day = date("Ymd", strtotime("1 day"));
$previous_day_flag = $arr_result["previous_day_flag"];
$next_day_flag = $arr_result["next_day_flag"];

//前日の出勤打刻有りかつ、前日の退勤打刻が無しかつ、退勤ボタンの場合は翌日フラグを立てる
if ($arr_result["date"] == $previous_day && $wherefrom == "4"){
	$next_day_flag = 1;
}

//更新対象日が翌日かつ、出勤ボタン押下時は前日フラグを立てる
if ($arr_result["date"] == $next_day && $wherefrom == "1"){
	$previous_day_flag = 1;
}

$sql = "insert into atdbkrslt (emp_id, date, pattern, reason, night_duty, allow_id, start_time, out_time, ret_time, end_time, o_start_time1, o_end_time1, o_start_time2, o_end_time2, o_start_time3, o_end_time3, o_start_time4, o_end_time4, o_start_time5, o_end_time5, o_start_time6, o_end_time6, o_start_time7, o_end_time7, o_start_time8, o_end_time8, o_start_time9, o_end_time9, o_start_time10, o_end_time10, status, tmcd_group_id, meeting_time, previous_day_flag, next_day_flag, reg_prg_flg, start_btn_time, end_btn_time, start_date, end_date, meeting_start_time, meeting_end_time, allow_count, over_start_time, over_end_time, over_start_next_day_flag, over_end_next_day_flag, over_start_time2, over_end_time2, over_start_next_day_flag2, over_end_next_day_flag2, rest_start_time, rest_end_time, start_btn_date1, end_btn_date1) values (";
$content = array($emp_id, $arr_result["date"], $arr_result["pattern"], $arr_result["reason"], $arr_result["night_duty"], $arr_result["allow_id"], $arr_result["start_time"], $arr_result["out_time"], $arr_result["ret_time"], $arr_result["end_time"], $arr_result["o_start_time1"], $arr_result["o_end_time1"], $arr_result["o_start_time2"], $arr_result["o_end_time2"], $arr_result["o_start_time3"], $arr_result["o_end_time3"], $arr_result["o_start_time4"], $arr_result["o_end_time4"], $arr_result["o_start_time5"], $arr_result["o_end_time5"], $arr_result["o_start_time6"], $arr_result["o_end_time6"], $arr_result["o_start_time7"], $arr_result["o_end_time7"], $arr_result["o_start_time8"], $arr_result["o_end_time8"], $arr_result["o_start_time9"], $arr_result["o_end_time9"], $arr_result["o_start_time10"], $arr_result["o_end_time10"], $status, $arr_result["tmcd_group_id"], $arr_result["meeting_time"], $previous_day_flag, $next_day_flag, "1", $arr_result["start_btn_time"], $arr_result["end_btn_time"], $arr_result["start_date"], $arr_result["end_date"], $arr_result["meeting_start_time"], $arr_result["meeting_end_time"], $arr_result["allow_count"], $arr_result["over_start_time"], $arr_result["over_end_time"], $arr_result["over_start_next_day_flag"], $arr_result["over_end_next_day_flag"], $arr_result["over_start_time2"], $arr_result["over_end_time2"], $arr_result["over_start_next_day_flag2"], $arr_result["over_end_next_day_flag2"], $arr_result["rest_start_time"], $arr_result["rest_end_time"], $arr_result["start_btn_date1"], $arr_result["end_btn_date1"]);
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 翌日以降の休暇を実績へ登録
$timecard_common_class->next_hol_set($arr_result["date"], $emp_id);

$sinseisumi_flg = false;
$ovtmscr_flg = false;	
if ($wherefrom == "4") { //退勤ボタンの時、残業申請対応
	//残業時刻が既にある場合は申請状態を確認、申請中、承認済は、申請画面表示は不要 20111109
	if ($arr_result["over_start_time"] != "" || $arr_result["over_end_time"] != "" ||
			$arr_result["over_start_time2"] != "" || $arr_result["over_end_time2"] != "") {
		//既存データ確認、削除以外
		$wk_date = $arr_result["date"];
		$sql = "select apply_id, apply_status from ovtmapply ";
		$cond = "where emp_id = '$emp_id' and target_date = '$wk_date' and delete_flg = 'f' order by apply_id desc limit 1";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$num = pg_num_rows($sel);
		if ($num > 0) {
			$apply_status = pg_fetch_result($sel, 0, "apply_status");
			$apply_id = pg_fetch_result($sel, 0, "apply_id");
			//申請中、承認済み
			if ($apply_status == "0" || $apply_status == "1") {
				$sinseisumi_flg = true;
			}
		}
	}
	//申請済みは、画面表示不要
	if ($sinseisumi_flg) {
		$ovtmscr_flg = false;	
	}
	else {
		//所定終了時刻より退勤時刻があとの場合は、残業申請判定のため残業終了時刻とする 20110913
		$wk_office_end_time = str_replace(":", "", $arr_result["office_end_time"]);
		if ($arr_result["end_time"] > $wk_office_end_time) {
			$wk_over_end_time = $arr_result["end_time"];
		} else {
			$wk_over_end_time = "";
		}
        //個人別勤務時間帯履歴対応 20130220 start
        $emp_officehours = $timecard_common_class->get_officehours_emp($emp_id, $arr_result["date"], $arr_result["tmcd_group_id"], $arr_result["pattern"]);
        if ($emp_officehours["office_start_time"] != "") {
            $arr_result["office_start_time"] = $emp_officehours["office_start_time"];
            $arr_result["office_end_time"] = $emp_officehours["office_end_time"];
        }
        //個人別勤務時間帯履歴対応 20130220 end
        // 残業申請画面表示フラグを設定
		$ovtmscr_flg = $timecard_common_class->is_overtime_apply($arr_result["date"], $arr_result["start_time"], $previous_day_flag, $arr_result["end_time"], $next_day_flag, $arr_result["night_duty"], $arr_result["office_start_time"], $arr_result["office_end_time"], false, $arr_result["tmcd_group_id"], "", $wk_over_end_time, "", $arr_result["over_24hour_flag"]);
	}
}

// データベース接続を閉じる
pg_close($con);

// 職員登録の勤務条件の残業管理をしないフラグを条件追加 20090924
if ($ovtmscr_flg && $no_overtime == "f") {
	// 残業申請子画面を表示
	echo("<script type=\"text/javascript\">window.open('overtime_apply.php?session=$session&date={$arr_result["date"]}', 'newwin', 'width=640,height=480,scrollbars=yes');</script>");
}

// トップページを再表示
echo("<script type=\"text/javascript\">location.href = 'main_menu.php?session=$session';</script>");
?>
