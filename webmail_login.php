<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>ウェブメール</title>
<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// ウェブメール権限のチェック
$checkauth = check_authority($session, 0, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// ログイン中ユーザのメールアカウント情報を取得
$sql = "select get_mail_login_id(emp_id) as account, emp_login_pass from login";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$account = pg_fetch_result($sel, 0, "account");
$login_pass = pg_fetch_result($sel, 0, "emp_login_pass");

// Cyrus使用フラグを取得
$sql = "select use_cyrus from config";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$use_cyrus = pg_fetch_result($sel, 0, "use_cyrus");

// Cyrusを使わない場合、ログインユーザのE-Mailをメールアカウントとして使用
if ($use_cyrus == "f") {
	$sql = "select emp_email2 from empmst";
	$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$account = pg_fetch_result($sel, 0, "emp_email2");
}

// データベース接続を閉じる
pg_close($con);
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function login() {
	var login_form = login_frame.document.forms[0];
	login_form.login_username.value = '<? echo($account); ?>';
	login_form.secretkey.value = '<? echo($login_pass); ?>';
	login_form.target = '_parent';
	login_form.submit();
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<iframe name="login_frame" src="./webmail/src/login.php" width="0" height="0" frameborder="0">
</body>
</html>
