<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" action="hiyari_rm_post_register.php" method="post">
<?
foreach ($_POST as $key => $value) {
	if (!is_array($value)) {
		echo("<input type=\"hidden\" name=\"$key\" value=\"$value\">\n");
	} else {
		foreach ($value as $array_value) {
			echo("<input type=\"hidden\" name=\"{$key}[]\" value=\"$array_value\">\n");
		}
	}
}
?>
</form>
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("Cmx.php");
require_once("aclg_set.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}


//====================================
// 権限チェック
//====================================
$auth_chk = check_authority($session, 48, $fname);
if ($auth_chk == '0') { 
	showLoginPage();
	exit;
}

$regist_data = array();
foreach ($_POST as $key => $value) {

	if (substr_count($key, "_") != 1) {
		continue;
	}

	list($tmp_column, $tmp_id) = split("_", $key);

	if ($tmp_column != "head") {
		continue;
	}

	$regist_data[$tmp_id][$tmp_column] = $value;
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// トランザクションを開始
pg_query($con, "begin");

// 部門データを全削除
$sql = "delete from inci_rm";
$cond = "where post_id = $post_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 部門データを登録
foreach ($regist_data as $id => $data) {

	list($class_id, $atrb_id, $dept_id, $room_id) = split("-", $id);
	if ($atrb_id == "") {$atrb_id = null;}
	if ($dept_id == "") {$dept_id = null;}
	if ($room_id == "") {$room_id = null;}

	if($data["head"] != "") {
		$arr_emp_id = split(",", $data["head"]);

		for($i=0; $i<count($arr_emp_id);$i++) {

			$tmp_id = $arr_emp_id[$i];
			$sql = "insert into inci_rm (post_id, class_id, attribute_id, dept_id, room_id, disp_index, emp_id) values (";
			$content = array($post_id, $class_id, $atrb_id, $dept_id, $room_id, $i + 1, $tmp_id);
			$ins = insert_into_table($con, $sql, $content, $fname);
			if ($ins == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}
}


// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 画面を再表示
echo("<script type=\"text/javascript\">items.submit();</script>\n");
?>
</body>
