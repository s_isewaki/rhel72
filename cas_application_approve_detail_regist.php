<?
require_once("about_postgres.php");
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.ini");
require_once("cas_application_workflow_common_class.php");
require_once("cas_common.ini");


$fname=$PHP_SELF;


// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 決裁・申請権限のチェック
$checkauth = check_authority($session, $CAS_MENU_AUTH, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);



//テンプレートの場合XML形式のテキスト$contentを作成
if ($wkfw_content_type == "2") { 
	$ext = ".php";
	$savexmlfilename = "cas_workflow/tmp/{$session}_x{$ext}";
//	if (is_file($savexmlfilename)) {
//		include( $savexmlfilename );
//	}
	// ワークフロー情報取得
	$arr_wkfw_template_history = $obj->get_wkfw_template_history($wkfw_id, $wkfw_history_no);
	$wkfw_content = $arr_wkfw_template_history["wkfw_content"];

	$wkfw_content_xml = ereg_replace("^.*<\? // XML", "<? // XML", $wkfw_content);
	$fp = fopen($savexmlfilename, "w");
	if (!$fp) {
		echo("<script language='javascript'>alert('一時ファイルがオープンできません。再度、更新してください。$savexmlfilename');</script>");
		echo("<script language='javascript'>history.back();</script>");
	}
	if(!fwrite($fp, $wkfw_content_xml, 2000000)) {
		fclose($fp);
		echo("<script language='javascript'>alert('一時ファイルに書込みできません。再度、更新してください。');</script>");
		echo("<script language='javascript'>history.back();</script>");
	}
	fclose($fp);

	include( $savexmlfilename );

}


$obj = new cas_application_workflow_common_class($con, $fname);

// トランザクションを開始
pg_query($con, "begin");

$arr_apply_wkfwmst = $obj->get_apply_wkfwmst($apply_id);
$wkfw_id           = $arr_apply_wkfwmst[0]["wkfw_id"];
$wkfw_history_no   = $arr_apply_wkfwmst[0]["wkfw_history_no"];
$wkfw_content_type = $arr_apply_wkfwmst[0]["wkfw_content_type"];
$short_wkfw_name   = $arr_apply_wkfwmst[0]["short_wkfw_name"];


//テンプレートの場合XML形式のテキスト$contentを作成
if ($wkfw_content_type == "2") { 
	$ext = ".php";
	$savexmlfilename = "cas_workflow/tmp/{$session}_x{$ext}";

	$arr_wkfw_template_history = $obj->get_wkfw_template_history($wkfw_id, $wkfw_history_no);
	$wkfw_content = $arr_wkfw_template_history["wkfw_content"];

	$wkfw_content_xml = ereg_replace("^.*<\? // XML", "<? // XML", $wkfw_content);
	$fp = fopen($savexmlfilename, "w");
	if (!$fp) {
		echo("<script language='javascript'>alert('一時ファイルがオープンできません。再度、更新してください。$savexmlfilename');</script>");
		echo("<script language='javascript'>history.back();</script>");
	}
	if(!fwrite($fp, $wkfw_content_xml, 2000000)) {
		fclose($fp);
		echo("<script language='javascript'>alert('一時ファイルに書込みできません。再度、更新してください。');</script>");
		echo("<script language='javascript'>history.back();</script>");
	}
	fclose($fp);

	include( $savexmlfilename );

    // 病棟評価申請の場合
    if($obj->is_ward_template($short_wkfw_name))
    {
		$arr = array(
                     "apply_id" => $apply_id,
                     "apv_order" => $apv_order,
                     "apv_sub_order" => $apv_sub_order,
                     "eval_content" => $content
	                 );
    	$obj->update_eval_content($arr);

        if($approve_mode != "DRAFT")
        {
			// 申請内容更新
			$obj->update_apply_content($apply_id, $content);
        }
    }
    // 評価申請の場合
    else if($obj->is_eval_template($short_wkfw_name))
    {
        // 承認階層が１階層目の承認者のみ評価可能
		// 評価申請レベル�供�No1,No3,No10、レベル�掘�No2,No4,No11で2階層目の場合、更新可能 20090501
        if($apv_order == "1" || ($apv_order == "2" && in_array($short_wkfw_name, array("c201","c202","c207","c209","c210","c215","c218","c221"))))
        {
        	$tmp_apv_sub_order = "";
        	if ($apv_order == "1") {
        		$tmp_apv_sub_order = $apv_sub_order;
        	} else {
        		if ($short_wkfw_name == "c201" || $short_wkfw_name == "c209") {
                	$tmp_apv_sub_order = $obj->get_send_apv_sub_order($apply_id, $send_apved_order_fix);
                }
        	}
			$arr = array(
	                      "apply_id" => $apply_id,
	                      "apv_order" => "1", //$apv_order,
	                      "apv_sub_order" => $tmp_apv_sub_order,
	                      "eval_content" => $content
	                     );
            $obj->update_eval_content($arr);
        }
    }
    // プリセプターチュータ制度評価の場合
    else if($short_wkfw_name == "c217")
    {
        $arr = array(
	                 "apply_id" => $apply_id,
	                 "apv_order" => $apv_order,
	                 "apv_sub_order" => $apv_sub_order,
	                 "eval_content" => $content
	                 );
        $obj->update_eval_content($arr);
        if($approve_mode != "DRAFT")
        {
			// 申請内容更新
			$obj->update_apply_content($apply_id, $content);
        }
    }
    else
    {
		// 申請内容更新
		$obj->update_apply_content($apply_id, $content);
    }
}

if($approve_mode == "DRAFT" || $approve_mode == "UPDATE")
{
    $draft_flg = "f";
    if($approve_mode == "DRAFT")
    {
        $draft_flg = "t";
    }
    $arr = array(
                 "apply_id" => $apply_id,
                 "apv_order" => $apv_order,
                 "apv_sub_order" => $apv_sub_order,
                 "apv_comment" => $apv_comment,
                 "draft_flg" => $draft_flg
	             );
    $obj->update_draft_flg($arr);
}
else if($approve_mode == "REGIST")
{
    // 承認処理
	// 2012/04/09 Yamagawa upd(s)
    //$obj->approve_application($apply_id, $wkfw_appr, $apv_order, $apv_sub_order, $approve, $apv_comment, $next_notice_div, $session, "DETAIL", $_POST, $short_wkfw_name);
	$obj->approve_application($apply_id, $wkfw_appr, $apv_order, $apv_sub_order, $hdn_approve, $apv_comment, $next_notice_div, $session, "DETAIL", $_POST, $short_wkfw_name);
	// 2012/04/09 Yamagawa upd(e)
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を切断
pg_close($con);

// 一覧画面に遷移
echo("<script language=\"javascript\">if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}</script>");
echo("<script language=\"javascript\">window.close();</script>\n");




?>
