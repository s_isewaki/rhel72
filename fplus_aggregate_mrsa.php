<?
ob_start();
require_once("about_comedix.php");
require_once("yui_calendar_util.ini");
require_once("fplus_common.ini");
ob_end_clean();

$fname = $PHP_SELF;
$session = qualify_session($session,$fname);
$fplusadm_auth = check_authority($session, 79, $fname);
$con = @connect2db($fname);
if(!$session || !$fplusadm_auth || !$con){
  echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
  exit;
}

$font = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';

//====================================================================
// 汎用マスタを取得する。プログラムからはエピシス項目名でアクセスできるようにする。
//====================================================================
fplus_common_maerge_general_master_to_episys_master("A401", "data_3_1_job");
fplus_common_maerge_general_master_to_episys_master("A403", "data_4_1_arise_area");
fplus_common_maerge_general_master_to_episys_master("A404", "data_1_2_belong_dept");
fplus_common_maerge_general_master_to_episys_master("A405", "data_1_13_find_germ");
fplus_common_maerge_general_master_to_episys_master("A407", "data_4_3_diagnosis");
fplus_common_maerge_general_master_to_episys_master("A409", "data_3_2_hospital_dept");

//====================================================================
// パラメータ取得
//====================================================================

//------------------
// 縦列フィールドの定義
//------------------
$vert_field_ab_definision = 
array(
		"00"=> "",
		"data_1_6_depart"=> "診療科",
		"data_1_7_ward"=> "病棟",
		"data_1_9_base_disease"=> "基礎疾患",
		"data_1_13_find_germ"=> "検出菌/微生物名",
		"data_2_1_specimen"=> "検出検体",
		"data_4_2_infectious"=> "感染症/保菌状態",
		"data_4_3_diagnosis"=> "診断名",
		"data_4_5_root"=> "感染経路"
);

if($search_start == "t")
{
	//検索開始
	
	// 指定された開始月
	$sel_d3_m = (int)@$_REQUEST["sel_d3_m"];
        $sel_d4_y = (int)@$_REQUEST["sel_d4_y"];
	$www = $mrsa_sel_d1_y;
	
        $header_month = array();
        $sel_d3_ms = $sel_d3_m;
        for($i=0;$i<=11;$i++){
            $header_month[$i]=$sel_d3_ms;
            $sel_d3_ms++;
            if($sel_d3_ms>12){
                $sel_d3_ms=$sel_d3_ms-12;
            }
        }
	
	// 指定された縦軸項目のインデックス
	//$sel_vert_field_idx = @$_REQUEST["sel_vert_field_idx"]; // 選択中の縦軸項目インデックス番号(0〜)
	$idx = $sel_vert_field_idx; // 選択中の縦軸項目インデックス番号(0〜)
	
	if($sel_vert_field_idx == "data_1_13_find_germ")
	{
		//縦軸の条件が"検出菌/微生物名"の場合は絞込み条件の"検出菌/微生物名"を検索条件にしない
		$germ_cond = "";
	}
	else
	{
		$germ_cond = "and ((data_1_13_find_germ = '$data_1_13_find_germ') or (data_1_15_find_germ = '$data_1_13_find_germ') or (data_1_16_find_germ = '$data_1_13_find_germ') or (data_1_17_find_germ = '$data_1_13_find_germ') or (data_1_18_find_germ = '$data_1_13_find_germ')) ";
	}
        
        if ($sel_d3_y)        $month_cond = " (data_1_14_pick_day >= '".sprintf("%04d/%02d/%02d",$sel_d3_y,$sel_d3_m,$sel_d3_d)."'"; // 開始日
        if ($sel_d4_y){        
            $month_cond .= " and data_1_14_pick_day <= '".sprintf("%04d/%02d/%02d",$sel_d4_y,$sel_d4_m,$sel_d4_d)."')  "; // 終了日
	}else{
            $month_cond .= " and data_1_14_pick_day < '".sprintf("%04d/%02d/%02d",$sel_d3_y+1,$sel_d3_m,$sel_d4_d)."')  "; // 終了日
        }
	/*$month_cond = array();
	
	$month_cond[0] = "data_1_14_pick_day >= '$mrsa_sel_d1_y/04/01' and data_1_14_pick_day <= '$mrsa_sel_d1_y/04/30' ";
	$month_cond[1] = "data_1_14_pick_day >= '$mrsa_sel_d1_y/05/01' and data_1_14_pick_day <= '$mrsa_sel_d1_y/05/31' ";
	$month_cond[2] = "data_1_14_pick_day >= '$mrsa_sel_d1_y/06/01' and data_1_14_pick_day <= '$mrsa_sel_d1_y/06/30' ";
	$month_cond[3] = "data_1_14_pick_day >= '$mrsa_sel_d1_y/07/01' and data_1_14_pick_day <= '$mrsa_sel_d1_y/07/31' ";
	$month_cond[4] = "data_1_14_pick_day >= '$mrsa_sel_d1_y/08/01' and data_1_14_pick_day <= '$mrsa_sel_d1_y/08/31' ";
	$month_cond[5] = "data_1_14_pick_day >= '$mrsa_sel_d1_y/09/01' and data_1_14_pick_day <= '$mrsa_sel_d1_y/09/30' ";
	$month_cond[6] = "data_1_14_pick_day >= '$mrsa_sel_d1_y/10/01' and data_1_14_pick_day <= '$mrsa_sel_d1_y/10/31' ";
	$month_cond[7] = "data_1_14_pick_day >= '$mrsa_sel_d1_y/11/01' and data_1_14_pick_day <= '$mrsa_sel_d1_y/11/30' ";
	$month_cond[8] = "data_1_14_pick_day >= '$mrsa_sel_d1_y/12/01' and data_1_14_pick_day <= '$mrsa_sel_d1_y/12/31' ";
	$month_cond[9] = "data_1_14_pick_day >= '".($mrsa_sel_d1_y+1)."/01/01' and data_1_14_pick_day <= '".($mrsa_sel_d1_y+1)."/01/31' ";
	$month_cond[10] = "data_1_14_pick_day >= '".($mrsa_sel_d1_y+1)."/02/01' and data_1_14_pick_day <= '".($mrsa_sel_d1_y+1)."/02/29' ";
	$month_cond[11] = "data_1_14_pick_day >= '".($mrsa_sel_d1_y+1)."/03/01' and data_1_14_pick_day <= '".($mrsa_sel_d1_y+1)."/03/31' ";
	*/
	$datalist = array();
	
	//for ($i = 0; $i <= 11; $i++){
		$sql = "select ".$idx.",data_1_14_pick_day from fplus_infection_mrsa 
				inner join fplusapply on (
				fplusapply.apply_id = fplus_infection_mrsa.apply_id
				and fplusapply.delete_flg = 'f'
				and fplusapply.apply_stat in ('0','1')
				and fplusapply.draft_flg = 'f'
				)
				where ".$month_cond . $germ_cond;

		$sel = @select_from_table($con, $sql, "", $fname);
		$row = @pg_fetch_all($sel);

		//表示しやすいように取得データから配列を加工しました
		for($i = 0; $i <= 11; $i++){
                $datalist[$i] = "";
		}
                foreach($row as $val )
		{
                    $split_day = split("/",$val["data_1_14_pick_day"]);
                    $i = $split_day["1"];
                    $i = ltrim($i, "0");
                    
                    $fin = 12-$sel_d3_m+$i;
                    if($fin>12){
                    $fin =$fin-12;
                    }
                    $datalist[$fin][$val[$idx]] ++;
		}
	//}
}


//====================================================================
// HTMLヘッダ
//====================================================================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ファントルくん＋ | 集計表 | <?=$epi_title?></title>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list1 {border-collapse:collapse;}
.list1 td {border:#E6B3D4 solid 1px; padding:3px; }
.list2 {border-collapse:collapse;}
.list2 td {border:#dfd95e solid 1px; padding:3px; }
</style>


<? //-------------------------------------------------------------------------?>
<? // JavaScript                                                              ?>
<? //-------------------------------------------------------------------------?>
<script language="javascript">
<!--
function startSearch()
{

	/*if(document.frm.mrsa_sel_d1_y.value == "")
	{
		alert("年度を指定してください");
		return false;
	}*/

	if(document.frm.sel_vert_field_idx.value == "00")
	{
		alert("種別を指定してください");
		return false;
	}

	if((document.frm.sel_vert_field_idx.value != "data_1_13_find_germ") && (document.frm.data_1_13_find_germ.value == ""))
	{
		alert("検出菌名/微生物名を指定してください");
		return false;
	}

	document.frm.search_start.value = "t";

	frmForm = document.getElementById("frm");
	frmForm.action = 'fplus_aggregate_mrsa.php';
	frmForm.method = 'post';
	frmForm.submit();

	return false;
}

function clickDownLoad() 
{
    frmForm = document.getElementById("frm");
    frmForm.action = 'fplus_aggregate_mrsa_excel.php';
    frmForm.method = 'post';
    frmForm.submit();
}

function clickDownLoad2() 
{
    frmForm = document.getElementById("frm");
    frmForm.action = 'fplus_aggregate_mrsa_excel2.php';
    frmForm.method = 'post';
    frmForm.submit();
}

//-->

</script>
<? // カレンダ ?>
<? write_yui_calendar_use_file_read_0_12_2(); ?>
<? write_yui_calendar_script2(2); ?>

</head>
<body style="margin:0" onload="initcal();">
<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>

<? //-------------------------------------------------------------------------?>
<? // ぱんくず、管理画面へのリンク                                            ?>
<? //-------------------------------------------------------------------------?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr bgcolor="#f6f9ff">
    <td width="32" height="32" class="spacing">
      <a href="fplus_menu.php?session=<? echo($session); ?>"><img src="img/icon/b47.gif" width="32" height="32" border="0" alt="ファントルくん＋"></a>
    </td>
    <td>
      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="fplus_menu.php?session=<? echo($session); ?>"><b>ファントルくん＋</b></a> &gt; <a href="fplus_aggregate.php?session=<? echo($session); ?>&epi_type=<?=$epi_type?>"><b>集計表 - 耐菌性報告</b></a></font>
    </td>
    <td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="fplusadm_menu.php?session=<? echo($session); ?>&workflow=1"><b>管理画面へ</b></a></font>
    </td>
  </tr>
</table>


<? //-------------------------------------------------------------------------?>
<? // メインメニュータブと、ボトムマージン描画                                ?>
<? //-------------------------------------------------------------------------?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr><? show_applycation_menuitem($session,$fname,""); ?></tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr><td bgcolor="#e6b3d4"><img src="img/spacer.gif" width="1" height="2" alt=""></td></tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>



<? //-------------------------------------------------------------------------?>
<? // 入力フォーム描画開始                                                    ?>
<? //-------------------------------------------------------------------------?>
<form name="frm" id="frm" action="fplus_aggregate_mrsa.php" method="post" enctype="multipart/form-data">
  <input type="hidden" name="session" value="<? echo($session); ?>">
  <input type="hidden" name="search_start">


	<? //-------------------------------------------------------------------------?>
	<? // 検索条件                                                                ?>
	<? //-------------------------------------------------------------------------?>
	<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list1">
		<tr>
			<td bgcolor="#feeae9" colspan="2"><?=$font?>報告書一覧出力</font></td>
		</tr>
		<tr>
			<td bgcolor="#feeae9"><?=$font?>期間</font></td>
			<td bgcolor="#fff8f7">
			<? // ************* 期間（日付From） ドロップダウン ******************** ?>
			<div style="float:left">
			  <select id="date_y1" name="sel_d1_y"><option></option>
	  			<? for($i=date("Y"); $i>=2000; $i--) { ?><option value="<?=$i?>" <?= ($i==$sel_d1_y ? 'selected="selected"': "") ?>><?=$i?></option><? } ?>
			  </select>
			  <select id="date_m1" name="sel_d1_m"><option></option>
	  			<? for($i=1; $i<=12; $i++) { ?><option value="<?=$i?>" <?= ($i==$sel_d1_m ? 'selected="selected"': "") ?>><?=$i?></option><? } ?>
			  </select>
			  <select id="date_d1" name="sel_d1_d"><option></option>
	  			<? for($i=1; $i<=31; $i++) { ?><option value="<?=$i?>" <?= ($i==$sel_d1_d ? 'selected="selected"': "") ?>><?=$i?></option><? } ?>
			  </select>
			</div>
			<? // ************* 日付From のカレンダボタン ******************** ?>
		  <div style="float:left; padding:0px 2px"><?=$font?>
  			<img src="img/calendar_link.gif" style="left:3px;z-index:1;cursor:pointer;" onclick="show_cal1();"/>
				<div id="cal1Container" style="position:absolute;display:none;z-index:10000;"></div></font>
			</div>
			<div style="float:left">
		  〜
		  </div>
			<? // ************* 期間（日付To） ドロップダウン ******************** ?>
			<div style="float:left">
			  <select id="date_y2" name="sel_d2_y"><option></option>
	  			<? for($i=date("Y"); $i>=2000; $i--) { ?><option value="<?=$i?>" <?= ($i==$sel_d2_y ? 'selected="selected"': "") ?>><?=$i?></option><? } ?>
			  </select>
			  <select id="date_m2" name="sel_d2_m"><option></option>
	  			<? for($i=1; $i<=12; $i++) { ?><option value="<?=$i?>" <?= ($i==$sel_d2_m ? 'selected="selected"': "") ?>><?=$i?></option><? } ?>
			  </select>
			  <select id="date_d2" name="sel_d2_d"><option></option>
	  			<? for($i=1; $i<=31; $i++) { ?><option value="<?=$i?>" <?= ($i==$sel_d2_d ? 'selected="selected"': "") ?>><?=$i?></option><? } ?>
			  </select>
			</div>
			<? // ************* 日付Toのカレンダボタン ******************** ?>
		  <div style="float:left; padding:0px 2px"><?=$font?>
  			<img src="img/calendar_link.gif" style="left:3px;z-index:1;cursor:pointer;" onclick="show_cal2();"/>
				<div id="cal2Container" style="position:absolute;display:none;z-index:10000;"></div></font>
			</div>
			<input type="button" onclick="clickDownLoad2();" value="エクセル出力" />
			<div style="clear:both"></div>
			</td>
		</tr>
	</table>


	<img src="img/spacer.gif" width="1" height="15" alt=""><br>




<? //-------------------------------------------------------------------------?>
<? // 検索条件                                                                ?>
<? //-------------------------------------------------------------------------?>
<table border="0" cellspacing="0" cellpadding="2" class="list1">
<tr>
	<!--<td bgcolor="#feeae9"><?=$font?>指定年度</font></td>
	<td bgcolor="#fff8f7">
		<? // ************* 期間（日付From） ドロップダウン ******************** ?>
		<div style="float:left">
		  <select id="mrsa_sel_d1_y" name="mrsa_sel_d1_y"><option></option>
	  		<? for($i=date("Y"); $i>=2000; $i--) { ?>
				<option value="<?=$i?>" <?= ($i==$mrsa_sel_d1_y ? 'selected="selected"': "") ?>><?=$i?></option>
			<? } ?>
		  </select>
	</td>
        
        
        -->
        
        <td bgcolor="#feeae9"><?=$font?>期間</font></td>
	<td bgcolor="#fff8f7">
	<? // ************* 期間（日付From） ドロップダウン ******************** ?>
	<div style="float:left">
	  <select id="date_y3" name="sel_d3_y"><option></option>
	  	<? for($i=date("Y"); $i>=2000; $i--) { ?><option value="<?=$i?>" <?= ($i==$sel_d3_y ? 'selected="selected"': "") ?>><?=$i?></option><? } ?>
	  </select>
	  <select id="date_m3" name="sel_d3_m"><option></option>
	  	<? for($i=1; $i<=12; $i++) { ?><option value="<?=$i?>" <?= ($i==$sel_d3_m ? 'selected="selected"': "") ?>><?=$i?></option><? } ?>
	  </select>
	  <select id="date_d3" name="sel_d3_d"><option></option>
	  	<? for($i=1; $i<=31; $i++) { ?><option value="<?=$i?>" <?= ($i==$sel_d3_d ? 'selected="selected"': "") ?>><?=$i?></option><? } ?>
	  </select>
	</div>
	<? // ************* 日付From のカレンダボタン ******************** ?>
  <div style="float:left; padding:0px 2px"><?=$font?>
  	<img src="img/calendar_link.gif" style="left:3px;z-index:1;cursor:pointer;" onclick="show_cal1();"/>
		<div id="cal1Container" style="position:absolute;display:none;z-index:10000;"></div></font>
	</div>
	<div style="float:left">
  〜
  </div>
	<? // ************* 期間（日付To） ドロップダウン ******************** ?>
	<div style="float:left">
	  <select id="date_y4" name="sel_d4_y"><option></option>
	  	<? for($i=date("Y"); $i>=2000; $i--) { ?><option value="<?=$i?>" <?= ($i==$sel_d4_y ? 'selected="selected"': "") ?>><?=$i?></option><? } ?>
	  </select>
	  <select id="date_m4" name="sel_d4_m"><option></option>
	  	<? for($i=1; $i<=12; $i++) { ?><option value="<?=$i?>" <?= ($i==$sel_d4_m ? 'selected="selected"': "") ?>><?=$i?></option><? } ?>
	  </select>
	  <select id="date_d4" name="sel_d4_d"><option></option>
	  	<? for($i=1; $i<=31; $i++) { ?><option value="<?=$i?>" <?= ($i==$sel_d4_d ? 'selected="selected"': "") ?>><?=$i?></option><? } ?>
	  </select>
	</div>
	<? // ************* 日付Toのカレンダボタン ******************** ?>
  <div style="float:left; padding:0px 2px"><?=$font?>
  	<img src="img/calendar_link.gif" style="left:3px;z-index:1;cursor:pointer;" onclick="show_cal2();"/>
		<div id="cal2Container" style="position:absolute;display:none;z-index:10000;"></div></font>
	</div>
	<div style="clear:both"></div>
  </td>
        
               
        
	<? // ************* 所属部門ドロップダウン ******************** ?>

	<td bgcolor="#feeae9"><?=$font?>種別</font></td>
	<td bgcolor="#feeae9">
		<?=$font?>
			<select name="sel_vert_field_idx">
  				<? foreach ($vert_field_ab_definision as $idx => $row) {?>
					<option value="<?=$idx?>" <?= ($idx==$sel_vert_field_idx ? 'selected="selected"' : "") ?>><?=$row?></option>
  				<? } ?>
			</select>
		</font>
	</td>

	 <td bgcolor="#feeae9"><?=$font?>検出菌名/微生物名</font></td>
	<td>
		<font class="j12">
			<SELECT name="data_1_13_find_germ" id="data_1_13_find_germ">
				<option value=""></option>
				<?= fplus_common_get_episys_dropdown_option_html("data_1_13_find_germ", $data_1_13_find_germ, true) ?>
			</SELECT>
		</font>
	</td>
</tr>
</table>

		<? // ************* 検索と印刷（エクセル出力）ボタン ******************** ?>
		<div style="float:right;">
			<input type="button" onclick="startSearch();" value="検索"/>
			<input type="button" onclick="clickDownLoad();" value="エクセル出力" <?= !count($datalist) ? "disabled" : "" ?> />
		</div>
		<br style="clear:both"/>
		</font>
	</div>

</form>

</td></tr></table>


<? //-------------------------------------------------------------------------?>
<? // 一覧表                                                                  ?>
<? //-------------------------------------------------------------------------?>
<? if (!count($datalist)){ ?>
	<div style="color:#999; text-align:center; margin-top:20px"><?=$font?>該当データはありません。</font></div>

<? } else { ?>

<table width="100%" class="list1" style="<?=$tableWidth?>; background-color:#fff;">

	<? // 列ヘッダ行 ?>
	<tr style="background-color:#ffdfff;">
		<td align="right"> </td>
                <? for($i=0;$i<12;$i++){ ?>
                <td align="right"><?=$font?><?=$header_month[$i]?>月</font></td>
                <?} ?>
                <td align="right"><?=$font?>合計</font></td>
	</tr>
	<?
	if($sel_vert_field_idx == "data_1_6_depart")//"診療科"
	{$vert_data = fplus_common_get_episys_master_field_list("data_3_2_hospital_dept", "", true);}
	elseif($sel_vert_field_idx == "data_1_7_ward")//"病棟"
	{$vert_data = fplus_common_get_episys_master_field_list("data_1_2_belong_dept", "", true);}
	elseif($sel_vert_field_idx == "data_1_9_base_disease")//"基礎疾患"
	{$vert_data = fplus_common_get_001_master_field_list("data_1_11_base_disease", "", true);}
	elseif($sel_vert_field_idx == "data_1_13_find_germ")//"検出菌/微生物名"
	{$vert_data = fplus_common_get_episys_master_field_list("data_1_13_find_germ", "", true);}
	elseif($sel_vert_field_idx == "data_2_1_specimen")//"検出検体"
	{$vert_data = fplus_common_get_001_master_field_list("data_2_1_specimen", "", true);}
	elseif($sel_vert_field_idx == "data_4_2_infectious")//"感染症/保菌状態"
	{$vert_data = fplus_common_get_001_master_field_list("data_4_2_infectious", "", true);}
	elseif($sel_vert_field_idx == "data_4_3_diagnosis")//"診断名"
	{$vert_data = fplus_common_get_episys_master_field_list("data_4_3_diagnosis", "", true);}
	elseif($sel_vert_field_idx == "data_4_5_root")//"感染経路"
	{$vert_data = fplus_common_get_001_master_field_list("data_4_5_root", "", true);}
	

	$gokei_line = array();
	
//	foreach($vert_data as $val )
	for ($name_cnt = 0; $name_cnt <= count($vert_data); $name_cnt++)
	{
?>
	<tr>
<?
		if($name_cnt == count($vert_data))
		{
			//合計行を表示

?>
		<td style="background-color:#ffdfff;"><?=$font?>合計</font></td>
<?
			
			for ($gokei_cnt = 0; $gokei_cnt <= 12; $gokei_cnt++)
			{
?>
		<td align="right" style="background-color:#ffdfff;"><?=$font?><?=$gokei_line[$gokei_cnt]?></font></td>
<?
				
			}
			
		}
		else
		{
			//項目行を表示
			
			$gokei = 0;
?>
		<td style="background-color:#ffdfff;"><?=$font?><?=$vert_data[$name_cnt]["record_caption"]?></font></td>
<?
			for ($i = 0; $i <= 12; $i++)
			{
				//0:4月　1:5月　2:6月　3:7月　4:8月　5:9月　6:10月　7:11月　8:12月　9:1月　10:2月　11:3月　12:合計
				if($i == 12)
				{
					//合計行
?>
		<td align="right"><?=$font?><?=$gokei?></font></td>
<?
					$gokei_line[$i] = $gokei_line[$i] + $gokei;
				}
				else
				{
					//合計用足しこみ(横軸用)
?>
		<td align="right"><?=$font?><?=$datalist[$i][(string)$vert_data[$name_cnt]["record_id"]]?></font></td>
<?
					if($datalist[$i][(string)$vert_data[$name_cnt]["record_id"]] != "")
					{
						$gokei = $gokei + (int)$datalist[$i][(string)$vert_data[$name_cnt]["record_id"]];
						$gokei_line[$i] = $gokei_line[$i] + (int)$datalist[$i][(string)$vert_data[$name_cnt]["record_id"]];
					}
					
				}		
			}
		}
?>
	</tr>
<?
	}
?>
</table>

<? 
} 
?>

</body>
</html>
