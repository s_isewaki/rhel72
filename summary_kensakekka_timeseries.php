<?
ini_set("display_startup_errors", "On");
ini_set("display_errors", "On");

ob_start();
require("about_authority.php");
require("about_session.php");
require("about_postgres.php");
require("show_patient_summary_list.ini");
require("show_patient_next.ini");
require("show_select_values.ini");
require("get_values.ini");
require("conf/sql.inf");
require_once("summary_common.ini");
require_once("sot_util.php");
require_once("sum_exist_workflow_check_exec.ini");
ob_end_clean();
ob_start();
//ページ名
$fname = $PHP_SELF;
//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
  echo("<script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script>");
  ob_end_flush() ;
  exit;
}
// 権限チェック
$summary = check_authority($session, 57, $fname);
if ($summary == "0") {
  showLoginPage();
  ob_end_flush() ;
  exit;
}
//DBコネクション
$con = connect2db($fname);
if($con == "0"){
  echo("<script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script>");
  ob_end_flush() ;
  exit;
}

// 表示モードを取得する
$isTimeSeries = (array_key_exists("uke_date", $_REQUEST)) ? false : true ; // (true, false) = (時系列表示, 結果表示)
// コピー用呼び出時のコピー先コントロールID
$target_id = @$_REQUEST["target_id"] ;
$cursor  = ($target_id) ? "hand" : "move" ;
$lineClr = ($target_id) ? "onmouseover='changeBgColor(this,true);' onmouseout='changeBgColor(this,false);'" : "" ;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix メドレポート｜検査結果表示</title>
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<style type="text/css">
.kensakekka_table    { width:100%; border-collapse:separate; border-spacing:1px; background-color:#9bc8ec; empty-cells:show; }
.kensakekka_table tr { background-color:#fff; }
.kensakekka_table th { padding:3px; vertical-align:middle; font-weight:normal; background-color:#eff9fd; }
.kensakekka_table td { padding:3px; vertical-align:middle; }
.ord_row             { cursor:<?=$cursor?>; }
</style>
<script type="text/javascript">
var sr=null;
var er=null;
function md(ob)
{
  sr=ob.parentNode.rowIndex;
}
function mu(ob)
{
  er=ob.parentNode.rowIndex;
<? if ($target_id) { ?>
  var tbl=document.getElementById("ktbl");
  var cels=tbl.rows[er].cells;
  var itm=cels[0].innerHTML.split("：");
<?//  var cptxt=itm[1]+" "+cels[2].innerHTML+" "+cels[3].innerHTML; ?>
  var cptxt=itm[1]+" "+cels[3].innerHTML+" ";
  window.opener.call_back_kensakekka_select('<?=$target_id?>', cptxt);
<? } else { ?>
  if((sr!=null)&&(sr!=er))
  {
    rc();
    sr=null;
  }
<? } ?>
}
function rc()
{
  var tbl=document.getElementById("ktbl");
  var cels=tbl.rows[sr].cells;
  var ntr=tbl.insertRow(er);
  for(var i=0;i<cels.length;i++)
  {
    var ntd = ntr.insertCell(i);
    ntd.innerHTML = cels[i].innerHTML;
    if(i==0)
    {
      ntd.setAttribute("class","ord_row");
      ntd.setAttribute("onmousedown","md(this);");
      ntd.setAttribute("onmouseup","mu(this);");
      ntd.setAttribute("onselectstart","return false;");
      ntd.setAttribute("unselectable","on");
      ntd.setAttribute("style","padding:3px;vertical-align:middle;text-align:left;font-weight:normal;background-color:#eff9fd;-moz-user-select:none;-khtml-user-select:none;-webkit-user-select:none;");
    }
    else if(i==1)
    {
      ntd.setAttribute("style","display:none;");
    }
    else
    {
      var styl="padding:3px;vertical-align:middle;text-align:";
      if((i%2)==0) {styl+="right;" ;}
      else         {styl+="center;";}
      ntd.setAttribute("style",styl);
    }
  }
  if(sr>er) sr++;
  tbl.deleteRow(sr);
  so();
}
function so()
{
  var rws=document.getElementById("ktbl").rows;
  var ord="";
  for(var j=2;j<rws.length;j++)
  {
    var ino=rws[j].cells[1].innerHTML;
    ord+=(ino+",");
  }
  parent.document.getElementById("itm_ord").value=ord;
}

function doinit()
{
  so();
}

	// この子画面の現在の高さを求める
	function getCurrentHeight(){
		if (!window.parent) return 0;
		var h = (document.documentElement.clientHeight || window.innerHeight || document.body.clientHeight);
		h = Math.max(h, document.body.scrollHeight);
		h = Math.max(h, 300);
		return (h*1+50);
	}
	// この子画面の最初の高さを親画面(summary_rireki.php)に通知しておく。onloadで呼ばれる。
	// 一旦サイズを小さくしたのち、正しい値を再通知するようにしないといけない。
	function setDefaultHeight(){
		if (!window.parent) return;
		window.parent.setDefaultHeight("hidari", 300);
		resetIframeHeight();
		var h = getCurrentHeight();
		if (h) window.parent.setDefaultHeight("hidari",h);
	}
	// この子画面がリサイズされたら、高さを再測定して親画面(summary_rireki.php)に通知する。
	// 「サマリ内容ポップアップ」が開くときも呼ばれる。onloadでも呼ばれる。
	// 親画面はこの子画面のIFRAMEサイズを変更する。
	// 一旦大きくしたIFRAMEのサイズは通常小さくできない。
	// よって、getCurrentHeightでサイズ測定する前に、画面を一旦元に戻さないといけない。
	function resizeIframeHeight(){
		resetIframeHeight();
		var h = getCurrentHeight();
		if (h) window.parent.resizeIframeHeight("hidari", h);
	}
	// この子画面の高さを、最初の高さに戻す。「サマリ内容ポップアップ」が閉じるとき呼ばれる。
	// 親画面(summary_rireki.php)は、この子画面IFRAMEの高さを最初の値に戻す。
	function resetIframeHeight(){
		if (!window.parent) return;
		window.parent.resetIframeHeight("hidari");
	}

var celcs=new Array();
function changeBgColor(obj,on)
{
  var cels=obj.cells;
  if(on)
  {
    for(var i=0;i<cels.length;i++)
    {
      celcs[i]=cels[i].style.backgroundColor;
      cels[i].style.backgroundColor="#fefc8f";
    }
  }
  else
  {
    for(var i=0;i<cels.length;i++)
    {
      cels[i].style.backgroundColor=celcs[i];
    }
  }
}

function allCopy(lf)
{
//  var br=(window.opener.tinyMCE.isOpera||window.opener.tinyMCE.isSafari)?"\n":"<br />";	//20120220
  var br=(window.opener.tinyMCE.isOpera)?"\n":"<br />";
  var le=(lf)?br:" ";
  var rws=document.getElementById("ktbl").rows;
  var cpall="";
  for(var i=2;i<rws.length;i++)
  {
    var val=rws[i].cells[3].innerHTML;
    if((val.length>0)&&(val!="検査中"))
    {
      var itm=rws[i].cells[0].innerHTML.split("：");
      cpall+=(itm[1]+" "+val+le);
    }
  }
  window.opener.call_back_kensakekka_select('<?=$target_id?>', cpall);
}
//PDF印刷
function startPrint()
{
  document.pdf.submit();
}

</script>
</head>
<?
//===================================
// 当該患者の検査結果データを取得する
//===================================
// 項目番号による固定の並び順
$fixedCodes = array(
    "0001 ","0058 ","0002 ","0051 ","0052 ","0007 ","0008 ","0009 ","0010 ","0011 ","0012 ","0013 ","0014 ","0015 ","0016 ","0027 ","0028 ","0030 ","0054 ","2632 ",
    "0029 ","0034 ","0036 ","0037 ","0038 ","0040 ","0039 ","0041 ","0042 ","0043 ","0045 ","0046 ","0047 ","0490 ","0162 ","1119 ","0020 ","0021 ","0022 ","3011 ",
    "3012 ","3013 ","3014 ","3015 ","3016 ","0402 ","0401 ","0403 ","0404 ","0405 ","0406 ","0407 ","0408 ","0414 ","3228 ","3229 ","3201 ","3202 ","3203 ","3204 ",
    "3205 ","3206 ","3212 ","3207 ","3208 ","3209 ","3210 ","3211 ","3221 ","3222 ","3223 ","3224 ","0511 ","0512 ","0513 ","0515 ","0516 ","0517 ","0518 ","0519 ",
    "1235 ","1236 ","1237 ","3711 ","3712 ","3713 ","3735 ","3717 ","3721 ","3720 ","3718 ","3739 ","3715 ","3783 ","3747 ","3744 ","3754 ","3755 ","3756 ","3757 ",
    "3728 ","3731 ","3732 ","3733 ","3758 ","3729 ","3730 ","3716 ","3719 ","3722 ","3723 ","3724 ","3725 ","3726 ","3748 ","3749 ","3750 ","3727 ","3751 ","3735 ",
    "3736 ","3737 ","3738 ","3742 ","3745 "
    ) ;

//$uketukeCond = ($isTimeSeries) ? "" : " and ken.hdr_uketuke_date = '".$_REQUEST["uke_date"]."' and ken.hdr_uketuke_no = '".$_REQUEST["uke_no"]."'" ;
$sql =
  " select" .
  "   ken.hdr_uketuke_date" .
  "  ,ken.hdr_uketuke_no" .
  "  ,ken.ptr_name_kana" .
  //  "  ,ken.ptr_karte_no" .
  "  ,ken.ptr_age_yyy" .
  "  ,ken.ptr_sex" .
  "  ,ken.knr_sinryou_sect" .
  "  ,ken.knr_nyugai_kbn" .
  "  ,ken.knr_byoutou_1" .
  "  ,ken.knr_byoutou_2" .
  "  ,ken.knr_tantou_doc" .
  "  ,ken.knr_saiketu_date" .
  "  ,ken.knr_hospital_cd" .
  "  ,ken.itm_item_sno" .
  "  ,ken.itm_item_no" .
  "  ,ken.itm_item_name" .
  "  ,ken.itm_ijouti_kbn" .
  "  ,ken.itm_kensa_kekka" .
  "  ,ken.itm_seijou_low_m" .
  "  ,ken.itm_seijou_high_m" .
  "  ,ken.itm_seijou_low_f" .
  "  ,ken.itm_seijou_high_f" .
  "  ,ken.itm_tani" .
  "  ,ken.itm_ryouritu_kbn" .
  "  ,ken.itm_hoken_pt" .
  "  ,ken.itm_kekka_cmt_1" .
  "  ,ken.itm_kekka_cmt_2" .
  "  ,ken.ptif_id" .
  "  ,pts.ptsubif_dialysis" .
  " from" .
  "   sum_kensakekka ken" .
  "   left join ptsubif pts on (pts.ptif_id = ken.ptif_id)" .
  " where" .
  "       ken.hdr_update_kbn <> '9'" .
  "   and ken.ptif_id = '" . $pt_id . "'" .
//  $uketukeCond .
  " order by" .
  "   ken.hdr_uketuke_date desc" .
  "  ,ken.hdr_uketuke_no desc" .
  "  ,ken.itm_sort_no" .
  "  ,ken.itm_sort_sno" .
  "  ,ken.itm_item_sno" .
  "  ,ken.itm_item_no" ;

$sel = select_from_table($con, $sql, "" , $fname);

$allDatas   = array() ;
$itemInfos0 = array() ;
$uketukeKey_ = "" ;
$idx = -1 ;
while ($row = pg_fetch_array($sel))
{
  $uketukeKey = $row["hdr_uketuke_date"] . "_" . $row["hdr_uketuke_no"] ; // 受付日_受付番号
  $itemKey    = $row["itm_item_sno"    ] . "_" . $row["itm_item_no"   ] ; // 枝番_項目番号
  if ($uketukeKey != $uketukeKey_)
  {
    $idx++ ;
    $allDatas[$idx] = array() ;
    $allDatas[$idx][] = $uketukeKey ;
    $allDatas[$idx][] = array() ;
    $uketukeKey_ = $uketukeKey ;
  }
  // 検査データを配列に格納する
  $allDatas[$idx][1][$itemKey] = $row ;
  // 検査項目を配列に格納する
  $itemInfos0[$itemKey] = $row["itm_item_name"] ;
}
// 検査項目配列をキーで昇順ソートする
//ksort($itemInfos) ;
// 検査項目配列を既定のソート順で組み直す
$itemInfos1 = array() ; // 固定並び順
$itemInfos2 = array() ; // 並び順−親子
foreach ($fixedCodes as $idx => $code)
{
  $code_ = "_" . $code ;
  foreach ($itemInfos0 as $key => $val)
  {
    if (strpos($key, $code_) > -1)
    {
      $itemInfos1[$key] = $val ;
    }
    else
    {
      $itemInfos2[$key] = $val ;
    }
  }
}
$itemInfos = $itemInfos1 + $itemInfos2 ;
$sexs    = array("m"=>"男",   "f"=>"女"  ) ;
$nyugais = array("1"=>"外来", "2"=>"入院") ;



  // この受付日_受付番号に対応する検査データを取得する
  $recs = null ;
  $rec1 = null ;
  foreach ($allDatas as $idx => $kensaDatas) // 検査データのループ
  {
    $recs = $kensaDatas[1] ;
    foreach ($recs as $key => $kensaData) { $rec1 = $kensaData ; break ; } // 最初のデータだけを取得する
  }
  $nyugai  = @$nyugais[$rec1["knr_nyugai_kbn"]] ;
  $byoutou = trim($rec1["knr_byoutou_1"]) . " " . trim($rec1["knr_byoutou_2"]) ;


//===================================
// 検査結果時系列表示  ■ここ！
//===================================
if ($isTimeSeries) // 検査結果時系列表示 if START
{
  if ($target_id) {
?>
<body style="background-color:#f4f4f4">
<?
  } else {
?>
<body style="background-color:#f4f4f4" onload="doinit();setDefaultHeight();resizeIframeHeight();">
<?
  }
?>
<script type="text/javascript">
function dosubmit(idx)
{
  document.frm.itm_ord.value=parent.document.getElementById("itm_ord").value;
  document.frm.s_idx.value=idx;
  document.frm.submit();
}
function popupKensaData(dt, no)
{
  window.open("summary_kensakekka_timeseries.php?session=<?=$session?>&pt_id=<?=$pt_id?>&uke_date="+dt+"&uke_no="+no,
    "", "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,width=900,height=700");
}

</script>

<? $colNum = 5 ; ?>
<? $allCopyBttn = "" ; ?>
<? if ($target_id) { ?>
<? $colNum = 1 ; ?>
<?= summary_common_show_dialog_header("検査結果"); ?>
<? $allCopyBttn = "<input type='button' value='全結果コピー' onclick='allCopy(false);' />" ?>
<? } else { ?>
	<div style="float:right"><input type="button" value=" PDF印刷 " onclick="startPrint();" <?=count($itemInfos) <= 0 ? disabled : '' ?> /></div>
<table border="0" cellspacing="0" cellpadding="0">
 <tr height="22">
  <td width="110" align="center" bgcolor="#63bafe"><a href="./summary_rireki_table.php?session=<?=$session?>&pt_id=<?=$pt_id?>" style="text-decoration:none;"><font size="2" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#0055dd">医療文書</font></a></td>
  <td width="5">&nbsp;</td>
  <td width="110" align="center" bgcolor="#156cec"><a href="#" onclick="dosubmit(0);" style="text-decoration:none;"><font size="2" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>検査時系列</b></font></a></td>
  <td width="5">&nbsp;</td>
  <td width="110" align="center" bgcolor="#63bafe"><a href="summary_disease_list.php?session=<?=$session?>&pt_id=<?=$pt_id?>" style="text-decoration:none;"><font size="2" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#0055dd">傷病名一覧</font></a></td>
	</tr>
</table>

<? } ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>
<td bgcolor="#156cec"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr></table>
<?
//===================================
// 後へ／前へのリンクを作成
//===================================
$sIdx = (int)$s_idx ;
$dLen = count($allDatas) ;
$is_show_next_link = ($sIdx >= $colNum         ) ? 1 : 0 ; // nextリンクを作成するかどうか
$is_show_prev_link = ($sIdx < ($dLen - $colNum)) ? 1 : 0 ; // prevリンクを作成するかどうか
if($is_show_next_link){
  $next_link = "<img src=\"img/left.gif\" alt=\"\" width=\"17\" height=\"17\" border=\"0\" onclick=\"dosubmit(".($sIdx - $colNum).")\" style=\"cursor:hand;\">" ;
}
if($is_show_prev_link){
  $prev_link = "<img src=\"img/right.gif\" alt=\"\" width=\"17\" height=\"17\" border=\"0\" onclick=\"dosubmit(".($sIdx + $colNum).")\" style=\"cursor:hand;\">" ;
}
?>
<table style="width:100%">
 <tr><td><?=@$next_link?></td><td style="text-align:right"><?=@$prev_link?></td></tr>
</table>

<form name="frm" action="summary_kensakekka_timeseries.php" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="pt_id" value="<?=$pt_id?>">
<input type="hidden" name="target_id" value="<?=$target_id?>">
<input type="hidden" name="s_idx" id="s_idx" value="">
<input type="hidden" style="width:100%;" name="itm_ord" id="itm_ord" value="">
</form>

<?
//===================================
// 一覧表描画
//===================================
// ヘッダ
?>
<table id="ktbl" class="kensakekka_table" cellspacing="0" cellpadding="2">
 <tr>
  <th rowspan="2"><?=$allCopyBttn?></th>
<?
for($i = $sIdx ; $i < ($sIdx + $colNum) ; $i++) // 受付日のループ
{
  $uketuke_strs = explode("_", $allDatas[$i][0]) ;
  $uketuke_date = str_replace("/", ".", $c_sot_util->slash_yyyymmdd($uketuke_strs[0])) ;
?>
  <th colspan="2"><a href="" onclick="popupKensaData('<?=$uketuke_strs[0]?>','<?=$uketuke_strs[1]?>'); return false;"><?=$uketuke_date?></th>
<?
}
?>
 </tr>
 <tr>
<?
for($i = 0 ; $i < $colNum ; $i++)
{
?>
  <th>LH</th><th>成績</th>
<?
}
?>
 </tr>
<?
  // 画面で項目入れ替えがあったときは、その順番で項目配列をソートする
  $itemInfos2 = null ;
  $itmNosCsv  = @$_REQUEST["itm_ord"] ;
  if ($itmNosCsv)
  {
    $itmNosCsv = rtrim($itmNosCsv, ",") ;
    $itmNos = explode(",", $itmNosCsv) ;
    $itemInfos2 = array() ;
    foreach ($itmNos as $idx => $itmNo)
    {
      $itemInfos2[$itmNo] = $itemInfos[$itmNo] ;
    }
  }
  if ($itemInfos2) $itemInfos = $itemInfos2 ;

// 各行
foreach ($itemInfos as $itemNo => $itemName) // 検査項目のループ
{
  $itemNos = explode("_", $itemNo) ;
//  $itemInfo = $itemNos[1] . "：" . $itemName ;
  $itemInfo = $itemNos[1] . "：" . rtrim($itemName) ;
?>
 <tr style="background-color:#fff;" <?=$lineClr?>>
  <td class="ord_row" onmousedown="md(this);" onmouseup="mu(this);" onselectstart="return false;" unselectable="on" style="padding:3px;vertical-align:middle;text-align:left;font-weight:normal;background-color:#eff9fd;-moz-user-select:none;-khtml-user-select:none;-webkit-user-select:none;"><?=$itemInfo?></td>
  <td style="display:none;"><?=$itemNo?></td>
<?
  for($i = $sIdx ; $i < ($sIdx + $colNum) ; $i++) // 受付日のループ
  {
    $kensa_kekka     = "" ;
    $kekka_jouge_kbn = "" ;
    $datas = @$allDatas[$i] ;
    if ($datas)
    {
      $recs = @$datas[1][$itemNo] ; // レコード配列
      if ($recs)
      {
        $kensa_kekka     = trim($recs["itm_kensa_kekka"]) ;
        $kekka_jouge_kbn =      $recs["itm_ijouti_kbn" ]  ;
        if (!$kensa_kekka) $kensa_kekka = "検査中" ;
      }
    }
?>
  <td style="padding:3px;text-align:center;vertical-align:middle;"><?=$kekka_jouge_kbn?></td><td style="padding:3px;text-align:right;vertical-align:middle;"><?=$kensa_kekka?></td>
<?
  }

?>
 </tr>
<?
}
?>
</table>
<?
} // 検査結果時系列表示 if END
//===================================
// 検査結果表示
//===================================
else // 検査結果表示 if START
{
  $uke_date    = $_REQUEST["uke_date"] ;     // 受付日
  $uke_no      = $_REQUEST["uke_no"  ] ;     // 受付番号
  $uke_date_no = $uke_date . "_" . $uke_no ; // 例：20110306_000001
  // この受付日_受付番号に対応する検査データを取得する
  $recs = null ;
  $rec1 = null ;
  foreach ($allDatas as $idx => $kensaDatas) // 検査データのループ
  {
    if ($kensaDatas[0] == $uke_date_no)
    {
      $recs = $kensaDatas[1] ;
      foreach ($recs as $key => $kensaData) { $rec1 = $kensaData ; break ; } // 最初のデータだけを取得する
    }
  }
  if (!$rec1) { pg_close($con) ; die ; }
  $dialysis = ($rec1["ptsubif_dialysis"] == "t") ? "あり" : "なし" ;
  // 上部検査情報表示
?>

<body style="background-color:#f4f4f4">

<div style="margin:6px;"></div>

<table class="kensakekka_table">
 <tr><td>
  <table width="100%">
   <tr>
    <td>
     <? $uketuke_date = $c_sot_util->kanji_ymd($uke_date) ; // 受付日 例：2011年3月14日 ?>
     採取日     <input type="text" class="kensa_info" style="width:100px;" value="<?=$uketuke_date?>"                  readonly>&nbsp;
     カルテ番号 <input type="text" class="kensa_info" style="width: 90px;" value="<?=trim($rec1["ptif_id"        ])?>" readonly>&nbsp;
     医院コード <input type="text" class="kensa_info" style="width: 20px;" value="<?=trim($rec1["knr_hospital_cd"])?>" readonly>
<!--                <input type="text" class="kensa_info" style="width:140px;" value=""                                    readonly>&nbsp;
     請求区分   <input type="text" class="kensa_info" style="width: 50px;" value=""                                    readonly> -->
    </td>
    <td style="text-align:right"><input type="button" value=" PDF印刷 " onclick="startPrint();" /></td>
   </tr>
   <tr>
    <td colspan="2">
     <? $age  = (int)$rec1["ptr_age_yyy"] ;
        $sex  = strtolower($rec1["ptr_sex"]) ;
        $sexk = @$sexs[strtolower($rec1["ptr_sex"])] ; ?>
     患者名     <input type="text" class="kensa_info" style="width:120px;" value="<?=trim($rec1["ptr_name_kana"])?>" readonly>&nbsp;
     年齢       <input type="text" class="kensa_info" style="width: 30px;" value="<?=$age?>"                         readonly>&nbsp;
     性別       <input type="text" class="kensa_info" style="width: 30px;" value="<?=$sexk?>"                        readonly>&nbsp;
<!--     処理レベル <input type="text" class="kensa_info" style="width: 80px;" value=""                                   readonly>&nbsp;
     原本ID     <input type="text" class="kensa_info" style="width: 50px;" value=""                                   readonly>&nbsp;
     原本受付日 <input type="text" class="kensa_info" style="width:100px;" value=""                                   readonly> -->
    </td>
   </tr>
   <tr>
    <td colspan="2">
     科名     <input type="text" class="kensa_info" style="width: 80px;" value="<?=trim($rec1["knr_sinryou_sect"])?>" readonly>&nbsp;
     入院区分 <input type="text" class="kensa_info" style="width: 40px;" value="<?=$nyugai?>"                         readonly>&nbsp;
     病棟     <input type="text" class="kensa_info" style="width:120px;" value="<?=$byoutou?>" readonly>&nbsp;
     主治医   <input type="text" class="kensa_info" style="width:100px;" value="<?=trim($rec1["knr_tantou_doc"  ])?>" readonly>&nbsp;
     透析     <input type="text" class="kensa_info" style="width: 30px;" value="<?=$dialysis?>"                       readonly>&nbsp;
<!--     検体名   <input type="text" class="kensa_info" style="width:120px;" value=""                                     readonly> -->
    </td>
   </tr>
  </table>
 </td><tr>
</table>
<div style="margin:6px;"></div>
<table class="kensakekka_table">
 <tr>
  <th>項目コード</th>
  <th>項目名</th>
  <th>LH</th>
  <th>成績</th>
  <th>再希</th>
  <th>状態</th>
  <th>基準値</th>
  <th>単位</th>
  <th>料率区分</th>
  <th>保険点数</th>
  <th>コメント1</th>
  <th>コメント2</th>
 </tr>
<?
  // 各行
  foreach ($recs as $codes => $rec) // 検査データのループ
  {
    $itemNo = explode("_", $codes) ;
    // 性別によって基準値を決定する
    $seijou_low  = "" ;
    $seijou_high = "" ;
    if (($sex == "m") || ($sex == "f"))
    {
      $seijou_low  = $rec["itm_seijou_low_"  . $sex] ;
      $seijou_high = $rec["itm_seijou_high_" . $sex] ;
    }
    $kijunti = $seijou_low . " - " . $seijou_high ;
    $kensa_kekka = trim($rec["itm_kensa_kekka"]) ;
    if (!$kensa_kekka) $kensa_kekka = "検査中" ;
?>
 <tr>
  <td><?=$itemNo[1]?></td>
  <td><?=$rec["itm_item_name"]?></td>
  <td style="text-align:center;"><?=$rec["itm_ijouti_kbn"]?></td>
  <td style="text-align:right;" ><?=$kensa_kekka?></td>
  <td></td>
  <td></td>
  <td><?=$kijunti?></td>
  <td style="text-align:center;"><?=$rec["itm_tani"        ]?></td>
  <td style="text-align:center;"><?=$rec["itm_ryouritu_kbn"]?></td>
  <td style="text-align:right;" ><?=$rec["itm_hoken_pt"    ]?></td>
  <td><?=$rec["itm_kekka_cmt_1"]?></td>
  <td><?=$rec["itm_kekka_cmt_2"]?></td>
 </tr>
<?
  }
} // 検査結果表示 if END
?>
<form name="pdf" action="summary_kensakekka_timeseries.php" method="post" target="_blank">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="pt_id" value="<?=$pt_id?>">
<? if(!$isTimeSeries){?><input type="hidden" name="uke_date" value="<?=$uke_date?>"><?}; ?>
<input type="hidden" name="uke_no" value="<?=$uke_no?>">
<input type="hidden" name="for_pdf" value="1">
<input type="hidden" name="sIdx" id="sIdx" value="<?=$sIdx?>">
</form>
</body>
</html>
<? pg_close($con); ?>
<?
if (!array_key_exists("for_pdf", $_REQUEST))
{
  ob_end_flush() ;
  exit;
}
require_once("fpdf153/mbfpdf.php");
ob_end_clean();
// ↓↓↓ PDF印刷 ↓↓↓
//exec_pdf_print() ;
//function exec_pdf_print()
//{
//
class CustomMBFPDF extends MBFPDF
{
  var $MOST_LEFT  =  15 ;
//  var $MOST_RIGHT = 195 ;
  var $MOST_TOP   =  15 ;
  var $MOST_BTM   = 279 ;

  var $THD1_CELL_W = array(45,  0) ;
  var $THD2_CELL_W = array(45, 65, 0) ;
  var $THD3_CELL_W = array(45, 65, 0) ;

  var $CHD1_CELL_W = array(46, 8, 20, 17, 26, 63) ;

  var $TFT_CELL_W = array(35, 35, 0) ;

  var $kensaInfos ;

  function CustomMBFPDF($kensaInfos_)
  {
    $this->MBFPDF("P", "mm", "A4") ; // PHPでは継承元クラスのコンストラクタは明示的に呼び出す必要がある
    $this->AddMBFont(GOTHIC, "EUC-JP") ;
    $this->SetFont(GOTHIC, "", 10) ;
    $this->SetMargins($this->MOST_LEFT, $this->MOST_TOP) ;
    $this->SetFillColor(210) ;
    $this->kensaInfos = $kensaInfos_ ;
  }

  function Header()
  {
    if ($this->PageNo() == 1) $this->writeTitleHeader() ;
    if(!$this->kensaInfos["isTimeSeries"]) $this->writeContsHeader() ;
  }

  function Footer()
  {
    $this->SetY($this->MOST_BTM) ;
    $this->Cell($this->TFT_CELL_W[0], 5.0, "患者ID：" . $this->kensaInfos[2]) ;
    if(!$this->kensaInfos["isTimeSeries"]) $this->Cell($this->TFT_CELL_W[1], 5.0, "受付ID：" . $this->kensaInfos[5]) ;
    $this->Cell($this->TFT_CELL_W[2], 5.0, $this->PageNo() . "ページ", 0, 0, "R") ;
  }

  function writeTitleHeader()
  	{
    $this->SetFontSize(15) ;
    $this->Cell(0, 5.0, "検査結果報告書", 0, 1, "C") ;
    $this->SetFontSize(10) ;
    $this->SetY($this->GetY() + 7) ;
    $this->Cell($this->THD1_CELL_W[0], 5.0, "病棟："     . $this->kensaInfos[0]) ;
    $this->Cell($this->THD1_CELL_W[1], 5.0, "入院外来：" . $this->kensaInfos[1], 0, 1) ;
    $this->Cell($this->THD2_CELL_W[0], 5.0, "患者ID："   . $this->kensaInfos[2]) ;
    $this->Cell($this->THD2_CELL_W[1], 5.0, "患者名："   . $this->kensaInfos[3] . "　様", 0, 0) ;
    if($this->kensaInfos["isTimeSeries"]) $this->Cell($this->THD2_CELL_W[0], 1, "", 0, 1) ;
    if(!$this->kensaInfos["isTimeSeries"]) $this->Cell($this->THD2_CELL_W[2], 5.0, "日付　："   . $this->kensaInfos[4], 0, 1) ;
    if(!$this->kensaInfos["isTimeSeries"]) $this->Cell($this->THD3_CELL_W[0], 5.0, "受付ID："   . $this->kensaInfos[5]) ;
    if(!$this->kensaInfos["isTimeSeries"]) $this->Cell($this->THD3_CELL_W[1], 5.0, "Ｄｒ．："   . $this->kensaInfos[6]) ;
    if(!$this->kensaInfos["isTimeSeries"]) $this->Cell($this->THD3_CELL_W[2], 5.0, "採血日："   . $this->kensaInfos[7], 0, 1) ;
    if(!$this->kensaInfos["isTimeSeries"]) $this->SetY($this->GetY() - 2) ;
  }

    function writeContsHeader()
    {
        $this->SetY($this->GetY() + 5) ; // 3
        $this->Cell($this->CHD1_CELL_W[0], 5.0, "項目名称",     1, 0, "C", 1) ;
        $this->Cell($this->CHD1_CELL_W[1], 5.0, "H/L",          1, 0, "C", 1) ;
        $this->Cell($this->CHD1_CELL_W[2], 5.0, "検査結果",     1, 0, "C", 1) ;
        $this->Cell($this->CHD1_CELL_W[3], 5.0, "単位",         1, 0, "C", 1) ;
        $this->Cell($this->CHD1_CELL_W[4], 5.0, "基準値",       1, 0, "C", 1) ;
        $this->Cell($this->CHD1_CELL_W[5], 5.0, "結果コメント", 1, 1, "C", 1) ;
    }

    function writeConts($kensaDatas)
    {
      $this->Cell($this->CHD1_CELL_W[0], 5.0, $kensaDatas[0], 1,     0     ) ;
      $this->Cell($this->CHD1_CELL_W[1], 5.0, $kensaDatas[1], "TRB", 0, "C") ;
      $this->Cell($this->CHD1_CELL_W[2], 5.0, $kensaDatas[2], "TRB", 0, "R") ;
      $this->Cell($this->CHD1_CELL_W[3], 5.0, $kensaDatas[3], "TRB", 0, "C") ;
      $this->Cell($this->CHD1_CELL_W[4], 5.0, $kensaDatas[4], "TRB", 0     ) ;
      $this->Cell($this->CHD1_CELL_W[5], 5.0, $kensaDatas[5], "TRB", 1     ) ;
    }

}

$kensaInfos_ = array(
    $byoutou,                      // 病棟
    $nyugai,                       // 入院外来
    trim($rec1["ptif_id"]),        // 患者ID
    trim($rec1["ptr_name_kana"]),  // 患者名
    $uketuke_date,                 // 日付
    $uke_no,                       // 受付ID
    trim($rec1["knr_tantou_doc"]), // Ｄｒ．
    $c_sot_util->kanji_ymd($rec1["knr_saiketu_date"]), // 採血日 例：2011年3月14日
    isTimeSeries => $isTimeSeries  //検査時系列表示
    ) ;

$pdf = new CustomMBFPDF($kensaInfos_) ;
$pdf->Open() ;
$pdf->AddPage() ;

  $seijou_low_key  = "" ;
  $seijou_high_key = "" ;
  $sex = strtolower($rec1["ptr_sex"]) ;
  if (($sex == "m") || ($sex == "f"))
  {
    $seijou_low_key  = "itm_seijou_low_"  . $sex ;
    $seijou_high_key = "itm_seijou_high_" . $sex ;
  }
if(!$isTimeSeries) {
  foreach ($recs as $codes => $rec) // 検査データのループ
  {
    $seijou_low  = @$rec[$seijou_low_key ] ;
    $seijou_high = @$rec[$seijou_high_key] ;
    $kijunti = trim($seijou_low) . " - " . trim($seijou_high) ;
    $kensa_kekka = trim($rec["itm_kensa_kekka"]) ;
  //  if (!$kensa_kekka) $kensa_kekka = "検査中" ;
    if (!$kensa_kekka) continue ;
    $comments = array();
    if (trim($rec["itm_kekka_cmt_1"]) != "") {
      $comments[] = trim($rec["itm_kekka_cmt_1"]);
    }
    if (trim($rec["itm_kekka_cmt_2"]) != "") {
      $comments[] = trim($rec["itm_kekka_cmt_2"]);
    }
    $comment = implode(" / ", $comments);
    $kensaDatas = array(
        trim($rec["itm_item_name"]),
        $rec["itm_ijouti_kbn"],
        $kensa_kekka,
        trim($rec["itm_tani"]),
        $kijunti,
        $comment
        ) ;
    $pdf->writeConts($kensaDatas) ;
  }

}else{
//---------------
//検査時系列
//---------------
  $cell_w = array(45, 8, 19) ;
  $curr_y = $pdf->GetY() + 5;
  $pdf->SetY($curr_y);
  $sIdx  = (int)@$_REQUEST["sIdx"] ;

  $pdf->Cell($cell_w[0], 5.0, "",         1, 2, "C", 1) ;
  $pdf->Cell($cell_w[0], 5.0, "項目名称", 1, 0, "C", 1) ;

  $curr_x = $pdf->GetX();
  $this_x = $curr_x;
  $this_y = $curr_y;
  for($ii = $sIdx ; $ii < ($sIdx + 5) ; ++$ii) // 受付日のループ
  {
    $pdf->SetXY($this_x, $this_y) ;
    $uketuke_strs = explode("_", $allDatas[$ii][0]) ;
    $uketuke_date = str_replace("/", ".", $c_sot_util->slash_yyyymmdd($uketuke_strs[0])) ;
    $pdf->Cell($cell_w[1] + $cell_w[2], 5.0, $uketuke_date,  1, 2, "C", 1) ;
    $pdf->Cell($cell_w[1], 5.0, "H/L",          1, 0, "C", 1) ;
    $pdf->Cell($cell_w[2], 5.0, "検査結果",     1, 1, "C", 1) ;
    $this_x = $this_x + $cell_w[1] + $cell_w[2];
  }
  // 検査項目
  $itemInfos2 = null ;
  $itmNosCsv  = @$_REQUEST["itm_ord"] ;

  if ($itmNosCsv)
  {
    $itmNosCsv = rtrim($itmNosCsv, ",") ;
    $itmNos = explode(",", $itmNosCsv) ;
    $itemInfos2 = array() ;
    foreach ($itmNos as $idx => $itmNo)
    {
      $itemInfos2[$itmNo] = $itemInfos[$itmNo] ;
    }
  }
  if ($itemInfos2) $itemInfos = $itemInfos2 ;
  $curr_y = $pdf->GetY();
  foreach ($itemInfos as $itemNo => $itemName) // 検査項目のループ
  {
    $pdf->SetY($curr_y);
    $pdf->Cell($cell_w[0], 5.0, rtrim($itemName), 1, 0, "L") ;
    $curr_x = 60;
    for($i = $sIdx ; $i < ($sIdx + $colNum) ; $i++) // 受付日のループ
    {
      $kensa_kekka     = "" ;
      $kekka_jouge_kbn = "" ;
      $datas = @$allDatas[$i] ;
      if ($datas)
      {
        $recs = @$datas[1][$itemNo] ; // レコード配列
        if ($recs)
        {
          $kensa_kekka     = trim($recs["itm_kensa_kekka"]) ;
          $kekka_jouge_kbn =      $recs["itm_ijouti_kbn" ]  ;
          if (!$kensa_kekka) $kensa_kekka = "検査中" ;
        }
      }
      $pdf->SetX($curr_x);
      $pdf->Cell($cell_w[1], 5.0, $kekka_jouge_kbn, 1, 0, "C") ;
      $pdf->Cell($cell_w[2], 5.0, $kensa_kekka, 1, 0, "C") ;
      $curr_x = $curr_x + $cell_w[1] + $cell_w[2];
    }
    $pdf->SetX(15);
    $curr_y = $pdf->GetY() + 5 ;
  }
}

//ob_end_clean() ;
$pdf->Output();
//}
?>
