<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務シフト作成 | 管理 | 同期設定</title>

<?
require_once("about_authority.php");
require_once("about_session.php");
require_once("duty_shift_common.ini");
require_once("duty_shift_manage_tab_common.ini");
require_once("duty_shift_common_class.php");
require_once("show_class_name.ini");

$fname = $PHP_SELF;

$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

$con = connect2db($fname);
$obj = new duty_shift_common_class($con, $fname);

// 権限のチェック
if ($obj->check_authority_Management($session, $fname) == "") {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
$admin_auth = $obj->check_authority_user($session, $fname);

// 部署データの取得
$sql = "select class_id, class_nm from classmst";
$cond = "where class_del_flg = false order by order_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$classes = array();
while ($row = pg_fetch_array($sel)) {
	$classes[$row["class_id"]] = array("name" => $row["class_nm"], "atrbs" => array());
}
$sql = "select atrb_id, atrb_nm, class_id from atrbmst";
$cond = "where atrb_del_flg = false order by order_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
while ($row = pg_fetch_array($sel)) {
	if (!isset($classes[$row["class_id"]])) continue;
	$classes[$row["class_id"]]["atrbs"][$row["atrb_id"]] = array("name" => $row["atrb_nm"], "depts" => array());
}
$sql = "select d.dept_id, d.dept_nm, a.class_id, d.atrb_id from deptmst d inner join atrbmst a on a.atrb_id = d.atrb_id";
$cond = "where d.dept_del_flg = false order by d.order_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
while ($row = pg_fetch_array($sel)) {
	if (!isset($classes[$row["class_id"]]) || !isset($classes[$row["class_id"]]["atrbs"][$row["atrb_id"]])) continue;
	$classes[$row["class_id"]]["atrbs"][$row["atrb_id"]]["depts"][$row["dept_id"]] = array("name" => $row["dept_nm"], "rooms" => array());
}
$arr_class_name = get_class_name_array($con, $fname);
if ($arr_class_name["class_cnt"] == 4) {
	$sql = "select r.room_id, r.room_nm, a.class_id, a.atrb_id, r.dept_id from classroom r inner join deptmst d on d.dept_id = r.dept_id inner join atrbmst a on a.atrb_id = d.atrb_id";
	$cond = "where r.room_del_flg = false order by r.order_no";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		if (!isset($classes[$row["class_id"]]) || !isset($classes[$row["class_id"]]["atrbs"][$row["atrb_id"]]) || !isset($classes[$row["class_id"]]["atrbs"][$row["atrb_id"]]["depts"][$row["dept_id"]])) continue;
		$classes[$row["class_id"]]["atrbs"][$row["atrb_id"]]["depts"][$row["dept_id"]]["rooms"][$row["room_id"]] = array("name" => $row["room_nm"]);
	}
}

if ($postback != "t") {

	// 同期フラグの取得
	$sql = "select sync_flag from duty_shift_option";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$sync_flag = pg_fetch_result($sel, 0, "sync_flag");

	// リンク設定の取得
	$sql = "select * from duty_shift_sync_link";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$tmp_group_id = $row["group_id"];
		$class_var = "class_$tmp_group_id";
		$$class_var = $row["class_id"];
		$atrb_var = "atrb_$tmp_group_id";
		$$atrb_var = $row["atrb_id"];
		$dept_var = "dept_$tmp_group_id";
		$$dept_var = $row["dept_id"];
		$room_var = "room_$tmp_group_id";
		$$room_var = $row["room_id"];
	}
}

$groups = $obj->get_duty_shift_group_array("", null, $obj->get_wktmgrp_array());
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function onClassChange(row) {
	var atrb_box = document.mainform.elements['atrb_' + row];
	clearOptions(atrb_box);
	clearOptions(document.mainform.elements['dept_' + row]);
	clearOptions(document.mainform.elements['room_' + row]);

	var class_id = document.mainform.elements['class_' + row].value;
<?
foreach ($classes as $tmp_class_id => $tmp_class) {
	if (count($tmp_class["atrbs"]) == 0) continue;
	echo("\tif (class_id == '$tmp_class_id') {\n");
	foreach ($tmp_class["atrbs"] as $tmp_atrb_id => $tmp_atrb) {
		echo("\t\taddOption(atrb_box, '$tmp_atrb_id', '{$tmp_atrb["name"]}');\n");
	}
	echo("\t}\n");
}
?>
}

function onAtrbChange(row) {
	var dept_box = document.mainform.elements['dept_' + row];
	clearOptions(dept_box);
	clearOptions(document.mainform.elements['room_' + row]);

	var class_id = document.mainform.elements['class_' + row].value;
	var atrb_id = document.mainform.elements['atrb_' + row].value;
<?
foreach ($classes as $tmp_class_id => $tmp_class) {
	if (count($tmp_class["atrbs"]) == 0) continue;
	foreach ($tmp_class["atrbs"] as $tmp_atrb_id => $tmp_atrb) {
		if (count($tmp_atrb["depts"]) == 0) continue;
		echo("\tif (class_id == '$tmp_class_id' && atrb_id == '$tmp_atrb_id') {\n");
		foreach ($tmp_atrb["depts"] as $tmp_dept_id => $tmp_dept) {
			echo("\t\taddOption(dept_box, '$tmp_dept_id', '{$tmp_dept["name"]}');\n");
		}
		echo("\t}\n");
	}
}
?>
}

function onDeptChange(row) {
	var room_box = document.mainform.elements['room_' + row];
	clearOptions(room_box);

	var class_id = document.mainform.elements['class_' + row].value;
	var atrb_id = document.mainform.elements['atrb_' + row].value;
	var dept_id = document.mainform.elements['dept_' + row].value;
<?
foreach ($classes as $tmp_class_id => $tmp_class) {
	if (count($tmp_class["atrbs"]) == 0) continue;
	foreach ($tmp_class["atrbs"] as $tmp_atrb_id => $tmp_atrb) {
		if (count($tmp_atrb["depts"]) == 0) continue;
		foreach ($tmp_atrb["depts"] as $tmp_dept_id => $tmp_dept) {
			if (count($tmp_dept["rooms"]) == 0) continue;
			echo("\tif (class_id == '$tmp_class_id' && atrb_id == '$tmp_atrb_id' && dept_id == '$tmp_dept_id') {\n");
			foreach ($tmp_dept["rooms"] as $tmp_room_id => $tmp_room) {
				echo("\t\taddOption(room_box, '$tmp_room_id', '{$tmp_room["name"]}');\n");
			}
			echo("\t}\n");
		}
	}
}
?>
}

function clearOptions(box) {
	if (!box) return;
	for (var i = box.length - 1; i > 0; i--) {
		box.options[i] = null;
	}
	box.options[0].text = '　　　　';
}

function addOption(box, value, text) {
	var opt = document.createElement('option');
	opt.value = value;
	opt.text = text;
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
img {border-style:none;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<? show_manage_title($session, $admin_auth); ?>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<? show_manage_tab_menuitem($session, $fname, null); ?>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width"1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
</td>
</tr>
</table>

<table border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td style="padding-right:20px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>関連付け設定</b></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="duty_shift_sync.php?session=<? echo($session); ?>">同期処理</a></font></td>
</tr>
</table>

<form name="mainform" action="duty_shift_sync_setting_update.php" method="post">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td align="right"><input type="submit" value="更新"></td>
</tr>
</table>

<img src="img/spacer.gif" width="1" height="3" alt=""><br>

<table width="100%" border="0" cellspacing="0" cellpadding="4" class="list">
<tr height="22">
<td width="20%" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">リアルタイム同期</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="sync_flag" value="t"<? if ($sync_flag == "t") {echo(" checked");} ?>>する
<input type="radio" name="sync_flag" value="f"<? if ($sync_flag != "t") {echo(" checked");} ?>>しない
</font></td>
</tr>
</table>

<img src="img/spacer.gif" width="1" height="5" alt=""><br>

<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff">
<td width="20%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">シフトグループ名</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">リンク先の組織マスタ</font></td>
</tr>
<?
foreach ($groups as $tmp_group) {
	$tmp_group_id = $tmp_group["group_id"];
	$tmp_group_name = $tmp_group["group_name"];
	if ($tmp_group_name == "") { $tmp_group_name = "＿"; } // "　"->"＿"
	echo("<tr height=\"22\">\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_group_name</font></td>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
	echo($arr_class_name["class_nm"]);
?>
<select name="class_<? echo($tmp_group_id); ?>" onchange="onClassChange('<? echo($tmp_group_id); ?>');">
<option value="">　　　　</option>
<?
	$selected_class_id_var = "class_$tmp_group_id";
	foreach ($classes as $tmp_class_id => $tmp_class) {
		echo("<option value=\"{$tmp_class_id}\"");
		if ($tmp_class_id == $$selected_class_id_var) {echo(" selected");}
		echo(">{$tmp_class["name"]}\n");
	}
?>
</select>
<?
	echo($arr_class_name["atrb_nm"]);
?>
<select name="atrb_<? echo($tmp_group_id); ?>" onchange="onAtrbChange('<? echo($tmp_group_id); ?>');">
<option value="">　　　　</option>
<?
	$selected_atrb_id_var = "atrb_$tmp_group_id";
	if (isset($classes[$$selected_class_id_var])) {
		foreach ($classes[$$selected_class_id_var]["atrbs"] as $tmp_atrb_id => $tmp_atrb) {
			echo("<option value=\"{$tmp_atrb_id}\"");
			if ($tmp_atrb_id == $$selected_atrb_id_var) {echo(" selected");}
			echo(">{$tmp_atrb["name"]}\n");
		}
	}
?>
</select>
<?
	echo($arr_class_name["dept_nm"]);
?>
<select name="dept_<? echo($tmp_group_id); ?>" onchange="onDeptChange('<? echo($tmp_group_id); ?>');">
<option value="">　　　　</option>
<?
	$selected_dept_id_var = "dept_$tmp_group_id";
	if (isset($classes[$$selected_class_id_var]["atrbs"][$$selected_atrb_id_var])) {
		foreach ($classes[$$selected_class_id_var]["atrbs"][$$selected_atrb_id_var]["depts"] as $tmp_dept_id => $tmp_dept) {
			echo("<option value=\"{$tmp_dept_id}\"");
			if ($tmp_dept_id == $$selected_dept_id_var) {echo(" selected");}
			echo(">{$tmp_dept["name"]}\n");
		}
	}
?>
</select>
<?
	if ($arr_class_name["class_cnt"] == 4) {
		echo($arr_class_name["room_nm"]);
?>
<select name="room_<? echo($tmp_group_id); ?>">
<option value="">　　　　</option>
<?
		$selected_room_id_var = "room_$tmp_group_id";
		if (isset($classes[$$selected_class_id_var]["atrbs"][$$selected_atrb_id_var]["depts"][$$selected_dept_id_var]["rooms"])) {
			foreach ($classes[$$selected_class_id_var]["atrbs"][$$selected_atrb_id_var]["depts"][$$selected_dept_id_var]["rooms"] as $tmp_room_id => $tmp_room) {
				echo("<option value=\"{$tmp_room_id}\"");
				if ($tmp_room_id == $$selected_room_id_var) {echo(" selected");}
				echo(">{$tmp_room["name"]}\n");
			}
		}
?>
</select>
<?
	}

	echo("</font></td>\n");
	echo("</tr>\n");
}
?>
</table>


<input type="hidden" name="session" value="<? echo($session); ?>">
</form>

</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
