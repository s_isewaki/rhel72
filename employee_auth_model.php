<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 職員登録｜職員選択</title>
<?
require("about_session.php");
require("about_authority.php");
require("show_select_values.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 19, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 職種のデフォルトはログインユーザの職種
if ($job_id == "") {
	$sql = "select emp_job from empmst";
	$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$job_id = pg_fetch_result($sel, 0, "emp_job");
}

// イニシャルのデフォルトは「あ行」
if ($in_id == "") {
	$in_id = 1;
}
?>
<script language="javascript">
function jobOnChange(job_id) {
	location.href = '<? echo($fname); ?>?session=<? echo($session); ?>&job_id=' + job_id;
}

function employeeOnClick(emp_nm, auths) {
	opener.document.employee.model_emp_name.value = emp_nm;
	opener.document.employee.hid_model_emp_name.value = emp_nm;
	for (var i in auths) {
		if (opener.document.employee.elements[i]) {
			opener.document.employee.elements[i].checked = (auths[i] == 't');
		}
	}
	opener.initPage();
	window.close();
}
</script>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#5279a5">
<td width="520" height="22" class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>職員選択</b></font></td>
<td width="80" align="center"><a href="javascript:window.close()"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff">■閉じる</font></a></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22" bgcolor="#f6f9ff">
<td><select name="job_id" onchange="jobOnChange(this.value);"><? show_job_options($con, $job_id, $session, $fname); ?></select></td>
</tr>
<tr height="22" bgcolor="#f6f9ff">
<td><? show_initial_list($job_id, $in_id, $session, $fname); ?></td>
</tr>
<tr height="22" bgcolor="#f6f9ff">
<td><? show_employee_list($con, $job_id, $in_id, $session, $fname); ?></td>
</tr>
</table>
</center>
</body>
<? pg_close($con); ?>
</html>
<?
function show_job_options($con, $job_id, $session, $fname) {
	$sql = "select job_id, job_nm from jobmst";
	$cond = "where job_del_flg = 'f' order by order_no";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		echo("<option value=\"{$row["job_id"]}\"");
		if ($row["job_id"] == $job_id) {
			echo(" selected");
		}
		echo(">{$row["job_nm"]}\n");
	}
}

function show_initial_list($job_id, $in_id, $session, $fname) {
	$alphabet = array("あ行", "か行", "さ行", "た行", "な行", "は行", "ま行", "や行", "ら行", "わ行");
	for ($i = 0; $i < 10; $i++) {
		$index = $i + 1;
		echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		if ($in_id != $index) {
			echo("<a href=\"$fname?session=$session&job_id=$job_id&in_id=$index\">{$alphabet[$i]}</a>\n");
		} else {
			echo("{$alphabet[$i]}\n");
		}
		echo("</font>");
	}
}

function show_employee_list($con, $job_id, $in_id, $session, $fname) {
	$sql = "select empmst.emp_lt_nm, empmst.emp_ft_nm, authmst.* from empmst inner join authmst on empmst.emp_id = authmst.emp_id";

	switch ($in_id) {
	case "1":
		$keys = array("01", "02", "03", "04", "05");
		break;
	case "2":
		$keys = array("06", "07", "08", "09", "10", "11", "12", "13", "14", "15");
		break;
	case "3":
		$keys = array("16", "17", "18", "19", "20", "21", "22", "23", "24", "25");
		break;
	case "4":
		$keys = array("26", "27", "28", "29", "30", "31", "32", "33", "34", "35");
		break;
	case "5":
		$keys = array("36", "37", "38", "39", "40");
		break;
	case "6":
		$keys = array("41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55");
		break;
	case "7":
		$keys = array("56", "57", "58", "59", "60");
		break;
	case "8":
		$keys = array("61", "62", "63");
		break;
	case "9":
		$keys = array("64", "65", "66", "67", "68");
		break;
	case "10":
		$keys = array("69", "70", "71", "72", "73", "99");
		break;
	}
	$cond = "where empmst.emp_job = $job_id and (empmst.emp_keywd like '" . join("%' or empmst.emp_keywd like '", $keys) . "%') ";
	$cond .= "and authmst.emp_del_flg = 'f' order by empmst.emp_keywd";

	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	while ($row = pg_fetch_array($sel)) {
		$emp_nm = $row["emp_lt_nm"] . " " . $row["emp_ft_nm"];
		$auths = array();

		$auths[] = "'webml':'{$row["emp_webml_flg"]}'";
		$auths[] = "'schd':'{$row["emp_schd_flg"]}'";
		$auths[] = "'pjt':'{$row["emp_pjt_flg"]}'";
		$auths[] = "'newsuser':'{$row["emp_newsuser_flg"]}'";
		$auths[] = "'work':'{$row["emp_work_flg"]}'";
		$auths[] = "'phone':'{$row["emp_phone_flg"]}'";
		$auths[] = "'resv':'{$row["emp_resv_flg"]}'";
		$auths[] = "'attd':'{$row["emp_attdcd_flg"]}'";
		$auths[] = "'aprv_use':'{$row["emp_aprv_flg"]}'";
		$auths[] = "'conf':'{$row["emp_conf_flg"]}'";
		$auths[] = "'bbs':'{$row["emp_bbs_flg"]}'";
		$auths[] = "'qa':'{$row["emp_qa_flg"]}'";
		$auths[] = "'lib':'{$row["emp_lib_flg"]}'";
		$auths[] = "'adbk':'{$row["emp_adbk_flg"]}'";
		$auths[] = "'ext':'{$row["emp_ext_flg"]}'";
		$auths[] = "'link':'{$row["emp_link_flg"]}'";
		$auths[] = "'pass_flg':'{$row["emp_pass_flg"]}'";
		$auths[] = "'cmt':'{$row["emp_cmt_flg"]}'";
		$auths[] = "'memo':'{$row["emp_memo_flg"]}'";
		$auths[] = "'cauth':'{$row["emp_cauth_flg"]}'";

		$auths[] = "'intra':'{$row["emp_intra_flg"]}'";
		$auths[] = "'ymstat':'{$row["emp_ymstat_flg"]}'";
		$auths[] = "'search':'{$row["emp_search_flg"]}'";
		$auths[] = "'ptreg':'{$row["emp_ptreg_flg"]}'";
		$auths[] = "'ptif':'{$row["emp_ptif_flg"]}'";
		$auths[] = "'outreg':'{$row["emp_outreg_flg"]}'";
		$auths[] = "'inout':'{$row["emp_inout_flg"]}'";
		$auths[] = "'disreg':'{$row["emp_disreg_flg"]}'";
		$auths[] = "'dishist':'{$row["emp_dishist_flg"]}'";
		$auths[] = "'amb':'{$row["emp_amb_flg"]}'";
		$auths[] = "'ambadm':'{$row["emp_ambadm_flg"]}'";
		$auths[] = "'ward':'{$row["emp_ward_flg"]}'";
		$auths[] = "'nlcs':'{$row["emp_nlcs_flg"]}'";
		$auths[] = "'inci':'{$row["emp_inci_flg"]}'";
		$auths[] = "'fplus':'{$row["emp_fplus_flg"]}'";
		$auths[] = "'med':'{$row["emp_med_flg"]}'";
		$auths[] = "'biz':'{$row["emp_biz_flg"]}'";
		$auths[] = "'shift':'{$row["emp_shift_flg"]}'";
		$auths[] = "'cas':'{$row["emp_cas_flg"]}'";
		$auths[] = "'jinji':'{$row["emp_jinji_flg"]}'";
		$auths[] = "'ladder':'{$row["emp_ladder_flg"]}'";
		$auths[] = "'career':'{$row["emp_career_flg"]}'";
		$auths[] = "'shift2':'{$row["emp_shift2_flg"]}'";
		$auths[] = "'jnl':'{$row["emp_jnl_flg"]}'";

		$auths[] = "'news':'{$row["emp_news_flg"]}'";
		$auths[] = "'tmgd':'{$row["emp_tmgd_flg"]}'";
		$auths[] = "'wkadm':'{$row["emp_wkadm_flg"]}'";
		$auths[] = "'wkflw':'{$row["emp_wkflw_flg"]}'";
		$auths[] = "'cmtadm':'{$row["emp_cmtadm_flg"]}'";
		$auths[] = "'reg':'{$row["emp_reg_flg"]}'";
		$auths[] = "'empif':'{$row["emp_empif_flg"]}'";
		$auths[] = "'hspprf':'{$row["emp_hspprf_flg"]}'";
		$auths[] = "'dept_reg':'{$row["emp_dept_flg"]}'";
		$auths[] = "'job_flg':'{$row["emp_job_flg"]}'";
		$auths[] = "'status_flg':'{$row["emp_status_flg"]}'";
		$auths[] = "'attditem':'{$row["emp_attditem_flg"]}'";
		$auths[] = "'config':'{$row["emp_config_flg"]}'";
		$auths[] = "'lcs':'{$row["emp_lcs_flg"]}'";
		$auths[] = "'schdplc':'{$row["emp_schdplc_flg"]}'";
		$auths[] = "'fcl':'{$row["emp_fcl_flg"]}'";
		$auths[] = "'bbsadm':'{$row["emp_bbsadm_flg"]}'";
		$auths[] = "'qaadm':'{$row["emp_qaadm_flg"]}'";
		$auths[] = "'libadm':'{$row["emp_libadm_flg"]}'";
		$auths[] = "'adbkadm':'{$row["emp_adbkadm_flg"]}'";
		$auths[] = "'extadm':'{$row["emp_extadm_flg"]}'";
		$auths[] = "'linkadm':'{$row["emp_linkadm_flg"]}'";
		$auths[] = "'intram':'{$row["emp_intram_flg"]}'";
		$auths[] = "'stat':'{$row["emp_stat_flg"]}'";
		$auths[] = "'allot':'{$row["emp_allot_flg"]}'";
		$auths[] = "'life':'{$row["emp_life_flg"]}'";
		$auths[] = "'entity_flg':'{$row["emp_entity_flg"]}'";
		$auths[] = "'ward_reg':'{$row["emp_ward_reg_flg"]}'";
		$auths[] = "'nlcsadm':'{$row["emp_nlcsadm_flg"]}'";
		$auths[] = "'rm':'{$row["emp_rm_flg"]}'";
		$auths[] = "'fplusadm':'{$row["emp_fplusadm_flg"]}'";
		$auths[] = "'medadm':'{$row["emp_medadm_flg"]}'";
		$auths[] = "'webmladm':'{$row["emp_webmladm_flg"]}'";
		$auths[] = "'shiftadm':'{$row["emp_shiftadm_flg"]}'";
		$auths[] = "'casadm':'{$row["emp_casadm_flg"]}'";
		$auths[] = "'jinjiadm':'{$row["emp_jinjiadm_flg"]}'";
		$auths[] = "'ladderadm':'{$row["emp_ladderadm_flg"]}'";
		$auths[] = "'careeradm':'{$row["emp_careeradm_flg"]}'";
		$auths[] = "'shift2adm':'{$row["emp_shift2adm_flg"]}'";
		$auths[] = "'jnladm':'{$row["emp_jnladm_flg"]}'";
		$auths[] = "'aclg':'{$row["emp_accesslog_flg"]}'";

		$auths[] = "'manabu':'{$row["emp_manabu_flg"]}'";
		$auths[] = "'manabuadm':'{$row["emp_manabuadm_flg"]}'";
		$auths[] = "'kview':'{$row["emp_kview_flg"]}'";
		$auths[] = "'kviewadm':'{$row["emp_kviewadm_flg"]}'";
		$auths[] = "'ptadm':'{$row["emp_ptadm_flg"]}'";
		$auths[] = "'pjtadm':'{$row["emp_pjtadm_flg"]}'";

		for ($idx=1; $idx<=15; $idx++) {
			$auths[] = "'ccusr".$idx."':'".$row["emp_ccusr".$idx."_flg"]."'";
			$auths[] = "'ccadm".$idx."':'".$row["emp_ccadm".$idx."_flg"]."'";
		}
		for ($idx=1; $idx<=5; $idx++) {
			$auths[] = "'omusr".$idx."':'".$row["emp_omusr".$idx."_flg"]."'";
			$auths[] = "'omadm".$idx."':'".$row["emp_omadm".$idx."_flg"]."'";
		}

		echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		echo("<a href=\"javascript:void(0);\" onclick=\"employeeOnClick('$emp_nm', {" . join(", ", $auths) . "});\">$emp_nm</a>");
		echo("</font>");
		echo("<br>\n");
	}
}
?>
