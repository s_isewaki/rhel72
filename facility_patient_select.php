<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_session.php");
require("about_authority.php");
require("label_by_profile_type.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 設備予約権限のチェック
$checkauth = check_authority($session, 11, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// デフォルト値の設定
if ($page == "") {$page = 1;}
if ($copy == "") {$copy = "t";}

// データベースに接続
$con = connect2db($fname);

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

?>
<title>CoMedix 設備予約 | <?
// 患者選択/利用者選択
echo $_label_by_profile["PATIENT_SELECT"][$profile_type]; ?></title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function addPatient(pt_id, pt_nm) {
	var pt_ids = opener.document.mainform.pt_ids.value;
	var pt_id_array = (pt_ids == '') ? new Array() : pt_ids.split(',');

	for (var i = 0, j = pt_id_array.length; i < j; i++) {
		if (pt_id == pt_id_array[i]) {
			alert('すでに選択済みです。');
			return;
		}
	}

	pt_id_array.push(pt_id);
	opener.document.mainform.pt_ids.value = pt_id_array.join(',');

	var pt_nms = opener.document.getElementById('ptname').innerHTML;
	var msg = '「' + pt_nm + '」さんを';
	if (pt_nms == '') {
		msg += '選択しました。';
	} else {
		pt_nms += ', ';
		msg += '追加しました。';
	}
	pt_nms += pt_nm;
	opener.document.getElementById('ptname').innerHTML = pt_nms;
	if (document.mainform.copy.checked) {
		opener.document.mainform.title.value = pt_nms;
		msg += 'また、タイトルにもコピーしました。';
	}
	showMessage(msg);
}

function clearPatients() {
	opener.document.mainform.pt_ids.value = '';
	opener.document.getElementById('ptname').innerHTML = '';
	showMessage('選択済み<?
// 患者/利用者
echo $_label_by_profile["PATIENT"][$profile_type]; ?>を全クリアしました。');
}

function showMessage(msg) {
	document.getElementById('msgblock').style.backgroundColor = '#f7f7c9';
	document.getElementById('message').innerHTML = msg;
}

function toPage(page) {
	var copy = (document.mainform.copy.checked) ? 't' : 'f';
	location.href = 'facility_patient_select.php?session=<? echo($session); ?>&amp;pt_nm=<? echo(urlencode($pt_nm)); ?>&amp;page='.concat(page).concat('&amp;copy=').concat(copy);
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b><?
// 患者選択/利用者選択
echo $_label_by_profile["PATIENT_SELECT"][$profile_type]; ?></b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<form name="mainform" action="facility_patient_select.php" method="post">
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22" bgcolor="#f6f9ff">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 患者氏名/利用者氏名
echo $_label_by_profile["PATIENT_NAME"][$profile_type]; ?> <input type="text" name="pt_nm" value="<? echo($pt_nm); ?>" style="ime-mode:active;"> <input type="submit" value="検索"></font></td>
<td align="right"><input type="button" value="選択済み<?
// 患者/利用者
echo $_label_by_profile["PATIENT"][$profile_type]; ?>を全クリア" onclick="clearPatients();"></td>
</tr>
</table>
<? show_patient_list($con, $pt_nm, $page, $copy, $session, $fname); ?>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
<div id="msgblock" style="width:600px;margin-top:5px;text-align:left;padding:2px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><span id="message"></span></font></div>
</center>
</body>
<? pg_close($con); ?>
</html>
<?
function show_patient_list($con, $pt_nm, $page, $copy, $session, $fname) {
	$limit = 12;

	if ($pt_nm == "") {
		return;
	}

	$pt_nm = mb_convert_kana($pt_nm, "s", "EUC-JP");
	$pt_nm = str_replace(" ", "", $pt_nm);

	// 該当患者数を取得
	$sql = "select count(*) from ptifmst";
	$cond = "where ptif_del_flg = 'f' and (ptif_lt_kaj_nm || ptif_ft_kaj_nm like '%$pt_nm%' or ptif_lt_kana_nm || ptif_ft_kana_nm like '%$pt_nm%')";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$pt_cnt = intval(pg_fetch_result($sel, 0, 0));

	// 患者一覧を取得
	$sql = "select ptif_id, ptif_lt_kaj_nm, ptif_ft_kaj_nm, ptif_birth, ptif_sex from ptifmst";
	$offset = $limit * ($page - 1);
	$cond = "where ptif_del_flg = 'f' and (ptif_lt_kaj_nm || ptif_ft_kaj_nm like '%$pt_nm%' or ptif_lt_kana_nm || ptif_ft_kana_nm like '%$pt_nm%') order by ptif_id offset $offset limit $limit";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	echo("<table width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"margin-top:5px;\">\n");
	echo("<tr>\n");

	// ページ番号の表示
	if ($pt_cnt > $limit) {
		echo("<td valign=\"bottom\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">\n");

		if ($page > 1) {
			echo("<a href=\"javascript:toPage(" . ($page - 1) . ");\">＜前</a>　\n");
		} else {
			echo("＜前　\n");
		}

		$page_count = ceil($pt_cnt / $limit);
		for ($i = 1; $i <= $page_count; $i++) {
			if ($i != $page) {
				echo("<a href=\"javascript:toPage(" . ($i) . ");\">$i</a>\n");
			} else {
				echo("<b>$i</b>\n");
			}
		}

		if ($page < $page_count) {
			echo("　<a href=\"javascript:toPage(" . ($page + 1) . ");\">次＞</a>　\n");
		} else {
			echo("　次＞\n");
		}

		echo("</font></td>\n");
	}

	echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><input type=\"checkbox\" name=\"copy\" value=\"t\"");
	if ($copy == "t") {echo(" checked");}
	// 患者氏名/利用者氏名
	$patient_name_title = $_label_by_profile["PATIENT_NAME"][$profile_type];
	echo(">{$patient_name_title}をタイトルにもコピー</font></td>\n");
	echo("</tr>\n");
	echo("</table>\n");

	echo("<img src=\"img/spacer.gif\" alt=\"\" width=\"1\" height=\"2\"><br>\n");
	echo("<table width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"list\">\n");
	echo("<tr height=\"22\" bgcolor=\"#f6f9ff\">\n");
	echo("<td width=\"44%\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">氏名</font></td>\n");
	echo("<td width=\"26%\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">生年月日</font></td>\n");
	echo("<td width=\"18%\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">性別</font></td>\n");
	echo("<td width=\"12%\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">選択</font></td>\n");
	echo("</tr>\n");

	while ($row = pg_fetch_array($sel)) {
		$tmp_pt_id = $row["ptif_id"];
		$tmp_pt_nm = $row["ptif_lt_kaj_nm"] . " " . $row["ptif_ft_kaj_nm"];
		$tmp_birth = preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $row["ptif_birth"]);
		switch ($row["ptif_sex"]) {
			case "1":
				$tmp_sex = "男性";
				break;
			case "2":
				$tmp_sex = "女性";
				break;
			default:
				$tmp_sex = "不明";
				break;
		}

		echo("<tr height=\"22\">\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_pt_nm</font></td>\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_birth</font></td>\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_sex</font></td>\n");
		echo("<td align=\"center\"><input type=\"button\" value=\"選択\" onclick=\"addPatient('$tmp_pt_id', '$tmp_pt_nm');\"></td>\n");
		echo("</tr>\n");
	}

	echo("</table>\n");
}
?>
