<?php
require_once("about_comedix.php");
require_once("show_select_values.ini");
require_once("show_class_name.ini");
require_once("get_values.ini");
require_once("webmail_alias_functions.php");

$fname = $_SERVER["PHP_SELF"];

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 18, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 職員登録権限を取得
$reg_auth = check_authority($session, 19, $fname);

// 給与支給区分
$list_wage = array(
    "1" => "月給制",
//    "2" => "日給月給制",
    "3" => "日給制",
    "4" => "時間給制",
    "5" => "年俸制",
);

// データベースに接続
$con = connect2db($fname);

// 変更履歴を取得
if (!empty($_REQUEST["empcond_officehours_history_id"])) {
    $empcond_officehours_history_id = pg_escape_string($_REQUEST["empcond_officehours_history_id"]);

    // 個人別勤務時間帯履歴
    $sql = "select * from empcond_officehours_history";
    $cond = "where empcond_officehours_history_id=$empcond_officehours_history_id";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $num = pg_num_rows($sel);
    if ($num > 0) {
        //基本
        $weekday = "8";
        $officehours2_start = pg_fetch_result($sel, 0, "officehours2_start");
        $officehours2_end = pg_fetch_result($sel, 0, "officehours2_end");
        $officehours4_start = pg_fetch_result($sel, 0, "officehours4_start");
        $officehours4_end = pg_fetch_result($sel, 0, "officehours4_end");
        
        $varname1 = "start_hour".$weekday."_2";
        $varname2 = "start_min".$weekday."_2";
        $varname3 = "end_hour".$weekday."_2";
        $varname4 = "end_min".$weekday."_2";
        $varname5 = "start_hour".$weekday."_4";
        $varname6 = "start_min".$weekday."_4";
        $varname7 = "end_hour".$weekday."_4";
        $varname8 = "end_min".$weekday."_4";
        
        list($$varname1, $$varname2) = split(":", $officehours2_start);
        list($$varname3, $$varname4) = split(":", $officehours2_end);
        if ($officehours4_start != "") {
            list($$varname5, $$varname6) = split(":", $officehours4_start);
        }
        if ($officehours4_end != "") {
            list($$varname7, $$varname8) = split(":", $officehours4_end);
        }
    }
    $hist = pg_fetch_assoc($sel);
    
    //sub
    if ($hist["sub_rec_cnt"] > 0) {
        
        $sql = "select * from empcond_officehours_history_sub";
        $cond = "where empcond_officehours_history_sub_id=$empcond_officehours_history_id";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }

        $num = pg_num_rows($sel);
        if ($num > 0) {
            
            for ($i=0; $i<$num; $i++) {
                $weekday = pg_fetch_result($sel, $i, "weekday");
                $officehours2_start = pg_fetch_result($sel, $i, "officehours2_start");
                $officehours2_end = pg_fetch_result($sel, $i, "officehours2_end");
                $officehours4_start = pg_fetch_result($sel, $i, "officehours4_start");
                $officehours4_end = pg_fetch_result($sel, $i, "officehours4_end");
                
                $varname1 = "start_hour".$weekday."_2";
                $varname2 = "start_min".$weekday."_2";
                $varname3 = "end_hour".$weekday."_2";
                $varname4 = "end_min".$weekday."_2";
                $varname5 = "start_hour".$weekday."_4";
                $varname6 = "start_min".$weekday."_4";
                $varname7 = "end_hour".$weekday."_4";
                $varname8 = "end_min".$weekday."_4";
                
                list($$varname1, $$varname2) = split(":", $officehours2_start);
                list($$varname3, $$varname4) = split(":", $officehours2_end);
                if ($officehours4_start != "") {
                    list($$varname5, $$varname6) = split(":", $officehours4_start);
                }
                if ($officehours4_end != "") {
                    list($$varname7, $$varname8) = split(":", $officehours4_end);
                }
            }
        }
    }
    
}
else {
    $hist = array("histdate" => date('Y-m-d H:i:s'));
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 職員登録 | 勤務時間帯履歴</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/employee/employee.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="employee_info_menu.php?session=<? echo($session); ?>"><img src="img/icon/b26.gif" width="32" height="32" border="0" alt="職員登録"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="employee_info_menu.php?session=<? echo($session); ?>"><b>職員登録</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_info_menu.php?session=<? echo($session); ?>&key1=<? echo(urlencode($key1)); ?>&key2=<? echo(urlencode($key2)); ?>&page=<? echo($page); ?>&view=<? echo($view); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">検索</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_list.php?session=<? echo($session); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_detail.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基本</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="employee_contact_setting.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">連絡先</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_condition_setting.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務条件</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#5279a5"><a href="employee_cond_hr_history_form.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&empcond_officehours_history_id=<? echo($empcond_officehours_history_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>勤務時間帯履歴</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_auth_setting.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_display_setting.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&view=<? echo($view); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示設定</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_menu_setting.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&view=<? echo($view); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メニュー設定</font></a></td>
<? if (webmail_alias_enabled()) { ?>
<td width="5">&nbsp;</td>
<td width="130" align="center" bgcolor="#bdd1e7"><a href="employee_alias_setting.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&view=<? echo($view); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メール転送設定</font></a></td>
<? } ?>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>

<br />
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">■勤務時間帯 変更履歴の追加/変更</font><br />
<br />
<form action="employee_cond_hr_history_submit.php" method="post">
<table width="700" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
    <td width="30%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務時間帯<br>
（勤務管理、<br>時間帯の設定で、<br>個人勤務条件を優先する<br>設定の場合に有効です）</font></td>
    <td>

<table width="" border="0" cellspacing="0" cellpadding="1">
<tr height="22">
<td align="center"></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所定労働</font></td>
<td align="center">
</td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">休憩時間</font></td>
</tr>
<tr height="22">
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基本時間</font></td>
<td align="center">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="start_hour8_2"><? show_hour_options_0_23($start_hour8_2); ?></select>：<select name="start_min8_2"><? show_min_options_5($start_min8_2); ?></select>〜<select name="end_hour8_2"><? show_hour_options_0_23($end_hour8_2); ?></select>：<select name="end_min8_2"><? show_min_options_5($end_min8_2); ?></select></font>
</td>
<td align="center">&nbsp;</td>
<td align="center">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="start_hour8_4"><? show_hour_options_0_23($start_hour8_4); ?></select>：<select name="start_min8_4"><? show_min_options_5($start_min8_4); ?></select>〜<select name="end_hour8_4"><? show_hour_options_0_23($end_hour8_4); ?></select>：<select name="end_min8_4"><? show_min_options_5($end_min8_4); ?></select></font>

</td>
</tr>

<tr height="22">
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">曜日別時間&nbsp;</font></td>
<td align="center"></td>
<td align="center"></td>
<td align="center"></td>
</tr>

<? /* 繰り返す
1:月曜日-6:土曜日
0:日曜日
7:祝日
 */
$arr_weekday_name = array("月曜日", "火曜日", "水曜日", "木曜日", "金曜日", "土曜日", "日曜日", "祝日");
$arr_weekday_id = array("1", "2", "3", "4", "5", "6", "0", "7");
for ($i=0; $i<8; $i++) {
    $varname1 = "start_hour".$arr_weekday_id[$i]."_2";
    $varname2 = "start_min".$arr_weekday_id[$i]."_2";
    $varname3 = "end_hour".$arr_weekday_id[$i]."_2";
    $varname4 = "end_min".$arr_weekday_id[$i]."_2";
    $varname5 = "start_hour".$arr_weekday_id[$i]."_4";
    $varname6 = "start_min".$arr_weekday_id[$i]."_4";
    $varname7 = "end_hour".$arr_weekday_id[$i]."_4";
    $varname8 = "end_min".$arr_weekday_id[$i]."_4";
 ?>
<tr height="22">
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_weekday_name[$i]);?></font></td>
<td align="center">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="<? echo($varname1);?>"><? show_hour_options_0_23($$varname1); ?></select>：<select name="<? echo($varname2);?>"><? show_min_options_5($$varname2); ?></select>〜<select name="<? echo($varname3);?>"><? show_hour_options_0_23($$varname3); ?></select>：<select name="<? echo($varname4);?>"><? show_min_options_5($$varname4); ?></select></font>
</td>
<td align="center">&nbsp;&nbsp;</td>
<td align="center">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="<? echo($varname5);?>"><? show_hour_options_0_23($$varname5); ?></select>：<select name="<? echo($varname6);?>"><? show_min_options_5($$varname6); ?></select>〜<select name="<? echo($varname7);?>"><? show_hour_options_0_23($$varname7); ?></select>：<select name="<? echo($varname8);?>"><? show_min_options_5($$varname8); ?></select></font>

</td>
</tr>
<? } ?>
</table>

    </td>
</tr>
<tr height="22">
    <td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">変更日時</font></td>
    <td>
        <input type="text" name="histdate" value="<?=substr($hist["histdate"],0,19)?>" size="25"><br />
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">　9999-99-99 99:99:99の形式で入力してください</font>
    </td>
</tr>
</table>

<p>
<button type="submit">登録</button>
</p>

<input type="hidden" name="emp_id" value="<? echo($emp_id); ?>">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="empcond_officehours_history_id" value="<? echo($empcond_officehours_history_id); ?>">
</form>

</td>
</tr>
</table>

</body>
</html>
<? pg_close($con); ?>