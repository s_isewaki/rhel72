<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");
require_once("hiyari_common.ini");
require_once("hiyari_reporting_common.ini");
require_once("hiyari_mail.ini");
require_once("Cmx.php");
require_once("aclg_set.php");

//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$summary = check_authority($session, 47, $fname);
if ($summary == "0") {
	showLoginPage();
	exit;
}


//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con,$_GET);

//==============================
//ユーザーID取得
//==============================
$emp_id = get_emp_id($con,$session,$fname);

// 警告文章の表示フラグ＆警告文表示
$print_setting = get_print_setting($con, $fname);

//==============================
//HTML出力
//==============================

//====================================
//件名・差出人・日付・あて先情報取得
//====================================
$where = "where mail_id = '$mail_id'";
$order = "order by recv_class_disp_index asc ";

$fields = array(
		'report_id',
//		'report_title',
//		'job_id',
//		'registrant_id',
//		'registrant_name',
//		'registration_date',
//		'eis_id',
//		'eid_id',
//		'del_flag',
//		'report_no',
//		'shitagaki_flg',
//		'report_comment',
//		'edit_lock_flg',
//		'anonymous_report_flg',
//		'registrant_class',
//		'registrant_attribute',
//		'registrant_dept',
//		'registrant_room',

//		'report_emp_class',
//		'report_emp_attribute',
//		'report_emp_dept',
//		'report_emp_room',
//		'report_emp_job',
//		'report_emp_lt_nm',
//		'report_emp_ft_nm',
//		'report_emp_kn_lt_nm',
//		'report_emp_kn_ft_nm',

//		'[auth]_update_emp_id',
//		'[auth]_start_flg',
//		'[auth]_end_flg',
//		'[auth]_start_date',
//		'[auth]_end_date',
//		'[auth]_use_flg',

		'mail_id',
		'subject',
//		'send_emp_id',
		'send_emp_name',
		'send_date',
//		'send_del_flg',
//		'send_job_id',
//		'send_eis_id',
		'anonymous_send_flg',
		'marker',

		'send_emp_class',
		'send_emp_attribute',
		'send_emp_dept',
		'send_emp_room',
//		'send_emp_job',
//		'send_emp_lt_nm',
//		'send_emp_ft_nm',
//		'send_emp_kn_lt_nm',
//		'send_emp_kn_ft_nm',

//		'recv_emp_id',
		'recv_emp_name',
		'recv_class',
		'recv_class_disp_index',
//		'recv_read_flg',
//		'recv_read_flg_update_date',
//		'recv_del_flg',
		'anonymous_recv_flg',

//		'recv_emp_class',
//		'recv_emp_attribute',
//		'recv_emp_dept',
//		'recv_emp_room',
//		'recv_emp_job',
//		'recv_emp_lt_nm',
//		'recv_emp_ft_nm',
//		'recv_emp_kn_lt_nm',
//		'recv_emp_kn_ft_nm',
	);

$recv_info_arr = get_recv_mail_list($con,$fname,$where,$order,$fields);
if(count($recv_info_arr) < 1)
{
	//パラメータ異常
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
// ログインユーザーの最優先権限を取得
//====================================

//$anonymous_open_flg = is_anonymous_readable($session, $con, $fname);







$subject = "";              // 件名
$send_emp_name = "";        // 送信者
$send_emp_class_name = "";  // 送信者部署
$send_date = "";            // 送信日
$recv_name = "";            // あて先
$report_id = "";            // レポートID

$to_recv_emp_name_arr = array();
$cc_recv_emp_name_arr = array();

$cnt = 0;
foreach($recv_info_arr as $list_data) {

	if($cnt == 0) {
		$subject = $list_data["subject"];

		$anonymous_send_flg = $list_data["anonymous_send_flg"];
//		if(!$anonymous_open_flg && $anonymous_send_flg == "t")
		if($anonymous_send_flg == "t")
		{
			$send_emp_name = get_anonymous_name($con,$fname);
		}
		else
		{
			$send_emp_name = $list_data["send_emp_name"];
		}

		$send_date = $list_data["send_date"];
		$report_id = $list_data["report_id"];

		$marker_style = get_style_by_marker($list_data["marker"]);

		//送信者部署
		$send_emp_class = $list_data["send_emp_class"];
		$send_emp_attribute = $list_data["send_emp_attribute"];
		$send_emp_dept = $list_data["send_emp_dept"];
		$send_emp_room = $list_data["send_emp_room"];
		$profile_use = get_inci_profile($fname, $con);
		if($profile_use["class_flg"] == "t" && $send_emp_class != "")
		{
			$send_emp_class_name .= get_class_nm($con,$send_emp_class,$fname);
		}
		if($profile_use["attribute_flg"] == "t" && $send_emp_attribute != "")
		{
			$send_emp_class_name .= get_atrb_nm($con,$send_emp_attribute,$fname);
		}
		if($profile_use["dept_flg"] == "t" && $send_emp_dept != "")
		{
			$send_emp_class_name .= get_dept_nm($con,$send_emp_dept,$fname);
		}
		if($profile_use["room_flg"] == "t" && $send_emp_room != "")
		{
			$send_emp_class_name .= get_room_nm($con,$send_emp_room,$fname);
		}
	}

	$recv_class = $list_data["recv_class"];

	$anonymous_recv_flg = $list_data["anonymous_recv_flg"];
	if($anonymous_recv_flg == "t")
	{
		$recv_emp_name = get_anonymous_name($con,$fname);
	}
	else
	{
		$recv_emp_name = $list_data["recv_emp_name"];
	}

	if($recv_class == "TO") {
		array_push($to_recv_emp_name_arr, $recv_emp_name);
	} else if($recv_class == "CC") {
		array_push($cc_recv_emp_name_arr, $recv_emp_name);
	}

	$cnt++;
}

foreach($to_recv_emp_name_arr as $i => $value) {


	if ($i > 0) {
		$recv_name .= " , ";
	}
	$recv_name .= $to_recv_emp_name_arr[$i];
}

foreach($cc_recv_emp_name_arr as $i => $value) {

	if ($i == 0) {
		$recv_name .= " ";
		$recv_name .= "【CC】";
	} else {
		$recv_name .= " , ";
	}
	$recv_name .= $cc_recv_emp_name_arr[$i];
}

// レポート内容取得
$mail_message = get_mail_message($mail_id);

// ログインユーザのレポート権限取得
$rep_obj = new hiyari_report_class($con, $fname);
$arr_report_auth = $rep_obj->get_my_inci_auth_list_for_report_progress($report_id, $emp_id);

// トランザクションの開始
pg_query($con, "begin transaction");

// 既読・更新処理
update_read_flg($con,$fname,$mail_id,$emp_id, "t");

// レポート進捗・受付日付更新処理

if($arr_report_auth)
{
	for($i = 0; $i < count($arr_report_auth); $i++)
	{
		$auth = $arr_report_auth[$i]["auth"];
		$rep_obj->set_report_progress_start_date($report_id, $auth, $emp_id,$mail_id);
	}
}

// レポート進捗・確認日付更新処理
if($mode == "update")
{
	if($arr_report_auth)
	{
		for($i = 0; $i < count($arr_report_auth); $i++)
		{
			$auth = $arr_report_auth[$i]["auth"];
			$rep_obj->set_report_progress_end_date($report_id, $auth, $emp_id,$mail_id);
		}
	}
}

// トランザクションをコミット
pg_query($con, "commit");




//==============================
//印刷設定情報取得
//==============================
$print_setting = get_print_setting($con,$fname);
$mail_to_non_print = $print_setting["mail_to_non_print_flg"] == "t";



?>

<?
//====================================
// HTML
//====================================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$INCIDENT_TITLE?> | メール印刷</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script language="javascript">

function load_action()
{
	//印刷して閉じる
	self.print();
//	self.close();
	setTimeout('self.close()', 1000);
}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
<!--
table.list_2 {border-collapse:collapse;}
table.list_2 td {border:#CCCCCC solid 1px;padding:1px;}

table.list {border-collapse:collapse;}
table.list td {border:#35B341 solid 1px;}

// -->
</style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="load_action()">

<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php // 警告文の出力設定確認 ?>
<?php if ( $print_setting["warning_sentence_print_flg"] == 't' ) { ?>
    <table width="100%" cellspacing="0" cellpadding="3" >
        <tr>
            <td align="right" width="20%">
                <font size="3" face="ＭＳ Ｐゴシック, Osaka" >
                    <?php echo(h($print_setting["warning_sentence"])) ?>
                </font>
            </td>
        </tr>
    </table>
<? } ?>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
  <tr>
    <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
    <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
  </tr>
  <tr>
    <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
    <td bgcolor="#F5FFE5">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
	  		<tr>
    			<td width="100" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka">件名&nbsp;：&nbsp;</font></td>
				<td align="left">
				<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td<? echo($marker_style); ?>>
					<font size="3" face="ＭＳ Ｐゴシック, Osaka"><? echo h($subject); ?></font>
					</td>
				</tr>
				</table>
				</td>
  	  		</tr>
	  		<tr>
    			<td align="right"><nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka">送信者&nbsp;：&nbsp;</font></nobr></td>
				<td align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka"><? echo $send_emp_name ?></font></td>
  	  		</tr>
	  		<tr>
    			<td align="right" nowrap><nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka">送信者部署&nbsp;：&nbsp;</font></nobr></td>
				<td align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka"><? echo $send_emp_class_name ?></font></td>
  	  		</tr>
	  		<tr><!-- >2006年 7月 10日（木）11:54 -->
    			<td align="right"><nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka">日付&nbsp;：&nbsp;</font></nobr></td>
				<td align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka"><? echo $send_date ?></font></td>
  	  		</tr>
<?
if(!$mail_to_non_print)
{
?>
	  		<tr>
    			<td align="right"><nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka">あて先&nbsp;：&nbsp;</font></nobr></td>
				<td align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka"><? echo $recv_name ?></font></td>
  	  		</tr>
<?
}
?>
		</table>
  	</td>
  	<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
  </tr>
  <tr>
    <td><img src="img/r_3.gif" width="10" height="10"></td>
    <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td><img src="img/r_4.gif" width="10" height="10"></td>
  </tr>
</table>

<img src="img/spacer.gif" width="1" height="10" alt=""><br>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr valign="top" height="690">
		<td bgcolor="#FFFBE7">
			<?=$mail_message?>
		</td>
	</tr>
</table>

</td>
</tr>
</table>

</body>
</html>

<?
//==============================
//DBコネクション終了
//==============================
pg_close($con);
?>

