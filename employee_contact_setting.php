<?
// このファイルのひとつ上の階層に上ってから、Core2呼び出し
require_once(dirname(__FILE__)."/class/Cmx/Core2/C2FW.php");
$c2app = c2GetC2App();
$EAL = $c2app->getEmpAuthority(C2_LOGIN_EMP_ID); // ログイン者の機能権限有無リスト
if(!$EAL["emp_empif_flg"]) c2ShowAlertAndGotoLoginPageAndDie("操作権限がありません。"); // ﾛｸﾞｲﾝ者に閲覧権限(18)が無ければﾛｸﾞｲﾝﾍﾟｰｼﾞへ強制送還するJS発行

$emp_reg_flg = $EAL["emp_reg_flg"]; // 編集権限
$emp_id = $_REQUEST["emp_id"];

$LBP = $c2app->getLabelByProfile();
$phs_label = $LBP["PHS"][$c2app->getPrfType()]; // 院内PHS/所内PHS


$webmail_alias_enabled = c2Bool(c2dbGetSystemConfig('webmail.alias_enabled'));


// このファイルのひとつ上の階層に上ってから、Core2呼び出し
require_once(dirname(__FILE__)."/class/Cmx/Core2/C2FW.php");


// 拡張empmst項目1から5のデータ
$spftEmpmstRow = c2dbGetTopRow("select * from spft_empmst where emp_id = ".c2dbStr($emp_id));
if ($back == "t") {
	for ($idx=1; $idx<=5; $idx++) {
		$spftEmpmstRow["empmst_ext".$idx] = $_REQUEST["empmst_ext".$idx];
	}
}

// 拡張empmst項目1から5のラベル
$_rows = c2dbGetRows("select field_id, field_label from spfm_field_struct where table_id = 'spft_empmst' and field_id like 'empmst_ext%'");
$fieldLabels = array();
foreach ($_rows as $row) $fieldLabels[$row["field_id"]] = $row["field_label"];


// 拡張empmst項目1から5の表示可能判定と編集可能判定を取得
// ・利用判定：disp_empがall_okか、そうでないかを判定
	// ・利用判定：disp_empがall_okか、そうでないかを判定。職員登録では編集不可フラグは不問。編集不可フラグはパスワード設定で有効となる。
$sql =
" select field_id, disp_emp from spfm_field_struct_auth".
" where table_id = 'spft_empmst' and field_id like 'empmst_ext%' and target_place = 'all'";
$rows = c2dbGetRows($sql);
$fieldStructAuthRows = array();
$ret = array();
foreach ($rows as $row) $fieldStructAuthRows[$row["field_id"]] = $row;


$empmstRow = c2dbGetTopRow("select * from empmst where emp_id = ".c2dbStr($emp_id));

$errmsg = array();
if ($_REQUEST["try_update"]) {
	$empmstRow = $_REQUEST;
	if (!$emp_reg_flg) $errmsg[]= "操作権限がありません。"; // ﾛｸﾞｲﾝ者に編集権限(19)が無い
	else {
		// 入力チェック
		if (strlen($empmstRow["emp_zip1"]) > 3) $errmsg[]= '郵便番号1が長すぎます。';
		if (strlen($empmstRow["emp_zip2"]) > 4) $errmsg[]= '郵便番号2が長すぎます。';
		if (strlen($empmstRow["emp_addr1"]) > 100) $errmsg[]= '住所1が長すぎます。';
		if (strlen($empmstRow["emp_addr2"]) > 100) $errmsg[]= '住所2が長すぎます。';
		if (strlen($empmstRow["emp_tel1"]) > 6) $errmsg[]= '電話番号1が長すぎます。';
		if (strlen($empmstRow["emp_tel2"]) > 6) $errmsg[]= '電話番号2が長すぎます。';
		if (strlen($empmstRow["emp_tel3"]) > 6) $errmsg[]= '電話番号3が長すぎます。';
		if (strlen($empmstRow["emp_email2"]) > 120) $errmsg[]= 'E-Mailが長すぎます。';
		if (strlen($empmstRow["emp_mobile1"]) > 6) $errmsg[]= '携帯端末1が長すぎます。';
		if (strlen($empmstRow["emp_mobile2"]) > 6) $errmsg[]= '携帯端末2が長すぎます。';
		if (strlen($empmstRow["emp_mobile3"]) > 6) $errmsg[]= '携帯端末3が長すぎます。';
		if (strlen($empmstRow["emp_m_email"]) > 120) $errmsg[]= '携帯Mailが長すぎます。';
		if (strlen($empmstRow["emp_ext"]) > 10) $errmsg[]= '内線番号が長すぎます。';
		if (strlen($empmstRow["emp_phs"]) > 6) $errmsg[]= $phs_label.'が長すぎます。';
	}
	if (!count($errmsg)) {

		// 職員の連絡先情報を更新
		$sql =
		" update empmst set".
		" emp_zip1 = ".c2dbStr($empmstRow["emp_zip1"]).
		",emp_zip2 = ".c2dbStr($empmstRow["emp_zip2"]).
		",emp_prv = ".c2dbStr($empmstRow["emp_prv"]).
		",emp_addr1 = ".c2dbStr($empmstRow["emp_addr1"]).
		",emp_addr2 = ".c2dbStr($empmstRow["emp_addr2"]).
		",emp_ext = ".c2dbStr($empmstRow["emp_ext"]).
		",emp_tel1 = ".c2dbStr($empmstRow["emp_tel1"]).
		",emp_tel2 = ".c2dbStr($empmstRow["emp_tel2"]).
		",emp_tel3 = ".c2dbStr($empmstRow["emp_tel3"]).
		",emp_mobile1 = ".c2dbStr($empmstRow["emp_mobile1"]).
		",emp_mobile2 = ".c2dbStr($empmstRow["emp_mobile2"]).
		",emp_mobile3 = ".c2dbStr($empmstRow["emp_mobile3"]).
		",emp_m_email = ".c2dbStr($empmstRow["emp_m_email"]).
		",emp_email2 = ".c2dbStr($empmstRow["emp_email2"]).
		",emp_phs = ".c2dbStr($empmstRow["emp_phs"]).
		" where emp_id = ".c2dbStr($emp_id);
		c2dbExec($sql);

		//----------拡張empmstテーブルを更新する。なければインサートする----------
		if (!(int)c2dbGetOne("select count(emp_id) from spft_empmst where emp_id = ".c2dbStr($emp_id))) {
			c2dbExec("insert into spft_empmst (emp_id) values (".c2dbStr($emp_id).")");
		}

		// 拡張職員マスタ、更新対象フィールドがあれば更新
		$upd_ext = array();
		for ($idx=1; $idx<=5; $idx++) {
			$row = $fieldStructAuthRows["empmst_ext".$idx];
			if ($row["disp_emp"]!="all_ok") continue;
			c2dbExec("update spft_empmst set empmst_ext".$idx." = ".c2dbStr($_REQUEST["empmst_ext".$idx])." where emp_id = ".c2dbStr($emp_id));
		}

		// 連絡先画面を再表示
		$url_key1 = urlencode($_REQUEST["key1"]);
		$url_key2 = urlencode($_REQUEST["key2"]);

		header("Location: employee_contact_setting.php?emp_id=".$emp_id."&key1=".$url_key1."&key2=".$url_key2."&class_cond=$class_cond&atrb_cond=$atrb_cond&dept_cond=$dept_cond&room_cond=$room_cond&job_cond=$job_cond&page=$page&view=$view&emp_o=$emp_o");
		die;
	}
}






c2env::echoStaticHtmlTag("HTML5_DOCTYPE_HTML");
c2env::echoStaticHtmlTag("HTML5_HEAD_META");
?>
<title>CoMedix 職員登録 | 連絡先</title>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
	table { font-size:13px }
	.list {border-collapse:collapse; }
	.list td, .list th { border:#5279a5 solid 1px; }
	.prop_list tr { height:22px }
	.prop_list th { text-align:right; font-weight:normal; background-color:#f6f9ff }
	.basic_tab_menu { border-bottom:2px solid #5279a5; margin-bottom:5px; }
	.basic_tab_menu th { text-align:center; font-weight:normal; background-color:#bdd1e7; padding-bottom:2px }
	.basic_tab_menu th.current { background-color:#5279a5 }
	.basic_tab_menu th.current a { font-weight:bold; color:#ffffff }
</style>
<script type="text/javascript">
<? if (count($errmsg)) {?> alert("<?=implode("\\n", $errmsg)?>"); <? } ?>
</script>
</head>
<body style="margin:0; padding:0"><div id="CLIENT_FRAME_CONTENT">



<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" style="padding:0 0 0 6px"><a href="employee_info_menu.php?session=<?=hh($session)?>"><img src="img/icon/b26.gif" width="32" height="32" border="0" alt="職員登録" style="vertical-align:top"></a></td>
<td width="100%" style="font-size:16px; padding:0">&nbsp;<a href="employee_info_menu.php?session=<?=hh($session)?>"><b>職員登録</b></a></td>
</tr>
</table>




<table width="100%" border="0" cellspacing="0" cellpadding="0" class="basic_tab_menu">
<tr height="22">
<th width="70"><a href="employee_info_menu.php?session=<?=hh($session)?>&key1=<?=hh(urlencode($key1))?>&key2=<?=hh(urlencode($key2))?>&page=<?=hh($page)?>&view=<?=hh($view)?>&emp_o=<?=hh($emp_o)?>">検索</a></th>
<td width="5">&nbsp;</td>
<th width="70"><a href="employee_list.php?session=<?=hh($session)?>&class_cond=<?=hh($class_cond)?>&atrb_cond=<?=hh($atrb_cond)?>&dept_cond=<?=hh($dept_cond)?>&room_cond=<?=hh($room_cond)?>&job_cond=<?=hh($job_cond)?>&page=<?=hh($page)?>&emp_o=<?=hh($emp_o)?>">一覧</a></th>
<td width="5">&nbsp;</td>
<th width="70"><a href="employee_detail.php?session=<?=hh($session)?>&emp_id=<?=hh($emp_id)?>&class_cond=<?=hh($class_cond)?>&atrb_cond=<?=hh($atrb_cond)?>&dept_cond=<?=hh($dept_cond)?>&room_cond=<?=hh($room_cond)?>&job_cond=<?=hh($job_cond)?>&page=<?=hh($page)?>&emp_o=<?=hh($emp_o)?>">基本</a></th>
<td width="5">&nbsp;</td>
<th width="75" class="current"><a href="employee_contact_setting.php?session=<?=hh($session)?>&emp_id=<?=hh($emp_id)?>&class_cond=<?=hh($class_cond)?>&atrb_cond=<?=hh($atrb_cond)?>&dept_cond=<?=hh($dept_cond)?>&room_cond=<?=hh($room_cond)?>&job_cond=<?=hh($job_cond)?>&page=<?=hh($page)?>&emp_o=<?=hh($emp_o)?>">連絡先</a></th>
<td width="5">&nbsp;</td>
<th width="80"><a href="employee_condition_setting.php?session=<?=hh($session)?>&emp_id=<?=hh($emp_id)?>&class_cond=<?=hh($class_cond)?>&atrb_cond=<?=hh($atrb_cond)?>&dept_cond=<?=hh($dept_cond)?>&room_cond=<?=hh($room_cond)?>&job_cond=<?=hh($job_cond)?>&page=<?=hh($page)?>&emp_o=<?=hh($emp_o)?>">勤務条件</a></th>
<td width="5">&nbsp;</td>
<th width="70"><a href="employee_auth_setting.php?session=<?=hh($session)?>&emp_id=<?=hh($emp_id)?>&class_cond=<?=hh($class_cond)?>&atrb_cond=<?=hh($atrb_cond)?>&dept_cond=<?=hh($dept_cond)?>&room_cond=<?=hh($room_cond)?>&job_cond=<?=hh($job_cond)?>&page=<?=hh($page)?>&emp_o=<?=hh($emp_o)?>">権限</a></th>
<td width="5">&nbsp;</td>
<th width="80"><a href="employee_display_setting.php?session=<?=hh($session)?>&emp_id=<?=hh($emp_id)?>&class_cond=<?=hh($class_cond)?>&atrb_cond=<?=hh($atrb_cond)?>&dept_cond=<?=hh($dept_cond)?>&room_cond=<?=hh($room_cond)?>&job_cond=<?=hh($job_cond)?>&page=<?=hh($page)?>&view=<?=hh($view)?>&emp_o=<?=hh($emp_o)?>">表示設定</a></th>
<td width="5">&nbsp;</td>
<th width="100"><a href="employee_menu_setting.php?session=<?=hh($session)?>&emp_id=<?=hh($emp_id)?>&class_cond=<?=hh($class_cond)?>&atrb_cond=<?=hh($atrb_cond)?>&dept_cond=<?=hh($dept_cond)?>&room_cond=<?=hh($room_cond)?>&job_cond=<?=hh($job_cond)?>&page=<?=hh($page)?>&view=<?=hh($view)?>&emp_o=<?=hh($emp_o)?>">メニュー設定</a></th>
<? if ($webmail_alias_enabled) { ?>
<td width="5">&nbsp;</td>
<th width="130"><a href="employee_alias_setting.php?session=<?=hh($session)?>&emp_id=<?=hh($emp_id)?>&class_cond=<?=hh($class_cond)?>&atrb_cond=<?=hh($atrb_cond)?>&dept_cond=<?=hh($dept_cond)?>&room_cond=<?=hh($room_cond)?>&job_cond=<?=hh($job_cond)?>&page=<?=hh($page)?>&view=<?=hh($view)?>&emp_o=<?=hh($emp_o)?>">メール転送設定</a></th>
<? } ?>
<td width="">&nbsp;</td>
</tr>
</table>






<form action="employee_contact_setting.php" method="post" name="employee">
<input type="hidden" name="try_update" value="1" />

<table width="600" border="0" cellspacing="0" cellpadding="2" class="list prop_list">
	<tr>
		<th width="120">職員ID</th>
		<td width="480"><?=hh($empmstRow["emp_personal_id"])?></td>
	</tr>
	<tr>
		<th>名前</th>
		<td><?=hh($empmstRow["emp_lt_nm"]." ".$empmstRow["emp_ft_nm"])?></td>
	</tr>
	<tr>
		<th>郵便番号</th>
		<td>
			<input name="emp_zip1" type="text" size="5" maxlength="3" value="<?=hh(trim($empmstRow["emp_zip1"]))?>" style="ime-mode:inactive;">
			-
			<input name="emp_zip2" type="text" size="5" maxlength="4" value="<?=hh(trim($empmstRow["emp_zip2"]))?>" style="ime-mode:inactive;">
		</td>
	</tr>
	<tr>
		<th>都道府県</th>
		<td>
			<select name="emp_prv">
				<? $pref_list = c2GetPrefList(); ?>
				<option value=""></option>
				<? foreach($pref_list as $idx=>$pref) { ?>
				<option value="<?=$idx?>"<?=(trim($idx)===$empmstRow["emp_prv"]?" selected":"")?>><?=$pref?></option>
				<? } ?>
			</select>
		</td>
	</tr>
	<tr>
		<th>住所1</th>
		<td><input name="emp_addr1" type="text" size="50" maxlength="50" value="<?=hh($empmstRow["emp_addr1"])?>" style="ime-mode:inactive;"></td>
	</tr>
	<tr>
		<th>住所2</th>
		<td><input name="emp_addr2" type="text" size="50" maxlength="50" value="<?=hh($empmstRow["emp_addr2"])?>" style="ime-mode:inactive;"></td>
	</tr>
	<tr>
		<th>電話番号</th>
		<td>
			<input name="emp_tel1" type="text" size="5" maxlength="6" value="<?=hh($empmstRow["emp_tel1"])?>" style="ime-mode:inactive;">
			-
			<input name="emp_tel2" type="text" size="5" maxlength="6" value="<?=hh($empmstRow["emp_tel2"])?>" style="ime-mode:inactive;">
			-
			<input name="emp_tel3" type="text" size="5" maxlength="6" value="<?=hh($empmstRow["emp_tel3"])?>" style="ime-mode:inactive;">
		</td>
	</tr>
	<!--
	<tr>
		<th>院内メール：</th>
		<td><input name="email" type="text" size="40" maxlength="100" value="=<?=hh($empmstRow["emp_email"])?>" style="ime-mode:inactive;"></td>
	</tr>
	-->
	<tr>
		<th>E-Mail</td>
		<td><input name="emp_email2" type="text" size="40" maxlength="100" value="<?=hh($empmstRow["emp_email2"])?>" style="ime-mode:inactive;"></td>
	</tr>
	<tr>
		<th>携帯端末・外線</th>
		<td>
			<input name="emp_mobile1" type="text" size="5" maxlength="6" value="<?=hh($empmstRow["emp_mobile1"])?>" style="ime-mode:inactive;">
			-
			<input name="emp_mobile2" type="text" size="5" maxlength="6" value="<?=hh($empmstRow["emp_mobile2"])?>" style="ime-mode:inactive;">
			-
			<input name="emp_mobile3" type="text" size="5" maxlength="6" value="<?=hh($empmstRow["emp_mobile3"])?>" style="ime-mode:inactive;">
		</td>
	</tr>
	<tr>
		<th>携帯Mail</th>
		<td><input name="emp_m_email" type="text" size="40" maxlength="100" value="<?=hh($empmstRow["emp_m_email"])?>" style="ime-mode:inactive;"></td>
	</tr>
	<tr>
		<th>内線番号</th>
		<td><input name="emp_ext" type="text" size="10" maxlength="10" value="<?=hh($empmstRow["emp_ext"])?>" style="ime-mode:inactive;"></td>
	</tr>
	<tr>
		<th><?=$phs_label?></th>
		<td><input name="emp_phs" type="text" size="10" maxlength="6" value="<?=hh($empmstRow["emp_phs"])?>" style="ime-mode:inactive;"></td>
	</tr>

<?
	for ($idx=1; $idx<=5; $idx++) {
		$row = $fieldStructAuthRows["empmst_ext".$idx];
		if ($row["disp_emp"]!="all_ok") continue;
?>
	<tr>
		<th><?=hh($fieldLabels["empmst_ext".$idx])?></th>
		<td colspan="3">
			<input name="empmst_ext<?=$idx?>" type="text" size="20" value="<?=hh($spftEmpmstRow["empmst_ext".$idx])?>" />
		</td>
	</tr>
<? } ?>


</table>




<? if ($emp_reg_flg) { ?>
	<div style="width:600px; text-align:right; padding:2px 0"><input type="submit" value="登録"></div>
<? } ?>



<input type="hidden" name="emp_id" value="<? echo($emp_id); ?>">
<input type="hidden" name="key1" value="<? echo($key1); ?>">
<input type="hidden" name="key2" value="<? echo($key2); ?>">
<input type="hidden" name="class_cond" value="<? echo($class_cond); ?>">
<input type="hidden" name="atrb_cond" value="<? echo($atrb_cond); ?>">
<input type="hidden" name="dept_cond" value="<? echo($dept_cond); ?>">
<input type="hidden" name="room_cond" value="<? echo($room_cond); ?>">
<input type="hidden" name="job_cond" value="<? echo($job_cond); ?>">
<input type="hidden" name="page" value="<? echo($page); ?>">
<input type="hidden" name="view" value="<? echo($view); ?>">
<input type="hidden" name="emp_o" value="<? echo($emp_o); ?>">

</div>
</form>
</body>
</html>
