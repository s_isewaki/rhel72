<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 55, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 入力チェック
foreach ($stats as $tmp_stat) {
	if (strlen($tmp_stat) > 20) {
		echo("<script type=\"text/javascript\">alert('値が長すぎます。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}
	if ($tmp_stat != "" && preg_match("/^-?[0-9]*$/", $tmp_stat) == 0 && preg_match("/^-?[0-9]+\.[0-9]+$/", $tmp_stat) == 0) {
		echo("<script type=\"text/javascript\">alert('数字以外の値があります。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}
}

// トランザクションを開始
pg_query($con, "begin transaction");

// 統計値を削除
$sql = "delete from stat";
$cond = "where stat_date = '$date' and statfcl_id = $fcl_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 統計値を登録
for ($i = 0, $j = count($stats); $i < $j; $i++) {
	list($tmp_item_id, $tmp_assist_id) = explode("_", $ids[$i]);

	$sql = "insert into stat (stat_date, statitem_id, statassist_id, statfcl_id, stat_val) values (";
	$content = array($date, $tmp_item_id, $tmp_assist_id, $fcl_id, $stats[$i]);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 自画面を閉じ、親画面をリフレッシュする
echo("<script type=\"text/javascript\">self.close();opener.location.href = 'intra_stat.php?session=$session&date=" . mktime(0, 0, 0, substr($date, 4, 2), substr($date, 6, 2), substr($date, 0, 4)) . "';</script>");
?>
