<!-- ************************************************************************ -->
<!-- 勤務シフト作成｜当直予定表作成「登録」 -->
<!-- ************************************************************************ -->
<?
require_once("about_session.php");
require_once("about_authority.php");

require_once("duty_shift_common_class.php");
require_once("duty_shift_duty_common_class.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
// セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
$obj_duty = new duty_shift_duty_common_class($con, $fname);
///-----------------------------------------------------------------------------
// トランザクションを開始
///-----------------------------------------------------------------------------
pg_query($con, "begin transaction");

///-----------------------------------------------------------------------------
//日編集
///-----------------------------------------------------------------------------
$start_date = $duty_start_date;
$end_date = $duty_end_date;
$calendar_array = $obj->get_calendar_array($start_date, $end_date);

for ($i=0; $i<count($calendar_array); $i++) {
	$duty_date[$i + 1] = $calendar_array[$i]["date"];
}

/******************************************************************************/
//勤務当直情報テーブル
/******************************************************************************/
///-----------------------------------------------------------------------------
// 勤務当直情報（下書き）を登録
///-----------------------------------------------------------------------------
if ($draft_flg == "1") {

	$inp_flg = "";

	for($i=0;$i<$data_cnt;$i++) {
		///-----------------------------------------------------------------------------
		//項目の配列編集
		///-----------------------------------------------------------------------------
		$emp_id = $staff_id[$i];
		///-----------------------------------------------------------------------------
		//削除
		///-----------------------------------------------------------------------------
		$sql = "delete from duty_shift_duty_draft";
		$cond = "where emp_id = '$emp_id' ";
		$cond .= "and duty_date >= '$start_date' and duty_date <= '$end_date' ";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		///-----------------------------------------------------------------------------
		//登録
		///-----------------------------------------------------------------------------
		for($k=1;$k<=$day_cnt;$k++) {
			///-----------------------------------------------------------------------------
			//項目の配列編集
			///-----------------------------------------------------------------------------
			$night_duty = "night_duty_$i" . "_" . $k;
			$wk_date = $duty_date[$k];
			///-----------------------------------------------------------------------------
			//データが存在する場合のみ登録
			///-----------------------------------------------------------------------------
			if (($staff_id[$i] != "") &&
				($duty_date[$k] != "") &&
				($$night_duty != "")) {
				///-----------------------------------------------------------------------------
				//ＳＱＬ
				///-----------------------------------------------------------------------------
				$sql = "insert into duty_shift_duty_draft (emp_id, duty_date, night_duty";
				$sql .= ") values (";
				$content = array($emp_id, $wk_date, $$night_duty);
				$ins = insert_into_table($con, $sql, $content, $fname);
				if ($ins == 0) {
					pg_query($con, "rollback");
					pg_close($con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}

				$inp_flg = "1";
			}
		}
	}
	///-----------------------------------------------------------------------------
	//入力がない場合、空データを１件だけ登録
	///-----------------------------------------------------------------------------
	if ($inp_flg == "") {
		for($i=0;$i<$data_cnt;$i++) {
			$k = 1;
			///-----------------------------------------------------------------------------
			//項目の配列編集
			///-----------------------------------------------------------------------------
			$emp_id = $staff_id[$i];
			$night_duty = "";
			$wk_date = $duty_date[$k];
			///-----------------------------------------------------------------------------
			//登録
			///-----------------------------------------------------------------------------
			$sql = "insert into duty_shift_duty_draft (emp_id, duty_date, night_duty";
			$sql .= ") values (";
			$content = array($emp_id, $wk_date, $$night_duty);
			$ins = insert_into_table($con, $sql, $content, $fname);
			if ($ins == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}
} else {
	///-----------------------------------------------------------------------------
	//対象全データ削除
	///-----------------------------------------------------------------------------
	//検索条件（職員）
	$cond_add = "";
	for($i=0;$i<$data_cnt;$i++) {
		$wk_id = $staff_id[$i];
		if ($cond_add == "") {
			$cond_add .= "and (emp_id = '$wk_id' ";
		} else {
			$cond_add .= "or emp_id = '$wk_id' ";
		}
	}
	if ($cond_add != "") {
		$cond_add .= ") ";
	}
	//削除
	$sql = "delete from duty_shift_duty_draft";
	$cond = "where 1 = 1 ";
	$cond .= "and duty_date >= '$start_date' and duty_date <= '$end_date' ";
	$cond .= $cond_add;
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

/******************************************************************************/
//atdbkテーブル
//atdbkrsltテーブル
/******************************************************************************/
///-----------------------------------------------------------------------------
// 勤務予定ＤＢ(atdb）へのデータ反映
// 勤務実績ＤＢ(atdbrslt）へのデータ反映
///-----------------------------------------------------------------------------
if ($draft_flg == "") {
	for($i=0;$i<$data_cnt;$i++) {
		///-----------------------------------------------------------------------------
		//項目の配列編集
		///-----------------------------------------------------------------------------
		$emp_id = $staff_id[$i];
		$tmcd_group_id = "";
		///-----------------------------------------------------------------------------
		//職員情報より取得
		///-----------------------------------------------------------------------------
		$sql = "select * from empcond";
		$cond = "where emp_id = '$emp_id' ";
		$cond .= "order by emp_id";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$num = pg_numrows($sel);
		if ($num > 0) {
			$tmcd_group_id = pg_result($sel,0,"tmcd_group_id");
		}
		//20110629 登録時エラーの不具合対応、未設定時は先頭の1番目のグループとする
		if ($tmcd_group_id == "") {
			$tmcd_group_id = "1";
		}
		///-----------------------------------------------------------------------------
		//対象日を処理
		///-----------------------------------------------------------------------------
		for($k=1;$k<=$day_cnt;$k++) {
			///-----------------------------------------------------------------------------
			//項目の配列編集
			///-----------------------------------------------------------------------------
			$wk_date = $duty_date[$k];
			$night_duty = "night_duty_$i" . "_" . $k;
			///-----------------------------------------------------------------------------
			// 勤務予定ＤＢ(atdb）へのデータ反映
			// 勤務実績ＤＢ(atdbrslt）へのデータ反映
			///-----------------------------------------------------------------------------
			for ($m=0; $m<2; $m++) {
				///-----------------------------------------------------------------------------
				// 対象ＤＢ判定
				///-----------------------------------------------------------------------------
				if ($m == 0) {
					$db_name = "atdbk";
				} else {
					$db_name = "atdbkrslt";
				}
				///-----------------------------------------------------------------------------
				// データが存在するかチェック
				///-----------------------------------------------------------------------------
				$sql = "select * from $db_name ";
				$cond = "where emp_id = '$emp_id' and date = '$wk_date' ";
				$sel = select_from_table($con, $sql, $cond, $fname);
				if ($sel == 0) {
					pg_close($con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
				$num = pg_numrows($sel);
				if ($num > 0) {
					///-----------------------------------------------------------------------------
					// 値が同じ場合、更新しない
					///-----------------------------------------------------------------------------
					$wk1 = pg_result($sel,0,"night_duty");
					if ($$night_duty != $wk1) {
						///-----------------------------------------------------------------------------
						// 存在する場合、ＵＰＤＡＴＥ
						///-----------------------------------------------------------------------------
						$sql = "update $db_name set";
						$set = array("night_duty");
						$setvalue = array($$night_duty);
						$cond = "where emp_id = '$emp_id' and date = '$wk_date'";
						$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
						if ($upd == 0) {
							pg_query($con, "rollback");
							pg_close($con);
							echo("<script type='text/javascript' src='./js/showpage.js'></script>");
							echo("<script language='javascript'>showErrorPage(window);</script>");
							exit;
						}
					}
				} else {
					if ($$night_duty != "") {
						///-----------------------------------------------------------------------------
						// 存在しない場合、ＩＮＳＥＲＴ
						///-----------------------------------------------------------------------------
					    $sql = "insert into $db_name (emp_id, date, night_duty, tmcd_group_id";
						$sql .= ") values (";
						$content = array($emp_id, $wk_date, $$night_duty, $tmcd_group_id);
						$ins = insert_into_table($con, $sql, $content, $fname);
						if ($ins == 0) {
							pg_query($con, "rollback");
							pg_close($con);
							echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
							echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
							exit;
						}
					}
				}
			}
		}
	}
}
////////////////////////////////

///-----------------------------------------------------------------------------
// トランザクションをコミット
///-----------------------------------------------------------------------------
pg_query($con, "commit");
///-----------------------------------------------------------------------------
// データベース接続を閉じる
///-----------------------------------------------------------------------------
pg_close($con);
///-----------------------------------------------------------------------------
// 遷移元画面を再表示
///-----------------------------------------------------------------------------
echo("<script type=\"text/javascript\">location.href = 'duty_shift_duty.php?session=$session&group_id=$group_id&duty_yyyy=$duty_yyyy&duty_mm=$duty_mm&duty_only_disp_flg=$duty_only_disp_flg';</script>");

?>

<!-- ************************************************************************ -->
<!-- デバック用 -->
<!-- ************************************************************************ -->
<HTML>
<?
/***
echo("デバック");
echo("<br>");
echo("tmcd_group_id=$tmcd_group_id");
echo("<br>");
for($i=0;$i<count($calendar_array);$i++) {
	$wk = $duty_date[$i + 1];
	$k = $i + 1;
	echo("duty_date($k)=$wk");
	echo("<br>");
}
***/
?>
</HTML>
