<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 設備予約 | 週（印刷）</title>
<?
require_once("about_comedix.php");
require("show_facility.ini");
require("facility_common.ini");
require("facility_week_common.ini");
require("holiday.php");
require("show_calendar_memo.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 11, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 表示範囲が渡されていない場合は「全て」に設定
if ($range == "") {
	$range = "all";
}

// タイムスタンプが渡されていない場合は当日日付のタイムスタンプを取得
if ($date == "") {
	$date = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
}

// データベースに接続
$con = connect2db($fname);

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// 参照／予約／管理可能なカテゴリ・設備一覧を配列で取得
$categories = get_referable_categories($con, $emp_id, $fname);

// オプション情報を取得
$default_info = get_default_info($con, $session, $fname);
$default_url = $default_info["url"];
$calendar_start = $default_info["calendar_start"];

// スタート曜日の設定
$start_wd = ($calendar_start == "1") ? 0 : 1;  // 0：日曜、1：月曜

// 処理週のスタート日のタイムスタンプを求める
$start_day = get_start_day($date, $start_wd);

// 前月・前週・翌週・翌月のタイプスタンプを変数に格納
$last_month = get_last_month($date);
$last_week = strtotime("-7 days", $date);
$next_week = strtotime("+7 days", $date);
$next_month = get_next_month($date, $start_wd);
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
@media print {
	.noprint {visibility:hidden;}
}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="self.print();self.close();">
<center>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr bgcolor="#f6f9ff">
<td height="30"><span class="noprint"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示範囲&nbsp;&nbsp;<? show_range_string($con, $fname, $range); ?></font></span><img src="img/spacer.gif" width="180" height="1" alt=""><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><? show_date_string($date, $start_day, $session, $range); ?></font></td>
<?
	echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">\n");
	$time_str = date("Y")."/".date("m")."/".date("d")." ".date("H").":".date("i");
	echo("作成日時：$time_str\n");
	echo("</font></td>\n");
?>
</tr>
</table>
<table width="100%" border="1" cellspacing="0" cellpadding="1">
<tr>
<td colspan="2" height="22" width="16%">&nbsp;</td>
<?
$start_date = date("Ymd", $start_day);
$end_date = date("Ymd", strtotime("+6 days", $start_day));
// カレンダーのメモを取得
$arr_calendar_memo = get_calendar_memo($con, $fname, $start_date, $end_date);

for ($i = 0; $i <= 6; $i++) {
	show_date($start_day, $start_wd, $i, $session, $range, $arr_calendar_memo);
}
?>
</tr>
<?
// 予約データをまとめて取得。キー：設備ID_年月日
$reservations = get_reservation($con, $categories, $range, $emp_id, $start_day, $date, $session, $fname);

show_reservation($con, $categories, $range, $emp_id, $start_day, $session, $fname, $reservations); ?>
</table>
</center>
</body>
<? pg_close($con); ?>
</html>
<?
// 表示範囲を出力
function show_range_string($con, $fname, $range) {
	if ($range == "all") {
		$range_string = "全て";
	} else if (strpos($range, "-") === false) {
		$sql = "select fclcate_name from fclcate";
		$cond = "where fclcate_id = $range";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$range_string = "【" . pg_fetch_result($sel, 0, "fclcate_name") . "】";
	} else {
		list($cate_id, $facility_id) = explode("-", $range);
		$sql = "select facility_name from facility";
		$cond = "where fclcate_id = $cate_id and facility_id = $facility_id";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$range_string = "・" . pg_fetch_result($sel, 0, "facility_name");
	}

	echo($range_string);
}

// 選択日付を出力
function show_date_string($date, $start_day, $session, $range) {
	$tmp_start_day = date("Y/m/d", $start_day);
	$tmp_end_day = date("Y/m/d", strtotime("+6 days", $start_day));
	echo("【{$tmp_start_day}〜{$tmp_end_day}】");
}

// 予約状況を出力
function show_reservation($con, $categories, $range, $emp_id, $start_day, $session, $fname, $reservations) {
	$today = date("Ymd");

	list($selected_category_id, $selected_facility_id) = split("-", $range);

	// 不要なカテゴリー情報を削除
	if ($selected_category_id != "all") {
		$categories = array($selected_category_id => $categories[$selected_category_id]);
	}

	// 不要な設備情報を削除
	if ($selected_facility_id != "") {
		$categories[$selected_category_id]["facilities"] = array(
			$selected_facility_id => $categories[$selected_category_id]["facilities"][$selected_facility_id]
		);
	}

	$start_day_ymd = date("Ymd", $start_day);

	// カテゴリをループ
	foreach ($categories as $tmp_category_id => $tmp_category) {
		$tmp_category_name = $tmp_category["name"];
		$tmp_bg_color = $tmp_category["bg_color"];
		$show_content = $tmp_category["show_content"];

		// カテゴリ行を出力
		echo("<tr height=\"22\" bgcolor=\"#$tmp_bg_color\">\n");
		echo("<td colspan=\"16\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><b>$tmp_category_name</b></font></td>\n");
		echo("</tr>\n");

		// 設備をループ
		foreach ($tmp_category["facilities"] as $tmp_facility_id => $tmp_facility) {
			$tmp_facility_name = $tmp_facility["name"];
			$tmp_admin_flg = $tmp_facility["admin_flg"];

			// 施設・設備行を出力
			echo("<tr height=\"22\">\n");
			echo("<td width=\"13%\" valign=\"top\" style=\"border-right-style:none;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_facility_name</font></td>\n");
			echo("<td width=\"3%\" style=\"border-left-style:none;\">&nbsp;</td>");

			// 1週間をループ
			for ($i = 0; $i <= 6; $i++) {
				if ($i > 0) {
					$tmp_date = strtotime("+$i days", $start_day);
				} else {
					$tmp_date = $start_day;
				}
				$ymd = date("Ymd", $tmp_date);
				$wd = date("w", $tmp_date);

				// 予約情報を出力
				if ($ymd == $today) {
					$bgcolor = " bgcolor=\"#ccffcc\"";
				} else if ($wd == 6) {
					$bgcolor = " bgcolor=\"#defafa\"";
				} else if ($wd == 0) {
					$bgcolor = " bgcolor=\"#fadede\"";
				} else {
					$bgcolor = "";
				}
				echo("<td height=\"22\" valign=\"top\"$bgcolor>\n");
				echo("<div style=\"position:relative;\">");
				echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
				// 予約情報配列を参照
				$tmp_reservation = $reservations["{$tmp_facility_id}_{$ymd}"];
				$rsv_count = count($tmp_reservation);
				if ($rsv_count > 0) {
					for ($j = 0; $j < $rsv_count; $j++) {
						echo("<tr>\n");

						// ボーダーの設定
						if ($j == 0) {
							if ($rsv_count == 1) {
								$border = "";
							} else {
								$border = "border-bottom-style:none;padding-bottom:2px;";
							}
						} else if ($j < $rsv_count - 1) {
							$border = "border-top:silver dotted 1px;border-bottom-style:none;padding-bottom:2px;";
						} else {
							$border = "border-top:silver dotted 1px;";
						}

						// ラベルセルの表示
						$tmp_rsv_id = $tmp_reservation[$j]["reservation_id"];
						$tmp_rsv_emp_id = $tmp_reservation[$j]["emp_id"];
						$tmp_s_time = $tmp_reservation[$j]["start_time"];
						$tmp_e_time = $tmp_reservation[$j]["end_time"];
						$tmp_title = $tmp_reservation[$j]["title"];
						$tmp_marker = $tmp_reservation[$j]["marker"];
						if ($show_content == "t") {
							$tmp_fclhist_content_type = $tmp_reservation[$j]["fclhist_content_type"];
							$tmp_content = $tmp_reservation[$j]["content"];
							// 内容編集
							if ($tmp_fclhist_content_type == "2") {
								$tmp_content = get_text_from_xml($tmp_content);
							}
						}
						
						if ($tmp_s_time != "") {
							$tmp_s_time = substr($tmp_s_time, 0, 2) . ":" . substr($tmp_s_time, 2, 2);
							$tmp_e_time = substr($tmp_e_time, 0, 2) . ":" . substr($tmp_e_time, 2, 2);
							$time_str = "$tmp_s_time-$tmp_e_time<br>";
						} else {
							$time_str = "<img src=\"img/icon/timeless.gif\" alt=\"時刻指定なし\" width=\"8\" height=\"8\">&nbsp;";
						}

						$style = get_style_by_marker($tmp_marker);

						if ($tmp_rsv_emp_id == $emp_id || $tmp_admin_flg) {
							$child = "facility_reservation_update.php?session=$session&rsv_id=$tmp_rsv_id&path=2&range=$range";
						} else {
							$child = "facility_reservation_detail.php?session=$session&rsv_id=$tmp_rsv_id&path=2&range=$range";
						}
						echo("<td height=\"22\" valign=\"top\"$bgcolor style=\"$border\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$time_str}<span$style>" . h($tmp_title));
						echo("</span>");
						// 予定内容を表示の場合
						if ($show_content == "t") {
							echo("<br>" . preg_replace("/\r?\n/", "<br>", h($tmp_content)));
						}
						echo("</font></td>\n");
						echo("</tr>\n");
					}
				}
				echo("</table>\n");
				echo("</div>\n");
				echo("</td>\n");
			}
			echo("</tr>");
		}
	}
}
?>
