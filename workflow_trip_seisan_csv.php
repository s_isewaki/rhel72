<?php

// 出張旅費
// 精算CSV出力
require_once("about_comedix.php");
ob_start();

$fname = $_SERVER["PHP_SELF"];

// セッションと権限のチェック
$session = qualify_session($_REQUEST["session"], $fname);
$checkauth = check_authority($session, 3, $fname);
if ($session == "0" or $checkauth == "0") {
    js_login_exit();
}

// データベースに接続
$con = connect2db($fname);

// 年月チェック
if ($_REQUEST["year"] == '-' or $_REQUEST["month"] == '-') {
    js_alert_exit('年月が選択されていません。');
}
$ym = sprintf("%04d%02d", $_REQUEST["year"], $_REQUEST["month"]);

// 所属
$class = @$_REQUEST["search_emp_class"];
$atrb = @$_REQUEST["search_emp_attribute"];
$dept = @$_REQUEST["search_emp_dept"];
$room = @$_REQUEST["search_emp_room"];

// データ取得SQL
$sql = "SELECT a.*, e.emp_personal_id FROM apply a JOIN empmst e USING (emp_id) WHERE not delete_flg AND not draft_flg AND apply_stat='1' AND apply_date LIKE '{$ym}%'";
if ($class) {
    $sql .=" AND a.emp_class={$class}";
}
if ($atrb) {
    $sql .=" AND a.emp_attribute={$atrb}";
}
if ($dept) {
    $sql .=" AND a.emp_dept={$dept}";
}
if ($room) {
    $sql .=" AND a.emp_room={$room}";
}

// SQL実行
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0) {
    js_error_exit();
}

// データ集計
$csv = array();
while ($r = pg_fetch_assoc($sel)) {
    $seisan = get_seisan_apply_content($r["apply_content"]);
    if (empty($csv[$r["emp_personal_id"]])) {
        $csv[$r["emp_personal_id"]] = $seisan;
    }
    else {
        $csv[$r["emp_personal_id"]] += $seisan;
    }
}

// CSVデータ
$csv_body = mb_convert_encoding("データ区分,コード,年月,旅費精算\r\n", "Shift_JIS", "EUC-JP");
foreach ($csv as $emp_id => $fee) {
    if ($fee != 0) {
        $csv_body .= sprintf("103,\"%s\",{$ym},%s\r\n", $emp_id, $fee);
    }
}

pg_close($con);
ob_end_clean();

// CSV出力
$file_name = "seisan_$ym.csv";
header("Content-Disposition: attachment; filename=$file_name");
header("Content-Type: application/octet-stream; name=$file_name");
header("Content-Length: " . strlen($csv_body));
echo($csv_body);

//-----------------------------------------------
// 決済申請XMLデータから精算額を取得
//-----------------------------------------------
function get_seisan_apply_content($apply_content) {
    // 制御コード除去
    $apply_content = preg_replace("/[\x01-\x08\x0b\x0c\x0e-\x1f\x7f]+/", "", $apply_content);
    $apply_content = str_replace("\0", "", $apply_content);

    // XMLでないテキストはdomxmlに流さない
    if (preg_match('/^<\?xml/', $apply_content) !== 1) {
        return 0;
    }

    $doc = domxml_open_mem($apply_content);
    $total_fee = $doc->get_elements_by_tagname('total_fee');
    if (empty($total_fee)) {
        return 0;
    }
    if ($total_fee[0]->node_type() != XML_ELEMENT_NODE) {
        return 0;
    }
    return $total_fee[0]->get_content();
}
