<?
require("about_authority.php");
require("about_session.php");
require("about_postgres.php");

//==============================
//ページ名
//==============================
$fname = $PHP_SELF;

//==============================
//パラメータを取得
//==============================
$session = isset($_POST['session']) ? $_POST['session'] : '';
$pt_id = isset($_POST['pt_id']) ? $_POST['pt_id'] : '';
$mode = isset($_POST['mode']) ? $_POST['mode'] : '';

//==============================
//セッションのチェック
//==============================
$session = qualify_session($session,$fname);
if($session == "0")
{
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//==============================
//DBコネクション
//==============================
$con = connect2db($fname);
if($con == "0")
{
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

if ($mode == 'get_stdplan_observation') {
    $observation = createObservationListForJavaScript($con, $pt_id, $fname);
    ob_clean();
    echo $observation;
}
pg_close($con);    // DB切断

/**
 * JavaScriptの配列形式文字列として、観察項目を取得する
 * 
 * @param resource $con   データベース接続リソース
 * @param string   $pt_id 患者ID
 * @param string   $fname ページ名
 * 
 * @return 観察項目
 */
function createObservationListForJavaScript($con, $pt_id, $fname)
{
    $data = getStdplanObservationData($con, $pt_id, $fname);
    if ($data === false) {
        return '[]';
    }
    $observation = '';
    foreach ($data as $value) {
        if (!empty($observation)) {
            $observation .= ',';
        }
        $observation .= "'{$value['item_name']}'";
    }
    $observation = "[{$observation}]";
    return $observation;
}

/**
 * 標準チーム計画から観察項目を取得する
 * 
 * 指定した患者で、各プロブレム毎に最新の標準チーム計画から、重複なしに観察項目を取得する
 * 
 * @param resource $con   データベース接続リソース
 * @param string   $pt_id 患者ID
 * @param string   $fname ページ名
 * 
 * @return mixed 成功時:取得したレコードの配列/失敗時:false
 */
function getStdplanObservationData($con, $pt_id, $fname)
{
    $sql = "SELECT item_name FROM tmplitem " 
         . "WHERE mst_cd = 'A263' AND item_cd IN "
         . "("
         .   "SELECT item_cd FROM sum_med_std_obse "
         .   "WHERE team_id IN "
         .   "("
         .     "SELECT sum_med_std_team.team_id FROM sum_med_std_team "
         .     "INNER JOIN (SELECT MAX(create_date) AS create_date, ptif_id, problem_id FROM sum_med_std_team GROUP BY ptif_id, problem_id) AS max "
         .     "ON sum_med_std_team.problem_id = max.problem_id AND sum_med_std_team.create_date = max.create_date AND sum_med_std_team.ptif_id = max.ptif_id "
         .     "INNER JOIN summary_problem_list ON sum_med_std_team.ptif_id = summary_problem_list.ptif_id AND sum_med_std_team.problem_id = summary_problem_list.problem_id "
         .     "WHERE sum_med_std_team.ptif_id = '{$pt_id}' AND summary_problem_list.end_date = ''"
         .   ") "
         .   "GROUP BY item_cd ORDER BY item_cd"
         . ")";
    $result = select_from_table($con, $sql, '', $fname);
    if ($result == 0) {
        return false;
    }
    $data = pg_fetch_all($result);
    return $data;
}
?>
