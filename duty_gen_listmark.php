<?
header("Content-Type: image/gif");

function setDot(&$img, $dots, $sx, &$clr1, &$clr2) {
    global $zs;
    if ($clr1) imagefilledrectangle($img, $sx, $sy, $sx+$zs-1, $sy+$zs-1, $clr1); // 塗りつぶす
    for ($x=0; $x<$zs; $x++) {
        for ($y=0; $y<$zs; $y++) {
            if ($dots[$x+$y*$zs]=="*") imagesetpixel($img, $x+$sx, $y, $clr2); // 点描
        }
    }
}






$zs = 7; // 画像の縦横それぞれの大きさは7px
$gc = 4; // 画像の数


// 引数
$b_empty_taikin = $_REQUEST["empty_taikin"];
$b_is_chikoku = $_REQUEST["is_chikoku"];
$b_is_soutai = $_REQUEST["is_soutai"];
$b_exist_meeting = $_REQUEST["exist_meeting"];
$b_exist_paid_hol_hour = $_REQUEST["exist_paid_hol_hour"];



$img = imagecreate($zs*$gc, $zs);

$sx = $zs*-1;
$sy = 0;


$bg = imagecolorallocate($img, 1, 1, 1); // 空白カラー：黒:透過
imagefill($img, 0, 0, $bg); // ベースで塗りつぶす
imagecolortransparent($img, $bg);



// 退勤未入力
$sx += $zs;
if ($b_empty_taikin) {
    $clr2  = imagecolorallocate($img, 255, 255, 255); // 空白カラー：白
    $clr1  = imagecolorallocate($img, 255, 0, 0); // アスタマークカラー：赤
    $dots = // ドットマッピング
    "**   **".
    "*  *  *".
    "  ***  ".
    " ***** ".
    "  ***  ".
    "*  *  *".
    "**   **";
    setDot($img, $dots, $sx, $clr1, $clr2);
}



// 遅刻 or 早退
$sx += $zs;
if ($b_is_chikoku || $b_is_soutai) {
    $clr2  = imagecolorallocate($img, 255, 255, 255); // 空白カラー：白
    $clr1  = imagecolorallocate($img, 0, 0, 255); // アスタマークカラー：青
    $dots = // ドットマッピング
    "**   **".
    "*  *  *".
    "  ***  ".
    " ***** ".
    "  ***  ".
    "*  *  *".
    "**   **";
    setDot($img, $dots, $sx, $clr1, $clr2);
}



// 会議（ミーティングのＭ）
$sx += $zs;
if ($exist_meeting) {
    $clr1  = imagecolorallocate($img, 255, 255, 255); // 空白カラー：白
    $clr2  = imagecolorallocate($img, 0, 200, 200); // アスタマークカラー：青緑
    $dots = // ドットマッピング
    "*******".
    "*******".
    "*     *".
    "* * * *".
    "* * * *".
    "* * * *".
    "*******";
    setDot($img, $dots, $sx, $clr1, $clr2);
}


// 有休のＹ
$sx += $zs;
if ($b_exist_paid_hol_hour) {
    $clr2  = imagecolorallocate($img, 255, 255, 255); // 空白カラー：白
    $clr1  = imagecolorallocate($img, 0, 130, 0); // アスタマークカラー：緑
    $dots = // ドットマッピング
    "       ".
    " *   * ".
    " *   * ".
    "  * *  ".
    "   *   ".
    "   *   ".
    "   *   ";
    setDot($img, $dots, $sx, $clr1, $clr2);
}




imagegif($img); // ファイル出力
imagedestroy($img); // 開放
?>