<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務管理 | 休暇取得状況</title>
<?php
require_once("about_comedix.php");
require_once("referer_common.ini");
require_once("work_admin_menu_common.ini");
require_once("atdbk_menu_common.ini");
require_once("settings_common.php");
require_once("timecard_hol_menu_common.php");
require_once("show_select_values.ini");
require_once("show_timecard_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 遷移元の取得
$referer = get_referer($con, $session, "work", $fname);

// 締め日を取得
$sql = "select closing, closing_parttime, closing_month_flg from timecard";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$closing = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "closing") : "";
//未設定時は末日とする 20110720
if ($closing == "") {
	$closing = "6";
}
$closing_parttime = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "closing_parttime") : "";
$closing_month_flg = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "closing_month_flg") : "1";
//非常勤が未設定時は常勤を使用する 20091214
if ($closing_parttime == "") {
	$closing_parttime = $closing;
}

$year = date("Y");
$month = date("n");
$yyyymm = $year . date("m");

// 開始日・締め日を算出
$calced = false;
switch ($closing) {
	case "1":  // 1日
		$closing_day = 1;
		break;
	case "2":  // 5日
		$closing_day = 5;
		break;
	case "3":  // 10日
		$closing_day = 10;
		break;
	case "4":  // 15日
		$closing_day = 15;
		break;
	case "5":  // 20日
		$closing_day = 20;
		break;
	case "6":  // 末日
		$start_yr = $year;
		$start_mon = $month;
		$start_day = 1;
		$end_yr = $start_yr;
		$end_mon = $start_mon;
		$end_day = days_in_month($end_yr, $end_mon);
		$calced = true;
		break;
}
if (!$calced) {
	$day = date("j");
	$start_day = $closing_day + 1;
	$end_day = $closing_day;

	if ($day <= $closing_day) {
		$start_yr = $year;
		$start_mon = $month - 1;
		if ($start_mon == 0) {
			$start_yr--;
			$start_mon = 12;
		}

		$end_yr = $year;
		$end_mon = $month;
	} else {
		$start_yr = $year;
		$start_mon = $month;

		$end_yr = $year;
		$end_mon = $month + 1;
		if ($end_mon == 13) {
			$end_yr++;
			$end_mon = 1;
		}
	}
}

?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/jquery/jquery-1.7.2.min.js"></script>
<script type="text/javascript">
<!--
function check_date(flg) {
	if (flg == 1) {
		start_yr = $("#start_yr").val();
		start_mon = $("#start_mon").val();
		start_day = $("#start_day").val();

		end_yr = $("#end_yr").val();
		end_mon = $("#end_mon").val();
		end_day = $("#end_day").val();
	}
	else {
		start_yr = $("#start_yr2").val();
		start_mon = $("#start_mon2").val();
		start_day = $("#start_day2").val();

		end_yr = $("#end_yr2").val();
		end_mon = $("#end_mon2").val();
		end_day = $("#end_day2").val();
	}

	start_dt = new Date(start_yr, start_mon-1, start_day);
	end_dt = new Date(end_yr, end_mon-1, end_day);

	msg = "";
	if (start_dt.getFullYear() != start_yr || start_dt.getMonth() != start_mon-1 || start_dt.getDate() != start_day) {
		msg += "開始日付が正しく指定されていません" + "\n";
	}

	if (end_dt.getFullYear() != end_yr || end_dt.getMonth() != end_mon-1 || end_dt.getDate() != end_day) {
		msg += "終了日付が正しく指定されていません" + "\n";
	}

	if (start_dt > end_dt){
		msg += "日付が正しく指定されていません。" + "\n";
	}
	
	if (msg != ""){
		alert(msg);
		return false;
	}
	return true;
}
//終了日月末設定
function set_last_day(flg) {
	if (flg == 1) {
		day = document.mainform.end_day.value;
	}
	else {
		day = document.paidholform.end_day2.value;
	}
	
	day = parseInt(day,10);
	if (day >= 28) {
		if (flg == 1) {
			year = document.mainform.end_yr.value;
			month = document.mainform.end_mon.value;
		}
		else {
			year = document.paidholform.end_yr2.value;
			month = document.paidholform.end_mon2.value;
		}
		last_day = days_in_month(year, month);
		if (last_day != day) {
			day = last_day;
			dd = (day < 10) ? '0'+day : day;
			if (flg == 1) {
				document.mainform.end_day.value = dd;
			}
			else {
				document.paidholform.end_day2.value = dd;
			}
		}
	}

}
//月末取得
function days_in_month(year, month) {
	//月別日数配列
	lastDay = new Array(31,28,31,30,31,30,31,31,30,31,30,31,29);
	//閏年
	wk_year = parseInt(year,10);
	wk_mon = parseInt(month,10);
	if ((wk_mon == 2) &&
		((wk_year % 4) == 0 && ((wk_year % 100) != 0 || (wk_year % 400)))) {
		wk_mon = 13;
	}

	days = lastDay[wk_mon-1];
	return days;
}
//-->
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="work_admin_menu.php?session=<? echo($session); ?>"><img src="img/icon/b23.gif" width="32" height="32" border="0" alt="勤務管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="work_admin_menu.php?session=<? echo($session); ?>"><b>勤務管理</b></a></font></td>
</tr>
</table>
<? } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><img src="img/icon/b07.gif" width="32" height="32" border="0" alt="出勤表"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>出勤表</b></a> &gt; <a href="work_admin_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } ?>

<?
// メニュータブ
show_work_admin_menuitem($session, $fname, "");

?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
<tr>
<td>
<?php
//休暇種別メニュータブ表示
show_timecard_hol_menuitem($session,$fname);
?>
</td>
</tr>
<tr>
<td><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<br>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">休暇取得状況</font>

<form name="mainform" action="atdbk_reason_export_csv.php" method="post">
<table width="960" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td width="540" align="left" nowrap>
<?php
// 期間
?>
<select id='start_yr' name='start_yr'><?php 
show_select_years(15, $start_yr, false, true);
?></select>年
<select id='start_mon' name='start_mon'><?php 
show_select_months($start_mon, false);
?></select>月
<select id='start_day' name='start_day'><?php 
show_select_days($start_day, false);
?></select>日
&nbsp;〜&nbsp;
<select id='end_yr' name='end_yr' onchange='set_last_day(1);'><?php 
show_select_years(15, $end_yr, false, true);
?></select>年
<select id='end_mon' name='end_mon' onchange='set_last_day(1);'><?php 
show_select_months($end_mon, false);
?></select>月
<select id='end_day' name='end_day'><?php 
show_select_days($end_day, false);
?></select>日
<!-- 表示ボタン -->
<input type="submit" name="submit" value="CSV出力" onClick="return check_date(1);" /></td>
</tr>
</table>

<input type="hidden" name="session" value="<?php echo($session); ?>">
</form>
</td>
</tr>
</table>
<br>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">年休取得状況</font>
<form name="paidholform" action="atdbk_paid_hol_export_csv.php" method="post">
<table width="960" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td width="540" align="left" nowrap>
<?php
// 期間
?>
<select id='start_yr2' name='start_yr2'><?php 
show_select_years(15, $start_yr, false, true);
?></select>年
<select id='start_mon2' name='start_mon2'><?php 
show_select_months($start_mon, false);
?></select>月
<select id='start_day2' name='start_day2'><?php 
show_select_days($start_day, false);
?></select>日
&nbsp;〜&nbsp;
<select id='end_yr2' name='end_yr2' onchange='set_last_day(2);'><?php 
show_select_years(15, $end_yr, false, true);
?></select>年
<select id='end_mon2' name='end_mon2' onchange='set_last_day(2);'><?php 
show_select_months($end_mon, false);
?></select>月
<select id='end_day2' name='end_day2'><?php 
show_select_days($end_day, false);
?></select>日
<!-- 表示ボタン -->
<input type="submit" name="submit" value="CSV出力" onClick="return check_date(2);" /></td>
</tr>
</table>

<input type="hidden" name="session" value="<?php echo($session); ?>">
</form>
</td>
</tr>
</table>
</body>
<?php pg_close($con); ?>
</html>
