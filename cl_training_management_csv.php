<?
ob_start();
require_once("about_session.php");
require_once("about_authority.php");
require_once("cl_common.ini");
require_once("cl_common_log_class.inc");
require_once("Cmx.php");
require_once("MDB2.php");
ob_end_clean();

$log = new cl_common_log_class(basename(__FILE__));

$log->info(basename(__FILE__)." START");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, $CL_MANAGE_AUTH, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

//##########################################################################
// データベース接続オブジェクト取得
//##########################################################################
$log->debug("MDB2オブジェクト取得開始",__FILE__,__LINE__);
$mdb2 = MDB2::connect(CMX_DB_DSN);
if (PEAR::isError($mdb2)) {
	$log->error("MDB2オブジェクト取得エラー：".$mdb2->getDebugInfo(),__FILE__,__LINE__);
}
$log->debug("MDB2オブジェクト取得終了",__FILE__,__LINE__);

require_once("cl/model/master/cl_mst_inside_training_model.php");
$training_model = new cl_mst_inside_training_model($mdb2,$emp_id);
$log->debug("院内研修用DBA取得",__FILE__,__LINE__);

/**
 * 研修講座一覧取得
 */

// 日付オプション
if ($_POST['date_y_from'] == "-") {
	$date_y_from = "";
} else {
	$date_y_from = $_POST['date_y_from'];
}

if ($_POST['date_m_from'] == "-") {
	$date_m_from = "";
} else {
	$date_m_from = $_POST['date_m_from'];
}

if ($_POST['date_d_from'] == "-") {
	$date_d_from = "";
} else {
	$date_d_from = $_POST['date_d_from'];
}

if ($_POST['date_y_to'] == "-") {
	$date_y_to = "";
} else {
	$date_y_to = $_POST['date_y_to'];
}

if ($_POST['date_m_to'] == "-") {
	$date_m_to = "";
} else {
	$date_m_to = $_POST['date_m_to'];
}

if ($_POST['date_d_to'] == "-") {
	$date_d_to = "";
} else {
	$date_d_to = $_POST['date_d_to'];
}

if (
	$_POST['training_id_search']	== "" &&
	$_POST['training_name']			== "" &&
	$_POST['teacher_name']			== "" &&
	$_POST['level']					== "" &&
	$date_y_from					== "" &&
	$date_m_from					== "" &&
	$date_d_from					== "" &&
	$date_y_to						== "" &&
	$date_m_to						== "" &&
	$date_d_to						== ""
	// 2012/11/16 Yamagawa add(s)
	&&	$_POST['term_div']			== ""
	// 2012/11/16 Yamagawa add(e)
) {
	// 抽出条件無の場合、対象全取得
	$data = $training_model->getManagementList();
	$log->debug("一覧取得：条件無",__FILE__,__LINE__);
} else {
	// 抽出条件有
	if (
		$date_y_from	== "" ||
		$date_m_from	== "" ||
		$date_d_from	== ""
	) {
		$date_from = "";
	} else {
		$date_from = $date_y_from."-".$date_m_from."-".$date_d_from;
	}

	if (
		$date_y_to	== "" ||
		$date_m_to	== "" ||
		$date_d_to	== ""
	) {
		$date_to = "";
	} else {
		$date_to = $date_y_to."-".$date_m_to."-".$date_d_to;
	}

	$param = array(
		'training_id'		=> $_POST['training_id_search'],
		'training_name'		=> $_POST['training_name'],
		'level'				=> $_POST['level'],
		'teacher_name'		=> $_POST['teacher_name'],
		'date_from'			=> $date_from,
		'date_to'			=> $date_to
		// 2012/11/16 Yamagawa add(s)
		,'term_div'			=> $_POST['term_div']
		// 2012/11/16 Yamagawa add(e)
	);
	$data = $training_model->getManagementList_conditional($param);
	$log->debug("一覧取得：条件有",__FILE__,__LINE__);
}

// DB切断
$mdb2->disconnect();

// ヘッダー行
// 項目配列作成
$array_csv_header = array();
$array_csv_header[] = "院内研修番号";
$array_csv_header[] = "研修名";
// 2012/08/23 Yamagawa add(s)
// 2012/11/16 Yamagawa upd(s)
//$array_csv_header[] = "年度";
$array_csv_header[] = "申請期間";
// 2012/11/16 Yamagawa upd(e)
// 2012/08/23 Yamagawa add(e)
$array_csv_header[] = "レベルI";
$array_csv_header[] = "レベルII";
$array_csv_header[] = "レベルIII";
$array_csv_header[] = "レベルIV";
$array_csv_header[] = "レベルV";
$array_csv_header[] = "主催者";

// 文字コード変換
$csv_header = array();
foreach($array_csv_header as $tmp_csv_header){
	$csv_header[] = format_column_for_csv($tmp_csv_header);
}

// 配列から文字列化
$csv_header = implode(",", $csv_header);

$log->debug('■csv_header   :'.$csv_header	,__FILE__,__LINE__);

// 明細行
foreach ($data as $row){
	$csv_body .= format_column_for_csv($row['training_id']);
	$csv_body .= ",". format_column_for_csv($row['training_name']);
	// 2012/08/23 Yamagawa add(s)
	// 2012/11/16 Yamagawa upd(s)
	//$csv_body .= ",". format_column_for_csv($row['year']);
	$csv_body .= ",". format_column_for_csv($row['term_div']);
	// 2012/11/16 Yamagawa upd(e)
	// 2012/08/23 Yamagawa add(e)
	$csv_body .= ",". format_column_for_csv($row['target_level1']);
	$csv_body .= ",". format_column_for_csv($row['target_level2']);
	$csv_body .= ",". format_column_for_csv($row['target_level3']);
	$csv_body .= ",". format_column_for_csv($row['target_level4']);
	$csv_body .= ",". format_column_for_csv($row['target_level5']);
	$csv_body .= ",". format_column_for_csv($row['emp_lt_nm']."　".$row['emp_ft_nm']);
	$csv_body .= "\r\n";
}

$csv = $csv_header . "\r\n" . $csv_body;

$file_name = "inside_training_list.csv";
header("Content-Disposition: attachment; filename=$file_name");
header("Content-Type: application/octet-stream; name=$file_name");
header("Content-Length: " . strlen($csv));
echo($csv);

function format_column_for_csv($value, $from_encoding = "EUC-JP") {
	$buf = str_replace("\r", "", $value);
	$buf = str_replace("\n", "", $buf);
	if (strpos($buf, ",") !== false)  {
		$buf = '"' . $buf . '"';
	}
	return mb_convert_encoding($buf, "Shift_JIS", $from_encoding);
}
?>
