<?
ob_start();

require_once("about_authority.php");
require_once("about_session.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 表を出力
ob_clean();
show_building_status($con, $bldg_cd, $year, $month, $day, $hour, $minute, $session, $fname);

// データベース接続を閉じる
pg_close($con);

// Excelを出力
$excel_data = mb_convert_encoding(ob_get_contents(), "Shift_JIS", 'EUC-JP');
ob_clean();
header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=ward_status.xls");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
header("Pragma: public");
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
</head>
<body>
<? echo($excel_data); ?>
</body>
</html>
<?
ob_end_flush();

// 表を出力
function show_building_status($con, $bldg_cd, $year, $month, $day, $hour, $minute, $session, $fname) {

	// 合計行用の変数を初期化
	$sum_bed_count = 0;
	$sum_in_patient_count = 0;
	$sum_in_res_patient_count = 0;
	$sum_move_in_patient_count = 0;
	$sum_move_out_patient_count = 0;
	$sum_out_res_patient_count = 0;
	$sum_in_res_patient_count_week = 0;
	$sum_out_res_patient_count_week = 0;
	$sum_go_out_count = 0;
	$sum_stop_out_count = 0;
	$sum_move1_count = 0;
	$sum_move2_count = 0;
	$sum_move3_count = 0;
	$sum_family_count = 0;
	$sum_ventilator_count = 0;

	// 病棟一覧を配列に格納
	$sql = "select * from wdmst";
	$cond = "where bldg_cd = $bldg_cd and ward_del_flg = 'f' order by ward_cd";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$wards[$row["ward_cd"]] = array(
			"ward_name" => $row["ward_name"],
			"bed_count" => 0,
			"in_patient_count" => 0,
			"in_res_patient_count" => 0,
			"move_in_patient_count" => 0,
			"move_out_patient_count" => 0,
			"out_res_patient_count" => 0,
			"in_res_patient_count_week" => 0,
			"out_res_patient_count_week" => 0,
			"go_out_count" => 0,
			"stop_out_count" => 0,
			"move1_count" => 0,
			"move2_count" => 0,
			"move3_count" => 0,
			"family_count" => 0,
			"ventilator_count" => 0
		);
	}

	// 病室一覧を取得
	$sql = "select ward_cd, ptrm_room_no, ptrm_name, ptrm_bed_cur from ptrmmst";
	$cond = "where bldg_cd = $bldg_cd and ptrm_del_flg = 'f' order by ptrm_room_no";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$tmp_ward_cd = $row["ward_cd"];
		$tmp_bed_count = intval($row["ptrm_bed_cur"]);

		$wards[$tmp_ward_cd]["bed_count"] += $tmp_bed_count;
		$sum_bed_count += $tmp_bed_count;

		$wards[$tmp_ward_cd]["rooms"][$row["ptrm_room_no"]] = array(
			"name" => $row["ptrm_name"],
			"bed_count" => $tmp_bed_count,
			"male" => 0,
			"female" => 0,
			"unknown" => 0
		);
	}

	// 指定日に在院していた患者一覧を取得
	$sql = " select i.ptif_id, i.inpt_in_dt, i.inpt_in_tm, i.inpt_out_dt, i.inpt_out_tm, i.bldg_cd, i.ward_cd, i.ptrm_room_no, ptifmst.ptif_sex from inptmst i inner join ptifmst on ptifmst.ptif_id = i.ptif_id where i.inpt_in_dt is not null and ((i.inpt_in_dt = '$year$month$day' and i.inpt_in_tm <= '$hour$minute') or (i.inpt_in_dt < '$year$month$day')) ";
	$sql .= " union select i.ptif_id, i.inpt_in_dt, i.inpt_in_tm, i.inpt_out_dt, i.inpt_out_tm, i.bldg_cd, i.ward_cd, i.ptrm_room_no, ptifmst.ptif_sex from inpthist i inner join ptifmst on ptifmst.ptif_id = i.ptif_id where ((i.inpt_in_dt = '$year$month$day' and i.inpt_in_tm <= '$hour$minute') or (i.inpt_in_dt < '$year$month$day')) and ((i.inpt_out_dt = '$year$month$day' and i.inpt_out_tm >= '$hour$minute') or (i.inpt_out_dt > '$year$month$day')) ";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// 患者一覧をループ
	while ($row = pg_fetch_array($sel)) {
		$tmp_ptif_id = $row["ptif_id"];
		$tmp_in_dt = $row["inpt_in_dt"];
		$tmp_in_tm = $row["inpt_in_tm"];
		$tmp_out_dt = $row["inpt_out_dt"];
		$tmp_out_tm = $row["inpt_out_tm"];
		$tmp_ptif_sex = $row["ptif_sex"];

		// 当該患者の指定日直近の確定済み移転情報を取得
		$sql = "select * from inptmove";
		$cond = "where ptif_id = '$tmp_ptif_id' and move_cfm_flg = 't' and ((move_dt = '$tmp_in_dt' and move_tm >= '$tmp_in_tm') or (move_dt > '$tmp_in_dt')) and ((move_dt = '$year$month$day' and move_tm >= '$hour$minute') or (move_dt > '$year$month$day'))";
		if ($tmp_out_dt != "" && $tmp_out_tm != "") {
			$cond .= " and ((move_dt = '$tmp_out_dt' and move_tm < '$tmp_out_tm') or (move_dt < '$tmp_out_dt'))";
		}
		$cond .= " order by move_dt, move_tm limit 1";
		$sel2 = select_from_table($con, $sql, $cond, $fname);
		if ($sel2 == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 病室を確定
		if (pg_num_rows($sel2) == 0) {
			$tmp_bldg_cd = $row["bldg_cd"];
			$tmp_ward_cd = $row["ward_cd"];
			$tmp_ptrm_room_no = $row["ptrm_room_no"];
		} else {
			$tmp_bldg_cd = pg_fetch_result($sel2, 0, "from_bldg_cd");
			$tmp_ward_cd = pg_fetch_result($sel2, 0, "from_ward_cd");
			$tmp_ptrm_room_no = pg_fetch_result($sel2, 0, "from_ptrm_room_no");
		}

		// 棟が違う場合は次の患者へ
		if ($tmp_bldg_cd != $bldg_cd) {
			continue;
		}

		// 在院患者数をカウント
		$wards[$tmp_ward_cd]["in_patient_count"]++;
		$sum_in_patient_count++;
		switch ($tmp_ptif_sex) {
		case "1":
			$wards[$tmp_ward_cd]["rooms"][$tmp_ptrm_room_no]["male"]++;
			break;
		case "2":
			$wards[$tmp_ward_cd]["rooms"][$tmp_ptrm_room_no]["female"]++;
			break;
		default:
			$wards[$tmp_ward_cd]["rooms"][$tmp_ptrm_room_no]["unknown"]++;
			break;
		}

		// 外泊情報を取得
		$sql = "select count(*) from inptgoout";
		$cond = "where ptif_id = '$tmp_ptif_id' and out_type = '2' and out_date <= '$year$month$day' and ret_date >= '$year$month$day'";
		$sel2 = select_from_table($con, $sql, $cond, $fname);
		if ($sel2 == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$tmp_go_out_count = intval(pg_fetch_result($sel2, 0, 0));
		$wards[$tmp_ward_cd]["go_out_count"] += $tmp_go_out_count;
		$sum_go_out_count += $tmp_go_out_count;

		// 外出情報を取得
		$sql = "select count(*) from inptgoout";
		$cond = "where ptif_id = '$tmp_ptif_id' and out_type = '1' and out_date = '$year$month$day' and out_time <= '$hour$minute' and ret_time >= '$hour$minute'";
		$sel2 = select_from_table($con, $sql, $cond, $fname);
		if ($sel2 == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$tmp_stop_out_count = intval(pg_fetch_result($sel2, 0, 0));
		$wards[$tmp_ward_cd]["stop_out_count"] += $tmp_stop_out_count;
		$sum_stop_out_count += $tmp_stop_out_count;

		// 移動手段情報・人工呼吸器情報を取得
		$sql = "select ptsubif_move_flg, ptsubif_move1, ptsubif_move2, ptsubif_move3, ptsubif_move4, ptsubif_move5, ptsubif_ventilator from ptsubif";
		$cond = "where ptif_id = '$tmp_ptif_id'";
		$sel2 = select_from_table($con, $sql, $cond, $fname);
		if ($sel2 == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		if (pg_num_rows($sel2) > 0) {
			if (pg_fetch_result($sel2, 0, "ptsubif_move5") == "t") {
				$wards[$tmp_ward_cd]["move1_count"]++;
				$sum_move1_count++;
			} else if (pg_fetch_result($sel2, 0, "ptsubif_move1") == "t" ||
			           pg_fetch_result($sel2, 0, "ptsubif_move2") == "t" ||
			           pg_fetch_result($sel2, 0, "ptsubif_move3") == "t" ||
			           pg_fetch_result($sel2, 0, "ptsubif_move4") == "t") {
				$wards[$tmp_ward_cd]["move2_count"]++;
				$sum_move2_count++;
			} else if (pg_fetch_result($sel2, 0, "ptsubif_move_flg") == "t") {
				$wards[$tmp_ward_cd]["move3_count"]++;
				$sum_move3_count++;
			}

			if (pg_fetch_result($sel2, 0, "ptsubif_ventilator") == "t") {
				$wards[$tmp_ward_cd]["ventilator_count"]++;
				$sum_ventilator_count++;
			}
		}

		// 家族付き添い情報を取得
		$sql = "select sum(head_count) from inptfamily";
		$cond = "where ptif_id = '$tmp_ptif_id' and from_date <= '$year$month$day' and to_date >= '$year$month$day'";
		$sel2 = select_from_table($con, $sql, $cond, $fname);
		if ($sel2 == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$tmp_family_count = intval(pg_fetch_result($sel2, 0, 0));
		$wards[$tmp_ward_cd]["family_count"] += $tmp_family_count;
		$sum_family_count += $tmp_family_count;
	}

	// 入院予定患者数を取得
	$six_days_after = date("Ymd", strtotime("+6 days", mktime(0, 0, 0, $month, $day, $year)));
	$sql = "select ward_cd, inpt_in_res_dt, count(*) as in_res_patient_count from inptres";
	$cond = "where bldg_cd = $bldg_cd and inpt_in_res_dt between '$year$month$day' and '$six_days_after' group by ward_cd, inpt_in_res_dt";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$tmp_ward_cd = $row["ward_cd"];
		$tmp_in_res_patient_count = intval($row["in_res_patient_count"]);

		if ($row["inpt_in_res_dt"] == "$year$month$day") {
			$wards[$tmp_ward_cd]["in_res_patient_count"] = $tmp_in_res_patient_count;
			$sum_in_res_patient_count += $tmp_in_res_patient_count;
		}

		$wards[$tmp_ward_cd]["in_res_patient_count_week"] += $tmp_in_res_patient_count;
		$sum_in_res_patient_count_week += $tmp_in_res_patient_count;
	}

	// 転入予定患者数を取得
	$sql = "select to_ward_cd, count(*) as move_in_patient_count from inptmove";
	$cond = "where to_bldg_cd = $bldg_cd and move_dt = '$year$month$day' and move_cfm_flg = 'f' and move_del_flg = 'f' and (to_bldg_cd <> from_bldg_cd or to_ward_cd <> from_ward_cd) group by to_ward_cd";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$tmp_ward_cd = $row["to_ward_cd"];
		$tmp_move_in_patient_count = intval($row["move_in_patient_count"]);

		$wards[$tmp_ward_cd]["move_in_patient_count"] = $tmp_move_in_patient_count;
		$sum_move_in_patient_count += $tmp_move_in_patient_count;
	}

	// 転出予定患者数を取得
	$sql = "select from_ward_cd, count(*) as move_out_patient_count from inptmove";
	$cond = "where from_bldg_cd = $bldg_cd and move_dt = '$year$month$day' and move_cfm_flg = 'f' and move_del_flg = 'f' and (to_bldg_cd <> from_bldg_cd or to_ward_cd <> from_ward_cd) group by from_ward_cd";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$tmp_ward_cd = $row["from_ward_cd"];
		$tmp_move_out_patient_count = intval($row["move_out_patient_count"]);

		$wards[$tmp_ward_cd]["move_out_patient_count"] = $tmp_move_out_patient_count;
		$sum_move_out_patient_count += $tmp_move_out_patient_count;
	}

	// 退院予定患者数を取得
	$sql = "select ward_cd, inpt_out_res_dt, count(*) as out_res_patient_count from inptmst";
	$cond = "where bldg_cd = $bldg_cd and inpt_out_res_dt between '$year$month$day' and '$six_days_after' group by ward_cd, inpt_out_res_dt";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$tmp_ward_cd = $row["ward_cd"];
		$tmp_out_res_patient_count = intval($row["out_res_patient_count"]);

		if ($row["inpt_out_res_dt"] == "$year$month$day") {
			$wards[$tmp_ward_cd]["out_res_patient_count"] = $tmp_out_res_patient_count;
			$sum_out_res_patient_count += $tmp_out_res_patient_count;
		}

		$wards[$tmp_ward_cd]["out_res_patient_count_week"] += $tmp_out_res_patient_count;
		$sum_out_res_patient_count_week += $tmp_out_res_patient_count;
	}

	// 状況一覧表を表示
?>
<table border="1">
<tr align="center" valign="top">
<th rowspan="2">病棟</th>
<th rowspan="2">在院患者</th>
<th rowspan="2">入院予定</th>
<th colspan="2">転棟予定</th>
<th rowspan="2">退院予定</th>
<th rowspan="2" colspan="3">空床（3人部屋以上）</th>
<th colspan="2">一週間以内予定</th>
<th rowspan="2">個室</th>
<th rowspan="2">2人部屋</th>
<th rowspan="2">外泊</th>
<th rowspan="2">外出</th>
<th rowspan="2">担送</th>
<th rowspan="2">護送</th>
<th rowspan="2">独歩</th>
<th rowspan="2">家族</th>
<th rowspan="2">人工R器</th>
<th rowspan="2">状況他</th>
</tr>
<tr align="center" valign="top">
<th>転入</td>
<th>転出</td>
<th>入院</th>
<th>退院</th>
</tr>
<?
	foreach ($wards as $tmp_ward_cd => $tmp_ward) {
		$tmp_ward_name = $tmp_ward["ward_name"];
		$tmp_bed_count = $tmp_ward["bed_count"];
		$tmp_in_patient_count = $tmp_ward["in_patient_count"];
		$tmp_in_res_patient_count = $tmp_ward["in_res_patient_count"];
		$tmp_move_in_patient_count = $tmp_ward["move_in_patient_count"];
		$tmp_move_out_patient_count = $tmp_ward["move_out_patient_count"];
		$tmp_out_res_patient_count = $tmp_ward["out_res_patient_count"];
		$tmp_in_res_patient_count_week = $tmp_ward["in_res_patient_count_week"];
		$tmp_out_res_patient_count_week = $tmp_ward["out_res_patient_count_week"];
		$tmp_go_out_count = $tmp_ward["go_out_count"];
		$tmp_stop_out_count = $tmp_ward["stop_out_count"];
		$tmp_move1_count = $tmp_ward["move1_count"];
		$tmp_move2_count = $tmp_ward["move2_count"];
		$tmp_move3_count = $tmp_ward["move3_count"];
		$tmp_family_count = $tmp_ward["family_count"];
		$tmp_ventilator_count = $tmp_ward["ventilator_count"];

		// 空床リストの作成
		$tmp_male_rooms = array();
		$tmp_female_rooms = array();
		$tmp_sum_male_bed_count = 0;
		$tmp_sum_female_bed_count = 0;
		$tmp_single_rooms = array();
		$tmp_double_rooms = array();
		foreach ($tmp_ward["rooms"] as $tmp_ptrm_room_no => $tmp_room) {
			$tmp_room_bed_count = $tmp_room["bed_count"];
			$tmp_room_male_count = $tmp_room["male"];
			$tmp_room_female_count = $tmp_room["female"];

			$tmp_vacant_bed_count = $tmp_room_bed_count
			                      - $tmp_room_male_count
			                      - $tmp_room_female_count
			                      - $tmp_room["unknown"];
			if ($tmp_vacant_bed_count == 0) {
				continue;
			}

			// 個室の場合
			if ($tmp_room_bed_count == 1) {
				$tmp_single_rooms[] = $tmp_room["name"];

			// 2人部屋の場合
			} else if ($tmp_room_bed_count == 2) {
				if ($tmp_room_male_count == 1) {
					$tmp_room_sex = "male";
				} else if ($tmp_room_female_count == 1) {
					$tmp_room_sex = "female";
				} else {
					$tmp_room_sex = "";
				}

				$tmp_double_rooms[] = array(
					"name" => $tmp_room["name"],
					"sex" => $tmp_room_sex
				);

			// 3人部屋以上の場合
			} else if ($tmp_room_bed_count >= 3) {

				// 男部屋の場合
				if ($tmp_room_male_count >= $tmp_room_female_count) {
					$tmp_male_rooms[] = array(
						"name" => $tmp_room["name"],
						"bed_count" => $tmp_vacant_bed_count,
						"mixed" => ($tmp_room_male_count > 0 && $tmp_room_female_count > 0)
					);
					$tmp_sum_male_bed_count += $tmp_vacant_bed_count;

				// 女部屋の場合
				} else {
					$tmp_female_rooms[] = array(
						"name" => $tmp_room["name"],
						"bed_count" => $tmp_vacant_bed_count,
						"mixed" => ($tmp_room_male_count > 0 && $tmp_room_female_count > 0)
					);
					$tmp_sum_female_bed_count += $tmp_vacant_bed_count;
				}
			}
		}
?>
<tr align="center" valign="top">
<td rowspan="2"><? echo($tmp_ward_name); ?><br>&nbsp;(<? echo($tmp_bed_count); ?>)&nbsp;</td>
<td rowspan="2"><? echo($tmp_in_patient_count); ?></td>
<td rowspan="2"><? echo($tmp_in_res_patient_count); ?></td>
<td rowspan="2"><? echo($tmp_move_in_patient_count); ?></td>
<td rowspan="2"><? echo($tmp_move_out_patient_count); ?></td>
<td rowspan="2"><? echo($tmp_out_res_patient_count); ?></td>
<td>男</td>
<td><? echo($tmp_sum_male_bed_count); ?></td>
<td>
<?
		foreach ($tmp_male_rooms as $tmp_room) {
			if ($tmp_room["mixed"]) {
				echo("<font color=\"red\">");
			}
			echo("{$tmp_room["name"]}({$tmp_room["bed_count"]})<br>");
			if ($tmp_room["mixed"]) {
				echo("</font>");
			}
		}
?>
</td>
<td rowspan="2"><? echo($tmp_in_res_patient_count_week); ?></td>
<td rowspan="2"><? echo($tmp_out_res_patient_count_week); ?></td>
<td rowspan="2">
<?
		foreach ($tmp_single_rooms as $tmp_room_name) {
			echo("$tmp_room_name<br>");
		}
?>
</td>
<td rowspan="2">
<?
		foreach ($tmp_double_rooms as $tmp_room) {
			echo("{$tmp_room["name"]}<br>");
			if ($tmp_room["sex"] == "male") {
				echo("(男1)");
			} else if ($tmp_room["sex"] == "female") {
				echo("(女1)");
			}
		}
?>
</td>
<td rowspan="2"><? echo($tmp_go_out_count); ?></td>
<td rowspan="2"><? echo($tmp_stop_out_count); ?></td>
<td rowspan="2"><? echo($tmp_move1_count); ?></td>
<td rowspan="2"><? echo($tmp_move2_count); ?></td>
<td rowspan="2"><? echo($tmp_move3_count); ?></td>
<td rowspan="2"><? echo($tmp_family_count); ?></td>
<td rowspan="2"><? echo($tmp_ventilator_count); ?></td>
<td rowspan="2"></td>
</tr>
<tr align="center" valign="top">
<td>女</td>
<td><? echo($tmp_sum_female_bed_count); ?></td>
<td>
<?
		foreach ($tmp_female_rooms as $tmp_room) {
			if ($tmp_room["mixed"]) {
				echo("<font color=\"red\">");
			}
			echo("{$tmp_room["name"]}({$tmp_room["bed_count"]})<br>");
			if ($tmp_room["mixed"]) {
				echo("</font>");
			}
		}
?>
</td>
</tr>
<?
	}
?>
<tr align="center" valign="top">
<td>合計<br>&nbsp;(<? echo($sum_bed_count); ?>)&nbsp;</td>
<td><? echo($sum_in_patient_count); ?></td>
<td><? echo($sum_in_res_patient_count); ?></td>
<td><? echo($sum_move_in_patient_count); ?></td>
<td><? echo($sum_move_out_patient_count); ?></td>
<td><? echo($sum_out_res_patient_count); ?></td>
<td colspan="3"></td>
<td><? echo($sum_in_res_patient_count_week); ?></td>
<td><? echo($sum_out_res_patient_count_week); ?></td>
<td></td>
<td></td>
<td><? echo($sum_go_out_count); ?></td>
<td><? echo($sum_stop_out_count); ?></td>
<td><? echo($sum_move1_count); ?></td>
<td><? echo($sum_move2_count); ?></td>
<td><? echo($sum_move3_count); ?></td>
<td><? echo($sum_family_count); ?></td>
<td><? echo($sum_ventilator_count); ?></td>
<td></td>
</tr>
</table>
<?
}
?>
