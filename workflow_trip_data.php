<?php
// 出張旅費
// CSV出力画面
require_once("about_comedix.php");
require_once("workflow_common.ini");
require_once("referer_common.ini");
require_once("show_select_values.ini");
require_once("show_workflow_all_apply_list.ini");
require_once("application_workflow_select_box.ini");
require_once("application_workflow_common_class.php");
require_once("yui_calendar_util.ini");
require_once('Cmx/Model/SystemConfig.php');

$fname = $_SERVER["PHP_SELF"];
$workflow = @$_REQUEST["workflow"];

// セッションと権限のチェック
$session = qualify_session($_REQUEST["session"], $fname);
$checkauth = check_authority($session, 3, $fname);
if ($session == "0" or $checkauth == "0") {
    js_login_exit();
}

// データベースに接続
$con = connect2db($fname);

// 遷移元の設定
if ($workflow != "") {
    set_referer($con, $session, "workflow", $workflow, $fname);
    $referer = $workflow;
} else {
    $referer = get_referer($con, $session, "workflow", $fname);
}

$conf = new Cmx_SystemConfig();
$title_label = ($conf->get('apply.view.title_label') == "2") ? "標題" : "表題";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ワークフロー | 出張旅費 | 精算CSV出力</title>
<script type="text/javascript" src="js/yui_2.8.1/build/yahoo-dom-event/yahoo-dom-event.js"></script>
<script type="text/javascript" src="js/yui_2.8.1/build/dragdrop/dragdrop-min.js"></script>
<script type="text/javascript" src="js/yui_2.8.1/build/animation/animation-min.js"></script>
<script type="text/javascript" src="js/yui_2.8.1/build/element/element-min.js"></script>
<script type="text/javascript" src="js/yui_2.8.1/build/calendar/calendar-min.js"></script>
<script type="text/javascript" src="js/yui_2.8.1/DDTableRow.js"></script>
<script type="text/javascript" src="js/workflow/trip_data.js"></script>
<link rel="stylesheet" type="text/css" href="js/yui_2.8.1/build/calendar/assets/calendar.css">
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/workflow/workflow.css">
<link rel="stylesheet" type="text/css" href="css/workflow/trip.css">
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<? if ($referer == "2") { ?>
<table id="header">
<tr>
<td class="icon"><a href="workflow_menu.php?session=<? echo($session); ?>"><img src="img/icon/b24.gif" width="32" height="32" border="0" alt="ワークフロー"></a></td>
<td><a href="workflow_menu.php?session=<?=$session?>"><b>ワークフロー</b></a></td>
</tr>
</table>
<? } else { ?>
<table id="header">
<tr>
<td class="icon"><a href="application_menu.php?session=<? echo($session); ?>"><img src="img/icon/b08.gif" width="32" height="32" border="0" alt="決裁・申請"></a></td>
<td>
    <a href="application_menu.php?session=<? echo($session); ?>"><b>決裁・申請</b></a> &gt;
    <a href="workflow_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a> &gt;
    <a href="workflow_trip_seisan.php?session=<? echo($session); ?>"><b>出張旅費</b></a>
</td>
<td class="kanri"><a href="application_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></td>
</tr>
</table>
<? } ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td valign="top" width="120"><?show_wkfw_sidemenu($session);?></td>
<td width="5"><img src="img/spacer.gif" width="5" height="2" alt=""></td>
<td valign="top">

<?
show_wkfw_menuitem($session, $fname, "");
?>

<form action="workflow_trip_data_csv.php" method="post">
<input type="hidden" name="session" value="<?=$session?>" />
<table class="searchtable">
<tr>
    <td class="title">期間</td>
    <td>
        <table>
            <tr>
                <td>
                    <select id="date_y1" name="date_y1"><? show_select_years(10, date('Y'), true); ?></select>/
                    <select id="date_m1" name="date_m1"><? show_select_months(date('m'), true); ?></select>/
                    <select id="date_d1" name="date_d1"><? show_select_days(date('d'), true); ?></select>
                </td>
                <td>
                    <img src="img/calendar_link.gif" id="cal1_icon" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" /><br>
                    <div id="cal1Container" style="position:absolute;display:none;z-index:10000;"></div>
                </td>
                <td>
                    &nbsp;〜
                    <select id="date_y2" name="date_y2"><? show_select_years(10, date('Y'), true); ?></select>/
                    <select id="date_m2" name="date_m2"><? show_select_months(date('m'), true); ?></select>/
                    <select id="date_d2" name="date_d2"><? show_select_days(date('d'), true); ?></select>
                </td>
                <td>
                    <img src="img/calendar_link.gif" id="cal2_icon" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" /><br>
                    <div id="cal2Container" style="position:absolute;display:none;z-index:10000;"></div>
                </td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td class="title">所属</td>
    <td><?show_post_select_box($con, $fname, "", "", "", "");?></td>
</tr>
<tr>
    <td class="title">申請状況</td>
    <td>
        <select name="search_apply_stat">
            <option value="-">すべて</option>
            <option value="0">未承認</option>
            <option value="1" selected>承認確定</option>
            <option value="2">否認</option>
            <option value="3">差戻し</option>
        </select>
    </td>
</tr>
<tr>
    <td class="title">出力するCSV</td>
    <td>
        <label><input id="type1" type="radio" name="type" value="1" />出張・自己研修届</label>
        <label><input id="type2" type="radio" name="type" value="2" />出張報告書</label>
    </td>
</tr>
<tr>
    <td class="title">出力する項目<br /><br /><button id="allcheck" type="button">全部OFF</button></td>
    <td>
        <label><input checked type="checkbox" name="item[]" value="apply_no" />申請番号</label><br />
        <label><input checked type="checkbox" name="item[]" value="wkfw_title" />申請書名</label><br />
        <label><input checked type="checkbox" name="item[]" value="apply_title" /><? echo($title_label); ?></label><br />
        <label><input checked type="checkbox" name="item[]" value="emp_id" />職員ID</label><br />
        <label><input checked type="checkbox" name="item[]" value="emp_name" />職員名</label><br />
        <label><input checked type="checkbox" name="item[]" value="date" />申請日</label><br />
        <label><input checked type="checkbox" name="item[]" value="status" />申請状況</label><br />
        <label><input checked type="checkbox" name="item[]" value="emp_dept" />所属</label><br />
        <label><input checked type="checkbox" name="item[]" value="class_id" />部署1コード</label><br />
        <label><input checked type="checkbox" name="item[]" value="atrb_id" />部署2コード</label><br />
        <label><input checked type="checkbox" name="item[]" value="dept_id" />部署3コード</label><br />
        <label><input checked type="checkbox" name="item[]" value="comment" />承認者コメント</label><br />

        <div id="type1check">
        <label><input checked type="checkbox" name="item1[]" value="kubun" />種別</label><br />
        <label><input checked type="checkbox" name="item1[]" value="t1_start_date" />出張期間(開始)</label><br />
        <label><input checked type="checkbox" name="item1[]" value="t1_end_date" />出張期間(終了)</label><br />
        <label><input checked type="checkbox" name="item1[]" value="t1_kikan" />出張期間(日数)</label><br />
        <label><input checked type="checkbox" name="item1[]" value="pref" />出張先(都道府県)</label><br />
        <label><input checked type="checkbox" name="item1[]" value="city" />出張先(市区町村)</label><br />
        <label><input checked type="checkbox" name="item1[]" value="place" />出張先(出張場所)</label><br />
        <label><input checked type="checkbox" name="item1[]" value="youken" />出張要件</label><br />
        <label><input checked type="checkbox" name="item1[]" value="gakkai_name" />学会名</label><br />
        <label><input checked type="checkbox" name="item1[]" value="gakkai_organizer" />主催者</label><br />
        <label><input checked type="checkbox" name="item1[]" value="gakkai_start_date" />学会期間(開始)</label><br />
        <label><input checked type="checkbox" name="item1[]" value="gakkai_end_date" />学会期間(終了)</label><br />
        <label><input checked type="checkbox" name="item1[]" value="presentation" />演題発表</label><br />
        <label><input checked type="checkbox" name="item1[]" value="act" />主演/客演</label><br />
        <label><input checked type="checkbox" name="item1[]" value="presentation_titie" />演題名</label><br />
        <label><input checked type="checkbox" name="item1[]" value="gakkai_igai" />学会以外</label><br />
        <label><input checked type="checkbox" name="item1[]" value="transit1" />公共交通機関</label><br />
        <label><input checked type="checkbox" name="item1[]" value="transit2" />社用車</label><br />
        <label><input checked type="checkbox" name="item1[]" value="transit3" />タクシー</label><br />
        <label><input checked type="checkbox" name="item1[]" value="transit4" />航空機</label><br />
        <label><input checked type="checkbox" name="item1[]" value="transit5" />ビジネスパック</label><br />
        <label><input checked type="checkbox" name="item1[]" value="transit6" />私有車</label><br />
        <label><input checked type="checkbox" name="item1[]" value="reason_transit6" />私有車使用理由</label><br />
        <label><input checked type="checkbox" name="item1[]" value="sharei" />謝礼</label><br />
        <label><input checked type="checkbox" name="item1[]" value="t1_prepayment" />前渡金</label><br />
        <label><input checked type="checkbox" name="item1[]" value="prepayment_value" />前渡金額</label><br />
        <label><input checked type="checkbox" name="item1[]" value="preticket" />前渡チケット</label><br />
        <label><input checked type="checkbox" name="item1[]" value="preticket21" />新幹線</label><br />
        <label><input checked type="checkbox" name="item1[]" value="preticket22" />国内航空券</label><br />
        <label><input checked type="checkbox" name="item1[]" value="station1_from" />新幹線行き：発</label><br />
        <label><input checked type="checkbox" name="item1[]" value="station1_to" />新幹線行き：着</label><br />
        <label><input checked type="checkbox" name="item1[]" value="station1_shitei" />新幹線行き：列車指定</label><br />
        <label><input checked type="checkbox" name="item1[]" value="station2_from" />新幹線帰り：発</label><br />
        <label><input checked type="checkbox" name="item1[]" value="station2_to" />新幹線帰り：着</label><br />
        <label><input checked type="checkbox" name="item1[]" value="station2_shitei" />新幹線帰り：列車指定</label><br />
        <label><input checked type="checkbox" name="item1[]" value="airport1_from" />航空機行き：発</label><br />
        <label><input checked type="checkbox" name="item1[]" value="airport1_to" />航空機行き：着</label><br />
        <label><input checked type="checkbox" name="item1[]" value="airport1_shitei" />航空機行き：便指定</label><br />
        <label><input checked type="checkbox" name="item1[]" value="airport2_from" />航空機帰り：発</label><br />
        <label><input checked type="checkbox" name="item1[]" value="airport2_to" />航空機帰り：着</label><br />
        <label><input checked type="checkbox" name="item1[]" value="airport2_shitei" />航空機帰り：便指定</label><br />
        <label><input checked type="checkbox" name="item1[]" value="t2_apply_stat" />報告書</label><br />
        </div>
        <div id="type2check">
        <label><input checked type="checkbox" name="item2[]" value="report" />報告内容</label><br />
        <label><input checked type="checkbox" name="item2[]" value="t2_start_date" />出発時間</label><br />
        <label><input checked type="checkbox" name="item2[]" value="t2_end_date" />到着時間</label><br />

        <label><input checked type="checkbox" name="item2[]" value="t2_kikan" />出張実働日数</label><br />
        <label><input checked type="checkbox" name="item2[]" value="t2_kikan2" />出張実働日数 (B)</label><br />

        <label><input checked type="checkbox" name="item2[]" value="transit_flg" />交通費不要</label><br />
        <label><input checked type="checkbox" name="item2[]" value="stay_flg" />宿泊料不要</label><br />

        <label><input checked type="checkbox" name="item2[]" value="stay1" />宿泊地(東京23区内)</label><br />
        <label><input checked type="checkbox" name="item2[]" value="stay1_days" />宿泊日数(東京23区内)</label><br />
        <label><input checked type="checkbox" name="item2[]" value="stay1_days2" />宿泊日数(東京23区内) (B)</label><br />
        <label><input checked type="checkbox" name="item2[]" value="stay2" />宿泊地(東京23区以外)</label><br />
        <label><input checked type="checkbox" name="item2[]" value="stay2_days" />宿泊日数(東京23区以外)</label><br />
        <label><input checked type="checkbox" name="item2[]" value="stay2_days2" />宿泊日数(東京23区以外) (B)</label><br />
        <label><input checked type="checkbox" name="item2[]" value="stay2_city" />宿泊場所(東京23区以外)</label><br />

        <label><input checked type="checkbox" name="item2[]" value="tt1_fee_total" />交通費(公共交通機関)</label><br />
        <label><input checked type="checkbox" name="item2[]" value="tt2_fee_total" />交通費(ビジネスパック)</label><br />
        <label><input checked type="checkbox" name="item2[]" value="tt3_fee_total" />交通費(私有車)</label><br />
        <label><input checked type="checkbox" name="item2[]" value="tt3_reason" />私有車使用理由</label><br />

        <label><input checked type="checkbox" name="item2[]" value="entrance_fee" />参加費</label><br />

        <label><input checked type="checkbox" name="item2[]" value="daily_fee" />日当(単価)</label><br />
        <label><input checked type="checkbox" name="item2[]" value="daily_fee_days" />日当(日数)</label><br />
        <label><input checked type="checkbox" name="item2[]" value="daily_fee_percentage" />日当(割合)</label><br />
        <label><input checked type="checkbox" name="item2[]" value="daily_fee_total" />日当(金額)</label><br />

        <label><input checked type="checkbox" name="item2[]" value="daily_fee2" />日当(単価) (B)</label><br />
        <label><input checked type="checkbox" name="item2[]" value="daily_fee_days2" />日当(日数) (B)</label><br />
        <label><input checked type="checkbox" name="item2[]" value="daily_fee_percentage2" />日当(割合) (B)</label><br />
        <label><input checked type="checkbox" name="item2[]" value="daily_fee_total2" />日当(金額) (B)</label><br />

        <label><input checked type="checkbox" name="item2[]" value="tokyo_fee" />23区宿泊(単価)</label><br />
        <label><input checked type="checkbox" name="item2[]" value="tokyo_fee_days" />23区宿泊(日数)</label><br />
        <label><input checked type="checkbox" name="item2[]" value="tokyo_fee_percentage" />23区宿泊(割合)</label><br />
        <label><input checked type="checkbox" name="item2[]" value="tokyo_fee_total" />23区宿泊(金額)</label><br />

        <label><input checked type="checkbox" name="item2[]" value="tokyo_fee2" />23区宿泊(単価) (B)</label><br />
        <label><input checked type="checkbox" name="item2[]" value="tokyo_fee_days2" />23区宿泊(日数) (B)</label><br />
        <label><input checked type="checkbox" name="item2[]" value="tokyo_fee_percentage2" />23区宿泊(割合) (B)</label><br />
        <label><input checked type="checkbox" name="item2[]" value="tokyo_fee_total2" />23区宿泊(金額) (B)</label><br />

        <label><input checked type="checkbox" name="item2[]" value="hotel_fee" />宿泊(単価)</label><br />
        <label><input checked type="checkbox" name="item2[]" value="hotel_fee_days" />宿泊(日数)</label><br />
        <label><input checked type="checkbox" name="item2[]" value="hotel_fee_percentage" />宿泊(割合)</label><br />
        <label><input checked type="checkbox" name="item2[]" value="hotel_fee_total" />宿泊(金額)</label><br />

        <label><input checked type="checkbox" name="item2[]" value="hotel_fee2" />宿泊(単価) (B)</label><br />
        <label><input checked type="checkbox" name="item2[]" value="hotel_fee_days2" />宿泊(日数) (B)</label><br />
        <label><input checked type="checkbox" name="item2[]" value="hotel_fee_percentage2" />宿泊(割合) (B)</label><br />
        <label><input checked type="checkbox" name="item2[]" value="hotel_fee_total2" />宿泊(金額) (B)</label><br />

        <label><input checked type="checkbox" name="item2[]" value="t2_prepayment" />前渡金</label><br />
        <label><input checked type="checkbox" name="item2[]" value="total_fee" />精算金額</label><br />

        <label><input checked type="checkbox" name="item2[]" value="t1_apply_no" />出張・自己研修届 申請番号</label><br />
        </div>
    </td>
</tr>
</table>

<div class="button"><button type="submit">CSVをダウンロードする</button></div>
</td>
</tr>
</table>
</form>

</td>
</tr>
</table>
</body>
</html>
<?
pg_close($con);