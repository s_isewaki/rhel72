<?
ob_start();

require_once("Cmx.php");
require_once("aclg_set.php");
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// ウェブメール権限のチェック
$checkauth = check_authority($session, 0, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);
// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// ログインユーザのメールアカウントを取得
$sql = "select get_mail_login_id(emp_id) as account from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$account = pg_fetch_result($sel, 0, "account");

// Cyrus使用フラグを取得
$sql = "select use_cyrus from config";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$use_cyrus = pg_fetch_result($sel, 0, "use_cyrus");

// Cyrusを使わない場合、ログインユーザのE-Mailをメールアカウントとして使用
if ($use_cyrus == "f") {
	$sql = "select emp_email2 from empmst";
	$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$account = pg_fetch_result($sel, 0, "emp_email2");
}

// データベース接続を閉じる
pg_close($con);

ob_clean();
?>
<? if (preg_match("/[a-z]/", $account) > 0) { ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Frameset//EN" "http://www.w3.org/TR/REC-html40/frameset.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ウェブメール</title>
</head>
<frameset rows="31,*" border="0" frameborder="0">
<frame src="webmail_title.php?session=<? echo($session); ?>" name="webmail_title" scrolling="no" noresize>
<frame src="webmail_login.php?session=<? echo($session); ?>" name="webmail_body">
</frameset>
</html>
<? } else { ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ウェブメール</title>
<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" valign="middle" class="spacing"><a href="webmail_menu.php?session=<? echo($session); ?>" target="floatingpage"><img src="img/icon/b01.gif" width="32" height="32" border="0" alt="ウェブメール"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="webmail_menu.php?session=<? echo($session); ?>" target="floatingpage"><b>ウェブメール</b></a></font></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<div><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">システム上の制限により、この機能は使用できません。詳しくは管理者にお問い合わせください。</font></div>
</td>
</tr>
</table>
</body>
</html>
<? } ?>
