<?php
require_once("about_comedix.php");
require_once("library_common.php");
require_once('Cmx.php');
require_once('Cmx/Model/SystemConfig.php');

$fname = $_SERVER["PHP_SELF"];

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 20, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// データベースに接続
$con = connect2db($fname);

if ($back != "t") {

    // 環境設定情報を取得
    $sql = "SELECT * FROM config";
    $sel = select_from_table($con, $sql, "", $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $multi_login_alert = pg_fetch_result($sel, 0, "multi_login_alert");
    $weak_pwd_flg = pg_fetch_result($sel, 0, "weak_pwd_flg");
    $pass_expire_days = pg_fetch_result($sel, 0, "pass_expire_days");
    $login_autocomplete = pg_fetch_result($sel, 0, "login_autocomplete");
    $hide_passwd = pg_fetch_result($sel, 0, "hide_passwd");
    $pass_length = pg_fetch_result($sel, 0, "pass_length");
    $popup_news = pg_fetch_result($sel, 0, "popup_news");
    $use_web = pg_fetch_result($sel, 0, "use_web");
    $use_email = pg_fetch_result($sel, 0, "use_email");
    $use_cyrus = pg_fetch_result($sel, 0, "use_cyrus");
    $webmail_from_order1 = pg_fetch_result($sel, 0, "webmail_from_order1");
    $webmail_from_order2 = pg_fetch_result($sel, 0, "webmail_from_order2");
    $webmail_from_order3 = pg_fetch_result($sel, 0, "webmail_from_order3");
    $use_flash_logo = pg_fetch_result($sel, 0, "use_flash_logo");
    $kview_url = pg_fetch_result($sel, 0, "kview_url");
    $user_belong_change = pg_fetch_result($sel, 0, "user_belong_change");
    $user_job_change = pg_fetch_result($sel, 0, "user_job_change");
    $user_st_change = pg_fetch_result($sel, 0, "user_st_change");

    $lib_count_flg = lib_get_count_flg($con, $fname);
    $lib_dragdrop_flg = lib_get_dragdrop_flg($con, $fname);

    // SystemConfig
    $conf = new Cmx_SystemConfig();

    $schedule_alarm = $conf->get('schedule.alarm');
    if (is_null($schedule_alarm)) {
        $schedule_alarm = "0";
    }

    $schedule_alarm_start_h = 0;
    $schedule_alarm_start_m = 10;
    $schedule_alarm_start = $conf->get('schedule.alarm.start');
    if (!is_null($schedule_alarm_start)) {
        $schedule_alarm_start_h = intval($schedule_alarm_start / 60);
        $schedule_alarm_start_m = $schedule_alarm_start % 60;
    }

    $schedule_alarm_end_h = 2;
    $schedule_alarm_end_m = 0;
    $schedule_alarm_end = $conf->get('schedule.alarm.end');
    if (!is_null($schedule_alarm_end)) {
        $schedule_alarm_end_h = intval($schedule_alarm_end / 60);
        $schedule_alarm_end_m = $schedule_alarm_end % 60;
    }

    $security_client_auth = $conf->get('security.client_auth');
    if (is_null($security_client_auth)) {
        $security_client_auth = "0";
    }

    $password_first_time = $conf->get('password.first_time');
    if (is_null($password_first_time)) {
        $password_first_time = "0";
    }

    $timeout_minutes = get_session_expire('min');
    if ($timeout_minutes == 0) {
        $timeout_minutes = "";
    }
}

// カルテビューワーが購入済みかどうかチェック
$sql = "select lcs_func11 from license";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$kview_purchased = (pg_fetch_result($sel, 0, "lcs_func11") == "t");

pg_close($con);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
    <title>CoMedix 環境設定｜その他</title>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/fontsize.js"></script>
    <script type="text/javascript">
    function checkImap() {
        if (document.mainform.use_cyrus[0].checked || document.mainform.use_cyrus[0].disabled) {
            return true;
        }
        return confirm('一度「CoMedix以外のIMAPサーバを使用する」を選択すると、\n今後「CoMedixのCyrusを使用する」に戻せなくなります。\n更新してよろしいですか？');
    }
    </script>
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <style type="text/css">
        .list {border-collapse:collapse; border:#5279a5 solid 1px;}
        .list td {border:#5279a5 solid 1px;}
        .list td td {border-style:none;}
        div.subtitle {
            font-size:11pt;
            font-weight: bold;
            margin-top:15px;
        }
    </style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="config_register.php?session=<?php echo($session); ?>"><img src="img/icon/b28.gif" width="32" height="32" border="0" alt="環境設定"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="config_register.php?session=<?php echo($session); ?>"><b>環境設定</b></a></font></td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom:2px solid #5279a5;">
<tr height="22">
<td width="110" align="center" bgcolor="#bdd1e7"><a href="config_register.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ログイン画面</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="config_sidemenu.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メニュー</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="config_siteid.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">サイトID</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="config_log.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ログ</font></a></td>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#bdd1e7"><a href="config_server_information.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">サーバ情報</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="config_client_auth.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">端末認証履歴</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#5279a5"><a href="config_other.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>その他</b></font></a></td>
<td>&nbsp;</td>
</tr>
</table>

<form name="mainform" action="config_other_update_exe.php" method="post" onsubmit="return checkImap();">

<div class="subtitle">セキュリティ</div>

<table width="1000" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="30">
<td width="30%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">同一職員IDのログイン時</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input name="multi_login_alert" type="radio" value="f"<? if ($multi_login_alert == "f") {echo(" checked");} ?>>警告しない（後のログインを優先）
<input name="multi_login_alert" type="radio" value="t"<? if ($multi_login_alert == "t") {echo(" checked");} ?>>警告する
</font></td>
</tr>
<tr height="30">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ログインIDと同じパスワード</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input name="weak_pwd_flg" type="radio" value="t"<? if ($weak_pwd_flg == "t") {echo(" checked");} ?>>許可する
<input name="weak_pwd_flg" type="radio" value="f"<? if ($weak_pwd_flg == "f") {echo(" checked");} ?>>許可しない
</font></td>
</tr>
<tr height="30">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ログインフォームのオートコンプリート</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input name="login_autocomplete" type="radio" value="f"<? if ($login_autocomplete == "f") {echo(" checked");} ?>>無効にする
<input name="login_autocomplete" type="radio" value="t"<? if ($login_autocomplete == "t") {echo(" checked");} ?>>有効にする
</font></td>
</tr>
<tr height="30">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">パスワード変更期限</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input name="pass_expire_days" type="text" value="<?=$pass_expire_days?>" size="3" maxlength="3">日ごとにパスワード変更する（0日の場合は期限なし）
</font></td>
</tr>
<tr height="30">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員登録画面のパスワード</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input name="hide_passwd" type="radio" value="t"<? if ($hide_passwd == "t") {echo(" checked");} ?>>マスクする（「********」のように表示）
<input name="hide_passwd" type="radio" value="f"<? if ($hide_passwd == "f") {echo(" checked");} ?>>マスクしない<br>
<span style="padding-left:5px;">ライセンスキー番号：<input type="password" name="license_cd" size="20" maxlength="10" style="ime-mode:inactive;"></span><br>
<span style="color:red;padding-left:5px;">※「マスクする」から「マスクしない」に変えるには、ライセンスキー番号が必要です</span>
</font></td>
</tr>
<tr height="30">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">パスワードの長さ</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
最大 <input name="pass_length" type="text" value="<?=!empty($pass_length) ? $pass_length : 20?>" size="3" maxlength="3"> 文字
</font></td>
</tr>
<tr height="30">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">初回のログイン時にパスワードを変更する</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input name="password_first_time" type="radio" value="0" <? if ($password_first_time === "0") echo("checked"); ?>>無効にする
<input name="password_first_time" type="radio" value="1" <? if ($password_first_time === "1") echo("checked"); ?>>有効にする
</font></td>
</tr>
<tr height="30">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">セッションタイムアウト時間</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input name="timeout_minutes" type="text" value="<? eh($timeout_minutes); ?>" size="3" maxlength="3"> 分
<div style="color:red;padding-left:5px;">※タイムアウト不要の場合は空にしてください<br>※設定値に関わらず、ウェブメールは24分でタイムアウトとなります</div>
</font></td>
</tr>
<tr height="30">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一般ユーザによる所属変更</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input name="user_belong_change" type="radio" value="t"<? if ($user_belong_change == "t") {echo(" checked");} ?>>許可する
<input name="user_belong_change" type="radio" value="f"<? if ($user_belong_change == "f") {echo(" checked");} ?>>許可しない
</font></td>
</tr>

<tr height="30">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一般ユーザによる役職変更</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input name="user_st_change" type="radio" value="t"<? if ($user_st_change == "t") {echo(" checked");} ?>>許可する
<input name="user_st_change" type="radio" value="f"<? if ($user_st_change == "f") {echo(" checked");} ?>>許可しない
</font></td>
</tr>

<tr height="30">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一般ユーザによる職種変更</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input name="user_job_change" type="radio" value="t"<? if ($user_job_change == "t") {echo(" checked");} ?>>許可する
<input name="user_job_change" type="radio" value="f"<? if ($user_job_change == "f") {echo(" checked");} ?>>許可しない
</font></td>
</tr>

<tr height="30">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">端末認証キーチェック</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input name="security_client_auth" type="radio" value="0"<? if ($security_client_auth == "0") {echo(" checked");} ?>>無効にする
<input name="security_client_auth" type="radio" value="1"<? if ($security_client_auth == "1") {echo(" checked");} ?>>有効にする
</font></td>
</tr>

</table>

<div class="subtitle">文書管理</div>

<table width="1000" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="30">
<td width="30%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">フォルダ内文書数表示</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input name="lib_count_flg" type="radio" value="t"<? if ($lib_count_flg == "t") {echo(" checked");} ?>>表示する
<input name="lib_count_flg" type="radio" value="f"<? if ($lib_count_flg == "f") {echo(" checked");} ?>>表示しない
</font></td>
</tr>
<tr height="30">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文書のドラッグ＆ドロップ</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input name="lib_dragdrop_flg" type="radio" value="t"<? if ($lib_dragdrop_flg == "t") {echo(" checked");} ?>>有効にする
<input name="lib_dragdrop_flg" type="radio" value="f"<? if ($lib_dragdrop_flg == "f") {echo(" checked");} ?>>無効にする<br>
<span style="color:red;padding-left:5px;">※有効にするとフォルダ数が多くなった場合にレスポンスが悪化する場合があります</span>
</font></td>
</tr>
</table>

<div class="subtitle">ポップアップ表示</div>

<table width="1000" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="30">
<td width="30%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ内容のポップアップ</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input name="popup_news" type="radio" value="t"<? if ($popup_news == "t") {echo(" checked");} ?>>する
<input name="popup_news" type="radio" value="f"<? if ($popup_news == "f") {echo(" checked");} ?>>しない
</font></td>
</tr>
</table>

<div class="subtitle">インターネット接続</div>

<table width="1000" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="30">
<td width="30%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">WEB接続</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input name="use_web" type="radio" value="t"<? if ($use_web == "t") {echo(" checked");} ?>>する
<input name="use_web" type="radio" value="f"<? if ($use_web == "f") {echo(" checked");} ?>>しない
</font></td>
</tr>
<tr height="30">
<td width="30%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">Eメール送受信</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input name="use_email" type="radio" value="t"<? if ($use_email == "t") {echo(" checked");} ?>>する
<input name="use_email" type="radio" value="f"<? if ($use_email == "f") {echo(" checked");} ?>>しない
</font></td>
</tr>
<tr height="30">
<td width="30%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">IMAPサーバ</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input name="use_cyrus" type="radio" value="t"<? if ($use_cyrus == "t") {echo(" checked");} else {echo(" disabled");} ?>>CoMedixのCyrusを使用する
<input name="use_cyrus" type="radio" value="f"<? if ($use_cyrus == "f") {echo(" checked");} ?>>CoMedix以外のIMAPサーバを使用する
</font></td>
</tr>
<tr height="30">
<td width="30%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ウェブメール差出人表示順</font></td>
<td style="padding:4px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
1. <select name="webmail_from_order1"><option value="1"<? if ($webmail_from_order1 == "1") {echo(" selected");} ?>>院内メールアドレス<option value="2"<? if ($webmail_from_order1 == "2") {echo(" selected");} ?>>Eメールアドレス<option value="3"<? if ($webmail_from_order1 == "3") {echo(" selected");} ?>>ウェブメールオプションのメールアドレス</select><br>
2. <select name="webmail_from_order2"><option value="1"<? if ($webmail_from_order2 == "1") {echo(" selected");} ?>>院内メールアドレス<option value="2"<? if ($webmail_from_order2 == "2") {echo(" selected");} ?>>Eメールアドレス<option value="3"<? if ($webmail_from_order2 == "3") {echo(" selected");} ?>>ウェブメールオプションのメールアドレス<option value=""<? if ($webmail_from_order2 == "") {echo(" selected");} ?>>表示しない</select><br>
3. <select name="webmail_from_order3"><option value="1"<? if ($webmail_from_order3 == "1") {echo(" selected");} ?>>院内メールアドレス<option value="2"<? if ($webmail_from_order3 == "2") {echo(" selected");} ?>>Eメールアドレス<option value="3"<? if ($webmail_from_order3 == "3") {echo(" selected");} ?>>ウェブメールオプションのメールアドレス<option value=""<? if ($webmail_from_order3 == "") {echo(" selected");} ?>>表示しない</select><br>
</font></td>
</tr>
</table>

<div class="subtitle">カルテビューワー</div>

<table width="1000" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="30">
<td width="30%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カルテビューワーURL</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input name="kview_url" type="text" value="<?php echo($kview_url); ?>" style="width:80%;"<? if (!$kview_purchased) {echo(" disabled");} ?>>
</font></td>
</tr>
</table>

<div class="subtitle">CMXランチャー</div>

<table width="1000" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="30">
    <td width="30%" align="right" bgcolor="#f6f9ff">
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">スケジュールアラーム機能</font><br /><span style="font-size:11px;">(要V2011-12-01-53以降)</span>
    </td>
    <td>
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
            <label><input type="radio" name="schedule.alarm" value="0" <?php if ($schedule_alarm === "0") {echo 'checked';} ?> onclick="$('#schedule_alarm_start').hide();" />使用しない</label><br />
            <label><input type="radio" name="schedule.alarm" value="1" <?php if ($schedule_alarm === "1") {echo 'checked';} ?> onclick="$('#schedule_alarm_start').show();" />設定時間にポップアップする</label><br />
            <label><input type="radio" name="schedule.alarm" value="2" <?php if ($schedule_alarm === "2") {echo 'checked';} ?> onclick="$('#schedule_alarm_start').hide();" />直近5件をポップアップする</label><br />
        </font>
    </td>
</tr>
<tr height="30" id="schedule_alarm_start" <?php if ($schedule_alarm !== "1") {echo 'style="display:none;"';} ?>>
    <td width="30%" align="right" bgcolor="#f6f9ff">
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">スケジュールアラーム設定可能時間</font>
    </td>
    <td>
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
        <input type="number" name="schedule.alarm.start.h" size="4" value="<?php echo $schedule_alarm_start_h;?>" />時間
        <input type="number" name="schedule.alarm.start.m" size="4" value="<?php echo $schedule_alarm_start_m;?>" />分前から
        <input type="number" name="schedule.alarm.end.h"   size="4" value="<?php echo $schedule_alarm_end_h;?>"   />時間
        <input type="number" name="schedule.alarm.end.m"   size="4" value="<?php echo $schedule_alarm_end_m;?>"   />分前までを
        設定可能時間とする。<br />
        </font>
    </td>
</tr>
</table>


<table width="1000" border="0" cellspacing="0" cellpadding="2">
<tr height="30">
<td align="right"><input type="submit" value="更新"></td>
</tr>
</table>

<input type="hidden" name="session" value="<?php echo($session); ?>">
<input type="hidden" name="use_flash_logo" value="f" /><!-- ロゴのFlash表示:無効にする -->
</form>
</td>
</tr>
</table>
</body>
</html>
