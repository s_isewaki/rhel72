<?php
require_once("Cmx.php");
require_once("Cmx/Model/SystemConfig.php");

// **********このファイルはsessionに関するものです**********
// *****sessionをはじめる*****
function start_session($emp_id, $login_id, $session, $fname, $alert=false) {
    @require("./conf/conf.inf");                                                                        // 環境設定ファイルの読み込み
    require_once("./about_postgres.php");
    if (!defined("CONF_FILE")) {                                                                        // 読み込みの確認
        require("./conf/error.inf");                                                                    // エラーメッセージ一覧ファイルを読み込む
        require_once("./about_error.php");                                                              // エラーメッセージの書き込み処理をする関数のファイルを読み込む
        write_error($fname,$ERRORLOG, $ERROR001);                                                       // error.logへの書き込み

        return 0;                                                                                       // 失敗の場合0を返す
    }

    $con = connect2db($fname);                                                                          // データベースのコネクションを開く

    if ($con == "0") {
        return 0;                                                                                       // 接続失敗の場合は0を返す
    }

    // 同時ログインを許可しない場合
    if (!($login_id == "guest" || $login_id == "guest2" || $_POST['launcher'] == "1" || (defined("MULTIPLEX_LOGIN") && MULTIPLEX_LOGIN))) {
        // 同時ログイン警告フラグを取得
        $sql = "select multi_login_alert from config";
        $cond = "";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $multi_login_alert = pg_fetch_result($sel, 0, "multi_login_alert");

        // 無視してログインしてよければ
        if (!$alert || $multi_login_alert == "f" || $_REQUEST["del_session"] == "1") {
            delete_old_session($con, $emp_id, $session, $fname);

        // 無視してログインしたらだめな場合
        } else {
            $session_expire = get_session_expire();

            // 同じユーザがログイン中かどうかチェック
            $sql = "select session_id from session";
            $cond = "where emp_id = '$emp_id'";
            if ($session_expire > 0) {
                $current = time();
                $expire = $current - $session_expire;
                $cond .= " and session_made between '$expire' and '$current'";
            }
            $cond .= " limit 1";
            $sel = select_from_table($con, $sql, $cond, $fname);
            if ($sel == 0) {
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
            $already_login = (pg_num_rows($sel) > 0);
            unset($sel);

            // 同時ログインの場合
            if ($already_login) {
?>
<body>
<form name="login" action="main_menu.php" method="<?php echo($_SERVER['REQUEST_METHOD']); ?>">
<?php foreach ($_REQUEST as $key => $val) { ?>
<input type="hidden" name="<?php echo(htmlspecialchars($key, ENT_COMPAT, 'EUC-JP')); ?>" value="<?php echo(htmlspecialchars($val, ENT_COMPAT, 'EUC-JP')); ?>">
<?php } ?>
<input type="hidden" name="del_session" value="1">
</form>
<script type="text/javascript">
if (confirm('同じ職員IDで他の画面でログイン中です。\nログインすると、他の画面は、強制的にログアウトされ、編集中のデータは破棄されます。')) {
    document.login.submit();
} else {
    location.href = 'login.php';
}

</script>
</body>
<?php
                exit;
            }
        }
    }

    purge_session($con, $emp_id, $fname);

    $sessionid = register_session($con,$emp_id,$fname);                                                 // 新しいsessionを登録する

    if ($sessionid == "0") {
        return 0;
    } else {
        return $sessionid;
    }
}

// *****hashでsessionidの作成*****
function create_hash_id($fname) {
    //$input = cos(time());                                                                             // 現時刻にコサインをかけている
    //$hash = bin2hex(mhash (MHASH_MD5, $input));                                                       // ハッシュの生成
    $hash = md5(uniqid(rand(), 1));
    $hashlen = strlen($hash);

    if ($hashlen == 0) {
        require("./conf/error.inf");                                                                    // エラーメッセージ一覧ファイルを読み込む
        require_once("./about_error.php");                                                              // エラーメッセージの書き込み処理をする関数のファイルを読み込む
        write_error($fname,$ERRORLOG, $ERROR301);                                                       // error.logへの書き込み

        return 0;                                                                                       // 失敗の場合0を返す
    }
    return $hash;                                                                                       // $hashを返す
}

// *****sessionを保持しているかをチェック*****
function check_session($session) {
    if ($session == "") {                                                                               // sessionがある場合はdbと照らし合わせる
        return 0;                                                                                       // sessionが空の場合0を返す
    } else {
        return 1;
    }                                                                                                   // sessionがある場合1を返す
}

// *****古いsessionが残っている場合には削除する*****
function delete_old_session($con,$userid,$session,$fname) {
    require("./conf/sql.inf");                                                                          // sqlの一覧ファイルを読み込む
    require_once("./about_postgres.php");                                                               // db処理のためのファイルを読み込む

    $cond = " where emp_id = '$userid'";                                                                // sessionとユーザーIDが同じなら削除する
    $checkdb = select_from_table($con,$SESSIONSELECT,$cond,$fname);

    if ($checkdb == "0") {
        return 0;                                                                                       // 不正なアクセスか古いsessionを使用している場合は0を返す
    }

    $rows = check_record($checkdb);                                                                     // select文の結果をカウント、0ならば不正なアクセスを行っている

    if ($rows != 0) {
        $delete_old = delete_from_table($con,$SESSIONDELETE,$cond,$fname);                              // 古いsessionを削除する
    }
    return 1;
}

// *****新しいsessionを作成する*****
function register_session($con,$userid,$fname) {
    require("./conf/sql.inf");                                                                          // sqlの一覧ファイルを読み込む
    require_once("./about_postgres.php");                                                               // db処理のためのファイルを読み込む

    $hashid = create_hash_id($fname);                                                                   // hash値でsessionのIDを作成

    if ($hashid == "0") {                                                                               // hash値の作成に失敗した場合0を返す
        return 0;
    }

    $sessiondate = time();
    $content = array("$hashid","$userid","$sessiondate");                                               // insertするデータを配列にまとめる

    $create = insert_into_table($con,$SESSIONINSERT,$content,$fname);

    if ($create == "0") {
        return 0;                                                                                       // dbにsessionの登録失敗の場合0を返す
    } else {
        return $hashid;                                                                                 // dbにsessionの登録成功の場合$hashidを返す
    }
}

// *****前のページからのsessionのチェックと更新*****
function qualify_session($session,$fname) {
    @require("./conf/conf.inf");                                                                        // 環境設定ファイルの読み込み
    require_once("./about_postgres.php");
    if (!defined("CONF_FILE")) {                                                                        // 読み込みの確認
        require("./conf/error.inf");                                                                    // エラーメッセージ一覧ファイルを読み込む
        require_once("./about_error.php");                                                              // エラーメッセージの書き込み処理をする関数のファイルを読み込む
        write_error($fname,$ERRORLOG, $ERROR001);                                                       // error.logへの書き込み

        return 0;                                                                                       // 失敗の場合0を返す
    }

    $con = connect2db($con);                                                                            // データベースのコネクションを開く

    if ($con == "0") {
        return 0;                                                                                       // 接続失敗の場合は0を返す
    }

    $check = check_session($session);

    if ($check == "0") {
        return 0;
    }

    $new_session = update_old_session($con,$session,$fname);                                            // sessionを更新する

    if ($up == "0") {
        return 0;                                                                                       // 失敗の場合0を返す
    } else {
        return $new_session;                                                                            // 成功の場合1を返す
    }
}

// *****sessionを認証して、有効な場合には新しく更新する*****
function update_old_session($con, $session,$fname) {
    @require("./conf/conf.inf");                                                                        // 環境設定ファイルの読み込み
    require("./conf/sql.inf");                                                                          // sqlの一覧ファイルを読み込む
    require_once("./about_postgres.php");                                                               // db処理のためのファイルを読み込む

    if (!defined("CONF_FILE")) {                                                                        // 読み込みの確認
        require("./conf/error.inf");                                                                    // エラーメッセージ一覧ファイルを読み込む
        require_once("./about_error.php");                                                              // エラーメッセージの書き込み処理をする関数のファイルを読み込む
        write_error($fname,$ERRORLOG, $ERROR001);                                                       // error.logへの書き込み

        return 0;                                                                                       // 失敗の場合0を返す
    }

    $current = time();
    $session_expire = get_session_expire();
    if ($session_expire > 0) {
        $expire   = $current - $session_expire;
        $cond = " where session_made between '$expire' and '$current' and session_id = '$session'";     // 現在時刻とセッション有効期限の間なら有効
    } else {
        $cond = " where session_id = '$session'";                                                       // セッション無期限
    }

    $checkdb = select_from_table($con,$SESSIONSELECT,$cond,$fname);                                     // レコード数を数える

    if ($checkdb == 0) {                                                                                // 正しくselect文が実行されなかった場合0を返す
        return 0;
    }

    $rows = check_record($checkdb);                                                                     // レコード取得できない場合0を返す

    if ($rows != 1) {                                                                                   // レコードが1件以外の場合は0を返す
        return 0;
    }

    $session = pg_result($checkdb,0,"session_id");                                                      // セッションをdbから取得する

    //$set = array("session_id","session_made");                                                        // アップデートするフィールドを設定（session id もかえる場合はこちらを有効にする)
    $set = array("session_made");                                                                       // アップデートするフィールドを設定
    $newsession = create_hash_id($fname);                                                               // 新しいセッションを作成

    if ($newsession =="0") {                                                                            // 新しいセッションが作成されなかった場合0を返す
        return 0;
    }

    //$setvalue = array("$newsession","$current");                                                      // 更新する値を設定（session id もかえる場合はこちらを有効にする)
    $setvalue = array("$current");                                                                      // 更新する値を設定
    $cond = " where session_id = '$session'";                                                           // 更新条件
    $new_session = update_set_table($con,$SESSIONUPDATE,$set,$setvalue,$cond,$fname);                   // 更新作業

    if ($new_session == "0") {
        return 0;                                                                                       // 失敗の場合0を返す
    } else {
        return $session;                                                                                // 成功の場合1を返す
    }
}

function purge_session($con, $emp_id, $fname) {
    $conf = new Cmx_SystemConfig();
    $purge_on_login = $conf->get('session.purge_on_login');
    if (!$purge_on_login) {
        return;
    }

    $sql = "delete from session";
    $cond = "where emp_id = '$emp_id' and session_made <= '" . strtotime("-3 days") . "'";
    delete_from_table($con, $sql, $cond, $fname);
}

function get_session_expire($unit = "sec") {
    $conf = new Cmx_SystemConfig();
    $timeout_minutes = $conf->get('config.timeout_minutes');

    // タイムアウト値がSystemConfigに未移行ならconf.infの値を分単位に丸めて移行
    if (is_null($timeout_minutes)) {
        @require("./conf/conf.inf");
        $timeout_minutes = round(intval($SESSIONEXPIRE) / 60);
        $conf->set('config.timeout_minutes', "$timeout_minutes");
    }

    // デフォルトでは秒単位の値を返す
    if ($unit == "sec") {
        return intval($timeout_minutes) * 60;
    } else {
        return intval($timeout_minutes);
    }
}
