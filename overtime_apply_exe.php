<?
require_once("about_comedix.php");
require_once("show_clock_in_common.ini");
require_once("timecard_common_class.php");
require_once("atdbk_workflow_common_class.php");
require_once("ovtm_class.php"); //20140619

$fname = $PHP_SELF;

if ($session != "") {

	// セッションのチェック
	$session = qualify_session($session, $fname);
	if ($session == "0") {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
		exit;
	}
}

// データベースに接続
$con = connect2db($fname);
$ovtm_class = new ovtm_class($con, $fname); //20140619

$timecard_common_class = new timecard_common_class($con, $fname, $emp_id, $date, $date); //20140619

//入力チェック 
$ovtm_class->check_input(); 

// 残業理由一覧を取得
$sql = "select reason_id, reason, over_no_apply_flag from ovtmrsn";
$cond = "where del_flg = 'f' order by reason_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$arr_over_no_apply_flag = array();
while ($row = pg_fetch_array($sel)) {
	$tmp_reason_id = $row["reason_id"];
	$tmp_reason = $row["reason"];
	$tmp_over_no_apply_flag = $row["over_no_apply_flag"];
	
	$arr_over_no_apply_flag[$tmp_reason_id] = $tmp_over_no_apply_flag;
	
}
//その他はチェックなしで登録
if ($reason_id != "other") {
	// 申請ボタンが押された場合
	if ($no_apply != "y") {
		//理由の確認（不要以外）
		if ($arr_over_no_apply_flag[$reason_id] == "t") {
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('残業申請の場合には、残業申請不要理由以外を選択してください。');</script>");
			echo("<script type=\"text/javascript\">history.back();</script>");
			exit;
		} 
	}
	//申請不要ボタンの場合
	else {
		//理由の申請不要
		if ($arr_over_no_apply_flag[$reason_id] != "t") {
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('残業申請不要の場合には、残業申請不要理由を選択してください。');</script>");
			echo("<script type=\"text/javascript\">history.back();</script>");
			exit;
		} 
	}
}

// 入力チェック
if ($reason_id == "other") {
	if ($reason == "") {
		echo("<script type=\"text/javascript\">alert('理由が入力されていません。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}
	if (strlen($reason) > 200) {
		echo("<script type=\"text/javascript\">alert('理由が長すぎます。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}
	$reason_id = null;
}

$obj = new atdbk_workflow_common_class($con, $fname);

//キーボート入力の場合、前ゼロ対応
if ($ovtm_class->keyboard_input_flg == "t") { 
    
    if ($start_hour != "" && ($start_hour >= "0" && $start_hour <= "9")) {
        $start_hour = sprintf("%02d", $start_hour);
    }
    if ($start_min != "" && ($start_min >= "0" && $start_min <= "9")) {
        $start_min = sprintf("%02d", $start_min);
    }
    if ($end_hour != "" && ($end_hour >= "0" && $end_hour <= "9")) {
        $end_hour = sprintf("%02d", $end_hour);
    }
    if ($end_min != "" && ($end_min >= "0" && $end_min <= "9")) {
        $end_min = sprintf("%02d", $end_min);
    }
    
    for ($i=1; $i<=5; $i++) {
        $idx = ($i == 1) ? "" : $i;
        $s_h = "over_start_hour".$idx;
        $s_m = "over_start_min".$idx;
        $e_h = "over_end_hour".$idx;
        $e_m = "over_end_min".$idx;
        
        if ($$s_h != "" && ($$s_h >= "0" && $$s_h <= "9")) {
            $$s_h = sprintf("%02d", $$s_h);
        }
        if ($$s_m != "" && ($$s_m >= "0" && $$s_m <= "9")) {
            $$s_m = sprintf("%02d", $$s_m);
        }
        if ($$e_h != "" && ($$e_h >= "0" && $$e_h <= "9")) {
            $$e_h = sprintf("%02d", $$e_h);
        }
        if ($$e_m != "" && ($$e_m >= "0" && $$e_m <= "9")) {
            $$e_m = sprintf("%02d", $$e_m);
        }
        $rs_h = "rest_start_hour".$i;
        $rs_m = "rest_start_min".$i;
        $re_h = "rest_end_hour".$i;
        $re_m = "rest_end_min".$i;
        if ($$rs_h != "" && ($$rs_h >= "0" && $$rs_h <= "9")) {
            $$rs_h = sprintf("%02d", $$rs_h);
        }
        if ($$rs_m != "" && ($$rs_m >= "0" && $$rs_m <= "9")) {
            $$rs_m = sprintf("%02d", $$rs_m);
        }
        if ($$re_h != "" && ($$re_h >= "0" && $$re_h <= "9")) {
            $$re_h = sprintf("%02d", $$re_h);
        }
        if ($$re_m != "" && ($$re_m >= "0" && $$re_m <= "9")) {
            $$re_m = sprintf("%02d", $$re_m);
        }
    }
}

// トランザクションを開始
pg_query($con, "begin");

$start_time = ($start_hour == "--") ? "" : "$start_hour$start_min";
$end_time = ($end_hour == "--") ? "" : "$end_hour$end_min";
$over_start_time = ($over_start_hour == "--") ? "" : "$over_start_hour$over_start_min";
$over_end_time = ($over_end_hour == "--") ? "" : "$over_end_hour$over_end_min";
$over_start_time2 = ($over_start_hour2 == "--") ? "" : "$over_start_hour2$over_start_min2";
$over_end_time2 = ($over_end_hour2 == "--") ? "" : "$over_end_hour2$over_end_min2";

$maxline = 5; //20141205

for ($i=3; $i<=$maxline; $i++) {
    $s_t = "over_start_time".$i;
    $s_h = "over_start_hour".$i;
    $s_m = "over_start_min".$i;
    $s_f = "over_start_next_day_flag".$i;
    $$s_t = ($$s_h == "--") ? "" : $$s_h.$$s_m;
    if ($$s_t == "" || $$s_t == null || $$s_f == "" || $$s_f == null) {
        $$s_f = 0;
    }
    $e_t = "over_end_time".$i;
    $e_h = "over_end_hour".$i;
    $e_m = "over_end_min".$i;
    $e_f = "over_end_next_day_flag".$i;
    $$e_t = ($$e_h == "--") ? "" : $$e_h.$$e_m;
    if ($$e_t == "" || $$e_t == null || $$e_f == "" || $$e_f == null) {
        $$e_f = 0;
    }
}    
//残業開始翌日フラグを初期化(0にする)
if ($over_start_time == "" || $over_start_time == null || $over_start_next_day_flag == "" || $over_start_next_day_flag == null){
	$over_start_next_day_flag = 0;
}
//残業終了翌日フラグを初期化(0にする)
if ($over_end_time == "" || $over_end_time == null || $over_end_next_day_flag == "" || $over_end_next_day_flag == null){
	$over_end_next_day_flag = 0;
}
//残業開始2翌日フラグを初期化(0にする)
if ($over_start_time2 == "" || $over_start_time2 == null || $over_start_next_day_flag2 == "" || $over_start_next_day_flag2 == null){
	$over_start_next_day_flag2 = 0;
}
//残業終了2翌日フラグを初期化(0にする)
if ($over_end_time2 == "" || $over_end_time2 == null || $over_end_next_day_flag2 == "" || $over_end_next_day_flag2 == null){
	$over_end_next_day_flag2 = 0;
}

for ($i=1; $i<=$maxline; $i++) {
    $s_t = "rest_start_time".$i;
    $s_h = "rest_start_hour".$i;
    $s_m = "rest_start_min".$i;
    $s_f = "rest_start_next_day_flag".$i;
    $$s_t = ($$s_h == "--") ? "" : $$s_h.$$s_m;
    if ($$s_t == "" || $$s_t == null || $$s_f == "" || $$s_f == null) {
        $$s_f = 0;
    }
    $e_t = "rest_end_time".$i;
    $e_h = "rest_end_hour".$i;
    $e_m = "rest_end_min".$i;
    $e_f = "rest_end_next_day_flag".$i;
    $$e_t = ($$e_h == "--") ? "" : $$e_h.$$e_m;
    if ($$e_t == "" || $$e_t == null || $$e_f == "" || $$e_f == null) {
        $$e_f = 0;
    }
}    

// 申請ボタンが押された場合
if ($no_apply != "y") {

	//残業申請不要データを削除する
	$obj->delete_no_apply_ovtm_data($emp_id, $date);
	//既存データ確認
	$sql = "select apply_id from ovtmapply ";
	$cond = "where emp_id = '$emp_id' and target_date = '$date' and delete_flg = 'f' order by apply_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	
	while($row = pg_fetch_array($sel))
	{
		$ovtm_apply_id = $row["apply_id"];
		
		// 残業申請承認論理削除
		$obj->delete_ovtm_apply_id($ovtm_apply_id);
		
	}
	// 申請IDを採番
	$sql = "select max(apply_id) from ovtmapply";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$apply_id = intval(pg_fetch_result($sel, 0, 0)) + 1;

	// 申請情報を登録

    // 申請確定情報取得
    if($wkfw_approve_num != "")
    {
        $ovtmcfm = $obj->get_apply_confirm_hierarchy($_POST);
    }
    else
    {
        $ovtmcfm = null;
    }
	$sql = "insert into ovtmapply (apply_id, emp_id, target_date, reason_id, reason, apply_time, apply_status, ovtmcfm, delete_flg, end_time, next_day_flag, start_time, previous_day_flag, over_start_time, over_end_time, over_start_next_day_flag, over_end_next_day_flag, reason_detail, over_start_time2, over_end_time2, over_start_next_day_flag2, over_end_next_day_flag2";
    //追加 20140623
    for ($i=3; $i<=$maxline; $i++) {
        $sql .= ", over_start_time".$i;
        $sql .= ", over_end_time".$i;
        $sql .= ", over_start_next_day_flag".$i;
        $sql .= ", over_end_next_day_flag".$i;
    }
    for ($i=1; $i<=$maxline; $i++) {
        $sql .= ", rest_start_time".$i;
        $sql .= ", rest_end_time".$i;
        $sql .= ", rest_start_next_day_flag".$i;
        $sql .= ", rest_end_next_day_flag".$i;
    }
    //追加 20150105
    for ($i=2; $i<=$maxline; $i++) {
        $sql .= ", reason_id".$i;
        $sql .= ", reason".$i;
    }
    $sql .= ") values (";
    $content = array($apply_id, $emp_id, $date, $reason_id, pg_escape_string($reason), date("YmdHis"), '0', $ovtmcfm, 'f', $end_time, $next_day_flag, $start_time, $previous_day_flag, $over_start_time, $over_end_time, $over_start_next_day_flag, $over_end_next_day_flag, pg_escape_string($reason_detail), $over_start_time2, $over_end_time2, $over_start_next_day_flag2, $over_end_next_day_flag2);
    //追加
    for ($i=3; $i<=$maxline; $i++) {
        $s_t = "over_start_time".$i;
        $e_t = "over_end_time".$i;
        $s_f = "over_start_next_day_flag".$i;
        $e_f = "over_end_next_day_flag".$i;
        array_push($content, $$s_t);
        array_push($content, $$e_t);
        array_push($content, $$s_f);
        array_push($content, $$e_f);
    }    
    for ($i=1; $i<=$maxline; $i++) {
        $s_t = "rest_start_time".$i;
        $e_t = "rest_end_time".$i;
        $s_f = "rest_start_next_day_flag".$i;
        $e_f = "rest_end_next_day_flag".$i;
        array_push($content, $$s_t);
        array_push($content, $$e_t);
        array_push($content, $$s_f);
        array_push($content, $$e_f);
    }    
    //追加 20150105
    for ($i=2; $i<=$maxline; $i++) {
        $reason_id_nm = "reason_id".$i;
        $reason_nm = "reason".$i;
        $s_t = "over_start_time".$i;
        if ($$s_t == "") {
			$$reason_id_nm = null;
			$$reason_nm = null;
        }
        else {
			if ($$reason_id_nm == "other") {
				$$reason_id_nm = null;
			}
		}
        array_push($content, $$reason_id_nm);
        array_push($content, pg_escape_string($$reason_nm));
    }
    $ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// 承認登録
	for($i=1; $i<=$approve_num; $i++)
	{
		$varname = "regist_emp_id$i";
		$aprv_emp_id = ($$varname == "") ? null : $$varname;
	
		$varname = "apv_order$i";
		$apv_order = ($$varname == "") ? null : $$varname;
	
		$varname = "apv_sub_order$i";
		$apv_sub_order = ($$varname == "") ? null : $$varname;
	
		$varname = "multi_apv_flg$i";
		$multi_apv_flg = ($$varname == "") ? "f" : $$varname;
	
		$varname = "next_notice_div$i";
		$next_notice_div = ($$varname == "") ? null : $$varname;

        if($aprv_emp_id != "")
        {
			// 役職も登録する
			$sql = "select emp_st from empmst ";
			$cond = "where emp_id = '$aprv_emp_id'";
			$sel = select_from_table($con, $sql, $cond, $fname);
			if ($sel == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			$emp_st = pg_fetch_result($sel, 0, 0);
	
			// 承認者情報を登録
			$sql = "insert into ovtmaprv (apply_id, aprv_no, aprv_emp_id, aprv_status, aprv_sub_no, multi_apv_flg, next_notice_div, emp_st) values (";
			$content = array($apply_id, $apv_order, $aprv_emp_id, '0', $apv_sub_order, $multi_apv_flg, $next_notice_div, $emp_st);
			$ins = insert_into_table($con, $sql, $content, $fname);
			if ($ins == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
        }
	}

	// 承認者候補登録
	for($i=1; $i <= $approve_num; $i++)
	{
		$varname = "apv_order$i";
		$apv_order = $$varname;
	
		$varname = "pst_approve_num$apv_order";
		$pst_approve_num = $$varname;
	
		for($j=1; $j<=$pst_approve_num; $j++)
		{
			$varname = "pst_emp_id$apv_order";
			$varname .= "_$j";
			$pst_emp_id = $$varname;
	
			$sql = "insert into ovtmaprvemp (apply_id, aprv_no, person_no, emp_id, delete_flg) values (";
			$content = array($apply_id, $apv_order, $j, $pst_emp_id, 'f');
			$ins = insert_into_table($con, $sql, $content, $fname);
			if ($ins == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}

	// 非同期・同期受信登録
	$previous_apv_order = "";
	$arr_apv = array();
	$arr_apv_sub_order = array();
	for($i=1; $i <= $approve_num; $i++)
	{
		$varname = "apv_order$i";
		$apv_order = $$varname;
	
		$varname = "apv_sub_order$i";
		$apv_sub_order = ($$varname == "") ? null : $$varname;
	
		$varname = "multi_apv_flg$i";
		$multi_apv_flg = ($$varname == "") ? "f" : $$varname;
	
		$varname = "next_notice_div$i";
		$next_notice_div = ($$varname == "") ? null : $$varname;
	
		if($previous_apv_order != $apv_order)
		{
			$arr_apv_sub_order = array();
		}
		$arr_apv_sub_order[] = $apv_sub_order;
		$arr_apv[$apv_order] = array("multi_apv_flg" => $multi_apv_flg, "next_notice_div" => $next_notice_div, "apv_sub_order" => $arr_apv_sub_order);
	
		$previous_apv_order = $apv_order;
	}

	$arr_send_apv_sub_order = array();
	foreach($arr_apv as $apv_order => $apv_info)
	{
		$multi_apv_flg = $apv_info["multi_apv_flg"];
		$next_notice_div = $apv_info["next_notice_div"];
		$arr_apv_sub_order = $apv_info["apv_sub_order"];
	
		foreach($arr_send_apv_sub_order as $send_apv_order => $arr_send_apv_sub)
		{
			// 非同期通知
			if($arr_send_apv_sub != null)
			{
				foreach($arr_send_apv_sub as $send_apv_sub_order)
				{
					foreach($arr_apv_sub_order as $recv_apv_sub_order)
					{
						$sql = "insert into ovtm_async_recv (apply_id, send_aprv_no, send_aprv_sub_no, recv_aprv_no, recv_aprv_sub_no, send_aprved_no, apv_show_flg) values (";
						$content = array($apply_id, $send_apv_order, $send_apv_sub_order, $apv_order, $recv_apv_sub_order, null, "f");
						$ins = insert_into_table($con, $sql, $content, $fname);
						if ($ins == 0) {
							pg_query($con, "rollback");
							pg_close($con);
							echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
							echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
							exit;
						}
					}
				}
			}
			// 同期通知
			else
			{
				foreach($arr_apv_sub_order as $recv_apv_sub_order)
				{
					$sql = "insert into ovtm_async_recv (apply_id, send_aprv_no, send_aprv_sub_no, recv_aprv_no, recv_aprv_sub_no, send_aprved_no, apv_show_flg) values (";
					$content = array($apply_id, $send_apv_order, null, $apv_order, $recv_apv_sub_order, null, "f");
					$ins = insert_into_table($con, $sql, $content, $fname);
					if ($ins == 0) {
						pg_query($con, "rollback");
						pg_close($con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}
				}
			}

		}
		$arr_send_apv_sub_order = array();

		// 非同期通知の場合
		if($multi_apv_flg == "t" && $next_notice_div == "1" && count($arr_apv_sub_order) > 1)
		{
			$arr_send_apv_sub_order[$apv_order] = $arr_apv_sub_order;
		}
		// 同期通知または権限並列通知の場合
		else if($multi_apv_flg == "t" && $next_notice_div != "1" && count($arr_apv_sub_order) > 1)
		{
			$arr_send_apv_sub_order[$apv_order] = null;
		}
	}

// 定時退勤ボタンが押された場合
} else {

	$over_start_time = ""; //残業開始 20110830
	$over_end_time = ""; //残業終了
	// 残業申請不要に変更 20091006
	$sql = "select apply_id from ovtmapply";
	$cond = "where emp_id = '$emp_id' and target_date = '$date' order by apply_id desc limit 1";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0)
	{
		$apply_id = pg_fetch_result($sel, 0, 0);
		// ステータスを更新
		$sql = "update ovtmapply set";
		$set = array("apply_status", "delete_flg", "reason_id", "reason", "reason_detail", "over_start_time", "over_end_time");
		$setvalue = array('4', 'f', $reason_id, pg_escape_string($reason), pg_escape_string($reason_detail), $over_start_time, $over_end_time);
		$cond = "where apply_id = $apply_id";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
			
	} else {
		// 申請IDを採番
		$sql = "select max(apply_id) from ovtmapply";
		$cond = "";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$apply_id = intval(pg_fetch_result($sel, 0, 0)) + 1;
		// ステータスapply_statusを4:申請不要とする
		$sql = "insert into ovtmapply (apply_id, emp_id, target_date, reason_id, reason, apply_time, apply_status, ovtmcfm, delete_flg, end_time, next_day_flag, start_time, previous_day_flag, over_start_time, over_end_time, reason_detail) values (";
		$content = array($apply_id, $emp_id, $date, $reason_id, pg_escape_string($reason), date("YmdHis"), '4', null, 'f', $end_time, $next_day_flag, $start_time, $previous_day_flag, $over_start_time, $over_end_time, pg_escape_string($reason_detail));
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

}

//空・未入力の場合、翌日フラグを初期化(0にする)
if (empty($next_day_flag)){
	$next_day_flag = 0;
}

if (empty($previous_day_flag)){
	$previous_day_flag = 0;
}

//追加分の変更確認
$chg_flag = false;
for ($i=3; $i<=$maxline; $i++) {
    $s_t = "over_start_time".$i;
    $s_f = "over_start_next_day_flag".$i;
    $ps_t = "pre_over_start_time".$i;
    $ps_f = "pre_over_start_next_day_flag".$i;
    $e_t = "over_end_time".$i;
    $e_f = "over_end_next_day_flag".$i;
    $pe_t = "pre_over_end_time".$i;
    $pe_f = "pre_over_end_next_day_flag".$i;
    if ($$s_t != $$ps_t || $$s_f != $$ps_f || $$e_t != $$pe_t || $$e_f != $$pe_f) {
        $chg_flag = true;
        break;
    }
}
if (!$chg_flag) {
    for ($i=1; $i<=$maxline; $i++) {
        $s_t = "rest_start_time".$i;
        $s_f = "rest_start_next_day_flag".$i;
        $ps_t = "pre_rest_start_time".$i;
        $ps_f = "pre_rest_start_next_day_flag".$i;
        $e_t = "rest_end_time".$i;
        $e_f = "rest_end_next_day_flag".$i;
        $pe_t = "pre_rest_end_time".$i;
        $pe_f = "pre_rest_end_next_day_flag".$i;
        if ($$s_t != $$ps_t || $$s_f != $$ps_f || $$e_t != $$pe_t || $$e_f != $$pe_f) {
            $chg_flag = true;
            break;
        }
    }
}
//判定追加
// 出勤時刻、退勤時刻、残業時刻が変わる場合
if ($start_time != $pre_start_time || $previous_day_flag != $pre_previous_day_flag ||
		$end_time != $pre_end_time || $next_day_flag != $pre_next_day_flag || 
		$over_start_time != $pre_over_start_time || $over_end_time != $pre_over_end_time ||
		$over_start_next_day_flag != $pre_over_start_next_day_flag ||
		$over_end_next_day_flag != $pre_over_end_next_day_flag || 
		$over_start_time2 != $pre_over_start_time2 || $over_end_time2 != $pre_over_end_time2 ||
		$over_start_next_day_flag2 != $pre_over_start_next_day_flag2 ||
		$over_end_next_day_flag2 != $pre_over_end_next_day_flag2 || $chg_flag) {

	// 勤務実績の退勤時間を更新
	$sql = "update atdbkrslt set";
	$set = array("start_time", "previous_day_flag", "end_time", "next_day_flag", "over_start_time", "over_end_time", "over_start_next_day_flag", "over_end_next_day_flag", "over_start_time2", "over_end_time2", "over_start_next_day_flag2", "over_end_next_day_flag2");
	$setvalue = array($start_time, $previous_day_flag, $end_time, $next_day_flag, $over_start_time, $over_end_time, $over_start_next_day_flag, $over_end_next_day_flag, $over_start_time2, $over_end_time2, $over_start_next_day_flag2, $over_end_next_day_flag2);
    //追加
    for ($i=3; $i<=$maxline; $i++) {
        $s_t = "over_start_time".$i;
        $e_t = "over_end_time".$i;
        $s_f = "over_start_next_day_flag".$i;
        $e_f = "over_end_next_day_flag".$i;
        array_push($set, $s_t);
        array_push($set, $e_t);
        array_push($set, $s_f);
        array_push($set, $e_f);
        array_push($setvalue, $$s_t);
        array_push($setvalue, $$e_t);
        array_push($setvalue, $$s_f);
        array_push($setvalue, $$e_f);
    }    
    for ($i=1; $i<=$maxline; $i++) {
        $s_t = "rest_start_time".$i;
        $e_t = "rest_end_time".$i;
        $s_f = "rest_start_next_day_flag".$i;
        $e_f = "rest_end_next_day_flag".$i;
        array_push($set, $s_t);
        array_push($set, $e_t);
        array_push($set, $s_f);
        array_push($set, $e_f);
        array_push($setvalue, $$s_t);
        array_push($setvalue, $$e_t);
        array_push($setvalue, $$s_f);
        array_push($setvalue, $$e_f);
    }    
    
	$cond = "where emp_id = '$emp_id' and date = '$date'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// 勤務時間修正申請が出されている場合
	$sql = "select apply_id from tmmdapply";
	$cond = "where emp_id = '$emp_id' and target_date = '$date' and not delete_flg and re_apply_id is null";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0)
	{
		$tmmd_apply_id = pg_fetch_result($sel, 0, "apply_id");

		// 勤務時間修正申請の退勤時間とステータスを更新
		$sql = "update tmmdapply set";
		$set = array("a_end_time", "apply_status", "a_next_day_flag", "a_start_time", "a_previous_day_flag", "a_over_start_time", "a_over_end_time", "a_over_start_next_day_flag", "a_over_end_next_day_flag", "a_over_start_time2", "a_over_end_time2", "a_over_start_next_day_flag2", "a_over_end_next_day_flag2");
		$setvalue = array($end_time, 0, $next_day_flag, $start_time, $previous_day_flag, $over_start_time, $over_end_time, $over_start_next_day_flag, $over_end_next_day_flag, $over_start_time2, $over_end_time2, $over_start_next_day_flag2, $over_end_next_day_flag2);
        //追加
        for ($i=3; $i<=$maxline; $i++) {
            $s_t = "a_over_start_time".$i;
            $e_t = "a_over_end_time".$i;
            $s_f = "a_over_start_next_day_flag".$i;
            $e_f = "a_over_end_next_day_flag".$i;
            $s_tv = "over_start_time".$i;
            $e_tv = "over_end_time".$i;
            $s_fv = "over_start_next_day_flag".$i;
            $e_fv = "over_end_next_day_flag".$i;
            array_push($set, $s_t);
            array_push($set, $e_t);
            array_push($set, $s_f);
            array_push($set, $e_f);
            array_push($setvalue, $$s_tv);
            array_push($setvalue, $$e_tv);
            array_push($setvalue, $$s_fv);
            array_push($setvalue, $$e_fv);
        }    
        for ($i=1; $i<=$maxline; $i++) {
            $s_t = "a_rest_start_time".$i;
            $e_t = "a_rest_end_time".$i;
            $s_f = "a_rest_start_next_day_flag".$i;
            $e_f = "a_rest_end_next_day_flag".$i;
            $s_tv = "rest_start_time".$i;
            $e_tv = "rest_end_time".$i;
            $s_fv = "rest_start_next_day_flag".$i;
            $e_fv = "rest_end_next_day_flag".$i;
            array_push($set, $s_t);
            array_push($set, $e_t);
            array_push($set, $s_f);
            array_push($set, $e_f);
            array_push($setvalue, $$s_tv);
            array_push($setvalue, $$e_tv);
            array_push($setvalue, $$s_fv);
            array_push($setvalue, $$e_fv);
        }    
        $cond = "where apply_id = '$tmmd_apply_id'";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 勤務時間修正申請承認情報の承認日とステータスを更新
		$sql = "update tmmdaprv set";
		$set = array("aprv_date", "aprv_status", "aprv_comment");
		$setvalue = array(null, 0, "");
		$cond = "where apply_id = '$tmmd_apply_id'";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 画面を閉じる
if ($pnt_url == "") {
	if ($no_apply != "y") {
		echo("<script type=\"text/javascript\">alert('申請しました。');</script>");
	} else {
		echo("<script type=\"text/javascript\">alert('定時退勤登録しました。');</script>");
	}
} else {
	echo("<script type=\"text/javascript\">opener.location.href = '$pnt_url';</script>");
}
echo("<script type=\"text/javascript\">self.close();</script>");
?>
