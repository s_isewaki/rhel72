<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("show_select_values.ini");
require_once("cl_application_list.inc");
require_once("cl_yui_calendar_util.ini");
require_once("cl_application_common.ini");
require_once("cl_common.ini");
require_once("cl_application_workflow_common_class.php");
require_once("cl_title_name.ini");
require_once("cl_smarty_setting.ini");
require_once("cl_common_log_class.inc");

$log = new cl_common_log_class(basename(__FILE__));

$log->info(basename(__FILE__)." START");

$smarty = cl_get_smarty_obj();
$smarty->template_dir = dirname(__FILE__) . "/cl/view";

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,$CAS_MENU_AUTH,$fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
// ワークフロー権限の取得
$workflow_auth = check_authority($session, $CL_MANAGE_AUTH, $fname);

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
// ログインユーザ情報取得
//====================================
$obj = new cl_application_workflow_common_class($con, $fname);
$arr_login = $obj->get_empmst($session);
$login_emp_id = $arr_login[0]["emp_id"];

//====================================
// 初期化処理
//====================================
$arr_wkfwcatemst = array();
$sel_wkfwcate = search_wkfwcatemst($con, $fname);
while($row_cate = pg_fetch_array($sel_wkfwcate)) {
	$tmp_wkfw_type = $row_cate["wkfw_type"];
	$tmp_wkfw_nm = $row_cate["wkfw_nm"];

	$arr_wkfwmst_tmp = array();
	$sel_wkfwmst = search_wkfwmst($con, $fname, $tmp_wkfw_type);
	while($row_wkfwmst = pg_fetch_array($sel_wkfwmst)) {
		$tmp_wkfw_id = $row_wkfwmst["wkfw_id"];
		$wkfw_title = $row_wkfwmst["wkfw_title"];

		$arr_wkfwmst_tmp[$tmp_wkfw_id] = $wkfw_title;
	}
	$arr_wkfwcatemst[$tmp_wkfw_type] = array("name" => $tmp_wkfw_nm, "wkfw" => $arr_wkfwmst_tmp);
}

// 申請日セット(本日日付から過去２ヶ月分)
// 申請タブをクリックした場合のみ。
if($apply_date_defalut == "on")
{
	$today = date("Y/m/d", strtotime("today"));

	$arr_today = split("/", $today);
	$date_y2 = $arr_today[0];
	$date_m2 = $arr_today[1];
	$date_d2 = $arr_today[2];

	// ２ヶ月前の日付取得
	$two_months_ago = date("Y/m/d",strtotime("-2 months" ,strtotime($today)));

	$arr_two_months_ago = split("/", $two_months_ago);
	$date_y1 = $arr_two_months_ago[0];
	$date_m1 = $arr_two_months_ago[1];
	$date_d1 = $arr_two_months_ago[2];
}

if ($_POST['insertflg'] == 'true')
{

	require_once("cl_common_log_class.inc");
	require_once("Cmx.php");
	require_once('MDB2.php');

	$training_name		= $_POST['training_name'];
	$training_purpose	= $_POST['training_purpose'];
	$target_level1		= $_POST['target_level1'];
	$target_level2		= $_POST['target_level2'];
	$target_level3		= $_POST['target_level3'];
	$target_level4		= $_POST['target_level4'];
	$target_level5		= $_POST['target_level5'];
	$training_require	= $_POST['training_require'];
	$time_division		= $_POST['time_division'];
	$training_opener	= $_POST['training_opener_id'];
	$training_teacher1	= $_POST['training_teacher1_id'];
	$training_teacher2	= $_POST['training_teacher2_id'];
	$training_teacher3	= $_POST['training_teacher3_id'];
	$training_teacher4	= $_POST['training_teacher4_id'];
	$training_teacher5	= $_POST['training_teacher5_id'];
	$max_training_time	= $_POST['max_training_time'];
	$training_slogan	= $_POST['training_slogan'];
	$training_contents	= $_POST['training_contents'];
	$training_action	= $_POST['training_action'];
	$training_support	= $_POST['training_support'];
	if ($_POST['abolition_flag'] == 't') {
		$abolition_flag = 1;
	} else {
		$abolition_flag = 0;
	}
	// 2012/07/25 Yamagawa add(s)
	if ($_POST['auto_control_flg'] == 't') {
		$auto_control_flg = true;
	} else {
		$auto_control_flg = false;
	}
	// 2012/07/25 Yamagawa add(e)
	// 2012/11/19 Yamagawa add(s)
	$term_div = $_POST['term_div'];
	// 2012/11/19 Yamagawa add(e)

	$theme_contents_before	= $_POST['theme_contents_before'];
	$theme_method_before	= $_POST['theme_method_before'];
	$theme_deadline_before	= $_POST['theme_deadline_before'];
	$theme_present_before	= $_POST['theme_present_before_id'];

	$theme_contents_now		= $_POST['theme_contents_now'];
	$theme_method_now		= $_POST['theme_method_now'];
	$theme_deadline_now		= $_POST['theme_deadline_now'];
	$theme_present_now		= $_POST['theme_present_now_id'];

	$theme_contents_after	= $_POST['theme_contents_after'];
	$theme_method_after		= $_POST['theme_method_after'];
	$theme_deadline_after	= $_POST['theme_deadline_after'];
	$theme_present_after	= $_POST['theme_present_after_id'];

	//##########################################################################
	// データベース接続オブジェクト取得
	//##########################################################################
	$log->debug("MDB2オブジェクト取得開始",__FILE__,__LINE__);
	$mdb2 = MDB2::connect(CMX_DB_DSN);
	if (PEAR::isError($mdb2)) {
		$log->error("MDB2オブジェクト取得エラー：".$mdb2->getDebugInfo(),__FILE__,__LINE__);
	}
	$log->debug("MDB2オブジェクト取得終了",__FILE__,__LINE__);

	require_once("cl/model/master/cl_mst_inside_training_model.php");
	$training_model = new cl_mst_inside_training_model($mdb2,$login_emp_id);
	$log->debug("院内研修用DBA取得",__FILE__,__LINE__);

	require_once("cl/model/master/cl_mst_inside_training_theme_model.php");
	$theme_model = new cl_mst_inside_training_theme_model($mdb2,$login_emp_id);
	$log->debug("研修課題用DBA取得",__FILE__,__LINE__);

	require_once("cl/model/sequence/cl_inside_training_id_seq_model.php");
	$training_id_model = new cl_inside_training_id_seq_model($mdb2,$login_emp_id);
	$log->debug("研修ID用DBA取得",__FILE__,__LINE__);
	$training_id = $training_id_model->getInTrainingId();
	$log->debug("研修ID用取得",__FILE__,__LINE__,$training_id);

	require_once("cl/model/sequence/cl_inside_training_task_id_seq_model.php");
	$theme_id_model = new cl_inside_training_task_id_seq_model($mdb2,$login_emp_id);
	$log->debug("研修課題ID用DBA取得",__FILE__,__LINE__);


	if (
		$theme_contents_before== "" &&
		$theme_method_before== "" &&
		$theme_deadline_before== "" &&
		$theme_present_before== ""
	) {
		$theme_id_before = "";
	} else {
		$theme_id_before = $theme_id_model->getId();
	}
	$log->debug("研修前課題用ID取得",__FILE__,__LINE__);

	if (
		$theme_contents_now== "" &&
		$theme_method_now== "" &&
		$theme_deadline_now== "" &&
		$theme_present_now== ""
	) {
		$theme_id_now = "";
	} else {
		$theme_id_now = $theme_id_model->getId();
	}
	$log->debug("研修中課題用ID取得",__FILE__,__LINE__);

	if (
		$theme_contents_after== "" &&
		$theme_method_after== "" &&
		$theme_deadline_after== "" &&
		$theme_present_after== ""
	) {
		$theme_id_after = "";
	} else {
		$theme_id_after = $theme_id_model->getId();
	}
	$log->debug("研修後課題用ID取得",__FILE__,__LINE__);

	// トランザクション開始
	$mdb2->beginTransaction();
	$log->debug("トランザクション開始",__FILE__,__LINE__);
	$param = array(
		"training_id"			=> $training_id,
		"training_name"			=> $training_name,
		"training_purpose"		=> $training_purpose,
		"target_level1"			=> $target_level1,
		"target_level2"			=> $target_level2,
		"target_level3"			=> $target_level3,
		"target_level4"			=> $target_level4,
		"target_level5"			=> $target_level5,
		"max_training_time"		=> $max_training_time,
		"training_require"		=> $training_require,
		"time_division"			=> $time_division,
		"training_opener"		=> $training_opener,
		"training_teacher1"		=> $training_teacher1,
		"training_teacher2"		=> $training_teacher2,
		"training_teacher3"		=> $training_teacher3,
		"training_teacher4"		=> $training_teacher4,
		"training_teacher5"		=> $training_teacher5,
		"training_slogan"		=> $training_slogan,
		"training_contents"		=> $training_contents,
		"training_action"		=> $training_action,
		"training_support"		=> $training_support,
		"abolition_flag"		=> $abolition_flag,
		// 2012/07/25 Yamagawa add(s)
		"auto_control_flg"		=> $auto_control_flg
		// 2012/07/25 Yamagawa add(e)
		// 2012/11/19 Yamagawa add(s)
		,"term_div"				=> $term_div
		// 2012/11/19 Yamagawa add(e)
	);

	$log->debug("研修登録用パラメータ取得",__FILE__,__LINE__);
	$training_model->insert($param);
	$log->debug("研修登録",__FILE__,__LINE__);

	if ($theme_id_before != "") {
		$param = array(
			"training_id"		=> $training_id,
			"theme_id"			=> $theme_id_before,
			"theme_division"	=> 0,
			"theme_contents"	=> $theme_contents_before,
			"theme_method"		=> $theme_method_before,
			"theme_deadline"	=> $theme_deadline_before,
			"theme_present"		=> $theme_present_before
		);
		$theme_model->insert($param);
	}
	$log->debug("研修前課題登録",__FILE__,__LINE__);

	if ($theme_id_now != "") {
		$param = array(
			"training_id"		=> $training_id,
			"theme_id"			=> $theme_id_now,
			"theme_division"	=> 1,
			"theme_contents"	=> $theme_contents_now,
			"theme_method"		=> $theme_method_now,
			"theme_deadline"	=> $theme_deadline_now,
			"theme_present"		=> $theme_present_now
		);
		$theme_model->insert($param);
	}
	$log->debug("研修中課題登録",__FILE__,__LINE__);


	if ($theme_id_after != "") {
		$param = array(
			"training_id"		=> $training_id,
			"theme_id"			=> $theme_id_after,
			"theme_division"	=> 2,
			"theme_contents"	=> $theme_contents_after,
			"theme_method"		=> $theme_method_after,
			"theme_deadline"	=> $theme_deadline_after,
			"theme_present"		=> $theme_present_after
		);
		$theme_model->insert($param);
	}
	$log->debug("研修後課題登録",__FILE__,__LINE__);

	// コミット
	$mdb2->commit();
	$log->debug("コミット",__FILE__,__LINE__);

	echo("<script language='javascript'>window.opener.search_training();</script>");
	echo("<script language='javascript'>window.close();</script>");
	exit;

}

$obj = new cl_application_workflow_common_class($con, $fname);

$cl_title = cl_title_name();

// h-iwamoto ADD START
/**
 * YUIカレンダーに必要な外部ファイルの読み込みを行います。
 */
$yui_cal_part=get_yui_cal_part();

/**
 * アプリケーションメニューHTML取得
 */
$aplyctn_mnitm_str=get_applycation_menuitem($session,$fname);

/**
 * 申請年月日（開始）オプションHTML取得
 *
 */
// 年オプションHTML取得
$option_date_y1=cl_get_select_years($date_y1);

// 月オプションHTML取得
$option_date_m1=cl_get_select_months($date_m1);

// 日オプションHTML取得
$option_date_d1=cl_get_select_days($date_d1);

/**
 * 申請年月日（終了）オプションHTML取得
 *
 */
// 年オプションHTML取得
$option_date_y2=cl_get_select_years($date_y2);

// 月オプションHTML取得
$option_date_m2=cl_get_select_months($date_m2);

// 日オプションHTML取得
$option_date_d2=cl_get_select_days($date_d2);

// レベルI
$level1_options_str = get_level_options($target);

// レベルII
$level2_options_str = get_level_options($target);

// レベルIII
$level3_options_str = get_level_options($target);

// レベルIV
$level4_options_str = get_level_options($target);

// レベルV
$level5_options_str = get_level_options($target);

$time_division_options_str = get_time_division_options($division);

// 2012/11/19 Yamagawa add(s)
$term_div_options_str = get_term_div_options($term_div);
// 2012/11/19 Yamagawa add(e)

/**
 * 志願申請、評価申請用の志願者取得（申請者名）
 */
$arr_emp = $obj->get_empmst($session);
$emp_id  = $arr_emp["0"]["emp_id"];
$lt_nm  = $arr_emp["0"]["emp_lt_nm"];
$ft_nm  = $arr_emp["0"]["emp_ft_nm"];

$applicant_name = "$lt_nm"." "."$ft_nm";

/**
 * 申請状況HTML取得
 */
//$srch_aplctn_lst_str=get_search_application_list($con, $session, $fname, $category, $workflow, $apply_title, $approve_emp_nm, $date_y1, $date_m1, $date_d1, $date_y2, $date_m2, $date_d2, $date_y3, $date_m3, $date_d3, $date_y4, $date_m4, $date_d4, $apply_stat, $page);

/**
 * Javascriptコード生成（動的オプション項目変更用）
 */
$js_str = get_js_str($arr_wkfwcatemst,$workflow);

/**
 * データベースクローズ
 */
pg_close($con);

/**
 * テンプレートマッピング
 */
$smarty->assign( 'cl_title'				, $cl_title				);
$smarty->assign( 'session'				, $session				);
$smarty->assign( 'yui_cal_part'			, $yui_cal_part			);
$smarty->assign( 'workflow_auth'		, $workflow_auth		);
$smarty->assign( 'aplyctn_mnitm_str'	, $aplyctn_mnitm_str	);

$smarty->assign( 'training_id'			, $training_id			);
$smarty->assign( 'training_name'		, $training_name		);
$smarty->assign( 'level_options_str'	, $level_options_str	);
$smarty->assign( 'teacher_name'			, $teacher_name			);

$smarty->assign( 'option_date_y1'		, $option_date_y1		);
$smarty->assign( 'option_date_m1'		, $option_date_m1		);
$smarty->assign( 'option_date_d1'		, $option_date_d1		);

$smarty->assign( 'option_date_y2'		, $option_date_y2		);
$smarty->assign( 'option_date_m2'		, $option_date_m2		);
$smarty->assign( 'option_date_d2'		, $option_date_d2		);

$smarty->assign( 'level1_options_str'	, $level1_options_str	);
$smarty->assign( 'level2_options_str'	, $level2_options_str	);
$smarty->assign( 'level3_options_str'	, $level3_options_str	);
$smarty->assign( 'level4_options_str'	, $level4_options_str	);
$smarty->assign( 'level5_options_str'	, $level5_options_str	);
$smarty->assign( 'time_division_options_str'	, $time_division_options_str	);
// 2012/11/19 Yamagawa add(s)
$smarty->assign( 'level5_options_str'	, $level5_options_str	);
// 2012/11/19 Yamagawa add(e)
// 2012/11/19 Yamagawa add(s)
$smarty->assign( 'term_div_options_str'	, $term_div_options_str	);
// 2012/11/19 Yamagawa add(e)

//$smarty->assign( 'srch_aplctn_lst_str'	, $srch_aplctn_lst_str	);
$smarty->assign( 'js_str'				, $js_str				);

/**
 * テンプレート出力
 */
$smarty->display(basename($_SERVER['PHP_SELF'],'.php').".tpl");

/**
 * Javascript文字列取得
 */
function get_js_str($arr_wkfwcatemst,$workflow)
{
	$str_buff[]  = "";
	foreach ($arr_wkfwcatemst as $tmp_cate_id => $arr) {
		$str_buff[] .= "\n\t\tif (cate_id == '".$tmp_cate_id."') {\n";


		 foreach($arr["wkfw"] as $tmp_wkfw_id => $tmp_wkfw_nm){
			$tmp_wkfw_nm = htmlspecialchars($tmp_wkfw_nm, ENT_QUOTES);
			$tmp_wkfw_nm = str_replace("&#039;", "\'", $tmp_wkfw_nm);
			$tmp_wkfw_nm = str_replace("&quot;", "\"", $tmp_wkfw_nm);

			$str_buff[] .= "\t\t\taddOption(obj_wkfw, '".$tmp_wkfw_id."', '".$tmp_wkfw_nm."',  '".$workflow."');\n";

		}

		$str_buff[] .= "\t\t}\n";
	}

	return implode('', $str_buff);
}
/**
 * 申請状況HTML取得
 */
/*
function get_search_application_list(
	$con,
	$session,
	$fname,
	$category,
	$workflow,
	$apply_title,
	$approve_emp_nm,
	$date_y1,
	$date_m1,
	$date_d1,
	$date_y2,
	$date_m2,
	$date_d2,
	$date_y3,
	$date_m3,
	$date_d3,
	$date_y4,
	$date_m4,
	$date_d4,
	$apply_stat,
	$page
)
{
	ob_start();
	search_application_list($con, $session, $fname, $category, $workflow, pg_escape_string($apply_title), pg_escape_string($approve_emp_nm), $date_y1, $date_m1, $date_d1, $date_y2, $date_m2, $date_d2, $date_y3, $date_m3, $date_d3, $date_y4, $date_m4, $date_d4, $apply_stat, $page);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}
*/

/**
 * 年オプションHTML取得
 */
function cl_get_select_years($date)
{
	ob_start();
	show_select_years(10, $date, true);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

/**
 * 月オプションHTML取得
 */
function cl_get_select_months($date)
{

	ob_start();
	show_select_months($date, true);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

/**
 * 日オプションHTML取得
 */
function cl_get_select_days($date)
{

	ob_start();
	show_select_days($date, true);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}
/**
 * アプリケーションメニューHTML取得
 */
function get_applycation_menuitem($session,$fname)
{
	ob_start();
	show_applycation_menuitem($session,$fname,"");
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}
/**
 * YUIカレンダー用スクリプトHTML取得
 */
function get_yui_cal_part()
{

	ob_start();
	/**
	 * YUIカレンダーに必要な外部ファイルの読み込みを行います。
	 */
	write_yui_calendar_use_file_read_0_12_2();
	/**
	 * YUIカレンダー用(画面内)スクリプトを出力します。
	 */
	write_yui_calendar_script2(4);
	$yui_cal_part=ob_get_contents();
	ob_end_clean();
	return $yui_cal_part;
}

function get_level_options($target)
{
	ob_start();
	show_level_options($target);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

function get_time_division_options($division)
{
	ob_start();
	show_time_division_options($division);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

// 2012/11/19 Yamagawa add(s)
function get_term_div_options($term_div)
{
	ob_start();
	show_term_div_options($term_div);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}
// 2012/11/19 Yamagawa add(e)

// カテゴリ名称を取得
function search_wkfwcatemst($con, $fname) {

	$sql = "select * from cl_wkfwcatemst";
	$cond = "where wkfwcate_del_flg = 'f' order by wkfw_type";
	$sel=select_from_table($con,$sql,$cond,$fname);
	if($sel==0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	return $sel;
}

// ワークフロー情報取得
function search_wkfwmst($con, $fname, $wkfw_type) {

	$sql = "select * from cl_wkfwmst";
	$cond="where wkfw_type='$wkfw_type' and wkfw_del_flg = 'f' order by wkfw_id asc";
	$sel=select_from_table($con,$sql,$cond,$fname);
	if($sel==0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	return $sel;
}

// 申請状況オプションを出力
function show_level_options($target) {

	$arr_level_nm = array("","必須","任意");
	$arr_level_id = array("0","1","2");

	for($i=0;$i<count($arr_level_nm);$i++) {

		echo("<option value=\"$arr_level_id[$i]\"");
		if($target == $arr_level_id[$i]) {
			echo(" selected");
		}
		echo(">$arr_level_nm[$i]\n");
	}
}

function show_time_division_options($division) {

	$arr_time_division_nm = array("就業時間内","就業時間外");
	$arr_time_division_id = array("1","2");

	for($i=0;$i<count($arr_time_division_nm);$i++) {

		echo("<option value=\"$arr_time_division_id[$i]\"");
		if($division == $arr_time_division_id[$i]) {
			echo(" selected");
		}
		echo(">$arr_time_division_nm[$i]\n");
	}
}

// 2012/11/19 Yamagawa add(s)
function show_term_div_options($term_div) {

	$arr_term_div_nm = array("","上期","下期");
	$arr_term_div_id = array("","1","2");

	for($i=0;$i<count($arr_term_div_nm);$i++) {

		echo("<option value=\"$arr_term_div_id[$i]\"");
		if($term_div == $arr_term_div_id[$i]) {
			echo(" selected");
		}
		echo(">$arr_term_div_nm[$i]\n");
	}

}
// 2012/11/19 Yamagawa add(e)

$log->info(basename(__FILE__)." END");
$log->shutdown();
