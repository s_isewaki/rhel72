<?
require_once("read_xml.ini");


/**
 * 入力フォームを出力します。(テンプレートインターフェース関数)
 */
function write_tmpl_page()
{
?>
<FONT face="MS UI Gothic" size="4">S(Subjective)</FONT><BR>
<TEXTAREA id="Textarea_S" style="WIDTH: 424px;" name="Textarea_S" rows="10" cols="50"></TEXTAREA><BR>
<BR>
<FONT face="MS UI Gothic" size="4">O(Objective)</FONT><BR>
<TEXTAREA id="Textarea_O" style="WIDTH: 424px;" name="Textarea_O" rows="10" cols="50"></TEXTAREA><BR>
<BR>
<FONT face="MS UI Gothic" size="4">A(Assessment)</FONT><BR>
<TEXTAREA id="Textarea_A" style="WIDTH: 424px;" name="Textarea_A" rows="10" cols="50"></TEXTAREA><BR>
<BR>
<FONT face="MS UI Gothic" size="4">P(Plan)</FONT><BR>
<TEXTAREA id="Textarea_P" style="WIDTH: 424px;" name="Textarea_P" rows="10" cols="50"></TEXTAREA><BR>
<BR>
<FONT face="MS UI Gothic" size="4">N(その他)</FONT><BR>
<TEXTAREA id="Textarea_N" style="WIDTH: 424px;" name="Textarea_N" rows="10" cols="50"></TEXTAREA><BR>
<BR>
<BR>
<input type="hidden" name="xml_creator" value="create_SOAP_xml.php">
<?
}


/**
 * 初期表示データ設定スクリプトを出力します。(テンプレートインターフェース関数)
 * 
 * @param $xml_file データが保存されているXMLファイルを指定します。
 */
function write_tmpl_page_data_script($xml_file)
{
	if($xml_file != ""){
		echo("<script language=\"javascript\">");
		echo("function load_action(){\n");
		
		//XMLよりテンプレート初期表示値設定
		$XMLControl = getTmplControl($xml_file);
		while(list ($key, $val) = each($XMLControl)) {
			if(substr($key, 0, 9) == "Textarea_")
			{
				$val = str_replace("\r\n", "\\r\\n", $val);
				$val = str_replace("\"","\\\"", $val);
				$val = str_replace("\'","\\\'", $val);
				echo("document.tmplform.elements['".$key."'].value = \"$val\";\n");
			}
		}
		
		echo("}\n");
		echo("</script>\n");
	}
}

/**
 * フローシート一覧表示関数です。(テンプレートインターフェース関数)
 * この関数はテンプレートにおいて非必須です。
 * この関数を定義しない場合、フローシートには内容形式で表示されます。
 * 
 * @param array $xml_files  テンプレートデータXMLファイル名(string)の配列
 * @param int   $disp_count 表示件数
 */
function write_flowsheet_page($xml_files, $disp_count)
{
	require_once("summary_tmpl_util.ini");
	write_flowsheet_page_default($xml_files, $disp_count);
}


?>