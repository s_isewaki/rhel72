<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$auth_nc_reg = check_authority($session, 10, $fname);
if ($auth_nc_reg == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$con = connect2db($fname);

if($trashbox==""){
	echo("<script language='javascript'>alert(\"チェックボックスをオンにしてください。\");</script>");
	echo("<script language='javascript'>history.back();</script>");
	exit;
}

$in="where nmrm_id in (";
$count=count($trashbox);
for($i=0;$i<$count;$i++){
	if(!$trashbox[$i]==""){
		$in.="'$trashbox[$i]'";
		if($i<($count-1)){
			$in.=",";
		}
	}
}
$in.=")";
$delete_nmrmst="update nmrmmst set nmrm_del_flg='t' $in";
$result_delete=pg_exec($con,$delete_nmrmst);
pg_close($con);
if($result_delete==true){
	echo("<script language='javascript'>location.href = 'net_conference_menu.php?session=$session';</script>");
	exit;
}else{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
?>
