<?

require_once("Cmx.php");
require_once("Cmx/Model/SystemConfig.php");
require_once("about_comedix.php");
require_once("show_newsuser_refer_list.ini");
require_once("news_common.ini");
require_once("get_values.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}
// 権限のチェック
$check_auth = check_authority($session, 46, $fname);
if ($check_auth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}
// お知らせ管理権限の取得
$news_admin_auth = check_authority($session, 24, $fname);
// 処理モードの設定
define("LIST_REFERRED", 1);  // 確認済みリストを表示
define("LIST_UNREFERRED", 2);  // 未確認リストを表示
define("LIST_ALL", 4);  // 対象者リストを表示
define("LIST_UNREAD", 5);  // 未読リストを表示
define("LIST_READ", 6);  // 既読リストを表示
define("LIST_READ_UNREFERRED", 7);  // 既読・未確認リストを表示
// ページ番号の設定
if ($refer_page == "") {
    $refer_page = 1;
}
// ソート順の初期設定 3:職員名昇順
// ソート順  1:職員ID昇順 2:職員ID降順 3:職員名昇順 4:職員名降順 5:参照時間昇順 6:参照時間降順（DB項目はコメント更新日時） 7:所属昇順 8:所属降順 9:役職昇順 10:役職降順
if ($sort_id == "") {
    $sort_id = "3";
}
// データベースに接続
$con = connect2db($fname);
// ログインユーザの職員IDを取得
$emp_id = get_emp_id($con, $session, $fname);
// 参照者一覧の表示件数設定を取得
$refer_rows_per_page = get_refer_rows_per_page($con, $fname);
// お知らせ情報の取得
$sql = "select news.* from news";
$cond = "where news_id = $news_id";
$sel_news = select_from_table($con, $sql, $cond, $fname);
if ($sel_news == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$news_title = pg_fetch_result($sel_news, 0, "news_title");
$news_category = pg_fetch_result($sel_news, 0, "news_add_category");

$record_flg = pg_fetch_result($sel_news, 0, "record_flg");
$public_flg = pg_fetch_result($sel_news, 0, "public_flg");
$reg_emp_id = pg_fetch_result($sel_news, 0, "emp_id");
$read_control = pg_fetch_result($sel_news, 0, "read_control");
// 標準カテゴリの場合
if ($news_category <= "3") {
    $tmp_news_category = $news_category;
}
else {
// 追加カテゴリの場合
    $tmp_news_category = $standard_cate_id;
    $st_id_cnt = pg_fetch_result($sel_news, 0, "st_id_cnt2");
    $job_id = pg_fetch_result($sel_news, 0, "job_id2");
}
// 役職指定数がある場合条件追加
// 役職ID
$st_ids = "";
$st_cond = "";
if ($st_id_cnt > 0) {
    if ($news_category <= "3") {
        $newsst_info = get_newsst_info($con, $fname, $news_id);
    }
    else {
        $newsst_info = get_newscatest_info($con, $fname, $newscate_id);
    }
    foreach ($newsst_info as $st_id => $st_nm) {
        if ($st_ids != "") {
            $st_ids .= ",";
        }
        $st_ids .= $st_id;
    }
    // 役職条件
    $st_cond .= " and (empmst.emp_st in ($st_ids) or exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_st in ($st_ids)))";
}
//モードの初期値
if ($mode == "") {
    $mode = ($record_flg == 't') ? LIST_REFERRED : LIST_READ;
}
// 総返信数を取得
$sql = "select count(*) from news";
$cond = "join newscomment on newscomment.emp_id = '$emp_id' and news.news_id = newscomment.news_id
			 where henshin_origin_news_id = '$news_id' and news.news_del_flg = 'f'";
$sel_count_all = select_from_table($con, $sql, $cond, $fname);
if ($sel_count_all == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$count_all = pg_fetch_result($sel_count_all, 0, 0);


// 一般ユーザ登録許可フラグを取得
$everyone_flg = get_everyone_flg($con, $fname);
// 職員IDのカラム表示条件の初期値を取得
$default_emp_id_chk = get_emp_id_read_status($con, $fname);
// 職員組織のカラム表示条件の初期値を取得
$default_emp_class_chk = get_emp_class_read_status($con, $fname);
// 職員職種のカラム表示条件の初期値を取得
$default_emp_job_chk = get_emp_job_read_status($con, $fname);
// お知らせ集計機能設定を取得
$conf = new Cmx_SystemConfig();
$setting_aggregate = $conf->get('setting_aggregate');

//オプションから引用した設定が"t"ではなかった場合に、"f"を設定する。
if ($setting_aggregate != "t") {
    $setting_aggregate = "f";
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix お知らせ | 返信一覧</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
<!--
function downloadCSV(csv_flg) {
    document.csv.action = 'news_refer_csv.php?admin_flg=0';
    document.csv.csv_flg.value = csv_flg;
    document.csv.submit();
}
//-->
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="newsuser_menu.php?session=<? echo($session); ?>"><img src="img/icon/b35.gif" width="32" height="32" border="0" alt="お知らせ・回覧板"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="newsuser_menu.php?session=<? echo($session); ?>"><b>お知らせ・回覧板</b></a>
<? if($h_page == "1" && $news_admin_auth == "1"){?>
 &gt; <a href="news_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a>
<? }?>
</font></td>
<? if ($h_page == "0" && $news_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="news_menu.php?session=<? echo($session); ?>&news=1"><b>管理画面へ</b></a></font></td>
<? } else if($h_page == "1" && $news_admin_auth == "1"){?>
	<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="news_menu.php?session=<? echo($session); ?>&news=1"><b>ユーザ画面へ</b></a></font></td>
<?php }?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">




<?php if ($h_page == 1) {?>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_menu.php?session=<? echo($session); ?>&page=<? echo($page); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#5279a5"><a href="newsuser_henshin_list.php?session=<? echo($session); ?>&news_id=<? echo($news_id); ?>&page=<? echo($page); ?>&h_page=<? echo($h_page); ?><?
if ($frompage != "") {
    echo("&frompage=$frompage");
}
?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>返信一覧</b></font></a></td>
<td width="5">&nbsp;</td>
<? if ($everyone_flg == "t") { ?>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_copy.php?session=<? echo($session); ?>&news_id=<? echo($news_id); ?>&henshin_toroku_flg=<?echo($henshin_toroku_flg);?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" >お知らせ登録</font></a></td>
<? } ?>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_search.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ検索</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_notice_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">通達区分一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_notice_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">通達区分登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_notice2_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ区分<br>一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_notice2_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ区分<br>登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_cate_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カテゴリ一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_cate_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カテゴリ登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="60" align="center" bgcolor="#bdd1e7"><a href="news_trash.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ゴミ箱</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="news_option.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション</font></a></td>
<td width="5">&nbsp;</td>
<?php //オプション機能で集計表で使用するにチェックを入れることで、タブに集計表が表示される ?>
<?php if($setting_aggregate == 't'){?>
	<td width="70" align="center" bgcolor="#bdd1e7">
		<a href="news_aggregate.php?session=<? echo($session); ?>">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">集計表</font>
		</a>
	</td>
<?php }?>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10">&nbsp;</font></td>
</tr>
<?php }else{?>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="<?
/*
$frompage "1":全館 "2":部署 "3":職種 "4":回覧 "5":自己登録分
*/
switch ($frompage) {
case "1":
    echo("newsuser_menu.php");
    break;
default:
    echo("newsuser_list.php");
}

?>?session=<? echo($session); ?>&page=<? echo($page); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#5279a5"><a href="newsuser_henshin_list.php?session=<? echo($session); ?>&news_id=<? echo($news_id); ?>&page=<? echo($page); ?>&h_page=<? echo($h_page); ?><?
if ($frompage != "") {
    echo("&frompage=$frompage");
}
?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>返信一覧</b></font></a></td>
<? if ($everyone_flg == "t") { ?>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="newsuser_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録</font></a></td>
<? } ?>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="newsuser_search.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">検索</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="newsuser_option.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション</font></a></td>
<td>&nbsp;</td>
<?php }?>

</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="200" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイトル</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(h($news_title)); ?></font></td>
</tr>
<tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">総返信数</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo("<b>{$count_all}件</b>");?></font></td>
</tr>
</table>
<?
// 選択肢の集計表
if ($record_flg == "t" && $qa_cnt > 0) {
    // 登録者のみダウンロード可
    if ($emp_id == $reg_emp_id) {
?>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td align="left">
<input type="button" value="CSVダウンロード（集計）" onclick="downloadCSV(1);">
<input type="button" value="CSVダウンロード（明細）" onclick="downloadCSV(2);">
</td>
</tr>
</table>
<form name="csv" method="get" target="download">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="news_id" value="<? echo($news_id); ?>">
<input type="hidden" name="csv_flg" value="">
<input type="hidden" name="admin_flg" value="<? echo('0'); ?>">
<input type="hidden" name="h_page" value="<? echo($h_page); ?>">
</form>
<iframe name="download" width="0" height="0" frameborder="0"></iframe>
<?
    }
    // 公開の場合か、登録者の場合に表示
    if ($public_flg == "t" || $emp_id == $reg_emp_id) {
    for ($i=1; $i<=$qa_cnt; $i++) {
        $width = 30 + 7 * $select_num[$i];
?>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">質問<? echo("{$i} ".h($question[$i])); ?></font><br>
<table width="<?=$width?>%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td align="center" bgcolor="#f6f9ff" width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">項番</font></td>
<?
for ($j=1; $j<=$select_num[$i]; $j++) {
    echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$j</font></td>\n");
}
?>
</tr>
<tr height="22">
<td align="center" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">内容</font></td>
<?
for ($j=1; $j<=$select_num[$i]; $j++) {
    echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".h($answer[$i][$j])."</font></td>\n");
}
?>
</tr>
<tr height="22">
<td align="center" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">人数</font></td>
<?
for ($j=1; $j<=$select_num[$i]; $j++) {
    // 職員一覧
    if ($arr_answer_cnt[$i][$j] == "") {
        $arr_answer_cnt[$i][$j] = "0";
    }
    echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
    // 登録者の場合にリンクを表示
    if ($arr_answer_cnt[$i][$j] != "0" && $emp_id == $reg_emp_id) {
        echo("<a href=\"javascript:void(0);\" onclick=\"window.open('news_answer_list.php?session=$session&news_id=$news_id&question_no=$i&item_no=$j&page=1&admin_flg=0', 'newwin', 'width=730,height=700,scrollbars=yes')\">&nbsp;{$arr_answer_cnt[$i][$j]}&nbsp;</a>\n");
    } else {
        echo($arr_answer_cnt[$i][$j]);
    }
    echo("</font></td>\n");
}
?>
</tr>
</table>
<?
    }
    } // 公開、登録者
}
?>
<?
// ソート用のURL設定
$url = "newsuser_henshin_list.php?session=$session&news_id=$news_id&mode=$mode&frompage=$frompage&sort_id=";
$tmp_sort_id = ($sort_id == "1") ? "2" : "1";
$news_date_url = $url.$tmp_sort_id;


// 矢印設定
$updown_gif1 = "";
$updown_gif2 = "";
$updown_gif3 = "";
switch ($sort_id) {
    case "1":
    case "2":
        $gif_name = ($sort_id == "1") ? "up.gif" : "down.gif";
        $updown_gif1 = "<img src=\"img/$gif_name\" alt=\"\" width=\"17\" height=\"17\" border=\"0\" style=\"vertical-align:middle;\">";
        break;
}
?>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録日</font></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">対象カテゴリ</font></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイトル</font></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発信者</font></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">確認</font></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">掲載期間</font></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">参照者一覧</font></a></td>
</tr>
<? show_news_henshin_list($con, $news_id, $mode, $news_category, $refer_page, $session, $fname, $sort_id, $refer_rows_per_page,$emp_id,$h_page); ?>
</table>
<?
switch ($mode) {
    case LIST_ALL:
        $count = $count_all;
        break;
    case LIST_REFERRED:
        $count = $count_referred;
        break;
    case LIST_UNREFERRED:
        $count = $count_unreferred;
        break;
    case LIST_UNREAD:
        $count = $count_unread;
        break;
    case LIST_READ:
        $count = $count_read;
        break;
    case LIST_READ_UNREFERRED:
        $count = $count_read_unreferred;
        break;
}
show_news_refer_page_list($con, $count, $news_id, $mode, $refer_page, $page, $session, $fname, $frompage, $sort_id, $refer_rows_per_page);
?>
</body>
<? pg_close($con); ?>
</html>
