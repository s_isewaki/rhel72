<?
// CSVスクリプト改修(2010/03)
//
// ■改修の意図
//
// ・これまでヘッダフィールドが異なるテンプレートや、フリー入力テンプレートが混在していても、
//   最初に抽出したテンプレートのヘッダのみが１行目に表示された。
//   ⇒よってヘッダとデータはマージするようにした。
// ・以下がクォートされていないため、XLSではうまく表示されない問題があった。
//   ⇒001などのコード：XLSで数値となり、パディングされているゼロが消える。
//   ⇒スラッシュ区切りの日付け：場合により勝手に解釈される
//   ⇒改行など：CSV列数が壊れる、ずれる
// ・これまで[summary_write_tmpl_util.ini][summary_tmpl_util.ini]を利用していたが、かなり難解かつ冗長すぎた。
// ・また、これらのインクルード内の関数を共通で利用していたことにより、以下の問題があった。
//   ⇒不用意に&nbsp;が付与される。
//   ⇒インクルード内で、CSVでは不要な$cre_dateがグローバル参照されており、かつ、値が無い場合に本日が自動代入されている。
// ・SQLにインジェクションが行われる可能性を除去。数値・文字列をそれぞれクォートした。
// ・ヘッダのタイトルが子階層のみなので、わからない。親階層名もヘッダに付与するようにした。
// ・その他、エラーが発生してもわからなくなるため、ob_xxx()の開始終了範囲を狭くした。
//
// なお、新型テンプレート(COMEDIX_TEMPLATE_VERSION_DEFINITION_2.0タイプ）は、HIDDENパラメータがあるが、
// そのまま表示させるようにしている。メドレポートなどのHTML表示より、かなり冗長に表示される。
// ただし重複するデータも表示される。
//
// 【ヘッダフィールドの問題】
// ※今後、テンプレートの中には、XMLフィールドが増減するものもある。
//   このため、出力するごとに、ヘッダのタイトルの並びや数が変わる場合がある。
// ※かつ、テンプレートのXMLフィールドが違っていてもマージするようになっている。
//   いっそうヘッダフィールドは合わないことが予測される。
// ※ただし、既存状態でも、ヘッダは正しく出力されていなかった。
// ※同じテンプレートシリーズを出力選択した場合は、正しくヘッダ出力される。
//   これは過去も今後も同じである。
//
// この問題に対する抜本的解決はなされていないが、今回の対応で、過去にあった矛盾は改善された。


ob_start();
require("about_postgres.php");
require("about_session.php");
require("about_authority.php");
require_once("html_to_text_for_tinymce.php");
require("label_by_profile_type.ini");
require_once("sot_util.php");
ob_end_clean();

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 57, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

if ($select_flg == 0 && $ids == "") {
	echo("<script type=\"text/javascript\">alert('出力するデータを選択してください。');</script>");
	echo("<script type=\"text/javascript\">close();</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);
// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

$csv_basehead = array();
$csv_basehead[] = "作成年月日";
$csv_basehead[] = $_label_by_profile["PATIENT"][$profile_type]."ID";// 患者/利用者
$csv_basehead[] = $_label_by_profile["PATIENT"][$profile_type]."氏名";// 患者/利用者
$csv_basehead[] = "プロブレム";
$csv_basehead[] = $_label_by_profile["SUMMARY_RECORD_KUBUN"][$profile_type];// 診療記録区分/記録区分
$csv_basehead[] = "テンプレート";
$csv_basehead[] = "記載者";
$csv_basehead[] = "職種";
$csv_basehead[] = "重要度";


//==============================
// データ検索
// 条件はsummary_search_data.phpと同様。
//==============================
$sql =
	" select".
	" summary.*".
	",empmst.emp_lt_nm, empmst.emp_ft_nm, empmst.emp_job".
	",jobmst.job_nm, ptifmst.ptif_lt_kaj_nm, ptifmst.ptif_ft_kaj_nm, tmplmst.tmpl_name".
	" from summary ".
	" left join empmst on empmst.emp_id = summary.emp_id ".
	" left join jobmst on jobmst.job_id = empmst.emp_job ".
	" left join ptifmst on ptifmst.ptif_id = summary.ptif_id ".
	" left join tmplmst on tmplmst.tmpl_id = summary.tmpl_id ".
	" left join divmst on divmst.diag_div = summary.diag_div ".
	" where 1=1";

$rows = array();

//==============================
// select_flg:1  検索条件で指定されたデータを抽出し、レコードセット取得
//==============================
if ($_REQUEST["select_flg"] == 1) {
	// 診療記録区分
	if (trim($_REQUEST["sel_diag"])) {
		$sql .= " and divmst.div_id = ".(int)$_REQUEST["sel_diag"];
	}
	// テンプレート
	if ($_REQUEST["tmpl_id"] == "-") {
		$sql .= " and summary.tmpl_id is null";
	}
	if ((int)$_REQUEST["tmpl_id"]) {
		$sql .= " and summary.tmpl_id = ".(int)$_REQUEST["tmpl_id"];
	}
	// 記載者
	if (trim($_REQUEST["recorder_id"])) {
		$sql .= " and summary.emp_id = '".pg_escape_string(trim($_REQUEST["recorder_id"]))."'";
	}
	// 開始年月
	if ((int)$_REQUEST["start_yr"] && (int)$_REQUEST["start_mon"] && (int)$_REQUEST["start_day"]) {
		$sql .= " and summary.cre_date >= ".( ((int)$_REQUEST["start_yr"])*10000 + ((int)$_REQUEST["start_mon"])*100 + (int)$_REQUEST["start_day"] );
	}
	// 終了年月
	if ((int)$_REQUEST["end_yr"] && (int)$_REQUEST["end_mon"] && (int)$_REQUEST["end_day"]) {
		$sql .= " and summary.cre_date <= ".( ((int)$_REQUEST["end_yr"])*10000 + ((int)$_REQUEST["end_mon"])*100 + (int)$_REQUEST["end_day"] );
	}
	// 開始月(妙な検索？）
	if (!(int)$_REQUEST["start_yr"] && (int)$_REQUEST["start_mon"] && !(int)$_REQUEST["start_day"]) {
		$arr_cond[] = "(substring(summary.cre_date from 5 for 2) >= '$start_mon')";
	}
	// 終了月(妙な検索？）
	if (!(int)$_REQUEST["end_yr"] == "-" && (int)$_REQUEST["end_mon"] != "-" && !(int)$_REQUEST["end_day"]) {
		$arr_cond[] = "(substring(summary.cre_date from 5 for 2) <= '$end_mon')";
	}
	// 記載内容
	if (trim($_REQUEST["search_str"]) != "") {
		$sql .= " and (1<>1";
		$arr_key = split(" ", $_REQUEST["search_str"]);
		for ($i=0; $i<count($arr_key); $i++) $sql .= " or smry_cmt like '%".pg_escape_string($arr_key[$i])."%'";
		$sql .= ")";
	}
	$sql .= " order by summary.cre_date desc, summary.summary_id desc";
	$sel = select_from_table($con, $sql, "", $fname);
	$rows = pg_fetch_all($sel);
}
//==============================
// select_flg:0  チェックボックスで指定されたデータを抽出し、レコードセット取得
// $idsには"{emp_id}_{pt_id}_{summary_id}"がカンマ区切りで設定
//==============================
else {
	$arr_id = split(",", $_REQUEST["ids"]);
	for ($i=0,$num=count($arr_id); $i<$num; $i++) {
		list($emp_id, $pt_id, $summary_id) = split("_", $arr_id[$i]."__");
		$sql2 = $sql . " and summary.summary_id = ".(int)$summary_id." and summary.ptif_id = '".pg_escape_string($pt_id)."'";
		$sel = select_from_table($con,$sql2,"",$fname);
		$rows[] = pg_fetch_array($sel);
	}
}



//==============================
// DBレコードセットをループし、各レコードのXMLを取得する。XMLの内容をCSVのための配列へ格納
//==============================
$csv_basedata = array();
$csv_adddata = array();
$csv_addhead = array();

foreach($rows as $i => $row){
	if (!$row) continue;

	// 共通データ
	$csv_basedata[$i] = array(
		substr($row["cre_date"], 0, 4)."/".substr($row["cre_date"], 4, 2)."/".substr($row["cre_date"], 6, 2),
		$row["ptif_id"],
		$row["ptif_lt_kaj_nm"]." ".$row["ptif_ft_kaj_nm"],
		$row["problem"],
		$row["diag_div"],
		$row["tmpl_name"],
		$row["emp_lt_nm"]." ".$row["emp_ft_nm"],
		$row["job_nm"],
		$row["priority"]
	);

	// 非共通データ（テンプレートの場合）
	if ($row["tmpl_id"] != "") {
		$xml_file_name =
			convertXmlFileName2NewVersion(
				$con, $fname, $row["emp_id"], $row["ptif_id"], $row["summary_id"],
				$row["emp_id"]."_".$row["ptif_id"]."_".$row["summary_id"].".xml",
				$row["summary_seq"], $row["smry_cmt"]
			);

		$xml = new template_xml_class();
		$doc = $xml->get_xml_object_array_from_db($con, $fname, $xml_file_name);
		$root = @$doc->nodes["template"];
		$csv_data = array();
		get_recursive_xml_for_csv($root, "", $csv_addhead, $csv_data);
		$csv_adddata[$i] = $csv_data;
	}
	// 非共通データ(フリー入力の場合）
	else {
		if (!in_array("フリー入力", $csv_addhead)) $csv_addhead[] = "フリー入力";
		$data = $row["smry_cmt"];
		$data = html_to_text_for_tinymce($data);
		$csv_adddata[$i] = $data;
	}
}

//==============================
// CSV用配列から、CSVを作成
//==============================
// CSVタイトルヘッダ（共通データヘッダ ＋ 非共通データヘッダ）
$out = implode(",", $csv_basehead) .",".implode(",", $csv_addhead)."\r\n";
// CSV行数ループ
for($i = 0; $i < count($csv_basedata); $i++){
	// 共通データフィールド
	$out .= '"'.implode('","', $csv_basedata[$i]).'"';
	// 非共通データフィールド
	foreach($csv_addhead as $idx => $key){
		$v = @$csv_adddata[$i][$key];
		$vi = (string)((int)$v);
		$vf = (string)((float)$v);
		if ($v !== $vi && $v !== $vf) $v = '"'.str_replace('"', '""', $v).'"'; //int/float変換後と厳密に同じでなければクォートする
		$out .= "," . $v;
	}
	$out .= "\r\n";
}

pg_close($con);


//==============================
// CSV文字列の標準出力
//==============================
if (strlen($out)){
	$out = mb_convert_encoding($out, "SJIS", "EUC-JP");
	header("Content-Disposition: attachment; filename=med_report.csv");
	header("Content-Type: application/octet-stream; name=med_report.csv");
	header("Content-Length: " . strlen($out));
	echo($out);
}



//------------------------------------------------------------------------------
// XMLノードを再帰的に、ヘッダ部とデータ部を取得する。それぞれ２次元配列に格納する。
//------------------------------------------------------------------------------
function get_recursive_xml_for_csv($parent_node, $header_prefix, &$outhead, &$outdata){
	if (!$parent_node) return;
	if ($header_prefix) $header_prefix .=":";
	foreach($parent_node->nodes as $idx => $node){
		if (!$node) break;
		$title = @$node->attr["disp"];
		if (!$title) $title = @$node->attr["main"];
		$title = $header_prefix . $title;
		$value = @$node->value;
		if (count($node->nodes)){
			get_recursive_xml_for_csv($node, $title, $outhead, $outdata);
		} else {
			if (!in_array($title, $outhead)) $outhead[] = $title;
			if (@$outdata[$title]) $outdata[$title] .= "、";
			@$outdata[$title] .= $value;
		}
	}
}



//------------------------------------------------------------------------------
// 旧データのXMLファイル名(2010/01まで)から、新データのXMLファイル名に変換。
// また、物理ファイルで動作させている「旧状態」であれば、DBに格納しなおす。
//------------------------------------------------------------------------------
function convertXmlFileName2NewVersion($con, $fname, $emp_id, $pt_id, $summary_id, $old_xml_file_name, $summary_seq, $smry_cmt){
	// summary_seqがあれば、新データである。xmlファイルをＤＢから取得。
	// ※新データ(2010/02以降)は、最新の登録ファイルが正しいとする。
	//   apply_idがあればapply_idの一番大きなファイルが正しいとする。

	if ($summary_seq){
		$xml_file = $summary_seq.".xml";
		$sql =
			" select * from (".
			"   select xml_file_name, coalesce(apply_id, 0) as apply_id, update_timestamp".
			"   from sum_xml".
			"   where summary_seq = " . (int)$summary_seq.
			" ) d".
			" order by apply_id desc, update_timestamp desc limit 1";
		$sel_sum_apply = select_from_table($con, $sql, "", $fname);
		$tmp_xml_file_name = @pg_fetch_result($sel_sum_apply, 0, "xml_file_name");
		if ($tmp_xml_file_name != "") $xml_file = $tmp_xml_file_name;
	}

	// summary_seqがなければ、summary_seqを作成して、物理xmlファイル方式を破棄。DBに格納しなおす。
	// もし新連番のsummary_seqがなければ登録する（2010/01からの仕様 ）
	else {
		$summary_seq = $c_sot_util->regist_summary_seq_if_not_exists(@$summary_id, @$pt_id, $con, $fname);
		$new_xml_file_name = $summary_seq .".xml";
		// apply_idが存在するかどうか取得
		$sql =
			" select apply_id from sum_apply".
			" where summary_id = ".$summary_id.
			" and ptif_id = ".$pt_id.
			" and delete_flg = 'f'";
		$sel3  = select_from_table($con, $sql, "", $fname);
		$apply_id = @pg_fetch_result($sel3, 0, 0);
		if ($apply_id) $new_xml_file_name = $summary_seq."_".$apply_id.".xml";

		// もし旧物理XMLファイルが存在し、ＤＢにXMLデータがなければＤＢに登録
		$sql = "select count(*) as cnt from sum_xml where xml_file_name = '".$xml_file."'";
		$sel2  = select_from_table($con, $sql, "", $fname);
		if (!pg_fetch_result($sel2, 0, "cnt")){
			if (file_exists("./docArchive/xml/".$xml_file)){
				$smry_xml_lines = array();
				if ($fp = fopen ("./docArchive/xml/".$xml_file, "r")) {
					while (!feof($fp)) $smry_xml_lines[] = mb_convert_encoding(fgets($fp, 4096), "EUC-JP", "UTF-8");
					fclose($fp);
					$smry_xml = implode("", $smry_xml_lines);
					$sql =
						" insert into sum_xml (".
						" emp_id, xml_file_name, summary_id, smry_xml, smry_html, summary_seq, ptif_id, is_work_data, update_timestamp, apply_id) values (".
						" '". $emp_id. "'".
						",'". $new_xml_file_name. "'".
						",'". $sumary_id. "'".
						",'". pg_escape_string($smry_xml) ."'".
						",'". pg_escape_string($smry_cmt) ."'".
						",'". $summary_seq. "'".
						",'". $pt_id. "'".
						",current_timestamp".
						",".($apply_id ? (int)$apply_id : "null").
						",0".
					$ins = insert_into_table($con, $sql, array(), $fname);
					$xml_file = $new_xml_file_name;
				}
			}
		}
	}
	return $xml_file;
}

?>
