<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_session.php");
require("get_values.ini");
require("show_clock_in_common.ini");
require_once("about_postgres.php");
require_once("show_return_apply_detail.ini");
require_once("show_return_apply_history.ini");
require_once("application_imprint_common.ini");
require_once("timecard_bean.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// ログインユーザの職員IDを取得
$emp_id = get_emp_id($con, $session, $fname);

if($target_apply_id == "") {
	$target_apply_id = $apply_id;
}


// 退勤後復帰申請情報を取得
$sql = "select empmst.emp_lt_nm, empmst.emp_ft_nm, rtnapply.target_date, rtnapply.reason_id, rtnapply.reason, rtnapply.apply_time, rtnapply.comment from rtnapply inner join empmst on rtnapply.emp_id = empmst.emp_id";
$cond = "where rtnapply.apply_id = $target_apply_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$apply_emp_nm = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");
$target_date = pg_fetch_result($sel, 0, "target_date");
$reason_id = pg_fetch_result($sel, 0, "reason_id");
$reason = pg_fetch_result($sel, 0, "reason");
$apply_time = pg_fetch_result($sel, 0, "apply_time");
$comment = pg_fetch_result($sel, 0, "comment");

// 勤務実績情報を取得
$arr_result = get_atdbkrslt($con, $emp_id, $target_date, $fname);



// 退勤後復帰理由一覧を取得
$sql = "select reason_id, reason from rtnrsn";
$cond = "where del_flg = 'f' order by reason_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
//タイムカード情報の取得
$timecard_bean = new timecard_bean();
$timecard_bean->select($con, $fname);
$ret_str = ($timecard_bean->return_icon_flg != "2") ? "退勤後復帰" : "呼出勤務";
?>
<title>CoMedix タイムカード | <?=$ret_str?>再申請</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
<script type="text/javascript">

function setOtherReasonDisabled() {
	if (document.getElementById('other_reason')) {
		document.getElementById('other_reason').style.display = (document.mainform.reason_id.value == 'other') ? '' : 'none';
	}
}

function history_select(apply_id, target_apply_id) {

	document.mainform.apply_id.value = apply_id;
	document.mainform.target_apply_id.value = target_apply_id;
	document.mainform.action="return_back.php";
	document.mainform.submit();
}

function re_apply() {

	if (confirm('再申請します。よろしいですか？')) {
		document.mainform.submit();
	}
}

// テキストエリア行数の自動拡張
var ResizingTextArea = Class.create();

ResizingTextArea.prototype = {
    defaultRows: 1,

    initialize: function(field)
    {
        this.defaultRows = Math.max(field.rows, 1);
        this.resizeNeeded = this.resizeNeeded.bindAsEventListener(this);
        Event.observe(field, "click", this.resizeNeeded);
        Event.observe(field, "keyup", this.resizeNeeded);
    },

    resizeNeeded: function(event)
    {
        var t = Event.element(event);
        var lines = t.value.split('\n');
        var newRows = lines.length + 1;
        var oldRows = t.rows;
        for (var i = 0; i < lines.length; i++)
        {
            var line = lines[i];
            if (line.length >= t.cols) newRows += Math.floor(line.length / t.cols);
        }
        if (newRows > t.rows) t.rows = newRows;
        if (newRows < t.rows) t.rows = Math.max(this.defaultRows, newRows);

        // 履歴表示table高さ調整
        resize_history_tbl();
    }
}

// 全テキストエリアの行数変更
function resizeAllTextArea() {
	var objs = document.getElementsByTagName('textarea');
	for (var i = 0, j = objs.length; i < j; i++) {
	    var t = objs[i];
		var defaultRows = Math.max(t.rows, 1);
	    var lines = t.value.split('\n');
	    var newRows = lines.length + 1;
	    var oldRows = t.rows;
	    for (var k = 0; k < lines.length; k++)
	    {
	        var line = lines[k];
	        if (line.length >= t.cols) newRows += Math.floor(line.length / t.cols);
	    }
	    if (newRows > t.rows) t.rows = newRows;
	    if (newRows < t.rows) t.rows = Math.max(defaultRows, newRows);
	}
}


// 履歴表示tableの高さ調節
function resize_history_tbl()
{
    document.getElementById('history_tbl').style.height = document.getElementById('dtl_tbl').offsetHeight;
}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}

table.block {border-collapse:collapse;}
table.block td {border:#5279a5 solid 1px;padding:1px;}
table.block td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#5279a5 solid 0px;}


table.block2 {border-collapse:collapse;}
table.block2 td {border:#5279a5 solid 1px;padding:1px;}
table.block2 td {border-width:1;}

table.block3 {border-collapse:collapse;}
table.block3 td {border:#5279a5 solid 0px;}
table.block3 td {border-width:0;}

p {margin:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="setOtherReasonDisabled();resizeAllTextArea();resize_history_tbl();">
<center>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b><?=$ret_str?>再申請</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="page_close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form name="mainform" action="return_back_exe.php" method="post">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td valign="top" width="25%">
<? show_return_history_for_apply($con, $session, $fname, $apply_id, $target_apply_id);?>
</td>
<td><img src="img/spacer.gif" width="5" height="2" alt=""></td>
<td valign="top" width="75%">
<div id="dtl_tbl">
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="180" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請日時</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})\d{2}$/", "$1/$2/$3 $4:$5", $apply_time)); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務日</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $target_date)); ?></font></td>
</tr>
<?
for ($i = 1; $i <= 10; $i++) {
	if ($arr_result["o_start_time$i"] == "") {
		break;
	}
?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$ret_str?>時刻<? echo($i); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(preg_replace("/^(\d{2})(\d{2})$/", "$1:$2", $arr_result["o_start_time$i"])); ?> 〜 <? echo(preg_replace("/^(\d{2})(\d{2})$/", "$1:$2", $arr_result["o_end_time$i"])); ?></font></td>
</tr>
<?
}
?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">理由</font></td>
<td>
<select name="reason_id" onchange="setOtherReasonDisabled();">
<?
while ($row = pg_fetch_array($sel)) {
	$tmp_reason_id = $row["reason_id"];
	$tmp_reason = $row["reason"];

	echo("<option value=\"$tmp_reason_id\"");
	if ($tmp_reason_id == $reason_id) {echo(" selected");}
	echo(">$tmp_reason\n");
}
?>
<option value="other"<? if ($reason_id == "") {echo(" selected");} ?>>その他
</select><br>
<div id="other_reason" style="display:none;"><input type="text" name="reason" value="<? echo($reason); ?>" size="50" maxlength="100" style="ime-mode:active;"></div>
</td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請者コメント</font></td>
<td><textarea name="comment" rows="5" cols="40" style="ime-mode:active;" onFocus="new ResizingTextArea(this);"><? echo($comment); ?></textarea></td>
</tr>

<?
$page = "return_back";
show_application_apply_detail($con, $session, $fname, $target_apply_id, $page);
?>

</td>
</tr>
</table>


<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="apply_id" value="<? echo($apply_id); ?>">
<input type="hidden" name="target_apply_id" value="<? echo($target_apply_id); ?>">
<input type="hidden" name="pnt_url" value="<? echo($pnt_url); ?>">
</form>
</center>
</body>
</html>
<?
$approve_cnt = 0;
if($mode == 'show_flg_update') {

	$sql = "select * from rtnapply";
	$cond = "where apply_id = $apply_id and apv_bak_show_flg = 't' and apply_status = '3'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$approve_cnt = pg_numrows($sel);
	if($approve_cnt > 0) {

		// トランザクションを開始
		pg_query($con, "begin");
		
		$sql = "update rtnapply set";
		$set = array("apv_bak_show_flg");
		$setvalue = array('f');
		$cond = "where apply_id = $apply_id and apv_bak_show_flg = 't' and apply_status = '3'";	
		$up = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if($up ==0){
			pg_exec($con,"rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		
		// トランザクションをコミット
		pg_query($con, "commit");
	}
}
pg_close($con);
?>

<script type="text/javascript">
function page_close() {
<?
if($mode == 'show_flg_update' && $approve_cnt > 0) {
?>	
	if(window.opener && !window.opener.closed && window.opener.reload_page) {
		window.opener.reload_page();
		window.close();
	} else {
		window.close();
	}
<?	
} else {
?>	
	window.close();
<?
}
?> 

}
</script>