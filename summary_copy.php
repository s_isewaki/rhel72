<?
//print_r($_POST);

//ソート条件定数
define("SORT_CRE_DATE", "cre_date");
define("SORT_PROBLEM" , "problem");
define("SORT_DIAG_DIV", "diag_div");
define("SORT_KISAISHA", "kisaisha");
define("SORT_SHOKUSHU", "shokushu");
define("SORT_PRIORITY", "priority");
define("SORT_TMPLATE" , "template");
define("SORT_PT"      , "pt");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<?
require_once("about_comedix.php");
require_once("label_by_profile_type.ini");
require_once("summary_tmpl_util.ini");
require_once("summary_tmpl_copy_util.ini");
require_once("get_values.php");
require_once("summary_common.ini");
require_once("get_menu_label.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 57, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// ログインユーザーID
$login_emp_id = get_emp_id($con,$session,$fname);

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// メドレポート/ＰＯレポート(Problem Oriented)
$med_report_title = get_report_menu_label($con, $fname);

//画面情報
$page_title = "コピー選択";

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//送信データ解析
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//エントリーリスト
$entry_list = convert_entry_list(@$tmpl_copy_entry_info);

//オープン情報
$opened_data_line_id = null;
if(@$opened_data_line_id_csv != "")
{
	$opened_data_line_id = split(',',$opened_data_line_id_csv);
}
$opened_entry_line_id = null;
if(@$opened_entry_line_id_csv != "")
{
	$opened_entry_line_id = split(',',$opened_entry_line_id_csv);
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//ポストバック時の処理
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
if(@$is_postback == "true")
{
	//コピー対象にエントリー
	if($postback_mode == 'entry')
	{
		if($add_tag_id != "")
		{
			$add_tag_id_arr = split(",",$add_tag_id);
		}
		else
		{
			$add_tag_id_arr = "";
		}

		//既にエントリー済みのデータの場合は取り除き、追加分にマージ
		$is_new_summary = true;
		$work_entry_list = null;
		foreach((array)$entry_list as $entry_list1) {
			if (!$entry_list1) break;
			if($entry_list1['summary_id'] != $add_summary_id || $entry_list1['pt_id'] != $add_pt_id) {
				$work_entry_list[] = $entry_list1;
			}
			else
			{
				$is_new_summary = false;
				foreach($entry_list1['tag_id'] as $tag_id1)
				{
					if(!in_array($tag_id1,$add_tag_id_arr))
					{
						$add_tag_id_arr[] = $tag_id1;
					}
				}
				asort($add_tag_id_arr);//昇順
			}
		}
		$entry_list = $work_entry_list;

		//エントリーリストの末尾に追加
		$entry_list1 = null;
		$entry_list1['pt_id'] = $add_pt_id;
		$entry_list1['summary_id'] = $add_summary_id;
		$entry_list1['summary_seq'] = $add_summary_seq;
		$entry_list1['tag_id'] = $add_tag_id_arr;
		$entry_list[] = $entry_list1;

		//新規summary_idの追加の場合はオープン情報を引き継ぐ
		if($is_new_summary)
		{
			if(in_array($add_pt_id.':'.$add_summary_id, (array)$opened_data_line_id)) {
				$opened_entry_line_id[] = $add_pt_id.':'.$add_summary_id;
			}
		}
		//コピーXMLを作成(上書き作成)
		create_copy_xml2($con,$fname,$entry_list,$login_emp_id, $pt_id);
	}

	//コピー対象をキャンセル
	elseif($postback_mode == 'cancel') {
		if($add_tag_id != "") {
			$add_tag_id_arr = split(",",$add_tag_id);
		} else {
			$add_tag_id_arr = "";
		}


		$work_entry_list = null;
		foreach($entry_list as $entry_list1) {
			if($entry_list1['summary_id'] != $add_summary_id || $entry_list1['pt_id'] != $add_pt_id) {
				$work_entry_list[] = $entry_list1;
			} else {
				$work_tag_id = null;
				foreach($entry_list1['tag_id'] as $tag_id1) {
					if(!in_array($tag_id1,$add_tag_id_arr)) {
						$work_tag_id[] = $tag_id1;
					}
				}

				//全タグキャンセルされた場合
				if($work_tag_id == null) {
					//エントリー対象外。

					//オープン情報も削除。
					$opened_entry_line_id2 = null;
					foreach((array)$opened_entry_line_id as $opened_entry_line_id1) {
						if (!$opened_entry_line_id1) break;
						if($add_pt_id.':'.$add_summary_id != $opened_entry_line_id1) {
							$opened_entry_line_id2[] = $opened_entry_line_id1;
						}
					}
					$opened_entry_line_id = $opened_entry_line_id2;
				}
				//一部のタグがキャンセルされずに残る場合
				else
				{
					//残ったタグはエントリーのまま
					$entry_list1['tag_id'] = $work_tag_id;
					$work_entry_list[] = $entry_list1;
				}
			}
		}
		$entry_list = $work_entry_list;

		//コピーXMLを作成(上書き作成)
		create_copy_xml2($con,$fname,$entry_list,$login_emp_id, $pt_id);
	}

	//検索
	elseif($postback_mode == 'search')
	{
		//検索済み指定
		$is_searched = "true";

		//抽出条件に設定
		$search2_pt_name  = $search_pt_name;
		$search2_emp_name = $search_emp_name;
		$search2_div_id   = $search_div_id;
		$search2_problem  = $search_problem;
	}

	//患者本人・他患者変更
	elseif($postback_mode == 'pt_mode_change')
	{
		//未検索指定
		$is_searched = "";

		//抽出条件をクリア
		$search2_pt_name  = "";
		$search2_emp_name = "";
		$search2_div_id   = "";
		$search2_problem  = "";

		if (!@$_REQUEST["search_div_id"]) {
			$search_div_id = @$_REQUEST["div_id"];
			$search2_div_id   = $search_div_id;
			$is_searched = "true";
		}

		if (!@$_REQUEST["search_emp_name"]) {
			$sel = select_from_table($con, "select emp_lt_nm, emp_ft_nm from empmst where emp_id = '".$login_emp_id."'", "", $fname);
			$search_emp_name = trim(@pg_fetch_result($sel, 0, 0) . " " . @pg_fetch_result($sel, 0, 1));
			$search2_emp_name   = $search_emp_name;
			$is_searched = "true";
		}
	}

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//初期値
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
if(@$pt_mode == "")
{
	$pt_mode = 'self';
}
if(@$target_month == "")
{
	$date_wk = mktime(0,0,0,date('m'), date('d'), date('Y'));
	$year_wk = date('Y', $date_wk);
	$month_wk = date('m', $date_wk);
	$target_month = $year_wk.$month_wk;
}
if(@$month_hani == "")
{
	$month_hani = "1";
}
if(@$sort == "")
{
	$sort = "desc_cre_date";
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//検索／関連情報
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//========================================
//作成日の範囲決定
//========================================

//開始
$from_date = add_yyyymm($target_month,1-$month_hani);
$from_date .="00";

//終了
$to_date = $target_month."99";

//========================================
//前へ／次への移動日を決定
//========================================
$next_target_month = add_yyyymm($target_month,$month_hani);
$prev_target_month = add_yyyymm($target_month,-$month_hani);

//========================================
//ソート条件の解析／ソートSQL作成
//========================================

//ソート解析
$sort_item = $sort;
$desc_value = "";
if( substr($sort,0,5) == "desc_" )
{
	$sort_item = substr($sort,5);
	$desc_value = "desc";
}

//ソートSQL
$sql_sort_order = "";
switch($sort_item)
{
	case SORT_CRE_DATE:
		$sql_sort_order = "cre_date $desc_value";
		break;
	case SORT_PROBLEM:
		$sql_sort_order = "problem $desc_value";
		break;
	case SORT_DIAG_DIV:
		$sql_sort_order = "diag_div $desc_value";
		break;
	case SORT_KISAISHA:
		$sql_sort_order = "emp_kn_lt_nm $desc_value , emp_kn_ft_nm $desc_value";
		break;
	case SORT_SHOKUSHU:
		$sql_sort_order = "job_nm $desc_value";
		break;
	case SORT_PRIORITY:
		$sql_sort_order = "priority $desc_value";
		break;
	case SORT_TMPLATE:
		$sql_sort_order = "tmpl_name $desc_value";
		break;
	case SORT_PT:
		$sql_sort_order = "ptif_lt_kana_nm $desc_value , ptif_ft_kana_nm $desc_value";
		break;
	default:
		//何もしない。
		break;
}
if($sql_sort_order != "")
{
	$sql_sort_order .= ",";
}
$sql_sort_order .= "ptif_id,summary_id";

//ソートリンク用、リンク先ソート値。
$sort_link_cre_date = ($sort == SORT_CRE_DATE) ? "desc_".SORT_CRE_DATE : SORT_CRE_DATE;
$sort_link_problem  = ($sort == SORT_PROBLEM ) ? "desc_".SORT_PROBLEM  : SORT_PROBLEM ;
$sort_link_diag_div = ($sort == SORT_DIAG_DIV) ? "desc_".SORT_DIAG_DIV : SORT_DIAG_DIV;
$sort_link_kisaisha = ($sort == SORT_KISAISHA) ? "desc_".SORT_KISAISHA : SORT_KISAISHA;
$sort_link_shokushu = ($sort == SORT_SHOKUSHU) ? "desc_".SORT_SHOKUSHU : SORT_SHOKUSHU;
$sort_link_priority = ($sort == SORT_PRIORITY) ? "desc_".SORT_PRIORITY : SORT_PRIORITY;
$sort_link_tmplate  = ($sort == SORT_TMPLATE ) ? "desc_".SORT_TMPLATE  : SORT_TMPLATE;
$sort_link_pt       = ($sort == SORT_PT      ) ? "desc_".SORT_PT       : SORT_PT;


//========================================
//検索SQL
//========================================
$search2_diag_div = "";
if(@$search2_div_id != ""){
	$sel = select_from_table($con, "select diag_div from divmst where div_id = ".(int)$search2_div_id, "", $fname);
	$search2_diag_div = pg_fetch_result($sel, 0, 0);
}

//他患者未検索時以外の場合
if( !($pt_mode == 'other' && $is_searched != "true")){
	$sql  =  "select * from";
	$sql .= " (";
	$sql .= " select * from summary";
	$sql .= " where cre_date >= '$from_date' and cre_date <= '$to_date'";
	if($pt_mode == 'self')
	{
		$sql .= " and ptif_id = '$pt_id'";
	}
	else
	{
		$sql .= " and ptif_id <> '$pt_id'";
	}
	if(@$search2_diag_div != "")
	{
		$sql .= " and diag_div like '".pg_escape_string($search2_diag_div)."'";
	}
	if(@$search2_problem != "")
	{
		$sql .= " and problem like '%".pg_escape_string($search2_problem)."%'";
	}
	$sql .= " ) s";


	$sql .= " natural left join ";
	$sql .= " (";
	$sql .= " select tmpl_id,tmpl_name,tmpl_file from tmplmst";
	$sql .= " ) t";


	$sql .= " natural inner join";
	$sql .= " (";
	$sql .= " select ptif_id,ptif_lt_kana_nm,ptif_ft_kana_nm,ptif_lt_kaj_nm,ptif_ft_kaj_nm from ptifmst where not ptif_del_flg";
	if(@$search2_pt_name != "")
	{
		$sql .= " and";
		$sql .= " (";
		$sql .= " (ptif_lt_kana_nm || ptif_ft_kana_nm) like '%".pg_escape_string($search2_pt_name)."%'";
		$sql .= " or";
		$sql .= " (ptif_lt_kaj_nm || ptif_ft_kaj_nm) like '%".pg_escape_string($search2_pt_name)."%'";
		$sql .= " )";
	}
	$sql .= " ) p";


	$sql .= " natural inner join";
	$sql .= " (";
	$sql .= " select emp_id,emp_lt_nm,emp_ft_nm,emp_kn_lt_nm,emp_kn_ft_nm,emp_job from empmst where true";
	if(@$search2_emp_name != "") {
		$aname = explode(" ", @$search2_emp_name);
		foreach ($aname as $rname){
			if ($rname=="") continue;
			$sql .= " and";
			$sql .= " (";
			$sql .= " (emp_lt_nm || emp_ft_nm) like '%".pg_escape_string($rname)."%'";
			$sql .= " or";
			$sql .= " (emp_kn_lt_nm || emp_kn_ft_nm) like '%".pg_escape_string($rname)."%'";
			$sql .= " )";
		}
	}
	$sql .= " ) e";


	$sql .= " natural left join";
	$sql .= " (";
	$sql .= " select job_id as emp_job,job_nm from jobmst";
	$sql .= " ) j";


	$sql .= " order by $sql_sort_order";

	$sel = select_from_table($con, $sql, "", $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$data_list = pg_fetch_all($sel);
}
//他患者初期表示の場合
else
{
	//一覧は0件表示。
	$data_list = "";
}




//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//その他
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//エントリーリストをCSV化
$tmpl_copy_entry_info = convert_tmpl_copy_entry_info($entry_list);


//オープン情報
$opened_data_line_id_csv = join((array)@$opened_data_line_id,',');
$opened_entry_line_id_csv = join((array)@$opened_entry_line_id,',');


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//HTML出力
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
?>
<title>CoMedix <?=$med_report_title?> | <?=$page_title?></title>

<script type="text/javascript" src="js/summary_copy_popup.js"></script>
<script type="text/javascript">

//--------------------------------------------------
//全体処理
//--------------------------------------------------

var is_page_loaded = false;

//起動時の処理
function loadaction()
{
//	//オープン情報を更新
//	saveOpenClose();

	is_page_loaded = true;

<? if($pt_mode == 'other' && $postback_mode == 'pt_mode_change'){ ?>
	document.mainform.search_emp_name.focus();
	document.mainform.search_emp_name.select();
<? } ?>
}

//次のページへ
function next_page() {
<? if($entry_list == "") { ?>
	alert('コピー内容が指定されていません。');
<? } else {
	//フリー記載のみが選択されている場合はデフォルトでフリー記載を表示
	$tmpl_id_param = $tmpl_id;
	if(is_free_input_only_entry_list($entry_list)) {
		$tmpl_id_param = 'free';
	}
?>
	var url = 'summary_tmpl_read.php'
		+ '?session=<?=$session?>'
		+ '&pt_id=<?=$pt_id?>'
		+ '&emp_id=<?=$emp_id?>'
		+ '&enc_diag=<?=@$enc_diag?>'
		+ '&div_id=<?=@$div_id?>'
		+ '&tmpl_id=<?=$tmpl_id_param?>'
		+ '&summary_id=<?=$summary_id?>'
		+ '&update_flg=<?=$update_flg?>'
		+ '&xml_file=<?=$xml_file?>'
		+ '&is_copy=true'
		+ '&tmpl_copy_xml_file=wk_<?=$login_emp_id?>.xml'
		+ '&tmpl_copy_entry_info=<?=$tmpl_copy_entry_info?>'
		;
	if (window.opener.name=="tmpl_window" && window.name=="tmpl_copy_window" && !window.opener.closed){
		window.opener.location.href = url;
		window.close();
	} else {
		location.href = url;
	}
<?
}
?>
}

//--------------------------------------------------
//ポストバック関係
//--------------------------------------------------

//現在の内部情報でポストバック
function postback() {
	document.mainform.submit();
}


//本人／他患者の変更
function pt_change(pt_mode) {
	document.mainform.postback_mode.value = "pt_mode_change";
	document.mainform.pt_mode.value = pt_mode;
	//検索結果のオープン情報をクリア
	document.mainform.opened_data_line_id_csv.value = "";
	postback();
}

//ヶ月範囲の変更
function change_month_hani(month_hani)
{
	document.mainform.month_hani.value = month_hani;
	postback();
}

//対象ヶ月の変更
function change_target_month(target_month)
{
	document.mainform.target_month.value = target_month;
	postback();
}

//ソート条件を変更
function change_sort(sort)
{
	document.mainform.sort.value = sort;
	postback();
}

//検索
function search_start()
{
	document.mainform.postback_mode.value = "search";

	//検索結果のオープン情報をクリア
	document.mainform.opened_data_line_id_csv.value = "";

	postback();
}


//コピー対象にエントリー
function entry_data_line(pt_id,summary_id,tag_id,summary_seq)
{
	document.mainform.postback_mode.value = "entry";
	document.mainform.add_pt_id.value = pt_id;
	document.mainform.add_summary_id.value = summary_id;
	document.mainform.add_summary_seq.value = summary_seq;
	document.mainform.add_tag_id.value = tag_id;
	postback();
}

//コピー対象をキャンセル
function cancel_data_line(pt_id,summary_id,tag_id)
{
	document.mainform.postback_mode.value = "cancel";
	document.mainform.add_pt_id.value = pt_id;
	document.mainform.add_summary_id.value = summary_id;
	document.mainform.add_tag_id.value = tag_id;
	postback();
}


//--------------------------------------------------
//一覧の「＋」アイコン制御
//--------------------------------------------------
//親trのid属性
//　{グループ名}+「_tr_」+{id値}
//子trのclass属性
//　親trのid属性と同値
//imgの外側のdivタグのclass属性
//　「summary_id_」+{summary_id値}
//その外側のdivタグのclass属性
//　「pt_id_」+{pt_id値}
//アイコンimgのid属性
//　{グループ名}+「_img_」+{id値}
//アイコンimgのclass属性
//　'open'/'close' ※スタイルシートで画像定義


//「＋」アイコンがクリックされた時の処理を行います。
function changeOpenClose(img) {
	img.className = (img.className == 'close') ? 'open' : 'close';
	var row_id = img.id.replace('img', 'tr');
	setChildOpenClose(row_id);
	saveOpenClose();
}

//指定行(親)の状態と合致するよう子行を変更します。
function setChildOpenClose(parent_row_id) {
	var img = document.getElementById(parent_row_id.replace('tr', 'img'));
	if (!img) {
		return;
	}

	var rows = document.getElementById(parent_row_id).parentNode.rows;
	for (var i = 0, j = rows.length; i < j; i++) {
		if (rows[i].className == parent_row_id) {
			if (img.className == 'open') {
				rows[i].style.display = '';
			} else {
				rows[i].style.display = 'none';
			}
		}
	}
}

//展開情報をhiddenに保存します。
function saveOpenClose()
{
	var opened_data_line_id_csv = "";
	var opened_entry_line_id_csv = "";

	var imgs = document.getElementsByTagName('img');
	for(var i = 0; i < imgs.length; i++)
	{
		var img = imgs[i];
		if(img.className == 'open')
		{
			var imginfo = img.id.split("_img_");
			var list_name = imginfo[0];

			var s_p_id = img.parentNode.className.substr(14);//先頭summary_pt_id_を除く
			var s_p_id_separate_index = s_p_id.indexOf("_");
			var summary_id = s_p_id.substr(0,s_p_id_separate_index);
			var pt_id = s_p_id.substr(s_p_id_separate_index + 1);

			if(imginfo[0] == "list_data")
			{
				if(opened_data_line_id_csv != "")
				{
					opened_data_line_id_csv += ",";
				}
				opened_data_line_id_csv += pt_id + ":" + summary_id;
			}
			else
			{
				if(opened_entry_line_id_csv != "")
				{
					opened_entry_line_id_csv += ",";
				}
				opened_entry_line_id_csv += pt_id + ":" + summary_id;
			}
		}
	}

	document.mainform.opened_data_line_id_csv.value = opened_data_line_id_csv;
	document.mainform.opened_entry_line_id_csv.value = opened_entry_line_id_csv;
}

//--------------------------------------------------
//一覧の行ハイライト
//--------------------------------------------------
//tdのclass属性
//　{グループ名}


function highlightCells(class_name) {
	changeCellsColor(class_name, '#fefc8f');
}
function dehighlightCells(class_name) {
	changeCellsColor(class_name, '');
}
function changeCellsColor(class_name, color) {
	var cells = document.getElementsByTagName('td');
	for (var i = 0, j = cells.length; i < j; i++) {
		if (cells[i].className != class_name) {
			continue;
		}
		cells[i].style.backgroundColor = color;
	}
}



</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

.list {border-collapse:collapse;border:#5279a5 solid 1px;}
.list td {border:#5279a5 solid 1px;}
.inner td {border-style:none;}
.non_in_list {border-collapse:collapse;}
.non_in_list td {border:0px;}

img.close
{
	background-image:url("img/icon/plus.gif");
	vertical-align:middle;
}
img.open
{
	background-image:url("img/icon/minus.gif");
	vertical-align:middle;
}

</style>
</head>
<body onload="loadaction()">

<form name="mainform" action="summary_copy.php" method="post">
<!-- 画面パラメータ -->
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="pt_id" value="<?=$pt_id?>">
<input type="hidden" name="emp_id" value="<?=$emp_id?>">
<input type="hidden" name="enc_diag" value="<?=@$enc_diag?>">
<input type="hidden" name="div_id" value="<?=@$div_id?>">
<input type="hidden" name="tmpl_id" value="<?=@$tmpl_id?>">
<input type="hidden" name="summary_id" value="<?=@$summary_id?>">
<input type="hidden" name="xml_file" value="<?=@$xml_file?>">
<input type="hidden" name="update_flg" value="<?=@$update_flg?>">
<!-- ポストバック情報 -->
<input type="hidden" name="is_postback" value="true">
<input type="hidden" name="postback_mode" value="">
<input type="hidden" name="pt_mode" value="<?=$pt_mode?>">
<input type="hidden" name="target_month" value="<?=$target_month?>">
<input type="hidden" name="month_hani" value="<?=$month_hani?>">
<input type="hidden" name="sort" value="<?=$sort?>">
<input type="hidden" name="tmpl_copy_entry_info" value="<?=$tmpl_copy_entry_info?>">
<input type="hidden" name="is_searched"  value="<?=@$is_searched?>" >
<input type="hidden" name="search2_pt_name"  value="<?=@$search2_pt_name?>" >
<input type="hidden" name="search2_emp_name" value="<?=@$search2_emp_name?>">
<input type="hidden" name="search2_diag_div" value="<?=@$search2_diag_div?>">
<input type="hidden" name="search2_div_id" value="<?=@$search2_div_id?>">
<input type="hidden" name="search2_problem"  value="<?=@$search2_problem?>" >
<input type="hidden" name="opened_data_line_id_csv" value="<?=@$opened_data_line_id_csv?>">
<input type="hidden" name="opened_entry_line_id_csv" value="<?=@$opened_entry_line_id_csv?>">
<!-- エントリー／キャンセル専用 -->
<input type="hidden" name="add_pt_id" value="">
<input type="hidden" name="add_summary_id" value="">
<input type="hidden" name="add_summary_seq" value="">
<input type="hidden" name="add_tag_id" value="">


<div style="padding:4px">


<!-- ヘッダエリア -->
<?= summary_common_show_dialog_header($page_title); ?>





	<!-- 本人他患者 START -->
	<?
	$pt_default_style    = "background-color:#BDD1E7;color:#000000;border:#5279A5 solid 1px;font-weight:normal";
	$pt_selected_style   = "background-color:#5279A5;color:#FFFFFF;border:#5279A5 solid 1px;font-weight:bold";
	$pt_default_color = "#000000";
	$pt_selected_color = "#FFFFFF";
	if($pt_mode == 'self')
	{
		$pt_self_style  = $pt_selected_style;
		$pt_other_style = $pt_default_style;
		$pt_self_color  = $pt_selected_color;
		$pt_other_color = $pt_default_color;
	}
	else
	{
		$pt_self_style  = $pt_default_style;
		$pt_other_style = $pt_selected_style;
		$pt_self_color  = $pt_default_color;
		$pt_other_color = $pt_selected_color;
	}
	?>

<!-- 次の画面へリンク、本人他患者情報ボタン -->
<div>
<div style="border-bottom:1px solid #<?=$pt_mode=='self'?"ddddff":"edd8d6"?>">
<div style="border-bottom:1px solid #<?=$pt_mode=='self'?"bfbfff":"dfb7b3"?>">
<div style="border-bottom:2px solid #<?=$pt_mode=='self'?"9b9bff":"d39c96"?>">
<table style="margin-top:8px; width:100%; font-size:14px">
	<tr>
		<td style="width:100px;"><div style="margin-top:2px;  background-color:#9b9bff; color:#fff; text-align:center; cursor:pointer; padding:2px" onmouseover="this.style.backgroundColor='#b3b3ff'" onmouseout="this.style.backgroundColor='#9b9bff'" onmousedown="pt_change('self');">本人情報</div></td>
		<td style="width:100px"><div style="margin-left:4px; margin-top:2px; background-color:#d39c96; color:#fff; text-align:center; cursor:pointer; padding:2px" onmouseover="this.style.backgroundColor='#dbada8'" onmouseout="this.style.backgroundColor='#d39c96'" onmousedown="pt_change('other');">他患者情報</div></td>
		<td></td>
		<td align="right"><a href="javascript:next_page()" style="color:#f93f00"><b>次の画面へ&nbsp;≫</b></a></td>
	</tr>
</table>
</div>
</div>
</div>





<? if($pt_mode == 'other'){ ?>
<!-- 検索 START -->
<div class="dialog_searchfield">
	<table>
		<tr>
			<td>患者氏名</td>
			<td><input tyep="text" name="search_pt_name" value="<?=h(@$search_pt_name)?>" style="width:100px; ime-mode:active"></td>
			<td>記載者</td>
			<td><input tyep="text" name="search_emp_name" value="<?=h(@$search_emp_name)?>" style="width:100px; ime-mode:active"></td>
			<td>診療記録区分</td>
			<td>
				<? $div_info = summary_common_get_sinryo_kubun_info($con, $fname, $session, $emp_id, @$search_div_id, "yes", ""); // 参照権限つきリスト?>
				<select name="search_div_id" id="search_div_id">
					<option value=""></option>
					<?= $div_info["dropdown_options"]?>
				</select>
			</td>
			<td>プロブレム</td>
			<td><input tyep="text" name="search_problem" value="<?=h(@$search_problem)?>" style="width:100px; ime-mode:active"></td>
			<td><input type="button" onclick="search_start();" value="検索"/></td>
		</tr>
	</table>
</div>
<!-- 検索 END -->
<? } ?>



<!-- ヶ月選択 START -->
<? $arr_mon[$month_hani] = "background-color:#f8fdb7; border:1px solid #c5d506"; ?>
<table style="width:100%">
	<tr>
		<td style="width:130px; text-align:center">
			<div style="background-color:#fbffe8; padding-top:5px; padding-bottom:5px; margin-top:10px; border-right:2px solid #ddd">
			<table style="width:100%;">
				<tr><td style="padding:2px">
					&nbsp;<?=(int)substr($target_month,0,4)?>&nbsp;年&nbsp;<?=(int)substr($target_month,4,2)?>&nbsp;月
				</td></tr>
			</table>
			</div>
		</td>
		<td>
			<div style="background-color:#f4f4f4; padding-top:5px; padding-bottom:5px; margin-top:10px">
			<table style="width:100%;">
				<tr>
					<td width='20%' style="text-align:center">
						<a href="javascript:change_target_month(<?=$prev_target_month?>);">＜前ヶ月</a>
					</td>
					<td width='15%' style="text-align:center; padding:2px; <?=@$arr_mon[1]?>">
						<a href="javascript:change_month_hani(1);">１ヶ月</a>
					</td>
					<td width='15%' style="text-align:center; padding:2px; <?=@$arr_mon[2]?>">
						<a href="javascript:change_month_hani(2);">２ヶ月</a>
					</td>
					<td width='15%' style="text-align:center; padding:2px; <?=@$arr_mon[3]?>">
						<a href="javascript:change_month_hani(3);">３ヶ月</a>
					</td>
					<td width='15%' style="text-align:center; padding:2px; <?=@$arr_mon[6]?>">
						<a href="javascript:change_month_hani(6);">６ヶ月</a>
					</td>
					<td width='20%' style="text-align:center">
						<a href="javascript:change_target_month(<?=$next_target_month?>);">次ヶ月＞</a>
					</td>
				</tr>
			</table>
			</div>
		</td>
	</tr>
</table>
<!-- ヶ月選択 END -->




<!-- 検索結果一覧 START -->
<div style="height:350px; overflow: scroll;">
<table class="list_table" cellspacing="1">
	<tr>
		<th style="text-align:center" width="23"></th>
		<th style="text-align:center" width="75"><a href="javascript:change_sort('<?=$sort_link_cre_date?>');">作成年月日</a></th>
		<th style="text-align:center" width="75"><a href="javascript:change_sort('<?=$sort_link_problem?>');">プロブレム</a></th>
		<th style="text-align:center" width="85"><a href="javascript:change_sort('<?=$sort_link_diag_div?>');"><?=$_label_by_profile["SUMMARY_RECORD_KUBUN"][$profile_type]?></a></th>
		<th style="text-align:center" width="120"><a href="javascript:change_sort('<?=$sort_link_kisaisha?>');">記載者</a></th>
		<th style="text-align:center" width="100"><a href="javascript:change_sort('<?=$sort_link_shokushu?>');">職種</a></th>
		<th style="text-align:center" width='20'><a href="javascript:change_sort('<?=$sort_link_priority?>');"><img src="img/repo-jyuuyou.gif" alt="記事重要度"></a></th>
		<? if($pt_mode == 'other') { ?>
		<th style="text-align:center" width="120"><a href="javascript:change_sort('<?=$sort_link_pt?>');">患者氏名</a></td>
		<? } ?>
		<th style="text-align:center"><a href="javascript:change_sort('<?=$sort_link_tmplate?>');">テンプレート</a></th>
		<th style="text-align:center" width="170">タグ情報</th>
	</tr>
	<?
	$line_index = -1;

	foreach((array)$data_list as $data) {
		if (!$data) break;
		$line_index++;
		$line_group = $line_index;

		$d_summary_id      = $data['summary_id'];
		$d_summary_seq     = $data['summary_seq'];
		$d_diag_div        = $data['diag_div'];
		$d_problem         = $data['problem'];
		$d_smry_cmt        = $data['smry_cmt'];
		$d_cre_date        = $data['cre_date'];
		$d_priority        = $data['priority'];
		$d_problem_id      = $data['problem_id'];
		$d_sect_id         = $data['sect_id'];
		$d_tmpl_id         = $data['tmpl_id'];
		$d_tmpl_name       = $data['tmpl_name'];
		$d_tmpl_file       = $data['tmpl_file'];
		$d_ptif_id         = $data['ptif_id'];
		$d_ptif_lt_kana_nm = $data['ptif_lt_kana_nm'];
		$d_ptif_ft_kana_nm = $data['ptif_ft_kana_nm'];
		$d_ptif_lt_kaj_nm  = $data['ptif_lt_kaj_nm'];
		$d_ptif_ft_kaj_nm  = $data['ptif_ft_kaj_nm'];
		$d_emp_id          = $data['emp_id'];
		$d_emp_lt_nm       = $data['emp_lt_nm'];
		$d_emp_ft_nm       = $data['emp_ft_nm'];
		$d_emp_kn_lt_nm    = $data['emp_kn_lt_nm'];
		$d_emp_kn_ft_nm    = $data['emp_kn_ft_nm'];
		$d_emp_job         = $data['emp_job'];
		$d_job_nm          = $data['job_nm'];

		$d_cre_date = substr($d_cre_date, 0, 4)."/".substr($d_cre_date, 4, 2)."/".substr($d_cre_date, 6, 2);
		$d_emp_nm = $d_emp_lt_nm." ".$d_emp_ft_nm;
		$d_ptif_nm = $d_ptif_lt_kaj_nm." ".$d_ptif_ft_kaj_nm;

		if(mb_strlen($d_problem, "EUC-JP")>6) {
			$d_problem = mb_substr($d_problem,0,6, "EUC-JP");
		}
		if(mb_strlen($d_diag_div, "EUC-JP")>6) {
			$d_diag_div = mb_substr($d_diag_div,0,6, "EUC-JP");
		}
		if(mb_strlen($d_job_nm, "EUC-JP")>4) {
			$d_job_nm = mb_substr($d_job_nm,0,4, "EUC-JP");
		}


		//タグ情報
		$tmpl_tag_list = null;
		if($d_tmpl_id != "")
		{
			$xml_file = get_tmpl_xml_file_name($d_emp_id, $d_ptif_id, $d_summary_id);
			$tmpl_tag_list = get_tmpl_tag_list($xml_file);
		}

		//タグID(全て)
		$tag_id_work = "";
		for($tag_id = 0;$tag_id < count($tmpl_tag_list); $tag_id++)
		{
			if($tag_id_work != "")
			{
				$tag_id_work .= ",";
			}
			$tag_id_work .= $tag_id;
		}
		$tag_id = $tag_id_work;

		//セルの属性
		$popup_value_list = get_popup_value_list($d_tmpl_id,$d_smry_cmt,$tmpl_tag_list);
		$popup_script1 = "popupDetail('data_line',new Array($popup_value_list),event);";
		$popup_script1 = "if(is_page_loaded){".$popup_script1."};";
		$popup_script2 = "closeDetail();";
		$line_elem  = " class=\"data_list_line_$line_index\"";
		$line_elem .= " onmouseover=\"changeCellsColor(this.className, '#fefc8f');this.style.cursor = 'pointer'; \"";
		$line_elem .= " onmouseout=\"changeCellsColor(this.className, '');this.style.cursor = ''; \"";
		$line_elem .= " onclick=\"entry_data_line('$d_ptif_id','$d_summary_id','$tag_id','$d_summary_seq')\"";
		$line_elem_pop  = " class=\"data_list_line_$line_index\"";
		$line_elem_pop .= " onmouseover=\"changeCellsColor(this.className, '#fefc8f');this.style.cursor = 'pointer';$popup_script1 \"";
		$line_elem_pop .= " onmouseout=\"changeCellsColor(this.className, '');this.style.cursor = '';$popup_script2 \"";
		$line_elem_pop .= " onclick=\"entry_data_line('$d_ptif_id','$d_summary_id','$tag_id','$d_summary_seq')\"";
		$line_elem_img  = " class=\"data_list_line_$line_index\"";
		$line_elem_img .= " onmouseover=\"changeCellsColor(this.className, '#fefc8f');this.style.cursor = 'pointer';\"";
		$line_elem_img .= " onmouseout=\"changeCellsColor(this.className, '');this.style.cursor = ''; \"";


		//オープンクローズ情報
		if($d_tmpl_id == "") $openclose = "";
		else {
			if(in_array($d_ptif_id.':'.$d_summary_id,(array)$opened_data_line_id)) {
				$openclose = "open";
			} else {
				$openclose = "close";
			}
		}

	?>
	<tr id="list_data_tr_<?=$line_group?>">
		<td align='center' <?=$line_elem_img?> >
			<div class="summary_pt_id_<?=$d_summary_id?>_<?=$d_ptif_id?>">
			<img id="list_data_img_<?=$line_group?>" src="img/spacer.gif" alt="" width="20" height="20" class="<?=$openclose?>" style="margin-right:3px;" onclick="changeOpenClose(this)">
			</div>
		</td>
		<td align='center' <?=$line_elem?> ><?=$d_cre_date?></td>
		<td align='center' <?=$line_elem?> ><?=h($d_problem)?></td>
		<td align='center' <?=$line_elem_pop?> ><?=h($d_diag_div)?></td>
		<td align='center' <?=$line_elem?> ><?=h($d_emp_nm)?></td>
		<td align='center' <?=$line_elem?> ><?=h($d_job_nm)?></td>
		<td align='center' <?=$line_elem?> ><?=$d_priority?></td>
		<? if($pt_mode == 'other') { ?>
		<td align='center' <?=$line_elem?> ><?=h($d_ptif_nm)?></td>
		<? } ?>
		<td align='center' <?=$line_elem?> ><?=h($d_tmpl_name)?></td>
		<td align='center' <?=$line_elem?> ></td>
	</tr>


<?
		for($tag_id = 0;$tag_id < count($tmpl_tag_list); $tag_id++)
		{
			$line_index++;

			$tmpl_tag = $tmpl_tag_list[$tag_id]['tag_name'];

			//セルの属性
			$popup_value_list = get_popup_value_list($d_tmpl_id,$d_smry_cmt,$tmpl_tag_list,array($tag_id));
			$popup_script1 = "popupDetail('data_line',new Array($popup_value_list),event);";
			$popup_script1 = "if(is_page_loaded){".$popup_script1."};";
			$popup_script2 = "closeDetail();";
			$line_elem  = " class=\"data_list_line_$line_index\"";
			$line_elem .= " onmouseover=\"changeCellsColor(this.className, '#fefc8f');this.style.cursor = 'pointer'; \"";
			$line_elem .= " onmouseout=\"changeCellsColor(this.className, '');this.style.cursor = ''; \"";
			$line_elem .= " onclick=\"entry_data_line('$d_ptif_id','$d_summary_id','$tag_id','$d_summary_seq')\"";
			$line_elem_pop  = " class=\"data_list_line_$line_index\"";
			$line_elem_pop .= " onmouseover=\"changeCellsColor(this.className, '#fefc8f');this.style.cursor = 'pointer';$popup_script1\"";
			$line_elem_pop .= " onmouseout=\"changeCellsColor(this.className, '');this.style.cursor = '';$popup_script2\"";
			$line_elem_pop .= " onclick=\"entry_data_line('$d_ptif_id','$d_summary_id','$tag_id','$d_summary_seq')\"";
			$line_elem_img  = " class=\"data_list_line_$line_index\"";
			$line_elem_img .= " onmouseover=\"changeCellsColor(this.className, '#fefc8f');this.style.cursor = 'pointer';\"";
			$line_elem_img .= " onmouseout=\"changeCellsColor(this.className, '');this.style.cursor = '';\"";

?>
	<tr class="list_data_tr_<?=$line_group?>" <?if($openclose == 'close'){?>style="display:none;"<?}?> >
		<td align='center' <?=$line_elem_img?> >
			<img src="img/spacer.gif" alt="" width="20" height="20" style="margin-right:3px;">
		</td>
		<td align='center' <?=$line_elem?> >&nbsp</td>
		<td align='center' <?=$line_elem?> >&nbsp</td>
		<td align='center' <?=$line_elem_pop?> >&nbsp</td>
		<td align='center' <?=$line_elem?> >&nbsp</td>
		<td align='center' <?=$line_elem?> >&nbsp</td>
		<td align='center' <?=$line_elem?> >&nbsp</td>
<?
if($pt_mode == 'other')
{
?>
		<td align='center' <?=$line_elem?> >&nbsp</td>
<?
}
?>
		<td align='center' <?=$line_elem?> >&nbsp</td>
		<td align='center' <?=$line_elem?> >
			<?
			//=h("<$tmpl_tag>")
			//表示用の名称に変更
			$tag_disp_name = $tmpl_tag_list[$tag_id]['tag_disp_name'];
			echo(h("<$tag_disp_name>"));
			?>

		</td>
	</tr>

	<?
		}
	}
	?>
</table>
</div>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<!-- 検索結果一覧 END -->









<!-- コピー対象一覧 START -->
<?
if($entry_list != "")
{
?>
<div style="border-top:1px solid #d2eafd; border-bottom:1px solid #8ebce1; margin-top:8px; margin-bottom:2px; padding:2px; background-color:#bee0fc">
コピー指定済み情報
</div>


<div style="height:270px; overflow: scroll;">
<table class="list_table" cellspacing="1">
	<tr>
		<th style="text-align:center" width="23"></th>
		<th style="text-align:center" width="75">作成年月日</th>
		<th style="text-align:center" width="75">プロブレム</th>
		<th style="text-align:center" width="85"><?=$_label_by_profile["SUMMARY_RECORD_KUBUN"][$profile_type]?></th>
		<th style="text-align:center" width="120">記載者</th>
		<th style="text-align:center" width="100">職種</th>
		<th style="text-align:center" width='20'><img src="img/repo-jyuuyou.gif" alt="記事重要度"></th>
		<th style="text-align:center" width="100">患者氏名</th>
		<th style="text-align:center">テンプレート</th>
		<th style="text-align:center" width="170">タグ情報</th>
	</tr>
<?
	$line_index = -1;
	foreach($entry_list as $entry_list1)
	{
		$line_index++;
		$line_group = $line_index;
		$d_pt_id = $entry_list1['pt_id'];
		$d_summary_id = $entry_list1['summary_id'];
		$d_tag_id_arr = $entry_list1['tag_id'];
		$tag_id = join(',',$d_tag_id_arr);

		//検索
		$sql  =  "select * from";

		$sql .= " (";
		$sql .= " select * from summary";
		$sql .= " where summary_id= '$d_summary_id' and ptif_id = '$d_pt_id'";
		$sql .= " ) s";

		$sql .= " natural left join ";
		$sql .= " (";
		$sql .= " select tmpl_id,tmpl_name,tmpl_file from tmplmst";
		$sql .= " ) t";

		$sql .= " natural inner join";
		$sql .= " (";
		$sql .= " select ptif_id,ptif_lt_kana_nm,ptif_ft_kana_nm,ptif_lt_kaj_nm,ptif_ft_kaj_nm from ptifmst where not ptif_del_flg";
		$sql .= " ) p";

		$sql .= " natural inner join";
		$sql .= " (";
		$sql .= " select emp_id,emp_lt_nm,emp_ft_nm,emp_kn_lt_nm,emp_kn_ft_nm,emp_job from empmst";
		$sql .= " ) e";

		$sql .= " natural left join";
		$sql .= " (";
		$sql .= " select job_id as emp_job,job_nm from jobmst";
		$sql .= " ) j";

		$sel = select_from_table($con, $sql, "", $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$data = pg_fetch_array($sel,0);
		$d_summary_id      = $data['summary_id'];
		$d_diag_div        = $data['diag_div'];
		$d_problem         = $data['problem'];
		$d_smry_cmt        = $data['smry_cmt'];
		$d_cre_date        = $data['cre_date'];
		$d_priority        = $data['priority'];
		$d_problem_id      = $data['problem_id'];
		$d_sect_id         = $data['sect_id'];
		$d_tmpl_id         = $data['tmpl_id'];
		$d_tmpl_name       = $data['tmpl_name'];
		$d_tmpl_file       = $data['tmpl_file'];
		$d_ptif_id         = $data['ptif_id'];
		$d_ptif_lt_kana_nm = $data['ptif_lt_kana_nm'];
		$d_ptif_ft_kana_nm = $data['ptif_ft_kana_nm'];
		$d_ptif_lt_kaj_nm  = $data['ptif_lt_kaj_nm'];
		$d_ptif_ft_kaj_nm  = $data['ptif_ft_kaj_nm'];
		$d_emp_id          = $data['emp_id'];
		$d_emp_lt_nm       = $data['emp_lt_nm'];
		$d_emp_ft_nm       = $data['emp_ft_nm'];
		$d_emp_kn_lt_nm    = $data['emp_kn_lt_nm'];
		$d_emp_kn_ft_nm    = $data['emp_kn_ft_nm'];
		$d_emp_job         = $data['emp_job'];
		$d_job_nm          = $data['job_nm'];

		$d_cre_date = substr($d_cre_date, 0, 4)."/".substr($d_cre_date, 4, 2)."/".substr($d_cre_date, 6, 2);
		$d_emp_nm = $d_emp_lt_nm." ".$d_emp_ft_nm;
		if($d_ptif_id == $pt_id)
		{
			$d_ptif_nm = "本人";
		}
		else
		{
			$d_ptif_nm = $d_ptif_lt_kaj_nm." ".$d_ptif_ft_kaj_nm;
		}

		if(mb_strlen($d_problem, "EUC-JP")>6) {
			$d_problem = mb_substr($d_problem,0,6, "EUC-JP");
		}
		if(mb_strlen($d_diag_div, "EUC-JP")>6) {
			$d_diag_div = mb_substr($d_diag_div,0,6, "EUC-JP");
		}
		if(mb_strlen($d_job_nm, "EUC-JP")>4) {
			$d_job_nm = mb_substr($d_job_nm,0,4, "EUC-JP");
		}

		//タグ情報
		$tmpl_tag_list = null;
		if($d_tmpl_id != "")
		{
			$xml_file = get_tmpl_xml_file_name($d_emp_id, $d_ptif_id, $d_summary_id);
			$tmpl_tag_list = get_tmpl_tag_list($xml_file);
		}


		//セルの属性
		$popup_value_list = get_popup_value_list($d_tmpl_id,$d_smry_cmt,$tmpl_tag_list,$d_tag_id_arr);
		$popup_script1 = "popupDetail('entry_line',new Array($popup_value_list),event);";
		$popup_script1 = "if(is_page_loaded){".$popup_script1."};";
		$popup_script2 = "closeDetail();";
		$line_elem  = " class=\"entry_list_line_$line_index\"";
		$line_elem .= " onmouseover=\"changeCellsColor(this.className, '#fefc8f');this.style.cursor = 'pointer'; \"";
		$line_elem .= " onmouseout=\"changeCellsColor(this.className, '');this.style.cursor = ''; \"";
		$line_elem .= " onclick=\"cancel_data_line('$d_ptif_id','$d_summary_id','$tag_id')\"";
		$line_elem_pop  = " class=\"entry_list_line_$line_index\"";
		$line_elem_pop .= " onmouseover=\"changeCellsColor(this.className, '#fefc8f');this.style.cursor = 'pointer';$popup_script1\"";
		$line_elem_pop .= " onmouseout=\"changeCellsColor(this.className, '');this.style.cursor = '';$popup_script2\"";
		$line_elem_pop .= " onclick=\"cancel_data_line('$d_ptif_id','$d_summary_id','$tag_id')\"";
		$line_elem_img  = " class=\"entry_list_line_$line_index\"";
		$line_elem_img .= " onmouseover=\"changeCellsColor(this.className, '#fefc8f');this.style.cursor = 'pointer';\"";
		$line_elem_img .= " onmouseout=\"changeCellsColor(this.className, '');this.style.cursor = '';\"";

		//オープンクローズ情報
		if($d_tmpl_id == "")
		{
			$openclose = "";
		}
		else
		{
			if(in_array($d_ptif_id.':'.$d_summary_id, (array)$opened_entry_line_id)) {
				$openclose = "open";
			} else {
				$openclose = "close";
			}
		}

?>
	<tr id="list_data_e_tr_<?=$line_group?>">
		<td align='center' <?=$line_elem_img?> >
			<div class="summary_pt_id_<?=$d_summary_id?>_<?=$d_ptif_id?>">
			<img id="list_data_e_img_<?=$line_group?>" src="img/spacer.gif" alt="" width="20" height="20" class="<?=$openclose?>" style="margin-right:3px;" onclick="changeOpenClose(this)">
			</div>
		</td>
		<td align='center' <?=$line_elem?> ><?=$d_cre_date?></td>
		<td align='center' <?=$line_elem?> ><?=h($d_problem)?></td>
		<td align='center' <?=$line_elem_pop?> ><?=h($d_diag_div)?></td>
		<td align='center' <?=$line_elem?> ><?=h($d_emp_nm)?></td>
		<td align='center' <?=$line_elem?> ><?=h($d_job_nm)?></td>
		<td align='center' <?=$line_elem?> ><?=$d_priority?></td>
		<td align='center' <?=$line_elem?> ><?=h($d_ptif_nm)?></td>
		<td align='center' <?=$line_elem?> ><?=h($d_tmpl_name)?></td>
		<td align='center' <?=$line_elem?> ></td>
	</tr>


<?
		if($d_tag_id_arr != "")
		{
			foreach($d_tag_id_arr as $tag_id)
			{
				$line_index++;

				$tmpl_tag = $tmpl_tag_list[$tag_id]['tag_name'];

				//セルの属性
				$popup_value_list = get_popup_value_list($d_tmpl_id,$d_smry_cmt,$tmpl_tag_list,array($tag_id));
				$popup_script1 = "popupDetail('entry_line',new Array($popup_value_list),event);";
				$popup_script1 = "if(is_page_loaded){".$popup_script1."};";
				$popup_script2 = "closeDetail();";
				$line_elem  = " class=\"entry_list_line_$line_index\"";
				$line_elem .= " onmouseover=\"changeCellsColor(this.className, '#fefc8f');this.style.cursor = 'pointer'; \"";
				$line_elem .= " onmouseout=\"changeCellsColor(this.className, '');this.style.cursor = ''; \"";
				$line_elem .= " onclick=\"cancel_data_line('$d_ptif_id','$d_summary_id','$tag_id')\"";
				$line_elem_pop  = " class=\"entry_list_line_$line_index\"";
				$line_elem_pop .= " onmouseover=\"changeCellsColor(this.className, '#fefc8f');this.style.cursor = 'pointer';$popup_script1\"";
				$line_elem_pop .= " onmouseout=\"changeCellsColor(this.className, '');this.style.cursor = '';$popup_script2\"";
				$line_elem_pop .= " onclick=\"cancel_data_line('$d_ptif_id','$d_summary_id','$tag_id')\"";
				$line_elem_img  = " class=\"entry_list_line_$line_index\"";
				$line_elem_img .= " onmouseover=\"changeCellsColor(this.className, '#fefc8f');this.style.cursor = 'pointer';\"";
				$line_elem_img .= " onmouseout=\"changeCellsColor(this.className, '');this.style.cursor = '';\"";
?>
	<tr class="list_data_e_tr_<?=$line_group?>" <?if($openclose == 'close'){?>style="display:none;"<?}?> >
		<td align='center' <?=$line_elem_img?> ><img src="img/spacer.gif" alt="" width="20" height="20" style="margin-right:3px;"></td>
		<td align='center' <?=$line_elem?> >&nbsp</td>
		<td align='center' <?=$line_elem?> >&nbsp</td>
		<td align='center' <?=$line_elem_pop?> >&nbsp</td>
		<td align='center' <?=$line_elem?> >&nbsp</td>
		<td align='center' <?=$line_elem?> >&nbsp</td>
		<td align='center' <?=$line_elem?> >&nbsp</td>
		<td align='center' <?=$line_elem?> >&nbsp</td>
		<td align='center' <?=$line_elem?> >&nbsp</td>
		<td align='center' <?=$line_elem?> ><?
			//=h("<$tmpl_tag>")
			//表示用の名称に変更
			$tag_disp_name = $tmpl_tag_list[$tag_id]['tag_disp_name'];
			echo(h("<$tag_disp_name>"));
			?>
		</td>
	</tr>

<?
			}
		}
	}
?>
</table>
</div>
<?
}
?>
<!-- コピー対象一覧 END -->










</form>
</body>
</html>
<?

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//内部関数
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//↓同一関数が他にも定義あり。
/**
 * YYYYMM形式の日付に指定値を加算します。
 *
 * 注意：この関数は±12ヶ月以上は対応していません。
 * @param $yyyymm 加算前のYYYYMM値
 * @param $add 加算値
 * @return 加算後のYYYYMM値
 */
function add_yyyymm($yyyymm,$add)
{
	$year = substr($yyyymm, 0, 4);
	$month = substr($yyyymm, 4, 2);
	$year1  = $year;
	$month1 = $month + $add;
	if($month1 > 12)
	{
		$year1  = $year1 +1;
		$month1 = $month1 -12;
	}
	if($month1 < 1)
	{
		$year1  = $year1 -1;
		$month1 = $month1 +12;
	}
	$year1  = "0000".$year1;
	$month1 = "00".$month1;
	$year1  = substr($year1,strlen($year1)-4,4);
	$month1 = substr($month1,strlen($month1)-2,2);
	$yyyymm1 = $year1.$month1;
	return $yyyymm1;
}

/**
 * ポップアップ表示用のデータを精製します。
 *
 * @param integer $d_tmpl_id テンプレートID
 * @param string $d_smry_cmt 診療記録内容文字列
 * @param array $tmpl_tag_list テンプレートタグ情報
 * @param array $d_tag_id_arr 対象のテンプレートタグ番号(省略時全タグ)
 * @return array ポップアップの表示情報(javascript用)
 */
function get_popup_value_list($d_tmpl_id,$d_smry_cmt,$tmpl_tag_list,$d_tag_id_arr="") {
	$popup_value_list  = "";
	if($d_tmpl_id == "") {
		$popup_val = $d_smry_cmt;
		$popup_val = h($popup_val);
		$popup_val = str_replace("\"","&#34;", $popup_val);
		$popup_val = str_replace("'","\'", $popup_val);
		$popup_value_list .= "'内容','$popup_val'";
	} else {
		$tag_id = -1;
		foreach((array)$tmpl_tag_list as $tmpl_tag_list1) {
			if (!$tmpl_tag_list1) break;
			$tag_id++;
			if($d_tag_id_arr != "" && !in_array($tag_id,$d_tag_id_arr)) continue;
			if($popup_value_list != "") $popup_value_list .= ",";
			$popup_item = $tmpl_tag_list1['tag_disp_name'];
			$popup_item = h($popup_item);
			$popup_item = str_replace("\"","&#34;", $popup_item);
			$popup_item = str_replace("'","\'", $popup_item);
			$popup_val = $tmpl_tag_list1['tag_value'];
			$popup_val = h($popup_val);
			$popup_val = str_replace("\"","&#34;", $popup_val);
			$popup_val = str_replace("'","\'", $popup_val);
			$popup_value_list .= "'$popup_item','$popup_val'";
		}
	}
	return $popup_value_list;
}


?>
