<?
function check_license($con, $fname) {

	// ライセンス情報を取得
	$sql = "select * from license";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$max_user = pg_fetch_result($sel, 0, "lcs_max_user");
	$basic_user = pg_fetch_result($sel, 0, "lcs_basic_user");
	$intra_user = pg_fetch_result($sel, 0, "lcs_intra_user");
	$nursing_user = pg_fetch_result($sel, 0, "lcs_nursing_user");
	$fantol_user = pg_fetch_result($sel, 0, "lcs_fantol_user");
	$fplus_user = pg_fetch_result($sel, 0, "lcs_fplus_user");
	$baritess_user = pg_fetch_result($sel, 0, "lcs_baritess_user");
	$report_user = pg_fetch_result($sel, 0, "lcs_report_user");
	$ladder_user = pg_fetch_result($sel, 0, "lcs_ladder_user");
	$career_user = pg_fetch_result($sel, 0, "lcs_career_user");
	$jnl_user = pg_fetch_result($sel, 0, "lcs_jnl_user");
	$ccusr1_user = pg_fetch_result($sel, 0, "lcs_ccusr1_user"); // 最大数

	// 基本機能利用可能ユーザ数がnullの場合は登録可能ユーザ数で計算
	if ($basic_user == "") {
		$basic_user = $max_user;
	}

	// イントラネット利用可能ユーザ数がnullの場合は登録可能ユーザ数で計算
	if ($intra_user == "") {
		$intra_user = $max_user;
	}

	// 看護支援利用可能ユーザ数がnullの場合は登録可能ユーザ数で計算
	if ($nursing_user == "") {
		$nursing_user = $max_user;
	}

	// ファントルくん利用可能ユーザ数がnullの場合は登録可能ユーザ数で計算
	if ($fantol_user == "") {
		$fantol_user = $max_user;
	}

	// ファントルくん＋利用可能ユーザ数がnullの場合は登録可能ユーザ数で計算
	if ($fplus_user == "") {
		$fplus_user = $max_user;
	}

	// バリテス利用可能ユーザ数がnullの場合は登録可能ユーザ数で計算
	if ($baritess_user == "") {
		$baritess_user = $max_user;
	}

	// メドレポート利用可能ユーザ数がnullの場合は登録可能ユーザ数で計算
	if ($report_user == "") {
		$report_user = $max_user;
	}

	// クリニカルラダー利用可能ユーザ数がnullの場合は登録可能ユーザ数で計算
	if ($ladder_user == "") {
		$ladder_user = $max_user;
	}

	// キャリア開発ラダー利用可能ユーザ数がnullの場合は登録可能ユーザ数で計算
	if ($career_user == "") {
		$career_user = $max_user;
	}

	// 日報・月報利用可能ユーザ数がnullの場合は登録可能ユーザ数で計算
	if ($jnl_user == "") {
		$jnl_user = $max_user;
	}

	// スタッフ・ポートフォリオ利用可能ユーザ数がnullの場合は登録可能ユーザ数で計算
	if ($ccusr1_user == "") {
		$ccusr1_user = $max_user;
	}

	// 登録可能ユーザ数が無制限でない場合
	if ($max_user != "FULL") {

		// 登録ユーザ数が制限を超えないかチェック
		$columns = array();
		$result = lc_check_user_count($con, $max_user, $columns, $fname);

		// 制限を超える場合、エラー情報を返す
		if (!$result) {
			return array("ok" => false, "message" => lc_get_error_message("登録"));
		}
	}

	// 基本機能利用可能ユーザ数が無制限でない場合
	if ($basic_user != "FULL") {

		// 基本機能利用ユーザ数が制限を超えないかチェック
		$columns = array("emp_webml_flg", "emp_bbs_flg", "emp_schd_flg", "emp_phone_flg", "emp_attdcd_flg", "emp_qa_flg", "emp_aprv_flg", "emp_adbk_flg", "emp_conf_flg", "emp_resv_flg", "emp_work_flg", "emp_pjt_flg", "emp_lib_flg", "emp_cmt_flg", "emp_memo_flg", "emp_newsuser_flg", "emp_ext_flg", "emp_link_flg");
		$result = lc_check_user_count($con, $basic_user, $columns, $fname);

		// 制限を超える場合、エラー情報を返す
		if (!$result) {
			return array("ok" => false, "message" => lc_get_error_message("基本機能利用"));
		}
	}

	// イントラネット利用可能ユーザ数が無制限でない場合
	if ($intra_user != "FULL") {

		// イントラネット利用ユーザ数が制限を超えないかチェック
		$columns = array("emp_intra_flg", "emp_ymstat_flg", "emp_intram_flg", "emp_stat_flg", "emp_allot_flg", "emp_life_flg");
		$result = lc_check_user_count($con, $intra_user, $columns, $fname);

		// 制限を超える場合、エラー情報を返す
		if (!$result) {
			return array("ok" => false, "message" => lc_get_error_message("イントラネット利用"));
		}
	}

	// 看護支援利用可能ユーザ数が無制限でない場合
	if ($nursing_user != "FULL") {

		// 看護支援利用ユーザ数が制限を超えないかチェック
		$columns = array("emp_amb_flg", "emp_ambadm_flg");
		$result = lc_check_user_count($con, $nursing_user, $columns, $fname);

		// 制限を超える場合、エラー情報を返す
		if (!$result) {
			return array("ok" => false, "message" => lc_get_error_message("看護支援利用"));
		}
	}

	// ファントルくん利用可能ユーザ数が無制限でない場合
	if ($fantol_user != "FULL") {

		// ファントルくん利用ユーザ数が制限を超えないかチェック
		$columns = array("emp_inci_flg", "emp_rm_flg");
		$result = lc_check_user_count($con, $fantol_user, $columns, $fname);

		// 制限を超える場合、エラー情報を返す
		if (!$result) {
			return array("ok" => false, "message" => lc_get_error_message("ファントルくん利用"));
		}
	}

	// ファントルくん＋利用可能ユーザ数が無制限でない場合
	if ($fplus_user != "FULL") {

		// ファントルくん＋利用ユーザ数が制限を超えないかチェック
		$columns = array("emp_fplus_flg", "emp_fplusadm_flg");
		$result = lc_check_user_count($con, $fplus_user, $columns, $fname);

		// 制限を超える場合、エラー情報を返す
		if (!$result) {
			return array("ok" => false, "message" => lc_get_error_message("ファントルくん＋利用"));
		}
	}

	// バリテス利用可能ユーザ数が無制限でない場合
	if ($baritess_user != "FULL") {

		// バリテス利用ユーザ数が制限を超えないかチェック
		$columns = array("emp_manabu_flg", "emp_manabuadm_flg");
		$result = lc_check_user_count($con, $baritess_user, $columns, $fname);

		// 制限を超える場合、エラー情報を返す
		if (!$result) {
			return array("ok" => false, "message" => lc_get_error_message("バリテス利用"));
		}
	}

	// メドレポート利用可能ユーザ数が無制限でない場合
	if ($report_user != "FULL") {

		// メドレポート利用ユーザ数が制限を超えないかチェック
		$columns = array("emp_med_flg", "emp_medadm_flg");
		$result = lc_check_user_count($con, $report_user, $columns, $fname);

		// 制限を超える場合、エラー情報を返す
		if (!$result) {
			require_once("get_menu_label.ini");
			$report_menu_label = get_report_menu_label($con, $fname);
			return array("ok" => false, "message" => lc_get_error_message("{$report_menu_label}利用"));
		}
	}

	// クリニカルラダー利用可能ユーザ数が無制限でない場合
	if ($ladder_user != "FULL") {

		// クリニカルラダー利用ユーザ数が制限を超えないかチェック
		$columns = array("emp_ladder_flg", "emp_ladderadm_flg");
		$result = lc_check_user_count($con, $ladder_user, $columns, $fname);

		// 制限を超える場合、エラー情報を返す
		if (!$result) {
			return array("ok" => false, "message" => lc_get_error_message("クリニカルラダー利用"));
		}
	}

	// キャリア開発ラダー利用可能ユーザ数が無制限でない場合
	if ($career_user != "FULL") {

		// キャリア開発ラダー利用ユーザ数が制限を超えないかチェック
		$columns = array("emp_career_flg", "emp_careeradm_flg");
		$result = lc_check_user_count($con, $career_user, $columns, $fname);

		// 制限を超える場合、エラー情報を返す
		if (!$result) {
			return array("ok" => false, "message" => lc_get_error_message("キャリア開発ラダー利用"));
		}
	}

	// 日報・月報利用可能ユーザ数が無制限でない場合
	if ($jnl_user != "FULL") {

		// 日報・月報利用ユーザ数が制限を超えないかチェック
		$columns = array("emp_jnl_flg", "emp_jnladm_flg");
		$result = lc_check_user_count($con, $jnl_user, $columns, $fname);

		// 制限を超える場合、エラー情報を返す
		if (!$result) {
			return array("ok" => false, "message" => lc_get_error_message("日報・月報利用"));
		}
	}

	// スタッフ・ポートフォリオ利用可能ユーザ数が無制限でない場合
	if ($ccusr1_user != "FULL") {

		// 日報・月報利用ユーザ数が制限を超えないかチェック
		$columns = array("emp_ccusr1_flg", "emp_ccadm1_flg");
		$result = lc_check_user_count($con, $ccusr1_user, $columns, $fname);

		// 制限を超える場合、エラー情報を返す
		if (!$result) {
			return array("ok" => false, "message" => lc_get_error_message("スタッフ・ポートフォリオ利用"));
		}
	}
	// 制限を超えないため、成功情報を返す
	return array("ok" => true);
}

// ユーザ数が制限を超えないかチェック
function lc_check_user_count($con, $user_count_code, $columns, $fname) {

	// countだと遅かったので
	$sql = "select authmst.emp_id from authmst";
	$cond = "where authmst.emp_del_flg = false";
	if (count($columns)) {
		$cond .= " and (" . implode(" = true or ", $columns) . " = true)";

	}
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$user_count = pg_num_rows($sel);
	return ($user_count <= intval($user_count_code));
}

// エラーメッセージを取得
function lc_get_error_message($label) {
	return "{$label}ユーザ数が上限を超えてしまうため、実行できません。";
}
