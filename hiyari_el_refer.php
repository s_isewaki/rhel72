<?
ob_start();

require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("hiyari_el_common.ini");

$session = $s;
$lib_id = $i;
$lib_url = $u;

//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限チェック
if (check_authority($session, 47, $fname) == "0")
{
	showLoginPage();
	exit;
}


//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==========================================================================================
// DB処理
//==========================================================================================

//==============================
// トランザクションを開始
//==============================
pg_query($con, "begin");


//==============================
// 職員IDを取得
//==============================
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0)
{
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");



//==============================
// ログを保存
//==============================
$sql = "insert into el_reflog (lib_id, ref_date, emp_id) values (";
$content = array($lib_id, date("YmdHi"), $emp_id);
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0)
{
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}


//==============================
// 参照数を取得
//==============================
$sql = "select ref_count, lib_nm, lib_extension from el_info";
$cond = "where lib_id = '$lib_id' for update";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0)
{
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$ref_count = intval(pg_fetch_result($sel, 0, "ref_count"));
$lib_nm = pg_fetch_result($sel, 0, "lib_nm");
$lib_extension = pg_fetch_result($sel, 0, "lib_extension");


//==============================
// 参照数を更新
//==============================
$sql = "update el_info set";
$set = array("ref_count");
$setvalue = array($ref_count + 1);
$cond = "where lib_id = '$lib_id'";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0)
{
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}


//==============================
// トランザクションをコミット
//==============================
pg_query($con, "commit");

//==============================
// データベース接続を閉じる
//==============================
pg_close($con);




//==========================================================================================
// ファイルダウンロード処理
//==========================================================================================

//ここまでの無駄なHTML出力をクリア
ob_end_clean();

//物理ファイル相対パス
$file_path = $lib_url;

// 運用ディレクトリ名
$dir = str_replace($_SERVER["DOCUMENT_ROOT"], "", $_SERVER["SCRIPT_FILENAME"]);
$dir = str_replace("hiyari_el_refer.php", "", $dir);


//登録コンテンツ名でダウンロードする場合
$filename_flg = lib_get_filename_flg();

if ($filename_flg > 0)
{
	//物理ファイルが存在する場合
	if (is_file($file_path))
	{
		ob_clean();
		//MSIEの場合
		if ($filename_flg == 1)
		{
			//ファイル名をSJISに変換
			$lib_nm = mb_convert_encoding($lib_nm, "SJIS", "EUC-JP");
		}
		//MSIE以外の場合
		else
		{
			//ファイル名をUTF-8に変換
			$lib_nm = mb_convert_encoding($lib_nm, "UTF-8", "EUC-JP");
		}
		
		//登録コンテンツ名
		$file_name = "$lib_nm.$lib_extension";
		
		//ダウンロード用ヘッダー出力
		header("Content-Disposition: attachment; filename=$file_name");
		header("Content-Type: application/octet-stream; name=$file_name");
		header("Content-Length: " . filesize($file_path));
		readfile($file_path);
		ob_end_flush();
		exit;
	}
	//物理ファイルが見つからない場合
	else
	{
		//ログアウト
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
		exit;
	}
}
//システム名でダウンロードする場合
else
{
	//ダウンロードURL
	$lib_url = "http://" . $_SERVER['HTTP_HOST'] . $dir . $lib_url;
	
	//ダウンロード用ヘッダー出力
	header("Location: $lib_url");
}
?>
