<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>病床管理 | 一括登録 | 入院登録</title>
<?
require("about_authority.php");
require("about_session.php");

$fname = $PHP_SELF;

// セッションをチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限をチェック
$checkauth = check_authority($session, 21, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 診療科登録権限を取得
$section_admin_auth = check_authority($session, 28, $fname);

$default_url = ($section_admin_auth == "1") ? "entity_menu.php" : "building_list.php";

// 文字コードのデフォルトはShift_JIS
if ($encoding == "") {$encoding = "1";}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="bed_menu.php?session=<? echo($session); ?>"><img src="img/icon/b17.gif" width="32" height="32" border="0" alt="病床管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="bed_menu.php?session=<? echo($session); ?>"><b>病床管理</b></a> &gt; <a href="<? echo($default_url); ?>?session=<? echo($session); ?>"><b>管理画面</b></a> &gt; <a href="bed_bulk_inpatient_register.php?session=<? echo($session); ?>"><b>一括登録</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="bed_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- left -->
<td width="120" valign="top">
<table width="120" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width="118" valign="top">
<table width="118" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#5279A5">
<td height="22" class="spacing" colspan="2"><b><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff">管理項目</font></b></td>
</tr>
<? if ($section_admin_auth == "1") { ?>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="entity_menu.php?session=<? echo($session); ?>">診療科</a></font></td>
</tr>
<? } ?>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="building_list.php?session=<? echo($session); ?>">病棟</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_display_setting.php?session=<? echo($session); ?>">表示設定</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_check_setting.php?session=<? echo($session); ?>">管理項目設定</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_bulk_inpatient_register.php?session=<? echo($session); ?>">一括登録</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_master_menu.php?session=<? echo($session); ?>">マスタ管理</a></font></td>
</tr>
</table>
</td>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
</td>
<!-- left -->
<!-- center -->
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->
<!-- right -->
<td valign="top">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="100" align="center" bgcolor="#5279a5"><a href="bed_bulk_inpatient_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>入院登録</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="bed_bulk_inpatient_reserve_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院予定登録</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="3"><br>
<form action="bed_bulk_inpatient_insert.php" method="post" enctype="multipart/form-data">
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="140" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">CSVファイル</font></td>
<td><input name="csvfile" type="file" value="" size="50"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ファイルの文字コード</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="encoding" value="1"<? if ($encoding == "1") {echo(" checked");} ?>>Shift_JIS（Windows/Mac標準）
<input type="radio" name="encoding" value="2"<? if ($encoding == "2") {echo(" checked");} ?>>EUC-JP
<input type="radio" name="encoding" value="3"<? if ($encoding == "3") {echo(" checked");} ?>>UTF-8
</font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
<? if ($result != "") { ?>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<? } ?>
<? if ($result == "f") { ?>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="red">ファイルの内容に問題があるため、登録処理が実行できませんでした。</font></td>
</tr>
<? } else if ($result == "t") {?>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録処理が完了しました。</font></td>
</tr>
<? } ?>
<? if ($result != "") { ?>
</table>
<? } ?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<dl style="margin-top:0;">
<dt><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>注意</b></font></dt>
<dd>
<ul style="margin:0;">
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">データが重複する場合、無視します。</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">「崎」「高」などの異体字は「・」で置換されます。一般的な字形をお使いください。</font></li>
</ul>
</dd>
</dl>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>CSVのフォーマット</b></font></td>
</tr>
<tr>
<td width="30"></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<p style="margin:0;">下記項目をカンマで区切り、レコードごとに改行します。すべて必須です。</p>
<ul style="margin:0;">
<li style="margin-left:40px;">患者ID……使える文字は半角英数字のみです。</li>
<li style="margin-left:40px;">患者氏名（漢字）……苗字と名前はスペース区切り（スペースは全角半角個数問わず）</li>
<li style="margin-left:40px;">患者氏名（カナ）……同上</li>
<li style="margin-left:40px;">生年月日……YYYYMMDD形式</li>
<li style="margin-left:40px;">性別……半角数字1桁（0：不明、1：男性、2：女性）</li>
<li style="margin-left:40px;">病棟名</li>
<li style="margin-left:40px;">病室名</li>
<li style="margin-left:40px;">ベッド番号……半角数字1桁（1〜8）</li>
<li style="margin-left:40px;">入院日時……YYYYMMDDHHII形式（時刻は30分刻みで調整されます）</li>
<li style="margin-left:40px;">診療科</li>
<li style="margin-left:40px;">担当医……苗字と名前はスペース区切り（スペースは全角半角個数問わず）</li>
</ul>
<dl style="margin:0;">
<dt>例：</dt>
<dd style="margin-left:20px;font-family:monospace;">0000000001,山田　太郎,ヤマダ　タロウ,19550505,1,A病棟,301号室,2,200903231130,第一内科,田中　一郎</dd>
<dd style="margin-left:20px;font-family:monospace;">0000000002,鈴木　　花子,スズキ　　ハナコ,19770707,2,B病棟,402号室,8,200903231635,脳外科,渡辺　　三郎</dd>
</dl>
</font></td>
</tr>
</table>
</td>
<!-- right -->
</tr>
</table>
</body>
</html>
