<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$check_auth = check_authority($session, 12, $fname);
if ($check_auth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if (!is_array($fcl_ids)) {
	echo("<script type=\"text/javascript\">alert('削除対象が選択されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin transaction");

// チェックされた施設区分をループ
foreach ($fcl_ids as $fcl_id) {

/*
	// 施設に紐付く行事数を取得
	$sql = "select count(*) from timegd";
	$cond = "where fcl_id = $fcl_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$event_count = intval(pg_fetch_result($sel, 0, 0));

	// 行事が存在した場合
	if ($event_count > 0) {

		// 施設名を取得
		$sql = "select name from timegdfcl";
		$cond = "where fcl_id = $fcl_id";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$fcl_name = pg_fetch_result($sel, 0, "name");

		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('「{$fcl_name}」は行事が登録されているため削除できません。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}
*/

	// 施設情報を削除
	$sql = "delete from timegdfcl";
	$cond = "where fcl_id = $fcl_id";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 施設一覧画面を再表示
echo("<script type=\"text/javascript\">location.href = 'time_guide_fcl_list.php?session=$session';</script>");
?>
