<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">

<?
require_once("about_comedix.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// opt以下のphpを表示するかどうか
$opt_display_flag = file_exists("opt/flag");

// 入力チェック
// 手当
if ($allow_contents == "") {
	echo("<script type=\"text/javascript\">alert('手当が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
// 入力種別
if ($input_type == "" && $opt_display_flag) {
	echo("<script type=\"text/javascript\">alert('入力種別を選択してください。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
// 手当てコード
if ($allow_cd == "" && $opt_display_flag) {
	echo("<script type=\"text/javascript\">alert('手当コードが入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if (strlen($allow_contents) > 40) {
	echo("<script type=\"text/javascript\">alert('手当が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

if ($allow_price == "") {
	echo("<script type=\"text/javascript\">alert('金額が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

if (preg_match("/^[1-9]\d*/", $allow_price) == 0) {
	echo("<script type=\"text/javascript\">alert('金額は半角数字で入力してください。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}


// データベースに接続
$con = connect2db($fname);

// 手当を更新
$sql = "update tmcdallow set";
$set = array("allow_contents", "allow_price", "input_type", "allow_cd");
$setvalue = array($allow_contents, $allow_price, $input_type, $allow_cd);
$cond = "where allow_id = $allow_id";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// データベース接続を閉じる
pg_close($con);

// 一覧画面に遷移
echo("<script type=\"text/javascript\">location.href = 'timecard_allowance.php?session=$session';</script>");
?>
