<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 職員登録 | 職員属性一括登録更新</title>
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_class_name.ini");
require_once("label_by_profile_type.ini");
require_once("employee_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 19, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 文字コードのデフォルトはShift_JIS
if ($encoding == "") {$encoding = "1";}

// データベースに接続
$con = connect2db($fname);

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 組織情報を取得
$arr_class_name = get_class_name_array($con, $fname);
$class_cnt = $arr_class_name[4];

?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function checkForm(form) {
	if (form.csvfile.value == '') {
		alert('ファイルを選択してください。');
		return false;
	}
	document.getElementById('loading').style.display='inline';
	document.getElementById('submitbutton').disabled='disabled';
	return true;
}
function downloadCSV() {
	document.csv.submit();
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
#graph {
    clear:both;
    width: 1030px;
    height: 550px;
    border: 1px solid #e5eed5;
    margin: 5px 0;
    background: url("img/ajax-loader2.gif") #fff no-repeat center center;
}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="employee_info_menu.php?session=<? echo($session); ?>"><img src="img/icon/b26.gif" width="32" height="32" border="0" alt="職員登録"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="employee_info_menu.php?session=<? echo($session); ?>"><b>職員登録</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_info_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">検索</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">新規登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#5279a5"><a href="employee_bulk_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>一括登録</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="140" align="center" bgcolor="#bdd1e7"><a href="employee_db_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員登録用DB操作</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_auth_group.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限グループ</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_display_group.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示グループ</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_menu_group.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メニューグループ</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_auth_bulk_setting.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括設定</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_auth_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限管理</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_field_option.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">項目管理</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<? show_menu2(); ?>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form action="employee_attribute_bulk_insert4.php" method="post" enctype="multipart/form-data" onsubmit="return checkForm(this);">
<table width="680" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="140" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">CSVファイル</font></td>
<td><input name="csvfile" type="file" value="" size="50"></td>
<td style="border:0" align="right"><input type="button" value="一括更新用CSV出力" onclick="downloadCSV();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ファイルの文字コード</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="encoding" value="1"<? if ($encoding == "1") {echo(" checked");} ?>>Shift_JIS（Windows/Mac標準）
<input type="radio" name="encoding" value="2"<? if ($encoding == "2") {echo(" checked");} ?>>EUC-JP
<input type="radio" name="encoding" value="3"<? if ($encoding == "3") {echo(" checked");} ?>>UTF-8
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">新規登録</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="checkbox" name="insert_flg" value="1" >職員IDが一致しなかったとき、新規として職員を登録する</font></td>
</tr>


</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" id="submitbutton" value="登録">
<img id="loading" style="display:none" src="img/ajax-loader2.gif" height="20" alt="実行中"></td>
<td></td>
<td></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
<? if ($result != "") { ?>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<? } ?>
<? if ($result == "f") { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="red">登録処理が実行できませんでした。</font></td>
<? } else if ($result == "t") {?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">処理が完了しました。</font></td>
<? } ?>
<? if ($result != "") { ?>
</tr>
</table>
<? } ?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<dl style="margin-top:0;">
<dt><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>注意</b></font></dt>
<dd>
<ul style="margin:0;">
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">「崎」「高」などの異体字は「・」で置換されます。一般的な字形をお使いください。</br>職員1人あたり数秒かかります</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">給与支給区分、出勤パターンのグループ、雇用・勤務形態（常勤、短時間正職員、非常勤）を登録する場合、</br>３つとも値を入力してください。</font></li>
</ul>
</dd>
</dl>
<dl>
<dt><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>CSVのフォーマット</b></font></dt>
<dd><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">下記項目をカンマで区切り、レコードごとに改行します。※は必須項目となります。</font></dd>
<dd>
<ul>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限グループ</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示グループ</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メニューグループ</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">※職員ID</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">※ログインID……使用可能な文字：英数大文字小文字・アンダーバー・ハイフン（いずれも半角）</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">※パスワード……同上</font></li>

<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">※苗字（漢字）</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">※名前（漢字）</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">※苗字（かな）</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">※名前（かな）</font></li>

<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">※メールID……使用可能な文字：英数小文字・アンダーバー・ハイフン（いずれも半角）</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">※性別……数字1桁（1：男性、2：女性）</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">生年月日……YYYYMMDD形式</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入職日……同上</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退職日……同上</font></li>

<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo("※".$arr_class_name[0]); ?></font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo("※".$arr_class_name[1]); ?></font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo("※".$arr_class_name[2]); ?></font></li>
<? if ($class_cnt == "4") { ?>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_class_name[3]); ?></font></li>
<? } ?>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">※役職</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">※職種</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">※利用停止フラグ……英字1桁（f：利用中、t：利用停止中）</font></li>

<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">郵便番号……[数字3桁] - [数字4桁]</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">都道府県</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">住所1</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">住所2</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">電話番号</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">E-Mail</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">携帯端末・外線</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">携帯Mail</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">内線番号</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">院内PHS</font></li>

<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">給与支給区分</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤パターンのグループ</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">雇用・勤務形態（常勤、短時間正職員、非常勤）</font></li>
<? if (version_compare(PHP_VERSION, '5.0.0') >= 0 && substr(pg_parameter_status($con, 'server_version'),0,1) >= 8) { ?>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">氏名連携不可フラグ</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">部署連携不可フラグ</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職種連携不可フラグ</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">役職連携不可フラグ</font></li>
<? } ?>
</tr>
</table>

<form name="csv" action="employee_all_csv.php" method="post" target="download">
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
<iframe name="download" width="0" height="0" frameborder="0"></iframe>

</body>
<? pg_close($con); ?>
</html>
