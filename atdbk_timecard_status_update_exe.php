<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_session.php");
require("about_authority.php");
require("get_values.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 5, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 職員ID未指定の場合はログインユーザの職員IDを取得
if ($emp_id == "") {
	$emp_id = get_emp_id($con, $session, $fname);
}

// 勤務実績が登録されていたらUPDATE、されていなければINSERT
$sql = "select count(*) from atdbkrslt";
$cond = "where emp_id = '$emp_id' and date = '$date'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if (pg_fetch_result($sel, 0, 0) > 0) {
	$sql = "update atdbkrslt set";
	$set = array("status");
	$setvalue = array($status);
	$cond = "where emp_id = '$emp_id' and date = '$date'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
} else {
	$sql = "insert into atdbkrslt (emp_id, date, status) values (";
	$content = array($emp_id, $date, $status);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// データベース接続を閉じる
pg_close($con);

// 親画面をリフレッシュして自画面を閉じる
if ($pnt_url == "") {
    switch ($wherefrom) {
        case "1":
            $prg = "atdbk_timecard.php";
            break;
        case "2":
            $prg = "atdbk_timecard_a4.php";
            break;
        default:
            $prg = "atdbk_timecard_shift.php";
            break;
    }
        
	$pnt_url = "$prg?session=$session&yyyymm=$yyyymm&view=$view";
}
echo("<script type=\"text/javascript\">opener.location.href = '$pnt_url'; self.close();</script>");
?>
