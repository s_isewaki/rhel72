<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 患者管理｜待機患者一覧</title>
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_select_values.ini");
require_once("label_by_profile_type.ini");
require_once("yui_calendar_util.ini");

define("ROWS_PER_PAGE", 20);

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 15, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 診療科登録権限を取得
$section_admin_auth = check_authority($session, 28, $fname);

// 患者管理管理者権限を取得
$patient_admin_auth = check_authority($session, 77, $fname);

// 患者登録権限を取得
$ptreg = check_authority($session, 22, $fname);

// データベースに接続
$con = connect2db($fname);

$profile_type = get_profile_type($con, $fname);

if ($mode == "search") {

	// 待機患者数を取得
	$sql = "select count(*) from ptwait w inner join ptifmst p on w.ptif_id = p.ptif_id";
	$cond = ptwait_cond($date_y1, $date_m1, $date_d1, $date_y2, $date_m2, $date_d2, $progress_filter, $cond_id, $cond_name);
	$sel_count = select_from_table($con, $sql, $cond, $fname);
	if ($sel_count == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$ptwait_count = pg_fetch_result($sel_count, 0, 0);

	if ($ptwait_count > 0) {

		// 指定ページに表示対象が存在しなければ最大ページを表示する
		if ($page == "") $page = 1;
		$max_page = ceil($ptwait_count / ROWS_PER_PAGE);
		if ($page > $max_page) $page = $max_page;

		// チェック情報を取得
		$sql = "select * from bedcheckwait";
		$cond = "";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		for ($i = 1; $i <= 11; $i++) {
			$varname = "display$i";
			$$varname = pg_fetch_result($sel, 0, "display$i");
		}

		// 待機患者一覧を取得
		$sql = "select w.ptwait_id, w.ptif_id, (p.ptif_lt_kaj_nm || ' ' || p.ptif_ft_kaj_nm) as ptif_nm, p.ptif_birth, ptif_sex, (c.emp_lt_nm || ' ' || c.emp_ft_nm) as charge_emp_nm, w.progress, m.mst_name, s.item_name as intro_sect_name, w.intro_sect_text, case w.intro_doctor_no when 1 then s.doctor_name1 when 2 then s.doctor_name2 when 3 then s.doctor_name3 when 4 then s.doctor_name4 when 5 then s.doctor_name5 when 6 then s.doctor_name6 when 7 then s.doctor_name7 when 8 then s.doctor_name8 when 9 then s.doctor_name9 when 10 then s.doctor_name10 else null end as intro_doctor_name, w.intro_doctor_text, w.msw, w.disease, w.patho_from, w.patho_to, w.note, w.dementia, substr(w.reception_time, 1, 8) as reception_date, substr(w.verified_time, 1, 8) as verified_date, w.status1, w.status2, r.item_name as ccl_rsn_name, w.ccl_rsn as ccl_rsn_detail, i.inpt_in_dt from ptwait w";
		$sql .= " inner join ptifmst p on w.ptif_id = p.ptif_id";
		$sql .= " left join empmst c on w.charge_emp_id = c.emp_id";
		$sql .= " left join institemmst m on w.intro_inst_cd = m.mst_cd";
		$sql .= " left join institemrireki s on w.intro_inst_cd = s.mst_cd and w.intro_sect_rireki = s.rireki_index and w.intro_sect_cd = s.item_cd";
		$sql .= " left join inptmst i on w.ptif_id = i.ptif_id and w.reception_time <= cast(i.inpt_in_dt || i.inpt_in_tm as varchar)";
		$sql .= " left join (select rireki_index, item_cd, item_name from tmplitemrireki where mst_cd = 'A309') r on w.ccl_rsn_rireki = r.rireki_index and w.ccl_rsn_cd = r.item_cd";
		$cond = ptwait_cond($date_y1, $date_m1, $date_d1, $date_y2, $date_m2, $date_d2, $progress_filter, $cond_id, $cond_name);
		$offset = ($page - 1) * ROWS_PER_PAGE;
		$cond .= " order by w.reception_time desc offset $offset limit " . ROWS_PER_PAGE;
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function registerWaitingPatient() {
	window.open('patient_waiting_register.php?session=<? echo($session); ?>', 'wait', 'width=640,height=620,scrollbars=yes');
}

function updateWaitingPatient(id) {
	window.open('patient_waiting_detail.php?session=<? echo($session); ?>&id=' + id, 'wait', 'width=640,height=620,scrollbars=yes');
}

function highlightCells(class_name) {
	changeCellsColor(class_name, '#ffff66');
}

function dehighlightCells(class_name) {
	changeCellsColor(class_name, '');
}

function changeCellsColor(class_name, color) {
	var cells = document.getElementsByTagName('td');
	for (var i = 0, j = cells.length; i < j; i++) {
		if (cells[i].className == class_name) cells[i].style.backgroundColor = color;
	}
}

function call_back_calendar_select(id, date) {
	if (date.month < 10) date.month = '0' + date.month;
	if (date.day < 10) date.day = '0' + date.day;
	document.getElementById('date_y' + id).value = date.year;
	document.getElementById('date_m' + id).value = date.month;
	document.getElementById('date_d' + id).value = date.day;
}
</script>
<?
write_yui_calendar_use_file_read_0_12_2();
write_yui_calendar_script2(2);
?>
<script type="text/javascript">
function updateCal1() {
	var selMonth = document.getElementById("date_m1");
	var selDay = document.getElementById("date_d1");
	var selYear = document.getElementById("date_y1");

	var month = parseInt(selMonth.value, 10);
	var day = parseInt(selDay.value, 10);
	var year = parseInt(selYear.value, 10);

	if (!isNaN(month) && !isNaN(day) && !isNaN(year)) {
		var date = year + "/" + month + "/" + day;
		YAHOO.ui.calendar.cal1.select(date);
		YAHOO.ui.calendar.cal1.cfg.setProperty("pagedate", year + "/" + month);
		YAHOO.ui.calendar.cal1.render();
	}
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initcal();<? if ($date_y1 > 0 && $date_m1 > 0 && $date_d1 > 0) { ?>updateCal1();<? } ?>updateCal2();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="patient_info_menu.php?session=<? echo($session); ?>"><img src="img/icon/b16.gif" width="32" height="32" border="0" alt="<? echo($_label_by_profile["PATIENT_MANAGE"][$profile_type]); ?>"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="patient_info_menu.php?session=<? echo($session); ?>"><b><? echo($_label_by_profile["PATIENT_MANAGE"][$profile_type]); ?></b></a></font></td>
<? if ($section_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="entity_menu.php?session=<? echo($session); ?>&entity=2"><b>管理画面へ</b></a></font></td>
<? } else if ($patient_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="bed_check_setting.php?session=<? echo($session); ?>&entity=2"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="70" align="center" bgcolor="#bdd1e7"><a href="patient_info_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">検索</font></a></td>
<? if ($ptreg == 1) { ?>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="patient_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">新規登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="patient_bulk_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括登録</font></a></td>
<? } ?>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#5279a5"><a href="patient_waiting_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>待機患者一覧</b></font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>

<?
if ($date_y1 == "") $date_y1 = "0000";
if ($date_m1 == "") $date_m1 = "00";
if ($date_d1 == "") $date_d1 = "00";
if ($date_y2 == "") $date_y2 = date("Y");
if ($date_m2 == "") $date_m2 = date("m");
if ($date_d2 == "") $date_d2 = date("d");
?>
<form action="patient_waiting_list.php" method="get">
<table border="0" cellspacing="0" cellpadding="2">
<tr>
<td>

<table border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者ID</td>
<td colspan="6"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input name="cond_id" type="text" value="<? echo($cond_id); ?>" maxlength="12" style="ime-mode:inactive;" style="margin-right:10px;">
患者氏名 <input name="cond_name" type="text" value="<? echo($cond_name); ?>" maxlength="20" style="ime-mode:active;">
</font></td>
</tr>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">受付日</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select id="date_y1" name="date_y1">
<option value="0000">
<? show_select_years_span(1960, date("Y") + 1, $date_y1, false); ?>
</select>/<select id="date_m1" name="date_m1">
<option value="00">
<? show_select_months($date_m1); ?>
</select>/<select id="date_d1" name="date_d1">
<option value="00">
<? show_select_days($date_d1); ?>
</select>
</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<img src="img/calendar_link.gif" style="z-index:1;cursor:pointer;" onclick="show_cal1();"/><br>
<div id="cal1Container" style="position:absolute;display:none;z-index:10000;"></div>
</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<img src="img/spacer.gif" alt="" width="1" height="1">
〜
<img src="img/spacer.gif" alt="" width="1" height="1">
</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select id="date_y2" name="date_y2">
<? show_select_years_span(1960, date("Y") + 1, $date_y2, false); ?>
</select>/<select id="date_m2" name="date_m2">
<? show_select_months($date_m2); ?>
</select>/<select id="date_d2" name="date_d2">
<? show_select_days($date_d2); ?>
</select>
</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<img src="img/calendar_link.gif" style="z-index:1;cursor:pointer;" onclick="show_cal2();"/><br>
<div id="cal2Container" style="position:absolute;display:none;z-index:10000;"></div>
</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<img src="img/spacer.gif" alt="" width="2" height="1">
<input type="submit" value="検索">
<img src="img/spacer.gif" alt="" width="1" height="1">
<input type="radio" name="progress_filter" value="1"<? if ($progress_filter == "" || $progress_filter == "1") {echo(" checked");} ?>>全て
<input type="radio" name="progress_filter" value="2"<? if ($progress_filter == "2") {echo(" checked");} ?>>待機中
<input type="radio" name="progress_filter" value="4"<? if ($progress_filter == "4") {echo(" checked");} ?>>その他
<input type="radio" name="progress_filter" value="3"<? if ($progress_filter == "3") {echo(" checked");} ?>>キャンセル
</font></td>
</tr>
</table>
</td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="mode" value="search">
</form>

<table border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td>
<input type="button" value="待機登録" onclick="registerWaitingPatient();"<? if ($ptreg != 1) {echo(" disabled");} ?>>
<input type="button" value="Excel出力" onclick="document.excelform.submit();"<? if ($mode != "search" || $ptwait_count == 0) {echo(" disabled");} ?>>
</td>
</tr>
</table>

<? if ($mode == "search" && $ptwait_count == 0) { ?>
<p><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">該当する待機患者情報はありませんでした。</font></p>
<? } else if ($ptwait_count > 0) { ?>
<img src="img/spacer.gif" alt="" width="1" height="4"><br>
<? show_navigation($con, $ptwait_count, $page, $date_y1, $date_m1, $date_d1, $date_y2, $date_m2, $date_d2, $progress_filter, $cond_id, $cond_name, $session, $fname); ?>
<table border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者ID</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者氏名</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">年齢</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">性別</font></td>
<? if ($display1 == "t") { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">担当</font></td>
<? } ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">進捗</font></td>
<? if ($display2 == "t") { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">紹介元医療機関</font></td>
<? } ?>
<? if ($display3 == "t") { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">診療科</font></td>
<? } ?>
<? if ($display4 == "t") { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">前医</font></td>
<? } ?>
<? if ($display11 == "t") { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">MSW</font></td>
<? } ?>
<? if ($display6 == "t") { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">傷病名</font></td>
<? } ?>
<? if ($display7 == "t") { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発症日</font></td>
<? } ?>
<? if ($display8 == "t") { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">備考</font></td>
<? } ?>
<? if ($display5 == "t") { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">認知症</font></td>
<? } ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">受付日</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">最終確認日</font></td>
<? if ($display9 == "t") { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">調整状況1</font></td>
<? } ?>
<? if ($display10 == "t") { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">調整状況2</font></td>
<? } ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">キャンセル</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院日</font></td>
</tr>
<?
while ($row = pg_fetch_array($sel)) {
	$tmp_ptwait_id = $row["ptwait_id"];
	$tmp_ptif_id = $row["ptif_id"];
	$tmp_ptif_nm = $row["ptif_nm"];
	$tmp_age = calc_age($row["ptif_birth"]);
	$tmp_sex = format_sex($row["ptif_sex"]);
	$tmp_charge_emp_nm = $row["charge_emp_nm"];
	$tmp_progress = format_progress($row["progress"]);
	$tmp_intro_inst_name = $row["mst_name"];
	$tmp_intro_sect_name = format_name_detail($row["intro_sect_name"], $row["intro_sect_text"]);
	$tmp_intro_doctor_name = format_name_detail($row["intro_doctor_name"], $row["intro_doctor_text"]);
	$tmp_msw = $row["msw"];
	$tmp_disease = $row["disease"];
	$tmp_patho = format_patho($row["patho_from"], $row["patho_to"]);
	$tmp_note = $row["note"];
	$tmp_dementia = format_dementia($row["dementia"]);
	$tmp_reception_date = format_date($row["reception_date"]);
	$tmp_verified_date = format_date($row["verified_date"]);
	$tmp_status1 = $row["status1"];
	$tmp_status2 = $row["status2"];
	$tmp_ccl_rsn = format_name_detail($row["ccl_rsn_name"], $row["ccl_rsn_detail"]);
	$tmp_inpt_in_dt = format_date($row["inpt_in_dt"]);
?>
<tr height="22">
<td class="wait<? echo($tmp_ptwait_id); ?>" onmouseover="highlightCells(this.className); this.style.cursor = 'pointer';" onmouseout="dehighlightCells(this.className); this.style.cursor = '';" onclick="updateWaitingPatient('<? echo($tmp_ptwait_id); ?>');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_ptif_id); ?></font></td>
<td class="wait<? echo($tmp_ptwait_id); ?>" onmouseover="highlightCells(this.className); this.style.cursor = 'pointer';" onmouseout="dehighlightCells(this.className); this.style.cursor = '';" onclick="updateWaitingPatient('<? echo($tmp_ptwait_id); ?>');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><nobr><? echo($tmp_ptif_nm); ?></nobr></font></td>
<td class="wait<? echo($tmp_ptwait_id); ?>" onmouseover="highlightCells(this.className); this.style.cursor = 'pointer';" onmouseout="dehighlightCells(this.className); this.style.cursor = '';" onclick="updateWaitingPatient('<? echo($tmp_ptwait_id); ?>');" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_age); ?></font></td>
<td class="wait<? echo($tmp_ptwait_id); ?>" onmouseover="highlightCells(this.className); this.style.cursor = 'pointer';" onmouseout="dehighlightCells(this.className); this.style.cursor = '';" onclick="updateWaitingPatient('<? echo($tmp_ptwait_id); ?>');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_sex); ?></font></td>
<? if ($display1 == "t") { ?>
<td class="wait<? echo($tmp_ptwait_id); ?>" onmouseover="highlightCells(this.className); this.style.cursor = 'pointer';" onmouseout="dehighlightCells(this.className); this.style.cursor = '';" onclick="updateWaitingPatient('<? echo($tmp_ptwait_id); ?>');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><nobr><? echo($tmp_charge_emp_nm); ?></nobr></font></td>
<? } ?>
<td class="wait<? echo($tmp_ptwait_id); ?>" onmouseover="highlightCells(this.className); this.style.cursor = 'pointer';" onmouseout="dehighlightCells(this.className); this.style.cursor = '';" onclick="updateWaitingPatient('<? echo($tmp_ptwait_id); ?>');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_progress); ?></font></td>
<? if ($display2 == "t") { ?>
<td class="wait<? echo($tmp_ptwait_id); ?>" onmouseover="highlightCells(this.className); this.style.cursor = 'pointer';" onmouseout="dehighlightCells(this.className); this.style.cursor = '';" onclick="updateWaitingPatient('<? echo($tmp_ptwait_id); ?>');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_intro_inst_name); ?></font></td>
<? } ?>
<? if ($display3 == "t") { ?>
<td class="wait<? echo($tmp_ptwait_id); ?>" onmouseover="highlightCells(this.className); this.style.cursor = 'pointer';" onmouseout="dehighlightCells(this.className); this.style.cursor = '';" onclick="updateWaitingPatient('<? echo($tmp_ptwait_id); ?>');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_intro_sect_name); ?></font></td>
<? } ?>
<? if ($display4 == "t") { ?>
<td class="wait<? echo($tmp_ptwait_id); ?>" onmouseover="highlightCells(this.className); this.style.cursor = 'pointer';" onmouseout="dehighlightCells(this.className); this.style.cursor = '';" onclick="updateWaitingPatient('<? echo($tmp_ptwait_id); ?>');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_intro_doctor_name); ?></font></td>
<? } ?>
<? if ($display11 == "t") { ?>
<td class="wait<? echo($tmp_ptwait_id); ?>" onmouseover="highlightCells(this.className); this.style.cursor = 'pointer';" onmouseout="dehighlightCells(this.className); this.style.cursor = '';" onclick="updateWaitingPatient('<? echo($tmp_ptwait_id); ?>');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_msw); ?></font></td>
<? } ?>
<? if ($display6 == "t") { ?>
<td class="wait<? echo($tmp_ptwait_id); ?>" onmouseover="highlightCells(this.className); this.style.cursor = 'pointer';" onmouseout="dehighlightCells(this.className); this.style.cursor = '';" onclick="updateWaitingPatient('<? echo($tmp_ptwait_id); ?>');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_disease); ?></font></td>
<? } ?>
<? if ($display7 == "t") { ?>
<td class="wait<? echo($tmp_ptwait_id); ?>" onmouseover="highlightCells(this.className); this.style.cursor = 'pointer';" onmouseout="dehighlightCells(this.className); this.style.cursor = '';" onclick="updateWaitingPatient('<? echo($tmp_ptwait_id); ?>');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_patho); ?></font></td>
<? } ?>
<? if ($display8 == "t") { ?>
<td class="wait<? echo($tmp_ptwait_id); ?>" onmouseover="highlightCells(this.className); this.style.cursor = 'pointer';" onmouseout="dehighlightCells(this.className); this.style.cursor = '';" onclick="updateWaitingPatient('<? echo($tmp_ptwait_id); ?>');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_note); ?></font></td>
<? } ?>
<? if ($display5 == "t") { ?>
<td class="wait<? echo($tmp_ptwait_id); ?>" onmouseover="highlightCells(this.className); this.style.cursor = 'pointer';" onmouseout="dehighlightCells(this.className); this.style.cursor = '';" onclick="updateWaitingPatient('<? echo($tmp_ptwait_id); ?>');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_dementia); ?></font></td>
<? } ?>
<td class="wait<? echo($tmp_ptwait_id); ?>" onmouseover="highlightCells(this.className); this.style.cursor = 'pointer';" onmouseout="dehighlightCells(this.className); this.style.cursor = '';" onclick="updateWaitingPatient('<? echo($tmp_ptwait_id); ?>');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_reception_date); ?></font></td>
<td class="wait<? echo($tmp_ptwait_id); ?>" onmouseover="highlightCells(this.className); this.style.cursor = 'pointer';" onmouseout="dehighlightCells(this.className); this.style.cursor = '';" onclick="updateWaitingPatient('<? echo($tmp_ptwait_id); ?>');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_verified_date); ?></font></td>
<? if ($display9 == "t") { ?>
<td class="wait<? echo($tmp_ptwait_id); ?>" onmouseover="highlightCells(this.className); this.style.cursor = 'pointer';" onmouseout="dehighlightCells(this.className); this.style.cursor = '';" onclick="updateWaitingPatient('<? echo($tmp_ptwait_id); ?>');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_status1); ?></font></td>
<? } ?>
<? if ($display10 == "t") { ?>
<td class="wait<? echo($tmp_ptwait_id); ?>" onmouseover="highlightCells(this.className); this.style.cursor = 'pointer';" onmouseout="dehighlightCells(this.className); this.style.cursor = '';" onclick="updateWaitingPatient('<? echo($tmp_ptwait_id); ?>');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_status2); ?></font></td>
<? } ?>
<td class="wait<? echo($tmp_ptwait_id); ?>" onmouseover="highlightCells(this.className); this.style.cursor = 'pointer';" onmouseout="dehighlightCells(this.className); this.style.cursor = '';" onclick="updateWaitingPatient('<? echo($tmp_ptwait_id); ?>');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_ccl_rsn); ?></font></td>
<td class="wait<? echo($tmp_ptwait_id); ?>" onmouseover="highlightCells(this.className); this.style.cursor = 'pointer';" onmouseout="dehighlightCells(this.className); this.style.cursor = '';" onclick="updateWaitingPatient('<? echo($tmp_ptwait_id); ?>');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_inpt_in_dt); ?></font></td>
</tr>
<?
}
?>
</table>
<? } ?>

<form name="excelform" action="patient_waiting_list_excel.php" method="get" target="download">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="from_date" value="<? echo($date_y1 . $date_m1 . $date_d1); ?>">
<input type="hidden" name="to_date" value="<? echo($date_y2 . $date_m2 . $date_d2); ?>">
<input type="hidden" name="progress_filter" value="<? echo($progress_filter); ?>">
<input type="hidden" name="cond_id" value="<? echo($cond_id); ?>">
<input type="hidden" name="cond_name" value="<? echo($cond_name); ?>">
</form>
<iframe name="download" width="0" height="0" frameborder="0"></iframe>

</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
function ptwait_cond($date_y1, $date_m1, $date_d1, $date_y2, $date_m2, $date_d2, $progress_filter, $cond_id, $cond_name) {
	$cond = "where w.reception_time between '{$date_y1}{$date_m1}{$date_d1}0000' and '{$date_y2}{$date_m2}{$date_d2}9999'";

	switch ($progress_filter) {
	case "2":  // 待機中
		$cond .= " and w.progress = '1'";
		break;
	case "3":  // キャンセル
		$cond .= " and w.progress = '2'";
		break;
	case "4":  // その他
		$cond .= " and w.progress = '5'";
		break;
	}

	mb_regex_encoding("EUC-JP");
	$cond_id = mb_ereg_replace("[ 　]", "", $cond_id);
	$cond_name = mb_ereg_replace("[ 　]", "", $cond_name);
	$pt_conds = array();
	if ($cond_id !== "") {
		$pt_conds[] = "(p.ptif_id = '$cond_id')";
	}
	if ($cond_name !== "") {
		$pt_conds[] = "(p.ptif_lt_kaj_nm || p.ptif_ft_kaj_nm like '%$cond_name%') or (p.ptif_lt_kana_nm || p.ptif_ft_kana_nm like '%$cond_name%')";
	}
	if (count($pt_conds) > 0) {
		$cond .= " and (" . join(" or ", $pt_conds) . ")";
	}

	return $cond;
}

function show_navigation($con, $count, $page, $date_y1, $date_m1, $date_d1, $date_y2, $date_m2, $date_d2, $progress_filter, $cond_id, $cond_name, $session, $fname) {
	$max_page = ceil($count / ROWS_PER_PAGE);
	$start = ($page - 1) * ROWS_PER_PAGE + 1;
	$end = $page * ROWS_PER_PAGE;
	if ($end > $count) $end = $count;

	$e_cond_id = urlencode($cond_id);
	$e_cond_name = urlencode($cond_name);

	echo("<p style=\"margin:0 0 3px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><span style=\"margin-right:15px;\">");
	if ($page > 1) {
		echo("<a href=\"?session=$session&mode=search&date_y1=$date_y1&date_m1=$date_m1&date_d1=$date_d1&date_y2=$date_y2&date_m2=$date_m2&date_d2=$date_d2&progress_filter=$progress_filter&cond_id=$e_cond_id&cond_name=$e_cond_name&page=" . ($page - 1) . "\">");
	} else {
		echo("<font color=\"gray\">");
	}
	echo("←前のページ");
	if ($page > 1) {
		echo("</a>");
	} else {
		echo("</font>");
	}
	echo("</span>{$count}件中 {$start}");
	if ($start != $end) {
		echo("〜{$end}");
	}
	echo("件目を表示中<span style=\"margin-left:15px;\">");
	if ($page < $max_page) {
		echo("<a href=\"?session=$session&mode=search&date_y1=$date_y1&date_m1=$date_m1&date_d1=$date_d1&date_y2=$date_y2&date_m2=$date_m2&date_d2=$date_d2&progress_filter=$progress_filter&cond_id=$e_cond_id&cond_name=$e_cond_name&page=" . ($page + 1) . "\">");
	} else {
		echo("<font color=\"gray\">");
	}
	echo("次のページ→");
	if ($page < $max_page) {
		echo("</a>");
	} else {
		echo("</font>");
	}
	echo("</span></font></p>");
}

function calc_age($birth) {
	$age = date("Y") - substr($birth, 0, 4);
	if (date("md") < substr($birth, 4, 8)) $age--;
	return $age;
}

function format_sex($sex) {
	switch ($sex) {
	case "1":
		return "男";
	case "2":
		return "女";
	default:
		return "不";
	}
}

function format_progress($progress) {
	switch ($progress) {
	case "1":
		return "待機中";
	case "2":
		return "待機キャンセル";
	case "3":
		return "入院予定";
	case "4":
		return "入院済み";
	case "5":
		return "その他";
	}
}

function format_patho($from, $to) {
	if ($from == "" && $to == "") return "";
	$days = array(format_date($from), format_date($to));
	return join(" 〜 ", $days);
}

function format_date($date) {
	return preg_replace("/\A(\d{4})(\d{2})(\d{2})\z/", "$1/$2/$3", $date);
}

function format_dementia($dementia) {
	switch ($dementia) {
	case "t":
		return "有り";
	case "f":
		return "無し";
	default:
		return "";
	}
}

function format_name_detail($name, $detail) {
	$detail = str_replace("\n", " ", $detail);
	if ($name != "" && $detail != "") return "{$name} {$detail}";
	return "{$name}{$detail}";
}
