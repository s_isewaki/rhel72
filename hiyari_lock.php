<?

//新たにロックを使用する場合はロックテーブルにIDを登録する必要あり。

//create table inci_lock(id smallint);
//insert into inci_lock values(1);
//select * from inci_lock;
//
//drop table inci_lock;

//require_once("hiyari_lock.php");
//action_lock(LOCK_HIYARI_REPORT_UPDATE);
//action_unlock(LOCK_HIYARI_REPORT_UPDATE);
//以上３行で開発環境にて0.16msかかった。

//メモ：PGSQL_CONNECT_FORCE_NEWでpg_connectした後、pg_connectするとどっちのコネクションが得られる？(ロック用コネクションは他から参照されては困る。)

//このモジュール、共通にしてもいいかも？(モジュール名とDB名を適切に変えれば。)
require_once("about_postgres.php");

/** ファントルくんのレポート更新処理に対するロック */
define("LOCK_HIYARI_REPORT_UPDATE",1);





/**
 * 処理をロックします。
 * ロックの有効期間は、action_unlock($id)の呼び出し、もしくはリクエスト終了までとなります。
 * 
 * @param integer $id ロック処理ID
 * @param string $fname ファイル名(省略可能)
 */
function action_lock($id,$fname="hiyari_lock.php")
{
	//ロック専用コネクションを新規作成
	$con = connect2db_for_lock_connection($fname);
	
	//トランザクションを開始
	pg_query($con,"begin transaction");
	
	//処理IDに対してロック取得
	//※既にロック取得されている場合はロック取得されるまで待機となる。
	$sql = "select * from inci_lock where id = $id for update";
	$sel = select_from_table($con,$sql,"",$fname);
	if($sel == 0){
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	
	//ロック専用コネクションを登録
	set_lock_connection($con,$id);
}

/**
 * アンロックします。
 * 
 * @param integer $id ロック処理ID
 */
function action_unlock($id)
{
	//ロック専用コネクションを取得
	$con = get_lock_connection($id);
	if($con == "")
	{
		//現在ロックされていないため処理不要
		return;
	}
	
	//コネクション終了(=トランザクションをロールバックで終了)によりロック解除
	pg_close($con);
}


//====================================================================================================
// ロック専用コネクションの生成／管理
//====================================================================================================

/**
 * (private)
 * ロック専用コネクションを新規作成します。
 * ※この関数はabout_postgres.phpのconnect2db($fname)にPGSQL_CONNECT_FORCE_NEWの指定を追加したものです。
 * 
 * @param string $fname $PHP_SELF等、呼び出し元を特定する文字列(ログでのみ使用する)
 * @return string ロック専用コネクション
 */
function connect2db_for_lock_connection($fname)
{
	@require("./conf/conf.inf");  								  				//環境設定ファイルの読み込み

	if(!defined("CONF_FILE")){	        					  						//読み込みの確認

			require("./conf/error.inf");												//エラーメッセージ一覧ファイルを読み込む
			require_once("./about_error.php");									//エラーメッセージの書き込み処理をする関数のファイルを読み込む
			write_error($fname,$ERRORLOG, $ERROR001);									//errorログへの書き込み

		return 0;																			//失敗の場合は０を返す
	}

	if(!@($con = (pg_connect("host=$IP4DBSERVER port=$PORT4DB dbname=$NAME4DB user=$USER4DB password=$PASSWD4DB",PGSQL_CONNECT_FORCE_NEW)))){	//dbに接続

			require("./conf/error.inf");												//エラーメッセージ一覧ファイルを読み込む
			require_once("./about_error.php");									//エラーメッセージの書き込み処理をする関数のファイルを読み込む
			write_error($fname,$ERRORLOG, $ERROR101);									//errorログへの書き込み

		return 0;																			//失敗の場合は０を返す
	}else{
		return $con;																		//成功の場合はデータベースとのこねクッション変数$conを返す
	}

}


/**
 * (private)
 * ロック専用コネクションテーブル (リクエストスコープで管理)
 */
$_____lock_connection_table = array();


/**
 * (private)
 * ロック専用コネクションをロック専用コネクションテーブルにエントリーします。
 * 
 * @param string $con ロック専用コネクション
 * @param integer $id ロック処理ID
 */
function set_lock_connection($con,$id)
{
	global $_____lock_connection_table;
	$_____lock_connection_table[$id] = $con;
}


/**
 * (private)
 * ロック専用コネクションをロック専用コネクションテーブルにエントリーします。
 * 
 * @param integer $id ロック処理ID
 * @return string ロック専用コネクション
 */
function get_lock_connection($id)
{
	global $_____lock_connection_table;
	return $_____lock_connection_table[$id];
}


//====================================================================================================
// テスト
//====================================================================================================

/**
 * (テスト用)
 * 当モジュールの動作を検証します。
 * 
 * @param integer $sleep_sec ロック中に行う処理にかかる時間
 */
function lock_test($sleep_sec)
{
	action_lock(LOCK_HIYARI_REPORT_UPDATE);
	print "処理開始:".date('h:i:s')."<br>";
	sleep($sleep_sec);
	print "処理終了:".date('h:i:s')."<br>";
	action_unlock(LOCK_HIYARI_REPORT_UPDATE);	
}

//以下のコメントアウトを外してを複数ブラウザで直接このモジュールを同時実行
//lock_test(15);


?>