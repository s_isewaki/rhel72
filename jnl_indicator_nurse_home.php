<?php
require_once 'jnl_indicator.php';
class IndicatorNurseHome extends Indicator
{
    /**
     * @access protected
     */
    var $subCategoryArr = array(
        '2' => array(
            '1' => '���Խ���'
          , '2' => '�̽�'
          , '3' => '����'
          , '4' => '���硼��'
          , '5' => '����¾',
        ),
    );

    /**
     * @access protected
     */
    var $noArr = array(
        /* ---------- Ϸ�� ���Խ��� ---------- */
        '100' => array(
            'name'         => '�����̴�������',
            'file'         => 'jnl_application_indicator_daily_report_detail_nurse_home.php',
            'category'     => '2',
            'sub_category' => '1',
            'file_name'    => 'name_y_m_d',
        ),
        '101' => array(
            'name'         => '�����̴�������(����ץ���)',
            'file'         => 'jnl_application_indicator_daily_report_simple_nurse_home.php',
            'category'     => '2',
            'sub_category' => '1',
            'file_name'    => 'name_y_m_d',
        ),
        '102' => array(
            'name'         => '�����̴�������(��������ɽ)',
            'file'         => 'jnl_application_indicator_daily_report_stats_nurse_home.php',
            'category'     => '2',
            'sub_category' => '1',
            'file_name'    => 'name_y_m_d',
        ),
        '105' => array(
            'name'         => '�����̴��Է���',
            'file'         => 'jnl_application_indicator_patient_mon_report_nurse_home.php',
            'category'     => '2',
            'sub_category' => '1',
            'file_name'    => 'name_y_m',
        ),
        '106' => array(
            'name'         => '�����̴��Է���(��ǯ���)',
            'file'         => 'jnl_application_indicator_patient_mon_report_cmp_nurse_home.php',
            'category'     => '2',
            'sub_category' => '1',
            'file_name'    => 'name_y_m',
        ),
        '107' => array(
            'name'         => '�����̴��Է���(�ܺ���)',
            'file'         => 'jnl_application_indicator_patient_mon_report_detail_nurse_home.php',
            'category'     => '2',
            'sub_category' => '1',
            'file_name'    => 'name_y_m',
        ),
        '108' => array(
            'name'         => 'Ϸ���ֲû����ܡ�������Ӱ���',
            'file'         => 'jnl_application_indicator_mon_report_list_nurse_home.php',
            'category'     => '2',
            'sub_category' => '1',
            'file_name'    => 'name_y_m',
        ),
        /* ---------- Ϸ�� �̽� ---------- */
        '110' => array(
            'name'         => 'Ϸ���ݷ�����̽��ƯΨ',
            'file'         => 'jnl_application_indicator_facility_visit_rate_nurse_home.php',
            'category'     => '2',
            'sub_category' => '2',
            'file_name'    => 'name_y',
        ),
        '111' => array(
            'name'         => 'Ϸ���ݷ�����̽�(���)���',
            'file'         => 'jnl_application_indicator_facility_comp_previous_year_nurse_home.php',
            'category'     => '2',
            'sub_category' => '2',
            'file_name'    => 'name_y',
        ),
        '112' => array(
            'name'         => 'Ϸ���ݷ�����̽�(���)���',
            'file'         => 'jnl_application_indicator_facility_comp_previous_year_nurse_home.php',
            'category'     => '2',
            'sub_category' => '2',
            'file_name'    => 'name_y',
        ),
        '113' => array(
            'name'         => 'Ϸ���ݷ�����̽�(ͽ��)���',
            'file'         => 'jnl_application_indicator_facility_comp_previous_year_nurse_home.php',
            'category'     => '2',
            'sub_category' => '2',
            'file_name'    => 'name_y',
        ),
        /* ---------- Ϸ�� ���� ---------- */
        '114' => array(
            'name'         => 'Ϸ���ݷ���������ƯΨ',
            'file'         => 'jnl_application_indicator_facility_enter_rate_nurse_home.php',
            'category'     => '2',
            'sub_category' => '3',
            'file_name'    => 'name_y',
        ),
        '115' => array(
            'name'         => 'Ϸ���ݷ��������(���)���',
            'file'         => 'jnl_application_indicator_facility_comp_previous_year_nurse_home.php',
            'category'     => '2',
            'sub_category' => '3',
            'file_name'    => 'name_y',
        ),
        '116' => array(
            'name'         => 'Ϸ���ݷ��������(����)���',
            'file'         => 'jnl_application_indicator_facility_comp_previous_year_nurse_home.php',
            'category'     => '2',
            'sub_category' => '3',
            'file_name'    => 'name_y',
        ),
        '117' => array(
            'name'         => 'Ϸ���ݷ��������(ǧ��)���',
            'file'         => 'jnl_application_indicator_facility_comp_previous_year_nurse_home.php',
            'category'     => '2',
            'sub_category' => '3',
            'file_name'    => 'name_y',
        ),
        /* ---------- Ϸ�� ���硼�� ---------- */
        '118' => array(
            'name'         => 'Ϸ���ݷ��������(���̥��硼��)���',
            'file'         => 'jnl_application_indicator_facility_comp_previous_year_nurse_home.php',
            'category'     => '2',
            'sub_category' => '4',
            'file_name'    => 'name_y',
        ),
        '119' => array(
            'name'         => 'Ϸ���ݷ��������(ǧ�Υ��硼��)���',
            'file'         => 'jnl_application_indicator_facility_comp_previous_year_nurse_home.php',
            'category'     => '2',
            'sub_category' => '4',
            'file_name'    => 'name_y',
        ),
        '120' => array(
            'name'         => 'Ϸ���ݷ��������(ͽ�ɥ��硼��)���',
            'file'         => 'jnl_application_indicator_facility_comp_previous_year_nurse_home.php',
            'category'     => '2',
            'sub_category' => '4',
            'file_name'    => 'name_y',
        ),
        /* ---------- Ϸ�� ����¾ ---------- */
        '121' => array(
            'name'         => '��������Ψ',
            'file'         => 'jnl_application_indicator_facility_comp_previous_year_nurse_home.php',
            'category'     => '2',
            'sub_category' => '5',
            'file_name'    => 'name_y',
            'side_average' => true,
            'amg_average'  => true,
            'percent'      => true,
        ),
        '122' => array(
            'name'         => 'Ƚ����',
            'file'         => 'jnl_application_indicator_facility_comp_previous_year_nurse_home.php',
            'category'     => '2',
            'sub_category' => '5',
            'file_name'    => 'name_y',
        ),
        '123' => array(
            'name'         => '�Ķȷ��',
            'file'         => 'jnl_application_indicator_facility_comp_previous_year_nurse_home.php',
            'category'     => '2',
            'sub_category' => '5',
            'file_name'    => 'name_y',
            'raito'        => true,
            'side_total'   => true,
            'side_average' => true,
            'amg_total'    => true,
        ),
    );

    /**
     * ���󥹥ȥ饯��
     * @param $arr
     */
    function IndicatorNurseHome($arr) { parent::Indicator($arr); }
}