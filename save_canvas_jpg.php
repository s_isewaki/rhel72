<?php
//*******************************************************************
//* save_canvas_jpg.php                                              
//*******************************************************************

    if ($_POST['canvas_jpg_name'] != '' && $_POST['img'] != '') {
        saveCvsdata($_POST['canvas_jpg_name'], $_POST['img']);
    }

    if ($_POST['canvas_jpg_name1'] != '' && $_POST['img1'] != '') {
        saveCvsdata($_POST['canvas_jpg_name1'], $_POST['img1']);
    }

    if ($_POST['canvas_jpg_name2'] != '' && $_POST['img2'] != '') {
        saveCvsdata($_POST['canvas_jpg_name2'], $_POST['img2']);
    }

    function saveCvsdata($name, $data)
    {
        $dir = 'canvas_data_temp/';

        $path = $dir . $name;

        if (!is_dir($dir)) {
            mkdir($dir, 0755);
        }

        $fp = fopen($path, 'w+');
//        fwrite($fp, base64_decode($data));
        fwrite($fp, base64_decode(str_replace(' ', '+', $data)));
        fclose($fp);

    }
?>
