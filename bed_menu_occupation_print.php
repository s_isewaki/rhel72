<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>病床管理 | 病床利用状況</title>
<?
require("about_session.php");
require("about_authority.php");
require("show_occupation_detail_print.ini");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 病床管理権限チェック
$checkauth = check_authority($session, 14, $fname);
if($checkauth == "0"){
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 棟名を取得
$sql = "select bldg_name from bldgmst";
$cond = "where bldg_cd = '$bldg_cd'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$bldg_name = pg_fetch_result($sel, 0, "bldg_name");

// 病棟名を取得
if ($ward_cd == "") {
	$ward_name = "すべて";
} else {
	$sql = "select ward_name from wdmst";
	$cond = "where bldg_cd = '$bldg_cd' and ward_cd = '$ward_cd'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$ward_name = pg_fetch_result($sel, 0, "ward_name");
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/popup.js"></script>
<script type="text/javascript">
function printPage() {
	window.open('bed_menu_occupation_print.php?session=<? echo($session); ?>&bldg_cd=<? echo($bldg_cd); ?>&ward_cd=<? echo($ward_cd); ?>', 'occp', 'width=800,height=600,scrollbars=yes');
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
form {margin:0;}
table.submenu {border-collapse:collapse;}
table.submenu td {border:#5279a5 solid 1px;}
table.bed {border-collapse:collapse;}
table.bed td {border:solid #5279a5 1px;}
</style>
<style type="text/css" media="print">
div.separator {visibility:hidden; page-break-before:always;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="self.print();self.close();">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr height="22" bgcolor="#f6f9ff">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">事業所（棟）：<? echo($bldg_name); ?> 病棟：<? echo($ward_name); ?></font></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<?
if ($ward_cd == "") {
	show_bldg_report($con, $bldg_cd, $fname);
}

show_ward_layout($con, $bldg_cd, $ward_cd, $session, $fname);

if ($ward_cd != "") {
	show_ward_report($con, $bldg_cd, $ward_cd, $fname);
}
?>
</body>
<? pg_close($con); ?>
</html>
