<?php
//**********このファイルはファイルの操作とデータの保存に関するものです**********

//*****fileを開く*****
function open_file($file,$arw){
@require("./conf/conf.inf");  								  					//環境設定ファイルの読み込み

	if(!defined("CONF_FILE")){	        					  					//読み込みの確認
		require("./conf/error.inf");												//エラーメッセージ一覧ファイルを読み込む
		require_once("./about_error.php");									//エラーメッセージの書き込み処理をする関数のファイルを読み込む
		write_error($ERRORLOG, $ERROR001);									//errorログへの書き込み

		return 0;																		//失敗の場合は０を返す
	}

	if(!@$file=fopen("$file","$arw")){											//fopenでファイルを開く$arwでどのような権限で開くかを決定する
		require("./conf/error.inf");												//エラーメッセージ一覧ファイルを読み込む
		require_once("./about_error.php");									//エラーメッセージの書き込み処理をする関数のファイルを読み込む
		write_error($ERRORLOG, $ERROR201);								//errorログへの書き込み
		return 0;																		//ファイルを開くのに失敗した場合0を返す
	}else{
		return $file;																	//ファイルを開くのに成功した場合$fileを返す
	}

}

//*****fileを閉じる*****
function close_file($file){
@require("./conf/conf.inf");

	if(!defined("CONF_FILE")){	 		       					  				//読み込みの確認
		require("./conf/error.inf");												//エラーメッセージ一覧ファイルを読み込む
		require_once("./about_error.php");									//エラーメッセージの書き込み処理をする関数のファイルを読み込む
		write_error($ERRORLOG, $ERROR001);								//errorログへの書き込み
		return 0;																		//失敗の場合は０を返す
	}

	if(!@$close=fclose($file)){														//fcloseでファイルを閉じる
		require("./conf/error.inf");												//エラーメッセージ一覧ファイルを読み込む
		require_once("./about_error.php");									//エラーメッセージの書き込み処理をする関数のファイルを読み込む
		write_error($ERRORLOG, $ERROR202);								//errorログへの書き込み
		return 0;																		//ファイルを開くのに失敗した場合0を返す
	}else{
		return 1;																		//ファイルを開くのに成功した場合1を返す
	}
}

//*****fileの全体を読み込む*****
function read_whole_file($file){
@require("./conf/conf.inf");

	if(!defined("CONF_FILE")){	 		       					  				//読み込みの確認
		require("./conf/error.inf");												//エラーメッセージ一覧ファイルを読み込む
		require_once("./about_error.php");									//エラーメッセージの書き込み処理をする関数のファイルを読み込む
		write_error($ERRORLOG, $ERROR001);								//errorログへの書き込み
		return 0;																		//失敗の場合は０を返す
	}

	if(!@$wread=fpassthru($file)){												//fpassthruを使ってファイル内容全部を読み込む(この場合にはファイルを閉じる必要はない)
		require("./conf/error.inf");												//エラーメッセージ一覧ファイルを読み込む
		require_once("./about_error.php");									//エラーメッセージの書き込み処理をする関数のファイルを読み込む
		write_error($ERRORLOG, $ERROR203);								//errorログへの書き込み
		return 0;																		//ファイルを開くのに失敗した場合0を返す
	}else{
		return $wread;																//ファイルを開くのに成功した場合$wreadを返す
	}
}

//*****fileから指定した文字数読み込む*****
function read_part_file($file,$len){
@require("./conf/conf.inf");

	if(!defined("CONF_FILE")){	 		       					  				//読み込みの確認
		require("./conf/error.inf");												//エラーメッセージ一覧ファイルを読み込む
		require_once("./about_error.php");									//エラーメッセージの書き込み処理をする関数のファイルを読み込む
		write_error($ERRORLOG, $ERROR001);								//errorログへの書き込み
		return 0;																		//失敗の場合は０を返す
	}

	if(!@$pread=fread($file,$len)){												//freadで$lenの長さのデータを読み込む(ファイルを閉じる必要あり)
		require("./conf/error.inf");												//エラーメッセージ一覧ファイルを読み込む
		require_once("./about_error.php");									//エラーメッセージの書き込み処理をする関数のファイルを読み込む
		write_error($ERRORLOG, $ERROR204);								//errorログへの書き込み
		return 0;																		//ファイルを開くのに失敗した場合0を返す
	}else{
		return $pread;																//ファイルを開くのに成功した場合$preadを返す
	}
}


//*****fileに書き込む*****
function write_into_file($file,$message){
@require("./conf/conf.inf");

	if(!defined("CONF_FILE")){	 		       					  				//読み込みの確認
		require("./conf/error.inf");												//エラーメッセージ一覧ファイルを読み込む
		require_once("./about_error.php");									//エラーメッセージの書き込み処理をする関数のファイルを読み込む
		write_error($ERRORLOG, $ERROR001);								//errorログへの書き込み
		return 0;																		//失敗の場合は０を返す
	}

	if(!@$write=fputs($file,$message)){										//fputsを使ってファイルに書き込みを行う(ファイルを閉じる必要あり)
		require("./conf/error.inf");												//エラーメッセージ一覧ファイルを読み込む
		require_once("./about_error.php");									//エラーメッセージの書き込み処理をする関数のファイルを読み込む
		write_error($ERRORLOG, $ERROR205);								//errorログへの書き込み
		return 0;																		//ファイルを開くのに失敗した場合0を返す
	}else{
		return 1;																		//ファイルを開くのに成功した場合1を返す
	}
}

//*****fileの存在を確認する*****
function check_file($file){
@require("./conf/conf.inf");

	if(!defined("CONF_FILE")){	 		       					  				//読み込みの確認
		require("./conf/error.inf");												//エラーメッセージ一覧ファイルを読み込む
		require_once("./about_error.php");									//エラーメッセージの書き込み処理をする関数のファイルを読み込む
		write_error($ERRORLOG, $ERROR001);								//errorログへの書き込み
		return 0;																		//失敗の場合は０を返す
	}

	$exist = file_exists($file);														//file_existsでファイルの存在を確認
	if($exist == 0){
		require("./conf/error.inf");												//エラーメッセージ一覧ファイルを読み込む
		require_once("./about_error.php");									//エラーメッセージの書き込み処理をする関数のファイルを読み込む
		write_error($ERRORLOG, $ERROR206);								//errorログへの書き込み
		return 0;																		//ファイルが存在しない場合0を返す
	}else{
		return 1;																		//ファイル存在する場合1を返す
	}
}

//*****csvファイルから配列でデータを取り出す*****
function csv_read($file,$file_size){
@require("./conf/conf.inf");

	if(!defined("CONF_FILE")){	 		       					  				//読み込みの確認
		require("./conf/error.inf");												//エラーメッセージ一覧ファイルを読み込む
		require_once("./about_error.php");									//エラーメッセージの書き込み処理をする関数のファイルを読み込む
		write_error($ERRORLOG, $ERROR001);								//errorログへの書き込み
		return 0;																		//失敗の場合は０を返す
	}

	if(!@$csvread = fgetcsv($file,$file_size,",")){							//fgetcsvを使ってcsvファイルからの読み込み行う
		require("./conf/error.inf");												//エラーメッセージ一覧ファイルを読み込む
		require_once("./about_error.php");									//エラーメッセージの書き込み処理をする関数のファイルを読み込む
		write_error($ERRORLOG, $ERROR207);								//errorログへの書き込み
		return 0;																		//ファイルを開くのに失敗した場合0を返す
	}else{
		return $csvread;																//ファイルを開くのに成功した場合1を返す
	}
}

?>