<?php
global $table_ukey;

//ini_set('memory_limit', '50M');
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// DB・システムユーティリティ
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
function lib_debug_backtrace() {
    echo "<pre>";
    $out = array();
    foreach (debug_backtrace() as $d) {
        $out[]=array("file"=>$d["file"], "function"=>$d["function"], "line"=>$d["line"]);
    }
    print_r($out);
}
function lib_get_one($con, $fname, $sql) {
    $sel = select_from_table($con, $sql, "", $fname);
    if ($sel == 0) libcom_goto_error_page_and_die($sql);
    $row = pg_fetch_array($sel);
    return $row[0];
}
function lib_get_top_row($con, $fname, $sql) {
    $sel = select_from_table($con, $sql, "", $fname);
    if ($sel == 0) libcom_goto_error_page_and_die($sql);
    return pg_fetch_assoc($sel);
}
// エラーが発生した場合の処理。AJAXのときはエラーコードを返す
function libcom_goto_error_page_and_die($sql="", $con=0) {
    if ($con) pg_query($con, "rollback");
    if ($_REQUEST["ajax_action"]) {
        echo "SYSTEM_ERROR";
        global $con;
        echo "<pre>";
        lib_debug_backtrace();
        if ($con) echo pg_last_error($con)."<br>";
        echo $sql;
        exit;
    }
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}


//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// 書庫名を取得
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
function lib_get_archive_name($a) {
    return get_lib_archive_label($a);
}
function get_lib_archive_label($a) {
    if ($a=="1") return "個人ファイル";
    if ($a=="2") return "全体共有";
    if ($a=="3") return "部署共有";
    if ($a=="4") return "委員会・WG共有";
    return "";
}
function get_lib_archive_label2($a) {
    if ($a=="1") return "個人";
    if ($a=="2") return "全体";
    if ($a=="3") return "部署";
    if ($a=="4") return "委員会・WG";
    return "";
}

function get_document_path($a, $c, $did, $ext) {
    $dirname = "";
    if ($a=="2") $dirname = "all"; // 全体共有
    if ($a=="3") $dirname = "section"; // 部署共有
    if ($a=="4") $dirname = "project"; // 委員会・WG共有
    if ($a=="1") $dirname = "private"; // 個人ファイル
    return "docArchive/".$dirname."/cate".$c."/document".$did.".".$ext;
}

//――――――――――――――――――――――――
// 使用可能な書庫一覧を返す
//――――――――――――――――――――――――
function lib_available_archives($con, $fname, $for_public) {
    $available_archives = array("all");
    $section_use = "t";
    $project_use = "t";
    $private_use = "t";

    $row = lib_get_libconfig($con, $fname);
    if (count($row)) {
        $section_use = $row["section_archive_use"];
        $project_use = $row["project_archive_use"];
        $private_use = $row["private_archive_use"];
    }

    if ($section_use=="t") $available_archives[] = "section";
    if ($project_use=="t") $available_archives[] = "project";
    if ($private_use=="t" && !$for_public) $available_archives[] = "private";

    return $available_archives;
}

//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// カテゴリ名を取得
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
function lib_get_category_name($con, $c, $fname) {
    if ($c == "") return;
    $sql = "select lib_cate_nm from libcate where lib_cate_id = $c";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) libcom_goto_error_page_and_die($sql);
    return @pg_fetch_result($sel, 0, "lib_cate_nm");
}


//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// ファイルサイズ文字をフォーマット
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
function format_size($size) {
    if ($size == 0) {
        return "-";
    } else if ($size <= 1023) {
        return "{$size}バイト";
    } else if ($size >= 1024 && $size <= 1048576) {
        $ret = number_format(($size / 1024), 2) . "KB";
        $ret = str_replace(".00", "", $ret);
        return preg_replace("/(.*)\.(\d)0/", "$1.$2", $ret);
    } else {
        $ret = number_format(($size / 1024 / 1024), 2) . "MB";
        $ret = str_replace(".00", "", $ret);
        return preg_replace("/(.*)\.(\d)0/", "$1.$2", $ret);
    }
}


//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// フォルダパスを取得
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
function lib_get_folder_path($con, $f, $fname) {
    $_f = $f;
    $ret = array();
    $dup_checker = array();

    for (;;) {
        if (!$_f) return $ret;
        if (in_array($_f, $dup_checker)) {
            $ret[]= array("id" => $_f, "name" => "（循環参照エラー）", "link_id" => "");
            return $ret;
        }
        $dup_checker[]= $_f;

        $sql = "select folder_name from libfolder where folder_id = ".$_f;
        $sel = select_from_table($con, $sql, "", $fname);
        if ($sel == 0) libcom_goto_error_page_and_die();
        $folder_name = @pg_fetch_result($sel, 0, "folder_name");
        array_unshift($ret, array("id" => $_f, "name" => $folder_name, "link_id" => $lib["link_id"]));

        $sql = "select parent_id from libtree where child_id = ".$_f;
        $sel = select_from_table($con, $sql, "", $fname);
        if ($sel == 0) libcom_goto_error_page_and_die();
        $_f = @pg_fetch_result($sel, 0, "parent_id");
    }
    return $ret;
}

//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// フォルダパスを出力
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
function lib_write_folder_path($path, $pre_separator = false) {
    if (count($path) == 0) return;
    $folder_names = array();
    foreach ($path as $tmp_folder) $folder_names[] = $tmp_folder["name"];
    if ($pre_separator) echo(" &gt; ");
    echo(implode(" &gt; ", $folder_names));
}

//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// ファイルの種類を取得
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
function lib_get_file_type_name($file_type) {
    if ($file_type=="1") return "Word";
    if ($file_type=="2") return "Excel";
    if ($file_type=="3") return "PowerPoint";
    if ($file_type=="4") return "PDF";
    if ($file_type=="5") return "テキスト";
    if ($file_type=="6") return "JPEG";
    if ($file_type=="7") return "GIF";
    if ($file_type=="99") return "その他";
    return "";
}

//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// useragentに対応するコードを取得
// 1:MSIE 2:Firefox 3:Opera 4:google Chrome 5:Safari 0:左記以外、ファイル名に文書名を使用しない
// 文書名フラグがfalseの場合、0を返す
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
function lib_get_filename_flg() {
    $fname = $_SERVER['PHP_SELF'];
    require_once("about_postgres.php");
    $con = connect2db($fname);
    $real_name_flg = lib_get_real_name_flg($con, $fname);
    if ($real_name_flg == "f") return 0;

    if      (strpos($_SERVER["HTTP_USER_AGENT"], "MSIE")    !== FALSE) return 1;
    else if (strpos($_SERVER["HTTP_USER_AGENT"], "Trident") !== FALSE) return 11;
    else if (strpos($_SERVER["HTTP_USER_AGENT"], "Firefox") !== FALSE) return 2;
    else if (strpos($_SERVER["HTTP_USER_AGENT"], "Opera")   !== FALSE) return 3;
    else if (strpos($_SERVER["HTTP_USER_AGENT"], "Chrome")  !== FALSE) return 4;
    else if (strpos($_SERVER["HTTP_USER_AGENT"], "Safari")  !== FALSE) return 5;
    return 0;
}

//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// ソート
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// 文書一覧を名前の昇順でソート
function sort_docs_by_name($doc1, $doc2) {
    global $stick_folder;
    if ($stick_folder == "t") {
        if ($doc1["type"] == "folder" && $doc2["type"] == "document") return -1;
        else if ($doc1["type"] == "document" && $doc2["type"] == "folder") return 1;
    }
    if ($doc1["name"]!=$doc2["name"]) return strcasecmp($doc1["name"], $doc2["name"]);
    return strcasecmp($doc1["id"], $doc2["id"]);
}

// 文書一覧を名前の降順でソート
function sort_docs_by_name_desc($doc1, $doc2) {
    global $stick_folder;
    if ($stick_folder == "t") {
        if ($doc1["type"] == "folder" && $doc2["type"] == "document") return -1;
        else if ($doc1["type"] == "document" && $doc2["type"] == "folder") return 1;
    }
    if ($doc1["name"]!=$doc2["name"]) return sort_docs_by_name($doc1, $doc2) * -1;
    return strcasecmp($doc1["id"], $doc2["id"]);
}

// 文書一覧を更新日時の昇順でソート
function sort_docs_by_modified($doc1, $doc2) {
    global $stick_folder;
    if ($stick_folder == "t") {
        if ($doc1["type"] == "folder" && $doc2["type"] == "document") return -1;
        else if ($doc1["type"] == "document" && $doc2["type"] == "folder") return 1;
    }
    if ($doc1["modified"]!=$doc2["modified"]) return strcasecmp($doc1["modified"], $doc2["modified"]);
    return strcasecmp($doc1["id"], $doc2["id"]);
}

// 文書一覧を更新日時の降順でソート
function sort_docs_by_modified_desc($doc1, $doc2) {
    global $stick_folder;
    if ($stick_folder == "t") {
        if ($doc1["type"] == "folder" && $doc2["type"] == "document") return -1;
        else if ($doc1["type"] == "document" && $doc2["type"] == "folder") return 1;
    }
    if ($doc1["name"]!=$doc2["name"]) return sort_docs_by_modified($doc1, $doc2) * -1;
    return strcasecmp($doc1["id"], $doc2["id"]) * -1;
}

// 文書一覧をサイズの昇順でソート
function sort_docs_by_size($doc1, $doc2) {
    global $stick_folder;
    if ($stick_folder == "t") {
        if ($doc1["type"] == "folder" && $doc2["type"] == "document") return -1;
        else if ($doc1["type"] == "document" && $doc2["type"] == "folder") return 1;
    }
    if ($doc1["size"]!=$doc2["size"]) return strnatcasecmp($doc1["size"], $doc2["size"]);
    return strcasecmp($doc1["id"], $doc2["id"]);
}

// 文書一覧をサイズの降順でソート
function sort_docs_by_size_desc($doc1, $doc2) {
    global $stick_folder;
    if ($stick_folder == "t") {
        if ($doc1["type"] == "folder" && $doc2["type"] == "document") return -1;
        else if ($doc1["type"] == "document" && $doc2["type"] == "folder") return 1;
    }
    if ($doc1["size"]!=$doc2["size"]) return strnatcasecmp($doc1["size"], $doc2["size"]) * -1;
    return strcasecmp($doc1["id"], $doc2["id"]);
}

// 文書一覧を番号の昇順でソート
function sort_docs_by_no($doc1, $doc2) {
    global $stick_folder;
    if ($stick_folder == "t") {
        if ($doc1["type"] == "folder" && $doc2["type"] == "document") return -1;
        else if ($doc1["type"] == "document" && $doc2["type"] == "folder") return 1;
    }
    if ($doc1["no"] != $doc2["no"]) return strcasecmp($doc1["no"], $doc2["no"]);
    return sort_docs_by_name($doc1, $doc2);
}

// 文書一覧を番号の降順でソート
function sort_docs_by_no_desc($doc1, $doc2) {
    global $stick_folder;
    if ($stick_folder == "t") {
        if ($doc1["type"] == "folder" && $doc2["type"] == "document") return -1;
        else if ($doc1["type"] == "document" && $doc2["type"] == "folder") return 1;
    }
    if ($doc1["no"] != $doc2["no"]) return strcasecmp($doc1["no"], $doc2["no"]) * -1;
    return sort_docs_by_name($doc1, $doc2);
}

//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// ibconfig取得
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
function lib_get_libconfig($con, $fname, $field="") {
    $sql = "select * from libconfig";
    $sel = select_from_table($con, $sql, "", $fname);
    if ($sel == 0) libcom_goto_error_page_and_die($sql);
    $row = pg_fetch_assoc($sel);
    if (!$field) return $row;
    return @$row[$field];
}
function lib_get_lock_show_flg($con, $fname) { // 鍵つきフォルダ表示フラグを取得
    $ret = lib_get_libconfig($con, $fname, "lock_show_flg");
    return ($ret!="" ? $ret : "t");
}
function lib_get_lock_show_all_flg($con, $fname) { // 鍵つきフォルダ表示フラグを取得
    $ret = lib_get_libconfig($con, $fname, "lock_show_all_flg");
    return ($ret!="" ? $ret : "f");
}
function lib_get_stick_folder($con, $fname) { // フォルダ固定フラグを取得
    $ret = lib_get_libconfig($con, $fname, "stick_folder");
    return ($ret!="" ? $ret : "f");
}
function lib_get_count_flg($con, $fname) { // フォルダ内文書カウントフラグを取得
    $ret = lib_get_libconfig($con, $fname, "count_flg");
    return ($ret!="" ? $ret : "f");
}
function lib_get_dragdrop_flg($con, $fname) { // ドラッグ＆ドロップ使用フラグを取得
    $ret = lib_get_libconfig($con, $fname, "dragdrop_flg");
    return ($ret!="" ? $ret : "f");
}
function lib_get_real_name_flg($con, $fname) { // 文書名フラグを取得
    $ret = lib_get_libconfig($con, $fname, "real_name_flg");
    return ($ret!="" ? $ret : "t");
}
function lib_get_show_login_flg($con, $fname) { // ログイン画面へ表示するフラグを取得
    $ret = lib_get_libconfig($con, $fname, "show_login_flg");
    return ($ret!="" ? $ret : "f");
}
function lib_get_show_tree_flg($con, $fname) { // フォルダツリーを表示するフラグを取得
    $ret = lib_get_libconfig($con, $fname, "show_tree_flg");
    return ($ret!="" ? $ret : "t");
}
function lib_get_pjt_private_flg($con, $fname) { // 「委員会メンバーの参照可能とする」フラグを取得
    $ret = lib_get_libconfig($con, $fname, "pjt_private_flg");
    return ($ret!="" ? $ret : "f");
}
function lib_get_show_newlist_flg($con, $fname) { // 最新文書一覧を表示するフラグを取得
    $ret = lib_get_libconfig($con, $fname, "show_newlist_flg");
    return ($ret!="" ? $ret : "f");
}
function lib_get_newlist_chk_flg($con, $fname) { // 最新文書一覧を表示させないチェックフラグを取得
    $ret = lib_get_libconfig($con, $fname, "newlist_chk_flg");
    return ($ret!="" ? $ret : "t");
}
function lib_get_all_createfolder_flg($con, $fname) { // 全体共有の最上位でフォルダの作成を許可するフラグを取得
    $ret = lib_get_libconfig($con, $fname, "all_createfolder_flg");
    return ($ret!="" ? $ret : "f");
}

//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// リンクなら実体を返す。aとfは指定しないといけない
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
function lib_get_link_real_node($con, $fname, $f) {
    if (!$f) { echo "fは必須<br>"; lib_debug_backtrace(); die; }
    $link_id = lib_get_one($con, $fname, "select link_id from libfolder where folder_id = ".$f);
    if (!link_id) return 0;
    return lib_get_top_row($con, $fname, "select * from libfolder where folder_id = ".$row["link_id"]);
}

//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// 含まれる文書数を再帰的にカウントする。カテゴリは必須指定されている
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
function lib_get_visible_doc_count($con, $fname, $mode, $emp_id, $a, $c, $f) {
    // $fがあれば、フォルダ⇒フォルダを含めたフォルダ以下のファイルが対象
    $folders = array();
    if ($f) {
        // まず権限関係なく、対象フォルダ階層を全部取得する
        $sql = "select lt.parent_id, lf.folder_id from libfolder lf left outer join libtree lt on (lt.child_id = lf.folder_id)";
        $sel = select_from_table($con, $sql, "", $fname);
        if (!$sel) libcom_goto_error_page_and_die($sql);
        $rows = array();
        $children = array();
        while ($row = pg_fetch_assoc($sel)) {
            $children[(int)$row["parent_id"]][]= $row["folder_id"];
        }
        // 子を収集する
        $rows = array($f);
        for (;;) {
            if (!count($rows)) break; // 探索フォルダがなければ終了
            $_f = array_shift($rows); // 探索用フォルダ先頭1件取得
            if ($folders[$_f]) continue; // 循環参照エラー
            $folders[$_f]= 1; // 対象として追加
            if (is_array($children[$_f])) foreach ($children[$_f] as $cr) $rows[]= $cr; // 子フォルダ群を探索用に追加
        }
    }
    unset($children);

    if ($mode=="admin") {
        $sql =
        " select count(li.lib_id) as cnt from libinfo li".
        " where li.lib_archive = '".$a."'".
        " and li.lib_cate_id = ".$c.
        " ".(count($folders) ? " and li.folder_id in (".implode(",", array_keys($folders)).")" : "").
        " and exists (".
        "     select le1.lib_id from libedition le1".
        "     inner join (".
        "         select base_lib_id, max(edition_no) as edition_no".
        "         from libedition".
        "         group by base_lib_id".
        "     ) le2 on (le2.base_lib_id = le1.base_lib_id and le2.edition_no = le1.edition_no)".
        "     where le1.lib_id = li.lib_id".
        " )";
        return lib_get_one($con, $fname, $sql);
    }

    // このSQLで、全フォルダのうち閲覧可能なものすべてが取得される。
    $lock_show_flg = lib_get_lock_show_flg($con, $fname);
    $lock_show_all_flg = lib_get_lock_show_all_flg($con, $fname);
    lib_cre_tmp_concurrent_table($con, $fname, $emp_id);
    $opt = array("is_admin"=>0, "is_login"=>0, "folder_ids"=>array_keys($folders));
    $sql1 = lib_get_libfolder_sql($emp_id, $lock_show_flg, $lock_show_all_flg, $a, $c, 0, $opt);
    $sql = "select folder_id, parent_id from (".$sql1.") d";
    $sel = select_from_table($con, $sql, "", $fname);
    if (!$sel) libcom_goto_error_page_and_die($sql);
    $auth_folders = array(0);
    while ($row = pg_fetch_array($sel)) $auth_folders[$row["folder_id"]] = $row;

    // このSQLで、
    // ◆参照可能なカテゴリの
    // ◆参照可能な文書
    // が取得される。
    $opt = array("is_admin"=>0, "is_login"=>0, "all_folder"=>($f ? "" :"1"), "exclude_orderby"=>1);
    $sql = lib_get_libinfo_sql($emp_id, $lock_show_flg, $lock_show_all_flg, $a, $c, "", ($f ? array_keys($auth_folders) : 0), $opt);

    $sel = select_from_table($con, $sql, "", $fname);
    if (!$sel) libcom_goto_error_page_and_die($sql);
    $ok_docs = array();
    while ($row = pg_fetch_array($sel)) {
        if (!lib_check_edition($con, $fname, $row["lib_id"])) continue;
        // フォルダが無ければつまりカテゴリ直下。カテゴリは参照可能確定済につきOK
        // 親が無い、つまりトップノード。トップノードが取得できているということはOK
        // それ以外なら探索
        if ($row["folder_id"] && $row["parent_id"]) {
            $_p = $row["parent_id"];
            $ng = 0;
            for (;;) {
                $_row = $auth_folders[$_p]; // 親レコード
                if (!$_row) { $ng= 1; break; }// 親レコード無し。NG確定
                $_p = $_row["parent_id"]; // 親の親フォルダID
                if (!$_p) break; // さらに親が無い。トップノード到達。OK確定
            }
            if ($ng) continue;
        }
        $ok_docs[$row["lib_id"]] = 1;
    }
    return count($ok_docs);
}

//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// 指定されたフォルダ上位（自身を含む）のフォルダ更新日時を更新
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
function lib_update_folder_modified($con, $c, $f, $fname, $emp_id, $a) {

    // フォルダ指定ありの場合
    $now_ymdhms = date("YmdHis");
    if ($f != "") {
        //========================================
        // このフォルダ配下の文書の最新更新日時を、このフォルダにセットする
        //========================================
        $seed = array($f);
        $max_ymdhms = "";
        while (true) {
            if (!count($seed)) break;
            $_f = array_shift($seed);
            if (!$_f) continue;
            // 最大日付を取得
            $sql = "select max(lib_up_date) from libinfo where lib_delete_flag = 'f' and folder_id = ".$_f;
            $lib_up_date = lib_get_one($con, $fname, $sql);
            if ($max_ymdhms < $lib_up_date) $max_ymdhms = $lib_up_date;
            // 子階層があれば追加
            $sql = "select child_id from libtree where parent_id = ".$_f;
            $sel = select_from_table($con, $sql, "", $fname);
            if ($sel == 0) libcom_goto_error_page_and_die($sql, $con);
            while ($row = pg_fetch_assoc($sel)) {
                if (in_array($row["child_id"], $seed)) continue;
                $seed[]= $row["child_id"];
            }
        }
        if (!$max_ymdhms) $max_ymdhms = $now_ymdhms;
        $sql = "update libfolder set upd_time = '".$max_ymdhms."' where folder_id = ".$f;
        $upd = update_set_table($con, $sql, array(), null, "", $fname);
        if ($upd == 0) libcom_goto_error_page_and_die($sql, $con);

        //========================================
        // 上位フォルダをさかのぼって、子フォルダの更新日時と直下ファイルのうちから、最新日付を取得して自分にセット
        //========================================
        $_f = $f;
        while (true) {
            // ひとつ上にさかのぼる。親がいなければ終了。親なしの親は、カテゴリである。
            $sql = "select parent_id from libtree where child_id = ".$_f;
            $_f = lib_get_one($con, $fname, $sql);
            if (!$_f) break;

            // 直下フォルダのうち、最新日付を取得
            $sql =
            " select max(upd_time) from libfolder".
            " inner join libtree on (libtree.parent_id = ".$_f." and libtree.child_id = libfolder.folder_id )";
            $ymdhms = trim(lib_get_one($con, $fname, $sql));

            // 直下ファイルのうち、最新日付を取得
            $sql =
            " select max(lib_up_date) from libinfo".
            " inner join libtree on (libtree.parent_id = ".$_f." and libtree.child_id = libinfo.folder_id )".
            " where lib_delete_flag = 'f'";
            $lib_up_date = lib_get_one($con, $fname, $sql);
            if ($ymdhms < $lib_up_date) $ymdhms = $lib_up_date;
            if (!$ymdhms) $ymdhms = $now_ymdhms;

            $sql = "update libfolder set upd_time = '".$ymdhms."' where folder_id = ".$_f;
            $upd = update_set_table($con, $sql, array(), null, "", $fname);
            if ($upd == 0) libcom_goto_error_page_and_die($sql, $con);
        }
    }

    //========================================
    // カテゴリ直下のフォルダの更新日時と直下ファイルのうちから、最新日付を取得して自分にセット
    //========================================
    // カテゴリ直下フォルダのうち、最新日付を取得
    $sql =
    " select max(upd_time) from libfolder".
    " left outer join libtree on ( libtree.child_id = libfolder.folder_id )".
    " where libtree.parent_id is null".
    " and lib_cate_id = ".(int)$c;
    $ymdhms = trim(lib_get_one($con, $fname, $sql));

    // カテゴリ直下ファイルのうち、最新日付を取得。直下ファイルはフォルダIDがnull
    $sql = "select max(lib_up_date) from libinfo where lib_cate_id = ".((int)$c)." and folder_id is null and lib_delete_flag = 'f'";
    $lib_up_date = lib_get_one($con, $fname, $sql);
    if ($ymdhms < $lib_up_date) $ymdhms = $lib_up_date;
    if (!$ymdhms) $ymdhms = $now_ymdhms;

    $sql = "update libcate set upd_time = '".$ymdhms."' where lib_cate_id = ".(int)$c;
    $upd = update_set_table($con, $sql, array(), null, "", $fname);
    if ($upd == 0) libcom_goto_error_page_and_die($sql, $con);
}




//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// フォルダの移動
// 成功時は"ok"を返す。失敗時はエラーメッセージを返す。
// content_type : document or folder or category
// content_value : 移動させるカテゴリ、フォルダ、文書のどれかのID
// dest_a dest_c dest_f は、格納先（つまり新しい親）
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
function lib_move_content($con, $fname, $emp_id, $content_type, $content_value, $dest_c, $dest_f) {

    if ($dest_c) {
        $dest_a = (int)lib_get_one($con, $fname, "select lib_archive from libcate where lib_cate_id = ".(int)$dest_c);
    } else if ($content_type=="folder") {
        $dest_a = (int)lib_get_one($con, $fname, "select lib_archive from libfolder where folder_id = ".(int)$content_value);
    }

    //==============================================================================
    // 「複数の親に従えている」という不整合データが無いかを確認。本番では原則起こらない。
    //==============================================================================
    $sql = "select * from (select count(child_id) as cnt, child_id from libtree group by child_id ) d where cnt > 1";
    $sel = select_from_table($con, $sql, "", $fname);
    if ($row = pg_fetch_assoc($sel)) {
        // 不整合データがあった。最初の１件を取得する
        $sql = "select * from libtree where child_id = ".$row["child_id"]." limit 1";
        $sel = select_from_table($con, $sql, "", $fname);
        $row2 = pg_fetch_assoc($sel);
        // 不整合データ１件を抹消してしまう
        $sql = "delete from libtree where child_id = ".$row2["child_id"]." and parent_id = ".$row2["parent_id"];
        $upd = update_set_table($con, $sql, array(), null, "", $fname);
        // 不整合データを１件削除したことを通知して強制終了
        return " deleted error data (libtree) parent_id:".$row2["parent_id"]." child_id:".$row2["child_id"];
    }

    //==============================================================================
    // 文書を移動させる場合
    // ※ 文書は共有に格納できない。（現行システム的に無理）
    // ※ 文書はリンクに格納できない。（できるけど面倒）
    // ※ ドラッグドロップで可能。カテゴリ間移動を行う場所は存在しない。
    //==============================================================================
    if ($content_type=="document") {
        $move_lib_id = $content_value;

        // 移動元情報
        $sel = select_from_table($con, "select lib_archive, lib_cate_id, folder_id from libinfo where lib_id = $move_lib_id", "", $fname);
        if ($sel == 0) return "DATABASE_SELECT_ERROR";
        $archive = pg_fetch_result($sel, 0, "lib_archive");
        $pre_category = pg_fetch_result($sel, 0, "lib_cate_id");
        $pre_folder_id = pg_fetch_result($sel, 0, "folder_id");

        // 文書情報を更新。カテゴリへ移動させる場合は、folder_idはnullにする
        $sql =
        " update libinfo set".
        " lib_archive = '".$dest_a."', lib_cate_id = ".$dest_c.", folder_id = ".($dest_f==""?"null":$dest_f).
        " where lib_id = ".$move_lib_id;
        $upd = update_set_table($con, $sql, array(), null, "", $fname);
        if ($upd == 0) return "DATABASE_UPDATE_ERROR";

        // フォルダ更新日時を更新
        lib_update_folder_modified($con, $pre_category, $pre_folder_id, $fname, $emp_id, $archive);
        lib_update_folder_modified($con, $dest_c, ($dest_f=="null"?"":$dest_f), $fname, $emp_id, $archive);

        // ファイル保存先ディレクトリがなければ作成
        if ($dest_a=="1") $dir_name = "private";
        if ($dest_a=="2") $dir_name = "all";
        if ($dest_a=="3") $dir_name = "section";
        if ($dest_a=="4") $dir_name = "project";
        if (!is_dir("docArchive/{$dir_name}/cate{$dest_c}")) {
            mkdir("docArchive/{$dir_name}/cate{$dest_c}", 0755);
        }

        // ファイルを移動
        if ($pre_category!=$dest_c) {
            exec("mv docArchive/{$dir_name}/cate{$pre_category}/document{$move_lib_id}.* docArchive/{$dir_name}/cate{$dest_c}");
        }
        return "ok";
    }





    //==============================================================================
    // フォルダを移動させる場合
    // ※ リンクはカテゴリ昇格させられない。（現行システム的に無理）
    // ※ フォルダまたはリンクを、リンクに格納できない。（できるけど面倒）
    // ※ ドラッグドロップと、管理画面での親変更で可能。
    //==============================================================================
    if ($content_type=="folder") {
        $move_folder_id = $content_value;

        // 移動させるフォルダの情報
        $sel = select_from_table($con, "select lib_archive, lib_cate_id from libfolder where folder_id = $move_folder_id", "", $fname);
        if ($sel == 0) return "DATABASE_SELECT_ERROR";
        $archive = pg_fetch_result($sel, 0, "lib_archive");
        $cur_cate_id = pg_fetch_result($sel, 0, "lib_cate_id");

        // 移動させるフォルダの親
        $cur_folder_id = select_from_table($con, "select parent_id from libtree where child_id = $move_folder_id", "", $fname);
        if ($sel == 0) return "DATABASE_SELECT_ERROR";

        //==============================================================================
        // フォルダの親子関連をすべて取得しておく
        //==============================================================================
        $parentsChildren = array();
        $childsParent = array();
        $move_files = array();
        $loopArray = array($move_folder_id);
        $sql = "select * from libtree";
        $sel = select_from_table($con, $sql, "", $fname);
        if ($sel == 0) return "DATABASE_SELECT_ERROR";
        while ($row = pg_fetch_assoc($sel)) {
            $parentsChildren[$row["parent_id"]][] = $row["child_id"];
            $childsParent[$row["child_id"]][] = $row["parent_id"];
        }

        //==============================================================================
        // 全ての子要素を抽出
        //==============================================================================
        $allMoveFolders = array();
        while (count($loopArray)) {
            $child_id = array_shift($loopArray); // 先頭からひとつ取り出す
            if (!$child_id) continue; // 念の為。
            if (in_array($child_id, $allMoveFolders)) return "循環参照エラー1：フォルダ".$child_id."／".implode(",", $allMoveFolders);
            $allMoveFolders[] = $child_id;
            if (is_array($parentsChildren[$child_id])) { // 取り出した子要素の子要素を収集
                foreach ($parentsChildren[$child_id] as $mago_id) {
                    $loopArray[]= $mago_id;
                }
            }
        }

        // 物理ファイル移動に備え、直属ファイルを取得
        // 子階層フォルダ、子階層ファイルのカテゴリをすべて変更
        foreach ($allMoveFolders as $child_id) {
            $sel = select_from_table($con, "select lib_archive, lib_cate_id, lib_id from libinfo where folder_id = ".$child_id, "", $fname);
            while($row = pg_fetch_assoc($sel)) {
                $key = $row["lib_archive"]."_".$row["lib_cate_id"]."_".$row["lib_id"];
                $move_files[$key] = $row; // 配列を物理ファイルユニークにしたいため、こういうキー名とする。（folder_idでユニークになる。その必要はなかった。）
            }
        }

        $isOk = 0;
        $new_cate_id = "";
        $ymdhms = date("YmdHis");
        $dest_cate_id = $dest_c; // 移動先は$dest_cである。
        for (;;) {
            //==============================================================================
            // もし現在どこかの親に割当中ならいったん削除し、フリー状態とする
            //==============================================================================
            $sql = "delete from libtree where child_id = ".$move_folder_id;
            $upd = update_set_table($con, $sql, array(), null, "", $fname);
            if ($upd == 0) break;

            //----------------------------------
            // フォルダをフォルダへ移動させる場合。あたらしい親子紐つけを作成
            //----------------------------------
            if ($dest_f) { // 移動先フォルダ
                $sql = "insert into libtree (parent_id, child_id) values (".$dest_f.", ".$move_folder_id.")"; // 新しい親子関係
                $upd = update_set_table($con, $sql, array(), null, "", $fname);
                if ($upd == 0) break;
            }
            //----------------------------------
            // フォルダをカテゴリ直下へ移動させる場合
            //----------------------------------
            else if ($dest_c) { // 移動先カテゴリ
            // 特に処理なし
            }
            //----------------------------------
            // フォルダをカテゴリへ昇格させる場合
            // リンクはカテゴリを参照できない。このフォルダがカテゴリに昇格してしまうとリンク切れを起こすので、更新不可とする。
            //----------------------------------
            else {
                $sql = "select count(*) from libfolder where link_id = ".$move_folder_id;
                $sel = select_from_table($con, $sql, "", $fname);
                if ($sel == 0) libcom_goto_error_page_and_die($sql);
                $cnt = pg_fetch_result($sel, 0, 0);
                if ($cnt) return "共有フォルダへの移動を行おうとしましたが、このフォルダへのリンクが存在します。\nリンクが行えなくなるため、被リンクフォルダは共有フォルダへの移動を行えません。";

                // カテゴリ作成のための採番
                $sel = select_from_table($con, "select max(lib_cate_id) from libcate", "", $fname);
                if ($sel == 0) break;
                $dest_cate_id = 1 + intval(pg_fetch_result($sel, 0, 0)); // 移動先は採番

                // カテゴリ情報をフォルダ情報から複製して登録
                $sql =
                " insert into libcate (".
                "     lib_archive, lib_cate_id, lib_link_id, lib_cate_nm, lib_cate_no".
                "   , reg_emp_id, upd_time, private_flg, ref_dept_st_flg, ref_dept_flg".
                "   , ref_st_flg, upd_dept_st_flg, upd_dept_flg, upd_st_flg".
                " ) select".
                " '".$dest_a."', ".$dest_cate_id.", reg_emp_id, folder_name, folder_no".
                " , reg_emp_id, '".$ymdhms."', private_flg, ref_dept_st_flg, ref_dept_flg".
                " , ref_st_flg, upd_dept_st_flg, upd_dept_flg, upd_st_flg".
                " from libfolder where folder_id = ".$move_folder_id;
                $upd = update_set_table($con, $sql, array(), null, "", $fname);
                if ($upd == 0) break;


                // フォルダ削除
                $sql = "delete from libfolder where folder_id = ".$move_folder_id;
                $upd = update_set_table($con, $sql, array(), null, "", $fname);
                if ($upd == 0) break;


                // もし自フォルダを親としているものがあれば連携を切る
                $sql = "delete from libtree where parent_id = ".$move_folder_id;
                $upd = update_set_table($con, $sql, array(), null, "", $fname);
                if ($upd == 0) break;


                // フォルダ直下のファイルの、フォルダIDはカラとなる。
                // ファイルについては、ここでカテゴリも変更しておく
                $sql =
                " update libinfo set".
                " lib_archive = '".$dest_a."', lib_cate_id = ".$dest_cate_id.", folder_id = null".
                " where folder_id = ".$move_folder_id;
                $upd = update_set_table($con, $sql, array(), null, "", $fname);
                if ($upd == 0) break;

                // 権限系６テーブルをフォルダからカテゴリへ
                $upd = update_set_table($con, "delete from libcaterefdept where lib_cate_id = ".$dest_cate_id, array(), null, "", $fname);
                if ($upd == 0) break;
                $sql =
                " insert into libcaterefdept (lib_cate_id, class_id, atrb_id, dept_id)".
                " select ".$dest_cate_id.", class_id, atrb_id, dept_id from libfolderrefdept".
                " where folder_id = ".$move_folder_id;
                $upd = update_set_table($con, $sql, array(), null, "", $fname);
                if ($upd == 0) break;
                $upd = update_set_table($con, "delete from libfolderrefdept where folder_id = ".$move_folder_id, array(), null, "", $fname);
                if ($upd == 0) break;

                $upd = update_set_table($con, "delete from libcateupddept where lib_cate_id = ".$dest_cate_id, array(), null, "", $fname);
                if ($upd == 0) break;
                $sql =
                " insert into libcateupddept (lib_cate_id, class_id, atrb_id, dept_id)".
                " select ".$dest_cate_id.", class_id, atrb_id, dept_id from libfolderupddept".
                " where folder_id = ".$move_folder_id;
                $upd = update_set_table($con, $sql, array(), null, "", $fname);
                if ($upd == 0) break;
                $upd = update_set_table($con, "delete from libfolderupddept where folder_id = ".$move_folder_id, array(), null, "", $fname);
                if ($upd == 0) break;

                $upd = update_set_table($con, "delete from libcaterefst where lib_cate_id = ".$dest_cate_id, array(), null, "", $fname);
                if ($upd == 0) break;
                $sql =
                " insert into libcaterefst (lib_cate_id, st_id)".
                " select ".$dest_cate_id.", st_id from libfolderrefst".
                " where folder_id = ".$move_folder_id;
                $upd = update_set_table($con, $sql, array(), null, "", $fname);
                if ($upd == 0) break;
                $upd = update_set_table($con, "delete from libfolderrefst where folder_id = ".$move_folder_id, array(), null, "", $fname);
                if ($upd == 0) break;

                $upd = update_set_table($con, "delete from libcateupdst where lib_cate_id = ".$dest_cate_id, array(), null, "", $fname);
                if ($upd == 0) break;
                $sql =
                " insert into libcateupdst (lib_cate_id, st_id)".
                " select ".$dest_cate_id.", st_id from libfolderupdst".
                " where folder_id = ".$move_folder_id;
                $upd = update_set_table($con, $sql, array(), null, "", $fname);
                if ($upd == 0) break;
                $upd = update_set_table($con, "delete from libfolderupdst where folder_id = ".$move_folder_id, array(), null, "", $fname);
                if ($upd == 0) break;

                $upd = update_set_table($con, "delete from libcaterefemp where lib_cate_id = ".$dest_cate_id, array(), null, "", $fname);
                if ($upd == 0) break;
                $sql =
                " insert into libcaterefemp (lib_cate_id, emp_id)".
                " select ".$dest_cate_id.", emp_id from libfolderrefemp".
                " where folder_id = ".$move_folder_id;
                $upd = update_set_table($con, $sql, array(), null, "", $fname);
                if ($upd == 0) break;
                $upd = update_set_table($con, "delete from libfolderrefemp where folder_id = ".$move_folder_id, array(), null, "", $fname);
                if ($upd == 0) break;

                $upd = update_set_table($con, "delete from libcateupdemp where lib_cate_id = ".$dest_cate_id, array(), null, "", $fname);
                if ($upd == 0) break;
                $sql =
                " insert into libcateupdemp (lib_cate_id, emp_id)".
                " select ".$dest_cate_id.", emp_id from libfolderupdemp".
                " where folder_id = ".$move_folder_id;
                $upd = update_set_table($con, $sql, array(), null, "", $fname);
                if ($upd == 0) break;
                $upd = update_set_table($con, "delete from libfolderupdemp where folder_id = ".$move_folder_id, array(), null, "", $fname);
                if ($upd == 0) break;
            }


            //==============================================================================
            // カテゴリ移動またはカテゴリ昇格が発生した場合
            //==============================================================================
            if ($dest_cate_id!=$cur_cate_id) {

                // 子階層フォルダ、子階層ファイルのカテゴリをすべて変更
                foreach ($allMoveFolders as $child_id) {

                    // このフォルダのカテゴリ変更
                    $sql =
                    " update libfolder set".
                    " lib_archive = '".$dest_a."', lib_cate_id = ".$dest_cate_id.
                    " where folder_id = ".$child_id;
                    $upd = update_set_table($con, $sql, array(), null, "", $fname);
                    if ($upd == 0) break;

                    // フォルダ直属ファイルのカテゴリ変更
                    // 移動フォルダの直属ファイルは、フォルダIDが既にnull更新されているため該当しないだろう。
                    $sql =
                    " update libinfo set".
                    " lib_archive = '".$dest_a."', lib_cate_id = ".$dest_cate_id.
                    " where folder_id = ".$child_id;
                    $upd = update_set_table($con, $sql, array(), null, "", $fname);
                    if ($upd == 0) break;
                }
            }
            $isOk = 1;
            break;
        }
        if (!$isOk) return "DATABASE_UPDATE_ERROR";

        // フォルダ更新日時を更新
        lib_update_folder_modified($con, $cur_cate_id, $src_parent_folder_id, $fname, $emp_id, $archive);
        lib_update_folder_modified($con, $dest_cate_id, $move_folder_id, $fname, $emp_id, $archive);

        // ファイル保存先ディレクトリがなければ作成
        $dest_dir = "";
        if ($dest_a=="1") $dest_dir = "private";
        if ($dest_a=="2") $dest_dir = "all";
        if ($dest_a=="3") $dest_dir = "section";
        if ($dest_a=="4") $dest_dir = "project";

        $dest_path = "docArchive/".$dest_dir."/cate".$dest_cate_id;
        if (!is_dir($dest_path)) mkdir($dest_path, 0755);

        //==============================================================================
        // 物理ファイルを移動
        // カテゴリ移動またはカテゴリ昇格が発生した場合
        //==============================================================================
        if ($dest_cate_id!=$cur_cate_id) {
            foreach ($move_files as $key => $row) {
                $src_dir = "";
                if ($archive=="1") $src_dir = "private";
                if ($archive=="2") $src_dir = "all";
                if ($archive=="3") $src_dir = "section";
                if ($archive=="4") $src_dir = "project";

                // ファイルを移動
                exec("mv docArchive/".$src_dir."/cate".$row["lib_cate_id"]."/document".$row["lib_id"].".* ".$dest_path);
            }
        }
        return "ok";
    }




    //==============================================================================
    // カテゴリを移動させる場合（つまりフォルダに降格する）
    // ※ カテゴリは、リンク下に格納できない。（できるけど面倒）
    //==============================================================================
    if ($content_type=="category") {
        $move_cate_id = $content_value;

        // 移動させるカテゴリの情報
        $sel = select_from_table($con, "select lib_archive, lib_cate_id from libcate where lib_cate_id = $move_cate_id", "", $fname);
        if ($sel == 0) return "DATABASE_SELECT_ERROR";
        $archive = pg_fetch_result($sel, 0, "lib_archive");
        $cur_cate_id = pg_fetch_result($sel, 0, "lib_cate_id");

        //==============================================================================
        // 物理ファイルの移動準備。カテゴリ所属のファイルを、値を変更する前にすべて取得しておく
        //==============================================================================
        $sel = select_from_table($con, "select * from libinfo where lib_cate_id = $move_cate_id", "", $fname); // ファイル
        $childrenFile = pg_fetch_all($sel);

        $isOk = 0;
        $ymdhms = date("YmdHis");
        $move_files = array();
        for (;;) {
            //----------------------------------
            // フォルダIDを採番する
            //----------------------------------
            $sel = select_from_table($con, "select max(folder_id) from libfolder", "", $fname);
            if ($sel == 0) break;
            $new_folder_id = 1 + intval(pg_fetch_result($sel, 0, 0));

            //----------------------------------
            // これまでカテゴリ直下であったフォルダは、新しいフォルダとの親子関係を作成する。
            //----------------------------------
            $sql = "select folder_id from libfolder where folder_id not in (select child_id from libtree) and lib_cate_id = ".$move_cate_id;
            $sel = select_from_table($con, $sql, "", $fname);
            while ($row = pg_fetch_assoc($sel)) {
                $sql = "insert into libtree (parent_id, child_id) values (".$new_folder_id.", ".$row["folder_id"].")"; // 新しい親子関係
                $upd = update_set_table($con, $sql, array(), null, "", $fname);
                if ($upd == 0) break;
            }

            //----------------------------------
            // フォルダ情報をカテゴリ情報から複製して登録
            //----------------------------------
            $sql =
            " insert into libfolder (".
            "     folder_id".
            "   , lib_archive, lib_cate_id, folder_name, folder_no".
            "   , reg_emp_id, upd_time, private_flg, ref_dept_st_flg, ref_dept_flg".
            "   , ref_st_flg, upd_dept_st_flg, upd_dept_flg, upd_st_flg".
            " ) select".
            " ".$new_folder_id.
            " , '".$dest_a."', ".$dest_c.", lib_cate_nm, lib_cate_no".
            " , reg_emp_id, '".$ymdhms."', private_flg, ref_dept_st_flg, ref_dept_flg".
            " , ref_st_flg, upd_dept_st_flg, upd_dept_flg, upd_st_flg".
            " from libcate where lib_cate_id = ".$move_cate_id;
            $upd = update_set_table($con, $sql, array(), null, "", $fname);
            if ($upd == 0) break;

            //----------------------------------
            // カテゴリ削除
            //----------------------------------
            $sql = "delete from libcate where lib_cate_id = ".$move_cate_id;
            $upd = update_set_table($con, $sql, array(), null, "", $fname);
            if ($upd == 0) break;

            //----------------------------------
            // フォルダへ格納する場合は、フォルダとの親子紐つけを作成
            //----------------------------------
            if ($dest_f) { // 移動先フォルダ
                $sql = "insert into libtree (parent_id, child_id) values (".$dest_f.", ".$new_folder_id.")"; // 新しい親子関係
                $upd = update_set_table($con, $sql, array(), null, "", $fname);
                if ($upd == 0) break;
            }

            //----------------------------------
            // 別のカテゴリ直下へ移動させる場合
            //----------------------------------
            else if ($dest_c) { // 移動先カテゴリ
            // 特に処理なし
            }

            //----------------------------------
            // これまでカテゴリ直下であったファイルは、新しいフォルダの直下となる。
            //----------------------------------
            $sql = "update libinfo set folder_id = ".$new_folder_id." where folder_id is null and lib_cate_id = ".$move_cate_id;
            $upd = update_set_table($con, $sql, array(), null, "", $fname);
            if ($upd == 0) break;

            //----------------------------------
            // カテゴリ配下のものは、すべて、カテゴリを新しいものに書き換える
            //----------------------------------
            $sql =
            " update libfolder set".
            " lib_archive = '".$dest_a."', lib_cate_id = ".$dest_c.
            " where lib_cate_id = ".$move_cate_id;
            $upd = update_set_table($con, $sql, array(), null, "", $fname);
            if ($upd == 0) break;

            $sql =
            " update libinfo set".
            " lib_archive = '".$dest_a."', lib_cate_id = ".$dest_c.
            " where lib_cate_id = ".$move_cate_id;
            $upd = update_set_table($con, $sql, array(), null, "", $fname);
            if ($upd == 0) break;

            // 権限系６テーブルをカテゴリからフォルダへ
            $upd = update_set_table($con, "delete from libfolderrefdept where folder_id = ".$new_folder_id, array(), null, "", $fname);
            if ($upd == 0) break;
            $sql =
            " insert into libfolderrefdept (folder_id, class_id, atrb_id, dept_id)".
            " select ".$new_folder_id.", class_id, atrb_id, dept_id from libcaterefdept".
            " where lib_cate_id = ".$move_cate_id;
            $upd = update_set_table($con, $sql, array(), null, "", $fname);
            if ($upd == 0) break;
            $upd = update_set_table($con, "delete from libcaterefdept where lib_cate_id = ".$move_cate_id, array(), null, "", $fname);
            if ($upd == 0) break;

            $upd = update_set_table($con, "delete from libfolderupddept where folder_id = ".$new_folder_id, array(), null, "", $fname);
            if ($upd == 0) break;
            $sql =
            " insert into libfolderupddept (folder_id, class_id, atrb_id, dept_id)".
            " select ".$new_folder_id.", class_id, atrb_id, dept_id from libcateupddept".
            " where lib_cate_id = ".$move_cate_id;
            $upd = update_set_table($con, $sql, array(), null, "", $fname);
            if ($upd == 0) break;
            $upd = update_set_table($con, "delete from libcateupddept where lib_cate_id = ".$move_cate_id, array(), null, "", $fname);
            if ($upd == 0) break;

            $upd = update_set_table($con, "delete from libfolderrefst where folder_id = ".$new_folder_id, array(), null, "", $fname);
            if ($upd == 0) break;
            $sql =
            " insert into libfolderrefst (folder_id, st_id)".
            " select ".$new_folder_id.", st_id from libcaterefst".
            " where lib_cate_id = ".$move_cate_id;
            $upd = update_set_table($con, $sql, array(), null, "", $fname);
            if ($upd == 0) break;
            $upd = update_set_table($con, "delete from libcaterefst where lib_cate_id = ".$move_cate_id, array(), null, "", $fname);
            if ($upd == 0) break;

            $upd = update_set_table($con, "delete from libfolderupdst where folder_id = ".$new_folder_id, array(), null, "", $fname);
            if ($upd == 0) break;
            $sql =
            " insert into libfolderupdst (folder_id, st_id)".
            " select ".$new_folder_id.", st_id from libcateupdst".
            " where lib_cate_id = ".$move_cate_id;
            $upd = update_set_table($con, $sql, array(), null, "", $fname);
            if ($upd == 0) break;
            $upd = update_set_table($con, "delete from libcateupdst where lib_cate_id = ".$move_cate_id, array(), null, "", $fname);
            if ($upd == 0) break;

            $upd = update_set_table($con, "delete from libfolderrefemp where folder_id = ".$new_folder_id, array(), null, "", $fname);
            if ($upd == 0) break;
            $sql =
            " insert into libfolderrefemp (folder_id, emp_id)".
            " select ".$new_folder_id.", emp_id from libcaterefemp".
            " where lib_cate_id = ".$move_cate_id;
            $upd = update_set_table($con, $sql, array(), null, "", $fname);
            if ($upd == 0) break;
            $upd = update_set_table($con, "delete from libcaterefemp where lib_cate_id = ".$move_cate_id, array(), null, "", $fname);
            if ($upd == 0) break;

            $upd = update_set_table($con, "delete from libfolderupdemp where folder_id = ".$new_folder_id, array(), null, "", $fname);
            if ($upd == 0) break;
            $sql =
            " insert into libfolderupdemp (folder_id, emp_id)".
            " select ".$new_folder_id.", emp_id from libcateupdemp".
            " where lib_cate_id = ".$move_cate_id;
            $upd = update_set_table($con, $sql, array(), null, "", $fname);
            if ($upd == 0) break;
            $upd = update_set_table($con, "delete from libcateupdemp where lib_cate_id = ".$move_cate_id, array(), null, "", $fname);
            if ($upd == 0) break;

            $isOk = 1;
            break;
        }
        if (!$isOk) echo "DATABASE_UPDATE_ERROR";

        // フォルダ更新日時を更新
        lib_update_folder_modified($con, $dest_c, $dest_f, $fname, $emp_id, $archive);

        // ファイル保存先ディレクトリがなければ作成
        $dest_dir = "";
        if ($dest_a=="1") $dest_dir = "private";
        if ($dest_a=="2") $dest_dir = "all";
        if ($dest_a=="3") $dest_dir = "section";
        if ($dest_a=="4") $dest_dir = "project";

        $dest_path = "docArchive/".$dest_dir."/cate".$dest_c;
        if (!is_dir($dest_path)) mkdir($dest_path, 0755);

        //==============================================================================
        // 物理ファイルを移動
        //==============================================================================
        if (is_array($childrenFile)) {
            foreach ($childrenFile as $row) {
                $src_dir = "";
                if ($archive=="1") $src_dir = "private";
                if ($archive=="2") $src_dir = "all";
                if ($archive=="3") $src_dir = "section";
                if ($archive=="4") $src_dir = "project";

                // ファイルを移動
                exec("mv docArchive/".$src_dir."/cate".$row["lib_cate_id"]."/document".$row["lib_id"].".* ".$dest_path);
            }
        }
        return "ok";
    }
    return "不正な関数呼び出し：lib_move_content";
}



function create_table_ukey() {
    global $session;
    return substr($session,0,3).substr(strrev(uniqid("")), 0, 10);
}



// **********************************************************************************
// empmst + concurrentを取得しておく
// **********************************************************************************
function lib_cre_tmp_concurrent_table($con, $fname, $emp_id, $class_id="", $atrb_id="") {
    global $table_ukey;
    if (!$table_ukey) $table_ukey = create_table_ukey();

    $tbl = lib_get_one($con, $fname, "select relname from pg_class where relkind = 'r' and relname = 'tmp_concurrent".$table_ukey."'");
    if (!$tbl) {
        $sql =
        " create temporary table tmp_concurrent".$table_ukey." (".
        " t_class_id varchar, t_atrb_id varchar, t_dept_id varchar, t_st_id varchar".
        ",n_class_id smallint, n_atrb_id smallint, n_dept_id smallint, n_st_id smallint);";
    }
    else {
        $sql = "delete from tmp_concurrent".$table_ukey;
    }
    $upd = update_set_table($con, $sql, array(), array(), "", $fname);
    if (!$upd) libcom_goto_error_page_and_die($sql, $con);

    $sql =
    " insert into tmp_concurrent".$table_ukey.
    " select".
    " to_char(emp_class,'FM999999999999'), to_char(emp_attribute,'FM999999999999'), to_char(emp_dept,'FM999999999999'), to_char(emp_st,'FM999999999999')".
    ",emp_class, emp_attribute, emp_dept, emp_st from empmst where emp_id = '".$emp_id."';";
    $upd = update_set_table($con, $sql, array(), array(), "", $fname);
    if (!$upd) libcom_goto_error_page_and_die($sql, $con);

    $sql =
    " insert into tmp_concurrent".$table_ukey.
    " select".
    " to_char(emp_class,'FM999999999999'), to_char(emp_attribute,'FM999999999999'), to_char(emp_dept,'FM999999999999'), to_char(emp_st,'FM999999999999')".
    ",emp_class, emp_attribute, emp_dept, emp_st from concurrent where emp_id = '".$emp_id."';";
    $upd = update_set_table($con, $sql, array(), array(), "", $fname);
    if (!$upd) libcom_goto_error_page_and_die($sql, $con);

    // 参照可能なプロジェクト・ワークグループ
    $tbl = lib_get_one($con, $fname, "select relname from pg_class where relkind = 'r' and relname = 'tmp_project".$table_ukey."'");
    if (!$tbl) {
        $sql = "create temporary table tmp_project".$table_ukey." ( pjt_id varchar, class_id smallint, atrb_id smallint, pjt_or_wkgp varchar, is_users varchar, pjt_public_flag boolean );";
    } else {
        $sql = " delete from tmp_project".$table_ukey;
    }
    $upd = update_set_table($con, $sql, array(), array(), "", $fname);
    if (!$upd) libcom_goto_error_page_and_die($sql, $con);

    // 委員会か、ワークグループ(クラス指定時)か（wg.pjt_parent_id is not null and and pjt.pjt_public_flag = 't'）
    // ユーザの管理対象(更新可能)か、そうでないか（pjt_responseおよびpromember.emp_id）
    // 委員会メンバーのみ参照可能なのか、そうでないのか(libcate.provate_flg）

    // class/atrbが指定されていない場合は、ワークグループを含めない...らしい
    $sql = " delete from tmp_project".$table_ukey;
    $upd = update_set_table($con, $sql, array(), array(), "", $fname);
    if (!$upd) libcom_goto_error_page_and_die($sql, $con);

    // ★
    $sql = array(
        " insert into tmp_project".$table_ukey,

        // 委員会
        " select to_char(pj.pjt_id,'FM999999999999'), pj.class_id, pj.atrb_id, 'pjt'",
        ",case when",
        "     pj.pjt_response = '".$emp_id."'", // ◆責任者
        "     or exists ( select * from promember where promember.emp_id = '".$emp_id."' and promember.pjt_id = pj.pjt_id )", //担当メンバー
        " then 1 else 0 end",
        ",pj.pjt_public_flag".
        " from project pj",
        " where pj.pjt_delete_flag = 'f'",
        " and pj.pjt_parent_id is null",

        " union",

        // ワークグループ。所属は親委員会のもの。
        // ワークグループは、なぜか親がパブリックのもののみを抽出する。
        " select to_char(wg.pjt_id,'FM999999999999'), pj.class_id, pj.atrb_id, 'wkgp'",
        ",case when",
        "     wg.pjt_response = '".$emp_id."'", // ◆責任者
        "     or exists ( select * from promember where promember.emp_id = '".$emp_id."' and promember.pjt_id = wg.pjt_id )", //担当メンバー
        " then 1 else 0 end as b",
        ",pj.pjt_public_flag".
        " from project wg",
        " inner join project pj on ( pj.pjt_delete_flag = 'f' and pj.pjt_id = wg.pjt_parent_id )",
        " where wg.pjt_delete_flag = 'f'",
        " and wg.pjt_parent_id is not null"
    );
    $upd = update_set_table($con, implode("\n", $sql), array(), array(), "", $fname);
    if (!$upd) libcom_goto_error_page_and_die(implode("\n", $sql), $con);
}




//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
// libcate カテゴリの権限つきSELECT文
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
function lib_get_libcate_sql($emp_id, $lock_show_flg, $lock_show_all_flg, $a, $c, $class_id, $atrb_id, $opt) {
    if ($lock_show_flg=="f") $lock_show_flg = "";
    if ($lock_show_all_flg=="f") $lock_show_all_flg = "";
    global $table_ukey;
    if (!$table_ukey) {
        echo "table_ukeyが未設定（lib_cre_tmp_concurrent_table未実行）<br>";
        lib_debug_backtrace();
        die;
    }
    if (!$a && !$opt["lib_upd_date"] && !$opt["show_mypage_flg"]) {
        echo "最新文書取得でなければ書庫指定は必須<br>";
        lib_debug_backtrace();
        die;
    }

    // **********************************************************************************
    // 更新権限の確認
    // **********************************************************************************
    $upd_or_joken = array();

    if (!$opt["is_login"] && !$opt["is_admin"]) {
        // 個人書庫の場合は、個人フォルダなら即OK
        if ($a=="1" || !$a) {
            $upd_or_joken[]= "( lc.lib_archive = '1' and lc.lib_link_id = '".$emp_id."' )";
        }

        // ★
        // 委員会の更新可能対象
        if ($a=="4" || !$a) {
            $sql = array();
            $sql[]= "(";
            $sql[]= "    lc.lib_archive = '4'"; // 委員会でなければ無視

            if (!$opt["is_ignore_class_atrb"]) { // 委員会・WGの管理画面から呼ばれた場合。WGなどが画面選択可能な時点で、class/atrbは確認不要なのである。
            	if (!$opt["is_mypage"] && !$opt["for_new_list"]) {
	                if ($class_id)  $sql[]= "    and pj.class_id = ".$class_id; // class_idプルダウンが指定されていれば、マッチすること
	                if (!$class_id) $sql[]= "    and pj.class_id is null"; // class_idプルダウンが指定されていれば、マッチすること
	                if ($atrb_id)  $sql[]= "    and pj.atrb_id = ".$atrb_id; // atrb_idプルダウンが指定されていれば、マッチすること
	                if (!$atrb_id) $sql[]= "    and pj.atrb_id is null"; // atrb_idプルダウンが指定されていれば、マッチすること
                }
                $sql[]= "        and pj.is_users = '1'"; // 自分の委員会ならOK
                if (!$class_id) {
                    $sql[]= "        and pj.pjt_or_wkgp = 'pjt'"; // ワークグループではない
                }
            }
            $sql[]= ")";
            $upd_or_joken[]= implode("\n", $sql);
        }

        // 全体書庫、部署書庫、委員会書庫。更新権限に自分の名前が直接指定されていればOK
        if ($a=="2" || $a=="3" || $a=="4" || !$a) {
            $upd_or_joken[]= "(lc.lib_archive <> '1' and libcateupdemp.lib_cate_id is not null)";
        }

        // 部署書庫。更新権限が「すべてすべて」のとき、自分の部署であればOKという暗黙仕様
        if ($a=="3" || !$a) {
            $upd_or_joken[]= "( my_upd_busyo.t_class_id is not null )";
        }

        // 全体書庫、部署書庫、委員会書庫。部署と役職指定による判定
        if ($a=="2" || $a=="3" || !$a) {
            $upd_or_joken[]=
                "(".
                "    (lc.lib_archive = '2' or lc.lib_archive = '3')". // 個人書庫と委員会は除く
                "    and lc.upd_dept_st_flg = 'f'". // 「許可しない」チェックがオフ
                // 前段チェック「部署書庫の暗黙仕様」に該当「しない」こと
                "    and not (".
                "        lc.lib_archive = '3' and lc.upd_dept_flg <> '2' and lc.upd_st_flg <> '2'". // 部署書庫で、更新権限「すべてすべて」
                "    )".
                "    and exists (".
                "        select * from tmp_concurrent".$table_ukey." tc".
                "        where ( lc.upd_st_flg <> '2' or tc.n_st_id = libcateupdst.st_id )".
                "        and   ( lc.upd_dept_flg <> '2' or (tc.n_class_id = libcateupddept.class_id and tc.n_atrb_id = libcateupddept.atrb_id and tc.n_dept_id = libcateupddept.dept_id ))".
                "    )".
//              "    and ( lc.upd_st_flg <> '2' or libcateupdst.lib_cate_id is not null )". // 役職が「すべて」または、職員の役職とマッチ
//              "    and ( lc.upd_dept_flg <> '2' or libcateupddept.lib_cate_id is not null )".// 部署が「すべて」または、職員の部署とマッチ
                ")";
        }
    }


    // **********************************************************************************
    // 主セレクトの実行
    // **********************************************************************************
    $sql =array();
    $sql[]= " select";
    $sql[]= " lc.lib_archive";
    $sql[]= ",lc.lib_cate_id";
    $sql[]= ",lc.lib_link_id";
    $sql[]= ",lc.reg_emp_id";
    $sql[]= ",lc.upd_time";
    // 管理者で個人書庫なら、カテゴリ名は職員名を冠する
    if ($opt["is_admin"]) {
        $sql[]= ",case";
        $sql[]= "    when lc.lib_archive = '1' then em.emp_lt_nm || ' ' || em.emp_ft_nm || ' - ' || lc.lib_cate_nm";
        $sql[]= "    else lc.lib_cate_nm";
        $sql[]= " end as lib_cate_nm";
    } else {
        $sql[]= ",lc.lib_cate_nm";
    }
    $sql[]= ",lc.lib_cate_no";

    if ($opt["is_admin"]) $sql[]= ", 1 as writable";
    else if ($opt["is_login"] || !count($upd_or_joken)) $sql[]= ",0 as writable";
    else {
        $sql[]= ", max ( case";
        foreach ($upd_or_joken as $joken) {
            $sql[]= " when ".$joken." then 1";
        }
        $sql[]= " else 0 end ) as writable";
    }

    $sql[]= "from libcate lc";
    $sql[]= "left outer join empmst em on (em.emp_id = lc.lib_link_id and lib_archive = '1')";

    // ★ 委員会のJOIN
    if ($a=="4" || !$a) {
        if ($opt["is_admin"] || $opt["is_mypage"]) {
            $sql[]= "left outer join tmp_project".$table_ukey." pj on (lc.lib_archive = '4' and pj.pjt_id = lc.lib_link_id)";
        } else {
            $sql[]= "left outer join tmp_project".$table_ukey." pj on (lc.lib_archive = '4' and pj.pjt_id = lc.lib_link_id and (pj.pjt_public_flag = 't' or pj.pjt_or_wkgp = 'pjt'))";
        }
    }



    if (!$opt["is_admin"] && !$opt["is_login"] && count($upd_or_joken)) {

        if ($a=="2" || $a=="3" || $a=="4" || !$a) {
            $sql[]= "left outer join libcateupdemp on (";
            $sql[]= "    lc.lib_archive <> '1'"; // 個人書庫は除く
            $sql[]= "    and libcateupdemp.emp_id = '".$emp_id."'"; // 職員自身が含まれる
            $sql[]= "    and libcateupdemp.lib_cate_id = lc.lib_cate_id"; // フォルダIDが存在
            $sql[]= ")";
        }

        if ($a=="3" || !$a) {
            $sql[]= "left outer join (";
            $sql[]= "    select t_class_id from tmp_concurrent".$table_ukey." group by t_class_id";
            $sql[]= ") my_upd_busyo on ("; // 部署書庫で更新権限「すべてすべて」は、部署オンリーを意味する
            $sql[]= "    lc.lib_archive = '3'"; // 部署書庫である
            $sql[]= "    and lc.upd_dept_st_flg = 'f'"; // 「許可しない」チェックがオフ
            $sql[]= "    and lc.upd_dept_flg <> '2'"; // 部署が「すべて」である
            $sql[]= "    and lc.upd_st_flg <> '2'"; // 役職が「すべて」である
            $sql[]= "    and my_upd_busyo.t_class_id = lc.lib_link_id";
            $sql[]= ")";
        }

        if ($a=="2" || $a=="3" || !$a) {
            $sql[]= "left outer join libcateupdst on (";
            $sql[]= "    (lc.lib_archive = '2' or lc.lib_archive = '3')"; // 個人書庫と委員会は除く
            $sql[]= "    and lc.upd_dept_st_flg = 'f'"; // 「許可しない」チェックがオフ
            $sql[]= "    and lc.upd_st_flg = '2'"; // 役職が「指定する」である
            $sql[]= "    and libcateupdemp.lib_cate_id is null"; // 職員IDではマッチしなかった
            $sql[]= "    and libcateupdst.lib_cate_id = lc.lib_cate_id"; // フォルダIDが存在
            $sql[]= "    and exists ("; // 職員の役職である
            $sql[]= "        select n_st_id from tmp_concurrent".$table_ukey;
            $sql[]= "        where  tmp_concurrent".$table_ukey.".n_st_id = libcateupdst.st_id";
            $sql[]= "    )";
            $sql[]= ")";
        }

        if ($a=="2" || $a=="3" || !$a) {
            $sql[]= "left outer join libcateupddept on (";
            $sql[]= "    (lc.lib_archive = '2' or lc.lib_archive = '3')"; // 個人書庫と委員会は除く
            $sql[]= "    and lc.upd_dept_st_flg = 'f'"; // 「許可しない」チェックがオフ
            $sql[]= "    and lc.upd_dept_flg = '2'"; // 部署が「指定する」である
            $sql[]= "    and libcateupdemp.lib_cate_id is null"; // 職員IDではマッチしなかった
            //$sql[]= "    and libcateupdst.lib_cate_id is null"; // 役職でもマッチしなかった
            $sql[]= "    and libcateupddept.lib_cate_id = lc.lib_cate_id"; // フォルダIDが存在
            $sql[]= "    and exists ("; // 職員の所属である
            $sql[]= "        select n_class_id, n_atrb_id, n_dept_id from tmp_concurrent".$table_ukey;
            $sql[]= "        where  tmp_concurrent".$table_ukey.".n_class_id = libcateupddept.class_id";
            $sql[]= "        and tmp_concurrent".$table_ukey.".n_atrb_id = libcateupddept.atrb_id";
            $sql[]= "        and tmp_concurrent".$table_ukey.".n_dept_id = libcateupddept.dept_id";
            $sql[]= "    )";
            $sql[]= ")";
        }

        $sql[]= "left outer join libcaterefemp on (";
        $sql[]= "   libcaterefemp.emp_id = '".$emp_id."'";
        $sql[]= "   and libcaterefemp.lib_cate_id = lc.lib_cate_id";
        $sql[]= ")";

        if ($a=="2" || $a=="3" || $a=="4" || !$a) {
            $sql[]= "left outer join libcaterefst on (";
            $sql[]= "    (";
            $sql[]= "        lc.lib_archive = '2'";
            $sql[]= "        or lc.lib_archive = '3'";
            $sql[]= "        or (lc.lib_archive = '4' and lc.private_flg <> 't')"; // 委員会は「委員会のみ参照」でなければ、部署役職参照確認実施。
            $sql[]= "    )";
            $sql[]= "    and lc.ref_dept_st_flg = 'f'"; // 「許可しない」チェックがオフ
            $sql[]= "    and lc.ref_st_flg = '2'"; // 役職が「指定する」である
            $sql[]= "    and libcaterefemp.lib_cate_id is null"; // 職員IDではマッチしなかった
            $sql[]= "    and libcaterefst.lib_cate_id = lc.lib_cate_id"; // カテゴリIDが存在
            $sql[]= "    and exists ("; // 職員の役職である
            $sql[]= "        select n_st_id from tmp_concurrent".$table_ukey;
            $sql[]= "        where  tmp_concurrent".$table_ukey.".n_st_id = libcaterefst.st_id";
            $sql[]= "    )";
            $sql[]= ")";
        }

        if ($a=="2" || $a=="3" || $a=="4" || !$a) {
            $sql[]= "left outer join libcaterefdept on (";
            $sql[]= "    (";
            $sql[]= "        lc.lib_archive = '2'";
            $sql[]= "        or lc.lib_archive = '3'";
            $sql[]= "        or (lc.lib_archive = '4' and lc.private_flg <> 't')"; // 委員会は「委員会のみ参照」でなければ、部署役職参照確認実施。
            $sql[]= "    )";
            $sql[]= "    and lc.ref_dept_st_flg = 'f'"; // 「許可しない」チェックがオフ
            $sql[]= "    and lc.ref_dept_flg = '2'"; // 部署が「指定する」である
            $sql[]= "    and libcaterefemp.lib_cate_id is null"; // 職員IDではマッチしなかった
            //$sql[]= "    and libcaterefst.lib_cate_id is null"; // 役職でもマッチしなかった
            $sql[]= "    and libcaterefdept.lib_cate_id = lc.lib_cate_id"; // フォルダIDが存在
            $sql[]= "    and exists ("; // 職員の所属である
            $sql[]= "        select n_class_id, n_atrb_id, n_dept_id from tmp_concurrent".$table_ukey;
            $sql[]= "        where  tmp_concurrent".$table_ukey.".n_class_id = libcaterefdept.class_id";
            $sql[]= "        and tmp_concurrent".$table_ukey.".n_atrb_id = libcaterefdept.atrb_id";
            $sql[]= "        and tmp_concurrent".$table_ukey.".n_dept_id = libcaterefdept.dept_id";
            $sql[]= "    )";
            $sql[]= ")";
        }
    }

    $sql[]= "where lc.libcate_del_flg = 'f'"; // 削除済でないこと

    // ******************************************************************************************
    // AND系分岐（＝必須条件）
    // ******************************************************************************************

    // 書庫を指定されたら、その書庫であること
    if ($a) {
        $sql[]= "and lc.lib_archive = '".$a."'";
    }
    // カテゴリを指定されたら、ずばりそのカテゴリであること
    if ($c) {
        $sql[]= "and lc.lib_cate_id = ".$c;
    }
    // 部署のカテゴリを取得する際は、部署マスタにエントリが存在すること
    if ($a=="3" || !$a) {
        $sql[]= "and (";
        $sql[]= "    lc.lib_archive <> '3'";
        $sql[]= "    or (";
        $sql[]= "        lc.lib_archive = '3'";
        $sql[]= "        and exists (";
        $sql[]= "            select class_id from classmst";
        $sql[]= "            where class_del_flg = 'f'";
        $sql[]= "            and to_char(classmst.class_id,'FM999999999999') = lc.lib_link_id";
        $sql[]= "        )";
        $sql[]= "    )";
        $sql[]= ")";
    }

    // ★ 除外する委員会
    // 委員会カテゴリなら、委員会に紐つくこと
    // projectマスタに委員会かワークグループとしてエントリが存在すること
    // さらに、画面から所属指定されたら、委員会の子（ワークグループ）も含めること
    // 所属階層１のみ指定されたら、所属階層２がnullであるものを取得すること
    if ($a=="4" || !$a) {
        $sql[]= "and (";
        $sql[]= "    lc.lib_archive <> '4'";
        $sql[]= "    or";
        $sql[]= "    (";
        $sql[]= "        lc.lib_archive = '4'";
        $sql[]= "        and pj.pjt_id is not null";

        if ($a=="4" && !$opt["is_mypage"]) {
//          $sql[]= "and (";
//          $sql[]= "        4' and lc.private_flg <> 't')"; // 委員会は「委員会のみ参照」でなければ、部署役職参照確認実施。
//          $sql[]= "        and lc.ref_dept_st_flg = 'f'"; // 「許可しない」チェックがオフ
//          $sql[]= "        and ( lc.ref_st_flg <> '2' or libcaterefst.lib_cate_id is not null )"; // 役職が「すべて」または、職員の役職とマッチ
//          $sql[]= "        and ( lc.ref_dept_flg <> '2' or libcaterefdept.lib_cate_id is not null )";// 部署が「すべて」または、職員の部署とマッチ
            if ($class_id) $sql[]= "    and pj.class_id = ".$class_id; // class_idプルダウンが指定されていれば、マッチすること
            if (!$class_id) $sql[]= "   and pj.class_id is null"; // class_idプルダウンが指定されていれば、マッチすること
            if ($atrb_id)  $sql[]= "    and pj.atrb_id = ".$atrb_id; // atrb_idプルダウンが指定されていれば、マッチすること
            if (!$atrb_id) $sql[]= "    and pj.atrb_id is null"; // atrb_idプルダウンが指定されていれば、マッチすること
//          $sql[]= ")";
        }

        $sql[]= "    )";
        $sql[]= ")";
    }




    // ログイン画面の場合は、全体共有設定画面のshow_login_flgがオンでなければ、「すべてすべて」のみが表示されるように。
    // 共有については、show_login_flgがtrueであれば、全部表示
    if ($opt["is_login"]) {
        $sql[]= "and (";
        $sql[]= "    ("; // ↓設定画面で「すべて表示」を設定したなら、特に条件不要で全表示してよい
        $sql[]= "        exists (select libconfig.show_login_flg from libconfig where libconfig.show_login_flg = 't')";
        $sql[]= "        and lc.lib_archive = '2'";
        $sql[]= "    )";
        $sql[]= "    or ("; // すべて表示フラグがなければ
        $sql[]= "        (";
        $sql[]= "            (lc.ref_dept_st_flg = 'f' and lc.ref_dept_flg <> '2' and lc.ref_st_flg <> '2')"; // 「すべてすべて参照」ならOK
        $sql[]= "            or";
        $sql[]= "            (lc.upd_dept_st_flg = 'f' and lc.upd_dept_flg <> '2' and lc.upd_st_flg <> '2')"; // あるいは「すべてすべて更新」ならOK
        $sql[]= "        )";
        $sql[]= "        and";
        $sql[]= "        ( lc.private_flg is null or lc.private_flg = 'f' )"; // プライベートフラグが立っていないもの？（過去の引継ぎで実装）
        $sql[]= "    )";
        $sql[]= ")";
    }

    // ******************************************************************************************
    // 以降、OR系分岐
    // ******************************************************************************************
    if (!$opt["is_admin"] && !$opt["is_login"]) {
        $sql[]= "and ( 1 <> 1";

        // 更新権限
        if (count($upd_or_joken)) {
            $sql[]= " or ".implode(" or ", $upd_or_joken); // 更新参照両方可能条件オア文
        }

        // 参照権限
        if ($lock_show_flg) { // lock_show_flg オン時。全体を見せる場合
            // 参照権限に自分の名前が直接指定されていればOK
            $sql[]= "or (";
            $sql[]= " libcaterefemp.lib_cate_id is not null";
            $sql[]= ")";

            // 参照権限。部署と役職指定による判定。全体書庫か部署書庫
            if ($a=="2" || !$a) {
                $sql[]= "or (";
                $sql[]= "    (";
                $sql[]= "        lc.lib_archive = '2'"; // 全体書庫は部署役職参照確認実施
                $sql[]= "    )";
                $sql[]= "    and lc.ref_dept_st_flg = 'f'"; // 「許可しない」チェックがオフ
                $sql[]= "    and exists (";
                $sql[]= "        select * from tmp_concurrent".$table_ukey." tc";
                $sql[]= "        where ( lc.ref_st_flg <> '2' or tc.n_st_id = libcaterefst.st_id )";
                $sql[]= "        and   ( lc.ref_dept_flg <> '2' or (tc.n_class_id = libcaterefdept.class_id and tc.n_atrb_id = libcaterefdept.atrb_id and tc.n_dept_id = libcaterefdept.dept_id ))";
                $sql[]= "    )";
                $sql[]= ")";
            }

            // 参照権限。部署書庫。部署と役職指定による判定。
            if ($a=="3" || !$a) {
                $sql[]= "or";
                $sql[]= "(";
                $sql[]= "    lc.lib_archive = '3'"; // 部署書庫も部署役職参照確認実施(所属施設内のみ参照可能、ではない)
                $sql[]= "    and lc.ref_dept_st_flg = 'f'"; // 「許可しない」チェックがオフ
                $sql[]= "    and exists (";
                $sql[]= "        select * from tmp_concurrent".$table_ukey." tc";
                $sql[]= "        where ( lc.ref_st_flg <> '2' or tc.n_st_id = libcaterefst.st_id )";
                $sql[]= "        and";
                $sql[]= "        (";
                // 部署は「すべてxすべて」でない場合は、部署権限で指定したとおりになる
                $sql[]= "            (";
                $sql[]= "                lc.lib_archive = '3'";
                $sql[]= "                and lc.ref_dept_flg = '2'"; // すべてxすべてではない場合
                $sql[]= "                and (";
                $sql[]= "                    tc.n_class_id = libcaterefdept.class_id";
                $sql[]= "                    and tc.n_atrb_id = libcaterefdept.atrb_id";
                $sql[]= "                    and tc.n_dept_id = libcaterefdept.dept_id";
                $sql[]= "                )";
                $sql[]= "            )";
                // 部署「すべてｘすべて」の場合は、以下で意味が異なる
                // ・「所属部署のみ参照可能」のとき、所属部署のみ許可される
                // ・「所属部署のみ参照可能」でないとき、常に許可される
                $sql[]= "            or";
                $sql[]= "            (";
                $sql[]= "                lc.lib_archive = '3'";
                $sql[]= "                and lc.ref_dept_flg <> '2'"; // すべてxすべての場合
                $sql[]= "                and";
                $sql[]= "                (";
                $sql[]= "                    (lc.private_flg = 't' and tc.t_class_id = lc.lib_link_id )"; // 所属部署のみの場合
                $sql[]= "                    or";
                $sql[]= "                    (lc.private_flg <> 't')"; // 所属部署のみ参照可能ではない
                $sql[]= "                )";
                $sql[]= "            )";
                $sql[]= "        )";
                $sql[]= "    )";
                $sql[]= ")";
            }
            // ★ 参照権限。委員会書庫。委員会の場合は、「委員会のみ参照可」チェックがなければ判定する。
            if ($a=="4" || !$a) {
                $sql[]= "or";
                $sql[]= "(";
                $sql[]= "        (lc.lib_archive = '4' and lc.private_flg <> 't')"; // 委員会は「委員会のみ参照」でなければ、部署役職参照確認実施。
                $sql[]= "        and lc.ref_dept_st_flg = 'f'"; // 「許可しない」チェックがオフ
                $sql[]= "        and ( lc.ref_st_flg <> '2' or libcaterefst.lib_cate_id is not null )"; // 役職が「すべて」または、職員の役職とマッチ
                $sql[]= "        and ( lc.ref_dept_flg <> '2' or libcaterefdept.lib_cate_id is not null )";// 部署が「すべて」または、職員の部署とマッチ
                if (!$opt["is_mypage"]) {
                    if ($class_id) $sql[]= "    and pj.class_id = ".$class_id; // class_idプルダウンが指定されていれば、マッチすること
                    if (!$class_id) $sql[]= "   and pj.class_id is null"; // class_idプルダウンが指定されていれば、マッチすること
                    if ($atrb_id)  $sql[]= "    and pj.atrb_id = ".$atrb_id; // atrb_idプルダウンが指定されていれば、マッチすること
                    if (!$atrb_id) $sql[]= "    and pj.atrb_id is null"; // atrb_idプルダウンが指定されていれば、マッチすること
                }
                $sql[]= ")";
            }

        } else if ($lock_show_all_flg) { // 部署のみ隠す場合
            // 参照権限に自分の名前が直接指定されていればOK
            $sql[]= "or (";
            $sql[]= " libcaterefemp.lib_cate_id is not null";
            $sql[]= ")";

            // 参照権限。部署と役職指定による判定。全体書庫か部署書庫
            if ($a=="2" || !$a) {
                $sql[]= "or (";
                $sql[]= "    (";
                $sql[]= "        lc.lib_archive = '2'"; // 全体書庫は部署役職参照確認実施
                $sql[]= "    )";
                $sql[]= "    and lc.ref_dept_st_flg = 'f'"; // 「許可しない」チェックがオフ
                $sql[]= "    and exists (";
                $sql[]= "        select * from tmp_concurrent".$table_ukey." tc";
                $sql[]= "        where ( lc.ref_st_flg <> '2' or tc.n_st_id = libcaterefst.st_id )";
                $sql[]= "        and   ( lc.ref_dept_flg <> '2' or (tc.n_class_id = libcaterefdept.class_id and tc.n_atrb_id = libcaterefdept.atrb_id and tc.n_dept_id = libcaterefdept.dept_id ))";
                $sql[]= "    )";
                $sql[]= ")";
            }
        } // -- lock_sho_flg
        $sql[]= ")";
    } // is_admin + ls_login

    $sql[]= "group by lc.lib_archive, lc.lib_cate_id, lc.lib_link_id, lc.reg_emp_id, lc.upd_time, lc.lib_cate_no";
    $sql[]= ",em.emp_lt_nm, em.emp_ft_nm, lc.lib_cate_nm";

    if (!$opt["exclude_orderby"]) {
        $sql[]= "order by coalesce(lc.lib_cate_no, ''), lower(em.emp_lt_nm), lower(em.emp_ft_nm), lower(lc.lib_cate_nm), lc.lib_cate_id";
    }
    return implode("\n", $sql);
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
}







//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
// libfolder フォルダの権限つきSELECT文
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
function lib_get_libfolder_sql($emp_id, $lock_show_flg, $lock_show_all_flg, $a, $c, $f, $opt) {

    if ($lock_show_flg=="f") $lock_show_flg = "";
    if ($lock_show_all_flg=="f") $lock_show_all_flg = "";

    global $table_ukey;
    if (!$table_ukey) {
        echo "table_ukeyが未設定（lib_cre_tmp_concurrent_table未実行）<br>";
        lib_debug_backtrace();
        die;
    }

    $upd_or_joken = array();
    if (!$opt["is_login"] && !$opt["is_admin"]) {
        // リンクなら即OK。結局実体が大事
        $upd_or_joken[]= "( lf.link_id is not null )";
        // 個人書庫の場合は、個人フォルダなら即OK
        if ($a=="1" || !$a) {
            $upd_or_joken[]= "( lc.lib_archive = '1' and lc.lib_link_id = '".$emp_id."' )";
        }

        // 委員会書庫。 自己の委員会なら即OK
        if ($a=="4" || !$a) {
            $upd_or_joken[]= "(lc.lib_archive = '4' and pj.pjt_id is not null and pj.is_users = '1')";
        }

        // 全体書庫、部署書庫、委員会書庫。更新権限に自分の名前が直接指定されていればOK
        if ($a=="2" || $a=="3" || $a=="4" || !$a) {
            $upd_or_joken[]= "(lf.lib_archive <> '1' and libfolderupdemp.folder_id is not null)";
        }

        // 部署書庫。更新権限が「すべてすべて」のとき、自分の部署であればOKという暗黙仕様
        if ($a=="3" || !$a) {
                $upd_or_joken[]= "( my_upd_busyo.t_class_id is not null )";
        }

        // 全体書庫、部署書庫、委員会書庫。部署と役職指定による判定
        if ($a=="2" || $a=="3" || !$a) {
            $upd_or_joken[]=
                "(".
                "    (lf.lib_archive = '2' or lf.lib_archive = '3')". // 個人書庫と委員会は除く
                "    and lf.upd_dept_st_flg = 'f'". // 「許可しない」チェックがオフ
                // 前段チェック「部署書庫の暗黙仕様」に該当「しない」こと
                "    and not (".
                "        lf.lib_archive = '3' and lf.upd_dept_flg <> '2' and lf.upd_st_flg <> '2'". // 部署書庫で、更新権限「すべてすべて」
                "    )".
                "    and exists (".
                "        select * from tmp_concurrent".$table_ukey." tc".
                "        where ( lf.upd_st_flg <> '2' or tc.n_st_id = libfolderupdst.st_id )".
                "        and   ( lf.upd_dept_flg <> '2' or (tc.n_class_id = libfolderupddept.class_id and tc.n_atrb_id = libfolderupddept.atrb_id and tc.n_dept_id = libfolderupddept.dept_id ))".
                "    )".
                ")";
        }
    }

    // **********************************************************************************
    // 主セレクトの実行
    // **********************************************************************************
    $sql =array();
    $sql[]= " select";
    $sql[]= " lf.folder_id";
    $sql[]= ",lf.lib_archive";
    $sql[]= ",lf.lib_cate_id";
    $sql[]= ",lf.reg_emp_id";
    $sql[]= ",lf.upd_time";
    $sql[]= ",lf.folder_name";
    $sql[]= ",lf.folder_no";
    $sql[]= ",lf.link_id";
    $sql[]= ",lc.lib_link_id";
    $sql[]= ",lt.parent_id";
    if ($opt["check_hilink"]) {
        $sql[]= ",case when ll.link_id is null then 0 else 1 end as is_hilink";
    } else {
        $sql[]= ", 0 as is_hilink";
    }

    if ($opt["is_admin"]) $sql[]= ",1 as writable";
    else if ($opt["is_login"] || !count($upd_or_joken)) $sql[]= ",0 as writable";
    else {
        $sql[]= ", max ( case";
        foreach ($upd_or_joken as $joken) {
            $sql[]= " when ".$joken." then 1";
        }
        $sql[]= " else 0 end ) as writable";
    }

    $sql[]= "from libfolder lf";
    $sql[]= "inner join libcate lc on (lc.lib_cate_id = lf.lib_cate_id and lc.libcate_del_flg = 'f')";
    $sql[]= "left outer join libtree lt on (lt.child_id = lf.folder_id)";
    if ($opt["check_hilink"]) {
        $sql[]= "left outer join ( select link_id from libfolder group by link_id) ll on (ll.link_id = lf.folder_id)";
    }

    // ★ 委員会のJOIN
    if ($a=="4" || !$a) {
        if ($opt["is_admin"]) {
            $sql[]= "left outer join tmp_project".$table_ukey." pj on (lc.lib_archive = '4' and pj.pjt_id = lc.lib_link_id)";
        } else {
            $sql[]= "left outer join tmp_project".$table_ukey." pj on (lc.lib_archive = '4' and pj.pjt_id = lc.lib_link_id and (pj.pjt_public_flag = 't' or pj.pjt_or_wkgp = 'pjt'))";
        }
    }

    if (!$opt["is_admin"] && !$opt["is_login"] && count($upd_or_joken)) {

        if ($a=="3" || !$a) {
            $sql[]= "left outer join (";
            $sql[]= "    select t_class_id from tmp_concurrent".$table_ukey." group by t_class_id";
            $sql[]= ") my_upd_busyo on ("; // 部署書庫で更新権限「すべてすべて」は、部署オンリーを意味する
            $sql[]= "    lf.lib_archive = '3'"; // 部署書庫である
            $sql[]= "    and lf.upd_dept_st_flg = 'f'"; // 「許可しない」チェックがオフ
            $sql[]= "    and lf.upd_dept_flg <> '2'"; // 部署が「すべて」である（つまり「指定する」ではない）
            $sql[]= "    and lf.upd_st_flg <> '2'"; // 役職が「すべて」である（つまり「指定する」ではない）
            $sql[]= "    and my_upd_busyo.t_class_id = lc.lib_link_id";
            $sql[]= ")";
        }

        $sql[]= "left outer join libfolderupdemp on (";
        $sql[]= "    lf.lib_archive <> '1'"; // 個人書庫は除く
        $sql[]= "    and libfolderupdemp.emp_id = '".$emp_id."'"; // 職員自身が含まれる
        $sql[]= "    and libfolderupdemp.folder_id = lf.folder_id"; // フォルダIDが存在
        $sql[]= ")";

        if ($a=="2" || $a=="3" || !$a) {
            $sql[]= "left outer join libfolderupdst on (";
            $sql[]= "    (lf.lib_archive = '2' or lf.lib_archive = '3')"; // 個人書庫と委員会は除く
            $sql[]= "    and lf.upd_dept_st_flg = 'f'"; // 「許可しない」チェックがオフ
            $sql[]= "    and lf.upd_st_flg = '2'"; // 役職が「指定する」である
            $sql[]= "    and libfolderupdemp.folder_id is null"; // 職員IDではマッチしなかった
            $sql[]= "    and libfolderupdst.folder_id = lf.folder_id"; // フォルダIDが存在
            $sql[]= "    and exists ("; // 職員の役職である
            $sql[]= "        select n_st_id from tmp_concurrent".$table_ukey;
            $sql[]= "        where  tmp_concurrent".$table_ukey.".n_st_id = libfolderupdst.st_id";
            $sql[]= "    )";
            $sql[]= ")";
        }

        if ($a=="2" || $a=="3" || !$a) {
            $sql[]= "left outer join libfolderupddept on (";
            $sql[]= "    (lf.lib_archive = '2' or lf.lib_archive = '3')"; // 個人書庫と委員会は除く
            $sql[]= "    and lf.upd_dept_st_flg = 'f'"; // 「許可しない」チェックがオフ
            $sql[]= "    and lf.upd_dept_flg = '2'"; // 部署が「指定する」である
            $sql[]= "    and libfolderupdemp.folder_id is null"; // 職員IDではマッチしなかった
            //$sql[]= "    and libfolderupdst.folder_id is null"; // 役職でもマッチしなかった
            $sql[]= "    and libfolderupddept.folder_id = lf.folder_id"; // フォルダIDが存在
            $sql[]= "    and exists ("; // 職員の所属である
            $sql[]= "        select n_class_id, n_atrb_id, n_dept_id from tmp_concurrent".$table_ukey;
            $sql[]= "        where  tmp_concurrent".$table_ukey.".n_class_id = libfolderupddept.class_id";
            $sql[]= "        and tmp_concurrent".$table_ukey.".n_atrb_id = libfolderupddept.atrb_id";
            $sql[]= "        and tmp_concurrent".$table_ukey.".n_dept_id = libfolderupddept.dept_id";
            $sql[]= "    )";
            $sql[]= ")";
        }

        $sql[]= "left outer join libfolderrefemp on (";
        $sql[]= "   libfolderrefemp.emp_id = '".$emp_id."'";
        $sql[]= "   and libfolderrefemp.folder_id = lf.folder_id";
        $sql[]= ")";

        if ($a=="2" || $a=="3" || $a=="4" || !$a) {
            $sql[]= "left outer join libfolderrefst on (";
            $sql[]= "    (";
            $sql[]= "        lf.lib_archive = '2'";
            $sql[]= "        or lf.lib_archive = '3'";
            $sql[]= "        or (lf.lib_archive = '4' and lf.private_flg <> 't')"; // 委員会は「委員会のみ参照」でなければ、部署役職参照確認実施。
            $sql[]= "    )";
            $sql[]= "    and lf.ref_dept_st_flg = 'f'"; // 「許可しない」チェックがオフ
            $sql[]= "    and lf.ref_st_flg = '2'"; // 役職が「指定する」である
            $sql[]= "    and libfolderrefemp.folder_id is null"; // 職員IDではマッチしなかった
            $sql[]= "    and libfolderrefst.folder_id = lf.folder_id"; // フォルダIDが存在
            $sql[]= "    and exists ("; // 職員の役職である
            $sql[]= "        select n_st_id from tmp_concurrent".$table_ukey;
            $sql[]= "        where  tmp_concurrent".$table_ukey.".n_st_id = libfolderrefst.st_id";
            $sql[]= "    )";
            $sql[]= ")";
        }

        if ($a=="2" || $a=="3" || $a=="4" || !$a) {
            $sql[]= "left outer join libfolderrefdept on (";
            $sql[]= "    (";
            $sql[]= "        lf.lib_archive = '2'";
            $sql[]= "        or lf.lib_archive = '3'";
            $sql[]= "        or (lf.lib_archive = '4' and lf.private_flg <> 't')"; // 委員会は「委員会のみ参照」でなければ、部署役職参照確認実施。
            $sql[]= "    )";
            $sql[]= "    and lf.ref_dept_st_flg = 'f'"; // 「許可しない」チェックがオフ
            $sql[]= "    and lf.ref_dept_flg = '2'"; // 部署が「指定する」である
            $sql[]= "    and libfolderrefemp.folder_id is null"; // 職員IDではマッチしなかった
            //$sql[]= "    and libfolderrefst.folder_id is null"; // 役職でもマッチしなかった
            $sql[]= "    and libfolderrefdept.folder_id = lf.folder_id"; // フォルダIDが存在
            $sql[]= "    and exists ("; // 職員の所属である
            $sql[]= "        select n_class_id, n_atrb_id, n_dept_id from tmp_concurrent".$table_ukey;
            $sql[]= "        where  tmp_concurrent".$table_ukey.".n_class_id = libfolderrefdept.class_id";
            $sql[]= "        and tmp_concurrent".$table_ukey.".n_atrb_id = libfolderrefdept.atrb_id";
            $sql[]= "        and tmp_concurrent".$table_ukey.".n_dept_id = libfolderrefdept.dept_id";
            $sql[]= "    )";
            $sql[]= ")";
        }
    }
    // ******************************************************************************************
    // AND系分岐（＝必須条件）
    // ******************************************************************************************

    $sql[]= "where";
    $sql[]= "( 1 = 1";

    if ($a) {
        $sql[]= "and lf.lib_archive = '".$a."'";
    }

    // カテゴリを指定されたら、そのカテゴリであること
    if ($c) $sql[]= "and lf.lib_cate_id = ".$c;

    if (is_array($opt["cate_ids"])) $sql[]= "and lf.lib_cate_id in (".implode(",", $opt["cate_ids"]).")";
    // フォルダは指定されていないが、フォルダ配列を指定されたら、フォルダIDがその配列に含まれていること
    if (!$f && is_array($opt["folder_ids"]) && count($opt["folder_ids"])) {
        $sql[]= "and lf.folder_id in (".implode(",", $opt["folder_ids"]).")";
    }
    // ★ 除外する委員会
    // 委員会カテゴリなら、委員会に紐つくこと
    // projectマスタに委員会かワークグループとしてエントリが存在すること
    // さらに、画面から所属指定されたら、委員会の子（ワークグループ）も含めること
    // 所属階層１のみ指定されたら、所属階層２がnullであるものを取得すること
    if ($a=="4" || !$a) {
        $sql[]= "and (";
        $sql[]= "    lf.lib_archive <> '4'";
        $sql[]= "    or ( lf.lib_archive = '4' and pj.pjt_id is not null )";
        $sql[]= ")";
    }
    //---------------------------------------------------------------------
    // ナローモード
    //---------------------------------------------------------------------
    if ($opt["is_narrow_mode"]) {
        $sql[]= "and ( 1 <> 1"; // AND括弧2開始
        //-----------------------
        // 自分自身フォルダを含める
        //-----------------------
        if ($c && $f && !$opt["exclude_self"]) {
            $sql[]= " or lf.folder_id = ".$f;
        }
        //-----------------------
        // プラス、子を含める。共有選択時（カテゴリ未指定）なら、子はカテゴリ一覧であるからスルーされる
        //-----------------------
        if ($c && $opt["with_child"]) {
            if ($f) $sql[]= " or lt.parent_id = ".$f; // かつ、親が指定フォルダである
            else    $sql[]= " or lt.parent_id is null"; // かつ、トップフォルダである
        }
        $sql[]= ")"; // AND括弧2閉じ
    }

    $sql[]= ")"; // リンク系or閉じ

    // ログイン画面の場合は、全体共有設定画面のshow_login_flgがオンでなければ、「すべてすべて」のみが表示されるように。
    if ($opt["is_login"]) {
        $sql[]= " and (";
        $sql[]= "    (lf.link_id is not null or lf.lib_archive = '2' or (lf.lib_archive <> '2' and lf.private_flg <> 't'))";
        $sql[]= "        and (";
                             // 設定画面で「すべて表示」を設定したなら、特に条件不要で全表示してよい
        $sql[]= "            exists (select libconfig.show_login_flg from libconfig where libconfig.show_login_flg = 't')";
        $sql[]= "            or ("; // フラグがなければ
        $sql[]= "                (";
        $sql[]= "                    (lf.ref_dept_st_flg = 'f' and lf.ref_dept_flg <> '2' and lf.ref_st_flg <> '2')"; // 「すべてすべて参照」ならOK
        $sql[]= "                    or";
        $sql[]= "                    (lf.upd_dept_st_flg = 'f' and lf.upd_dept_flg <> '2' and lf.upd_st_flg = '')"; // あるいは「すべてすべて更新」ならOK
        $sql[]= "                )";
        $sql[]= "                and";
        $sql[]= "                (lf.private_flg is null or lf.private_flg = 'f')"; // プライベートフラグがオフ（これは関係ないと思うが過去の引継ぎで実装）
        $sql[]= "            )";
        $sql[]= "        )";
        $sql[]= ")";
    }

    // ******************************************************************************************
    // 以降、OR系分岐
    // ******************************************************************************************
    if (!$opt["is_admin"] && !$opt["is_login"]) {
        $sql[]= "and ( 1 <> 1";
        if (count($upd_or_joken)) {
            $sql[]= " or ".implode(" or ", $upd_or_joken); // 更新可能条件オア文
        }

        if ($lock_show_all_flg) { // lock_show_all_flg オン時。全部表示や部署のみ隠す場合は、フォルダは隠さない。
            // 参照権限に自分の名前が直接指定されていればOK
            $sql[]= "or (";
            $sql[]= " libfolderrefemp.folder_id is not null";
            $sql[]= ")";

            // 参照権限。部署と役職指定による判定。個人書庫以外。委員会の場合は、「委員会のみ参照可」チェックがなければ判定する。
            if ($a=="2" || $a=="3" || $a=="4" || !$a) {
                $sql[]= "or";
                $sql[]= "(";
                $sql[]= "    (";
                $sql[]= "        lf.lib_archive = '2'"; // 全体書庫は部署役職参照確認実施
                $sql[]= "        or (lf.lib_archive = '3')"; // 部署書庫も部署役職参照確認実施(所属施設内のみ参照可能、ではない)
                $sql[]= "        or (lf.lib_archive = '4' and lf.private_flg <> 't')"; // 委員会は「委員会のみ参照」でなければ、部署役職参照確認実施。
                $sql[]= "    )";
                $sql[]= "    and lf.ref_dept_st_flg = 'f'"; // 「許可しない」チェックがオフ
                $sql[]= "    and exists (";
                $sql[]= "        select * from tmp_concurrent".$table_ukey." tc";
                $sql[]= "        where ( lf.ref_st_flg <> '2' or tc.n_st_id = libfolderrefst.st_id )";
                $sql[]= "        and";
                $sql[]= "        (";
                // 部署以外
                $sql[]= "            (";
                $sql[]= "                lf.lib_archive <> '3'";
                $sql[]= "                and (";
                $sql[]= "                    lf.ref_dept_flg <> '2'";
                $sql[]= "                    or (";
                $sql[]= "                        tc.n_class_id = libfolderrefdept.class_id";
                $sql[]= "                        and tc.n_atrb_id = libfolderrefdept.atrb_id";
                $sql[]= "                        and tc.n_dept_id = libfolderrefdept.dept_id";
                $sql[]= "                    )";
                $sql[]= "                )";
                $sql[]= "            )";
                // 部署は「すべてxすべて」でない場合は、部署権限で指定したとおりになる
                $sql[]= "            or";
                $sql[]= "            (";
                $sql[]= "                lf.lib_archive = '3'";
                $sql[]= "                and lf.ref_dept_flg = '2'"; // すべてxすべてではない場合
                $sql[]= "                and (";
                $sql[]= "                    tc.n_class_id = libfolderrefdept.class_id";
                $sql[]= "                    and tc.n_atrb_id = libfolderrefdept.atrb_id";
                $sql[]= "                    and tc.n_dept_id = libfolderrefdept.dept_id";
                $sql[]= "                )";
                $sql[]= "            )";
                // 部署「すべてｘすべて」の場合は、以下で意味が異なる
                // ・「所属部署のみ参照可能」のとき、所属部署のみ許可される
                // ・「所属部署のみ参照可能」でないとき、常に許可される
                $sql[]= "            or";
                $sql[]= "            (";
                $sql[]= "                lf.lib_archive = '3'";
                $sql[]= "                and lf.ref_dept_flg <> '2'"; // すべてxすべての場合
                $sql[]= "                and";
                $sql[]= "                (";
                $sql[]= "                    (lf.private_flg = 't' and tc.t_class_id = lc.lib_link_id )"; // 所属部署のみの場合
                $sql[]= "                    or";
                $sql[]= "                    (lf.private_flg <> 't')"; // 所属部署のみ参照可能ではない
                $sql[]= "                )";
                $sql[]= "            )";
                $sql[]= "        )";
                $sql[]= "    )";
                $sql[]= ")";
            }
        }
        $sql[]= ")";
    }

    $sql[]= "group by lf.folder_id, lf.lib_archive, lf.lib_cate_id, lf.reg_emp_id, lf.upd_time, lf.folder_name";
    $sql[]= ",lf.folder_no, lf.link_id, lc.lib_link_id, lt.parent_id, lf.link_id";
    if ($opt["check_hilink"]) {
        $sql[]= ",ll.link_id";
    }

    if (!$opt["exclude_orderby"]) {
        if ($opt["top_only"]) {
            $sql[]= " order by lf.link_id desc limit 1";
        } else {
            $sql[]= " order by coalesce(lf.folder_no, ''), lower(lf.folder_name), lf.folder_id";
        }
    }

//  echo '<pre>';
//  echo implode("\n", $sql);
    return implode("\n", $sql);
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
}









//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
// libinfo 文書の権限つきSELECT文
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■


// エディションのみは重いので、以下で逐次別途判定とする
function lib_check_edition($con, $fname, $lib_id) {
    $edition = lib_get_top_row($con, $fname, "select * from libedition where lib_id = ".$lib_id);
    if (!$edition) return 0;
    $max_edition = lib_get_one($con, $fname, "select max(edition_no) from libedition where base_lib_id = ".$edition["base_lib_id"]);
    if ($max_edition==$edition["edition_no"]) return 1;
    return 0;
}

function lib_get_libinfo_sql($emp_id, $lock_show_flg, $lock_show_all_flg, $a, $c, $f, $folder_ids, $opt) {

    if ($lock_show_flg=="f") $lock_show_flg = "";
    if ($lock_show_all_flg=="f") $lock_show_all_flg = "";
    $today_ymd = date("Ymd");

    global $table_ukey;
    if (!$table_ukey) {
        echo "table_ukeyが未設定（lib_cre_tmp_concurrent_table未実行）<br>";
        lib_debug_backtrace();
        die;
    }

    $upd_or_joken = array();
    if (!$opt["is_login"] && !$opt["is_admin"]) {
        // 個人書庫の場合は、個人フォルダなら即OK
        if ($a=="1" || !$a) {
            $upd_or_joken[]= "( li.lib_archive = '1' and lc.lib_link_id = '".$emp_id."' )";
        }

        // 委員会書庫。 自己の委員会なら即OK
        if ($a=="4" || !$a) {
            $upd_or_joken[]= "(li.lib_archive = '4' and pj.pjt_id is not null and pj.is_users = '1')";
        }

        // 全体書庫、部署書庫、委員会書庫。更新権限に自分の名前が直接指定されていればOK
        if ($a=="2" || $a=="3" || $a=="4" || !$a) {
            $upd_or_joken[]= "(li.lib_archive <> '1' and libupdemp.lib_id is not null)";
        }

        // 部署書庫。更新権限が「すべてすべて」のとき、自分の部署であればOKという暗黙仕様
        if ($a=="3" || !$a) {
            $upd_or_joken[]= "( my_upd_busyo.t_class_id is not null )";
        }

        // 全体書庫、部署書庫、部署と役職指定による判定
        if ($a=="2" || $a=="3" || !$a) {
            $upd_or_joken[]=
                "(".
                "    (li.lib_archive = '2' or li.lib_archive = '3')". // 個人書庫と委員会は除く
                "    and li.upd_dept_st_flg = 'f'". // 「許可しない」チェックがオフ
                // 前段チェック「部署書庫の暗黙仕様」に該当「しない」こと
                "    and not (".
                "        li.lib_archive = '3' and li.upd_dept_flg <> '2' and li.upd_st_flg <> '2'". // 部署書庫で、更新権限「すべてすべて」
                "    )".
                "    and exists (".
                "        select * from tmp_concurrent".$table_ukey." tc".
                "        where ( li.upd_st_flg <> '2' or tc.n_st_id = libupdst.st_id )".
                "        and   ( li.upd_dept_flg <> '2' or (tc.n_class_id = libupddept.class_id and tc.n_atrb_id = libupddept.atrb_id and tc.n_dept_id = libupddept.dept_id ))".
                "    )".
                ")";
        }
    }


    // **********************************************************************************
    // 主セレクトの実行
    // libeditionリビジョンが最新かどうかは、SQL発行後に確認のこと。
    // **********************************************************************************
    $sql =array();
    $sql[]= " select";
    $sql[]= " li.lib_archive";
    $sql[]= ",li.lib_id";
    $sql[]= ",li.lib_cate_id";
    $sql[]= ",li.lib_extension";
    $sql[]= ",li.lib_keyword";
    $sql[]= ",li.lib_summary";
    $sql[]= ",li.emp_id";
    $sql[]= ",li.show_login_flg";
    $sql[]= ",li.show_login_begin";
    $sql[]= ",li.show_login_end";
    $sql[]= ",li.folder_id";
    $sql[]= ",li.lib_type";
    $sql[]= ",li.lib_nm";
    $sql[]= ",li.lib_up_date";
    $sql[]= ",li.ref_count";
    $sql[]= ",li.lib_no";
    $sql[]= ",li.apply_id";
    $sql[]= ",lt.parent_id";
    if ($opt["include_emp_nm"]) {
        $sql[]=",em.emp_lt_nm || ' ' || em.emp_ft_nm as emp_nm";
    }

    if ($opt["is_admin"]) $sql[]= ",1 as writable";
    else if ($opt["is_login"] || !count($upd_or_joken)) $sql[]= ",0 as writable";
    else {
        $sql[]= ", max ( case";
        foreach ($upd_or_joken as $joken) {
            $sql[]= " when ".$joken." then 1";
        }
        $sql[]= " else 0 end ) as writable";
    }

    $sql[]= "from (";
    $sql[]= "        select * from libinfo li";
    $sql[]= "        where lib_delete_flag = 'f'";
    if ($a) $sql[]= "and lib_archive = '".$a."'"; // 書庫を指定されたら、その書庫であること
    if ($c) $sql[]= "and lib_cate_id = ".$c; // カテゴリを指定されたら、そのカテゴリであること
    if (is_array($opt["cate_ids"])) $sql[]= "and lib_cate_id in (".implode(",", $opt["cate_ids"]).")";
    if ($f) $sql[]= "and folder_id = ".$f; // フォルダが指定されたら、そのフォルダ直下であること
    // フォルダは指定されていないが、フォルダ配列を指定されたら、フォルダIDがその配列に含まれていること
    else if (!$f && is_array($folder_ids) && count($folder_ids)) $sql[]= "and folder_id in (".implode(",", $folder_ids).")";
    // カテゴリが指定されていてフォルダが指定されていない場合は、トップノードフォルダ
    else if ($c && !$f && !$opt["all_folder"]) $sql[]= "and folder_id is null";
    // 指定された更新日時以上であること。最新情報を取得しない場合は、空文字以上となり、全文書が対象。
    if ($opt["lib_upd_date"]) $sql[]= "and lib_up_date >= '".$opt["lib_upd_date"]."'";
    // マイページ対象を指定された場合
    if ($opt["show_mypage_flg"]) $sql[]= "and show_mypage_flg = 't'";
    $sql[]= ") li";
    $sql[]= "inner join libcate lc on (lc.lib_cate_id = li.lib_cate_id and lc.libcate_del_flg = 'f')";
    $sql[]= "left outer join libtree lt on ( lt.child_id = li.folder_id )";
    if ($opt["include_emp_nm"]) {
        $sql[]= "left outer join empmst em on ( em.emp_id = li.emp_id )";
    }

    // ★ 委員会のJOIN
    if ($a=="4" || !$a) {
        if ($opt["is_admin"]) {
            $sql[]= "left outer join tmp_project".$table_ukey." pj on (lc.lib_archive = '4' and pj.pjt_id = lc.lib_link_id)";
        } else {
            $sql[]= "left outer join tmp_project".$table_ukey." pj on (lc.lib_archive = '4' and pj.pjt_id = lc.lib_link_id and (pj.pjt_public_flag = 't' or pj.pjt_or_wkgp = 'pjt'))";
        }
    }

    if (!$opt["is_admin"] && !$opt["is_login"] && count($upd_or_joken)) {

        if ($a=="2" || $a=="3" || $a=="4" || !$a) {
            $sql[]= "left outer join libupdemp on (";
            $sql[]= "    li.lib_archive <> '1'"; // 個人書庫は除く
            $sql[]= "    and libupdemp.emp_id = '".$emp_id."'"; // 職員自身が含まれる
            $sql[]= "    and libupdemp.lib_id = li.lib_id"; // 文書が存在
            $sql[]= ")";
        }

        if ($a=="3" || !$a) {
            $sql[]= "left outer join (";
            $sql[]= "    select t_class_id from tmp_concurrent".$table_ukey." group by t_class_id";
            $sql[]= ") my_upd_busyo on ("; // 部署書庫で更新権限「すべてすべて」は、部署オンリーを意味する
            $sql[]= "    li.lib_archive = '3'"; // 部署書庫である
            $sql[]= "    and li.upd_dept_st_flg = 'f'"; // 「許可しない」チェックがオフ
            $sql[]= "    and li.upd_dept_flg <> '2'"; // 部署が「すべて」である（つまり「指定する」ではない）
            $sql[]= "    and li.upd_st_flg <> '2'"; // 役職が「すべて」である（つまり「指定する」ではない）
            $sql[]= "    and li.lib_archive = '3' and my_upd_busyo.t_class_id = lc.lib_link_id";
            $sql[]= ")";
        }

        if ($a=="2" || $a=="3" || !$a) {
            $sql[]= "left outer join libupdst on (";
            $sql[]= "    (li.lib_archive = '2' or li.lib_archive = '3')"; // 個人書庫と委員会は除く
            $sql[]= "    and li.upd_dept_st_flg = 'f'"; // 「許可しない」チェックがオフ
            $sql[]= "    and li.upd_st_flg = '2'"; // 役職が「指定する」である
            $sql[]= "    and libupdemp.lib_id is null"; // 職員IDではマッチしなかった
            $sql[]= "    and libupdst.lib_id = li.lib_id"; // 文書が存在
            $sql[]= "    and exists ("; // 職員の役職である
            $sql[]= "        select n_st_id from tmp_concurrent".$table_ukey;
            $sql[]= "        where  tmp_concurrent".$table_ukey.".n_st_id = libupdst.st_id";
            $sql[]= "    )";
            $sql[]= ")";
        }

        if ($a=="2" || $a=="3" || !$a) {
            $sql[]= "left outer join libupddept on (";
            $sql[]= "    (li.lib_archive = '2' or li.lib_archive = '3')"; // 個人書庫と委員会は除く
            $sql[]= "    and li.upd_dept_st_flg = 'f'"; // 「許可しない」チェックがオフ
            $sql[]= "    and li.upd_dept_flg = '2'"; // 部署が「指定する」である
            $sql[]= "    and libupdemp.lib_id is null"; // 職員IDではマッチしなかった
            //$sql[]= "    and libupdst.lib_id is null"; // 役職でもマッチしなかった
            $sql[]= "    and libupddept.lib_id = li.lib_id"; // 文書が存在
            $sql[]= "    and exists ("; // 職員の所属である
            $sql[]= "        select n_class_id, n_atrb_id, n_dept_id from tmp_concurrent".$table_ukey;
            $sql[]= "        where  tmp_concurrent".$table_ukey.".n_class_id = libupddept.class_id";
            $sql[]= "        and tmp_concurrent".$table_ukey.".n_atrb_id = libupddept.atrb_id";
            $sql[]= "        and tmp_concurrent".$table_ukey.".n_dept_id = libupddept.dept_id";
            $sql[]= "    )";
            $sql[]= ")";
        }
        $sql[]= "left outer join librefemp on (";
        $sql[]= "   librefemp.emp_id = '".$emp_id."'";
        $sql[]= "   and librefemp.lib_id = li.lib_id";
        $sql[]= ")";

        if ($a=="2" || $a=="3" || $a=="4" || !$a) {
            $sql[]= "left outer join librefst on (";
            $sql[]= "    (";
            $sql[]= "        li.lib_archive = '2'";
            $sql[]= "        or li.lib_archive = '3'";
            $sql[]= "        or (li.lib_archive = '4' and li.private_flg <> 't')"; // 委員会は「委員会のみ参照」でなければ、部署役職参照確認実施。
            $sql[]= "    )";
            $sql[]= "    and li.ref_dept_st_flg = 'f'"; // 「許可しない」チェックがオフ
            $sql[]= "    and li.ref_st_flg = '2'"; // 役職が「指定する」である
            $sql[]= "    and librefemp.lib_id is null"; // 職員IDではマッチしなかった
            $sql[]= "    and librefst.lib_id = li.lib_id"; // 文書が存在
            $sql[]= "    and exists ("; // 職員の役職である
            $sql[]= "        select n_st_id from tmp_concurrent".$table_ukey;
            $sql[]= "        where  tmp_concurrent".$table_ukey.".n_st_id = librefst.st_id";
            $sql[]= "    )";
            $sql[]= ")";
        }

        if ($a=="2" || $a=="3" || $a=="4" || !$a) {
            $sql[]= "left outer join librefdept on (";
            $sql[]= "    (";
            $sql[]= "        li.lib_archive = '2'";
            $sql[]= "        or li.lib_archive = '3'";
            $sql[]= "        or (li.lib_archive = '4' and li.private_flg <> 't')"; // 委員会は「委員会のみ参照」でなければ、部署役職参照確認実施。
            $sql[]= "    )";
            $sql[]= "    and li.ref_dept_st_flg = 'f'"; // 「許可しない」チェックがオフ
            $sql[]= "    and li.ref_dept_flg = '2'"; // 部署が「指定する」である
            $sql[]= "    and librefemp.lib_id is null"; // 職員IDではマッチしなかった
            //$sql[]= "    and librefst.lib_id is null"; // 役職でもマッチしなかった
            $sql[]= "    and librefdept.lib_id = li.lib_id"; // 文書が存在
            $sql[]= "    and exists ("; // 職員の所属である
            $sql[]= "        select n_class_id, n_atrb_id, n_dept_id from tmp_concurrent".$table_ukey;
            $sql[]= "        where  tmp_concurrent".$table_ukey.".n_class_id = librefdept.class_id";
            $sql[]= "        and tmp_concurrent".$table_ukey.".n_atrb_id = librefdept.atrb_id";
            $sql[]= "        and tmp_concurrent".$table_ukey.".n_dept_id = librefdept.dept_id";
            $sql[]= "    )";
            $sql[]= ")";
        }
    }

    $sql[]= "where li.lib_delete_flag = 'f'"; // 削除済でないこと

    // ******************************************************************************************
    // AND系分岐（＝必須条件）
    // ******************************************************************************************

    // 書庫を指定されたら、その書庫であること
    if ($a) {
        $sql[]= "and li.lib_archive = '".$a."'";
    }
    // カテゴリを指定されたら、そのカテゴリであること
    if ($c) {
        $sql[]= "and li.lib_cate_id = ".$c;
    }
    // フォルダが指定されたら、そのフォルダ直下であること
    if ($f) {
        $sql[]= "and li.folder_id = ".$f;
    }
    // フォルダは指定されていないが、フォルダ配列を指定されたら、フォルダIDがその配列に含まれていること
    else if (!$f && is_array($folder_ids) && count($folder_ids)) {
        $sql[]= "and li.folder_id in (".implode(",", $folder_ids).")";
    }
    // カテゴリが指定されていてフォルダが指定されていない場合は、トップノードフォルダ
    else if ($c && !$f && !$opt["all_folder"]) {
        $sql[]= "and li.folder_id is null";
    }
    // 指定された更新日時以上であること。最新情報を取得しない場合は、空文字以上となり、全文書が対象。
    if ($opt["lib_upd_date"]) $sql[]= "and li.lib_up_date >= '".$opt["lib_upd_date"]."'";
    // マイページ対象を指定された場合
    if ($opt["show_mypage_flg"]) {
        $sql[]= "and li.show_mypage_flg = 't'";
    }

    if ($opt["is_login"]) {
        $sql[]= "and li.lib_archive <> '1'";
//        $sql[]= "and (li.show_login_begin is null or li.show_login_begin <= '".$today_ymd."')";
//        $sql[]= "and (li.show_login_end is null or li.show_login_end >= '".$today_ymd."')";
//      $sql[]= "and (li.show_login_flg = 't'";
//      $sql[]= "     or exists (select libconfig.show_login_flg from libconfig where libconfig.show_login_flg = 't'))";
    }

    $sql[]=
    " and li.lib_delete_flag = 'f'".
    " and exists (".
    "     select * from libedition".
    "     where exists (".
    "         select * from (".
    "             select libedition.base_lib_id, max(libedition.edition_no) as edition_no".
    "             from libedition".
    "             group by libedition.base_lib_id".
    "         ) latest_edition".
    "         where latest_edition.base_lib_id = libedition.base_lib_id".
    "         and latest_edition.edition_no = libedition.edition_no".
    "     )".
    "     and libedition.lib_id = li.lib_id".
    " )";



    // 部署のカテゴリを取得する際は、部署マスタにエントリが存在すること
    if ($a=="3" || !$a) {
        $sql[]= "and (";
        $sql[]= "    li.lib_archive <> '3'";
        $sql[]= "    or (";
        $sql[]= "        li.lib_archive = '3'";
        $sql[]= "        and exists (";
        $sql[]= "            select class_id from classmst";
        $sql[]= "            where class_del_flg = 'f'";
        $sql[]= "            and to_char(classmst.class_id,'FM999999999999') = lc.lib_link_id";
        $sql[]= "        )";
        $sql[]= "    )";
        $sql[]= ")";
    }
    // 委員会カテゴリを取得する際は、projectマスタにエントリが存在すること
    if ($a=="4" || !$a) {
        $sql[]= "and (";
        $sql[]= "    li.lib_archive <> '4'"; // 委員会でなければ無視
        $sql[]= "    or (";
        $sql[]= "        li.lib_archive = '4'"; // 委員会の場合
        $sql[]= "        and exists (";
        $sql[]= "            select pjt_id from project"; // 委員会マスタに存在すること
        $sql[]= "            where pjt_delete_flag = 'f'";
        $sql[]= "            and to_char(pjt_id,'FM999999999999') = lc.lib_link_id";
        $sql[]= "        )";
        $sql[]= "    )";
        $sql[]= ")";
    }
    // ログイン画面の場合は、全体共有設定画面のshow_login_flgがオンでなければ、「すべてすべて」のみが表示されるように。
    if ($opt["is_login"]) {
        $sql[]= " and (";
//      $sql[]= "    (li.lib_archive = '2' or (li.lib_archive <> '2' and li.private_flg <> 't'))";
//      $sql[]= "        and (";
                             // 設定画面で「すべて表示」を設定したなら、特に条件不要で全表示してよい
        $sql[]= "            exists (select libconfig.show_login_flg from libconfig where libconfig.show_login_flg = 't')";
        $sql[]= "            or ("; // フラグがなければ
        $sql[]= "                (";
        $sql[]= "                    (li.ref_dept_st_flg = 'f' and li.ref_dept_flg <> '2' and li.ref_st_flg <> '2')"; // 「すべてすべて参照」ならOK
        $sql[]= "                    or";
        $sql[]= "                    (li.upd_dept_st_flg = 'f' and li.upd_dept_flg <> '2' and li.upd_st_flg <> '2')"; // あるいは「すべてすべて更新」ならOK
        $sql[]= "                )";
        $sql[]= "                and (li.private_flg is null or li.private_flg = 'f')"; // プライベートフラグがオフ（これは関係ないと思うが過去の引継ぎで実装）
//        $sql[]= "                and (li.show_login_begin is null or li.show_login_begin <= '".$today_ymd."')";
//        $sql[]= "                and (li.show_login_end is null or li.show_login_end >= '".$today_ymd."')";
        $sql[]= "                and (li.show_login_flg = 't')";
        $sql[]= "            )";
//      $sql[]= "        )";
        $sql[]= ")";
    }

    // ******************************************************************************************
    // 以降、OR系分岐
    // ******************************************************************************************
    if (!$opt["is_admin"] && !$opt["is_login"]) {
        $sql[]= "and ( 1 <> 1";
        if (count($upd_or_joken)) {
            $sql[]= "or ".implode(" or ", $upd_or_joken); // 更新参照両方可能条件オア文
        }

        //if ($lock_show_all_flg) { // lock_show_all_flg オン時。全部表示や部署のみ隠す場合。このとき、フォルダは隠さない。
            // 参照権限に自分の名前が直接指定されていればOK
            $sql[]= "or (";
            $sql[]= " librefemp.lib_id is not null";
            $sql[]= ")";

            // 参照権限。部署と役職指定による判定。全体または委員会。委員会の場合は、「委員会のみ参照可」チェックがなければ判定する。
            if ($a=="2" || $a=="4" || !$a) {
                $sql[]= "or (";
                $sql[]= "    (";
                $sql[]= "        li.lib_archive = '2'"; // 全体書庫は部署役職参照確認実施
                $sql[]= "        or (li.lib_archive = '4' and li.private_flg <> 't')"; // 委員会は「委員会のみ参照」でなければ、部署役職参照確認実施。
                $sql[]= "    )";
                $sql[]= "    and li.ref_dept_st_flg = 'f'"; // 「許可しない」チェックがオフ
                $sql[]= "    and exists (";
                $sql[]= "        select * from tmp_concurrent".$table_ukey." tc";
                $sql[]= "        where ( li.ref_st_flg <> '2' or tc.n_st_id = librefst.st_id )";
                $sql[]= "        and   ( li.ref_dept_flg <> '2' or (tc.n_class_id = librefdept.class_id and tc.n_atrb_id = librefdept.atrb_id and tc.n_dept_id = librefdept.dept_id ))";
                $sql[]= "    )";
                $sql[]= ")";
            }
            // 参照権限。部署と役職指定による判定。個人書庫以外。
            if ($a=="3" || !$a) {
                $sql[]= "or";
                $sql[]= "(";
                $sql[]= "    lc.lib_archive = '3'"; // 部署書庫も部署役職参照確認実施(所属施設内のみ参照可能、ではない)
                $sql[]= "    and li.ref_dept_st_flg = 'f'"; // 「許可しない」チェックがオフ
                $sql[]= "    and exists (";
                $sql[]= "        select * from tmp_concurrent".$table_ukey." tc";
                $sql[]= "        where ( li.ref_st_flg <> '2' or tc.n_st_id = librefst.st_id )";
                $sql[]= "        and";
                $sql[]= "        (";
                // 部署は「すべてxすべて」でない場合は、部署権限で指定したとおりになる
                $sql[]= "            (";
                $sql[]= "                li.lib_archive = '3'";
                $sql[]= "                and li.ref_dept_flg = '2'"; // すべてxすべてではない場合
                $sql[]= "                and (";
                $sql[]= "                    tc.n_class_id = librefdept.class_id";
                $sql[]= "                    and tc.n_atrb_id = librefdept.atrb_id";
                $sql[]= "                    and tc.n_dept_id = librefdept.dept_id";
                $sql[]= "                )";
                $sql[]= "            )";
                // 部署「すべてｘすべて」の場合は、以下で意味が異なる
                // ・「所属部署のみ参照可能」のとき、所属部署のみ許可される
                // ・「所属部署のみ参照可能」でないとき、常に許可される
                $sql[]= "            or";
                $sql[]= "            (";
                $sql[]= "                li.lib_archive = '3'";
                $sql[]= "                and li.ref_dept_flg <> '2'"; // すべてxすべての場合
                $sql[]= "                and";
                $sql[]= "                (";
                $sql[]= "                    (li.private_flg = 't' and tc.t_class_id = lc.lib_link_id )"; // 所属部署のみの場合
                $sql[]= "                    or";
                $sql[]= "                    (li.private_flg <> 't')"; // 所属部署のみ参照可能ではない
                $sql[]= "                )";
                $sql[]= "            )";
                $sql[]= "        )";
                $sql[]= "    )";
                $sql[]= ")";
            }
        //}
        $sql[]= ")";
    }

    $sql[]= "group by li.lib_archive, li.lib_id, li.lib_cate_id, li.lib_extension, li.lib_keyword, li.lib_summary";
    $sql[]= ",li.emp_id, li.show_login_flg, li.show_login_begin, li.show_login_end, li.folder_id, li.lib_type";
    $sql[]= ",li.lib_nm, li.lib_up_date, li.ref_count, li.lib_no, li.apply_id, lt.parent_id";
    if ($opt["include_emp_nm"]) {
        $sql[]= ",em.emp_lt_nm, em.emp_ft_nm";
    }

    if ($opt["show_mypage_flg"]) {
        $sql[]= "order by li.lib_up_date desc, li.lib_id";
    } else {
        $sql[]= "order by coalesce(li.lib_no, ''), lower(li.lib_nm), li.lib_id";
    }

    return implode("\n", $sql);
}


