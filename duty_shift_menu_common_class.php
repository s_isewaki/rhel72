<?php
require_once("Cmx.php");
require_once("Cmx/Model/SystemConfig.php");

require_once("about_comedix.php");
require_once("duty_shift_common_class.php");
require_once("duty_shift_duty_common_class.php");
require_once("duty_shift_report_common_class.php");
//繰越公休残日数 start
require_once("duty_shift_hol_remain_common_class.php");
require_once("settings_common.php");
//繰越公休残日数 end
require_once("show_class_name.ini");    // ユニット（３階層名）表示のため追加
require_once("timecard_common_class.php");

class duty_shift_menu_common_class
{
    var $file_name;     // 呼び出し元ファイル名
    var $_db_con;       // DBコネクション
    var $obj;           // 勤務シフト共通クラス
    var $obj_duty;      // 当直共通クラス
    var $obj_report;    // 届出書添付書類画面 様式９ 共通部品ＣＬＡＳＳ

    var $default_width = "24";

    //繰越公休残日数 start
    //繰越公休残日数用の情報 $this->set_hol_remain_info()で設定後使用する
    var $hol_remain_info = null;
    //繰越公休残日数 end

    /*************************************************************************/
    // コンストラクタ
    // @param object $con DBコネクション
    // @param string $fname 画面名
    /*************************************************************************/
    function duty_shift_menu_common_class($con, $fname)
    {
        // 呼び出し元ファイル名を保持
        $this->file_name = $fname;
        // DBコネクションを保持
        $this->_db_con = $con;
        // クラス new
        $this->obj = new duty_shift_common_class($con, $fname);
        $this->obj_duty = new duty_shift_duty_common_class($con, $fname);
        $this->obj_report = new duty_shift_report_common_class($con, $fname);
        //繰越公休残日数 start
        //フラグ等の初期化
        $this->hol_remain_info = new duty_shift_hol_remain_common_class($con, $fname, "", "", -1);
        //繰越公休残日数 end
    }

//-------------------------------------------------------------------------------------------------------------------------
// 関数一覧（ボタン）
//-------------------------------------------------------------------------------------------------------------------------
    /*************************************************************************/
    //
    // ボタン（勤務表｜勤務シフト作成画面用）
    //
    /*************************************************************************/
    //スタンプ対応 20140205 start
    function showButtonAtdbk($show_flg, $plan_results_flg, $stamp_flg) {
        if ($show_flg == "1") {
            $wk_width = 43 * 3;
        }
        else {
            $wk_width = 43;
        }
?>
  <tr>
    <td width="<?php echo $wk_width; ?>" height="60" bgcolor="#F4F4F4" id="btn_tbl">
      <table border="0" cellpadding="0" cellspacing="0" id="font-small">
        <tr>
        <?
        //-------------------------------------------------------------------------------
        //登録
        //-------------------------------------------------------------------------------
        if ($show_flg == "1") { ?>
            <td width="43" align="center" valign="top">
            <a href="javascript:void(0);" onclick="editData(2);return false;">
            <img src="img/ico-illust-touroku-p.gif" alt="登録" width="36" height="36" border="0" />
            </a>
            </td>
        <?
        //
        $stamp_img = ($stamp_flg != "1") ? "img/ico-illust-stamp.gif" : "img/ico-illust-stamp-off.gif";
        ?>
            <td width="43" align="center" valign="top">
            <a href="javascript:void(0);" onclick="setStampFlg();return false;"><img src="<? echo($stamp_img); ?>" alt="スタンプ" width="36" height="36" border="0" id="stamp_img" /></a>
            </td>
        <?php
        }
        //-------------------------------------------------------------------------------
        //印刷
        //-------------------------------------------------------------------------------
        ?>
        <td width="43" align="center" valign="top">
        <a href="javascript:void(0);" onclick="printSelect();return false;"><img src="img/ico-illust-insatu.gif" alt="印刷" width="36" height="36" border="0" /></a>
        </td>
        </tr>
        <tr>
        <? if ($show_flg == "1") { ?>
            <td align="center" valign="top">登録</td>
            <td align="center" valign="top">スタンプ</td>
        <? } ?>
        <td align="center" valign="top">印刷</td>

        </tr>
      </table>
    </td>
    <td width="570" height="60" bgcolor="#F4F4F4" align="left">
      <table border="0" cellpadding="1" cellspacing="2" align="left">
        <tr>
        <td height="25" valign="top">
        <?php
        //-------------------------------------------------------------------------------
        //予定／実績表示
        //-------------------------------------------------------------------------------
        if ($plan_results_flg == "2") {
            echo("<input type=\"button\" value=\"予定表示\" onclick=\"showPlanResults(0);\" >\n");
        } else {
            echo("<input type=\"button\" value=\"予実績表示\" onclick=\"showPlanResults(2);\" >&nbsp; \n");
        }
?>
        </td>
        </tr>
        <tr>
        <td height="25" valign="top"><br>
        </td>
        </tr>
        </table>
        </td>
    <td height="60" align="right" bgcolor="#F4F4F4">
      <table border="0" cellpadding="0" cellspacing="0" id="font-small">
        <tr>
            <?
        //-------------------------------------------------------------------------------
        //登録
        //-------------------------------------------------------------------------------
        if ($show_flg == "1") { ?>
          <td width="" align="right" valign="top">
            <a href="javascript:void(0);" onclick="editData(2);return false;">
            <img src="img/ico-illust-touroku-p.gif" alt="登録" width="36" height="36" border="0" />
            </a>
            </td>
<? } ?>
            </tr>
        <tr>
        <? if ($show_flg == "1") { ?>
            <td align="center" valign="top">登録</td>
        <? } ?>

        </tr>
            </table>
        </td>
    </tr>
        <?
    }
    //スタンプ対応 20140205 end
    /*************************************************************************/
    //
    // ボタン（勤務シフト作成｜勤務シフト作成画面用）
    //
    /*************************************************************************/
    function showButton($create_flg,
        $plan_results_flg,      //予実績表示フラグ  （０：予定表示、２：予定／実績表示）
        $plan_hope_flg,         //勤務希望表示フラグ（０：予定表示、２：予定／勤務希望表示）
        $plan_duty_flg,         //当直表示フラグ    （０：予定表示、２：予定／当直表示）
        $plan_comment_flg,      //コメント表示フラグ（０：予定表示、２：予定／コメント表示）
        $edit_start_day,        //入力表示の開始日（位置）
        $edit_end_day,          //入力表示の終了日（位置）
        $finish_flg,
        $stamp_flg,
        $fullscreen_flg,
        $term_chg_flg,
        $hope_get_btn_flg,
        $total_disp_flg,
        $night_only_flg,
        $standard_id,
        $draft_button_flg) {
        //-------------------------------------------------------------------------------
        //初期値設定
        //-------------------------------------------------------------------------------
        $disabled_flg = "disabled";
        if ($create_flg == "1") {
            $disabled_flg = "";
        }
        $data_name = "";        //２行目の表示データ名

        if ($create_flg == "1") {
            $wk_color = "#000000";
            $a_end_str = "</a>";
        } else {
            $wk_color = "#c0c0c0";
            $a_end_str = "";
        }
?>
  <tr>
    <td width="344" height="60" bgcolor="#F4F4F4" id="btn_tbl">
      <table border="0" cellpadding="0" cellspacing="0" id="font-small">
        <tr>
          <td width="43" align="center" valign="top">
<?
    if ($create_flg == "1") {
        $img_src = "img/ico-illust-touroku-p.gif";
        $onclick_str = "<a href=\"javascript:void(0);\" onclick=\"editData(2);return false;\">";
    } else {
        $img_src = "img/ico-illust-touroku-d.gif";
        $onclick_str = "";
    }
?>
        <?=$onclick_str?>
        <img src="<?=$img_src?>" alt="登録" width="36" height="36" border="0" />
        <?=$a_end_str?>
        </td>
<?
        // 週の場合、非表示
        if ($edit_start_day == $edit_end_day) {
            $stamp_img = ($stamp_flg != "1") ? "img/ico-illust-stamp.gif" : "img/ico-illust-stamp-off.gif";
?>

<!--          <td width="43" align="center" valign="top" id="stmp_td"><a href="#floatWindow" class="open" onclick="setStampFlg();" id="stmp_anchor"><img src="<?=$stamp_img?>" alt="スタンプ" width="36" height="36" border="0" id="stamp_img" /></a></td>-->

          <td width="43" align="center" valign="top"><a href="javascript:void(0);" onclick="setStampFlg();return false;"><img src="<?=$stamp_img?>" alt="スタンプ" width="36" height="36" border="0" id="stamp_img" /></a></td>

          <td width="43" align="center" valign="top" style="display:"><a href="javascript:void(0);" onclick="showEdit(document.mainform.edit_start_day.value, document.mainform.edit_end_day.value);return false;"><img src="img/ico-illust-saikeisan.gif" alt="再計算" width="36" height="36" border="0" /></a></td>

<? } ?>
          <td width="43" align="center" valign="top">

<?
    if ($create_flg == "1") {
        $img_src = "img/ico-illust-copy-kako.gif";
        $a_start_str = "<a href=\"javascript:void(0);\" onclick=\"copyBeforeMonth();return false;\">";
    } else {
        $img_src = "img/ico-illust-copy-kako-d.gif";
        $a_start_str = "";
    }
?>
        <?=$a_start_str?>
        <img src="<?=$img_src?>" alt="コピー" width="36" height="36" border="0" />
        <?=$a_end_str?>

        </td>
          <td width="43" align="center" valign="top">
<?
    if ($create_flg == "1" && $draft_button_flg == "1") {
        $img_src = "img/ico-illust-shitagaki.gif";
        $a_start_str = "<a href=\"javascript:void(0);\" onclick=\"editData(1);return false;\">";
    } else {
        $img_src = "img/ico-illust-shitagaki-d.gif";
        $a_start_str = "";
    }

?>
        <?=$a_start_str?>
        <img src="<?=$img_src?>" alt="下書き" width="36" height="36" border="0" />
        <?=$a_end_str?>
        </td>
          <td width="43" align="center" valign="top"><a href="javascript:void(0);" onclick="dataCheck();return false;"><img src="img/ico-illust-check.gif" alt="チェック" width="36" height="36" border="0" /></a></td>
          <td width="43" align="center" valign="top"><a href="javascript:void(0);" onclick="printSelect();return false;"><img src="img/ico-illust-insatu.gif" alt="印刷" width="36" height="36" border="0" /></a></td>
          <td width="43" align="center" valign="top"><a href="javascript:void(0);" onclick="makeExcel();return false;"><img src="img/ico-illust-excel.gif" alt="エクセル" width="36" height="36" border="0" /></a></td>
        <? if ($fullscreen_flg != "1") { ?>
          <td width="43" align="center" valign="top"><a href="javascript:void(0);" onclick="fullScreen();return false;"><img src="img/ico-illust-zengamen.gif" alt="全画面" width="36" height="36" border="0" /></a></td>
        <? } ?>
        </tr>
        <tr>
          <td align="center" valign="top"><font color="<?=$wk_color?>">登録</font></td>
<?
        // 週の場合、非表示
        if ($edit_start_day == $edit_end_day) {
?>
          <td align="center" valign="top">スタンプ</td>
          <td align="center" valign="top" id="stamp_tbl4" style="display:<?=$stamp_tbl_disp?>">再計算</td>
<? } ?>
          <td align="center" valign="top"><font color="<?=$wk_color?>">コピー</font></td>
          <td align="center" valign="top"><font color="<?=$wk_color?>">下書き</font></td>
          <td align="center" valign="top">チェック</td>
          <td align="center" valign="top">印刷</td>
          <td align="center" valign="top">エクセル</td>
        <? if ($fullscreen_flg != "1") { ?>
          <td align="center" valign="top">全画面</td>
        <? } ?>
        </tr>
      </table>
    </td>
    <td width="475" height="60" bgcolor="#F4F4F4" align="left">
      <table border="0" cellpadding="1" cellspacing="2" align="left">
        <tr>
        <td height="25" valign="top">
        <?if ($create_flg == "1") {?>
            <input type="button" class="button" onclick="staffAdd();return false;" value="応援追加"/>
        <?} else {?>
            <input type="button" class="button" disabled value="応援追加"/>
        <?}?>
        </td>

        <td height="25" valign="top">
        <?if ($create_flg == "1") {?>
            <input type="button" class="button" onclick="dutyHopeUptake();return false;" value="勤務希望取込"/>
        <?} else {?>
            <input type="button" class="button" disabled value="勤務希望取込"/>
        <?}?>
        </td>
        <td height="25" valign="top">
        <?if ($create_flg == "1") {?>
            <input type="button" class="button" onclick="autoMake();return false;" value="自動作成"/>
        <?} else {?>
            <input type="button" class="button" disabled value="自動作成"/>
        <?}?>
        </td>

        <td width="25" valign="top">
            <input type="button" class="button" onclick="makeExcelDow();return false;" value="曜日別集計表"/>
        </td>

        <td width="25" valign="top">
        <?if ($hope_get_btn_flg == "1") {?>
            <input type="button" class="button" onclick="staffSetAdd();return false;" value="職員設定取込"/>
        <?} else {?>
            <input type="button" class="button" disabled value="職員設定取込"/>
        <?}?>
        </td>

        <td width="25" valign="top">
        <?if ($night_only_flg == "1") {?>
            <input type="button" class="button" id="disp_night_shift" onclick="dispNightShiftOnly();return false;" value="夜勤のみ表示"/>
        <?} else {?>
            <input type="button" class="button" disabled value="夜勤のみ表示"/>
        <?}?>
        </td>
        </tr>

        <tr>
        <td>
        <?if ($plan_hope_flg == "2") {?>
        <?$data_name = "２段目：勤務希望";?>
        <input type="button" class="button" onclick="showHope(0);return false;" value="勤務希望非表示"/>
        <?} else {?>
            <?if ($create_flg == "1") {?>
            <input type="button" class="button" onclick="showHope(2);return false;" value="勤務希望表示"/>
            <?} else {?>
            <input type="button" class="button" disabled value="勤務希望表示"/>
            <?}?>
        <?}?>
        </td>

        <td>
        <?if ($plan_duty_flg == "2") {?>
        <?$data_name = "２段目：{$this->obj->duty_or_oncall_str}";?>
        <input type="button" class="button" onclick="showDuty(0);return false;" value="<? echo($this->obj->duty_or_oncall_str); ?>非表示"/>
        <?} else {?>
            <?if ($create_flg == "1") {?>
            <input type="button" class="button" onclick="showDuty(2);return false;" value="<? echo($this->obj->duty_or_oncall_str); ?>表示"/>
            <?} else {?>
            <input type="button" class="button" disabled value="<? echo($this->obj->duty_or_oncall_str); ?>表示"/>
            <?}?>
        <?}?>
        </td>

        <td>
        <?if ($plan_comment_flg == "2") {?>
        <?$data_name = "２段目：コメント";?>
        <input type="button" class="button" onclick="showComment(0);return false;" value="コメント非表示"/>
        <?} else {?>
            <?if ($create_flg == "1") {?>
            <input type="button" class="button" onclick="showComment(2);return false;" value="コメント表示"/>
            <?} else {?>
            <input type="button" class="button" disabled value="コメント表示"/>
            <?}?>
        <?}?>
        </td>

        <td>
        <?if ($total_disp_flg == "1") {?>
        <input type="button" class="button" onclick="setTotalDisp();return false;" value="集計表示"/>
        <?} else {?>
        <input type="button" class="button" onclick="setTotalDisp(1);return false;" value="集計非表示"/>
        <?}?>
        </td>

        <td>
        <?if ($plan_results_flg == "2") {?>
        <?$data_name = "２段目：勤務実績";?>
        <input type="button" class="button" onclick="showPlanResults();return false;" value="予定表示"/>
        <?} else {?>
        <input type="button" class="button" onclick="showPlanResults(2);return false;" value="予実績表示"/>
        <?}?>
        </td>

        <td>
        <input type="button" class="button" id="floatYakinAvg_button" value="月平均夜勤" <?if (empty($standard_id)) { echo 'disabled'; }?>/>
        </td>
        </tr>
      </table>
    </td>
    <td width="120" bgcolor="#F4F4F4">
<?
$this->showArrowButton();
?>
    </td>
    <td  width="" bgcolor="#F4F4F4">
      <table border="0" cellpadding="0" cellspacing="0">
        <tr>
  <td align="left" width="" nowrap>
<?
        //-------------------------------------------------------------------------------
        //登録済み
        //-------------------------------------------------------------------------------
        if ($finish_flg != "") {
            echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\">&nbsp;$finish_flg</font>");
        }
?>
</td>
     </tr>
        <tr>
        <td>
        <?
        echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\" nowrap>&nbsp;");
        echo($data_name);
        echo("</font>\n");
        ?>
        </td>
     </tr>
   </table>

    </td>
    <td height="60" align="right" bgcolor="#F4F4F4">
      <table border="0" cellpadding="0" cellspacing="0" id="font-small">
        <tr>
          <td width="" align="right" valign="top">
<?
    if ($create_flg == "1") {
        $img_src = "img/ico-illust-touroku-p.gif";
        $onclick_str = "<a href=\"javascript:void(0);\" onclick=\"editData(2);return false;\">";
    } else {
        $img_src = "img/ico-illust-touroku-d.gif";
        $onclick_str = "";
    }
?>
        <?=$onclick_str?>
        <img src="<?=$img_src?>" alt="登録" width="36" height="36" border="0" />
        <?=$a_end_str?>
        </td>
        </tr>
         <tr>
          <td align="center" valign="top"><font color="<?=$wk_color?>">登録</font></td>
         </tr>
    </table>
    </td>
  </tr>

<?

    }
    /**
     * This is method showButtonRslt
     * ボタン（勤務シフト作成｜勤務実績入力画面）
     * @param mixed $create_flg 更新権限 1:あり "":なし
     * @param mixed $edit_start_day 入力表示の開始日（位置）
     * @param mixed $edit_end_day 入力表示の終了日（位置）
     * @param mixed $stamp_flg スタンプフラグ
     * @param mixed $session セッションID
     * @param mixed $group_id グループID
        * @param mixed $disp_tosYotei_flg 当初予定フラグ
     * @return mixed なし
     *
     */
    function showButtonRslt($create_flg
        ,$edit_start_day
        ,$edit_end_day
        ,$stamp_flg
        ,$session
        ,$group_id
        ,$disp_tosYotei_flg) {

        $disabled_flg = "disabled";
        if ($create_flg == "1") { $disabled_flg = ""; }

        if ($create_flg == "1") {
            $wk_color = "#000000";
            $a_end_str = "</a>";
        } else {
            $wk_color = "#c0c0c0";
            $a_end_str = "";
        }
        ?>
        <tr>
        <!--    <td height="60" bgcolor="#F4F4F4" width="129" id="btn_tbl">--> <? // 幅 129 = 43 * 3  ?>
        <td height="60" bgcolor="#F4F4F4" width="215" id="btn_tbl"> <? // 幅 172 = 43 * 5  4を5に変更⇒20130124 ?>
        <table border="0" cellpadding="0" cellspacing="0" id="font-small">
        <tr>
        <td width="43" align="center" valign="top">
        <?
        if ($create_flg == "1") {
            $img_src = "img/ico-illust-touroku-p.gif";
            $a_start_str = "<a href=\"javascript:void(0);\" onclick=\"editData();return false;\">";
        } else {
            $img_src = "img/ico-illust-touroku-d.gif";
            $a_start_str = "";
        }

        ?>
        <?=$a_start_str?>
        <img src="<?=$img_src?>" alt="登録" width="36" height="36" border="0" />
        <?=$a_end_str?>

        </td>
        <?
        // 週の場合、非表示
        if ($edit_start_day == $edit_end_day) {
            $stamp_img = ($stamp_flg != "1") ? "img/ico-illust-stamp.gif" : "img/ico-illust-stamp-off.gif";
            ?>
            <td width="43" align="center" valign="top"><a href="javascript:void(0);" onclick="setStampFlg();return false;">
            <img src="<?=$stamp_img?>" alt="スタンプ" width="36" height="36" border="0" id="stamp_img" /></a>
            </td>
            <? $stamp_tbl_disp = ($stamp_flg == "1") ? "" : "none"; ?>

            <!--<td width="43" align="center" valign="top" id="stamp_tbl3" style="display:<?=$stamp_tbl_disp?>">-->
            <td width="43" align="center" valign="top" id="stamp_tbl3">
            <a href="javascript:void(0);" onclick="stampComp();return false;"><img src="img/ico-illust-saikeisan.gif" alt="再計算" width="36" height="36" border="0" /></a>
            <!--          <a href="duty_shift_results.php?session=<?=$session?>&group_id=<?=$group_id?>"><img src="img/ico-illust-saikeisan.gif" alt="再計算" width="36" height="36" border="0" /></a>-->
            </td>
        <? } ?>
        <td width="43" align="center" valign="top">
        <?
        if ($create_flg == "1") {
            $img_src = "img/ico-illust-copy.gif";
            $a_start_str = "<a href=\"javascript:void(0);\" onclick=\"planCopy();return false;\">";
        } else {
            $img_src = "img/ico-illust-copy-d.gif";
            $a_start_str = "";
        }

        ?>
        <?=$a_start_str?>
        <img src="<?=$img_src?>" alt="コピー" width="36" height="36" border="0" />
        <?=$a_end_str?>

        </td>

        <!-- エクセル対応 20130124 -->
        <td width="43" align="center" valign="top">
        <a href="javascript:void(0);" onclick="makeExcel();return false;">
        <img src="img/ico-illust-excel.gif" alt="エクセル" width="36" height="36" border="0" />
        </a>
        </td>


        </tr>
        <tr>
        <td align="center" valign="top"><font color="<?=$wk_color?>">登録</font></td>
        <?
        // 週の場合、非表示
        if ($edit_start_day == $edit_end_day) {
            ?>
            <td align="center" valign="top">スタンプ</td>
            <!--<td align="center" valign="top" id="stamp_tbl4" style="display:<?=$stamp_tbl_disp?>">再計算</td>-->
            <td align="center" valign="top" id="stamp_tbl4">再計算</td>
        <? } ?>
        <td align="center" valign="top"><font color="<?=$wk_color?>">コピー</font></td>
        <td align="center" valign="top"><font color="<?=$wk_color?>">エクセル</font></td>
        </tr>
        </table>
        </td>
        <td height="60" bgcolor="#F4F4F4" align="left">
        <table border="0" cellpadding="1" cellspacing="2" align="left">
        <tr>
        <? //繰越公休残日数 start ?>
        <td height="25" valign="top" align="left" width="">
        <input type="button" value="勤務集計表" onclick="total();" style="WIDTH: 90px; HEIGHT: 22px;">
        </td>
        <td height="25" valign="top" align="left" width="">
        <? //ボタン情報を返す関数を用意
        $arr_btn = $this->get_arr_rslt_add_btn();
        $idx = 0;
        if ($arr_btn[$idx]["str"] != "") {
            echo($arr_btn[$idx]["str"]);
        }
        ?>
        </td>
        <td height="25" valign="top" align="left" width="">
        <?
        $idx = 1;
        if ($arr_btn[$idx]["str"] != "") {
            echo($arr_btn[$idx]["str"]);
        }
        ?>
        </td>
        <td height="25" valign="top" align="left" width="">
        <?
        $idx = 3;
        if ($arr_btn[$idx]["str"] != "") {
            echo($arr_btn[$idx]["str"]);
        }
        ?>
        </td>
        <td height="25" valign="top" width="75">
        </td>
        </tr>
        <tr>
        <td nowrap height="25" valign="top">
        <input type="button" name="pdf_shift" value="出勤簿印刷" onclick="print_pdf(this.name);" style="WIDTH: 90px; HEIGHT: 22px;">
        </td>
        <td nowrap height="25" valign="top">
        <input type="button" name="pdf_A4" value="タイムカードA4横印刷" onclick="print_pdf(this.name);" style="WIDTH: 140px; HEIGHT: 22px;">
        </td>
        <td nowrap height="25" valign="top">
        <?
        $idx = 2;
        if ($arr_btn[$idx]["str"] != "") {
            echo($arr_btn[$idx]["str"]);
        }

        ?>
        </td>
        <td nowrap height="25" valign="top">

        </td>
        <td height="25" valign="top">

        </td>
        </tr>
        </table>
        </td>
        <td width="30" bgcolor="#F4F4F4"><? // 20131119 260->30 ?>
        </td>
        <? //繰越公休残日数 end ?>
        <td bgcolor="#F4F4F4">
        <table border="0" cellpadding="0" cellspacing="0">
        <tr>
        <td rowspan="2" valign="middle">
        <input type="button" value=" ← " onclick="scroll_region(1);" style="cursor:pointer;">
        </td>
        <td >
        <input type="button" value=" ↑ " onclick="scroll_region(2);" style="cursor:pointer;">
        </td>
        <td rowspan="2" valign="middle">
        <input type="button" value=" → " onclick="scroll_region(3);" style="cursor:pointer;">
        </td>
        </tr>
        <tr>
        <td >
        <input type="button" value=" ↓ " onclick="scroll_region(4);" style="cursor:pointer;">
        </td>
        </tr>
        </table>
        </td>
        <td width="300" bgcolor="#F4F4F4">
        </td>
        <td height="60" align="right" bgcolor="#F4F4F4">
        <table border="0" cellpadding="0" cellspacing="0" id="font-small">
        <tr>
        <td width="" align="right" valign="top">
        <?
        if ($create_flg == "1") {
            $img_src = "img/ico-illust-touroku-p.gif";
            $onclick_str = "<a href=\"javascript:void(0);\" onclick=\"editData();return false;\">";
        } else {
            $img_src = "img/ico-illust-touroku-d.gif";
            $onclick_str = "";
        }
        ?>
        <?=$onclick_str?>
        <img src="<?=$img_src?>" alt="登録" width="36" height="36" border="0" />
        <?=$a_end_str?>
        </td>
        </tr>
        <tr>

        <td align="center" valign="top"><font color="<?=$wk_color?>">登録</font></td>
        </tr>
        </table>
        </td>
        </tr>

        <?
        /*
        echo("<tr height=\"22\">\n");
        echo("<td align=\"left\" width=\"\">\n");
        //-------------------------------------------------------------------------------
        //予定コピー
        //-------------------------------------------------------------------------------
        echo("<input $disabled_flg type=\"button\" value=\"予定コピー\" onclick=\"planCopy();\" style=\"$wk_style\">&nbsp; \n");
        // スタンプ機能
        // 週の場合、非表示
        if ($edit_start_day == $edit_end_day) {
            $wk_btn_name = "スタンプ";
            if ($stamp_flg != "1") {
                echo("<input type=\"button\" value=\"{$wk_btn_name}有効\" onclick=\"setStampFlg();\" style=\"$wk_style\" id=\"stampbtn\">&nbsp; \n");
            } else {
                echo("<input type=\"button\" value=\"{$wk_btn_name}無効\" onclick=\"setStampFlg();\" style=\"$wk_style\" id=\"stampbtn\">&nbsp; \n");
            }
        }
        //-------------------------------------------------------------------------------
        //登録
        //-------------------------------------------------------------------------------
        echo("<input $disabled_flg type=\"button\" value=\"登録\" onclick=\"editData();\" style=\"$wk_style\">&nbsp; \n");
        echo("</td>\n");
        echo("</tr>\n");
        */
    }


    /**
     * シフト作成画面の矢印ボタンを表示する
     *
     * @return なし
     *
     */
    function showArrowButton() {
?>
      <table border="0" cellpadding="0" cellspacing="0">
        <tr>
  <td rowspan="2" valign="middle">
<input type="button" value=" ← " onclick="scroll_region(1);" style="cursor:pointer;">
</td>
<td >
<input type="button" value=" ↑ " onclick="scroll_region(2);" style="cursor:pointer;">
</td>
<td rowspan="2" valign="middle">
<input type="button" value=" → " onclick="scroll_region(3);" style="cursor:pointer;">
</td>
         </tr>
         <tr>
  <td >
<input type="button" value=" ↓ " onclick="scroll_region(4);" style="cursor:pointer;">
</td>
     </tr>
   </table>

<?

    }
//-------------------------------------------------------------------------------------------------------------------------
// 関数一覧（画面）
//-------------------------------------------------------------------------------------------------------------------------
    /*************************************************************************/
    //
    // シフトグループ（病棟）名 、年、月
    //
    /*************************************************************************/
    function showHead($session, $fname,
        $group_show_flg,            //シフトグループ表示フラグ（１：表示）
        $group_id,                  //勤務シフトグループＩＤ
        $group_array,               //勤務シフトグループ情報
        $duty_yyyy,                 //勤務年
        $duty_mm,                   //勤務月
        $url,                       //遷移先URL
        $url_option,                //URLオプション
        $finish_flg,
        $data_name="",
        $week_index                 //表示期間以外にプラス何週前表示
        ) {
        echo("<tr height=\"22\"> \n");
        echo("<td width=\"100%\" align=\"left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\">\n");
        //-------------------------------------------------------------------------------
        // 年
        //-------------------------------------------------------------------------------
//*     echo("&nbsp;&nbsp; \n");
//*     $wk_next = $duty_yyyy + 1;
//*     $wk_back = $duty_yyyy - 1;
//*     echo("<a href=\"$url?session=$session&group_id=$group_id&duty_yyyy=$wk_back&duty_mm=$duty_mm$url_option\">←</a>&nbsp; \n");
//*     echo("{$duty_yyyy}年&nbsp; \n");
//*     echo("<a href=\"$url?session=$session&group_id=$group_id&duty_yyyy=$wk_next&duty_mm=$duty_mm$url_option\">→</a>&nbsp;\n");
//*     echo("</font>\n");
//*     echo("&nbsp;\n");

        //翌年から過去5年までを出力 20130115
        $st_year = date("Y") - 5;
        echo("&nbsp; \n");
        echo("<select name=\"duty_yyyy\" id=\"duty_yyyy\" onchange=\"this.form.action = '$fname'; this.form.fullscreen_flg.value = ''; this.form.target = ''; $group_change_str this.form.submit();this.disabled = true\" disabled>");
        for ($year=date("Y")+1;$year >= $st_year; $year--) {
            echo("<option  value=\"$year\"");
            if ($year == $duty_yyyy) {
                echo(" selected");
            }
            echo(">".$year."年\n");
        }

        echo("</select>\n");
        echo("&nbsp;&nbsp; \n");

//*     //-------------------------------------------------------------------------------
//*     // シフトグループ（病棟）名
//*     //-------------------------------------------------------------------------------
//*     echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\">&nbsp;&nbsp;シフトグループ(病棟)名");
//*     if ($group_show_flg == "1") {
//*         //個別の場合、表示のみ
//*         for ($k=0; $k < count($group_array); $k++) {
//*             $wk_id = $group_array[$k]["group_id"];
//*             $wk_name = $group_array[$k]["group_name"];
//*             if ($group_id == $wk_id) {
//*                 break;
//*             }
//*         }
//*         echo("&nbsp;&nbsp;$wk_name");
//*     } else {
//*         //全体の場合
//*         $group_change_str = "";
//*         //シフト作成の場合、グループ変更フラグを１に設定
//*         if (strpos($fname, "duty_shift_menu.php") !== false) {
//*             $group_change_str = "this.form.group_change_flg.value = '1'; ";
//*         }
//*         echo("<select name=\"group_id\" id=\"group_id\" onchange=\"this.form.action = '$fname'; this.form.fullscreen_flg.value = ''; this.form.target = ''; $group_change_str this.form.submit();this.disabled = true\" disabled>"); //変更後と表示中に無効化 20111221
//*         for ($k=0; $k < count($group_array); $k++) {
//*             $wk_id = $group_array[$k]["group_id"];
//*             $wk_name = $group_array[$k]["group_name"];
//*             echo("<option  value=\"$wk_id\"");
//*             if ($group_id == $wk_id) {
//*                 echo(" selected");
//*             }
//*             echo(">$wk_name \n");
//*         }
//*         echo("</select>\n");
//*     }
//*     echo("</font>\n");

//*     echo("</td></tr> \n");
//*     echo("<tr height=\"22\"> \n");
//*     echo("<td width=\"100%\" align=\"left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\">&nbsp;&nbsp; \n");

        //-------------------------------------------------------------------------------
        // 月
        //-------------------------------------------------------------------------------
        $prev_mm = $duty_mm - 1;
        $prev_yyyy = $duty_yyyy;
        if ($prev_mm <= 0) {
            $prev_mm = 12;
            $prev_yyyy = $duty_yyyy - 1;
        }
        $next_mm = $duty_mm + 1;
        $next_yyyy = $duty_yyyy;
        if ($next_mm >= 13) {
            $next_mm = 1;
            $next_yyyy = $duty_yyyy + 1;
        }
        echo("<a href=\"$url?session=$session&group_id=$group_id&duty_yyyy=$prev_yyyy&duty_mm=$prev_mm$url_option\">←</a>&nbsp; \n");
        for ($i=1;$i<=12;$i++) {
            if ((int)$duty_mm == (int)$i) {
                $wk = $i . "月&nbsp; \n";
                echo($wk);
            } else {
                echo("<a href=\"$url?session=$session&group_id=$group_id&duty_yyyy=$duty_yyyy&duty_mm=$i$url_option\"> $i 月</a>&nbsp; \n");
            }
        }
        echo("<a href=\"$url?session=$session&group_id=$group_id&duty_yyyy=$next_yyyy&duty_mm=$next_mm$url_option\">→</a>\n");
        echo("</font>\n");

        //-------------------------------------------------------------------------------
        // 何週前表示   20130307
        //-------------------------------------------------------------------------------
        if ($url != "atdbk_duty_shift.php") {
            echo("&nbsp;&nbsp; \n");
            echo("<select name=\"before_week\" id=\"before_week\" onchange=\"show_before_week(this.selectedIndex);\">");
            for ($i = 0; $i <= 4; $i++){
                echo("<option value=\"$i\"");
                if ($week_index == $i){
                    echo(" selected");
                }
                echo(">$i \n");
            }
            echo("</select><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\">週前表示</font>\n");
        }
        //-------------------------------------------------------------------------------
        // シフトグループ（病棟）名
        //-------------------------------------------------------------------------------
        echo("&nbsp;&nbsp; \n");
        if ($group_show_flg == "1") {
            //個別の場合、表示のみ
            for ($k=0; $k < count($group_array); $k++) {
                $wk_id = $group_array[$k]["group_id"];
                $wk_name = $group_array[$k]["group_name"];
                if ($group_id == $wk_id) {
                    break;
                }
            }
            echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\">"); //20140214
            echo("&nbsp;&nbsp;$wk_name");
            echo("</font>\n"); //20140214
        } else {
            //全体の場合
            $group_change_str = "";
            //シフト作成の場合、グループ変更フラグを１に設定
            if (strpos($fname, "duty_shift_menu.php") !== false) {
                $group_change_str = "this.form.group_change_flg.value = '1'; ";
            }
            echo("<select name=\"group_id\" id=\"group_id\" onchange=\"this.form.action = '$fname'; this.form.fullscreen_flg.value = ''; this.form.target = ''; $group_change_str this.form.submit();this.disabled = true\" disabled>"); //変更後と表示中に無効化 20111221
            for ($k=0; $k < count($group_array); $k++) {
                $wk_id = $group_array[$k]["group_id"];
                $wk_name = $group_array[$k]["group_name"];
                echo("<option  value=\"$wk_id\"");
                if ($group_id == $wk_id) {
                    echo(" selected");
                }
                echo(">$wk_name \n");
            }
            echo("</select>\n");
        }



        //-------------------------------------------------------------------------------
        //登録済み
        //-------------------------------------------------------------------------------
//      if ($finish_flg != "") {
//          echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\">&nbsp;$finish_flg</font>");
//      }
//      echo("&nbsp;$data_name");
        echo("</td>\n");
        echo("</tr>\n");
    }
    /*************************************************************************/
    //
    // 見出し（週段）
    //
    /*************************************************************************/
    function showTitleSub1(
        $duty_yyyy,         //勤務年
        $duty_mm,           //勤務月
        $day_cnt,           //日数
        $edit_start_day,    //入力表示の開始日（位置）
        $edit_end_day,      //入力表示の終了日（位置）
        $week_array,        //週情報
        $title_gyo_array,   //行合計タイトル情報
        $check_flg,         //警告行表示フラグ（１：表示）
        $plan_comment_flg="",   //コメント表示フラグ
        $wk_group               //シフトグループ情報
        ){
        //-------------------------------------------------------------------------------
        //週段
        //-------------------------------------------------------------------------------
        //  echo("<tr height=\"22\"> \n");
        echo("<tr>\n");
        echo("<td width=\"40\"></td>\n");
        //チーム表示
        if ($wk_group[0]["team_disp_flg"] == "t") {
            echo("<td width=\"40\"></td>\n");
        }
        echo("<td width=\"100\"></td>\n");
        //職種表示
        if ($wk_group[0]["job_disp_flg"] == "t") {
            echo("<td width=\"120\"></td>\n");
        }
        //役職表示
        if ($wk_group[0]["st_disp_flg"] == "t") {
            echo("<td width=\"60\"></td>\n");
        }
        //雇用・勤務形態表示 20130219
        if ($wk_group[0]["es_disp_flg"] == "t") {
            echo("<td width=\"60\"></td>\n");
        }
        //勤務状況（日）
        $wk_cnt = 1;
        $wk_idx = 1;
        for ($k=1; $k<=$day_cnt; $k++) {
            //表示チェック
            $wk_show_flg = "";
            if ((($edit_start_day <= $k) && ($edit_end_day >= $k)) ||
                    ($edit_start_day == $edit_end_day)) {
                $wk_show_flg = "1";
            }
            //表示
            if ($wk_show_flg == "1") {
                $wk_td_id = "week" . $k;
                $wk_id_img = "img" . $wk_cnt;
                //週の最後日を算出
                for ($m=$k; $m<=$day_cnt; $m++) {
                    if (($week_array[$m]["name"] == "日") || ($m == $day_cnt)){
                        break;
                    }
                }
                //表示／入力で幅変更
                $wk_width = $this->default_width;
                if (($edit_start_day <= $k) && ($edit_end_day >= $k)) {
                    $wk_width = "100";
                    if ($edit_start_day == $edit_end_day) {
                        $wk_width = "200";
                    }
                } else {
                    if ($plan_comment_flg == "2") {
                        $wk_width = "40";
                    }
                }
                //１週間入力指定の場合、他の日は非表示
                if (($week_array[$k]["name"] == "月") || ($k == 1)){
                    echo("<td id=\"$wk_td_id\" width=\"$wk_width\" align=\"left\" style=\"cursor:pointer;\">");
                    if ($wk_width == $this->default_width) {
                        echo("<img src=\"img/spacer.gif\" width=\"11\" height=\"11\" id=\"$wk_id_img\" class=\"open\" onclick=\"showEdit($k, $m);\">");
                    } else {
                        echo("<img src=\"img/spacer.gif\" width=\"11\" height=\"11\" id=\"$wk_id_img\" class=\"close\" onclick=\"showEdit($k, $m);\">");
                    }
                    $wk_idx = 0;
                    $wk_cnt++;
                } else {
                    echo("<td id=\"$wk_td_id\" width=\"$wk_width\" align=\"left\">");
                }
                echo("</td>\n");
            }
            $wk_idx++;
        }

        //チェック結果
        if ($check_flg == "1"){
            echo("<td width=\"20\"></td> \n");
        }

        //個人日数
        $wk = count($title_gyo_array);
        $wk_width = 30 * $wk;
        echo("<td width=\"$wk_width\" colspan=\"$wk\"></td> \n");
        echo("</tr>\n");
    }
    /*************************************************************************/
    //
    // 見出し（日段）
    //
    /*************************************************************************/
    function showTitleSub2(
            $duty_yyyy,         //勤務年
            $duty_mm,           //勤務月
            $day_cnt,           //日数
            $edit_start_day,    //入力表示の開始日（位置）
            $edit_end_day,      //入力表示の終了日（位置）
            $week_array,        //週情報
            $title_gyo_array,   //行合計タイトル情報 ※集計列幅設定用に合計(total_0,total_1)と幅(td:td30,td40)情報を設定 20140502
            $check_flg,         //警告行表示フラグ（１：表示）
            $calendar_array,    //カレンダー情報
            $total_disp_flg,    //集計 1:非表示
            $four_week_line,    //４週８休チェックの線位置、0の場合はなし
            $hol_dt_flg,        //休暇日数詳細 1:表示 "0":非表示 "":詳細用の処理不要
            $title_hol_array,   //休暇タイトル情報
            $plan_comment_flg,  //コメント表示フラグ
            $plan_results_flg,  //予実表示フラグ （０：予定表示、２，３：予定／勤務実績表示）
            $paid_hol_disp_flg, //有給休暇残日数表示フラグ
            $wk_group,          //シフトグループ情報
            $list_kind = "",     //一覧種類
            $extra_day,         //表示期間前の日数 20130307
            $unit_name,         //ユニット（３階層）名
            $gyo_array,             //行合計情報 20140502
            $atdbk_duty_shift_flg   //勤務シフト希望フラグ（１:勤務シフト希望）
        ){
        //-------------------------------------------------------------------------------
        //日段
        //-------------------------------------------------------------------------------
        echo("<tr>\n");
        echo("<td class=\"td40\" rowspan=\"2\">番号</td>\n");
        //チーム表示
        if ($wk_group[0]["team_disp_flg"] == "t") {
            echo("<td class=\"td40\" rowspan=\"2\">チーム</td>\n");
        }
        //ユニット表示
        if ($wk_group[0]["unit_disp_flg"] == "t") {
            echo("<td class=\"td100\" rowspan=\"2\">$unit_name</td>\n");
        }
        echo("<td class=\"td100\" rowspan=\"2\">氏名</td>\n");
        //職種表示
        if ($wk_group[0]["job_disp_flg"] == "t") {
            echo("<td class=\"td120\" rowspan=\"2\">職種</td>\n");
        }
        //役職表示
        if ($wk_group[0]["st_disp_flg"] == "t") {
            echo("<td class=\"td60\" rowspan=\"2\">役職</td>\n");
        }
        //雇用・勤務形態表示 20130219
        if ($wk_group[0]["es_disp_flg"] == "t") {
            echo("<td class=\"td60\" rowspan=\"2\">雇用・勤務形態</td>\n");
        }

		//行事予定 20150709
		global $obj, $group_id, $start_date, $end_date;
		$arr_event_data = $obj->get_event_data($group_id, $start_date, $end_date);

        //勤務状況（日）
        for ($k=1; $k<=$day_cnt; $k++) {

            $f_style2 = "";
            // ４週８休チェックの線
            if ($four_week_line != 0 && ($k == $four_week_line || $k == $four_week_line + 28)) {
                $f_style2 = "border-left:#ff0000 solid 2px;";
            }

            $wk_bgcolor = "";
            //背景色を設定
            if($calendar_array[$k-1]["type"] == "6"){
                $wk_bgcolor = "#C8FFD5";
            }
            else if ($week_array[$k]["name"] == "土" || $calendar_array[$k-1]["type"] == "5") {
                $wk_bgcolor = "#E0FFFF";
            }
            else if ($week_array[$k]["name"] == "日" || $calendar_array[$k-1]["type"] == "4") {
                $wk_bgcolor = "#FFF0F5";
            }

            //表示チェック
            $wk_show_flg = "";
            if ((($edit_start_day <= $k) && ($edit_end_day >= $k)) ||
                    ($edit_start_day == $edit_end_day)) {
                $wk_show_flg = "1";
            }
            //表示／入力で幅変更
            $wk_width = $this->default_width;

            if (($edit_start_day <= $k) && ($edit_end_day >= $k)) {
                $wk_width = "100";
                if ($edit_start_day == $edit_end_day) {
                    $wk_width = "200";
                }
            } else {
                if ($plan_comment_flg == "2") {
                    $wk_width = "40";
                }
            }
            //表示
            if ($wk_show_flg == "1") {
                $wk_td_id = "day" . $k;
                $wk_date = $calendar_array[$k-1]["date"];
                $wk_dd = (int)substr($wk_date,6,2);
                //ドラック＆ドロップ 未使用
                //タイムカード一括修正対応 20100303
                if ($plan_results_flg == "3") {
                    $onclick_str = "openTimecardGroup('$group_id', '$wk_date');";
                } else {
                    //20150709
                    if ($atdbk_duty_shift_flg == "1") {
                        $onclick_str = "showEdit($k, $k);";
                    }
                    else {
                        $onclick_str = "openEventEdit('$wk_date', '$k');";
                    }
                    //mark・
                    if ($arr_event_data["$wk_date"]["mark_flg"] == "1") {
                    	$wk_dd .= "<font color='red'>・</font>";
                    }
                }
                $wk_div_id = "doc" . $k;


                if ($k <= $extra_day){
                    //表示期間外の日付のリングをはずす 20130307
                    $f_style2 .= "cursor:default;";
                    echo("<td id=\"$wk_td_id\" class=\"td{$wk_width}a day{$k}\" colspan=\"1\" bgcolor=\"#B7B7B7\" style=\"$f_style2\">");
                }else{
					$title_str = "";
					if ($arr_event_data["$wk_date"]["mark_flg"] == "1") {
						for ($e_idx=1; $e_idx<=4; $e_idx++) {
							$e_name = "event".$e_idx;
							if ($arr_event_data["$wk_date"][$e_name] != "") {
								if ($title_str != "") {
									$title_str .= "\n";
								}
								$title_str .= $arr_event_data["$wk_date"][$e_name];
							}
						}
					}
					
                    echo("<td id=\"$wk_td_id\" class=\"td{$wk_width}a day{$k}\" colspan=\"1\" bgcolor=\"$wk_bgcolor\" onClick=\"$onclick_str\" style=\"$f_style2\" title=\"$title_str\">");
                }
                echo("<div id=\"$wk_div_id\">$wk_dd</div>");
                echo("</td>\n");
            }
        }

        //チェック結果
        if ($check_flg == "1"){
            echo("<td class=\"td20\" rowspan=\"2\">警告</td>\n");
        }

        //個人日数
        $Agent = getenv( "HTTP_USER_AGENT" );
        //個人日数（タイトル）
        if ($total_disp_flg != "1") {
            // 集計
            $data_cnt = count($gyo_array);
            $title_gyo_cnt = count($title_gyo_array);
            $wk_kijun = 5; //列幅を変える基準文字列数 5桁(例99.25,100.5) 20140502
            for ($k=0; $k<count($title_gyo_array); $k++) {
                $wk_total_0 = 0;
                $wk_total_1 = 0; //2段目用
                $wk_long_flg = false; //各行が長い場合
                for ($m=0; $m<$data_cnt; $m++) {
                    $wk_total_0 += $gyo_array[$m][$k];
                    $k2 = $title_gyo_cnt + $k; //2段目用
                    $wk_total_1 += $gyo_array[$m][$k2];
                    if (!$wk_long_flg && (strlen($gyo_array[$m][$k]) >= $wk_kijun || strlen($gyo_array[$m][$k2]) >= $wk_kijun)) {
                        $wk_long_flg = true;
                    }
                }
                $title_gyo_array[$k]["total_0"] = $wk_total_0;
                $title_gyo_array[$k]["total_1"] = $wk_total_1; //2段目用
                if ($wk_long_flg || strlen($wk_total_0) >= $wk_kijun || strlen($wk_total_1) >= $wk_kijun) {
                    $title_gyo_array[$k]["td"] = "td40";
                }
                else {
                    $title_gyo_array[$k]["td"] = "td30";
                }
            }

            for ($k=0; $k<count($title_gyo_array); $k++) {
                $wk = $title_gyo_array[$k]["name"];

                if ($title_gyo_array[$k]["id"] == "10") {
                    //公休、有給表示
                    echo("<td class=\"td30b\" rowspan=\"2\">公休</td>\n");
                    echo("<td class=\"td30b\" rowspan=\"2\">年休<br>有休</td>\n");

                    if ($hol_dt_flg == "1") {
                        $wk = "<img src=\"img/spacer.gif\" width=\"11\" height=\"11\" class=\"close\" onclick=\"setTotalHolDisp('0');\"><br><a href=\"javascript:setTotalHolDisp('0');\">".$title_gyo_array[$k]["name"]."</a>";
                    } elseif ($hol_dt_flg == "0") {
                        $wk = "<img src=\"img/spacer.gif\" width=\"11\" height=\"11\" class=\"open\" onclick=\"setTotalHolDisp('1');\"><br><a href=\"javascript:setTotalHolDisp('1');\">".$title_gyo_array[$k]["name"]."</a>";
                    } else {
                        $wk = $title_gyo_array[$k]["name"];
                    }
                }
                $wk_td = ($title_gyo_array[$k]["td"] == "td30") ? "td30b" : $title_gyo_array[$k]["td"];
                echo("<td  class=\"{$wk_td}\" rowspan=\"2\">$wk</td>\n");
            }
            //有休残日数の表示
            if ($paid_hol_disp_flg === "t") {
                echo("<td class=\"td30b\" rowspan=\"2\">有残</td>\n");
            }
        }

        //休暇詳細
        $hol_detail = "";
        $hol_detail_count = 0;
        if ($hol_dt_flg == "1") {
            for ($k=0; $k<count($title_hol_array); $k++) {
                //公休・年休非表示
                if ($title_hol_array[$k]["reason"] == "1" ||
                        $title_hol_array[$k]["reason"] == "24") {
                    continue;
                }
                $wk = $title_hol_array[$k]["name"];

                $hol_detail .= "<td class=\"td30b\" colspan=\"1\">$wk</td>\n";
                $hol_detail_count++;
            }

            //ブラウザにより幅を調整
            if (preg_match("/Firefox/", $Agent) || preg_match("/Chrome/", $Agent) || preg_match("/Trident/", $Agent)) {
                $wk_width = 31 * $hol_detail_count - 1;
            } else {
                $wk_width = 30 * $hol_detail_count;
            }
            echo("<td class=\"td00\" colspan=\"$hol_detail_count\" style=\"width:$wk_width;\">休暇内訳</td>\n");
        }
        //繰越公休残日数 start
        if ($total_disp_flg != "1" && $this->hol_remain_info->transfer_holiday_days_flg == "t") {
            echo("<td class=\"td00a\" rowspan=\"2\" style=\"width:30;\">繰越公休残</td>\n");
        }
        //繰越公休残日数 end
        echo("<td class=\"td100\" rowspan=\"2\">氏名</td>\n");

        echo("</tr> \n");
        //-------------------------------------------------------------------------------
        //曜日段
        //-------------------------------------------------------------------------------
        echo("<tr>\n");
        //勤務状況（曜日）
        for ($k=1; $k<=$day_cnt; $k++) {
            $f_style2 = "";
            // ４週８休チェックの線
            if ($four_week_line != 0 && ($k == $four_week_line || $k == $four_week_line + 28)) {
                $f_style2 = "border-left:#ff0000 solid 2px;";
            } else {
                $f_style2 = "";
            }
            //表示チェック
            $wk_show_flg = "";
            if ((($edit_start_day <= $k) && ($edit_end_day >= $k)) ||
                    ($edit_start_day == $edit_end_day)) {
                $wk_show_flg = "1";
            }
            //表示
            if ($wk_show_flg == "1") {
                //TDタグID
                $wk_td_id = "day_of_week" . $k;
                //表示／入力で幅変更
                $wk_width = $this->default_width;
                if (($edit_start_day <= $k) && ($edit_end_day >= $k)) {
                    $wk_width = "100";
                    if ($edit_start_day == $edit_end_day) {
                        $wk_width = "200";
                    }
                } else {
                    if ($plan_comment_flg == "2") {
                        $wk_width = "40";
                    }
                }
            //土、日、祝日の文字色変更
            $wk_color = "";
            $wk_bgcolor = "";
            if($calendar_array[$k-1]["type"] == "6"){
                $wk_color = "color:#ff0000;";
                $wk_bgcolor = "#C8FFD5";
            }
            else if ($week_array[$k]["name"] == "土" || $calendar_array[$k-1]["type"] == "5") {
                $wk_color = "color:#0000ff;";
                $wk_bgcolor = "#E0FFFF";
            }
            else if ($week_array[$k]["name"] == "日" || $calendar_array[$k-1]["type"] == "4") {
                $wk_color = "color:#ff0000;";
                $wk_bgcolor = "#FFF0F5";
            }

            //表示
            $wk_memo = trim($calendar_array[$k-1]["memo"]);
            $wk_name = $week_array[$k]["name"];
                if ($k <= $extra_day){
                    //表示期間外の日付のリングをはずす 20130307
                    $f_style2 .= "cursor:default;";
                    echo("<td id=\"$wk_td_id\" class=\"td{$wk_width}a\" colspan=\"1\"  bgcolor=\"#B7B7B7\" style=\"".$wk_color.$f_style2."\">$wk_name</td>\n");
                }else{
                    echo("<td id=\"$wk_td_id\" class=\"td{$wk_width}a\" colspan=\"1\" onClick=\"showEdit($k, $k);\" onmouseover=\"showMemo($k,'$wk_memo');\" onmouseout=\"hideMsg();\" bgcolor=\"$wk_bgcolor\" style=\"".$wk_color.$f_style2."\">$wk_name</td>\n");
                }
            }
        }

        //休暇詳細
        if ($hol_dt_flg == "1") {
            echo($hol_detail);
        }
        echo("</tr> \n");
    }
////////////
    /*************************************************************************/
    //
    // 表データ
    // 引数に$atdbk_duty_shift_flg=""と$create_flg=""を追加しているため呼び出し元の修正時注意する。
    /*************************************************************************/
    function showList(
            $plan_array,            //勤務シフト情報
            $pattern_array,         //出勤パターン情報
            $warning_gyo_array,     //警告行情報
            $plan_results_flg,      //予実表示フラグ    （０：予定表示、２，３：予定／勤務実績表示）
            $plan_hope_flg,         //勤務希望表示フラグ（０：予定表示、２：予定／勤務希望表示）
            $plan_duty_flg,         //当直表示フラグ    （０：予定表示、２：予定／当直表示）
            $plan_comment_flg,      //コメント表示フラグ（０：予定表示、２：予定／コメント表示）
            $day_cnt,               //日数
            $edit_start_day,        //入力表示の開始日（位置）
            $edit_end_day,          //入力表示の終了日（位置）
            $week_array,            //週情報
            $title_gyo_array,       //行合計タイトル情報
            $gyo_array,             //行合計情報
            $check_flg,             //警告行表示フラグ（１：表示）
            $emp_id,                //職員ＩＤ
            $staff_del_flg,         //スタッフ削除可能フラグ（１：可能）
            $start_cnt,             //行表示開始位置
            $end_cnt,               //行表示終了位置
            $highlight_flg,         //強調表示フラグ（１：強調表示）
            $group_id,              //勤務シフトグループＩＤ
            $pattern_id,            //出勤パターンＩＤ
            $atdbk_duty_shift_flg,  //勤務シフト希望フラグ（１:勤務シフト希望）
            $create_flg,            //登録権限フラグ（１：可能）
            $total_disp_flg,        //集計 1:非表示
            $four_week_line,        //４週８休チェックの線位置、0の場合はなし
            $calendar_array,
            $hol_dt_flg,            //休暇日数詳細 1:表示 "":非表示
            $title_hol_array,       //休暇タイトル情報
            $hol_cnt_array,         //休暇日数
            $paid_hol_all_array,    //有給休暇残日数情報
            $reason_setting_flg,    //事由設定フラグ t:表示 f:非表示
            $plan_copy_flg,         //予定コピーフラグ "1":予定コピー 20100125
            $wk_group,              //シフトグループ情報
            $plan_individual,       //希望情報
            $paid_hol_disp_flg,     //有給休暇残日数表示フラグ
            $list_kind = "",        //一覧種類
            $extra_day,         //表示期間前の日数 20130307
            $unit_name          //ユニット（３階層）名
            )
    {
        $cnt = 1;
        $team_array = $this->obj->get_duty_shift_staff_team_array();
        $reason_2_array = $this->obj->get_reason_2("");
        $reason_2_all_array = $this->obj->get_reason_2("1");

//add 20111115-->
        //所定労働時間(通常勤務日/休日)情報取得
        $officehours_array = $this->obj->get_duty_shift_officehours2_array($group_id);
//add 20111115<--
//20140318
        $k = count($calendar_array) - 1;
        $wk_start_date = $calendar_array[0]["date"];
        $wk_end_date = $calendar_array[$k]["date"];
        $timecard_common_class = new timecard_common_class($this->_db_con, $this->file_name, $plan_array[0]["staff_id"], $wk_start_date, "");
        $arr_weekday_flg = $timecard_common_class->get_arr_weekday_flag($wk_start_date, $wk_end_date);

        // 勤務希望：色指定
        $conf = new Cmx_SystemConfig();

        // 希望一致
        $match_color = $conf->get('shift.hope.match.color');
        if (empty($match_color)) {
            $match_color = '#000000';
        }
        $match_bgcolor = $conf->get('shift.hope.match.bgcolor');
        if (empty($match_bgcolor)) {
            $match_bgcolor = '#00ff00';
        }

        // 希望不一致
        $unmatch_color = $conf->get('shift.hope.unmatch.color');
        if (empty($unmatch_color)) {
            $unmatch_color = '#000000';
        }
        $unmatch_bgcolor = $conf->get('shift.hope.unmatch.bgcolor');
        if (empty($unmatch_bgcolor)) {
            $unmatch_bgcolor = '#ffff00';
        }

        // 20100112 追加
        //換算日数の配列 [勤務パターングループ][勤務パターン]
        $arr_workday_count = array();
        $arr_after_night_duty = array();
        if ($plan_results_flg == "2" || $plan_results_flg == "3")   {
            $data_atdptn_all = $this->obj->get_atdptn_array("");

            for ($m=0; $m<count($data_atdptn_all); $m++) {
                $wk_group_id = $data_atdptn_all[$m]["group_id"];
                $wk_atdptn_ptn_id = $data_atdptn_all[$m]["id"];
                $arr_workday_count[$wk_group_id][$wk_atdptn_ptn_id] = $data_atdptn_all[$m]["workday_count"];
                $arr_after_night_duty[$wk_group_id][$wk_atdptn_ptn_id] = $data_atdptn_all[$m]["after_night_duty_flag"];
            }
        }

        // html要素のサイズ等設定 20121205追加(レイアウト調整の為)
        if ($list_kind === "create_shift") {    // 勤務シフト作成画面の表示
            $input_comment_size = "3";    // コメント入力エリアのサイズ
            $shift_select_class = " class=\"td100\"";    // シフト設定ドロップダウンリストのクラス
        } else {
            $input_comment_size = "10";
            $shift_select_class = "";
        }

        // 役職・職種・雇用勤務形態の履歴情報 20130219
        $history_info = $this->obj->get_history_info($calendar_array, $wk_group);

        for ($i=$start_cnt; $i<$end_cnt; $i++) {
            //-------------------------------------------------------------------------------
            // 予定データ
            //-------------------------------------------------------------------------------
            switch ($plan_array[$i]["night_duty_flg"]) {    // 夜勤の有無
                case "1":   // 有り
                case "2":   // 夜専
                    // 夜勤あり、又は夜専の場合
                    $shift_type = "night_shift";
                    break;
                default:
                    $shift_type = "day_shift";
                    break;
            }
            echo("<tr class=\"$shift_type\">\n");
            $span = "1";
            if (($plan_results_flg == "2") || ($plan_results_flg == "3") ||
                    ($plan_hope_flg == "2") ||
                    ($plan_duty_flg == "2") ||
                    ($plan_comment_flg == "2")) {
                $span = "2";
            }
            //-------------------------------------------------------------------------------
            // 表示順
            //-------------------------------------------------------------------------------
            //TDタグID
            $wk_td_id = "no" . $cnt;
            $wk = $plan_array[$i]["no"];
            // 背景色　週の場合
            if (($edit_start_day != $edit_end_day) && ($i % 2 == 1)) {
                $wk_bgcolor = "#ededed";
            } else {
                $wk_bgcolor = "";
            }

            //３人毎にセル下側の線を太くする
            if (($i + 1) % 3 == 0) {
                $f_style3 = " border-bottom:#5279a5 solid 2px;";
            } else {
                $f_style3 = "";
            }

            //番号
            if ($plan_results_flg == "3" || $atdbk_duty_shift_flg == "1") {
                echo("<td id=\"$wk_td_id\" class=\"td40\" rowspan=\"$span\" bgcolor=\"$wk_bgcolor\" style=\"$f_style3\">$wk</td>\n");

            } else {
                echo("<td id=\"$wk_td_id\" class=\"td40\" rowspan=\"$span\" bgcolor=\"$wk_bgcolor\" style=\"$f_style3\">");
                echo("<select name=\"no[$i]\">");
                for ($n_idx=1; $n_idx<=count($plan_array); $n_idx++) {
                    if ($n_idx == $wk) {
                        $selected = " selected";
                    } else {
                        $selected = "";
                    }
                    echo("<option value=\"$n_idx\"$selected>$n_idx</option>");
                }
                echo("</select>");
                echo("</td>\n");
            }
            //-------------------------------------------------------------------------------
            // チーム
            //-------------------------------------------------------------------------------
            //TDタグID
            $wk_td_id = "team" . $cnt;
            // チームの背景色
            $wk_color = "";
            if ($plan_array[$i]["other_staff_flg"] == "1") {
                //他病棟スタッフ
                $wk_color = "#dce8bb";
            }
            else {
                if ($i % 2 == 1) {
                    $wk_color = $wk_bgcolor;
                }
            }

            //チーム表示
            if ($wk_group[0]["team_disp_flg"] == "t") {
                echo("<td id=\"$wk_td_id\" width=\"40\" align=\"center\" rowspan=\"$span\" bgcolor=\"$wk_color\" style=\"$f_style3\">");
                echo("<select name=\"team[$i]\"><option></option>");
                foreach($team_array as $team){
                    $selected = "";
                    if($team["team_id"] == $plan_array[$i]["team_id"]){
                        $selected = 'selected';
                    }
                    echo("<option value=\"{$team["team_id"]}\" $selected>{$team["team_name"]}</option>");
                }
                echo("</select>\n");
                echo("</td>\n");
            }
            //-------------------------------------------------------------------------------
            //ユニット
            //-------------------------------------------------------------------------------
            if ($wk_group[0]["unit_disp_flg"] == "t") {
                $wk_td_id = "dept" . $cnt;  //TDタグID
                $es_color = ""; //現在は履歴考慮なし
                ///-----------------------------------------------------------------------------
                $wk = $this->obj->get_emp_unitname($plan_array[$i]["staff_id"], $group_id);
                echo("<td id=\"$wk_td_id\" class=\"td100\" rowspan=\"$span\" bgcolor=\"$wk_bgcolor\" style=\"color:$es_color;$f_style3\">$wk</td>\n");
            }
            //-------------------------------------------------------------------------------
            // 氏名
            //-------------------------------------------------------------------------------
            //TDタグID
            $wk_td_id = "name" . $cnt;
            // 氏名の背景色
            $wk_color = "";
            if ($plan_array[$i]["other_staff_flg"] == "1") {
                //他病棟スタッフ
                $wk_color = "#dce8bb";
            }
            else {
                if ($i % 2 == 1) {
                    $wk_color = $wk_bgcolor;
                }
            }
            $color = ( $plan_array[$i]["sex"] == 1 ) ? 'blue' : 'black';
            // 氏名
            if ($staff_del_flg == "1") {
                $wk1 = $plan_array[$i]["staff_id"];
                $wk2 = $plan_array[$i]["staff_name"];
                echo("<td id=\"$wk_td_id\" class=\"td100a\" rowspan=\"$span\" bgcolor=\"$wk_color\" onClick=\"staffDelete('$wk1', '$wk2');\" style=\"color:$color;$f_style3\">");
            } else {
                //実績入力時は、タイムカード一括修正画面を開く 20100303
                if ($plan_results_flg == "3") {
                    $wk_emp_id = $plan_array[$i]["staff_id"];
                    $onclick_str = "openTimecardAll('$wk_emp_id');";
                } else {
                    $onclick_str = "";
                }
                echo("<td id=\"$wk_td_id\" class=\"td100a\" rowspan=\"$span\" bgcolor=\"$wk_color\" onClick=\"$onclick_str\" style=\"color:$color;$f_style3\">");
            }
            $name_span = $span;
            $wk = $plan_array[$i]["staff_name"];
            echo("$wk</td>\n");
            //-------------------------------------------------------------------------------
            // 職種
            //-------------------------------------------------------------------------------
            $staff_id = $plan_array[$i]["staff_id"];
            //職種表示
            if ($wk_group[0]["job_disp_flg"] == "t") {
                $wk_td_id = "job" . $cnt;   //TDタグID

                //職種履歴を考慮する 20130219
                $arr_name_color = $this->obj->get_show_name($history_info["job_id_info"][$staff_id], $history_info["job_id_name"], "job_id", $history_info["start_date"], $history_info["end_date"]);
                $es_color = ($arr_name_color["color"] !="") ? $arr_name_color["color"] : "black" ;
                $wk = ($arr_name_color["name"] !="") ? $arr_name_color["name"] : $plan_array[$i]["job_name"];

                echo("<td id=\"$wk_td_id\" class=\"td120\" rowspan=\"$span\" bgcolor=\"$wk_bgcolor\" style=\"color:$es_color;$f_style3\">$wk</td>\n");
            }
            //-------------------------------------------------------------------------------
            // 役職
            //-------------------------------------------------------------------------------
            //役職表示
            if ($wk_group[0]["st_disp_flg"] == "t") {
                $wk_td_id = "st" . $cnt;    //TDタグID

                //役職履歴を考慮する 20130219
                $arr_name_color = $this->obj->get_show_name($history_info["st_id_info"][$staff_id], $history_info["st_id_name"], "st_id", $history_info["start_date"], $history_info["end_date"]);
                $es_color = ($arr_name_color["color"] !="") ? $arr_name_color["color"] : "black" ;
                $wk = ($arr_name_color["name"] !="") ? $arr_name_color["name"] : $plan_array[$i]["status"];

                echo("<td id=\"$wk_td_id\" class=\"td60\" rowspan=\"$span\" bgcolor=\"$wk_bgcolor\" style=\"color:$es_color;$f_style3\">$wk</td>\n");
            }
            //-------------------------------------------------------------------------------
            // 雇用・勤務形態 20130219
            //-------------------------------------------------------------------------------

            if ($wk_group[0]["es_disp_flg"] == "t") {
                $wk_td_id = "es" . $cnt;    //TDタグID

                //雇用・勤務形態履歴を考慮する
                $arr_wk = array("1"=>"常勤","2"=>"非常勤","3"=>"短時間正職員");
                $buf_wk = $plan_array[$i]["duty_form"];
                $arr_name_color = $this->obj->get_show_name($history_info["duty_form_info"][$staff_id], $arr_wk, "duty_form", $history_info["start_date"], $history_info["end_date"]);
                $es_color = ($arr_name_color["color"] !="") ? $arr_name_color["color"] : "black" ;
                $wk = ($arr_name_color["name"] !="") ? $arr_name_color["name"] : $arr_wk[$buf_wk];

                echo("<td id=\"$wk_td_id\" class=\"td60\" rowspan=\"$span\" bgcolor=\"$wk_bgcolor\" style=\"color:$es_color;$f_style3\">$wk</td>\n");
            }


            //-------------------------------------------------------------------------------
            // 勤務状況
            //-------------------------------------------------------------------------------

            //1段表示の場合、下側の線を太くする
            if ($span == "1") {
                $f_style4 = $f_style3;
            } else {
                //2段表示の場合、真ん中の線をオレンジにする
                $f_style4 = "border-bottom:#FF9900 solid thin;";
//              $f_style4 = "border-bottom:#FF9900 solid 2px;";
            }
            //マウスカーソル
            $wk_cursor1 = "";
            $wk_cursor2 = "";
            //シフト希望スタンプ対応 20140205 start
            if ($atdbk_duty_shift_flg == "1") {
                if ($i == 0) {
                    $wk_cursor1 = "cursor:pointer;";
                }
            }
            //1行目 実績入力以外はポインター
            elseif ($plan_results_flg != "3") {
                $wk_cursor1 = "cursor:pointer;";
            }
            //シフト希望スタンプ対応 20140205 end
            //2行目 実績入力、希望表示の場合
            if ($plan_results_flg == "3" || $plan_hope_flg == "2") {
                $wk_cursor2 = "cursor:pointer;";
            }

            $ex_cursor1 = $wk_cursor1;
            for ($k=1; $k<=$day_cnt; $k++) {

                $wk_cursor1 = ($k <= $extra_day) ? "" : $ex_cursor1; //表示期間外表示 20130307

                // ４週８休チェックの線
                if ($four_week_line != 0 && ($k == $four_week_line || $k == $four_week_line + 28)) {
                    $f_style = "border-left:#ff0000 solid 2px;".$f_style4.$wk_cursor1;
                } else {
                    $f_style = $f_style4.$wk_cursor1;
                }

                //-------------------------------------------------------------------------------
                //表示チェック
                //-------------------------------------------------------------------------------
                $wk_show_flg = "";
                if ((($edit_start_day <= $k) && ($edit_end_day >= $k)) ||
                        ($edit_start_day == $edit_end_day)) {
                    $wk_show_flg = "1";
                }
                //-------------------------------------------------------------------------------
                //対象データの場合
                //-------------------------------------------------------------------------------
                if ($wk_show_flg == "1") {
                    $wk_flg = "";
                    if (($edit_start_day <= $k) && ($edit_end_day >= $k)) {
                        $wk_flg = "1";
                    }
                    //-------------------------------------------------------------------------------
                    //希望の場合
                    //-------------------------------------------------------------------------------
                    if ($emp_id != "") {
                        //指定職員以外は表示のみ
                        if ($emp_id != $plan_array[$i]["staff_id"]) {
                            $wk_flg = "";
                        }
                    }
                    //-------------------------------------------------------------------------------
                    //TDタグID
                    //-------------------------------------------------------------------------------
                    $wk_td_id = "data" . $cnt . "_" . $k;
                    //-------------------------------------------------------------------------------
                    //表示／入力で幅変更
                    //-------------------------------------------------------------------------------
                    $wk_width = $this->default_width;
                    if (($edit_start_day <= $k) && ($edit_end_day >= $k)) {
                        $wk_width = "100";
                        if ($edit_start_day == $edit_end_day) {
                            $wk_width = "200";
                        }
                    } else {
                        if ($plan_comment_flg == "2") {
                            $wk_width = "40";
                        }
                    }
                    //-------------------------------------------------------------------------------
                    //他病棟の場合、表示のみ
                    //-------------------------------------------------------------------------------
                    //応援追加の場合
                    $wk_assist_flg = "";
                    if (/*$plan_array[$i]["pattern_id_$k"] != $pattern_id ||*/
                        ($plan_array[$i]["assist_group_$k"] != "" &&
                                $plan_array[$i]["assist_group_$k"] != $group_id)) {
                        $wk_flg = "";
                        //応援先のグループについて所属しているか確認（月別の所属） 20091019
                        $wk_group_id = $plan_array[$i]["assist_group_$k"];
                        $wk_emp_id = $plan_array[$i]["staff_id"];
                        //応援先がある場合、グレー表示、ない場合は、グレー表示しない（応援扱いしない）
                        $wk_assist_flg = "1";
                    }

                    //-------------------------------------------------------------------------------
                    //実績入力画面の場合、表示のみ
                    //-------------------------------------------------------------------------------
                    if ($plan_results_flg == "3") {
                        $wk_flg = "";
                    }
                    //-------------------------------------------------------------------------------
                    //入力又は表示
                    //-------------------------------------------------------------------------------
                    if ($wk_flg == "1") {
                        //-------------------------------------------------------------------------------
                        //勤務予定（シフト入力）
                        //-------------------------------------------------------------------------------
//*                     echo("<td id=\"$wk_td_id\" width=\"$wk_width\" align=\"center\" colspan=\"1\" bgcolor=\"$wk_bgcolor\" style=\"$f_style\">");
//*                     echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
                        echo("<td id=\"$wk_td_id\" class=\"td$wk_width\" colspan=\"1\" bgcolor=\"$wk_bgcolor\" style=\"$f_style\">");

                        $wk = "atdptn_ptn_id_$i" . "_" . $k;
                        $wk_tmcd_gid = $plan_array[$i]["pattern_id_$k"];
                        echo("<select name=\"$wk\" onchange=\"chk_individual($i, $k);setReason($i, $k, $wk_tmcd_gid, this.value, 1);");
                        //更新情報設定。ただし、出勤表の勤務希望入力からの場合を除く
                        if ($atdbk_duty_shift_flg != "1") {
                            echo("setUpdFlg($i);");
                        }
                        echo("\"$shift_select_class>");
                        echo("<option value=\"0000\" style=\"background-color:$wk_bgcolor;\">__ \n");
                        for ($m=0; $m<count($pattern_array); $m++) {
                            // 出勤表の休暇種別等画面の非表示を確認
                            if($pattern_array[$m]["reason_display_flag"] == "f") {
                                continue;
                            }
                            // 出勤表のパターン名＞出勤パターン名画面の非表示を確認 20120830
                            if($pattern_array[$m]["display_flag"] == "f") {
                                continue;
                            }
                            //ＩＤ
                            $wk_id = sprintf("%02d%02d", $pattern_array[$m]["atdptn_ptn_id"], $pattern_array[$m]["reason"]);
                            echo("<option value=\"$wk_id\"");
                            if ($pattern_array[$m]["reason"] != "") {
                                if (($plan_array[$i]["atdptn_ptn_id_$k"] == $pattern_array[$m]["atdptn_ptn_id"]) &&
                                        ($plan_array[$i]["reason_$k"] == $pattern_array[$m]["reason"])) {
                                    echo(" selected");
                                }
                            } else {
                                if ($plan_array[$i]["atdptn_ptn_id_$k"] == $pattern_array[$m]["atdptn_ptn_id"]) {
                                    echo(" selected");
                                }
                            }
                            //名称
                            $wk_name = $pattern_array[$m]["atdptn_ptn_name"];
                            // 半有半公等
                            if (($pattern_array[$m]["reason"] >= "45" &&
                                    $pattern_array[$m]["reason"] <= "47")) {
                                $wk_name = $pattern_array[$m]["reason_name"];
                                // 表示変更
                                for ($r_idx=0; $r_idx<count($reason_2_all_array); $r_idx++) {
                                    if ($reason_2_all_array[$r_idx]["id"] == $pattern_array[$m]["reason"]) {
                                        $wk_name = $reason_2_all_array[$r_idx]["name"];
                                        break;
                                    }
                                }

                            } else
                                if ($pattern_array[$m]["reason_name"] != "") {
                                    $wk_name .= "(" . $pattern_array[$m]["reason_name"] . ")";
                                }
                            echo(" style=\"background-color:$wk_bgcolor;\">$wk_name \n");
                        }
                        echo("</select> \n");
                        //-------------------------------------------------------------------------------
                        //勤務予定（事由：午前有給／午後有給）
                        //-------------------------------------------------------------------------------
                        //事由設定フラグが表示の場合
                        if ($edit_start_day == $edit_end_day &&
                                $reason_setting_flg == "t") {
                            $wk = "reason_2_$i" . "_" . $k;
                            echo("<select name=\"$wk\"");
                            //更新情報設定。ただし、出勤表の勤務希望入力からの場合を除く
                            if ($atdbk_duty_shift_flg != "1") {
                                echo(" onchange=\"setUpdFlg($i);\"");
                            }
                            echo(">");
                            echo("<option value=\"\">__ \n");
                            for ($m=0; $m<count($reason_2_array); $m++) {
                                $wk_id = $reason_2_array[$m]["id"];
                                echo("<option value=\"$wk_id\"");
                                if ($plan_array[$i]["reason_2_$k"] == $wk_id) {
                                    echo(" selected");
                                }
                                //名称
                                $wk_name = $reason_2_array[$m]["name"];
                                echo(">$wk_name \n");
                            }
                            echo("</select> \n");
                        }
                    } else {
                        //-------------------------------------------------------------------------------
                        //勤務予定（表示）
                        //-------------------------------------------------------------------------------
                        //他病棟、応援追加の判定追加
                        if ($wk_assist_flg == "") {
                            $wk_font_color = $plan_array[$i]["font_color_$k"];
                            $wk_back_color = $plan_array[$i]["back_color_$k"];
                        } else {
                            $wk_font_color = "#C0C0C0";     //灰色
                            $wk_back_color = "";
                        }
                        //勤務パターン登録子画面対応、onclick時の動作を変更
                        $wk_atdptn_ptn_id = $plan_array[$i]["atdptn_ptn_id_$k"];
                        $wk_reason = ($wk_atdptn_ptn_id == "10") ? $plan_array[$i]["reason_$k"] : "00";
                        $wk_id = sprintf("%02d%02d", $wk_atdptn_ptn_id, $wk_reason);
                        $onclick_str = "";
                        if ($create_flg == "1") {
                            if ($atdbk_duty_shift_flg == "1" && $emp_id != $plan_array[$i]["staff_id"]){
                                // 勤務希望入力で他職員
                                $onclick_str = "";
                            } else if ($plan_results_flg != "3" || ($atdbk_duty_shift_flg == "1" && $emp_id == $plan_array[$i]["staff_id"])) {
                                // 実績入力以外、勤務希望入力でログインユーザ
                                // 同グループ
                                //if ($plan_array[$i]["pattern_id_$k"] == $pattern_id) {
                                if ($wk_assist_flg == "") {
                                    $onclick_str = "ptnUpdate('0','".$i."','".$wk_id."','".$plan_array[$i]["reason_2_$k"]."','".$k."');";
                                } else {
                                    // 他グループ
                                    $onclick_str = "alert('他シフトグループの予定は更新できません。');";
                                }
                            } else {
                                // 表示
                                // 実績入力以外
                                if ($plan_results_flg != "3") {
                                    $onclick_str = "showEdit($k, $k);";
                                } else {
                                    // 実績入力
                                    //$onclick_str = "setStamp($i, $k, '2');";
                                    $onclick_str = "ptnUpdate('2','".$i."','','','".$k."');";
                                }
                            }
                        }


                        //曜日で背景色設定
                        if ($wk_back_color=="" || strtoupper($wk_back_color)=="#FFFFFF" ){
                                                    if($calendar_array[$k-1]["type"] == "6"){
                                                        $wk_back_color = "#C8FFD5";
                                                    }
                                                    else if ($week_array[$k]["name"] == "土" || $calendar_array[$k-1]["type"] == "5") {
                                                        $wk_back_color = "#E0FFFF";
                                                    }
                                                    else if ($week_array[$k]["name"] == "日" || $calendar_array[$k-1]["type"] == "4") {
                                                        $wk_back_color = "#FFF0F5";
                                                    }
                        }

                        //希望と予定比較し背景色変更 20120403
                        $wk_emp_id = $plan_array[$i]["staff_id"];
                        $wk_hope_ptn = $plan_individual["e{$wk_emp_id}"]["d{$k}"]["id"];
                        $wk_plan_ptn = $plan_array[$i]["atdptn_ptn_id_$k"];
                        //両方設定されている場合
                        if ($wk_hope_ptn != "" && $wk_plan_ptn != "") {
                            //一致の場合
                            if ($wk_hope_ptn == $wk_plan_ptn) {
                                if ($match_bgcolor !== 'noset') {
                                    $wk_back_color = $match_bgcolor;
                                }
                                if ($match_color !== 'noset') {
                                    $wk_font_color = $match_color;
                                }
                            }
                            //不一致の場合
                            else {
                                if ($unmatch_bgcolor !== 'noset') {
                                    $wk_back_color = $unmatch_bgcolor;
                                }
                                if ($unmatch_color !== 'noset') {
                                    $wk_font_color = $unmatch_color;
                                }
                            }
                        }

                        //表示期間外表示 20130307
                        if ($k <= $extra_day){
                            $onclick_str = "";
                        }
                        // 2014/10 tdタグ内属性にて、reasonとplan_ptnを管理するようにしてみた
                        echo("<td reason=\"".$plan_array[$i]["reason_$k"]."\" plan_ptn=\"".$wk_plan_ptn."\" id=\"$wk_td_id\" class=\"td$wk_width\" colspan=\"1\" bgcolor=\"$wk_back_color\" onClick=\"$onclick_str\" style=\"color:$wk_font_color;$f_style\">");
                        //名称設定
                        $wk = $plan_array[$i]["font_name_$k"];
	                    if ($wk != "&nbsp;") {
	                    	$wk = h($wk);
	                    }
                        if ($plan_array[$i]["reason_2_$k"] != "" &&
                                $reason_setting_flg == "t") {
                            for ($m=0; $m<count($reason_2_array); $m++) {
                                if ($plan_array[$i]["reason_2_$k"] == $reason_2_array[$m]["id"]) {
                                    $wk .= $reason_2_array[$m]["font_name"];
                                    break;
                                }
                            }
                        }
//*                     $fsize = (strlen($wk) > 2) ? "j10" : "j12";
//*                     echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$fsize\" color=\"$wk_font_color\" >");
                        echo($wk);
                    }
//*                 echo("</font></td> \n");
                    echo("</td> \n");
                }
            }
            //-------------------------------------------------------------------------------
            //チェック結果
            //-------------------------------------------------------------------------------
            if ($check_flg == "1"){
                $wk_td_id = "error_gyo" . $cnt; //TDタグID
                $wk = $warning_gyo_array[$i]["msg"];
                if ($wk == "") { $wk_back_color = $wk_bgcolor; }
                if ($wk != "") { $wk_back_color = "#ff0000"; }

                echo("<td id=\"$wk_td_id\" width=\"20\" align=\"center\" rowspan=\"$span\" bgcolor=\"$wk_back_color\" onmouseover=\"showErrGyo($i,'$wk');\" onmouseout=\"hideMsg();\" style=\"$f_style3\"> \n");
                echo($warning_gyo_array[$i]["msg_abb"]);
                echo("</td> \n");
            }
            //-------------------------------------------------------------------------------
            //個人日数
            //-------------------------------------------------------------------------------
            if ($total_disp_flg != "1") {
                //予実績の集計
                if ($plan_results_flg == "2" || $plan_results_flg == "3" || $plan_hope_flg == "2") {
                    $span = "1";
                }
                elseif ($plan_duty_flg == "2") {
                    $span = "2";
                }

                //集計欄では当直とコメント時は1段用の色とする
                if ($plan_duty_flg == "2" || $plan_comment_flg == "2") {
                    $f_style4 = $f_style3;
                }
                //公休・年休表示
                $wk_koukyu_cnt = 0;
                $wk_nenkyu_cnt = 0;
                $wk_koukyu_pos = 0;
                $wk_nenkyu_pos = 0;
                for ($k=0; $k<count($title_hol_array); $k++) {
                    //公休・年休表示
                    if ($title_hol_array[$k]["reason"] == "1") {
                        $wk_nenkyu_cnt = $hol_cnt_array[$i][$k];
                        $wk_nenkyu_pos = $k+1;
                    }
                    if ($title_hol_array[$k]["reason"] == "24") {
                        $wk_koukyu_cnt = $hol_cnt_array[$i][$k];
                        $wk_koukyu_pos = $k+1;
                    }
                }
                if ($hol_dt_flg != "1") {
                    $wk_nenkyu_pos = 1;
                    $wk_koukyu_pos = 2;
                }
                for ($k=0; $k<count($title_gyo_array); $k++) {
                    $wk = $gyo_array[$i][$k];
                    if ($title_gyo_array[$k]["id"] == "10") {
                        //公休、有休表示 $wk_koukyu_pos $wk_nenkyu_pos
                        $wk_td_id = "sum_gyo" . $cnt . "_" . ($k + $wk_koukyu_pos); //TDタグID
                        echo("<td id=\"$wk_td_id\" class=\"td30\" rowspan=\"$span\" bgcolor=\"$wk_bgcolor\" style=\"$f_style4\">$wk_koukyu_cnt</td>\n");
                        $wk_td_id = "sum_gyo" . $cnt . "_" . ($k + $wk_nenkyu_pos); //TDタグID
                        echo("<td id=\"$wk_td_id\" class=\"td30\" rowspan=\"$span\" bgcolor=\"$wk_bgcolor\" style=\"$f_style4\">$wk_nenkyu_cnt</td> \n");
                        //減算する
                        $wk = $wk - $wk_nenkyu_cnt - $wk_koukyu_cnt;
                    }
                    $wk_td_id = "sum_gyo" . $cnt . "_" . $k;    //TDタグID
                    //集計列幅 20140502
                    $wk_td = $title_gyo_array[$k]["td"];
                    echo("<td id=\"$wk_td_id\" class=\"{$wk_td} sum_gyo_0\" rowspan=\"$span\" bgcolor=\"$wk_bgcolor\" style=\"$f_style4\">$wk</td> \n"); //集計用 20140205
                }
                //有給休暇残日数(予定)
                if ($paid_hol_disp_flg === "t") {
                    $wk = $paid_hol_all_array["plan"][$i];
                    $wk_td_id = "sum_gyo" . $cnt . "_" . $k;    //TDタグID
                    echo("<td id=\"$wk_td_id\" class=\"td30\" rowspan=\"$span\" bgcolor=\"$wk_bgcolor\" style=\"$f_style4\">$wk</td> \n");
                }
            }
            if ($hol_dt_flg == "1") {
                //予実績の集計
                if ($plan_results_flg == "2" || $plan_results_flg == "3" || $plan_hope_flg == "2") {
                    $span = "1";
                }
                elseif ($plan_duty_flg == "2") {
                    $span = "2";
                }
                $k_idx = ($total_disp_flg != "1") ? count($title_gyo_array) : 0;
                for ($k=0; $k<count($title_hol_array); $k++) {
                    //公休・年休非表示
                    if ($title_hol_array[$k]["reason"] == "1" ||
                            $title_hol_array[$k]["reason"] == "24") {
                        continue;
                    }
                    $wk_td_id = "sum_gyo" . $cnt . "_" . ($k_idx + $k); //TDタグID
                    $wk = $hol_cnt_array[$i][$k];
                    echo("<td id=\"$wk_td_id\" class=\"td30\" rowspan=\"$span\" bgcolor=\"$wk_bgcolor\" style=\"$f_style4\">$wk</td>\n");
                }
            }
            //繰越公休残日数 start
            if ($total_disp_flg != "1" && $this->hol_remain_info->transfer_holiday_days_flg == "t") {
                //2段の場合、予定
                $wk_td_id = "hol_remain" . $cnt;    //TDタグID
                $wk = "";
                echo("<td id=\"$wk_td_id\" class=\"td30\" bgcolor=\"\" style=\"width:30;$f_style4\">$wk</td>\n");
            }
            //繰越公休残日数 end
            //TDタグID
            $wk_td_id = "name" . $cnt."_2";
            $color = ( $plan_array[$i]["sex"] == 1 ) ? 'blue' : 'black';
            // 氏名
            if ($staff_del_flg == "1") {
                $wk1 = $plan_array[$i]["staff_id"];
                $wk2 = $plan_array[$i]["staff_name"];
                echo("<td id=\"$wk_td_id\" class=\"td100a\" rowspan=\"$name_span\" bgcolor=\"$wk_color\" onClick=\"staffDelete('$wk1', '$wk2');\" style=\"color:$color;$f_style3\">");
            } else {
                //実績入力時は、タイムカード一括修正画面を開く 20100303
                if ($plan_results_flg == "3") {
                    $wk_emp_id = $plan_array[$i]["staff_id"];
                    $onclick_str = "openTimecardAll('$wk_emp_id');";
                } else {
                    $onclick_str = "";
                }
                echo("<td id=\"$wk_td_id\" class=\"td100a\" rowspan=\"$name_span\" bgcolor=\"$wk_color\" onClick=\"$onclick_str\" style=\"color:$color;$f_style3\">");           }
            $wk = $plan_array[$i]["staff_name"];
            echo("$wk</td>\n");
            echo("</tr> \n");

////////////////////////
            //-------------------------------------------------------------------------------
            // ２行目データ（勤務実績、勤務希望、当直、コメント）
            //-------------------------------------------------------------------------------
            if (($plan_results_flg == "2") || ($plan_results_flg == "3") ||
                    ($plan_hope_flg == "2") ||
                    ($plan_duty_flg == "2") ||
                    ($plan_comment_flg == "2")) {

                $cnt++;
                $this->showListSecondRow(
                        $i,
                        $cnt,
                        $plan_array,            //勤務シフト情報
                        $pattern_array,         //出勤パターン情報
                        $plan_results_flg,      //予実表示フラグ    （０：予定表示、２，３：予定／勤務実績表示）
                        $plan_hope_flg,         //勤務希望表示フラグ（０：予定表示、２：予定／勤務希望表示）
                        $plan_duty_flg,         //当直表示フラグ    （０：予定表示、２：予定／当直表示）
                        $plan_comment_flg,      //コメント表示フラグ（０：予定表示、２：予定／コメント表示）
                        $day_cnt,               //日数
                        $edit_start_day,        //入力表示の開始日（位置）
                        $edit_end_day,          //入力表示の終了日（位置）
                        $week_array,            //週情報
                        $title_gyo_array,       //行合計タイトル情報
                        $gyo_array,             //行合計情報
                        $emp_id,                //職員ＩＤ
                        $group_id,              //勤務シフトグループＩＤ
                        $pattern_id,            //出勤パターンＩＤ
                        $create_flg,            //登録権限フラグ（１：可能）
                        $total_disp_flg,        //集計 1:非表示
                        $four_week_line,        //４週８休チェックの線位置、0の場合はなし
                        $calendar_array,
                        $hol_dt_flg,            //休暇日数詳細 1:表示 "":非表示
                        $title_hol_array,       //休暇タイトル情報
                        $hol_cnt_array,         //休暇日数
                        $paid_hol_all_array,          //年休情報
                        $reason_2_array,
                        $arr_after_night_duty,
                        $officehours_array,
                        $reason_setting_flg,    //事由設定フラグ t:表示 f:非表示
                        $plan_copy_flg,         //予定コピーフラグ "1":予定コピー 20100125
                        $paid_hol_disp_flg,
                        $shift_type,            //シフト種別
                        $f_style3,
                        $wk_cursor2,
                        $span,
                        $list_kind,              //一覧種類
                        $extra_day,
                        $timecard_common_class,
                        $arr_weekday_flg
                );
            }

            $cnt++;
        }
    }
//////////////
    /*************************************************************************/
    //
    // 列警告
    //
    /*************************************************************************/
    function showError(
            $day_cnt,               //日数
            $edit_start_day,        //入力表示の開始日（位置）
            $edit_end_day,          //入力表示の終了日（位置）
            $week_array,            //週情報
            $title_gyo_array,       //行合計タイトル情報
            $gyo_array,             //行合計情報
            $title_retu_array,      //列合計タイトル情報
            $retu_array,            //列合計情報
            $warning_retu_array,    //警告列情報
            $check_flg,             //警告行表示フラグ（１：表示）
            $data_cnt,              //スタッフ数
            $total_disp_flg,        //集計 1:非表示
            $four_week_line,        //４週８休チェックの線位置、0の場合はなし
            $hol_dt_flg,            //休暇日数詳細 1:表示 "":非表示
            $title_hol_array,       //休暇タイトル情報
            $hol_cnt_array,         //休暇日数
            $plan_comment_flg,      //コメント表示フラグ
            $wk_group,              //シフトグループ情報
            $calendar_array,        //カレンダー情報
            $paid_hol_disp_flg,     //有給休暇残日数表示フラグ
            $paid_hol_array,        //有給休暇残日数
            $list_kind = ""         //一覧種類
    ){
        // 列警告
        if ($check_flg !== "1") {    //表示しない
            return;
        }
        echo("<tr> \n");

        // 番号（ブランク）
        echo("<td class=\"td40\"><input type=\"button\" value=\"更新\" onclick=\"update_no();\" style=\"width: 40px;\"></td>\n");

        //チーム表示
        if ($wk_group[0]["team_disp_flg"] == "t") {
            echo("<td class=\"td40\"></td>\n");  // チーム（ブランク）
        }

        if ($wk_group[0]["unit_disp_flg"] == "t") {
            echo("<td class=\"td100\"></td>\n"); //ユニット（ブランク）
        }
        echo("<td class=\"td100\">警告</td>\n");

        //職種表示
        if ($wk_group[0]["job_disp_flg"] == "t") {
            echo("<td class=\"td120\"></td>\n"); // 職種（ブランク）
        }
        //役職表示
        if ($wk_group[0]["st_disp_flg"] == "t") {
            echo("<td class=\"td60\"></td>\n");  // 役職（ブランク）
        }
        //雇用・勤務形態表示 20130219
        if ($wk_group[0]["es_disp_flg"] == "t") {
            echo("<td class=\"td60\"></td>\n");
        }

        //-------------------------------------------------------------------------------
        // 日ごとの警告
        //-------------------------------------------------------------------------------
        for ($k=1; $k<=$day_cnt; $k++) {
            // ４週８休チェックの線
            $f_style = "";
            if ($four_week_line != 0 && ($k == $four_week_line || $k == $four_week_line + 28)) {
                $f_style = "border-left:#ff0000 solid 2px;";
            }

            //表示チェック
            $wk_show_flg = "";
            if ((($edit_start_day <= $k) && ($edit_end_day >= $k)) ||
                    ($edit_start_day == $edit_end_day)) {
                $wk_show_flg = "1";
            }
            //表示
            if ($wk_show_flg == "1") {
                //TDタグID
                $wk_td_id = "error" . $k;
                //表示／入力で幅変更
                $wk_width = $this->default_width;
                if (($edit_start_day <= $k) && ($edit_end_day >= $k)) {
                    $wk_width = "100";
                    if ($edit_start_day == $edit_end_day) {
                        $wk_width = "200";
                    }
                } else {
                    if ($plan_comment_flg == "2") {
                        $wk_width = "40";
                    }
                }
                //表示
                $wk = $warning_retu_array[$k]["msg"];
                if ($wk == "") { $wk_back_color = ""; }
                if ($wk != "") { $wk_back_color = "#ff0000"; }

                //*曜日で背景色設定
                if ($wk_back_color=="" || strtoupper($wk_back_color)=="#FFFFFF" ){
                                //背景色を設定
                                    if ($wk_back_color=="" || strtoupper($wk_back_color)=="#FFFFFF" ){
                                        if($calendar_array[$k-1]["type"] == "6"){
                                            $wk_back_color = "#C8FFD5";
                                        }
                                        else if ($week_array[$k]["name"] == "土" || $calendar_array[$k-1]["type"] == "5") {
                                            $wk_back_color = "#E0FFFF";
                                        }
                                        else if ($week_array[$k]["name"] == "日" || $calendar_array[$k-1]["type"] == "4") {
                                            $wk_back_color = "#FFF0F5";
                                        }
                                    }
                }
                echo("<td id=\"$wk_td_id\" class=\"td$wk_width\" bgcolor=\"$wk_back_color\" style=\"$f_style\" onmouseover=\"showErrRetu($k,'$wk');\" onmouseout=\"hideMsg();\">");

                if ($warning_retu_array[$k]["msg_abb"]){
                    echo($warning_retu_array[$k]["msg_abb"]);
                }
                echo("</td> \n");
            }
        }
        //-------------------------------------------------------------------------------
        //警告（行）
        //-------------------------------------------------------------------------------
        echo("<td class=\"td20\"></td> \n");
        //-------------------------------------------------------------------------------
        // 個人日数の列合計
        //-------------------------------------------------------------------------------
        $this->showPersonDaysTotal(
                $title_hol_array,
                $hol_cnt_array,
                $gyo_array,
                $data_cnt,
                $title_gyo_array,
                $total_disp_flg,
                $hol_dt_flg,
                $paid_hol_disp_flg,
                $paid_hol_array[0],
                0    //インデックスには0を指定
        );
        //繰越公休残日数 start
        if ($this->hol_remain_info->transfer_holiday_days_flg == "t") {
            echo("<td class=\"td30\" style=\"border:0;\"></td>\n");
        }
        //繰越公休残日数 end
        echo("<td class=\"td100\" style=\"border:0;\"></td>\n");
        echo("</tr> \n");
    }

    /*************************************************************************/
    //
    // 列計
    //
    /*************************************************************************/
    function showTitleRetu(
            $day_cnt,           //日数
            $edit_start_day,    //入力表示の開始日（位置）
            $edit_end_day,      //入力表示の終了日（位置）
            $week_array,        //週情報
            $title_gyo_array,   //行合計タイトル情報
            $gyo_array,         //行合計情報
            $title_retu_array,  //列合計タイトル情報
            $retu_array,        //列合計情報
            $check_flg,         //警告行表示フラグ（１：表示）
            $data_cnt,          //スタッフ数
            $four_week_line,    //４週８休チェックの線位置、0の場合はなし
            $total_gyo_flg="1", // 2:2段 2以外:1段
            $total_disp_flg,    //集計 1:非表示
            $hol_dt_flg,        //休暇日数詳細 1:表示 "":非表示
            $title_hol_array,   //休暇タイトル情報
            $hol_cnt_array,     //休暇日数
            $plan_comment_flg="",   //コメント表示フラグ
            $wk_group,              //シフトグループ情報
            $calendar_array,    //カレンダー情報
            $paid_hol_disp_flg,    //有給休暇残日数表示フラグ
            $paid_hol_array    //有給休暇残日数
    ){
        for ($i=0; $i<count($title_retu_array); $i++) {

            echo("<tr> \n");
            echo("<td class=\"td40\"></td>\n");  // 番号（ブランク）
            //チーム表示
            if ($wk_group[0]["team_disp_flg"] == "t") {
                echo("<td class=\"td40\"></td>\n");  // チーム（ブランク）
            }
            //ユニット表示
            if ($wk_group[0]["unit_disp_flg"] == "t") {
                echo("<td class=\"td100\"></td>\n"); // ユニット（ブランク）
            }
            //-------------------------------------------------------------------------------
            // 名称
            //-------------------------------------------------------------------------------
            $wk = $title_retu_array[$i]["name"];
            echo("<td class=\"td100\" style=\"text-align:right;\">$wk&nbsp;</td> \n");
            //-------------------------------------------------------------------------------
            // 総合計
            //-------------------------------------------------------------------------------
            $wk_cnt = 0;
            for ($k=1; $k<=$day_cnt; $k++) {
                $wk_cnt += $retu_array[$k][$i];
            }
            //職種表示
            $i2 = $i + 1;
            if ($wk_group[0]["job_disp_flg"] == "t") {
                //役職表示がない場合は合計表示
                if ($wk_group[0]["st_disp_flg"] == "f") {
                    $i2 = $i + 1;
                    echo("<td id=\"emp_total_{$i2}\" class=\"td120\" style=\"text-align:right;font-size:13px;\" >$wk_cnt&nbsp;</td>");
                }
                else {
                    echo("<td class=\"td120\" style=\"text-align:right;font-size:13px;\" ></td>");
                }
            }
            //役職表示
            if ($wk_group[0]["st_disp_flg"] == "t") {
                //雇用・勤務形態表示がない場合 20130219
                //echo("<td id=\"emp_total_{$i2}\" class=\"td60\" style=\"text-align:right;font-size:13px;\">$wk_cnt&nbsp;</td> \n");
                if ($wk_group[0]["es_disp_flg"] != "t") {
                    $i2 = $i + 1;
                    echo("<td id=\"emp_total_{$i2}\" class=\"td60\" style=\"text-align:right;font-size:13px;\" >$wk_cnt&nbsp;</td>");
                }
                else {
                    echo("<td class=\"td60\" style=\"text-align:right;font-size:13px;\" ></td>");
                }
            }
            //雇用・勤務形態表示
            if ($wk_group[0]["es_disp_flg"] == "t") {
                echo("<td id=\"emp_total_{$i2}\" class=\"td60\" style=\"text-align:right;font-size:13px;\">$wk_cnt&nbsp;</td> \n");
            }
            //-------------------------------------------------------------------------------
            // 日ごとの合計
            //-------------------------------------------------------------------------------
            for ($k=1; $k<=$day_cnt; $k++) {
                //fontsize調整 20140502
                $wk_fontsize = (strlen($retu_array[$k][$i]) >= 5) ? "11px" : "13px";
                // ４週８休チェックの線
                if ($four_week_line != 0 && ($k == $four_week_line || $k == $four_week_line + 28)) {
                    $f_style = "border-left:#ff0000 solid 2px;font-size:{$wk_fontsize};";
                } else {
                    $f_style = "font-size:{$wk_fontsize};";
                }

                //表示チェック
                $wk_show_flg = "";
                if ((($edit_start_day <= $k) && ($edit_end_day >= $k)) ||
                        ($edit_start_day == $edit_end_day)) {
                    $wk_show_flg = "1";
                }
                //表示
                if ($wk_show_flg == "1") {
                    //TDタグID
                    $wk = $i + 1;
                    $wk_td_id = "summary" . $wk . "_" . $k;
                    //表示／入力で幅変更
                    $wk_width = $this->default_width;
                    if (($edit_start_day <= $k) && ($edit_end_day >= $k)) {
                        $wk_width = "100";
                        if ($edit_start_day == $edit_end_day) {
                            $wk_width = "200";
                        }
                    } else {
                        if ($plan_comment_flg == "2") {
                            $wk_width = "40";
                        }
                    }
                    //表示
                    $wk = $retu_array[$k][$i];

                    //*曜日で背景色設定
                                        //背景色を設定
                                        $wk_back_color="";
                                        //曜日で背景色設定
                                        if ($wk_back_color=="" || strtoupper($wk_back_color)=="#FFFFFF" ){
                                            if($calendar_array[$k-1]["type"] == "6"){
                                                $wk_back_color = "#C8FFD5";
                                            }
                                            else if ($week_array[$k]["name"] == "土" || $calendar_array[$k-1]["type"] == "5") {
                                                $wk_back_color = "#E0FFFF";
                                            }
                                            else if ($week_array[$k]["name"] == "日" || $calendar_array[$k-1]["type"] == "4") {
                                                $wk_back_color = "#FFF0F5";
                                            }
                                        }

                    echo("<td id=\"$wk_td_id\" class=\"td$wk_width sum_retu day{$k}\" bgcolor=\"$wk_back_color\" style=\"".$f_style."\">$wk</td>\n");
                }
            }
            //-------------------------------------------------------------------------------
            //チェック結果
            //-------------------------------------------------------------------------------
            if ($check_flg == "1"){
                if ($total_gyo_flg == "2" && $i == 0) {
                    echo("<td class=\"td20\"></td> \n");
                    // 個人日数の列合計
                    $this->showPersonDaysTotal(
                            $title_hol_array,
                            $hol_cnt_array,
                            $gyo_array,
                            $data_cnt,
                            $title_gyo_array,
                            $total_disp_flg,
                            $hol_dt_flg,
                            $paid_hol_disp_flg,
                            $paid_hol_array[$i],
                            1    //0以外を渡す必要があるので、1を渡しておく
                    );
                }
            } else {
                if (($total_gyo_flg != "2" && $i == 0) || ($total_gyo_flg == "2" && $i <= 1)) {
                    // 個人日数の列合計
                    $this->showPersonDaysTotal(
                            $title_hol_array,
                            $hol_cnt_array,
                            $gyo_array,
                            $data_cnt,
                            $title_gyo_array,
                            $total_disp_flg,
                            $hol_dt_flg,
                            $paid_hol_disp_flg,
                            $paid_hol_array[$i],
                            $i
                    );
                }
            }
            //繰越公休残日数 start
            //レイアウト調整、firefoxで不要な罫線が表示される
            //警告行があり1行の場合1行目から、2行の場合2行目から、警告行がない場合3行目から見えないセル出力
            if (($check_flg == "1" && $total_gyo_flg == "1" && $i >= 0) ||
                    ($check_flg == "1" && $total_gyo_flg == "2" && $i >= 1) ||
                    ($check_flg != "1" && $i>=2)) {
                echo("<td class=\"td20\" style=\"border:0;\"></td>\n");
                foreach ($title_gyo_array as $wk_key => $dummy) {
                    //集計列幅対応 class=\"td30\"削除 20140502
                    echo("<td id=\"day_total_{$i}_{$wk_key}\" style=\"border:0;\"></td>\n");
                }
                if ($paid_hol_disp_flg === "t") {
                    echo("<td class=\"td30\" style=\"border:0;\"></td>\n");
                }
                if ($this->hol_remain_info->transfer_holiday_days_flg == "t") {
                    echo("<td class=\"td30\" style=\"border:0;\"></td>\n");
                }
                echo("<td class=\"td100\" style=\"border:0;\"></td>\n");
            }
            //繰越公休残日数 end
            echo("</tr> \n");
        }
    }
    /*************************************************************************/
    //
    // 月平均夜勤時間
    //
    /*************************************************************************/
    function showNightTime(
            $recomputation_flg,    //１ヵ月計算４週計算フラグ（１：１ヵ月計算、２：４週計算）
            $draft_reg_flg,        //下書きデータ登録状態
            $night_start_day,      //４週計算用（開始日）
            $date_ymd1,            //「計算」開始年月日
            $duty_ym,              //勤務年月
            $limit_time,           //月平均夜勤時間上限
            $day_cnt,              //日数
            $standard_id,          //施設基準ＩＤ
            $group_id              //グループＩＤ
    ) {
        //ブラウザにより、幅とマージンを調整
        $agent = getenv("HTTP_USER_AGENT");
        if (preg_match("/MSIE/", $agent)) {    //IE
            $width = "343";
            $margin_left = "70";
        } else {    //IE以外
            $width = "342";
            $margin_left = "90";
        }

        echo("<tr height=\"50\">\n");
        echo("<td width=\"{$width}\" align=\"left\" style=\"word-break:break-all;\">月平均夜勤時間<br>");

        ///-----------------------------------------------------------------------------
        // １ヵ月計算、４週計算
        ///-----------------------------------------------------------------------------
        $wk1 = "";
        $wk2 = "";
        if (($recomputation_flg == "1") || ($recomputation_flg == "")){ $wk1 = "checked"; }
        if ($recomputation_flg == "2") { $wk2 = "checked"; }
        echo("<label><input type=\"radio\" name=\"recomputation_flg\" value=\"1\" $wk1 >１ヵ月計算</label>\n");
        echo("<label><input type=\"radio\" name=\"recomputation_flg\" value=\"2\" $wk2 >４週計算</label>&nbsp;&nbsp;&nbsp;開始日\n");

        ///-----------------------------------------------------------------------------
        // 年
        ///-----------------------------------------------------------------------------
        extract($date_ymd1);    //「計算」開始年月日を変数化($date_y1, $date_m1, $date_d1)
        echo("<input type=\"hidden\" name=\"date_y1\" id=\"date_y1\" value=\"$date_y1\">");

        ///-----------------------------------------------------------------------------
        // 月
        ///-----------------------------------------------------------------------------
        echo("<input type=\"hidden\" name=\"date_m1\" id=\"date_m1\" value=\"$date_m1\">\n");

        ///-----------------------------------------------------------------------------
        // 日
        ///-----------------------------------------------------------------------------
        $day_cnt = $this->obj->days_in_month($date_y1, $date_m1);
        echo("<select name=\"date_d1\" id=\"date_d1\" disabled>\n");
        echo("<option value=\"\">\n");
        for ($i = 1; $i <= $day_cnt; $i++) {
            $val = sprintf("%02d", $i);
            echo("<option value=\"$val\"");
            if ($i == $date_d1) {
                echo(" selected");
            }
            echo(">$val</option>\n");
        }
        echo("</select>\n");

        ///-----------------------------------------------------------------------------
        // カレンダー
        ///-----------------------------------------------------------------------------
        echo("<div id=\"cal1Container\" style=\"position:absolute;display:none;z-index:10000;\"></div>\n");
        echo("<img id=\"calendar_img\" src=\"img/calendar_link.gif\" style=\"cursor:pointer;\" onclick=\"\">\n");

        ///-----------------------------------------------------------------------------
        // 「計算」ボタン
        ///-----------------------------------------------------------------------------
        echo("<input type=\"button\" value=\"再計算\" onclick=\"compNightTime();\">\n");
        echo("</td>\n");

        //施設基準情報取得（様式９用　履歴）
        $history_array = $this->obj->get_duty_shift_institution_history_array($standard_id, $duty_ym["duty_yyyy"], $duty_ym["duty_mm"]);

        //様式９の値を算出
        $values = $this->calcReportValue(
                $recomputation_flg,    //１ヵ月計算４週計算フラグ（１：１ヵ月計算、２：４週計算）
                $draft_reg_flg,        //下書きデータ登録状態（１：下書き、２：登録）
                $night_start_day,      //４週計算用（開始日）
                $date_ymd1,            //「計算」開始年月日
                $duty_ym,              //勤務年月
                $day_cnt,              //日数
                $standard_id,          //施設基準ＩＤ
                $group_id,             //グループＩＤ
                $history_array         //施設基準情報（履歴）
        );

        ///-----------------------------------------------------------------------------
        // 月平均夜勤時間
        ///-----------------------------------------------------------------------------
        $wk_back_color = "";
        if ($values["night_sumtime"] > $limit_time) {
            //月平均夜勤時間が上限を超えていた場合、背景を赤にする
            $wk_back_color = "#ff0000";
        }
        echo("<td width=\"50\" align=\"right\" rowspan=\"1\" bgcolor=\"$wk_back_color\">");
        echo($values["night_sumtime"]);
        echo("</td>\n");
        echo("<td width=\"40\" align=\"left\" rowspan=\"1\">時間</td>\n");
        echo("</tr>\n");

        ///-----------------------------------------------------------------------------
        // 看護配置数・看護補助配置数
        ///-----------------------------------------------------------------------------
        echo("<tr>\n");
        echo("<td colspan=\"3\">\n");
        echo("<div class=\"nurse\">\n");
        echo("１日あたり看護配置数（実績） {$values["nurse_actual_assign"]}人<br>\n");
        echo("１日看護配置数（基準） {$values["nurse_standard_assign"]}人\n");
        echo("</div>\n");
        echo("<div class=\"nurse\">\n");
        echo("１日あたり看護補助配置数（実績） {$values["assistance_actual_assign"]}人<br>\n");
        echo("１日看護補助配置数（基準） {$values["assistance_standard_assign"]}人</div>\n");

        ///-----------------------------------------------------------------------------
        // 病棟単位の勤務計画表出力ボタン
        ///-----------------------------------------------------------------------------
        echo("<div style=\"margin-left: {$margin_left}px; margin-top: 8px; margin-bottom: 15px;\">\n");
        if ($history_array[0]["report_kbn"]) {    // 施設基準が未登録で、届出区分が取得できない場合はボタンを表示しない
            echo("<input type=\"button\" value=\"看護職員表作成\" onclick=\"makeExcelNurseShift('$night_start_day', '$standard_id', '{$history_array[0]["report_kbn"]}', 1);\">");
            echo("&nbsp;&nbsp;<input type=\"button\" value=\"看護補助者表作成\" onclick=\"makeExcelNurseShift('$night_start_day', '$standard_id', '{$history_array[0]["report_kbn"]}', 2);\">");
        }
        echo("</div>\n");
        echo("</td>\n");
        echo("</tr>\n");
    }

//-------------------------------------------------------------------------------------------------------------------------
// 関数一覧（ＨＩＤＤＥＮ）
//-------------------------------------------------------------------------------------------------------------------------
    /*************************************************************************/
    //
    // 勤務シフ作成ト／希望／実績共通１
    //
    /*************************************************************************/
    function showHidden_1(
        $data_cnt,              //スタッフ数
        $day_cnt,               //日数
        $edit_start_day,        //入力表示の開始日（位置）
        $edit_end_day,          //入力表示の終了日（位置）
        $title_gyo_cnt          //行タイトル数
        ){
        echo("<input type=\"hidden\" name=\"data_cnt\" value=\"$data_cnt\">\n");
        echo("<input type=\"hidden\" name=\"day_cnt\" value=\"$day_cnt\">\n");

        echo("<input type=\"hidden\" name=\"edit_start_day\" value=\"$edit_start_day\">\n");
        echo("<input type=\"hidden\" name=\"edit_end_day\" value=\"$edit_end_day\">\n");

        echo("<input type=\"hidden\" name=\"title_gyo_cnt\" value=\"$title_gyo_cnt\">\n");

        echo("<input type=\"hidden\" name=\"week_index\" value=\"\">\n");
    }
    /*************************************************************************/
    //
    // 勤務シフ作成ト／希望／実績共通２
    //
    /*************************************************************************/
    function showHidden_2($rslt_flg,        //表示ＨＩＤＤＥＮ判定フラグ（""：予定、１：実績）
        $plan_array,        //勤務シフト情報
        $calendar_array,    //カレンダー情報
        $session,           //セッション
        $day_cnt,           //日数
        $plan_results_flg,  //予実表示フラグ    （０：予定表示、２，３：予定／勤務実績表示）
        $plan_hope_flg,     //勤務希望表示フラグ（０：予定表示、２：予定／勤務希望表示）
        $plan_duty_flg,     //当直表示フラグ    （０：予定表示、２：予定／当直表示）
        $plan_comment_flg,  //コメント表示フラグ（０：予定表示、２：予定／コメント表示）
        $group_id,          //勤務シフトグループＩＤ
        $duty_yyyy,         //勤務年
        $duty_mm,           //勤務月
        $edit_start_day,    //入力表示の開始日（位置）
        $edit_end_day,      //入力表示の終了日（位置）
        $add_staff_cnt,     //追加スタッフ数
        $add_staff_id,      //追加スタッフＩＤ
        $del_staff_id,      //削除スタッフＩＤ
        $pattern_id,        //出勤パターンＩＤ
        $draft_flg,         //下書きフラグ（１：下書き）
        $check_flg,         //警告行表示フラグ（１：表示）
        $fullscreen_flg,    //全画面フラグ（１：全画面）
        $reason_setting_flg //事由設定フラグ t:表示 f:非表示
        ) {
        echo("<input type=\"hidden\" name=\"session\" value=\"$session\">\n");

        echo("<input type=\"hidden\" name=\"plan_results_flg\" value=\"$plan_results_flg\">\n");
        echo("<input type=\"hidden\" name=\"plan_hope_flg\" value=\"$plan_hope_flg\">\n");
        echo("<input type=\"hidden\" name=\"plan_duty_flg\" value=\"$plan_duty_flg\">\n");
        echo("<input type=\"hidden\" name=\"plan_comment_flg\" value=\"$plan_comment_flg\">\n");

        echo("<input type=\"hidden\" name=\"cause_group_id\" id=\"cause_group_id\" value=\"$group_id\">\n"); //id追加 20100728
        echo("<input type=\"hidden\" name=\"cause_duty_yyyy\" id=\"cause_duty_yyyy\" value=\"$duty_yyyy\">\n"); //id追加 繰越公休残日数対応
        echo("<input type=\"hidden\" name=\"cause_duty_mm\" id=\"cause_duty_mm\" value=\"$duty_mm\">\n"); //id追加 繰越公休残日数対応
        //全画面時は出力する 20120321
        if ($fullscreen_flg == "1") {
            echo("<input type=\"hidden\" name=\"duty_yyyy\" value=\"$duty_yyyy\">\n");
        }
        echo("<input type=\"hidden\" name=\"duty_mm\" value=\"$duty_mm\">\n");

        echo("<input type=\"hidden\" name=\"add_staff_cnt\" value=\"$add_staff_cnt\">\n");
        echo("<input type=\"hidden\" name=\"add_staff_id\" value=\"$add_staff_id\">\n");
        echo("<input type=\"hidden\" name=\"del_staff_id\" value=\"$del_staff_id\">\n");

        echo("<input type=\"hidden\" name=\"pattern_id\" value=\"$pattern_id\">\n");
        echo("<input type=\"hidden\" name=\"draft_flg\" value=\"$draft_flg\">\n");
        echo("<input type=\"hidden\" name=\"check_flg\" value=\"$check_flg\">\n");

        //-------------------------------------------------------------------------------
        /// 勤務シフトデータ
        //-------------------------------------------------------------------------------
        for ($i=0; $i<count($plan_array); $i++) {
            //-------------------------------------------------------------------------------
            //スタッフ
            //-------------------------------------------------------------------------------
            $wk = $plan_array[$i]["staff_id"];
            echo("<input type=\"hidden\" name=\"staff_id[$i]\" value=\"$wk\" >\n");
            echo("<input type=\"hidden\" name=\"job_id[$i]\" value=\"{$plan_array[$i]["job_id"]}\" >\n");

            //-------------------------------------------------------------------------------
            //出勤グループＩＤ
            //-------------------------------------------------------------------------------
            for ($k=1; $k<=$day_cnt; $k++) {
                //-------------------------------------------------------------------------------
                //入力項目（予定または実績）
                //-------------------------------------------------------------------------------
                if ($rslt_flg == "") {
                    $wk2 = $plan_array[$i]["pattern_id_$k"];
                    $wk3 = "pattern_id_$i" . "_" . $k;
                } else {
                    $wk2 = $plan_array[$i]["rslt_pattern_id_$k"];
                    $wk3 = "rslt_pattern_id_$i" . "_" . $k;
                }
                echo("<input type=\"hidden\" name=\"$wk3\" value=\"$wk2\" >\n");
            }
            //-------------------------------------------------------------------------------
            //出勤パターンＩＤ　＋　事由
            //-------------------------------------------------------------------------------
            for ($k=1; $k<=$day_cnt; $k++) {
                //-------------------------------------------------------------------------------
                //入力項目（予定または実績）
                //-------------------------------------------------------------------------------
                $wk_flg = "";
                if (($edit_start_day <= $k) && ($edit_end_day >= $k)) {
                    //入力データで既に設定されているため、ここでは未設定
                    //ただし他病棟のデータは設定
                    if ($rslt_flg == "") {
                        if (/*$plan_array[$i]["pattern_id_$k"] != $pattern_id ||*/
                            ($plan_array[$i]["assist_group_$k"] != "" &&
                                    $plan_array[$i]["assist_group_$k"] != $group_id)){
                            $wk_flg = "1";
                        }
                    } else {
                        if (/*$plan_array[$i]["rslt_pattern_id_$k"] != $pattern_id ||*/
                            ($plan_array[$i]["assist_group_$k"] != "" &&
                                    $plan_array[$i]["assist_group_$k"] != $group_id)){
                            $wk_flg = "1";
                        }
                    }
                } else {
                    $wk_flg = "1";
                }
                if ($wk_flg == "1") {
                    if ($rslt_flg == "") {
                        $wk2 = sprintf("%02d%02d", $plan_array[$i]["atdptn_ptn_id_$k"], $plan_array[$i]["reason_$k"]);
                        $wk3 = "atdptn_ptn_id_$i" . "_" . $k;
                    } else {
                        $wk2 = sprintf("%02d%02d", $plan_array[$i]["rslt_id_$k"], $plan_array[$i]["rslt_reason_$k"]);
                        $wk3 = "rslt_id_$i" . "_" . $k;
                    }
                    //出勤パターンＩＤ　＋　事由
                    echo("<input type=\"hidden\" class=\"atdptn\" name=\"$wk3\" value=\"$wk2\" >\n");
                }
            }
            //-------------------------------------------------------------------------------
            //コメント
            //-------------------------------------------------------------------------------
            for ($k=1; $k<=$day_cnt; $k++) {
                $wk_flg = "";
                //入力データで既に設定されているため、ここでは未設定
                if ($plan_comment_flg != "2") {
                    $wk_flg = "1";
                }
                if ($wk_flg == "1") {
                    $wk2 = $plan_array[$i]["comment_$k"];
                    $wk3 = "comment_$i" . "_" . $k;
                    //コメント
                    if ($wk2 != "") {
                        echo("<input type=\"hidden\" name=\"$wk3\" value=\"$wk2\" >\n");
                    }
                }
            }
            //-------------------------------------------------------------------------------
            //事由（午前有給／午後有給）
            //-------------------------------------------------------------------------------
            for ($k=1; $k<=$day_cnt; $k++) {
                $wk_flg = "";
                if (($edit_start_day <= $k) && ($edit_end_day >= $k)) {
                    //入力データで既に設定されているため、ここでは未設定
                    if ($edit_start_day != $edit_end_day) {
                        $wk_flg = "1";
                    }
                    //事由設定フラグが非表示の場合
                    if ($reason_setting_flg != "t") {
                        $wk_flg = "1";
                    }
                    if (/*$plan_array[$i]["pattern_id_$k"] != $pattern_id ||*/
                        ($plan_array[$i]["assist_group_$k"] != "" &&
                                $plan_array[$i]["assist_group_$k"] != $group_id)){
                        $wk_flg = "1";
                    }
                } else {
                    $wk_flg = "1";
                }
                if ($wk_flg == "1") {
                    if ($rslt_flg == "") {
                        $wk2 = $plan_array[$i]["reason_2_$k"];
                        $wk3 = "reason_2_$i" . "_" . $k;
                    } else {
                        $wk2 = $plan_array[$i]["rslt_reason_2_$k"];
                        $wk3 = "rslt_reason_2_$i" . "_" . $k;
                    }
                    //事由
                    echo("<input type=\"hidden\" name=\"$wk3\" value=\"$wk2\" >\n");
                }
            }
            // シフト希望分出力 2008/6/12
            // 項目名称はrslt_*を使用
            if ($plan_hope_flg == "2") {

                for ($k=1; $k<=$day_cnt; $k++) {
                    $wk2 = $plan_array[$i]["rslt_pattern_id_$k"];
                    $wk3 = "rslt_pattern_id_$i" . "_" . $k;
                    echo("<input type=\"hidden\" name=\"$wk3\" value=\"$wk2\" >\n");
                }

                for ($k=1; $k<=$day_cnt; $k++) {

                    $wk_flg = "";
                    if (($edit_start_day <= $k) && ($edit_end_day >= $k)) {
                        //入力データで既に設定されているため、ここでは未設定
                        //ただし他病棟のデータは設定
                        if (/*$plan_array[$i]["rslt_pattern_id_$k"] != $pattern_id ||*/
                            ($plan_array[$i]["rslt_assist_group_$k"] != "" &&
                                    $plan_array[$i]["rslt_assist_group_$k"] != $group_id)){
                            $wk_flg = "1";
                        }
                    } else {
                        $wk_flg = "1";
                    }

                    if ($wk_flg == "1") {
                        $wk2 = sprintf("%02d%02d", $plan_array[$i]["rslt_id_$k"], $plan_array[$i]["rslt_reason_$k"]);
                        $wk3 = "rslt_id_$i" . "_" . $k;
                        //出勤パターンＩＤ　＋　事由
                        echo("<input type=\"hidden\" name=\"$wk3\" value=\"$wk2\" >\n");
                    }
                }
                //事由
                for ($k=1; $k<=$day_cnt; $k++) {
                    $wk_flg = "";
                    if (($edit_start_day <= $k) && ($edit_end_day >= $k)) {
                        //入力データで既に設定されているため、ここでは未設定
                        if ($edit_start_day != $edit_end_day) {
                            $wk_flg = "1";
                        }
                        //事由設定フラグが非表示の場合
                        if ($reason_setting_flg != "t") {
                            $wk_flg = "1";
                        }
                        if (/*$plan_array[$i]["rslt_pattern_id_$k"] != $pattern_id ||*/
                            ($plan_array[$i]["rslt_assist_group_$k"] != "" &&
                                    $plan_array[$i]["rslt_assist_group_$k"] != $group_id)){
                            $wk_flg = "1";
                        }
                    } else {
                        $wk_flg = "1";
                    }
                    if ($wk_flg == "1") {
                        $wk2 = $plan_array[$i]["rslt_reason_2_$k"];
                        $wk3 = "rslt_reason_2_$i" . "_" . $k;
                        //事由
                        echo("<input type=\"hidden\" name=\"$wk3\" value=\"$wk2\" >\n");
                    }
                }
            }

        }
        //-------------------------------------------------------------------------------
        //応援追加グループデータ
        //-------------------------------------------------------------------------------
        for ($i=0; $i<count($plan_array); $i++) {
            $wk_str = "";
            for ($k=1; $k<=$day_cnt; $k++) {
                if ($k > 1) {
                    $wk_str .= ",";
                }
                $wk_str .= $plan_array[$i]["assist_group_$k"];
            }
            $wk3 = "assist_group_$i";
            echo("<input type=\"hidden\" name=\"$wk3\" value=\"$wk_str\" >\n");
            if ($plan_hope_flg == "2") {
                $wk_str = "";
                for ($k=1; $k<=$day_cnt; $k++) {
                    if ($k > 1) {
                        $wk_str .= ",";
                    }
                    $wk_str .= $plan_array[$i]["rslt_assist_group_$k"];
                }
                $wk3 = "rslt_assist_group_$i";
                echo("<input type=\"hidden\" name=\"$wk3\" value=\"$wk_str\" >\n");

            }

        }

        //-------------------------------------------------------------------------------
        /// 年月日（カレンダーより）
        //-------------------------------------------------------------------------------
        $k = count($calendar_array) - 1;
        $duty_start_date = $calendar_array[0]["date"];
        $duty_end_date = $calendar_array[$k]["date"];
        echo("<input type=\"hidden\" name=\"duty_start_date\" value=$duty_start_date >\n");
        echo("<input type=\"hidden\" name=\"duty_end_date\" value=$duty_end_date >\n");
        // 全画面フラグ
        echo("<input type=\"hidden\" name=\"fullscreen_flg\" value=\"$fullscreen_flg\" >\n");
    }

    // 2行目のデータを表示する
    function showListSecondRow(
            $i,                     //処理対象の勤務シフト情報配列インデックス
            $cnt,
            $plan_array,            //勤務シフト情報
            $pattern_array,         //出勤パターン情報
            $plan_results_flg,      //予実表示フラグ    （０：予定表示、２，３：予定／勤務実績表示）
            $plan_hope_flg,         //勤務希望表示フラグ（０：予定表示、２：予定／勤務希望表示）
            $plan_duty_flg,         //当直表示フラグ    （０：予定表示、２：予定／当直表示）
            $plan_comment_flg,      //コメント表示フラグ（０：予定表示、２：予定／コメント表示）
            $day_cnt,               //日数
            $edit_start_day,        //入力表示の開始日（位置）
            $edit_end_day,          //入力表示の終了日（位置）
            $week_array,            //週情報
            $title_gyo_array,       //行合計タイトル情報
            $gyo_array,             //行合計情報
            $emp_id,                //職員ＩＤ
            $group_id,              //勤務シフトグループＩＤ
            $pattern_id,            //出勤パターンＩＤ
            $create_flg,            //登録権限フラグ（１：可能）
            $total_disp_flg,        //集計 1:非表示
            $four_week_line,        //４週８休チェックの線位置、0の場合はなし
            $calendar_array,        //カレンダー情報
            $hol_dt_flg,            //休暇日数詳細 1:表示 "":非表示
            $title_hol_array,       //休暇タイトル情報
            $hol_cnt_array,         //休暇日数
            $paid_hol_all_array,    //有給休暇残日数
            $reason_2_array,
            $arr_after_night_duty,
            $officehours_array,
            $reason_setting_flg,    //事由設定フラグ t:表示 f:非表示
            $plan_copy_flg,         //予定コピーフラグ "1":予定コピー 20100125
            $paid_hol_disp_flg,
            $shift_type,            //シフト種別
            $f_style3,              //３人毎にセル下側の線を太くするのスタイル
            $wk_cursor2,
            $span,
            $list_kind,             //一覧種類
            $extra_day,
            $timecard_common_class,
            $arr_weekday_flg
            )
    {
        //個人別勤務時間 20140318
        $wk_start_date = $calendar_array[0]["date"];
        $arr_empcond_officehours = $timecard_common_class->get_arr_empcond_officehours($plan_array[$i]["staff_id"], $wk_start_date);

        // 当日
        $today = Date("Ymd");
        echo("<tr height=\"\" class=\"$shift_type\"> \n");
        //-------------------------------------------------------------------------------
        //勤務状況（実績）
        //-------------------------------------------------------------------------------
        $ex_cursor2 = $wk_cursor2;
        for ($k=1; $k<=$day_cnt; $k++) {

            $wk_cursor2 = ($k <= $extra_day) ? "" : $ex_cursor2; //表示期間外表示 20130307

//*                 // ４週８休チェックの線
            if ($four_week_line != 0 && ($k == $four_week_line || $k == $four_week_line + 28)) {
//*                     $f_style = " style=\"border-left:#ff0000 solid 2px;$f_style3 $wk_cursor2\"";
                $f_style = "border-left:#ff0000 solid 2px;".$f_style3.$wk_cursor2;
//*                 } else {
//*                     if ($k != 1) {
//*                         //土日の線を設定 20110721
//*                         if ($week_array[$k]["name"] == "日"){
//*                             $f_style = " style=\"border-left:#008000 solid 2px;$f_style3 $wk_cursor2\"";
//*                         } else if ($week_array[$k]["name"] == "月") {
//*                             $f_style = " style=\"border-left:#ff00ff solid 2px;$f_style3 $wk_cursor2\"";
//*                         } else if ($week_array[$k]["name"] == "木") {
//*                             $f_style = " style=\"border-left:#5279a5 solid 2px;$f_style3 $wk_cursor2\"";
//*                         } else {
//*                             $f_style = " style=\"$f_style3 $wk_cursor2\"";
//*                         }
            } else {
//*                         $f_style = " style=\"$f_style3 $wk_cursor2\"";
                $f_style = $f_style3.$wk_cursor2;
//*                     }
            }
            //-------------------------------------------------------------------------------
            //表示チェック
            //-------------------------------------------------------------------------------
            $wk_show_flg = "";
            if ((($edit_start_day <= $k) && ($edit_end_day >= $k)) ||
                    ($edit_start_day == $edit_end_day)) {
                $wk_show_flg = "1";
            }
            //-------------------------------------------------------------------------------
            // ★★★ 表示しない場合はスルー 2014/10 if文ネストを少なくしたい為に変更
            //-------------------------------------------------------------------------------
            if ($wk_show_flg != "1") continue;


            $wk_flg = "";
            if (($edit_start_day <= $k) && ($edit_end_day >= $k)) {
                $wk_flg = "1";
            }
            //-------------------------------------------------------------------------------
            //希望の場合
            //-------------------------------------------------------------------------------
            if ($emp_id != "") {
                //指定職員以外は表示のみ
                if ($emp_id != $plan_array[$i]["staff_id"]) {
                    $wk_flg = "";
                }
            }
            //-------------------------------------------------------------------------------
            //TDタグID
            //-------------------------------------------------------------------------------
            $wk_td_id = "data" . $cnt . "_" . $k;
            //-------------------------------------------------------------------------------
            //表示／入力で幅変更
            //-------------------------------------------------------------------------------
            //コメント時の幅を変更 20090821
//              $wk_width = "20";
            $wk_width = $this->default_width;
            if (($edit_start_day <= $k) && ($edit_end_day >= $k)) {
                $wk_width = "100";
                if (($edit_start_day == $edit_end_day) && ($list_kind !== "create_shift")) {
                    $wk_width = "200";
                }
            } else {
                if ($plan_comment_flg == "2") {
                    $wk_width = "40";
                }
            }
            if ($list_kind === "create_shift"  && (($edit_start_day > $k) || ($edit_end_day < $k))) {    // 勤務シフト作成画面の表示
                $input_comment_size = "3";    // コメント入力エリアのサイズ
            } else {
                $input_comment_size = "10";
            }
            //-------------------------------------------------------------------------------
            //他病棟の場合、表示のみ
            //-------------------------------------------------------------------------------
//              if ($plan_array[$i]["rslt_pattern_id_$k"] != $pattern_id) {
//                  $wk_flg = "";
//              }
            //応援追加の場合
            //シフト希望表示時の条件追加
            $wk_assist_flg = "";
            if (($plan_hope_flg != "2" &&
                        $plan_array[$i]["assist_group_$k"] != "" &&
                        $plan_array[$i]["assist_group_$k"] != $group_id) ||
                    ($plan_hope_flg == "2" &&
                        $plan_array[$i]["rslt_assist_group_$k"] != "" &&
                        $plan_array[$i]["rslt_assist_group_$k"] != $group_id)) {
                $wk_flg = "";
                $wk_assist_flg = "1";
            }
            //-------------------------------------------------------------------------------
            //実績入力画面以外の場合、表示のみ
            //-------------------------------------------------------------------------------
            // 2008/6/12 管理者はシフト希望も入力可とする
            if ($plan_results_flg != "3" && $plan_hope_flg != "2") {
                $wk_flg = "";
            }
            //-------------------------------------------------------------------------------
            //入力又は表示：まず入力の場合
            //-------------------------------------------------------------------------------
            if ($wk_flg == "1") {
                //-------------------------------------------------------------------------------
                //勤務実績（選択入力）
                //-------------------------------------------------------------------------------
//*                 echo("<td id=\"$wk_td_id\" width=\"$wk_width\" align=\"center\" colspan=\"1\" $f_style><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
                echo("<td id=\"$wk_td_id\" class=\"td$wk_width\" colspan=\"1\" style=\"$f_style\">");
                // 項目名、シフト希望用
                $wk = "rslt_id_$i" . "_" . $k;
                $wk_tmcd_gid = $plan_array[$i]["rslt_pattern_id_$k"];
                echo("<select name=\"$wk\" onchange=\"setReason($i, $k, $wk_tmcd_gid, this.value, 2);setRsltUpdFlg($i);\"$shift_select_class>");
                echo("<option value=\"0000\" style=\"background-color:$wk_bgcolor;\">__ \n");
                for ($m=0; $m<count($pattern_array); $m++) {
                    // 出勤表の休暇種別等画面の非表示を確認
                    if($pattern_array[$m]["reason_display_flag"] == "f") {
                        continue;
                    }
                    // 出勤表のパターン名＞出勤パターン名画面の非表示を確認 20120830
                    if($pattern_array[$m]["display_flag"] == "f") {
                        continue;
                    }
                    //ＩＤ
                    $wk_id = sprintf("%02d%02d", $pattern_array[$m]["atdptn_ptn_id"], $pattern_array[$m]["reason"]);
                    echo("<option value=\"$wk_id\"");
                    if ($pattern_array[$m]["reason"] != "") {
                        if (($plan_array[$i]["rslt_id_$k"] == $pattern_array[$m]["atdptn_ptn_id"]) &&
                                ($plan_array[$i]["rslt_reason_$k"] == $pattern_array[$m]["reason"])) {
                            echo(" selected");
                        }
                    } else {
                        if ($plan_array[$i]["rslt_id_$k"] == $pattern_array[$m]["atdptn_ptn_id"]) {
                            echo(" selected");
                        }
                    }
                    //名称
                    $wk_name = $pattern_array[$m]["atdptn_ptn_name"];
                    // 半有半公等
                    if (($pattern_array[$m]["reason"] >= "45" && //44
                            $pattern_array[$m]["reason"] <= "47") ) {
                        $wk_name = $pattern_array[$m]["reason_name"];
                        // 表示変更
                        for ($r_idx=0; $r_idx<count($reason_2_all_array); $r_idx++) {
                            if ($reason_2_all_array[$r_idx]["id"] == $pattern_array[$m]["reason"]) {
                                $wk_name = $reason_2_all_array[$r_idx]["name"];
                                break;
                            }
                        }
                    } else
                        if ($pattern_array[$m]["reason_name"] != "") {
                            $wk_name .= "(" . $pattern_array[$m]["reason_name"] . ")";
                        }
                    echo(" style=\"background-color:$wk_bgcolor;\">$wk_name \n");
                }
                echo("</select> \n");
                //-------------------------------------------------------------------------------
                //勤務実績（事由：午前有給／午後有給）
                //-------------------------------------------------------------------------------
                //事由設定フラグが表示の場合
                if ($edit_start_day == $edit_end_day &&
                    $reason_setting_flg == "t") {
                    $wk = "rslt_reason_2_$i" . "_" . $k;
                    echo("<select name=\"$wk\" onchange=\"setRsltUpdFlg($i);\">");
                    echo("<option value=\"\">__ \n");
                    for ($m=0; $m<count($reason_2_array); $m++) {
                        $wk_id = $reason_2_array[$m]["id"];
                        echo("<option value=\"$wk_id\"");
                        if ($plan_array[$i]["rslt_reason_2_$k"] == $wk_id) {
                            echo(" selected");
                        }
                        //名称
                        $wk_name = $reason_2_array[$m]["name"];
                        echo(">$wk_name \n");
                    }
                    echo("</select> \n");
                }
                echo("</td>\n"); // 2014/10 if文ネスト削減対応
                continue; // 2014/10 if文ネスト削減対応
            }


            //-------------------------------------------------------------------------------
            //入力又は表示：次に表示の場合
            //-------------------------------------------------------------------------------
            $wk_font_color = "";
            $wk_back_color = "";

            // 20100112 追加
            //勤務実績は退勤時刻も打刻済の場合に表示 20100112
            //$plan_results_flg "2":予実績表示 "3":実績入力
            //休日と換算日数０は表示
            $wk_tmcd_gid = $plan_array[$i]["rslt_pattern_id_$k"];
            $wk_rslt_id = $plan_array[$i]["rslt_id_$k"];
            $wk_workday_count = $arr_workday_count[$wk_tmcd_gid][$wk_rslt_id];
            $wk_after_night_duty_flag = $arr_after_night_duty[$wk_tmcd_gid][$wk_rslt_id];

            // 前日のレコードの翌日フラグを確認 20100622
            $wk_prev_idx = $k - 1;
            $wk_prev_next_day_flg = $plan_array[$i]["next_day_flag_$wk_prev_idx"];

            if (($plan_results_flg != "2" && $plan_results_flg != "3") ||
                    $plan_copy_flg == "1" ||        //予定コピーの場合を追加 20100125
                    $plan_array[$i]["rslt_id_$k"] == "10" ||
                    $wk_workday_count == 0 ||
                    $wk_after_night_duty_flag == 1 ||   //明けの設定確認 20110927
                    $wk_prev_next_day_flg == 1 ||   // 前日のレコードの翌日フラグを確認 20100622
                    (($plan_results_flg == "2" || $plan_results_flg == "3") &&
                            ($plan_array[$i]["rslt_start_time_$k"] != "" || //20110124 出勤退勤どちらかが登録された場合
                                    $plan_array[$i]["rslt_end_time_$k"] != "") )) {
                $wk_rslt_disp_flg = true;
            } else {
                $wk_rslt_disp_flg = false;
            }

            //-------------------------------------------------------------------------------
            //文字、背景色（勤務実績／勤務希望）
            //-------------------------------------------------------------------------------
            $duty_ymd = $calendar_array[$k-1]["date"];
            //勤務実績は退勤時刻も打刻済の場合に表示 20100112
            if ($wk_rslt_disp_flg == true) {
                if ($wk_assist_flg == "") {
                    $wk_font_color = $plan_array[$i]["rslt_font_color_$k"];
                    $wk_back_color = $plan_array[$i]["rslt_back_color_$k"];
                } else {
                    $wk_font_color = "#C0C0C0";     //灰色
                    $wk_back_color = "";
                }
            }

            //-------------------------------------------------------------------------------
            //文字、背景色（当直）
            //-------------------------------------------------------------------------------
            if ($plan_duty_flg == "2") {
                //当直パターン取得
                $duty_pattern = $this->obj_duty->get_duty_pattern_array();
                //文字、背景色
                $wk_name = "&nbsp;"; // "_" -> " "
                for ($m=0; $m<count($duty_pattern); $m++) {
                    if ($plan_array[$i]["night_duty_$k"] == $duty_pattern[$m]["id"]) {
                        $wk_name = $duty_pattern[$m]["font_name"];  //表示文字名
                        $wk_font_color = $duty_pattern[$m]["font_color"];
                        $wk_back_color = $duty_pattern[$m]["back_color"];
                    }
                }
            }


            //*曜日で背景色設定
            if ($wk_back_color=="" || strtoupper($wk_back_color)=="#FFFFFF"){
                //曜日で背景色設定
                if ($wk_back_color=="" || strtoupper($wk_back_color)=="#FFFFFF" ){
                    if($calendar_array[$k-1]["type"] == "6"){
                        $wk_back_color = "#C8FFD5";
                    }
                    else if ($week_array[$k]["name"] == "土" || $calendar_array[$k-1]["type"] == "5") {
                        $wk_back_color = "#E0FFFF";
                    }
                    else if ($week_array[$k]["name"] == "日" || $calendar_array[$k-1]["type"] == "4") {
                        $wk_back_color = "#FFF0F5";
                    }
                }
            }

            //-------------------------------------------------------------------------------
            //表示（コメント時は入力）
            //-------------------------------------------------------------------------------
            //表示（勤務実績／勤務希望）
            if (($plan_results_flg == "2") || ($plan_results_flg == "3") ||
                ($plan_hope_flg == "2")) {
                //勤務パターン登録子画面対応、onclick時の動作を変更
                $onclick_str = "";
                // 実績入力
                if ($plan_results_flg == "3") {
                    //$onclick_str = "setStamp($i, $k, '2');";
                    //$onclick_str = "ptnUpdate('2','".$i."','','','".$k."');";
                    //タイムカード修正画面 20100302
                    $onclick_str = "timecardEdit('".$i."','".$k."','".$duty_ymd."');";
                } else
                    // 権限 ２行目がシフト希望
                    if ($create_flg == "1" && $plan_hope_flg == "2") {
                        //同グループ
                        if ($wk_assist_flg == "") {
                            $wk_atdptn_ptn_id = $plan_array[$i]["rslt_id_$k"];
                            $wk_reason = ($wk_atdptn_ptn_id == "10") ? $plan_array[$i]["rslt_reason_$k"] : "00";
                            $wk_id = sprintf("%02d%02d", $wk_atdptn_ptn_id, $wk_reason);
                            $onclick_str = "ptnUpdate('2','".$i."','".$wk_id."','".$plan_array[$i]["rslt_reason_2_$k"]."','".$k."');";
                        } else {
                            //他グループ
                            $onclick_str = "alert('他シフトグループの予定は更新できません。');";
                        }
                    }

                if ($k <= $extra_day){      //表示期間外表示の際にリングをはずす 20130307
                    $onclick_str = "";
                }
                // 2014/10 この位置にあった<td>出力文は、都合が悪いため、以降でまとめて出力。このコメントは削除されたし。
                $wk = "";

                //名称設定
                //勤務実績は退勤時刻も打刻済の場合に表示 20100112
                if ($wk_rslt_disp_flg == true) {
                    $wk = $plan_array[$i]["rslt_name_$k"];
                    if ($wk != "&nbsp;") {
                    	$wk = h($wk);
                    }
                    if ($plan_array[$i]["rslt_reason_2_$k"] != "" &&
                            $reason_setting_flg == "t") {
                        for ($m=0; $m<count($reason_2_array); $m++) {
                            if ($plan_array[$i]["rslt_reason_2_$k"] == $reason_2_array[$m]["id"]) {
                                $wk .= $reason_2_array[$m]["font_name"];
                                break;
                            }
                        }
                    }
                }

//*                     $fsize = (strlen($wk) > 2) ? "j10" : "j12";
//*                     echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$fsize\" color=\"$wk_font_color\" >");
                // 2014/10 ここにあった$wk出力は、都合が悪いため、以降でまとめて出力。このコメントは削除されたし。
                $listmarks = array(); // 2014/10 TDボックスに表示するためのマーカーID配列
                //退勤がない場合の表示20100820
                if (($plan_array[$i]["rslt_start_time_$k"] != "" &&
                        $plan_array[$i]["rslt_end_time_$k"] == "") ||
                        ($plan_array[$i]["rslt_start_time_$k"] == "" && //出勤がない場合 20110124
                                $plan_array[$i]["rslt_end_time_$k"] != "")) {
                    //echo("<font size=\"3\" color=\"red\" >・</font>"); // 2014/10 既存のマーカー廃止
                    $listmarks[]="empty_taikin"; // 2014/10 マーカー配列に追加
                    //add 20111115-->
                } else {

                    //基準シフトグループマッチ
                    if ($wk_tmcd_gid == $officehours_array[0]["pg_id"]){
                        //時刻が設定されている場合
                        if ($plan_array[$i]["rslt_start_time_$k"] != "" && $plan_array[$i]["rslt_end_time_$k"] != ""){

                            for ($m=0; $m<count($officehours_array); $m++) {
                                //勤務パターンマッチ
                                if ($wk_rslt_id == $officehours_array[$m]["pattern"]){

                                    //20140318
                                    $wk_start_time = "";
                                    $wk_end_time = "";
                                    if ($officehours_array[$m]["empcond_officehours_flag"] != "") {
                                        $wk_date = $calendar_array[$k-1]["date"];
                                        $wk_weekday = $arr_weekday_flg[$wk_date];
                                        $arr_empcond_officehours = $timecard_common_class->get_empcond_officehours($wk_weekday, $wk_date);
                                        //echo("<!--");
                                        //echo("wk_date=$wk_date");
                                        //echo(" wk_weekday=$wk_weekday");
                                        //print_r($arr_empcond_officehours);
                                        //echo("-->\n");
                                        if ($arr_empcond_officehours["officehours2_start"] != "") {
                                            $wk_start_time = str_replace(":", "", $arr_empcond_officehours["officehours2_start"]);
                                            $wk_end_time = str_replace(":", "", $arr_empcond_officehours["officehours2_end"]);
                                        }

                                    }
                                    //前日設定ではない場合
                                    if ($plan_array[$i]["previous_day_flag_$k"] == 0) {
                                        if ($wk_start_time == "") {
                                            $wk_start_time = $officehours_array[$m]["start_time"];
                                        }

                                        //出勤時間判定(遅刻)
                                        if($wk_start_time<$plan_array[$i]["rslt_start_time_$k"]){
                                            //echo("<font size=\"3\" color=\"blue\">・</font>"); // 2014/10 既存のマーカー廃止
                                            $listmarks[]="is_chikoku"; // 2014/10 マーカー配列に追加
                                            break;
                                        }
                                    }

                                    //翌日設定ではない場合
                                    if ($plan_array[$i]["next_day_flag_$k"] == 0) {
                                        if ($wk_end_time == "") {
                                            $wk_end_time = $officehours_array[$m]["end_time"];
                                        }
                                        //出勤時間判定(早退)
                                        if($wk_end_time>$plan_array[$i]["rslt_end_time_$k"]){
                                            //echo("<font size=\"3\" color=\"blue\">・</font>"); // 2014/10 既存のマーカー廃止
                                            $listmarks[]="is_soutai"; // 2014/10 マーカー配列に追加
                                        }
                                    }
                                    break;
                                }
                            }

                        }
                    }
                    //add 20111115<--
                }

                // 2014/10 会議時間があれば、マーカーへ追加
                if ($plan_array[$i]["rslt_meeting_start_time_$k"] != "" || $plan_array[$i]["rslt_meeting_start_time_$k"] != "") {
                    $listmarks[]= "exist_meeting";
                }
                // 2014/10 有休があれば、マーカーへ追加
                if ($plan_array[$i]["paid_hol_start_time_$k"] != "" || $plan_array[$i]["paid_hol_use_hour_$k"] != "" || $plan_array[$i]["paid_hol_use_minute_$k"] != "") {
                    $listmarks[]= "exist_paid_hol_hour";
                }


                // 2014/10
                // ここでようやく<TD>の中身を出力する。
                // マーカーがあれば、マーカーがよい位置にくるように<TD>内を調整
                if (count($listmarks)) {
                    echo("<td id=\"$wk_td_id\" class=\"td$wk_width\" colspan=\"1\" bgcolor=\"$wk_back_color\" onClick=\"$onclick_str\"");
                    echo(' style="background-image:url(duty_gen_listmark.php?'.implode("=1&",array_unique($listmarks)).'=1); background-repeat:no-repeat; background-position:left bottom; padding-bottom:7px; color:'.$wk_font_color.';'.$f_style.'">');
                    echo($wk);

                }
                // 2014/10
                // マーカーがない場合の<TD>を出力する
                else {
                    echo("<td id=\"$wk_td_id\" class=\"td$wk_width\" colspan=\"1\" bgcolor=\"$wk_back_color\" onClick=\"$onclick_str\" style=\"color:$wk_font_color;$f_style\">");
                    echo($wk);
                }
            }
            //表示（当直）
            if ($plan_duty_flg == "2") {
                echo("<td id=\"$wk_td_id\" class=\"td$wk_width\" colspan=\"1\" bgcolor=\"$wk_back_color\" style=\"color:$wk_font_color;$f_style\">");
                echo($wk_name);
            }
            //入力（コメント）
            if ($plan_comment_flg == "2") {
                echo("<td id=\"$wk_td_id\" class=\"td$wk_width\" colspan=\"1\" bgcolor=\"$wk_back_color\" style=\"color:$wk_font_color;$f_style\">");
                $wk = h($plan_array[$i]["comment_$k"]);
                $wk_id = "comment_$i" . "_" . $k;

                if ($k <= $extra_day) { //表示期間外表示の場合 20130318
                    echo("<input name=\"$wk_id\" type=\"text\" value=\"$wk\" size=\"$input_comment_size\" maxlength=\"5\" style=\"ime-mode:active;\" disabled=\"disabled\">\n");
                }else{
                    echo("<input name=\"$wk_id\" type=\"text\" value=\"$wk\" size=\"$input_comment_size\" maxlength=\"5\" style=\"ime-mode:active;\" onchange=\"setCmtUpdFlg($i);\">\n");
                }
            }
            echo("</td>\n");
        }

        //２段目の集計
        if ($total_disp_flg != "1" && ($plan_results_flg == "2" || $plan_results_flg == "3" || $plan_hope_flg == "2")) {
            $gyo_array_cnt = count($title_gyo_array);
            $hol_array_cnt = count($title_hol_array);

            //公休・年休表示
            $wk_koukyu_cnt = 0;
            $wk_nenkyu_cnt = 0;
            $wk_koukyu_pos = 0;
            $wk_nenkyu_pos = 0;
            for ($k=0; $k<count($title_hol_array); $k++) {
                //公休・年休表示
                if ($title_hol_array[$k]["reason"] == "1") {
                    $wk_nenkyu_cnt = $hol_cnt_array[$i][$hol_array_cnt + $k];
                    $wk_nenkyu_pos = $k;
                }
                if ($title_hol_array[$k]["reason"] == "24") {
                    $wk_koukyu_cnt = $hol_cnt_array[$i][$hol_array_cnt + $k];
                    $wk_koukyu_pos = $k;
                }
            }
            for ($k=0; $k<count($title_gyo_array); $k++) {
                $wk = $gyo_array[$i][$gyo_array_cnt + $k];
                if ($title_gyo_array[$k]["id"] == "10") {
                    //公休、年休表示 $wk_koukyu_pos $wk_nenkyu_pos
                    $wk_td_id = "sum_gyo" . $cnt . "_" . ($k + 1);  //TDタグID
                    echo("<td id=\"$wk_td_id\" class=\"td30\" rowspan=\"$span\" bgcolor=\"$wk_bgcolor\" style=\"$f_style3\">$wk_koukyu_cnt</td> \n");
                    $wk_td_id = "sum_gyo" . $cnt . "_" . ($k + 2);  //TDタグID
                    echo("<td id=\"$wk_td_id\" class=\"td30\" rowspan=\"$span\" bgcolor=\"$wk_bgcolor\" style=\"$f_style3\">$wk_nenkyu_cnt</td> \n");
                    $wk = $wk - $wk_nenkyu_cnt - $wk_koukyu_cnt;
                }
                $wk_td_id = "sum_gyo" . $cnt . "_" . $k;    //TDタグID
                //集計列幅 20140502
                $wk_td = $title_gyo_array[$k]["td"];
                echo("<td id=\"$wk_td_id\" class=\"{$wk_td} sum_gyo_1\" bgcolor=\"$wk_bgcolor\" style=\"$f_style3\">$wk</td> \n");
            }
            //有給休暇残日数
            if ($paid_hol_disp_flg === "t") {
                if ($plan_hope_flg == "2") {    //勤務希望を表示
                    echo("<td id=\"$wk_td_id\" class=\"td30\" bgcolor=\"$wk_bgcolor\" style=\"$f_style3\">{$paid_hol_all_array["hope"][$i]}</td> \n");
                } else {                        //実績を表示
                    //繰越公休残日数 start
                    $wk_td_id = "sum_gyo" . $cnt . "_" . $k;  //TDタグID
                    //繰越公休残日数 end
                    echo("<td id=\"$wk_td_id\" class=\"td30\" bgcolor=\"$wk_bgcolor\" style=\"$f_style3\">{$paid_hol_all_array["result"][$i]}</td> \n");
                }
            }
        }
        if ($hol_dt_flg == "1" && ($plan_results_flg == "2" || $plan_results_flg == "3" || $plan_hope_flg == "2")) {
            $gyo_array_cnt = count($title_hol_array);
            for ($k=0; $k<count($title_hol_array); $k++) {
                //公休・年休非表示
                if ($title_hol_array[$k]["reason"] == "1" ||
                        $title_hol_array[$k]["reason"] == "24") {
                    continue;
                }
                $wk_td_id = "sum_gyo" . $cnt . "_" . $k;    //TDタグID
                $wk = $hol_cnt_array[$i][$gyo_array_cnt + $k];
                echo("<td id=\"$wk_td_id\" class=\"td30\" bgcolor=\"$wk_bgcolor\" style=\"$f_style3\">$wk</td> \n");
            }
        }
        //繰越公休残日数 start
        if ($total_disp_flg != "1" && $this->hol_remain_info->transfer_holiday_days_flg == "t") {
            if (($plan_results_flg == "2" || $plan_results_flg == "3" || $plan_hope_flg == "2")) {
                $wk = $plan_array[$i]["hol_remain"];
                if ($wk == "") {//公休残がなければ計算
                    //前月がなければ計算せず、背景を赤とする
                    if ($plan_array[$i]["prev_remain"] == "") {
                        $hol_bgcolor = "#ff0000";
                    }
                    else {
                        $hol_bgcolor = "";
                        //前月残＋所定公休数−当月実績公休数
                        $wk = $plan_array[$i]["prev_remain"] + $plan_array[$i]["std_hol_cnt"] - $plan_array[$i]["hol_cnt"];
                    }
                }
                $wk_td_id = "hol_remain" . $cnt;    //TDタグID
                echo("<td id=\"$wk_td_id\" class=\"td30 hol_remain_1\" bgcolor=\"$hol_bgcolor\" style=\"$f_style3\">$wk</td>\n");
                //前月残＋所定公休数をhiddenに設定
                $l_hol_idx = $this->hol_remain_info->legal_hol_idx;
                if ($l_hol_idx >= 0) {
                    //前月残＋所定公休数（javascriptでの計算用）
                    $prev_std_cnt = $plan_array[$i]["prev_remain"] + $plan_array[$i]["std_hol_cnt"];
                    echo("<input type=\"hidden\" name=\"prev_std_cnt{$cnt}\" id=\"prev_std_cnt{$cnt}\" value=\"{$prev_std_cnt}\">\n");
                }
                //公休残登録済み確認用hidden
                echo("<input type=\"hidden\" name=\"hol_remain_reg{$cnt}\" id=\"hol_remain_reg{$cnt}\" value=\"{$plan_array[$i]['hol_remain']}\">\n");
            }
        }
        //繰越公休残日数 end
        echo("</tr>\n");

    }

    // 個人日数の列合計を表示する
    function showPersonDaysTotal(
            $title_hol_array,      //休暇タイトル情報
            $hol_cnt_array,        //休暇日数
            $gyo_array,            //行合計情報
            $data_cnt,             //スタッフ数
            $title_gyo_array,      //行合計タイトル情報
            $total_disp_flg,       //集計 1:非表示
            $hol_dt_flg,           //休暇日数詳細 1:表示 "":非表示
            $paid_hol_disp_flg,    //有給休暇残日数表示フラグ
            $paid_hol_array,       //有給休暇残日数
            $index)                //データインデックス
    {
        if ($total_disp_flg != "1") {
            $wk_nenkyu_cnt = 0;
            $wk_koukyu_cnt = 0;
            //集計
            for ($k=0; $k<count($title_hol_array); $k++) {
                //公休・年休表示
                if ($index == 0) {
                    $k2 = $k;
                } else {
                    $k2 = count($title_hol_array) + $k;
                }

                if ($title_hol_array[$k]["reason"] == "1") {
                    for ($m=0; $m<$data_cnt; $m++) {
                        $wk_nenkyu_cnt += $hol_cnt_array[$m][$k2];
                    }
                }
                if ($title_hol_array[$k]["reason"] == "24") {
                    for ($m=0; $m<$data_cnt; $m++) {
                        $wk_koukyu_cnt += $hol_cnt_array[$m][$k2];
                    }
                }
            }

            for ($k=0; $k<count($title_gyo_array); $k++) {
                $wk = 0;
                if ($index == 0) {
                    $k2 = $k;
                } else {
                    $k2 = count($title_gyo_array) + $k;
                }
                for ($m=0; $m<$data_cnt; $m++) {
                    $wk = $wk + $gyo_array[$m][$k2];
                }
                //公休、有休表示
                if ($title_gyo_array[$k]["id"] == "10") {
                    echo("<td class=\"td30\">$wk_koukyu_cnt</td> \n");
                    echo("<td class=\"td30\">$wk_nenkyu_cnt</td> \n");
                    $wk = $wk - $wk_nenkyu_cnt - $wk_koukyu_cnt;
                }
                //集計列幅 20140502
                $wk_td = $title_gyo_array[$k]["td"];
                echo("<td id=\"day_total_{$index}_{$k}\" class=\"{$wk_td}\">$wk</td> \n");
            }
            //有給休暇残日数
            $wk = 0;
            foreach ($paid_hol_array as $value) {
                $wk += $value;
            }
            if ($paid_hol_disp_flg === "t") {
                echo("<td class=\"td30\">$wk</td> \n");
            }
            //繰越公休残日数 start
            if ($this->hol_remain_info->transfer_holiday_days_flg == "t") {
                if ($index == 0) {
                    $wk = "";
                    echo("<td class=\"td30\">$wk</td> \n");
                }
                else if($index == 1) {
                    //呼び出し元のset_hol_remain_data()で設定
                    $wk = $this->hol_remain_info->hol_remain_total;
                    echo("<td id=\"hol_remain_total\" class=\"td30\">$wk</td> \n");
                }
            }
            //繰越公休残日数 end
        }
        //休暇詳細
        if ($hol_dt_flg == "1") {
            for ($k=0; $k<count($title_hol_array); $k++) {
                //公休・年休非表示
                if ($title_hol_array[$k]["reason"] == "1" ||
                        $title_hol_array[$k]["reason"] == "24") {
                    continue;
                }
                $wk = 0;
                if ($index == 0) {
                    $k2 = $k;
                } else {
                    $k2 = count($title_hol_array) + $k;
                }
                for ($m=0; $m<$data_cnt; $m++) {
                    $wk = $wk + $hol_cnt_array[$m][$k2];
                }
                echo("<td class=\"td30\">$wk</td> \n");
            }
        }
    }

    /**
     * 十字ハイライトJavaScriptを生成する
     *
     * @param string $show_data_flg 2行目表示データフラグ
     *
     * @return void
     */
    function createCrossHighlightJavaScript($show_data_flg) {
        // 色指定
        $conf = new Cmx_SystemConfig();
        $color = $conf->get('shift.highlight.color');
        if (empty($color)) {
            $color = '#000000';
        }
        $bgcolor = $conf->get('shift.highlight.bordercolor');
//        if (empty($bgcolor)) {
//            $bgcolor = '#ffffe0';
//        }
        if (empty($bgcolor)) $bgcolor = '#ff8080'; // 2014/10 デフォルト色


        $script = <<< _SCRIPT_
var highlight_show_data_flg = '$show_data_flg';
var gBeginCell=0; // 2014/10 矩形選択開始セルTDタグ自体。初期値はゼロ
var gSquareMode=""; // 2014/10 カラ：何もしていない、dragging：緑半透明矩形描画中、moving：貼付先緑枠の移動中、applying：貼付先確定＋貼付処理中
var gSquareModeDelay=0; // 2014/10 ブラウザのマウスムーブイベントを無視する回数を数えるカウンタ
var gDayFirstIdx = 0; // 2014/10 月初日の列位置
var gDayLastIdx = 0; // 2014/10 月末日の列位置
var gColLefts = []; // 2014/10 各列の、絶対始点エックス座標を格納しておくもの
var gColWidths = []; // 2014/10 各列の、列幅を格納しておくもの
var gRowTops = []; // 2014/10 各行の、絶対始点ワイ座標を格納しておくもの
var gRowHeights = []; // 2014/10 各列の、列高を格納しておくもの
var gRectInfo = {}; // 2014/10 矩形情報
var gMouseCellInfo = {}; // 2014/10 マウス位置にあるセル情報
var gThruChangCell = 0; // 2014/10 多量セル貼付時、逐一サーバに集計計算をさせたくない場合に1。menu.jsも参照のこと
// 2014/10 以下はエレメントアクセッサ
var gDivRegion=0, gDivOutCountWindowBar=0, gDivCountWindow=0, gTblData=0, gTblHeader=0, gDivParent=0, gDivLineHol=0, gDivLineVert=0;


$(function() {
// コメントアウト 2014/10
//     $("<style type='text/css'>#data tr.hl td,td.hl{color:{$color} !important; background-color:{$bgcolor} !important;}</style>").appendTo('head');
//    $('#data').find('td').mouseenter(function(e) {
//        var cell = $(this);
//        if (cell && cell.attr('id').indexOf("data") !== -1) {
//            highlight_in(cell);
//        }
//    }).mouseleave(function(e){
//        var cell = $(this);
//        if (cell && cell.attr('id').indexOf("data") !== -1) {
//            highlight_out(cell);
//        }
//    });
// コメントアウトここまで 2014/10

    // 十字表示の開始
    $("#floatWindowStp").css({zIndex:2}); // 既存パーツ様態変更
    $("#floatWindowKey").css({zIndex:2}); // 既存パーツ様態変更
    gDivRegion = $("#region").get(0); // データリストのスクロールDIV。gDivParentと等しいかもしれない
    gDivOutCountWindowBar = $("div#outCountWindowBar").get(0); // データ部直下のリサイズスプリットバー
    gDivCountWindow = $("div#countWindow").get(0); // フッタリスト部を囲うDIV
    gTblData = $("table#data").get(0); // データリスト
    gTblHeader = $("table#header").get(0); // ヘッダリスト
    var tblSummary = $("table#summary").get(0); // フッタリスト。無いかもしれない
    gDivParent = gTblData.parentNode; // データリストを囲うDIV。gDivRegionと等しいかもしれない
    gDivParent.style.position = "relative"; // 既存パーツ様態変更
    // 十字オブジェクト。色は「管理画面」−「オプション」−「カーソルハイライト」−「背景色」で指定。
    // 「色変更しない」とすると、赤色(#ff7777)になる。
    gDivLineHol = $('<div style="position:absolute; height:3px; z-index:1; border-top:3px solid {$bgcolor}; left:'+gDivParent.offsetLeft+'; line-height:0;"></div>');
    gDivLineVert = $('<div style="position:absolute; width:3px; z-index:1; background-color:{$bgcolor}; top:'+gTblHeader.offsetTop+';"></div>');
    var PNP = gDivParent.parentNode; // データリストを囲うDIVを、さらに囲うDIV
    PNP.insertBefore(gDivLineHol.get(0), gDivParent); // スクロール領域のひとつ外に組み込む
    PNP.insertBefore(gDivLineVert.get(0), gDivParent); // スクロール領域のひとつ外に組み込む

    // ヘッダ、データ、フッタテーブルの全てのセルに、十字キーを発生させるイベントを組み込む。
    // フッタテーブル（summary）のIDが付与されていないフッタがある場合あり。その場合は、本来summaryであろうテーブルに対して組み込む
    $('table#data td').mouseenter(function(e) {    highlight_in2("data",this);    }).mouseleave(function(e){ highlight_out2(); });
    $('table#header td').mouseenter(function(e) {  highlight_in2("header",this);  }).mouseleave(function(e){ highlight_out2(); });
    $('table#summary td').mouseenter(function(e) { highlight_in2("summary",this); }).mouseleave(function(e){ highlight_out2(); });
    if (!tblSummary) {
        $('div#countWindow table td').mouseenter(function(e) { highlight_in2("summary",this); }).mouseleave(function(e){ highlight_out2(); });
    }

    // 範囲コピー時の緑の矩形（２個）を、スクロール領域内に組み込む
    if (window.IS_SHIFT_MAKER) {
        gLineM = $('<table cellspacing="0 cellpadding="0" style="position:absolute; display:none; z-index:1; background-color:#ffffff;"><tr><td style="border:3px solid #00aa00">&nbsp;</td></tr></table>');
        gLineD = $('<table cellspacing="0 cellpadding="0" style="position:absolute; display:none; z-index:1;" onmouseup="applyRect()"><tr><td style="border:3px solid #00aa00">&nbsp;</td></tr></table>');
        gDivParent.insertBefore(gLineM.get(0), gTblData);
        gDivParent.insertBefore(gLineD.get(0), gTblData);
    }




    if (window.IS_SHIFT_MAKER) {
        stockCellSizes();

        //==================================================
        // ウィンドウ全体のマウスアップイベント
        // 現在矩形作成中だったなら、矩形確定。貼り付け先選定モードとする
        //==================================================
        $("body").mouseup(function(e){
            if (gSquareMode=="dragging") {
                gLineM.css({background:"#aaffaa", opacity:0.5}).show(); // 矩形は緑の半透明にする
                gSquareMode="moving";
            }
        })

        //==================================================
        // ウィンドウ全体のマウスムーブイベント
        //==================================================
        .mousemove(function(e){
            // 他機能との干渉を防ぐため、マウスムーブをほんの少し行っただけで矩形が発生しないようにする。
            // マウスムーブイベントが８回発生してから、矩形を発生させるようにする。
            if (gSquareMode=="" && gSquareModeDelay<8) { gSquareModeDelay++; return; }

            if (gSquareMode=="") { if (!gBeginCell) return;  } // 初期状態の場合。最初の矩形開始セルを決めていなければ終了
            else if (gSquareMode!="dragging" && gSquareMode!="moving") return; // その他。矩形作成中でもなく、貼付先選定中でもなければ終了

            // マウス座標。データスクロール領域上の座標とする。
            var mouseY = e.clientY + $(window).scrollTop()  - gDivParent.offsetTop + gDivParent.scrollTop;
            var mouseX = e.clientX + $(window).scrollLeft() - gDivParent.offsetLeft;

            // マウス位置のセル列順番号（０オリジン）を求める。月初から月末までの範囲とする。
            // ヘッダ列があるので、最低でも３とか７とか以上の値となる。
            var colIdx = 0;
            if (mouseX < gColLefts[gDayFirstIdx])  colIdx = gDayFirstIdx; // 月初より小さい→月初のインデックスとする
            if (mouseX > gColLefts[gDayLastIdx+1]) colIdx = gDayLastIdx;  // 月末より大きい→月末のインデックスとする
            if (colIdx==0) {
                for (var idx=gDayFirstIdx; idx<=gDayLastIdx; idx++) {
                    if (gColLefts[idx] > mouseX) break;
                    colIdx = idx;
                }
            }

            // マウス位置のセル行順番号（０オリジン）を求める
            var empRow = 0; // 行番号。１オリジンのもの。２行形式の場合は、上位行にデータがある。つまり奇数行にデータがあるため、値は奇数となる。
            var rowIdx = 0; // 行インデックス。０オリジン。２行形式の場合は偶数行を指す。ゼロオリジンなので、値自体は奇数となる。つまりempRowと等しい。
            // ２行形式
            if (highlight_show_data_flg) {
                for (var idx=0; idx<=gRowTops.length; idx++) {
                    // ２行形式の場合の奇数行にマウスがある場合
                    if (!(idx % 2)) {
                        if (gRowTops[idx] > mouseY) break;
                        rowIdx = idx-1; // ひとつ前のセル開始からとみなす
                    }
                    // ２行形式の場合の偶数行にマウスがある場合
                    else {
                        rowIdx = idx; // 最初に絶対代入。
                        if (idx > 0 && gRowTops[idx] > mouseY) break;
                    }
                }
                empRow = rowIdx; // １オリジンのempRowは偶数行を示すrowIdxの値と、等しくなる。
                if (empRow==0) empRow = 1;
            }
            // １行形式
            else {
                for (var idx=0; idx<gRowTops.length; idx++) {
                    if (gRowTops[idx] > mouseY) break;
                    rowIdx = idx;
                }
                empRow = rowIdx+1; // １オリジンのempRowは、行インデックスに１を足す
            }

            // 貼付先が日付ｘ従業員データ領域の中にない場合はisOutOfArea=1とする。貼付先矩形描画に影響。
            var isOutOfArea = 0;
            if (mouseY < 0) isOutOfArea = 1;
            if (mouseY > gRowTops[gRowTops.length-1] + gRowHeights[gRowHeights.length-1]) isOutOfArea = 1;
            if (mouseX < gColLefts[gDayFirstIdx]) isOutOfArea = 1;
            if (mouseX > gColLefts[gDayLastIdx+1]) isOutOfArea = 1;

            // マウスが指し示す位置インフォ
            gMouseCellInfo = {
                "isOutOfArea" : isOutOfArea,
                "left"   : gColLefts[colIdx], // マウス下セルの、セル左位置ピクセル
                "right"  : gColLefts[colIdx] + gColWidths[colIdx], // マウス下セルの、セル右位置ピクセル
                "top"    : gRowTops[rowIdx], // マウス下セルの、セル上位置ピクセル（複数行なら、偶数行の場合の位置）
                "bottom" : gRowTops[rowIdx]+gRowHeights[rowIdx], // マウス下セルの、セル下位置ピクセル（複数行なら、偶数行の場合の位置）
                "dayCol" : colIdx+1 - gDayFirstIdx, // セルの日付番号
                "empRow" : empRow // セルの従業員番号（１オリジンの行番号と等しい）
            };

            //==============================================
            // 矩形描画中の場合（draggingは、矩形作成中のモード）
            //==============================================
            if (gSquareMode=="" || gSquareMode=="dragging") {
                gSquareMode="dragging"; // 矩形ドラッグ作成中モードに
                gSquareModeDelay = 0; // 矩形開始の遊びタイムをリセット
                var sPos = gBeginCell.id.replace("data","").split("_"); // 開始セルの位置を取得したい。セルのIDからセルの縦横位置がわかる。
                // 開始セルは２行形式なら奇数行である。
                // もしマウスを下から上へ持っていった場合、
                // ◆開始セルのひとつ下の行からドラッグしたものとしたい。
                // ◆終了セルのひとつ上の行までドラッグしたものとしたい。
                var beginTop = gBeginCell.offsetTop;
                var beginHeight = gBeginCell.offsetHeight;
                var endTop = gMouseCellInfo.top;
                var endBottom = gMouseCellInfo.bottom;
                if (highlight_show_data_flg) {
                    if (sPos[0] > empRow) {
                        var tBeginCell = document.getElementById("data"+(parseInt(sPos[0],10)+1)+"_"+sPos[1]);
                        beginTop = tBeginCell.offsetTop;
                        beginHeight = tBeginCell.offsetHeight;
                        var tEndCell = document.getElementById("data"+(gMouseCellInfo.empRow)+"_"+gMouseCellInfo.dayCol);
                        if (!tEndCell) {
                            alert("data"+(gMouseCellInfo.empRow)+"_"+gMouseCellInfo.dayCol);
                        }
                        endTop = tEndCell.offsetTop;
                        endBottom = endTop+tEndCell.offsetHeight;
                    }
                }
                // 矩形の構造
                gRectInfo = {
                    "minX": Math.min(gBeginCell.offsetLeft, gMouseCellInfo.left), // 矩形の最左ピクセル
                    "minY": Math.min(beginTop, endTop), // 矩形の最上ピクセル
                    "maxX": Math.max(gBeginCell.offsetLeft+gBeginCell.offsetWidth, gMouseCellInfo.right), // 矩形の最右ピクセル
                    "maxY": Math.max(beginTop+beginHeight, endBottom), // 矩形の最下ピクセル
                    "minDay":Math.min(sPos[1], gMouseCellInfo.dayCol), // 矩形の最左日付（ここから列順番号が判定可能）
                    "minEmp":Math.min(sPos[0], gMouseCellInfo.empRow), // 矩形の最上従業員（ここから行順番号が判定可能）
                    "maxDay":Math.max(sPos[1], gMouseCellInfo.dayCol), // 矩形の最右日付（ここから列順番号が判定可能）
                    "maxEmp":Math.max(sPos[0], gMouseCellInfo.empRow)  // 矩形の最下従業員（ここから行順番号が判定可能）
                }
                gRectInfo["colSize"] = gRectInfo.maxDay - gRectInfo.minDay + 1; // 矩形範囲内の列数。１以上
                gRectInfo["rowSize"] = gRectInfo.maxEmp - gRectInfo.minEmp + 1; // 矩形範囲内の行数。１以上

                locateGreenRect(); // 矩形DIV表示位置へロケート
                gLineM.css({ background:"transparent", opacity:1 }).show(); // 矩形DIVをshow。背景色なし。
            }

            //==============================================
            // 矩形描画後の、貼付先選定中モード
            //==============================================
            if (gSquareMode=="moving") {
                gRectInfo["dcol"] = gMouseCellInfo.dayCol; // 貼付先セルの列順番号
                gRectInfo["drow"] = gMouseCellInfo.empRow; // 貼付先セルの行順番号
                locatePasteRect(); // 貼付先DIV表示位置へロケート
                if (isOutOfArea) gLineD.hide(); // ただしマウスが範囲外なら非表示
                else gLineD.css({background:"transparent"}).show(); // 貼付先DIVを表示。背景色なし。（というか、背景色がつくことは無い）
            }
        });
        // -- windowイベントここまで --


        //==================================================
        // データリスト領域のスタイルとイベントのセッティイング
        // データリスト領域をドラッグしたとき、「文字選択状態」にしたくないための処理
        //==================================================
        $('table#data td')
        .bind("selectstart", function(){return false});
        var css = $('table#data').attr("style");
        $('table#data')
        .attr("style", css+"; -moz-user-select: none; -khtml-user-select: none; -webkit-user-select: none; -o-user-select:none; user-select:none")
        .attr("unselectable", "on")
        .bind("selectreturn", function(){return false})
        //----------------------------------------
        // データリスト クリックイベント。クリック時は矩形描画関連機能は全クリアする。
        //----------------------------------------
        .click(function(e) {
            if (gSquareMode=="applying") return; // 現在貼付実行中なら何もしない
            gBeginCell = 0;
            gSquareMode="";
            gSquareModeDelay = 0;
            if (gLineM) gLineM.hide();
            gLineD.hide();
        })
        //----------------------------------------
        // データリスト マウスダウンイベント。矩形描画関連機能を初期化し、矩形開始セルを決定する。
        //----------------------------------------
        .mousedown(function(e){
            gSquareModeDelay = 0;
            if (gSquareMode=="applying") return; // 現在貼付実行中なら何もしない
            if (!e) var e = window.event;
            var cell = e.srcElement? e.srcElement: e.target; // クリックしたセル(TDタグ）
            if(cell.tagName=="FONT") cell = cell.parentNode; // TDでなく、FONTタグかもしれない。FONTタグであればそのParentを取得
            if(cell.tagName!="TD") return; // このイベントバブリングがTDでなければ終了
            if (cell.id.substring(0,4)!="data") return; // TDのidから、セル位置を取得したい。
            // ２行形式の場合で、偶数行をクリックした場合は、奇数行をクリックしたものとしたい
            if (highlight_show_data_flg) {
                var ids = cell.id.replace("data", "").split("_");
                if (!(ids[0] % 2)) {
                    ids[0]--;
                    cell = $("#data"+ids[0]+"_"+ids[1]).get(0);
                }
            }
            gBeginCell = cell; // グローバルにセルを記録する。
        });
        // -- データリスト領域イベント ここまで --
    }
});

//----------------------------------------------------------
// 矩形選択領域表示を、しかるべき位置へロケート
//----------------------------------------------------------
function locateGreenRect() {
    gLineM.css({
        left:gRectInfo.minX-1,
        top:gRectInfo.minY,
        width:gRectInfo.maxX-gRectInfo.minX+2,
        height:gRectInfo.maxY-gRectInfo.minY+2
    });
}
//----------------------------------------------------------
// 貼付先領域表示を、しかるべき位置へロケート
// 貼付先位置がヘッダ部などなら表示領域は隠すように
//----------------------------------------------------------
function locatePasteRect() {
    var colIdx = gMouseCellInfo.dayCol-1+gDayFirstIdx;
    var rowIdx = gMouseCellInfo.empRow-1;
    var oL = gColLefts[colIdx];
    var oT = gRowTops[rowIdx];
    var maxR = Math.min(gDayLastIdx,       colIdx+gRectInfo.colSize-1);
    var maxB = Math.min(gRowTops.length-1, rowIdx+gRectInfo.rowSize-1+(highlight_show_data_flg?1:0));
    var oR = gColLefts[maxR] + gColWidths[maxR];
    var oB = gRowTops[maxB] + gRowHeights[maxB];
    gLineD.css({left:oL-1, top:oT-1, width:oR-oL+2, height:oB-oT+2});
}

//----------------------------------------------------------
// スクロール領域のスクロールイベント（duty_shift_menu.php参照）
//----------------------------------------------------------
function regionScrolled() {
    if (!window.IS_SHIFT_MAKER) return;
    locateGreenRect();
    locatePasteRect();
}
//----------------------------------------------------------
// 矩形の貼付処理
//----------------------------------------------------------
function applyRect() {
    gSquareMode="applying";
    stamp_ptn = "";
    document.mainform.stamp_flg.value = "";
    reasonData = {};
    planPtnData = {};
    // 現在のセルの勤務区分を取得。勤務区分はセルのアトリビュートに格納している。
    // 「アトリビュートのコピーのコピー…」が発生しないよう、先にコピー元矩形のアトリビュートをすべて取得しておく
    for (var rowIdx = 0; rowIdx < gRectInfo.rowSize; rowIdx++) {
        for (var colIdx = 0; colIdx < gRectInfo.colSize; colIdx++) {
            var frCol = gRectInfo.minDay + colIdx;
            var frRow = gRectInfo.minEmp + rowIdx;
            var frCell = $("#data"+frRow + "_" + frCol).get(0);
            reasonData[frRow+"_"+frCol] = frCell.getAttribute("reason");
            planPtnData[frRow+"_"+frCol] = frCell.getAttribute("plan_ptn");
        }
    }
    // 貼付処理を実施
    for (var rowIdx = 0; rowIdx < gRectInfo.rowSize; rowIdx++) {
        for (var colIdx = 0; colIdx < gRectInfo.colSize; colIdx++) {
            var frCol = gRectInfo.minDay + colIdx; // 複製元セルの年月連番。１オリジン
            var frRow = gRectInfo.minEmp + rowIdx; // 複製元セルの従業員連番。１オリジン
            var toCol = gRectInfo.dcol + colIdx; // 複製先セルの年月連番。１オリジン
            var toRow = gRectInfo.drow + rowIdx; // 複製先セルの従業員連番。１オリジン
            var frCell = $("#data"+frRow + "_" + frCol).get(0); // 複製元セル
            var toCell = $("#data"+toRow + "_" + toCol).get(0); // 複製先先セル
            if (!toCell) continue; // 範囲外への複製。飛ばす。
            var rh1 = toCell.parentNode.offsetHeight; // 複製先セルの、値セット前の<TR>行の高さ
            var reason = reasonData[frRow+"_"+frCol]; // 事由
            var plan_ptn = planPtnData[frRow+"_"+frCol]; //
            if (toCell.getAttribute("reason")==reason && toCell.getAttribute("plan_ptn")==plan_ptn) continue; // 複製前後で変化のないセル。スキップ。
            var targetid = (plan_ptn=="10" ? plan_ptn+"_"+reason : plan_ptn); // スタンプ用の事由番号。
            document.getElementById("listPosition").value = "data"+toRow+"_"+toCol; // 選択中セルを示す変数に値セット。既存処理で必要。
            document.getElementById('listPositionPlanHope').value = "0"; // 選択中セルを示す変数に値セット。既存処理で必要。

            // 登録。登録直後にajaxにてサーバ側計算が入るが、時間がかかるため、
            // 行の最右列のセル、または最終行でなければ、計算させない。
            gThruChangCell = 1;
            if (colIdx==gRectInfo.colSize-1 || toCol-1==gDayLastIdx-gDayFirstIdx) gThruChangCell = 0; // 最右列セルなら計算させる
            if (rowIdx==gRectInfo.rowSize-1 || toRow==gRowHeights.length) gThruChangCell = 0; // 最終行なら計算させる
            setStampPtn(plan_ptn, reason, "stamp_"+targetid); // 既存スタンプ処理呼出。セルに値がセットされ、必要な計算結果セルも更新される。計算にはajax利用

            toCell.setAttribute("reason", reason); // セルのアトリビュートに事由をセット
            toCell.setAttribute("plan_ptn", plan_ptn); // セルのアトリビュートに事由をセット
            var rh2 = toCell.parentNode.offsetHeight; // 再度、<TR>行の高さを測定してみる。
            // 行の高さが変化しているなら、行の高さを測定しなおす。
            if (rh1!=rh2) {
                gRowTops = [];
                gRowHeights = [];
                $('table#data tr').each(function(){
                    gRowTops.push(this.offsetTop);
                    gRowHeights.push(this.offsetHeight);
                });
                locatePasteRect();
            }
        }
    }
    // フラグマークを消したい
    document.mainform.stamp_flg.value = "1"; // 一旦表示している状態フラグにする
    setStampFlg(); // こうしてこの関数を呼ぶと、フラグを消すことができる。
    gBeginCell = 0;
    gLineD.hide();
    gLineM.hide();
    gSquareMode="";
    gSquareModeDelay = 0;
    stockCellSizes();
}
//----------------------------------------------------------
// 十字ボーダーの表示
//----------------------------------------------------------
function highlight_in2(table_id, cell) {
    if (gSquareMode) { highlight_out2(); return; } // 矩形系処理を行っている最中なら、十字を表示しないで終了

    //====================
    // 縦線。セルの右にライン用DIVを表示する。
    //====================
    var vh = gTblHeader.offsetHeight; //縦線の高さ。まずはヘッダリストの高さを加える
    if (gDivRegion) vh += gDivRegion.offsetHeight; // データリストスクロール領域が有る場合は、その高さを加える
    else vh += gTblData.offsetHeight; // スクロール領域がなければ、データリスト自体の高さを加える
    if (gDivOutCountWindowBar) vh += gDivOutCountWindowBar.offsetHeight; // データリスト直下スプリットバーがあればその高さを加える
    if (gDivCountWindow) vh += gDivCountWindow.offsetHeight; // フッタ集計領域があれば、その高さを加える
    gDivLineVert.css({height:vh, top:gDivParent.offsetTop-gTblHeader.offsetHeight, left:cell.offsetLeft+cell.offsetWidth-1+gDivParent.offsetLeft});

    //====================
    // 横線。セルの下にライン用DIVを表示する。２行表示の場合は、２行目の下に線を引くように。
    //====================
    if (table_id!="data") { gDivLineHol.css({top:-10}); return; } // イベントの発生元がデータリストでなければ、横線不要。非表示にして終了。
    var hl_row = $(cell.parentNode); // マウス位置のセルの親<TR>タグを取得
    var tds = hl_row.children()[0]; // その<TR>の最初の<TD>を取得
    if (parseInt(tds.getAttribute("rowspan"),10)>1) hl_row = $(hl_row).next(); // その<TD>にrowspanがあれば２行表示中かつ１行目のセルである。次の行にする。

    var tr = hl_row.get(0);
    var ht = tr.offsetTop-1+tr.offsetHeight;
    gDivLineHol.css({width:gTblData.offsetWidth, top:ht+gDivParent.offsetTop-gDivParent.scrollTop});
}
//----------------------------------------------------------
// 十字ボーダーを隠す
//----------------------------------------------------------
function highlight_out2(){
    gDivLineHol.css({top:-10}); // 横線を隠す
    gDivLineVert.css({left:-10}); // 縦線を隠す
}
//----------------------------------------------------------
// データリスト内セルの位置、サイズを測定してグローバル変数に記憶する
//----------------------------------------------------------
function stockCellSizes() {
    var cidx = -1;
    gColLefts = [];
    gColWidths = [];
    gRowTops = [];
    gRowHeights = [];
    $('table#data tr:first td').each(function(){
        cidx++;
        gColLefts.push(this.offsetLeft);
        gColWidths.push(this.offsetWidth);
        if (gDayFirstIdx==0 && this.id.substring(0,4)=="data") gDayFirstIdx = cidx;
        if (gDayLastIdx==0 && gDayFirstIdx>0 && this.id.substring(0,4)!="data") gDayLastIdx = cidx-1;
    });

    $('table#data tr').each(function(){
        gRowTops.push(this.offsetTop);
        gRowHeights.push(this.offsetHeight);
    });
}
///-----------------------------------------------------------------------------
// テーブルハイライト in/out
//------------------------------------------------------------------------------
// コメントアウト 2014/10
//var hl_row, hl_row2, hl_cols;
//function highlight_in(cell) {
//    var idx = cell.attr('id').slice(4).split('_');
//
//    // 1行目
//    hl_row = cell.parent()[0];
//    hl_row.className += ' hl';
//
//    // 2行目
//    if (highlight_show_data_flg) {
//        if (idx[0] % 2) {
//            hl_row2 = cell.parent().next()[0];
//        }
//        else {
//            hl_row2 = cell.parent().prev()[0];
//        }
//        hl_row2.className += ' hl';
//    }
//
//    // 見出しと集計行
//    hl_cols = $('table').find('td.day'+idx[1]);
//    for (var i = 0; i < hl_cols.length; i++) {
//        hl_cols[i].className += ' hl';
//      }
//
//}
//function highlight_out(cell){
//    if (hl_row) {
//        hl_row.className = hl_row.className.replace(/ hl/g, '');
//    }
//    if (hl_row2) {
//        hl_row2.className = hl_row2.className.replace(/ hl/g, '');
//    }
//    if (hl_cols) {
//        for (var i = 0; i < hl_cols.length; i++) {
//            hl_cols[i].className = hl_cols[i].className.replace(/ hl/g, '');
//        }
//    }
//}
// コメントアウトここまで 2014/10
_SCRIPT_;
        echo $script;
    }

    /**
     * 様式９の値を算出する(参考：duty_shift_report.phpの様式９作成処理)
     *
     * @param string  $recomputation_flg １ヵ月計算４週計算フラグ（１：１ヵ月計算、２：４週計算）
     * @param string  $draft_reg_flg     下書きデータ登録状態（１：下書き、２：登録）
     * @param integer $night_start_day   ４週計算用（開始日）
     * @param array   $date_ymd1         「計算」開始年月日
     * @param array   $duty_ym           勤務年月
     * @param integer $day_cnt           日数
     * @param string  $standard_id       施設基準ＩＤ
     * @param string  $group_id          グループＩＤ
     * @param array   $history_array     施設基準情報（履歴）
     *
     * @return array 様式９の計算値
     */
    function calcReportValue(
            $recomputation_flg,
            $draft_reg_flg,
            $night_start_day,
            $date_ymd1,
            $duty_ym,
            $day_cnt,
            $standard_id,
            $group_id,
            $history_array
    ) {
        extract($date_ymd1);    //「計算」開始年月日を変数化($date_y1, $date_m1, $date_d1)
        extract($duty_ym);      //勤務年月を変数化($duty_yyyy, $duty_mm)

        ///-----------------------------------------------------------------------------
        //初期値設定
        ///-----------------------------------------------------------------------------
        //４週計算用のカレンダー年月日
        if ($date_y1 == "") { $date_y1 = $duty_yyyy; };
        if ($date_m1 == "") { $date_m1 = $duty_mm; };
        if ($date_d1 == "") { $date_d1 = 1; };

        ///-----------------------------------------------------------------------------
        //終了年月
        ///-----------------------------------------------------------------------------
        $end_yyyy = (int)$duty_yyyy;
        $end_mm = (int)$duty_mm;
        $end_dd = (int)$day_cnt;
        if ($recomputation_flg == "2") {
            ///-----------------------------------------------------------------------------
            //４週計算時の年月チェック
            ///-----------------------------------------------------------------------------
            $wk_err_flg = $this->obj_report->checkYm4Week($duty_yyyy, $duty_mm, $date_y1, $date_m1);

            ///-----------------------------------------------------------------------------
            //４週計算用の終了年月日設定
            ///-----------------------------------------------------------------------------
            $night_start_day = $date_d1;
            if ($wk_err_flg == "") {
                $this->obj_report->setEndYmd4Week(
                        $night_start_day,
                        $duty_yyyy,
                        $duty_mm,
                        $end_yyyy,
                        $end_mm,
                        $end_dd
                );
            }
            $day_cnt = 28;
        } else {
            $date_d1 = 1;
        }

        ///-----------------------------------------------------------------------------
        //データ取得
        ///-----------------------------------------------------------------------------
        //様式９用　看護職員情報取得
        $data = $this->obj_report->get_report_array(
                $standard_id,
                $duty_yyyy,
                $duty_mm,
                $night_start_day,
                $end_yyyy,
                $end_mm,
                $end_dd,
                $day_cnt,
                $history_array[0],
                $group_id,
                3,    //予定と実績の組合せ
                $draft_reg_flg
        );

        ///-----------------------------------------------------------------------------
        //月平均夜勤時間(前日までは実績、当日以降は予定での計算)の算出
        ///-----------------------------------------------------------------------------
        //まず、月延べ夜勤時間数を算出
        $this->obj_report->calcNightSumTime(
                $data["data_array"],
                $day_cnt,
                $history_array[0]["labor_cnt"],
                $sum_night_b,
                $sum_night_d,
                $sum_night_e
        );

        //月平均夜勤時間を算出
        $night_sumtime = $this->obj_report->calcNightSumTimeAverage(
                "",                                 //ＥＸＣＥＬ表示フラグ（１：ＥＸＣＥＬ表示）
                $data["data_sub_array"],            //看護補助者情報
                $history_array[0]["report_kbn"],    //届出区分
                $day_cnt,                           //日数
                $history_array[0]["labor_cnt"],     //常勤職員の週所定労働時間
                $sum_night_b,                       //夜勤従事者数(夜勤ありの職員数)〔Ｂ〕
                $sum_night_d,                       //月延べ夜勤時間数の計〔Ｄ〕
                $sum_night_e,                       //夜勤専従者及び月16時間以下の者の夜勤時間数〔Ｅ〕
                $sum_sub_night_b,                   //夜勤従事者数(夜勤ありの職員数)〔Ｂ〕
                $sum_sub_night_d,                   //月延べ夜勤時間数の計〔Ｄ〕
                $sum_sub_night_e                    //夜勤専従者及び月16時間以下の者の夜勤時間数〔Ｅ〕
        );

        $group_array = $this->obj->get_duty_shift_group_array($group_id, "", array());

        // 届出区分WK
        $wk_report_kbn = ($history_array[0]["report_kbn"] > 900) ? $history_array[0]["report_kbn"] - 900 : $history_array[0]["report_kbn"];

        //月平均１日あたり看護配置数(実績値)と1日看護配置数(基準値)を算出
        $this->obj_report->calcNurseAssign(
                $history_array[0]["report_kbn"],
                $wk_report_kbn,
                $group_array[0]["hosp_patient_cnt"],
                $data["nurse_sumtime"],
                $data["sub_nurse_sumtime"],
                $day_cnt,
                $history_array[0]["nurse_staffing_cnt"],
                $sum_night_c,
                $nurse_standard_assign,
                $nurse_actual_assign
        );


        if ($history_array[0]["report_kbn"] == REPORT_KBN_CURE_BASIC1
                || $history_array[0]["report_kbn"] == REPORT_KBN_CURE_BASIC2
                || $history_array[0]["report_kbn"] == REPORT_KBN_SPECIFIC
                || $history_array[0]["report_kbn"] == REPORT_KBN_SPECIFIC_PLUS) {
            $long_term_care_flg = true;
        } else {
            $long_term_care_flg = false;
        }

        //1日看護補助配置数(基準値)を算出
        $this->obj_report->calcAssistanceStandard(
                $history_array[0]["report_kbn"],
                $group_array[0]["hosp_patient_cnt"],
                $history_array[0]["nursing_assistance_cnt"],
                $history_array[0]["acute_assistance_cnt"],
                $history_array[0]["assistance_staffing_cnt"],
                $long_term_care_flg,
                $wk_report_kbn,
                sprintf("%s%02d", $duty_yyyy, $duty_mm),
                $report_kbn2,
                $assistance_standard_assign
        );

        //月平均１日あたり看護補助配置数(実績値)を算出
        $ave_duty_times = $nurse_standard_assign * 8 * $day_cnt;
        $this->obj_report->calcAssistanceActual(
                $history_array[0]["report_kbn"],
                $ave_duty_times,
                $day_cnt,
                $data["assistance_nurse_sumtime"],
                $sum_night_c,
                $sum_sub_night_g,
                $assistance_actual_assign
        );

        $values = compact(
                "night_sumtime",
                "nurse_actual_assign",
                "nurse_standard_assign",
                "assistance_actual_assign",
                "assistance_standard_assign"
        );

        return $values;
    }

    //繰越公休残日数 start
    /**
     * set_hol_remain_info
     * 繰越公休残日数用のフラグ情報を設定する
     * @param obj $hol_remain_info 繰越公休残日数用クラス
     * @return なし
     *
     */
    function set_hol_remain_info($hol_remain_info) {
        $this->hol_remain_info = $hol_remain_info;
    }


    /**
     * get_arr_rslt_add_btn
     * 追加分のボタンを配列に設定して返す
     * @return array 追加分のボタンを配列
     *
     */
    function get_arr_rslt_add_btn() {
        $arr_rslt_btn = array();

        $i=0;
        //繰越公休残調整
        if ($this->hol_remain_info->transfer_holiday_days_flg == "t" && $this->hol_remain_info->charge_flg) {
            $arr_rslt_btn[$i]["value"] = "繰越公休残調整";
            $arr_rslt_btn[$i]["str"] = "<input type=\"button\" value=\"繰越公休残調整\" onClick=\"hol_remain(1);\" style=\"WIDTH: 140px; HEIGHT: 22px;\">";

            $i++;
        }
        //当初予定表示
        // 当初予定ボタン表示／非表示
        $conf = new Cmx_SystemConfig();

        if ($conf->get('shift.tosYotei_button_flg') == "t") {
            // 当初予定表示状態
            if (isset($_GET["yotei"])) {
                $disp_tosYotei_flg = $_GET["yotei"];
                // 当初予定表示状態を反転（反転はここのみで処理する）
                if ($disp_tosYotei_flg == "t") {
                    $disp_tosYotei_flg = "f";
                } else {
                    $disp_tosYotei_flg = "t";
                }
            } else {
                //  初期表示を「当初予定表示」ボタンにする
                $disp_tosYotei_flg = "f";   //初期表示
            }
            if ($disp_tosYotei_flg == "t") {
                $arr_rslt_btn[$i]['value'] = "勤務予定表示";
            } else {
                $arr_rslt_btn[$i]['value'] = "当初予定表示";
            }
            $arr_rslt_btn[$i]["str"] = "<input type=\"button\" id=\"yotei\" value=\"{$arr_rslt_btn[$i]['value']}\" onClick=\"switch_yotei();\" style=\"WIDTH: 140px; HEIGHT: 22px;\">";

            $i++;
        }

        //年次有給休暇簿
        if (phpversion() >= "5.1" && $conf->get('shift.year_paid_hol_btn_flg') == "t" && $this->hol_remain_info->charge_flg) {
            $arr_rslt_btn[$i]["value"] = get_settings_value("kintai_vacation_paid_hol_title", "年次休暇簿");
            $arr_rslt_btn[$i]["str"] = "<input type=\"button\" value=\"{$arr_rslt_btn[$i]['value']}\" onClick=\"openStaffList();\" style=\"WIDTH: 140px; HEIGHT: 22px;\">";
            $i++;
        }
        //打刻エラーリスト→打刻チェック 20140106
        if ($conf->get("shift.timecard_error_btn_flg") == "t" && $this->hol_remain_info->charge_flg) {
            $arr_rslt_btn[$i]["value"] = "打刻チェック";
            $arr_rslt_btn[$i]["str"] = "<input type=\"button\" value=\"打刻チェック\" onClick=\"timecard_error_list();\" style=\"WIDTH: 140px; HEIGHT: 22px;\">";
        }
        return $arr_rslt_btn;
    }

    //繰越公休残日数 end
}
?>