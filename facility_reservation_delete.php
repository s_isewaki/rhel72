<?
/*
<form name="items" action="facility_reservation_register.php" method="post">
<input type="hidden" name="path" value="<? echo($path); ?>">
<input type="hidden" name="range" value="<? echo($range); ?>">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="rsv_id" value="<? echo($rsv_id); ?>">
</form>
*/
?>
<?
//ini_set("display_errors","1");
require_once("Cmx.php");
require_once("aclg_set.php");
require("about_session.php");
require("about_authority.php");
require("schedule_repeat_common.php");
require("schedule_common.ini");
define('SM_PATH','webmail/');
require("webmail/config/config.php");
require("facility_common.ini");

// ファイル名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 設備予約権限のチェック
$checkauth = check_authority($session, 11, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// トランザクションを開始
pg_query($con, "begin");

// ログインユーザの職員情報を取得
$sql = "select emp_id, emp_lt_nm, emp_ft_nm, get_mail_login_id(emp_id) from empmst";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");
$emp_nm = pg_fetch_result($sel, 0, "emp_lt_nm") . pg_fetch_result($sel, 0, "emp_ft_nm");
$emp_login_id = pg_fetch_result($sel, 0, 3);

$table_name = ($original_timeless != "t") ? "reservation" : "reservation2";
// カテゴリ情報と設備情報を取得
$sql = "select fclcate_id, facility_id from $table_name";
$cond = "where reservation_id = $rsv_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$cate_id = pg_fetch_result($sel, 0, "fclcate_id");
$facility_id = pg_fetch_result($sel, 0, "facility_id");

// 削除用条件

// 繰返しなし、同時予約なし
if ($repeat_flg == "f" && $sync_flg == "f") {
	$cond = "where reservation_id = $rsv_id";
} else {
	$cond = "where emp_id = (select emp_id from $table_name where reservation_id = '$rsv_id') ";
	$cond .= " and reservation_del_flg = 'f' ";
	$cond .= " and reg_time = '$reg_time' ";
	// 繰返しオプション
	if ($repeat_flg == "f" || $rptopt == "1") {
		// 1件のみ
		$cond .= " and date = '$year_old$month_old$day_old' ";
	} else if ($rptopt == "2") {
		// 全て
		;
	} else if ($rptopt == "3") {
		// 将来の予定のみ
		$cond .= " and date >= '$year_old$month_old$day_old' ";
	}

	// 同時予約オプション 当施設・設備のみ
	if ($sync_flg == "f" || $syncopt == "1") {
		$cond .= " and fclcate_id = $cate_id_old and facility_id = $facility_id_old ";
	}

}
//echo($cond);

// 予約レコードを論理削除
$sql = "update $table_name set";
$set = array("reservation_del_flg");
$setvalue = array("t");
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

$table_name2 = ($original_timeless != "t") ? "rsvpt" : "rsvpt2";
// 予約患者情報を削除
$sql = "delete from $table_name2";
$cond_del = "where reservation_id in (select reservation_id from $table_name " .$cond .")";
$del = delete_from_table($con, $sql, $cond_del, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

// スケジュール情報を削除
if ($schdopt == "1") {
	$del_schedules = array();
	// 同時予約したスケジュールIDを取得
	$schd_id = get_sync_schd_id($con, $fname, $rsv_id, $original_timeless);
	// スケジュールデータがある場合
	if ($schd_id != "") {
		// 削除対象スケジュール一覧を取得
		$del_schedules = get_repeated_schedules($con, $schd_id, $original_timeless, $rptopt, $fname, true);
	}

	$table_name3 = ($original_timeless != "t") ? "schdmst" : "schdmst2";
	$sql = "delete from $table_name3";
	// 削除用条件
	$cond = "where schd_reg_id = (select emp_id from $table_name where reservation_id = '$rsv_id') ";
	$cond .= " and reg_time = '$reg_time' ";
	// 繰返しオプション
	if ($repeat_flg == "f" || $rptopt == "1") {
		// 1件のみ
		$cond .= " and schd_start_date = '$year_old$month_old$day_old' ";
	} else if ($rptopt == "2") {
		// 全て
		;
	} else if ($rptopt == "3") {
		// 将来の予定のみ
		$cond .= " and schd_start_date >= '$year_old$month_old$day_old' ";
	}

	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// トランザクションをコミット
pg_query($con, "commit");

//echo("cnt=".count($del_schedules));
if ($schdopt == "1" && count($del_schedules) > 0) {
	// 行先一覧を配列に格納
	$places = get_places($con, $fname);

	$schedules_for_mail = array_filter($del_schedules, "is_approved_schedule");
	usort($schedules_for_mail, "sort_by_emp_id_datetime");

	$schedule_count = count($schedules_for_mail);
	if ($schedule_count > 0) {
		$subject = "[CoMedix] スケジュール削除のお知らせ";
		$additional_headers = "From: ".mb_encode_mimeheader (mb_convert_encoding($emp_nm,"ISO-2022-JP","AUTO")) ." <$emp_login_id@$domain>";
		$message = "下記の承認済みスケジュールが削除されました。\n\n";
		$message .= "登録者　：{$emp_nm}\n";

		// 職員IDをキーとして処理
		$old_emp_id = "";

		foreach ($schedules_for_mail as $tmp_schedule) {

			$tmp_emp_id = $tmp_schedule["emp_id"];
			// 初回のみキー設定
			if ($old_emp_id == "") {
				$old_emp_id = $tmp_emp_id;
			}

			// 職員が変わった場合
			if ($old_emp_id != $tmp_emp_id) {
				// メールアドレス取得
				$to_addr = get_mail_addr($con, $fname, $old_emp_id, $domain);
				// メール送信
				mb_send_mail($to_addr, $subject, $message, $additional_headers);
				// キー設定
				$old_emp_id = $tmp_emp_id;
				// 初期化
				$message = "下記の承認済みスケジュールが削除されました。\n\n";
				$message .= "登録者　：{$emp_nm}\n";
			}

			$tmp_place_name = ($tmp_schedule["place_id"] == "0") ? "その他" : $places[$tmp_schedule["place_id"]];
			if ($tmp_place_name != "") {
				$tmp_place_name .= " {$tmp_schedule["place_detail"]}";
			} else {
				$tmp_place_name = "{$tmp_schedule["place_detail"]}";
			}
			$mail_schd_detail = str_replace("\n", "\n　　　　　", $tmp_schedule["detail"]);
	//		if ($schedule_count >= 2) {
				$message .= "-----------------------------------------------------\n";
	//		}
			$message .= "タイトル：{$tmp_schedule["title"]}\n";
			$message .= "行先　　：{$tmp_place_name}\n";
			$message .= "日時　　：{$tmp_schedule["date"]} {$tmp_schedule["time"]}\n";
			$message .= "詳細　　：{$mail_schd_detail}\n";
		}

		// メールアドレス取得
		$to_addr = get_mail_addr($con, $fname, $old_emp_id, $domain);

		mb_send_mail($to_addr, $subject, $message, $additional_headers);
	}
}

// データベース接続を閉じる
pg_close($con);

// 親画面を更新
if ($rptopt >= "2") {
	$year = "$year_old";
	$month = "$month_old";
	$day = "$day_old";
}
$date = mktime(0, 0, 0, $month, $day, $year);
switch ($path) {
case "1":
	$file = "facility_menu.php?session=$session&range=$range&date=$date";
	break;
case "2":
	$file = "facility_week.php?session=$session&range=$range&date=$date";
	break;
case "3":
	$file = "facility_month.php?session=$session&range=$range&date=$date";
	break;
case "4":
	$file = "facility_year.php?session=$session&range=$range&date=$date";
	break;
case "5":
	$file = "facility_week_chart.php?session=$session&range=$range&date=$date&cate_id=$cate_id&fcl_id=$facility_id";
	break;
case "6":
	$file = "left.php?session=$session";
	break;
}
echo("<script language='javascript'>opener.location.href = '$file'; self.close();</script>");

function is_approved_schedule($schd) {
	return ($schd["approve_status"] == "3");
}

function sort_by_emp_id_datetime($schd1, $schd2) {
	if ($schd1["emp_id"] != $schd2["emp_id"]) {
		return strcmp($schd1["emp_id"], $schd2["emp_id"]);
	}
	if ($schd1["date"] != $schd2["date"]) {
		return strcmp($schd1["date"], $schd2["date"]);
	}
	if ($schd1["timeless"] != $schd2["timeless"]) {
		return strcmp($schd1["timeless"], $schd2["timeless"]) * -1;
	}
	if ($schd1["time"] != $schd2["time"]) {
		return strcmp($schd1["time"], $schd2["time"]);
	}
	return strcmp($schd1["id"], $schd2["id"]);
}

function get_mail_addr($con, $fname, $emp_id, $domain) {
	$sql = "select get_mail_login_id(emp_id) as mail_login_id, emp_lt_nm, emp_ft_nm from empmst";
	$cond = "where emp_id = '$emp_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$mail_addr = pg_fetch_result($sel, 0, "mail_login_id");
	$emp_nm = pg_fetch_result($sel, 0, "emp_lt_nm") . pg_fetch_result($sel, 0, "emp_ft_nm");
	$to_addr = mb_encode_mimeheader (mb_convert_encoding($emp_nm,"ISO-2022-JP","AUTO")) ." <$mail_addr@$domain>";
	return $to_addr;
}
?>
