<?php
require_once("about_comedix.php");
require_once("show_select_values.ini");
require_once("show_class_name.ini");
require_once("get_values.ini");
require_once("webmail_quota_functions.php");
require_once("webmail_alias_functions.php");

$fname = $_SERVER["PHP_SELF"];

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 18, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 職員登録権限を取得
$reg_auth = check_authority($session, 19, $fname);

// データベースに接続
$con = connect2db($fname);

// 環境設定情報を取得
$sql = "select site_id, use_cyrus, hide_passwd from config";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}
$site_id = pg_fetch_result($sel, 0, "site_id");
$use_cyrus = pg_fetch_result($sel, 0, "use_cyrus");
$hide_passwd = pg_fetch_result($sel, 0, "hide_passwd");

$arr_class_name = get_class_name_array($con, $fname);

// 初期表示時
if ($back != "t") {

    // 職員情報を取得
    $sql = "SELECT * FROM empmst JOIN authmst USING (emp_id) JOIN login USING (emp_id) JOIN option USING (emp_id) LEFT JOIN emp_relation USING (emp_id) WHERE emp_id='{$emp_id}'";
    $sel_emp = select_from_table($con,$sql,"",$fname);
    if ($sel_emp == 0) {
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $emp = pg_fetch_assoc($sel_emp);

    $personal_id    = $emp["emp_personal_id"];
    $cls            = $emp["emp_class"];
    $atrb           = $emp["emp_attribute"];
    $dept           = $emp["emp_dept"];
    $job            = $emp["emp_job"];
    $status         = $emp["emp_st"];
    $lt_nm          = $emp["emp_lt_nm"];
    $ft_nm          = $emp["emp_ft_nm"];
    $lt_kana_nm     = $emp["emp_kn_lt_nm"];
    $ft_kana_nm     = $emp["emp_kn_ft_nm"];

    $sex            = $emp["emp_sex"];
    $room           = $emp["emp_room"];
    $idm            = $emp["emp_idm"];
    $profile        = $emp["emp_profile"];

    $updated_on     = $emp["updated_on"];

    $birth_yr       = substr($emp["emp_birth"], 0, 4);
    $birth_mon      = substr($emp["emp_birth"], 4, 2);
    $birth_day      = substr($emp["emp_birth"], 6, 2);
    $entry_yr       = substr($emp["emp_join"], 0, 4);
    $entry_mon      = substr($emp["emp_join"], 4, 2);
    $entry_day      = substr($emp["emp_join"], 6, 2);
    $retire_yr      = substr($emp["emp_retire"], 0, 4);
    $retire_mon     = substr($emp["emp_retire"], 4, 2);
    $retire_day     = substr($emp["emp_retire"], 6, 2);
    $webmail_quota  = $emp["webmail_quota"];

    // 権限情報
    $aprv           = $emp["emp_aprv_auth"];
    $suspend        = $emp["emp_del_flg"];
    $group_id       = $emp["group_id"];

    // ログインID・パスワード
    $id             = $emp["emp_login_id"];
    $pass           = $emp["emp_login_pass"];
    $mail_id        = $emp["emp_login_mail"];
    $pass_date      = $emp["pass_change_date"];

    // パスワードをマスクする設定の場合、ソースにも出力しない
    if ($hide_passwd == "t") {
        $pass = "********";
    }

    // 兼務所属情報を取得
    $sql = "select emp_class, emp_attribute, emp_dept, emp_room, emp_st from concurrent";
    $cond = "where emp_id = '$emp_id'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $concurrent = pg_num_rows($sel);
    for ($i = 1;  $i <= $concurrent; $i++) {
        $class_var_name = "o_cls$i";
        $atrb_var_name = "o_atrb$i";
        $dept_var_name = "o_dept$i";
        $room_var_name = "o_room$i";
        $status_var_name = "o_status$i";

        $$class_var_name = pg_fetch_result($sel, $i - 1, "emp_class");
        $$atrb_var_name = pg_fetch_result($sel, $i - 1, "emp_attribute");
        $$dept_var_name = pg_fetch_result($sel, $i - 1, "emp_dept");
        $$room_var_name = pg_fetch_result($sel, $i - 1, "emp_room");
        $kenmu[$i]['status_var_name'] = pg_fetch_result($sel, $i - 1, "emp_st");
        //$$status_var_name = pg_fetch_result($sel, $i - 1, "emp_st");
    }
}

// 役職変更履歴を取得
$sql = "select h.*,st.st_nm from st_history h JOIN stmst st USING (st_id)";
$cond = "where emp_id='$emp_id' ORDER BY histdate DESC";
$sel_sthist = select_from_table($con, $sql, $cond, $fname);
if ($sel_sthist == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// 名前変更履歴を取得
$sql = "select * from name_history";
$cond = "where emp_id='$emp_id' ORDER BY histdate DESC";
$sel_namehist = select_from_table($con, $sql, $cond, $fname);
if ($sel_namehist == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// 所属変更履歴を取得
$sql = "SELECT h.*,c.class_nm,a.atrb_nm,d.dept_nm,r.room_nm
        FROM class_history h
        JOIN classmst c USING (class_id)
        JOIN atrbmst a USING (atrb_id)
        JOIN deptmst d USING (dept_id)
        LEFT JOIN classroom r USING (room_id)";
$cond = "where emp_id='$emp_id' ORDER BY histdate DESC";
$sel_classhist = select_from_table($con, $sql, $cond, $fname);
if ($sel_classhist == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// 職種変更履歴を取得
$sql = "select h.*,j.job_nm from job_history h JOIN jobmst j USING (job_id)";
$cond = "where emp_id='$emp_id' ORDER BY histdate DESC";
$sel_jobhist = select_from_table($con, $sql, $cond, $fname);
if ($sel_jobhist == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// 部門一覧を取得
$sql = "select * from classmst";
$cond = "where class_del_flg = 'f' order by order_no";
$sel_class = select_from_table($con, $sql, $cond, $fname);
if ($sel_class == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// 課一覧を取得
$sql = "select * from atrbmst";
$cond = "where atrb_del_flg = 'f' order by order_no";
$sel_atrb = select_from_table($con, $sql, $cond, $fname);
if ($sel_atrb == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 科一覧を取得
$sql = "select * from deptmst";
$cond = "where dept_del_flg = 'f' order by order_no";
$sel_dept = select_from_table($con, $sql, $cond, $fname);
if ($sel_dept == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 室一覧を取得
if ($arr_class_name["class_cnt"] == 4) {
    $sql = "select * from classroom";
    $cond = "where room_del_flg = 'f' order by order_no";
    $sel_room = select_from_table($con, $sql, $cond, $fname);
    if ($sel_room == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
        exit;
    }
} else {
    $sel_room = null;
}

// グループ一覧を配列に格納
$sql = "select group_id, group_nm from authgroup";
$cond = "order by group_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$groups = array();
while ($row = pg_fetch_array($sel)) {
    $groups[$row["group_id"]] = $row["group_nm"];
}

$webmail_default_quota = webmail_quota_get_default();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 職員登録 | 基本</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/jquery/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<script type="text/javascript">
function initPage() {
    if (window.opera) document.mainform.photofile.size = 8;

    setAttributeOptions(document.mainform.cls, document.mainform.atrb, document.mainform.dept, document.mainform.room, '<? echo($atrb); ?>', '<? echo($dept); ?>', '<? echo($room); ?>');
<?
for ($i = 1; $i <= $concurrent; $i++) {
    $atrb_var_name = "o_atrb$i";
    $dept_var_name = "o_dept$i";
    $room_var_name = "o_room$i";
    if ($back == "t") {
        $kenmu[$i]['status_var_name'] = $_REQUEST["o_status".$i];
    }
    echo("\tsetAttributeOptions(document.mainform.o_cls$i, document.mainform.o_atrb$i, document.mainform.o_dept$i, document.mainform.o_room$i, '{$$atrb_var_name}', '{$$dept_var_name}', '{$$room_var_name}');\n");
}
?>
}

function setAttributeOptions(class_box, atrb_box, dept_box, room_box, atrb_id, dept_id, room_id) {
    deleteAllOptions(atrb_box);

    var class_id = class_box.value;
<? while ($row = pg_fetch_array($sel_atrb)) { ?>
    if (class_id == '<? echo $row["class_id"]; ?>') {
        addOption(atrb_box, '<? echo $row["atrb_id"]; ?>', '<? echo $row["atrb_nm"]; ?>', atrb_id);
    }
<? } ?>

    if (atrb_box.options.length == 0) {
        addOption(atrb_box, '0', '（未登録）');
    }

    setDepartmentOptions(atrb_box, dept_box, room_box, dept_id, room_id);
}

function setDepartmentOptions(atrb_box, dept_box, room_box, dept_id, room_id) {
    deleteAllOptions(dept_box);

    var atrb_id = atrb_box.value;
<? while ($row = pg_fetch_array($sel_dept)) { ?>
    if (atrb_id == '<? echo $row["atrb_id"]; ?>') {
        addOption(dept_box, '<? echo $row["dept_id"]; ?>', '<? echo $row["dept_nm"]; ?>', dept_id);
    }
<? } ?>

    if (dept_box.options.length == 0) {
        addOption(dept_box, '0', '（未登録）');
    }

    setRoomOptions(dept_box, room_box, room_id);
}

function setRoomOptions(dept_box, room_box, room_id) {
    if (!room_box) {
        return;
    }

    deleteAllOptions(room_box);

    var dept_id = dept_box.value;
<? while ($row = pg_fetch_array($sel_room)) { ?>
    if (dept_id == '<? echo $row["dept_id"]; ?>') {
        if (room_box.options.length == 0) {
            addOption(room_box, '0', '　　　　　');
        }
        addOption(room_box, '<? echo $row["room_id"]; ?>', '<? echo $row["room_nm"]; ?>', room_id);
    }
<? } ?>

    if (room_box.options.length == 0) {
        addOption(room_box, '0', '（未登録）');
    }
}

function deleteAllOptions(box) {
    for (var i = box.length - 1; i >= 0; i--) {
        box.options[i] = null;
    }
}

function addOption(box, value, text, selected) {
    var opt = document.createElement("option");
    opt.value = value;
    opt.text = text;
    if (selected == value) {
        opt.selected = true;
    }
    box.options[box.length] = opt;
    try {box.style.fontSize = 'auto';} catch (e) {}
    box.style.overflow = 'auto';
}

function setConcurrent(concurrent) {
    if (document.mainform.updatephoto.value == 't' &&
                !confirm('写真の選択が無効になります。よろしいですか？')) {
        return;
    }

    document.mainform.concurrent.value = concurrent;
    document.mainform.action = 'employee_detail.php';
    try {
        document.mainform.submit();
    } catch (e) {
        alert('エラーが発生しました。恐れ入りますが、画面を再表示してください。');
    }
}

function readFelica() {
    var felica_anchor = document.getElementById('felica_anchor');
    if (!felica_anchor) {
        alert('プラグインがインストールされていないため利用できません。');
        return;
    }
    felica_anchor.click();
    document.mainform.idm.value = document.mainform.idm_hid.value;
}

function setPhoto() {
    var filename = document.mainform.photofile.value;
    if (!filename) {
        document.mainform.updatephoto.value = 'f';
        return;
    }

    var extension = filename.substring(filename.lastIndexOf('.') + 1).toLowerCase();
    if (extension != 'jpg' && extension != 'jpeg') {
        alert('登録可能な画像はJPEG形式のみです。写真は更新されません。');
        document.mainform.updatephoto.value = 'f';
        return;
    }

    document.mainform.updatephoto.value = 't';
    document.mainform.clearphoto.checked = false;

    if (document.all) {
        document.getElementById('photo').src = filename;
    }
}

/* 役職履歴 */
function st_history_delete(id) {
    if (confirm('削除してよろしいでしょうか?')) {
        document.sthist.action = 'employee_st_history_delete.php';
        document.sthist.st_history_id.value = id;
        document.sthist.submit();
    }
}
function st_history_edit(id) {
    if (id) {
        document.sthist.st_history_id.value = id;
    }
    document.sthist.submit();
}

/* 職種履歴 */
function job_history_delete(id) {
    if (confirm('削除してよろしいでしょうか?')) {
        document.jobhist.action = 'employee_job_history_delete.php';
        document.jobhist.job_history_id.value = id;
        document.jobhist.submit();
    }
}
function job_history_edit(id) {
    if (id) {
        document.jobhist.job_history_id.value = id;
    }
    document.jobhist.submit();
}

/* 部署履歴 */
function class_history_delete(id) {
    if (confirm('削除してよろしいでしょうか?')) {
        document.classhist.action = 'employee_class_history_delete.php';
        document.classhist.class_history_id.value = id;
        document.classhist.submit();
    }
}
function class_history_edit(id) {
    if (id) {
        document.classhist.class_history_id.value = id;
    }
    document.classhist.submit();
}

/* 氏名履歴 */
function name_history_delete(id) {
    if (confirm('削除してよろしいでしょうか?')) {
        document.namehist.action = 'employee_name_history_delete.php';
        document.namehist.name_history_id.value = id;
        document.namehist.submit();
    }
}
function name_history_edit(id) {
    if (id) {
        document.namehist.name_history_id.value = id;
    }
    document.namehist.submit();
}

/**
 * jQuery
 */
$(function(){
    /**
     *  履歴タブ
     */
    $('div.hist').hide();
    $('#menu_tab>li').removeClass('selected');

    var hist = $.cookie('_employee_detail_history');
    switch (hist) {
        case 'name':
            $('#namehist').show();
            $('#namehist_tab').addClass('selected');
            break;
        case 'job':
            $('#jobhist').show();
            $('#jobhist_tab').addClass('selected');
            break;
        case 'st':
            $('#sthist').show();
            $('#sthist_tab').addClass('selected');
            break;
        case 'class':
        default:
            $('#classhist').show();
            $('#classhist_tab').addClass('selected');
            break;
    }

    /**
     * 連携更新しない
     */
    $('#deadlink').click(function() {
        $('.js-deadlink').prop('checked', $(this).prop('checked'));
    });
    $('.js-deadlink').click(function() {
        var flg = true;
        $('.js-deadlink').each(function(i, e) {
            if (! $(this).prop('checked')) {
                flg = false;
                return;
            }
        });
        $('#deadlink').prop('checked', flg);
    });

    /**
     * パスワードリセット
     */
    $('.js-passdate-reset').click(function() {
        if (!confirm('パスワード変更日をリセットしてもよろしいでしょうか？\n※この操作は元に戻すことができません')) {
            return false;
        }

        $.ajax({
            url: 'employee_passdate_reset_ajax.php',
            type: 'post',
            data: {
                emp: $(this).data('emp')
            }
        }).done(function() {
            alert('変更日をリセットしました');
            location.reload();
        });
    });
});

function history_tab(tab) {
    $('div.hist').hide();
    $('#menu_tab>li').removeClass('selected');
    $('#'+tab+'hist').show();
    $('#'+tab+'hist_tab').addClass('selected');
    $.cookie('_employee_detail_history', tab);
}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/employee/employee.css">
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="employee_info_menu.php?session=<? echo($session); ?>"><img src="img/icon/b26.gif" width="32" height="32" border="0" alt="職員登録"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="employee_info_menu.php?session=<? echo($session); ?>"><b>職員登録</b></a></font></td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom:2px solid #5279a5;margin-bottom:5px;">
<tr height="22">
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_info_menu.php?session=<? echo($session); ?>&key1=<? echo(urlencode($key1)); ?>&key2=<? echo(urlencode($key2)); ?>&page=<? echo($page); ?>&view=<? echo($view); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">検索</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_list.php?session=<? echo($session); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#5279a5"><a href="employee_detail.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>基本</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="employee_contact_setting.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">連絡先</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_condition_setting.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務条件</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_auth_setting.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_display_setting.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&view=<? echo($view); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示設定</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_menu_setting.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&view=<? echo($view); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メニュー設定</font></a></td>
<? if (webmail_alias_enabled()) { ?>
<td width="5">&nbsp;</td>
<td width="130" align="center" bgcolor="#bdd1e7"><a href="employee_alias_setting.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&view=<? echo($view); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メール転送設定</font></a></td>
<? } ?>
<td width="">&nbsp;</td>
</tr>
</table>

<? if ($site_id == "" && $use_cyrus == "t") { ?>
<div><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="red">※ログインIDにアルファベットを含まない職員はウェブメール機能を利用できません</font></div>
<? } ?>

<form name="mainform" action="employee_update_confirm.php" method="post" enctype="multipart/form-data">

<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
    <td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員ID</font></td>
    <td><input name="personal_id" type="text" maxlength="12" value="<? echo($personal_id); ?>" style="ime-mode:inactive;"> <i style="font-size:10px; color:#ccc;"><? eh($emp_id); ?></i></td>
    <td width="15%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">更新日時</font></td>
    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php eh(substr($updated_on,0,19))?></font></td>
    <td rowspan="12" align="center" valign="top">
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
<?
if ($updatephoto == "t") {
    $photo_bg_color = "#fefe83";
    $photo_src = "img/spacer.gif";
} else {
    $photo_bg_color = "white";
    $photo_src = (file_exists("profile/{$emp_id}.jpg")) ? "profile/{$emp_id}.jpg" : "img/noprofile.jpg";
}
?>
        <tr>
        <td align="center" style="border-style:none;"><img id="photo" src="<? echo($photo_src); ?>" height="152" width="114" style="border:solid 1px #5279a5; margin:5px auto 2px; background-color:<? echo($photo_bg_color); ?>"></td>
        </tr>
        <tr>
        <td align="center" style="border-style:none;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
        <input type="file" name="photofile" size="13" style="width=:135px;" onchange="setPhoto();" tabindex="-1"><br>
        <input type="checkbox" name="clearphoto" value="t"<? if ($clearphoto == "t") {echo(" checked");} ?> tabindex="-1">写真を削除する
        </font></td>
        </tr>
        <tr>
        <td style="padding:5px 2px 2px; border-style:none;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">プロフィール<br><textarea name="profile" style="width:100%; height:116px;" tabindex="-1"><? echo($profile); ?></textarea></font></td>
        </tr>
        </table>
    </td>
</tr>
<tr height="22">
<td width="15%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">苗字（漢字）</font></td>
<td width="23%"><input name="lt_nm" type="text" maxlength="10" value="<? echo($lt_nm); ?>" style="ime-mode:active;"></td>
<td width="15%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">名前（漢字）</font></td>
<td width="23%"><input name="ft_nm" type="text" maxlength="10" value="<? echo($ft_nm); ?>" style="ime-mode:active;"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">苗字（ひらがな）</font></td>
<td><input name="lt_kana_nm" type="text" maxlength="10" value="<? echo($lt_kana_nm); ?>" style="ime-mode:active;"></td>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">名前（ひらがな）</font></td>
<td><input name="ft_kana_nm" type="text" maxlength="10" value="<? echo($ft_kana_nm); ?>" style="ime-mode:active;"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ログインID</font></td>
<td><input name="id" type="text" maxlength="20" value="<? echo($id); ?>" style="ime-mode:inactive;"></td>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">パスワード</font></td>
<td>
    <input name="pass" type="<? if ($hide_passwd == "t") {echo("password");} else {echo("text");} ?>" value="<? echo($pass); ?>" style="ime-mode:inactive;">
    <?if ($pass_date) {?>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" style="font-size:0.7em;">変更日: <?=h($pass_date)?></font>
    <button type="button" style="font-size:0.7em;" class="js-passdate-reset" data-emp="<?=h($emp_id)?>">変更日リセット</button>
        <?}?>
</td>
</tr>
<tr height="22">
<? if ($site_id != "") { ?>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メールID</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input name="mail_id" type="text" maxlength="20" value="<? echo($mail_id); ?>" style="ime-mode:inactive;"><? if ($use_cyrus == "t") {echo("_$site_id");} ?></font></td>
<? } ?>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">性別</font></td>
<td<? if ($site_id == "") {echo(" colspan=\"3\"");} ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="sex" value="1"<? if ($sex == "1") {echo(" checked");} ?>>男性
<input type="radio" name="sex" value="2"<? if ($sex == "2") {echo(" checked");} ?>>女性
<input type="radio" name="sex" value="3"<? if ($sex == "3") {echo(" checked");} ?>>未設定
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">生年月日</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="birth_yr"><? show_select_years(100, $birth_yr, true); ?></select>/<select name="birth_mon"><? show_select_months($birth_mon, true); ?></select>/<select name="birth_day"><? show_select_days($birth_day, true); ?></select></font></td>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入職日</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="entry_yr"><? show_select_years(100, $entry_yr, true, true); ?></select>/<select name="entry_mon"><? show_select_months($entry_mon, true) ?></select>/<select name="entry_day"><? show_select_days($entry_day, true); ?></select></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属<? echo($arr_class_name[0]); ?></font></td>
<td><select name="cls" onchange="setAttributeOptions(this.form.cls, this.form.atrb, this.form.dept, this.form.room);">
<?
while ($row = pg_fetch_array($sel_class)) {
    echo("<option value=\"{$row["class_id"]}\"");
    if ($row["class_id"] == $cls) {
        echo(" selected");
    }
    echo(">{$row["class_nm"]}");
}
?>
</select></td>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属<? echo($arr_class_name[1]); ?></font></td>
<td><select name="atrb" onchange="setDepartmentOptions(this.form.atrb, this.form.dept, this.form.room);"></select></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属<? echo($arr_class_name[2]); ?></font></td>
<td<? if ($arr_class_name["class_cnt"] == 3) {echo(" colspan=\"3\"");} ?>><select name="dept" onchange="setRoomOptions(this.form.dept, this.form.room);"></select></td>
<? if ($arr_class_name["class_cnt"] == 4) { ?>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属<? echo($arr_class_name[3]); ?></font></td>
<td><select name="room"></select></td>
<? } ?>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">役職</font></td>
<td><select name="status"><? show_select_status($con, $status, $fname); ?></select></td>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職種</font></td>
<td><select name="job"><? show_select_job($con, $job, $fname); ?></select></td>
</tr>

<tr height="22">
    <td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限グループ</font></td>
    <td colspan="3">
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="group_id">
<option value="">　　　　　</option>
<?
foreach ($groups as $tmp_group_id => $tmp_group_nm) {
    echo("<option value=\"$tmp_group_id\"");
    if ($tmp_group_id == $group_id) {
        echo(" selected");
    }
    echo(">$tmp_group_nm\n");
}
?>
</select></font>
    </td>
</tr>


<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ICカードIDm</font></td>
<td colspan="3">
<input type="text" name="idm" value="<? echo($idm); ?>" size="30">
<input type="button" value="カード読み込み" onclick="readFelica();">
<span id="FeliCa">
<!--
<a id="felica_anchor" href="felica:">
<input type="hidden" name="idm_hid" />
<polling target="cTafzB6PR9ZGl7MmdfBDm37wfzoBABhLg6mKuzDiXFAH" result="idm_hid" />
</a>
-->
</span>
<div><font size="2" face="ＭＳ Ｐゴシック, Osaka" class="j10" color="red">
注意：<br>
・ICカードを読み込むためには，felica対応のカードリーダを接続する必要があります。<br>
・カード読み込みができるのは、IEブラウザエクステンションのプラグインをインストールしたIEブラウザのみです。
</font></div>
</td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">決裁者</font></td>
<td colspan="3"><input type="checkbox" name="aprv" value="t"<? if ($aprv == "t") {echo(" checked");} ?>></td>
</tr>
<? if (version_compare(PHP_VERSION, '5.0.0') >= 0 && substr(pg_parameter_status($con, 'server_version'),0,1) >= 8) { ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員連携</font></td>
<td colspan="3">
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
    <label><input type="checkbox" id="deadlink" <? if ($emp['emp_name_deadlink_flg'] === "t" && $emp['emp_class_deadlink_flg'] === "t" && $emp['emp_job_deadlink_flg'] === "t" && $emp['emp_st_deadlink_flg'] === "t") {echo("checked");} ?>>連携更新しない</label>
     (
     <label><input type="checkbox" class="js-deadlink" name="emp_name_deadlink_flg" value="t" <? if ($emp['emp_name_deadlink_flg'] === "t") {echo("checked");} ?>>苗字、名前の連携更新をしない</label>
     <label><input type="checkbox" class="js-deadlink" name="emp_class_deadlink_flg" value="t" <? if ($emp['emp_class_deadlink_flg'] === "t") {echo("checked");} ?>>所属の連携更新しない</label>
     <label><input type="checkbox" class="js-deadlink" name="emp_job_deadlink_flg" value="t" <? if ($emp['emp_job_deadlink_flg'] === "t") {echo("checked");} ?>>職種の連携更新しない</label>
     <label><input type="checkbox" class="js-deadlink" name="emp_st_deadlink_flg" value="t" <? if ($emp['emp_st_deadlink_flg'] === "t") {echo("checked");} ?>>役職の連携更新しない</label>
     )
     </font>
</td>
</tr>
<? } ?>
<? if ($webmail_default_quota > 0) { ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メールボックスの容量制限</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="text" name="webmail_quota" value="<? echo($webmail_quota); ?>" size="4" style="ime-mode:disabled"> MB <font color="gray">（空の場合はデフォルトの <? echo($webmail_default_quota); ?>MB となります）</font></font></td>
</tr>
<? } ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退職日</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="retire_yr"><? show_select_years(100, $retire_yr, true, true); ?></select>/<select name="retire_mon"><? show_select_months($retire_mon, true) ?></select>/<select name="retire_day"><? show_select_days($retire_day, true); ?></select></font></td>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">利用停止</font></td>
<td><input type="checkbox" name="suspend" value="t"<? if ($suspend == "t") {echo(" checked");} ?>></td>
</tr>
<?
$border_width = ($concurrent > 1) ? "2px" : "1px";
?>
<tr height="22" bgcolor="#f6f9ff">
<td colspan="3" style="border-top-width:<? echo($border_width); ?>;border-right-style:none;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">兼務所属</font></td>
<td align="right" style="border-top-width:<? echo($border_width); ?>;border-left-style:none;"><input type="button" value="追加" onclick="setConcurrent(<? echo($concurrent + 1); ?>);"></td>
</tr>
<?
for ($i = 1; $i <= $concurrent; $i++) {
    $class_var_name = "o_cls$i";
    $status_var_name = "o_status$i";
?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff" style="border-top-width:<? echo($border_width); ?>;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属<? echo($arr_class_name[0]); ?></font></td>
<td style="border-top-width:<? echo($border_width); ?>;"><select name="o_cls<? echo($i); ?>" onchange="setAttributeOptions(this.form.o_cls<? echo($i); ?>, this.form.o_atrb<? echo($i); ?>, this.form.o_dept<? echo($i); ?>, this.form.o_room<? echo($i); ?>);">
<?
pg_result_seek($sel_class, 0);
while ($row = pg_fetch_array($sel_class)) {
    echo("<option value=\"{$row["class_id"]}\"");
    if ($row["class_id"] == $$class_var_name) {
        echo(" selected");
    }
    echo(">{$row["class_nm"]}");
}
?>
</select></td>
<td align="right" bgcolor="#f6f9ff" style="border-top-width:<? echo($border_width); ?>;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属<? echo($arr_class_name[1]); ?></font></td>
<td style="border-top-width:<? echo($border_width); ?>;"><select name="o_atrb<? echo($i); ?>" onchange="setDepartmentOptions(this.form.o_atrb<? echo($i); ?>, this.form.o_dept<? echo($i); ?>, this.form.o_room<? echo($i); ?>);"></select></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属<? echo($arr_class_name[2]); ?></font></td>
<td<? if ($arr_class_name["class_cnt"] == 3) {echo(" colspan=\"3\"");} ?>><select name="o_dept<? echo($i); ?>" onchange="setRoomOptions(this.form.o_dept<? echo($i); ?>, this.form.o_room<? echo($i); ?>);"></select></td>
<? if ($arr_class_name["class_cnt"] == 4) { ?>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属<? echo($arr_class_name[3]); ?></font></td>
<td><select name="o_room<? echo($i); ?>"></select></td>
<? } ?>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff" style="border-bottom-width:<? echo($border_width); ?>;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">役職</font></td>
<td colspan="2" style="border-right-style:none;border-bottom-width:<? echo($border_width); ?>;"><select name="o_status<? echo($i); ?>"><? show_select_status($con, $kenmu[$i]['status_var_name'], $fname); ?></select></td>
<td align="right" style="border-left-style:none;border-bottom-width:<? echo($border_width); ?>;"><? if ($i == $concurrent) { ?><input type="button" value="削除" onclick="setConcurrent(<? echo($concurrent - 1); ?>);"><? } else { ?>&nbsp;<? } ?></td>
</tr>
<?
}
?>
<? if ($reg_auth == 1) { ?>
<tr height="22">
<td colspan="4" align="right" style="border-left-style:none; border-right-style:none; border-bottom-style:none;"><input type="submit" value="更新"></td>
</tr>
</table>
<? } ?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="emp_id" value="<? echo($emp_id); ?>">
<input type="hidden" name="key1" value="<? echo($key1); ?>">
<input type="hidden" name="key2" value="<? echo($key2); ?>">
<input type="hidden" name="class_cond" value="<? echo($class_cond); ?>">
<input type="hidden" name="atrb_cond" value="<? echo($atrb_cond); ?>">
<input type="hidden" name="dept_cond" value="<? echo($dept_cond); ?>">
<input type="hidden" name="room_cond" value="<? echo($room_cond); ?>">
<input type="hidden" name="job_cond" value="<? echo($job_cond); ?>">
<input type="hidden" name="page" value="<? echo($page); ?>">
<input type="hidden" name="view" value="<? echo($view); ?>">
<input type="hidden" name="emp_o" value="<? echo($emp_o); ?>">
<input type="hidden" name="concurrent" value="<? echo($concurrent); ?>">
<input type="hidden" name="back" value="t">
<input type="hidden" name="updatephoto" value="f">
</form>
</td>
</tr>
</table>

<ul id="menu_tab">
<li id="classhist_tab"><a href="javascript:void(0);" onclick="history_tab('class');">所属履歴</a></li>
<li id="sthist_tab"><a href="javascript:void(0);" onclick="history_tab('st');">役職履歴</a></li>
<li id="jobhist_tab"><a href="javascript:void(0);" onclick="history_tab('job');">職種履歴</a></li>
<li id="namehist_tab"><a href="javascript:void(0);" onclick="history_tab('name');">氏名履歴</a></li>
</ul>

<div class="hist" id="sthist">
<form name="sthist" action="employee_st_history_form.php" method="post">
<button type="button" onclick="st_history_edit()">役職の履歴を手動で追加する</button>
<? if (pg_num_rows($sel_sthist) > 0) {?>
<table class="hist">
<thead>
<tr>
    <td class="j12">役職</td>
    <td class="j12">変更日時</td>
    <td class="j12">修正</td>
    <td class="j12">削除</td>
</tr>
</thead>
<tbody>
<? while ($r = pg_fetch_assoc($sel_sthist)) {?>
<tr>
    <td class="j12"><?php eh($r["st_nm"]);?></td>
    <td class="j12"><?php eh(substr($r["histdate"],0,19))?></td>
    <td><button type="button" onclick="st_history_edit(<?php eh($r["st_history_id"]);?>)">修正</button></td>
    <td><button type="button" onclick="st_history_delete(<?php eh($r["st_history_id"]);?>)">削除</button></td>
</tr>
<?}?>
</tbody>
</table>
<?}?>
<input type="hidden" name="session" value="<?php eh($session); ?>">
<input type="hidden" name="emp_id" value="<?php eh($emp_id); ?>">
<input type="hidden" name="st_history_id" value="">
</form>
</div>

<div class="hist" id="jobhist">
<form name="jobhist" action="employee_job_history_form.php" method="post">
<button type="button" onclick="job_history_edit()">職種の履歴を手動で追加する</button>
<? if (pg_num_rows($sel_jobhist) > 0) {?>
<table class="hist">
<thead>
<tr>
    <td class="j12">職種</td>
    <td class="j12">変更日時</td>
    <td class="j12">修正</td>
    <td class="j12">削除</td>
</tr>
</thead>
<tbody>
<? while ($r = pg_fetch_assoc($sel_jobhist)) {?>
<tr>
    <td class="j12"><?php eh($r["job_nm"]);?></td>
    <td class="j12"><?php eh(substr($r["histdate"],0,19))?></td>
    <td><button type="button" onclick="job_history_edit(<?php eh($r["job_history_id"]);?>)">修正</button></td>
    <td><button type="button" onclick="job_history_delete(<?php eh($r["job_history_id"]);?>)">削除</button></td>
</tr>
<?}?>
</tbody>
</table>
<?}?>
<input type="hidden" name="session" value="<?php eh($session); ?>">
<input type="hidden" name="emp_id" value="<?php eh($emp_id); ?>">
<input type="hidden" name="job_history_id" value="">
</form>
</div>

<div class="hist" id="classhist">
<form name="classhist" action="employee_class_history_form.php" method="post">
<button type="button" onclick="class_history_edit()">所属の履歴を手動で追加する</button>
<? if (pg_num_rows($sel_classhist) > 0) {?>
<table class="hist">
<thead>
<tr>
    <td class="j12"><? eh($arr_class_name[0]); ?></td>
    <td class="j12"><? eh($arr_class_name[1]); ?></td>
    <td class="j12"><? eh($arr_class_name[2]); ?></td>
<? if ($arr_class_name["class_cnt"] == 4) { ?>
    <td class="j12"><? eh($arr_class_name[3]); ?></td>
<?}?>
    <td class="j12">変更日時</td>
    <td class="j12">修正</td>
    <td class="j12">削除</td>
</tr>
</thead>
<tbody>
<? while ($r = pg_fetch_assoc($sel_classhist)) {?>
<tr>
    <td class="j12"><?php eh($r["class_nm"]);?></td>
    <td class="j12"><?php eh($r["atrb_nm"]);?></td>
    <td class="j12"><?php eh($r["dept_nm"]);?></td>
<? if ($arr_class_name["class_cnt"] == 4) { ?>
    <td class="j12"><?php eh($r["room_nm"]);?></td>
<?}?>
    <td class="j12"><?php eh(substr($r["histdate"],0,19))?></td>
    <td><button type="button" onclick="class_history_edit(<?php eh($r["class_history_id"]);?>)">修正</button></td>
    <td><button type="button" onclick="class_history_delete(<?php eh($r["class_history_id"]);?>)">削除</button></td>
</tr>
<?}?>
</tbody>
</table>
<?}?>
<input type="hidden" name="session" value="<?php eh($session); ?>">
<input type="hidden" name="emp_id" value="<?php eh($emp_id); ?>">
<input type="hidden" name="class_history_id" value="">
</form>
</div>

<div class="hist" id="namehist">
<form name="namehist" action="employee_name_history_form.php" method="post">
<button type="button" onclick="name_history_edit()">氏名の履歴を手動で追加する</button>
<? if (pg_num_rows($sel_namehist) > 0) {?>
<table class="hist">
<thead>
<tr>
    <td class="j12">苗字（漢字）</td>
    <td class="j12">名前（漢字）</td>
    <td class="j12">苗字（かな）</td>
    <td class="j12">名前（かな）</td>
    <td class="j12">変更日時</td>
    <td class="j12">修正</td>
    <td class="j12">削除</td>
</tr>
</thead>
<tbody>
<? while ($r = pg_fetch_assoc($sel_namehist)) {?>
<tr>
    <td class="j12"><?php eh($r["lt_nm"]);?></td>
    <td class="j12"><?php eh($r["ft_nm"]);?></td>
    <td class="j12"><?php eh($r["kn_lt_nm"]);?></td>
    <td class="j12"><?php eh($r["kn_ft_nm"]);?></td>
    <td class="j12"><?php eh(substr($r["histdate"],0,19))?></td>
    <td><button type="button" onclick="name_history_edit(<?php eh($r["name_history_id"]);?>)">修正</button></td>
    <td><button type="button" onclick="name_history_delete(<?php eh($r["name_history_id"]);?>)">削除</button></td>
</tr>
<?}?>
</tbody>
</table>
<?}?>
<input type="hidden" name="session" value="<?php eh($session); ?>">
<input type="hidden" name="emp_id" value="<?php eh($emp_id); ?>">
<input type="hidden" name="name_history_id" value="">
</form>
</div>

</body>
<? pg_close($con); ?>
</html>
