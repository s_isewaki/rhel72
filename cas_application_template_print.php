<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_cas_application_category_list.ini");
require_once("cas_application_template.ini");
require_once("show_select_values.ini");
require_once("cas_yui_calendar_util.ini");
require_once("get_values.ini");
require_once("cas_application_workflow_common_class.php");
require_once("cas_common.ini");
require_once("get_cas_title_name.ini");

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session, $CAS_MENU_AUTH, $fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$obj = new cas_application_workflow_common_class($con, $fname);

$cas_title = get_cas_title_name();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$cas_title?> | 申請印刷</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
// 全テキストエリアの行数変更
function resizeAllTextArea() {
	var objs = document.getElementsByTagName('textarea');
	for (var i = 0, j = objs.length; i < j; i++) {
	    var t = objs[i];
		var defaultRows = Math.max(t.rows, 1);
	    var lines = t.value.split('\n');
	    var newRows = lines.length + 1;
	    var oldRows = t.rows;
	    for (var k = 0; k < lines.length; k++)
	    {
	        var line = lines[k];
	        if (line.length >= t.cols) newRows += Math.floor(line.length / t.cols);
	    }
	    if (newRows > t.rows) t.rows = newRows;
	    if (newRows < t.rows) t.rows = Math.max(defaultRows, newRows);
	}
}
</script>
<?
if($wkfw_id != "") {

	// ワークフロー情報取得
	$arr_wkfwmst = $obj->get_wkfwmst($wkfw_id);
	$wkfw_content = $arr_wkfwmst[0]["wkfw_content"];

	$num = 0;
	$pos = 0;
	while (1) {
		$pos = strpos($wkfw_content, 'show_cal', $pos);
		if ($pos === false) {
			break;
		} else {
			$num++;
		}
		$pos++;
	}

	if ($simple != "t") {
		if ($num > 0) {
			// 外部ファイルを読み込む
			write_yui_calendar_use_file_read_0_12_2();
		}
		// カレンダー作成、関数出力
		write_yui_calendar_script2($num);
	}
}
?>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#A0D25A solid 1px;}

table.block {border-collapse:collapse;}
table.block td {border:#A0D25A solid 1px;padding:1px;}
table.block td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#A0D25A solid 0px;}

table.block2 {border-collapse:collapse;}
table.block2 td {border:#A0D25A solid 1px;padding:1px;}
table.block2 td {border-width:1;}

table.block3 {border-collapse:collapse;}
table.block3 td {border:#A0D25A solid 0px;}
table.block3 td {border-width:0;}

img.close {
	background-image:url("images/folder_close_bro.gif");
	cursor:hand;
}
img.open {
	background-image:url("images/folder_open_bro.gif");
	cursor:hand;
}
p {margin:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="<? if ($simple != "t") { ?>initcal();<? } ?>if (window.OnloadSub) { OnloadSub(); };resizeAllTextArea();if(window.cas_load){cas_load();};if(window.no_7_load){no_7_load();};onLoadFunc2();"<? if ($simple == "t") { ?> style="margin:0;padding:0 0 0 50px;"<? } ?>>
<form name="apply" action="#" method="post">
<?
if ($wkfw_id != "") {
	if ($simple != "t") {
		show_application_template($con, $session, $fname, $wkfw_id, $apply_title, $content, $file_id, $filename, $back, $mode, $achievement_order);
	} else {
		show_application_template_simple($con, $obj, $session, $wkfw_id);
	}
}
?>
</form>
</body>
<script type="text/javascript">
function onLoadFunc2() {
	if (document.apply.apply_title) {
		document.apply.apply_title.value=opener.document.apply.apply_title.value;
	}
	setTimeout("self.print();self.close();", 100);
}
</script>
</html>
<?
pg_close($con);

// 受講報告の簡易印刷用
function show_application_template_simple($con, $obj, $session, $wkfw_id) {
	require_once("get_values.ini");

	$arr_wkfwmst = $obj->get_wkfwmst($wkfw_id);
	$wkfw_title = $arr_wkfwmst[0]["wkfw_title"];

	$arr_login = $obj->get_empmst($session);
	$emp_class     = $arr_login[0]["emp_class"];
	$emp_attribute = $arr_login[0]["emp_attribute"];
	$emp_dept      = $arr_login[0]["emp_dept"];
	$emp_room      = $arr_login[0]["emp_room"];
	$emp_class_nm     = get_class_nm($con, $emp_class, $fname);
	$emp_attribute_nm = get_atrb_nm($con, $emp_attribute, $fname);
	$emp_dept_nm      = get_dept_nm($con, $emp_dept, $fname);
	$class_nm = "$emp_class_nm > $emp_attribute_nm > $emp_dept_nm";
	if ($emp_room != "" && $emp_room != "0") {
		$class_nm .= " > " . $obj->get_room_nm($emp_room);
	}

	$arr_login = $obj->get_empmst($session);
	$apply_lt_name = $arr_login[0]["emp_lt_nm"];
	$apply_ft_name = $arr_login[0]["emp_ft_nm"];
	$apply_full_nm = "$apply_lt_name $apply_ft_name";

	// 一時ファイルの読み込み
	$datfilename = "cas_workflow/tmp/{$session}_d.php";
	if (!$fp = fopen($datfilename, "r")) {
		echo("<script language=\"javascript\">alert('ファイルのオープンができませんでした。');</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}
	$apply_content = fread($fp, filesize($datfilename));
	fclose($fp);

	// DOMに変換
	$utf_content = mb_convert_encoding($apply_content, "UTF-8", "EUC-JP");
	$utf_content = str_replace("EUC-JP", "UTF-8", $utf_content);
	$doc = domxml_open_mem($utf_content);
	$root = $doc->get_elements_by_tagname('apply');
	$root = $root[0];

	// 各タグの値を取得
	$date_y1 = get_node_content($root, "date_y1");
	if ($date_y1 == "") $date_y1 = 0;
	$date_m1 = get_node_content($root, "date_m1");
	if ($date_m1 == "-") $date_m1 = 0;
	$date_d1 = get_node_content($root, "date_d1");
	if ($date_d1 == "-") $date_d1 = 0;
	$course_code = get_node_content($root, "course_code");
	$course_theme = get_node_content($root, "course_theme");
	$purpose = get_node_content($root, "purpose");
	$outline = get_node_content($root, "outline");
	$date_y2 = get_node_content($root, "date_y2");
	if ($date_y2 == "") $date_y2 = 0;
	$date_m2 = get_node_content($root, "date_m2");
	if ($date_m2 == "-") $date_m2 = 0;
	$date_d2 = get_node_content($root, "date_d2");
	if ($date_d2 == "-") $date_d2 = 0;
	$date_y3 = get_node_content($root, "date_y3");
	if ($date_y3 == "") $date_y3 = 0;
	$date_m3 = get_node_content($root, "date_m3");
	if ($date_m3 == "-") $date_m3 = 0;
	$date_d3 = get_node_content($root, "date_d3");
	if ($date_d3 == "-") $date_d3 = 0;
	$course_days = get_node_content($root, "course_days");
	$course_times = get_node_content($root, "course_times");
	$reason = get_node_content($root, "reason");
	$impression = get_node_content($root, "impression");
	$chks = get_checked_values($root, "chk");
	$feedback = is_nursing_dept($wkfw_title) ? get_node_content($root, "feedback") : $impression;
?>
<table width="100%" align="center" border="0" cellpadding="2">
<tr>
<td align="center"><font size="5"><? echo(htmlspecialchars($wkfw_title)); ?></font></td>
</tr>
</table>
<table width="98%" align="center" border="0" cellpadding="2" style="margin:5px;">
<tr valign="top">
<td colspan="2">1. 報告者</td>
</tr>
<tr valign="top">
<td width="35%">　所属</td><td><? echo(htmlspecialchars($class_nm)); ?></td>
</tr>
<tr valign="top">
<td>　氏名</td><td><? echo(htmlspecialchars($apply_full_nm)); ?></td>
</tr>
<tr valign="top">
<td>2. 報告日</td><td><? echo(htmlspecialchars(format_date($date_y1, $date_m1, $date_d1))); ?></td>
</tr>
<tr valign="top">
<td colspan="2">3. 研修テーマ</td>
</tr>
<tr valign="top">
<td class="s1">(1)研修コード</td><td><? echo(htmlspecialchars($course_code)); ?></td>
</tr>
<tr valign="top">
<td class="s1">(2)研修テーマ</td><td><? echo(htmlspecialchars($course_theme)); ?></td>
</tr>
<tr valign="top">
<td colspan="2">4. 研修内容</td>
</tr>
<tr valign="top">
<td colspan="2" class="s1">＜目的＞</td>
</tr>
<tr valign="top">
<td colspan="2" class="s2"><? echo(str_replace("\n", "<br>", htmlspecialchars($purpose))); ?></td>
</tr>
<tr valign="top">
<td colspan="2" class="s1">＜概要＞</td>
</tr>
<tr valign="top">
<td colspan="2" class="s2"><? echo(str_replace("\n", "<br>", htmlspecialchars($outline))); ?></td>
</tr>
<tr valign="top">
<td colspan="2" class="s1">＜日程＞</td>
</tr>
<tr valign="top">
<td colspan="2" class="s2"><? echo(htmlspecialchars(format_term($date_y2, $date_m2, $date_d2, $date_y3, $date_m3, $date_d3, $course_days, $course_times))); ?></td>
</tr>
<? if (is_nursing_dept($wkfw_title)) { ?>
<tr valign="top">
<td colspan="2">5. 評価</td>
</tr>
<tr valign="top">
<td class="s1">(1)研修スタイル</td>
<td><? echo(format_reason($reason)); ?></td>
</tr>
<tr valign="top">
<td class="s1">(2)研修満足度</td>
<td><? echo(format_impression($impression)); ?></td>
</tr>
<tr valign="top">
<td class="s1">(3)研修満足度（理由）</td>
<td><? echo(format_chks($chks)); ?></td>
</tr>
<? } ?>
<tr valign="top">
<td colspan="2"><? echo(is_nursing_dept($wkfw_title) ? "6" : "5") ?>. 感想</td>
</tr>
<tr valign="top">
<td colspan="2" class="s1"><? echo(str_replace("\n", "<br>", htmlspecialchars($feedback))); ?></td>
</tr>
</table>
<?
}

function get_node_content($root, $tag_name) {
	$elements = $root->get_elements_by_tagname($tag_name);
	if (count($elements) == 0) {
		return "";
	}

	return mb_convert_encoding($elements[0]->get_content(), "EUC-JP", "UTF-8");
}

function get_checked_values($root, $tag_name) {
	$elements = $root->get_elements_by_tagname($tag_name);
	if (count($elements) == 0) {
		return array();
	}
	$values = array();
	foreach ($elements as $element) {
		$values[] = $element->get_attribute("id");
	}
	return $values;
}

function format_date($y, $m, $d) {
	$y = ($y == 0) ? "　" : intval($y);
	$m = ($m == 0) ? "　" : intval($m);
	$d = ($d == 0) ? "　" : intval($d);
	return "{$y}年{$m}月{$d}日";
}

function format_term($from_y, $from_m, $from_d, $to_y, $to_m, $to_d, $course_days, $course_times) {
	if ($course_days == "") $course_days = "　";
	if ($course_times == "") $course_times = "　";
	return format_date($from_y, $from_m, $from_d) . "〜" . format_date($to_y, $to_m, $to_d) . "　{$course_days}日間　{$course_times}時間";
}

function is_nursing_dept($wkfw_title) {
	return (mb_strpos($wkfw_title, "看護部主催", $wkfw_title) !== false);
}

function format_reason($reason) {
	switch ($reason) {
	case "1":
		return "自主的";
	case "2":
		return "師長に勧められて";
	case "3":
		return "主任に勧められて";
	case "4":
		return "同僚に勧められて";
	case "5":
		return "必須";
	default:
		return "";
	}
}

function format_impression($impression) {
	switch ($impression) {
	case "1":
		return "大変不満";
	case "2":
		return "やや不満";
	case "3":
		return "どちらとも言えない";
	case "4":
		return "やや満足";
	case "5":
		return "大変満足";
	default:
		return "";
	}
}

function format_chks($chks) {
	$labels = array();
	foreach ($chks as $chk) {
		switch ($chk) {
		case "0":
			$labels[] = "話が分かりにくい (理解しにくい)";
			break;
		case "1":
			$labels[] = "資料が不適切 (少ない、難しい)";
			break;
		case "2":
			$labels[] = "時間が短い";
			break;
		case "3":
			$labels[] = "時間が長い";
			break;
		case "4":
			$labels[] = "その他";
			break;
		default:
			break;
		}
	}
	return implode(", ", $labels);
}
?>
