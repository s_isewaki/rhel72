<?php
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");
require_once("hiyari_common.ini");
require_once("hiyari_report_class.php");
//require_once("hiyari_reporting_common.ini");
//require_once("hiyari_mail.ini");
require_once("hiyari_post_select_box.ini");
require_once("Cmx.php");
require_once("aclg_set.php");

//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// 権限チェック
$summary = check_authority($session, 47, $fname);
if ($summary == "0") {
    showLoginPage();
    exit;
}


//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

//==============================
//ユーザーID取得
//==============================
$emp_id = get_emp_id($con,$session,$fname);


//==============================
//年情報
//==============================
$this_year = date('Y');
$this_month = date('m');
$start_year = 2004;//統計分析と同じ。

//今月最終日
$this_month_last_date = null;
for($i = 29; $i<=31;$i++)
{
    $tmp_date = mktime(0, 0, 0, $this_month, $i, $this_year);
    if(date("m",$tmp_date) != $this_month)
    {
        break;
    }
    $this_month_last_date = date("d",$tmp_date);
}


//==============================
//患者影響レベル
//==============================
$rep_obj = new hiyari_report_class($con, $fname);
//$level_infos = $rep_obj->get_inci_level_infos($inci_lvl);




    //==============================
    //ポストバック時の処理
    //==============================
    if(! empty($is_postback))
    {

        //==============================
        //入力値の変換
        //==============================

        //AND_OR区分が空(disable)の場合はAND。
        if($and_or_div == "")
        {
            $and_or_div = "0";
        }

        //副報告が空(未チェック)の場合は"f"。
        if($sub_report_flg == "")
        {
            $sub_report_flg = "f";
        }

        //==============================
        //入力チェック
        //==============================

        //未入力チェック
        if($folder_name == "")
        {
            //未入力エラー
            echo("<script language=\"javascript\">alert(\"フォルダ名が入力されていません。\");</script>\n");
            echo("<script language=\"javascript\">history.back();</script>\n");
            exit;
        }

        //重複名称チェック(同一年内での重複はNG)
        $folder_name_sql = pg_escape_string($folder_name);
        $sql = "select count(*) from inci_hiyari_folder where folder_name = '$folder_name_sql' and hiyari_year = '$hiyari_year'";
        if($folder_id != "")
        {
            $sql .=  " and not folder_id = $folder_id";
        }
        $sel = select_from_table($con, $sql, "", $fname);
        if ($sel == 0){
            pg_query($con,"rollback");
            pg_close($con);
            showErrorPage();
            exit;
        }
        if (pg_fetch_result($sel,0,"count") != 0)
        {
            //名称重複エラー
            echo("<script language=\"javascript\">alert(\"同じ名前の{$tmp_common_flg_name}が登録されています。\");</script>\n");
            echo("<script language=\"javascript\">history.back();</script>\n");
            exit;
        }

        //==============================
        //DB処理
        //==============================
        if($mode == "new")
        {
            //==============================
            //フォルダIDの採番
            //==============================
            $sql = "SELECT max(folder_id) FROM inci_hiyari_folder";
            $sel = select_from_table($con, $sql, "", $fname);
            if ($sel == 0){
                pg_query($con,"rollback");
                pg_close($con);
                showErrorPage();
                exit;
            }
            $folder_id = pg_result($sel,0,"max");
            $folder_id++;


            //==============================
            //分類フォルダ追加
            //==============================
            $report_registrant_class = ($search_emp_class == "") ? null:$search_emp_class;
            $report_registrant_attribute = ($search_emp_attribute == "") ? null:$search_emp_attribute;
            $report_registrant_dept = ($search_emp_dept == "") ? null:$search_emp_dept;
            $report_registrant_room = ($search_emp_room == "") ? null:$search_emp_room;

            $sql  = "insert into inci_hiyari_folder(";
            $sql .= " folder_id,";
            $sql .= " folder_name,";
            $sql .= " hiyari_year,";
            $sql .= " hiyari_data_type,";

            $sql .= " incident_date_start_year,";
            $sql .= " incident_date_start_month,";
            $sql .= " incident_date_start_date,";
            $sql .= " incident_date_end_year,";
            $sql .= " incident_date_end_month,";
            $sql .= " incident_date_end_date,";
            $sql .= " sub_report_flg,";

            $sql .= " and_or_div,";

            $sql .= " report_registrant_class,";
            $sql .= " report_registrant_attribute,";
            $sql .= " report_registrant_dept,";
            $sql .= " report_registrant_room,";
//          $sql .= " incident_level,";
            $sql .= " hiyari_summary,";
            $sql .= " report_title_keyword,";
            $sql .= " incident_contents_keyword";

            $sql .= ") values(";
            $registry_data = array(
                $folder_id,
                pg_escape_string($folder_name),
                $hiyari_year,
                $hiyari_data_type,

                $incident_date_start_year,
                $incident_date_start_month,
                $incident_date_start_date,
                $incident_date_end_year,
                $incident_date_end_month,
                $incident_date_end_date,
                $sub_report_flg,

                $and_or_div,

                $report_registrant_class,
                $report_registrant_attribute,
                $report_registrant_dept,
                $report_registrant_room,
//              join ( ",",$incident_level),//CSV,
                join ( ",",$hiyari_summary),//CSV
                pg_escape_string($report_title_keyword),
                pg_escape_string($incident_contents_keyword)
            );

            $result = insert_into_table($con, $sql, $registry_data, $fname);
            if ($result == 0) {
                pg_query($con,"rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }

            //==============================
            //自分画面を閉じる。
            //==============================
            echo("<script language='javascript'>");
            echo("if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}");
            echo("window.close();");
            echo("</script>");
            exit;

        }
        elseif($mode == "update")
        {
            //==============================
            //分類フォルダ更新
            //==============================
            $report_registrant_class = ($search_emp_class == "") ? null:$search_emp_class;
            $report_registrant_attribute = ($search_emp_attribute == "") ? null:$search_emp_attribute;
            $report_registrant_dept = ($search_emp_dept == "") ? null:$search_emp_dept;
            $report_registrant_room = ($search_emp_room == "") ? null:$search_emp_room;

            $sql = "update inci_hiyari_folder set";
            $set = array(
                    "folder_name",
                    "hiyari_data_type",

                    "incident_date_start_year",
                    "incident_date_start_month",
                    "incident_date_start_date",
                    "incident_date_end_year",
                    "incident_date_end_month",
                    "incident_date_end_date",
                    "sub_report_flg",

                    "and_or_div",

                    "report_registrant_class",
                    "report_registrant_attribute",
                    "report_registrant_dept",
                    "report_registrant_room",
//                  "incident_level",
                    "hiyari_summary",
                    "report_title_keyword",
                    "incident_contents_keyword");
            $setvalue = array(
                pg_escape_string($folder_name),
                $hiyari_data_type,

                $incident_date_start_year,
                $incident_date_start_month,
                $incident_date_start_date,
                $incident_date_end_year,
                $incident_date_end_month,
                $incident_date_end_date,
                $sub_report_flg,

                $and_or_div,

                $report_registrant_class,
                $report_registrant_attribute,
                $report_registrant_dept,
                $report_registrant_room,
//              join ( ",",$incident_level),//CSV,
                join ( ",",$hiyari_summary),//CSV
                pg_escape_string($report_title_keyword),
                pg_escape_string($incident_contents_keyword)
                );
            $cond = "where folder_id = $folder_id";
            $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
            if ($upd == 0) {
                pg_query($con,"rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }

            //==============================
            //自分画面を閉じる。
            //==============================
            echo("<script language='javascript'>");
            echo("if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}");
            echo("window.close();");
            echo("</script>");
            exit;

        }

    }


//==============================
//フォルダ情報取得
//==============================
if($mode == "update")
{
    $sql = "select * from inci_hiyari_folder where folder_id = $folder_id";
    $sel = select_from_table($con,$sql,"",$fname);
    if($sel == 0){
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    if(pg_num_rows($sel) != 1){
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    $PAGE_TITLE ="ヒヤリ・ハット分類フォルダ更新";
    $SUBMIT_BUTTON_TITLE = "更新";

    $folder_name               = pg_fetch_result($sel,0,"folder_name");
    $hiyari_data_type                 = pg_fetch_result($sel,0,"hiyari_data_type");

    $incident_date_start_year  = pg_fetch_result($sel,0,"incident_date_start_year");
    $incident_date_start_month = pg_fetch_result($sel,0,"incident_date_start_month");
    $incident_date_start_date  = pg_fetch_result($sel,0,"incident_date_start_date");
    $incident_date_end_year    = pg_fetch_result($sel,0,"incident_date_end_year");
    $incident_date_end_month   = pg_fetch_result($sel,0,"incident_date_end_month");
    $incident_date_end_date    = pg_fetch_result($sel,0,"incident_date_end_date");
    $sub_report_flg            = pg_fetch_result($sel,0,"sub_report_flg");

//  $incident_level_csv        = pg_fetch_result($sel,0,"incident_level");
//  $incident_level            = split(",", $incident_level_csv );
    $search_emp_class          = pg_fetch_result($sel,0,"report_registrant_class");
    $search_emp_attribute      = pg_fetch_result($sel,0,"report_registrant_attribute");
    $search_emp_dept           = pg_fetch_result($sel,0,"report_registrant_dept");
    $search_emp_room           = pg_fetch_result($sel,0,"report_registrant_room");

    $hiyari_summary_csv        = pg_fetch_result($sel,0,"hiyari_summary");
    $hiyari_summary            = split(",", $hiyari_summary_csv );
    $report_title_keyword      = pg_fetch_result($sel,0,"report_title_keyword");
    $incident_contents_keyword = pg_fetch_result($sel,0,"incident_contents_keyword");

    $and_or_div                = pg_fetch_result($sel,0,"and_or_div");
}
else
{
    $PAGE_TITLE ="ヒヤリ・ハット分類フォルダ登録";
    $SUBMIT_BUTTON_TITLE = "作成";

    $folder_name = "";
    $hiyari_data_type = "1";//コード化のみ

    $incident_date_start_year = $this_year;
    $incident_date_start_month = $this_month;
    $incident_date_start_date = "01";
    $incident_date_end_year = $this_year;
    $incident_date_end_month = $this_month;
    $incident_date_end_date = $this_month_last_date;
    $sub_report_flg = "f";

//  $incident_level = "";
    $search_emp_class = "";
    $search_emp_attribute = "";
    $search_emp_dept = "";
    $search_emp_room = "";

    $hiyari_summary = "";
    $report_title_keyword = "";
    $incident_contents_keyword = "";

    $and_or_div = "0";//AND
}


//==============================
//ヒヤリ・ハット分類マスタ取得
//==============================
$arr_hiyari_summary = get_report_item_list($con, $fname,125,85);

//======================================================================================================================================================
//HTML出力
//======================================================================================================================================================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$INCIDENT_TITLE?> | <?=$PAGE_TITLE?></title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script language="javascript">

function init_page()
{
    set_disabled_enabled();
}

function set_disabled_enabled()
{
    //----------------------------------------
    //入力している項目数を取得
    //----------------------------------------
    var input_cnt = 0;

//  var objs = document.getElementsByName("incident_level[]");
//  for(var i=0; i<objs.length;i++)
//  {
//      var obj = objs[i];
//      if(obj.checked)
//      {
//          input_cnt++;
//          break;
//      }
//  }

//  var objs = document.getElementsByName("search_emp_class");
//  for(var i=0; i<objs.length;i++)
//  {
//      var obj = objs[i];
//      if(obj.value != "")
//      {
//          input_cnt++;
//          break;
//      }
//  }

    var objs = document.getElementsByName("hiyari_summary[]");
    for(var i=0; i<objs.length;i++)
    {
        var obj = objs[i];
        if(obj.checked)
        {
            input_cnt++;
            break;
        }
    }

    var objs = document.getElementsByName("report_title_keyword");
    for(var i=0; i<objs.length;i++)
    {
        var obj = objs[i];
        if(obj.value != "")
        {
            input_cnt++;
            break;
        }
    }

    var objs = document.getElementsByName("incident_contents_keyword");
    for(var i=0; i<objs.length;i++)
    {
        var obj = objs[i];
        if(obj.value != "")
        {
            input_cnt++;
            break;
        }
    }

    //----------------------------------------
    //AND･ORのdisabledを判定
    //----------------------------------------
    var and_or_div_disabled = (input_cnt < 2);

    //----------------------------------------
    //AND･ORのdisabledを設定
    //----------------------------------------
    var objs = document.getElementsByName("and_or_div");
    for(var i=0; i<objs.length;i++)
    {
        var obj = objs[i];
        if(and_or_div_disabled && obj.value == "0")
        {
            obj.checked = true;
        }
        obj.disabled = and_or_div_disabled;
    }

}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

.list {border-collapse:collapse;}
.list td {border:#35B341 solid 1px;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#35B341 solid 0px;}
table.block_in td td {border-width:1;}

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="init_page();">
<form name="mainform" action="hiyari_hiyari_folder_input.php" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="mode" value="<?=$mode?>">
<input type="hidden" name="hiyari_year" value="<?=$hiyari_year?>">
<input type="hidden" name="is_postback" value="true">
<input type="hidden" name="folder_id" value="<?=$folder_id?>">

<!-- BODY全体 START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<!-- ヘッダー START -->
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
<?
    show_hiyari_header_for_sub_window($PAGE_TITLE);
?>
</table>
<!-- ヘッダー END -->


</td></tr><tr><td>


<!-- 本体 START -->
<table border="0" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
        <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
        <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
    </tr>
    <tr>
        <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
        <td bgcolor="#F5FFE5">








            <!-- 主情報 START -->
            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                <tr>
                    <td align="right" width="160" bgcolor="#DFFFDC">
                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">分類フォルダ名</font>
                    </td>
                    <td align="left" bgcolor="#FFFFFF">
                        <input type="text" name="folder_name" value="<?=h($folder_name)?>" maxlength="50" style="width:500px">
                    </td>
                </tr>
                <tr>
                    <td align="right" width="160" bgcolor="#DFFFDC">
                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">報告タイプ</font>
                    </td>
                    <td align="left" bgcolor="#FFFFFF">
                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
                        <select name="hiyari_data_type">
                        <option value="1" <? if($hiyari_data_type == 1){?>selected<?}?> >コード化のみ</option>
                        <option value="2" <? if($hiyari_data_type == 2){?>selected<?}?> >コード化＋記述</option>
                        <option value="3" <? if($hiyari_data_type == 3){?>selected<?}?> >記述のみ</option>
                        </select>
                        </font>
                    </td>
                </tr>
            </table>
            <!-- 主情報 END -->







            <img src="img/spacer.gif" width="1" height="10" alt="">







            <!-- 分類範囲 START -->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
                    <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
                    <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
                </tr>
                <tr>
                    <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
                    <td bgcolor="#F5FFE5">


                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                        <td>

                            <table border="0" cellspacing="0" cellpadding="2" class="list">
                                <tr>
                                    <td width="150" align="center" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">フォルダの範囲設定</font></td>
                                </tr>
                            </table>


                        </td>
                        </tr>
                        <tr>
                        <td height="5">

                        </td>
                        </tr>
                        <tr>
                        <td>

                            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">


                                <!-- 発生年月 START -->
                                <tr>
                                    <td width="150" bgcolor="#DFFFDC">
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発生年月日</font>
                                    </td>
                                    <td bgcolor="#FFFFFF" nowrap>
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">

                                            <select name="incident_date_start_year">
<?
for($i=$this_year;$i>=$start_year;$i--)
{
?>
                                                <option value="<?=$i?>" <? if($i == $incident_date_start_year){?>selected="selected"<?} ?>><?=$i?></option>
<?
}
?>
                                            </select>
                                            年

                                            <select name="incident_date_start_month">
                                                <option value="01" <? if("01" == $incident_date_start_month){?>selected="selected"<?} ?> >1</option>
                                                <option value="02" <? if("02" == $incident_date_start_month){?>selected="selected"<?} ?> >2</option>
                                                <option value="03" <? if("03" == $incident_date_start_month){?>selected="selected"<?} ?> >3</option>
                                                <option value="04" <? if("04" == $incident_date_start_month){?>selected="selected"<?} ?> >4</option>
                                                <option value="05" <? if("05" == $incident_date_start_month){?>selected="selected"<?} ?> >5</option>
                                                <option value="06" <? if("06" == $incident_date_start_month){?>selected="selected"<?} ?> >6</option>
                                                <option value="07" <? if("07" == $incident_date_start_month){?>selected="selected"<?} ?> >7</option>
                                                <option value="08" <? if("08" == $incident_date_start_month){?>selected="selected"<?} ?> >8</option>
                                                <option value="09" <? if("09" == $incident_date_start_month){?>selected="selected"<?} ?> >9</option>
                                                <option value="10" <? if("10" == $incident_date_start_month){?>selected="selected"<?} ?> >10</option>
                                                <option value="11" <? if("11" == $incident_date_start_month){?>selected="selected"<?} ?> >11</option>
                                                <option value="12" <? if("12" == $incident_date_start_month){?>selected="selected"<?} ?> >12</option>
                                            </select>
                                            月

                                            <select name="incident_date_start_date">
<?
for($i=1;$i<=31;$i++)
{
    $val = $i;
    if($val < 10)
    {
        $val = "0".$i;
    }
?>
                                                <option value="<?=$val?>" <? if($val == $incident_date_start_date){?>selected="selected"<?} ?> ><?=$i?></option>
<?
}
?>
                                            </select>
                                            日
                                            &nbsp
                                            から
                                            &nbsp
                                            <select name="incident_date_end_year">
<?
for($i=$this_year;$i>=$start_year;$i--)
{
?>
                                                <option value="<?=$i?>" <? if($i == $incident_date_end_year){?>selected="selected"<?} ?>><?=$i?></option>
<?
}
?>
                                            </select>

                                            年

                                            <select name="incident_date_end_month">
                                                <option value="01" <? if("01" == $incident_date_end_month){?>selected="selected"<?} ?> >1</option>
                                                <option value="02" <? if("02" == $incident_date_end_month){?>selected="selected"<?} ?> >2</option>
                                                <option value="03" <? if("03" == $incident_date_end_month){?>selected="selected"<?} ?> >3</option>
                                                <option value="04" <? if("04" == $incident_date_end_month){?>selected="selected"<?} ?> >4</option>
                                                <option value="05" <? if("05" == $incident_date_end_month){?>selected="selected"<?} ?> >5</option>
                                                <option value="06" <? if("06" == $incident_date_end_month){?>selected="selected"<?} ?> >6</option>
                                                <option value="07" <? if("07" == $incident_date_end_month){?>selected="selected"<?} ?> >7</option>
                                                <option value="08" <? if("08" == $incident_date_end_month){?>selected="selected"<?} ?> >8</option>
                                                <option value="09" <? if("09" == $incident_date_end_month){?>selected="selected"<?} ?> >9</option>
                                                <option value="10" <? if("10" == $incident_date_end_month){?>selected="selected"<?} ?> >10</option>
                                                <option value="11" <? if("11" == $incident_date_end_month){?>selected="selected"<?} ?> >11</option>
                                                <option value="12" <? if("12" == $incident_date_end_month){?>selected="selected"<?} ?> >12</option>
                                            </select>
                                            月

                                            <select name="incident_date_end_date">
<?
for($i=1;$i<=31;$i++)
{
    $val = $i;
    if($val < 10)
    {
        $val = "0".$i;
    }
?>
                                                <option value="<?=$val?>" <? if($val == $incident_date_end_date){?>selected="selected"<?} ?> ><?=$i?></option>
<?
}
?>
                                            </select>
                                            日
                                            &nbsp
                                            まで

                                        </font>
                                    </td>
                                </tr>
                                <!-- 発生年月 END -->


                                <!-- 報告部署 START -->
                                <tr>
<?
show_post_select_box_for_hiyari_folder($con, $fname, $search_emp_class, $search_emp_attribute, $search_emp_dept, $search_emp_room,$emp_id);

//  $emp_id = get_emp_id($con,$session,$fname);
//  $report_db_read_div = 0;//分類フォルダの設定上では絞らない。(SM固定のため不要。)
//  show_post_select_box_for_hiyari_folder($con, $fname, $search_emp_class, $search_emp_attribute, $search_emp_dept, $search_emp_room,$emp_id,$report_db_read_div);
?>
                                </tr>
                                <!-- 報告部署 END -->


                                <!-- 副報告 START -->
                                <tr>
                                    <td width="150" bgcolor="#DFFFDC">
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">副報告</font>
                                    </td>
                                    <td bgcolor="#FFFFFF" nowrap>
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                            <input type="checkbox" name="sub_report_flg" value="t" <? if($sub_report_flg == "t"){?>checked<?} ?>>
                                            副報告を含む
                                        </font>
                                    </td>
                                </tr>
                                <!-- 副報告 END -->


                            </table>

                        </td>
                        </tr>
                        </table>

                    </td>
                    <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
                </tr>
                <tr>
                    <td><img src="img/r_3.gif" width="10" height="10"></td>
                    <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
                    <td><img src="img/r_4.gif" width="10" height="10"></td>
                </tr>
            </table>
            <!-- 分類範囲 END -->








            <img src="img/spacer.gif" width="1" height="10" alt="">







            <!-- 分類条件 START -->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
                    <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
                    <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
                </tr>
                <tr>
                    <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
                    <td bgcolor="#F5FFE5">

                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                        <td>

                            <table border="0" cellspacing="0" cellpadding="2" class="list">
                                <tr>
                                    <td width="150" align="center" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">自動分類の条件設定</font></td>
                                </tr>
                            </table>

                        </td>
                        </tr>
                        <tr>
                        <td height="5">

                        </td>
                        </tr>
                        <tr>
                        <td>

                            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">







                                <!-- 患者影響レベル START -->
<?/*
//                                  <tr>
//                                      <td bgcolor="#DFFFDC" width="150">
//                                          <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><?show_inci_level_popup($con, $fname, $level_infos);?></font>
//                                      </td>
//                                      <td bgcolor="#FFFFFF">
//                                          <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
//                                              <tr>
//  <?
//  $i = 0;
//  foreach ($level_infos as $level_info)
//  {
//          if($level_info["use_flg"])
//          {
//              $code = $level_info["easy_code"];
//              $name = $level_info["easy_name"];
//
//
//              $i++;
//              if($i !=1 && $i%4 == 1)
//              {
//  ?>
//                                              </tr>
//                                              <tr>
//  <?
//              }
//  ?>
//                                                  <td width="25%">
//                                                  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
//                                                  <nobr>
//                                                  <input type="checkbox" name="incident_level[]" value="<?=$code?>" <? if(in_array($code,$incident_level)){?>checked<?} ?>  onclick="set_disabled_enabled();"><?=h($name)?>
//                                                  </nobr>
//                                                  </font>
//                                                  </td>
//  <?
//          }
//  }
//  ?>
//                                              </tr>
//                                          </table>
//                                      </td>
//                                  </tr>
*/?>
                                                                <!-- 患者影響レベル END -->




                                <!-- ヒヤリ・ハット分類 START -->
                                <tr>
                                    <td bgcolor="#DFFFDC" width="150">
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">分類</font>
                                    </td>
                                    <td bgcolor="#FFFFFF">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                                            <tr>
<?
$i = 0;
foreach($arr_hiyari_summary as $code => $name)
{
    $i++;
    if($i !=1 && $i%4 == 1)
    {
?>
                                            </tr>
                                            <tr>
<?
    }
?>
                                                <td width="25%">
                                                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                                <nobr>
                                                <input type="checkbox" name="hiyari_summary[]" value="<?=$code?>" <? if(in_array($code,$hiyari_summary)){?>checked<?} ?>  onclick="set_disabled_enabled();"><?=h($name)?>
                                                </nobr>
                                                </font>
                                                </td>
<?
}
?>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <!-- ヒヤリ・ハット分類 END -->





                                <!-- 表題 START -->
                                <tr>
                                    <td bgcolor="#DFFFDC" width="150">
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表題へのキーワード</font>
                                    </td>
                                    <td bgcolor="#FFFFFF">
                                        <input type="text" name="report_title_keyword" size="50" maxlength="50" value="<?=h($report_title_keyword)?>" onkeyup="set_disabled_enabled();">
                                    </td>
                                </tr>
                                <!-- 表題 END -->





                                <!-- インシデントの内容 START -->
                                <tr>
                                    <td bgcolor="#DFFFDC" width="150">
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">インシデントの内容<BR>へのキーワード</font>
                                    </td>
                                    <td bgcolor="#FFFFFF">
                                        <input type="text" name="incident_contents_keyword" size="50" maxlength="50" value="<?=h($incident_contents_keyword)?>" onkeyup="set_disabled_enabled();">
                                    </td>
                                </tr>
                                <!-- インシデントの内容 END -->






                                <!-- AND_OR条件 START -->
                                <tr>
                                    <td bgcolor="#DFFFDC" width="150">
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">AND_OR条件</font>
                                    </td>
                                    <td bgcolor="#FFFFFF">
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                        <input type="radio" name="and_or_div" value="0" <?if($and_or_div == "0"){echo( "checked");}?>>AND
                                        <input type="radio" name="and_or_div" value="1" <?if($and_or_div == "1"){echo( "checked");}?>>OR
                                        </font>
                                    </td>
                                </tr>
                                <!-- AND_OR条件 END -->





                            </table>

                        </td>
                        </tr>
                        </table>


                    </td>
                    <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
                </tr>
                <tr>
                    <td><img src="img/r_3.gif" width="10" height="10"></td>
                    <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
                    <td><img src="img/r_4.gif" width="10" height="10"></td>
                </tr>
            </table>
            <!-- 分類条件 END -->








            <!-- 実効ボタン START -->
            <table width="100%" border="0" cellspacing="0" cellpadding="5">
                <tr>
                    <td align="right">
                        <input type="submit" value="<?=$SUBMIT_BUTTON_TITLE?>">
                    </td>
                </tr>
            </table>
            <!-- 実効ボタン END -->






        </td>
        <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
    </tr>
    <tr>
        <td><img src="img/r_3.gif" width="10" height="10"></td>
        <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
        <td><img src="img/r_4.gif" width="10" height="10"></td>
    </tr>
</table>
<!-- 本体 END -->


</td>
</tr>
</table>
<!-- BODY全体 END -->

</body>
</html>
<?
//======================================================================================================================================================
//内部関数
//======================================================================================================================================================


/**
 * 報告書項目の一覧を取得
 * @param $con DBコネクション
 * @param $fname 画面名
 * @param $grp_code グループコード
 * @param $easy_item_code 項目コード
 */
function get_report_item_list($con, $fname,$grp_code,$easy_item_code)
{
    $arr = array();
    $sql = "select easy_code, easy_name from inci_report_materials where grp_code = $grp_code and easy_item_code = $easy_item_code order by easy_num";
    $sel = select_from_table($con,$sql,"",$fname);
    if($sel == 0){
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    while ($row = pg_fetch_array($sel)) {

        $code = $row["easy_code"];
        $name = $row["easy_name"];
        $arr[$code] = $name;
    }

    //非表示項目を除外
    if($grp_code == 125 && $easy_item_code == 85)
    {
        $rep_obj = new hiyari_report_class($con, $fname);
        $item_element_no_disp = $rep_obj->get_item_element_no_disp();

        $arr2 = null;
        foreach($arr as $code => $name)
        {
            if(!in_array($code , $item_element_no_disp[$grp_code][$easy_item_code]) )
            {
                $arr2[$code] = $name;
            }
        }
        $arr = $arr2;
    }
    else
    {
        //この関数に対するユースケースで非表示項目となり得るケースはない。
    }

    return $arr;
}






//==============================
//DBコネクション終了
//==============================
pg_close($con);
?>

