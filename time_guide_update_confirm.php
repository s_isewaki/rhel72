<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$check_auth = check_authority($session, 12, $fname);
if ($check_auth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if (strlen($fcl_detail) > 50) {
	echo("<script type=\"text/javascript\">alert('施設詳細が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (!checkdate($date2, $date3, $date1)) {
	echo("<script type=\"text/javascript\">alert('日付が不正です。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if ($time_flg == "") {
	if ($start_hrs == "--" || $start_min == "-" || $dur_hrs == "--" || $dur_min == "-") {
		echo("<script type=\"text/javascript\">alert('時刻が不正です。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}

	$start_time = "$start_hrs:$start_min";
	$end_time = "$dur_hrs:$dur_min";
	if ($start_time >= $end_time) {
		echo("<script type=\"text/javascript\">alert('終了時刻は開始時刻より後にしてください。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}

	$time_flg = "t";
} else {
	$start_time = "08:00";
	$end_time = "20:00";
}
if ($event_name == "") {
	echo("<script type=\"text/javascript\">alert('行事名が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if (strlen($event_name) > 100) {
	echo("<script type=\"text/javascript\">alert('行事名が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

/* 2010/09/02 Tsuzuki Revised
if ($event_content == '') {
	echo("<script type=\"text/javascript\">alert('詳細が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
*/


if (strlen($event_content) > 400) {
	echo("<script type=\"text/javascript\">alert('詳細が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($contact) > 100) {
	echo("<script type=\"text/javascript\">alert('連絡先が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 行事情報を更新
if ($fcl_id == "-") {
	$fcl_id = null;
}

if($sch_del == "on")
{
	// チェックされた行事を削除
	$sql = "delete from timegd";
	$cond = "where event_id = '$e_id'";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}
else
{
	//更新を実行

	$sql = "update timegd set";
	$cond = "where event_id = '$e_id'";
	$set = array("event_name", "event_content", "event_date", "event_time", "event_dur", "fcl_id", "time_flg", "fcl_detail", "contact");
	$setvalue = array($event_name, $event_content, "$date1$date2$date3", "$start_time", "$end_time", $fcl_id, $time_flg, $fcl_detail, $contact);
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	
}



// データベース接続を閉じる
pg_close($con);

if ($sch == "on") 
{ //スケジュール機能から飛んできた場合にはヘッダリストは表示しないで画面終了
// 親画面をリフレッシュし、自画面を閉じる
	echo("<script type=\"text/javascript\">window.opener.location.reload();</script>");
	echo("<script type=\"text/javascript\">self.close();</script>");
	return;
	
}
else
{
	// 行事一覧画面に遷移
	echo("<script type=\"text/javascript\">location.href = 'time_guide_list.php?session=$session&fcl_id=$fcl_id&date=" . mktime(0, 0, 0, $date2, $date3, $date1) . "&mode=display';</script>");
}

?>
