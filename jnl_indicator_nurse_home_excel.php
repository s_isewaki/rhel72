<?php

//PHPのタイムアウト時間を無制限にしました
ini_set("max_execution_time", 0);

require_once 'jnl_indicator_display.php';
require_once 'jnl_indicator_nurse_home_display.php';
require_once 'jnl_indicator_nurse_home_data.php';
class IndicatorNurseHomeExcel extends IndicatorDisplay
{
    /**
     * コンストラクタ
     * @param $arr
     */
    function IndicatorNurseHomeExcel($arr)
    {
        parent::IndicatorDisplay($arr);
//        parent::IndicatorNurseHomeDisplay($arr);
    }

    function setIndicatorNurseHomeData() {
        $this->indicatorNurseHomeData = new IndicatorNurseHomeData(array('fname' => $this->fname, 'con' => $this->con));
    }

    function getIndicatorNurseHomeData() {
        if (is_object($this->indicatorNurseHomeData)) {
            return $this->indicatorNurseHomeData;
        }
        else {
            $this->setIndicatorNurseHomeData();
            return $this->indicatorNurseHomeData;
        }
    }

    /**
     * 施設別患者日報 のエクセル出力
     * @param  array $dataArr
     * @param  array $plantArr
     * @param  array $arr
     * @param  array $optionArr
     * @return string HTML table
     */
    function ourputExcelPatientDailyFacilities($dataArr, $plantArr, $arr, $optionArr)
    {
        $rtnStr  = $this->getExcelMeta();
        $fontTag = $this->getFontTag();

        $displayYear  = $this->getArrValueByKey('year',  $optionArr);
        $displayMonth = $this->getArrValueByKey('month', $optionArr);
        $displayDay   = $this->getArrValueByKey('day',   $optionArr);
        $rtnStr  .= sprintf('%04d年%02d月%02d日(%s)', $displayYear, $displayMonth, $displayDay, $this->getDayOfTheWeekForDate($displayYear, $displayMonth, $displayDay))."<br />";

        # attr
        $rowspan_2  = array('name' => 'rowspan', 'value' =>  2);
        $rowspan_3  = array('name' => 'rowspan', 'value' =>  3);
        $colspan_2  = array('name' => 'colspan', 'value' =>  2);
        $colspan_3  = array('name' => 'colspan', 'value' =>  3);
        $colspan_8  = array('name' => 'colspan', 'value' =>  8);
        $colspan_12 = array('name' => 'colspan', 'value' => 12);
        $colspan_13 = array('name' => 'colspan', 'value' => 13);
        $colspan_14 = array('name' => 'colspan', 'value' => 14);
        $colspan_15 = array('name' => 'colspan', 'value' => 15);
        $colspan_16 = array('name' => 'colspan', 'value' => 16);

        # style
        $bgColor_blue      = array('name' => 'background-color', 'value' => $this->getRgbColor('blue'));
        $textAlign_center  = array('name' => 'text-align',       'value' => 'center');
        $textAlign_right   = array('name' => 'text-align',       'value' => 'right');
        $whiteSpace_nowrap = array('name' => 'white-space',      'value' => 'nowrap');
        $msoNumberFormat_percent0_0 = $this->getMsoNumberFormat('percent');
        $msoNumberFormat_comma      = $this->getMsoNumberFormat('comma');

        $header = '<table class="list" width="100%" border="1" >'
                     .'<tr>'
                         .$this->convertCellData(
                              array(
                                  0 => array('data' => '&nbsp;', 'attr' => array($rowspan_3))
                                , 1 => array('data' => '通所', 'attr' => array($colspan_13))
                                , 2 => array('data' => '入所', 'attr' => array($colspan_16))
                                , 3 => array('data' => "訪問リハビリ<br />(単位)", 'attr' => array($rowspan_2, $colspan_3))
                                , 4 => array('data' => "判定件数<br />（本入所のみ）", 'attr' => array($rowspan_2, $colspan_2))
                                , 5 => array('data' => "訪問件数<br />（訪問件数のみ）", 'attr' => array($rowspan_3))
                              ), $fontTag, array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
                         )
                     .'</tr>'
                     .'<tr>'
                         .$this->convertCellData(
                              array(
                                   0 => array('data' => '定員数', 'attr' => array($rowspan_2))
                                 , 1 => array('data' => '通所リハビリ', 'attr' => array($colspan_8))
                                 , 2 => array('data' => "予防通所<br />リハビリ", 'attr' => array($rowspan_2))
                                 , 3 => array('data' => '合計', 'attr' => array($rowspan_2))
                                 , 4 => array('data' => '稼働率', 'attr' => array($rowspan_2, $colspan_2))
                                 , 5 => array('data' => '定員数', 'attr' => array($rowspan_2))
                                 , 6 => array('data' => '一般', 'attr' => array($rowspan_2))
                                 , 7 => array('data' => "一般<br />ショートスティ", 'attr' => array($rowspan_2))
                                 , 8 => array('data' => "一般<br />予防ショート", 'attr' => array($rowspan_2))
                                 , 9 => array('data' => '認知', 'attr' => array($rowspan_2))
                                , 10 => array('data' => "認知<br />ショートスティ", 'attr' => array($rowspan_2))
                                , 11 => array('data' => "認知<br />予防ショート", 'attr' => array($rowspan_2))
                                , 12 => array('data' => '小計', 'attr' => array($rowspan_2))
                                , 13 => array('data' => '入所数', 'attr' => array($rowspan_2))
                                , 14 => array('data' => $this->sc('1').'退所数', 'attr' => array($rowspan_2))
                                , 15 => array('data' => $this->sc('1')."の内、<br />在宅復帰人数", 'attr' => array($rowspan_2))
                                , 16 => array('data' => "ショート<br />予防ショート<br />入所数", 'attr' => array($rowspan_2))
                                , 17 => array('data' => "ショート<br />予防ショート<br />退所数", 'attr' => array($rowspan_2))
                                , 18 => array('data' => '空床数', 'attr' => array($rowspan_2))
                                , 19 => array('data' => '稼働率', 'attr' => array($rowspan_2, $colspan_2))
                              ),
                              $fontTag,
                              array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
                         )
                     .'</tr>'
                     .'<tr>'
                         .$this->convertCellData(
                              array(
                                   0 => array('data' => "1時間以上<br />2時間未満")
                                 , 1 => array('data' => "2時間以上<br />3時間未満")
                                 , 2 => array('data' => "3時間以上<br />4時間未満")
                                 , 3 => array('data' => "4時間以上<br />6時間未満")
                                 , 5 => array('data' => "6時間以上<br />8時間未満")
                                 , 6 => array('data' => "8時間以上<br />9時間未満")
                                 , 7 => array('data' => "9時間以上<br />10時間未満")
                                 , 8 => array('data' => '小計')
                                 , 9 => array('data' => '訪問リハビリ')
                                , 10 => array('data' => "介護予防<br />訪問リハビリ")
                                , 11 => array('data' => '合計')
                                , 12 => array('data' => '可')
                                , 13 => array('data' => '不可')
                              ),
                              $fontTag,
                              array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
                         )
                     .'</tr>';
        $rtnStr .= $header;

        $startRowNum    = 7;
        $startColumnNum = 1;
        $totalRowNum = $startRowNum + count($plantArr);
        $colAlphaName = $this->columnAlphaNameArr;
        foreach ($plantArr as $plantKey => $plantVal) {
            $rowNum = $startRowNum + $plantKey;
            $facilityId   = $this->getArrValueByKey('facility_id',   $plantVal);
            $facilityName = $this->getArrValueByKey('facility_name', $plantVal);
            $dailyLimit   = null;
            $enterLimit   = null;
            $dispNum7     = null;
            $dispNum14    = null;
            $dispNum21    = null;
            $dispNum28    = null;
            $dispNum35    = null;
            $dispNum42    = null;
            $dispNum49    = null;
            $dispNum53    = null;
            $dispNum60    = null;
            $dispNum74    = null;
            $dispNum85    = null;
            $dispNum67    = null;
            $dispNum81    = null;
            $dispNum89    = null;
            $dispNum101   = null;
            $dispNum102   = null;
            $dispNum103   = null;
            $dispNum104   = null;
            $dispNum105   = null;
            $dispNum96    = null;
            $dispNum100   = null;
            $dispNum106   = null;
            $dispNum107   = null;
            $dispNum108   = null;

            if (is_array($dataArr)) {
                foreach ($dataArr as $dataKey => $dataVal) {
                    if ($facilityId == $this->getArrValueByKey('jnl_facility_id', $dataVal)) {
                        $dailyLimit = $this->getArrValueByKey('daily_limit', $dataVal);
                        $enterLimit = $this->getArrValueByKey('enter_limit', $dataVal);
                        $dispNum7   = $this->getArrValueByKey('dispnum_7',   $dataVal);
                        $dispNum14  = $this->getArrValueByKey('dispnum_14',  $dataVal);
                        $dispNum21  = $this->getArrValueByKey('dispnum_21',  $dataVal);
                        $dispNum28  = $this->getArrValueByKey('dispnum_28',  $dataVal);
                        $dispNum35  = $this->getArrValueByKey('dispnum_35',  $dataVal);
                        $dispNum42  = $this->getArrValueByKey('dispnum_42',  $dataVal);
                        $dispNum49  = $this->getArrValueByKey('dispnum_49',  $dataVal);
                        $dispNum53  = $this->getArrValueByKey('dispnum_53',  $dataVal);
                        $dispNum60  = $this->getArrValueByKey('dispnum_60',  $dataVal);
                        $dispNum74  = $this->getArrValueByKey('dispnum_74',  $dataVal);
                        $dispNum85  = $this->getArrValueByKey('dispnum_85',  $dataVal);
                        $dispNum67  = $this->getArrValueByKey('dispnum_67',  $dataVal);
                        $dispNum81  = $this->getArrValueByKey('dispnum_81',  $dataVal);
                        $dispNum89  = $this->getArrValueByKey('dispnum_89',  $dataVal);
                        $dispNum101 = $this->getArrValueByKey('dispnum_101', $dataVal);
                        $dispNum102 = $this->getArrValueByKey('dispnum_102', $dataVal);
                        $dispNum103 = $this->getArrValueByKey('dispnum_103', $dataVal);
                        $dispNum104 = $this->getArrValueByKey('dispnum_104', $dataVal);
                        $dispNum105 = $this->getArrValueByKey('dispnum_105', $dataVal);
                        $dispNum96  = $this->getArrValueByKey('dispnum_96',  $dataVal);
                        $dispNum100 = $this->getArrValueByKey('dispnum_100', $dataVal);
                        $dispNum106 = $this->getArrValueByKey('dispnum_106', $dataVal);
                        $dispNum107 = $this->getArrValueByKey('dispnum_107', $dataVal);
                        $dispNum108 = $this->getArrValueByKey('dispnum_108', $dataVal);
                    }
                }
            }

            $mainData = array();
            # 通所
            $mainData[0]  = array('data' => $facilityName, 'style' => array('textAlign' => $textAlign_center, 'bgColor' => $bgColor_blue));
            $mainData[1]  = array('data' => $dailyLimit);
            $mainData[2]  = array('data' => $dispNum7);
            $mainData[3]  = array('data' => $dispNum14);
            $mainData[4]  = array('data' => $dispNum21);
            $mainData[5]  = array('data' => $dispNum28);
            $mainData[6]  = array('data' => $dispNum35);
            $mainData[7]  = array('data' => $dispNum42);
            $mainData[8]  = array('data' => $dispNum49);
            $mainData[9]  = array(
                'data' => sprintf(
                    '=%s+%s+%s+%s+%s+%s+%s'
                  , $this->coord($colAlphaName[$startColumnNum+2], $rowNum)
                  , $this->coord($colAlphaName[$startColumnNum+3], $rowNum)
                  , $this->coord($colAlphaName[$startColumnNum+4], $rowNum)
                  , $this->coord($colAlphaName[$startColumnNum+5], $rowNum)
                  , $this->coord($colAlphaName[$startColumnNum+6], $rowNum)
                  , $this->coord($colAlphaName[$startColumnNum+7], $rowNum)
                  , $this->coord($colAlphaName[$startColumnNum+8], $rowNum)
                )
            );
            $mainData[10] = array('data' => $dispNum53);
            $mainData[11] = array('data' => sprintf('=%s+%s', $this->coord($colAlphaName[$startColumnNum+9], $rowNum), $this->coord($colAlphaName[$startColumnNum+10], $rowNum)));
            $mainData[13] = array('data' => $this->getExcelDivData($this->coord($colAlphaName[$startColumnNum+11], $rowNum), $this->coord($colAlphaName[$startColumnNum+1], $rowNum)), 'style' => array('msoNumberFormat' => $msoNumberFormat_percent0_0));
            $mainData[12] = array('data' => $this->getExcelUtilizationRatesTrigona($colAlphaName[$startColumnNum+13], $rowNum, 95), 'style' => array('textAlign' => $textAlign_center));

            # 入所
            $mainData[14] = array('data' => $enterLimit);
            $mainData[15] = array('data' => $dispNum60);
            $mainData[16] = array('data' => $dispNum74);
            $mainData[17] = array('data' => $dispNum85);
            $mainData[18] = array('data' => $dispNum67);
            $mainData[19] = array('data' => $dispNum81);
            $mainData[20] = array('data' => $dispNum89);
            $mainData[21] = array('data' => sprintf('=%s+%s+%s+%s+%s+%s', $this->coord($colAlphaName[$startColumnNum+15], $rowNum), $this->coord($colAlphaName[$startColumnNum+16], $rowNum), $this->coord($colAlphaName[$startColumnNum+17], $rowNum), $this->coord($colAlphaName[$startColumnNum+18], $rowNum), $this->coord($colAlphaName[$startColumnNum+19], $rowNum), $this->coord($colAlphaName[$startColumnNum+20], $rowNum)));
            $mainData[22] = array('data' => $dispNum101);
            $mainData[23] = array('data' => $dispNum102);
            $mainData[24] = array('data' => $dispNum103);
            $mainData[25] = array('data' => $dispNum104);
            $mainData[26] = array('data' => $dispNum105);
            $mainData[27] = array('data' => $this->getExcelSubData($this->coord($colAlphaName[$startColumnNum+14], $rowNum), $this->coord($colAlphaName[$startColumnNum+21], $rowNum)));
            $mainData[29] = array('data' => $this->getExcelDivData($this->coord($colAlphaName[$startColumnNum+21], $rowNum), $this->coord($colAlphaName[$startColumnNum+14], $rowNum)), 'style' => array('msoNumberFormat' => $msoNumberFormat_percent0_0));
            $mainData[28] = array('data' => $this->getExcelUtilizationRatesTrigona($colAlphaName[$startColumnNum+29], $rowNum, 95), 'style' => array('textAlign' => $textAlign_center));

            # 訪問リハビリ
            $mainData[30] = array('data' => $dispNum96);
            $mainData[31] = array('data' => $dispNum100);
            $mainData[32] = array('data' => sprintf('=%s+%s', $this->coord($colAlphaName[$startColumnNum+30], $rowNum), $this->coord($colAlphaName[$startColumnNum+31], $rowNum)));

            # 判定件数
            $mainData[33] = array('data' => $dispNum106);
            $mainData[34] = array('data' => $dispNum107);

            # 訪問件数
            $mainData[35] = array('data' => $dispNum108);

            $totalData = array();
            for ($i=0; $i <= 35; $i++) {
                switch ($i) {
                    case '0':
                        $totalData[$i] = array('data' => '合計', 'style' => array('textAlign' => $textAlign_center, 'bgColor' => $bgColor_blue));
                        break;

                    case '9';
                        $totalData[$i]['data'] = sprintf(
                            '=%s+%s+%s+%s+%s+%s+%s'
                          , $this->coord($colAlphaName[$startColumnNum+2], $totalRowNum)
                          , $this->coord($colAlphaName[$startColumnNum+3], $totalRowNum)
                          , $this->coord($colAlphaName[$startColumnNum+4], $totalRowNum)
                          , $this->coord($colAlphaName[$startColumnNum+5], $totalRowNum)
                          , $this->coord($colAlphaName[$startColumnNum+6], $totalRowNum)
                          , $this->coord($colAlphaName[$startColumnNum+7], $totalRowNum)
                          , $this->coord($colAlphaName[$startColumnNum+8], $totalRowNum)
                        );
                        break;

                    case '11': case '32':
                        $totalData[$i] = array('data' => sprintf('=%s+%s', $this->coord($colAlphaName[$startColumnNum+$i-2], $totalRowNum), $this->coord($colAlphaName[$startColumnNum+$i-1], $totalRowNum)));
                        break;

                    case '12': case '28':
                        break;

                    case '13': case '29':
                        $leftKey  = ($i==13)? 11 : 21;
                        $rightKey = ($i==13)?  1 : 14;
                        $totalData[$i] = array(
                            'data'  => $this->getExcelDivData($this->coord($colAlphaName[$startColumnNum+$leftKey], $totalRowNum), $this->coord($colAlphaName[$startColumnNum+$rightKey], $totalRowNum))
                          , 'style' => array('msoNumberFormat' => $msoNumberFormat_percent0_0)
                        );
                        $totalData[$i-1] = array(
                            'data'  => $this->getExcelUtilizationRatesTrigona($colAlphaName[$startColumnNum+$i], $totalRowNum, 95)
                          , 'style' => array('textAlign' => $textAlign_center)
                        );
                        break;

                    case '21':
                        $totalData[$i] = array('data' => sprintf(
                            '=%s+%s+%s+%s+%s+%s'
                          , $this->coord($colAlphaName[$startColumnNum+15], $totalRowNum)
                          , $this->coord($colAlphaName[$startColumnNum+16], $totalRowNum)
                          , $this->coord($colAlphaName[$startColumnNum+17], $totalRowNum)
                          , $this->coord($colAlphaName[$startColumnNum+18], $totalRowNum)
                          , $this->coord($colAlphaName[$startColumnNum+19], $totalRowNum)
                          , $this->coord($colAlphaName[$startColumnNum+20], $totalRowNum)
                        ));
                        break;

                    default:
                        $totalData[$i] = array('data' => $this->getExcelLengthSumData($colAlphaName[$startColumnNum+$i], $startRowNum, $totalRowNum-1));
                        break;
                }

            }

            $rtnStr .= '<tr>'.$this->convertCellData($mainData, $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
        }

        $rtnStr .= '<tr>'.$this->convertCellData($totalData, $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr></table>';
        return $rtnStr;
    }

    /**
     * 施設別患者日報(シンプル版) のエクセル出力
     * @param  array $dataArr
     * @param  array $plantArr
     * @param  array $arr
     * @param  array $optionArr
     * @return string HTML table
     */
    function outputExcelPatientDailySimpleFacilities($dataArr, $plantArr, $arr, $optionArr)
    {
        $rtnStr  = $this->getExcelMeta();
        $fontTag = $this->getFontTag();

        $displayYear  = $this->getArrValueByKey('year',  $optionArr);
        $displayMonth = $this->getArrValueByKey('month', $optionArr);
        $displayDay   = $this->getArrValueByKey('day',   $optionArr);

        $rtnStr  .= sprintf('%04d年%02d月%02d日(%s)', $displayYear, $displayMonth, $displayDay, $this->getDayOfTheWeekForDate($displayYear, $displayMonth, $displayDay))."<br />";

        # attr
        $rowspan_2  = array('name' => 'rowspan', 'value' =>  2);
        $rowspan_3  = array('name' => 'rowspan', 'value' =>  3);
        $colspan_2  = array('name' => 'colspan', 'value' =>  2);
        $colspan_3  = array('name' => 'colspan', 'value' =>  3);
        $colspan_4  = array('name' => 'colspan', 'value' =>  4);
        $colspan_7  = array('name' => 'colspan', 'value' =>  7);
        $colspan_8  = array('name' => 'colspan', 'value' =>  8);
        $colspan_12 = array('name' => 'colspan', 'value' => 12);
        $colspan_13 = array('name' => 'colspan', 'value' => 13);
        $colspan_14 = array('name' => 'colspan', 'value' => 14);
        $colspan_15 = array('name' => 'colspan', 'value' => 15);
        $colspan_16 = array('name' => 'colspan', 'value' => 16);

        # style
        $bgColor_blue               = array('name' => 'background-color',  'value' => $this->getRgbColor('blue'));
        $textAlign_center           = array('name' => 'text-align',        'value' => 'center');
        $textAlign_right            = array('name' => 'text-align',        'value' => 'right');
        $whiteSpace_nowrap          = array('name' => 'white-space',       'value' => 'nowrap');
        $msoNumberFormat_percent    = $this->getMsoNumberFormat('percent');
        $msoNumberFormat_comma      = $this->getMsoNumberFormat('comma');

        $header = '<table class="list" width="100%" border="1" >'
                     .'<tr>'
                         .$this->convertCellData(
                              array(
                                  0 => array('data' => '&nbsp;', 'attr' => array($rowspan_2))
                                , 1 => array('data' => '通所', 'attr' => array($colspan_3))
                                , 2 => array('data' => '入所', 'attr' => array($colspan_7))
                                , 3 => array('data' => "訪問リハビリ<br />(単位)", 'attr' => array($rowspan_2))
                                , 4 => array('data' => "判定件数<br />（本入所のみ）", 'attr' => array($colspan_2))
                                , 5 => array('data' => "訪問件数<br />（訪問件数のみ）", 'attr' => array($rowspan_2))
                              )
                            , $fontTag
                            , array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
                         )
                     .'</tr>'
                     .'<tr>'
                         .$this->convertCellData(
                              array(
                                  0 => array('data' => '定員数')
                                , 1 => array('data' => '利用者数')
                                , 2 => array('data' => '稼働率')
                                , 3 => array('data' => '定員数')
                                , 4 => array('data' => '利用者数')
                                , 5 => array('data' => '入所後<br />(ショートスティ含む)')
                                , 6 => array('data' => '退所後<br />(ショートスティ含む)')
                                , 7 => array('data' => '空床数')
                                , 8 => array('data' => '稼働率', 'attr' => array($colspan_2))
                                , 9 => array('data' => '可')
                               , 10 => array('data' => '不可'),
                              )
                            , $fontTag
                            , array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
                         )
                     .'</tr>';
        $rtnStr .= $header;

        $startRowNum    = 6;
        $startColumnNum = 1;
        $totalRowNum    = $startRowNum + count($plantArr);
        $colAlphaName   = $this->columnAlphaNameArr;

        foreach ($plantArr as $plantKey => $plantVal) {
            $rowNum       = $startRowNum + $plantKey;
            $facilityId   = $this->getArrValueByKey('facility_id',   $plantVal);
            $facilityName = $this->getArrValueByKey('facility_name', $plantVal);
            $dailyLimit   = null;
            $enterLimit   = null;
            $dispNum7     = null;
            $dispNum14    = null;
            $dispNum21    = null;
            $dispNum28    = null;
            $dispNum35    = null;
            $dispNum42    = null;
            $dispNum49    = null;
            $dispNum53    = null;
            $dispNum60    = null;
            $dispNum74    = null;
            $dispNum85    = null;
            $dispNum67    = null;
            $dispNum81    = null;
            $dispNum89    = null;
            $dispNum101   = null;
            $dispNum102   = null;
            $dispNum103   = null;
            $dispNum104   = null;
            $dispNum105   = null;
            $dispNum96    = null;
            $dispNum100   = null;
            $dispNum106   = null;
            $dispNum107   = null;
            $dispNum108   = null;

            if (is_array($dataArr)) {
                foreach ($dataArr as $dataKey => $dataVal) {
                    if ($facilityId == $this->getArrValueByKey('jnl_facility_id', $dataVal)) {
                        $dailyLimit = $this->getArrValueByKey('daily_limit', $dataVal);
                        $enterLimit = $this->getArrValueByKey('enter_limit', $dataVal);
                        $dispNum7   = $this->getArrValueByKey('dispnum_7',   $dataVal);
                        $dispNum14  = $this->getArrValueByKey('dispnum_14',  $dataVal);
                        $dispNum21  = $this->getArrValueByKey('dispnum_21',  $dataVal);
                        $dispNum28  = $this->getArrValueByKey('dispnum_28',  $dataVal);
                        $dispNum35  = $this->getArrValueByKey('dispnum_35',  $dataVal);
                        $dispNum42  = $this->getArrValueByKey('dispnum_42',  $dataVal);
                        $dispNum49  = $this->getArrValueByKey('dispnum_49',  $dataVal);
                        $dispNum53  = $this->getArrValueByKey('dispnum_53',  $dataVal);
                        $dispNum60  = $this->getArrValueByKey('dispnum_60',  $dataVal);
                        $dispNum74  = $this->getArrValueByKey('dispnum_74',  $dataVal);
                        $dispNum85  = $this->getArrValueByKey('dispnum_85',  $dataVal);
                        $dispNum67  = $this->getArrValueByKey('dispnum_67',  $dataVal);
                        $dispNum81  = $this->getArrValueByKey('dispnum_81',  $dataVal);
                        $dispNum89  = $this->getArrValueByKey('dispnum_89',  $dataVal);
                        $dispNum101 = $this->getArrValueByKey('dispnum_101', $dataVal);
                        $dispNum102 = $this->getArrValueByKey('dispnum_102', $dataVal);
                        $dispNum103 = $this->getArrValueByKey('dispnum_103', $dataVal);
                        $dispNum104 = $this->getArrValueByKey('dispnum_104', $dataVal);
                        $dispNum105 = $this->getArrValueByKey('dispnum_105', $dataVal);
                        $dispNum96  = $this->getArrValueByKey('dispnum_96',  $dataVal);
                        $dispNum100 = $this->getArrValueByKey('dispnum_100', $dataVal);
                        $dispNum106 = $this->getArrValueByKey('dispnum_106', $dataVal);
                        $dispNum107 = $this->getArrValueByKey('dispnum_107', $dataVal);
                        $dispNum108 = $this->getArrValueByKey('dispnum_108', $dataVal);
                    }
                }
            }

            $dispArrNum2 = $dispNum7  + $dispNum14 + $dispNum21 + $dispNum28 + $dispNum35 + $dispNum42 + $dispNum49 + $dispNum53;
            $dispArrNum5 = $dispNum60 + $dispNum74 + $dispNum85 + $dispNum67 + $dispNum81 + $dispNum89;

            $mainRowCoord_1 = $this->coord($colAlphaName[$startColumnNum+1], $rowNum);
            $mainRowCoord_2 = $this->coord($colAlphaName[$startColumnNum+2], $rowNum);
            $mainRowCoord_4 = $this->coord($colAlphaName[$startColumnNum+4], $rowNum);
            $mainRowCoord_5 = $this->coord($colAlphaName[$startColumnNum+5], $rowNum);

            $mainData = array(
              # 施設名
                0 => array('data'  => $facilityName, 'style' => array('textAlign' => $textAlign_center, 'bgColor' => $bgColor_blue))
              # 通所 定員数
              , 1 => array('data' => $dailyLimit)
              # 通所 利用者数
              , 2 => array('data' => $dispArrNum2)
              # 通所 稼働率
              , 3 => array(
                    'data'  => sprintf('=IF(%s="", "", IF(%s=0, 0, ROUND(%s/%s, %d)))', $mainRowCoord_2, $mainRowCoord_1, $mainRowCoord_2, $mainRowCoord_1, 3)
                  , 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)
                )
              # 入所 定員数
              , 4 => array('data' => $enterLimit)
              # 入所 利用者数
              , 5 => array('data' => is_null($dispArrNum5)? null : $dispArrNum5)
              # 入所 入所後(ショートスティ含む)
              , 6 => array('data' => (is_null($dispNum101) && is_null($dispNum104))? null : $dispNum101 + $dispNum104)
              # 入所 退所後(ショートスティ含む)
              , 7 => array('data' => (is_null($dispNum102) && is_null($dispNum105)) ? null : $dispNum102 + $dispNum105)
              # 入所 空床数
              , 8 => array('data' => sprintf('=ROUND(ROUND(%s, %d)-ROUND(%s, %d), %d)', $mainRowCoord_4, 3, $mainRowCoord_5, 3, 3))
              # 入所 稼働率
              , 9 => array('data' => $this->getExcelUtilizationRatesTrigona($colAlphaName[$startColumnNum+10], $rowNum, 95))
             , 10 => array(
                   'data' => sprintf('=IF(%s="", "", IF(%s=0, 0, ROUND(%s/%s, %d)))', $mainRowCoord_5, $mainRowCoord_4, $mainRowCoord_5, $mainRowCoord_4, 3)
                 , 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)
               )
             # 訪問リハビリ
             , 11 => array('data' => (is_null($dispNum96) && is_null($dispNum100))? null : $dispNum96 + $dispNum100)
             # 判定件数 可
             , 12 => array('data' => $dispNum106)
             # 判定件数 不可
             , 13 => array('data' => $dispNum107)
             # 訪問件数
             , 14 => array('data' => $dispNum108)
            );

            $totalRowCoord_1 = $this->coord($colAlphaName[$startColumnNum+1], $totalRowNum);
            $totalRowCoord_2 = $this->coord($colAlphaName[$startColumnNum+2], $totalRowNum);
            $totalRowCoord_4 = $this->coord($colAlphaName[$startColumnNum+4], $totalRowNum);
            $totalRowCoord_5 = $this->coord($colAlphaName[$startColumnNum+5], $totalRowNum);

            for ($i=0; $i <= 14; $i++) {
                switch ($i) {
                    case '0':
                        $totalData[$i] = array('data' => '合計', 'style' => array('textAlign' => $textAlign_center, 'bgColor' => $bgColor_blue));
                        break;

                    case '3':
                        $totalData[$i] = array('data' => $this->getExcelDivData($totalRowCoord_2, $totalRowCoord_1, 3), 'style' => array('msoNumberFormat' => $msoNumberFormat_percent));
                        break;

                    case '9';
                        break;

                    case '10':
                        $totalData[$i] = array('data' => $this->getExcelDivData($totalRowCoord_5, $totalRowCoord_4, 3), 'style' => array('msoNumberFormat' => $msoNumberFormat_percent));
                        $totalData[$i-1]['data'] = $this->getExcelUtilizationRatesTrigona($colAlphaName[$startColumnNum+10], $totalRowNum, 95);

                        break;

                    default:
                        $totalData[$i]['data'] = $this->getExcelLengthSumData($colAlphaName[$startColumnNum+$i], $startRowNum, $totalRowNum-1);
                        break;
                }
            }

            $rtnStr .= '<tr>'.$this->convertCellData($mainData, $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
        }

        $rtnStr .= '<tr>'.$this->convertCellData($totalData, $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr></table>';
        return $rtnStr;
    }

    /**
	 * 施設別患者日報(累積統計表) のエクセル出力
     * @param  array  $dataArr
     * @param  array  $plantArr
     * @param  array  $arr
     * @param  array  $optionArr
     * @return string
     */
    function outputExcelDailyReportStats($dataArr, $plantArr, $arr, $optionArr)
    {
        $rtnStr    = '';
        $fontTag   = $this->getFontTag();
        $dispYear  = $this->getArrValueByKey('year',  $optionArr);
        $dispMonth = $this->getArrValueByKey('month', $optionArr);
        $dispDay   = $this->getArrValueByKey('day',   $optionArr);

        # attr
        $colspan_2  = array('name' => 'colspan', 'value' =>  2);
        $colspan_3  = array('name' => 'colspan', 'value' =>  3);
        $colspan_4  = array('name' => 'colspan', 'value' =>  4);
        $colspan_5  = array('name' => 'colspan', 'value' =>  5);
        $colspan_12 = array('name' => 'colspan', 'value' => 12);
        $rowspan_2  = array('name' => 'rowspan', 'value' =>  2);
        $rowspan_3  = array('name' => 'rowspan', 'value' =>  3);
        $rowspan_4  = array('name' => 'rowspan', 'value' =>  4);

        # style
        $bgColor_blue                        = array('name' => 'background-color',  'value' => $this->getRgbColor('blue'));
        $textAlign_center                    = array('name' => 'text-align',        'value' => 'center');
        $textAlign_right                     = array('name' => 'text-align',        'value' => 'right');
        $whiteSpace_nowrap                   = array('name' => 'white-space',       'value' => 'nowrap');
        $msoNumberFormat_percent             = $this->getMsoNumberFormat('percent');
        $msoNumberFormat_percent_parenthesis = $this->getMsoNumberFormat('percent_parenthesis' );
        $msoNumberFormat_0_0_parenthesis     = $this->getMsoNumberFormat('decimal_parenthesis');
        $msoNumberFormat_parenthesis         = $this->getMsoNumberFormat('comma_parenthesis');
        $msoNumberFormat_comma               = $this->getMsoNumberFormat('comma');

        $rtnStr .= '<table border="1" >'
                      .'<tr>'
                        .$this->convertCellData(
                            array(0 => array('data' => sprintf('%04d年%02d月%02d日(%s)', $dispYear, $dispMonth, $dispDay, $this->getDayOfTheWeekForDate($dispYear, $dispMonth, $dispDay))))
                          , $fontTag
                          , array('whiteSpace' => $whiteSpace_nowrap)
                        )
                      .'</tr>'
                  .'</table>';

        $targetDate = sprintf('%02d月%02d日', $dispMonth, $dispDay);
        $rtnStr .= '<table class="list" width="100%" border="1" >'
                      .'<tr>'
                      .$this->convertCellData(
                          array(
                              0 => array('data' => '&nbsp;', 'attr' => array($rowspan_4))
                            , 1 => array('data' => '通所', 'attr' => array($colspan_5))
                            , 2 => array('data' => '入所', 'attr' => array($colspan_12))
                            , 3 => array('data' => "訪問リハビリ<br />(単位)", 'attr' => array($colspan_2, $rowspan_3))
                            , 4 => array('data' => "判定件数<br />(本入所のみ)", 'attr' => array($colspan_4, $rowspan_2))
                            , 5 => array('data' => "営業件数<br />(訪問件数のみ)", 'attr' => array($colspan_2, $rowspan_3))
                          ),
                          $fontTag,
                          array('textAlign' => $textAlign_center, 'bgColor' => $bgColor_blue, 'whiteSpace' => $whiteSpace_nowrap)
                      )
                      .'</tr>'
                      .'<tr>'
                      .$this->convertCellData(
                          array(
                              0 => array('data' => '定員数', 'attr' => array($rowspan_3))
                            , 1 => array('data' => '利用者数', 'attr' => array($rowspan_2, $colspan_2))
                            , 2 => array('data' => '稼働率', 'attr' => array($rowspan_2, $colspan_2))
                            , 3 => array('data' => '定員数', 'attr' => array($rowspan_3))
                            , 4 => array('data' => '利用者数', 'attr' => array($rowspan_2, $colspan_2))
                            , 5 => array('data' => "入所数<br />ショート含む", 'attr' => array($rowspan_2, $colspan_2))
                            , 6 => array('data' => "退所数<br />ショート含む", 'attr' => array($rowspan_2, $colspan_2))
                            , 7 => array('data' => '空床数', 'attr' => array($rowspan_3))
                            , 8 => array('data' => '稼働率', 'attr' => array($rowspan_2, $colspan_4))
                          ),
                          $fontTag,
                          array('textAlign' => $textAlign_center, 'bgColor' => $bgColor_blue, 'whiteSpace' => $whiteSpace_nowrap)
                      )
                      .'</tr>'
                      .'<tr>'
                      .$this->convertCellData(
                          array(
                              0 => array('data' => '可',   'attr' => array($colspan_2))
                            , 1 => array('data' => '不可', 'attr' => array($colspan_2))
                          ),
                          $fontTag,
                          array('textAlign' => $textAlign_center, 'bgColor' => $bgColor_blue, 'whiteSpace' => $whiteSpace_nowrap)
                      )
                      .'</tr>'
                      .'<tr>'
                      .$this->convertCellData(
                          array(
                              0 => array('data' => $targetDate), 1 => array('data' => '月平均')
                            , 2 => array('data' => $targetDate), 3 => array('data' => '月平均')
                            , 4 => array('data' => $targetDate), 5 => array('data' => '月平均')
                            , 6 => array('data' => $targetDate), 7 => array('data' => '月累計')
                            , 8 => array('data' => $targetDate), 9 => array('data' => '月累計')
                           , 10 => array('data' => $targetDate, 'attr' => array($colspan_2))
                           , 11 => array('data' => '月平均',    'attr' => array($colspan_2))
                           , 12 => array('data' => $targetDate), 13 => array('data' => '月累計')
                           , 14 => array('data' => $targetDate), 15 => array('data' => '月累計')
                           , 16 => array('data' => $targetDate), 17 => array('data' => '月累計')
                           , 18 => array('data' => $targetDate), 19 => array('data' => '月累計')
                          ),
                          $fontTag,
                          array('textAlign' => $textAlign_center, 'bgColor' => $bgColor_blue, 'whiteSpace' => $whiteSpace_nowrap)
                      )
                      .'</tr>';

        $leftSide       = array();
        $mainData       = array();
        $totalLeftSide  = array();
        $totalMainData  = array();
        $startRowNum    = 6;
        $startColumnNum = 2;
        $totalRowNum    = $startRowNum + count($plantArr);
        $colAlphaName   = $this->columnAlphaNameArr;

        if (is_array($plantArr)) {
            foreach ($plantArr as $plantKey => $plantVal) {
                $rowNum     = $startRowNum + $plantKey;
                $facilityId = $this->getArrValueByKey('facility_id', $plantVal);

                for ($c=1; $c<=108; $c++) {
                    $dispNum[$c]    = null;
                    $sumDispNum[$c] = null;
                }
                $dailyLimit    = null;
                $enterLimit    = null;
                $sumDailyLimit = null;
                $sumEnterLimit = null;
                $dayCnt        = null;
                $holidayCnt    = null;

                foreach ($dataArr as $dataVal) {
                    if ($facilityId == $dataVal['jnl_facility_id'] && $dispYear == $dataVal['year'] && $dispMonth == $dataVal['month']) {
                        for ($c=1; $c<=108; $c++) {
                            $dispNum[$c]    = $this->getArrayValueByKeyForEmptyIsNull('dispnum_'.$c,     $dataVal);
                            $sumDispNum[$c] = $this->getArrayValueByKeyForEmptyIsNull('sum_dispnum_'.$c, $dataVal);
                        }

                        $dailyLimit    = $this->getArrValueByKey('daily_limit',     $dataVal);
                        $enterLimit    = $this->getArrValueByKey('enter_limit',     $dataVal);
                        $sumDailyLimit = $this->getArrValueByKey('sum_daily_limit', $dataVal);
                        $sumEnterLimit = $this->getArrValueByKey('sum_enter_limit', $dataVal);
                        $dayCnt        = $this->getArrValueByKey('day_cnt',         $dataVal);
                        $holidayCnt    = $this->getArrValueByKey('holiday_cnt',     $dataVal);
                    }
                }

				$check_month = sprintf('%02d', $dispMonth);
				if($check_month == "01")
				{
					//表示月が1月である
					
					//休日指定用設定ファイルを読み込む
					require_once("jnl_nurse_home_new_year_holiday.php");
					
					$check_day = sprintf('%02d', $dispDay);
					foreach ($new_year_holiday[$dispYear.$check_month] as $holdayVal) 
					{
						if($check_day == $holdayVal)
						{
							//外部ファイルに掲載されている日付（休日）なので日付カウントしない
							$dayCnt=1;
							$check_flg=1;
						}
					}
				}
				
				if(($check_month == "01")&&($check_flg!=1))
				{
					$dayCnt = ($dayCnt - count($new_year_holiday[$dispYear.$check_month]));
					$check_flg=0;
				}
				

                $leftSide[0] = array('data' => $this->getArrValueByKey('facility_name', $plantVal));

                # 通所 定員数
                $mainData[0] = array('data' => $dailyLimit);
                $colnum0Data = $this->coord($colAlphaName[$startColumnNum+0], $rowNum);

                # 通所 利用者数(○月○日)
                $mainData[1] = array('data' => sprintf('=IF(%s+%s+%s+%s+%s+%s+%s+%s=0, "0", %s+%s+%s+%s+%s+%s+%s+%s)', (int)$dispNum[7], (int)$dispNum[14], (int)$dispNum[21], (int)$dispNum[28], (int)$dispNum[35], (int)$dispNum[42], (int)$dispNum[49], (int)$dispNum[53], (int)$dispNum[7], (int)$dispNum[14], (int)$dispNum[21], (int)$dispNum[28], (int)$dispNum[35], (int)$dispNum[42], (int)$dispNum[49], (int)$dispNum[53]));
                $colnum1Data = $this->coord($colAlphaName[$startColumnNum+1], $rowNum);

                # 通所 利用者数(月平均)
                $mainData[2] = array(
                    'data'        => sprintf('=IF((%s-%s)=0, "0", ROUND((%s+%s+%s+%s+%s+%s+%s+%s)/(%s-%s),1))', (int)$dayCnt, (int)$holidayCnt, (int)$sumDispNum[7], (int)$sumDispNum[14], (int)$sumDispNum[21], (int)$sumDispNum[28], (int)$sumDispNum[35], (int)$sumDispNum[42], (int)$sumDispNum[49], (int)$sumDispNum[53], (int)$dayCnt, (int)$holidayCnt)
                  , 'numerator'   => $sumDispNum[7]+$sumDispNum[14]+$sumDispNum[21]+$sumDispNum[28]+$sumDispNum[35]+$sumDispNum[42]+$sumDispNum[49]+$sumDispNum[53]
                  , 'denominator' => $dayCnt-$holidayCnt
                  , 'style'       => array('msoNumberFormat' => $msoNumberFormat_0_0_parenthesis)
                );

                # 通所 稼働数(○月○日)
                $mainData[3] = array(
                     'data'        => sprintf('=IF(%s="", "", IF(%s=0, "0", (%s/%s)))', $colnum1Data, $colnum0Data, $colnum1Data, $colnum0Data)
                   , 'numerator'   => $dispNum[7]+$dispNum[14]+$dispNum[21]+$dispNum[28]+$dispNum[35]+$dispNum[42]+$dispNum[49]+$dispNum[53]
                   , 'denominator' => $dailyLimit
                   , 'style'       => array('msoNumberFormat' => $msoNumberFormat_percent)
                );

                # 通所 稼働数(月平均)

                $mainData_4_numerator = $sumDispNum[7]+$sumDispNum[14]+$sumDispNum[21]+$sumDispNum[28]+$sumDispNum[35]+$sumDispNum[42]+$sumDispNum[49]+$sumDispNum[53];
                $mainData_4_numerator_excel = sprintf('(%s+%s+%s+%s+%s+%s+%s+%s)', (int)$sumDispNum[7], (int)$sumDispNum[14], (int)$sumDispNum[21], (int)$sumDispNum[28], (int)$sumDispNum[35], (int)$sumDispNum[42], (int)$sumDispNum[49], (int)$sumDispNum[53]);
                $mainData[4] = array(
                    'data'        => sprintf('=IF(%s="", "", IF(%s=0, "0", (%s/%s)))', $mainData_4_numerator_excel, (int)$sumDailyLimit, $mainData_4_numerator_excel, (int)$sumDailyLimit)
                  , 'numerator'   => $mainData_4_numerator
                  , 'denominator' => $sumDailyLimit
                  , 'style'       => array('msoNumberFormat' => $msoNumberFormat_percent_parenthesis)
                );

                # 入所 定員数
                $mainData[5] = array('data' => $enterLimit);
                $colnum5Data = $this->coord($colAlphaName[$startColumnNum+5], $rowNum);

                # 入所 利用者数(○月○日)
                $mainData[6] = array('data' => sprintf('=if(%s+%s+%s+%s+%s+%s=0, "0", (%s+%s+%s+%s+%s+%s))', (int)$dispNum[60], (int)$dispNum[74], (int)$dispNum[85], (int)$dispNum[67], (int)$dispNum[81], (int)$dispNum[89], (int)$dispNum[60], (int)$dispNum[74], (int)$dispNum[85], (int)$dispNum[67], (int)$dispNum[81], (int)$dispNum[89]));
                $colnum6Data = $this->coord($colAlphaName[$startColumnNum+6], $rowNum);

                # 入所 利用者数(月平均)
                $mainData[7] = array(
                    'data'        => sprintf('=IF(%s=0,"0",ROUND((%s+%s+%s+%s+%s+%s)/%s,1))', (int)$dayCnt, (int)$sumDispNum[60], (int)$sumDispNum[74], (int)$sumDispNum[85], (int)$sumDispNum[67], (int)$sumDispNum[81], (int)$sumDispNum[89], (int)$dayCnt)
                  , 'numerator'   => $sumDispNum[60]+$sumDispNum[74]+$sumDispNum[85]+$sumDispNum[67]+$sumDispNum[81]+$sumDispNum[89]
                  , 'denominator' => $dayCnt
                  , 'style'       => array('msoNumberFormat' => $msoNumberFormat_0_0_parenthesis)
                );

                # 入所 入所数(ショート含む)(○月○日)
                $mainData[8] = array('data' => sprintf('=IF((%s+%s)=0, "0", (%s+%s))', (int)$dispNum[101], (int)$dispNum[104], (int)$dispNum[101], (int)$dispNum[104]));

                # 入所 入所数(ショート含む)(月累積)
                $mainData[9] = array(
                    'data' => sprintf('=IF((%s+%s)=0, "(0)", (%s+%s))', (int)$sumDispNum[101], (int)$sumDispNum[104], (int)$sumDispNum[101], (int)$sumDispNum[104])
                  , 'style' => array('msoNumberFormat' => $msoNumberFormat_parenthesis)
                );

                # 入所 退所数(ショート含む)(○月○日)
                $mainData[10] = array('data' => sprintf('=IF((%s+%s)=0, "0", (%s+%s))', (int)$dispNum[102], (int)$dispNum[105], (int)$dispNum[102], (int)$dispNum[105]));

                # 入所 退所数(ショート含む)(月累積)
                $mainData[11] = array(
                    'data' => sprintf('=IF((%s+%s)=0, "(0)", (%s+%s))', (int)$sumDispNum[102], (int)$sumDispNum[105], (int)$sumDispNum[102], (int)$sumDispNum[105])
                  , 'style' => array('msoNumberFormat' => $msoNumberFormat_parenthesis)
                );

                # 入所 空床数
                $mainData[12] = array('data' => sprintf('=IF(%s="", "", IF(%s=0, "0", %s-%s))', $colnum6Data, $colnum6Data, $colnum5Data, $colnum6Data));

                # 入所 稼働率(○月○日)
                $mainData[14] = array(
                    'data'        => $this->getExcelDivData($colnum6Data, $colnum5Data)
                  , 'numerator'   => $dispNum[60]+$dispNum[74]+$dispNum[85]+$dispNum[67]+$dispNum[81]+$dispNum[89]
                  , 'denominator' => $enterLimit
                  , 'style'       => array('msoNumberFormat' => $msoNumberFormat_percent)
                );
                $mainData[13] = array('data' => $this->getExcelUtilizationRatesTrigona($colAlphaName[$startColumnNum+14], $rowNum, 95));

                # 入所 稼働率(月平均)
                $mainData_16_nomerator = $sumDispNum[60]+$sumDispNum[74]+$sumDispNum[85]+$sumDispNum[67]+$sumDispNum[81]+$sumDispNum[89];
                $mainData_16_nomerator_excel = sprintf('(%s+%s+%s+%s+%s+%s)', (int)$sumDispNum[60], (int)$sumDispNum[74], (int)$sumDispNum[85], (int)$sumDispNum[67], (int)$sumDispNum[81], (int)$sumDispNum[89]);
                $mainData[16] = array(
                    'data'        => sprintf('=IF(%s="", "", IF(%s=0, "0", (%s/%s)))', $mainData_16_nomerator_excel, (int)$sumEnterLimit, $mainData_16_nomerator_excel, (int)$sumEnterLimit)
                  , 'numerator'   => $mainData_16_nomerator
                  , 'denominator' => $sumEnterLimit
                  , 'style'       => array('msoNumberFormat' => $msoNumberFormat_percent_parenthesis)
                );
                $mainData[15] = array('data' => $this->getExcelUtilizationRatesTrigona($colAlphaName[$startColumnNum+16], $rowNum, 95));

                # 訪問リハビリ(○月○日)
                $mainData[17] = array('data' => sprintf('=IF((%s+%s)=0, "0", (%s+%s))', (int)$dispNum[96], (int)$dispNum[100], (int)$dispNum[96], (int)$dispNum[100]));

                # 訪問リハビリ(月累積)
                $mainData[18] = array(
                    'data' => sprintf('=IF((%s+%s)=0, "(0)", (%s+%s))', (int)$sumDispNum[96], (int)$sumDispNum[100], (int)$sumDispNum[96], (int)$sumDispNum[100])
                  , 'style' => array('msoNumberFormat' => $msoNumberFormat_parenthesis)
                );

                # 判定件数 可(○月○日)
				if($dispNum[106] == null)
				{
					$dispNum[106] = 0;
				}
                $mainData[19] = array('data' => $dispNum[106]);

                # 判定件数 可(月累積)
				if($sumDispNum[106] == null)
				{
					$sumDispNum[106] = 0;
				}
				$mainData[20] = array('data' => $sumDispNum[106], 'style' => array('msoNumberFormat' => $msoNumberFormat_parenthesis));

                # 判定件数 不可(○月○日)
				if($dispNum[107] == null)
				{
					$dispNum[107] = 0;
				}
				$mainData[21] = array('data' => $dispNum[107]);

                # 判定件数 不可(月累積)
				if($sumDispNum[107] == null)
				{
					$sumDispNum[107] = 0;
				}
				$mainData[22] = array('data' => $sumDispNum[107], 'style' => array('msoNumberFormat' => $msoNumberFormat_parenthesis));

                # 営業件数 (○月○日)
				if($dispNum[108] == null)
				{
					$dispNum[108] = 0;
				}
				$mainData[23] = array('data' => $dispNum[108]);

                # 営業件数 (月累積)
				if($sumDispNum[108] == null)
				{
					$sumDispNum[108] = 0;
				}
				$mainData[24] = array('data' => $sumDispNum[108], 'style' => array('msoNumberFormat' => $msoNumberFormat_parenthesis));

                for ($i=0; $i <= 24; $i++) {
                    switch ($i) {
                        default:
                            $totalMainData[$i]['data'] = $this->getExcelSumData($colAlphaName[$startColumnNum+$i], $startRowNum, $colAlphaName[$startColumnNum+$i], $totalRowNum-1);
                            break;

						case '2': case '7':# 通所 利用者数(月平均)、入所 利用者数(月平均)
							//全施設の平均を表示するのをやめて、全施設の合計を表示するように仕様変更しました
                            //$totalMainData[$i]['numerator']   = $totalMainData[$i]['numerator']   + $mainData[$i]['numerator'];
                            //$totalMainData[$i]['denominator'] = $totalMainData[$i]['denominator'] + $mainData[$i]['denominator'];
                            //$totalMainData[$i]['data']        = sprintf('=IF(%s=0,0,round(%s/%s,1))', $totalMainData[$i]['denominator'], $totalMainData[$i]['numerator'], $totalMainData[$i]['denominator']);
                            //$totalMainData[$i]['style']       = array('msoNumberFormat' => $msoNumberFormat_0_0_parenthesis);
							$totalMainData[$i]['data']  = $this->getExcelSumData($colAlphaName[$startColumnNum+$i], $startRowNum, $colAlphaName[$startColumnNum+$i], $totalRowNum-1);
							$totalMainData[$i]['style'] = array('msoNumberFormat' => $msoNumberFormat_0_0_parenthesis);

                            break;

                        case  '3': # 通所 稼働数(○月○日)
                            $totalMainData[$i]['numerator']   = $totalMainData[$i]['numerator']   + $mainData[$i]['numerator'];
                            $totalMainData[$i]['denominator'] = $totalMainData[$i]['denominator'] + $mainData[$i]['denominator'];
                            $totalMainData[$i]['style']       = array('msoNumberFormat' => $msoNumberFormat_percent);
                            $totalMainData[$i]['data']        = $this->getExcelDivData($totalMainData[$i]['numerator'], $totalMainData[$i]['denominator']);
                            break;

                        case  '4': # 通所 稼働数(月平均)
                            $totalMainData[$i]['numerator']   = $totalMainData[$i]['numerator']   + $mainData[$i]['numerator'];
                            $totalMainData[$i]['denominator'] = $totalMainData[$i]['denominator'] + $mainData[$i]['denominator'];
                            $totalMainData[$i]['style']       = array('msoNumberFormat' => $msoNumberFormat_percent_parenthesis);
                            $totalMainData[$i]['data']        = $this->getExcelDivData($totalMainData[$i]['numerator'], $totalMainData[$i]['denominator']);
                            break;

                        case '13': case '15':
                            break;

                        case '14': # 入所 稼働率(○月○日)
                            $totalMainData[$i]['numerator']   = $totalMainData[$i]['numerator']   + $mainData[$i]['numerator'];
                            $totalMainData[$i]['denominator'] = $totalMainData[$i]['denominator'] + $mainData[$i]['denominator'];
                            $totalMainData[$i]['style']       = array('msoNumberFormat' => $msoNumberFormat_percent);
                            $totalMainData[$i]['data']        = $this->getExcelDivData($totalMainData[$i]['numerator'], $totalMainData[$i]['denominator']);
                            $totalMainData[$i-1]['data']      = $this->getExcelUtilizationRatesTrigona($colAlphaName[$startColumnNum+$i],$totalRowNum, 95);
                            break;

                        case '16': # 入所 稼働率(月平均)
                            $totalMainData[$i]['numerator']   = $totalMainData[$i]['numerator']   + $mainData[$i]['numerator'];
                            $totalMainData[$i]['denominator'] = $totalMainData[$i]['denominator'] + $mainData[$i]['denominator'];
                            $totalMainData[$i]['style']       = array('msoNumberFormat' => $msoNumberFormat_percent_parenthesis);
                            $totalMainData[$i]['data']        = $this->getExcelDivData($totalMainData[$i]['numerator'], $totalMainData[$i]['denominator']);
                            $totalMainData[$i-1]['data']      = $this->getExcelUtilizationRatesTrigona($colAlphaName[$startColumnNum+$i],$totalRowNum, 95);
                            break;

						case '9': case '11': case '18': case '20': case '22': case '24':
                            $totalMainData[$i]['data']  = $this->getExcelSumData($colAlphaName[$startColumnNum+$i], $startRowNum, $colAlphaName[$startColumnNum+$i], $totalRowNum-1);
                            $totalMainData[$i]['style'] = array('msoNumberFormat' => $msoNumberFormat_parenthesis);
                            break;
                    }
                }

                $rtnStr .= '<tr>'
                              .$this->convertCellData($leftSide, $fontTag, array('textAlign' => $textAlign_center, 'bgColor' => $bgColor_blue, 'whiteSpace' => $whiteSpace_nowrap))
                              .$this->convertCellData($mainData, $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma))
                          .'</tr>';
            }
        }

        $totalLeftSide[] = array('data' => '合計');
        $rtnStr .= '<tr>'
                      .$this->convertCellData($totalLeftSide, $fontTag, array('textAlign' => $textAlign_center, 'bgColor' => $bgColor_blue, 'whiteSpace' => $whiteSpace_nowrap))
                      .$this->convertCellData($totalMainData, $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma))
                  .'</tr>';
        $rtnStr .= '</table>';

        return $rtnStr;
    }

    /**
     * 施設別患者月報のエクセル出力
     * @param  array  $dataArr
     * @param  array  $plantArr
     * @param  array  $arr
     * @param  array  $optionArr
     * @return string
     */
    function outputExcelPatientMonReport($dataArr, $plantArr, $arr, $optionArr)
    {
        $rtnStr        = $this->getExcelMeta();
        $fontTag       = $this->getFontTag();
        $indicatorData = $this->getIndicatorNurseHomeData();
        $displayYear   = $this->getArrValueByKey('year',  $optionArr);
        $displayMonth  = $this->getArrValueByKey('month', $optionArr);
        $dayCntArr     = $indicatorData->getDayCountOfMonth($displayYear, $displayMonth);

        # attr
        $rowspan_2  = array('name' => 'rowspan', 'value' =>  2);
        $rowspan_3  = array('name' => 'rowspan', 'value' =>  3);
        $colspan_2  = array('name' => 'colspan', 'value' =>  2);
        $colspan_3  = array('name' => 'colspan', 'value' =>  3);
        $colspan_8  = array('name' => 'colspan', 'value' =>  8);
        $colspan_13 = array('name' => 'colspan', 'value' => 13);
        $colspan_17 = array('name' => 'colspan', 'value' => 17);

        # style
        $bgColor_blue               = array('name' => 'background-color',  'value' => $this->getRgbColor('blue'));
        $bgColor_lightpink          = array('name' => 'background-color',  'value' => $this->getRgbColor('lightpink'));
        $bgColor_weekday            = array('name' => 'background-color',  'value' => $this->getRgbColor('weekday'));
        $bgColor_saturday           = array('name' => 'background-color',  'value' => $this->getRgbColor('saturday'));
        $bgColor_holiday            = array('name' => 'background-color',  'value' => $this->getRgbColor('holiday'));
        $textAlign_center           = array('name' => 'text-align',        'value' => 'center');
        $textAlign_right            = array('name' => 'text-align',        'value' => 'right');
        $textAlign_left             = array('name' => 'text-align',        'value' => 'left');
        $whiteSpace_nowrap          = array('name' => 'white-space',       'value' => 'nowrap');
        $msoNumberFormat_percent0_0 = $this->getMsoNumberFormat('percent');
        $msoNumberFormat_comma      = $this->getMsoNumberFormat('comma');

        $header = '<table class="list" border="1" >'
                     .'<tr>'
                         .$this->convertCellData(
                             array(
                                 0 => array('data' => '平日', 'style' => array('bgColor' => $bgColor_weekday, 'textAlign' => $textAlign_left))
                               , 1 => array('data' => $this->getArrValueByKey('weekday', $dayCntArr).'日')
                               , 2 => array('data' => '土曜', 'style' => array('bgColor' => $bgColor_saturday, 'textAlign' => $textAlign_left))
                               , 3 => array('data' => $this->getArrValueByKey('saturday', $dayCntArr).'日')
                               , 4 => array('data' => '休日', 'style' => array('bgColor' => $bgColor_lightpink, 'textAlign' => $textAlign_left))
                               , 5 => array('data' => $this->getArrValueByKey('holiday', $dayCntArr).'日')
                             ),
                             $fontTag,
                             array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
                         )
                     .'</tr>'
                 .'</table>';

        $tableHeader = '<table class="list" width="100%" border="1" >'
                          .'<tr>'
                              .$this->convertCellData(
                                   array(
                                       0 => array('data' => '&nbsp;', 'attr' => array($rowspan_3))
                                     , 1 => array('data' => '通所', 'attr' => array($colspan_13))
                                     , 2 => array('data' => '入所', 'attr' => array($colspan_17))
                                     , 3 => array('data' => "訪問リハビリ<br />(単位)", 'attr' => array($rowspan_2, $colspan_3))
                                     , 4 => array('data' => "判定件数<br />(本入所のみ)", 'attr' => array($rowspan_2, $colspan_2))
                                     , 5 => array('data' => "営業件数<br />(訪問件数のみ)", 'attr' => array($rowspan_3))
                                   )
                                 , $fontTag
                                 , array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_blue)
                              )
                          .'</tr>'
                          .'<tr>'
                              .$this->convertCellData(
                                 array(
                                 # 通所
                                     0 => array('data' => '定員数', 'attr' => array($rowspan_2))
                                   , 1 => array('data' => '通所リハビリ', 'attr' => array($colspan_8))
                                   , 2 => array('data' => "予防通所<br />リハビリ", 'attr' => array($rowspan_2))
                                   , 3 => array('data' => '合計', 'attr' => array($rowspan_2))
                                   , 4 => array('data' => '稼働率', 'attr' => array($rowspan_2, $colspan_2))

                                 # 入所
                                   , 5 => array('data' => '定員数', 'attr' => array($rowspan_2))
                                   , 6 => array('data' => '一般', 'attr' => array($rowspan_2))
                                   , 7 => array('data' => "一般<br />ショートステイ", 'attr' => array($rowspan_2))
                                   , 8 => array('data' => "一般<br />予防ショート", 'attr' => array($rowspan_2))
                                   , 9 => array('data' => '認知', 'attr' => array($rowspan_2))
                                  , 10 => array('data' => "認知<br />ショートステイ", 'attr' => array($rowspan_2))
                                  , 11 => array('data' => "認知<br />予防ショート", 'attr' => array($rowspan_2))
                                  , 12 => array('data' => '小計', 'attr' => array($rowspan_2))
                                  , 13 => array('data' => '入所数', 'attr' => array($rowspan_2))
                                  , 14 => array('data' => $this->sc(1).'退所数', 'attr' => array($rowspan_2))
                                  , 15 => array('data' => $this->sc(1)."の内、<br />在宅復帰人数", 'attr' => array($rowspan_2))
                                  , 16 => array('data' => '在宅復帰率', 'attr' => array($rowspan_2))
                                  , 17 => array('data' => "ショート<br />予防ショート<br />入所数", 'attr' => array($rowspan_2))
                                  , 18 => array('data' => "ショート<br />予防ショート<br />退所数", 'attr' => array($rowspan_2))
                                  , 19 => array('data' => '空床数', 'attr' => array($rowspan_2))
                                  , 20 => array('data' => '稼働率', 'attr' => array($rowspan_2, $colspan_2))
                                 )
                               , $fontTag
                               , array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_blue)
                             )
                          .'</tr>'
                          .'<tr>'
                              .$this->convertCellData(
                                 array(
                                 # 通所
                                     0 => array('data' => "1時間以上<br />2時間未満")
                                   , 1 => array('data' => "2時間以上<br />3時間未満")
                                   , 2 => array('data' => "3時間以上<br />4時間未満")
                                   , 3 => array('data' => "4時間以上<br />6時間未満")
                                   , 4 => array('data' => "6時間以上<br />8時間未満")
                                   , 5 => array('data' => "8時間以上<br />9時間未満")
                                   , 6 => array('data' => "9時間以上<br />10時間未満")
                                   , 7 => array('data' => '小計')
                                 # 訪問リハビリ
                                   , 8 => array('data' => "訪問<br />リハビリ")
                                   , 9 => array('data' => "介護予防<br />訪問リハビリ")
                                  , 10 => array('data' => '合計')
                                 # 判定検数
                                  , 11 => array('data' => '可')
                                  , 12 => array('data' => '不可')
                                 )
                               , $fontTag
                               , array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_blue)
                             )
                          .'</tr>';

        $rtnStr .= $header;
        $rtnStr .= $tableHeader;

        $leftSide      = array();
        $mainData      = array();
        $totalLeftSide = array();
        $totalMainData = array();

        $startRowNum     = 7;
        $startColumnNum  = 2;
        $columnAlphaName = $this->columnAlphaNameArr;
        $totalRowNum     = $startRowNum + count($plantArr);

        foreach ($plantArr as $plantKey => $plantVal) {
            $rowNum              = $startRowNum + $plantKey;
            $facilityId          = $this->getArrValueByKey('facility_id',   $plantVal);
            $leftSide[0]['data'] = $this->getArrValueByKey('facility_name', $plantVal);
            $dailyLimit          = null;
            $enterLimit          = null;
            $dailyLimitAvrg      = null;
            $enterLimitAvrg      = null;

            for ($c=1; $c<=108; $c++) { $dispNum[$c] = null; }

            if (is_array($dataArr)) {
                foreach ($dataArr as $dataVal) {
                    if ($facilityId == $dataVal['jnl_facility_id'] && $displayMonth == $dataVal['month'] && $displayYear == $dataVal['year']) {
                        for ($c=1; $c<=108; $c++) { $dispNum[$c] = $this->getArrValueByKey('dispnum_'.$c, $dataVal); }
                        $dailyLimit     = $this->getArrValueByKey('daily_limit',      $dataVal);
                        $enterLimit     = $this->getArrValueByKey('enter_limit',      $dataVal);
                        $dailyLimitAvrg = $this->getArrValueByKey('daily_limit_avrg', $dataVal);
                        $enterLimitAvrg = $this->getArrValueByKey('enter_limit_avrg', $dataVal);
                    }
                }
            }

            # 通所 定員数
            $mainData[0] = array('data' => $dailyLimit);
            $mainData[1] = array('data' => $dispNum[7]);
            $mainData[2] = array('data' => $dispNum[14]);
            $mainData[3] = array('data' => $dispNum[21]);
            $mainData[4] = array('data' => $dispNum[28]);
            $mainData[5] = array('data' => $dispNum[35]);
            $mainData[6] = array('data' => $dispNum[42]);
            $mainData[7] = array('data' => $dispNum[49]);

            # 通所 小計
            $cellNum8Data = sprintf(
                '=%s+%s+%s+%s+%s+%s+%s'
              , $this->coord($columnAlphaName[$startColumnNum+1], $rowNum)
              , $this->coord($columnAlphaName[$startColumnNum+2], $rowNum)
              , $this->coord($columnAlphaName[$startColumnNum+3], $rowNum)
              , $this->coord($columnAlphaName[$startColumnNum+4], $rowNum)
              , $this->coord($columnAlphaName[$startColumnNum+5], $rowNum)
              , $this->coord($columnAlphaName[$startColumnNum+6], $rowNum)
              , $this->coord($columnAlphaName[$startColumnNum+7], $rowNum)
            );
            $mainData[8] = array('data' => $cellNum8Data);
            $mainData[9] = array('data' => $dispNum[53]);

            # 通所 合計
            $mainData[10] = array('data' => sprintf('=%s+%s', $this->coord($columnAlphaName[$startColumnNum+8], $rowNum), $this->coord($columnAlphaName[$startColumnNum+9], $rowNum), 3));

            # 通所 稼働率
            $cellNum12Data = $this->getExcelDivData($this->coord($columnAlphaName[$startColumnNum+10], $rowNum), $dailyLimit, 3);
            $mainData[12] = array('data' => $cellNum12Data, 'limit' => $dailyLimit, 'style' => array('msoNumberFormat' => $msoNumberFormat_percent0_0));
            $mainData[11] = array(
                'data'  => $this->getExcelUtilizationRatesTrigona($columnAlphaName[$startColumnNum+12], $rowNum, 95),
                'style' => array('textAlign' => $textAlign_center)
            );

            # 入所 定員数
            $mainData[13] = array('data' => $enterLimit);
            $mainData[14] = array('data' => $dispNum[60]);
            $mainData[15] = array('data' => $dispNum[74]);
            $mainData[16] = array('data' => $dispNum[85]);
            $mainData[17] = array('data' => $dispNum[67]);
            $mainData[18] = array('data' => $dispNum[81]);
            $mainData[19] = array('data' => $dispNum[89]);

            # 入所 小計
            $cellNum20Data = sprintf(
                '=%s+%s+%s+%s+%s+%s'
              , $this->coord($columnAlphaName[$startColumnNum+14], $rowNum)
              , $this->coord($columnAlphaName[$startColumnNum+15], $rowNum)
              , $this->coord($columnAlphaName[$startColumnNum+16], $rowNum)
              , $this->coord($columnAlphaName[$startColumnNum+17], $rowNum)
              , $this->coord($columnAlphaName[$startColumnNum+18], $rowNum)
              , $this->coord($columnAlphaName[$startColumnNum+19], $rowNum)
            );
            $mainData[20] = array('data' => $cellNum20Data);
            $mainData[21] = array('data' => $dispNum[101]);
            $mainData[22] = array('data' => $dispNum[102]);
            $mainData[23] = array('data' => $dispNum[103]);

            # 在宅復帰率
            $mainData[24] = array(
                'data'  => $this->getExcelDivData($this->coord($columnAlphaName[$startColumnNum+23], $rowNum), $this->coord($columnAlphaName[$startColumnNum+22], $rowNum), 3),
                'style' => array('msoNumberFormat' => $msoNumberFormat_percent0_0)
            );
            $mainData[25] = array('data' => $dispNum[104]);
            $mainData[26] = array('data' => $dispNum[105]);

            # 入所 空床数
            $mainData[27] = array('data' => $this->getExcelSubData((int)$enterLimit, $this->coord($columnAlphaName[$startColumnNum+20], $rowNum), 3));

            # 入所 稼働率
            $mainData[29] = array('data' => $this->getExcelDivData($this->coord($columnAlphaName[$startColumnNum+20], $rowNum), $enterLimit, 3), 'limit' => $enterLimit, 'style' => array('msoNumberFormat' => $msoNumberFormat_percent0_0));
            $mainData[28] = array('data' => $this->getExcelUtilizationRatesTrigona($columnAlphaName[$startColumnNum+29], $rowNum, 95), 'style' => array('textAlign' => $textAlign_center));

            $mainData[30] = array('data' => $dispNum[96]);
            $mainData[31] = array('data' => $dispNum[100]);
            $mainData[32] = array('data' => sprintf('=%s+%s', $this->coord($columnAlphaName[$startColumnNum+30], $rowNum), $this->coord($columnAlphaName[$startColumnNum+31], $rowNum)));
            $mainData[33] = array('data' => $dispNum[106]);
            $mainData[34] = array('data' => $dispNum[107]);
            $mainData[35] = array('data' => $dispNum[108]);

            for ($i=0; $i<=35; $i++) {
                switch ($i) {
                    case '11':
                    case '28':
                        break;

                    case '12':
                    case '29':
                        $leftKey = ($i==12)? 10 : 20;
                        $totalMainData[$i]['limit'] = $totalMainData[$i]['limit'] + $mainData[$i]['limit'];
                        $totalMainData[$i]['data']  = $this->getExcelDivData($this->coord($columnAlphaName[$startColumnNum+$leftKey], $totalRowNum), $totalMainData[$i]['limit'], 3);
                        $totalMainData[$i]['style'] = array('msoNumberFormat' => $msoNumberFormat_percent0_0);
                        $totalMainData[$i-1] = array(
                            'data'  => $this->getExcelUtilizationRatesTrigona($columnAlphaName[$startColumnNum+$i], $totalRowNum, 95),
                            'style' => array('textAlign' => $textAlign_center)
                        );
                        break;

                    case '24':
                        $totalMainData[$i]['data']  = $this->getExcelDivData($this->coord($columnAlphaName[$startColumnNum+23], $totalRowNum), $this->coord($columnAlphaName[$startColumnNum+22], $totalRowNum), 3);
                        $totalMainData[$i]['style'] = array('msoNumberFormat' => $msoNumberFormat_percent0_0);
                        break;

                    default:
                        $totalMainData[$i]['data'] = $this->getExcelSumData($columnAlphaName[$startColumnNum+$i], $startRowNum, $columnAlphaName[$startColumnNum+$i], $totalRowNum-1);
                        break;
                }
            }

            $rtnStr .= '<tr>'
                          .$this->convertCellData($leftSide, $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_blue))
                          .$this->convertCellData($mainData, $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma))
                      .'</tr>';
        }

        $totalLeftSide[0] = array('data' => '合計');

        $rtnStr .= '<tr>'
                      .$this->convertCellData($totalLeftSide, $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_blue))
                      .$this->convertCellData($totalMainData, $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma))
                  .'</tr>';

        $rtnStr .= '</table>';
        return $rtnStr;
    }

    /**
     * 施設別患者月報(詳細版)のエクセル出力
     * @param  array  $dataArr
     * @param  array  $plantArr
     * @param  array  $arr
     * @param  array  $optionArr
     * @return string [HTML table]
     */
    function outputExcelPatientMonReportDetail($dataArr, $plantArr, $arr, $optionArr)
    {
        $rtnStr  = $this->getExcelMeta();
        $fontTag = $this->getFontTag();

        /* @var $indicatorData IndicatorNurseHomeData */
        $indicatorData = $this->getIndicatorNurseHomeData();

        $displayYear  = $this->getArrValueByKey('year',  $optionArr);
        $displayMonth = $this->getArrValueByKey('month', $optionArr);
        $dayCntArr    = $indicatorData->getDayCountOfMonth($displayYear, $displayMonth);

        # attr
        $rowspan_2  = array('name' => 'rowspan', 'value' =>  2);
        $rowspan_3  = array('name' => 'rowspan', 'value' =>  3);
        $rowspan_7  = array('name' => 'rowspan', 'value' =>  7);
        $colspan_2  = array('name' => 'colspan', 'value' =>  2);
        $colspan_4  = array('name' => 'colspan', 'value' =>  4);
        $colspan_8  = array('name' => 'colspan', 'value' =>  8);
        $colspan_9  = array('name' => 'colspan', 'value' =>  9);
        $colspan_14 = array('name' => 'colspan', 'value' => 14);
        $colspan_19 = array('name' => 'colspan', 'value' => 19);

        # style
        $bgColor_blue               = array('name' => 'background-color',  'value' => $this->getRgbColor('blue'));
        $bgColor_weekday            = array('name' => 'background-color',  'value' => $this->getRgbColor('weekday'));
        $bgColor_saturday           = array('name' => 'background-color',  'value' => $this->getRgbColor('saturday'));
        $bgColor_holiday            = array('name' => 'background-color',  'value' => $this->getRgbColor('holiday'));
        $bgColor_lightpink          = array('name' => 'background-color',  'value' => $this->getRgbColor('lightpink'));
        $textAlign_center           = array('name' => 'text-align',        'value' => 'center');
        $textAlign_right            = array('name' => 'text-align',        'value' => 'right');
        $textAlign_left             = array('name' => 'text-align',        'value' => 'left');
        $whiteSpace_nowrap          = array('name' => 'white-space',       'value' => 'nowrap');
        $msoNumberFormat_percent0_0 = $this->getMsoNumberFormat('percent');
        $msoNumberFormat_0_0        = $this->getMsoNumberFormat('decimal');
//        $msoNumberFormat_percent0_0 = array('name' => 'mso-number-format', 'value' => '0.0%');
//        $msoNumberFormat_0_0        = array('name' => 'mso-number-format', 'value' => '0.0');

        $rtnStr .= '<table class="list" border="1" >'
                      .'<tr>'
                          .$this->convertCellData(
                              array(
                                  0 => array('data' => '平日', 'style' => array('bgColor' => $bgColor_weekday, 'textAlign' => $textAlign_left))
                                , 1 => array('data' => $this->getArrValueByKey('weekday', $dayCntArr).'日')
                                , 2 => array('data' => '土曜', 'style' => array('bgColor' => $bgColor_saturday, 'textAlign' => $textAlign_left))
                                , 3 => array('data' => $this->getArrValueByKey('saturday', $dayCntArr).'日')
                                , 4 => array('data' => '休日', 'style' => array('bgColor' => $bgColor_lightpink, 'textAlign' => $textAlign_left))
                                , 5 => array('data' => $this->getArrValueByKey('holiday', $dayCntArr).'日')
                              )
                            , $fontTag
                            , array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
                          )
                      .'</tr>'
                  .'</table>'
                  .'<table class="list" width="100%" border="1" >'
                      .'<tr>'
                      .$this->convertCellData(
                          array(
                              0 => array('data' => '施設名', 'attr' => array($rowspan_3))
                            , 1 => array('data' => '時間', 'attr' => array($rowspan_3))
                            , 2 => array('data' => '通所', 'attr' => array($colspan_14))
                            , 3 => array('data' => '入所', 'attr' => array($colspan_19))
                            , 4 => array('data' => '入所件数', 'attr' => array($rowspan_3))
                            , 5 => array('data' => $this->sc(1).'退所件数', 'attr' => array($rowspan_3))
                            , 6 => array('data' => $this->sc(1)."の内<br />在宅復帰人数", 'attr' => array($rowspan_3))
                            , 7 => array('data' => '在宅復帰率', 'attr' => array($rowspan_3))
                            , 8 => array('data' => "ショート<br />予防ショート<br />入所件数", 'attr' => array($rowspan_3))
                            , 9 => array('data' => "ショート<br />予防ショート<br />退所件数", 'attr' => array($rowspan_3))
                           , 10 => array('data' => "訪問リハビリ・介護予防訪問リハビリ<br />(単位)", 'attr' => array($rowspan_2, $colspan_9))
                           , 11 => array('data' => "判定件数<br />(本入所のみ)", 'attr' => array($rowspan_2, $colspan_2))
                           , 12 => array('data' => "営業件数<br />(訪問件数のみ)", 'attr' => array($rowspan_3))
                          )
                        , $fontTag
                        , array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
                      )
                      .'</tr>'
                      .'<tr>'
                      .$this->convertCellData(
                          array(
                              0 => array('data' => '通所リハビリ', 'attr' => array($colspan_8))
                            , 1 => array('data' => '予防通所リハビリ', 'attr' => array($colspan_4))
                            , 2 => array('data' => '合計', 'attr' => array($rowspan_2))
                            , 3 => array('data' => '稼働率', 'attr' => array($rowspan_2))
                            , 4 => array('data' => '一般／一般ショート・予防ショート', 'attr' => array($colspan_8))
                            , 5 => array('data' => '認知／認知ショート・予防ショート', 'attr' => array($colspan_8))
                            , 6 => array('data' => '入所者計', 'attr' => array($rowspan_2))
                            , 7 => array('data' => '稼働率', 'attr' => array($rowspan_2))
                            , 8 => array('data' => '平均介護度', 'attr' => array($rowspan_2))
                          )
                        , $fontTag
                        , array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
                      )
                      .'</tr>'
                      .'<tr>'
                      .$this->convertCellData(
                          array(
                              0 => array('data' => '要介護5')
                            , 1 => array('data' => '要介護4')
                            , 2 => array('data' => '要介護3')
                            , 3 => array('data' => '要介護2')
                            , 4 => array('data' => '要介護1')
                            , 5 => array('data' => 'その他')
                            , 6 => array('data' => '小計')
                            , 7 => array('data' => '平均介護度')
                            , 8 => array('data' => '要支援2')
                            , 9 => array('data' => '要支援1')
                           , 10 => array('data' => 'その他')
                           , 11 => array('data' => '小計')
                           , 12 => array('data' => '要介護5')
                           , 13 => array('data' => '要介護4')
                           , 14 => array('data' => '要介護3')
                           , 15 => array('data' => '要介護2')
                           , 16 => array('data' => '要介護1')
                           , 17 => array('data' => '要支援2')
                           , 18 => array('data' => '要支援1')
                           , 19 => array('data' => 'その他')
                           , 20 => array('data' => '要介護5')
                           , 21 => array('data' => '要介護4')
                           , 22 => array('data' => '要介護3')
                           , 23 => array('data' => '要介護2')
                           , 24 => array('data' => '要介護1')
                           , 25 => array('data' => '要支援2')
                           , 26 => array('data' => '要支援1')
                           , 27 => array('data' => 'その他')
                           , 28 => array('data' => '要介護5')
                           , 29 => array('data' => '要介護4')
                           , 30 => array('data' => '要介護3')
                           , 31 => array('data' => '要介護2')
                           , 32 => array('data' => '要介護1')
                           , 33 => array('data' => '要支援2')
                           , 34 => array('data' => '要支援1')
                           , 35 => array('data' => 'その他')
                           , 36 => array('data' => '合計')
                           , 37 => array('data' => '可')
                           , 38 => array('data' => '不可')
                          )
                        , $fontTag
                        , array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
                      )
                      .'</tr>';

        $totalData       = array();
        $startRowNum     = 5;
        $startColNum     = 3;
        $columnAlphaName = $this->columnAlphaNameArr;

        foreach ($plantArr as $plantKey => $plantVal) {
            $facilityId   = $this->getArrValueByKey('facility_id', $plantVal);
            $facilityName = $this->getArrValueByKey('facility_name', $plantVal);

            for ($n=1; $n<=108; $n++) { $dispNum[$n] = null; }
            $dailyLimit = null;
            $enterLimit = null;

            foreach ($dataArr as $dataVal) {
                if ($facilityId == $this->getArrValueByKey('jnl_facility_id', $dataVal)) {
                    for ($n=1; $n<=108; $n++) { $dispNum[$n] = $this->getArrValueByKey('dispnum_'.$n, $dataVal); }
                    $dailyLimit = $this->getArrValueByKey('daily_limit', $dataVal);
                    $enterLimit = $this->getArrValueByKey('enter_limit', $dataVal);
                }
            }

            for ($r=0; $r <= 6; $r++) {
                $rowNum = $startRowNum + ($plantKey * 7) + $r;
                $leftSide = array();
                $mainData = array();
                switch ($r) {
                    case '0':
                        $leftSide[] = array('data' => $facilityName, 'attr' => array($rowspan_7));
                        $leftSide[] = array('data' => '2時間まで');

                        # 通所
                        $mainData[0] = array('data' => $dispNum[1]);
                        $mainData[1] = array('data' => $dispNum[2]);
                        $mainData[2] = array('data' => $dispNum[3]);
                        $mainData[3] = array('data' => $dispNum[4]);
                        $mainData[4] = array('data' => $dispNum[5]);
                        $mainData[5] = array('data' => $dispNum[6]);
                        $mainData[6] = array('data' => $dispNum[7]);
                        $rowNum7Data = sprintf(
                            '=IF(%s=0, 0, IF(%s="", "", ROUND(((%s*5+%s*4+%s*3+%s*2+%s*1+%s*1)/%s),1)))'
                          , $this->coord($columnAlphaName[$startColNum+6], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+6], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum],   $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+1], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+2], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+3], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+4], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+5], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+6], $rowNum)
                        );
                        $mainData[7]  = array('data' => $rowNum7Data, 'style' => ($msoNumberFormat_percent0_0));
                        $mainData[8]  = array('data' => $dispNum[50], 'attr' => array($rowspan_7));
                        $mainData[9]  = array('data' => $dispNum[51], 'attr' => array($rowspan_7));
                        $mainData[10] = array('data' => $dispNum[52], 'attr' => array($rowspan_7));
                        $mainData[11] = array('data' => $dispNum[53], 'attr' => array($rowspan_7));

                        $rowNum12Data = sprintf(
                            '=%s+%s+%s+%s+%s+%s+%s+%s'
                          , $this->coord($columnAlphaName[$startColNum+6], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+6], $rowNum+1)
                          , $this->coord($columnAlphaName[$startColNum+6], $rowNum+2)
                          , $this->coord($columnAlphaName[$startColNum+6], $rowNum+3)
                          , $this->coord($columnAlphaName[$startColNum+6], $rowNum+4)
                          , $this->coord($columnAlphaName[$startColNum+6], $rowNum+5)
                          , $this->coord($columnAlphaName[$startColNum+6], $rowNum+6)
                          , $this->coord($columnAlphaName[$startColNum+11], $rowNum)
                        );
                        $mainData[12] = array('data' => $rowNum12Data, 'attr' => array($rowspan_7));

                        $rowNum13Data = $this->getExcelDivData($this->coord($columnAlphaName[$startColNum+12], $rowNum), $dailyLimit);
                        $mainData[13] = array('data' => $rowNum13Data, 'attr' => array($rowspan_7), 'style' => array($msoNumberFormat_percent0_0));

                        # 入所
                        $mainData[14] = array('data' => $dispNum[54], 'attr' => array($rowspan_3));
                        $mainData[15] = array('data' => $dispNum[55], 'attr' => array($rowspan_3));
                        $mainData[16] = array('data' => $dispNum[56], 'attr' => array($rowspan_3));
                        $mainData[17] = array('data' => $dispNum[57], 'attr' => array($rowspan_3));
                        $mainData[18] = array('data' => $dispNum[58], 'attr' => array($rowspan_3));
                        $mainData[19] = array('data' => '&nbsp;', 'attr' => array($rowspan_3));
                        $mainData[20] = array('data' => '&nbsp;', 'attr' => array($rowspan_3));
                        $mainData[21] = array('data' => $dispNum[59], 'attr' => array($rowspan_3));

                        $mainData[22] = array('data' => $dispNum[61], 'attr' => array($rowspan_3));
                        $mainData[23] = array('data' => $dispNum[62], 'attr' => array($rowspan_3));
                        $mainData[24] = array('data' => $dispNum[63], 'attr' => array($rowspan_3));
                        $mainData[25] = array('data' => $dispNum[64], 'attr' => array($rowspan_3));
                        $mainData[26] = array('data' => $dispNum[65], 'attr' => array($rowspan_3));
                        $mainData[27] = array('data' => '&nbsp;', 'attr' => array($rowspan_3));
                        $mainData[28] = array('data' => '&nbsp;', 'attr' => array($rowspan_3));
                        $mainData[29] = array('data' => $dispNum[66], 'attr' => array($rowspan_3));

                        $rowNum30Data = $dispNum[60] + $dispNum[74] + $dispNum[85] + $dispNum[67] + $dispNum[81] + $dispNum[89];
                        $mainData[30] = array('data' => '='.$rowNum30Data, 'attr' => array($rowspan_7));
                        $rowNum31Data = $this->getExcelDivData($rowNum30Data, $enterLimit);
                        $mainData[31] = array('data' => $rowNum31Data, 'attr' => array($rowspan_7), 'style' => array($msoNumberFormat_percent0_0));

                        # 入所 平均介護度
                        $rowNum32Data = sprintf(
                            '=IF((%s+%s+%s+%s)=0, 0,ROUND((%s*5+%s*4+%s*3+%s*2+%s*1+%s*1+%s*5+%s*4+%s*3+%s*2+%s*1+%s*1+%s*1+%s*5+%s*4+%s*3+%s*2+%s*1+%s*1+%s*5+%s*4+%s*3+%s*2+%s*1+%s*1+%s*1+%s*1)/(%s+%s+%s+%s),1))'
                          , $this->coord($columnAlphaName[$startColNum+14], $rowNum+5) # 60    Q
                          , $this->coord($columnAlphaName[$startColNum+18], $rowNum+5) # 74+85 U
                          , $this->coord($columnAlphaName[$startColNum+22], $rowNum+5) # 67    Y
                          , $this->coord($columnAlphaName[$startColNum+26], $rowNum+5) # 81+89 AC
                          , $this->coord($columnAlphaName[$startColNum+14], $rowNum)   # 54    Q
                          , $this->coord($columnAlphaName[$startColNum+15], $rowNum)   # 55    R
                          , $this->coord($columnAlphaName[$startColNum+16], $rowNum)   # 56    S
                          , $this->coord($columnAlphaName[$startColNum+17], $rowNum)   # 57    T
                          , $this->coord($columnAlphaName[$startColNum+18], $rowNum)   # 58    U
                          , $this->coord($columnAlphaName[$startColNum+21], $rowNum)   # 59    X
                          , $this->coord($columnAlphaName[$startColNum+14], $rowNum+3) # 68    Q
                          , $this->coord($columnAlphaName[$startColNum+15], $rowNum+3) # 69    R
                          , $this->coord($columnAlphaName[$startColNum+16], $rowNum+3) # 70    S
                          , $this->coord($columnAlphaName[$startColNum+17], $rowNum+3) # 71    T
                          , $this->coord($columnAlphaName[$startColNum+19], $rowNum+3) # 82    V
                          , $this->coord($columnAlphaName[$startColNum+20], $rowNum+3) # 83    W
                          , $this->coord($columnAlphaName[$startColNum+21], $rowNum+3) # 73+84 X
                          , $this->coord($columnAlphaName[$startColNum+22], $rowNum)   # 61    Y
                          , $this->coord($columnAlphaName[$startColNum+23], $rowNum)   # 62    Z
                          , $this->coord($columnAlphaName[$startColNum+24], $rowNum)   # 63    AA
                          , $this->coord($columnAlphaName[$startColNum+25], $rowNum)   # 64    AB
                          , $this->coord($columnAlphaName[$startColNum+26], $rowNum)   # 65    AC
                          , $this->coord($columnAlphaName[$startColNum+29], $rowNum)   # 66    AF
                          , $this->coord($columnAlphaName[$startColNum+22], $rowNum+3) # 75    Y
                          , $this->coord($columnAlphaName[$startColNum+23], $rowNum+3) # 76    Z
                          , $this->coord($columnAlphaName[$startColNum+24], $rowNum+3) # 77    AA
                          , $this->coord($columnAlphaName[$startColNum+25], $rowNum+3) # 78    AB
                          , $this->coord($columnAlphaName[$startColNum+26], $rowNum+3) # 79    AC
                          , $this->coord($columnAlphaName[$startColNum+27], $rowNum+3) # 86    AD
                          , $this->coord($columnAlphaName[$startColNum+28], $rowNum+3) # 87    AE
                          , $this->coord($columnAlphaName[$startColNum+29], $rowNum+3) # 80+88 AF
                          , $this->coord($columnAlphaName[$startColNum+14], $rowNum+5) # 60    Q
                          , $this->coord($columnAlphaName[$startColNum+18], $rowNum+5) # 74+85 U
                          , $this->coord($columnAlphaName[$startColNum+22], $rowNum+5) # 67    Y
                          , $this->coord($columnAlphaName[$startColNum+26], $rowNum+5) # 81+89 AC
                        );
                        $mainData[32] = array('data' => $rowNum32Data, 'attr' => array($rowspan_7));

                        $mainData[33] = array('data' => $dispNum[101], 'attr' => array($rowspan_7));
                        $mainData[34] = array('data' => $dispNum[102], 'attr' => array($rowspan_7));
                        $mainData[35] = array('data' => $dispNum[103], 'attr' => array($rowspan_7));

                        # 在宅復帰率
                        $rowNum36Data = $this->getExcelDivData((int)$dispNum[103], (int)$dispNum[102]);
                        $mainData[36] = array('data' => $rowNum36Data, 'attr' => array($rowspan_7), 'style' => array($msoNumberFormat_percent0_0));

                        $mainData[37] = array('data' => $dispNum[104], 'attr' => array($rowspan_7));
                        $mainData[38] = array('data' => $dispNum[105], 'attr' => array($rowspan_7));

                        $mainData[39] = array('data' => $dispNum[90], 'attr' => array($rowspan_7));
                        $mainData[40] = array('data' => $dispNum[91], 'attr' => array($rowspan_7));
                        $mainData[41] = array('data' => $dispNum[92], 'attr' => array($rowspan_7));
                        $mainData[42] = array('data' => $dispNum[93], 'attr' => array($rowspan_7));
                        $mainData[43] = array('data' => $dispNum[94], 'attr' => array($rowspan_7));
                        $mainData[44] = array('data' => $dispNum[97], 'attr' => array($rowspan_7));
                        $mainData[45] = array('data' => $dispNum[98], 'attr' => array($rowspan_7));

                        $rowNum46Data = $dispNum[95] + $dispNum[99];
                        $mainData[46] = array('data' => $rowNum46Data, 'attr' => array($rowspan_7));
                        $rowNum47Data = $dispNum[96] + $dispNum[100];
                        $mainData[47] = array('data' => $rowNum47Data, 'attr' => array($rowspan_7));

                        $mainData[48] = array('data' => $dispNum[106], 'attr' => array($rowspan_7));
                        $mainData[49] = array('data' => $dispNum[107], 'attr' => array($rowspan_7));
                        $mainData[50] = array('data' => $dispNum[108], 'attr' => array($rowspan_7));
                        break;

                    case '1':
                        $leftSide[] = array('data' => '3時間まで');
                        $mainData[0] = array('data' => $dispNum[8]);
                        $mainData[1] = array('data' => $dispNum[9]);
                        $mainData[2] = array('data' => $dispNum[10]);
                        $mainData[3] = array('data' => $dispNum[11]);
                        $mainData[4] = array('data' => $dispNum[12]);
                        $mainData[5] = array('data' => $dispNum[13]);
                        $mainData[6] = array('data' => $dispNum[14]);
                        $rowNum7Data = sprintf(
                            '=IF(%s=0, 0, IF(%s="", "", ROUND(((%s*5+%s*4+%s*3+%s*2+%s*1+%s*1)/%s),1)))'
                          , $this->coord($columnAlphaName[$startColNum+6], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+6], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+1], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+2], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+3], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+4], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+5], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+6], $rowNum)
                        );
                        $mainData[7] = array('data' => $rowNum7Data);
                        break;

                    case '2':
                        $leftSide[] = array('data' => '4時間まで');
                        $mainData[0] = array('data' => $dispNum[15]);
                        $mainData[1] = array('data' => $dispNum[16]);
                        $mainData[2] = array('data' => $dispNum[17]);
                        $mainData[3] = array('data' => $dispNum[18]);
                        $mainData[4] = array('data' => $dispNum[19]);
                        $mainData[5] = array('data' => $dispNum[20]);
                        $mainData[6] = array('data' => $dispNum[21]);
                        $rowNum7Data = sprintf(
                            '=IF(%s=0, 0, IF(%s="", "", ROUND(((%s*5+%s*4+%s*3+%s*2+%s*1+%s*1)/%s),1)))'
                          , $this->coord($columnAlphaName[$startColNum+6], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+6], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+1], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+2], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+3], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+4], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+5], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+6], $rowNum)
                        );
                        $mainData[7] = array('data' => $rowNum7Data, 'style' => ($msoNumberFormat_percent0_0));
                        break;

                    case '3':
                        $leftSide[] = array('data' => '6時間まで');
                        # 通所
                        $mainData[0] = array('data' => $dispNum[22]);
                        $mainData[1] = array('data' => $dispNum[23]);
                        $mainData[2] = array('data' => $dispNum[24]);
                        $mainData[3] = array('data' => $dispNum[25]);
                        $mainData[4] = array('data' => $dispNum[26]);
                        $mainData[5] = array('data' => $dispNum[27]);
                        $mainData[6] = array('data' => $dispNum[28]);
                        $rowNum7Data = sprintf(
                            '=IF(%s=0, 0, IF(%s="", "", ROUND(((%s*5+%s*4+%s*3+%s*2+%s*1+%s*1)/%s),1)))'
                          , $this->coord($columnAlphaName[$startColNum+6], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+6], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+1], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+2], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+3], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+4], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+5], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+6], $rowNum)
                        );
                        $mainData[7] = array('data' => $rowNum7Data, 'style' => ($msoNumberFormat_percent0_0));

                        # 入所
                        $mainData[8] = array('data' => $dispNum[68], 'attr' => array($rowspan_2));
                        $mainData[9] = array('data' => $dispNum[69], 'attr' => array($rowspan_2));
                        $mainData[10] = array('data' => $dispNum[70], 'attr' => array($rowspan_2));
                        $mainData[11] = array('data' => $dispNum[71], 'attr' => array($rowspan_2));
                        $mainData[12] = array('data' => $dispNum[72], 'attr' => array($rowspan_2));
                        $mainData[13] = array('data' => $dispNum[82], 'attr' => array($rowspan_2));
                        $mainData[14] = array('data' => $dispNum[83], 'attr' => array($rowspan_2));
                        //$rowNum15Data = sprintf('=IF(%s+%s=0, "", %s+%s)', (int)$dispNum[73], (int)$dispNum[84], (int)$dispNum[73], (int)$dispNum[84]);
						$rowNum15Data = sprintf('=IF(%s+%s=0, 0, %s+%s)', (int)$dispNum[73], (int)$dispNum[84], (int)$dispNum[73], (int)$dispNum[84]);
						$mainData[15] = array('data' => $rowNum15Data, 'attr' => array($rowspan_2));
                        $mainData[16] = array('data' => $dispNum[75], 'attr' => array($rowspan_2));
                        $mainData[17] = array('data' => $dispNum[76], 'attr' => array($rowspan_2));
                        $mainData[18] = array('data' => $dispNum[77], 'attr' => array($rowspan_2));
                        $mainData[19] = array('data' => $dispNum[78], 'attr' => array($rowspan_2));
                        $mainData[20] = array('data' => $dispNum[79], 'attr' => array($rowspan_2));
                        $mainData[21] = array('data' => $dispNum[86], 'attr' => array($rowspan_2));
                        $mainData[22] = array('data' => $dispNum[87], 'attr' => array($rowspan_2));
                        $rowNum23Data = $dispNum[80] + $dispNum[88];
                        $mainData[23] = array('data' => $rowNum23Data, 'attr' => array($rowspan_2));
                        break;

                    case '4':
                        $leftSide[] = array('data' => '8時間まで');
                        $mainData[0] = array('data' => $dispNum[29]);
                        $mainData[1] = array('data' => $dispNum[30]);
                        $mainData[2] = array('data' => $dispNum[31]);
                        $mainData[3] = array('data' => $dispNum[32]);
                        $mainData[4] = array('data' => $dispNum[33]);
                        $mainData[5] = array('data' => $dispNum[34]);
                        $mainData[6] = array('data' => $dispNum[35]);
                        $rowNum7Data = sprintf(
                            '=IF(%s=0, 0, IF(%s="", "", ROUND(((%s*5+%s*4+%s*3+%s*2+%s*1+%s*1)/%s),1)))'
                          , $this->coord($columnAlphaName[$startColNum+6], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+6], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+1], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+2], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+3], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+4], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+5], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+6], $rowNum)
                        );
                        $mainData[7] = array('data' => $rowNum7Data, 'style' => ($msoNumberFormat_percent0_0));
                        break;

                    case '5':
                        $leftSide[] = array('data' => '9時間まで');
                        # 通所
                        $mainData[0] = array('data' => $dispNum[36]);
                        $mainData[1] = array('data' => $dispNum[37]);
                        $mainData[2] = array('data' => $dispNum[38]);
                        $mainData[3] = array('data' => $dispNum[39]);
                        $mainData[4] = array('data' => $dispNum[40]);
                        $mainData[5] = array('data' => $dispNum[41]);
                        $mainData[6] = array('data' => $dispNum[42]);
                        $rowNum7Data = sprintf(
                            '=IF(%s=0, 0, IF(%s="", "", ROUND(((%s*5+%s*4+%s*3+%s*2+%s*1+%s*1)/%s),1)))'
                          , $this->coord($columnAlphaName[$startColNum+6], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+6], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+1], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+2], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+3], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+4], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+5], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+6], $rowNum)
                        );
                        $mainData[7] = array('data' => $rowNum7Data, 'style' => ($msoNumberFormat_percent0_0));

                        # 入所
                        $mainData[8] = array(
                            'data'  => (int)$dispNum[60]
                          , 'attr'  => array($rowspan_2, $colspan_4)
                          , 'style' => array(
                                'textAlign' => $textAlign_center
                              , 'msoNumberFormat' => array('name' => 'mso-number-format', 'value' => '\0022一般入所\0022\#\,\#\#0\0022名\0022')
                            )
                        );
                        $rowNum9Data = $dispNum[74] + $dispNum[85];
                        $mainData[9] = array(
                            'data' => $rowNum9Data
                          , 'attr' => array($rowspan_2, $colspan_4)
                          , 'style' => array(
                                'textAlign' => $textAlign_center
                              , 'msoNumberFormat' => array('name' => 'mso-number-format', 'value' => '\0022一般ショート・予防ショート\0022\#\,\#\#0\0022名\0022')
                            )
                        );
                        $mainData[10] = array(
                            'data' => (int)$dispNum[67]
                          , 'attr' => array($rowspan_2, $colspan_4)
                          , 'style' => array(
                                'textAlign' => $textAlign_center
                              , 'msoNumberFormat' => array('name' => 'mso-number-format', 'value' => '\0022認知入所\0022\#\,\#\#0\0022名\0022'
                          )
                        )
                        );
                        $rowNum11Data = $dispNum[81]+$dispNum[89];
                        $mainData[11] = array(
                            'data' => $rowNum11Data
                          , 'attr' => array($rowspan_2, $colspan_4)
                          , 'style' => array(
                                'textAlign'       => $textAlign_center
                              , 'msoNumberFormat' => array('name' => 'mso-number-format', 'value' => '\0022認知ショート・予防ショート\0022\#\,\#\#0\0022名\0022')
                          )
                        );
                        break;

                    case '6':
                        $leftSide[] = array('data' => '10時間まで');
                        $mainData[0] = array('data' => $dispNum[43]);
                        $mainData[1] = array('data' => $dispNum[44]);
                        $mainData[2] = array('data' => $dispNum[45]);
                        $mainData[3] = array('data' => $dispNum[46]);
                        $mainData[4] = array('data' => $dispNum[47]);
                        $mainData[5] = array('data' => $dispNum[48]);
                        $mainData[6] = array('data' => $dispNum[49]);
                        $rowNum7Data = sprintf(
                            '=IF(%s=0, 0, IF(%s="", "", ROUND(((%s*5+%s*4+%s*3+%s*2+%s*1+%s*1)/%s),1)))'
                          , $this->coord($columnAlphaName[$startColNum+6], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+6], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum],   $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+1], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+2], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+3], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+4], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+5], $rowNum)
                          , $this->coord($columnAlphaName[$startColNum+6], $rowNum)
                        );
                        $mainData[7] = array('data' => $rowNum7Data, 'style' => ($msoNumberFormat_percent0_0));
                        break;
                }

                $rtnStr .= '<tr>'
                              .$this->convertCellData($leftSide, $fontTag, array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                              .$this->convertCellData($mainData, $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_0_0))
                          .'</tr>';
            }
        }
        $rtnStr .= '</table>';
        return $rtnStr;
    }

    /**
     * 施設別患者月報(前年比較)のエクセル出力
     * @param  array  $dataArr
     * @param  array  $plantArr
     * @param  array  $arr
     * @param  array  $optionArr
     * @return string [html table tag]
     */
    function outputExcelPatientMonReportCmp($dataArr, $plantArr, $arr, $optionArr)
    {
        $rtnStr  = $this->getExcelMeta();
        $fontTag = $this->getFontTag();

        $displayCurrentYear = $this->getArrValueByKey('year', $optionArr);
        $displayLastYear    = $displayCurrentYear - 1;
        $displayMonth       = $this->getArrValueByKey('month', $optionArr);

        $dataObj             = $this->getIndicatorNurseHomeData();
        $currentYearDayCount = $dataObj->getDayCountOfMonth($displayCurrentYear, $displayMonth);
        $lastYearDayCount    = $dataObj->getDayCountOfMonth($displayLastYear, $displayMonth);

        # attr
        $rowspan_2  = array('name' => 'rowspan', 'value' =>  2);
        $rowspan_3  = array('name' => 'rowspan', 'value' =>  3);
        $rowspan_4  = array('name' => 'rowspan', 'value' =>  4);
        $colspan_2  = array('name' => 'colspan', 'value' =>  2);
        $colspan_3  = array('name' => 'colspan', 'value' =>  3);
        $colspan_8  = array('name' => 'colspan', 'value' =>  8);
        $colspan_12 = array('name' => 'colspan', 'value' => 12);
        $colspan_17 = array('name' => 'colspan', 'value' => 17);

        # style
        $bgColor_blue      = array('name' => 'background-color', 'value' => $this->getRgbColor('blue'));
        $bgColor_green     = array('name' => 'background-color', 'value' => $this->getRgbColor('green'));
        $bgColor_weekday   = array('name' => 'background-color', 'value' => $this->getRgbColor('weekday'));
        $bgColor_saturday  = array('name' => 'background-color', 'value' => $this->getRgbColor('saturday'));
        $bgColor_holiday   = array('name' => 'background-color', 'value' => $this->getRgbColor('lightpink'));
        $textAlign_center  = array('name' => 'text-align',       'value' => 'center');
        $textAlign_right   = array('name' => 'text-align',       'value' => 'right');
        $textAlign_left    = array('name' => 'text-align',       'value' => 'left');
        $whiteSpace_nowrap = array('name' => 'white-space',      'value' => 'nowrap');
        $msoNumberFormat_percent0_0 = $this->getMsoNumberFormat('percent');
        $msoNumberFormat_comma      = $this->getMsoNumberFormat('comma');
        $msoNumberFormat_integer    = array('name' => 'mso-number-format', 'value' => '0');

        $header .= '<table border="1" width="100%" >'
                      .'<tr>'
                          .'<td style="text-align: left;">'
                              .'<table class="list" width="25%" border="1" >'
                                  .'<tr>'
                                      .$this->convertCellData(
                                          array(
                                              0 => array('data' => sprintf('%04d年', $displayCurrentYear))
                                            , 1 => array('data' => '平日', 'style' => array('bgColor' => $bgColor_weekday))
                                            , 2 => array('data' => $this->getArrValueByKey('weekday', $currentYearDayCount).'日', 'style' =>  array('textAlign'  => $textAlign_center))
                                            , 3 => array('data' => '土曜', 'style' => array('bgColor' => $bgColor_saturday))
                                            , 4 => array('data' => $this->getArrValueByKey('saturday', $currentYearDayCount).'日', 'style' =>  array('textAlign'  => $textAlign_center))
                                            , 5 => array('data' => '休日', 'style' => array('bgColor' => $bgColor_holiday))
                                            , 6 => array('data' => $this->getArrValueByKey('holiday', $currentYearDayCount).'日', 'style' =>  array('textAlign'  => $textAlign_center))
                                          )
                                        , $fontTag
                                        , array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_left)
                                      )
                                  .'</tr>'
                                  .'<tr>'
                                      .$this->convertCellData(
                                          array(
                                              0 => array('data' => sprintf('%04d年', $displayLastYear))
                                            , 1 => array('data' => '平日', 'style' => array('bgColor' => $bgColor_weekday))
                                            , 2 => array('data' => $this->getArrValueByKey('weekday', $lastYearDayCount).'日', 'style' =>  array('textAlign'  => $textAlign_center))
                                            , 3 => array('data' => '土曜', 'style' => array('bgColor' => $bgColor_saturday))
                                            , 4 => array('data' => $this->getArrValueByKey('saturday', $lastYearDayCount).'日', 'style' =>  array('textAlign'  => $textAlign_center))
                                            , 5 => array('data' => '休日', 'style' => array('bgColor' => $bgColor_holiday))
                                            , 6 => array('data' => $this->getArrValueByKey('holiday', $lastYearDayCount).'日', 'style' =>  array('textAlign'  => $textAlign_center))
                                          )
                                        , $fontTag
                                        , array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_left)
                                      )
                                  .'</tr>'
                              .'</table>'
                          .'</td>'
                          .'<td style="text-align: right;">'
                              .sprintf($fontTag, $displayCurrentYear.'年'.$displayMonth.'月')
                          .'</td>'
                      .'</tr>'
                  .'</table>'
                  .'<table class="list" width="100%" border="1" >'
                      .'<tr>'
                          .$this->convertCellData(
                              array(
                                  0 => array('data' => '施設名', 'attr' => array($rowspan_3))
                                , 1 => array('data' => '年', 'attr' => array($rowspan_3))
                                , 2 => array('data' => '通所', 'attr' => array($colspan_12))
                                , 3 => array('data' => '入所', 'attr' => array($colspan_17))
                                , 4 => array('data' => "訪問リハビリ<br />(単位)", 'attr' => array($rowspan_2, $colspan_3))
                                , 5 => array('data' => "判定件数<br />(本入所のみ)", 'attr' => array($rowspan_2, $colspan_2))
                                , 6 => array('data' => "営業件数<br />(訪問件数のみ)", 'attr' => array($rowspan_3))
                              )
                            , $fontTag
                            , array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
                          )
                      .'</tr>'
                      .'<tr>'
                          .$this->convertCellData(
                              array(
                                  # 通所
                                  0 => array('data' => '定員数', 'attr' => array($rowspan_2))
                                , 1 => array('data' => '通所リハビリ', 'attr' => array($colspan_8))
                                , 2 => array('data' => '予防通所リハビリ', 'attr' => array($rowspan_2))
                                , 3 => array('data' => '合計', 'attr' => array($rowspan_2))
                                , 4 => array('data' => '稼働率', 'attr' => array($rowspan_2))
                                  # 入所
                                , 5 => array('data' => '定員数', 'attr' => array($rowspan_2))
                                , 6 => array('data' => '一般', 'attr' => array($rowspan_2))
                                , 7 => array('data' => "一般<br />ショートステイ", 'attr' => array($rowspan_2))
                                , 8 => array('data' => "一般<br />予防ショート", 'attr' => array($rowspan_2))
                                , 9 => array('data' => '認知', 'attr' => array($rowspan_2))
                               , 10 => array('data' => "認知<br />ショートステイ", 'attr' => array($rowspan_2))
                               , 11 => array('data' => "認知<br />予防ショート", 'attr' => array($rowspan_2))
                               , 12 => array('data' => '小計', 'attr' => array($rowspan_2))
                               , 13 => array('data' => '入所数', 'attr' => array($rowspan_2))
                               , 14 => array('data' => $this->sc(1).'退所数', 'attr' => array($rowspan_2))
                               , 15 => array('data' => $this->sc(1)."の内、<br />在宅復帰人数", 'attr' => array($rowspan_2))
                               , 16 => array('data' => '在宅復帰率', 'attr' => array($rowspan_2))
                               , 17 => array('data' => "ショート<br />予防ショート<br />入所率", 'attr' => array($rowspan_2))
                               , 18 => array('data' => "ショート<br />予防ショート<br />退所率", 'attr' => array($rowspan_2))
                               , 19 => array('data' => '空床数', 'attr' => array($rowspan_2))
                               , 20 => array('data' => '稼働率', 'attr' => array($rowspan_2, $colspan_2))
                              )
                            , $fontTag
                            , array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
                          )
                      .'</tr>'
                      .'<tr>'
                          .$this->convertCellData(
                              array(
                                  # 通所リハビリ
                                   0 => array('data' => "1時間以上<br />2時間未満")
                                 , 1 => array('data' => "2時間以上<br />3時間未満")
                                 , 2 => array('data' => "3時間以上<br />4時間未満")
                                 , 3 => array('data' => "4時間以上<br />6時間未満")
                                 , 4 => array('data' => "6時間以上<br />8時間未満")
                                 , 5 => array('data' => "8時間以上<br />9時間未満")
                                 , 6 => array('data' => "9時間以上<br />10時間未満")
                                 , 7 => array('data' => '小計')
                                  # 訪問リハビリ
                                 , 8 => array('data' => '訪問リハビリ')
                                 , 9 => array('data' => "介護予防<br />訪問リハビリ")
                                , 10 => array('data' => '合計')
                                  # 判定件数
                                , 11 => array('data' => '可')
                                , 12 => array('data' => '不可')
                              )
                            , $fontTag
                            , array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
                          )
                      .'</tr>';

        $rtnStr     .= $header;
        $currentData = $this->getArrValueByKey('current', $dataArr);
        $lastData    = $this->getArrValueByKey('last',    $dataArr);

        $totalCurrentRow = array();
        $totalLastRow    = array();
        $totalCompRow    = array();
        $totalRaitoRow   = array();

        $startRowNum     = 8;
        $startColumnNum  = 3;
        $totalRowNum     = $startRowNum + (count($plantArr) * 4);
        $colmunAlphaName = $this->columnAlphaNameArr;

        if (is_array($plantArr)) {
            foreach ($plantArr as $plantKey => $plantVal) {
                $rowNum = $startRowNum + ($plantKey * 4);
                $facilityId = $this->getArrValueByKey('facility_id', $plantVal);
                for ($c=1; $c<=108; $c++) {
                    $cDispNum[$c] = null;
                    $lDispNum[$c] = null;
                }
                $cDailyLimit = null;
                $lDailyLimit = null;
                $cEnterLimit = null;
                $lEnterLimit = null;
                $cStatus     = null;
                $lStatus     = null;

                foreach ($currentData as $currentVal) {
                    if ($facilityId == $this->getArrValueByKey('jnl_facility_id', $currentVal) && $displayCurrentYear == $this->getArrValueByKey('year', $currentVal) && $displayMonth == $this->getArrValueByKey('month', $currentVal)) {
                        for ($c=1; $c<=108; $c++) { $cDispNum[$c] = $this->getArrValueByKey('dispnum_'.$c, $currentVal); }
                        $cDailyLimit = $this->getArrValueByKey('daily_limit', $currentVal);
                        $cEnterLimit = $this->getArrValueByKey('enter_limit', $currentVal);
                    }
                }
                foreach ($lastData as $lastVal) {
                    if ($facilityId == $this->getArrValueByKey('jnl_facility_id', $lastVal) && $displayLastYear == $this->getArrValueByKey('year', $lastVal) && $displayMonth == $this->getArrValueByKey('month', $currentVal)) {
                        for ($c=1; $c<=108; $c++) { $lDispNum[$c] = $this->getArrValueByKey('dispnum_'.$c, $lastVal); }
                        $lDailyLimit = $this->getArrValueByKey('daily_limit', $lastVal);
                        $lEnterLimit = $this->getArrValueByKey('enter_limit', $lastVal);
                    }
                }

                # 通所
                $currentRow[0] = $cDailyLimit;
                $currentRow[1] = $cDispNum[7];
                $currentRow[2] = $cDispNum[14];
                $currentRow[3] = $cDispNum[21];
                $currentRow[4] = $cDispNum[28];
                $currentRow[5] = $cDispNum[35];
                $currentRow[6] = $cDispNum[42];
                $currentRow[7] = $cDispNum[49];
                $currentRow[8] = sprintf(
                    '=%s+%s+%s+%s+%s+%s+%s',
                    $this->coord($colmunAlphaName[$startColumnNum+1], $rowNum),
                    $this->coord($colmunAlphaName[$startColumnNum+2], $rowNum),
                    $this->coord($colmunAlphaName[$startColumnNum+3], $rowNum),
                    $this->coord($colmunAlphaName[$startColumnNum+4], $rowNum),
                    $this->coord($colmunAlphaName[$startColumnNum+5], $rowNum),
                    $this->coord($colmunAlphaName[$startColumnNum+6], $rowNum),
                    $this->coord($colmunAlphaName[$startColumnNum+7], $rowNum)
                );
                $currentRow[9] = $cDispNum[53];
                $currentRow[10] = sprintf('=%s+%s', $this->coord($colmunAlphaName[$startColumnNum+8], $rowNum), $this->coord($colmunAlphaName[$startColumnNum+9], $rowNum));
                $currentRow[11] = $this->getExcelDivData($this->coord($colmunAlphaName[$startColumnNum+10], $rowNum), $this->coord($colmunAlphaName[$startColumnNum+0], $rowNum));

                # 入所
                $currentRow[12] = $cEnterLimit;
                $currentRow[13] = $cDispNum[60];
                $currentRow[14] = $cDispNum[74];
                $currentRow[15] = $cDispNum[85];
                $currentRow[16] = $cDispNum[67];
                $currentRow[17] = $cDispNum[81];
                $currentRow[18] = $cDispNum[89];
                $currentRow[19] = sprintf(
                    '=%s+%s+%s+%s+%s+%s',
                    $this->coord($colmunAlphaName[$startColumnNum+13], $rowNum),
                    $this->coord($colmunAlphaName[$startColumnNum+14], $rowNum),
                    $this->coord($colmunAlphaName[$startColumnNum+15], $rowNum),
                    $this->coord($colmunAlphaName[$startColumnNum+16], $rowNum),
                    $this->coord($colmunAlphaName[$startColumnNum+17], $rowNum),
                    $this->coord($colmunAlphaName[$startColumnNum+18], $rowNum)
                );
                $currentRow[20] = $cDispNum[101];
                $currentRow[21] = $cDispNum[102];
                $currentRow[22] = $cDispNum[103];
                $currentRow[23] = $this->getExcelDivData($this->coord($colmunAlphaName[$startColumnNum+22], $rowNum), $this->coord($colmunAlphaName[$startColumnNum+21], $rowNum));
                $currentRow[24] = $cDispNum[104];
                $currentRow[25] = $cDispNum[105];
                $currentRow[26] = $this->getExcelSubData($this->coord($colmunAlphaName[$startColumnNum+12], $rowNum), $this->coord($colmunAlphaName[$startColumnNum+19], $rowNum));
                $currentRow[28] = $this->getExcelDivData($this->coord($colmunAlphaName[$startColumnNum+19], $rowNum), $this->coord($colmunAlphaName[$startColumnNum+12], $rowNum));
                $currentRow[27] = $this->getExcelUtilizationRatesTrigona($colmunAlphaName[$startColumnNum+28], $rowNum, 95);

                # 訪問リハビリ
                $currentRow[29] = $cDispNum[96];
                $currentRow[30] = $cDispNum[100];
                $currentRow[31] = sprintf('=%s+%s', $this->coord($colmunAlphaName[$startColumnNum+29], $rowNum), $this->coord($colmunAlphaName[$startColumnNum+30], $rowNum));

                # 判定件数
                $currentRow[32] = $cDispNum[106];
                $currentRow[33] = $cDispNum[107];

                # 営業件数
                $currentRow[34] = $cDispNum[108];

                # 通所
                $lastRow[0] = $lDailyLimit;
                $lastRow[1] = $lDispNum[7];
                $lastRow[2] = $lDispNum[14];
                $lastRow[3] = $lDispNum[21];
                $lastRow[4] = $lDispNum[28];
                $lastRow[5] = $lDispNum[35];
                $lastRow[6] = $lDispNum[42];
                $lastRow[7] = $lDispNum[49];
                $lastRow[8] = sprintf(
                    '=%s+%s+%s+%s+%s+%s+%s',
                    $this->coord($colmunAlphaName[$startColumnNum+1], $rowNum+1),
                    $this->coord($colmunAlphaName[$startColumnNum+2], $rowNum+1),
                    $this->coord($colmunAlphaName[$startColumnNum+3], $rowNum+1),
                    $this->coord($colmunAlphaName[$startColumnNum+4], $rowNum+1),
                    $this->coord($colmunAlphaName[$startColumnNum+5], $rowNum+1),
                    $this->coord($colmunAlphaName[$startColumnNum+6], $rowNum+1),
                    $this->coord($colmunAlphaName[$startColumnNum+7], $rowNum+1)
                );
                $lastRow[9] = $lDispNum[53];
                $lastRow[10] = sprintf('=%s+%s', $this->coord($colmunAlphaName[$startColumnNum+8], $rowNum+1), $this->coord($colmunAlphaName[$startColumnNum+9], $rowNum+1));
                $lastRow[11] = $this->getExcelDivData($this->coord($colmunAlphaName[$startColumnNum+10], $rowNum+1), $this->coord($colmunAlphaName[$startColumnNum+0], $rowNum+1));

                # 入所
                $lastRow[12] = $lEnterLimit;
                $lastRow[13] = $lDispNum[60];
                $lastRow[14] = $lDispNum[74];
                $lastRow[15] = $lDispNum[85];
                $lastRow[16] = $lDispNum[67];
                $lastRow[17] = $lDispNum[81];
                $lastRow[18] = $lDispNum[89];

                $lastRow[19] = sprintf(
                    '=%s+%s+%s+%s+%s+%s',
                    $this->coord($colmunAlphaName[$startColumnNum+13], $rowNum+1),
                    $this->coord($colmunAlphaName[$startColumnNum+14], $rowNum+1),
                    $this->coord($colmunAlphaName[$startColumnNum+15], $rowNum+1),
                    $this->coord($colmunAlphaName[$startColumnNum+16], $rowNum+1),
                    $this->coord($colmunAlphaName[$startColumnNum+17], $rowNum+1),
                    $this->coord($colmunAlphaName[$startColumnNum+18], $rowNum+1)
                );
                $lastRow[20] = $lDispNum[101];
                $lastRow[21] = $lDispNum[102];
                $lastRow[22] = $lDispNum[103];
                $lastRow[23] = $this->getExcelDivData($this->coord($colmunAlphaName[$startColumnNum+22], $rowNum+1), $this->coord($colmunAlphaName[$startColumnNum+21], $rowNum+1));
                $lastRow[24] = $lDispNum[104];
                $lastRow[25] = $lDispNum[105];
                $lastRow[26] = $this->getExcelSubData($this->coord($colmunAlphaName[$startColumnNum+12], $rowNum+1), $this->coord($colmunAlphaName[$startColumnNum+19], $rowNum+1));
                $lastRow[28] = $this->getExcelDivData($this->coord($colmunAlphaName[$startColumnNum+19], $rowNum+1), $this->coord($colmunAlphaName[$startColumnNum+12], $rowNum+1));
                $lastRow[27] = $this->getExcelUtilizationRatesTrigona($colmunAlphaName[$startColumnNum+28], $rowNum+1, 95);

                # 訪問リハビリ
                $lastRow[29] = $lDispNum[96];
                $lastRow[30] = $lDispNum[100];
                $lastRow[31] = sprintf('=%s+%s', $this->coord($colmunAlphaName[$startColumnNum+29], $rowNum+1), $this->coord($colmunAlphaName[$startColumnNum+30], $rowNum+1));

                # 判定件数
                $lastRow[32] = $lDispNum[106];
                $lastRow[33] = $lDispNum[107];

                # 営業件数
                $lastRow[34] = $lDispNum[108];

                for ($i=0; $i<=34; $i++) {
                    switch ($i) {
                        default:
                            $compLeft    = $this->coord($colmunAlphaName[$startColumnNum+$i], $rowNum);
                            $compRight   = $this->coord($colmunAlphaName[$startColumnNum+$i], $rowNum+1);
                            $compRow[$i] = sprintf('=IF(%s="", IF(%s="", "", -%s), IF(%s="", %s, %s-%s))', $compLeft, $compRight, $compRight, $compRight, $compLeft, $compLeft, $compRight);
//                            $compRow[$i] = sprintf('=%s-%s', $this->coord($colmunAlphaName[$startColumnNum+$i], $rowNum), $this->coord($colmunAlphaName[$startColumnNum+$i], $rowNum+1));
                            break;

                        case '28':
                            $compLeft    = $this->coord($colmunAlphaName[$startColumnNum+$i], $rowNum);
                            $compRight   = $this->coord($colmunAlphaName[$startColumnNum+$i], $rowNum+1);
                            $compRow[$i] = sprintf('=IF(%s="", IF(%s="", "", -%s), IF(%s="", %s, %s-%s))', $compLeft, $compRight, $compRight, $compRight, $compLeft, $compLeft, $compRight);
//                            $compRow[$i] = sprintf('=%s-%s', $this->coord($colmunAlphaName[$startColumnNum+$i], $rowNum), $this->coord($colmunAlphaName[$startColumnNum+$i], $rowNum+1));
                            $compRow[27] = $this->getExcelUtilizationRatesTrigona($colmunAlphaName[$startColumnNum+28], $rowNum+2, 95);

                        case '27':
                            break;
                    }
                    switch ($i) {
                        default:
                            $raitoRow[$i] = $this->getExcelDivData($this->coord($colmunAlphaName[$startColumnNum+$i], $rowNum+2), $this->coord($colmunAlphaName[$startColumnNum+$i], $rowNum+1));
                            break;

                        case '11': case '23': case '27': case '28':
                            break;
                    }
                    switch ($i) {
                        default:
                            $tmpCurrentRowArr = array();
                            $tmpLastRowArr = array();
                            for ($j=0; $j<count($plantArr); $j++) {
                                $tmpCurrentRowArr[] = $this->coord($colmunAlphaName[$startColumnNum+$i], ($startRowNum + ($j * 4)));
                                $tmpLastRowArr[] = $this->coord($colmunAlphaName[$startColumnNum+$i], ($startRowNum + ($j * 4) + 1));
                            }
                            $totalCurrentRow[$i] = $this->getExcelSumDataByArray($tmpCurrentRowArr);
                            $totalLastRow[$i]    = $this->getExcelSumDataByArray($tmpLastRowArr);
                            break;

                        case '11':
                            $totalCurrentRow[$i] = $this->getExcelDivData($this->coord($colmunAlphaName[$startColumnNum+10], $totalRowNum), $this->coord($colmunAlphaName[$startColumnNum+0], $totalRowNum));
                            $totalLastRow[$i]    = $this->getExcelDivData($this->coord($colmunAlphaName[$startColumnNum+10], $totalRowNum+1), $this->coord($colmunAlphaName[$startColumnNum+0], $totalRowNum+1));
                            break;

                        case '23':
                            $totalCurrentRow[$i] = $this->getExcelDivData($this->coord($colmunAlphaName[$startColumnNum+22], $totalRowNum), $this->coord($colmunAlphaName[$startColumnNum+21], $totalRowNum));
                            $totalLastRow[$i]    = $this->getExcelDivData($this->coord($colmunAlphaName[$startColumnNum+22], $totalRowNum+1), $this->coord($colmunAlphaName[$startColumnNum+21], $totalRowNum+1));
                            break;

                        case '27':
                            break;

                        case '28':
                            $totalCurrentRow[$i] = $this->getExcelDivData($this->coord($colmunAlphaName[$startColumnNum+19], $totalRowNum), $this->coord($colmunAlphaName[$startColumnNum+12], $totalRowNum));
                            $totalCurrentRow[27] = $this->getExcelUtilizationRatesTrigona($colmunAlphaName[$startColumnNum+28], $totalRowNum, 95);
                            $totalLastRow[$i] = $this->getExcelDivData($this->coord($colmunAlphaName[$startColumnNum+19], $totalRowNum+1), $this->coord($colmunAlphaName[$startColumnNum+12], $totalRowNum+1));
                            $totalLastRow[27] = $this->getExcelUtilizationRatesTrigona($colmunAlphaName[$startColumnNum+28], $totalRowNum+1, 95);
                            break;
                    }
                    switch ($i) {
                        default:
                            $totalCompRow[$i] = sprintf('=%s-%s', $this->coord($colmunAlphaName[$startColumnNum+$i], $totalRowNum), $this->coord($colmunAlphaName[$startColumnNum+$i], $totalRowNum+1));
                            break;

                        case '27':
                            break;

                        case '28':
                            $totalCompRow[$i] = sprintf('=%s-%s', $this->coord($colmunAlphaName[$startColumnNum+$i], $totalRowNum), $this->coord($colmunAlphaName[$startColumnNum+$i], $totalRowNum+1));
                            $totalCompRow[27] = $this->getExcelUtilizationRatesTrigona($colmunAlphaName[$startColumnNum+$i], $totalRowNum+2, 95);
                            break;
                    }
                    switch ($i) {
                        default:
                            $totalRaitoRow[$i] = $this->getExcelDivData($this->coord($colmunAlphaName[$startColumnNum+$i], $totalRowNum+2), $this->coord($colmunAlphaName[$startColumnNum+$i], $totalRowNum+1));
                            break;

                        case '11': case '23': case '27': case '28':
                            break;
                    }
                }

                for ($r=0; $r<=3; $r++) {
                    $leftSide = array();
                    $mainData = array();
                    switch ($r) {
                        case '0':
                            $leftSide[0] = array('data' => $this->getArrValueByKey('facility_name', $plantVal), 'attr' => array($rowspan_4), 'style' => array('bgColor' => $bgColor_blue));
                            $leftSide[1] = array('data' => $displayCurrentYear, 'disp_type' => 'exception', 'style' => array('msoNumberFormat' => $msoNumberFormat_integer));
                            for ($c=0; $c<=34; $c++) {
                                switch ($c) {
                                    default:
                                        $mainData[$c] = array('data' => $this->getArrValueByKey($c, $currentRow));
                                        break;

                                    case '11': case '23': case '28':
                                        $mainData[$c] = array('data' => $this->getArrValueByKey($c, $currentRow), 'style' => array('msoNumberFormat' => $msoNumberFormat_percent0_0));
                                        break;

                                    case '27':
                                        $mainData[$c] = array('data' => $this->getArrValueByKey($c, $currentRow), 'style' => array('textAlign' => $textAlign_center));
                                        break;
                                }
                            }
                            break;

                        case '1':
                            $leftSide[0] = array('data' => $displayLastYear, 'disp_type' => 'exception', 'style' => array('msoNumberFormat' => $msoNumberFormat_integer));
                            for ($c=0; $c<=34; $c++) {
                                switch ($c) {
                                    default:
                                        $mainData[$c] = array('data' => $this->getArrValueByKey($c, $lastRow));
                                        break;

                                    case '11': case '23': case '28':
                                        $mainData[$c] = array('data' => $this->getArrValueByKey($c, $lastRow), 'style' => array('msoNumberFormat' => $msoNumberFormat_percent0_0));
                                        break;

                                    case '27':
                                        $mainData[$c] = array('data' => $this->getArrValueByKey($c, $lastRow), 'style' => array('textAlign' => $textAlign_center));
                                        break;
                                }
                            }
                            break;

                        case '2':
                            $leftSide[0] = array('data' => '前年比', 'style' => array('bgColor' => $bgColor_green));
                            for ($c=0; $c<=34; $c++) {
                                switch ($c) {
                                    default:
                                        $mainData[$c] = array('data' => $this->getArrValueByKey($c, $compRow), 'style' => array('bgColor' => $bgColor_green));
                                        break;

                                    case '27':
										//前年比で▲は表示しない
										$backSet = $compRow[27];
										$compRow[27] = " ";
										$mainData[$c] = array('data' => $this->getArrValueByKey($c, $compRow), 'style' => array('bgColor' => $bgColor_green, 'textAlign' => $textAlign_center));
										$compRow[27] = $backSet;
										
                                        break;

                                    case '11': case '23': case '28':
                                        $mainData[$c] = array('data' => $this->getArrValueByKey($c, $compRow), 'style' => array('bgColor' => $bgColor_green, 'msoNumberFormat' => $msoNumberFormat_percent0_0));
                                        break;
                                }
                            }
                            break;

                        case '3':
                            $leftSide[0] = array('data' => '増減率', 'style' => array('bgColor' => $bgColor_green));
                            for ($c=0; $c<=34; $c++) {
                                switch ($c) {
                                    default:
                                        $mainData[$c] = array('data' => $this->getArrValueByKey($c, $raitoRow), 'style' => array('bgColor' => $bgColor_green, 'msoNumberFormat' => $msoNumberFormat_percent0_0));
                                        break;

                                    case '27':
                                        $mainData[$c] = array('data' => $this->getArrValueByKey($c, $raitoRow), 'style' => array('bgColor' => $bgColor_green, 'textAlign' => $textAlign_center));
                                        break;
                                }
                            }
                            break;
                    }

                    $rtnStr .= '<tr>'
                                  .$this->convertCellData($leftSide, $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                                  .$this->convertCellData($mainData, $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma))
                              .'</tr>';
                }
            }
        }

        for ($t=0; $t<=3; $t++) {
            $totalLeftSide = array();
            $totalData     = array();
            switch ($t) {
                case '0':
                    $totalLeftSide[0] = array('data' => '合計', 'attr' => array($rowspan_4), 'style' => array('bgColor' => $bgColor_blue));
                    $totalLeftSide[1] = array('data' => $displayCurrentYear, 'disp_type' => 'exception', 'style' => array('msoNumberFormat' => $msoNumberFormat_integer));
                    for ($c=0; $c<=34; $c++) {
                        switch ($c) {
                            default:
                                $totalData[$c] = array('data' => $this->getArrValueByKey($c, $totalCurrentRow));
                                break;

                            case '11': case '23': case '28':
                                $totalData[$c] = array('data' => $this->getArrValueByKey($c, $totalCurrentRow), 'style' => array('msoNumberFormat' => $msoNumberFormat_percent0_0));
                                break;

                            case '27':
                                $totalData[$c] = array('data' => $this->getArrValueByKey($c, $totalCurrentRow), 'style' => array('textAlign' => $textAlign_center));
                                break;
                        }
                    }
                    break;

                case '1':
                    $totalLeftSide[0] = array('data' => $displayLastYear, 'disp_type' => 'exception', 'style' => array('msoNumberFormat' => $msoNumberFormat_integer));
                    for ($c=0; $c<=34; $c++) {
                        switch ($c) {
                            default:
                                $totalData[$c] = array('data' => $this->getArrValueByKey($c, $totalLastRow));
                                break;

                            case '11': case '23': case '28':
                                $totalData[$c] = array('data' => $this->getArrValueByKey($c, $totalLastRow), 'style' => array('msoNumberFormat' => $msoNumberFormat_percent0_0));
                                break;

                            case '27':
                                $totalData[$c] = array('data' => $this->getArrValueByKey($c, $totalLastRow), 'style' => array('textAlign' => $textAlign_center));
                                break;
                        }
                    }
                    break;

                case '2':
                    $totalLeftSide[0] = array('data' => '前年比', 'style' => array('bgColor' => $bgColor_green));
                    for ($c=0; $c<=34; $c++) {
                        switch ($c) {
                            default:
                                $totalData[$c] = array('data' => $this->getArrValueByKey($c, $totalCompRow), 'style' => array('bgColor' => $bgColor_green));
                                break;

                            case '27':
								//前年比で▲は表示しない
								$backSet = $totalCompRow[27];
								$totalCompRow[27] = " ";
                                $totalData[$c] = array('data' => $this->getArrValueByKey($c, $totalCompRow), 'style' => array('bgColor' => $bgColor_green, 'textAlign' => $textAlign_center));
								//$totalData[$c] = array('data' => $this->getArrValueByKey($c, $totalCompRow), 'style' => array('bgColor' => $bgColor_green, 'textAlign' => $textAlign_center));
								$totalCompRow[27] = $backSet;
								break;
                        }
                    }
                    break;

                case '3':
                    $totalLeftSide[0] = array('data' => '増減率', 'style' => array('bgColor' => $bgColor_green));
                    for ($c=0; $c<=34; $c++) {
                        switch ($c) {
                            default:
                                $totalData[$c] = array('data' => $this->getArrValueByKey($c, $totalRaitoRow), 'style' => array('bgColor' => $bgColor_green, 'msoNumberFormat' => $msoNumberFormat_percent0_0));
                                break;

                            case '27':
                                $totalData[$c] = array('data' => $this->getArrValueByKey($c, $totalRaitoRow), 'style' => array('bgColor' => $bgColor_green, 'textAlign' => $textAlign_center));
                                break;
                        }
                    }
                    break;
            }
            $rtnStr .= '<tr>'
                          .$this->convertCellData($totalLeftSide, $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($totalData, $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma))
                      .'</tr>';
        }

        $rtnStr .= '</table>';
        return $rtnStr;
    }

    /**
     * 老人保健施設施設稼働率
     * @param  array  $dataArr
     * @param  array  $plantArr
     * @param  array  $arr
     * @param  array  $optionArr
     * @return string
     */
    function outputExcelFacilityRate($dataArr, $plantArr, $arr, $optionArr)
    {
        $rtnStr  = $this->getExcelMeta();
        $fontTag = $this->getFontTag();

        # attr
        $colspan_2 = array('name' => 'colspan', 'value' => 2);
        $rowspan_4 = array('name' => 'rowspan', 'value' => 4);

        # style
        $bgColor_blue      = array('name' => 'background-color', 'value' => $this->getRgbColor('blue'));
        $bgColor_green     = array('name' => 'background-color', 'value' => $this->getRgbColor('green'));
        $textAlign_center  = array('name' => 'text-align', 'value' => 'center');
        $textAlign_right   = array('name' => 'text-align', 'value' => 'right');
        $whiteSpace_nowrap = array('name' => 'white-space', 'value' => 'nowrap');
        $msoNumberFormat_percent0_0 = $this->getMsoNumberFormat('percent');

        $cYear      = $this->getArrValueByKey('year', $optionArr);
        $lYear      = $cYear - 1;
        $startMonth = $this->getFiscalYearStartMonth();

        $monthArr        = array();
        $monthNameArr    = array();
        $monthNameArr[0] = array('data' => '&nbsp;', 'attr' => array($colspan_2));
        for ($i=0; $i<12; $i++) {
            $tmpMonth = $startMonth + $i;
            $monthArr[$i]['current_year'] = ($tmpMonth <= 12)? $cYear : $cYear+1;
            $monthArr[$i]['last_year']    = ($tmpMonth <= 12)? $lYear : $lYear+1;
            $monthArr[$i]['target_month'] = ($tmpMonth <= 12)? $tmpMonth : $tmpMonth-12;
            $monthNameArr[] = array('data' => $monthArr[$i]['target_month'].'月');
        }
        $monthNameArr[] = array('data' => '平均');

        $rtnStr .= '<table class="list" width="100%" border="1" >'
                      .'<tr>'
                          .$this->convertCellData(
                              $monthNameArr,
                              $fontTag,
                              array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
                          )
                      .'</tr>';


        $cAvrgRowMonthCount = array();
        $lAvrgRowMonthCount = array();

        $avrgRowCAvrgData     = 0;
        $avrgRowLAvrgData     = 0;
        $avrgRowCompAvrgData  = 0;
        $avrgRowRaitoAvrgData = 0;

        $startRowNum     = 2;
        $startColumnNum  = 3;
        $totalRowNum     = $startRowNum + (count($plantArr)*4);
        $columnAlphaName = $this->columnAlphaNameArr;

        foreach ($plantArr as $plantKey => $plantVal) {

            $rowNum = $startRowNum + ($plantKey * 4);
            $facilityId = $this->getArrValueByKey('facility_id', $plantVal);

            for ($r=0; $r<=12; $r++) {
                $cRowData[$r]  = 0;
                $lRowData[$r]  = 0;
                $cRowExcel[$r] = null;
                $lRowExcel[$r] = null;
                $compData[$r]  = null;
                $raitoData[$r] = 0;
            }

            $cRowMonthCount = array();
            $lRowMonthCount = array();

            foreach ($monthArr as $monthKey => $monthVal) {
                $targetMonth = $this->getArrValueByKey('target_month', $monthVal);
                foreach ($dataArr as $dataVal) {
                    $targetData = null;
                    if ($facilityId == $this->getArrValueByKey('jnl_facility_id', $dataVal) && $targetMonth == $this->getArrValueByKey('month', $dataVal) ) {
                        if ($this->getArrValueByKey('current_year', $monthVal) == $this->getArrValueByKey('year', $dataVal)) {
                            # 表示年度データ
                            $targetData = $this->getArrValueByKey('target_data', $dataVal);
                            $cRowData[$monthKey] = $targetData;
                            $cRowExcel[$monthKey] = $targetData/100;
                            $cRowMonthCount[$monthKey] = true;
                        }
                        else if ($this->getArrValueByKey('last_year', $monthVal) == $this->getArrValueByKey('year', $dataVal)) {
                            # 前年度データ
                            $targetData = $this->getArrValueByKey('target_data', $dataVal);
                            $lRowData[$monthKey] = $targetData;
                            $lRowExcel[$monthKey] = $targetData/100;
                            $lRowMonthCount[$monthKey] = true;
                        }
                    }
                }
            }

            $commonRowDataArr = array();
            foreach ($cRowMonthCount as $cKey => $cVal) {
                if ($cVal===true) {
                    $commonRowDataArr[$cKey]   = $commonRowDataArr[$cKey] + 1;
                    $cAvrgRowMonthCount[$cKey] = $cAvrgRowMonthCount[$cKey] + 1;
                }
            }
            foreach ($lRowMonthCount as $lKey => $lVal) {
                if ($lVal===true) {
                    $commonRowDataArr[$lKey]   = $commonRowDataArr[$lKey] + 1;
                    $lAvrgRowMonthCount[$lKey] = $lAvrgRowMonthCount[$lKey] + 1;
                }
            }

            $cTotalData     = 0;
            $lTotalData     = 0;
            $compTotalData  = 0;
            $raitoTotalData = 0;

            for ($r=0; $r<=12; $r++) {
                switch ($r) {
                    default:
                        $compData[$r]  = $this->getExcelSubData($this->coord($columnAlphaName[$startColumnNum+$r], $rowNum), $this->coord($columnAlphaName[$startColumnNum+$r], $rowNum+1));
                        $raitoData[$r] = $this->getExcelDivData($this->coord($columnAlphaName[$startColumnNum+$r], $rowNum+2), $this->coord($columnAlphaName[$startColumnNum+$r], $rowNum+1));
                        $cTotalData    = $cTotalData + $cRowData[$r];
                        $lTotalData    = $lTotalData + $lRowData[$r];
                        break;

                    case '12':
                        $cRowData[$r]  = $this->getExcelSideAverageData($columnAlphaName[$startColumnNum], $columnAlphaName[$startColumnNum+11], $rowNum, count($cRowMonthCount));
                        $lRowData[$r]  = $this->getExcelSideAverageData($columnAlphaName[$startColumnNum], $columnAlphaName[$startColumnNum+11], $rowNum+1, count($lRowMonthCount));
                        $compData[$r]  = $this->getExcelSubData($this->coord($columnAlphaName[$startColumnNum+12], $rowNum), $this->coord($columnAlphaName[$startColumnNum+12], $rowNum+1), 3);
                        $raitoData[$r] = $this->getExcelDivData($this->coord($columnAlphaName[$startColumnNum+12], $rowNum+2), $this->coord($columnAlphaName[$startColumnNum+12], $rowNum+1));
                        break;
                }
            }
            for ($d=0; $d<=3; $d++) {
                $leftSide = array();
                $mainData = array();

                switch ($d) {
                    case '0':
                        $leftSide[0] = array('data' => $this->getArrValueByKey('facility_name', $plantVal), 'attr' => array($rowspan_4), 'style' => array('bgColor' => $bgColor_blue));
                        $leftSide[1] = array('data' => sprintf('%04d年度', $cYear));
                        for ($r=0; $r<=12; $r++) {
                            switch ($r) {
                                default:
                                    $mainData[$r] = array('data' => $cRowExcel[$r], 'style' => array($msoNumberFormat_percent0_0), 'disp_type' => 'exception');
                                    break;

                                case '12':
                                    $mainData[$r] = array('data' => $cRowData[$r], 'style' => array($msoNumberFormat_percent0_0), 'disp_type' => 'exception');
                                    break;
                            }
                        }
                        break;

                    case '1':
                        $leftSide[0] = array('data' => sprintf('%04d年度', $lYear));
                        for ($r=0; $r<=12; $r++) {
                            switch ($r) {
                                default:
                                    $mainData[$r] = array('data' => $lRowExcel[$r], 'style' => array($msoNumberFormat_percent0_0), 'disp_type' => 'exception');
                                    break;

                                case '12':
                                    $mainData[$r] = array('data' => $lRowData[$r], 'style' => array($msoNumberFormat_percent0_0), 'disp_type' => 'exception');
                                    break;
                            }
                        }
                        break;

                    case '2':
                        $leftSide[0] = array('data' => '増減', 'style' => array($bgColor_green));
                        for ($r=0; $r<=12; $r++) { $mainData[$r] = array('data' => $compData[$r], 'style' => array($bgColor_green, $msoNumberFormat_percent0_0), 'disp_type' => 'exception'); }
                        break;

                    case '3':
                        $leftSide[0] = array('data' => '増減率', 'style' => array($bgColor_green));
                        for ($r=0; $r<=12; $r++) { $mainData[$r] = array('data' => $raitoData[$r], 'style' => array($bgColor_green, $msoNumberFormat_percent0_0), 'disp_type' => 'exception'); }
                        break;
                }

                $rtnStr .= '<tr>'
                               .$this->convertCellData($leftSide, $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                               .$this->convertCellData($mainData, $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap))
                          .'</tr>';
            }
        }

        # AMG平均の施設平均の取得
        for ($i=0; $i<=12; $i++) {
            switch ($i) {
                default:
                    $targetColumn = $columnAlphaName[$startColumnNum+$i];

                    $cColumnArr = array();
                    $lColumnArr = array();
                    foreach ($plantArr as $plantKey => $plantVal) {
                        $cColumnArr[] = $this->coord($targetColumn, $startRowNum+($plantKey*4));
                        $lColumnArr[] = $this->coord($targetColumn, $startRowNum+($plantKey*4)+1);
                    }

                    $avrgRowCArr[$i]     = $this->getExcelAverageDataByArray($cColumnArr, $cAvrgRowMonthCount[$i], 3);
                    $avrgRowLArr[$i]     = $this->getExcelAverageDataByArray($lColumnArr, $lAvrgRowMonthCount[$i], 3);
                    $avrgRowCompArr[$i]  = $this->getExcelSubData($this->coord($targetColumn, $startRowNum+(count($plantArr)*4)), $this->coord($targetColumn, $startRowNum+(count($plantArr)*4)+1));
                    $avrgRowRaitoArr[$i] = $this->getExcelDivData($this->coord($targetColumn, $startRowNum+(count($plantArr)*4)+2), $this->coord($targetColumn, $startRowNum+(count($plantArr)*4)+1));

                    $avrgRowCAvrg = $avrgRowCAvrg + $avrgRowCArr[$i];
                    $avrgRowLAvrg = $avrgRowLAvrg + $avrgRowLArr[$i];
                    break;

                case '12':
                    $targetColumn = $columnAlphaName[$startColumnNum+$i];

                    $cColumnArr = array();
                    $lColumnArr = array();
                    foreach ($plantArr as $plantKey => $plantVal) {
                        $cColumnArr[] = $this->coord($targetColumn, $startRowNum+($plantKey*4));
                        $lColumnArr[] = $this->coord($targetColumn, $startRowNum+($plantKey*4)+1);
                    }

                    $avrgRowCArr[$i]     = $this->getExcelSideAverageData($columnAlphaName[$startColumnNum], $columnAlphaName[$startColumnNum+11], $totalRowNum, count($cAvrgRowMonthCount), 3);
                    $avrgRowLArr[$i]     = $this->getExcelSideAverageData($columnAlphaName[$startColumnNum], $columnAlphaName[$startColumnNum+11], $totalRowNum+1, count($lAvrgRowMonthCount), 3);
                    $avrgRowCompArr[$i]  = $this->getExcelSubData($this->coord($targetColumn, $totalRowNum), $this->coord($targetColumn, $totalRowNum+1));
                    $avrgRowRaitoArr[$i] = $this->getExcelDivData($this->coord($targetColumn, $totalRowNum+2), $this->coord($targetColumn, $totalRowNum+1));
                    break;
            }
        }

        # AMG平均行の出力
        for ($a=0; $a<=3; $a++) {
            $avrgData     = array();
            $avrgLeftSide = array();

            switch ($a) {
                case '0':
                    $avrgLeftSide[0] = array('data' => 'ＡＭＧ平均', 'attr' => array($rowspan_4), 'style' => array('bgColor' => $bgColor_blue));
                    $avrgLeftSide[1] = array('data' => sprintf('%04d年度', $cYear));
                    for ($r=0; $r<=12; $r++) { $avrgData[$r] = array('data' => $avrgRowCArr[$r], 'style' => array($msoNumberFormat_percent0_0)); }
                    break;

                case '1':
                    $avrgLeftSide[0] = array('data' => sprintf('%04d年度', $lYear));
                    for ($r=0; $r<=12; $r++) { $avrgData[$r] = array('data' => $avrgRowLArr[$r], 'style' => array($msoNumberFormat_percent0_0)); }
                    break;

                case '2':
                    $avrgLeftSide[0] = array('data' => '増減', 'style' => array($bgColor_green));
                    for ($r=0; $r<=12; $r++) { $avrgData[$r] = array('data' => $avrgRowCompArr[$r], 'style' => array($bgColor_green, $msoNumberFormat_percent0_0)); }
                    break;

                case '3':
                    $avrgLeftSide[0] = array('data' => '増減率', 'style' => array($bgColor_green));
                    for ($r=0; $r<=12; $r++) { $avrgData[$r] = array('data' => $avrgRowRaitoArr[$r], 'style' => array($bgColor_green, $msoNumberFormat_percent0_0)); }
                    break;
            }

            $rtnStr .= '<tr>'
                          .$this->convertCellData($avrgLeftSide, $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($avrgData, $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap))
                      .'</tr>';
        }

        $rtnStr .= '</table>';
        return $rtnStr;
    }

    /**
     * No.111 [老健] 老人保健施設通所(合計)延数
     * No.112 [老健] 老人保健施設通所(介護)延数
     * No.113 [老健] 老人保健施設通所(予防)延数
     * No.115 [老健] 老人保健施設入所(合計)延数
     * No.116 [老健] 老人保健施設入所(一般)延数
     * No.117 [老健] 老人保健施設入所(認知)延数
     * No.118 [老健] 老人保健施設入所(一般ショート)延数
     * No.119 [老健] 老人保健施設入所(認知ショート)延数
     * No.120 [老健] 老人保健施設入所(予防ショート)延数
     * のエクセル出力
     * @param  array  $dataArr
     * @param  array  $plantArr
     * @param  array  $arr
     * @param  array  $optionArr
     * @return string
     */
    function outputExcelNumberTotalFacility($dataArr, $plantArr, $arr, $optionArr)
    {
        $rtnStr  = $this->getExcelMeta();
        $fontTag = $this->getFontTag();

        # attr
        $colspan_2 = array('name' => 'colspan', 'value' => 2);
        $rowspan_4 = array('name' => 'rowspan', 'value' => 4);

        # style
        $bgColor_blue               = array('name' => 'background-color',  'value' => $this->getRgbColor('blue'));
        $bgColor_green              = array('name' => 'background-color',  'value' => $this->getRgbColor('green'));
        $textAlign_center           = array('name' => 'text-align',        'value' => 'center');
        $textAlign_right            = array('name' => 'text-align',        'value' => 'right');
        $whiteSpace_nowrap          = array('name' => 'white-space',       'value' => 'nowrap');
        $msoNumberFormat_0_0        = $this->getMsoNumberFormat('decimal');
        $msoNumberFormat_percent0_0 = $this->getMsoNumberFormat('percent');
        $msoNumberFormat_comma      = $this->getMsoNumberFormat('comma');
//        $msoNumberFormat_0_0        = array('name' => 'mso-number-format', 'value' => '0.0');
//        $msoNumberFormat_percent0_0 = array('name' => 'mso-number-format', 'value' => '0.0%');

        $cYear      = $this->getArrValueByKey('year', $optionArr);
        $lYear      = $cYear - 1;
        $startMonth = $this->getFiscalYearStartMonth();

        $monthArr        = array();
        $monthNameArr    = array();
        $monthNameArr[0] = array('data' => '&nbsp;', 'attr' => array($colspan_2));
        for ($i=0; $i<12; $i++) {
            $tmpMonth = $startMonth + $i;
            $monthArr[$i]['current_year'] = ($tmpMonth <= 12)? $cYear : $cYear+1;
            $monthArr[$i]['last_year']    = ($tmpMonth <= 12)? $lYear : $lYear+1;
            $monthArr[$i]['target_month'] = ($tmpMonth <= 12)? $tmpMonth : $tmpMonth-12;
            $monthNameArr[] = array('data' => $monthArr[$i]['target_month'].'月');
        }
        $monthNameArr[] = array('data' => '合計');
        $monthNameArr[] = array('data' => '平均');

        $rtnStr .= '<table class="list" width="100%" border="1" >'
                      .'<tr>'
                          .$this->convertCellData(
                              $monthNameArr,
                              $fontTag,
                              array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
                          )
                      .'</tr>';


        $cAvrgRowMonthCount = array();
        $lAvrgRowMonthCount = array();
        $commonRowDataArr   = array();

        $avrgRowCAvrgData     = 0;
        $avrgRowLAvrgData     = 0;
        $avrgRowCompAvrgData  = 0;
        $avrgRowRaitoAvrgData = 0;

        $startRowNum     = 2;
        $startColumnNum  = 3;
        $totalRowNum     = $startRowNum + (count($plantArr)*4);
        $columnAlphaName = $this->columnAlphaNameArr;

        # 病院配列
        foreach ($plantArr as $plantKey => $plantVal) {

            $rowNum = $startRowNum + ($plantKey * 4);
            $facilityId = $this->getArrValueByKey('facility_id', $plantVal);

            for ($r=0; $r<=13; $r++) {
                $cRowData[$r]  = 0;
                $lRowData[$r]  = 0;
                $cRowExcel[$r] = null;
                $lRowExcel[$r] = null;
                $compData[$r]  = null;
                $raitoData[$r] = 0;
            }

            $cRowMonthCount = array();
            $lRowMonthCount = array();

            foreach ($monthArr as $monthKey => $monthVal) {
                $targetMonth = $this->getArrValueByKey('target_month', $monthVal);
                foreach ($dataArr as $dataVal) {
                    $targetData = null;
                    if ($facilityId == $this->getArrValueByKey('jnl_facility_id', $dataVal) && $targetMonth == $this->getArrValueByKey('month', $dataVal) ) {
                        if ($this->getArrValueByKey('current_year', $monthVal) == $this->getArrValueByKey('year', $dataVal)) { # 表示年度データ
                            $targetData = $this->getArrValueByKey('target_data', $dataVal);
                            $cRowData[$monthKey] = $targetData;
                            $cRowExcel[$monthKey] = $targetData;
                            $cRowMonthCount[$monthKey] = true;
                        }
                        else if ($this->getArrValueByKey('last_year', $monthVal) == $this->getArrValueByKey('year', $dataVal)) { # 前年度データ
                            $targetData = $this->getArrValueByKey('target_data', $dataVal);
                            $lRowData[$monthKey] = $targetData;
                            $lRowExcel[$monthKey] = $targetData;
                            $lRowMonthCount[$monthKey] = true;
                        }
                    }
                }
            }

            foreach ($cRowMonthCount as $cKey => $cVal) {
                if ($cVal===true) {
                    $commonRowDataArr[$cKey]   = $commonRowDataArr[$cKey] + 1;
                    $cAvrgRowMonthCount[$cKey] = $cAvrgRowMonthCount[$cKey] + 1;
                }
            }
            foreach ($lRowMonthCount as $lKey => $lVal) {
                if ($lVal===true) {
                    $commonRowDataArr[$lKey]   = $commonRowDataArr[$lKey] + 1;
                    $lAvrgRowMonthCount[$lKey] = $lAvrgRowMonthCount[$lKey] + 1;
                }
            }

            $cTotalData     = 0;
            $lTotalData     = 0;
            $compTotalData  = 0;
            $raitoTotalData = 0;

            for ($r=0; $r<=13; $r++) {
                switch ($r) {
                    default:
                        $compData[$r]  = $this->getExcelSubData($this->coord($columnAlphaName[$startColumnNum+$r], $rowNum), $this->coord($columnAlphaName[$startColumnNum+$r], $rowNum+1), 1);
                        $raitoData[$r] = $this->getExcelDivData($this->coord($columnAlphaName[$startColumnNum+$r], $rowNum+2), $this->coord($columnAlphaName[$startColumnNum+$r], $rowNum+1), 3);
                        $cTotalData    = $cTotalData + $cRowData[$r];
                        $lTotalData    = $lTotalData + $lRowData[$r];
                        break;

                    case '12':
                        $cRowData[$r]  = $this->getExcelSideSumData($columnAlphaName[$startColumnNum], $columnAlphaName[$startColumnNum+11], $rowNum, 1);
                        $lRowData[$r]  = $this->getExcelSideSumData($columnAlphaName[$startColumnNum], $columnAlphaName[$startColumnNum+11], $rowNum+1, 1);
                        $compData[$r]  = $this->getExcelSubData($this->coord($columnAlphaName[$startColumnNum+12], $rowNum), $this->coord($columnAlphaName[$startColumnNum+12], $rowNum+1), 1);
                        $raitoData[$r] = $this->getExcelDivData($this->coord($columnAlphaName[$startColumnNum+12], $rowNum+2), $this->coord($columnAlphaName[$startColumnNum+12], $rowNum+1), 3);
                        break;

                    case '13':
                        $cRowData[$r]  = $this->getExcelSideAverageData($columnAlphaName[$startColumnNum], $columnAlphaName[$startColumnNum+11], $rowNum, count($cRowMonthCount), 1);
                        $lRowData[$r]  = $this->getExcelSideAverageData($columnAlphaName[$startColumnNum], $columnAlphaName[$startColumnNum+11], $rowNum+1, count($lRowMonthCount), 1);
                        $compData[$r]  = $this->getExcelSubData($this->coord($columnAlphaName[$startColumnNum+13], $rowNum), $this->coord($columnAlphaName[$startColumnNum+13], $rowNum+1), 1);
                        $raitoData[$r] = $this->getExcelDivData($this->coord($columnAlphaName[$startColumnNum+13], $rowNum+2), $this->coord($columnAlphaName[$startColumnNum+13], $rowNum+1), 3);
                        break;
                }
            }
            for ($d=0; $d<=3; $d++) {
                $leftSide = array();
                $mainData = array();

                switch ($d) {
                    case '0':
                        $leftSide[0] = array('data' => $this->getArrValueByKey('facility_name', $plantVal), 'attr' => array($rowspan_4), 'style' => array('bgColor' => $bgColor_blue));
                        $leftSide[1] = array('data' => sprintf('%04d年度', $cYear));
                        for ($r=0; $r<=13; $r++) {
                            switch ($r) {
                                default:
                                    $mainData[$r] = array('data' => $cRowExcel[$r]);
                                    break;

                                case '12':
                                    $mainData[$r] = array('data' => $cRowData[$r]);
                                    break;

                                case '13':
                                    $mainData[$r] = array('data' => $cRowData[$r], 'style' => array('msoNumberFormat' => $msoNumberFormat_0_0));
                                    break;
                            }
                        }
                        break;

                    case '1':
                        $leftSide[0] = array('data' => sprintf('%04d年度', $lYear));
                        for ($r=0; $r<=13; $r++) {
                            switch ($r) {
                                default:
                                    $mainData[$r] = array('data' => $lRowExcel[$r]);
                                    break;

                                case '12':
                                    $mainData[$r] = array('data' => $lRowData[$r]);
                                    break;

                                case '13':
                                    $mainData[$r] = array('data' => $lRowData[$r], 'style' => array('msoNumberFormat' => $msoNumberFormat_0_0));
                                    break;
                            }
                        }
                        break;

                    case '2':
                        $leftSide[0] = array('data' => '増減', 'style' => array($bgColor_green));
                        for ($r=0; $r<=13; $r++) { $mainData[$r] = array('data' => $compData[$r], 'style' => array($bgColor_green, 'msoNumberFormat' => $msoNumberFormat_0_0)); }
                        break;

                    case '3':
                        $leftSide[0] = array('data' => '増減率', 'style' => array($bgColor_green));
                        for ($r=0; $r<=13; $r++) { $mainData[$r] = array('data' => $raitoData[$r], 'style' => array($bgColor_green, 'msoNumberFormat' => $msoNumberFormat_percent0_0)); }
                        break;
                }

                $rtnStr .= '<tr>'
                               .$this->convertCellData($leftSide, $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                               .$this->convertCellData($mainData, $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma))
                          .'</tr>';
            }
        }

        # AMG合計の施設合計と施設平均の取得
        for ($i=0; $i<=13; $i++) {
            $targetColumn = $columnAlphaName[$startColumnNum+$i];
            $cColumnArr   = array();
            $lColumnArr   = array();

            foreach ($plantArr as $plantKey => $plantVal) {
                $cColumnArr[] = $this->coord($targetColumn, $startRowNum+($plantKey*4));
                $lColumnArr[] = $this->coord($targetColumn, $startRowNum+($plantKey*4)+1);
            }

            switch ($i) {
                default:
                    $avrgRowCArr[$i]     = $this->getExcelSumDataByArray($cColumnArr);
                    $avrgRowLArr[$i]     = $this->getExcelSumDataByArray($lColumnArr);
                    $avrgRowCompArr[$i]  = $this->getExcelSubData($this->coord($targetColumn, $startRowNum+(count($plantArr)*4)), $this->coord($targetColumn, $startRowNum+(count($plantArr)*4)+1));
                    $avrgRowRaitoArr[$i] = $this->getExcelDivData($this->coord($targetColumn, $startRowNum+(count($plantArr)*4)+2), $this->coord($targetColumn, $startRowNum+(count($plantArr)*4)+1));

                    $avrgRowCAvrg = $avrgRowCAvrg + $avrgRowCArr[$i];
                    $avrgRowLAvrg = $avrgRowLAvrg + $avrgRowLArr[$i];
                    break;

                case '12':
                    $avrgRowCArr[$i]     = $this->getExcelSideSumData($columnAlphaName[$startColumnNum], $columnAlphaName[$startColumnNum+11], $totalRowNum);
                    $avrgRowLArr[$i]     = $this->getExcelSideSumData($columnAlphaName[$startColumnNum], $columnAlphaName[$startColumnNum+11], $totalRowNum+1);
                    $avrgRowCompArr[$i]  = $this->getExcelSubData($this->coord($targetColumn, $totalRowNum), $this->coord($targetColumn, $totalRowNum+1));
                    $avrgRowRaitoArr[$i] = $this->getExcelDivData($this->coord($targetColumn, $totalRowNum+2), $this->coord($targetColumn, $totalRowNum+1), 3);
                    break;

                case '13':
                    $avrgRowCArr[$i]     = $this->getExcelSideAverageData($columnAlphaName[$startColumnNum], $columnAlphaName[$startColumnNum+11], $totalRowNum, count($cAvrgRowMonthCount), 1);
                    $avrgRowLArr[$i]     = $this->getExcelSideAverageData($columnAlphaName[$startColumnNum], $columnAlphaName[$startColumnNum+11], $totalRowNum+1, count($lAvrgRowMonthCount), 1);
                    $avrgRowCompArr[$i]  = $this->getExcelSubData($this->coord($targetColumn, $totalRowNum), $this->coord($targetColumn, $totalRowNum+1), 1);
                    $avrgRowRaitoArr[$i] = $this->getExcelDivData($this->coord($targetColumn, $totalRowNum+2), $this->coord($targetColumn, $totalRowNum+1), 3);
                    break;
            }
        }

        # AMG合計行の出力
        for ($a=0; $a<=3; $a++) {
            $avrgData     = array();
            $avrgLeftSide = array();

            switch ($a) {
                case '0':
                    $avrgLeftSide[0] = array('data' => 'ＡＭＧ合計', 'attr' => array($rowspan_4), 'style' => array('bgColor' => $bgColor_blue));
                    $avrgLeftSide[1] = array('data' => sprintf('%04d年度', $cYear));
                    for ($r=0; $r<=13; $r++) {
                        switch ($r) {
                            case '13':
                                $avrgData[$r] = array('data' => $avrgRowCArr[$r], 'style' => array('msoNumberFormat' => $msoNumberFormat_0_0));
                                break;

                            default:
                                $avrgData[$r] = array('data' => $avrgRowCArr[$r]);
                                break;
                        }
                    }
                    break;

                case '1':
                    $avrgLeftSide[0] = array('data' => sprintf('%04d年度', $lYear));
                    for ($r=0; $r<=13; $r++) {
                        switch ($r) {
                            case '13':
                                $avrgData[$r] = array('data' => $avrgRowLArr[$r], 'style' => array('msoNumberFormat' => $msoNumberFormat_0_0));
                                break;

                            default:
                                $avrgData[$r] = array('data' => $avrgRowLArr[$r]);
                                break;
                        }
                    }
                    break;

                case '2':
                    $avrgLeftSide[0] = array('data' => '増減', 'style' => array($bgColor_green));
                    for ($r=0; $r<=13; $r++) { $avrgData[$r] = array('data' => $avrgRowCompArr[$r], 'style' => array($bgColor_green, 'msoNumberFormat' => $msoNumberFormat_0_0)); }
                    break;

                case '3':
                    $avrgLeftSide[0] = array('data' => '増減率', 'style' => array($bgColor_green));
                    for ($r=0; $r<=13; $r++) { $avrgData[$r] = array('data' => $avrgRowRaitoArr[$r], 'style' => array($bgColor_green, 'msoNumberFormat' => $msoNumberFormat_percent0_0)); }
                    break;
            }

            $rtnStr .= '<tr>'
                          .$this->convertCellData($avrgLeftSide, $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($avrgData, $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma))
                      .'</tr>';
        }

        $rtnStr .= '</table>';
        return $rtnStr;
    }


    /**
     * No.122 [老健] 判定件数のエクセル出力
     * @param  array  $dataArr
     * @param  array  $plantArr
     * @param  array  $arr
     * @param  array  $optionArr
     * @return string
     */
    function outputExcelJudgmentNumber($dataArr, $plantArr, $arr, $optionArr)
    {
        $rtnStr = $this->getExcelMeta();
        $fontTag = $this->getFontTag();

        # attr
        $colspan_2 = array('name' => 'colspan', 'value' => 2);
        $rowspan_2 = array('name' => 'rowspan', 'value' => 2);
        $rowspan_4 = array('name' => 'rowspan', 'value' => 4);

        # style
        $bgColor_blue            = array('name' => 'background-color',  'value' => $this->getRgbColor('blue'));
        $bgColor_green           = array('name' => 'background-color',  'value' => $this->getRgbColor('green'));
        $textAlign_center        = array('name' => 'text-align',        'value' => 'center');
        $textAlign_right         = array('name' => 'text-align',        'value' => 'right');
        $whiteSpace_nowrap       = array('name' => 'white-space',       'value' => 'nowrap');
        $msoNumberFormat_percent = $this->getMsoNumberFormat('percent');
        $msoNumberFormat_0_0     = $this->getMsoNumberFormat('decimal');
        $msoNumberFormat_comma   = $this->getMsoNumberFormat('comma');
//        $msoNumberFormat_percent = array('name' => 'mso-number-format', 'value' => '0.0%');
//        $msoNumberFormat_0_0     = array('name' => 'mso-number-format', 'value' => '0.0');

        $cYear      = $this->getArrValueByKey('year', $optionArr);
        $lYear      = $cYear - 1;
        $startMonth = $this->getFiscalYearStartMonth();

        $monthArr        = array();
        $monthNameArr    = array();
        $secondRowArr    = array();
        $monthNameArr[0] = array('data' => '&nbsp;', 'attr' => array($rowspan_2, $colspan_2));
        for ($i=0; $i<12; $i++) {
            $tmpMonth = $startMonth + $i;
            $monthArr[$i]['current_year'] = ($tmpMonth <= 12)? $cYear : $cYear+1;
            $monthArr[$i]['last_year']    = ($tmpMonth <= 12)? $lYear : $lYear+1;
            $monthArr[$i]['target_month'] = ($tmpMonth <= 12)? $tmpMonth : $tmpMonth-12;
            $monthNameArr[] = array('data' => $monthArr[$i]['target_month'].'月', 'attr' => array($colspan_2));
            $secondRowArr[] = array('data' => '可');
            $secondRowArr[] = array('data' => '不可');
        }
        $monthNameArr[] = array('data' => '合計', 'attr' => array($colspan_2));
        $secondRowArr[] = array('data' => '可');
        $secondRowArr[] = array('data' => '不可');
        $monthNameArr[] = array('data' => '平均', 'attr' => array($colspan_2));
        $secondRowArr[] = array('data' => '可');
        $secondRowArr[] = array('data' => '不可');

        $rtnStr .= '<table class="list" width="100%" border="1" >'
                      .'<tr>'
                          .$this->convertCellData($monthNameArr, $fontTag, array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                      .'</tr>'
                      .'<tr>'
                          .$this->convertCellData($secondRowArr, $fontTag, array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                      .'</tr>';

        $totalRowCAvrgData     = 0;
        $totalRowLAvrgData     = 0;
        $totalRowCompAvrgData  = 0;
        $totalRowRaitoAvrgData = 0;

        $cExRowMonthCount = array();
        $lExRowMonthCount = array();
        $commonRowDataArr = array();

        $avrgRowCAvrgData     = 0;
        $avrgRowLAvrgData     = 0;
        $avrgRowCompAvrgData  = 0;
        $avrgRowRaitoAvrgData = 0;

        $exCStatus     = array();
        $exLStatus     = array();
        $exCompStatus  = array();
        $exRaitoStatus = array();

        $startRowNum     = 3;
        $startColumnNum  = 3;
        $totalRowNum     = $startRowNum + (count($plantArr)*4);
        $columnAlphaName = $this->columnAlphaNameArr;

        foreach ($plantArr as $plantKey => $plantVal) {

            $rowNum = $startRowNum + ($plantKey * 4);
            $facilityId = $this->getArrValueByKey('facility_id', $plantVal);
            for ($r=0; $r<=24; $r++) {
                $cRowData[$r]  = null;
                $lRowData[$r]  = null;
                $compData[$r]  = null;
                $raitoData[$r] = null;
                $cStatus[$r]   = null;
                $lStatus[$r]   = null;
            }

            $cRowMonthCount_pass       = array();
            $lRowMonthCount_pass       = array();
            $cRowMonthCount_disapprove = array();
            $lRowMonthCount_disapprove = array();

            foreach ($monthArr as $monthKey => $monthVal) {
                $targetMonth = $this->getArrValueByKey('target_month', $monthVal);
                if (is_array($dataArr) && count($dataArr)!=0) {
                    foreach ($dataArr as $dataVal) {
                        $targetData = null;
                        if ($facilityId == $this->getArrValueByKey('jnl_facility_id', $dataVal) && $targetMonth == $this->getArrValueByKey('month', $dataVal) ) {
                            if ($this->getArrValueByKey('current_year', $monthVal) == $this->getArrValueByKey('year', $dataVal)) {
                                # 表示年度データ
                                $targetData = $this->getArrValueByKey('target_data', $dataVal);
                                $cRowData[$monthKey*2] = $targetData['pass'];
                                $cRowData[($monthKey*2)+1] = $targetData['disapprove'];
                                $cRowMonthCount[$monthKey*2] = true;
                                $cRowMonthCount[($monthKey*2)+1] = true;
                                $cRowMonthCount_pass[$monthKey*2] = true;
                                $cRowMonthCount_disapprove[($monthKey*2)+1] = true;
                                $cStatus[$monthKey*2]     = $this->checkCellDataStatus('facility_rate', $dataVal);
                                $cStatus[($monthKey*2)+1] = $this->checkCellDataStatus('facility_rate', $dataVal);
                            }
                            else if ($this->getArrValueByKey('last_year', $monthVal) == $this->getArrValueByKey('year', $dataVal)) {
                                # 前年度データ
                                $targetData = $this->getArrValueByKey('target_data', $dataVal);
                                $lRowData[$monthKey*2] = $targetData['pass'];
                                $lRowData[($monthKey*2)+1] = $targetData['disapprove'];
                                $lRowMonthCount[$monthKey*2]     = true;
                                $lRowMonthCount[($monthKey*2)+1] = true;
                                $lRowMonthCount_pass[$monthKey*2] = true;
                                $lRowMonthCount_disapprove[($monthKey*2)+1] = true;
                                $lStatus[$monthKey*2]     = $this->checkCellDataStatus('facility_rate', $dataVal);
                                $lStatus[($monthKey*2)+1] = $this->checkCellDataStatus('facility_rate', $dataVal);
                            }
                        }
                    }
                }
                if (is_null($cStatus[$monthKey*2])) {
                    $cStatus[$monthKey*2]     = $this->checkCellDataStatus('facility_rate', array('year' => $monthVal['current_year'], 'month' => $targetMonth));
                    $cStatus[($monthKey*2)+1] = $this->checkCellDataStatus('facility_rate', array('year' => $monthVal['current_year'], 'month' => $targetMonth));
                }
                if (is_null($lStatus[$monthKey*2])) {
                    $lStatus[$monthKey*2]     = $this->checkCellDataStatus('facility_rate', array('year' => $monthVal['last_year'], 'month' => $targetMonth));
                    $lStatus[($monthKey*2)+1] = $this->checkCellDataStatus('facility_rate', array('year' => $monthVal['last_year'], 'month' => $targetMonth));
                }
            }

            $commonRowDataArr = array();
            foreach ($cRowMonthCount as $cKey => $cVal) {
                if ($cVal===true) {
                    $commonRowDataArr[$cKey] = $commonRowDataArr[$cKey] + 1;
                    $cExRowMonthCount[$cKey] = $cExRowMonthCount[$cKey] + 1;
                }
            }
            foreach ($lRowMonthCount as $lKey => $lVal) {
                if ($lVal===true) {
                    $commonRowDataArr[$lKey] = $commonRowDataArr[$lKey] + 1;
                    $lExRowMonthCount[$lKey] = $lExRowMonthCount[$lKey] + 1;
                }
            }

            $cTotalData_pass       = 0;
            $lTotalData_pass       = 0;
            $cTotalData_disapprove = 0;
            $lTotalData_disapprove = 0;

            $compTotalData  = 0;
            $raitoTotalData = 0;

            $cColumnArr_pass = array();
            $lColumnArr_pass = array();
            $cColumnArr_disapprove = array();
            $lColumnArr_disapprove = array();

            for ($r=0; $r<=27; $r++) {
                $cCoord    = $this->coord($columnAlphaName[$startColumnNum+$r], $rowNum);
                $lCoord    = $this->coord($columnAlphaName[$startColumnNum+$r], $rowNum+1);
                $compCoord = $this->coord($columnAlphaName[$startColumnNum+$r], $rowNum+2);

                if ($r<=23) {
                    if (!bcmod($r, 2)) {
                        $cColumnArr_pass[] = $this->coord($columnAlphaName[$startColumnNum+$r], $rowNum);
                        $lColumnArr_pass[] = $this->coord($columnAlphaName[$startColumnNum+$r], $rowNum+1);
                    }
                    else {
                        $cColumnArr_disapprove[] = $this->coord($columnAlphaName[$startColumnNum+$r], $rowNum);
                        $lColumnArr_disapprove[] = $this->coord($columnAlphaName[$startColumnNum+$r], $rowNum+1);
                    }
                }

                switch ($r) {
                    default:
                        $compData[$r]  = $this->getExcelSubData($cCoord, $lCoord);
                        $raitoData[$r] = $this->getExcelDivData($compCoord, $lCoord);

                        break;

                    case '24': case '25':
                        for ($k=0; $k<=23; $k++) {
                            $cStatusArr[] = $cStatus[$i];
                            $lStatusArr[] = $lStatus[$i];
                        }

                        if (!bcmod($r, 2)) {
                            $cRowData[$r] = $this->getExcelSumDataByArray($cColumnArr_pass);
                            $lRowData[$r] = $this->getExcelSumDataByArray($lColumnArr_pass);
                        }
                        else {
                            $cRowData[$r] = $this->getExcelSumDataByArray($cColumnArr_disapprove);
                            $lRowData[$r] = $this->getExcelSumDataByArray($lColumnArr_disapprove);
                        }

                        $compData[$r]  = $this->getExcelSubData($cCoord,    $lCoord);
                        $raitoData[$r] = $this->getExcelDivData($compCoord, $lCoord);

                        $cStatus[$r] = $this->checkCellDataStatus('comp', $cStatusArr);
                        $lStatus[$r] = $this->checkCellDataStatus('comp', $lStatusArr);
                        break;
                        break;

                    case '26': case '27':
                        if (!bcmod($r, 2)) {
                            $cRowData[$r] = $this->getExcelAverageDataByArray($cColumnArr_pass, count($cRowMonthCount_pass), 1);
                            $lRowData[$r] = $this->getExcelAverageDataByArray($lColumnArr_pass, count($lRowMonthCount_pass), 1);
                        }
                        else {
                            $cRowData[$r] = $this->getExcelAverageDataByArray($cColumnArr_disapprove, count($cRowMonthCount_disapprove), 1);
                            $lRowData[$r] = $this->getExcelAverageDataByArray($lColumnArr_disapprove, count($lRowMonthCount_disapprove), 1);
                        }

                        $compData[$r]  = $this->getExcelSubData($cCoord, $lCoord);
                        $raitoData[$r] = $this->getExcelDivData($compCoord, $lCoord);

                        for ($i=0; $i<=23; $i++) {
                            $cStatusArr[] = $cStatus[$i];
                            $lStatusArr[] = $lStatus[$i];
                        }
                        $cStatus[$r] = $this->checkCellDataStatus('comp', $cStatusArr);
                        $lStatus[$r] = $this->checkCellDataStatus('comp', $lStatusArr);
                        break;
                }

                $compStatus[$r]    = $this->checkCellDataStatus('comp', array($cStatus[$r],     $lStatus[$r]));
                $raitoStatus[$r]   = $this->checkCellDataStatus('comp', array($compStatus[$r],  $lStatus[$r]));
                $exCStatus[$r]     = $this->checkCellDataStatus('comp', array($exCStatus[$r],   $cStatus[$r]));
                $exLStatus[$r]     = $this->checkCellDataStatus('comp', array($exLStatus[$r],   $lStatus[$r]));
                $exCompStatus[$r]  = $this->checkCellDataStatus('comp', array($compStatus[$r],  $exCompStatus[$r]));
                $exRaitoStatus[$r] = $this->checkCellDataStatus('comp', array($raitoStatus[$r], $exRaitoStatus[$r]));
            }

            for ($d=0; $d<=3; $d++) {
                $leftSide = array();
                $mainData = array();

                switch ($d) {
                    case '0':
                        $leftSide[0] = array('data' => $this->getArrValueByKey('facility_name', $plantVal), 'attr' => array($rowspan_4), 'style' => array('bgColor' => $bgColor_blue));
                        $leftSide[1] = array('data' => sprintf('%04d年度', $cYear));
                        for ($r=0; $r<=27; $r++) {
                            switch ($r) {
                                default:
                                    $mainData[$r] = array('data' => $cRowData[$r], 'status' => $cStatus[$r]);
                                    break;

                                case '26': case '27':
                                    $mainData[$r] = array('data' => $cRowData[$r], 'status' => $cStatus[$r], 'style' => array('msoNumberFormat' => $msoNumberFormat_0_0));
                                    break;
                            }
                        }
                        break;

                    case '1':
                        $leftSide[0] = array('data' => sprintf('%04d年度', $lYear));
                        for ($r=0; $r<=27; $r++) {
                            switch ($r) {
                                default:
                                    $mainData[$r] = array('data' => $lRowData[$r], 'status' => $lStatus[$r]);
                                    break;

                                case '26': case '27':
                                    $mainData[$r] = array('data' => $lRowData[$r], 'status' => $lStatus[$r], 'style' => array('msoNumberFormat' => $msoNumberFormat_0_0));
                                    break;
                            }
                        }
                        break;

                    case '2':
                        $leftSide[0] = array('data' => '増減', 'style' => array($bgColor_green));
                        for ($r=0; $r<=27; $r++) {
                            switch ($r) {
                                default:
                                    $mainData[$r] = array('data' => $compData[$r], 'style' => array($bgColor_green), 'status' => $compStatus[$r]);
                                    break;

                                case '26': case '27':
                                    $mainData[$r] = array('data' => $compData[$r], 'style' => array($bgColor_green, 'msoNumberFormat' => $msoNumberFormat_0_0), 'status' => $compStatus[$r]);
                                    break;
                            }
                        }
                        break;

                    case '3':
                        $leftSide[0] = array('data' => '増減率', 'style' => array($bgColor_green));
                        for ($r=0; $r<=27; $r++) { $mainData[$r] = array('data' => $raitoData[$r], 'style' => array($bgColor_green, 'msoNumberFormat' => $msoNumberFormat_percent), 'status' => $raitoStatus[$r]); }
                        break;
                }

                $rtnStr .= '<tr>'
                               .$this->convertCellData($leftSide, $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                               .$this->convertCellData($mainData, $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma))
                          .'</tr>';
            }
        }

        $cSideTotalCol_pass = array();
        $lSideTotalCol_pass = array();
        $cSideTotalCol_disapprove = array();
        $lSideTotalCol_disapprove = array();

        # AMG合計の施設合計と施設平均の取得
        for ($i=0; $i<=27; $i++) {
            $cTotalColNameArr = array();
            $lTotalColNameArr = array();

            foreach ($plantArr as $plantKey => $plantVal) {
                $cTotalColNameArr[] = $this->coord($columnAlphaName[$startColumnNum+$i], $startRowNum+($plantKey*4));
                $lTotalColNameArr[] = $this->coord($columnAlphaName[$startColumnNum+$i], $startRowNum+($plantKey*4)+1);
            }

            if ($i<=23) {
                if (!bcmod($i, 2)) {
                    $cSideTotalCol_pass[] = $this->coord($columnAlphaName[$startColumnNum+$i], $totalRowNum);
                    $lSideTotalCol_pass[] = $this->coord($columnAlphaName[$startColumnNum+$i], $totalRowNum+1);
                }
                else {
                    $cSideTotalCol_disapprove[] = $this->coord($columnAlphaName[$startColumnNum+$i], $totalRowNum);
                    $lSideTotalCol_disapprove[] = $this->coord($columnAlphaName[$startColumnNum+$i], $totalRowNum+1);
                }
            }

            $cCoord    = $this->coord($columnAlphaName[$startColumnNum+$i], $totalRowNum);
            $lCoord    = $this->coord($columnAlphaName[$startColumnNum+$i], $totalRowNum+1);
            $compCoord = $this->coord($columnAlphaName[$startColumnNum+$i], $totalRowNum+2);

            switch ($i) {
                default:
                    $totalRowCArr[$i] = $this->getExcelSumDataByArray($cTotalColNameArr);
                    $totalRowLArr[$i] = $this->getExcelSumDataByArray($lTotalColNameArr);

                    $totalRowCompArr[$i]  = $this->getExcelSubData($cCoord, $lCoord);
                    $totalRowRaitoArr[$i] = $this->getExcelDivData($compCoord, $lCoord);
                    break;

                case '24': case '25':
                    if (!bcmod($i, 2)) {
                        $totalRowCArr[$i] = $this->getExcelSumDataByArray($cSideTotalCol_pass);
                        $totalRowLArr[$i] = $this->getExcelSumDataByArray($lSideTotalCol_pass);
                    }
                    else {
                        $totalRowCArr[$i] = $this->getExcelSumDataByArray($cSideTotalCol_disapprove);
                        $totalRowLArr[$i] = $this->getExcelSumDataByArray($lSideTotalCol_disapprove);
                    }
                    $totalRowCompArr[$i]  = $this->getExcelSubData($cCoord, $lCoord);
                    $totalRowRaitoArr[$i] = $this->getExcelDivData($compCoord, $lCoord);
                    break;

                case '26': case '27':
                    if (!bcmod($i, 2)) {
                        $totalRowCArr[$i] = $this->getExcelAverageDataByArray($cSideTotalCol_pass, (count($cExRowMonthCount)/2));
                        $totalRowLArr[$i] = $this->getExcelAverageDataByArray($lSideTotalCol_pass, (count($lExRowMonthCount)/2));
                    }
                    else {
                        $totalRowCArr[$i] = $this->getExcelAverageDataByArray($cSideTotalCol_disapprove, (count($cExRowMonthCount)/2));
                        $totalRowLArr[$i] = $this->getExcelAverageDataByArray($lSideTotalCol_disapprove, (count($lExRowMonthCount)/2));
                    }

                    $totalRowCompArr[$i]  = $this->getExcelSubData($cCoord, $lCoord);
                    $totalRowRaitoArr[$i] = $this->getExcelDivData($compCoord, $lCoord);
                    break;
            }
        }

        # AMG合計行の出力
        for ($a=0; $a<=3; $a++) {
            $totalData     = array();
            $totalLeftSide = array();
            switch ($a) {
                case '0':
                    $totalLeftSide[0] = array('data' => 'ＡＭＧ合計', 'attr' => array($rowspan_4), 'style' => array('bgColor' => $bgColor_blue));
                    $totalLeftSide[1] = array('data' => sprintf('%04d年度', $cYear));
                    for ($r=0; $r<=27; $r++) {
                        switch ($r) {
                            default:
                            $totalData[$r] = array('data' => $totalRowCArr[$r], 'status' => $exCStatus[$r]);
                            break;

                            case '26': case '27':
                                $totalData[$r] = array('data' => $totalRowCArr[$r], 'status' => $exCStatus[$r], 'style' => array('msoNumberFormat' => $msoNumberFormat_0_0));
                            break;
                        }
                    }
                    break;

                case '1':
                    $totalLeftSide[0] = array('data' => sprintf('%04d年度', $lYear));
                    for ($r=0; $r<=27; $r++) {
                        switch ($r) {
                            default:
                                $totalData[$r] = array('data' => $totalRowLArr[$r], 'status' => $exLStatus[$r]);
                                break;

                            case '26': case '27':
                                $totalData[$r] = array('data' => $totalRowLArr[$r], 'status' => $exLStatus[$r], 'style' => array('msoNumberFormat' => $msoNumberFormat_0_0));
                                break;
                        }
                    }
                    break;

                case '2':
                    $totalLeftSide[0] = array('data' => '増減', 'style' => array($bgColor_green));
                    for ($r=0; $r<=27; $r++) {
                        switch ($r) {
                            default:
                                $totalData[$r] = array('data' => $totalRowCompArr[$r], 'style' => array($bgColor_green), 'status' => $exCompStatus[$r]);
                                break;

                            case '26': case '27':
                                $totalData[$r] = array('data' => $totalRowCompArr[$r], 'style' => array($bgColor_green, 'msoNumberFormat' => $msoNumberFormat_0_0), 'status' => $exCompStatus[$r]);
                                break;
                        }
                    }

                    break;

                case '3':
                    $totalLeftSide[0] = array('data' => '増減率', 'style' => array($bgColor_green));
                    for ($r=0; $r<=27; $r++) { $totalData[$r] = array('data' => $totalRowRaitoArr[$r], 'style' => array($bgColor_green, 'msoNumberFormat' => $msoNumberFormat_percent), 'status' => $exRaitoStatus[$r]); }
                    break;
            }

            $rtnStr .= '<tr>'
                          .$this->convertCellData($totalLeftSide, $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($totalData, $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma))
                      .'</tr>';
        }

        $rtnStr .= '</table>';
        return $rtnStr;
    }

    /**
     * No.121 [老健] 在宅復帰率のエクセル出力
     * @param  array $dataArr
     * @param  array $plantArr
     * @param  array $arr
     * @param  array $optionArr
     * @return string
     */
    function outputExcelHomeReturnRate($dataArr, $plantArr, $arr, $optionArr)
    {
        $rtnStr  = $this->getExcelMeta();
        $fontTag = $this->getFontTag();

        $raitoFlg     = $this->getArrValueByKey('raito',        $arr);
        $sideTotalFlg = $this->getArrValueByKey('side_total',   $arr);
        $sideAvrgFlg  = $this->getArrValueByKey('side_average', $arr);
        $amgTotalFlg  = $this->getArrValueByKey('amg_total',    $arr);
        $amgAvrgFlg   = $this->getArrValueByKey('amg_average',  $arr);
        $percentFlg   = $this->getArrValueByKey('percent',      $arr);

        # attr
        $colspan_2 = array('name' => 'colspan', 'value' => 2);
        $rowspan_3 = array('name' => 'rowspan', 'value' => 3);
        $rowspan_4 = array('name' => 'rowspan', 'value' => 4);

        $rowspan   = ($raitoFlg==true)? $rowspan_4 : $rowspan_3;
        $repertNum = ($raitoFlg==true)? 3 : 2;

        $mainDataColNum = 11;
        $sideTotalColNum = false;
        $sideAvrgColNum  = false;

        # style
        $bgColor_blue            = array('name' => 'background-color',  'value' => $this->getRgbColor('blue'));
        $bgColor_green           = array('name' => 'background-color',  'value' => $this->getRgbColor('green'));
        $textAlign_center        = array('name' => 'text-align',        'value' => 'center');
        $textAlign_right         = array('name' => 'text-align',        'value' => 'right');
        $whiteSpace_nowrap       = array('name' => 'white-space',       'value' => 'nowrap');
        $msoNumberFormat_percent = $this->getMsoNumberFormat('percent');
        $msoNumberFormat_0_0     = $this->getMsoNumberFormat('decimal');

        $cYear      = $this->getArrValueByKey('year', $optionArr);
        $lYear      = $cYear - 1;
        $startMonth = $this->getFiscalYearStartMonth();

        $monthArr        = array();
        $monthNameArr    = array();
        $monthNameArr[0] = array('data' => '&nbsp;', 'attr' => array($colspan_2));
        for ($i=0; $i<=$mainDataColNum; $i++) {
            $tmpMonth = $startMonth + $i;
            $monthArr[$i]['current_year'] = ($tmpMonth <= 12)? $cYear : $cYear+1;
            $monthArr[$i]['last_year']    = ($tmpMonth <= 12)? $lYear : $lYear+1;
            $monthArr[$i]['target_month'] = ($tmpMonth <= 12)? $tmpMonth : $tmpMonth-12;
            $monthNameArr[] = array('data' => $monthArr[$i]['target_month'].'月');
        }

        if (!is_null($sideTotalFlg)) {
            $monthNameArr[] = array('data' => '合計');
            $mainDataColNum++;
            $sideTotalColNum = $mainDataColNum;
        }

        if (!is_null($sideAvrgFlg)) {
            $monthNameArr[] = array('data' => '平均');
            $mainDataColNum++;
            $sideAvrgColNum = $mainDataColNum;
        }

        $rtnStr .= '<table class="list" width="100%" border="1" >'
                      .'<tr>'
                          .$this->convertCellData($monthNameArr, $fontTag, array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                      .'</tr>';

        $totalRowCAvrgData     = 0;
        $totalRowLAvrgData     = 0;
        $totalRowCompAvrgData  = 0;
        $totalRowRaitoAvrgData = 0;

        $cExRowMonthCount = array();
        $lExRowMonthCount = array();

        $avrgRowCAvrgData     = 0;
        $avrgRowLAvrgData     = 0;
        $avrgRowCompAvrgData  = 0;
        $avrgRowRaitoAvrgData = 0;

        $exCStatus     = array();
        $exLStatus     = array();
        $exCompStatus  = array();
        $exRaitoStatus = array();

        $startRowNum     = 2;
        $startColumnNum  = 3;

        if ($amgTotalFlg===true) {
            $amgTotalRowNum = $startRowNum + (count($plantArr)*($repertNum+1));
            if ($amgAvrgFlg===true) { $amgAvrgRowNum  = $startRowNum + ((count($plantArr)+1)*($repertNum+1)); }
        }
        else if ($amgAvrgFlg===true) { $amgAvrgRowNum  = $startRowNum + (count($plantArr)*($repertNum+1)); }

        $columnAlphaName = $this->columnAlphaNameArr;

        foreach ($plantArr as $plantVKey => $plantVal) {
            $rowNum = $startRowNum+($plantVKey*($repertNum+1));
            $facilityId = $this->getArrValueByKey('facility_id', $plantVal);

            for ($r=0; $r<=$mainDataColNum; $r++) {
                $cRowData[$r]  = null;
                $lRowData[$r]  = null;
                $compData[$r]  = null;
                $raitoData[$r] = null;
                $cStatus[$r]   = null;
                $lStatus[$r]   = null;
            }

            $cRowMonthCount = array();
            $lRowMonthCount = array();

            foreach ($monthArr as $monthKey => $monthVal) {
                $targetMonth = $this->getArrValueByKey('target_month', $monthVal);
                foreach ($dataArr as $dataVal) {
                    $targetData = null;
                    if ($facilityId == $this->getArrValueByKey('jnl_facility_id', $dataVal) && $targetMonth == $this->getArrValueByKey('month', $dataVal) ) {
                        if ($this->getArrValueByKey('current_year', $monthVal) == $this->getArrValueByKey('year', $dataVal)) {
                            # 表示年度データ
                            $targetData = $this->getArrValueByKey('target_data', $dataVal);
                            $cRowData[$monthKey] = $targetData;
                            $cRowMonthCount[$monthKey] = true;
                            $cStatus[$monthKey] = $this->checkCellDataStatus('facility_rate', $dataVal);
                        }
                        else if ($this->getArrValueByKey('last_year', $monthVal) == $this->getArrValueByKey('year', $dataVal)) {
                            # 前年度データ
                            $targetData = $this->getArrValueByKey('target_data', $dataVal);
                            $lRowData[$monthKey] = $targetData;
                            $lRowMonthCount[$monthKey] = true;
                            $lStatus[$monthKey] = $this->checkCellDataStatus('facility_rate', $dataVal);
                        }
                    }
                }
                if (is_null($cStatus[$monthKey])) { $cStatus[$monthKey] = $this->checkCellDataStatus('facility_rate', array('year' => $monthVal['current_year'], 'month' => $targetMonth)); }
                if (is_null($lStatus[$monthKey])) { $lStatus[$monthKey] = $this->checkCellDataStatus('facility_rate', array('year' => $monthVal['last_year'], 'month' => $targetMonth)); }
            }

            $commonRowDataArr = array();
            foreach ($cRowMonthCount as $cKey => $cVal) {
                if ($cVal===true) {
                    $commonRowDataArr[$cKey] = $commonRowDataArr[$cKey] + 1;
                    $cExRowMonthCount[$cKey] = $cExRowMonthCount[$cKey] + 1;
                }
            }
            foreach ($lRowMonthCount as $lKey => $lVal) {
                if ($lVal===true) {
                    $commonRowDataArr[$lKey] = $commonRowDataArr[$lKey] + 1;
                    $lExRowMonthCount[$lKey] = $lExRowMonthCount[$lKey] + 1;
                }
            }

            $cTotalData     = 0;
            $lTotalData     = 0;
            $compTotalData  = 0;
            $raitoTotalData = 0;

            for ($r=0; $r<=$mainDataColNum; $r++) {
                $cCoord    = $this->coord($columnAlphaName[$startColumnNum+$r], $rowNum);
                $lCoord    = $this->coord($columnAlphaName[$startColumnNum+$r], $rowNum+1);
                $compCoord = $this->coord($columnAlphaName[$startColumnNum+$r], $rowNum+2);
                switch ($r) {
                    default: case '':
                        $compData[$r]  = $this->getExcelSubData($cCoord, $lCoord, 3);
                        $raitoData[$r] = $this->getExcelDivData($compCoord, $lCoord, 3);
                        break;

                    case $sideTotalColNum:
                        if ($sideTotalColNum!==false) {
                            $cRowData[$r]  = $this->getExcelSideSumData($columnAlphaName[$startColumnNum], $columnAlphaName[$startColumnNum+11], $rowNum, 1);
                            $lRowData[$r]  = $this->getExcelSideSumData($columnAlphaName[$startColumnNum], $columnAlphaName[$startColumnNum+11], $rowNum+1, 1);
                            $compData[$r]  = $this->getExcelSubData($cCoord,    $lCoord, 3);
                            $raitoData[$r] = $this->getExcelDivData($compCoord, $lCoord, 3);

                            for ($i=0; $i<$mainDataColNum; $i++) {
                                $cStatusArr[] = $cStatus[$i];
                                $lStatusArr[] = $lStatus[$i];
                            }
                            $cStatus[$r] = $this->checkCellDataStatus('comp', $cStatusArr);
                            $lStatus[$r] = $this->checkCellDataStatus('comp', $lStatusArr);
                        }
                        break;

                    case $sideAvrgColNum:
                        if ($sideAvrgColNum!==false) {
                            $cRowData[$r]  = $this->getExcelSideAverageData($columnAlphaName[$startColumnNum], $columnAlphaName[$startColumnNum+11], $rowNum,   count($cRowMonthCount), 3);
                            $lRowData[$r]  = $this->getExcelSideAverageData($columnAlphaName[$startColumnNum], $columnAlphaName[$startColumnNum+11], $rowNum+1, count($lRowMonthCount), 3);
                            $compData[$r]  = $this->getExcelSubData($cCoord, $lCoord, 3);
                            $raitoData[$r] = $this->getExcelDivData($compCoord, $lCoord, 3);

                            for ($i=0; $i<$mainDataColNum; $i++) {
                                $cStatusArr[] = $cStatus[$i];
                                $lStatusArr[] = $lStatus[$i];
                            }
                            $cStatus[$r] = $this->checkCellDataStatus('comp', $cStatusArr);
                            $lStatus[$r] = $this->checkCellDataStatus('comp', $lStatusArr);
                        }
                        break;
                }

                $compStatus[$r]    = $this->checkCellDataStatus('comp', array($cStatus[$r],       $lStatus[$r]));
                $raitoStatus[$r]   = $this->checkCellDataStatus('comp', array($compStatus[$r],    $lStatus[$r]));
                $exCStatus[$r]     = $this->checkCellDataStatus('comp', array($exCStatus[$r],     $cStatus[$r]));
                $exLStatus[$r]     = $this->checkCellDataStatus('comp', array($exLStatus[$r],     $lStatus[$r]));
                $exCompStatus[$r]  = $this->checkCellDataStatus('comp', array($exCompStatus[$r],  $compStatus[$r]));
                $exRaitoStatus[$r] = $this->checkCellDataStatus('comp', array($exRaitoStatus[$r], $raitoStatus[$r]));
            }
            for ($d=0; $d<=$repertNum; $d++) {
                $leftSide = array();
                $mainData = array();

                switch ($d) {
                    case '0':
                        $leftSide[0] = array('data' => $this->getArrValueByKey('facility_name', $plantVal), 'attr' => array($rowspan), 'style' => array('bgColor' => $bgColor_blue));
                        $leftSide[1] = array('data' => sprintf('%04d年度', $cYear));
                        for ($r=0; $r<=$mainDataColNum; $r++) {
                            switch ($r) {
                                default: case '0':
                                    if (!is_null($percentFlg)) { $mainData[$r] = array('data' => ($cRowData[$r]/100)? ($cRowData[$r]/100) : null, 'status' => $cStatus[$r], 'style' => array($msoNumberFormat_percent), 'disp_type' => 'exception'); }
                                    else { $mainData[$r] = array('data' => $cRowData[$r], 'status' => $cStatus[$r], 'disp_type' => 'exception'); }
                                    break;

                                case '12': # 合計
                                    if (!is_null($percentFlg)) { $mainData[$r] = array('data' => $cRowData[$r], 'status' => $cStatus[$r], 'style' => array($msoNumberFormat_percent), 'disp_type' => 'exception'); }
                                    else { $mainData[$r] = array('data' => $cRowData[$r], 'status' => $cStatus[$r], 'disp_type' => 'exception'); }
                                    break;

                                case '13': # 平均
                                    if (!is_null($percentFlg)) { $mainData[$r] = array('data' => $cRowData[$r], 'status' => $cStatus[$r], 'style' => array($msoNumberFormat_percent), 'disp_type' => 'exception'); }
                                    else { $mainData[$r] = array('data' => $cRowData[$r], 'status' => $cStatus[$r], 'style' => array($msoNumberFormat_0_0), 'disp_type' => 'exception'); }
                                    break;

                            }
                        }
                        break;

                    case '1':
                        $leftSide[0] = array('data' => sprintf('%04d年度', $lYear));
                        for ($r=0; $r<=$mainDataColNum; $r++) {
                            switch ($r) {
                                default: case '0':
                                    if (!is_null($percentFlg)) { $mainData[$r] = array('data' => ($lRowData[$r]/100)? ($lRowData[$r]/100) : null, 'status' => $lStatus[$r], 'style' => array($msoNumberFormat_percent), 'disp_type' => 'exception'); }
                                    else { $mainData[$r] = array('data' => $lRowData[$r], 'status' => $lStatus[$r], 'disp_type' => 'exception'); }
                                    break;

                                case '12':
                                    if (!is_null($percentFlg)) { $mainData[$r] = array('data' => $lRowData[$r], 'status' => $lStatus[$r], 'style' => array($msoNumberFormat_percent), 'disp_type' => 'exception'); }
                                    else { $mainData[$r] = array('data' => $lRowData[$r], 'status' => $lStatus[$r], 'disp_type' => 'exception'); }
                                    break;

                                case '13':
                                    if (!is_null($percentFlg)) { $mainData[$r] = array('data' => $lRowData[$r], 'status' => $lStatus[$r], 'style' => array($msoNumberFormat_percent), 'disp_type' => 'exception'); }
                                    else { $mainData[$r] = array('data' => $lRowData[$r], 'status' => $lStatus[$r], 'style' => array($msoNumberFormat_0_0), 'disp_type' => 'exception'); }
                                    break;
                            }
                        }
                        break;

                    case '2':
                        $leftSide[0] = array('data' => '増減', 'style' => array($bgColor_green));
                        for ($r=0; $r<=$mainDataColNum; $r++) {
                            if (!is_null($percentFlg)) { $mainData[$r] = array('data' => $compData[$r], 'style' => array($bgColor_green, $msoNumberFormat_percent), 'status' => $compStatus[$r], 'disp_type' => 'exception'); }
                            else { $mainData[$r] = array('data' => $compData[$r], 'style' => array($bgColor_green), 'status' => $compStatus[$r], 'disp_type' => 'exception'); }
                        }
                        break;

                    case '3':
                        $leftSide[0] = array('data' => '増減率', 'style' => array($bgColor_green), 'disp_type' => 'exception');
                        for ($r=0; $r<=$mainDataColNum; $r++) { $mainData[$r] = array('data' => $raitoData[$r], 'style' => array($bgColor_green, $msoNumberFormat_percent), 'status' => $raitoStatus[$r], 'disp_type' => 'exception'); }
                        break;
                }

                $rtnStr .= '<tr>'
                               .$this->convertCellData($leftSide, $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                               .$this->convertCellData($mainData, $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap))
                          .'</tr>';
            }
        }

        $totalRowCAvrg = 0;
        $totalRowLAvrg = 0;
        $avrgRowCAvrg  = 0;
        $avrgRowLAvrg  = 0;

        # AMG合計・AMG平均の施設平均の取得
        for ($i=0; $i<=$mainDataColNum; $i++) {
            $targetColumn = $columnAlphaName[$startColumnNum+$i];
            $cColumnArr   = array();
            $lColumnArr   = array();
            foreach ($plantArr as $plantKey => $plantVal) {
                $cColumnArr[] = $this->coord($targetColumn, $startRowNum+($plantKey*($repertNum+1)));
                $lColumnArr[] = $this->coord($targetColumn, $startRowNum+($plantKey*($repertNum+1))+1);
            }

            # AMG合計の施設平均の取得
            if (!is_null($amgTotalFlg)) {
                switch ($i) {
                    default: case '0':
                        $totalRowCArr[$i]     = $this->getExcelSumDataByArray($cColumnArr);
                        $totalRowLArr[$i]     = $this->getExcelSumDataByArray($lColumnArr);
                        $totalRowCompArr[$i]  = $this->getExcelSubData($this->coord($columnAlphaName[$startColumnNum+$i], $amgTotalRowNum), $this->coord($columnAlphaName[$startColumnNum+$i], $amgTotalRowNum+1), 3);
                        $totalRowRaitoArr[$i] = $this->getExcelDivData($this->coord($columnAlphaName[$startColumnNum+$i], $amgTotalRowNum+2), $this->coord($columnAlphaName[$startColumnNum+$i], $amgTotalRowNum+1), 3);
                        break;

                    # 施設合計
                    case $sideTotalColNum:
                        if ($sideTotalColNum!==false) {
                            $totalRowCArr[$i]     = $this->getExcelSideSumData($columnAlphaName[$startColumnNum], $columnAlphaName[$startColumnNum+11], $amgTotalRowNum, 1);
                            $totalRowLArr[$i]     = $this->getExcelSideSumData($columnAlphaName[$startColumnNum], $columnAlphaName[$startColumnNum+11], $amgTotalRowNum+1, 1);
                            $totalRowCompArr[$i]  = $this->getExcelSubData($this->coord($columnAlphaName[$startColumnNum+$i], $amgTotalRowNum), $this->coord($columnAlphaName[$startColumnNum+$i], $amgTotalRowNum+1), 3);
                            $totalRowRaitoArr[$i] = $this->getExcelDivData($this->coord($columnAlphaName[$startColumnNum+$i], $amgTotalRowNum+2), $this->coord($columnAlphaName[$startColumnNum+$i], $amgTotalRowNum+1), 3);
                        }
                        break;

                    # 施設平均
                    case $sideAvrgColNum:
                        if ($sideAvrgColNum!==false) {
                            $totalRowCArr[$i]     = $this->getExcelSideAverageData($columnAlphaName[$startColumnNum], $columnAlphaName[$startColumnNum+11], $amgTotalRowNum, count($cExRowMonthCount), 1);
                            $totalRowLArr[$i]     = $this->getExcelSideAverageData($columnAlphaName[$startColumnNum], $columnAlphaName[$startColumnNum+11], $amgTotalRowNum+1, count($lExRowMonthCount), 1);
                            $totalRowCompArr[$i]  = $this->getExcelSubData($this->coord($columnAlphaName[$startColumnNum+$i], $amgTotalRowNum), $this->coord($columnAlphaName[$startColumnNum+$i], $amgTotalRowNum+1), 3);
                            $totalRowRaitoArr[$i] = $this->getExcelDivData($this->coord($columnAlphaName[$startColumnNum+$i], $amgTotalRowNum+2), $this->coord($columnAlphaName[$startColumnNum+$i], $amgTotalRowNum+1), 3);
                        }
                        break;
                }
            }
            # AMG平均の施設平均の取得
            if (!is_null($amgAvrgFlg)) {
                switch ($i) {
                    default: case '0':
                        $avrgRowCArr[$i]     = $this->getExcelAverageDataByArray($cColumnArr, $cExRowMonthCount[$i]);
                        $avrgRowLArr[$i]     = $this->getExcelAverageDataByArray($lColumnArr, $lExRowMonthCount[$i]);
                        $avrgRowCompArr[$i]  = $this->getExcelSubData($this->coord($columnAlphaName[$startColumnNum+$i], $amgAvrgRowNum), $this->coord($columnAlphaName[$startColumnNum+$i], $amgAvrgRowNum+1), 3);
                        $avrgRowRaitoArr[$i] = $this->getExcelDivData($this->coord($columnAlphaName[$startColumnNum+$i], $amgAvrgRowNum+2), $this->coord($columnAlphaName[$startColumnNum+$i], $amgAvrgRowNum+1), 3);
                        break;

                    case $sideTotalColNum:
                        if ($sideTotalColNum!==false) {
                            $avrgRowCArr[$i]     = $this->getExcelSideSumData($columnAlphaName[$startColumnNum], $columnAlphaName[$startColumnNum+11], $amgAvrgRowNum, 1);
                            $avrgRowLArr[$i]     = $this->getExcelSideSumData($columnAlphaName[$startColumnNum], $columnAlphaName[$startColumnNum+11], $amgAvrgRowNum+1, 1);
                            $avrgRowCompArr[$i]  = $this->getExcelSubData($this->coord($columnAlphaName[$startColumnNum+$i], $amgAvrgRowNum), $this->coord($columnAlphaName[$startColumnNum+$i], $amgAvrgRowNum+1), 3);
                            $avrgRowRaitoArr[$i] = $this->getExcelDivData($this->coord($columnAlphaName[$startColumnNum+$i], $amgAvrgRowNum+2), $this->coord($columnAlphaName[$startColumnNum+$i], $amgAvrgRowNum+1), 3);
                        }
                        break;

                    case $sideAvrgColNum:
                        if ($sideAvrgColNum!==false) {
                            $avrgRowCArr[$i]     = $this->getExcelSideAverageData($columnAlphaName[$startColumnNum], $columnAlphaName[$startColumnNum+11], $amgAvrgRowNum, count($cExRowMonthCount), 3);
                            $avrgRowLArr[$i]     = $this->getExcelSideAverageData($columnAlphaName[$startColumnNum], $columnAlphaName[$startColumnNum+11], $amgAvrgRowNum+1, count($lExRowMonthCount), 3);
                            $avrgRowCompArr[$i]  = $this->getExcelSubData($this->coord($columnAlphaName[$startColumnNum+$i], $amgAvrgRowNum), $this->coord($columnAlphaName[$startColumnNum+$i], $amgAvrgRowNum+1), 3);
                            $avrgRowRaitoArr[$i] = $this->getExcelDivData($this->coord($columnAlphaName[$startColumnNum+$i], $amgAvrgRowNum+2), $this->coord($columnAlphaName[$startColumnNum+$i], $amgAvrgRowNum+1), 3);
                        }
                        break;
                }
            }
        }

        # AMG合計・AMG平均行の出力
        for ($a=0; $a<=$repertNum; $a++) {
            # AMG合計行の出力
            if (!is_null($amgTotalFlg)) {
                $totalData     = array();
                $totalLeftSide = array();
                switch ($a) {
                    case '0':
                        $totalLeftSide[0] = array('data' => 'ＡＭＧ合計', 'attr' => array($rowspan), 'style' => array('bgColor' => $bgColor_blue));
                        $totalLeftSide[1] = array('data' => sprintf('%04d年度', $cYear));
                        for ($r=0; $r<=$mainDataColNum; $r++) {
                            switch ($r) {
                                default: case '':
                                    if (!is_null($percentFlg)) { $totalData[$r] = array('data' => $totalRowCArr[$r], 'status' => $exCStatus[$r], 'style' => array($msoNumberFormat_percent)); }
                                    else { $totalData[$r] = array('data' => $totalRowCArr[$r], 'status' => $exCStatus[$r]); }
                                    break;

                                case $sideAvrgColNum:
                                    if (!is_null($percentFlg)) { $totalData[$r] = array('data' => $totalRowCArr[$r], 'status' => $exCStatus[$r], 'style' => array($msoNumberFormat_0_0)); }
                                    else { $totalData[$r] = array('data' => $totalRowCArr[$r], 'status' => $exCStatus[$r], 'style' => array($msoNumberFormat_0_0)); }
                                    break;
                            }
                        }
                        break;

                    case '1':
                        $totalLeftSide[0] = array('data' => sprintf('%04d年度', $lYear));
                        for ($r=0; $r<=$mainDataColNum; $r++) {
                            switch ($r) {
                                default:
                                    if (!is_null($percentFlg)) { $totalData[$r] = array('data' => $totalRowLArr[$r], 'status' => $exLStatus[$r], 'style' => array($msoNumberFormat_percent)); }
                                    else { $totalData[$r] = array('data' => $totalRowLArr[$r], 'status' => $exLStatus[$r]); }
                                    break;

                                case $sideAvrgColNum:
                                    if (!is_null($percentFlg)) { $totalData[$r] = array('data' => $totalRowLArr[$r], 'status' => $exLStatus[$r], 'style' => array($msoNumberFormat_percent)); }
                                    else { $totalData[$r] = array('data' => $totalRowLArr[$r], 'status' => $exLStatus[$r], 'style' => array($msoNumberFormat_0_0)); }
                                    break;
                            }
                        }
                        break;

                    case '2':
                        $totalLeftSide[0] = array('data' => '増減', 'style' => array($bgColor_green));
                        for ($r=0; $r<=$mainDataColNum; $r++) {
                            switch ($r) {
                                default: case '':
                                    if (!is_null($percentFlg)) { $totalData[$r] = array('data' => $totalRowCompArr[$r], 'style' => array($bgColor_green, $msoNumberFormat_percent), 'status' => $exCompStatus[$r]); }
                                    else { $totalData[$r] = array('data' => $totalRowCompArr[$r], 'style' => array($bgColor_green), 'status' => $exCompStatus[$r]); }
                                    break;

                                case $sideAvrgColNum:
                                    if (!is_null($percentFlg)) { $totalData[$r] = array('data' => $totalRowCompArr[$r], 'style' => array($bgColor_green, $msoNumberFormat_0_0), 'status' => $exCompStatus[$r]); }
                                    else { $totalData[$r] = array('data' => $totalRowCompArr[$r], 'style' => array($bgColor_green), 'status' => $exCompStatus[$r]); }
                                    break;
                            }
                        }
                        break;

                    case '3':
                        $totalLeftSide[0] = array('data' => '増減率', 'style' => array($bgColor_green));
                        for ($r=0; $r<=$mainDataColNum; $r++) { $totalData[$r] = array('data' => $totalRowRaitoArr[$r], 'style' => array($bgColor_green, $msoNumberFormat_percent), 'status' => $exRaitoStatus[$r]); }
                        break;
                }

                $rtnStr .= '<tr>'
                              .$this->convertCellData($totalLeftSide, $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                              .$this->convertCellData($totalData, $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap))
                          .'</tr>';
            }
            # AMG平均行の出力
            if (!is_null($amgAvrgFlg)) {
                $avrgData     = array();
                $avrgLeftSide = array();
                switch ($a) {
                    case '0':
                        $avrgLeftSide[0] = array('data' => 'ＡＭＧ平均', 'attr' => array($rowspan), 'style' => array('bgColor' => $bgColor_blue));
                        $avrgLeftSide[1] = array('data' => sprintf('%04d年度', $cYear));
                        for ($r=0; $r<=$mainDataColNum; $r++) {
                            if (!is_null($percentFlg)) { $avrgData[$r] = array('data' => $avrgRowCArr[$r], 'status' => $exCStatus[$r], 'style' => array($msoNumberFormat_percent)); }
                            else { $avrgData[$r] = array('data' => $avrgRowCArr[$r], 'status' => $exCStatus[$r]); }
                        }
                        break;

                    case '1':
                        $avrgLeftSide[0] = array('data' => sprintf('%04d年度', $lYear));
                        for ($r=0; $r<=$mainDataColNum; $r++) {
                            if (!is_null($percentFlg)) { $avrgData[$r] = array('data' => $avrgRowLArr[$r], 'status' => $exLStatus[$r], 'style' => array($msoNumberFormat_percent)); }
                            else { $avrgData[$r] = array('data' => $avrgRowLArr[$r], 'status' => $exLStatus[$r]); }
                        }
                        break;

                    case '2':
                        $avrgLeftSide[0] = array('data' => '増減', 'style' => array($bgColor_green));
                        for ($r=0; $r<=$mainDataColNum; $r++) {
                            if (!is_null($percentFlg)) { $avrgData[$r] = array('data' => $avrgRowCompArr[$r], 'style' => array($bgColor_green, $msoNumberFormat_percent), 'status' => $exCompStatus[$r]); }
                            else { $avrgData[$r] = array('data' => $avrgRowCompArr[$r], 'style' => array($bgColor_green), 'status' => $exCompStatus[$r]); }
                        }
                        break;

                    case '3':
                        $avrgLeftSide[0] = array('data' => '増減率', 'style' => array($bgColor_green));
                        for ($r=0; $r<=$mainDataColNum; $r++) { $avrgData[$r] = array('data' => $avrgRowRaitoArr[$r], 'style' => array($bgColor_green, $msoNumberFormat_percent), 'status' => $exRaitoStatus[$r]); }
                        break;
                }

                $rtnStr .= '<tr>'
                              .$this->convertCellData($avrgLeftSide, $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                              .$this->convertCellData($avrgData, $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap))
                          .'</tr>';
            }
        }

        $rtnStr .= '</table>';
        return $rtnStr;
    }

    /**
     * No.123 [老健] 営業件数のエクセル出力
     * @param  array $dataArr
     * @param  array $plantArr
     * @param  array $arr
     * @param  array $optionArr
     * @return string
     */
    function outputExcelOtherCount($dataArr, $plantArr, $arr, $optionArr)
    {
        $rtnStr  = $this->getExcelMeta();
        $fontTag = $this->getFontTag();

        $raitoFlg     = $this->getArrValueByKey('raito',        $arr);
        $sideTotalFlg = $this->getArrValueByKey('side_total',   $arr);
        $sideAvrgFlg  = $this->getArrValueByKey('side_average', $arr);
        $amgTotalFlg  = $this->getArrValueByKey('amg_total',    $arr);
        $amgAvrgFlg   = $this->getArrValueByKey('amg_average',  $arr);
        $percentFlg   = $this->getArrValueByKey('percent',      $arr);

        # attr
        $colspan_2 = array('name' => 'colspan', 'value' => 2);
        $rowspan_3 = array('name' => 'rowspan', 'value' => 3);
        $rowspan_4 = array('name' => 'rowspan', 'value' => 4);

        $rowspan   = ($raitoFlg==true)? $rowspan_4 : $rowspan_3;
        $repertNum = ($raitoFlg==true)? 3 : 2;

        $mainDataColNum = 11;
        $sideTotalColNum = false;
        $sideAvrgColNum  = false;

        # style
        $bgColor_blue            = array('name' => 'background-color',  'value' => $this->getRgbColor('blue'));
        $bgColor_green           = array('name' => 'background-color',  'value' => $this->getRgbColor('green'));
        $textAlign_center        = array('name' => 'text-align',        'value' => 'center');
        $textAlign_right         = array('name' => 'text-align',        'value' => 'right');
        $whiteSpace_nowrap       = array('name' => 'white-space',       'value' => 'nowrap');
        $msoNumberFormat_percent = $this->getMsoNumberFormat('percent');
        $msoNumberFormat_0_0     = $this->getMsoNumberFormat('decimal');
        $msoNumberFormat_comma   = $this->getMsoNumberFormat('comma');
//        $msoNumberFormat_percent = array('name' => 'mso-number-format', 'value' => '0.0%');
//        $msoNumberFormat_0_0     = array('name' => 'mso-number-format', 'value' => '0.0');

        $cYear      = $this->getArrValueByKey('year', $optionArr);
        $lYear      = $cYear - 1;
        $startMonth = $this->getFiscalYearStartMonth();

        $monthArr        = array();
        $monthNameArr    = array();
        $monthNameArr[0] = array('data' => '&nbsp;', 'attr' => array($colspan_2));
        for ($i=0; $i<=$mainDataColNum; $i++) {
            $tmpMonth = $startMonth + $i;
            $monthArr[$i]['current_year'] = ($tmpMonth <= 12)? $cYear : $cYear+1;
            $monthArr[$i]['last_year']    = ($tmpMonth <= 12)? $lYear : $lYear+1;
            $monthArr[$i]['target_month'] = ($tmpMonth <= 12)? $tmpMonth : $tmpMonth-12;
            $monthNameArr[] = array('data' => $monthArr[$i]['target_month'].'月');
        }

        if (!is_null($sideTotalFlg)) {
            $monthNameArr[] = array('data' => '合計');
            $mainDataColNum++;
            $sideTotalColNum = $mainDataColNum;
        }

        if (!is_null($sideAvrgFlg)) {
            $monthNameArr[] = array('data' => '平均');
            $mainDataColNum++;
            $sideAvrgColNum = $mainDataColNum;
        }

        $rtnStr .= '<table class="list" width="100%" border="1" >'
                      .'<tr>'
                          .$this->convertCellData($monthNameArr, $fontTag, array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                      .'</tr>';

        $totalRowCAvrgData     = 0;
        $totalRowLAvrgData     = 0;
        $totalRowCompAvrgData  = 0;
        $totalRowRaitoAvrgData = 0;

        $cExRowMonthCount = array();
        $lExRowMonthCount = array();

        $avrgRowCAvrgData     = 0;
        $avrgRowLAvrgData     = 0;
        $avrgRowCompAvrgData  = 0;
        $avrgRowRaitoAvrgData = 0;

        $exCStatus     = array();
        $exLStatus     = array();
        $exCompStatus  = array();
        $exRaitoStatus = array();

        $startRowNum     = 2;
        $startColumnNum  = 3;

        if ($amgTotalFlg===true) {
            $amgTotalRowNum = $startRowNum + (count($plantArr)*($repertNum+1));
            if ($amgAvrgFlg===true) { $amgAvrgRowNum  = $startRowNum + ((count($plantArr)+1)*($repertNum+1)); }
        }
        else if ($amgAvrgFlg===true) { $amgAvrgRowNum  = $startRowNum + (count($plantArr)*($repertNum+1)); }

        $columnAlphaName = $this->columnAlphaNameArr;

        foreach ($plantArr as $plantVKey => $plantVal) {
            $rowNum = $startRowNum+($plantVKey*($repertNum+1));
            $facilityId = $this->getArrValueByKey('facility_id', $plantVal);

            for ($r=0; $r<=$mainDataColNum; $r++) {
                $cRowData[$r]  = null;
                $lRowData[$r]  = null;
                $compData[$r]  = null;
                $raitoData[$r] = null;
                $cStatus[$r]   = null;
                $lStatus[$r]   = null;
            }

            $cRowMonthCount = array();
            $lRowMonthCount = array();

            foreach ($monthArr as $monthKey => $monthVal) {
                $targetMonth = $this->getArrValueByKey('target_month', $monthVal);
                foreach ($dataArr as $dataVal) {
                    $targetData = null;
                    if ($facilityId == $this->getArrValueByKey('jnl_facility_id', $dataVal) && $targetMonth == $this->getArrValueByKey('month', $dataVal) ) {
                        if ($this->getArrValueByKey('current_year', $monthVal) == $this->getArrValueByKey('year', $dataVal)) {
                            # 表示年度データ
                            $targetData = $this->getArrValueByKey('target_data', $dataVal);
                            $cRowData[$monthKey] = $targetData;
                            $cRowMonthCount[$monthKey] = true;
                            $cStatus[$monthKey] = $this->checkCellDataStatus('facility_rate', $dataVal);
                        }
                        else if ($this->getArrValueByKey('last_year', $monthVal) == $this->getArrValueByKey('year', $dataVal)) {
                            # 前年度データ
                            $targetData = $this->getArrValueByKey('target_data', $dataVal);
                            $lRowData[$monthKey] = $targetData;
                            $lRowMonthCount[$monthKey] = true;
                            $lStatus[$monthKey] = $this->checkCellDataStatus('facility_rate', $dataVal);
                        }
                    }
                }
                if (is_null($cStatus[$monthKey])) { $cStatus[$monthKey] = $this->checkCellDataStatus('facility_rate', array('year' => $monthVal['current_year'], 'month' => $targetMonth)); }
                if (is_null($lStatus[$monthKey])) { $lStatus[$monthKey] = $this->checkCellDataStatus('facility_rate', array('year' => $monthVal['last_year'], 'month' => $targetMonth)); }
            }

            $commonRowDataArr = array();
            foreach ($cRowMonthCount as $cKey => $cVal) {
                if ($cVal===true) {
                    $commonRowDataArr[$cKey] = $commonRowDataArr[$cKey] + 1;
                    $cExRowMonthCount[$cKey] = $cExRowMonthCount[$cKey] + 1;
                }
            }
            foreach ($lRowMonthCount as $lKey => $lVal) {
                if ($lVal===true) {
                    $commonRowDataArr[$lKey] = $commonRowDataArr[$lKey] + 1;
                    $lExRowMonthCount[$lKey] = $lExRowMonthCount[$lKey] + 1;
                }
            }

            $cTotalData     = 0;
            $lTotalData     = 0;
            $compTotalData  = 0;
            $raitoTotalData = 0;

            for ($r=0; $r<=$mainDataColNum; $r++) {
                $cCoord    = $this->coord($columnAlphaName[$startColumnNum+$r], $rowNum);
                $lCoord    = $this->coord($columnAlphaName[$startColumnNum+$r], $rowNum+1);
                $compCoord = $this->coord($columnAlphaName[$startColumnNum+$r], $rowNum+2);
                switch ($r) {
                    default: case '':
                        $compData[$r]  = $this->getExcelSubData($cCoord, $lCoord, 3);
                        $raitoData[$r] = $this->getExcelDivData($compCoord, $lCoord, 3);
                        break;

                    case $sideTotalColNum:
                        if ($sideTotalColNum!==false) {
                            $cRowData[$r]  = $this->getExcelSideSumData($columnAlphaName[$startColumnNum], $columnAlphaName[$startColumnNum+11], $rowNum, 1);
                            $lRowData[$r]  = $this->getExcelSideSumData($columnAlphaName[$startColumnNum], $columnAlphaName[$startColumnNum+11], $rowNum+1, 1);
                            $compData[$r]  = $this->getExcelSubData($cCoord,    $lCoord, 3);
                            $raitoData[$r] = $this->getExcelDivData($compCoord, $lCoord, 3);

                            for ($i=0; $i<$mainDataColNum; $i++) {
                                $cStatusArr[] = $cStatus[$i];
                                $lStatusArr[] = $lStatus[$i];
                            }
                            $cStatus[$r] = $this->checkCellDataStatus('comp', $cStatusArr);
                            $lStatus[$r] = $this->checkCellDataStatus('comp', $lStatusArr);
                        }
                        break;

                    case $sideAvrgColNum:
                        if ($sideAvrgColNum!==false) {
                            $cRowData[$r]  = $this->getExcelSideAverageData($columnAlphaName[$startColumnNum], $columnAlphaName[$startColumnNum+11], $rowNum, count($cRowMonthCount), 1);
                            $lRowData[$r]  = $this->getExcelSideAverageData($columnAlphaName[$startColumnNum], $columnAlphaName[$startColumnNum+11], $rowNum+1, count($lRowMonthCount), 1);

                            $compData[$r]  = sprintf('=ROUND(ROUND(%s, %d)-ROUND(%s, %d), %d)', $cCoord, 3, $lCoord, 3, 1);
//                            $compData[$r]  = $this->getExcelSubData($cCoord, $lCoord, 3);

                            $raitoData[$r] = $this->getExcelDivData($compCoord, $lCoord, 3);

                            for ($i=0; $i<$mainDataColNum; $i++) {
                                $cStatusArr[] = $cStatus[$i];
                                $lStatusArr[] = $lStatus[$i];
                            }
                            $cStatus[$r] = $this->checkCellDataStatus('comp', $cStatusArr);
                            $lStatus[$r] = $this->checkCellDataStatus('comp', $lStatusArr);
                        }
                        break;
                }

                $compStatus[$r]    = $this->checkCellDataStatus('comp', array($cStatus[$r],       $lStatus[$r]));
                $raitoStatus[$r]   = $this->checkCellDataStatus('comp', array($compStatus[$r],    $lStatus[$r]));
                $exCStatus[$r]     = $this->checkCellDataStatus('comp', array($exCStatus[$r],     $cStatus[$r]));
                $exLStatus[$r]     = $this->checkCellDataStatus('comp', array($exLStatus[$r],     $lStatus[$r]));
                $exCompStatus[$r]  = $this->checkCellDataStatus('comp', array($exCompStatus[$r],  $compStatus[$r]));
                $exRaitoStatus[$r] = $this->checkCellDataStatus('comp', array($exRaitoStatus[$r], $raitoStatus[$r]));
            }
            for ($d=0; $d<=$repertNum; $d++) {
                $leftSide = array();
                $mainData = array();

                switch ($d) {
                    case '0':
                        $leftSide[0] = array('data' => $this->getArrValueByKey('facility_name', $plantVal), 'attr' => array($rowspan), 'style' => array('bgColor' => $bgColor_blue));
                        $leftSide[1] = array('data' => sprintf('%04d年度', $cYear));
                        for ($r=0; $r<=$mainDataColNum; $r++) {
                            switch ($r) {
                                default:
                                    if (!is_null($percentFlg)) { $mainData[$r] = array('data' => ($cRowData[$r]/100), 'status' => $cStatus[$r], 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)); }
                                    else { $mainData[$r] = array('data' => $cRowData[$r], 'status' => $cStatus[$r]); }
                                    break;

                                case '12': # 合計
                                    if (!is_null($percentFlg)) { $mainData[$r] = array('data' => $cRowData[$r], 'status' => $cStatus[$r], 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)); }
                                    else { $mainData[$r] = array('data' => $cRowData[$r], 'status' => $cStatus[$r]); }
                                    break;

                                case '13': # 平均
                                    if (!is_null($percentFlg)) { $mainData[$r] = array('data' => $cRowData[$r], 'status' => $cStatus[$r], 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)); }
                                    else { $mainData[$r] = array('data' => $cRowData[$r], 'status' => $cStatus[$r], 'style' => array('msoNumberFormat' => $msoNumberFormat_0_0)); }
                                    break;

                            }
                        }
                        break;

                    case '1':
                        $leftSide[0] = array('data' => sprintf('%04d年度', $lYear));
                        for ($r=0; $r<=$mainDataColNum; $r++) {
                            switch ($r) {
                                default:
                                    if (!is_null($percentFlg)) { $mainData[$r] = array('data' => ($lRowData[$r]/100), 'status' => $lStatus[$r], 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)); }
                                    else { $mainData[$r] = array('data' => $lRowData[$r], 'status' => $lStatus[$r]); }
                                    break;

                                case '12':
                                    if (!is_null($percentFlg)) { $mainData[$r] = array('data' => $lRowData[$r], 'status' => $lStatus[$r], 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)); }
                                    else { $mainData[$r] = array('data' => $lRowData[$r], 'status' => $lStatus[$r]); }
                                    break;

                                case '13':
                                    if (!is_null($percentFlg)) { $mainData[$r] = array('data' => $lRowData[$r], 'status' => $lStatus[$r], 'style' => array('msoNumberFormat' => $msoNumberFormat_percent)); }
                                    else { $mainData[$r] = array('data' => $lRowData[$r], 'status' => $lStatus[$r], 'style' => array('msoNumberFormat' => $msoNumberFormat_0_0)); }
                                    break;
                            }
                        }
                        break;

                    case '2':
                        $leftSide[0] = array('data' => '増減', 'style' => array($bgColor_green));
                        for ($r=0; $r<=$mainDataColNum; $r++) {
                            switch ($r) {
                                default:
                                    if (!is_null($percentFlg)) { $mainData[$r] = array('data' => $compData[$r], 'style' => array($bgColor_green, 'msoNumberFormat' => $msoNumberFormat_percent), 'status' => $compStatus[$r]); }
                                    else { $mainData[$r] = array('data' => $compData[$r], 'style' => array($bgColor_green), 'status' => $compStatus[$r]); }
                                    break;

                                case '13':
                                    if (!is_null($percentFlg)) { $mainData[$r] = array('data' => $compData[$r], 'style' => array($bgColor_green, 'msoNumberFormat' => $msoNumberFormat_percent), 'status' => $compStatus[$r]); }
                                    else { $mainData[$r] = array('data' => $compData[$r], 'style' => array($bgColor_green, 'msoNumberFormat' => $msoNumberFormat_0_0), 'status' => $compStatus[$r]); }
                                    break;
                            }
                        }
                        break;

                    case '3':
                        $leftSide[0] = array('data' => '増減率', 'style' => array($bgColor_green));
                        for ($r=0; $r<=$mainDataColNum; $r++) { $mainData[$r] = array('data' => $raitoData[$r], 'style' => array($bgColor_green, 'msoNumberFormat' => $msoNumberFormat_percent), 'status' => $raitoStatus[$r]); }
                        break;
                }

                $rtnStr .= '<tr>'
                               .$this->convertCellData($leftSide, $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                               .$this->convertCellData($mainData, $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma))
                          .'</tr>';
            }
        }

        $totalRowCAvrg = 0;
        $totalRowLAvrg = 0;
        $avrgRowCAvrg  = 0;
        $avrgRowLAvrg  = 0;

        # AMG合計・AMG平均の施設平均の取得
        for ($i=0; $i<=$mainDataColNum; $i++) {
            $targetColumn = $columnAlphaName[$startColumnNum+$i];
            $cColumnArr   = array();
            $lColumnArr   = array();
            foreach ($plantArr as $plantKey => $plantVal) {
                $cColumnArr[] = $this->coord($targetColumn, $startRowNum+($plantKey*($repertNum+1)));
                $lColumnArr[] = $this->coord($targetColumn, $startRowNum+($plantKey*($repertNum+1))+1);
            }

            # AMG合計の施設平均の取得
            if (!is_null($amgTotalFlg)) {
                switch ($i) {
                    default: case '0':
                        $totalRowCArr[$i]     = $this->getExcelSumDataByArray($cColumnArr);
                        $totalRowLArr[$i]     = $this->getExcelSumDataByArray($lColumnArr);
                        $totalRowCompArr[$i]  = $this->getExcelSubData($this->coord($columnAlphaName[$startColumnNum+$i], $amgTotalRowNum), $this->coord($columnAlphaName[$startColumnNum+$i], $amgTotalRowNum+1), 3);
                        $totalRowRaitoArr[$i] = $this->getExcelDivData($this->coord($columnAlphaName[$startColumnNum+$i], $amgTotalRowNum+2), $this->coord($columnAlphaName[$startColumnNum+$i], $amgTotalRowNum+1), 3);
                        break;

                    # 施設合計
                    case $sideTotalColNum:
                        if ($sideTotalColNum!==false) {
                            $totalRowCArr[$i]     = $this->getExcelSideSumData($columnAlphaName[$startColumnNum], $columnAlphaName[$startColumnNum+11], $amgTotalRowNum, 1);
                            $totalRowLArr[$i]     = $this->getExcelSideSumData($columnAlphaName[$startColumnNum], $columnAlphaName[$startColumnNum+11], $amgTotalRowNum+1, 1);
                            $totalRowCompArr[$i]  = $this->getExcelSubData($this->coord($columnAlphaName[$startColumnNum+$i], $amgTotalRowNum), $this->coord($columnAlphaName[$startColumnNum+$i], $amgTotalRowNum+1), 3);
                            $totalRowRaitoArr[$i] = $this->getExcelDivData($this->coord($columnAlphaName[$startColumnNum+$i], $amgTotalRowNum+2), $this->coord($columnAlphaName[$startColumnNum+$i], $amgTotalRowNum+1), 3);
                        }
                        break;

                    # 施設平均
                    case $sideAvrgColNum:
                        if ($sideAvrgColNum!==false) {
                            $totalRowCArr[$i]     = $this->getExcelSideAverageData($columnAlphaName[$startColumnNum], $columnAlphaName[$startColumnNum+11], $amgTotalRowNum, count($cExRowMonthCount), 1);
                            $totalRowLArr[$i]     = $this->getExcelSideAverageData($columnAlphaName[$startColumnNum], $columnAlphaName[$startColumnNum+11], $amgTotalRowNum+1, count($lExRowMonthCount), 1);
                            $totalRowCompArr[$i]  = $this->getExcelSubData($this->coord($columnAlphaName[$startColumnNum+$i], $amgTotalRowNum), $this->coord($columnAlphaName[$startColumnNum+$i], $amgTotalRowNum+1), 3);
                            $totalRowRaitoArr[$i] = $this->getExcelDivData($this->coord($columnAlphaName[$startColumnNum+$i], $amgTotalRowNum+2), $this->coord($columnAlphaName[$startColumnNum+$i], $amgTotalRowNum+1), 3);
                        }
                        break;
                }
            }
            # AMG平均の施設平均の取得
            if (!is_null($amgAvrgFlg)) {
                switch ($i) {
                    default: case '0':
                        $avrgRowCArr[$i]     = $this->getExcelAverageDataByArray($cColumnArr, $cExRowMonthCount[$i]);
                        $avrgRowLArr[$i]     = $this->getExcelAverageDataByArray($lColumnArr, $lExRowMonthCount[$i]);
                        $avrgRowCompArr[$i]  = $this->getExcelSubData($this->coord($columnAlphaName[$startColumnNum+$i], $amgAvrgRowNum), $this->coord($columnAlphaName[$startColumnNum+$i], $amgAvrgRowNum+1), 3);
                        $avrgRowRaitoArr[$i] = $this->getExcelDivData($this->coord($columnAlphaName[$startColumnNum+$i], $amgAvrgRowNum+2), $this->coord($columnAlphaName[$startColumnNum+$i], $amgAvrgRowNum+1), 3);
                        break;

                    case $sideTotalColNum:
                        if ($sideTotalColNum!==false) {
                            $avrgRowCArr[$i]     = $this->getExcelSideSumData($columnAlphaName[$startColumnNum], $columnAlphaName[$startColumnNum+11], $amgAvrgRowNum, 1);
                            $avrgRowLArr[$i]     = $this->getExcelSideSumData($columnAlphaName[$startColumnNum], $columnAlphaName[$startColumnNum+11], $amgAvrgRowNum+1, 1);
                            $avrgRowCompArr[$i]  = $this->getExcelSubData($this->coord($columnAlphaName[$startColumnNum+$i], $amgAvrgRowNum), $this->coord($columnAlphaName[$startColumnNum+$i], $amgAvrgRowNum+1), 3);
                            $avrgRowRaitoArr[$i] = $this->getExcelDivData($this->coord($columnAlphaName[$startColumnNum+$i], $amgAvrgRowNum+2), $this->coord($columnAlphaName[$startColumnNum+$i], $amgAvrgRowNum+1), 3);
                        }
                        break;

                    case $sideAvrgColNum:
                        if ($sideAvrgColNum!==false) {
                            $avrgRowCArr[$i]     = $this->getExcelSideAverageData($columnAlphaName[$startColumnNum], $columnAlphaName[$startColumnNum+11], $amgAvrgRowNum, count($cExRowMonthCount), 3);
                            $avrgRowLArr[$i]     = $this->getExcelSideAverageData($columnAlphaName[$startColumnNum], $columnAlphaName[$startColumnNum+11], $amgAvrgRowNum+1, count($lExRowMonthCount), 3);
                            $avrgRowCompArr[$i]  = $this->getExcelSubData($this->coord($columnAlphaName[$startColumnNum+$i], $amgAvrgRowNum), $this->coord($columnAlphaName[$startColumnNum+$i], $amgAvrgRowNum+1), 3);
                            $avrgRowRaitoArr[$i] = $this->getExcelDivData($this->coord($columnAlphaName[$startColumnNum+$i], $amgAvrgRowNum+2), $this->coord($columnAlphaName[$startColumnNum+$i], $amgAvrgRowNum+1), 3);
                        }
                        break;
                }
            }
        }

        # AMG合計・AMG平均行の出力
        for ($a=0; $a<=$repertNum; $a++) {
            # AMG合計行の出力
            if (!is_null($amgTotalFlg)) {
                $totalData     = array();
                $totalLeftSide = array();
                switch ($a) {
                    case '0':
                        $totalLeftSide[0] = array('data' => 'ＡＭＧ合計', 'attr' => array($rowspan), 'style' => array('bgColor' => $bgColor_blue));
                        $totalLeftSide[1] = array('data' => sprintf('%04d年度', $cYear));
                        for ($r=0; $r<=$mainDataColNum; $r++) {
                            switch ($r) {
                                default: case '':
                                    if (!is_null($percentFlg)) { $totalData[$r] = array('data' => $totalRowCArr[$r], 'status' => $exCStatus[$r], 'style' => array($msoNumberFormat_percent)); }
                                    else { $totalData[$r] = array('data' => $totalRowCArr[$r], 'status' => $exCStatus[$r]); }
                                    break;

                                case $sideAvrgColNum:
                                    if (!is_null($percentFlg)) { $totalData[$r] = array('data' => $totalRowCArr[$r], 'status' => $exCStatus[$r], 'style' => array($msoNumberFormat_0_0)); }
                                    else { $totalData[$r] = array('data' => $totalRowCArr[$r], 'status' => $exCStatus[$r], 'style' => array($msoNumberFormat_0_0)); }
                                    break;
                            }
                        }
                        break;

                    case '1':
                        $totalLeftSide[0] = array('data' => sprintf('%04d年度', $lYear));
                        for ($r=0; $r<=$mainDataColNum; $r++) {
                            switch ($r) {
                                default:
                                    if (!is_null($percentFlg)) { $totalData[$r] = array('data' => $totalRowLArr[$r], 'status' => $exLStatus[$r], 'style' => array($msoNumberFormat_percent)); }
                                    else { $totalData[$r] = array('data' => $totalRowLArr[$r], 'status' => $exLStatus[$r]); }
                                    break;

                                case $sideAvrgColNum:
                                    if (!is_null($percentFlg)) { $totalData[$r] = array('data' => $totalRowLArr[$r], 'status' => $exLStatus[$r], 'style' => array($msoNumberFormat_percent)); }
                                    else { $totalData[$r] = array('data' => $totalRowLArr[$r], 'status' => $exLStatus[$r], 'style' => array($msoNumberFormat_0_0)); }
                                    break;
                            }
                        }
                        break;

                    case '2':
                        $totalLeftSide[0] = array('data' => '増減', 'style' => array($bgColor_green));
                        for ($r=0; $r<=$mainDataColNum; $r++) {
                            switch ($r) {
                                default: case '':
                                    if (!is_null($percentFlg)) { $totalData[$r] = array('data' => $totalRowCompArr[$r], 'style' => array($bgColor_green, $msoNumberFormat_percent), 'status' => $exCompStatus[$r]); }
                                    else { $totalData[$r] = array('data' => $totalRowCompArr[$r], 'style' => array($bgColor_green), 'status' => $exCompStatus[$r]); }
                                    break;

                                case $sideAvrgColNum:
                                    if (!is_null($percentFlg)) { $totalData[$r] = array('data' => $totalRowCompArr[$r], 'style' => array($bgColor_green, $msoNumberFormat_0_0), 'status' => $exCompStatus[$r]); }
                                    else { $totalData[$r] = array('data' => $totalRowCompArr[$r], 'style' => array($bgColor_green, 'msoNumberFormat' => $msoNumberFormat_0_0), 'status' => $exCompStatus[$r]); }
                                    break;
                            }
                        }
                        break;

                    case '3':
                        $totalLeftSide[0] = array('data' => '増減率', 'style' => array($bgColor_green));
                        for ($r=0; $r<=$mainDataColNum; $r++) { $totalData[$r] = array('data' => $totalRowRaitoArr[$r], 'style' => array($bgColor_green, $msoNumberFormat_percent), 'status' => $exRaitoStatus[$r]); }
                        break;
                }

                $rtnStr .= '<tr>'
                              .$this->convertCellData($totalLeftSide, $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                              .$this->convertCellData($totalData, $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma))
                          .'</tr>';
            }
            # AMG平均行の出力
            if (!is_null($amgAvrgFlg)) {
                $avrgData     = array();
                $avrgLeftSide = array();
                switch ($a) {
                    case '0':
                        $avrgLeftSide[0] = array('data' => 'ＡＭＧ平均', 'attr' => array($rowspan), 'style' => array('bgColor' => $bgColor_blue));
                        $avrgLeftSide[1] = array('data' => sprintf('%04d年度', $cYear));
                        for ($r=0; $r<=$mainDataColNum; $r++) {
                            if (!is_null($percentFlg)) { $avrgData[$r] = array('data' => $avrgRowCArr[$r], 'status' => $exCStatus[$r], 'style' => array($msoNumberFormat_percent)); }
                            else { $avrgData[$r] = array('data' => $avrgRowCArr[$r], 'status' => $exCStatus[$r]); }
                        }
                        break;

                    case '1':
                        $avrgLeftSide[0] = array('data' => sprintf('%04d年度', $lYear));
                        for ($r=0; $r<=$mainDataColNum; $r++) {
                            if (!is_null($percentFlg)) { $avrgData[$r] = array('data' => $avrgRowLArr[$r], 'status' => $exLStatus[$r], 'style' => array($msoNumberFormat_percent)); }
                            else { $avrgData[$r] = array('data' => $avrgRowLArr[$r], 'status' => $exLStatus[$r]); }
                        }
                        break;

                    case '2':
                        $avrgLeftSide[0] = array('data' => '増減', 'style' => array($bgColor_green));
                        for ($r=0; $r<=$mainDataColNum; $r++) {
                            if (!is_null($percentFlg)) { $avrgData[$r] = array('data' => $avrgRowCompArr[$r], 'style' => array($bgColor_green, $msoNumberFormat_percent), 'status' => $exCompStatus[$r]); }
                            else { $avrgData[$r] = array('data' => $avrgRowCompArr[$r], 'style' => array($bgColor_green), 'status' => $exCompStatus[$r]); }
                        }
                        break;

                    case '3':
                        $avrgLeftSide[0] = array('data' => '増減率', 'style' => array($bgColor_green));
                        for ($r=0; $r<=$mainDataColNum; $r++) { $avrgData[$r] = array('data' => $avrgRowRaitoArr[$r], 'style' => array($bgColor_green, $msoNumberFormat_percent), 'status' => $exRaitoStatus[$r]); }
                        break;
                }

                $rtnStr .= '<tr>'
                              .$this->convertCellData($avrgLeftSide, $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                              .$this->convertCellData($avrgData, $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma))
                          .'</tr>';
            }
        }

        $rtnStr .= '</table>';
        return $rtnStr;
    }

    /**
     * Np.108 [老健] 老健月間加算項目・算定実績一覧のエクセル出力
     * @param  array $dataArr
     * @param  array $plantArr
     * @param  array $arr
     * @param  array $optionArr
     * @return string
     */
    function outputExcelCalculationResults($dataArr, $plantArr, $arr, $optionArr)
    {
        $rtnStr  = $this->getExcelMeta();
        $fontTag = $this->getFontTag();

        $targetYear  = $this->getArrValueByKey('year', $optionArr);
        $targetMonth = $this->getArrValueByKey('month', $optionArr);

        # 特殊文字
        $roman1 = $this->sc('roman_1');
        $roman2 = $this->sc('roman_2');
        $roman3 = $this->sc('roman_3');

        /* @var $indicatorData IndicatorData */
        $indicatorData = $this->getIndicatorData();

        # style
        $bgColor_blue      = array('name' => 'background-color', 'value' => $this->getRgbColor('blue'));
        $bgColor_weekday   = array('name' => 'background-color', 'value' => $this->getRgbColor('weekday'));
        $bgColor_saturday  = array('name' => 'background-color', 'value' => $this->getRgbColor('saturday'));
        $bgColor_holiday   = array('name' => 'background-color', 'value' => $this->getRgbColor('lightpink'));
        $textAlign_center  = array('name' => 'text-align',       'value' => 'center');
        $textAlign_right   = array('name' => 'text-align',       'value' => 'right');
        $textAlign_left    = array('name' => 'text-align',       'value' => 'left');
        $whiteSpace_nowrap = array('name' => 'white-space',      'value' => 'nowrap');
        $msonumberFormat_comma = $this->getMsoNumberFormat('comma');

        # 各日数表示
        $dayCountData = $indicatorData->getDayCountOfMonth($targetYear, $targetMonth);
        $dayCountArr = array(
            0 => array('data' => sprintf('%04d年%02d月', $targetYear, $targetMonth), 'style' => array('textAlign' => $textAlign_left)),
            1 => array('data' => '平日', 'style' => array('bgColor' => $bgColor_weekday, 'textAlign' => $textAlign_left)),
            2 => array('data' => $this->getArrValueByKey('weekday', $dayCountData).'日'),
            3 => array('data' => '土曜', 'style' => array('bgColor' => $bgColor_saturday, 'textAlign' => $textAlign_left)),
            4 => array('data' => $this->getArrValueByKey('saturday', $dayCountData).'日'),
            5 => array('data' => '休日', 'style' => array('bgColor' => $bgColor_holiday, 'textAlign' => $textAlign_left)),
            6 => array('data' => $this->getArrValueByKey('holiday', $dayCountData).'日'),
        );

        # 入所のヘッダー
        $enterHeaderArr = array(
             0 => array(0 => array('data' => '加算項目'), 1 => array('data' => '&nbsp;'), 2 => array('data' => '単位数')),
             1 => array(0 => array('data' => '1.夜間職員配置加算', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '24単位')),
             2 => array(0 => array('data' => '2.短期集中ﾘﾊﾋﾞﾘﾃｰｼｮﾝ実施加算', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '240単位')),
             3 => array(0 => array('data' => '3.認知症短期集中ﾘﾊﾋﾞﾘﾃｰｼｮﾝ実施加算(週3日を限度)', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '240単位')),
             4 => array(0 => array('data' => '4.認知症ケア加算(ユニットは算定不可)', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '76単位')),
             5 => array(0 => array('data' => '5.若年性認知症入所者受入加算', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '120単位')),
             6 => array(0 => array('data' => '6.ターミナル加算(1)死亡日以前15日以上30日以下', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '200単位')),
             7 => array(0 => array('data' => '7.ターミナル加算(1)死亡日以前15日以上31日以下', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '315単位')),
             8 => array(0 => array('data' => '8.療養体制維持特別加算', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '27単位')),
             9 => array(0 => array('data' => '9.初期加算', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '30単位')),
            10 => array(0 => array('data' => '10.退所前後訪問指導加算(入所中1回又は2回、退所後1回を限度)', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '460単位')),
            11 => array(0 => array('data' => '11.退所時指導加算', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '400単位')),
            12 => array(0 => array('data' => '12.退所時情報提供加算', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '500単位')),
            13 => array(0 => array('data' => '13.退所前連携加算', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '500単位')),
            14 => array(0 => array('data' => '14.老人訪問看護指示加算', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '300単位')),
            15 => array(0 => array('data' => '15.栄養マネジメント加算', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '14単位')),
            16 => array(0 => array('data' => '16.経口移行加算', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '28単位')),
            17 => array(0 => array('data' => '17.経口維持加算('.$roman1.')', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '28単位')),
            18 => array(0 => array('data' => '18.経口維持加算('.$roman2.')', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '5単位')),
            19 => array(0 => array('data' => '19.口腔機能維持管理加算', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1月につき'), 2 => array('data' => '30単位')),
            20 => array(0 => array('data' => '20.療養食加算', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '23単位')),
            21 => array(0 => array('data' => '21.在宅復帰支援機能加算('.$roman1.')', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '15単位')),
            22 => array(0 => array('data' => '22.在宅復帰支援機能加算('.$roman2.')', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '5単位')),
            23 => array(0 => array('data' => '23.緊急時施設療養費(1)緊急時治療管理(1月に1回3日を限度)', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '500単位')),
            24 => array(0 => array('data' => '24.認知症専門ケア加算('.$roman1.')', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '3単位')),
            25 => array(0 => array('data' => '25.認知症専門ケア加算('.$roman2.')', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '4単位')),
            26 => array(0 => array('data' => '26.認知症情報提供加算', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '350単位')),
            27 => array(0 => array('data' => '27.サービス提供体制強化加算('.$roman1.')', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '12単位')),
            28 => array(0 => array('data' => '28.サービス提供体制強化加算('.$roman2.')', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '6単位')),
            29 => array(0 => array('data' => '29.サービス提供体制強化加算('.$roman3.')', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '6単位')),
        );

        # ショートのヘッダー
        $shortHeaderArr = array(
             0 => array(0 => array('data' => '加算項目'), 1 => array('data' => '&nbsp;'), 2 => array('data' => '単位数')),
             1 => array(0 => array('data' => '1.夜勤職員配置加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '24単位')),
             2 => array(0 => array('data' => '2.リハビリ機能強化加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '30単位')),
             3 => array(0 => array('data' => '3.個別リハビリテーション実施加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '240単位')),
             4 => array(0 => array('data' => '4.認知症ケア加算(ユニットは算定不可)', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '76単位')),
             5 => array(0 => array('data' => '5.認知症行動・心理症状緊急対応加算(7日間を限度)', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '200単位')),
             6 => array(0 => array('data' => '6.若年性認知症入所者受入加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '120単位')),
             7 => array(0 => array('data' => '7.送迎加算(片道につき)', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '184単位')),
             8 => array(0 => array('data' => '8.療養体制維持特別加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '27単位')),
             9 => array(0 => array('data' => '9.療養食加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '23単位')),
            10 => array(0 => array('data' => '10.緊急短期入所ネットワーク加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '50単位')),
            11 => array(0 => array('data' => '11.緊急時施設療養費(１)緊急時治療管理(1月に1回3日を限度)', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '500単位')),
            12 => array(0 => array('data' => '12.サービス提供体制強化加算('.$roman1.')', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '12単位')),
            13 => array(0 => array('data' => '13.サービス提供体制強化加算('.$roman2.')', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '6単位')),
            14 => array(0 => array('data' => '14.サービス提供体制強化加算('.$roman3.')', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '6単位')),
        );

        # 通所のヘッダー
        $communteHeaderArr = array(
             0 => array(0 => array('data' => '加算項目'), 1 => array('data' => '&nbsp;'), 2 => array('data' => '単位数')),
             1 => array(0 => array('data' => '1.連続介護加算(ｱ･8h-9h)', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '50単位')),
             2 => array(0 => array('data' => '2.連続介護加算(ｲ･9ｈ-10h)', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '100単位')),
             3 => array(0 => array('data' => '3.入浴介助加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '50単位')),
             4 => array(0 => array('data' => '4.リハ計画作成加算（月1回限度）', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '550単位')),
             5 => array(0 => array('data' => '5.リハビリテーションマネジメント加算（月1回限度）', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '230単位')),
             6 => array(0 => array('data' => '6.短期集中リハ実施加算（１月以内）', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '280単位')),
             7 => array(0 => array('data' => '7.短期集中リハ実施加算（１月超３月以内）', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '140単位')),
             8 => array(0 => array('data' => '8.個別リハ実施加算（３月超）（月13回限度）', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '80単位')),
             9 => array(0 => array('data' => '9.認知症短期集中リハ実施加算（週2日）を限度', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '240単位')),
            10 => array(0 => array('data' => '10.若年性認知症利用者受入加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '60単位')),
            11 => array(0 => array('data' => '11.栄養改善加算（月2回限度）', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '150単位')),
            12 => array(0 => array('data' => '12.口腔機能向上加算（月2回限度）', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '150単位')),
            13 => array(0 => array('data' => '13.サービス提供体制加算（'.$roman1.'）', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '12単位')),
            14 => array(0 => array('data' => '14.サービス提供体制加算（'.$roman2.'）', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '6単位')),
        );

        # 介護予防通所のヘッダー
        $communtePreHeaderArr = array(
            0 => array(0 => array('data' => '加算項目'), 1 => array('data' => '&nbsp;'), 2 => array('data' => '単位数')),
            1 => array(0 => array('data' => '1．若年性認知症利用者受入加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1月につき'), 2 => array('data' => '240単位')),
            2 => array(0 => array('data' => '2.運動機能加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1月につき'), 2 => array('data' => '225単位')),
            3 => array(0 => array('data' => '3.栄養改善加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1月につき'), 2 => array('data' => '150単位')),
            4 => array(0 => array('data' => '4.口腔機能向上加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1月につき'), 2 => array('data' => '150単位')),
            5 => array(0 => array('data' => '5.事業所評価加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1月につき'), 2 => array('data' => '100単位')),
            6 => array(0 => array('data' => '6.サービス提供強化加算（1）要支援１', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1月につき'), 2 => array('data' => '48単位')),
            7 => array(0 => array('data' => '7.サービス提供強化加算（1）要支援２', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1月につき'), 2 => array('data' => '96単位')),
            8 => array(0 => array('data' => '8.サービス提供強化加算（2）要支援１', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1月につき'), 2 => array('data' => '24単位')),
            9 => array(0 => array('data' => '9.サービス提供強化加算（2）要支援２', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1月につき'), 2 => array('data' => '48単位')),
        );

        # 訪問リハのヘッダー
        $visitHeaderArr = array(
            0 => array(0 => array('data' => '加算項目'), 1 => array('data' => '&nbsp;'), 2 => array('data' => '単位数')),
            1 => array(0 => array('data' => '1.短期集中リハビリテーション実施加算(1月以内)', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '340単位')),
            2 => array(0 => array('data' => '2.短期集中リハビリテーション実施加算(1月超3月以内)', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '200単位')),
            3 => array(0 => array('data' => '3.サービス提供強化加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '6単位')),
        );

        # 介護予防訪問リハのヘッダー
        $visitPreHeaderArr = array(
            0 => array(0 => array('data' => '加算項目'), 1 => array('data' => '&nbsp;'), 2 => array('data' => '単位数')),
            1 => array(0 => array('data' => '1.短期集中リハビリテーション実施加算(3月以内)', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '200単位')),
            2 => array(0 => array('data' => '2.サービス提供強化加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '6単位')),
        );

        $plantCnt = count($plantArr);

        $enterMainArr       = array();
        $shortMainArr       = array();
        $communteMainArr    = array();
        $communtePreMainArr = array();
        $visitMainArr       = array();
        $visitPreMainArr    = array();

        $enterTotalArr       = array();
        $shortTotalArr       = array();
        $communteTotalArr    = array();
        $communtePreTotalArr = array();
        $visitTotalArr       = array();
        $visitPreTotalArr    = array();

        # エクセル座標算出用
        $enterStartRowNum       =  4 - 1;
        $shortStartRowNum       = 35 - 1;
        $communteStartRowNum    = 51 - 1;
        $communtePreStartRowNum = 67 - 1;
        $visitStartRowNum       = 78 - 1;
        $visitPreStartRowNum    = 83 - 1;
        $startColumnNum         = 4;
        $totalColumnNum         = $startColumnNum + $plantCnt - 1;
        $columnAlphaName        = $this->columnAlphaNameArr;

        foreach ($plantArr as $plantVal) {
            $facilityName = $this->getArrValueByKey('facility_name', $plantVal);
            $facilityId   = $this->getArrValueByKey('facility_id', $plantVal);

            # 施設名
            $enterMainArr[0][]       = array('data' => $facilityName, 'style' => array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center));
            $shortMainArr[0][]       = array('data' => $facilityName, 'style' => array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center));
            $communteMainArr[0][]    = array('data' => $facilityName, 'style' => array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center));
            $communtePreMainArr[0][] = array('data' => $facilityName, 'style' => array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center));
            $visitMainArr[0][]       = array('data' => $facilityName, 'style' => array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center));
            $visitPreMainArr[0][]    = array('data' => $facilityName, 'style' => array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center));

            # 初期化
            for ($i=109; $i<=179; $i++) { $dispNum[$i] = null;; }

            if (is_array($dataArr) && count($dataArr)!=0) {
                foreach ($dataArr as $dataVal) {
                    if ($facilityId==$this->getArrValueByKey('jnl_facility_id', $dataVal) && $targetYear == $this->getArrValueByKey('year', $dataVal) && $targetMonth == $this->getArrValueByKey('month', $dataVal)) {
                        for ($i=109; $i<=179; $i++) { $dispNum[$i] = $this->getArrValueByKey('dispnum_'.$i, $dataVal); }
                    }
                }
            }

            for ($i=1; $i<=29; $i++) {
                $enterMainArr[$i][] = array('data' => $dispNum[108+$i]);
                $enterTotalArr[$i][0]['data'] = $this->getExcelSideSumData($columnAlphaName[$startColumnNum], $columnAlphaName[$totalColumnNum], $enterStartRowNum+$i);

                if ($i<=14) {
                    $shortMainArr[$i][] = array('data' => $dispNum[137+$i]);
                    $shortTotalArr[$i][0]['data'] = $this->getExcelSideSumData($columnAlphaName[$startColumnNum], $columnAlphaName[$totalColumnNum], $shortStartRowNum+$i);
                    $communteMainArr[$i][] = array('data' => $dispNum[151+$i]);
                    $communteTotalArr[$i][0]['data'] = $this->getExcelSideSumData($columnAlphaName[$startColumnNum], $columnAlphaName[$totalColumnNum], $communteStartRowNum+$i);

                    if ($i<=9) {
                        $communtePreMainArr[$i][] = array('data' => $dispNum[165+$i]);
                        $communtePreTotalArr[$i][0]['data'] = $this->getExcelSideSumData($columnAlphaName[$startColumnNum], $columnAlphaName[$totalColumnNum], $communtePreStartRowNum+$i);

                        if ($i<=3) {
                            $visitMainArr[$i][] = array('data' => $dispNum[174+$i]);
                            $visitTotalArr[$i][0]['data'] = $this->getExcelSideSumData($columnAlphaName[$startColumnNum], $columnAlphaName[$totalColumnNum], $visitStartRowNum+$i);

                            if ($i<=2) {
                                $visitPreMainArr[$i][] = array('data' => $dispNum[177+$i]);
                                $visitPreTotalArr[$i][0]['data'] = $this->getExcelSideSumData($columnAlphaName[$startColumnNum], $columnAlphaName[$totalColumnNum], $visitPreStartRowNum+$i);
                            }
                        }
                    }
                }
            }
        }

        $enterMainArr[0][$plantCnt]       = array('data' => '合計', 'style' => array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center));
        $shortMainArr[0][$plantCnt]       = array('data' => '合計', 'style' => array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center));
        $communteMainArr[0][$plantCnt]    = array('data' => '合計', 'style' => array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center));
        $communtePreMainArr[0][$plantCnt] = array('data' => '合計', 'style' => array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center));
        $visitMainArr[0][$plantCnt]       = array('data' => '合計', 'style' => array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center));
        $visitPreMainArr[0][$plantCnt]    = array('data' => '合計', 'style' => array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center));

        # 出力データ表示
        $rtnStr .= '<table width="100%" >'
                      .'<tr>'
                          .'<td style="text-align: right; ">'
                              .'<table class="list" width="30%" border="1" >'
                                  .'<tr>'. $this->convertCellData($dayCountArr, $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)).'</tr>'
                              .'</table>'
                          .'</td>'
                      .'</tr>'
                      .'<tr>'
                          .$this->convertCellData(array(0 => array('data'=> '入所', 'style' => array($textAlign_left))), $fontTag, array())
                      .'</tr>'
                      .'<tr>'
                          .'<td>'
                              .'<table class="list" width="100%" border="1" >';
        for ($i=0; $i<=29; $i++) {
            $rtnStr .= '<tr>'
                          .$this->convertCellData($enterHeaderArr[$i], $fontTag, array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($enterMainArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msonumberFormat' => $msonumberFormat_comma))
                          .$this->convertCellData($enterTotalArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msonumberFormat' => $msonumberFormat_comma))
                      .'</tr>';
        }
        $rtnStr .= '</table>'
                          .'</td>'
                      .'</tr>'
                      .'<tr>'
                          .$this->convertCellData(array(0 => array('data'=> 'ショート(介護・予防を含む)', 'style' => array($textAlign_left))), $fontTag, array())
                      .'</tr>'
                      .'<tr>'
                          .'<td>'
                              .'<table class="list" width="100%" border="1" >';
        for ($i=0; $i<=14; $i++) {
            $rtnStr .= '<tr>'
                          .$this->convertCellData($shortHeaderArr[$i], $fontTag, array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($shortMainArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($shortTotalArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap))
                      .'</tr>';
        }
        $rtnStr .=              '</table>'
                          .'</td>'
                      .'</tr>'
                      .'<tr>'
                          .$this->convertCellData(array(0 => array('data'=> '通所リハビリテーション', 'style' => array($textAlign_left))), $fontTag, array())
                      .'</tr>'
                      .'<tr>'
                          .'<td>'
                              .'<table class="list" width="100%" border="1" >';
        for ($i=0; $i<=14; $i++) {
            $rtnStr .= '<tr>'
                          .$this->convertCellData($communteHeaderArr[$i], $fontTag, array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($communteMainArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msonumberFormat' => $msonumberFormat_comma))
                          .$this->convertCellData($communteTotalArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msonumberFormat' => $msonumberFormat_comma))
                      .'</tr>';
        }
        $rtnStr .=             '</table>'
                          .'</td>'
                      .'</tr>'
                      .'<tr>'
                          .$this->convertCellData(array(0 => array('data'=> '介護予防通所リハビリテーション', 'style' => array($textAlign_left))), $fontTag, array())
                      .'</tr>'
                      .'<tr>'
                          .'<td>'
                              .'<table class="list" width="100%" border="1" >';
        for ($i=0; $i<=9; $i++) {
            $rtnStr .= '<tr>'
                          .$this->convertCellData($communtePreHeaderArr[$i], $fontTag, array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($communtePreMainArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msonumberFormat' => $msonumberFormat_comma))
                          .$this->convertCellData($communtePreTotalArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msonumberFormat' => $msonumberFormat_comma))
                      .'</tr>';
        }
        $rtnStr .=             '</table>'
                          .'</td>'
                      .'</tr>'
                      .'<tr>'
                          .$this->convertCellData(array(0 => array('data'=> '訪問リハビリテーション', 'style' => array($textAlign_left))), $fontTag, array())
                      .'</tr>'
                      .'<tr>'
                          .'<td>'
                              .'<table class="list" width="100%" border="1" >';
        for ($i=0; $i<=3; $i++) {
            $rtnStr .= '<tr>'
                          .$this->convertCellData($visitHeaderArr[$i], $fontTag, array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($visitMainArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msonumberFormat' => $msonumberFormat_comma))
                          .$this->convertCellData($visitTotalArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msonumberFormat' => $msonumberFormat_comma))
                      .'</tr>';
        }
        $rtnStr .=             '</table>'
                          .'</td>'
                      .'</tr>'
                      .'<tr>'
                          .$this->convertCellData(array(0 => array('data'=> '介護予防訪問リハビリテーション', 'style' => array($textAlign_left))), $fontTag, array())
                      .'</tr>'
                      .'<tr>'
                          .'<td>'
                              .'<table class="list" width="100%" border="1" >';
        for ($i=0; $i<=2; $i++) {
            $rtnStr .= '<tr>'
                          .$this->convertCellData($visitPreHeaderArr[$i], $fontTag, array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($visitPreMainArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msonumberFormat' => $msonumberFormat_comma))
                          .$this->convertCellData($visitPreTotalArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msonumberFormat' => $msonumberFormat_comma))
                      .'</tr>';
        }
        $rtnStr .=             '</table>'
                          .'</td>'
                      .'</tr>'
                  .'</table>';
        return $rtnStr;
    }
}