<?php

// セッションの開始
session_start();

// ==================================================
// ファイルの読込み
// ==================================================
require_once('Cmx.php');
require_once('Cmx/View/Smarty.php');
require_once("hiyari_header_class.php");

require_once("about_authority.php");
require_once("about_session.php");
require_once("about_postgres.php");

require_once("hiyari_common.ini");
require_once("hiyari_post_select_box.ini");
require_once("hiyari_report_class.php");
require_once("hiyari_mail.ini");
require_once("hiyari_mail_input.ini");
require_once("hiyari_wf_utils.php");
require_once("hiyari_auth_class.php");

require_once("get_values.php");
require_once('class/Cmx/Model/SystemConfig.php');
require_once("conf/conf.inf");

// ==================================================
// 初期処理
// ==================================================
// 画面名
$fname = $PHP_SELF;

// モード
$mode = $_POST["mode"];

// 日付データ取得
$this_year = date('Y');
$this_month = date('m');

// 小数点桁数
$decimal_num = 2;

// ==================================================
// 画面データ取得
// ==================================================
// 集計モード
$date_mode = $_POST["date_mode"];

// 集計年月
$date_year = $_POST["date_year"];
$date_month = $_POST["date_month"];
$date_term = $_POST["date_term"];
if ($date_year == "") {
	$date_year = $this_year;
}
if ($date_month == "") {
	$date_month = $this_month;
}
if ($date_term == "") {
	$date_term = 1;
}

// 副報告フラグ
$sub_report_use = $_POST["sub_report_use"];

// 所属データ(配列)
$belong_cnt = $_POST["belong_cnt"];
$belong_class = $_POST["belong_class"];
$belong_attribute = $_POST["belong_attribute"];
$belong_dept = $_POST["belong_dept"];
$belong_room = $_POST["belong_room"];

// 縦軸組織項目
$vertical_axis = $_POST["vertical_axis"];

// 横軸項目
$horizontal_axis = $_POST["horizontal_axis"];

// ==================================================
// セッションのチェック
// qualify_session() from about_session.php
// ==================================================
$session = qualify_session($session, $fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// ==================================================
// 権限チェック
// check_authority() from about_authority.php
// ==================================================
$summary = check_authority($session, 47, $fname);
if ($summary == "0") {
	showLoginPage();
	exit;
}

// ==================================================
// DBコネクション取得
// connect2db() from about_postgres.php
// ==================================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==================================================
//デザイン（1=旧デザイン、2=新デザイン）
//==================================================
$sysConf = new Cmx_SystemConfig();
$design_mode = $sysConf->get('fantol.design.mode');

// ==================================================
// セクション名(部門・課・科・室)取得
// get_class_name_array() from show_class_name.ini
// ==================================================
$arr_class_name = get_class_name_array($con, $fname);
$class_cnt = $arr_class_name["class_cnt"];

// ==================================================
// 職員ID取得
// get_emp_id() from get_values.php
// ==================================================
$emp_id = get_emp_id($con, $session, $fname);

// 初期表示時
if ($mode == "") {
	// ==================================================
	// 職員情報取得
	// get_empmst() from get_values.php
	// ==================================================
	$emp_info = get_empmst($con, $emp_id, $fname);
	$_POST["belong_class"][0] = $emp_info[2];
	$_POST["belong_attribute"][0] = $emp_info[3];
	$_POST["belong_dept"][0] = $emp_info[4];
	$_POST["belong_room"][0] = $emp_info[33];
}

// ==================================================
// 表示
// ==================================================
$smarty = new Cmx_View();

$smarty->assign("session", $session);
$smarty->assign("session_name", session_name());
$smarty->assign("session_id", session_id());

//------------------------------------------------------------------------------
//ヘッダ用データ
//------------------------------------------------------------------------------
$header = new hiyari_header_class($session, $fname, $con);
$smarty->assign("INCIDENT_TITLE", $header->get_inci_title());
$smarty->assign("inci_auth_name", $header->get_inci_auth_name());
$smarty->assign("is_kanrisha", $header->is_kanrisha());
$smarty->assign("tab_info", $header->get_user_tab_info());

//------------------------------------------------------------------------------
//MENU:集計ダウンロード
//------------------------------------------------------------------------------
$stats_download_mode = defined("STATS_DOWNLOAD_MODE");
$smarty->assign("stats_download_mode", $stats_download_mode);

//------------------------------------------------------------------------------
//集計条件：集計モード
//------------------------------------------------------------------------------
$smarty->assign("date_mode", $date_mode);
if ($date_mode == "" || $date_mode == "report_date") {
    $report_date_str = "selected";
    $incident_date_str = "";
} else if ($date_mode == "incident_date") {
    $report_date_str = "";
    $incident_date_str = "selected";
}
$smarty->assign("report_date_str", $report_date_str);
$smarty->assign("incident_date_str", $incident_date_str);

//------------------------------------------------------------------------------
//集計条件：集計年月
//------------------------------------------------------------------------------
$smarty->assign("year_list", array_reverse(range(2004, $this_year)));
$smarty->assign("date_year", $date_year);
$smarty->assign("month_list", range(1,12));
$smarty->assign("date_month", $date_month);
$smarty->assign("date_term", $date_term);

//------------------------------------------------------------------------------
//集計条件：副報告
//------------------------------------------------------------------------------
$smarty->assign("sub_report_use", $sub_report_use);

//------------------------------------------------------------------------------
//集計条件：部署
//------------------------------------------------------------------------------
//-----------------
//部署階層の名前
//-----------------
$smarty->assign("arr_class_name", $arr_class_name);

//-----------------
//部署の選択肢
//-----------------
// 統計分析の所属参照範囲を取得
// get_stats_read_div() from hiyari_common.ini
$read_div = get_stats_read_div($session,$fname,$con);

// ログインユーザーの最優先権限を取得
// get_emp_priority_auth() from hiyari_auth_class.php
$priority_auth = get_emp_priority_auth($session, $fname, $con);

// 統計分析の利用制限の所属/担当範囲判定フラグを取得
// get_stats_read_auth_flg() from hiyari_common.ini
$read_auth_flg = get_stats_read_auth_flg($session,$fname,$con);

$post_select_data = get_post_select_data($con, $fname, $emp_id, $read_div, $priority_auth, $read_auth_flg);
$smarty->assign("post_select_data", $post_select_data);

//-----------------
//部署条件数と選択値
//-----------------
//部署条件数
$belong_cnt = $_POST["belong_cnt"];
if ($belong_cnt == "") {
    $belong_cnt = 1;
}

// 行追加
if ($_POST["mode"] == "add_belong") {
    $belong_cnt++;
}

// 行削除
if ($_POST["mode"] == "del_belong") {
    $tmp_class = array();
    $tmp_attribute = array();
    $tmp_dept = array();
    $tmp_room = array();

    for($i=0; $i<$belong_cnt; $i++) {
        if ($_POST["del_line_no"] != $i) {
            $tmp_class[] = $_POST["belong_class"][$i];
            $tmp_attribute[] = $_POST["belong_attribute"][$i];
            $tmp_dept[] = $_POST["belong_dept"][$i];
            if ($class_cnt == 4){
                $tmp_room[] = $_POST["belong_room"][$i];
            }
        }
    }

    $belong_cnt--;

    $belong_class = $tmp_class;
    $belong_attribute = $tmp_attribute;
    $belong_dept = $tmp_dept;
    if ($class_cnt == 4){
        $belong_room = $tmp_room;
    }
}

$smarty->assign("belong_cnt", $belong_cnt);
$smarty->assign("belong_class", $belong_class);
$smarty->assign("belong_attribute", $belong_attribute);
$smarty->assign("belong_dept", $belong_dept);
if ($class_cnt == 4){
    $smarty->assign("belong_room", $belong_room);
}

//--------------------
// 集計実行時の部署条件
//--------------------
if ($mode == "sum") {
	// 部門一覧取得
	// get_class_mst() from hiyari_common.ini
	$sel_class = get_class_mst($con, $fname);
	$sel_class_arr = pg_fetch_all($sel_class);

	// 課一覧取得
	// get_atrb_mst() from hiyari_common.ini
	$sel_atrb = get_atrb_mst($con, $fname);
	$sel_atrb_arr = pg_fetch_all($sel_atrb);

	// 科一覧取得
	// get_dept_mst() from hiyari_common.ini
	$sel_dept = get_dept_mst($con, $fname);
	$sel_dept_arr = pg_fetch_all($sel_dept);

	// 室一覧取得
	// get_room_mst() from hiyari_common.ini
	$sel_room = get_room_mst($con, $fname);
	$sel_room_arr = pg_fetch_all($sel_room);

    $belong_list = array();
    for ($i = 0; $i < $belong_cnt; $i++) {
        $belong = array();

        $wk_class = $belong_class[$i];
        $wk_attribute = $belong_attribute[$i];
        $wk_dept = $belong_dept[$i];
        $wk_room = $belong_room[$i];

        $wk_belong_selected = "";

        if ($wk_class != "") {
            foreach ($sel_class_arr as $row) {
                $tmp_class_id = $row["class_id"];
                $tmp_class_nm = $row["class_nm"];

                if ($wk_class == $tmp_class_id) {
                    $wk_belong_selected .= $tmp_class_nm;
                }
            }
        } else {
            $wk_belong_selected .= "全て";
        }
        if ($wk_attribute != "") {
            foreach ($sel_atrb_arr as $row) {
                $tmp_class_id = $row["class_id"];
                $tmp_atrb_id = $row["atrb_id"];
                $tmp_atrb_nm = $row["atrb_nm"];

                if ($wk_class == $tmp_class_id && $wk_attribute == $tmp_atrb_id) {
                    $wk_belong_selected .= "＞" . $tmp_atrb_nm;
                }
            }
        } else {
            $wk_belong_selected .= "＞全て";
        }
        if ($wk_dept != "") {
            foreach ($sel_dept_arr as $row) {
                $tmp_atrb_id = $row["atrb_id"];
                $tmp_dept_id = $row["dept_id"];
                $tmp_dept_nm = $row["dept_nm"];

                if ($wk_attribute == $tmp_atrb_id && $wk_dept == $tmp_dept_id) {
                    $wk_belong_selected .= "＞" . $tmp_dept_nm;
                }
            }
        } else {
            $wk_belong_selected .= "＞全て";
        }
        if ($wk_room != "") {
            foreach ($sel_room_arr as $row) {
                $tmp_dept_id = $row["dept_id"];
                $tmp_room_id = $row["room_id"];
                $tmp_room_nm = $row["room_nm"];

                if ($wk_dept == $tmp_dept_id && $wk_room == $tmp_room_id) {
                    $wk_belong_selected .= "＞" . $tmp_room_nm;
                }
            }
        } else {
            $wk_belong_selected .= "＞全て";
        }

        $belong['class'] = $wk_class;
        $belong['attribute'] = $wk_attribute;
        $belong['dept'] = $wk_dept;
        $belong['room'] = $wk_room;
        $belong['selected'] = $wk_belong_selected;

        $belong_list[] = $belong;
    }

    $smarty->assign("belong_list", $belong_list);
}

//------------------------------------------------------------------------------
//集計条件：縦軸組織
//------------------------------------------------------------------------------
$vertical_axis_arr = array();
$vertical_axis_arr["class"] = '第１階層';
$vertical_axis_arr["attribute"] = '第２階層';
$vertical_axis_arr["dept"] = '第３階層';
$vertical_axis_arr["room"] = '第４階層';
$smarty->assign("vertical_axis_arr", $vertical_axis_arr);

$vertical_label = "";
foreach($vertical_axis_arr as $value => $text){
    if ($vertical_axis == $value){
        $vertical_label = $text;
        break;
    }
}
$smarty->assign("vertical_label", $vertical_label);

$smarty->assign("vertical_axis", $vertical_axis);

//------------------------------------------------------------------------------
//集計条件：横軸
//------------------------------------------------------------------------------
$horizontal_axis_arr = array();
$horizontal_axis_arr["months"] = "月別報告件数";                    // registration_date or 100,5
$horizontal_axis_arr["job"] = "当事者職種別報告件数";               // 305_,30
$horizontal_axis_arr["summary"] = "概要別報告件数";                 // 900,10
$horizontal_axis_arr["patient_level"] = "患者影響レベル別報告件数"; // 90,10
$horizontal_axis_arr["clinic"] = "診療科別報告件数";                // 130,90
$smarty->assign("horizontal_axis_arr", $horizontal_axis_arr);

$horizontal_label = "";
foreach ($horizontal_axis_arr as $value => $text) {
    if ($horizontal_axis == $value) {
        $horizontal_label = $text;
        break;
    }
}
$smarty->assign("horizontal_label", $horizontal_label);

$smarty->assign("horizontal_axis", $horizontal_axis);

//------------------------------------------------------------------------------
//集計結果
//------------------------------------------------------------------------------
$smarty->assign("show_excel_btn_flg", (PHP_VERSION >= '5'));

$smarty->assign("mode", $mode);

if ($mode == "sum"){
    $previous_year = $date_year - 1;

    $vertical_arr = array();
    $horizontal_arr = array();
    $target_arr = array();
    $result_arr = array();
    $previous_target_arr = array();
    $previous_result_arr = array();
    
    // 縦軸組織の項目データを取得
    // get_vertical_head() from self
    $vertical_arr = get_vertical_head($con, $fname, $vertical_axis);

    // 横軸の項目データを取得
    // get_horizontal_head() from self
    $horizontal_arr = get_horizontal_head($con, $fname, $horizontal_axis, $_POST);

    // 集計対象データを取得
    // get_target_data() from self
    $target_arr = get_target_data($con, $fname, $_POST, 0);

    // 集計データを表形式(配列)に変換
    // get_convert_data() from self
    $result_arr = get_convert_data($vertical_arr, $horizontal_arr, $target_arr, $_POST, 0);

    // 集計対象データを取得(前年)
    // get_target_data() from self
    $previous_target_arr = get_target_data($con, $fname, $_POST, -1);

    // 集計データを表形式(配列)に変換(前年)
    // get_convert_data() from self
    $previous_result_arr = get_convert_data($vertical_arr, $horizontal_arr, $previous_target_arr, $_POST, -1);
    
    // EXCEL出力用にセッション変数に格納
    $_SESSION["HIYARI_RM_STATS_PATTERN"]["VERTICAL"] = $vertical_arr;
    $_SESSION["HIYARI_RM_STATS_PATTERN"]["HORIZONTAL"] = $horizontal_arr;
    $_SESSION["HIYARI_RM_STATS_PATTERN"]["RESULT"] = $result_arr;
    $_SESSION["HIYARI_RM_STATS_PATTERN"]["PREVIOUS_RESULT"] = $previous_result_arr;

    // 列計
    $col_total_arr = array();
    $col_report_total_arr = array();

    // 列計(前年)
    $previous_col_total_arr = array();
    $previous_col_report_total_arr = array();

    // 合計
    $all_total = 0;
    $all_report_total = "";

    // 合計(前年)
    $previous_all_total = 0;
    $previous_all_report_total = "";

    $vertical_cnt = count($vertical_arr);
    $horizontal_cnt = count($horizontal_arr);

    $col_width = intval(80 / ($horizontal_cnt + 1));

    // 縦軸組織ループ
    // タイトルを表示するので "-1" からループ
    // 計を表示するので "<=" までループ
    $rows = array();
    for ($i = -1; $i <= $vertical_cnt; $i++) {
        // 行計
        $row_total = 0;
        $row_report_total = "";

        // 行計(前年)
        $previous_row_total = 0;
        $previous_row_report_total = "";

        // 行の1項目に対して4つの表示(指定年、前年、増減、増減率)
        for ($j = 0; $j < 4; $j++) {
            if ($i == -1 && $j > 0) {
                continue;
            }

            $row = array();

            // ボーダーの太さ
            if ($j == 0) {
                $style_str = "; border-top-width: 2px";
            } else {
                $style_str = "";
            }
            $row['style_str'] = $style_str;

            //列タイトル行フラグ
            $row['is_col_header'] = ($i == -1);

            // 横軸ループ
            // タイトルを２つ表示するので "-2" からループ
            // 計を表示するので "<=" までループ
            for ($k = -2; $k <= $horizontal_cnt; $k++) {
                $cell = array();

                //区切りフラグ
                $cell['is_delimiter'] = false;

                // ブランク(左上)
                if ($i == -1 && $k == -2) {
                    $cell['isBlank'] = true;
                }

                // ブランク(左上に統合)
                else if ($i == -1 && $k == -1) {
                    continue;
                }

                // 列ヘッダ
                else if ($i == -1) {
                    $col_header = "";
                    // 計の列ヘッダ(右上)
                    if ($k == $horizontal_cnt) {
                        $col_header = "計";
                    // 列ヘッダ(上)
                    } else {
                        $col_header = $horizontal_arr[$k]["name"];
                    }
                    $cell['col_header'] = $col_header;
                }

                // 行ヘッダ
                else if ($k == -2) {
                    if ($j > 0) {
                        continue;
                    }

                    $row_header = "";
                    // 計の行ヘッダ(左下)
                    if ($i == $vertical_cnt) {
                        $row_header = "計";
                    // 行ヘッダ(左)
                    } else {
                        $row_header = $vertical_arr[$i]["name"];
                        $cell['is_delimiter'] = true;
                    }
                    $cell['row_header'] = $row_header;                    
                }

                // 行サブヘッダ
                else if ($k == -1) {
                    $sub_header = "";
                    switch ($j) {
                        case "0":
                            $sub_header = $date_year . "年";
                            break;
                        case "1":
                            $sub_header = $previous_year . "年";
                            break;
                        case "2":
                            $sub_header = "増減";
                            break;
                        case "3":
                            $sub_header = "前年比率";
                            if ($i < $vertical_cnt){
                                $cell['is_delimiter'] = true;
                            }
                            break;
                    }
                    $cell['sub_header'] = $sub_header;
                }
                
                // 計
                else if ($i == $vertical_cnt || $k == $horizontal_cnt) {
                    // 合計(右下)
                    if ($i == $vertical_cnt && $k == $horizontal_cnt) {
                        $wk_total = $all_total;
                        $wk_report_tsv = $all_report_total;

                        $wk_previous_total = $previous_all_total;
                        $wk_previous_report_tsv = $previous_all_report_total;

                        $wk_vertical_str = "合計";
                        $wk_horizontal_str = "合計";
                    // 列計(下)
                    } else if ($i == $vertical_cnt) {
                        $wk_total = $col_total_arr[$k];
                        $wk_report_tsv = $col_report_total_arr[$k];

                        $wk_previous_total = $previous_col_total_arr[$k];
                        $wk_previous_report_tsv = $previous_col_report_total_arr[$k];

                        $wk_vertical_str = "合計";
                        $wk_horizontal_str = $horizontal_arr[$k]["name"];
                    // 行計(右)
                    } else if ($k == $horizontal_cnt) {
                        $wk_total = $row_total;
                        $wk_report_tsv = $row_report_total;

                        $wk_previous_total = $previous_row_total;
                        $wk_previous_report_tsv = $previous_row_report_total;

                        $wk_vertical_str = $vertical_arr[$i]["name"];
                        $wk_horizontal_str = "合計";
                    }

                    $sum = array();
                    switch ($j) {
                    case "0":
                        $sum['total'] = $wk_total;
                        if ($wk_total > 0) {
                            $sum['v_title'] = $wk_vertical_str;
                            $sum['h_title'] = $wk_horizontal_str;
                            $sum['report_ids'] = $wk_report_tsv;
                        }
                        break;
                    case "1":
                        $sum['total'] = $wk_previous_total;
                        if ($wk_previous_total > 0) {
                            $sum['v_title'] = $wk_vertical_str;
                            $sum['h_title'] = $wk_horizontal_str;
                            $sum['report_ids'] = $wk_previous_report_tsv;
                        }
                        break;
                    case "2":
                        $sum['total'] =$wk_total - $wk_previous_total;
                        break;
                    case "3":
                        if ($wk_previous_total == 0) {
                            $sum['total'] = "-";
                        } else {
                            $inc_dec_rate = $wk_total / $wk_previous_total * 100;
                            $sum['total'] = number_format(round($inc_dec_rate, $decimal_num), 2) . "%";
                        }
                        if ($i < $vertical_cnt){
                            $cell['is_delimiter'] = true;
                        }
                        break;
                    }
                    $cell['sum'] = $sum;
                }

                // データ
                else{
                    $num = $result_arr["data"][$i][$k];
                    $previous_num = $previous_result_arr["data"][$i][$k];
                    $report = $result_arr["report"][$i][$k];
                    $previous_report = $previous_result_arr["report"][$i][$k];

                    $data = array();
                    switch ($j) {
                    case "0":
                        // 列の開始で初期化
                        if ($i == 0) {
                            $col_total_arr[$k] = 0;
                            $col_report_total_arr[$k] = "";

                            $previous_col_total_arr[$k] = 0;
                            $previous_col_report_total_arr[$k] = "";
                        }

                        // 対象年データ加算
                        $col_total_arr[$k] += $num;
                        $row_total += $num;
                        $all_total += $num;

                        // 前年データ加算
                        $previous_col_total_arr[$k] += $previous_num;
                        $previous_row_total += $previous_num;
                        $previous_all_total += $previous_num;

                        // 対象年データのレポートID保持
                        if ($num > 0) {
                            // 列
                            if ($col_report_total_arr[$k] != "") {
                                $col_report_total_arr[$k] .= ",";
                            }
                            $col_report_total_arr[$k] .= $report;

                            // 行
                            if ($row_report_total != "") {
                                $row_report_total .= ",";
                            }
                            $row_report_total .= $report;

                            // 全体
                            if ($all_report_total != "") {
                                $all_report_total .= ",";
                            }
                            $all_report_total .= $report;
                        }

                        // 前年データのレポートID保持
                        if ($previous_num > 0) {
                            // 列
                            if ($previous_col_report_total_arr[$k] != "") {
                                $previous_col_report_total_arr[$k] .= ",";
                            }
                            $previous_col_report_total_arr[$k] .= $previous_report;

                            // 行
                            if ($previous_row_report_total != "") {
                                $previous_row_report_total .= ",";
                            }
                            $previous_row_report_total .= $previous_report;

                            // 全体
                            if ($previous_all_report_total != "") {
                                $previous_all_report_total .= ",";
                            }
                            $previous_all_report_total .= $previous_report;
                        }

                        $data['num'] = $num;
                        if ($num > 0) {
                            $data['v_title'] = $vertical_arr[$i]["name"];
                            $data['h_title'] = $horizontal_arr[$k]["name"];
                            $data['report_ids'] = $report;
                        }
                        break;
                    case "1":
                        $data['num'] = $previous_num;
                        if ($previous_num > 0) {
                            $data['v_title'] = $vertical_arr[$i]["name"];
                            $data['h_title'] = $horizontal_arr[$k]["name"];
                            $data['report_ids'] = $previous_report;
                        }
                        break;
                    case "2":
                        $data['num'] = $num - $previous_num;
                        break;
                    case "3":
                        if ($previous_num == 0) {
                            $data['num'] = "-";
                        } else {
                            $inc_dec_rate = $num / $previous_num * 100;
                            $data['num'] = number_format(round($inc_dec_rate, $decimal_num), 2) . "%";
                        }
                        if ($i < $vertical_cnt){
                            $cell['is_delimiter'] = true;
                        }
                        break;
                    }
                    $cell['data'] = $data;
                }

                $row['cells'][] = $cell;
            }
            $rows[] = $row;
        }
    }
    
    $smarty->assign("col_width", $col_width);
    $smarty->assign("rows", $rows);
}

//------------------------------------------------------------------------------
//表示
//------------------------------------------------------------------------------
if ($design_mode == 1){
    $smarty->display("hiyari_rm_stats_pattern1.tpl");
}
else{
    $smarty->display("hiyari_rm_stats_pattern2.tpl");
}

// DB接続の切断
pg_close($con);

/**
 * 指定ヶ月後の年月を取得する
 *
 * @param  string  $year  年
 * @param  string  $month 月
 * @param  string  $num   指定ヶ月
 * @return string         指定ヶ月後の年月
 */
function add_month($year, $month, $num)
{
	if ((int)$month + (int)$num > 12) {
		$month = ((int)$month + (int)$num) - 12;
		$year++;
	} else if ((int)$month + (int)$num <= 0) {
		$month = 12 + ((int)$month + (int)$num);
		$year--;
	} else {
		$month = (int)$month + (int)$num;
	}
	return $year . "/" . substr("00" . $month, -2);
}

/**
 * 縦軸組織の項目データを取得する
 *
 * @param  object $con           DBコネクション
 * @param  string $fname         画面名
 * @param  string $vertical_axis 縦軸組織
 * @return array                 縦軸項目データ配列
 */
function get_vertical_head($con, $fname, $vertical_axis)
{
	$vertical_arr = array();
	
	switch ($vertical_axis) {
		case "class":
			// ==================================================
			// 部門一覧取得
			// get_class_mst() from hiyari_common.ini
			// ==================================================
			$sel = get_class_mst($con, $fname);
			
			$class_arr = array();
			$class_arr = pg_fetch_all($sel);
			
			$i = 0;
			foreach ($class_arr as $arr) {
				$vertical_arr[$i]["upper_code"] = "nothing";
				$vertical_arr[$i]["code"] = $arr["class_id"];
				$vertical_arr[$i]["name"] = $arr["class_nm"];
				$i++;
			}
			
			break;
			
		case "attribute":
			// ==================================================
			// 課一覧取得
			// get_atrb_mst() from hiyari_common.ini
			// ==================================================
			$sel = get_atrb_mst($con, $fname);
			
			$atrb_arr = array();
			$atrb_arr = pg_fetch_all($sel);
			
			$i = 0;
			foreach ($atrb_arr as $arr) {
				$vertical_arr[$i]["upper_code"] = $arr["class_id"];
				$vertical_arr[$i]["code"] = $arr["atrb_id"];
				$vertical_arr[$i]["name"] = $arr["atrb_nm"];
				$i++;
			}
			
			break;
			
		case "dept":
			// ==================================================
			// 科一覧取得
			// get_dept_mst() from hiyari_common.ini
			// ==================================================
			$sel = get_dept_mst($con, $fname);
			
			$dept_arr = array();
			$dept_arr = pg_fetch_all($sel);
			
			$i = 0;
			foreach ($dept_arr as $arr) {
				$vertical_arr[$i]["upper_code"] = $arr["atrb_id"];
				$vertical_arr[$i]["code"] = $arr["dept_id"];
				$vertical_arr[$i]["name"] = $arr["dept_nm"];
				$i++;
			}
			
			break;
			
		case "room":
			// ==================================================
			// 室一覧取得
			// get_room_mst() from hiyari_common.ini
			// ==================================================
			$sel = get_room_mst($con, $fname);
			
			$room_arr = array();
			$room_arr = pg_fetch_all($sel);
			
			$i = 0;
			foreach ($room_arr as $arr) {
				$vertical_arr[$i]["upper_code"] = $arr["dept_id"];
				$vertical_arr[$i]["code"] = $arr["room_id"];
				$vertical_arr[$i]["name"] = $arr["room_nm"];
				$i++;
			}
			
			break;
	}
	
	$vertical_arr[$i]["upper_code"] = "";
	$vertical_arr[$i]["code"] = "";
	$vertical_arr[$i]["name"] = "不明";
	
	return $vertical_arr;
}

/**
 * 横軸の項目データを取得する
 *
 * @param  object $con           DBコネクション
 * @param  string $fname         画面名
 * @param  string $vertical_axis 縦軸組織
 * @param  array  $post          画面データ
 * @return array                 横軸項目データ配列
 */
function get_horizontal_head($con, $fname, $horizontal_axis, $post)
{
	$horizontal_arr = array();
	
	switch ($horizontal_axis) {
		// 月別報告件数
		case "months":
			$date_year = $post["date_year"];
			$date_month = $post["date_month"];
			$date_term = $post["date_term"];
			
			for ($i = 0; $i < (int)$date_term; $i++) {
				$horizontal_arr[$i]["code"] = add_month($date_year, $date_month, $i);
				$horizontal_arr[$i]["name"] = add_month($date_year, $date_month, $i);
			}
			
			break;
		// 当事者職種別報告件数
		case "job":
			$sql = "select distinct";
				$sql .= " easy_code";
				$sql .= ", easy_name";
				$sql .= ", easy_num";
			$sql .= " from";
				$sql .= " inci_report_materials";
			$sql .= " where";
				$sql .= " easy_flag = '1'";
				$sql .= " and grp_code::text like '305_'";
				$sql .= " and easy_item_code = 30";
				$sql .= " and easy_code not in (";
					$sql .= "select distinct";
						$sql .= " easy_code";
					$sql .= " from";
						$sql .= " inci_easyinput_item_element_no_disp";
					$sql .= " where";
						$sql .= " grp_code::text like '305_'";
						$sql .= " and easy_item_code = 30";
				$sql .= ")";
			$sql .= " order by";
				$sql .= " easy_num";
			
			// ==================================================
			// SQL実行
			// select_from_table() from about_postgres.php
			// ==================================================
			$sel = select_from_table($con, $sql, "", $fname);
			if ($sel == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				showErrorPage();
				exit;
			}
			
			$job_arr = pg_fetch_all($sel);
			
			$i = 0;
			foreach ($job_arr as $arr) {
				$horizontal_arr[$i]["code"] = $arr["easy_code"];
				$horizontal_arr[$i]["name"] = $arr["easy_name"];
				
				$i++;
			}
			
			$horizontal_arr[$i]["code"] = "";
			$horizontal_arr[$i]["name"] = "未入力";
			
			break;
		// 概要別報告件数
		case "summary":
			$sql = "select";
				$sql .= " super_item_id";
				$sql .= ", super_item_name";
			$sql .= " from";
				$sql .= " inci_super_item_2010";
			$sql .= " where";
				$sql .= " disp_flg = 't'";
				$sql .= " and available = 't'";
			$sql .= " order by";
				$sql .= " super_item_order";
			
			// ==================================================
			// SQL実行
			// select_from_table() from about_postgres.php
			// ==================================================
			$sel = select_from_table($con, $sql, "", $fname);
			if ($sel == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				showErrorPage();
				exit;
			}
			
			$summary_arr = pg_fetch_all($sel);
			
			$i = 0;
			foreach ($summary_arr as $arr) {
				$horizontal_arr[$i]["code"] = $arr["super_item_id"];
				$horizontal_arr[$i]["name"] = $arr["super_item_name"];
				
				$i++;
			}
			
			break;
		// 患者影響レベル別報告件数
		case "patient_level":
			$sql = "select";
				$sql .= " a.easy_code";
				$sql .= ", a.easy_name";
			$sql .= " from";
				$sql .= " (";
					$sql .= "select";
						$sql .= " *";
					$sql .= " from";
						$sql .= " inci_report_materials";
					$sql .= " where";
						$sql .= " easy_flag = '1'";
						$sql .= " and grp_code = 90";
						$sql .= " and easy_item_code = 10";
						$sql .= " and easy_code not in (";
							$sql .= "select distinct";
								$sql .= " easy_code";
							$sql .= " from";
								$sql .= " inci_easyinput_item_element_no_disp";
							$sql .= " where";
								$sql .= " grp_code = 90";
								$sql .= " and easy_item_code = 10";
						$sql .= ")";
				$sql .= ") a";
				$sql .= " left join";
				$sql .= " inci_level_message b";
				$sql .= " on";
				$sql .= " a.easy_code = b.easy_code";
			$sql .= " where";
				$sql .= " b.use_flg = 't'";
			$sql .= " order by";
				$sql .= " a.easy_num";
			
			// ==================================================
			// SQL実行
			// select_from_table() from about_postgres.php
			// ==================================================
			$sel = select_from_table($con, $sql, "", $fname);
			if ($sel == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				showErrorPage();
				exit;
			}
			
			$level_arr = pg_fetch_all($sel);
			
			$i = 0;
			foreach ($level_arr as $arr) {
				$horizontal_arr[$i]["code"] = $arr["easy_code"];
				$horizontal_arr[$i]["name"] = $arr["easy_name"];
				
				$i++;
			}
			
			$horizontal_arr[$i]["code"] = "";
			$horizontal_arr[$i]["name"] = "未入力";
			
			break;
		// 診療科別報告件数
		case "clinic":
			$sql = "select";
				$sql .= " easy_code";
				$sql .= ", easy_name";
				$sql .= ", easy_num";
			$sql .= " from";
				$sql .= " inci_report_materials";
			$sql .= " where";
				$sql .= " easy_flag = '1'";
				$sql .= " and grp_code = 130";
				$sql .= " and easy_item_code = 90";
				$sql .= " and easy_code not in (";
					$sql .= "select distinct";
						$sql .= " easy_code";
					$sql .= " from";
						$sql .= " inci_easyinput_item_element_no_disp";
					$sql .= " where";
						$sql .= " grp_code = 130";
						$sql .= " and easy_item_code = 90";
				$sql .= ")";
			$sql .= " order by";
				$sql .= " easy_num";
			
			// ==================================================
			// SQL実行
			// select_from_table() from about_postgres.php
			// ==================================================
			$sel = select_from_table($con, $sql, "", $fname);
			if ($sel == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				showErrorPage();
				exit;
			}
			
			$clinic_arr = pg_fetch_all($sel);
			
			$i = 0;
			foreach ($clinic_arr as $arr) {
				$horizontal_arr[$i]["code"] = $arr["easy_code"];
				$horizontal_arr[$i]["name"] = $arr["easy_name"];
				
				$i++;
			}
			
			$horizontal_arr[$i]["code"] = "";
			$horizontal_arr[$i]["name"] = "未入力";
			
			break;
	}
	
	return $horizontal_arr;
}

/**
 * 集計対象データを取得する
 *
 * @param  object $con      DBコネクション
 * @param  string $fname    画面名
 * @param  array  $post     画面データ
 * @param  string $year_num 年数指定値
 * @return array            集計対象データ配列
 */
function get_target_data($con, $fname, $post, $year_num)
{
	// 集計モード
	$date_mode = $post["date_mode"];
	
	// 集計年月
	$date_year = $post["date_year"] + $year_num;
	$date_month = $post["date_month"];
	$date_term = $post["date_term"];
	
	// 所属データ(配列)
	$belong_cnt = $post["belong_cnt"];
	$belong_class = $post["belong_class"];
	$belong_attribute = $post["belong_attribute"];
	$belong_dept = $post["belong_dept"];
	$belong_room = $post["belong_room"];
	
	// 副報告フラグ
	$sub_report_use = $post["sub_report_use"];
	
	// 縦軸組織項目
	$vertical_axis = $post["vertical_axis"];
	
	// 横軸項目
	$horizontal_axis = $post["horizontal_axis"];
	
	// 日付の検索範囲を取得
	$search_start_date = $date_year . "/" . $date_month;
	$search_end_date = add_month($date_year, $date_month, $date_term - 1);		// 月までの指定なので、-1して調整
	
	// 検索する所属のwhere句生成(検索する所属一式の数が可変)
	$where_belong = "";
	for ($i = 0; $i < $belong_cnt; $i++) {
		$row_str = "";
		
		if ($belong_class[$i] != "") {
			$row_str .= "(registrant_class = " . $belong_class[$i];
		}
		if ($belong_attribute[$i] != "") {
			$row_str .= " and registrant_attribute = " . $belong_attribute[$i];
		}
		if ($belong_dept[$i] != "") {
			$row_str .= " and registrant_dept = " . $belong_dept[$i];
		}
		if ($belong_room[$i] != "") {
			$row_str .= " and registrant_room = " . $belong_room[$i];
		}
		if ($row_str != "") {
			$row_str .= ")";
		}
		
		if ($where_belong != "" && $row_str != "") {
			$where_belong .= " or ";
		}
		
		$where_belong .= $row_str;
	}
	if ($where_belong != "") {
		$where_belong = " and (" . $where_belong . ")";
	}
	
	$sql = "select";
		$sql .= " header.report_id";
		// 集計時簡素化の為、同名のエイリアスを設定
		switch ($vertical_axis) {
			case "class":
				$sql .= ", 'nothing' as v_upper";
				$sql .= ", header.registrant_class as v_code";
				
				break;
			case "attribute":
				$sql .= ", header.registrant_class as v_upper";
				$sql .= ", header.registrant_attribute as v_code";
				
				break;
			case "dept":
				$sql .= ", header.registrant_attribute as v_upper";
				$sql .= ", header.registrant_dept as v_code";
				
				break;
			case "room":
				$sql .= ", header.registrant_dept as v_upper";
				$sql .= ", header.registrant_room as v_code";
				
				break;
		}
		// 横軸が月別の場合はエイリアス[header]のデータを取得
		if ($horizontal_axis == "months") {
			// 集計モード：報告日
			if ($date_mode == "report_date") {
				$sql .= ", substr(header.registration_date, 1, 7) as input_item";
			// 集計モード：発生日
			} else if ($date_mode == "incident_date") {
				$sql .= ", substr(detail.input_item, 1, 7) as input_item";
			}
		// 横軸が月別"以外"はエイリアス[detail]のデータを取得
		} else {
			$sql .= ", detail.input_item";
		}
	$sql .= " from";
		$sql .= " (";
			$sql .= "select";
				$sql .= " base.*";
			$sql .= " from";
				$sql .= " inci_report base";
				$sql .= " join";
				// 必ず指定される条件(集計モード、集計年月、副報告、所属一式)はエイリアス[search_result]で絞り込んでおく
				$sql .= " (";
					$sql .= "select distinct";
						$sql .= " a.report_id";
					$sql .= " from";
						// 副報告を含める場合は全てのレポートが対象
						if ($sub_report_use == "true") {
							$sql .= " inci_report a";
						// 副報告を含めない場合は主レポート＋リンクされていないレポートが対象
						} else {
							$sql .= " (";
								$sql .= "select";
									$sql .= " data.*";
								$sql .= " from";
									$sql .= " inci_report data";
									$sql .= " left outer join";
									$sql .= " inci_report_link link";
									$sql .= " on";
									$sql .= " data.report_id = link.report_id";
								$sql .= " where";
									$sql .= " link.main_flg = 't'";
									$sql .= " or link.main_flg is Null";
							$sql .= ") a";
						}
					$sql .= " where";
						// 集計モード：報告日
						if ($date_mode == "report_date") {
							$sql .= " a.registration_date between '" . $search_start_date . "/00' and '" . $search_end_date . "/99'";
						// 集計モード：発生日
						} else if ($date_mode == "incident_date") {
							$sql .= " a.incident_date between '" . $search_start_date . "/00' and '" . $search_end_date . "/99'";
						}
						$sql .= " and shitagaki_flg = false";
						$sql .= " and del_flag = false";
						$sql .= " and kill_flg = false";
						// 所属
						$sql .= $where_belong;
				$sql .= ") search_result";
				$sql .= " on";
				$sql .= " base.report_id = search_result.report_id";
		$sql .= ") header";
		// 集計モードが報告日、横軸が月別の場合"以外"はエイリアス[detail]を使用
		if (!($date_mode == "report_date" && $horizontal_axis == "months")) {
			$sql .= " left outer join";
			$sql .= " (";
				$sql .= "select";
					$sql .= " *";
				$sql .= " from";
					$sql .= " inci_easyinput_data";
				$sql .= " where";
					// 横軸別where句生成
					switch ($horizontal_axis) {
						// 月別報告件数
						case "months":
							$sql .= " grp_code = 100";
							$sql .= " and easy_item_code = 5";
							
							break;
						// 当事者職種別報告件数
						case "job":
							$sql .= " grp_code::text like '305_'";
							$sql .= " and easy_item_code = 30";
							
							break;
						// 概要別報告件数
						case "summary":
							$sql .= " grp_code = 900";
							$sql .= " and easy_item_code = 10";
							
							break;
						// 患者影響レベル別報告件数
						case "patient_level":
							$sql .= " grp_code = 90";
							$sql .= " and easy_item_code = 10";
							
							break;
						// 診療科別報告件数
						case "clinic":
							$sql .= " grp_code = 130";
							$sql .= " and easy_item_code = 90";
							
							break;
					}
			$sql .= ") detail";
			$sql .= " on";
			$sql .= " header.eid_id = detail.eid_id";
		}
	
	// ==================================================
	// SQL実行
	// select_from_table() from about_postgres.php
	// ==================================================
	$sel = select_from_table($con, $sql, "", $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		showErrorPage();
		exit;
	}
	
	return pg_fetch_all($sel);
}

/**
 * 集計データを表形式(配列)に変換する
 *
 * @param  array  $vertical_arr   縦軸組織項目データ配列
 * @param  array  $horizontal_arr 横軸項目データ配列
 * @param  array  $target_arr     集計対象データ配列
 * @param  array  $post           画面データ
 * @param  string $year_num       年数指定値
 * @return array                  変換済み集計データ配列
 */
function get_convert_data($vertical_arr, $horizontal_arr, $target_arr, $post, $year_num)
{
	$horizontal_axis = $post["horizontal_axis"];
	
	$data_arr = array();
	
	// ==================================================
	// データ集計：通常分
	// ==================================================
	// 縦軸組織ループ
	for ($i = 0; $i < count($vertical_arr); $i++) {
		$v_upper = $vertical_arr[$i]["upper_code"];
		$v_code = $vertical_arr[$i]["code"];
		
		// 横軸ループ
		for ($j = 0; $j < count($horizontal_arr); $j++) {
			if ($horizontal_axis == "months") {
				$wk_date = $horizontal_arr[$j]["code"];
				
				$h_code = add_month(substr($wk_date, 0, 4), substr($wk_date, 5, 2), $year_num * 12);
			} else {
				$h_code = $horizontal_arr[$j]["code"];
			}
			
			// セルデータの初期化
			$data_arr["data"][$i][$j] = 0;
			$data_arr["report"][$i][$j] = "";
			
			// 取得データループ
			foreach ($target_arr as $result) {
				$wk_report = $result["report_id"];
				$wk_upper = $result["v_upper"];
				$wk_code = $result["v_code"];
				$wk_item = $result["input_item"];
				
				$split_arr = array();
				$tmp_item = "";
				
				// 関連診療科のみTSV形式
				if ($horizontal_axis == "clinic") {
					$split_arr = explode("\t", $wk_item);
				} else {
					$split_arr[0] = $wk_item;
				}
				
				foreach ($split_arr as $tmp_item) {
					// 縦、横軸のキーが一致
					if ($v_upper == $wk_upper && $v_code == $wk_code && $h_code == $tmp_item) {
						// 集計数をインクリメント
						$data_arr["data"][$i][$j]++;
						
						if ($data_arr["report"][$i][$j] != "") {
							$data_arr["report"][$i][$j] .= ",";
						}
						$data_arr["report"][$i][$j] .= $wk_report;
					}
				}
			}
		}
	}
	
	// ==================================================
	// データ集計：不明・未入力分
	// ==================================================
	// 取得データループ
	foreach ($target_arr as $result) {
		$wk_report = $result["report_id"];
		$wk_upper = $result["v_upper"];
		$wk_code = $result["v_code"];
		$wk_item = $result["input_item"];
		
		// 関連診療科のみTSV形式
		if ($horizontal_axis == "clinic") {
			$split_arr = explode("\t", $wk_item);
		} else {
			$split_arr[0] = $wk_item;
		}
		
		foreach ($split_arr as $tmp_item) {
			$v_all_flg = 0;
			$h_flg = 0;
			
			// 縦軸組織ループ
			for ($i = 0; $i < count($vertical_arr); $i++) {
				$v_upper = $vertical_arr[$i]["upper_code"];
				$v_code = $vertical_arr[$i]["code"];
				
				$v_flg = 0;
				
				// 横軸ループ
				for ($j = 0; $j < count($horizontal_arr); $j++) {
					if ($horizontal_axis == "months") {
						$wk_date = $horizontal_arr[$j]["code"];
						
						$h_code = add_month(substr($wk_date, 0, 4), substr($wk_date, 5, 2), $year_num * 12);
					} else {
						$h_code = $horizontal_arr[$j]["code"];
					}
					
					// 縦軸のキーが一致
					if ($v_upper == $wk_upper && $v_code == $wk_code) {
						$v_flg = 1;
						$v_all_flg = 1;
					}
					
					// 横軸のキーが一致
					if ($h_code == $tmp_item) {
						$h_flg = 1;
					}
					
					// 当事者職種、患者影響レベル、診療科の場合は"未入力"の集計数をカウント
					if ($horizontal_axis == "job" || $horizontal_axis == "patient_level" || $horizontal_axis == "clinic") {
						if ($j == count($horizontal_arr) - 1) {
							if ($v_flg == 1 && $h_flg == 0) {
								// 未入力の集計数をインクリメント
								$data_arr["data"][$i][$j]++;
								
								if ($data_arr["report"][$i][$j] != "") {
									$data_arr["report"][$i][$j] .= ",";
								}
								$data_arr["report"][$i][$j] .= $wk_report;
								
								break 2;
							}
						}
					}
					
					if ($i == count($vertical_arr) - 1) {
						if ($v_all_flg == 0 && $h_flg == 1) {
							if ($h_code == $tmp_item) {
								// 不明の集計数をインクリメント
								$data_arr["data"][$i][$j]++;
								
								if ($data_arr["report"][$i][$j] != "") {
									$data_arr["report"][$i][$j] .= ",";
								}
								$data_arr["report"][$i][$j] .= $wk_report;
								
								break 2;
							}
						}
					}
					
					if ($i == count($vertical_arr) - 1 && $j == count($horizontal_arr) - 1) {
						if ($v_all_flg == 0 && $h_flg == 0) {
							// 未入力、不明の集計数をインクリメント
							$data_arr["data"][$i][$j]++;
							
							if ($data_arr["report"][$i][$j] != "") {
								$data_arr["report"][$i][$j] .= ",";
							}
							$data_arr["report"][$i][$j] .= $wk_report;
							
							break 2;
						}
					}
				}
			}
		}
	}
	
	return $data_arr;
}
?>