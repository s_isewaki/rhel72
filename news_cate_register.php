<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix お知らせ管理 | カテゴリ登録</title>
<?php
require("about_session.php");
require("about_authority.php");
require("referer_common.ini");
require("news_common.ini");
require_once("Cmx.php");
require_once("Cmx/Model/SystemConfig.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 24, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 遷移元の取得
$referer = get_referer($con, $session, "news", $fname);

// カテゴリ一覧を取得 1:全館〜職種
$category_list_db = get_category_list_from_db($con, $fname, 1, null, false);
$category_list = array();
foreach ($category_list_db as $category_id => $category_name) {
    $category_list[$category_id] = $category_name["newscate_name"];
}

// 部署情報を配列で取得
$class_list = get_class_list($con, $fname);
$atrb_list = get_atrb_list($con, $fname);
$dept_list = get_dept_list($con, $fname);
$room_list = get_room_list($con, $fname);

// 職種一覧を取得
$job_list = get_job_list($con, $fname);

// 通達区分一覧を取得
$notice_list = get_notice_list($con, $fname);

// 役職一覧を取得
$st_list = get_st_list($con, $fname);

// お知らせ集計機能設定を取得
$conf = new Cmx_SystemConfig();
$setting_aggregate = $conf->get('setting_aggregate');

//オプションから引用した設定が"t"ではなかった場合に、"f"を設定する。
if($setting_aggregate != "t"){
	$setting_aggregate = "f";
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function initPage() {
<?php
	if (!is_array($class_id)) {
		echo("\tsetClassOptions(0);\n");
	} else {
		for ($i = 0, $j = count($class_id); $i < $j; $i++) {
			echo("\tsetClassOptions($i, '$class_id[$i]', '$atrb_id[$i]', '$dept_id[$i]');\n");
		}
	}
	if (!is_array($job_id)) {
		echo("\tsetJobOptions(0);\n");
	} else {
		for ($i = 0, $j = count($job_id); $i < $j; $i++) {
			echo("\tsetJobOptions($i, '$job_id[$i]');\n");
		}
	}
	if (!is_array($st_id)) {
		echo("\tsetStOptions(0);\n");
	} else {
		for ($i = 0, $j = count($st_id); $i < $j; $i++) {
			echo("\tsetStOptions($i, '$st_id[$i]');\n");
		}
	}
?>
	showSubCategory();
}

function setClassOptions(row_id, class_id, atrb_id, dept_id) {
	var table = document.getElementById('depts');
	if (row_id > table.rows.length - 1) {
		addDeptRow();
	}

	var class_elm;
	if (document.mainform.elements['class_id[]'].options) {
		class_elm = document.mainform.elements['class_id[]'];
	} else {
		class_elm = document.mainform.elements['class_id[]'][row_id];
	}

	deleteAllOptions(class_elm);
<?php
foreach ($class_list as $tmp_class_id => $tmp_class_nm) {
	echo("\taddOption(class_elm, '$tmp_class_id', '$tmp_class_nm', class_id);\n");
}
?>
	setAtrbOptions(row_id, atrb_id, dept_id);
}

function setAtrbOptions(row_id, atrb_id, dept_id) {
	var atrb_elm;
	if (document.mainform.elements['atrb_id[]'].options) {
		atrb_elm = document.mainform.elements['atrb_id[]'];
	} else {
		atrb_elm = document.mainform.elements['atrb_id[]'][row_id];
	}

	deleteAllOptions(atrb_elm);
	addOption(atrb_elm, '0', 'すべて', atrb_id);

	var class_id;
	if (document.mainform.elements['class_id[]'].options) {
		class_id = document.mainform.elements['class_id[]'].value;
	} else {
		class_id = document.mainform.elements['class_id[]'][row_id].value;
	}

	switch (class_id) {
<?php
foreach ($atrb_list as $tmp_class_id => $atrbs_in_class) {
	echo("\tcase '$tmp_class_id':\n");
	foreach ($atrbs_in_class as $tmp_atrb_id => $tmp_atrb_nm) {
		echo("\t\taddOption(atrb_elm, '$tmp_atrb_id', '$tmp_atrb_nm', atrb_id);\n");
	}
	echo("\t\tbreak;\n");
}
?>
	}

	setDeptOptions(row_id, dept_id);
}

function setDeptOptions(row_id, dept_id) {
	var dept_elm;
	if (document.mainform.elements['dept_id[]'].options) {
		dept_elm = document.mainform.elements['dept_id[]'];
	} else {
		dept_elm = document.mainform.elements['dept_id[]'][row_id];
	}

	deleteAllOptions(dept_elm);
	addOption(dept_elm, '0', 'すべて', dept_id);

	var atrb_id;
	if (document.mainform.elements['atrb_id[]'].options) {
		atrb_id = document.mainform.elements['atrb_id[]'].value;
	} else {
		atrb_id = document.mainform.elements['atrb_id[]'][row_id].value;
	}

	switch (atrb_id) {
<?php
foreach ($dept_list as $tmp_atrb_id => $depts_in_atrb) {
	echo("\tcase '$tmp_atrb_id':\n");
	foreach ($depts_in_atrb as $tmp_dept_id => $tmp_dept_nm) {
		echo("\t\taddOption(dept_elm, '$tmp_dept_id', '$tmp_dept_nm', dept_id);\n");
	}
	echo("\t\tbreak;\n");
}
?>
	}
}

function showSubCategory() {
	document.getElementById('sub_category1').style.display = 'none';
	document.getElementById('sub_category2').style.display = 'none';
	document.getElementById('sub_category3').style.display = 'none';

	switch (document.mainform.news_category.value) {
	case '1':  // 全館
		document.getElementById('sub_category1').style.display = '';
		break;
	case '2':  // 部門
		document.getElementById('sub_category2').style.display = '';
		break;
	case '3':  // 職種
		document.getElementById('sub_category3').style.display = '';
		break;
	}
}

function addDeptRow() {
	var addbtn = document.getElementById('addbtn');
	addbtn.parentNode.removeChild(addbtn);

	var table = document.getElementById('depts');
	var row_id = table.rows.length;
	var row = table.insertRow(row_id);
	var cell = row.insertCell(0);
	cell.innerHTML = '<select name="class_id[]" onchange="setAtrbOptions(this.parentNode.parentNode.rowIndex);"><\/select><select name="atrb_id[]" onchange="setDeptOptions(this.parentNode.parentNode.rowIndex);"><\/select><select name="dept_id[]"><\/select><input type="button" value="削除" onclick="deleteDeptRow(this.parentNode.parentNode.rowIndex);" style="margin-left:2px;"><input id="addbtn" type="button" value="追加" onclick="addDeptRow();" style="margin-left:2px;">';

	var class_id = document.mainform.elements['class_id[]'][row_id - 1].value;
	var atrb_id = document.mainform.elements['atrb_id[]'][row_id - 1].value;
	var dept_id = document.mainform.elements['dept_id[]'][row_id - 1].value;
	setClassOptions(row_id, class_id, atrb_id, dept_id);
}

function deleteDeptRow(row_id) {
	var table = document.getElementById('depts');
	table.rows[row_id].parentNode.removeChild(table.rows[row_id]);

	if(!document.getElementById('addbtn')) {
		table.rows[table.rows.length - 1].cells[0].innerHTML += '<input id="addbtn" type="button" value="追加" onclick="addDeptRow();" style="margin-left:2px;">';
	}
}

function setJobOptions(row_id, job_id) {
	var table = document.getElementById('jobs');
	if (row_id > table.rows.length - 1) {
		addJobRow();
	}

	var job_elm;
	if (document.mainform.elements['job_id[]'].options) {
		job_elm = document.mainform.elements['job_id[]'];
	} else {
		job_elm = document.mainform.elements['job_id[]'][row_id];
	}

	deleteAllOptions(job_elm);
<?php
foreach ($job_list as $tmp_job_id => $tmp_job_nm) {
	echo("\taddOption(job_elm, '$tmp_job_id', '$tmp_job_nm', job_id);\n");
}
?>
}

function addJobRow() {
	var addbtn = document.getElementById('addjobbtn');
	addbtn.parentNode.removeChild(addbtn);

	var table = document.getElementById('jobs');
	var row_id = table.rows.length;
	var row = table.insertRow(row_id);
	var cell = row.insertCell(0);
	cell.innerHTML = '<select name="job_id[]"><\/select><input type="button" value="削除" onclick="deleteJobRow(this.parentNode.parentNode.rowIndex);" style="margin-left:2px;"><input id="addjobbtn" type="button" value="追加" onclick="addJobRow();" style="margin-left:2px;">';

	var job_id = document.mainform.elements['job_id[]'][row_id - 1].value;
	setJobOptions(row_id, job_id);
}

function deleteJobRow(row_id) {
	var table = document.getElementById('jobs');
	table.rows[row_id].parentNode.removeChild(table.rows[row_id]);

	if(!document.getElementById('addjobbtn')) {
		table.rows[table.rows.length - 1].cells[0].innerHTML += '<input id="addjobbtn" type="button" value="追加" onclick="addJobRow();" style="margin-left:2px;">';
	}
}

function deleteAllOptions(box) {
	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}
}

function addOption(box, value, text, selected) {
	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	box.options[box.length] = opt;
	if (selected == value) {
		box.options[box.length - 1].selected = true;
	}
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
}

function setStOptions(row_id, st_id) {
	var table = document.getElementById('sts');
	if (row_id > table.rows.length - 1) {
		addStRow();
	}

	var st_elm;
	if (document.mainform.elements['st_id[]'].options) {
		st_elm = document.mainform.elements['st_id[]'];
	} else {
		st_elm = document.mainform.elements['st_id[]'][row_id];
	}

	deleteAllOptions(st_elm);
<?php
	echo("\taddOption(st_elm, '', '　', st_id);\n");
foreach ($st_list as $tmp_st_id => $tmp_st_nm) {
	echo("\taddOption(st_elm, '$tmp_st_id', '$tmp_st_nm', st_id);\n");
}
?>
}

function addStRow() {
	var addbtn = document.getElementById('addstbtn');
	addbtn.parentNode.removeChild(addbtn);

	var table = document.getElementById('sts');
	var row_id = table.rows.length;
	var row = table.insertRow(row_id);
	var cell = row.insertCell(0);
	cell.innerHTML = '<select name="st_id[]"><\/select><input type="button" value="削除" onclick="deleteStRow(this.parentNode.parentNode.rowIndex);" style="margin-left:2px;"><input id="addstbtn" type="button" value="追加" onclick="addStRow();" style="margin-left:2px;">';

	var st_id = document.mainform.elements['st_id[]'][row_id - 1].value;
	setStOptions(row_id, st_id);
}

function deleteStRow(row_id) {
	var table = document.getElementById('sts');
	table.rows[row_id].parentNode.removeChild(table.rows[row_id]);

	if(!document.getElementById('addstbtn')) {
		table.rows[table.rows.length - 1].cells[0].innerHTML += '<input id="addstbtn" type="button" value="追加" onclick="addStRow();" style="margin-left:2px;">';
	}
}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-style:none;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<?php if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="news_menu.php?session=<? echo($session); ?>"><img src="img/icon/b21.gif" width="32" height="32" border="0" alt="お知らせ・回覧板管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="news_menu.php?session=<? echo($session); ?>"><b>お知らせ・回覧板管理</b></a></font></td>
</tr>
</table>
<?php } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="newsuser_menu.php?session=<? echo($session); ?>"><img src="img/icon/b35.gif" width="32" height="32" border="0" alt="お知らせ・回覧板"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="newsuser_menu.php?session=<?php echo($session); ?>"><b>お知らせ・回覧板</b></a> &gt; <a href="news_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="newsuser_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<?php } ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="90" height="22" align="center" bgcolor="#bdd1e7"><a href="news_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_search.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ検索</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_notice_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">通達区分一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_notice_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">通達区分登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_notice2_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ区分<br>一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_notice2_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ区分<br>登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_cate_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カテゴリ一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#5279a5"><a href="news_cate_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>カテゴリ登録</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="60" align="center" bgcolor="#bdd1e7"><a href="news_trash.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ゴミ箱</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="news_option.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション</font></a></td>
<td width="5">&nbsp;</td>
		<?php if($setting_aggregate == 't'){?>
		<td width="70" align="center" bgcolor="#bdd1e7">
			<a href="news_aggregate.php?session=<? echo($session); ?>">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">集計表</font>
			</a>
		</td>
		<?php }?><td width="">&nbsp;</td><td align="right">&nbsp;</td><td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form action="news_cate_insert.php" method="post" name="mainform">
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="140" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カテゴリ名称</font></td>
<td><input type="text" name="newscate_name" value="" size="40" maxlength="40" style="ime-mode:active;"></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td width="140" align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>通知範囲の条件</b></font></td>
<td></td>
</tr>
</table>

<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="28">
<td width="140" valign="top" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">対象カテゴリ</font></td>
<td colspan="3">
<table border="0" cellspacing="0" cellpadding="1">
<tr valign="top">
<td style="padding-right:4px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="news_category" onchange="showSubCategory();"><?php show_options($category_list, $news_category); ?></select></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<div id="sub_category1" style="display:none;"></div>
<div id="sub_category2" style="display:none;">
<table id="depts" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="class_id[]" onchange="setAtrbOptions(0);"></select><select name="atrb_id[]" onchange="setDeptOptions(0);"></select><select name="dept_id[]"></select><input type="button" value="削除" disabled style="margin-left:2px;"><input id="addbtn" type="button" value="追加" onclick="addDeptRow();" style="margin-left:2px;"></td>
</tr>
</table>
</div>
<div id="sub_category3" style="display:none;">
<table id="jobs" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><select name="job_id[]"><? show_options($job_list, $job_id); ?></select><input type="button" value="削除" disabled style="margin-left:2px;"><input id="addjobbtn" type="button" value="追加" onclick="addJobRow();" style="margin-left:2px;"></td>
</tr>
</table></div>
</font></td>
</tr>
</table>
</td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">対象役職</font></td>
<td colspan="3">
<table id="sts" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<select name="st_id[]"></select><input type="button" value="削除" disabled style="margin-left:2px;"><input id="addstbtn" type="button" value="追加" onclick="addStRow();" style="margin-left:2px;"></td>
</tr>
</table>
</td>
</tr>

</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
