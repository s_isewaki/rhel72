<?php
require_once('Cmx.php');
require_once('Cmx/View/Smarty.php');
require_once('about_comedix.php');
require_once("hiyari_menu_news.php");
require_once("hiyari_report_class.php");
require_once("get_values.php");
require_once("hiyari_common.ini");
require_once("hiyari_header_class.php");
require_once("aclg_set.php");
require_once("hiyari_mail.ini");

// CmxSystemConfigクラス
require_once('Cmx.php');
require_once('class/Cmx/Model/SystemConfig.php');

// CmxSystemConfigインスタンス生成
$sysConf = new Cmx_SystemConfig();

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if($session == "0"){
    showLoginPage();
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 47, $fname);
if ($checkauth == "0") {
    showLoginPage();
    exit;
}

// データベースに接続
$con = connect2db($fname);
// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

//デザイン（1=旧デザイン、2=新デザイン）
$design_mode = $sysConf->get('fantol.design.mode');

//評価予定期日の表示の有無
$predetermined_eval = $sysConf->get('fantol.predetermined.eval');

$font = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';
$classification_link = "hiyari_rpt_report_classification.php?session=".$session."&called_from_other_page=hiyari_menu.php&search_evaluation_date=";

// インシデント権限マスタ取得
$arr_auth = array('SM','RM');
$arr_inci = get_inci_mst($con, $fnme, $arr_auth);
$auth_sm_name = $arr_inci['SM']["auth_name"];
$auth_sm_name = h($auth_sm_name);

//新着お知らせのアクセス済み化
require_once("hiyari_info_access.ini");
$emp_id = get_emp_id($con, $session, $fname);
access_entry_all_new_news($con,$fname,$emp_id);

//未読件数
$no_recv_read_num = get_no_recv_read_num($emp_id, $fname);

//下書き件数
$where = "where not del_flag = 't' and shitagaki_flg = 't' and registrant_id = '{$emp_id}'";
$list_data_array = get_report_db_list($con,$fname,$where);
$no_shitagaki_num = 0;
if ($list_data_array){
    $no_shitagaki_num = count($list_data_array);
}

$disp_setting = get_disp_setting($con,$fname);
$news_list = get_menu_news($session, $con, $fname, $emp_id, $disp_setting["news_all_disp"]);

//報告ファイルの利用権限
$is_usable = is_report_db_usable($session,$fname,$con);
$report_db_read_div = get_report_db_read_div($session,$fname,$con);

$priority_auth = get_emp_priority_auth($session,$fname,$con);
$report_db_read_auth_flg = get_report_db_read_auth_flg($session,$fname,$con);


//==============================
//評価予定日の表示判定
//==============================

$is_evaluation_date_auth_all_disp = false;
$is_evaluation_date_auth_post_disp = false;
//SMもしくはRMの場合
$is_sm = is_inci_auth_emp_of_auth($session, $fname, $con, "SM");
$is_rm = is_inci_auth_emp_of_auth($session, $fname, $con, "RM");
$is_ra = is_inci_auth_emp_of_auth($session, $fname, $con, "RA");
$is_sd = is_inci_auth_emp_of_auth($session, $fname, $con, "SD");
$is_md = is_inci_auth_emp_of_auth($session, $fname, $con, "MD");
$is_hd = is_inci_auth_emp_of_auth($session, $fname, $con, "HD");
if ($is_sm || $is_rm || $is_ra || $is_sd || $is_md || $is_hd) {
    
    if($predetermined_eval != 't'){
        $is_evaluation_date_auth_all_disp = false;
        $is_evaluation_date_auth_post_disp = false; 
    //報告ファイルが利用不可能な場合
    }else if (!$is_usable) {
        $is_evaluation_date_auth_all_disp = true;
        $is_evaluation_date_auth_post_disp = false;
    }
    //所属制限がない場合
    else if ($report_db_read_div == 0) {
        $is_evaluation_date_auth_all_disp = true;
        $is_evaluation_date_auth_post_disp = false;
    
    //所属制限がある場合
    }else{
        $is_emp_auth_system_all = is_emp_auth_system_all($emp_id, $priority_auth, $con, $fname);

        //担当範囲でシステム全体に対する担当者の場合
        if ($report_db_read_auth_flg && $is_emp_auth_system_all) {
            $is_evaluation_date_auth_all_disp = true;
            $is_evaluation_date_auth_post_disp = false;
        }
        //所属範囲、もしくはシステム全体に対する担当者ではない場合
        else {
            $is_evaluation_date_auth_all_disp = false;
            $is_evaluation_date_auth_post_disp = true;
        }
    }
}
 
 

//==================================================
//表示部署の決定
//==================================================
//ルール
//・報告ファイルで参照制限がかかる部署単位で表示。
//・報告ファイルで参照制限がない部署は対象外。
//・SM・RM担当者になれない部署は対象外。
if($is_evaluation_date_auth_post_disp)
{
    $post_list = null;

    //報告ファイル制限：制限なしの場合　･･･部署表示はない。
    if($report_db_read_div == 0)
    {
        //このルートは通らない。
    }
    //報告ファイル制限：自部のみの場合　･･･部単位表示
    elseif($report_db_read_div == 1)
    {
        //兼務所属情報を取得
        if($report_db_read_auth_flg)
        {
            $emp_class_post = get_emp_auth_class_post($emp_id, $priority_auth, $con, $fname);
        }
        else
        {
            $emp_class_post = get_emp_class_post($emp_id, $con, $fname);
        }

        $data = get_all_entryable_post_info($con, $fname);
        foreach($data as $data1)
        {
            //部単位にて
            $class_id = $data1['class_id'];
            $atrb_id = "";
            $dept_id = "";
            $room_id = "";
            if($old_class_id == $class_id)
            {
                continue;
            }

            //自所属の部の場合のみ
            $is_self_post = false;
            foreach($emp_class_post as $emp_class_post1)
            {
                if($emp_class_post1['emp_class'] == $class_id)
                {
                    $is_self_post = true;
                }
            }
            if(!$is_self_post)
            {
                continue;
            }

            //表示対象となる。
            $d = null;
            $d['class_id'] = $class_id;
            $d['atrb_id'] = $atrb_id;
            $d['dept_id'] = $dept_id;
            $d['room_id'] = $room_id;
            $d['name'] = $data1['class_nm'];
            $post_list[] = $d;

            $old_class_id = $class_id;
        }
    }
    //報告ファイル制限：自課のみの場合　･･･課単位表示
    elseif($report_db_read_div == 2)
    {
        //兼務所属情報を取得
        if($report_db_read_auth_flg)
        {
            $emp_class_post = get_emp_auth_class_post($emp_id, $priority_auth, $con, $fname);
        }
        else
        {
            $emp_class_post = get_emp_class_post($emp_id, $con, $fname);
        }

        $data = get_all_entryable_post_info($con, $fname);
        foreach($data as $data1)
        {
            //課単位にて
            $class_id = $data1['class_id'];
            $atrb_id = $data1['atrb_id'];
            $dept_id = "";
            $room_id = "";
            if($old_class_id == $class_id && $old_atrb_id == $atrb_id)
            {
                continue;
            }

            //自所属の課の場合のみ
            $is_self_post = false;
            foreach($emp_class_post as $emp_class_post1)
            {
                if($emp_class_post1['emp_class'] == $class_id && $emp_class_post1['emp_attribute'] == $atrb_id )
                {
                    $is_self_post = true;
                }
            }
            if(!$is_self_post)
            {
                continue;
            }

            //表示対象となる。
            $d = null;
            $d['class_id'] = $class_id;
            $d['atrb_id'] = $atrb_id;
            $d['dept_id'] = $dept_id;
            $d['room_id'] = $room_id;
            $d['name'] = $data1['class_nm'].'＞'.$data1['atrb_nm'];
            $post_list[] = $d;

            $old_class_id = $class_id;
            $old_atrb_id = $atrb_id;
        }
    }
    //報告ファイル制限：自科のみの場合　･･･科単位表示
    elseif($report_db_read_div == 3)
    {
        //兼務所属情報を取得
        if($report_db_read_auth_flg)
        {
            $emp_class_post = get_emp_auth_class_post($emp_id, $priority_auth, $con, $fname);
        }
        else
        {
            $emp_class_post = get_emp_class_post($emp_id, $con, $fname);
        }

        $data = get_all_entryable_post_info($con, $fname);
        foreach($data as $data1)
        {
            //科単位にて
            $class_id = $data1['class_id'];
            $atrb_id = $data1['atrb_id'];
            $dept_id = $data1['dept_id'];
            $room_id = "";
            if($old_class_id == $class_id && $old_atrb_id == $atrb_id && $old_dept_id == $dept_id)
            {
                continue;
            }

            //自所属の科の場合のみ
            $is_self_post = false;
            foreach($emp_class_post as $emp_class_post1)
            {
                if($emp_class_post1['emp_class'] == $class_id && $emp_class_post1['emp_attribute'] == $atrb_id && $emp_class_post1['emp_dept'] == $dept_id )
                {
                    $is_self_post = true;
                }
            }
            if(!$is_self_post)
            {
                continue;
            }

            //表示対象となる。
            $d = null;
            $d['class_id'] = $class_id;
            $d['atrb_id'] = $atrb_id;
            $d['dept_id'] = $dept_id;
            $d['room_id'] = $room_id;
            $d['name'] = $data1['class_nm'].'＞'.$data1['atrb_nm'].'＞'.$data1['dept_nm'];
            $post_list[] = $d;

            $old_class_id = $class_id;
            $old_atrb_id = $atrb_id;
            $old_dept_id = $dept_id;
        }
    }
    //報告ファイル制限：自室のみの場合　･･･室単位表示
    elseif($report_db_read_div == 4)
    {
        //兼務所属情報を取得
        if($report_db_read_auth_flg)
        {
            $emp_class_post = get_emp_auth_class_post($emp_id, $priority_auth, $con, $fname);
        }
        else
        {
            $emp_class_post = get_emp_class_post($emp_id, $con, $fname);
        }

        $data = get_all_entryable_post_info($con, $fname);
        foreach($data as $data1)
        {
            //室単位にて
            $class_id = $data1['class_id'];
            $atrb_id = $data1['atrb_id'];
            $dept_id = $data1['dept_id'];
            $room_id = $data1['room_id'];

            //自所属の室の場合のみ
            $is_self_post = false;
            foreach($emp_class_post as $emp_class_post1)
            {
                if($emp_class_post1['emp_class'] == $class_id && $emp_class_post1['emp_attribute'] == $atrb_id && $emp_class_post1['emp_dept'] == $dept_id && $emp_class_post1['emp_room'] == $room_id )
                {
                    $is_self_post = true;
                }
            }
            if(!$is_self_post)
            {
                continue;
            }

            //表示対象となる。
            $d = null;
            $d['class_id'] = $class_id;
            $d['atrb_id'] = $atrb_id;
            $d['dept_id'] = $dept_id;
            $d['room_id'] = $room_id;
            if($room_id != "")
            {
                $d['name'] = $data1['class_nm'].'＞'.$data1['atrb_nm'].'＞'.$data1['dept_nm'].'＞'.$data1['room_nm'];
            }
            else
            {
                $d['name'] = $data1['class_nm'].'＞'.$data1['atrb_nm'].'＞'.$data1['dept_nm'];
            }
            $post_list[] = $d;
        }
    }
}

//SM表示フラグ取得
$sm_disp_flg = get_sm_disp_flg($fname, $con);

//==============================
//表示
//==============================
$smarty = new Cmx_View();

$smarty->assign("session", $session);

//ヘッダ用データ
$header = new hiyari_header_class($session, $fname, $con);
$smarty->assign("INCIDENT_TITLE", $header->get_inci_title());
$smarty->assign("inci_auth_name", $header->get_inci_auth_name());
$smarty->assign("is_kanrisha", $header->is_kanrisha());
$smarty->assign("tab_info", $header->get_user_tab_info());

//警告メッセージ
$smarty->assign("warning_flg", $sysConf->get('fantol.warning.news.flg') === '1');
$smarty->assign("warning_message", $sysConf->get('fantol.warning.message.text'));

//未読の受信レポート件数
$smarty->assign("no_recv_read_num", $no_recv_read_num);

//下書きの件数
$smarty->assign("no_shitagaki_num", $no_shitagaki_num);

//お知らせ
$smarty->assign("news_title", ($sm_disp_flg == "t") ? $auth_sm_name."からのお知らせ" : "お知らせ");
if (count($news_list)){
    $smarty->assign("news_list", $news_list);
}

//評価予定期日の報告書件数
$smarty->assign("is_usable", $is_usable);
$smarty->assign("classification_link", $classification_link);
$smarty->assign("is_evaluation_date_auth_all_disp", $is_evaluation_date_auth_all_disp);
$smarty->assign("is_evaluation_date_auth_post_disp", $is_evaluation_date_auth_post_disp);

$report_cnt_list = array();
if ($is_evaluation_date_auth_all_disp){
    $class_cond = '';
    if($class_id != ""){
        $class_cond = " and registrant_class = $class_id";
    }
    if($atrb_id != ""){
        $class_cond .= " and registrant_attribute = $atrb_id";
    }
    if($dept_id != ""){
        $class_cond .= " and registrant_dept = $dept_id";
    }
    if($room_id != ""){
        $class_cond .= " and registrant_room = $room_id";
    }

    if ($design_mode == 1){
        $report_cnt_list[0]["post_param"] = "";
        $report_cnt_list[0]["cnt"]["passed"] = get_evaluation_date_report_count($con,$fname,'passed', $priority_auth, $class_cond);
        $report_cnt_list[0]["cnt"]["today"] = get_evaluation_date_report_count($con,$fname,'today', $priority_auth);
        $report_cnt_list[0]["cnt"]["tomorrow"] = get_evaluation_date_report_count($con,$fname,'tomorrow', $priority_auth);
        $report_cnt_list[0]["cnt"]["1week"] = get_evaluation_date_report_count($con,$fname,'1week', $priority_auth);
        $report_cnt_list[0]["cnt"]["2week"] = get_evaluation_date_report_count($con,$fname,'2week',$priority_auth);
    }
    else{
        $report_cnt_list["passed"] = get_evaluation_date_report_count($con,$fname,'passed', $priority_auth, $class_cond);
        $report_cnt_list["today"] = get_evaluation_date_report_count($con,$fname,'today', $priority_auth);
        $report_cnt_list["tomorrow"] = get_evaluation_date_report_count($con,$fname,'tomorrow', $priority_auth);
        $report_cnt_list["1week"] = get_evaluation_date_report_count($con,$fname,'1week', $priority_auth);
        $report_cnt_list["2week"] = get_evaluation_date_report_count($con,$fname,'2week',$priority_auth);
    }
}

if ($is_evaluation_date_auth_post_disp){
    $class_cond = '';

    foreach($post_list as $i => $post_list1){
        $class_id = $post_list1['class_id'];
        $atrb_id = $post_list1['atrb_id'];
        $dept_id = $post_list1['dept_id'];
        $room_id = $post_list1['room_id'];

        $temp_cond = '';
        $link_post_param = '';

        if($class_id != ""){
            $link_post_param .= "&search_emp_class=$class_id";
            $temp_cond .= " registrant_class = $class_id";
        }
        if($atrb_id != ""){
            $link_post_param .= "&search_emp_attribute=$atrb_id";
            $temp_cond .= " and registrant_attribute = $atrb_id";
        }
        if($dept_id != ""){
            $link_post_param .= "&search_emp_dept=$dept_id";
            $temp_cond .= " and registrant_dept = $dept_id";
        }
        if($room_id != ""){
            $link_post_param .= "&search_emp_room=$room_id";
            $temp_cond .= " and registrant_room = $room_id";
        }

        if ($design_mode == 1){
            $class_cond = ' and ' . $temp_cond;
            $report_cnt_list[$i]["post_name"] = $post_list1['name'];
            $report_cnt_list[$i]["post_param"] = $link_post_param;
            $report_cnt_list[$i]["cnt"]["passed"] = get_evaluation_date_report_count($con,$fname,'passed',$priority_auth,$class_cond);
            $report_cnt_list[$i]["cnt"]["today"] = get_evaluation_date_report_count($con,$fname,'today',$priority_auth,$class_cond);
            $report_cnt_list[$i]["cnt"]["tomorrow"] = get_evaluation_date_report_count($con,$fname,'tomorrow',$priority_auth,$class_cond);
            $report_cnt_list[$i]["cnt"]["1week"] = get_evaluation_date_report_count($con,$fname,'1week',$priority_auth,$class_cond);
            $report_cnt_list[$i]["cnt"]["2week"] = get_evaluation_date_report_count($con,$fname,'2week',$priority_auth,$class_cond);
        }
        else{
            if ($class_cond != '') $class_cond .= ' or ';
            $class_cond .= "($temp_cond)";
        }
    }

    if ($design_mode == 2){
        $class_cond= " and ($class_cond)";
        $report_cnt_list["passed"] = get_evaluation_date_report_count  ($con,$fname,'passed',  $priority_auth,$class_cond);
        $report_cnt_list["today"] = get_evaluation_date_report_count   ($con,$fname,'today',   $priority_auth,$class_cond);
        $report_cnt_list["tomorrow"] = get_evaluation_date_report_count($con,$fname,'tomorrow',$priority_auth,$class_cond);
        $report_cnt_list["1week"] = get_evaluation_date_report_count   ($con,$fname,'1week',   $priority_auth,$class_cond);
        $report_cnt_list["2week"] = get_evaluation_date_report_count   ($con,$fname,'2week',   $priority_auth,$class_cond);
    }
}
$smarty->assign("report_cnt_list", $report_cnt_list);

//出来事分析の評価予定期日一覧
$db_rs = select_from_table($con, "select * from inci_analysis_available", "", $fname);
$is_analysis_available = @pg_fetch_result($db_rs, 0, 0)=="t";
if ($is_analysis_available && $is_evaluation_date_auth_all_disp){
    $t = strtotime(date("Y/m/d")." - 14 day");
    $before2week1 = date("Y/m/d", $t);
    $before2week2 = date("Ymd", $t);
    $kanrisya_where = ($is_sm ? " 1=1" : " 1<>1"); // 管理者でなければ特定条件は通さない
    $ippan_where = (!$is_sm ? " and emp_id_list like '%,".$emp_id.",%'" : "");
    $page = max((int)@$_REQUEST["page"], 1);
    //一画面内の最大表示件数
    $disp_max_analysis_page = 10;
    $analysis_offset = ($page - 1) * $disp_max_analysis_page;

    $sql =
        " select".
        " r.analysis_id, r.analysis_no ,r.analysis_title ,r.progress_date ,r.analysis_progress".
        ",f.analysis_plan ,f.analysis_yusenjun ,f.analysis_kijitu ,f.analysis_sincyoku ,f.analysis_rca_emp_name_list".
        " from inci_analysis_regist r".
        " left join inci_analysis_rca_form f on (f.analysis_id = r.analysis_id)".
        " where 1 = 1".
        " and (" .
        "   (  r.analysis_progress in ('3')". // 評価中のみ
        "     and r.progress_date >= '".$before2week1."'".
        "     and".$kanrisya_where.
        "   ) or (".
        "     r.analysis_id in (".
        "       select analysis_id from (".
        "         select".
        "         f2.analysis_id".
        "         , ','|| f2.analysis_rca_emp_id_list||',' as emp_id_list".
        "         from inci_analysis_rca_form f2".
        "         where f2.analysis_sincyoku in ('評価中')".
        "         and f2.analysis_kijitu >= '".$before2week2."'".
        "       ) d".
        "       where 1 = 1 ". $ippan_where.
        "     )".
        "   )".
        " )";
    $db_rs = select_from_table($con, "select count(*) as cnt from ( ".$sql.") d", "", $fname);
    $db_list_count = pg_fetch_result($db_rs, 0, "cnt");

    //最大ページ数
    $page_max  = 1;
    if($db_list_count > 0) $page_max  = floor( ($db_list_count-1) / $disp_max_analysis_page ) + 1;

    $sql .=
        " order by r.progress_date, r.analysis_id, f.analysis_order".
        " offset ".$analysis_offset." limit ".$disp_max_analysis_page;
    $db_rs = select_from_table($con, $sql, "", $fname);

    //一覧
    $analysis_list = array();
    $old_analysis_id = "";
    while ($row = pg_fetch_assoc($db_rs)) {
        if ($old_analysis_id != $row["analysis_id"]) {
            $data["no"] = $row["analysis_no"];
            $data["title"] = $row["analysis_title"];
            $data["progress_date"] = str_replace("-","/",$row["progress_date"]);
            $data["progress"] = $row["analysis_progress"];
        }
        else {
            $data["no"] = "";
            $data["title"] = "";
            $data["progress_date"] = "";
            $data["progress"] = "";
        }

        $data["plan"] = $row["analysis_plan"];
        $data["yusenjun"] =$row["analysis_yusenjun"];
        $data["kijitu"] =$row["analysis_kijitu"]!="" ? substr($row["analysis_kijitu"],0,4)."/".substr($row["analysis_kijitu"],4,2)."/".substr($row["analysis_kijitu"],6,2) : "";
        $data["sincyoku"] =$row["analysis_sincyoku"];
        $data["rca_emp_name_list"] =$row["analysis_rca_emp_name_list"];
        $old_analysis_id = $row["analysis_id"];

        $analysis_list[] = $data;
    }
    $smarty->assign("analysis_list", $analysis_list);

    $smarty->assign("page_max", $page_max);
    if ($db_list_count) {
        if ($page_max > 1){
            $smarty->assign("page", $page);
            $page_list = array();
            $page_stt = max(1, $page - 3);
            $page_end = min($page_stt + 6, $page_max);
            $page_stt = max(1, $page_end - 6);
            for ($i=$page_stt; $i<=$page_end; $i++){
                $page_list[] = $i;
            }
            $smarty->assign("page_list", $page_list);
        }
    }
}

if ($design_mode == 1){
    $smarty->display("hiyari_news1.tpl");
}
else{
    $smarty->display("hiyari_news2.tpl");
}

//====================================================================================================
//内部関数
//====================================================================================================
function get_evaluation_date_report_count($con,$fname,$search_evaluation_date,$priority_auth,$class_cond='')
{
    // *** 開始日、終了日の特定 ***
    switch ( $search_evaluation_date ) {

        // 評価日が過ぎた報告書を検索
        case 'passed':
            $evaluation_date_max = date('Y/m/d', strtotime('-1 day'));
            $evaluation_date_min = '2004/01/01';
            break;

        // 評価日が当日の報告書を検索
        case 'today':
            $evaluation_date_max = date('Y/m/d');
            $evaluation_date_min = $evaluation_date_max;
            break;

        // 評価日が明日の報告書を検索
        case 'tomorrow':
            $evaluation_date_max = date('Y/m/d', strtotime('+1 day'));
            $evaluation_date_min = $evaluation_date_max;
            break;

        // 評価日が一週間の報告書を検索
        case '1week':
            $evaluation_date_max = date('Y/m/d', strtotime('+7 day'));
            $evaluation_date_min = date('Y/m/d');
            break;

        // 評価日が二週間の報告書を検索
        case '2week':
            $evaluation_date_max = date('Y/m/d', strtotime('+14 day'));
            $evaluation_date_min = date('Y/m/d');
            break;
    }

    // *** SQL実効 ***
    $sql  = "";
    $sql .= " select count(*) as cnt from";
    $sql .= " (";
    $sql .= " select eid_id from inci_report r";
    $sql .= " left join inci_report_progress p on r.report_id = p.report_id and auth='" . $priority_auth . "'";
    $sql .= " where not shitagaki_flg  and not del_flag and not p.evaluation_flg";
    if($class_cond != "")
    {
        $sql .= $class_cond;
    }
    $sql .= " and r.evaluation_date >= '$evaluation_date_min' and r.evaluation_date <= '$evaluation_date_max'";
    $sql .= " ) a";

    $sel = select_from_table($con, $sql, "", $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    return pg_fetch_result($sel,0,"cnt");
}
