<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
switch ($module) {

case "bbs":
	$module_auth = 1;
	$module_name = "掲示板";
	break;

case "facility":
	$module_auth = 41;
	$module_name = "施設・設備";
	break;

case "library":
	$module_auth = 32;
	$module_name = "文書管理";
	break;

}

require("about_authority.php");
require("about_session.php");
require("show_class_name.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$check_auth = check_authority($session, $module_auth, $fname);
if ($check_auth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 部門一覧を取得
if ($emp_class == "") {
	$sql = "select class_id, class_nm from classmst";
	$cond = "where class_del_flg = 'f' order by order_no";
	$sel_class = select_from_table($con, $sql, $cond, $fname);
	if ($sel_class == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$classes = array();
	$classes["-"] = "----------";
	while ($row = pg_fetch_array($sel_class)) {
		$tmp_class_id = $row["class_id"];
		$tmp_class_nm = $row["class_nm"];
		$classes[$tmp_class_id] = $tmp_class_nm;
	}
	pg_result_seek($sel_class, 0);
} else {
	$sql = "select class_id, class_nm from classmst";
	$cond = "where class_id = '$emp_class'";
	$sel_class = select_from_table($con, $sql, $cond, $fname);
	if ($sel_class == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$classes = array();
	while ($row = pg_fetch_array($sel_class)) {
		$tmp_class_id = $row["class_id"];
		$tmp_class_nm = $row["class_nm"];
		$classes[$tmp_class_id] = $tmp_class_nm;
	}
	pg_result_seek($sel_class, 0);
}

// 課一覧を取得
$sql = "select classmst.class_id, atrbmst.atrb_id, classmst.class_nm, atrbmst.atrb_nm from atrbmst inner join classmst on atrbmst.class_id = classmst.class_id";
$cond = "where classmst.class_del_flg = 'f' and atrbmst.atrb_del_flg = 'f' order by classmst.order_no, atrbmst.order_no";
$sel_atrb = select_from_table($con, $sql, $cond, $fname);
if ($sel_atrb == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 科一覧を取得
$sql = "select classmst.class_id, atrbmst.atrb_id, deptmst.dept_id, classmst.class_nm, atrbmst.atrb_nm, deptmst.dept_nm from deptmst inner join atrbmst on deptmst.atrb_id = atrbmst.atrb_id inner join classmst on atrbmst.class_id = classmst.class_id";
$cond = "where classmst.class_del_flg = 'f' and atrbmst.atrb_del_flg = 'f' and deptmst.dept_del_flg = 'f' order by classmst.order_no, atrbmst.order_no, deptmst.order_no";
$sel_dept = select_from_table($con, $sql, $cond, $fname);
if ($sel_dept == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$dept_names = array();
while ($row = pg_fetch_array($sel_dept)) {
	$tmp_class_id = $row["class_id"];
	$tmp_atrb_id = $row["atrb_id"];
	$tmp_dept_id = $row["dept_id"];
	$tmp_dept_nm = "{$row['class_nm']}＞{$row['atrb_nm']}＞{$row['dept_nm']}";
	$dept_names["{$tmp_class_id}-{$tmp_atrb_id}-{$tmp_dept_id}"] = $tmp_dept_nm;
}
pg_result_seek($sel_dept, 0);

// 組織名を取得
$arr_class_name = get_class_name_array($con, $fname);

// 初期表示時の設定
if (!is_array($dept)) {$dept = array();}
?>
<title>CoMedix <? echo($module_name); ?> | 部署選択</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
var classes = [];
<?
while ($row = pg_fetch_array($sel_class)) {
	$tmp_class_id = $row["class_id"];
	$tmp_class_nm = $row["class_nm"];
	echo("classes.push({id: $tmp_class_id, name: '$tmp_class_nm'});\n");
}
pg_result_seek($sel_class, 0);
?>

var atrbs = {};
<?
$pre_class_id = "";
while ($row = pg_fetch_array($sel_atrb)) {
	$tmp_class_id = $row["class_id"];
	$tmp_atrb_id = $row["atrb_id"];
	$tmp_class_nm = $row["class_nm"];
	$tmp_atrb_nm = $row["atrb_nm"];

	if ($tmp_class_id != $pre_class_id) {
		echo("atrbs[$tmp_class_id] = [];\n");
	}

	echo("atrbs[$tmp_class_id].push({id: $tmp_atrb_id, name: '$tmp_atrb_nm'});\n");

	$pre_class_id = $tmp_class_id;
}
pg_result_seek($sel_atrb, 0);
?>

var depts = {};
<?
$pre_class_id = "";
$pre_atrb_id = "";
while ($row = pg_fetch_array($sel_dept)) {
	$tmp_class_id = $row["class_id"];
	$tmp_atrb_id = $row["atrb_id"];
	$tmp_dept_id = $row["dept_id"];
	$tmp_class_nm = $row["class_nm"];
	$tmp_atrb_nm = $row["atrb_nm"];
	$tmp_dept_nm = $row["dept_nm"];

	if ($tmp_class_id != $pre_class_id) {
		echo("depts[$tmp_class_id] = {};\n");
	}

	if ($tmp_class_id != $pre_class_id || $tmp_atrb_id != $pre_atrb_id) {
		echo("depts[$tmp_class_id][$tmp_atrb_id] = [];\n");
	}

	echo("depts[$tmp_class_id][$tmp_atrb_id].push({id: $tmp_dept_id,  hierarchy: '{$tmp_class_nm}＞{$tmp_atrb_nm}＞',name: '$tmp_dept_nm'});\n");

	$pre_class_id = $tmp_class_id;
	$pre_atrb_id = $tmp_atrb_id;
}
pg_result_seek($sel_dept, 0);
?>

//----------

function initPage() {
	var default_class_src = opener.document.mainform.<? echo($div); ?>_class_src.value;
	var default_atrb_src = opener.document.mainform.<? echo($div); ?>_atrb_src.value;
	setClassSrcOptions(default_class_src, default_atrb_src);
	for (var i = 0, j = opener.document.mainform.<? echo($div); ?>_dept.options.length; i < j; i++) {
		addOption(
			document.mainform.dept,
			opener.document.mainform.<? echo($div); ?>_dept.options[i].value,
			opener.document.mainform.<? echo($div); ?>_dept.options[i].text
		);
	}
}

function setClassSrcOptions(default_class_src, default_atrb_src) {
	deleteAllOptions(document.mainform.class_src);

<? if ($emp_class == "") { ?>
	addOption(document.mainform.class_src, '-', '----------', default_class_src);
<? } ?>
	for (var i = 0, len = classes.length; i < len; i++) {
		addOption(document.mainform.class_src, classes[i].id, classes[i].name, default_class_src);
	}

	setAtrbSrcOptions(default_atrb_src);
}

function setAtrbSrcOptions(default_atrb_src) {
	deleteAllOptions(document.mainform.atrb_src);

	addOption(document.mainform.atrb_src, '-', '----------', default_atrb_src);

	var class_id = document.mainform.class_src.value;
	if (atrbs[class_id]) {
		for (var i = 0, len = atrbs[class_id].length; i < len; i++) {
			addOption(document.mainform.atrb_src, atrbs[class_id][i].id, atrbs[class_id][i].name, default_atrb_src);
		}
	}

	setDeptSrcOptions();
}

function setDeptSrcOptions() {
	deleteAllOptions(document.mainform.dept_src);

	var class_id = document.mainform.class_src.value;
	var atrb_id = document.mainform.atrb_src.value;
	if (depts[class_id]) {
		if (depts[class_id][atrb_id]) {
			for (var i = 0, len = depts[class_id][atrb_id].length; i < len; i++) {
				var dept_id = depts[class_id][atrb_id][i].id;
				var value = class_id + '-' + atrb_id + '-' + dept_id;
				addOption(document.mainform.dept_src, value, depts[class_id][atrb_id][i].name);
			}
		} else if (atrb_id == '-') {
			for (var i = 0, len = atrbs[class_id].length; i < len; i++) {
				var atrb_id = atrbs[class_id][i].id;
				if (depts[class_id][atrb_id]) {
					for (var j = 0, len2 = depts[class_id][atrb_id].length; j < len2; j++) {
						var dept_id = depts[class_id][atrb_id][j].id;
						var value = class_id + '-' + atrb_id + '-' + dept_id;
						addOption(document.mainform.dept_src, value, depts[class_id][atrb_id][j].name);
					}
				}
			}
		}
	}
}

function addOption(box, value, text, selected) {
	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	if (selected == value) {
		opt.selected = true;
	}
	box.options[box.length] = opt;
	if (!box.multiple) {
		try {box.style.fontSize = 'auto';} catch (e) {}
		box.style.overflow = 'auto';
	}
}

function addSelectedOptions(dest_box, src_box) {
    var options = new Array();
    for (var i = 0, j = dest_box.length; i < j; i++) {
        options[dest_box.options[i].value] = dest_box.options[i].text;
    }
    deleteAllOptions(dest_box);
    for (var i = 0, j = src_box.length; i < j; i++) {
        if (src_box.options[i].selected) {
            var class_atrb_dept = src_box.options[i].value.split('-');
            var class_id = class_atrb_dept[0];
            var atrb_id = class_atrb_dept[1];
            var dept_id = class_atrb_dept[2];
            for (var k = 0; k < depts[class_id][atrb_id].length; k++) {
                if (depts[class_id][atrb_id][k].id == dept_id) {
                    options[src_box.options[i].value] = depts[class_id][atrb_id][k].hierarchy + depts[class_id][atrb_id][k].name;
                }
            }
        }
    }

    for (var i in options) {
        addOption(dest_box, i, options[i]);
    }
}

function selectAllOptions(box) {
	for (var i = 0, j = box.length; i < j; i++) {
		box.options[i].selected = true;
	}
}

function deleteSelectedOptions(box) {
	for (var i = box.length - 1; i >= 0; i--) {
		if (box.options[i].selected) {
			box.options[i] = null;
		}
	}
}

function deleteAllOptions(box) {
	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}
}

function copyDepts() {
	var default_class_src = document.mainform.class_src.value;
	var default_atrb_src = document.mainform.atrb_src.value;
<? if ($div == "ref") { ?>
	opener.setRefClassSrcOptions(false, default_class_src, default_atrb_src);
<? } else { ?>
	opener.setUpdClassSrcOptions(false, default_class_src, default_atrb_src);
<? } ?>
	for (var i = 0, j = document.mainform.dept.options.length; i < j; i++) {
		opener.addOption(
			opener.document.mainform.<? echo($div); ?>_dept,
			document.mainform.dept.options[i].value,
			document.mainform.dept.options[i].text
		);
	}
	self.close();
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-style:none;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="initPage();">
<center>
<table width="760" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>部署選択</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form name="mainform" action="bbsthread_insert.php" method="post">
<table width="760" border="0" cellspacing="2" cellpadding="0">
<tr>
<td width="50%" valign="bottom" style="position:relative;top:3px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">可能とする<? echo($arr_class_name[2]); ?></font></td>
<td></td>
<td width="50%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="class_src" onchange="setAtrbSrcOptions();">
</select><? echo($arr_class_name[0]); ?><br>
<select name="atrb_src" onchange="setDeptSrcOptions();">
</select><? echo($arr_class_name[1]); ?>
</font></td>
</tr>
<tr>
<td>
<select name="dept" size="6" multiple style="width:100%;">
<?
foreach ($dept as $tmp_dept_id) {
	echo("<option value=\"$tmp_dept_id\">{$dept_names[$tmp_dept_id]}");
}
?>
</select>
</td>
<td align="center"><input type="button" value=" &lt; " onclick="addSelectedOptions(this.form.dept, this.form.dept_src);"><br><br><input type="button" value=" &gt; " onclick="deleteSelectedOptions(this.form.dept);"></td>
<td><select name="dept_src" size="6" multiple style="width:100%;"></select></td>
</tr>
<tr>
<td><input type="button" value="全て消去" onclick="deleteAllOptions(this.form.dept);"></td>
<td></td>
<td><input type="button" value="全て選択" onclick="selectAllOptions(this.form.dept_src);"></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="button" value="コピー" onclick="copyDepts();"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
</center>
</body>
<? pg_close($con); ?>
</html>
