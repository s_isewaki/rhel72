<?
// $sync_emp_flg  true:同時登録の職員分 、false:指定職員のみ
function get_repeated_schedules($con, $schd_id, $timeless, $repeat_type, $fname, $sync_emp_flg) {
	$timeless = ($timeless != "t") ? "f" : "t";

	$schedules = array();

	// 基準となるスケジュール情報を取得
	if ($timeless == "f") {
		$sql = "select * from schdmst";
	} else {
		$sql = "select * from schdmst2";
	}
	$cond = "where schd_id = '$schd_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	// 削除済みの場合
	if (pg_num_rows($sel) == 0) {
		return $schedules;
	}
	$cmp_emp_id = pg_fetch_result($sel, 0, "emp_id");
	$cmp_reg_emp_id = pg_fetch_result($sel, 0, "schd_reg_id");
	$cmp_reg_time = pg_fetch_result($sel, 0, "reg_time");
	$cmp_start_date = pg_fetch_result($sel, 0, "schd_start_date");
	if ($timeless == "f") {
		$cmp_start_time = pg_fetch_result($sel, 0, "schd_start_time");
	} else {
		$cmp_start_time = "00:00:00";
	}

	// 指定職員のみの場合で、1件のみの場合はそのスケジュールのみ返す。
	if ($sync_emp_flg == false && ($repeat_type == "" || $repeat_type == "1")) {
		if ($timeless == "f") {
			$tmp_time = substr(pg_fetch_result($sel, 0, "schd_start_time_v"), 0, 2).":".substr(pg_fetch_result($sel, 0, "schd_start_time_v"), 2, 2);
			$tmp_time .= "〜" . substr(pg_fetch_result($sel, 0, "schd_dur_v"), 0, 2).":".substr(pg_fetch_result($sel, 0, "schd_dur_v"), 2, 2);
		} else {
			$tmp_time = "時刻指定なし";
		}

		$schedules[] = array(
			"timeless" => $timeless,
			"id" => $schd_id,
			"emp_id" => $cmp_emp_id,
			"title" => pg_fetch_result($sel, 0, "schd_title"),
			"place_id" => pg_fetch_result($sel, 0, "schd_place_id"),
			"place_detail" => pg_fetch_result($sel, 0, "schd_plc"),
			"date" => str_replace("-", "/", pg_fetch_result($sel, 0, "schd_start_date")),
			"time" => $tmp_time,
			"detail" => pg_fetch_result($sel, 0, "schd_detail"),
			"approve_status" => pg_fetch_result($sel, 0, "schd_status")
		);
		return $schedules;
	}

	// 同時登録された時間指定ありスケジュール一覧を取得〜配列に格納
	$sql = "select * from schdmst";
	$cond = "where ";
	// 指定職員のみ
	if ($sync_emp_flg == false) {
		$cond .= " emp_id = '$cmp_emp_id' and ";
	}
	$cond .= " schd_reg_id = '$cmp_reg_emp_id' and reg_time = '$cmp_reg_time'";
	// 当日分
	if ($repeat_type == "1") {
		$cond .= " and schd_start_date = '$cmp_start_date'";
	}
	// 将来分
	if ($repeat_type == "3") {
		$cond .= " and schd_start_date >= '$cmp_start_date' and schd_start_time >= '$cmp_start_time'";
	}
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$tmp_time = substr(pg_fetch_result($sel, 0, "schd_start_time_v"), 0, 2).":".substr(pg_fetch_result($sel, 0, "schd_start_time_v"), 2, 2);
		$tmp_time .= "〜" . substr(pg_fetch_result($sel, 0, "schd_dur_v"), 0, 2).":".substr(pg_fetch_result($sel, 0, "schd_dur_v"), 2, 2);

		$schedules[] = array(
			"timeless" => "f",
			"id" => $row["schd_id"],
			"emp_id" => $row["emp_id"],
			"title" => $row["schd_title"],
			"place_id" => $row["schd_place_id"],
			"place_detail" => $row["schd_plc"],
			"date" => str_replace("-", "/", $row["schd_start_date"]),
			"time" => $tmp_time,
			"detail" => $row["schd_detail"],
			"approve_status" => $row["schd_status"]
		);
	}

	// 同時登録された時間指定なしスケジュール一覧を取得〜配列に格納
	$sql = "select * from schdmst2";
	$cond = "where ";
	// 指定職員のみ
	if ($sync_emp_flg == false) {
		$cond .= " emp_id = '$cmp_emp_id' and ";
	}
	$cond .= " schd_reg_id = '$cmp_reg_emp_id' and reg_time = '$cmp_reg_time'";
	// 当日分
	if ($repeat_type == "1") {
		$cond .= " and schd_start_date = '$cmp_start_date'";
	}
	// 将来分
	if ($repeat_type == "3") {
		$cond .= " and schd_start_date >= '$cmp_start_date'";
	}
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$schedules[] = array(
			"timeless" => "t",
			"id" => $row["schd_id"],
			"emp_id" => $row["emp_id"],
			"title" => $row["schd_title"],
			"place_id" => $row["schd_place_id"],
			"place_detail" => $row["schd_plc"],
			"date" => str_replace("-", "/", $row["schd_start_date"]),
			"time" => "時刻指定なし",
			"detail" => $row["schd_detail"],
			"approve_status" => $row["schd_status"]
		);
	}

	return $schedules;
}

function get_repeated_project_schedules($con, $pjt_schd_id, $timeless, $repeat_type, $fname) {
	$timeless = ($timeless != "t") ? "f" : "t";

	$schedules = array();

	// 1件のみの場合はそのスケジュールのみ返す
	if ($repeat_type == "" || $repeat_type == "1") {
		$schedules[] = array("timeless" => $timeless, "id" => $pjt_schd_id);
		return $schedules;
	}

	// 基準となるスケジュール情報を取得
	if ($timeless == "f") {
		$sql = "select pjt_id, emp_id, reg_time, pjt_schd_start_date, pjt_schd_start_time from proschd";
	} else {
		$sql = "select pjt_id, emp_id, reg_time, pjt_schd_start_date from proschd2";
	}
	$cond = "where pjt_schd_id = '$pjt_schd_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$cmp_pjt_id = pg_fetch_result($sel, 0, "pjt_id");
	$cmp_emp_id = pg_fetch_result($sel, 0, "emp_id");
	$cmp_reg_time = pg_fetch_result($sel, 0, "reg_time");
	$cmp_start_date = pg_fetch_result($sel, 0, "pjt_schd_start_date");
	if ($timeless == "f") {
		$cmp_start_time = pg_fetch_result($sel, 0, "pjt_schd_start_time");
	} else {
		$cmp_start_time = "00:00:00";
	}

	// 同時登録された時間指定ありスケジュール一覧を取得〜配列に格納
	$sql = "select pjt_schd_id from proschd";
	$cond = "where pjt_id = $cmp_pjt_id and emp_id = '$cmp_emp_id' and repeat_flg = 't' and reg_time = '$cmp_reg_time'";
	if ($repeat_type == "3") {
		$cond .= " and pjt_schd_start_date >= '$cmp_start_date' and pjt_schd_start_time >= '$cmp_start_time'";
	}
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$schedules[] = array("timeless" => "f", "id" => $row["pjt_schd_id"]);
	}

	// 同時登録された時間指定なしスケジュール一覧を取得〜配列に格納
	$sql = "select pjt_schd_id from proschd2";
	$cond = "where pjt_id = $cmp_pjt_id and emp_id = '$cmp_emp_id' and repeat_flg = 't' and reg_time = '$cmp_reg_time'";
	if ($repeat_type == "3") {
		$cond .= " and pjt_schd_start_date >= '$cmp_start_date'";
	}
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$schedules[] = array("timeless" => "t", "id" => $row["pjt_schd_id"]);
	}

	return $schedules;
}

// 同時予約された職員IDを取得する
function get_emp_id_from_schedules($con, $schd_id, $timeless, $fname) {
	$timeless = ($timeless != "t") ? "f" : "t";

	$arr_emp_id = array();

	// 基準となるスケジュール情報を取得
	if ($timeless == "f") {
		$sql = "select * from schdmst";
	} else {
		$sql = "select * from schdmst2";
	}
	$cond = "where schd_id = '$schd_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$cmp_reg_emp_id = pg_fetch_result($sel, 0, "schd_reg_id");
	$cmp_reg_time = pg_fetch_result($sel, 0, "reg_time");
	$cmp_start_date = pg_fetch_result($sel, 0, "schd_start_date");

	$table_name = ($timeless != "t") ? "schdmst" : "schdmst2";
	// 同時登録された職員ID一覧を取得〜配列に格納
	$sql = "select $table_name.emp_id from $table_name inner join empmst on empmst.emp_id = $table_name.emp_id";
	$cond = "where schd_reg_id = '$cmp_reg_emp_id' and reg_time = '$cmp_reg_time'";
	$cond .= " and schd_start_date = '$cmp_start_date'";
	$cond .= " order by emp_kn_lt_nm, emp_kn_ft_nm";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$arr_emp_id[] = $row["emp_id"];
	}

	return $arr_emp_id;
}

// 同時予約された日付を取得する
function get_date_from_schedules($con, $schd_id, $timeless, $repeat_type, $fname) {
	$timeless = ($timeless != "t") ? "f" : "t";

	$arr_date = array();

	// 基準となるスケジュール情報を取得
	if ($timeless == "f") {
		$sql = "select * from schdmst";
	} else {
		$sql = "select * from schdmst2";
	}
	$cond = "where schd_id = '$schd_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$cmp_emp_id = pg_fetch_result($sel, 0, "emp_id");
	$cmp_reg_emp_id = pg_fetch_result($sel, 0, "schd_reg_id");
	$cmp_reg_time = pg_fetch_result($sel, 0, "reg_time");
	$cmp_start_date = pg_fetch_result($sel, 0, "schd_start_date");

	// 1件のみの場合はそのスケジュールのみ返す
	if ($repeat_type == "" || $repeat_type == "1") {
		$arr_date[] = $cmp_start_date;
		return $arr_date;
	}

	$table_name = ($timeless != "t") ? "schdmst" : "schdmst2";
	// 同時登録された日付を取得〜配列に格納
	$sql = "select schd.schd_start_date from (select schd_start_date from schdmst";
	$sql .= " where emp_id = '$cmp_emp_id' and schd_reg_id = '$cmp_reg_emp_id' and reg_time = '$cmp_reg_time'";
	if ($repeat_type == "3") {
		$sql .= " and schd_start_date >= '$cmp_start_date'";
	}
	$sql .= " union select schd_start_date from schdmst2";
	$sql .= " where emp_id = '$cmp_emp_id' and schd_reg_id = '$cmp_reg_emp_id' and reg_time = '$cmp_reg_time'";
	if ($repeat_type == "3") {
		$sql .= " and schd_start_date >= '$cmp_start_date'";
	}
	$sql .= ") schd ";

	
	$cond = " order by schd.schd_start_date";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$arr_date[] = $row["schd_start_date"];
	}

	return $arr_date;
}

?>
