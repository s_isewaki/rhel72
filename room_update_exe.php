<?
require("about_session.php");
require("about_authority.php");
require("about_validation.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 21, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// 入力チェック
if ($ptrm_nm == "") {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('病室名が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// 病棟区分未使用チェック
$sql = "select wddv_use_flg from wddvmst";
$cond = "where wddv_id = '$ward_type'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if (pg_fetch_result($sel, 0, "wddv_use_flg") == "f") {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('選択された病棟区分は「使用しない」と設定されています。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}

if ($max_bed_no < $bed_cur) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('現在病床数が最大病床数を超えています。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if ($sex == "") {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('性別を選択してください。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if ($adult == "") {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('大人・子供を選択してください。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
$check = positive_number_validation($bed_chg);
if ($check == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('ベット差額は半角数字で入力してください。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
$check = positive_number_validation($res_fee);
if ($check == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('貸切室料は半角数字で入力してください。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}

if ($vertical == "t") {
	if ($bed_cur < 2 || $bed_cur > 5) {
		pg_query($con, "rollback");
		pg_close($con);
		$vertical_label = ($door_pos == "u" || $door_pos == "d") ? "縦置き配置" : "横置き配置";
		echo("<script language=\"javascript\">alert(\"{$vertical_label}は、現在病床数が2〜5床の場合のみ選択可能です。\");</script>\n");
		echo("<script language=\"javascript\">history.back();</script>\n");
		exit;
	}
} else {
	$vertical = "f";
}

// 登録値の編集
$window = ($window != "t") ? "f" : "t";
$sp_flg = ($sp_flg != "t") ? "f" : "t";

// 更新前の現在病床数を取得
$sql = "select ptrm_bed_cur from ptrmmst";
$cond = "where bldg_cd = '$bldg_cd' and ward_cd = '$ward_cd' and ptrm_room_no = '$rm_no'";$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$pre_bed_cur = pg_fetch_result($sel, 0, "ptrm_bed_cur");

// 病床数が増える場合
if ($bed_cur > $pre_bed_cur) {
	$sql = "insert into inptmst (inpt_enti_id, bldg_cd, ward_cd, ptrm_room_no, inpt_bed_no) values (";
	for ($i = $pre_bed_cur + 1; $i <= $bed_cur; $i++) {
		$content = array(1, $bldg_cd, $ward_cd, $rm_no, $i);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

// 病床数が減る場合
} else if ($bed_cur < $pre_bed_cur) {
	for ($i = $pre_bed_cur; $i > $bed_cur; $i--) {

		// 当該病床に入院患者が存在したら病床削減不可とする
		$sql = "select ptif_id, inpt_in_dt, inpt_in_tm from inptmst";
		$cond = "where bldg_cd = '$bldg_cd' and ward_cd = '$ward_cd' and ptrm_room_no = '$rm_no' and inpt_bed_no = '$i' and not (ptif_id = '' or ptif_id is null)";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		if (pg_num_rows($sel) > 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script language='javascript'>alert('ベッド{$i}に入院患者が存在するため病床数を減らせません。');</script>");
			echo("<script language='javascript'>history.back();</script>");
			exit;
		}

		// 当該病床の入院履歴情報を取得
		$sql = "select ptif_id, inpt_in_dt, inpt_in_tm, inpt_out_dt, inpt_out_tm from inpthist";
		$cond = "where bldg_cd = '$bldg_cd' and ward_cd = '$ward_cd' and ptrm_room_no = '$rm_no' and inpt_bed_no = '$i'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 入院履歴一覧をループ
		while ($row = pg_fetch_array($sel)) {
			$ptif_id = $row["ptif_id"];
			$inpt_in_dt = $row["inpt_in_dt"];
			$inpt_in_tm = $row["inpt_in_tm"];
			$inpt_out_dt = $row["inpt_out_dt"];
			$inpt_out_tm = $row["inpt_out_tm"];

			// 退院情報の削除
			$sql = "delete from inpthist";
			$cond = "where ptif_id = '$ptif_id' and inpt_in_dt = '$inpt_in_dt' and inpt_in_tm = '$inpt_in_tm'";
			$del = delete_from_table($con, $sql, $cond, $fname);
			if ($del == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}

			// チェックリスト情報の削除
			$sql = "delete from inptqhist";
			$cond = "where ptif_id = '$ptif_id' and inpt_in_dt = '$inpt_in_dt' and inpt_in_tm = '$inpt_in_tm'";
			$del = delete_from_table($con, $sql, $cond, $fname);
			if ($del == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}

			// 栄養問診表情報の削除
			$sql = "delete from inptnuthist";
			$cond = "where ptif_id = '$ptif_id' and inpt_in_dt = '$inpt_in_dt' and inpt_in_tm = '$inpt_in_tm'";
			$del = delete_from_table($con, $sql, $cond, $fname);
			if ($del == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}

			// 入院状況情報の削除
			$sql = "delete from inptcond";
			$cond = "where ptif_id = '$ptif_id' and to_date(date, 'YYYYMMDD') between to_date('$inpt_in_dt', 'YYYYMMDD') and to_date('$inpt_out_dt', 'YYYYMMDD') and hist_flg = 't'";
			$del = delete_from_table($con, $sql, $cond, $fname);
			if ($del == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}

			// 移転情報の削除
			$sql = "delete from inptmove";
			$cond = "where ptif_id = '$ptif_id' and to_timestamp(move_dt || move_tm, 'YYYYMMDDHH24MI') between to_timestamp('$inpt_in_dt$inpt_in_tm', 'YYYYMMDDHH24MI') and to_timestamp('$inpt_out_dt$inpt_out_tm', 'YYYYMMDDHH24MI')";
			$del = delete_from_table($con, $sql, $cond, $fname);
			if ($del == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}

			// 外出情報の削除
			$sql = "delete from inptgoout";
			$cond = "where ptif_id = '$ptif_id' and out_type = '1' and to_timestamp(out_date || out_time, 'YYYYMMDDHH24MI') between to_timestamp('$inpt_in_dt$inpt_in_tm', 'YYYYMMDDHH24MI') and to_timestamp('$inpt_out_dt$inpt_out_tm', 'YYYYMMDDHH24MI')";
			$del = delete_from_table($con, $sql, $cond, $fname);
			if ($del == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}

			// 外泊情報の削除
			$sql = "delete from inptgoout";
			$cond = "where ptif_id = '$ptif_id' and out_type = '2' and to_date(out_date, 'YYYYMMDD') between to_date('$inpt_in_dt', 'YYYYMMDD') and to_date('$inpt_out_dt', 'YYYYMMDD')";
			$del = delete_from_table($con, $sql, $cond, $fname);
			if ($del == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}

			// 担当者情報の削除
			$sql = "delete from inptophist";
			$cond = "where ptif_id = '$ptif_id' and inpt_in_dt = '$inpt_in_dt' and inpt_in_tm = '$inpt_in_tm'";
			$del = delete_from_table($con, $sql, $cond, $fname);
			if ($del == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}

		// 病床情報の削除
		$sql = "delete from inptmst";
		$cond = "where bldg_cd = '$bldg_cd' and ward_cd = '$ward_cd' and ptrm_room_no = '$rm_no' and inpt_bed_no = '$i'";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 仮抑え情報の削除
		$sql = "delete from bedtemp";
		$cond = "where bldg_cd = '$bldg_cd' and ward_cd = '$ward_cd' and ptrm_room_no = '$rm_no' and inpt_bed_no = '$i'";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 当該病床の入院予定患者の担当者情報を削除
		$sql = "delete from inptopres";
		$cond = "where exists (select * from inptres where inptres.ptif_id = inptopres.ptif_id and inptres.bldg_cd = '$bldg_cd' and inptres.ward_cd = '$ward_cd' and inptres.ptrm_room_no = '$rm_no' and inptres.inpt_bed_no = '$i')";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 当該病床の入院予定情報を削除
		$sql = "delete from inptres";
		$cond = "where bldg_cd = '$bldg_cd' and ward_cd = '$ward_cd' and ptrm_room_no = '$rm_no' and inpt_bed_no = '$i'";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 当該病床の入院予定キャンセル患者の担当者情報を削除
		$sql = "delete from inptopcancel";
		$cond = "where exists (select * from inptcancel where inptcancel.inpt_res_ccl_id = inptopcancel.inpt_res_ccl_id and inptcancel.bldg_cd = '$bldg_cd' and inptcancel.ward_cd = '$ward_cd' and inptcancel.ptrm_room_no = '$rm_no' and inpt_bed_no = '$i')";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 当該病床の入院予定キャンセル情報を削除
		$sql = "delete from inptcancel";
		$cond = "where bldg_cd = '$bldg_cd' and ward_cd = '$ward_cd' and ptrm_room_no = '$rm_no' and inpt_bed_no = '$i'";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
}

// 病室情報の更新
$sql = "update ptrmmst set";
$set = array("ptrm_name", "ptrm_type", "ptrm_bed_max", "ptrm_bed_cur", "ptrm_window", "ptrm_sex", "ptrm_adult", "ptrm_sp_flg", "ptrm_bed_chg", "ptrm_res_fee", "ward_type", "door_pos", "vertical");
$setvalue = array($ptrm_nm, $room_type, $max_bed_no, $bed_cur, $window, $sex, $adult, $sp_flg, $bed_chg, $res_fee, $ward_type, $door_pos, $vertical);
$cond = "where bldg_cd = '$bldg_cd' and ward_cd = '$ward_cd' and ptrm_room_no = '$rm_no'";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// ライセンスチェック
$sql = "select lcs_bed_count from license";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$lcs_bed_count = pg_fetch_result($sel, 0, "lcs_bed_count");
if ($lcs_bed_count != "" && $lcs_bed_count != "FULL") {
	$sql = "select sum(r.ptrm_bed_max) from ptrmmst r";
	$cond = "where not r.ptrm_del_flg and exists (select * from bldgmst b where b.bldg_cd = r.bldg_cd and not b.bldg_del_flg) and exists (select * from wdmst w where w.ward_cd = r.ward_cd and not w.ward_del_flg)";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$reg_bed_count = pg_fetch_result($sel, 0, 0);

	if (intval($reg_bed_count) > intval($lcs_bed_count)) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script language='javascript'>alert('病床数がライセンスを超えるため登録できません。');</script>");
		echo("<script language='javascript'>history.back();</script>");
		exit;
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 病室修正画面を再表示
echo("<script type=\"text/javascript\">location.href = 'room_update.php?session=$session&bldg_cd=$bldg_cd&ward_cd=$ward_cd&rm_no=$rm_no'</script>");
?>
