<?php
require_once 'kintai/common/code_util.php';
require_once 'kintai/common/master_util.php';

/**
 * 日付ユーティリティ
 */
class DateUtil {
	
	/**
	 * 日時に変換 
	 * 主として計算に使用する(はず)
	 * @param $y 年
	 * @param $m 月
	 * @param $d 日
	 * @param $format フォーマット
	 * @param $h 時間
	 * @param $i 分
	 * @param $s 秒
	 * @return 日付(date)
	 */
	static function to_date($y, $m, $d, $format='Y/m/d', $h = 0, $i = 0, $s = 0) {
		return date($format, mktime($h, $i, $s, $m, $d, $y));
	}

	/**
	 * time値に変換する
	 * @param $y 年
	 * @param $m 月
	 * @param $d 日
	 * @param $tommorow 翌日フラグ 
	 * @param $h 時間
	 * @param $i 分
	 * @param $s 秒
	 * @return 日付(date)
	 */
	static function to_time($y, $m, $d, $tommorow = false, $h = 0, $i = 0, $s = 0) {
		if ($tommorow) {
			return mktime($h, $i, $s, $m, $d++, $y);
		}
		return mktime($h, $i, $s, $m, $d, $y);
	}
	
	/**
	 * 日付間の日数を求める
	 * @param $from 開始日付(yyyymmdd)
	 * @param $to   終了日付(yyyymmdd)
	 * @return 日数
	 */
	static function date_diff($from, $to) {
		$sp_from = self::split_ymd($from);
		$sp_to   = self::split_ymd($to);
		
		$from_time = mktime(0,0,0,$sp_from['m'], $sp_from['d'], $sp_from['y']);
		$to_time = mktime(0,0,0,$sp_to['m'], $sp_to['d'], $sp_to['y']);
		
		return 1+abs($to_time - $from_time) / (60*60*24);
	}
	
	/**
	 * 曜日を取得。
	 * $labelをtrueにすると日本語曜日を返す
	 * @param $date 日付(日付形式)
	 * @param $label 和名有無
	 * @return 曜日
	 */
	static function day_of_week($date, $label = true) {
		$day_of_weak = date('w', strtotime($date));
		if ($label) {
			return CodeUtil::get_day_of_week($day_of_weak);
		} else {
			return $day_of_weak;
		}
	}
	
	/**
	 * dateを分解
	 * @param $date 日付(date形式)
	 * @return 連想配列(Y, m, d, w, h, i)
	 */
	static function split_date($date) {
		return array(
			"y" => date('Y', strtotime($date)),	"m" => date('m', strtotime($date)),
			"d" => date('d', strtotime($date)),	"w" => date('w', strtotime($date)),
			"h" => date('h', strtotime($date)),	"i" => date('i', strtotime($date)),
		);
	}
	
	/**
	 * DB登録用日付
	 * @param $y 年
	 * @param $m 月
	 * @param $d 日
	 * @return 日付(yyyymmdd)
	 */
	static function to_db_ymd($y,$m, $d) {
		return sprintf('%02d', $y).sprintf('%02d', $m).sprintf('%02d', $d);
	}
	
	/**
	 * DB登録用時分
	 * @param $hour 時間
	 * @param $minute 分
	 * @return 時分(hhmm)
	 */
	static function to_db_hi($hour, $minute) {
		if (empty($hour) && empty($minute)) {
			return null;
		}
		return sprintf('%02d', $hour).sprintf('%02d', $minute);
	}
	
	/**
	 * DB登録用(時、分単項目)
	 * @param $num 数値
	 * @return 先頭0埋めフォーマット値
	 */
	static function to_02d($num) {
		if ("" == strval($num)) {
			return null;
		}
		return sprintf('%02d',$num);
	}
	
	/**
	 * yyyymmddを分解
	 * @param $ymd 日付(yyyymmdd)
	 * @return 連想配列(y, m, d)
	 */
	static function split_ymd($ymd) {
		if (empty($ymd)) return array();
		return array("y" => substr($ymd, 0,4), "m" => substr($ymd, 4, 2), "d" => substr($ymd, 6, 2));
	}

	/**
	 * hhiiを分解
	 * @param $hm 時分(hm)
	 * @return 連想配列(h, i)
	 */
	static function split_hi($hm) {
		if (empty($hm)) return array();
		$hm = str_replace(":", "", $hm);
		return array("h" => substr($hm, 0,2), "i" => substr($hm, 2, 2));
	}
	
	/**
	 * 分→時分へ変換
	 * @param $minute 分
	 * @return 時分 
	 */
	function convert_time($minute) {
		if (empty($minute)) return null;
		return intval($minute / 60).":".sprintf("%02d", $minute % 60);
	}

	/**
	 * 時分→分へ変換
	 * @param $tm 時分(hh:mm, hhmm)
	 * @return 分
	 */
	function convert_min($tm) {
		if (empty($tm)) return 0;
		$hi = str_replace(":", "", $tm);
		if(strlen($hi) == 3) $hi = "0".$hi;
		$hiArr = self::split_hi($hi);
		return intval($hiArr["h"]) * 60 + intval($hiArr["i"]);
	}

	/**
	 * 時間1(hhii)、時間2(hhii)の差分 戻り値（G:i）頭ゼロ無し
	 * @param $Hi1 開始時分
	 * @param $hi2 終了時分
	 * @return 差分(分)
	 */
	function diff_time($Hi1, $Hi2) {
		if (empty($Hi1) || empty($Hi2)) return null;
		$h1 = intval(substr($Hi1, 0, 2));
		$m1 = intval(substr($Hi1, 2, 2));
		$h2 = intval(substr($Hi2, 0, 2));
		$m2 = intval(substr($Hi2, 2, 2));
		$min = ($h2 * 60 + $m2) - ($h1 * 60 + $m1);
		return intval($min / 60).":".sprintf("%02d", $min % 60);
	}

	/**
	 * 月度のfrom, toを取得
	 * @param $duty_form 勤務形態(1:常勤, 2:非常勤 3:短時間正職員)
	 * @param $y 年
	 * @param $m 月
	 * @return 締め日期間連想配列(start, end)
	 */
	static function get_cutoff_period($duty_form, $y, $m) {
		// マスタからデータを取得
		$data = MasterUtil::get_closing();
		$cutoff_date;
		switch ($duty_form) {
			case "1": // 常勤
			case "3": // 短時間正職員
				$cutoff_date = CodeUtil::get_cutoff_date($data["closing"]);
				break;
			case "2": // 非常勤
				$cutoff_date = CodeUtil::get_cutoff_date($data["closing_parttime"]);
				break;
			default:
				return null;
		}
		
		if (empty($cutoff_date) || $cutoff_date == "E") { // 締め日未設定または末日は月初〜月末
			$time = mktime(0,0,0,$m, 1, $y);
			return array("start" => date('Ymd', $time), "end" => date('Ymt', $time));
		} 
		
		// 基準日付
		$base_date = date('Y-m-d', mktime(0,0,0,$m, $cutoff_date, $y));
		
		$closing_month_flg = $data["closing_month_flg"]; // 月度
		if ($closing_month_flg == "1") { // 開始日の月とする
			$start_date = date('Ymd', strtotime("$base_date +1 day")); // 基準日+1日
			$end_date = date('Ymd', strtotime("$base_date +1 month")); // 基準日+1月
		} else { // 終了日の月とする
			$end_date = date('Ymd', strtotime($base_date)); // endが締め日
			$start_date = date('Ymd', strtotime("$base_date -1 month +1 day")); // 基準日-1ヶ月前+1日
		}
		return array("start" => $start_date, "end" => $end_date);
	}
	
	/**
	 * 指定された日付に対する月度のfrom,toを取得
	 * @param $duty_form 勤務形態(1:常勤,2:非常勤)
	 * @param $date_char 日付(yyyymmdd)
	 * @param 締め日期間連想配列(start, end)
	 */
	static function get_cutoff_period_from_date($duty_form, $date_char) {
		
		// 年、月を取得
		$sp_date = self::split_ymd($date_char);
		// 前後1ヶ月を取得
		$center = self::to_date($sp_date["y"],$sp_date["m"],$sp_date["d"]);
		$prev = date("Y/m/d", strtotime("$center -1 month"));
		$next = date("Y/m/d", strtotime("$center +1 month"));
		
		foreach (array($prev, $center, $next) as $day) {
			$date_map = self::split_date($day);
			
			$closing = self::get_cutoff_period($duty_form, $date_map["y"], $date_map["m"]);

			// 締め日の範囲に含まれたら返却
			if ($closing["start"] <= $date_char && $closing["end"] >= $date_char) {
				return $closing;
			}
		}
		return null;
	}
}
?>
