<?php
/**
 * Ajax返却オブジェクト
 */
class AjaxResponse {
	// 結果("success" or "error")
	public $result;
	// メッセージ
	public $message;

	/**
	 * コンストラクタ
	 * @param $result 結果
	 * @param $message メッセージ
	 */
	function __construct($result, $message = null) {
		$this->result = $result;
		$this->message = $message;
	}
}
?>
