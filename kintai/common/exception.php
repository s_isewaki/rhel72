<?php
/**
 * 例外クラス(業務例外)
 */
class BusinessException extends Exception {
	function __construct($message) {
		parent::__construct($message);
	}
}

/**
 * 致命的例外(続行不可能)
 */
class FatalException extends Exception {
}
?>
