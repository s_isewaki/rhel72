<?
require_once './common_log_class.ini';
require_once './kintai/common/mdb_wrapper.php';
require_once './kintai/common/exception.php';


/**
 * MDBユーティリティ.
 */
class MDB {

	// MDBWrapper
	private $mdb;
	// ロガー
	private $logger;

	/**
	 * コンストラクタ.
	 * @param $log ロガー
	 */
	public function __construct($log = null) {
		if (empty($log)) {
			$log = new common_log_class(basename(__FILE__));
		}
		$this->logger = $log;

		if (empty($GLOBALS['kintai_mdb'])) {
			$this->logger->error("MDBが初期化されていません。MDBWrapper:init()をCallしてください。");
			throw new FatalException();
		}
		$this->mdb = $GLOBALS['kintai_mdb'];
	}
	
	/**
	 * 検索クエリ.
	 * @param $sql SQL
	 * @param $types bind型
	 * @param $param bind値(key,value)
	 * @return true or false
	 */
	public function find($sql, $types = array(), $param = array()) {

		$this->queryDebug($sql, array_keys($param), array_values($param), $types);

		// Prepare
		$statement = $this->mdb->get()->prepare($sql,$types);
		if (PEAR::isError($statement)) {
			$this->logger->error("mdb->prepare(sql, types)に失敗しました".$statement->getDebugInfo());
			throw new FatalException();
		}

		// Execute
		$query_result = $statement->execute($param);
		if (PEAR::isError($query_result)) {
			$this->logger->error("mdb->execute(param)に失敗しました".$query_result->getDebugInfo());
			throw new FatalException();
		}

		$fetch_result = $query_result->fetchAll(MDB2_FETCHMODE_ASSOC);
		$statement->free();
		$query_result->free();

		return $fetch_result;
	}
	
	/**
	 * 先頭一行を取得
	 */
	public function first_row($sql, $types = array(), $param = array()) {
		$result = $this->find($sql, $types, $param);
		if (count($result) > 0) {
			return $result[0];
		} else {
			return null;
		}
	}
	
	/**
	 * 先頭1行、1カラム目を取得
	 */
	public function one($sql, $types = array(), $param = array()) {
		$result = $this->first_row($sql, $types, $param);
		if ($result != null) {
			$values = array_values($result);
			return $values[0];
		} else {
			return null;
		}
	}

	/**
	 * 登録処理.
	 * @param $table_name テーブル名
	 * @param $columns カラム配列
	 * @param $types 型配列
	 * @param $values bind配列(array(array(...)))
	 * @return true or false
	 */
	public function insert($table_name, $columns, $types, $values) {

		$this->insertDebug($table_name, $columns, $values, $types);

		// モジュールロード
		$this->mdb->get()->loadModule("Extended");
		// autoPrepare(ステートメントプールを活用)
		$statement = $this->mdb->get()->extended->autoPrepare($table_name, $columns,
						MDB2_AUTOQUERY_INSERT, NULL, $types);
		if (PEAR::isError($statement)) {
			$this->logger->error("mdb->autoPrepareに失敗しました".$statement->getDebugInfo());
			$this->logger->error($statement->getMessage());
			throw new FatalException();
		}

		foreach ($values as $data) {
			// autoExecute
			$query_result = $statement->execute($data);
			if (PEAR::isError($query_result)) {
				$this->logger->error("mdb->autoExecuteに失敗しました".$query_result->getDebugInfo());
				throw new FatalException();
			}
		}
		$statement->free();
		return true;
	}

	/**
	 * 更新処理.
	 * @param $sql SQL
	 * @param $types bind型
	 * @param $param bind値(key,value)
	 * @return true or false
	 */
	public function update($sql, $types, $param) {

		$this->queryDebug($sql, array_keys($param), array_values($param), $types);
		
		// Prepare
		$statement = $this->mdb->get()->prepare($sql,$types);
		if (PEAR::isError($statement)) {
			$this->logger->error("mdb->prepare(sql, types)に失敗しました".$statement->getDebugInfo());
			throw new FatalException();
		}

		// Execute
		$query_result = $statement->execute($param);
		if (PEAR::isError($query_result)) {
			$this->logger->error("mdb->execute(param)に失敗しました".$query_result->getDebugInfo());
			throw new FatalException();
		}
		$statement->free();
		return true;
	}

	/**
	 * TODO 物理削除しないかも
	 * 削除処理.
	 * @param $sql SQL
	 * @param $types bind型
	 * @param $param bind値(key,value)
	 * @return true or false
	 */
	public function delete($sql, $types, $param) {
		return $this->update($sql, $types, $param);
	}

	/**
	 * 最新のシーケンスを取得する.
	 * @param $sequence_name
	 * @return false or seq_no
	 */
	public final function getNextSequence($sequence_name) {

		$seq = $this->mdb->get()->nextID($sequence_name);
		if (PEAR::isError($seq)) {
			$this->logger->error("mdb->nextID('sequence_name')に失敗しました".$seq->getDebugInfo());
			throw new FatalException();
		}
		return $seq;
	}

	/**
	 * 現在のシーケンスを取得する.
	 * @param $sequence_name
	 * @return false or seq_no
	 */
	public final function getCurrentSequence($sequence_name) {

		$seq = $this->mdb->get()->currID($sequence_name);
		if (PEAR::isError($seq)) {
			$this->logger->error("mdb->currID('sequence_name')に失敗しました".$seq->getDebugInfo());
			throw new FatalException();
		}
		return $seq;
	}

	/**
	 * トランザクションを開始する.
	 * @return 開始に成功した場合はtrueを返す.
	 */
	public function beginTransaction() {

		$res = $this->mdb->get()->beginTransaction();
		// エラー処理
		if(PEAR::isError($res)) {
			$this->logger->error("トランザクション開始エラー： ".$res->getDebugInfo());
			throw new FatalException();
		}
		return true;
	}

	/**
	 * ロールバックする.
	 * @return ロールバックに成功した場合はtrueを返す.
	 */
	public function rollback() {

		$res = $this->mdb->get()->rollback();
		// エラー処理
		if(PEAR::isError($res)) {
			$this->logger->error("ロールバックエラー： ".$res->getDebugInfo());
			throw new FatalException();
		}
		return true;
	}

	/**
	 * トランザクションを終了する.
	 * @return 終了に成功した場合はtrueを返す.
	 */
	public function endTransaction() {
		$res = $this->mdb->get()->commit();
		// エラー処理
		if(PEAR::isError($res)) {
			$this->logger->error("トランザクションコミットエラー： ".$res->getDebugInfo());
			throw new FatalException();
		}
		return true;
	}

	private function insertDebug($table_name, $columns, $values, $types) {

		$this->logger->debug("INSERT TABLE:".$table_name);
		foreach($values as $data) {
			for ($i = 0, $size = sizeof($columns); $i < $size;  $i++) {
				$this->logger->debug($columns[$i]."|".var_export($data[$i], TRUE)."|".$types[$i]);
			}
		}
	}

  private function queryDebug($sql, $columns = NULL, $values, $types) {
		$this->logger->debug("PREPARE QUERY:".$sql);
		for ($i = 0, $size = sizeof($columns); $i < $size;  $i++) {
			$this->logger->debug($columns[$i]."|".var_export($values[$i], TRUE)."|".$types[$i]);
			$sql = str_replace(":".$columns[$i], "".var_export($values[$i], TRUE)."", $sql);
		}
		$this->logger->debug("EXECUTE QUERY:".$sql);
	}
}
?>
