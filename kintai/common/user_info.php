<?php
require_once 'kintai/common/mdb.php';
require_once 'kintai/common/code_util.php';

class UserInfo {

    /** param */
    private $key, $key_type;

    // 基本情報
    private $base_info;

    // logger
    private $logger;

    // マスタ
    private $class_info, $attr_info, $dept_info, $room_info, $job_info, $st_info, $condition_info, $auth_info;

    private function get_mdb() {
        return new MDB($this->logger);
    }

    /**
     * UserInfo::get()で情報取得
     * type:"s"はセッション、"u":emp_id
     */
    static function get($key, $key_type = "s") {
        return new UserInfo($key, $key_type);
    }

    private function __construct($key, $key_type) {
        if (empty($key)) {
            return;
        }

        $this->key = $key;
        $this->key_type = $key_type;

        $this->logger = new common_log_class(basename(__FILE__));
    }

    function get_base_info() {
        if (empty($this->base_info)) {
            $sql;
            if ($this->key_type === "s") {
                $sql = "select * from empmst ".
                    "where emp_id = (".
                    "select emp_id from session where session_id = :id".
                    ")";
            } else {
                $sql = "select * from empmst where emp_id = :id";
            }
            $result = $this->get_mdb()->first_row(
                $sql, array("text"), array("id" => $this->key));

            if ($result != null) {
                $base = new BaseInfo();

                $base->emp_id = $result["emp_id"];
                $base->emp_personal_id = $result["emp_personal_id"];
                $base->emp_class = $result["emp_class"];
                $base->emp_attribute = $result["emp_attribute"];
                $base->emp_dept = $result["emp_dept"];
                $base->emp_job = $result["emp_job"];
                $base->emp_st = $result["emp_st"];
                $base->emp_lt_nm = $result["emp_lt_nm"];
                $base->emp_ft_nm = $result["emp_ft_nm"];
                $base->emp_kn_lt_nm = $result["emp_kn_lt_nm"];
                $base->emp_kn_ft_nm = $result["emp_kn_ft_nm"];
                $base->emp_zip1 = $result["emp_zip1"];
                $base->emp_zip2 = $result["emp_zip2"];
                $base->emp_prv = $result["emp_prv"];
                $base->emp_addr1 = $result["emp_addr1"];
                $base->emp_addr2 = $result["emp_addr2"];
                $base->emp_ext = $result["emp_ext"];
                $base->emp_tel1 = $result["emp_tel1"];
                $base->emp_tel2 = $result["emp_tel2"];
                $base->emp_tel3 = $result["emp_tel3"];
                $base->emp_mobile1 = $result["emp_mobile1"];
                $base->emp_mobile2 = $result["emp_mobile2"];
                $base->emp_mobile3 = $result["emp_mobile3"];
                $base->emp_email = $result["emp_email"];
                $base->emp_m_email = $result["emp_m_email"];
                $base->emp_sex = $result["emp_sex"];
                $base->emp_birth = $result["emp_birth"];
                $base->emp_join = $result["emp_join"];
                $base->emp_retire = $result["emp_retire"];
                $base->emp_pic_flg = $result["emp_pic_flg"];
                $base->emp_keywd = $result["emp_keywd"];
                $base->emp_email2 = $result["emp_email2"];
                $base->emp_phs = $result["emp_phs"];
                $base->emp_room = $result["emp_room"];
                $base->emp_idm = $result["emp_idm"];
                $base->emp_profile = $result["emp_profile"];
                $base->updated_on = $result["updated_on"];

                $this->base_info = $base;
            }
        }
        return $this->base_info;
    }

    function get_class_info() {
        if (empty($this->class_info)) {
            $id = $this->get_base_info()->emp_class;

            $sql = "select * from classmst where class_id = :id";
            $result = $this->get_mdb()->first_row(
                $sql, array("integer"), array("id" => $id));
            if ($result != null) {
                $class = new ClassInfo();
                $class->class_id = $result["class_id"];
                $class->class_nm = $result["class_nm"];
                $class->class_del_flg = $result["class_del_flg"];
                $class->area_id = $result["area_id"];
                $class->class_show_flg = $result["class_show_flg"];
                $class->link_key = $result["link_key"];
                $class->order_no = $result["order_no"];
            }
            $this->class_info = $class;
        }
        return $this->class_info;
    }

    function get_attr_info() {
        if (empty($this->attr_info)) {
            $id = $this->get_base_info()->emp_attribute;

            $sql = "select * from atrbmst where atrb_id = :id";
            $result = $this->get_mdb()->first_row(
                $sql, array("integer"), array("id" => $id));
            if ($result != null) {
                $atrb = new AttributeInfo();
                $atrb->atrb_id = $result["atrb_id"];
                $atrb->class_id = $result["class_id"];
                $atrb->atrb_nm = $result["atrb_nm"];
                $atrb->atrb_del_flg = $result["atrb_del_flg"];
                $atrb->link_key = $result["link_key"];
                $atrb->order_no = $result["order_no"];
                $this->attr_info = $atrb;
            }
        }
        return $this->attr_info;
    }

    function get_dept_info() {
        if (empty($this->dept_info)) {
            $id = $this->get_base_info()->emp_dept;

            $sql = "select * from deptmst where dept_id = :id";
            $result = $this->get_mdb()->first_row(
                $sql, array("integer"), array("id" => $id));
            if ($result != null) {
                $dept = new DeptInfo();
                $dept->dept_id = $result["dept_id"];
                $dept->atrb_id = $result["atrb_id"];
                $dept->dept_nm = $result["dept_nm"];
                $dept->dept_del_flg = $result["dept_del_flg"];
                $dept->link_key1 = $result["link_key1"];
                $dept->link_key2 = $result["link_key2"];
                $dept->link_key3 = $result["link_key3"];
                $dept->order_no = $result["order_no"];

                $this->dept_info = $dept;
            }
        }
        return $this->dept_info;
    }

    function get_class_room_info() {
        if (empty($this->room_info)) {
            $id = $this->get_base_info()->emp_room;

            $sql = "select * from classroom where room_id = :id";
            $result = $this->get_mdb()->first_row(
                $sql, array("integer"), array("id" => $id));
            if ($result != null) {
                $room = new ClassRoomInfo();
                $room->room_id = $result["room_id"];
                $room->dept_id = $result["dept_id"];
                $room->room_nm = $result["room_nm"];
                $room->room_del_flg = $result["room_del_flg"];
                $room->link_key = $result["link_key"];
                $room->order_no = $result["order_no"];
                $this->room_info = $room;
            }
        }
        return $this->room_info;
    }

    function get_job_info() {
        if (empty($this->job_info)) {
            $id = $this->get_base_info()->emp_job;

            $sql = "select * from jobmst where job_id = :id";
            $result = $this->get_mdb()->first_row(
                $sql, array("integer"), array("id" => $id));
            if ($result != null) {
                $job = new JobInfo();
                $job->job_id = $result["job_id"];
                $job->job_nm = $result["job_nm"];
                $job->job_del_flg = $result["job_del_flg"];
                $job->order_no = $result["order_no"];
                $job->link_key = $result["link_key"];
                $job->bed_op_flg = $result["bed_op_flg"];

                $this->job_info = $job;
            }
        }
        return $this->job_info;
    }
    function get_st_info() {
        if (empty($this->st_info)) {
            $id = $this->get_base_info()->emp_st;

            $sql = "select * from stmst where st_id = :id";
            $result = $this->get_mdb()->first_row(
                $sql, array("integer"), array("id" => $id));
            if ($result != null) {
                $st = new STInfo();
                $st->st_id = $result["st_id"];
                $st->st_nm = $result["st_nm"];
                $st->st_del_flg = $result["st_del_flg"];
                $st->order_no = $result["order_no"];
                $st->link_key = $result["link_key"];

                $this->st_info = $st;
            }
        }
        return $this->st_info;
    }

    function get_emp_condition() {
        if (empty($this->condition_info)) {
            $id = $this->get_base_info()->emp_id;

            $sql = "select * from empcond where emp_id = :id";
            $result = $this->get_mdb()->first_row(
                $sql, array("text"), array("id" => $id));

            if ($result != null) {
                $condition = new ConditionInfo();

                $condition->emp_id = $result["emp_id"];
                $condition->wage = $result["wage"];
                $condition->unit_price1 = $result["unit_price1"];
                $condition->unit_price2 = $result["unit_price2"];
                $condition->unit_price3 = $result["unit_price3"];
                $condition->unit_price4 = $result["unit_price4"];
                $condition->unit_price5 = $result["unit_price5"];
                $condition->irrg_type = $result["irrg_type"];
                $condition->tmcd_group_id = $result["tmcd_group_id"];
                $condition->hol_minus = $result["hol_minus"];
                $condition->duty_form = $result["duty_form"];
                $condition->atdptn_id = $result["atdptn_id"];
                $condition->no_overtime = $result["no_overtime"];
                $condition->specified_time = $result["specified_time"];
                $condition->paid_hol_tbl_id = $result["paid_hol_tbl_id"];
                $condition->csv_layout_id = $result["csv_layout_id"];
                $condition->paid_hol_start_date = $result["paid_hol_start_date"];
                $condition->am_time = $result["am_time"];
                $condition->pm_time = $result["pm_time"];
                $condition->updated_on = $result["updated_on"];
                $condition->duty_form_name = CodeUtil::get_duty_form($result["duty_form"]);
                $condition->salary_type_name = CodeUtil::get_salary_type($result["wage"]);

                $this->condition_info = $condition;
            }
        }
        return $this->condition_info;
    }
    function get_auth_info() {
        if(empty($this->auth_info)) {
            $id = $this->get_base_info()->emp_id;

            $sql = "select * from authmst where emp_id = '$id'";
            $result = $this->get_mdb()->first_row($sql);

            if ($result != null) {
                $auth_info = new AuthInfo();
                foreach ($result as $key => $value) {
                    $auth_info->$key = $value;
                }
                $this->auth_info = $auth_info;
            }
        }
        return $this->auth_info;
    }
}

class ClassInfo {
    public $class_id, $class_nm;
    public $class_del_flg, $area_id, $class_show_flg, $link_key, $order_no;
}
class AttributeInfo {
    public $atrb_id, $class_id, $atrb_nm, $atrb_del_flg, $link_key, $order_no;
}
class DeptInfo {
    public $dept_id, $atrb_id, $dept_nm, $dept_del_flg;
    public $link_key1, $link_key2, $link_key3, $order_no;
}
class JobInfo {
    public $job_id, $job_nm, $job_del_flg, $order_no, $link_key, $bed_op_flg;
} 
class STInfo {
    public $st_id, $st_nm, $st_del_flg, $order_no, $link_key;
}
class ClassRoomInfo {
    public $room_id, $dept_id, $room_nm, $room_del_flg, $link_key, $order_no;
}
class BaseInfo {
    public $emp_id, $emp_personal_id;
    public $emp_class, $emp_attribute, $emp_dept, $emp_job, $emp_st;
    public $emp_lt_nm, $emp_ft_nm, $emp_kn_lt_nm, $emp_kn_ft_nm;
    public $emp_zip1, $emp_zip2, $emp_prv, $emp_addr1, $emp_addr2, $emp_ext;
    public $emp_tel1, $emp_tel2, $emp_tel3, $emp_mobile1, $emp_mobile2, $emp_mobile3;
    public $emp_email, $emp_m_email, $emp_sex, $emp_birth;
    public $emp_join, $emp_retire, $emp_pic_flg, $emp_keywd,$emp_email2;
    public $emp_phs, $emp_room, $emp_idm, $emp_profile, $updated_on;
}
class ConditionInfo {
    public $emp_id, $wage, $unit_price1,$unit_price2,$unit_price3,$unit_price4,$unit_price5;
    public $irrg_type, $tmcd_group_id, $hol_minus, $duty_form, $atdptn_id, $no_overtime;
    public $specified_time, $paid_hol_tbl_id, $csv_layout_id, $paid_hol_start_date, $am_time, $pm_time, $updated_on;

    // 名称変換用フィールド追加
    public $duty_form_name, $salary_type_name;
}
class AuthInfo {

}
?>
