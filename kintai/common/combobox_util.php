<?php 
require_once("about_comedix.php");

/**
 * 日付、時刻関連をコンボボックスを作成するクラス
 */
class TimeComboBox {

    /**
     * 年のコンボボックスを作成
     * @param $name 属性値(name, id)
     * @param $under_limit 過去何年まで
     * @param $select_year 現在年
     */
    static function output_select_years($name, $under_limit, $select_year = null) {

        // 現在年を取得
        $current_year = date('Y', time());
        if (empty($select_year)) $select_year = $current_year;
        // 翌年を取得
        $next_year = $current_year+1;

        $start_year = $current_year - $under_limit;

        echo "<select name=\"$name\" id=\"$name\">";
        echo "<option />";
        for ($i = $start_year; $i <= $next_year; $i++) {
            if ($select_year == $i) {
                echo "<option value=\"$i\" selected>$i</option>";
            } else {
                echo "<option value=\"$i\">$i</option>";
            }
        }
        echo "</select>";
    }

    /**
     * 月のコンボボックスを作成
     * value値は先頭0埋めで出力する
     * @param $name 属性値(name, id)
     * @param $select_month 現在月
     */
    static function output_select_months($name, $select_month = null) {

        if (empty($select_month)) {
            $select_month = date('n', time());
        }

        echo "<select name=\"$name\" id=\"$name\">";
        echo "<option />";
        for ($month = 1; $month <= 12; $month++) {
            if (intval($select_month) == $month) {
                echo "<option value=\"".sprintf('%02d', $month)."\" selected>$month</option>";
            } else {
                echo "<option value=\"".sprintf('%02d', $month)."\">$month</option>";
            }
        }
        echo "</select>";
    }

    /**
     * 日のコンボボックスを作成
     * value値は先頭0埋めで出力する
     * @param $name 属性値(name, id)
     * @param $select_day 現在日
     */
    static function output_select_days($name, $select_day = null) {

        if (empty($select_day)) $select_day = date('j', time());

        echo "<select name=\"$name\" id=\"$name\">";
        echo "<option />";
        for ($day = 1; $day <= 31; $day++) {
            if (intval($select_day) == $day) {
                echo "<option value=\"".sprintf('%02d', $day)."\" selected>$day</option>";
            } else {
                echo "<option value=\"".sprintf('%02d', $day)."\">$day</option>";
            }
        }
        echo "</select>";
    }

    /**
     * 時のコンボボックスを作成
     * @param $name 属性値(name, id)
     * @param $select_hour 現在時
     * @param $max_hour 最大時間
     */
    static function output_select_hours($name, $select_hour = null, $max_hour = 23, $disabled = "") {

        echo "<select name=\"$name\" id=\"$name\" $disabled>";
        echo "<option />";
        for ($hour = 0; $hour <= $max_hour; $hour++) {
            if (!empty($select_hour) && intval($select_hour) == $hour) {
                echo "<option value=\"".sprintf('%02d', $hour)."\" selected>".$hour."</option>";
            } else {
                echo "<option value=\"".sprintf('%02d', $hour)."\">".$hour."</option>";
            }
        }
        echo "</select>";
    }

    /**
     * 分のコンボボックスを作成
     * @param $name 属性値(name, id)
     * @param $select_minute 現在時
     * @param $interval 表示間隔
     */
    static function output_select_minutes($name, $select_minute = null, $interval = 1, $disabled = "") {

        echo "<select name=\"$name\" id=\"$name\" $disabled>";
        echo "<option />";
        $interval_num = 0;
        for ($minute = 0; $minute < 60; $minute++) {
            if ($minute == $interval_num) {
                if (!empty($select_minute) && intval($select_minute) == $minute) {
                    echo "<option value=\"".sprintf("%02d", $minute)."\" selected>".sprintf("%02d", $minute)."</option>";
                } else {
                    echo "<option value=\"".sprintf("%02d", $minute)."\">".sprintf("%02d", $minute)."</option>";
                }
                $interval_num += $interval;
            }
        }
        echo "</select>";
    }
}

/**
 * 日付、時刻以外のコンボボックスを作成する
 */
class ComboBox {

    /**
     * コンボボックスを作成する
     * @param $name属性
     * @param $list 連想配列(keyが属性名、valueが属性値、"label"はテキスト)
     * @param $value 選択する値
     * @param $parent 親コンボボックスと関連づける場合
     */
    static function output_select($name, $list, $value = null, $parent = null) {
        echo "<select name=\"$name\" id=\"$name\">";
        echo "<option />";
        foreach($list as $item) {
            // 親子関連
            if ("" != strval($parent) && $parent != $item["parent"] ) {
                // 関連づかないものはスキップ
                continue; 
            }

            $attr = "";
            $keys = array_keys($item);
            foreach ($keys as $key) {
                if ($key != "label") {
                    $attr .= " $key=$item[$key]";
                }
            }
            if ($value == $item["value"]) {
                echo "<option $attr selected>".h($item["label"])."</option>";
            } else {
                echo "<option $attr >".h($item["label"])."</option>";
            }
        }
        echo "</select>";
    }
}
?>
