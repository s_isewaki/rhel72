<?php
set_include_path(get_include_path() . PATH_SEPARATOR . './libs/phpexcel/Classes/');
include 'PHPExcel.php';
include 'PHPExcel/IOFactory.php';

// PHP Excel のライブラリ・クラスオブジェクトを扱うクラス 2012.1.11
// シートは1つのみ。複数シートを出力する際は改造してください。
class ExcelWorkShop{
	private $xl;
	private $excelSheet;
	private $CellArea;
	private $styleCondition;
	private $borderStyleArrayTHICK;
	private $borderStyleArrayTHIN;
	private $borderStyleArray;
	private $borderType;
	private $defFont , $defFontSize , $defCharColor , 
		$defBackColor , $defBorderThick , $defBorderType , $defBorderColor , 
		$defVirticalPos , $defHorizonPos ,
		$defCellFormatStyle;
	private $objDrawing;
	public  $writer;
	// コンストラクタ
	function __construct(){
		// エクセルオブジェクト
		$this->xl = new PHPExcel();
		// エクセルオブジェクトにsheet1を設定
		$this->xl->setActiveSheetIndex(0);
		// シートオブジェクト
		$this->excelSheet = $this->xl->getActiveSheet();
		// 出力オブジェクト
		$this->writer = PHPExcel_IOFactory::createWriter($this->xl, "Excel5");
		// 既定値フォント
		$this->xl->getDefaultStyle()->getFont()->setName(mb_convert_encoding('ＭＳ Ｐゴシック','UTF-8','EUC-JP')); // 標準フォント設定
		$this->xl->getDefaultStyle()->getFont()->setSize(9); // 標準フォントサイズ12p
		$this->defBackColor = "FFFFFF" ; // セルの既定値色は白
		$this->defdefCharColor = "000000" ; // セルの既定値フォント色は黒、フォント書式はセル単位でしか扱えない
		// 枠線非表示。動的に切り替えたい場合はメソッドを追加してください
		$this->xl->getActiveSheet()->setShowGridlines(false);
		// 描画オブジェクト
		$this->objDrawing = new PHPExcel_Worksheet_Drawing();
		// 条件つきフォーマットのオブジェクト。セルのみ
		$this->styleCondition = new PHPExcel_Style_Conditional();
		$this->styleCondition->setConditionType(PHPExcel_Style_Conditional::CONDITION_CELLIS);
	}
	
	//シート追加
	function setNewSheet($cnt){
		$this->xl->createSheet(); 
		$this->xl->setActiveSheetIndex($cnt);
		// シートオブジェクト
		$this->excelSheet = $this->xl->getActiveSheet();
		// 既定値フォント
		$this->xl->getDefaultStyle()->getFont()->setName(mb_convert_encoding('ＭＳ Ｐゴシック','UTF-8','EUC-JP')); // 標準フォント設定
		$this->xl->getDefaultStyle()->getFont()->setSize(9); // 標準フォントサイズ12p
		// 枠線非表示。動的に切り替えたい場合はメソッドを追加してください
		$this->xl->getActiveSheet()->setShowGridlines(false);
	}

	//操作シート
	function setActiveSheet($cnt){
		$this->xl->setActiveSheetIndex($cnt);
	}

	// sheet名
	function SetSheetName( $_name ){
		$this->excelSheet->setTitle( $_name );
	}
	
	// セルに対して何かするときは必ずこのSetAreaを設定すること
	// セル修飾を規定値で使うときはSetValueWithAttrib()を使うこと
	function SetArea( $_cell_area ){
		$this->CellArea = $_cell_area;
	}
	
	// デバッグ用
	function GetArea(){
		return ($this->CellArea);
	}
	
	// セルへ値を設定
	// セル修飾を規定値で使うときはSetValueWithAttrib()を使うこと
	// EUC-JPのときはSetValueJP()を使うこと
	function SetValue( $_value ){
		$this->excelSheet->setCellValue( $this->CellArea , $_value );
	}
	
		// セルへ値を設定、EUC-JP日本語専用
	// 特に�´◆ΑΑΔ覆百殄佞�数字などEUC-JPで文字化けしそうな字を扱うときはこれを使うこと
	function SetValueJP( $_value ){
        $this->excelSheet->setCellValue( $this->CellArea , mb_convert_encoding(mb_convert_encoding($_value,"sjis-win","EUC-JP"),"UTF-8","sjis-win"));
	}
	
	// 【20120207NEW】位置を指定してセルへ値を設定
	// EUC-JPのときはSetValueJPwith()を使うこと
	// 特に�´◆ΑΑΔ覆百殄佞�数字などEUC-JPで文字化けしそうな字を扱うときはこれを使うこと
	function SetValuewith( $_cell_area  , $_value , $_style ){
		$this->CellArea = $_cell_area;
		$this->excelSheet->setCellValue( $this->CellArea , $_value );
		if( $_style != "" ){
			$this->PrivateSubCall( $_style );
		}
	}
	
	// 【20120207NEW】位置を指定してセルへ値を設定
	// EUC-JP専用
	// $_styleの指定は以下の通り。指定順自由
	function SetValueJPwith( $_cell_area  , $_value , $_style ){
		$this->CellArea = $_cell_area;
        $this->excelSheet->setCellValue( $this->CellArea , mb_convert_encoding(mb_convert_encoding($_value,"sjis-win","EUC-JP"),"UTF-8","sjis-win"));
		if( $_style != "" ){
			$this->PrivateSubCall( $_style );
		}
	}
	
	// 【20120207NEW】位置を指定してセルへ値を設定、修飾、フォント名、フォントサイズを指定
	// $_style の文字列はいくつでも同時指定可。指定順自由
	function SetValueJP2( $_cell_area  , $_value , $_style , $_font , $_font_sz){
		$this->CellArea = $_cell_area;
        $this->excelSheet->setCellValue( $this->CellArea , mb_convert_encoding(mb_convert_encoding($_value,"sjis-win","EUC-JP"),"UTF-8","sjis-win"));
		if( $_style != "" ){
			$this->PrivateSubCall( $_style );
		}
		if( $_font != "" ){
			if( $_font_sz == "" || $_font_sz == 0 ){$_font_sz = 11;}
			$this->SetFont( $_font , $_font_sz );
		}
	}
	
	// 【20120207NEW】内部専用メソッド
	function PrivateSubCall( $_style ){
		if ( preg_match("/BOLD/",$_style)){
			$this->SetFontBold();
		}
		if ( preg_match("/ITALIC/",$_style)){
			$this->SetFontItalic();
		}
		if ( preg_match("/WRAP/",$_style)){
			$this->SetWrapText();
		}
		if ( preg_match("/UNDER/",$_style)){
			$this->SetFontUnderline("true");
		}
		if ( preg_match("/FITSIZE/",$_style)){
			$this->SetShrinkToFit();
		}
		if ( preg_match("/VCENTER/",$_style,$hit) || preg_match("/TOP/",$_style,$hit) || preg_match("/BOTTOM/",$_style,$hit)){
			$this->SetPosV( $hit[0] );
		}
		if ( preg_match("/HCENTER/",$_style,$hit) || preg_match("/LEFT/",$_style,$hit) || preg_match("/RIGHT/",$_style,$hit)){
			$this->SetPosH( $hit[0] );
		}
	}
	
	// 前に指定した修飾を有効としたまま値をセルへ設定する。性能が悪いので要注意
	// フォント、フォントサイズ、セル文字の色、背景色、セル内の縦位置、セル内の横位置
	function SetValueWithAttrib( $_cell_area , $_value ){ // セルの位置 , 値
		$this->CellArea=$_cell_area;
		$this->excelSheet->setCellValue( $_cell_area , $_value );
		$this->SetFont( $this->defFont , $this->defFontSize);
		$this->SetCharColor( $this->defCharColor );
		$this->SetBackColor( $this->defBackColor );
		$this->SetPosH( $this->defHorizonPos );
		$this->SetPosV( $this->defVirticalPos );
	}
	
	// フォント名とサイズ、セルへ反映
	function SetFont( $_type , $_size ){
		$this->xl->GetActiveSheet()->getStyle($this->CellArea)->getFont()->setName( $_type );
		$this->xl->GetActiveSheet()->getStyle($this->CellArea)->getFont()->setSize( $_size );
		$this->defFont = $_type;
		$this->defFontSize = $_size;
	}
	
	// 【20120207NEW】太字 B
	function SetFontBold(){
		$this->xl->GetActiveSheet()->getStyle($this->CellArea)->getFont()->setBold(true);
	}
	
	// 【20120207NEW】太字Ｂ解除
	function SetFontUnBold(){
		$this->xl->GetActiveSheet()->getStyle($this->CellArea)->getFont()->setBold(false);
	}
	
	// 【20120207NEW】ITALIC I
	function SetFontItalic(){
		$this->xl->GetActiveSheet()->getStyle($this->CellArea)->getFont()->setItalic(true);
	}
	
	// 【20120207NEW】ITALIC解除
	function SetFontUnItalic(){
		$this->xl->GetActiveSheet()->getStyle($this->CellArea)->getFont()->setItalic(false);
	}
	
	// 【20120207NEW】下線
	function SetFontUnderline( $_sw ){ // true:下線つける false:下線外す
		if( $_sw == "true" ){
			$this->xl->GetActiveSheet()->getStyle($this->CellArea)->getFont()->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
		}else{
			$this->xl->GetActiveSheet()->getStyle($this->CellArea)->getFont()->setUnderline(PHPExcel_Style_Font::UNDERLINE_NONE);
		}
	}
	
	// フォント名とサイズ、指定だけ
	function SetFontDef( $_type , $_size ){
		$this->defFont = $_type;
		$this->defFontSize = $_size;
	}
	
	// 文字色。セル内の全文字が対象、セルへ反映
	function SetCharColor( $_color ){ // rrggbb
		$this->xl->getActiveSheet()->getStyle($this->CellArea)->getFont()->getColor()->setARGB( $_color );
		$this->defCharColor = $_color ;
	}
	
	// 文字色、指定だけ
	function SetCharColorDef( $_color ){ // rrggbb
		$this->defCharColor = $_color ;
	}
	
	// セルの色、セルへ反映
	// SetFillTypeを実行後にsetRGBしないと色がつかない
	function SetBackColor( $_color ){ // rrggbb
		$this->xl->GetActiveSheet()->getStyle($this->CellArea)->getFill()->SetFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$this->xl->GetActiveSheet()->getStyle($this->CellArea)->getFill()->getStartColor()->setRGB( $_color );
		$this->defBackColor = $_color;
	}
	
	// セルの色指定解除、セルへ反映
	function SetBackColorCancel(){ 
		$this->xl->GetActiveSheet()->getStyle($this->CellArea)->getFill()->SetFillType(PHPExcel_Style_Fill::FILL_NONE);
	}
	
	// 【20120207NEW】網掛け
	// $_maskの値は以下のいずれか
	// 'linear'
	// 'path'
	// 'darkDown'
	// 'darkGray'
	// 'darkGrid'
	// 'darkHorizontal'
	// 'darkTrellis'
	// 'darkUp'
	// 'darkVertical'
	// 'gray0625'
	// 'gray125'
	// 'lightDown'
	// 'lightGray'
	// 'lightGrid'
	// 'lightHorizontal'
	// 'lightTrellis'
	// 'lightUp'
	// 'lightVertical'
	// 'mediumGray'
	function SetMask( $_mask ){ 
		$this->xl->GetActiveSheet()->getStyle($this->CellArea)->getFill()->SetFillType( $_mask );
	}
	
	// セルの色、指定だけ
	function SetBackColorDef( $_color ){ // aarrggbb
		$this->defBackColor = $_color;
	}
	
	// 【20120207機能追加】セルの書式設定、桁数など、セルへ反映
	// 例。'00.0' を指定すると 12 は 12.0 と表示される
	// エクセルの「セルの書式設定」の「表示形式」の設定を指定する。
	// 設定値により色を付ける場合
	// '[Blue][>=3000]$#,##0;[Red][<0]$#,##0;$#,##0' <-- $記号はUSドルの意味
	function SetFormatStyle( $_style ){
		$this->xl->GetActiveSheet()->getStyle($this->CellArea)->getNumberFormat()->setFormatCode( $_style );
		$this->defCellFormatStyle = $_style;
	}
	
	// 【2012.02.07UPDATE】罫線、MEDIUMを追加2012.2.7
	// THICK:太い THIN:細い MEDIUM:中間, 色。色を省略したら黒 , outline:外周、all:範囲内縦横
	// 上下左右が必要なときは追加してください。
	function SetBorder( $_thick , $_color  , $_type ){ 
		
		if( $_color == "" ){
			$_color='FF000000';
		}
		switch ($_type){
		case 'outline'	: $this->borderType = $_type;
				break;
		default	      :	$this->borderType = 'allborders';
		}
		$this->borderStyleArrayTHICK = array(
			'borders' => array(
				$this->borderType => array(
					'style' => PHPExcel_Style_Border::BORDER_THICK, // 細い
					'color' => array('argb' => $_color),
				),
			),
		);
		$this->borderStyleArrayTHIN = array(
			'borders' => array(
				$this->borderType => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN, // 中くらい
					'color' => array('argb' => $_color),
				),
			),
		);
		$this->borderStyleArrayMEDIUM = array(
			'borders' => array(
				$this->borderType => array(
					'style' => PHPExcel_Style_Border::BORDER_MEDIUM, // 太い
					'color' => array('argb' => $_color),
				),
			),
		);
		
		switch ($_thick){
		case 'THIN': 	$this->excelSheet->getStyle($this->CellArea)->applyFromArray($this->borderStyleArrayTHIN);
				break;
		case 'MEDIUM': 	$this->excelSheet->getStyle($this->CellArea)->applyFromArray($this->borderStyleArrayMEDIUM);
				break;
		default	   :	$this->excelSheet->getStyle($this->CellArea)->applyFromArray($this->borderStyleArrayTHICK);
		}
		$this->defBorderThick = $_thick;
		$this->defBorderType  = $_color;
		$this->defBorderColor = $_type;
	}
	
	// 【20120209NEW】罫線処理見直し
	/* $_thick は以下の文字列を指定する **
	** Border style from PHP Excel **
	'none';		// 線無し
	'dashDot';	// 破線 ･････
	'dashDotDot';	// 一点破線 -･-･-･-･
	'dashed';	// -･･-･･-･･-･･
	'dotted';	// 点線 -----
	'double';	// 二重線
	'hair';		// とても細い破線
	'medium';	// 中くらいの太さ
	'mediumDashDot';
	'mediumDashDotDot';
	'mediumDashed';
	'slantDashDot';
	'thick';	// 太い
	'thin';		// 細い・標準の線
	
	** $_typeは罫線を引く位置。次の文字列で指定する
	'top'		// 上
	'bottom'	// 下
	'left'		// 左
	'right'		// 右
	'allborders'	// 上下左右と指定範囲内の縦横
	'outline'	// 周囲
	'inside'	// 指定範囲内の上下、周囲は引かない
	'horizontial'	// 指定範囲内の横線
	'vertical'	// 指定範囲内の縦線
	*/
	function SetBorder2( $_thick , $_color  , $_type ){
		if( $_color == "" ){
			$_color = '000000'; // 色指定はrgb
		}
		$this->borderType = $_type;
		$this->borderStyleArray = array('borders' => array($this->borderType => array('style' => $_thick ,'color' => array('argb' => $_color),),),);
		$this->excelSheet->getStyle($this->CellArea)->applyFromArray($this->borderStyleArray);
	}

	// 縦位置、セルへ反映
	function SetPosV( $_v  ) {    // 'CENTER','TOP','BOTTOM'
		switch ( $_v ){
		case 'VCENTER':
		case 'CENTER':	$this->xl->getActiveSheet()->getStyle($this->CellArea)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				break;
		case 'TOP':	$this->xl->getActiveSheet()->getStyle($this->CellArea)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
				break;
		default   :	$this->xl->getActiveSheet()->getStyle($this->CellArea)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_BOTTOM);
		}
		$this->defVirticalPos = $_v;
	}

	// 横位置、セルへ反映
	function SetPosH( $_h ) {     // 'LEFT','CENTER','RIGHT'
		switch ( $_h ){
		case 'HCENTER':
		case 'CENTER':	$this->xl->getActiveSheet()->getStyle($this->CellArea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				break;
		case 'LEFT'  :	$this->xl->getActiveSheet()->getStyle($this->CellArea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
				break;
		case 'JUST'  :	$this->xl->getActiveSheet()->getStyle($this->CellArea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_GENERAL);
				break;
		default	     :	$this->xl->getActiveSheet()->getStyle($this->CellArea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		}
		$this->defHorizonPos = $_h;
	}

	// 縦位置、指定だけ
	function SetPosVDef( $_v  ) {    // 'CENTER','TOP','BOTTOM'
		$this->defVirticalPos = $_v;
	}

	// 横位置、指定だけ
	function SetPosHDef( $_h ) {     // 'LEFT','CENTER','RIGHT'
		$this->defHorizonPos = $_h;
	}

	// セル結合、指定済みの領域を結合
	function CellMergeReady(){
		$this->CellMerge($this->CellArea);
	}

	// セル結合、領域を指定して結合
	function CellMerge($_area){ // 
		$this->excelSheet->mergeCells($_area);
		$this->CellArea = $_area;
	}

	// セル結合、領域を指定して結合、罫線指定、背景色指定
	function CellMerge2($_area , $_border_line , $_border_type , $_color){ // 
		$this->excelSheet->mergeCells($_area);
		$this->CellArea = $_area;
		if ($_border_line != "" ){
			$this->SetBorder2($_border_line,"",$_border_type);
		}
		if ($_color != ""){
			$this->SetBackColor( $_color );
		}
	}

	// セル内で折り返す
	function SetWrapText(){
		$this->xl->getActiveSheet()->getStyle($this->CellArea)->getAlignment()->setWrapText(true);
	}

	// 【20120207NEW】セル内、縮小して全体を表示する
	function SetShrinkToFit(){
		$this->xl->getActiveSheet()->getStyle($this->CellArea)->getAlignment()->setShrinkToFit(true);
	}

	// 条件付き書式セル色設定。条件を変えて呼び出すと設定を追加できる。2012.2.20現在、機能せず。
	// $_conditionには以下のいずれかの値を設定する
	// '';
	// 'beginsWith';
	// 'endsWith';
	// 'equal';
	// 'greaterThan';
	// 'greaterThanOrEqual';
	// 'lessThan';
	// 'lessThanOrEqual';
	// 'notEqual';
	// 'containsText';
	// 'notContains';
	// 'between';
	// $_valueには値を設定する
	// $_colorは変化させたい色指定(argv)
	// 例。値が 0 のときにセルの背景色を赤にする場合は、$_condition='equal',$_value=0,$_color=FFFF0000
	//     セルの位置は他のメソッドで位置が指定されていること。
	function SetStyleConditional( $_condition , $_value , $_color ){
		$this->styleCondition->setOperatorType( $_condition );
		$this->styleCondition->addCondition( $_value );
		$this->styleCondition->getStyle()->getFont()->getColor()->setARGB( $_color );
		$this->styleCondition->getStyle()->getFont()->setBold(true);
		$CStyle = $this->xl->getActiveSheet()->getStyle( $this->CellArea )->getConditionalStyles();
		array_push( $CStyle , $this->styleCondition );
		$this->xl->getActiveSheet()->getStyle( $this->CellArea )->setConditionalStyles( $CStyle );
	}

	// 列幅 Excel2003の場合、指定値に0.62を加算する
	// 列幅をゼロにして表示しない場合は、SetColDim( 列名 , -0.62 )と指定する。
	function SetColDim( $_col , $_csize ){ // 列名、サイズ
		$this->xl->getActiveSheet()->getColumnDimension($_col)->setWidth($_csize + 0.62);
	}

	// 行高さ
	function SetRowDim( $_row , $_rsize ){ // 行番号、サイズ
		$this->xl->getActiveSheet()->getRowDimension($_row)->setRowHeight($_rsize);
	}

	// 【20120207NEW】表示時の画面表示倍率
	function DisplayZoom( $_number ){
		$this->excelSheet->getSheetView()->setZoomScale( $_number );
	}

	// 【20120207NEW】イメージデータ描画。読み込みパス、高さ(既定値のときは0または"")、描画オブジェクト名(何でも良い)、セル位置、
	// 例。image.gifを読み込み高さ30で B3 へ表示するとき
	// $ExcelObj->ImageDraw( "./image.gif","30", "image-data" , "B3" );
	function ImageDraw( $_path , $_size , $_name , $_cell_area ){
		$this->objDrawing->setName( $_name );
		$this->objDrawing->setDescription( $_name );
		$this->objDrawing->setPath( $_path );
		if( $_size != "" || $_size!= 0 ){
			$this->objDrawing->setHeight( $_size );
			$this->objDrawing->setWidth( $_size );
		}
		$this->objDrawing->setCoordinates( $_cell_area );
//		$this->objDrawing->getShadow()->setVisible(true);
		$this->objDrawing->setWorksheet( $this->excelSheet );
	}

	// *** ここからはブック全体の指定 ***

	// 用紙サイズ、ここではA3,A4,B4を扱う、他のサイズが必要なときは追加してください。
	function PaperSize( $_name ){
		switch ( $_name ){
			case "A4":$this->xl->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);break;
			case "B4":$this->xl->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_B4);break;
			default	 :$this->xl->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A3);break; // 既定値
		}
	} 

	// 用紙方向
	function PageSetUp( $_name ){ // 縦virticalか横horizontialか
		if( $_name == 'virtical'){
			$this->xl->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetUp::ORIENTATION_PORTRAIT); // 縦
		}else{
			$this->xl->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetUp::ORIENTATION_LANDSCAPE); // 横
		}
	}

	// 拡大縮小と倍率
	function PageRatio( $_name , $_value){ // 'USER' 'AUTO', 縮小拡大倍率。'AUTO'のときは倍率$_valueは無視される
		switch ($_name){
			case "USER":	$this->xl->getActiveSheet()->getPageSetup()->setFitToPage(false); // 倍率指定のほうへチェック
					$this->xl->getActiveSheet()->getPageSetup()->setScale($_value);
					break;
			default	   :	$this->xl->getActiveSheet()->getPageSetup()->setFitToPage(true); // 倍率を縦横に合わせて自動設定
					$this->xl->getActiveSheet()->getPageSetup()->setFitToWidth(1);   // 合わせるページは縦横ともに1
					$this->xl->getActiveSheet()->getPageSetup()->setFitToHeight(1);
		}
	}

	// 余白、インチ単位で指定
	function SetMargin( $_left , $_right , $_top , $_bottom , $_head , $_foot){ // 左右上下頭足
		// 日本語版Excelではメートル法で表示される。計算で変換する。1in=25.4mm
		$this->xl->getActiveSheet()->getPageMargins()->setHeader($_head / 2.54);
		$this->xl->getActiveSheet()->getPageMargins()->setFooter($_foot / 2.54);
		$this->xl->getActiveSheet()->getPageMargins()->setTop($_top / 2.54);
		$this->xl->getActiveSheet()->getPageMargins()->setRight($_right / 2.54);
		$this->xl->getActiveSheet()->getPageMargins()->setLeft($_left / 2.54);
		$this->xl->getActiveSheet()->getPageMargins()->setBottom($_bottom / 2.54);
	}
	
	// 枠線表示・非表示切替
	function SetGridline( $_sw ){ // true or false
		$this->xl->getActiveSheet()->setShowGridlines( $_sw );
	}

	// ヘッダのフォントを動的に変更する場合はメソッドを追加してください。
	function SetPrintHeader( $_name ){
		$this->xl->getDefaultStyle()->getFont()->setName(mb_convert_encoding('ＭＳ Ｐゴシック','UTF-8','EUC-JP')); // 標準フォント設定
		$this->xl->getDefaultStyle()->getFont()->setSize(11); // 標準フォントサイズ11p
        $this->xl->getActiveSheet()->getHeaderFooter()->setOddHeader( mb_convert_encoding(mb_convert_encoding($_name,"sjis-win","EUC-JP"),"UTF-8","sjis-win") );
	}
	
	function SetPrintFooter( $_name ){
		//$this->xl->getActiveSheet()->getHeaderFooter()->setOddFooter('&L&B' . $objPHPExcel->getProperties()->getTitle() . '&RPage &P of &N');
		$this->xl->getDefaultStyle()->getFont()->setName(mb_convert_encoding('ＭＳ Ｐゴシック','UTF-8','EUC-JP')); // 標準フォント設定
		$this->xl->getDefaultStyle()->getFont()->setSize(11); // 標準フォントサイズ11p
        $this->xl->getActiveSheet()->getHeaderFooter()->setOddFooter( mb_convert_encoding(mb_convert_encoding($_name,"sjis-win","EUC-JP"),"UTF-8","sjis-win") );
	}
	
	// 印刷範囲指定
	function SetPritingArea( $_area ){
		$this->xl->getActiveSheet()->getPageSetup()->setPrintArea( $_area );
	}
	
	// 行のタイトル
	function setRowTitle($s, $e){
		$this->xl->getActiveSheet()->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd($s, $e);

	}
	
	//デフォルトフォントなど
	function setDefaultFont($f, $s, $b){
		$this->xl->getDefaultStyle()->getFont()->setName(mb_convert_encoding($f,'UTF-8','EUC-JP')); // 標準フォント設定
		$this->xl->getDefaultStyle()->getFont()->setSize($s); // 標準フォントサイズ12p
		$this->excelSheet->getDefaultRowDimension()->setRowHeight(16.5); 
		$this->excelSheet->getPageSetup()->setHorizontalCentered($b);
		//$this->excelSheet->freezePaneByColumnAndRow(0,6);

  
	}
	
	//ウィンド枠固定
	function setPane($c, $r){
		$this->excelSheet->freezePaneByColumnAndRow($c, $r);
	}
	
	//2重線　上
	function setLineBottomTOP(){
		$this->excelSheet->getStyle($this->CellArea)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);
	}
	
	// ダウンロード形式で出力
	function OutPut(){
		$this->writer->save('php://output');
	}
	function Save( $_fn ){
		$this->writer->save( $_fn );
	}
}
?>
