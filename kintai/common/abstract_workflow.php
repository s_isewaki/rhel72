<?php
/**
 * ワークフロー抽象クラス
 * ワークフロー機能から呼び出される際はこのクラスを必ず経由する
 */
abstract class AbstractWorkflow {

	/**
	 * 申請登録
	 * @param $id 申請ID
	 */
	abstract protected function regist_apply($id); // 申請
	
	/**
	 * 申請更新
	 * @param $id 申請ID
	 */
	abstract protected function update_apply($id); // 更新、ステータス変更なし
	
	/**
	 * 申請取消
	 * @param $id 申請ID
	 */
	abstract protected function cancel_apply($id); // 取消、物理削除
	
	/**
	 * 再申請
	 * 新しいIDが采番される
	 * @param $new_id 新申請ID
	 * @param $old_id 旧申請ID
	 */
	abstract protected function regist_re_apply($new_id, $old_id); // 再申請
	
	/**
	 * 承認
	 * @param $id 申請ID
	 */
	abstract protected function approve($id, $con); // 承認
	
	/**
	 * ステータス更新
	 * @param $id 申請ID
	 * @param $status 申請状態(0:未承認, 1:承認済, 2:否認, 3:差戻)
	 */
	abstract protected function update_status($id, $status); // 申請、差戻し、否認のみ
	
	/**
	 * ワークフロー削除(管理画面から)
	 * @param $id 申請ID
	 */
	abstract protected function delete($id); // ワークフローの削除
}
?>
