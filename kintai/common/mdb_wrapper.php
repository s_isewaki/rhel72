<?php
require_once './common_log_class.ini';
require_once './kintai/common/mdb2.conf';
require_once './kintai/common/exception.php';

/**
 * MDBラッパークラス.
 */
class MDBWrapper {

	// コネクション
	private $connection;
	// ロガー
	private $log;
	// 接続状態
	public $isConnect = false;

	/**
	 * コンストラクタ.
	 */
	private function __construct() {
		// Logインスタンス
		$this->log = new common_log_class(basename(__FILE__));
		// コネクション取得
		$this->connection =& MDB2::connect(CMX_DB_DSN);
		if (PEAR::isError($this->connection)) {
			$this->log->error("DB接続エラー： ".$this->connection->getDebugInfo());
			throw new FatalException();
		} else {
			$this->isConnect = true;
		}
	}
	
	static function init() {
		$GLOBALS['kintai_mdb'] = new MDBWrapper();
	}
	
	static function destroy() {
		unset($GLOBALS['kintai_mdb']);
	}
	
	/**
	 * コネクションを取得する.
	 */
	public function get() {
		return $this->connection;
	}
}