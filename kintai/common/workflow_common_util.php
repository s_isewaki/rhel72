<?php
require_once 'kintai/common/mdb.php';

/**
 * ワークフロー共通
 */
class WorkflowCommonUtil {
	
	/**
	 * 申請IDがどのワークフローかを取得する
	 */
	static function get_template_type($id) {
		if (self::apply_exists("kintai_hol", $id) && self::apply_exists("kintai_late", $id)){
			return "late";
		}
		
		if (self::apply_exists("kintai_hol", $id)) {
			return "hol";
		}
		if (self::apply_exists("kintai_ovtm", $id)) {
			return "ovtm";
		}
		if (self::apply_exists("travel_wkfw", $id)) {
			return "travel";
		}
        if (self::apply_exists("kintai_late", $id)) {
            return "late";
        }
        
		return null;
	}
	
    /**
     * 承認が確定されているかどうか
     */
    static function is_apply_complete($con, $id) {
        $result = pg_fetch_array(pg_query($con, "select apply_stat from apply where apply_id = $id"));
        return $result["apply_stat"];
    }
    
    /**
	 * 指定されたテーブルに指定された申請IDが存在するか
	 */
	static function apply_exists($table, $apply_id) {
		
		$sql = "select count(apply_id) from $table where apply_id = :apply_id";
		$count = self::get_mdb()->one($sql, array("integer"), array("apply_id" => $apply_id));

		if ($count > 0) {
			return true;
		}
		return false;
	}
	
	/**
	 * 申請日のみを更新する
	 */
	static function apply_date_update($con, $apply_id) {

		$log = self::get_logger();

		$template_type = self::get_template_type($apply_id);

		$table;
		switch ($template_type) {
		case "hol":
			$table = "kintai_hol";
			break;
		case "ovtm":
			$table = "kintai_ovtm";
			break;
		case "travel":
			$table = "travel_wkfw";
			break;
        case "late":
            $table = "kintai_late";
            break;
        default:
			return;
		}
		
		// 日付を取得
		$get_date_sql = "select apply_date from $table where apply_id = :apply_id";
		$date = self::get_mdb()->one($get_date_sql, array("integer"), array("apply_id" => $apply_id));
		$time = date('Hi', time()); // 時分は現在時間で代用
		$sql = "update apply set apply_date = '".$date.$time."' where apply_id = $apply_id";
		$log->debug($sql);
		pg_query($con, $sql);
	}

	/**
	 * 申請済チェック
	 */
	static function is_approved($table, $apply_id) {
		$sql = "select count(apply_id) from $table where apply_id = :apply_id and apply_stat = '1'";
		$count = self::get_mdb()->one($sql, array("integer"), array("apply_id" => $apply_id));
		if ($count > 0) {
			return true;
		}
		return false;
	}

	private static function get_mdb() {
		return new MDB(self::get_logger());
	}

	private static function get_logger() {
		return new common_log_class(basename(__FILE__));
	}
	
	private function __construct() {}
}
?>
