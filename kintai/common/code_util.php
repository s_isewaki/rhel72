<?php
/**
 * コードユーティリティ
 * DBにて管理されないコードおよび、その名称等を取り扱う
 */
class CodeUtil {
	
	// 勤務形態
	private static $duty_form_array = array(
		"1" => "常勤",
		"2" => "非常勤",
		"3" => "短時間"
	);

	/**
	 * コード値より勤務形態の名称を取得
	 * @param $code コード
	 * @return 名称
	 */
	static function get_duty_form($code) { return self::$duty_form_array["$code"]; }
    static function get_duty_form_list() { return self::$duty_form_array; }
    
	// 給与支給区分
	private static $salary_type_array = array(
		"1" => "月給制",
		"3" => "日給制",
		"4" => "時間給制",
		"5" => "年俸制"
	);
	
	/**
	 * コード値より給与支給区分の名称を取得
	 * @param $code コード
	 * @return 名称
	 */
	static function get_salary_type($code) { return self::$salary_type_array["$code"]; }

	// 曜日
	private static $day_of_week_array = array(
		"日","月","火","水","木","金","土"
	);
	/**
	 * 曜日の名称を取得
	 * @param $num date('w', time())の結果
	 * @return 名称
	 */
	static function get_day_of_week($num) { return self::$day_of_week_array[$num]; }

	// 申請状態
	private static $apply_status_array = array(
		"0" => "申請中",
		"1" => "承認済み",
		"2" => "否認",
		"3" => "差戻し"
	);
	
	/**
	 * コード値より申請状態の名称を取得
	 * @param $code コード
	 * @return 名称
	 */
	static function get_apply_status($code) { return self::$apply_status_array[$code]; }

	// 休暇種類
	private static $vacation_kind_array = array(
		"1" => array("short" => "有給", "long" => "年次休暇"),
		"2" => array("short" => "週休", "long" => "週休"),
		"3" => array("short" => "夏休", "long" => "夏休"),
		"4" => array("short" => "公休", "long" => "公休"),
		"5" => array("short" => "特別", "long" => "特別休暇"),
		"6" => array("short" => "病欠", "long" => "病気休暇"),
		"99" => array("short" => "その他", "long" => "その他")
	);
	
	/**
	 * コード値より休暇種類の名称を取得
	 * @param $code コード
	 * @param $long コード名称(falseで略称)
	 * @return 名称
	 */
	static function get_vacation_kind($code, $long = false) {
	 if ($long) {
		 return self::$vacation_kind_array[$code]["long"];
	 } else {
		 return self::$vacation_kind_array[$code]["short"];
	 }
	}
	
	// 締め日
	private static $cutoff_date_code_array = array(
		"1" => "1",
		"2" => "5",
		"3" => "15",
		"4" => "20",
		"5" => "25",
		"6" => "E"
	);

	/**
	 * コード値より締め日の日数を取得
	 * @param $code コード
	 * @return 日数 
	 */
	static function get_cutoff_date($code) { return self::$cutoff_date_code_array[$code]; }

    // 手当入力種別
    private static $allowance_input_type = array(
        "1" => "日別",
        "2" => "月別"
        );
    static function get_allowance_input_type($code) { return self::$allowance_input_type[$code]; }
    
	private function __construct() {}
}
?>
