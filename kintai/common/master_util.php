<?php
require_once 'common_log_class.ini';
require_once 'kintai/common/mdb.php';

/** 
 * マスタアクセスユーティリティ
 */
class MasterUtil {

    /**
     * 残業単位(分)を取得
     * @return 残業単位(分)
     */
    static function get_unit_overtime() {
        return self::get_mdb()->one(
            "select fraction_over_unit from timecard",null,null
        );
    }

    /**
     * 締め日情報を取得
     */
    static function get_closing() {
        return self::get_mdb()->first_row(
            "select closing, closing_parttime, closing_month_flg from timecard", null, null
        );
    }

    /**
     * 組織階層
     */
    static function get_organization_level() {
        return self::get_mdb()->first_row(
            "select class_nm, atrb_nm, dept_nm, room_nm, class_cnt from classname", null, null
        );
    }

    /**
     * 組織階層数を取得
     */
    static function get_organization_level_count() {
        $level_info = self::get_organization_level();
        return $level_info["class_cnt"];
    }

    /**
     * 組織を取得
     */
    static function get_organization($level) {
        $sql;
        switch($level) {
        case 1:
            $sql = "select class_id as value, class_nm as label from classmst where class_del_flg = false";
            break;
        case 2:
            $sql = "select atrb_id as value, atrb_nm as label, class_id as parent from atrbmst where atrb_del_flg = false";
            break;
        case 3:
            $sql = "select dept_id as value, dept_nm as label, atrb_id as parent from deptmst where dept_del_flg = false";
            break;
        case 4:
            $sql = "select room_id as value, room_nm as label, dept_id as parent from classroom where room_del_flg = false";
            break;
        default:
            return null;
        }
        return self::get_mdb()->find($sql);
    }

    /**
     * 残業理由を取得
     * @group_id 勤務グループID
     */
    static function get_ovtm_rsn($wkgrp_id) {
        return self::get_mdb()->find(
            "select * from ovtmrsn where del_flg = false and (tmcd_group_id is NULL or tmcd_group_id = :wkgrp_id) order by reason_id",
            array("integer"), array("wkgrp_id" => $wkgrp_id)); 
    }

    /**
     * 勤務グループを取得
     */
    static function get_tmcd_group_list() {
        return self::get_mdb()->find(
            "select * from wktmgrp order by group_name",
            array(), array()
        );
    }

    /**
     * 勤務パターンを取得
     */
    static function get_atdptn_list($tmcd_group_id) {
        return self::get_mdb()->find(
            "select * from atdptn where group_id = :group_id order by atdptn_id",
            array("integer"), array("group_id" => $tmcd_group_id)
        );
    }

    /**
     * 休暇種別等の事由を取得
     */
    static function get_reason_list() {
        return self::get_mdb()->find(
            "select * from atdbk_reason_mst where display_flag = true order by sequence",
            array(),array());
    }

    /**
     * 超過勤務時間区分マスタを取得(計算用)
     */
    static function get_ovtm_div() {
        return self::get_mdb()->find(
            "select * from kintai_ovtmkbn_mst where ovtmkbn_kind not like '%_none' order by order_no asc",
            array(),array());
    }

    /**
     * 超過勤務時間区分マスタを取得(全て)
     */
    static function get_ovtm_div_all() {
        return self::get_mdb()->find(
            "select * from kintai_ovtmkbn_mst order by order_no asc",
            array(),array());
    }

    /**
     * 組織の所定労時時間を取得
     */
    static function get_organization_work_time() {
        return self::get_mdb()->one(
            "select day1_time from calendarname",
            array(), array()
        );
    }

    /**
     * kintai関連機能表示パターン　0:通常　1:特殊A　2:特殊B
     仮です　仮
     */
    static function get_kintai_ptn() {

        return 1;

    }

    /**
     * 遅刻早退の理由を取得
     */
    static function get_iregrsn_list() {
        return self::get_mdb()->find(
            "select * from iregrsn where del_flg = false order by reason_id",
            array(),array());
    }

    private static function get_mdb() {
        $log = new common_log_class(basename(__FILE__));
        return new MDB($log);
    }
}
?>
