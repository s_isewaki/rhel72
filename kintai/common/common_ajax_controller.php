<?php
require_once 'about_comedix.php';
require_once 'common_log_class.ini';

require_once 'kintai/common/ajax_response.php';
require_once 'kintai/common/user_info.php';
require_once 'kintai/common/master_util.php';

/**
 * 職員情報を取得する
 */
function get_user_info() {
	$emp_id = $_POST["emp_id"];
	$user_info = UserInfo::get($emp_id, "u");
	
	// [休暇を受けるもの]表示
	$base_info = $user_info->get_base_info();
	$condition = $user_info->get_emp_condition();
	$class_info = $user_info->get_class_info();
	$attr_info = $user_info->get_attr_info();
	$dept_info = $user_info->get_dept_info();
	$room_info = null;
	$job_info = $user_info->get_job_info();
	$st_info = $user_info->get_st_info();
	$auth_info = $user_info->get_auth_info();

	// 組織階層数を取得
	$level_count = MasterUtil::get_organization_level_count();

	$current_organization = $dept_info->dept_nm;
	if ($level_count == 4) { // 組織階層が4の場合はroomを取得
		$room_info = $user_info->get_class_room_info();
		if (!empty($room_info->room_nm)) {
			$current_organization = $room_info->room_nm;
		}
	}
	
	$res = new AjaxResponse("success");
	$res->data = array(
		"base_info" => $base_info,
		"condition" => $condition,
		"class_info" => $class_info,
		"attr_info" => $attr_info,
		"dept_info" => $dept_info,
		"room_info" => $room_info,
		"job_info" => $job_info,
		"st_info" => $st_info,
		"st_info" => $st_info,
		"auth_info" => $auth_info,
		"organization" => $current_organization
	);

	echo cmx_json_encode($res);
}

/**
 * 出張有無を更新する
 */
function update_trip() {
	$date = $_POST["date"];
	$emp_id = $_POST["emp_id"];
	$flag  = $_POST["flag"];
	$db = new MDB(get_logger());
	
	$delete_sql = "delete from atdbk_trip where date = '$date' and emp_id = '$emp_id'";
	$db->delete($delete_sql);
	if ($flag == "1") {
		$db->insert("atdbk_trip", 
				array("date", "emp_id", "trip_flag"),
				array("text", "text", "text"),
				array(array($date, $emp_id, $flag)));
	}
	$res = new AjaxResponse("success");
	echo cmx_json_encode($res);
}

function get_logger() {
	return new common_log_class(basename(__FILE__));
}
?>
