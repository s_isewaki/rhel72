<?php
require_once 'kintai/common/mdb.php';

class ApplyHolDBAccess {
	
	// 休暇種別から事由を取得
	static function get_atdbk_reason($kind_flg) {
		$sql = 
			"select reason_id, default_name, display_name, holiday_count, time_hol_flag, recital ".
			"from atdbk_reason_mst where ".
			"kind_flag = :kind_flg and display_flag = true order by sequence";
		return self::get_mdb()->find($sql, array("text"), array("kind_flg" => $kind_flg));
	}
	
	// 事由IDをキーに取得
	static function get_reason($reason_id) {
		$sql = 
			"select reason_id, default_name, display_name, holiday_count ".
			"from atdbk_reason_mst where ".
			"reason_id = :reason_id";
		return self::get_mdb()->first_row($sql, array("text"), array("reason_id" => $reason_id));
	}
	
	// 半日申請用の勤務パターン取得
	static function get_pattern($group_id) {
		$sql = 
			"select atdptn_id, atdptn_nm from atdptn where group_id = :group_id and reason in (2,3) ".
			"order by atdptn_order_no";
		return self::get_mdb()->find($sql, array("integer"), array("group_id" => $group_id));
	}

	// 勤務パターン取得(すべて)
	static function get_all_pattern($group_id) {
		$sql = 
			"select atdptn_id, atdptn_nm from atdptn where group_id = :group_id ".
			"order by atdptn_order_no";
		return self::get_mdb()->find($sql, array("integer"), array("group_id" => $group_id));
	}
	
	// 勤務パターンの事由を取得
	static function get_atdptn_reason($group_id, $atdptn) {
		return self::get_mdb()->one("select reason from atdptn where group_id = $group_id and atdptn_id = $atdptn");
	}
	
	// 指定された期間に申請済の休暇が存在するか
	static function hol_exists($emp_id, $start_date, $end_date, $apply_id) {
		
		$sql = 
			"select count(apply_id) from kintai_hol where ".
			" emp_id = :emp_id and ".
			"(".
				"(start_date1 <= '$start_date' and end_date1 >= '$end_date' ) or ".
				"(start_date1 >  '$start_date' and end_date1 <  '$end_date' ) or ".
				"(start_date1 >  '$start_date' and start_date1 <= '$end_date' ) or ".
				"(end_date1 >= '$start_date' and end_date1 <  '$end_date' ) or ".
				"(start_date2 <= '$start_date' and end_date2 >= '$end_date' ) or ".
				"(start_date2 >  '$start_date' and end_date2 <  '$end_date' ) or ".
				"(start_date2 >  '$start_date' and start_date2 <= '$end_date' ) or ".
				"(end_date2 >= '$start_date' and end_date2 <  '$end_date' ) or ".
				"(start_date3 <= '$start_date' and end_date3 >= '$end_date' ) or ".
				"(start_date3 >  '$start_date' and end_date3 <  '$end_date' ) or ".
				"(start_date3 >  '$start_date' and start_date3 <= '$end_date' ) or ".
				"(end_date3 >= '$start_date' and end_date3 <  '$end_date' ) or ".
				"(start_date4 <= '$start_date' and end_date4 >= '$end_date' ) or ".
				"(start_date4 >  '$start_date' and end_date4 <  '$end_date' ) or ".
				"(start_date4 >  '$start_date' and start_date4 <= '$end_date' ) or ".
				"(end_date4 >= '$start_date' and end_date4 <  '$end_date' ) or ".
				"(start_date5 <= '$start_date' and end_date5 >= '$end_date' ) or ".
				"(start_date5 >  '$start_date' and end_date5 <  '$end_date' ) or ".
				"(start_date5 >  '$start_date' and start_date5 <= '$end_date' ) or ".
				"(end_date5 >= '$start_date' and end_date5 <  '$end_date' ) ".
			") and apply_id <> :apply_id";
		$types = array(
			"text","text","text","text","text","integer"
		);
		$values = array(
			"emp_id" => $emp_id,
			"apply_id" => empty($apply_id) ? 0:$apply_id
		);
		
		$count = self::get_mdb()->one($sql, $types, $values);
		
		return (bool) $count;
	}
	
	// 勤務予定、勤務実績の存在チェック
	static function atdbk_exists($emp_id, $date, $table) {
		
		$sql = "select count(emp_id) from $table where ".
						"emp_id = :emp_id and ".
						"date   = :date";
		$types = array(
			"text", "text"
		);
		$values = array(
			"emp_id" => $emp_id,
			"date"   => $date
		);
		
		$count = self::get_mdb()->one($sql, $types, $values);
		
		return (bool) $count;
	}
	
	// 時間有給レコードの存在チェック
	static function time_hol_exists($emp_id, $date) {
		
		$sql = "select count(emp_id) from timecard_paid_hol_hour where ".
						"emp_id = :emp_id and ".
						"start_date   = :date";
		$types = array(
			"text", "text"
		);
		$values = array(
			"emp_id" => $emp_id,
			"date"   => $date
		);
		
		$count = self::get_mdb()->one($sql, $types, $values);
		
		return (bool) $count;
	}
	
	
	// 休暇申請承認時の登録処理
	static function atdbk_hol_approve($emp_id, $hol, $reason, $pattern, $group_id, $time_hol_input, $table) {
		
		// 時間有休じゃなく、勤務パターンがないときは休暇
		if (!$time_hol_input && empty($pattern)) { 
			$pattern = "10";
		}
		
		// レコードの存在チェック
		if (self::atdbk_exists($emp_id, $hol, $table)) {
			self::atdbk_hol_update($emp_id, $hol, $reason, $pattern, $group_id, $time_hol_input, $table);
		} else {
			if ($table == "atdbk"); // 追加できるのは予定
				self::atdbk_hol_insert($emp_id, $hol, $reason, $pattern, $group_id, $time_hol_input, $table);
		}
	}

	// 時間有給の登録処理
	static function atdbk_time_hol_approve($emp_id, $start_date, $start_time, $use_hour, $use_minute) {
		
		// レコードの存在チェック
		if (self::time_hol_exists($emp_id, $start_date)) {
			self::atdbk_time_hol_update($emp_id, $start_date, $start_time, $use_hour, $use_minute);
		} else {
			self::atdbk_time_hol_insert($emp_id, $start_date, $start_time, $use_hour, $use_minute);
		}
	}

	// 勤務実績、予定の登録
	static function atdbk_hol_insert($emp_id, $date, $reason, $pattern, $group_id, $time_hol_input, $table) {
		if (!$time_hol_input) {
			$column = array(
				"emp_id",
				"date",
				"pattern",
				"reason",
				"tmcd_group_id"
			);
			$types = array(
				"text","text","text","text","integer"
			);
			$values = array(array(
				$emp_id, $date, $pattern, $reason, $group_id
			));
			
			self::get_mdb()->insert($table, $column, $types, $values);
		
		} else {
			$column = array(
				"emp_id",
				"date",
				"reason",
				"tmcd_group_id"
			);
			$types = array(
				"text","text","text","integer"
			);
			$values = array(array(
				$emp_id, $date, $reason, $group_id
			));
			
			self::get_mdb()->insert($table, $column, $types, $values);
		}
	}

	// 勤務実績予定の更新
	static function atdbk_hol_update($emp_id, $date, $reason, $pattern, $group_id, $time_hol_input, $table) {
		if (!$time_hol_input) {
			$sql = 
				"update $table set pattern = :pattern, reason = :reason ".
				"where emp_id = :emp_id and date = :date";
			$types = array(
				"text","text","text","text"
			);
			$values = array(
				"pattern" => $pattern,
				"reason" => $reason,
				"emp_id" => $emp_id,
				"date"   => $date
			);
			self::get_mdb()->update($sql, $types, $values);
		} else {
			$sql = 
				"update $table set reason = :reason ".
				"where emp_id = :emp_id and date = :date";
			$types = array(
				"text","text","text"
			);
			$values = array(
				"reason" => $reason,
				"emp_id" => $emp_id,
				"date"   => $date
			);
			self::get_mdb()->update($sql, $types, $values);
		}
	}

	// 時間有休登録
	static function atdbk_time_hol_insert($emp_id, $date, $start_time, $hour, $minute) {
		$column = array(
			"emp_id",
			"start_date",
			"start_time",
			"use_hour",
			"use_minute",
			"calc_minute"
		);
		$types = array(
			"text","text","text","text","text","integer"
		);
		$values = array(array(
			$emp_id, $date, $start_time, intval($hour), $minute, (intval($hour)*60 + $minute)
		));
		
		self::get_mdb()->insert("timecard_paid_hol_hour", $column, $types, $values);
	}

	// 時間有休更新
	static function atdbk_time_hol_update($emp_id, $date, $start_time, $hour, $minute) {
		$sql = 
			"update timecard_paid_hol_hour set start_time = :start_time, use_hour = :hour, use_minute = :minute, calc_minute = :calc_minute ".
			"where emp_id = :emp_id and start_date = :start_date";
		$types = array(
			"text","text","text","integer","text","text"
		);
		$values = array(
			"start_time" => $start_time,
			"hour" => intval($hour),
			"minute" => $minute,
			"calc_minute" => (intval($hour)*60 + $minute),
			"emp_id" => $emp_id,
			"start_date"   => $date
		);
		self::get_mdb()->update($sql, $types, $values);
	}
	
	private static function get_mdb() {
		$log = new common_log_class(basename(__FILE__));
		return new MDB($log);
	}
	
	private function __construct() {}
}
?>
