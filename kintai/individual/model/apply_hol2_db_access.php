<?php
require_once 'kintai/common/mdb.php';
require_once 'kintai/model/atdbkrslt_backup_model.php';

class ApplyHol2DBAccess {
    
	// 事由の夏休を取得
	static function get_atdbk_reason_mst_summer(){
		$sql = 
			"select reason_id, holiday_count, kind_flag ".
			"from atdbk_reason_mst where kind_flag = '3'";
		 $info = self::get_mdb()->find($sql);
		 return $info[0];
	}
	
	// 休暇の夏休を取得
	static function get_kintai_hol_summer($emp_id, $start_date, $end_date){
		//半日対応
        $sql = 
              "select start_date, end_date, case when reason = '31' then 1.0 else 0.5 end as hol_count ".
              " from kintai_hol where ".
              " emp_id = :emp_id and ".
              "(".
                  "(start_date <= :start_date1 and end_date >= :end_date1 ) or ".
                  "(start_date >  :start_date2 and end_date <  :end_date2 ) or ".
                  "(start_date >  :start_date3 and start_date <= :end_date3 ) or ".
                  "(end_date >= :start_date4 and end_date <  :end_date4 ) ".
               ") and kind_flag = '3' and apply_stat in('0','1') "; // 夏休が指定されている

        $types = array(
            "text","text","text","text","text","integer"
        );
        $values = array(
            "emp_id" => $emp_id,
            "start_date1" => $start_date,
            "start_date2" => $start_date,
            "start_date3" => $start_date,
            "start_date4" => $start_date,
            "end_date1" => $end_date,
            "end_date2" => $end_date,
            "end_date3" => $end_date,
            "end_date4" => $end_date,
            "apply_id" => empty($apply_id) ? 0:$apply_id,
        );
        
        return self::get_mdb()->find($sql, $types, $values);
	}
	
	// 夏休予定を取得
	static function get_atdbk_summer($emp_id, $start_date, $end_date){
		//半日対応
		$sql = 
              "select date, case when reason = '31' then 1.0 else 0.5 end as hol_count from atdbk where ".
              " emp_id = :emp_id and ".
              "(".
                  "date >= :start_date and date <= :end_date ".
               ") and reason in (select reason_id from atdbk_reason_mst where kind_flag = '3')"; // 夏休が指定されている

        $types = array(
            "text","text","text"
        );
        $values = array(
            "emp_id" => $emp_id,
            "start_date" => $start_date,
            "end_date" => $end_date
        );
        
        return self::get_mdb()->find($sql, $types, $values);
	}
	
	// 夏休実績を取得
	static function get_atdbkrslt_summer($emp_id, $start_date, $end_date){
		//半日対応
		$sql = 
              "select date, case when reason = '31' then 1.0 else 0.5 end as hol_count from atdbkrslt where ".
              " emp_id = :emp_id and ".
              "(".
                  "date >= :start_date and date <= :end_date ".
               ") and reason in (select reason_id from atdbk_reason_mst where kind_flag = '3')"; // 夏休が指定されている

        $types = array(
            "text","text","text"
        );
        $values = array(
            "emp_id" => $emp_id,
            "start_date" => $start_date,
            "end_date" => $end_date
        );
        
        return self::get_mdb()->find($sql, $types, $values);
	}
	
	
	// 事由の細目を取得
	static function get_detail($reason_id) {
		$sql =
		"select reason_detail_id, reason_detail_name, reason_fulltime_day, reason_parttime_day ".
		"from atdbk_reason_detail_mst where ".
		"reason_id = :reason_id order by reason_detail_id asc";
		return self::get_mdb()->find($sql, array("text"), array("reason_id" => $reason_id));
	}
	
    // 休暇種別から事由を取得
    static function get_atdbk_reason($kind_flg) {
        $sql = 
            "select reason_id, default_name, display_name, holiday_count, time_hol_flag, recital ".
            "from atdbk_reason_mst where ".
            "kind_flag = :kind_flg and display_flag = true order by sequence";
        return self::get_mdb()->find($sql, array("text"), array("kind_flg" => $kind_flg));
    }
    
    // 事由IDをキーに取得
    static function get_reason($reason_id) {
        $sql = 
            "select reason_id, default_name, display_name, holiday_count, half_hol_flag ".
            "from atdbk_reason_mst where ".
            "reason_id = :reason_id";
        return self::get_mdb()->first_row($sql, array("text"), array("reason_id" => $reason_id));
    }
    
    // 半日申請用の勤務パターン取得
    static function get_pattern($group_id) {
        $sql = 
            "select atdptn_id, atdptn_nm from atdptn where group_id = :group_id and workday_count = '0.5' ".
            "order by atdptn_order_no";
        return self::get_mdb()->find($sql, array("integer"), array("group_id" => $group_id));
    }

    // 勤務パターン取得(すべて)
    static function get_all_pattern($group_id) {
        $sql = 
            "select atdptn_id, atdptn_nm from atdptn where group_id = :group_id ".
            "order by atdptn_order_no";
        return self::get_mdb()->find($sql, array("integer"), array("group_id" => $group_id));
    }
    
    // 指定された期間に申請済の休暇が存在するか。
    // ただし、時間指定、1日未満は含まない
    static function hol_exists($emp_id, $start_date, $end_date, $apply_id, $type) {
        
        // 全日の有無を調べる      
        if (!$type) {
            $sql = 
                "select count(apply_id) from kintai_hol where ".
                " emp_id = :emp_id and ".
                "(".
                    "(start_date <= :start_date1 and end_date >= :end_date1 ) or ".
                    "(start_date >  :start_date2 and end_date <  :end_date2 ) or ".
                    "(start_date >  :start_date3 and start_date <= :end_date3 ) or ".
                    "(end_date >= :start_date4 and end_date <  :end_date4 ) ".
                ") and apply_id <> :apply_id and start_time is null and pattern is null"; // 半日でも時間でもない
        }
        
        // 半日の場合
        if ($type == "half") {
            $sql = 
                "select count(apply_id) from kintai_hol where ".
                " emp_id = :emp_id and ".
                "(".
                    "(start_date <= :start_date1 and end_date >= :end_date1 ) or ".
                    "(start_date >  :start_date2 and end_date <  :end_date2 ) or ".
                    "(start_date >  :start_date3 and start_date <= :end_date3 ) or ".
                    "(end_date >= :start_date4 and end_date <  :end_date4 ) ".
                ") and apply_id <> :apply_id and start_time is null and pattern is not null"; // 半日が指定されている
        }
        
        // 時間有給の場合
        if ($type == "time_paid_hol") {
            $sql = 
                "select count(apply_id) from kintai_hol where ".
                " emp_id = :emp_id and ".
                "(".
                    "(start_date <= :start_date1 and end_date >= :end_date1 ) or ".
                    "(start_date >  :start_date2 and end_date <  :end_date2 ) or ".
                    "(start_date >  :start_date3 and start_date <= :end_date3 ) or ".
                    "(end_date >= :start_date4 and end_date <  :end_date4 ) ".
                ") and apply_id <> :apply_id and start_time is not null and kind_flag = '1'"; // 有給かつ時間が指定されている
        }
        
        $types = array(
            "text","text","text","text","text","integer"
        );
        $values = array(
            "emp_id" => $emp_id,
            "start_date1" => $start_date,
            "start_date2" => $start_date,
            "start_date3" => $start_date,
            "start_date4" => $start_date,
            "end_date1" => $end_date,
            "end_date2" => $end_date,
            "end_date3" => $end_date,
            "end_date4" => $end_date,
            "apply_id" => empty($apply_id) ? 0:$apply_id,
        );
        
        $count = self::get_mdb()->one($sql, $types, $values);
        
        return (bool) $count;
    }
    
    // 時間有給レコードの存在チェック
    static function time_hol_exists($emp_id, $date) {
        
        $sql = "select count(emp_id) from timecard_paid_hol_hour where ".
                        "emp_id = :emp_id and ".
                        "start_date   = :date";
        $types = array(
            "text", "text"
        );
        $values = array(
            "emp_id" => $emp_id,
            "date"   => $date
        );
        
        $count = self::get_mdb()->one($sql, $types, $values);
        
        return (bool) $count;
    }
    
    function get_check_type($kind_flag, $start_date, $end_date, $start_hour, $start_minute, $day_count) {
    
        // 期間区分
        $one_day   = $start_date == $end_date     ? true : false;
        $paid_hol  = $kind_flag  == "1"           ? true : false;
        $time_hol  = $start_hour && $start_minute ? true : false;
        
        if ($day_count == 0.5) {
            return "half";
        }
        
        // 単日の申請
        if ($one_day) {
            if ($time_hol) {
                // 全日の申請が既にあればNG 下に続く
                if ($paid_hol) {
                    return "time_paid_hol"; // 時間有給が申請されていればNG
                } else {
                    return "time_nopaid_hol"; // 時間無給は何回でも
                }
            } else {
                return "single";
            }
        } else { // 複数日の申請
            return "multi";
        }
    }

    
    // 勤務予定、勤務実績の存在チェック
    static function atdbk_exists($emp_id, $date, $table) {
        
        $sql = "select count(emp_id) from $table where ".
                        "emp_id = :emp_id and ".
                        "date   = :date";
        $types = array(
            "text", "text"
        );
        $values = array(
            "emp_id" => $emp_id,
            "date"   => $date
        );
        
        $count = self::get_mdb()->one($sql, $types, $values);
        
        return (bool) $count;
    }
    
    // 休暇申請承認時の登録処理
    static function atdbk_hol_approve($emp_id, $hol, $reason, $pattern, $group_id, $time_hol_input, $table, $detail=null) {
        
        // 時間有休じゃなく、勤務パターンがないときは休暇
        if (!$time_hol_input && empty($pattern)) { 
            $pattern = "10";
        }
        
        // レコードの存在チェック
        if (self::atdbk_exists($emp_id, $hol, $table)) {
            if($table == "atdbkrslt") {
                //半日有休が設定されていた場合、有休に更新しない 20150205
                $update_flg = true;
                if ($time_hol_input && ($reason == "1" || $reason == "37")) {
                    $arr_atdbkrslt = AtdbkRsltBackupModel::get_atdbkrslt($emp_id, $hol);
                    $wk_db_reason = $arr_atdbkrslt["reason"];
                    if ($wk_db_reason == "2" || $wk_db_reason == "3" || $wk_db_reason == "38" || $wk_db_reason == "39") {
                        $update_flg = false;
                    }
                }
                if ($update_flg) {
                    self::atdbk_hol_update($emp_id, $hol, $reason, $pattern, $group_id, $time_hol_input, $table, $detail);
                }
        	}else{
            	self::atdbk_hol_update($emp_id, $hol, $reason, $pattern, $group_id, $time_hol_input, $table);
        	}
        } else {
//             if ($table == "atdbk"); // 追加できるのは予定
        	if($table == "atdbkrslt") {
                self::atdbk_hol_insert($emp_id, $hol, $reason, $pattern, $group_id, $time_hol_input, $table, $detail);
        	} else{
                self::atdbk_hol_insert($emp_id, $hol, $reason, $pattern, $group_id, $time_hol_input, $table);
        	}
        }
    }

    // 時間有給の登録処理
    static function atdbk_time_hol_approve($emp_id, $start_date, $start_time, $use_hour, $use_minute) {
        
        // レコードの存在チェック
        if (self::time_hol_exists($emp_id, $start_date)) {
            self::atdbk_time_hol_update($emp_id, $start_date, $start_time, $use_hour, $use_minute);
        } else {
            self::atdbk_time_hol_insert($emp_id, $start_date, $start_time, $use_hour, $use_minute);
        }
    }

    // 勤務実績、予定の登録
    static function atdbk_hol_insert($emp_id, $date, $reason, $pattern, $group_id, $time_hol_input, $table, $detail=null) {
    	if ($table == "atdbkrslt" && !empty($detail)){
	        if (!$time_hol_input) {
	            $column = array(
	                "emp_id",
	                "date",
	                "pattern",
	                "reason",
	                "tmcd_group_id",
	                "detail"
	            );
	            $types = array(
	                "text","text","text","text","integer","integer"
	            );
	            $values = array(array(
	                $emp_id, $date, $pattern, $reason, $group_id, $detail
	            ));
	            
	            self::get_mdb()->insert($table, $column, $types, $values);
	        
	        } else {
	            $column = array(
	                "emp_id",
	                "date",
	                "reason",
	                "tmcd_group_id",
            		"detail"
	            );
	            $types = array(
	                "text","text","text","integer","integer"
	            );
	            $values = array(array(
	                $emp_id, $date, $reason, $group_id,$detail
	            ));
	            
	            self::get_mdb()->insert($table, $column, $types, $values);
	        }
    	}else{
	        if (!$time_hol_input) {
	            $column = array(
	                "emp_id",
	                "date",
	                "pattern",
	                "reason",
	                "tmcd_group_id"
	            );
	            $types = array(
	                "text","text","text","text","integer"
	            );
	            $values = array(array(
	                $emp_id, $date, $pattern, $reason, $group_id
	            ));
	            
	            self::get_mdb()->insert($table, $column, $types, $values);
	        
	        } else {
	            $column = array(
	                "emp_id",
	                "date",
	                "reason",
	                "tmcd_group_id"
	            );
	            $types = array(
	                "text","text","text","integer"
	            );
	            $values = array(array(
	                $emp_id, $date, $reason, $group_id
	            ));
	            
	            self::get_mdb()->insert($table, $column, $types, $values);
	        }
    	}
    }

    // 勤務実績予定の更新
    static function atdbk_hol_update($emp_id, $date, $reason, $pattern, $group_id, $time_hol_input, $table, $detail=null) {
    	
    	if ($table == "atdbkrslt" && !empty($detail)){
    		if (!$time_hol_input) {
	            $sql = 
	                "update $table set pattern = :pattern, reason = :reason, detail =:detail ".
	                "where emp_id = :emp_id and date = :date";
	            $types = array(
	                "text","text","integer","text","text"
	            );
	            $values = array(
	                "pattern" => $pattern,
	                "reason" => $reason,
	                "detail" => $detail,
	                "emp_id" => $emp_id,
	                "date"   => $date
	            );
	            self::get_mdb()->update($sql, $types, $values);
	        } else {
	            $sql = 
	                "update $table set reason = :reason, detail =:detail ".
	                "where emp_id = :emp_id and date = :date";
	            $types = array(
	                "text","integer","text","text"
	            );
	            $values = array(
	                "reason" => $reason,
	                "detail" => $detail,
	                "emp_id" => $emp_id,
	                "date"   => $date
	            );
	            self::get_mdb()->update($sql, $types, $values);
	        }
    	}else{
	        if (!$time_hol_input) {
	            $sql = 
	                "update $table set pattern = :pattern, reason = :reason ".
	                "where emp_id = :emp_id and date = :date";
	            $types = array(
	                "text","text","text","text"
	            );
	            $values = array(
	                "pattern" => $pattern,
	                "reason" => $reason,
	                "emp_id" => $emp_id,
	                "date"   => $date
	            );
	            self::get_mdb()->update($sql, $types, $values);
	        } else {
	            $sql = 
	                "update $table set reason = :reason ".
	                "where emp_id = :emp_id and date = :date";
	            $types = array(
	                "text","text","text"
	            );
	            $values = array(
	                "reason" => $reason,
	                "emp_id" => $emp_id,
	                "date"   => $date
	            );
	            self::get_mdb()->update($sql, $types, $values);
	        }
    	}
    }

    // 時間有休登録
    static function atdbk_time_hol_insert($emp_id, $date, $start_time, $hour, $minute) {
        $column = array(
            "emp_id",
            "start_date",
            "start_time",
            "use_hour",
            "use_minute",
            "calc_minute"
        );
        $types = array(
            "text","text","text","text","text","integer"
        );
        $values = array(array(
            $emp_id, $date, $start_time, intval($hour), $minute, (intval($hour)*60 + $minute)
        ));
        
        self::get_mdb()->insert("timecard_paid_hol_hour", $column, $types, $values);
    }

    // 時間有休更新
    static function atdbk_time_hol_update($emp_id, $date, $start_time, $hour, $minute) {
        $sql = 
            "update timecard_paid_hol_hour set start_time = :start_time, use_hour = :hour, use_minute = :minute, calc_minute = :calc_minute ".
            "where emp_id = :emp_id and start_date = :start_date";
        $types = array(
            "text","text","text","integer","text","text"
        );
        $values = array(
            "start_time" => $start_time,
            "hour" => intval($hour),
            "minute" => $minute,
            "calc_minute" => (intval($hour)*60 + $minute),
            "emp_id" => $emp_id,
            "start_date"   => $date
        );
        self::get_mdb()->update($sql, $types, $values);
    }
    
    // 時間有休削除 2013/09/28
    static function atdbk_time_hol_delete($emp_id, $date) {
        $sql = 
            "delete from timecard_paid_hol_hour".
            " where emp_id = :emp_id and start_date = :start_date";
        $types = array(
            "text","text"
        );
        $values = array(
            "emp_id" => $emp_id,
            "start_date" => $date
        );
        self::get_mdb()->delete($sql, $types, $values);
    }
    
    // 時間有休取り消し
    static function cancel_time_hol_hour($emp_id, $date) {
        // 取り消し対象の事由
        $cancel_reason = array("1", "2", "3", "37", "38", "39");
        $str_reason = implode("','", $cancel_reason);
        
        $cancel_sql = "set reason = '' where emp_id = :emp_id and date = :date and ".
            "reason in ('$str_reason')";
        $types = array("text", "text");
        $values = array("emp_id" => $emp_id, "date" => $date);
        self::get_mdb()->update("update atdbk ".$cancel_sql, $types, $values);
        self::get_mdb()->update("update atdbkrslt ".$cancel_sql, $types, $values);
    }
    
    private static function get_mdb() {
        $log = new common_log_class(basename(__FILE__));
        return new MDB($log);
    }
    
    private function __construct() {}
}
?>
