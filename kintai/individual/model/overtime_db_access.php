<?php
require_once 'kintai/common/mdb.php';
require_once 'kintai/common/date_util.php';

class OvertimeDBAccess {
	
	/**
	 * 勤務実績を取得(テンプレート命令時間上部)
	 * 画面初期表示およびAjaxで取得する際に使用
	 */
	static function get_atd_result($date, $emp_id, $tmcd_group_id = null, $atdptn_id = null) {
		
		$atdbkrslt;
		
		if (empty($tmcd_group_id) || empty($atdptn_id)) {
			$types = array("text", "text");
			$values = array("date" => $date, "emp_id" => $emp_id);
			
			$atdbkrslt_table = "select emp_id, date as date_key, pattern as ptn_key, reason as reason_key, tmcd_group_id as group_key from atdbkrslt ";
			$atdbk_table = "select emp_id, date as date_key, pattern as ptn_key, reason as reason_key, tmcd_group_id as group_key from atdbkrslt ";
			
			$columns = 
					"group_key as tmcd_group_id, ".
					"ptn_key as pattern, ".
					"atdptn_nm, ".
					"officehours2_start, ".
					"officehours2_end, ".
					"officehours4_start, ".
					"officehours4_end, ".
					"empcond_officehours_flag, ".
					"reason_key as reason_id,".
					"meeting_start_time, ".
					"meeting_end_time, ".
					"display_name, ".
					"default_name ";
			$atdbkrslt_sql = "select ".
				$columns.
				"from ".
				"( ".
					"( ".
						"( ".
							"select emp_id, date as date_key, pattern as ptn_key, reason as reason_key, tmcd_group_id as group_key from atdbkrslt ".
						") rslt left outer join ( ".
							"select atdptn_id, group_id, empcond_officehours_flag, atdptn_nm, meeting_start_time, meeting_end_time from atdptn) ptn on rslt.ptn_key = CAST(ptn.atdptn_id AS varchar) and rslt.group_key = ptn.group_id ".
					") a left outer join atdbk_reason_mst mst on a.reason_key = mst.reason_id ".
				") b left outer join  ".
					"( ".
						"select tmcd_group_id, pattern, officehours_id, officehours4_start, officehours4_end, officehours2_start, officehours2_end from officehours ".
					")  hours on b.ptn_key = hours.pattern and b.group_key = hours.tmcd_group_id and hours.officehours_id = '1' ".
					"where emp_id = :emp_id and date_key = :date";
			$atdbk_sql = "select ".
				$columns.
				"from ".
				"( ".
					"( ".
						"( ".
							"select emp_id, date as date_key, pattern as ptn_key, reason as reason_key, tmcd_group_id as group_key from atdbk ".
						") rslt left outer join ( ".
							"select atdptn_id, group_id, empcond_officehours_flag, atdptn_nm, meeting_start_time, meeting_end_time from atdptn) ptn on rslt.ptn_key = CAST(ptn.atdptn_id AS varchar) and rslt.group_key = ptn.group_id ".
					") a left outer join atdbk_reason_mst mst on a.reason_key = mst.reason_id ".
				") b left outer join  ".
					"( ".
						"select tmcd_group_id, pattern, officehours_id, officehours4_start, officehours4_end, officehours2_start, officehours2_end from officehours ".
					")  hours on b.ptn_key = hours.pattern and b.group_key = hours.tmcd_group_id and hours.officehours_id = '1' ".
					"where emp_id = :emp_id and date_key = :date";
					
			$atdrslt = self::get_mdb()->first_row($atdbkrslt_sql, $types, $values);
			if (empty($atdrslt) || empty($atdrslt["pattern"])) {
				$atdrslt = self::get_mdb()->first_row($atdbk_sql, $types, $values);
                if (empty($atdrslt) || empty($atdrslt["pattern"])) {
                    return null; 
                }
			}

		} else {
			$types = array("integer", "integer");
			$values = array("group_id" => $tmcd_group_id, "atdptn_id" => $atdptn_id);
			
			$sql = 
				"select ".
					"tmcd_group_id, ".
					"atdptn_id as pattern, ".
					"atdptn_nm, ".
					"officehours2_start, ".
					"officehours2_end, ".
					"officehours4_start, ".
					"officehours4_end, ".
					"empcond_officehours_flag, ".
					"reason as reason_id, ".
					"meeting_start_time, ".
					"meeting_end_time, ".
					"display_name, ".
					"default_name ".
				"from ".
					"atdptn p left outer join officehours o on p.atdptn_id = o.pattern and p.group_id = o.tmcd_group_id and officehours_id = '1' ".
					"left outer join atdbk_reason_mst m on p.reason = m.reason_id ".
				"where  ".
					"p.atdptn_id = :atdptn_id and ".
					"p.group_id = :group_id";
					
			$atdrslt = self::get_mdb()->first_row($sql, $types, $values);
		}

		if ($atdrslt["empcond_officehours_flag"] != "1") {
			return $atdrslt;
		}
		
		// 個人の勤務条件を優先する場合
		// 現在の曜日を取得
		$sp_ymd = DateUtil::split_ymd($date);
		$day_of_week = DateUtil::day_of_week(date('Y-m-d', mktime(0,0,0,$sp_ymd['m'], $sp_ymd['d'], $sp_ymd['y'])), false);
		// 祝日を取得
		$holiday = self::get_mdb()->one(
			"select type from calendar where date = :date",
			array("text"), array("date" => $date));

		// 曜日の所定労働時間を取得
		$day_of_week_time_sql = 
			"select officehours2_start, officehours2_end, officehours4_start, officehours4_end from empcond_officehours where ".
			"emp_id = :emp_id and weekday = :day_of_week";

		$types = array("text", "integer");
		$values["emp_id"] = $emp_id;
		if ($holiday == "6") { // 祝日の場合
			$values["day_of_week"] = "7";
			
			$rslt = self::get_mdb()->first_row($day_of_week_time_sql, $types, $values);

			if (!empty($rslt)) {
				return self::override_office_hour($atdrslt, $rslt);
			}
		}

		// 曜日で取得
		$values["day_of_week"] = $day_of_week;
		$rslt = self::get_mdb()->first_row($day_of_week_time_sql, $types, $values);

		if (!empty($rslt)) {
			return self::override_office_hour($atdrslt, $rslt);
		}

		// 基本で取得
		$values["day_of_week"] = "8";
		$rslt = self::get_mdb()->first_row($day_of_week_time_sql, $types, $values);
		if (!empty($rslt)) {
			return self::override_office_hour($atdrslt, $rslt);
		}

		// 基本もなければ変わらない
		return $atdrslt;
	}

	private static function override_office_hour($atd_rslt, $override_hours) {
		$atd_rslt["officehours2_start"] = $override_hours["officehours2_start"];
		$atd_rslt["officehours4_start"] = $override_hours["officehours4_start"];
		$atd_rslt["officehours2_end"] = $override_hours["officehours2_end"];
		$atd_rslt["officehours4_end"] = $override_hours["officehours4_end"];
		return $atd_rslt;
	}
	
	/**
	 * 所定労働時間を取得(秒)
	 */
	static function get_fixed_work_time($date_map, $emp_id, $tmcd_group_id = null, $atdptn_id = null) {

		$year = $date_map["year"]; 
		$month = $date_map["month"];
		$day   = $date_map["day"];

		// 日付
		$rslt_date = DateUtil::to_db_ymd($year, $month, $day);

		$rslt = self::get_atd_result($rslt_date, $emp_id, $tmcd_group_id, $atdptn_id);

		$start = $rslt["officehours2_start"];
		$end   = $rslt["officehours2_end"];

		$start_hi = DateUtil::split_hi($rslt["officehours2_start"]);
		$end_hi   = DateUtil::split_hi($rslt["officehours2_end"]);

		$start_time = mktime($start_hi["h"], $start_hi["i"], 0, $month, $day, $year);
		
		// start > end の場合、endは翌日とする
		$end_time;
		if (intval(DateUtil::to_db_hi($start_hi["h"], $start_hi["i"])) > intval(DateUtil::to_db_hi($end_hi["h"], $end_hi["i"]))) {
			$end_time   = mktime($end_hi["h"], $end_hi["i"], 0, $month, $day+1, $year);
		} else {
			$end_time   = mktime($end_hi["h"], $end_hi["i"], 0, $month, $day, $year);
		}

		$start4 = $rslt["officehours4_start"];
		$end4   = $rslt["officehours4_end"];
		$start_rest_time = 0;
		$end_rest_time = 0;

		if (!empty($start4) && !empty($end4)) {
			$start_hi4 = DateUtil::split_hi($rslt["officehours4_start"]);
			$end_hi4   = DateUtil::split_hi($rslt["officehours4_end"]);
			$start_rest_time = mktime($start_hi4["h"], $start_hi4["i"], 0, $month, $day, $year);
			
			// start > end の場合、endは翌日とする
			if (intval(DateUtil::to_db_hi($start_hi4["h"], $start_hi4["i"])) > intval(DateUtil::to_db_hi($end_hi4["h"], $end_hi4["i"]))) {
				$end_rest_time   = mktime($end_hi4["h"], $end_hi4["i"], 0, $month, $day+1, $year);
			} else {
				$end_rest_time   = mktime($end_hi4["h"], $end_hi4["i"], 0, $month, $day, $year);
			}
		}

		return array("atdptn" => $rslt["pattern"], "start_time" => $start_time, "end_time" => $end_time,
									"start_rest_time" => $start_rest_time, "end_rest_time" => $end_rest_time);
	}
	
	static function is_draft($apply_id) {
		if (empty($apply_id)) {
			return "false";
		}

		$result = self::get_mdb()->one(
			"select draft_flg from apply where apply_id = $apply_id", array(), array()
		);

		if ($result == "t") {
			return "true";
		} else {
			return "false";
		}
	}
	/**
	 * 超過勤務時間を取得
	 * 超過勤務区分表内の実施日の値
	 */
	static function get_ovtm_minute($date, $emp_id) {
	
		$sql = 
			"select * ".
			"from kintai_ovtmkbn_mst mst left outer join  ".
			"( ".
				"select  ".
					"emp_id,  ".
					"date,  ".
					"ovtmkbn_id as fkey,  ".
					"minute  ".
				"from kintai_ovtm_sum  ".
				"where  ".
					"emp_id = :emp_id and  ".
					"date = :date ".
			") sum  on mst.ovtmkbn_id = sum.fkey ".
			"where ovtmkbn_kind not like '%_none' order by mst.order_no asc";

		$types = array("text", "text");
		$values = array("date" => $date, "emp_id" => $emp_id);
		
		return self::get_mdb()->find($sql, $types, $values);
	}
	
	/**
	 * 超過勤務時間を集計
	 */
	static function get_ovtm_sum($from, $to, $div, $emp_id, $date = null) {
		
		$sql = 
			"select sum(minute) from kintai_ovtm_sum where ".
			"emp_id = :emp_id and ".
			"ovtmkbn_id = :ovtmkbn_id and ".
			"date >= :from and date <= :to";
		$types = array("text","integer","text","text");
		$values = array(
			"emp_id" => $emp_id,
			"ovtmkbn_id" => $div,
			"from" => $from,
			"to" => $to
		);

		if (!empty($date)) {
			$sql .= " and date <> :date";
			array_push($types, "text");
			$values["date"] = $date;
		}
		return self::get_mdb()->one($sql, $types, $values);
	}
	
	/**
	 * ステータスを取得
	 */
	static function get_status($emp_id, $date) {
		$sql = 
			"select apply_stat from kintai_ovtm where ".
				"emp_id = :emp_id and date = :date";
		return self::get_mdb()->one($sql, 
			array("text", "text"),
			array("emp_id" => $emp_id, "date" => $date));
	}

	private static function get_mdb() {
		$log = new common_log_class(basename(__FILE__));
		return new MDB($log);
	}
	
	/**
	 * 時間有休を取得
	 */
	static function get_hol_hour($emp_id, $start_date) {
		return self::get_mdb()->first_row(
			"select * from timecard_paid_hol_hour where emp_id = :emp_id and start_date = :start_date",
			array("text", "text"),
			array("emp_id" => $emp_id, "start_date" => $start_date)
		);
	}
	
	/**
	 * 勤務実績の更新
	 */
	static function update_atdbkrslt($emp_id, $date, $pattern, $reason, $tmcd_group_id, $meeting_start_time, $meeting_end_time) {
		return self::get_mdb()->update(
			"update atdbkrslt ".
				"set pattern = :pattern, ".
				"reason = :reason, ".
				"tmcd_group_id = :tmcd_group_id, ".
				"meeting_start_time = :meeting_start_time, ".
				"meeting_end_time = :meeting_end_time ".
				"where emp_id = :emp_id and date = :date",
			array(
				"text", "text", "integer", "text", "text", "text", "text"
			),
			array(
				"pattern" => $pattern,
				"reason"  => $reason,
				"tmcd_group_id" => $tmcd_group_id,
				"meeting_start_time" => $meeting_start_time,
				"meeting_end_time"   => $meeting_end_time,
				"emp_id" => $emp_id,
				"date"   => $date
			)
		);
	}
	
	private function __construct() {}
}
?>
