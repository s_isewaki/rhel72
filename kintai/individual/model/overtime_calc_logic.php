<?php
require_once 'common_log_class.ini';
require_once 'kintai/common/master_util.php';
require_once 'kintai/common/date_util.php';
require_once 'kintai/common/user_info.php';

require_once 'kintai/individual/model/overtime_calc_util.php';
require_once 'kintai/individual/model/overtime_kbn.php';

require_once 'kintai/model/overtime_seq_model.php';

class OvertimeCalcLogic {

    // 計算する行を取得する
    static function get_calc_line($date_map, $rslt_array) {

        $calc_line = array();
        // 計算行を取得
        array_push($calc_line, self::edit_calc_line($date_map, $rslt_array[0]));
        array_push($calc_line, self::edit_calc_line($date_map, $rslt_array[1]));
        array_push($calc_line, self::edit_calc_line($date_map, $rslt_array[2]));

        foreach ($calc_line as $key => $row) {
            $sort[$key] = $row["start_time"];
        }
        // start_timeでsort
        array_multisort($sort, SORT_ASC, $calc_line);
        return $calc_line;
    }

    // 計算行を編集して取得する
    private static function edit_calc_line($date_map, $rslt) {

        $line = array();

        $start_hour = $rslt["start_hour"];
        $start_minute = $rslt["start_minute"];
        $start_next_flag = $rslt["start_next_flag"];
        $end_hour = $rslt["end_hour"];
        $end_minute = $rslt["end_minute"];
        $end_next_flag = $rslt["end_next_flag"];

        $start_time;
        if ("true" == $start_next_flag) {
            $start_time = mktime($start_hour, $start_minute, 0, $date_map["month"], $date_map["day"]+1, $date_map["year"]);
            $start_next = true;
        } else {
            $start_time = mktime($start_hour, $start_minute, 0, $date_map["month"], $date_map["day"], $date_map["year"]);
        }

        $end_time;
        if ("true" == $end_next_flag) {
            $end_time = mktime($end_hour, $end_minute, 0, $date_map["month"], $date_map["day"]+1, $date_map["year"]);
            $end_next = true;
        } else {
            $end_time = mktime($end_hour, $end_minute, 0, $date_map["month"], $date_map["day"], $date_map["year"]);
        }

        $line["start_time"] = $start_time; // 開始時間
        $line["end_time"]   = $end_time; // 終了時間

        if ($start_hour == "") { // 従事がなかったら計算しない
            return array("cancel" => true);
        }

        $line["reason"] = $rslt["ovtmrsn_id"];
        $line["reason"] = $rslt["call_flag"];
        $rest_hour = $rslt["rest_hour"];
        $rest_minute = $rslt["rest_minute"];

        // 休憩時間は秒にして持っておく
        $line["rest_time"] = 
            (intval($rest_hour) * 60 * 60) + (intval($rest_minute) * 60);

        return $line;
    }

    // 所定労働時間のチェック
    static function fixed_time_check($lines, $fixed_work_time) {

        $warning = false;
        $fixed_start_time = $fixed_work_time["start_time"];
        $fixed_end_time   = $fixed_work_time["end_time"];
        foreach ($lines as $line) {
            if ($line["cancel"]) {
                continue;
            }

            $start_time = $line["start_time"];
            $end_time = $line["end_time"];

            // 所定労働時間の範囲にあったら警告を出す
            if (
                ($end_time > $fixed_start_time && $end_time <= $fixed_end_time) ||
                ($start_time >= $fixed_start_time && $start_time < $fixed_end_time)
            ) {
                $warning = true;
                break;
            }
        }
        return $warning;
    }

    // 休憩時間のチェック
    static function rest_time_error($is_shorttime_emp, $calc_line, $emp_id, $fixed_work_time) {
        
        // 組織の所定労働時間を取得(分)
        $org_work_time = DateUtil::convert_min(MasterUtil::get_organization_work_time()) * 60; // 0745
        // 閾値
        $work_time_0600 = 6 * 60 * 60; // 6時間

        // 残業時間(入力)
        $ovtm_ttl_seconds = 0;
        // 休憩時間(入力)
        $rest_ttl_seconds = 0;
        // 合計を取得
        foreach($calc_line as $line) {
            if ($line["cancel"] || $line["call_flag"] == "true") { // 取消と呼出は時間を考慮しない
                continue;
            }
            
            $ovtm_ttl_seconds += ($line["end_time"] - $line["start_time"]); 
            $rest_ttl_seconds += $line["rest_time"];
        }

        if ($ovtm_ttl_seconds == 0) {
            return false; // 残業してなかったらチェックしない(念の為)
        }

        $emp_fixed_time = 0; // 所定労働時間 
        $emp_rest_time = 0; // 所定休憩
        if ($fixed_work_time["atdptn"] != 10) { // 
            if ($is_shorttime_employee) {
                // 非常勤の人の所定労働時間を取得
                $user_info = UserInfo::get($emp_id, "u");
                $emp_fixed_time = DateUtil::convert_min($user_info->get_emp_condition()->specified_time) * 60;
            } else {
                // 所定労働時間帯(From-to) - 休憩時間(From-To)
                $emp_fixed_time = $fixed_work_time["end_time"] - $fixed_work_time["start_time"] - 
                    ($fixed_work_time["end_rest_time"] - $fixed_work_time["start_rest_time"]);  
            }
            // 非常勤の人も所属するシフトの休憩時間に従う 
            $emp_rest_time = $fixed_work_time["end_rest_time"] - $fixed_work_time["start_rest_time"]; 
        }
        // 休暇(パターン10)の場合の出勤は所定労働時間を0として以後計算する
        // 所定時間と取得時間を加算
        $ttl_work_time = $emp_fixed_time + $ovtm_ttl_seconds;
        $ttl_rest_time = $emp_rest_time + $rest_ttl_seconds;
        // ６時間以上、７時間45分未満
        if ($work_time_0600 <= $ttl_work_time && $org_work_time > $ttl_work_time ) {
            // 45分の休憩が必要
            if ($ttl_rest_time < 60 * 45) {
                return true;
            }            
        // 7時間45分以上 
        } if ($org_work_time < $ttl_work_time) {
            // 1時間分以上の休憩が必要
            if ($ttl_rest_time < 60 * 60) {
                return true;
            }
        }
        // 休憩時間は不要
        return false;
    }
    // 超過区分を計算する
    static function calc($lines, $date_map, $emp_id, $tmcd_group_id = null, $atdptn_id = null, $reason_id = null) {
        // 短時間勤務職員の場合
        /*
        if (OvertimeCalcUtil::is_shorttime_employee($emp_id)) {
            return self::short_time_calc($lines, $date_map, $emp_id, $tmcd_group_id, $atdptn_id);
        } else {
            return self::time_calc($lines, $date_map, $emp_id, $tmcd_group_id, $atdptn_id, $reason_id);
        }
        */
        // 常勤、非常勤処理統合
        $is_shorttime_emp = OvertimeCalcUtil::is_shorttime_employee($emp_id);
        return self::time_calc($lines, $date_map, $emp_id, $tmcd_group_id, $atdptn_id, $reason_id, $is_shorttime_emp);
    }

    // 通常職員の計算
//    static function time_calc($lines, $date_map, $emp_id, $tmcd_group_id = null, $atdptn_id = null, $reason_id = null) {
    static function time_calc($lines, $date_map, $emp_id, $tmcd_group_id = null, $atdptn_id = null, $reason_id = null, $is_shorttime_emp) {
        $log = self::get_logger();
        $log->debug("----- Calc Start -----");

        $year = $date_map["year"];
        $month = $date_map["month"];
        $day  = $date_map["day"];

        // 実施日とその翌日
        $rslt_date = date('Ymd', mktime(0,0,0, $month, $day, $year));
        $tommorow  = date('Ymd', mktime(0,0,0, $month, $day+1, $year));
        // 実施日、翌日の休日有無
        $current_holiday = OvertimeCalcUtil::is_holiday($rslt_date, $emp_id, $atdptn_id, $reason_id);
        $tommorow_holiday = OvertimeCalcUtil::is_holiday($tommorow, $emp_id);

        // 深夜帯
        $late_time = OvertimeCalcUtil::get_overtime_late($date_map);
        $start_late = $late_time["start_time"];
        $end_late   = $late_time["end_time"];
        
        // 前日の深夜帯
        $prev_late_time = OvertimeCalcUtil::get_overtime_late(
            array(
                "year" => $date_map["year"], 
                "month" => $date_map["month"],
                "day" => intval($date_map["day"]) - 1
            )
        );
        $prev_start_late = $prev_late_time["start_time"];
        $prev_end_late   = $prev_late_time["end_time"];
        
        // Add Start 非常勤用 
        $org_work_time = intval(DateUtil::convert_min(MasterUtil::get_organization_work_time())) * 60; // 組織の所定労働秒
        $user_info = UserInfo::get($emp_id, "u");
        $emp_work_time = intval(DateUtil::convert_min($user_info->get_emp_condition()->specified_time)) * 60; // 個人の所定労働病
        // Add End 

        // 所定労働時間
        $fixed_work_time = OvertimeDBAccess::get_fixed_work_time($date_map, $emp_id, $tmcd_group_id, $atdptn_id);
        $fixed_start_work_time = $fixed_work_time["start_time"];
        $fixed_end_work_time   = $fixed_work_time["end_time"];

        // 0時切り替え
        $date_line = mktime(0,0,0,$month, $day+1, $year);
        
        $ttl_ovtm_100     = 0;
        $ttl_ovtm_125     = 0;
        $ttl_ovtm_135     = 0;
        $ttl_ovtm_135_hol = 0;
        $ttl_ovtm_150     = 0;
        $ttl_ovtm_160     = 0;

        $ovtm_seq = 0;
        $ovtm_seq_list = array();
        foreach ($lines as $line) {
            if ($line["cancel"]) continue;

            $work_100 = 0;
            $work_125 = 0;
            $work_135 = 0;
            $work_135_hol = 0;
            $work_150 = 0;
            $work_160 = 0;
            $prev_work_150 = 0;
            $prev_work_160 = 0;
            $next_work_125 = 0;
            $next_work_135 = 0;
            $next_work_135_hol = 0;
            $next_work_150 = 0;
            $next_work_160 = 0;

            $reason    = $line["reason"];
            $rest_time = $line["rest_time"];
            $start_time = $line["start_time"];
            $end_time = $line["end_time"];

            // endが実施日にかかる 
            if ($end_time <= $date_line) {
                switch ($current_holiday) {
                case "0": // 時間外を125, 深夜を150
                    $work_125 = OvertimeCalcUtil::outer_calc($start_time, $end_time, $start_late, $end_late);
                    $work_150 = OvertimeCalcUtil::inner_calc($start_time, $end_time, $start_late, $end_late);
                    
                    // 前日分の深夜も考慮
                    $prev_work_150 = OvertimeCalcUtil::inner_calc($start_time, $end_time, $prev_start_late, $prev_end_late);
                    //$work_150 += $prev_work_150;
                    $work_125 -= $prev_work_150;
/*					
                    // 深夜帯かつ所定労働時間内に申請された残業時間の計算
                    // 当日深夜
                    $rest_work = self::get_rest_work($start_time, $end_time, $start_late, $end_late, $fixed_start_work_time, $fixed_end_work_time);
                    $log->debug("rest_work:".$rest_work);
                    $work_150 -= $rest_work;
                    $work_125 += $rest_work;

                    // 前日深夜
                    $prev_rest_work = self::get_rest_work($start_time, $end_time, $prev_start_late, $prev_end_late, $fixed_start_work_time, $fixed_end_work_time);
                    $log->debug("rest_work_prev:".$prev_rest_work);
                    $work_150 -= $prev_rest_work;
                    $work_125 += $prev_rest_work;
*/					
                    break;
                case "1": // 時間内を135hol, 時間外を135, 深夜を160
                    $work_135_hol = OvertimeCalcUtil::inner_calc($start_time, $end_time, $fixed_start_work_time, $fixed_end_work_time);
                    $work_135     = OvertimeCalcUtil::outer_calc($start_time, $end_time, $start_late, $end_late) - $work_135_hol;
                    $work_160     = OvertimeCalcUtil::inner_calc($start_time, $end_time, $start_late, $end_late);

                    $prev_work_160 = OvertimeCalcUtil::inner_calc($start_time, $end_time, $prev_start_late, $prev_end_late);
                    //$work_160 += $prev_work_160;
                    if ($work_135_hol < $prev_work_160) {
                        $work_135 -= ($prev_work_160 - $work_135_hol);
                        $work_135_hol = 0;
                    } else {
                        $work_135_hol -= $prev_work_160;
                    }
/*
                    // 深夜帯かつ所定労働時間内に申請された残業時間の計算
                    // 当日深夜
                    $rest_work = self::get_rest_work($start_time, $end_time, $start_late, $end_late, $fixed_start_work_time, $fixed_end_work_time);
                    $log->debug("rest_work:".$rest_work);
                    $work_150 -= $rest_work;
                    $work_125 += $rest_work;

                    // 前日深夜
                    $prev_rest_work = self::get_rest_work($start_time, $end_time, $prev_start_late, $prev_end_late, $fixed_start_work_time, $fixed_end_work_time);
                    $log->debug("rest_work_prev:".$prev_rest_work);
                    $work_150 -= $prev_rest_work;
                    $work_125 += $prev_rest_work;
*/
                    break;
                case "2": // 時間外を135, 深夜を160
                    $work_135     = OvertimeCalcUtil::outer_calc($start_time, $end_time, $start_late, $end_late);
                    $work_160     = OvertimeCalcUtil::inner_calc($start_time, $end_time, $start_late, $end_late);

                    $prev_work_160 = OvertimeCalcUtil::inner_calc($start_time, $end_time, $prev_start_late, $prev_end_late);
                    //$work_160 += $prev_work_160;
                    $work_135 -= $prev_work_160;
                    break;
                }
            }
            // startが翌実施日にかかる
            if ($start_time >= $date_line) {
                switch ($tommorow_holiday) {
                case "0": // 時間外を125, 深夜を150
                    $work_125 = OvertimeCalcUtil::outer_calc($start_time, $end_time, $start_late, $end_late);
                    $work_150 = OvertimeCalcUtil::inner_calc($start_time, $end_time, $start_late, $end_late);
/*
                    // 深夜帯かつ所定労働時間内に申請された残業時間の計算
                    // 当日深夜
                    $rest_work = self::get_rest_work($start_time, $end_time, $start_late, $end_late, $fixed_start_work_time, $fixed_end_work_time);
                    $log->debug("rest_work_next:".$rest_work);
                    $work_150 -= $rest_work;
                    $work_125 += $rest_work;
*/
                    break;
                case "1": // 時間内を135hol, 時間外を135, 深夜を160
                    $work_135_hol = OvertimeCalcUtil::inner_calc($start_time, $end_time, $fixed_start_work_time, $fixed_end_work_time);
                    $work_135     = OvertimeCalcUtil::outer_calc($start_time, $end_time, $start_late, $end_late) - $work_135_hol;
                    $work_160     = OvertimeCalcUtil::inner_calc($start_time, $end_time, $start_late, $end_late);
                    break;
                case "2": // 時間外を135, 深夜を160
                    $work_135     = OvertimeCalcUtil::outer_calc($start_time, $end_time, $start_late, $end_late);
                    $work_160     = OvertimeCalcUtil::inner_calc($start_time, $end_time, $start_late, $end_late);
                    break;
                }
            }

            // start, endが0時を跨る
            if ($start_time < $date_line && $end_time > $date_line) {
                switch ($current_holiday) {
                case "0": // 時間外を125, 深夜を150
                    $work_125 = OvertimeCalcUtil::outer_calc($start_time, $date_line, $start_late, $end_late);
                    $work_150 = OvertimeCalcUtil::inner_calc($start_time, $date_line, $start_late, $end_late);

                    // 前日分の深夜も考慮
                    $prev_work_150 = OvertimeCalcUtil::inner_calc($start_time, $date_line, $prev_start_late, $prev_end_late);
                    //$work_150 += $prev_work_150;
                    $work_125 -= $prev_work_150;
                    /*
                    // 深夜帯かつ所定労働時間内に申請された残業時間の計算
                    // 当日深夜
                    $rest_work = self::get_rest_work($start_time, $date_line, $start_late, $end_late, $fixed_start_work_time, $fixed_end_work_time);
                    $log->debug("rest_work:".$rest_work);
                    $work_150 -= $rest_work;
                    $work_125 += $rest_work;

                    // 前日深夜
                    $prev_rest_work = self::get_rest_work($start_time, $date_line, $prev_start_late, $prev_end_late, $fixed_start_work_time, $fixed_end_work_time);
                    $log->debug("rest_work_prev:".$prev_rest_work);
                    $work_150 -= $prev_rest_work;
                    $work_125 += $prev_rest_work;
                    */
                    break;
                case "1": // 時間内を135hol, 時間外を135, 深夜を160
                    $work_135_hol = OvertimeCalcUtil::inner_calc($start_time, $date_line, $fixed_start_work_time, $fixed_end_work_time);
                    $work_135     = OvertimeCalcUtil::outer_calc($start_time, $date_line, $start_late, $end_late) - $work_135_hol;
                    $work_160     = OvertimeCalcUtil::inner_calc($start_time, $date_line, $start_late, $end_late);

                    $prev_work_160 = OvertimeCalcUtil::inner_calc($start_time, $date_line, $prev_start_late, $prev_end_late);
                    //$work_160 += $prev_work_160;
                    if ($work_135_hol < $prev_work_160) {
                        $work_135 -= ($prev_work_160 - $work_135_hol);
                        $work_135_hol = 0;
                    } else {
                        $work_135_hol -= $prev_work_160;
                    }
                    break;
                case "2": // 時間外を135, 深夜を160
                    $work_135     = OvertimeCalcUtil::outer_calc($start_time, $date_line, $start_late, $end_late);
                    $work_160     = OvertimeCalcUtil::inner_calc($start_time, $date_line, $start_late, $end_late);

                    $prev_work_160 = OvertimeCalcUtil::inner_calc($start_time, $date_line, $prev_start_late, $prev_end_late);
                    //$work_160 += $prev_work_160;
                    $work_135 -= $prev_work_160;
                    break;
                }

                switch ($tommorow_holiday) {
                case "0": // 時間外を125, 深夜を150
                    $next_work_125 = OvertimeCalcUtil::outer_calc($date_line, $end_time, $start_late, $end_late);
                    $next_work_150 = OvertimeCalcUtil::inner_calc($date_line, $end_time, $start_late, $end_late);
                    // 深夜帯かつ所定労働時間内に申請された残業時間の計算
                    // 当日深夜
/*
                    $rest_work = self::get_rest_work($date_line, $end_time, $start_late, $end_late, $fixed_start_work_time, $fixed_end_work_time);
                    $log->debug("rest_work2:".$rest_work);
                    $next_work_150 -= $rest_work;
                    $next_work_125 += $rest_work;
*/
                    break;
                case "1": // 時間内を135hol, 時間外を135, 深夜を160
                    $next_work_135_hol = OvertimeCalcUtil::inner_calc($date_line, $end_time, $fixed_start_work_time, $fixed_end_work_time);
                    $next_work_135     = OvertimeCalcUtil::outer_calc($date_line, $end_time, $start_late, $end_late) - $work_135_hol;
                    $next_work_160     = OvertimeCalcUtil::inner_calc($date_line, $end_time, $start_late, $end_late);
                    break;
                case "2": // 時間外を135, 深夜を160
                    $next_work_135     = OvertimeCalcUtil::outer_calc($date_line, $end_time, $start_late, $end_late);
                    $next_work_160     = OvertimeCalcUtil::inner_calc($date_line, $end_time, $start_late, $end_late);
                    break;
                }
            }

            self::rest_time_calc($next_work_125, $rest_time);
            self::rest_time_calc($work_125     , $rest_time);
            self::rest_time_calc($work_135_hol , $rest_time);
            self::rest_time_calc($next_work_135_hol, $rest_time);
            self::rest_time_calc($work_135     , $rest_time);
            self::rest_time_calc($next_work_135, $rest_time);
            self::rest_time_calc($prev_work_150, $rest_time);
            self::rest_time_calc($work_150     , $rest_time);
            self::rest_time_calc($next_work_150, $rest_time);
            self::rest_time_calc($prev_work_160, $rest_time);
            self::rest_time_calc($work_160     , $rest_time);
            self::rest_time_calc($next_work_160, $rest_time);
            
            if ($is_shorttime_emp) {
                self::dispatch_short_time_calc(
                    $org_work_time, $emp_work_time,
                    $prev_work_150,
                    $prev_work_160,
                    $work_125     ,
                    $work_135     ,
                    $work_135_hol ,
                    $work_150     ,
                    $work_160     ,
                    $next_work_150,
                    $next_work_160,
                    $next_work_125,
                    $next_work_135,
                    $next_work_135_hol,
                    $work_100
                );
            }

            // 発生順に格納
            if ($prev_work_160     > 0) { 
                $ovtm_seq++; array_push($ovtm_seq_list, self::create_seq($ovtm_seq, 5, $prev_work_160    )); 
            }
            if ($work_125          > 0) { 
                $ovtm_seq++; array_push($ovtm_seq_list, self::create_seq($ovtm_seq, 2, $work_125         )); 
            }
            if ($work_135          > 0) { 
                $ovtm_seq++; array_push($ovtm_seq_list, self::create_seq($ovtm_seq, 3, $work_135         )); 
            }
            if ($work_135_hol      > 0) { 
                $ovtm_seq++; array_push($ovtm_seq_list, self::create_seq($ovtm_seq, 7, $work_135_hol     )); 
            }
            if ($work_150          > 0) { 
                $ovtm_seq++; array_push($ovtm_seq_list, self::create_seq($ovtm_seq, 4, $work_150         )); 
            }
            if ($work_160          > 0) { 
                $ovtm_seq++; array_push($ovtm_seq_list, self::create_seq($ovtm_seq, 5, $work_160         )); 
            }
            if ($next_work_150     > 0) { 
                $ovtm_seq++; array_push($ovtm_seq_list, self::create_seq($ovtm_seq, 4, $next_work_150    )); 
            }
            if ($next_work_160     > 0) { 
                $ovtm_seq++; array_push($ovtm_seq_list, self::create_seq($ovtm_seq, 5, $next_work_160    )); 
            }
            if ($next_work_125     > 0) { 
                $ovtm_seq++; array_push($ovtm_seq_list, self::create_seq($ovtm_seq, 2, $next_work_125    )); 
            }
            if ($next_work_135     > 0) { 
                $ovtm_seq++; array_push($ovtm_seq_list, self::create_seq($ovtm_seq, 3, $next_work_135    )); 
            }
            if ($next_work_135_hol > 0) { 
                $ovtm_seq++; array_push($ovtm_seq_list, self::create_seq($ovtm_seq, 7, $next_work_135_hol)); 
            }
            if ($prev_work_150     > 0) { 
                $ovtm_seq++; array_push($ovtm_seq_list, self::create_seq($ovtm_seq, 4, $prev_work_150    )); 
            }
            if ($work_100     > 0) { 
                $ovtm_seq++; array_push($ovtm_seq_list, self::create_seq($ovtm_seq, 1, $work_100         )); 
            }

            $ttl_ovtm_100     += $work_100;
            $ttl_ovtm_125     += $work_125 + $next_work_125;
            $ttl_ovtm_135     += $work_135 + $next_work_135;
            $ttl_ovtm_135_hol += $work_135_hol + $next_work_135_hol;
            $ttl_ovtm_150     += $work_150 + $prev_work_150 + $next_work_150;
            $ttl_ovtm_160     += $work_160 + $prev_work_160 + $next_work_160;
        }

        $log->debug("----- total calc -----");
        $log->debug("100:".$ttl_ovtm_100);
        $log->debug("125:".$ttl_ovtm_125);
        $log->debug("135:".$ttl_ovtm_135);
        $log->debug("135_hol:".$ttl_ovtm_135_hol);
        $log->debug("150:".$ttl_ovtm_150);
        $log->debug("160:".$ttl_ovtm_160);

        $obj = new OvertimeKbn();
        $obj->kbn_100 = $ttl_ovtm_100 / 60;
        $obj->kbn_125 = $ttl_ovtm_125 / 60;
        $obj->kbn_135 = $ttl_ovtm_135 / 60;
        $obj->kbn_135_hol = $ttl_ovtm_135_hol / 60;
        $obj->kbn_150 = $ttl_ovtm_150 / 60;
        $obj->kbn_160 = $ttl_ovtm_160 / 60;

        $log->debug("----- Calc End -----");

        // 計算順序をDBへ登録
        OvertimeSeqModel::delete($emp_id, $rslt_date);
        OvertimeSeqModel::insert($emp_id, $rslt_date, $ovtm_seq_list);
        return $obj;
    }

    private static function rest_time_calc(&$work_min, &$rest_min) {
        if ($work_min < $rest_min) {
            $rest_min -= $work_min;
            $work_min = 0;
        } else {
            $work_min  -= $rest_min;
            $rest_min = 0;
        }
    }
    
    // 短時間勤務用の計算
    private static function dispatch_short_time_calc(
                    $org_work_time, &$emp_work_time,
                    &$prev_work_150,&$prev_work_160,
                    &$work_125     ,&$work_135     ,
                    &$work_135_hol , // ok
                    &$work_150     ,&$work_160     ,
                    &$next_work_150,&$next_work_160,
                    &$next_work_125,&$next_work_135,
                    &$next_work_135_hol, &$work_100
    ) 
    {
        // prev 160 
        if ($prev_work_150 > 0) {
            $bkup_prev_work_150 = $prev_work_150;
            if (($emp_work_time + $prev_work_150) <= $org_work_time) { // 加算しても超えない
                $work_125 = $prev_work_150;
                $prev_work_150 = 0;
            } else { // 加算したら超える
                if ($emp_work_time <= $org_work_time) { // 加算前は越えるのか、超えているならそのまま
                    $prev_work_150 = ($emp_work_time + $prev_work_150) - $org_work_time;
                    $work_125 = $org_work_time - $emp_work_time;
                }
            }
            $emp_work_time += $bkup_prev_work_150;
        }

        // prev 160 
        if ($prev_work_160 > 0) {
            $bkup_prev_work_160 = $prev_work_160;
            if (($emp_work_time + $prev_work_160) <= $org_work_time) { // 加算しても超えない
                $work_135 = $prev_work_160;
                $prev_work_160 = 0;
            } else { // 加算したら超える
                if ($emp_work_time <= $org_work_time) { // 加算前は越えるのか、超えているならそのまま
                    $prev_work_160 = ($emp_work_time + $prev_work_160) - $org_work_time;
                    $work_135 = $org_work_time - $emp_work_time;
                }
            }
            $emp_work_time += $bkup_prev_work_160;
        }
        // 125 
        if ($work_125 > 0) {
            $bkup_work_125 = $work_125;
            if (($emp_work_time + $work_125) <= $org_work_time) { // 加算しても超えない
                $work_100 = $work_125;
                $work_125 = 0;
            } else { // 加算したら超える
                if ($emp_work_time <= $org_work_time) { // 加算前は越えるのか、超えているならそのまま
                    $work_125 = ($emp_work_time + $work_125) - $org_work_time;
                    $work_100 = $org_work_time - $emp_work_time;
                }
            }
            $emp_work_time += $bkup_work_125;        
        }

        // 135 
        if ($work_135 > 0) {
            $bkup_work_135 = $work_135;
            if (($emp_work_time + $work_135) <= $org_work_time) { // 加算しても超えない
                $work_100 = $work_135;
                $work_135 = 0;
            } else { // 加算したら超える
                if ($emp_work_time <= $org_work_time) { // 加算前は越えるのか、超えているならそのまま
                    $work_135 = ($emp_work_time + $work_135) - $org_work_time;
                    $work_100 = $org_work_time - $emp_work_time;
                }
            }
            $emp_work_time += $bkup_work_135;        
        }

        // 135_hol 
        if ($work_135_hol > 0) {
            $bkup_work_135_hol = $work_135_hol;
            if (($emp_work_time + $work_135_hol) <= $org_work_time) { // 加算しても超えない
                $work_100 = $work_135_hol;
                $work_135_hol = 0;
            } else { // 加算したら超える
                if ($emp_work_time <= $org_work_time) { // 加算前は越えるのか、超えているならそのまま
                    $work_135_hol = ($emp_work_time + $work_135_hol) - $org_work_time;
                    $work_100 = $org_work_time - $emp_work_time;
                }
            }
            $emp_work_time += $bkup_work_135_hol;        
        }

        //  150 
        if ($work_150 > 0) {
            $bkup_work_150 = $work_150;
            if (($emp_work_time + $work_150) <= $org_work_time) { // 加算しても超えない
                $work_125 = $work_150;
                $work_150 = 0;
            } else { // 加算したら超える
                if ($emp_work_time <= $org_work_time) { // 加算前は越えるのか、超えているならそのまま
                    $work_150 = ($emp_work_time + $work_150) - $org_work_time;
                    $work_125 = $org_work_time - $emp_work_time;
                }
            }
            $emp_work_time += $bkup_work_150;
        }

        //  160
        if ($work_160 > 0) {
            $bkup_work_160 = $work_160;
            if (($emp_work_time + $work_160) <= $org_work_time) { // 加算しても超えない
                $work_135 = $work_160;
                $work_160 = 0;
            } else { // 加算したら超える
                if ($emp_work_time <= $org_work_time) { // 加算前は越えるのか、超えているならそのまま
                    $work_160 = ($emp_work_time + $work_160) - $org_work_time;
                    $work_135 = $org_work_time - $emp_work_time;
                }
            }
            $emp_work_time += $bkup_work_160;
        }

        // next_ 150 
        if ($next_work_150 > 0) {

            $bkup_next_work_150 = $next_work_150;
            if (($emp_work_time + $next_work_150) <= $org_work_time) { // 加算しても超えない
                $work_125 = $next_work_150;
                $next_work_150 = 0;
            } else { // 加算したら超える
                if ($emp_work_time <= $org_work_time) { // 加算前は越えるのか、超えているならそのまま
                    $next_work_150 = ($emp_work_time + $next_work_150) - $org_work_time;
                    $work_125 = $org_work_time - $emp_work_time;
                }
            }
            $emp_work_time += $bkup_next_work_150;
        }

        // next_ 160
        if ($next_work_160 > 0) {
            $bkup_next_work_160 = $next_work_160;
            if (($emp_work_time + $next_work_160) <= $org_work_time) { // 加算しても超えない
                $work_135 = $next_work_160;
                $next_work_160 = 0;
            } else { // 加算したら超える
                if ($emp_work_time <= $org_work_time) { // 加算前は越えるのか、超えているならそのまま
                    $next_work_160 = ($emp_work_time + $next_work_160) - $org_work_time;
                    $work_135 = $org_work_time - $emp_work_time;
                }
            }
            $emp_work_time += $bkup_next_work_160;
        } 
        // next_ 125 
        if ($next_work_125 > 0 ) {
            $bkup_next_work_125 = $next_work_125;
            if (($emp_work_time + $next_work_125) <= $org_work_time) { // 加算しても超えない
                $work_100 = $next_work_125;
                $next_work_125 = 0;
            } else { // 加算したら超える
                if ($emp_work_time <= $org_work_time) { // 加算前は越えるのか、超えているならそのまま
                    $next_work_125 = ($emp_work_time + $next_work_125) - $org_work_time;
                    $work_100 = $org_work_time - $emp_work_time;
                }
            }
            $emp_work_time += $bkup_next_work_125;
        }

        // next_ 135 
        if ($next_work_135 > 0) {
            $bkup_next_work_135 = $next_work_135;
            if (($emp_work_time + $next_work_135) <= $org_work_time) { // 加算しても超えない
                $work_100 = $next_work_135;
                $next_work_135 = 0;
            } else { // 加算したら超える
                if ($emp_work_time <= $org_work_time) { // 加算前は越えるのか、超えているならそのまま
                    $next_work_135 = ($emp_work_time + $next_work_135) - $org_work_time;
                    $work_100 = $org_work_time - $emp_work_time;
                }
            }
            $emp_work_time += $bkup_next_work_135;
        }
        // next_ 135_hol 
        if ($next_work_135_hol > 0) {
            $bkup_next_work_135_hol = $next_work_135_hol;
            if (($emp_work_time + $next_work_135_hol) <= $org_work_time) { // 加算しても超えない
                $work_100 = $next_work_135_hol;
                $next_work_135_hol = 0;
            } else { // 加算したら超える
                if ($emp_work_time <= $org_work_time) { // 加算前は越えるのか、超えているならそのまま
                    $next_work_135_hol = ($emp_work_time + $next_work_135_hol) - $org_work_time;
                    $work_100 = $org_work_time - $emp_work_time;
                }
            }
            $emp_work_time += $bkup_next_work_135_hol;
        }
    }
    
    // 深夜帯かつ所定労働時間内に申請された残業時間の計算
    private static function get_rest_work($start_time, $end_time, $start_late, $end_late, $fixed_start_work_time, $fixed_end_work_time) {
        
        // 深夜帯 && 所定労働時間の集合積をとる
        $start_period;
        $end_period;
        $log = self::get_logger();
        // 覆う
        if ($fixed_start_work_time < $start_late && $end_late < $fixed_end_work_time) {
            $log->debug("1");
            $start_period = $start_late;
            $end_period = $end_late;
        }
        // 覆われる
        if ($fixed_start_work_time >= $start_late && $end_late >= $fixed_end_work_time) {
            $log->debug("2");
            $start_period = $fixed_start_work_time;
            $end_period = $fixed_end_work_time;
        }
        // 範囲より過去重複あり
        if ($fixed_start_work_time < $start_late && $start_late <= $fixed_end_work_time && $fixed_end_work_time > $end_late) {
            $log->debug("3");
            $start_period = $start_late;
            $end_period = $fixed_end_work_time;
        }
        // 範囲より未来重複あり
        if ($fixed_start_work_time < $end_late && $fixed_start_work_time >= $start_late && $fixed_end_work_time > $end_late) {
            $log->debug("4");
            $start_period = $fixed_start_work_time;
            $end_period = $end_late;
        }
        
        // 集合積がない場合は、0で戻る
        if (empty($start_period) || empty($end_period)) {
            return 0;
        }
        
        // 勤務時間と上記集合積の集合積が深夜帯かつ所定労働時間内に申請された残業時間の計算
        return OvertimeCalcUtil::inner_calc($start_time, $end_time, $start_period, $end_period);
}

    private static function get_logger() {
        return new common_log_class(basename(__FILE__));
    }

    private static function create_seq($seq, $kbn_id, $minute) {
        return array(
            "no" => $seq,
            "ovtmkbn_id" => $kbn_id,
            "minute" => $minute / 60
        );
    }
}
?>
