<?php
require_once 'kintai/common/mdb.php';
require_once 'kintai/common/master_util.php';
require_once 'kintai/common/user_info.php';

class OvertimeCalcUtil {

    /**
     * 休日判定
     * 0:平日 1:休日 2:週休
     */
    static function is_holiday($date, $emp_id = null, $atdptn_id = null, $reason_id = null) {
        // カレンダー休日かどうか
        $is_holiday = self::is_cal_holiday($date);
        /*
        if (empty($emp_id)) {
            return $is_holiday;
        }
        */
        // 事由を取得 20150204 y-itou reason_idとatdptn_idがテンプレートから取得できてなかったため。
        if (empty($atdptn_id)) {
        $rslt = self::get_reason($date, $emp_id);
            $reason_id = $rslt["reason"];
            $atdptn_id = $rslt["pattern"];
        }
        /*
        if ($atdptn_id == "10") {
            return "2";
        } else if ($reason_id == "4" || ($is_holiday && $reason_id != "14")) {
            return "1";
        } else {
            return "0";
        }
        */
        
        //if ($atdptn_id == "10" || $is_holiday) {	//20150204 y-itou 公休(24)と代休(4)のみが135/100の計算。
        if (($atdptn_id == "10" && $reason_id == "24" || $reason_id == "4") || $is_holiday) {
            // 週休の残業申請＋祝日の残業申請
            return 2;
        } else {
            return 0;
        }
        // return 1は計算しなくなる
    }

    /**
     * カレンダー上の休日かどうか
     * 0:通常 1:休日
     */
    static function is_cal_holiday($date) {
        $sql = "select type from calendar where date = :date";
        $value = self::get_mdb()->one(
            $sql, array("text"), array("date" => $date));

        if (in_array($value, array("6", "7"))) {
            return true;
        }
        return false;
    }
    
    /**
     * 勤務実績から事由を取得
     */
    private static function get_reason($date, $emp_id) {
        $sql = "select reason, pattern from atdbkrslt where ".
            "emp_id = :emp_id and ".
            "date   = :date";

        return self::get_mdb()->first_row(
            $sql,
            array("text","text"),
            array(
                "emp_id" => $emp_id,
                "date" => $date
            )
        );
    }

    
    
    /**
     * 短時間勤務職員かどうか
     */
    static function is_shorttime_employee($emp_id) {
        
        // 個人の所定労働時間を取得
        $user_info = UserInfo::get($emp_id, "u");
        $emp_work_time = $user_info->get_emp_condition()->specified_time;

        if (empty($emp_work_time)) {
            return false; // 所定労働時間がなければ常勤もしくは非常勤職員
        }

        // 組織の所定労働時間を取得
        $org_work_time = MasterUtil::get_organization_work_time();
        if (empty($org_work_time)) {
            return false; // 判定できないの常勤、非常勤
        }

        if (intval($emp_work_time) < intval($org_work_time)) {
            return true; // 職員＜組織で短時間勤務職員
        }
        return false;
    }

    private static function get_mdb() {
        $log = new common_log_class(basename(__FILE__));
        return new MDB($log);
    }
    
    // 指定された時間外の外側を計算 
    static function outer_calc($start_time, $end_time, $start_prod, $end_prod) {

        // 覆うパターン
        if ($start_time < $start_prod && $end_time > $end_prod) {
            return ($start_prod - $start_time) + ($end_time - $end_prod);
        }

        // 範囲より過去重複なし
        if ($start_time < $start_prod && $end_time <= $start_prod) {
            return $end_time - $start_time;
        }
        // 範囲より過去重複あり
        if ($start_time < $start_prod && $end_time > $start_prod) {
            return $start_prod - $start_time;
        }
        // 範囲より未来重複なし
        if ($start_time >= $end_prod && $end_time > $end_prod) {
            return $end_time - $start_time;
        }
        // 範囲より未来重複あり
        if ($start_time < $end_prod && $end_time > $end_prod) {
            return $end_time - $end_prod;
        }
        return 0;
    }

    // 指定された時間帯の内側を計算を計算
    static function inner_calc($start_time, $end_time, $start_prod, $end_prod) {
        // 覆う
        if ($start_time < $start_prod && $end_time > $end_prod) {
            return $end_prod - $start_prod;
        }
        // 覆われる
        if ($start_time >= $start_prod && $end_time <= $end_prod) {
            return $end_time - $start_time;
        }
        // 範囲より過去重複あり
        if ($start_time < $start_prod && $end_time > $start_prod) {
            return $end_time - $start_prod;
        }
        // 範囲より未来重複あり
        if ($start_time < $end_prod && $end_time > $end_prod) {
            return $end_prod - $start_time;
        }
        return 0;
    }

    // 深夜帯の時間を返す
    static function get_overtime_late($date) {
        $start = mktime(22,0,0,$date["month"], $date["day"], $date["year"]);
        $end   = mktime(5,0,0, $date["month"], $date["day"]+1, $date["year"]);
        return array("start_time" => $start, "end_time" => $end);
    }

    private function __construct() {}
}
?>
