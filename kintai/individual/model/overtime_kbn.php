<?php
require_once 'kintai/common/date_util.php';
require_once 'kintai/model/overtime_basic_sum_model.php';
require_once 'kintai/model/overtime_sum_record.php';

class OvertimeKbn {
    public $kbn_100 = 0;
    public $kbn_125 = 0;
    public $kbn_135 = 0;
    public $kbn_135_hol = 0;
    public $kbn_150 = 0;
    public $kbn_160 = 0;
    public $kbn_25 = 0;

    private static $kbn_list = array(
        "kbn_100"     => "1",
        "kbn_125"     => "2",
        "kbn_135"     => "3",
        "kbn_150"     => "4",
        "kbn_160"     => "5",
    );
    private static $id_list = array(
        "id_1" => "kbn_100",
        "id_2" => "kbn_125",
        "id_3" => "kbn_135",
        "id_4" => "kbn_150",
        "id_5" => "kbn_160",
    );
    private static $calc_list = array(
        "ovtm_calc_1" => "kbn_100",
        "ovtm_calc_2" => "kbn_125",
        "ovtm_calc_3" => "kbn_135",
        "ovtm_calc_4" => "kbn_150",
        "ovtm_calc_5" => "kbn_160",
    );

    function get_response() {
        $response = array();
        $log = new common_log_class(basename(__FILE__));
        foreach ($this as $name => $value) {
            $item = array();
            $id = self::$kbn_list[$name];
            $item["element_id"] = "ovtm_minute_".$id;
            $item["element_id_hidden"] = "ovtm_calc_".$id;
            $item["over_time"]  = DateUtil::convert_time($value);
            array_push($response, $item);
        }
        return $response;
    }

    function get_minute($id) {
        $property = self::$id_list["id_".$id];
        return intval($this->$property);
    }

    // Formからパラメータへ
    static function create_from_submit_form() {
        $obj = new OvertimeKbn();

        foreach (self::$calc_list as $name => $property) {
            $value;
            if (isset($_POST[$name])) {
                $value = $_POST[$name];
            } else {
                $value = $_POST["form"][$name];//Ajaxの場合
            }
            $obj->$property = DateUtil::convert_min($value);
        }
        return $obj;
    }

    function update($emp_id, $date) {
        foreach (self::$kbn_list as $prop => $id) {

            if (OvertimeBasicSumModel::get($date, $emp_id, $id) != null) {
                OvertimeBasicSumModel::update($this->$prop, $emp_id, $date, $id);
            } else {
                $rec = new OvertimeSumRecord();
                $rec->emp_id = $emp_id;
                $rec->date   = $date;
                $rec->ovtmkbn_id = $id;
                $rec->minute = $this->$prop;
                OvertimeBasicSumModel::insert($rec);
            }
        }
    }
    function delete($emp_id, $date) {
        OvertimeBasicSumModel::delete($emp_id, $date);
    }
}
?>
