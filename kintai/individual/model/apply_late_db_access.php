<?php
require_once 'kintai/common/mdb.php';
require_once 'kintai/common/date_util.php';

class ApplyLateDBAccess {
    
    // 指定された日付に申請済の遅刻早退が存在するか。
    static function late_exists($emp_id, $start_date, $apply_id, $kind_flag) {
        
        // 有無を調べる      
        if (!$type) {
            $sql = 
                "select count(apply_id) from kintai_late where ".
                " emp_id = :emp_id and ".
                " start_date = :start_date and ".
                " kind_flag = :kind_flag and ".
                " apply_id <> :apply_id";
        }

        $types = array(
            "text","text","text","integer"
        );
        $values = array(
            "emp_id" => $emp_id,
            "start_date" => $start_date,
            "kind_flag" => $kind_flag,
            "apply_id" => empty($apply_id) ? 0:$apply_id
        );
        
        $count = self::get_mdb()->one($sql, $types, $values);
        
        return (bool) $count;
    }
    
    /**
     * 勤務実績を取得(テンプレート)
     * 画面初期表示およびAjaxで取得する際に使用
     */
    static function get_atd_result($date, $emp_id, $tmcd_group_id = null, $atdptn_id = null) {
        
        $wk_date = $date;
        if (empty($tmcd_group_id) || empty($atdptn_id)) {
			$arr_ymd = DateUtil::split_ymd($wk_date);
			//翌日勤務パターンの開始が前日である場合、翌日データを取得
			$wk_date2 = DateUtil::to_date($arr_ymd["y"], $arr_ymd["m"], $arr_ymd["d"]+1, "Ymd");
			$arr_info = self::get_atdptn_info($wk_date2, $emp_id);
			if ($arr_info["previous_day_flag"] == "1") {
		        $wk_date = $wk_date2;
			}
			else {
				//明け、休暇の場合は前日データを取得
				$arr_info = self::get_atdptn_info($wk_date, $emp_id);
				if ($arr_info["after_night_duty_flag"] == "1" || $arr_info["pattern"] == "10") {
					//$last_day_time = DateUtil::to_time($arr_ymd["y"], $arr_ymd["m"], $arr_ymd["d"]-1);
					$wk_date = DateUtil::to_date($arr_ymd["y"], $arr_ymd["m"], $arr_ymd["d"]-1, "Ymd");
				}
			}
            $types = array("text", "text");
            $values = array("date" => $wk_date, "emp_id" => $emp_id);
            
            $sql = 
                "select ".
                "group_key as tmcd_group_id, ".
                "ptn_key as pattern, ".
                "atdptn_nm, ".
                "officehours2_start, ".
                "officehours2_end, ".
                "officehours4_start, ".
                "officehours4_end, ".
                "empcond_officehours_flag, ".
                "reason_key as reason_id,".
                "meeting_start_time, ".
                "meeting_end_time, ".
                "display_name, ".
                "default_name, ".
                "start_time, ".
                "end_time ".
                "from ".
                "( ".
                "( ".
                "( ".
                "select emp_id, date as date_key, pattern as ptn_key, reason as reason_key, tmcd_group_id as group_key, start_time, end_time from atdbkrslt ".
                ") rslt left outer join ( ".
                "select atdptn_id, group_id, empcond_officehours_flag, atdptn_nm, meeting_start_time, meeting_end_time from atdptn) ptn on rslt.ptn_key = CAST(ptn.atdptn_id AS varchar) and rslt.group_key = ptn.group_id ".
                ") a left outer join atdbk_reason_mst mst on a.reason_key = mst.reason_id ".
                ") b left outer join  ".
                "( ".
                "select tmcd_group_id, pattern, officehours_id, officehours4_start, officehours4_end, officehours2_start, officehours2_end from officehours ".
                ")  hours on b.ptn_key = hours.pattern and b.group_key = hours.tmcd_group_id and hours.officehours_id = '1' ".
                "where emp_id = :emp_id and date_key = :date";
        } else {
            $types = array("integer", "integer");
            $values = array("group_id" => $tmcd_group_id, "atdptn_id" => $atdptn_id);
            
            $sql = 
                "select ".
                "tmcd_group_id, ".
                "atdptn_id as pattern, ".
                "atdptn_nm, ".
                "officehours2_start, ".
                "officehours2_end, ".
                "officehours4_start, ".
                "officehours4_end, ".
                "empcond_officehours_flag, ".
                "reason as reason_id, ".
                "meeting_start_time, ".
                "meeting_end_time, ".
                "display_name, ".
                "default_name ".
                "from ".
                "atdptn p left outer join officehours o on CAST(p.atdptn_id AS varchar) = o.pattern and p.group_id = o.tmcd_group_id and officehours_id = '1' ".
                "left outer join atdbk_reason_mst m on p.reason = m.reason_id ".
                "where  ".
                "p.atdptn_id = :atdptn_id and ".
                "p.group_id = :group_id";
        }
        
        $atdrslt = self::get_mdb()->first_row($sql, $types, $values);
        
        //実績がない場合、予定から所定を取得
        if (count($atdrslt) == 0) {
            $types = array("text", "text");
            $values = array("date" => $wk_date, "emp_id" => $emp_id);
            
            $sql = 
                "select ".
                "group_key as tmcd_group_id, ".
                "ptn_key as pattern, ".
                "atdptn_nm, ".
                "officehours2_start, ".
                "officehours2_end, ".
                "officehours4_start, ".
                "officehours4_end, ".
                "empcond_officehours_flag, ".
                "reason_key as reason_id,".
                "meeting_start_time, ".
                "meeting_end_time, ".
                "display_name, ".
                "default_name ".
                "from ".
                "( ".
                "( ".
                "( ".
                "select emp_id, date as date_key, pattern as ptn_key, reason as reason_key, tmcd_group_id as group_key from atdbk ".
                ") rslt left outer join ( ".
                "select atdptn_id, group_id, empcond_officehours_flag, atdptn_nm, meeting_start_time, meeting_end_time from atdptn) ptn on rslt.ptn_key = CAST(ptn.atdptn_id AS varchar) and rslt.group_key = ptn.group_id ".
                ") a left outer join atdbk_reason_mst mst on a.reason_key = mst.reason_id ".
                ") b left outer join  ".
                "( ".
                "select tmcd_group_id, pattern, officehours_id, officehours4_start, officehours4_end, officehours2_start, officehours2_end from officehours ".
                ")  hours on b.ptn_key = hours.pattern and b.group_key = hours.tmcd_group_id and hours.officehours_id = '1' ".
                "where emp_id = :emp_id and date_key = :date";
            
            $atdrslt = self::get_mdb()->first_row($sql, $types, $values);
        }
        
        //start_time,end_timeを:付きにする。テンプレートへの表示のため。
        if ($atdrslt["start_time"] != "") {
            $atdrslt["start_time"] = substr($atdrslt["start_time"], 0, 2).":".substr($atdrslt["start_time"], 2, 2);
        }
        if ($atdrslt["end_time"] != "") {
            $atdrslt["end_time"] = substr($atdrslt["end_time"], 0, 2).":".substr($atdrslt["end_time"], 2, 2);
        }
        
        if ($atdrslt["empcond_officehours_flag"] != "1") {
            return $atdrslt;
        }
        
        // 個人の勤務条件を優先する場合
        // 現在の曜日を取得
        $sp_ymd = DateUtil::split_ymd($wk_date);
        $day_of_week = DateUtil::day_of_week(date('Y-m-d', mktime(0,0,0,$sp_ymd['m'], $sp_ymd['d'], $sp_ymd['y'])), false);
        // 祝日を取得
        $holiday = self::get_mdb()->one(
                "select type from calendar where date = :date",
                array("text"), array("date" => $wk_date));
        
        // 曜日の所定労働時間を取得
        $day_of_week_time_sql = 
            "select officehours2_start, officehours2_end, officehours4_start, officehours4_end from empcond_officehours where ".
            "emp_id = :emp_id and weekday = :day_of_week";
        
        $types = array("text", "integer");
        $values["emp_id"] = $emp_id;
        if ($holiday == "6") { // 祝日の場合
            $values["day_of_week"] = "7";
            
            $rslt = self::get_mdb()->first_row($day_of_week_time_sql, $types, $values);
            
            if (!empty($rslt)) {
                return self::override_office_hour($atdrslt, $rslt);
            }
        }
        
        // 曜日で取得
        $values["day_of_week"] = $day_of_week;
        $rslt = self::get_mdb()->first_row($day_of_week_time_sql, $types, $values);
        
        if (!empty($rslt)) {
            return self::override_office_hour($atdrslt, $rslt);
        }
        
        // 基本で取得
        $values["day_of_week"] = "8";
        $rslt = self::get_mdb()->first_row($day_of_week_time_sql, $types, $values);
        if (!empty($rslt)) {
            return self::override_office_hour($atdrslt, $rslt);
        }
        
        // 基本もなければ変わらない
        return $atdrslt;
    }

	//対象日の勤務パターンの時間帯情報を取得
    static function get_atdptn_info($date, $emp_id) {
    
        $types = array("text", "text");
        $values = array("date" => $date, "emp_id" => $emp_id);
        
        $sql = 
            "select ".
            "a.pattern, ".
            "b.* ".
            "from atdbkrslt a ".
            "left join atdptn b on a.tmcd_group_id = b.group_id and a.pattern = cast(b.atdptn_id as varchar) ".
            "where a.emp_id = :emp_id and a.date = :date";
        $atdrslt = self::get_mdb()->first_row($sql, $types, $values);
        //実績がない場合、予定から取得
        if (count($atdrslt) == 0) {
            $types = array("text", "text");
            $values = array("date" => $date, "emp_id" => $emp_id);
        
	        $sql = 
	            "select ".
	            "a.pattern, ".
	            "b.* ".
	            "from atdbk a ".
	            "left join atdptn b on a.tmcd_group_id = b.group_id and a.pattern = cast(b.atdptn_id as varchar) ".
	            "where a.emp_id = :emp_id and a.date = :date";
	        $atdrslt = self::get_mdb()->first_row($sql, $types, $values);
		}
        return $atdrslt;
    }

    private static function override_office_hour($atd_rslt, $override_hours) {
        $atd_rslt["officehours2_start"] = $override_hours["officehours2_start"];
        $atd_rslt["officehours4_start"] = $override_hours["officehours4_start"];
        $atd_rslt["officehours2_end"] = $override_hours["officehours2_end"];
        $atd_rslt["officehours4_end"] = $override_hours["officehours4_end"];
        return $atd_rslt;
    }
    
    private static function get_mdb() {
        $log = new common_log_class(basename(__FILE__));
        return new MDB($log);
    }
    
    private function __construct() {}
}
?>
