<?php
require_once 'kintai/common/mdb.php';

/**
 * 勤務実績テーブル(バックアップ)
 */
class AtdbkRsltBackupModel {
	
	/**
	 * バックアップ(承認前に実行)
	 */
	static function backup($emp_id, $date) {
		
		$where_types = array("text","text");
		$where_values= array("emp_id" => $emp_id, "date" => $date);

		// select
		$atdbkrslt = self::get_mdb()->first_row(
			"select * from atdbkrslt where emp_id = :emp_id and date = :date",
			$where_types, $where_values
		);
		$values = array(array(
			$emp_id,
			$date,
			$atdbkrslt["pattern"],
			$atdbkrslt["reason"],
			$atdbkrslt["night_duty"],
			$atdbkrslt["allow_id"],
			$atdbkrslt["start_time"],
			$atdbkrslt["out_time"],
			$atdbkrslt["ret_time"],
			$atdbkrslt["end_time"],
			$atdbkrslt["o_start_time1"],
			$atdbkrslt["o_end_time1"],
			$atdbkrslt["o_start_time2"],
			$atdbkrslt["o_end_time2"],
			$atdbkrslt["o_start_time3"],
			$atdbkrslt["o_end_time3"],
			$atdbkrslt["o_start_time4"],
			$atdbkrslt["o_end_time4"],
			$atdbkrslt["o_start_time5"],
			$atdbkrslt["o_end_time5"],
			$atdbkrslt["o_start_time6"],
			$atdbkrslt["o_end_time6"],
			$atdbkrslt["o_start_time7"],
			$atdbkrslt["o_end_time7"],
			$atdbkrslt["o_start_time8"],
			$atdbkrslt["o_end_time8"],
			$atdbkrslt["o_start_time9"],
			$atdbkrslt["o_end_time9"],
			$atdbkrslt["o_start_time10"],
			$atdbkrslt["o_end_time10"],
			$atdbkrslt["status"],
			$atdbkrslt["tmcd_group_id"],
			$atdbkrslt["meeting_time"],
			$atdbkrslt["previous_day_flag"],
			$atdbkrslt["next_day_flag"],
			$atdbkrslt["reg_prg_flg"],
			$atdbkrslt["start_btn_time"],
			$atdbkrslt["end_btn_time"],
			$atdbkrslt["meeting_start_time"],
			$atdbkrslt["meeting_end_time"],
			$atdbkrslt["start_date"],
			$atdbkrslt["end_date"],
			$atdbkrslt["over_start_time"],
			$atdbkrslt["over_end_time"],
			$atdbkrslt["over_start_next_day_flag"],
			$atdbkrslt["over_end_next_day_flag"],
			$atdbkrslt["update_emp_id"],
			$atdbkrslt["update_time"],
			$atdbkrslt["legal_in_over_time"],
			$atdbkrslt["early_over_time"],
			$atdbkrslt["rest_start_time"],
			$atdbkrslt["rest_end_time"],
			$atdbkrslt["allow_count"],
			$atdbkrslt["over_start_time2"],
			$atdbkrslt["over_end_time2"],
			$atdbkrslt["over_start_next_day_flag2"],
			$atdbkrslt["over_end_next_day_flag2"],
			$atdbkrslt["detail"]
		));
		
		// 削除
		self::get_mdb()->delete(
			"delete from atdbkrslt_backup where emp_id = :emp_id and date = :date",
			$where_types, $where_values
		);
		
		// 挿入
		self::get_mdb()->insert("atdbkrslt_backup", self::$columns, self::$types, $values);
	}
	
	/**
	 * リストア(削除前に実行)
	 */
	static function restore($emp_id, $date) {

		$where_types = array("text","text");
		$where_values= array("emp_id" => $emp_id, "date" => $date);

		// select
		$atdbkrslt = self::get_mdb()->first_row(
			"select * from atdbkrslt_backup where emp_id = :emp_id and date = :date",
			$where_types, $where_values
		);
		$values = array(array(
			$emp_id,
			$date,
			$atdbkrslt["pattern"],
			$atdbkrslt["reason"],
			$atdbkrslt["night_duty"],
			$atdbkrslt["allow_id"],
			$atdbkrslt["start_time"],
			$atdbkrslt["out_time"],
			$atdbkrslt["ret_time"],
			$atdbkrslt["end_time"],
			$atdbkrslt["o_start_time1"],
			$atdbkrslt["o_end_time1"],
			$atdbkrslt["o_start_time2"],
			$atdbkrslt["o_end_time2"],
			$atdbkrslt["o_start_time3"],
			$atdbkrslt["o_end_time3"],
			$atdbkrslt["o_start_time4"],
			$atdbkrslt["o_end_time4"],
			$atdbkrslt["o_start_time5"],
			$atdbkrslt["o_end_time5"],
			$atdbkrslt["o_start_time6"],
			$atdbkrslt["o_end_time6"],
			$atdbkrslt["o_start_time7"],
			$atdbkrslt["o_end_time7"],
			$atdbkrslt["o_start_time8"],
			$atdbkrslt["o_end_time8"],
			$atdbkrslt["o_start_time9"],
			$atdbkrslt["o_end_time9"],
			$atdbkrslt["o_start_time10"],
			$atdbkrslt["o_end_time10"],
			$atdbkrslt["status"],
			$atdbkrslt["tmcd_group_id"],
			$atdbkrslt["meeting_time"],
			$atdbkrslt["previous_day_flag"],
			$atdbkrslt["next_day_flag"],
			$atdbkrslt["reg_prg_flg"],
			$atdbkrslt["start_btn_time"],
			$atdbkrslt["end_btn_time"],
			$atdbkrslt["meeting_start_time"],
			$atdbkrslt["meeting_end_time"],
			$atdbkrslt["start_date"],
			$atdbkrslt["end_date"],
			$atdbkrslt["over_start_time"],
			$atdbkrslt["over_end_time"],
			$atdbkrslt["over_start_next_day_flag"],
			$atdbkrslt["over_end_next_day_flag"],
			$atdbkrslt["update_emp_id"],
			$atdbkrslt["update_time"],
			$atdbkrslt["legal_in_over_time"],
			$atdbkrslt["early_over_time"],
			$atdbkrslt["rest_start_time"],
			$atdbkrslt["rest_end_time"],
			$atdbkrslt["allow_count"],
			$atdbkrslt["over_start_time2"],
			$atdbkrslt["over_end_time2"],
			$atdbkrslt["over_start_next_day_flag2"],
			$atdbkrslt["over_end_next_day_flag2"]
		));
		
		// 削除
		self::get_mdb()->delete(
			"delete from atdbkrslt where emp_id = :emp_id and date = :date",
			$where_types, $where_values
		);
		// 挿入
		self::get_mdb()->insert("atdbkrslt", self::$columns, self::$types, $values);
	}
	
	private static function get_mdb() {
		$log = new common_log_class(basename(__FILE__));
		return new MDB($log);
	}
	
	private static $columns = array(
		"emp_id",
		"date",
		"pattern",
		"reason",
		"night_duty",
		"allow_id",
		"start_time",
		"out_time",
		"ret_time",
		"end_time",
		"o_start_time1",
		"o_end_time1",
		"o_start_time2",
		"o_end_time2",
		"o_start_time3",
		"o_end_time3",
		"o_start_time4",
		"o_end_time4",
		"o_start_time5",
		"o_end_time5",
		"o_start_time6",
		"o_end_time6",
		"o_start_time7",
		"o_end_time7",
		"o_start_time8",
		"o_end_time8",
		"o_start_time9",
		"o_end_time9",
		"o_start_time10",
		"o_end_time10",
		"status",
		"tmcd_group_id",
		"meeting_time",
		"previous_day_flag",
		"next_day_flag",
		"reg_prg_flg",
		"start_btn_time",
		"end_btn_time",
		"meeting_start_time",
		"meeting_end_time",
		"start_date",
		"end_date",
		"over_start_time",
		"over_end_time",
		"over_start_next_day_flag",
		"over_end_next_day_flag",
		"update_emp_id",
		"update_time",
		"legal_in_over_time",
		"early_over_time",
		"rest_start_time",
		"rest_end_time",
		"allow_count",
		"over_start_time2",
		"over_end_time2",
		"over_start_next_day_flag2",
		"over_end_next_day_flag2"
	);
	
	private static $types = array(
		"text",
		"text",
		"text",
		"text",
		"text",
		"integer",
		"text",
		"text",
		"text",
		"text",
		"text",
		"text",
		"text",
		"text",
		"text",
		"text",
		"text",
		"text",
		"text",
		"text",
		"text",
		"text",
		"text",
		"text",
		"text",
		"text",
		"text",
		"text",
		"text",
		"text",
		"text",
		"integer",
		"text",
		"integer",
		"integer",
		"text",
		"text",
		"text",
		"text",
		"text",
		"text",
		"text",
		"text",
		"text",
		"integer",
		"integer",
		"text",
		"text",
		"text",
		"text",
		"text",
		"text",
		"text",
		"text",
		"text",
		"integer",
		"integer"
	);
	
	private function __construct() {}
}
?>
