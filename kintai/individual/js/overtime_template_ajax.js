var AJAX_CONTROLLER = "kintai/individual/controller/ovtm_ajax_controller.php";

// 画面初期化時や勤務実績日変更の際に呼ばれる
function init_display_ajax() {
	// 実施日を取得
	var date = format_date(new DateFormat("yyyyMMdd"), get_date(j$("#rslt_year").val(), j$("#rslt_month").val(), j$("#rslt_day").val()));

	var data = {
		controller:AJAX_CONTROLLER,
		method:"get_ovtm_data",
		auth:7,
		session:j$("#session").val(),
		apply_emp_id:j$("#apply_emp_id").val(),
		rslt_date:date
	}

	ajaxSend("kintai_ajax_dispatcher.php", data,
		function (res) {
			set_initial_data(res);
		}
	);
}

function overtime_calc(param, calc_flag) {

	var check_data = {
		controller:AJAX_CONTROLLER,
		method:"submit_check",
		auth:7,
		session:j$("#session").val(),
		form:param
	}

	var continue_flg = true;
	ajaxSend("kintai_ajax_dispatcher.php", check_data,
			function (res) {
				if (res.message) {
					if (res.next) {
						if (!confirm(res.message)) {
							continue_flg = false;
						}
					} else {
						alert(res.message);
						continue_flg = false;
					}
				}
			}
	);

	if (!continue_flg) {
		return false;
	}

	var data = {
		controller:AJAX_CONTROLLER,
		method:"ovtm_calc",
		auth:7,
		session:j$("#session").val(),
        form:param,
        calc_regist:calc_flag
	}

	ajaxSend("kintai_ajax_dispatcher.php", data,
			function (res) {
				set_calc_data(res);
			}
			);
    return true;
}

// 計算結果を表示する
function set_calc_data(data) {
	// 計算結果を表示
	set_ovtm_minute(data);
	// 合計を表示
	set_ovtm_sum(data);
}

// データをテンプレートに反映
function set_initial_data(data) {

	// 超過勤務時間計
	set_ovtm_sum(data);

	// 勤務実績
	if (data.rslt_info == null) {
		alert("勤務実績が存在しません。");
		return;
	}

	// 勤務実績の設定
	set_kintai_rslt(data);

	// 残業時間の設定
	if (j$("#draft").val() != "true") { // 下書きのときはそのまま
		clear_ovtm();
		if (data.ovtm_exists) {
			set_ovtm(data);
		}
	}

	// 超過勤務時間
	set_ovtm_minute(data);
}

// 残業時間を設定する
function set_ovtm(data) {
	set_ovtm_result(data.ovtm_info); // 従事時間
}

function clear_ovtm() {
	j$("#result_table").find("select").val("");
	j$("#result_table").find("input").attr("checked", "");
}

/*
 * rslt_start_time1":
 * rslt_start_next_flag1":
 * rslt_end_time1":
 * rslt_end_next_flag1":
 * rslt_rest_time1":
 * rslt_ovtmrsn_id1":
 * rslt_call_flag1":
 * rslt_start_time2":
 * rslt_start_next_flag2":
 * rslt_end_time2":
 * rslt_end_next_flag2":
 * rslt_rest_time2":
 * rslt_ovtmrsn_id2":
 * rslt_call_flag2":
 * rslt_start_time3":
 * rslt_start_next_flag3":
 * rslt_end_time3":
 * rslt_end_next_flag3":
 * rslt_rest_time3":
 * rslt_ovtmrsn_id3":
 * rslt_call_flag3":
 */
function set_ovtm_result(data) {
	// 1行目
	var sp_start1 = split_hi(data.rslt_start_time1);
	set_combobox("rslt_start_hour1", sp_start1.hour);
	set_combobox("rslt_start_minute1", sp_start1.minute);
	set_chkbox("rslt_start_next_flag1", data.rslt_start_next_flag1);
	var sp_end1 = split_hi(data.rslt_end_time1);
	set_combobox("rslt_end_hour1", sp_end1.hour);
	set_combobox("rslt_end_minute1", sp_end1.minute);
	set_chkbox("rslt_end_next_flag1", data.rslt_end_next_flag1);
	var sp_rest1 = split_hi(data.rslt_rest_time1);
	set_combobox("rslt_rest_hour1", sp_rest1.hour);
	set_combobox("rslt_rest_minute1", sp_rest1.minute);
	set_combobox("rslt_ovtmrsn_id1", data.rslt_ovtmrsn_id1);
	set_chkbox("rslt_call_flag1", data.rslt_call_flag1);
  set_value("rslt_ovtmrsn_txt1", data.rslt_ovtmrsn_txt1);

	// 2行目
	var sp_start2 = split_hi(data.rslt_start_time2);
	set_combobox("rslt_start_hour2", sp_start2.hour);
	set_combobox("rslt_start_minute2", sp_start2.minute);
	set_chkbox("rslt_start_next_flag2", data.rslt_start_next_flag2);
	var sp_end2 = split_hi(data.rslt_end_time2);
	set_combobox("rslt_end_hour2", sp_end2.hour);
	set_combobox("rslt_end_minute2", sp_end2.minute);
	set_chkbox("rslt_end_next_flag2", data.rslt_end_next_flag2);
	var sp_rest2 = split_hi(data.rslt_rest_time2);
	set_combobox("rslt_rest_hour2", sp_rest2.hour);
	set_combobox("rslt_rest_minute2", sp_rest2.minute);
	set_combobox("rslt_ovtmrsn_id2", data.rslt_ovtmrsn_id2);
	set_chkbox("rslt_call_flag2", data.rslt_call_flag2);
  set_value("rslt_ovtmrsn_txt2", data.rslt_ovtmrsn_txt2);

	// 3行目
	var sp_start3 = split_hi(data.rslt_start_time3);
	set_combobox("rslt_start_hour3", sp_start3.hour);
	set_combobox("rslt_start_minute3", sp_start3.minute);
	set_chkbox("rslt_start_next_flag3", data.rslt_start_next_flag3);
	var sp_end3 = split_hi(data.rslt_end_time3);
	set_combobox("rslt_end_hour3", sp_end3.hour);
	set_combobox("rslt_end_minute3", sp_end3.minute);
	set_chkbox("rslt_end_next_flag3", data.rslt_end_next_flag3);
	var sp_rest3 = split_hi(data.rslt_rest_time3);
	set_combobox("rslt_rest_hour3", sp_rest3.hour);
	set_combobox("rslt_rest_minute3", sp_rest3.minute);
	set_combobox("rslt_ovtmrsn_id3", data.rslt_ovtmrsn_id3);
	set_chkbox("rslt_call_flag3", data.rslt_call_flag3);
  set_value("rslt_ovtmrsn_txt3", data.rslt_ovtmrsn_txt3);
}

/**
 * 勤務実績の設定
 "rslt_info": {
 "tmcd_group_id":
 "pattern":
 "atdptn_nm":
 "officehours2_start":
 "officehours2_end":
 "display_name":
 "default_name":
 */
function set_kintai_rslt(data) {
	j$("#atdptn_nm").empty();
	j$("#officehours2_start").empty();
	j$("#officehours2_end").empty();
	j$("#reason").empty();

	j$("#atdptn_nm").append(data.rslt_info.atdptn_nm);
	j$("#officehours2_start").append(data.rslt_info.officehours2_start);
	j$("#officehours2_end").append(data.rslt_info.officehours2_end);
	var reason = "";
	if (data.rslt_info.display_name != null) {
		reason = data.rslt_info.display_name;
	} else if (data.rslt_info.display_name == null) {
		reason = data.rslt_info.default_name;
	}
	j$("#reason").append(reason);
}

/**
 * 超過勤務時間計を設定
 "ovtm_total": [
 "ovtm_sum_1":
 "ovtm_sum_2":
 ]
 */
function set_ovtm_sum(data) {
	j$.each(data.ovtm_total, function(key, val) {
		j$("#"+key).empty();
		j$("#"+key).append(val);
	});
}

/**
 * 超過勤務時間を設定
 "ovtm_minute": [
 {
 "ovtmkbn_id":
 "ovtmkbn_kind":
 "ovtmkbn_name":
 "order_no":
 "emp_id":
 "date":
 "fkey":
 "minute":
 "element_id":
 "over_time":
 },
 */
function set_ovtm_minute(data) {
	j$.each(data.ovtm_minute, function () {
		j$("#"+this.element_id).empty();
		j$("#"+this.element_id).append(this.over_time);
		j$("#"+this.element_id_hidden).empty();
		j$("#"+this.element_id_hidden).val(this.over_time);
	});
}
