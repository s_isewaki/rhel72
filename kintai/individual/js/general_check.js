// 入力種別を取得
function get_input_type(kind_flg) {
    if (kind_flg == 1 || kind_flg == 4 || kind_flg == 5 || kind_flg == 11) {
        return "TYPE1"; 
    }
    if (kind_flg == 9 || kind_flg == 10 || kind_flg == 12 || kind_flg == 13 || kind_flg == 14) {
        return "TYPE3"; 
    }
    return "TYPE2";
}

// 申請時の入力チェック
function input_check() {

    var param = get();
    var kind_flg = param.kind_flag;
    // typeを取得
    var db_check = true;
    switch(get_input_type(kind_flg)) {
    case "TYPE1":
        if (!type1_check()) { return false; }
        break;
    case "TYPE2":
        if (!type2_check()) { return false; }
        break;
    case "TYPE3":
        if (!type3_check()) { return false; }
        break;
    } 

    set_template_title(kind_flg);

    // 重複申請チェック
    var exists = true;
    var data = {
        controller:AJAX_CONTROLLER,
        method:"apply_check",
        auth:7,
        session:param.session,
        data:param
    }

    ajaxSend("kintai_ajax_dispatcher.php", data, 
            function (res) {
                exists = false;
            }
            );
    if (exists) {
        return false;
    }
    return true;
}

function type1_check() {
    return day_calc();
}
function type2_check() {
    var param = get();
    if (!require_date(
                param.date3.year,
                param.date3.month,
                param.date3.day)) {
                    alert("日付が入力されていません。");
                    return false;
                }
    if (!validate_date(
                param.date3.year,
                param.date3.month,
                param.date3.day
                )) {
                    alert("日付が不正です。");
                    return false;
                }
    var kind_flg = param.kind_flag;
    if (kind_flg == 2 || kind_flg == 7) {
        if (!param.atdptn) {
            alert("勤務実績が入力されていません。");
            return false;
        }
    }

    return true;
}
function type3_check() {
    var param = get();
    if (!require_date(
                param.date4.year,
                param.date4.month,
                param.date4.day)) {
                    alert("日付が入力されていません。");
                    return false;
                }
    if (!validate_date(
                param.date4.year,
                param.date4.month,
                param.date4.day
                )) {
                    alert("日付が不正です。");
                    return false;
                }
    var start_hour = param.start_time.hour;
    var start_minute = param.start_time.minute;
    var end_hour = param.end_time.hour;
    var end_minute = param.end_time.minute;
    if (!require_time(start_hour, start_minute)) {
        alert("開始時刻が入力されていません。");
        return false;
    }

    if (require_time(end_hour  , end_minute)) {
        var start_date = new Date(2000, 1, 1, start_hour, start_minute, 0);
        var end_date = new Date(2000, 1, 1, end_hour, end_minute, 0);

        if (compare_date(start_date, end_date) > 0) {
            alert("開始時刻と終了時刻が前後しています。");
            return false;
        }
    }

    return true;
}

