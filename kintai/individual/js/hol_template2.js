var AJAX_CONTROLLER = "kintai/individual/controller/hol2_ajax_controller.php";

j$(document).ready(function() {

    // 職員選択の制御
    // 決裁・申請のオプション、代理申請の設定を使用するためコメント化
    //var approved_by_flg = j$("#approved_by_flg").val();
    //if (approved_by_flg == 'f') {
    //    j$("#emp_select_btn").hide();
    //}

    // 種別選択
    j$("[name^=kind_flag]").change(function() {
        var type = j$(this).val();
        get_reason(type, "");
    });

    j$("[name^=reason]").change(function() {
    	var type1 = j$(this).val();
    	reason_change(type1 ,"");
    	time_hol_control();
    });

    // 休暇期間選択の計算
    j$("#date_y2, #date_m2, #date_d2").change(function() {
    	var tmp_y = j$("#date_y2").val();
        var tmp_m = j$("#date_m2").val();
        var tmp_d = j$("#date_d2").val();

    	j$("#date_y3").val(tmp_y);
    	j$("#date_m3").val(tmp_m);
    	j$("#date_d3").val(tmp_d);
        day_calc();
    });
    j$("#date_y3, #date_m3, #date_d3").change(function() {
        day_calc();
    });

    // 日数の計算
    day_calc();

    var type = "";
    if (j$("[name^=kind_flag]:checked").val()) {
        type = j$("[name^=kind_flag]:checked").val();
    } else {
        return;
    }

    // コンボボックス作成と復元(事由)
    var reason = j$("#hid_reason").val();
//alert('debug');
    get_reason(type, reason);
    time_hol_control();

    // コンボボックス作成と復元(パターン)
    var pattern = j$("#hid_pattern").val();
    get_pattern(reason, pattern);

    // コンボボックス作成と復元(パターン)
    var detail = j$("#hid_detail").val();
    get_detail(reason, detail);
});

//事由・細目選択 20140626
function reason_change(type,value) {

	get_pattern(type, value); //事由表示
	get_detail(type, value); //細目表示

	time_hol_control();
	select_pattern_by_reason(type);
}


// 時間有休の入力制御をコントロール
function time_hol_control() {
//    if (j$("#reason option:selected").attr("time_hol") == "true") {
    if (j$("[name^=reason]:checked").attr("time_hol") == "true") {
        j$("#start_time_hour").attr("disabled", "");
        j$("#start_time_minute").attr("disabled", "");
        j$("#use_time_hour").attr("disabled", "");
        j$("#use_time_minute").attr("disabled", "");
    } else {
        j$("#start_time_hour").attr("disabled", "disabled");
        j$("#start_time_minute").attr("disabled", "disabled");
        j$("#use_time_hour").attr("disabled", "disabled");
        j$("#use_time_minute").attr("disabled", "disabled");
    }
}

// 事由を取得
function get_reason(type, value) {

	j$("#tr_pattern").hide();
    j$("#area_pattern").empty();
    j$("#area_pattern").attr("disabled", "disabled");

    j$("#tr_detail").hide();
    j$("#area_detail").empty();
	j$("#area_detail").attr("disabled", "disabled");

    if (!type) {
        return;
    };
    var data = {
        controller:AJAX_CONTROLLER,
        method:"get_reason_list",
        auth:7,
        session:j$("#session").val(),
        kind_flg:type,
        t_hol_type:j$("#t_hol_type").val()
    };

    ajaxSend("kintai_ajax_dispatcher.php", data,
            function (res) {
                j$("#area_reason").empty();
                j$("#area_reason").append(create_radio(res.data, 'reason'));
                j$("#area_reason").width();
                if (j$("#area_reason").children().length > 1) {
                    if (value) {
                    	j$("[name^=reason]").each(function(){
                    		var reason_value = j$(this).val();
                    		if (reason_value == value){
                    			j$(this).attr('checked','checked');
                    		}
                    	});
                    }
                }
                recital_list = res.data;
            }
            );
}
var recital_list;

// 勤務パターンを取得
function get_pattern(reason_id, value) {

    j$("#area_pattern").empty();
    j$("#area_pattern").attr("disabled", "disabled");

    var data = {
        controller:AJAX_CONTROLLER,
        method:"get_pattern_list",
        auth:7,
        session:j$("#session").val(),
        emp_id:j$("#apply_emp_id").val(),
        reason:reason_id
    };

    ajaxSend("kintai_ajax_dispatcher.php", data,
        function (res) {
            j$("#area_pattern").empty();
            j$("#area_pattern").append(create_radio(res.data, 'pattern'));
            j$("#area_pattern").width();
            if (j$("#area_pattern").children().length > 1) {
            	j$("#tr_pattern").show();
            	j$("#area_pattern").removeAttr("disabled");
                if (value) {
                	j$("[name^=pattern]").each(function(){
                		var reason_value = j$(this).val();
                		if (reason_value == value){
                			j$(this).attr('checked','checked');
                		}
                	});
                }
            } else {
            	j$("#tr_pattern").hide();
            	j$("#area_pattern").attr("disabled", "disabled");
            }
        }
        );
}

// 細目を取得
function get_detail(reason_id, value) {

	j$("#area_detail").empty();
	j$("#area_detail").attr("disabled", "disabled");

	var data = {
			controller:AJAX_CONTROLLER,
			method:"get_detail_list",
			auth:7,
			session:j$("#session").val(),
			reason:reason_id
	};

	ajaxSend("kintai_ajax_dispatcher.php", data,
		function (res) {
			j$("#area_detail").empty();
			j$("#area_detail").append(create_radio(res.data, 'detail'));
			j$("#area_detail").width();
			if (j$("#area_detail").children().length >= 1) {
				j$("#tr_detail").show();
				j$("#area_detail").removeAttr("disabled");
				if (value) {
					j$("[name^=detail]").each(function(){
                		var reason_value = j$(this).val();
                		if (reason_value == value){
                			j$(this).attr('checked','checked');
                		}
                	});
				}
			} else {
				j$("#tr_detail").hide();
				j$("#area_detail").attr("disabled", "disabled");
			}
		}
	);
}

// 取得済の夏休日数を取得
function get_summer_holidays(param){
	var taken_holidays = 0;

	var data = {
			controller:AJAX_CONTROLLER,
			method:"get_summer_holidays",
			auth:7,
			session:j$("#session").val(),
			emp_id:param.emp_id,
			start_date:param.start_date,
			end_date:param.end_date,
			summer_start_date:param.summer_start_date,
			summer_end_date:param.summer_end_date,
			reason:param.reason
	};

	ajaxSend("kintai_ajax_dispatcher.php", data,
			function (res) {
				if (res.data){
					taken_holidays = res.data;
				}
			}
	);

	return taken_holidays;
}

function create_radio(res, mode) {

	var radio = "";
	if (res.length > 0){
		var event_flg = (mode == "reason") ?  " onclick='reason_change(this.value);' " : "";
		var class_flg = (mode == "detail") ? " class='__day__' " : "";
		var chk_flg = (res.length == 1) ? " checked " : "";

		var radio_str = '<label name=\"' + mode + '__no__\"><input type=\"radio\" id=\"' + mode + '__id__\" name=\"' + mode + '[]\" value=\"__value__\" __option__' + class_flg + event_flg +  chk_flg + '>__label__</label>';
	    j$.each(res, function() {
	        radio += radio_str.
	        replace(/__no__/, this.value).
	        replace(/__id__/, this.value).
	        replace(/__value__/, this.value).
	        replace(/__label__/, this.label) + "\n";

	        if(this.time_hol_flag == "t"){
	        	radio = radio.replace(/__option__/, "time_hol=\"true\"");
	        }else{
	        	radio = radio.replace(/__option__/, "");
	        }

	        if (mode == "detail") {radio = radio.replace(/__day__/,this.day);}
	    });
	}
    return radio;
}

// 職員選択画面
var childwin;
function openEmployeeListHol(item_id) {
    dx = screen.width;
    dy = screen.top;
    base = 0;
    wx = 720;
    wy = 600;
    var url = './emplist_popup.php';
    url += '?session=' + j$("[name=session]").val();
    url += '&emp_id=' + j$("[name=apply_emp_id]").val();
    url += '&item_id='+item_id;
    url += '&callback_func=add_target_list_hol';
    childwin = window.open(url, 'emplistpopup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
    childwin.focus();
}

// 職員選択画面から呼ばれる
function add_target_list_hol(item_id, select_emp_id, name) {
    var list = select_emp_id.split(",");
    if (list.length > 1) {
        alert("複数の職員が選択されています。");
        return false;
    }

    var data = {
        controller:AJAX_CONTROLLER,
        method:"get_emp_info",
        auth:7,
        session:j$("#session").val(),
        emp_id:select_emp_id
    };

    ajaxSend("kintai_ajax_dispatcher.php", data,
            function (res) {
                // 職員ID更新
                j$("#apply_emp_id").val(res.data.emp_id);
                j$("#span_emp_id").empty();
                j$("#span_emp_id").append(res.data.emp_personal_id);
                // 名称更新
                j$("#span_emp_name").empty();
                j$("#span_emp_name").append(res.data.emp_lt_nm.escapeHTML() + "　" + res.data.emp_ft_nm.escapeHTML());
                // 職種
                j$("#span_emp_job").empty();
                j$("#span_emp_job").append(res.data.job_nm.escapeHTML());
                // 雇用、勤務形態
                j$("#hid_duty_form").val(res.data.duty_form);
                j$("#duty_form").empty();
                j$("#duty_form").append(res.data.duty_form_name.escapeHTML());
                // 給与支給区分
                j$("#salary_type").empty();
                j$("#salary_type").append(res.data.salary_type_name.escapeHTML());
                // 所属
                j$("#belong_class").empty();
                j$("#belong_class").append(res.data.organization.escapeHTML());
                // 年休残
                j$("#paid_hol_remain").empty();
                j$("#paid_hol_remain").append(res.data.paid_hol_remain.escapeHTML());
            }
    );
    childwin.close();
}

// 申請時の入力チェック
function input_check() {

    var param = get();
    if (!require(param)) {
        return false;
    }
    if (!validate(param)) {
        return false;
    }
    if (!related_check(param)) {
        return false;
    }
    day_calc();

    set_template_title();

    return true;
}

// 必須チェック
function require(param) {

	var notice_msg = "";

    if (!require_date(
                param.apply_date.year,
                param.apply_date.month,
                param.apply_date.day)) {
                    notice_msg += "申請日が入力されていません。" + ",";
                }

    if (!param.kind_flag) {notice_msg += "休暇の種類を選択してください。" + ",";}

    if (!param.reason) {notice_msg += "休暇の事由を選択してください。" + ",";}

    if (j$("#tr_pattern").is(':visible') && !param.pattern) {notice_msg += "半日休暇の場合の勤務パターンを選択してください。" + ",";}

    if (j$("#tr_detail").is(':visible') && !param.detail) {notice_msg += "休暇の細目を選択してください。" + ",";}

    if (!require_date(
                param.start_date.year,
                param.start_date.month,
                param.start_date.day)) {
    				notice_msg += "休暇取得予定日(開始)が入力されていません。" + ",";
                }

    if (!require_date(
                param.end_date.year,
                param.end_date.month,
                param.end_date.day)) {
    				notice_msg += "休暇取得予定日(終了)が入力されていません。" + ",";
                }

    //事由の細目の限度日数を超過したら警告する
    if (j$("#tr_detail").is(':visible') && param.detail && j$("#hid_duty_form").val() != ""){

    	day_calc();
    	var day_count = (j$("#day_count").val() != "") ? Number(j$("#day_count").val()) : 0;
    	var detail_items = param.detail_class.split("_");
    	var days = (j$("#hid_duty_form").val() == "2") ? Number(detail_items[1]) : Number(detail_items[0]);

    	if (day_count > 0 && days > 0 && day_count > days){
    		notice_msg += "休暇取得限度日数（" + days + "日）を超えているため、申請できません。" + ",";
    	}
    }

    // 夏休管理のチェック
    if(param.summer_flag == "t") {
    	var summer_info = get_summer_holidays(param);

    	if (summer_info){
    		var remaining_days = Number(summer_info.remaining_days);
    		var apply_days = Number(summer_info.apply_days);

    		var start_date = param.start_date["year"] + param.start_date["month"] + param.start_date["day"];
    		var end_date = param.end_date["year"] + param.end_date["month"] + param.end_date["day"];

			var check_summer_start_date = param.start_date["year"] + param.summer_start_date.substr(4,4);
			var check_summer_end_date = param.start_date["year"] + param.summer_end_date.substr(4,4);

    		if (check_summer_start_date > start_date || start_date > check_summer_end_date || check_summer_start_date > end_date || end_date > check_summer_end_date){
    			var st_date = check_summer_start_date.substr(0,4) + "年" + param.summer_start_date.substr(4,2) + "月" + param.summer_start_date.substr(6,2) + "日";
    			var ed_date = check_summer_end_date.substr(0,4) + "年" + param.summer_end_date.substr(4,2) + "月" + param.summer_end_date.substr(6,2) + "日";

    			notice_msg += "夏季休暇は、" + st_date +  "〜" + ed_date + "までの間に取得してください。" + ",";
    		}

    		if (apply_days > remaining_days) {
    			var days = apply_days - remaining_days;
    			notice_msg += "夏季休暇の限度日数を" + days + "日超えているため、申請できません。";
    		}
    	}
    }

    if (notice_msg != ""){
    	var msg = notice_msg.replace(/,/g, '\n');
    	alert(msg);
    	return false;
    }

    return true;
}

// validateチェック
function validate(param) {

    // 日付の不正をチェック
    if (!validate_date(
                param.apply_date.year,
                param.apply_date.month,
                param.apply_date.day)) {
                    alert("申請日の形式が不正です。");
                    return false;
                }

    if (!validate_date(
                param.start_date.year,
                param.start_date.month,
                param.start_date.day)) {
                    alert("休暇取得予定日(開始)の形式が不正です。");
                    return false;
                }

    if (!validate_date(
                param.end_date.year,
                param.end_date.month,
                param.end_date.day)) {
                    alert("休暇取得予定日(終了)の形式が不正です。");
                    return false;
                }
    return true;
}

function related_check(param) {
    // 日付をdateに変換
    var apply_date = get_date(
            param.apply_date.year,
            param.apply_date.month,
            param.apply_date.day
            );
    var start_date = get_date(
            param.start_date.year,
            param.start_date.month,
            param.start_date.day
            );
    var end_date = get_date(
            param.end_date.year,
            param.end_date.month,
            param.end_date.day
            );

    // 休暇予定日の整合性
    if (compare_date(start_date, end_date) > 0) {
        alert("休暇日が不正です。");
        return false;
    }

    // 休暇時間の必須チェック
    if (compare_date(start_date, end_date) == 0) {
        if (
                !(
                    ( // 全て空はOK(1日分の休暇申請)
                      !param.start_time.hour &&
                      !param.start_time.minute &&
                      !param.use_time.hour &&
                      !param.use_time.minute
                    ) ||
                    ( // 全て入力は(時間単位の申請)
                      param.start_time.hour &&
                      param.start_time.minute &&
                      param.use_time.hour &&
                      param.use_time.minute
                    )
                 )
           ) {
               alert("休暇時間が不正です。");
               return false;
           }
    } else {
        if (
                param.start_time.hour ||
                param.start_time.minute ||
                param.use_time.hour ||
                param.use_time.minute) {
                    alert("時間単位に申請できるのは１日分の申請に限ります。");
                    return false;
                }
    }

    // 申請済休暇日の存在チェック
    var data = {
        controller:AJAX_CONTROLLER,
        method:"apply_check",
        auth:7,
        session:param.session,
        params:param
        /*
        apply_id:param.apply_id,
        emp_id:param.emp_id,
        start_date_year:param.start_date.year,
        start_date_month:param.start_date.month,
        start_date_day:param.start_date.day,
        end_date_year:param.end_date.year,
        end_date_month:param.end_date.month,
        end_date_day:param.end_date.day
        */
    };

    var ajax = true;
    var continue_flg = true;
    ajaxSend("kintai_ajax_dispatcher.php", data,
            function (res) {
                if (res.message) {
                    if (res.next) {
                        if (!confirm(res.message)) {
                            continue_flg = false;
                        }
                    } else {
                        alert(res.message);
                        continue_flg = false;
                    }
                }
            }
            );

    if (!continue_flg) {
        return false;
    }

    return ajax;
}

// パラメータをまとめて取得
function get() {
    return {
        session:j$("#session").val(),
        apply_id:j$("[name=apply_id]").val(),
        emp_id:j$("#apply_emp_id").val(),
        apply_date:{
            year:j$("#date_y1").val(),
            month:j$("#date_m1").val(),
            day:j$("#date_d1").val()
        },
        kind_flag:j$("[name^=kind_flag]:checked").val(),
        reason:j$("#reason").val(),
        reason:j$("[name^=reason]:checked").val(),
        tr_pattern:j$("#tr_pattern").val(),
        pattern:j$("[name^=pattern]:checked").val(),
        tr_detail:j$("#tr_detail").val(),
        detail:j$("[name^=detail]:checked").val(),
        detail_class:j$("[name^=detail]:checked").attr("class"),
        remark:j$("#remark").val(),
        start_date:{
            year:j$("#date_y2").val(),
            month:j$("#date_m2").val(),
            day:j$("#date_d2").val()
        },
        end_date:{
            year:j$("#date_y3").val(),
            month:j$("#date_m3").val(),
            day:j$("#date_d3").val()
        },
        start_time:{
            hour:j$("#start_time_hour").val(),
            minute:j$("#start_time_minute").val()
        },
        use_time:{
            hour:j$("#use_time_hour").val(),
            minute:j$("#use_time_minute").val()
        },
        time_hol:!j$("#start_time_hour").attr("disabled"),
        summer_start_date:j$("#summer_start_date").val(),
        summer_end_date:j$("#summer_end_date").val(),
    	summer_flag:j$("#summer_flag").val()
    };
}

function day_calc() {

    j$("#day_count").val("");

    var param = get();

    if (!require_date(
                param.start_date.year,
                param.start_date.month,
                param.start_date.day)) {
                    return false;
                }

    if (!require_date(
                param.end_date.year,
                param.end_date.month,
                param.end_date.day)) {
                    return false;
                }

    if (!validate_date(
                param.start_date.year,
                param.start_date.month,
                param.start_date.day)) {
                    return false;
                }

    if (!validate_date(
                param.end_date.year,
                param.end_date.month,
                param.end_date.day)) {
                    return false;
                }

    var start_date = get_date(param.start_date.year,param.start_date.month,param.start_date.day);
    var end_date = get_date(param.end_date.year,param.end_date.month,param.end_date.day);

        if (compare_date(start_date, end_date) > 0) {
            return false;
        }

    var diff_day = Math.floor((end_date.getTime() - start_date.getTime()) / (1000 * 60 * 60 * 24)) + 1;

	// 2013/09/30 100日以上までカウントできるように修正
    //if (diff_day > 1 && diff_day < 100) {
    if (diff_day >= 1) {
        j$("#day_count").val(diff_day);
    } else {
        j$("#day_count").val("");
    }
}

function set_template_title() {

    var name = j$("#span_emp_name").text();

    var start_date = j$("#date_y2").val() + "年" + new Number(j$("#date_m2").val()) + "月" + new Number(j$("#date_d2").val()) + "日";
    var end_date = j$("#date_y3").val() + "年" + new Number(j$("#date_m3").val()) + "月" + new Number(j$("#date_d3").val()) + "日";

    var day_count = j$("#day_count").val();

    var reason = j$("#reason option:selected").text();
    if (reason == "") {
    	reason = j$("[name^='reason']:checked").parent().text();
    }

    var user_info = get_user_info(j$("#session").val(), j$("#apply_emp_id").val(), 7);

    var title;

    title = user_info.organization;
    title += " ";
    title += name;
    title += " ";
    title += reason;
    title += " ";

    if (day_count && day_count > 1) {
        title += start_date + "〜" + end_date;
    } else {
        if (j$("#start_time_hour").val()) {
            var start_time = new Number(j$("#start_time_hour").val()) + ":" + j$("#start_time_minute").val() + "から";
            var use_time = new Number(j$("#use_time_hour").val()) + "時間" + new Number(j$("#use_time_minute").val()) + "分";
            title += start_date + " " + start_time + use_time;
        } else {
            title += start_date;
        }
    }

    j$("[name=apply_title]").val(get_limit(title, 80));
}

//年休簿、休暇簿
function openHolList(item_id) {
    dx = screen.width;
    dy = screen.top;
    base = 0;
    wx = 720;
    wy = 600;
    if (item_id == 1) {
        var url = './kintai_vacation_paid_holiday.php';
    }
    else {
        var url = './kintai_vacation.php';
    }

    url += '?session=' + j$("[name=session]").val();
    url += '&selEmpID=' + j$("[name=apply_emp_id]").val();
    url += '&umode=apply';
    childwin = window.open(url, 'paidhollistpopup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
    childwin.focus();
}

//休暇事由の説明
function openExplanation() {
	dx = screen.width;
	dy = screen.top;
	base = 0;
	wx = 720;
	wy = 600;

	var url = './workflow_explanation_setting.php';

	url += '?session=' + j$("[name=session]").val();
	childwin = window.open(url, 'explanation', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
	childwin.focus();
}

//事由と同じ勤務パターンに設定する
function select_pattern_by_reason(no) {
    var reason_text = j$("[name=reason" + no + "]").text();

    j$('label[name^="pattern"]').each(function(i) {
    	var id =  j$(this).attr("name").replace(/pattern/,"");

    	if (reason_text == j$(this).text()){
		   j$("#pattern" + id).attr('checked','checked');
	   	   return false;
    	}

    	if (i == 0) j$("#pattern" + id).attr('checked','checked');
    });
}

//休暇を受ける日を設定する
function set_hol_date(hol_date, item_id) {
	var date_array = hol_date.split("/");

	if (item_id == 1) {
		j$("#date_y2").val(new Number(date_array[0]));
		j$("#date_m2").val(new Number(date_array[1]));
		j$("#date_d2").val(new Number(date_array[2]));
	}
	else {
		j$("#date_y3").val(new Number(date_array[0]));
		j$("#date_m3").val(new Number(date_array[1]));
		j$("#date_d3").val(new Number(date_array[2]));
	}

}
