// onload処理
$(document).ready(function() {

	// 勤務実績や残業時間を取得して表示
	init_display();

	// 勤務グループ選択時
	$("#tmcd_group_id").change(function() {
		change_tmcd_group();
	});
	// 勤務実績選択時
	$("#atdptn_id").change(function() {
		change_atdptn();
	});
	
	// 事由選択時
	$("#reason_id").change(function() {
		time_hol_control();
	});
	
	// 計算ボタン
	$("#ovtm_calc").click(function() {
		ovtm_calc(false);
	});
	// 登録ボタン
	$("#regist").click(function() {
		regist();
	});
});

function ovtm_calc(calc_flag) {
	
	var p = get_param();
	
	// 時間項目のチェック
	if (!calc_check(p)) {
		return false;
	}
	// 超過時間計算
	return overtime_calc(p, calc_flag);
}

function calc_check(p) {
	
	// 勤務グループ
	if (!p.tmcd_group_id) {
		alert("勤務グループが入力されていません。");
		return false;
	}
	// 勤務パターン
	if (!p.atdptn_id) {
		alert("勤務実績が入力されていません。");
		return false;
	}

	// 時間有休のチェック
	if (!check_time_require(
				p.hol_start_hour,
				p.hol_start_minute,
				p.hol_time_hour,
				p.hol_time_minute
				)) {
		alert("時間有休の入力値が不正です。");
		return false;
	}

	// 会議時間等のチェック
	if (!check_time_require(
				p.ward_start_hour,
				p.ward_start_minute,
				p.ward_end_hour,
				p.ward_end_minute
				)) {
		alert("病棟外勤務の入力値が不正です。");
		return false;
	}
	
	if (p.ward_start_hour) {
		if (
			compare_date(
				get_date_time(p.ward_start_hour, p.ward_start_minute),
				get_date_time(p.ward_end_hour, p.ward_end_minute)
				) >= 0 ) {
			alert("病棟外勤務の入力値が不正です。");
			return false;
		}
	}
	return true;
}

function regist() {
	// 入力チェックいろいろ
	if (!ovtm_calc(true)) {
		return;
	}
	var p = get_param();
	ovtm_regist(p);
}

function get_param() {
	var param = {
		session           : $("#session").val(),
		emp_id            : $("#emp_id").val(),
		rslt_date         : $("#rslt_date").val(),
		tmcd_group_id     : $("#tmcd_group_id").val(),
		atdptn_id         : $("#atdptn_id").val(),
		reason_id         : $("#reason_id").val(),
		hol_start_hour    : $("#hol_start_hour").val(),
		hol_start_minute  : $("#hol_start_minute").val(),
		hol_time_hour     : $("#hol_time_hour").val(),
		hol_time_minute   : $("#hol_time_minute").val(),
		ward_start_hour   : $("#ward_start_hour").val(),
		ward_start_minute : $("#ward_start_minute").val(),
		ward_end_hour     : $("#ward_end_hour").val(),
		ward_end_minute   : $("#ward_end_minute").val(),

		rslt:[
			{
				start_hour      : $("#rslt_start_hour1").val(),
				start_minute    : $("#rslt_start_minute1").val(),
				start_next_flag : $("#rslt_start_next_flag1").attr("checked"),
				end_hour        : $("#rslt_end_hour1").val(),
				end_minute      : $("#rslt_end_minute1").val(),
				end_next_flag   : $("#rslt_end_next_flag1").attr("checked"),
				rest_hour       : $("#rslt_rest_hour1").val(),
				rest_minute     : $("#rslt_rest_minute1").val(),
				ovtmrsn_id      : $("#rslt_ovtmrsn_id1").val(),
				call_flag       : $("#rslt_call_flag1").attr("checked"),
				ovtmrsn_txt      : EscapeEUCJP($("#rslt_ovtmrsn_txt1").val())
				},
				{
				start_hour      : $("#rslt_start_hour2").val(),
				start_minute    : $("#rslt_start_minute2").val(),
				start_next_flag : $("#rslt_start_next_flag2").attr("checked"),
				end_hour        : $("#rslt_end_hour2").val(),
				end_minute      : $("#rslt_end_minute2").val(),
				end_next_flag   : $("#rslt_end_next_flag2").attr("checked"),
				rest_hour       : $("#rslt_rest_hour2").val(),
				rest_minute     : $("#rslt_rest_minute2").val(),
				ovtmrsn_id      : $("#rslt_ovtmrsn_id2").val(),
				call_flag       : $("#rslt_call_flag2").attr("checked"),
				ovtmrsn_txt      : EscapeEUCJP($("#rslt_ovtmrsn_txt2").val())
				},
				{
				start_hour      : $("#rslt_start_hour3").val(),
				start_minute    : $("#rslt_start_minute3").val(),
				start_next_flag : $("#rslt_start_next_flag3").attr("checked"),
				end_hour        : $("#rslt_end_hour3").val(),
				end_minute      : $("#rslt_end_minute3").val(),
				end_next_flag   : $("#rslt_end_next_flag3").attr("checked"),
				rest_hour       : $("#rslt_rest_hour3").val(),
				rest_minute     : $("#rslt_rest_minute3").val(),
				ovtmrsn_id      : $("#rslt_ovtmrsn_id3").val(),
				call_flag       : $("#rslt_call_flag3").attr("checked"),
				ovtmrsn_txt      : EscapeEUCJP($("#rslt_ovtmrsn_txt3").val())
			}
		]
	};

	// 計算値を追加
	$("[id^=ovtm_calc_]").each(function() {
		var property = $(this).attr("id");
		var value    = $(this).val();
		param[property] = value;
	});
	return param;
}
