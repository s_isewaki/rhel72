// 日数を計算
function day_calc() {

    j$("#day_count1").val("");
    j$("#day_count2").val("");
    j$("#day_count3").val("");
    j$("#day_count4").val("");
    j$("#day_count5").val("");

    var param = get();

    if (!require_date( param.date1.year, param.date1.month, param.date1.day)) {
        alert("開始日(1行目)は必須です。");
        return false;
    }

    if (!validate_date( param.date1.year, param.date1.month, param.date1.day)) {
        alert("開始日(1行目)の形式が不正です。");
        return false;
    }

    if (require_date( param.date2.year, param.date2.month, param.date2.day)) {
        if (!validate_date( param.date2.year, param.date2.month, param.date2.day)) {
            alert("終了日(1行目)の形式が不正です。");
            return false;
        }
        var date1 = get_date(param.date1.year,param.date1.month,param.date1.day);
        var date2 = get_date(param.date2.year,param.date2.month,param.date2.day);
        if (compare_date(date1, date2) > 0) {
            alert("開始日(1行目)と終了日(1行目)が前後しています。");
            return false;
        }
        var diff_day = Math.floor((date2.getTime() - date1.getTime()) / (1000 * 60 * 60 * 24)) + 1;
        j$("#day_count1").val(diff_day);
    } else {
        j$("#day_count1").val("1");
    }
    
    if (require_date( param.date5.year, param.date5.month, param.date5.day)) {
        if (!validate_date( param.date5.year, param.date5.month, param.date5.day)) {
            alert("開始日(2行目)の形式が不正です。");
            return false;
        }
        if (require_date( param.date6.year, param.date6.month, param.date6.day)) {
            if (!validate_date( param.date6.year, param.date6.month, param.date6.day)) {
                alert("終了日(2行目)の形式が不正です。");
                return false;
            }
            var date5 = get_date(param.date5.year,param.date5.month,param.date5.day);
            var date6 = get_date(param.date6.year,param.date6.month,param.date6.day);
            if (compare_date(date5, date6) > 0) {
                alert("開始日(2行目)と終了日(2行目)が前後しています。");
                return false;
            }
            var diff_day = Math.floor((date6.getTime() - date5.getTime()) / (1000 * 60 * 60 * 24)) + 1;
            j$("#day_count2").val(diff_day);
        } else {
            j$("#day_count2").val("1");
        }
    }
    
    if (require_date( param.date7.year, param.date7.month, param.date7.day)) {
        if (!validate_date( param.date7.year, param.date7.month, param.date7.day)) {
            alert("開始日(3行目)の形式が不正です。");
            return false;
        }
        if (require_date( param.date8.year, param.date8.month, param.date8.day)) {
            if (!validate_date( param.date8.year, param.date8.month, param.date8.day)) {
                alert("終了日(3行目)の形式が不正です。");
                return false;
            }
            var date7 = get_date(param.date7.year,param.date7.month,param.date7.day);
            var date8 = get_date(param.date8.year,param.date8.month,param.date8.day);
            if (compare_date(date7, date8) > 0) {
                alert("開始日(3行目)と終了日(3行目)が前後しています。");
                return false;
            }
            var diff_day = Math.floor((date8.getTime() - date7.getTime()) / (1000 * 60 * 60 * 24)) + 1;
            j$("#day_count3").val(diff_day);
        } else {
            j$("#day_count3").val("1");
        }
    }
    
    if (require_date( param.date9.year, param.date9.month, param.date9.day)) {
        if (!validate_date( param.date9.year, param.date9.month, param.date9.day)) {
            alert("開始日(4行目)の形式が不正です。");
            return false;
        }
        if (require_date( param.date10.year, param.date10.month, param.date10.day)) {
            if (!validate_date( param.date10.year, param.date10.month, param.date10.day)) {
                alert("終了日(4行目)の形式が不正です。");
                return false;
            }
            var date9 = get_date(param.date9.year,param.date9.month,param.date9.day);
            var date10 = get_date(param.date10.year,param.date10.month,param.date10.day);
            if (compare_date(date9, date10) > 0) {
                alert("開始日(4行目)と終了日(4行目)が前後しています。");
                return false;
            }
            var diff_day = Math.floor((date10.getTime() - date9.getTime()) / (1000 * 60 * 60 * 24)) + 1;
            j$("#day_count4").val(diff_day);
        } else {
            j$("#day_count4").val("1");
        }
    }
    if (require_date( param.date11.year, param.date11.month, param.date11.day)) {
        if (!validate_date( param.date11.year, param.date11.month, param.date11.day)) {
            alert("開始日(5行目)の形式が不正です。");
            return false;
        }
        if (require_date( param.date12.year, param.date12.month, param.date12.day)) {
            if (!validate_date( param.date12.year, param.date12.month, param.date12.day)) {
                alert("終了日(5行目)の形式が不正です。");
                return false;
            }
            var date11 = get_date(param.date11.year,param.date11.month,param.date11.day);
            var date12 = get_date(param.date12.year,param.date12.month,param.date12.day);
            if (compare_date(date11, date12) > 0) {
                alert("開始日(5行目)と終了日(5行目)が前後しています。");
                return false;
            }
            var diff_day = Math.floor((date12.getTime() - date11.getTime()) / (1000 * 60 * 60 * 24)) + 1;
            j$("#day_count5").val(diff_day);
        } else {
            j$("#day_count5").val("1");
        }
    }
    return true;
}

// 時間を計算
function time_calc() {
    j$("#hour").val("");
    j$("#minute").val("");

    var param = get();
    var start_hour = param.start_time.hour;
    var start_minute = param.start_time.minute;
    var end_hour = param.end_time.hour;
    var end_minute = param.end_time.minute;
    if (!require_time(start_hour, start_minute)) {
        return false;
    }
    if (!require_time(end_hour  , end_minute)) {
        return false;
    }
    var start_date = new Date(2000, 1, 1, start_hour, start_minute, 0);
    var end_date = new Date(2000, 1, 1, end_hour, end_minute, 0);

    if (compare_date(start_date, end_date) > 0) {
        return false;
    }
    var diff_minute = Math.floor((end_date.getTime() - start_date.getTime()) / (1000 * 60));
    j$("#hour").val(Math.floor(diff_minute / 60));
    j$("#minute").val(diff_minute % 60);
}

