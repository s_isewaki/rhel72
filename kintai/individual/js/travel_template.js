j$(document).ready(function() {
    j$("#date_y2, #date_m2, #date_d2, #date_y3, #date_m3, #date_d3").change(function() {
        day_calc();
    });
    day_calc();
});

// 申請時の入力チェック
function input_check() {

    var param = get();
    if (!require(param)) {
        return false;
    }
    if (!validate(param)) {
        return false;
    }
    if (!related_check(param)) {
        return false;
    }

    set_template_title();

    return true;
}

// 必須チェック
function require(param) {

    if (!require_date(
                param.apply_date.year,
                param.apply_date.month,
                param.apply_date.day)) {
                    alert("発令日が入力されていません。");
                    return false;
                }

    if (!param.trip_title) {
        alert("用務が入力されていません。");
        return false;
    }

    if (!param.destination) {
        alert("用務先が入力されていません。");
        return false;
    }

    if (!require_date(
                param.start_date.year,
                param.start_date.month,
                param.start_date.day)) {
                    alert("開始日が入力されていません。");
                    return false;
                }

    if (!require_date(
                param.end_date.year,
                param.end_date.month,
                param.end_date.day)) {
                    alert("終了日が入力されていません。");
                    return false;
                }
    return true;
}

// validateチェック
function validate(param) {

    // 日付の不正をチェック
    if (!validate_date(
                param.apply_date.year,
                param.apply_date.month,
                param.apply_date.day)) {
                    alert("発令日の形式が不正です。");
                    return false;
                }

    if (!validate_date(
                param.start_date.year,
                param.start_date.month,
                param.start_date.day)) {
                    alert("開始日のの形式が不正です。");
                    return false;
                }

    if (!validate_date(
                param.end_date.year,
                param.end_date.month,
                param.end_date.day)) {
                    alert("終了日の形式が不正です。");
                    return false;
                }
    if (param.amount1 && isNaN(param.amount1)) {
        alert("実費は数値を入力してください。");
        return false;
    }
    if (param.amount2 && isNaN(param.amount2)) {
        alert("日当は数値を入力してください。");
        return false;
    }
    return true;
}

function related_check(param) {
    var start_date = get_date(
            param.start_date.year,
            param.start_date.month,
            param.start_date.day
            );
    var end_date = get_date(
            param.end_date.year,
            param.end_date.month,
            param.end_date.day
            );
    // 休暇予定日の整合性
    if (compare_date(start_date, end_date) > 0) {
        alert("開始日と終了日が前後しています。");
        return false;
    }
    return true;
}

// パラメータをまとめて取得
function get() {
    return {
        session:j$("#session").val(),
        apply_id:j$("[name=apply_id]").val(),
        emp_id:j$("#apply_emp_id").val(),
        apply_date:{
            year:j$("#date_y1").val(),
            month:j$("#date_m1").val(),
            day:j$("#date_d1").val()
        },
        start_date:{
            year:j$("#date_y2").val(),
            month:j$("#date_m2").val(),
            day:j$("#date_d2").val()
        },
        end_date:{
            year:j$("#date_y3").val(),
            month:j$("#date_m3").val(),
            day:j$("#date_d3").val()
        },
        trip_title:j$("#trip_title").val(),
        destination:j$("#destination").val(),
        amount1:j$("#amount1").val(),
        amount2:j$("#amount2").val(),
        remark:j$("#remark").val()
    };
}

function day_calc() {

    j$("#day_count").val("");

    var param = get();

    if (!require_date(
                param.start_date.year,
                param.start_date.month,
                param.start_date.day)) {
                    return false;
                }

    if (!require_date(
                param.end_date.year,
                param.end_date.month,
                param.end_date.day)) {
                    return false;
                }

    if (!validate_date(
                param.start_date.year,
                param.start_date.month,
                param.start_date.day)) {
                    return false;
                }

    if (!validate_date(
                param.end_date.year,
                param.end_date.month,
                param.end_date.day)) {
                    return false;
                }

    var start_date = get_date(param.start_date.year,param.start_date.month,param.start_date.day);
    var end_date = get_date(param.end_date.year,param.end_date.month,param.end_date.day)

        if (compare_date(start_date, end_date) > 0) {
            return false;
        }

    var diff_day = Math.floor((end_date.getTime() - start_date.getTime()) / (1000 * 60 * 60 * 24)) + 1;

    if (diff_day > 1) {
        j$("#day_count").val(diff_day);
    } else {
        j$("#day_count").val("");
    }
}

function set_template_title() {

    var title = "旅行申請 ";
    var start_date = j$("#date_y2").val() + "年" + new Number(j$("#date_m2").val()) + "月" + new Number(j$("#date_d2").val()) + "日";
    var end_date = j$("#date_y3").val() + "年" + new Number(j$("#date_m3").val()) + "月" + new Number(j$("#date_d3").val()) + "日";
    title += start_date + "〜" + end_date;

    j$("[name=apply_title]").val(title);
}

