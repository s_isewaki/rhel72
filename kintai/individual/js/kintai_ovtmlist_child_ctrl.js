// コンボボックスを初期化する
function init_combobox(id, data) {
	var option_str = '<option value=\"__value__\">__label__</option>';
	var option = option_str.
			replace(/__value__/, "").
			replace(/__label__/, "") + "\n";

	$.each(data, function() {
		option += option_str.
			replace(/__value__/, this.value).
			replace(/__label__/, this.label) + "\n";
	});
	
	var selector = "[id^=" + id + "]";
	$(selector).empty();
	$(selector).width();
	$(selector).append(option);
}

function set_value(id, value) {
	j$("#" + id).val("");
	if (value) {
		j$("#" + id).val(value);
	}
}

function set_combobox(id, value) {
	$("#" + id).val("");
	if (value) {
		$("#" + id).val(value);
	}
}

function set_chkbox(id, value) {
	$("#" + id).attr("checked", "");
	if (value) {
		$("#" + id).attr("checked", "checked");
	}
}

// 時間有休の入力制御をコントロール
function time_hol_control() {
	if (j$("#reason_id option:selected").attr("time_hol") == "t") {
		j$("#hol_start_hour").attr("disabled", "");
		j$("#hol_start_minute").attr("disabled", "");
		j$("#hol_time_hour").attr("disabled", "");
		j$("#hol_time_minute").attr("disabled", "");
	} else {
		j$("#hol_start_hour").attr("disabled", "disabled");
		j$("#hol_start_minute").attr("disabled", "disabled");
		j$("#hol_time_hour").attr("disabled", "disabled");
		j$("#hol_time_minute").attr("disabled", "disabled");
		j$("#hol_start_hour").val("");
		j$("#hol_start_minute").val("");
		j$("#hol_time_hour").val("");
		j$("#hol_time_minute").val("");
	}
}
function checked(id) {
	return $("#"+id).is(":checked");
}

