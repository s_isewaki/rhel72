var AJAX_CONTROLLER = "kintai/individual/controller/hol2_ajax_controller.php";

j$(document).ready(function() {
    // tooltip
    j$("#summary").mouseover(function() {
        // 事由IDを取得
        var reason_id = j$("#reason").val();
        if (reason_id) {
            j$("#tooltip").empty();
            j$.each(recital_list, function() {
                if (reason_id == this.value) {
                    var recital = this.recital;
                    if (recital) {
                        j$("#tooltip").append(recital);
                        j$("#tooltip").show();
                    }
                }
            });
        }
    }).mouseleave(function() {
        j$("#tooltip").hide();
    });

    // 職員選択の制御
    // 決裁・申請のオプション、代理申請の設定を使用するためコメント化
    //var approved_by_flg = j$("#approved_by_flg").val();
    //if (approved_by_flg == 'f') {
    //    j$("#emp_select_btn").hide();    
    //}
    
    // 種別選択
    j$("[name^=kind_flag]").change(function() {  
        var type = j$(this).val();
        get_reason(type, "");
    });
    
    // 事由選択
    j$("#reason").change(function() {
        var reason = j$(this).val();
        get_pattern(reason, "");
        time_hol_control();
        select_pattern_by_reason(); //20130925
    });
    
    // 休暇期間選択の計算
    j$("#date_y2, #date_m2, #date_d2, #date_y3, #date_m3, #date_d3").change(function() {
        day_calc();
    });
    
    // 日数の計算
    day_calc();
    var type = "";
    if (j$("[name^=kind_flag]:checked").val()) {
        type = j$("[name^=kind_flag]:checked").val();
    } else {
        return;
    }
    
    // コンボボックス作成と復元(事由)
    var reason = j$("#hid_reason").val();
    get_reason(type, reason);
    time_hol_control();
    
    // コンボボックス作成と復元(パターン)
    var pattern = j$("#hid_pattern").val();
    get_pattern(reason, pattern);
});

// 時間有休の入力制御をコントロール
function time_hol_control() {
    if (j$("#reason option:selected").attr("time_hol") == "true") {
        j$("#start_time_hour").attr("disabled", "");
        j$("#start_time_minute").attr("disabled", "");
        j$("#use_time_hour").attr("disabled", "");
        j$("#use_time_minute").attr("disabled", "");
    } else {
        j$("#start_time_hour").attr("disabled", "disabled");
        j$("#start_time_minute").attr("disabled", "disabled");
        j$("#use_time_hour").attr("disabled", "disabled");
        j$("#use_time_minute").attr("disabled", "disabled");
    }
}

// 事由を取得
function get_reason(type, value) {

    j$("#pattern").empty();
    j$("#pattern").attr("disabled", "disabled");

    if (!type) { 
        return; 
    };

    var data = {
        controller:AJAX_CONTROLLER,
        method:"get_reason_list",
        auth:7,
        session:j$("#session").val(),
        kind_flg:type,
        t_hol_type:j$("#t_hol_type").val()
    }

    ajaxSend("kintai_ajax_dispatcher.php", data, 
            function (res) {
                j$("#reason").empty();
                j$("#reason").append(create_option(res.data));
                j$("#reason").width();
                if (j$("#reason").children().length > 1) {
                    if (value) {
                        j$("#reason").val(value);
                    }
                }
                recital_list = res.data;
            }
            );
}
var recital_list;
// <option>作成
function create_option(res) {
    var option_str = '<option value=\"__value__\" __option__>__label__</option>';
    var option = option_str.
        replace(/__value__/, "").
        replace(/__label__/, "").
        replace(/__option__/, "") + "\n";

    j$.each(res, function() {
        option += option_str.
        replace(/__value__/, this.value).
        replace(/__label__/, this.label) + "\n";
    if (this.time_hol_flag == "t") { // 時間有休フラグ
        option = option.replace(/__option__/, "time_hol=\"true\"");
    } else {
        option = option.replace(/__option__/, "");
    }
    });
    return option;
}

// 勤務パターンを取得
function get_pattern(reason_id, value) {

    j$("#pattern").empty();
    j$("#pattern").attr("disabled", "disabled");

    var data = {
        controller:AJAX_CONTROLLER,
        method:"get_pattern_list",
        auth:7,
        session:j$("#session").val(),
        emp_id:j$("#apply_emp_id").val(),
        reason:reason_id
    }

    ajaxSend("kintai_ajax_dispatcher.php", data, 
            function (res) {
                j$("#pattern").empty();
                j$("#pattern").append(create_option(res.data));
                j$("#pattern").width();
                if (j$("#pattern").children().length > 1) {
                    j$("#pattern").removeAttr("disabled");
                    if (value) {
                        j$("#pattern").val(value);
                    }
                } else {
                    j$("#pattern").attr("disabled", "disabled");
                }
            }
            );
}

// 職員選択画面
var childwin;
function openEmployeeListHol(item_id) {
    dx = screen.width;
    dy = screen.top;
    base = 0;
    wx = 720;
    wy = 600;
    var url = './emplist_popup.php';
    url += '?session=' + j$("[name=session]").val();
    url += '&emp_id=' + j$("[name=apply_emp_id]").val();
    url += '&item_id='+item_id;
    url += '&callback_func=add_target_list_hol';
    childwin = window.open(url, 'emplistpopup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
    childwin.focus();
}

// 職員選択画面から呼ばれる
function add_target_list_hol(item_id, select_emp_id, name) {
    var list = select_emp_id.split(",");
    if (list.length > 1) {
        alert("複数の職員が選択されています。");
        return false;
    }

    var data = {
        controller:AJAX_CONTROLLER,
        method:"get_emp_info",
        auth:7,
        session:j$("#session").val(),
        emp_id:select_emp_id
    }

    ajaxSend("kintai_ajax_dispatcher.php", data, 
            function (res) {
                // 職員ID更新
                j$("#apply_emp_id").val(res.data.emp_id);
                j$("#span_emp_id").empty();
                j$("#span_emp_id").append(res.data.emp_personal_id);
                // 名称更新
                j$("#span_emp_name").empty();
                j$("#span_emp_name").append(res.data.emp_lt_nm.escapeHTML() + "　" + res.data.emp_ft_nm.escapeHTML());
                // 職種
                j$("#span_emp_job").empty();
                j$("#span_emp_job").append(res.data.job_nm.escapeHTML());
                // 雇用、勤務形態
                j$("#duty_form").empty();
                j$("#duty_form").append(res.data.duty_form_name.escapeHTML());
                // 給与支給区分
                j$("#salary_type").empty();
                j$("#salary_type").append(res.data.salary_type_name.escapeHTML());
                // 所属
                j$("#belong_class").empty();
                j$("#belong_class").append(res.data.organization.escapeHTML());
                // 年休残
                j$("#paid_hol_remain").empty();
                j$("#paid_hol_remain").append(res.data.paid_hol_remain.escapeHTML());
            }
    );
    childwin.close();
}

// 申請時の入力チェック
function input_check() {

    var param = get();
    if (!require(param)) {
        return false;
    }
    if (!validate(param)) {
        return false;
    }
    if (!related_check(param)) {
        return false;
    }
    day_calc();

    set_template_title();

    return true;
}

// 必須チェック
function require(param) {

    if (!require_date(
                param.apply_date.year,
                param.apply_date.month,
                param.apply_date.day)) {
                    alert("申請日が入力されていません。");
                    return false;
                }

    if (!param.kind_flag) {
        alert("休暇種類が入力されていません。");
        return false;
    }

    if (!param.reason) {
        alert("休暇事由が入力されていません。");
        return false;
    }

    if (!j$("#pattern").attr("disabled") && !param.pattern) {
        alert("勤務パターンが入力されていません。");
        return false;
    }

    if (!require_date(
                param.start_date.year,
                param.start_date.month,
                param.start_date.day)) {
                    alert("休暇取得予定日(開始)が入力されていません。");
                    return false;
                }

    if (!require_date(
                param.end_date.year,
                param.end_date.month,
                param.end_date.day)) {
                    alert("休暇取得予定日(終了)が入力されていません。");
                    return false;
                }
    return true;
}

// validateチェック
function validate(param) {

    // 日付の不正をチェック
    if (!validate_date(
                param.apply_date.year,
                param.apply_date.month,
                param.apply_date.day)) {
                    alert("申請日の形式が不正です。");
                    return false;
                }

    if (!validate_date(
                param.start_date.year,
                param.start_date.month,
                param.start_date.day)) {
                    alert("休暇取得予定日(開始)の形式が不正です。");
                    return false;
                }

    if (!validate_date(
                param.end_date.year,
                param.end_date.month,
                param.end_date.day)) {
                    alert("休暇取得予定日(終了)の形式が不正です。");
                    return false;
                }
    return true;
}

function related_check(param) {
    // 日付をdateに変換
    var apply_date = get_date(
            param.apply_date.year,
            param.apply_date.month,
            param.apply_date.day
            );
    var start_date = get_date(
            param.start_date.year,
            param.start_date.month,
            param.start_date.day
            );
    var end_date = get_date(
            param.end_date.year,
            param.end_date.month,
            param.end_date.day
            );

    // 休暇予定日の整合性
    if (compare_date(start_date, end_date) > 0) {
        alert("休暇日が不正です。");
        return false;
    }

    // 休暇時間の必須チェック
    if (compare_date(start_date, end_date) == 0) {
        if ( 
                !(
                    ( // 全て空はOK(1日分の休暇申請)
                      !param.start_time.hour &&
                      !param.start_time.minute &&
                      !param.use_time.hour &&
                      !param.use_time.minute
                    ) || 
                    ( // 全て入力は(時間単位の申請)
                      param.start_time.hour &&
                      param.start_time.minute &&
                      param.use_time.hour &&
                      param.use_time.minute
                    )
                 )
           ) {
               alert("休暇時間が不正です。");
               return false;
           }	
    } else {
        if (
                param.start_time.hour ||
                param.start_time.minute ||
                param.use_time.hour ||
                param.use_time.minute) {
                    alert("時間単位に申請できるのは１日分の申請に限ります。");
                    return false;
                }
    }

    // 申請済休暇日の存在チェック
    var data = {
        controller:AJAX_CONTROLLER,
        method:"apply_check",
        auth:7,
        session:param.session,
        params:param
        /*
        apply_id:param.apply_id,
        emp_id:param.emp_id,
        start_date_year:param.start_date.year,
        start_date_month:param.start_date.month,
        start_date_day:param.start_date.day,
        end_date_year:param.end_date.year,
        end_date_month:param.end_date.month,
        end_date_day:param.end_date.day
        */
    }

    var ajax = true;
    var continue_flg = true;
    ajaxSend("kintai_ajax_dispatcher.php", data, 
            function (res) {
                if (res.message) {
                    if (res.next) {
                        if (!confirm(res.message)) {
                            continue_flg = false;
                        }
                    } else {
                        alert(res.message);
                        continue_flg = false;
                    }
                }
            }
            );

    if (!continue_flg) {
        return false;
    }

    return ajax;
}

// パラメータをまとめて取得
function get() {
    return {
        session:j$("#session").val(),
        apply_id:j$("[name=apply_id]").val(),
        emp_id:j$("#apply_emp_id").val(),
        apply_date:{
            year:j$("#date_y1").val(),
            month:j$("#date_m1").val(),
            day:j$("#date_d1").val()
        },
        kind_flag:j$("[name^=kind_flag]:checked").val(),
        reason:j$("#reason").val(),
        pattern:j$("#pattern").val(),
        remark:j$("#remark").val(),
        start_date:{
            year:j$("#date_y2").val(),
            month:j$("#date_m2").val(),
            day:j$("#date_d2").val()
        },
        end_date:{
            year:j$("#date_y3").val(),
            month:j$("#date_m3").val(),
            day:j$("#date_d3").val()
        },
        start_time:{
            hour:j$("#start_time_hour").val(),
            minute:j$("#start_time_minute").val()
        },
        use_time:{
            hour:j$("#use_time_hour").val(),
            minute:j$("#use_time_minute").val()
        },
        time_hol:!j$("#start_time_hour").attr("disabled")
    };
}

function day_calc() {

    j$("#day_count").val("");

    var param = get();

    if (!require_date(
                param.start_date.year,
                param.start_date.month,
                param.start_date.day)) {
                    return false;
                }

    if (!require_date(
                param.end_date.year,
                param.end_date.month,
                param.end_date.day)) {
                    return false;
                }

    if (!validate_date(
                param.start_date.year,
                param.start_date.month,
                param.start_date.day)) {
                    return false;
                }

    if (!validate_date(
                param.end_date.year,
                param.end_date.month,
                param.end_date.day)) {
                    return false;
                }

    var start_date = get_date(param.start_date.year,param.start_date.month,param.start_date.day);
    var end_date = get_date(param.end_date.year,param.end_date.month,param.end_date.day)

        if (compare_date(start_date, end_date) > 0) {
            return false;
        }

    var diff_day = Math.floor((end_date.getTime() - start_date.getTime()) / (1000 * 60 * 60 * 24)) + 1;

	// 2013/09/30 100日以上までカウントできるように修正
    //if (diff_day > 1 && diff_day < 100) {
    if (diff_day > 1) {
        j$("#day_count").val(diff_day);
    } else {
        j$("#day_count").val("");
    }
}

function set_template_title() {

    var name = j$("#span_emp_name").text();

    var start_date = j$("#date_y2").val() + "年" + new Number(j$("#date_m2").val()) + "月" + new Number(j$("#date_d2").val()) + "日";
    var end_date = j$("#date_y3").val() + "年" + new Number(j$("#date_m3").val()) + "月" + new Number(j$("#date_d3").val()) + "日";

    var day_count = j$("#day_count").val();

    var reason = j$("#reason option:selected").text();

    var user_info = get_user_info(j$("#session").val(), j$("#apply_emp_id").val(), 7);
    
    var title;
    
    title = user_info.organization;
    title += " ";
    title += name;
    title += " ";
    title += reason;
    title += " ";

    if (day_count && day_count > 1) {
        title += start_date + "〜" + end_date;
    } else {
        if (j$("#start_time_hour").val()) {
            var start_time = new Number(j$("#start_time_hour").val()) + ":" + j$("#start_time_minute").val() + "から";
            var use_time = new Number(j$("#use_time_hour").val()) + "時間" + new Number(j$("#use_time_minute").val()) + "分";
            title += start_date + " " + start_time + use_time;
        } else {
            title += start_date;
        }
    }

    j$("[name=apply_title]").val(get_limit(title, 80));
}

//年休簿、休暇簿
function openHolList(item_id) {
    dx = screen.width;
    dy = screen.top;
    base = 0;
    wx = 720;
    wy = 600;
    if (item_id == 1) {
        var url = './kintai_vacation_paid_holiday.php';
    }
    else {
        var url = './kintai_vacation.php';
    }
    
    url += '?session=' + j$("[name=session]").val();
    url += '&selEmpID=' + j$("[name=apply_emp_id]").val();
    url += '&umode=apply';
    childwin = window.open(url, 'paidhollistpopup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
    childwin.focus();
}

//事由と同じ勤務パターンに設定する
function select_pattern_by_reason() {
    var reason_text = j$("#reason option:selected").text();
    var options = document.getElementById('pattern').options;
    for (var i=0; i<options.length; i++) {
        if (options[i].text === reason_text) {
            options[i].selected = true;
            break;
        }
    }
}

//休暇を受ける日を設定する
function set_hol_date(hol_date, item_id) {
	var date_array = hol_date.split("/");

	if (item_id == 1) {
		j$("#date_y2").val(new Number(date_array[0]));
		j$("#date_m2").val(new Number(date_array[1]));
		j$("#date_d2").val(new Number(date_array[2]));
	}
	else {
		j$("#date_y3").val(new Number(date_array[0]));
		j$("#date_m3").val(new Number(date_array[1]));
		j$("#date_d3").val(new Number(date_array[2]));
	}

}
