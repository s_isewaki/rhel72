// onload処理
j$(document).ready(function() {
	// 勤務実績や残業時間を取得して表示
	init_display_ajax();
});

function calc() {
    ovtm_calc(false);
}

function ovtm_calc(calc_flag) {
	var param = get_param();
    if (!calc_check(param)) {
		return false;
	}
	// 超過時間計算
    return overtime_calc(param, calc_flag);
}

function input_check() {

    // 超過時間計算
    if (!ovtm_calc(true)) {
        return false;
    }

    if (!confirm("申請もしくは更新します。宜しいですか？")) {
        return false;
    }

    set_template_title();
    return true;
}

function calc_check(param) {

    var atdptn_nm = j$("#atdptn_nm").text();
    if (atdptn_nm.length == 0) {
        alert("勤務実績が存在しません。");
        return false;
    }

	// 申請日必須チェック
	if (!require_date(
				param.apply_date.year,
				param.apply_date.month,
				param.apply_date.day)) {
		alert("申請日が入力されていません。");
		return false;
	}

	// 時間項目のチェック
	if (!validate(param)) {
		return false;
	}
	return true;
}

// 勤務実績一覧で日付を選択した場合に呼ばれる
function set_rslt_date(rslt_date) {
	var date_array = rslt_date.split("/");

	j$("#rslt_year").val(new Number(date_array[0]));
	j$("#rslt_month").val(new Number(date_array[1]));
	j$("#rslt_day").val(new Number(date_array[2]));

	init_display_ajax(); // 表示を更新
}

function get_param() {
	return {
		apply_emp_id:j$("#apply_emp_id").val(),
		current_user_id:j$("#current_user_id").val(),

		apply_date: { // 申請日
			year:j$("#date_y1").val(),
			month:j$("#date_m1").val(),
			day:j$("#date_d1").val()
		},
		rslt_date:{ // 実施日
			year:j$("#rslt_year").val(),
			month:j$("#rslt_month").val(),
			day:j$("#rslt_day").val()
		},
		rslt:[
			{
				start_hour:j$("#rslt_start_hour1").val(),
				start_minute:j$("#rslt_start_minute1").val(),
				start_next_flag:j$("#rslt_start_next_flag1").attr("checked"),
				end_hour:j$("#rslt_end_hour1").val(),
				end_minute:j$("#rslt_end_minute1").val(),
				end_next_flag:j$("#rslt_end_next_flag1").attr("checked"),
				rest_hour:j$("#rslt_rest_hour1").val(),
				rest_minute:j$("#rslt_rest_minute1").val(),
				ovtmrsn_id:j$("#rslt_ovtmrsn_id1").val(),
				call_flag:j$("#rslt_call_flag1").val(),
                ovtmrsn_txt:j$("#rslt_ovtmrsn_txt1").val()
			},
			{
				start_hour:j$("#rslt_start_hour2").val(),
				start_minute:j$("#rslt_start_minute2").val(),
				start_next_flag:j$("#rslt_start_next_flag2").attr("checked"),
				end_hour:j$("#rslt_end_hour2").val(),
				end_minute:j$("#rslt_end_minute2").val(),
				end_next_flag:j$("#rslt_end_next_flag2").attr("checked"),
				rest_hour:j$("#rslt_rest_hour2").val(),
				rest_minute:j$("#rslt_rest_minute2").val(),
				ovtmrsn_id:j$("#rslt_ovtmrsn_id2").val(),
				call_flag:j$("#rslt_call_flag2").val(),
                ovtmrsn_txt:j$("#rslt_ovtmrsn_txt2").val()
			},
			{
				start_hour:j$("#rslt_start_hour3").val(),
				start_minute:j$("#rslt_start_minute3").val(),
				start_next_flag:j$("#rslt_start_next_flag3").attr("checked"),
				end_hour:j$("#rslt_end_hour3").val(),
				end_minute:j$("#rslt_end_minute3").val(),
				end_next_flag:j$("#rslt_end_next_flag3").attr("checked"),
				rest_hour:j$("#rslt_rest_hour3").val(),
				rest_minute:j$("#rslt_rest_minute3").val(),
				ovtmrsn_id:j$("#rslt_ovtmrsn_id3").val(),
				call_flag:j$("#rslt_call_flag3").val(),
                ovtmrsn_txt:j$("#rslt_ovtmrsn_txt2").val()
			}
		]
	};
}
