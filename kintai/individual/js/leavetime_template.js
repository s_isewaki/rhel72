var AJAX_CONTROLLER = "kintai/individual/controller/late_ajax_controller.php";
// onload処理
j$(document).ready(function() {

    // 対象日選択
	j$("#date_y2, #date_m2, #date_d2").change(function() {
		calc_late_time();
	});
	j$("#start_time_hour, #start_time_minute").change(function() {
		calc_late_time();
	});
    	
    
    // 対象日選択
    j$("#date_y3, #date_m3, #date_d3").change(function() {
    	calc_late_time();
    });
    j$("#end_time_hour, #end_time_minute").change(function() {
    	calc_late_time();
    });
    
    calc_late_time();
});
// 職員選択画面
var childwin;
function openEmployeeListHol(item_id) {
    dx = screen.width;
    dy = screen.top;
    base = 0;
    wx = 720;
    wy = 600;
    var url = './emplist_popup.php';
    url += '?session=' + j$("[name=session]").val();
    url += '&emp_id=' + j$("[name=apply_emp_id]").val();
    url += '&item_id='+item_id;
    url += '&callback_func=add_target_list_hol';
    childwin = window.open(url, 'emplistpopup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
    childwin.focus();
}

// 職員選択画面から呼ばれる
function add_target_list_hol(item_id, select_emp_id, name) {
    var list = select_emp_id.split(",");
    if (list.length > 1) {
        alert("複数の職員が選択されています。");
        return false;
    }

    var data = {
        controller:AJAX_CONTROLLER,
        method:"get_emp_info",
        auth:7,
        session:j$("#session").val(),
        emp_id:select_emp_id
    };

    ajaxSend("kintai_ajax_dispatcher.php", data, 
            function (res) {
                // 職員ID更新
                j$("#apply_emp_id").val(res.data.emp_id);
                j$("#span_emp_id").empty();
                j$("#span_emp_id").append(res.data.emp_personal_id);
                // 名称更新
                j$("#span_emp_name").empty();
                j$("#span_emp_name").append(res.data.emp_lt_nm.escapeHTML() + "　" + res.data.emp_ft_nm.escapeHTML());
                // 職種
                j$("#span_emp_job").empty();
                j$("#span_emp_job").append(res.data.job_nm.escapeHTML());
                // 雇用、勤務形態
                j$("#duty_form").empty();
                j$("#duty_form").append(res.data.duty_form_name.escapeHTML());
                // 給与支給区分
                j$("#salary_type").empty();
                j$("#salary_type").append(res.data.salary_type_name.escapeHTML());
                // 所属
                j$("#belong_class").empty();
                j$("#belong_class").append(res.data.organization.escapeHTML());
            }
    );
    childwin.close();
}

// 申請時の入力チェック
function input_check() {

    var param = get();
    if (!require(param)) {
        return false;
    }
    if (!validate(param)) {
        return false;
    }
    if (!related_check(param)) {
        return false;
    }

    set_template_title();

    return true;
}

// 必須チェック
function require(param) {
	
	var notice_msg = "";
	
    if (!require_date(
                param.apply_date.year,
                param.apply_date.month,
                param.apply_date.day)) {
    				notice_msg += "申請日が選択されていません。" + ",";
                }

    if (!require_date(
                param.start_date.year,
                param.start_date.month,
                param.start_date.day)) {
    				notice_msg += "開始日が指定されていません。" + ",";
                }

    if (!require_time(
                param.start_time.hour,
                param.start_time.minute)) {
    				notice_msg += "開始時刻が指定されていません。" + ",";
                }

    if (!require_date(
                param.end_date.year,
                param.end_date.month,
                param.end_date.day)) {
    				notice_msg += "終了日が指定されていません。" + ",";
                }

    if (!require_time(
                param.end_time.hour,
                param.end_time.minute)) {
    				notice_msg += "終了時刻が指定されていません。" + ",";
               }
    
    if (j$("#late_early_type").val() == "new"){
    	if (!param.transfer) {notice_msg += "振替区分が選択されていません。" + ",";}
    	
    	if (param.transfer == "1" && Number(param.apply_time) > Number(param.hol_minutes)){
    		notice_msg += "年次有給休暇残日数がマイナスになるため、年休振替できません。";
    	}
    }
    
    if (notice_msg != ""){
    	var msg = notice_msg.replace(/,/g, '\n');
    	alert(msg);
    	return false;
    }
    
    return true;
}

// validateチェック
function validate(param) {

    // 日付の不正をチェック
    if (!validate_date(
                param.apply_date.year,
                param.apply_date.month,
                param.apply_date.day)) {
                    alert("申請日の形式が不正です。");
                    return false;
                }

    if (!validate_date(
                param.start_date.year,
                param.start_date.month,
                param.start_date.day)) {
                    alert("開始日の形式が不正です。");
                    return false;
                }
    if (!validate_date(
                param.end_date.year,
                param.end_date.month,
                param.end_date.day)) {
                    alert("終了日の形式が不正です。");
                    return false;
                }
    return true;
}

function related_check(param) {
    
    var tmp_start = new Date(param.start_date.year, param.start_date.month, param.start_date.day, param.start_time.hour, param.start_time.minute);
    var tmp_end   = new Date(param.end_date.year, param.end_date.month, param.end_date.day, param.end_time.hour, param.end_time.minute);
    
    // 対象日の整合性
    if (compare_date(tmp_start, tmp_end) > 0) {
        alert("開始日時、終了日時を正しく指定してください。");
        return false;
    }
    
    if (Number(param.apply_time) >= Number(param.specified_time)){
    	alert("所定労働時間を超えているため、申請できません。");
    	return false;
    }
    
    if (Number(param.apply_time) > 540){
    	alert("申請する時間数が9時間を超えているため、申請できません。");
    	return false;
    }
    return true;
}

// パラメータをまとめて取得
function get() {
    return {
        session:j$("#session").val(),
        apply_id:j$("[name=apply_id]").val(),
        emp_id:j$("#apply_emp_id").val(),
        apply_date:{
            year:j$("#date_y1").val(),
            month:j$("#date_m1").val(),
            day:j$("#date_d1").val()
        },
        kind_flag:j$("[name^=kind_flag]:checked").val(),
        start_date:{
            year:j$("#date_y2").val(),
            month:j$("#date_m2").val(),
            day:j$("#date_d2").val()
        },
        start_time:{
            hour:j$("#start_time_hour").val(),
            minute:j$("#start_time_minute").val()
        },
        end_date:{
            year:j$("#date_y3").val(),
            month:j$("#date_m3").val(),
            day:j$("#date_d3").val()
        },
        end_time:{
            hour:j$("#end_time_hour").val(),
            minute:j$("#end_time_minute").val()
        },
        reason_id:j$("#reason_id").val(),
        hol_minutes:j$("#hol_minutes").val(),
        apply_time:j$("#apply_time").val(),
        specified_time:j$("#specified_time").val(),
        apply_time:j$("#apply_time").val(),
        transfer:j$("[name^=transfer]:checked").val()
    };
}

//表題設定
function set_template_title() {

    var name = j$("#span_emp_name").text();
        
    var kind = "外出(中抜け)";
    var late_date = j$("#date_y2").val() + "年" + new Number(j$("#date_m2").val()) + "月" + new Number(j$("#date_d2").val()) + "日";
    var late_time = new Number(j$("#start_time_hour").val()) + ":" + j$("#start_time_minute").val();
    var reason = j$("#reason_id option:selected").text();

    var user_info = get_user_info(j$("#session").val(), j$("#apply_emp_id").val(), 7);
    
    var title;
    
    title = user_info.organization;
    title += " ";
    title += name;
    title += " ";
    title += kind;
    title += " ";

    title += late_date + " " + late_time;
    title += " ";
    title += reason;
    
    if (j$("#late_early_type").val() == "new"){
    	title += " ";
    	if (j$("[name^=transfer]:checked").val() == "1") {
    		title += "年休振替";
    	}else{
    		title += "欠勤振替";
    	}
    }
    
    j$("[name=apply_title]").val(get_limit(title, 80));
}

//年休簿、休暇簿
function openHolList(item_id) {
    dx = screen.width;
    dy = screen.top;
    base = 0;
    wx = 720;
    wy = 600;
    if (item_id == 1) {
        var url = './kintai_vacation_paid_holiday.php';
    }
    else {
        var url = './kintai_vacation.php';
    }
    
    url += '?session=' + j$("[name=session]").val();
    url += '&selEmpID=' + j$("[name=apply_emp_id]").val();
    url += '&umode=apply';
    childwin = window.open(url, 'paidhollistpopup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
    childwin.focus();
}

//休暇を受ける日を設定する
function set_hol_date(hol_date, item_id) {
	var date_array = hol_date.split("/");

	if (item_id == 1) {
		j$("#date_y2").val(new Number(date_array[0]));
		j$("#date_m2").val(new Number(date_array[1]));
		j$("#date_d2").val(new Number(date_array[2]));
		j$("#date_y3").val(new Number(date_array[0]));
		j$("#date_m3").val(new Number(date_array[1]));
		j$("#date_d3").val(new Number(date_array[2]));
	}
	else {
		j$("#date_y3").val(new Number(date_array[0]));
		j$("#date_m3").val(new Number(date_array[1]));
		j$("#date_d3").val(new Number(date_array[2]));
	}
	
	calc_late_time();
}

//時間計算
function calc_late_time(){
	var start_y = j$("#date_y2").val();
	var start_m = j$("#date_m2").val();
	var start_d = j$("#date_d2").val();
	var start_h = j$("#start_time_hour").val();
	var start_t = j$("#start_time_minute").val();
	
	var end_y = j$("#date_y3").val();
	var end_m = j$("#date_m3").val();
	var end_d = j$("#date_d3").val();
	var end_h = j$("#end_time_hour").val();
	var end_t = j$("#end_time_minute").val();
	
	var result_h = "0";
	var result_t = "00";
	var diff_minute = "";
	
	if (require_date(start_y, start_m, start_d) && require_time(start_h, start_t)){
		if (require_date(end_y, end_m, end_d) && require_time(end_h, end_t)){
			var start_date = new Date(start_y, start_m, start_d, start_h, start_t);
			var end_date = new Date(end_y, end_m, end_d, end_h, end_t);
			
			if (compare_date(end_date, start_date) > 0){
				diff_minute = Math.floor((end_date.getTime() - start_date.getTime()) / (1000 * 60));
				result_h = Math.floor(diff_minute / 60);
				result_t = diff_minute % 60;
			}
		}
	}

	j$("#apply_time").val(diff_minute);
	j$("#diff_hour").val(result_h);
	j$("#diff_minutes").val(result_t);
	
	j$("#span_diff_time").empty();
	j$("#span_diff_time").append(result_h + " 時間 " + result_t + " 分 ");
}
