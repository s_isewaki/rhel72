function set_combobox(id, value) {
	j$("#" + id).val("");
	if (value) {
		j$("#" + id).val(value);
	}
}

function set_chkbox(id, value) {
	j$("#" + id).attr("checked", "");
	if (value) {
		j$("#" + id).attr("checked", "checked");
	}
}

function set_value(id, value) {
	j$("#" + id).val("");
	if (value) {
		j$("#" + id).val(value);
	}
}

function checked(id) {
	return j$("#"+id).is(":checked");
}

function set_template_title() {
	// 実施日
	var date = j$("#rslt_year").val() + "年" + j$("#rslt_month").val() + "月" + j$("#rslt_day").val() + "日"; 
	// 時刻
	var rslt_start_hour = j$("#rslt_start_hour1").val();
	var rslt_end_hour = j$("#rslt_end_hour1").val();
	var rslt_start_minute = j$("#rslt_start_minute1").val();
	var rslt_end_minute = j$("#rslt_end_minute1").val();

	var start = "";
	var end = "";
	if (rslt_start_hour && rslt_end_hour) {
		start = rslt_start_hour + ":" + rslt_start_minute;
		end   = rslt_end_hour   + ":" + rslt_end_minute;
	}

	var user_info = get_user_info(j$("#session").val(), j$("#apply_emp_id").val(), 7);
	
	// 事由
	var rsn_str = j$("select#rslt_ovtmrsn_id1 option:selected").text();

	var title = user_info.organization + " " + date + " " + start + " - " + end + " " + rsn_str;
	j$("[name=apply_title]").val(get_limit(title, 80));
}
