// 申請時か否か
function is_apply() {
	return j$("#apply_emp_id").val() == j$("#current_user_id").val();
}

// チェックするか否か
function is_check(line) {
	if (!line.start_hour && !line.start_minute && !line.end_hour && !line.end_minute) {
		return false; // すべて未入力は非チェック
	} else {
		return true; // どれか入力されていたらチェック対象とする。
	}
}

// すべて入力済でtrueを返す
function require_time(line) {
	if (line.start_hour && line.start_minute && line.end_hour && line.end_minute) {
		return true;
	} else {
		return false;
	} 
}

// 翌日フラグを加味した日付を取得
function get_date_time(hour, minute, tommorow, year, month, day) {
	if (year == undefined) year = 2000;
	if (month == undefined) month = 0;
	if (day   == undefined) day = 1;
	if (tommorow) {
		day++;
	} 
	return new Date(year, month, day, hour, minute, 0);
}

// 翌日フラグを加味した日付の前後をチェック
function validate_time(line) {
	var start = get_date_time(line.start_hour, line.start_minute, line.start_next_flag);
	var end = get_date_time(line.end_hour, line.end_minute, line.end_next_flag);
	
	// 休憩時間(msec)
	var rest_time = new Number(line.rest_hour) * 60 * 60 * 1000;
	rest_time += new Number(line.rest_minute) * 60 * 1000;
	// 休憩時間が長すぎる場合はエラー
	var time = end.getTime() - start.getTime() - rest_time;
	if (time <= 0) {
		return false;
	}
	
	// 整合性チェック
	if (compare_date(start, end) >= 0) {
		return false;
	}
	return true;
}

// 日付の妥当性などをチェック
function validate(param) {

	// 日付の不正をチェック
	if (!validate_date(
				param.apply_date.year,
				param.apply_date.month,
				param.apply_date.day)) {
		alert("申請日の形式が不正です。");
		return false;
	}

	// 従事
	if (!check_line_time(param.rslt)) {
		alert("時間外従事時間が不正です。");
		return false; 
	}
	return true;
}

// 超過勤務時間のチェック(ブロックごと)
function check_line_time(lines) {
	
	var error = false;
	j$.each(lines, 
			function() {
				// チェック対象行とするか
				if (is_check(this)) {
					// 必須チェック
					if (!require_time(this)) {
						error = true;
						return false;
					}
					// 整合性
					if (!validate_time(this)) {
						error = true;
						return false;
					}
				}
			}
			);
	return !error;
}
