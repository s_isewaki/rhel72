var AJAX_CONTROLLER = "kintai/individual/controller/hol_ajax_controller.php";

j$(document).ready(function() {
    
    // 種別選択
    j$("[name^=kind_flag]").change(function() {  
        input_div_control(j$("[name^=kind_flag]:checked").val());
    });
    
    // 勤務パターン選択
    j$("#atdptn").change(function() {
        change_atdptn(j$(this).val());
    });
    
    j$("#start_time_hour, #start_time_minute, #end_time_hour, #end_time_minute").change(function() {
        time_calc();
    });
    init();
});

function init() {
    // 初回申請は空にしてあげる
    j$("#date_y2").val(j$("#hid_date_y2").val());
    j$("#date_m2").val(j$("#hid_date_m2").val());
    j$("#date_d2").val(j$("#hid_date_d2").val());
    j$("#date_y5").val(j$("#hid_date_y5").val());
    j$("#date_m5").val(j$("#hid_date_m5").val());
    j$("#date_d5").val(j$("#hid_date_d5").val());
    j$("#date_y6").val(j$("#hid_date_y6").val());
    j$("#date_m6").val(j$("#hid_date_m6").val());
    j$("#date_d6").val(j$("#hid_date_d6").val());
    j$("#date_y7").val(j$("#hid_date_y7").val());
    j$("#date_m7").val(j$("#hid_date_m7").val());
    j$("#date_d7").val(j$("#hid_date_d7").val());
    j$("#date_y8").val(j$("#hid_date_y8").val());
    j$("#date_m8").val(j$("#hid_date_m8").val());
    j$("#date_d8").val(j$("#hid_date_d8").val());
    j$("#date_y9").val(j$("#hid_date_y9").val());
    j$("#date_m9").val(j$("#hid_date_m9").val());
    j$("#date_d9").val(j$("#hid_date_d9").val());
    j$("#date_y10").val(j$("#hid_date_y10").val());
    j$("#date_m10").val(j$("#hid_date_m10").val());
    j$("#date_d10").val(j$("#hid_date_d10").val());
    j$("#date_y11").val(j$("#hid_date_y11").val());
    j$("#date_m11").val(j$("#hid_date_m11").val());
    j$("#date_d11").val(j$("#hid_date_d11").val());
    j$("#date_y12").val(j$("#hid_date_y12").val());
    j$("#date_m12").val(j$("#hid_date_m12").val());
    j$("#date_d12").val(j$("#hid_date_d12").val());
    
    // 日数の計算
    day_calc();
    time_calc();
    j$("#reason").attr("disabled", j$("#reason_ctrl").val());
    j$("#atdptn").attr("disabled", j$("#atdptn_ctrl").val());
}
// 区別変更時のコントロール
function input_div_control(kind_flg) {
    var input_type = get_input_type(kind_flg);
    switch(input_type) {
    case "TYPE1":
        j$("#input_type1").show();
        j$("#input_type2").hide();
        j$("#input_type3").hide();
        break;
    case "TYPE2":
        j$("#input_type1").hide();
        j$("#input_type2").show();
        j$("#input_type3").hide();
        
        var atdptn_ctrl = false;
        var reason_ctrl = false;
        if (kind_flg == 7) {
            init_atdptn(kind_flg);
            init_reason();
            atdptn_ctrl = true;
            reason_ctrl = true;   
        }
        if (kind_flg == 2) {
            init_atdptn(kind_flg);
            atdptn_ctrl = true;
            reason_ctrl = false;   
        }

        if (atdptn_ctrl) {
            j$("#atdptn").attr("disabled", "");
        } else {
            j$("#atdptn").attr("disabled", "disabled");
        }
        
        if (reason_ctrl) {
            j$("#reason").attr("disabled", "");
        } else {
            j$("#reason").attr("disabled", "disabled");
        }
        break;
    case "TYPE3":
        j$("#input_type1").hide();
        j$("#input_type2").hide();
        j$("#input_type3").show();
        break;
    }
}

// 勤務パターンを取得
function init_atdptn(kind_flg) {
    var data = {
        controller:AJAX_CONTROLLER,
        method:"get_atdptn_list",
        auth:7,
        session:j$("#session").val(),
        type:kind_flg,
        tmcd_group_id:j$("#tmcd_group_id").val()
    }

    ajaxSend("kintai_ajax_dispatcher.php", data, 
            function (res) {
                j$("#atdptn").empty();
                j$("#atdptn").append(create_option(res.data));
                j$("#atdptn").width();
            }
            );

}

function change_atdptn(pattern) {
   
   if (!pattern) {
       j$("#reason").val("");
       return;
   }

   var data = {
        controller:AJAX_CONTROLLER,
        method:"get_reason",
        auth:7,
        session:j$("#session").val(),
        atdptn:pattern,
        tmcd_group_id:j$("#tmcd_group_id").val()
    }

    ajaxSend("kintai_ajax_dispatcher.php", data, 
            function (res) {
                j$("#reason").val(res.data);
            }
            );
}

// 事由を取得
function init_reason() {
    var data = {
        controller:AJAX_CONTROLLER,
        method:"get_reason_list",
        auth:7,
        session:j$("#session").val()
    }

    ajaxSend("kintai_ajax_dispatcher.php", data, 
            function (res) {
                j$("#reason").empty();
                j$("#reason").append(create_option(res.data));
                j$("#reason").width();
            }
            );
}

// <option>作成
function create_option(res) {
    var option_str = '<option value=\"__value__\">__label__</option>';
    var option = option_str.
        replace(/__value__/, "").
        replace(/__label__/, "") + "\n";

    j$.each(res, function() {
        option += option_str.
        replace(/__value__/, this.value).
        replace(/__label__/, this.label) + "\n";
    });
    return option;
}

// パラメータをまとめて取得
function get() {
    return {
        session:j$("#session").val(),
        apply_id:j$("[name=apply_id]").val(),
        emp_id:j$("#apply_emp_id").val(),
        tmcd_group_id:j$("#tmcd_group_id").val(),
        date1:{
            year:j$("#date_y1").val(),
            month:j$("#date_m1").val(),
            day:j$("#date_d1").val()
        },
        date2:{
            year:j$("#date_y2").val(),
            month:j$("#date_m2").val(),
            day:j$("#date_d2").val()
        },
        date3:{
            year:j$("#date_y3").val(),
            month:j$("#date_m3").val(),
            day:j$("#date_d3").val()
        },
        date4:{
            year:j$("#date_y4").val(),
            month:j$("#date_m4").val(),
            day:j$("#date_d4").val()
        },
        date5:{
            year:j$("#date_y5").val(),
            month:j$("#date_m5").val(),
            day:j$("#date_d5").val()
        },
        date6:{
            year:j$("#date_y6").val(),
            month:j$("#date_m6").val(),
            day:j$("#date_d6").val()
        },
        date7:{
            year:j$("#date_y7").val(),
            month:j$("#date_m7").val(),
            day:j$("#date_d7").val()
        },
        date8:{
            year:j$("#date_y8").val(),
            month:j$("#date_m8").val(),
            day:j$("#date_d8").val()
        },
        date9:{
            year:j$("#date_y9").val(),
            month:j$("#date_m9").val(),
            day:j$("#date_d9").val()
        },
        date10:{
            year:j$("#date_y10").val(),
            month:j$("#date_m10").val(),
            day:j$("#date_d10").val()
        },
        date11:{
            year:j$("#date_y11").val(),
            month:j$("#date_m11").val(),
            day:j$("#date_d11").val()
        },
        date12:{
            year:j$("#date_y12").val(),
            month:j$("#date_m12").val(),
            day:j$("#date_d12").val()
        },
        kind_flag:j$("[name^=kind_flag]:checked").val(),
        atdptn:j$("#atdptn").val(),
        reason:j$("#reason").val(),
        remark:j$("#remark").val(),
        start_time:{
            hour:j$("#start_time_hour").val(),
            minute:j$("#start_time_minute").val()
        },
        end_time:{
            hour:j$("#end_time_hour").val(),
            minute:j$("#end_time_minute").val()
        }
    };
}

// テンプレートのタイトルをセット
function set_template_title(kind_flg) {
   
    var title;
    switch(kind_flg) {
    case "1":
        title = "年次有給休暇";
        break;
    case "2":
        title = "半日有休";
        break;
    case "3":
        title = "半公半給";
        break;
    case "4":
        title = "慶弔休暇";
        break;
    case "5":
        title = "欠勤";
        break;
    case "6":
        title = "半公半欠";
        break;
    case "7":
        title = "勤務交代";
        break;
    case "8":
        title = "代休";
        break;
    case "9":
        title = "外出";
        break;
    case "10":
        title = "遅刻";
        break;
    case "11":
        title = "出張";
        break;
    case "12":
        title = "早退";
        break;
    case "13":
        title = "午前出張";
        break;
    case "14":
        title = "午後出張";
        break;
    case "15":
        title = "半日代休";
        break;
    }

    title += " ";
    switch(get_input_type(kind_flg)) {
    case "TYPE1":
        title += j$("#date_y1").val() + "年" + new Number(j$("#date_m1").val()) + "月" + new Number(j$("#date_d1").val()) + "日";
        if (j$("#date_y2").val() && j$("#date_m2").val() && j$("#date_d2").val()) {
            title += "〜" + j$("#date_y2").val() + "年" + new Number(j$("#date_m2").val()) + "月" + new Number(j$("#date_d2").val()) + "日 ";
        }
        break;
    case "TYPE2":
        title += j$("#date_y3").val() + "年" + new Number(j$("#date_m3").val()) + "月" + new Number(j$("#date_d3").val()) + "日 ";
        if (kind_flg == 2 || kind_flg == 7) {
            title += j$("#atdptn option:selected").text() + " ";
        }
        if (kind_flg == 7) {
            title += j$("#reason option:selected").text() + " ";
        }
        break;
    case "TYPE3":
        title += j$("#date_y4").val() + "年" + new Number(j$("#date_m4").val()) + "月" + new Number(j$("#date_d4").val()) + "日 ";
        title += new Number(j$("#start_time_hour").val()) + ":" + j$("#start_time_minute").val() + "から";
        if (j$("#hour").val() != "") {
            title += new Number(j$("#hour").val()) + "時間" + new Number(j$("#minute").val()) + "分";
        }
        break;
    }
    j$("[name=apply_title]").val(title);
}

