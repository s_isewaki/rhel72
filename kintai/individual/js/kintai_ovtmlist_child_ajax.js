var AJAX_CONTROLLER = "kintai/individual/controller/ovtmlist_child_ajax_controller.php";

// 画面初期化時のデータ取得等
function init_display() {
	// 実施日を取得
	var p = get_param();
	
	var data = {
		controller : AJAX_CONTROLLER,
		method     : "get_ovtm_data",
		auth       : 5,
		session    : p.session,
		emp_id     : p.emp_id,
		date       : p.rslt_date
	}
	ajaxSend("kintai_ajax_dispatcher.php", data, 
		function (res) {
			set_initial_data(res);
			time_hol_control();
		}
	);
}

// 勤務グループ変更時
function change_tmcd_group() {
	
	var p = get_param();

	var data = {
		controller    : AJAX_CONTROLLER,
		method        : "change_tmcd_group",
		auth          : 5,
		session       : p.session,
		tmcd_group_id : p.tmcd_group_id
	}
	ajaxSend("kintai_ajax_dispatcher.php", data, 
		function (res) {
			init_combobox("atdptn_id", res.atdptn_list);
			init_combobox("rslt_ovtmrsn_id", res.ovtmrsn_list);
				// 関連項目を初期化
			$("#atdptn_id").val("");
			$("#fixed_start_time").empty();
			$("#fixed_end_time").empty();
			$("#reason_id").val("");
		}
	);
}

// 勤務実績変更時
function change_atdptn() {
	
	$("#fixed_start_time").empty();
	$("#fixed_end_time").empty();
	$("#reason_id").val("");

	var p = get_param();
	
	if (p.atdptn_id == "") {
		return;
	}

	var data = {
		controller    : AJAX_CONTROLLER,
		method        : "change_atdptn",
		auth          : 5,
		session       : p.session,
		tmcd_group_id : p.tmcd_group_id,
		atdptn_id     : p.atdptn_id,
		emp_id        : p.emp_id,
		date          : p.rslt_date
	}
	ajaxSend("kintai_ajax_dispatcher.php", data, 
		function (res) {
			$("#fixed_start_time").append(res.rslt_info.officehours2_start);
			$("#fixed_end_time").append(res.rslt_info.officehours2_end);
			$("#reason_id").val(res.rslt_info.reason_id);
		}
	);
}

function overtime_calc(param, calc_flag) {

	var check_data = {
		controller : AJAX_CONTROLLER,
		method     : "submit_check",
		auth       : 5,
		session    : param.session,
		form       : param
	}

	var continue_flg = true;
	ajaxSend("kintai_ajax_dispatcher.php", check_data, 
			function (res) {
				if (res.message) {
					if (res.next) {
						if (!confirm(res.message)) {
							continue_flg = false;
						}
					} else {
						alert(res.message);
						continue_flg = false;
					}
				}
			}
	);
	if (!continue_flg) {
		return false;
	}
	var data = {
		controller:AJAX_CONTROLLER,
		method:"ovtm_calc",
		auth:5,
		session:param.session,
		form:param,
        calc_regist: calc_flag
	}

	ajaxSend("kintai_ajax_dispatcher.php", data, 
			function (res) {
				set_ovtm_minute(res);
			}
	);
	
	return true;
}

// 登録用のAjax
function ovtm_regist(p) {
	
	var submit_data = {
		controller : AJAX_CONTROLLER,
		method     : "submit",
		auth       : 5,
		session    : p.session,
		form       : p
	}
	
	ajaxSend("kintai_ajax_dispatcher.php", submit_data,
			function (res) {
				// 親画面を更新
				window.opener.document.frmKintai.submit();
				window.close();
			}
	);
}

// 取得したデータを反映
function set_initial_data(data) {
	// 勤務実績
	if (data.rslt_info == null) {
		alert("勤務実績が存在しません。");
		return;
	}

	// 動的に変更されるコンボボックスを初期化
	init_combobox("atdptn_id", data.atdptn_list);
	init_combobox("rslt_ovtmrsn_id", data.ovtmrsn_list);
	
	// 勤務実績の設定
	set_kintai_rslt(data);

	// 残業時間の設定
	if (data.ovtm_exists) {
		set_ovtm(data);
	}

	// 時間有休の設定
	set_hol_hour(data.hol_hour);

	// 超過勤務時間
	set_ovtm_minute(data);
	// 超過勤務時間計
	//set_ovtm_sum(data);
}

/**
 * 勤務実績の設定
 "rslt_info": {
 "tmcd_group_id":
 "pattern":
 "atdptn_nm":
 "officehours2_start":
 "officehours2_end":
 "reason_id":
 "display_name": 
 "default_name":
 */ 
function set_kintai_rslt(data) {
	$("#tmcd_group_id").val("");
	$("#atdptn_id").val("");
	$("#fixed_start_time").empty();
	$("#fixed_end_time").empty();
	$("#reason_id").val("");

	$("#tmcd_group_id").val(data.rslt_info.tmcd_group_id);
	$("#atdptn_id").val(data.rslt_info.pattern);
	$("#fixed_start_time").append(data.rslt_info.officehours2_start);
	$("#fixed_end_time").append(data.rslt_info.officehours2_end);
	$("#reason_id").val(data.rslt_info.reason_id);

	// 会議等時間
	var meeting_time_start = split_hi(data.rslt_info.meeting_start_time);
	var meeting_time_end   = split_hi(data.rslt_info.meeting_end_time);
	set_combobox("ward_start_hour", meeting_time_start.hour);
	set_combobox("ward_start_minute", meeting_time_start.minute);
	set_combobox("ward_end_hour", meeting_time_end.hour);
	set_combobox("ward_end_minute", meeting_time_end.minute);
}

// 残業時間を設定する
function set_ovtm(data) {
	set_ovtm_result(data.ovtm_info); // 従事時間
}

/*
 * rslt_start_time1": 
 * rslt_start_next_flag1": 
 * rslt_end_time1": 
 * rslt_end_next_flag1": 
 * rslt_rest_time1": 
 * rslt_ovtmrsn_id1": 
 * rslt_start_time2": 
 * rslt_start_next_flag2": 
 * rslt_end_time2": 
 * rslt_end_next_flag2": 
 * rslt_rest_time2": 
 * rslt_ovtmrsn_id2": 
 * rslt_start_time3": 
 * rslt_start_next_flag3": 
 * rslt_end_time3": 
 * rslt_end_next_flag3": 
 * rslt_rest_time3": 
 * rslt_ovtmrsn_id3": 
 */
function set_ovtm_result(data) {
	// 1行目
	var sp_start1 = split_hi(data.rslt_start_time1);
	set_combobox("rslt_start_hour1", sp_start1.hour);
	set_combobox("rslt_start_minute1", sp_start1.minute);
	set_chkbox("rslt_start_next_flag1", data.rslt_start_next_flag1);
	var sp_end1 = split_hi(data.rslt_end_time1);
	set_combobox("rslt_end_hour1", sp_end1.hour);
	set_combobox("rslt_end_minute1", sp_end1.minute);
	set_chkbox("rslt_end_next_flag1", data.rslt_end_next_flag1);
	var sp_rest1 = split_hi(data.rslt_rest_time1);
	set_combobox("rslt_rest_hour1", sp_rest1.hour);
	set_combobox("rslt_rest_minute1", sp_rest1.minute);
	set_combobox("rslt_ovtmrsn_id1", data.rslt_ovtmrsn_id1);
	set_chkbox("rslt_call_flag1", data.rslt_call_flag1);
	set_value("rslt_ovtmrsn_txt1", data.rslt_ovtmrsn_txt1);

	// 2行目
	var sp_start2 = split_hi(data.rslt_start_time2);
	set_combobox("rslt_start_hour2", sp_start2.hour);
	set_combobox("rslt_start_minute2", sp_start2.minute);
	set_chkbox("rslt_start_next_flag2", data.rslt_start_next_flag2);
	var sp_end2 = split_hi(data.rslt_end_time2);
	set_combobox("rslt_end_hour2", sp_end2.hour);
	set_combobox("rslt_end_minute2", sp_end2.minute);
	set_chkbox("rslt_end_next_flag2", data.rslt_end_next_flag2);
	var sp_rest2 = split_hi(data.rslt_rest_time2);
	set_combobox("rslt_rest_hour2", sp_rest2.hour);
	set_combobox("rslt_rest_minute2", sp_rest2.minute);
	set_combobox("rslt_ovtmrsn_id2", data.rslt_ovtmrsn_id2);
	set_chkbox("rslt_call_flag2", data.rslt_call_flag2);
	set_value("rslt_ovtmrsn_txt2", data.rslt_ovtmrsn_txt2);

	// 3行目
	var sp_start3 = split_hi(data.rslt_start_time3);
	set_combobox("rslt_start_hour3", sp_start3.hour);
	set_combobox("rslt_start_minute3", sp_start3.minute);
	set_chkbox("rslt_start_next_flag3", data.rslt_start_next_flag3);
	var sp_end3 = split_hi(data.rslt_end_time3);
	set_combobox("rslt_end_hour3", sp_end3.hour);
	set_combobox("rslt_end_minute3", sp_end3.minute);
	set_chkbox("rslt_end_next_flag3", data.rslt_end_next_flag3);
	var sp_rest3 = split_hi(data.rslt_rest_time3);
	set_combobox("rslt_rest_hour3", sp_rest3.hour);
	set_combobox("rslt_rest_minute3", sp_rest3.minute);
	set_combobox("rslt_ovtmrsn_id3", data.rslt_ovtmrsn_id3);
	set_chkbox("rslt_call_flag3", data.rslt_call_flag3);
	set_value("rslt_ovtmrsn_txt3", data.rslt_ovtmrsn_txt3);
}

/**
 * 超過勤務時間を設定
 "ovtm_minute": [
 {
 "ovtmkbn_id":
 "ovtmkbn_kind": 
 "ovtmkbn_name": 
 "order_no":
 "emp_id": 
 "date": 
 "fkey": 
 "minute": 
 "element_id":
 "over_time": 
 },
 */
function set_ovtm_minute(data) {
	$.each(data.ovtm_minute, function () {
		$("#"+this.element_id).empty();
		$("#"+this.element_id).append(this.over_time);
		$("#"+this.element_id_hidden).empty();
		$("#"+this.element_id_hidden).val(this.over_time);
	});
}

// 時間有休を設定(propertyはDBと一緒)
function set_hol_hour(data) {
	if(data != null) {
		var start_time = split_hi(data.start_time);
		set_combobox("hol_start_hour", start_time.hour);
		set_combobox("hol_start_minute", start_time.minute);
		set_combobox("hol_time_hour", "0" + data.use_hour);
		set_combobox("hol_time_minute", data.use_minute);
	}
}
