<?php
require_once 'about_comedix.php';
require_once 'common_log_class.ini';

require_once 'kintai/common/ajax_response.php';
require_once 'kintai/common/user_info.php';
require_once 'kintai/common/date_util.php';
require_once 'kintai/common/master_util.php';
require_once 'kintai/common/workflow_common_util.php';

require_once 'kintai/model/kintai_late_basic_model.php';

require_once 'kintai/individual/model/apply_late_db_access.php';

/**
 * 職員情報を取得する
 */
function get_emp_info() {
    $emp_id = $_POST["emp_id"];
    $user_info = UserInfo::get($emp_id, "u");
    
    // [休暇を受けるもの]表示
    $base_info = $user_info->get_base_info();
    $job_info = $user_info->get_job_info();
    $condition = $user_info->get_emp_condition();
    $dept_info = $user_info->get_dept_info();

    // 組織階層数を取得
    $level_count = MasterUtil::get_organization_level_count();

    $current_organization = $dept_info->dept_nm;
    if ($level_count == 4) { // 組織階層が4の場合はroomを取得
        $room_info = $user_info->get_class_room_info();
        if (!empty($room_info->room_nm)) {
            $current_organization = $room_info->room_nm;
        }
    }
    
    $res = new AjaxResponse("success");
    $res->data = array(
        "emp_id" => $emp_id,
        "emp_personal_id" => $base_info->emp_personal_id,
        "emp_lt_nm" => $base_info->emp_lt_nm,
        "emp_ft_nm" => $base_info->emp_ft_nm,
        "job_nm" => $job_info->job_nm,
        "duty_form_name" => $condition->duty_form_name,
        "salary_type_name" => $condition->salary_type_name,
        "organization" => $current_organization
    );

    echo cmx_json_encode($res);
}

/**
 * 申請前チェック
 * クライアントで不可能なチェック処理を行う
 */
function apply_check() {
    
    $res = new AjaxResponse("success");
    $res->next = false;
    
    $param = $_POST["params"];

    $emp_id = $param["emp_id"];
    $apply_id = $param["apply_id"];
    $start_date_y = $param["start_date"]["year"];
    $start_date_m = $param["start_date"]["month"];
    $start_date_d = $param["start_date"]["day"];
    
    $kind_flag = $param["kind_flag"];
    $reason_id    = $param["reason_id"];
    $start_hour = $param["start_time"]["hour"];
    $start_minute = $param["start_time"]["minute"];
    
    // 承認済チェック
    if (WorkflowCommonUtil::is_approved("kintai_late", $apply_id)) {
        $res->message = "既に承認されています。";
        $res->next = false;
        echo cmx_json_encode($res);
        return;
    }
    
    $start_date = $start_date_y.DateUtil::to_02d($start_date_m).DateUtil::to_02d($start_date_d); // 日
    
    // 既にあればNG
    if (ApplyLateDBAccess::late_exists($emp_id, $start_date, $apply_id, $kind_flag)) {
        $kind_str =  ($kind_flag == "1") ? "遅刻" : "早退";
        $res->message = "既に申請済の{$kind_str}が存在します。";
        $res->next = false;
        echo cmx_json_encode($res);
        return;
    }
    echo cmx_json_encode($res);
    return;
}

/**
 * テンプレートに表示するデータ等を取得
 */
function get_late_data() {
    $log = get_logger();
    $log->info("----- get_late_data Start -----");
    // 実績日
    $late_date = $_POST["late_date"];
    $rslt_info = ApplyLateDBAccess::get_atd_result($late_date, $_POST["apply_emp_id"]);
    // AjaxResponse
    $response = new AjaxResponse("success");
    // プロパティとして結果を追加
    $response->rslt_info = $rslt_info;
    $log->debug(var_export($response, true));
    echo cmx_json_encode($response);
    
    $log->info("----- get_late_data End  -----");
}


function get_logger() {
    return new common_log_class(basename(__FILE__));
}
?>
