<?php
require_once 'about_comedix.php';
require_once 'common_log_class.ini';

require_once 'kintai/common/ajax_response.php';
require_once 'kintai/common/user_info.php';
require_once 'kintai/common/date_util.php';
require_once 'kintai/common/master_util.php';
require_once 'kintai/common/workflow_common_util.php';

require_once 'kintai/model/kintai_hol_basic_model.php';

require_once 'kintai/individual/model/apply_hol_db_access.php';

/**
 * 休暇事由を取得する
 */
function get_reason_list() {
    // 事由の取得
    $reason_result = MasterUtil::get_reason_list();

    $option_list = array();

    // コンボボックス用に編集
    foreach ($reason_result as $item) {
        $option = array();
        $option["value"] = $item["reason_id"];

        if (!empty($item["display_name"])) { // 表示名称が空ならデフォルト名称
            $option["label"] = h($item["display_name"]);
        } else {
            $option["label"] = h($item["default_name"]);
        }
        array_push($option_list, $option);
    }
    $res = new AjaxResponse("success");
    $res->data = $option_list;
    echo cmx_json_encode($res);
}

/**
 * 勤務パターンを取得する(半日の場合)
 */
function get_atdptn_list() {

    $type = $_POST["type"];
    $tmcd_group_id = $_POST["tmcd_group_id"];

    $pattern_list = array();
    if ($type == 2) {
        $pattern_list = ApplyHolDBAccess::get_pattern($tmcd_group_id);
    }
    if ($type == 7) {
        $pattern_list = ApplyHolDBAccess::get_all_pattern($tmcd_group_id);
    }

    // コンボボックス用に編集
    $option_list = array();
    foreach ($pattern_list as $item) {
        $option = array();
        $option["value"] = $item["atdptn_id"];
        $option["label"] = h($item["atdptn_nm"]);
        array_push($option_list, $option);
    }

    $res = new AjaxResponse("success");
    $res->data = $option_list;

    echo cmx_json_encode($res);
}

/**
 * 事由を取得
 */
function get_reason() {
    $res = new AjaxResponse("success");
    $res->data = ApplyHolDBAccess::get_atdptn_reason($_POST["tmcd_group_id"], $_POST["atdptn"]);
    echo cmx_json_encode($res);
}

/**
 * 申請前チェック
 * クライアントで不可能なチェック処理を行う
 */
function apply_check() {

    $res = new AjaxResponse("success");

    $data = $_POST["data"]; 
    $kind_flg = $data["kind_flag"];
    // 遅刻、早退はチェックなし
    if ($kind_flag == "9" || $kind_flag == "10" || $kind_flag == "12" || $kind_flag == "8" || $kind_flag == "15") {
        echo cmx_json_encode($res);
        return;
    } 

    $emp_id = $data["emp_id"];
    $apply_id = $data["apply_id"];
    
    if (WorkflowCommonUtil::is_approved("kintai_hol", $apply_id)) {
        $res->result = "error";
        $res->message = "既に承認されています。";
        echo cmx_json_encode($res);
        return;
    }

/*
    $isError = false;
    include 'kintai/individual/const.php';
    $input_type = $TYPE[$kind_flg];
    switch($input_type) {
    case "TYPE1":
        $start_y1 = $data["date1"]["year"];
        $start_y2 = $data["date5"]["year"];
        $start_y3 = $data["date7"]["year"];
        $start_y4 = $data["date9"]["year"];
        $start_y5 = $data["date11"]["year"];
        $end_y1   = $data["date2"]["year"];
        $end_y2   = $data["date6"]["year"];
        $end_y3   = $data["date8"]["year"];
        $end_y4   = $data["date10"]["year"];
        $end_y5   = $data["date12"]["year"];
        $start_m1 = $data["date1"]["month"];
        $start_m2 = $data["date5"]["month"];
        $start_m3 = $data["date7"]["month"];
        $start_m4 = $data["date9"]["month"];
        $start_m5 = $data["date11"]["month"];
        $end_m1   = $data["date2"]["month"];
        $end_m2   = $data["date6"]["month"];
        $end_m3   = $data["date8"]["month"];
        $end_m4   = $data["date10"]["month"];
        $end_m5   = $data["date12"]["month"];
        $start_d1 = $data["date1"]["day"];
        $start_d2 = $data["date5"]["day"];
        $start_d3 = $data["date7"]["day"];
        $start_d4 = $data["date9"]["day"];
        $start_d5 = $data["date11"]["day"];
        $end_d1   = $data["date2"]["day"];
        $end_d2   = $data["date6"]["day"];
        $end_d3   = $data["date8"]["day"];
        $end_d4   = $data["date10"]["day"];
        $end_d5   = $data["date12"]["day"];

        // from, to
        $start_date1;
        $end_date1;
        $start_date2;
        $end_date2;
        $start_date3;
        $end_date3;
        $start_date4;
        $end_date4;
        $start_date5;
        $end_date5;

        // 入力の補完&check
        if (require_date($start_y1, $start_m1, $start_d1)) {
            $start_date1 = $start_y1.$start_m1.$start_d1;
            if (require_date($end_y1, $end_m1, $end_d1)) {
                $end_date1 = $end_y1.$end_m1.$end_d1;
            } else {
                $end_date1 = $start_date1;
            }
            if (ApplyHolDBAccess::hol_exists($emp_id, $start_date1, $end_date1, $apply_id)) {
                $isError = true;
            }
        } 
        if (require_date($start_y2, $start_m2, $start_d2)) {
            $start_date2 = $start_y2.$start_m2.$start_d2;
            if (require_date($end_y2, $end_m2, $end_d2)) {
                $end_date2 = $end_y2.$end_m2.$end_d2;
            } else {
                $end_date2 = $start_date2;
            }
            if (ApplyHolDBAccess::hol_exists($emp_id, $start_date2, $end_date2, $apply_id)) {
                $isError = true;
            }
        } 
        if (require_date($start_y3, $start_m3, $start_d3)) {
            $start_date3 = $start_y3.$start_m3.$start_d3;
            if (require_date($end_y3, $end_m3, $end_d3)) {
                $end_date3 = $end_y3.$end_m3.$end_d3;
            } else {
                $end_date3 = $start_date3;
            }
            if (ApplyHolDBAccess::hol_exists($emp_id, $start_date3, $end_date3, $apply_id)) {
                $isError = true;
            }
        } 
        if (require_date($start_y4, $start_m4, $start_d4)) {
            $start_date4 = $start_y4.$start_m4.$start_d4;
            if (require_date($end_y4, $end_m4, $end_d4)) {
                $end_date4 = $end_y4.$end_m4.$end_d4;
            } else {
                $end_date4 = $start_date4;
            }
            if (ApplyHolDBAccess::hol_exists($emp_id, $start_date4, $end_date4, $apply_id)) {
                $isError = true;
            }
        } 
        if (require_date($start_y5, $start_m5, $start_d5)) {
            $start_date5 = $start_y5.$start_m5.$start_d5;
            if (require_date($end_y5, $end_m5, $end_d5)) {
                $end_date5 = $end_y5.$end_m5.$end_d5;
            } else {
                $end_date5 = $start_date5;
            }
            if (ApplyHolDBAccess::hol_exists($emp_id, $start_date5, $end_date5, $apply_id)) {
                $isError = true;
            }
        }
            break;
        case "TYPE2":
            $date = $data["date3"]["year"].$data["date3"]["month"].$data["date3"]["day"];
            if (ApplyHolDBAccess::hol_exists($emp_id, $date, $date, $apply_id)) {
                $isError = true;
            }
            break;
        case "TYPE3":
            $date = $data["date4"]["year"].$data["date4"]["month"].$data["date4"]["day"];
            if (ApplyHolDBAccess::hol_exists($emp_id, $date, $date, $apply_id)) {
                $isError = true;
            }
            break;
    }
    if ($isError) {
        $res->result = "error";
        $res->message = "申請済の届出が存在します。";
        echo cmx_json_encode($res);
        return;
    }
*/
    echo cmx_json_encode($res);
}

function require_date($y, $m, $d) {
    return ($y && $m && $d);
}


function get_logger() {
    return new common_log_class(basename(__FILE__));
}
?>
