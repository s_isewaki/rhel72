<?php
require_once 'about_comedix.php';
require_once 'common_log_class.ini';

require_once 'kintai/common/ajax_response.php';
require_once 'kintai/common/user_info.php';
require_once 'kintai/common/date_util.php';
require_once 'kintai/common/master_util.php';

require_once 'kintai/model/overtime_basic_model.php';
require_once 'kintai/model/overtime_record.php';

require_once 'kintai/individual/model/overtime_db_access.php';
require_once 'kintai/individual/model/overtime_calc_logic.php';
require_once 'kintai/individual/model/overtime_kbn.php';

/**
 * テンプレートに表示する残業データ等を取得
 */
function get_ovtm_data() {

    $log = get_logger();
    $log->info("----- get_ovtm_data Start -----");
    // 実績日
    $rslt_date = $_POST["rslt_date"];

    $user_info = UserInfo::get($_POST["apply_emp_id"], "u");
    $base_info = $user_info->get_base_info();
    $condition_info = $user_info->get_emp_condition();

    // 勤務実績を取得
    $rslt_info = OvertimeDBAccess::get_atd_result($rslt_date, $base_info->emp_id);

    // 残業時間を取得
    $ovtm_info = OvertimeBasicModel::get($rslt_date, $base_info->emp_id);

    // 超過時間を取得
    $ovtm_minute = OvertimeDBAccess::get_ovtm_minute($rslt_date, $base_info->emp_id);
    // 画面出力用の編集
    for ($i = 0; $i < sizeof($ovtm_minute); $i++) {
        $ovtm_minute[$i]["element_id"] = "ovtm_minute_".$ovtm_minute[$i]["ovtmkbn_id"];
        $ovtm_minute[$i]["over_time"] = DateUtil::convert_time($ovtm_minute[$i]["minute"]);
    }

    // 超過時間を集計
    // マスタを取得
    $ovtm_mst_list = MasterUtil::get_ovtm_div();
    // マスタ分loopして集計
    // 集計期間を取得
    $sum_period = DateUtil::get_cutoff_period_from_date($condition_info->duty_form, $rslt_date);

    $ovtm_total = array();
    // 集計＋画面出力用の編集
    foreach ($ovtm_mst_list as $item) {
        // 超過勤務時間区分
        $ovtm_div = $item["ovtmkbn_id"];
        // 1区分ずつ集計
        $sum_minute = DateUtil::convert_time(
            OvertimeDBAccess::get_ovtm_sum($sum_period["start"],$sum_period["end"], $ovtm_div, $base_info->emp_id));
        $ovtm_total["ovtm_sum_".$ovtm_div] = $sum_minute;
    }

    // AjaxResponse
    $response = new AjaxResponse("success");
    // プロパティとして結果を追加
    $response->rslt_info = $rslt_info;
    $response->ovtm_exists = OvertimeBasicModel::exists($base_info->emp_id, $rslt_date);
    $response->ovtm_info = $ovtm_info;
    $response->ovtm_minute = $ovtm_minute;
    $response->ovtm_total = $ovtm_total;
    $log->debug(var_export($response, true));
    echo cmx_json_encode($response);

    $log->info("----- get_ovtm_data End  -----");
}

/**
 * 所定労働時間のチェック, ステータスチェック
 */
function submit_check() {

    $log = get_logger();
    $log->info("----- submit_check start -----");

    $log->debug(var_export($_POST["form"], true));

    $response = new AjaxResponse("success");

    $param = $_POST["form"];
    $rslt_date_map = $param["rslt_date"];
    $rslt_lines    = $param["rslt"];
    $emp_id        = $param["apply_emp_id"]; 
    $current_user_id = $param["current_user_id"];

    // 非常勤職員かどうか
    $is_shorttime_emp = OvertimeCalcUtil::is_shorttime_employee($emp_id);
    // 計算対象の時間を取得する
    $calc_line = OvertimeCalcLogic::get_calc_line($rslt_date_map, $rslt_lines);
    // 所定労働時間を取得
    $fixed_work_time = OvertimeDBAccess::get_fixed_work_time($rslt_date_map, $emp_id);
    // 常勤職員かつ、休暇以外は所定労働時間をチェック
    // 非常勤職員は所定労働時間(from toがシステム上にないため、チェックなし)
    if (!$is_shorttime_emp && $fixed_work_time["atdptn"] != "10") {
        $warning = OvertimeCalcLogic::fixed_time_check($calc_line, $fixed_work_time);
        if ($warning) {
            $response->message = "所定労働時間内に申請されています。継続しますか？";
            $response->next = true;
            echo cmx_json_encode($response);
            exit;
        }
    }

    // 労働基準法に従った休憩時間のチェック
    /*
    if (OvertimeCalcLogic::rest_time_error($is_shorttime_emp, $calc_line1, $emp_id, $fixed_work_time) || 
        OvertimeCalcLogic::rest_time_error($is_shorttime_emp, $calc_line2, $emp_id, $fixed_work_time)) {
            $response->message = "規定に従った休憩時間を取得してください。";
            $response->next = false;
            echo cmx_json_encode($response);
            exit;
        }
     */

    // 正常の場合
    $response->next = true;
    echo cmx_json_encode($response);

    $log->info("----- submit_check end -----");
}

/**
 * 残業時間を計算する
 */
function ovtm_calc() {

    $log = get_logger();
    $log->info("----- ovtm_calc Start -----");
    $log->debug(var_export($_POST["form"], true));

    $GLOBALS["calc_regist"] = $_POST["calc_regist"];

    $param = $_POST["form"];
    $rslt_date_map = $param["rslt_date"];
    $rslt_lines    = $param["rslt"];
    $emp_id        = $param["apply_emp_id"]; 

    $user_info = UserInfo::get($emp_id, "u");
    $condition_info = $user_info->get_emp_condition();

    // 計算対象の時間を取得する
    $calc_line = OvertimeCalcLogic::get_calc_line($rslt_date_map, $rslt_lines);
    $log->debug(var_export($calc_line, true));

    // 実績日(yyyymmdd)
    $rslt_date = DateUtil::to_db_ymd($rslt_date_map["year"], $rslt_date_map["month"], $rslt_date_map["day"]);

    // 計算
    $calc_rslt = OvertimeCalcLogic::calc($calc_line, $rslt_date_map, $emp_id);
    $log->debug("calc_rslt:".var_export($calc_rslt->get_response(), true));
    // 超過時間を集計
    // マスタを取得
    $ovtm_mst_list = MasterUtil::get_ovtm_div();
    // マスタ分loopして集計
    // 集計期間を取得
    $sum_period = DateUtil::get_cutoff_period_from_date($condition_info->duty_form, $rslt_date);

    $ovtm_total = array();
    // 集計＋画面出力用の編集
    foreach ($ovtm_mst_list as $item) {
        // 超過勤務時間区分
        $ovtm_div = $item["ovtmkbn_id"];
        // 1区分ずつ集計(実施日分は除外)
        $minute = OvertimeDBAccess::get_ovtm_sum($sum_period["start"],$sum_period["end"], $ovtm_div, $emp_id, $rslt_date);
        $calc_minute = $calc_rslt->get_minute($ovtm_div);

        $sum_minute = null;
        if (!empty($minute) || !empty($calc_minute)) {
            $sum_minute = DateUtil::convert_time(intval($minute) + intval($calc_minute));
        }
        $ovtm_total["ovtm_sum_".$ovtm_div] = $sum_minute;
    }

    // AjaxResponse
    $response = new AjaxResponse("success");
    // プロパティとして結果を追加
    $response->ovtm_minute = $calc_rslt->get_response();
    $response->ovtm_total = $ovtm_total;
    $log->debug(var_export($response, true));
    echo cmx_json_encode($response);

    $log->info("----- ovtm_calc End  -----");
}


function get_logger() {
    return new common_log_class(basename(__FILE__));
}
?>
