<?php
require_once 'about_comedix.php';
require_once 'common_log_class.ini';
require_once 'Cmx.php';
require_once 'Cmx/Model/SystemConfig.php';

require_once 'kintai/common/ajax_response.php';
require_once 'kintai/common/user_info.php';
require_once 'kintai/common/date_util.php';
require_once 'kintai/common/master_util.php';
require_once 'kintai/common/workflow_common_util.php';

require_once 'kintai/model/kintai_hol2_basic_model.php';

require_once 'kintai/individual/model/apply_hol2_db_access.php';
require_once 'timecard_paid_hol_hour_class.php';

/**
 * 職員情報を取得する
 */
function get_emp_info() {
    $emp_id = $_POST["emp_id"];
    $user_info = UserInfo::get($emp_id, "u");
    
    // [休暇を受けるもの]表示
    $base_info = $user_info->get_base_info();
    $job_info = $user_info->get_job_info();
    $condition = $user_info->get_emp_condition();
    $dept_info = $user_info->get_dept_info();
    $st_info = $user_info->get_st_info();

    // 組織階層数を取得
    $level_count = MasterUtil::get_organization_level_count();

    $current_organization = $dept_info->dept_nm;
    if ($level_count == 4) { // 組織階層が4の場合はroomを取得
        $room_info = $user_info->get_class_room_info();
        if (!empty($room_info->room_nm)) {
            $current_organization = $room_info->room_nm;
        }
    }
    
    //年休残 20130917
    //時間有休
    $obj_hol_hour = new timecard_paid_hol_hour_class("", "");
    $obj_hol_hour->select();
    $paid_hol_remain = $obj_hol_hour->get_nenkyu_zan_kintai($emp_id, "1", "", "");
    
    $res = new AjaxResponse("success");
    $res->data = array(
        "emp_id" => $emp_id,
        "emp_personal_id" => $base_info->emp_personal_id,
        "emp_lt_nm" => $base_info->emp_lt_nm,
        "emp_ft_nm" => $base_info->emp_ft_nm,
        "job_nm" => $job_info->job_nm,
    	"duty_form"=>$condition->duty_form,
        "duty_form_name" => $condition->duty_form_name,
        "salary_type_name" => $condition->salary_type_name,
        "organization" => $current_organization,
        "paid_hol_remain" => $paid_hol_remain,
        "st_nm" => $st_info->st_nm
    );

    echo cmx_json_encode($res);
}

/**
 * 夏休取得済日数を取得する 20140808
 */
function get_summer_holidays(){
	$res = new AjaxResponse("success");
	$emp_id = $_POST["emp_id"];
	$start_date = $_POST["start_date"];
	$str_start = $start_date["year"] . $start_date["month"] .$start_date["day"];
	$end_date = $_POST["end_date"];
	$str_end = $end_date["year"] . $end_date["month"] .$end_date["day"];
	$summer_start_date = $start_date["year"].substr($_POST["summer_start_date"],4,4);
	$summer_end_date = $start_date["year"].substr($_POST["summer_end_date"],4,4);
	$reason = $_POST["reason"];
	
	// 夏休付与データを取得
	$obj_hol_hour = new timecard_paid_hol_hour_class("", "");
	$summer_list = $obj_hol_hour->get_emppaid_summer($emp_id, $start_date["year"]."-01-01");
		
	$summer_holidays = (empty($summer_list["days"]))? 0 : (int)$summer_list["days"];
	$reason_mst = ApplyHol2DBAccess::get_atdbk_reason_mst_summer();

	$taken_holidays = 0;
	if ($reason_mst["reason_id"]){
		$kintai_hol_list = ApplyHol2DBAccess::get_kintai_hol_summer($emp_id, $summer_start_date, $summer_end_date);
		//期間
		$today = date("Ymd");
		$nextday = date("Ymd", strtotime("+1 day"));
		//予定は未来分、実績は当日以前とする
		//$atdbk_list = ApplyHol2DBAccess::get_atdbk_summer($emp_id, $nextday, $summer_end_date);
		//$atdbkrslt_list = ApplyHol2DBAccess::get_atdbkrslt_summer($emp_id, $summer_start_date, $today);
		$tmp_holidays = array();
		foreach ($kintai_hol_list as $item){
			if (strtotime($item["end_date"]) >= strtotime($item["start_date"])){
				$day_count = (int) date("d", strtotime($item["end_date"]) - strtotime($item["start_date"]));
				
				for($i = 0; $i < $day_count; $i++){
					$tmp_date = date("Ymd", strtotime("$i day", strtotime($item["start_date"])));
					//申請する期間は除く
					if (!($str_start <= $tmp_date && $tmp_date <= $str_end)) {
						//半日対応
						$tmp_holidays[$tmp_date] = $item["hol_count"];
					}
				}
			}
		}
		
		/*
		foreach ($atdbk_list as $item){
			//申請する期間は除く
			if (!($str_start <= $item["date"] && $item["date"] <= $str_end) &&
				 $tmp_holidays[$item["date"]] == "") {
				//半日対応
				$tmp_holidays[$item["date"]] = $item["hol_count"];
			}
		}
		
		foreach ($atdbkrslt_list as $item){
			//申請する期間は除く
			if (!($str_start <= $item["date"] && $item["date"] <= $str_end) &&
				 $tmp_holidays[$item["date"]] == "") {
				//半日対応
				$tmp_holidays[$item["date"]] = $item["hol_count"];
			}
		}
		*/
		//半日対応
		$taken_holidays = 0.0;
		foreach ($tmp_holidays as $wk_date => $hol_count) {
            $taken_holidays += $hol_count;
		}

	}
	
	// 申請しよう日数を取得
	$apply_start = strtotime($str_start);
	$apply_end = strtotime($str_end);
	$apply_diff = abs($apply_start - $apply_end);
	$apply_days = $apply_diff / (60 * 60 * 24) + 1;
	//半日対応	
	if ($reason != "31") {
		$apply_days *= 0.5;
	}
	
	// 夏休休暇の残日数
	if ($summer_holidays == 0 || $summer_holidays <= $taken_holidays){
		$remaining_days = 0;
	}else{
		$remaining_days = $summer_holidays - $taken_holidays;
	}
	
	//半日対応(int)除く
	$option_list = array("remaining_days"=>$remaining_days, "apply_days"=>$apply_days);
	
	$res->data = $option_list;
	echo cmx_json_encode($res);
}

/**
 * 休暇事由を取得する
 */
function get_reason_list() {
    
    $kind = $_POST["kind_flg"]; // 休暇種別
    $t_hol_type = $_POST["t_hol_type"]; // 0:通常、1:年休・夏休、2:病休・特休

    $reason_list = ApplyHol2DBAccess::get_atdbk_reason($kind);
    
    $option_list = array();
    // コンボボックス用に編集
    foreach ($reason_list as $item) {
        $option = array();
        $option["value"] = $item["reason_id"];

        // 特別休暇が選択された場合
        // 夏休み以外も表示20131107、変更されるかもしれないため、下の処理をコメントで残します。
        /*
        if ($kind == "5") {
            if ($t_hol_type == "1") { // 1:年休、夏休は夏休だけ残るように
                if ($item["reason_id"] != "31") {
                    continue;
                }
            } else if ($t_hol_type == "2") {
                if ($item["reason_id"] == "31") { // 2:夏休はスキップ
                    continue;
                }
            }
        }
        */      
        if (!empty($item["display_name"])) { // 表示名称が空ならデフォルト名称
            $option["label"] = htmlspecialchars($item["display_name"]);
        } else {
            $option["label"] = htmlspecialchars($item["default_name"]);
        }
        
        $option["count"] = $item["holiday_count"];
        $option["time_hol_flag"] = $item["time_hol_flag"];
        $option["recital"] = $item["recital"];
        array_push($option_list, $option);
    }

    $res = new AjaxResponse("success");
    $res->data = $option_list;
    echo cmx_json_encode($res);
}

/**
 * 勤務パターンを取得する(半日の場合)
 */
function get_pattern_list() {

    $res = new AjaxResponse("success");

    $emp_id = $_POST["emp_id"];
    $reason = $_POST["reason"];
    
    $select_reason = ApplyHol2DBAccess::get_reason($reason);
    $day_count = $select_reason["holiday_count"];
    
    if ($day_count != 0.5) { // 休暇日数が0.5以外は処理を終了
        $res->data = array();
        echo cmx_json_encode($res);
        return;
    }
    $half_hol_flag = $select_reason["half_hol_flag"];
    if ($half_hol_flag == "t") { // 半休/半休は処理を終了
        $res->data = array();
        echo cmx_json_encode($res);
        return;
    }
    
    $user_info = UserInfo::get($emp_id, "u");
    $condition = $user_info->get_emp_condition();
    
    // 勤務パターンを取得(0.5日のものだけ)
    $pattern_list = ApplyHol2DBAccess::get_pattern($condition->tmcd_group_id);
    if (sizeof($pattern_list) == 0) { // なければ全部とってくる
        $pattern_list = ApplyHol2DBAccess::get_all_pattern($condition->tmcd_group_id);
    }
    // コンボボックス用に編集
    $option_list = array();
    foreach ($pattern_list as $item) {
        $option = array();
        $option["value"] = $item["atdptn_id"];
        $option["label"] = htmlspecialchars($item["atdptn_nm"]);
        array_push($option_list, $option);
    }
    
    $res->data = $option_list;

    echo cmx_json_encode($res);
}
/**
 * 細目を取得する(半日の場合)
 */
function get_detail_list() {

    $res = new AjaxResponse("success");
    $reason = $_POST["reason"];
    
    $detail_list = ApplyHol2DBAccess::get_detail($reason);
    
    $option_list = array();
    foreach ($detail_list as $detail) {
    	$option = array();
    	$full_day = ($detail["reason_fulltime_day"]) ? $detail["reason_fulltime_day"] : "0";
    	$part_day = ($detail["reason_parttime_day"]) ? $detail["reason_parttime_day"] : "0";
    	$option["value"] = $detail["reason_detail_id"];
    	$option["label"] = $detail["reason_detail_name"];
    	$option["day"] = $full_day . "_" . $part_day;
    	
    	array_push($option_list, $option);
    }
    
    $res->data = $option_list;

    echo cmx_json_encode($res);
}

/**
 * 申請前チェック
 * クライアントで不可能なチェック処理を行う
 */
function apply_check() {
    
    $res = new AjaxResponse("success");

    $param = $_POST["params"];

    $emp_id = $param["emp_id"];
    $apply_id = $param["apply_id"];
    $start_date_y = $param["start_date"]["year"];
    $start_date_m = $param["start_date"]["month"];
    $start_date_d = $param["start_date"]["day"];
    $end_date_y =   $param["end_date"]["year"];
    $end_date_m =   $param["end_date"]["month"];
    $end_date_d =   $param["end_date"]["day"];
    
    $kind_flag = $param["kind_flag"];
    $reason    = $param["reason"];
    $start_hour = $param["start_time"]["hour"];
    $start_minute = $param["start_time"]["minute"];
    
    $time_hol = $param["time_hol"];
    
    // 承認済チェック
    if (WorkflowCommonUtil::is_approved("kintai_hol", $apply_id)) {
        //$res->result = "error";
        $res->message = "既に承認されています。";
        $res->next = false;
        echo cmx_json_encode($res);
        return;
    }
    
    $start_date = $start_date_y.DateUtil::to_02d($start_date_m).DateUtil::to_02d($start_date_d); // 開始日
    $end_date   = $end_date_y.DateUtil::to_02d($end_date_m).DateUtil::to_02d($end_date_d); // 終了日
    
    // 休暇日数を取得
    $select_reason = ApplyHol2DBAccess::get_reason($reason);
    $day_count = $select_reason["holiday_count"];

    // チェックタイプを取得
    $check_type = ApplyHol2DBAccess::get_check_type($kind_flag, $start_date, $end_date, $start_hour, $start_minute, $day_count);
    
    // いかなる場合も全日休暇が既にあればNG
    if (ApplyHol2DBAccess::hol_exists($emp_id, $start_date, $end_date, $apply_id, null)) {
        //$res->result = "error";
        $res->message = "既に申請済の休暇が存在します。";
        $res->next = false;
        echo cmx_json_encode($res);
        return;
    }
    
    // 時間申請じゃないときに半日休暇が既にあればNG
    if (!in_array($check_type, array("time_paid_hol", "time_nopaid_hol")) && ApplyHol2DBAccess::hol_exists($emp_id, $start_date, $end_date, $apply_id, "half")) {
        //$res->result = "error";
        $res->message = "既に申請済の休暇(半日)が存在します。";
        $res->next = false;
        echo cmx_json_encode($res);
        return;
    }
    
    // 時間有給は既に時間有給があればNG
    if ($check_type == "time_paid_hol" && ApplyHol2DBAccess::hol_exists($emp_id, $start_date, $end_date, $apply_id, "time_paid_hol")) {
        //$res->result = "error";
        $res->message = "有給休暇(時間)は日に２回以上申請できません。";
        $res->next = false;
        echo cmx_json_encode($res);
        return;
    }
    //時間有休と所定時間を比較する 20130806 更新時パラメータが"null"になるため対応 20130823
    if ($param["use_time"]["hour"] != "null" && $param["use_time"]["minute"] != "null") {
        $use_time = $param["use_time"]["hour"].$param["use_time"]["minute"];
        if ($use_time != "") {
	        $user_info = UserInfo::get($emp_id, "u");
	        $condition = $user_info->get_emp_condition();
	        if ($condition->specified_time != "") {
	            $specified_time = $condition->specified_time;
	        }
	        //未設定時は組織の所定時間
	        else {
	            $specified_time = MasterUtil::get_organization_work_time();
	        }
	        if ($use_time > $specified_time) {
	            $min = DateUtil::convert_min($specified_time);
	            $hm =  DateUtil::convert_time($min);
	            //$res->result = "error";
	            $res->message = "所定時間({$hm})より長い時間有休が指定されています。";
                $res->next = false;
                echo cmx_json_encode($res);
	            return;
	        }
        }
    }
    //有休、時間有休の場合、年休残と比較チェックする 20130919
    if ($kind_flag == "1") {
		$conf = new Cmx_SystemConfig();
		//対象日数が年休残を超える申請時の動作  1:警告を出すが申請はできる 2:申請できない
		$over_apply_flg = $conf->get('apply.hol.over_apply_flg');
		//初期値
		if ($over_apply_flg == "") {
			$over_apply_flg = "1";
		}
        $user_info = UserInfo::get($emp_id, "u");
        $condition = $user_info->get_emp_condition();
        if ($condition->specified_time != "") {
            $specified_time = $condition->specified_time;
        }
        //未設定時は組織の所定時間
        else {
            $specified_time = MasterUtil::get_organization_work_time();
        }
        //所定時間
        $specified_time_min = DateUtil::convert_min($specified_time);
        //開始日を基準に年休残を取得
        $obj_hol_hour = new timecard_paid_hol_hour_class("", "");
        $obj_hol_hour->select();
        //残取得、第2引数は関数内で設定を取得し使用するため、指定しない
        //第3引数、第4引数に開始日終了日を設定
        $paid_hol_remain = $obj_hol_hour->get_nenkyu_zan_kintai($emp_id, "", $start_date, $end_date, "t");
        //付与データがない場合
        if ($paid_hol_remain == "") {
            //オプション確認 20141209
            if ($over_apply_flg == "1") {
                $res->message = "年休付与がまだ行われていませんが申請しますか？";
                $res->next = true;
            }
            else {
                $res->message = "まだ年休の付与がされていないので申請ができません。";
                $res->next = false;
            }
            echo cmx_json_encode($res);
            return;
        }
        else {
            //前回付与日より1年以降の場合は警告なしで申請可能とする
            $emppaid_info = $obj_hol_hour->get_last_emppaid_info($emp_id, $start_date, "1");
            $last_add_date = $emppaid_info["year"].$emppaid_info["paid_hol_add_mmdd"];
            $sp_date = DateUtil::split_ymd($last_add_date);
            $base_date = DateUtil::to_date($sp_date["y"],$sp_date["m"],$sp_date["d"]);
            $next_add_date = date("Ymd", strtotime("$base_date +1 year"));
            if ($start_date >= $next_add_date) {
                $sp_date = DateUtil::split_ymd($next_add_date);
                $wk_next_add_date = $sp_date["y"]."/".intval($sp_date["m"],10)."/".intval($sp_date["d"],10);
                $res->message = "年休付与予定日（{$wk_next_add_date}）にまだ年休付与がされていません。このまま申請しますか？";
                $res->next = true;
                echo cmx_json_encode($res);
                return;
            }

            //日時を日と時間に分ける
            $arr_dhm = split("\(", $paid_hol_remain);
            $zan_day = $arr_dhm[0];
            $zan_day = str_replace("日", "", $zan_day);
            if ($zan_day < 0) {
                //事由が年休か確認
                $reason_check = $obj_hol_hour->check_paid_reason($emp_id, $start_date, $end_date);
                if ($reason_check) {
                    echo cmx_json_encode($res);
                    return;
                }
            	if ($over_apply_flg == "1") {
	                $res->message = "年休残を超えていますが申請しますか？";
	                $res->next = true;
                }
                else {
	                $res->message = "年休残を超えて申請できません。";
	                $res->next = false;
                }
                echo cmx_json_encode($res);
                return;
            }
            
            $wk_hm = trim($arr_dhm[1], ")");
            //時間を分換算
            $zan_min = DateUtil::convert_min($wk_hm);
            
            //時間有休の場合、半日有休も同時に確認
            if (($param["use_time"]["hour"] != "null" && $param["use_time"]["minute"] != "null") &&
                ($param["use_time"]["hour"] != "" && $param["use_time"]["minute"] != "")) {
                //残が1日以上ならOK
                //残が少ない場合、時間を分換算で比較する
                if ($zan_day < 1.0) {
                    $use_time = $param["use_time"]["hour"].$param["use_time"]["minute"];
                    $use_time_min = DateUtil::convert_min($use_time);
                    if ($day_count == 0.5) {
                        $use_time_min += $specified_time_min * $day_count;
                    }
                    if ($zan_day == 0.5) {
                        $zan_min += $specified_time_min * $zan_day;
                    }
                    if ($zan_min < $use_time_min) {
                        $reason_check = $obj_hol_hour->check_paid_reason($emp_id, $start_date, $end_date);
                        if ($reason_check) {
                            echo cmx_json_encode($res);
                            return;
                        }
		            	if ($over_apply_flg == "1") {
			                $res->message = "年休残を超えていますが申請しますか？";
			                $res->next = true;
		                }
		                else {
			                $res->message = "年休残を超えて申請できません";
			                $res->next = false;
		                }
                        echo cmx_json_encode($res);
                        return;
                    }
                }
                
            }
            //有休の場合
            else {
                $use_day = DateUtil::date_diff($start_date, $end_date);
                //残が少ない場合、時間を合わせて1日を引ける場合
                if ($use_day == 1.0 && $zan_day == 0.5 && $zan_min >= ($specified_time_min/2)) {
                    ;
                }
                //半日の場合
                elseif ($day_count == 0.5 && $zan_day >= $day_count) {
                    ;
                }
                //時間から0.5日を引ける場合
                elseif ($day_count == 0.5 && $zan_day == 0 && $zan_min >= ($specified_time_min/2)) {
                    ;
                }
                elseif ($zan_day < $use_day) {
                    $reason_check = $obj_hol_hour->check_paid_reason($emp_id, $start_date, $end_date);
                    if ($reason_check) {
                        echo cmx_json_encode($res);
                        return;
                    }
	            	if ($over_apply_flg == "1") {
		                $res->message = "年休残を超えていますが申請しますか？";
		                $res->next = true;
	                }
	                else {
		                $res->message = "年休残を超えて申請できません";
		                $res->next = false;
	                }
                    echo cmx_json_encode($res);
                    return;
                }
            }            
        }
    }
    
    echo cmx_json_encode($res);
    return;
}


function get_logger() {
    return new common_log_class(basename(__FILE__));
}
?>
