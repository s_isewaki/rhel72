<?php
require_once 'about_comedix.php';
require_once 'common_log_class.ini';

require_once 'kintai/common/ajax_response.php';
require_once 'kintai/common/user_info.php';
require_once 'kintai/common/date_util.php';
require_once 'kintai/common/master_util.php';

require_once 'kintai/model/overtime_basic_model.php';
require_once 'kintai/model/overtime_record.php';

require_once 'kintai/individual/model/overtime_db_access.php';
require_once 'kintai/individual/model/overtime_calc_logic.php';
require_once 'kintai/individual/model/overtime_kbn.php';
require_once 'kintai/individual/model/apply_hol_db_access.php';


// 初期データを表示する
function get_ovtm_data() {

    $log = get_logger();
    $log->info("----- get_ovtm_data Start -----");

    // 実績日
    $date   = $_POST["date"];
    $emp_id = $_POST["emp_id"];

    // 勤務実績を取得
    $rslt_info = OvertimeDBAccess::get_atd_result($date, $emp_id);
    // 勤務パターンを取得
    $tmcd_group_id = $rslt_info["tmcd_group_id"];
    $atdptn_list = MasterUtil::get_atdptn_list($tmcd_group_id);
    // コンボボックス用に編集
    $atdptn_combo_list = array();
    foreach ($atdptn_list as $item) {
        array_push($atdptn_combo_list,
            array(
                "value" => $item["atdptn_id"],
                "label" => h($item["atdptn_nm"])
            )
        );
    }

    // 残業理由を取得
    $ovtmrsn_list = MasterUtil::get_ovtm_rsn($tmcd_group_id);
    $ovtmrsn_combo_list = array();
    // コンボボックス用に編集
    foreach($ovtmrsn_list as $item) {
        array_push($ovtmrsn_combo_list, 
            array(
                "value" => $item["reason_id"],
                "label" => h($item["reason"])
            )
        );
    }

    // 残業時間を取得
    $ovtm_info = OvertimeBasicModel::get($date, $emp_id);

    // 超過時間を取得
    $ovtm_minute = OvertimeDBAccess::get_ovtm_minute($date, $emp_id);
    // 画面出力用の編集
    for ($i = 0; $i < sizeof($ovtm_minute); $i++) {
        $ovtm_minute[$i]["element_id"] = "ovtm_minute_".$ovtm_minute[$i]["ovtmkbn_id"];
        $ovtm_minute[$i]["over_time"]  = DateUtil::convert_time($ovtm_minute[$i]["minute"]);
    }

    // 時間有休を取得
    $hol_hour = OvertimeDBAccess::get_hol_hour($emp_id, $date);
    // AjaxResponse
    $response = new AjaxResponse("success");
    // プロパティとして結果を追加
    $response->rslt_info    = $rslt_info;
    $response->ovtm_exists  = OvertimeBasicModel::exists($emp_id, $date);
    $response->ovtm_info    = $ovtm_info;
    $response->ovtm_minute  = $ovtm_minute;
    $response->ovtm_total   = $ovtm_total;
    $response->atdptn_list  = $atdptn_combo_list;
    $response->ovtmrsn_list = $ovtmrsn_combo_list;
    $response->hol_hour     = $hol_hour;
    $log->debug(var_export($response, true));
    echo cmx_json_encode($response);

    $log->info("----- get_ovtm_data End  -----");
}

// 勤務グループ変更時
function change_tmcd_group() {

    $log = get_logger();
    $tmcd_group_id = $_POST["tmcd_group_id"];

    $atdptn_list = MasterUtil::get_atdptn_list($tmcd_group_id);
    // コンボボックス用に編集
    $atdptn_combo_list = array();
    foreach ($atdptn_list as $item) {
        array_push($atdptn_combo_list,
            array(
                "value" => $item["atdptn_id"],
                "label" => h($item["atdptn_nm"])
            )
        );
    }

    // 残業理由を取得
    $ovtmrsn_list = MasterUtil::get_ovtm_rsn($tmcd_group_id);
    $ovtmrsn_combo_list = array();
    // コンボボックス用に編集
    foreach($ovtmrsn_list as $item) {
        array_push($ovtmrsn_combo_list, 
            array(
                "value" => $item["reason_id"],
                "label" => h($item["reason"])
            )
        );
    }

    // AjaxResponse
    $response = new AjaxResponse("success");
    // プロパティとして結果を追加
    $response->atdptn_list  = $atdptn_combo_list;
    $response->ovtmrsn_list = $ovtmrsn_combo_list;
    $log->debug(var_export($response, true));
    echo cmx_json_encode($response);
}

// 勤務パターン変更時
function change_atdptn() {

    $log = get_logger();

    $date          = $_POST["date"];
    $emp_id        = $_POST["emp_id"];
    $tmcd_group_id = $_POST["tmcd_group_id"];
    $atdptn_id     = $_POST["atdptn_id"];

    // 勤務実績を取得
    $rslt_info = OvertimeDBAccess::get_atd_result($date, $emp_id, $tmcd_group_id, $atdptn_id);

    // AjaxResponse
    $response = new AjaxResponse("success");
    // プロパティとして結果を追加
    $response->rslt_info    = $rslt_info;
    $log->debug(var_export($response, true));
    echo cmx_json_encode($response);
}

/**
 * 所定労働時間のチェック, ステータスチェック
 */
function submit_check() {

    $log = get_logger();
    $log->info("----- submit_check start -----");

    $log->debug(var_export($_POST["form"], true));

    $response = new AjaxResponse("success");

    $param         = $_POST["form"];
    $rslt_date     = $param["rslt_date"];
    $rslt_lines    = $param["rslt"];
    $emp_id        = $param["emp_id"];

    $split_rslt_date = DateUtil::split_ymd($rslt_date);
    $rslt_date_map = array(
        "year"  => $split_rslt_date["y"],
        "month" => intval($split_rslt_date["m"]),
        "day"   => intval($split_rslt_date["d"])
    );

    // 非常勤職員かどうか
    $is_shorttime_emp = OvertimeCalcUtil::is_shorttime_employee($emp_id);
    // 計算対象の時間を取得する
    $calc_line = OvertimeCalcLogic::get_calc_line($rslt_date_map, $rslt_lines);
    // 所定労働時間を取得
    $fixed_work_time = OvertimeDBAccess::get_fixed_work_time($rslt_date_map, $emp_id, $param["tmcd_group_id"], $param["atdptn_id"]);
    // 常勤職員かつ、休暇以外は所定労働時間をチェック
    // 非常勤職員は所定労働時間(from toがシステム上にないため、チェックなし)
    if (!$is_shorttime_emp && $fixed_work_time["atdptn"] != "10") {
        $warning = OvertimeCalcLogic::fixed_time_check($calc_line, $fixed_work_time);
        if ($warning) {
            $response->message = "所定労働時間内に申請されています。継続しますか？";
            $response->next = true;
            echo cmx_json_encode($response);
            exit;
        }
    }

    // 労働基準法に従った休憩時間のチェック
    /*
    if (OvertimeCalcLogic::rest_time_error($is_shorttime_emp, $calc_line1, $emp_id, $fixed_work_time) || 
        OvertimeCalcLogic::rest_time_error($is_shorttime_emp, $calc_line2, $emp_id, $fixed_work_time)) {
            $response->message = "規定に従った休憩時間を取得してください。";
            $response->next = false;
            echo cmx_json_encode($response);
            exit;
        }
     */

    // 正常の場合
    $response->next = true;
    echo cmx_json_encode($response);

    $log->info("----- submit_check end -----");
}

/**
 * 残業時間を計算する
 */
function ovtm_calc() {

    $log = get_logger();
    $log->info("----- ovtm_calc Start -----");
    $log->debug(var_export($_POST["form"], true));

    // 計算登録フラグ
	$GLOBALS["calc_regist"] = $_POST["calc_regist"];

    $param = $_POST["form"];
    $rslt_date     = $param["rslt_date"];
    $rslt_lines    = $param["rslt"];
    $emp_id        = $param["emp_id"];
    $tmcd_group_id = $param["tmcd_group_id"];
    $atdptn_id     = $param["atdptn_id"];
    $reason_id     = $param["reason_id"];

    $user_info = UserInfo::get($emp_id, "u");
    $condition_info = $user_info->get_emp_condition();

    // 計算対象の時間を取得する
    // rslt_date_map作成
    $split_rslt_date = DateUtil::split_ymd($rslt_date);
    $rslt_date_map = array(
        "year"  => $split_rslt_date["y"],
        "month" => intval($split_rslt_date["m"]),
        "day"   => intval($split_rslt_date["d"])
    );

    $calc_line = OvertimeCalcLogic::get_calc_line($rslt_date_map, $rslt_lines);
    $log->debug(var_export($calc_line, true));

    // 計算
    $calc_rslt = OvertimeCalcLogic::calc($calc_line, $rslt_date_map, $emp_id, 
        $tmcd_group_id, $atdptn_id, $reason_id);
    $log->debug("calc_rslt:".var_export($calc_rslt->get_response(), true));

    // AjaxResponse
    $response = new AjaxResponse("success");
    // プロパティとして結果を追加
    $response->ovtm_minute = $calc_rslt->get_response();
    $log->debug(var_export($response, true));
    echo cmx_json_encode($response);

    $log->info("----- ovtm_calc End  -----");
}

// 残業時間を等を登録する
function submit() {

    $param = get_param();

    $emp_id = $param->emp_id;
    $date   = $param->date;

    // 勤務実績の更新
    OvertimeDBAccess::update_atdbkrslt(
        $emp_id,
        $date,
        $param->atdptn_id,
        $param->reason_id,
        $param->tmcd_group_id,
        $param->meeting_start_time,
        $param->meeting_end_time
    );
    // 時間有休の更新
    if (!empty($param->start_time)) {
        ApplyHolDBAccess::atdbk_time_hol_approve(
            $emp_id,
            $date,
            $param->start_time,
            $param->use_hour,
            $param->use_minute
        );
    }

    // レコード存在チェック 
    $result = OvertimeBasicModel::get($param->date, $param->emp_id, true);
    if (empty($result)) {
        OvertimeBasicModel::insert($param);
    } else {
        $apply_id = $result->apply_id;

        $types = array(
            "text","text",

            "text","integer","text","integer","text","integer","text",
            "text","integer","text","integer","text","integer","text",
            "text","integer","text","integer","text","integer","text",

            "integer", "integer", "integer", "integer"
        );
        // 前申請がある場合、内容を維持
        $values = array(
            "apply_date"            => $result->apply_date,
            "apply_stat"            => $result->apply_stat,

            "rslt_start_time1"      => $param->rslt_start_time1,
            "rslt_start_next_flag1" => $param->rslt_start_next_flag1,
            "rslt_end_time1"        => $param->rslt_end_time1,
            "rslt_end_next_flag1"   => $param->rslt_end_next_flag1,
            "rslt_rest_time1"       => $param->rslt_rest_time1,
            "rslt_ovtmrsn_id1"      => $param->rslt_ovtmrsn_id1,
            "rslt_ovtmrsn_txt1"      => $param->rslt_ovtmrsn_txt1,
            "rslt_start_time2"      => $param->rslt_start_time2,
            "rslt_start_next_flag2" => $param->rslt_start_next_flag2,
            "rslt_end_time2"        => $param->rslt_end_time2,
            "rslt_end_next_flag2"   => $param->rslt_end_next_flag2,
            "rslt_rest_time2"       => $param->rslt_rest_time2,
            "rslt_ovtmrsn_id2"      => $param->rslt_ovtmrsn_id2,
            "rslt_ovtmrsn_txt2"      => $param->rslt_ovtmrsn_txt2,
            "rslt_start_time3"      => $param->rslt_start_time3,
            "rslt_start_next_flag3" => $param->rslt_start_next_flag3,
            "rslt_end_time3"        => $param->rslt_end_time3,
            "rslt_end_next_flag3"   => $param->rslt_end_next_flag3,
            "rslt_rest_time3"       => $param->rslt_rest_time3,
            "rslt_ovtmrsn_id3"      => $param->rslt_ovtmrsn_id3,
            "rslt_ovtmrsn_txt3"      => $param->rslt_ovtmrsn_txt3,

            "apply_id"              => $id,
            "rslt_call_flag1"       => $param->rslt_call_flag1,
            "rslt_call_flag2"       => $param->rslt_call_flag2,
            "rslt_call_flag3"       => $param->rslt_call_flag3,
        );
        OvertimeBasicModel::update($types, $values, $emp_id, $date); 
    }

    // 超過勤務区分を更新
    $ovtm_kbn = $param->ovtm_calc;
    $ovtm_kbn->update($emp_id, $date);
    $log = get_logger();

    // AjaxResponse
    $response = new AjaxResponse("success");
    $log->debug(cmx_json_encode($response));
    echo cmx_json_encode($response);
}

function get_param() {

    // モデル登録用に色々編集
    $log = get_logger();
    $form                  = $_POST["form"];
    $values;
    $values->emp_id        = $form["emp_id"];
    $values->date          = $form["rslt_date"];
    $values->apply_date    = date('Ymd', time());
    $values->apply_stat    = "1";
    $values->tmcd_group_id = $form["tmcd_group_id"];
    $values->atdptn_id     = $form["atdptn_id"];
    $values->reason_id     = $form["reason_id"];

    for ($i = 1; $i <= 3; $i++) {
        $rslt_start_time      = "rslt_start_time"      .$i; 
        $rslt_start_next_flag = "rslt_start_next_flag" .$i; 
        $rslt_end_time        = "rslt_end_time"        .$i; 
        $rslt_end_next_flag   = "rslt_end_next_flag"   .$i; 
        $rslt_rest_time       = "rslt_rest_time"       .$i; 
        $rslt_ovtmrsn_id      = "rslt_ovtmrsn_id"      .$i; 
        $rslt_call_flag       = "rslt_call_flag"       .$i; 
        $rslt_ovtmrsn_txt      = "rslt_ovtmrsn_txt"      .$i; 

        $rslt_start_hour      = "rslt_start_hour"      .$i; 
        $rslt_start_minute    = "rslt_start_minute"    .$i; 
        $rslt_end_hour        = "rslt_end_hour"        .$i; 
        $rslt_end_minute      = "rslt_end_minute"      .$i; 
        $rslt_rest_hour       = "rslt_rest_hour"       .$i; 
        $rslt_rest_minute     = "rslt_rest_minute"     .$i; 

        $values->$rslt_start_time      = DateUtil::to_db_hi(
            $form["rslt"][$i-1]["start_hour"], $form["rslt"][$i-1]["start_minute"]);
        $values->$rslt_start_next_flag = $form["rslt"][$i-1]["start_next_flag"] == "true" ? 1:null;
        $values->$rslt_end_time        = DateUtil::to_db_hi(
            $form["rslt"][$i-1]["end_hour"], $form["rslt"][$i-1]["end_minute"]);
        $values->$rslt_end_next_flag   = $form["rslt"][$i-1]["end_next_flag"] == "true" ? 1:null;
        $values->$rslt_rest_time       = DateUtil::to_db_hi(
            $form["rslt"][$i-1]["rest_hour"], $form["rslt"][$i-1]["rest_minute"]);
        $values->$rslt_ovtmrsn_id      = $form["rslt"][$i-1]["ovtmrsn_id"];
        $values->$rslt_ovtmrsn_txt      = urldecode($form["rslt"][$i-1]["ovtmrsn_txt"]);
        $values->$rslt_call_flag       = $form["rslt"][$i-1]["call_flag"] == "true" ? 1:null;
    }

    // 時間有休
    $values->start_time = DateUtil::to_db_hi($form["hol_start_hour"], $form["hol_start_minute"]);
    $values->use_hour = intval($form["hol_time_hour"]);
    $values->use_minute = DateUtil::to_02d($form["hol_time_minute"]);

    // 会議時間等
    $values->meeting_start_time = DateUtil::to_db_hi($form["ward_start_hour"], $form["ward_start_minute"]);
    $values->meeting_end_time   = DateUtil::to_db_hi($form["ward_end_hour"], $form["ward_end_minute"]);

    $values->ovtm_calc = OvertimeKbn::create_from_submit_form();
    $log->debug("POST :".var_export($form, true));
    $log->debug("param:".var_export($values, true));

    return $values;
}

function get_logger() {
    return new common_log_class(basename(__FILE__));
}
?>
