<?php
require_once 'common_log_class.ini';

require_once 'kintai/common/abstract_workflow.php';
require_once 'kintai/common/user_info.php';
require_once 'kintai/common/date_util.php';
require_once 'kintai/common/mdb.php';

require_once 'kintai/model/kintai_hol2_basic_model.php';
require_once 'kintai/model/kintai_hol2_record.php';
require_once 'kintai/model/atdbk_backup_model.php';
require_once 'kintai/model/atdbkrslt_backup_model.php';
require_once 'kintai/model/timecard_paid_hol_hour_backup_model.php';

require_once 'kintai/individual/model/apply_hol2_db_access.php';


class Hol2Workflow extends AbstractWorkflow {

    private $log;
    
    public function __construct() {
        $this->log = new common_log_class(basename(__FILE__));
    }

    /**
     * 申請
     */
    public function regist_apply($id) {
        $this->log->info("hol-workflow-regist-Start");

        $values = $this->get_param();
        $this->log->debug(var_export($values, true));

        $user_info = UserInfo::get($values->apply_emp_id, "u");
        $condition = $user_info->get_emp_condition();

        // レコードを作成
        $bean = new KintaiHol2Record();
        $bean->apply_id      = $id;
        $bean->apply_date    = DateUtil::to_db_ymd($values->date_y1, $values->date_m1, $values->date_d1);
        $bean->apply_stat    = "0";
        $bean->emp_id        = $values->apply_emp_id;
        $bean->kind_flag     = $values->kind_flag;
        $bean->reason        = $values->reason;
        $bean->tmcd_group_id = $condition->tmcd_group_id;
        $bean->pattern       = $values->pattern;
        $bean->start_date    = DateUtil::to_db_ymd($values->date_y2, $values->date_m2, $values->date_d2);
        $bean->end_date      = DateUtil::to_db_ymd($values->date_y3, $values->date_m3, $values->date_d3);
        if ($bean->start_date == $bean->end_date) {
            $bean->start_time    = DateUtil::to_db_hi($values->start_time_hour, $values->start_time_minute);
            $bean->use_hour      = DateUtil::to_02d($values->use_time_hour);
            $bean->use_minute    = DateUtil::to_02d($values->use_time_minute);
        }
        $bean->remark        = $values->remark;
        $bean->detail        = $values->detail;

        // 登録
        KintaiHol2BasicModel::insert($bean);

        $this->log->info("hol-workflow-regist");
    }

    /**
     * 更新
     */
    public function update_apply($id) {
        $this->log->info("hol-workflow-update-start");

        // 内容更新の際にステータスが申請済の場合、
        // 内容更新後、再度承認処理を行う

        $values = $this->get_param();
        $this->log->debug(var_export($values, true));

        $user_info = UserInfo::get($values->apply_emp_id, "u");
        $condition = $user_info->get_emp_condition();

        $types = array(
            "text",
            "text",
            "text",
            "text",
            "text",
            "integer",
            "text",
            "text",
            "text",
            "text",
            "text",
            "text",
            "text"
        );

        $params = array(
            "emp_id" => $values->apply_emp_id,
            "apply_date" => DateUtil::to_db_ymd($values->date_y1, $values->date_m1, $values->date_d1),
            "kind_flag" => $values->kind_flag,
            "reason" => $values->reason,
            "apply_stat" => "0",
            "tmcd_group_id" => $condition->tmcd_group_id,
            "pattern" => $values->pattern,
            "start_date" => DateUtil::to_db_ymd($values->date_y2, $values->date_m2, $values->date_d2),
            "end_date" =>   DateUtil::to_db_ymd($values->date_y3, $values->date_m3, $values->date_d3),
            "start_time" => null,
            "use_hour" =>   null,
            "use_minute" => null,
            "remark" => $values->remark,
            "detail" => $values->detail
        );


        if ($values->start_date == $values->end_date && !empty($values->start_time_hour)) {
            $params["start_time"] = DateUtil::to_db_hi($values->start_time_hour, $values->start_time_minute);
            $params["use_hour"] = $values->use_time_hour;
            $params["use_minute"] = DateUtil::to_02d($values->use_time_minute);
        }

        KintaiHol2BasicModel::update($types, $params, $id);

        $this->log->info("hol-workflow-update-end");
    }

    /**
     * 取消し・否認
     */
    public function cancel_apply($id) {
        $this->log->info("hol-workflow-cancel-start");
        // 承認済の場合はデータをリストア
        $this->restore($id, "2");
        KintaiHol2BasicModel::delete($id);
        $this->log->info("hol-workflow-cancel-end");
    }

    /**
     * 再申請
     */
    public function regist_re_apply($new_id, $old_id) {
        $this->log->info("hol-workflow-re_apply-start");
        if ($_POST["kintai_template_type"] == "hol") {
            KintaiHol2BasicModel::delete($old_id);
            $this->regist_apply($new_id);
        } else {
            KintaiHol2BasicModel::re_apply($new_id, $old_id);
        }
        $this->log->info("hol-workflow-re_apply-end");
    }

    /**
     * 承認
     */
    public function approve($id, $con) {
        $this->log->info("hol-workflow-approve-start");
        
        // 元データを取得
        $data = KintaiHol2BasicModel::get($id);

        $this->log->debug(var_export($data, true));

        // 現在日を取得
        $now_date = date('Ymd', time());
        // start, end日付を配列に変換する
        $start_date = $data->start_date;
        $end_date = $data->end_date;

        $hol_list = array();

        // 分割
        $sp_start_date = DateUtil::split_ymd($start_date);
        $sp_end_date = DateUtil::split_ymd($end_date);
        $start_time = mktime(0,0,0,$sp_start_date['m'], $sp_start_date['d'], $sp_start_date['y']);
        $end_time = mktime(0,0,0,$sp_end_date['m'], $sp_end_date['d'], $sp_end_date['y']);
        for ($time = $start_time; $time <= $end_time; $time = $time + (60*60*24)) { // endまで1日ずつ足す
            array_push($hol_list, date('Ymd', $time));
        }
        $this->log->debug(var_export($hol_list, true));

        $emp_id = $data->emp_id;
        $reason = $data->reason;
        $pattern = $data->pattern;
        $group_id = $data->tmcd_group_id;
        $detail = $data->detail;

        $time_hol_input = !empty($data->start_time) ? true:false;

        // 勤務実績、勤務予定を更新
        foreach ($hol_list as $hol) {
            // 実績、予定のバックアップ
            AtdbkBackupModel::backup($emp_id, $hol);
            AtdbkRsltBackupModel::backup($emp_id, $hol);

            if (intval($now_date) >= intval($hol)) { // 事後の場合は実績のみ更新
                ApplyHol2DBAccess::atdbk_hol_approve($emp_id, $hol, $reason, $pattern, $group_id, $time_hol_input, "atdbkrslt", $detail);
            } else { // 事前の場合は予定を更新
                ApplyHol2DBAccess::atdbk_hol_approve($emp_id, $hol, $reason, $pattern, $group_id, $time_hol_input, "atdbk");
                //勤務パターン、事由が設定済か確認、設定済は予定に合わせる。未設定はそのままとする 20141212
                $arr_atdbkrslt = AtdbkRsltBackupModel::get_atdbkrslt($emp_id, $hol);
                if ($arr_atdbkrslt["pattern"] != "" || $arr_atdbkrslt["reason"] != "") {
                	ApplyHol2DBAccess::atdbk_hol_approve($emp_id, $hol, $reason, $pattern, $group_id, $time_hol_input, "atdbkrslt");
                }
            }
        }

        if (sizeof($hol_list) == 1 && $time_hol_input && $data->kind_flag == "1") {
            // 時間有休のバックアップ、リストアは不要　2013/09/28
            //TimecardPaidHolHourBackupModel::backup($emp_id, $start_date);

            $start_time = $data->start_time;
            $use_hour = $data->use_hour;
            $use_minute = $data->use_minute;
            ApplyHol2DBAccess::atdbk_time_hol_approve($emp_id, $start_date, $start_time, $use_hour, $use_minute);
        }

        // ステータスを承認へ変更
        $this->update_status($id, "1"); // 1:承認

        $this->log->info("hol-workflow-approve-end");
    }

    /**
     * ステータス更新
     */
    public function update_status($id, $status) {
        $this->log->info("hol-workflow-update-status-start");
        // データをリストア
        $this->restore($id, $status);
        KintaiHol2BasicModel::update(array("text"), array("apply_stat" => $status), $id);
        $this->log->info("hol-workflow-update-status-end");
    }

    /**
     * 削除
     */
    public function delete($id) {
        $this->log->info("hol-workflow-delete-start");
        // データのリストア 
        $this->restore($id, "2");
        // 申請テーブルを削除
        KintaiHol2BasicModel::delete($id);

        $this->log->info("hol-workflow-delete-end");
    }

    /**
     * 勤務実績、予定のリストア
     */
    private function restore($id, $status) {
        // 元データを取得
        $data = KintaiHol2BasicModel::get($id);
        
        // 承認済じゃなかったらリストア不要
        if ($data->apply_stat != "1" ) {
            return;        
        }
        // 差戻,否認のみ処理する
        if (!in_array($status, array("2", "3"))) {
            return;
        }
        
        // start, end日付を配列に変換する
        $start_date = $data->start_date;
        $end_date = $data->end_date;

        $emp_id = $data->emp_id;
        $hol_list = array(); // リストア対象の日付

        // 分割
        $sp_start_date = DateUtil::split_ymd($start_date);
        $sp_end_date = DateUtil::split_ymd($end_date);
        $start_time = mktime(0,0,0,$sp_start_date['m'], $sp_start_date['d'], $sp_start_date['y']);
        $end_time = mktime(0,0,0,$sp_end_date['m'], $sp_end_date['d'], $sp_end_date['y']);
        for ($time = $start_time; $time <= $end_time; $time = $time + (60*60*24)) { // endまで1日ずつ足す
            array_push($hol_list, date('Ymd', $time));
        }

        // 2013/09/28 時間有休をリストアすると勤務実績が削除される問題
        // 時間有休のリストア
        if (!empty($data->start_time)) {
            // 時間有休は申請しないと生成されないため、リストア＝削除
            ApplyHol2DBAccess::atdbk_time_hol_delete($emp_id, $start_date);
            // 事由が年休なら事由を空にupdateする
            ApplyHol2DBAccess::cancel_time_hol_hour($emp_id, $start_date);
        } else {
            // 勤務実績、予定のリストア
            foreach ($hol_list as $hol) {
                AtdbkBackupModel::restore($emp_id, $hol);
                AtdbkRsltBackupModel::restore($emp_id, $hol);
            }
        }
    }


    /**
     * 画面からパラメータを取得
     */
    private function get_param() {
        $values = new Hol2TemplateValues();
        $values->date_y1           = $_POST["date_y1"];
        $values->date_m1           = $_POST["date_m1"];
        $values->date_d1           = $_POST["date_d1"];
        $values->date_y2           = $_POST["date_y2"];
        $values->date_m2           = $_POST["date_m2"];
        $values->date_d2           = $_POST["date_d2"];
        $values->date_y3           = $_POST["date_y3"];
        $values->date_m3           = $_POST["date_m3"];
        $values->date_d3           = $_POST["date_d3"];
        $values->start_time_hour   = $_POST["start_time_hour"];
        $values->start_time_minute = $_POST["start_time_minute"];
        $values->use_time_hour     = $_POST["use_time_hour"];
        $values->use_time_minute   = $_POST["use_time_minute"];
        $values->apply_emp_id      = $_POST["apply_emp_id"];
        $values->kind_flag         = $_POST["kind_flag"][0];
        if($_POST["tpl_tag_flag"] == "1"){
        	$values->reason        = $_POST["reason"][0];
        	$values->pattern       = $_POST["pattern"][0];
        }else{
        	$values->reason        = $_POST["reason"];
        	$values->pattern       = $_POST["pattern"];
        }
        $values->remark            = $_POST["remark"];
        $values->detail            = $_POST["detail"][0];
        return $values;
    }
}

class Hol2TemplateValues {
    public $date_y1;
    public $date_m1;
    public $date_d1;
    public $date_y2;
    public $date_m2;
    public $date_d2;
    public $date_y3;
    public $date_m3;
    public $date_d3;
    public $start_time_hour;
    public $start_time_minute;
    public $use_time_hour;
    public $use_time_minute;
    public $apply_emp_id;
    public $kind_flag;
    public $reason;
    public $pattern;
    public $remark;
    public $detail;
}
?>
