<?php
require_once 'common_log_class.ini';

require_once 'kintai/common/abstract_workflow.php';
require_once 'kintai/common/user_info.php';
require_once 'kintai/common/date_util.php';
require_once 'kintai/common/mdb.php';
require_once 'kintai/common/workflow_common_util.php';

require_once 'kintai/model/kintai_hol_basic_model.php';
require_once 'kintai/model/kintai_hol_record.php';
require_once 'kintai/model/atdbk_backup_model.php';
require_once 'kintai/model/atdbkrslt_backup_model.php';
require_once 'kintai/model/timecard_paid_hol_hour_backup_model.php';

require_once 'kintai/individual/model/apply_hol_db_access.php';



class HolWorkflow extends AbstractWorkflow {

    private $log;

    public function __construct() {
        $this->log = new common_log_class(basename(__FILE__));
    }

    /**
     * 申請
     */
    public function regist_apply($id) {
        $this->log->info("hol-workflow-regist-Start");

        $values = $this->get_param();
        $this->log->debug(var_export($values, true));

        $user_info = UserInfo::get($values->apply_emp_id, "u");
        $condition = $user_info->get_emp_condition();

        $kind_flag = $values->kind_flag;

        // 遅刻、外出、早退、半日、半日代休、は抜ける
        if ($kind_flag == "9" || $kind_flag == "10" || $kind_flag == "12" || $kind_flag == "8" || $kind_flag == "15") {
            $this->log->info("hol-workflow-regist-End");
            return;
        }

        require_once 'kintai/individual/const.php';
        // 共通のレコードをセット
        $bean = new KintaiHolRecord();
        $bean->apply_id      = $id;
        $bean->apply_date    = date('Ymd', time());
        $bean->apply_stat    = "0";
        $bean->emp_id        = $values->apply_emp_id;
        $bean->kind_flag     = $kind_flag;
        $bean->tmcd_group_id = $condition->tmcd_group_id;
        
        $atdptn = 10; // デフォルトは10(休暇)
        $reason = $KIND_REASON[$kind_flag]; // 休暇事由を取得

        $mdb = new MDB($this->log);
        if (!$reason) { // 該当しない場合は別のとり方
            switch ($kind_flag) {
            case "2": // 半日有休
                $atdptn = $values->atdptn;
                $reason = ApplyHolDBAccess::get_atdptn_reason($condition->tmcd_group_id, $atdptn);
                break;
            case "7": // 勤務交代
                $atdptn = $values->atdptn;
                $reason = $values->reason;
                break;
            case "11": // 出張
                $atdptn = $mdb->one(
                    "select atdptn_id from atdptn where group_id = :group_id and atdptn_nm = :name", 
                    array("integer", "text"), 
                    array("group_id" => $condition->tmcd_group_id, "name" => $ATDPTN_TRIP_NAME));
                if (!$atdptn) {
                    $this->log->info("hol-workflow-regist-End");
                    return;
                }
                break;
            case "13": // 午前出張
                $atdptn = $mdb->one(
                    "select atdptn_id from atdptn where group_id = :group_id and atdptn_nm = :name", 
                    array("integer", "text"), 
                    array("group_id" => $condition->tmcd_group_id, "name" => $ATDPTN_TRIP_NAME_AM));
                if (!$atdptn) {
                    $this->log->info("hol-workflow-regist-End");
                    return;
                }
                break;
            case "14": // 午後出張
                $atdptn = $mdb->one(
                    "select atdptn_id from atdptn where group_id = :group_id and atdptn_nm = :name", 
                    array("integer", "text"), 
                    array("group_id" => $condition->tmcd_group_id, "name" => $ATDPTN_TRIP_NAME_PM));
                if (!$atdptn) {
                    $this->log->info("hol-workflow-regist-End");
                    return;
                }
                break;
            }
        }
        $bean->reason        = $reason;
        $bean->pattern       = $atdptn;

        // 入力タイプを取得
        $input_type = $TYPE[$kind_flag];
        switch($input_type) { 
        case "TYPE1":
            if ($this->require_date($values->date_y1, $values->date_m1, $values->date_d1)) {
                $bean->start_date1 = $values->date_y1.$values->date_m1.$values->date_d1;
                if ($this->require_date($values->date_y2, $values->date_m2, $values->date_d2)) {
                    $bean->end_date1 = $values->date_y2.$values->date_m2.$values->date_d2;
                } else {
                    $bean->end_date1 = $bean->start_date1;
                }
            } 
            if ($this->require_date($values->date_y5, $values->date_m5, $values->date_d5)) {
                $bean->start_date2 = $values->date_y5.$values->date_m5.$values->date_d5;
                if ($this->require_date($values->date_y6, $values->date_m6, $values->date_d6)) {
                    $bean->end_date2 = $values->date_y6.$values->date_m6.$values->date_d6;
                } else {
                    $bean->end_date2 = $bean->start_date2;
                }
            } 
            if ($this->require_date($values->date_y7, $values->date_m7, $values->date_d7)) {
                $bean->start_date3 = $values->date_y7.$values->date_m7.$values->date_d7;
                if ($this->require_date($values->date_y8, $values->date_m8, $values->date_d8)) {
                    $bean->end_date3 = $values->date_y8.$values->date_m8.$values->date_d8;
                } else {
                    $bean->end_date3 = $bean->start_date3;
                }
            } 
            if ($this->require_date($values->date_y9, $values->date_m9, $values->date_d9)) {
                $bean->start_date4 = $values->date_y9.$values->date_m9.$values->date_d9;
                if ($this->require_date($values->date_y10, $values->date_m10, $values->date_d10)) {
                    $bean->end_date4 = $values->date_y10.$values->date_m10.$values->date_d10;
                } else {
                    $bean->end_date4 = $bean->start_date4;
                }
            } 
            if ($this->require_date($values->date_y11, $values->date_m11, $values->date_d11)) {
                $bean->start_date5 = $values->date_y11.$values->date_m11.$values->date_d11;
                if ($this->require_date($values->date_y12, $values->date_m12, $values->date_d12)) {
                    $bean->end_date5 = $values->date_y12.$values->date_m12.$values->date_d12;
                } else {
                    $bean->end_date5 = $bean->start_date5;
                }
            } 
            break;
        case "TYPE2":
            $bean->start_date1    = DateUtil::to_db_ymd($values->date_y3, $values->date_m3, $values->date_d3);
            $bean->end_date1      = $bean->start_date1;
            break;
        case "TYPE3":
            $bean->start_date1    = DateUtil::to_db_ymd($values->date_y4, $values->date_m4, $values->date_d4);
            $bean->end_date1      = $bean->start_date1;
            break;
        }

        // 登録
        KintaiHolBasicModel::insert($bean);

        $this->log->info("hol-workflow-regist");
    }

    private function require_date($y, $m, $d) {
        return ($y && $m && $d);
    }

    /**
     * 更新
     */
    public function update_apply($id) {
        $this->log->info("hol-workflow-update-start");

        $values = $this->get_param();
        $this->log->debug(var_export($values, true));

        $user_info = UserInfo::get($values->apply_emp_id, "u");
        $condition = $user_info->get_emp_condition();

        $kind_flag = $values->kind_flag;

        // 遅刻、外出、早退、代休、半日代休は現データを削除して抜ける
        if ($kind_flag == "9" || $kind_flag == "10" || $kind_flag == "12" || $kind_flag == "8" || $kind_flag == "15") {
            KintaiHolBasicModel::delete($id);
            $this->log->info("hol-workflow-regist-End");
            return;
        }
        
        // 登録されていなかったらinsert(DB更新無し→有で更新した場合)
        $data = KintaiHolBasicModel::get($id);
        if (!$data->apply_id) {
            $this->regist_apply($id);
            return;
        }
        
        require_once 'kintai/individual/const.php';
        
        $atdptn = 10; // デフォルトは10(休暇)
        $reason = $KIND_REASON[$kind_flag]; // 休暇事由を取得
        $mdb = new MDB($this->log);
        if (!$reason) { // 該当しない場合は別のとり方
            switch ($kind_flag) {
            case "2": // 半日有休
                $atdptn = $values->atdptn;
                $reason = ApplyHolDBAccess::get_atdptn_reason($condition->tmcd_group_id, $atdptn);
                break;
            case "7": // 勤務交代
                $atdptn = $values->atdptn;
                $reason = $values->reason;
                break;
            case "11": // 出張
                $atdptn = $mdb->one(
                    "select atdptn_id from atdptn where group_id = :group_id and atdptn_nm = :name", 
                    array("integer", "text"), 
                    array("group_id" => $condition->tmcd_group_id, "name" => $ATDPTN_TRIP_NAME));
                if (!$atdptn) {
                    $this->log->info("hol-workflow-regist-End");
                    return;
                }
                break;
            case "13": // 午前出張
                $atdptn = $mdb->one(
                    "select atdptn_id from atdptn where group_id = :group_id and atdptn_nm = :name", 
                    array("integer", "text"), 
                    array("group_id" => $condition->tmcd_group_id, "name" => $ATDPTN_TRIP_NAME_AM));
                if (!$atdptn) {
                    $this->log->info("hol-workflow-regist-End");
                    return;
                }
                break;
            case "14": // 午後出張
                $atdptn = $mdb->one(
                    "select atdptn_id from atdptn where group_id = :group_id and atdptn_nm = :name", 
                    array("integer", "text"), 
                    array("group_id" => $condition->tmcd_group_id, "name" => $ATDPTN_TRIP_NAME_PM));
                if (!$atdptn) {
                    $this->log->info("hol-workflow-regist-End");
                    return;
                }
                break;
            }
        }

        // 入力タイプを取得
        $input_type = $TYPE[$kind_flag];
        switch($input_type) { 
        case "TYPE1":
            if ($this->require_date($values->date_y1, $values->date_m1, $values->date_d1)) {
                $start_date1 = $values->date_y1.$values->date_m1.$values->date_d1;
                if ($this->require_date($values->date_y2, $values->date_m2, $values->date_d2)) {
                    $end_date1 = $values->date_y2.$values->date_m2.$values->date_d2;
                } else {
                    $end_date1 = $start_date1;
                }
            } 
            if ($this->require_date($values->date_y5, $values->date_m5, $values->date_d5)) {
                $start_date2 = $values->date_y5.$values->date_m5.$values->date_d5;
                if ($this->require_date($values->date_y6, $values->date_m6, $values->date_d6)) {
                    $end_date2 = $values->date_y6.$values->date_m6.$values->date_d6;
                } else {
                    $end_date2 = $start_date2;
                }
            } 
            if ($this->require_date($values->date_y7, $values->date_m7, $values->date_d7)) {
                $start_date3 = $values->date_y7.$values->date_m7.$values->date_d7;
                if ($this->require_date($values->date_y8, $values->date_m8, $values->date_d8)) {
                    $end_date3 = $values->date_y8.$values->date_m8.$values->date_d8;
                } else {
                    $end_date3 = $start_date3;
                }
            } 
            if ($this->require_date($values->date_y9, $values->date_m9, $values->date_d9)) {
                $start_date4 = $values->date_y9.$values->date_m9.$values->date_d9;
                if ($this->require_date($values->date_y10, $values->date_m10, $values->date_d10)) {
                    $end_date4 = $values->date_y10.$values->date_m10.$values->date_d10;
                } else {
                    $end_date4 = $start_date4;
                }
            } 
            if ($this->require_date($values->date_y11, $values->date_m11, $values->date_d11)) {
                $start_date5 = $values->date_y11.$values->date_m11.$values->date_d11;
                if ($this->require_date($values->date_y12, $values->date_m12, $values->date_d12)) {
                    $end_date5 = $values->date_y12.$values->date_m12.$values->date_d12;
                } else {
                    $end_date5 = $start_date5;
                }
            } 
            break;
        case "TYPE2":
            $start_date1    = DateUtil::to_db_ymd($values->date_y3, $values->date_m3, $values->date_d3);
            $end_date1      = $start_date1;
            break;
        case "TYPE3":
            $start_date1    = DateUtil::to_db_ymd($values->date_y4, $values->date_m4, $values->date_d4);
            $end_date1      = $start_date1;
            break;
        }

        $types = array(
            "text",
            "text",
            "text",
            "text",
            "text",
            "integer",
            "text",
            "text",
            "text",
            "text",
            "text",
            "text",
            "text",
            "text",
            "text",
            "text",
            "text",
            "text",
            "text",
            "text",
            "text"
        );

        $params = array(
            "emp_id"        => $values->apply_emp_id,
            "apply_date"    => date('Ymd', time()),
            "kind_flag"     => $kind_flag,
            "reason"        => $reason,
            "apply_stat"    => "0",
            "tmcd_group_id" => $condition->tmcd_group_id,
            "pattern"       => $atdptn,
            "start_date1"    => $start_date1,
            "end_date1"      => $end_date1,
            "start_time"    => null,
            "use_hour"      => null,
            "use_minute"    => null,
            "remark"        => null,
            "start_date2"    => $start_date2,
            "end_date2"      => $end_date2,
            "start_date3"    => $start_date3,
            "end_date3"      => $end_date3,
            "start_date4"    => $start_date4,
            "end_date4"      => $end_date4,
            "start_date5"    => $start_date5,
            "end_date5"      => $end_date5,
        );

        KintaiHolBasicModel::update($types, $params, $id);

        $this->log->info("hol-workflow-update-end");
    }

    /**
     * 取消し・否認
     */
    public function cancel_apply($id) {
        $this->log->info("hol-workflow-cancel-start");
        // 承認済の場合はデータをリストア
        $this->restore($id, "2");
        KintaiHolBasicModel::delete($id);
        $this->log->info("hol-workflow-cancel-end");
    }

    /**
     * 再申請
     */
    public function regist_re_apply($new_id, $old_id) {
        $this->log->info("hol-workflow-re_apply-start");
        if ($_POST["kintai_template_type"] == "hol") {
            KintaiHolBasicModel::delete($old_id);
            $this->regist_apply($new_id);
        } else {
            KintaiHolBasicModel::re_apply($new_id, $old_id);
        }
        $this->log->info("hol-workflow-re_apply-end");
    }


    private function get_hol_list( $start_date1, $end_date1,
        $start_date2, $end_date2, $start_date3, $end_date3,
        $start_date4, $end_date4, $start_date5, $end_date5) 
    {
        $hol_list = array();

        $work_date_list = array(
            array("start" => $start_date1, "end" => $end_date1),
            array("start" => $start_date2, "end" => $end_date2),
            array("start" => $start_date3, "end" => $end_date3),
            array("start" => $start_date4, "end" => $end_date4),
            array("start" => $start_date5, "end" => $end_date5),
        );

        foreach ($work_date_list as $item) {
            if (!$item["start"] || !$item["end"]) {
                continue;
            }
            $sp_start_date = DateUtil::split_ymd($item["start"]);
            $sp_end_date = DateUtil::split_ymd($item["end"]);
            $start_time = mktime(0,0,0,$sp_start_date['m'], $sp_start_date['d'], $sp_start_date['y']);
            $end_time = mktime(0,0,0,$sp_end_date['m'], $sp_end_date['d'], $sp_end_date['y']);
            for ($time = $start_time; $time <= $end_time; $time = $time + (60*60*24)) { // endまで1日ずつ足す
                array_push($hol_list, date('Ymd', $time));
            }
        }
        
        return array_unique($hol_list);

    }
    /**
     * 承認
     */
    public function approve($id, $con) {
        $this->log->info("hol-workflow-approve-start");

        // ワークフロー大元の承認が確定しているか
        if (!WorkflowCommonUtil::is_apply_complete($con, $id)) {
            echo WorkflowCommonUtil::is_apply_complete($con, $id);
            return;
        }

        // 元データを取得
        $data = KintaiHolBasicModel::get($id);

        $this->log->debug(var_export($data, true));

        // 現在日を取得
        $now_date = date('Ymd', time());
        // start, end日付を配列に変換する
        $hol_list = $this->get_hol_list(
            $data->start_date1, $data->end_date1,
            $data->start_date2, $data->end_date2,
            $data->start_date3, $data->end_date3,
            $data->start_date4, $data->end_date4,
            $data->start_date5, $data->end_date5
        );
        $this->log->debug(var_export($hol_list, true));

        $emp_id = $data->emp_id;
        $reason = $data->reason;
        $pattern = $data->pattern;
        $group_id = $data->tmcd_group_id;

        $time_hol_input = !empty($data->start_time) ? true:false;

        // 勤務実績、勤務予定を更新
        foreach ($hol_list as $hol) {
            // 実績、予定のバックアップ
            AtdbkBackupModel::backup($emp_id, $hol);
            AtdbkRsltBackupModel::backup($emp_id, $hol);

            if (intval($now_date) >= intval($hol)) { // 事後の場合は実績のみ更新
                ApplyHolDBAccess::atdbk_hol_approve($emp_id, $hol, $reason, $pattern, $group_id, $time_hol_input, "atdbkrslt");
            } else { // 事前の場合は予定を更新。実績は更新しない（自動打刻でコピーするため） 20130304
                ApplyHolDBAccess::atdbk_hol_approve($emp_id, $hol, $reason, $pattern, $group_id, $time_hol_input, "atdbk");
                //勤務パターン、事由が設定済か確認、設定済は予定に合わせる。未設定はそのままとする 20150306
                $arr_atdbkrslt = AtdbkRsltBackupModel::get_atdbkrslt($emp_id, $hol);
                if ($arr_atdbkrslt["pattern"] != "" || $arr_atdbkrslt["reason"] != "") {
                	ApplyHolDBAccess::atdbk_hol_approve($emp_id, $hol, $reason, $pattern, $group_id, $time_hol_input, "atdbkrslt");
                }
            }
        }
/*
        if (sizeof($hol_list) == 1 && $time_hol_input) {
            // 時間有休のバックアップ
            TimecardPaidHolHourBackupModel::backup($emp_id, $start_date);

            $start_time = $data->start_time;
            $use_hour = $data->use_hour;
            $use_minute = $data->use_minute;
            ApplyHolDBAccess::atdbk_time_hol_approve($emp_id, $start_date, $start_time, $use_hour, $use_minute);
        }
*/
        // ステータスを承認へ変更
        $this->update_status($id, "1"); // 1:承認

        $this->log->info("hol-workflow-approve-end");
    }

    /**
     * ステータス更新
     */
    public function update_status($id, $status) {
        $this->log->info("hol-workflow-update-status-start");
        // データをリストア
        $this->restore($id, $status);
        KintaiHolBasicModel::update(array("text"), array("apply_stat" => $status), $id);
        $this->log->info("hol-workflow-update-status-end");
    }

    /**
     * 削除
     */
    public function delete($id) {
        $this->log->info("hol-workflow-delete-start");
        // データのリストア 
        $this->restore($id, "2");
        // 申請テーブルを削除
        KintaiHolBasicModel::delete($id);

        $this->log->info("hol-workflow-delete-end");
    }

    /**
     * 勤務実績、予定のリストア
     */
    private function restore($id, $status) {
        // 元データを取得
        $data = KintaiHolBasicModel::get($id);

        // 承認済じゃなかったらリストア不要
        if ($data->apply_stat != "1" ) {
            return;        
        }
        // 差戻,否認のみ処理する
        if (!in_array($status, array("2", "3"))) {
            return;
        }

        $emp_id = $data->emp_id;
        // start, end日付を配列に変換する
        $hol_list = $this->get_hol_list(
            $data->start_date1, $data->end_date1,
            $data->start_date2, $data->end_date2,
            $data->start_date3, $data->end_date3,
            $data->start_date4, $data->end_date4,
            $data->start_date5, $data->end_date5
        );
        $this->log->debug(var_export($hol_list, true));


        // 勤務実績、予定のリストア
        foreach ($hol_list as $hol) {
            AtdbkBackupModel::restore($emp_id, $hol);
            AtdbkRsltBackupModel::restore($emp_id, $hol);
        }

        // 時間有休のリストア
        if (!empty($data->start_time)) {
            TimecardPaidHolHourBackupModel::backup($emp_id, $start_date);
        }
    }


    /**
     * 画面からパラメータを取得
     */
    private function get_param() {
        $values = new HolTemplateValues();
        $values->date_y1           = $_POST["date_y1"];
        $values->date_m1           = $_POST["date_m1"];
        $values->date_d1           = $_POST["date_d1"];
        $values->date_y2           = $_POST["date_y2"];
        $values->date_m2           = $_POST["date_m2"];
        $values->date_d2           = $_POST["date_d2"];
        $values->date_y3           = $_POST["date_y3"];
        $values->date_m3           = $_POST["date_m3"];
        $values->date_d3           = $_POST["date_d3"];
        $values->date_y4           = $_POST["date_y4"];
        $values->date_m4           = $_POST["date_m4"];
        $values->date_d4           = $_POST["date_d4"];
        $values->date_y5           = $_POST["date_y5"];
        $values->date_m5           = $_POST["date_m5"];
        $values->date_d5           = $_POST["date_d5"];
        $values->date_y6           = $_POST["date_y6"];
        $values->date_m6           = $_POST["date_m6"];
        $values->date_d6           = $_POST["date_d6"];
        $values->date_y7           = $_POST["date_y7"];
        $values->date_m7           = $_POST["date_m7"];
        $values->date_d7           = $_POST["date_d7"];
        $values->date_y8           = $_POST["date_y8"];
        $values->date_m8           = $_POST["date_m8"];
        $values->date_d8           = $_POST["date_d8"];
        $values->date_y9           = $_POST["date_y9"];
        $values->date_m9           = $_POST["date_m9"];
        $values->date_d9           = $_POST["date_d9"];
        $values->date_y10           = $_POST["date_y10"];
        $values->date_m10           = $_POST["date_m10"];
        $values->date_d10           = $_POST["date_d10"];
        $values->date_y11           = $_POST["date_y11"];
        $values->date_m11           = $_POST["date_m11"];
        $values->date_d11           = $_POST["date_d11"];
        $values->date_y12           = $_POST["date_y12"];
        $values->date_m12           = $_POST["date_m12"];
        $values->date_d12           = $_POST["date_d12"];
        $values->start_time_hour   = $_POST["start_time_hour"];
        $values->start_time_minute = $_POST["start_time_minute"];
        $values->end_time_hour     = $_POST["end_time_hour"];
        $values->end_time_minute   = $_POST["end_time_minute"];
        $values->apply_emp_id      = $_POST["apply_emp_id"];
        $values->kind_flag         = $_POST["kind_flag"][0];
        $values->reason            = $_POST["reason"];
        $values->atdptn            = $_POST["atdptn"];
        $values->remark            = $_POST["remark"];
        return $values;
    }
}

class HolTemplateValues {
    public $date_y1;
    public $date_m1;
    public $date_d1;
    public $date_y2;
    public $date_m2;
    public $date_d2;
    public $date_y3;
    public $date_m3;
    public $date_d3;
    public $date_y4;
    public $date_m4;
    public $date_d4;
    public $date_y5;
    public $date_m5;
    public $date_d5;
    public $date_y6;
    public $date_m6;
    public $date_d6;
    public $date_y7;
    public $date_m7;
    public $date_d7;
    public $date_y8;
    public $date_m8;
    public $date_d8;
    public $date_y9;
    public $date_m9;
    public $date_d9;
    public $date_y10;
    public $date_m10;
    public $date_d10;
    public $date_y11;
    public $date_m11;
    public $date_d11;
    public $date_y12;
    public $date_m12;
    public $date_d12;
    public $start_time_hour;
    public $start_time_minute;
    public $end_time_hour;
    public $end_time_minute;
    public $apply_emp_id;
    public $kind_flag;
    public $reason;
    public $atdptn;
    public $remark;
}
?>
