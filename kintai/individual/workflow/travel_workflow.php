<?php
require_once 'common_log_class.ini';

require_once 'kintai/common/abstract_workflow.php';
require_once 'kintai/common/user_info.php';
require_once 'kintai/common/date_util.php';
require_once 'kintai/common/mdb.php';

require_once 'kintai/model/travel_wkfw_model.php';

class TravelWorkflow extends AbstractWorkflow {

    private $log;

    public function __construct() {
        $this->log = new common_log_class(basename(__FILE__));
    }

    /**
     * 申請
     */
    public function regist_apply($id) {
        $this->log->info("travel-workflow-regist-start");

        $values = $this->get_param();
        $this->log->debug(var_export($values, true));

        $info["apply_id"]    = $id;
        $info["apply_date"]  = DateUtil::to_db_ymd($values->date_y1, $values->date_m1, $values->date_d1); 
        $info["apply_stat"]  = "0"; 
        $info["emp_id"]      = $values->apply_emp_id; 
        $info["title"]       = $values->title; 
        $info["destination"] = $values->destination; 
        $info["start_date"]  = DateUtil::to_db_ymd($values->date_y2, $values->date_m2, $values->date_d2); 
        $info["end_date"]    = DateUtil::to_db_ymd($values->date_y3, $values->date_m3, $values->date_d3); 
        $info["amount1"]     = $values->amount1; 
        $info["amount2"]     = $values->amount2; 
        $info["amount3"]     = null; 
        $info["amount4"]     = null; 
        $info["remark"]      = $values->remark;
        
        // 登録
        TravelWorkflowModel::insert($info);

        $this->log->info("travel-workflow-regist-end");
    }

    /**
     * 更新
     */
    public function update_apply($id) {
        $this->log->info("travel-workflow-update-start");

        $values = $this->get_param();
        $this->log->debug(var_export($values, true));

        $info["apply_id"]    = $id;
        $info["apply_date"]  = DateUtil::to_db_ymd($values->date_y1, $values->date_m1, $values->date_d1); 
        $info["apply_stat"]  = "0"; 
        $info["emp_id"]      = $values->apply_emp_id; 
        $info["title"]       = $values->title; 
        $info["destination"] = $values->destination; 
        $info["start_date"]  = DateUtil::to_db_ymd($values->date_y2, $values->date_m2, $values->date_d2); 
        $info["end_date"]    = DateUtil::to_db_ymd($values->date_y3, $values->date_m3, $values->date_d3); 
        $info["amount1"]     = $values->amount1; 
        $info["amount2"]     = $values->amount2; 
        $info["amount3"]     = null; 
        $info["amount4"]     = null; 
        $info["remark"]      = $values->remark;

        TravelWorkflowModel::update($info);

        $this->log->info("travel-workflow-update-end");
    }

    /**
     * 取消し・否認
     */
    public function cancel_apply($id) {
        $this->log->info("travel-workflow-cancel-start");
        TravelWorkflowModel::delete($id);
        $this->log->info("travel-workflow-cancel-end");
    }

    /**
     * 再申請
     */
    public function regist_re_apply($new_id, $old_id) {
        $this->log->info("travel-workflow-re_apply-start");

        if ($_POST["kintai_template_type"] == "travel") {
            TravelWorkflowModel::delete($old_id);
            $this->regist_apply($new_id);
        } else {
            $old_apply = TravelWorkflowModel::get($old_id);

            $info["apply_id"]    = $new_id;
            $info["apply_date"]  = $old_apply->apply_date;
            $info["apply_stat"]  = $old_apply->apply_stat;
            $info["emp_id"]      = $old_apply->emp_id;
            $info["title"]       = $old_apply->title;
            $info["destination"] = $old_apply->destination;
            $info["start_date"]  = $old_apply->start_date;
            $info["end_date"]    = $old_apply->end_date; 
            $info["amount1"]     = $old_apply->amount1;
            $info["amount2"]     = $old_apply->amount2;
            $info["amount3"]     = $old_apply->amount3;
            $info["amount4"]     = $old_apply->amount4;
            $info["remark"]      = $old_apply->remark;

            TravelWorkflowModel::insert($info);
            TravelWorkflowModel::delete($old_id);
        }
        $this->log->info("travel-workflow-re_apply-end");
    }

    /**
     * 承認
     */
    public function approve($id, $con) {
        $this->log->info("travel-workflow-approve-start");
        $this->update_status($id, "1"); // 1:承認
        $this->log->info("travel-workflow-approve-end");
    }

    /**
     * ステータス更新
     */
    public function update_status($id, $status) {
        $this->log->info("travel-workflow-update-status-start");
        TravelWorkflowModel::update_status($id, $status);
        $this->log->info("travel-workflow-update-status-end");
    }

    /**
     * 削除
     */
    public function delete($id) {
        $this->log->info("travel-workflow-delete-start");
        TravelWorkflowModel::delete($id);
        $this->log->info("travel-workflow-delete-end");
    }

    /**
     * 画面からパラメータを取得
     */
    private function get_param() {
        $values = new stdclass;
        $values->date_y1      = $_POST["date_y1"];
        $values->date_m1      = $_POST["date_m1"];
        $values->date_d1      = $_POST["date_d1"];
        $values->date_y2      = $_POST["date_y2"];
        $values->date_m2      = $_POST["date_m2"];
        $values->date_d2      = $_POST["date_d2"];
        $values->date_y3      = $_POST["date_y3"];
        $values->date_m3      = $_POST["date_m3"];
        $values->date_d3      = $_POST["date_d3"];
        $values->apply_emp_id = $_POST["apply_emp_id"];
        $values->title        = $_POST["trip_title"];
        $values->destination  = $_POST["destination"];
        $values->amount1      = $_POST["amount1"];
        $values->amount2      = $_POST["amount2"];
        $values->remark       = $_POST["remark"];
        return $values;
    }
}
?>
