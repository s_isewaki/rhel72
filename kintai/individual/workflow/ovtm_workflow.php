<?php
require_once 'common_log_class.ini';

require_once 'kintai/common/abstract_workflow.php';
require_once 'kintai/common/user_info.php';
require_once 'kintai/common/date_util.php';
require_once 'kintai/common/mdb.php';

require_once 'kintai/model/overtime_basic_model.php';
require_once 'kintai/model/overtime_basic_sum_model.php';

require_once 'kintai/individual/model/overtime_kbn.php';

class OvtmWorkflow extends AbstractWorkflow {

    private $log;

    public function __construct() {
        $this->log = new common_log_class(basename(__FILE__));
    }

    /**
     * 申請
     */
    public function regist_apply($id) {
        $this->log->info("ovtm-workflow-regist-Start");

        $values = $this->get_param();

        // 過去の申請がある場合はupdateする
        $result = OvertimeBasicModel::get($values->date, $values->emp_id, true);

        $update = false;

        if (!empty($result)) {
            // ステータスが"承認済"でなければいけない
            if ($result->apply_stat != "1") {
                echo("<script language='javascript'>alert('ワークフロー中の申請があるため、申請できません。');</script>");
                echo("<script language='javascript'>history.back();</script>");
                exit;
            } else {
                $update = true;
            }
        }

        if ($update) {
            $this->update_apply($id);
        } else {

            // 申請IDやステータスを追加
            $values->apply_id = $id;
            $values->apply_stat = "0";
            // 登録
            OvertimeBasicModel::insert($values);
            $ovtm_sum = OvertimeKbn::create_from_submit_form(); // 画面から取得
            $ovtm_sum->update($values->emp_id, $values->date);
        }

        $this->log->info("ovtm-workflow-regist-end");
    }

    /**
     * 更新
     */
    public function update_apply($id) {
        $this->log->info("ovtm-workflow-update-start");

        $values = $this->get_param();

        $result = OvertimeBasicModel::get_from_apply_id($id);
        $this->log->debug("registerd".$result->date);
        $this->log->debug("posted   ".$values->date);
        // 実績日が変更される場合は更新updateできない
        if (!empty($result) && $values->date != $result->date) { // delete & insert
            $this->regist_apply($id);
            OvertimeBasicModel::delete($result->emp_id, $result->date);
            OvertimeBasicSumModel::delete($return->emp_id, $return->date);
            $this->log->info("ovtm-workflow-update-end");
            return;
        }

        $apply_stat = 0;
        if ($values->emp_id != $values->current_user_id) {
            $apply_stat = $result->apply_stat;
        }

        $types = array(
            "text","text",

            "text","integer","text","integer","text","integer","text",
            "text","integer","text","integer","text","integer","text",
            "text","integer","text","integer","text","integer","text",

            "integer", "integer", "integer", "integer"
        );
        $params = array(
            "apply_date" => $values->apply_date,
            "apply_stat" => $apply_stat,
            "rslt_start_time1" => $values->rslt_start_time1,
            "rslt_start_next_flag1" => $values->rslt_start_next_flag1,
            "rslt_end_time1" => $values->rslt_end_time1,
            "rslt_end_next_flag1" => $values->rslt_end_next_flag1,
            "rslt_rest_time1" => $values->rslt_rest_time1,
            "rslt_ovtmrsn_id1" => $values->rslt_ovtmrsn_id1,
            "rslt_ovtmrsn_txt1" => $values->rslt_ovtmrsn_txt1,
            "rslt_start_time2" => $values->rslt_start_time2,
            "rslt_start_next_flag2" => $values->rslt_start_next_flag2,
            "rslt_end_time2" => $values->rslt_end_time2,
            "rslt_end_next_flag2" => $values->rslt_end_next_flag2,
            "rslt_rest_time2" => $values->rslt_rest_time2,
            "rslt_ovtmrsn_id2" => $values->rslt_ovtmrsn_id2,
            "rslt_ovtmrsn_txt2" => $values->rslt_ovtmrsn_txt2,
            "rslt_start_time3" => $values->rslt_start_time3,
            "rslt_start_next_flag3" => $values->rslt_start_next_flag3,
            "rslt_end_time3" => $values->rslt_end_time3,
            "rslt_end_next_flag3" => $values->rslt_end_next_flag3,
            "rslt_rest_time3" => $values->rslt_rest_time3,
            "rslt_ovtmrsn_id3" => $values->rslt_ovtmrsn_id3,
            "rslt_ovtmrsn_txt3" => $values->rslt_ovtmrsn_txt3,
            "apply_id" => $id,
            "rslt_call_flag1" => $values->rslt_call_flag1,
            "rslt_call_flag2" => $values->rslt_call_flag2,
            "rslt_call_flag3" => $values->rslt_call_flag3   
        );

        OvertimeBasicModel::update($types, $params, $values->emp_id, $values->date);

        $ovtm_sum = OvertimeKbn::create_from_submit_form(); // 画面から取得

        $ovtm_sum->update($values->emp_id, $values->date);

        $this->log->info("ovtm-workflow-update-end");
    }

    /**
     * 取消し・否認
     */
    public function cancel_apply($id) {
        $this->log->info("ovtm-workflow-cancel-start");

        // 申請IDでレコード取得
        // データがない場合は新しい申請があるため、何もしない
        $result = OvertimeBasicModel::get_from_apply_id($id);
        if (empty($result)) {
            $this->log->info("ovtm-workflow-cancel-end");
            return;
        }

        OvertimeBasicModel::delete($result->emp_id, $result->date);
        OvertimeBasicSumModel::delete($result->emp_id, $result->date);

        $this->log->info("ovtm-workflow-cancel-end");
    }

    /**
     * 再申請
     */
    public function regist_re_apply($new_id, $old_id) {
        $this->log->info("ovtm-workflow-re_apply-start");

        if ($_POST["kintai_template_type"] == "ovtm") {
            $result = OvertimeBasicModel::get_from_apply_id($old_id);
            if ($result) {
                OvertimeBasicModel::delete($result->emp_id, $result->date);
                $this->regist_apply($new_id);
            }
        } else {
            OvertimeBasicModel::re_apply($new_id, $old_id);
        }

        $this->log->info("ovtm-workflow-re_apply-end");
    }

    /**
     * 承認
     */
    public function approve($id, $con) {
        $this->log->info("ovtm-workflow-approve-start");

        // ステータスを承認へ変更
        $this->update_status($id, "1"); // 1:承認

        $this->log->info("ovtm-workflow-approve-end");
    }

    /**
     * ステータス更新
     */
    public function update_status($id, $status) {
        $this->log->info("ovtm-workflow-update-status-start");
        $result = OvertimeBasicModel::get_from_apply_id($id);
        OvertimeBasicModel::update(array("text"), array("apply_stat" => $status), $result->emp_id, $result->date);
        $this->log->info("ovtm-workflow-update-status-end");
    }

    /**
     * ワークフロー削除
     */
    public function delete($id) {
        $this->log->info("ovtm-workflow-delete-start");

        $ovtm_info = OvertimeBasicModel::get_from_apply_id($id);

        if (!empty($ovtm_info)) {
            OvertimeBasicModel::delete($ovtm_info->emp_id, $ovtm_info->date);
            OvertimeBasicSumModel::delete($ovtm_info->emp_id, $ovtm_info->date);
        }

        $this->log->info("ovtm-workflow-delete-end");
    }

    /**
     * 画面からパラメータを取得
     */
    private function get_param() {
        $values;
        $values->emp_id     = $_POST["apply_emp_id"];
        $values->current_user_id = $_POST["current_user_id"];
        $values->date       = DateUtil::to_db_ymd($_POST["rslt_year"], $_POST["rslt_month"], $_POST["rslt_day"]);
        $values->apply_date = DateUtil::to_db_ymd($_POST["date_y1"], $_POST["date_m1"], $_POST["date_d1"]);

        for ($i = 1; $i <= 3; $i++) {
            $rslt_start_time      = "rslt_start_time"      .$i; 
            $rslt_start_next_flag = "rslt_start_next_flag" .$i; 
            $rslt_end_time        = "rslt_end_time"        .$i; 
            $rslt_end_next_flag   = "rslt_end_next_flag"   .$i; 
            $rslt_rest_time       = "rslt_rest_time"       .$i; 
            $rslt_ovtmrsn_id      = "rslt_ovtmrsn_id"      .$i; 
            $rslt_ovtmrsn_txt      = "rslt_ovtmrsn_txt"      .$i; 
            $rslt_call_flag       = "rslt_call_flag"       .$i; 

            $rslt_start_hour      = "rslt_start_hour"      .$i; 
            $rslt_start_minute    = "rslt_start_minute"    .$i; 
            $rslt_end_hour        = "rslt_end_hour"        .$i; 
            $rslt_end_minute      = "rslt_end_minute"      .$i; 
            $rslt_rest_hour       = "rslt_rest_hour"       .$i; 
            $rslt_rest_minute     = "rslt_rest_minute"     .$i; 

            $values->$rslt_start_time      = DateUtil::to_db_hi($_POST[$rslt_start_hour], $_POST[$rslt_start_minute]);
            $values->$rslt_start_next_flag = is_array($_POST[$rslt_start_next_flag]) ? $_POST[$rslt_start_next_flag][0]:$_POST[$rslt_start_next_flag];
            $values->$rslt_end_time        = DateUtil::to_db_hi($_POST[$rslt_end_hour], $_POST[$rslt_end_minute]);
            $values->$rslt_end_next_flag   = is_array($_POST[$rslt_end_next_flag]) ? $_POST[$rslt_end_next_flag][0]:$_POST[$rslt_end_next_flag];
            $values->$rslt_rest_time       = DateUtil::to_db_hi($_POST[$rslt_rest_hour], $_POST[$rslt_rest_minute]);
            $values->$rslt_ovtmrsn_id      = $_POST[$rslt_ovtmrsn_id];
            $values->$rslt_ovtmrsn_txt      = $_POST[$rslt_ovtmrsn_txt];
            $values->$rslt_call_flag       = is_array($_POST[$rslt_call_flag]) ? $_POST[$rslt_call_flag][0]:$_POST[$rslt_call_flag];
        }
        $this->log->debug("POST :".var_export($_POST, true));
        $this->log->debug("param:".var_export($values, true));

        return $values;
    }
}
?>
