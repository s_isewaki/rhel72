<?php
require_once 'common_log_class.ini';

require_once 'kintai/common/abstract_workflow.php';
require_once 'kintai/common/user_info.php';
require_once 'kintai/common/date_util.php';
require_once 'kintai/common/mdb.php';

require_once 'kintai/model/kintai_late_basic_model.php';
require_once 'kintai/model/kintai_late_record.php';

require_once 'kintai/individual/model/apply_late_db_access.php';


class LateWorkflow extends AbstractWorkflow {

    private $log;
    
    public function __construct() {
        $this->log = new common_log_class(basename(__FILE__));
    }

    /**
     * 申請
     */
    public function regist_apply($id) {
        $this->log->info("late-workflow-regist-Start");

        $values = $this->get_param();
        $this->log->debug(var_export($values, true));

        $user_info = UserInfo::get($values->apply_emp_id, "u");
        $condition = $user_info->get_emp_condition();

        // レコードを作成
        $bean = new KintaiLateRecord();
        $bean->apply_id      = $id;
        $bean->apply_date    = DateUtil::to_db_ymd($values->date_y1, $values->date_m1, $values->date_d1);
        $bean->apply_stat    = "0";
        $bean->emp_id        = $values->apply_emp_id;
        $bean->kind_flag     = $values->kind_flag;
        $bean->start_date    = DateUtil::to_db_ymd($values->date_y2, $values->date_m2, $values->date_d2);
        $bean->start_time    = DateUtil::to_db_hi($values->start_time_hour, $values->start_time_minute);
        $bean->end_date      = DateUtil::to_db_ymd($values->date_y3, $values->date_m3, $values->date_d3);
        $bean->end_time      = DateUtil::to_db_hi($values->end_time_hour, $values->end_time_minute);
        $bean->reason_id     = $values->reason_id;

        // 登録
        KintaiLateBasicModel::insert($bean);

        $this->log->info("late-workflow-regist");
    }

    /**
     * 更新
     */
    public function update_apply($id) {
        $this->log->info("late-workflow-update-start");

        // 内容更新の際にステータスが申請済の場合、
        // 内容更新後、再度承認処理を行う

        $values = $this->get_param();
        $this->log->debug(var_export($values, true));

        $user_info = UserInfo::get($values->apply_emp_id, "u");
        $condition = $user_info->get_emp_condition();

        $types = array(
            "text",
            "text",
            "text",
            "text",
            "text",
            "text",
            "text",
            "text",
            "integer"
        );

        $params = array(
            "apply_date" => DateUtil::to_db_ymd($values->date_y1, $values->date_m1, $values->date_d1),
            "apply_stat" => "0",
            "emp_id" => $values->apply_emp_id,
            "kind_flag" => $values->kind_flag,
            "start_date" => DateUtil::to_db_ymd($values->date_y2, $values->date_m2, $values->date_d2),
            "start_time" => DateUtil::to_db_hi($values->start_time_hour, $values->start_time_minute),
            "end_date" => DateUtil::to_db_ymd($values->date_y3, $values->date_m3, $values->date_d3),
            "end_time" => DateUtil::to_db_hi($values->end_time_hour, $values->end_time_minute),
            "reason_id" => $values->reason_id
        );


        KintaiLateBasicModel::update($types, $params, $id);

        $this->log->info("late-workflow-update-end");
    }

    /**
     * 取消し・否認
     */
    public function cancel_apply($id) {
        $this->log->info("late-workflow-cancel-start");
        KintaiLateBasicModel::delete($id);
        $this->log->info("late-workflow-cancel-end");
    }

    /**
     * 再申請
     */
    public function regist_re_apply($new_id, $old_id) {
        $this->log->info("late-workflow-re_apply-start");
        if ($_POST["kintai_template_type"] == "late") {
            KintaiLateBasicModel::delete($old_id);
            $this->regist_apply($new_id);
        } else {
            KintaiLateBasicModel::re_apply($new_id, $old_id);
        }
        $this->log->info("late-workflow-re_apply-end");
    }

    /**
     * 承認
     */
    public function approve($id, $con) {
        $this->log->info("late-workflow-approve-start");
        
        // ステータスを承認へ変更
        $this->update_status($id, "1"); // 1:承認

        $this->log->info("late-workflow-approve-end");
    }

    /**
     * ステータス更新
     */
    public function update_status($id, $status) {
        $this->log->info("late-workflow-update-status-start");
        KintaiLateBasicModel::update(array("text"), array("apply_stat" => $status), $id);
        $this->log->info("late-workflow-update-status-end");
    }

    /**
     * 削除
     */
    public function delete($id) {
        $this->log->info("late-workflow-delete-start");
        // 申請テーブルを削除
        KintaiLateBasicModel::delete($id);

        $this->log->info("late-workflow-delete-end");
    }

    /**
     * 画面からパラメータを取得
     */
    private function get_param() {
        $values = new LateTemplateValues();
        $values->date_y1           = $_POST["date_y1"];
        $values->date_m1           = $_POST["date_m1"];
        $values->date_d1           = $_POST["date_d1"];
        $values->date_y2           = $_POST["date_y2"];
        $values->date_m2           = $_POST["date_m2"];
        $values->date_d2           = $_POST["date_d2"];
        $values->start_time_hour   = $_POST["start_time_hour"];
        $values->start_time_minute = $_POST["start_time_minute"];
        $values->date_y3           = $_POST["date_y3"];
        $values->date_m3           = $_POST["date_m3"];
        $values->date_d3           = $_POST["date_d3"];
        $values->end_time_hour     = $_POST["end_time_hour"];
        $values->end_time_minute   = $_POST["end_time_minute"];
        $values->apply_emp_id      = $_POST["apply_emp_id"];
        $values->kind_flag         = $_POST["kind_flag"][0];
        $values->reason_id         = $_POST["reason_id"];
        return $values;
    }
}

class LateTemplateValues {
    public $date_y1;
    public $date_m1;
    public $date_d1;
    public $date_y2;
    public $date_m2;
    public $date_d2;
    public $start_time_hour;
    public $start_time_minute;
    public $date_y3;
    public $date_m3;
    public $date_d3;
    public $end_time_hour;
    public $end_time_minute;
    public $apply_emp_id;
    public $kind_flag;
    public $reason_id;
}
?>
