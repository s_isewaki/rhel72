<?php
require_once 'common_log_class.ini';

require_once 'kintai/common/abstract_workflow.php';
require_once 'kintai/common/user_info.php';
require_once 'kintai/common/date_util.php';
require_once 'kintai/common/mdb.php';

require_once 'kintai/model/kintai_late_basic_model.php';
require_once 'kintai/model/kintai_late_record.php';

require_once 'kintai/individual/model/apply_late_db_access.php';

require_once 'kintai/individual/model/apply_hol2_db_access.php'; // 20140723
require_once 'kintai/model/kintai_hol2_basic_model.php'; // 20140723
require_once 'kintai/model/kintai_hol2_record.php';

class Late2Workflow extends AbstractWorkflow {

    private $log;
    
    public function __construct() {
        $this->log = new common_log_class(basename(__FILE__));
    }

    /**
     * 申請
     */
    public function regist_apply($id) {
        $this->log->info("late-workflow-regist-Start");

        $values = $this->get_param();
        $this->log->debug(var_export($values, true));

        $user_info = UserInfo::get($values->apply_emp_id, "u");
        $condition = $user_info->get_emp_condition();

        // レコードを作成
        $bean = new KintaiLateRecord();
        $bean->apply_id      = $id;
        $bean->apply_date    = DateUtil::to_db_ymd($values->date_y1, $values->date_m1, $values->date_d1);
        $bean->apply_stat    = "0";
        $bean->emp_id        = $values->apply_emp_id;
        $bean->kind_flag     = $values->kind_flag;
        
        if ($values->kind_flag == "2"){
        	$bean->start_date    = DateUtil::to_db_ymd($values->date_y3, $values->date_m3, $values->date_d3);
        	$bean->start_time    = DateUtil::to_db_hi($values->end_time_hour, $values->end_time_minute);
        	$bean->end_date      = DateUtil::to_db_ymd($values->date_y2, $values->date_m2, $values->date_d2);
        	$bean->end_time      = DateUtil::to_db_hi($values->start_time_hour, $values->start_time_minute);
        }else{
	        $bean->start_date    = DateUtil::to_db_ymd($values->date_y2, $values->date_m2, $values->date_d2);
	        $bean->start_time    = DateUtil::to_db_hi($values->start_time_hour, $values->start_time_minute);
	        $bean->end_date      = DateUtil::to_db_ymd($values->date_y3, $values->date_m3, $values->date_d3);
	        $bean->end_time      = DateUtil::to_db_hi($values->end_time_hour, $values->end_time_minute);
        }
        $bean->reason_id     = $values->reason_id;
        $bean->transfer      = $values->transfer;

        // 登録
        KintaiLateBasicModel::insert($bean);

        $this->log->info("late-workflow-regist");
    }

    /**
     * 更新
     */
    public function update_apply($id) {
        $this->log->info("late-workflow-update-start");

        // 内容更新の際にステータスが申請済の場合、
        // 内容更新後、再度承認処理を行う

        $values = $this->get_param();
        $this->log->debug(var_export($values, true));

        $user_info = UserInfo::get($values->apply_emp_id, "u");
        $condition = $user_info->get_emp_condition();

        $types = array(
            "text",
            "text",
            "text",
            "text",
            "text",
            "text",
            "text",
            "text",
            "integer",
        	"integer"
        );

        if ($values->kind_flag == "2"){
        	$st_date    = DateUtil::to_db_ymd($values->date_y3, $values->date_m3, $values->date_d3);
        	$st_time    = DateUtil::to_db_hi($values->end_time_hour, $values->end_time_minute);
        	$ed_date    = DateUtil::to_db_ymd($values->date_y2, $values->date_m2, $values->date_d2);
        	$ed_time    = DateUtil::to_db_hi($values->start_time_hour, $values->start_time_minute);
        }else{
        	$st_date    = DateUtil::to_db_ymd($values->date_y2, $values->date_m2, $values->date_d2);
        	$st_time    = DateUtil::to_db_hi($values->start_time_hour, $values->start_time_minute);
        	$ed_date    = DateUtil::to_db_ymd($values->date_y3, $values->date_m3, $values->date_d3);
        	$ed_time    = DateUtil::to_db_hi($values->end_time_hour, $values->end_time_minute);
        }
        
        $params = array(
            "apply_date" => DateUtil::to_db_ymd($values->date_y1, $values->date_m1, $values->date_d1),
            "apply_stat" => "0",
            "emp_id" => $values->apply_emp_id,
            "kind_flag" => $values->kind_flag,
            "start_date" => $st_date,
            "start_time" => $st_time,
            "end_date" => $ed_date,
            "end_time" => $ed_time,
            "reason_id" => $values->reason_id,
            "transfer" => $values->transfer
        );


        KintaiLateBasicModel::update($types, $params, $id);

        $this->log->info("late-workflow-update-end");
    }

    /**
     * 取消し・否認
     */
    public function cancel_apply($id) {
        $this->log->info("late-workflow-cancel-start");
        
        $kintai_late_info = KintaiLateBasicModel::get($id);
        if ($kintai_late_info->transfer == "1"){
        	// 時間年休テーブルを削除 20140723
        	ApplyHol2DBAccess::atdbk_time_hol_delete($kintai_late_info->emp_id, $kintai_late_info->start_date);
        }
        if ($kintai_late_info->transfer == "2"){
        	KintaiHol2BasicModel::delete($id);
        }
        
        KintaiLateBasicModel::delete($id);
        $this->log->info("late-workflow-cancel-end");
    }

    /**
     * 再申請
     */
    public function regist_re_apply($new_id, $old_id) {
        $this->log->info("late-workflow-re_apply-start");
        if ($_POST["kintai_template_type"] == "late") {
            KintaiLateBasicModel::delete($old_id);
            $this->regist_apply($new_id);
        } else {
            KintaiLateBasicModel::re_apply($new_id, $old_id);
        }
        $this->log->info("late-workflow-re_apply-end");
    }

    /**
     * 承認
     */
    public function approve($id, $con) {
        $this->log->info("late-workflow-approve-start");
        
        // ステータスを承認へ変更
        $this->update_status($id, "1"); // 1:承認
        
        $kintai_late_info = KintaiLateBasicModel::get($id);
        
        // 1:年休振替 2:欠勤振替
        if ($kintai_late_info->transfer == "1" || $kintai_late_info->transfer == "2"){
			$arr_start = str_split($kintai_late_info->start_time, 2); 
			$arr_end = str_split($kintai_late_info->end_time, 2);
			
			$dt_start = strtotime($kintai_late_info->start_date . $kintai_late_info->start_time);
			$dt_end   = strtotime($kintai_late_info->end_date . $kintai_late_info->end_time);
			$tmp_time = gmdate("H:i", $dt_end - $dt_start) ;
			
			if (!empty($tmp_time)){
				$array_time = explode( ":", $tmp_time);
		
				$values = $this->get_param();
				
				if ($kintai_late_info->transfer == "1"){
			        ApplyHol2DBAccess::atdbk_time_hol_delete($kintai_late_info->emp_id, $kintai_late_info->start_date);
			        ApplyHol2DBAccess::atdbk_time_hol_insert($kintai_late_info->emp_id, $kintai_late_info->start_date, $kintai_late_info->start_time, (int)$array_time[0], (int)$array_time[1]);
				}else{
					$user_info = UserInfo::get($kintai_late_info->emp_id, "u");
					$condition = $user_info->get_emp_condition();
					
					$info = new KintaiHol2Record();
					$info->apply_id = $kintai_late_info->apply_id;
					$info->apply_date = $kintai_late_info->apply_date;
					$info->apply_stat = $kintai_late_info->apply_stat;
					$info->emp_id = $kintai_late_info->emp_id;
					$info->kind_flag = $kintai_late_info->kind_flag;
					$info->reason = $kintai_late_info->reason_id;
					$info->tmcd_group_id = $condition->tmcd_group_id;
					$info->pattern = "";
					$info->start_date = $kintai_late_info->start_date;
					$info->end_date = $kintai_late_info->end_date;
					$info->start_time = $kintai_late_info->start_time;
					$info->use_hour = (int)$array_time[0];
					$info->use_minute = (int)$array_time[1];
					$info->remark = "";
					KintaiHol2BasicModel::delete($id);
					KintaiHol2BasicModel::insert($info);
				}
			}
        }
        
        	
        $this->log->info("late-workflow-approve-end");
    }

    /**
     * ステータス更新
     */
    public function update_status($id, $status) {
        $this->log->info("late-workflow-update-status-start");
        
        // 差戻しと否認の際に、削除処理（年休、欠勤）を行う
        if ($status == "2" || $status == "3"){
	        $kintai_late_info = KintaiLateBasicModel::get($id);
	        if ($kintai_late_info->transfer == "1"){
	        	// 時間年休テーブルを削除 20140723
	        	ApplyHol2DBAccess::atdbk_time_hol_delete($kintai_late_info->emp_id,$kintai_late_info->start_date);
	        }
	        if ($kintai_late_info->transfer == "2"){
	        	KintaiHol2BasicModel::delete($id);
	        }
        }
        
        KintaiLateBasicModel::update(array("text"), array("apply_stat" => $status), $id);
        $this->log->info("late-workflow-update-status-end");
    }

    /**
     * 削除
     */
    public function delete($id) {
        $this->log->info("late-workflow-delete-start");
        $kintai_late_info = KintaiLateBasicModel::get($id);
        
//         $start_day = ($kintai_late_info->kind_flag == 2) ? $kintai_late_info->end_date : $kintai_late_info->start_date;
        if ($kintai_late_info->transfer == "1"){
	        // 時間年休テーブルを削除 20140723
        	ApplyHol2DBAccess::atdbk_time_hol_delete($kintai_late_info->emp_id,$kintai_late_info->start_date);
        }
        if ($kintai_late_info->transfer == "2"){
        	KintaiHol2BasicModel::delete($id);
        }

        // 申請テーブルを削除
        KintaiLateBasicModel::delete($id);
        	
        $this->log->info("late-workflow-delete-end");
    }

    /**
     * 画面からパラメータを取得
     */
    private function get_param() {
        $values = new Late2TemplateValues();
        $values->date_y1           = $_POST["date_y1"];
        $values->date_m1           = $_POST["date_m1"];
        $values->date_d1           = $_POST["date_d1"];
        $values->date_y2           = $_POST["date_y2"];
        $values->date_m2           = $_POST["date_m2"];
        $values->date_d2           = $_POST["date_d2"];
        $values->start_time_hour   = $_POST["start_time_hour"];
        $values->start_time_minute = $_POST["start_time_minute"];
        $values->date_y3           = $_POST["date_y3"];
        $values->date_m3           = $_POST["date_m3"];
        $values->date_d3           = $_POST["date_d3"];
        $values->end_time_hour     = $_POST["end_time_hour"];
        $values->end_time_minute   = $_POST["end_time_minute"];
        $values->apply_emp_id      = $_POST["apply_emp_id"];
        $values->kind_flag         = $_POST["kind_flag"][0];
        $values->reason_id         = $_POST["reason_id"];
        $values->transfer         = $_POST["transfer"][0];
        return $values;
    }
}

class Late2TemplateValues {
    public $date_y1;
    public $date_m1;
    public $date_d1;
    public $date_y2;
    public $date_m2;
    public $date_d2;
    public $start_time_hour;
    public $start_time_minute;
    public $date_y3;
    public $date_m3;
    public $date_d3;
    public $end_time_hour;
    public $end_time_minute;
    public $apply_emp_id;
    public $kind_flag;
    public $reason_id;
    public $transfer;
}
?>
