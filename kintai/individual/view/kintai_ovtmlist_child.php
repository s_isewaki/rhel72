<?php
require_once 'about_comedix.php';
require_once 'kintai/common/combobox_util.php';
require_once 'kintai/common/master_util.php';
require_once 'kintai/common/mdb_wrapper.php';
require_once 'kintai/common/user_info.php';
require_once 'kintai/common/date_util.php';

try {
	// MDB�����
	MDBWrapper::init();
	// �ѥ�᡼������
	$emp_id = $_GET["emp_id"]; // �оݥ桼��
	$date   = $_GET["date"]; // �о�����
	// �桼���������
	$user_info = UserInfo::get($emp_id, "u");
	$base_info = $user_info->get_base_info();

	// ������
	$date_map = DateUtil::split_ymd($date);
	$rslt_year = $date_map["y"];
	$rslt_month = intval($date_map["m"]);
	$rslt_day   = intval($date_map["d"]);
	$disp_date = $rslt_year."/".$rslt_month."/".$rslt_day;

	// ����̾��
	$emp_name = $base_info->emp_lt_nm." ".$base_info->emp_ft_nm;

	// ü��������Ķ�ñ�̤����(ʬ�Υ��󥿡��Х�)
	$unit_overtime = MasterUtil::get_unit_overtime();

	// Ķ���̳��ʬ�ޥ��������
	$ovtm_mst = MasterUtil::get_ovtm_div();

	// ��ͳ�μ���
	$reason_list = MasterUtil::get_reason_list();
	// ����ܥܥå����Ѥ˥ꥹ�Ⱥ���
	$reason_combo_list = array();
	foreach ($reason_list as $item) {
		$label = empty($item["display_name"]) ? $item["default_name"]:$item["display_name"];
		$value = $item["reason_id"];
        $time_hol_flag = $item["time_hol_flag"];
		array_push($reason_combo_list, 
			array(
				"value" => $value,
                "label" => h($label),
                "time_hol" => $time_hol_flag
			)
		);
	}

	// ��̳���롼�׼���
	$tmcd_group_list = MasterUtil::get_tmcd_group_list();
	$group_combo_list = array();
	foreach ($tmcd_group_list as $item) {
		array_push($group_combo_list,
			array(
				"value" => $item["group_id"],
				"label" => h($item["group_name"])
			)
		);
	}

} catch (FatalException $fex) {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=EUC-JP">
		<title>CoMedix �ж���(Ķ��)����</title>
		<script type="text/javascript" src="js/fontsize.js"></script>
		<script type="text/javascript" src="js/jquery.js"></script>
		<link rel="stylesheet" type="text/css" href="css/main.css">
		<?php include 'kintai/common/tmpl_header.php'; ?>
		<script type="text/javascript" src="kintai/individual/js/kintai_ovtmlist_child_ctrl.js?<?php echo time(); ?>"></script>
		<script type="text/javascript" src="kintai/individual/js/kintai_ovtmlist_child_check.js?<?php echo time(); ?>"></script>
		<script type="text/javascript" src="kintai/individual/js/kintai_ovtmlist_child_ajax.js?<?php echo time(); ?>"></script>
		<script type="text/javascript" src="kintai/individual/js/kintai_ovtmlist_child.js?<?php echo time(); ?>"></script>
        <script type="text/javascript" src="kintai/js/ecl.js?<?php echo time(); ?>"></script>
		<style type="text/css">
			.list {
				border-collapse:collapse;
			}
			.list td {
				border:#5279a5 solid 1px;padding:1px;
				border-width:1;
			}
		</style>
	</head>
	<body>
		<input type="hidden" name="rslt_date" id="rslt_date" value="<?php echo $date; ?>" />
		<input type="hidden" name="session" id="session" value="<?php echo $_GET["session"]; ?>" />
		<input type="hidden" name="emp_id" id="emp_id" value="<?php echo $emp_id; ?>" />
		<div class="pad5px">
		<!-- �إå��� -->
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr bgcolor="#5279a5">
				<td width="480" height="32" class="spacing"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j14" color="#ffffff"><b>�ж���(Ķ��)����</b></font></td>
				<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="�Ĥ���" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
			</tr>
		</table>
		<img src="img/spacer.gif" width="10" height="10" alt=""><br>
		<div class="pad5px">
			<!-- ���ա���̾ -->
			<table border="0" cellspacing="0" cellpadding="3">
				<tr class="j12">
					<td>����</td>
					<td>��</td>
					<td><?php echo $disp_date; ?></td>
				</tr>
				<tr class="j12">
					<td>��̾</td>
					<td>��</td>
					<td><?php echo h($emp_name); ?></td>
				</tr>
			</table>
			<!-- ��̳���롼�׶�̳���� -->
			<table border="0" cellspacing="0" cellpadding="3">
				<tr class="j12">
					<td>���롼��</td>
					<td>��</td>
					<td>
						<?php ComboBox::output_select("tmcd_group_id", $group_combo_list);?>
					</td>
				</tr>
				<tr class="j12">
					<td>��̳����</td>
					<td>��</td>
					<td>
						<select name="atdptn_id" id="atdptn_id">
						</select>
					</td>
					<td width="30"><span id="fixed_start_time"><span></td>
					<td>��</td>
					<td width="30"><span id="fixed_end_time"><span></td>
					<td>&nbsp;&nbsp;</td>
					<td>��ͳ</td>
					<td>��</td>
					<td>
						<?php ComboBox::output_select("reason_id", $reason_combo_list);?>
					</td>
				</tr>
			</table>
		</div>
		<!-- �ĶȻ��� -->

		<div style="width:700px" class="pad5px">
				<div style="width:700px;overflow:auto;">			
				<table id="result_table" cellspacing="0" cellpadding="2" class="list">
				<tr bgcolor="#f6f9ff" class="j12">
					<td width="10"></td>
					<td align="center">���ֳ���������</td>
					<td align="center">��ٷƻ���</td>
					<td align="center">������̳����</td>
                    <td align="center">������̳���ƾܺ�</td>
					<td align="center">�ƽ�</td>
				</tr>
				<?php
				for($i = 1; $i <= 3; $i++) {
				// html name°��
				$rslt_start_hour = "rslt_start_hour".$i;
				$rslt_start_minute = "rslt_start_minute".$i;
				$rslt_end_hour = "rslt_end_hour".$i;
				$rslt_end_minute = "rslt_end_minute".$i;
				$rslt_rest_hour = "rslt_rest_hour".$i;
				$rslt_rest_minute = "rslt_rest_minute".$i;
				$rslt_start_next_flag = "rslt_start_next_flag".$i;
				$rslt_end_next_flag = "rslt_end_next_flag".$i;
				$rslt_ovtmrsn_id = "rslt_ovtmrsn_id".$i;
				$rslt_call_flag  = "rslt_call_flag".$i;
                $rslt_ovtmrsn_txt = "rslt_ovtmrsn_txt".$i;
				?>
				<tr class="j12">
					<td align="center" nowrap><?php echo $i; ?></td>
					<td nowrap>
						<?php TimeCombobox::output_select_hours($rslt_start_hour); ?>��<?php TimeCombobox::output_select_minutes($rslt_start_minute, null, $unit_overtime); ?>
						<input id="<?php echo $rslt_start_next_flag; ?>" name="<?php echo $rslt_start_next_flag; ?>[0]" type="checkbox" value="1" />����
						<?php TimeCombobox::output_select_hours($rslt_end_hour); ?>��<?php TimeCombobox::output_select_minutes($rslt_end_minute, null, $unit_overtime); ?>
						<input id="<?php echo $rslt_end_next_flag; ?>" name="<?php echo $rslt_end_next_flag; ?>[0]" type="checkbox" value="1" />����
					</td>
					<td nowrap>
						<?php TimeCombobox::output_select_hours($rslt_rest_hour); ?>��<?php TimeCombobox::output_select_minutes($rslt_rest_minute); ?>
					</td>
					<td nowrap>
						<?php ComboBox::output_select($rslt_ovtmrsn_id);?>
					</td>
					<td align="center" nowrap>
                        <input type="text" name="<?php echo $rslt_ovtmrsn_txt; ?>" id="<?php echo $rslt_ovtmrsn_txt; ?>" value="" />
					</td>
                    <td nowrap>
						<input id="<?php echo $rslt_call_flag; ?>" name="<?php echo $rslt_call_flag; ?>[0]" type="checkbox" value="1" />
                    </td>
				</tr>
				<?php
				}
				?>
			</table>
		</div>
		<!-- ����ͭ�٤����ﳰ��̳ -->
		<div class="pad5px">
			<!-- ���ա���̾ -->
			<table border="0" cellspacing="0" cellpadding="3">
				<tr class="j12">
					<td>����ͭ��</td>
					<td>��</td>
					<td>
						<?php TimeCombobox::output_select_hours("hol_start_hour"); ?>��<?php TimeCombobox::output_select_minutes("hol_start_minute"); ?>
					</td>
					<td>����</td>
					<td>
						<?php TimeCombobox::output_select_hours("hol_time_hour", null, 9); ?>����<?php TimeCombobox::output_select_minutes("hol_time_minute"); ?>
					</td>
					<td>ʬ</td>
				</tr>
				<tr class="j12">
					<td>���ﳰ��̳</td>
					<td>��</td>
					<td>
						<?php TimeCombobox::output_select_hours("ward_start_hour"); ?>��<?php TimeCombobox::output_select_minutes("ward_start_minute"); ?>
					</td>
					<td>����</td>
					<td>
						<?php TimeCombobox::output_select_hours("ward_end_hour"); ?>��<?php TimeCombobox::output_select_minutes("ward_end_minute"); ?>
					</td>
				</tr>
			</table>
		</div>

		<!-- Ķ���̳��ʬ�׻� -->
		<div class="pad5px">
			<input type="button" id="ovtm_calc" value="Ķ���̳��ʬ�׻�" />
			<div class="j12 pad5px">
				Ķ���̳��ʬ
				<table cellspacing="0" cellpadding="2" class="list">
					<tr bgcolor="#f6f9ff" class="j12">
						<td align="center">��ʬ̾</td>
						<?php foreach ($ovtm_mst as $item) { ?>
						<td align="center" width="40"><?php echo $item["ovtmkbn_name"]; ?></td>
						<?php } ?>
					</tr>
					<tr class="j12">
						<td bgcolor="#f6f9ff" align="center">Ķ�л���</td>
						<?php foreach ($ovtm_mst as $item) { ?>
						<td align="right" id="ovtm_minute_<?php echo $item["ovtmkbn_id"]; ?>"></td>
						<?php } ?>
					</tr>
				</table>
				<!-- Ķ���̳��ʬ�׻���� -->
				<?php foreach ($ovtm_mst as $item) { ?>
				<input type="hidden" id="ovtm_calc_<?php echo $item["ovtmkbn_id"]; ?>" name="ovtm_calc_<?php echo $item["ovtmkbn_id"]; ?>" value="" />
				<?php } ?>
			</div>
		</div>
		<div class="pad5px">
			<input id="regist" type="button" value="��Ͽ" />
		</div>
		</div>
	</body>
</html>
