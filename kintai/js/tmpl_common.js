jQuery.noConflict();  
var j$ = jQuery;  

function show_kintai_rslt_calendar(emp_id) {
	var url  = "kintai_rslt_calendar.php?session=" + j$("[name=session]").val();
			url += "&emp_id=" + emp_id;

	window.open(
		url,
		"work_calendar",
		"width=640,height=480,scrollbars=yes"
	);
}

var COMMON_AJAX_CONTROLLER = "kintai/common/common_ajax_controller.php";
function get_user_info(p_session, p_emp_id, p_auth) {
    var data = {
        controller:COMMON_AJAX_CONTROLLER,
        method:"get_user_info",
        auth:p_auth,
        session:p_session,
        emp_id:p_emp_id
    }
    
    var user_info;
    ajaxSend("kintai_ajax_dispatcher.php", data, 
        function (res) {
            user_info = res.data;
        }
    );
    return user_info;
}

function update_trip(p_session, p_date, p_emp_id, p_flag, p_auth) {
    var data = {
        controller:COMMON_AJAX_CONTROLLER,
        method:"update_trip",
        auth:p_auth,
        session:p_session,
        date:p_date,
        emp_id:p_emp_id,
        flag:p_flag
    }
    
    ajaxSend("kintai_ajax_dispatcher.php", data, 
        function (res) {
        }
    );
}

// ajax通信開始
function ajaxSend(sendUrl, sendData, success_handler) {
	j$.ajax({
		async: false,
		type:"POST",
		url:sendUrl,
		data:sendData,
		success: get_success_handler(success_handler),
		error: error_handler,
		beforeSend: function(xhr){
			xhr.overrideMimeType("text/html;charset=EUC-JP");
		}
	});
}

function get_success_handler(func) {
	return function (data, datatype) {
			//console.log(data);
			var obj;
			try {
				obj = j$.parseJSON(data);
			} catch(ex) {
				alert("JSONのparseに失敗しました.\n" + ex);
				return false;
			}
		
			if (obj.result == "error") {
				alert(obj.message);
				return false;
			}
			func(obj);
	};
}

// リミット数まで以降を切り捨てて文字列を返す
function get_limit(string, limit){
	if(string.length == 0){return 0;}
	
	var count = 0;
	var str = "";
	for(var i = 0; i <string.length; i++){
		if( escape(string.charAt(i)).length  < 4 ){
			count = count + 1;
		}else{
			count = count + 2;
		}
		
		if (count > limit) {
			return string.substr(0, i);
		}
	}
	return string;
}

var error_handler = function (XMLHttpRequest, textStatus, errorThrown) {
	var msg = "システムエラーが発生しました。\n管理者にお問い合わせください。";
	if (errorThrown != null) {
		msg += "\n error=" + errorThrown;
	}
	if (textStatus != null) {
		msg += "\n status=" + textStatus;
	}
	alert(msg);
};

function show_kintai_calendar(item_id, init_date_flag) {
	var url  = "kintai_calendar.php?session=" + j$("[name=session]").val();
			url += "&emp_id=" + j$("[name=apply_emp_id]").val();
			url += "&item_id=" + item_id;
			url += "&init_date_flag=" + init_date_flag;

	window.open(
		url,
		"kintai_calendar",
		"width=640,height=480,scrollbars=yes"
	);
}
