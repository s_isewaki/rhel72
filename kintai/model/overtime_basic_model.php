<?php
require_once 'kintai/common/mdb.php';
require_once 'kintai/model/overtime_record.php';

/**
 * 勤怠残業時間テーブル
 */
class OvertimeBasicModel {
	
	/**
	 * データ挿入
	 */
	static function insert($info) {
		$values = array(array(
            $info->emp_id, 
            $info->date,
			$info->apply_date,
			$info->apply_stat,
			$info->cmmd_start_time1,
			$info->cmmd_start_next_flag1,
			$info->cmmd_end_time1,
			$info->cmmd_end_next_flag1,
			$info->cmmd_rest_time1,
			$info->cmmd_ovtmrsn_id1,
			$info->cmmd_start_time2,
			$info->cmmd_start_next_flag2,
			$info->cmmd_end_time2,
			$info->cmmd_end_next_flag2,
			$info->cmmd_rest_time2,
			$info->cmmd_ovtmrsn_id2,
			$info->cmmd_start_time3,
			$info->cmmd_start_next_flag3,
			$info->cmmd_end_time3,
			$info->cmmd_end_next_flag3,
			$info->cmmd_rest_time3,
			$info->cmmd_ovtmrsn_id3,
			$info->rslt_start_time1,
			$info->rslt_start_next_flag1,
			$info->rslt_end_time1,
			$info->rslt_end_next_flag1,
			$info->rslt_rest_time1,
			$info->rslt_ovtmrsn_id1,
			$info->rslt_start_time2,
			$info->rslt_start_next_flag2,
			$info->rslt_end_time2,
			$info->rslt_end_next_flag2,
			$info->rslt_rest_time2,
			$info->rslt_ovtmrsn_id2,
			$info->rslt_start_time3,
			$info->rslt_start_next_flag3,
			$info->rslt_end_time3,
			$info->rslt_end_next_flag3,
			$info->rslt_rest_time3,
			$info->rslt_ovtmrsn_id3,
			$info->mdfy_start_time1,
			$info->mdfy_start_next_flag1,
			$info->mdfy_end_time1,
			$info->mdfy_end_next_flag1,
			$info->mdfy_rest_time1,
			$info->mdfy_ovtmrsn_id1,
			$info->mdfy_cancel_flag1,
			$info->mdfy_start_time2,
			$info->mdfy_start_next_flag2,
			$info->mdfy_end_time2,
			$info->mdfy_end_next_flag2,
			$info->mdfy_rest_time2,
			$info->mdfy_ovtmrsn_id2,
			$info->mdfy_cancel_flag2,
			$info->mdfy_start_time3,
			$info->mdfy_start_next_flag3,
			$info->mdfy_end_time3,
			$info->mdfy_end_next_flag3,
			$info->mdfy_rest_time3,
			$info->mdfy_ovtmrsn_id3,
			$info->mdfy_cancel_flag3,
			$info->apply_id,
			$info->rslt_call_flag1,
			$info->rslt_call_flag2,
            $info->rslt_call_flag3,
			$info->cmmd_ovtmrsn_txt1,
			$info->cmmd_ovtmrsn_txt2,
			$info->cmmd_ovtmrsn_txt3,
			$info->rslt_ovtmrsn_txt1,
			$info->rslt_ovtmrsn_txt2,
			$info->rslt_ovtmrsn_txt3,
			$info->mdfy_ovtmrsn_txt1,
			$info->mdfy_ovtmrsn_txt2,
            $info->mdfy_ovtmrsn_txt3,
            $info->cmmd_call_flag1,
            $info->cmmd_call_flag2,
            $info->cmmd_call_flag3,
            $info->mdfy_call_flag1,
            $info->mdfy_call_flag2,
            $info->mdfy_call_flag3,
		));
		
		self::get_mdb()->insert("kintai_ovtm", self::$columns, self::$types, $values);
	}

	/**
	 * 更新
	 * @param $types set句の型を指定
	 * @param $types set句のbindIdとvalueを指定
	 * @param $emp_id キー1
	 * @param $date キー2
	 */
	static function update($types, $values, $emp_id, $date) {
		
		$key_list = array_keys($values);
		
		$set_array = array();
		
		for ($i = 0; $i < sizeof($key_list); $i++) {
			$key = $key_list[$i];
			$value = $values[$key];
			if (!isset($value)) {
				array_push($set_array, "$key = NULL");
				unset($types[$i]);
				unset($values[$i]);
			} else {
				array_push($set_array, "$key = :$key");
			}
		}
		
		// 条件用の型を追加
		array_push($types, "text", "text");
		// valuesを追加
		$values["emp_id"] = $emp_id;
		$values["date"] = $date;
		
		$sql = 
			"update kintai_ovtm set ".implode(",", $set_array)." ".
			"where emp_id = :emp_id and date = :date";
		
		self::get_mdb()->update($sql, $types, $values);
	}
	
	// 1件だけ取得
	static function get($date, $emp_id, $nullable = false) {
		$result = 
			self::get_mdb()->first_row(
				"select * from kintai_ovtm where emp_id = :emp_id and date = :date",
				array("text", "text"),
				array("emp_id" => $emp_id, "date" => $date)
			);
		
		$obj = new OvertimeRecord();
		if (empty($result)) {
			if ($nullable) {
				return null;
			}
			return $obj;
		}
		// ORマッパー的な
		$keys = array_keys($result);
		foreach ($keys as $key) {
			$obj->$key = $result[$key];
		}
		return $obj;
	}

	static function exists($emp_id, $date) {
		$result = self::get_mdb()->one(
			"select count(emp_id) from kintai_ovtm where emp_id = :emp_id and date = :date",
			array("text", "text"),
			array("emp_id" => $emp_id, "date" => $date)
		);

		if ($result > 0) {
			return true;
		} else {
			return false;
		}
	}

	static function delete($emp_id, $date) {
		self::get_mdb()->delete(
			"delete from kintai_ovtm where emp_id = :emp_id and date = :date",
			array("text", "text"),
			array("emp_id" => $emp_id, "date" => $date)
		);
	}
	
	static function get_from_apply_id($id) {
		$sql = "select * from kintai_ovtm where apply_id = :apply_id";
		$result = self::get_mdb()->first_row(
			$sql, array("integer"), array("apply_id" => $id)
		);
		

		if (empty($result)) {
			return $null;
		}

		$obj = new OvertimeRecord();
		// ORマッパー的な
		$keys = array_keys($result);
		foreach ($keys as $key) {
			$obj->$key = $result[$key];
		}
		return $obj;
	}

	static function re_apply($new_id, $old_id) {
		$origin = self::get_from_apply_id($old_id);
		if (!empty($origin)) {
			// 申請ID、ステータスのみ
			$types = array("integer", "text");
			$values = array(
				"apply_id" => $new_id,
				"apply_stat" => "0"
			);
			self::update($types, $values, $origin->emp_id, $origin->date);
		}
	}

	private static function get_mdb() {
		$log = new common_log_class(basename(__FILE__));
		return new MDB($log);
	}
	
	private static $columns = array(
		"emp_id",
		"date",
		"apply_date",
		"apply_stat",
		"cmmd_start_time1",
		"cmmd_start_next_flag1",
		"cmmd_end_time1",
		"cmmd_end_next_flag1",
		"cmmd_rest_time1",
		"cmmd_ovtmrsn_id1",
		"cmmd_start_time2",
		"cmmd_start_next_flag2",
		"cmmd_end_time2",
		"cmmd_end_next_flag2",
		"cmmd_rest_time2",
		"cmmd_ovtmrsn_id2",
		"cmmd_start_time3",
		"cmmd_start_next_flag3",
		"cmmd_end_time3",
		"cmmd_end_next_flag3",
		"cmmd_rest_time3",
		"cmmd_ovtmrsn_id3",
		"rslt_start_time1",
		"rslt_start_next_flag1",
		"rslt_end_time1",
		"rslt_end_next_flag1",
		"rslt_rest_time1",
		"rslt_ovtmrsn_id1",
		"rslt_start_time2",
		"rslt_start_next_flag2",
		"rslt_end_time2",
		"rslt_end_next_flag2",
		"rslt_rest_time2",
		"rslt_ovtmrsn_id2",
		"rslt_start_time3",
		"rslt_start_next_flag3",
		"rslt_end_time3",
		"rslt_end_next_flag3",
		"rslt_rest_time3",
		"rslt_ovtmrsn_id3",
		"mdfy_start_time1",
		"mdfy_start_next_flag1",
		"mdfy_end_time1",
		"mdfy_end_next_flag1",
		"mdfy_rest_time1",
		"mdfy_ovtmrsn_id1",
		"mdfy_cancel_flag1",
		"mdfy_start_time2",
		"mdfy_start_next_flag2",
		"mdfy_end_time2",
		"mdfy_end_next_flag2",
		"mdfy_rest_time2",
		"mdfy_ovtmrsn_id2",
		"mdfy_cancel_flag2",
		"mdfy_start_time3",
		"mdfy_start_next_flag3",
		"mdfy_end_time3",
		"mdfy_end_next_flag3",
		"mdfy_rest_time3",
		"mdfy_ovtmrsn_id3",
		"mdfy_cancel_flag3",
		"apply_id",
		"rslt_call_flag1",
		"rslt_call_flag2",
        "rslt_call_flag3",
		"cmmd_ovtmrsn_txt1",
		"cmmd_ovtmrsn_txt2",
		"cmmd_ovtmrsn_txt3",
		"rslt_ovtmrsn_txt1",
		"rslt_ovtmrsn_txt2",
		"rslt_ovtmrsn_txt3",
		"mdfy_ovtmrsn_txt1",
		"mdfy_ovtmrsn_txt2",
        "mdfy_ovtmrsn_txt3",
        "cmmd_call_flag1",
        "cmmd_call_flag2",
        "cmmd_call_flag3",
        "mdfy_call_flag1",
        "mdfy_call_flag2",
        "mdfy_call_flag3",
	);
	private static $types = array(
		"text",
		"text",
		"text",
		"text",
		"text",
		"integer",
		"text",
		"integer",
		"text",
		"integer",
		"text",
		"integer",
		"text",
		"integer",
		"text",
		"integer",
		"text",
		"integer",
		"text",
		"integer",
		"text",
		"integer",
		"text",
		"integer",
		"text",
		"integer",
		"text",
		"integer",
		"text",
		"integer",
		"text",
		"integer",
		"text",
		"integer",
		"text",
		"integer",
		"text",
		"integer",
		"text",
		"integer",
		"text",
		"integer",
		"text",
		"integer",
		"text",
		"integer",
		"integer",
		"text",
		"integer",
		"text",
		"integer",
		"text",
		"integer",
		"integer",
		"text",
		"integer",
		"text",
		"integer",
		"text",
		"integer",
		"integer",
		"integer",
		"integer",
		"integer",
        "integer",
        "text",
        "text",
        "text",
        "text",
        "text",
        "text",
        "text",
        "text",
        "text",
        "integer",
        "integer",
        "integer",
        "integer",
        "integer",
        "integer",
	);
	
	private function __construct() {}
}
?>
