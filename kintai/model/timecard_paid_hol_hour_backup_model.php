<?php
require_once 'kintai/common/mdb.php';

/**
 * 時間有休テーブル(バックアップ)
 */
class TimecardPaidHolHourBackupModel {
	
	/**
	 * バックアップ(承認前に実行)
	 */
	static function backup($emp_id, $date) {
		
		$where_types = array("text","text");
		$where_values= array("emp_id" => $emp_id, "start_date" => $date);

		// select
		$time_hol = self::get_mdb()->first_row(
			"select * from timecard_paid_hol_hour where emp_id = :emp_id and start_date = :start_date",
			$where_types, $where_values
		);
		$values = array(array(
			$emp_id,
			$date,
			$time_hol["start_time"],
			$time_hol["use_hour"],
			$time_hol["use_minute"],
			$time_hol["calc_minute"]
		));
		
		// 削除
		self::get_mdb()->delete(
			"delete from timecard_paid_hol_hour_backup where emp_id = :emp_id and start_date = :start_date",
			$where_types, $where_values
		);
		
		if (!empty($time_hol)) {
			// 挿入
			self::get_mdb()->insert("timecard_paid_hol_hour_backup", self::$columns, self::$types, $values);
		}
	}
	
	/**
	 * リストア(削除前に実行)
	 */
	static function restore($emp_id, $date) {

		$where_types = array("text","text");
		$where_values= array("emp_id" => $emp_id, "start_date" => $date);

		// select
		$time_hol = self::get_mdb()->first_row(
			"select * from timecard_paid_hol_hour_backup where emp_id = :emp_id and start_date = :start_date",
			$where_types, $where_values
		);
		$values = array(array(
			$emp_id,
			$date,
			$time_hol["start_time"],
			$time_hol["use_hour"],
			$time_hol["use_minute"],
			$time_hol["calc_minute"]
		));
		
		// 削除
		self::get_mdb()->delete(
			"delete from timecard_paid_hol_hour where emp_id = :emp_id and start_date = :start_date",
			$where_types, $where_values
		);
		
		if (!empty($time_hol)) {
			// 挿入
			self::get_mdb()->insert("timecard_paid_hol_hour", self::$columns, self::$types, $values);
		}
	}
	
	private static function get_mdb() {
		$log = new common_log_class(basename(__FILE__));
		return new MDB($log);
	}
	
	private static $columns = array(
		"emp_id",
		"start_date",
		"start_time",
		"use_hour",
		"use_minute",
		"calc_minute"
	);
	private static $types = array(
		"text",
		"text",
		"text",
		"text",
		"text",
		"integer"
	);
	
	private function __construct() {}
}
?>
