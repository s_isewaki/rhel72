<?php
require_once 'kintai/common/mdb.php';
require_once 'kintai/model/kintai_hol2_record.php';

/**
 * 休暇申請テーブル
 */
class KintaiHol2BasicModel {
	
	/**
	 * データ挿入
	 */
	static function insert($info) {
		$values = array(array(
			$info->apply_id,
			$info->apply_date,
			$info->apply_stat,
			$info->emp_id,
			$info->kind_flag,
			$info->reason,
			$info->tmcd_group_id,
			$info->pattern,
			$info->start_date,
			$info->end_date,
			$info->start_time,
			$info->use_hour,
			$info->use_minute,
			$info->remark,
			$info->detail
		));
		
		self::get_mdb()->insert("kintai_hol", self::$columns, self::$types, $values);
	}
	
	
	/**
	 * 更新
	 * @param $types set句の型を指定
	 * @param $types set句のbindIdとvalueを指定
	 * @param $emp_id キー1
	 * @param $date キー2
	 */
	static function update($types, $values, $apply_id) {
		
		// set句を構築
		// keyのみ取得
		$keys = array_keys($values);
		$set_array = array();
		foreach ($keys as $key) {
			array_push($set_array, "$key = :$key");
		}
		
		// 条件用の型を追加
		array_push($types, "integer", "text");
		// valuesを追加
		$values["apply_id"] = $apply_id;
		
		$sql = 
			"update kintai_hol set ".implode(",", $set_array).
			" where apply_id = :apply_id";
		
		self::get_mdb()->update($sql, $type, $values);
	}
	
	// 1件だけ取得
	static function get($apply_id) {
		$result = 
			self::get_mdb()->first_row(
				"select * from kintai_hol where apply_id = :apply_id",
				array("text"),
				array("apply_id" => $apply_id)
			);
		
		$obj = new KintaiHol2Record();
		if (empty($result)) {
			return $obj;
		}
		// ORマッパー的な
		$keys = array_keys($result);
		foreach ($keys as $key) {
			$obj->$key = $result[$key];
		}
		return $obj;
	}
	
	/**
	 * 再申請(データコピー)
	 */
	static function re_apply($new_id, $old_id) {
		
		// 元データを取得
		$origin = self::get($old_id);
		// ＩＤとステータスを変更
		$origin->apply_id = $new_id;
		$origin->apply_stat = "0";
		
		self::insert($origin);
		self::delete($old_id);
	}
	
	/**
	 * 休暇申請を削除
	 */
	static function delete($id) {
		$sql = "delete from kintai_hol where apply_id = :apply_id";
		self::get_mdb()->delete($sql, array("integer"), array("apply_id" => $id));
	}

	private static function get_mdb() {
		$log = new common_log_class(basename(__FILE__));
		return new MDB($log);
	}
	
	private static $columns = array(
		"apply_id",
		"apply_date",
		"apply_stat",
		"emp_id",
		"kind_flag",
		"reason",
		"tmcd_group_id",
		"pattern",
		"start_date",
		"end_date",
		"start_time",
		"use_hour",
		"use_minute",
		"remark",
		"detail"
	);
	private static $types = array(
		"integer",
		"text",
		"text",
		"text",
		"text",
		"text",
		"integer",
		"text",
		"text",
		"text",
		"text",
		"text",
		"text",
		"text",
		"integer"
	);
	
	private function __construct() {}
}
?>
