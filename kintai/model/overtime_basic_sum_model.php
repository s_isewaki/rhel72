<?php
require_once 'kintai/common/mdb.php';
require_once 'kintai/model/overtime_sum_record.php';

/**
 * 超過勤務時間モデル
 */
class OvertimeBasicSumModel {
	
	/**
	 * データ挿入
	 */
	static function insert($info) {
		$values = array(array(
			$info->emp_id,
			$info->date,
			$info->ovtmkbn_id,
			$info->minute,
		));
		
		self::get_mdb()->insert("kintai_ovtm_sum", self::$columns, self::$types, $values);
	}
	
	/**
	 * 超勤時間を更新
	 */
	static function update($minute, $emp_id, $date, $ovtmkbn_id) {
		
		$sql = 
			"update kintai_ovtm_sum set minute = :minute ".
			"where emp_id = :emp_id and date = :date and ovtmkbn_id = :ovtmkbn_id";
		$types = array("integer", "text", "text", "integer");
		$values = array("minute" => $minute, "emp_id" => $emp_id, "date" => $date, "ovtmkbn_id" => $ovtmkbn_id);

		self::get_mdb()->update($sql, $types, $values);
	}
	
	// その日の超過時間を取得
	static function get($date, $emp_id, $ovtmkbn_id = null) {
		
		$sql = "select * from kintai_ovtm_sum where emp_id = :emp_id and date = :date";
		$types = array("text", "text");
		$values = array("emp_id" => $emp_id, "date" => $date);
		
		$first_row = false;
		if (!empty($ovtmkbn_id)) {
			$sql .= " and ovtmkbn_id = :ovtmkbn_id";
			array_push($types, "integer");
			$values["ovtmkbn_id"] = $ovtmkbn_id;
			$first_row = true;
		}
		
		$sql .= " order by ovtmkbn_id asc";
	
		if (!$first_row) {
			return self::get_mdb()->find($sql, $types, $values);
		} else {
			return self::get_mdb()->first_row($sql, $types, $values);
		}
	}
	
	static function delete($emp_id, $date) {
		return self::get_mdb()->delete(
			"delete from kintai_ovtm_sum where emp_id = :emp_id and date = :date",
			array("text", "text"),
			array("emp_id" => $emp_id, "date" => $date)
		);
	}

	private static function get_mdb() {
		$log = new common_log_class(basename(__FILE__));
		return new MDB($log);
	}
	
	private static $columns = array(
		"emp_id",
		"date",
		"ovtmkbn_id",
		"minute",
	);
	private static $types = array(
		"text",
		"text",
		"integer",
		"integer"
	);
	
	private function __construct() {}
}
?>
