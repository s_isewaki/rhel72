<?php
require_once 'kintai/common/mdb.php';

/**
 * 勤務予定テーブル(バックアップ)
 */
class AtdbkBackupModel {
	
	/**
	 * バックアップ(承認前に実行)
	 */
	static function backup($emp_id, $date) {
		
		$where_types = array("text","text");
		$where_values= array("emp_id" => $emp_id, "date" => $date);

		// select
		$atdbk = self::get_mdb()->first_row(
			"select * from atdbk where emp_id = :emp_id and date = :date",
			$where_types, $where_values
		);
		$values = array(array(
			$emp_id,
			$date,
			$atdbk["pattern"],
			$atdbk["reason"],
			$atdbk["night_duty"],
			$atdbk["allow_id"],
			$atdbk["prov_start_time"],
			$atdbk["prov_end_time"],
			$atdbk["tmcd_group_id"],
			$atdbk["atrb_link_key"]
		));
		
		// 削除
		self::get_mdb()->delete(
			"delete from atdbk_backup where emp_id = :emp_id and date = :date",
			$where_types, $where_values
		);
		
		// 挿入
		self::get_mdb()->insert("atdbk_backup", self::$columns, self::$types, $values);
	}
	
	/**
	 * リストア(削除前に実行)
	 */
	static function restore($emp_id, $date) {

		$where_types = array("text","text");
		$where_values= array("emp_id" => $emp_id, "date" => $date);

		// select
		$atdbk = self::get_mdb()->first_row(
			"select * from atdbk_backup where emp_id = :emp_id and date = :date",
			$where_types, $where_values
		);
		$values = array(array(
			$emp_id,
			$date,
			$atdbk["pattern"],
			$atdbk["reason"],
			$atdbk["night_duty"],
			$atdbk["allow_id"],
			$atdbk["prov_start_time"],
			$atdbk["prov_end_time"],
			$atdbk["tmcd_group_id"],
			$atdbk["atrb_link_key"]
		));
		
		// 削除
		self::get_mdb()->delete(
			"delete from atdbk where emp_id = :emp_id and date = :date",
			$where_types, $where_values
		);
		// 挿入
		self::get_mdb()->insert("atdbk", self::$columns, self::$types, $values);
	}
	
	private static function get_mdb() {
		$log = new common_log_class(basename(__FILE__));
		return new MDB($log);
	}
	
	private static $columns = array(
		"emp_id",
		"date",
		"pattern",
		"reason",
		"night_duty",
		"allow_id",
		"prov_start_time",
		"prov_end_time",
		"tmcd_group_id",
		"atrb_link_key"
	);
	private static $types = array(
		"text",
		"text",
		"text",
		"text",
		"text",
		"integer",
		"text",
		"text",
		"integer",
		"text"
	);
	
	private function __construct() {}
}
?>
