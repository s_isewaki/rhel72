<?php
require_once 'kintai/common/mdb.php';

/**
 * 旅行申請モデル
 */
class TravelWorkflowModel {

    public static function get($apply_id) {
        $result = self::get_mdb()->first_row(
            "select * from travel_wkfw where apply_id = $apply_id"
        );
        $obj = new stdclass;
        foreach ($result as $key => $value) {
            $obj->$key = $value;
        }
        return $obj;
    }
    
    public static function insert($info) { 
        $values = array(array(
            $info["apply_id"],
            $info["apply_date"],
            $info["apply_stat"],
            $info["emp_id"],
            $info["title"],
            $info["destination"],
            $info["start_date"],
            $info["end_date"],
            $info["amount1"],
            $info["amount2"],
            $info["amount3"],
            $info["amount4"],
            $info["remark"]
        )); 

        self::get_mdb()->insert("travel_wkfw", self::$columns, self::$types, $values);    
    }

    public static function update($info) {
        
        $sql  = "";
        $sql .= "update travel_wkfw set ";
        $sql .= "apply_date = :apply_date, "; 
        $sql .= "apply_stat = :apply_stat, ";
        $sql .= "emp_id = :emp_id, ";   
        $sql .= "title = :title, ";    
        $sql .= "destination = :destination, ";
        $sql .= "start_date = :start_date, "; 
        $sql .= "end_date = :end_date, ";   
        $sql .= "amount1 = :amount1, ";   
        $sql .= "amount2 = :amount2, ";    
        $sql .= "amount3 = :amount3, ";    
        $sql .= "amount4 = :amount4, ";    
        $sql .= "remark = :remark ";    
        $sql .= "where apply_id = :apply_id"; 
        
        $types = array_slice(self::$types, 1);
        array_push($types, "integer");
  
        self::get_mdb()->update($sql, $types, $info);
    }

    public static function update_status($id, $status) {
        return self::get_mdb()->update(
            "update travel_wkfw set apply_stat = '$status' where apply_id = $id");
    }

    /**
     * データ削除
     */
    static function delete($apply_id) {
        return self::get_mdb()->delete(
            "delete from travel_wkfw where apply_id = $apply_id"
        );
    }

    private static function get_mdb() {
        $log = new common_log_class(basename(__FILE__));
        return new MDB($log);
    }

    private static $columns = array(
        "apply_id",
        "apply_date",
        "apply_stat",
        "emp_id",
        "title",
        "destination",
        "start_date",
        "end_date",
        "amount1",
        "amount2",
        "amount3",
        "amount4",
        "remark"
    );
    private static $types = array(
        "integer",
        "text",
        "text",
        "text",
        "text",
        "text",
        "text",
        "text",
        "integer",
        "integer",
        "integer",
        "integer",
        "text"
    );

    private function __construct() {}
}
?>
