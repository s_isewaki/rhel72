<?php
require_once 'kintai/common/mdb.php';

/**
 * 超勤時間発生順モデル
 */
class OvertimeSeqModel {
	
	/**
	 * データ挿入
	 */
	static function insert($emp_id, $date, $seq_list) {
	
		$values = array();
		foreach ($seq_list as $value) {
			array_push($values, array(
				$date,
				$emp_id,
				$value["no"],
				$value["minute"],
				$value["ovtmkbn_id"]
			));
		}
		
		self::get_mdb()->insert("kintai_ovtm_seq", self::$columns, self::$types, $values);
	}
	
	/**
	 * データ削除
	 */
	static function delete($emp_id, $date) {
		return self::get_mdb()->delete(
			"delete from kintai_ovtm_seq where emp_id = :emp_id and date = :date",
			array("text", "text"),
			array("emp_id" => $emp_id, "date" => $date)
		);
	}

	private static function get_mdb() {
		$log = new common_log_class(basename(__FILE__));
		return new MDB($log);
	}
	
	private static $columns = array(
		"date",
		"emp_id",
		"no",
		"minute",
		"ovtmkbn_id"
	);
	private static $types = array(
		"text",
		"text",
		"integer",
		"integer",
		"integer"
	);
	
	private function __construct() {}
}
?>
