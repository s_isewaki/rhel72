<?php
/**
 * 超過勤務時間テーブルにインサートするときはこれを使う
 */
class OvertimeSumRecord {
	public $emp_id;
	public $date;
	public $ovtmkbn_id;
	public $minute;
}
?>