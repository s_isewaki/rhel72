<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 病床管理 | 入院患者検索−基本</title>
<?
require("about_session.php");
require("about_authority.php");
require("show_select_values.ini");
require("show_in_search.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 診療科登録権限を取得
$section_admin_auth = check_authority($session, 28, $fname);

// 病棟登録権限を取得
$ward_admin_auth = check_authority($session, 21, $fname);

// 患者登録権限を取得
$pt_reg_auth = check_authority($session, 22, $fname);

// データベースに接続
$con = connect2db($fname);

// 患者情報を取得
$sql = "select * from ptifmst";
$cond = "where ptif_id = '$pt_id' and ptif_del_flg = 'f'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$lt_nm = pg_fetch_result($sel, 0, "ptif_lt_kaj_nm");
$ft_nm = pg_fetch_result($sel, 0, "ptif_ft_kaj_nm");
$lt_kana_nm = pg_fetch_result($sel, 0, "ptif_lt_kana_nm");
$ft_kana_nm = pg_fetch_result($sel, 0, "ptif_ft_kana_nm");
$birth = pg_fetch_result($sel, 0, "ptif_birth");
$sex = pg_fetch_result($sel, 0, "ptif_sex");
$job = pg_fetch_result($sel, 0, "ptif_buis");

$birth_yr = substr($birth, 0, 4);
$birth_mon = substr($birth, 4, 2);
$birth_day = substr($birth, 6, 2);

// 診療科情報、主治医情報を配列に格納
list($arr_sect, $arr_sectdr) = get_sectdr($con, $fname);

$url_key1 = urlencode($key1);
$url_key2 = urlencode($key2);
$url_pt_nm = urlencode($pt_nm);
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function initPage() {
	sectOnChange2('<? echo($search_doc); ?>');
}

function sectOnChange2(doc) {
	var sect_id = document.search.search_sect.value;

	// 主治医セレクトボックスのオプションを全削除
	deleteAllOptions(document.search.search_doc);

	// 主治医セレクトボックスのオプションを作成
	addOption(document.search.search_doc, '0', 'すべて', doc);
<? foreach ($arr_sectdr as $sectdr) { ?>
	if (sect_id == '<? echo $sectdr["sect_id"]; ?>') {
	<? foreach($sectdr["drs"] as $dritem) { ?>
		addOption(document.search.search_doc, '<? echo $dritem["dr_id"]; ?>', '<? echo $dritem["dr_nm"]; ?>', doc);
	<? } ?>
	}
<? } ?>
}

function deleteAllOptions(box) {
	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}
}

function addOption(box, value, text, selected) {
	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	if (selected == value) {
		opt.selected = true;
	}
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="bed_menu.php?session=<? echo($session); ?>"><img src="img/icon/b17.gif" width="32" height="32" border="0" alt="病床管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="bed_menu.php?session=<? echo($session); ?>"><b>病床管理</b></a></font></td>
<? if ($section_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="entity_menu.php?session=<? echo($session); ?>&entity=1"><b>管理画面へ</b></a></font></td>
<? } else if ($ward_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="building_list.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="115" align="center" bgcolor="#5279a5"><a href="bed_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>病床管理トップ</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="ward_reference.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病棟照会</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="inpatient_reserve_register_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入退院登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="bedmake_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ベットメイク</font></a></td>
<td width="5">&nbsp;</td>
<td width="125" align="center" bgcolor="#bdd1e7"><a href="bed_rate_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床利用率管理</font></a>
</td>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#bdd1e7"><a href="bed_date_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">在院日数管理</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="bed_patient_export.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者データ抽出</font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="submenu">
<tr height="22">
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu.php?session=<? echo($session); ?>">お知らせ</a></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_status.php?session=<? echo($session); ?>">病棟状況一覧表</a></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_schedule.php?session=<? echo($session); ?>">入退院カレンダー</a></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院患者検索</font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_vacant_search.php?session=<? echo($session); ?>">空床検索</a></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_occupation.php?session=<? echo($session); ?>">病床利用状況</a></font></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<? write_search_form($pt_nm, $arr_sect, $search_sect, $session); ?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<!-- タブ2 -->
<? if ($path == "A") {  // 入院患者 ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="50" align="center" bgcolor="#5279a5"><a href="bed_menu_inpatient_patient.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=A&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>基本</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_sub.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=A&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基礎データ</font></a></td>
<td width="5">&nbsp;</td>
<td width="60" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_contact.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=A&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">連絡先</font></a></td>
<td width="5">&nbsp;</td>
<td width="50" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_claim.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=A&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">請求</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_detail.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=A&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院情報</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_check.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=A&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">チェックリスト</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_nutrition.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=A&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">栄養問診表</font></a></td>
<td width="5">&nbsp;</td>
<td width="170" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_exception.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=A&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">平均在院日数除外指定</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<? } else if ($path == "B") {  // 退院予定患者 ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="50" align="center" bgcolor="#5279a5"><a href="bed_menu_inpatient_patient.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=B&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>基本</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_sub.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=B&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基礎データ</font></a></td>
<td width="5">&nbsp;</td>
<td width="60" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_contact.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=B&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">連絡先</font></a></td>
<td width="5">&nbsp;</td>
<td width="50" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_claim.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=B&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">請求</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_detail.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=B&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院情報</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_check.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=B&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">チェックリスト</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_nutrition.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=B&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">栄養問診表</font></a></td>
<td width="5">&nbsp;</td>
<td width="170" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_exception.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=B&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">平均在院日数除外指定</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_out_reserve.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退院予定情報</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<? } ?>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<form action="inpatient_patient_detail_update_exe.php" method="post">
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者ID</font></td>
<td colspan="3"><input name="new_pt_id" type="text" value="<? echo $pt_id; ?>" maxlength="20" style="ime-mode:inactive;"></td>
</tr>
<tr height="22">
<td width="20%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">姓（漢字）</font></td>
<td width="30%"><input name="lt_nm" type="text" value="<? echo($lt_nm); ?>" maxlength="10" style="ime-mode:active;"></td>
<td width="20%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">名（漢字）</font></td>
<td width="30%"><input name="ft_nm" type="text" value="<? echo($ft_nm); ?>" maxlength="10" style="ime-mode:active;"></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">姓（かな）</font></td>
<td><input name="lt_kana_nm" type="text" value="<? echo($lt_kana_nm); ?>" maxlength="10" style="ime-mode:active;"></td>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">名（かな）</font></td>
<td><input name="ft_kana_nm" type="text" value="<? echo($ft_kana_nm); ?>" maxlength="10" style="ime-mode:active;"></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">生年月日</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="birth_yr"><? show_select_years(120, $birth_yr); ?></select>/<select name="birth_mon"><? show_select_months($birth_mon); ?></select>/<select name="birth_day"><? show_select_days($birth_day); ?></select></font></td>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">性別</font></td>
<td><select name="sex">
<option value="0"<? if ($sex == "0") {echo " selected";} ?>>不明
<option value="1"<? if ($sex == "1") {echo " selected";} ?>>男性
<option value="2"<? if ($sex == "2") {echo " selected";} ?>>女性
</select></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職業</font></td>
<td colspan="3"><input type="text" name="job" value="<? echo($job); ?>" style="ime-mode:active;"></td>
</tr>
</table>
<? if ($pt_reg_auth == 1) { ?>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="更新"></td>
</tr>
</table>
<? } ?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="pt_id" value="<? echo($pt_id); ?>">
<input type="hidden" name="path" value="<? echo($path); ?>">
<input type="hidden" name="pt_nm" value="<? echo($pt_nm); ?>">
<input type="hidden" name="search_sect" value="<? echo($search_sect); ?>">
<input type="hidden" name="search_doc" value="<? echo($search_doc); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
