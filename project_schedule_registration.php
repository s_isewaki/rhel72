<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 委員会・WG | 予定の追加</title>
<?
require("about_session.php");
require("about_authority.php");
require("show_select_values.ini");
require("show_project_schedule_common.ini");
require("holiday.php");
require("facility_common.ini");
require("show_facility.ini");

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 委員会・WG権限のチェック
$checkauth = check_authority($session, 31, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 初期表示時の設定
if ($back != "t") {
	$year = date("Y", $date);
	$month = date("m", $date);
	$day = date("d", $date);

	if ($time == "") {
		$hr = "09";
		$min = "00";
	} else {
		$hr = substr($time, 0, 2);
		$min = substr($time, 3, 2);
	}

	if ($schd_dur_hrs == "") {
		$schd_dur_hrs = sprintf("%02d", intval($hr) + 1);
		$schd_dur_min = $min;
		if ($schd_dur_hrs >= "24") {
			$schd_dur_hrs = "24";
			$schd_dur_min = "00";
		}
	}

	if ($repeat_type == "") {
		$repeat_type = "2";
	}

	if ($rpt_end_year == "") {
		$rpt_end_year = $year;
	}

	if ($rpt_end_month == "") {
		$rpt_end_month = $month;
	}

	if ($rpt_end_day == "") {
		$rpt_end_day = $day;
	}
} else {
	$year = $schd_start_yrs;
	$month = $schd_start_mth;
	$day = $schd_start_day;
	$hr = $schd_start_hrs;
	$min = $schd_start_min;
}

$date = mktime(0, 0, 0, $month, $day, $year);

// データベースに接続
$con = connect2db($fname);

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// 予約／管理可能なカテゴリ・設備一覧を配列で取得
$categories = get_reservable_categories($con, $emp_id, $fname);

// オプション設定情報を取得
$sql = "select calendar_start1 from option";
$cond = "where emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$calendar_start = pg_fetch_result($sel, 0, "calendar_start1");

// スタート曜日の設定
$start_wd = ($calendar_start == 1) ? 0 : 1;  // 0：日曜、1：月曜

if ($calendar == "") {
	$calendar = "off";
}
if ($repeat == "") {
	$repeat = "off";
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function initPage() {
	setDayOptions('<? echo($day); ?>');

<?
if ($datelist != "") {
	$tmp_arr_date = split(",", $datelist);
	foreach ($tmp_arr_date as $tmp_date) {
		echo("\tdateOnClick('$tmp_date')\n");
	}
}
?>

	for (var i = 0, j = document.mainform.cate_id.options.length; i < j; i++) {
		if (document.mainform.cate_id.options[i].value == '<? echo($cate_id); ?>') {
			document.mainform.cate_id.options[i].selected = true;
		}
	}
	cateOnChange('<? echo($facility_id); ?>');

	setRepeatDisplay('<? echo($repeat); ?>');
	setTimeDisabled();
	setCalendarDisplay('<? echo($calendar); ?>');
}

function setDayOptions(selectedDay) {
	var dayPulldown = document.mainform.schd_start_day;
	if (!selectedDay) {
		selectedDay = dayPulldown.value;
	}
	deleteAllOptions(dayPulldown);

	var year = parseInt(document.mainform.schd_start_yrs.value, 10);
	var month = parseInt(document.mainform.schd_start_mth.value, 10);
	var daysInMonth = new Date(year, month, 0).getDate();

	for (var d = 1; d <= daysInMonth; d++) {
		var tmpDayValue = d.toString();
		if (tmpDayValue.length == 1) {
			tmpDayValue = '0'.concat(tmpDayValue);
		}

		var tmpDayText = d.toString();

		var tmpDate = new Date(year, month - 1, d);
		var tmpWeekDay = tmpDate.getDay();
		switch (tmpWeekDay) {
		case 0:
			tmpDayText = tmpDayText.concat('（日）');
			break;
		case 1:
			tmpDayText = tmpDayText.concat('（月）');
			break;
		case 2:
			tmpDayText = tmpDayText.concat('（火）');
			break;
		case 3:
			tmpDayText = tmpDayText.concat('（水）');
			break;
		case 4:
			tmpDayText = tmpDayText.concat('（木）');
			break;
		case 5:
			tmpDayText = tmpDayText.concat('（金）');
			break;
		case 6:
			tmpDayText = tmpDayText.concat('（土）');
			break;
		}

		addOption(dayPulldown, tmpDayValue, tmpDayText);
	}

	while (parseInt(selectedDay, 10) > daysInMonth) {
		selectedDay = (parseInt(selectedDay, 10) - 1).toString();
	}
	dayPulldown.value = selectedDay;
}

function cateOnChange(facility_id) {

	// 施設・設備セレクトボックスのオプションを全削除
	deleteAllOptions(document.mainform.facility_id);

	// 施設・設備セレクトボックスのオプションを作成
	var category_id = document.mainform.cate_id.value;
<?
foreach ($categories as $tmp_category_id => $tmp_category) {
	echo("\tif (category_id == '$tmp_category_id') {\n");
	foreach ($tmp_category["facilities"] as $tmp_facility_id => $tmp_facility) {
		echo("\t\taddOption(document.mainform.facility_id, '$tmp_facility_id', '{$tmp_facility["name"]}', facility_id);\n");
	}
	echo("\t}\n");
}
?>

	// オプションが1件も作成されなかった場合
	var opt_cnt = document.mainform.facility_id.options.length;
	if (opt_cnt == 0) {
		addOption(document.mainform.facility_id, '0', '----------', facility_id);
	}
}

function deleteAllOptions(box) {
	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}
}

function addOption(box, value, text, selected) {
	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	if (selected == value) {
		opt.selected = true;
	}
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
}

function getSelectedValue(sel) {
	return sel.options[sel.selectedIndex].value;
}

function setRepeatDisplay(repeat) {
	if (repeat == 'on') {
		document.getElementById('repeat_off_row').style.display = 'none';
		document.getElementById('repeat_on_row').style.display = '';
		document.mainform.repeat.value = 'on';
	} else {
		document.getElementById('repeat_on_row').style.display = 'none';
		document.getElementById('repeat_off_row').style.display = '';
		document.mainform.repeat.value = 'off';
	}
}

function setTimeDisabled() {
	var disabled = document.mainform.timeless.checked;
	document.mainform.schd_start_hrs.disabled = disabled;
	document.mainform.schd_start_min.disabled = disabled;
	document.mainform.schd_dur_hrs.disabled = disabled;
	document.mainform.schd_dur_min.disabled = disabled;
	document.mainform.time_option.disabled = disabled;
	document.mainform.cate_id.disabled = disabled;
	document.mainform.facility_id.disabled = disabled;
	document.mainform.users.disabled = disabled;
}

function setCalendarDisplay(calendar) {
	if (calendar == 'on') {
		document.getElementById('date_row').style.display = 'none';
		document.getElementById('calendar_row').style.display = '';
		document.mainform.calendar.value = 'on';
		document.getElementById('repeat_off_row').style.display = 'none';
		document.getElementById('repeat_on_row').style.display = 'none';
	} else {
		document.getElementById('date_row').style.display = '';
		document.getElementById('calendar_row').style.display = 'none';
		document.mainform.calendar.value = 'off';
		setRepeatDisplay(document.mainform.repeat.value);
	}
}

function setTime() {
	var span_minutes = document.mainform.time_option.value;
	if (span_minutes == '') {
		return;
	}

	var start_hour = parseInt(document.mainform.schd_start_hrs.value, 10);
	var start_minute = parseInt(document.mainform.schd_start_min.value, 10);
	var start_timestamp = start_hour * 60 + start_minute;

	var end_timestamp = start_timestamp + parseInt(span_minutes, 10);
	while (end_timestamp > 24 * 60) {
		end_timestamp -= 30;
	}

	var end_hour = Math.floor(end_timestamp / 60);
	var end_minute = end_timestamp - end_hour * 60;

	document.mainform.schd_dur_hrs.value = lpad(end_hour);
	document.mainform.schd_dur_min.value = lpad(end_minute);
}

function lpad(num) {
	var str = num.toString();
	if (str.length == 1) {
		str = '0'.concat(str);
	}
	return str;
}

function dateOnClick(ymd) {
	var add;
	var datelist = document.mainform.datelist.value;
	if (datelist.length == 0) {
		datelist += ymd;
		add = true;
	} else if (datelist.indexOf(ymd) == -1) {
		datelist += ',' + ymd;
		add = true;
	} else {
		if (datelist.length == 8) {
			datelist = '';
		} else {
			datelist = datelist.replace(',' + ymd, '');
		}
		add = false;
	}
	document.mainform.datelist.value = datelist;

	var cell = document.getElementById('id' + ymd);
	if (add) {
		cell.style.backgroundColor = '#ccffcc';
	} else {
		cell.style.backgroundColor = '';
	}
}

function checkEndMinute() {
	if (document.mainform.schd_dur_hrs.value == '24' &&
		document.mainform.schd_dur_min.value != '00') {
		document.mainform.schd_dur_min.value = '00';
		alert('終了時刻が24時の場合には00分しか選べません。');
	}
}

function registerSchedule() {
<? if (is_array($log)) { ?>
	if (!confirm('新たな予定が追加されますが、よろしいですか？')) {
		return;
	}
<? } ?>
	document.mainform.submit();
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td, .inner td .list td {border:#5279a5 solid 1px;}
.inner td {border-style:none;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="initPage();">
<center>
<form name="mainform" action="project_schedule_registration_checker.php" method="post">
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>予定の追加</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="600" border="0" cellpadding="2" cellspacing="0">
<tr height="22">
<td align="right"><input type="button" value="登録" onclick="registerSchedule();"></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td bgcolor="#f6f9ff" width="120" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイトル</font></td>
<td colspan="2"><input type="text" name="schd_title" value="<? echo($schd_title); ?>" maxlength="100" size="50" style="ime-mode:active;"></td>
</tr>
<tr heigth="22">
<td width="125" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">行先</font></td>
<td colspan="2"><select name="place_id"><option value=""></option><? show_schedule_place_selected($con, $place_id, $fname); ?><option value="0">その他</option></select>&nbsp;<input type="text" name="schd_plc" value="<? echo($schd_plc); ?>" maxlength="100" size="35" style="ime-mode:active;"></td>
</tr>
<tr id="date_row" height="22" style="display:none;">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日付</font></td>
<td style="border-right-style:none;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="schd_start_yrs" onchange="setDayOptions();"><? show_select_years_span(min($year, 2000), date("Y") + 3, $year); ?></select>/<select name="schd_start_mth" onchange="setDayOptions();"><? show_select_months($month); ?></select>/<select name="schd_start_day"></select></font></td>
<td align="right" style="border-left-style:none;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:void(0);" onclick="setCalendarDisplay('on');">カレンダー指定</a></font></td>
</tr>
<tr id="calendar_row" style="display:none;">
<td colspan="3">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="inner">
<tr>
<td align="center" valign="top"><? show_calendar($year, $month, $start_wd, 0); ?></td>
<td width="5"></td>
<td align="center" valign="top"><? show_calendar($year, $month, $start_wd, 1); ?></td>
<td width="5"></td>
<td align="center" valign="top"><? show_calendar($year, $month, $start_wd, 2); ?></td>
</tr>
<tr>
<td colspan="5" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:void(0);" onclick="setCalendarDisplay('off');">日付指定</a></font></td>
</tr>
</table>
</td>
</tr>
<tr heigth="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時刻</font></td>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">

<select name="schd_start_hrs" onchange="setTime();"><? show_hour_options_0_23($hr, false); ?></select>：<select name="schd_start_min" onchange="setTime();"><? show_min_options_5($min) ?></select>〜<select name="schd_dur_hrs" onchange="checkEndMinute();"><? show_select_hrs_by_args(0, 24, $schd_dur_hrs, false); ?></select>：<select name="schd_dur_min" onchange="checkEndMinute();"><? show_min_options_5($schd_dur_min) ?></select>
<!--
<select name="schd_start_hrs" onchange="setTime();"><? show_hour_options_0_23($hr, false); ?></select>：<select name="schd_start_min" onchange="setTime();"><option value="00"<? if ($min == "00") {echo(" selected");} ?>>00<option value="30"<? if ($min == "30") {echo(" selected");} ?>>30</select>〜<select name="schd_dur_hrs" onchange="checkEndMinute();"><? show_select_hrs_by_args(0, 24, $schd_dur_hrs, false); ?></select>：<select name="schd_dur_min" onchange="checkEndMinute();"><option value="00"<? if ($schd_dur_min == "30") {echo(" selected");} ?>>00<option value="30"<? if ($schd_dur_min == "30") {echo(" selected");} ?>>30</select>
-->

<img src="img/spacer.gif" alt="" width="15" height="1">
時間指定：<select name="time_option" onchange="setTime();">
<option value="">
<option value="30"<? if ($time_option == "30") {echo(" selected");} ?>>30分
<option value="60"<? if ($time_option == "60") {echo(" selected");} ?>>1時間
<option value="90"<? if ($time_option == "90") {echo(" selected");} ?>>1時間30分
<option value="120"<? if ($time_option == "120") {echo(" selected");} ?>>2時間
<option value="150"<? if ($time_option == "150") {echo(" selected");} ?>>2時間30分
<option value="180"<? if ($time_option == "180") {echo(" selected");} ?>>3時間
</select><br>
<input type="checkbox" name="timeless" value="t"<? if ($timeless == "t") {echo(" checked");} ?> onclick="setTimeDisabled();">指定しない<font color="gray">（施設・設備の同時予約は不可）</font>
</font></td>
</tr>
<tr>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">種別</font></td>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_proschd_type_radio($con, $schd_type, $fname); ?></font></td>
</tr>
<tr id="repeat_off_row" style="display:none;">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><img src="img/icon/plus.gif" alt="" width="20" height="20" style="margin-right:3px;vertical-align:middle;" onclick="setRepeatDisplay('on');">繰り返し</font></td>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="gray">（指定しない）</font></td>
</tr>
<tr id="repeat_on_row" style="display:none;">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><img src="img/icon/minus.gif" alt="" width="20" height="20" style="margin-right:3px;vertical-align:middle;" onclick="setRepeatDisplay('off');">繰り返し</font></td>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="repeat_type" value="2"<? if ($repeat_type == "2") {echo(" checked");} ?>><select name="rpt2_1" onchange="this.form.repeat_type[0].click();">
<option value="1"<? if ($rpt2_1 == "1") {echo(" selected");} ?>>毎
<option value="2"<? if ($rpt2_1 == "2") {echo(" selected");} ?>>隔
</select><select name="rpt2_2" onchange="this.form.repeat_type[0].click();">
<option value="1"<? if ($rpt2_2 == "1") {echo(" selected");} ?>>日
<option value="2"<? if ($rpt2_2 == "2") {echo(" selected");} ?>>週
<option value="3"<? if ($rpt2_2 == "3") {echo(" selected");} ?>>月
<option value="4"<? if ($rpt2_2 == "4") {echo(" selected");} ?>>月、水、金
<option value="5"<? if ($rpt2_2 == "5") {echo(" selected");} ?>>火、木
<option value="6"<? if ($rpt2_2 == "6") {echo(" selected");} ?>>月〜金
<option value="7"<? if ($rpt2_2 == "7") {echo(" selected");} ?>>土、日
</select><br>
<input type="radio" name="repeat_type" value="3"<? if ($repeat_type == "3") {echo(" checked");} ?>><select name="rpt3_1" onchange="this.form.repeat_type[1].click();">
<option value="1"<? if ($rpt3_1 == "1") {echo(" selected");} ?>>毎月（まいつき）
<option value="2"<? if ($rpt3_1 == "2") {echo(" selected");} ?>>毎隔月
<option value="3"<? if ($rpt3_1 == "3") {echo(" selected");} ?>>毎3ヶ月ごと
</select><select name="rpt3_2" onchange="this.form.repeat_type[1].click();">
<option value="1"<? if ($rpt3_2 == "1") {echo(" selected");} ?>>第1
<option value="2"<? if ($rpt3_2 == "2") {echo(" selected");} ?>>第2
<option value="3"<? if ($rpt3_2 == "3") {echo(" selected");} ?>>第3
<option value="4"<? if ($rpt3_2 == "4") {echo(" selected");} ?>>第4
<option value="5"<? if ($rpt3_2 == "5") {echo(" selected");} ?>>最終
</select><select name="rpt3_3" onchange="this.form.repeat_type[1].click();">
<option value="1"<? if ($rpt3_3 == "1") {echo(" selected");} ?>>日
<option value="2"<? if ($rpt3_3 == "2") {echo(" selected");} ?>>月
<option value="3"<? if ($rpt3_3 == "3") {echo(" selected");} ?>>火
<option value="4"<? if ($rpt3_3 == "4") {echo(" selected");} ?>>水
<option value="5"<? if ($rpt3_3 == "5") {echo(" selected");} ?>>木
<option value="6"<? if ($rpt3_3 == "6") {echo(" selected");} ?>>金
<option value="7"<? if ($rpt3_3 == "7") {echo(" selected");} ?>>土
</select><br>
　終了日：<select name="rpt_end_year">
<? show_select_years_span(min($rpt_end_year, 2000), date("Y") + 3, $rpt_end_year); ?>
</select>/<select name="rpt_end_month">
<? show_select_months($rpt_end_month); ?>
</select>/<select name="rpt_end_day">
<? show_select_days($rpt_end_day); ?>
</select>
</font></td>
</tr>
<tr>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">詳細</font></td>
<td colspan="2"><textarea name="schd_detail" rows="5" cols="40" style="ime-mode:active;"><? echo($schd_detail); ?></textarea></td>
</tr>
<tr>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">施設・設備予約</font></td>
<td colspan="2"><select name="cate_id" onchange="cateOnChange();"><? show_category_options($categories, $cate_id); ?></select><select name="facility_id"></select>&nbsp;&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">利用人数：<input type="text" name="users" value="<? echo($users); ?>" size="4" maxlength="4" style="ime-mode:inactive;">名</font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr>
<td height="22" align="right"><input type="button" value="登録" onclick="registerSchedule();"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="pjt_id" value="<? echo($pjt_id); ?>">
<input type="hidden" name="calendar" value="">
<input type="hidden" name="repeat" value="">
<input type="hidden" name="datelist" value="">
</form>
<?
if (is_array($log)) {
	show_log($log);
}
?>
</center>
</body>
<? pg_close($con); ?>
</html>
<?
// 設備カテゴリオプションを出力
function show_category_options($categories, $cate_id) {
	$len = 0;
	foreach ($categories as $tmp_category) {
		$tmp_len = mb_strwidth($tmp_category["name"], "EUC-JP");
		if ($tmp_len > $len) {
			$len = $tmp_len;
		}
	}
	if ($len == 0) {$len = 10;}

	echo("<option value=\"0\">" . str_repeat("-", $len) . "\n");
	foreach ($categories as $tmp_category_id => $tmp_category) {
		echo("<option value=\"$tmp_category_id\">{$tmp_category["name"]}\n");
	}
}

// ログを表示
function show_log($log) {

	echo("<table width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\" style=\"margin-top:5px;\">\n");
	echo("<tr>\n");
	echo("<td colspan=\"3\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><b>施設・設備予約結果</b></font></td>\n");
	echo("</tr>\n");
	echo("<tr bgcolor=\"#f6f9ff\">\n");
	echo("<td height=\"22\" width=\"30\"></td>\n");
	echo("<td width=\"120\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">日付</font></td>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">結果</font></td>\n");
	echo("</tr>\n");

	foreach ($log as $row) {
		list($date, $msg) = split(",", $row);

		echo("<tr>\n");
		echo("<td height=\"22\" width=\"30\"></td>\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$date</font></td>\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$msg</font></td>\n");
		echo("</tr>\n");
	}

	echo("</table>\n");

}

// 1か月分のカレンダーを出力
function show_calendar($year, $month, $start_wd, $offset) {

	// 処理対象年月を求める
	for ($i = 0; $i < $offset; $i++) {
		if ($month == "12") {
			$month = "01";
			$year++;
		} else {
			$month = substr("0" . ($month + 1), -2);
		}
	}

	// 日付配列を取得
	$arr_month = get_arr_month("$year$month", $start_wd);

	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
	echo("<tr>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$year/$month</font></td>\n");
	echo("</tr>\n");
	echo("<tr>\n");
	echo("<td>\n");
	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"list\">\n");
	echo("<tr>\n");
	if ($start_wd == 0) {
		echo("<td width=\"14%\" align=\"center\" bgcolor=\"#fadede\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">日</font></td>\n");
	}
	echo("<td width=\"14%\" align=\"center\" bgcolor=\"#fefcdf\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">月</font></td>\n");
	echo("<td width=\"14%\" align=\"center\" bgcolor=\"#fefcdf\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">火</font></td>\n");
	echo("<td width=\"14%\" align=\"center\" bgcolor=\"#fefcdf\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">水</font></td>\n");
	echo("<td width=\"14%\" align=\"center\" bgcolor=\"#fefcdf\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">木</font></td>\n");
	echo("<td width=\"14%\" align=\"center\" bgcolor=\"#fefcdf\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">金</font></td>\n");
	echo("<td width=\"14%\" align=\"center\" bgcolor=\"#defafa\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">土</font></td>\n");
	if ($start_wd == 1) {
		echo("<td width=\"14%\" align=\"center\" bgcolor=\"#fadede\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">日</font></td>\n");
	}
	echo("</tr>\n");

	foreach ($arr_month as $arr_week) {
		echo("<tr>\n");

		foreach ($arr_week as $tmp_ymd) {
			$tmp_y = substr($tmp_ymd, 0, 4);
			$tmp_m = substr($tmp_ymd, 4, 2);
			if ($tmp_m != $month) {
				echo("<td>&nbsp;</td>\n");
			} else {
				if (ktHolidayName(to_timestamp($tmp_ymd)) != "") {
					$bgcolor = "#fadede";
				} else {
					$bgcolor = "#f6f9ff";
				}
				$tmp_d = intval(substr($tmp_ymd, 6, 2));
				echo("<td id=\"id$tmp_ymd\" align=\"center\" bgcolor=\"$bgcolor\" style=\"cursor:pointer;\" onclick=\"dateOnClick('$tmp_ymd');\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a id=\"id{$tmp_ymd}_a\" href=\"javascript:void(0);\">$tmp_d</a></font></td>\n");
			}
		}

		echo("</tr>");
	}

	echo("</table>\n");
	echo("</td>\n");
	echo("</tr>\n");
	echo("</table>\n");
}

// 指定年月分の日付配列を返す
function get_arr_month($ym, $start_wd) {

	// 当月1日〜末日の配列を作成
	$year = substr($ym, 0, 4);
	$month = substr($ym, 4, 2);
	$arr_date = array();
	for ($i = 1; $i <= 31; $i++) {
		$day = substr("0" . $i, -2);
		if (checkdate($month, $day, $year)) {
			array_push($arr_date, "$year$month$day");
		} else {
			break;
		}
	}

	// 空白セル分の日付を配列の先頭に追加
	$first_day = mktime(0, 0, 0, $month, "01", $year);
	$empty = date("w", $first_day) - $start_wd;
	if ($empty < 0) {
		$empty += 7;
	}
	for ($i = 1; $i <= $empty; $i++) {
		array_unshift($arr_date, date("Ymd", strtotime("-$i days", $first_day)));
	}

	// 空白セル分の日付を配列の末尾に追加
	$day = substr($arr_date[count($arr_date) - 1], -2);
	$end_day = mktime(0, 0, 0, $month, $day, $year);
	$empty = 7 - (count($arr_date) % 7);
	if ($empty == 7) {
		$empty = 0;
	}
	for ($i = 1; $i <= $empty; $i++) {
		array_push($arr_date, date("Ymd", strtotime("$i days", $end_day)));
	}

	// 週単位に分割した配列を返す
	return array_chunk($arr_date, 7);
}

// 日付をタイムスタンプに変換
function to_timestamp($yyyymmdd) {
	$y = substr($yyyymmdd, 0, 4);
	$m = substr($yyyymmdd, 4, 2);
	$d = substr($yyyymmdd, 6, 2);
	return mktime(0, 0, 0, $m, $d, $y);
}
?>
