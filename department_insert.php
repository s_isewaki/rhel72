<?php
require("about_session.php");
require("about_authority.php");
require("show_class_name.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 23, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// データベースに接続
$con = connect2db($fname);
pg_query($con, "begin");

$arr_class_name = get_class_name_array($con, $fname);

// 入力チェック
if ($dept_name == "") {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\">alert('{$arr_class_name[2]}名を入力してください。');</script>\n");
    echo("<script type=\"text/javascript\">history.back();</script>\n");
    exit;
}
if (strlen($dept_name) > 60) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\">alert('{$arr_class_name[2]}名が長すぎます。');</script>\n");
    echo("<script type=\"text/javascript\">history.back();</script>\n");
    exit;
}

//XX if (strlen($link_key1) > 12) {
//XX     pg_query($con, "rollback");
//XX     pg_close($con);
//XX     echo("<script type=\"text/javascript\">alert('外部連携キー1が長すぎます。');</script>\n");
//XX     echo("<script type=\"text/javascript\">history.back();</script>\n");
//XX     exit;
//XX }
//XX if (strlen($link_key2) > 12) {
//XX     pg_query($con, "rollback");
//XX     pg_close($con);
//XX     echo("<script type=\"text/javascript\">alert('外部連携キー2が長すぎます。');</script>\n");
//XX     echo("<script type=\"text/javascript\">history.back();</script>\n");
//XX     exit;
//XX }
//XX if (strlen($link_key3) > 12) {
//XX     pg_query($con, "rollback");
//XX     pg_close($con);
//XX     echo("<script type=\"text/javascript\">alert('外部連携キー3が長すぎます。');</script>\n");
//XX     echo("<script type=\"text/javascript\">history.back();</script>\n");
//XX     exit;
//XX }

// 科IDを採番
$sql = "select max(dept_id) from deptmst";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$dept_id = intval(pg_fetch_result($sel, 0, 0)) + 1;

// 表示順の決定
$sql = "select max(order_no) from deptmst";
$cond = "where atrb_id = $atrb_id and not dept_del_flg";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$order_no = intval(pg_fetch_result($sel, 0, 0)) + 1;

// 科情報を登録
$sql  = "insert into deptmst (dept_id, atrb_id, dept_nm, link_key1, link_key2, link_key3, order_no) values (";
$content = array($dept_id, $atrb_id, $dept_name, $link_key1, $link_key2, $link_key3, $order_no);
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// データベース接続を閉じる
pg_query($con, "commit");
pg_close($con);

// 組織一覧画面に遷移
echo("<script type=\"text/javascript\">location.href = 'class_menu.php?session=$session&class_id=$class_id&atrb_id=$atrb_id';</script>");
