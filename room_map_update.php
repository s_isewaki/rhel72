<?
require("about_authority.php");
require("about_session.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 21, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// マップ情報をいったん削除
$sql = "delete from ptrmmap";
$cond = "where bldg_cd = $bldg_cd and ward_cd = $ward_cd";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// マップ情報を作成
$created = false;
foreach ($_POST as $tmp_key => $tmp_room_no) {
	if (substr($tmp_key, 0, 6) != "target") {
		continue;
	}

	$created = true;

	list($prefix, $tmp_row_no, $tmp_column_no) = split("_", $tmp_key);
	switch ($tmp_room_no) {
	case "ns":
		$tmp_room_type = 2;
		$tmp_room_no = null;
		break;
	case "wc":
		$tmp_room_type = 3;
		$tmp_room_no = null;
		break;
	default:
		$tmp_room_type = 1;
		break;
	}
	$sql = "insert into ptrmmap (bldg_cd, ward_cd, row_no, column_no, room_type, ptrm_room_no) values (";
	$content = array($bldg_cd, $ward_cd, $tmp_row_no, $tmp_column_no, $tmp_room_type, $tmp_room_no);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// 列数・行数情報をDELETE〜INSERT
$sql = "delete from ptrmmapboard";
$cond = "where bldg_cd = $bldg_cd and ward_cd = $ward_cd";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if ($created) {
	$sql = "insert into ptrmmapboard (bldg_cd, ward_cd, column_count, row_count) values (";
	$content = array($bldg_cd, $ward_cd, $column_count, $row_count);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 病床マップ画面を表示
echo("<script type=\"text/javascript\">location.href = 'room_map.php?session=$session&bldg_cd=$bldg_cd&ward_cd=$ward_cd';</script>");
?>
