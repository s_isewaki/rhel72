<?php

mb_internal_encoding('EUC-JP');

require_once("about_session.php");
require_once("about_postgres.php");
require_once("show_select_values.ini");
require_once("referer_common.ini");
require_once("get_values.ini");
require_once("jnl_application_imprint_common.ini");
require_once("./conf/sql.inf");
require_once("jnl_application_common.ini");
require_once('jnl_indicator_facility_data.php');
require_once('jnl_indicator_facility_display.php');

$fname = $PHP_SELF;

if($argv[1] !="")
{
	//バッチから来た場合
	

	$userid="admin";
	
	$con = connect2db($fname);
	delete_old_session($con,$userid,$session,$fname);
	$session=register_session($con,$userid,$fname);
	
	// 職員ID取得
	//$emp_id=get_emp_id($con,$session,$fname);

	$display_year=substr($argv[1],0,4);
	$display_month = substr($argv[1],4,2);
	

	exec_fuction_jnl($session,$con,$display_year,$display_month);
	
	pg_close($con);
	
	//echo("aaaa".$argv[1]);
	exit;
	

}


//処理を実行
function exec_fuction_jnl($session,$con,$display_year,$display_month)
{

	//保存ディレクトリ作成	
	if (!is_dir("jnl_apply/batch")) 
	{
		mkdir("jnl_apply/batch", 0777);
	}
	if (!is_dir("jnl_apply/batch/071_facility")) 
	{
		mkdir("jnl_apply/batch/071_facility", 0777);
	}
	if (!is_dir("jnl_apply/batch/071_all")) 
	{
		mkdir("jnl_apply/batch/071_all", 0777);
	}

	$mon = $display_month."月";

	//echo($display_year.$mon);

	$year=$display_year."年";


	//病院集計＝'病院別月間報告書患者数総括'
	$no="71";
	$plant=null;
	$file_name = "";
	summary_of_number_of_monthly_report_patients_according_to_hospital($display_year,$no,$plant,$session,$fname,$con,$file_name,$year,$mon,$display_month);
	
	
	//施設別＝'病院別月間報告書患者数総括'
	$no="74";
	$plant="1";
	
	$sql  = "select * from jnl_facility_master where jnl_facility_type='1' order by jnl_facility_id";
	$cond="";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	
	//病院施設数ぶんループ
	while ($row = pg_fetch_array($sel)) 
	{
		
		$file_name = $row["jnl_facility_name"];
		$plant = $row["jnl_facility_id"];
		
		summary_of_number_of_monthly_report_patients_according_to_hospital($display_year,$no,$plant,$session,$fname,$con,$file_name,$year,$mon,$display_month);
	}
	
}





//施設別＝'病院別月間報告書患者数総括'
function summary_of_number_of_monthly_report_patients_according_to_hospital($display_year,$no,$plant,$session,$fname,$con,$file_name,$year,$mon,$display_month)
{
	
	/* @var $indicatorData IndicatorFacilityData */
	$indicatorData = new IndicatorFacilityData(array('fname' => $fname, 'con' => $con));
	/* @var $indicatorDisplay IndicatorFacilityDisplay */
	$indicatorDisplay = new IndicatorFacilityDisplay(array(
				'fname'         => $fname,
				'con'           => $con,
				'session'       => $session,
				'display_year'  => $display_year,
				'display_month' => $display_month,
				'display_day'   => $display_day
				));
	
	$indicatorDisplay->setCurrentYear($display_year);
	$fiscal_year  = $indicatorDisplay->getCurrentYear();

	$display_year  = !empty($fiscal_year)? $fiscal_year : $display_year;
	
	$display_month = $indicatorDisplay->getDisplayMonth();
	$display_day   = $indicatorDisplay->getDisplayDay($display_month, $display_year);
	
	$noArr         = $indicatorData->getNoForNo($no);
	$fileNameType  = $noArr['file_name'];
	
	$plantArr = $indicatorData->getHospitalList($plant);
	
	if ($plantArr==false) {
		$plantArr = $indicatorData->getHealthFacilities($plant);
	}
	$dataList = $indicatorData->getDisplayDataList(
			$no,
			$indicatorDisplay->getDateToLastYearStartMonth(),
			$indicatorDisplay->getDateToCurrentYearLastMonth(),
			array('year' => $display_year, 'month' => $display_month, 'day' => $display_day, 'facility_id' => $plant)
			);
	$download_data = $indicatorDisplay->outputExcelData(
			$no,
			$dataList,
			$plantArr,
			$noArr,
			array('year' => $display_year, 'month' => $display_month, 'day' => $display_day, 'facility_id' => $plant)
			);


	$excel_name = "";
	if($plant == null)
	{
		//病院集計ファイル名
		$excel_name = "jnl_apply/batch/071_all/".$file_name."(".$year.$mon.")";
	}
	else
	{
		//施設別ファイル名
		$number = sprintf("%03d", $plant);
		$excel_name = "jnl_apply/batch/071_facility/".$number.$file_name."(".$fiscal_year."年度)";
	}
	
		
	$fp = fopen(mb_convert_encoding($excel_name,'utf-8','EUC-JP').".xls", "w");
	fwrite($fp, mb_convert_encoding(
				nl2br($download_data),
				'sjis',
				'EUC-JP'
				));

	fclose($fp);
	
}

?>