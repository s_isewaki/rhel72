<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 患者管理｜家族構成図</title>
<?
require("about_session.php");
require("about_authority.php");
require("about_postgres.php");
require("show_select_values.ini");
require("get_values.ini");
require("conf/sql.inf");
require("label_by_profile_type.ini");
require("show_select_years_wareki.ini");

$fname = $PHP_SELF;

$session = qualify_session($session, $fname);
if ($session == '0') {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

$ptreg   = check_authority($session, 22, $fname);
$ptif    = check_authority($session, 15, $fname);
$outreg  = check_authority($session, 35, $fname);
$inout   = check_authority($session, 33, $fname);
$disreg  = check_authority($session, 36, $fname);
$dishist = check_authority($session, 34, $fname);

if ($ptif == '0') {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

$section_admin_auth = check_authority($session, 28, $fname);

$patient_admin_auth = check_authority($session, 77, $fname);

$con = connect2db($fname);

$emp_id = get_emp_id($con, $session, $fname);

$profile_type = get_profile_type($con, $fname);

$sql  = "select ptif_lt_kaj_nm, ptif_ft_kaj_nm from ptifmst";
$cond = "where ptif_id = '" . $pt_id . "'";
$sel  = select_from_table($con, $sql, $cond, $fname);

if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$pt_nm = pg_fetch_result($sel, 0, 'ptif_lt_kaj_nm') . ' ' . pg_fetch_result($sel, 0, 'ptif_ft_kaj_nm');


$url_key1 = urlencode($key1);
$url_key2 = urlencode($key2);

// セーブデータ呼び出し
$sql      = "select svg_data from genogram";
//$cond     = "where ptif_id='" . $pt_id . "' and emp_id='" . $emp_id . "' and tpl_flg = '2' and delete_flg = '0'";	// 20120306　キーを患者IDのみに変更
$cond     = "where ptif_id='" . $pt_id . "' and tpl_flg = '2' and delete_flg = '0'";
$sel      = select_from_table($con, $sql, $cond, $fname);
$svg_data = pg_fetch_row($sel);

if ($svg_data[0] != '') {
    $sdcnt = 1;
} else {
    $sdcnt = 0;
}

// データ存在フラグ
$is_data_flg = 0;
if (0 < $sdcnt) {
    $is_data_flg = 1;
}
// データ存在の場合は画像呼出
if ($is_data_flg == 1) {
//    $sql = "select img_data from genogram where ptif_id='" . $pt_id . "' and emp_id='" . $emp_id . "' and tpl_flg = '2' and delete_flg = '0'";	// 20120306　キーを患者IDのみに変更
    $sql = "select img_data from genogram where ptif_id='" . $pt_id . "' and tpl_flg = '2' and delete_flg = '0'";
    $sel = select_from_table($con, $sql, '', $fname);
    $svg_data = pg_fetch_all($sel);
}

?>

<link rel="stylesheet" type="text/css" href="css/main.css">

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type='text/javascript'>
    $(function () {
        $('#pt_gm_disp').css({
           'width':'890',
           'height':'500'
        });
        $('#btn_rld').click(function () {
            location.href = 'patient_genogram.php?session=<?=$session?>&pt_id=<?=$pt_id?>';
        });
        $('#btn_del').click(function () {
            if (confirm('データを削除します。よろしいですか？')) {
                var sts = 'D';
                var pt_id  = '<?=$pt_id?>';
                var emp_id = '<?=$emp_id?>';
                var data1 = {"status":sts, "pt_id":pt_id, "emp_id":emp_id};
                $.ajax({
                    type: 'POST',
                    url: 'svg_data_accessor.php',
                    data: data1,
                    success: function(data) {
                        alert('データを削除しました。');
                        $('#btn_rld').trigger('click');
                    }
                });
            }
        });
    });
    function openSvgEditor() {
        window.open('genogram_editor.php?session=<?=$session ?>&pt_id=<?=$pt_id?>', 'newwin', 'width=800,height=700,scrollbars=yes');
    }
</script>
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr bgcolor="#f6f9ff">
    <td width="32" height="32" class="spacing"><a href="patient_info_menu.php?session=<? echo($session); ?>"><img src="img/icon/b16.gif" width="32" height="32" border="0" alt="<? echo($_label_by_profile["PATIENT_MANAGE"][$profile_type]); ?>"></a></td>
    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="patient_info_menu.php?session=<? echo($session); ?>"><b><? echo($_label_by_profile["PATIENT_MANAGE"][$profile_type]); ?></b></a></font></td>
<? if ($section_admin_auth == '1') { ?>
    <td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="entity_menu.php?session=<? echo($session); ?>&entity=2"><b>管理画面へ</b></a></font></td>
<? } else if ($patient_admin_auth == '1') { ?>
    <td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="patient_admin_menu.php?session=<? echo($session); ?>&entity=2"><b>管理画面へ</b></a></font></td>
<? } ?>
  </tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr height="22">
    <td width="70" align="center" bgcolor="#bdd1e7"><a href="patient_info_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">検索</font></a></td>
<? if ($ptif == 1) { ?>
    <td width="5">&nbsp;</td>
    <td width="70" align="center" bgcolor="#bdd1e7"><a href="patient_detail.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&page=<? echo($page); ?>&key1=<? echo($key1); ?>&key2=<? echo($ekey2); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基本</font></a></td>
    <td width="5">&nbsp;</td>
    <td width="85" align="center" bgcolor="#bdd1e7"><a href="patient_insurance.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo($url_key1); ?>&key2=<? echo($url_key2); ?>&page=<? echo($page); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">健康保険</font></a></td>
    <td width="5">&nbsp;</td>
    <td width="95" align="center" bgcolor="#5279a5"><a href="patient_genogram.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>家族構成図</b></font></a></td>
    <td width="5">&nbsp;</td>
    <td width="95" align="center" bgcolor="#bdd1e7"><a href="patient_sub_setting.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo($url_key1); ?>&key2=<? echo($url_key2); ?>&page=<? echo($page); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基礎データ</font></a></td>
    <td width="5">&nbsp;</td>
    <td width="75" align="center" bgcolor="#bdd1e7"><a href="patient_contact_setting.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo($url_key1); ?>&key2=<? echo($url_key2); ?>&page=<? echo($page); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">連絡先</font></a></td>
    <td width="5">&nbsp;</td>
    <td width="70" align="center" bgcolor="#bdd1e7"><a href="patient_claim_setting.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo($url_key1); ?>&key2=<? echo($url_key2); ?>&page=<? echo($page); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">請求</font></a></td>
<? } ?>
<? if ($inout == 1) { ?>
    <td width="5">&nbsp;</td>
    <td width="110" align="center" bgcolor="#bdd1e7"><a href="inoutpatient_detail.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo($url_key1); ?>&key2=<? echo($url_key2); ?>&page=<? echo($page); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($_label_by_profile["IN_OUT"][$profile_type]); ?></font></a></td>
<? } ?>
<? if ($dishist == 1) { ?>
    <td width="5">&nbsp;</td>
    <td width="125" align="center" bgcolor="#bdd1e7"><a href="disease_detail.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo($url_key1); ?>&key2=<? echo($url_key2); ?>&page=<? echo($page); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">プロブレムリスト</font></a></td>
<? } ?>
<? if ($ptif == 1) { ?>
    <td width="5">&nbsp;</td>
    <td width="135" align="center" bgcolor="#bdd1e7"><a href="patient_karte.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo($url_key1); ?>&key2=<? echo($url_key2); ?>&page=<? echo($page); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($_label_by_profile["KARTE_NO"][$profile_type]); ?></font></a></td>
<? } ?>
    <td width="">&nbsp;</td>
  </tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
  </tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
  <tr height="22">
    <td width="20%" bgcolor="#f6f9ff" align="right">
      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($_label_by_profile["PATIENT_ID"][$profile_type]); ?></font>
    </td>
    <td width="30%" bgcolor="#ffffff">
      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($pt_id); ?></font>
    </td>
    <td width="20%" bgcolor="#f6f9ff" align="right">
      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($_label_by_profile["PATIENT_NAME"][$profile_type]); ?></font>
    </td>
    <td width="30%" bgcolor="#ffffff">
      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo("$pt_nm"); ?></font>
    </td>
  </tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
  <tr height="22">
    <td width="70%" bgcolor="#ffffff">
<? if ($is_data_flg == 1) { ?>
      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">　　構成図登録済　　　<a href="javascript:void(0);" onclick="openSvgEditor();">編集する</a></font>
      <input id="btn_rld" type="button" value="画面更新" style="margin-left:100px;">
      <input id="btn_del" type="button" value="データ削除" style="margin-left:140px;">
<? } else { ?>
      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">　　登録されている構成図はありません　　　<a href="javascript:void(0);" onclick="openSvgEditor();">新規作成</a></font>
<? } ?>
    </td>
  </tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>

<table id="pt_gm_disp" class="list">
  <tr>
    <td style="width:230px">
      <img src="svg_editor/images/genogram_input_sample.png" style="width:90%; height:90%; margin-left:16px;">
    </td>
    <td width="660px" bgcolor="#ffffff">
<? if ($svg_data[0]['img_data'] && $svg_data[0]['img_data'] != '') { ?>
      <image src="<?=$svg_data[0]['img_data']?>">
<? } else { ?>
      &nbsp;
<? } ?>
    </td>
  </tr>
</table>

</body>
<? pg_close($con); ?>
</html>
