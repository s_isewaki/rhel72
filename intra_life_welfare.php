<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_session.php");
require("about_authority.php");
require("menu_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 55, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 私たちの生活サポート設定権限の取得
$life_setting_auth = check_authority($session, 54, $fname);

// 文書フォルダがなければ作成
if (!is_dir("intra")) {mkdir("intra", 0755);}
if (!is_dir("intra/welfare")) {mkdir("intra/welfare", 0755);}

// データベースに接続
$con = connect2db($fname);

// ライフステージ情報を配列に格納
$sql = "select lifestage_id, name from lifestage";
$cond = "where exists (select * from welfare where welfare.lifestage_id = lifestage.lifestage_id) order by order_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$arr_stage = array();
while ($row = pg_fetch_array($sel)) {
	$lifestage_id = $row["lifestage_id"];
	$stage_name = $row["name"];

	$arr_stage[$lifestage_id] = array();
	$arr_stage[$lifestage_id]["name"] = $stage_name;
	$arr_stage[$lifestage_id]["welfare"] = array();
}
$lifestage_count = count($arr_stage);

// 文書情報を配列に追加
$sql = "select * from welfare";
$cond = "order by welfare.welfare_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
while ($row = pg_fetch_array($sel)) {
	$lifestage_id = $row["lifestage_id"];
	$welfare_id = $row["welfare_id"];
	$system = $row["system"];
	$description = $row["description"];

	$arr_stage[$lifestage_id]["welfare"]["$welfare_id"] = array(
		"system" => $system, "description" => $description
	);
}

// 文書登録権限があるか確認
$sql = "select count(*) from lifeemp";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$is_admin = (intval(pg_fetch_result($sel, 0, 0)) > 0);

// イントラメニュー情報を取得
$sql = "select * from intramenu";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$menu3 = pg_fetch_result($sel, 0, "menu3");
$menu3_2 = pg_fetch_result($sel, 0, "menu3_2");
?>
<title>イントラネット | <? echo($menu3); ?> | <? echo($menu3_2); ?></title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function registWelfare() {
	window.open('intra_life_welfare_register.php?session=<? echo($session); ?>', 'newwin', 'width=640,height=480,scrollbars=yes');
}

function deleteWelfare() {
	if (confirm('削除してよろしいですか？')) {
		document.mainform.submit();
	}
}

function changeDisplay(stage_id) {
	var img = document.getElementById('stage'.concat(stage_id));
	if (img.className == 'close') {
		img.className = 'open';
		row_display = '';
	} else {
		img.className = 'close';
		row_display = 'none';
	}

	var row_class = 'children_of_'.concat(stage_id);
	var rows = document.getElementsByTagName('tr');
	for (var i = 0, j = rows.length; i < j; i++) {
		if (rows[i].className == row_class) {
			rows[i].style.display = row_display;
		}
	}
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;border:#5279a5 solid 1px;}
.list td {border:#5279a5 solid 1px;}
img.close {
	background-image:url("img/icon/plus.gif");
	vertical-align:middle;
}
img.open {
	background-image:url("img/icon/minus.gif");
	vertical-align:middle;
}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"<? if ($default_stage_id != "") {echo(" onload=\"changeDisplay('$default_stage_id');\"");} ?>>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="intra_menu.php?session=<? echo($session); ?>"><img src="img/icon/b38.gif" width="32" height="32" border="0" alt="イントラネット"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="intra_menu.php?session=<? echo($session); ?>"><b>イントラネット</b></a> &gt; <a href="intra_life.php?session=<? echo($session); ?>"><b><? echo($menu3); ?></b></a> &gt; <a href="intra_life_welfare.php?session=<? echo($session); ?>"><b><? echo($menu3_2); ?></b></a></font></td>
<? if ($life_setting_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="life_master_menu.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<form name="mainform" action="intra_life_welfare_delete.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="105" align="center" bgcolor="#bdd1e7"><a href="intra_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メインメニュー</font></a></td>
<td width="5">&nbsp;</td>
<td width="<? echo(get_tab_width($menu3)); ?>" align="center" bgcolor="#bdd1e7"><a href="intra_life.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($menu3); ?></font></a></td>
<td width="5">&nbsp;</td>
<td width="<? echo(get_tab_width($menu3_2)); ?>" align="center" bgcolor="#5279a5"><a href="intra_life_welfare.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b><? echo($menu3_2); ?></b></font></a></td>
<?
if ($is_admin) {
	echo("<td align=\"right\">\n");
	echo("<input type=\"button\" value=\"登録\" onclick=\"registWelfare();\">\n");
	echo("<input type=\"button\" value=\"削除\" onclick=\"deleteWelfare();\"");
	if ($lifestage_count == 0) {echo(" disabled");}
	echo(">\n");
	echo("</td>\n");
} else {
	echo("<td>&nbsp;</td>\n");
}
?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<?
if ($is_admin && $lifestage_count == 0) {
	echo("<div><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">文書が登録されていません。</font></div>\n");
}
?>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff">
<?
if ($is_admin) {
	echo("<td width=\"5%\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">削除</font></td>\n");
}
?>
<td width="18%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ライフステージ</font></td>
<td width="22%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">制度</font></td>
<td width="31%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">概要</font></td>
<td width="8%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">詳細説明</font></td>
<td width="8%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">記入例</font></td>
<td width="8%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請書</font></td>
</tr>
<?
foreach ($arr_stage as $lifestage_id => $stage_data) {
	$stage_name = $stage_data["name"];
	$arr_welfare = $stage_data["welfare"];

	// ライスステージ行を出力
	echo("<tr>\n");
	if ($is_admin) {
		echo("<td></td>\n");
	}
	echo("<td><img id=\"stage$lifestage_id\" src=\"img/spacer.gif\" alt=\"\" width=\"20\" height=\"20\" class=\"close\" style=\"margin-right:3px;\" onclick=\"changeDisplay('$lifestage_id')\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$stage_name</font></td>\n");
	echo("<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n");
	echo("</tr>\n");

	// 文書行を出力
	foreach ($arr_welfare as $welfare_id => $welfare_data) {
		$welfare_system = $welfare_data["system"];
		$welfare_description = $welfare_data["description"];

		echo("<tr class=\"children_of_$lifestage_id\" style=\"display:none;height:26px;\">\n");
		if ($is_admin) {
			echo("<td align=\"center\"><input type=\"checkbox\" name=\"welfare_ids[]\" value=\"$welfare_id\"></td>\n");
		}
		echo("<td></td>\n");
		if ($is_admin) {
			echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"javascript:void(0);\" onclick=\"window.open('intra_life_welfare_update.php?session=$session&welfare_id=$welfare_id', 'newwin', 'width=640,height=640,scrollbars=yes');\">$welfare_system</a></font></td>\n");
		} else {
			echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$welfare_system</font></td>\n");
		}
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$welfare_description</font></td>\n");
		for ($i = 1; $i <= 3; $i++) {
			$paths = glob("intra/welfare/{$welfare_id}_{$i}.*");
			$path = (is_array($paths)) ? $paths[0] : "";

			echo("<td align=\"center\">");
			if ($path != "") {
				echo("<input type=\"button\" value=\"参照\" onclick=\"window.open('$path');\">");
			}
			echo("</td>\n");
		}
		echo("</tr>\n");
	}
}
?>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
