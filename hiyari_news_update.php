<?
require_once('Cmx.php');
require_once('Cmx/View/Smarty.php');
require_once("hiyari_common.ini");
require_once("about_authority.php");
require_once("about_session.php");
require_once("show_class_name.ini");
require_once("news_common.ini");
require_once("hiyari_header_class.php");
require_once('class/Cmx/Model/SystemConfig.php');

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 47, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

//デザイン（1=旧デザイン、2=新デザイン）
$sysConf = new Cmx_SystemConfig();
$design_mode = $sysConf->get('fantol.design.mode');

// 組織名を取得
$arr_class_name = get_class_name_array($con, $fname);
$class_called = $arr_class_name[0];

// カテゴリ一覧を取得
$category_list = get_category_list($class_called);

// 部門一覧を取得
$class_list = get_class_list($con, $fname);

// 職種一覧を取得
$job_list = get_job_list($con, $fname);

//添付ファイルのフォルダ名
$newsfile_folder_name = get_newsfile_folder_name();

//添付ファイル
$file_list = array();

if ($back == "t") {
    for ($i = 0; $i < count($filename); $i++){
        $file_list[$file_id[$i]]["name"] = $filename[$i];
        $file_list[$file_id[$i]]["ext"]  = strrchr($filename[$i], ".");
    }
}
// 初期表示時はお知らせ情報を取得
else {
	$sql = "select inci_news.*, empmst.emp_lt_nm, empmst.emp_ft_nm from inci_news inner join empmst on inci_news.emp_id = empmst.emp_id";
	$cond = "where news_id = $news_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$news_title = pg_fetch_result($sel, 0, "news_title");
	$news_category = pg_fetch_result($sel, 0, "news_category");
	$class_id = pg_fetch_result($sel, 0, "class_id");
	$job_id = pg_fetch_result($sel, 0, "job_id");
	$news = pg_fetch_result($sel, 0, "news");
	$news_begin = pg_fetch_result($sel, 0, "news_begin");
	$news_end = pg_fetch_result($sel, 0, "news_end");
	$emp_name = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");
	$news_date = pg_fetch_result($sel, 0, "news_date");
	$record_flg = pg_fetch_result($sel, 0, "record_flg");
	$marker = pg_fetch_result($sel, 0, "marker");
	
	$news_date1 = substr($news_date, 0, 4);
	$news_date2 = substr($news_date, 4, 2);
	$news_date3 = substr($news_date, 6, 2);
	$news_begin1 = substr($news_begin, 0, 4);
	$news_begin2 = substr($news_begin, 4, 2);
	$news_begin3 = substr($news_begin, 6, 2);
	$news_end1 = substr($news_end, 0, 4);
	$news_end2 = substr($news_end, 4, 2);
	$news_end3 = substr($news_end, 6, 2);

	// 添付ファイル情報を取得
	$sql = "select newsfile_no, newsfile_name from inci_newsfile";
	$cond = "where news_id = $news_id order by newsfile_no";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$tmp_file_no = $row["newsfile_no"];
		$tmp_filename = $row["newsfile_name"];

		// 一時フォルダにコピー
		$ext = strrchr($tmp_filename, ".");
		copy("{$newsfile_folder_name}/{$news_id}_{$tmp_file_no}{$ext}", "{$newsfile_folder_name}/tmp/{$session}_{$tmp_file_no}{$ext}");

        $file_list[$tmp_file_no]["name"] = $tmp_filename;
        $file_list[$tmp_file_no]["ext"]  = $ext;
	}
}

// 30分以上前に保存されたファイルを削除
create_newsfile_folder();
foreach (glob("{$newsfile_folder_name}/tmp/*.*") as $tmpfile) {
	if (time() - filemtime($tmpfile) >= 30 * 60) {
		unlink($tmpfile);
	}
}

//年の選択肢
$stt_years = get_years($news_begin1);
$end_years = get_years($news_end1);
$reg_years = get_years($news_date1);

//月の選択肢
$months = array();
for ($i = 1; $i <= 12; $i++) {
    $months[$i] = sprintf("%02d", $i);
}
//日の選択肢
$days = array();
for ($i = 1; $i <= 31; $i++) {
    $days[$i] = sprintf("%02d", $i);
}

//==============================
//表示
//==============================
$smarty = new Cmx_View();

$smarty->assign("session", $session);
$smarty->assign("mode", "update");
$smarty->assign("action_file", "hiyari_news_update_exe.php");
$smarty->assign("action_btn", "更新");

//ヘッダ用データ
$header = new hiyari_header_class($session, $fname, $con);
$smarty->assign("INCIDENT_TITLE", $header->get_inci_title());
$smarty->assign("inci_auth_name", $header->get_inci_auth_name());
$smarty->assign("PAGE_TITLE", "お知らせ詳細");
$smarty->assign("is_kanrisha", $header->is_kanrisha());
if ($design_mode == 1){
    $smarty->assign("tab_info", $header->get_news_tab_info($page,$news_id));
    $smarty->assign("is_news_menu", false);
}
else{
    $smarty->assign("tab_info", $header->get_user_tab_info());
}

//お知らせデータ
//Opera用タグ抜きしたTEXT
$news_non_tag = html_to_text_for_tinymce($news);
$news_non_tag = str_replace("\r","",$news_non_tag);
$news_non_tag = str_replace("\n","\\n",$news_non_tag);
$news_non_tag = str_replace("\"","\\\"",$news_non_tag);
$smarty->assign("news_non_tag", $news_non_tag);

$smarty->assign("news_title", h($news_title));
$smarty->assign("marker", $marker);

$smarty->assign("category_list", $category_list);
$smarty->assign("news_category", $news_category);

$smarty->assign("class_list", $class_list);
$smarty->assign("class_id", $class_id);

$smarty->assign("job_list", $job_list);
$smarty->assign("job_id", $job_id);

$smarty->assign("news", nl2br($news));

$smarty->assign("file_list", $file_list);
$smarty->assign("newsfile_folder_name", $newsfile_folder_name);

$smarty->assign("record_flg", $record_flg);

$smarty->assign("stt_years", $stt_years);
$smarty->assign("end_years", $end_years);
$smarty->assign("reg_years", $reg_years);
$smarty->assign("months", $months);
$smarty->assign("days", $days);
$smarty->assign("news_begin1", $news_begin1);
$smarty->assign("news_begin2", $news_begin2);
$smarty->assign("news_begin3", $news_begin3);
$smarty->assign("news_end1", $news_end1);
$smarty->assign("news_end2", $news_end2);
$smarty->assign("news_end3", $news_end3);
$smarty->assign("news_date1", $news_date1);
$smarty->assign("news_date2", $news_date2);
$smarty->assign("news_date3", $news_date3);

$smarty->assign("emp_name", $emp_name);

$smarty->assign("news_id", $news_id);
$smarty->assign("page", $page);

if ($design_mode == 1){
    $smarty->display("hiyari_news_edit1.tpl");
}
else{
    $smarty->display("hiyari_news_edit2.tpl");
}

//========================================================================================================================================================================================================
//内部関数
//========================================================================================================================================================================================================
/* 年の配列取得（select用）
 *
 * @param string $target_year 対象年
 */
function get_years($target_year = 0){    
    $years = array();

    $year = date("Y");
    if ($target_year == 0) {
        $from = $year;
        $to = $year + 1;
    }
    else {        
        $from = min($year, $target_year);
        $to = max($year, $target_year) + 1;
    }

    for ($i=$from; $i<=$to; $i++) {
        $years[$i] = $i;
    }

    return $years;
}