<?php
require_once("about_comedix.php");
require_once("show_select_values.ini");
require_once("date_utils.php");
require_once("timecard_common_class.php");
require_once("settings_common.php"); //20140124
require_once('Cmx.php');
require_once('Cmx/Model/SystemConfig.php');
class timecard_paid_hol_hour_class {

	var $file_name;	// 呼び出し元ファイル名
	var $_db_con;	// DBコネクション

	var $paid_hol_hour_flag;
	var $paid_hol_hour_full_flag;
	var $paid_hol_hour_part_flag;
	var $paid_hol_hour_max_flag;
	var $paid_hol_hour_max_day;
	var $paid_hol_hour_unit_time;
    var $paid_hol_hour_carry_flg;

	/*************************************************************************/
	// コンストラクタ
	// @param object $con DBコネクション
	// @param string $fname 画面名
	/*************************************************************************/
    function timecard_paid_hol_hour_class($con_param, $fname_param){
        //テンプレートから呼ばれて未設定の場合、申請画面の内容を使用
        if ($con_param == "") {
            global $con;
            global $fname;
            if ($con == "") {
                $fname=$_SERVER['PHP_SELF'];
                $con = connect2db($fname);
            }
        }
        else {
            $con = $con_param;
            $fname = $fname_param;
        }
        // 呼び出し元ファイル名を保持
		$this->file_name = $fname;
		// DBコネクションを保持
		$this->_db_con = $con;

	}


	/**
	 * 設定情報取得
	 *
	 * @return なし
	 *
	*/
	function select(){
		//1レコード取得
		$sql = "select * from timecard_paid_hol_hour_conf";
		$sel = select_from_table($this->_db_con, $sql, "", $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$row = pg_fetch_all($sel);
		if(count($row) > 0){
			$this->set_select_value($row[0]);
		}
	}

	/**
	 * 設定情報を変数へセットする
	 *
	 * @param mixed $row 設定情報レコード
	 * @return なし
	 *
	 */
	function set_select_value($row){

		//キー（カラム名取得）
		$arr_key = (array_keys($row));

		foreach ($arr_key as $key) {
			//カラム名の変数に値を保持
			$colmun_name = $key;
			$value       = $row[$key];
			$this->$colmun_name = $value;
		}

	}

	/**
	 * タイムカード入力画面等の時間有休入力項目を表示する
	 *
	 * @param mixed $paid_hol_start_hour 開始時刻の時
	 * @param mixed $paid_hol_start_min 開始時刻の分
	 * @param mixed $paid_hol_hour 有休時間の時
	 * @param mixed $paid_hol_min 有休時間の分
	 * @param mixed $emp_id 職員ID
	 * @param mixed $colspan_num 内容列のcolspan(呼出元のレイアウトが変わるため)
	 * @return なし
	 *
	 */
	function show_input_item($paid_hol_start_hour, $paid_hol_start_min, $paid_hol_hour, $paid_hol_min, $emp_id, $colspan_num="1"){

		if ($this->paid_hol_hour_flag == "t") {
			$duty_form = $this->get_duty_form($emp_id);
			if (($duty_form != "2" && $this->paid_hol_hour_full_flag == "t") ||
					($duty_form == "2" && $this->paid_hol_hour_part_flag == "t")) {
			?>
			<tr height="22">
			<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時間有休</font></td>
			<td colspan="<?php echo $colspan_num; ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="paid_hol_start_hour" id="paid_hol_start_hour"><? show_hour_options_0_23($paid_hol_start_hour); ?></select>：<select name="paid_hol_start_min" id="paid_hol_start_min">
			<? show_min_options_00_59($paid_hol_start_min); ?></select> 〜
			<select name="paid_hol_hour" id="paid_hol_hour">

			<option value="--"></option>
			<?
			for ($i=0; $i<=7; $i++) {
				echo("<option value=\"{$i}\"");
				if ("$paid_hol_hour" === "$i") {
					echo(" selected");
				}
				echo(">{$i}</option>\n");

			}
			?>
			</select>時間
			<select name="paid_hol_min" id="paid_hol_min">

			<option value="--"></option>
			<option value="00"<? if ($paid_hol_min == "00") {echo(" selected");} ?>>00</option>
			<?
            if ($this->paid_hol_hour_unit_time == "1") {
                    for ($idx=1; $idx<60; $idx++) {
                        $selected = ($paid_hol_min == $idx) ? " selected" : " ";
                        $str = sprintf("%02d", $idx);
                        echo("<option value=\"$str\" $selected>$str</option>\n");
                    }

            }
			if ($this->paid_hol_hour_unit_time == "15") {
				echo("<option value=\"15\"");
				if ($paid_hol_min == "15") {
					echo(" selected");
				}
				echo(">15</option>\n");
			}
			if ($this->paid_hol_hour_unit_time == "15" ||
					$this->paid_hol_hour_unit_time == "30") {
				echo("<option value=\"30\"");
				if ($paid_hol_min == "30") {
					echo(" selected");
				}
				echo(">30</option>\n");
			}
			if ($this->paid_hol_hour_unit_time == "15") {
				echo("<option value=\"45\"");
				if ($paid_hol_min == "45") {
					echo(" selected");
				}
				echo(">45</option>\n");
			}
			?>

			</select>分

			</font>
			</td>
			</tr>


			<?
			}
		}

	}

	/**
	 * 有休情報をDBへ更新する
	 *
	 * @param mixed $emp_id 職員ID
	 * @param mixed $date 日付
	 * @param mixed $paid_hol_start_hour 開始時刻の時
	 * @param mixed $paid_hol_start_min 開始時刻の分
	 * @param mixed $paid_hol_hour 有休時間の時
	 * @param mixed $paid_hol_min 有休時間の分
	 * @return なし
	 *
	 */
	function update_paid_hol_hour($emp_id, $date, $paid_hol_start_hour, $paid_hol_start_min, $paid_hol_hour, $paid_hol_min){

		if ($paid_hol_start_hour == "") {
			return;
		}
		if ($this->paid_hol_hour_flag == "t") {

			$duty_form = $this->get_duty_form($emp_id);
			if (($duty_form != "2" && $this->paid_hol_hour_full_flag == "t") ||
					($duty_form == "2" && $this->paid_hol_hour_part_flag == "t")) {
				$calc_minute = intval($paid_hol_hour) * 60 + intval($paid_hol_min);
				$arr_hol_start_hour = $this->get_paid_hol_hour($emp_id, $date);

				if (count($arr_hol_start_hour) > 0) {
					if ($paid_hol_start_hour == "--") {
						//delete
						$sql = "delete from timecard_paid_hol_hour";
						$cond = "WHERE emp_id = '$emp_id' and start_date = '$date'";
						$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
						if ($del == 0) {
							pg_query($this->_db_con, "rollback");
							pg_close($this->_db_con);
							echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
							echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
							exit;
						}
					}
					else {
						//update
						$sql = "update timecard_paid_hol_hour set";
						$set = array("start_time", "use_hour", "use_minute", "calc_minute");
						$setvalue = array($paid_hol_start_hour.$paid_hol_start_min, $paid_hol_hour, $paid_hol_min, $calc_minute);
						$cond = "WHERE emp_id = '$emp_id' and start_date = '$date'";

						$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
						if ($upd == 0) {
							pg_query($this->_db_con, "rollback");
							pg_close($this->_db_con);
							echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
							echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
							exit;
						}
					}
				}
				else {
					if ($paid_hol_start_hour != "--") {
						//insert
						$sql = "insert into timecard_paid_hol_hour (emp_id, start_date, start_time, use_hour, use_minute, calc_minute ";
						$sql .= ") values (";

						$content = array($emp_id, $date, $paid_hol_start_hour.$paid_hol_start_min, $paid_hol_hour, $paid_hol_min, $calc_minute);
						$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
						if ($ins == 0) {
							pg_query($this->_db_con, "rollback");
							pg_close($this->_db_con);
							echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
							echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
							exit;
						}
					}
				}
			}
		}
	}

	/**
	 * 有休情報を削除する
	 *
	 * @param mixed $emp_id 職員ID
	 * @param mixed $date 日付
	 * @return なし
	 *
	 */
	function delete_paid_hol_hour($emp_id, $date){

		if ($this->paid_hol_hour_flag == "t") {

			$duty_form = $this->get_duty_form($emp_id);
			if (($duty_form != "2" && $this->paid_hol_hour_full_flag == "t") ||
					($duty_form == "2" && $this->paid_hol_hour_part_flag == "t")) {
				$arr_hol_start_hour = $this->get_paid_hol_hour($emp_id, $date);

				if (count($arr_hol_start_hour) > 0) {
					//delete
					$sql = "delete from timecard_paid_hol_hour";
					$cond = "WHERE emp_id = '$emp_id' and start_date = '$date'";
					$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
					if ($del == 0) {
						pg_query($this->_db_con, "rollback");
						pg_close($this->_db_con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}
				}

			}
		}
	}


	/**
	 * 指定日の時間有休データを取得する
	 *
	 * @param mixed $emp_id 職員ID
	 * @param mixed $date 指定日
	 * @return mixed 指定日の時間有休データ
	 *
	 */
	function get_paid_hol_hour($emp_id, $date) {

		$sql = "select * from timecard_paid_hol_hour";
		$cond = "where emp_id = '$emp_id' and start_date = '$date'";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$ret_dat = array();
		if (pg_num_rows($sel) > 0) {
			$row = pg_fetch_all($sel);
			$ret_dat = $row[0];
		}

		return $ret_dat;
	}

	/**
	 * 指定期間の時間有休データを取得する
	 *
	 * @param mixed $emp_id 職員ID
	 * @param mixed $start_date 開始日
	 * @param mixed $end_date 終了日
	 * @return mixed 期間分の時間有休データ。keyが日付の配列
	 *
	 */
	function get_paid_hol_hour_month($emp_id, $start_date, $end_date) {

		$sql = "select * from timecard_paid_hol_hour";
		$cond = "where emp_id = '$emp_id' and start_date >= '$start_date' and start_date <= '$end_date'";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$ret_dat = array();
		while ($row = pg_fetch_array($sel)) {
			$wk_date = $row["start_date"];
			$ret_dat["$wk_date"] = $row;
		}

		return $ret_dat;
	}

	/**
	 * 指定職員、日付の時間有休データを取得する
	 *
	 * @param mixed $emp_id 職員ID
	 * @param mixed $date 対象日
	 * @return mixed 時間有休データ。keyがemp_idの配列
	 *
	 */
	function get_paid_hol_hour_emp($emp_ids, $date) {

		$ret_dat = array();
		if (count($emp_ids) == 0) {
			return $ret_dat;
		}

		$sql = "select * from timecard_paid_hol_hour";
		$cond_staff = "";
		for ($i=0; $i<count($emp_ids); $i++) {
			if ($i > 0) {
				$cond_staff .= " or ";
			}
			$cond_staff .= " emp_id = '". $emp_ids[$i] . "' ";
		}
		$cond = "where ($cond_staff) and start_date = '$date'"; //SQL不具合 20120411
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		while ($row = pg_fetch_array($sel)) {
			$wk_emp_id = $row["emp_id"];
			$ret_dat["$wk_emp_id"] = $row;
		}

		return $ret_dat;
	}
//時間有休追加 start 20120213
	/**
	 * 指定職員、指定期間の時間有休データを取得する
	 *
	 * @param mixed $emp_id 職員ID
	 * @param mixed $start_date 開始日
	 * @param mixed $end_date 終了日
	 * @return mixed 時間有休データ。keyがemp_idの配列
	 *
	 */
	function get_paid_hol_hour_emp_month($emp_ids, $start_date, $end_date) {

		$ret_dat = array();
		if (count($emp_ids) == 0) {
			return $ret_dat;
		}

		$sql = "select * from timecard_paid_hol_hour";
		$cond_staff = "";
		for ($i=0; $i<count($emp_ids); $i++) {
			if ($i > 0) {
				$cond_staff .= " or ";
			}
			$cond_staff .= " emp_id = '". $emp_ids[$i] . "' ";
		}
		$cond = "where ($cond_staff) and start_date >= '$start_date' and start_date <= '$end_date'";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		while ($row = pg_fetch_array($sel)) {
			$wk_emp_id = $row["emp_id"];
			$wk_date = $row["start_date"];
			$ret_dat["$wk_emp_id"]["$wk_date"] = $row;
		}

		return $ret_dat;
	}
//時間有休追加 end 20120213

	/**
	 * 時間有休数取得
	 *
	 * @param mixed $emp_id 職員ID
	 * @param mixed $start_date 開始日
	 * @param mixed $end_date 終了日
	 * @param mixed $today 当日、指定時はその日の分を除外する
	 * @return mixed 時間有休数（分）
	 *
	 */
	function get_paid_hol_day($emp_id, $start_date, $end_date, $today) {

        $sql = "select sum(to_number(use_hour, '99') * 60 + to_number(use_minute, '99')) as total_minute from timecard_paid_hol_hour";
		$cond = "where emp_id = '$emp_id' and start_date >= '$start_date' and start_date <= '$end_date'";
		if ($today != "") {
			$cond .= " and start_date != '$today' ";
		}
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$total_minute = 0;
		if (pg_num_rows($sel) > 0) {
			$total_minute = pg_fetch_result($sel, 0, "total_minute");
		}
		return $total_minute;
	}

	/**
	 * 年休付与開始日取得
	 *
	 * @param mixed $year 年
	 * @param mixed $month 月
	 * @param mixed $emp_id 職員ID
	 * @param mixed $closing_paid_holiday 有休締め日
	 * @param mixed $criteria_months 基準月
	 * @param mixed $start_date 開始日
	 * @param mixed $end_date 終了日
	 * @return mixed 年休付与開始日YYYYMMDD形式
	 *
	 */
	function get_paid_hol_start_date($year, $month, $emp_id, $closing_paid_holiday, $criteria_months, $start_date, $end_date) {

		$wk_month = ($criteria_months == "1") ? "3" : "6";

		$sql = "select empmst.emp_id, emp_lt_nm, emp_ft_nm, emppaid.days1, emppaid.days2, emppaid.adjust_day ";
		$sql .= " , g.days1 as prev_days1 ,g.days2 as prev_days2, g.adjust_day as prev_adjust_day, emp_join, to_char(to_timestamp(case when emppaid.paid_hol_start_date is not null and  emppaid.paid_hol_start_date != '' then emppaid.paid_hol_start_date when emppaid.paid_hol_emp_join is not null and emppaid.paid_hol_emp_join != '' then emppaid.paid_hol_emp_join when f.paid_hol_start_date != '' then f.paid_hol_start_date else emp_join end, 'yyyymmdd')  + '$wk_month months', 'yyyymmdd') as paid_hol_add_date, to_char(to_timestamp(case when emppaid.paid_hol_start_date is not null and  emppaid.paid_hol_start_date != '' then emppaid.paid_hol_start_date when emppaid.paid_hol_emp_join is not null and emppaid.paid_hol_emp_join != '' then emppaid.paid_hol_emp_join when f.paid_hol_start_date != '' then f.paid_hol_start_date else emp_join end, 'yyyymmdd')  + '12 months', 'yyyymmdd') as year_after_join, h.curr_use, h.data_migration_date, h.last_remain, h.curr_carry, h.curr_add, h.last_add_date, emppaid.year as emppaid_year, emppaid.paid_hol_add_mmdd ";
		$sql .= " from empmst ";
		//年範囲調整
		if ($closing_paid_holiday != "3") { // 1:4/1, 2:1/1
			$sql .= " left join emppaid on emppaid.emp_id = empmst.emp_id and emppaid.year = '$year' ";
		}
		//3:採用日
		else {
			$wk_yr = substr($start_date, 0, 4);
			$start_ymd = ($wk_yr-1).substr($start_date, 4, 4);
			$end_ymd = $end_date;
			//2件有り得るので1件に絞り込む
			$sql .= " left join (select * from emppaid where emppaid.emp_id = '$emp_id' and  ";
			$sql .= " CAST(emppaid.year AS varchar)||emppaid.paid_hol_add_mmdd >= '$start_ymd' and ";
			$sql .= " CAST(emppaid.year AS varchar)||emppaid.paid_hol_add_mmdd <= '$end_ymd' ";
			$sql .= " order by emppaid.year desc, emppaid.paid_hol_add_mmdd desc limit 1) emppaid on emppaid.emp_id = empmst.emp_id ";
		}
		$sql .= " left join empcond f on f.emp_id = empmst.emp_id ";
		if ($closing_paid_holiday != "3") { // 1:4/1, 2:1/1
            $wk_year = $year - 1;
            $sql .= " left join emppaid g on g.emp_id = empmst.emp_id and g.year = '$wk_year' ";
		}
		//3:採用日
		else {
			$wk_yr = substr($start_date, 0, 4);
			$start_ymd = ($wk_yr-2).substr($start_date, 4, 4);
			$wk_yr = substr($end_date, 0, 4);
			$end_ymd = ($wk_yr-1).substr($end_date, 4, 4);
			//2件有り得るので1件に絞り込む
			$sql .= " left join (select * from emppaid where emppaid.emp_id = '$emp_id' and  ";
			$sql .= " CAST(emppaid.year AS varchar)||emppaid.paid_hol_add_mmdd >= '$start_ymd' and ";
			$sql .= " CAST(emppaid.year AS varchar)||emppaid.paid_hol_add_mmdd <= '$end_ymd' ";
			$sql .= " order by emppaid.year desc, emppaid.paid_hol_add_mmdd desc limit 1) g on g.emp_id = empmst.emp_id ";
		}
		$sql .= " left join timecard_paid_hol_import h on h.emp_personal_id = empmst.emp_personal_id ";
		$cond = "where empmst.emp_id = '$emp_id'";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$tmp_days3 = 0;
		$num = pg_numrows($sel);
		if ($num>0) {
			$emppaid_year = pg_fetch_result($sel, 0, "emppaid_year");//直近付与データの年
			$paid_hol_add_mmdd = pg_fetch_result($sel, 0, "paid_hol_add_mmdd");//直近付与データの月日
			$tmp_days1 = pg_fetch_result($sel, 0, "days1"); //繰越
			$tmp_days2 = pg_fetch_result($sel, 0, "days2");
			$tmp_adjust_day = pg_fetch_result($sel, 0, "adjust_day");
			$tmp_emp_join = pg_fetch_result($sel, 0, "emp_join");
			$tmp_prev_days1 = pg_fetch_result($sel, 0, "prev_days1"); //前年分
			$tmp_prev_days2 = pg_fetch_result($sel, 0, "prev_days2");
			$tmp_prev_adjust_day = pg_fetch_result($sel, 0, "prev_adjust_day");
			//移行データ
			$data_migration_date = pg_fetch_result($sel,0,"data_migration_date");
			$curr_use = pg_fetch_result($sel,0,"curr_use");
			$last_remain = pg_fetch_result($sel,0,"last_remain");
			$curr_carry = pg_fetch_result($sel,0,"curr_carry");
			$curr_add = pg_fetch_result($sel,0,"curr_add");
			$last_add_date = pg_fetch_result($sel,0,"last_add_date");
			//移行データの前回付与日
			$import_last_add_date = "";
			if ($last_add_date != "") {
				$wk_ymd = split("/", $last_add_date);
				$import_last_add_date = $wk_ymd[0].sprintf("%02d%02d", $wk_ymd[1], $wk_ymd[2]);
			}

		}
		//付与開始日
		//付与データが有る場合、年休使用数を求める開始日に設定
		if ($emppaid_year != "" && $paid_hol_add_mmdd != "") {
			$fuyo_start_date = $emppaid_year.$paid_hol_add_mmdd;
			$chk_ymd1 = $fuyo_start_date;
		}
		else {
			switch ($closing_paid_holiday) {
				case "1":
					//年度調整
					$wk_year = ($month < "04") ? $year - 1 : $year;
					$fuyo_start_date = $wk_year."0401";
					$start_mm = 4;
					break;
				case "2":
					$fuyo_start_date = $year."0101";
					$wk_year = $year;
					$start_mm = 1;
					break;
				case "3":

					//6ヶ月基準
					if ($criteria_months == "2" ) {
						$paid_hol_add_date = pg_fetch_result($sel, 0, "paid_hol_add_date");
					}
					//3ヶ月基準
					else {
						$today = date("Ymd");
						$year_after_join = pg_fetch_result($sel, 0, "year_after_join");
						//入職後1年以上か確認
						if (substr($year_after_join, 0, 4) <= $year) {
							$paid_hol_add_date = $year_after_join;
						}
						else {
							$paid_hol_add_date = pg_fetch_result($sel, 0, "paid_hol_add_date");
						}
					}

					$wk_mon = substr($paid_hol_add_date, 4, 2);
					$start_mm = intval($wk_mon);
					//年度調整 締め日 付与日　例1001 0401 1001
					$wk_fuyo_mmdd = substr($paid_hol_add_date, 4, 4);
					$wk_start_mmdd = substr($start_date, 4, 4);
					$wk_end_mmdd = substr($end_date, 4, 4);
					$wk_year = ($wk_end_mmdd < $wk_fuyo_mmdd) ? $year - 1 : $year;
					$fuyo_start_date = $wk_year.substr($paid_hol_add_date, 4, 4); //年度
					break;
			}
			$chk_ymd1 = $wk_year.sprintf("%02d",$start_mm)."01";
		}

		//処理年月範囲確認
		//$chk_ymd1_timestamp = date_utils::to_timestamp_from_ymd($chk_ymd1);
		//$chk_ymd2 =  date("Ymd", strtotime("+12 month", $chk_ymd1_timestamp));

		return $chk_ymd1;
	}


	/**
	 * 職員別の所定労働時間(勤務条件)を返す。未設定の場合はカレンダーの所定労働時間。分単位
	 *
	 * @param mixed $emp_id 職員ID
	 * @param mixed $day1_time カレンダーの所定労働時間
	 * @return mixed 所定労働時間。分単位
	 *
	 */
	function get_specified_time($emp_id, $day1_time){
		// 勤務条件テーブルより所定労働時間を取得
		$sql = "select specified_time from empcond";
		$cond = "where emp_id = '$emp_id'";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		if (pg_num_rows($sel) > 0) {
			$paid_specified_time = pg_fetch_result($sel, 0, "specified_time");
		}

        if ($paid_specified_time == "") {
            //settings.phpにある場合はそれを使用 20140124
            $paid_specified_time = get_settings_value("paid_hol_spec_time", "");
            //ない場合はカレンダーの所定を使用
            if ($paid_specified_time == "") {
                $paid_specified_time = $day1_time;
            }
        }
		//hhmm形式から時分にする
		$wk_hh = substr($paid_specified_time, 0, 2);
		$wk_mm = substr($paid_specified_time, 2, 2);
//		//単位が1時間以上で、所定労働時間の分に1時間未満がある場合、切り上げ、古い仕様
//		if ($this->paid_hol_hour_unit_time >= "60" && $wk_mm != "00") {
        //上限日数がある場合に、切り上げとする 20120515
        if ($this->paid_hol_hour_max_flag == "t" && $wk_mm != "00") {
			//切り上げ
			$specified_time = (intval($wk_hh) + 1) * 60;
		}
		else {
			$specified_time = intval($wk_hh) * 60 + intval($wk_mm);
		}

		return $specified_time;
	}

    /**
     * 所定労働時間を条件により切り上げ、未設定の場合はカレンダーの所定労働時間。分単位
     * 繰り返し処理するために引数に時間を指定
     *
     * @param mixed $$empcond_specified_time 勤務条件テーブルの所定労働時間
     * @param mixed $day1_time カレンダーの所定労働時間
     * @return mixed 所定労働時間。分単位
     *
     */
    function get_specified_time2($empcond_specified_time, $day1_time){

        if ($empcond_specified_time == "") {
            //settings.phpにある場合はそれを使用 20140124
            $paid_specified_time = get_settings_value("paid_hol_spec_time", "");
            //ない場合はカレンダーの所定を使用
            if ($paid_specified_time == "") {
                $paid_specified_time = $day1_time;
            }
        }
        else {
            $paid_specified_time = $empcond_specified_time;
        }

        //hhmm形式から時分にする
        $wk_hh = substr($paid_specified_time, 0, 2);
        $wk_mm = substr($paid_specified_time, 2, 2);
        //上限日数がある場合に、切り上げとする
        if ($this->paid_hol_hour_max_flag == "t" && $wk_mm != "00") {
            //切り上げ
            $specified_time = (intval($wk_hh) + 1) * 60;
        }
        else {
            $specified_time = intval($wk_hh) * 60 + intval($wk_mm);
        }

        return $specified_time;
    }

    /**
	 * カレンダーの所定労働時間を取得する
	 *
	 * @return mixed $day1_time カレンダーの所定労働時間
	 *
	 */
	function get_specified_time_calendar() {
		// 勤務条件テーブルより所定労働時間を取得
		$sql = "select day1_time from calendarname";
		$cond = "";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		if (pg_num_rows($sel) > 0) {
			$day1_time = pg_fetch_result($sel, 0, "day1_time");
		}

		return $day1_time;
	}
	/**
	 * 複数の職員の所定労働時間(勤務条件)を返す。未設定の場合はカレンダーの所定労働時間。分単位
	 *
	 * @param mixed $emp_ids 職員IDの配列
	 * @param mixed $day1_time カレンダーの所定労働時間
	 * @return mixed 所定労働時間の配列、職員IDをキーとする。分単位
	 *
	 */
	function get_specified_time_emp($emp_ids, $day1_time){

		if (count($emp_ids) == 0) {
			return array();
		}
        //settings.phpにある場合はそれを使用 20140124
        $wk_paid_hol_spec_time = get_settings_value("paid_hol_spec_time", "");
        // 勤務条件テーブルより所定労働時間を取得
		$sql = "select emp_id, specified_time from empcond";
		$cond_staff = "";
		for ($i=0; $i<count($emp_ids); $i++) {
			if ($i > 0) {
				$cond_staff .= " or ";
			}
			$cond_staff .= " emp_id = '". $emp_ids[$i] . "' ";
		}
		$cond = "where $cond_staff ";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$num = pg_num_rows($sel);
		$arr_specified_time = array();

		for ($i=0; $i<$num; $i++) {
			$wk_emp_id = pg_fetch_result($sel, $i, "emp_id");
			$paid_specified_time = pg_fetch_result($sel, $i, "specified_time");


			if ($paid_specified_time == "") {
				if ($wk_paid_hol_spec_time != "") { //20140124
                    $paid_specified_time = $wk_paid_hol_spec_time;
                }
                else {
                    $paid_specified_time = $day1_time;
                }
            }
			//hhmm形式から時分にする
			$wk_hh = substr($paid_specified_time, 0, 2);
			$wk_mm = substr($paid_specified_time, 2, 2);
			//単位が1時間以上で、所定労働時間の分に1時間未満がある場合、切り上げ
			//if ($this->paid_hol_hour_unit_time >= "60" && $wk_mm != "00") {
            //上限日数がある場合に、切り上げとする 20120515
            if ($this->paid_hol_hour_max_flag == "t" && $wk_mm != "00") {
				//切り上げ
				$specified_time = (intval($wk_hh) + 1) * 60;
			}
			else {
				$specified_time = intval($wk_hh) * 60 + intval($wk_mm);
			}
			$arr_specified_time["$wk_emp_id"] = $specified_time;
		}
		return $arr_specified_time;
	}

	/**
	 * 時間有休限度チェック
	 *
	 * @param mixed $year 年
	 * @param mixed $month 月
	 * @param mixed $emp_id 職員ID
	 * @param mixed $closing_paid_holiday 有休締め日
	 * @param mixed $criteria_months 基準月
	 * @param mixed $date 対象日
	 * @param mixed $paid_hol_hour 時間
	 * @param mixed $paid_hol_min 分
	 * @param mixed $day1_time 所定労働時間（カレンダー）
	 * @return mixed エラー情報 ["err_flg"]:0or1, ["err_msg"]:err_flg=1の場合に、alert用のメッセージを設定
	 *
	 */
	function paid_hol_hour_max_check($year, $month, $emp_id, $closing_paid_holiday, $criteria_months, $date, $paid_hol_hour, $paid_hol_min, $day1_time) {

		$arr_ret = array();
		$arr_ret["err_flg"] = "0";
		$arr_ret["err_msg"] = "";

		//有休付与開始日取得
		$paid_hol_start_date = $this->get_paid_hol_start_date($year, $month, $emp_id, $closing_paid_holiday, $criteria_months, $date, $date);
		//限度数との確認
		$wk_end =  date("Ymd", strtotime("+12 month", date_utils::to_timestamp_from_ymd($paid_hol_start_date)));
		$paid_hol_end_date = date("Ymd", strtotime("-1 day", date_utils::to_timestamp_from_ymd($wk_end)));
		$year_use_minute = $this->get_paid_hol_day($emp_id, $paid_hol_start_date, $paid_hol_end_date, $date);

		$wk_today_minute = $paid_hol_hour * 60 + $paid_hol_min;
		$chk_minute = $year_use_minute + $wk_today_minute; //取得時間
		//１日分の所定労働時間(分)
		$specified_time_per_day = $this->get_specified_time($emp_id, $day1_time);
		if ($chk_minute > $this->paid_hol_hour_max_day * $specified_time_per_day) {
			//所定労働時間（メッセージ用）
			$wk_hh = intval($specified_time_per_day / 60);
			$msg_time1 = "{$wk_hh}時間";
			$wk_mm = $specified_time_per_day - ($wk_hh * 60);
			if ($wk_mm > 0) {
				$msg_time1 .= "{$wk_mm}分";
			}
			//取得数合計（メッセージ用）
			$wk_day = intval($chk_minute / $specified_time_per_day);
			$wk_hh = intval(($chk_minute - ($wk_day * $specified_time_per_day)) / 60);
			$wk_mm = $chk_minute - $wk_day * $specified_time_per_day - $wk_hh * 60;
			$msg_time2 = "{$wk_day}日{$wk_hh}時間{$wk_mm}分";
			$arr_ret["err_flg"] = "1";
			$arr_ret["err_msg"] = "時間有休の取得時間の合計（{$msg_time2}）が年間使用上限（{$this->paid_hol_hour_max_day}日×{$msg_time1}）を超える指定はできません。";

		}

		return $arr_ret;
	}

	/**
	 * 時間有休数取得
	 *
	 * @param mixed $year 年
	 * @param mixed $month 月
	 * @param mixed $emp_id 職員ID
	 * @param mixed $closing_paid_holiday 有休締め日
	 * @param mixed $criteria_months 基準月
	 * @param mixed $start_date 開始日
	 * @param mixed $end_date 終了日
	 * @param mixed $specified_time_per_day 所定労働時間
	 * @param mixed $flg 1:日数 2:分
	 * @return mixed $flg=1の時:日数 or $flg=2の時:分
	 *
	 */
	function get_paid_hol_hour_total($year, $month, $emp_id, $closing_paid_holiday, $criteria_months, $start_date, $end_date, $specified_time_per_day, $flg) {

		$ret = 0;

		//有休付与開始日取得
		$paid_hol_start_date = $this->get_paid_hol_start_date($year, $month, $emp_id, $closing_paid_holiday, $criteria_months, $start_date, $end_date);

        $paid_hol_end_date = date("Ymd", strtotime("+1 year", to_timestamp($paid_hol_start_date)));
        $paid_hol_end_date = date("Ymd", strtotime("-1 day", to_timestamp($paid_hol_end_date)));

        //時間有休移行データ 20120515
        $arr_paid_hol_hour_import = $this->get_paid_hol_hour_import($emp_id, $paid_hol_start_date, $paid_hol_end_date);
        //移行データあり
        if ($arr_paid_hol_hour_import["data_migration_date"] != "") {
 //取得数＝当年取得数＋移行日後の使用数
//（移行日の翌日からとする）
            $year_use_minute = $this->hh_or_hhmm_to_minute($arr_paid_hol_hour_import["curr_use"]); //20141014 HH:MM対応
            $tmp_data_migration_date = date("Ymd", strtotime("+1 day", to_timestamp($arr_paid_hol_hour_import["data_migration_date"])));
            $tmp_minute = $this->get_paid_hol_day($emp_id, $tmp_data_migration_date, $end_date, "");
            $year_use_minute += $tmp_minute;
        }
        else {
            //年間の有休取得合計
            $year_use_minute = $this->get_paid_hol_day($emp_id, $paid_hol_start_date, $end_date, "");
        }

		//日数
		if ($flg == 1) {
            $ret = $year_use_minute / $specified_time_per_day;
		}
		//分
		else {
			$ret = $year_use_minute;
		}

		return $ret;
	}


	/**
	 * 年休残を編集する。
	 *
	 * @param mixed $day 日数の文字列
	 * @return mixed 小数点以下２位までを編集しゼロがある場合は消した文字列
	 *
	 */
	function edit_nenkyu_zan($day) {

		if ($day == 0 || $day == null) {
			return "0";
		}

		$wk = (int)(($day * 100));
		$wk /= 100;
		$wk_day = sprintf("%0.2f", $wk);
		$wk_day = $this->rtrim_zero($wk_day);

		return $wk_day;
	}

	/**
	 * 小数点以下の後ろの0を除く、全部除かれる場合は.ドットも除く
	 *
	 * @param mixed $str 数字文字列
	 * @return mixed 後ろの0を除いた文字列
	 *
	 */
	function rtrim_zero($str) {

		$arr_str = split("\.", $str);
		if (count($arr_str) != 2) {
			return($str);
		}

		$tmp_str = $arr_str[0];
		$dec_str = rtrim($arr_str[1], "0\.");
		if ($dec_str != "") {
			$tmp_str .= ".".$dec_str;
		}
		return $tmp_str;
	}

	/**
	 * 職員別の雇用・勤務形態を取得する
	 *
	 * @param mixed $emp_id 職員ID
	 * @return mixed 雇用・勤務形態
	 *
	 */
	function get_duty_form($emp_id){
		// 勤務条件テーブルより雇用・勤務形態を取得
		$sql = "select duty_form from empcond";
		$cond = "where emp_id = '$emp_id'";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$duty_form = "";
		if (pg_num_rows($sel) > 0) {
			$duty_form = pg_fetch_result($sel, 0, "duty_form");
		}

		return $duty_form;
	}

    /**
     * 時間有休移行データ取得 20120515
     *
     * @param mixed $emp_id 職員ID
     * @param mixed $start_date 開始日
     * @param mixed $end_date 終了日
     * @return mixed 移行データ
     *
     */
    function get_paid_hol_hour_import($emp_id, $start_date, $end_date) {

        $sql = "select a.* from timecard_paid_hol_hour_import a "
            ." left join empmst b on b.emp_personal_id = a.emp_personal_id ";
        $cond = "where b.emp_id = '$emp_id' and a.data_migration_date >= '$start_date' and a.data_migration_date <= '$end_date'";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type='text/javascript' src='./js/showpage.js'></script>");
            echo("<script language='javascript'>showErrorPage(window);</script>");
            exit;
        }
        $arr_data = array();
        if (pg_num_rows($sel) > 0) {
            $row = pg_fetch_all($sel);
            $arr_data = $row[0];
        }
        return $arr_data;
    }

    /**
     * 時間有休繰越数取得 20120515
     *
     * @param mixed $emp_id 職員ID
     * @param mixed $paid_hol_start_date 付与日
     * @return mixed 時間有休繰越数
     *
     */
    function get_paid_hol_hour_carry($emp_id, $paid_hol_start_date) {

        $start_year = substr($paid_hol_start_date, 0, 4);
        $start_mmdd = substr($paid_hol_start_date, 4, 4);

        $sql = "select carry_time_minute from emppaid ";
        $cond = "where emp_id = '$emp_id' and year = '$start_year' and paid_hol_add_mmdd = '$start_mmdd'";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type='text/javascript' src='./js/showpage.js'></script>");
            echo("<script language='javascript'>showErrorPage(window);</script>");
            exit;
        }
        $ret = "";
        if (pg_num_rows($sel) > 0) {
            $ret = pg_fetch_result($sel, 0, "carry_time_minute");
        }
        return $ret;
    }

    /**
     * 年休残文字列取得 20120522
     *
	 * @param mixed $year 年
	 * @param mixed $month 月
	 * @param mixed $emp_id 職員ID
	 * @param mixed $closing_paid_holiday 有休締め日
	 * @param mixed $criteria_months 基準月
	 * @param mixed $start_date 開始日
	 * @param mixed $end_date 終了日
	 * @param mixed $specified_time_per_day 所定労働時間
     * @param mixed $remain_paid_hol 有休残（日数）
     * @param mixed $duty_form 雇用・勤務形態
     * @param mixed $output_flg 出力種別フラグ "":html ｎ日(ｎ時間) 1:pdf ｎ(ｎ) "2":pxdoc ｎ日ｎ時間 "3":pdf ｎ(hh:mm)形式
     * @return mixed ｎ日(ｎ時間)
     *
     */
    function get_nenkyu_zan_str($year, $month, $emp_id, $closing_paid_holiday, $criteria_months, $start_date, $end_date, $specified_time_per_day, $remain_paid_hol, $duty_form, $output_flg="", $timecard_common_class) {

        $ret_str = $remain_paid_hol;
        if (($duty_form != "2" && $this->paid_hol_hour_full_flag == "f") ||
                ($duty_form == "2" && $this->paid_hol_hour_part_flag == "f")) {
            $ret_str .= "日";
            return $ret_str;
        }
        //時間有休合計取得
        $paid_hol_hour_total = $this->get_paid_hol_hour_total($year, $month, $emp_id, $closing_paid_holiday, $criteria_months, $start_date, $end_date, $specified_time_per_day, 2);
        //有休付与開始日取得
        $paid_hol_start_date = $this->get_paid_hol_start_date($year, $month, $emp_id, $closing_paid_holiday, $criteria_months, $start_date, $end_date);
        $tmp_carry_time_minute = "";
        if ($paid_hol_start_date != "") {
            //時間有休繰越数
            $tmp_carry_time_minute = $this->get_paid_hol_hour_carry($emp_id, $paid_hol_start_date);
        }
        if ($tmp_carry_time_minute == "") {
            //時間有休移行データ 20120515
            $arr_paid_hol_hour_import = $this->get_paid_hol_hour_import($emp_id, $paid_hol_start_date, $end_date);
            //移行データあり
            if ($arr_paid_hol_hour_import["data_migration_date"] != "") {
                $tmp_carry_time_minute = $this->hh_or_hhmm_to_minute($arr_paid_hol_hour_import["curr_carry"] ); //20141014 HH:MM対応
            }
        }

        //付与データ
        $wk_date  = date("Ymd", strtotime("+1 day", date_utils::to_timestamp_from_ymd($paid_hol_start_date)));
        $arr_emppaid = $this->get_last_emppaid_info($emp_id, $wk_date);
        $tmp_days1 = $arr_emppaid["days1"];
        $tmp_days2 = $arr_emppaid["days2"];
        $tmp_adjust_day = $arr_emppaid["adjust_day"];
        //移行データがある場合は、引数の$remain_paid_holを入れた$ret_strが計算済みの値のため、それを使用する
        //有休移行データの確認
        $arr_paid_hol_import = $this->get_paid_hol_import($emp_id, $paid_hol_start_date, $end_date);
        $ikougo_siyo = 0;
        //有休移行データがない場合 20121108
        if (count($arr_paid_hol_import) == 0) {
            //日数換算
            if ($tmp_carry_time_minute > $specified_time_per_day) {
                $wk_day = intval($tmp_carry_time_minute / $specified_time_per_day);
                //繰越数
                $tmp_days1 += $wk_day;
                $tmp_carry_time_minute -= ($specified_time_per_day * $wk_day);

            }
            //繰越方法
            if ($tmp_carry_time_minute > 0) {
                switch ($this->paid_hol_hour_carry_flg) {
                    case "2": //そのまま繰越
                        break;
                    case "3": //切捨て
                        $tmp_carry_time_minute = 0;
                        break;
                    case "4": //半日切上げ
                        $half_time = $specified_time_per_day / 2;
                        //半日時間（分）
                        if ($tmp_carry_time_minute > $half_time) {
                            $tmp_days1 += 1;
                            $tmp_carry_time_minute = 0;
                        }
                        else if ($tmp_carry_time_minute > 0) {
                            $tmp_days1 += 0.5;
                            $tmp_carry_time_minute = 0;
                        }
                        break;
                    case "1": //切上げ
                    default:
                        //$ret_str += 1;
                        $tmp_days1 += 1;
                        $tmp_carry_time_minute = 0;
                        break;

                }
            }
            //前々年考慮、前々年分の繰越はできない
            //前年の付与日数取得
            $arr_last_emppaid = $this->get_last_emppaid_info($emp_id, $paid_hol_start_date);
            if ($arr_last_emppaid["days2"] != "") {
                $tmp_prev_days2 = $arr_last_emppaid["days2"];
                //前々年付与
                if ($tmp_days1 > $tmp_prev_days2) {
                    $tmp_days1 = $tmp_prev_days2; //付与日数
                    $tmp_carry_time_minute = 0;
                }
            }
            //取得
            $nenkyu_siyo = $timecard_common_class->get_nenkyu_siyo($emp_id, $paid_hol_start_date, $end_date);
            //有休残＝繰越＋付与＋調整日数−取得 20121102
            $ret_str = $tmp_days1 + $tmp_days2 + $tmp_adjust_day - $nenkyu_siyo;
        }
        //移行データがある場合 20150210
        else {
        
            $ret_str = $tmp_days1 + $tmp_days2 + $tmp_adjust_day - $arr_paid_hol_import["curr_use"];
            //移行日後の使用数を取得、時間の計算のあとで引く
            //移行日の翌日からとする
            $tmp_data_migration_date = date("Ymd", strtotime("+1 day", date_utils::to_timestamp_from_ymd($arr_paid_hol_import["data_migration_date"]))); //to_timestamp関数変更 20130219
            $ikougo_siyo = $timecard_common_class->get_nenkyu_siyo($emp_id, $tmp_data_migration_date, $end_date);
        }
        //上限なし
        if ($this->paid_hol_hour_max_flag == "f") {
            //日数時間計算 20130611
            //残時間（分）＝残日数＊所定時間＋繰越時間−取得時間
            $wk_zan_minute = $ret_str * $specified_time_per_day + $tmp_carry_time_minute - $paid_hol_hour_total;
            if ($wk_zan_minute >= 0) {
            	//0.5日がある場合、残時間の計算が合わないため、除いてあとで加算 20150212
            	$wk_half_hour = (($ret_str / 0.5) % 2) * 0.5;
            	$ret_str -= $wk_half_hour;
            	$wk_zan_minute = $ret_str * $specified_time_per_day + $tmp_carry_time_minute - $paid_hol_hour_total;
                //所定時間をもとに残日数、残時間を計算
                $ret_str = intval($wk_zan_minute / $specified_time_per_day);
                $wk_hasuu = $wk_zan_minute - ($ret_str * $specified_time_per_day);
            	$ret_str += $wk_half_hour;
            }
            //マイナスの場合の対応
            else {
                //日数から繰り下げて、日数、時間を表示
                $wk_borrow_day = ceil((0 - $wk_zan_minute) / $specified_time_per_day);
                $wk_hasuu = ($specified_time_per_day * $wk_borrow_day) - (0 - $wk_zan_minute);
                $ret_str = 0 - $wk_borrow_day;
            }
            //換算
            $wk_day = intval($wk_hasuu / $specified_time_per_day);
            $wk_hasuu = $wk_hasuu - ($wk_day * $specified_time_per_day);
            $ret_str += $wk_day;

            $wk_hasuu_str = $wk_hasuu / 60;
            $wk_hasuu_str = $this->edit_nenkyu_zan($wk_hasuu_str);

            $ret_str -= $ikougo_siyo;
            switch($output_flg) {
                case "1": //pdf
                    $ret_str .= "($wk_hasuu_str)";
                    break;
                case "2": //pxdoc
                    $ret_str .= "日$wk_hasuu_str";//時間
                    break;
                case "3": //pdf hh:mm形式 20140806
                    $wk_hasuu_str = $timecard_common_class->minute_to_hmm($wk_hasuu);
                    $ret_str .= "($wk_hasuu_str)";
                    break;
                default: //html
                    $ret_str .= "日({$wk_hasuu_str}時間)";
                    break;
            }

        }
        //上限あり
        else {
            //残数、取得時間の比較 20130611
            $wk_zan_minute = $ret_str * $specified_time_per_day + $tmp_carry_time_minute;
            //ゼロの場合
            if ($paid_hol_hour_total == $wk_zan_minute) {
                $ret_str = 0;
                $wk_zan_str = 0;
            }
            else {
                //時間有休分を減算
                $ret_str -= $this->paid_hol_hour_max_day;
                $wk_max_minute = $this->paid_hol_hour_max_day * $specified_time_per_day;
                //繰越分
                $wk_max_minute += $tmp_carry_time_minute;
                $wk_hol_hour_zan = $wk_max_minute - $paid_hol_hour_total;
                //時間有休残が1日の所定時間以上の場合、日数換算して日にプラスする 20120925
                if ($wk_hol_hour_zan >= $specified_time_per_day) {
                    //wk日数=int(時間有休残/所定時間)
                    $wk_nissu = intval($wk_hol_hour_zan/$specified_time_per_day);
                    //日数+=wk日数
                    $ret_str += $wk_nissu;
                    //時間有休算=時間有休残-(wk日数*所定時間)
                    $wk_hol_hour_zan -= ($wk_nissu * $specified_time_per_day);
                }
                //マイナスの場合の対応
                if ($wk_hol_hour_zan < 0) {
                    //日数から繰り下げて、日数、時間を表示
                    $wk_borrow_day = ceil((0 - $wk_hol_hour_zan) / $specified_time_per_day);
                    $wk_hol_hour_zan = ($specified_time_per_day * $wk_borrow_day) - (0 - $wk_hol_hour_zan);
                    $ret_str -= $wk_borrow_day;
                }

                $wk_hol_hour_zan_str = $wk_hol_hour_zan / 60;
                $wk_zan_str = $this->edit_nenkyu_zan($wk_hol_hour_zan_str);
            }

            $ret_str -= $ikougo_siyo;
            switch($output_flg) {
                case "1": //pdf
                    $ret_str .= "($wk_zan_str)";
                    break;
                case "2": //pxdoc
                    $ret_str .= "日{$wk_zan_str}"; //時間
                    break;
                case "3": //pdf hh:mm形式 20140806
                    $wk_zan_str = $timecard_common_class->minute_to_hmm($wk_hol_hour_zan);
                    $ret_str .= "($wk_zan_str)";
                    break;
                default: //html
                    $ret_str .= "日({$wk_zan_str}時間)";
                    break;
            }
        }
        return $ret_str;
    }
    //指定付与日より前の1年間にある付与データを取得する
    //オプションにより2年前からのデータの直近を取得できるようにする。
    //@param $twoyear_flag 1:範囲の基準を2年前からとする。省略時は1年前から。
    function get_last_emppaid_info($emp_id, $add_date, $twoyear_flag="") {

        $month_str = "12";
        if ($twoyear_flag == "1") {
            $month_str = "24";
        }
        $start_date =  date("Ymd", strtotime("-{$month_str} month", date_utils::to_timestamp_from_ymd($add_date)));
        $end_date = date("Ymd", strtotime("-1 day", date_utils::to_timestamp_from_ymd($add_date)));
        $sql = "select * from emppaid ";
        $cond = "where emp_id = '$emp_id' "
            ." and cast(year as varchar)||paid_hol_add_mmdd >= '$start_date' "
            ." and cast(year as varchar)||paid_hol_add_mmdd < '$add_date' "
            ." order by year desc, paid_hol_add_mmdd desc limit 1 ";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $arr_data = array();
        if (pg_num_rows($sel) > 0) {
            $row = pg_fetch_all($sel);
            $arr_data = $row[0];
        }
        return $arr_data;
    }

    /**
     * 有休移行データ取得 20120808
     *
     * @param mixed $emp_id 職員ID
     * @param mixed $start_date 開始日
     * @param mixed $end_date 終了日
     * @return mixed 移行データ
     *
     */
    function get_paid_hol_import($emp_id, $start_date, $end_date) {

        $sql = "select a.* from timecard_paid_hol_import a "
            ." left join empmst b on b.emp_personal_id = a.emp_personal_id ";
        $cond = "where b.emp_id = '$emp_id' and a.data_migration_date >= '$start_date' and a.data_migration_date <= '$end_date'";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type='text/javascript' src='./js/showpage.js'></script>");
            echo("<script language='javascript'>showErrorPage(window);</script>");
            exit;
        }
        $arr_data = array();
        if (pg_num_rows($sel) > 0) {
            $row = pg_fetch_all($sel);
            $arr_data = $row[0];

            //付与日確認
            //$last_add_date = $arr_date["last_add_date"];
            $arr_add_date = split("/", $arr_date["last_add_date"]);
            $last_add_date = sprintf("%04d%02d%02d", $arr_add_date[0], $arr_add_date[1], $arr_add_date[2]);
            //範囲外の場合は、空とする
            if (!($start_date <= $last_add_date &&
                   $last_add_date <= $end_date)) {
                $arr_date = array();
            }
        }
        return $arr_data;
    }

    /**
     * 年休申請一覧用のデータを取得する
     *
     * @param mixed $emp_id 職員ID
     * @param mixed $start_date 開始日
     * @param mixed $end_date 終了日
     * @return array 年休データの配列
     *
     */
    function get_paid_hol_list($emp_id, $start_date, $end_date) {

        //有休の事由のID、休暇日数を取得
        $sql = "select * from atdbk_reason_mst  ";
        $cond = "where kind_flag = '1' and display_flag = 't' order by sequence ";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $arr_holiday_count = array();
        $arr_reason_cond = array();
        $arr_half_reason_cond = array(); //時間有休で半日有休対応 20140514
        while ($row = pg_fetch_array($sel)) {
            $reason_id = $row["reason_id"];
            $arr_holiday_count[$reason_id] = $row["holiday_count"];
            $arr_reason_cond[] = "'$reason_id'";
            if ($row["holiday_count"] == 0.5) {
                $arr_half_reason_cond[] = "'$reason_id'";
            }
        }
        if (count($arr_reason_cond) > 0) {
            $cond_reason_str = join(",", $arr_reason_cond);
        }
        else {
            $cond_reason_str = "'1','2','3','37','38','39','44','55','57','58','62'";
        }
        if (count($arr_half_reason_cond) > 0) {
            $cond_half_reason_str = join(",", $arr_half_reason_cond);
        }
        else {
            $cond_half_reason_str = "'2','3','38','39','44','55','57','58','62'";
        }

        $sql = "select emp_id, date, reason, '' as start_time, '' as use_hour, '' as use_minute from atdbkrslt "
            ."where emp_id = '$emp_id' and date >= '$start_date' and date <= '$end_date' and "
            ."(reason in ($cond_reason_str))  "
            ." and date not in (select start_date as date from timecard_paid_hol_hour " //時間有休がある場合、1日単位の有休を出力しない 20130123
            ." where emp_id = '$emp_id' and start_date >= '$start_date' and start_date <= '$end_date') "
            ."union "
            ."select emp_id, start_date as date, '' as reason, start_time, use_hour, use_minute from timecard_paid_hol_hour "
            ." where emp_id = '$emp_id' and start_date >= '$start_date' and start_date <= '$end_date' "
        ;

        $cond =" order by date";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $arr_data =  pg_fetch_all($sel);
        $data_cnt = pg_num_rows($sel); //件数を確認 20140925

        //時間有休で半日有休対応
        $sql = "select emp_id, date, reason, '' as start_time, '' as use_hour, '' as use_minute from atdbkrslt "
            ."where emp_id = '$emp_id' and date >= '$start_date' and date <= '$end_date' and "
            ."(reason in ($cond_half_reason_str))  "
            ." and date in (select start_date as date from timecard_paid_hol_hour " //時間有休がある場合
            ." where emp_id = '$emp_id' and start_date >= '$start_date' and start_date <= '$end_date') "
        ;
        $cond =" order by date";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $arr_half_data = array();
        while ($row = pg_fetch_array($sel)) {
            if ($row["reason"] != "") {
                $arr_half_data[$row["date"]]["reason"] = $row["reason"];
            }
        }
        //遅参早退の欠勤振替を除く 20140902
        $sql = "select a.* from kintai_late a ";
        $cond ="where a.emp_id = '$emp_id' and ((a.start_date >= '$start_date' and a.start_date <= '$end_date' ) or (a.end_date >= '$start_date' and a.end_date <= '$end_date' )) and a.transfer = 2 order by a.start_date;";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $arr_jogai_list = array();
        while ($row = pg_fetch_array($sel)) {
            $arr_jogai_list[] = $row["start_date"];
        }

        //勤怠申請
		//総合届け使用フラグ
		$general_flag = file_exists("opt/general_flag");
		$start_date_nm = ($general_flag) ? "start_date1" : "start_date";
		$end_date_nm = ($general_flag) ? "end_date1" : "end_date";
		//通常の場合
		if (!$general_flag) {
	        $sql = "select a.* from kintai_hol a ";
	        $cond ="where a.emp_id = '$emp_id' and ((a.start_date >= '$start_date' and a.start_date <= '$end_date' ) or (a.end_date >= '$start_date' and a.end_date <= '$end_date' )) and a.kind_flag = '1' and (a.apply_stat = '0' or a.apply_stat = '1') order by a.start_date, a.start_time;";
        }
        //総合届けの場合
        else {
	        $sql = "select hol.* from ( ";
	        $sql .= "select a.start_date1, a.end_date1, a.apply_stat, a.apply_date, a.reason, a.start_time, a.use_hour, a.use_minute from kintai_hol a ";
	        $sql .=" where a.emp_id = '$emp_id' and ((a.$start_date_nm >= '$start_date' and a.$start_date_nm <= '$end_date' ) or (a.$end_date_nm >= '$start_date' and a.$end_date_nm <= '$end_date' )) and a.kind_flag = '1' and (a.apply_stat = '0' or a.apply_stat = '1') ";
			for ($d_idx=2; $d_idx<=5; $d_idx++) {
				$start_date_nm2 = "start_date".$d_idx;
				$end_date_nm2 = "end_date".$d_idx;
				$sql .= "union ";
		        $sql .= "select a.$start_date_nm2 as start_date1, a.$end_date_nm2 as end_date1, a.apply_stat, a.apply_date, a.reason, '' as start_time, '' as use_hour, '' as use_minute from kintai_hol a ";
		        $sql .=" where a.emp_id = '$emp_id' and ((a.$start_date_nm >= '$start_date' and a.$start_date_nm <= '$end_date' ) or (a.$end_date_nm >= '$start_date' and a.$end_date_nm <= '$end_date' )) and a.kind_flag = '1' and (a.apply_stat = '0' or a.apply_stat = '1') ";
			}
			$sql .= ") hol ";

			$sql .=" order by hol.$start_date_nm, hol.start_time;";

			$cond ="";
        }
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $arr_kintai_hol = array();
        while ($row = pg_fetch_array($sel)) {
            $wk_start_date = $row["$start_date_nm"];
            //除外
            if (in_array($wk_start_date, $arr_jogai_list)) {
                continue;
            }

            $wk_end_date = $row["$end_date_nm"];
            $apply_stat = $row["apply_stat"];

            $apply_date = $row["apply_date"];

            $wk_reason = $row["reason"];

            $tmp_date = $wk_start_date;

            while ($tmp_date <= $wk_end_date) {
                if ($tmp_date >= $start_date && $tmp_date <= $end_date) {
                    $arr_kintai_hol[$tmp_date]["apply_stat"] = $apply_stat;
                    $arr_kintai_hol[$tmp_date]["apply_date"] = $apply_date;
                    $arr_kintai_hol[$tmp_date]["reason"] = $wk_reason;
                    $arr_kintai_hol[$tmp_date]["start_time"] = $row["start_time"];
                    $arr_kintai_hol[$tmp_date]["use_hour"] = intval($row["use_hour"]);
                    $arr_kintai_hol[$tmp_date]["use_minute"] = $row["use_minute"];
                }
                $timestamp = date_utils::to_timestamp_from_ymd($tmp_date);
                $tmp_date = date("Ymd", strtotime("+1 day", $timestamp));
            }
        }
        //遅参早退の時間有休振替を追加 20140902
        $sql = "select a.*,b.use_hour, b.use_minute, b.calc_minute, c.apply_date as c_apply_date,c.apply_stat as c_apply_stat from kintai_late a "
        ." left join timecard_paid_hol_hour b on b.emp_id = a.emp_id and b.start_date = a.start_date "
        ." left join apply c on c.apply_id = a.apply_id ";
        //applyはあとで必要になるかもしれないため
        $cond ="where a.emp_id = '$emp_id' and ((a.start_date >= '$start_date' and a.start_date <= '$end_date' ) or (a.end_date >= '$start_date' and a.end_date <= '$end_date' )) and (a.apply_stat = '0' or a.apply_stat = '1') and a.transfer = 1  order by a.start_date;";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        while ($row = pg_fetch_array($sel)) {
            $wk_start_date = $row["start_date"];

            $wk_end_date = $row["end_date"];
            $apply_stat = $row["apply_stat"];

            $apply_date = $row["apply_date"];

            $wk_reason = $row["reason"];

            $tmp_date = $wk_start_date;

            while ($tmp_date <= $wk_end_date) {
                if ($tmp_date >= $start_date && $tmp_date <= $end_date &&
                	(($tmp_date == $wk_start_date && $row["kind_flag"] == "2") || 
                	 ($tmp_date == $wk_end_date && $row["kind_flag"] == "1"))) {
                    $arr_kintai_hol[$tmp_date]["apply_stat"] = $apply_stat;
                    $arr_kintai_hol[$tmp_date]["apply_date"] = $apply_date;
                    $arr_kintai_hol[$tmp_date]["reason"] = $wk_reason;
                    $arr_kintai_hol[$tmp_date]["start_time"] = $row["start_time"];
                    $arr_kintai_hol[$tmp_date]["use_hour"] = intval($row["use_hour"]);
                    $arr_kintai_hol[$tmp_date]["use_minute"] = $row["use_minute"];
                }
                $timestamp = date_utils::to_timestamp_from_ymd($tmp_date);
                $tmp_date = date("Ymd", strtotime("+1 day", $timestamp));
            }
        }
        //実績なしで申請のみある場合 20130802
        //実績の日付チェック用
        $arr_rslt_date = array();
        $i = 0; //配列位置変数初期化 20140925
		if ($data_cnt > 0) { //件数を確認 20140925
	        for ($i=0; $i<count($arr_data); $i++) {
	            $tmp_date = $arr_data[$i]["date"];
	            if ($tmp_date == "") {
	                continue;
	            }
	            if ($arr_kintai_hol[$tmp_date]["apply_stat"] != "") {
	                $arr_data[$i]["apply_stat"] = $arr_kintai_hol[$tmp_date]["apply_stat"];
	                $arr_data[$i]["apply_date"] = $arr_kintai_hol[$tmp_date]["apply_date"];
	                //時間有休で半日有休の場合 20130805
	                if ($arr_kintai_hol[$tmp_date]["reason"] != "" &&
	                    $arr_kintai_hol[$tmp_date]["reason"] != "1" &&
	                    $arr_kintai_hol[$tmp_date]["reason"] != "37") {
	                    $arr_data[$i]["reason"] = $arr_kintai_hol[$tmp_date]["reason"];
	                }
	            }
	            else {
	                //申請がなく時間有休で半日有休の場合 20140514
	                if ($arr_half_data[$tmp_date]["reason"] != "") {
	                    $arr_data[$i]["reason"] = $arr_half_data[$tmp_date]["reason"];
	                }
	            }
	            //休暇日数設定
	            if ($arr_data[$i]["reason"] != "") {

	                if (count($arr_holiday_count) > 0) {
	                    $wk_reason_id =  $arr_data[$i]["reason"];
	                    $wk_hol_day = $arr_holiday_count[$wk_reason_id];
	                }
	                else {
	                    switch($arr_data[$i]["reason"]) {
	                        case "1":
	                        case "37":
	                            $wk_hol_day = 1.0;
	                            break;
	                        default:
	                            $wk_hol_day = 0.5;
	                    }
	                }
	            }
	            else {
	                $wk_hol_day = "";
	            }
	            //時間有休で半日有休の場合 20130805
	            if ( $arr_kintai_hol[$tmp_date]["start_time"] != "" &&
	                    ($arr_data[$i]["reason"] != "1" && $arr_data[$i]["reason"] != "37")) {
	                $arr_data[$i]["start_time"] = $arr_kintai_hol[$tmp_date]["start_time"];
	                $arr_data[$i]["use_hour"] = $arr_kintai_hol[$tmp_date]["use_hour"];
	                $arr_data[$i]["use_minute"] = $arr_kintai_hol[$tmp_date]["use_minute"];
	            }
	            $arr_data[$i]["hol_day"] = $wk_hol_day;
	            $arr_rslt_date[] = $tmp_date;
	        }
        }
        $wk_idx = $i;
        //実績なしで申請のみある場合 20130802
        foreach ($arr_kintai_hol as $tmp_date => $wk_kintai_hol) {
            //実績にない場合、申請情報から配列へ設定
            if (!in_array($tmp_date, $arr_rslt_date)) {
                $arr_data[$wk_idx]["date"] = $tmp_date;
                $arr_data[$wk_idx]["apply_stat"] = $wk_kintai_hol["apply_stat"];
                $arr_data[$wk_idx]["apply_date"] = $wk_kintai_hol["apply_date"];
                $arr_data[$wk_idx]["reason"] = $wk_kintai_hol["reason"];
                //時間がある場合時間有休
                if ($wk_kintai_hol["start_time"] != "") {
                    $arr_data[$wk_idx]["start_time"] = $wk_kintai_hol["start_time"];
                    $arr_data[$wk_idx]["use_hour"] = $wk_kintai_hol["use_hour"];
                    $arr_data[$wk_idx]["use_minute"] = $wk_kintai_hol["use_minute"];
                    //半日有休がある場合
                    if ($wk_kintai_hol["reason"] != "1" &&
                        $wk_kintai_hol["reason"] != "37") {
                        $arr_data[$wk_idx]["hol_day"] = "0.5";
                    }
                }
                else {
                    if ($wk_kintai_hol["reason"] == "1" || $wk_kintai_hol["reason"] == "37") {
                        $arr_data[$wk_idx]["hol_day"] = "1.0";
                    }
                    else {
                        $arr_data[$wk_idx]["hol_day"] = "0.5";
                    }
                }
                $arr_data[$wk_idx]["no_rslt_flag"] = "1";
                $wk_idx++;
            }
        }
        //移行データ確認
        $arr_paid_hol_import = $this->get_paid_hol_import($emp_id, $start_date, $end_date);
        if ($arr_paid_hol_import["data_migration_date"] != "" &&
                $arr_paid_hol_import["curr_use"]) {
            $data_migration_date = $arr_paid_hol_import["data_migration_date"];
            $curr_use = $arr_paid_hol_import["curr_use"];
            if (count($arr_data) > 0) {
                $arr_data_org = $arr_data;
                $arr_data = array();
                $arr_data[0]["date"] = $data_migration_date;
                $arr_data[0]["curr_use"] = $curr_use;
                //移行日以前の有休のデータは除く
                for ($j=0; $j<count($arr_data_org); $j++) {
                    if ($arr_data_org[$j]["date"] > $data_migration_date ||
                            $arr_data_org[$j]["start_time"] != "") {
                        $arr_data[] = $arr_data_org[$j];
                    }
                }
            }
            else {
                $arr_data[0]["date"] = $arr_paid_hol_import["data_migration_date"];
                $arr_data[0]["curr_use"] = $arr_paid_hol_import["curr_use"];
            }

        }
        //時間移行データ確認
        $arr_paid_hol_hour_import = $this->get_paid_hol_hour_import($emp_id, $start_date, $end_date);
        if ($arr_paid_hol_hour_import["data_migration_date"] != "" &&
                $arr_paid_hol_hour_import["curr_use"]) {
            $data_migration_date = $arr_paid_hol_hour_import["data_migration_date"];
            $curr_use = $arr_paid_hol_hour_import["curr_use"];
            //移行日以前の有休のデータは除く
            if (count($arr_data) > 0) {
                $arr_data_org = $arr_data;
                $arr_data = array();
                //移行日以前の時間有休のデータは除く
                for ($j=0; $j<count($arr_data_org); $j++) {
                    if ($arr_data_org[$j]["date"] > $data_migration_date ||
                            $arr_data_org[$j]["start_time"] == "") {
                        $arr_data[] = $arr_data_org[$j];
                    }
                }

            }
            //移行データ分を追加し、日付でソート
            $wk_idx = count($arr_data);
            $arr_data[$wk_idx]["date"] = $data_migration_date;
            $arr_data[$wk_idx]["curr_use_hour"] = $curr_use;


        }
        //列方向の配列を得る
        for ($i=0; $i<count($arr_data); $i++) {
            $wk_key1[$i] = $arr_data[$i]["date"];
        }
        // キーでソートする。
        array_multisort($wk_key1, SORT_ASC, $arr_data);
        return $arr_data;

    }

    // 勤怠申請用年休残
    // @param $aprv_flag ※未使用system_config設定を使用 20141204 1:承認済みのみ、2:申請中、承認済み 
    // @param $date 休暇を受ける日（開始日）省略可、省略時はシステム日
    // @param $date 休暇を受ける日（終了日）更新時の確認用、省略可
    // @param $paid_hol_hour_flg 時間有休フラグ、省略可 fの場合、(h:mm)を出力しない
    function get_nenkyu_zan_kintai($emp_id, $aprv_flag, $date="", $end_date="", $paid_hol_hour_flg="f") {

		$conf = new Cmx_SystemConfig();
		//年休残の計算対象 1:承認済みのみ 2:申請中、承認済み
		$remain_count_flg = $conf->get('apply.hol.remain_count_flg');
		//初期値
		if ($remain_count_flg == "") {
			$remain_count_flg = "1";
		}
        //更新時の確認用
        $target_date = $date;
        if ($date == "") {
            $date = date("Ymd");
        }
        //所定時間
        $day1_time = $this->get_specified_time_calendar();
        $wk_minute = $this->get_specified_time($emp_id, $day1_time);
        //有休付与データ
        $emppaid_info = $this->get_last_emppaid_info($emp_id, $date, "1");

        $nenkyu_zan = "";
        if (count($emppaid_info) > 0) {

            $start_date = $emppaid_info["year"].$emppaid_info["paid_hol_add_mmdd"];
            $date_y1 = substr($start_date, 0, 4);
            $date_m1 = substr($start_date, 4, 2);
            $date_d1 = substr($start_date, 6, 2);
            $edYMD = mktime(0, 0, 0, intval($date_m1), intval($date_d1), (intval($date_y1) + 2));	//2年プラス(付与が遅れている場合があるため)
            $edYMD = mktime(0, 0, 0, date("n", $edYMD), date("j", $edYMD) - 1, date("Y", $edYMD));	//１日マイナス
            $wk_end_date = date("Ymd", $edYMD);


            //繰越[$curr_idx]
            $wk_carry = $emppaid_info["days1"];
            //有休付与
            $wk_add = $emppaid_info["days2"];

            //調整日数
            $wk_adjust_day = $emppaid_info["adjust_day"];

            $wk_total = $wk_carry + $wk_add + $wk_adjust_day;
            //繰越時刻
            $wk_carry_time_minute = $emppaid_info["carry_time_minute"];

            $arr_data = $this->get_paid_hol_list($emp_id, $start_date, $wk_end_date);

            $zan_day = $wk_total;
            $zan_minute = $wk_carry_time_minute;
            for ($i=0; $i<count($arr_data); $i++) {

                if ($arr_data[$i]["date"] == "") {
                    continue;
                }
                //更新時に範囲が同じ日で申請中は除く
                if ($target_date != "" && $end_date != "" && $arr_data[$i]["apply_stat"] != "1" &&
                        $target_date <= $arr_data[$i]["date"] && $arr_data[$i]["date"] <= $end_date ) {
                    continue;
                }
                //承認済みのみ集計
                if ($remain_count_flg == "1") {
                    if ($arr_data[$i]["apply_stat"] == "0" && //申請中を除く 20150319
                        $arr_data[$i]["curr_use"] == "" && //移行データがある場合を除く
                        $arr_data[$i]["curr_use_hour"] == "") {
                        continue;
                    }
                }
                //移行データの確認
                if ($arr_data[$i]["curr_use"] != "") {
                    $wk_hol_day = $arr_data[$i]["curr_use"];
                }
                //
                elseif ($arr_data[$i]["reason"] != "") {
                        $wk_hol_day = $arr_data[$i]["hol_day"];
                }
                else {
                    $wk_hol_day = "";
                }
                //移行データがある場合
                if ($arr_data[$i]["curr_use_hour"] != "") {
                    $use_minute_calc = $this->hh_or_hhmm_to_minute($arr_data[$i]["curr_use_hour"]); //20141014 HH:MM対応
                }
                elseif ($arr_data[$i]["start_time"] != "") {
                        $use_minute_calc = $arr_data[$i]["use_hour"] * 60 + intval($arr_data[$i]["use_minute"]);
                }
                else {
                    $use_minute_calc = 0;
                }
                //残数計算
                //所定時間未満分の時間を表示
                //残数から引ける場合
                if ($use_minute_calc > 0) {
                    if ($zan_minute >= $use_minute_calc) {
                        $zan_minute -= $use_minute_calc;
                    }
                    //残が少ない場合、日から繰り下げ
                    else {
                        //残が半日で引ける場合
                        if ($zan_day == 0.5 && ($zan_minute + ($wk_minute / 2) >= $use_minute_calc )) {
                            $zan_day -= 0.5; //日から引く
                            $zan_minute += ($wk_minute / 2); //所定時間
                        }
                        else {
                            $zan_day--; //日から引く
                            $zan_minute += $wk_minute; //所定時間
                        }
                        $zan_minute -= $use_minute_calc;
                    }
                }
                //時間を合わせて1日を引ける場合
                if ($arr_data[$i]["hol_day"] == 1 && $zan_day == 0.5 && $zan_minute >= ($wk_minute / 2)) {
                    $zan_day -= 0.5; //日から引く
                    $zan_minute -= floor($wk_minute / 2);
                    $wk_hol_day = "";
                }

                //時間から0.5日を引ける場合
                if ($arr_data[$i]["hol_day"] == 0.5 && $zan_day == 0 && $zan_minute >= ($wk_minute / 2)) {
                    $zan_minute -= floor($wk_minute / 2);
                    $wk_hol_day = "";
                }

                if ($wk_hol_day != "") {
                    $zan_day -= $wk_hol_day;
                }
            }
            $time_h = intval($zan_minute / 60);
            $time_m = $zan_minute % 60;
            $time_hm = $time_h.sprintf(":%02d", $time_m);
            $nenkyu_zan = $zan_day."日";
            if ($paid_hol_hour_flg == "t") {
                $nenkyu_zan .= "({$time_hm})";
            }
        }

        return $nenkyu_zan;
    }

    /**
     * 夏休付与データを取得する
     *
     * @param mixed $emp_id 職員ID
     * @param mixed $year 年
     * @return mixed 夏休付与データ
     *
     */
    function get_emppaid_summer($emp_id, $date){
        $sql = "select * from emppaid_summer";
        $cond = "where emp_id = '$emp_id' and add_date >= '$date' order by add_date desc limit 1";

        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type='text/javascript' src='./js/showpage.js'></script>");
            echo("<script language='javascript'>showErrorPage(window);</script>");
            exit;
        }

        $rslt_data = array();
        if (pg_num_rows($sel) > 0) {
            $row = pg_fetch_all($sel);
            $rslt_data = $row[0];
        }

        return $rslt_data;
    }
    //時間、HH形式またはHH:MM形式を分に変換する
	function hh_or_hhmm_to_minute($hhmm) {
		$min = 0;
		if (strpos($hhmm, ":") !== false) {
			$min = date_utils::hi_to_minute($hhmm);
		}
		else {
			$min = $hhmm * 60;
		}
		return $min;
	}
    //指定期間の年休事由を取得する。$tableにはatdbkかatdbkrsltを指定
    function get_paid_hol_reason($emp_id, $table, $start_date, $end_date) {

        $sql = "select date, reason from $table ";
        $cond =" where emp_id = '$emp_id' and date >= '$start_date' and date <= '$end_date' and reason in ('1','2','3','37','38','39') order by date";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $arr_date_reason = array();
        while ($row = pg_fetch_array($sel)) {
            $date = $row["date"];
            $arr_date_reason["$date"] = $row["reason"];
        }
        return $arr_date_reason;
    }
    //指定期間の事由が年休か確認する。true:全て年休 false:年休ではない
    //当日以前は実績、当日よりあとは予定を確認
	function check_paid_reason($emp_id, $start_date, $end_date) {

		$arr_date_reason = array();
		$today = date("Ymd");
		//過去のみ
		if ($end_date <= $today) {
			$arr_date_reason = $this->get_paid_hol_reason($emp_id, "atdbkrslt", $start_date, $end_date);
		}
		//未来のみ
		elseif ($start_date > $today) {
			$arr_date_reason = $this->get_paid_hol_reason($emp_id, "atdbk", $start_date, $end_date);
			//実績も確認 20141212
			$arr_date_reason_rslt = $this->get_paid_hol_reason($emp_id, "atdbkrslt", $start_date, $end_date);
			foreach ($arr_date_reason_rslt as $key => $value) {
				$arr_date_reason["$key"] = $value;
			}
		}
		//両方
		else {
			//実績は未来も確認 20141212
			$arr_date_reason = $this->get_paid_hol_reason($emp_id, "atdbkrslt", $start_date, $end_date);
			$next_day = date("Ymd",strtotime("+1 day"));
			$arr_date_yotei = $this->get_paid_hol_reason($emp_id, "atdbk", $next_day, $end_date);
            foreach ($arr_date_yotei as $key => $value) {
                $arr_date_reason["$key"] = $value;
            }
		}
		//件数を確認
		$reason_cnt = count($arr_date_reason);

		//日数
		$day_cnt = ((date_utils::to_timestamp_from_ymd($end_date) - date_utils::to_timestamp_from_ymd($start_date)) / (24 * 3600)) + 1;
		
		if ($reason_cnt >= $day_cnt) {
			$ret = true;
		}
		else {
			$ret = false;
		}
		
		return $ret;
	}
}
?>
