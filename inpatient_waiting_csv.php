<?
ob_start();
require_once("about_session.php");
require_once("about_authority.php");
ob_end_clean();

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

$sql = "select * from bedcheckwait";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
for ($i = 1; $i <= 10; $i++) {
	$varname = "display$i";
	$$varname = pg_fetch_result($sel, 0, "display$i");
}

$sql = "select w.ptwait_id, w.ptif_id, (p.ptif_lt_kaj_nm || ' ' || p.ptif_ft_kaj_nm) as ptif_nm, p.ptif_birth, ptif_sex, m.mst_name, w.disease, w.patho_from, w.patho_to, substr(w.reception_time, 1, 8) as reception_date, w.status1, w.status2, r.ptif_id as r_ptif_id, i.ptif_id as i_ptif_id from ptwait w inner join ptifmst p on w.ptif_id = p.ptif_id left join institemmst m on w.intro_inst_cd = m.mst_cd left join inptres r on w.ptif_id = r.ptif_id left join inptmst i on w.ptif_id = i.ptif_id";
$cond = "where w.progress = ";
$cond .= ($progress == "2") ? "'2'" : "'1'";
if ($fromdate != "" && $todate != "") {
	$cond .= " and w.reception_time between '{$fromdate}0000' and '{$todate}9999'";
}
$cond .= " order by w.reception_time desc";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
pg_close($con);

$header_line = construct_header_line($display2, $display6, $display7, $display9, $display10);
$body_lines = construct_body_lines($display2, $display6, $display7, $display9, $display10, $sel);
$csv = mb_convert_encoding($header_line . "\r\n" . $body_lines, "Shift_JIS", "EUC-JP");
header("Content-Type: text/csv");
header("Content-Disposition: attachment; filename=waiting_list.csv");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
header("Pragma: public");
header("Content-Length: " . strlen($csv));
echo($csv);


function construct_header_line($display2, $display6, $display7, $display9, $display10) {
	$cols = array("患者ID", "患者氏名", "年齢", "性別");
	if ($display2 == "t") $cols[] = "紹介元医療機関";
	if ($display6 == "t") $cols[] = "傷病名";
	if ($display7 == "t") $cols[] = "発症日";
	$cols[] = "受付日";
	if ($display9 == "t") $cols[] = "調整状況1";
	if ($display10 == "t") $cols[] = "調整状況2";
	return join(",", $cols);
}

function construct_body_lines($display2, $display6, $display7, $display9, $display10, $sel) {
	if (pg_num_rows($sel) == 0) return "";

	$lines = array();
	while ($row = pg_fetch_array($sel)) {
		$tmp_line = array(
			$row["ptif_id"],
			$row["ptif_nm"],
			calc_age($row["ptif_birth"]) . "歳",
			format_sex($row["ptif_sex"])
		);

		if ($display2 == "t") $tmp_line[] = $row["mst_name"];
		if ($display6 == "t") $tmp_line[] = $row["disease"];
		if ($display7 == "t") $tmp_line[] = format_patho($row["patho_from"], $row["patho_to"]);
		$tmp_line[] = format_date($row["reception_date"]);
		if ($display9 == "t") $tmp_line[] = $row["status1"];
		if ($display10 == "t") $tmp_line[] = $row["status2"];
		$lines[] = join(",", $tmp_line);
	}
	return join("\r\n", $lines) . "\r\n";
}

function calc_age($birth) {
	$age = date("Y") - substr($birth, 0, 4);
	if (date("md") < substr($birth, 4, 8)) $age--;
	return $age;
}

function format_sex($sex) {
	switch ($sex) {
	case "1":
		return "男性";
	case "2":
		return "女性";
	default:
		return "不明";
	}
}

function format_patho($from, $to) {
	if ($from == "" && $to == "") return "";
	$days = array(format_date($from), format_date($to));
	return join(" 〜 ", $days);
}

function format_date($date) {
	return preg_replace("/\A(\d{4})(\d{2})(\d{2})\z/", "$1/$2/$3", $date);
}
