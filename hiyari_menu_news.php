<?php
require_once('about_comedix.php');
require_once("hiyari_common.ini");
require_once("show_class_name.ini");

function get_menu_news($session, $con, $fname, $emp_id, $detail_flg=false)
{
    $today = date("Ymd");

    $arr_class_name = get_class_name_array($con, $fname);
    $newsfile_folder_name = get_newsfile_folder_name();

    // ログインユーザの所属部門・職種を取得
    $sql = "SELECT emp_class, emp_job FROM empmst WHERE emp_id='{$emp_id}' ";
    $sql.= "UNION ALL ";
    $sql.= "SELECT emp_class, null FROM concurrent WHERE emp_id='{$emp_id}' ";

    $sel = select_from_table($con, $sql, "", $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }

    $job_id = pg_fetch_result($sel, 0, "emp_job");
    $class_id = "";
    while ($row = pg_fetch_assoc($sel)) {
        if($class_id != "") {
            $class_id .= ",";
        }
        $class_id .= $row["emp_class"];
    }

    $sql = "SELECT * FROM inci_news ";
    $sql.= "WHERE news_begin <= '$today'";
    $sql.= " AND news_end >= '$today'";
    $sql.= " AND (news_category = '1' OR (news_category = '2' AND class_id in ($class_id)) OR (news_category = '3' AND job_id = $job_id))";
    $sql.= "ORDER BY news_date desc";
    $sel = select_from_table($con, $sql, "", $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script type='text/javascript'>showErrorPage(window);</script>");
        exit;
    }
    $num = pg_num_rows($sel);

    $news_list = array();
    while ($row = pg_fetch_assoc($sel)) {
        $data = array();
        switch ($row["news_category"]) {
            case "1":
                $category_name = "全館";
                break;
            case "2":
                $category_name = $arr_class_name[0];
                break;
            case "3":
                $category_name = "職種";
                break;
        }

        $data["id"] = $row["news_id"];
        $data["style"] = get_style_by_marker_hiyari_title($row["marker"]);
        $data["title"] = h($row["news_title"]);
        $data["category"] = $category_name;
        $data["date"] = preg_replace("/^(\d\d\d\d)(\d\d)(\d\d)/", "$1/$2/$3", $row["news_date"]);

        $data["class"] = "";
        if ($detail_flg) {
            $data["class"] = "has_detail";
            $data["news"] = $row["news"];

            // 添付ファイル情報を取得
            $sql = "SELECT newsfile_no, newsfile_name FROM inci_newsfile WHERE news_id={$row["news_id"]} ORDER BY newsfile_no";
            $sel_file = select_from_table($con, $sql, "", $fname);
            if ($sel_file == 0) {
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
            $imgfiles = "";
            while ($file = pg_fetch_assoc($sel_file)) {
                if (preg_match("/(png|jpe*g|gif)$/i", $file["newsfile_name"])) {
                    $ext = strrchr($file["newsfile_name"], ".");
                    $src = sprintf('%s/%s_%s%s', $newsfile_folder_name, $row["news_id"], $file["newsfile_no"], $ext);
                    $size = getimagesize($src);
                    $style = "";
                    if ($size[0] > 960){
                        $style = 'width="960"';
                    }
                    $imgfiles .= sprintf('<br><br><img src="%s" alt="%s" %s>', $src, $file["newsfile_name"], $style);
                }
            }
            if ($imgfiles) {
                $data["img"] = $imgfiles;
            }
        }

        $news_list[] = $data;
    }

    return $news_list;
}

function get_no_recv_read_num($emp_id, $fname) {
    $con=connect2db($fname);
    if($con=="0"){
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }

    $sql  = " select count(*) as cnt";
    $sql .= " from";
    $sql .= " (";
    $sql .= " select mail_id from inci_mail_recv_info where recv_emp_id = '$emp_id' and recv_read_flg = 'f' and recv_del_flg = 'f'";
    $sql .= " ) mr";
    $sql .= " natural inner join (select mail_id,report_id from inci_mail_send_info) ms";
    $sql .= " natural inner join (select report_id from inci_report where not kill_flg) rp";
    $cond = "";

    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }

    $cnt = pg_fetch_result($sel, 0, "cnt");

    return $cnt;

}


// ラインマーカーのスタイル定義を取得
function get_style_by_marker_hiyari_title($marker) {
	switch ($marker) {
		case 1: return " style=\"background-color:red;color:white;padding:2px;\"";// 赤
		case 2: return " style=\"background-color:aqua;color:blue;padding:2px;\"";// 青
		case 3: return " style=\"background-color:yellow;color:blue;padding:2px;\"";// 黄
		case 4: return " style=\"background-color:lime;color:blue;padding:2px;\"";// 緑
		case 5: return " style=\"background-color:fuchsia;color:white;padding:2px;\"";// ピンク
	default:return "";  
	}
}
