<?php
require_once("about_comedix.php");
require_once("duty_shift_common.ini");
require_once("duty_shift_manage_tab_common.ini");
require_once("duty_shift_common_class.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
//セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}
///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if ($con == "0") {
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
///-----------------------------------------------------------------------------
// 権限のチェック
///-----------------------------------------------------------------------------
//管理画面用
$chk_flg = $obj->check_authority_Management($session, $fname);
if ($chk_flg == "") {
    pg_close($con);
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}
//ユーザ画面用
$section_admin_auth = $obj->check_authority_user($session, $fname);
///-----------------------------------------------------------------------------
//初期値設定
///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
// 勤務パターン情報を取得
///-----------------------------------------------------------------------------
$wktmgrp_array = $obj->get_wktmgrp_array();
if (count($wktmgrp_array) <= 0) {
    $err_msg_1 = "勤務パターングループ情報(wktmgrp)が未設定です。管理者に連絡してください。";
}
if ($group_id == "") {
    $group_id = $wktmgrp_array[0]["id"];
}

if ($regist_flg == "1") {
    //入力チェック
    for ($i = 0; $i < $data_cnt; $i++) {
        $wk_varname = "sfc_code_$i";
        if (!empty($$wk_varname) and preg_match("/^\w{1,4}$/", $$wk_varname) === 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\">alert('連携用コードは半角英数4桁以内で入力してください。');</script>");
            echo("<script type=\"text/javascript\">history.back();</script>");
            exit;
        }
    }

    ///-----------------------------------------------------------------------------
    // トランザクションを開始
    ///-----------------------------------------------------------------------------
    pg_query($con, "begin transaction");
    ///-----------------------------------------------------------------------------
    // 選択された勤務パターングループの連携コード情報を更新
    ///-----------------------------------------------------------------------------
    for ($i = 0; $i < $data_cnt; $i++) {
        //値取得
        $wk1 = "atdptn_ptn_id_$i";
        $wk2 = "sfc_code_$i";
        //reason
        $wk3 = "reason_$i";
        $reason = $$wk3;

        $wk4 = "output_kbn_$i";

        $sql = "update duty_shift_pattern set";
        $set = array("sfc_code", "output_kbn");
        $setvalue = array($$wk2, $$wk4);
        $cond = "where pattern_id = $group_id and atdptn_ptn_id = " . $$wk1;
        //reason
        if ($reason != "") {
            $cond .= " and reason = '$reason' ";
        }

        $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
        if ($upd == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }
    ///-----------------------------------------------------------------------------
    // トランザクションをコミット
    ///-----------------------------------------------------------------------------
    pg_query("commit");
}
///-----------------------------------------------------------------------------
//ＤＢ(atdptn)より出勤パターン情報取得
///-----------------------------------------------------------------------------
$atdptn_array = $obj->get_atdptn_array($group_id);
///-----------------------------------------------------------------------------
//勤務シフトパターン情報の取得
///-----------------------------------------------------------------------------
$pattern_array = $obj->get_duty_shift_pattern_array($group_id, $atdptn_array);
if (count($pattern_array) <= 0) {
    //-------------------------------------------------------------------
    //データがない時
    //-------------------------------------------------------------------
    $wk_atdptn_ptn_id = "";
    $m = 0;
    for ($i = 0; $i < count($atdptn_array); $i++) {
        //-------------------------------------------------------------------
        //休暇の有無判定
        //-------------------------------------------------------------------
        // 休暇を名称ではなくIDで確認
        if ($atdptn_array[$i]["id"] == "10") {
            $wk_atdptn_ptn_id = $atdptn_array[$i]["id"];  //出勤パターンＩＤ
            $wk_atdptn_ptn_name = $atdptn_array[$i]["name"]; //出勤パターン名称
            $wk_count_kbn_gyo = "9999";       //集計区分(行)	（9999：その他）
            $wk_count_kbn_retu = "9999";      //集計区分(列)	（9999：その他）
            $wk_font_name = "";         //表示文字
            $wk_font_color_id = $font_color_array[0]["color"]; //文字色
            $wk_back_color_id = $back_color_array[0]["color"]; //背景色
            $wk_count_kbn_gyo_name = "";      //行名
            $wk_count_kbn_retu_name = "";      //列名
        } else {
            //-------------------------------------------------------------------
            //出勤パターン値設定
            //-------------------------------------------------------------------
            $pattern_array[$m]["pattern_id"] = $group_id;      //出勤グループＩＤ
            $pattern_array[$m]["atdptn_ptn_id"] = $atdptn_array[$i]["id"];  //出勤パターンＩＤ
            $pattern_array[$m]["atdptn_ptn_name"] = $atdptn_array[$i]["name"]; //出勤パターン名称
            $pattern_array[$m]["reason"] = " ";    //事由
            $m++;
        }
    }
    //-------------------------------------------------------------------
    //休暇が存在する場合
    //-------------------------------------------------------------------
    if ($wk_atdptn_ptn_id != "") {
        $pattern_array = $obj->setReasonPatternArray2($pattern_array, $group_id, $wk_atdptn_ptn_id, $wk_atdptn_ptn_name, $wk_count_kbn_gyo, $wk_count_kbn_retu, $wk_count_kbn_gyo_name, $wk_count_kbn_retu_name, $wk_font_name, $wk_font_color_id, $wk_back_color_id);
        // 出勤表の休暇種別等画面の非表示設定を反映
        $arr_reason_display_flag = $obj->get_reason_display_flag();
        for ($j = 0; $j < count($pattern_array); $j++) {
            $tmp_reason_id = $pattern_array[$j]["reason"];
            if ($arr_reason_display_flag["$tmp_reason_id"] != "") {
                $pattern_array[$j]["reason_display_flag"] = $arr_reason_display_flag["$tmp_reason_id"];
            }
        }
    }
}
$data_cnt = count($pattern_array);

$arr_output_kbn = array(
	"0" => "深夜",
	"1" => "日勤",
	"2" => "準夜",
	"9" => "その他"
);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
        <title>CoMedix 勤務シフト作成｜管理｜管理日誌（SFC形式）</title>

        <!-- ************************************************************************ -->
        <!-- JavaScript -->
        <!-- ************************************************************************ -->
        <script type="text/javascript" src="js/fontsize.js"></script>
        <script type="text/javascript">
            <!--
            /**
             * regist 登録処理
             **/
            function regist() {
                document.mainform.regist_flg.value = '1';
                document.mainform.submit();
            }
            // -->
        </script>

        <!-- ************************************************************************ -->
        <!-- HTML -->
        <!-- ************************************************************************ -->
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <style type="text/css">
            .list {border-collapse:collapse;}
            .list td {border:#5279a5 solid 1px;}
        </style>
    </head>

    <body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td>
                    <!-- ------------------------------------------------------------------------ -->
                    <!-- 画面遷移／タブ -->
                    <!-- ------------------------------------------------------------------------ -->
                    <?php
                    // 画面遷移
                    echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
                    show_manage_title($session, $section_admin_auth);   //duty_shift_common.ini
                    echo("</table>\n");

                    // タブ
                    $arr_option = "";
                    echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
                    show_manage_tab_menuitem($session, $fname, $arr_option); //duty_shift_manage_tab_common.ini
                    echo("</table>\n");

                    // 下線
                    echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td bgcolor=\"#5279a5\"><img src=\"img/spacer.gif\" width\"1\" height=\"2\" alt=\"\"></td></tr></table><img src=\"img/spacer.gif\" width=\"1\" height=\"5\" alt=\"\"><br>\n");
                    ?>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr height="22" >
                            <td ><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                    <a href="duty_shift_diary_shift_pattern.php?session=<?php eh($session); ?>&group_id=<?php eh($group_id); ?>">病棟管理日誌</a>&nbsp;&nbsp;
                                    管理日誌（SFC形式）&nbsp;&nbsp;
                                </font>
                            </td>
                        </tr>
                    </table>
                    <!-- ------------------------------------------------------------------------ -->
                    <!-- データ不備時 -->
                    <!-- ------------------------------------------------------------------------ -->
                    <?php
                    if ($err_msg_1 != "") {
                        echo(h($err_msg_1));
                        echo("<br>\n");
                    } else {
                        ?>
                        <!-- ------------------------------------------------------------------------ -->
                        <!-- 入力エリア -->
                        <!-- ------------------------------------------------------------------------ -->
                        <form name="mainform" action="duty_shift_sfc_conf.php" method="post">
                            <!-- ------------------------------------------------------------------------ -->
                            <!-- 勤務パターングループ名 -->
                            <!-- 登録ボタン -->
                            <!-- ------------------------------------------------------------------------ -->
                            <table width="600" border="0" cellspacing="0" cellpadding="2">
                                <?php
                                echo("<tr height=\"22\">\n");
                                ///-----------------------------------------------------------------------------
                                // 勤務パターングループ名
                                ///-----------------------------------------------------------------------------
                                echo("<td width=\"35%\" align=\"left\" bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">勤務パターングループ名</font></td>\n");
                                echo("<td width=\"\"><select name=\"group_id\" onchange=\"this.form.action='$fname'; this.form.submit();\">\n");
                                for ($i = 0; $i < count($wktmgrp_array); $i++) {
                                    $wk_id = $wktmgrp_array[$i]["id"];
                                    $wk_name = $wktmgrp_array[$i]["name"];
                                    echo("<option value=\"$wk_id\"");
                                    if ($group_id == $wk_id) {
                                        echo(" selected");
                                    }
                                    echo(">$wk_name\n");
                                }
                                echo("</select></td>\n");
                                ///-----------------------------------------------------------------------------
                                // 登録ボタン
                                ///-----------------------------------------------------------------------------
                                echo("<td colspan=\"2\" align=\"right\"><input type=\"button\" value=\"登録\" onClick=\"regist();\"></td>\n");
                                echo("</tr>\n");
                                ?>
                            </table>
                            <!-- ------------------------------------------------------------------------ -->
                            <!-- パターン一覧（見出し） -->
                            <!-- ------------------------------------------------------------------------ -->
                            <table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
                                <tr height="22" bgcolor="#f6f9ff">
                                    <td width="25%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤パターン</font></td>
                                    <td width="25%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">記号</font></td>
                                    <td width="25%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">連携コード</font></td>
                                    <td width="25%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">看護日誌出力区分</font></td>
                                </tr>
                                <!-- ------------------------------------------------------------------------ -->
                                <!-- パターン一覧（データ） -->
                                <!-- ------------------------------------------------------------------------ -->
                                <?php
                                for ($i = 0; $i < $data_cnt; $i++) {
                                    // 出勤表の休暇種別等画面の非表示を確認
                                    if ($pattern_array[$i]["reason_display_flag"] == "f") {
                                        continue;
                                    }
                                    echo("<tr height=\"22\">\n");

                                    ///-----------------------------------------------------------------------------
                                    // 出力値設定
                                    ///-----------------------------------------------------------------------------
                                    for ($k = 0; $k < count($atdptn_array); $k++) {
                                        if ($pattern_array[$i]["atdptn_ptn_id"] == $atdptn_array[$k]["id"]) {
                                            $gyo_id = $pattern_array[$i]["count_kbn_gyo"];
                                            $retu_id = $pattern_array[$i]["count_kbn_retu"];
                                            $font_name = $pattern_array[$i]["font_name"];
                                            $font_color_id = $pattern_array[$i]["font_color_id"];
                                            $back_color_id = $pattern_array[$i]["back_color_id"];
                                            $sfc_code = $pattern_array[$i]["sfc_code"];
                                            $output_kbn = $pattern_array[$i]["output_kbn"];
                                            if ($output_kbn == "") {
                                            	if (mb_strpos($pattern_array[$i]["atdptn_ptn_name"], "休") !== false ||
                                            		$pattern_array[$i]["reason"] == "44") {
                                            		$output_kbn = "9";
                                            	}
                                            	elseif (mb_strpos($pattern_array[$i]["atdptn_ptn_name"], "深夜") !== false) {
                                            		$output_kbn = "0";
                                            	}
                                            	elseif (mb_strpos($pattern_array[$i]["atdptn_ptn_name"], "準夜") !== false) {
                                            		$output_kbn = "2";
                                            	}
                                            	elseif (mb_strpos($pattern_array[$i]["atdptn_ptn_name"], "明け") !== false) {
                                            		$output_kbn = "9";
                                            	}
                                            	elseif (mb_strpos($pattern_array[$i]["atdptn_ptn_name"], "夜") !== false) {
                                            		$output_kbn = "0";
                                            	}
                                            	else {
                                            		$output_kbn = "1";
                                            	}
                                            }
                                            break;
                                        }
                                    }

                                    // 表示順
                                    ///-----------------------------------------------------------------------------
                                    // 出勤パターン
                                    ///-----------------------------------------------------------------------------
                                    $wk = $pattern_array[$i]["atdptn_ptn_name"];
                                    // 半有半公等
                                    if ($pattern_array[$i]["reason"] >= "44" &&
                                        $pattern_array[$i]["reason"] <= "47") {
                                        $wk = $pattern_array[$i]["reason_name"];
                                    }
                                    // 34:休暇（事由無し）は事由非表示
                                    else if ($pattern_array[$i]["reason_name"] != "" && $pattern_array[$i]["reason"] != "34") {
                                        $wk .= "(" . $pattern_array[$i]["reason_name"] . ")";
                                    }
                                    echo("<td width=\"20%\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk</font></td>\n");
                                    ///-----------------------------------------------------------------------------
                                    // 記号
                                    ///-----------------------------------------------------------------------------
                                    $wk = h($font_name);
                                    echo("<td width=\"10%\" align=\"center\"<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">\n");
                                    echo("$wk");
                                    echo("</font></td>\n");
                                    //連携コード
                                    $wk = h($sfc_code);
                                    echo("<td width=\"10%\" align=\"center\"<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">\n");
                                    echo("<input name=\"sfc_code_$i\" id=\"sfc_code_$i\" type=\"text\" value=\"$wk\" size=\"5\" maxlength=\"4\" style=\"ime-mode:disabled;\">\n");
                                    echo("</font></td>\n");
                                    //看護日誌出力区分
                                    //$wk = h($sfc_code);
                                    echo("<td width=\"10%\" align=\"center\"<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">\n");
                                    echo("<select name=\"output_kbn_$i\" id=\"output_kbn_$i\">\n");
                                    foreach ($arr_output_kbn as $key => $val) {
                                    	$selected = ($key == $output_kbn) ? "selected" : "";
                                    	echo("<option value=\"$key\" $selected>");
                                    	echo($val);
                                    	echo("</option>\n");
                                    }
                                    echo("</select>\n");
                                    echo("</font></td>\n");
                                    echo("</tr>\n");
                                }
                                ?>
                            </table>
                            <!-- 登録ボタン -->
                            <table width="600" border="0" cellspacing="0" cellpadding="2">
                                <?php
                                echo("<tr height=\"22\">\n");
                                echo("<td colspan=\"2\" align=\"right\"><input type=\"button\" value=\"登録\" onClick=\"regist();\"></td>\n");
                                echo("</tr>\n");
                                ?>
                            </table>
                            <!-- ------------------------------------------------------------------------ -->
                            <!-- ＨＩＤＤＥＮ -->
                            <!-- ------------------------------------------------------------------------ -->
                            <input type="hidden" name="regist_flg" value="">
                            <?php
                            echo("<input type=\"hidden\" name=\"session\" value=\"$session\">\n");
                            echo("<input type=\"hidden\" name=\"data_cnt\" value=\"$data_cnt\">\n");
                            for ($i = 0; $i < $data_cnt; $i++) {
                                //出勤パターンＩＤ
                                $wk = $pattern_array[$i]["atdptn_ptn_id"];
                                echo("<input type=\"hidden\" name=\"atdptn_ptn_id_$i\" value=\"$wk\">\n");
                                //事由
                                $wk = $pattern_array[$i]["reason"];
                                echo("<input type=\"hidden\" name=\"reason_$i\" value=\"$wk\">\n");
                            }
                            ?>
                        </form>

                        <?php
                    }
                    ?>
                </td>
            </tr>
        </table>
    </body>
    <?php pg_close($con); ?>
</html>
