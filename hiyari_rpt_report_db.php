<?php
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");
require_once("hiyari_common.ini");
require_once("hiyari_reporting_common.ini");
require_once("hiyari_mail.ini");

//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// 権限チェック
$summary = check_authority($session, 47, $fname);
if ($summary == "0") {
    showLoginPage();
    exit;
}

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//==============================
//パラメータ精査
//==============================

//日付検索(年)初期値は今年
if($search_date_year == "")
{
    $search_date_year = date("Y");
}
//日付検索(月)初期値は今月
if($search_date_month == "")
{
    $search_date_month = date("m");
}

//日付最近検索の初期値は２週間
if($search_date_mode == "" && $search_date_late_mode == "")
{
    $search_date_late_mode = "2week";
}

//==============================
//ユーザーID取得
//==============================
$sql = "SELECT * FROM empmst JOIN session USING (emp_id) WHERE session_id='{$session}'";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0) {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$emp = pg_fetch_assoc($sel);
$emp_id = $emp["emp_id"];


//==============================
//処理モードごとの処理
//==============================
if($is_postback == "true")
{
    //この画面では特に何もない。
}
//==============================
//初回アクセス時の処理
//==============================
else
{
    //担当範囲でシステム全体に対する担当者ではない場合
    $report_db_read_auth_flg = get_report_db_read_auth_flg($session,$fname,$con);
    $priority_auth = get_emp_priority_auth($session,$fname,$con);
    $is_emp_auth_system_all = is_emp_auth_system_all($emp_id, $priority_auth, $con, $fname);
    if(!$report_db_read_auth_flg || !$is_emp_auth_system_all)
    {
        //==============================
        //報告ファイルの所属参照制限がある場合の所属検索条件
        //==============================

        //ログインユーザーの報告書の所属制限
        $report_db_read_div = get_report_db_read_div($session,$fname,$con);

        //検索初期時の部署の条件をプルダウンの先頭と同じとする 20090317
        //権限指定なし／一般ユーザー：ユーザー所属の部署一覧
        if($priority_auth == "" || $priority_auth == "GU")
        {
            $emp_class_post = get_emp_class_post($emp_id, $con, $fname);
        }
        //権限指定あり：担当者となる部署一覧
        else
        {
            //指定ユーザーが、指定権限で担当者となる部署の一覧を取得し、先頭の部署を使用
            //担当範囲
            if($report_db_read_auth_flg)
            {
                $emp_class_post = get_emp_auth_class_post($emp_id, $priority_auth, $con, $fname);
            }
            //所属範囲
            else
            {
                $emp_class_post = get_emp_class_post($emp_id, $con, $fname);
            }
        }
        $emp_class = $emp_class_post[0]["emp_class"];
        $emp_attribute = $emp_class_post[0]["emp_attribute"];
        $emp_dept = $emp_class_post[0]["emp_dept"];
        $emp_room = $emp_class_post[0]["emp_room"];

        //所属検索初期条件を設定
        if($report_db_read_div > 0) {
            $search_emp_class = $emp_class;
        }
        if($report_db_read_div > 1) {
            $search_emp_attribute = $emp_attribute;
        }
        if($report_db_read_div > 2) {
            $search_emp_dept = $emp_dept;
        }
        if($report_db_read_div > 3) {
            $search_emp_room = $emp_room;
        }
    }
}

//==============================
//レポート一覧取得
//==============================
$where = "where not del_flag = 't' and not shitagaki_flg = 't' and not kill_flg";

//日付による絞込み(フィールド特定)
if($search_date_target == "incident_date")
{
    $search_date_field = "input_item_100_5";
}
elseif($search_date_target == "evaluation_date")
{
    $search_date_field = "input_item_620_33";
}
else
{
    $search_date_field = "registration_date";
}

//日付による絞込み
if($search_date_mode == "month")
{
    $search_date_1 = $search_date_year."/".$search_date_month;
    $where .= " and $search_date_field like '$search_date_1%' ";
}
elseif($search_date_mode == "ymd_st_ed")
{
    if($search_date_ymd_st != "")
    {
        $where .= " and $search_date_field >= '$search_date_ymd_st' ";
    }
    if($search_date_ymd_ed != "")
    {
        $where .= " and $search_date_field <= '$search_date_ymd_ed' ";
    }
}
elseif($search_date_mode == "non")
{
    //絞り込み条件なし。
}
else
{
    if($search_date_late_mode == "1month")
    {
        //最近１ヶ月(先月の今日以降)
        $search_date_y = date("Y");
        $search_date_m = date("n");
        $search_date_d = date("d");

        $search_date_m--;
        if($search_date_m <= 0)
        {
            $search_date_y--;
            $search_date_m = 12;
        }
        if($search_date_m <= 9)
        {
            $search_date_m = "0".$search_date_m;
        }
    }
    elseif($search_date_late_mode == "2week")
    {
        //最近２週間(先々週の今日以降)
        $date_today = mktime (0, 0, 0, date("m"), date("d"),  date("y"));
        $date_2week = $date_today - 1209600;//2w * (7d/w) * (24h/d) * (60m/h) * (60s/m) = 1209600s
        $search_date_y = date('Y', $date_2week);
        $search_date_m = date('n', $date_2week);
        $search_date_d = date('d', $date_2week);
        if($search_date_m <= 9)
        {
            $search_date_m = "0".$search_date_m;
        }
    }
    else//all
    {
        //過去全件の場合:何もしない。(絞り込まない)

        //報告日以外の場合は入力ありのみ絞り込む(最過去)
        if($search_date_target != "report_date")
        {
            $search_date_y = '0000';
            $search_date_m = '00';
            $search_date_d = '00';
        }
    }

    if($search_date_target != "report_date" || $search_date_late_mode != "all")
    {
        $search_date_1 = $search_date_y."/".$search_date_m."/".$search_date_d;
        $where .= " and (not $search_date_field isnull) and ($search_date_field <> '') and ($search_date_field > '$search_date_1') ";
    }

}

//検索条件による絞込み
$serch_emp_target = "report";
if($search_emp_class != "")
{
    $where .= " and registrant_class = $search_emp_class ";
}
if($search_emp_attribute != "")
{
    $where .= " and registrant_attribute = $search_emp_attribute ";
}
if($search_emp_dept != "")
{
    $where .= " and registrant_dept = $search_emp_dept ";
}
if($search_emp_room != "")
{
    $where .= " and registrant_room = $search_emp_room ";
}
if($search_emp_job != "")
{
    $where .= " and ".$serch_emp_target."_emp_job = $search_emp_job ";
}
if($search_emp_name != "")
{
    $search_emp_name2 = str_replace(" ", "", pg_escape_string($search_emp_name));
    $search_emp_name2 = str_replace("　", "", $search_emp_name2);
    $where .= " and ";
    $where .= " ( ";
    $where .= "   ( ";
    $where .= "     anonymous_report_flg ";
    $where .= "     and ";
    $where .= "     '".get_anonymous_name($con,$fname)."' like '%$search_emp_name2%' ";
    $where .= "   ) ";
    $where .= "   or ";
    $where .= "   ( ";
    $where .= "     not anonymous_report_flg ";
    $where .= "     and  ";
    $where .= "       ( ";
    $where .= "            ".$serch_emp_target."_emp_lt_nm like '%$search_emp_name2%' ";
    $where .= "         or ".$serch_emp_target."_emp_ft_nm like '%$search_emp_name2%' ";
    $where .= "         or ".$serch_emp_target."_emp_kn_lt_nm like '%$search_emp_name2%' ";
    $where .= "         or ".$serch_emp_target."_emp_kn_ft_nm like '%$search_emp_name2%' ";
    $where .= "         or (".$serch_emp_target."_emp_lt_nm || ".$serch_emp_target."_emp_ft_nm) like '%$search_emp_name2%' ";
    $where .= "         or (".$serch_emp_target."_emp_kn_lt_nm || ".$serch_emp_target."_emp_kn_ft_nm) like '%$search_emp_name2%' ";
    $where .= "       ) ";
    $where .= "   ) ";
    $where .= " ) ";
}

if($search_patient_id != "")
{
    $search_patient_id2 = pg_escape_string($search_patient_id);
    $where .= " and input_item_210_10 = '$search_patient_id2' ";
}

if($search_patient_name != "")
{
    $search_patient_name2 = pg_escape_string($search_patient_name);
    $where .= " and input_item_210_20 like '%$search_patient_name2%' ";
}

if($search_patient_year_from == "900")
{
    $where .= " and input_item_210_40 = '$search_patient_year_from' ";
}
else
{
    if($search_patient_year_from != "" && $search_patient_year_to != "")
    {
        $where .= " and input_item_210_40 >= '$search_patient_year_from' ";
        $where .= " and input_item_210_40 <= '$search_patient_year_to' ";
    }
    else if($search_patient_year_from != "" && $search_patient_year_to == "")
    {
        $where .= " and input_item_210_40 >= '$search_patient_year_from' ";
        $where .= " and input_item_210_40 < '900' ";
    }
    else if($search_patient_year_from == "" && $search_patient_year_to != "")
    {
        $where .= " and input_item_210_40 <= '$search_patient_year_to' ";
    }
}

if($search_report_no != "")
{
    $search_report_no2 = pg_escape_string($search_report_no);
    $where .= " and report_no like '{$search_report_no2}%' ";
}

if($search_evaluation_date != "")
{
    if($search_evaluation_date == "today")
    {
        $evaluation_date_add = 0;
    }
    elseif($search_evaluation_date == "tomorrow")
    {
        $evaluation_date_add = 86400;//1d * (24h/d) * (60m/h) * (60s/m) = 86400s
    }
    elseif($search_evaluation_date == "1week")
    {
        $evaluation_date_add = 604800;//1w * (7d/w) * (24h/d) * (60m/h) * (60s/m) = 604800s
    }
    elseif($search_evaluation_date == "2week")
    {
        $evaluation_date_add = 1209600;//2w * (7d/w) * (24h/d) * (60m/h) * (60s/m) = 1209600s
    }
    $date_today = mktime (0, 0, 0, date("m"), date("d"),  date("y"));
    $evaluation_date_max_time = $date_today + $evaluation_date_add;
    $evaluation_date_max_y = date('Y', $evaluation_date_max_time);
    $evaluation_date_max_m = date('n', $evaluation_date_max_time);
    $evaluation_date_max_d = date('d', $evaluation_date_max_time);
    if($evaluation_date_max_m <= 9)
    {
        $evaluation_date_max_m = "0".$evaluation_date_max_m;
    }
    $evaluation_date_max = $evaluation_date_max_y."/".$evaluation_date_max_m."/".$evaluation_date_max_d;

    if($search_evaluation_date == "today" || $search_evaluation_date == "tomorrow")
    {
        $evaluation_date_min = $evaluation_date_max;
    }
    else
    {
        $evaluation_date_min_y = date('Y');
        $evaluation_date_min_m = date('n');
        $evaluation_date_min_d = date('d');
        if($evaluation_date_min_m <= 9)
        {
            $evaluation_date_min_m = "0".$evaluation_date_min_m;
        }
        $evaluation_date_min = $evaluation_date_min_y."/".$evaluation_date_min_m."/".$evaluation_date_min_d;
    }

    $where .= " and input_item_620_33 >= '$evaluation_date_min' and input_item_620_33 <= '$evaluation_date_max' ";
}

if($search_torey_date_in_search_area_start != "")
{
    $where .= " and registration_date >= '$search_torey_date_in_search_area_start' ";
}
if($search_torey_date_in_search_area_end != "")
{
    $where .= " and registration_date <= '$search_torey_date_in_search_area_end' ";
}


if($search_incident_date_start != "")
{
    $where .= " and input_item_100_5 >= '$search_incident_date_start'";
}
if($search_incident_date_end != "")
{
    $where .= " and input_item_100_5 <= '$search_incident_date_end'";
}



if($sort_div == "0")
{
    $sort = "asc";
}
else
{
    $sort = "desc";
}


$order = "order by report_id desc";
if($search_date_mode == "non")
{
    if($sort_item == "INCIDENT_DATE")
    {
        $sort_item = "REPORT_NO";
    }
    elseif($sort_item == "EVALUATION_DATE" && $search_evaluation_date == "")
    {
        $sort_item = "REPORT_NO";
    }
}
switch($sort_item)
{
    case "REPORT_NO":
        $order = "order by report_no $sort";
        break;
    case "REPORT_TITLE":
        $order = "order by report_title $sort, report_id desc";
        break;
    case "CLASS":
        $order = "order by registrant_class $sort, registrant_attribute $sort , registrant_dept $sort , registrant_room, report_id desc";
        break;
    case "REGIST_NAME":
        $order = "order by anonymous_report_flg $sort, report_emp_kn_lt_nm || report_emp_kn_ft_nm $sort, report_id desc";
        break;
    case "REGIST_DATE":
        $order = "order by registration_date $sort, report_id desc";
        break;
    case "EVALUATION_DATE":
        $order = "order by input_item_620_33 $sort, report_id desc";
        break;
    case "INCIDENT_DATE":
        $order = "order by input_item_100_5 $sort, report_id desc";
        break;
    case "LEVEL":
        $order = "order by level_num $sort ,report_id desc";
        break;
    case "SM_START":
        $order = "order by  sm_start_flg $sort, report_id desc";
        break;
    case "SM_END":
        $order = "order by  sm_end_flg $sort, report_id desc";
        break;
    case "RM_START":
        $order = "order by  rm_start_flg $sort, report_id desc";
        break;
    case "RM_END":
        $order = "order by  rm_end_flg $sort, report_id desc";
        break;
    case "RA_START":
        $order = "order by  ra_start_flg $sort, report_id desc";
        break;
    case "RA_END":
        $order = "order by  ra_end_flg $sort, report_id desc";
        break;
    case "SD_START":
        $order = "order by  sd_start_flg $sort, report_id desc";
        break;
    case "SD_END":
        $order = "order by  sd_end_flg $sort, report_id desc";
        break;
    case "MS_START":
        $order = "order by  md_start_flg $sort, report_id desc";
        break;
    case "MS_END":
        $order = "order by  md_end_flg $sort, report_id desc";
        break;
    case "HD_START":
        $order = "order by  hd_start_flg $sort, report_id desc";
        break;
    case "HD_END":
        $order = "order by  hd_end_flg $sort, report_id desc";
        break;
        
    default:
        //何もしない。
        break;
}



$fields = array(
            'report_id',
            'report_title',
//          'job_id',
//          'registrant_id',
            'registrant_name',
            'registration_date',
//          'eis_id',
//          'eid_id',
            'del_flag',
            'report_no',
            'shitagaki_flg',
//          'report_comment',
//          'edit_lock_flg',
            'anonymous_report_flg',
            'registrant_class',
            'registrant_attribute',
            'registrant_dept',
            'registrant_room',
            'kill_flg',

//          'report_emp_class',
//          'report_emp_attribute',
//          'report_emp_dept',
//          'report_emp_room',
//          'report_emp_job',
//          'report_emp_lt_nm',
//          'report_emp_ft_nm',
            'report_emp_kn_lt_nm',
            'report_emp_kn_ft_nm',

//          '[auth]_update_emp_id',
            '[auth]_start_flg',
            '[auth]_end_flg',
//          '[auth]_start_date',
//          '[auth]_end_date',
            '[auth]_use_flg',

//          'mail_id',
//          'subject',
//          'send_emp_id',
//          'send_emp_name',
//          'send_date',
//          'send_del_flg',
//          'send_job_id',
//          'send_eis_id',
//          'anonymous_send_flg',
//          'marker',
//
//          'send_emp_class',
//          'send_emp_attribute',
//          'send_emp_dept',
//          'send_emp_room',
//          'send_emp_job',
//          'send_emp_lt_nm',
//          'send_emp_ft_nm',
//          'send_emp_kn_lt_nm',
//          'send_emp_kn_ft_nm',

//          'recv_emp_id',
//          'recv_emp_name',
//          'recv_class',
//          'recv_class_disp_index',
//          'recv_read_flg',
//          'recv_read_flg_update_date',
//          'recv_del_flg',
//          'anonymous_recv_flg',
//
//          'recv_emp_class',
//          'recv_emp_attribute',
//          'recv_emp_dept',
//          'recv_emp_room',
//          'recv_emp_job',
//          'recv_emp_lt_nm',
//          'recv_emp_ft_nm',
//          'recv_emp_kn_lt_nm',
//          'recv_emp_kn_ft_nm',

//          'report_link_id',
//          'report_link_status',
//          'main_report_id',
            'level',
            'level_num'
    );
if($search_emp_class != "" || $search_emp_attribute != "" || $search_emp_dept != "" || $search_emp_room != "" || $search_emp_job != "" || $search_emp_name != "" ||
    $search_patient_id != "" || $search_patient_name != "" || $search_patient_year_from != "" || $search_patient_year_to != "" || $search_evaluation_date != "" || $search_incident_date_start != "" || $search_incident_date_end != "")
{
//  $fields[] = 'registrant_class';
//  $fields[] = 'registrant_attribute';
//  $fields[] = 'registrant_dept';
//  $fields[] = 'registrant_room';
    $fields[] = 'report_emp_job';
    $fields[] = 'report_emp_lt_nm';
    $fields[] = 'report_emp_ft_nm';
    $fields[] = 'report_emp_kn_lt_nm';
    $fields[] = 'report_emp_kn_ft_nm';
    $fields[] = 'input_item_210_10';
    $fields[] = 'input_item_210_20';
    $fields[] = 'input_item_210_40';
    $fields[] = 'input_item_620_33';
    $fields[] = 'input_item_100_5';
}
if($search_date_target == 'incident_date' && ! in_array('input_item_100_5',$fields))
{
    $fields[] = 'input_item_100_5';
}
if($search_date_target == 'evaluation_date' && ! in_array('input_item_620_33',$fields))
{
    $fields[] = 'input_item_620_33';
}

$list_data_array = get_report_db_list($con,$fname,$where,$order,$fields);

//==============================
//Excelリスト出力用フォルダー内レポート一覧情報
//==============================

$excel_target_report_id_list = "";
if($list_data_array)
{
    foreach($list_data_array as $list_data)
    {
        $tmp_report_id =  $list_data["report_id"];
        if($excel_target_report_id_list != "")
        {
            $excel_target_report_id_list .= ",";
        }
        $excel_target_report_id_list .= $tmp_report_id;
    }
}

//==============================
//ページングに関する情報
//==============================

//pate省略時は先頭ページを対象とする。
if($page == "")
{
    $page = 1;
}

//一画面内の最大表示件数
$disp_count_max_in_page = 20;

//表示開始/終了インデックス
$disp_start_index = ($page - 1)*$disp_count_max_in_page;
$disp_end_index   = $disp_start_index + $disp_count_max_in_page - 1;

//全データ件数
$rec_count = count($list_data_array);
if($rec_count == 1 && $list_data_array[0] == "")
{
    $rec_count = 0;
}

//最大ページ数
if($rec_count == 0)
{
    $page_max  = 1;
}
else
{
    $page_max  = floor( ($rec_count-1) / $disp_count_max_in_page ) + 1;
}

//==============================
//ページングによる絞込み
//==============================

$list_data_array_work = array();
for($i=$disp_start_index;$i<=$disp_end_index;$i++)
{
    if($i >= $rec_count)
    {
        break;
    }
    $list_data_array_work[] = $list_data_array[$i];
}
$list_data_array = $list_data_array_work;

//==============================
//インシレベルの表示
//==============================
$is_inci_level_disp = is_inci_level_disp($session,$fname,$con);

//==============================
//インシデント権限マスタ取得
//==============================
$arr_inci = get_inci_mst($con, $fnme);

//SM表示フラグ取得/SM除去
$sm_disp_flg = get_sm_disp_flg($fname, $con);
if($sm_disp_flg != "t")
{
    $wk_arr_inci = null;
    foreach($arr_inci as $auth => $auth_info)
    {
        if($auth != "SM")
        {
            $wk_arr_inci[$auth] = $auth_info;
        }
    }
    $arr_inci = $wk_arr_inci;
}


//==============================
//患者影響レベル略称情報を取得
//==============================
$level_disp_list = get_inci_level_short_name($con,$fname);


//==============================
//HTML出力
//==============================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$INCIDENT_TITLE?> | 報告ファイル</title>
<?
show_rp_torey_common_header_html($session,$fname,$con);

// ポップアップ表示用JavaScript
$anonymous_setting = get_anonymous_setting($session, $con, $fname);
show_anonymous_emp_name_popup_js();

?>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">

<!-- BODY全体 START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<!-- ヘッダー START -->
<?
show_hiyari_header($session,$fname,false);
?>
<!-- ヘッダー END -->

<!-- レポーティング本体 START -->
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="200" valign="top" bgcolor="#D5F4D8">


<!-- トレイ選択 START -->
<?
$torey_select_area_param_list = array();
$torey_select_area_param_list["search_date_target"] = $search_date_target;
$torey_select_area_param_list["search_date_mode"] = $search_date_mode;
$torey_select_area_param_list["search_date_late_mode"] = $search_date_late_mode;
$torey_select_area_param_list["search_date_year"] = $search_date_year;
$torey_select_area_param_list["search_date_month"] = $search_date_month;
$torey_select_area_param_list["search_date_ymd_st"] = $search_date_ymd_st;
$torey_select_area_param_list["search_date_ymd_ed"] = $search_date_ymd_ed;
show_rp_torey_select_area($session,$fname,$con,$torey_select_area_param_list);
?>
<!-- トレイ選択 END -->

</td>
<td width="10"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td valign="top">

<!-- 実行アイコン、スクリプト START -->
<?
show_rp_exe_icon_area($session,$fname,$con);
?>
<!-- 実行アイコン、スクリプト END -->



<!-- 検索領域 START -->
<?
$search_area_param_list = array();
$search_area_param_list["search_emp_class"] = $search_emp_class;
$search_area_param_list["search_emp_attribute"] = $search_emp_attribute;
$search_area_param_list["search_emp_dept"] = $search_emp_dept;
$search_area_param_list["search_emp_room"] = $search_emp_room;
$search_area_param_list["search_emp_job"] = $search_emp_job;
$search_area_param_list["search_emp_name"] = $search_emp_name;
$search_area_param_list["search_patient_id"] = $search_patient_id;
$search_area_param_list["search_patient_name"] = $search_patient_name;
$search_area_param_list["search_patient_year_from"] = $search_patient_year_from;
$search_area_param_list["search_patient_year_to"] = $search_patient_year_to;
$search_area_param_list["search_toggle_div"] = $search_toggle_div;
$search_area_param_list["search_report_no"] = $search_report_no;
$search_area_param_list["search_evaluation_date"] = $search_evaluation_date;
$search_area_param_list["search_torey_date_in_search_area_start"] = $search_torey_date_in_search_area_start;
$search_area_param_list["search_torey_date_in_search_area_end"] = $search_torey_date_in_search_area_end;
$search_area_param_list["search_incident_date_start"] = $search_incident_date_start;
$search_area_param_list["search_incident_date_end"] = $search_incident_date_end;
show_rp_torey_search_area($session,$fname,$con,$search_area_param_list);
?>
<!-- 検索領域 END -->



<!-- エクセル出力用領域 START -->
<form name="excel_form" method="post" action="hiyari_rp_report_excel.php">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="report_id" value="">
</form>
<form name="list_excel_form" method="post" action="hiyari_excel_output_item.php" target="list_excel_window">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="report_id_list" value="<?=$excel_target_report_id_list?>">
</form>
<!-- エクセル出力用領域 END -->



<!-- 一覧 START -->
<form name="list_form" method="post" action="hiyari_rpt_report_db.php">
<input type="hidden" name="is_postback" value="true">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="mode" value="">
<input type="hidden" name="search_date_target" value="<?=$search_date_target?>">
<input type="hidden" name="search_date_mode" value="<?=$search_date_mode?>">
<input type="hidden" name="search_date_late_mode" value="<?=$search_date_late_mode?>">
<input type="hidden" name="search_date_year" value="<?=$search_date_year?>">
<input type="hidden" name="search_date_month" value="<?=$search_date_month?>">
<input type="hidden" name="search_date_ymd_st" value="<?=$search_date_ymd_st?>">
<input type="hidden" name="search_date_ymd_ed" value="<?=$search_date_ymd_ed?>">
<input type="hidden" name="search_emp_class" value="<?=$search_emp_class?>">
<input type="hidden" name="search_emp_attribute" value="<?=$search_emp_attribute?>">
<input type="hidden" name="search_emp_dept" value="<?=$search_emp_dept?>">
<input type="hidden" name="search_emp_room" value="<?=$search_emp_room?>">
<input type="hidden" name="search_emp_job" value="<?=$search_emp_job?>">
<input type="hidden" name="search_emp_name" value="<?=$search_emp_name?>">
<input type="hidden" name="search_patient_id" value="<?=$search_patient_id?>">
<input type="hidden" name="search_patient_name" value="<?=$search_patient_name?>">
<input type="hidden" name="search_patient_year_from" value="<?=$search_patient_year_from?>">
<input type="hidden" name="search_patient_year_to" value="<?=$search_patient_year_to?>">
<input type="hidden" name="search_toggle_div" value="<?=$search_toggle_div?>">

<input type="hidden" name="search_report_no" value="<?=$search_report_no?>">
<input type="hidden" name="search_evaluation_date" value="<?=$search_evaluation_date?>">

<input type="hidden" name="search_torey_date_in_search_area_start" value="<?=$search_torey_date_in_search_area_start?>">
<input type="hidden" name="search_torey_date_in_search_area_end" value="<?=$search_torey_date_in_search_area_end?>">
<input type="hidden" name="search_incident_date_start" value="<?=$search_incident_date_start?>">
<input type="hidden" name="search_incident_date_end" value="<?=$search_incident_date_end?>">

<input type="hidden" name="search_title" value="<?=$search_title?>">
<input type="hidden" name="search_incident" value="<?=$search_incident?>">
<input type="hidden" name="title_and_or_div" value="<?=$title_and_or_div?>">
<input type="hidden" name="incident_and_or_div" value="<?=$incident_and_or_div?>">

<input type="hidden" name="page" value="<?=$page?>">
<input type="hidden" name="sort_item" value="<?=$sort_item?>">
<input type="hidden" name="sort_div" value="<?=$sort_div?>">
<table width="100%" border="0" cellspacing="0" cellpadding="3" class="list_2">
  <tr>
<?
$profile_use = get_inci_profile($fname, $con);
?>
    <td rowspan="2" bgcolor="#FFFBE7" width="25">&nbsp;</td>
    <td rowspan="2" align="center" bgcolor="#FFFBE7"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="javascript:set_report_list_sort('REPORT_NO');">事案番号</a></font></td>
    <td rowspan="2" align="center" bgcolor="#FFFBE7"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="javascript:set_report_list_sort('REPORT_TITLE');">表題</a></font></td>
    <td rowspan="2" align="center" bgcolor="#FFFBE7"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="javascript:set_report_list_sort('CLASS');">報告部署</a></font></td>
    <td rowspan="2" align="center" bgcolor="#FFFBE7"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="javascript:set_report_list_sort('REGIST_NAME');">報告者</a></font></td>


    <? if($search_date_mode != "non"){?>
        <? if($search_date_target == "incident_date"){ ?>
    <td rowspan="2" align="center" bgcolor="#FFFBE7"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="javascript:set_report_list_sort('INCIDENT_DATE');">発生日</a></font></td>
        <? }elseif($search_date_target == "evaluation_date"){ ?>
    <td rowspan="2" align="center" bgcolor="#FFFBE7"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="javascript:set_report_list_sort('EVALUATION_DATE');">評価予定日</a></font></td>
        <? }else{ ?>
    <td rowspan="2" align="center" bgcolor="#FFFBE7"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="javascript:set_report_list_sort('REGIST_DATE');">報告日</a></font></td>
        <? } ?>
    <? }else{ ?>
        <? if($search_evaluation_date == ""){ ?>
    <td rowspan="2" align="center" bgcolor="#FFFBE7"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="javascript:set_report_list_sort('REGIST_DATE');">報告日</a></font></td>
        <? }else{ ?>
    <td rowspan="2" align="center" bgcolor="#FFFBE7"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="javascript:set_report_list_sort('EVALUATION_DATE');">評価予定日</a></font></td>
        <? } ?>
    <? } ?>




    <?
    if($is_inci_level_disp)
    {
    ?>
    <td rowspan="2" align="center" bgcolor="#FFDDFD" width="40"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="javascript:set_report_list_sort('LEVEL');">影<BR>響</a></font></td>
    <?
    }
    ?>
    <?
    $disp_auth_count = 0;
    foreach($arr_inci as $arr_key => $arr)
    {
        if(is_usable_auth($arr_key,$fname,$con))
        {
            $disp_auth_count++;
    ?>
    <td colspan="2" align="center" bgcolor="#FFDDFD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><?=h($arr['auth_short_name'])?></font></td>
    <?
        }
    }
    ?>
  </tr>
  <tr>
<?
$auth_key_list = array_keys($arr_inci);
for($i = 0; $i < $disp_auth_count; $i++)
{
?>
    <td bgcolor="#FFDDFD" align="center" width="20"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="javascript:set_report_list_sort('<?=$auth_key_list[$i]?>_START');">受<br>付</a></font></td>
    <td bgcolor="#FFDDFD" align="center" width="20"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="javascript:set_report_list_sort('<?=$auth_key_list[$i]?>_END');">確<br>認</a></font></td>
<?
}
?>
  </tr>
<?

foreach($list_data_array as $list_data)
{
    $mail_id =  $list_data["mail_id"];

    $report_id =  $list_data["report_id"];

    $list_id = $report_id;

    $title = $list_data["report_title"];
    $title = h($title);

    $anonymous_report_flg =  $list_data["anonymous_report_flg"];
    $create_emp_name =  $list_data["registrant_name"];

    $create_date =  $list_data["registration_date"];

    $level = $list_data["level"];
    if($level != "")
    {
        $level = $level_disp_list[$level];
    }

    $progress_comment_use_flg = get_progress_comment_use($con,$fname);
    $non_progress_style = "style='background-color:#C0C0C0'";
    $sm_area_style =  ($list_data["sm_use_flg"]   == "t") ? ""  :$non_progress_style;
    $rm_area_style =  ($list_data["rm_use_flg"]   == "t") ? ""  :$non_progress_style;
    $sd_area_style =  ($list_data["sd_use_flg"]   == "t") ? ""  :$non_progress_style;
    $md_area_style =  ($list_data["md_use_flg"]   == "t") ? ""  :$non_progress_style;
    $hd_area_style =  ($list_data["hd_use_flg"]   == "t") ? ""  :$non_progress_style;
    $sm_start_mark =  ($list_data["sm_start_flg"] == "t") ? "○":"&nbsp";
    $sm_end_mark   =  ($list_data["sm_end_flg"]   == "t") ? ( $progress_comment_use_flg ? "<a href=\"javascript:rp_action_progress_comment_list($report_id,'SM')\">○</a>" : "○" ):"&nbsp";
    $rm_start_mark =  ($list_data["rm_start_flg"] == "t") ? "○":"&nbsp";
    $rm_end_mark   =  ($list_data["rm_end_flg"]   == "t") ? ( $progress_comment_use_flg ? "<a href=\"javascript:rp_action_progress_comment_list($report_id,'RM')\">○</a>" : "○" ):"&nbsp";
    $ra_start_mark =  ($list_data["ra_start_flg"] == "t") ? "○":"&nbsp";
    $ra_end_mark   =  ($list_data["ra_end_flg"]   == "t") ? ( $progress_comment_use_flg ? "<a href=\"javascript:rp_action_progress_comment_list($report_id,'RA')\">○</a>" : "○" ):"&nbsp";
    $sd_start_mark =  ($list_data["sd_start_flg"] == "t") ? "○":"&nbsp";
    $sd_end_mark   =  ($list_data["sd_end_flg"]   == "t") ? ( $progress_comment_use_flg ? "<a href=\"javascript:rp_action_progress_comment_list($report_id,'SD')\">○</a>" : "○" ):"&nbsp";
    $md_start_mark =  ($list_data["md_start_flg"] == "t") ? "○":"&nbsp";
    $md_end_mark   =  ($list_data["md_end_flg"]   == "t") ? ( $progress_comment_use_flg ? "<a href=\"javascript:rp_action_progress_comment_list($report_id,'MD')\">○</a>" : "○" ):"&nbsp";
    $hd_start_mark =  ($list_data["hd_start_flg"] == "t") ? "○":"&nbsp";
    $hd_end_mark   =  ($list_data["hd_end_flg"]   == "t") ? ( $progress_comment_use_flg ? "<a href=\"javascript:rp_action_progress_comment_list($report_id,'HD')\">○</a>" : "○" ):"&nbsp";

    $report_no = $list_data["report_no"];

    $class_nm = "";
    if($profile_use["class_flg"] == "t" && $list_data["registrant_class"] != "")
    {
        $class_nm .= get_class_nm($con,$list_data["registrant_class"],$fname);
    }
    if($profile_use["attribute_flg"] == "t" && $list_data["registrant_attribute"] != "")
    {
        $class_nm .= get_atrb_nm($con,$list_data["registrant_attribute"],$fname);
    }
    if($profile_use["dept_flg"] == "t" && $list_data["registrant_dept"] != "")
    {
        $class_nm .= get_dept_nm($con,$list_data["registrant_dept"],$fname);
    }
    if($profile_use["room_flg"] == "t" && $list_data["registrant_room"] != "")
    {
        $class_nm .= get_room_nm($con,$list_data["registrant_room"],$fname);
    }

    //評価予定日 ※通常は取得できない。
    $evaluation_date = $list_data["input_item_620_33"];
    //発生日 ※通常は取得できない。
    $incident_date = $list_data["input_item_100_5"];

    ?>
  <tr>
    <td align="center">
    <input type="checkbox" name="list_select[]" value="<?=$list_id?>">
    <input type="hidden" id="report_id_of_list_id_<?=$list_id?>" value="<?=$report_id?>">
    </td>
    <td nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><?=$report_no?></font></td>
    <td><a href="javascript:rp_action_update_from_report_id('<?=$report_id?>');">
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><?=$title?></font></a></td>
    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><?=$class_nm?></font></td>



    <td>
    <?php
    $mouseover_popup_flg = false;
    // セーフティマネージャで一部匿名の場合 → 実名が見える
    if(is_inci_auth_emp_of_auth($session,$fname,$con,"SM") and !empty($anonymous_setting)) {
        $mouseover_popup_flg = true;
    }
    else {
        foreach ($anonymous_setting as $auth => $data) {
            if ($data["report_db_use_flg"] == 't' and $data["report_db_read_div"] == 0) {
                    $mouseover_popup_flg = true;
            }
            else if ($data["report_db_use_flg"] == 't' and $data["report_db_read_div"] == 1) {
                if ($list_data["registrant_class"] == $emp["emp_class"] and $list_data["registrant_attribute"] == $emp["emp_attribute"]) {
                    $mouseover_popup_flg = true;
                }
            }
            else if ($data["report_db_use_flg"] == 't' and $data["report_db_read_div"] == 2) {
                if ($list_data["registrant_class"] == $emp["emp_class"] and $list_data["registrant_attribute"] == $emp["emp_attribute"]) {
                    $mouseover_popup_flg = true;
                }
            }
            else if ($data["report_db_use_flg"] == 't' and $data["report_db_read_div"] == 3) {
                if ($list_data["registrant_class"] == $emp["emp_class"] and $list_data["registrant_attribute"] == $emp["emp_attribute"] and $list_data["registrant_dept"] == $emp["emp_dept"]) {
                    $mouseover_popup_flg = true;
                }
            }
            if ($data["report_db_read_auth_flg"] == 't') {
                if ($list_data["registrant_room"]) {
                    if ($data["rm"][ $list_data["registrant_class"] ][ $list_data["registrant_attribute"] ][ $list_data["registrant_dept"] ][ $list_data["registrant_room"] ]) {
                        $mouseover_popup_flg = true;
                    }
                }
                else {
                    if ($data["rm"][ $list_data["registrant_class"] ][ $list_data["registrant_attribute"] ][ $list_data["registrant_dept"] ]) {
                        $mouseover_popup_flg = true;
                    }
                }
            }
        }
    }
    show_anonymous_emp_name($con,$fname,$create_emp_name,$anonymous_report_flg,$mouseover_popup_flg,"regist");
    ?>
    </td>




    <? if($search_date_mode != "non"){?>
        <? if($search_date_target == "incident_date"){ ?>
    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><?=$incident_date?></font></td>
        <? }elseif($search_date_target == "evaluation_date"){ ?>
    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><?=$evaluation_date?></font></td>
        <? }else{ ?>
    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><?=$create_date?></font></td>
        <? } ?>
    <? }else{ ?>
        <? if($search_evaluation_date == ""){ ?>
    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><?=$create_date?></font></td>
        <? }else{ ?>
    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><?=$evaluation_date?></font></td>
        <? } ?>
    <? } ?>


    <?
    if($is_inci_level_disp)
    {
    ?>
    <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><?=$level?></font></td>
    <?
    }
    ?>

    <?
    foreach($arr_inci as $arr_key => $arr_val) {
        switch ($arr_key){
            case "SM":
                $auth_area_style = $sm_area_style;
                $auth_start_mark = $sm_start_mark;
                $auth_end_mark   = $sm_end_mark;
                break;
            case "RM":
                $auth_area_style = $rm_area_style;
                $auth_start_mark = $rm_start_mark;
                $auth_end_mark   = $rm_end_mark;
                break;
            case "RA":
                $auth_area_style = $ra_area_style;
                $auth_start_mark = $ra_start_mark;
                $auth_end_mark   = $ra_end_mark;
                break;
            case "SD":
                $auth_area_style = $sd_area_style;
                $auth_start_mark = $sd_start_mark;
                $auth_end_mark   = $sd_end_mark;
                break;
            case "MD":
                $auth_area_style = $md_area_style;
                $auth_start_mark = $md_start_mark;
                $auth_end_mark   = $md_end_mark;
                break;
            case "HD":
                $auth_area_style = $hd_area_style;
                $auth_start_mark = $hd_start_mark;
                $auth_end_mark   = $hd_end_mark;
                break;
            default:
        }

        if(is_usable_auth($arr_key,$fname,$con))
        {
        ?>
        <td align="center" <?=$auth_area_style?> ><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><?=$auth_start_mark?></font></td>
        <td align="center" <?=$auth_area_style?> ><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><?=$auth_end_mark?></font></td>
        <?
        }
    }
    ?>

  </tr>
    <?
}
?>
</table>
</form>
<!-- 一覧 END -->


<!-- ページ選択 START -->
<?
show_rp_page_select_area($session,$fname,$con,$page_max,$page)
?>
<!-- ページ選択 END -->




</td>
</tr>
</table>
<!-- レポーティング本体 END -->

<!-- フッター START -->
<?
show_hiyari_footer($session,$fname,false);
?>
<!-- フッター END -->

</td>
</tr>
</table>
<!-- BODY全体 END -->

</body>
</html>
<?


//==============================
//DBコネクション終了
//==============================
pg_close($con);
