<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 掲示板 | 投稿一覧</title>
<?
require_once("Cmx.php");
require("about_session.php");
require("about_authority.php");
require("get_values.ini");
require("show_bbsthread.ini");
require_once("aclg_set.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$check_auth = check_authority($session, 1, $fname);
if ($check_auth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 掲示板管理権限を取得
$bbs_admin_auth = check_authority($session, 62, $fname);

// 表示順のデフォルトを「ツリー表示（新しい順）」とする
if ($sort == "") {$sort = "1";}

// データベースに接続
$con = connect2db($fname);
// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// ログインユーザの職員IDを取得
$emp_id = get_emp_id($con, $session, $fname);

// オプション設定情報を取得
$sql = "select newpost_days, count_per_page from bbsoption";
$cond = "where emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if (pg_num_rows($sel) > 0) {
	$newpost_days = pg_fetch_result($sel, 0, "newpost_days");
	$count_per_page = pg_fetch_result($sel, 0, "count_per_page");
}
if ($newpost_days == "") {$newpost_days = 3;}
if ($count_per_page == "") {$count_per_page = 0;}

// オプション設定（管理者）情報を取得
$sql = "select allow_category, allow_theme from bbsadmopt";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$allow_category = pg_fetch_result($sel, 0, "allow_category");
$allow_theme = pg_fetch_result($sel, 0, "allow_theme");

// カテゴリ一覧を取得
$categories = get_categories($con, $emp_id, $category_id, $theme_id, $fname);

// for test
/*
$categories = array();
$category_id = "";
$theme_id = "";
*/

// 投稿可能フラグの設定
$category_postable = $categories[$category_id]["postable"];
$theme_postable = $categories[$category_id]["themes"][$theme_id]["postable"];
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script src="js/prototype/dist/prototype.js"></script>
<script type="text/javascript" src="js/yui/build/yahoo/yahoo-min.js"></script>
<script type="text/javascript" src="js/yui/build/treeview/treeview-min.js"></script>
<link rel="stylesheet" type="text/css" href="js/yui/build/treeview/assets/skins/sam/treeview.css">
<script type="text/javascript">
function initPage() {
	var tree = new YAHOO.widget.TreeView('sam');
	var root = tree.getRoot();
<?
	$comp_date = date("Ymd", strtotime("-" . ($newpost_days - 1) . " day", mktime(0, 0, 0, date("m"), date("d"), date("Y"))));

	while ($tmp_data = each($categories)) {
		$tmp_category_id = $tmp_data["key"];
		$tmp_category = $tmp_data["value"];
		$tmp_category_title = $tmp_category["title"];
		$tmp_category_date = $tmp_category["date"];
		$tmp_category_count = $tmp_category["count"];
		$tmp_category_reg_emp_id = $tmp_category["reg_emp_id"];
		if ($tmp_category_reg_emp_id == $emp_id) {
			$tmp_category_disabled = "";
		} else {
			$tmp_category_disabled = "disabled";
		}
		$tmp_themes = $tmp_category["themes"];
		if ($tmp_category_id == $category_id && count($tmp_themes) > 0) {
			$tmp_category_expanded = "true";
		} else {
			$tmp_category_expanded = "false";
		}

		$tmp_category_html = "<input type=\"checkbox\" name=\"category_ids[]\" value=\"$tmp_category_id\" $tmp_category_disabled style=\"vertical-align:middle;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
		if ($tmp_category_id == $category_id && $theme_id == "") {
			$tmp_category_html .= "<b>$tmp_category_title</b>";
		} else {
			$tmp_category_html .= "<a href=\"bbsthread_menu.php?session=$session&category_id=$tmp_category_id&sort=$sort&show_content=$show_content\">$tmp_category_title</a>";
		}
		$tmp_category_html .= "</font>";
		if ($tmp_category_date >= $comp_date && $tmp_category_count > 0) {
			$tmp_category_html .= "<img src=\"img/icon/new.gif\" alt=\"NEW\" width=\"24\" height=\"10\" style=\"margin-left:4px;\">";
		}
?>
	var category<? echo($tmp_category_id); ?> = new YAHOO.widget.HTMLNode('<? echo($tmp_category_html); ?>', root, <? echo($tmp_category_expanded); ?>, true);
<?
		$theme_count = count($tmp_themes);
		$theme_index = 1;
		while ($tmp_data = each($tmp_themes)) {
			$tmp_theme_id = $tmp_data["key"];
			$tmp_theme = $tmp_data["value"];
			$tmp_theme_title = $tmp_theme["title"];
			$tmp_theme_date = $tmp_theme["date"];
			$tmp_theme_count = $tmp_theme["count"];
			$tmp_theme_reg_emp_id = $tmp_theme["reg_emp_id"];
			if ($tmp_theme_reg_emp_id == $emp_id) {
				$tmp_theme_disabled = "";
			} else {
				$tmp_theme_disabled = "disabled";
			}

			$tmp_theme_html = "<input type=\"checkbox\" name=\"theme_ids[]\" value=\"$tmp_theme_id\" $tmp_theme_disabled style=\"vertical-align:middle;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
			if ($tmp_theme_id == $theme_id) {
				$tmp_theme_html .= "<b>$tmp_theme_title</b>";
			} else {
				$tmp_theme_html .= "<a href=\"bbsthread_menu.php?session=$session&category_id=$tmp_category_id&theme_id=$tmp_theme_id&sort=$sort&show_content=$show_content\">$tmp_theme_title</a>";
			}
			$tmp_theme_html .= "</font>";
			if ($tmp_theme_date >= $comp_date && $tmp_theme_count > 0) {
				$tmp_theme_html .= "<img src=\"img/icon/new.gif\" alt=\"NEW\" width=\"24\" height=\"10\" style=\"margin-left:4px;\" style=\"vertical-align:middle;\">";
			}
?>
	var theme<? echo($tmp_theme_id); ?> = new YAHOO.widget.HTMLNode('<? echo($tmp_theme_html); ?>', category<? echo($tmp_category_id); ?>, false, true);
<?
			$theme_index++;
		}
	}
?>
	tree.draw();
}

function registCategory() {
	location.href = 'bbscategory_register.php?session=<? echo($session); ?>&sort=<? echo($sort); ?>&show_content=<? echo($show_content); ?>'
}

function registTheme() {
<? if ($category_id == "") { ?>
	alert('カテゴリが選択されていません。');
<? } else { ?>
	location.href = 'bbsthread_register.php?session=<? echo($session); ?>&category_id=<? echo($category_id); ?>&sort=<? echo($sort); ?>&show_content=<? echo($show_content); ?>';
<? } ?>
}

function updateCategory() {
	location.href = 'bbscategory_update.php?session=<? echo($session); ?>&category_id=<? echo($category_id); ?>&sort=<? echo($sort); ?>&show_content=<? echo($show_content); ?>';
}

function updateTheme() {
	location.href = 'bbsthread_update.php?session=<? echo($session); ?>&category_id=<? echo($category_id); ?>&theme_id=<? echo($theme_id); ?>&sort=<? echo($sort); ?>&show_content=<? echo($show_content); ?>';
}

function deleteCategoryTheme() {
	var category_ids = getCheckedIds(document.delform.elements['category_ids[]']);
	var theme_ids = getCheckedIds(document.delform.elements['theme_ids[]']);
	if (category_ids.length == 0 && theme_ids.length == 0) {
		alert('カテゴリ・テーマが選択されていません。');
		return;
	}

	if (confirm('選択されたカテゴリ・テーマを削除します。よろしいですか？')) {
		document.delform.submit();
	}
}

function changeSortType(sort) {
	location.href = 'bbsthread_menu.php?session=<? echo($session); ?>&category_id=<? echo($category_id); ?>&theme_id=<? echo($theme_id); ?>&sort=' + sort + '&show_content=<? echo($show_content); ?>';
}

function changeContentView(checked) {
	var show_content = (checked) ? 't' : '';
	location.href = 'bbsthread_menu.php?session=<? echo($session); ?>&category_id=<? echo($category_id); ?>&theme_id=<? echo($theme_id); ?>&sort=<? echo($sort); ?>&page=<? echo($page); ?>&show_content=' + show_content;
}

function postComment() {
<? if ($theme_id == "") { ?>
	alert('テーマが選択されていません。');
<? } else { ?>
	location.href = 'bbs_post_register.php?session=<? echo($session); ?>&theme_id=<? echo($theme_id); ?>&sort=<? echo($sort); ?>&show_content=<? echo($show_content); ?>';
<? } ?>
}

function getCheckedIds(boxes) {
	var checked_ids = new Array();
	if (boxes) {
		if (!boxes.length) {
			if (boxes.checked) {
				checked_ids.push(boxes.value);
			}
		} else {
			for (var i = 0, j = boxes.length; i < j; i++) {
				if (boxes[i].checked) {
					checked_ids.push(boxes[i].value);
				}
			}
		}
	}
	return checked_ids;
}

function setBbsMail()
{
	var url = 'bbs_mail_set.php';
	var mail = (document.getElementById('mail_notify').checked) ? "true" : "false";
	var params = $H({'session':'<?=$session?>','theme_id':'<?=$theme_id?>','mail':mail}).toQueryString();
	var myAjax = new Ajax.Request(
		url,
		{
			method: 'post',
			postBody: params
		});
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="bbsthread_menu.php?session=<? echo($session); ?>"><img src="img/icon/b10.gif" width="32" height="32" border="0" alt="掲示板・電子会議室"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="bbsthread_menu.php?session=<? echo($session); ?>"><b>掲示板・電子会議室</b></a> &gt; <a href="bbsthread_menu.php?session=<? echo($session); ?>&sort=<? echo($sort); ?>&show_content=<? echo($show_content); ?>"><b>投稿一覧</b></a></font></td>
<? if ($bbs_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="bbs_admin_menu.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="110" align="center" bgcolor="#5279a5"><a href="bbsthread_menu.php?session=<? echo($session); ?>&sort=<? echo($sort); ?>&show_content=<? echo($show_content); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>投稿一覧</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="130" align="center" bgcolor="#bdd1e7"><a href="bbs_option.php?session=<? echo($session); ?>&sort=<? echo($sort); ?>&show_content=<? echo($show_content); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション設定</font></a></td>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#bdd1e7"><a href="bbs_search.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">検索</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<? if ($allow_category == "t" || $allow_theme == "t") { ?>
<img src="img/spacer.gif" width="1" height="1" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
<td align="right">
<? if ($allow_category == "t") { ?>
<input type="button" value="カテゴリ作成" onclick="registCategory();">
<? } ?>
<? if ($allow_theme == "t") { ?>
<input type="button" value="テーマ作成" <? if (!$category_postable) {echo("disabled");} ?> onclick="registTheme();">
<? } ?>
</td>
</tr>
</table>
<? } ?>
<img src="img/spacer.gif" width="1" height="2" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="30%" valign="top">
<!-- theme list start -->
<form name="delform" action="bbsthread_delete.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr height="25" bgcolor="#f6f9ff">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カテゴリ＞テーマ</font></td>
<? if ($allow_category == "t" || $allow_theme == "t") { ?>
<td align="right"><input type="button" value="削除" onclick="deleteCategoryTheme();"></td>
<? } ?>
</tr>
</table>
</tr>
</td>
<tr height="400">
<td valign="top">
<? if (count($categories) == 0)  { ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カテゴリが登録されていません。</font>
<? } ?>
<div id="sam"></div>
</td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="sort" value="<? echo($sort); ?>">
<input type="hidden" name="show_content" value="<? echo($show_content); ?>">
</form>
<!-- theme list end -->
</td>
<td><img src="img/spacer.gif" alt="" width="2" height="1"></td>
<td width="70%" valign="top">
<!-- theme detail start -->
<? show_bbsthread_detail($con, $category_id, $theme_id, $emp_id, $fname); ?>
<!-- theme detail end -->
<!-- comment view start -->
<? if ($category_id != "" && $theme_id == "") { ?>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr bgcolor="#f6f9ff">
<td width="20%" class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">テーマ一覧<input type="checkbox" name="show_content" value="t"<? if ($show_content == "t") {echo(" checked");} ?> style="margin-left:10px;" onclick="changeContentView(this.checked);">内容を表示する</font></td>
</tr>
</table>
</td>
</tr>
<tr height="328">
<td valign="top" style="padding:0 2px;">
<? show_theme_list($con, $category_id, $categories[$category_id]["themes"], $sort, $newpost_days, $show_content, $session, $fname); ?>
</td>
</tr>
</table>
<? } ?>
<? if ($theme_id != "") { ?>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr bgcolor="#f6f9ff">
<td width="20%" class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">投稿一覧</font></td>
<td>
<select name="sort" onchange="changeSortType(this.value);">
<option value="1"<? if ($sort == "1") {echo(" selected");} ?>>ツリー表示（新しい順）
<option value="2"<? if ($sort == "2") {echo(" selected");} ?>>ツリー表示（古い順）
<option value="3"<? if ($sort == "3") {echo(" selected");} ?>>投稿順表示（新しい順）
<option value="4"<? if ($sort == "4") {echo(" selected");} ?>>投稿順表示（古い順）
</select>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="show_content" value="t"<? if ($show_content == "t") {echo(" checked");} ?> onclick="changeContentView(this.checked);">内容を表示する</font></td>
<td align="right"><input type="button" value="新しい投稿" onclick="postComment();"<? if (!$theme_postable) {echo(" disabled");} ?>></td>
</tr>
</table>
</td>
</tr>
<tr height="328">
<td valign="top" style="padding:0 2px;">
<? show_post_list($con, $theme_id, $sort, $newpost_days, $count_per_page, $page, $show_content, $emp_id, $session, $fname); ?>
</td>
</tr>
</table>
<? } ?>
<!-- comment view end -->
</td>
</tr>
</table>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
