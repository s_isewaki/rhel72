<?php
require_once("settings_common.php");
/**
 * �ٲ˼�������˥塼����ɽ��
 *
 * @param mixed $session ���å����ID
 * @param mixed $fname �ץ������ե�����̾
 * @return mixed �ʤ�
 *
 */
function show_timecard_hol_menuitem($session,$fname)
{
    define("HOL_REASON"                 ,"�ٲ˼���");
    define("HOL_PAID_LIST"              ,get_settings_value("kintai_vacation_paid_hol_title", "ǯ���ٲ���"));
    define("HOL_WORK"                   ,"�����ж�Ƚ��");
    define("HOL_LEGAL"                  ,"���ٿ�");
    define("HOL_PAID_TBL"               ,"ͭ��ٲ���Ϳ����ɽ");
    define("HOL_PAID_TBL2"              ,"ͭ��ٲ���Ϳ����ɽ�ʣ������ࡢ����ǯ���ѡ�");
    define("HOL_SUMMER"                 ,"�Ƶ��ٲ���Ϳ����ɽ");
    define("HOL_EXPORT"                 ,"�ٲ˼�������");
    
    //==============================
    //���̥ե�����̾���Ѵ�
    //==============================
    $index = strrpos($fname,"/");
    if($index != false)
    {
        $file = substr($fname,$index+1);
    }
    else
    {
        $file = $fname;
    }
    //==============================
    //���򥿥֤η���
    //==============================
    switch($file)
    {
        case "atdbk_reason_mst_config.php":
            $selected_tab = HOL_REASON;
            break;
        case "kintai_vacation_paid_holiday.php":
            $selected_tab = HOL_PAID_LIST;
            break;
        case "timecard_holwk_day.php":
            $selected_tab = HOL_WORK;
            break;
        case "timecard_legal_hol_cnt.php":
            $selected_tab = HOL_LEGAL;
            break;
        case "timecard_paid_hol_tbl.php":
            $selected_tab = HOL_PAID_TBL;
            break;
        case "timecard_paid_hol_tbl2.php":
            $selected_tab = HOL_PAID_TBL2;
            break;
        case "timecard_paid_hol_tbl_summer.php":
            $selected_tab = HOL_SUMMER;
            break;
        case "atdbk_reason_export.php":
            $selected_tab = HOL_EXPORT;
            break;
        default:
            $selected_tab = HOL_REASON;
            break;
    }
    //==============================
    //HTML����
    //==============================
    echo("&nbsp;&nbsp;");
    echo("<font size=\"3\" face=\"�ͣ� �Х����å�, Osaka\" class=\"j12\">");
    // �ٲ˼���
    $url = "atdbk_reason_mst_config.php?session=$session";
    show_timecard_hol_tab(HOL_REASON, $url, $selected_tab == HOL_REASON);
    // ǯ���ٲ���
    $opt_display_flag = file_exists("opt/flag");
    if (phpversion() >= "5.1" && !$opt_display_flag) {
        $url = "kintai_vacation_paid_holiday.php?session=$session&umode=adm2";
        show_timecard_hol_tab(HOL_PAID_LIST, $url, $selected_tab == HOL_PAID_LIST);
    }
    // �����ж�Ƚ��
    $url = "timecard_holwk_day.php?session=$session";
    show_timecard_hol_tab(HOL_WORK, $url, $selected_tab == HOL_WORK);
    // ���ٿ�
    $url = "timecard_legal_hol_cnt.php?session=$session";
    show_timecard_hol_tab(HOL_LEGAL, $url, $selected_tab == HOL_LEGAL);
    // ͭ��ٲ���Ϳ����ɽ
    $url = "timecard_paid_hol_tbl.php?session=$session";
    show_timecard_hol_tab(HOL_PAID_TBL, $url, $selected_tab == HOL_PAID_TBL);
    // ͭ��ٲ���Ϳ����ɽ�ʣ������ࡢ����ǯ���ѡ�
    $url = "timecard_paid_hol_tbl2.php?session=$session";
    show_timecard_hol_tab(HOL_PAID_TBL2, $url, $selected_tab == HOL_PAID_TBL2);
    // �Ƶ��ٲ���Ϳ����ɽ
    $url = "timecard_paid_hol_tbl_summer.php?session=$session";
    show_timecard_hol_tab(HOL_SUMMER, $url, $selected_tab == HOL_SUMMER);
    // �ٲ˼�������
    $url = "atdbk_reason_export.php?session=$session";
    show_timecard_hol_tab(HOL_EXPORT, $url, $selected_tab == HOL_EXPORT);
    
    echo("</font>");
}


/**
 * ����ɽ��
 *
 * @param mixed $text ��˥塼̾
 * @param mixed $url URL
 * @param mixed $is_selected ������� true:����
 * @return mixed �ʤ�
 *
 */
function show_timecard_hol_tab($text,$url,$is_selected)
{
    if($is_selected)
    {
        ?>
        <b><?=$text?></b>&nbsp;&nbsp;
        <?
    }
    else
    {
        ?>
        <a href="<?=$url?>"><?=$text?></a>&nbsp;&nbsp;
        <?
    }
}
?>
