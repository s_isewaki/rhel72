<?
require_once("about_comedix.php");
require_once('Cmx.php');
require_once('Cmx/Model/SystemConfig.php');

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 3, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 設定を更新
$conf = new Cmx_SystemConfig();
$conf->set('apply.hol.cfm', $hol_cfm);
$conf->set('apply.hol.remain_count_flg', $remain_count_flg);
$conf->set('apply.hol.over_apply_flg', $over_apply_flg);

// 画面をリフレッシュ
echo("<script type=\"text/javascript\">location.href = 'workflow_hol_approve_num.php?session=$session';</script>");
