<?
ob_start();
require_once("about_comedix.php");
require_once("show_select_values.ini");
require_once("yui_calendar_util.ini");
require_once("sot_util.php");
require_once("summary_common.ini");
require_once("get_values.php");
require_once("get_menu_label.ini");
require_once("html_to_text_for_tinymce.php");
require_once('fpdf153/mbfpdf.php');
ob_end_clean();

$THIS_WORKSHEET_ID = "1"; // [sotmst][sot_id]の固定値

$tmpl_node = array('Kango','Kaigo');

//====================================================================
//セッションチェック、権限チェック、DB接続
//====================================================================
$fname = $_SERVER["PHP_SELF"];
$session = qualify_session($session,$fname);
$summary_check = @check_authority($session,57,$fname);
$con = @connect2db($fname);
if(!$session || !$summary_check || !$con){
  echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
  exit;
}

//ログイン職員ＩＤ
$emp_id = get_emp_id($con, $session, $fname);


//=================================================
// 全共通
// SOAPテンプレートIDを取得する
//=================================================
$tmpl_id = array();
for($i = 0; $i < 2; ++$i){
  $sel = $c_sot_util->select_from_table("select tmpl_id from tmplmst where tmpl_file = 'tmpl_".$tmpl_node[$i]."ChukanSummary.php'");
  $tmpl_id[$i] = (int)pg_fetch_result($sel, 0, 0);
}

$sql = "select ptif_lt_kaj_nm, ptif_ft_kaj_nm from ptifmst where ptif_id = '".pg_escape_string(@$_REQUEST["sel_ptif_id"])."'";
$sel = select_from_table($con, $sql, "", $fname);
$ptif_name = trim(@pg_fetch_result($sel, 0, "ptif_lt_kaj_nm") . " " . @pg_fetch_result($sel, 0, "ptif_ft_kaj_nm"));


//=================================================
// 全共通
// 検索対象日付がない場合は本日とする
//=================================================
$date_y = (int)(@$_REQUEST["date_y"] ? $_REQUEST["date_y"] : date("Y"));
$date_m = (int)(@$_REQUEST["date_m"] ? $_REQUEST["date_m"] : date("m"));
$date_d = (int)(@$_REQUEST["date_d"] ? $_REQUEST["date_d"] : date("d"));
if (!checkdate($date_m, $date_d, $date_y)) list($date_y, $date_m, $date_d) = split("/", date("Y/m/d", strtotime("today")));
$from_yyyymmdd = $date_y. ((int)@$date_m <10 ? "0".(int)@$date_m : $date_m) . ((int)@$date_d <10 ? "0".(int)@$date_d : $date_d);

//$hasKikan = (@$_REQUEST["date_y2"]) ? true : false ;
$date_y2 = (int)(@$_REQUEST["date_y2"] ? $_REQUEST["date_y2"] : date("Y"));
$date_m2 = (int)(@$_REQUEST["date_m2"] ? $_REQUEST["date_m2"] : date("m"));
$date_d2 = (int)(@$_REQUEST["date_d2"] ? $_REQUEST["date_d2"] : date("d"));
if (!checkdate($date_m2, $date_d2, $date_y2)) list($date_y2, $date_m2, $date_d2) = split("/", date("Y/m/d", strtotime("today")));
$to_yyyymmdd = $date_y2. ((int)@$date_m2 <10 ? "0".(int)@$date_m2 : $date_m2) . ((int)@$date_d2 <10 ? "0".(int)@$date_d2 : $date_d2);



//=================================================
// 共通
//=================================================
$sel_team_id = (int)@$_REQUEST["sel_team_id"];
$sel_bldg_cd = (int)$_REQUEST["sel_bldg_cd"];
$sel_ward_cd = (int)$_REQUEST["sel_ward_cd"];
$sel_ptrm_room_no = (int)$_REQUEST["sel_ptrm_room_no"];


$nyuin_info = $c_sot_util->get_nyuin_info($con, $fname, @$_REQUEST["sel_ptif_id"], $from_yyyymmdd);
$ptif_ward_cd = @$nyuin_info["ward_cd"];



//=================================================
// Web画面用
// 病棟などが指定されていなければデフォルトを設定する
//=================================================
if (@$_REQUEST["mode"]=="show_print_dialog" || @$_REQUEST["mode"]==""){
  if (!$sel_bldg_cd && !$sel_ward_cd){
    $sql =
      " select swer.bldg_cd, swer.ward_cd from sot_ward_empclass_relation swer".
      " inner join empmst emp on (".
      "   emp.emp_class = swer.class_id".
      "   and emp.emp_attribute = swer.atrb_id".
      "   and emp.emp_dept = swer.dept_id".
      "   and emp.emp_id = '".pg_escape_string($emp_id)."'".
      " )";
    $emp_ward = @$c_sot_util->select_from_table($sql);
    $sel_bldg_cd = (int)@pg_fetch_result($emp_ward, 0, "bldg_cd");
    $sel_ward_cd = (int)@pg_fetch_result($emp_ward, 0, "ward_cd");
  }
}
if (!$sel_bldg_cd) $sel_bldg_cd = 1;


//=================================================
// 一括印刷用（Web画面またはPDF印刷時）
// 印刷対象患者のリストアップ
// 患者が指定されていない場合は、検索範囲から以下のリストアップ
// ・対象期間内に
// ・指定病棟に入院していること
//=================================================
$pt_list = array();
if (@$_REQUEST["mode"]=="show_print_dialog" || @$_REQUEST["mode"]=="pdf_download_list"){

  if (@$_REQUEST["sel_ptif_id"]) $pt_list[] = @$_REQUEST["sel_ptif_id"];
  else {
    $sql =
      " select ptif_id from inptmst".
      " where bldg_cd = " . $sel_bldg_cd.
      " and ward_cd = " . $sel_ward_cd.
      " and inpt_in_dt <= '".(int)$to_yyyymmdd."'".
      " order by ptrm_room_no";
    $sel = select_from_table($con, $sql, "", $fname);
    while ($row = pg_fetch_array($sel)){
      $pt_list[] = $row["ptif_id"];
    }

    $sql =
      " select ptif_id from inpthist".
      " where bldg_cd = " . $sel_bldg_cd.
      " and ward_cd = " . $sel_ward_cd.
      " and inpt_in_dt is not null and inpt_in_dt <> ''".
      " and (" .
      "   (".
      "     inpt_in_dt <= '".(int)$from_yyyymmdd."'".
      "     and inpt_out_dt >= '" . (int)$from_yyyymmdd."'".
      "   )".
      "   or".
      "   (".
      "     inpt_in_dt <= '".(int)$to_yyyymmdd."'".
      "     and (inpt_out_dt is null or inpt_out_dt >= '" . (int)$to_yyyymmdd . "')".
      "   )".
      " )".
      " order by ptrm_room_no";
    $sel = select_from_table($con, $sql, "", $fname);
    while ($row = pg_fetch_array($sel)){
      if (in_array($row["ptif_id"], $pt_list)) continue;
      $pt_list[] = $row["ptif_id"];
    }
  }
}

//=================================================
// 患者指定印刷用（Web画面またはPDF印刷時）
//=================================================
else {
  if (@$_REQUEST["sel_ptif_id"]) $pt_list[] = @$_REQUEST["sel_ptif_id"];
}



//=================================================
// Web画面用（患者指定印刷、一括印刷）
// 事業所と病棟の一覧。ドロップダウン用
//=================================================
if (@$_REQUEST["mode"]=="show_print_dialog" || @$_REQUEST["mode"]=="" || @$_REQUEST["mode"]=="pdf_download_list"){
  $bldg_list = array();
  $bldg_data_options = array();
  $bldg_data_options[] = $EMPTY_OPTION;
  $ward_list = array();
  $ward_data_options = array();
  $ward_data_options[] = $EMPTY_OPTION;

  // 事業所と病棟選択肢
  $sql =
    " select bldg.bldg_name, bldg.bldg_cd, wd.ward_cd, wd.ward_name from wdmst wd".
    " inner join bldgmst bldg on (bldg.bldg_cd = wd.bldg_cd)".
    " where wd.ward_del_flg = 'f'".
    " and bldg.bldg_del_flg = 'f'".
    " order by bldg.bldg_cd, wd.ward_cd";
  $sel = $c_sot_util->select_from_table($sql);
  while($row = pg_fetch_array($sel)){
    $selected1 = ($row["bldg_cd"] == $sel_bldg_cd ? " selected" : "");
    if (!@$bldg_list[$row["bldg_cd"]]){
      $bldg_list[$row["bldg_cd"]] = $row["bldg_name"];
      $bldg_data_options[] = '<option value="'.$row["bldg_cd"].'"'.$selected1.'>'.$row["bldg_name"].'</option>';
    }

    $ward_list[$row["bldg_cd"]][$row["ward_cd"]] = $row["ward_name"];
    if ($selected1){
      $selected2 = ($row["ward_cd"] == $sel_ward_cd ? " selected" : "");
      $ward_data_options[] = '<option value="'.$row["ward_cd"].'"'.$selected2.'>'.$row["ward_name"].'</option>';
    }
  }
}






//=================================================
// 該当患者のSOAP発行履歴の検索
//=================================================

$where1 = "";
if (@$_REQUEST["mode"]=="pdf_single_preview" || @$_REQUEST["mode"]==""){
  $where1 = " and cre_date = '".$from_yyyymmdd."'";
}
if (@$_REQUEST["mode"]=="show_print_dialog" || @$_REQUEST["mode"]=="pdf_download_list"){
  $where1 = " and cre_date >= '".$from_yyyymmdd."' and cre_date <= '".$to_yyyymmdd."'";
}

if($_REQUEST["sel_ptif_id"] || ($_REQUEST["sel_bldg_cd"] && $_REQUEST["sel_ward_cd"])){
  $sql =
    " select".
    " x.xml_file_name,".
    " s.cre_date, s.diag_div, s.emp_id, s.ptif_id, s.tmpl_id, ".
    " e.emp_lt_nm, e.emp_ft_nm,".
    " p.ptif_lt_kaj_nm, p.ptif_ft_kaj_nm,".
    " n.ward_cd, n.inpt_bed_no, w.ward_name, r.ptrm_name, j.job_nm, t.tmpl_name".
    " from (".
    "     select cre_date, emp_id, diag_div, summary_seq, ptif_id, tmpl_id from summary".
    "     where 1 = 1".
    " " . (count($pt_list) ? " and ptif_id in ('".implode($pt_list,"','")."')" : "").
        "     and (tmpl_id = ".$tmpl_id[0].
    "     or tmpl_id = ".$tmpl_id[1].")".
    "     " .$where1.
    " ) s".
    " inner join (".
        " select ptif_id, bldg_cd, ward_cd, ptrm_room_no, inpt_bed_no from inptmst".
        " where 1=1".
        (@$_REQUEST["sel_ptif_id"] ? "" : " and bldg_cd = " . $sel_bldg_cd).
        (@$_REQUEST["sel_ptif_id"] ? "" : " and ward_cd = " . $sel_ward_cd).
        " and inpt_in_dt <= '".(int)$to_yyyymmdd."'".
        " union".
        " select ptif_id, bldg_cd, ward_cd, ptrm_room_no, inpt_bed_no from inpthist".
        " where 1=1".
        (@$_REQUEST["sel_ptif_id"] ? "" : " and bldg_cd = " . $sel_bldg_cd).
        (@$_REQUEST["sel_ptif_id"] ? "" : " and ward_cd = " . $sel_ward_cd).
        " and inpt_in_dt is not null and inpt_in_dt <> ''".
        " and (" .
        "   (".
        "     inpt_in_dt <= '".(int)$from_yyyymmdd."'".
        "     and inpt_out_dt >= '" . (int)$from_yyyymmdd."'".
        "   )".
        "   or".
        "   (".
        "     inpt_in_dt <= '".(int)$to_yyyymmdd."'".
        "     and (inpt_out_dt is null or inpt_out_dt >= '" . (int)$to_yyyymmdd . "')".
        "   )".
        " )".
        " group by ptif_id, bldg_cd, ward_cd, ptrm_room_no, inpt_bed_no".
    " ) n on (n.ptif_id = s.ptif_id)".
    " inner join (".
    "     select summary_seq, max(xml_file_name) as xml_file_name from sum_xml".
    "     where 1 = 1".
    " " . (count($pt_list) ? " and ptif_id in ('".implode($pt_list,"','")."')" : "").
    "     and (delete_flg = 0 or delete_flg is null)".
    "     and is_work_data = '0'".
    "     group by summary_seq".
    " ) x on (x.summary_seq = s.summary_seq)".
    " left outer join empmst e on (e.emp_id = s.emp_id)".
    " left outer join jobmst j on (j.job_id = e.emp_job)".
    " left outer join ptifmst p on (p.ptif_id = s.ptif_id)".
    " left outer join tmplmst t on (t.tmpl_id = s.tmpl_id)".
    " left outer join wdmst w on (w.bldg_cd = n.bldg_cd and w.ward_cd = n.ward_cd)".
    " left outer join ptrmmst r on (r.bldg_cd = n.bldg_cd and r.ward_cd = n.ward_cd and r.ptrm_room_no = n.ptrm_room_no)".
    " order by r.ptrm_name, n.inpt_bed_no, s.cre_date, s.diag_div, s.summary_seq";

  $sel = $c_sot_util->select_from_table($sql);
  $pt_data = pg_fetch_all($sel);
}

  if (!is_array($pt_data) || !$pt_data[0]) $pt_data = array();

  $fulllist = array();
  if ($pt_data) foreach($pt_data as $key => $row){
    $xml = new template_xml_class();
    $xmldata = $xml->get_xml_object_array_from_db($con, $fname, $row["xml_file_name"]);
    $worksheet = @$xmldata->nodes["template"]->nodes["ChukanSummary"];
    $ymd_y = @$worksheet->nodes["exec_y"]->value;
    $ymd_m = @$worksheet->nodes["exec_m"]->value;
    $ymd_d = @$worksheet->nodes["exec_d"]->value;
    $time_h = @$worksheet->nodes["exec_time_h"]->value;
    $time_m = @$worksheet->nodes["exec_time_m"]->value;
    $ymdhmstr = $ymd_y . substr("00" . $ymd_m, -2) . substr("00" . $ymd_d, -2) . substr("00" . $time_h, -2) . substr("00" . $time_m, -2) ;
    $ymdhm = "";
    if ($ymd_y && $ymd_m && $ymd_d){
  //    $ymdhm = $ymd_y ."年". $ymd_m ."月". $ymd_d."日 ";
      $ymdhm = $ymd_y ."/". $ymd_m ."/". $ymd_d ." " ;
    }
    if ($time_h !="") $ymdhm .= $time_h."時";
    if ($time_h !="" && $time_m!="") $ymdhm .= $time_m."分";
    $pt_data[$key]["ymdhm"] = $ymdhm;

    $nodes = $worksheet->nodes ;
    if($row["tmpl_name"] == '看護中間サマリ') $txt = "kncs_txt";
    if($row["tmpl_name"] == '介護中間サマリ') $txt = "kics_txt";
    $pt_data[$key]["dataN"] = getSoapFromXmlData($nodes, $txt) ;

    $pt_data[$key]["key"] = $key;
    $pt_data[$key]["ymdhmstr"] = $ymdhmstr;
  }

  usort($pt_data, "compare_pt_data");

  foreach($pt_data as $key => $row)
  {
    $fulllist[$row["ward_cd"]][$row["ptif_id"]]["ptinfo"] = $row;
    $fulllist[$row["ward_cd"]][$row["ptif_id"]]["rows"][] = $row;
  }

// *****************************************************************************
// *****************************************************************************
// 処理分岐１of３ PDFの作成モード
// *****************************************************************************
// *****************************************************************************
if (@$_REQUEST["mode"]=="pdf_single_preview" || (@$_REQUEST["mode"]=="pdf_download_list" && count($pt_data))){

    // PDF作成については以下を参照しました。
    // http://www.phpbook.jp/fpdf/index.html

class CustomMBFPDF extends MBFPDF {
    // A4：210 x 297
    var $M          = 0.3;
    var $MOST_LEFT  =  15;
    var $MOST_RIGHT = 200;
    var $MOST_TOP   =  10;
    var $MOST_BTM   = 282;
    var $HD_PT_CELL_W = array(40, 40, 45, 60);
    var $HD_CL_CELL_W = array(25, 30, 130);
    var $SP_CHRS = array("S", "O", "A", "P", "N");
    var $contentLeft  = 71;
    var $contentRight = 11;
    var $currPtInfo;
    var $page_no;
    var $is_new_pt = 1;

    var $saveY;

    // アンダーラインを打ち消し線に代用するためのオーバーライド
    function _dounderline($x,$y,$txt) {
        return parent::_dounderline($x,$y-2,$txt);
    }

    //｢EUC-JP → SJIS｣変換のためのオーバーライド
    //MultiCell()もWrite()もこのCell()で表示しているので、オーバーライドはCell()だけでOK
    function Cell($w, $h = 0, $txt = '', $border = 0, $ln = 0, $align = '', $fill = 0, $link = '') {
        $enc = mb_internal_encoding();
        if($enc != "sjis-win"){
            $txt = mb_convert_encoding($txt, "sjis-win", $enc);
            parent::Cell($w, $h, $txt, $border, $ln, $align, $fill, $link);
        }
    }

    function writePtHeader($pt_id) {
        $this->setX($this->MOST_LEFT);
        $this->SetFont(MINCHO, '' ,11);
        $this->Cell($this->HD_PT_CELL_W[0], 5.0, "病棟　"   .  $this->currPtInfo["ward_name"], 'B', 0, 'L');
        $this->Cell($this->HD_PT_CELL_W[1], 5.0, "病室　"   .  $this->currPtInfo["ptrm_name"], 'B', 0, 'L');
        $this->Cell($this->HD_PT_CELL_W[2], 5.0, "患者ID　" . @$pt_id,                         'B', 0, 'L');
        $this->Cell($this->HD_PT_CELL_W[3], 5.0, "氏名　"   .  $this->currPtInfo["ptif_lt_kaj_nm"] . " " . $this->currPtInfo["ptif_ft_kaj_nm"], 'B', 1, 'L');
    }

    function writeColHeader() {
        $this->setY($this->getY() + 5);
        $this->setX($this->MOST_LEFT);
        $this->Cell($this->HD_CL_CELL_W[0], 5.0, "日時",     '1', 0, 'C');
        $this->Cell($this->HD_CL_CELL_W[1], 5.0, "記録区分", '1', 0, 'C');
        $this->Cell($this->HD_CL_CELL_W[2], 5.0, "内容",     '1', 1, 'C');
    }

    // SOAPNボックス
    function writeSoapBox($soapY) {
        $curY = $this->getY() + 5;
        $this->setXY($this->MOST_LEFT + $this->HD_CL_CELL_W[0] + $this->HD_CL_CELL_W[1] + 1, $soapY);
        $this->Cell(128, $curY - $soapY, "", '1', 0); // S/O/A/P/Nのボックス
        $this->setY($curY);
    }

    // 患者ボックス
    function writePtBox($ptStartY) {
        $curY = $this->getY();
        $cellH = $curY - $ptStartY + 2;
        $this->setXY($this->MOST_LEFT, $ptStartY);
        $this->Cell($this->HD_CL_CELL_W[0], $cellH, "", '1', 0); // 日時     枠線
        $this->Cell($this->HD_CL_CELL_W[1], $cellH, "", '1', 0); // 記録区分 枠線
        $this->Cell($this->HD_CL_CELL_W[2], $cellH, "", '1', 1); // 内容     枠線
        $this->setY($curY);
    }

    function writePageNo() {
        $this->page_no++;
        $this->SetFont(MINCHO, '' ,11);
        $this->setXY($this->MOST_LEFT, $this->MOST_BTM + 3);
        $this->Cell(0, 3, "- " . $this->page_no . " -", '', 0, 'C');
    }

    function writePtData($pt_id, &$ptData, $is_rewrite) {
        $ptStartY = $this->getY();
        // 1列目 日時 線なし
        $this->setY($ptStartY + 1);
        $this->setX($this->MOST_LEFT);
        $nichiji = str_replace(" ", "\n", $ptData["ymdhm"]);
        $this->MultiCell($this->HD_CL_CELL_W[0], 5.0, $nichiji, '', 'L');

        // 2列目 診療区分
        $this->setY($ptStartY + 1);
        $this->setX($this->MOST_LEFT + $this->HD_CL_CELL_W[0]);
        $this->Cell($this->HD_CL_CELL_W[1], 5.0,          $ptData["diag_div"], '',   1);

        // 3列目 内容
        $this->setY($ptStartY + 1);
        $sX = $this->MOST_LEFT + $this->HD_CL_CELL_W[0] + $this->HD_CL_CELL_W[1] + 1;
        $this->setX($sX);
        $this->Cell(14, 5.0, "記載者",                             '1', 0, 'C');
        $this->Cell( 0, 5.0, " " . $ptData["emp_lt_nm"]. " " . $ptData["emp_ft_nm"] . " (" . $ptData["job_nm"] . ")", '0' , 1);
        $this->setX($sX);
        $this->Cell( 0, 5.0,  $ptData["tmpl_name"], '0' , 2);
        $this->setY($this->getY() + 1);

        $soapY = $this->getY();
        $cnt = count($ptData["lines"]);
        $is_block_top = 1;
        $this->SetLeftMargin($this->contentLeft);
        $this->SetRightMargin($this->contentRight);
        for($idx=0; $idx<$cnt; $idx++) {
            $lineInfo = $ptData["lines"][$idx];
            $line = $lineInfo["line"];
            $mend = $lineInfo["is_mark_end"];
            $strike = $lineInfo["strike"];
            if ($is_block_top) {
                $line = trim($line);
            }
            if ($line=="") continue;

            $this->SetFont(MINCHO, $strike ,11);
            $this->Write(5.0, $line);
            $this->SetFont(MINCHO, "" ,11);

            if ($this->getY()+20 > $this->MOST_BTM) {
                if ($this->is_new_pt || $is_rewrite || $ptStartY == 20) {
                    $this->writeSoapBox($soapY);
                    $this->writePtBox($ptStartY); // 患者ボックス
                    $this->writePageNo();
                    $this->AddPage();
                    $this->setX(0);
                    $this->writePtHeader($pt_id);
                    $this->writeColHeader();
                    $this->setY($this->getY() + 3);
                    $soapY = 27;
                    $ptStartY = 20;
                    $is_block_top = 1;
                    continue;
                } else {
                    // 指定範囲を白で塗りつぶす
                    $this->SetFillColor(255, 255, 255);
                    $this->Rect($this->MOST_LEFT - 2, $ptStartY + $this->M, $this->MOST_RIGHT - $this->MOST_LEFT + 4, 297 - $ptStartY, "F");
                    $this->writePageNo();
                    $this->AddPage();
                    $this->writePtHeader($pt_id);
                    $this->writeColHeader();
                    $this->writePtData($pt_id, $ptData, 1);
                    $is_block_top = 1;
                    return;
                }
            }
            $is_block_top = 0;

            if ($mend) {
                $this->writeSoapBox($soapY);
            }

            if ($idx+1==$cnt) {
                $this->writePtBox($ptStartY);
            }
        }
        $this->setY($this->getY() + 3);
    }
}

    $pdf = new CustomMBFPDF();
    $to_yyyymmdd = (@$_REQUEST["mode"] == "pdf_download_list") ? $to_yyyymmdd : "";
    $kikanFrom = $c_sot_util->kanji_ymd($from_yyyymmdd); // 例："20101201" → "2010年12月1日"
    $kikanTo   = $c_sot_util->kanji_ymd($to_yyyymmdd  ); // 例："20110131" → "2011年1月31日" (一括印刷でない場合は"" → "")
    $pdf->SetAutoPageBreak(false);
    $pdf->AddMBFont(MINCHO,'SJIS');
    $pdf->SetMargins($pdf->MOST_LEFT, $pdf->MOST_TOP);
    $pdf->Open();

    $ua = $_REQUEST["User_Agent"];
    $useTinyMCE = ((strpos($ua, "Opera") === FALSE) && (strpos($ua, "Safari") === FALSE)) ? true : false;

    foreach ($fulllist as $ward_cd => $ward_data){
        foreach ($ward_data as $pt_id => $data){

            $pdf->currPtInfo = $data["ptinfo"];
            $pdf->AddPage();
            $curY = $pdf->getY();

            $pdf->SetFont(MINCHO, '' ,11);
            $pdf->Cell(0, 5, date("Y/m/d H:i"), '', 0, 'R');

            $kikanText = "日付 " . $kikanFrom;
            if (strlen($kikanTo) > 0) $kikanText = "期間 " . $kikanFrom . "〜" . $kikanTo;
            $pdf->setY($curY);
            $pdf->SetFont(MINCHO, '' ,12);
            $pdf->Cell(0, 17, $kikanText, '', 0, 'R');

            $pdf->setX($pdf->MOST_LEFT);
            $pdf->SetFont(MINCHO, '', 20);
            $pdf->Cell(0, 37.0, "中 間 サ マ リ", '', 1, 'C'); // 30.0
            $pdf->setY($pdf->getY()-8);

            $pdf->writePtHeader($pt_id);
            $pdf->writeColHeader();
            $pdf->page_no = 0;
            $pdf->is_new_pt = 1;

            foreach($data["rows"] as $key => $ptData){
                $dataN = trim($ptData["dataN"]);
                $dataN = html_entity_decode($dataN);
                if (strlen($dataN) == 0) continue;
                $dataN = html2text4tinymce2($dataN, $useTinyMCE, 1);
                $lines = array();

                //文字化け処理
                // 打消線指定(<strike>xxx</strike>)を探しながら印字していく
                $si = 0;
                $line_per_strike = array();
                $slen = mb_strlen($dataN);
                while (true) {
                    if ($slen<=$si) break;
                    $fi = mb_strpos($dataN, "<strike>", $si);
                    if ($fi === false) {
                        $line_per_strike[]= array("strike"=>"", "text"=>mb_substr($dataN, $si));
                        break;
                    } else {
                        $line_per_strike[]= array("strike"=>"", "text"=>mb_substr($dataN, $si, $fi - $si));
                        $fi += 8;
                        $ei = mb_strpos($dataN, "</strike>", $fi);
                        if ($ei===FALSE) $ei = $slen;
                        $line_per_strike[]= array("strike"=>"U", "text"=>mb_substr($dataN, $fi, $ei - $fi));
                        $si = $ei + 9;
                    }
                }

                $lcnt = count($line_per_strike);
                for ($lidx=0; $lidx<$lcnt; $lidx++) {
                    $lpsLine = $line_per_strike[$lidx];
                    $alines = explode("\n", $lpsLine["text"]);
                    $len = count($alines);
                    for ($idx=0; $idx<$len; $idx++) {
                        $lf = "";
                        if ($idx>0) $lf = "\n";
                        $mend = "";
                        if ($idx==$len-1 && $lidx==$lcnt-1) $mend = 1;
                        $lines[] = array("strike"=>$lpsLine["strike"], "line"=>$lf.$alines[$idx], "is_mark_end"=>$mend);
                    }
                }
                $ptData["lines"] = $lines;
                $pdf->writePtData($pt_id, $ptData, 0);
                $pdf->is_new_pt = 0;
            }
            // 下に余白が残っていれば表示
            if ($pdf->PageBreakTrigger > $pdf->getY() + 12){
                $pdf->Cell(0, 5, "以下余白",0,1,"R");
            }

            $pdf->writePageNo();

            // 対象患者の終わり
            // 一括印刷時は裏面印刷分の空ページを挿入
            if (@$_REQUEST["mode"]=="pdf_download_list") {
                if ($pdf->page % 2) {
                    $pdf->AddPage();
                    $pdf->Cell(0, 5, "以下余白",0,1,"R");
                    $pdf->writePageNo();
                }
            }
        }
    }

    if (@$_REQUEST["mode"]=="pdf_download_list"){
        $pdf->Output("summarylist".date("Ymd_His").".pdf", "D");
    } else {
        $pdf->Output();
    }
    die;
}

// *****************************************************************************
// *****************************************************************************
// 処理分岐２of３ 一括印刷ダイアログ画面モード
// *****************************************************************************
// *****************************************************************************
if ($_REQUEST["mode"]=="show_print_dialog" || @$_REQUEST["mode"]=="pdf_download_list"){
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix チーム記録印刷</title>
<link rel="stylesheet" type="text/css" href="css/main_summary.css" />
<? require_once("yui_calendar_util.ini"); ?>
<? write_yui_calendar_use_file_read_0_12_2(); ?>
<? write_yui_calendar_script2(2); ?>
<script type="text/javascript">
  var bldg_list = {};
<? foreach ($bldg_list as $bldg_cd => $bldg_name){ ?>
    bldg_list["<?=$bldg_cd?>"] = [];
  <? foreach ($ward_list[$bldg_cd] as $ward_cd => $ward_name){ ?>
      bldg_list["<?=$bldg_cd?>"].push({"ward_cd":"<?=$ward_cd?>","ward_name":"<?=str_replace('"','\"',$ward_name)?>"});
  <? } ?>
<? } ?>
  function bldgChanged(bldg_id){
    var dropdown = document.getElementById("sel_ward_cd");
    dropdown.options.length = bldg_list[bldg_id].length + 1;
    for (var i = 0; i < bldg_list[bldg_id].length; i++){
      dropdown.options[i+1].value = bldg_list[bldg_id][i].ward_cd;
      dropdown.options[i+1].text  = bldg_list[bldg_id][i].ward_name;
    }
    dropdown.selectedIndex = 0;
  }

  function popupKanjaSelect(){
    window.open(
      "sot_kanja_select.php?session=<?=$session?>&js_callback=callbackKanjaSelect&sel_bldg_cd=1&sel_ward_cd=<?=@$ptif_ward_cd?>&sel_ptif_id=<?=@$sel_ptif_id?>&sel_sort_by_byoto=1",
      "kanjaSelectWindow",
      "directories=no,location=no,menubar=no,resizable=yes,scrollbars=no,status=no,titlebar=no,toolbar=no,width=630,height="+window.screen.availHeight+",left=0,top=0"
    );
  }
  function callbackKanjaSelect(ptif_id, ptif_name){
    document.getElementById("sel_ptif_id").value = ptif_id;
    document.getElementById("sel_ptif_name").value = ptif_name;
  }
  function clearKanja(){
    document.getElementById("sel_ptif_id").value = "";
    document.getElementById("sel_ptif_name").value = "";
  }
  function startPrint(){
    var wardidx = document.getElementById("sel_ward_cd").options.selectedIndex;
    var wardcd = document.getElementById("sel_ward_cd").options[wardidx].value;
    if (wardcd=="" && document.getElementById("sel_ptif_id").value=="") {
      alert("病棟または患者を指定してください。");
      return;
    }
    if (!confirm("印刷PDF作成処理を開始します。\nこの処理には時間がかかる場合があります。\n\nPDF作成処理を開始してよろしいですか？")) return;
    document.search.mode.value = "pdf_download_list";
    document.search.submit();
  }
  <? if ($_REQUEST["mode"]=="pdf_download_list"){ ?>
  alert("指定した印刷条件では、印刷対象情報は存在しませんでした。");
  <? } ?>
</script>
</head>
<body onload="initcal();">
<center>
<div style="width:590px; margin-top:4px">

<!-- タイトルヘッダ -->
<?= summary_common_show_dialog_header("チーム記録印刷"); ?>

<!-- 検索条件エリア -->
<form name="search" action="" method="get">
<input type="hidden" name="searching" value="1">
<input type="hidden" name="mode" value="">
<input type="hidden" name="session" value="<?= $session ?>">
<input type="submit" value="dummy" style="display:none"/>
<div class="dialog_searchfield">
  <table style="width:100%">
    <tr>
      <td style="white-space:nowrap">
        <div style="margin-top:4px;">事業所(棟)</div>
      </td>
      <td>
        <div style="float:left; margin-top:2px">
          <select onchange="bldgChanged(this.value);" name="sel_bldg_cd"><?=implode("\n", $bldg_data_options)?></select>
        </div>
        <div style="float:left; margin-top:4px; margin-left:4px">病棟</div>
        <div style="float:left; margin-top:2px; margin-left:4px">
          <select name="sel_ward_cd" id="sel_ward_cd"><option value=""></option><?=implode("\n", $ward_data_options)?></select>
        </div>
        <script>
          function actSwitch(val){
            if(val == 1){
              document.search.action = "sot_worksheet_soaplist.php";
            }else if(val == 2){
              document.search.action = "sot_worksheet_summarylist.php";
            }
          }
        </script>
        <div style="background-color:#d9edfd; margin-left:170px; margin-top:2px; width:200px">
        <label><input type="radio" name="switching_pdf" id="switching_pdf" value="1" onclick="actSwitch(this.value)">SOAP</input></label>
        <label><input type="radio" name="switching_pdf" id="switching_pdf" value="2" onclick="actSwitch(this.value)" checked>中間サマリ</input></label>
        </div>
        <div style="clear:both"></div>
      </td>
      <td></td>
    </tr>
    <tr>
      <td style="white-space:nowrap">
        <div style="margin-top:4px;">期間</div>
      </td>
      <td>
        <table>
        <tr>
          <td>
            <select id="date_y1" name="date_y"><option value="-"><? show_select_years_span(date("Y") - 14, date("Y") + 1, $date_y, false); ?></select>/<select id="date_m1" name="date_m"><? show_select_months($date_m, true); ?></select>/<select id="date_d1" name="date_d"><? show_select_days($date_d, true); ?></select>
          </td>
          <td>
            <img src="img/calendar_link.gif" style="left:3px;z-index:1;cursor:pointer;" onclick="show_cal1();"/><br>
              <div id="cal1Container" style="position:absolute;display:none;z-index:10000;"></div>
          </td>
          <td>
            &nbsp;〜<select id="date_y2" name="date_y2"><option value="-"><? show_select_years_span(date("Y") - 14, date("Y") + 1, $date_y2, false); ?></select>/<select id="date_m2" name="date_m2"><? show_select_months($date_m2, true); ?></select>/<select id="date_d2" name="date_d2"><? show_select_days($date_d2, true); ?></select>
          </td>
          <td>
            <img src="img/calendar_link.gif" style="left:3px;z-index:1;cursor:pointer;" onclick="show_cal2()"/><br>
              <div id="cal2Container" style="position:absolute;display:none;z-index:10000;"></div>
          </td>
        </tr>
        </table>
      </td>
      <td></td>
    </tr>
    <tr>
      <td style="width:10%">患者ID</td>
      <td>
        <div style="float:left">
          <input type="text" id="sel_ptif_id" name="sel_ptif_id" style="width:90px; ime-mode:active; background-color:#d9edfd; border:1px solid #aaa; padding:2px" value="<?=@$_REQUEST["sel_ptif_id"]?>" readonly="readonly"/><!--
      --><input type="text" id="sel_ptif_name" name="sel_ptif_name" style="width:120px; ime-mode:active; background-color:#d9edfd; border:1px solid #aaa; padding:2px" value="<?=$ptif_name?>" readonly="readonly" /><!--
      --><input type="button" onclick="popupKanjaSelect();" value="患者検索"/><input type="button" onclick="clearKanja();" value="患者クリア"/>
        </div>
        <div style="clear:both"></div>
      </td>
      <td style="text-align:right; padding-right:8px">
      </td>
    </tr>
    <tr>
      <td colspan="3">
      <div style="background-color:#dbeefd; border:1px solid #aaa; margin:4px; padding:4px">
      ※ 病棟または患者を指定してください。<br/>
      ※ 患者を指定しない場合は、期間内・指定病棟に入院していた患者情報が印刷されます。<br/>
      ※ 患者を指定した場合、指定病棟情報は無視されます。<br/>
      <br/>
      ※ 印刷用PDF作成には時間がかかる場合があります。<br/>
      　 PDF作成処理完了後は、ダウンロードダイアログが表示されます。<br/>
      </div>
      </td>
    </tr>
    <tr>
      <td colspan="3" style="text-align:right; padding-right:8px">
        <input type="button" onclick="startPrint();" value="印刷開始"/>
      </td>
    </tr>
  </table>
</div>
</form>
</div>
</center>
</body>
</html>
<?
  die;
}



// *****************************************************************************
// *****************************************************************************
// 処理分岐１of３ 一覧表示画面モード
// *****************************************************************************
// *****************************************************************************
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<title>CoMedix <?=get_report_menu_label($con, $fname)?> | チーム記録</title>
<? write_yui_calendar_use_file_read_0_12_2();// 外部ファイルを読み込む ?>
<? write_yui_calendar_script2(1);// カレンダー作成、関数出力 ?>
<script type="text/javascript">

function popupPrintPreview(){
  window.open(
    "sot_worksheet_summarylist.php?session=<?=$session?>&date_y=<?=@$date_y?>&date_m=<?=@$date_m?>&date_d=<?=@$date_d?>&sel_bldg_cd=<?=$sel_bldg_cd?>
    &sel_ward_cd=<?=$sel_ward_cd?>&sel_ptrm_room_no=<?=$sel_ptrm_room_no?>&sel_team_id=<?=@$sel_team_id?>&sel_ptif_id=<?=@$_REQUEST["sel_ptif_id"]?>&mode=pdf_single_preview",
    "worksheet_summarylist_preview",
    "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,width=800,height=700"
  );
}
function popupPrintDialog(){
  window.open(
    "sot_worksheet_summarylist.php?session=<?=$session?>&date_y=<?=@$date_y?>&date_m=<?=@$date_m?>&date_d=<?=@$date_d?>&sel_bldg_cd=<?=$sel_bldg_cd?>
    &sel_ward_cd=<?=$sel_ward_cd?>&sel_ptrm_room_no=<?=$sel_ptrm_room_no?>&sel_team_id=<?=@$sel_team_id?>&sel_ptif_id=<?=@$_REQUEST["sel_ptif_id"]?>&mode=show_print_dialog",
    "worksheet_summarylist_print_dialog",
    "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,width=600,height=300"
  );
}
</script>
<style type="text/css" media="all">
  body { margin:0; color:#000; background-color:#fff; }
  .list {border-collapse:collapse;}
  .list td {border:#5279a5 solid 1px; padding:1px;}
  table.block_in {border-collapse:collapse;}
  table.block_in td {border:#5279a5 solid 0px;}
  img { border: 0; }
  .date_header { background-color:#ffa; }
  .c_red { color:#f05 }
  .apply_header { border: 1px solid #ccc; }
  .timecalcullator table{ border:1px solid #aaa; background-color:#ddd; }
  .timecalcullator td { border:1px solid #aaa; text-align:center; padding:2px 4px; background-color:#fff; cursor:pointer; color:#00f }
  .plane { border:0; border-collapse:collapse; }
  .plane td { border:0; }

  .suggest_div { font-size:12px; padding:4px; background-color:#f7ffd2; border:2px solid #ddff3e; color:#777; white-space:nowrap }
  .suggest_result { background-color:#f7ffd2 }
  .suggest_result th { background:0; border:1px solid #ddff3e; padding:2px 6px; background-color:#f7ffd2 }
  .suggest_result td { background:0; border:1px solid #ddff3e; padding:2px 6px; background-color:#f7ffd2 }

  .inside_table { width:100%; border-collapse:collapse; margin-top:2px }
  .inside_table th { border:1px solid #9bc8ec; text-align:center }
  .inside_table td { border:1px solid #9bc8ec; }
</style>
</head>
<body onload="initcal();">

<? $param = "&date_y=".@$date_y."&date_m=".@$date_m."&date_d=".@$date_d."&sel_bldg_cd=".$sel_bldg_cd."&sel_ward_cd=".$sel_ward_cd."&sel_ptrm_room_no=".$sel_ptrm_room_no."&sel_team_id=".$sel_team_id; ?>

<? summary_common_show_sinryo_top_header($session, $fname, "チーム記録", $param); ?>

<? summary_common_show_main_tab_menu($con, $fname, "看護支援", 1); ?>

<script type="text/javascript">
var ward_selections = {};
var room_selections = {};
var combobox = {};
function sel_changed(node, selval){
  combobox[1] = document.getElementById("sel_bldg_cd");
  combobox[2] = document.getElementById("sel_ward_cd");
  combobox[3] = document.getElementById("sel_ptrm_room_no");
  if (node < 3) combobox[3].options.length = 1;
  if (node < 2) combobox[2].options.length = 1;

  <? // 初期化用 自分コンボを再選択?>
  var cmb = combobox[node];
  if (node > 0 && cmb.value != selval) {
    for(var i=0; i<cmb.options.length; i++) if (cmb.options[i].value==selval) cmb.selectedIndex = i;
  }

  var idx = 1;
  if (node == 1){
    for(var i in ward_selections){
      if (ward_selections[i].bldg_cd != selval) continue;
      combobox[2].options.length = combobox[2].options.length + 1;
      combobox[2].options[idx].text  = ward_selections[i].name;
      combobox[2].options[idx].value = ward_selections[i].code;
      idx++;
    }
  }
  if (node == 2){
    for(var i in room_selections){
      if (room_selections[i].bldg_cd != combobox[1].value) continue;
      if (room_selections[i].ward_cd != selval) continue;
      combobox[3].options.length = combobox[3].options.length + 1;
      combobox[3].options[idx].text  = room_selections[i].name;
      combobox[3].options[idx].value = room_selections[i].code;
      idx++;
    }
  }
}

function searchByDay(day_incriment){
  var yy = document.search.date_y;
  var mm = document.search.date_m;
  var dd = document.search.date_d;
  var y = (parseInt(yy.value,10) ? yy.value : yy.options[1].value);
  var m = (parseInt(mm.value,10) ? mm.value : 1)*1;
  var d = (parseInt(dd.value,10) ? dd.value : 1)*1;
  var dt = new Date(y, m-1, d);
  dt.setTime(dt.getTime() + day_incriment * 86400000);

  var idx_y = yy.options[1].value - dt.getFullYear() + 1;
  if (idx_y < 1) return;
  yy.selectedIndex = idx_y;
  mm.selectedIndex = dt.getMonth()+1;
  dd.selectedIndex = dt.getDate();
  document.search.submit();
}

function searchByToday(){
  var dt = new Date();
  document.search.date_y.selectedIndex = document.search.date_y.options[1].value - dt.getFullYear() + 1;
  document.search.date_m.selectedIndex = dt.getMonth()+1;
  document.search.date_d.selectedIndex = dt.getDate();
  document.search.submit();
}

<? //++++++++++++++++++++++++++++++++++++++?>
<? // 患者検索ダイアログのポップアップ     ?>
<? //++++++++++++++++++++++++++++++++++++++?>
function popupKanjaSelect(){
  window.open(
    "sot_kanja_select.php?session=<?=$session?>&js_callback=callbackKanjaSelect&sel_bldg_cd=1&sel_ward_cd=<?=@$ptif_ward_cd?>&sel_ptif_id=<?=@$sel_ptif_id?>&sel_sort_by_byoto=1",
    "",
    "directories=no,location=no,menubar=no,resizable=yes,scrollbars=no,status=no,titlebar=no,toolbar=no,width=630,height=480"
  );
}
<? //++++++++++++++++++++++++++++++++++++++?>
<? // 患者検索結果を受けるコールバック関数 ?>
<? //++++++++++++++++++++++++++++++++++++++?>
function callbackKanjaSelect(ptif_id){
  document.getElementById("sel_ptif_id").value = ptif_id;
  document.search.submit();
}
</script>
<?
$EMPTY_OPTION = '<option value="">　　　　</option>';

// チーム一覧
$team_options = array();
$sel = $c_sot_util->select_from_table("select team_id, team_nm from teammst where team_del_flg = 'f'");
while($row = pg_fetch_array($sel)){
  $team_options[] =
  $selected = ($row["team_id"] == $sel_team_id ? " selected" : "");
  $team_options[] = '<option value="'.$row["team_id"].'"'.$selected.'>'.$row["team_nm"].'</option>';
}

// 病棟選択肢 JavaScript変数として出力する
$ary = array();
foreach($ward_list as $_bldg_cd => $_ward){
  foreach($_ward as $_ward_cd => $_ward_name){
    $ary[] = '{"bldg_cd":"'.$_bldg_cd.'","code":"'.$_ward_cd.'","name":"'.$_ward_name.'"}';
  }
}
echo "<script type='text/javascript'>ward_selections = [". implode(",", $ary) . "];\n</script>\n";

// 病室選択肢 JavaScript変数として出力する
$ary = array();
$sel = $c_sot_util->select_from_table("select bldg_cd, ward_cd, ptrm_room_no, ptrm_name from ptrmmst where ptrm_del_flg = 'f' order by ptrm_room_no");
$mst = (array)pg_fetch_all($sel);
foreach($mst as $key => $row) $ary[] = '{"bldg_cd":"'.$row["bldg_cd"].'","ward_cd":"'.$row["ward_cd"].'","code":"'.$row["ptrm_room_no"].'","name":"'.$row["ptrm_name"].'"}';
echo "<script type='text/javascript'>room_selections = [". implode(",", $ary) . "];\n</script>\n";
?>

<form name="search" action="sot_worksheet_summarylist.php?session=<?=$session?>" method="post">
<input type="hidden" name="session" value="<? echo($session); ?>">
<table class="list_table" style="margin-top:4px; margin-bottom:8px" cellspacing="1">
  <tr>
    <th>事業所(棟)</th>
    <td><select onchange="sel_changed(1, this.value);" name="sel_bldg_cd" id="sel_bldg_cd"><?=implode("\n", $bldg_data_options);?></select></td>
    <th>病棟</th>
    <td><select onchange="sel_changed(2, this.value);" name="sel_ward_cd" id="sel_ward_cd"><?=$EMPTY_OPTION?></select></td>
    <th>病室</th>
    <td><select name="sel_ptrm_room_no" id="sel_ptrm_room_no"><?=$EMPTY_OPTION?></select></td>
    <!--th>患者氏名</th>
    <td><input type="text" name="sel_ptif_name" id="sel_ptif_name" style="width:200px" value="<?=@$sel_ptif_name?>" /></td-->
    <script type="text/javascript">sel_changed(1, document.getElementById("sel_bldg_cd").value);</script>
    <script type="text/javascript">sel_changed(2, "<?=$sel_ward_cd?>");</script>
    <script type="text/javascript">sel_changed(3, "<?=$sel_ptrm_room_no?>");</script>
    <th bgcolor="#fefcdf">チーム</th>
    <td>
      <select name="sel_team_id" id="sel_team_id"><?=$EMPTY_OPTION?><?=implode("\n", $team_options)?></select>
    </td>
  </tr>
  <tr>
    <th>日付</th>
    <td colspan="7">
      <div id="cal1Container" style="margin-top:24px; position:absolute;display:none;z-index:10000;"></div>
      <div style="float:left">
        <div style="float:left">
          <select id="date_y1" name="date_y"><option value="-"><? show_select_years_span(date("Y") - 14, date("Y") + 1, $date_y, false); ?></select>&nbsp;/
          <select id="date_m1" name="date_m"><? show_select_months($date_m, true); ?></select>&nbsp;/
          <select id="date_d1" name="date_d"><? show_select_days($date_d, true); ?></select>
        </div>
        <div style="float:left">
          <img src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal1();"/><br>
        </div>
        <div style="float:left; padding-left:4px">
          <input type="button" onclick="searchByDay('-7');" value="≪" title="１週間前"/><!--
        --><input type="button" onclick="searchByDay('-1');" value="<" title="１日前"/><!--
        --><input type="button" onclick="searchByDay('1');" value=">" title="１日後"/><!--
        --><input type="button" onclick="searchByDay('7');" value="≫" title="１週間後"/>
        </div>
        <div style="float:left;">
          <input type="button" onclick="searchByToday();" value="本日"/>
        </div>
        <div style="clear:both"></div>
      </div>
      <div style="float:right">
        <input type="button" onclick="document.search.submit();" value="検索"/>
        <input type="button" onclick="popupPrintDialog();" value="一括印刷"/>
        <input type="button" onclick="popupPrintPreview();" value="印刷"/>
      </div>
      <div style="clear:both"></div>
    </td>
  </tr>
</table>

<? summary_common_show_worksheet_tab_menu("チーム記録", $param); ?>

<? //=========================================================================?>
<? // 検索指定領域（患者）                                                    ?>
<? //=========================================================================?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<table class="list_table" cellspacing="1">
  <tr>
    <th width="50" style="white-space:nowrap">患者ID</th>
    <td width="">
      <div style="float:left">
          <input type="text" style="width:100px; ime-mode:disabled" name="sel_ptif_id" id="sel_ptif_id" value="<?=@$_REQUEST["sel_ptif_id"]?>"/><!--
      --><span id="sel_ptif_name" style="padding-left:2px; padding-right:2px"><?=$ptif_name?></span><!--
      --><input type="button" onclick="popupKanjaSelect();" value="患者検索"/><!--
      --><input type="button" onclick="document.search.sel_ptif_id.value=''; document.search.submit();" value="クリア"/>
         <label><input type="radio" name="switching_soap" id="switch1" onclick="radio_switch()">SOAP</input></label>

<label><input type="radio" name="switching_soap" id="switch2" checked>中間サマリ</input></label>
<script>
  document.getElementById("switch2").checked = true;

  function radio_switch(){
    location.href = "sot_worksheet_soaplist.php?session=<?=@$session?>&date_y=<?=@$date_y?>&date_m=<?=@$date_m?>&date_d=<?=@$date_d?>&sel_bldg_cd=<?=@$sel_bldg_cd?>&sel_ward_cd=<?=@$sel_ward_cd?>&sel_ptrm_room_no=<?=@$sel_ptrm_room_no?>&sel_team_id=<?=@$sel_team_id?>&sel_ptif_id=<?=@$sel_ptif_id?>";
  }
</script>

      </div>
      <div style="clear:both"></div>
    </td>
    <th width="70" style="text-align:center; white-space:nowrap">
      患者名検索
    </th>
    <td width="220" style="text-align:right">
      <div style="float:left">
        <input type="text" style="width:200px" onkeyup="inquirySuggest(this.value, '')" onfocus="inquirySuggest(this.value, 'show');" onblur="inquirySuggest(this.value, 'hide');" /><br />
        <div id="patient_suggest_div" style="position:absolute; width:200px; z-index:1000; display:none;">
          <div id="patient_suggest_script" style="padding-top:4px; text-align:left;"></div>
        </div>
      </div>
      <br style="clear:both" />
    </td>
  </tr>
</table>

</form>

<? //=========================================================================?>
<? // 患者名サジェスト                                                        ?>
<? //=========================================================================?>
<script type="text/javascript">
<?//--------------------------------------------?>
<?// 患者名サジェスト：サーバへ送信             ?>
<?//--------------------------------------------?>
var suggestExistResult = false;
var suggestLinkClicked = false;
var currentSuggestString = "";
var ajaxObj = null;
function inquirySuggest(ptif_name, disp){
  var curDisp = document.getElementById("patient_suggest_div").style.display;
  var newDisp = (disp=="hide" && !suggestExistResult ? "none" : "");
  if (curDisp != newDisp) {
    document.getElementById("patient_suggest_div").style.display = newDisp;
  }
  if (disp=="hide") {
    currentSuggestString = "";
    return;
  }

  if (ptif_name=="") {
    suggestExistResult = false;
    document.getElementById("patient_suggest_script").innerHTML = '' +
      '<div class="suggest_div">'+
      '・患者名/患者IDを指定してください<br/>'+
      '・指定事業所/病棟範囲内で検索<br/>'+
      '・本日入院中の患者、該当10件まで</div>';
    currentSuggestString = "";
    return;
  }

  if (currentSuggestString == ptif_name) return;
  currentSuggestString = ptif_name;

  var postval =
    "session=<?=$session?>"+
    "&sel_ptif_name="+encodeURIComponent(encodeURIComponent(ptif_name)) +
    "&sel_bldg_cd="+document.search.sel_bldg_cd.value +
    "&sel_ward_cd="+document.search.sel_ward_cd.value;
  if (window.XMLHttpRequest) ajaxObj = new XMLHttpRequest();
  else {
    try { ajaxObj = new ActiveXObject("Msxml2.XMLHTTP"); }
    catch(e) { ajaxObj = new ActiveXObject("Microsoft.XMLHTTP"); }
  }
  ajaxObj.onreadystatechange = returnInquirySuggest;
  ajaxObj.open("POST", "sot_kanja_suggest.php", true);
  ajaxObj.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  ajaxObj.send(postval);
}
<?//--------------------------------------------?>
<?// 患者名サジェスト：サーバからの応答リスト   ?>
<?//--------------------------------------------?>
function returnInquirySuggest(){
  if (ajaxObj == null) return;
  if (ajaxObj.readyState != 4) return;
  if (typeof(ajaxObj.status) != "number") return;
  if (ajaxObj.status != 200) return;
  if (!ajaxObj.responseText) return;
  try{ var ret = false; eval("ret = " + ajaxObj.responseText); if (ret.error) { alert(ret.error); return; }}
  catch(e){ alert("[response invalid]\n"+ajaxObj.responseText); return; }
  if (!ret) return;
  var out = [];
  if (!ret.list_length) {
    suggestExistResult = false;
    document.getElementById("patient_suggest_script").innerHTML = "<div class='suggest_div'>（該当はありません）</div>";
    return;
  }
  out.push('<table class="suggest_result">');
  for (var i = 0; i < ret.list_length; i++){
    out.push('<tr><th>'+ret.list[i].id+'</th><td><a href="" class="always_blue" onclick="suggestLinkClick(\''+ret.list[i].id+'\'); return false;">'+ret.list[i].name+'</a></td></tr>');
  }
  out.push("</table>");
  document.getElementById("patient_suggest_script").innerHTML = out.join("");
  suggestExistResult = true;
}

<?//--------------------------------------------?>
<?// 患者名サジェスト：サジェストの患者IDのクリック   ?>
<?//--------------------------------------------?>
function suggestLinkClick(ptif_id){
  suggestLinkClicked = true;
  callbackKanjaSelect(ptif_id);
}

</script>

<? if (!@$_REQUEST["sel_ptif_id"]) { ?>
  <div style="padding:10px; color:#aaa; text-align:center">患者を指定してください。</div>
<? } else if (count($pt_data)){ ?>
  <table class="listp_table print_target_table" style="margin-top:4px">
  <tr>
    <th style="width:10%">日時</th>
    <th style="width:10%">記録区分</th>
    <th style="width:80%">内容</th>
  </tr>
<? foreach($pt_data as $key => $row){ ?>
<?
    $nichiji = str_replace(" ", "<br>" , $row["ymdhm"]) ;
?>
    <tr>
    <td style="white-space:nowrap; vertical-align:top"><?=$nichiji?></td>
    <td style="white-space:nowrap; vertical-align:top"><?=h($row["diag_div"])?></td>
    <td style="vertical-align:top; padding:4px;">
      <div style="margin:3px">
        <span style="padding:2px; background-color:#ccc">記載者</span>&nbsp;<?=h($row["emp_lt_nm"]." ".$row["emp_ft_nm"]." (".$row["job_nm"].")")?>

     </div>
    <div><?=h(@$row["tmpl_name"])?></div>
      <table class="inside_table">
            <? if (trim($row["dataN"])!="") { ?><tr><td><?=$row["dataN"]?></td></tr><? } ?>
      </table>
    </td>
    </tr>
  <? } ?>
  </table>
<? } ?>
<?
function getSoapFromXmlData($nodes, $key)
{
  $soap = @$nodes[$key."_tm"]->value ;
  if (!$soap) $soap = @$nodes[$key]->value ;

  // ｢×｣｢÷｣などhtml_entity_decodeでうまくデコードできない文字はここで変換する(必要に応じて書き足す)
  $soap = str_replace(    "&nbsp;",   " ",  $soap) ;
  $soap = mb_ereg_replace("&times;",  "×", $soap) ;
  $soap = mb_ereg_replace("&divide;", "÷", $soap) ;

  // html_entity_decodeでデコードする
  $soap = html_entity_decode($soap) ;

  $soap = str_replace("<",               "&lt;",      $soap) ;
  $soap = str_replace(">",               "&gt;",      $soap) ;
  $soap = str_replace("&lt;br /&gt;",    "<br />",    $soap) ;
  $soap = str_replace("&lt;strike&gt;",  "<strike>",  $soap) ;
  $soap = str_replace("&lt;/strike&gt;", "</strike>", $soap) ;
  $soap = str_replace("\n",              "<br />",    $soap) ;

  return $soap ;
}

function compare_pt_data($a, $b) {
    if ($a["ptif_id"] === $b["ptif_id"]) {
        return strcmp($a["ymdhmstr"], $b["ymdhmstr"]);
    }

    if ($a["key"] < $b["key"]) {
        return -1;
    } else if ($a["key"] > $b["key"]) {
        return 1;
    } else {
        return 0;
    }
}
?>
</body>
</html>
