<?
require_once("about_comedix.php");
ob_start();
require_once("atdbk_close_class.php");
ob_end_clean();

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    pq_close($con);
    ajax_server_erorr();
    exit;
}

// データベースに接続
$con = connect2db($fname);

$atdbk_close_class = new atdbk_close_class($con, $fname);

$close_stat = $atdbk_close_class->get_close_stat($emp_id, $start_date, $end_date);


//レスポンスデータ作成
$response_data = "ajax=success\n";

$p_key = "close_stat";
$p_value = $$p_key;
$response_data .= "$p_key=$p_value\n";

print $response_data;
// データベース切断
pg_close($con);

//サーバーエラー
function ajax_server_erorr()
{
    print "ajax=error\n";
    exit;
}

?>
