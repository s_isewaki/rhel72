<?
require("about_session.php");
require("about_authority.php");
require("news_common.ini");
require("webmail/config/config.php");
require_once("Cmx.php");
require_once("aclg_set.php");
?>
<body>
<?
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$auth_id = ($path == "2") ? 46 : 24;
$checkauth = check_authority($session, $auth_id, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// トランザクションを開始
pg_query($con, "begin");

// ログインユーザの職員IDを取得
$sql = "select emp_id, emp_lt_nm, emp_ft_nm, get_mail_login_id(emp_id) from empmst";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");
$emp_nm = pg_fetch_result($sel, 0, "emp_lt_nm") . pg_fetch_result($sel, 0, "emp_ft_nm");
$emp_login_id = pg_fetch_result($sel, 0, 3);


$reg_date_time = date("YmdHi");

// お知らせ情報の取得
$sql = "select news_title, news_add_category, news.job_id as job_id1, news.st_id_cnt as st_id_cnt1, record_flg, newscate.standard_cate_id, newscate.job_id as job_id2, newscate.st_id_cnt as st_id_cnt2, newscate.newscate_id, newscate.newscate_name, news.date, newsnotice.notice_name, srcempmst.emp_lt_nm as src_emp_lt_nm, srcempmst.emp_ft_nm as src_emp_ft_nm, regempmst.emp_lt_nm as reg_emp_lt_nm, regempmst.emp_ft_nm as reg_emp_ft_nm from news inner join newscate on news.news_add_category = newscate.newscate_id left join newsnotice on news.notice_id = newsnotice.notice_id left join empmst srcempmst on news.src_emp_id = srcempmst.emp_id  left join empmst regempmst on news.emp_id = regempmst.emp_id";
$cond = "where news_id = $news_id";
$sel_news = select_from_table($con, $sql, $cond, $fname);
if ($sel_news == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$news_title = pg_fetch_result($sel_news, 0, "news_title");
$news_category = pg_fetch_result($sel_news, 0, "news_add_category");
$standard_cate_id = pg_fetch_result($sel_news, 0, "standard_cate_id");
$newscate_id = pg_fetch_result($sel_news, 0, "newscate_id");
$newscate_name = pg_fetch_result($sel_news, 0, "newscate_name");
$record_flg = pg_fetch_result($sel_news, 0, "record_flg");
$reg_date_time = pg_fetch_result($sel_news, 0, "date");
$notice_name = pg_fetch_result($sel_news, 0, "notice_name");
$src_emp_nm = pg_fetch_result($sel_news, 0, "src_emp_lt_nm") . " " . pg_fetch_result($sel_news, 0, "src_emp_ft_nm");
$reg_emp_nm = pg_fetch_result($sel_news, 0, "reg_emp_lt_nm") . " " . pg_fetch_result($sel_news, 0, "reg_emp_ft_nm");

// 標準カテゴリの場合
if ($news_category <= "3") {
	$tmp_news_category = $news_category;
	$st_id_cnt = pg_fetch_result($sel_news, 0, "st_id_cnt1");
	$job_id = pg_fetch_result($sel_news, 0, "job_id1");
} else {
// 追加カテゴリの場合
	$tmp_news_category = $standard_cate_id;
	$st_id_cnt = pg_fetch_result($sel_news, 0, "st_id_cnt2");
	$job_id = pg_fetch_result($sel_news, 0, "job_id2");
}
// 役職指定数がある場合条件追加
// 役職ID
$st_ids = "";
$st_nms = "";
$st_cond = "";
if ($st_id_cnt > 0) {
	if ($news_category <= "3") {
		$newsst_info = get_newsst_info($con, $fname, $news_id);
	} else {
		$newsst_info = get_newscatest_info($con, $fname, $newscate_id);
	}
	foreach ($newsst_info as $st_id => $st_nm) {
		if ($st_ids != "") {
			$st_ids .= ",";
		}
		$st_ids .= $st_id;
		if ($st_nms != "") {
			$st_nms .= "、";
		}
		$st_nms .= $st_nm;
	}
	// 役職条件
	$st_cond .= " and (empmst.emp_st in ($st_ids) or empmst.emp_id in (select emp_id from concurrent where emp_st in ($st_ids)))";
}


// メール通知対象者取得
$mail_info = get_mail_info($con, $fname, $news_id, $news_category, $job_id, $st_cond, $standard_cate_id, $newscate_id, $mode);

// メール通知
if (count($mail_info) > 0) {
	$date_time = preg_replace("/(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})/", "$1/$2/$3 $4:$5", $reg_date_time);
	// メール内容編集
	$subject = "[CoMedix] お知らせを確認してください";

	// サブカテゴリ
	$news_sub_category = "";
	if ($news_category == "2" || $news_category == "3") {
		$news_sub_category = get_sub_category_label($con, $news_id, $fname);
		$news_sub_category = str_replace("<br>", "、", $news_sub_category);
		$news_sub_category = substr($news_sub_category, 0, -2);
		$newscate_name .= " $news_sub_category";
	}

	$conf_message = $limit.$send_message;

	$message = "";
	$message .= "タイトル：{$news_title}\n";
	$message .= "対象カテゴリ：{$newscate_name}\n";
	if ($st_nms != "") {
		$message .= "対象役職者　：{$st_nms}\n";
	}
	if ($notice_name != "") {
		$message .= "通達区分：{$notice_name}\n";
	}
	if ($src_emp_nm != " ") {
		$message .= "発信者　：{$src_emp_nm}\n";
	} else {
		$message .= "登録者　：{$reg_emp_nm}\n";
	}
	$message .= "登録日時：{$date_time}\n";
	$message .= " \n{$conf_message}\n";
	$additional_headers = "From: ".mb_encode_mimeheader (mb_convert_encoding($emp_nm,"ISO-2022-JP","AUTO")) ." <$emp_login_id@$domain>";

	// メール送信
	foreach ($mail_info as $tmp_emp_id => $tmp_mail_data) {
		$to_login_id = $tmp_mail_data["mailaddr"];
		$tmp_emp_nm = $tmp_mail_data["name"];
		$to_addr = mb_encode_mimeheader (mb_convert_encoding($tmp_emp_nm,"ISO-2022-JP","AUTO")) ." <$to_login_id@$domain>";
		mb_send_mail($to_addr, $subject, $message, $additional_headers);
		// DB登録
		$sql = "insert into newsmail (news_id, emp_id, send_time) values (";
		$content = array($news_id, $tmp_emp_id, date("YmdHi"));
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 一覧画面に遷移
if ($path == "2") {
	echo("<script type=\"text/javascript\">location.href = 'newsuser_refer_list.php?session=$session&news_id=$news_id&mode=$mode&page=1';</script>");
} else {
	echo("<script type=\"text/javascript\">location.href = 'news_refer_list.php?session=$session&news_id=$news_id&mode=$mode&page=1';</script>");
}
?>
</body>
<?
// メール通知用情報取得、emp_idをキーとする配列に、アドレス(mailaddr)と名前(name)を設定した配列を設定
function get_mail_info($con, $fname, $news_id, $news_category, $job_id, $st_cond, $standard_cate_id, $newscate_id, $mode) {

	$mail_info = array();

	$sql = "select emp_id, get_mail_login_id(emp_id) as mail_login_id, emp_lt_nm, emp_ft_nm from empmst";

	// 標準カテゴリまたは回覧板の場合
	if ($news_category <= "3" || $news_category == "10000") {
		$tmp_news_category = $news_category;
	} else {
	// 追加カテゴリの場合
		$tmp_news_category = $standard_cate_id;
	}
	switch ($tmp_news_category) {
	case "1":  // 全館
//		$cond = "where emp_id in (select emp_id from authmst where emp_del_flg = 'f')";
		$cond = "where exists (select * from authmst where authmst.emp_del_flg = 'f' and authmst.emp_id = empmst.emp_id)";
		break;
	case "2":  // 部署
		// 標準カテゴリの場合
		if ($news_category <= "3") {
			$cond = "where exists (select * from newsdept where newsdept.news_id = $news_id and ((newsdept.class_id = empmst.emp_class) or (exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_class = newsdept.class_id))) and ((newsdept.atrb_id = empmst.emp_attribute) or (newsdept.atrb_id is null) or (exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_attribute = newsdept.atrb_id))) and ((newsdept.dept_id = empmst.emp_dept) or (newsdept.dept_id is null) or (exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_dept = newsdept.dept_id)))) and exists (select * from authmst where authmst.emp_del_flg = 'f' and authmst.emp_id = empmst.emp_id)";
		} else {
		// 追加カテゴリの場合
			$cond = "where exists (select * from newscatedept where newscatedept.newscate_id = $newscate_id and ((newscatedept.class_id = empmst.emp_class) or (exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_class = newscatedept.class_id))) and ((newscatedept.atrb_id = empmst.emp_attribute) or (newscatedept.atrb_id is null) or (exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_attribute = newscatedept.atrb_id))) and ((newscatedept.dept_id = empmst.emp_dept) or (newscatedept.dept_id is null) or (exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_dept = newscatedept.dept_id)))) and exists (select * from authmst where authmst.emp_del_flg = 'f' and authmst.emp_id = empmst.emp_id)";
		}
		break;
	case "3":  // 職種
		// 職種が1件の場合
		if ($job_id != "") {
			$cond = "where emp_job = $job_id";
		} else {
			// 標準カテゴリの場合
			if ($news_category <= "3") {
				$cond = "where exists (select * from newsjob where newsjob.news_id = $news_id and empmst.emp_job = newsjob.job_id)";
			} else {
				$cond = "where exists (select * from newscatejob where newscatejob.newscate_id = $newscate_id and empmst.emp_job = newscatejob.job_id)";
			}
		}
		$cond .= " and exists (select * from authmst where authmst.emp_del_flg = 'f' and authmst.emp_id = empmst.emp_id)";
		break;
	case "10000":  // 回覧板
		$cond = "where exists (select * from authmst where authmst.emp_del_flg = 'f' and authmst.emp_id = empmst.emp_id)";
		$cond .= " and exists (select * from newscomment where newscomment.news_id = $news_id and newscomment.emp_id = empmst.emp_id)";
		break;
	}

	switch ($mode) {
		// 未確認
		case "2": 
			$cond .= " and not exists (select * from newsref where newsref.news_id = $news_id and newsref.emp_id = empmst.emp_id and newsref.ref_time is not null) ";
			break;
		// 未読
		case "5":
			$cond .= " and not exists (select * from newsref where newsref.news_id = $news_id and newsref.emp_id = empmst.emp_id and newsref.read_flg='t') ";
			break;
		// 既読・未確認
		case "7":
			$cond .= " and exists (select * from newsref where newsref.news_id = $news_id and newsref.emp_id = empmst.emp_id and newsref.read_flg = 't' and newsref.ref_time is null) ";
			break;
	}
        
	// 役職条件追加
	if ($st_cond != "") {
		$cond .= $st_cond;
	}


	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$tmp_emp_id = $row["emp_id"];
		$mail_data = array();
		$mail_data["mailaddr"] = $row["mail_login_id"];
		$mail_data["name"]  = $row["emp_lt_nm"].$row["emp_ft_nm"];
		$mail_info["$tmp_emp_id"] = $mail_data;
	}

	return $mail_info;
}

?>
