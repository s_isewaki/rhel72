<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">

<?php 
	$fname=$PHP_SELF;
	
	require("./conf/sql.inf");						//sqlの一覧ファイルを読み込む
	require_once("./about_postgres.php");			//db処理のためのファイルを読み込む
	require_once("./about_validation.php");			//db処理のためのファイルを読み込む
	require("./about_session.php");
	
//----------DB処理の開始-----------
	$con = connect2db($fname);
	

//----------トランズアクションの開始----------
	pg_exec($con,"begin transaction");
	
//----------職員ＩＤの取得-----------
	$cond = " where session_id='$session'";
	$sel = select_from_table($con,$SQL1,$cond,$fname);
	
	if($sel==0){
		pg_exec($con,"rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	
	$emp_id=pg_result($sel,0,"emp_id");	//職員ＩＤの取得

//---------schdprojectから削除する---------
	$cond = "where emp_id='$emp_id' and ownother='2'";
	$del = delete_from_table($con,$SQL174,$cond,$fname);
	
	if($del==0){
		pg_exec($con,"rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

//---------schdprojectへ挿入する---------
	
	$cnt  = count($pjt_ids);
	for($i=0;$i<$cnt;$i++){
		if(trim($pjt_ids[$i]) != ""){
		$content = array($pjt_ids[$i],$emp_id,2);
		$in = insert_into_table($con,$SQL175,$content,$fname);
		
			if($in==0){	
				pg_exec($con,"rollback");
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}
		}
	}	
	
	pg_exec($con,"commit");	
	pg_close($con);  //Close this connection 
	
	echo("<script language='javascript'>window.opener.location.reload();</script>");
	echo("<script language='javascript'>self.close();</script>");
	
?>