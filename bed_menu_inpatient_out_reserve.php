<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>病床管理 | 退院予定情報</title>
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_select_values.ini");
require_once("get_values.ini");
require_once("show_in_search.ini");
require_once("inpatient_common.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 診療科登録権限を取得
$section_admin_auth = check_authority($session, 28, $fname);

// 病棟登録権限を取得
$ward_admin_auth = check_authority($session, 21, $fname);

// データベースに接続
$con = connect2db($fname);

// 患者氏名の取得
$sql = "select ptif_lt_kaj_nm, ptif_ft_kaj_nm from ptifmst";
$cond = "where ptif_id = '$pt_id' and ptif_del_flg = 'f'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$lt_kj_nm = pg_fetch_result($sel, 0, "ptif_lt_kaj_nm");
$ft_kj_nm = pg_fetch_result($sel, 0, "ptif_ft_kaj_nm");

// 初期表示時
if ($back != "t") {

	// 退院予定情報の取得
	$sql = "select * from inptmst";
	$cond = "where ptif_id = '$pt_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$out_dt = pg_fetch_result($sel, 0, "inpt_out_res_dt");
	$out_tm = pg_fetch_result($sel, 0, "inpt_out_res_tm");
	$result = pg_fetch_result($sel, 0, "inpt_result");
	$result_dtl = pg_fetch_result($sel, 0, "inpt_result_dtl");
	$out_rsn_cd = pg_fetch_result($sel, 0, "inpt_out_rsn_cd");
	$out_pos_cd = pg_fetch_result($sel, 0, "inpt_out_pos_cd");
	$out_pos_dtl = pg_fetch_result($sel, 0, "inpt_out_pos_dtl");
	$fd_end = pg_fetch_result($sel, 0, "inpt_fd_end");
	$comment = pg_fetch_result($sel, 0, "inpt_out_comment");
	$out_inst_cd = pg_fetch_result($sel, 0, "inpt_out_inst_cd");
	$out_sect_cd = pg_fetch_result($sel, 0, "inpt_out_sect_cd");
	$out_doctor_no = pg_fetch_result($sel, 0, "inpt_out_doctor_no");
	$out_city = pg_fetch_result($sel, 0, "inpt_out_city");
	$pr_inst_cd = pg_fetch_result($sel, 0, "inpt_pr_inst_cd");
	$pr_sect_cd = pg_fetch_result($sel, 0, "inpt_pr_sect_cd");
	$pr_doctor_no = pg_fetch_result($sel, 0, "inpt_pr_doctor_no");
	$back_bldg_cd = pg_fetch_result($sel, 0, "inpt_back_bldg_cd");
	$back_ward_cd = pg_fetch_result($sel, 0, "inpt_back_ward_cd");
	$ptrm = pg_fetch_result($sel, 0, "inpt_back_ptrm_room_no");
	$bed = pg_fetch_result($sel, 0, "inpt_back_bed_no");
	$back_in_dt = pg_fetch_result($sel, 0, "inpt_back_in_dt");
	$back_in_tm = pg_fetch_result($sel, 0, "inpt_back_in_tm");
	$back_sect = pg_fetch_result($sel, 0, "inpt_back_sect_id");
	$back_doc = pg_fetch_result($sel, 0, "inpt_back_dr_id");
	$back_change_purpose = pg_fetch_result($sel, 0, "inpt_back_change_purpose");

	$out_yr = substr($out_dt, 0, 4);
	$out_mon = substr($out_dt, 4, 2);
	$out_day = substr($out_dt, 6, 2);
	$out_hr = substr($out_tm, 0, 2);
	$out_min = substr($out_tm, 2, 2);
	$ward = ($back_bldg_cd != "" && $back_ward_cd != "") ? "{$back_bldg_cd}-{$back_ward_cd}" : "";
	$back_in_yr = substr($back_in_dt, 0, 4);
	$back_in_mon = substr($back_in_dt, 4, 2);
	$back_in_day = substr($back_in_dt, 6, 2);
	$back_in_hr = substr($back_in_tm, 0, 2);
	$back_in_min = substr($back_in_tm, 2, 2);
}

// 退院理由マスタを取得
list($out_rsn_rireki, $out_rsn_master) = get_rireki_index_and_master($con, "A308", $fname);

// 退院先マスタを取得
list($out_pos_rireki, $out_pos_master) = get_rireki_index_and_master($con, "A307", $fname);

// 医療機関マスタを取得
$sql = "select a.mst_cd, a.mst_name, b.rireki_index, a.address1 from institemmst a left join (select mst_cd, max(rireki_index) as rireki_index from institemrireki group by mst_cd) b on b.mst_cd = a.mst_cd";
$cond = "order by a.mst_cd";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$insts = array();
while ($row = pg_fetch_array($sel)) {
	$insts[$row["mst_cd"]] = array(
		"name"   => $row["mst_name"],
		"rireki" => $row["rireki_index"],
		"city"   => $row["address1"]
	);
}

// 診療科マスタを取得（最新の履歴のみ）
$sql = "select a.* from institemrireki a inner join (select mst_cd, max(rireki_index) as rireki_index from institemrireki group by mst_cd) b on b.mst_cd = a.mst_cd and b.rireki_index = a.rireki_index";
$cond = "where a.disp_flg order by a.disp_order";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
while ($row = pg_fetch_array($sel)) {
	$insts[$row["mst_cd"]]["sects"][$row["item_cd"]]["name"] = $row["item_name"];
	for ($i = 1; $i <= 10; $i++) {
		if ($row["doctor_name$i"] == "" || $row["doctor_hide$i"] == "t") {
			continue;
		}
		$insts[$row["mst_cd"]]["sects"][$row["item_cd"]]["doctors"][$i] = $row["doctor_name$i"];
	}
}

// 病棟一覧の取得
$sql = "select bldg_cd, ward_cd, ward_name from wdmst";
$cond = "where ward_del_flg = 'f' order by bldg_cd, ward_cd";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$wards = array();
while ($row = pg_fetch_array($sel)) {
	$wards["{$row["bldg_cd"]}-{$row["ward_cd"]}"] = array("name" => $row["ward_name"]);
}

// 病室一覧の取得
$sql = "select bldg_cd, ward_cd, ptrm_room_no, ptrm_name, ptrm_bed_chg, ptrm_bed_cur from ptrmmst";
$cond = "where ptrm_del_flg = 'f' order by ptrm_room_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
while ($row = pg_fetch_array($sel)) {
	$tmp_ward_code = "{$row["bldg_cd"]}-{$row["ward_cd"]}";
	if (!isset($wards[$tmp_ward_code])) {continue;}

	$tmp_ptrm_name = $row["ptrm_name"];
	$tmp_ptrm_charge = $row["ptrm_bed_chg"];
	if ($tmp_ptrm_charge > 0) {
		$tmp_ptrm_name .= "（差額" . number_format($tmp_ptrm_charge) . "円）";
	}

	$wards[$tmp_ward_code]["ptrms"][$row["ptrm_room_no"]] =
			array("name" => $tmp_ptrm_name, "bed_count" => $row["ptrm_bed_cur"]);
}

// 診療科一覧の取得
$sql = "select sect_id, sect_nm from sectmst";
$cond = "where sect_del_flg = 'f' order by sect_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$sects = array();
while ($row = pg_fetch_array($sel)) {
	$sects[$row["sect_id"]] = array("name" => $row["sect_nm"]);
}

// 主治医一覧の取得
$sql = "select sect_id, dr_id, dr_nm from drmst";
$cond = "where dr_del_flg = 'f' order by dr_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
while ($row = pg_fetch_array($sel)) {
	if (!isset($sects[$row["sect_id"]])) {continue;}
	$sects[$row["sect_id"]]["doctors"][$row["dr_id"]] = $row["dr_nm"];
}
unset($sel);

// 診療科情報、主治医情報を配列に格納
list($arr_sect, $arr_sectdr) = get_sectdr($con, $fname);

$url_pt_nm = urlencode($pt_nm);

$bedundec1 = ($ward == 0) ? "t" : "f";
$bedundec2 = ($ptrm == 0) ? "t" : "f";
$bedundec3 = ($bed == 0) ? "t" : "f";

// チェック情報を取得
$sql = "select * from bedcheckout";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
for ($i = 1; $i <= 9; $i++) {
	$varname = "display$i";
	$$varname = pg_fetch_result($sel, 0, "display$i");
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function initPage() {
	sectOnChange2('<? echo($search_doc); ?>');

	switch ('<? echo($out_inst_cd); ?>') {
<? foreach ($insts as $tmp_inst_cd => $tmp_insts) { ?>
	case '<? echo($tmp_inst_cd); ?>':
		if (document.inpt.out_inst_name) {
			document.inpt.out_inst_name.value = '<? echo($tmp_insts["name"]); ?>';
		}
		document.inpt.out_sect_rireki.value = '<? echo($tmp_insts["rireki"]); ?>';
		break;
<? } ?>
	}
	setSectOptions('<? echo($out_sect_cd); ?>', '<? echo($out_doctor_no); ?>');

	switch ('<? echo($pr_inst_cd); ?>') {
<? foreach ($insts as $tmp_inst_cd => $tmp_inst) { ?>
	case '<? echo($tmp_inst_cd); ?>':
		if (document.inpt.pr_inst_name) {
			document.inpt.pr_inst_name.value = '<? echo($tmp_inst["name"]); ?>';
		}
		document.inpt.pr_sect_rireki.value = '<? echo($tmp_inst["rireki"]); ?>';
		break;
<? } ?>
	}
	setPrSectOptions('<? echo($pr_sect_cd); ?>', '<? echo($pr_doctor_no); ?>');

	setBackWardOptions('<? echo($ward); ?>', '<? echo($ptrm); ?>', '<? echo($bed); ?>');
	bedundec1OnClick();
	setBackSectOptions('<? echo($back_sect); ?>', '<? echo($back_doc); ?>');
}

function sectOnChange2(doc) {
	var sect_id = document.search.search_sect.value;

	// 主治医セレクトボックスのオプションを全削除
	deleteAllOptions(document.search.search_doc);

	// 主治医セレクトボックスのオプションを作成
	addOption(document.search.search_doc, '0', 'すべて', doc);
<? foreach ($arr_sectdr as $sectdr) { ?>
	if (sect_id == '<? echo $sectdr["sect_id"]; ?>') {
	<? foreach($sectdr["drs"] as $dritem) { ?>
		addOption(document.search.search_doc, '<? echo $dritem["dr_id"]; ?>', '<? echo $dritem["dr_nm"]; ?>', doc);
	<? } ?>
	}
<? } ?>
}

function openInstSearchWindow(callback) {
	window.open('bed_inst_search.php?session=<? echo($session); ?>&callback=' + callback, 'instsrch', 'width=640,height=480,scrollbars=yes');
}

function clearInst() {
	document.inpt.out_inst_cd.value = '';
	if (document.inpt.out_inst_name) {
		document.inpt.out_inst_name.value = '';
	}
	document.inpt.out_sect_rireki.value = '';
	if (document.inpt.out_sect_cd) {
		clearOptions(document.inpt.out_sect_cd);
	}
	if (document.inpt.out_doctor_no) {
		clearOptions(document.inpt.out_doctor_no);
	}
	if (document.inpt.out_city) {
		document.inpt.out_city.value = '';
	}
}

function selectInst(cd) {
	switch (cd) {
<? foreach ($insts as $tmp_inst_cd => $tmp_insts) { ?>
	case '<? echo($tmp_inst_cd); ?>':
		document.inpt.out_inst_cd.value = '<? echo($tmp_inst_cd); ?>';
		if (document.inpt.out_inst_name) {
			document.inpt.out_inst_name.value = '<? echo($tmp_insts["name"]); ?>';
		}
		document.inpt.out_sect_rireki.value = '<? echo($tmp_insts["rireki"]); ?>';
		if (document.inpt.out_city) {
			document.inpt.out_city.value = '<? echo($tmp_insts["city"]); ?>';
		}
		break;
<? } ?>
	}

	setSectOptions();
}

function setSectOptions(defaultSectCd, defaultDoctorNo) {
	if (!document.inpt.out_sect_cd) {
		return;
	}

	clearOptions(document.inpt.out_sect_cd);

	var instCd = document.inpt.out_inst_cd.value;
	if (!instCd) {
		return;
	}

	switch (instCd) {
<? foreach ($insts as $tmp_inst_cd => $tmp_insts) { ?>
	case '<? echo($tmp_inst_cd); ?>':
<? foreach ($tmp_insts["sects"] as $tmp_sect_cd => $tmp_sects) { ?>
		addOption(document.inpt.out_sect_cd, '<? echo($tmp_sect_cd); ?>', '<? echo($tmp_sects["name"]); ?>', defaultSectCd);
<? } ?>
		break;
<? } ?>
	}

	setDoctorOptions(defaultDoctorNo);
}

function setDoctorOptions(defaultDoctorNo) {
	if (!document.inpt.out_doctor_no) {
		return;
	}

	clearOptions(document.inpt.out_doctor_no);

	var sectCd = document.inpt.out_sect_cd.value;
	if (!sectCd) {
		return;
	}

	var instCd = document.inpt.out_inst_cd.value;
<? foreach ($insts as $tmp_inst_cd => $tmp_insts) { ?>
	<? foreach ($tmp_insts["sects"] as $tmp_sect_cd => $tmp_sects) { ?>
	if (instCd == '<? echo($tmp_inst_cd); ?>' && sectCd == '<? echo($tmp_sect_cd); ?>') {
		<? foreach ($tmp_sects["doctors"] as $tmp_doctor_no => $tmp_doctor_name) { ?>
			addOption(document.inpt.out_doctor_no, '<? echo($tmp_doctor_no); ?>', '<? echo($tmp_doctor_name); ?>', defaultDoctorNo);
		<? } ?>
	}
	<? } ?>
<? } ?>
}

function clearPrInst() {
	document.inpt.pr_inst_cd.value = '';
	if (document.inpt.pr_inst_name) {
		document.inpt.pr_inst_name.value = '';
	}
	document.inpt.pr_sect_rireki.value = '';
	if (document.inpt.pr_sect_cd) {
		clearOptions(document.inpt.pr_sect_cd);
	}
	if (document.inpt.pr_doctor_no) {
		clearOptions(document.inpt.pr_doctor_no);
	}
}

function selectPrInst(cd) {
	switch (cd) {
<? foreach ($insts as $tmp_inst_cd => $tmp_inst) { ?>
	case '<? echo($tmp_inst_cd); ?>':
		document.inpt.pr_inst_cd.value = '<? echo($tmp_inst_cd); ?>';
		if (document.inpt.pr_inst_name) {
			document.inpt.pr_inst_name.value = '<? echo($tmp_inst["name"]); ?>';
		}
		document.inpt.pr_sect_rireki.value = '<? echo($tmp_inst["rireki"]); ?>';
		break;
<? } ?>
	}

	setPrSectOptions();
}

function setPrSectOptions(defaultSectCd, defaultDoctorNo) {
	if (!document.inpt.pr_sect_cd) {
		return;
	}

	clearOptions(document.inpt.pr_sect_cd);

	var instCd = document.inpt.pr_inst_cd.value;
	if (!instCd) {
		return;
	}

	switch (instCd) {
<? foreach ($insts as $tmp_inst_cd => $tmp_inst) { ?>
	case '<? echo($tmp_inst_cd); ?>':
<? foreach ($tmp_inst["sects"] as $tmp_sect_cd => $tmp_sect) { ?>
		addOption(document.inpt.pr_sect_cd, '<? echo($tmp_sect_cd); ?>', '<? echo($tmp_sect["name"]); ?>', defaultSectCd);
<? } ?>
		break;
<? } ?>
	}

	setPrDoctorOptions(defaultDoctorNo);
}

function setPrDoctorOptions(defaultDoctorNo) {
	if (!document.inpt.pr_doctor_no) {
		return;
	}

	clearOptions(document.inpt.pr_doctor_no);

	var sectCd = document.inpt.pr_sect_cd.value;
	if (!sectCd) {
		return;
	}

	var instCd = document.inpt.pr_inst_cd.value;
<? foreach ($insts as $tmp_inst_cd => $tmp_inst) { ?>
	<? foreach ($tmp_inst["sects"] as $tmp_sect_cd => $tmp_sect) { ?>
	if (instCd == '<? echo($tmp_inst_cd); ?>' && sectCd == '<? echo($tmp_sect_cd); ?>') {
		<? foreach ($tmp_sect["doctors"] as $tmp_doctor_no => $tmp_doctor_name) { ?>
			addOption(document.inpt.pr_doctor_no, '<? echo($tmp_doctor_no); ?>', '<? echo($tmp_doctor_name); ?>', defaultDoctorNo);
		<? } ?>
	}
	<? } ?>
<? } ?>
}

function clearOptions(selectbox) {
	for (var i = selectbox.length - 1; i > 0; i--) {
		selectbox.options[i] = null;
	}
}

function deleteAllOptions(box) {
	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}
}

function addOption(box, value, text, selected) {
	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	if (selected == value) {
		opt.selected = true;
	}
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
}

function setBackWardOptions(ward, ptrm, bed) {
	if (!document.inpt.ward) {
		return;
	}

	deleteAllOptions(document.inpt.ward);

<? foreach($wards as $tmp_ward_code => $tmp_ward) { ?>
	addOption(document.inpt.ward, '<? echo($tmp_ward_code); ?>', '<? echo($tmp_ward["name"]); ?>', ward);
<? } ?>

	if (document.inpt.ward.options.length == 0) {
		addOption(document.inpt.ward, '0', '（未登録）', ward);
	}

	wardOnChange(ptrm, bed);
}

function wardOnChange(ptrm, bed) {
	deleteAllOptions(document.inpt.ptrm);

	var bldgwd = document.inpt.ward.value;
<? foreach ($wards as $tmp_ward_code => $tmp_ward) { ?>
	if (bldgwd == '<? echo($tmp_ward_code); ?>') {
	<? foreach($tmp_ward["ptrms"] as $tmp_ptrm_no => $tmp_ptrm) { ?>
		addOption(document.inpt.ptrm, '<? echo($tmp_ptrm_no); ?>', '<? echo($tmp_ptrm["name"]); ?>', ptrm);
	<? } ?>
	}
<? } ?>

	if (document.inpt.ptrm.options.length == 0) {
		addOption(document.inpt.ptrm, '0', '（未登録）', ptrm);
	}

	bedundec1OnClick();
	ptrmOnChange(bed);
}

function ptrmOnChange(bed) {
	deleteAllOptions(document.inpt.bed);

	var bldgwd = document.inpt.ward.value;
	var ptrm_no = document.inpt.ptrm.value;

<? foreach ($wards as $tmp_ward_code => $tmp_ward) { ?>
	<? foreach ($tmp_ward["ptrms"] as $tmp_ptrm_no => $tmp_ptrm) { ?>
	if (bldgwd == '<? echo($tmp_ward_code); ?>' && ptrm_no == '<? echo($tmp_ptrm_no); ?>') {
		for (var i = 1; i <= <? echo($tmp_ptrm["bed_count"]); ?>; i++) {
			addOption(document.inpt.bed, i.toString(), 'ベットNo.' + i, bed);
		}
	}
	<? } ?>
<? } ?>

	if (document.inpt.bed.options.length == 0) {
		addOption(document.inpt.bed, '0', '（未登録）', bed);
	}
}

function openSearchWindow() {
<?
if ($in_yr == "") {$in_yr = date("Y");}
if ($in_mon == "") {$in_mon = date("n");}
if ($in_day == "") {$in_day = date("j");}
if ($in_hr == "") {$in_hr = date("G");}
if ($in_min == "") {$in_min = (date("i") <= "29") ? "00" : "30";}
?>
	var ward = document.inpt.ward.value;
	var in_yr = document.inpt.back_in_yr.value;
	if (in_yr == '-') {in_yr = '<? echo($in_yr); ?>';}
	var in_mon = document.inpt.back_in_mon.value;
	if (in_mon == '-') {in_mon = '<? echo($in_mon); ?>';}
	var in_day = document.inpt.back_in_day.value;
	if (in_day == '-') {in_day = '<? echo($in_day); ?>';}
	var in_hr = document.inpt.back_in_hr.value;
	if (in_hr == '--') {in_hr = '<? echo($in_hr); ?>';}
	var in_min = document.inpt.back_in_min.value;
	if (in_min == '--') {in_min = '<? echo($in_min); ?>';}
	var url = 'sub_vacant_room_list.php?session=<? echo($session); ?>&bldgwd=' + ward + '&in_yr=' + in_yr + '&in_mon=' + in_mon + '&in_day=' + in_day + '&in_hr=' + in_hr + '&in_min=' + in_min;
	window.open(url, 'vacant', 'width=840,height=640,scrollbars=yes');
}

function bedundec1OnClick() {
	if (!document.inpt.bedundec1) {
		return;
	}

	var bedDisabled = (document.inpt.bedundec1.checked);
	document.inpt.ward.disabled = bedDisabled;
	document.inpt.ptrm.disabled = bedDisabled;
	document.inpt.bed.disabled = bedDisabled;

	document.inpt.bedundec2.disabled = bedDisabled;
	document.inpt.bedundec3.disabled = bedDisabled;
	if (bedDisabled) {
		document.inpt.bedundec2.checked = true;
		document.inpt.bedundec3.checked = true;
	}

	if (document.inpt.ptrm.value == '0') {
		bedDisabled = true;
	}
	document.inpt.btnEquipDetail.disabled = bedDisabled;
	document.inpt.btnRoomDetail.disabled = bedDisabled;

	bedundec2OnClick();
}

function bedundec2OnClick() {
	var bedDisabled = (document.inpt.bedundec2.checked);
	document.inpt.ptrm.disabled = bedDisabled;
	document.inpt.bed.disabled = bedDisabled;

	document.inpt.bedundec3.disabled = bedDisabled;
	if (bedDisabled) {
		document.inpt.bedundec3.checked = true;
	}

	if (document.inpt.ptrm.value == '0') {
		bedDisabled = true;
	}
	document.inpt.btnEquipDetail.disabled = bedDisabled;
	document.inpt.btnRoomDetail.disabled = bedDisabled;

	bedundec3OnClick();
}

function bedundec3OnClick() {
	var bedDisabled = (document.inpt.bedundec3.checked);
	document.inpt.bed.disabled = bedDisabled;
}

function setBackSectOptions(sect, doc) {
	if (!document.inpt.back_sect) {
		return;
	}

	clearOptions(document.inpt.back_sect);

<? foreach($sects as $tmp_sect_id => $tmp_sect) { ?>
	addOption(document.inpt.back_sect, '<? echo($tmp_sect_id); ?>', '<? echo($tmp_sect["name"]); ?>', sect);
<? } ?>

	setBackDoctorOptions(doc);
}

function setBackDoctorOptions(doc) {
	clearOptions(document.inpt.back_doc);

	var sect = document.inpt.back_sect.value;
<? foreach($sects as $tmp_sect_id => $tmp_sect) { ?>
	if (sect == '<? echo($tmp_sect_id); ?>') {
	<? foreach($tmp_sect["doctors"] as $tmp_doctor_id => $tmp_doctor_name) { ?>
		addOption(document.inpt.back_doc, '<? echo($tmp_doctor_id); ?>', '<? echo($tmp_doctor_name); ?>', doc);
	<? } ?>
	}
<? } ?>
}

function openEquipWin() {
	var arr_bldgwd = document.inpt.ward.value.split('-');
	var url = 'room_equipment_detail.php';
	url += '?session=<? echo($session); ?>';
	url += '&bldg_cd=' + arr_bldgwd[0];
	url += '&ward_cd=' + arr_bldgwd[1];
	url += '&rm_no=' + document.inpt.ptrm.value;
	window.open(url, 'equipwin', 'width=640,height=480,scrollbars=yes');
}

function openRoomWin() {
	var arr_bldgwd = document.inpt.ward.value.split('-');
	var url = 'bed_info_detail_sub.php';
	url += '?session=<? echo($session); ?>';
	url += '&bldg_cd=' + arr_bldgwd[0];
	url += '&ward_cd=' + arr_bldgwd[1];
	url += '&ptrm_room_no=' + document.inpt.ptrm.value;
	window.open(url, 'rmdtl', 'width=800,height=600,scrollbars=yes');
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="bed_menu.php?session=<? echo($session); ?>"><img src="img/icon/b17.gif" width="32" height="32" border="0" alt="病床管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="bed_menu.php?session=<? echo($session); ?>"><b>病床管理</b></a></font></td>
<? if ($section_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="entity_menu.php?session=<? echo($session); ?>&entity=1"><b>管理画面へ</b></a></font></td>
<? } else if ($ward_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="building_list.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="115" align="center" bgcolor="#5279a5"><a href="bed_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>病床管理トップ</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="ward_reference.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病棟照会</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="inpatient_reserve_register_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入退院登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="bedmake_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ベットメイク</font></a></td>
<td width="5">&nbsp;</td>
<td width="125" align="center" bgcolor="#bdd1e7"><a href="bed_rate_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床利用率管理</font></a>
</td>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#bdd1e7"><a href="bed_date_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">在院日数管理</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="bed_patient_export.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者データ抽出</font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="submenu">
<tr height="22">
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu.php?session=<? echo($session); ?>">お知らせ</a></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_status.php?session=<? echo($session); ?>">病棟状況一覧表</a></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_schedule.php?session=<? echo($session); ?>">入退院カレンダー</a></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院患者検索</font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_vacant_search.php?session=<? echo($session); ?>">空床検索</a></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_occupation.php?session=<? echo($session); ?>">病床利用状況</a></font></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<? write_search_form($pt_nm, $arr_sect, $search_sect, $session); ?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<!-- タブ2 -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="50" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_patient.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=B&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基本</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_sub.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=B&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基礎データ</font></a></td>
<td width="5">&nbsp;</td>
<td width="60" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_contact.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=B&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">連絡先</font></a></td>
<td width="5">&nbsp;</td>
<td width="50" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_claim.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=B&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">請求</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_detail.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=B&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院情報</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_check.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=B&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">チェックリスト</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_nutrition.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=B&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">栄養問診表</font></a></td>
<td width="5">&nbsp;</td>
<td width="170" align="center" bgcolor="#bdd1e7"><a href="bed_menu_inpatient_exception.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=B&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">平均在院日数除外指定</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#5279a5"><a href="bed_menu_inpatient_out_reserve.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&pt_nm=<? echo($url_pt_nm); ?>&search_sect=<? echo($search_sect); ?>&search_doc=<? echo($search_doc); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>退院予定情報</b></font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="20%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者ID</font></td>
<td width="30%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($pt_id); ?></font></td>
<td width="20%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者氏名</font></td>
<td width="30%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo("$lt_kj_nm $ft_kj_nm"); ?></font></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<form name="inpt" action="outpatient_reserve_update_exe.php" method="post">
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="20%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退院予定日時</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="out_yr">
<? show_select_years_span(min($out_yr, date("Y") - 3), date("Y") + 1, $out_yr); ?>
</select>/<select name="out_mon">
<? show_select_months($out_mon); ?>
</select>/<select name="out_day">
<? show_select_days($out_day); ?>
</select>&nbsp;<select name="out_hr">
<? show_select_hrs_0_23($out_hr); ?>
</select>：<select name="out_min">
<option value="00"<? if ($out_min == "00") {echo " selected";} ?>>00</option>
<option value="30"<? if ($out_min == "30") {echo " selected";} ?>>30</option>
</select>
</font></td>
</tr>
<? if ($display1 == "t") { ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">転帰</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="result">
<option value="">
<option value="2"<? if ($result == "2") {echo " selected";} ?>>完治
<option value="3"<? if ($result == "3") {echo " selected";} ?>>不変
<option value="4"<? if ($result == "4") {echo " selected";} ?>>軽快
<option value="5"<? if ($result == "5") {echo " selected";} ?>>後遺症残
<option value="6"<? if ($result == "6") {echo " selected";} ?>>不明
<option value="7"<? if ($result == "7") {echo " selected";} ?>>悪化
<option value="1"<? if ($result == "1") {echo " selected";} ?>>死亡
<option value="8"<? if ($result == "8") {echo " selected";} ?>>その他
</select>
&nbsp;詳細：<input type="text" name="result_dtl" value="<? echo $result_dtl; ?>" size="30" style="ime-mode:active;">
</font></td>
</tr>
<? } ?>
<? if ($display2 == "t") { ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退院理由</font></td>
<td colspan="3">
<select name="out_rsn_cd">
<option value="">
<? foreach ($out_rsn_master as $tmp_out_rsn_cd => $tmp_out_rsn_name) { ?>
<option value="<? echo($tmp_out_rsn_cd); ?>"<? if ($tmp_out_rsn_cd == $out_rsn_cd) {echo " selected";} ?>><? echo($tmp_out_rsn_name); ?>
<? } ?>
</select>
</td>
</tr>
<? } ?>
<? if ($display3 == "t") { ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退院先区分</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="out_pos_cd">
<option value="">
<? foreach ($out_pos_master as $tmp_out_pos_cd => $tmp_out_pos_name) { ?>
<option value="<? echo($tmp_out_pos_cd); ?>"<? if ($tmp_out_pos_cd == $out_pos_cd) {echo " selected";} ?>><? echo($tmp_out_pos_name); ?>
<? } ?>
</select>
&nbsp;詳細：<input type="text" name="out_pos_dtl" value="<? echo $out_pos_dtl; ?>" size="40" style="ime-mode:active;">
</font></td>
</tr>
<? } ?>
<? if ($display4 == "t" || $display5 == "t") { ?>
<tr height="22">
<? if ($display4 == "t") { ?>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退院先施設名</font></td>
<td<? if ($display5 != "t") {echo(' colspan="3"');} ?>><input type="text" name="out_inst_name" value="<? echo($out_inst_name); ?>" size="25" disabled> <input type="button" value="検索" onclick="openInstSearchWindow('selectInst');"> <input type="button" value="クリア" onclick="clearInst();"></td>
<? } ?>
<? if ($display5 == "t") { ?>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">診療科</font></td>
<td<? if ($display4 != "t") {echo(' colspan="3"');} ?>><select name="out_sect_cd" onchange="setDoctorOptions();">
<option value="">　　　　　</option>
</select></td>
<? } ?>
</tr>
<? } ?>
<? if ($display6 == "t") { ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">担当医</font></td>
<td colspan="3"><select name="out_doctor_no">
<option value="">　　　　　</option>
</select></td>
</tr>
<? } ?>
<? if ($display7 == "t") { ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退院先市区町村</font></td>
<td colspan="3"><input type="text" name="out_city" value="<? echo($out_city); ?>" size="60" maxlength="100" style="ime-mode:active;"></td>
</tr>
<? } ?>
<? if ($display9 == "t") { ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">かかりつけ医</font></td>
<td colspan="3">

<table width="100%" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">医療機関名</font></td>
<td><input type="text" name="pr_inst_name" value="<? echo($pr_inst_name); ?>" size="25" disabled> <input type="button" value="検索" onclick="openInstSearchWindow('selectPrInst');"> <input type="button" value="クリア" onclick="clearPrInst();"></td>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">診療科</font></td>
<td><select name="pr_sect_cd" onchange="setPrDoctorOptions();">
<option value="">　　　　　</option>
</select></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">担当医</font></td>
<td colspan="3"><select name="pr_doctor_no">
<option value="">　　　　　</option>
</select></td>
</tr>
</table>

</td>
</tr>
<? } ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">食事終了帯</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="fd_end" value="1"<? if ($fd_end == "1") {echo " checked";} ?>>朝
<input type="radio" name="fd_end" value="2"<? if ($fd_end == "2") {echo " checked";} ?>>昼
<input type="radio" name="fd_end" value="3"<? if ($fd_end == "3") {echo " checked";} ?>>夕
</font></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">コメント</font></td>
<td colspan="3"><textarea name="comment" cols="50" rows="4" wrap="virtual" style="ime-mode:active;"><? echo($comment); ?></textarea></td>
</tr>
</table>
<? if ($display8 == "t") { ?>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">＜戻り入院の予定＞</font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病棟</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="ward" onchange="wardOnChange();">
</select>
<input value="空床検索" type="button" onclick="openSearchWindow();" style="margin-right:2px;">
病棟未定：<input type="checkbox" name="bedundec1" value="t" <? if ($bedundec1 == "t") {echo("checked");} ?> onclick="bedundec1OnClick();">
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病室</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="ptrm" onchange="ptrmOnChange();">
</select>
<input type="button" name="btnEquipDetail" value="設備詳細" onclick="openEquipWin();">
<input type="button" name="btnRoomDetail" value="病室詳細" onclick="openRoomWin();" style="margin-right:2px;">
病室未定：<input type="checkbox" name="bedundec2" value="t" <? if ($bedundec2 == "t") {echo("checked");} ?> onclick="bedundec2OnClick();">
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ベット番号</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="bed">
</select>
ベット未定：<input type="checkbox" name="bedundec3" value="t" <? if ($bedundec3 == "t") {echo("checked");} ?> onclick="bedundec3OnClick();">
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院日時</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="back_in_yr">
<option value="-">
<? show_select_years_span(date("Y"), date("Y") + 1, $back_in_yr); ?>
</select>/<select name="back_in_mon">
<? show_select_months($back_in_mon, true); ?>
</select>/<select name="back_in_day">
<? show_select_days($back_in_day, true); ?>
</select>&nbsp;<select name="back_in_hr">
<? show_hour_options_0_23($back_in_hr, true); ?>
</select>：<select name="back_in_min">
<option value="--">
<option value="00"<? if ($back_in_min == "00") {echo " selected";} ?>>00
<option value="30"<? if ($back_in_min == "30") {echo " selected";} ?>>30
</select>
</font></td>
</tr>
<tr height="22">
<td width="20%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">診療科</font></td>
<td width="34%">
<select name="back_sect" onChange="setBackDoctorOptions();">
<option value="">　　　　　</option>
</select>
</td>
<td width="14%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">主治医</font></td>
<td width="32%">
<select name="back_doc">
<option value="">　　　　　</option>
</select>
</td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">転院目的</font></td>
<td colspan="3"><textarea name="back_change_purpose" cols="50" rows="4" wrap="virtual" style="ime-mode:active;"><? echo($back_change_purpose); ?></textarea></td>
</tr>
</table>
<? } ?>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right">
<input type="button" value="変更" onclick="this.form.is_delete.value = 'f'; this.form.submit();">
<input type="button" value="取消" onclick="if (confirm('取消してもよろしいですか？')) {this.form.is_delete.value = 't'; this.form.submit();}">
</td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="pt_id" value="<? echo($pt_id); ?>">
<input type="hidden" name="pt_nm" value="<? echo($pt_nm); ?>">
<input type="hidden" name="search_sect" value="<? echo($search_sect); ?>">
<input type="hidden" name="search_doc" value="<? echo($search_doc); ?>">
<input type="hidden" name="out_pos_rireki" value="<? echo($out_pos_rireki); ?>">
<input type="hidden" name="out_inst_cd" value="<? echo($out_inst_cd); ?>">
<input type="hidden" name="out_sect_rireki" value="<? echo($out_sect_rireki); ?>">
<input type="hidden" name="pr_inst_cd" value="<? echo($pr_inst_cd); ?>">
<input type="hidden" name="pr_sect_rireki" value="<? echo($pr_sect_rireki); ?>">
<input type="hidden" name="is_delete" value="">
<input type="hidden" name="back_url" value="bed_menu_inpatient_out_reserve.php">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
