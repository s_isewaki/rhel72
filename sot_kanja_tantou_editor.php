<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 入院患者担当者登録</title>
<link rel="stylesheet" type="text/css" href="css/main_summary.css" />
<?
$fname = $_SERVER["PHP_SELF"];
require_once("about_comedix.php");
require_once("summary_common.ini");
require_once("./get_values.ini");
require_once("sot_util.php");


$session = qualify_session($session,$fname);
$summary_check = check_authority($session, 59, $fname);
$con = @connect2db($fname);
if(!$session || !$con){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo $c_sot_util->javascript("showLoginPage(window);");
  exit;
}
$pt_id = trim(@$_REQUEST["pt_id"]);
$target_ymd = trim(@$_REQUEST["target_ymd"]);
if (strlen($target_ymd) != 8) $target_ymd = date("Ymd");



//******************************************************************************
// 登録する場合
//******************************************************************************
if ($_REQUEST["regist_tantou"]){
	$sql =
		" update inptmst set".
		" nurse_id = " . (int)@$_REQUEST["nurse_id"].
		" where ptif_id = '".pg_escape_string($_REQUEST["pt_id"])."'".
		" and bldg_cd = ".(int)@$_REQUEST["bldg_cd"].
		" and ward_cd = ".(int)@$_REQUEST["ward_cd"].
		" and ptrm_room_no = ".(int)@$_REQUEST["ptrm_room_no"].
		" and inpt_in_dt = '".pg_escape_string($_REQUEST["inpt_in_dt"])."'";
	$upd = update_set_table($con, $sql, array(), null, "", $fname);

	$sql = "delete from inptop where ptif_id = '".pg_escape_string($_REQUEST["pt_id"])."'";
	$upd = update_set_table($con, $sql, array(), null, "", $fname);

	$reg_tantou_list = $_REQUEST["tantou_list"];
	if (!is_array($reg_tantou_list)) $reg_tantou_list = array();
	for ($i = 1; $i <= count($reg_tantou_list); $i++){
		$sql =
			" insert into inptop (ptif_id, emp_id, order_no) values (".
			" '".pg_escape_string($_REQUEST["pt_id"])."'".
			",'".pg_escape_string($reg_tantou_list[$i-1])."'".
			"," . $i.
			")";
		$upd = update_set_table($con, $sql, array(), null, "", $fname);
	}
?>
	<script type="text/javascript">
		<? if (@$_REQUEST["js_callback"]){ ?>
		if (window.opener && window.opener.<?= @$_REQUEST["js_callback"] ?>){
			window.opener.<?= @$_REQUEST["js_callback"] ?>();
		}
		<? } ?>
		window.close();
	</script>
<?
	die;
}


//============================
// 指定日の入院データレコード
//============================
$nyuin_info = $c_sot_util->get_nyuin_info($con, $fname, $pt_id, $target_ymd);


//============================
// 患者の主治医（参照のみ用）
//============================
$sql =
	" select dr_emp_id, dr_nm from drmst".
	" where enti_id = ". (int)@$nyuin_info["inpt_enti_id"].
	" and sect_id = ". (int)@$nyuin_info["inpt_sect_id"].
	" and dr_id = ". (int)@$nyuin_info["dr_id"];
$sel = select_from_table($con, $sql, "", $fname);
$dr_info = pg_fetch_array($sel);


//============================
// 患者の担当者リスト
//============================
$sql =
	" select iop.emp_id, emp.emp_lt_nm, emp.emp_ft_nm, emp.emp_job, job.job_nm from inptop iop".
	" left outer join empmst emp on (emp.emp_id = iop.emp_id)".
	" left outer join jobmst job on (job.job_id = emp.emp_job)".
	" where ptif_id = '".pg_escape_string($pt_id)."' order by iop.order_no";
$sel = select_from_table($con, $sql, "", $fname);
$tantou_list = pg_fetch_all($sel);
if (!is_array($tantou_list)) $tantou_list = array();


//============================
// 入院診療科の診療科名
//============================
$sql = "select sect_nm from sectmst where enti_id = ". (int)@$nyuin_info["inpt_enti_id"]." and sect_id = ". (int)@$nyuin_info["inpt_sect_id"];
$sel = select_from_table($con, $sql, "", $fname);
$sect_nm = pg_fetch_result($sel, 0, 0);

//============================
// 職種名全リスト
//============================
$sel = select_from_table($con, "select job_id, job_nm from jobmst where job_del_flg = 'f' order by order_no", "", $fname);
$job_list = pg_fetch_all($sel);
if (!is_array($job_list)) $job_list = array();


//============================
// 入院診療科に属する、全看護師リスト
//============================
$sql =
	" select nu.nurse_id, nu.nurse_emp_id, nu.nurse_nm from numst nu".
	" inner join sectmst sect on (sect.enti_id = nu.enti_id and sect.sect_id = nu.sect_id and sect_del_flg = 'f')".
	" where nurse_del_flg = 'f'".
	" and nu.enti_id = ". (int)@$nyuin_info["inpt_enti_id"].
	" and nu.sect_id = ". (int)@$nyuin_info["inpt_sect_id"].
	" order by nurse_id";
$sel = select_from_table($con, $sql, "", $fname);
$nurse_list = pg_fetch_all($sel);
if (!is_array($nurse_list)) $nurse_list = array();


?>
<script type="text/javascript">
	var nurse_list = [];
	var nurse_info = {};
	<? foreach($nurse_list as $idx => $row){ ?>
	nurse_list.push({"nurse_id":"<?=$row["nurse_id"]?>","nurse_nm":"<?=str_replace('"','\"',$row["nurse_nm"])?>"});
		<? if ($nyuin_info["nurse_id"] == $row["nurse_id"]) { ?>
		var len = nurse_list.length;
		nurse_info = nurse_list[len-1];
		<? } ?>
	<? } ?>

	var job_list = {};
	<? foreach($job_list as $idx => $row){ ?>
	job_list["<?=$row["job_id"]?>"] = "<?=str_replace('"','\"',$row["job_nm"])?>";
	<? } ?>

	//==========================
	// 名簿画面のポップアップ
	//==========================
	function sectionEmpSelectPopup(){
		window.open("section_emp_select.php?session=<?=$session?>&wherefrom=4", "section_emp_select_window", "scrollbars=yes,width=640,height=480");
	}
	//==========================
	// 名簿画面からのコールバック
	//==========================
	function callbackSectionEmpSelect(emp_lt_nm, emp_ft_nm, emp_personal_id, emp_id, job_id){
		var dropdown = document.getElementById("tantou_list");
		var len = dropdown.options.length;
		var txt = "["+job_list[job_id]+"]  "+emp_lt_nm+" "+emp_ft_nm;
		for (var i = 0; i < len; i++){
			var opt = dropdown.options[i];
			if (opt.value == emp_id && opt.text == txt) { dropdown.options.selectedIndex = i; return; }
			if (opt.value == emp_id) { opt.text = txt; dropdown.options.selectedIndex = i; return; }
		}
		dropdown.options.length = len + 1;
		dropdown.options[len].value = emp_id;
		dropdown.options[len].text = txt;
		dropdown.options.selectedIndex = len;
	}
	//==========================
	// 担当者をリストから除去
	//==========================
	function deleteEmpEntry(){
		var dropdown = document.getElementById("tantou_list");
		var len = dropdown.options.length;
		var idx = dropdown.options.selectedIndex;
		for (var i = idx; i < len-1; i++){
			dropdown.options[i].value = dropdown.options[i+1].value;
			dropdown.options[i].text  = dropdown.options[i+1].text;
		}
		dropdown.options.length = len-1;
		if (idx==len-1) dropdown.options.selectedIndex = idx-1;
	}
	//==========================
	// サブミット
	//==========================
	function formSubmit(){
		var dropdown = document.getElementById("tantou_list");
		var len = dropdown.options.length;
		dropdown.multiple = true;
		for (var i = 0; i < len; i++){
			dropdown.options[i].selected = "selected";
		}
		document.frm.submit();
	}
</script>


</head>
<body>
<center>
<div style="width:590px; margin-top:4px">


<!-- タイトルヘッダ -->
<?= summary_common_show_dialog_header("入院患者担当者登録"); ?>


<!-- ヘッダ情報部 -->
<table class="list_table" cellspacing="1" style="margin-top:4px">
	<tr>
		<th width="10%" style="text-align:center">患者名</th>
		<td width="24%" style="text-align:center"><?= h(@$nyuin_info["inpt_lt_kj_nm"]." ".@$nyuin_info["inpt_ft_kj_nm"]) ?></td>
		<th width="10%" style="text-align:center">診療科</th>
		<td width="22%" style="text-align:center"><?= h($sect_nm) ?></td>
		<th width="10%" style="text-align:center">担当医</th>
		<td width="24%" style="text-align:center"><?= @$dr_info["dr_nm"]?></td>
	</tr>
</table>


<!-- 登録内容 -->
<form name="frm" action="sot_kanja_tantou_editor.php" method="post">
<input type="hidden" name="session" value="<?=$session?>" />
<input type="hidden" name="regist_tantou" value="1" />
<input type="hidden" name="js_callback" value="<?=$_REQUEST["js_callback"]?>" />
<input type="hidden" name="pt_id" value="<?=$pt_id?>" />
<input type="hidden" name="taerget_ymd" value="<?=$target_ymd?>" />
<input type="hidden" name="bldg_cd" value="<?=@$nyuin_info["bldg_cd"]?>" />
<input type="hidden" name="ward_cd" value="<?=@$nyuin_info["ward_cd"]?>" />
<input type="hidden" name="ptrm_room_no" value="<?=@$nyuin_info["ptrm_room_no"]?>" />
<input type="hidden" name="inpt_in_dt" value="<?=@$nyuin_info["inpt_in_dt"]?>" />

<table class="prop_table" cellspacing="1" style="margin-top:16px">
	<tr>
		<th width="20%" style="text-align:center">看護師</th>
		<td width="80%">
			<select name="nurse_id" id="nurse_id">
				<option value="">　　　　</option>
				<? foreach($nurse_list as $idx => $row){ ?>
				<? $selected = ($row["nurse_id"] == $nyuin_info["nurse_id"] ? 'selected="selected"' : ""); ?>
				<option value="<?=$row["nurse_id"]?>" <?=$selected?>><?=$row["nurse_nm"]?></option>
				<? } ?>
			</select>
		</td>
	</tr>
	<tr>
		<th width="20%" style="text-align:center">担当者</th>
		<td width="80%" style="padding:8px; vertical-align:top">
			<input type="button" value="職員名簿" style="float:left; display:block;" onclick="sectionEmpSelectPopup()" />
			<input type="button" value="選択を削除" style="float:right; display:block;" onclick="deleteEmpEntry()" />
			<br style="clear:both" />
			<select id="tantou_list" name="tantou_list[]" size="14" style="width:100%; height:200px; margin-top:4px">
				<? foreach($tantou_list as $idx => $row){ ?>
				<option value="<?=$row["emp_id"]?>"><?=h("[".$row["job_nm"]."]  ".$row["emp_lt_nm"]." ".$row["emp_ft_nm"])?></option>
				<? } ?>
			</select>
		</td>
	</tr>
</table>
<div style="margin-top:10px; text-align:right"><input type="button" value="登録" onclick="formSubmit()" /></div>
</form>




</div>
</center>
</body>
</html>
