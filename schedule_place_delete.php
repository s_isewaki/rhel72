<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require_once("Cmx.php");
require("./about_postgres.php");
require("./conf/sql.inf");
require_once("aclg_set.php");

$fname=$PHP_SELF;
$con=connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

if($con==0){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

if($trashbox==""){
	echo("<script language='javascript'>alert(\"チェックボックスをオンにしてください。\");</script>");
	echo("<script language='javascript'>history.back();</script>");
	exit;
}

$in="where place_id in (";
$count=count($trashbox);
for($i=0;$i<$count;$i++){
	if(!$trashbox[$i]==""){
		$in.="'$trashbox[$i]'";
		if($i<($count-1)){
			$in.=",";
		}
	}
}
$in.=")";
$delete_scheduleplace="update scheduleplace set place_flag='t' $in";
$result_delete=pg_exec($con,$delete_scheduleplace);
pg_close($con);
if($result_delete==true){
	echo("<script language='javascript'>location.href=\"./schedule_place_list.php?session=".$session."&page=".$page."\";</script>");
	exit;
}else{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
?>