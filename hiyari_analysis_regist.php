<?php
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");
require_once("hiyari_common.ini");
require_once("hiyari_reporting_common.ini");
require_once("hiyari_mail.ini");
require_once("hiyari_db_class.php");
require_once('Cmx.php');
require_once('Cmx/View/Smarty.php');

//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// 権限チェック
$summary = check_authority($session, 47, $fname);
if ($summary == "0") {
    showLoginPage();
    exit;
}

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// DBクラスのインスタンス化
$db = new hiyari_db_class($con, $fname);

//==================================================
//デザイン（1=旧デザイン、2=新デザイン）
//==================================================
$sysConf = new Cmx_SystemConfig();
$design_mode = $sysConf->get('fantol.design.mode');

//==============================
//ユーザーID取得
//==============================
$emp_id  = get_emp_id($con,$session,$fname);
$renewer = get_emp_kanji_name($con,$emp_id,$fname);


// レポートIDがあれば、事案番号を取得する
if($_GET['report_id']) {
    $sql = "SELECT report_no FROM inci_report WHERE report_id = ". $_GET['report_id'];
    $db->query($sql);
    $get_report_no = $db->getOne();
}


//==============================
//更新日付取得
//==============================
if ( $mode == "" ) {
    $renewal_date = date('Y').'年'.date('m').'月'.date('d').'日';
}


if($_POST['is_postback'] == 'true') {
    // トランザクションの開始
    pg_query($con, "begin");

    // 分析登録
    if($_POST['mode'] == 'regist') {
        // 事案番号
        if($analysis_no == "") {
            //レポート番号の先頭4桁を決定
            $date1 = mktime(0,0,0,date('m'), date('d'), date('Y'));
            $yyyy = date('Y', $date1);
            $mm = date('m', $date1);
            $no_header = 'A'.$yyyy.$mm."-";

            //レポート番号下4桁を決定
            $sql = "SELECT max(analysis_no) as max FROM inci_analysis_regist";
            $cond = "where analysis_no like '$no_header%'";
            $result = select_from_table($con, $sql, $cond, $fname);
            if ($result == 0) {
                pg_query($con,"rollback");
                pg_close($con);
                showErrorPage();
                exit;
            }
            $sec = pg_fetch_result($result,0,0);
            if($sec == "")
            {
                $sec = "0001";
            }
            else
            {
                $sec = substr($sec,8,4);//下4桁
                $sec = $sec + 1;//採番
                $sec = sprintf("%04d", $sec);//0埋め4桁
            }

            //レポート番号を作成
            $analysis_no = $no_header.$sec;
        }

        // 最新のIDを取得する
        $sql = "SELECT max(analysis_id) FROM inci_analysis_regist";
        $result = select_from_table($con, $sql, '', $fname);
        if ($result == 0) {
            pg_query($con,"rollback");
            pg_close($con);
            showErrorPage();
            exit;
        }
        $analysis_id = pg_fetch_result($result,0,0) + 1;

        // 日付は空文字だとエラーがでるので、から文字の場合NULLに変換する必要あり
        if($progress_date == '') {
            unset($progress_date);
        }

        // INSERT inci_analysis_regist
        $sql = "INSERT INTO inci_analysis_regist(analysis_id, analysis_no, renewer, renewer_emp_id, renewal_date, analysis_member_id, analysis_member, analysis_title, analysis_method, progress_date, analysis_progress, analysis_problem_id, analysis_problem, analysis_resemble_id, analysis_resemble, registed_date, available, registed_user_id) VALUES ("; //)
        $content = array($analysis_id, $analysis_no, $renewer, $emp_id, date("Y-m-d"), $disp_area_ids, $disp_area_names, pg_escape_string($analysis_title), trim($analysis_method), $progress_date, $analysis_progress, $disp_area_ids_1, $disp_area_names_1, $disp_area_ids_2, $disp_area_names_2, date("Y-m-d"), 't', $emp_id);
        $result = insert_into_table($con,$sql,$content,$fname);
        if ($result == 0) {
            pg_query($con,"rollback");
            pg_close($con);
            showErrorPage();
            exit;
        }

        //===== 事例の詳細(時系列)の転記
        // SELECT inci_easyinput_data
        $sql = "SELECT data.* FROM inci_easyinput_data data JOIN inci_report repo USING (eid_id) WHERE repo.report_id={$disp_area_ids_1} AND grp_code>=4000 AND grp_code<=4049 ORDER BY grp_code,easy_item_code";
        $result = select_from_table($con, $sql, '', $fname);
        if ($result == 0) {
            pg_query($con,"rollback");
            pg_close($con);
            showErrorPage();
            exit;
        }
        $inci4000 = array();
        while ($row = pg_fetch_assoc($result)) {
            $order = $row["grp_code"] - 4000;

            switch ($row["easy_item_code"]) {
                case "1":
                    $inci4000[$order]['year']  = substr($row["input_item"], 0, 4);
                    $inci4000[$order]['month'] = substr($row["input_item"], 5, 2);
                    $inci4000[$order]['day']   = substr($row["input_item"], 8, 2);
                    break;

                case "2":
                    $inci4000[$order]['hour'] = $row["input_item"];
                    break;

                case "3":
                    $inci4000[$order]['minute'] = $row["input_item"];
                    break;

                case "5":
                    $inci4000[$order]['phenomenon'] = $row["input_item"];
                    break;

                default:
                    break;
            }
        }

        // INSERT inci_analysis_rca_date
        foreach ($inci4000 as $order => $analysis) {
            $sql = "INSERT INTO inci_analysis_rca_date(analysis_id, analysis_order, analysis_year, analysis_month, analysis_day, analysis_hour, analysis_minute, analysis_phenomenon) VALUES ( "; //)
            $content = array($analysis_id, $order, $analysis['year'], $analysis['month'], $analysis['day'], $analysis['hour'], $analysis['minute'], $analysis['phenomenon']);
            $result = insert_into_table($con,$sql,$content,$fname);
            if ($result == 0) {
                pg_query($con,"rollback");
                pg_close($con);
                showErrorPage();
                exit;
            }
        }
    }
    else if($mode == 'update') {
        // 日付は空文字だとエラーがでるので、から文字の場合NULLに変換する必要あり
        if($progress_date == '') {
            unset($progress_date);
        }

        $sql = "UPDATE inci_analysis_regist SET ";
        $set = array('renewer', 'renewer_emp_id', 'renewal_date', 'analysis_member_id', 'analysis_member', 'analysis_title', 'analysis_method', 'progress_date', 'analysis_progress', 'analysis_problem_id', 'analysis_problem', 'analysis_resemble_id', 'analysis_resemble', 'updated_date');
        $dat = array($renewer, $emp_id, date("Y-m-d"), $disp_area_ids, $disp_area_names, pg_escape_string($analysis_title), trim($analysis_method), $progress_date, $analysis_progress, $disp_area_ids_1, $disp_area_names_1, $disp_area_ids_2, $disp_area_names_2, date("Y-m-d"));
        $cond = "WHERE analysis_id = ".$analysis_id;
        $db->update($sql, $set, $dat, $cond);
    }

    // トランザクションのコミット
    pg_query($con, "commit");

    // INSERTかUPDATEをした時点で親ページを更新し、子画面を閉じる
    if($_GET['report_id']) {
        echo "<script>window.close();</script>";
    } else {
        // 呼出し元が統計分析レポート表示画面だったらcloseのみ
        if ( $caller == 'hiyari_rm_stats_report.php' ) {
            echo "<script>window.close();</script>";
        }
        else {
            $tmp_from_pastreport = $_POST['from_pastreport'];
            if(isset($tmp_from_pastreport)){
                echo "<script>window.opener.opener.reload_page();</script>";
                echo "<script>window.close();</script>";
            }else{
                echo "<script>window.opener.reload_page();window.close();</script>";
            }
        }
    }
}

if($mode == 'stats') {
    // 統計分析から
    $get_report_no = get_report_no_by_report_id($con, $fname, $report_id);

    $tmp_arr_report_id = explode(",", $report_id_list);
    foreach($tmp_arr_report_id as $value) {
        if($report_id == $value) continue;
        $arr_report_id[] = $value;
        $arr_report_no[] = get_report_no_by_report_id($con, $fname, $value);
    }
    $analysis_resemble_id = implode(",", $arr_report_id);
    $analysis_resemble    = implode(",", $arr_report_no);
}

if(!empty($a_id)) {
    $sql  = "SELECT * FROM inci_analysis_regist WHERE available = 't' AND analysis_id = " . $db->escape($a_id);
    $db->query($sql);
    $data = $db->getRow();

    $analysis_id          = $data['analysis_id'];
    $analysis_no          = $data['analysis_no'];
    $renewer              = $data['renewer'];
    $renewer_emp_id       = $data['renewer_emp_id'];
    $renewal_date         = $data['renewal_date'];
    $analysis_member_id   = $data['analysis_member_id'];
    $analysis_member      = $data['analysis_member'];
    $analysis_title       = $data['analysis_title'];
    $analysis_method      = $data['analysis_method'];
    $progress_date        = $data['progress_date'];
    $analysis_progress    = $data['analysis_progress'];
    $analysis_problem_id  = $data['analysis_problem_id'];
    $analysis_problem     = $data['analysis_problem'];
    $analysis_resemble_id = $data['analysis_resemble_id'];
    $analysis_resemble    = $data['analysis_resemble'];
    $registed_date        = $data['registed_date'];
    $updated_date         = $data['updated_date'];
    $available            = $data['available'];
}


// SMか調べる(BOOLEAN型)
$sm_flg = is_sm_emp($session,$fname,$con);
// メンバーのemp_idを取得
$arr_member_emp_id = explode(",", $analysis_member_id);
// 閲覧している人がメンバーに含まれているか確認
$update_flg = false;
foreach($arr_member_emp_id as $member_emp_id) {
    if($member_emp_id == $emp_id) $update_flg = true;
}

//==============================
//表示
//==============================
$smarty = new Cmx_View();

$smarty->assign("INCIDENT_TITLE", $INCIDENT_TITLE);
$smarty->assign("session", $session);
$smarty->assign("mode", $mode);
$smarty->assign("caller", $caller);
$smarty->assign("emp_id", $emp_id);

//分析番号
$smarty->assign("analysis_id", $analysis_id);
$smarty->assign("analysis_no", $analysis_no);

//更新者
$smarty->assign("renewer", $renewer);

//更新日
$smarty->assign("renewal_date", $renewal_date);

//タイトル
$smarty->assign("analysis_title", h($analysis_title));

//手法
$smarty->assign("analysis_method", trim($analysis_method));

//分析メンバー
if($analysis_member_id) {
    $smarty->assign("analysis_member_id", $analysis_member_id);
    $smarty->assign("analysis_member", $analysis_member);
}
else{
    if (!isset($_POST['is_postback'])){
        $smarty->assign("analysis_member_id", $emp_id);
        $smarty->assign("analysis_member", $renewer);
    }
}

//評価予定期日
$smarty->assign("progress_date", $progress_date);

//進捗
$smarty->assign("analysis_progress", $analysis_progress);

//分析事案
if($analysis_problem_id) {
    $smarty->assign("analysis_problem_id", $analysis_problem_id);
    $smarty->assign("analysis_problem", $analysis_problem);
}
elseif($get_report_no){
    $smarty->assign("analysis_problem_id", $report_id);
    $smarty->assign("analysis_problem", $get_report_no);
}

//類似事案
if($analysis_resemble_id) {
    $smarty->assign("analysis_resemble_id", $analysis_resemble_id);
    $smarty->assign("analysis_resemble", $analysis_resemble);
}

//登録可否（$a_idは、分析画面で分析番号をクリックした場合に設定されている   ）
$smarty->assign("can_regist", (empty($a_id) || $sm_flg == true || $update_flg == true));

//登録後に分析画面に移動するかどうかのフラグ
if(empty($_GET['from_pastreport'])){
    $smarty->assign('moveto_analysis', isset($_GET['report_id']));
}else{
     $smarty->assign('from_pastreport', 'from_pastreport');
}

if ($design_mode == 1){
    $smarty->display("hiyari_analysis_regist1.tpl");
}
else{
    $smarty->display("hiyari_analysis_regist2.tpl");
}