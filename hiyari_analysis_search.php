<?php
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");
require_once("hiyari_common.ini");
require_once("hiyari_reporting_common.ini");
require_once("hiyari_mail.ini");
require_once("hiyari_report_class.php");

//==============================
//初期処理
//==============================
//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if ($session == "0") {
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// 権限チェック
$summary = check_authority($session, 47, $fname);
if ($summary == "0") {
    showLoginPage();
    exit;
}

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if ($con == "0") {
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//==============================
//パラメータ精査
//==============================

//日付検索(年)初期値は今年
if ( $search_date_year == "" ) {
    $search_date_year = date("Y");
}

//日付検索(月)初期値は今月
if ( $search_date_month == "" ) {
    $search_date_month = date("m");
}

//日付最近検索の初期値は２週間
if ( $search_date_mode == "" && $search_date_late_mode == "" ) {
    $search_date_late_mode = "";
}

//==============================
//ユーザーID取得
//==============================
$emp_id = get_emp_id($con,$session,$fname);

//==============================
//処理モード共通情報
//==============================
//ログインユーザーの報告書の所属制限
$report_db_read_div = get_report_db_read_div($session,$fname,$con);


$rep_obj = new hiyari_report_class($con, $fname);

//割付利用権限
$priority_auth = get_emp_priority_auth($session,$fname,$con);
$is_emp_auth_system_all = is_emp_auth_system_all($emp_id, $priority_auth, $con, $fname);
$is_report_classification_usable = $report_db_read_div == "0";
if ( $report_db_read_div >= 1 && $report_db_read_div <= 4 || $is_emp_auth_system_all ) {
    $is_report_classification_usable = true;
}

if ( $search_emp_class ) {
    $search_emp_class = explode(",", $search_emp_class);
}

if ( $search_emp_attribute ) {
    $search_emp_attribute = explode(",", $search_emp_attribute);
}

if ( $search_emp_dept ) {
    $search_emp_dept = explode(",", $search_emp_dept);
}

if ( $search_emp_room ) {
    $search_emp_room = explode(",", $search_emp_room);
}

//起動パラメータに指定されていない場合
if ( $search_emp_class == "" ) {
    //$search_emp_all = true;
    
    //担当範囲でシステム全体に対する担当者ではない場合
    $report_db_read_auth_flg = get_report_db_read_auth_flg($session,$fname,$con);
    $priority_auth = get_emp_priority_auth($session,$fname,$con);
    $is_emp_auth_system_all = is_emp_auth_system_all($emp_id, $priority_auth, $con, $fname);
    if ( !$report_db_read_auth_flg || !$is_emp_auth_system_all ) {
        //検索初期時の部署の条件をプルダウンの先頭と同じとする 20090317
        //権限指定なし／一般ユーザー：ユーザー所属の部署一覧
        if ( $priority_auth == "" || $priority_auth == "GU" ) {
            $emp_class_post = get_emp_class_post($emp_id, $con, $fname);
        }
        //権限指定あり：担当者となる部署一覧
        else {
            //指定ユーザーが、指定権限で担当者となる部署の一覧を取得し、先頭の部署を使用
            //担当範囲
            if($report_db_read_auth_flg) {
                $emp_class_post = get_emp_auth_class_post($emp_id, $priority_auth, $con, $fname);
            }
            //所属範囲
            else {
                $emp_class_post = get_emp_class_post($emp_id, $con, $fname);
            }
        }
        foreach ($emp_class_post as $key => $value) {
            $emp_class[$key] = $value["emp_class"];
            $emp_attribute[$key] = $value["emp_attribute"];
            $emp_dept[$key] = $value["emp_dept"];
            $emp_room[$key] = $value["emp_room"];
        }
        //==============================
        //報告ファイルの所属参照制限がある場合の所属検索条件
        //==============================

        //所属検索初期条件を設定
        if ( $report_db_read_div > 0 ) {
            $search_emp_class = $emp_class;
        }
        if ( $report_db_read_div > 1 ) {
            $search_emp_attribute = $emp_attribute;
        }
        if ( $report_db_read_div > 2 ) {
            $search_emp_dept = $emp_dept;
        }
        if ( $report_db_read_div > 3 ) {
            $search_emp_room = $emp_room;
        }
    }
}
if ( $_POST['search_emp_all'] == 'true' ) {
    //担当範囲でシステム全体に対する担当者ではない場合
    $report_db_read_auth_flg = get_report_db_read_auth_flg($session,$fname,$con);
    $priority_auth = get_emp_priority_auth($session,$fname,$con);
    $is_emp_auth_system_all = is_emp_auth_system_all($emp_id, $priority_auth, $con, $fname);
    if ( !$report_db_read_auth_flg || !$is_emp_auth_system_all ) {
        //検索初期桙ﾌ部署の条件をプルダウンの先頭と同じとする 20090317
        //権限w定なし／一般ユーザー：ユーザー所属の部署一覧
        if ( $priority_auth == "" || $priority_auth == "GU" ) {
            $emp_class_post = get_emp_class_post($emp_id, $con, $fname);
        }
        //権限w定あり：担当者となる部署一覧
        else {
            //w定ユーザーが、w定権限で担当者となる部署の一覧を謫ｾし、先頭の部署をg用
            //担当範囲
            if ( $report_db_read_auth_flg ) {
                $emp_class_post = get_emp_auth_class_post($emp_id, $priority_auth, $con, $fname);
            }
            //所属範囲
            else {
                $emp_class_post = get_emp_class_post($emp_id, $con, $fname);
            }
        }

        foreach ( $emp_class_post as $key => $value ) {
            $emp_class[$key] = $value["emp_class"];
            $emp_attribute[$key] = $value["emp_attribute"];
            $emp_dept[$key] = $value["emp_dept"];
            $emp_room[$key] = $value["emp_room"];
        }

        //所属検索初期条件を設定
        if ( $report_db_read_div > 0 ) {
            $search_emp_class = $emp_class;
        }
        if ( $report_db_read_div > 1 ) {
            $search_emp_attribute = $emp_attribute;
        }
        if ( $report_db_read_div > 2 ) {
            $search_emp_dept = $emp_dept;
        }
        if ( $report_db_read_div > 3 ) {
            $search_emp_room = $emp_room;
        }
        
        
        
    }
}


// 検索パラメータ取得
if ($search_date_target == "") {
    $search_date_target = "report_date";
}
$search_info = array();
$search_info["search_date_target"] = $search_date_target;
$search_info["search_date_late_mode"] = $search_date_late_mode;
$search_info["search_date_mode"] = $search_date_mode;
$search_info["search_date_year"] = $search_date_year;
$search_info["search_date_month"] = $search_date_month;
$search_info["search_date_ymd_st"] = $search_date_ymd_st;
$search_info["search_date_ymd_ed"] = $search_date_ymd_ed;
$search_info["search_evaluation_date"] = $search_evaluation_date;
$search_info["search_torey_date_in_search_area_start"] = $search_torey_date_in_search_area_start;
$search_info["search_torey_date_in_search_area_end"] = $search_torey_date_in_search_area_end;
$search_info["search_incident_date_start"] = $search_incident_date_start;
$search_info["search_incident_date_end"] = $search_incident_date_end;

$search_info["search_emp_class"] = $search_emp_class;
$search_info["search_emp_attribute"] = $search_emp_attribute;
$search_info["search_emp_dept"] = $search_emp_dept;
$search_info["search_emp_room"] = $search_emp_room;
$search_info["search_emp_job"] = $search_emp_job;
$search_info["search_emp_name"] = $search_emp_name;
$search_info["search_patient_id"] = $search_patient_id;
$search_info["search_patient_name"] = $search_patient_name;
$search_info["search_patient_year_from"] = $search_patient_year_from;
$search_info["search_patient_year_to"] = $search_patient_year_to;
//$search_info["search_toggle_div"] = $search_toggle_div;
$search_info["search_report_no"] = $search_report_no;

$search_info["search_title"] = $search_title;
$search_info["search_incident"] = $search_incident;
$search_info["title_and_or_div"] = $title_and_or_div;
$search_info["incident_and_or_div"] = $incident_and_or_div;
$search_info["search_toggle_div"] = 'open'; // 検索画面は常に開いておく

$search_info["search_emp_all"] = $search_emp_all;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$INCIDENT_TITLE?> | 受信トレイ</title>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">

<!-- ヘッダー START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr height="32" bgcolor="#35B341">
    <td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>事案検索</b></font></td>
    <td>&nbsp</td>
    <td width="10">&nbsp</td>
    <td width="32" align="center">
        <a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる"
        width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
    </tr>
</table>
<!-- ヘッダー END -->
    <script type="text/javascript">
    //list_idがmail_idである場合はtrue、report_idの場合はfalse。
    var m_list_id_is_mail_id = true;
    <?php
    $file = get_file_name_from_php_self($fname);
    if ( $file != "hiyari_rpt_recv_torey.php" && $file != "hiyari_rpt_send_torey.php") {
    ?>
    m_list_id_is_mail_id = false;
    <?php } ?>
    //受信トレイ画面の場合はrecv、送信済みトレイ画面の場合はsend、以外の場合は空文字。
    var mail_id_mode = "";
    <?php if($file == "hiyari_rpt_recv_torey.php") { ?>
    m_mail_id_mode = "recv";
    <?php } else if ( $file == "hiyari_rpt_send_torey.php" ) { ?>
    m_mail_id_mode = "send";
    <?php } ?>

    //亀画像選択
    function rp_action_image_select() {
        var url = "hiyari_image_select.php?session=<?=$session?>";
        show_rp_sub_window(url);
    }

    //レポート編集(レポートID指定)
    function rp_action_update_from_report_id(report_id,mail_id)
    {
        var url = "hiyari_easyinput.php?session=<?=$session?>&gamen_mode=update&report_id=" + report_id + "&callerpage=" + "<?=$file?>" + "&mail_id=" + mail_id;
        show_rp_sub_window(url);
    }
    //下書き編集(レポートID指定)
    function rp_action_shitagaki_update_from_report_id(report_id)
    {
        var url = "hiyari_easyinput.php?session=<?=$session?>&gamen_mode=shitagaki&report_id=" + report_id;
        show_rp_sub_window(url);
    }
    //メール内容表示
    function rp_action_mail_disp(mail_id)
    {
        var url = "hiyari_rp_mail_disp.php?session=<?=$session?>&mail_id_mode=" + m_mail_id_mode + "&mail_id=" + mail_id;
        show_rp_sub_window(url);
    }
    //受信者一覧表示
    function rp_action_recv_emp_list_disp(mail_id)
    {
        var url = "hiyari_rp_mail_recv_emp_list.php?session=<?=$session?>&mail_id=" + mail_id;
        show_rp_sub_window(url);
    }

    function rp_action_analysis_disp(report_id)
    {
        var url = "hiyari_easyinput.php?session=<?=$session?>&gamen_mode=analysis&report_id=" + report_id + "&non_update_flg=true";
        show_rp_sub_window(url);
    }

    //新規登録
    function rp_action_new()
    {
        //選択無条件
        var url = "hiyari_easyinput.php?session=<?=$session?>&gamen_mode=new";
        show_rp_sub_window(url);
    }
    //返信
    function rp_action_return()
    {
        //単一選択
        var list_ids = get_selected_list_ids();
        if(! check_single_select(list_ids))
        {
            return;
        }

        //※リストID=メールIDを前提とする。
        var url = "hiyari_rp_mail_input.php?session=<?=$session?>&mode=return&mail_id=" + list_ids[0] + "&mail_id_mode=" + m_mail_id_mode;
        show_rp_sub_window(url);
    }
    //転送
    function rp_action_forward()
    {
        //単一選択
        var list_ids = get_selected_list_ids();
        if(! check_single_select(list_ids))
        {
            return;
        }

        //※リストID=メールIDを前提とする。
        var url = "hiyari_rp_mail_input.php?session=<?=$session?>&mode=forward&mail_id=" + list_ids[0] + "&mail_id_mode=" + m_mail_id_mode;
        show_rp_sub_window(url);
    }
    //削除
    function rp_action_delete()
    {
        //複数選択
        var list_ids = get_selected_list_ids();
        if(! check_multi_select(list_ids))
        {
            return;
        }

        if(! confirm("削除してもよろしいですか？"))
        {
            return;
        }

        document.list_form.mode.value="delete";
        document.list_form.submit();
    }
    //削除取消
    function rp_action_delete_cancel()
    {
        //複数選択
        var list_ids = get_selected_list_ids();
        if(! check_multi_select(list_ids))
        {
            return;
        }

        document.list_form.mode.value="delete_cancel";
        document.list_form.submit();
    }
    //ゴミ箱を空にする
    function rp_action_kill()
    {
        //        //複数選択
        //        var list_ids = get_selected_list_ids();
        //        if(! check_multi_select(list_ids))
        //        {
        //            return;
        //        }
        //選択して削除ではなく、表示されていないものも含めて全削除。

        if(! confirm("ゴミ箱内の全ての報告を削除します。\nゴミ箱から削除された報告は元に戻すことが出来なくなります。\nまたメール等、その報告に紐付く情報全てにアクセス出来なくなります。\nよろしいでしょうか？"))
        {
            return;
        }

        document.list_form.mode.value="kill";
        document.list_form.submit();
    }



    //レポート編集
    function rp_action_update()
    {
        //単一選択
        var list_ids = get_selected_list_ids();
        if(! check_single_select(list_ids))
        {
            return;
        }

        var report_id = get_report_id_from_list_id(list_ids[0]);
        var url = "hiyari_easyinput.php?session=<?=$session?>&gamen_mode=update&report_id=" + report_id + "&callerpage=" + "<?=$file?>";
        if(m_list_id_is_mail_id)
        {
            url = url + "&mail_id=" + list_ids[0];
            url = url + "&mail_id_mode=" + m_mail_id_mode;
        }
        show_rp_sub_window(url);
    }
    //分析・再発防止
    function rp_action_analysis()
    {
        //単一選択
        var list_ids = get_selected_list_ids();
        if(! check_single_select(list_ids))
        {
            return;
        }

        var report_id = get_report_id_from_list_id(list_ids[0]);
        var url = "hiyari_easyinput.php?session=<?=$session?>&gamen_mode=analysis&report_id=" + report_id;
        if(m_list_id_is_mail_id)
        {
            url = url + "&mail_id=" + list_ids[0];
            url = url + "&mail_id_mode=" + m_mail_id_mode;
        }
        show_rp_sub_window(url);
    }
    //下書き編集
    function rp_action_shitagaki_update()
    {
        //単一選択
        var list_ids = get_selected_list_ids();
        if(! check_single_select(list_ids))
        {
            return;
        }

        var report_id = get_report_id_from_list_id(list_ids[0]);
        rp_action_shitagaki_update_from_report_id(report_id);
    }
    //既読・未読
    function rp_action_readflg()
    {
        //複数選択
        var list_ids = get_selected_list_ids();
        if(! check_multi_select(list_ids))
        {
            return;
        }

        document.list_form.mode.value="read_change";
        document.list_form.submit();
    }
    //進捗登録
    function rp_action_progress()
    {
        //単一選択
        var list_ids = get_selected_list_ids();
        if(! check_single_select(list_ids))
        {
            return;
        }

        var report_id = get_report_id_from_list_id(list_ids[0]);
        var url = "hiyari_rp_progress.php?session=<?=$session?>&report_id=" + report_id;
        if(m_list_id_is_mail_id)
        {
            url = url + "&mail_id=" + list_ids[0];
            url = url + "&mail_id_mode=" + m_mail_id_mode;
        }
        show_rp_sub_window(url);
    }
    //エクセル出力
    function rp_action_report_excel()
    {
        var list_ids = get_selected_list_ids();
        //単一選択の場合:単一出力
        if(list_ids.length == 1)
        {
            var report_id = get_report_id_from_list_id(list_ids[0]);
            document.excel_form.report_id.value=report_id;
            document.excel_form.submit();
        }
        //未選択の場合:リスト出力
        else if(list_ids.length == 0)
        {
            var report_id_list = document.list_excel_form.report_id_list.value;
            if(report_id_list == "")
            {
                alert("出力する報告がありません。");
            }
            else
            {
                var h = window.screen.availHeight - 30;
                var w = window.screen.availWidth - 10;
                var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;
                window.open("", "list_excel_window", option);
                document.list_excel_form.submit();
            }
        }
        //それ以外(複数選択の場合):NG
        else
        {
            alert("選択しない、もしくは1件だけ選択してください。 (" + list_ids.length + "件選択されています。)");
        }
    }

    //確認コメント一覧表示
    function rp_action_progress_comment_list(report_id,auth)
    {
        var url = "hiyari_rp_progress_comment_list.php?session=<?=$session?>&report_id=" + report_id + "&auth=" + auth;
        show_rp_sub_window(url);
    }

    //選択されているリストID配列を取得します。
    function get_selected_list_ids()
    {
        var result = new Array();
        var r_i = 0;

        var list_select_objs = document.getElementsByName("list_select[]");
        for(var i = 0; i < list_select_objs.length ; i++)
        {
            list_select_obj = list_select_objs.item(i);
            if(list_select_obj.checked)
            {
                result[r_i] = list_select_obj.value;
                r_i++;
            }
        }

        return result;
    }

    //単一選択チェック
    function check_single_select(ids)
    {
        if(ids.length < 1)
        {
            alert("選択されていません。");
            return false;
        }
        if(ids.length != 1)
        {
            alert("1件だけ選択してください。 (" + ids.length + "件選択されています。)");
            return false;
        }
        return true;
    }

    //複数選択チェック
    function check_multi_select(ids)
    {
        if(ids.length < 1)
        {
            alert("選択されていません。");
            return false;
        }
        return true;
    }


    //リストIDからレポートIDを取得
    function get_report_id_from_list_id(list_id)
    {
        var obj = document.getElementById("report_id_of_list_id_"+list_id);
        return obj.value;
    }


    //レポーティング子画面を表示
    function show_rp_sub_window(url)
    {
        var h = window.screen.availHeight - 30;
        var w = window.screen.availWidth - 10;
        var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;
//      var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=1000,height=700";

        window.open(url, '_blank',option);
    }

    // ソート処理
    function set_report_list_sort(item)
    {

        var sort_item = document.list_form.sort_item.value;

        if(sort_item == item)
        {
            var sort_div = document.list_form.sort_div.value

            if(sort_div == "0")
            {
                // 降順
                document.list_form.sort_div.value = "1";
            }
            else
            {
                // 昇順
                document.list_form.sort_div.value = "0";
            }
        }
        else
        {
            // 昇順
            document.list_form.sort_div.value = "0";
        }

        document.list_form.sort_item.value = item;
        document.list_form.mode.value="sort";
        document.list_form.submit();
    }

    </script>




    <?
    $file = get_file_name_from_php_self($fname);
    ?>
    <script type="text/javascript">

    //検索条件入力画面を表示します。
    function show_search_input_page()
    {

//      var url = "hiyari_rp_torey_search.php?session=<?=$session?>&caller=<?=$file?>";
//      show_rp_sub_window(url);

//      //TODO ダミー実装
//      var obj = new Object();
//      obj.search_emp_class     = prompt("class","");
//      obj.search_emp_attribute = prompt("attribute","");
//      obj.search_emp_dept      = prompt("dept","");
//      obj.search_emp_room      = prompt("room","");
//      obj.search_emp_job       = prompt("job","");
//      obj.search_emp_name      = prompt("name","");
//      set_search_input(obj);
        var display = document.getElementById('search_toggle').style.display;
        if(display == '')
        {
            display = 'none';
            document.list_form.search_toggle_div.value = '';
        }
        else
        {
            display = '';
            document.list_form.search_toggle_div.value = 'open';
        }
        document.getElementById('search_toggle').style.display = display;

        // デフォルト設定
        if (document.list_form.search_default_date_start) {
            if (document.getElementById('search_torey_date_in_search_area_start').value == '') {

                document.getElementById('search_torey_date_in_search_area_start').value = document.list_form.search_default_date_start.value;
            }
            if (document.getElementById('search_torey_date_in_search_area_end').value == '') {
                document.getElementById('search_torey_date_in_search_area_end').value = document.list_form.search_default_date_end.value;
            }
        }

        <?
        if($file == "hiyari_rpt_report_classification.php") {
        ?>
        document.getElementById('auto_group_flg').checked = false;
        <?}?>
    }

    //検索条件情報を取得します。※この関数は他画面より呼び出されます。
    function get_search_input()
    {
        //検索条件取得
        var obj = new Object();
        obj.search_emp_class = document.list_form.search_emp_class.value;
        obj.search_emp_attribute = document.list_form.search_emp_attribute.value;
        obj.search_emp_dept = document.list_form.search_emp_dept.value;
        obj.search_emp_room = document.list_form.search_emp_room.value;
        obj.search_emp_job = document.list_form.search_emp_job.value;
        obj.search_emp_name = document.list_form.search_emp_name.value;

        <?if(get_file_name_from_php_self($fname) != "hiyari_rpt_shitagaki_db.php") {?>
        obj.search_patient_id = document.list_form.search_patient_id.value;
        obj.search_patient_name = document.list_form.search_patient_name.value;
        obj.search_patient_year_from = document.list_form.search_patient_year_from.value;
        obj.search_patient_year_to = document.list_form.search_patient_year_to.value;
        <?}?>

        return obj;
    }

    //検索条件情報を設定し、検索します。※この関数は他画面より呼び出されます。
    function set_search_input(obj)
    {
        //検索条件設定
        document.list_form.search_emp_class.value = obj.search_emp_class;
        document.list_form.search_emp_attribute.value = obj.search_emp_attribute;
        document.list_form.search_emp_dept.value = obj.search_emp_dept;
        document.list_form.search_emp_room.value = obj.search_emp_room;
        document.list_form.search_emp_job.value = obj.search_emp_job;
        document.list_form.search_emp_name.value = obj.search_emp_name;

        <?if(get_file_name_from_php_self($fname) != "hiyari_rpt_shitagaki_db.php") {?>
        document.list_form.search_patient_id.value = obj.search_patient_id;
        document.list_form.search_patient_name.value = obj.search_patient_name;
        document.list_form.search_patient_year_from.value = obj.search_patient_year_from;
        document.list_form.search_patient_year_to.value = obj.search_patient_year_to;
        <?}?>

        //検索実行
        document.list_form.page.value=1;
        document.list_form.submit();
    }

    </script>

    <table border="0" cellspacing="0" cellpadding="0" style="margin-top:10px;margin-left:10px;">
        <tr>
            <td align="center"><img src="img/hiyari_rp_search.gif" style="cursor: pointer;" onclick="show_search_input_page();"></td>
        </tr>
        <tr>
            <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">検索</font></td>
        </tr>
    </table>

<?
show_rp_torey_common_header_html($session,$fname,$con);
// 検索画面表示
show_rp_torey_search_area($session,$fname,$con,$search_info);
?>




<!-- 分類・一覧・ページ選択 START -->
<form name="list_form" method="post" action="hiyari_analysis_search.php">
<input type="hidden" name="is_postback" value="true">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="mode" value="">
<input type="hidden" name="search_date_target" value="<?=$search_date_target?>">
<input type="hidden" name="search_date_mode" value="<?=$search_date_mode?>">
<input type="hidden" name="search_date_late_mode" value="<?=$search_date_late_mode?>">
<input type="hidden" name="search_date_year" value="<?=$search_date_year?>">
<input type="hidden" name="search_date_month" value="<?=$search_date_month?>">
<input type="hidden" name="search_date_ymd_st" value="<?=$search_date_ymd_st?>">
<input type="hidden" name="search_date_ymd_ed" value="<?=$search_date_ymd_ed?>">
<input type="hidden" name="search_emp_class" value="<?=implode(',', $search_emp_class)?>">
<input type="hidden" name="search_emp_attribute" value="<?=implode(',', $search_emp_attribute)?>">
<input type="hidden" name="search_emp_dept" value="<?=implode(',', $search_emp_dept)?>">
<input type="hidden" name="search_emp_room" value="<?=implode(',', $search_emp_room)?>">
<input type="hidden" name="search_emp_job" value="<?=$search_emp_job?>">
<input type="hidden" name="search_emp_name" value="<?=$search_emp_name?>">
<input type="hidden" name="search_patient_id" value="<?=$search_patient_id?>">
<input type="hidden" name="search_patient_name" value="<?=$search_patient_name?>">
<input type="hidden" name="search_patient_year_from" value="<?=$search_patient_year_from?>">
<input type="hidden" name="search_patient_year_to" value="<?=$search_patient_year_to?>">
<input type="hidden" name="search_toggle_div" value="<?=$search_toggle_div?>">

<input type="hidden" name="search_report_no" value="<?=$search_report_no?>">
<input type="hidden" name="search_evaluation_date" value="<?=$search_evaluation_date?>">

<input type="hidden" name="search_torey_date_in_search_area_start" value="<?=$search_torey_date_in_search_area_start?>">
<input type="hidden" name="search_torey_date_in_search_area_end" value="<?=$search_torey_date_in_search_area_end?>">
<input type="hidden" name="search_incident_date_start" value="<?=$search_incident_date_start?>">
<input type="hidden" name="search_incident_date_end" value="<?=$search_incident_date_end?>">
<input type="hidden" name="search_default_date_start" value="<?=$search_default_date_start?>">
<input type="hidden" name="search_default_date_end" value="<?=$search_default_date_end?>">

<input type="hidden" name="search_title" value="<?=$search_title?>">
<input type="hidden" name="search_incident" value="<?=$search_incident?>">
<input type="hidden" name="title_and_or_div" value="<?=$title_and_or_div?>">
<input type="hidden" name="incident_and_or_div" value="<?=$incident_and_or_div?>">


<input type="hidden" name="input_mode" value="<?=$input_mode?>">


<input type="hidden" id="search_emp_all" name="search_emp_all" value="<?=$search_emp_all?>">


<input type="hidden" name="page" value="<?=$page?>">
<input type="hidden" name="folder_id" value="<?=$folder_id?>">
<input type="hidden" name="change_report_classification_report_id" value="">
<input type="hidden" name="change_report_classification_folder_id" value="">
<input type="hidden" name="sort_item" value="<?=$sort_item?>">
<input type="hidden" name="sort_div" value="<?=$sort_div?>">
<input type="hidden" name="auto_group_flg" value="<?=$auto_group_flg?>">



</form>
<?



//==============================
//ページングに関する情報
//==============================

//pate省略時は先頭ページを対象とする。
if($page == "")
{
    $page = 1;
}

//一画面内の最大表示件数
$disp_count_max_in_page = 20;

//件数取得
//報告書参照権限がある場合には、それによる。
if($is_report_classification_usable){
    $cnt = get_report_count($con, $fname, "", $search_info);
    //echo("cnt=$cnt");
    $all_cnt = $cnt;
    $list_data_array = get_report_data($con, $fname, $folder_id, $search_info, $page, $sort, $sort_item, $disp_count_max_in_page,"1");
}else{
//報告書参照権限がない場合には、自分のものだけ。

    $sql="select emp_lt_nm,emp_ft_nm from empmst where emp_id='$emp_id'";
    $sel = select_from_table($con,$sql,$cond,$fname);
    if($sel == 0){
        pg_query($con,"rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $row= pg_fetch_all($sel);

    //$search_info["search_date_mode"] = "non";
    //if(is_null($search_info["search_emp_name"])){
        $search_info["search_emp_name_tmp"] =$row[0]["emp_lt_nm"]." ".$row[0]["emp_ft_nm"];
    //}
    $cnt = get_report_count($con, $fname, "", $search_info,"2");
    $all_cnt = $cnt;
    $list_data_array = get_report_data($con, $fname, $folder_id, $search_info, $page, $sort, $sort_item, $disp_count_max_in_page,"2");

    
}

//==============================
//ページングに関する情報
//==============================

//表示開始/終了インデックス
$disp_start_index = ($page - 1)*$disp_count_max_in_page;
$disp_end_index   = $disp_start_index + $disp_count_max_in_page - 1;

//全データ件数
$rec_count = $all_cnt;

if($rec_count == 1 && $list_data_array[0] == "")
{
    $rec_count = 0;
}

//最大ページ数
if($rec_count == 0)
{
    $page_max  = 1;
}
else
{
    $page_max  = floor( ($rec_count-1) / $disp_count_max_in_page ) + 1;
}

?>


<table width="100%" border="0" cellspacing="0" cellpadding="3" class="list_2">
  <tr>
<?
$profile_use = get_inci_profile($fname, $con);
?>
    <td align="center" bgcolor="#FFFBE7"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="javascript:set_report_list_sort('REPORT_NO');">事案番号</a></font></td>



    <td align="center" bgcolor="#FFFBE7"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="javascript:set_report_list_sort('SUBJECT');">件名</a></font></td>



    <td align="center" bgcolor="#FFFBE7"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="javascript:set_report_list_sort('SEND_CLASS');">報告部署</font></a></td>



    <td align="center" bgcolor="#FFFBE7"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="javascript:set_report_list_sort('SEND_NAME');">送信者</font></a></td>



    <td align="center" bgcolor="#FFFBE7"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="javascript:set_report_list_sort('SEND_DATE');">送信日</a></font></td>
  </tr>
<?
foreach($list_data_array as $list_data)
{

    $report_id =  $list_data["report_id"];
    $list_id = $report_id;
    
     $mail_id = $rep_obj->get_newest_mail_id($report_id);
    //$mail_id =  $list_data["mail_id"];

    $title = h($list_data["report_title"]);

    $anonymous_report_flg =  $list_data["anonymous_report_flg"];
    $create_emp_name =  $list_data["registrant_name"];

    $create_date =  $list_data["registration_date"];

    $report_no = $list_data["report_no"];

    if($folder_id == "")
    {
        $report_link_status = $list_data["report_link_status"];
        $little_report_img = get_little_report_img($report_link_status);
    }
    else
    {
        $report_link_status = 0;//全て単独レポートとして表示
        $little_report_img = get_little_report_img($report_link_status);
    }

    $class_nm = "";
    if($profile_use["class_flg"] == "t" && $list_data["registrant_class"] != "")
    {
//      $class_nm .= get_class_nm($con,$list_data["registrant_class"],$fname);
        $class_nm .= $list_data["class_nm"];
    }
    if($profile_use["attribute_flg"] == "t" && $list_data["registrant_attribute"] != "")
    {
//      $class_nm .= get_atrb_nm($con,$list_data["registrant_attribute"],$fname);
        $class_nm .= $list_data["atrb_nm"];
    }
    if($profile_use["dept_flg"] == "t" && $list_data["registrant_dept"] != "")
    {
//      $class_nm .= get_dept_nm($con,$list_data["registrant_dept"],$fname);
        $class_nm .= $list_data["dept_nm"];
    }
    if($profile_use["room_flg"] == "t" && $list_data["registrant_room"] != "")
    {
//      $class_nm .= get_room_nm($con,$list_data["registrant_room"],$fname);
        $class_nm .= $list_data["room_nm"];
    }

    $report_no_align = "left";
    if($report_link_status == "2")
    {
        $report_no_align = "right";
    }

    //評価予定日 ※通常は取得できない。
    $evaluation_date = $list_data["input_item_620_33"];
    //発生日 ※通常は取得できない。
    $incident_date = $list_data["input_item_100_5"];

    $report_read_flg = is_report_db_readable_report_post($session,$fname,$con,$list_data["registrant_class"],$list_data["registrant_attribute"],$list_data["registrant_dept"],$list_data["registrant_room"]);
    ?>
  <tr>
    <td align="<?=$report_no_align?>">
        <div id="dd1_target_report_<?=$report_id?>" class="dd1_target">
        <div id="dd1_item_report_<?=$report_id?>" class="dd1_item">

        <table border="0" cellspacing="0" cellpadding="0" class="list_in">
        <tr>
        <td width="20" align="center">
          <div id="dd1_handle_report_<?=$report_id?>" class="dd1_handle" style="cursor:move;">
          <img src="<?=$little_report_img?>" style="vertical-align:middle;">
          </div>
        </td>
        <td width="100" align="left">
          <nobr>
          <a href="javascript:opener.add_analysis_list('<?=$report_id?>', '<?=$report_no?>', <?=$input_mode?>);">
          <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><?=$report_no?></font>
          </a>
          </nobr>
        </td>
        </tr>
        </table>

        </div>
        </div>
    </td>
    <td>
       
            <a href="javascript:rp_action_update_from_report_id('<?=$report_id?>','<?=$mail_id?>');">
           
            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><?=$title?></font>
         
            </a>
           
        </td>
    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><?=$class_nm?></font></td>



    <td>
    <? show_anonymous_emp_name($con,$fname,$create_emp_name,$anonymous_report_flg,$mouseover_popup_flg,"regist"); ?>
    </td>



    <? if($search_date_mode != "non"){?>
        <? if($search_date_target == "incident_date"){ ?>
    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><?=$incident_date?></font></td>
        <? }elseif($search_date_target == "evaluation_date"){ ?>
    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><?=$evaluation_date?></font></td>
        <? }else{ ?>
    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><?=$create_date?></font></td>
        <? } ?>
    <? }else{ ?>
        <? if($search_evaluation_date == ""){ ?>
    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><?=$create_date?></font></td>
        <? }else{ ?>
    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><?=$evaluation_date?></font></td>
        <? } ?>
    <? } ?>






  </tr>
    <?


}
?>
</table>
</form>

<!-- ページ選択 START -->
<?php show_rp_page_select_area($session,$fname,$con,$page_max,$page); ?>
<!-- ページ選択 END -->

<?php

//条件にあう報告書の件数を取得
function get_report_count($con, $fname, $folder_id, $search_info,$selfflg) {

    $cond_del_flg = ($folder_id != "delete") ? "not a.del_flag" : "a.del_flag";

    list ($cond_kikan, $arr_join_field) = get_kikan_cond($search_info);

    $sql   = "select count(*) as cnt ";
    $sql   .= " from inci_report a ";
    //職員
    $sql .= " left join empmst e on e.emp_id = a.registrant_id ";
    //自動分類条件
    list ($cond_folder, $folder_join_field) = get_folder_cond($con, $fname, $folder_id);
    //join
    $arr_join_field = array_merge($arr_join_field, $folder_join_field);

    //検索条件
    $cond_search = "";
    if ($search_info["search_date_mode"] == "non") {
        list ($cond_search, $search_join_field) = get_search_cond($con,$fname,$search_info);

        $arr_join_field = array_merge($arr_join_field, $search_join_field);
    }

    $arr_join_field = array_unique($arr_join_field);

//echo("cnt=".count($arr_join_field));
//print_r($arr_join_field);

    $join_str = "";
//  foreach ($i=0; $i<count($arr_join_field); $i++) {
    foreach ($arr_join_field as $wk_i => $wk_join_field) {
        list ($gi_g, $gi_i) = split("_", $wk_join_field);
        $join_str .= "left join inci_easyinput_data a_{$gi_g}_{$gi_i} on a.eid_id = a_{$gi_g}_{$gi_i}.eid_id and a_{$gi_g}_{$gi_i}.grp_code = {$gi_g} and a_{$gi_g}_{$gi_i}.easy_item_code = {$gi_i} ";
    }
    $sql .= $join_str;

//echo("join_str=$join_str");

    //期間条件
    if ($cond_kikan != "") {
        $cond_kikan = $cond_kikan." and ";
    }

    $cond  = " where $cond_kikan not a.shitagaki_flg and $cond_del_flg and not a.kill_flg ";
    $cond .= $cond_search;
    if($selfflg =="2"){
        $name = $search_info["search_emp_name_tmp"];
        $cond .=" and registrant_name= '$name'";
    }
    if ($folder_id != "" && $folder_id != "delete" && $cond_folder != "") {
        $cond .= $cond_folder;
//echo("join_str=$join_str");
//exit;
    }

    $sel = select_from_table($con,$sql,$cond,$fname);
    if($sel == 0){
        pg_query($con,"rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    return pg_fetch_result($sel,0,"cnt");

}


//条件にあう報告書のデータを取得
function get_report_data($con, $fname, $folder_id, $search_info, $page, $sort, $sort_item, $disp_count_max_in_page,$selfflg) {

    $cond_del_flg = ($folder_id != "delete") ? "not a.del_flag" : "a.del_flag";

    list ($cond_kikan, $arr_join_field) = get_kikan_cond($search_info);

    //自動分類条件
    list ($cond_folder, $folder_join_field) = get_folder_cond($con, $fname, $folder_id);

    $arr_join_field = array_merge($arr_join_field, $folder_join_field);
    //検索条件
    $cond_search = "";
    if ($search_info["search_date_mode"] == "non") {
        list ($cond_search, $search_join_field) = get_search_cond($con,$fname,$search_info);
        $arr_join_field = array_merge($arr_join_field, $search_join_field);
    }

    $arr_join_field = array_unique($arr_join_field);

    $sql   = "select ";
    $sql   .= " a.report_id ";
    $sql   .= ",a.report_title ";
    $sql   .= ",a.registrant_name ";
    $sql   .= ",a.registration_date ";
    $sql   .= ",a.del_flag ";
    $sql   .= ",a.report_no ";
    $sql   .= ",a.shitagaki_flg ";
    $sql   .= ",a.anonymous_report_flg ";
    $sql   .= ",a.registrant_class ";
    $sql   .= ",a.registrant_attribute ";
    $sql   .= ",a.registrant_dept ";
    $sql   .= ",a.registrant_room ";
    $sql   .= ",a.kill_flg ";
    $sql   .= ",e.emp_lt_nm || e.emp_ft_nm as registrant_name1 ";
    $sql   .= ",e.emp_kn_lt_nm || e.emp_kn_ft_nm as registrant_name2 ";
    $sql   .= ",l.report_link_id ";
    $sql   .= ",l.report_link_status ";
    $sql   .= ",l.main_report_id ";
    $sql   .= ",l.main_report_id ";
    $sql   .= ",classmst.class_nm ";
    $sql   .= ",atrbmst.atrb_nm ";
    $sql   .= ",deptmst.dept_nm ";
    $sql   .= ",classroom.room_nm ";

    if (in_array("100_5", $arr_join_field)) {
        $sql   .= ",a_100_5.input_item as input_item_100_5 ";
    }
    if (in_array("620_33", $arr_join_field)) {
        $sql   .= ",a_620_33.input_item as input_item_620_33 ";
    }

    $sql   .= " from inci_report a ";
    //職員
    $sql .= " left join empmst e on e.emp_id = a.registrant_id ";
    //関連付
    $sql .= " left join inci_report_link_view l on a.report_id = l.report_id ";
    //部署
    $sql .= " left join classmst on classmst.class_id = a.registrant_class ";
    $sql .= " left join atrbmst on atrbmst.atrb_id = a.registrant_attribute ";
    $sql .= " left join deptmst on deptmst.dept_id = a.registrant_dept ";
    $sql .= " left join classroom on classroom.room_id = a.registrant_room ";
    $join_str = "";
//  for ($i=0; $i<count($arr_join_field); $i++) {
//      list ($gi_g, $gi_i) = split("_", $arr_join_field[$i]);
    foreach ($arr_join_field as $wk_i => $wk_join_field) {
        list ($gi_g, $gi_i) = split("_", $wk_join_field);
        $join_str .= "left join inci_easyinput_data a_{$gi_g}_{$gi_i} on a.eid_id = a_{$gi_g}_{$gi_i}.eid_id and a_{$gi_g}_{$gi_i}.grp_code = {$gi_g} and a_{$gi_g}_{$gi_i}.easy_item_code = {$gi_i} ";
    }
    $sql .= $join_str;

    //期間条件
    if ($cond_kikan != "") {
        $cond_kikan = $cond_kikan." and ";
    }

    $cond  = " where $cond_kikan not a.shitagaki_flg and $cond_del_flg and not a.kill_flg ";
    $cond .= $cond_search;
    if($selfflg =="2"){
        $name = $search_info["search_emp_name_tmp"];
        $cond .=" and registrant_name= '$name'";
    }
    if ($folder_id != "" && $folder_id != "delete" && $cond_folder != "") {
        $cond .= $cond_folder;
    }

    if($_POST['sort_div'] == "0")
    {
        $sort = "asc";
    }
    else
    {
        $sort = "desc";
    }

    $order = "order by ";
    switch($sort_item)
    {
        case "REPORT_NO":
            $order .= "a.report_no $sort ";
            break;
        case "SUBJECT":
            $order .= "a.report_title $sort, a.report_id desc ";
            break;
        case "SEND_CLASS":
            $order .= "a.registrant_class $sort, a.registrant_attribute $sort , a.registrant_dept $sort , a.registrant_room, a.report_id desc ";
            break;
        case "SEND_NAME":
            $order .= "a.registrant_name $sort, a.report_id desc ";
            break;
        case "SEND_DATE":
            $order .= "a.registration_date $sort, a.report_id desc ";
            break;
        default:
            //何もしない。
            $order .= "l.main_report_id desc, l.report_link_status, a.report_id desc ";
//          $order = "order by a.report_id desc";
            break;
    }

    // offset limit
    $offset = ($page - 1)*$disp_count_max_in_page;
    $cond .= " $order offset $offset limit $disp_count_max_in_page ";

    $sel = select_from_table($con,$sql,$cond,$fname);
    if($sel == 0){
        pg_query($con,"rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    return pg_fetch_all($sel);

}

//期間の条件を取得
//権限で見れる部署も条件に追加する
function get_kikan_cond($search_info) {

    $search_date_target = $search_info["search_date_target"];
    $search_date_late_mode = $search_info["search_date_late_mode"];
    $search_date_mode = $search_info["search_date_mode"];
    $search_date_year = $search_info["search_date_year"];
    $search_date_month = $search_info["search_date_month"];
    $search_date_ymd_st = $search_info["search_date_ymd_st"];
    $search_date_ymd_ed = $search_info["search_date_ymd_ed"];
    $search_evaluation_date = $search_info["search_evaluation_date"];
    $search_torey_date_in_search_area_start = $search_info["search_torey_date_in_search_area_start"];
    $search_torey_date_in_search_area_end = $search_info["search_torey_date_in_search_area_end"];
    $search_incident_date_start = $search_info["search_incident_date_start"];
    $search_incident_date_end = $search_info["search_incident_date_end"];

    $search_emp_class = $search_info["search_emp_class"];
    $search_emp_attribute = $search_info["search_emp_attribute"];
    $search_emp_dept = $search_info["search_emp_dept"];
    $search_emp_room = $search_info["search_emp_room"];

    $search_title = $search_info["search_title"];
    $search_incident = $search_info["search_incident"];
    $title_and_or_div = $search_info["title_and_or_div"];
    $incident_and_or_div = $search_info["incident_and_or_div"];



    $cond = "";
    $join_field = array();

    //日付による絞込み(フィールド特定)
    if($search_date_target == "incident_date")
    {
        $search_date_field = "a_100_5.input_item";
        $join_field[] = "100_5";
    }
    elseif($search_date_target == "evaluation_date")
    {
        $search_date_field = "a_620_33.input_item";
        $join_field[] = "620_33";
    }
    else
    {
        $search_date_field = "a.registration_date";
    }
    //日付による絞込み
    if($search_date_mode == "month")
    {
        $search_date_1 = $search_date_year."/".$search_date_month;
        $cond .= " $search_date_field like '$search_date_1%' ";
    }
    elseif($search_date_mode == "ymd_st_ed")
    {
        if($search_date_ymd_st != "")
        {
            $cond .= " $search_date_field >= '$search_date_ymd_st' ";
        }
        if($search_date_ymd_ed != "")
        {
            if($search_date_ymd_st != "")
            {
                $cond .= " and ";
            }
            $cond .= " $search_date_field <= '$search_date_ymd_ed' ";
        }
    }
    elseif($search_date_mode == "non")
    {
    }
    else
    {
        if($search_date_late_mode == "1month")
        {
            //最近１ヶ月(先月の今日以降)
            $search_date_y = date("Y");
            $search_date_m = date("n");
            $search_date_d = date("d");

            $search_date_m--;
            if($search_date_m <= 0)
            {
                $search_date_y--;
                $search_date_m = 12;
            }
            if($search_date_m <= 9)
            {
                $search_date_m = "0".$search_date_m;
            }
        }
        elseif($search_date_late_mode == "2week")
        {
            //最近２週間(先々週の今日以降)
            $date_today = mktime (0, 0, 0, date("m"), date("d"),  date("y"));
            $date_2week = $date_today - 1209600;//2w * (7d/w) * (24h/d) * (60m/h) * (60s/m) = 1209600s
            $search_date_y = date('Y', $date_2week);
            $search_date_m = date('n', $date_2week);
            $search_date_d = date('d', $date_2week);
            if($search_date_m <= 9)
            {
                $search_date_m = "0".$search_date_m;
            }
        }
        else//all
        {
            //過去全件の場合:絞り込まない。

            //報告日以外の場合は入力ありのみ絞り込む(最過去)
            if($search_date_target != "report_date")
            {
                $search_date_y = '0000';
                $search_date_m = '00';
                $search_date_d = '00';
            }
        }

        if($search_date_target != "report_date" || $search_date_late_mode != "all")
        {
            $search_date_1 = $search_date_y."/".$search_date_m."/".$search_date_d;
            $cond .= " (not $search_date_field isnull) and ($search_date_field <> '') and ($search_date_field > '$search_date_1') ";
        }
    }

    //報告部署
    /*
    if($search_emp_class != "")
    {
        if ($cond != "") {
            $cond .= " and ";
        }
        $cond .= " a.registrant_class = $search_emp_class ";
    }
    if($search_emp_attribute != "")
    {
        if ($cond != "") {
            $cond .= " and ";
        }
        $cond .= " a.registrant_attribute = $search_emp_attribute ";
    }
    if($search_emp_dept != "")
    {
        if ($cond != "") {
            $cond .= " and ";
        }
        $cond .= " a.registrant_dept = $search_emp_dept ";
    }
    if($search_emp_room != "")
    {
        if ($cond != "") {
            $cond .= " and ";
        }
        $cond .= " a.registrant_room = $search_emp_room ";
    }
    */
    //報告部署
    if($search_emp_class[0] != "" && $cond != "") {
        $cond .= " and ( ";
        $flag = false;
    } else if($search_emp_class[0] != ""){
        $cond .= " ( ";
    }
    foreach($search_emp_class as $key => $value) {
        if($value != "")
        {
            if ($cond != "" && $flag == false) {
                $cond .= " ( ";
                $flag = true;
            } else if($cond != ""){
                $cond .= " or ( ";
            } else {
                $cond .= " ( ";
            }
            $cond .= " a.registrant_class = $value ";
        }
        if($search_emp_attribute[$key] != "")
        {
            if ($cond != "") {
                $cond .= " and ";
            }
            $cond .= " a.registrant_attribute = $search_emp_attribute[$key] ";
        }
        if($search_emp_dept[$key] != "")
        {
            if ($cond != "") {
                $cond .= " and ";
            }
            $cond .= " a.registrant_dept = $search_emp_dept[$key] ";
        }
        if($search_emp_room[$key] != "")
        {
            if ($cond != "") {
                $cond .= " and ";
            }
            $cond .= " a.registrant_room = $search_emp_room[$key] ";
        }
        if($search_emp_class[0] != "" && $cond != "") {
            $cond .= " ) ";
        }
    }
    if($search_emp_class[0] != "" && $cond != "") {
        $cond .= " ) ";
    }


    return array($cond, $join_field);
}

//自動分類条件を取得
function get_folder_cond($con, $fname, $folder_id) {

    if ($folder_id == "" || $folder_id == "delete") {
        return array("", array());
    }

    $arr_auto_classification_folder = get_auto_classification_folder_info($con, $fname,$emp_id, $folder_id);
    $arr_folder_info_400 = get_folder_info_400($con, $fname, $folder_id);
    $arr_folder_info_1000 = get_folder_info_1000($con, $fname, $folder_id);
    $arr_folder_info_1000_detail = get_folder_info_1000_detail($con, $fname, $folder_id);

    $cond_folder = "";
    $join_field = array();
    $arr_cond_item = array();

    $arr_folder_info = $arr_auto_classification_folder[0];

    // 患者影響レベル・未入力チェック
    if($arr_folder_info["inci_level_regist_flg"] == "f") {
        $arr_cond_item[] .= " a_90_10.input_item is null ";
        $join_field[] = "90_10";
    }
    // 発生要因・未入力チェック
    if($arr_folder_info["happened_cause_regist_flg"] == "f") {
        $arr_cond_item[] .= " a_600_10.input_item is null ";
        $join_field[] = "600_10";
    }
    // インシデント背景要因・未入力チェック
    if($arr_folder_info["inci_background_regist_flg"] == "f") {
        $arr_cond_item[] .= " a_610_20.input_item is null ";
        $join_field[] = "610_20";
    }
    // インシデント改善策・未入力チェック
    if($arr_folder_info["improve_plan_regist_flg"] == "f") {
        $arr_cond_item[] .= " a_620_30.input_item is null ";
        $join_field[] = "620_30";
    }
    // 効果の確認・未入力チェック
    if($arr_folder_info["effect_confirm_regist_flg"] == "f") {
        $arr_cond_item[] .= " a_625_35.input_item is null ";
        $join_field[] = "625_35";
    }
    // 警鐘事例・入力チェック
    if($arr_folder_info["warning_case_regist_flg"] == "t") {
        $arr_cond_item[] .= " a_510_20.input_item is not null ";
        $join_field[] = "510_20";
    }
    // 事例分析検討対象・対象チェック
    if($arr_folder_info["analysis_target_flg"] == "t") {
        $arr_cond_item[] .= " a_650_50.input_item = '1' ";
        $join_field[] = "650_50";
    }
    // 表題
    if($arr_folder_info["title_keyword_condition"] != "") {
        $title_keyword = mb_ereg_replace("　"," ",$arr_folder_info["title_keyword_condition"]); //全角スペース除去
        $title_keyword = trim($title_keyword); //前後スペース除去
        $title_keyword_list = mb_split(" ", $title_keyword); //スペース区切り

        $wk_str = "";
        //マルチワードについては常にOR結合
        for ($i=0; $i<count($title_keyword_list); $i++) {
            if ($i > 0) {
                if($arr_folder_info["title_and_or_div"] == 1) {
                    $wk_str .= " and ";
                } else {
                    $wk_str .= " or ";
                }
            }
            $wk_str .= " a.report_title like '%{$title_keyword_list[$i]}%' ";
        }
        $arr_cond_item[] .= " ($wk_str)";
    }

    // インシデントの内容
    if($arr_folder_info["incident_contents_condition"] != "") {
        $incident_contents = mb_ereg_replace("　"," ",$arr_folder_info["incident_contents_condition"]); //全角スペース除去
        $incident_contents = trim($incident_contents); //前後スペース除去
        $incident_contents_list = mb_split(" ", $incident_contents); //スペース区切り

        $wk_str = "";
        //マルチワードについては常にOR結合
        for ($i=0; $i<count($incident_contents_list); $i++) {
            if ($i > 0) {
                if($arr_folder_info["incident_and_or_div"] == 1) {
                    $wk_str .= " and ";
                } else {
                    $wk_str .= " or ";
                }
            }
            $wk_str .= " a_520_30.input_item like '%{$incident_contents_list[$i]}%' ";
        }
        $arr_cond_item[] .= " ($wk_str)";
        $join_field[] = "520_30";
    }

    //報告部署
    if($arr_folder_info["emp_class_condition"] != "")
    {
        $wk_str = " a.registrant_class = ".$arr_folder_info["emp_class_condition"]." ";
        if($arr_folder_info["emp_attribute_condition"] != "") {
            $wk_str .= " and a.registrant_attribute = ".$arr_folder_info["emp_attribute_condition"]." ";
        }
        if($arr_folder_info["emp_dept_condition"] != "") {
            $wk_str .= " and a.registrant_dept = ".$arr_folder_info["emp_dept_condition"]." ";
        }
        if($arr_folder_info["emp_room_condition"] != "") {
            $wk_str .= " and a.registrant_room = ".$arr_folder_info["emp_room_condition"]." ";
        }
        $arr_cond_item[] .= " ($wk_str)";
    }

    //報告者氏名
    if($arr_folder_info["emp_name_condition"] != "")
    {
        //トレイ検索と合わせるため、マルチ検索にはしない。
        $tmp_folder_conditions = mb_ereg_replace("　"," ",$arr_folder_info["emp_name_condition"]); //全角スペース除去
        $tmp_folder_conditions = trim($tmp_folder_conditions); //前後スペース除去
        //匿名
        $anonymous_name = get_anonymous_name($con,$fname);
        if ($anonymous_name == $tmp_folder_conditions) {
            $wk_str = " a.anonymous_report_flg = 't' ";
        } else {
            $wk_str = " (e.emp_lt_nm || e.emp_ft_nm) like '%{$tmp_folder_conditions}%' or (e.emp_kn_lt_nm || e.emp_kn_ft_nm) like '%{$tmp_folder_conditions}%' ";
        }
        $arr_cond_item[] .= " ($wk_str)";
    }
    //報告者職種
    if($arr_folder_info["emp_job_condition"] != "")
    {
        $arr_cond_item[] .= " (a.registrant_job = ".$arr_folder_info["emp_job_condition"].")";
    }
    //患者ID
    if($arr_folder_info["patient_id_condition"] != "")
    {
        $arr_cond_item[] .= " a_210_10.input_item = '".$arr_folder_info["patient_id_condition"]."' ";
        $join_field[] = "210_10";
    }
    //患者氏名
    if($arr_folder_info["patient_name_condition"] != "")
    {
        //トレイ検索と合わせるため、マルチ検索にはしない。
        $tmp_folder_conditions = mb_ereg_replace("　"," ",$arr_folder_info["patient_name_condition"]); //全角スペース除去
        $tmp_folder_conditions = trim($tmp_folder_conditions); //前後スペース除去
        $arr_cond_item[] .= " a_210_20.input_item like '%{$tmp_folder_conditions}%' ";
        $join_field[] = "210_20";
    }
    //患者年齢
    if($arr_folder_info["patient_year_from_condition"] != "" || $arr_folder_info["patient_year_to_condition"] != "")
    {
        $tmp_f = $arr_folder_info["patient_year_from_condition"];
        $tmp_t = $arr_folder_info["patient_year_to_condition"];
        //不明
        if($tmp_f == "900")
        {
            $wk_str = " a_210_40.input_item = '900' ";
        }
        //from - to
        else if($tmp_f != "" && $tmp_t != "")
        {
            $wk_str = " a_210_40.input_item >= '$tmp_f' and a_210_40.input_item <= '$tmp_t' ";
        }
        //from - ""
        else if($tmp_f != "" && $tmp_t == "")
        {
            $wk_str = " a_210_40.input_item >= '$tmp_f' and a_210_40.input_item < '900' ";
        }
        //"" - to
        else if($tmp_f == "" && $tmp_t != "")
        {
            $wk_str = " a_210_40.input_item < '$tmp_t' ";
        }

        $arr_cond_item[] .= " ($wk_str)";
        $join_field[] = "210_40";
    }
    //評価予定日
    if($arr_folder_info["evaluation_date_condition"] != "")
    {
        if($arr_folder_info["evaluation_date_condition"] == "today")
        {
            $date_min = date("Y/m/d");
            $date_max = $date_min;
        }
        elseif($arr_folder_info["evaluation_date_condition"] == "tomorrow")
        {
            $date_min = date("Y/m/d", strtotime("+1 day"));
            $date_max = $date_min;
        }
        elseif($arr_folder_info["evaluation_date_condition"] == "1week")
        {
            $date_min = date("Y/m/d");
            $date_max = date("Y/m/d", strtotime("+7 day"));
        }
        elseif($arr_folder_info["evaluation_date_condition"] == "2week")
        {
            $date_min = date("Y/m/d");
            $date_max = date("Y/m/d", strtotime("+14 day"));
        }
        $arr_cond_item[] = "(a_620_33.input_item >= '$date_min' and a_620_33.input_item <= '$date_max')";
        $join_field[] = "620_33";
    }
    //患者影響レベル
    if($arr_folder_info["use_level_list"] != "")
    {
        $arr_use_level = split(",", $arr_folder_info["use_level_list"] );
        $wk_str = "";
        for ($i=0; $i<count($arr_use_level); $i++) {
            if ($i > 0) {
                $wk_str .= " or ";
            }
            $wk_str .= " a_90_10.input_item = '$arr_use_level[$i]' ";
        }

        $arr_cond_item[] = "($wk_str)";
        if (!in_array("90_10", $join_field)) {
            $join_field[] = "90_10";
        }
    }
    //ヒヤリハット分類
    if($arr_folder_info["use_hiyari_summary_list"] != "")
    {
        $arr_use_hiyari_summary = split(",", $arr_folder_info["use_hiyari_summary_list"] );
        $wk_str = "";
        for ($i=0; $i<count($arr_use_hiyari_summary); $i++) {
            if ($i > 0) {
                $wk_str .= " or ";
            }
            $wk_str .= " a_125_85.input_item = '$arr_use_hiyari_summary[$i]' ";
        }

        $arr_cond_item[] = "($wk_str)";
        $join_field[] = "125_85";
    }
    //インシデントの概要
    if($arr_folder_info["use_incident_summary_list"] != "")
    {
        //item_400 [インシデントの概要 - 1][0場面、1内容]
        // 400_10,400_20
        // 410_10,410_30
        // 420_10,420_20...
        for ($i=0; $i<2; $i++) {
            for ($j=0; $j<10; $j++) {
                if ($i == 0) {
                    $grp_item = "4".$j."0_10";
                } else {
                    if ($j == 1) {
                        $grp_item = "4".$j."0_30";
                    } else {
                        $grp_item = "4".$j."0_20";
                    }
                }
                $arr_item_400[$j][$i] = $grp_item;
            }
        }
        $folder_id = $arr_folder_info["folder_id"];
        $use_incident_summary_list = $arr_folder_info["use_incident_summary_list"];
        $use_incident_summary_arr = split(",", $use_incident_summary_list);

        $incident_str = "";
        foreach($use_incident_summary_arr as $use_incident_summary)
        {
            $incident_str = "a_120_80.input_item = '$use_incident_summary'";
            $index_400 = $use_incident_summary - 1;

            $code_str = "";
            //発生場面・内容
            if($arr_folder_info_400[$folder_id][$index_400] != "")
            {

                //0:発生場面、1:内容に対して
                for($i = 0; $i < 2; $i++)
                {
                    //制限がある場合
                    if($arr_folder_info_400[$folder_id][$index_400][$i] != "")
                    {
                        //各制限コードに対して
                        foreach($arr_folder_info_400[$folder_id][$index_400][$i] as $use_400_code)
                        {
                            if ($code_str != "") {
                                $code_str .= " or ";
                            }
                            //arr_item?
                            $code_str .= " a_{$arr_item_400[$index_400][$i]}.input_item = '$use_400_code' ";

                            if (!in_array($arr_item_400[$index_400][$i], $join_field)) {
                                $join_field[] = $arr_item_400[$index_400][$i];
                            }
                        }
                    }
                }

            }
            if ($code_str != "") {
                $incident_str .= " and ($code_str) ";
            }
            $arr_incident_str[] = "($incident_str)";
        }
        $wk_incident_str = join(" or ", $arr_incident_str);
        $arr_cond_item[] = "($wk_incident_str)";

        if (!in_array("120_80", $join_field)) {
            $join_field[] = "120_80";
        }
    }
    //概要･場面･内容
    if($arr_folder_info["use_2010_summary_list"] != "")
    {
        //item_400 [インシデントの概要 - 1][0場面、1内容]
        // 400_10,400_20
        // 410_10,410_30
        // 420_10,420_20...
        for ($i=0; $i<3; $i++) {
            for ($j=0; $j<8; $j++) {
                if ($i == 0) {
                    $grp_item = "4".$j."0_10";
                } else {
                    if ($j == 1) {
                        $grp_item = "4".$j."0_30";
                    } else {
                        $grp_item = "4".$j."0_20";
                    }
                }
                $arr_item_400[$j][$i] = $grp_item;
            }
        }
        $folder_id = $arr_folder_info["folder_id"];
        $use_2010_summary_list = $arr_folder_info["use_2010_summary_list"];
        $use_2010_summary_arr = split(",", $use_2010_summary_list);

        $incident_str = "";
        foreach($use_2010_summary_arr as $use_2010_summary)
        {
            $incident_str = "a_900_10.input_item = '$use_2010_summary'";
            $index_1000 = $use_2010_summary - 1;

            $code_str = "";
            $detail_code_str = "";
            //発生場面・内容
            if($arr_folder_info_1000[$folder_id][$index_1000] != "")
            {

                //0:種類、1:場面、2:内容に対して
                for($i = 0; $i < 3; $i++)
                {
                    //制限がある場合
                    if($arr_folder_info_1000[$folder_id][$index_1000][$i] != "")
                    {
                        //各制限コードに対して
                        foreach($arr_folder_info_1000[$folder_id][$index_1000][$i] as $use_1000_code)
                        {
                            if ($code_str != "") {
                                $code_str .= " or ";
                            }
                            //arr_item?
                            //$code_str .= " a_{$arr_item_400[$index_400][$i]}.input_item = '$use_1000_code' ";
                            if($i == 0) {
                                $code_str .= " a_910_10.input_item = '$use_1000_code' ";
                                $join_field[] = "910_10";

                                foreach($arr_folder_info_1000_detail[$folder_id][$use_1000_code][$i] as $use_1000_detail_code) {
                                    if($detail_code_str != "") $detail_code_str .= " or ";

                                    $detail_code_str .= " a_920_10.input_item = '$use_1000_detail_code' ";
                                    $join_field[] = "920_10";
                                }
                            } else if($i == 1) {
                                $code_str .= " a_940_10.input_item = '$use_1000_code' ";
                                $join_field[] = "940_10";

                                foreach($arr_folder_info_1000_detail[$folder_id][$use_1000_code][$i] as $use_1000_detail_code) {
                                    if($detail_code_str != "") $detail_code_str .= " or ";

                                    $detail_code_str .= " a_950_10.input_item = '$use_1000_detail_code' ";
                                    $join_field[] = "950_10";
                                }
                            } else {
                                $code_str .= " a_970_10.input_item = '$use_1000_code' ";
                                $join_field[] = "970_10";

                                foreach($arr_folder_info_1000_detail[$folder_id][$use_1000_code][$i] as $use_1000_detail_code) {
                                    if($detail_code_str != "") $detail_code_str .= " or ";

                                    $detail_code_str .= " a_980_10.input_item = '$use_1000_detail_code' ";
                                    $join_field[] = "980_10";
                                }
                            }
                        }
                    }
                }
            }
            if ($code_str != "") {
                $incident_str .= " and ($code_str) ";
            }
            if($detail_code_str != "") $incident_str .= " and ($detail_code_str) ";
            $arr_incident_str[] = "($incident_str)";
        }
        $wk_incident_str = join(" or ", $arr_incident_str);
        $arr_cond_item[] = "($wk_incident_str)";

        if (!in_array("900_10", $join_field)) {
            $join_field[] = "900_10";
        }
    }
    $and_or = ($arr_folder_info["and_or_div"] == "0") ? " and " : " or ";
    $cond_tmp = "";
    //分類
    if (count($arr_cond_item) > 0) {
        $cond_tmp = " (".join($and_or, $arr_cond_item).") ";
    }
    //手動追加分
    if ($folder_id != "" && $folder_id != "delete") {
        if ($cond_tmp != "") {
            $cond_tmp .= " or ";
        }
        $cond_tmp .= " exists (select f.report_id from inci_report_classification f where f.report_id = a.report_id and f.folder_id = $folder_id and f.add_del_div = 0) ";
    }

    $cond_folder = " and ($cond_tmp)";

    //手動削除分
    if ($folder_id != "" && $folder_id != "delete") {
        $cond_folder .= "and (not exists (select f.report_id from inci_report_classification f where f.report_id = a.report_id and f.folder_id = $folder_id and f.add_del_div = 1)) ";
    }

    return array($cond_folder, $join_field);
}

//検索条件を取得
function get_search_cond($con,$fname,$search_info) {

    $where = "";
    $join_field = array();

    $search_emp_class = $search_info["search_emp_class"];
    $search_emp_attribute = $search_info["search_emp_attribute"];
    $search_emp_dept = $search_info["search_emp_dept"];
    $search_emp_room = $search_info["search_emp_room"];
    $search_emp_job = $search_info["search_emp_job"];
    $search_emp_name = $search_info["search_emp_name"];
    $search_patient_id = $search_info["search_patient_id"];
    $search_patient_name = $search_info["search_patient_name"];
    $search_patient_year_from = $search_info["search_patient_year_from"];
    $search_patient_year_to = $search_info["search_patient_year_to"];
    $search_toggle_div = $search_info["search_toggle_div"];
    $search_report_no = $search_info["search_report_no"];
    $search_evaluation_date = $search_info["search_evaluation_date"];
    $search_torey_date_in_search_area_start = $search_info["search_torey_date_in_search_area_start"];
    $search_torey_date_in_search_area_end = $search_info["search_torey_date_in_search_area_end"];
    $search_incident_date_start = $search_info["search_incident_date_start"];
    $search_incident_date_end = $search_info["search_incident_date_end"];

    $search_title = $search_info["search_title"];
    $search_incident = $search_info["search_incident"];
    $title_and_or_div = $search_info["title_and_or_div"];
    $incident_and_or_div = $search_info["incident_and_or_div"];

    $search_emp_all = $search_info["search_emp_all"];

    //報告部署
    /*if($search_emp_class != "")
    {
        $where .= " and a.registrant_class = $search_emp_class ";
    }
    if($search_emp_attribute != "")
    {
        $where .= " and a.registrant_attribute = $search_emp_attribute ";
    }
    if($search_emp_dept != "")
    {
        $where .= " and a.registrant_dept = $search_emp_dept ";
    }
    if($search_emp_room != "")
    {
        $where .= " and a.registrant_room = $search_emp_room ";
    }*/
    //報告部署
    foreach($search_emp_class as $key => $value) {
        if($key == 0 && $search_emp_class[0] != "") {
            $where .= " and ( ";
        } else if($search_emp_class[$key] != "") {
            $where .= " or ";
        }
        if($search_emp_class[$key] != "")
        {
            $where .= " ( a.registrant_class = $search_emp_class[$key] ";
        }
        if($search_emp_attribute[$key] != "")
        {
            $where .= " and a.registrant_attribute = $search_emp_attribute[$key] ";
        }
        if($search_emp_dept[$key] != "")
        {
            $where .= " and a.registrant_dept = $search_emp_dept[$key] ";
        }
        if($search_emp_room[$key] != "")
        {
            $where .= " and a.registrant_room = $search_emp_room[$key] ";
        }
        if($search_emp_class[0] != "") {
            $where .= " ) ";
        }
    }
    if($search_emp_class[0] != "") {
        $where .= " ) ";
    }

    if($search_emp_job != "")
    {
        $where .= " and e.emp_job = $search_emp_job ";
    }
    if($search_emp_name != "")
    {
        $search_emp_name2 = mb_ereg_replace(" ", "", pg_escape_string($search_emp_name));
        $search_emp_name2 = mb_ereg_replace("　", "", $search_emp_name2);

        $where .= " and ";
        $where .= " ( ";
        $where .= "   ( ";
        $where .= "     a.anonymous_report_flg ";
        $where .= "     and ";
        $where .= "     '".get_anonymous_name($con,$fname)."' like '%$search_emp_name2%' ";
        $where .= "   ) ";
        $where .= "   or ";
        $where .= "   ( ";
        $where .= "     not a.anonymous_report_flg ";
        $where .= "     and  ";
        $where .= "       ( ";
        $where .= "            e.emp_lt_nm like '%$search_emp_name2%' ";
        $where .= "         or e.emp_ft_nm like '%$search_emp_name2%' ";
        $where .= "         or e.emp_kn_lt_nm like '%$search_emp_name2%' ";
        $where .= "         or e.emp_kn_ft_nm like '%$search_emp_name2%' ";
        $where .= "         or (e.emp_lt_nm || e.emp_ft_nm) like '%$search_emp_name2%' ";
        $where .= "         or (e.emp_kn_lt_nm || e.emp_kn_ft_nm) like '%$search_emp_name2%' ";
        $where .= "       ) ";
        $where .= "   ) ";
        $where .= " ) ";
    }

    if($search_patient_id != "")
    {
        $search_patient_id2 = pg_escape_string($search_patient_id);
        $where .= " and a_210_10.input_item = '$search_patient_id2' ";
        $join_field[] = "210_10";
    }

    if($search_patient_name != "")
    {
        $search_patient_name2 =  pg_escape_string($search_patient_name);
        $where .= " and a_210_20.input_item like '%$search_patient_name2%' ";
        $join_field[] = "210_20";
    }

    if($search_patient_year_from == "900")
    {
        $where .= " and a_210_40.input_item = '$search_patient_year_from' ";
        $join_field[] = "210_40";
    }
    else
    {
        if($search_patient_year_from != "" && $search_patient_year_to != "")
        {
            $where .= " and a_210_40.input_item >= '$search_patient_year_from' ";
            $where .= " and a_210_40.input_item <= '$search_patient_year_to' ";
            $join_field[] = "210_40";
        }
        else if($search_patient_year_from != "" && $search_patient_year_to == "")
        {
            $where .= " and a_210_40.input_item >= '$search_patient_year_from' ";
            $where .= " and a_210_40.input_item < '900' ";
            $join_field[] = "210_40";
        }
        else if($search_patient_year_from == "" && $search_patient_year_to != "")
        {
            $where .= " and a_210_40.input_item <= '$search_patient_year_to' ";
            $join_field[] = "210_40";
        }
    }

    if($search_report_no != "")
    {
        $search_report_no2 = pg_escape_string($search_report_no);
        $where .= " and a.report_no like '{$search_report_no2}%' ";
    }

    //評価予定日
    if($search_evaluation_date != "")
    {
        if($search_evaluation_date == "today")
        {
            $date_min = date("Y/m/d");
            $date_max = $date_min;
        }
        elseif($search_evaluation_date == "tomorrow")
        {
            $date_min = date("Y/m/d", strtotime("+1 day"));
            $date_max = $date_min;
        }
        elseif($search_evaluation_date == "1week")
        {
            $date_min = date("Y/m/d");
            $date_max = date("Y/m/d", strtotime("+7 day"));
        }
        elseif($search_evaluation_date == "2week")
        {
            $date_min = date("Y/m/d");
            $date_max = date("Y/m/d", strtotime("+14 day"));
        }
        $where .= " and (a_620_33.input_item >= '$date_min' and a_620_33.input_item <= '$date_max')";
        $join_field[] = "620_33";
    }

    if($search_torey_date_in_search_area_start != "")
    {
        $where .= " and a.registration_date >= '$search_torey_date_in_search_area_start' ";
    }
    if($search_torey_date_in_search_area_end != "")
    {
        $where .= " and a.registration_date <= '$search_torey_date_in_search_area_end' ";
    }


    if($search_incident_date_start != "")
    {
        $where .= " and a_100_5.input_item >= '$search_incident_date_start'";
        $join_field[] = "100_5";
    }
    if($search_incident_date_end != "")
    {
        $where .= " and a_100_5.input_item <= '$search_incident_date_end'";
        if (!in_array("100_5", $join_field)) {
            $join_field[] = "100_5";
        }
    }

    if($search_title != "") {
        $title_keyword = mb_ereg_replace("　"," ",$search_title); //全角スペース除去
        $title_keyword = trim($title_keyword); //前後スペース除去
        $title_keyword_list = mb_split(" ", $title_keyword); //スペース区切り

        $wk_str = "";
        //マルチワードについては常にOR結合
        for ($i=0; $i<count($title_keyword_list); $i++) {
            if ($i > 0) {
                if($title_and_or_div == 1 || $title_and_or_div == "") {
                    $wk_str .= " and ";
                } else {
                    $wk_str .= " or ";
                }
            }
            $wk_str .= " report_title like '%{$title_keyword_list[$i]}%' ";
        }
        $where .= " and ($wk_str) ";
    }

    if($search_incident != "") {
        $title_keyword = mb_ereg_replace("　"," ",$search_incident); //全角スペース除去
        $title_keyword = trim($title_keyword); //前後スペース除去
        $title_keyword_list = mb_split(" ", $title_keyword); //スペース区切り

        $wk_str = "";
        //マルチワードについては常にOR結合
        for ($i=0; $i<count($title_keyword_list); $i++) {
            if ($i > 0) {
                if($incident_and_or_div == 1 || $incident_and_or_div == "") {
                    $wk_str .= " and ";
                } else {
                    $wk_str .= " or ";
                }
            }
            $wk_str .= " a_520_30.input_item like '%{$title_keyword_list[$i]}%' ";
        }
        $where .= " and ($wk_str) ";
        $join_field[] = "520_30";
    }

    return array($where, $join_field);
}