<?
//パラメータ
//$session
//  セッションID
//$caller
//  呼び出し元情報　※
//$year
//  初期表示する年。省略時は今年。
//$month
//  初期表示する月。省略時は今月。
//$future
//  1の場合に未来用カレンダーとなる。それ以外は過去用カレンダー
//
//
//※この画面は以下の画面より呼び出されます。呼び出し元の情報が$callerに格納されます。
//・レポート画面内の発生日時     … $caller=""　※$coller省略時はレポート画面内発生日時とみなされます。
//・各トレイ画面内の日付範囲指定 … $caller="torey_search_st"/"torey_search_ed"
//・各トレイ画面検索領域内の日付範囲指定　･･･
//・各トレイ画面発生日　･･･
//等。
//省略した場合はレポート画面内(発生日時)からの呼び出しとみなします。

require("about_session.php");
require("about_authority.php");
require("about_postgres.php");
require("holiday.php");
require_once("hiyari_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// インシデント権限のチェック
$checkauth = check_authority($session, 47, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 日付の設定
if ($year == "") {$year = date("Y");}
if ($month == "") {$month = date("m");}

//未来用変換(２ヶ月シフト)
if($future == 1)
{
	$month += 2;
	if($month>12)
	{
		$month -= 12;
		$year++;
	}
	if($month<10)
	{
		$month = "0".$month;
	}
}





// データベースに接続
$con = connect2db($fname);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>CoMedix <?echo $INCIDENT_TITLE ?> | カレンダー</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
<?

//レポート画面内(発生日時)で呼び出された場合
if($caller == "")
{
	?>
	function setDateInfo(ymd, month, wd, wd_div) {
		if (opener.document.form_list.elements['progress_date']) {
			opener.document.form_list.elements['progress_date'].value = ymd;
		}
		
		self.close();
	}
	<?
}

//レポート画面(発生日時)以外で呼び出された場合
else
{
	?>
	function call_back(ymd) {
		
		//呼び出し元へ通知
		if(window.opener && !window.opener.closed && window.opener.call_back_hiyari_calender)	{
			window.opener.call_back_hiyari_calender('<?=$caller?>',ymd);
		}
		
		//自画面を閉じる。
		self.close();
	}
	<?
}
?>
</script>
<style type="text/css">
form {margin:0;}
/*table.list {border-collapse:collapse;}*/
table.list td {border:solid #5279a5 1px;}
</style>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<center>
<? show_hiyari_header_for_sub_window("カレンダー"); ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
<tr>
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td>

<? show_navigation($caller, $year, $month, $session); ?>
<? show_calendars($caller,$year, $month); ?>

</td>
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>

</center>
</body>
<? pg_close($con); ?>
</html>
<?
function show_navigation($caller, $year, $month, $session) {
	list($last_year, $last_month) = get_year_month($year, $month, -3);
	list($next_year, $next_month) = get_year_month($year, $month, 3);

	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\n");
	echo("<tr height=\"22\" bgcolor=\"#F5FFE5\">\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"hiyari_calendar_analysis.php?session=$session&amp;caller=$caller&amp;year=$last_year&amp;month=$last_month\">&lt;&lt; 前3か月</a></font></td>");
	echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"hiyari_calendar_analysis.php?session=$session&amp;caller=$caller&amp;year=$next_year&amp;month=$next_month\">翌3か月 &gt;&gt;</a></font></td>");
	echo("</tr>\n");
	echo("</table>\n");
}

function show_calendars($caller,$year, $month) {
	list($last_last_year, $last_last_month) = get_year_month($year, $month, -2);
	list($last_year, $last_month) = get_year_month($year, $month, -1);

	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\n");
	echo("<tr bgcolor=\"#F5FFE5\">\n");
	echo("<td align=\"center\" valign=\"top\">");
	show_calendar($caller,$last_last_year, $last_last_month);
	echo("</td>\n");
	echo("<td align=\"center\" valign=\"top\">");
	show_calendar($caller,$last_year, $last_month);
	echo("</td>\n");
	echo("<td align=\"center\" valign=\"top\">");
	show_calendar($caller,$year, $month);
	echo("</td>\n");
	echo("</tr>\n");
	echo("</table>\n");
}

function show_calendar($caller,$year, $month) {
	$arr_month = get_arr_month($year, $month);

	echo("<table width=\"100%\" border=\"0\" cellspacing=\"2\" cellpadding=\"1\" class=\"list\">\n");
	echo("<tr>\n");
	echo("<td colspan=\"7\" style=\"border-style:none;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$year/$month</font></td>\n");
	echo("</tr>\n");
	echo("<tr>\n");
	echo("<td bgcolor=\"#bdd1e7\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">月</font></td>\n");
	echo("<td bgcolor=\"#bdd1e7\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">火</font></td>\n");
	echo("<td bgcolor=\"#bdd1e7\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">水</font></td>\n");
	echo("<td bgcolor=\"#bdd1e7\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">木</font></td>\n");
	echo("<td bgcolor=\"#bdd1e7\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">金</font></td>\n");
	echo("<td bgcolor=\"#defafa\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">土</font></td>\n");
	echo("<td bgcolor=\"#fadede\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">日</font></td>\n");
	echo("</tr>\n");
	//DBコネクション
	$con = connect2db($fname);
	if($con == "0"){
		showErrorPage();
		exit;
	}
	foreach ($arr_month as $arr_week) {
		echo("<tr>\n");
		foreach ($arr_week as $tmp_index => $tmp_ymd) {
			
			//レポート画面内(発生日時)で呼び出された場合
			if($caller == "")
			{
				$tmp_y = substr($tmp_ymd, 0, 4);
				$tmp_m = substr($tmp_ymd, 4, 2);
				if ($tmp_m != $month) {
					echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">・</font></td>\n");
				} else {
					$tmp_m = intval($tmp_m);
					$tmp_d = intval(substr($tmp_ymd, 6, 2));
					$tmp_wd = $tmp_index + 1;
					//$tmp_wd_div = ($tmp_wd == 6 || $tmp_wd == 7 || get_holiday_name($tmp_ymd) != "") ? "2" : "1";
					// カレンダマスタから休みを取得
					$sql = "SELECT type FROM calendar WHERE date = '{$tmp_ymd}'";
					$cond = "";
					$sel = select_from_table($con, $sql, $cond, $fname);
					if ($sel == 0) {
						pg_close($con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}
					$result = pg_fetch_array($sel);
					if($result['type'] > 0 && $result['type'] <= 3) { 
						$tmp_wd_div = 1;
					} else if($result['type'] >= 4) { 
						$tmp_wd_div = 2;
					} else { 
						$tmp_wd_div = "";
					}
					//$tmp_wd_div = ($result['type'] >= 4) ? 2 : 1;
					
					$tmp_ymd = $tmp_y."/";
					if($tmp_m<10)
					{
						$tmp_ymd .= "0";
					}
					$tmp_ymd .= $tmp_m."/";
					if($tmp_d<10)
					{
						$tmp_ymd .= "0";
					}
					$tmp_ymd .= $tmp_d;
					
					echo("<td id=\"id$tmp_ymd\" align=\"center\" style=\"cursor:pointer;\" onclick=\"setDateInfo('$tmp_ymd','$tmp_m', '$tmp_wd', '$tmp_wd_div');\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"javascript:void(0);\">$tmp_d</a></font></td>\n");
				}
			}
			
			//レポート画面(発生日時)以外で呼び出された場合
			else
			{
				$tmp_y = substr($tmp_ymd, 0, 4);
				$tmp_m = substr($tmp_ymd, 4, 2);
				if ($tmp_m != $month) {
					echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">・</font></td>\n");
				} else {
					$tmp_m = intval($tmp_m);
					$tmp_d = intval(substr($tmp_ymd, 6, 2));
					
					$tmp_ymd = $tmp_y."/";
					if($tmp_m<10)
					{
						$tmp_ymd .= "0";
					}
					$tmp_ymd .= $tmp_m."/";
					if($tmp_d<10)
					{
						$tmp_ymd .= "0";
					}
					$tmp_ymd .= $tmp_d;
					
					echo("<td id=\"id$tmp_ymd\" align=\"center\" style=\"cursor:pointer;\" onclick=\"call_back('$tmp_ymd');\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"javascript:void(0);\">$tmp_d</a></font></td>\n");
				}
			}

		}
		echo("</tr>");
	}
	echo("</table>\n");
	//==================================================
	//DB切断
	//==================================================
	pg_close($con);
}

function get_year_month($year, $month, $diff) {
	$result_year = $year;
	$result_month = $month + $diff;
	if ($result_month > 12) {
		$result_year++;
		$result_month -= 12;
	} else if ($result_month < 1) {
		$result_year--;
		$result_month += 12;
	}
	$result_month = sprintf("%02d", $result_month);
	return array($result_year, $result_month);
}

function get_arr_month($year, $month) {

	// 当月1日〜末日の配列を作成
	$arr_date = array();
	for ($i = 1; $i <= 31; $i++) {
		$day = substr("0" . $i, -2);
		if (checkdate($month, $day, $year)) {
			array_push($arr_date, "$year$month$day");
		} else {
			break;
		}
	}

	// 当月1日が月曜でなければ空白セル分の日付を配列の先頭に追加
	$first_day = mktime(0, 0, 0, $month, "01", $year);
	$wd = date("w", $first_day);
	switch ($wd) {
	case 0:  // 日
		$empty = 6;
		break;
	case 1:  // 月
		$empty = 0;
		break;
	case 2:  // 火
		$empty = 1;
		break;
	case 3:  // 水
		$empty = 2;
		break;
	case 4:  // 木
		$empty = 3;
		break;
	case 5:  // 金
		$empty = 4;
		break;
	case 6:  // 土
		$empty = 5;
		break;
	}
	for ($i = 1; $i <= $empty; $i++) {
		array_unshift($arr_date, date("Ymd", strtotime("-$i days", $first_day)));
	}

	// 当月末日が日曜でなければ空白セル分の日付を配列の末尾に追加
	$day = substr($arr_date[count($arr_date) - 1], -2);
	$end_day = mktime(0, 0, 0, $month, $day, $year);
	$wd = date("w", $end_day);
	switch ($wd) {
	case 0:  // 日
		$empty = 0;
		break;
	case 1:  // 月
		$empty = 6;
		break;
	case 2:  // 火
		$empty = 5;
		break;
	case 3:  // 水
		$empty = 4;
		break;
	case 4:  // 木
		$empty = 3;
		break;
	case 5:  // 金
		$empty = 2;
		break;
	case 6:  // 土
		$empty = 1;
		break;
	}
	for ($i = 1; $i <= $empty; $i++) {
		array_push($arr_date, date("Ymd", strtotime("$i days", $end_day)));
	}

	// 週単位に分割した配列を返す
	return array_chunk($arr_date, 7);
}


?>
