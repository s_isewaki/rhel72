<?php
require_once 'about_comedix.php';
require_once 'about_session.php';
require_once 'about_certify.php';
require_once 'about_authority.php';
require_once 'conf/conf.inf';

require_once 'kintai/common/mdb_wrapper.php';
require_once 'kintai/common/mdb.php';
require_once 'kintai/common/exception.php';
require_once 'kintai/common/ajax_response.php';

try {
	//====================================
	//ロガー設定
	//====================================
	require_once 'common_log_class.ini';
	$log = new common_log_class(basename(__FILE__));

	//====================================
	//セッションのチェック
	//====================================
	$session = qualify_session($_POST["session"], $fname);
	if($session == "0"){
		$log->error("セッションチェックに失敗");
		$res = new AjaxResponse("error", "セッションチェックに失敗しました");
		echo cmx_json_encode($res);
		exit;
	}

	//====================================
	//権限チェック
	//====================================
	$checkauth = check_authority($session, $_POST["auth"],$fname);
	if($checkauth == "0"){
		$log->error("権限チェックエラー:auth=".$_POST["auth"]);
		throw new FatalException();
	}

	MDBWrapper::init();
	$mdb = new MDB($log);
	$mdb->beginTransaction();
	require_once $_POST["controller"]; // controllerをload
	$method(); // function実行
	$mdb->endTransaction();
} catch (FatalException $fex) {
	$res = new AjaxResponse("error", "処理中に致命的な例外が発生しました");
	echo cmx_json_encode($res);
}
?>