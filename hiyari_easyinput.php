<?php
require_once('Cmx.php');
require_once('Cmx/View/Smarty.php');
require_once('Cmx/Model/SystemConfig.php');
require_once('about_comedix.php');
require_once("get_values.php");
require_once("hiyari_mail.ini");
require_once("hiyari_common.ini");
require_once("hiyari_report_class.php");
require_once("hiyari_wf_utils.php");
require_once("holiday.php");
require_once("hiyari_item_time_setting_models.php");
require_once("hiyari_item_timelag_setting_models.php");
require_once("hiyari_report_classification.php");
require_once("hiyari_report_auth_models.php");
//====================================================================================================
//前処理
//====================================================================================================

//テンプレート読み込み
require_once("smarty_setting.ini");

//ページ名
$fname = $PHP_SELF;

$callerpage = $_POST['callerpage'];
if(!$callerpage)
    $callerpage = $_GET['callerpage'];

$frompage = $_GET['frompage'];


//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
    showLoginPage();
    exit;
}

// 権限チェック
$hiyari = check_authority($session, 47, $fname);
if ($hiyari == "0") {
    showLoginPage();
    exit;
}

//DBコネクション
$con = connect2db($fname);
if($con == "0"){
    showErrorPage();
    exit;
}

// 医療機関コードの取得
$sql = "select prf_org_cd from profile";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showErrorPage(window);</script>");
    exit;
}
if (pg_num_rows($sel) == 1) {
    $org_cd = pg_fetch_result($sel, 0, "prf_org_cd");
}

//==================================================
//レポートクラス
//==================================================

$rep_obj = new hiyari_report_class($con, $fname);

//==================================================
//オプション
//==================================================
$sysConf = new Cmx_SystemConfig();
//報告日ロックフラグ
$is_report_date_lock = $sysConf->get('fantol.report.date.edit.flg') == '1';
// 事例の詳細(時系列)のテンプレート入力フラグ
$is_grpcode_4000_template = $sysConf->get('fantol.grpcode.4000.template.flg') == '1';
//デザイン（1=旧デザイン、2=新デザイン）
$design_mode = $sysConf->get('fantol.design.mode');

// 報告書へのファイル添付フラグ
$fantol_file_attach = $sysConf->get('fantol.report.file.attach');
$fantol_file_attach = $fantol_file_attach === null ? '0' : $fantol_file_attach;

//評価予定期日の表示の有無
$predetermined_eval = $sysConf->get('fantol.predetermined.eval');




//==================================================
//SM未設定の場合は終了
//==================================================
if(! is_sm_emp_exist($session,$fname,$con))
{
    //画面終了。
    ?>
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
    <html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
    <script language='javascript'>
    alert("ＳＭが設定されていません。");
    window.close();
    </script>
    </head>
    <body>
    </body>
    </html>
    <?
    pg_close($con);
    exit;
}


//==================================================
// セッション開始
//==================================================
@session_name("hyr_sid");
@session_start();



//====================================================================================================
//ポストバック時の処理
//====================================================================================================

if($is_postback != "")
{

    //==================================================
    // 確認画面へ移動の場合
    //==================================================
    if ( $_POST['next'] != '' )  {
        // 入力データをセッションに保存し、ページ遷移する
        $_SESSION['easyinput'] = $_POST;

        //確認画面から戻る場合のための処理(これを行わないと確認画面へ移動することとなる。)
        $_SESSION['easyinput']['is_postback'] = "";
        $_SESSION['easyinput']['next'] = "";
        $callerpage = $_POST['callerpage'];
        //確認画面へ移動
        redirectPage("hiyari_easyinput_confirm.php?session={$session}&callerpage={$callerpage}&mail_id={$mail_id}" . SID);

        //DB切断
        pg_close($con);

        // 実行終了
        exit;
    }

    //==================================================
    //更新処理の場合(カテゴリ指定限定)
    //==================================================
    elseif ( $_POST['report_update'] != '' )
    {
        // 入力データをセッションに保存し、ページ遷移する
        $_SESSION['easyinput'] = $_POST;

        //確認画面から戻る場合のための処理(これを行わないと確認画面へ移動することとなる。)
        $_SESSION['easyinput']['is_postback'] = "";
        $_SESSION['easyinput']['next'] = "";

        //確認画面へ更新実行状態で移動
        $url = "hiyari_easyinput_confirm.php?session={$session}&is_postback=true&update=true&" . SID;
        if ($gamen_mode == "analysis"){
            $url .= "&analysis_draft=true";
        }
        redirectPage($url);

        //DB切断
        pg_close($con);

        // 実行終了
        exit;
    }

    //==================================================
    //下書き保存処理の場合
    //==================================================
    elseif ( $_POST['shitagaki'] != '' )
    {
        //入力内容をセッションに保存
        $_SESSION['easyinput'] = $_POST;

        // 2012/01/24 invalid
        // 報告書更新ロック
        // action_lock(LOCK_HIYARI_REPORT_UPDATE);

        //トランザクション開始
        pg_query($con,"begin transaction");

        //ユーザーID取得
        $emp_id = get_emp_id($con, $session, $fname);

        //下書きモード
        $is_shitagaki = true;

        $rep_obj = new hiyari_report_class($con, $fname);

        if($report_id == "")
        {
            // レポートを新規登録(セッションデータ→DB)
            // $is_anonymous = false;
            $is_anonymous = get_anonymous_mail_use_flg($con,$fname);
            $report_id = $rep_obj->set_report($session,$emp_id, $_SESSION['easyinput'], $is_anonymous, $is_shitagaki);
        }
        else
        {
            //レポートデータの更新
            $report_title = $_SESSION['easyinput']['report_title'];
            $regist_date_y = $_SESSION['easyinput']['regist_date_y'];
            $regist_date_m = $_SESSION['easyinput']['regist_date_m'];
            $regist_date_d = $_SESSION['easyinput']['regist_date_d'];
            $first_send_time = $_SESSION['easyinput']['first_send_time'];
            $job_id = $_SESSION['easyinput']['job_id'];
            $eis_id = $rep_obj->get_eis_id_from_eis_no($eis_no);
            $rep_obj->set_report_title($report_id,$report_title);
            $rep_obj->set_registration_date($report_id,$regist_date_y, $regist_date_m, $regist_date_d);
            $rep_obj->set_report_data_field($report_id, "job_id", "$job_id");
            $rep_obj->set_report_data_field($report_id, "eis_id", "$eis_id");
            //グループコード配列$use_grp_codeを取得
            $grp_flags = $rep_obj->get_report_flags_by_eis_no($eis_no);
            $use_grp_code = $grp_flags['use_grp_code'];
            $rep_obj->set_report_content($session,$report_id, $eis_id, $_SESSION['easyinput'], $use_grp_code);
            // 匿名情報の更新
            $is_anonymous = get_anonymous_mail_use_flg($con,$fname);
            if($is_anonymous === true) {
                $is_anonymous_bool = 't';
            } else if($is_anonymous === false) {
                $is_anonymous_bool = 'f';
            }
/* 2009/08/14 岩本修正ここから */
//          $rep_obj->set_report_anonymous($report_id,$is_anonymous_bool);
            $rep_obj->set_report_data_field($report_id, "anonymous_report_flg", $is_anonymous_bool);
/* 2009/08/14 岩本修正ここまで */

            //所属情報の更新
            $registrant_class = $_SESSION['easyinput']['registrant_class'];
            $registrant_attribute = $_SESSION['easyinput']['registrant_attribute'];
            $registrant_dept = $_SESSION['easyinput']['registrant_dept'];
            $registrant_room = $_SESSION['easyinput']['registrant_room'];
            if($registrant_class == ""){$registrant_class = null;}
            if($registrant_attribute == ""){$registrant_attribute = null;}
            if($registrant_dept == ""){$registrant_dept = null;}
            if($registrant_room == ""){$registrant_room = null;}
            $rep_obj->set_report_data_field($report_id, "registrant_class", $registrant_class);
            $rep_obj->set_report_data_field($report_id, "registrant_attribute", $registrant_attribute);
            $rep_obj->set_report_data_field($report_id, "registrant_dept", $registrant_dept);
            $rep_obj->set_report_data_field($report_id, "registrant_room", $registrant_room);

            $doctor_emp_id = $_SESSION['easyinput']['doctor_emp_id'];
            $rep_obj->set_report_data_field($report_id, "doctor_emp_id", $doctor_emp_id);


            //20130630
            //==================================================
            //ファイル添付
            //==================================================
            $rep_obj->move_hiyari_file($report_id, $emp_id,$_POST['gamen_mode']);
            $rep_obj->set_inci_file_report($report_id);
        }

        //トランザクション終了
        pg_query($con, "commit");

        // 2012/01/24 invalid
        // 報告書更新ロック解除
        // action_unlock(LOCK_HIYARI_REPORT_UPDATE);

        // セッションの破棄
        session_unset();
        session_destroy();

        //更新リロード
        $msg = '報告書を下書き保存しました。\n報告は完了していません。\n再開後速やかに報告を完了してください。';
        ?>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
        <html>
        <head>
        <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
        <script language='javascript'>
        alert("<?=$msg?>");
        if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}
        <?
        if($_POST['shitagaki'] != '')
        {
        ?>
        window.close();
        <?
        }
        ?>

//      location.href="hiyari_easyinput.php?session=<?=$session?>&gamen_mode=shitagaki&report_id=<?=$report_id?>";
        </script>
        </head>
        <body>
        </body>
        </html>
        <?

        //DB切断
        pg_close($con);

        // 実行終了
        exit;
    }

    //==================================================
    //様式変更処理の場合
    //==================================================
    elseif ( $_POST['sel_eis_change'] != '' )
    {
        //ここでは何も処理をせず、通常の表示(セカンドアクセス)へ。
    }
    //==================================================
    //報告部署変更処理の場合
    //==================================================
    elseif ( $_POST['registrant_post_change'] != '' )
    {
        //ここでは何も処理をせず、通常の表示(セカンドアクセス)へ。
    }
}

//====================================================================================================
//通常表示時の処理 (初期表示／様式変更／確認画面から戻る)
//====================================================================================================

//==================================================
//職種指定のプルダウン一覧情報
//==================================================

//様式定義されている職種IDの配列を取得
$job_ids = $rep_obj->get_eis_job_list();

//新規モードの場合は様式が割り当ててないとＮＧ
//(その他のモードの場合は様式が後で追加される。)
if($gamen_mode == "new")
{
    if ( count($job_ids) == 0)
    {
        $err_msg = "簡易報告書様式が未定義のため、作成できません。";
        $smarty = new Cmx_View();
        $smarty->assign("session", $session);
        $smarty->assign("err_msg", $err_msg);
        $smarty->display("hiyari_easyinput_error.tpl");
        pg_close($con);
        exit;
    }
}

// 職種名一覧を取得
$job_names = get_job_nm($con, $job_ids, $fname);


//==================================================
// 確認画面より戻った場合のセッション情報変換
//==================================================
//※ポストバックした時と同等の状態にする。

if($_SESSION['easyinput']['return_from_confirm'] == true)
{
    $_SESSION['easyinput']['return_from_confirm'] = "";
    $post_data = $_SESSION['easyinput'];
    $is_first_access = false;
}
elseif($is_postback)
{
    $post_data = $_POST;
    $is_first_access = false;
}
else
{
    $is_first_access = true;
}


//==================================================
//報告ファイルの表示設定を取得
//==================================================
$disp_setting = get_disp_setting($con,$fname);
$non_eis_report_default_disp_mode = $disp_setting["non_eis_report_default_disp_mode"];
$must_item_disp_mode = $disp_setting["must_item_disp_mode"];


//==================================================
//画面入力情報の取得
//==================================================
//※ポストバックデータをここで隠蔽する。


//セカンドアクセスの場合
if ( !$is_first_access ) {
    //==============================
    //画面パラメータ系
    //==============================
    $gamen_mode          = $post_data['gamen_mode'];
    $report_id           = $post_data['report_id'];
    $mail_id             = $post_data['mail_id'];
    $mail_id_mode        = $post_data['mail_id_mode'];
    $cate                = $post_data['cate'];

    //==============================
    //レポートデータ
    //==============================
    if($gamen_mode == "new")
    {
        $report_data = "";
    }
    elseif($gamen_mode == "shitagaki" || $gamen_mode == "update" || $gamen_mode == "analysis" || $gamen_mode == "cate_update")
    {
        $report_data = $rep_obj->get_report($report_id);

        //添付ファイル情報を取得
        $ret_file = $rep_obj->get_inci_file($report_id);
    }
    else{}


    //==============================
    //職種・様式
    //==============================
    $sel_job    = $post_data['sel_job'];
    $sel_eis_no = $post_data['sel_eis_no'];

    //==============================
    //表題・報告日・入力内容
    //==============================
    $report_title = $post_data['report_title'];
    $report_title = h($report_title);
    $registrant_id=$report_data['registrant_id'];

    $regist_date_y = $post_data['regist_date_y'];
    $regist_date_m = $post_data['regist_date_m'];
    $regist_date_d = $post_data['regist_date_d'];
    $first_send_time = $post_data['first_send_time'];


    $doctor_emp_id = $post_data['doctor_emp_id'];

    //入力したデータを取得
    $vals = $rep_obj->conv_form_item_codes($post_data);
    if($report_data){
        //様式の切り替え時にデータを取得
        $vals += $report_data['content']['input_items'];
    }
    $vals = $rep_obj->get_easy_item_names_value($vals);

    //様式変更処理の場合、値がnullなら様式の初期値を設定
    if ( $gamen_mode == "new" && $post_data['sel_eis_change'] != '' ){
        $eis_id = $rep_obj->get_eis_id_from_eis_no($sel_eis_no);
        $default = $rep_obj->get_default_value($eis_id);

        $array_skip = array();
        $target_flag = false;
        $target_list = array(90, 900, 910, 920, 940, 950, 970, 980);
        foreach ($target_list as $target_no){
            if ($vals[$target_no][10]) {
                $target_flag = true;
                break;
            }
        }
        if ($vals[520][30] || $target_flag) {
            $tmp_default = $rep_obj->get_default_value($post_data['eis_title_id']);
            //内容
            if($vals[520][30][0] == $tmp_default[520][30]){
                $vals[520][30] = "";
            }else{
                $array_skip["520_30"] = true;
            }
            //概要
            foreach ($target_list as $item_no){
                if($vals[$item_no][10][0] == $tmp_default[$item_no][10]){
                    $vals[$item_no][10] = "";
                }else{
                    $tmp_key = $item_no .  "_10";
                    $array_skip[$tmp_key] = true;
                }
            }
        }

        foreach($default as $key1 => $value1) {
            foreach($value1 as $key2 => $value2) {
                $tmp_key = $key1 . "_" . $key2;
                if (!$array_skip[$tmp_key]){
                    $vals[$key1][$key2] = explode("\t", $value2);
                }
            }
        }
    }


    //==============================
    //所属情報
    //==============================
    $registrant_class = $post_data["registrant_class"];
    $registrant_attribute = $post_data["registrant_attribute"];
    $registrant_dept = $post_data["registrant_dept"];
    $registrant_room = $post_data["registrant_room"];

    //20130630
    //==================================================
    //ファイル添付
    //==================================================
    $tmpFileArray = array();
    $arr_file = array();
    foreach ($_COOKIE['hiyariFileEnc'] as $enc => $name) {
        $tmpFileArray = array($enc => $name);
        $arr_file += $tmpFileArray;
        $tmpFileArray = array();
    }
}
//ファーストアクセスの場合
else {
    //==============================
    //画面パラメータ系
    //==============================
    //※CoMedixの仕組みにより自動設定。

    //==============================
    //レポートデータ
    //==============================
    //※セカンドアクセスと同等の処理

    //20130630
    //==============================
    //添付ファイルの一時保存エリア、Cookieを初期化する
    //==============================
    $emp_id = get_emp_id( $con, $session, $fname );
    //$emp_dir = $rep_obj->make_tmp_dir($emp_id);
    $emp_dir = $rep_obj->get_tmp_dir("Tmp",$emp_id, $report_id);

    //$emp_dir = "/tmp/hiyariTmp_".$emp_id;

    if ($handle = opendir($emp_dir)){
        while(false !== ($item = readdir($handle))){
            if($item != "." && $item != ".."){unlink($emp_dir."/".$item);}
        }
        closedir($handle);
        rmdir($emp_dir);
    }

    //Cookieの初期化
    $arrFileCheck = array_unique($_COOKIE['hiyariFileEnc']);
    foreach ($_COOKIE['hiyariFileEnc'] as $enc => $name) {
        setcookie('hiyariFileEnc['.$enc.']', 'noFile', time() - 1800);
    }
    setcookie('hiyariFileEnc', 'noFile', time() - 1800);
    //==============================
    //添付ファイルの一時保存エリア、Cookieを初期化する
    //==============================


    if ( $gamen_mode == "new") {
        $report_data = "";
    }

    elseif($gamen_mode == "shitagaki" || $gamen_mode == "update" || $gamen_mode == "analysis" || $gamen_mode == "cate_update") {
        $report_data = $rep_obj->get_report($report_id);
    }

    else{}

    //==============================
    //職種・様式
    //==============================

    //新規モードの場合：ユーザーの職種のデフォルト様式とする。
    if($gamen_mode == "new")
    {
        $emp_id = get_emp_id( $con, $session, $fname );
        $sel_job = get_own_job( $con, $emp_id, $fname );
        $sel_eis_no = $rep_obj->get_default_eis_no_from_job($sel_job);

        //ユーザーの職種に様式が割り当てられていない場合、先頭職種の様式を表示する。
        if($sel_eis_no == "")
        {
            $sel_job = $job_ids[0];
            $sel_eis_no = $rep_obj->get_default_eis_no_from_job($sel_job);
        }
    }

    //下書きモードの場合：下書きレポートの様式とする。
    elseif($gamen_mode == "shitagaki")
    {
        //※様式IDをレポートより取得するのは基本的に以下の下書きケースのみである。
        $sel_job = $report_data['job_id'];
        $sel_eis_no = $rep_obj->get_eis_no_from_eis_id($report_data['eis_id']);
    }

    //更新(メール情報なし)の場合：全項目様式とする。
    else if ($gamen_mode == "update" && ($callerpage == "hiyari_rpt_report_classification.php" || $callerpage == "hiyari_rm_stats_reports.php")) {
        //特殊様式のため、様式情報はなし
        if($non_eis_report_default_disp_mode == 'all'){
            $sel_job    = "";
            $sel_eis_no = "";
        }else{
            require_once("hiyari_mail.ini");
            $sel_job = get_job_id_from_mail_id($con,$fname,$mail_id);
            $eis_id = get_eis_id_from_mail_id($con,$fname,$mail_id);
            $sel_eis_no = $rep_obj->get_eis_no_from_eis_id($eis_id);
        }
    }

    //更新(メール情報あり)の場合：メールと紐付く様式とする。
    elseif($gamen_mode == "update")
    {
        require_once("hiyari_mail.ini");
        $sel_job = get_job_id_from_mail_id($con,$fname,$mail_id);
        $eis_id = get_eis_id_from_mail_id($con,$fname,$mail_id);
        $sel_eis_no = $rep_obj->get_eis_no_from_eis_id($eis_id);
    }


    //更新(カテゴリ指定)の場合：カテゴリ様式とする。
    elseif($gamen_mode == "cate_update" && $mail_id == "")
    {
        //特殊様式のため、様式情報はなし。
        $sel_job    = "";
        $sel_eis_no = "";
    }

    //分析・再発防止の場合：分析専用様式とする。
    elseif ($gamen_mode == "analysis") {
        //特殊様式のため、様式情報はなし。←変更：様式を一番上のものにする。20100202
        $emp_id = get_emp_id($con, $session, $fname);
        if($design_mode == 1){
            $mail_id = $rep_obj->get_newest_mail_id($report_id);
        }
        // #1416: 評価をすると様式が変更される。
        // job_id, eis_noをGETで受け取るように修正
        if (empty($_GET['job_id'])) {
            $sel_job = get_own_job($con, $emp_id, $fname);
            $sel_eis_no = $rep_obj->get_default_eis_no_from_job($sel_job);
        }
        else {
            $sel_job = $_GET['job_id'];
            $sel_eis_no = $_GET['eis_no'];
        }

        //ユーザーの職種に様式が割り当てられていない場合、先頭職種の様式を表示する。
        if ($sel_eis_no == "") {
            $sel_job = $job_ids[0];
            $sel_eis_no = $rep_obj->get_default_eis_no_from_job($sel_job);
        }
    }

    //その他：ありえない。
    else{}


    //==============================
    //表題・報告日・入力内容
    //==============================

    //新規モードの場合：デフォルト状態とする。
    if($gamen_mode == "new")
    {

        $report_title = "";

        $regist_date_y = date('Y');
        $regist_date_m = date('m');
        $regist_date_d = date('d');

        $doctor_emp_id = '';

        $vals = array();
        $vals = $rep_obj->get_easy_item_names_value($vals);

        //デフォルト値
        $eis_id = $rep_obj->get_eis_id_from_eis_no($sel_eis_no);
        $default = $rep_obj->get_default_value($eis_id);
        foreach($default as $key1 => $value1) {
            foreach($value1 as $key2 => $value2) {
                $vals[$key1][$key2] = explode("\t", $value2);
            }
        }
    }

    //下書きモード／更新モード／分析モードの場合：参照元のレポートデータを使用する。
    elseif($gamen_mode == "shitagaki" || $gamen_mode == "update" || $gamen_mode == "analysis" || $gamen_mode == "cate_update")
    {
        $report_title = $report_data['report_title'];
        $report_title = h($report_title);
        $registrant_id=$report_data['registrant_id'];

        //※下書きの場合は報告日を本日日付に変更する。
        if($gamen_mode == "shitagaki")
        {
            $regist_date_y = date('Y');
            $regist_date_m = date('m');
            $regist_date_d = date('d');
        }
        else
        {
            $regist_date = explode("/", $report_data['registration_date']);
            $regist_date_y = $regist_date[0];
            $regist_date_m = $regist_date[1];
            $regist_date_d = $regist_date[2];

            $first_send_time = sprintf( "%s/%s/%s %s:%s",
                substr($report_data['first_send_time'], 0, 4),
                substr($report_data['first_send_time'], 4, 2),
                substr($report_data['first_send_time'], 6, 2),
                substr($report_data['first_send_time'], 8, 2),
                substr($report_data['first_send_time'], 10, 2)
            );
        }


        $doctor_emp_id = $report_data['doctor_emp_id'];

        $vals = $report_data['content']['input_items'];
        $vals = $rep_obj->get_easy_item_names_value($vals);
    }

    //その他：ありえない。
    else{}

    //==============================
    //所属情報
    //==============================

    //新規モードの場合：ログインユーザーの所属情報を使用する。
    if($gamen_mode == "new")
    {
        $emp_info = get_empmst($con,$emp_id,$fname);
        $registrant_class = $emp_info[2];
        $registrant_attribute = $emp_info[3];
        $registrant_dept = $emp_info[4];
        $registrant_room = $emp_info[33];
    }

    //下書きモード／更新モード／分析モードの場合：参照元のレポートデータから取得する。
    elseif($gamen_mode == "shitagaki" || $gamen_mode == "update" || $gamen_mode == "analysis" || $gamen_mode == "cate_update")
    {
        $registrant_class = $report_data['registrant_class'];
        $registrant_attribute = $report_data['registrant_attribute'];
        $registrant_dept = $report_data['registrant_dept'];
        $registrant_room = $report_data['registrant_room'];

        //20130630
        //==================================================
        //ファイル添付
        //==================================================
        //添付ファイル情報を取得
        $ret_file = $rep_obj->get_inci_file($report_id);



    }

    //その他：ありえない。
    else{}

}


//==================================================
//下書きモードの場合、自作でない場合は参照のみ
//==================================================

$readonly_flg = false;
if($gamen_mode == "shitagaki")
{
    $emp_id = get_emp_id($con,$session,$fname);
    $readonly_flg = ($emp_id != $report_data['registrant_id']);
}


//==================================================
//削除済みの場合、分析不可能、更新は参照のみ。
//==================================================

if($gamen_mode == "analysis")
{
    if($rep_obj->is_deleted_report($report_id))
    {
        //画面終了。
        ?>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
        <html>
        <head>
        <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
        <script language='javascript'>
        alert("指定された報告は既に削除されています。");
        window.close();
        </script>
        </head>
        <body>
        </body>
        </html>
        <?
        pg_close($con);
        exit;
    }
}
elseif($gamen_mode == "update" || $gamen_mode == "cate_update")
{
    if($rep_obj->is_deleted_report($report_id))
    {
        $readonly_flg = true;
    }
}


//==================================================
//SM以外編集ロックの場合、SM以外は参照のみ
//==================================================
if($gamen_mode == "update" || $gamen_mode == "analysis" || $gamen_mode == "cate_update")
{
    $is_sm      = is_sm_emp($session,$fname);
    $is_sm_lock = $rep_obj->get_sm_lock($report_id);
    if($is_sm_lock && !$is_sm)
    {
        $readonly_flg = true;
    }
    if($non_update_flg)
    {
        $readonly_flg = true;
    }
}

//==================================================
//報告ファイルからの更新モードでの呼び出しの場合、一般ユーザーは参照のみ、担当者は報告ファイルの更新権限設定に依存
//==================================================
if(($callerpage == "hiyari_rpt_report_classification.php" || $callerpage == "hiyari_rpt_report_db.php"|| $callerpage == "hiyari_pastreport_list.php") && $gamen_mode == "update")
{
    //一般ユーザーは参照のみ
    if(!is_inci_auth_emp($session,$fname,$con))
    {
        $readonly_flg = true;
    }
    //担当者の場合
    else
    {
        //報告部署に対する更新権限を判定
        $is_report_db_update_able_report_post = is_report_db_update_able_report_post($session, $fname,$con,$registrant_class,$registrant_attribute,$registrant_dept,$registrant_room);
        if(!$is_report_db_update_able_report_post)
        {
            $readonly_flg = true;
        }
    }
}

//==================================================
//レポート使用グループ、必須項目グループ
//==================================================


//分析・再発防止の場合：分析様式非必須。
if($gamen_mode == "analysis") {
    //使用項目
    if($non_update_flg) $use_grp_code = $rep_obj->get_analysis_grps();
    else $use_grp_code = $rep_obj->get_analysis_grps($session);
    //SMの場合はSM専用項目を追加
    if(is_sm_emp($session,$fname)) $use_grp_code[] = 690;
    //必須項目
    $item_must = array();

    ////////////////////////////////////////////////////////////////// 20100202
    //使用項目
    $grp_flags = $rep_obj->get_report_flags_by_eis_no($sel_eis_no);

    // 様式があれば文言を修正する
    if($id == $grp_flags['eis_id']) {
        $sql  = "select * from inci_item_title ";
        $cond = "where eis_id = " . pg_escape_string($id);
        $result = select_from_table( $con, $sql, $cond, $fname );
        $tmp_eis_item_title = pg_fetch_all($result);

        // 文言変更データを修正する
        foreach($tmp_eis_item_title as $val) {
            $eis_item_title[$val['eis_column']] = $val['eis_title'];
        }
    }

   // 文字数制限
    if($id = $grp_flags['eis_id']) {
        $sql  = "select * from inci_item_number_of_character ";
        $cond = "where eis_id = " . pg_escape_string($id);
        $result = select_from_table( $con, $sql, $cond, $fname );
        $tmp_item_number_of_character = pg_fetch_all($result);

        // 文言変更データを修正する
        foreach($tmp_item_number_of_character as $val) {
            $eis_item_number_of_character[$val['group_code']] = $val['number_of_characters'];
        }
    }


    //////////////////////////////////////////////////////////////////
    // 表題モード取得
    if($id) {
        $sql    = "select subject_mode from inci_subject_mode ";
        $cond   = "where eis_id = " . pg_escape_string($id);
        $result = select_from_table($con, $sql, $cond, $fname);
        $subject_mode = pg_fetch_result($result, 0, "subject_mode");
    }

}


//カテゴリ指定の場合：カテゴリ項目非必須。
elseif($gamen_mode == "cate_update") {
    //使用項目
    $use_grp_code = $rep_obj->get_original_eis_grps($cate);
    //SM専用項目はなしとする。
    //必須項目
    $item_must = array();
}


//様式がある場合：様式に従う。
elseif($sel_eis_no != "") {
    //使用項目
    $grp_flags = $rep_obj->get_report_flags_by_eis_no($sel_eis_no);
    $use_grp_code = $grp_flags['use_grp_code'];
    //SM専用項目はなしとする。

    //必須項目
    $item_must = $rep_obj->get_item_must_from_eis_no($sel_eis_no);

    // 様式があれば文言を修正する
    if($id = $grp_flags['eis_id']) {
        $sql  = "select * from inci_item_title ";
        $cond = "where eis_id = " . pg_escape_string($id);
        $result = select_from_table( $con, $sql, $cond, $fname );
        $tmp_eis_item_title = pg_fetch_all($result);

        // 文言変更データを修正する
        if (is_array($tmp_eis_item_title)){
            foreach($tmp_eis_item_title as $val) {
                $eis_item_title[$val['eis_column']] = $val['eis_title'];
            }
        }
    }


    // 文字数制限
    if($id = $grp_flags['eis_id']) {
        $sql  = "select * from inci_item_number_of_character ";
        $cond = "where eis_id = " . pg_escape_string($id);
        $result = select_from_table( $con, $sql, $cond, $fname );
        $tmp_item_number_of_character = pg_fetch_all($result);

        // 文字数制限
        foreach($tmp_item_number_of_character as $val) {
            $eis_item_number_of_character[$val['group_code']] = $val['number_of_characters'];
        }
    }


    // 表題モード取得
    if($id) {
        $sql    = "select subject_mode from inci_subject_mode ";
        $cond   = "where eis_id = " . pg_escape_string($id);
        $result = select_from_table($con, $sql, $cond, $fname);
        $subject_mode = @pg_fetch_result($result, 0, "subject_mode");
    }

    // ユーザー定義カテゴリ名変更
    $user_cate_name = $grp_flags['user_cate_name'];
}

//更新(様式なし)の場合：全項目非必須。
elseif($gamen_mode == "update") {
    //使用項目
    $use_grp_code = $rep_obj->get_full_grps();
    //SMの場合はSM専用項目を追加
    if(is_sm_emp($session,$fname)) $use_grp_code[] = 690;
    //必須項目
    $item_must = array();
}
if (!is_array($use_grp_code)) $use_grp_code = array();

// 様式なしの場合、マスタからユーザー定義カテゴリ名取得
if($sel_eis_no == "") {
    $sql    = "select cate_name from inci_easyinput_category_mst";
    $cond   = "where cate_code = 10";
    $result = select_from_table($con, $sql, $cond, $fname);
    $user_cate_name = @pg_fetch_result($result, 0, "cate_name");
}

// 表題リストデータ
if($subject_mode == 1 || $subject_mode == 2) {
    $sql    = "select * from inci_report_materials ";
    $cond   = "where grp_code = 125 and easy_item_code = 85 and easy_flag = '1' order by easy_num";
    $result = select_from_table($con, $sql, $cond, $fname);
    $classification = pg_fetch_all($result);
}

//==================================================
//職種-様式リンク情報
//==================================================
//全項目の場合
if($gamen_mode == "update"  && ($callerpage == "hiyari_rpt_report_classification.php" || $callerpage == "hiyari_rm_stats_reports.php" ))

{

    //==================================================
    //全項目ビューモード
    //==================================================

    //職種-様式リンク情報
    if($eis_job_link_info == "")
    {
        $eis_job_link_info = $rep_obj->get_eis_job_link_info_from_report_rireki($report_id);
    }

    // 職種ID一覧を作成
    $job_ids = null;
    $wk_joib_id2 = null;
    foreach($eis_job_link_info as $eis_job_link)
    {
        $wk_joib_id = $eis_job_link['job_id'];
        if($wk_joib_id != $wk_joib_id2)
        {
            $job_ids[] = $wk_joib_id;
            $wk_joib_id2 = $wk_joib_id;
        }
    }

    if(count($eis_job_link_info) > 0)
    {
        $eis_job_link_info[0]["default_flg"] = "t";
    }

    // 職種名一覧を取得
    $job_names = get_job_nm($con, $job_ids, $fname);

    //全項目を追加
    $a = null;
    $a['job_id'] = "";
    $a['eis_no'] = "";
    $a['eis_name'] = "全項目";
    $a['default_flg'] = "f";

    array_unshift($eis_job_link_info, $a);
    array_unshift($job_ids, "");
    array_unshift($job_names, "全項目");

}
//様式指定の場合
else if($gamen_mode != "analysis" && $gamen_mode != "cate_update") {

    $eis_job_link_info = $rep_obj->get_eis_job_link_info();

    //==================================================
    //表示する様式が割付解除されている場合、様式リストに追加
    //==================================================

    //新規モード以外で、様式情報がある場合
    if($gamen_mode != "new" && $sel_eis_no != "" )
    {
        //様式割付解除され、今回の職種に様式が１つもなくなっている場合
        if(!in_array($sel_job,$job_ids))
        {
            //職種リストに今回の職種を追加
            $job_ids[] = $sel_job;
        }
        //様式割付解除され、今回の様式が割り付いていない場合
        $is_canceled_eis_job_link = true;
        foreach($eis_job_link_info as $tmp_eis_job_link_info)
        {
            if($tmp_eis_job_link_info["job_id"] == $sel_job && $tmp_eis_job_link_info["eis_no"] == $sel_eis_no)
            {
                $is_canceled_eis_job_link = false;
                break;
            }
        }
        if($is_canceled_eis_job_link)
        {
            //様式リストに今回の様式を追加
            $tmp_array = "";
            $tmp_array["job_id"] = $sel_job;
            $tmp_array["eis_no"] = $sel_eis_no;
            $tmp_array["eis_name"] = $rep_obj->get_eis_name_from_eis_no($sel_eis_no);
            $tmp_array["default_flg"] = "f";
            $eis_job_link_info[] = $tmp_array;
        }
    }
}
else
{
    //以下の場合は様式選択不可能。
    //・カテゴリ指定
    //・分析
}

$eis_select_able = true;
if($gamen_mode == "analysis" || $gamen_mode == "cate_update")
{
    //様式選択有無
    $eis_select_able = false;
}


//==================================================
//報告書スタイル
//==================================================
if ($design_mode == 1){
    //旧デザインのとき
    $style_code = 1;
    if($gamen_mode != "analysis"){
        if ($sel_eis_no){
            $style_code = $rep_obj->get_eis_style_from_eis_no($sel_eis_no);
        }
    }
}
else{
    //新デザインのとき
    $style_code = 2;
}

//==================================================
//その他の画面情報
//==================================================
//全項目情報
$grps = $rep_obj->get_all_easy_items();

foreach($grps[605]['easy_item_list'] as $key => $value) {
    if($key > 1000)  {
        $grps_605[] = $key;
    }
}


/*
// SQLが遅い場合は、以下６個のクエリを復活させてください。

$sql =
    " SELECT item_id, sub_item_id, sub_item_name, item_code FROM inci_sub_item".
    " WHERE disp_flg = 't' AND available = 't' ORDER BY sub_item_order";
$db_result = select_from_table($con, $sql, "", $fname );
$tmplist = pg_fetch_all($db_result);
$inci_sub_item_list = array();
foreach($tmplist as $idx => $row){
    $inci_sub_item_list[trim($row["item_code"])][trim($row["item_id"])][] =
        array("sub_item_id"=>trim($row["sub_item_id"]), "sub_item_name"=>trim($row["sub_item_name"]));
}

$sql =
    " SELECT item_id, sub_item_id, sub_item_name, item_code FROM inci_sub_item_2010".
    " WHERE disp_flg = 't' AND available = 't' ORDER BY sub_item_order";
$db_result = select_from_table($con, $sql, "", $fname);
$tmplist = pg_fetch_all($db_result);
$inci_sub_item_2010list = array();
foreach($tmplist as $idx => $row){
    $inci_sub_item_2010list[trim($row["item_code"])][trim($row["item_id"])][] =
        array("sub_item_id"=>trim($row["sub_item_id"]), "sub_item_name"=>trim($row["sub_item_name"]));
}

$sql =
    " SELECT item_id, item_name, super_item_id, item_code FROM inci_item".
    " WHERE disp_flg = 't' AND available = 't' ORDER BY item_order";
$db_result = select_from_table($con, $sql, "", $fname);
$tmplist = pg_fetch_all($db_result);
$inci_item_list = array();
foreach($tmplist as $idx => $row){
    $inci_item_list[trim($row["item_code"])][trim($row["super_item_id"])][] =
        array("item_id"=>trim($row["item_id"]), "item_name"=>trim($row["item_name"]));
}

$sql =
    " SELECT item_id, item_name, super_item_id, item_code FROM inci_item_2010".
    " WHERE disp_flg = 't' AND available = 't' ORDER BY item_order";
$db_result = select_from_table($con, $sql, "", $fname);
$tmplist = pg_fetch_all($db_result);
$inci_item_2010list = array();
foreach($tmplist as $idx => $row){
    $inci_item_2010list[trim($row["item_code"])][trim($row["super_item_id"])][] =
        array("item_id"=>trim($row["item_id"]), "item_name"=>trim($row["item_name"]));
}

$sql =
    " SELECT item_id, item_name, super_item_id, item_code FROM inci_ic_item".
    " WHERE disp_flg = 't' AND available = 't' ORDER BY item_order";
$db_result = select_from_table($con, $sql, "", $fname);
$tmplist = pg_fetch_all($db_result);
$inci_ic_item_list = array();
foreach($tmplist as $idx => $row){
    $inci_ic_item_list[trim($row["item_code"])][trim($row["super_item_id"])][] =
        array("item_id"=>trim($row["item_id"]), "item_name"=>trim($row["item_name"]));
}


$sql =
    " SELECT item_id, sub_item_id, super_item_id, sub_item_name, item_code FROM inci_ic_sub_item".
    " WHERE disp_flg = 't' AND available = 't' ORDER BY sub_item_order";
$db_result = select_from_table($con, $sql, "", $fname );
$tmplist = pg_fetch_all($db_result);
$inci_ic_sub_item_list = array();
foreach($tmplist as $idx => $row){
    $inci_ic_sub_item_list[trim($row["item_code"])][trim($row["item_id"])][] =
        array("sub_item_id"=>trim($row["sub_item_id"]), "sub_item_name"=>trim($row["sub_item_name"]));
}
*/
//値をマージ
if(array_key_exists(700, $grps)) {
    $method = $rep_obj->get_all_super_item();
    unset($grps[700]['easy_item_list'][10]['easy_list']);

    foreach($method as $key => $value) {
        $grps[700]['easy_item_list'][10]['easy_list'][$key]['easy_code'] = $value['super_item_id'];
        $grps[700]['easy_item_list'][10]['easy_list'][$key]['easy_name'] = $value['super_item_name'];
    }

    // 発生した場面のデフォルト値を削除
    unset($grps[710]['easy_item_list'][10]['easy_list']);
    unset($grps[720]['easy_item_list'][10]['easy_list']);

    foreach($method as $value) {
        $param = $rep_obj->get_scene($value['super_item_id']);
//      $param = $inci_item_list["scene"][$value['super_item_id']];// SQLが遅い場合は、直上文をコメントして、この行を復活させてください。
        if ($param) $scene[$value['super_item_id']] = $param;
        if (is_array($param)){
            foreach($param as $value2) {
                $scene_item[$value2['item_id']] = $rep_obj->get_scene_item($value2['item_id']);
//              $scene_item[$value2['item_id']] = $inci_sub_item_list["scene"][$value2['item_id']]; // SQLが遅い場合は、直上文をコメントして、この行を復活させてください。
            }
        }
    }

    // 発生した場面のデフォルト値を削除
    unset($grps[740]['easy_item_list'][10]['easy_list']);
    unset($grps[750]['easy_item_list'][10]['easy_list']);

    foreach($method as $value) {
        $param = $rep_obj->get_kind($value['super_item_id']);
//      $param = $inci_item_list["kind"][$value['super_item_id']];// SQLが遅い場合は、直上文をコメントして、この行を復活させてください。
        if ($param) $kind[$value['super_item_id']] = $param;
        if (is_array($param)){
            foreach($param as $value2) {
                $kind_item[$value2['item_id']] = $rep_obj->get_kind_item($value2['item_id']);
//              $kind_item[$value2['item_id']] = $inci_sub_item_list["kind"][$value2['item_id']];// SQLが遅い場合は、直上文をコメントして、この行を復活させてください。
            }
        }
    }

    // 発生した場面のデフォルト値を削除
    unset($grps[770]['easy_item_list'][10]['easy_list']);
    unset($grps[780]['easy_item_list'][10]['easy_list']);

    foreach($method as $value) {
        $param = $rep_obj->get_content($value['super_item_id']);
//      $param = $inci_item_list["content"][$value['super_item_id']];// SQLが遅い場合は、直上文をコメントして、この行を復活させてください。
        if ($param) $content[$value['super_item_id']] = $param;
        if (is_array($param)){
            foreach($param as $value2) {
                $content_item[$value2['item_id']] = $rep_obj->get_content_item($value2['item_id']);
//              $content_item[$value2['item_id']] = $inci_sub_item_list["content"][$value2['item_id']];// SQLが遅い場合は、直上文をコメントして、この行を復活させてください。
            }
        }
    }
}

//値をマージ 2010年版
if(array_key_exists(900, $grps)) {
    $method = $rep_obj->get_all_super_item_2010();
    unset($grps[900]['easy_item_list'][10]['easy_list']);

    foreach($method as $key => $value) {
        $grps[900]['easy_item_list'][10]['easy_list'][$key]['easy_code'] = $value['super_item_id'];
        $grps[900]['easy_item_list'][10]['easy_list'][$key]['easy_name'] = $value['super_item_name'];
        $grps[900]['easy_item_list'][10]['easy_list'][$key]['export_code'] = $value['export_item_id'];
        $grps[900]['easy_item_list'][10]['easy_list'][$key]['other_code'] = $value['other_code'];
    }

    // 発生した場面のデフォルト値を削除
    unset($grps[910]['easy_item_list'][10]['easy_list']);
    unset($grps[920]['easy_item_list'][10]['easy_list']);

    foreach($method as $value) {
        $param = $rep_obj->get_scene_2010($value['super_item_id']);
//      $param = $inci_item_2010list["scene"][$value['super_item_id']];// SQLが遅い場合は、直上文をコメントして、この行を復活させてください。
        if ($param) $scene_2010[$value['super_item_id']] = $param;
        if (is_array($param)){
            foreach($param as $value2) {
                $param2 = $rep_obj->get_scene_item_2010($value2['item_id']);
                if ($param2) $scene_item_2010[$value2['item_id']] = $param2;
//              $scene_item_2010[$value2['item_id']] = $inci_sub_item_2010list["scene"][$value2['item_id']];// SQLが遅い場合は、直上文をコメントして、この行を復活させてください。
            }
        }
    }

    // 発生した場面のデフォルト値を削除
    unset($grps[940]['easy_item_list'][10]['easy_list']);
    unset($grps[950]['easy_item_list'][10]['easy_list']);

    foreach($method as $value) {
        $param = $rep_obj->get_kind_2010($value['super_item_id']);
//      $param = $inci_item_2010list["kind"][$value['super_item_id']];
        if ($param) $kind_2010[$value['super_item_id']] = $param;
        if (is_array($param)){
            foreach($param as $value2) {
                $param2 = $rep_obj->get_kind_item_2010($value2['item_id']);
                if ($param2) $kind_item_2010[$value2['item_id']] = $param2;
                //$kind_item_2010[$value2['item_id']] = $inci_sub_item_2010list["kind"][$value2['item_id']];// SQLが遅い場合は、直上文をコメントして、この行を復活させてください。
            }
        }
    }

    // 発生した場面のデフォルト値を削除
    unset($grps[970]['easy_item_list'][10]['easy_list']);
    unset($grps[980]['easy_item_list'][10]['easy_list']);

    foreach($method as $value) {
        $param = $rep_obj->get_content_2010($value['super_item_id']);
//      $param = $inci_item_2010list["content"][$value['super_item_id']];// SQLが遅い場合は、直上文をコメントして、この行を復活させてください。
        if ($param) $content_2010[$value['super_item_id']] = $param;
        if (is_array($param)){
            foreach($param as $value2) {
                $param2 = $rep_obj->get_content_item_2010($value2['item_id']);
                if ($param2) $content_item_2010[$value2['item_id']] = $param2;
//              content_item_2010[$value2['item_id']] = $inci_sub_item_2010list["content"][$value2['item_id']];// SQLが遅い場合は、直上文をコメントして、この行を復活させてください。
            }
        }
    }

}

/*************************************************
 * ここに患者の影響・対応を記述する
 * 2010/03/10
 *************************************************/
if(array_key_exists(1400, $grps)) {
    $method = $rep_obj->get_all_ic_super_item();
    unset($grps[1400]['easy_item_list'][10]['easy_list']);

    foreach($method as $key => $value) {
        $grps[1400]['easy_item_list'][10]['easy_list'][$key]['easy_code'] = $value['super_item_id'];
        $grps[1400]['easy_item_list'][10]['easy_list'][$key]['easy_name'] = $value['super_item_name'];
    }

    // 発生した場面のデフォルト値を削除
    unset($grps[1400]['easy_item_list'][30]['easy_list']);

    foreach($method as $value) {
        $param = $rep_obj->get_ic_items($value['super_item_id'], 'influence');
//      $param = $inci_ic_item_list["influence"][$value['super_item_id']];// SQLが遅い場合は、直上文をコメントして、この行を復活させてください。
        if ($param) $ic_influence[$value['super_item_id']] = $param;
    }

    // 発生した場面のデフォルト値を削除
    unset($grps[1400]['easy_item_list'][50]['easy_list']);

    foreach($method as $value) {
        $param = $rep_obj->get_ic_items($value['super_item_id'], 'mental');
//      $param = $inci_ic_item_list["mental"][$value['super_item_id']];// SQLが遅い場合は、直上文をコメントして、この行を復活させてください。
        if ($param) $ic_mental[$value['super_item_id']] = $param;
    }
}
if(array_key_exists(1410, $grps)) {
    $method = $rep_obj->get_all_ic_super_item();
    unset($grps[1410]['easy_item_list'][10]['easy_list']);

    foreach($method as $key => $value) {
        $grps[1410]['easy_item_list'][10]['easy_list'][$key]['easy_code'] = $value['super_item_id'];
        $grps[1410]['easy_item_list'][10]['easy_list'][$key]['easy_name'] = $value['super_item_name'];
    }

    // 発生した場面のデフォルト値を削除
    unset($grps[1410]['easy_item_list'][20]['easy_list']);

    foreach($method as $value) {
        $param = $rep_obj->get_ic_items($value['super_item_id'], 'correspondence');
//      $param = $inci_ic_item_list["correspondence"][$value['super_item_id']];// SQLが遅い場合は、直上文をコメントして、この行を復活させてください。
        if ($param) $ic_correspondence[$value['super_item_id']] = $param;
    }

    foreach($method as $value) {
        $params = $rep_obj->get_ic_items($value['super_item_id'], 'correspondence');
//      $params = $inci_ic_item_list["correspondence"][$value['super_item_id']];// SQLが遅い場合は、直上文をコメントして、この行を復活させてください。
        if (is_array($params)){
            foreach($params as $param) {
                $ic_parent_item[$param['item_id']] = $param['item_name'];
            }
        }
    }

    // 発生した場面のデフォルト値を削除
    unset($grps[1410]['easy_item_list'][30]['easy_list']);
    foreach($ic_correspondence as $value) {
        if (is_array($value)){
            foreach($value as $val) {
                $param = $rep_obj->get_ic_sub_items($val['item_id'], 'correspondence');
//              $param = $inci_ic_sub_item_list["correspondence"][$value['item_id']];// SQLが遅い場合は、直上文をコメントして、この行を復活させてください。
                if (is_array($param)){
                    foreach($param as $value2) {
                        $ic_sub_item[$value2['super_item_id']][$value2['item_id']] = $param;
                    }
                }
            }
        }
    }
}

//連動グループ
$rel_grps = $rep_obj->get_relate_item_code_group();


//レベル説明文
$used_level = "";
if($vals != "" and $vals[90] != "" and $vals[90][10] != "" and $vals[90][10][0] != "" )
{
    $used_level = $vals[90][10][0];
}
$level_infos = $rep_obj->get_inci_level_infos($used_level);

//患者影響レベル表示項目を取得　20140423 R.C
if (!empty($sel_eis_no)){
    $tmp_eis_id = $rep_obj->get_eis_id_from_eis_no($sel_eis_no);
    $level_use = $rep_obj->get_inci_easyinput_other_disp($tmp_eis_id, 90, 10);
}

//レポート入力支援関連
$pt_auth = check_authority($session, 15, $fname);
$emp_id = get_emp_id($con, $session, $fname);
$profile = get_profile($con, $emp_id, $fname);
$emp_class_nm = get_emp_profile_class_name($profile, get_inci_profile($fname, $con));
$experience = get_experience($profile);
$profile_rowspan = get_profile_rowspan($use_grp_code);

$discoverer_rowspan = 0;
if(in_array(3030, $use_grp_code)) {
    $discoverer_rowspan++;
}
if(in_array(3032, $use_grp_code)) {
    $discoverer_rowspan++;
}
if(in_array(3034, $use_grp_code)) {
    $discoverer_rowspan++;
}
if(in_array(3036, $use_grp_code)) {
    $discoverer_rowspan += 2;
}
if(in_array(3038, $use_grp_code)) {
    $discoverer_rowspan += 2;
}

// 今期のテーマを取得する
if($sel_eis_no) $theme = $rep_obj->get_this_term_theme($sel_eis_no);

//画面タイトル
if($gamen_mode == "new" || $gamen_mode == "shitagaki")
{
    $PAGE_TITLE = "報告書登録";
}
elseif($gamen_mode == "update" || $gamen_mode == "cate_update")
{
    $PAGE_TITLE = "出来事報告編集";
}
elseif($gamen_mode == "analysis")
{
    if ($design_mode == 1){
        $PAGE_TITLE = "分析・再発防止";
    }
    else{
        $PAGE_TITLE = "評価";
    }
}

/*************************************************
 * 発生場所・詳細
 *************************************************/
foreach($grps[115]['easy_item_list'] as $item_id => $item_value) {
    $list = array();
    foreach($item_value['easy_list'] as $key => $list_value) {
        $list[ $list_value['easy_code'] ] = $list_value['easy_name'];
    }

    if ($design_mode == 2){
        $extra = count($list) % 4;
        if ($extra > 0){
            for ($i=0; $i < 4-$extra; $i++){
                $list[] = 'is_extra';
            }
        }
    }

    $grps_115[$item_id] = $list;
}

// GRM(SM)フラグ
$is_sm_emp_flg =  is_sm_emp($session, $fname, $con);

// タイムラグ表示テキスト取得
$timelag_view = getTimelagView( $vals[105][75][0] );

//コントロールタイプを変更
if ($style_code == 2){
  $rep_obj->change_easy_item_type($grps);
}else{
  if ($design_mode == 1){
      $grps[1100]['easy_item_list'][50]['easy_item_type']='radio';  //仮に実施された場合の影響度
      $grps[1110]['easy_item_list'][10]['easy_item_type']='radio';  //特に報告を求める事例
  }
}

// ユーザー定義カテゴリ項目
$use_user_grp_code = array();
foreach ($use_grp_code as $grp_code){
    if ($grp_code >= 10000){
        $use_user_grp_code[] = $grp_code;
    }
}
if ($design_mode == 2){
    //$tabletypes = get_table_type($grps, $use_user_grp_code);
    $tabletypes = array();
    foreach($use_user_grp_code as $grp_code) {
        foreach($grps[$grp_code]['easy_item_list'] as $easy_item_code => $easy_item) {
            $max_len = 0;
            $tabletype = null;
            foreach($easy_item['easy_list'] as $easy) {
                if ($max_len < mb_strlen($easy['easy_name'])){
                    $max_len = mb_strlen($easy['easy_name']);
                }
            }
            if ($max_len < 10){
                $tabletype = 'tabletype03';
            }
            if ($tabletype != null){
                $tabletypes[$grp_code.'_'.$easy_item_code] = $tabletype;
            }
        }
    }
}

//==================================================
//テンプレートに値を設定
//==================================================
//※ポストバックデータは全てここでテンプレートに送られる。

$smarty = new Cmx_View();

$smarty->assign("emp_id", $emp_id);

// 医療機関コード
$smarty->assign("org_cd", $org_cd);

// 報告書スタイル
$smarty->assign("style_code", $style_code);

// GRM(SM)フラグ
$smarty->assign("is_sm_emp_flg", $is_sm_emp_flg);

//報告日ロックフラグ
$smarty->assign("is_report_date_lock", $is_report_date_lock);

//評価機能利用フラグ 20140421 R.C
if($gamen_mode == "new" || $report_id == "" || $emp_id == ""){
    $emp_auth = get_emp_priority_auth($session,$fname,$con);
    $is_evaluation_use = $sysConf->get('fantol.evaluation.use.' . strtolower($emp_auth) . '.flg') == 't';
}else{
    $arr_report_auth = $rep_obj->get_my_inci_auth_list_for_report_progress($report_id, $emp_id);
    foreach ($arr_report_auth as $auth_item){
        $emp_auth = $auth_item["auth"];
        $auth_flag = $sysConf->get('fantol.evaluation.use.' . strtolower($emp_auth) . '.flg') == 't';

        if ($auth_flag){
            $is_evaluation_use = $auth_flag;
            break;
        }
    }

    // レポート受付・確認日付・更新可能チェックフラグ 20141010
    if (!empty($report_id) && count($arr_report_auth) > 0) {
        $report_progress_updatable_flg = $rep_obj->is_report_progress_kakunin_updatable($report_id, $arr_report_auth);
    }else{
        $report_progress_updatable_flg = false;
    }
    $smarty->assign("report_progress_updatable_flg", $report_progress_updatable_flg);
}
$smarty->assign("is_evaluation_use", $is_evaluation_use);

// 分類ラベル
$label_use_flg = is_report_db_usable($session,$fname,$con);
$smarty->assign("label_use_flg", $label_use_flg);
if ($label_use_flg && $gamen_mode != "new" && $gamen_mode != "shitagaki"){
    $folder_info = get_all_classification_folder_info($con, $fname, $emp_id);
    $folders = array();
    foreach($folder_info as $info){
        $work['id'] = $info["folder_id"];
        $work['name'] = h($info["folder_name"]);
        $work['is_common'] = $info["common_flg"]=="t";
        $work['is_selected'] = is_report_in_folder($con, $fname, $report_id, $info["folder_id"]);
        $folders[] = $work;
    }
    $smarty->assign('folders', $folders);
}

// 事例の詳細(時系列)のテンプレート入力フラグ
$smarty->assign("is_grpcode_4000_template",$is_grpcode_4000_template);

// 発生場所詳細
$smarty->assign("grps_115", $grps_115);

// 発生要因詳細
$smarty->assign("grps_605", $grps_605);

// 発生場面と内容
$smarty->assign("scene", $scene);
$smarty->assign("scene_item", $scene_item);
$smarty->assign("kind", $kind);
$smarty->assign("kind_item", $kind_item);
$smarty->assign("content", $content);
$smarty->assign("content_item", $content_item);

// 概要2010年版
if ($design_mode == 1){
    $smarty->assign("kind_2010", $kind_2010);
    $smarty->assign("kind_item_2010", $kind_item_2010);
    $smarty->assign("scene_2010", $scene_2010);
    $smarty->assign("scene_item_2010", $scene_item_2010);
    $smarty->assign("content_2010", $content_2010);
    $smarty->assign("content_item_2010", $content_item_2010);
}
else{
    $gaiyo_selection = array();
    $gaiyo_selection[910]['list'] = $kind_2010;
    $gaiyo_selection[920]['list'] = $kind_item_2010;
    $gaiyo_selection[940]['list'] = $scene_2010;
    $gaiyo_selection[950]['list'] = $scene_item_2010;
    $gaiyo_selection[970]['list'] = $content_2010;
    $gaiyo_selection[980]['list'] = $content_item_2010;
    $smarty->assign("gaiyo_selection", $gaiyo_selection);
}

// 影響と対応
$smarty->assign("ic_influence", $ic_influence);
$smarty->assign("ic_mental", $ic_mental);
$smarty->assign("ic_correspondence", $ic_correspondence);
$smarty->assign("ic_sub_item", $ic_sub_item);
$smarty->assign("ic_parent_item", $ic_parent_item);

//DBアクセス情報
$smarty->assign("con", $con);
$smarty->assign("fname", $fname);

//セッション情報
$smarty->assign("session", $session);
$smarty->assign("hyr_sid", session_id());

//画面タイトル
$smarty->assign("INCIDENT_TITLE", $INCIDENT_TITLE);
$smarty->assign("PAGE_TITLE", $PAGE_TITLE);

//参照のみフラグ
$smarty->assign("readonly_flg", $readonly_flg);

//画面パラメータ
$smarty->assign("gamen_mode", $gamen_mode);
$smarty->assign("report_id", $report_id);
$smarty->assign("mail_id", $mail_id);
$smarty->assign("mail_id_mode", $mail_id_mode);
$smarty->assign("cate", $cate);

//職種・様式
$smarty->assign("job_ids", $job_ids);
$smarty->assign("job_names", $job_names);
$smarty->assign("eis_job_link_info", $eis_job_link_info);
$smarty->assign("sel_job", $sel_job);
$smarty->assign("sel_eis_no", $sel_eis_no);
$smarty->assign("eis_select_able", $eis_select_able);

$smarty->assign("eis_item_title", $eis_item_title);
$smarty->assign("eis_title_id", $grp_flags['eis_id']);


$smarty->assign("eis_item_number_of_character", $eis_item_number_of_character);

if (strpos($vals[105][75][0], "表示をする場合は") !== false) {
    $smarty->assign("is_no_timelag", true);
}

// 表題モード
$smarty->assign("subject_mode", $subject_mode);
if($subject_mode != 0 && $classification) {
    $smarty->assign("classification", $classification);
}

// ユーザー定義カテゴリ名
$smarty->assign("user_cate_name", $user_cate_name);

// *** 発見・発生日時のモード設定 ***

// 設定情報を取得
$time_setting_flg = getTimeSetting($con, $PHP_SELF);

// 発見・発生日時のモード設定を送信
$smarty->assign("time_setting_flg", $time_setting_flg);


// タイムラグの表示フラグ取得
$disp_taimelag_flg = getDispTimelagSetting($con, $fname);

$smarty->assign("disp_taimelag_flg", $disp_taimelag_flg);

// タイムラグの表示文字列
$smarty->assign("timelag_view", $timelag_view);

// プロフィールボタン設定
// ボタン表示設定を取得
$profile_button_flg = get_my_inci_profile_button_display($session, $con, $fname);
$smarty->assign("self_button_flg",       $profile_button_flg['self']);
$smarty->assign("other_user_button_flg", $profile_button_flg['other']);
$smarty->assign("unknown_button_flg",    $profile_button_flg['unknown']);



// 今期のテーマを送信
$smarty->assign("theme", $theme[0]);

//レポート情報(ヘッダー系)
$smarty->assign("report_title", $report_title);
$smarty->assign("regist_date_y", $regist_date_y);
$smarty->assign("regist_date_m", $regist_date_m);
$smarty->assign("regist_date_d", $regist_date_d);

$smarty->assign("registrant_id",$registrant_id);

//レポート情報(準ヘッダー系)
$smarty->assign("doctor_emp_id", $doctor_emp_id);

// 報告時間
if ( isset($first_send_time) ) {
    $smarty->assign("first_send_time", $first_send_time);
}


//レポート情報(項目系)
$smarty->assign("grps", $grps);
$smarty->assign("grp_flags", $use_grp_code);
$smarty->assign("user_grp_flags", $use_user_grp_code);
$smarty->assign("rel_grps", $rel_grps);
$smarty->assign("vals", $vals);
$smarty->assign("item_must", $item_must);
$smarty->assign("tabletypes", $tabletypes);

//所属情報
$smarty->assign("registrant_class", $registrant_class);
$smarty->assign("registrant_attribute", $registrant_attribute);
$smarty->assign("registrant_dept", $registrant_dept);
$smarty->assign("registrant_room", $registrant_room);
if ($style_code == 2){
    $arr_class_name = get_class_name_array($con, $fname);
    if($registrant_class != "") {
        $smarty->assign("class_name", get_class_nm($con,$registrant_class,$fname));
    }else{
        $smarty->assign("class_name", "不明");
    }
    if($registrant_attribute != "") {
        $smarty->assign("attribute_name", get_atrb_nm($con,$registrant_attribute,$fname));
    }
    if($registrant_dept != "") {
        $smarty->assign("dept_name", get_dept_nm($con,$registrant_dept,$fname));
    }
    if($registrant_room != "" && $arr_class_name["class_cnt"] == 4) {
        $smarty->assign("room_name", get_room_nm($con,$registrant_room,$fname));
    }
}

//レベル説明文
$smarty->assign("level_infos", $level_infos);
$smarty->assign("level_use", $level_use);

//レポート入力支援関連
$smarty->assign("pt_auth", $pt_auth);
$smarty->assign("profile", $profile);
$smarty->assign("emp_class_nm", $emp_class_nm);
$smarty->assign("experience", $experience);
$smarty->assign("profile_rowspan", $profile_rowspan);
$smarty->assign("discoverer_rowspan", $discoverer_rowspan);

//進捗登録権限有無
$progres_edit_flg = ($gamen_mode == 'update' || $gaemn_mode == 'analysis' || $gaemn_mode == 'cate_update') && is_progres_editable($session,$fname,$con);
$smarty->assign("progres_edit_flg", $progres_edit_flg);

//必須項目の表示設定
$smarty->assign("must_item_disp_mode", $must_item_disp_mode);

//「今日ボタン」関連
$this_ymd = date('Ymd');
$this_month = substr($this_ymd, 4, 2);
$week_arr = array(0=>'7',1=>'1',2=>'2',3=>'3',4=>'4',5=>'5',6=>'6');
$day = mktime(0,0,0,date('m'), date('d'), date('Y'));
$day_week = date('w', $day);

// カレンダマスタから休みを取得
// $week_div = ($week_arr[$day_week] == 6 || $week_arr[$day_week] == 7 || get_holiday_name($this_ymd) != "") ? "2" : "1";
$sql = "SELECT type FROM calendar WHERE date = '".date('Y').date('m').date('d')."'";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$result = pg_fetch_array($sel);
if($result['type'] > 0 && $result['type'] <= 3) {
    $week_div = 1;
} else if($result['type'] >= 4) {
    $week_div = 2;
} else {
    $week_div = "";
}
//$week_div = ($result['type'] >= 4) ? 2 : 1;

$smarty->assign("this_ymd", date('Y/m/d')); // 本日日付
$smarty->assign("this_month", intval($this_month)); // 月
$smarty->assign("day_week", $week_arr[$day_week]); // 曜日
$smarty->assign("week_div", $week_div); // 土・日・祝日 : 2  それ以外 : 1

//返信／転送ボタンの利用権限
$inci_btn_show_flg = get_inci_btn_show_flg($con, $fname, $session);
$return_btn_show_flg = ($inci_btn_show_flg["return_btn_show_flg"] == "t");
$forward_btn_show_flg = ($inci_btn_show_flg["forward_btn_show_flg"] == "t");
$print_btn_show_flg = ($inci_btn_show_flg["print_btn_show_flg"] == "t");
$smarty->assign("return_btn_show_flg", $return_btn_show_flg);
$smarty->assign("forward_btn_show_flg", $forward_btn_show_flg);
$smarty->assign("print_btn_show_flg", $print_btn_show_flg);

//事案番号
if($gamen_mode == "update" || $gamen_mode == "analysis" || $gaemn_mode == 'cate_update')
{
    $inci_report_record = $rep_obj->get_inci_report_record($report_id);
    $report_no = $inci_report_record["report_no"];
}
else
{
    $report_no = "";
}
$smarty->assign("report_no", $report_no);


if(($gamen_mode == "update" || $gamen_mode == "analysis" || $gamen_mode == "cate_update") && $mail_id != "")
{
    require_once("hiyari_mail.ini");
    $mail_eis_id = get_eis_id_from_mail_id($con,$fname,$mail_id);
    $smarty->assign("mail_eis_id", $mail_eis_id);
}
else if ($gamen_mode == "shitagaki") {
    $mail_eis_id = $rep_obj->get_eis_id_from_eis_no($sel_eis_no);
    $smarty->assign("mail_eis_id", $mail_eis_id);
}

$is_110_65_use = false;
if($sel_eis_no != "")
{
    $eis_id = $rep_obj->get_eis_id_from_eis_no($sel_eis_no);

    // 患者プロフィール・項目使用
    $patient_use = array();
    // インシデントの概要連動項目・項目使用
    $eis_400_use = array();
    $eis_410_use = array();
    $eis_420_use = array();
    $eis_430_use = array();
    $eis_440_use = array();
    $eis_450_use = array();
    $eis_460_use = array();
    $eis_470_use = array();
    $eis_480_use = array();
    $eis_490_use = array();
    $asses_use = array();

    $sql =
        " SELECT grp_code, easy_item_code FROM inci_easyinput_set2".
        " WHERE eis_id=" . pg_escape_string($eis_id).
        " ORDER BY easy_item_code";
    $db_result = select_from_table($con, $sql, "", $fname);
    while($row = pg_fetch_row($db_result)) {
        $gcode = trim($row[0]);
        $eacode = trim($row[1]);
        if ($gcode==210) $patient_use[] = $eacode;
        else if ($gcode==400) $eis_400_use[] = $eacode;
        else if ($gcode==410) $eis_410_use[] = $eacode;
        else if ($gcode==420) $eis_420_use[] = $eacode;
        else if ($gcode==430) $eis_430_use[] = $eacode;
        else if ($gcode==440) $eis_440_use[] = $eacode;
        else if ($gcode==450) $eis_450_use[] = $eacode;
        else if ($gcode==460) $eis_460_use[] = $eacode;
        else if ($gcode==470) $eis_470_use[] = $eacode;
        else if ($gcode==480) $eis_480_use[] = $eacode;
        else if ($gcode==490) $eis_490_use[] = $eacode;
        else if ($gcode==150) $asses_use[]   = $eacode;
        else if ($style_code==2 && $gcode==110 && $eacode==65) $is_110_65_use = true;
    }
}
else
{
    $patient_use = array("10", "20", "30", "40", "50");

    $eis_400_use = array("10", "20", "30");
    $eis_410_use = array("10", "15", "20", "25", "30", "40", "50", "51", "52", "53", "60", "61", "62", "63");
    $eis_420_use = array("10", "20", "30");
    $eis_430_use = array("10", "20", "30", "40", "41", "42");
    $eis_440_use = array("10", "20", "30", "40", "41", "42", "43", "44");
    $eis_450_use = array("10", "20", "30", "40", "41", "42");
    $eis_460_use = array("10", "20", "30", "40", "41", "42", "43", "44", "50", "51", "52");
    $eis_470_use = array("10", "20", "30", "40", "41", "42", "43", "44", "50", "51", "52");
    $eis_480_use = array("10", "20", "30", "40", "41", "42");
    $eis_490_use = array("10", "20", "30");

    $asses_use = array(3,10,6,7,8,5,20,30,40,50,60,70,80);
}
$smarty->assign("patient_use", $patient_use);
$smarty->assign("patient_use_csv", join(",", $patient_use));

$smarty->assign("eis_400_use", $eis_400_use);
$smarty->assign("eis_410_use", $eis_410_use);
$smarty->assign("eis_420_use", $eis_420_use);
$smarty->assign("eis_430_use", $eis_430_use);
$smarty->assign("eis_440_use", $eis_440_use);
$smarty->assign("eis_450_use", $eis_450_use);
$smarty->assign("eis_460_use", $eis_460_use);
$smarty->assign("eis_470_use", $eis_470_use);
$smarty->assign("eis_480_use", $eis_480_use);
$smarty->assign("eis_490_use", $eis_490_use);
$smarty->assign("eis_400_use_csv", join(",", $eis_400_use));
$smarty->assign("eis_410_use_csv", join(",", $eis_410_use));
$smarty->assign("eis_420_use_csv", join(",", $eis_420_use));
$smarty->assign("eis_430_use_csv", join(",", $eis_430_use));
$smarty->assign("eis_440_use_csv", join(",", $eis_440_use));
$smarty->assign("eis_450_use_csv", join(",", $eis_450_use));
$smarty->assign("eis_460_use_csv", join(",", $eis_460_use));
$smarty->assign("eis_470_use_csv", join(",", $eis_470_use));
$smarty->assign("eis_480_use_csv", join(",", $eis_480_use));
$smarty->assign("eis_490_use_csv", join(",", $eis_490_use));

$smarty->assign("asses_use", $asses_use);

$smarty->assign("is_110_65_use", $is_110_65_use);

//データ連携設定情報
$arr_data_connect = $rep_obj->get_inci_data_connect();
$smarty->assign("patient_profile_flg", $arr_data_connect["patient_profile_flg"]);
$smarty->assign("hospital_ymd_flg", $arr_data_connect["hospital_ymd_flg"]);
$smarty->assign("byoumei_search_flg", $arr_data_connect["byoumei_search_flg"]);
$smarty->assign("emr_flg", $arr_data_connect["emr_flg"]);

// 亀田データ連携対応
$kameda_flg = file_exists("./db2_hiyari_patient_get.php");
$smarty->assign("kameda_flg", $kameda_flg);

// 磐田データ連携対応
if(file_exists("hiyari_iwata_setting.ini"))
{
    require_once("hiyari_iwata_setting.ini");
    $iwata_flg = is_iwata_env($con,$fname,$iwata_prf_org_cd);
    $smarty->assign("iwata_flg", $iwata_flg);
    $smarty->assign("iwata_patient_search_url", $iwata_patient_search_url);
}
else
{
    $smarty->assign("iwata_flg", false);
    $smarty->assign("iwata_patient_search_url", "");
}

// 西淀データ連携対応
if(file_exists("hiyari_nishiyodo.ini.php")) {
    require_once("hiyari_nishiyodo.ini.php");
    $nishiyodo_flg = is_nishiyodo_env($con, $fname, $nishiyodo_prf_org_cd);
    $smarty->assign("nishiyodo_flg", $nishiyodo_flg);
} else {
    $smarty->assign("nishiyodo_flg", false);
}

// 勤医協中央病院様データ連携対応
if(file_exists("hiyari_kinkyo.ini.php")) {
    require_once("hiyari_kinkyo.ini.php");
    $kinkyo_flg = is_kinkyo_env($con, $fname, $kinkyo_prf_org_cd);
    $smarty->assign("kinkyo_flg", $kinkyo_flg);
} else {
    $smarty->assign("kinkyo_flg", false);
}

// 国立福島病院データ連携対応
if(file_exists("hiyari_fukushima.ini.php")) {
    require_once("hiyari_fukushima.ini.php");
    $fukushima_flg = is_fukushima_env($con, $fname, $fukushima_prf_org_cd);
    $smarty->assign("fukushima_flg", $fukushima_flg);
} else {
    $smarty->assign("fukushima_flg", false);
}


// CISデータ連携対応
$sql = "SELECT patient_search_url FROM inci_data_connect";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$result = pg_fetch_array($sel);
if($result['patient_search_url'] == "hiyari_patient_search_cis.php")
{
    require_once("hiyari_cis.ini.php");
    $cis_flg = true;
    $smarty->assign("cis_flg", $cis_flg);

    //他の連携接続フラグをオフにしておく
    $kameda_flg = false;
    $smarty->assign("kameda_flg", false);
    $iwata_flg = false;
    $smarty->assign("iwata_flg", false);
    $nishiyodo_flg = false;
    $smarty->assign("nishiyodo_flg", false);
    $kinkyo_flg = false;
    $smarty->assign("kinkyo_flg", false);
    $boueiidai_flg = false;
    $smarty->assign("boueiidai_flg", false);
    $fukushima_flg = false;
    $smarty->assign("fukushima_flg", false);

}
else
{
    $smarty->assign("cis_flg", false);
}


//外部患者検索データ連携
if ($arr_data_connect["patient_profile_flg"] == "t" &&
    $arr_data_connect["patient_search_url"] != "") {
    $patient_search_ex_flg = true;
} else {
    $patient_search_ex_flg = false;
}
$smarty->assign("patient_search_ex_flg", $patient_search_ex_flg);
$smarty->assign("patient_search_ex_url", $arr_data_connect["patient_search_url"]);

//項目要素非表示情報(更新系の場合は入力値とマージする。)
$item_element_no_disp = $rep_obj->get_item_element_no_disp($registrant_class,$registrant_attribute,$registrant_dept,$registrant_room);

//全ての非表示項目要素に対して
$item_element_no_disp_tmp = array();
foreach($item_element_no_disp as $nd_grp => $nd_grp_val) {
    foreach($nd_grp_val as $nd_item => $nd_item_val) {
        foreach($nd_item_val as $nd_code) {
            //入力値がない場合
            if(!in_array($nd_code,$vals[$nd_grp][$nd_item])) {
                //非表示対象となる。
                $item_element_no_disp_tmp[$nd_grp][$nd_item][] = $nd_code;
            }
        }
    }
}
$item_element_no_disp = $item_element_no_disp_tmp;

//  //入力値の各グループに対して
//  foreach($vals as $vals_grp => $vals_grp_val)
//  {
//      //入力値の各項目に対して
//      foreach($vals_grp_val as $vals_item => $vals_item_val)
//      {
//          //表示制限項目の場合
//          if($item_element_no_disp[$vals_grp][$vals_item] != "")
//          {
//              //全入力値を表示可能項目として追加エントリー
//              foreach($vals_item_val as $vals_input_value)
//              {
//                  $item_element_no_disp[$vals_grp][$vals_item][] = $vals_input_value;
//              }
//          }
//      }
//  }

$smarty->assign("item_element_no_disp", $item_element_no_disp);

//ラジオボタン・チェックボックスの選択肢から非表示項目を抜いたリスト
$selection=null;
if ($design_mode == 2){
    $selection=$rep_obj->get_item_selection($grps, $item_element_no_disp);
    $smarty->assign('selection', $selection);
}

//---------------------------------------
//表示グループ（テーブル）の最後の項目フラグ
//---------------------------------------
if ($design_mode == 2){
    $smarty->assign('last_item', $rep_obj->get_last_item($use_grp_code));
}

// 項目グループ
$item_groups = '';
if ($design_mode == 2){
    foreach ($grps as $grp_code => $grp_val){
        foreach ($grp_val['easy_item_list'] as $easy_item_code => $easy_item_val){
            if (is_array($selection[$grp_code][$easy_item_code]['group'])){
                if ($item_groups != ''){
                    $item_groups .= ',';
                }
                $item_groups .= "'_${grp_code}_${easy_item_code}[]': ";
                $grp_ids = '';
                foreach ($selection[$grp_code][$easy_item_code]['group'] as $item_group_code => $id_list){
                    $ids = '';
                    foreach ($id_list as $id){
                        if ($ids != ''){
                            $ids .= ',';
                        }
                        $ids .= "'id_${grp_code}_${easy_item_code}_${id}'";
                    }
                    if ($grp_ids != ''){
                        $grp_ids .= ',';
                    }
                    $grp_ids .= '[' . $ids . ']';
                }
                $item_groups .= '[' . $grp_ids . ']';
            }
        }
    }
} else {

    foreach ($grps as $grp_code => $grp_val){
        foreach ($grp_val['easy_item_list'] as $easy_item_code => $easy_item_val){
            $item_list = array();
            $id = 0;
            foreach ($easy_item_val['easy_list'] as $item){
                if ($item_element_no_disp[$grp_code][$easy_item_code] != "" && in_array($item['easy_code'], $item_element_no_disp[$grp_code][$easy_item_code])){
                    $id++;
                    continue;  // 非表示項目
                }
                if ($item['item_group_code'] == ""){
                    $id++;
                    continue;  // 非グループ項目
                }
                if ($item_list[$item['item_group_code']] == null){
                    $item_list[$item['item_group_code']] = array();
                }
                $item_list[$item['item_group_code']][] = $id;
                $id++;
            }

            if (count($item_list) > 0){
                if ($item_groups != ''){
                    $item_groups .= ',';
                }
                $item_groups .= "'_${grp_code}_${easy_item_code}[]': ";
                $grp_ids = '';
                foreach ($item_list as $item_group_code => $id_list){
                    $ids = '';
                    foreach ($id_list as $id){
                        if ($ids != ''){
                            $ids .= ',';
                        }
                        $ids .= "'id_${grp_code}_${easy_item_code}_${id}'";
                    }
                    if ($grp_ids != ''){
                        $grp_ids .= ',';
                    }
                    $grp_ids .= '[' . $ids . ']';
                }
                $item_groups .= '[' . $grp_ids . ']';
            }
        }
    }
}
$item_groups = '{' . $item_groups . '}';
$smarty->assign('item_groups', $item_groups);


//印刷設定情報取得
$print_setting = get_print_setting($con,$fname);
$patient_non_print_flg = $print_setting["patient_non_print_flg"] == "t";
$patient_non_print_select_flg = $print_setting["patient_non_print_select_flg"] == "t";
$smarty->assign("patient_non_print_flg", $patient_non_print_flg);
$smarty->assign("patient_non_print_select_flg", $patient_non_print_select_flg);

//オートリロード設定取得
$auto_reload_setting = get_auto_reload_setting($con,$fname);
$report_gamen = $auto_reload_setting["report_gamen"];
if($report_gamen == "t")
{
    $smarty->assign("auto_reload", true);
}


$list_btn_show_flg =getListBtnShowFlg($con, $session, $fname);
$smarty->assign("list_btn_show_flg", $list_btn_show_flg);


//分析再発防止画面
if($gamen_mode == "analysis")
{
    //-------------------------------------
    //送信データ情報
    //-------------------------------------
    $rpt_data = $rep_obj->get_report_data_for_disp($report_id, $grp_flags['eis_id']);

    $smarty->assign("rpt_data", $rpt_data);

    //-------------------------------------
    //確認コメント情報
    //-------------------------------------
    $progress_comment_use_flg = get_progress_comment_use($con,$fname);

    $arr_report_auth = array();
    if ($design_mode == 2){
        $arr_report_auth = $rep_obj->get_my_inci_auth_list_for_report_progress($report_id, $emp_id);
    }

    if($progress_comment_use_flg || $design_mode == 2){
        $sql  = " SELECT au.auth, au.auth_name, au.auth_rank, come.comment_date, come.emp_id, come.progress_comment";
        $sql .= " FROM (SELECT * FROM inci_report_progress_comment WHERE report_id = {$report_id}) come";
        $sql .= " LEFT JOIN inci_auth_mst au ON come.auth = au.auth";
        if(count($arr_report_auth) > 0){
            $date = date("Y/m/d");
            $sql .= " UNION";
            $sql .= " SELECT auth, auth_name, auth_rank, '{$date}', '{$emp_id}', ''";
            $sql .= " FROM inci_auth_mst WHERE";
            foreach($arr_report_auth as $key=> $value){
                $auth = $value['auth'];
                if ($key > 0){
                    $sql .= " OR";
                }
                $sql .= " (auth='{$auth}' AND '{$emp_id}' NOT IN (SELECT emp_id FROM inci_report_progress_comment WHERE report_id={$report_id} AND auth='{$auth}'))";
            }
        }
        $sql .= " ORDER BY auth_rank,comment_date";

        $sel = select_from_table($con,$sql,"",$fname);
        if($sel == 0){
            pg_close($con);
            echo("<script type='text/javascript' src='./js/showpage.js'></script>");
            echo("<script language='javascript'>showErrorPage(window);</script>");
            exit;
        }
        $tmp_db_data_arr = pg_fetch_all($sel);

        $progress_comment_data = "";
        $target_auth_list = "";
        foreach($tmp_db_data_arr as $tmp_db_data)
        {
            $auth = $tmp_db_data["auth"];


            // 評価権限フラグ 20140421 R.C
            //受信トレイにある他の部署の報告書を評価可能にするフラグを取得
            //$is_progress_eval = get_progress_eval($con, $fname);
            $is_progress_eval = $sysConf->get('fantol.evaluation.common.use.flg');
            if($is_progress_eval=='t'){
                $auth_flag = true;
            }else{
                $auth_flag = $sysConf->get('fantol.evaluation.use.' . strtolower($auth) . '.flg') == 't';
            }

            if($auth_flag){
                $progress_comment_data[$auth]['auth_name'] = $tmp_db_data["auth_name"];

                $tmp_data["date"] = $tmp_db_data["comment_date"];
                $tmp_data["emp_name"] = get_emp_kanji_name($con,$tmp_db_data["emp_id"],$fname);;
                $tmp_data["comment"] = $tmp_db_data["progress_comment"];

                if ($emp_id == $tmp_db_data["emp_id"]){
                    $tmp_data["is_editable"] = true;
                    if($target_auth_list != ""){
                        $target_auth_list .= ",";
                    }
                    $target_auth_list .= $auth;

                    if ( !$is_first_access) {
                        $key = 'comment_' . $auth;
                        $tmp_data["comment"] = $_SESSION['easyinput'][$key];
                    }

                }
                else{
                    $tmp_data["is_editable"] = false;
                }

                $progress_comment_data[$auth]['data'][] = $tmp_data;
             }
        }
    }

    //コメントがなければ表示しない。
    if ($progress_comment_data != ""){
        $smarty->assign("progress_comment_data", $progress_comment_data);
    }
    if ($target_auth_list != ""){
        $smarty->assign("target_auth_list", $target_auth_list);
    }

    //ここまで
}

//20130630
//==================================================
//ファイル添付
//==================================================

// 報告書へのファイル添付フラグ
$fantol_file_attach = $fantol_file_attach === null ? '0' : $fantol_file_attach;
$smarty->assign("file_attach", $fantol_file_attach);
$smarty->assign("file_max_size", ini_get("upload_max_filesize"));

//既に登録済みとなっているファイル一覧
$smarty->assign("file_list", $ret_file);

//登録作業中ファイル一覧
$smarty->assign('arr_file', $arr_file);

//様式２の報告書のみ分析再発防止でファイル添付を表示する
if ($sel_eis_no){
    $style_code_for_tmp_file = $rep_obj->get_eis_style_from_eis_no($sel_eis_no);
    $smarty->assign('style_code_for_tmp_file', $style_code_for_tmp_file);
}

if($frompage){
$smarty->assign(frompage,$frompage);
}else{
$smarty->assign(callerpage,$callerpage);
}

$smarty->assign(predetermined_eval,$predetermined_eval);



//がん研カスタマイズ
$report_title_wording = $sysConf->get('fantol.report.title.wording');
if($report_title_wording){
    $smarty->assign(report_title_wording,$report_title_wording);
}else{
    $smarty->assign(report_title_wording,"表題");
}

//==================================================
//表示
//==================================================
if ($design_mode == 1){
    if ($style_code == 1){
    //様式情報なし
        $smarty->display("hiyari_easyinput.tpl");
    }
    else{
    //指定様式
        $smarty->display("hiyari_easyinput2.tpl");
    }
}
else{
    if ($report_contents == 1){
        //評価画面の報告書内容欄で「別ウィンドウに表示」をクリックされた場合の画面
        $smarty->display("report_contents.tpl");
    }
    else{
        $smarty->display("report_form.tpl");
    }
}

//==================================================
//DB切断
//==================================================
pg_close($con);

//==================================================
// 実行終了
//==================================================
exit;
