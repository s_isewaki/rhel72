<?
require_once("about_comedix.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	js_login_exit();
}

// 権限のチェック
if ($wherefrom == "employee") {
	$checkauth = check_authority($session, 19, $fname);
	if ($checkauth == "0") {
		js_login_exit();
		exit;
	}
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// ログインユーザの職員IDを取得
if ($wherefrom != "employee") {
	$sql = "select emp_id from session";
	$cond = q("where session_id = '%s'", $session);
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		js_error_exit();
	}
	$emp_id = pg_fetch_result($sel, 0, "emp_id");
}

// 追加モード
if ($del_alias == "") {

	// 入力チェック（形式）
	// 「:」と「,」以外の印字可能ASCIIのみ許可
	if (preg_match("/\A[\x20-\x2B\x2D-\x39\x3B-\x7E]+\z/", $alias) === 0) {
		pg_query($con, "rollback");
		pg_close($con);
		js_alert_exit("正しいアドレスを入力してください。");
	}
	// 「@」は必ず含むこと
	if (strpos($alias, "@") === false) {
		pg_query($con, "rollback");
		pg_close($con);
		js_alert_exit("正しいアドレスを入力してください。");
	}

	// 転送先アドレスを登録
	$sql = "select count(*) from webmail_alias";
	$cond = q("where emp_id = '%s'", $emp_id);
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		js_error_exit();
	}
	$count = intval(pg_fetch_result($sel, 0, 0));
	if ($count === 0) {
		$sql = "insert into webmail_alias (emp_id, alias) values (";
		$content = array($emp_id, $alias);
		$ins = insert_into_table($con, $sql, q($content), $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			js_error_exit();
		}
	} else {
		$sql = "update webmail_alias set";
		$set = array("alias");
		$setvalue = array($alias);
		$cond = q("where emp_id = '%s'", $emp_id);
		$upd = update_set_table($con, $sql, $set, q($setvalue), $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			js_error_exit();
		}
	}

// 削除モード
} else {

	// 転送先アドレスを削除
	$sql = "delete from webmail_alias";
	$cond = q("where emp_id = '%s' and alias = '%s'", $emp_id, $del_alias);
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		js_error_exit();
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 画面をリフレッシュ
if ($wherefrom == "employee") {
	$url_key1 = urlencode($key1);
	$url_key2 = urlencode($key2);
	echo("<script type=\"text/javascript\">location.href = 'employee_alias_setting.php?session=$session&emp_id=$emp_id&key1=$url_key1&key2=$url_key2&class_cond=$class_cond&atrb_cond=$atrb_cond&dept_cond=$dept_cond&room_cond=$room_cond&job_cond=$job_cond&page=$page&view=$view&emp_o=$emp_o';</script>");
} else {
	echo("<script type=\"text/javascript\">location.href = 'option_alias.php?session=$session';</script>");
}
