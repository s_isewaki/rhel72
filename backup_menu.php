<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>バックアップ | バックアップ</title>
<? require("about_authority.php"); ?>
<? require("about_session.php"); ?>
<? require("about_postgres.php"); ?>
<?
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// バックアップ権限のチェック
$auth = check_authority($session, 39, $fname);
if ($auth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$con = connect2db($fname);
?>
<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="backup_menu.php?session=<? echo($session); ?>"><img src="img/icon/b30.gif" width="32" height="32" border="0" alt="バックアップ"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="./backup_menu.php?session=<? echo($session); ?>"><b>バックアップ</b></a></font></td>
</tr>
</table>
<form action="backup_dump_download.php" method="get" target="download">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
<td height="22" class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">データベースのバックアップファイルをクライアントPCに保存できます。<br>
このファイルは障害発生時の復旧ファイルとして使用します。詳しい復旧手順はマニュアルをご参照下さい。
</font></td>
</tr>
<tr>
<td height="30"><input type="submit" value="バックアップファイルのダウンロード"><input type="hidden" name="session" value="<? echo($session); ?>"></td>
</tr>
</table>
</form>
<iframe name="download" width="0" height="0" frameborder="0"></iframe>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
