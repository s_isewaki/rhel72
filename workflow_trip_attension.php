<?php
// 出張旅費
// 注意事項マスタ・一覧
require_once("about_comedix.php");
require_once("workflow_common.ini");
require_once("referer_common.ini");

$fname = $_SERVER["PHP_SELF"];
$workflow = @$_REQUEST["workflow"];

// セッションと権限のチェック
$session = qualify_session($_REQUEST["session"], $fname);
$checkauth = check_authority($session, 3, $fname);
if ($session == "0" or $checkauth == "0") {
	js_login_exit();
}

// データベースに接続
$con = connect2db($fname);

// 遷移元の設定
if ($workflow != "") {
	set_referer($con, $session, "workflow", $workflow, $fname);
	$referer = $workflow;
} else {
	$referer = get_referer($con, $session, "workflow", $fname);
}

// 注意事項マスタの取得
$sql = "SELECT * FROM trip_attension ORDER BY attension_id";
$sel = select_from_table($con,$sql,"",$fname);
if ($sel == 0) {
	js_error_exit();
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ワークフロー | 出張旅費 | 注意事項マスタ・一覧</title>
<script type="text/javascript" src="js/yui_2.8.1/build/yahoo-dom-event/yahoo-dom-event.js"></script>
<script type="text/javascript" src="js/yui_2.8.1/build/dragdrop/dragdrop-min.js"></script>
<script type="text/javascript" src="js/yui_2.8.1/build/animation/animation-min.js"></script>
<script type="text/javascript" src="js/yui_2.8.1/build/element/element-min.js"></script>
<script type="text/javascript" src="js/workflow/trip_attension.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/workflow/workflow.css">
<link rel="stylesheet" type="text/css" href="css/workflow/trip.css">
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<? if ($referer == "2") { ?>
<table id="header">
<tr>
<td class="icon"><a href="workflow_menu.php?session=<? echo($session); ?>"><img src="img/icon/b24.gif" width="32" height="32" border="0" alt="ワークフロー"></a></td>
<td><a href="workflow_menu.php?session=<?=$session?>"><b>ワークフロー</b></a></td>
</tr>
</table>
<? } else { ?>
<table id="header">
<tr>
<td class="icon"><a href="application_menu.php?session=<? echo($session); ?>"><img src="img/icon/b08.gif" width="32" height="32" border="0" alt="決裁・申請"></a></td>
<td>
	<a href="application_menu.php?session=<? echo($session); ?>"><b>決裁・申請</b></a> &gt;
	<a href="workflow_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a> &gt;
	<a href="workflow_trip_seisan.php?session=<? echo($session); ?>"><b>出張旅費</b></a>
</td>
<td class="kanri"><a href="application_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></td>
</tr>
</table>
<? } ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td valign="top" width="120"><?show_wkfw_sidemenu($session);?></td>
<td width="5"><img src="img/spacer.gif" width="5" height="2" alt=""></td>
<td valign="top">

<?
show_wkfw_menuitem($session, $fname, "");
?>

<form id="mstform" method="post">
<div class="button">
<button type="button" id="submit_add">レコード登録</button>
<button type="button" id="submit_view">表示/非表示の更新</button>
</div>

<table id="msttable">
<thead>
<tr>
	<td>ID</td>
	<td>名称</td>
	<td>作成日時</td>
	<td>更新日時</td>
	<td>表示</td>
</tr>
</thead>
<tbody>
<?
while ($r = pg_fetch_assoc($sel)) {
?>
<tr>
	<td class="id"><?eh($r["attension_id"]);?><input name="no[]" type="hidden" value="<?eh($r["attension_id"]);?>"/></td>
	<td><a href="workflow_trip_attension_form.php?session=<?=$session?>&amp;id=<?eh($r["attension_id"]);?>"><?eh($r["title"]);?></a></td>
	<td class="date"><?eh($r["created_on"]);?></td>
	<td class="date"><?eh($r["updated_on"]);?></td>
	<td class="check"><input name="view[<?eh($r["attension_id"]);?>]" type="checkbox" value="t" <?echo ($r["view_flg"]=='t') ? 'checked' : '';?>/></td>
</tr>
<?}?>
</tbody>
</table>

<input type="hidden" name="session" value="<?=$session?>" />
<input type="hidden" id="mode" name="mode" value="" />
</form>

</td>
</tr>
</table>
</body>
</html>
<?php
pg_close($con);