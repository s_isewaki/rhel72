<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("show_select_values.ini");
require_once("cl_application_list.inc");
require_once("cl_yui_calendar_util.ini");
require_once("cl_application_common.ini");
require_once("cl_common.ini");
require_once("cl_application_workflow_common_class.php");
require_once("cl_title_name.ini");
require_once("cl_smarty_setting.ini");
require_once("cl_common_log_class.inc");
require_once("Cmx.php");
require_once('MDB2.php');

$log = new cl_common_log_class(basename(__FILE__));

$log->info(basename(__FILE__)." START");

$smarty = cl_get_smarty_obj();
$smarty->template_dir = dirname(__FILE__) . "/cl/view";

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,$CAS_MENU_AUTH,$fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
// ワークフロー権限の取得
$workflow_auth = check_authority($session, $CL_MANAGE_AUTH, $fname);

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
// ログインユーザ情報取得
//====================================
$obj = new cl_application_workflow_common_class($con, $fname);
$arr_login = $obj->get_empmst($session);
$login_emp_id = $arr_login[0]["emp_id"];

//====================================
// 初期化処理
//====================================
$arr_wkfwcatemst = array();
$sel_wkfwcate = search_wkfwcatemst($con, $fname);
while($row_cate = pg_fetch_array($sel_wkfwcate)) {
	$tmp_wkfw_type = $row_cate["wkfw_type"];
	$tmp_wkfw_nm = $row_cate["wkfw_nm"];

	$arr_wkfwmst_tmp = array();
	$sel_wkfwmst = search_wkfwmst($con, $fname, $tmp_wkfw_type);
	while($row_wkfwmst = pg_fetch_array($sel_wkfwmst)) {
		$tmp_wkfw_id = $row_wkfwmst["wkfw_id"];
		$wkfw_title = $row_wkfwmst["wkfw_title"];

		$arr_wkfwmst_tmp[$tmp_wkfw_id] = $wkfw_title;
	}
	$arr_wkfwcatemst[$tmp_wkfw_type] = array("name" => $tmp_wkfw_nm, "wkfw" => $arr_wkfwmst_tmp);
}

/**
 * 志願申請、評価申請用の志願者取得（申請者名）
 */
$arr_emp = $obj->get_empmst($session);
$emp_id  = $arr_emp["0"]["emp_id"];
$lt_nm  = $arr_emp["0"]["emp_lt_nm"];
$ft_nm  = $arr_emp["0"]["emp_ft_nm"];

$applicant_name = "$lt_nm"." "."$ft_nm";


// 申請日セット(本日日付から過去２ヶ月分)
// 申請タブをクリックした場合のみ。
if($apply_date_defalut == "on")
{
	$today = date("Y/m/d", strtotime("today"));

	$arr_today = split("/", $today);
	$date_y2 = $arr_today[0];
	$date_m2 = $arr_today[1];
	$date_d2 = $arr_today[2];

	// ２ヶ月前の日付取得
	$two_months_ago = date("Y/m/d",strtotime("-2 months" ,strtotime($today)));

	$arr_two_months_ago = split("/", $two_months_ago);
	$date_y1 = $arr_two_months_ago[0];
	$date_m1 = $arr_two_months_ago[1];
	$date_d1 = $arr_two_months_ago[2];
}

//##########################################################################
// データベース接続オブジェクト取得
//##########################################################################
$log->debug("MDB2オブジェクト取得開始",__FILE__,__LINE__);
$mdb2 = MDB2::connect(CMX_DB_DSN);
if (PEAR::isError($mdb2)) {
	$log->error("MDB2オブジェクト取得エラー：".$mdb2->getDebugInfo(),__FILE__,__LINE__);
}
$log->debug("MDB2オブジェクト取得終了",__FILE__,__LINE__);

require_once("cl/model/master/cl_mst_inside_training_model.php");
$training_model = new cl_mst_inside_training_model($mdb2,$login_emp_id);
$log->debug("院内研修用DBA取得",__FILE__,__LINE__);

require_once("cl/model/master/cl_mst_inside_training_theme_model.php");
$theme_model = new cl_mst_inside_training_theme_model($mdb2,$login_emp_id);
$log->debug("研修課題用DBA取得",__FILE__,__LINE__);

require_once("cl/model/search/cl_mst_inside_training_schedule_util.php");
$schedule_model = new cl_mst_inside_training_schedule_util($mdb2, $login_emp_id);
$log->debug("研修日程取得用DBA取得",__FILE__,__LINE__);


if ($_POST['updateflg'] == 'true')
{
	$training_id		= $_POST['training_id'];
	$training_name		= $_POST['training_name'];
	$training_purpose	= $_POST['training_purpose'];
	$target_level1		= $_POST['target_level1'];
	$target_level2		= $_POST['target_level2'];
	$target_level3		= $_POST['target_level3'];
	$target_level4		= $_POST['target_level4'];
	$target_level5		= $_POST['target_level5'];
	$training_require	= $_POST['training_require'];
	$time_division		= $_POST['time_division'];
	$training_opener	= $_POST['training_opener_id'];
	$training_teacher1	= $_POST['training_teacher1_id'];
	$training_teacher2	= $_POST['training_teacher2_id'];
	$training_teacher3	= $_POST['training_teacher3_id'];
	$training_teacher4	= $_POST['training_teacher4_id'];
	$training_teacher5	= $_POST['training_teacher5_id'];
	$max_training_time  = $_POST['max_training_time'];
	$training_slogan	= $_POST['training_slogan'];
	$training_contents	= $_POST['training_contents'];
	$training_action	= $_POST['training_action'];
	$training_support	= $_POST['training_support'];
	if ($_POST['abolition_flag'] == 't') {
		$abolition_flag = 1;
	} else {
		$abolition_flag = 0;
	}
	// 2012/07/25 Yamagawa add(s)
	if ($_POST['auto_control_flg'] == 't') {
		$auto_control_flg = true;
	} else {
		$auto_control_flg = false;
	}
	// 2012/07/25 Yamagawa add(e)
	// 2012/11/19 Yamagawa add(s)
	$term_div = $_POST['term_div'];
	// 2012/11/19 Yamagawa add(e)

	define("THEME_FLAG_NONE",0);
	define("THEME_FLAG_INSERT",1);
	define("THEME_FLAG_UPDATE",2);
	define("THEME_FLAG_DELETE",3);

	$theme_flag_before		= THEME_FLAG_NONE;
	$theme_id_before		= $_POST['theme_id_before'];
	$theme_contents_before	= $_POST['theme_contents_before'];
	$theme_method_before	= $_POST['theme_method_before'];
	$theme_deadline_before	= $_POST['theme_deadline_before'];
	$theme_present_before	= $_POST['theme_present_before_id'];

	$theme_flag_now			= THEME_FLAG_NONE;
	$theme_id_now			= $_POST['theme_id_now'];
	$theme_contents_now		= $_POST['theme_contents_now'];
	$theme_method_now		= $_POST['theme_method_now'];
	$theme_deadline_now		= $_POST['theme_deadline_now'];
	$theme_present_now		= $_POST['theme_present_now_id'];

	$theme_flag_after		= THEME_FLAG_NONE;
	$theme_id_after			= $_POST['theme_id_after'];
	$theme_contents_after	= $_POST['theme_contents_after'];
	$theme_method_after		= $_POST['theme_method_after'];
	$theme_deadline_after	= $_POST['theme_deadline_after'];
	$theme_present_after	= $_POST['theme_present_after_id'];

	require_once("cl/model/sequence/cl_inside_training_task_id_seq_model.php");
	$theme_id_model = new cl_inside_training_task_id_seq_model($mdb2,$login_emp_id);
	$log->debug("研修課題ID用DBA取得",__FILE__,__LINE__);

	if (
		$theme_contents_before== "" &&
		$theme_method_before== "" &&
		$theme_deadline_before== "" &&
		$theme_present_before== ""
	) {
		if ($theme_id_before == "") {
			$theme_flag_before = THEME_FLAG_NONE;
		} else {
			$theme_flag_before = THEME_FLAG_DELETE;
		}
	} else if ($theme_id_before == "") {
		$theme_flag_before = THEME_FLAG_INSERT;
		$theme_id_before = $theme_id_model->getId();
	} else {
		$theme_flag_before = THEME_FLAG_UPDATE;
	}
	$log->debug("研修前課題用ID取得",__FILE__,__LINE__);

	if (
		$theme_contents_now== "" &&
		$theme_method_now== "" &&
		$theme_deadline_now== "" &&
		$theme_present_now== ""
	) {
		if ($theme_id_now == "") {
			$theme_flag_now = THEME_FLAG_NONE;
		} else {
			$theme_flag_now = THEME_FLAG_DELETE;
		}
	} else if ($theme_id_now == "") {
		$theme_flag_now = THEME_FLAG_INSERT;
		$theme_id_now = $theme_id_model->getId();
	} else {
		$theme_flag_now = THEME_FLAG_UPDATE;
	}
	$log->debug("研修中課題用ID取得",__FILE__,__LINE__);

	if (
		$theme_contents_after== "" &&
		$theme_method_after== "" &&
		$theme_deadline_after== "" &&
		$theme_present_after== ""
	) {
		if ($theme_id_after == "") {
			$theme_flag_after = THEME_FLAG_NONE;
		} else {
			$theme_flag_after = THEME_FLAG_DELETE;
		}
	} else if ($theme_id_after == "") {
		$theme_flag_after = THEME_FLAG_INSERT;
		$theme_id_after = $theme_id_model->getId();
	} else {
		$theme_flag_after = THEME_FLAG_UPDATE;
	}
	$log->debug("研修後課題用ID取得",__FILE__,__LINE__);

	// トランザクション開始
	$mdb2->beginTransaction();
	$log->debug("トランザクション開始",__FILE__,__LINE__);
	$param = array(
		"training_id"			=> $training_id,
		"training_name"			=> pg_escape_string($training_name),
		"training_purpose"		=> pg_escape_string($training_purpose),
		"target_level1"			=> $target_level1,
		"target_level2"			=> $target_level2,
		"target_level3"			=> $target_level3,
		"target_level4"			=> $target_level4,
		"target_level5"			=> $target_level5,
		"max_training_time"		=> $max_training_time,
		"training_require"		=> pg_escape_string($training_require),
		"time_division"			=> $time_division,
		"training_opener"		=> $training_opener,
		"training_teacher1"		=> $training_teacher1,
		"training_teacher2"		=> $training_teacher2,
		"training_teacher3"		=> $training_teacher3,
		"training_teacher4"		=> $training_teacher4,
		"training_teacher5"		=> $training_teacher5,
		"training_slogan"		=> pg_escape_string($training_slogan),
		"training_contents"		=> pg_escape_string($training_contents),
		"training_action"		=> pg_escape_string($training_action),
		"training_support"		=> pg_escape_string($training_support),
		"abolition_flag"		=> $abolition_flag,
		"delete_flg"			=> 0,
		// 2012/07/25 Yamagawa add(s)
		"auto_control_flg"		=> $auto_control_flg
		// 2012/07/25 Yamagawa add(e)
		// 2012/11/19 Yamagawa add(s)
		,"term_div"				=> $term_div
		// 2012/11/19 Yamagawa add(e)
	);

	$log->debug("研修更新用パラメータ取得",__FILE__,__LINE__);
	$training_model->update($param);
	$log->debug("研修更新",__FILE__,__LINE__);

	/** 日程更新 */
	if ($_POST['before_max_training_time'] != $_POST['max_training_time']){

		require_once("cl/model/master/cl_mst_inside_training_schedule_model.php");
		$schedule_regist_model = new cl_mst_inside_training_schedule_model($mdb2, $login_emp_id);
		$log->debug("研修日程更新用DBA取得",__FILE__,__LINE__);

		//require_once("cl/model/sequence/cl_inside_training_schedule_id_seq_model.php");
		//$cl_inside_training_schedule_id_seq_model = new cl_inside_training_schedule_id_seq_model($mdb2, $login_user);
		//$log->debug("研修日程ID取得用DBA取得",__FILE__,__LINE__);

		// 対象データ取得
		$schedule_data = $schedule_model->getListForReInsert($training_id);

		foreach($schedule_data as $schedule_row){

			// 論理削除
			//$schedule_regist_model->logical_delete($schedule_row['plan_id']);

			// 再登録用研修日程ID取得
			//$plan_id=$cl_inside_training_schedule_id_seq_model->getId();

			// 最終フラグ
			if ($schedule_row['training_time'] == $_POST['max_training_time']) {
				$final_flg = 1;
			} else {
				$final_flg = 0;
			}

			$param = array(
				"plan_id"			=> $schedule_row['plan_id']
				,"first_plan_id"	=> $schedule_row['first_plan_id']
				,"training_id"		=> $schedule_row['training_id']
				,"plan_no"			=> $schedule_row['plan_no']
				,"year"				=> $schedule_row['year']
				,"term_div"			=> $schedule_row['term_div']
				,"training_time"	=> $schedule_row['training_time']
				,"final_flg"		=> $final_flg
				,"plan_date"		=> $schedule_row['plan_date']
				,"from_time"		=> $schedule_row['from_time']
				,"to_time"			=> $schedule_row['to_time']
				,"report_division"	=> $schedule_row['report_division']
				,"answer_division"	=> $schedule_row['answer_division']
				,"max_people"		=> $schedule_row['max_people']
				,"apply_status"		=> $schedule_row['apply_status']
				,"place"			=> $schedule_row['place']
				,"remarks"			=> $schedule_row['remarks']
				,"delete_flg"		=> 0
			);

			// 再登録
			//$schedule_regist_model->insert($param);
			// 更新
			$schedule_regist_model->update($param);

		}

	}

	$param = array(
		"training_id"		=> $training_id,
		"theme_id"			=> $theme_id_before,
		"theme_division"	=> 0,
		"theme_contents"	=> pg_escape_string($theme_contents_before),
		"theme_method"		=> pg_escape_string($theme_method_before),
		"theme_deadline"	=> pg_escape_string($theme_deadline_before),
		"theme_present"		=> $theme_present_before,
		"delete_flg"		=> 0
	);
	switch($theme_flag_before){
		case THEME_FLAG_INSERT:
			$theme_model->insert($param);
			break;
		case THEME_FLAG_UPDATE:
			$theme_model->update($param);
			break;
		case THEME_FLAG_DELETE:
			$theme_model->logical_delete($theme_id_before);
			break;
		default:
			break;
	}
	$log->debug("研修前課題更新",__FILE__,__LINE__);

	$param = array(
		"training_id"		=> $training_id,
		"theme_id"			=> $theme_id_now,
		"theme_division"	=> 1,
		"theme_contents"	=> pg_escape_string($theme_contents_now),
		"theme_method"		=> pg_escape_string($theme_method_now),
		"theme_deadline"	=> pg_escape_string($theme_deadline_now),
		"theme_present"		=> $theme_present_now,
		"delete_flg"		=> 0
	);
	switch($theme_flag_now){
		case THEME_FLAG_INSERT:
			$theme_model->insert($param);
			break;
		case THEME_FLAG_UPDATE:
			$theme_model->update($param);
			break;
		case THEME_FLAG_DELETE:
			$theme_model->logical_delete($theme_id_now);
			break;
		default:
			break;
	}
	$log->debug("研修中課題更新",__FILE__,__LINE__);

	$param = array(
		"training_id"		=> $training_id,
		"theme_id"			=> $theme_id_after,
		"theme_division"	=> 2,
		"theme_contents"	=> pg_escape_string($theme_contents_after),
		"theme_method"		=> pg_escape_string($theme_method_after),
		"theme_deadline"	=> pg_escape_string($theme_deadline_after),
		"theme_present"		=> $theme_present_after,
		"delete_flg"		=> 0
	);
	switch($theme_flag_after){
		case THEME_FLAG_INSERT:
			$theme_model->insert($param);
			break;
		case THEME_FLAG_UPDATE:
			$theme_model->update($param);
			break;
		case THEME_FLAG_DELETE:
			$theme_model->logical_delete($theme_id_after);
			break;
		default:
			break;
	}
	$log->debug("研修後課題更新",__FILE__,__LINE__);

	// コミット
	$mdb2->commit();
	$log->debug("コミット",__FILE__,__LINE__);

	echo("<script language='javascript'>window.opener.search_training();</script>");
	echo("<script language='javascript'>window.close();</script>");
	exit;

}

$cl_title = cl_title_name();

/**
 * 研修講座を取得
 */
$training_data = $training_model->select_training_view($training_id);

$schedule_data = $schedule_model->getList($training_id);
$schedule_count = count($schedule_data);

$param = array(
	"training_id"		=> $training_id,
	"theme_division"	=> 0
);

$theme_data_before = $theme_model->select_training_view($param);

$param = array(
	"training_id"		=> $training_id,
	"theme_division"	=> 1
);

$theme_data_now = $theme_model->select_training_view($param);

$param = array(
	"training_id"		=> $training_id,
	"theme_division"	=> 2
);
$theme_data_after = $theme_model->select_training_view($param);

if ($training_data['abolition_flag'] == 1) {
	$abolition_flag = 'checked';
} else {
	$abolition_flag = '';
}

// 2012/07/25 Yamagawa add(s)
if ($training_data['auto_control_flg'] == 't') {
	$auto_control_flg = 'checked';
} else {
	$auto_control_flg = '';
}
// 2012/07/25 Yamagawa add(e)

$training_opener_nm			= $training_data['opener_lt_nm']."&nbsp;&nbsp;".$training_data['opener_ft_nm'];
$training_teacher1_nm		= $training_data['teacher1_lt_nm']."&nbsp;&nbsp;".$training_data['teacher1_ft_nm'];
$training_teacher2_nm		= $training_data['teacher2_lt_nm']."&nbsp;&nbsp;".$training_data['teacher2_ft_nm'];
$training_teacher3_nm		= $training_data['teacher3_lt_nm']."&nbsp;&nbsp;".$training_data['teacher3_ft_nm'];
$training_teacher4_nm		= $training_data['teacher4_lt_nm']."&nbsp;&nbsp;".$training_data['teacher4_ft_nm'];
$training_teacher5_nm		= $training_data['teacher5_lt_nm']."&nbsp;&nbsp;".$training_data['teacher5_ft_nm'];
$theme_present_before_nm	= $theme_data_before['emp_lt_nm']."&nbsp;&nbsp;".$theme_data_before['emp_ft_nm'];
$theme_present_now_nm		= $theme_data_now['emp_lt_nm']."&nbsp;&nbsp;".$theme_data_now['emp_ft_nm'];
$theme_present_after_nm		= $theme_data_after['emp_lt_nm']."&nbsp;&nbsp;".$theme_data_after['emp_ft_nm'];

// レベルI
$level1_options_str = get_level_options($training_data['target_level1']);

// レベルII
$level2_options_str = get_level_options($training_data['target_level2']);

// レベルIII
$level3_options_str = get_level_options($training_data['target_level3']);

// レベルIV
$level4_options_str = get_level_options($training_data['target_level4']);

// レベルV
$level5_options_str = get_level_options($training_data['target_level5']);

$time_division_options_str = get_time_division_options($training_data['time_division']);

// 2012/11/19 Yamagawa add(s)
$term_div_options_str = get_term_div_options($training_data['term_div']);
// 2012/11/19 Yamagawa add(e)

/**
 * Javascriptコード生成（動的オプション項目変更用）
 */
$js_str = get_js_str($arr_wkfwcatemst,$workflow);

/**
 * データベースクローズ
 */
pg_close($con);

	// 研修情報 エスケープ
	//2012/05/09 K.Fujii upd(s)
	//$training_data = str_replace('"',    '&quot;', $training_data);
	//$training_data = str_replace('<',    '&lt;',   $training_data);
	//$training_data = str_replace('>',    '&gt;',   $training_data);
	//$training_data = str_replace('\\\\', '\\',     $training_data);
	$training_data = str_replace('\'\'',   '&#039;', $training_data);
	$training_data = str_replace('"',      '&quot;', $training_data);
	$training_data = str_replace('\\\\',   '\\',     $training_data);
	$training_data = str_replace('&lt;',   '<',      $training_data);
	$training_data = str_replace('&gt;',   '>',      $training_data);
	$training_data = str_replace('\\n',   '&#13;',   $training_data);
	//2012/05/09 K.Fujii upd(e)

	//2012/05/09 K.Fujii upd(s)
	//$theme_data_before = str_replace('"',    '&quot;', $theme_data_before);
	//$theme_data_before = str_replace('<',    '&lt;',   $theme_data_before);
	//$theme_data_before = str_replace('>',    '&gt;',   $theme_data_before);
	//$theme_data_before = str_replace('\\\\', '\\',     $theme_data_before);
	$theme_data_before = str_replace('\'\'',   '&#039;', $theme_data_before);
	$theme_data_before = str_replace('"',      '&quot;', $theme_data_before);
	$theme_data_before = str_replace('\\\\',   '\\',     $theme_data_before);
	$theme_data_before = str_replace('&lt;',   '<',      $theme_data_before);
	$theme_data_before = str_replace('&gt;',   '>',      $theme_data_before);
	$theme_data_before = str_replace('\\n',   '&#13;',   $theme_data_before);
	//2012/05/09 K.Fujii upd(e)

	//2012/05/09 K.Fujii upd(s)
	//$theme_data_now = str_replace('"',    '&quot;', $theme_data_now);
	//$theme_data_now = str_replace('<',    '&lt;',   $theme_data_now);
	//$theme_data_now = str_replace('>',    '&gt;',   $theme_data_now);
	//$theme_data_now = str_replace('\\\\', '\\',     $theme_data_now);
	$theme_data_now = str_replace('\'\'',   '&#039;', $theme_data_now);
	$theme_data_now = str_replace('"',      '&quot;', $theme_data_now);
	$theme_data_now = str_replace('\\\\',   '\\',     $theme_data_now);
	$theme_data_now = str_replace('&lt;',   '<',      $theme_data_now);
	$theme_data_now = str_replace('&gt;',   '>',      $theme_data_now);
	$theme_data_now = str_replace('\\n',   '&#13;',   $theme_data_now);
	//2012/05/09 K.Fujii upd(e)

	//2012/05/09 K.Fujii upd(s)
	//$theme_data_after = str_replace('"',    '&quot;', $theme_data_after);
	//$theme_data_after = str_replace('<',    '&lt;',   $theme_data_after);
	//$theme_data_after = str_replace('>',    '&gt;',   $theme_data_after);
	//$theme_data_after = str_replace('\\\\', '\\',     $theme_data_after);
	$theme_data_after = str_replace('\'\'',   '&#039;', $theme_data_after);
	$theme_data_after = str_replace('"',      '&quot;', $theme_data_after);
	$theme_data_after = str_replace('\\\\',   '\\',     $theme_data_after);
	$theme_data_after = str_replace('&lt;',   '<',      $theme_data_after);
	$theme_data_after = str_replace('&gt;',   '>',      $theme_data_after);
	$theme_data_after = str_replace('\\n',   '&#13;',   $theme_data_after);
	//2012/05/09 K.Fujii upd(e)



/**
 * テンプレートマッピング
 */
$smarty->assign( 'cl_title'						, $cl_title								);
$smarty->assign( 'session'						, $session								);
$smarty->assign( 'mode'							, $mode									);
$smarty->assign( 'workflow_auth'				, $workflow_auth						);
$smarty->assign( 'training_id'					, $training_id							);
$smarty->assign( 'schedule_count'				, $schedule_count						);
$smarty->assign( 'theme_id_before'				, $theme_data_before['theme_id']		);
$smarty->assign( 'theme_id_now'					, $theme_data_now['theme_id']			);
$smarty->assign( 'theme_id_after'				, $theme_data_after['theme_id']			);
$smarty->assign( 'training_name'				, $training_data['training_name']		);
$smarty->assign( 'abolition_flag'				, $abolition_flag						);
$smarty->assign( 'training_purpose'				, $training_data['training_purpose']	);
$smarty->assign( 'training_opener_nm'			, $training_opener_nm					);
$smarty->assign( 'training_opener_id'			, $training_data['training_opener']		);
$smarty->assign( 'training_teacher1_nm'			, $training_teacher1_nm					);
$smarty->assign( 'training_teacher1_id'			, $training_data['training_teacher1']	);
$smarty->assign( 'training_teacher2_nm'			, $training_teacher2_nm					);
$smarty->assign( 'training_teacher2_id'			, $training_data['training_teacher2']	);
$smarty->assign( 'training_teacher3_nm'			, $training_teacher3_nm					);
$smarty->assign( 'training_teacher3_id'			, $training_data['training_teacher3']	);
$smarty->assign( 'training_teacher4_nm'			, $training_teacher4_nm					);
$smarty->assign( 'training_teacher4_id'			, $training_data['training_teacher4']	);
$smarty->assign( 'training_teacher5_nm'			, $training_teacher5_nm					);
$smarty->assign( 'training_teacher5_id'			, $training_data['training_teacher5']	);
$smarty->assign( 'level1_options_str'			, $level1_options_str					);
$smarty->assign( 'level2_options_str'			, $level2_options_str					);
$smarty->assign( 'level3_options_str'			, $level3_options_str					);
$smarty->assign( 'level4_options_str'			, $level4_options_str					);
$smarty->assign( 'level5_options_str'			, $level5_options_str					);
$smarty->assign( 'max_training_time'			, $training_data['max_training_time']	);
$smarty->assign( 'time_division_options_str'	, $time_division_options_str			);
$smarty->assign( 'training_require'				, $training_data['training_require']	);
$smarty->assign( 'training_slogan'				, $training_data['training_slogan']		);
$smarty->assign( 'training_contents'			, $training_data['training_contents']	);
$smarty->assign( 'training_action'				, $training_data['training_action']		);
$smarty->assign( 'training_support'				, $training_data['training_support']	);
// 2012/07/25 Yamagawa add(s)
$smarty->assign( 'auto_control_flg'				, $auto_control_flg						);
// 2012/07/25 Yamagawa add(e)
// 2012/11/19 Yamagawa add(s)
$smarty->assign( 'term_div_options_str'			, $term_div_options_str					);
// 2012/11/19 Yamagawa add(e)

$smarty->assign( 'theme_contents_before'		, $theme_data_before['theme_contents']	);
$smarty->assign( 'theme_method_before'			, $theme_data_before['theme_method']	);
$smarty->assign( 'theme_deadline_before'		, $theme_data_before['theme_deadline']	);
$smarty->assign( 'theme_present_before_nm'		, $theme_present_before_nm				);
$smarty->assign( 'theme_present_before_id'		, $theme_data_before['theme_present']	);

$smarty->assign( 'theme_contents_now'			, $theme_data_now['theme_contents']		);
$smarty->assign( 'theme_method_now'				, $theme_data_now['theme_method']		);
$smarty->assign( 'theme_deadline_now'			, $theme_data_now['theme_deadline']		);
$smarty->assign( 'theme_present_now_nm'			, $theme_present_now_nm					);
$smarty->assign( 'theme_present_now_id'			, $theme_data_now['theme_present']		);

$smarty->assign( 'theme_contents_after'			, $theme_data_after['theme_contents']	);
$smarty->assign( 'theme_method_after'			, $theme_data_after['theme_method']		);
$smarty->assign( 'theme_deadline_after'			, $theme_data_after['theme_deadline']	);
$smarty->assign( 'theme_present_after_nm'		, $theme_present_after_nm				);
$smarty->assign( 'theme_present_after_id'		, $theme_data_after['theme_present']	);

$smarty->assign( 'js_str'						, $js_str								);

/**
 * テンプレート出力
 */
$smarty->display(basename($_SERVER['PHP_SELF'],'.php').".tpl");

/**
 * Javascript文字列取得
 */
function get_js_str($arr_wkfwcatemst,$workflow)
{
	$str_buff[]  = "";
	foreach ($arr_wkfwcatemst as $tmp_cate_id => $arr) {
		$str_buff[] .= "\n\t\tif (cate_id == '".$tmp_cate_id."') {\n";


		 foreach($arr["wkfw"] as $tmp_wkfw_id => $tmp_wkfw_nm){
			$tmp_wkfw_nm = htmlspecialchars($tmp_wkfw_nm, ENT_QUOTES);
			$tmp_wkfw_nm = str_replace("&#039;", "\'", $tmp_wkfw_nm);
			$tmp_wkfw_nm = str_replace("&quot;", "\"", $tmp_wkfw_nm);

			$str_buff[] .= "\t\t\taddOption(obj_wkfw, '".$tmp_wkfw_id."', '".$tmp_wkfw_nm."',  '".$workflow."');\n";

		}

		$str_buff[] .= "\t\t}\n";
	}

	return implode('', $str_buff);
}

/**
 * 年オプションHTML取得
 */
function cl_get_select_years($date)
{
	ob_start();
	show_select_years(10, $date, true);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

/**
 * 月オプションHTML取得
 */
function cl_get_select_months($date)
{

	ob_start();
	show_select_months($date, true);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

/**
 * 日オプションHTML取得
 */
function cl_get_select_days($date)
{

	ob_start();
	show_select_days($date, true);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

function get_level_options($target)
{
	ob_start();
	show_level_options($target);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

function get_time_division_options($division)
{
	ob_start();
	show_time_division_options($division);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

// 2012/11/19 Yamagawa add(s)
function get_term_div_options($term_div)
{
	ob_start();
	show_term_div_options($term_div);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}
// 2012/11/19 Yamagawa add(e)

// カテゴリ名称を取得
function search_wkfwcatemst($con, $fname) {

	$sql = "select * from cl_wkfwcatemst";
	$cond = "where wkfwcate_del_flg = 'f' order by wkfw_type";
	$sel=select_from_table($con,$sql,$cond,$fname);
	if($sel==0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	return $sel;
}

// ワークフロー情報取得
function search_wkfwmst($con, $fname, $wkfw_type) {

	$sql = "select * from cl_wkfwmst";
	$cond="where wkfw_type='$wkfw_type' and wkfw_del_flg = 'f' order by wkfw_id asc";
	$sel=select_from_table($con,$sql,$cond,$fname);
	if($sel==0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	return $sel;
}

// 申請状況オプションを出力
function show_level_options($target) {

	$arr_level_nm = array("","必須","任意");
	$arr_level_id = array("0","1","2");

	for($i=0;$i<count($arr_level_nm);$i++) {

		echo("<option value=\"$arr_level_id[$i]\"");
		if($target == $arr_level_id[$i]) {
			echo(" selected");
		}
		echo(">$arr_level_nm[$i]\n");
	}
}

function show_time_division_options($division) {

	$arr_time_division_nm = array("就業時間内","就業時間外");
	$arr_time_division_id = array("1","2");

	for($i=0;$i<count($arr_time_division_nm);$i++) {

		echo("<option value=\"$arr_time_division_id[$i]\"");
		if($division == $arr_time_division_id[$i]) {
			echo(" selected");
		}
		echo(">$arr_time_division_nm[$i]\n");
	}
}

// 2012/11/19 Yamagawa add(s)
function show_term_div_options($term_div) {

	$arr_term_div_nm = array("","上期","下期");
	$arr_term_div_id = array("","1","2");

	for($i=0;$i<count($arr_term_div_nm);$i++) {

		echo("<option value=\"$arr_term_div_id[$i]\"");
		if($term_div == $arr_term_div_id[$i]) {
			echo(" selected");
		}
		echo(">$arr_term_div_nm[$i]\n");
	}

}
// 2012/11/19 Yamagawa add(e)

$log->info(basename(__FILE__)." END");
$log->shutdown();
