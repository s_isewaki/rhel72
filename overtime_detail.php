<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix タイムカード | 残業申請詳細</title>
<?
require_once("about_comedix.php");
require_once("get_values.ini");
require_once("show_clock_in_common.ini");
require_once("show_select_values.ini");
require_once("show_overtime_apply_detail.ini");
require_once("show_overtime_apply_history.ini");
require_once("application_imprint_common.ini");
require_once("timecard_bean.php");
require_once("timecard_common_class.php");
require_once("ovtm_class.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);
//タイムカード情報の取得
$timecard_bean = new timecard_bean();
$timecard_bean->select($con, $fname);

$ovtm_class = new ovtm_class($con, $fname);

// ログインユーザの職員IDを取得
$emp_id = get_emp_id($con, $session, $fname);


if($target_apply_id == "") {
	$target_apply_id = $apply_id;
}

// 承認者ワークフロー情報取得
$sql = "select ovtmcfm from ovtmapply";
$cond = "where apply_id = $target_apply_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$ovtmcfm = pg_fetch_result($sel, 0, "ovtmcfm");


// 残業申請情報を取得
// 出勤、退勤時刻は実績から取得 20121212
$sql = "select empmst.emp_lt_nm, empmst.emp_ft_nm, ovtmapply.target_date, ovtmapply.reason_id, ovtmapply.reason, ovtmapply.apply_time, ovtmapply.comment, c.end_time, ovtmapply.next_day_flag, c.start_time, ovtmapply.previous_day_flag, ovtmapply.over_start_time, ovtmapply.over_end_time, ovtmapply.over_start_next_day_flag, ovtmapply.over_end_next_day_flag, ovtmapply.reason_detail, ovtmapply.over_start_time2, ovtmapply.over_end_time2, ovtmapply.over_start_next_day_flag2, ovtmapply.over_end_next_day_flag2 ";
for ($i=3; $i<=5; $i++) {
    $sql .=	", ovtmapply.over_start_time{$i}";
    $sql .=	", ovtmapply.over_end_time{$i}";
    $sql .=	", ovtmapply.over_start_next_day_flag{$i}";
    $sql .=	", ovtmapply.over_end_next_day_flag{$i}";
}
for ($i=1; $i<=5; $i++) {
    $sql .=	", ovtmapply.rest_start_time{$i}";
    $sql .=	", ovtmapply.rest_end_time{$i}";
    $sql .=	", ovtmapply.rest_start_next_day_flag{$i}";
    $sql .=	", ovtmapply.rest_end_next_day_flag{$i}";
}
//理由追加 20150106
for ($i=2; $i<=5; $i++) {
    $sql .=	", ovtmapply.reason_id{$i}";
    $sql .=	", ovtmapply.reason{$i}";
}
$sql .= " from ovtmapply inner join empmst on ovtmapply.emp_id = empmst.emp_id ".
    "left join atdbkrslt c on c.emp_id = ovtmapply.emp_id and CAST(c.date AS varchar) = ovtmapply.target_date ";
$cond = "where ovtmapply.apply_id = $target_apply_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$apply_emp_nm = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");
$target_date = pg_fetch_result($sel, 0, "target_date");
if ($postback != "t") {
    $reason_id = pg_fetch_result($sel, 0, "reason_id");
    $reason = pg_fetch_result($sel, 0, "reason");
    $comment = pg_fetch_result($sel, 0, "comment");
    $reason_detail = pg_fetch_result($sel, 0, "reason_detail");
}
$apply_time = pg_fetch_result($sel, 0, "apply_time");
$start_time = pg_fetch_result($sel, 0, "start_time");
$next_day_flag = pg_fetch_result($sel, 0, "next_day_flag");
$end_time = pg_fetch_result($sel, 0, "end_time");
$previous_day_flag = pg_fetch_result($sel, 0, "previous_day_flag");
$over_start_time = pg_fetch_result($sel, 0, "over_start_time");
$over_end_time = pg_fetch_result($sel, 0, "over_end_time");
if ($over_end_time == "") {
	$over_end_time = $end_time;
}
$over_start_next_day_flag = pg_fetch_result($sel, 0, "over_start_next_day_flag");
$over_end_next_day_flag = pg_fetch_result($sel, 0, "over_end_next_day_flag");
$over_start_next_day_flag_checked = "";
if ($over_start_next_day_flag == 1){
	$over_start_next_day_flag_checked = "checked";
}
$over_end_next_day_flag_checked = "";
if ($over_end_next_day_flag == 1){
	$over_end_next_day_flag_checked = "checked";
}

$over_start_time2 = pg_fetch_result($sel, 0, "over_start_time2");
$over_end_time2 = pg_fetch_result($sel, 0, "over_end_time2");
$over_start_next_day_flag2 = pg_fetch_result($sel, 0, "over_start_next_day_flag2");
$over_end_next_day_flag2 = pg_fetch_result($sel, 0, "over_end_next_day_flag2");
$over_start_next_day_flag2_checked = "";
if ($over_start_next_day_flag2 == 1){
	$over_start_next_day_flag2_checked = "checked";
}
$over_end_next_day_flag2_checked = "";
if ($over_end_next_day_flag2 == 1){
	$over_end_next_day_flag2_checked = "checked";
}
//ovtmapplyをまとめて取得
$arr_ovtmapply = pg_fetch_array($sel, 0, PGSQL_ASSOC);

//個人別勤務時間帯履歴対応 20130220 start
$timecard_common_class = new timecard_common_class($con, $fname, $emp_id, $target_date, $target_date);
// 勤務実績情報を取得
$arr_result = $timecard_common_class->get_clock_in_atdbkrslt_array($con, $emp_id, $fname, "1", $target_date);

$emp_officehours = $timecard_common_class->get_officehours_emp($emp_id, $target_date, $arr_result["tmcd_group_id"], $arr_result["pattern"]);
if ($emp_officehours["office_start_time"] != "") {
    $arr_result["office_start_time"] = $emp_officehours["office_start_time"];
    $arr_result["office_end_time"] = $emp_officehours["office_end_time"];
}
//個人別勤務時間帯履歴対応 20130220 end
//残業開始が未設定、かつ、終了時刻が設定済みの場合 20120605
if ($over_start_time == "" && $end_time != "") {
	$over_start_time = $arr_result["office_end_time"];
}

// 承認情報を取得
$sql = "select empmst.emp_lt_nm, empmst.emp_ft_nm, ovtmaprv.aprv_date, ovtmaprv.aprv_comment from ovtmaprv inner join empmst on ovtmaprv.aprv_emp_id = empmst.emp_id";
$cond = "where ovtmaprv.apply_id = $target_apply_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$aprv_emp_nm = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");
$aprv_date = pg_fetch_result($sel, 0, "aprv_date");
$aprv_comment = pg_fetch_result($sel, 0, "aprv_comment");

// 残業理由一覧を取得
$sql = "select reason_id, reason from ovtmrsn";
$cond = "where del_flg = 'f' order by reason_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if ($postback != "t") {
    $start_hour = ($start_time != "") ? intval(substr($start_time, 0, 2), 10) : "";
    $start_min = substr($start_time, 2, 2);
    $end_hour = ($end_time != "") ? intval(substr($end_time, 0, 2), 10) : "";
    $end_min = substr($end_time, 2, 2);
}
$over_start_hour = substr($over_start_time, 0, 2);
$over_start_min = substr($over_start_time, 2, 2);
$over_end_hour = substr($over_end_time, 0, 2);
$over_end_min = substr($over_end_time, 2, 2);
$over_start_hour2 = substr($over_start_time2, 0, 2);
$over_start_min2 = substr($over_start_time2, 2, 2);
$over_end_hour2 = substr($over_end_time2, 0, 2);
$over_end_min2 = substr($over_end_time2, 2, 2);

//前日チェックがあるかフラグ
$previous_day_flag_checked = "";
if ($previous_day_flag == 1){
	$previous_day_flag_checked = "checked";
}
//翌日チェックがあるかフラグ
$next_day_flag_checked = "";
if ($next_day_flag == 1){
	$next_day_flag_checked = "checked";
}

$maxline = ($_POST["ovtmadd_disp_flg"] == '1') ? 5 : 2; 

$arr_rest_data = $timecard_common_class->get_office_rest_data($emp_id, $target_date, $arr_result);

$page = "overtime_detail";
$btn_str = $ovtm_class->get_btn_str($target_apply_id, $page);
$input_or_refer = ($btn_str == "") ? "refer" : "input";
//残業理由
$arr_ovtmrsn = $ovtm_class->get_ovtmrsn($arr_result["tmcd_group_id"]);
$arr_ovtm_data = $timecard_common_class->get_ovtm_month_total($emp_id, $target_date, $input_or_refer);

?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
<script type="text/javascript" src="js/focustonext.js?t=<? echo time(); ?>"></script>
<script type="text/javascript" src="js/atdbk_timecard_edit.js?t=<? echo time(); ?>"></script>
<script type="text/javascript">
<?php
if ($ovtm_class->ovtm_reason2_input_flg == "t") {
	echo("resizeTo(1240, 680);\n");
}
?>
var arr_over_no_apply_flag = new Array();
<?
foreach ($arr_ovtmrsn as $i => $arr_info) {
    
    echo "arr_over_no_apply_flag[{$arr_info["reason_id"]}] = '{$arr_info["over_no_apply_flag"]}';\n"; 
}

?>
function setOtherReasonDisabled(item_idx) {
	var reason_id_nm = 'reason_id'+item_idx;
	var other_reason_nm = 'other_reason'+item_idx;
	
	if (document.getElementById(other_reason_nm)) {
		document.getElementById(other_reason_nm).style.display = (document.getElementById(reason_id_nm).value == 'other') ? '' : 'none';
	}
}

function history_select(apply_id, target_apply_id) {

	document.mainform.postback.value = '';
	document.mainform.apply_id.value = apply_id;
	document.mainform.target_apply_id.value = target_apply_id;
	document.mainform.action="overtime_detail.php?session=<?=$session?>";
	document.mainform.submit();
}

//休憩不足時間（分）
var rest_fusoku_min = 0;
//残業限度超時間（メッセージ用）
var over_limit_str = '';

var msg = '';
function updateApply() {
    //36協定残業限度時間
var ovtm_limit_month = <?
$limit_month = ($ovtm_class->ovtm_limit_month == '') ? 0 : $ovtm_class->ovtm_limit_month;
echo $limit_month;
    ?>;
    //申請不要時理由確認
    //その他はチェックなしで登録
    if (document.mainform.reason_id.value != 'other') {
        var reason_id = document.mainform.reason_id.value;
 	    // 申請ボタンが押された場合
        //if (document.mainform.no_apply.value != 'y') {
    		//理由の確認（不要以外）
		    if (arr_over_no_apply_flag[reason_id] == "t") {
                alert('残業申請の場合には、残業申請不要理由以外を選択してください。');
                return;
            }
        //}
    }
	//理由
    if (!document.mainform.reason_id2 &&
		document.mainform.reason_id.value == 'other') {
        if (document.mainform.reason.value == '') {
            alert('理由が入力されていません。');
            return;
        }
    }
    //休憩時間、残業時間チェック
    chk_flg = checkTime(2);
	if(chk_flg)
	{
        if (chk_flg == 1) {
            return false;
        }
		if (!confirm(msg))
		{
			return;
		}
    	document.mainform.postback.value = 't';
	    document.mainform.action = 'overtime_update_exe.php';
	    document.mainform.submit();
	}
	else if (confirm('更新します。よろしいですか？')) {
        document.mainform.postback.value = 't';
		document.mainform.action = 'overtime_update_exe.php';
		document.mainform.submit();
	}
}

function deleteApply() {
	if (confirm('削除します。よろしいですか？')) {
		document.mainform.action = 'overtime_delete.php';
		document.mainform.submit();
	}
}

// テキストエリア行数の自動拡張
var ResizingTextArea = Class.create();

ResizingTextArea.prototype = {
    defaultRows: 1,

    initialize: function(field)
    {
        this.defaultRows = Math.max(field.rows, 1);
        this.resizeNeeded = this.resizeNeeded.bindAsEventListener(this);
        Event.observe(field, "click", this.resizeNeeded);
        Event.observe(field, "keyup", this.resizeNeeded);
    },

    resizeNeeded: function(event)
    {
        var t = Event.element(event);
        var lines = t.value.split('\n');
        var newRows = lines.length + 1;
        var oldRows = t.rows;
        for (var i = 0; i < lines.length; i++)
        {
            var line = lines[i];
            if (line.length >= t.cols) newRows += Math.floor(line.length / t.cols);
        }
        if (newRows > t.rows) t.rows = newRows;
        if (newRows < t.rows) t.rows = Math.max(this.defaultRows, newRows);

        // 履歴表示table高さ調整
        resize_history_tbl();
    }
}

// 全テキストエリアの行数変更
function resizeAllTextArea() {
	var objs = document.getElementsByTagName('textarea');
	for (var i = 0, j = objs.length; i < j; i++) {
	    var t = objs[i];
		var defaultRows = Math.max(t.rows, 1);
	    var lines = t.value.split('\n');
	    var newRows = lines.length + 1;
	    var oldRows = t.rows;
	    for (var k = 0; k < lines.length; k++)
	    {
	        var line = lines[k];
	        if (line.length >= t.cols) newRows += Math.floor(line.length / t.cols);
	    }
	    if (newRows > t.rows) t.rows = newRows;
	    if (newRows < t.rows) t.rows = Math.max(defaultRows, newRows);
	}
}


// 履歴表示tableの高さ調節
function resize_history_tbl()
{
    document.getElementById('history_tbl').style.height = document.getElementById('dtl_tbl').offsetHeight;
}
function ovtmAddDisp(flg) {
    var timedisp = (flg == true) ? 'none' : '';
    var ovtmadddisp = (flg == true) ? '' : 'none';
    var val = (flg) ? '1' : '0';
    document.mainform.ovtmadd_disp_flg.value = val;
    document.mainform.action = 'overtime_detail.php';
    document.mainform.submit();
    return false;
}

<? //翌日フラグチェック ?>
var base_time = '<?
$base_time = $arr_result["office_end_time"];
if ($arr_result["office_end_time"] < $arr_result["office_start_time"]) {
    $base_time = $arr_result["office_start_time"];  
}
$base_time = str_replace(":", "", $base_time);
echo $base_time; ?>';

    //労働基準法の休憩時間が取れていない場合は警告する
    var rest_check_flg = '<? echo $ovtm_class->rest_check_flg; ?>';
    //36協定の残業限度時間をこえたら警告する
    var ovtm_limit_check_flg = '<? echo $ovtm_class->ovtm_limit_check_flg; ?>';
 
    //行数
    var maxline = <? echo $maxline; ?>;
    //休憩時間チェック
    var kinmu_total_min = 0;
    var rest_total_min = 0;
    var office_time_min = <? echo $arr_rest_data["office_time_min"]; ?>;
    var rest_time_min = <? echo $arr_rest_data["rest_time_min"]; ?>;
    
    //36協定残業限度時間
var ovtm_limit_month = <?
$limit_month = ($ovtm_class->ovtm_limit_month == '') ? 0 : $ovtm_class->ovtm_limit_month;
echo $limit_month;
    ?>;
    var ovtm_limit_month_min = ovtm_limit_month * 60;
    //当月残業累計時間申請中含む
    var ovtm_approve_apply_min = <? echo $arr_ovtm_data["ovtm_approve_apply_min"]; ?>;

    //当月残業累計時間承認のみ
    var ovtm_approve_min = <? echo $arr_ovtm_data["ovtm_approve_min"]; ?>;

    var ovtm_rest_disp_flg = '<? echo $ovtm_class->ovtm_rest_disp_flg; ?>';

    var rest_disp_flg = '<? echo $ovtm_class->rest_disp_flg; ?>';

    var over_time_apply_type = '<? echo $timecard_bean->over_time_apply_type; ?>';

    var no_overtime = '<? echo $no_overtime; ?>';

    var apply_update_flg = '2'; //1:申請 2:更新

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}

table.block {border-collapse:collapse;}
table.block td {border:#5279a5 solid 1px;padding:1px;}
table.block td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#5279a5 solid 0px;}


table.block2 {border-collapse:collapse;}
table.block2 td {border:#5279a5 solid 1px;padding:1px;}
table.block2 td {border-width:1;}

table.block3 {border-collapse:collapse;}
table.block3 td {border:#5279a5 solid 0px;}
table.block3 td {border-width:0;}

p {margin:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="setOtherReasonDisabled('');resizeAllTextArea();resize_history_tbl();document.onkeydown=focusToNext;document.mainform.over_start_hour.focus();<?php
if ($ovtm_class->ovtm_reason2_input_flg == "t") {
	for ($i=2; $i<=5; $i++) {
		echo "setOtherReasonDisabled('$i');";
	}
}
if ($ovtm_class->ovtm_reason2_input_flg == "t") {
	echo("resizeTo(1240, 680);\n");
}
?>">
<center>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>残業申請詳細</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="page_close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form name="mainform" action="overtime_update_exe.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="left" width="145">
</td>
<td align="left">
<? //タブを追加
if ($ovtm_class->ovtm_tab_flg == "t") {
?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<? if ($ovtmadd_disp_flg == '1') { ?>
<a href="javascript:void(0);" onclick="return ovtmAddDisp(false);">
<? } ?>
残業申請詳細
<? if ($ovtmadd_disp_flg == '1') { ?>
</a>
<? } ?>
&nbsp;
<? if ($ovtmadd_disp_flg != '1') { ?>
<a href="javascript:void(0);" onclick="return ovtmAddDisp(true);">
<? } ?>
残業追加
<? if ($ovtmadd_disp_flg != '1') { ?>
</a>
<? } ?>
</font>
<? } ?>
</td>
<td align="right">
<? echo $btn_str; ?>
</td>
</tr>
</table>


<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td valign="top" width="140"> <? // 25% ?>
<? show_overtime_history_for_apply($con, $session, $fname, $apply_id, $target_apply_id);?>
</td>
<td><img src="img/spacer.gif" width="5" height="2" alt=""></td>
<td valign="top" width=""> <? // 75% ?>
<div id="dtl_tbl">
<?php
$colspan_num = ($ovtm_class->ovtm_reason2_input_flg == "t") ? "2" : "1";
 ?>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<? // width="180" ?>
<td width="90" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請日時</font></td>
<td colspan="<?php echo $colspan_num; ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})\d{2}$/", "$1/$2/$3 $4:$5", $apply_time)); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務日</font></td>
<td colspan="<?php echo $colspan_num; ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $target_date)); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">始業時刻</font></td>
<td colspan="<?php echo $colspan_num; ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(preg_replace("/^(\d{2})(\d{2})$/", "$1:$2", $arr_result["office_start_time"])); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤時刻</font></td>
<td colspan="<?php echo $colspan_num; ?>">
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10">
    <?
    if ($ovtm_class->keyboard_input_flg == "t") { 
        ?>
        <input type="text" name="start_hour"  id="start_hour" value="<? echo $start_hour; ?>" size="2" maxlength="2" style="ime-mode:inactive;" onkeydown="nextElement='start_min';backElement='<?
        //
        $backElement =($ovtm_class->ovtm_rest_disp_flg == "t") ? "rest_end_min$maxline" : "over_end_min$maxline";
        echo $backElement;
              ?>';">：<input type="text" name="start_min"  id="start_min" value="<? echo $start_min; ?>" size="2" maxlength="2" style="ime-mode:inactive;" onkeydown="nextElement='end_hour';backElement='start_hour';">
        <?
    }
    else {
    ?>
		<select name="start_hour" id="start_hour"><? show_hour_options_0_23($start_hour, true); ?></select>：<select name="start_min" id="start_min"><? show_min_options_00_59($start_min, true); ?></select>
        <? } ?>
		<label for="zenjitsu"><input type="checkbox" name="previous_day_flag" id="zenjitsu" <?=$previous_day_flag_checked ?> value="1">前日</label>
	</font>
<?
//打刻 20140624
?>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
if ($ovtmadd_disp_flg != '1') {
    echo ("　　打刻&nbsp;");
    echo($ovtm_class->get_btn_date_format($arr_result["start_btn_date1"]));
    $tmp_start_btn_time = $ovtm_class->get_btn_time_format($arr_result["start_btn_time"]);
    echo($tmp_start_btn_time);
    if ($arr_result["start_btn_time2"] != "") {
        echo ("　　　出勤2&nbsp;");
	    echo($ovtm_class->get_btn_date_format($arr_result["start_btn_date2"]));
        $tmp_start_btn_time2 = $ovtm_class->get_btn_time_format($arr_result["start_btn_time2"]);
        echo($tmp_start_btn_time2);
    }
}
    ?>
	</font>
</td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">終業時刻</font></td>
<td colspan="<?php echo $colspan_num; ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(preg_replace("/^(\d{2})(\d{2})$/", "$1:$2", $arr_result["office_end_time"])); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退勤時刻</font></td>
<td colspan="<?php echo $colspan_num; ?>">
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10">
    <?
    if ($ovtm_class->keyboard_input_flg == "t") { 
        ?>
            <input type="text" name="end_hour"  id="end_hour" value="<? echo $end_hour; ?>" size="2" maxlength="2" style="ime-mode:inactive;" onkeydown="nextElement='end_min';backElement='start_min';">：<input type="text" name="end_min"  id="end_min" value="<? echo $end_min; ?>" size="2" maxlength="2" style="ime-mode:inactive;" onkeydown="nextElement='over_start_hour';backElement='end_hour';">
        <?
    }
    else {
    ?>
		<select name="end_hour" id="end_hour"><? show_hour_options_0_23($end_hour, true); ?></select>：<select name="end_min" id="end_min"><? show_min_options_00_59($end_min, true); ?></select>
        <? } ?>
		<label for="yokujitsu"><input type="checkbox" name="next_day_flag" id="yokujitsu" <?=$next_day_flag_checked ?> value="1">翌日</label>
	</font>
<?
//打刻 20140624
?>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
if ($ovtmadd_disp_flg != '1') {
    echo ("　　打刻&nbsp;");
    echo($ovtm_class->get_btn_date_format($arr_result["end_btn_date1"]));
    $tmp_end_btn_time = $ovtm_class->get_btn_time_format($arr_result["end_btn_time"]);
    echo($tmp_end_btn_time);
    if ($arr_result["end_btn_time2"] != "") {
        echo ("　　　退勤2&nbsp;");
	    echo($ovtm_class->get_btn_date_format($arr_result["end_btn_date2"]));
        $tmp_end_btn_time2 = $ovtm_class->get_btn_time_format($arr_result["end_btn_time2"]);
        echo($tmp_end_btn_time2);
    }
}
    ?>
	</font>
</td>
</tr>
<?
$ovtm_class->disp_btn_time_data($arr_result, "input");
$arr_default_time = array();
$ovtm_class->disp_input_form($arr_result, $timecard_bean, $arr_default_time, $apply_status, $arr_ovtmapply);
$ovtm_class->disp_ovtm_total_time($arr_ovtm_data);

//残業時刻2以降にも理由を入力する場合は既存の理由を表示しない 20150105
if ($ovtm_class->ovtm_reason2_input_flg != "t") {
?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">理由</font></td>
<td colspan="<?php echo $colspan_num; ?>">
<select name="reason_id" id="reason_id" onchange="setOtherReasonDisabled('');" onblur="document.onkeydown=focusToNext;" onfocus="document.onkeydown='';">
<?
$str = $ovtm_class->get_ovtmrsn_option($arr_result["tmcd_group_id"], $reason_id);
echo $str;
?>
<option value="other"<? if ($reason_id == "" || $reason_id == "other") {echo(" selected");} ?>>その他</option>
</select><br>
<div id="other_reason" style="display:none;"><input type="text" name="reason" value="<? echo($reason); ?>" size="50" maxlength="100" style="ime-mode:active;" onblur="document.onkeydown=focusToNext;" onfocus="document.onkeydown='';"></div>
</td>
</tr>
<?php } ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">理由詳細</font></td>
<td colspan="<?php echo $colspan_num; ?>"><textarea name="reason_detail" rows="5" cols="40" style="ime-mode:active;" onFocus="new ResizingTextArea(this);document.onkeydown='';" onblur="document.onkeydown=focusToNext;"><? echo($reason_detail); ?></textarea></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請者コメント</font></td>
<td colspan="<?php echo $colspan_num; ?>"><textarea name="comment" rows="5" cols="40" style="ime-mode:active;" onFocus="new ResizingTextArea(this);document.onkeydown='';" onblur="document.onkeydown=focusToNext;"><? echo($comment); ?></textarea></td>
</tr>

<?
    show_application_apply_detail($con, $session, $fname, $target_apply_id, $page, "", "", "", "", $ovtm_class);
?>

<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="apply_id" value="<? echo($apply_id); ?>">
<input type="hidden" name="target_apply_id" value="<? echo($target_apply_id); ?>">
<input type="hidden" name="pnt_url" value="<? echo($pnt_url); ?>">
<input type="hidden" name="pre_start_time" value="<? echo($arr_result["start_time"]); ?>">
<input type="hidden" name="pre_previous_day_flag" value="<? echo($arr_result["previous_day_flag"]); ?>">
<input type="hidden" name="pre_end_time" value="<? echo($arr_result["end_time"]); ?>">
<input type="hidden" name="pre_next_day_flag" value="<? echo($arr_result["next_day_flag"]); ?>">
<input type="hidden" name="ovtmcfm" value="<? echo($ovtmcfm); ?>">

<input type="hidden" name="approve" value="">
<input type="hidden" name="drag_flg" value="">

<input type="hidden" name="apv_print_comment" value="">
<input type="hidden" name="pre_over_start_time" value="<? echo($arr_result["over_start_time"]); ?>">
<input type="hidden" name="pre_over_end_time" value="<? echo($arr_result["over_end_time"]); ?>">
<input type="hidden" name="pre_over_start_next_day_flag" value="<? echo($arr_result["over_start_next_day_flag"]); ?>">
<input type="hidden" name="pre_over_end_next_day_flag" value="<? echo($arr_result["over_end_next_day_flag"]); ?>">
<input type="hidden" name="pre_over_start_time2" value="<? echo($arr_result["over_start_time2"]); ?>">
<input type="hidden" name="pre_over_end_time2" value="<? echo($arr_result["over_end_time2"]); ?>">
<input type="hidden" name="pre_over_start_next_day_flag2" value="<? echo($arr_result["over_start_next_day_flag2"]); ?>">
<input type="hidden" name="pre_over_end_next_day_flag2" value="<? echo($arr_result["over_end_next_day_flag2"]); ?>">
<input type="hidden" name="keyboard_input_flg" value="<? echo($ovtm_class->keyboard_input_flg); ?>">
<?/* 追加 */
for ($i=3; $i<=5; $i++) {
    $s_t = "over_start_time".$i;
    $e_t = "over_end_time".$i;
    $s_f = "over_start_next_day_flag".$i;
    $e_f = "over_end_next_day_flag".$i;
?>
<input type="hidden" name="pre_<? echo $s_t; ?>" value="<? echo($arr_result[$s_t]); ?>">
<input type="hidden" name="pre_<? echo $e_t; ?>" value="<? echo($arr_result[$e_t]); ?>">
<input type="hidden" name="pre_<? echo $s_f; ?>" value="<? echo($arr_result[$s_f]); ?>">
<input type="hidden" name="pre_<? echo $s_f; ?>" value="<? echo($arr_result[$s_f]); ?>">
    <?
}    
for ($i=1; $i<=5; $i++) {
    $s_t = "rest_start_time".$i;
    $e_t = "rest_end_time".$i;
    $s_f = "rest_start_next_day_flag".$i;
    $e_f = "rest_end_next_day_flag".$i;
?>
<input type="hidden" name="pre_<? echo $s_t; ?>" value="<? echo($arr_result[$s_t]); ?>">
<input type="hidden" name="pre_<? echo $e_t; ?>" value="<? echo($arr_result[$e_t]); ?>">
<input type="hidden" name="pre_<? echo $s_f; ?>" value="<? echo($arr_result[$s_f]); ?>">
<input type="hidden" name="pre_<? echo $s_f; ?>" value="<? echo($arr_result[$s_f]); ?>">
    <?
}    
?>
<input type="hidden" name="postback" value="t">
<input type="hidden" name="ovtmadd_disp_flg" value="<? echo($ovtmadd_disp_flg); ?>">

</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="left" width="145">
</td>
<td align="left">
</td>
<td align="right">
<? echo $btn_str; ?>
</td>
</tr>
</table>

</form>
</center>
</body>

</html>
<?
$update_cnt = 0;
if($mode == 'show_flg_update') {

	// トランザクションを開始
	pg_query($con, "begin");

    // 一部承認の場合
	$sql = "select count(apply_id) as cnt from ovtmaprv";
	$cond = "where apply_id = $apply_id and aprv_status = '1' and apv_fix_show_flg = 't'";

	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

	$update_cnt = pg_fetch_result($sel, 0, "cnt");

	if ($update_cnt > 0) {
		$sql = "update ovtmaprv set";
		$set = array("apv_fix_show_flg");
		$setvalue = array("f");
		$cond = "where apply_id = $apply_id and apv_fix_show_flg = 't' and aprv_status = '1'";	
		$up = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if($up ==0){
			pg_exec($con,"rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

	// トランザクションをコミット
	pg_query($con, "commit");
}

pg_close($con);
?>
<script type="text/javascript">
function page_close() {
<?
if($mode == 'show_flg_update' && $update_cnt > 0) {
?>	
	if(window.opener && !window.opener.closed && window.opener.reload_page) {
		window.opener.reload_page();
		window.close();
	} else {
		window.close();
	}
<?	
} else {
?>	
	window.close();
<?
}
?> 

}
</script>