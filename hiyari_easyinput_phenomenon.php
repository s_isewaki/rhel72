<?php
require_once('Cmx.php');
require_once('Cmx/View/Smarty.php');
require_once("about_session.php");
require_once("about_authority.php");
require_once("hiyari_common.ini");

//====================================================================================================
//前処理
//====================================================================================================
//ページ名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
    showLoginPage();
    exit;
}

// 権限チェック
$hiyari = check_authority($session, 47, $fname);
if ($hiyari == "0") {
    showLoginPage();
    exit;
}

//====================================================================================================
//表示
//====================================================================================================
$smarty = new Cmx_View();

$smarty->assign("session", $session);
$smarty->assign("INCIDENT_TITLE", $INCIDENT_TITLE);

if ($grp_code != ''){
    $smarty->assign("grp_code", $grp_code);    
}

if ($phenomenon != ''){
    if ( preg_match("/【当事者】(.*)【関与者】(.*)【患者】(.*)/s", $phenomenon, $matches) ){
        $tojisha = trim($matches[1]);
        $kanyosha = trim($matches[2]);
        $patient = trim($matches[3]);
        $smarty->assign("tojisha", $tojisha);
        $smarty->assign("kanyosha", $kanyosha);
        $smarty->assign("patient", $patient);
    }
}

//表示
$smarty->display("hiyari_easyinput_phenomenon.tpl");
