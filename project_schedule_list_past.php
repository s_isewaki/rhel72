<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>委員会・WG | スケジュール一覧</title>
<?
$fname=$PHP_SELF;
require("conf/sql.inf");						//sqlの一覧ファイルを読み込む
require("about_postgres.php");			//db処理のためのファイルを読み込む
require("show_select_values.ini");
require("about_session.php");
require("about_authority.php");
require("show_schedule_to_do.ini");
require("time_check3.ini");
require("show_project_schedule_common.ini");
require("show_project_schedule_chart.ini");

$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
$empif = check_authority($session,31,$fname);
if($empif == "0"){
	echo("<script type='text/javascript' src='js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

if(!isset($_REQUEST['date'])){
	$date = mktime(0,0,0,date('m'), date('d'), date('Y'));
}
else
{
	$date = $_REQUEST['date'];
}
$day = date('d', $date);
$month = date('m', $date);
$year = date('Y', $date);
$todayday = date('d');
$todaymonth = date('m');
$todayyear = date('Y');

$con = connect2db($fname);
$cond = " where session_id='$session'";
$sel = select_from_table($con,$SQL1,$cond,$fname);

if($sel==0)
{
	echo("<script type='text/javascript' src='js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$emp_id=pg_result($sel,0,"emp_id");


//--0302追加ここから---
$cond_pjt = " where pjt_id='$pjt_id'";
$sel_pjt = select_from_table($con,$SQL165,$cond_pjt,$fname);

if($sel_pjt==0){
pg_close($con);
echo("<script type='text/javascript' src='js/showpage.js'></script>");
echo("<script language='javascript'>showErrorPage(window);</script>");
exit;
}

$project_name=pg_result($sel_pjt,0,"pjt_name");
//--0302追加ここまで---

if($timegd == "on" || $timegd == "off"){
time_update($con,$emp_id,$timegd,$fname);
}

if($timegd == ""){
$timegd = time_check($con,$emp_id,$fname);
}
//--0217追加ここまで---

// 開始時刻のデフォルトは9時
if ($hr == "") {
$hr = "9";
}

echo("<script language=\"javascript\">\n");
echo("function showGuide(){\n");
echo("var ses = \"".$session."\";\n");
$dt = mktime(0,0,0,$month,$day,$year);
echo("var dt  = ".$dt.";\n");
echo("var pjt_id  = ".$pjt_id.";\n");
echo("if(document.timegd.timeguide.checked == true){\n");
echo("var url = \"project_schedule_list_past.php?session=\"+ses+\"&pjt_id=\"+pjt_id+\"&date=\"+dt+\"&timegd=on\";\n");
echo("location.href=url;\n");
echo("}else{\n");
echo("var url = \"project_schedule_list_past.php?session=\"+ses+\"&pjt_id=\"+pjt_id+\"&date=\"+dt+\"&timegd=off\";\n");
echo("location.href=url;\n");
echo("}\n");
echo("}\n");
echo("</script>\n");

// オプション設定情報を取得
$sql = "select calendar_start2 from option";
$cond = "where emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$calendar_start = pg_fetch_result($sel, 0, "calendar_start2");

// ログインユーザが委員会責任者かどうかチェック
$sql = "select count(*) from project";
$cond = "where pjt_id = $pjt_id and pjt_response = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$response_flg = (intval(pg_fetch_result($sel, 0, 0)) > 0);

// ログインユーザが事務局メンバーかどうかチェック
$sql = "select count(*) from promember";
$cond = "where pjt_id = $pjt_id and member_kind = '1' and emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$secretariat_flg = (intval(pg_fetch_result($sel, 0, 0)) > 0);

// 委員会・WK管理権限を取得
$pjt_admin_auth = check_authority($session, 82, $fname);

?>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" valign="middle" colspan="2" class="spacing"><a href="project_menu.php?session=<? echo($session); ?>"><img src="img/icon/b03.gif" width="32" height="32" border="0" alt="委員会・WG"></a></td>

<? if ($pjt_admin_auth == "1") { ?>

	<? if($adm_flg=="t") {?>
		<td width="85%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="project_menu.php?session=<? echo($session); ?>"><b>委員会・WG</b></a></font></td>
		<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="project_menu.php?session=<? echo($session); ?>&entity=1"><b>ユーザ画面へ</b></a></font></td>
	<? }else{ ?>
		<td width="85%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="project_menu.php?session=<? echo($session); ?>"><b>委員会・WG</b></a></font></td>
		<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">　<a href="project_menu_adm.php?session=<? echo($session); ?>&entity=1"><b>管理画面へ</b></a></font></td>
	<? } ?>

<? }else{ ?>
		<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="project_menu.php?session=<? echo($session); ?>"><b>委員会・WG</b></a></font></td>

<? } ?>


</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>

<? if($adm_flg=="t") {?>
	<td width="120" height="22" align="center" bgcolor="#bdd1e7"><a href="project_menu_adm.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">委員会・WG一覧</font></a></td>
<? }else{ ?>
	<td width="120" height="22" align="center" bgcolor="#bdd1e7"><a href="project_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">委員会・WG一覧</font></a></td>
<? } ?>

<td width="5"></td>
<td width="120" align="center" bgcolor="#5279a5"><a href="project_schedule_list_past.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo(mktime(0,0,0,$month,$day,$year)); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>スケジュール</b></font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5" colspan="10"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td height="22"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b><? echo($project_name); ?></b></font></td>
</tr>
</table>
<form name="timegd">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="70" align="center" bgcolor="#bdd1e7"><a href="project_schedule_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo(mktime(0,0,0,$month,$day,$year)); ?>&adm_flg=<?=$adm_flg?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="project_schedule_week_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo(mktime(0,0,0,$month,$day,$year)); ?>&adm_flg=<?=$adm_flg?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">週</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="project_schedule_month_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo(mktime(0,0,0,$month,$day,$year)); ?>&adm_flg=<?=$adm_flg?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">月</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="project_schedule_year_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo($year); ?>&s_date=<? echo(mktime(0,0,0,$month,$day,$year)); ?>&adm_flg=<?=$adm_flg?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">年</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#5279a5"><a href="project_schedule_list_upcoming.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo(mktime(0,0,0,$month,$day,$year)); ?>&adm_flg=<?=$adm_flg?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>一覧</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="project_schedule_option.php?session=<? echo($session); ?>&date=<? echo(mktime(0,0,0,$month,$day,$year)); ?>&pjt_id=<? echo($pjt_id); ?>&adm_flg=<?=$adm_flg?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="80" height="22" align="center"><a href="project_schedule_list_all.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo(mktime(0,0,0,$month,$day,$year)); ?>&adm_flg=<?=$adm_flg?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">予定一覧</font></a></td>
<td width="5"></td>
<td width="80" align="center"><a href="project_schedule_list_upcoming.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo(mktime(0,0,0,$month,$day,$year)); ?>&adm_flg=<?=$adm_flg?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">予定今後</font></a></td>
<td width="5"></td>
<td width="80" height="22" align="center" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>予定過去</b></font></td>
<td width="">&nbsp;</td>
</tr>
</table>
</form>
<table width="100%" border="1" cellpadding="0" cellspacing="0">
<?
// スケジュール一覧を取得
$today = date("Y-m-d");
$sql = "select proschd.pjt_schd_id, proschd.pjt_schd_title, proschd.pjt_schd_type, proschd.pjt_schd_start_time, proschd.pjt_schd_dur, proschd.pjt_schd_start_date, proschd.pjt_schd_start_time_v, proschd.pjt_schd_dur_v, proschd.emp_id, proceeding.prcd_create_emp_id from proschd left join proceeding on proceeding.pjt_schd_id = proschd.pjt_schd_id where proschd.pjt_id = '$pjt_id' and proschd.pjt_schd_start_date < '$today' union select proschd2.pjt_schd_id, proschd2.pjt_schd_title, proschd2.pjt_schd_type, null, null, proschd2.pjt_schd_start_date, null, null, proschd2.emp_id, proceeding.prcd_create_emp_id from proschd2 left join proceeding on proceeding.pjt_schd_id = proschd2.pjt_schd_id where proschd2.pjt_id = '$pjt_id' and proschd2.pjt_schd_start_date < '$today' order by 6, 4";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// スケジュール一覧を表示
while ($row = pg_fetch_array($sel)) {
	$schd_id = $row["pjt_schd_id"];
	$schd_title = $row["pjt_schd_title"];
	$schd_type = $row["pjt_schd_type"];
	$schd_start_date = $row["pjt_schd_start_date"];
	$schd_start_time = substr($row["pjt_schd_start_time_v"],0,2).":".substr($row["pjt_schd_start_time_v"],2,2);
	$schd_dur = substr($row["pjt_schd_dur_v"],0,2).":".substr($row["pjt_schd_dur_v"],2,2);
	$schd_emp_id = $row["emp_id"];
	$schd_prcd_emp_id = $row["prcd_create_emp_id"];
	$color = get_schedule_color($schd_type);

	$tmp_date = mktime(0, 0, 0, substr($schd_start_date, 5, 2), substr($schd_start_date, 8, 2), substr($schd_start_date, 0, 4));
	if ($schd_start_time != ":" && $schd_dur != ":") {
		$timeless = "";
		$formatted_time = substr($schd_start_time, 0, 5) . "-" . substr($schd_dur, 0, 5);
	} else {
		$timeless = "t";
		$formatted_time = "指定なし";
	}
	$cur_url = "project_schedule_list_past.php?session=$session&pjt_id=$pjt_id&date=$date&adm_flg=$adm_flg";

	echo("<tr height=\"22\" bgcolor=\"$color\">\n");
	echo("<td width=\"100\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"project_schedule_menu.php?session=$session&pjt_id=$pjt_id&adm_flg=$adm_flg&date=$tmp_date\">$schd_start_date</a></font></td>\n");
	echo("<td width=\"100\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$formatted_time</font></td>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"javascript:void(0);\" onclick=\"window.open('project_schedule_update.php?session=$session&pjt_schd_id=$schd_id&timeless=$timeless&pjt_id=$pjt_id&adm_flg=$adm_flg&date=$tmp_date&time=$schd_start_time', 'newwin', 'width=640,height=480,scrollbars=yes');\">$schd_title</a></font></td>\n");
	echo("<td width=\"160\" align=\"center\">" . get_prcd_btn_html($con, $schd_id, $timeless, $emp_id, $session, $cur_url, $fname) . "</td>\n");
	echo("<td width=\"50\" align=\"center\"><input type=\"button\" value=\"削除\" onclick=\"if(confirm('削除してよろしいですか？')){location.href = 'project_schedule_delete.php?session=$session&pjt_schd_id=$schd_id&path=3&pjt_id=$pjt_id&adm_flg=$adm_flg&date=$date';}\"");
	if ($schd_emp_id != $emp_id && $schd_prcd_emp_id != $emp_id && !$response_flg && !$secretariat_flg && $adm_flg!="t") {
		echo(" disabled");
	}
	echo("></td></tr>\n");
	echo("</tr>\n");
}
?>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#bdd1e7"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#bdd1e7"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width=""><? show_proschd_type_color_list($con, $fname); ?></td>
<td width="1" bgcolor="#bdd1e7"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#bdd1e7"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
