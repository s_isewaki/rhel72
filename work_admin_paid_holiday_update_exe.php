<?
//ini_set("display_errors","1");
require("about_session.php");
require("about_authority.php");
require("get_values.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

$arr_emp_id = split(",", $emp_ids);

// 入力チェック
for ($i=0; $i<$data_cnt; $i++) {
	$var_name = "days1_".$i;
	if ($$var_name != "") {
		if (preg_match("/^\d$|^\d+\.?\d+$/", $$var_name) == 0) {
			$emp_name = get_emp_kanji_name($con, $arr_emp_id[$i], $fname);
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('{$emp_name}さんの繰越日数を半角数字で入力してください。');</script>");
			echo("<script type=\"text/javascript\">history.back();</script>");
			exit;
		}
	}
	
	$var_name = "days2_".$i;
	if ($$var_name != "") {
		if (preg_match("/^\d$|^\d+\.?\d+$/", $$var_name) == 0) {
			$emp_name = get_emp_kanji_name($con, $arr_emp_id[$i], $fname);
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('{$emp_name}さんの付与日数を半角数字で入力してください。');</script>");
			echo("<script type=\"text/javascript\">history.back();</script>");
			exit;
		}
	}
 	
	$var_name = "adjust_".$i;
	if ($$var_name != "") {
//		if (preg_match("/^-?\d{1,3}$/", $$var_name) == 0) {
		if (preg_match("/^-?\d$|^-?\d+\.?\d+$/", $$var_name) == 0) {
			$emp_name = get_emp_kanji_name($con, $arr_emp_id[$i], $fname);
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('{$emp_name}さんの調整日数を半角数字で入力してください。マイナスも入力できます。');</script>");
			echo("<script type=\"text/javascript\">history.back();</script>");
			exit;
		}
	}
	
}
$sql = "select closing, closing_parttime, closing_paid_holiday, criteria_months from timecard";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$closing_paid_holiday = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "closing_paid_holiday") : "1";
//基準月数
$criteria_months = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "criteria_months") : "2";

// トランザクションを開始
pg_query($con, "begin transaction");

//情報取得
/*
$emp_id_str = "";
for ($i=0; $i<count($arr_emp_id); $i++) {
	if ($i>0) {
		$emp_id_str .= ",";
	}
	$emp_id_str .= "'$arr_emp_id[$i]'";
}
*/
$cond_str = "";
$first_flg = true;
for ($i=0; $i<count($arr_emp_id); $i++) {
	$var_name = "add_yyyy_".$i;
	$wk_add_yyyy = $$var_name;
	if ($wk_add_yyyy != "") {
		if (!$first_flg) {
			$cond_str .= " or ";
		}
		$cond_str .= " (emppaid.emp_id = '$arr_emp_id[$i]' and emppaid.year = '$wk_add_yyyy' ";
		if ($closing_paid_holiday == "3" && $criteria_months == "1") {
			$var_name7 = "add_mmdd_".$i;
			$paid_hol_add_mmdd = $$var_name7;
			if ($paid_hol_add_mmdd != "0000") {
				$cond_str .= " and emppaid.paid_hol_add_mmdd = '$paid_hol_add_mmdd' ";
			}
		}
		$cond_str .= " )";
		$first_flg = false;
	}
}


//更新
if ($update_flg == "1") {
	//$sql = "select emppaid.emp_id, days1, days2, days3, days4, adjust_day, empcond.paid_hol_tbl_id from emppaid ";
	$sql = "select * from emppaid ";
	//$sql .= " left join empcond on empcond.emp_id = emppaid.emp_id ";
	//$cond = "where emppaid.emp_id in ($emp_id_str) and emppaid.year = $year";
	$cond = "where $cond_str";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$arr_days = array();
	$num = pg_num_rows($sel);
	for ($i=0; $i<$num; $i++) {
		$emp_id = pg_fetch_result($sel, $i, "emp_id");
		$arr_days["$emp_id"]["days1"] = pg_fetch_result($sel, $i, "days1");
		$arr_days["$emp_id"]["days2"] = pg_fetch_result($sel, $i, "days2");
		$arr_days["$emp_id"]["days3"] = pg_fetch_result($sel, $i, "days3");
		$arr_days["$emp_id"]["days4"] = pg_fetch_result($sel, $i, "days4");
		$arr_days["$emp_id"]["adjust_day"] = pg_fetch_result($sel, $i, "adjust_day");
		$arr_days["$emp_id"]["paid_hol_tbl_id"] = pg_fetch_result($sel, $i, "paid_hol_tbl_id");
		$arr_days["$emp_id"]["paid_hol_emp_join"] = pg_fetch_result($sel, $i, "paid_hol_emp_join");
		$arr_days["$emp_id"]["paid_hol_start_date"] = pg_fetch_result($sel, $i, "paid_hol_start_date");
		$arr_days["$emp_id"]["paid_hol_add_date"] = pg_fetch_result($sel, $i, "paid_hol_add_date");
		$arr_days["$emp_id"]["service_year"] = pg_fetch_result($sel, $i, "service_year");
		$arr_days["$emp_id"]["service_mon"] = pg_fetch_result($sel, $i, "service_mon");
	}
	
	//日数が違う場合に更新
	for ($i=0; $i<count($arr_emp_id); $i++) {
		
		$var_name1 = "days1_".$i;
		$var_name2 = "days2_".$i;
		$var_name3 = "adjust_".$i;
		$wk_emp_id = $arr_emp_id[$i];
		$var_name4 = "join_".$i;
		$var_name5 = "paid_start_".$i;
		$var_name6 = "tbl_id_".$i;
		$var_name7 = "add_mmdd_".$i;
		$var_name8 = "len_service_".$i;
		$var_name9 = "add_yyyy_".$i;
		//有休表がなしの場合は処理しない
		if ($$var_name6 == "") {
			continue;
		}
		//付与日がなし(0000)の場合は処理しない
		if ($$var_name7 == "0000") {
			continue;
		}
		
		list($wk_yy, $wk_mm) = split("/", $$var_name8);
		if ($$var_name1 != $arr_days["$wk_emp_id"]["days1"] ||
				$$var_name2 != $arr_days["$wk_emp_id"]["days2"] ||
				$$var_name3 != $arr_days["$wk_emp_id"]["adjust_day"] ||
				$$var_name4 != $arr_days["$wk_emp_id"]["paid_hol_emp_join"] ||
				$$var_name5 != $arr_days["$wk_emp_id"]["paid_hol_start_date"] ||
				$$var_name6 != $arr_days["$wk_emp_id"]["paid_hol_tbl_id"] ||
				$$var_name7 != $arr_days["$wk_emp_id"]["paid_hol_add_mmdd"] ||
				$wk_yy != $arr_days["$wk_emp_id"]["service_year"] ||
				$wk_mm != $arr_days["$wk_emp_id"]["service_mon"]
			) {
			$wk_year = $$var_name9;
			//削除
			$sql = "delete from emppaid";
			$cond = "where emp_id = '$wk_emp_id' and year = $wk_year";
			if ($closing_paid_holiday == "3" && $criteria_months == "1") {
				$var_name7 = "add_mmdd_".$i;
				$paid_hol_add_mmdd = $$var_name7;
				if ($paid_hol_add_mmdd != "0000") {
					$cond .= " and paid_hol_add_mmdd = '$paid_hol_add_mmdd' ";
				}
			}
			$del = delete_from_table($con, $sql, $cond, $fname);
			if ($del == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}
			//追加
			if ($$var_name1 != "" || $$var_name2 != "" || $$var_name3 != "") {
				$sql = "insert into emppaid (emp_id, year, days1, days2, days3, days4, adjust_day, paid_hol_emp_join, paid_hol_start_date, paid_hol_tbl_id, paid_hol_add_mmdd, service_year, service_mon) values (";
				$days1 = $$var_name1;
				$days2 = $$var_name2;
				$days3 = $arr_days["$wk_emp_id"]["days3"];
				$days4 = $arr_days["$wk_emp_id"]["days4"];
				$adjust_day = $$var_name3;
				$paid_hol_emp_join = $$var_name4;
				$paid_hol_start_date = $$var_name5;
				$paid_hol_tbl_id = $$var_name6;
				$paid_hol_add_mmdd = $$var_name7;
				$content = array($wk_emp_id, $wk_year, $days1, $days2, $days3, $days4, $adjust_day, $paid_hol_emp_join, $paid_hol_start_date, $paid_hol_tbl_id, $paid_hol_add_mmdd, $wk_yy, $wk_mm);
				$ins = insert_into_table($con, $sql, $content, $fname);
				if ($ins == 0) {
					pg_query($con, "rollback");
					pg_close($con);
					echo("<script type='text/javascript' src='./js/showpage.js'></script>");
					echo("<script language='javascript'>showErrorPage(window);</script>");
					exit;
				}
			}
		}
		/* 有休表更新しない 20101122
		$var_name3 = "tbl_id_".$i;
		if ($$var_name3 != $arr_days["$wk_emp_id"]["paid_hol_tbl_id"]) {
			//update
			$sql = "update empcond set";
			$set = array("paid_hol_tbl_id");
			$setvalue = array($$var_name3);
			$cond = "where emp_id = '$wk_emp_id'";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
		*/
	}
	
}
//取消リセット
else {
	$sql = "delete from emppaid ";
	//$cond = "where emppaid.emp_id in ($emp_id_str) and emppaid.year = $year";
	$cond = "where $cond_str ";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を切断
pg_close($con);

$url_srch_name = urlencode($srch_name);

// 有休管理画面を再表示 検索条件を追加
echo("<script type=\"text/javascript\">location.href = 'work_admin_paid_holiday.php?session=$session&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&duty_form_jokin=$duty_form_jokin&duty_form_hijokin=$duty_form_hijokin&group_id=$group_id&srch_flg=$srch_flg&page=$page&select_add_yr=$select_add_yr&select_add_mon=$select_add_mon&select_add_day=$select_add_day&select_close_yr=$select_close_yr&select_close_mon=$select_close_mon&select_close_day=$select_close_day';</script>");

?>
