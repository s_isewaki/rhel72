<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_session.php");
require("about_authority.php");
require("show_date_navigation_day.ini");
require("menu_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 55, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 稼動状況統計設定権限の取得
$stat_setting_auth = check_authority($session, 52, $fname);

// 稼動状況統計設定権限の取得
$ymstat_auth = check_authority($session, 56, $fname);

// デフォルト日付はシステム日の前日
if ($date == "") {$date = get_last_day(mktime(0, 0, 0, date("m"), date("d"), date("Y")));}

// データベースに接続
$con = connect2db($fname);

// 表示対象となる管理項目・補助項目の一覧を取得
// 　補助項目
// 　　項目タイプ、管理項目ID、管理項目名、補助項目ID、補助項目名、補助項目属性、計算式
// 　補助項目のない管理項目
// 　　項目タイプ、管理項目ID、管理項目名、それ以降はnull
$sql = "select 'assist' as type, statitem.statitem_id, statitem.name, statassist.statassist_id, statassist.statassist_name, statassist.assist_type, statassist.calc_var1_type, statassist.calc_var1_const_id, statassist.calc_var1_item_id, statassist.calc_var1_assist_id, statassist.calc_operator, statassist.calc_var2_type, statassist.calc_var2_const_id, statassist.calc_var2_item_id, statassist.calc_var2_assist_id, statassist.calc_format from statassist inner join statitem on statassist.statitem_id = statitem.statitem_id where statassist.del_flg = 'f' union select 'item', statitem_id, name, null, null, null, null, null, null, null, null, null, null, null, null, null from statitem where del_flg = 'f' and not exists (select * from statassist where statassist.statitem_id = statitem.statitem_id and statassist.del_flg = 'f')";
$cond = "order by 2, 4";
$sel_item = select_from_table($con, $sql, $cond, $fname);
if ($sel_item == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$item_count = pg_num_rows($sel_item);

// 施設一覧を取得
$sql = "select char(1) '1' as area_type, statfcl_id, name, area_id, (select order_no from area where area.area_id = statfcl.area_id) as order_no from statfcl where area_id is not null and del_flg = 'f' union select '2', statfcl_id, name, area_id, statfcl_id from statfcl where area_id is null and del_flg = 'f'";
$cond = "order by 1, order_no";
$sel_fcl = select_from_table($con, $sql, $cond, $fname);
if ($sel_fcl == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$fcl_count = pg_num_rows($sel_fcl);

// エリア情報を取得
$sql = "select area_id, area_name from area";
$cond = "";
$sel_area = select_from_table($con, $sql, $cond, $fname);
if ($sel_area == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$areas = array();
while ($row = pg_fetch_array($sel_area)) {
	$areas[$row["area_id"]] = $row["area_name"];
}

// 定数一覧を取得
$sql = "select statconst_id, statconst_value from statconst";
$cond = "where del_flg = 'f' order by statconst_id";
$sel_const = select_from_table($con, $sql, $cond, $fname);
if ($sel_const == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$consts = array();
while ($row = pg_fetch_array($sel_const)) {
	$consts[$row["statconst_id"]] = $row["statconst_value"];
}

// 統計値を取得
$ymd = date("Ymd", $date);
$sql = "select statitem_id, statassist_id, statfcl_id, stat_val from stat";
$cond = "where stat_date = '$ymd'";
$sel_stat = select_from_table($con, $sql, $cond, $fname);
if ($sel_stat == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 統計登録権限のある施設を取得
$sql = "select statfcl_id from statemp";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel_admin_fcl = select_from_table($con, $sql, $cond, $fname);
if ($sel_admin_fcl == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// イントラメニュー情報を取得
$sql = "select menu1 from intramenu";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$menu1 = pg_fetch_result($sel, 0, "menu1");
?>
<title>イントラネット | <? echo($menu1); ?></title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function changeDate(dt) {
	location.href = 'intra_stat.php?session=<? echo($session); ?>&date=' + dt;
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-style:none;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="intra_menu.php?session=<? echo($session); ?>"><img src="img/icon/b38.gif" width="32" height="32" border="0" alt="イントラネット"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="intra_menu.php?session=<? echo($session); ?>"><b>イントラネット</b></a> &gt; <a href="intra_stat.php?session=<? echo($session); ?>"><b><? echo($menu1); ?></b></a></font></td>
<? if ($stat_setting_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="stat_master_menu.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="105" align="center" bgcolor="#bdd1e7"><a href="intra_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メインメニュー</font></a></td>
<td width="5">&nbsp;</td>
<td width="<? echo(get_tab_width($menu1)); ?>" align="center" bgcolor="#5279a5"><a href="intra_stat.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b><? echo($menu1); ?></b></font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<?
if ($item_count == 0 && $fcl_count == 0) {
	echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">管理項目・施設が登録されていません。</font>");
} else if ($item_count == 0) {
	echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">管理項目が登録されていません。</font>");
} else if ($fcl_count == 0) {
	echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">施設が登録されていません。</font>");
} else {
	show_stats($sel_item, $sel_fcl, $areas, $sel_stat, $sel_admin_fcl, $date, $ymstat_auth, $session, $consts);
}
?>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
function show_stats($sel_item, $sel_fcl, $areas, $sel_stat, $sel_admin_fcl, $date, $ymstat_auth, $session, $consts) {
	define(COLUMNS_PER_ROW, 5);

	$last_month = get_last_month($date);
	$last_day = get_last_day($date);
	$next_day = get_next_day($date);
	$next_month = get_next_month($date);

	$stats = array();
	while ($row = pg_fetch_array($sel_stat)) {
		$tmp_item_id = $row["statitem_id"];
		$tmp_assist_id = $row["statassist_id"];

		if ($tmp_assist_id == "") {
			$tmp_id = $tmp_item_id;
		} else {
			$tmp_id = $tmp_item_id . "_" . $tmp_assist_id;
		}

		$stats[$row["statfcl_id"]][$tmp_id] = $row["stat_val"];
	}

	echo("<table width=\"$table_width\" border=\"0\" cellspacing=\"0\">\n");
	echo("<tr>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"intra_stat.php?session=$session&date=$last_month\">&lt;&lt;前月</a>&nbsp;<a href=\"intra_stat.php?session=$session&date=$last_day\">&lt;前日</a>&nbsp;<select onchange=\"changeDate(this.value);\">");
	show_date_options_d($date);
	echo("</select>&nbsp;<a href=\"intra_stat.php?session=$session&date=$next_day\">翌日&gt;</a>&nbsp;<a href=\"intra_stat.php?session=$session&date=$next_month\">翌月&gt;&gt;</a></font></td>\n");
	echo("</tr>\n");
	echo("</table>\n");
	echo("<img src=\"img/spacer.gif\" alt=\"\" width=\"1\" height=\"2\"><br>\n");

// 前の管理項目に入力系の補助項目が1件でも登録されている場合
// 合計欄を追加

	$inputable_assists = array();
	$pre_item_id = "";
	$pre_item_name = "";
	$items = array();
	while ($row = pg_fetch_array($sel_item)) {
		$tmp_item_id = $row["statitem_id"];
		$tmp_assist_id = $row["statassist_id"];

		if ($tmp_assist_id == "") {
			$tmp_id = $tmp_item_id;
			$tmp_name = $row["name"];
		} else {
			$tmp_id = $tmp_item_id . "_" . $tmp_assist_id;
			$tmp_name = $row["name"] . "<br>&nbsp;&gt; " . $row["statassist_name"];
		}

		if (!$inputable_assists[$tmp_item_id] && $row["assist_type"] == "1") {
			$inputable_assists[$tmp_item_id] = true;
		}

		if ($tmp_item_id != $pre_item_id) {
			if ($inputable_assists[$pre_item_id]) {
				$items[] = array(
					"id" => $pre_item_id,
					"name" => $pre_item_name . "<br>合計",
					"is_sum" => true
				);
			}

			$pre_item_id = $tmp_item_id;
			$pre_item_name = $row["name"];
		}

		$items[] = array(
			"id" => $tmp_id,
			"name" => $tmp_name,
			"assist_type" => $row["assist_type"],
			"var1_type" => $row["calc_var1_type"],
			"var1_const_id" => $row["calc_var1_const_id"],
			"var1_item_id" => $row["calc_var1_item_id"],
			"var1_assist_id" => $row["calc_var1_assist_id"],
			"operator" => $row["calc_operator"],
			"var2_type" => $row["calc_var2_type"],
			"var2_const_id" => $row["calc_var2_const_id"],
			"var2_item_id" => $row["calc_var2_item_id"],
			"var2_assist_id" => $row["calc_var2_assist_id"],
			"format" => $row["calc_format"]
		);
	}
	if ($inputable_assists[$pre_item_id]) {
		$items[] = array(
			"id" => $pre_item_id,
			"name" => $pre_item_name . "<br>合計",
			"is_sum" => true
		);
	}
	$rows = array_chunk($items, COLUMNS_PER_ROW, true);

	$fcls = array();
	for ($i = 0, $j = pg_num_rows($sel_fcl); $i < $j; $i++) {
		$tmp_fcl_id = pg_fetch_result($sel_fcl, $i, "statfcl_id");
		$tmp_fcl_name = pg_fetch_result($sel_fcl, $i, "name");
		$tmp_area_id = pg_fetch_result($sel_fcl, $i, "area_id");

		$fcls[] = array(
			"is_area_row" => false,
			"id" => $tmp_fcl_id,
			"name" => $tmp_fcl_name
		);
		if ($tmp_area_id == "") {
			continue;
		}

		if ($i < ($j - 1)) {
			$next_row_area_id = pg_fetch_result($sel_fcl, $i + 1, "area_id");
		} else {
			$next_row_area_id = "";
		}
		if ($next_row_area_id == $tmp_area_id) {
			continue;
		}

		$fcls[] = array(
			"is_area_row" => true,
			"id" => $tmp_area_id,
			"name" => $areas[$tmp_area_id]
		);
	}

	$admin_fcls = array();
	while ($row = pg_fetch_array($sel_admin_fcl)) {
		$admin_fcls[] = $row["statfcl_id"];
	}

	foreach ($rows as $items) {
		$col_count = count($items);
		$table_width = $col_count * 120 + 150;

		echo("<table width=\"$table_width\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"list\">\n");

		echo("<tr height=\"22\" bgcolor=\"#f6f9ff\" valign=\"top\">\n");
		echo("<td width=\"150\"></td>\n");
		foreach ($items as $tmp_item) {
			$item_id = $tmp_item["id"];
			$item_name = $tmp_item["name"];
			echo("<td width=\"120\">\n");
			echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
			echo("<tr valign=\"top\">\n");
			echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$item_name</font></td>\n");

			if ($ymstat_auth == "1" && !$tmp_item["is_sum"]) {
				echo("<td align=\"right\"><a href=\"intra_stat_item_day.php?session=$session&date=$date&item_id=$item_id\"><img src=\"img/icon/month.gif\" alt=\"1ヶ月表示\" width=\"20\" height=\"20\" border=\"0\"></a></td>\n");
			}
			echo("</tr>\n");
			echo("</table>\n");
			echo("</td>\n");
		}
		echo("</tr>\n");

		foreach ($fcls as $tmp_fcl) {
			$tmp_is_area_row = $tmp_fcl["is_area_row"];
			$tmp_id = $tmp_fcl["id"];
			$tmp_name = $tmp_fcl["name"];

			echo("<tr height=\"22\" valign=\"top\">\n");
			echo("<td bgcolor=\"#f6f9ff\">\n");
			echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
			echo("<tr valign=\"top\">\n");
			if ($tmp_is_area_row) {
				echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$tmp_name}合計</font></td>\n");
			} else {
				echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_name</font></td>\n");
			}
			echo("<td width=\"13\">");
			if (!$tmp_is_area_row && in_array($tmp_id, $admin_fcls)) {
				echo("<a href=\"javascript:void(0);\" onclick=\"window.open('intra_stat_register.php?session=$session&date=$date&fcl_id=$tmp_id', 'newwin', 'width=640,height=480,scrollbars=yes');\"><img src=\"img/pencil.gif\" alt=\"登録\" width=\"13\" height=\"13\" border=\"0\"></a>");
			}
			echo("</td>\n");
			echo("</tr>\n");
			if (!$tmp_is_area_row && $ymstat_auth == "1") {
				echo("<tr>\n");
				echo("<td colspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"intra_stat_fcl_day.php?session=$session&date=$date&fcl_id=$tmp_id\">&lt;詳細&gt;</a></font></td>\n");
				echo("</tr>\n");
			}
			echo("</table>\n");
			echo("</td>\n");

			foreach ($items as $tmp_item) {
				$item_id = $tmp_item["id"];
				$tmp_is_sum = $tmp_item["is_sum"];
				$tmp_assist_type = $tmp_item["assist_type"];
				$tmp_var1_type = $tmp_item["var1_type"];
				$tmp_var1_const_id = $tmp_item["var1_const_id"];
				$tmp_var1_item_id = $tmp_item["var1_item_id"];
				$tmp_var1_assist_id = $tmp_item["var1_assist_id"];
				$tmp_operator = $tmp_item["operator"];
				$tmp_var2_type = $tmp_item["var2_type"];
				$tmp_var2_const_id = $tmp_item["var2_const_id"];
				$tmp_var2_item_id = $tmp_item["var2_item_id"];
				$tmp_var2_assist_id = $tmp_item["var2_assist_id"];
				$tmp_format = $tmp_item["format"];

				echo("<td align=\"right\"><span style=\"font-size:1.6em;font-family:'Times New Roman';\">");

				// 合計列の場合
				if ($tmp_is_sum) {
					if ($tmp_is_area_row) {
						if (isset($sums["area" . $tmp_id][$item_id])) {
							echo($sums["area" . $tmp_id][$item_id]);
						}
						unset($sums["area" . $tmp_id][$item_id]);
					} else {
						if (isset($sums[$tmp_id][$item_id])) {
							echo($sums[$tmp_id][$item_id]);
						}
						unset($sums[$tmp_id][$item_id]);
					}
					continue;
				}

				// エリア行の場合
				if ($tmp_is_area_row) {
					echo($stat_sums[$item_id]);
					if (!is_null($stat_sums[$item_id])) {
						list($base_item_id) = split("_", $item_id);
						$sums["area" . $tmp_id][$base_item_id] += $stat_sums[$item_id];
					}
					$stat_sums[$item_id] = null;

				// 施設行の場合
				} else {

					// 管理項目または入力系の補助項目の場合
					if ($tmp_assist_type != "2") {
						echo($stats[$tmp_id][$item_id]);
						if (!is_null($stats[$tmp_id][$item_id])) {
							list($base_item_id) = split("_", $item_id);
							$sums[$tmp_id][$base_item_id] += $stats[$tmp_id][$item_id];
						}
						if (preg_match("/^-?[0-9.]+$/", $stats[$tmp_id][$item_id])) {
							if (!isset($stat_sums[$item_id])) {
								$stat_sums[$item_id] = 0;
							}
							$stat_sums[$item_id] += $stats[$tmp_id][$item_id];
						}

					// 計算系の補助項目の場合
					} else {

						// 項目1が定数の場合
						if ($tmp_var1_type == "1") {
							$tmp_var1_val = $consts[$tmp_var1_const_id];

						// 項目1が項目指定の場合
						} else {
							if ($tmp_var1_assist_id == "") {
								$tmp_var1_val = $stats[$tmp_id][$tmp_var1_item_id];
							} else {
								$tmp_var1_val = $stats[$tmp_id]["{$tmp_var1_item_id}_{$tmp_var1_assist_id}"];
							}
						}

						// 項目1が空の場合、計算しない
						if ($tmp_var1_val == "") {
							continue;
						}

						// 項目2が定数の場合
						if ($tmp_var2_type == "1") {
							$tmp_var2_val = $consts[$tmp_var2_const_id];

						// 項目2が項目指定の場合
						} else {
							if ($tmp_var2_assist_id == "") {
								$tmp_var2_val = $stats[$tmp_id][$tmp_var2_item_id];
							} else {
								$tmp_var2_val = $stats[$tmp_id]["{$tmp_var2_item_id}_{$tmp_var2_assist_id}"];
							}
						}

						// 項目2が空の場合、計算しない
						if ($tmp_var2_val == "") {
							continue;
						}

						// 四則演算
						$tmp_var1_val = floatval($tmp_var1_val);
						$tmp_var2_val = floatval($tmp_var2_val);
						switch ($tmp_operator) {
						case "1":  // ＋
							$tmp_val = $tmp_var1_val + $tmp_var2_val;
							break;
						case "2":  // −
							$tmp_val = $tmp_var1_val - $tmp_var2_val;
							break;
						case "3":  // ×
							$tmp_val = $tmp_var1_val * $tmp_var2_val;
							break;
						case "4":  // ÷
							if ($tmp_var2_val <> 0) {
								$tmp_val = $tmp_var1_val / $tmp_var2_val;
							} else {
								$tmp_val = null;
							}
							break;
						}

						// 実数表示の場合
						if ($tmp_format == "1") {
							$tmp_val = round($tmp_val, 1);
							if (preg_match("/^-?[0-9.]+$/", $tmp_val)) {
								if (!isset($stat_sums[$item_id])) {
									$stat_sums[$item_id] = 0;
								}
								$stat_sums[$item_id] += $tmp_val;
							}

						// %表示の場合
						} else {
							$tmp_val *= 100;
							$tmp_val = round($tmp_val, 1) . "%";
						}

						echo($tmp_val);
					}
				}
				echo("</span></td>\n");
			}
			echo("</tr>\n");
		}

		echo("</table>\n");
		echo("<img src=\"img/spacer.gif\" alt=\"\" width=\"1\" height=\"2\"><br>\n");
	}
}
?>
