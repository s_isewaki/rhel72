<?php
require_once("timecard_bean.php");
require_once("timecard_paid_hol_hour_class.php");

/**
 * 夏休付与画面の検索条件を返す
 *
 * @param mixed $srch_name 職員名
 * @param mixed $cls 事務所
 * @param mixed $atrb 課
 * @param mixed $dept 科
 * @param mixed $group_id グループ
 * @param mixed $select_add_yr  付与日の年
 * @param mixed $select_add_mon 付与日の月
 * @param mixed $select_add_day 付与日の日
 * @param mixed $year 年度
 * @param mixed $srch_id 職員ID
 * @return mixed 検索条件
 *
 */
function get_paid_srch_cond($srch_name, $cls, $atrb, $dept, $group_id, $select_add_yr, $select_add_mon, $select_add_day, $year, $srch_id) {
	
	$cond = "where exists (select * from authmst where authmst.emp_id = empmst.emp_id and authmst.emp_del_flg = 'f')";
    //検索条件　職員ID
    if ($srch_id != "") {
        $tmp_srch_id = str_replace(" ", "", $srch_id);
        $cond .= " and (empmst.emp_personal_id like '%$tmp_srch_id%')";
    }
    //検索条件　職員名
	if ($srch_name != "") {
		$tmp_srch_name = str_replace(" ", "", $srch_name);
		$cond .= " and (emp_lt_nm like '%$tmp_srch_name%' or emp_ft_nm like '%$tmp_srch_name%' or emp_kn_lt_nm like '%$tmp_srch_name%' or emp_kn_ft_nm like '%$tmp_srch_name%' or emp_lt_nm || emp_ft_nm like '%$tmp_srch_name%' or emp_kn_lt_nm || emp_kn_ft_nm like '%$tmp_srch_name%')";
	}
	//検索条件　属性：事務所
	if ($cls != "" && substr($cls, 0, 1) != "-") {
		$cond .= " and ((empmst.emp_class = $cls) or exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_class = $cls))";
	}
	//検索条件　属性：課
	if ($atrb != "" && substr($atrb, 0, 1) != "-") {
		$cond .= " and ((empmst.emp_attribute = $atrb) or exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_attribute = $atrb))";
	}
	//検索条件　属性：科
	if ($dept != "" && substr($dept, 0, 1) != "-") {
		$cond .= " and ((empmst.emp_dept = $dept) or exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_dept = $dept))";
	}
	//検索条件　グループ
	if ($group_id != "" && $group_id != "-") {
		$cond .= " and exists (select * from duty_shift_staff where empmst.emp_id = duty_shift_staff.emp_id and group_id in (select group_id from duty_shift_group where pattern_id = $group_id))";
	}
 
    //検索条件
    if ($select_add_yr != "" && $select_add_mon !="" && $select_add_day !="" && $select_add_yr != "-" && $select_add_mon !="-" && $select_add_day !="-"){
		$cond .= " and k.add_date = '".$select_add_yr.$select_add_mon.$select_add_day."' ";
    } elseif ($select_add_yr != "" && $select_add_mon !="" && $select_add_yr != "-" && $select_add_mon !="-"){
		$cond .= " and k.add_date between '".$select_add_yr.$select_add_mon."01' and '".date('ymt', strtotime($select_add_yr.$select_add_mon.'01'))."'";
    }

	return $cond;
}

/**
 * 夏休付与画面用のSQL文を返す
 *
 * @param mixed $srch_name 職員名
 * @param mixed $cls 事務所
 * @param mixed $atrb 課
 * @param mixed $dept 科
 * @param mixed $group_id グループ
 * @param mixed $select_add_yr  付与日の年
 * @param mixed $select_add_mon 付与日の月
 * @param mixed $select_add_day 付与日の日
 * @param mixed $year 年度
 * @param mixed $sort ソート順
 * @return mixed 検索条件
 *
 */
function get_paid_srch_sql($srch_name, $cls, $atrb, $dept, $group_id, $select_add_yr, $select_add_mon, $select_add_day, $year, $sort) {

	//付与年月日未設定時、プルダウンの"-"を""にする
	if ($select_add_yr == "-") {
		$select_add_yr = "";
	}
	if ($select_add_mon == "-") {
		$select_add_mon = "";
	}
	if ($select_add_day == "-") {
		$select_add_day = "";
	}
	
	//有給付与起算日paid_hol_start_dateがある場合は入職日emp_joinより優先して判断する
	// empmst 職員マスタ
	// emppaid b 夏休付与データ 直近1年のデータ、検索条件の付与日がある場合は付与日とあっているデータ
	// classmst c 所属マスタ1階層
	// atrbmst d 所属マスタ2階層　
	// deptmst e 所属マスタ3階層
	// empcond f 勤務条件
	// emppaid g 前回夏休付与データ
	// timecard_paid_hol_import h 夏休残移行データ
	// emppaid i 基準月数3ヶ月の夏休付与データ
	// emppaid k 最新の夏休付与データ、検索条件の付与日・夏休残締め日が未設定の場合使用
	// jobmst l 職種
	// 
	
	//ソート順
	if ($sort == "") {
		$sort = "1";
	}
	//2件ある場合でも1件のみ表示するようにする
	switch ($sort) {
		case "1":
		case "2":
			$distinct = " distinct on (empmst.emp_personal_id)  ";
			break;
		case "3":
		case "4":
			$distinct = " distinct on (empmst.emp_kn_lt_nm, empmst.emp_kn_ft_nm, empmst.emp_personal_id) ";
			break;
		case "5":
		case "6":
			$distinct = " distinct on (c.order_no, d.order_no, e.order_no, empmst.emp_personal_id) ";
			break;
	}
	$sql = "select $distinct empmst.emp_id, empmst.emp_personal_id, emp_lt_nm, emp_ft_nm ";
	$sql .= " , c.class_nm, d.atrb_nm, e.dept_nm, emp_join, f.paid_hol_tbl_id, f.paid_hol_start_date ";
	$sql .= " , l.job_nm ";
	$sql .= " , k.add_date, k.days, k.paid_hol_tbl_id_s ";
	
	//reg_* 夏休付与テーブルに登録済のデータ
	$sql .= " from empmst ";
    $sql .= " left join (select distinct on (b.emp_id) b.emp_id, b.add_date, b.days, b.paid_hol_tbl_id as paid_hol_tbl_id_s from emppaid_summer b ";
    $sql .= "  order by b.emp_id, b.add_date desc ";
    $sql .= " ) k on k.emp_id = empmst.emp_id ";
	$sql .= " left join classmst c on c.class_id = empmst.emp_class ";
	$sql .= " left join atrbmst d on d.atrb_id = empmst.emp_attribute ";
	$sql .= " left join deptmst e on e.dept_id = empmst.emp_dept ";
	$sql .= " left join empcond f on f.emp_id = empmst.emp_id ";
	$sql .= " left join jobmst l on l.job_id = empmst.emp_job ";
	
	return $sql;
}

/**
 * 職員一覧を表示
 *
 * @param object $con DBコネクション
 * @param string $fname 画面名
 * @param object $sel SQL結果
 * @param array $arr_paid_hol 夏休付与日数表の配列
 * @param string $year 処理年度
 * @param object $atdbk_common_class 出勤表関連共通クラス
 * @param string $sort ソート順
 * @param string $select_add_date 付与年月日
 * @return 夏休数一覧の文字列データ、2:excel時データを返し、それ以外の1,3は、echo表示しデータを返さない。
 *
 */
function get_emp_list_data($con, $fname, $sel, $arr_paid_hol, $year, $atdbk_common_class, $sort, $select_add_date) {
	if (!$sel) {
		return "";
	}
	
	//一覧見出しの配列
	$arr_title = array(
			"職員ID",
			"職員氏名",
			"所属",
            "夏休表",
			"付与年月日",
			"付与日数",
			);
	
    $font_size = "3";
    $font_class = "j12";
	
	$data = "";
	$data .= "<table width=\"980\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"list\">\n";
	$data .= "<tr height=\"22\" bgcolor=\"#f6f9ff\">\n";
	$data .= "<td width=\"80\" align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
	$arrow = ($sort == "1") ? "img/up.gif" : "img/down.gif";
	$data .= "<a href=\"javascript:set_sort('EMP_ID');\"><img src=\"{$arrow}\" alt=\"\" width=\"17\" height=\"17\" border=\"0\" style=\"vertical-align:middle;\">職員ID</a>";
	$data .= "</font></td>\n";
	$data .= "<td width=\"150\" align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
	$arrow = ($sort == "3") ? "img/up.gif" : "img/down.gif";
	$data .= "<a href=\"javascript:set_sort('EMP_NAME');\"><img src=\"{$arrow}\" alt=\"\" width=\"17\" height=\"17\" border=\"0\" style=\"vertical-align:middle;\">職員氏名</a>";
	$data .= "</font></td>\n";
	$data .= "<td width=\"390\" align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
	$arrow = ($sort == "5") ? "img/up.gif" : "img/down.gif";
	$data .= "<a href=\"javascript:set_sort('DEPT');\"><img src=\"{$arrow}\" alt=\"\" width=\"17\" height=\"17\" border=\"0\" style=\"vertical-align:middle;\">所属</a>";
	$data .= "</font></td>\n";
	$data .= "<td width=\"120\" align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">夏休表</font></td>\n";
	
    $start_pos = 4; //付与年月日
    $end_pos = 5;   //当年付与
	for ($i=$start_pos; $i<=$end_pos; $i++) {
		$data .= "<td width=\"110\" align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">{$arr_title[$i]}</font></td>\n";
	}
	$data .= "</tr>\n";
	
	echo($data);
	$data = "";
	
	//配列に設定
	//年休使用数取得のための情報
	$i = 0;
	while ($row = pg_fetch_array($sel)) {

        $tmp_id = $row["emp_id"];
		$tmp_personal_id = $row["emp_personal_id"];             //職員ID
		$tmp_name = "{$row["emp_lt_nm"]} {$row["emp_ft_nm"]}";  //職員氏名
		$tmp_add_date = $row["add_date"];                       //付与日
			
		$data .= "<tr height=\"22\">\n";
		//職員ID
		$data .= "<td width=\"80\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
		//当年、前年のデータがない場合は、情報を引き継ぐ
		if ($emppaid_year == "" && $prev_year == "") {
			$wk_paid_hol_tbl_id = $paid_hol_tbl_id;
			$wk_paid_hol_add_date = str_replace("/", "", $row["add_date"]);
		} else {
			$wk_paid_hol_tbl_id = "";
			$wk_paid_hol_add_date = "";
		}
		$data .= "<a href=\"javascript:void(0);\" onclick=\"openEdit('$tmp_id', '$wk_paid_hol_tbl_id', '$wk_paid_hol_add_date', '$wk_yy', '$wk_mm');\">";
		$data .= "$tmp_personal_id";
		$data .= "</a>";
		$data .= "</font></td>\n";
		//職員氏名
		$data .= "<td width=\"150\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">$tmp_name</font>\n";
		$data .= "<input type=\"hidden\" name=\"emp_name_{$i}\" value=\"$tmp_name\">";
		$data .= "</td>\n";
		
		//所属
		$wk_width = "390";
		$data .= "<td width=\"$wk_width\" align=\"left\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
		$data .= $row["class_nm"]."＞".$row["atrb_nm"]."＞".$row["dept_nm"];
		$data .= "</font></td>\n";

        //夏休表
        if ($row["paid_hol_tbl_id_s"]!=""){
            $wk_paid_hol_tbl_id = $row["paid_hol_tbl_id_s"];
        }else{
            $wk_paid_hol_tbl_id = $row["paid_hol_tbl_id"];            
        }
        $data .= "<td align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";	
		$data .= $atdbk_common_class->get_paid_hol_tbl_id_name($wk_paid_hol_tbl_id)."</font></td>\n";
        
		//付与日
        $wk_add_ymd="";
        if ($row["add_date"]!=""){
            $wk_add_ymd = intval(substr($row["add_date"],0,4))."年".intval(substr($row["add_date"],5,2))."月".intval(substr($row["add_date"],8,2))."日";
        }
		$data .= "<td align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";	
		$data .= $wk_add_ymd."</font></td>\n";
        		
		//付与日数
        $wk_days="";
        if ($row["days"]!=""){
            $wk_days = $row["days"]."日";
        }
        $data .= "<td align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
		$data .= $wk_days."</font></td>\n";
        $data .= "</tr>\n";
		$i++;

        echo($data);
        $data = "";
	}
	$data .= "</table>\n";
    $data .= "<input type=\"hidden\" name=\"data_cnt\" value=\"$i\">\n";
    echo($data);
    $data = "";
	return $data;
}

?>