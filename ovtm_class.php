<?php
require_once("Cmx.php");
require_once('Cmx/Model/SystemConfig.php');
require_once("about_comedix.php");

class ovtm_class
{

	var $file_name;	// 呼び出し元ファイル名
	var $_db_con;	// DBコネクション
    var $ovtm_tab_flg = ""; // 残業追加タブを表示する
    var $keyboard_input_flg = ""; //時間をキーボードで入力する
    var $ovtm1_no_default_flg = ""; //残業１の時間をデフォルト表示しない
    var $ovtm_rest_disp_flg = ""; //残業に休憩時間を入力する
    var $rest_check_flg = ""; //労働基準法の休憩時間が取れていない場合は警告する
    var $ovtm_total_disp_flg = ""; //残業申請、タイムカード修正申請画面に当月残業累計時間を表示する
    var $ovtm_limit_check_flg = ""; //36協定の残業限度時間をこえたら警告する
    var $ovtm_limit_month = ""; //残業限度時間　月
    var $ovtm_limit_year = ""; //残業限度時間　年
    var $import_5time_flg = ""; //1日5回までの出退勤を取り込む
    var $no_nextday_hol_ovtm_flg = ""; //公休以外の日から連続した、残業は休日残業としない
    var $rest_disp_flg = ""; //タイムカード修正画面で休憩時間を表示する
    var $sousai_sinai_flg = ""; //時間有休を遅刻早退と相殺しない
    var $delay_overtime_inout_flg = ""; //遅刻早退で法定内残業がある場合、法定外残業とする 20141024
    var $ovtm_reason2_input_flg = ""; //残業時刻2以降にも理由を入力する 20141226
    var $duty_input_nodisp_flg = ""; //当直
    var $allowance_input_nodisp_flg = ""; //手当
    var $rest_input_nodisp_flg = ""; //休憩時間
    var $out_input_nodisp_flg = ""; //外出時刻
    var $hol_hour_input_nodisp_flg = ""; //時間有休
    var $meeting_input_nodisp_flg = ""; //会議・研修・病棟外勤務
    var $list_allowance_disp_flg = ""; //出勤簿．手当
    var $list_meeting_disp_flg = ""; //出勤簿．会議・研修
    var $list_absence_disp_flg = ""; //出勤簿．欠勤
    var $ovtmrsn = array();

	/** コンストラクタ
	 * @param $con          DBコネクション
	 * @param $fname        画面名
	 * @param $emp_id       対象emp_id   文字列または、配列
	 * @param $start_date	取得範囲開始日
	 * @param $end_date     取得範囲終了日
	 *
	 */
	function ovtm_class($con, $fname)
    {
        // 呼び出し元ファイル名を保持
        $this->file_name = $fname;
        // DBコネクションを保持
        $this->_db_con = $con;

        $this->get_systemconfig();
    }


    /**
     *  get_ovtmrsn残業理由一覧を取得
     *
     * @param mixed $tmcd_group_id 勤務パターングループID
     * @return mixed 残業理由一覧の配列
     *
     */
    function get_ovtmrsn($tmcd_group_id) {
        // 残業理由一覧を取得
        $sql = "select reason_id, reason, over_no_apply_flag from ovtmrsn";
        $cond = "where del_flg = 'f' and (hide_flag = 'f' or hide_flag is NULL) and (tmcd_group_id is NULL";
        if ($tmcd_group_id != "") {
            $cond .= " or tmcd_group_id = $tmcd_group_id";
        }
        $cond .= ") order by sort";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        return pg_fetch_all($sel);

    }

    /**
     *  get_ovtmrsn_reason 指定された残業理由IDの残業理由を取得
     *
     * @param mixed $reason_id 残業理由ID
     * @return mixed 残業理由
     *
     */
    function get_ovtmrsn_reason($reason_id) {
    	if ($reason_id == "") {
    		return "";
    	}
        // 残業理由を取得
        $sql = "select reason from ovtmrsn";
        $cond = "where reason_id = $reason_id";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        return pg_fetch_result($sel, 0, "reason");

    }

    /**
     * get_ovtmrsn_option残業理由のオプション文字列を返す
     *
     * @param mixed $tmcd_group_id 勤務パターングループID
     * @param mixed $reason_id 初期値
     * @param mixed $no_apply_disp_flg 申請不要理由表示フラグ true:表示 false:非表示 省略時は表示 
     * @return mixed オプション文字列
     *
     */
    function get_ovtmrsn_option($tmcd_group_id, $reason_id, $no_apply_disp_flg = true) {
    	if (count($this->ovtmrsn) == 0) {
        	$arr_ovtmrsn = $this->get_ovtmrsn($tmcd_group_id);
        	$this->ovtmrsn = $arr_ovtmrsn;
        }
        else {
        	$arr_ovtmrsn = $this->ovtmrsn;
        }

        $str = "";
        foreach ($arr_ovtmrsn as $i => $row) {
        	if ($no_apply_disp_flg == false &&
        		$row["over_no_apply_flag"] == "t") {
        		continue;
        	}
            $tmp_reason_id = $row["reason_id"];
            $tmp_reason = h($row["reason"]);

            $selected = ($reason_id == $tmp_reason_id) ? " selected" : "";
            $str .= "<option value=\"$tmp_reason_id\" $selected>$tmp_reason</option>\n";
        }
        return $str;
    }

    /**
     *  get_ovtmapply 指定された残業申請IDのデータを取得
     *
     * @param mixed $apply_id 残業申請ID
     * @return mixed 残業申請データ
     *
     */
    function get_ovtmapply($apply_id) {
    	if ($apply_id == "") {
    		return array();
    	}
        // 残業申請を取得
        $sql = "select * from ovtmapply";
        $cond = "where apply_id = $apply_id";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
	    $arr_ovtmapply = array();
	    if (pg_num_rows($sel) > 0) {
	        $arr_ovtmapply = pg_fetch_array($sel, 0, PGSQL_ASSOC);
	    }
	    return $arr_ovtmapply;

    }

    /**
     *  get_ovtmapply_emp_date 職員ID、日付から残業申請データを取得
     *
     * @param mixed $emp_id 職員ID
     * @param mixed $date 日付
     * @return mixed 残業申請データ
     *
     */
    function get_ovtmapply_emp_date($emp_id, $date) {
    	if ($emp_id == "" || $date == "") {
    		return array();
    	}
        // 残業申請を取得
        $sql = "select * from ovtmapply";
        $cond = "where emp_id = '$emp_id' and target_date = '$date' and delete_flg = 'f' order by apply_id desc limit 1";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
	    $arr_ovtmapply = array();
	    if (pg_num_rows($sel) > 0) {
	        $arr_ovtmapply = pg_fetch_array($sel, 0, PGSQL_ASSOC);
	    }
	    return $arr_ovtmapply;

    }

    /**
     * disp_input_form残業申請画面の残業時刻入力フォームを返す
     *
     * @param mixed $atdbkrslt 勤務実績データ
     * @param mixed $timecard_bean タイムカード設定
     * @param mixed $arr_default_time デフォルト時刻情報
     * @param mixed $apply_status 申請ステータス
     * @param mixed $arr_ovtmapply 残業申請データ
     * @param mixed $timecard_or_ovtm_flg "1":タイムカード修正 "":残業申請
     * @param mixed $no_overtime 残業管理をしないフラグ
     * @param mixed $paid_hol_hour_flag 時間有休を許可するフラグ
     * @param mixed $ret_show_flg 呼出表示フラグ
     * @param mixed $apply_id 残業申請ID
     * @return mixed なし（input用のフォームを直接出力する）
     *
     */
    function disp_input_form($atdbkrslt, $timecard_bean, $arr_default_time, $apply_status, $arr_ovtmapply, $timecard_or_ovtm_flg="", $no_overtime="", $paid_hol_hour_flag, $ret_show_flg, $apply_id="", $colspan_num) {
    	//$arr_ovtmapply2残業理由処理で使用
    	//タイムカード修正画面からの場合、残業申請データがないため取得
    	if ($timecard_or_ovtm_flg == "1") {
    		$arr_ovtmapply2 = $this->get_ovtmapply($apply_id);
    	}
    	else {
    		$arr_ovtmapply2 = $arr_ovtmapply;
    	}
        //残業理由表示用のJavascript関数呼出文字列
        if ($timecard_or_ovtm_flg == "1" && $timecard_bean->over_time_apply_type != "0" &&
                $no_overtime != "t" // 残業管理をする場合
                && $this->ovtm_reason2_input_flg != "t" //残業理由２以降を入れない場合
                ) {
            $wk_onchange_ovtm_str = "setOvtmReasonIdDisabled();";
        }
        else {
            $wk_onchange_ovtm_str = "";
        }
        //残業申請の最初の画面か確認、overtime_apply.php
        //申請不要理由の場合、タイムカード修正画面からの場合も確認 20150107
        if (count($arr_ovtmapply) == 0 || $arr_ovtmapply2["apply_status"] == "4" || $_REQUEST["pnt_url"] != "") {
        	$apply_first_flg = true;
        }
        else {
        	$apply_first_flg = false;
        }
        //$apply_first_flg = (count($arr_ovtmapply) == 0);
        $maxline = 5;
        //表示上の行数、display:noneで2行にすることがあるため
        $dispmaxline = ($_POST["ovtmadd_disp_flg"] == "1") ? 5 : 2;
        for ($i=1; $i<=$maxline; $i++) {
            //
            $item_idx = ($i == 1) ? "" : $i;
            $s_hour_nm = "over_start_hour".$item_idx;
            $s_min_nm = "over_start_min".$item_idx;
            $e_hour_nm = "over_end_hour".$item_idx;
            $e_min_nm = "over_end_min".$item_idx;

            $s_item_name = "over_start_time".$item_idx;
            $e_item_name = "over_end_time".$item_idx;

            if ($_POST["postback"] == "t") {
                $over_start_hour = $_POST[$s_hour_nm];
                $over_start_min = $_POST[$s_min_nm];
                $over_end_hour = $_POST[$e_hour_nm];
                $over_end_min = $_POST[$e_min_nm];
                if ($over_start_hour == "--") {
                    $over_start_hour = "";
                }
                if ($over_start_min == "--") {
                    $over_start_min = "";
                }
                if ($over_end_hour == "--") {
                    $over_end_hour = "";
                }
                if ($over_end_min == "--") {
                    $over_end_min = "";
                }
            }
            else {
                if ($apply_first_flg) {
                    $s_item_value = $atdbkrslt[$s_item_name];
                    $e_item_value = $atdbkrslt[$e_item_name];
                }
                else {
                    //タイムカード修正の場合
                    if ($timecard_or_ovtm_flg == "1") {
                        $s_item_value = $arr_ovtmapply["a_".$s_item_name];
                        $e_item_value = $arr_ovtmapply["a_".$e_item_name];
                    }
                    else {
                        $s_item_value = $arr_ovtmapply[$s_item_name];
                        $e_item_value = $arr_ovtmapply[$e_item_name];
                    }
                }

                $over_start_hour = ($s_item_value != "") ? intval(substr($s_item_value, 0, 2), 10) : "";
                $over_start_min = substr($s_item_value, 2, 2);
                $over_end_hour = ($e_item_value != "") ? intval(substr($e_item_value, 0, 2), 10) : "";
                $over_end_min = substr($e_item_value, 2, 2);
            }

            $s_check_nm = "over_start_next_day_flag".$item_idx;
            $e_check_nm = "over_end_next_day_flag".$item_idx;
            if ($_POST["postback"] == "t") {
                $over_start_next_day_flag = $_POST[$s_check_nm];
                $over_end_next_day_flag = $_POST[$e_check_nm];
            }
            else {
                if ($apply_first_flg) {
                    $over_start_next_day_flag = $atdbkrslt[$s_check_nm];
                    $over_end_next_day_flag = $atdbkrslt[$e_check_nm];
                }
                else {
                    //タイムカード修正の場合
                    if ($timecard_or_ovtm_flg == "1") {
                        $over_start_next_day_flag = $arr_ovtmapply["a_".$s_check_nm];
                        $over_end_next_day_flag = $arr_ovtmapply["a_".$e_check_nm];
                    }
                    else {
                        $over_start_next_day_flag = $arr_ovtmapply[$s_check_nm];
                        $over_end_next_day_flag = $arr_ovtmapply[$e_check_nm];
                    }
                }
            }
            $over_start_next_day_flag_checked = ($over_start_next_day_flag == 1) ? " checked" : "";
            $over_end_next_day_flag_checked = ($over_end_next_day_flag == 1) ? " checked" : "";

            if ($_POST["postback"] != "t") {
                //残業申請不要の場合、残業時刻を空白とする
                if ($apply_status == "4") {
                    $over_start_hour = "";
                    $over_start_min = "";
                    $over_start_next_day_flag = "";
                    $over_start_next_day_flag_checked = "";
                    $over_end_hour = "";
                    $over_end_min = "";
                    $over_end_next_day_flag = "";
                    $over_end_next_day_flag_checked = "";
                } else {
                    //残業時刻１にデフォルト設定。残業申請の場合のみとする20150206
                    if ($this->ovtm1_no_default_flg == "f" && $i == 1 && $timecard_or_ovtm_flg != "1") {
                        if ($s_item_value == "") {
                            $over_start_hour = $arr_default_time["over_start_hour"];
                            $over_start_min = $arr_default_time["over_start_min"];
                            $over_start_next_day_flag = $arr_default_time["over_start_next_day_flag"];
                        }
                        if ($e_item_value == "") {
                            $over_end_hour = $arr_default_time["over_end_hour"];
                            $over_end_min = $arr_default_time["over_end_min"];
                            $over_end_next_day_flag = $arr_default_time["over_end_next_day_flag"];
                        }
                    }
                }
            }
            //休憩
            $rs_hour_nm = "rest_start_hour".$i;
            $rs_min_nm = "rest_start_min".$i;
            $re_hour_nm = "rest_end_hour".$i;
            $re_min_nm = "rest_end_min".$i;
            $s_item_name = "rest_start_time".$i;
            $e_item_name = "rest_end_time".$i;
            if ($_POST["postback"] == "t") {
                $rest_start_hour = $_POST[$rs_hour_nm];
                $rest_start_min = $_POST[$rs_min_nm];
                $rest_end_hour = $_POST[$re_hour_nm];
                $rest_end_min = $_POST[$re_min_nm];
                if ($rest_start_hour == "--") {
                    $rest_start_hour = "";
                }
                if ($rest_start_min == "--") {
                    $rest_start_min = "";
                }
                if ($rest_end_hour == "--") {
                    $rest_end_hour = "";
                }
                if ($rest_end_min == "--") {
                    $rest_end_min = "";
                }
            }
            else {
                if ($apply_first_flg) {
                    $s_item_value = $atdbkrslt[$s_item_name];
                    $e_item_value = $atdbkrslt[$e_item_name];
                }
                else {
                    //タイムカード修正の場合
                    if ($timecard_or_ovtm_flg == "1") {
                        $s_item_value = $arr_ovtmapply["a_".$s_item_name];
                        $e_item_value = $arr_ovtmapply["a_".$e_item_name];
                    }
                    else {
                        $s_item_value = $arr_ovtmapply[$s_item_name];
                        $e_item_value = $arr_ovtmapply[$e_item_name];
                    }
                }

                $rest_start_hour = ($s_item_value != "") ? intval(substr($s_item_value, 0, 2), 10) : "";
                $rest_start_min = substr($s_item_value, 2, 2);
                $rest_end_hour =  ($e_item_value != "") ? intval(substr($e_item_value, 0, 2), 10) : "";
                $rest_end_min = substr($e_item_value, 2, 2);
            }
            $rs_check_nm = "rest_start_next_day_flag".$i;
            $re_check_nm = "rest_end_next_day_flag".$i;
            if ($_POST["postback"] == "t") {
                $rest_start_next_day_flag = $_POST[$rs_check_nm];
                $rest_end_next_day_flag = $_POST[$re_check_nm];
            }
            else {
                if ($apply_first_flg) {
                    $rest_start_next_day_flag = $atdbkrslt[$rs_check_nm];
                    $rest_end_next_day_flag = $atdbkrslt[$re_check_nm];
                }
                else {
                    //タイムカード修正の場合
                    if ($timecard_or_ovtm_flg == "1") {
                        $rest_start_next_day_flag = $arr_ovtmapply["a_".$rs_check_nm];
                        $rest_end_next_day_flag = $arr_ovtmapply["a_".$re_check_nm];
                    }
                    else {
                        $rest_start_next_day_flag = $arr_ovtmapply[$rs_check_nm];
                        $rest_end_next_day_flag = $arr_ovtmapply[$re_check_nm];
                    }
                }
            }
            $rest_start_next_day_flag_checked = ($rest_start_next_day_flag == 1) ? " checked" : "";
            $rest_end_next_day_flag_checked = ($rest_end_next_day_flag == 1) ? " checked" : "";

            $ovtmadddisp = "none";
            if ($_POST["postback"] == "t" || $_POST["stat_update_flg"] == "1") {
                $ovtmadddisp = ($_POST["ovtmadd_disp_flg"] == '1') ? '' : 'none';
            }

            $trstyle = ($i>=3) ? " id='ovtmarea{$i}' style='display:{$ovtmadddisp};'" : "";
                ?>
            <tr height="22" <? echo $trstyle; ?>>
            <?
            $chk_flg = true; //翌日フラグ設定　true:する false:しない
            $chk_flg_str1 = ($chk_flg) ? " onchange=\"ovtmNextFlgSet('over', 'start', $i, '$s_check_nm');$wk_onchange_ovtm_str\"" : "";
            $chk_flg_str2 = ($chk_flg) ? " onchange=\"ovtmNextFlgSet('over', 'end', $i, '$e_check_nm');$wk_onchange_ovtm_str\"" : "";
            $chk_flg_str3 = ($chk_flg) ? " onchange=\"ovtmNextFlgSet('rest', 'start', $i, '$rs_check_nm');\"" : "";
            $chk_flg_str4 = ($chk_flg) ? " onchange=\"ovtmNextFlgSet('rest', 'end', $i, '$re_check_nm');\"" : "";
            ?>
            <?
            if ($this->keyboard_input_flg == "t") {
                ?>
                <td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><nobr>残業時刻<? echo $i; ?></font></td>
                <td <?php 
                $style_str = ($this->ovtm_reason2_input_flg == "t") ? "style=\"border-right:none;\"" : "";
                echo $style_str;
                
                if ($this->ovtm_reason2_input_flg != "t" &&
                	$colspan_num == "3") {
                	echo " colspan=\"3\"";
                }
                 ?>><nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10" nowrap>

                <input type="text" name="<? echo $s_hour_nm; ?>"  id="<? echo $s_hour_nm; ?>" value="<? echo $over_start_hour; ?>" size="2" maxlength="2" style="ime-mode:inactive;" <? echo $chk_flg_str1; ?> onkeydown="nextElement='<?
                echo $s_min_nm; ?>';backElement='<?
                //ユーザ側
            	$user_flg = ($_REQUEST["wherefrom"] == "0" || $_REQUEST["wherefrom"] == "1" || $_REQUEST["wherefrom"] == "6");
                if ($i == 1) {
                    if ($timecard_or_ovtm_flg == '1') {
                        $backElement = ($this->out_input_nodisp_flg == "t" && $user_flg) ? "end_min" : "ret_min";
                    }
                    else {
                        $backElement = "end_min";
                    }
                }
                elseif ($i == 2){
                    $line = $i - 1;
                    $backElement =($this->ovtm_rest_disp_flg == "t") ? "rest_end_min$line" : "over_end_min";
                }
                else {
                    $line = $i - 1;
                    $backElement =($this->ovtm_rest_disp_flg == "t") ? "rest_end_min$line" : "over_end_min$line";
                }
                echo $backElement;
                ?>';">：<input type="text" name="<? echo $s_min_nm; ?>"  id="<? echo $s_min_nm; ?>" value="<? echo $over_start_min; ?>" size="2" maxlength="2" style="ime-mode:inactive;" <? echo $chk_flg_str1; ?> onkeydown="nextElement='<? echo $e_hour_nm; ?>';backElement='<? echo $s_hour_nm; ?>';">
                <label><input type="checkbox" name="<? echo $s_check_nm; ?>" id="<? echo $s_check_nm; ?>" <?=$over_start_next_day_flag_checked ?> value="1">翌日</label>〜
                <input type="text" name="<? echo $e_hour_nm; ?>"  id="<? echo $e_hour_nm; ?>" value="<? echo $over_end_hour; ?>" size="2" maxlength="2" style="ime-mode:inactive;" <? echo $chk_flg_str2; ?> onkeydown="nextElement='<? echo $e_min_nm; ?>';backElement='<? echo $s_min_nm; ?>';">：<input type="text" name="<? echo $e_min_nm; ?>"  id="<? echo $e_min_nm; ?>" value="<? echo $over_end_min; ?>" size="2" maxlength="2" style="ime-mode:inactive;" <? echo $chk_flg_str2; ?> onkeydown="nextElement='<?
                //
                if ($i == $dispmaxline) {
                    if ($this->ovtm_rest_disp_flg == "t") {
                        $nextElement = $rs_hour_nm;
                    }
                    else {
                        if ($timecard_or_ovtm_flg == "1") {
                            //時間有休を許可する場合
                            if ($paid_hol_hour_flag == "t" && ($this->hol_hour_input_nodisp_flg != "t" || !$user_flg)) {
                                $nextElement = "paid_hol_start_hour";
                            }
                            else if ($user_flg && strpos($this->file_name, "atdbk_timecard_edit.php") !== false) {
                                $nextElement = "status";
                            }
                            //呼出を使用する場合
                            else if ($ret_show_flg == "t") {
                                $nextElement = "o_start_hour1";
                            }
                            //会議・研修
                            else if ($this->meeting_input_nodisp_flg != "t"){
                                $nextElement = "meeting_start_hour";
                            }
                            //上記以外の場合、理由
                            else {
                                $nextElement = "reason_id";
                            }
                        }
                        else {
                            $nextElement = "reason_id";
                        }
                    }
                }
                else {
                    $nextElement = ($this->ovtm_rest_disp_flg == "t") ? $rs_hour_nm : "over_start_hour".($i+1);
                }
                echo $nextElement; ?>';backElement='<? echo $e_hour_nm; ?>';">

                <label><input type="checkbox" name="<? echo $e_check_nm; ?>" id="<? echo $e_check_nm; ?>" <?=$over_end_next_day_flag_checked ?> value="1">翌日</label>
                <?
                if ($this->ovtm_rest_disp_flg == "t") {
                    ?>
                    休憩（
                    <input type="text" name="<? echo $rs_hour_nm; ?>"  id="<? echo $rs_hour_nm; ?>" value="<? echo $rest_start_hour; ?>" size="2" maxlength="2" style="ime-mode:inactive;" <? echo $chk_flg_str3; ?> onkeydown="nextElement='<? echo $rs_min_nm; ?>';backElement='<? echo $e_min_nm; ?>';">：<input type="text" name="<? echo $rs_min_nm; ?>"  id="<? echo $rs_min_nm; ?>" value="<? echo $rest_start_min; ?>" size="2" maxlength="2" style="ime-mode:inactive;" <? echo $chk_flg_str3; ?>  onkeydown="nextElement='<? echo $re_hour_nm; ?>';backElement='<? echo $rs_hour_nm; ?>';">
                    <label><input type="checkbox" name="<? echo $rs_check_nm; ?>" id="<? echo $rs_check_nm; ?>" <? echo $rest_start_next_day_flag_checked; ?> value="1">翌日</label>〜
                    <input type="text" name="<? echo $re_hour_nm; ?>"  id="<? echo $re_hour_nm; ?>" value="<? echo $rest_end_hour; ?>" size="2" maxlength="2" style="ime-mode:inactive;" <? echo $chk_flg_str4; ?>  onkeydown="nextElement='<? echo $re_min_nm; ?>';backElement='<? echo $rs_min_nm; ?>';">：<input type="text" name="<? echo $re_min_nm; ?>"  id="<? echo $re_min_nm; ?>" value="<? echo $rest_end_min; ?>" size="2" maxlength="2" style="ime-mode:inactive;" <? echo $chk_flg_str4; ?>  onkeydown="nextElement='<?
                    if ($i == $dispmaxline) {
                        if ($timecard_or_ovtm_flg == "1") {
                            //時間有休を許可する場合
                            if ($paid_hol_hour_flag == "t" && ($this->hol_hour_input_nodisp_flg != "t" || !$user_flg)) {
                                $nextElement = "paid_hol_start_hour";
                            }
                            else if ($user_flg && strpos($this->file_name, "atdbk_timecard_edit.php") !== false) {
                                $nextElement = "status";
                            }
                            //呼出を使用する場合
                            else if ($ret_show_flg == "t") {
                                $nextElement = "o_start_hour1";
                            }
                            //会議・研修
                            else if ($this->meeting_input_nodisp_flg != "t"){
                                $nextElement = "meeting_start_hour";
                            }
                            //上記以外の場合、理由
                            else {
                                $nextElement = "reason_id";
                            }
                        }
                        else {
                            $nextElement = "reason_id";
                        }
                    }
                    else {
                        $nextElement = "over_start_hour".($i+1);
                    }

                    echo $nextElement; ?>';backElement='<? echo $re_hour_nm; ?>';">

                    <label><input type="checkbox" name="<? echo $re_check_nm; ?>" id="<? echo $re_check_nm; ?>" <? echo $rest_end_next_day_flag_checked; ?> value="1">翌日</label>
                    ）
                <? } ?>
                </font>

                <?
            }
            //pulldown
            else {
                $style_str = ($this->ovtm_reason2_input_flg == "t") ? "style=\"border-right:none;\"" : "";
                ?>

                <td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><nobr>残業時刻<? echo $i; ?></font></td>
                <td <?php echo $style_str; ?>><nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10"><select name="<? echo $s_hour_nm; ?>" id="<? echo $s_hour_nm; ?>" <? echo $chk_flg_str1; ?>><? show_hour_options_0_23($over_start_hour, true); ?></select>：<select name="<? echo $s_min_nm; ?>"  id="<? echo $s_min_nm; ?>" <? echo $chk_flg_str1; ?>><? show_min_options_over_time($over_start_min, $timecard_bean->fraction_over_unit, true, true); ?></select>
                <label><input type="checkbox" name="<? echo $s_check_nm; ?>" id="<? echo $s_check_nm; ?>" <?=$over_start_next_day_flag_checked ?> value="1">翌日</label>
                〜 <select name="<? echo $e_hour_nm; ?>"  id="<? echo $e_hour_nm; ?>" <? echo $chk_flg_str2; ?>><? show_hour_options_0_23($over_end_hour, true); ?></select>：<select name="<? echo $e_min_nm; ?>"  id="<? echo $e_min_nm; ?>" <? echo $chk_flg_str2; ?>><? show_min_options_over_time($over_end_min, $timecard_bean->fraction_over_unit, true, false); ?></select>
                <label><input type="checkbox" name="<? echo $e_check_nm; ?>" id="<? echo $e_check_nm; ?>" <?=$over_end_next_day_flag_checked ?> value="1">翌日</label>
                <?
                if ($this->ovtm_rest_disp_flg == "t") {
                    ?>
                    休憩（
                    <select name="<? echo $rs_hour_nm; ?>" id="<? echo $rs_hour_nm; ?>" <? echo $chk_flg_str3; ?>><? show_hour_options_0_23($rest_start_hour, true); ?></select>：<select name="<? echo $rs_min_nm; ?>"  id="<? echo $rs_min_nm; ?>" <? echo $chk_flg_str3; ?>><? show_min_options_over_time($rest_start_min, $timecard_bean->fraction_over_unit, true, false); ?></select>
                    <label><input type="checkbox" name="<? echo $rs_check_nm; ?>" id="<? echo $rs_check_nm; ?>" <? echo $rest_start_next_day_flag_checked; ?> value="1">翌日</label>
                    〜 <select name="<? echo $re_hour_nm; ?>"  id="<? echo $re_hour_nm; ?>" <? echo $chk_flg_str4; ?>><? show_hour_options_0_23($rest_end_hour, true); ?></select>：<select name="<? echo $re_min_nm; ?>"  id="<? echo $re_min_nm; ?>" <? echo $chk_flg_str4; ?>><? show_min_options_over_time($rest_end_min, $timecard_bean->fraction_over_unit, true, false); ?></select>
                    <label><input type="checkbox" name="<? echo $re_check_nm; ?>" id="<? echo $re_check_nm; ?>" <? echo $rest_end_next_day_flag_checked; ?> value="1">翌日</label>
                    ）
                <? }

                 ?>
                </font>

                <?
            }
            //残業時刻2以降にも理由を入力する 20150105
            if ($this->ovtm_reason2_input_flg == "t") {
            	$reason_id_nm = ($timecard_or_ovtm_flg == "1") ? "ovtm_reason_id".$item_idx : "reason_id".$item_idx;
            	$other_reason_nm = ($timecard_or_ovtm_flg == "1") ? "ovtm_reason_div".$item_idx : "other_reason".$item_idx;
            	$reason_nm = ($timecard_or_ovtm_flg == "1") ? "ovtm_reason".$item_idx : "reason".$item_idx;
            	
            	$setreason_func = ($timecard_or_ovtm_flg == "1") ? "setOvtmReasonDisabled('{$item_idx}');" : "setOtherReasonDisabled('{$item_idx}');";
            	
            	$width_str = ($this->ovtm_rest_disp_flg == "t") ? "30%" : "60%";
            	
            	$style_str = ($colspan_num == "3" && $this->ovtm_reason2_input_flg == "t") ? "border-right:none;" : "";
            ?>
                </td>
                <td style="border-left:none;<?php echo "$style_str"; ?>" align="left" width="<?php echo "$width_str"; ?>">
            <table border="0" cellspacing="0" cellpadding="0" class="block_in">
            <tr>
            <td>
            <select name="<?php echo "$reason_id_nm"; ?>" id="<?php echo "$reason_id_nm"; ?>" onchange="<?php echo "$setreason_func"; ?>" onblur="document.onkeydown=focusToNext;" onfocus="document.onkeydown='';" style="width:300px">
<?
//先頭を空白とする 20150128
    echo "<option value=\"\"></option>\n";
//残業理由
    if ($_POST["postback"] == "t") {
		$reason_id_val = $_POST[$reason_id_nm];
	}
	else {
		$reason_id_val = $arr_ovtmapply2["reason_id".$item_idx];
	}
	//申請不要理由表示フラグ設定、2行目は表示しない
	$no_apply_disp_flg = ($i >= 2) ? false : true;
    $reason_optstr = $this->get_ovtmrsn_option($atdbkrslt["tmcd_group_id"], $reason_id_val, $no_apply_disp_flg);

    echo $reason_optstr;
//理由が入力済みの場合、その他とする
    if ($_POST["postback"] == "t") {
		$reason_val = $_POST[$reason_nm];
	}
	else {
		$reason_val = $arr_ovtmapply2["reason".$item_idx];
	}
	$selected = ($reason_val != "" || $reason_id_val == "other") ? " selected" : "";
?>
<option value="other" <? echo($selected); ?>>その他</option>
</select>
<?php 
//if ($this->ovtm_rest_disp_flg == "t") {&nbsp;
//	echo "<br>";
//}
//else {
//	echo "&nbsp;</td>";
//	echo "<td>";
//}
 ?>
<div id="<?php echo "$other_reason_nm"; ?>" style="display:none;"><input type="text" name="<?php echo "$reason_nm"; ?>" id="<?php echo "$reason_nm"; ?>" value="<? echo($reason_val); ?>" size="32" maxlength="100" style="ime-mode:active;" onblur="document.onkeydown=focusToNext;" onfocus="document.onkeydown='';"></div>
</td>
</tr>
</table>
            <?
            }
                 ?>
                 
                </td>
<?php 
if ($colspan_num == "3" && $this->ovtm_reason2_input_flg == "t") {
	echo "<td style=\"border-left:none;\">";
	echo "</td>";
}
 ?>
                </tr>
                <?
        }
    }


    /**
     * get_systemconfig残業申請関連の設定取得
     *
     * @return なし（$this->ovtm_tab_flgに値を設定
     *
     */
    function get_systemconfig() {
        // system_config
        $conf = new Cmx_SystemConfig();

        // 残業追加タブを表示する
        $this->ovtm_tab_flg = $conf->get('timecard.ovtm_tab_flg');
        //時間をキーボードで入力する
        $this->keyboard_input_flg = $conf->get('timecard.keyboard_input_flg');
        //残業１の時間をデフォルト表示しない
        $this->ovtm1_no_default_flg = $conf->get('timecard.ovtm1_no_default_flg');
        //残業に休憩時間を入力する
        $this->ovtm_rest_disp_flg = $conf->get('timecard.ovtm_rest_disp_flg');
        //労働基準法の休憩時間が取れていない場合は警告する
        $this->rest_check_flg = $conf->get('timecard.rest_check_flg');
        //残業申請、タイムカード修正申請画面に当月残業累計時間を表示する
        $this->ovtm_total_disp_flg = $conf->get('timecard.ovtm_total_disp_flg');
        //36協定の残業限度時間をこえたら警告する
        $this->ovtm_limit_check_flg = $conf->get('timecard.ovtm_limit_check_flg');
        //残業限度時間　月
        $this->ovtm_limit_month = $conf->get('timecard.ovtm_limit_month');
        //残業限度時間　年
        $this->ovtm_limit_year = $conf->get('timecard.ovtm_limit_year');
        //1日5回までの出退勤を取り込む
        $this->import_5time_flg = $conf->get('timecard.import_5time_flg');
        //公休以外の日から連続した、残業は休日残業としない
        $this->no_nextday_hol_ovtm_flg = $conf->get('timecard.no_nextday_hol_ovtm_flg');
        //タイムカード修正画面で休憩時間を表示する
        $this->rest_disp_flg = $conf->get('timecard.rest_disp_flg');
        //時間有休を遅刻早退と相殺しない
        $this->sousai_sinai_flg = $conf->get('timecard.sousai_sinai_flg');
	    //遅刻早退で法定内残業がある場合、法定外残業とする 20141024
	    $this->delay_overtime_inout_flg = $conf->get('timecard.delay_overtime_inout_flg');
	    //残業時刻2以降にも理由を入力する 20141226
	    $this->ovtm_reason2_input_flg = $conf->get('timecard.ovtm_reason2_input_flg');
		//ユーザ側の入力画面で表示しない項目 20150109
	    $this->duty_input_nodisp_flg = $conf->get('timecard.duty_input_nodisp_flg');
	    $this->allowance_input_nodisp_flg = $conf->get('timecard.allowance_input_nodisp_flg');
	    $this->rest_input_nodisp_flg = $conf->get('timecard.rest_input_nodisp_flg');
	    $this->out_input_nodisp_flg = $conf->get('timecard.out_input_nodisp_flg');
	    $this->hol_hour_input_nodisp_flg = $conf->get('timecard.hol_hour_input_nodisp_flg');
	    $this->meeting_input_nodisp_flg = $conf->get('timecard.meeting_input_nodisp_flg');
		//手当、会議研修、欠勤の表示 20150113
	    $this->list_allowance_disp_flg = $conf->get('timecard.list_allowance_disp_flg');
	    $this->list_meeting_disp_flg = $conf->get('timecard.list_meeting_disp_flg');
	    $this->list_absence_disp_flg = $conf->get('timecard.list_absence_disp_flg');

    }

    /**
     * get_btn_time_format打刻の時刻を表示用にして返す
     * 例）0800 -> 8:00
     * @param mixed $time 時刻HHMM形式
     * @return mixed H:MM形式
     *
     */
    function get_btn_time_format($time) {

        if ($time == "") {
            return "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        }
        $start_btn_hour = substr($time, 0, 2);
        $start_btn_min = substr($time, 2, 2);
        $tmp_start_btn_time = sprintf("%2d:%02d", (int)$start_btn_hour, $start_btn_min);
        return $tmp_start_btn_time;
    }

    /**
     * get_btn_date_format打刻の日付を表示用にして返す
     * 例）20141201 -> 12/1
     * @param mixed $time 時刻HHMM形式
     * @return mixed H:MM形式
     *
     */
    function get_btn_date_format($date) {

        if ($date == "") {
            return "";
        }
        $start_btn_mon = substr($date, 4, 2);
        $start_btn_day = substr($date, 6, 2);
        $tmp_start_btn_date = sprintf("%d/%d", (int)$start_btn_mon, (int)$start_btn_day)."&nbsp;";
        return $tmp_start_btn_date;
    }


    /**
     * disp_ovtm_total_time残業累計情報表示
     *
     * @param mixed $arr_ovtm_data 残業累計情報
     * @param mixed $timecard_or_ovtm_flg "1":タイムカード修正 "":残業申請
     * @param mixed $input_or_refer_flg "input":入力あり "refer":参照 "approve":承認
     * @return mixed なし（残業時間を出力）
     *
     */
    function disp_ovtm_total_time($arr_ovtm_data, $timecard_or_ovtm_flg, $input_or_refer_flg) {
        if ($this->ovtm_total_disp_flg != "t") {
            return;
        }
        if ($timecard_or_ovtm_flg == "1") {
        	$colspan = "3";
        }
        else {
        	if ($input_or_refer_flg == "refer") {
	        	$colspan = "1";
	        }
	        else {
        		$colspan = ($this->ovtm_reason2_input_flg == "t") ? "2" : "1";
        	}
        }
        ?>
        <tr height="22">
        <td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">当月残業<br>時間累計</font></td>
        <td colspan="<? echo($colspan); ?>">
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
承認のみ　<?
        echo $arr_ovtm_data["ovtm_approve_time_all"];
        ?>
　　申請中含む　<?
        echo $arr_ovtm_data["ovtm_approve_apply_time_all"];
        ?>
        </font>
        </td>
        </tr>
        <?
    }


    /**
     * disp_btn_time_data打刻データを表示する
     *
     * @param mixed $arr_result 勤務実績データ
     * @param mixed $input_or_refer_flg "input":入力あり "refer":参照 "approve":承認
     * @return なし
     *
     */
    function disp_btn_time_data($arr_result, $input_or_refer_flg, $colspan_num="") {
        if ($input_or_refer_flg == "input") {
            if ($_POST["ovtmadd_disp_flg"] != "1") {
                return;
            }
        }
        else {
            if ($this->ovtm_tab_flg != "t") {
                return;
            }
        }
        //$colspan = ($input_or_refer_flg == "approve") ? "3" : "2";
        if ($colspan_num != "") {
        	$colspan = $colspan_num;
        }
        elseif ($input_or_refer_flg == "approve") {
        	$colspan = "3";
        }
        elseif ($input_or_refer_flg == "refer") {
        	$colspan = "1";
        }
        else {
        	$colspan = ($this->ovtm_reason2_input_flg == "t") ? "2" : "1";
        }

?>
        <tr height="22" id="dakokuarea">
                <td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">打刻データ<? echo $i; ?></font></td>
        <td colspan="<? echo($colspan); ?>">

        <table border="0" class="block">
        <tr>
        <td>
                <?
        for ($i=1; $i<=5; $i++) {
            echo "<tr>";
            echo "<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
            echo ("出勤{$i}&nbsp;");
            echo($this->get_btn_date_format($arr_result["start_btn_date".$i]));
            $varname = ($i == 1) ? "start_btn_time" : "start_btn_time$i";
            $tmp_start_btn_time = $this->get_btn_time_format($arr_result["$varname"]);
            echo($tmp_start_btn_time);

            echo "</font></td>";
            echo "<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
            echo ("　　退勤{$i}&nbsp;");
            echo($this->get_btn_date_format($arr_result["end_btn_date".$i]));
            $varname = ($i == 1) ? "end_btn_time" : "end_btn_time$i";
            $tmp_end_btn_time = $this->get_btn_time_format($arr_result["$varname"]);
            echo($tmp_end_btn_time);
            echo "<br>\n";
            echo "</font></td>";

            echo "</tr>";
        }
?>
        </table>

        </td>
        </tr>
        <?
    }


    /**
     * check_input入力チェック（JavaScriptでやるため実際は使用されないが念のため残す）
     *
     * @return なし
     *
     */
    function check_input() {
        $maxline = ($this->ovtm_tab_flg == "t") ? 5 : 2;
        $start_hour = $_POST["start_hour"];
        $start_min = $_POST["start_min"];
        $end_hour = $_POST["end_hour"];
        $end_min = $_POST["end_min"];
        if ($start_hour == "--") {
            $start_hour = "";
        }
        if ($start_min == "--") {
            $start_min = "";
        }
        if ($end_hour == "--") {
            $end_hour = "";
        }
        if ($end_min == "--") {
            $end_min = "";
        }

        //出勤時刻のチェック
        if ($start_hour != "") {
            if (preg_match("/^\d{1,2}$/", $start_hour) == 0) {
                echo("<script type=\"text/javascript\">alert('出勤時刻の時は2桁以下の半角数字で入力してください。');</script>");
                echo("<script type=\"text/javascript\">history.back();</script>");
                exit;
            }
            if ($start_hour < 0 || $start_hour > 23) {
                echo("<script type=\"text/javascript\">alert('出勤時刻の時は0から23の範囲内で入力してください。');</script>");
                echo("<script type=\"text/javascript\">history.back();</script>");
                exit;
            }
        }
        if ($start_min != "") {
            if (preg_match("/^\d{1,2}$/", $start_min) == 0) {
                echo("<script type=\"text/javascript\">alert('出勤時刻の分は2桁以下の半角数字で入力してください。');</script>");
                echo("<script type=\"text/javascript\">history.back();</script>");
                exit;
            }
            if ($start_min < 0 || $start_min > 59) {
                echo("<script type=\"text/javascript\">alert('出勤時刻の分は0から59の範囲内で入力してください。');</script>");
                echo("<script type=\"text/javascript\">history.back();</script>");
                exit;
            }
        }
        //出勤時刻の時分チェック
        if (($start_hour != "" && $start_min == "") || ($start_hour == "" && $start_min != "")) {
            echo("<script type=\"text/javascript\">alert('出勤時刻の時と分の一方のみは登録できません。');</script>");
            echo("<script language='javascript'>history.back();</script>");
            exit;
        }
        //退勤時刻のチェック
        if ($end_hour != "") {
            if (preg_match("/^\d{1,2}$/", $end_hour) == 0) {
                echo("<script type=\"text/javascript\">alert('退勤時刻の時は2桁以下の半角数字で入力してください。');</script>");
                echo("<script type=\"text/javascript\">history.back();</script>");
                exit;
            }
            if ($end_hour < 0 || $end_hour > 23) {
                echo("<script type=\"text/javascript\">alert('退勤時刻の時は0から23の範囲内で入力してください。');</script>");
                echo("<script type=\"text/javascript\">history.back();</script>");
                exit;
            }
        }
        if ($end_min != "") {
            if (preg_match("/^\d{1,2}$/", $end_min) == 0) {
                echo("<script type=\"text/javascript\">alert('退勤時刻の分は2桁以下の半角数字で入力してください。');</script>");
                echo("<script type=\"text/javascript\">history.back();</script>");
                exit;
            }
            if ($end_min < 0 || $end_min > 59) {
                echo("<script type=\"text/javascript\">alert('退勤時刻の分は0から59の範囲内で入力してください。');</script>");
                echo("<script type=\"text/javascript\">history.back();</script>");
                exit;
            }
        }
        //退勤時刻の時分チェック
        if (($end_hour != "" && $end_min == "") || ($end_hour == "" && $end_min != "")) {
            echo("<script type=\"text/javascript\">alert('退勤時刻の時と分の一方のみは登録できません。');</script>");
            echo("<script language='javascript'>history.back();</script>");
            exit;
        }

        $ovtm_input_chk_flg = false;
        //残業繰り返し
        for ($i=1; $i<=$maxline; $i++) {
            $idx = ($i == 1) ? "" : $i;
            $s_t = "over_start_time".$idx;
            $s_h = "over_start_hour".$idx;
            $s_m = "over_start_min".$idx;

            $start_hour = $_POST[$s_h];
            $start_min = $_POST[$s_m];
            if ($start_hour == "--") {
                $start_hour = "";
            }
            if ($start_min == "--") {
                $start_min = "";
            }
            if ($start_hour != "") {
                $ovtm_input_chk_flg = true;
            }
            $item_str = "残業時刻{$i}開始";
            if ($start_hour != "") {
                if (preg_match("/^\d{1,2}$/", $start_hour) == 0) {
                    echo("<script type=\"text/javascript\">alert('{$item_str}の時は2桁以下の半角数字で入力してください。');</script>");
                    echo("<script type=\"text/javascript\">history.back();</script>");
                    exit;
                }
                if ($start_hour < 0 || $start_hour > 23) {
                    echo("<script type=\"text/javascript\">alert('{$item_str}の時は0から23の範囲内で入力してください。');</script>");
                    echo("<script type=\"text/javascript\">history.back();</script>");
                    exit;
                }
            }
            if ($start_min != "") {
                if (preg_match("/^\d{1,2}$/", $start_min) == 0) {
                    echo("<script type=\"text/javascript\">alert('{$item_str}の分は2桁以下の半角数字で入力してください。');</script>");
                    echo("<script type=\"text/javascript\">history.back();</script>");
                    exit;
                }
                if ($start_min < 0 || $start_min > 59) {
                    echo("<script type=\"text/javascript\">alert('{$item_str}の分は0から59の範囲内で入力してください。');</script>");
                    echo("<script type=\"text/javascript\">history.back();</script>");
                    exit;
                }
            }
            //残業時刻の時分チェック
            if (($start_hour != "" && $start_min == "") || ($start_hour == "" && $start_min != "")) {
                echo("<script type=\"text/javascript\">alert('{$item_str}の時と分の一方のみは登録できません。');</script>");
                echo("<script language='javascript'>history.back();</script>");
                exit;
            }

            //終了
            $e_h = "over_end_hour".$idx;
            $e_m = "over_end_min".$idx;
            $end_hour = $_POST[$e_h];
            $end_min = $_POST[$e_m];
            if ($end_hour == "--") {
                $end_hour = "";
            }
            if ($end_min == "--") {
                $end_min = "";
            }
            $item_str = "残業時刻{$i}終了";
            if ($end_hour != "") {
                if (preg_match("/^\d{1,2}$/", $end_hour) == 0) {
                    echo("<script type=\"text/javascript\">alert('{$item_str}の時は2桁以下の半角数字で入力してください。');</script>");
                    echo("<script type=\"text/javascript\">history.back();</script>");
                    exit;
                }
                if ($end_hour < 0 || $end_hour > 23) {
                    echo("<script type=\"text/javascript\">alert('{$item_str}の時は0から23の範囲内で入力してください。');</script>");
                    echo("<script type=\"text/javascript\">history.back();</script>");
                    exit;
                }
            }
            if ($end_min != "") {
                if (preg_match("/^\d{1,2}$/", $end_min) == 0) {
                    echo("<script type=\"text/javascript\">alert('{$item_str}の分は2桁以下の半角数字で入力してください。');</script>");
                    echo("<script type=\"text/javascript\">history.back();</script>");
                    exit;
                }
                if ($end_min < 0 || $end_min > 59) {
                    echo("<script type=\"text/javascript\">alert('{$item_str}の分は0から59の範囲内で入力してください。');</script>");
                    echo("<script type=\"text/javascript\">history.back();</script>");
                    exit;
                }
            }
            //退勤時刻の時分チェック
            if (($end_hour != "" && $end_min == "") || ($end_hour == "" && $end_min != "")) {
                echo("<script type=\"text/javascript\">alert('{$item_str}の時と分の一方のみは登録できません。');</script>");
                echo("<script language='javascript'>history.back();</script>");
                exit;
            }
            //一方のみの入力チェック
            $item_str = "残業時刻{$i}";
            if ($start_hour == "" && $start_min == "" && $end_hour != "" && $end_min != "") {
                echo("<script type=\"text/javascript\">alert('{$item_str}の開始時刻を入力してください。');</script>");
                echo("<script language='javascript'>history.back();</script>");
                exit;
            }
            if ($start_hour != "" && $start_min != "" && $end_hour == "" && $end_min == "") {
                echo("<script type=\"text/javascript\">alert('{$item_str}の終了時刻を入力してください。');</script>");
                echo("<script language='javascript'>history.back();</script>");
                exit;
            }

            //休憩
            $s_h = "rest_start_hour".$i;
            $s_m = "rest_start_min".$i;
            $rest_start_hour = $_POST[$s_h];
            $rest_start_min = $_POST[$s_m];
            if ($rest_start_hour == "--") {
                $rest_start_hour = "";
            }
            if ($rest_start_min == "--") {
                $rest_start_min = "";
            }
            $item_str = "残業時刻{$i}休憩開始";
            if ($rest_start_hour != "") {
                if (preg_match("/^\d{1,2}$/", $rest_start_hour) == 0) {
                    echo("<script type=\"text/javascript\">alert('{$item_str}の時は2桁以下の半角数字で入力してください。');</script>");
                    echo("<script type=\"text/javascript\">history.back();</script>");
                    exit;
                }
                if ($rest_start_hour < 0 || $rest_start_hour > 23) {
                    echo("<script type=\"text/javascript\">alert('{$item_str}の時は0から23の範囲内で入力してください。');</script>");
                    echo("<script type=\"text/javascript\">history.back();</script>");
                    exit;
                }
            }
            if ($rest_start_min != "") {
                if (preg_match("/^\d{1,2}$/", $rest_start_min) == 0) {
                    echo("<script type=\"text/javascript\">alert('{$item_str}の分は2桁以下の半角数字で入力してください。');</script>");
                    echo("<script type=\"text/javascript\">history.back();</script>");
                    exit;
                }
                if ($rest_start_min < 0 || $rest_start_min > 59) {
                    echo("<script type=\"text/javascript\">alert('{$item_str}の分は0から59の範囲内で入力してください。');</script>");
                    echo("<script type=\"text/javascript\">history.back();</script>");
                    exit;
                }
            }
            //休憩時刻の時分チェック
            if (($rest_start_hour != "" && $rest_start_min == "") || ($rest_start_hour == "" && $rest_start_min != "")) {
                echo("<script type=\"text/javascript\">alert('{$item_str}の時と分の一方のみは登録できません。');</script>");
                echo("<script language='javascript'>history.back();</script>");
                exit;
            }
            $e_h = "rest_end_hour".$i;
            $e_m = "rest_end_min".$i;
            $rest_end_hour = $_POST[$e_h];
            $rest_end_min = $_POST[$e_m];
            if ($rest_end_hour == "--") {
                $rest_end_hour = "";
            }
            if ($rest_end_min == "--") {
                $rest_end_min = "";
            }
            $item_str = "残業時刻{$i}休憩終了";
            if ($rest_end_hour != "") {
                if (preg_match("/^\d{1,2}$/", $rest_end_hour) == 0) {
                    echo("<script type=\"text/javascript\">alert('{$item_str}の時は2桁以下の半角数字で入力してください。');</script>");
                    echo("<script type=\"text/javascript\">history.back();</script>");
                    exit;
                }
                if ($rest_end_hour < 0 || $rest_end_hour > 23) {
                    echo("<script type=\"text/javascript\">alert('{$item_str}の時は0から23の範囲内で入力してください。');</script>");
                    echo("<script type=\"text/javascript\">history.back();</script>");
                    exit;
                }
            }
            if ($rest_end_min != "") {
                if (preg_match("/^\d{1,2}$/", $rest_end_min) == 0) {
                    echo("<script type=\"text/javascript\">alert('{$item_str}の分は2桁以下の半角数字で入力してください。');</script>");
                    echo("<script type=\"text/javascript\">history.back();</script>");
                    exit;
                }
                if ($rest_end_min < 0 || $rest_end_min > 59) {
                    echo("<script type=\"text/javascript\">alert('{$item_str}の分は0から59の範囲内で入力してください。');</script>");
                    echo("<script type=\"text/javascript\">history.back();</script>");
                    exit;
                }
            }
            //休憩時刻の時分チェック
            if (($rest_end_hour != "" && $rest_end_min == "") || ($rest_end_hour == "" && $rest_end_min != "")) {
                echo("<script type=\"text/javascript\">alert('{$item_str}の時と分の一方のみは登録できません。');</script>");
                echo("<script language='javascript'>history.back();</script>");
                exit;
            }
            //休憩の一方のみの入力チェック
            //一方のみの入力チェック
            $item_str = "残業時刻{$i}休憩";
            if ($rest_start_hour == "" && $rest_start_min == "" && $rest_end_hour != "" && $rest_end_min != "") {
                echo("<script type=\"text/javascript\">alert('{$item_str}の開始時刻を入力してください。');</script>");
                echo("<script language='javascript'>history.back();</script>");
                exit;
            }
            if ($rest_start_hour != "" && $rest_start_min != "" && $rest_end_hour == "" && $rest_end_min == "") {
                echo("<script type=\"text/javascript\">alert('{$item_str}の終了時刻を入力してください。');</script>");
                echo("<script language='javascript'>history.back();</script>");
                exit;
            }

            //残業時刻なしで休憩のみある場合のチェック
            $item_str = "残業時刻{$i}";
            if ($start_hour == "" && $start_min == "" && $end_hour == "" && $end_min == "" &&
                    $rest_start_hour != "" && $rest_start_min != "" && $rest_end_hour != "" && $rest_end_min != "") {
                echo("<script type=\"text/javascript\">alert('{$item_str}の休憩時刻のみの登録はできません。');</script>");
                echo("<script language='javascript'>history.back();</script>");
                exit;
            }

        }
        //未入力チェック
        if ($_POST["no_apply"] != "y" && !$ovtm_input_chk_flg) {
            echo("<script type=\"text/javascript\">alert('残業時刻が入力されていません。');</script>");
            echo("<script language='javascript'>history.back();</script>");
            exit;
        }
    }


    /**
     * get_btn_str更新ボタン等の情報を返す
     *
     * @param mixed $apply_id 申請ID
     * @param mixed $page 画面overtime_detail or overtime_back or tmmd_detail or tmmd_back
     * @return mixed ボタンの文字列
     *
     */
    function get_btn_str($apply_id, $page) {
        $btn_str = "";
        $obj = new atdbk_workflow_common_class($this->_db_con, $this->file_name);

        if ($page == "overtime_detail" || $page == "overtime_back") {
            $div = "OVTM";
        }
        else {
            $div = "TMMD";
        }
        // 申請・ワークフロー情報取得
        $arr_apply_wkfwmst = $obj->get_apply_wkfwmst($apply_id, $div);
        $apply_stat       = $arr_apply_wkfwmst[0]["apply_status"];
        $apply_date       = $arr_apply_wkfwmst[0]["apply_time"];
        $re_apply_id      = $arr_apply_wkfwmst[0]["re_apply_id"];
        $disapv_stat_flg = true;
        $tmp_arr_applyapv = $obj->get_applyapv($apply_id, $div);
        $arr_applyapv = $obj->get_applyapv_for_non_pertinence($tmp_arr_applyapv);
        foreach($arr_applyapv as $applyapv) {
            $apv_stat = $applyapv["aprv_status"];
            if($apv_stat != 0)
            {
                $disapv_stat_flg = false;
            }
        }

        $onkeydown_str = ($this->keyboard_input_flg == "t") ? " onblur=\"document.onkeydown=focusToNext;\" onfocus=\"document.onkeydown='';\"" : "";

		if($disapv_stat_flg == true && ($page == "overtime_detail" || $page == "timecard_detail")) {

            $btn_str = "<input type=\"button\" value=\"更新\" onclick=\"updateApply();\" $onkeydown_str>\n";
            $btn_str .= "<input type=\"button\" value=\"削除\" onclick=\"deleteApply();\" $onkeydown_str>";
		}
        else if($apply_stat != 3 && ($page == "overtime_detail" || $page == "timecard_detail")) {
            $btn_str = "";
		}
        else if($re_apply_id == "" && ($page == "overtime_back" || $page == "timecard_back"))	{
            $btn_str = "<input type=\"button\" value=\"再申請\" onclick=\"re_apply();\" $onkeydown_str>\n";
		}

        return $btn_str;

    }

}
?>