<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>ワークフロー｜印影登録</title>
<?
require("about_session.php");
require("about_authority.php");
require("about_postgres.php");
require("show_select_values.ini");
require("referer_common.ini");
require("get_values.ini");
require("jnl_application_imprint_common.ini");
require("./conf/sql.inf");

require("jnl_workflow_common.ini");
require_once("show_select_values.ini");

require_once("jnl_application_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,80,$fname);
if($wkfw=="0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}
// ワークフロー権限の取得
$workflow_auth = check_authority($session, 81, $fname);

$con = connect2db($fname);

// 遷移元の設定
if ($workflow != "") 
{
	set_referer($con, $session, "workflow", $workflow, $fname);
	$referer = $workflow;
} 
else 
{
	$referer = get_referer($con, $session, "workflow", $fname);
}




// 職員ID取得
$emp_id=get_emp_id($con,$session,$fname);


require_once("jnl_indicator_data.php");
require_once("jnl_indicator_display.php");
require_once("jnl_indicator_facility.php");
$indicatorData = new IndicatorData(array(
			'session' => $session,
			'fname'   => $fname,
			'con'     => $con
			));
$indicatorDisplay = new IndicatorDisplay(array(
			'session' => $session,
			'fname'   => $fname,
			'con'     => $con
			));
$indicatorFacility = new indicatorFacility(array(
			'session' => $session,
			'fname'   => $fname,
			'con'     => $con
			));



//登録実行
if($update_type == "1")
{
	
	$no =$rep_no;
	$plant = $rep_plant;
	
	//閲覧者情報の登録
	insert_jnl_input_member($con,$fname,$target_list,$rep_no,$ctg,$sub_ctg,$plant);
	
}
else
{
	//登録以外（表示時）にカテゴリ、サブカテゴリを取得する
	
	$noArr = $indicatorFacility->getNoForNo($no);
	$ctg = $noArr['category'];
	$sub_ctg = $noArr['sub_category'];
}




if($ctg == "0")
{
	//施設別の帳票を指定
	
	$sql = "select a.emp_id, b.emp_lt_nm,b.emp_ft_nm from jnl_inspector_list a, empmst b ";
	$cond = "where a.emp_id=b.emp_id and a.report_no='$no' and a.report_ctg='$ctg' and a.report_sub_ctg='$sub_ctg' and jnl_facility_id='$plant' order by a.sort_id";
	
}
else
{
	//施設別以外の帳票を指定
	
	$sql = "select a.emp_id, b.emp_lt_nm,b.emp_ft_nm from jnl_inspector_list a, empmst b ";
	$cond = "where a.emp_id=b.emp_id and a.report_no='$no' order by a.sort_id";
}



//画面初期表示用のデータを取得する
//日報・月報の閲覧対象者を保持するリスト
$arr_input_member_list = array();


$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) 
{
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

$num1 = pg_numrows($sel);

for ($i=0; $i<$num1; $i++) 
{
	$arr_input_member_list[$i]["id"] =  pg_result($sel,$i,"emp_id");
	$arr_input_member_list[$i]["name"] =  pg_result($sel,$i,"emp_lt_nm").pg_result($sel,$i,"emp_ft_nm");
}


?>


<script type="text/javascript">
<!--
function changeOptionType(e) {
    type = document.getElementById('hidden_type');

    if (e.value==type.value) {
        return;
    }
    else {
        plant = document.getElementById('plant');
        plant.innerHTML = '';
        if (e.value == 1) {
            hospital = document.getElementById('hospital');
            for (l=0; l < hospital.options.length; l++) {
                plant.options[l] = new Option(hospital.options[l].text, hospital.options[l].value);
            }
        }
        else if (e.value == 2) {
            healthFacilities = document.getElementById('health_facilities');
            for (l=0; l<healthFacilities.options.length; l++) {
                plant.options[l] = new Option(healthFacilities.options[l].text, healthFacilities.options[l].value);
            }
        }

        type.value = e.value;
        return changeOptionPlant(plant);
    }
}

function changeOptionPlant(e) {
    trHospital = document.getElementById('tr_hospital');
    trHealthFacilities = document.getElementById('tr_health_facilities');
    if (e.value=='') {
        trHospital.style.display = 'none';
        trHealthFacilities.style.display = 'none';
    }
    else {
        type = document.getElementById('hidden_type');
        if (type.value==1) {
            trHospital.style.display = '';
            trHealthFacilities.style.display = 'none';
        }
        else if (type.value==2) {
            trHospital.style.display = 'none';
            trHealthFacilities.style.display = '';
        }
    }
}

function clickLink(cNum, scNum) {
    wkfwForm = document.getElementById("wkfw");
    wkfwForm.action = 'jnl_application_indicator_menu.php';
    wkfwForm.method = 'get';

    plant = document.getElementById('plant');

    category = document.getElementById('category');
    category.value = cNum;

    subCategory = document.getElementById('sub_category');
    subCategory.value = scNum;

    type = document.getElementById('type');
    type.value = 'plant';

    wkfwForm.submit();
    return;
}

function selectYear(e, gCNum, gSCNum, gNo, gType, gPlant) {
    wkfw = document.getElementById('wkfw');
    wkfw.action = 'jnl_application_indicator_menu.php';
    wkfw.method = 'get';

    type = document.getElementById('type');
    category = document.getElementById('category');
    plant = document.getElementById('plant');
    subCategory = document.getElementById('sub_category');
    displayYear = document.getElementById('display_year');

    if ((type!=null && type.value=='plant') || (category!=null && category.value==0) && (subCategory!=null && subCategory.value=='')) {
        if (plant.value!=null) {
            hiddenType = document.getElementById('hidden_type');
            category.value = hiddenType.value;
            type.value = 'plant';
        }
        else {
            category.value = '0';
        }
        subCategory.parentNode.removeChild(subCategory);
    }
    else if (category==null) {
        alert('check');
        alert(category.value);
        alert(subCategory.value);
        alert(no.value);
    }
    else if (subCategory==null) {
        alert('check_a');
        alert(category.value);
    }

    displayYear.value = e.value;
    wkfw.submit();
    //return;
}


var childwin = null;
function openEmployeeList(item_id) {
	dx = screen.width;
	dy = screen.top;
	base = 0;
	wx = 720;
	wy = 600;
	var url = './emplist_popup.php';
	url += '?session=<?=$session?>';
	url += '&emp_id=<?=$emp_id?>';
	url += '&mode=9';
	url += '&item_id='+item_id;
	childwin = window.open(url, 'emplistpopup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

	childwin.focus();
}

// 職員指定画面から呼ばれる関数
function add_target_list(item_id, emp_id,emp_name)
{
	var emp_ids = emp_id.split(", ");
	var emp_names = emp_name.split(", ");

	var target_length = document.wkfw.target.options.length;
	
	
	//追加
	for(i=0;i<emp_ids.length;i++)
	{
		var in_group = false;
		emp_id = emp_ids[i];
		emp_name = emp_names[i];
		for (var k = 0, l = document.wkfw.target.options.length; k < l; k++) {
			if (document.wkfw.target[k].value == emp_id) {
				in_group = true;
				break;
			}
		}

		if (!in_group) {
			addOption(document.wkfw.target, emp_id, emp_name);
		}
	}
}

function addOption(box, value, text, selected) {

	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	if (selected == value) {
		opt.selected = true;
	}
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
}
// 削除ボタン
function deleteEmp() {

	for (var i = document.wkfw.target.options.length - 1; i >= 0; i--) {
		if (document.wkfw.target.options[i].selected) {
			document.wkfw.target.options[i] = null;
		}
	}
}


/* updateSetting 指定されたデータを登録するためにhiddenエリアにデータを設定し、submitする
**/	
function updateSetting(type) 
{

	//登録者設定
	document.wkfw.target_list.value = '';
	for (var i = 0, j = document.wkfw.target.options.length; i < j; i++) 
	{
		if (i > 0) 
		{
			document.wkfw.target_list.value += ',';
		}
		document.wkfw.target_list.value += document.wkfw.target.options[i].value;
	}
	
	//登録（更新）内容の種類を保持する
	document.wkfw.update_type.value = type;
	
	
	document.wkfw.submit();


}

//-->
</script>
<script type="text/javascript" src="js/jnl/jnl_inspector_list.js"></script>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
p {margin:0;}

table.block3 {border-collapse:collapse;}
table.block3 td {border:#5279a5 solid 0px;}
table.block3 td {border-width:0;}
</style>

<style type="text/css">
table#indicator {
    margin-left: auto;
    margin-right: auto;
}
table#indicator td {
    background-color: #ffffdd;
    padding: 20px;
    text-align: center;
}
</style>

</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<? 
if ($referer == "2") 
{ 
?>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr bgcolor="#f6f9ff">
					<td width="32" height="32" class="spacing">
						<a href="jnl_workflow_menu.php?session=<? echo($session); ?>"><img src="img/icon/b24.gif" width="32" height="32" border="0" alt="ワークフロー"></a>
					</td>
					<td>
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="jnl_workflow_menu.php?session=<? echo($session); ?>"><b>ワークフロー</b></a></font>
					</td>
				</tr>
			</table>
	<? 
} else { 
?>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr bgcolor="#f6f9ff">
					<td width="32" height="32" class="spacing">
						<a href="jnl_application_menu.php?session=<? echo($session); ?>"><img src="img/icon/b48.gif" width="32" height="32" border="0" alt="日報・月報"></a>
					</td>
					<td>
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="jnl_application_menu.php?session=<? echo($session); ?>"><b>日報・月報</b></a> &gt; <a href="jnl_workflow_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font>
					</td>
					<td align="right" style="padding-right:6px;">
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="jnl_application_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font>
					</td>
				</tr>
			</table>
	<? 
} 
?>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
<? show_wkfw_menuitem($session, $fname, ""); ?>
					<td></td>
				</tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
				</tr>
				<tr>
					<td><img src="img/spacer.gif" width="1" height="2" alt=""></td>
				</tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
				</tr>
			</table>



<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>

<form id="wkfw" name="wkfw" action="jnl_workflow_inspector_list.php" method="post" enctype="multipart/form-data">

<?php
/* menu list */
echo $indicatorDisplay->getRegisterMenuList(array(
			'category'     => $category,
			'sub_category' => $sub_category,
			'no'           => $no,
			'plant'        => $plant,
			'type'         => $type,
			),null,"日報月報帳票閲覧者設定");


?>

<?

if($no != "")
{
?>


	<table border="0" cellspacing="0" cellpadding="2" class="list">
		<tr height="22">
			<td colspan="2" align="center" bgcolor="C8D5FF">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>日報・月報帳票閲覧対象者</b></font><br>
			</td>
		</tr>
		<tr height="22">
			<td align="left"><input type="button" value="登録" onclick="updateSetting(1);"></td>
			<td>

				<table border="0" cellspacing="0" cellpadding="2" class="list">
					<tr>
						<td>
							<input type="button" name="emplist_btn" value="職員名簿" style="margin-left:2em;width=5.5em;" onclick="openEmployeeList('1');"><br>
						</td>
						<td>
							<select name="target" size="10" multiple style="width:150px;">
								<?show_options($arr_input_member_list);?>
							</select>
						</td>
						<td>
							<input type="button" name="delete_emp" value="削除" onclick="deleteEmp();">
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<img src="img/spacer.gif" alt="" width="1" height="10"><br>
	<input type="hidden" name="target_list" id="target_list" value="">
	<input type="hidden" name="rep_no"  value="<?=$no?>">
	<input type="hidden" name="rep_plant"  value="<?=$plant?>">
	<input type="hidden" name="update_type">

<?
}
?>


<input type="hidden" id="session" name="session" value="<? echo($session); ?>">

</form>
</td>
</tr>
</table>

</body>
<? pg_close($con); ?>

</html>

<?

//職員情報の登録
/**
 * insert_jnl_input_member 日報・月報の閲覧者を登録する
 *
 * @param mixed $con 接続情報
 * @param mixed $fname PHPファイル名
 * @param mixed $target_list 閲覧者対象職員リスト
 * @param mixed $no 登録する帳票番号
 * @param mixed $ctg 登録する帳票のカテゴリ番号
 * @param mixed $sub_ctg 登録する帳票のサブカテゴリ番号
 * @param mixed $sub_ctg 登録する帳票ID
 * 
 * @return なし
 *
 */
function insert_jnl_input_member($con,$fname,$target_list,$rep_no,$ctg,$sub_ctg,$_plant)
{
	
	//登録されているメンバーを一旦全削除する
	$sql = "delete from jnl_inspector_list ";
	
	if($ctg == "0")
	{
		//施設別帳票の登録・更新
		$cond = "where report_no='$rep_no' and report_ctg='$ctg' and report_sub_ctg='$sub_ctg' and jnl_facility_id='$_plant' ";
	}
	else
	{
		//施設別以外帳票の登録・更新
		$cond = "where report_no='$rep_no' and report_ctg='$ctg' and report_sub_ctg='$sub_ctg' ";
	}

	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) 
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	
	//登録職員ID
	$arr_target_list = split(",", $target_list);
	
	$lp_cnt = count($arr_target_list);
	
	if(1 == count($arr_target_list))
	{
		//データチェック
		if("" == $arr_target_list[0])
		{
			//入力無しでも1件となるのでデータが無ければ登録しない
			$lp_cnt = 0;
		}
	}
	
	for ($i=0; $i<$lp_cnt; $i++) 
	{
		$sql = "insert into jnl_inspector_list (sort_id, emp_id, report_no, report_ctg, report_sub_ctg, jnl_facility_id ";
		$sql .= ") values (";
		$content = array($i+1,$arr_target_list[$i],$rep_no,$ctg,$sub_ctg,$_plant);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) 
		{
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
}

/**
 * show_options 日報・月報の閲覧対象職員をセレクトリストのリスト欄に出力する項目を表示する
 *
 * @param mixed $arr_input_member_list 閲覧対象職員リスト
 * @return なし
 *
 */
function show_options($arr_input_member_list) 
{
	
	for ($i=0; $i<count($arr_input_member_list); $i++) 
	{
		$id = $arr_input_member_list[$i]["id"];
		$nm = $arr_input_member_list[$i]["name"];
		echo("<option value=\"$id\"");
		echo(">$nm</option>\n");
	}
	
}


?>
