<?php

//    require_once("about_comedix.php");
    require_once("about_postgres.php");

    $fname = $argv[0];

echo "### program start ###\n";

    // データベースに接続
    $con = connect2db($fname);


    // 引数を取得する
    if (is_null($argv[1])) {
    	$kijun_date = date("Ymd");	// 西暦年月
    }
    else {

    	$kijun_date = $argv[1];		// 西暦年月？
echo "argment=>".$argv[1]."\n";

        // 入力日付の妥当性チェック
		if (strlen($kijun_date) !== 8) {
echo "invalid argment -> format:yyyymmdd\n";
			exit;
		}
        elseif (check_date($kijun_date) == false) {
echo "invalid argment -> format:yyyymmdd\n";
        	exit;
        }
    }


echo "kijun_date=>>".$kijun_date."\n";

    // 基準日の翌日から次月まで
    $p_start = next_date($kijun_date);

	// 次月の締め日
    $p_end = next_month($p_start);

echo "selected between ".$p_start." -> ".$p_end."\n";

    // トランザクションの開始
    pg_query($con, "begin transaction");

echo "transaction start\n";


    // ※起動された日時（又は指定された日時）から一ヶ月間を保存する


    // 確定範囲を当初予定テーブルから削除する(初回空振り)
    $sql = "delete from atdbk_sche";
    $cond = "where date between '".$p_start."' and '".$p_end."'";
    $del = delete_from_table($con, $sql, $cond, $fname);

    $sql = "delete from duty_shift_plan_assist_sche";
    $cond = "where duty_date between '".$p_start."' and '".$p_end."'";
    $del = delete_from_table($con, $sql, $cond, $fname);

	// ※空振りをスルーします


    // 抽出の確認
    $sql = "select * from atdbk";
    $cond = "where date between '".$p_start."' and '".$p_end."'";
    $sel = select_from_table($con, $sql, $cond, $fname);

    $count = pg_num_rows($sel);

    echo "atdbk : target ".$count." records\n";

    if ($count > 0 ) {
        // 確定範囲を出勤予定から抽出し当初予定に挿入
        $sql = "insert into atdbk_sche select * from atdbk ";
        $sql.= "where date between '".$p_start."' and '".$p_end."'";
        $ins = insert_into_table_no_content($con, $sql, $fname);

        if ($ins == 0) {
            echo "insert faire atdbk : return (".$ins.")\n";
        	pg_query($con,"rollback");
          	pg_close($con);
           	exit;
        }
    }

    // 抽出の確認
    $sql = "select * from duty_shift_plan_assist";
    $cond = "where duty_date between '".$p_start."' and '".$p_end."'";
    $sel = select_from_table($con, $sql, $cond, $fname);

    $count = pg_num_rows($sel);

    echo "duty_shift_plan_assist : target ".$count." records\n";

    if ($count > 0 ) {
        // 確定範囲を出勤予定から抽出し当初予定に挿入
        $sql = "insert into duty_shift_plan_assist_sche select * from duty_shift_plan_assist ";
        $sql.= "where duty_date between '".$p_start."' and '".$p_end."'";
        $ins = insert_into_table_no_content($con, $sql, $fname);

        if ($ins == 0) {
            echo "insert faire duty_shift_plan_assist : return (".$ins.")\n";
        	pg_query($con,"rollback");
          	pg_close($con);
           	exit;
        }
    }


    // トランザクションのコミット
    pg_query($con, "commit");

    echo "transaction end\n";

    // データベース接続を閉じる
    pg_close($con);

echo "### program end ###\n";

//------------------------------------------------------------------------------
// 関数
//------------------------------------------------------------------------------

// 日付の妥当性をチェックする
function check_date($yyyymmdd) {

	$y = intval(substr($yyyymmdd, 0, 4));
	$m = intval(substr($yyyymmdd, 4, 2));
	$d = intval(substr($yyyymmdd, 6, 2));

	return checkdate($m, $d, $y);	// true or false
}

// 日付をタイムスタンプに変換
function to_timestamp($yyyymmdd) {
	$y = intval(substr($yyyymmdd, 0, 4));
	$m = intval(substr($yyyymmdd, 4, 2));
	$d = intval(substr($yyyymmdd, 6, 2));

	return mktime(0, 0, 0, $m, $d, $y);
}

// 翌日日付をyyyymmdd形式で取得
function next_date($yyyymmdd) {
	return date("Ymd", strtotime("+1 day", to_timestamp($yyyymmdd)));
}

// 一ヵ月後の締め日をyyyymmddで取得
function next_month($yyyymmdd) {

	// パラメータ保存
	$y = intval(substr($yyyymmdd, 0, 4));
	$m = intval(substr($yyyymmdd, 4, 2));
	$d = intval(substr($yyyymmdd, 6, 2));

    $tmpdate = date("Ymd", strtotime("+1 month", to_timestamp($yyyymmdd)));

    // 2013/11/10 -> 2013/12/10                   : OK
	// 2013/12/10 -> 2013/13/10 -> 2014/01/10     : OK

    // カスタマイズ要件では10日締めですが
	// 2013/10/31 -> 2013/11/31 -> 2013/12/01     : NG
    // 2013/02/28 -> 2013/03/28                   : NG

	//月末を取得する
	$end_day = date('d', mktime(0, 0, 0, $m + 1, 0, $y));

	// 翌月末へ
    if ($d == $end_day) {
	    $tmpdate = date('Ymd', mktime(0, 0, 0, $m + 2, 0, $y));
    	// 2013/10/31 -> 2013/11/30 : OK
    	// 2013/02/28 -> 2013/03/31 : OK
	} else {
        $tmpdate = date("Ymd", strtotime("-1 day", to_timestamp($tmpdate)));
	}

	return $tmpdate;
}

?>
