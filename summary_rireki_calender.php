
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<style type="text/css">
	.center_all td { text-align:center; vertical-align:middle; }
</style>

<title>メドレポート｜カレンダー</title>
<? //require("conf/sql.inf"); ?>
<? require("about_postgres.php"); ?>
<? require("about_session.php"); ?>
<? require("about_authority.php"); ?>
<? require("time_check.ini"); ?>
<? require("project_check.ini"); ?>
<? require_once("summary_common.ini"); ?>
<?
$fname = $PHP_SELF;

$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 対象月のついたち
$time_mon_cur = mktime(0,0,0,date('m'), 1, date('Y'));
if(@$_REQUEST['date']) $time_mon_cur = $_REQUEST['date'];

$time_mon_next = strtotime(date("Y-m-01", $time_mon_cur) . "+1 month");// 翌月ついたち
$time_mon_prev = strtotime(date("Y-m-01", $time_mon_cur) . "-1 month");// 先月ついたち
$time_mon_2prev = strtotime(date("Y-m-01", $time_mon_cur) . "-2 month");// 先々月ついたち

//DBコネクション
$con = connect2db($fname);
$sel = select_from_table($con,"select emp_id from session where session_id='$session' limit 1", "",$fname);
$emp_id = @pg_result($sel,0,"emp_id");

// 何をやっているか不明(2010/3/17)
$timegd = @$_REQUEST["timegd"];
if($timegd == "on" || $timegd == "off") time_update($con,$emp_id,$timegd,$fname);
if($timegd == "") $timegd = time_check($con,$emp_id,$fname);

// 何をやっているか不明(2010/3/17)
$pjtgd = @$_REQUEST["pjtgd"];
if($pjtgd == "on" || $pjtgd == "off") project_update($con,$emp_id,$pjtgd,$fname);
if($pjtgd == "") $pjtgd = project_check($con,$emp_id,$fname);


// オプション設定情報から、日曜日が左か右かを取得
$sel = select_from_table($con, "select calendar_start1 from option where emp_id = '$emp_id'", "", $fname);
$calendar_start = @pg_fetch_result($sel, 0, "calendar_start1");
if (!$calendar_start) $calendar_start = 1;

// 一日の秒数
$ONE_DAY = 86400;

// オーダが出ている日付を取得
$sql =
	" select cre_date from summary".
	" where ptif_id = '" . pg_escape_string($pt_id)."'".
	" and cre_date >= '" . date("Ymd", $time_mon_2prev)."'".
	" and cre_date < '" . date("Ymd", $time_mon_next)."'";
$sel = select_from_table($con,$sql,"",$fname);
$summary_days = array();
while ($row = pg_fetch_array($sel)) $summary_days[$row["cre_date"]] = 1;

?>
</head>
<body>



<? //一覧／カレンダー切り替えボタン ?>
<div class="search_button_div" style="text-align:left">
	<input type="button" onclick="location.href='summary_rireki_list.php?session=<?=$session?>&pt_id=<?=$pt_id?>';" value="一覧"/>
	<input type="button" onclick="location.href='summary_rireki_calender.php?session=<?=$session?>&pt_id=<?=$pt_id?>';" value="カレンダー"/>
</div>

<center>

<? // 次月翌月リンク ?>
<? $linkurl = "summary_rireki_calender.php?session=".$session."&pt_id=".$pt_id."&key1=".@$key1."&wherefrom=1"; ?>
<table style="width:430px">
	<tr>
		<td><a href="<?=$linkurl?>&date=<?=$time_mon_prev?>"><img src="img/left.gif" alt=""></a></td>
		<td style="text-align:right"><a href="<?=$linkurl?>&date=<?=$time_mon_next?>"><img src="img/right.gif" alt=""></a></td>
	</tr>
</table>


<table style="width:430px">
	<tr>

		<? //-------------------------------------------------?>
		<? // 前月カレンダー                                  ?>
		<? //-------------------------------------------------?>
		<td style="vertical-align:top">
			<? // 年月ヘッダ ?>
			<div style="width:200px; background-color:#4baaf8; color:#fff; border:1px solid #bee0fc; padding:3px; text-align:center"><?=(int)date('Y', $time_mon_prev)?>&nbsp;年&nbsp;<?=(int)date('m', $time_mon_prev)?>&nbsp;月&nbsp;</div>

			<table class="prop_table center_all" cellspacing="1">
				<? // 曜日部分 ?>
				<tr>
					<? if ($calendar_start == 1) { ?><td style="color:#fff; background-color:#e382ad">日</td><? } ?>
					<td style="color:#fff; background-color:#a1a1a1">月</td>
					<td style="color:#fff; background-color:#a1a1a1">火</td>
					<td style="color:#fff; background-color:#a1a1a1">水</td>
					<td style="color:#fff; background-color:#a1a1a1">木</td>
					<td style="color:#fff; background-color:#a1a1a1">金</td>
					<td style="color:#fff; background-color:#5cd177">土</td>
					<? if ($calendar_start == 2) { ?><td style="color:#fff; background-color:#fcdfec">日</td><? } ?>
				</tr>
<?
				$basetime = $time_mon_prev; // ついたちのタイム
				$target_month = date("m", $time_mon_prev); // 月
				$youbi = date('w', $basetime); // ついたちの曜日
				if ($calendar_start == 1) $basetime = $basetime - ($youbi * $ONE_DAY); // 左上が日曜の場合の、左上のセルの日付
				if ($calendar_start == 2) {
					if ($youbi==0){
						$basetime = $basetime - (6 * $ONE_DAY); // 左上が月曜の場合の、左上のセルの日付(ついたちが日曜日の場合)
					} else {
						$basetime = $basetime - (($youbi-1) * $ONE_DAY); // 左上が月曜の場合の、左上のセルの日付（ついたちが日曜日以外）
					}
				}

				$basetime = $basetime - $ONE_DAY; // 左上の日付けから、さらに１日引いてスタート
				$cnt = 0;
				for (;;){
					$basetime += $ONE_DAY;
					$ymd = date("Ymd", $basetime);
					$cnt++;
					$s = ((int)date("m", $basetime)==$target_month ? (int)date("d", $basetime) : "・");
					if ($cnt % 7 == 1) echo '<tr height="30">';
					if (@$summary_days[$ymd] && $s!="・"){
						echo "<td style=\"background-color:#fefc8f; font-size:16px;\">";
						echo "<a href=\"./summary_rireki_naiyo.php?session=".$session."&pt_id=".$pt_id."&emp_id=".$emp_id."&date=".$ymd."\" target=hidari>".$s."</a>";
						echo "</td>";
					} else if ($s=="・") {
						echo "<td style='font-size:16px;'>・</td>";
					} else {
						echo "<td style='background-color:#eee; font-size:16px;'>".$s."</td>";
					}
					if ($cnt % 7 == 0) echo "</tr>";
					if (($cnt % 7 == 0) && ($s=="・")) break;
				}
?>
			</table>
		</td>



		<td width="30">&nbsp;</td>


		<? //-------------------------------------------------?>
		<? // 当月カレンダー                                  ?>
		<? //-------------------------------------------------?>
		<td style="vertical-align:top">
			<? // 年月ヘッダ ?>
			<div style="width:200px; background-color:#4baaf8; color:#fff; border:1px solid #bee0fc; padding:3px; text-align:center"><?=date('Y', $time_mon_cur)?>&nbsp;年&nbsp;<?=(int)date('m', $time_mon_cur)?>&nbsp;月&nbsp;</div>
			<table class="prop_table center_all" cellspacing="1">
				<? // 曜日部分 ?>
				<tr>
					<? if ($calendar_start == 1) { ?><td style="color:#fff; background-color:#e382ad">日</td><? } ?>
					<td style="color:#fff; background-color:#a1a1a1">月</td>
					<td style="color:#fff; background-color:#a1a1a1">火</td>
					<td style="color:#fff; background-color:#a1a1a1">水</td>
					<td style="color:#fff; background-color:#a1a1a1">木</td>
					<td style="color:#fff; background-color:#a1a1a1">金</td>
					<td style="color:#fff; background-color:#5cd177">土</td>
					<? if ($calendar_start == 2) { ?><td style="color:#fff; background-color:#fcdfec">日</td><? } ?>
				</tr>
<?
				$basetime = $time_mon_cur; // ついたちのタイム
				$target_month = date("m", $time_mon_cur); // 月
				$youbi = date('w', $basetime); // ついたちの曜日
				if ($calendar_start == 1) $basetime = $basetime - ($youbi * $ONE_DAY); // 左上が日曜の場合の、左上のセルの日付
				if ($calendar_start == 2) {
					if ($youbi==0){
						$basetime = $basetime - (6 * $ONE_DAY); // 左上が月曜の場合の、左上のセルの日付(ついたちが日曜日の場合)
					} else {
						$basetime = $basetime - (($youbi-1) * $ONE_DAY); // 左上が月曜の場合の、左上のセルの日付（ついたちが日曜日以外）
					}
				}
				$basetime = $basetime - $ONE_DAY; // 左上の日付けから、さらに１日引いてスタート
				$cnt = 0;
				for (;;){
					$basetime += $ONE_DAY;
					$ymd = date("Ymd", $basetime);
					$cnt++;
					$s = ((int)date("m", $basetime)==$target_month ? (int)date("d", $basetime) : "・");
					if ($cnt % 7 == 1) echo '<tr height="30">';
					if (@$summary_days[$ymd] && $s!="・"){
						echo "<td style=\"background-color:#fefc8f; font-size:16px;\">";
						echo "<a href=\"./summary_rireki_naiyo.php?session=".$session."&pt_id=".$pt_id."&emp_id=".$emp_id."&date=".$ymd."\" target=hidari>".$s."</a>";
						echo "</td>";
					} else if ($s=="・") {
						echo "<td style='font-size:16px;'>・</td>";
					} else {
						echo "<td style='background-color:#eee; font-size:16px;'>".$s."</td>";
					}
					if ($cnt % 7 == 0) echo "</tr>";
					if (($cnt % 7 == 0) && ($s=="・")) break;
				}
?>
			</table>
		</td>
	</tr>
</table>
</center>

</body>
<? pg_close($con); ?>
</html>
