<?
require_once('Cmx.php');
require_once('Cmx/View/Smarty.php');
require_once("hiyari_common.ini");
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_class_name.ini");
require_once("news_common.ini");
require_once("hiyari_header_class.php");
require_once('class/Cmx/Model/SystemConfig.php');

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 47, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

//デザイン（1=旧デザイン、2=新デザイン）
$sysConf = new Cmx_SystemConfig();
$design_mode = $sysConf->get('fantol.design.mode');

// デフォルト値の設定
if ($back == "") {
	$record_flg = "f";
}

// 日付の設定
if ($news_date1 == "") {$news_date1 = date("Y");}
if ($news_date2 == "") {$news_date2 = date("m");}
if ($news_date3 == "") {$news_date3 = date("d");}
if ($news_begin1 == "") {$news_begin1 = date("Y");}
if ($news_begin2 == "") {$news_begin2 = date("m");}
if ($news_begin3 == "") {$news_begin3 = date("d");}
if ($news_end1 == "") {$news_end1 = date("Y");}
if ($news_end2 == "") {$news_end2 = date("m");}
if ($news_end3 == "") {$news_end3 = date("d");}

// データベースに接続
$con = connect2db($fname);

// 組織名を取得
$arr_class_name = get_class_name_array($con, $fname);
$class_called = $arr_class_name[0];

// カテゴリ一覧を取得
$category_list = get_category_list($class_called);

// 部門一覧を取得
$class_list = get_class_list($con, $fname);

// 職種一覧を取得
$job_list = get_job_list($con, $fname);

//添付ファイルフォルダ
$newsfile_folder_name = get_newsfile_folder_name();

//添付ファイル
$file_list = array();
for ($i = 0; $i < count($filename); $i++){
  $file_list[$file_id[$i]]["name"] = $filename[$i];
  $file_list[$file_id[$i]]["ext"]  = strrchr($filename[$i], ".");
}

//日付の選択肢
$now  = date("Y");
$years = array();
for ($i = 0; $i < 2; $i++) {
    $years[$i] = $now + $i;
}
$months = array();
for ($i = 1; $i <= 12; $i++) {
    $months[$i] = sprintf("%02d", $i);
}
$days = array();
for ($i = 1; $i <= 31; $i++) {
    $days[$i] = sprintf("%02d", $i);
}

//==============================
//表示
//==============================
$smarty = new Cmx_View();

$smarty->assign("session", $session);
$smarty->assign("mode", "register");
$smarty->assign("action_file", "hiyari_news_insert.php");
$smarty->assign("action_btn", "登録");

//ヘッダ用データ
$header = new hiyari_header_class($session, $fname, $con);
$smarty->assign("INCIDENT_TITLE", $header->get_inci_title());
$smarty->assign("inci_auth_name", $header->get_inci_auth_name());
$smarty->assign("PAGE_TITLE", "お知らせ登録");
$smarty->assign("is_kanrisha", $header->is_kanrisha());
if ($design_mode == 1){
    $smarty->assign("tab_info", $header->get_news_tab_info());
    $smarty->assign("is_news_menu", false);}
else{
    $smarty->assign("tab_info", $header->get_user_tab_info());
}

//お知らせデータ
$smarty->assign("news_title", $news_title);
$smarty->assign("marker", $marker);

$smarty->assign("category_list", $category_list);
$smarty->assign("news_category", $news_category);

$smarty->assign("class_list", $class_list);
$smarty->assign("class_id", $class_id);

$smarty->assign("job_list", $job_list);
$smarty->assign("job_id", $job_id);

$smarty->assign("news", $news);

$smarty->assign("file_list", $file_list);
$smarty->assign("newsfile_folder_name", $newsfile_folder_name);

$smarty->assign("record_flg", $record_flg);

$smarty->assign("stt_years", $years);
$smarty->assign("end_years", $years);
$smarty->assign("reg_years", $years);
$smarty->assign("months", $months);
$smarty->assign("days", $days);
$smarty->assign("news_begin1", $news_begin1);
$smarty->assign("news_begin2", $news_begin2);
$smarty->assign("news_begin3", $news_begin3);
$smarty->assign("news_end1", $news_end1);
$smarty->assign("news_end2", $news_end2);
$smarty->assign("news_end3", $news_end3);
$smarty->assign("news_date1", $news_date1);
$smarty->assign("news_date2", $news_date2);
$smarty->assign("news_date3", $news_date3);

if ($design_mode == 1){
    $smarty->display("hiyari_news_edit1.tpl");
}
else{
    $smarty->display("hiyari_news_edit2.tpl");
}

