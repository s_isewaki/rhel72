<?
require_once("about_comedix.php");
?>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<?
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	js_login_exit();
}

// 権限のチェック
$check_auth = check_authority($session, 14, $fname);
if ($check_auth == "0") {
	js_login_exit();
}

// データベースに接続
$con = connect2db($fname);

// トランザクションの開始
pg_query($con, "begin");

// 入院情報の取得
$sql = "select * from inptmst";
$cond = q("where ptif_id = '%s'", $ptif_id);
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}
$bldg_cd = pg_fetch_result($sel, 0, "bldg_cd");
$ward_cd = pg_fetch_result($sel, 0, "ward_cd");
$ptrm_room_no = pg_fetch_result($sel, 0, "ptrm_room_no");
$inpt_lt_kn_nm = pg_fetch_result($sel, 0, "inpt_lt_kn_nm");
$inpt_ft_kn_nm = pg_fetch_result($sel, 0, "inpt_ft_kn_nm");
$inpt_lt_kj_nm = pg_fetch_result($sel, 0, "inpt_lt_kj_nm");
$inpt_ft_kj_nm = pg_fetch_result($sel, 0, "inpt_ft_kj_nm");
$inpt_keywd = pg_fetch_result($sel, 0, "inpt_keywd");
$inpt_arrival = pg_fetch_result($sel, 0, "inpt_arrival");
$inpt_enti_id = pg_fetch_result($sel, 0, "inpt_enti_id");
$inpt_sect_id = pg_fetch_result($sel, 0, "inpt_sect_id");
$dr_id = pg_fetch_result($sel, 0, "dr_id");
$nurse_id = pg_fetch_result($sel, 0, "nurse_id");
$inpt_sample_blood = pg_fetch_result($sel, 0, "inpt_sample_blood");
$inpt_purpose_rireki = pg_fetch_result($sel, 0, "inpt_purpose_rireki");
if ($inpt_purpose_rireki == "") {$inpt_purpose_rireki = null;}
$inpt_purpose_cd = pg_fetch_result($sel, 0, "inpt_purpose_cd");
$inpt_purpose_content = pg_fetch_result($sel, 0, "inpt_purpose_content");
$inpt_short_stay = pg_fetch_result($sel, 0, "inpt_short_stay");
if ($inpt_short_stay == "") {$inpt_short_stay = null;}
$inpt_outpt_test = pg_fetch_result($sel, 0, "inpt_outpt_test");
if ($inpt_outpt_test == "") {$inpt_outpt_test = null;}
$inpt_diagnosis = pg_fetch_result($sel, 0, "inpt_diagnosis");
$inpt_nrs_obsv = pg_fetch_result($sel, 0, "inpt_nrs_obsv");
$inpt_free = pg_fetch_result($sel, 0, "inpt_free");
$inpt_special = pg_fetch_result($sel, 0, "inpt_special");
$inpt_bed_no = pg_fetch_result($sel, 0, "inpt_bed_no");
$inpt_disease = pg_fetch_result($sel, 0, "inpt_disease");
$inpt_patho_from = pg_fetch_result($sel, 0, "inpt_patho_from");
$inpt_patho_to = pg_fetch_result($sel, 0, "inpt_patho_to");
$inpt_in_dt = pg_fetch_result($sel, 0, "inpt_in_dt");
$inpt_in_tm = pg_fetch_result($sel, 0, "inpt_in_tm");
$inpt_ope_dt_flg = pg_fetch_result($sel, 0, "inpt_ope_dt_flg");
$inpt_ope_dt = pg_fetch_result($sel, 0, "inpt_ope_dt");
$inpt_ope_ym = pg_fetch_result($sel, 0, "inpt_ope_ym");
$inpt_ope_td = pg_fetch_result($sel, 0, "inpt_ope_td");
$inpt_emergency = pg_fetch_result($sel, 0, "inpt_emergency");
$inpt_in_plan_period = pg_fetch_result($sel, 0, "inpt_in_plan_period");
$inpt_in_way = pg_fetch_result($sel, 0, "inpt_in_way");
$inpt_nursing = pg_fetch_result($sel, 0, "inpt_nursing");
$inpt_staple_fd = pg_fetch_result($sel, 0, "inpt_staple_fd");
$inpt_fd_type = pg_fetch_result($sel, 0, "inpt_fd_type");
$inpt_fd_dtl = pg_fetch_result($sel, 0, "inpt_fd_dtl");
$inpt_wish_rm_rireki = pg_fetch_result($sel, 0, "inpt_wish_rm_rireki");
if ($inpt_wish_rm_rireki == "") {$inpt_wish_rm_rireki = null;}
$inpt_wish_rm_cd = pg_fetch_result($sel, 0, "inpt_wish_rm_cd");
$inpt_insurance = pg_fetch_result($sel, 0, "inpt_insurance");
$inpt_insu_rate_rireki = pg_fetch_result($sel, 0, "inpt_insu_rate_rireki");
if ($inpt_insu_rate_rireki == "") {$inpt_insu_rate_rireki = null;}
$inpt_insu_rate_cd = pg_fetch_result($sel, 0, "inpt_insu_rate_cd");
$inpt_reduced = pg_fetch_result($sel, 0, "inpt_reduced");
if ($inpt_reduced == "") {$inpt_reduced = null;}
$inpt_impaired = pg_fetch_result($sel, 0, "inpt_impaired");
if ($inpt_impaired == "") {$inpt_impaired = null;}
$inpt_insured = pg_fetch_result($sel, 0, "inpt_insured");
$inpt_fd_start = pg_fetch_result($sel, 0, "inpt_fd_start");
$inpt_observe = pg_fetch_result($sel, 0, "inpt_observe");
$inpt_meet_flg = pg_fetch_result($sel, 0, "inpt_meet_flg");
$inpt_dcb_cls = pg_fetch_result($sel, 0, "inpt_dcb_cls");
$inpt_dcb_bas = pg_fetch_result($sel, 0, "inpt_dcb_bas");
$inpt_dcb_exp = pg_fetch_result($sel, 0, "inpt_dcb_exp");
$inpt_rhb_cls = pg_fetch_result($sel, 0, "inpt_rhb_cls");
$inpt_rhb_bas = pg_fetch_result($sel, 0, "inpt_rhb_bas");
$inpt_rhb_exp = pg_fetch_result($sel, 0, "inpt_rhb_exp");
$inpt_privacy_flg = pg_fetch_result($sel, 0, "inpt_privacy_flg");
$inpt_privacy_text = pg_fetch_result($sel, 0, "inpt_privacy_text");
$inpt_pre_div = pg_fetch_result($sel, 0, "inpt_pre_div");
$inpt_care_no = pg_fetch_result($sel, 0, "inpt_care_no");
$inpt_care_grd_rireki = pg_fetch_result($sel, 0, "inpt_care_grd_rireki");
if ($inpt_care_grd_rireki == "") {$inpt_care_grd_rireki = null;}
$inpt_care_grd_cd = pg_fetch_result($sel, 0, "inpt_care_grd_cd");
$inpt_care_apv = pg_fetch_result($sel, 0, "inpt_care_apv");
$inpt_care_from = pg_fetch_result($sel, 0, "inpt_care_from");
$inpt_care_to = pg_fetch_result($sel, 0, "inpt_care_to");
$inpt_dis_grd_rireki = pg_fetch_result($sel, 0, "inpt_dis_grd_rireki");
if ($inpt_dis_grd_rireki == "") {$inpt_dis_grd_rireki = null;}
$inpt_dis_grd_cd = pg_fetch_result($sel, 0, "inpt_dis_grd_cd");
$inpt_dis_type_rireki = pg_fetch_result($sel, 0, "inpt_dis_type_rireki");
if ($inpt_dis_type_rireki == "") {$inpt_dis_type_rireki = null;}
$inpt_dis_type_cd = pg_fetch_result($sel, 0, "inpt_dis_type_cd");
$inpt_spec_name = pg_fetch_result($sel, 0, "inpt_spec_name");
$inpt_spec_from = pg_fetch_result($sel, 0, "inpt_spec_from");
$inpt_spec_to = pg_fetch_result($sel, 0, "inpt_spec_to");
$inpt_intro_inst_cd = pg_fetch_result($sel, 0, "inpt_intro_inst_cd");
$inpt_intro_sect_rireki = pg_fetch_result($sel, 0, "inpt_intro_sect_rireki");
if ($inpt_intro_sect_rireki == "") {$inpt_intro_sect_rireki = null;}
$inpt_intro_sect_cd = pg_fetch_result($sel, 0, "inpt_intro_sect_cd");
$inpt_intro_doctor_no = pg_fetch_result($sel, 0, "inpt_intro_doctor_no");
if ($inpt_intro_doctor_no == "") {$inpt_intro_doctor_no = null;}
$inpt_hotline = pg_fetch_result($sel, 0, "inpt_hotline");
if ($inpt_hotline == "") {$inpt_hotline = null;}
$inpt_exemption = pg_fetch_result($sel, 0, "inpt_exemption");
if ($inpt_exemption == "") {$inpt_exemption = null;}

// 入院予定情報の作成
$sql = "insert into inptres (ptif_id, bldg_cd, ward_cd, ptrm_room_no, inpt_lt_kn_nm, inpt_ft_kn_nm, inpt_lt_kj_nm, inpt_ft_kj_nm, inpt_keywd, inpt_enti_id, inpt_sect_id, dr_id, nurse_id, inpt_sample_blood, inpt_purpose_rireki, inpt_purpose_cd, inpt_purpose_content, inpt_diagnosis, inpt_special, inpt_bed_no, inpt_disease, inpt_in_res_dt_flg, inpt_in_res_dt, inpt_in_res_tm, inpt_ope_dt_flg, inpt_ope_dt, inpt_ope_ym, inpt_ope_td, inpt_emergency, inpt_in_plan_period, inpt_in_way, inpt_nursing, inpt_staple_fd, inpt_fd_type, inpt_fd_dtl, inpt_wish_rm_rireki, inpt_wish_rm_cd, inpt_insurance, inpt_fd_start, inpt_observe, inpt_meet_flg, inpt_privacy_flg, inpt_privacy_text, inpt_short_stay, inpt_outpt_test, inpt_insu_rate_rireki, inpt_insu_rate_cd, inpt_reduced, inpt_impaired, inpt_insured, inpt_arrival, inpt_patho_from, inpt_patho_to, inpt_dcb_cls, inpt_dcb_bas, inpt_dcb_exp, inpt_rhb_cls, inpt_rhb_bas, inpt_rhb_exp, inpt_care_no, inpt_care_grd_rireki, inpt_care_grd_cd, inpt_care_apv, inpt_care_from, inpt_care_to, inpt_dis_grd_rireki, inpt_dis_grd_cd, inpt_dis_type_rireki, inpt_dis_type_cd, inpt_spec_name, inpt_spec_from, inpt_spec_to, inpt_pre_div, inpt_intro_inst_cd, inpt_intro_sect_rireki, inpt_intro_sect_cd, inpt_intro_doctor_no, inpt_hotline, inpt_nrs_obsv, inpt_free, in_res_updated, inpt_exemption) values (";
$content = array($ptif_id, $bldg_cd, $ward_cd, $ptrm_room_no, $inpt_lt_kn_nm, $inpt_ft_kn_nm, $inpt_lt_kj_nm, $inpt_ft_kj_nm, $inpt_keywd, $inpt_enti_id, $inpt_sect_id, $dr_id, $nurse_id, $inpt_sample_blood, $inpt_purpose_rireki, $inpt_purpose_cd, $inpt_purpose_content, $inpt_diagnosis, $inpt_special, $inpt_bed_no, $inpt_disease, "t", $inpt_in_dt, $inpt_in_tm, $inpt_ope_dt_flg, $inpt_ope_dt, $inpt_ope_ym, $inpt_ope_td, $inpt_emergency, $inpt_in_plan_period, $inpt_in_way, $inpt_nursing, $inpt_staple_fd, $inpt_fd_type, $inpt_fd_dtl, $inpt_wish_rm_rireki, $inpt_wish_rm_cd, $inpt_insurance, $inpt_fd_start, $inpt_observe, $inpt_meet_flg, $inpt_privacy_flg, $inpt_privacy_text, $inpt_short_stay, $inpt_outpt_test, $inpt_insu_rate_rireki, $inpt_insu_rate_cd, $inpt_reduced, $inpt_impaired, $inpt_insured, $inpt_arrival, $inpt_patho_from, $inpt_patho_to, $inpt_dcb_cls, $inpt_dcb_bas, $inpt_dcb_exp, $inpt_rhb_cls, $inpt_rhb_bas, $inpt_rhb_exp, $inpt_care_no, $inpt_care_grd_rireki, $inpt_care_grd_cd, $inpt_care_apv, $inpt_care_from, $inpt_care_to, $inpt_dis_grd_rireki, $inpt_dis_grd_cd, $inpt_dis_type_rireki, $inpt_dis_type_cd, $inpt_spec_name, $inpt_spec_from, $inpt_spec_to, $inpt_pre_div, $inpt_intro_inst_cd, $inpt_intro_sect_rireki, $inpt_intro_sect_cd, $inpt_intro_doctor_no, $inpt_hotline, $inpt_nrs_obsv, $inpt_free, date("YmdHis"), $inpt_exemption);
$ins = insert_into_table($con, $sql, q($content), $fname);
if ($ins == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}

// 入院情報の削除
$sql = "delete from inptmst";
$cond = q("where ptif_id = '%s'", $ptif_id);
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}

// 空床情報を作成
$sql = "insert into inptmst (inpt_enti_id, bldg_cd, ward_cd, ptrm_room_no, inpt_bed_no) values (";
$content = array($inpt_enti_id, $bldg_cd, $ward_cd, $ptrm_room_no, $inpt_bed_no);
$ins = insert_into_table($con, $sql, q($content), $fname);
if ($ins == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}

// チェックリスト情報の削除
$sql = "delete from inptq";
$cond = q("where ptif_id = '%s'", $ptif_id);
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}

// 栄養問診表情報の削除
$sql = "delete from inptnut";
$cond = q("where ptif_id = '%s'", $ptif_id);
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}

// 入院状況情報の削除
$sql = "delete from inptcond";
$cond = q("where ptif_id = '%s' and hist_flg = 'f'", $ptif_id);
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}

// 移転情報の削除
$sql = "delete from inptmove";
$cond = q("where ptif_id = '%s' and to_timestamp(move_dt || move_tm, 'YYYYMMDDHH24MI') >= to_timestamp('%s%s', 'YYYYMMDDHH24MI')", $ptif_id, $inpt_in_dt, $inpt_in_tm);
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}

// 外出情報の削除
$sql = "delete from inptgoout";
$cond = q("where ptif_id = '%s' and out_type = '1' and to_timestamp(out_date || out_time, 'YYYYMMDDHH24MI') >= to_timestamp('%s%s', 'YYYYMMDDHH24MI')", $ptif_id, $inpt_in_dt, $inpt_in_tm);
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}

// 外泊情報の削除
$sql = "delete from inptgoout";
$cond = q("where ptif_id = '%s' and out_type = '2' and to_date(out_date, 'YYYYMMDD') >= to_date('%s', 'YYYYMMDD')", $ptif_id, $inpt_in_dt);
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}

// 入院分の担当者情報を退避
$sql = "select emp_id, order_no from inptop";
$cond = q("where ptif_id = '%s' order by order_no", $ptif_id);
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}

// 入院分の担当者情報を削除
$sql = "delete from inptop";
$cond = q("where ptif_id = '%s'", $ptif_id);
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}

// 入院予定分の担当者情報を作成
while ($row = pg_fetch_array($sel)) {
	$sql = "insert into inptopres (ptif_id, emp_id, order_no) values (";
	$content = array($ptif_id, $row["emp_id"], $row["order_no"]);
	$ins = insert_into_table($con, $sql, q($content), $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		js_error_exit();
	}
}

// 入院予定表の備考を移動（念のため削除してから）
$sql = "delete from bed_innote";
$cond = q("where ptif_id = '%s' and type = 'p' and mode = 'r' and in_dttm is null and back_flg = 'f'", $ptif_id);
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}
$sql = "update bed_innote set";
$set = array("mode");
$setvalue = array("r");
$cond = q("where ptif_id = '%s' and type = 'p' and mode = 'i' and in_dttm is null and back_flg = 'f'", $ptif_id);
$upd = update_set_table($con, $sql, $set, q($setvalue), $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}

// トランザクションのコミット
//pg_query($con, "rollback");
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 患者一覧画面を再表示
?>
<form name="reloadform">
<? if ($list_type == "2") { ?>
<input type="hidden" name="href" value="inpatient_list.php?session=<? ehu($session); ?>&bldgwd=<? ehu($bldgwd); ?>&list_sect_id=<? ehu($list_sect_id); ?>&order=<? ehu($order); ?>&page=<? ehu($page); ?>">
<? } else if ($list_type == "A") { ?>
<input type="hidden" name="href" value="inpatient_date_list.php?session=<? ehu($session); ?>&bldgwd=<? ehu($bldgwd); ?>&list_sect_id=<? ehu($list_sect_id); ?>&list_year=<? ehu($list_year); ?>&list_month=<? ehu($list_month); ?>&list_day=<? ehu($list_day); ?>&list_year2=<? ehu($list_year2); ?>&list_month2=<? ehu($list_month2); ?>&list_day2=<? ehu($list_day2); ?>&order=<? ehu($order); ?>&page=<? ehu($page); ?>">
<? } ?>
</form>
<script type="text/javascript">
location.href = document.reloadform.href.value;
</script>
</body>
