<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>病床管理｜病室詳細</title>
<? require("about_authority.php"); ?>
<? require("about_session.php"); ?>
<? require("about_postgres.php"); ?>
<? require("./conf/sql.inf"); ?>
<? require("./show_select_values.ini"); ?>
<? require("./show_bed_info_detail.ini"); ?>
<? require("./show_res_info_detail.ini"); ?>
<?
//ページ名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//病床管理権限チェック
$auth = check_authority($session,14,$fname);
if($auth == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 診療科登録権限を取得
$section_admin_auth = check_authority($session, 28, $fname);

// 病棟登録権限を取得
$ward_admin_auth = check_authority($session, 21, $fname);

// 日付の設定
if ($dt == "") {
	$dt = date("Ymd");
}
$dt_y = substr($dt, 0, 4);
$dt_m = substr($dt, 4, 2);
$dt_d = substr($dt, 6, 2);
$p = mktime(0, 0, 0, $dt_m, $dt_d, $dt_y);
$p1 = date("Ymd", strtotime("+1 day", $p));
$p2 = date("Ymd", strtotime("+1 week", $p));
$p3 = date("Ymd", strtotime("-1 day", $p));
$p4 = date("Ymd", strtotime("-1 week", $p));

// 退院（退院予定）時刻のデフォルト
$out_hr = date("G");
if (date("i") <= "29") {
	$out_min = "00";
} else {
	$out_min = "30";
}

//DBコネクション
$con = connect2db($fname);
?>
<script language="javascript">
function setCondDate(date,pid,nm){
	var cond_date = date;
	document.status.date.value = cond_date;
	var pid = pid;
	document.status.pt_id.value = pid;
	var nm = nm;
	document.status.nm.value = nm;
}

function setCond(cond){
	var cond = cond;
	if(cond == 9){
	  document.status.status.options[1].selected=true;
	}else if(cond == 2){
	  document.status.status.options[2].selected=true;
	}else if(cond == 6){
	  document.status.status.options[3].selected=true;
	}else if(cond == 7){
	  document.status.status.options[4].selected=true;
	}else if(cond == 8){
	  document.status.status.options[5].selected=true;
	}
	statusOnChange();
}

function initPage() {

	statusOnChange();

}

function statusOnChange() {

	var timeDisabled;

	switch (document.status.status.value) {
	case '2':
	case '3':
	case '9':
		timeDisabled = false;
		break;
	case '0':
	case '4':
	case '5':
	case '6':
	case '7':
	case '8':
	default:
		timeDisabled = true;
		break;
	}

	document.status.out_hr.disabled = timeDisabled;
	document.status.out_min.disabled = timeDisabled;

}

function sbmtOnClick() {
	if (document.status.pt_id.value == '') {
		alert('患者が選択されていません。');
		return;
	}

	if (document.status.date.value == '') {
		alert('日付が選択されていません。');
		return;
	}

	if (document.status.status.value == '0') {
		alert('状況が選択されていません。');
		return;
	}

	switch (document.status.status.value) {
	case '2':
	 	var url = 'bed_info_out_register.php';
		url += '?session=<? echo $session ?>';
		url += '&pt_id=' + document.status.pt_id.value;
		url += '&date=' + document.status.date.value;
		url += '&out_hr=' + document.status.out_hr.value;
		url += '&out_min=' + document.status.out_min.value;
		window.open(url, 'newwin', 'width=640,height=640,scrollbars=yes');
		break;
	case '9':
	 	var url = 'bed_info_out_reserve_register.php';
		url += '?session=<? echo $session ?>';
		url += '&pt_id=' + document.status.pt_id.value;
		url += '&date=' + document.status.date.value;
		url += '&out_hr=' + document.status.out_hr.value;
		url += '&out_min=' + document.status.out_min.value;
		window.open(url, 'newwin', 'width=640,height=600,scrollbars=yes');
		break;
	default:
		document.status.submit();
		break;
	}
}

</script>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
form {margin:0;}
table.submenu {border-collapse:collapse;}
table.submenu td {border:#5279a5 solid 1px;}
table.bed {border-collapse:collapse;}
table.bed td {border:solid #5279a5 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="bed_menu.php?session=<? echo($session); ?>"><img src="img/icon/b17.gif" width="32" height="32" border="0" alt="病床管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="bed_menu.php?session=<? echo($session); ?>"><b>病床管理</b></a></font></td>
<? if ($section_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="entity_menu.php?session=<? echo($session); ?>&entity=1"><b>管理画面へ</b></a></font></td>
<? } else if ($ward_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="building_list.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="115" align="center" bgcolor="#bdd1e7"><a href="bed_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床管理トップ</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#5279a5"><a href="ward_reference.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>病棟照会</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="inpatient_reserve_register_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入退院登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="bedmake_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ベットメイク</font></a></td>
<td width="5">&nbsp;</td>
<td width="125" align="center" bgcolor="#bdd1e7"><a href="bed_rate_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床利用率管理</font></a>
</td>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#bdd1e7"><a href="bed_date_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">在院日数管理</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="bed_patient_export.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者データ抽出</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src='img/spacer.gif' width='1' height='5' alt=''><br>
<? bldg_link($con,$session,$fname); ?>
<img src='img/spacer.gif' width='1' height='5' alt=''><br>
<? show_room_detail_list($con, $session, $bldg_cd, $ward_cd, $ptrm_room_no, $fname); ?>
<img src='img/spacer.gif' width='1' height='5' alt=''><br>
<? show_res_detail_list($con, $session, $bldg_cd, $ward_cd, $ptrm_room_no, $fname); ?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<? show_move_detail_list($con, $session, $bldg_cd, $ward_cd, $ptrm_room_no, $fname); ?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<? next_status($session,$bldg_cd,$ward_cd,$ptrm_room_no,$p1,$p2,$p3,$p4,$mode,$bldgwd1,$in_yr,$in_mon,$in_day,$in_hr,$in_min,$bk_bldg_cd,$wherefrom); ?>
<? show_inpt_status($con,$fname,$ward_cd,$bldg_cd,$ptrm_room_no,$dt); ?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="15" bgcolor="#8F6AFF"><img src="./images/spacer.gif" width="15" height="15" border="0"></td>
<td width="80"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院</font></td>
<td width="15" bgcolor="#6AB9FF"><img src="./images/spacer.gif" width="15" height="15" border="0"></td>
<td width="80"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退院予定</font></td>
<td width="15" bgcolor="#FFCCFF"><img src="./images/spacer.gif" width="15" height="15" border="0"></td>
<td width="80"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">転床</font></td>
<td width="15" bgcolor="#FFFF80"><img src="./images/spacer.gif" width="15" height="15" border="0"></td>
<td width="80"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">外出泊</font></td>
<td width="15" bgcolor="#F4427C"><img src="./images/spacer.gif" width="15" height="15" border="0"></td>
<td width="80"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">手術</font></td>
<td width="15" bgcolor="#FCD3D3"><img src="./images/spacer.gif" width="15" height="15" border="0"></td>
<td width="80"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">透析</font></td>
<td width="15" bgcolor="#D8D2FD"><img src="./images/spacer.gif" width="15" height="15" border="0"></td>
<td width="80"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">在院</font></td>
<td>&nbsp;</td>
</tr>
</table>

<img src="img/spacer.gif" width="1" height="5" alt="">

<!-- 病床状況登録 -->
<form action="inpatient_bed_status_insert.php" method="post" name="status">
<table width="600" border="0" cellspacing="0" cellpadding="1">
<td width="120" height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者氏名：</font></td>
<td width="480"><input name="nm" type="text" maxlength="20" size="15">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10">＊上記のカレンダーをクリックしてください</font><input type="hidden" name="pt_id"></td>
</tr>
<tr>
<td height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日付：</font></td>
<td><input name="date" type="text" maxlength="8" size="10"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10">&nbsp;＊8桁の数字で入力するか、上記のカレンダーをクリックしてください</font></td>
</tr>
<tr>
<td height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時刻：</font></td>
<td><select name="out_hr"><? show_select_hrs_0_23($out_hr); ?></select><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">：</font><select name="out_min"><option value="00"<? if ($out_min == "00") {echo " selected";} ?>>00</option><option value="30"<? if ($out_min == "30") {echo " selected";} ?>>30</option></select>&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10">＊「退院予定」「退院」の場合、選択してください</font></td>
</tr>
<tr>
<td height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">状況：</font></td>
<td><select name="status" onchange="statusOnChange();"><option value="0">選択してください</option><option value="9">退院予定</option><option value="2">退院</option><option value="6">手術</option><option value="7">透析</option><option value="8">在院</option></select>&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10">＊選択するか、下記のイメージをクリックしてください</font></td>
</tr>
<tr>
<td height="22">&nbsp;</td>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="17" height="22" bgcolor="#6AB9FF"><a href="javascript:void(0);" onClick="setCond(9);"><img src="./images/spacer.gif" width="17" height="17" border="0"></a></td>
<td width="80"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退院予定</font></td>
<td width="17" bgcolor="#6AE6FF"><a href="javascript:void(0);" onClick="setCond(2);"><img src="./images/spacer.gif" width="17" height="17" border="0"></a></td>
<td width="80"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退院</font></td>
<td width="17" bgcolor="#F4427C"><a href="javascript:void(0);" onClick="setCond(6);"><img src="./images/spacer.gif" width="17" height="17" border="0"></a></td>
<td width="80"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">手術</font></td>
<td width="17" bgcolor="#FCD3D3"><a href="javascript:void(0);" onClick="setCond(7);"><img src="./images/spacer.gif" width="17" height="17" border="0"></a></td>
<td width="80"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">透析</font></td>
<td width="17" bgcolor="#D8D2FD"><a href="javascript:void(0);" onClick="setCond(8);"><img src="./images/spacer.gif" width="17" height="17" border="0"></a></td>
<td width="80"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">在院</font></td>
<td>&nbsp;</td>
</tr>
</table>
<tr>
<td height="22" align="right" colspan="2"><input type="button" value="登録" onclick="sbmtOnClick();"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="in_dt" value="<? echo($in_dt); ?>">
<input type="hidden" name="dt" value="<? echo($dt); ?>">
<input type="hidden" name="ward_cd" value="<? echo($ward_cd); ?>">
<input type="hidden" name="bldg_cd" value="<? echo($bldg_cd); ?>">
<input type="hidden" name="ptrm_room_no" value="<? echo($ptrm_room_no); ?>">
<input type="hidden" name="mode" value="<? echo($mode); ?>">
<input type="hidden" name="bldgwd1" value="<? echo($bldgwd1); ?>">
<input type="hidden" name="in_yr" value="<? echo($in_yr); ?>">
<input type="hidden" name="in_mon" value="<? echo($in_mon); ?>">
<input type="hidden" name="in_day" value="<? echo($in_day); ?>">
<input type="hidden" name="in_hr" value="<? echo($in_hr); ?>">
<input type="hidden" name="in_min" value="<? echo($in_min); ?>">
<input type="hidden" name="bk_bldg_cd" value="<? echo($bk_bldg_cd); ?>">
<input type="hidden" name="wherefrom" value="<? echo($wherefrom); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
