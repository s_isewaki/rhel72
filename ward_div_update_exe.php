<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkuath = check_authority($session, 21, $fname);
if ($checkuath == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if (!is_array($disable_flg)) {
	$disable_flg = array();
}
$ward_divs = array();
foreach ($name as $tmp_idx => $tmp_wddv_nm) {
	$tmp_wddv_id = $tmp_idx + 1;
	$tmp_wddv_use_flg = (in_array($tmp_wddv_id, $disable_flg)) ? "f" : "t";

	if ($tmp_wddv_use_flg == "t" && $tmp_wddv_nm == "") {
		echo("<script type=\"text/javascript\">alert('区分名が入力されていません。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}

	$ward_divs[$tmp_wddv_id] = array("name" => $tmp_wddv_nm, "use_flg" => $tmp_wddv_use_flg);
}

$con = connect2db($fname);
pg_query($con, "begin");

// 病棟区分情報を更新
$sql = "update wddvmst set";
$set = array("wddv_nm", "wddv_use_flg");
foreach ($ward_divs as $tmp_wddv_id => $tmp_wddv) {
	$setvalue = array($tmp_wddv["name"], $tmp_wddv["use_flg"]);
	$cond = "where wddv_id = $tmp_wddv_id";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

pg_query($con, "commit");
pg_close($con);

// 病棟区分画面を再表示
echo("<script type=\"text/javascript\">location.href = 'ward_div_update.php?session=$session';</script>");
