<?


//==========================================================
//初期処理と認証
//==========================================================
ob_start();
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("get_values.php");
ob_end_clean();

$fname = $PHP_SELF;
$session = qualify_session($session,$fname);
$session_auth = check_authority($session, 47, $fname);
$con = @connect2db($fname);
if(!$session || !$session_auth || !$con){
  echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
  exit;
}
$font = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';

// member_idで編集可能かcheckする
$emp_id  = get_emp_id($con,$session,$fname);


//==========================================================
// パラメータ取得
//==========================================================
$command_mode = @$_REQUEST["command_mode"];
$guidance_html = @$_REQUEST["guidance_html"];
$stylesheet_html = @$_REQUEST["stylesheet_html"];



//==========================================================
// スクリプト更新
//==========================================================
if ($command_mode=="edit"){
    $guidance_html  = str_replace("<script" , "&lt;script", $guidance_html);
    $guidance_html  = str_replace("</script>" , "&lt;/script&gt;", $guidance_html);
    $guidance_html  = str_replace("<object" , "&lt;object", $guidance_html);
    $guidance_html  = str_replace("</object>" , "&lt;/object&gt;", $guidance_html);
    $guidance_html  = str_replace("<input" , "&lt;input", $guidance_html);
    $guidance_html  = str_replace("<img" , "&lt;img", $guidance_html);
    $guidance_html  = str_replace("<select" , "&lt;select", $guidance_html);
    $guidance_html  = str_replace("<textarea" , "&lt;textarea", $guidance_html);
    $guidance_html  = str_replace("<style" , "&lt;style", $guidance_html);
    $guidance_html  = str_replace("<body" , "&lt;/body", $guidance_html);
    $guidance_html  = str_replace("<meta" , "&lt;meta", $guidance_html);
    $guidance_html  = str_replace("<html" , "&lt;/html", $guidance_html);
    $guidance_html  = str_replace("<title" , "&lt;title", $guidance_html);
    $guidance_html  = str_replace("<a " , "&lt;a ", $guidance_html);
    $guidance_html  = str_replace("</a>" , "&lt;/a&gt;", $guidance_html);

    update_set_table($con, "delete from inci_analysis_rca_chart_guidance", array(), null, "", $fname);
    $sql = "insert into inci_analysis_rca_chart_guidance (stylesheet_html, guidance_html) values('".pg_escape_string($stylesheet_html)."','".pg_escape_string($guidance_html)."')";
    update_set_table($con, $sql, array(), null, "", $fname);
    header("Location: hiyari_analysis_rca_chart_guidance.php?session=".$session);
    die;
}


//==========================================================
// 内容の取得
//==========================================================
$sel = select_from_table($con, "select guidance_html, stylesheet_html from inci_analysis_rca_chart_guidance", "", $fname);
$guidance_html = pg_fetch_result($sel, 0,0);
$stylesheet_html = pg_fetch_result($sel, 0,1);



?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix RCA分析ガイダンス | ファントルくん | 出来事分析</title>
</head>
<script type="text/javascript" src="js/fontsize.js"></script>
<style type="text/css">
    form {margin:0;}
    table.list {border-collapse:collapse;}
    tr.list td {border:solid #35B341 1px;border-bottom:0;}
    table.list td {border:solid #35B341 1px;}
    table.headtab { border-collapse:collapse; }
    table.headtab th { padding:0; width:5px; }
    table.headtab td { padding:0px 6px; vertical-align:middle; text-align:center; white-space:nowrap }
    .always_green { color:#13781d; }
    .always_green:hover { color:#ff5809; }
    .engawa { width:5px; height:22px }
    .tab_space { padding:0; width:5px }
    .tab_active { background-color:#35b341; color:#fff; }
    .tab_deactive { background-color:#aee1b3; }
<?=$stylesheet_html?>
</style>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body style="padding:0; margin:0; background-color:#ddd" onload="bodyResized()" onresize="bodyResized()">

<script>

function getClientHeight(){
    if (document.body.clientHeight) return document.body.clientHeight;
    if (window.innerHeight) return window.innerHeight;
    if (document.documentElement && document.documentElement.clientHeight) {
        return document.documentElement.clientHeight;
    }
    return 0;
}
function getClientWidth(){
    if (document.body.clientWidth) return document.body.clientWidth;
    if (window.innerWidth) return window.innerWidth;
    if (document.documentElement && document.documentElement.clientWidth) {
        return document.documentElement.clientWidth;
    }
    return 0;
}

var TOP_TO_SCROLL_CONTENT_HEIGHT = 70;
var SCROLL_CONTENT_TO_BOTTOM_HEIGHT = 22;
function bodyResized(twice){
    var h = getClientHeight();
    var w = getClientWidth();
    if (h < 1 || w < 1) return;

    document.getElementById("scroll_div_guidance").style.height = (h - TOP_TO_SCROLL_CONTENT_HEIGHT - SCROLL_CONTENT_TO_BOTTOM_HEIGHT) + "px";
    document.getElementById("guidance_html").style.height = (h - TOP_TO_SCROLL_CONTENT_HEIGHT - SCROLL_CONTENT_TO_BOTTOM_HEIGHT - 240) + "px";
    document.getElementById("guidance_html").style.width = (w - 12) + "px";
//  document.getElementById("stylesheet_html").style.width = (w - 12) + "px";
    if (twice) return;
    setTimeout("bodyResized(true)", 100);
}

function changeTab(tabName){
    document.getElementById("guidance_div").style.display = (tabName=="guidance"    ? "" : "none");
    document.getElementById("edit_div").style.display =     (tabName=="edit"  ? "" : "none");
    document.getElementsByTagName("body")[0].style.backgroundColor = (tabName=="edit"  ? "#fff" : "#ddd");

    document.getElementById("maintab_guidance").style.display    = (tabName=="guidance"    ? "" : "none");
    document.getElementById("maintab_edit").style.display   = (tabName=="edit"   ? "" : "none");

    bodyResized();
}

</script>





<? //------------------------------------------------------------------------ ?>
<? // ヘッダー                                                                ?>
<? //------------------------------------------------------------------------ ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr height="32" bgcolor="#35B341">
    <td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>ＲＣＡ分析ガイダンス</b></font></td>
    <td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる"
        width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
    </tr>
</table>




<div style="padding:6px">


<div style="height:30px">
<div id="maintab_guidance" style="text-align:right"><?=$font?><a href="javascript:changeTab('edit');" class="always_green">編集</a></font></div>
<div id="maintab_edit" style="text-align:right; display:none"><?=$font?><a href="javascript:changeTab('guidance');" class="always_green">編集キャンセル</a></font></div>
</div>


<? //------------------------------------------------------------------------ ?>
<? // ガイダンス編集                                                          ?>
<? //------------------------------------------------------------------------ ?>
<? require_once("html_to_text_for_tinymce.php"); ?>

<script type="text/javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
    m_use_tyny_mce = true;
    tinyMCE.init({
        mode : "textareas",
        theme : "advanced",
        plugins : "preview,table,fullscreen,layer",
        //language : "ja_euc-jp",
        language : "ja",
        width : "98%",
        height : "500",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        content_css : "tinymce/tinymce_content.css",
        theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,fontsizeselect,|,forecolor,backcolor,|,removeformat",
        theme_advanced_buttons2 : "bullist,numlist,|,outdent,indent,|,hr,|,charmap,|,preview,|,undo,redo,|,fullscreen",
        theme_advanced_buttons3 : "tablecontrols,|,visualaid",
        theme_advanced_statusbar_location : "none",
        force_br_newlines : true,
        forced_root_block : '',
        force_p_newlines : false
    });
</script>
<div id="edit_div" style="display:none">
    <form name="frm" action="hiyari_analysis_rca_chart_guidance.php" method="post">
    <input type="hidden" name="session" value="<?=$session?>"/>
    <input type="hidden" name="command_mode" value="edit"/>
    <textarea id="guidance_html" name="guidance_html" style="width:98%; height:500px; background-color:#ffc; border:2px solid #aaa; padding:4px"><?=h($guidance_html)?></textarea>



    <!--div style="padding-top:10px"><?=$font?>スタイルシート</font></div>
    <textarea id="stylesheet_html" name="stylesheet_html" style="height:120px; padding:4px"><?=h($stylesheet_html)?></textarea><br/>
    <div style="padding-top:10px"><?=$font?>本文HTML</font></div>
    <textarea id="guidance_html" name="guidance_html" style="height:300px; background-color:#ffc; border:2px solid #aaa; padding:4px"><?=h($guidance_html)?></textarea-->


    <div style="color:#888; text-align:center; padding-top:4px"><?=$font?>セキュリティ上、保存できないタグ・書式があります。</font></div>
    <div style="text-align:center;"><input type="button" value="保存" onclick="document.frm.submit();" style="margin-top:10px" /></div>
    </form>
</div>



<? //------------------------------------------------------------------------ ?>
<? // データ引用タブ内                                                        ?>
<? //------------------------------------------------------------------------ ?>
<div id="guidance_div">
    <!-- 引用スクロール領域 -->
    <div id="scroll_div_guidance" style="overflow:scroll; border:#cccccc solid 1px; background-color:#fff">
        <div id="guidance_contents" style="padding:4px"><?=$guidance_html?></div>
    </div>
</div>



</div>
</body>
</html>

