<?
// ライブラリマスターからチーム計画マスターへコピー
// 新しいチーム計画マスターを作成
// 観察項目も作成
// 当初の仕様では、共用、看護、介護、リハビリの4職種別に入力ができるようになっていましたが
// 当面は職種別分けをしないことになりました
// 2012.8.1

//ソート条件定数
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 57, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);
$blank="";

$create_date = $selectyy."-".$selectmo."-".$selectdd;

//================================
// 新規登録しようとしている患者ID,プロブレムID,日付のチーム計画がすでに存在しているかチェック
// 他ユーザーが登録した可能性があるため
//================================
$last_date = date("Y-m-d H:i",strtotime($create_date));
$sql  = "SELECT COUNT(*) AS count ";
$sql .= " FROM sum_med_std_team  ";
$sql .= " WHERE ptif_id='$pt_id' AND problem_id=$problem_id AND create_date='$last_date' ";
$cond = "";
$sel = select_from_table($con,$sql,$cond,$fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	require_once("summary_common.ini");
	summary_common_show_error_page_and_die();
}
$master_count = pg_fetch_result($sel,0,"count");
if ( $master_count != 0 ){ // 登録しようとしている、患者ID、プロプレムID、日付が存在している
	$mode="already";
}else{
	//================================
	// ライブラリマスターを読み込む
	//================================
	$sql = "SELECT * FROM sum_med_std_wkfwmst";
	$cond = " WHERE med_wkfw_id=$wkfw_id ";
	$cond .= " ORDER BY med_wkfw_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		return false;
	}
	$libmaster_list = pg_fetch_all($sel);
	
	//====================================
	// ライブラリの観察項目を読み込む
	//====================================
	$sql = "SELECT * FROM sum_med_std_wkfwobse";
	$cond = " WHERE med_wkfw_id=$wkfw_id ";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		return false;
	}
	$libobse_list = pg_fetch_all($sel);
	$wk=count($libobse_list);
	
	//====================================
	// チーム計画マスターの新しいIDを作成
	//====================================
	if( $team_id == 0 && $updatetype != 99 ){ // 新規のときは、一旦キー値だけでINSERTして後方のUPDATE処理へ
		$sql="SELECT MAX(team_id) AS team_id FROM sum_med_std_team "; // 新しいteam_idを生成する処理
		$sel = select_from_table($con,$sql,"",$fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			require_once("summary_common.ini");
			summary_common_show_error_page_and_die();
		}
		$ready_team_id = pg_fetch_result($sel, 0, "team_id");
		if( pg_num_rows($sel) == 0 ){
			$new_team_id = 1;
		}else{
			$new_team_id = $ready_team_id + 1;
		}
	}
	
	// emp_idから、職名と氏名を得る
	$sql  = "SELECT J.job_nm AS job_name,E.emp_lt_nm||E.emp_ft_nm AS name ";
	$sql .= " FROM empmst AS E ";
	$sql .= " INNER JOIN jobmst AS J ON J.job_id=E.emp_job ";
	$sql .= " WHERE E.emp_id='$emp_id' ";
	$cond = "";
	$sel = select_from_table($con,$sql,$cond,$fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		require_once("summary_common.ini");
		summary_common_show_error_page_and_die();
	}
	$job_name = pg_fetch_result($sel,0,"job_name");
	$emp_name = pg_fetch_result($sel,0,"name");
	//====================================
	// チーム計画マスター作成
	//====================================
	$create_date = $selectyy."-".$selectmo."-".$selectdd;
	$sql  = "INSERT INTO sum_med_std_team (";
	$sql .= "team_id ,";		// プライマリキー
	$sql .= "problem_id ,";	// プロブレムID
	$sql .= "ptif_id ,";		// 患者ID
	$sql .= "create_date, ";	// 立案日
	$sql .= "def_value ,";	// 定義内容
	$sql .= "s_detail ,";		// S:症状・兆候の内容
	$sql .= "e_detail ,";		// E:原因･関連因子の内容
	$sql .= "risk_detail ,";	// 危険因子
	$sql .= "outcome ,";		// アウトカムの内容
	$sql .= "left_jobname,";	// 左側記載者職名
	$sql .= "left_emp_name ";	// 左側記載者氏名
	$sql .= ") VALUES (";
	
	$cont = array(
		$new_team_id,
		$problem_id,
		$pt_id,
		$create_date,
		$libmaster_list[0]["def_value"],
		$libmaster_list[0]["s_detail"],
		$libmaster_list[0]["e_detail"],
		$libmaster_list[0]["risk_detail"],
		$libmaster_list[0]["outcome"],
		$job_name,
		$emp_name
	);
	
	$ins = insert_into_table($con, $sql, $cont, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		summary_common_show_error_page_and_die();
	}
	// 共用
	$sql  = "INSERT INTO sum_med_priactice0 (";
	$sql .= "team_id,";
	$sql .= "kyoyo_op,";
	$sql .= "kyoyo_tp,";
	$sql .= "kyoyo_ep,";
	$sql .= "kyoyo_jobname,";
	$sql .= "kyoyo_emp_name,";
	$sql .= "c0_lock_flag ";
	$sql .= ") VALUES (";
	$cont = array(
		$new_team_id,
		$libmaster_list[0]["kyoyo_op"],
		$libmaster_list[0]["kyoyo_tp"],
		$libmaster_list[0]["kyoyo_ep"],
		$job_name,
		$emp_name,
		"f",
	);
	$ins = insert_into_table($con, $sql, $cont, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		summary_common_show_error_page_and_die();
	}
/*
	// 看護
	$sql  = "INSERT INTO sum_med_priactice1 (";
	$sql .= "team_id,";
	$sql .= "kango_op,";
	$sql .= "kango_tp,";
	$sql .= "kango_ep,";
	$sql .= "kango_jobname,";
	$sql .= "kango_emp_name,";
	$sql .= "c1_lock_flag ";
	$sql .= ") VALUES (";
	$cont = array(
		$new_team_id,
		$libmaster_list[0]["kango_op"],
		$libmaster_list[0]["kango_tp"],
		$libmaster_list[0]["kango_ep"],
		$job_name,
		$emp_name,
		"f",
	);
	$ins = insert_into_table($con, $sql, $cont, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		summary_common_show_error_page_and_die();
	}
	// 介護
	$sql  = "INSERT INTO sum_med_priactice2 (";
	$sql .= "team_id,";
	$sql .= "kaigo_op,";
	$sql .= "kaigo_tp,";
	$sql .= "kaigo_ep,";
	$sql .= "kaigo_jobname,";
	$sql .= "kaigo_emp_name,";
	$sql .= "c2_lock_flag ";
	$sql .= ") VALUES (";
	$cont = array(
		$new_team_id,
		$libmaster_list[0]["kaigo_op"],
		$libmaster_list[0]["kaigo_tp"],
		$libmaster_list[0]["kaigo_ep"],
		$job_name,
		$emp_name,
		"f",
	);
	$ins = insert_into_table($con, $sql, $cont, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		summary_common_show_error_page_and_die();
	}
	// リハビリ
	$sql  = "INSERT INTO sum_med_priactice3 (";
	$sql .= "team_id,";
	$sql .= "rihabiri_op,";
	$sql .= "rihabiri_tp,";
	$sql .= "rihabiri_ep,";
	$sql .= "rihabiri_jobname,";
	$sql .= "rihabiri_emp_name,";
	$sql .= "c3_lock_flag ";
	$sql .= ") VALUES (";
	$cont = array(
		$new_team_id,
		$libmaster_list[0]["rihabiri_op"],
		$libmaster_list[0]["rihabiri_tp"],
		$libmaster_list[0]["rihabiri_ep"],
		$job_name,
		$emp_name,
		"f",
	);
	$ins = insert_into_table($con, $sql, $cont, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		summary_common_show_error_page_and_die();
	}
*/	
	//====================================
	// チーム計画マスターの観察項目作成
	//====================================
	$sql  = "INSERT INTO sum_med_std_obse (";
	$sql .= "team_id ,";
	$sql .= "item_cd ";
	$sql .= " )VALUES( ";
	for($i=0; $i<count($libobse_list); $i++){
		$cont=array($new_team_id , $libobse_list[$i]["item_cd"]);
		$ins = insert_into_table($con, $sql, $cont, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			summary_common_show_error_page_and_die();
		}
	}
	$mode = "normal";
	pg_query($con, "commit");
}
pg_close($con);

?>
<script type="text/javascript">
parent.opener.teamSelect(<? echo $problem_id; ?>,"<? echo $create_date; ?>","<? echo $mode; ?>");
window.close();
</script>
