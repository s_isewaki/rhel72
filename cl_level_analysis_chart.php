<?php
require_once("about_comedix.php");
require_once("about_session.php");
require_once("about_postgres.php");
require_once("show_class_name.ini");
require_once("cl_common_log_class.inc");
require_once("cl_application_workflow_common_class.php");
require_once("cl_title_name.ini");
require_once("cl_level_analysis_util.php");
require_once("Cmx.php");
require_once("MDB2.php");

$log = new cl_common_log_class(basename(__FILE__));

$log->info(basename(__FILE__)." START");

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

$cl_title = cl_title_name();

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$obj = new cl_application_workflow_common_class($con, $fname);

/**
 * 志願申請、評価申請用の志願者取得（申請者名）
 */
$arr_emp = $obj->get_empmst($session);
$emp_id  = $arr_emp["0"]["emp_id"];

/**
 * データベースクローズ
 */
pg_close($con);

//==============================
//情報取得
//==============================
$PAGE_TITLE = "統計分析グラフ";

//==============================
//パラメータを取得
//==============================
$type   = !empty($_GET["type"])  ? $_GET["type"]  : 1;
$cache  = !empty($_GET["cache"]) ? $_GET["cache"] : 0;
$hanrei = !empty($_COOKIE["_cl_graph_hanrei"]) ? $_COOKIE["_cl_graph_hanrei"] : 1;
$h_size = !empty($_COOKIE["_cl_graph_h_size"]) ? $_COOKIE["_cl_graph_h_size"] : 9;
$z_size = !empty($_COOKIE["_cl_graph_z_size"]) ? $_COOKIE["_cl_graph_z_size"] : 9;

//==============================
//専用セッションを使用する。
//==============================
session_name("cl_level_analysis_sid");
session_start();

//##########################################################################
// データベース接続オブジェクト取得
//##########################################################################
if (count($_POST) > 0) {
	$log->debug("MDB2オブジェクト取得開始",__FILE__,__LINE__);
	$mdb2 = MDB2::connect(CMX_DB_DSN);
	if (PEAR::isError($mdb2)) {
		$log->error("MDB2オブジェクト取得エラー：".$mdb2->getDebugInfo(),__FILE__,__LINE__);
	}
	$log->debug("MDB2オブジェクト取得終了",__FILE__,__LINE__);

	get_analysis_data($mdb2, $emp_id);

	// DB切断
	$mdb2->disconnect();
	$log->debug("DB切断",__FILE__,__LINE__);
}

//==============================
//凡例
//==============================
$label = array();
if ($hanrei == 2) {
    //集計結果データ
    $stats     = $_SESSION['stats_result_info']['report_stats_data'];
    $v_sums    = $stats["stats_data_list"]["vertical_sums"];

    if ($type != 2) {
        $v_index = -1;
        foreach ($stats["vertical_options"] as $v) {
            $v_index++;
            $label[] = $v['easy_name'];
        }
    }
    else {
        $v_index = -1;
        foreach ($stats["vertical_options"] as $v) {
            $v_index++;
            if ($v_sums[$v_index] > 0) {
                $label[] = $v['easy_name'];
            }
        }
    }
}
// 凡例色
$lcolor = array(
    '#0000ff',
    '#00ff00',
    '#ff0000',
    '#00ffff',
    '#ffff00',
    '#80ffff',
    '#ff80ff',
    '#ffff80',
    '#800000',
    '#008000',
    '#000080',
    '#ff8000',
    '#80ff00',
    '#8000ff',
    '#ff0080',
    '#00ff80',
    '#0080ff',
    '#ff8080',
    '#80ff80',
    '#8080ff',
    '#008080',
    '#800080',
    '#808000',
    '#808080',
    '#400000',
    '#004000',
    '#000040',
    '#c00000',
    '#00c000',
    '#0000c0',
    '#00ff40',
    '#ff4000',
    '#4000ff',
);

//==============================
//HTML出力
//==============================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?php eh($cl_title);?> | <?php eh($PAGE_TITLE);?></title>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<script type="text/javascript">
$(document).ready(function() {

    // [オプション設定]を隠す
    $('#forms').hide();

    // アイコン枠
    $('#icon<?php eh($type);?>').css('border-color','#00f');

    // グラフアイコンのclickイベント
    $('#icon1').click(function() { location.href = chart_url(1); });
    $('#icon2').click(function() { location.href = chart_url(2); });
    $('#icon3').click(function() { location.href = chart_url(3); });

    // [オプション設定]のclickイベント
    $('#option').click(function() {
        $('#forms').toggle();
    });

    // グラフ更新ボタンのclickイベント
    $('#redraw').click(function() {
        // Cookieのセット
        $.cookie('_cl_graph_hanrei', $('input[name=hanrei]:checked').val());
        $.cookie('_cl_graph_h_size', $('#h_size').val());
        $.cookie('_cl_graph_z_size', $('#z_size').val());
        location.href = chart_url();
    });

});

function chart_url(type) {
    if (!type) {
        type = '<?php eh($type);?>';
    }
    return "cl_level_analysis_chart.php?cache=1&session=<?php eh($session);?>&<?php e(session_name());?>=<?php e(session_id());?>&type=" + type;
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
body {
    margin:0;
    padding:0;
    color: #000;
    background-color:#fff;
}
#header {
    background-color:#35B341;
    color: #fff;
    font-size: 16px;
    font-weight: bold;
    padding:4px 0 4px 10px;
}
a#close {
    display: block;
    position:absolute;
    top:2px;
    right:2px;
    width: 24px;
    height: 24px;
    text-indent: -10000px;
    background-image: url("img/icon/close.gif");
}
a#close:hover {
    background-image: url("img/icon/closeo.gif");
}
#chart {
    margin:5px;
    padding:10px;
    background-color:#F5FFE5;
    border: 1px solid #cfcfcf;
    border-radius: 10px;            /* CSS3草案 */
    -webkit-border-radius: 10px;    /* Safari,Google Chrome用 */
    -moz-border-radius: 10px;       /* Firefox用 */
}
#icons {
    float:left;
}
#icons span.icon {
    display: block;
    cursor: pointer;
    border: 1px solid #cfcfcf;
    margin-right: 2px;
    width: 45px;
    height: 45px;
    background: #fff no-repeat center center;
    float:left;
}
#icons span#icon1 {
    background-image: url("img/hiyari_chart1.gif");
}
#icons span#icon2 {
    background-image: url("img/hiyari_chart2.gif");
}
#icons span#icon3 {
    background-image: url("img/hiyari_chart3.gif");
}

#option {
    clear:both;
    cursor: pointer;
    font-size: 11px;
    color: #00f;
    margin: 5px 0 0 5px;
}
#forms {
    margin-left: 160px;
    font-size: 12px;
    line-height:150%;
}
#forms input, #forms select, #forms button {
    font-size: 12px;
}
.form {
    white-space:nowrap;
}
#graph {
    clear:both;
    width: 1030px;
    height: 550px;
    border: 1px solid #e5eed5;
    margin: 5px 0;
    background: url("img/ajax-loader2.gif") #fff no-repeat center center;
}
#graph img {
    width: 1030px;
    height: 550px;
}
#hanrei {
    border: 1px solid #e5eed5;
    margin: 5px 0;
    padding: 10px;
    background-color: #fff;
    font-size: 12px;
}
.label {
    white-space:nowrap;
}
.save {
    font-size: 11px;
}
</style>
</head>

<body>

<div id="header">
<?php eh($PAGE_TITLE);?>
<a href="javascript:window.close();" id="close">[閉じる]</a>
</div>

<div id="chart">

<div id="icons">
    <span class="icon" id="icon1"></span>
    <span class="icon" id="icon2"></span>
    <span class="icon" id="icon3"></span>
    <div id="option">[オプション設定]</div>
</div>

<div id="forms">
<span class="form">
    凡例：
    <label><input type="radio" name="hanrei" value="1" <?php if ($hanrei == 1) { echo "checked"; } ?> />グラフ(画像)の中に入れる</label>
    <label><input type="radio" name="hanrei" value="2" <?php if ($hanrei == 2) { echo "checked"; } ?> />グラフ(画像)の中に入れない</label>
</span><br />
<span class="form">
    凡例の文字サイズ：
    <select id="h_size">
        <option value="7"  <?php if ($h_size == 7)  { echo "selected"; } ?>>7</option>
        <option value="8"  <?php if ($h_size == 8)  { echo "selected"; } ?>>8</option>
        <option value="9"  <?php if ($h_size == 9)  { echo "selected"; } ?>>9</option>
        <option value="10" <?php if ($h_size == 10) { echo "selected"; } ?>>10</option>
        <option value="11" <?php if ($h_size == 11) { echo "selected"; } ?>>11</option>
        <option value="12" <?php if ($h_size == 12) { echo "selected"; } ?>>12</option>
    </select>
</span><br />
<span class="form">
    XY軸ラベルの文字サイズ：
    <select id="z_size">
        <option value="7"  <?php if ($z_size == 7)  { echo "selected"; } ?>>7</option>
        <option value="8"  <?php if ($z_size == 8)  { echo "selected"; } ?>>8</option>
        <option value="9"  <?php if ($z_size == 9)  { echo "selected"; } ?>>9</option>
        <option value="10" <?php if ($z_size == 10) { echo "selected"; } ?>>10</option>
        <option value="11" <?php if ($z_size == 11) { echo "selected"; } ?>>11</option>
        <option value="12" <?php if ($z_size == 12) { echo "selected"; } ?>>12</option>
    </select>
</span><br />
<button id="redraw" type="button">設定を保存してグラフを更新する</button>
</div>

<div id="graph">
<img
    alt="グラフ(描画には時間がかかります。しばらくお待ちください。)"
    src="cl_level_analysis_chart_image.php?cache=<?php eh($cache);?>&amp;session=<?php eh($session);?>&amp;<?php e(session_name());?>=<?php e(session_id());?>&amp;type=<?php eh($type);?>&amp;t=<?php echo time();?>"
/>
</div>

<?php if ($hanrei == 2) {?>
<hr size="0" />
<div id="hanrei">
<?php
    foreach ($label as $data) {
        $color = array_shift($lcolor);
?>
<span class="label"><span style="color:<?php eh($color);?>">■</span><?php eh($data);?><span>
<?php
    }
?>
</div>
<?php } ?>

<hr size="0" />
<div class="save">
<span style="color:#00f;">[グラフの保存方法]</span><br />
グラフにマウスカーソルを合わせて右クリックしてください。メニュー内の[名前を付けて画像を保存]を選択してください。
</div>

</div>

</body>
</html>
