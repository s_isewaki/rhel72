<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" method="post" action="library_folder_update.php">
<input type="hidden" name="folder_name" value="<?=$folder_name?>">
<input type="hidden" name="folder_type" value="<?=$folder_type?>">
<input type="hidden" name="folder_no" value="<?=$folder_no?>">
<input type="hidden" name="archive" value="<?=$archive?>">
<input type="hidden" name="arch_id" value="<?=$arch_id?>">
<input type="hidden" name="category" value="<?=$category?>">
<input type="hidden" name="cate_id" value="<?=$cate_id?>">
<input type="hidden" name="folder_id" value="<?=$folder_id?>">

<input type="hidden" name="private_flg1" value="<?=$private_flg1?>">
<input type="hidden" name="private_flg2" value="<?=$private_flg2?>">
<input type="hidden" name="ref_dept_st_flg" value="<?=$ref_dept_st_flg?>">
<input type="hidden" name="ref_dept_flg" value="<?=$ref_dept_flg?>">
<input type="hidden" name="ref_class_src" value="<?=$ref_class_src?>">
<input type="hidden" name="ref_atrb_src" value="<?=$ref_atrb_src?>">
<input type="hidden" name="parent_cate_id" value="<?=$parent_cate_id?>">
<input type="hidden" name="parent_folder_id" value="<?=$parent_folder_id?>">
<input type="hidden" name="link_arch_id" value="<?=$link_arch_id?>">
<input type="hidden" name="link_cate_id" value="<?=$link_cate_id?>">
<input type="hidden" name="link_folder_id" value="<?=$link_folder_id?>">
<input type="hidden" name="link_id" value="<?=$link_id?>">
<input type="hidden" name="update_auth_recursively" value="<?=$update_auth_recursively?>">
<?
if (is_array($hid_ref_dept)) {
    foreach ($hid_ref_dept as $tmp_dept_id) {
        echo("<input type=\"hidden\" name=\"ref_dept[]\" value=\"$tmp_dept_id\">\n");
    }
} else {
    $hid_ref_dept = array();
}
?>
<input type="hidden" name="ref_st_flg" value="<?=$ref_st_flg?>">
<?
if (is_array($ref_st)) {
    foreach ($ref_st as $tmp_st_id) {
        echo("<input type=\"hidden\" name=\"ref_st[]\" value=\"$tmp_st_id\">\n");
    }
} else {
    $ref_st = array();
}
?>
<input type="hidden" name="target_id_list1" value="<?=$target_id_list1?>">
<input type="hidden" name="target_id_list2" value="<?=$target_id_list2?>">
<?
if ($target_id_list1 != "") {
    $hid_ref_emp = split(",", $target_id_list1);
} else {
    $hid_ref_emp = array();
}
if ($archive=="4") {
    $upd_dept_flg = "1";
    $upd_st_flg = "1";
}



?>
<input type="hidden" name="upd_dept_st_flg" value="<?=$upd_dept_st_flg?>">
<input type="hidden" name="upd_dept_flg" value="<?=$upd_dept_flg?>">
<input type="hidden" name="upd_class_src" value="<?=$upd_class_src?>">
<input type="hidden" name="upd_atrb_src" value="<?=$upd_atrb_src?>">
<?
if (is_array($hid_upd_dept)) {
    foreach ($hid_upd_dept as $tmp_dept_id) {
        echo("<input type=\"hidden\" name=\"upd_dept[]\" value=\"$tmp_dept_id\">\n");
    }
} else {
    $hid_upd_dept = array();
}
?>
<input type="hidden" name="upd_st_flg" value="<?=$upd_st_flg?>">
<?
if (is_array($upd_st)) {
    foreach ($upd_st as $tmp_st_id) {
        echo("<input type=\"hidden\" name=\"upd_st[]\" value=\"$tmp_st_id\">\n");
    }
} else {
    $upd_st = array();
}
?>
<?
if ($target_id_list2 != "") {
    $hid_upd_emp = split(",", $target_id_list2);
} else {
    $hid_upd_emp = array();
}
?>
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="o" value="<?=$o?>">
<input type="hidden" name="path" value="<?=$path?>">
<input type="hidden" name="ref_toggle_mode" value="<?=$ref_toggle_mode?>">
<input type="hidden" name="upd_toggle_mode" value="<?=$upd_toggle_mode?>">
<input type="hidden" name="back" value="t">
</form>
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("library_common.php");
require_once("Cmx.php");
require_once("aclg_set.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$auth_id = ($path == "3") ? 63 : 32;
$checkauth = check_authority($session, $auth_id, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// 入力チェック
if ($folder_name == "") {
    echo("<script type=\"text/javascript\">alert('フォルダ名が入力されていません。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}
else{
    if (preg_match("/[\/:*?\"<>|'\\\]/", $folder_name)) {
        echo("<script type=\"text/javascript\">alert(\"フォルダ名に以下の文字は使えません。\\n\\\ / : * ? \\\" < > | '\");</script>\n");
        echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
        exit;
    }
    if (strlen($folder_name) > 60) {
        echo("<script type=\"text/javascript\">alert('フォルダ名が長すぎます。');</script>");
        echo("<script type=\"text/javascript\">document.items.submit();</script>");
        exit;
    }
}
if (strlen($folder_no) > 50) {
    echo("<script type=\"text/javascript\">alert('フォルダ番号が長すぎます。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}
// 入力チェック・フォルダ作成
if ($folder_type != 1) {
    if ($archive != "1" && (($ref_dept_flg != "1" && count($hid_ref_dept) == 0) || ($ref_st_flg != "1" && count($ref_st) == 0)) && count($hid_ref_emp) == 0 && !($archive == "4" && $private_flg2 == "t")) {
        echo("<script type=\"text/javascript\">alert('参照可能範囲が不正です。');</script>");
        echo("<script type=\"text/javascript\">document.items.submit();</script>");
        exit;
    }
    // 全体、部署の場合更新可能者確認、個人、委員会（所属者がいるため）は不要
    if (($archive == "2" || $archive == "3") && (($upd_dept_flg != "1" && count($hid_upd_dept) == 0) || ($upd_st_flg != "1" && count($upd_st) == 0)) && count($hid_upd_emp) == 0) {
        echo("<script type=\"text/javascript\">alert('更新可能範囲が不正です。');</script>");
        echo("<script type=\"text/javascript\">document.items.submit();</script>");
        exit;
    }
}
// 入力チェック・リンク作成
else {
    if (empty($link_id)) {
        echo("<script type=\"text/javascript\">alert('フォルダのリンクが選択されていません。');</script>");
        echo("<script type=\"text/javascript\">document.items.submit();</script>");
        exit;
    }
}
switch ($archive) {
case "1":  // 個人ファイル
case "2":  // 全体共有
    $private_flg = null;
    break;
case "3":  // 部署共有
    $private_flg = ($private_flg1 == "t") ? "t" : "f";
    break;
case "4":  // 委員会・WG共有
    $private_flg = ($private_flg2 == "t") ? "t" : "f";
    break;
}
if ($ref_dept_st_flg != "t") {$ref_dept_st_flg = "f";}
if ($ref_dept_flg == "") {$ref_dept_flg = null;}
if ($ref_st_flg == "") {$ref_st_flg = null;}
if ($upd_dept_st_flg != "t") {$upd_dept_st_flg = "f";}
if ($upd_dept_flg == "") {$upd_dept_flg = null;}
if ($upd_st_flg == "") {$upd_st_flg = null;}

// トランザクションを開始
pg_query($con, "begin");

// ログインユーザの職員情報を取得
$sql = "select emp_id, emp_class, emp_attribute, emp_dept, emp_st from empmst";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel_emp = select_from_table($con, $sql, $cond, $fname);
if ($sel_emp == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);
$emp_id = pg_fetch_result($sel_emp, 0, "emp_id");

$lock_show_flg = true;
$lock_show_all_flg = true;

//======================================================================================================================
// 管理画面以外、上位カテゴリ以外、個人以外の場合、更新権限確認
//======================================================================================================================
if ($path != "3" && $cate_id != "" && $arch_id != "1") {
    lib_cre_tmp_concurrent_table($con, $fname, $emp_id);
    $opt = array("is_admin"=>"", "is_login"=>"", "is_narrow_mode"=>1, "is_ignore_class_atrb"=>($arch_id==4 ? 1 : ""));
    if ($folder_id) {
        $sql = lib_get_libfolder_sql($emp_id, $lock_show_flg, $lock_show_all_flg, $arch_id, $cate_id, $folder_id, $opt);
    } else {
        $sql = lib_get_libcate_sql($emp_id, $lock_show_flg, $lock_show_all_flg, $arch_id, $cate_id, 0, 0, $opt);
    }
    $top_row = lib_get_top_row($con, $fname, $sql);
    $upd_flg = ($top_row["writable"] ? true : false);

    if ($upd_flg == false) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('更新権限がありません。');</script>");
        echo("<script type=\"text/javascript\">document.items.submit();</script>");
        exit;
    }
}



//======================================================================================================================
// 更新対象がカテゴリの場合
//======================================================================================================================
if ($folder_id == "") {
    // カテゴリ名を更新
    $sql = "update libcate set";
    $set = array("lib_cate_nm", "lib_cate_no", "private_flg", "ref_dept_st_flg", "ref_dept_flg", "ref_st_flg", "upd_dept_st_flg", "upd_dept_flg", "upd_st_flg", "upd_time");
    $setvalue = array($folder_name, $folder_no, $private_flg, $ref_dept_st_flg, $ref_dept_flg, $ref_st_flg, $upd_dept_st_flg, $upd_dept_flg, $upd_st_flg, date("YmdHis"));
    $cond = "where lib_cate_id = $cate_id";
    $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
    if ($upd == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);

//======================================================================================================================
// 更新対象がフォルダの場合
//======================================================================================================================
} else {
    // フォルダ情報を更新
    if ($folder_type != 1) {
        $sql = "update libfolder set";
        $set = array("folder_name", "folder_no", "private_flg", "ref_dept_st_flg", "ref_dept_flg", "ref_st_flg", "upd_dept_st_flg", "upd_dept_flg", "upd_st_flg", "upd_time");
        $setvalue = array($folder_name, $folder_no, $private_flg, $ref_dept_st_flg, $ref_dept_flg, $ref_st_flg, $upd_dept_st_flg, $upd_dept_flg, $upd_st_flg, date("YmdHis"));
        $cond = "where folder_id = $folder_id";
        $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
        if ($upd == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);
    }
    // リンク情報を更新
    else {
        $sql = "update libfolder set";
        $set = array("folder_name", "folder_no", "link_id", "upd_time");
        $setvalue = array($folder_name, $folder_no, $link_id, date("YmdHis"));
        $cond = "where folder_id = $folder_id";
        $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
        if ($upd == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);
    }
}

//======================================================================================================================
// 権限変更
//======================================================================================================================


// 更新対象がカテゴリの場合
if ($folder_id == "") {
    $refdept_tblname = "libcaterefdept";
    $refst_tblname = "libcaterefst";
    $refemp_tblname = "libcaterefemp";
    $upddept_tblname = "libcateupddept";
    $updst_tblname = "libcateupdst";
    $updemp_tblname = "libcateupdemp";
    $id_name = "lib_cate_id";
    $lib_id = $cate_id;
// 更新対象がフォルダの場合
} else {
    $refdept_tblname = "libfolderrefdept";
    $refst_tblname = "libfolderrefst";
    $refemp_tblname = "libfolderrefemp";
    $upddept_tblname = "libfolderupddept";
    $updst_tblname = "libfolderupdst";
    $updemp_tblname = "libfolderupdemp";
    $id_name = "folder_id";
    $lib_id = $folder_id;
}

// 参照可能部署情報を登録
$sql = "delete from $refdept_tblname where $id_name = $lib_id";
$del = delete_from_table($con, $sql, "", $fname);
if ($del == 0) libcom_goto_error_page_and_die($sql, $con);
foreach ($hid_ref_dept as $tmp_val) {
    list($tmp_class_id, $tmp_atrb_id, $tmp_dept_id) = split("-", $tmp_val);
    $sql = "insert into $refdept_tblname ($id_name, class_id, atrb_id, dept_id) values (";
    $content = array($lib_id, $tmp_class_id, $tmp_atrb_id, $tmp_dept_id);
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) libcom_goto_error_page_and_die($sql, $con);
}

// 参照可能役職情報を登録
$sql = "delete from $refst_tblname where $id_name = $lib_id";
$del = delete_from_table($con, $sql, "", $fname);
if ($del == 0) libcom_goto_error_page_and_die($sql, $con);
foreach ($ref_st as $tmp_st_id) {
    $sql = "insert into $refst_tblname ($id_name, st_id) values (";
    $content = array($lib_id, $tmp_st_id);
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) libcom_goto_error_page_and_die($sql, $con);
}

// 参照可能職員情報を登録
$sql = "delete from $refemp_tblname where $id_name = $lib_id";
$del = delete_from_table($con, $sql, "", $fname);
if ($del == 0) libcom_goto_error_page_and_die($sql, $con);
foreach ($hid_ref_emp as $tmp_emp_id) {
    $sql = "insert into $refemp_tblname ($id_name, emp_id) values (";
    $content = array($lib_id, $tmp_emp_id);
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) libcom_goto_error_page_and_die($sql, $con);
}

// 更新可能部署情報を登録
$sql = "delete from $upddept_tblname where $id_name = $lib_id";
$del = delete_from_table($con, $sql, "", $fname);
if ($del == 0) libcom_goto_error_page_and_die($sql, $con);
foreach ($hid_upd_dept as $tmp_val) {
    list($tmp_class_id, $tmp_atrb_id, $tmp_dept_id) = split("-", $tmp_val);
    $sql = "insert into $upddept_tblname ($id_name, class_id, atrb_id, dept_id) values (";
    $content = array($lib_id, $tmp_class_id, $tmp_atrb_id, $tmp_dept_id);
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) libcom_goto_error_page_and_die($sql, $con);
}

// 更新可能役職情報を登録
$sql = "delete from $updst_tblname where $id_name = $lib_id";
$del = delete_from_table($con, $sql, "", $fname);
if ($del == 0) libcom_goto_error_page_and_die($sql, $con);
foreach ($upd_st as $tmp_st_id) {
    $sql = "insert into $updst_tblname ($id_name, st_id) values (";
    $content = array($lib_id, $tmp_st_id);
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) libcom_goto_error_page_and_die($sql, $con);
}

// 更新可能職員情報を登録
$sql = "delete from $updemp_tblname where $id_name = $lib_id";
$del = delete_from_table($con, $sql, "", $fname);
if ($del == 0) libcom_goto_error_page_and_die($sql, $con);
foreach ($hid_upd_emp as $tmp_emp_id) {
    $sql = "insert into $updemp_tblname ($id_name, emp_id) values (";
    $content = array($lib_id, $tmp_emp_id);
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) libcom_goto_error_page_and_die($sql, $con);
}

//======================================================================================================================
// 階層下のフォルダの権限更新。リンクは無視する
//======================================================================================================================
if ($update_auth_recursively == "t") {
    $f_or_c = ($folder_id ? "folder":"cate");
    $where = ($folder_id ? " folder_id = ".$folder_id : " lib_cate_id = ".$cate_id);
    $appended = array();
    $folders = array();
    if (!$folder_id) {
        $sql =
        " select lf.folder_id as id from libfolder lf".
        " left outer join libtree lt on ( lt.child_id = lf.folder_id )".
        " where lf.lib_cate_id = ".$cate_id." and lf.link_id is null".
        " and lt.parent_id is null";
    } else {
        $sql =
        " select child_id as id from libtree".
        " where parent_id = ".$folder_id.
        " and child_id in (select folder_id from libfolder where link_id is null)";
    }
    $sel = select_from_table($con, $sql, "", $fname);
    if ($sel == 0) libcom_goto_error_page_and_die($sql, $con);
    while ($row = pg_fetch_assoc($sel)) $folders[]= $row["id"];
    for (;;) {
        if (!count($folders)) break;
        $_f = array_shift($folders);
        if (!$_f) continue;

        //----------------------------------------
        // フォルダ自体の権限変更
        //----------------------------------------
        $sql =
        " update libfolder set".
        " private_flg = base.private_flg".
        ",ref_dept_st_flg = base.ref_dept_st_flg".
        ",ref_dept_flg = base.ref_dept_flg".
        ",ref_st_flg = base.ref_st_flg".
        ",upd_dept_st_flg = base.upd_dept_st_flg".
        ",upd_dept_flg = base.upd_dept_flg".
        ",upd_st_flg = base.upd_st_flg".
        " from (".
        "     select * from lib".$f_or_c." where ".$where.
        " ) base".
        " where libfolder.folder_id = ".$_f;
        $upd = update_set_table($con, $sql, array(), array(), "", $fname);
        if ($upd == 0) libcom_goto_error_page_and_die($sql, $con);

        //----------------------------------------
        // 権限をいったん削除
        //----------------------------------------
        $tables = array("libfolderrefdept", "libfolderrefst", "libfolderrefemp", "libfolderupddept", "libfolderupdst", "libfolderupdemp");
        foreach($tables as $table_id) {
            $sql = "delete from ".$table_id." where folder_id = ".$_f;
            $del = delete_from_table($con, $sql, "", $fname);
            if ($del == 0) libcom_goto_error_page_and_die($sql, $con);
        }

        //----------------------------------------
        // 複製インサート
        //----------------------------------------
        $upd_or_ref = array("ref", "upd");
        foreach($upd_or_ref as $ur) {
            $sql =
            " insert into libfolder".$ur."dept (folder_id, class_id, atrb_id, dept_id)".
            " select ".$_f.", class_id, atrb_id, dept_id from lib".$f_or_c.$ur."dept".
            " where ".$where;
            $upd = update_set_table($con, $sql, array(), array(), "", $fname);
            if ($upd == 0) libcom_goto_error_page_and_die($sql, $con);

            $sql =
            " insert into libfolder".$ur."st (folder_id, st_id)".
            " select ".$_f.", st_id from lib".$f_or_c.$ur."st".
            " where ".$where;
            $upd = update_set_table($con, $sql, array(), array(), "", $fname);
            if ($upd == 0) libcom_goto_error_page_and_die($sql, $con);

            $sql =
            " insert into libfolder".$ur."emp (folder_id, emp_id)".
            " select ".$_f.", emp_id from lib".$f_or_c.$ur."emp".
            " where ".$where;
            $upd = update_set_table($con, $sql, array(), array(), "", $fname);
            if ($upd == 0) libcom_goto_error_page_and_die($sql, $con);
        }

        //----------------------------------------
        // 直下の子を収集
        //----------------------------------------
        $sql = "select child_id from libtree where parent_id = ".$_f." and child_id in (select folder_id from libfolder where link_id is null)";
        $sel = select_from_table($con, $sql, "", $fname);
        if ($sel == 0) libcom_goto_error_page_and_die($sql, $con);
        while ($row = pg_fetch_assoc($sel)) {
            $folders[]= $row["child_id"];
            if ($appended[$row["child_id"]]) { echo "循環参照エラー：".implode(",",$appended); die; }
            $appended[$row["child_id"]] = 1;
        }
    }
}




//======================================================================================================================
// 移動およびタイムスタンプ更新
//
// 説明
// ・folder_id : 変更前のfolder_id
// ・cate_id : 変更前のcate_id
// ・link_id : 変更後の親folder_id。カラの場合はカテゴリ直下にするということ。
// ・link_cate_id : 変更後のcate_id
// ・cur_parent_foler_id : 変更前の親フォルダ。以下で取得。無い場合はカテゴリ直下だったということ。
//======================================================================================================================
$cur_parent_folder_id = "";
if ($folder_id) $cur_parent_folder_id = lib_get_one($con, $fname, "select parent_id from libtree where child_id = ".$folder_id);

$content_type = ($folder_id == "" ? "category" : "folder");
$content_value = ($folder_id == "" ? $cate_id : $folder_id);

$move_msg = "";
if ($link_cate_id != $cate_id || $link_folder_id != $cur_parent_foler_id) {
    $move_msg = lib_move_content($con, $fname, $emp_id, $content_type, $content_value, $link_cate_id, $link_folder_id);
    if ($move_msg!="ok") libcom_goto_error_page_and_die("", $con);
    // 遷移先変更
    $arch_id = lib_get_one($con, $fname, "select lib_archive from libcate where lib_cate_id = ".$link_cate_id);
    $cate_id = $link_cate_id;
    $folder_id = $link_folder_id;
}

// トランザクションをコミット
pg_query($con, "commit");

$ff = ($path == "3" ? "library_admin_menu.php" : "library_list_all.php");
echo("<script type=\"text/javascript\">location.href = '".$ff."?session=$session&a=$arch_id&c=$cate_id&f=$folder_id&o=$o&path=<?=$path?>';</script>");

// データベース接続を閉じる
pg_close($con);