<?php
require_once 'Cmx.php';
ob_start();
require_once 'about_certify.php';
require_once 'about_comedix.php';
require_once 'get_values.ini';
require_once 'show_copyright.ini';
require_once 'show_select_values.ini';
require_once 'label_by_profile_type.ini';
require_once 'passwd_change_common.ini';
require_once 'timecard_common_class.php';
require_once 'conf/conf.inf';
require_once 'aclg_set.php';
require_once 'atdbk_menu_common.ini';
require_once 'Cmx/Model/SystemConfig.php';
ob_end_clean();

$fname = $PHP_SELF;

$con = connect2db($fname);

// SystemConfig
$conf = new Cmx_SystemConfig();

if (defined("PERMIT_GET_FOR_SYSTEMID")) {
    $tmp_systemid = $systemid;
}
else {
    $tmp_systemid = $_POST["systemid"];
}

if ($id != "" && $tmp_systemid != "") {
    $sql = "select prf_org_cd from profile";
    $cond = "";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $org_cd = (pg_num_rows($sel) == 1) ? pg_fetch_result($sel, 0, "prf_org_cd") : "";
    if (strcmp($tmp_systemid, md5($org_cd)) == 0) {
        $sql = "select l.emp_login_pass from login l";
        $cond = "where l.emp_login_id = '$id' and exists (select * from authmst a where a.emp_id = l.emp_id and (not a.emp_del_flg))";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $pass = (pg_num_rows($sel) == 1) ? pg_fetch_result($sel, 0, "emp_login_pass") : "";
    }
}

$is_login = ($id != "" && $pass != "" && @$session == "");

// ログインIDゼロ埋め
if (defined("LOGINID_STR_PAD_LENGTH")) {
    $id = str_pad($id, LOGINID_STR_PAD_LENGTH, "0", STR_PAD_LEFT);
}

$session = certify($id, $pass, @$session, $fname);
// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"], $_POST, $con, $_GET);

$cookie_time = 0;
setcookie('CMX_AUTH_SESSION', $session, $cookie_time);

$authorities = get_authority(@$session, $fname);

require("menu_list.ini");

if (!isset($_REQUEST['date'])) {
    $date1 = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
}
else {
    $date1 = $_REQUEST['date'];
}
$day = date('d', $date1);
$month = date('m', $date1);
$year = date('Y', $date1);
$todayday = date('d');
$todaymonth = date('m');
$todayyear = date('Y');

$cond = " where session_id='$session'";
$sel = select_from_table($con, "select emp_id from session", $cond, $fname);
if ($sel == 0) {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// ライセンス有効期限情報を取得
$sql = "select lcs_expire_flg, lcs_expire_date from license";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$lcs_expire_flg = pg_fetch_result($sel, 0, "lcs_expire_flg");
$lcs_expire_date = pg_fetch_result($sel, 0, "lcs_expire_date");

// 有効期限を過ぎている場合
if ($lcs_expire_flg == "t" && date("Ymd") > $lcs_expire_date) {

    // ログインユーザの職員登録権限・ライセンス管理権限を取得
    $sql = "select emp_reg_flg, emp_lcs_flg from authmst";
    $cond = "where emp_id = '$emp_id'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $reg_flg = pg_fetch_result($sel, 0, "emp_reg_flg");
    $lcs_flg = pg_fetch_result($sel, 0, "emp_lcs_flg");

    // 権限がなければログイン不可とする
    if ($reg_flg == "f" && $lcs_flg == "f") {
        echo("<script type=\"text/javascript\">alert('CoMedix利用期限日を過ぎました。継続して利用される場合は、ライセンスの購入手続をお願いします。');</script>");
        echo("<script type=\"text/javascript\">location.href = 'login.php';</script>");
        exit;
    }
}

// ディスク容量 (/var)
$disk_total = disk_total_space("/var");
$disk_free = disk_free_space("/var");
$disk_used = $disk_total - $disk_free;
$used_bar = (int)($disk_used / $disk_total * 148);
$free_per = (int)($disk_free / $disk_total * 100);

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

$hour = date("H");
$minute = date("i");
$time = $hour . "：" . $minute;

// 環境設定情報を取得
$sql = "select timecard_use, timecard_menu_flg, calendar_menu_flg, quick_menu_flg, basic_menu_flg, community_menu_flg, business_menu_flg, soft_menu_flg, manual_menu_flg, license_menu_flg, use_flash_logo from config";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$timecard_use = pg_fetch_result($sel, 0, "timecard_use");
$timecard_menu_flg = pg_fetch_result($sel, 0, "timecard_menu_flg");
$calendar_menu_flg = pg_fetch_result($sel, 0, "calendar_menu_flg");
$quick_menu_flg = pg_fetch_result($sel, 0, "quick_menu_flg");
$basic_menu_flg = pg_fetch_result($sel, 0, "basic_menu_flg");
//XX $community_menu_flg = pg_fetch_result($sel, 0, "community_menu_flg");
$business_menu_flg = pg_fetch_result($sel, 0, "business_menu_flg");
$soft_menu_flg = pg_fetch_result($sel, 0, "soft_menu_flg");
$manual_menu_flg = pg_fetch_result($sel, 0, "manual_menu_flg");
$license_menu_flg = pg_fetch_result($sel, 0, "license_menu_flg");
$use_flash_logo = pg_fetch_result($sel, 0, "use_flash_logo");

// オプション設定情報を取得
$sql = "select default_page, logo_type, schedule1_default, facility_default, calendar_start1, sidemenu_show_flg, sidemenu_position, schd_show_old_pjt, default_fclcate from option";
$cond = "where emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$default_page = pg_fetch_result($sel, 0, "default_page");
$logo_type = pg_fetch_result($sel, 0, "logo_type");
$schedule1_default = pg_fetch_result($sel, 0, "schedule1_default");
$facility_default = pg_fetch_result($sel, 0, "facility_default");
$calendar_start = pg_fetch_result($sel, 0, "calendar_start1");
$sidemenu_show_flg = pg_fetch_result($sel, 0, "sidemenu_show_flg");
$sidemenu_position = pg_fetch_result($sel, 0, "sidemenu_position");
$sidemenu_style = ($sidemenu_show_flg == "f") ? " style=\"display:none;\"" : "";
$sidemenu_position = ($sidemenu_position == "2") ? "left" : "right";
$schd_show_old_pjt = pg_fetch_result($sel, 0, "schd_show_old_pjt");
$default_fclcate_id = pg_fetch_result($sel, 0, "default_fclcate");

// 終了済みの委員会・WGを表示しない設定の場合
if ($schd_show_old_pjt == "f") {

    // 終了済み委員会・WGの表示設定を外す
    $sql = "delete from schdproject";
    $cond = "where emp_id = '$emp_id' and ownother = '1' and pjt_id in (select pjt_id from project where pjt_real_end_date != '' and pjt_real_end_date is not null)";
    $del = delete_from_table($con, $sql, $cond, $fname);
    if ($del == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}

// 環境設定からマスタ変更情報を取得
$sql = "select user_belong_change, user_st_change, user_job_change, ic_read_flg from config";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$user_belong_change = pg_fetch_result($sel, 0, "user_belong_change");
$user_st_change = pg_fetch_result($sel, 0, "user_st_change");
$user_job_change = pg_fetch_result($sel, 0, "user_job_change");
if ($user_belong_change == "t" || $user_job_change == "t" || $user_st_change == "t") {
    $master_chg_flg = "1";
}
else {
    $master_chg_flg = "0";
}


// メイン画面のURLを設定
switch ($schedule1_default) {
    case "1":
        $schedule_url = "schedule_menu.php";
        break;
    case "2":
        $schedule_url = "schedule_week_menu.php";
        break;
    case "3":
        $schedule_url = "schedule_month_menu.php";
        break;
    default:
        $schedule_url = "schedule_week_menu.php";
        break;
}
switch ($facility_default) {
    case "1":
        $facility_url = "facility_menu.php";
        break;
    case "2":
        $facility_url = "facility_week.php";
        break;
    case "3":
        $facility_url = "facility_month.php";
        break;
    case "4":
        $facility_url = "facility_week_chart.php";
        break;
    default:
        $facility_url = "facility_week.php";
        break;
}
if ($is_login) {
    switch ($default_page) {
        case "01":  // マイページ
            $main_url = "left.php?session=$session";
            break;
        case "02":  // ウェブメール
            $main_url = ($authorities[0] == "t") ? "webmail_menu.php?session=$session" : "left.php?session=$session";
            break;
        case "03":  // スケジュール
            $main_url = ($authorities[2] == "t") ? "$schedule_url?session=$session" : "left.php?session=$session";
            break;
        case "04":  // 委員会・WG
            $main_url = ($authorities[31] == "t") ? "project_menu.php?session=$session" : "left.php?session=$session";
            break;
        case "05":  // タスク
            $main_url = ($authorities[30] == "t") ? "work_menu.php?session=$session" : "left.php?session=$session";
            break;
        case "06":  // 伝言メモ
            $main_url = ($authorities[4] == "t") ? "message_menu.php?session=$session" : "left.php?session=$session";
            break;
        case "07":  // 設備予約
            if ($authorities[11] == "t") {
                $main_url = "$facility_url?session=$session";
                if ($facility_default == "4") {
                    $main_url .= "&cate_id=$default_fclcate_id";
                }
            }
            else {
                $main_url = "left.php?session=$session";
            }
            break;
        case "08":  // 出勤表
            $main_url = ($authorities[5] == "t") ? atdbk_menu_default($con, $fname, $session) : "left.php?session=$session";
            break;
        case "09":  // 決裁・申請
            $main_url = ($authorities[7] == "t") ? "application_menu.php?session=$session" : "left.php?session=$session";
            break;
        case "10":  // ネットカンファレンス
            $main_url = ($authorities[10] == "t") ? "net_conference_menu.php?session=$session" : "left.php?session=$session";
            break;
        case "11":  // 掲示板
            $main_url = ($authorities[11] == "t") ? "bbsthread_menu.php?session=$session" : "left.php?session=$session";
            break;
        case "12":  // Q&A
            $main_url = ($authorities[6] == "t") ? "qa_category_menu.php?session=$session&qa=1" : "left.php?session=$session";
            break;
        case "13":  // 文書管理
            $main_url = ($authorities[32] == "t") ? "library_list_all.php?session=$session&library=1" : "left.php?session=$session";
            break;
        case "14":  // アドレス帳
            $main_url = ($authorities[9] == "t") ? "address_menu.php?session=$session" : "left.php?session=$session";
            break;
        case "15":  // パスワード変更
            if ($authorities[8] == "t") {
                $main_url = "passwd_change.php?session=$session";
            }
            else {
                if ($master_chg_flg == "1") {
                    //本人情報画面へ遷移
                    $main_url = "passwd_belong_change.php?session=$session";
                }
                else {
                    $main_url = "left.php?session=$session";
                }
            }
            break;
        case "16":  // オプション設定
            $main_url = "option_register.php?session=$session";
            break;
        case "18":  // 患者管理
            $main_url = ($authorities[15] == "t" || $authorities[22] == "t" || $authorities[33] == "t" || $authorities[35] == "t" || $authorities[34] == "t" || $authorities[36] == "t") ? "patient_info_menu.php?session=$session" : "left.php?session=$session";
            break;
        case "19":  // 病床管理
            $main_url = ($authorities[14] == "t") ? "bed_menu.php?session=$session" : "left.php?session=$session";
            break;
        case "20":  // 経営支援
            $main_url = ($authorities[25] == "t") ? "biz_menu.php?session=$session" : "left.php?session=$session";
            break;
        case "21":  // イントラネット
            $main_url = ($authorities[55] == "t") ? "intra_menu.php?session=$session" : "left.php?session=$session";
            break;
        case "22":  // お知らせ
            $main_url = ($authorities[46] == "t") ? "newsuser_menu.php?session=$session" : "left.php?session=$session";
            break;
        case "23":  // 内線電話帳
            $main_url = ($authorities[49] == "t") ? "extension_menu.php?session=$session&extension=1" : "left.php?session=$session";
            break;
        case "24":  // ファントルくん
            $main_url = ($authorities[47] == "t") ? "hiyari_menu.php?session=$session" : "left.php?session=$session";
            break;
        case "25":  // メドレポート
            $med_program_list = array(
                array("mode" => 1, "location" => "summary_menu.php?session=$session", "lname" => "お知らせ"),
                array("mode" => 2, "location" => "summary_menu.php?session=$session&search_mode=1", "lname" => "患者検索"),
                array("mode" => 3, "location" => "summary_search_bed.php?session=$session", "lname" => "病床検索"),
                array("mode" => 4, "location" => "summary_search_data.php?session=$session", "lname" => "複合検索"),
                array("mode" => 5, "location" => "sum_application_apply_list.php?session=$session", "lname" => "送信一覧"),
                array("mode" => 6, "location" => "sum_application_approve_list.php?session=$session", "lname" => "受信一覧"),
                array("mode" => 7, "location" => "sot_worksheet.php?session=$session&apply_date_default=on", "lname" => "看護支援"),
            );
            $menu_flg = 2; // 患者検索

            $sql = "select menu_flg from sum_application_option";
            $cond = "where emp_id = '" . pg_escape_string($emp_id) . "'";
            $sel = select_from_table($con, $sql, $cond, $fname);
            if ($sel == 0) {
                pg_close($con);
                summary_common_show_error_page_and_die();
            }
            if (pg_num_rows($sel) == 1) {
                $menu_flg = (int)pg_fetch_result($sel, 0, "menu_flg");
            }
            foreach ($med_program_list as $plist) {
                if ($menu_flg == $plist["mode"]) {
                    $pname = $plist["location"];
                    break;
                }
            }
            $main_url = ($authorities[57] == "t") ? $pname : "left.php?session=$session";
            break;
        case "26":  // リンクライブラリ
            $main_url = ($authorities[60] == "t") ? "link_menu.php?session=$session" : "left.php?session=$session";
            break;
        case "27":  // 検索ちゃん
            $main_url = ($authorities[65] == "t") ? "kensaku_byoumei.php?session=$session" : "left.php?session=$session";
            break;
        case "28":  // CAS
            $main_url = ($authorities[67] == "t") ? "cas_application_menu.php?session=$session" : "left.php?session=$session";
            break;
        case "29":  // 勤務シフト作成
            $main_url = ($authorities[69] == "t") ? "duty_shift_results.php?session=$session" : "left.php?session=$session";
            break;
        case "30":  // 看護観察記録
            $main_url = ($authorities[71] == "t") ? "kango_top.php?session=$session" : "left.php?session=$session";
            break;
        case "31":  // e-ラーニング（バリテス）
            $main_url = ($authorities[75] == "t") ? "study/study_menu_user.php?session=$session" : "left.php?session=$session";
            break;
        case "32":  // カルテビューワー
            $main_url = ($authorities[73] == "t") ? "kview_menu.php?session=$session" : "left.php?session=$session";
            break;
        case "33":  // ファントルくん＋
            $main_url = ($authorities[78] == "t") ? "fplus_menu.php?session=$session" : "left.php?session=$session";
            break;
        case "34":  // 日報・月報
            $main_url = ($authorities[80] == "t") ? "jnl_application_menu.php?session=$session" : "left.php?session=$session";
            break;
        case "35":  // 看護支援
            $main_url = ($authorities[83] == "t") ? "kangoshien/ks_index.php?session=$session" : "left.php?session=$session";
            break;
        case "36":  // 人事管理
            $main_url = ($authorities[85] == "t") ? "jinji_menu.php?session=$session" : "left.php?session=$session";
            break;
        case "37":  // クリニカルラダー
            $main_url = ($authorities[87] == "t") ? "ladder_menu.php?session=$session" : "left.php?session=$session";
            break;
        case "38":  // 勤務表
            $main_url = ($authorities[89] == "t") ? "shift/index.php" : "left.php?session=$session";
            break;
        case "39":  // キャリア開発ラダー
            $main_url = ($authorities[93] == "t") ? "ladder/index.php" : "left.php?session=$session";
            break;
        case "41":  // スタッフポートフォリオ
            $main_url = ($authorities[95] == "t") ? "spf/index.php?session=$session" : "left.php?session=$session";
            break;
        default:
            $main_url = "left.php?session=$session";
            break;
    }
    // パスワード変更の権限取得
    $pass_auth = check_authority($session, 8, $fname);
    if ($pass_auth == "1") {
        // 初回ログイン時パスワード変更が有効の場合、
        // 初回ログイン時に、強制パスワード変更画面に飛ぶ
        $pass_change_date = get_pass_change_date($con, $fname, $emp_id);
        $password_first_time = $conf->get('password.first_time');
        if (!$pass_change_date && $password_first_time === '1' && empty($tmp_systemid)) {
            $f_url = CMX_BASE_URL . "/pass/passwd_change_force.php?session=$session&id=$id";
            header("Location: {$f_url}");
            exit;
        }
        // パスワード変更日が未設定、有効期限切れの場合、パスワード変更へ
        $pass_expire_flg = check_pass_expire($con, $fname, $emp_id);
        if ($pass_expire_flg == 1 || $pass_expire_flg == 2) {
            $tmp_url = urlencode($main_url);
            $f_url = "passwd_change.php?session=$session&next_url=$tmp_url&pass_expire_flg=$pass_expire_flg";
        }
    }
}
else {
    if (isset($wherefrom)) {
        if ($wherefrom == "1") {
            $main_url = "schedule_menu.php?session=$session";
        }
        else {
            $main_url = "left.php?session=$session";
        }
    }
    // ランチャーポップアップからの呼び出し
    else if ($_GET["module"]) {
        switch ($_GET["module"]) {
            // お知らせ・回覧板
            case 'newsuser':
                $main_url = ($authorities[46] == "t") ? "newsuser_menu.php?session=$session" : "left.php?session=$session";
                break;
            // 決裁・申請
            case 'application':
                $main_url = ($authorities[7] == "t") ? "application_menu.php?session=$session" : "left.php?session=$session";
                break;
            // ウェブメール
            case 'webmail':
                $main_url = ($authorities[0] == "t") ? "webmail_menu.php?session=$session" : "left.php?session=$session";
                break;
            // 伝言メモ
            case 'message':
                $main_url = ($authorities[4] == "t") ? "message_menu.php?session=$session" : "left.php?session=$session";
                break;
            // ファントルくん
            case 'hiyari':
                $main_url = ($authorities[47] == "t") ? "hiyari_menu.php?session=$session" : "left.php?session=$session";
                break;
            // ファントルくん＋
            case 'fplus':
                $main_url = ($authorities[78] == "t") ? "fplus_menu.php?session=$session" : "left.php?session=$session";
                break;
            // スケジュール
            case 'schedule':
                $main_url = ($authorities[2] == "t") ? "schedule_confirm.php?session=$session" : "left.php?session=$session";
                break;
            // 勤務表
            case 'shift':
                $main_url = ($authorities[89] == "t") ? "shift/index.php" : "left.php?session=$session";
                break;
            // 勤務シフト作成
            case 'dutyshift':
                $main_url = ($authorities[69] == "t") ? "duty_shift_results.php?session=$session" : "left.php?session=$session";
                break;
            // バリテス：お知らせ
            case 'baritess_oshirase':
                $main_url = ($authorities[75] == "t") ? "study/study_notif_user.php?session=$session" : "left.php?session=$session";
                break;
            // バリテス：未受講講座件数
            case 'baritess_mijukou':
                $main_url = ($authorities[75] == "t") ? "study/study_menu_user.php?session=$session" : "left.php?session=$session";
                break;
            // スタッフポートフォリオ
            case 'spf':
                $main_url = ($authorities[95] == "t") ? "spf/notice.php" : "left.php?session=$session";
                break;
            // COMPANY
            case 'company':
                require_once("Cmx/Model/SystemConfig.php");
                $conf = new Cmx_systemConfig();
                $sel = select_from_table($con, "select emp_login_id from login", "where emp_id = '" . $emp_id . "'", $fname);
                if ($sel == 0) {
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }
                $emp_login_id = pg_fetch_result($sel, 0, "emp_login_id");
                if ($conf->get('shift.notice.company_use') === "1") {
                    $company_url = $conf->get('shift.notice.company_transition_url');
                }
                echo("<script type=\"text/javascript\">");
                echo("var company_url='".$company_url."';");
                echo("var company_user='".$emp_login_id."';");
                echo("</script>");
                break;
            default:
                $main_url = "left.php?session=$session";
                break;
        }
    }
    else {
        $main_url = "left.php?session=$session";
    }
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
        <title>CoMedix</title>
        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/jquery.cookie.js"></script>
        <script type="text/javascript" src="js/header.js"></script>
        <script type="text/javascript" src="js/fontsize.js"></script>
        <script type="text/javascript" src="js/headermenu.js"></script>
        <script type="text/javascript">
            var callSideMenu = false;

            function checkValue(wherefrom) {
                if (wherefrom == "1") {
                    document.clockin.action = "clock_in_insert_from_top.php?session=<?=h($session);?>&wherefrom=1";
                    document.clockin.submit();
                }
                if (wherefrom == "2") {
                    document.clockin.action = "clock_in_insert_from_top.php?session=<?=h($session);?>&wherefrom=2";
                    document.clockin.submit();
                }
                if (wherefrom == "3") {
                    document.clockin.action = "clock_in_insert_from_top.php?session=<?=h($session);?>&wherefrom=3";
                    document.clockin.submit();
                }
                if (wherefrom == "4") {
                    document.clockin.action = "clock_in_insert_from_top.php?session=<?=h($session);?>&wherefrom=4";
                    document.clockin.submit();
                }
            }

            function resizeIframe() {
                try {
                    if (floatingpage.resizeIframeEx) {
                        floatingpage.resizeIframeEx();
                    } else {
                        document.getElementById('floatingpage').style.height = '620px';
                        var height;
                        try {
                            if (document.all || window.opera) {
                                height = floatingpage.document.body.scrollHeight;
                                if (height > floatingpage.document.body.clientHeight) {
                                    height = floatingpage.document.body.scrollHeight;
                                }
                            } else if (floatingpage.document.body.offsetHeight) {
                                height = floatingpage.document.body.offsetHeight;
                            }
                        } catch (e) {
                        }

                        if (height && height > 600) {
                            document.getElementById('floatingpage').style.height = (height + 20) + 'px';
                        }

                        var hash = window.frames['floatingpage'].location.hash;
                        if (hash != '') {
                            window.frames['floatingpage'].document.getElementById(hash.replace('#', '')).scrollIntoView(true);
                        }
                    }
                } catch (e) {
                }
            }

            function setFloatingPage() {
<?if (@$f_url != "") {?>
    <?
    if (strpos($f_url, "session=") === false) {
        if (strpos($f_url, "&") === false) {
            $f_url .= "?";
        }
        else {
            $f_url .= "&";
        }
        $f_url .= "session=$session";
    }
    ?>
                    floatingpage.location.href = '<?echo($f_url);?>';
<? }
else if (@$goto == "1") {?>
                    floatingpage.location.href = 'schedule_menu.php?session=<?echo($session);?>&date=' + <?echo(mktime(0, 0, 0, $todaymonth, $todayday, $todayyear));?>;
<? }
else {?>
                    floatingpage.location.href = '<?echo($main_url);?>';
<? }?>
            }

            function reloadPage() {
                location.href = 'main_menu.php?session=<?echo($session);?>';
            }

            function registerOthersTimecard(dt) {
                window.open('atdbk_timecard_others_select.php?session=<?echo($session);?>&date=' + dt + '', 'newwin', 'width=640,height=480,scrollbars=yes');
            }

            function updateTimecard(dt) {
                window.open('atdbk_timecard_edit.php?session=<?echo($session);?>&emp_id=<?echo($emp_id);?>&date=' + dt + '&wherefrom=3', 'newwin', 'width=640,height=480,scrollbars=yes');
            }

            function changeSideMenuDisplay() {
                var centerblock = document.getElementById('centerblock');
                var block_display = (centerblock.style.display == 'none') ? '' : 'none';
                centerblock.style.width = '5px';
                centerblock.style.backgroundColor = 'white';
                centerblock.style.display = block_display;

                var menublock = document.getElementById('menublock');
                menublock.style.width = '160px';
                menublock.style.display = block_display;

                callSideMenu = true;
                resizeIframe();
            }

            function resizeCalendar(height) {
                document.getElementById('calframe').style.height = height + 'px';
            }

            function createSchedule() {
                if (document.quick.schd_title.value == '') {
                    alert('タイトルが入力されていません。');
                    return;
                }
                document.quick.submit();
            }

            $(window).load(function(){
                if(typeof(company_url) !== 'undefined') {
                    var form = $('<form></form>',{action:company_url, target:'_blank', method:'POST'}).hide();
                    var body = $('body');
                    body.append(form);
                    form.append($('<input>',{type:'hidden',name:'user',value:company_user}));
                    form.append($('<input>',{type:'hidden',name:'@JUMP',value:'root.cws.intray'}));
                    form.append($('<input>',{type:'hidden',name:'@SUB',value:1}));
                    form.append($('<input>',{type:'hidden',name:'@SN',value:'root.cws'}));
                    form.submit();
                    try {
                        window.open('about:brank','_self').close();
                    }
                    catch(e){
                        alert(e);
                    }
                }
            });

            $(function() {
                $('#disk_title').toggle(
                    function() {
                        $('#disk_content').hide();
                        $('#disk_title_mark').text('▼');
                        $.cookie('sidemenu_disk', '0');
                    },
                    function() {
                        $('#disk_content').show();
                        $('#disk_title_mark').text('▲');
                        $.cookie('sidemenu_disk', '1');
                    }
                );
                if ($.cookie('sidemenu_disk') === '0') {
                    $('#disk_title').click();
                }
            });
        </script>
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <style>
            <!--
            #disk {
                width: 158px;
                margin:0;
                border:1px solid #818890;
                font-size: 14px;
                line-height: 140%;
                font-family: ＭＳ Ｐゴシック, Osaka, sans-serif;
                background-color:#818890;
                /* IE6 hack */
                _width: 160px;
                _font-size: 80%;
            }
            #disk_title {
                position: relative;
                font-weight:bold;
                color:#fff;
                background-color:#818890;
                padding: 1px 0 1px 5px;
                cursor:pointer;
            }
            #disk_title_mark {
                position: absolute;
                top:0;
                right:3px;
            }
            #disk_content {
                background-color:#fff;
                padding: 2px 2px 2px 5px;
            }
            #disk_content dl {
                margin:0;

            }
            #disk_content dl dt {
                width: 70px;
                float:left;
            }
            #disk_content dl dd {
                margin-left:75px;
                width: 70px;
                text-align: right;

            }
            #disk_bar {
                margin: 3px 0 0 0;
                width: 148px;
                height: 20px;
                padding:0;
                border: 1px solid black;
                background-color: blue;
            }
            #disk_bar_used {
                margin:0;
                padding:0;
                background-color:red;
                height:20px;
            }
            #disk_alert {
                font-size: 12px;
                padding-top:20px;
            }
            #disk_alert span {
                padding:5px 10px;
            }
            -->
        </style>
    </head>
    <body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="setFloatingPage();">
        <a name="pagetop"></a>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td>
                    <!--header-->
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="120">
                                <a href="left.php?session=<?php echo $session;?>" target="floatingpage"><img src="img/logo.gif" alt="マイページ" width="120" height="60" border="0" /></a>
                            </td>
                            <?
                            // ヘッダアイコン
                            $query = ($facility_default == "4") ? "&cate_id=$default_fclcate_id" : "";
                            show_header_menu_area($con, $emp_id, $authorities, $schedule_url, $facility_url, $session, $fname, $query, $profile_type);

// ディスク残量警告
                            if ($free_per <= 30) {
                                $disk_style = 'color:#000; background-color:#f0e68c;';
                            }
                            if ($free_per <= 20) {
                                $disk_style = 'color:#000; background-color:#ff0;';
                            }
                            if ($free_per <= 10) {
                                $disk_style = 'color:#fff; background-color:#f00;font-weight:bold;';
                            }
                            if ($authorities[20] === "t" and $free_per <= 30) {
                                printf('<td id="disk_alert"><span style="%s">ディスク残量: <b>%s%%</b></span></td>', $disk_style, $free_per);
                            }

// 右上ロゴ
                            show_logo($fname);
                            ?>
                        </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td colspan="2" height="7" background="img/line_1.gif"><img src="img/spacer.gif" width="1" height="7" alt=""></td>
                            <td width="25"><img src="img/line_2.gif" width="25" height="7"></td>
                            <td colspan="3" background="img/line_3.gif"><img src="img/spacer.gif" width="1" height="7" alt=""></td>
                        </tr>
                        <tr height="22" bgcolor="#f6f9ff">
<?if ($sidemenu_position == "left") {?>
                                <td width="158" style="background-image:url('img/line_4.gif');padding-left:3px;cursor:pointer;" onclick="changeSideMenuDisplay();" onmouseover="this.style.backgroundColor = '#e6e6fa';
                                        this.style.backgroundImage = '';" onmouseout="this.style.backgroundImage = 'url(img/line_4.gif)';
                                                this.style.backgroundColor = '';"><img src="img/openclose.gif" width="20" height="13" border="0" alt="open/close"></td>
                                <td width="45%" background="img/line_4.gif"><img src="img/spacer.gif" width="1" height="22" alt=""></td>
                            <? }
                            else {?>
                                <td width="158" background="img/line_4.gif"><img src="img/spacer.gif" width="1" height="22" alt=""></td>
                                <td width="45%" background="img/line_4.gif"><img src="img/spacer.gif" width="1" height="22" alt=""></td>
                            <? }?>
                            <td width="25"><img src="img/line_5.gif" width="25" height="22"></td>
                            <td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?echo(get_own_job_name($con, $emp_id, $fname));?>&nbsp;<?echo(get_emp_kanji_name($con, $emp_id, $fname));?>&nbsp;<span onclick="floatingpage.location.href = 'option_register.php?session=<?echo($session);?>';">さん</span></font></td>
<?if ($sidemenu_position == "right") {?>
                                <td width="12"></td>
                                <td width="158" align="right" style="padding-right:2px;cursor:pointer;" onclick="changeSideMenuDisplay();" onmouseover="this.style.backgroundColor = '#e6e6fa';" onmouseout="this.style.backgroundColor = '';"><img src="img/openclose.gif" width="20" height="13" border="0" alt="open/close"></td>
<? }
else {?>
                                <td width="170"></td>
                            <? }?>
                        </tr>
                    </table>
                    <img src="./img/spacer.gif" width="1" height="2"><br>
                    <!--header-->
                    <!--body-->
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#f6f9ff">
                        <tr>
<?if ($sidemenu_position == "right") {?>
                                <td valign="top" bgcolor="#ffffff">
                                    <img src="img/spacer.gif" width="1" height="5" alt=""><br>
                                    <iframe id="floatingpage" name="floatingpage" topmargin="0" leftmargin="0" src="blank.php" frameborder="0" width="100%" height="700" onload="resizeIframe();"></iframe>
                                </td>
                                <td bgcolor="#ffffff" width="5" valign="top" id="centerblock"<?echo($sidemenu_style);?>><img src="./img/spacer.gif" width="5" height="1"></td>
<? }?>
                            <!--sidemenu-->
                            <td width="160" valign="top" id="menublock"<?echo($sidemenu_style);?>>
                                <!--
                                <table width="160" border="0" cellspacing="0" cellpadding="0"<?if ($sidemenu_position == "right") {
    echo(" style=\"position:relative;top:-25px;z-index:2;\"");
}?>>
                                -->
                                <table width="160" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td colspan="3"><img src="img/spacer.gif" width="1" height="3" alt=""></td>
                                    </tr>
                                    <!--attendancebook-->
<?if ($timecard_menu_flg == "t") {?>
                                        <tr>
                                            <td valign="top">
                                                <table width="160" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td colspan="3" bgcolor="#a5526e"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="1" bgcolor="#a5526e"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
                                                        <td width="158">
                                                            <table width="158" border="0" cellspacing="0" cellpadding="0" onClick="expandcontent('sc2')" style="cursor:hand; cursor:pointer">
                                                                <tr bgcolor="#a5526e">
                                                                    <td height="22" class="spacing" background="img/bg_timecard.gif"><b><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff">タイムカード</font></b></td>
                                                                    <td width="18" height="22" class="spacing" background="img/bg_timecard.gif"><b><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><span id="sc2_d">▼</span><span id="sc2_u" style="display:none;">▲</span></font></b></td>
                                                                </tr>
                                                            </table>
                                                            <div id="sc2" class="switchcontent">
                                                                <form name="clockin" method="post">
                                                                    <table width="158" border="0" cellspacing="0" cellpadding="1">
                                                                        <tr>
                                                                            <td height="22" colspan="4"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">只今の時間は<b><?echo($time);?></b></font></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <?
// 勤務実績レコードを取得
                                                                            $timecard_common_class = new timecard_common_class($con, $fname, $emp_id, date("Ymd", strtotime("-1 day")), date("Ymd", strtotime("1 day")));
//翌日の勤務パターンの勤務開始時刻が前日であるフラグの設定を確認
//※フラグが前日の場合、実績のレコードは翌日へ登録されている。
                                                                            $tmp_previous_day_flag = $timecard_common_class->get_previous_day_flag($emp_id);
                                                                            if ($tmp_previous_day_flag == "1") {
                                                                                $arr_result = $timecard_common_class->get_atdbkrslt_array($con, date("Ymd", strtotime("1 day")), $emp_id, $fname);
                                                                            }
                                                                            else {
                                                                                $arr_result = $timecard_common_class->get_clock_in_atdbkrslt_array($con, $emp_id, $fname);
                                                                            }
                                                                            $start_time = $arr_result["start_time"];
                                                                            $out_time = $arr_result["out_time"];
                                                                            $ret_time = $arr_result["ret_time"];
                                                                            $end_time = $arr_result["end_time"];
                                                                            echo("<td width=\"25%\" height=\"22\" align=\"center\">\n");
                                                                            if ($start_time != "") {
                                                                                $atdbk_hour = intval(substr($start_time, 0, 2));
                                                                                $atdbk_min = substr($start_time, 2, 2);
                                                                                $atdbk_time = "$atdbk_hour:$atdbk_min";
                                                                                echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">出勤<br>$atdbk_time</font>");
                                                                            }
                                                                            else {
                                                                                echo("<input type=\"button\" value=\"出勤\" onclick=\"checkValue(1);\" style=\"font-size:11px;width:36px\">");
                                                                            }
                                                                            echo("</td>\n");
                                                                            echo("<td width=\"25%\" align=\"center\">\n");
                                                                            if ($out_time != "") {
                                                                                $atdbk_hour = intval(substr($out_time, 0, 2));
                                                                                $atdbk_min = substr($out_time, 2, 2);
                                                                                $atdbk_time = "$atdbk_hour:$atdbk_min";
                                                                                echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">外出<br>$atdbk_time</font>");
                                                                            }
                                                                            else if ($start_time != "" && $ret_time == "" && $end_time == "") {
                                                                                echo("<input type=\"button\" value=\"外出\" onclick=\"checkValue(2);\" style=\"font-size:11px;width:36px\">");
                                                                            }
                                                                            else {
                                                                                echo("<input type=\"button\" value=\"外出\" disabled style=\"font-size:11px;width:36px\">");
                                                                            }
                                                                            echo("</td>\n");
                                                                            echo("<td width=\"25%\" align=\"center\">\n");
                                                                            if ($ret_time != "") {
                                                                                $atdbk_hour = intval(substr($ret_time, 0, 2));
                                                                                $atdbk_min = substr($ret_time, 2, 2);
                                                                                $atdbk_time = "$atdbk_hour:$atdbk_min";
                                                                                echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">復帰<br>$atdbk_time</font>");
                                                                            }
                                                                            else if ($out_time != "" && $ret_time == "") {
                                                                                echo("<input type=\"button\" value=\"復帰\" onclick=\"checkValue(3);\" style=\"font-size:11px;width:36px\">");
                                                                            }
                                                                            else {
                                                                                echo("<input type=\"button\" value=\"復帰\" disabled style=\"font-size:11px;width:36px\">");
                                                                            }
                                                                            echo("</td>\n");
                                                                            echo("<td width=\"25%\" align=\"center\">\n");
                                                                            if ($end_time != "") {
                                                                                $atdbk_hour = intval(substr($end_time, 0, 2));
                                                                                $atdbk_min = substr($end_time, 2, 2);
                                                                                $atdbk_time = "$atdbk_hour:$atdbk_min";
                                                                                echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">退勤<br>$atdbk_time</font>");
                                                                            }
                                                                            else if ($start_time != "" && ($out_time == "" || $ret_time != "") and $end_time == "") {
                                                                                echo("<input type=\"button\" value=\"退勤\" onclick=\"checkValue(4);\" style=\"font-size:11px;width:36px\">");
                                                                            }
                                                                            else {
                                                                                echo("<input type=\"button\" value=\"退勤\" disabled style=\"font-size:11px;width:36px\">");
                                                                            }
                                                                            echo("</td>\n");

// 代行入力対象が存在するかチェック
                                                                            $agent_date = date("Ymd");
                                                                            $sql = "select count(*) from tmcdwkfw";
                                                                            $cond = "where modify_id = '$emp_id' and exists (select * from authmst where emp_id = tmcdwkfw.emp_id and emp_del_flg = 'f')";
                                                                            $sel = select_from_table($con, $sql, $cond, $fname);
                                                                            if ($sel == 0) {
                                                                                pg_close($con);
                                                                                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                                                                                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                                                                                exit;
                                                                            }
                                                                            $agent_flg = (pg_fetch_result($sel, 0, 0) > 0);

// タイムカード修正可否フラグを取得
                                                                            $sql = "select modify_flg from timecard";
                                                                            $cond = "";
                                                                            $sel = select_from_table($con, $sql, $cond, $fname);
                                                                            if ($sel == 0) {
                                                                                pg_close($con);
                                                                                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                                                                                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                                                                                exit;
                                                                            }
                                                                            $modify_flg = (pg_num_rows($sel)) ? pg_fetch_result($sel, 0, "modify_flg") : "t";
                                                                            ?>
                                                                        </tr>
                                                                        <tr height="22">
                                                                            <td colspan="4" align="right">
                                                                                <input type="button" value="代行入力" style="font-size:11px;" onclick="registerOthersTimecard('<?echo($agent_date);?>');"<?if (!$agent_flg) {
                                                                                echo(" disabled");
                                                                            }?>>
                                                                                <input type="button" value="修正" style="font-size:11px;width:36px;" onclick="updateTimecard('<?echo($arr_result["date"]);?>');"<?if ($modify_flg == "f") {
                                                                                echo(" disabled");
                                                                            }?>>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </form>
                                                            </div>
                                                        </td>
                                                        <td width="1" bgcolor="#a5526e"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" bgcolor="#a5526e"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><img src="img/spacer.gif" width="1" height="5" border="0"></td>
                                        </tr>
<? }?>
                                    <!--attendancebook-->
                                    <!--calendar-->
<?if ($calendar_menu_flg == "t") {?>
                                        <tr>
                                            <td valign="top">
                                                <table width="160" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td colspan="3" bgcolor="#a552aa"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="1" bgcolor="#a552aa"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
                                                        <td width="158">
                                                            <table width="158" border="0" cellspacing="0" cellpadding="0" onClick="expandcontent('sc3')" style="cursor:hand; cursor:pointer">
                                                                <tr bgcolor="#a552aa">
                                                                    <td height="22" class="spacing" background="img/bg_calendar.gif"><b><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff">カレンダー</font></b></td>
                                                                    <td width="18" height="22" class="spacing" background="img/bg_calendar.gif"><b><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><span id="sc3_d">▼</span><span id="sc3_u" style="display:none;">▲</span></font></b></td>
                                                                </tr>
                                                            </table>
                                                            <div id="sc3" class="switchcontent">
                                                                <iframe id="calframe" name="calframe" src="menu_calendar.php?session=<?echo($session);?>" width="158" height="40" topmargin="0" leftmargin="0" margin="0" marginheight="0" marginwidth="0" frameborder="0" scrolling="no" style="border-style:none;margin:0;padding:0;"></iframe>
                                                            </div>
                                                        </td>
                                                        <td width="1" bgcolor="#a552aa"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" bgcolor="#a552aa"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><img src="img/spacer.gif" width="1" height="5" border="0"></td>
                                        </tr>
<? }?>
                                    <!--calendar-->
                                    <!--quick-->
<?if ($quick_menu_flg == "t" && $authorities[2] == "t") {?>
                                        <tr>
                                            <td valign="top">
                                                <table width="160" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td colspan="3" bgcolor="#35c584"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="1" bgcolor="#35c584"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
                                                        <td width="158">
                                                            <table width="158" border="0" cellspacing="0" cellpadding="0" onClick="expandcontent('sc10')" style="cursor:hand; cursor:pointer">
                                                                <tr bgcolor="#35c584">
                                                                    <td height="22" class="spacing" background="img/bg_quick.gif"><b><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff">クイック登録</font></b></td>
                                                                    <td width="18" height="22" class="spacing" background="img/bg_quick.gif"><b><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><span id="sc10_d">▼</span><span id="sc10_u" style="display:none;">▲</span></font></b></td>
                                                                </tr>
                                                            </table>
                                                            <div id="sc10" class="switchcontent">
                                                                <form name="quick" action="quick_insert.php" target="floatingpage" method="post">
                                                                    <table width="158" border="0" cellspacing="0" cellpadding="1">
                                                                        <tr height="22">
                                                                            <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="schd_start_yrs" style="font-size:11px;"><?show_select_years_future(2, $todayyear);?></select>/<select name="schd_start_mth" style="font-size:11px;"><?show_select_months($todaymonth);?></select>/<select name="schd_start_day" style="font-size:11px;"><?show_select_days($todayday);?></select></font></td>
                                                                        </tr>
                                                                        <tr height="22">
                                                                            <td><input type="text" name="schd_title" value="" style="ime-mode:active;width:144px;"></td>
                                                                        </tr>
                                                                        <tr height="22">
                                                                            <td align="right"><input type="button" value="登録" onclick="createSchedule();
                                                                                    document.quick.schd_title.value = '';"></td>
                                                                        </tr>
                                                                    </table>
                                                                    <input type="hidden" name="session" value="<?echo($session);?>">
                                                                </form>
                                                            </div>
                                                        </td>
                                                        <td width="1" bgcolor="#35c584"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" bgcolor="#35c584"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><img src="img/spacer.gif" width="1" height="5" border="0"></td>
                                        </tr>
                                    <? }?>
                                    <!--quick-->
<?
if ($basic_menu_flg == "t") {
    show_basic_menu($authorities, $session, $fname, "sc4");
}
//XX if ($community_menu_flg == "t") {
//XX     show_community_menu($authorities, $session);
//XX }
if ($business_menu_flg == "t") {
    show_business_menu($authorities, $session, "sc5");
}
show_admin_menu($authorities, $session, "sc6");
?>
                                    <!--license-->
<?if ($license_menu_flg == "t") {?>
                                        <tr>
                                            <td>
                                                <table width="160" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td colspan="3" bgcolor="#818890"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="1" bgcolor="#818890"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
                                                        <td width="158" valign="top">
                                                            <table width="158" border="0" cellspacing="0" cellpadding="0" onclick="expandcontent('sc9')" style="cursor:hand; cursor:pointer">
                                                                <tr bgcolor="#818890">
                                                                    <td height="22" class="spacing" background="img/bg_other.gif"><b><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff">ライセンス</font></b></td>
                                                                    <td width="18" height="22" class="spacing" background="img/bg_other.gif"><b><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><span id="sc9_d">▼</span><span id="sc9_u" style="display:none;">▲</span></font></b></td>
                                                                </tr>
                                                            </table>
                                                            <div id="sc9" class="switchcontent">
                                                                <table width="158" border="0" cellspacing="0" cellpadding="1">
                                                                    <tr>
                                                                        <td width="20" height="22"><a href="./gpl_menu.php?session=<?echo($session);?>" target="floatingpage" onclick="scrollTo(0, 0);"><img src="img/icon/s31.gif" width="20" height="20" border="0" alt=""></a></td>
                                                                        <td width=""><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="./gpl_menu.php?session=<?echo($session);?>" target="floatingpage" onclick="scrollTo(0, 0);">GPLライセンス</a></font></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="20" height="22"><a href="./medis_menu.php?session=<?echo($session);?>" target="floatingpage" onclick="scrollTo(0, 0);"><img src="img/icon/s31.gif" width="20" height="20" border="0" alt=""></a></td>
                                                                        <td width=""><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="./medis_menu.php?session=<?echo($session);?>" target="floatingpage" onclick="scrollTo(0, 0);">MEDIS標準マスタ</a></font></td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </td>
                                                        <td width="1" bgcolor="#818890"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" bgcolor="#818890"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3"><img src="img/spacer.gif" width="1" height="5" alt=""></td>
                                        </tr>
<? }?>
                                    <!--license-->

                                    <!-- disk size -->
<?if ($authorities[20] === "t") {?>
                                        <tr>
                                            <td>
                                                <div id="disk">
                                                    <div id="disk_title">ディスク容量<div id="disk_title_mark">▲</div></div>
                                                    <div id="disk_content">
                                                        <dl>
                                                            <dt>TOTAL</dt>
                                                            <dd><?printf('%.2fGB', $disk_total / 1024 / 1024 / 1024);?></dd>
                                                            <dt>USED</dt>
                                                            <dd><?printf('%.2fGB', $disk_used / 1024 / 1024 / 1024);?></dd>
                                                            <dt>FREE</dt>
                                                            <dd><?printf('%.2fGB', $disk_free / 1024 / 1024 / 1024);?></dd>
                                                        </dl>
                                                        <div id="disk_bar">
                                                            <div id="disk_bar_used" style="width:<?=$used_bar?>px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
<? }?>
                                    <!-- /disk size -->

                                </table>
                            </td>
                            <!--sidemenu-->
<?if ($sidemenu_position == "left") {?>
                                <td bgcolor="#ffffff" width="5" valign="top" id="centerblock"<?echo($sidemenu_style);?>><img src="./img/spacer.gif" width="5" height="1"></td>
                                <td valign="top" bgcolor="#ffffff">
                                    <img src="img/spacer.gif" width="1" height="3" alt=""><br>
                                    <iframe id="floatingpage" name="floatingpage" topmargin="0" leftmargin="0" src="blank.php" frameborder="0" width="100%" height="700" onload="resizeIframe();"></iframe>
                                </td>
<? }?>
                        </tr>
                    </table>
                    <!--body-->
                </td>
            </tr>
        </table>
    </body>
</html>
