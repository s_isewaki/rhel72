<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css">

<?/*

画面パラメータ
	$session
		セッションID
	$pt_id
		患者ID

*/


/**
 * 氏、名を氏名フォーマットに従い氏名に変換します。
 * get_values.iniのget_emp_kanji_name()と同様の変換ルールで変換します。
 * @param $lt_nm 氏
 * @param $ft_nm 名
 * @return 氏名
 */
function format_kanji_name($lt_nm,$ft_nm)
{
	$name = "$lt_nm"." "."$ft_nm";
	return $name;
}


/**
 * プロブレムの一覧を表示します。
 * (この関数はsummary_problem_list.phpの同名関数を元に作成しました。)
 *
 * @param $con DBコネクション
 * @param $pt_id 患者ＩＤ
 * @param $fname 画面名
 */
function show_problem_list($con,$pt_id,$fname)
{

	//==============================
	//DBより一覧データを取得
	//==============================
	$sql = "select summary_problem_list.*,empmst.emp_lt_nm,empmst.emp_ft_nm,empmst.emp_job,jobmst.job_nm from summary_problem_list ";
	$sql .= "left join empmst on empmst.emp_id = summary_problem_list.emp_id ";
	$sql .= "left join jobmst on jobmst.job_id = empmst.emp_job ";
	$cond = "where summary_problem_list.ptif_id = '$pt_id'";

//	//インアクティブは表示しない。
//	$cond .= " and not (summary_problem_list.problem_class = 'インアクティブ')";
//	//解決済みも表示しない。
//	$cond .= " and (summary_problem_list.end_date = '        ')";

	$cond .= " order by summary_problem_list.problem_list_no";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	//==============================
	//一覧表 出力
	//==============================
	?>
	<table id="problem_list_table" class="listp_table" cellspacing="1">
		<tr>
			<th style="text-align:center; width:75" rowspan="2"><nobr>年月日</nobr></th>
			<th style="text-align:center; width:25" rowspan="2">番<br>号</th>
			<th style="text-align:center; width:180" rowspan="2"><nobr>プロブレム</nobr><br><nobr>(問題)</nobr></th>
			<th style="text-align:center" colspan="2"><nobr>記載者</nobr></th>
			<th style="text-align:center; width:110" rowspan="2"><nobr>問題分類</nobr></th>
			<th style="text-align:center; width:75" rowspan="2"><nobr>出現期日</nobr></th>
			<th style="text-align:center; width:200" rowspan="2"><nobr>備考</nobr></th>
			<th style="text-align:center; width:50" rowspan="2"></th>
		</tr>
		<tr>
			<th style="text-align:center; width:100"><nobr>職種</nobr></td>
			<th style="text-align:center; width:100"><nobr>氏名</nobr></td>
		</tr>
		<?
		$list_disp_num = pg_numrows($sel);
		for($i=0;$i<$list_disp_num;$i++) {
			$problem_id = pg_result($sel,$i,"problem_id");
			$problem_list_no = pg_result($sel,$i,"problem_list_no");
			$create_date = pg_result($sel,$i,"create_date");
			$create_date_disp = substr($create_date, 0, 4)."/".substr($create_date, 4, 2)."/".substr($create_date, 6, 2);
			$problem_class = pg_result($sel,$i,"problem_class");
			$problem_kind = pg_result($sel,$i,"problem_kind");
			$disease_name_flg = pg_result($sel,$i,"disease_name_flg");
			if($disease_name_flg == "t") {
				$disease_name_flg_mark = "●";
			} else {
				$disease_name_flg_mark = "○";
			}

			$start_date = pg_result($sel,$i,"start_date");
			if($start_date != "        ") {
				$start_date_disp = substr($start_date, 0, 4)."/".substr($start_date, 4, 2)."/".substr($start_date, 6, 2);
			} else {
				$start_date_disp = "&nbsp;";
			}

			$end_date = pg_result($sel,$i,"end_date");
			if($end_date != "        ") {
				$end_date_disp = substr($end_date, 0, 4)."/".substr($end_date, 4, 2)."/".substr($end_date, 6, 2);
			} else {
				$end_date_disp = "&nbsp;";
			}

			$memo = pg_result($sel,$i,"memo");
			$memo_html = h($memo);

			$problem = pg_result($sel,$i,"problem");
			$problem_html = h($problem);
			$active_probrem_html = "";
			if($problem_class == "アクティブ") {
				$active_probrem_html = $problem_html;
			}
			$inactive_probrem_html = "";
			if($problem_class == "インアクティブ") {
				$inactive_probrem_html = $problem_html;
			}

			$lt_nm = pg_result($sel,$i,"emp_lt_nm");
			$ft_nm = pg_result($sel,$i,"emp_ft_nm");
			$kisaisha = format_kanji_name($lt_nm,$ft_nm);
			$kisaisha_name_html = h($kisaisha);

			$shokushu = pg_result($sel,$i,"job_nm");
			$kisaisha_job_html = h($shokushu);

			//インアクティブ もしくは 解決済み の場合はグレー表示
			$line_color = "";
			if($problem_class == "インアクティブ" || $end_date != "        ") {
				$line_color = "bgcolor='#AAAAAA'";
			}
		?>
		<tr <?=$line_color?>>
			<!-- 作成年月日 -->
			<td style="text-align:center"><nobr><?=$create_date_disp?></nobr></td>
			<!-- 番号 -->
			<td style="text-align:center"><nobr><?=$problem_list_no?></nobr></td>
			<!-- プロブレム -->
			<td style="text-align:center"><nobr>
				<script type="text/javascript">
					var isOpenerFunctionExist = (window.opener && window.opener.problem_list_selected_call_back) ? true : false;
					if (isOpenerFunctionExist) document.write("<a href=\"javascript:problem_selected('<?=$problem_id?>','<?=$problem_list_no?>','<?=$problem?>');\">");
					document.write("<?=str_replace('"','\"',$problem_html)?>");
					if (isOpenerFunctionExist) document.write("</a>");
				</script>
				</nobr>
			</td>
			<!-- 記載者 職種 -->
			<td style="text-align:center"><nobr><?=$kisaisha_job_html?></nobr></td>
			<!-- 記載者 氏名 -->
			<td style="text-align:center"><nobr><?=$kisaisha_name_html?></nobr></td>
			<!-- 問題分類 -->
			<td style="text-align:center"><nobr><?=$problem_kind?></nobr></td>
			<!-- 出現期日 -->
			<td style="text-align:center"><nobr><?=$start_date_disp?></nobr></td>
			<!-- 備考 -->
			<td style="text-align:center"><nobr><?=$memo_html?></nobr></td>
			<!-- 更新ボタン -->
			<td style="text-align:center">
				<input type="button" onclick="showUpdateWindow(<?=$problem_id?>);" value="更新"/>
			</td>
		</tr>
		<?
		}
		?>
	</table>

<? //追加ボタン 出力 ?>

<div class="search_button_div">
	<input type="button" onclick="showInsertWindow('');" value="追加"/>
</div>

<?
}


//==========================================================================================
//実処理開始
//==========================================================================================
require_once("about_comedix.php");
require_once("show_kanja_joho.ini");
require_once("show_sinryo_top_header.ini");
require_once("get_menu_label.ini");
require_once("./get_values.ini");
require_once("label_by_profile_type.ini");
require_once("summary_common.ini");

//ページ名
$fname = $PHP_SELF;

//==============================
//セッションのチェック
//==============================
$session = qualify_session($session,$fname);
if($session == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
// 権限チェック
//==============================
$summary = check_authority($session, 57, $fname);
if ($summary == "0")
{
	showLoginPage();
	exit;
}

//==============================
//DBコネクション
//==============================
$con = connect2db($fname);
if($con == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

//==========================================================================================
//HTML出力
//==========================================================================================
//==============================
//スクリプト/スタイルシート/BODY宣言 出力
//==============================
?>
<title>CoMedix <?=get_report_menu_label($con, $fname)?>｜プロブレム選択</title>
<script type="text/javascript">

var problem_input_window_open_option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=700,height=500";

//プロブレム追加画面を表示します。
function showInsertWindow(insert_default_class) {
	var url = "summary_problem_input.php?session=<?=$session?>&pt_id=<?=$pt_id?>&mode=insert&default_class=" + insert_default_class;
	window.open(url, 'problem_input_window',problem_input_window_open_option);
}

//プロブレム更新画面を表示します。
function showUpdateWindow(problem_id) {
	var url = "summary_problem_input.php?session=<?=$session?>&pt_id=<?=$pt_id?>&mode=update&problem_id=" + problem_id;
	window.open(url, 'problem_input_window',problem_input_window_open_option);
}

//プロブレム選択時、呼び出し画面に通知します。
function problem_selected(problem_id,problem_no,problem) {
	if (window.opener && window.opener.problem_list_selected_call_back){
		window.opener.problem_list_selected_call_back(problem_id,problem_no,problem)
		window.close();
	}
}


function openersListUpdate(){
	if (window.opener && window.opener.problem_list_all_selected_call_back) {
		var datalist = [];
		<?
			$sql =
			 " select * from summary_problem_list".
			 " where ptif_id = '".pg_escape_string($pt_id)."'".
			 " and problem_kind like '医学的領域の問題'".
			 " order by problem_list_no";
			$selall = select_from_table($con,$sql,"",$fname);
			while($row = pg_fetch_array($selall)) {
		?>
				datalist.push({
					"problem_id":"<?=str_replace('"','\"',$row["problem_id"])?>",
					"problem_list_no":"'<?=str_replace('"','\"',$row["problem_list_no"])?>",
					"problem":"<?=str_replace('"','\"',$row["problem"])?>",
					"icd10":"<?=str_replace('"','\"',$row["icd10"])?>",
					"byoumei_code":"<?=str_replace('"','\"',$row["byoumei_code"])?>"
				});
		<?} ?>
		window.opener.problem_list_all_selected_call_back(datalist);
	}
}

</script>
</head>
<body onload="openersListUpdate()">


<div style="padding:4px">

<? //ヘッダー 出力 ?>
<? summary_common_show_dialog_header("プロブレム選択"); ?>

<? //追加選択/一覧選択切り替え ?>
<? summary_common_show_problem_select("一覧選択", $pt_id); ?>


<? //一覧 出力 ?>
<div style="margin-top:12px">
<? show_problem_list($con,$pt_id,$fname); ?>
</div>


</div>
</body>
</html>
<? pg_close($con); ?>
