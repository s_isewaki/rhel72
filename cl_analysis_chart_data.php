<?
ob_start();

require_once("charts/charts.php");
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("cl_application_workflow_common_class.php");

$fname = $PHP_SELF;

if ($level == "2") {
	// 2011/12/1 Yamagawa add(s)
	if ($nurse_type == "1") {
	// 2011/12/1 Yamagawa add(e)
		$arr_eval = array("", "看護過程展開能力\nNo.1", "看護過程実践能力\nNo.3", "ﾌﾟﾚｾﾞﾝﾃｰｼｮﾝ能力\nNo.5", "自己学習能力\nNo.8", "指導・教育能力\nNo.6 + No.9", "問題解決／ﾘｰﾀﾞｰｼｯﾌﾟ能力\nNo.10");
		$arr_pass = array("合格ライン", "60", "53", "40", "50", "0", "55");
	// 2011/12/1 Yamagawa add(s)
	} else if ($nurse_type == "2") {
		$arr_eval = array("", "看護過程展開能力\nNo.1", "看護過程実践能力\nNo.3", "ﾌﾟﾚｾﾞﾝﾃｰｼｮﾝ能力\nNo.5", "自己学習能力\nNo.8", "指導・教育能力\nNo.6 + No.9", "問題解決／ﾘｰﾀﾞｰｼｯﾌﾟ能力\nNo.10");
		$arr_pass = array("合格ライン", "60", "53", "40", "50", "0", "55");
	} else if ($nurse_type == "3") {
		$arr_eval = array("", "看護過程展開能力\nNo.1", "看護過程実践能力\nNo.3", "ﾌﾟﾚｾﾞﾝﾃｰｼｮﾝ能力\nNo.5", "自己学習能力\nNo.8", "指導・教育能力\nNo.6 + No.9", "問題解決／ﾘｰﾀﾞｰｼｯﾌﾟ能力\nNo.10");
		$arr_pass = array("合格ライン", "0", "53", "40", "50", "0", "55");
	}
	// 2011/12/1 Yamagawa add(e)
} else if($level == "3") {
	// 2011/12/1 Yamagawa add(s)
	if ($nurse_type == "1") {
	// 2011/12/1 Yamagawa add(e)
		$arr_eval = array("", "看護過程展開能力\nNo.2", "看護過程実践能力\nNo.4", "ﾌﾟﾚｾﾞﾝﾃｰｼｮﾝ能力\nNo.5", "自己学習能力\nNo.8", "指導・教育能力\nNo.7 + No.9", "問題解決／ﾘｰﾀﾞｰｼｯﾌﾟ能力\nNo.11");
		$arr_pass = array("合格ライン", "80", "60", "40", "33", "0", "53");
	// 2011/12/1 Yamagawa add(s)
	} else if ($nurse_type == "2") {
		$arr_eval = array("", "看護過程展開能力\nNo.2", "看護過程実践能力\nNo.4", "ﾌﾟﾚｾﾞﾝﾃｰｼｮﾝ能力\nNo.5", "自己学習能力\nNo.8", "指導・教育能力\nNo.7 + No.9", "問題解決／ﾘｰﾀﾞｰｼｯﾌﾟ能力\nNo.11");
		$arr_pass = array("合格ライン", "80", "60", "40", "22", "0", "53");
	} else if ($nurse_type == "3") {
		$arr_eval = array("", "看護過程展開能力\nNo.1", "看護過程実践能力\nNo.3", "ﾌﾟﾚｾﾞﾝﾃｰｼｮﾝ能力\nNo.5", "自己学習能力\nNo.8", "指導・教育能力\nNo.7 + No.9", "問題解決／ﾘｰﾀﾞｰｼｯﾌﾟ能力\nNo.11");
		$arr_pass = array("合格ライン", "0", "60", "40", "33", "0", "53");
	}
	// 2011/12/1 Yamagawa add(e)
}

$con = connect2db($fname);
$obj = new cl_application_workflow_common_class($con, $fname);

// スコア取得
$arr_apply_wkfwmst = $obj->get_apply_wkfwmst($apply_id);
$apply_content = $arr_apply_wkfwmst[0]["apply_content"];
// 2011/12/1 Yamagawa upd(s)
//$arr_score_data = $obj->get_score_data($apply_content, $level);
$arr_score_data = $obj->get_score_data($apply_content, $level, $nurse_type);
// 2011/12/1 Yamagawa upd(e)

// グラフのタイプを円状棒グラフにする。
$chart['chart_type'] = "polar";

$chart['chart_data'][0] = $arr_eval;
$chart['chart_data'][1] = $arr_score_data;
$chart['chart_data'][2] = $arr_pass;

// 日本語表示に必要
$chart['chart_value'] = array('font' => "Sans Serif",);
$chart['legend_label'] = array('font' => "Sans Serif",);
$chart['axis_category'] = array('font' => "Sans Serif", 'size' => 12);

// グラフ軸の詳細
$chart['axis_value'] = array ('min'              =>  0,
                              'max'              =>  100,
                              'steps'            =>  5,
                              'prefix'           =>  "",
                              'suffix'           =>  "%",
                              'decimals'         =>  0,
                              'decimal_char'     =>  "",
                              'separator'        =>  "",
                              'show_min'         =>  true,
                              'font'             =>  "Sans Serif",
//                            'bold'             =>  true,
                              'size'             =>  14,
                              'color'            =>  "#000000",
//                            'alpha'            =>  100,
//                            'orientation'      =>  "horizontal",
                             );

// グラフ外枠の線
$chart['chart_border'] = array ('bottom_thickness' => 0, 'left_thickness' => 0);

// 申請スコア・合格スコアの表示を線で表す
$chart['chart_pref'] = array ('point_shape' => "square", 'point_size' => 5, 'fill_shape' => true, 'line_thickness' => 2, 'type' => "line");

// 申請スコア・合格スコアの色を決定する
$chart['series_color'] = array ("88ffff", "ff4400");

// グラフ内の区切りをダッシュラインにする
$chart['chart_grid_h'] = array ('type' => "dashed");

ob_clean();
mb_convert_variables('UTF-8', 'EUC-JP', $chart);
SendChartData($chart);
ob_end_flush();
?>
