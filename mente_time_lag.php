<?php
header("Cache-Control: no-cache");
header("Pragma: no-cache");
header("Expires: Thu, 01 Jan 1970 00:00:00 GMT");

chdir(dirname(__FILE__));
require_once("about_postgres.php");

$fname = $PHP_SELF;

// データベースに接続
$con = connect2db($fname);

ini_set("max_execution_time", 0);

$sql = "SELECT relname FROM pg_class WHERE relkind = 'r' AND relname = 'inci_easyinput_data_bakcup_timelag'";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if (pg_num_rows($sel) < 1) {

	$sql = "select *,oid as work_oid into inci_easyinput_data_bakcup_timelag from inci_easyinput_data";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}



$sql = "select * from inci_easyinput_data where grp_code=105 and easy_item_code=75";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

$count = 0;
while ($row = pg_fetch_array($sel)) {
	
//	$work = "0日 08時間 31分";
	$work = $row["input_item"];

	if(is_numeric($work))
	{
		//数値であれば既に変換済なので処理をスキップ
		continue;
	}


	preg_match_all("/[0-9]/",$work,$match);
	
	list($one, $two, $three) = preg_split('/[\s]+/', $work, -1, PREG_SPLIT_NO_EMPTY);
	
	$int_day = mb_ereg_replace ("[^0-9]","", $one);
	$int_hour = mb_ereg_replace ("[^0-9]","", $two);
	$int_minute = mb_ereg_replace ("[^0-9]","", $three);
	
	$int_day = ($int_day * 60 * 60 * 24);
	$int_hour = ($int_hour * 60 * 60);
	$int_minute = ($int_minute * 60);
	$int_time = $int_day+$int_hour+$int_minute;
	
	//日付単位登録を秒単位登録へ変更
	//（x60）31分：1860秒
	//(x60x60)8時間：28800秒
	//(x60x60x24)5日：432000秒
	
	$sql = "update inci_easyinput_data set ";
	$set = array("input_item");
	$setvalue = array($int_time);
	$cond = "where eid_id={$row["eid_id"]} and eis_id={$row["eis_id"]} 
			and report_id={$row["report_id"]} and grp_code={$row["grp_code"]} 
			and easy_item_code={$row["easy_item_code"]} ";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script type='text/javascript'>showErrorPage(window);</script>");
		exit;
	}

}