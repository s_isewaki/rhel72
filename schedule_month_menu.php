<?php
$fname = $_SERVER["PHP_SELF"];

require_once("Cmx.php");
require_once("aclg_set.php");
require_once("about_comedix.php");
require_once("time_check.ini");
require_once("project_check.ini");
require_once("show_date_navigation_month.ini");
require_once("holiday.php");
require_once("show_schedule_common.ini");
require_once("show_schedule_chart.ini");
require_once("label_by_profile_type.ini");
require_once("show_calendar_memo.ini");

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    js_login_exit();
}

// 権限のチェック
$checkauth = check_authority($session, 2, $fname);
if ($checkauth == "0") {
    js_login_exit();
}

// スケジュール管理権限を取得
$schd_place_auth = check_authority($session, 13, $fname);

// 日付の設定
$this_month = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
if ($date == "") {
    $date = $this_month;
}
$day = date("d", $date);
$month = date("m", $date);
$year = date("Y", $date);

// 「前月」「翌月」のタイムスタンプを取得
$last_month = get_last_month($date);
$next_month = get_next_month($date);

// データベースに接続
$con = connect2db($fname);

// ログインユーザの職員IDを取得
$sql = "SELECT emp_id FROM session WHERE session_id='{$session}'";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0) {
    pg_close($con);
    js_error_exit();
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// アクセスログ
$optsql = "select schedule1_default from option";
$optcond = "where emp_id = '{$emp_id}'";
$optsel = select_from_table($con, $optsql, $optcond, $fname);
if ($optsel == 0) {
    pg_close($con);
    js_error_exit();
}
$schedule1_default = pg_fetch_result($optsel, 0, "schedule1_default");

if ($schedule1_default === '3') {
    aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);
}

// 院内行事チェックボックスの処理
if ($timegd == "on" || $timegd == "off") {
    time_update($con, $emp_id, $timegd, $fname);
}
else if ($timegd == "") {
    $timegd = time_check($con, $emp_id, $fname);
}

// 委員会・WGチェックボックスの処理
if ($pjtgd == "on" || $pjtgd == "off") {
    project_update($con, $emp_id, $pjtgd, $fname);
}
else if ($pjtgd == "") {
    $pjtgd = project_check($con, $emp_id, $fname);
}

// スケジュール機能の初期画面情報を取得
$default_view = get_default_view($con, $emp_id, $fname);

// オプション設定情報を取得
$sql = "SELECT * FROM option WHERE emp_id='{$emp_id}'";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0) {
    pg_close($con);
    js_error_exit();
}
$calendar_start = pg_fetch_result($sel, 0, "calendar_start1");

// スタート曜日の設定
$start_wd = ($calendar_start == 1) ? 0 : 1;  // 0：日曜、1：月曜
// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix スケジュール | 月</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/popup.js"></script>
<script type="text/javascript">
function showGuide() {
    var timegd = (document.timegd.timeguide.checked) ? 'on' : 'off';
    location.href = 'schedule_month_menu.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>&timegd=' + timegd;
}

function showProject() {
    var pjtgd = (document.timegd.pjtguide.checked) ? 'on' : 'off';
    location.href = 'schedule_month_menu.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>&pjtgd=' + pjtgd;
}

function openPrint() {
    window.open('schedule_print_month_menu.php?&session=<?php eh($session); ?>&date=<?php eh($date); ?>', 'newwin', 'width=800,height=600,scrollbars=yes');
}

function changeDate(dt) {
    location.href = 'schedule_month_menu.php?session=<?php eh($session); ?>&date=' + dt;
}

function popupScheduleDetail(title, place, date, time, type, detail, e) {
    popupDetailBlue(
        new Array(
            'タイトル', title,
            '行先', place,
            '日付', date,
            '時刻', time,
            '種別', type,
            '詳細', detail
        ), 400, 80, e
    );
}

function popupEventDetail(fcl_name, evt_date, evt_time, evt_name, evt_content, evt_contact, e) {
    popupDetailBlue(
        new Array(
            '施設', fcl_name,
            '日付', evt_date,
            '時刻', evt_time,
            '行事名', evt_name,
            '詳細', evt_content,
            '連絡先', evt_contact
        ), 400, 80, e
    );
}

// CSVダウンロード用
function downloadCSV(){
    document.csv.action = 'schedule_csv.php';
    document.csv.submit();

}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<?php eh($default_view); ?>?session=<?php eh($session); ?>"><img src="img/icon/b02.gif" width="32" height="32" border="0" alt="スケジュール"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php eh($default_view); ?>?session=<?php eh($session); ?>"><b>スケジュール</b></a></font></td>
<? if ($schd_place_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="schedule_place_list.php?session=<?php eh($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom:2px solid #5279a5; margin-bottom:5px;">
<tr height="22">
<td width="70" align="center" bgcolor="#bdd1e7"><a href="schedule_menu.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日</font></a></td>
<td width="5"></td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="schedule_week_menu.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">週</font></a></td>
<td width="5"></td>
<td width="70" align="center" bgcolor="#5279a5"><a href="schedule_month_menu.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>月</b></font></a></td>
<td width="5"></td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="schedule_year_menu.php?session=<?php eh($session); ?>&year=<?php eh($year); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">年</font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_list_upcoming.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一覧</font></a></td>
<td width="5"></td>
<td width="160" align="center" bgcolor="#bdd1e7"><a href="schedule_search.php?session=<?php eh($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">公開スケジュール検索</font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_mygroup.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">マイグループ</font></a></td>
<td width="5"></td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="schedule_ical.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カレンダー連携</font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_option.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション</font></a></td>
<td align="right"><input type="button" value="印刷" onClick="openPrint();"></td>
<td width="20"><input type="button" value="CSV出力" onclick="downloadCSV();"></td>
</tr>
</table>
<form name="timegd">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td height="22"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<a href="schedule_month_menu.php?session=<?php eh($session); ?>&date=<?php eh($last_month); ?>">&lt;前月</a>
<a href="schedule_month_menu.php?session=<?php eh($session); ?>&date=<?php eh($this_month); ?>">今月</a>
<select onchange="changeDate(this.options[this.selectedIndex].value);">
<? show_date_options_m($date); ?>
</select>
<a href="schedule_month_menu.php?session=<?php eh($session); ?>&date=<?php eh($next_month); ?>">翌月&gt;</a>
</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 院内行事/所内行事
echo ($_label_by_profile["EVENT"][$profile_type]); ?></font><input type="checkbox" name="timeguide" onClick="showGuide()" <? if ($timegd == "on") {echo(" checked");} ?>>&nbsp;<a href="javascript:void(0);" onclick="window.open('schedule_project_setting.php?session=<?php eh($session); ?>', 'newwin', 'width=640,height=700,scrollbars=yes');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">委員会・WG</font></a><input type="checkbox" name="pjtguide" onClick="showProject();" <? if ($pjtgd == "on") {echo(" checked");} ?>></td>
</tr>
</table>
<? show_menu_schedule_list_m($date, $emp_id, $session, $fname, $start_wd, $con); ?>
</form>
</td>
</tr>
</table>

<!-- CSV出力用form -->
<form name="csv" method="get">
    <input type="hidden" name="session" value="<?php echo $session; ?>">
    <input type="hidden" name="start_date" value="<?php echo $year.$month.'01' ?>">
    <input type="hidden" name="end_date" value="<?php echo $year.$month.date('t',$date) ?>">
    <input type="hidden" name="pjt_f" value="<?php echo $pjtgd ?>">
    <input type="hidden" name="time_f" value="<?php echo $timegd ?>">
</form>

</body>
<? pg_close($con); ?>
</html>
<?
// 月間スケジュールを表示
function show_menu_schedule_list_m($date, $emp_id, $session, $fname, $start_wd, $con) {

    // 曜日行の表示
    echo("<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"2\">");
    echo("<tr height=\"22\">\n");
    echo("<td width=\"9%\" bgcolor=\"#f6f9ff\">&nbsp;</td>\n");
    for ($i = 0; $i < 7; $i++) {
        $tmp_wd = $start_wd + $i;
        if ($tmp_wd >= 7) {
            $tmp_wd -= 7;
        }
        switch ($tmp_wd) {
        case 0:
            $show_wd = "日";
            $bgcolor = "#fadede";
            break;
        case 1:
            $show_wd = "月";
            $bgcolor = "#fefcdf";
            break;
        case 2:
            $show_wd = "火";
            $bgcolor = "#fefcdf";
            break;
        case 3:
            $show_wd = "水";
            $bgcolor = "#fefcdf";
            break;
        case 4:
            $show_wd = "木";
            $bgcolor = "#fefcdf";
            break;
        case 5:
            $show_wd = "金";
            $bgcolor = "#fefcdf";
            break;
        case 6:
            $show_wd = "土";
            $bgcolor = "#defafa";
            break;
        }
        echo("<td width=\"13%\" align=\"center\" bgcolor=\"$bgcolor\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$show_wd</font></td>\n");
    }
    echo("</tr>\n");

    // 当月1日〜末日の配列を作成
    $year = date("Y", $date);
    $month = date("m", $date);
    $arr_date = array();
    for ($i = 1; $i <= 31; $i++) {
        $day = substr("0" . $i, -2);
        if (checkdate($month, $day, $year)) {
            array_push($arr_date, "$year$month$day");
        } else {
            break;
        }
    }

    // 空白セル分の日付を配列の先頭に追加
    $first_day = mktime(0, 0, 0, $month, "01", $year);
    $empty = date("w", $first_day) - $start_wd;
    if ($empty < 0) {
        $empty += 7;
    }
    for ($i = 1; $i <= $empty; $i++) {
        array_unshift($arr_date, date("Ymd", strtotime("-$i days", $first_day)));
    }

    // 空白セル分の日付を配列の末尾に追加
    $day = substr($arr_date[count($arr_date) - 1], -2);
    $end_day = mktime(0, 0, 0, $month, $day, $year);
    $empty = 7 - (count($arr_date) % 7);
    if ($empty == 7) {
        $empty = 0;
    }
    for ($i = 1; $i <= $empty; $i++) {
        array_push($arr_date, date("Ymd", strtotime("$i days", $end_day)));
    }

    // 配列を週単位に分割
    $arr_week = array_chunk($arr_date, 7);

    // 「週表示」リンク出力用インデックスを求める
    $index = date("w", $date) - $start_wd;
    if ($index < 0) {
        $index += 7;
    }

    // カレンダーのメモを取得
    $start_date = "$year{$month}01";
    $end_date = "$year{$month}31";
    $arr_calendar_memo = get_calendar_memo($con, $fname, $start_date, $end_date);

    // 当月の予定を取得
    $schedules = get_schedules_array($con, $emp_id, $year, $month, $fname);

    // 予定行を表示
    foreach ($arr_week as $arr_date) {
        echo("<tr height=\"80\">\n");

        // ＜週間＞セルを出力（リンク先は処理日と同じ曜日）
        $tmp_year = substr($arr_date[$index], 0, 4);
        $tmp_month = substr($arr_date[$index], 4, 2);
        $tmp_day = substr($arr_date[$index], 6, 2);
        $tmp_date = mktime(0, 0, 0, $tmp_month, $tmp_day, $tmp_year);
        echo("<td align=\"right\" bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"schedule_week_menu.php?session=$session&date=$tmp_date\">&lt;週間&gt;</a></font></td>\n");

        // 日付セルを出力
        foreach ($arr_date as $tmp_ymd) {
            $tmp_year = substr($tmp_ymd, 0, 4);
            $tmp_month = substr($tmp_ymd, 4, 2);
            $tmp_day = substr($tmp_ymd, 6, 2);
            $tmp_date = mktime(0, 0, 0, $tmp_month, $tmp_day, $tmp_year);
            $wd = date("w", $tmp_date);

            if ($tmp_ymd == date("Ymd")) {
                $bgcolor = "#ccffcc";
            } else {
                if ($wd == 6) {
                    $bgcolor = "#defafa";
                } else if ($wd == 0) {
                    $bgcolor = "#fadede";
                } else {
                    $bgcolor = "#fefcdf";
                }
            }

            // 他の月の場合は空表示
            if ($tmp_month != $month) {
                echo("<td bgcolor=\"$bgcolor\">&nbsp;</td>\n");

            // 当月の場合は詳細表示
            } else {

                $holiday_name = ktHolidayName($tmp_date);
                if ($tmp_ymd == date("Ymd")) {
                    $bgcolor = "#ccffcc";
                } else if ($holiday_name != "") {
                    $bgcolor = "#fadede";
                } else if ($arr_calendar_memo["{$tmp_ymd}_type"] == "5") {
                    $bgcolor = "#defafa";
                }
                $tmp_day = intval($tmp_day);

                // 出勤予定を取得
                $tmp_schd_type_name = get_schd_name($con, $emp_id, $emp_id, $tmp_ymd, $fname, true);
                if ($tmp_schd_type_name != "") {
                    $tmp_schd_type_name = "&nbsp;$tmp_schd_type_name";
                }

                echo("<td bgcolor=\"$bgcolor\" valign=\"top\">\n");

                echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
                echo("<tr>\n");
                echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\"><b><a href=\"schedule_menu.php?session=$session&date=$tmp_date\">$tmp_day</a></b></font><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_schd_type_name</font></td>\n");
                echo("<td align=\"right\"><img src=\"img/pencil.gif\" alt=\"\" width=\"13\" height=\"13\" onclick=\"window.open('schedule_registration.php?session=$session&schd_emp_id=$emp_id&date=$tmp_date', 'newwin', 'width=640,height='+((screen.availHeight-60<800)? screen.availHeight-60: 800)+',scrollbars=yes');\" style=\"cursor:pointer;\"></td>\n");
                echo("</tr>\n");
                echo("</table>\n");

                // カレンダーのメモがある場合は設定する
                if ($arr_calendar_memo["$tmp_ymd"] != "") {
                    if ($holiday_name == "") {
                        $holiday_name = $arr_calendar_memo["$tmp_ymd"];
                    } else {
                        $holiday_name .= "<br>".$arr_calendar_memo["$tmp_ymd"];
                    }
                }

                if ($holiday_name != "") {
                    echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
                    echo("<tr>\n");
                    echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"red\">$holiday_name</font></td>\n");
                    echo("</tr>\n");
                    echo("</table>\n");
                }

                display_schedule_list($con, $emp_id, $tmp_ymd, $schedules[$tmp_ymd], $session, $fname);

                echo("</td>\n");
            }
        }
        echo("</tr>\n");
    }
    echo("</table>\n");
}

// スケジュール一覧を出力
function display_schedule_list($con, $emp_id, $ymd, $schedules, $session, $fname) {
    $schedule_count = count($schedules);
    if ($schedule_count == 0) {
        return;
    }

    $year = substr($ymd, 0, 4);
    $month = substr($ymd, 4, 2);
    $day = substr($ymd, 6, 2);
    $date = mktime(0, 0, 0, $month, $day, $year);
    $formatted_date = "$year/$month/$day";

    // スケジュールを表示
    echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
    for ($i = 0; $i < $schedule_count; $i++) {
        $tmp_object_type = $schedules[$i]["object_type"];
        $tmp_timeless = $schedules[$i]["timeless"];
        $tmp_schedule_id = $schedules[$i]["schedule_id"];
        $tmp_title = $schedules[$i]["title"];
        $tmp_place = $schedules[$i]["place"];
        $tmp_place_detail = $schedules[$i]["place_detail"];
        $tmp_facility = $schedules[$i]["facility"];
        $tmp_fcl_detail = $schedules[$i]["fcl_detail"];
        $tmp_start_time = substr($schedules[$i]["start_time"], 0, 5);
        $tmp_end_time = substr($schedules[$i]["end_time"], 0, 5);
        $tmp_schedule_type = $schedules[$i]["schedule_type"];
        $tmp_detail = preg_replace("/\r?\n/", "<br>", $schedules[$i]["detail"]);
        $tmp_status = ($schedules[$i]["status"] == "2") ? "[未承認]" : "";
        $tmp_anchor_style = get_style_by_marker($schedules[$i]["marker_id"]);
        $tmp_project_id = $schedules[$i]["project_id"];
        $tmp_project_name = $schedules[$i]["project_name"];
        $tmp_contact = preg_replace("/\r?\n/", "<br>", $schedules[$i]["contact"]);
        $tmp_place_id = $schedules[$i]["schd_place_id"];

        if ($tmp_place_id == "0") {
            $tmp_place = "その他";
        }
        if ($tmp_place_detail != "") {
            $tmp_place .= " $tmp_place_detail";
        }
        if ($tmp_facility == "") {
            $tmp_facility = "全体";
        }
        if ($tmp_fcl_detail != "") {
            $tmp_facility .= " $tmp_fcl_detail";
        }

        // 時間の設定
        if ($tmp_timeless) {
            $timeless = "t";
            $formatted_time1 = "<img src=\"img/icon/timeless.gif\" alt=\"時刻指定なし\" width=\"8\" height=\"8\">&nbsp;";
            $formatted_time2 = "指定なし";
        } else {
            $timeless = "";
            $formatted_time1 = "{$tmp_start_time}-{$tmp_end_time}<br>";
            $formatted_time2 = "$tmp_start_time 〜 $tmp_end_time";
        }

        // ボーダーの設定
        if ($i < $schedule_count - 1) {
            $style = "padding:1px;border-bottom:silver dotted 1px;";
        } else {
            $style = "padding:1px;";
        }

        echo("<tr>\n<td style=\"$style\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$formatted_time1");
        switch ($tmp_object_type) {

        case "personal":  // 個人スケジュール
            echo("<a href=\"javascript:void(0);\" onclick=\"window.open('schedule_update.php?session=$session&timeless=$timeless&schd_id=$tmp_schedule_id', 'newwin', 'width=640,height='+((screen.availHeight-60<800)? screen.availHeight-60: 800)+',scrollbars=yes');\" onmousemove=\"popupScheduleDetail('" . h(h_jsparam($tmp_title)) . "', '$tmp_place', '$formatted_date', '$formatted_time2', '$tmp_schedule_type', '" . h(preg_replace("/\r?\n/", "<br>", h_jsparam($schedules[$i]["detail"]))) . "', event);\" onmouseout=\"closeDetail();\"$tmp_anchor_style>" . h("$tmp_title$tmp_status") . "</a>");
            break;

        case "project":  // 委員会・WGスケジュール
            echo("<font color=\"red\">委員会・WG：</font><a href=\"javascript:void(0);\" onclick=\"window.open('project_schedule_update.php?session=$session&pjt_schd_id=$tmp_schedule_id&pjt_id=$tmp_project_id&date=$date&time=$tmp_start_time&timeless=$timeless', 'newwin', 'width=640,height=480,scrollbars=yes');\" onmousemove=\"popupScheduleDetail('$tmp_title', '$tmp_place', '$formatted_date', '$formatted_time2', '$tmp_schedule_type', '$tmp_detail', event);\" onmouseout=\"closeDetail();\">$tmp_project_name $tmp_title</a>");
            break;

        case "timeguide":  // 行事

                // 「院内行事」権限のチェック
                $check_auth = check_authority($session, 12, $fname);
                if ($check_auth == "0") {
                    echo("<font color=\"red\">行事：</font><a href=\"javascript:void(0);\" onclick=\"window.open('time_guide_open_detail.php?session=$session&e_id=$tmp_schedule_id', 'newwin', 'width=640,height=480,scrollbars=yes');\" onmousemove=\"popupEventDetail('$tmp_facility', '$formatted_date', '$formatted_time2', '$tmp_title', '$tmp_detail', '$tmp_contact', event);\" onmouseout=\"closeDetail();\">$tmp_title</a>");
                }
                else
                {
                    //「院内行事」の権限があるので編集可能とする
                    echo("<font color=\"red\">行事：</font><a href=\"javascript:void(0);\" onclick=\"window.open('time_guide_detail.php?session=$session&e_id=$tmp_schedule_id&sch=on', 'newwin', 'width=640,height=480,scrollbars=yes');\" onmousemove=\"popupEventDetail('$tmp_facility', '$formatted_date', '$formatted_time2', '$tmp_title', '$tmp_detail', '$tmp_contact', event);\" onmouseout=\"closeDetail();\">$tmp_title</a>");
                }


            break;

        }
        echo("</font></td>\n</tr>\n");
    }
    echo("</table>\n");
}

// スケジュール情報を配列で取得
function get_schedules_array($con, $emp_id, $year, $month, $fname) {
    $timeless_schedules = get_timeless_schedule_array($con, $emp_id, $year, $month, $fname);
    $timeless_project_schedules = get_timeless_project_schedule_array($con, $emp_id, $year, $month, $fname);
    $timeless_events = get_timeless_event_array($con, $emp_id, $year, $month, $fname);
    $normal_schedules = get_normal_schedule_array($con, $emp_id, $year, $month, $fname);
    $normal_project_schedules = get_normal_project_schedule_array($con, $emp_id, $year, $month, $fname);
    $normal_events = get_normal_event_array($con, $emp_id, $year, $month, $fname);
    $schedules = merge_schedules($year, $month, $timeless_schedules, $timeless_project_schedules, $timeless_events, $normal_schedules, $normal_project_schedules, $normal_events);
    return $schedules;
}

// 時間指定のない個人スケジュールを配列で取得
function get_timeless_schedule_array($con, $emp_id, $year, $month, $fname) {
    $sql = "select s.schd_id, s.schd_title, p.place_name, s.schd_plc, t.type_name, s.schd_detail, s.schd_status, s.marker, s.schd_place_id, to_char(s.schd_start_date, 'YYYYMMDD') as date from (schdmst2 s inner join schdtype t on t.type_id = s.schd_type) left join scheduleplace p on p.place_id = s.schd_place_id";
    $cond = "where s.emp_id = '$emp_id' and to_char(s.schd_start_date, 'YYYYMM') = '$year$month' order by s.schd_id";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    $ret = array();
    while ($row = pg_fetch_array($sel)) {
        $ret[$row["date"]][] = array(
            "object_type" => "personal",
            "timeless" => true,
            "schedule_id" => $row["schd_id"],
            "title" => $row["schd_title"],
            "place" => $row["place_name"],
            "place_detail" => $row["schd_plc"],
            "schedule_type" => $row["type_name"],
            "detail" => $row["schd_detail"],
            "status" => $row["schd_status"],
            "marker_id" => $row["marker"],
            "schd_place_id" => $row["schd_place_id"]
        );
    }
    return $ret;
}

// 時間指定のない委員会・WGスケジュールを配列で取得
function get_timeless_project_schedule_array($con, $emp_id, $year, $month, $fname) {
    $sql = "select s.pjt_schd_id, s.pjt_schd_title, pl.place_name, s.pjt_schd_plc, t.type_name, s.pjt_schd_detail, s.pjt_id, pr.pjt_name_abbrev, s.pjt_schd_place_id, to_char(s.pjt_schd_start_date, 'YYYYMMDD') as date from ((proschd2 s inner join project pr on s.pjt_id = pr.pjt_id) inner join proschdtype t on t.type_id = s.pjt_schd_type) left join scheduleplace pl on pl.place_id = s.pjt_schd_place_id";
    $cond = "where exists (select * from projectcheck where emp_id = '$emp_id' and ownother = '1' and projectcheck = true) and s.pjt_id in (select pjt_id from schdproject where emp_id = '$emp_id' and ownother = '1') and to_char(s.pjt_schd_start_date, 'YYYYMM') = '$year$month' and s.pjt_id in (select pjt_id from project where pjt_delete_flag = 'f') order by s.pjt_schd_id";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    $ret = array();
    while ($row = pg_fetch_array($sel)) {
        $ret[$row["date"]][] = array(
            "object_type" => "project",
            "timeless" => true,
            "schedule_id" => $row["pjt_schd_id"],
            "title" => $row["pjt_schd_title"],
            "place" => $row["place_name"],
            "place_detail" => $row["pjt_schd_plc"],
            "schedule_type" => $row["type_name"],
            "detail" => $row["pjt_schd_detail"],
            "project_id" => $row["pjt_id"],
            "project_name" => $row["pjt_name_abbrev"],
            "schd_place_id" => $row["pjt_schd_place_id"]
        );
    }
    return $ret;
}

// 時間指定のない行事を配列で取得
function get_timeless_event_array($con, $emp_id, $year, $month, $fname) {
    $sql = "select s.event_id, s.event_name, f.name as fcl_name, s.event_content, s.fcl_detail, s.contact, to_char(s.event_date, 'YYYYMMDD') as date from timegd s left join timegdfcl f on f.fcl_id = s.fcl_id";
    $cond = "where exists (select * from timecheck where emp_id = '$emp_id' and ownother = '1' and timecheck = true) and to_char(s.event_date, 'YYYYMM') = '$year$month' and s.time_flg = false order by s.event_id";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    $ret = array();
    while ($row = pg_fetch_array($sel)) {
        $ret[$row["date"]][] = array(
            "object_type" => "timeguide",
            "timeless" => true,
            "schedule_id" => $row["event_id"],
            "title" => $row["event_name"],
            "facility" => $row["fcl_name"],
            "fcl_detail" => $row["fcl_detail"],
            "detail" => $row["event_content"],
            "contact" => $row["contact"]
        );
    }
    return $ret;
}

// 時間指定のある個人スケジュールを配列で取得
function get_normal_schedule_array($con, $emp_id, $year, $month, $fname) {
    $sql = "select s.schd_id, s.schd_title, p.place_name, s.schd_plc, s.schd_start_time, s.schd_dur, t.type_name, s.schd_detail, s.schd_status, s.marker, s.schd_start_time_v, s.schd_dur_v, s.schd_place_id, to_char(s.schd_start_date, 'YYYYMMDD') as date from (schdmst s inner join schdtype t on t.type_id = s.schd_type) left join scheduleplace p on p.place_id = s.schd_place_id";
    $cond = "where s.emp_id = '$emp_id' and to_char(s.schd_start_date, 'YYYYMM') = '$year$month' order by s.schd_start_time_v, s.schd_dur_v, s.schd_id";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    $ret = array();
    while ($row = pg_fetch_array($sel)) {
        $ret[$row["date"]][] = array(
            "object_type" => "personal",
            "timeless" => false,
            "schedule_id" => $row["schd_id"],
            "title" => $row["schd_title"],
            "place" => $row["place_name"],
            "place_detail" => $row["schd_plc"],
            "start_time" => substr($row["schd_start_time_v"], 0, 2).":".substr($row["schd_start_time_v"], 2, 2),
            "end_time" => substr($row["schd_dur_v"], 0, 2).":".substr($row["schd_dur_v"], 2, 2),
            "schedule_type" => $row["type_name"],
            "detail" => $row["schd_detail"],
            "status" => $row["schd_status"],
            "marker_id" => $row["marker"],
            "schd_place_id" => $row["schd_place_id"]
        );
    }
    return $ret;
}

// 時間指定のある委員会・WGスケジュールを配列で取得
function get_normal_project_schedule_array($con, $emp_id, $year, $month, $fname) {
    $sql = "select s.pjt_schd_id, s.pjt_schd_title, pl.place_name, s.pjt_schd_plc, s.pjt_schd_start_time, s.pjt_schd_dur, t.type_name, s.pjt_schd_detail, s.pjt_id, pr.pjt_name_abbrev, s.pjt_schd_start_time_v, s.pjt_schd_dur_v, s.pjt_schd_place_id, to_char(s.pjt_schd_start_date, 'YYYYMMDD') as date from ((proschd s inner join project pr on s.pjt_id = pr.pjt_id) inner join proschdtype t on t.type_id = s.pjt_schd_type) left join scheduleplace pl on pl.place_id = s.pjt_schd_place_id";
    $cond = "where exists (select * from projectcheck where emp_id = '$emp_id' and ownother = '1' and projectcheck = true) and s.pjt_id in (select pjt_id from schdproject where emp_id = '$emp_id' and ownother = '1') and to_char(s.pjt_schd_start_date, 'YYYYMM') = '$year$month' and s.pjt_id in (select pjt_id from project where pjt_delete_flag = 'f') order by s.pjt_schd_start_time_v, s.pjt_schd_dur_v, s.pjt_schd_id";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    $ret = array();
    while ($row = pg_fetch_array($sel)) {
        $ret[$row["date"]][] = array(
            "object_type" => "project",
            "timeless" => false,
            "schedule_id" => $row["pjt_schd_id"],
            "title" => $row["pjt_schd_title"],
            "place" => $row["place_name"],
            "place_detail" => $row["pjt_schd_plc"],
            "start_time" => substr($row["pjt_schd_start_time_v"], 0, 2).":".substr($row["pjt_schd_start_time_v"], 2, 2),
            "end_time" => substr($row["pjt_schd_dur_v"], 0, 2).":".substr($row["pjt_schd_dur_v"], 2, 2),
            "schedule_type" => $row["type_name"],
            "detail" => $row["pjt_schd_detail"],
            "project_id" => $row["pjt_id"],
            "project_name" => $row["pjt_name_abbrev"],
            "schd_place_id" => $row["pjt_schd_place_id"]
        );
    }
    return $ret;
}

// 時間指定のある行事を配列で取得
function get_normal_event_array($con, $emp_id, $year, $month, $fname) {
    $sql = "select s.event_id, s.event_name, f.name as fcl_name, s.event_time, s.event_dur, s.event_content, s.fcl_detail, s.contact, to_char(s.event_date, 'YYYYMMDD') as date from timegd s left join timegdfcl f on f.fcl_id = s.fcl_id";
    $cond = "where exists (select * from timecheck where emp_id = '$emp_id' and ownother = '1' and timecheck = true) and to_char(s.event_date, 'YYYYMM') = '$year$month' and s.time_flg = true order by s.event_id";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    $ret = array();
    while ($row = pg_fetch_array($sel)) {
        $ret[$row["date"]][] = array(
            "object_type" => "timeguide",
            "timeless" => false,
            "schedule_id" => $row["event_id"],
            "title" => $row["event_name"],
            "facility" => $row["fcl_name"],
            "fcl_detail" => $row["fcl_detail"],
            "start_time" => substr($row["event_time"], 0, 5),
            "end_time" => substr($row["event_dur"], 0, 5),
            "detail" => $row["event_content"],
            "contact" => $row["contact"]
        );
    }
    return $ret;
}

// スケジュール配列を日付単位にマージ
function merge_schedules($year, $month, $timeless_schedules, $timeless_project_schedules, $timeless_events, $normal_schedules, $normal_project_schedules, $normal_events) {
    $all_schedules = array($timeless_schedules, $timeless_project_schedules, $timeless_events, $normal_schedules, $normal_project_schedules, $normal_events);
    $result = array();
    for ($day = 1; $day <= 31; $day++) {
        if (!checkdate($month, $day, $year)) break;
        $date = sprintf("%04d%02d%02d", $year, $month, $day);
        $schedules_of_the_day = array();
        foreach ($all_schedules as $schedules) {
            if (isset($schedules[$date])) {
                $schedules_of_the_day = array_merge($schedules_of_the_day, $schedules[$date]);
            }
        }
        usort($schedules_of_the_day, "sort_scehdules");
        $result[$date] = $schedules_of_the_day;
    }
    return $result;
}

// スケジュールを表示順に並べ替え
function sort_scehdules($schedule1, $schedule2) {
    if ($schedule1["start_time"] != $schedule2["start_time"]) {
        return strcmp($schedule1["start_time"], $schedule2["start_time"]);
    }

    if ($schedule1["object_type"] != $schedule2["object_type"]) {
        return strcmp($schedule1["object_type"], $schedule2["object_type"]);
    }

    if ($schedule1["end_time"] != $schedule2["end_time"]) {
        return strcmp($schedule1["end_time"], $schedule2["end_time"]);
    }

    return strcmp($schedule1["schedule_id"], $schedule2["schedule_id"]);
}

// 1週間分の日付をタイムスタンプで配列に格納
function get_date_array($date, $start_wd) {
    $diff = date("w", $date) - $start_wd;
    if ($diff < 0) {
        $diff += 7;
    }
    $start_date = strtotime("-$diff days", $date);

    $arr_result = array();
    for ($i = 0; $i < 7; $i++) {
        array_push($arr_result, strtotime("+$i days", $start_date));
    }
    return $arr_result;
}
