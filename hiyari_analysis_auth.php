<?php
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");
require_once("about_postgres.php");
require_once("hiyari_common.ini");
require_once("show_class_name.ini");
require_once("hiyari_db_class.php");

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//====================================
// 権限チェック
//====================================
$auth_chk = check_authority($session, 48, $fname);
if ($auth_chk == '0') {
    showLoginPage();
    exit;
}

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// DBクラスのインスタンス化
$db = new hiyari_db_class($con, $fname);






//====================================
//更新処理
//====================================
if($mode == "update") {

    // トランザクションの開始
    pg_query($con, "begin transaction");

    // 出来事分析機能の利用
    update_inci_analysis_available($db, $analysis_available);


    $auth_class = "";
    $arr_auths = split(",", $inci_auths);
    foreach($arr_auths as $auth)
    {
        //当設定画面表示形式→DB形式(旧画面仕様)に変換
        $setting_ref_name = "setting_ref_".$auth;
        $setting_update_name = "setting_update_".$auth;



        //$report_db_use_flg = "t";
        //$report_db_read_div = $$setting_ref_name;
        //$report_db_read_auth_flg = "f";
        if($$setting_ref_name == "f")
        {
            $analysis_read_flg = "f";
            //$report_db_read_div = "";
        }
        elseif($$setting_ref_name == "t")
        {
            $analysis_read_flg = "t";
        }

        // 詳細なアクセスコントロール　20141022
        $access_name = "access_" . $auth;
        $analysis_access_flg = ($$access_name == $auth)? "t" : "f";
        
        //$report_db_update_flg = "t";
        //$report_db_update_div = $$setting_update_name;
        if($$setting_update_name == "f")
        {
            $analysis_update_flg = "f";
            //$report_db_update_div = "";
        } else if($$setting_update_name == "t") {
            $analysis_update_flg = "t";
        }
		
        
        /*if($auth == "GU")
        {
            $report_db_update_flg = "f";
            $report_db_update_div = "";
        }

        if($report_db_read_auth_flg == "t" || $report_db_update_div == "1")
        {
            $sql = "select max(target_class) as target_class from inci_auth_emp_post where auth = '$auth'";
            $sel = select_from_table($con, $sql, "", $fname);
            if($sel == 0){
                pg_close($con);
                echo("<script type='text/javascript' src='./js/showpage.js'></script>");
                echo("<script language='javascript'>showErrorPage(window);</script>");
                exit;
            }
            $target_class = pg_fetch_result($sel,0,"target_class");

            if($target_class == "")
            {
                //ダミー設定。このケースの場合、担当範囲=システム全体であるため効果はない。
                $target_class = "1";
            }

            if($report_db_read_auth_flg == "t")
            {
                //if($target_class == 4) $target_class = 3;
                $report_db_read_div = $target_class;
            }
            if($report_db_update_div == "1")
            {
                $report_db_update_div = $target_class;
            }
        }*/

        //更新
        update_inci_analysis_auth($db, $auth, $analysis_read_flg, $analysis_update_flg, $analysis_access_flg);
    }

    // トランザクションをコミット
    pg_query($con, "commit");
} else {
    // 初回アクセスはDBからデータを取得
    $analysis_auth = get_inci_analysis_auth($db);

    foreach($analysis_auth as $value) {
        $setting_ref_name    = "setting_ref_".$value['analysis_auth'];
        $setting_update_name = "setting_update_".$value['analysis_auth'];

        $$setting_ref_name    = $value['analysis_read_flg'];
        $$setting_update_name = $value['analysis_update_flg'];
    }

    // 出来事分析機能を利用するか否か
    $analysis_available = get_inci_analysis_available($db);
}









//==============================
//インシデント権限取得
//==============================
$arr_inci = get_inci_auth_case($db);
$inci_auths = "";
for($i=0; $i<count($arr_inci); $i++)
{
    if($arr_inci[$i]["auth"] != 'GU' && !is_usable_auth($arr_inci[$i]["auth"],$fname,$con))
    {
        continue;
    }

    if($inci_auths != "") {
        $inci_auths .= ",";
    }
    $inci_auths .= $arr_inci[$i]["auth"];
}




?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?php echo($INCIDENT_TITLE); ?> | 出来事分析権限設定</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">

    function init_page()
    {
    }

    function update()
    {
        document.mainform.mode.value="update";
        document.mainform.submit();
    }

    function ref_update_changed(action_obj,auth)
    {
    }

    function ref_read_changed(obj, auth)
    {
        var access_item = document.getElementById("access_" + auth);
		if (obj.value == "t"){
			access_item.disabled = false;
		}else{
			access_item.checked = false;
			access_item.disabled = true;
		}
    }

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#35B341 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="init_page();">
<form name="mainform" action="hiyari_analysis_auth.php" method="post">
<input type="hidden" name="session" value="<?php echo($session); ?>">
<input type="hidden" name="mode" value="">
<input type="hidden" name="inci_auths" value="<?php echo($inci_auths); ?>">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<!-- ヘッダー -->
<?
show_hiyari_header($session,$fname,true);
?>
<!-- ヘッダー -->

<img src="img/spacer.gif" width="1" height="5" alt=""><br>

<table width="800" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
<tr>
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td valign="top" align="left" bgcolor="#F5FFE5">




<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td><img src="img/spacer.gif" width="10" height="5" alt=""></td>
</tr>
</table>



<!--// 出来事分析機能の利用 START -->
<table border="0" cellspacing="1" cellpadding="0" class="list">

<tr>
<td width="200" bgcolor="#DFFFDC" align="center" rowspan="2" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出来事分析機能</font>
</td>
<td width="160" bgcolor="#FFFFFF" align="center" colspan="3" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
        <input type="radio" name="analysis_available" value="f"<?if($analysis_available=='f') echo ' checked'?>>利用しない
    </font>
</td>
<td width="160" bgcolor="#FFFFFF" align="center" colspan="3" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
        <input type="radio" name="analysis_available" value="t"<?if($analysis_available=='t') echo ' checked'?>>利用する
    </font>
</td>
</tr>
</table>




<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td><img src="img/spacer.gif" width="10" height="5" alt=""></td>
</tr>
</table>

<!--// 出来事分析機能の利用 END -->

<!-- 参照 START -->
<table border="0" cellspacing="1" cellpadding="0" class="list">

<tr>
<td width="200" bgcolor="#DFFFDC" align="center" rowspan="2" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">&nbsp;</font>
</td>
<td width="160" bgcolor="#DFFFDC" align="center" colspan="3" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">参照</font>
</td>
</tr>


<tr>
<td width="80" bgcolor="#DFFFDC" align="center" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">可</font>
</td>
<td width="80" bgcolor="#DFFFDC" align="center" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">不可</font>
</td>
<td width="160" bgcolor="#DFFFDC" align="center" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">詳細なアクセスコントロール</font>
</td>
</tr>






<?
for($i=0; $i<count($arr_inci); $i++)
{
    $setting_ref_name = "setting_ref_".$arr_inci[$i]["auth"];
?>



<tr>
<td width="200" bgcolor="#DFFFDC" align="center" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo($arr_inci[$i]["auth_name"]); ?></font>
</td>
<td width="80" bgcolor="#FFFFFF" align="center" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="radio" id="<?php echo($setting_ref_name); ?>_t" name="<?php echo($setting_ref_name); ?>" value="t" <?   if($$setting_ref_name == "t"){?>checked<?}   ?>  onclick="ref_read_changed(this,'<?php echo($arr_inci[$i]["auth"]); ?>')"></font>
</td>
<td width="80" bgcolor="#FFFFFF" align="center" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="radio" id="<?php echo($setting_ref_name); ?>_f" name="<?php echo($setting_ref_name); ?>" value="f" <?   if($$setting_ref_name == "f"){?>checked<?}   ?>  onclick="ref_read_changed(this,'<?php echo($arr_inci[$i]["auth"]); ?>')"></font>
</td>
<td width="80" bgcolor="#FFFFFF" align="center" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" id="access_<?php echo($arr_inci[$i]["auth"]);?>" name="access_<?php echo($arr_inci[$i]["auth"]);?>" value="<?php echo($arr_inci[$i]["auth"]); ?>" "<?php if($$setting_ref_name == 'f'){echo(' disabled ');} ?>" <?php if($arr_inci[$i]["access"] == 't'){echo('checked');} ?>></font>
</td>
</tr>
<?php
}
?>
<tr>
	<td colspan="4">
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#FF0000">
			※ 詳細なアクセスコントロール機能を有効にすると、分析メンバーであるまたは<br>分析事案の報告書が参照可能である場合のみ分析事案が参照できます。
		</font>
	</td>
</tr>
</table>
<!-- 参照 END -->




<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td><img src="img/spacer.gif" width="10" height="5" alt=""></td>
</tr>
</table>




<!-- 更新 START -->
<table border="0" cellspacing="1" cellpadding="0" class="list">

<tr>
<td width="200" bgcolor="#DFFFDC" align="center" rowspan="2" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">&nbsp;</font>
</td>
<td width="160" bgcolor="#DFFFDC" align="center" colspan="3" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">新規作成</font>
</td>
</tr>


<tr>
<td width="80" bgcolor="#DFFFDC" align="center" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">可</font>
</td>
<td width="80" bgcolor="#DFFFDC" align="center" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">不可</font>
</td>
</tr>


<?php
for($i=0; $i<count($arr_inci); $i++)
{
    $setting_update_name = "setting_update_".$arr_inci[$i]["auth"];

?>
<tr>
<td width="200" bgcolor="#DFFFDC" align="center" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo($arr_inci[$i]["auth_name"]); ?></font>
</td>
<td width="80" bgcolor="#FFFFFF" align="center" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="radio" id="setting_update_<?php echo($arr_inci[$i]["auth"]); ?>_t" name="setting_update_<?php echo($arr_inci[$i]["auth"]); ?>" value="t" <?   if($$setting_update_name == "t"){?>checked<?}   ?> onclick="ref_update_changed(this,'<?php echo($arr_inci[$i]["auth"]); ?>')"></font>
</td>
<td width="80" bgcolor="#FFFFFF" align="center" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="radio" id="setting_update_<?php echo($arr_inci[$i]["auth"]); ?>_f" name="setting_update_<?php echo($arr_inci[$i]["auth"]); ?>" value="f" <?   if($$setting_update_name == "f"){?>checked<?}   ?> onclick="ref_update_changed(this,'<?php echo($arr_inci[$i]["auth"]); ?>')"></font>
</td>
</tr>
<?php
}
?>

</table>
<!-- 更新 END -->


<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td><img src="img/spacer.gif" width="10" height="5" alt=""></td>
</tr>
</table>



<table width="100%" border="0" cellspacing="1" cellpadding="0">
<tr>
<td align="right">
<input type="button" value="登録" onclick="update();">
</td>
</tr>
</table>

</td>
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>


<!-- フッター START -->
<?php
show_hiyari_footer($session,$fname,true);
?>
<!-- フッター END -->

</td>
</tr>
</table>

</form>
</body>
</html>



<?php
function get_inci_analysis_auth($db) {
    $sql = "SELECT * FROM inci_analysis_auth";
    $db->query($sql);
    return $db->getAll();
}

function get_inci_analysis_available($db) {
    $sql = "SELECT * FROM inci_analysis_available";
    $db->query($sql);
    return $db->getOne();
}

function update_inci_analysis_available($db, $analysis_available) {
    $sql = "UPDATE inci_analysis_available SET ";
    $set = array("analysis_available");
    $dat = array("$analysis_available");

    $db->update($sql, $set, $dat, '');
}

function get_inci_auth_case($db)
{

    $sql  = "select a.*, b.auth_rank, case when a.auth = 'GU' then '一般ユーザー' else b.auth_name end, c.analysis_access_flg as access ";
    $sql .= "from inci_auth_case a left join inci_auth_mst b on a.auth = b.auth ";
    $sql .= "left join inci_analysis_auth c on a.auth = c.analysis_auth ";
    $cond = "order by auth_rank is not null, auth_rank asc";
    $db->query($sql.$cond);

    $array = $db->getAll();
    return $array;
}


function update_inci_analysis_auth($db, $auth, $analysis_read_flg, $analysis_update_flg, $analysis_access_flg)
{

    $sql = "UPDATE inci_analysis_auth SET ";
    $set = array("analysis_read_flg","analysis_update_flg", "analysis_access_flg");
    $dat = array("$analysis_read_flg", "$analysis_update_flg", "$analysis_access_flg");
    $cond = "where analysis_auth = '$auth'";

    $db->update($sql, $set, $dat, $cond);
}


//==============================
//DBコネクション終了
//==============================
pg_close($con);
?>
