<?
require("about_authority.php");
require("about_session.php");
require("about_postgres.php");
require("summary_tmpl_util.ini");
require_once("summary_common.ini");
//===============================================
// メドレポート、標準チーム計画サブウィンドウ
//===============================================

// 当初の仕様では、共用、看護、介護、リハビリの4職種についての項目設定でしたが
// 当面は職種別分けをしないことになりました
// 将来的にこれらを復活させる場合コメントを外し、「共用」や「看護」の文字列を復活させてください
// 2012.8.1

//==============================
//ページ名
//==============================
$fname = $PHP_SELF;

//==============================
//セッションのチェック
//==============================
$session = qualify_session($session,$fname);
if($session == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
// 権限チェック
//==============================
$summary = check_authority($session, 57, $fname);
if ($summary == "0")
{
	showLoginPage();
	exit;
}

//==============================
//DBコネクション
//==============================
$con = connect2db($fname);
if($con == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
// DBテーブルロック制御
// ロックは 't'で設定 'f'で解除
//==============================
$jtype=$type;
if ( $jtype == 10 ){ $jtype = 4; }// ヘッダ部のテーブルとロックカラムは患者の問題アウトカムと同じ
switch($jtype){
 case 0:
 case 1:
 case 2:
 case 3:
 case 4:
 $tbl_name=array("sum_med_priactice0","sum_med_priactice1","sum_med_priactice2","sum_med_priactice3","sum_med_std_team");
 $col_nm_lockflg=array("c0_lock_flag","c1_lock_flag","c2_lock_flag","c3_lock_flag","left_lock_flag");
 $col_nm_lockemp=array("c0_lock_emp_id","c1_lock_emp_id","c2_lock_emp_id","c3_lock_emp_id","lef_lock_emp_id");
 $col_nm_locktim=array("c0_lock_time","c1_lock_time","c2_lock_time","c3_lock_time","lef_lock_time");
 $fetch_all_array = select_all($con,$fname,$tbl_name[$jtype],$team_id,$col_nm_lockemp[$jtype]);
 $lock_flag = $fetch_all_array[0][$col_nm_lockflg[$jtype]];
 $lock_time = $fetch_all_array[0][$col_nm_locktim[$jtype]];
 $lock_name = $fetch_all_array[0]["name"];
 if ( $lock_flag != "t" ){
	$sql  = "UPDATE ".$tbl_name[$jtype]." SET ";
	$cond = "WHERE team_id = $team_id";
	$set = array($col_nm_lockflg[$jtype],$col_nm_lockemp[$jtype]);
	$setvalue =  array("t",$emp_id);
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		summary_common_show_error_page_and_die();
	}
 }
 break;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<HTML XMLNS="http://www.w3.org/1999/xhtml">
<HEAD>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=EUC-JP">
<LINK REL="STYLEsheet" TYPE="text/css" HREF="css/main_summary.css"/>
<style type="text/css">
textarea {
  width: 280px;
  border-color:#9bc8ec;
  border-style: solid;
  border-width: 1px;
  resize: none;
  overflow: hidden;
}
</style>

<TITLE>CoMedix メドレポート｜標準チーム計画、テキスト入力</TITLE>
<script type="text/javascript" src="js/adjustTextAreaHeight.js"></script>
<SCRIPT TYPE="text/javascript">
var atype=<? echo $type;?>;

// 入力した内容で親画面を書き替え、DB更新を呼び出してウィンドウを閉じる
function editUpdate(){
	switch (atype){
	case 10:// 上部・責任者氏名
		parent.opener.document.standardform.creater_job.value=document.subwinform.job.value;
		parent.opener.document.standardform.creater_name.value=document.subwinform.name.value;
		break;
	case 4: // 左側
		parent.opener.document.getElementById("leftdefval").innerHTML=replaceToBR(document.subwinform.area1.value);
		parent.opener.document.getElementById("leftS").innerHTML=replaceToBR(document.subwinform.area2.value);
		parent.opener.document.getElementById("leftE").innerHTML=replaceToBR(document.subwinform.area3.value);
		parent.opener.document.getElementById("leftRisk").innerHTML=replaceToBR(document.subwinform.area4.value);
		parent.opener.document.getElementById("leftO").innerHTML=replaceToBR(document.subwinform.area5.value);
		break;
	case 0: // 中央共用
	case 1: // 中央看護
	case 2: // 中央介護
	case 3: // 中央リハビリ
		parent.opener.document.standardform.OP.value=replaceToBR(document.subwinform.area2.value);
		parent.opener.document.standardform.TP.value=replaceToBR(document.subwinform.area3.value);
		parent.opener.document.standardform.EP.value=replaceToBR(document.subwinform.area4.value);
		break;
	case 5: // 右看護
	case 6: // 右介護
	case 7: // 右リハビリ
		var type = atype - 4;
		parent.opener.document.standardform.rightvalu.value=document.subwinform.area1.value;
		parent.opener.document.standardform.updatemode.value="insert";
		parent.opener.document.standardform.rightdate.value=parent.opener.now(2);
		break;
	default:;
	}
	parent.opener.submitUpd(atype); // DB更新する
	window.close();
}
function editCancel(){
	parent.opener.unLock(atype); // ロック外すため再起動
	window.close();
}

// 改行コードを<BR>へ変換する
function replaceToBR( val ){
 if ( val != "" ){
	val = val.replace(/\n/g,'<BR>');
 }
 return val;
}

// <BR>を改行コードへ変換する
// FFとCHROMEでは大文字の<BR>は自動的に小文字の<br>へ強制変換させられている
function replaceToCRLF( val ){
 var CRLF = document.getElementById("return_code").value; // ブラウザ別の改行コードを得る
 if ( val != "" ){
	val = val.replace(/<BR>/g,CRLF);
	val = val.replace(/<br>/g,CRLF);
 }
 return val;
}

// テキスト入力領域設置
function leftDefValue( value ){return 'コメント<BR><TEXTAREA ID="area1" style="ime-mode:active" onkeyup="adjustTextAreaHeight(this.id, 2);" onchange="adjustTextAreaHeight(this.id, 2);">' + replaceToCRLF(value) + '</TEXTAREA>';} // 左側のコメント専用
function TextA( title,value ){return title + '<BR><TEXTAREA ID="area2" style="ime-mode:active" onkeyup="adjustTextAreaHeight(this.id, 2);" onchange="adjustTextAreaHeight(this.id, 2);">' + replaceToCRLF(value) + '</TEXTAREA>';} // テキスト1
function TextB( title,value ){return title + '<BR><TEXTAREA ID="area3" style="ime-mode:active" onkeyup="adjustTextAreaHeight(this.id, 2);" onchange="adjustTextAreaHeight(this.id, 2);">' + replaceToCRLF(value) + '</TEXTAREA>';} // テキスト2
function TextC( title,value ){return title + '<BR><TEXTAREA ID="area4" style="ime-mode:active" onkeyup="adjustTextAreaHeight(this.id, 2);" onchange="adjustTextAreaHeight(this.id, 2);">' + replaceToCRLF(value) + '</TEXTAREA>';} // テキスト3
function TextD( title,value ){return title + '<BR><TEXTAREA ID="area5" style="ime-mode:active" onkeyup="adjustTextAreaHeight(this.id, 2);" onchange="adjustTextAreaHeight(this.id, 2);">' + replaceToCRLF(value) + '</TEXTAREA>';} // テキスト4
function shortInput( title ){return '<TEXTAREA COLS="60" ROWS="1" ID="area1" style="ime-mode:active"></TEXTAREA>';} // 右側の評価入力用
function headInput(){return '職種<INPUT TYPE="TEXT" SIZE="8" MAXLENGTH="20" ID="job" style="ime-mode:active">&nbsp氏名<INPUT TYPE="TEXT" SIZE="20" MAXLENGTH="40" ID="name" style="ime-mode:active">';} // 職種、氏名

// 今日の日付
function toDay(){
	var now=new Date();
	var yy=now.getFullYear();
	var mo=now.getMonth()+1;
	var dd=now.getDate();
	return yy+"年"+mo+"月"+dd+"日";
}

function main(){
	// 親ウィンドウ画面から必要なデータを取る
	var exswitch = "";
	switch (atype){ // atypeはグローバルデータ
	case 10:// 上側、責任者職種、氏名
		document.getElementById("title").innerHTML= "責任者";
		document.getElementById("tarea0").innerHTML=headInput();
		document.getElementById("job").value=parent.opener.document.standardform.creater_job.value;
		document.getElementById("name").value=parent.opener.document.standardform.creater_name.value;
		exswitch = "<? echo $lock_flag; ?>";
		break;
	case 4: // 左側
		document.getElementById("tarea0").innerHTML= leftDefValue( parent.opener.document.getElementById("leftdefval").innerHTML );
		document.getElementById("tarea1").innerHTML= TextA( "S:症状・徴候",parent.opener.document.getElementById("leftS").innerHTML );
		document.getElementById("tarea2").innerHTML= TextB( "E:原因・関連因子",parent.opener.document.getElementById("leftE").innerHTML );
		document.getElementById("tarea3").innerHTML= TextC( "危険因子",parent.opener.document.getElementById("leftRisk").innerHTML );
		document.getElementById("tarea4").innerHTML= TextD( "アウトカム",parent.opener.document.getElementById("leftO").innerHTML );
		document.getElementById("title").innerHTML= "患者の問題及びアウトカム";
		exswitch = "<? echo $lock_flag; ?>";
		break;
	case 0: // 中央共用
		document.getElementById("tarea1").innerHTML= TextA( "O-P:観察計画",parent.opener.document.getElementById("op0").innerHTML );
		document.getElementById("tarea2").innerHTML= TextB( "T-P:実践計画",parent.opener.document.getElementById("tp0").innerHTML );
		document.getElementById("tarea3").innerHTML= TextC( "E-P:教育・指導計画",parent.opener.document.getElementById("ep0").innerHTML );
		document.getElementById("title").innerHTML= " チーム実践";
//		document.getElementById("title").innerHTML= " チーム実践《共用》";
		exswitch = "<? echo $lock_flag; ?>";
		break;
<?
/* 画面ロードしないようphpコメント
	case 1: // 中央看護
		document.getElementById("tarea1").innerHTML= TextA( "O-P:観察計画",parent.opener.document.getElementById("op1").innerHTML );
		document.getElementById("tarea2").innerHTML= TextB( "T-P:実践計画",parent.opener.document.getElementById("tp1").innerHTML );
		document.getElementById("tarea3").innerHTML= TextC( "E-P:教育・指導計画",parent.opener.document.getElementById("ep1").innerHTML );
		document.getElementById("title").innerHTML= " チーム実践《看護》";
		exswitch = "<? echo $lock_flag; ?>";
		break;
	case 2: // 中央介護
		document.getElementById("tarea1").innerHTML= TextA( "O-P:観察計画",parent.opener.document.getElementById("op2").innerHTML );
		document.getElementById("tarea2").innerHTML= TextB( "T-P:実践計画",parent.opener.document.getElementById("tp2").innerHTML );
		document.getElementById("tarea3").innerHTML= TextC( "E-P:教育・指導計画",parent.opener.document.getElementById("ep2").innerHTML );
		document.getElementById("title").innerHTML= " チーム実践《介護》";
		exswitch = "<? echo $lock_flag; ?>";
		break;
	case 3: // 中央リハビリ
		document.getElementById("tarea1").innerHTML= TextA( "O-P:観察計画",parent.opener.document.getElementById("op3").innerHTML );
		document.getElementById("tarea2").innerHTML= TextB( "T-P:実践計画",parent.opener.document.getElementById("tp3").innerHTML );
		document.getElementById("tarea3").innerHTML= TextC( "E-P:教育・指導計画",parent.opener.document.getElementById("ep3").innerHTML );
		document.getElementById("title").innerHTML= " チーム実践《リハビリ》";
		exswitch = "<? echo $lock_flag; ?>";
		break;
*/
?>
	case 5: // 右・看護
		var emp_name = parent.opener.getEmpName();
		document.getElementById("tarea1").innerHTML= emp_name+" "+toDay();
		document.getElementById("tarea2").innerHTML= shortInput();
		document.getElementById("title").innerHTML= " 評価入力";
//		document.getElementById("title").innerHTML= " 評価入力《看護》";
		break;
<?
/* 画面ロードしないようphpコメント
	case 6: // 右・介護
		var emp_name = parent.opener.getEmpName();
		document.getElementById("tarea1").innerHTML= emp_name+" "+toDay();
		document.getElementById("tarea2").innerHTML= shortInput();
		document.getElementById("title").innerHTML= " 評価入力《介護》";
		break;
	case 7: // 右・リハビリ
		var emp_name = parent.opener.getEmpName();
		document.getElementById("tarea1").innerHTML= emp_name+" "+toDay();
		document.getElementById("tarea2").innerHTML= shortInput();
		document.getElementById("title").innerHTML= " 評価入力《リハビリ》";
		break;
*/
?>
	}
	if ( exswitch == "t" ){
		var msg = "<? echo $lock_name; ?>"+"さんが修正中です。しばらくしてからやり直してください。\n\n強制的に開いて更新しますか?\n\n（内容を更新する場合は、修正中の職員に連絡してください）";
		if(  !confirm ( msg ) ){
			window.close();
		}
	}
	if ( atype < 5 ){
		var textAreaIdArray = new Array("area1","area2","area3","area4");
		var tlength = textAreaIdArray.length;
		for (var i = 0; i < tlength; i++) {
			adjustTextAreaHeight(textAreaIdArray[i], 2, 1);
		}
	}
}

</SCRIPT>
</HEAD>

<BODY ONLOAD="javascript:main();">


<DIV STYLE="padding:4px">

<!-- ヘッダエリア -->

<table class="dialog_titletable"><tr>
	<th>チーム計画・データ入力</th>
	<td><a href="javascript:editCancel()"><img src="img/icon/close.gif" alt="閉じる"
					onmouseover="swapimg(this,'img/icon/closeo.gif');" onmouseout="swapimg(this,'img/icon/close.gif');" /></a></td>
</tr></table>

<img src="img/spacer.gif" width="1" height="4" alt="">
<TABLE CLASS="list_table" ><TR><TH><B><DIV ID="title"></DIV></B></TH></TR></TABLE>
<TABLE WIDTH="100%" CLASS="list_table" STYLE="margin-top:4px"><TR><TD>
 <FORM NAME="subwinform">
 <TABLE>
 <TR><TD ID="tarea0"></TD></TR>
 <TR><TD ID="tarea1"></TD></TR>
 <TR><TD ID="tarea2"></TD></TR>
 <TR><TD ID="tarea3"></TD></TR>
 <TR><TD ID="tarea4"></TD></TR>
 <TR><TD ALIGN="RIGHT">
 <INPUT TYPE="BUTTON" VALUE="登録" ONCLICK="javascript:editUpdate();">
 <INPUT TYPE="BUTTON" VALUE="中止" ONCLICK="javascript:editCancel();">
 </TD></TR>
</TABLE>
</FORM>

</TD>
</TR>
</TABLE>

</DIV>

<!-- textarea 制御用 -->
<textarea id="textarea_dummy" rows="2" onkeyup="tx();" onchange="tx();" style="overflow:hidden; position:absolute; left:0; top:-3000px; display:none;">
</textarea>
<!-- ブラウザ毎の改行コード違い用 -->
<textarea id="return_code" rows="2" style="overflow:hidden; position:absolute; left:0; top:-1000px; display:none;">

</textarea>

<SPAN ID="debug"></SPAN><? echo $DEBUG; ?>
</BODY>
</HTML>
<? // PHP function
function select_all($con,$fname,$tbl_name,$team_id,$emp_id){
	$sql= "SELECT M.*,E.emp_lt_nm||E.emp_ft_nm AS name FROM ".$tbl_name." AS M ";
	$sql .= " LEFT OUTER JOIN empmst AS E ON E.emp_id=$emp_id ";
	$cond = "WHERE team_id=$team_id ";
	$sel = select_from_table($con,$sql,$cond,$fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		require_once("summary_common.ini");
		summary_common_show_error_page_and_die();
	}
	return  pg_fetch_all($sel);
}

// 問題点
// textarea入力で大量の文字列を入力するとサブウィンドウの上部に不正な枠が表示されることがあります
// その際はtextarea制御用のダミーである textarea_dummy の style="... top:-3000px..." の値を増やしてください
// 発現する文字数はブラウザ毎で異なります。これはjs/adjustTextAreaHeight.jsの問題です。
pg_close($con);
?>
