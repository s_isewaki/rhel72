<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 設備予約 | 週（チャート）</title>
<?
$fname = $PHP_SELF;
//ini_set("display_errors","1");
require_once("Cmx.php");
require_once("aclg_set.php");
require_once("about_comedix.php");
require("show_facility.ini");
require("facility_common.ini");
require("facility_week_chart_common.ini");
require("holiday.php");
require("label_by_profile_type.ini");
require("show_calendar_memo.ini");

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 11, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 施設・設備登録権限を取得
$fcl_admin_auth = check_authority($session, 41, $fname);

// 表示範囲が渡されていない場合は「全て」に設定
if ($range == "") {
	$range = "all";
}

// タイムスタンプが渡されていない場合は当日日付のタイムスタンプを取得
$this_week = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
if ($date == "") {
	$date = $this_week;
}

// 日付の設定
$day = date("d", $date);
$month = date("m", $date);
$year = date("Y", $date);

// データベースに接続
$con = connect2db($fname);

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// アクセスログ
$optsql = "select facility_default from option";
$optcond = "where emp_id = '{$emp_id}'";
$optsel = select_from_table($con, $optsql, $optcond, $fname);
if ($optsel == 0) {
    pg_close($con);
    js_error_exit();
}
$facility_default = pg_fetch_result($optsel, 0, "facility_default");

if ($facility_default === '4') {
	aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);
}

// 参照／予約／管理可能なカテゴリ・設備一覧を配列で取得
$categories = get_referable_categories($con, $emp_id, $fname);

// カテゴリ未選択の場合、最初のカテゴリが選択されたものとする
if ($cate_id == "") {
	if ($range != "all") {
		list($cate_id, $fcl_id) = split("-", $range);
	}
	if ($cate_id == "") {
		$cate_id = (count($categories) > 0) ? strval(key($categories)) : "";
	}
} else if (!isset($categories[$cate_id])) {
	$cate_id = (count($categories) > 0) ? strval(key($categories)) : "";
}

// 設備未選択の場合、最初の設備が選択されたものとする
if ($cate_id != "" && $fcl_id == "") {
	$facilities = $categories[$cate_id]["facilities"];
	$fcl_id = (count($facilities) > 0) ? strval(key($facilities)) : "";
}

// 印刷用の表示範囲に選択内容を反映
$print_range = "{$cate_id}-{$fcl_id}";

// 選択されたカテゴリの患者情報連携フラグを取得
if ($cate_id != "") {
	$pt_flg = ($categories[$cate_id]["pt_flg"] == "t");
} else {
	$pt_flg = false;
}

// 選択された設備の管理者権限を取得
if ($fcl_id != "") {
	$fcl_admin_flg = $categories[$cate_id]["facilities"][$fcl_id]["admin_flg"];
} else {
	$fcl_admin_flg = false;
}

// 選択されたカテゴリの週表示に予定内容を表示するを取得
if ($cate_id != "") {
	$show_content = $categories[$cate_id]["show_content"];
} else {
	$show_content = "f";
}

// 選択された設備の種別表示フラグを取得
if ($fcl_id != "") {
	$show_type_flg = $categories[$cate_id]["facilities"][$fcl_id]["show_type_flg"];
} else {
	$show_type_flg = "t";
}
$show_type = ($show_type_flg == 't') ? "true" : "false";

// 他画面の表示範囲に選択内容を反映
/*
if ($range = "all") {
	if ($cate_id != "") {
		$range = $cate_id;
	}
	if ($fcl_id != "") {
		$range .= "-{$fcl_id}";
	}
}
*/

// オプション情報を取得
$default_info = get_default_info($con, $session, $fname);
$default_url = $default_info["url"];
$calendar_start = $default_info["calendar_start"];
$default_fclcate_id = $default_info["default_cate_id"];

// 表示対象週のスタート日のタイムスタンプを取得
$start_date = get_start_date_timestamp($con, $date, $emp_id, $fname);

// 表示対象週の7日分の日付を配列で取得
$dates = get_dates_in_week($start_date);

// 時間枠の設定
$base_min = 30;
if ($cate_id != "") {
	$base_min = $categories[$cate_id]["base_min"];
}

// 時刻指定なしの予約情報を配列で取得
$timeless_reservations = get_timeless_reservations($con, $cate_id, $fcl_id, $dates, $base_min, $fcl_admin_flg, $emp_id, $pt_flg, $fname);

// 予約情報を配列で取得
$reservations = get_reservations($con, $cate_id, $fcl_id, $dates, $base_min, $fcl_admin_flg, $emp_id, $pt_flg, $fname);

// スタート曜日の設定
$start_wd = ($calendar_start == "1") ? 0 : 1;  // 0：日曜、1：月曜

// 処理週のスタート日のタイムスタンプを求める
$start_day = get_start_day($date, $start_wd);

// 前月・前週・翌週・翌月のタイプスタンプを変数に格納
$last_month = get_last_month($date);
$last_week = strtotime("-7 days", $date);
$next_week = strtotime("+7 days", $date);
$next_month = get_next_month($date, $start_wd);

// 患者氏名/利用者氏名
$patient_name_title = $_label_by_profile["PATIENT_NAME"][$profile_type];
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/popup.js"></script>
<script type="text/javascript">
function changeDate(dt) {
	document.view.date.value = dt;
	document.view.submit();
}

function popupReservationDetail(facility, date, time, title, type, text, user_count, reg_emp, upd_emp, e, with_patient, patient, show_type, ext, tmp_class) {

	var arr_value = new Array(
			'施設・設備', facility,
			'日付', date,
			'時刻', time,
			'タイトル', title
		);

	if (show_type) {
		arr_value.push( '種別', type );
	}
	if (with_patient) {
		arr_value.push( '<?=$patient_name_title?>', patient );
	}
	arr_value.push(
			'内容', text,
			'利用人数', user_count,
			'登録者', reg_emp + '<br />' + tmp_class,
			'登録者内線番号', ext,
			'最終更新者', upd_emp
		);

	popupDetailBlue(arr_value, 400, 100, e);
}

function highlightCells(class_name) {
	changeCellsColor(class_name, '#ffff66');
}

function dehighlightCells(class_name) {
	changeCellsColor(class_name, '');
}

function changeCellsColor(class_name, color) {
	var rows = document.getElementsByTagName('tr');
	for (var i = 0, j = rows.length; i < j; i++) {
		if (rows[i].className != class_name) {
			continue;
		}
		for (var k = 0, l = rows[i].cells.length; k < l; k++) {
			if (rows[i].cells[k].className != 'highlight') {
				continue;
			}
			rows[i].cells[k].style.backgroundColor = color;
		}
	}
}

function openPrintWindow(flg) {
	if (flg == 1) {
		prg_name = 'facility_print_week_chart.php';
	} else {
		prg_name = 'facility_print_week_chart_detail.php';
	}
	window.open(prg_name+'?session=<? echo($session); ?>&range=<? echo($print_range); ?>&date=<? echo($date); ?>', 'newwin', 'width=840,height='+((screen.availHeight-60<750)? screen.availHeight-60: 750)+' ,scrollbars=yes');
}
// CSVダウンロード用
function downloadCSV(){
    document.csv.action = 'facility_csv.php';
    document.csv.submit();
}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<? echo($default_url); ?>"><img src="img/icon/b06.gif" width="32" height="32" border="0" alt="設備予約"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<? echo($default_url); ?>"><b>設備予約</b></a></font></td>
<? if ($fcl_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="facility_master_menu.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="95" align="center" bgcolor="#bdd1e7"><a href="facility_menu.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="facility_week.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">週（表）</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#5279a5"><a href="facility_week_chart.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>&cate_id=<? echo($cate_id); ?>&fcl_id=<? echo($fcl_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>週（チャート）</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="facility_month.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">月</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="facility_year.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">年</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="facility_bulk_register.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="facility_option.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション</font></a></td>
<td align="right"><input type="button" value="印刷（詳細）" onclick="openPrintWindow(2);"><input type="button" value="印刷" onclick="openPrintWindow(1);"></td>
<td width="20"><input type="button" value="CSV出力" onclick="downloadCSV();"></td>

</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<form name="view" action="facility_week_chart.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr bgcolor="#f6f9ff">
<td height="30"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示範囲
<select name="cate_id" style="vertical-align:middle;" onchange="this.form.submit();"><? show_category_options($categories, $cate_id); ?></select><img src="img/spacer.gif" width="50" height="1" alt=""><a href="facility_week_chart.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($last_month); ?>&cate_id=<? echo($cate_id); ?>&fcl_id=<? echo($fcl_id); ?>">&lt;&lt;前月</a>
<a href="facility_week_chart.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($last_week); ?>&cate_id=<? echo($cate_id); ?>&fcl_id=<? echo($fcl_id); ?>">&lt;前週</a>
<a href="facility_week_chart.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($this_week); ?>&cate_id=<? echo($cate_id); ?>&fcl_id=<? echo($fcl_id); ?>">今週</a>
<select onchange="changeDate(this.options[this.selectedIndex].value);" style="vertical-align:middle;"><? show_date_options($date, $start_day, $session, $range); ?></select>
<a href="facility_week_chart.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($next_week); ?>&cate_id=<? echo($cate_id); ?>&fcl_id=<? echo($fcl_id); ?>">翌週&gt;</a>
<a href="facility_week_chart.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($next_month); ?>&cate_id=<? echo($cate_id); ?>&fcl_id=<? echo($fcl_id); ?>">翌月&gt;&gt;</a>
</font></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="range" value="<? echo($range); ?>">
<input type="hidden" name="date" value="<? echo($date); ?>">
</form>
<? show_facilities($categories, $cate_id, $fcl_id, $range, $date, $session); ?>
<?
if ($fcl_id != "") {
	$start_date = date("Ymd", $start_day);
	$end_date = date("Ymd", strtotime("+6 days", $start_day));
	// カレンダーのメモを取得
	$arr_calendar_memo = get_calendar_memo($con, $fname, $start_date, $end_date);
	$holiday_memo_flg = false;
	foreach($arr_calendar_memo as $tmp_date => $tmp_memo) {
		if ($tmp_memo != "" && strlen($tmp_date) == 8) {
			$y = substr($tmp_date, 0, 4);
			$m = substr($tmp_date, 4, 2);
			$d = substr($tmp_date, 6, 2);
			$holiday_name = ktHolidayName(mktime(0, 0, 0, $m, $d, $y));
			if ($holiday_name != "") {
				$holiday_memo_flg = true;
				break;
			}
		}
	}
	if ($holiday_memo_flg == true) {
		$tmp_height = "58";
	} else {
		$tmp_height = "40";
	}
	echo("<div style=\"height:{$tmp_height}px; overflow: auto; border:#CCCCCC solid 1px;\">\n");
	echo("<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"2\">\n");

	// 日毎の最大重複数（列数）
	$max_col_of_days = get_max_col_of_days($reservations);

	// 日付行を表示
	show_dates($con, $dates, $range, $session, $fname, $arr_calendar_memo, $tmp_height, $reservations);

	// 時刻指定なしの予約行を表示
	show_timeless_reservations($timeless_reservations, $base_min, $cate_id, $fcl_id, $range, $pt_flg, $session, false, false, $max_col_of_days, $reservations);
	// 予約行を表示
	show_reservations($reservations, $base_min, $cate_id, $fcl_id, $range, $pt_flg, $session, false, false, $max_col_of_days);

	echo("</table>\n");
	echo("</div>\n");

	echo("<div style=\"height:520px; overflow: scroll;border:#CCCCCC solid 1px;\">\n");
	echo("<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"2\">\n");
	// 時刻指定なしの予約行を表示
	show_timeless_reservations($timeless_reservations, $base_min, $cate_id, $fcl_id, $range, $pt_flg, $session, false, false, $max_col_of_days, $reservations);
	// 予約行を表示
	show_reservations($reservations, $base_min, $cate_id, $fcl_id, $range, $pt_flg, $session, false, false, $max_col_of_days);

	echo("</table>\n");
	echo("</div>\n");

} else {
	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\n");
	echo("<tr height=\"22\">\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">表示可能な設備がありません。</font></td>\n");
	echo("</tr>\n");
	echo("</table>\n");
}
?>
</td>
</tr>
</table>
</body>
<form name="csv" method="get">
    <input type="hidden" name="session" value="<?php echo $session; ?>">
    <input type="hidden" name="start_date" value="<?php echo $start_date ?>">
    <input type="hidden" name="end_date" value="<?php echo $end_date ?>">
    <input type="hidden" name="range" value="<?php echo $range; ?>">
</form>

<? pg_close($con); ?>
</html>
<?
// 表示範囲オプションを出力（カテゴリのみ）
function show_category_options($categories, $cate_id) {
	if (count($categories) == 0) {
		echo("<option value=\"\">（未登録）\n");
		return;
	}

	foreach ($categories as $tmp_category_id => $tmp_category) {
		echo("<option value=\"$tmp_category_id\"");
		if ($tmp_category_id == $cate_id) {
			echo(" selected");
		}
		echo(">【{$tmp_category["name"]}】");
	}
}

// 日付オプションを出力
function show_date_options($date, $start_day, $session, $range) {
	for ($i = -3; $i <= -1; $i++) {
		$diff = $i * 7;
		$tmp_date = strtotime("$diff days", $date);
		$tmp_start_day = date("Y/m/d", strtotime("$diff days", $start_day));
		$diff += 6;
		$tmp_end_day = date("Y/m/d", strtotime("$diff days", $start_day));
		echo("<option value=\"$tmp_date\">{$tmp_start_day}〜{$tmp_end_day}");
	}

	$tmp_start_day = date("Y/m/d", $start_day);
	$tmp_end_day = date("Y/m/d", strtotime("+6 days", $start_day));
	echo("<option value=\"$date\" selected>【{$tmp_start_day}〜{$tmp_end_day}】");

	for ($i = 1; $i <= 3; $i++) {
		$diff = $i * 7;
		$tmp_date = strtotime("+$diff days", $date);
		$tmp_start_day = date("Y/m/d", strtotime("+$diff days", $start_day));
		$diff += 6;
		$tmp_end_day = date("Y/m/d", strtotime("+$diff days", $start_day));
		echo("<option value=\"$tmp_date\">{$tmp_start_day}〜{$tmp_end_day}");
	}
}

// 設備リストを出力
function show_facilities($categories, $cate_id, $fcl_id, $range, $date, $session) {
	if (count($categories) == 0) {
		return;
	}
	if (count($categories[$cate_id]["facilities"]) == 0) {
		return;
	}

	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"margin-bottom:1px;\">\n");
	echo("<tr bgcolor=\"#{$categories[$cate_id]["bg_color"]}\">\n");
	echo("<td>\n");
	echo("<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\n");
	echo("<tr height=\"22\">\n");

	foreach ($categories[$cate_id]["facilities"] as $tmp_fcl_id => $tmp_facility) {
		echo("<td style=\"padding-right:15px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		if ($tmp_fcl_id != $fcl_id) {
			echo("<a href=\"facility_week_chart.php?session=$session&range=$range&date=$date&cate_id=$cate_id&fcl_id=$tmp_fcl_id\">");
		} else {
			echo("<b>");
		}
		echo($tmp_facility["name"]);
		if ($tmp_fcl_id != $fcl_id) {
			echo("</a>");
		} else {
			echo("</b>");
		}
		echo("</font></td>\n");
	}

	echo("</tr>\n");
	echo("</table>\n");
	echo("</td>\n");
	echo("</tr>\n");
	echo("</table>\n");
}

?>
