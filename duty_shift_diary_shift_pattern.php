<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
  <title>CoMedix 病棟管理日誌設定</title>
  <?php
  //ini_set("display_errors", "On");
  //ini_set("error_reporting", E_ALL);

  require_once("about_comedix.php");
  require_once("duty_shift_common.ini");
  require_once("duty_shift_manage_tab_common.ini");
  require_once("duty_shift_common_class.php");
  require_once("duty_shift_duty_common_class.php");

  //勤怠パターンテーブル登録用フラグ設定(shift_pattern_id)
  define("PATTERN_ALL", "0000"); //選択リスト用全出力
  define("YAKIN_AKE", "0001"); //夜勤明
  define("NIKKIN", "0002"); //日勤
  define("HANKIN", "0003"); //半勤
  define("YAKIN_IRI", "0004"); //夜勤入
  define("KOUKYU", "0005"); //公休
  define("NENKYU", "0006"); //年休
  ///-----------------------------------------------------------------------------
  //ページ名
  ///-----------------------------------------------------------------------------
  $fname = $PHP_SELF;

  ///-----------------------------------------------------------------------------
  // セッションのチェック
  ///-----------------------------------------------------------------------------
  $session = qualify_session($session, $fname);
  if ($session == "0") {
      echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
      echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
      exit;
  }

  ///-----------------------------------------------------------------------------
  // データベースに接続
  ///-----------------------------------------------------------------------------
  $con = connect2db($fname);
  if ($con == "0") {
      echo("<script type='text/javascript' src='./js/showpage.js'></script>");
      echo("<script language='javascript'>showLoginPage(window);</script>");
      exit;
  }

  ///-----------------------------------------------------------------------------
  //利用するCLASS
  ///-----------------------------------------------------------------------------
  $obj = new duty_shift_common_class($con, $fname);
  $obj_duty = new duty_shift_duty_common_class($con, $fname);

  ///-----------------------------------------------------------------------------
  //ＤＢ(wktmgrp)より勤務パターングループ情報を取得
  ///-----------------------------------------------------------------------------
  $err_msg_1 = "";
  $data_emp = $obj->get_empmst_array("");
  $data_wktmgrp = $obj->get_wktmgrp_array();
  if (count($data_wktmgrp) <= 0) {
      $err_msg_1 = "勤務パターングループ情報(wktmgrp)が未設定です。管理者に連絡してください。";
  } else {
      ///-----------------------------------------------------------------------------
      // 権限のチェック
      ///-----------------------------------------------------------------------------
      //管理画面用
      $chk_flg = $obj->check_authority_Management($session, $fname);
      if ($chk_flg == "") {
          pg_close($con);
          echo("<script type='text/javascript' src='./js/showpage.js'></script>");
          echo("<script language='javascript'>showLoginPage(window);</script>");
          exit;
      }
      //ユーザ画面用
      $section_admin_auth = $obj->check_authority_user($session, $fname);
      ///-----------------------------------------------------------------------------
      //初期処理
      ///-----------------------------------------------------------------------------
      ///-----------------------------------------------------------------------------
      //ＤＢ(atdptn)より勤務パターン情報取得
      ///-----------------------------------------------------------------------------
      $data_job = $obj->get_jobmst_array();
      if (!isset($postback)) {
          $postback = "";
      }
      //登録時
      if ($postback == "1") {
          //"0001"登録
          $arr_job_list1 = split(",", $job_list1);
          update_list($con, $fname, $arr_job_list1, $group_id, YAKIN_AKE);

          //"0002"登録
          $arr_job_list2 = split(",", $job_list2);
          update_list($con, $fname, $arr_job_list2, $group_id, NIKKIN);

          //"0003"登録
          $arr_job_list3 = split(",", $job_list3);
          update_list($con, $fname, $arr_job_list3, $group_id, HANKIN);

          //"0004"登録
          $arr_job_list4 = split(",", $job_list4);
          update_list($con, $fname, $arr_job_list4, $group_id, YAKIN_IRI);

          //"0005"登録
          $arr_job_list5 = split(",", $job_list5);
          update_list($con, $fname, $arr_job_list5, $group_id, KOUKYU);

          //"0006"登録
          $arr_job_list6 = split(",", $job_list6);
          update_list($con, $fname, $arr_job_list6, $group_id, NENKYU);
      }

      //表示用処理
      if ($group_id == "") {
          //グループが設定されていない場合（初期表示）はDBから取得してきたデータの一番目のデータから取得する
          $group_id = $data_wktmgrp[0]["id"];
      }

      //勤務用リスト取得
      list($arr_shift_pattern_all, $arr_shift_pattern_regist) = get_list_kinmu($con, $fname, $group_id);
      //休日リスト取得
      list($arr_shift_pattern_all_kyu, $arr_shift_pattern_regist_kyu) = get_list_kyu($con, $fname, $group_id);
  }

  $conf = new Cmx_SystemConfig();
  if (isset($_POST["pattern"])) {
      if (!file_exists("opt/kanrinishi_config.php")) {
          ?>
          <script type="text/javascript">
              alert('Bパターン用の設定ファイルが設置されていない為切り替えができません。\n管理者にご連絡ください。');
          </script>
          <?php
      } else {
          $pattern = $conf->set('duty_shift.use_pattern', $_POST["pattern"]);
      }
  }
  $pattern = $conf->get('duty_shift.use_pattern');
  if (empty($pattern)) {
      $pattern = 'A';
  }
  $selectedA = "";
  $selectedB = "";
  switch ($pattern) {
      case "A":
          $checkedA = "checked";
          break;
      case "B":
          $checkedB = "checked";
          break;
  }
  ?>

  <!-- ************************************************************************ -->
  <!-- JavaScript -->
  <!-- ************************************************************************ -->
  <script type="text/javascript" src="js/fontsize.js"></script>
  <script type="text/javascript" src="js/duty_shift/duty_shift_diary_shift_pattern.js?<? echo time(); ?>"></script>
  <script type="text/javascript">
              function changePattern(value) {
                  if (value == 'A') {
                      document.getElementById("tblPatternA").style.display = "block";
                  } else {
                      document.getElementById("tblPatternA").style.display = "none";
                  }
              }

  </script>

  <!-- ************************************************************************ -->
  <!-- HTML -->
  <!-- ************************************************************************ -->
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <style type="text/css">
   .list {border-collapse:collapse;}
   .list td {border:#5279a5 solid 1px;}
   .list td td {border-width:0;}
  </style>
 </head>
 <body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
   <tr>
    <td>
     <!-- ------------------------------------------------------------------------ -->
     <!-- 画面遷移／タブ -->
     <!-- ------------------------------------------------------------------------ -->
     <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
       <td>
        <?
        // 画面遷移
        echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
        show_manage_title($session, $section_admin_auth);			//duty_shift_common.ini
        echo("</table>\n");

        // タブ
        $arr_option = "";
        echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
        show_manage_tab_menuitem($session, $fname, $arr_option);	//duty_shift_manage_tab_common.ini
        echo("</table>\n");

        // 下線
        echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td bgcolor=\"#5279a5\"><img src=\"img/spacer.gif\" width=\"1\" height=\"2\" alt=\"\"></td></tr></table><img src=\"img/spacer.gif\" width=\"1\" height=\"5\" alt=\"\"><br>\n");
        ?>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr height="22" >
          <td ><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
            病棟管理日誌&nbsp;&nbsp;
            <a href="duty_shift_sfc_conf.php?session=<?= $session ?>&group_id=<?= $group_id ?>">管理日誌（SFC形式）</a>&nbsp;&nbsp;
           </font>
          </td>
         </tr>
        </table>
       </td>
      </tr>
     </table>


     <!-- ------------------------------------------------------------------------------------- -->
     <!-- データ不備時(勤務パターングループ情報(wktmgrp)が未設定の場合はメッセージを出して終了) -->
     <!-- ------------------------------------------------------------------------------------- -->
     <?
     if ($err_msg_1 != "") 
     {
     echo($err_msg_1);
     echo("<br>\n");
     } 
     else
     {
     ?>

     <!-- ------------------------------------------------------------------------ -->
     <!-- 勤務パターングループ名設定 ------------------------------------------->
     <!-- ------------------------------------------------------------------------ -->
     <form name="mainform" method="post">
      <p>
       使用する様式
       <input type = "radio" name = "pattern" value = "A" <?php echo $checkedA;
  ?> onclick="changePattern(this.value);">Aパターン
       <input type="radio" name="pattern" value="B" <?php echo $checkedB; ?> onclick="changePattern(this.value);">Bパターン(データ取込あり)
       <input type="submit" value="登録" onclick="updateSetting();">
      </p>

      <table id="tblPatternA" width="500" border="0" cellspacing="0" cellpadding="0">
       <tr height="22">

        <td width=30%><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務パターングループ名</font></td>

        <td width=30%>
         <?
         echo("<select name=\"group_id\" onchange=\"this.form.submit();\"> \n");
         for($i=0;$i<count($data_wktmgrp);$i++) 
         {
         $wk_id= $data_wktmgrp[$i]["id"];
         $wk_name = $data_wktmgrp[$i]["name"];
         echo("<option value=\"$wk_id\"");
         if ($group_id == $wk_id) 
         {
         echo(" selected");
         }
         echo(">$wk_name\n");
         }
         echo("</select> \n");
         ?>
        </td>

        <td width=40% align="right">
        </td>
       </tr>



       <tr height="22">
        <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>＜勤務パターン分類＞</b></font></td>
       </tr>

       <tr height="22">
        <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>夜勤明</b></font></td>
       </tr>
       <tr height="22">
        <td>
         <select name="tgt_job1" id="tgt_job1" size="6" multiple style="width:180px;">
          <?show_options($arr_shift_pattern_regist, YAKIN_AKE);?>
         </select>
        </td>
        <td>
         <input type="button" name="select_item" value="←" style="margin-left:2em;width:4.5em;" onclick="selectItem('src_job1', 'tgt_job1');">&nbsp;&nbsp;&nbsp;&nbsp;
         <br><br>
         <input type="button" value="→" style="margin-left:2em;width:4.5em;" onclick="deleteItem('tgt_job1');">&nbsp;&nbsp;&nbsp;&nbsp;
        </td>
        <td>
         <select name="src_job1" id="src_job1" size="6" multiple style="width:180px;">
          <?show_options($arr_shift_pattern_all, PATTERN_ALL);?>
         </select>
        </td>
       </tr>


       <tr height="22">
        <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>日勤</b></font></td>
       </tr>
       <tr height="22">
        <td>
         <select name="tgt_job2" id="tgt_job2" size="6" multiple style="width:180px;">
          <?show_options($arr_shift_pattern_regist, NIKKIN);?>
         </select>
        </td>
        <td>
         <input type="button" name="select_item" value="←" style="margin-left:2em;width:4.5em;" onclick="selectItem('src_job2', 'tgt_job2');">&nbsp;&nbsp;&nbsp;&nbsp;
         <br><br>
         <input type="button" value="→" style="margin-left:2em;width:4.5em;" onclick="deleteItem('tgt_job2');">&nbsp;&nbsp;&nbsp;&nbsp;
        </td>
        <td>
         <select name="src_job2" id="src_job2" size="6" multiple style="width:180px;">
          <?show_options($arr_shift_pattern_all, PATTERN_ALL);?>
         </select>
        </td>
       </tr>


       <tr height="22">
        <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>半勤</b></font></td>
       </tr>
       <tr height="22">
        <td>
         <select name="tgt_job3" id="tgt_job3" size="6" multiple style="width:180px;">
          <?show_options($arr_shift_pattern_regist, HANKIN);?>
         </select>
        </td>
        <td>
         <input type="button" name="select_item" value="←" style="margin-left:2em;width:4.5em;" onclick="selectItem('src_job3', 'tgt_job3');">&nbsp;&nbsp;&nbsp;&nbsp;
         <br><br>
         <input type="button" value="→" style="margin-left:2em;width:4.5em;" onclick="deleteItem('tgt_job3');">&nbsp;&nbsp;&nbsp;&nbsp;
        </td>
        <td>
         <select name="src_job3" id="src_job3" size="6" multiple style="width:180px;">
          <?show_options($arr_shift_pattern_all, PATTERN_ALL);?>
         </select>
        </td>
       </tr>


       <tr height="22">
        <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>夜勤入</b></font></td>
       </tr>
       <tr height="22">
        <td>
         <select name="tgt_job4" id="tgt_job4" size="6" multiple style="width:180px;">
          <?show_options($arr_shift_pattern_regist, YAKIN_IRI);?>
         </select>
        </td>
        <td>
         <input type="button" name="select_item" value="←" style="margin-left:2em;width:4.5em;" onclick="selectItem('src_job4', 'tgt_job4');">&nbsp;&nbsp;&nbsp;&nbsp;
         <br><br>
         <input type="button" value="→" style="margin-left:2em;width:4.5em;" onclick="deleteItem('tgt_job4');">&nbsp;&nbsp;&nbsp;&nbsp;
        </td>
        <td>
         <select name="src_job4" id="src_job4" size="6" multiple style="width:180px;">
          <?show_options($arr_shift_pattern_all, PATTERN_ALL);?>
         </select>
        </td>
       </tr>


       <tr height="22">
        <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>公休</b></font></td>
       </tr>
       <tr height="22">
        <td>
         <select name="tgt_job5" id="tgt_job5" size="6" multiple style="width:180px;">
          <?show_options($arr_shift_pattern_regist_kyu, KOUKYU);?>
         </select>
        </td>
        <td>
         <input type="button" name="select_item" value="←" style="margin-left:2em;width:4.5em;" onclick="selectItem('src_job5', 'tgt_job5');">&nbsp;&nbsp;&nbsp;&nbsp;
         <br><br>
         <input type="button" value="→" style="margin-left:2em;width:4.5em;" onclick="deleteItem('tgt_job5');">&nbsp;&nbsp;&nbsp;&nbsp;
        </td>
        <td>
         <select name="src_job5" id="src_job5" size="6" multiple style="width:180px;">
          <?show_options($arr_shift_pattern_all_kyu, PATTERN_ALL);?>
         </select>
        </td>
       </tr>



       <tr height="22">
        <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>年休</b></font></td>
       </tr>
       <tr height="22">
        <td>
         <select name="tgt_job6" id="tgt_job6" size="6" multiple style="width:180px;">
          <?show_options($arr_shift_pattern_regist_kyu, NENKYU);?>
         </select>
        </td>
        <td>
         <input type="button" name="select_item" value="←" style="margin-left:2em;width:4.5em;" onclick="selectItem('src_job6', 'tgt_job6');">&nbsp;&nbsp;&nbsp;&nbsp;
         <br><br>
         <input type="button" value="→" style="margin-left:2em;width:4.5em;" onclick="deleteItem('tgt_job6');">&nbsp;&nbsp;&nbsp;&nbsp;
        </td>
        <td>
         <select name="src_job6" id="src_job6" size="6" multiple style="width:180px;">
          <?show_options($arr_shift_pattern_all_kyu, PATTERN_ALL);?>
         </select>
        </td>
       </tr>


       <tr height="2">
        <td colspan="3" align="right">
        </td>
       </tr>

       <tr height="22">
        <td colspan="3" align="right">
         <input type="submit" value="登録" onclick="updateSetting();">
        </td>
       </tr>
      </table>

      <!-- ------------------------------------------------------------------------ -->
      <!-- ＨＩＤＤＥＮ -->
      <!-- ------------------------------------------------------------------------ -->
      <input type="hidden" name="session" value="<? echo($session); ?>">
      <input type="hidden" name="postback" value="">
      <input type="hidden" name="job_list1" value="">
      <input type="hidden" name="job_list2" value="">
      <input type="hidden" name="job_list3" value="">
      <input type="hidden" name="job_list4" value="">
      <input type="hidden" name="job_list5" value="">
      <input type="hidden" name="job_list6" value="">
      <input type="hidden" name="ptn_list" value="">
     </form>

     <?
     }
     ?>

    </td>
   </tr>
  </table>
  <script type="text/javascript">changePattern('<?php echo $pattern; ?>');
  </script>
 </body>
 <? pg_close($con); ?>
</html>

<?php

/**
 * show_options 勤務パターンごとにセレクトリストのリスト欄に出力する項目を表示する
 *
 * @param mixed $data_job グループごとに取得した勤務、休暇パターンリスト
 * @param mixed $$shift_pattern_id 指定勤務パターンID
 * @return なし
 *
 */
function show_options($data_job, $shift_pattern_id) {

    for ($i = 0; $i < count($data_job); $i++) {

        //指定された勤務パターンＩＤのみ出力する（夜勤明、半勤等）or 全件出力する
        if ($data_job[$i][shift_pattern_id] == $shift_pattern_id || $shift_pattern_id == PATTERN_ALL) {
            $id = $data_job[$i]["id"];
            $nm = $data_job[$i]["name"];
            echo("<option value=\"$id\"");
            echo(">$nm\n");
        }
    }
}

/**
 * update_list 勤務パターンテーブルの削除→登録処理
 *
 * @param mixed $con 接続情報
 * @param mixed $fname PHPファイル名
 * @param mixed $arr_job_list1 登録する勤務、休暇リスト
 * @param mixed $group_id 登録するグループID
 * @param mixed $pattern_id 登録する勤務、休暇パターン種別
 * @return なし
 *
 */
function update_list($con, $fname, $arr_job_list1, $group_id, $pattern_id) {

    ///-----------------------------------------------------------------------------
    // トランザクションを開始
    ///-----------------------------------------------------------------------------
    pg_query($con, "begin transaction");

    //勤務パターンテーブル(duty_shift_group_shift_pattern)を一旦グループ単位で削除する
    $sql = "delete from duty_shift_group_shift_pattern";
    $cond = "where group_id='" . $group_id . "' AND shift_pattern_id='" . $pattern_id . "'";
    $del = delete_from_table($con, $sql, $cond, $fname);
    if ($del == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }


    //勤務パターンテーブル(duty_shift_group_shift_pattern)にパターン分類ごとに登録する
    for ($i = 0; $i < count($arr_job_list1); $i++) {

        $id_int = null;
        $id_char = null;

        //年休・公休であったらreason_idにデータを設定、それ以外はatdptn_idにデータを設定
        if ($pattern_id == KOUKYU || $pattern_id == NENKYU) {
            //空白を除く
            if ($arr_job_list1[$i] == "") {
                continue;
            } else {
                $id_char = $arr_job_list1[$i];
            }
        } else {
            //空白を除く
            if ($arr_job_list1[$i] == "") {
                continue;
            } else {
                //キャスト   string → int
                $id_int = (int) $arr_job_list1[$i];
            }
        }

        $sql = "insert into duty_shift_group_shift_pattern (group_id, shift_pattern_id, atdptn_id ,reason_id ,sort_number";
        $sql .= ") values (";
        $content = array($group_id, $pattern_id, $id_int, $id_char, $i + 1);
        $ins = insert_into_table($con, $sql, $content, $fname);
        if ($ins == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }

    ///-----------------------------------------------------------------------------
    // トランザクションをコミット
    ///-----------------------------------------------------------------------------
    pg_query("commit");
}

/**
 * get_list_kinmu 指定されたグループの勤務リストを取得する
 *
 * @param mixed $con 接続情報
 * @param mixed $fname PHPファイル名
 * @param mixed $group_id 指定された勤務パターングループ
 * @return mixed　$arr_shift_pattern_all 全勤務パターングループ
 * @return mixed　$arr_shift_pattern_regist 登録されている勤務パターングループ
 *
 */
function get_list_kinmu($con, $fname, $group_id) {

    $arr_shift_pattern_all = array();
    $arr_shift_pattern_regist = array();

    ////グループ単位の全勤務パターンリスト取得(atdptn)
    $sql = "select atdptn_id, atdptn_nm from atdptn";
    $cond = "where group_id=" . $group_id;
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $num1 = pg_numrows($sel);

    for ($i = 0; $i < $num1; $i++) {
        $arr_shift_pattern_all[$i]["id"] = pg_result($sel, $i, "atdptn_id");
        $arr_shift_pattern_all[$i]["name"] = pg_result($sel, $i, "atdptn_nm");
    }

    ////登録されているグループ単位の勤務パターンリスト取得(duty_shift_group_shift_pattern)
    $sql = "select  group_id, LTRIM(reason_id), shift_pattern_id, atdptn_id, sort_number from duty_shift_group_shift_pattern";
    $cond = "where group_id='" . $group_id . "' order by shift_pattern_id ,sort_number";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $num2 = pg_numrows($sel);

    //取得したデータを配列へ設定する
    for ($i = 0; $i < $num2; $i++) {
        $arr_shift_pattern_regist[$i]["group_id"] = pg_result($sel, $i, "group_id");
        $arr_shift_pattern_regist[$i]["shift_pattern_id"] = pg_result($sel, $i, "shift_pattern_id");
        $arr_shift_pattern_regist[$i]["id"] = pg_result($sel, $i, "atdptn_id");
        $arr_shift_pattern_regist[$i]["sort_number"] = (int) pg_result($sel, $i, "sort_number");
    }

    ////登録されているリストのIDを基にして全勤務パターンリストから名前を参照し、取得する
    for ($wrk2 = 0; $wrk2 < $num2; $wrk2++) {
        for ($wrk1 = 0; $wrk1 < $num1; $wrk1++) {
            if ($arr_shift_pattern_regist[$wrk2]["id"] == $arr_shift_pattern_all[$wrk1]["id"]) {
                $arr_shift_pattern_regist[$wrk2]["name"] = $arr_shift_pattern_all[$wrk1]["name"];
            }
        }
    }

    //設定した配列を返却する
    return array($arr_shift_pattern_all, $arr_shift_pattern_regist);
}

/**
 * get_list_kyu 指定された休暇リストのパターンを取得する
 *
 * @param mixed $con 接続情報
 * @param mixed $fname PHPファイル名
 * @param mixed $group_id 指定された勤務パターングループ
 * @return mixed　$arr_shift_pattern_all_kyu 全休暇パターングループ
 * @return mixed　$arr_shift_pattern_regist_kyu 登録されている休暇パターングループ
 *
 */
function get_list_kyu($con, $fname, $group_id) {

    $arr_shift_pattern_all_kyu = array();
    $arr_shift_pattern_regist_kyu = array();

    ////グループ単位の全休暇パターンリスト取得(atdbk_reason_mst)
    $sql = "select reason_id, display_name, default_name from atdbk_reason_mst";
    $cond = "where display_flag='t' order by sequence";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $num1 = pg_numrows($sel);

    for ($i = 0; $i < $num1; $i++) {
        $arr_shift_pattern_all_kyu[$i]["id"] = pg_result($sel, $i, "reason_id");

        //もし表示用の名前が登録されていたらそれを表示し、表示用の名前が登録されていなかったらデフォルトの名前を取得して表示する
        $work_name = pg_result($sel, $i, "display_name");
        if ($work_name == "") {
            $arr_shift_pattern_all_kyu[$i]["name"] = pg_result($sel, $i, "default_name");
        } else {
            $arr_shift_pattern_all_kyu[$i]["name"] = $work_name;
        }
    }

    ////登録されているグループ単位の休日パターンリスト取得(duty_shift_group_shift_pattern)
    $sql = "select group_id, reason_id, shift_pattern_id, atdptn_id, sort_number from duty_shift_group_shift_pattern";
    $cond = "where group_id='" . $group_id . "' and reason_id IS NOT NULL order by shift_pattern_id ,sort_number";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $num2 = pg_numrows($sel);

    //DBから取得したグループ単位の休日パターンリストを配列へ設定する
    for ($i = 0; $i < $num2; $i++) {
        $arr_shift_pattern_regist_kyu[$i]["group_id"] = pg_result($sel, $i, "group_id");
        $arr_shift_pattern_regist_kyu[$i]["shift_pattern_id"] = pg_result($sel, $i, "shift_pattern_id");
        $arr_shift_pattern_regist_kyu[$i]["id"] = trim(pg_result($sel, $i, "reason_id"));
        $arr_shift_pattern_regist_kyu[$i]["sort_number"] = (int) pg_result($sel, $i, "sort_number");
    }

    ////登録されているリストのIDを基にして全勤務パターンリストから名前を参照し、取得する
    for ($wrk2 = 0; $wrk2 < $num2; $wrk2++) {
        for ($wrk1 = 0; $wrk1 < $num1; $wrk1++) {
            if ($arr_shift_pattern_regist_kyu[$wrk2]["id"] == $arr_shift_pattern_all_kyu[$wrk1]["id"]) {
                $arr_shift_pattern_regist_kyu[$wrk2]["name"] = $arr_shift_pattern_all_kyu[$wrk1]["name"];
            }
        }
    }

    //設定した配列を返却する
    return array($arr_shift_pattern_all_kyu, $arr_shift_pattern_regist_kyu);
}
