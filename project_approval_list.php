<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("mygroup_common.php");
require_once("label_by_profile_type.ini");
require_once("application_workflow_select_box.ini");


//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// ログインユーザの職員IDを取得
$sql = "select emp_id from session where session_id = '$session'";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// マイグループと職員名リスト取得
list($mygroups, $employees) = get_mygroups_employees($con, $fname, $emp_id);
// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);


if($from_parent_st == "prcd")
{
	//親画面が議事録作成画面
	$str_parent_form_name = "prcd";
	
}
else
{
	//親画面が委員会ＷＧ登録、詳細画面
	$str_parent_form_name = "pjt";
}


?>

<?
//====================================
// HTML
//====================================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ワークフロー｜承認者選択</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script src="js/prototype/dist/prototype.js"></script>
<script language="javascript">

var childwin = null;
function openEmployeeList(item_id) {
	dx = screen.width;
	dy = screen.top;
	base = 0;
	wx = 720;
	wy = 600;
	var url = './emplist_popup.php';
	url += '?session=<?=$session?>';
	url += '&emp_id=<?=$emp_id?>';
	url += '&mode=6';
	url += '&item_id='+item_id;
	childwin = window.open(url, 'emplistpopup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

	childwin.focus();
}

function openProjectList()
{
	dx = screen.width;
	dy = screen.top;
	base = 0;
	wx = 720;
	wy = 600;
	var url = './workflow_project_list.php';
	url += '?session=<?=$session?>';
	childwin = window.open(url, 'popup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
	childwin.focus();
}


function closeEmployeeList() {
	if (childwin != null && !childwin.closed) {
		childwin.close();
	}
	childwin = null;
}

// 職員指定画面から呼ばれる関数
function add_target_list(item_id, emp_id,emp_name)
{
	var emp_ids = emp_id.split(", ");
	var emp_names = emp_name.split(", ");
	var target_length = document.wkfw.target.options.length;

	//追加
	for(i=0;i<emp_ids.length;i++)
	{
		var in_group = false;
		emp_id = emp_ids[i];
		emp_name = emp_names[i];
		for (var k = 0, l = document.wkfw.target.options.length; k < l; k++) {
			if (document.wkfw.target[k].value == emp_id) {
				in_group = true;
				break;
			}
		}

		if (!in_group) {
			addOption(document.wkfw.target, emp_id, emp_name);
		}
	}
}

function getSelectedValue(sel) {
	return sel.options[sel.selectedIndex].value;
}

function deleteAllOptions(box) {

	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}
}

function addOption(box, value, text, selected) {

	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	if (selected == value) {
		opt.selected = true;
	}
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
}

// 追加ボタン
function addEmp() {

	for (var i = 0, j = document.wkfw.emplist.options.length; i < j; i++)
	{
		if (document.wkfw.emplist.options[i].selected)
		{
			var emp_id = document.wkfw.emplist.options[i].value;
			var emp_name = document.wkfw.emplist.options[i].text;
	
			var in_group = false;
			for (var k = 0, l = document.wkfw.target.options.length; k < l; k++)
			{
				if (document.wkfw.target.options[k].value == emp_id)
				{
					in_group = true;
					break;
				}
			}
	
			if (!in_group)
			{
				set_wm_counter(emp_id);
				addOption(document.wkfw.target, emp_id, emp_name);
			}
		}
	}
}

// 削除ボタン
function deleteEmp() {

	for (var i = document.wkfw.target.options.length - 1; i >= 0; i--) 
	{
		if (document.wkfw.target.options[i].selected) 
		{
<?

//if($from_parent_st == "prcd")
//{
	//議事録作成画面から呼ばれた場合はデフォルトで設定されている承認者は削除できないようにする→無くしました
?>
/*
			var emp_id = window.opener.document.<?=$str_parent_form_name?>.<?=$wkfw_nm?>_emp_id_def<? echo($approve); ?>.value;
			if(emp_id != "") 
			{
				arr_emp_id = emp_id.split(",");

				for(wcnt=0; wcnt<arr_emp_id.length; wcnt++)
				{
					if(arr_emp_id[i] == document.wkfw.target.options[i].value)
					{
						alert("初期設定されている承認者なので削除できません");
						return;
					}
				}
			}
*/
<?
//}		
?>		
			document.wkfw.target.options[i] = null;
		}
	}
}


function initPage() {

	groupOnChange();

	// 次の承認者への通知タイミング
	var next_notice_div = window.opener.document.<?=$str_parent_form_name?>.<?=$wkfw_nm?>_next_notice_div<? echo($approve); ?>.value;

	if(next_notice_div != '')
	{
	    for (i = 0; i < document.wkfw.next_notice_div.length; i++)
	    {
			if(next_notice_div == document.wkfw.next_notice_div[i].value)
			{
				document.wkfw.next_notice_div[i].checked = true;
			}
	    }
	}
	else
	{
		document.wkfw.next_notice_div[0].checked = true;	
	}
	//changeNextNotice(multi_apv_flg);


	var emp_id = window.opener.document.<?=$str_parent_form_name?>.<?=$wkfw_nm?>_emp_id<? echo($approve); ?>.value;
	var emp_nm = window.opener.document.<?=$str_parent_form_name?>.<?=$wkfw_nm?>_emp_nm<? echo($approve); ?>.value;
	if(emp_id != "") {
		var obj_target = document.wkfw.target;
		deleteAllOptions(obj_target);

		arr_emp_id = emp_id.split(",");
		arr_emp_nm = emp_nm.split(", ");

		for(i=0; i<arr_emp_id.length; i++)
		{
			addOption(obj_target, arr_emp_id[i], arr_emp_nm[i], '');
		}
	}

}


function setApprove()
{

	var approve_content = "";
	var emp_id = "";
	var emp_nm = "";

	// 職員指定
	if(approve_content != '')
	{
		approve_content += "<BR>";
	}
    var target_length = document.wkfw.target.options.length;
    if(target_length == 0)
    {
        alert('職員を選択してください。');
        return false;
    }



	//他の階層に同じ承認者がいればエラーメッセージを出す処理
	for(var cnta=0; cnta<target_length; cnta++)
	{
        emp_id_self = document.wkfw.target.options[cnta].value;
    	emp_nm_self = document.wkfw.target.options[cnta].text;
	
<?
		for($wcnt=1; $wcnt < ($maxCnt+1) ; $wcnt++)
		{
			if($wcnt == $approve)
			{
				//自分の階層は判定しない
				continue;
			}
			else
			{
?>
				var emp_id_parent = window.opener.document.<?=$str_parent_form_name?>.<?=$wkfw_nm?>_emp_id<? echo($wcnt); ?>.value;
				var emp_nm_parent = window.opener.document.<?=$str_parent_form_name?>.<?=$wkfw_nm?>_emp_nm<? echo($wcnt); ?>.value;

				if(emp_id_parent != "") 
				{
					arr_emp_id_parent = emp_id_parent.split(",");
					arr_emp_nm_parent = emp_nm_parent.split(", ");

					for(var cntb=0; cntb < arr_emp_id_parent.length; cntb++)
					{
						if(emp_id_self == arr_emp_id_parent[cntb])
						{
							alert("他の階層に同様の承認者が設定されています「"+arr_emp_nm_parent[cntb]+"」");
							return ;
						}
					}
				}
<?		
			}
		}
?>		
	}















		for(i=0; i<target_length; i++)
		{
			if(i > 0)
			{
				emp_id += ",";
				emp_nm += ", ";
			}
	        emp_id += document.wkfw.target.options[i].value;
    	    emp_nm += document.wkfw.target.options[i].text;
		}
		//approve_content += emp_nm;

	window.opener.document.getElementById('<?=$wkfw_nm?>_approve_content<?=$approve?>').innerHTML = emp_nm;
	
	//window.opener.document.<?=$str_parent_form_name?>.<?=$wkfw_nm?>_approve<? echo($approve); ?>.value = approve_content;

	window.opener.document.<?=$str_parent_form_name?>.<?=$wkfw_nm?>_emp_id<? echo($approve); ?>.value = emp_id;

	window.opener.document.<?=$str_parent_form_name?>.<?=$wkfw_nm?>_emp_nm<? echo($approve); ?>.value = emp_nm;

	for(i=0; i<document.wkfw.next_notice_div.length; i++)
	{
		if(document.wkfw.next_notice_div[i].checked) {
			next_notice_div = document.wkfw.next_notice_div[i].value;
		}
	}
	window.opener.document.<?=$str_parent_form_name?>.<?=$wkfw_nm?>_next_notice_div<? echo($approve); ?>.value = next_notice_div;


	closeEmployeeList();

	self.close();
}

function groupOnChange() {
	var group_id = getSelectedValue(document.wkfw.mygroup);

	// 職員一覧セレクトボックスのオプションを全削除
	deleteAllOptions(document.wkfw.emplist);

	// 職員一覧セレクトボックスのオプションを作成
<? foreach ($employees as $tmp_group_id => $arr) { ?>
	if (group_id == '<? echo $tmp_group_id; ?>') {
	<? foreach($arr as $tmp_emp) { ?>
		addOption(document.wkfw.emplist, '<? echo $tmp_emp["emp_id"]; ?>', '<? echo $tmp_emp["name"]; ?>');
	<? } ?>
	}
<? } ?>
}

function set_wm_counter(ids)
{
	
	var url = 'emplist_address_counter.php';
	var params = $H({'session':'<?=$session?>','emp_ids':ids}).toQueryString();
	var myAjax = new Ajax.Request(
		url, 
		{
			method: 'post',
			postBody: params
		});
}

/*
function changeNextNotice(val)
{
	if(val == 't')
	{
		notice_flg = false;
	}
	else
	{
		notice_flg = true;
	}

	for (var i = 0; i < document.wkfw.next_notice_div.length; i++) {
		document.wkfw.next_notice_div[i].disabled = notice_flg;
	}
}
*/


</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
<!--
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}

table.block {border-collapse:collapse;}
table.block td {border:#5279a5 solid 1px;padding:1px;}
table.block td td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#5279a5 solid 0px;}
table.block_in td td {border-width:1;}
// -->
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="3" leftmargin="3" marginheight="3" marginwidth="3" onload="initPage();">
<form name="wkfw" action="timecard_approval_list.php" method="post">
<input type="hidden" name="approve" value="<? echo($approve); ?>">


<img src="img/spacer.gif" width="1" height="3" alt=""><br>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>承認者選択</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="closeEmployeeList();window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>

<img src="img/spacer.gif" width="1" height="3" alt=""><br>

<table width="100%" border="0" cellspacing="0" cellpadding="0">

<tr>
<td>
<table width="20%" border="0" cellspacing="0" cellpadding="0"  class="list">
<tr height="22">
<td bgcolor="#f6f9ff" align="center">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">承認階層<? echo($approve); ?>の設定</font>
</td>
</tr>
</table>

<img src="img/spacer.gif" width="1" height="5" alt=""><br>

<!--複数承認者指定、通知タイミング設定 -->
<table border='0' cellspacing='0' cellpadding='0' class="block_in">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">■次の承認者への通知タイミング</font></td>
<td><input type="radio" name="next_notice_div" value="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">同期(全員が承認したら通知)</font></td>
</tr>
<tr>
<td></td>
<td colspan="2"><input type="radio" name="next_notice_div" value="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限並列(承認者の一人でも承認すれば承認完了、通知)</font></td>
</tr>
</table>

<img src="img/spacer.gif" width="1" height="2" alt=""><br>


<table width="100%" border='0' cellspacing='0' cellpadding='0' class="block">
<!-- 職員指定 -->
	<tr>
		<td width="20%" bgcolor="#f6f9ff">
			<table>
				<tr>
				<td>
					<input type="button" name="emplist_btn" value="職員名簿" style="margin-left:2em;width=5.5em;" onclick="openEmployeeList('1');"><br>
				</td>
				</tr>
			</table>
		</td>

		<td colspan="2" bgcolor="#FFFFFF">
			<table width="100%" border="0" cellspacing="3" cellpadding="0">
				<tr>
					<td width="160" height="22" valign="bottom">
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">対象者</font>
					</td>
					<td width="30"></td>
					<td width="*">
						<select name="mygroup" onchange="groupOnChange();">
						<?
						 foreach ($mygroups as $tmp_mygroup_id => $tmp_mygroup) {
						?>
						<option value="<?=$tmp_mygroup_id?>"
						<?
						if($mygroup == $tmp_mygroup_id) {
						 echo(" selected");
						}
						?>
						><?=$tmp_mygroup?></option>
						<?
						}
						?>
						</selelct>
					</td>
				</tr>
				<tr>
					<td>
					<select name="target" size="10" multiple style="width:150px;height:150px;"></select>
					</td>
					<td>
						<input type="button" name="add_emp" value="&nbsp;&lt;&nbsp;追加" onclick="addEmp();">
						<br><br>
						<input type="button" name="delete_emp" value="削除&nbsp;&gt;&nbsp;" onclick="deleteEmp();">
					</td>
					<td>
						<select name="emplist" size="10" style="width:150px;height:150px;" multiple></select>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<img src="img/spacer.gif" width="1" height="5" alt=""><br>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
<td align="right">
<input type="button" value="設定" onclick="setApprove();">
</td>
</tr>
</table>
</td>
</tr>
</table>


</form>
</body>
</html>

<?
pg_close($con);
?>