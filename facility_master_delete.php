<?
require_once("Cmx.php");
require_once("aclg_set.php");
require("about_session.php");
require("about_authority.php");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 施設・設備権限チェック
$checkauth = check_authority($session, 41, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// トランザクションの開始
pg_query($con, "begin transaction");

// チェックされた施設・設備をループ
if (is_array($trashbox)) {
	foreach ($trashbox as $catefcl) {
		list($cate_id, $facility_id) = split("-", $catefcl);

		// カテゴリがチェックされている場合
		if ($facility_id == "") {

			// 予約可能職員情報を削除
			$sql = "delete from fclauth";
			$cond = "where facility_id in (select facility_id from facility where fclcate_id = $cate_id)";
			$del = delete_from_table($con, $sql, $cond, $fname);
			if ($del == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}

			// 施設・設備管理者情報を削除
			$sql = "delete from fcladmin";
			$cond = "where facility_id in (select facility_id from facility where fclcate_id = $cate_id)";
			$del = delete_from_table($con, $sql, $cond, $fname);
			if ($del == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}
			// 施設・設備管理者部署情報を削除
			$sql = "delete from fcladmindept";
			$cond = "where facility_id in (select facility_id from facility where fclcate_id = $cate_id)";
			$del = delete_from_table($con, $sql, $cond, $fname);
			if ($del == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}
			// 施設・設備管理者役職情報を削除
			$sql = "delete from fcladminst";
			$cond = "where facility_id in (select facility_id from facility where fclcate_id = $cate_id)";
			$del = delete_from_table($con, $sql, $cond, $fname);
			if ($del == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}
			// 設備カテゴリ管理者情報を削除
			$sql = "delete from fclcateadmin";
			$cond = "where fclcate_id = $cate_id";
			$del = delete_from_table($con, $sql, $cond, $fname);
			if ($del == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}

			// 設備カテゴリ管理者部署情報を削除
			$sql = "delete from fclcateadmindept";
			$cond = "where fclcate_id = $cate_id";
			$del = delete_from_table($con, $sql, $cond, $fname);
			if ($del == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}

			// 設備カテゴリ管理者役職情報を削除
			$sql = "delete from fclcateadminst";
			$cond = "where fclcate_id = $cate_id";
			$del = delete_from_table($con, $sql, $cond, $fname);
			if ($del == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}

			// カテゴリ内の施設・設備に対する予約を論理削除
			$sql = "update reservation set";
			$set = array("reservation_del_flg");
			$setvalue = array("t");
			$cond = "where fclcate_id = $cate_id";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}

			$sql = "update reservation2 set";
			$set = array("reservation_del_flg");
			$setvalue = array("t");
			$cond = "where fclcate_id = $cate_id";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}

			// カテゴリ内の施設・設備を論理削除
			$sql = "update facility set";
			$set = array("facility_del_flg");
			$setvalue = array("t");
			$cond = "where fclcate_id = $cate_id";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}

			// カテゴリーを論理削除
			$sql = "update fclcate set";
			$set = array("fclcate_del_flg");
			$setvalue = array("t");
			$cond = "where fclcate_id = $cate_id";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}

			// オプションの初期選択カテゴリを初期化
			$sql = "update option set";
			$set = array("default_fclcate");
			$setvalue = array(null);
			$cond = "where default_fclcate = $cate_id";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}

		// 施設・設備がチェックされている場合
		} else {

			// 予約可能職員情報を削除
			$sql = "delete from fclauth";
			$cond = "where facility_id = $facility_id";
			$del = delete_from_table($con, $sql, $cond, $fname);
			if ($del == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}

			// 施設・設備管理者情報を削除
			$sql = "delete from fcladmin";
			$cond = "where facility_id = $facility_id";
			$del = delete_from_table($con, $sql, $cond, $fname);
			if ($del == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}
			// 施設・設備管理者部署情報を削除
			$sql = "delete from fcladmindept";
			$cond = "where facility_id = $facility_id";
			$del = delete_from_table($con, $sql, $cond, $fname);
			if ($del == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}
			// 施設・設備管理者役職情報を削除
			$sql = "delete from fcladminst";
			$cond = "where facility_id = $facility_id";
			$del = delete_from_table($con, $sql, $cond, $fname);
			if ($del == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}

			// カテゴリ内の施設・設備に対する予約を論理削除
			$sql = "update reservation set";
			$set = array("reservation_del_flg");
			$setvalue = array("t");
			$cond = "where facility_id = $facility_id";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}
			$sql = "update reservation2 set";
			$set = array("reservation_del_flg");
			$setvalue = array("t");
			$cond = "where facility_id = $facility_id";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}

			// 施設・設備を論理削除
			$sql = "update facility set";
			$set = array("facility_del_flg");
			$setvalue = array("t");
			$cond = "where facility_id = $facility_id";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}
		}
	}
}

// トランザクションのコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 画面再表示
echo("<script language='javascript'>location.href = 'facility_master_menu.php?session=$session&opens=$opens';</script>");
?>
