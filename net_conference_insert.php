<?
//------------------------------------------------------------------------------
// Title     :  ネット会議登録処理
// File Name :  net_conference_insert.php
// History   :  2004/07/16 - 新規作成（岩本隆史）
// Copyright :
//------------------------------------------------------------------------------

require("about_session.php");
require("about_authority.php");

//==============================================================================
// 共通処理
//==============================================================================

$fname = $PHP_SELF;  // スクリプトパス

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// ネット会議登録権限のチェック
$auth_nc_reg = check_authority($session, 10, $fname);
if ($auth_nc_reg == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================================================================
// 登録値の編集
//==============================================================================

// 開始日時
$nmrm_start = $nmrm_start_year . $nmrm_start_month . $nmrm_start_day . $nmrm_start_hour . $nmrm_start_minute;

// 終了日時
$nmrm_end = $nmrm_end_year . $nmrm_end_month . $nmrm_end_day . $nmrm_end_hour . $nmrm_end_minute;

// 制限フラグ
$nmrm_lmt_flg = ($nmrm_lmt_flg == "on") ? "t" : "f";

//==============================================================================
// 入力チェック
//==============================================================================

// 会議室名 - 文字列長
if ($nmrm_nm == "") {
	echo("<script language='javascript'>alert('会議室名を入力してください');");
	echo("history.back();</script>");
	exit;
} else if (strlen($nmrm_nm) > 40) {
	echo("<script language='javascript'>alert('会議室名が長すぎます');");
	echo("history.back();</script>");
	exit;
}

// テーマ - 文字列長
if (strlen($nmrm_thm) > 40) {
	echo("<script language='javascript'>alert('テーマが長すぎます');");
	echo("history.back();</script>");
	exit;
}

// 開始日時 - 日付
if (!checkdate($nmrm_start_month, $nmrm_start_day, $nmrm_start_year)) {
	echo("<script language='javascript'>alert('開始日が無効です');");
	echo("history.back();</script>");
	exit;
}

// 開始日時 - 日時
if ($nmrm_start < date("YmdHi")) {
	echo("<script language='javascript'>alert('開始日時を過ぎています');");
	echo("history.back();</script>");
	exit;
}

// 終了日時 - 日付
if (!checkdate($nmrm_end_month, $nmrm_end_day, $nmrm_end_year)) {
	echo("<script language='javascript'>alert('終了日が無効です');");
	echo("history.back();</script>");
	exit;
}

// 終了日時 - 日時
if ($nmrm_end <= $nmrm_start) {
	echo("<script language='javascript'>alert('終了日時を開始日時より後にしてください');");
	echo("history.back();</script>");
	exit;
}

//==============================================================================
// 登録処理
//==============================================================================

// データベースに接続
$con = connect2db($fname);

// ネット会議IDの採番
$sql_nmrm_id = "select count(*) + 1 as nmrm_id from nmrmmst";
$cond_nmrm_id = "";
$sel_nmrm_id = select_from_table($con, $sql_nmrm_id, $cond_nmrm_id, $fname);
if ($sel_nmrm_id == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$nmrm_id = pg_fetch_result($sel_nmrm_id, 0, "nmrm_id");

// トランザクションの開始
pg_query($con, "begin transaction");

// ネット会議登録SQLの実行
$sql_nmrm = "insert into nmrmmst (nmrm_id, nmrm_nm, nmrm_thm, nmrm_start, nmrm_end, nmrm_lmt_flg) values (";
$content_nmrm = array($nmrm_id, $nmrm_nm, $nmrm_thm, $nmrm_start, $nmrm_end, $nmrm_lmt_flg);
$in_nmrm = insert_into_table($con, $sql_nmrm, $content_nmrm, $fname);
if ($in_nmrm == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

// ネット会議メンバー登録SQLの実行
if (is_array($member)) {
	foreach ($member as $emp_id) {
		$sql_nmme = "insert into nmmemst (nmrm_id, emp_id) values (";
		$content_nmme = array($nmrm_id, $emp_id);
		$in_nmme = insert_into_table($con, $sql_nmme, $content_nmme, $fname);
		if ($in_nmme == 0) {
			pg_query("rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}
}

// トランザクションのコミット
pg_query($con, "commit");

// データベース接続をクローズ
pg_close($con);

// ネット会議一覧画面に遷移
echo("<script language='javascript'>location.href='net_conference_menu.php?session=$session';</script>");
exit;
?>
