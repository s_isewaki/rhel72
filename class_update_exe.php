<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_class_name.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 23, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);
pg_query($con, "begin");

$arr_class_name = get_class_name_array($con, $fname);

// 入力チェック
if ($class_name == "") {
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('{$arr_class_name[0]}名を入力してください。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($class_name) > 60) {
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('{$arr_class_name[0]}名が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($link_key) > 12) {
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('外部連携キーが長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}

// 部門情報を更新
$area_id = ($area_id == "") ? null : intval($area_id);
$sql = "update classmst set";
$set = array("class_nm", "area_id", "class_show_flg", "link_key");
$setvalue = array($class_name, $area_id, $class_show_flg, $link_key);
$cond = "where class_id = $class_id";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 文書カテゴリとして登録されているかチェック
$sql = "select count(*) from libcate";
$cond = "where lib_archive = '3' and lib_link_id = '$class_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 登録されている場合
if (pg_fetch_result($sel, 0, 0) > 0) {

	// 文書カテゴリ名を更新
	$sql = "update libcate set";
	$set = array("lib_cate_nm");
	$setvalue = array($class_name);
	$cond = "where lib_archive = '3' and lib_link_id = '$class_id'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

// 登録されていない場合
} else {

	// 文書カテゴリIDを採番
	$sql = "select max(lib_cate_id) from libcate";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$lib_cate_id = intval(pg_fetch_result($sel, 0, 0)) + 1;

	// 文書カテゴリ情報を登録
	$sql = "insert into libcate (lib_archive, lib_cate_id, lib_link_id, lib_cate_nm) values (";
	$content = array("3", $lib_cate_id, $class_id, $class_name);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// データベース接続を閉じる
pg_query($con, "commit");
pg_close($con);

// 一覧画面に遷移
echo("<script type=\"text/javascript\">location.href = 'class_menu.php?session=$session';</script>");