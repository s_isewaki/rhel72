<?
//ファイルの読み込み
require_once('about_postgres.php');

//ＤＢのコネクション作成
$con = connect2db($fname);

$sql      = "select svg_data from genogram";
//$cond     = "where ptif_id='" . $_POST['pt_id'] . "' and emp_id='" . $_POST['emp_id'] . "' and tpl_flg = '2' and delete_flg = '0'"; // 20120306　キーを患者IDのみに変更
$cond     = "where ptif_id='" . $_POST['pt_id'] . "' and tpl_flg = '2' and delete_flg = '0'";
$sel      = select_from_table($con, $sql, $cond, $fname);
$svg_data = pg_fetch_row($sel);

if ($svg_data[0] != '') {
   	$is_data_flg = 1;			// 画像データ存在フラグ
	$xml_img = $svg_data[0];	// PDF画像用データ(xml)
} else {
	$is_data_flg = 0;			// 画像データ存在フラグ
	$xml_img = "";
}

// 画像データ存在の場合は画像呼出

if ($is_data_flg == 1) {
	$sql = "select img_data from genogram";
//	$cond     = "where ptif_id='" . $_POST['pt_id'] . "' and emp_id='" . $_POST['emp_id'] . "' and tpl_flg = '2' and delete_flg = '0'"; // 20120306　キーを患者IDのみに変更
	$cond     = "where ptif_id='" . $_POST['pt_id'] . "' and tpl_flg = '2' and delete_flg = '0'";
	$sel = select_from_table($con, $sql, $cond, $fname);

	$svg_data_i = pg_fetch_all($sel);

	$png = $svg_data_i[0]['img_data'];
}

pg_close($con);
echo $png."@@@".$xml_img;	// php4対策:php5.2.0以上はjsonで配列で処理できる。
