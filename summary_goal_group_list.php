<?
ob_start();
$fname = $_SERVER["PHP_SELF"];
require_once("about_comedix.php");
require_once("get_values.php");
require_once("show_sinryo_top_header.ini");
require_once("label_by_profile_type.ini");
require_once("sot_util.php");
require_once("summary_common.ini");
ob_end_clean();

$session = qualify_session($session,$fname);
$fplusadm_auth = check_authority($session, 59, $fname);
$con = @connect2db($fname);    // DB接続
if(!$session || !$fplusadm_auth || !$con){
    js_login_exit();
}

$mode = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : "";
$therapy_type = isset($_REQUEST["therapy_type"]) ? $_REQUEST["therapy_type"] : "PT";

//==========================================================================
// 削除
//==========================================================================
if ($mode == "delete") {
    $group_code = isset($_REQUEST["group_code"]) ? $_REQUEST["group_code"] : "";
    $ret = deleteGoalGroupMst($con, $therapy_type, $group_code, $fname);
    pg_close($con);
    
    if ($ret === true) {
        header("Location: summary_goal_group_list.php?session=$session&therapy_type=$therapy_type");
        die;
    } else {
        js_error_exit();
    }
}

//==========================================================================
// 以下、通常処理
//==========================================================================
$font = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';

// 診療科権限を取得
$entity_admin_auth = check_authority($session, 28, $fname);
// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);
// 診療区分/記録区分
$summary_kubun_title = $_label_by_profile["SUMMARY_KUBUN"][$profile_type];
// メドレポート/ＰＯレポート(Problem Oriented)
$med_report_title = $_label_by_profile["MED_REPORT"][$profile_type];
// 診療科/担当部門
$section_title = $_label_by_profile["SECTION"][$profile_type];

// リハビリ目標データを取得
$rehabili_goal_data = $c_sot_util->getGoalGroupMst($con, $therapy_type, '', 'G', $fname);
if ($rehabili_goal_data === false) {
    pg_close($con);
    js_error_exit();
}

// 治療プログラムデータを取得
$cure_program_data = $c_sot_util->getGoalGroupMst($con, $therapy_type, '', 'P', $fname);
if ($cure_program_data === false) {
    pg_close($con);
    js_error_exit();
}

// グループを取得( リハビリ目標と治療プログラムで重複は無いはずだが、念の為マージする)
$group_codes = array_unique(array_merge(array_keys($rehabili_goal_data), array_keys($cure_program_data)));
asort($group_codes);

/**
 * 目標グループデータを削除する
 * 
 * @param resource $con          データベース接続リソース
 * @param string   $therapy_type 療法種別
 * @param string   $group_code   グループコード
 * @param string   $fname        ページ名
 * 
 * @return boolean 成功時:true/失敗時:false
 */
function deleteGoalGroupMst($con, $therapy_type, $group_code, $fname)
{
    $sql = "delete from sum_med_goal_group_mst where therapy_type='$therapy_type' and group_code = $group_code";
    $upd = update_set_table($con, $sql, array(), null, "", $fname);
    if ($upd == 1) {
        return true;
    } else {
        return false;
    }
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix マスターメンテナンス | マスタ管理</title>

<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.td_border { border:#5279a5 solid 1px; }
.subtitle { font-weight:bold; margin-top:10px }
.list table { font-family: "ＭＳ Ｐゴシック", Osaka; font-size: 8px; }
.list th { border: #5279a5 solid 1px; bgcolor: #f6f9ff; font-family: "ＭＳ Ｐゴシック", "Osaka"; font-size: 13px; }
.list td { border: #5279a5 solid 1px; font-family: "ＭＳ Ｐゴシック", "Osaka"; font-size: 13px; vertical-align: top; line-height: 18px; }
</style>
<script type="text/javascript">
function registData(mode, therapy_type, group_code) {
    if (mode == 'delete') {
        if (!window.confirm('No' + group_code + 'のデータを削除しますがよろしいですか？')) {
            return;
        }
        $('#mainform').attr('action', 'summary_goal_group_list.php')
                      .append($('<input/>').attr({'type':'hidden', 'name':'mode', 'value':'delete'}));
    } else if (mode == 'update') {
        $('#mainform').attr('action', 'summary_goal_group_regist.php');
    }
    $('#mainform').append($('<input/>').attr({'type':'hidden', 'name':'therapy_type', 'value':therapy_type}))
                  .append($('<input/>').attr({'type':'hidden', 'name':'group_code', 'value':group_code}));

    $('#mainform').submit();
}
</script>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">

<? show_sinryo_top_header($session, $fname, "KANRI_MST"); ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- left -->
<td width="120" valign="top">
<table width="120" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width="118" valign="top">
<? echo summary_common_show_admin_left_menu($session, $entity_admin_auth, $section_title, $summary_kubun_title); ?>
</td>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
</td>
<!-- left -->
<!-- center -->
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->
<!-- right -->
<td valign="top">

<?
//==========================================================================
// マスタ管理メニュー
//==========================================================================
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<? summary_common_show_mst_tab_menu($session, '目標グループ'); ?>
<td>&nbsp;</td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr></table>


<?
//==========================================================================
// サブメニュー
//==========================================================================
?>
<table border="0" cellspacing="0" cellpadding="0" style="margin-top:8px">
  <tr height="22">
<?
    $therapy_types = array('PT' => '理学療法', 'OT' => '作業療法', 'ST' => '言語聴覚療法', 'PST' => '心理療法');
    foreach ($therapy_types as $key => $value) {
        if ($key == $therapy_type) {
            $bgcolor = '#958f28';
            $color = '#ffffff';
        } else {
            $bgcolor = '#e8e4bd';
            $color = '';
        }
        echo "<td width=\"130\" align=\"center\" bgcolor=\"$bgcolor\">\n";
        echo "<a href=\"summary_goal_group_list.php?session=$session&therapy_type=$key\">";
        echo "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"$color\">";
        echo "<b>{$value}一覧</b></font></a>\n";
        echo "</td>";
        echo "<td width=\"5\">&nbsp;</td>";
    }
?>  
    <td width="130" align="center" bgcolor="#e8e4bd">
      <a href="summary_goal_group_regist.php?session=<? echo $session; ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>新規登録</b></font></a>
    </td>
    <td width="5">&nbsp;</td>
    <td></td>
  </tr>
</table>

<?
//==========================================================================
// 一覧
//==========================================================================
?>
<form method="get" id="mainform" name="mainform" style="margin-top:10px">
<table border="0" cellspacing="0" cellpadding="2" style="margin-top:10px; border-collapse:collapse" class="list">
    <tr style="background-color: #f6f9ff;">
        <td style="width: 30px; text-align: center;">No.</td>
        <td style="width: 300px; text-align: center;">リハビリ目標</td>
        <td style="width: 300px; text-align: center;">治療プログラム</td>
        <td style="width: 50px; text-align: center;">変更</td>
        <td style="width: 50px; text-align: center;">削除</td>
    </tr>
<?
    foreach ($group_codes as $group_code) {
        echo "<tr>\n";
        echo "<td style=\"text-align: right;\">$group_code</td>";
        echo "<td>\n";
        if (array_key_exists($group_code, $rehabili_goal_data)) {
            foreach ($rehabili_goal_data[$group_code] as $caption) {
                echo "$caption<br>\n";
            }
        }
        echo "</td>\n";
        echo "<td>\n";
        if (array_key_exists($group_code, $cure_program_data)) {
            foreach ($cure_program_data[$group_code] as $caption) {
                echo "$caption<br>\n";
            }
        }
        echo "</td>\n";
        echo "<td style=\"text-align: center;\"><input type=\"button\" value=\"変更\" onclick=\"registData('update', '$therapy_type', $group_code);\" /></td>\n";
        echo "<td style=\"text-align: center;\"><input type=\"button\" value=\"削除\" onclick=\"registData('delete', '$therapy_type', $group_code);\" /></td>\n";
        echo "</tr>\n";
    }
?>
</table>
<input type="hidden" name="session" value="<? echo $session; ?>">
</form>

</td>
<!-- right -->
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
