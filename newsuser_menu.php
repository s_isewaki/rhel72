<?php
require_once("Cmx.php");
require_once("Cmx/Model/SystemConfig.php");
require_once("about_comedix.php");
require_once("show_class_name.ini");
require_once("show_newsuser_menu.ini");
require_once("news_common.ini");
require_once("get_values.ini");
require_once("aclg_set.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$check_auth = check_authority($session, 46, $fname);
if ($check_auth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// お知らせ管理権限の取得
$news_admin_auth = check_authority($session, 24, $fname);

// ページ番号の設定
if ($page == "") {
    $page = 1;
}

// データベースに接続
$con = connect2db($fname);
// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"], $_POST, $con);

// ログインユーザの職員IDを取得
$emp_id = get_emp_id($con, $session, $fname);

// 組織名の取得
$arr_class_name = get_class_name_array($con, $fname);
$class_called = $arr_class_name[0];

// 一般ユーザ登録許可フラグを取得
$everyone_flg = get_everyone_flg($con, $fname);

// カテゴリを取得
//$category_list = get_category_list_from_db($con, $fname, 4, $emp_id);
$category_list_db = get_category_list_from_db($con, $fname, 4, $emp_id);
$category_list = array();
foreach ($category_list_db as $category_id => $category_name) {
    $category_list[$category_id] = $category_name["newscate_name"];
}

// 一覧表示の件数を取得
$rows_per_page = get_rows_per_page($con, $fname);

// お知らせ区分リストを取得
$notice2_list = get_notice2_list($con, $fname);

// 前回指定したカテゴリ
if ($sele_newscate == "") {
    $sele_newscate = get_last_newscate($con, $fname, $emp_id);
}
// 前回指定したステータス
if ($sele_stat == "") {
    $default = get_default_read_status($con, $fname);
    $sele_stat = get_last_status_news($con, $fname, $emp_id, $default);
}
// マイページの未読・既読選択
$read_flg = get_mypage_read_flg($con, $fname);

// お知らせ返信機能設定を取得
$conf = new Cmx_SystemConfig();
$setting_henshin = $conf->get('setting_henshin');

//オプションから引用した設定が"t"ではなかった場合に、"f"を設定する。
if ($setting_henshin != "t") {
    $setting_henshin = "f";
}

// 前回指定カテゴリ、ステータスを更新
update_newsempoption($con, $fname, $emp_id, $sele_newscate, $sele_stat);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix お知らせ | 全館</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="newsuser_menu.php?session=<?php echo($session); ?>"><img src="img/icon/b35.gif" width="32" height="32" border="0" alt="お知らせ・回覧板"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="newsuser_menu.php?session=<?php echo($session); ?>"><b>お知らせ・回覧板</b></a></font></td>
<?php if ($news_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="news_menu.php?session=<?php echo($session); ?>&news=1"><b>管理画面へ</b></a></font></td>
<?php } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="120" align="center" bgcolor="#5279a5"><a href="newsuser_menu.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>一覧</b></font></a></td>
<?php if ($everyone_flg == "t") { ?>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="newsuser_register.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録</font></a></td>
<?php } ?>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="newsuser_search.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">検索</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="newsuser_option.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション</font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<form name="mainform">
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td style="padding-right:30px;">
<?php disp_sele_newscate($category_list,$sele_newscate,false); ?>
</td>
<td style="padding-right:30px;">
<?php disp_sele_stat($sele_stat,$read_flg); ?>
</td>
<?php if ($everyone_flg == "t") { ?>
<td style="padding-right:30px;"><a href="newsuser_list.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">自己登録分</font></a></td>
<?php } ?>
</tr>
</table>
<input type="hidden" name="session" value="<?php echo($session); ?>">
</form>
    <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
        <tr height="22" bgcolor="#f6f9ff">
            <td align="center" width="70"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録日</font></td>
            <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">対象カテゴリ</font></td>
            <td align="center" width="70"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">通達区分</font></td>
            <?php if (!empty($notice2_list)) { ?>
                <td align="center" width="70"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ<br>区分</font></td>
            <?php } ?>
            <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイトル</font></td>
            <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発信者</font></td>
            <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">確認</font></td>
            <td align="center" width="175"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">掲載期間<br />（掲載期間：ログイン画面）</font></td>
            <td align="center" width="70"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">参照者一覧</font></td>
            <?php if ($setting_henshin == 't') { ?>
                <td align="center" width="55"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">返信一覧</font></td>
            <?php } ?>

        </tr>
        <?php show_news($con, $page, $session, $fname, $sele_newscate, $sele_stat, $rows_per_page, $notice2_list, $setting_henshin); ?>
    </table>
<?php show_news_page_list($con, $page, $session, $fname, $sele_newscate, $sele_stat, $rows_per_page); ?>
</body>
</html>
<?php pg_close($con);