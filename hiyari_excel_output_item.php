<?php
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");
require_once("about_postgres.php");
require_once("hiyari_common.ini");
require_once("hiyari_mail.ini");
require_once("hiyari_report_class.php");
require_once('Cmx.php');
require_once('Cmx/View/Smarty.php');

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
} 

//====================================
// 権限チェック
//====================================
$auth_chk = check_authority($session, 47, $fname);
if ($auth_chk == '0') {
    showLoginPage();
    exit;
}

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//==================================================
//デザイン（1=旧デザイン、2=新デザイン）
//==================================================
$sysConf = new Cmx_SystemConfig();
$design_mode = $sysConf->get('fantol.design.mode');

//========================================
// ログインユーザの職員IDを取得
//========================================
$emp_id = get_emp_id($con, $session, $fname);

//========================================
//最大ダウンロード件数を更新
//========================================
if ($mode == "set_max_report_dl_cnt") {
    set_max_report_dl_cnt($con, $fname, $max_report_dl_cnt);
}

//========================================
//報告者氏名を表示するフラグ
//========================================
$excel_output = get_anonymous_excel_output($con,$fname);


//========================================
//EXCEL出力項目取得処理
//========================================
$flags = get_excel_output_item($con, $fname, $emp_id);

//========================================
//カテゴリグループ項目情報の取得
//========================================
$rep_obj = new hiyari_report_class($con, $fname);
$cate_list = $rep_obj->get_report_category_list($session);

$cate_list_add = array(
    'cate_code' => 9,
    'cate_name' => '報告者',
);
if ($excel_output) {
    $cate_list_add['grp_list'][] = array(
        'grp_code' => '9000',
        'grp_name' => '報告者氏名',
        'rel_grp' => null,
    );
}
$cate_list_add['grp_list'][] = array(
    'grp_code' => '9100',
    'grp_name' => '報告者職種',
    'rel_grp' => null,
);
$cate_list_add['grp_list'][] = array(
    'grp_code' => '9200',
    'grp_name' => '報告部署',
    'rel_grp' => null,
);
array_unshift($cate_list, $cate_list_add);

$cate_list_no = array(
		'cate_code' => 11,
		'cate_name' => '事案番号',
);
$cate_list_no['grp_list'][] = array(
		'grp_code' => '1111',
		'grp_name' => '主報告事案番号',
		'rel_grp' => null,
);
array_unshift($cate_list, $cate_list_no);

$cate_list_wk = array();
foreach($cate_list as $cate) {
    switch ($cate['cate_code']){
    case "4":
        $grp_wk = array(
            'cate_code' => $cate['cate_code'],
            'cate_name' => $cate['cate_name'],
        );
        foreach ($cate['grp_list'] as $grp) {
            // アセスメント・患者の状態
            if ($grp['grp_code'] == "140") {
                $grp_wk['grp_list'][] = array(
                    'grp_code' => '140',
                    'grp_name' => 'アセスメント・患者の状態',
                    'rel_grp'  => null,
                );
                $grp_wk['grp_list'][] = array(
                    'grp_code' => '15003',
                    'grp_name' => '転倒・転落アセスメントスコア',
                    'rel_grp'  => null,
                );
                $grp_wk['grp_list'][] = array(
                    'grp_code' => '15010',
                    'grp_name' => '転倒・転落の危険度',
                    'rel_grp'  => null,
                );
                $grp_wk['grp_list'][] = array(
                    'grp_code' => '15006',
                    'grp_name' => 'リスク回避器具',
                    'rel_grp'  => null,
                );
                $grp_wk['grp_list'][] = array(
                    'grp_code' => '15007',
                    'grp_name' => '拘束用具',
                    'rel_grp'  => null,
                );
                $grp_wk['grp_list'][] = array(
                    'grp_code' => '15008',
                    'grp_name' => '身体拘束行為',
                    'rel_grp'  => null,
                );
                $grp_wk['grp_list'][] = array(
                    'grp_code' => '15005',
                    'grp_name' => '離床センサー',
                    'rel_grp'  => null,
                );
                $grp_wk['grp_list'][] = array(
                    'grp_code' => '15020',
                    'grp_name' => 'ルート関連（自己抜去）',
                    'rel_grp'  => null,
                );
                $grp_wk['grp_list'][] = array(
                    'grp_code' => '15030',
                    'grp_name' => '日常生活機能評価',
                    'rel_grp'  => null,
                );
                $grp_wk['grp_list'][] = array(
                    'grp_code' => '15040',
                    'grp_name' => '日常生活自立度(認知症高齢者)',
                    'rel_grp'  => null,
                );
                $grp_wk['grp_list'][] = array(
                    'grp_code' => '15050',
                    'grp_name' => '日常生活自立度(寝たきり度)',
                    'rel_grp'  => null,
                );
                $grp_wk['grp_list'][] = array(
                    'grp_code' => '15060',
                    'grp_name' => '機能的自立度評価法(FIM)',
                    'rel_grp'  => null,
                );
                $grp_wk['grp_list'][] = array(
                    'grp_code' => '15070',
                    'grp_name' => 'バーサルインデックス',
                    'rel_grp'  => null,
                );
                $grp_wk['grp_list'][] = array(
                    'grp_code' => '15080',
                    'grp_name' => '皮膚トラブルレベル',
                    'rel_grp'  => null,
                );
            }
            else if ($grp['grp_code'] < "140" or $grp['grp_code'] > "190") {
                //概要が出たら、名前を変更
                if($grp['grp_code'] == 900) {
                    $grp["grp_name"] = '概要・場面・内容(2010年改訂)';
                }
                $grp_wk['grp_list'][] = $grp;
            }
        }
        $cate_list_wk[] = $grp_wk;
        break;

    case 7:
    case 8:
        $grp_wk = array(
            'cate_code' => $cate['cate_code'],
            'cate_name' => $cate['cate_name'],
        );
        foreach ($cate['grp_list'] as $grp) {
            //出力対象外
            if ($grp["grp_code"] == 605     //発生要因詳細
             || $grp["grp_code"] == 1130    //今期のテーマ
            ) {
                continue;
            }
            $grp_wk['grp_list'][] = $grp;
        }
        $cate_list_wk[] = $grp_wk;
        break;

    default:
        $cate_list_wk[] = $cate;
    }
}
$cate_list = $cate_list_wk;


//==============================
//表示
//==============================
$smarty = new Cmx_View();

$smarty->assign("INCIDENT_TITLE", $INCIDENT_TITLE);
$smarty->assign("session", $session);
$smarty->assign("title", 'EXCEL出力');

$smarty->assign("report_id_list", $report_id_list);
$smarty->assign("emp_id", $emp_id);

//最大ダウンロード件数
$max_report_dl_cnt = get_max_report_dl_cnt($con, $fname);
$smarty->assign("max_report_dl_cnt", $max_report_dl_cnt);

// ダウンロード件数情報
$arr_report_ids = split(",", $report_id_list);
$report_total_cnt = count($arr_report_ids);

if ( $max_report_dl_cnt != 0 ) {
    $row = floor($report_total_cnt / $max_report_dl_cnt);
    $rest = $report_total_cnt % $max_report_dl_cnt;

    if ( $rest > 0 ) {
        $row++;
    }

    $arr_start_end = array();

    for ( $i = 0; $i < $row; $i++ ) {
        $start = $i * $max_report_dl_cnt + 1;

        if ( $i == ($row - 1) ) {
            if ( $rest > 0 ) {
                $end   = $start + $rest - 1;
            }
            else {
                $end = ($i + 1) * $max_report_dl_cnt;
            }
        }
        else {
            $end   = ($i + 1) * $max_report_dl_cnt;
        }
        $arr_start_end[] = array("start" => $start, "end" => $end);
    }
}
else {
    $row = 1;
    $arr_start_end = array();
    $arr_start_end[] = array("start" => 1, "end" => $report_total_cnt);
}
$smarty->assign("arr_start_end", $arr_start_end);

//グループに対する出力列数
$grp_excel_output_cnt = get_grp_excel_output_cnt($con, $fname);
$smarty->assign("grp_excel_output_cnt", $grp_excel_output_cnt);

//項目情報
$smarty->assign("cate_list", $cate_list);

//出力項目フラグ
$smarty->assign("flags", $flags);

if ($design_mode == 1){
    $smarty->display("hiyari_excel_output_item1.tpl");
}
else{
    $smarty->display("hiyari_excel_output_item2.tpl");
}

pg_close($con);

//==============================================================================
//内部関数
//==============================================================================
function get_excel_output_item($con, $fname, $emp_id) {
    $arr = array();

    $sql = "select grp_code,easy_item_code from inci_excel_output_item";
    $cond = "where emp_id = '$emp_id'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if($sel == 0){
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }

    while($row = pg_fetch_array($sel))
    {
        $grp_item_code = $row["grp_code"];
        if ($row["easy_item_code"] != ""){
            $grp_item_code .= "_" . $row["easy_item_code"];
        }
        $arr[] = $grp_item_code;
    }

    return $arr;
}


/**
 * EXCEL一覧出力の指定グループに対する出力項目を得るSQLを返却します。
 *
 * @return string ＳＱＬ文
 */
function get_excel_output_item_sql()
{
    $sql = "
SELECT
    CASE
        WHEN (grp_code > 3000 AND grp_code < 3500) THEN grp_code/10*10
        WHEN (grp_code >= 400 AND grp_code < 500) THEN 400
        WHEN (grp_code >= 4000 AND grp_code < 4049) THEN 4000
        ELSE grp_code
    END AS target_grp_code,
    grp_code,
    easy_item_code
FROM inci_easyinput_item_mst
WHERE NOT (grp_code = 100 and easy_item_code = 10)
AND NOT (grp_code = 100 and easy_item_code = 20)
AND NOT (grp_code = 100 and easy_item_code = 30)
AND NOT (grp_code = 95)
AND NOT (grp_code = 605)
"; //"
    return $sql;
}

/**
 * グループに対してEXCEL出力列数はいくらかを取得します。
 *
 * @param object $con DBコネクション
 * @param string $fname 画面名
 * @return array キー:グループコード／値：列数
 */
function get_grp_excel_output_cnt($con, $fname)
{
    $sql  = " select target_grp_code as grp_code,count(*) as cnt from";
    $sql .= " (";
    $sql .= get_excel_output_item_sql();
    $sql .= " ) i";
    $sql .= " group by target_grp_code";

    $sel = select_from_table($con, $sql, "", $fname);
    if($sel == 0){
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }

    $cnt_list = null;
    $db_data = pg_fetch_all($sel);
    foreach($db_data as $db_record)
    {
        $cnt_list[  $db_record["grp_code"]  ] = $db_record["cnt"];
    }

    $cnt_list["9000"] = 1;
    $cnt_list["9100"] = 1;
    $cnt_list["9200"] = 1;

    $cnt_list["15003"] = 1;   //転倒・アセスメントスコア 20100107
    $cnt_list["15005"] = 1;   //離床センサー 20100107
    $cnt_list["15006"] = 1;   //リスク回避用具
    $cnt_list["15007"] = 1;   //拘束用具 20100107
    $cnt_list["15008"] = 1;   //身体拘束行為
    $cnt_list["15010"] = 1;   //転倒・転落の危険度 20090304
    $cnt_list["15020"] = 1;   //ルート関連（自己抜去） 20090304
    $cnt_list["15030"] = 1;   //日常生活機能評価    20100315
    $cnt_list["15040"] = 1;   //日常生活自立度(認知症高齢者)
    $cnt_list["15050"] = 1;   //日常生活自立度(寝たきり度)
    $cnt_list["15060"] = 1;   //機能的自立度評価法(FIM)
    $cnt_list["15070"] = 1;   //バーサルインデックス
    $cnt_list["15080"] = 1;   //皮膚トラブルレベル

    return $cnt_list;
}


function get_max_report_dl_cnt($con, $fname)
{
    $sql = "select max_report_dl_cnt from inci_excel_output_info";
    $sel = select_from_table($con, $sql, "", $fname);
    if($sel == 0){
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }
    $max_report_dl_cnt = pg_fetch_result($sel,0,"max_report_dl_cnt");
    return $max_report_dl_cnt;
}


function set_max_report_dl_cnt($con, $fname, $max_report_dl_cnt)
{
    $sql = "update inci_excel_output_info set";
    $set = array("max_report_dl_cnt");
    $setvalue = array($max_report_dl_cnt);
    $cond = "";
    $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
    if ($upd == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}