<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>スケジュール｜行先更新</title>
<? require("about_authority.php"); ?>
<? require("about_session.php"); ?>
<? require("about_postgres.php"); ?>
<?
$fname = $PHP_SELF;

$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$ward = check_authority($session,13,$fname);
if($ward == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$con = connect2db($fname);

// スケジュールの初期画面を取得
$sql = "select schedule1_default from option";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$schedule1_default = pg_fetch_result($sel, 0, "schedule1_default");
switch ($schedule1_default) {
case "1":
	$default_url = "schedule_menu.php";
	break;
case "2":
	$default_url = "schedule_week_menu.php";
	break;
case "3":
	$default_url = "schedule_month_menu.php";
	break;
}

// 行先情報を取得
$sql = "select place_name from scheduleplace";
$cond = "where place_id = '$place_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$place_name = pg_fetch_result($sel, 0, "place_name");
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<? echo($default_url); ?>?session=<? echo($session); ?>"><img src="img/icon/b02.gif" width="32" height="32" border="0" alt="スケジュール"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<? echo($default_url); ?>?session=<? echo($session); ?>"><b>スケジュール</b></a> &gt; <a href="schedule_place_list.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="<? echo($default_url); ?>?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="100" align="center" bgcolor="#bdd1e7"><a href="schedule_place_list.php?session=<? echo($session); ?>&page=<? echo($page); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">行先一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#5279a5"><a href="schedule_place_update.php?session=<? echo($session); ?>&place_id=<? echo($place_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>行先更新</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="schedule_type_update.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">種別</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="schedule_comgroup.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">共通グループ</font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<form action="schedule_place_update_exe.php" method="post">
<table width="400" border="0" cellspacing="0" cellpadding="1">
<tr>
<td width="120" height="30" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">行先名：</font></td>
<td width="280"><input type="text" name="place_name" value="<? echo($place_name); ?>" size="25" maxlength="40" style="ime-mode: active;">&nbsp;<input type="submit" value="更新"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="page" value="<? echo($page); ?>">
<input type="hidden" name="place_id" value="<? echo($place_id); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
