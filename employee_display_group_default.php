<?
require("about_session.php");
require("about_authority.php");
require_once('Cmx.php');
require_once('Cmx/Model/SystemConfig.php');

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 19, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// system_config
$conf = new Cmx_SystemConfig();

// 表示設定をINSERT
$ret_val = $conf->set("default_set.disp_group_id",$group_id);

// データベース接続を閉じる
pg_close($con);
//XX mysql_close($con_mysql);

// 表示グループ画面をリフレッシュ
echo("<script type=\"text/javascript\">location.href = 'employee_display_group.php?session=$session';</script>");
?>
