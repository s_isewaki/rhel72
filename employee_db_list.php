<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 職員登録 | 職員登録用DB操作</title>
<?
require("about_session.php");
require("about_authority.php");

define("ROW_COUNT", 10);

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 職員登録権限のチェック
$checkauth = check_authority($session, 19, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
?>
<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript">
function copyData(personal_id, lt_nm, ft_nm, kn_lt_nm, kn_ft_nm, login_id, login_pass, sex, birth_y, birth_m, birth_d, join_y, join_m, join_d) {
	opener.document.employee.personal_id.value = personal_id;
	opener.document.employee.lt_nm.value = lt_nm;
	opener.document.employee.ft_nm.value = ft_nm;
	opener.document.employee.lt_kana_nm.value = kn_lt_nm;
	opener.document.employee.ft_kana_nm.value = kn_ft_nm;
	opener.document.employee.id.value = login_id;
	opener.document.employee.pass.value = login_pass;

	switch (sex) {
	case '1':
		opener.document.employee.sex[0].checked = true;
		break;
	case '2':
		opener.document.employee.sex[1].checked = true;
		break;
	}

	for (var i = 0, j = opener.document.employee.birth_yr.length; i < j; i++) {
		if (opener.document.employee.birth_yr.options[i].value == birth_y) {
			opener.document.employee.birth_yr.options[i].selected = true;
			break;
		}
	}
	for (var i = 0, j = opener.document.employee.birth_mon.length; i < j; i++) {
		if (opener.document.employee.birth_mon.options[i].value == birth_m) {
			opener.document.employee.birth_mon.options[i].selected = true;
			break;
		}
	}
	for (var i = 0, j = opener.document.employee.birth_day.length; i < j; i++) {
		if (opener.document.employee.birth_day.options[i].value == birth_d) {
			opener.document.employee.birth_day.options[i].selected = true;
			break;
		}
	}

	for (var i = 0, j = opener.document.employee.entry_yr.length; i < j; i++) {
		if (opener.document.employee.entry_yr.options[i].value == join_y) {
			opener.document.employee.entry_yr.options[i].selected = true;
			break;
		}
	}
	for (var i = 0, j = opener.document.employee.entry_mon.length; i < j; i++) {
		if (opener.document.employee.entry_mon.options[i].value == join_m) {
			opener.document.employee.entry_mon.options[i].selected = true;
			break;
		}
	}
	for (var i = 0, j = opener.document.employee.entry_day.length; i < j; i++) {
		if (opener.document.employee.entry_day.options[i].value == join_d) {
			opener.document.employee.entry_day.options[i].selected = true;
			break;
		}
	}

	var dellabel = opener.document.getElementById('dellabel');
	var cur_txt = dellabel.firstChild;
	if (cur_txt != null) {
		dellabel.removeChild(cur_txt);
	}
	var nm = lt_nm + ' ' + ft_nm;
	var txt = opener.document.createTextNode(personal_id + ' ' + nm);
	dellabel.appendChild(txt);

	opener.document.employee.del.checked = true;
	opener.document.employee.del_id.value = personal_id;
	opener.document.employee.del_nm.value = nm;

	var delrow = opener.document.getElementById('delrow');
	delrow.style.display = '';

	self.close();
}

function searchEmployee() {
	document.mainform.mode.value = 'search';
	document.mainform.submit();
}

function displayAll() {
	document.mainform.mode.value = 'all';
	document.mainform.submit();
}

function deleteEmployees() {
	var checked = false;
	if (document.mainform.elements['emp_pids[]']) {
		if (document.mainform.elements['emp_pids[]'].length) {
			for (var i = 0, j = document.mainform.elements['emp_pids[]'].length; i < j; i++) {
				if (document.mainform.elements['emp_pids[]'][i].checked) {
					checked = true;
					break;
				}
			}
		} else {
			checked = document.mainform.elements['emp_pids[]'].checked;
		}
	}
	if (!checked) {
		alert('削除対象が選択されていません。');
		return;
	}

	if (confirm('削除します。よろしいですか？')) {
		document.mainform.action = 'employee_db_delete.php';
		document.mainform.submit();
	}
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="employee_info_menu.php?session=<? echo($session); ?>"><img src="img/icon/b26.gif" width="32" height="32" border="0" alt="職員登録"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="employee_info_menu.php?session=<? echo($session); ?>"><b>職員登録</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_info_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">検索</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">新規登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_bulk_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="140" align="center" bgcolor="#5279a5"><a href="employee_db_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>職員登録用DB操作</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_auth_group.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限グループ</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_display_group.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示グループ</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_menu_group.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メニューグループ</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_auth_bulk_setting.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括設定</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_auth_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限管理</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_field_option.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">項目管理</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<form name="mainform" action="employee_db_list.php" method="post" onsubmit="if (document.mainform.mode.value == '') {document.mainform.mode.value = 'search';}">
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="600" border="0" cellspacing="0" cellpadding="1">
<tr height="22" bgcolor="#f6f9ff">
<td nowrap style="padding-right:5px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員氏名：<input type="text" name="srch_name" value="<? echo($srch_name); ?>" style="ime-mode: active;"> <input type="button" value="検索" onclick="searchEmployee();"></font></td>
<td width="100%" style="padding-left:5px;border-left:#5279a5 solid 1px;"><input type="button" value="全表示" onclick="displayAll();"></td>
<td align="right"><input type="button" value="削除" onclick="deleteEmployees();"></td>
</tr>
</table>
<? show_emp_list($mode, $srch_name, $page, $session, $fname); ?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="mode" value="<? echo($mode); ?>">
<input type="hidden" name="page" value="<? echo($page); ?>">
</form>
</td>
</tr>
</table>
</body>
</html>
<?
function show_emp_list($mode, $srch_name, $page, $session, $fname) {

	// ボタンが押されていない場合、何もせず復帰
	if ($mode == "") {
		return;
	}

	// 職員氏名なしで検索ボタンが押された場合、何もせず復帰
	if ($mode == "search" && $srch_name == "") {
		return;
	}

	// データベースに接続
	$con = connect2db($fname);

	// 職員登録用DBを検索
	$sql = "select * from emptmp";
	$cond = "";
	if ($mode == "search") {
		$cond .= "where emp_lt_nm || emp_ft_nm like '%$srch_name%' or emp_kn_lt_nm || emp_kn_ft_nm like '%$srch_name%'";
	}
	if ($page == "") {$page = 1;}
	$cond .= " order by emp_personal_id limit " . ROW_COUNT . " offset " . (ROW_COUNT * ($page - 1));
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// 一覧表示
	echo("<img src=\"img/spacer.gif\" width=\"1\" height=\"5\" alt=\"\"><br>\n");
	echo("<table width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"list\">\n");
	echo("<tr bgcolor=\"#f6f9ff\">\n");
	echo("<td width=\"40\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">削除</font></td>\n");
	echo("<td width=\"120\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">職員ID</font></td>\n");
	echo("<td width=\"140\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">職員氏名</font></td>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">本登録状況</font></td>\n");
	echo("</tr>\n");
	while ($row = pg_fetch_array($sel)) {
		$emp_personal_id = $row["emp_personal_id"];
		$emp_lt_nm = $row["emp_lt_nm"];
		$emp_ft_nm = $row["emp_ft_nm"];
		$emp_kn_lt_nm = $row["emp_kn_lt_nm"];
		$emp_kn_ft_nm = $row["emp_kn_ft_nm"];
		$emp_login_id = $row["emp_login_id"];
		$emp_login_pass = $row["emp_login_pass"];
		$emp_sex = $row["emp_sex"];
		$status = ($row["registered"] == "t") ? "登録済み" : "未登録";

		echo("<tr>\n");
		echo("<td align=\"center\"><input type=\"checkbox\" name=\"emp_pids[]\" value=\"$emp_personal_id\"></td>\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$emp_personal_id</font></td>\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$emp_lt_nm $emp_ft_nm</font></td>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$status</font></td>\n");
		echo("</tr>\n");
	}
	echo("</table>\n");

	// 検索条件に合致するレコード数を取得
	$sql = "select count(*) from emptmp";
	$cond = "";
	if ($mode == "search") {
		$cond .= "where emp_lt_nm || emp_ft_nm like '%$srch_name%' or emp_kn_lt_nm || emp_kn_ft_nm like '%$srch_name%'";
	}
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$record_count = intval(pg_fetch_result($sel, 0, 0));
	$total_page = ceil($record_count / ROW_COUNT);

	// データベース接続を閉じる
	pg_close($con);

	// ページング処理が不要であれば復帰
	if ($total_page < 2) {
		return;
	}

	// ページ切替リンクを表示
	echo("<img src=\"img/spacer.gif\" width=\"1\" height=\"2\" alt=\"\"><br>\n");
	echo("<div style=\"width:600px;text-align:right;\">\n");
	for ($i = 1; $i <= $total_page; $i++) {
		if ($i != $page) {
			echo("<a href=\"$fname?session=$session&srch_name=" . urlencode($srch_name) . "&mode=$mode&page=$i\">$i</a>\n");
		} else {
			echo("[$i]\n");
		}
	}
	echo("</div>\n");
}
?>
