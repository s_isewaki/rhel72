<?php

require_once('Cmx.php');

class Cmx_Shift_Date_Cours
{

    var $_base_year;
    var $_base_date;
    var $_base_time;
    var $_base_count = 13;

    // コンストラクタ
    function Cmx_Shift_Date_Cours($date, $year, $count = 13)
    {
        $this->_base($date, $year, $count);
    }

    function _base($date, $year, $count = 13)
    {
        $this->_base_year = $year;
        $this->_base_date = $date;
        $this->_base_time = strtotime($date);
        $this->_base_count = $count;
    }

    // クール開始日(秒)
    function _start_time($year, $cours)
    {
        $diff = ($year - $this->_base_year) * $this->_base_count;
        $diff = $diff + $cours - 1;
        $diff = $diff * 28 * 86400;
        return $this->_base_time + $diff;
    }

    // クール開始日
    function start_date($year, $cours)
    {
        return date('Y-m-d', $this->_start_time($year, $cours));
    }

    // クール開始日(Ymd)
    function start_ymd($year, $cours)
    {
        return date('Ymd', $this->_start_time($year, $cours));
    }

    // クール終了日(秒)
    function _end_time($year, $cours)
    {
        $diff = ($year - $this->_base_year) * $this->_base_count;
        $diff = $diff + $cours;
        $diff = ($diff * 28 - 1) * 86400;
        return $this->_base_time + $diff;
    }

    // クール終了日
    function end_date($year, $cours)
    {
        return date('Y-m-d', $this->_end_time($year, $cours));
    }

    // クール終了日(Ymd)
    function end_ymd($year, $cours)
    {
        return date('Ymd', $this->_end_time($year, $cours));
    }

    // クール日付リスト
    function dates($year, $cours)
    {
        $time = $this->_start_time($year, $cours);

        $dates = array();
        foreach (range(1, 28) as $i) {
            $dates[] = date('Y-m-d', $time);
            $time += 86400;
        }
        return $dates;
    }

    // 日付からクールを割り出す
    function cours($date)
    {
        $cours_time = strtotime($date);
        $diff_time = $cours_time - $this->_base_time;

        if ($diff_time >= 0) {
            $diff = intval($diff_time / (28 * 86400));
            $year = intval($diff / $this->_base_count) + $this->_base_year;
            $cours = $diff % $this->_base_count + 1;
        }
        else {
            $diff = intval(($diff_time + 1) / (28 * 86400));
            $year = intval($diff / $this->_base_count) + $this->_base_year - 1;
            $cours = $this->_base_count + ($diff % $this->_base_count);
        }
        return array($year, $cours);
    }

    function months($date)
    {
        return $this->cours($date);
    }

    // 前後クール計算
    function cours_add($year, $cours, $add)
    {
        $cours += $add;
        if ($cours > $this->_base_count) {
            $year += intval($cours / $this->_base_count);
            $cours = $cours % $this->_base_count;
        }
        else if ($cours < 1) {
            $year += intval($cours / $this->_base_count) - 1;
            $cours = $this->_base_count + ($cours % $this->_base_count);
        }
        return array($year, $cours);
    }

    function months_add($year, $cours, $add)
    {
        return $this->cours_add($year, $cours, $add);
    }

    /**
     * 年クールレンジリスト
     * @param type $start
     * @param type $end
     */
    function cours_range($start, $end)
    {
        list($y, $m) = $this->cours($start);
        list($ey, $em) = $this->cours($end);

        $range = array();
        while ((int)$y !== (int)$ey || (int)$m !== (int)$em) {
            $range[] = array($y, $m);
            list($y, $m) = $this->cours_add($y, $m, 1);
        }
        $range[] = array($ey, $em);
        return $range;
    }

    function months_range($start, $end)
    {
        return $this->cours_range($start, $end);
    }

    /**
     * 2015年4月より
     * ・2015/4/1の含まれるクールを2015年1クールとする
     * ・それまでの2015年1〜3クールは、2014年14〜16クールとする
     * の対応
     * @param type $year
     * @param type $cours
     * @return type
     */
    function view_cours($year, $cours)
    {
        if ((int)$year > 2015 || (int)$year === 2015 && (int)$cours >= 4) {
            if ((int)$cours <= 3) {
                return array($year - 1, $cours + 10);
            }
            else {
                return array($year, $cours - 3);
            }
        }
        else if ((int)$year === 2015 && (int)$cours >= 1) {
            return array(2014, $cours + 13);
        }
        else {
            return array($year, $cours);
        }
    }

    /**
     * 2015年4月より
     * ・2015年1クールのパラメータを渡された時に従来の2015年4クールとして計算できるよう値を返す
     * ・2014年14クールのパラメータを渡された時に従来の2015年1クールとして計算できるよう値を返す
     * @param type $year
     * @param type $cours
     * @return type
     */
    function convert_old_cours($year, $cours)
    {
        if ((int)$year > 2015 || (int)$year === 2015) {
            if ((int)$cours > 10) {
                return array($year + 1, $cours - 10);
        }
            else {
                return array($year, $cours + 3);
            }
        }
        else if ((int)$year === 2014 && (int)$cours > 13) {
            return array(2015, $cours - 13);
        }
        else {
            return array($year, $cours);
        }
    }

}
