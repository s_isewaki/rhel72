<?php

require_once('Cmx.php');

class Cmx_Shift_Date_Months
{

    var $_base_start_month_flg;
    var $_base_start_day;

    // コンストラクタ
    function Cmx_Shift_Date_Months($start_month_flg, $start_day)
    {
        if ($start_month_flg === '' or is_null($start_month_flg)) {
            $start_month_flg = 1;
        }
        if (!$start_day) {
            $start_day = 1;
        }
        $this->_base($start_month_flg, $start_day);
    }

    function _base($start_month_flg, $start_day)
    {
        $this->_base_start_month_flg = $start_month_flg;
        $this->_base_start_day = $start_day;
    }

    // 月 開始日(秒)
    function _start_time($year, $month)
    {
        return mktime(0, 0, 0, $month + $this->_base_start_month_flg - 1, $this->_base_start_day, $year);
    }

    // 月 開始日
    function start_date($year, $month)
    {
        return date('Y-m-d', $this->_start_time($year, $month));
    }

    // 月 開始日(Ymd)
    function start_ymd($year, $month)
    {
        return date('Ymd', $this->_start_time($year, $month));
    }

    // 月 終了日(秒)
    function _end_time($year, $month)
    {
        return mktime(0, 0, 0, $month + $this->_base_start_month_flg, $this->_base_start_day - 1, $year);
    }

    // 月 終了日
    function end_date($year, $month)
    {
        return date('Y-m-d', $this->_end_time($year, $month));
    }

    // 月 終了日(Ymd)
    function end_ymd($year, $month)
    {
        return date('Ymd', $this->_end_time($year, $month));
    }

    // 月 日付リスト
    function dates($year, $month)
    {
        $time = $this->_start_time($year, $month);
        $end = $this->_end_time($year, $month);

        $dates = array();
        foreach (range(1, 31) as $i) {
            $dates[] = date('Y-m-d', $time);
            $time += 86400;
            if ($time > $end) {
                break;
            }
        }
        return $dates;
    }

    // 日付から月を割り出す
    function months($date)
    {
        list($y, $m, $d) = explode('-', $date);

        if ($d < $this->_base_start_day and $this->_base_start_month_flg) {
            $time = mktime(0, 0, 0, $m - 1, 1, $y);
        }
        else if ($d >= $this->_base_start_day and ! $this->_base_start_month_flg) {
            $time = mktime(0, 0, 0, $m + 1, 1, $y);
        }
        else {
            $time = mktime(0, 0, 0, $m, 1, $y);
        }
        return array(date('Y', $time), date('m', $time));
    }

    function cours($date)
    {
        return $this->months($date);
    }

    // 前後月計算
    function months_add($year, $month, $add)
    {
        $month += $add;
        if ($month > 12) {
            $year += intval($month / 12);
            $month = $month % 12;
        }
        else if ($month < 1) {
            $year += intval($month / 12) - 1;
            $month = 12 + ($month % 12);
        }
        return array($year, $month);
    }

    function cours_add($year, $month, $add)
    {
        return $this->months_add($year, $month, $add);
    }

    /**
     * 年月レンジリスト
     * @param type $start
     * @param type $end
     */
    function months_range($start, $end)
    {
        list($y, $m) = $this->months($start);
        list($ey, $em) = $this->months($end);

        $range = array();
        while ((int)$y !== (int)$ey || (int)$m !== (int)$em) {
            $range[] = array($y, $m);
            list($y, $m) = $this->months_add($y, $m, 1);
        }
        $range[] = array($ey, $em);
        return $range;
    }

    function cours_range($start, $end)
    {
        return $this->months_range($start, $end);
    }

    function view_cours($year, $cours)
    {
        return array($year, $cours);
    }

    function convert_old_cours($year, $cours)
    {
        return array($year, $cours);
    }

}
