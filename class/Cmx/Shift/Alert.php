<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';

/*
 * 勤務表：電気錠監視クラス
 */

class Cmx_Shift_Alert extends Model
{

    var $alived_count;
    var $illigal_data_count;

    /**
     * コンストラクタ
     */
    public function __construct()
    {
        parent::connectDB();
    }

    /**
     * 接続が切れているか
     * @return bool
     */
    public function is_dead() {
        $this->alived_count = $this->db->extended->getOne(
            "SELECT COUNT(*) FROM duty_shift_log WHERE timestamp > (CURRENT_TIMESTAMP + '-2 hour') AND message='alived';"
        );
        return $this->alived_count === '0' ? true : false;
    }

    /**
     * 不正なデータか
     * @return bool
     */
    public function is_illigal_data() {
        $this->illigal_data_count = $this->db->extended->getOne(
            "SELECT COUNT(*) FROM duty_shift_log WHERE timestamp > (CURRENT_TIMESTAMP + '-2 hour') AND message = 'illegal data';"
        );
        return $this->illigal_data_count > '0' ? true : false;
    }
}
