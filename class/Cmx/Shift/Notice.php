<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'Cmx/Model/SystemConfig.php';

/*
 * メール通知
 */

class Cmx_Shift_Notice extends Model
{

    /**
     * コンストラクタ
     */
    public function __construct()
    {
        parent::connectDB();
    }

    /**
     * Fromアドレス(Admin)
     * @return type
     */
    private function admin_address()
    {
        require "webmail/config/config.php";
        $mail_id = $this->db->extended->getOne(
            "SELECT CASE WHEN config.site_id = '' OR config.site_id IS NULL THEN login.emp_login_id WHEN config.use_cyrus = false THEN login.emp_login_mail ELSE login.emp_login_mail || '_' || config.site_id END FROM login, config WHERE login.emp_id = ?", null, array('000000000000'), array('text')
        );
        return "{$mail_id}@$domain";
    }

    /**
     * Toアドレス一覧
     * @return type
     */
    private function to_address()
    {
        require "webmail/config/config.php";

        $conf = new Cmx_SystemConfig();
        $employee = unserialize($conf->get('shift.notice.employee'));

        $email = $conf->get('shift.notice.email');
        if (!empty($email)) {
            $emails = unserialize($email);
        }
        else {
            $emails = array();
        }

        foreach ($employee as $emp_id) {
            $mail_id = $this->db->extended->getOne(
                "SELECT CASE WHEN config.site_id = '' OR config.site_id IS NULL THEN login.emp_login_id WHEN config.use_cyrus = false THEN login.emp_login_mail ELSE login.emp_login_mail || '_' || config.site_id END FROM login, config WHERE login.emp_id = ?", null, array($emp_id), array('text')
            );
            $emails[] = "{$mail_id}@$domain";
        }

        return $emails;
    }

    /**
     * テストメール
     */
    public function test()
    {
        $from = $this->admin_address();
        $to_list = $this->to_address();

        $subject = "テストメール: " . date('Y-m-d H:i:s');
        $message = <<<__MES_END__
テストメール
__MES_END__;

        $to_address = implode(",", $to_list);
        mb_send_mail($to_address, $subject, $message, "From: {$from}", "-f{$from}");
    }

    /**
     * メール送信
     */
    public function send($subject, $message)
    {
        $from = $this->admin_address();
        $to_list = $this->to_address();
        $to_address = implode(",", $to_list);
        mb_send_mail($to_address, $subject, $message, "From: {$from}", "-f{$from}");
    }

}
