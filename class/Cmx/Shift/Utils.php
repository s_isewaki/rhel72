<?php

/**
 * Smarty Function
 * 缎坛パタ〖ンプルダウンリスト
 *
 * @param type $params
 * @param type $smarty
 * @return type
 */
function smarty_pattern_pulldown($params, $smarty)
{
    $output = '';
    if (strlen($params['ptn']) !== 4) {
        $ptn_id = sprintf('%02d%02d', $params['ptn'], $params['reason']);
    }
    else {
        $ptn_id = $params['ptn'];
    }

    foreach ($params['array'] as $row) {
        $selected = "";
        if (!empty($params['ptn']) && ($row->ptn_id_4() === $ptn_id || $row->ptn_id_4b() === $ptn_id)) {
            $selected = 'selected';
        }
        $output.= sprintf(
            '<option value="%s" %s>%s</option>', htmlspecialchars($row->ptn_id_4(), ENT_QUOTES, 'euc-jp'), $selected, htmlspecialchars($row->ptn_name(), ENT_QUOTES, 'euc-jp')
        );
    }
    return $output;
}

/**
 * Debug Log
 */
if (!function_exists("shift_log")) {

    function shift_log($mes)
    {
        error_log(date('[Y-m-d H:i:s] ') . "$mes\n", 3, CMX_BASE_DIR . "/log/shift.log");
    }

}

/**
 * 涟泣
 */
function prev_ymd($ymd)
{
    return strftime('%Y%m%d', strtotime($ymd) - 86400);
}

/**
 * 外泣
 */
function next_ymd($ymd)
{
    return strftime('%Y%m%d', strtotime($ymd) + 86400);
}

/**
 * 尸を箕粗に恃垂
 * @param type $min
 * @return type
 */
function min2time($min)
{
    return sprintf('%.2f', $min / 60 + 0.004);
}

/**
 * Smarty min2time modifier plugin
 */
function smarty_modifier_min2time($string, $format = '%b %e, %Y', $default_date = '')
{
    return min2time($string);
}

/**
 * 箕粗プルダウン (0×23)
 */
function pulldown_24hours()
{
    return array(
        0 => '0',
        1 => '1',
        2 => '2',
        3 => '3',
        4 => '4',
        5 => '5',
        6 => '6',
        7 => '7',
        8 => '8',
        9 => '9',
        10 => '10',
        11 => '11',
        12 => '12',
        13 => '13',
        14 => '14',
        15 => '15',
        16 => '16',
        17 => '17',
        18 => '18',
        19 => '19',
        20 => '20',
        21 => '21',
        22 => '22',
        23 => '23',
    );
}

/**
 * 箕粗プルダウン (0×47)
 */
function pulldown_48hours()
{
    return array(
        0 => '0',
        1 => '1',
        2 => '2',
        3 => '3',
        4 => '4',
        5 => '5',
        6 => '6',
        7 => '7',
        8 => '8',
        9 => '9',
        10 => '10',
        11 => '11',
        12 => '12',
        13 => '13',
        14 => '14',
        15 => '15',
        16 => '16',
        17 => '17',
        18 => '18',
        19 => '19',
        20 => '20',
        21 => '21',
        22 => '22',
        23 => '23',
        24 => '外0',
        25 => '外1',
        26 => '外2',
        27 => '外3',
        28 => '外4',
        29 => '外5',
        30 => '外6',
        31 => '外7',
        32 => '外8',
        33 => '外9',
        34 => '外10',
        35 => '外11',
        36 => '外12',
        37 => '外13',
        38 => '外14',
        39 => '外15',
        40 => '外16',
        41 => '外17',
        42 => '外18',
        43 => '外19',
        44 => '外20',
        45 => '外21',
        46 => '外22',
        47 => '外23',
    );
}

/**
 * 缎坛箕粗纷换
 * @param type $start
 * @param type $end
 * @param type $next_flg
 * @param type $rest_min
 * @return type
 */
function workMinutes($start, $end, $next_flg, $rest_min)
{
    $start_min = ((int)substr($start, 0, 2) * 60) + (int)substr($start, 2, 2);
    if ($next_flg) {
        $end_min = (((int)substr($end, 0, 2) + 24) * 60) + (int)substr($end, 2, 2);
    }
    else {
        $end_min = ((int)substr($end, 0, 2) * 60) + (int)substr($end, 2, 2);
    }

    $work_min = $end_min - $start_min;
    return $work_min - $rest_min;
}

/**
 * 蒂菲箕粗纷换
 * @param type $start
 * @param type $end
 * @return type
 */
function restMinutes($start, $end)
{
    $start_min = ((int)substr($start, 0, 2) * 60) + (int)substr($start, 2, 2);
    $end_min = ((int)substr($end, 0, 2) * 60) + (int)substr($end, 2, 2);

    if ($start_min && $start_min >= $end_min) {
        $end_min += 1440;
    }

    return $end_min - $start_min;
}
