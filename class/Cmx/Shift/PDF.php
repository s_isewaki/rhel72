<?php

require_once 'tcpdf/tcpdf.php';

class Shift_PDF extends TCPDF {

    protected $emp;
    protected $print;

    /**
     * コンストラクタ
     */
    public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false) {
        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache);

        $this->SetFont('kozgopromedium', '', 10);
    }

    /**
     * Page header
     */
    public function Header() {
    }

    /**
     * Page footer
     */
    public function Footer() {
    }

    /**
     * Set ヘッダータイトル
     * @param type $title
     */
    public function setHeaderTitle($title) {
        $this->header_title = $title;
    }

    /**
     * Set 職員データ
     * @param type $emp
     */
    public function setEmployee($emp) {
        $this->emp = $emp;
    }

    /**
     * Set 印刷職員データ
     * @param type $emp
     */
    public function setPrinter($emp) {
        $this->print = $emp;
    }

    /**
     * 水平線
     */
    public function hr() {
        $this->Line($this->lMargin - 2, $this->y, $this->w - $this->lMargin + 4, $this->y);
    }

}
