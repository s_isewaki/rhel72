<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';

class Cmx_Shift_Log extends Model
{

    var $function;
    var $ins_sth;
    var $emp_id;

    /**
     * コンストラクタ
     */
    public function __construct($function)
    {
        parent::connectDB();
        $this->function = $function;
        $this->ins_sth = $this->db->prepare("INSERT INTO duty_shift_log (emp_id, function, message, param) VALUES (?, ?, ?, ?)", array('text', 'text', 'text', 'text'), MDB2_PREPARE_MANIP);
        $this->emp_id = ($GLOBALS['_session_emp_id'] ? $GLOBALS['_session_emp_id'] : '000000000000');
    }

    /**
     * ログ
     * @param type $message
     * @param type $param
     * @throws Exception
     */
    public function log($message, $param = null)
    {
        if (is_array($param)) {
            $param = print_r($param, true);
        }

        $this->db->beginNestedTransaction();

        $res = $this->ins_sth->execute(array($this->emp_id, $this->function, $message, $param));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception("duty_shift_log INSERT ERROR: {$this->emp_id} " . $res->getDebugInfo());
        }

        $this->db->completeNestedTransaction();
    }

    /**
     * 検索条件の作成
     * @param type $cond
     * @return string
     */
    protected function make_condition($cond = null)
    {
        // 名前
        if (!empty($cond['srh_name'])) {
            $where[] = "e.emp_ft_nm || ' ' || e.emp_lt_nm || ' ' || e.emp_kn_ft_nm || ' ' || e.emp_kn_lt_nm || ' ' || e.emp_personal_id ILIKE :srh_name";
            $types[] = 'text';
            $data['srh_name'] = "%" . $cond['srh_name'] . "%";
        }

        // キーワード
        if (!empty($cond['srh_keyword'])) {
            $where[] = "param ILIKE :srh_keyword";
            $types[] = 'text';
            $data['srh_keyword'] = "%" . $cond['srh_keyword'] . "%";
        }

        // 機能
        if (!empty($cond['srh_function'])) {
            $where[] = " function = :srh_function";
            $types[] = 'text';
            $data['srh_function'] = $cond['srh_function'];
        }

        // 開始日
        if (!empty($cond['srh_start_date'])) {
            $where[] = " :srh_start_date <= timestamp";
            $types[] = 'text';
            $data['srh_start_date'] = $cond['srh_start_date'];
        }

        // 終了日
        if (!empty($cond['srh_last_date'])) {
            $where[] = " to_char(timestamp,'YYYY-MM-DD') <= :srh_last_date";
            $types[] = 'text';
            $data['srh_last_date'] = $cond['srh_last_date'];
        }

        if (empty($where)) {
            return array('cond' => 'WHERE id > 0');
        }

        return array(
            'cond' => 'WHERE ' . implode(' AND ', $where),
            'types' => $types,
            'data' => $data,
        );
    }

    /**
     * データ数を取得します
     * @return type
     */
    public function count($cond = null)
    {
        $ret = $this->make_condition($cond);

        $sql = "SELECT count(*) FROM duty_shift_log JOIN empmst e USING (emp_id) ";
        $sql.= $ret['cond'];

        $sth = $this->db->prepare($sql, array_merge(array('integer', 'text'), (array)$ret['types']), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array_merge(array('emp_id' => $this->emp_id), (array)$ret['data']));
        if (PEAR::isError($res)) {
            throw new Exception('duty_shift_log SELECT ERROR: ' . $res->getDebugInfo());
        }
        $count = $res->fetchOne();
        $sth->free();
        return $count;
    }

    /**
     * 一覧の取得
     * @param type $st
     * @param type $pageCount
     * @param type $cond
     * @return type
     */
    public function lists($st = null, $pageCount = null, $cond = null)
    {
        $ret = $this->make_condition($cond);

        $sql = "SELECT timestamp, emp_personal_id, e.emp_lt_nm || ' ' || emp_ft_nm AS emp_name, function, message, param FROM duty_shift_log JOIN empmst e USING (emp_id) ";
        $sql .= $ret['cond'];
        $sql .= " ORDER BY timestamp DESC";

        if (isset($st) && isset($pageCount)) {
            $this->db->setLimit($pageCount, $st);
        }

        $sth = $this->db->prepare($sql, $ret['types'], MDB2_PREPARE_RESULT);
        $res = $sth->execute($ret['data']);
        if (PEAR::isError($res)) {
            throw new Exception('duty_shift_log SELECT ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 機能名の配列を取得する
     * @return type
     */
    public function function_name()
    {
        return array(
            'table' => '勤務表',
            'personal' => '個人勤務表',
            'approval' => '月次承認',
            'data_output' => 'データ出力',
            'yukyu' => '有休一括付与',
            'door_open' => '電気錠打刻',
            'launcher_open' => 'ランチャー打刻',
            'csv_batch' => 'CSVバッチ出力',
            'ake_dakoku' => '明け自動打刻',
        );
    }

}
