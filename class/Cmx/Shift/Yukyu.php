<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'Cmx/Shift/Log.php';

/*
 * ͭ�ٰ����Ϳ
 */

class Cmx_Shift_Yukyu extends Model
{

    var $log;

    /**
     * ���󥹥ȥ饯��
     */
    public function __construct()
    {
        parent::connectDB();

        $this->log = new Cmx_Shift_Log('yukyu');
    }

    private function get_hol_tbl()
    {
        $sth = $this->db->prepare("SELECT * FROM timecard_paid_hol", array(), MDB2_PREPARE_RESULT);
        $res = $sth->execute();
        if (PEAR::isError($res)) {
            throw new Exception('timecard_paid_hol SELECT ERROR: ' . $res->getDebugInfo());
        }

        $holtbl = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            foreach (range(1, 8) as $i) {
                $holtbl[$row['paid_hol_tbl_id']][$i] = $row["add_day{$i}"];
            }
        }
        $sth->free();
        return $holtbl;
    }

    /**
     * ��Ϳ
     */
    public function fuyo($year, $month, $mode = null)
    {
        // �ٲ˥ơ��֥�
        $holtbl = $this->get_hol_tbl();

        // prepare
        $sth_emp = $this->db->prepare("SELECT * FROM empmst e JOIN authmst a USING (emp_id) JOIN empcond c USING (emp_id) WHERE NOT a.emp_del_flg AND e.emp_join<>'' ORDER BY emp_join", array(), MDB2_PREPARE_RESULT);
        $sth_paid = $this->db->prepare("SELECT * FROM emppaid WHERE emp_id = ? AND year = ? AND paid_hol_add_mmdd = ?", array('text', 'text', 'text'), MDB2_PREPARE_RESULT);
        $sth_zan1 = $this->db->prepare("
            SELECT
                days1,
                days2,
                adjust_day,
                YEAR || PAID_HOL_ADD_MMDD AS paid_date,
                i.curr_use AS used
            FROM EMPPAID p
            JOIN empmst e USING (emp_id)
            LEFT JOIN timecard_paid_hol_import i ON (i.emp_personal_id=e.emp_personal_id AND REPLACE(i.last_add_date,'/','')=(p.year||p.paid_hol_add_mmdd))

            WHERE EMP_ID = ?
            AND YEAR || PAID_HOL_ADD_MMDD <= ?
            ORDER BY YEAR || PAID_HOL_ADD_MMDD DESC
            OFFSET 0 LIMIT 1", array('text', 'text'), MDB2_PREPARE_RESULT);
        $sth_zan2 = $this->db->prepare("
            SELECT
                SUM(TO_NUMBER(HOLIDAY_COUNT,'999.99')) AS HOLIDAY
            FROM ATDBKRSLT r
            JOIN atdptn a ON (a.group_id=r.tmcd_group_id AND a.atdptn_id=to_number('0'||r.pattern,'999'))
            JOIN ATDBK_REASON_MST ON (to_number('0'||r.REASON,'999') = CAST(REASON_ID AS int) OR a.REASON = CAST(REASON_ID AS int))

            WHERE EMP_ID = ?
            AND KIND_FLAG = '1'
            AND DATE >= ? AND DATE < ?
            GROUP BY EMP_ID", array('text', 'text', 'text'), MDB2_PREPARE_RESULT);
        $paid_ins = $this->db->prepare('INSERT INTO emppaid (emp_id, year, days1, days2, paid_hol_tbl_id, paid_hol_add_mmdd, service_year, service_mon) values (?,?,?,?,?,?,?,?)', array('text', 'text', 'text', 'text', 'text', 'text', 'text', 'text'), MDB2_PREPARE_MANIP);

        $this->db->beginNestedTransaction();

        //�����롼��
        $cnt = 0;
        $lists = array();
        $res_emp = $sth_emp->execute();
        if (PEAR::isError($res_emp)) {
            throw new Exception('empmst SELECT ERROR: ' . $res_emp->getDebugInfo());
        }
        while ($emp = $res_emp->fetchRow(MDB2_FETCHMODE_ASSOC)) {

            # ��Ϳ�Ѥߡ�
            $res_paid = $sth_paid->execute(array($emp['emp_id'], $year, "0401"));
            if (PEAR::isError($res_paid)) {
                $this->db->failNestedTransaction();
                throw new Exception('emppaid SELECT ERROR: ' . $res_paid->getDebugInfo());
            }
            if ($res_paid->numRows() > 0) {
                continue;
            }

            # 2015-01-01��Ϳ�Ѥߡ�
            $res_paid_20150101 = $sth_paid->execute(array($emp['emp_id'], 2015, "0101"));
            if (PEAR::isError($res_paid_20150101)) {
                $this->db->failNestedTransaction();
                throw new Exception('emppaid SELECT ERROR: ' . $res_paid->getDebugInfo());
            }
            $paid_20150101 = false;
            if ($res_paid_20150101->numRows() > 0) {
                $paid_20150101 = true;
            }

            # ��1
            $res_zan1 = $sth_zan1->execute(array($emp['emp_id'], "{$year}0401"));
            if (PEAR::isError($res_zan1)) {
                $this->db->failNestedTransaction();
                throw new Exception('zan1 SELECT ERROR: ' . $res_zan1->getDebugInfo());
            }
            $zan1 = $res_zan1->fetchRow(MDB2_FETCHMODE_ASSOC);
            $paid_date = $zan1['paid_date'] ? $zan1['paid_date'] : '19000101';
            $last_days1 = $zan1['days1'] or 0;
            $last_days2 = $zan1['days2'] or 0;
            $last_adjust = $zan1['adjust_day'] or 0;
            $last_used = $zan1['used'] or 0;

            # ��2
            $res_zan2 = $sth_zan2->execute(array($emp['emp_id'], $paid_date, "{$year}0401"));
            if (PEAR::isError($res_zan2)) {
                $this->db->failNestedTransaction();
                throw new Exception('zan2 SELECT ERROR: ' . $res_zan2->getDebugInfo());
            }
            $zan2 = $res_zan2->fetchRow(MDB2_FETCHMODE_ASSOC);
            $used2 = $zan2['holiday'] or 0;

            # service_year
            $join = $emp['emp_join'];
            $fuyo = "{$year}0401";

            if (substr($fuyo, 4, 4) < substr($join, 4, 4)) {
                $sery = substr($fuyo, 0, 4) - substr($join, 0, 4) - 1;
                $serm = sprintf("%d", (substr($fuyo, 4, 4) - substr($join, 4, 4) + 1200) / 100);
            }
            else {
                $sery = substr($fuyo, 0, 4) - substr($join, 0, 4);
                $serm = sprintf("%d", (substr($fuyo, 4, 4) - substr($join, 4, 4)) / 100);
            }

            # days2
            $days2 = 0;
            $tbl_id = $sery;
            if (substr($join, 4, 4) !== "0401") {
                $tbl_id ++;
            }
            if ($paid_20150101) {
                $tbl_id ++;
            }
            if ($tbl_id > 8) {
                $tbl_id = 8;
            }

            $days2 = $holtbl[$emp['paid_hol_tbl_id']][$tbl_id];

            # kurikoshi
            $kurikoshi = $last_days1 + $last_days2 + $last_adjust;
            $used = $last_used + $used2;
            $nokori = $kurikoshi - $used;
            $days1 = ($nokori < $last_days2) ? $nokori : $last_days2;

            $lists[] = array(
                $emp['emp_id'],
                $emp['emp_personal_id'],
                $emp['emp_lt_nm'] . ' ' . $emp['emp_ft_nm'],
                $join,
                $fuyo,
                $sery,
                $serm,
                $days2,
                $kurikoshi,
                $last_days1,
                $last_days2,
                $last_adjust,
                $used,
                $last_used,
                $used2,
                $nokori,
                $last_days2,
                $days1,
                $this->tbl_name($tbl_id),
                $paid_20150101
            );

            # update
            if ($mode === 'update') {
                $res_ins = $paid_ins->execute(array($emp['emp_id'], $year, $days1, $days2, $emp['paid_hol_tbl_id'], "0401", $sery, $serm));
                if (PEAR::isError($res_ins)) {
                    $this->db->failNestedTransaction();
                    throw new Exception('paid_ins SELECT ERROR: ' . $res_ins->getDebugInfo());
                }
            }

            $cnt ++;
        }

        if ($mode === 'update') {
            $this->db->completeNestedTransaction();
            $this->log->log("{$year}-04-01");
            $mode = '';
        }
        else {
            $mode = 'update';
        }

        return array(
            'cnt' => $cnt,
            'lists' => $lists,
            'mode' => $mode,
        );
    }

    /**
     * ͭ��ɽ��̾��
     * @param type $tbl
     * @return string
     */
    private function tbl_name($tbl)
    {
        if ($tbl === 8) {
            return '7ǯ�ʾ�';
        }
        else if ($tbl <= 7 && $tbl >= 1) {
            return $tbl - 1 . 'ǯ';
        }
    }

    public function csv($year, $month)
    {
        $fuyo = $this->fuyo($year, $month);

        $fp = fopen('php://temp', 'r+b');
        foreach ($fuyo['lists'] as $row) {
            fputcsv($fp, array(
                $row[1],
                $row[2],
                $row[3],
                $row[4],
                $row[5],
                $row[19] ? '��' : '-',
                $row[18],
                $row[7],
                $row[8],
                $row[9],
                $row[10],
                $row[11],
                $row[12],
                $row[13],
                $row[14],
                $row[15],
                $row[16],
                $row[17],
            ));
        }
        rewind($fp);

        $csv = "����ID,����̾,������,ͭ����Ϳ��,��Ϳ���ζ�³ǯ��,2015/1/1��Ϳ,ͭ��ɽ,��ǯ��Ϳ,��ǯ(ͭ��(, ����ǯ +,��Ϳ +,Ĵ��) -,����(,Ĵ�� +,�ò�) =,������),��ǯ��Ϳ,��ǯ����\r\n";
        $csv .= str_replace(PHP_EOL, "\r\n", stream_get_contents($fp));

        return $csv;
    }

}
