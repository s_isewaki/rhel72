<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'Cmx/Shift/Log.php';
require_once 'Cmx/Shift/Date/Months.php';
require_once 'Cmx/Model/Shift/Ward.php';
require_once 'Cmx/Model/Shift/Staff.php';
require_once 'Cmx/Model/Shift/Table.php';
require_once 'Cmx/Model/Shift/Personal.php';
require_once 'Cmx/Model/Shift/DataLayout.php';

class Cmx_Shift_DataCSV extends Model
{

    /**
     * constractor
     */
    function __construct($data)
    {
        parent::connectDB();
        $this->init($data);
        $this->_log = new Cmx_Shift_Log('data_csv');
    }

    /**
     * init.
     * @param type $data
     */
    private function init($data)
    {
        // layout
        $dl = new Cmx_Shift_DataLayout();
        $this->layout = $dl->layout($data['layout']);

        // ward
        $this->group_id = $data['ward'];
        $this->ward = new Cmx_Shift_Ward($data['ward']);

        // date
        $this->start_date = $data['start_date'];
        $this->end_date = $data['end_date'];

        // date to time
        $this->start_time = strtotime($data['start_date']);
        $this->end_time = strtotime($data['end_date']);

        //  date ymd
        $this->start_ymd = date('Ymd', $this->start_time);
        $this->end_ymd = date('Ymd', $this->end_time);

        // range
        $sdate = $this->ward->shift_date('month');
        $this->range = $sdate->months_range($this->start_date, $this->end_date);

        // wage
        $this->wage = $data['wage'];

        // obj
        $this->staff = new Cmx_Shift_Staff();
    }

    /**
     * �ե�����̾
     * @return type
     */
    public function filename()
    {
        $filename = sprintf('%s-%s-%s-%s-%s', $this->ward->group_name(), date('Ymd', $this->start_time), date('Ymd', $this->end_time), $this->layout['title'], $this->wage);

        if (strstr($_SERVER['HTTP_USER_AGENT'], 'MSIE')) {
            return to_sjis($filename);
        }
        return to_utf8($filename);
    }

    /**
     * CSV����
     * @return string
     */
    public function output($all_flag = false)
    {
        // CSV
        $csv = $this->header() . $this->body($all_flag);

        // ���󥳡���
        switch ($this->layout['encoding']) {
            case 'shift_jis':
                return to_sjis($csv);

            case 'utf-8':
                return to_utf8($csv);
        }

        return $csv;
    }

    /**
     * CSV�إå�
     * @return type
     */
    private function header()
    {
        $header = array();
        foreach ($this->layout['layout'] as $row) {
            $header[] = $row['header'];
        }
        return implode(',', $header) . "\r\n";
    }

    /**
     * CSV�ܥǥ�
     * @return type
     */
    private function body($all_flag)
    {
        // �����ǡ���
        if ($all_flag) {
            $staff_lists = $this->staff->all_master_lists();
        }
        else {
            $staff_lists = $this->staff->master_lists($this->group_id);
        }

        // ��Ϳ�ٵ��ʬ����
        $wagehist = $this->staff->wage_history($this->end_date);

        // CSV���ϥХåե�
        $fp = fopen('php://temp', 'r+b');

        // �����롼��
        foreach ($staff_lists as $staff) {

            // ��Ϳ�ٵ��ʬ�����б�
            if (!empty($wagehist[$staff->emp_id()])) {
                $wage = $wagehist[$staff->emp_id()];
            }
            else {
                $wage = $staff->wage();
            }

            // �������Ͻ��Ϥ��ʤ�
            if ($wage === '3') {
                continue;
            }

            // ��Ϳ�ٵ��ʬ�����å�
            if ($this->wage === '4' && $wage !== '4') {
                continue;
            }
            if ($this->wage !== '4' && $wage === '4') {
                continue;
            }

            // ǯ����
            $item = array();
            foreach ($this->range as $ym) {
                $person = new Cmx_Shift_Personal($staff->emp_id(), 'month');
                $lists = $person->lists($ym[0], $ym[1]);

                // ����
                foreach ($lists[0] as $list) {

                    // ���֥����å�
                    if ($this->start_ymd > $list['date'] || $this->end_ymd < $list['date']) {
                        continue;
                    }

                    // �쥤������
                    foreach ($this->layout['layout'] as $i => $layout) {
                        // ��̳����
                        if ($layout['type'] === 'count') {
                            foreach ($layout['count'] as $count) {
                                $item["count{$i}"] += ($list[$count['pattern']] * $count['coeffient']);
                            }
                        }

                        // ���׹���
                        else if (substr($layout['type'], 0, 2) === 'c:') {
                            $type = substr($layout['type'], 2);
                            $item[$type] += $list[$type];
                        }
                    }
                }
            }

            // ͭ��
            $holiday = $staff->paid_holiday($staff->emp_id(), $this->end_ymd);

            // �Ƶ�
            list($list['summer_fuyo'], $list['summer_use'], $list['summer_date']) = $staff->paid_holiday_summer($staff->emp_id(), $this->end_ymd);

            // ����
            list($list['week_fuyo'], $list['week_use'], $list['week_date']) = $staff->paid_holiday_week($staff->emp_id(), $this->end_ymd);

            // �쥤������
            $csv = array();
            foreach ($this->layout['layout'] as $i => $layout) {
                switch ($layout['type']) {

                    // ���򥻥�
                    case 'empty':
                        $csv[] = '';
                        break;

                    // ����
                    case 'zero':
                        $csv[] = 0;
                        break;

                    // ��̳����
                    case 'count':
                        $csv[] = $item["count{$i}"];
                        break;

                    // ����°��
                    case 'emp_personal_id':
                    case 'emp_name':
                    case 'emp_kana':
                    case 'class_name':
                    case 'atrb_name':
                    case 'dept_name':
                    case 'room_name':
                    case 'st_name':
                    case 'job_name':
                    case 'wage_name':
                    case 'duty_form_name':
                        $csv[] = $staff->{$layout['type']}();
                        break;

                    // ������
                    case 'start_date':
                        $csv[] = date($layout['format'], $this->start_time);
                        break;

                    // ��λ��
                    case 'end_date':
                        $csv[] = date($layout['format'], $this->end_time);
                        break;

                    // �̵ڷ�
                    case 'sokyu_stuki':
                        $csv[] = date('Ym', $this->start_time);
                        break;

                    // ����
                    case 'group_name':
                        $csv[] = $this->ward->group_name();
                        break;

                    // ͭ��
                    case 'holiday_date':
                    case 'holiday_mae_kuri':
                    case 'holiday_mae_fuyo':
                    case 'holiday_mae_adjust':
                    case 'holiday_mae_use':
                    case 'holiday_mae_zan':
                    case 'holiday_kuri':
                    case 'holiday_fuyo':
                    case 'holiday_adjust':
                    case 'holiday_use':
                        $csv[] = $holiday[4][$layout['type']];
                        break;

                    // �Ƶ�
                    case 'summer_fuyo':
                    case 'summer_use':
                    case 'summer_date':
                    // ����
                    case 'week_fuyo':
                    case 'week_use':
                    case 'week_date':
                        $csv[] = $list[$layout['type']];
                        break;

                    // ���׹���
                    default:
                        $type = substr($layout['type'], 2);

                        switch ($type) {
                            // ����
                            case 'totalworktime':
                            case 'worktime':
                            case 'overtime':
                            case 'over100':
                            case 'over125':
                            case 'over25':
                            case 'over135':
                            case 'over150':
                            case 'conference_minutes':
                            case 'base_shorter_minutes':
                            case 'real_shorter_minutes':
                            case 'base_shorter_minutes1':
                            case 'real_shorter_minutes1':
                            case 'base_shorter_minutes6':
                            case 'real_shorter_minutes6':
                                $csv[] = sprintf("%d.%02d", intval($item[$type] / 60), intval($item[$type] % 60));
                                break;

                            default:
                                $csv[] = $item[$type];
                                break;
                        }
                        break;
                }
            }

            fputcsv($fp, $csv);
        }
        rewind($fp);

        return str_replace(PHP_EOL, "\r\n", stream_get_contents($fp));
    }

}
