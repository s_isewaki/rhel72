<?php

require_once('Cmx.php');
require_once('MDB2.php');

/**
 * CoMedix データベースモデル：ベースクラス
 */
class Model {

    var $db;

    /**
     * DB接続
     *
     * @param DSN。指定が無ければ CMX_DB_DSNを使う
     */
    function connectDB($dsn = CMX_DB_DSN) {
        $dbh = MDB2::singleton($dsn);
        if (PEAR::isError($dbh)) {
            die($dbh->getDebugInfo());
        }

        $dbh->loadModule('Extended');
//        $dbh->setOption('debug', true);
//        $dbh->setOption('debug_handler', 'mdb2_err_handler');

        $this->db = $dbh;
    }

    /**
     * getterメソッド
     *
     * @param type $field
     * @return type
     */
    function _getter($field) {
        return $this->_row[$field];
    }

}

/**
 * MDB2のSQLをログに出力するハンドラ
 * @link http://e6sc8e.jugem.cc/?eid=343
 *
 * @param type $db
 * @param type $scope
 * @param type $message
 * @param type $is_manip
 */
function mdb2_err_handler(&$db, $scope, $message, $is_manip = null) {
    cmx_log("$scope, $is_manip, $message");
}
