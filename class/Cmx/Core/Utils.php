<?php

/**
 * CoMedix Common Functions
 */

/**
 * javascript alert
 */
if (!function_exists("js_alert_exit")) {

    function js_alert_exit($mes) {
        printf('<script type="text/javascript">alert(\'%s\');</script>', htmlspecialchars($mes, ENT_QUOTES, 'euc-jp'));
        print ('<script type="text/javascript">history.back();</script>');
        exit;
    }

}

/**
 * javascript go to error page
 */
if (!function_exists("js_error_exit")) {

    function js_error_exit() {
        printf('<script type="text/javascript" src="%s/js/showpage.js"></script>', CMX_BASE_URL);
        printf('<script type="text/javascript">showErrorPage(window, "%s");</script>', CMX_BASE_URL);
        exit;
    }

}

/**
 * javascript back to login page
 */
if (!function_exists("js_login_exit")) {

    function js_login_exit() {
        printf('<script type="text/javascript" src="%s/js/showpage.js"></script>', CMX_BASE_URL);
        printf('<script type="text/javascript">showLoginPage(window, "%s");</script>', CMX_BASE_URL);
        exit;
    }

}


/**
 * short name: htmlspecialchars
 */
if (!function_exists("h")) {

    function h($var, $flags = ENT_COMPAT, $charset = 'ISO-8859-1') {
        return htmlspecialchars($var, $flags, $charset);
    }

}

/**
 * convert EUC-JP to Shift_JIS
 */
if (!function_exists("to_sjis")) {

    function to_sjis($string) {
        return mb_convert_encoding($string, "SJIS-win", "EUC-JP");
    }

}

/**
 * convert EUC-JP to UTF-8
 */
if (!function_exists("to_utf8")) {

    function to_utf8($string)
    {
        return mb_convert_encoding(mb_convert_encoding($string, "SJIS-win", "EUC-JP"), "UTF-8", "SJIS-win");
    }

}

/**
 * convert EOL
 */
if (!function_exists("convert_EOL")) {

    function convert_EOL($string, $to = "\n")
    {
        return strtr($string, array(
            "\r\n" => $to,
            "\r" => $to,
            "\n" => $to,
        ));
    }

}

/**
 * convert Shift_JIS to EUC-JP
 */
if (!function_exists("from_sjis")) {

    function from_sjis($string)
    {
        // PHP 5.2.1以上であればcp51932が使える
        if (version_compare(PHP_VERSION, '5.2.1') >= 0) {
            return mb_convert_encoding($string, "cp51932", "cp932");
        }
        else {
            return mb_convert_encoding($string, "eucJP-win", "cp932");
        }
    }

}

/**
 * convert UTF-8 to EUC-JP
 */
if (!function_exists("from_utf8")) {

    function from_utf8($string) {
        // PHP 5.2.1以上であればcp51932が使える
        if (version_compare(PHP_VERSION, '5.2.1') >= 0) {
            return mb_convert_encoding($string, "cp51932", "UTF-8");
        }

        // IBM拡張文字変換テーブル
        $__UTF8_TO_EUCJP_MAP = array(
            array("/\x8f\xf3\xf3/", "\xfc\xf1"), // �齠: e285b0
            array("/\x8f\xf3\xf4/", "\xfc\xf2"), // �齡: e285b1
            array("/\x8f\xf3\xf5/", "\xfc\xf3"), // �齦: e285b2
            array("/\x8f\xf3\xf6/", "\xfc\xf4"), // �齧: e285b3
            array("/\x8f\xf3\xf7/", "\xfc\xf5"), // �齬: e285b4
            array("/\x8f\xf3\xf8/", "\xfc\xf6"), // �齪: e285b5
            array("/\x8f\xf3\xf9/", "\xfc\xf7"), // �齷: e285b6
            array("/\x8f\xf3\xfa/", "\xfc\xf8"), // �齲: e285b7
            array("/\x8f\xf3\xfb/", "\xfc\xf9"), // �齶: e285b8
            array("/\x8f\xf3\xfc/", "\xfc\xfa"), // �龕: e285b9
            //array("/\x8f\xa2\xc3/", "\x3f"), // ¦: c2a6
            array("/\x8f\xf4\xa9/", "\xfc\xfd"), // ���: efbc87
            array("/\x8f\xf4\xaa/", "\xfc\xfe"), // ���: efbc82
            array("/\x8f\xd4\xe3/", "\xf9\xa1"), // 纊: e7ba8a
            array("/\x8f\xdc\xdf/", "\xf9\xa2"), // 褜: e8a49c
            array("/\x8f\xe4\xe9/", "\xf9\xa3"), // 鍈: e98d88
            array("/\x8f\xe3\xf8/", "\xf9\xa4"), // 銈: e98a88
            array("/\x8f\xd9\xa1/", "\xf9\xa5"), // 蓜: e8939c
            array("/\x8f\xb1\xbb/", "\xf9\xa6"), // 俉: e4bf89
            array("/\x8f\xf4\xae/", "\xf9\xa7"), // ���: e782bb
            array("/\x8f\xc2\xad/", "\xf9\xa8"), // 昱: e698b1
            array("/\x8f\xc3\xfc/", "\xf9\xa9"), // 棈: e6a388
            array("/\x8f\xe4\xd0/", "\xf9\xaa"), // 鋹: e98bb9
            array("/\x8f\xc2\xbf/", "\xf9\xab"), // 曻: e69bbb
            array("/\x8f\xbc\xf4/", "\xf9\xac"), // 彅: e5bd85
            array("/\x8f\xb0\xa9/", "\xf9\xad"), // 丨: e4b8a8
            array("/\x8f\xb0\xc8/", "\xf9\xae"), // 仡: e4bba1
            array("/\x8f\xf4\xaf/", "\xf9\xaf"), // ���: e4bbbc
            array("/\x8f\xb0\xd2/", "\xf9\xb0"), // 伀: e4bc80
            array("/\x8f\xb0\xd4/", "\xf9\xb1"), // 伃: e4bc83
            array("/\x8f\xb0\xe3/", "\xf9\xb2"), // 伹: e4bcb9
            array("/\x8f\xb0\xee/", "\xf9\xb3"), // 佖: e4bd96
            array("/\x8f\xb1\xa7/", "\xf9\xb4"), // 侒: e4be92
            array("/\x8f\xb1\xa3/", "\xf9\xb5"), // 侊: e4be8a
            array("/\x8f\xb1\xac/", "\xf9\xb6"), // 侚: e4be9a
            array("/\x8f\xb1\xa9/", "\xf9\xb7"), // 侔: e4be94
            array("/\x8f\xb1\xbe/", "\xf9\xb8"), // 俍: e4bf8d
            array("/\x8f\xb1\xdf/", "\xf9\xb9"), // 偀: e58180
            array("/\x8f\xb1\xd8/", "\xf9\xba"), // 倢: e580a2
            array("/\x8f\xb1\xc8/", "\xf9\xbb"), // 俿: e4bfbf
            array("/\x8f\xb1\xd7/", "\xf9\xbc"), // 倞: e5809e
            array("/\x8f\xb1\xe3/", "\xf9\xbd"), // 偆: e58186
            array("/\x8f\xb1\xf4/", "\xf9\xbe"), // 偰: e581b0
            array("/\x8f\xb1\xe1/", "\xf9\xbf"), // 偂: e58182
            array("/\x8f\xb2\xa3/", "\xf9\xc0"), // 傔: e58294
            array("/\x8f\xf4\xb0/", "\xf9\xc1"), // ���: e583b4
            array("/\x8f\xb2\xbb/", "\xf9\xc2"), // 僘: e58398
            array("/\x8f\xb2\xe6/", "\xf9\xc3"), // 兊: e5858a
            array("/\x8f\xb2\xed/", "\xf9\xc4"), // 兤: e585a4
            array("/\x8f\xb2\xf5/", "\xf9\xc5"), // 冝: e5869d
            array("/\x8f\xb2\xfc/", "\xf9\xc6"), // 冾: e586be
            array("/\x8f\xf4\xb1/", "\xf9\xc7"), // ���: e587ac
            array("/\x8f\xb3\xb5/", "\xf9\xc8"), // 刕: e58895
            array("/\x8f\xb3\xd8/", "\xf9\xc9"), // 劜: e58a9c
            array("/\x8f\xb3\xdb/", "\xf9\xca"), // 劦: e58aa6
            array("/\x8f\xb3\xe5/", "\xf9\xcb"), // 勀: e58b80
            array("/\x8f\xb3\xee/", "\xf9\xcc"), // 勛: e58b9b
            array("/\x8f\xb3\xfb/", "\xf9\xcd"), // 匀: e58c80
            array("/\x8f\xf4\xb2/", "\xf9\xce"), // ���: e58c87
            array("/\x8f\xf4\xb3/", "\xf9\xcf"), // ���: e58ca4
            array("/\x8f\xb4\xc0/", "\xf9\xd0"), // 卲: e58db2
            array("/\x8f\xb4\xc7/", "\xf9\xd1"), // 厓: e58e93
            array("/\x8f\xb4\xd0/", "\xf9\xd2"), // 厲: e58eb2
            array("/\x8f\xb4\xde/", "\xf9\xd3"), // 叝: e58f9d
            array("/\x8f\xf4\xb4/", "\xf9\xd4"), // ���: efa88e
            array("/\x8f\xb5\xaa/", "\xf9\xd5"), // 咜: e5929c
            array("/\x8f\xf4\xb5/", "\xf9\xd6"), // ���: e5928a
            array("/\x8f\xb5\xaf/", "\xf9\xd7"), // 咩: e592a9
            array("/\x8f\xb5\xc4/", "\xf9\xd8"), // 哿: e593bf
            array("/\x8f\xb5\xe8/", "\xf9\xd9"), // 喆: e59686
            array("/\x8f\xf4\xb6/", "\xf9\xda"), // ���: e59d99
            array("/\x8f\xb7\xc2/", "\xf9\xdb"), // 坥: e59da5
            array("/\x8f\xb7\xe4/", "\xf9\xdc"), // 垬: e59eac
            array("/\x8f\xb7\xe8/", "\xf9\xdd"), // 埈: e59f88
            array("/\x8f\xb7\xe7/", "\xf9\xde"), // 埇: e59f87
            array("/\x8f\xf4\xb7/", "\xf9\xdf"), // ���: efa88f
            array("/\x8f\xf4\xb9/", "\xf9\xe1"), // ���: e5a29e
            array("/\x8f\xb8\xce/", "\xf9\xe2"), // 墲: e5a2b2
            array("/\x8f\xb8\xe1/", "\xf9\xe3"), // 夋: e5a48b
            array("/\x8f\xb8\xf5/", "\xf9\xe4"), // 奓: e5a593
            array("/\x8f\xb8\xf7/", "\xf9\xe5"), // 奛: e5a59b
            array("/\x8f\xb8\xf8/", "\xf9\xe6"), // 奝: e5a59d
            array("/\x8f\xb8\xfc/", "\xf9\xe7"), // 奣: e5a5a3
            array("/\x8f\xb9\xaf/", "\xf9\xe8"), // 妤: e5a6a4
            array("/\x8f\xb9\xb7/", "\xf9\xe9"), // 妺: e5a6ba
            array("/\x8f\xba\xbe/", "\xf9\xea"), // 孖: e5ad96
            array("/\x8f\xba\xdb/", "\xf9\xeb"), // 寀: e5af80
            array("/\x8f\xcd\xaa/", "\xf9\xec"), // 甯: e794af
            array("/\x8f\xba\xe1/", "\xf9\xed"), // 寘: e5af98
            array("/\x8f\xf4\xba/", "\xf9\xee"), // ���: e5afac
            array("/\x8f\xba\xeb/", "\xf9\xef"), // 尞: e5b09e
            array("/\x8f\xbb\xb3/", "\xf9\xf0"), // 岦: e5b2a6
            array("/\x8f\xbb\xb8/", "\xf9\xf1"), // 岺: e5b2ba
            array("/\x8f\xf4\xbb/", "\xf9\xf2"), // ���: e5b3b5
            array("/\x8f\xbb\xca/", "\xf9\xf3"), // 崧: e5b4a7
            array("/\x8f\xf4\xbc/", "\xf9\xf4"), // ���: e5b593
            array("/\x8f\xf4\xbd/", "\xf9\xf5"), // ���: efa891
            array("/\x8f\xbb\xd0/", "\xf9\xf6"), // 嵂: e5b582
            array("/\x8f\xbb\xde/", "\xf9\xf7"), // 嵭: e5b5ad
            array("/\x8f\xbb\xf4/", "\xf9\xf8"), // 嶸: e5b6b8
            array("/\x8f\xbb\xf5/", "\xf9\xf9"), // 嶹: e5b6b9
            array("/\x8f\xbb\xf9/", "\xf9\xfa"), // 巐: e5b790
            array("/\x8f\xbc\xe4/", "\xf9\xfb"), // 弡: e5bca1
            array("/\x8f\xbc\xed/", "\xf9\xfc"), // 弴: e5bcb4
            array("/\x8f\xbc\xfe/", "\xf9\xfd"), // 彧: e5bda7
            array("/\x8f\xf4\xbe/", "\xf9\xfe"), // ���: e5beb7
            array("/\x8f\xbd\xc2/", "\xfa\xa1"), // 忞: e5bf9e
            array("/\x8f\xbd\xe7/", "\xfa\xa2"), // 恝: e6819d
            array("/\x8f\xf4\xbf/", "\xfa\xa3"), // ���: e68285
            array("/\x8f\xbd\xf0/", "\xfa\xa4"), // 悊: e6828a
            array("/\x8f\xbe\xb0/", "\xfa\xa5"), // 惞: e6839e
            array("/\x8f\xbe\xac/", "\xfa\xa6"), // 惕: e68395
            array("/\x8f\xf4\xc0/", "\xfa\xa7"), // ���: e684a0
            array("/\x8f\xbe\xb3/", "\xfa\xa8"), // 惲: e683b2
            array("/\x8f\xbe\xbd/", "\xfa\xa9"), // 愑: e68491
            array("/\x8f\xbe\xcd/", "\xfa\xaa"), // 愷: e684b7
            array("/\x8f\xbe\xc9/", "\xfa\xab"), // 愰: e684b0
            array("/\x8f\xbe\xe4/", "\xfa\xac"), // 憘: e68698
            array("/\x8f\xbf\xa8/", "\xfa\xad"), // 戓: e68893
            array("/\x8f\xbf\xc9/", "\xfa\xae"), // 抦: e68aa6
            array("/\x8f\xc0\xc4/", "\xfa\xaf"), // 揵: e68fb5
            array("/\x8f\xc0\xe4/", "\xfa\xb0"), // 摠: e691a0
            array("/\x8f\xc0\xf4/", "\xfa\xb1"), // 撝: e6929d
            array("/\x8f\xc1\xa6/", "\xfa\xb2"), // 擎: e6938e
            array("/\x8f\xf4\xc1/", "\xfa\xb3"), // ���: e6958e
            array("/\x8f\xc1\xf5/", "\xfa\xb4"), // 昀: e69880
            array("/\x8f\xc1\xfc/", "\xfa\xb5"), // 昕: e69895
            array("/\x8f\xf4\xc2/", "\xfa\xb6"), // ���: e698bb
            array("/\x8f\xc1\xf8/", "\xfa\xb7"), // 昉: e69889
            array("/\x8f\xc2\xab/", "\xfa\xb8"), // 昮: e698ae
            array("/\x8f\xc2\xa1/", "\xfa\xb9"), // 昞: e6989e
            array("/\x8f\xc2\xa5/", "\xfa\xba"), // 昤: e698a4
            array("/\x8f\xf4\xc3/", "\xfa\xbb"), // ���: e699a5
            array("/\x8f\xc2\xb8/", "\xfa\xbc"), // 晗: e69997
            array("/\x8f\xc2\xba/", "\xfa\xbd"), // 晙: e69999
            array("/\x8f\xc2\xc4/", "\xfa\xbf"), // 晳: e699b3
            array("/\x8f\xc2\xd2/", "\xfa\xc0"), // 暙: e69a99
            array("/\x8f\xc2\xd7/", "\xfa\xc1"), // 暠: e69aa0
            array("/\x8f\xc2\xdb/", "\xfa\xc2"), // 暲: e69ab2
            array("/\x8f\xc2\xde/", "\xfa\xc3"), // 暿: e69abf
            array("/\x8f\xc2\xed/", "\xfa\xc4"), // 曺: e69bba
            array("/\x8f\xc2\xf0/", "\xfa\xc5"), // 朎: e69c8e
            array("/\x8f\xc3\xa1/", "\xfa\xc7"), // 杦: e69da6
            array("/\x8f\xc3\xb5/", "\xfa\xc8"), // 枻: e69ebb
            array("/\x8f\xc3\xc9/", "\xfa\xc9"), // 桒: e6a192
            array("/\x8f\xc3\xb9/", "\xfa\xca"), // 柀: e69f80
            array("/\x8f\xf4\xc6/", "\xfa\xcb"), // ���: e6a081
            array("/\x8f\xc3\xd8/", "\xfa\xcc"), // 桄: e6a184
            array("/\x8f\xc3\xfe/", "\xfa\xcd"), // 棏: e6a38f
            array("/\x8f\xf4\xc7/", "\xfa\xce"), // ���: efa893
            array("/\x8f\xc4\xcc/", "\xfa\xcf"), // 楨: e6a5a8
            array("/\x8f\xf4\xc8/", "\xfa\xd0"), // ���: efa894
            array("/\x8f\xc4\xd9/", "\xfa\xd1"), // 榘: e6a698
            array("/\x8f\xc4\xea/", "\xfa\xd2"), // 槢: e6a7a2
            array("/\x8f\xc4\xfd/", "\xfa\xd3"), // 樰: e6a8b0
            array("/\x8f\xf4\xc9/", "\xfa\xd4"), // ���: e6a9ab
            array("/\x8f\xc5\xa7/", "\xfa\xd5"), // 橆: e6a986
            array("/\x8f\xc5\xb5/", "\xfa\xd6"), // 橳: e6a9b3
            array("/\x8f\xc5\xb6/", "\xfa\xd7"), // 橾: e6a9be
            array("/\x8f\xf4\xca/", "\xfa\xd8"), // ���: e6aba2
            array("/\x8f\xc5\xd5/", "\xfa\xd9"), // 櫤: e6aba4
            array("/\x8f\xc6\xb8/", "\xfa\xda"), // 毖: e6af96
            array("/\x8f\xc6\xd7/", "\xfa\xdb"), // 氿: e6b0bf
            array("/\x8f\xc6\xe0/", "\xfa\xdc"), // 汜: e6b19c
            array("/\x8f\xc6\xea/", "\xfa\xdd"), // 沆: e6b286
            array("/\x8f\xc6\xe3/", "\xfa\xde"), // 汯: e6b1af
            array("/\x8f\xc7\xa1/", "\xfa\xdf"), // 泚: e6b39a
            array("/\x8f\xc7\xab/", "\xfa\xe0"), // 洄: e6b484
            array("/\x8f\xc7\xc7/", "\xfa\xe1"), // 涇: e6b687
            array("/\x8f\xc7\xc3/", "\xfa\xe2"), // 浯: e6b5af
            array("/\x8f\xc7\xcb/", "\xfa\xe3"), // 涖: e6b696
            array("/\x8f\xc7\xcf/", "\xfa\xe4"), // 涬: e6b6ac
            array("/\x8f\xc7\xd9/", "\xfa\xe5"), // 淏: e6b78f
            array("/\x8f\xf4\xcb/", "\xfa\xe6"), // ���: e6b7b8
            array("/\x8f\xf4\xcc/", "\xfa\xe7"), // ���: e6b7b2
            array("/\x8f\xc7\xe6/", "\xfa\xe8"), // 淼: e6b7bc
            array("/\x8f\xc7\xee/", "\xfa\xe9"), // 渹: e6b8b9
            array("/\x8f\xc7\xfc/", "\xfa\xea"), // 湜: e6b99c
            array("/\x8f\xc7\xeb/", "\xfa\xeb"), // 渧: e6b8a7
            array("/\x8f\xc7\xf0/", "\xfa\xec"), // 渼: e6b8bc
            array("/\x8f\xc8\xb1/", "\xfa\xed"), // 溿: e6babf
            array("/\x8f\xc8\xe5/", "\xfa\xee"), // 澈: e6be88
            array("/\x8f\xc8\xf8/", "\xfa\xef"), // 澵: e6beb5
            array("/\x8f\xc9\xa6/", "\xfa\xf0"), // 濵: e6bfb5
            array("/\x8f\xc9\xab/", "\xfa\xf1"), // 瀅: e78085
            array("/\x8f\xc9\xad/", "\xfa\xf2"), // 瀇: e78087
            array("/\x8f\xf4\xcd/", "\xfa\xf3"), // ���: e780a8
            array("/\x8f\xc9\xca/", "\xfa\xf4"), // 炅: e78285
            array("/\x8f\xc9\xd3/", "\xfa\xf5"), // 炫: e782ab
            array("/\x8f\xc9\xe9/", "\xfa\xf6"), // 焏: e7848f
            array("/\x8f\xc9\xe3/", "\xfa\xf7"), // 焄: e78484
            array("/\x8f\xc9\xfc/", "\xfa\xf8"), // 煜: e7859c
            array("/\x8f\xc9\xf4/", "\xfa\xf9"), // 煆: e78586
            array("/\x8f\xc9\xf5/", "\xfa\xfa"), // 煇: e78587
            //array("/\x8f\xb3\xa8/", "\x3f"), // 凞: e5879e
            array("/\x8f\xca\xb3/", "\xfa\xfc"), // 燁: e78781
            array("/\x8f\xca\xbd/", "\xfa\xfd"), // 燾: e787be
            array("/\x8f\xca\xef/", "\xfa\xfe"), // 犱: e78ab1
            array("/\x8f\xca\xf1/", "\xfb\xa1"), // 犾: e78abe
            array("/\x8f\xcb\xae/", "\xfb\xa2"), // 猤: e78ca4
            array("/\x8f\xcb\xca/", "\xfb\xa4"), // 獷: e78db7
            array("/\x8f\xcb\xe6/", "\xfb\xa5"), // 玽: e78ebd
            array("/\x8f\xcb\xea/", "\xfb\xa6"), // 珉: e78f89
            array("/\x8f\xcb\xf0/", "\xfb\xa7"), // 珖: e78f96
            array("/\x8f\xcb\xf4/", "\xfb\xa8"), // 珣: e78fa3
            array("/\x8f\xcb\xee/", "\xfb\xa9"), // 珒: e78f92
            array("/\x8f\xcc\xa5/", "\xfb\xaa"), // 琇: e79087
            array("/\x8f\xcb\xf9/", "\xfb\xab"), // 珵: e78fb5
            array("/\x8f\xcc\xab/", "\xfb\xac"), // 琦: e790a6
            array("/\x8f\xcc\xae/", "\xfb\xad"), // 琪: e790aa
            array("/\x8f\xcc\xad/", "\xfb\xae"), // 琩: e790a9
            array("/\x8f\xcc\xb2/", "\xfb\xaf"), // 琮: e790ae
            array("/\x8f\xcc\xc2/", "\xfb\xb0"), // 瑢: e791a2
            array("/\x8f\xcc\xd0/", "\xfb\xb1"), // 璉: e79289
            array("/\x8f\xcc\xd9/", "\xfb\xb2"), // 璟: e7929f
            array("/\x8f\xf4\xd0/", "\xfb\xb3"), // ���: e79481
            array("/\x8f\xcd\xbb/", "\xfb\xb4"), // 畯: e795af
            array("/\x8f\xf4\xd1/", "\xfb\xb5"), // ���: e79a82
            array("/\x8f\xce\xbb/", "\xfb\xb6"), // 皜: e79a9c
            array("/\x8f\xf4\xd2/", "\xfb\xb7"), // ���: e79a9e
            array("/\x8f\xce\xba/", "\xfb\xb8"), // 皛: e79a9b
            array("/\x8f\xce\xc3/", "\xfb\xb9"), // 皦: e79aa6
            array("/\x8f\xce\xf2/", "\xfb\xbb"), // 睆: e79d86
            array("/\x8f\xb3\xdd/", "\xfb\xbc"), // 劯: e58aaf
            array("/\x8f\xcf\xd5/", "\xfb\xbd"), // 砡: e7a0a1
            array("/\x8f\xcf\xe2/", "\xfb\xbe"), // 硎: e7a18e
            array("/\x8f\xcf\xe9/", "\xfb\xbf"), // 硤: e7a1a4
            array("/\x8f\xcf\xed/", "\xfb\xc0"), // 硺: e7a1ba
            array("/\x8f\xf4\xd4/", "\xfb\xc1"), // ���: e7a4b0
            array("/\x8f\xd0\xe5/", "\xfb\xc5"), // 禔: e7a694
            array("/\x8f\xd0\xe9/", "\xfb\xc7"), // 禛: e7a69b
            array("/\x8f\xd1\xe8/", "\xfb\xc8"), // 竑: e7ab91
            array("/\x8f\xf4\xd9/", "\xfb\xc9"), // ���: e7aba7
            array("/\x8f\xd1\xec/", "\xfb\xcb"), // 竫: e7abab
            array("/\x8f\xd2\xbb/", "\xfb\xcc"), // 箞: e7ae9e
            array("/\x8f\xd3\xe1/", "\xfb\xce"), // 絈: e7b588
            array("/\x8f\xd3\xe8/", "\xfb\xcf"), // 絜: e7b59c
            array("/\x8f\xd4\xa7/", "\xfb\xd0"), // 綷: e7b6b7
            array("/\x8f\xf4\xdc/", "\xfb\xd1"), // ���: e7b6a0
            array("/\x8f\xf4\xdd/", "\xfb\xd2"), // ���: e7b796
            array("/\x8f\xd4\xd4/", "\xfb\xd3"), // 繒: e7b992
            array("/\x8f\xd4\xf2/", "\xfb\xd4"), // 罇: e7bd87
            array("/\x8f\xd5\xae/", "\xfb\xd5"), // 羡: e7bea1
            array("/\x8f\xd7\xde/", "\xfb\xd7"), // 茁: e88c81
            array("/\x8f\xf4\xdf/", "\xfb\xd8"), // ���: e88da2
            array("/\x8f\xd8\xa2/", "\xfb\xd9"), // 荿: e88dbf
            array("/\x8f\xd8\xb7/", "\xfb\xda"), // 菇: e88f87
            array("/\x8f\xd8\xc1/", "\xfb\xdb"), // 菶: e88fb6
            array("/\x8f\xd8\xd1/", "\xfb\xdc"), // 葈: e89188
            array("/\x8f\xd8\xf4/", "\xfb\xdd"), // 蒴: e892b4
            array("/\x8f\xd9\xc6/", "\xfb\xde"), // 蕓: e89593
            array("/\x8f\xd9\xc8/", "\xfb\xdf"), // 蕙: e89599
            array("/\x8f\xd9\xd1/", "\xfb\xe0"), // 蕫: e895ab
            array("/\x8f\xf4\xe0/", "\xfb\xe1"), // ���: efa89f
            array("/\x8f\xf4\xe1/", "\xfb\xe2"), // ���: e896b0
            // array("/\x8f\xda\xa9/", "\x3f"), // 蘒: e89892
            array("/\x8f\xf4\xe3/", "\xfb\xe4"), // ���: efa8a1
            array("/\x8f\xf4\xe4/", "\xfb\xe5"), // ���: e8a087
            array("/\x8f\xdc\xd3/", "\xfb\xe6"), // 裵: e8a3b5
            array("/\x8f\xdd\xc8/", "\xfb\xe7"), // 訒: e8a892
            array("/\x8f\xdd\xd4/", "\xfb\xe8"), // 訷: e8a8b7
            array("/\x8f\xdd\xea/", "\xfb\xe9"), // 詹: e8a9b9
            array("/\x8f\xdd\xfa/", "\xfb\xea"), // 誧: e8aaa7
            array("/\x8f\xde\xa4/", "\xfb\xeb"), // 誾: e8aabe
            array("/\x8f\xde\xb0/", "\xfb\xec"), // 諟: e8ab9f
            array("/\x8f\xde\xb5/", "\xfb\xee"), // 諶: e8abb6
            array("/\x8f\xde\xcb/", "\xfb\xef"), // 譓: e8ad93
            array("/\x8f\xf4\xe6/", "\xfb\xf0"), // ���: e8adbf
            array("/\x8f\xdf\xb9/", "\xfb\xf1"), // 賰: e8b3b0
            array("/\x8f\xf4\xe7/", "\xfb\xf2"), // ���: e8b3b4
            array("/\x8f\xdf\xc3/", "\xfb\xf3"), // 贒: e8b492
            array("/\x8f\xf4\xe8/", "\xfb\xf4"), // ���: e8b5b6
            array("/\x8f\xf4\xe9/", "\xfb\xf5"), // ���: efa8a3
            array("/\x8f\xe0\xd9/", "\xfb\xf6"), // 軏: e8bb8f
            array("/\x8f\xf4\xea/", "\xfb\xf7"), // ���: efa8a4
            array("/\x8f\xe1\xe2/", "\xfb\xf9"), // 遧: e981a7
            array("/\x8f\xf4\xec/", "\xfb\xfa"), // ���: e9839e
            array("/\x8f\xf4\xee/", "\xfb\xfc"), // ���: e98495
            array("/\x8f\xe2\xc7/", "\xfb\xfd"), // 鄧: e984a7
            array("/\x8f\xe3\xa8/", "\xfb\xfe"), // 釚: e9879a
            array("/\x8f\xe3\xa6/", "\xfc\xa1"), // 釗: e98797
            array("/\x8f\xe3\xa9/", "\xfc\xa2"), // 釞: e9879e
            array("/\x8f\xe3\xaf/", "\xfc\xa3"), // 釭: e987ad
            array("/\x8f\xe3\xb0/", "\xfc\xa4"), // 釮: e987ae
            array("/\x8f\xe3\xaa/", "\xfc\xa5"), // 釤: e987a4
            array("/\x8f\xe3\xab/", "\xfc\xa6"), // 釥: e987a5
            array("/\x8f\xe3\xbc/", "\xfc\xa7"), // 鈆: e98886
            array("/\x8f\xe3\xc1/", "\xfc\xa8"), // 鈐: e98890
            array("/\x8f\xe3\xbf/", "\xfc\xa9"), // 鈊: e9888a
            array("/\x8f\xe3\xd5/", "\xfc\xaa"), // 鈺: e988ba
            array("/\x8f\xe3\xd8/", "\xfc\xab"), // 鉀: e98980
            array("/\x8f\xe3\xd6/", "\xfc\xac"), // 鈼: e988bc
            array("/\x8f\xe3\xdf/", "\xfc\xad"), // 鉎: e9898e
            array("/\x8f\xe3\xe3/", "\xfc\xae"), // 鉙: e98999
            array("/\x8f\xe3\xe1/", "\xfc\xaf"), // 鉑: e98991
            array("/\x8f\xe3\xd4/", "\xfc\xb0"), // 鈹: e988b9
            array("/\x8f\xe3\xe9/", "\xfc\xb1"), // 鉧: e989a7
            array("/\x8f\xe4\xa6/", "\xfc\xb2"), // 銧: e98aa7
            array("/\x8f\xe3\xf1/", "\xfc\xb3"), // 鉷: e989b7
            array("/\x8f\xe3\xf2/", "\xfc\xb4"), // 鉸: e989b8
            array("/\x8f\xe4\xcb/", "\xfc\xb5"), // 鋧: e98ba7
            array("/\x8f\xe4\xc1/", "\xfc\xb6"), // 鋗: e98b97
            array("/\x8f\xe4\xc3/", "\xfc\xb7"), // 鋙: e98b99
            array("/\x8f\xe4\xbe/", "\xfc\xb8"), // 鋐: e98b90
            array("/\x8f\xf4\xef/", "\xfc\xb9"), // ���: efa8a7
            array("/\x8f\xe4\xc0/", "\xfc\xba"), // 鋕: e98b95
            array("/\x8f\xe4\xc7/", "\xfc\xbb"), // 鋠: e98ba0
            array("/\x8f\xe4\xbf/", "\xfc\xbc"), // 鋓: e98b93
            array("/\x8f\xe4\xe0/", "\xfc\xbd"), // 錥: e98ca5
            array("/\x8f\xe4\xde/", "\xfc\xbe"), // 錡: e98ca1
            array("/\x8f\xe4\xd1/", "\xfc\xbf"), // 鋻: e98bbb
            array("/\x8f\xf4\xf0/", "\xfc\xc0"), // ���: efa8a8
            array("/\x8f\xe4\xdc/", "\xfc\xc1"), // 錞: e98c9e
            array("/\x8f\xe4\xd2/", "\xfc\xc2"), // 鋿: e98bbf
            array("/\x8f\xe4\xdb/", "\xfc\xc3"), // 錝: e98c9d
            array("/\x8f\xe4\xd4/", "\xfc\xc4"), // 錂: e98c82
            array("/\x8f\xe4\xfa/", "\xfc\xc5"), // 鍰: e98db0
            array("/\x8f\xe4\xef/", "\xfc\xc6"), // 鍗: e98d97
            array("/\x8f\xe5\xb3/", "\xfc\xc7"), // 鎤: e98ea4
            array("/\x8f\xe5\xbf/", "\xfc\xc8"), // 鏆: e98f86
            array("/\x8f\xe5\xc9/", "\xfc\xc9"), // 鏞: e98f9e
            array("/\x8f\xe5\xd0/", "\xfc\xca"), // 鏸: e98fb8
            array("/\x8f\xe5\xe2/", "\xfc\xcb"), // 鐱: e990b1
            array("/\x8f\xe5\xea/", "\xfc\xcc"), // 鑅: e99185
            array("/\x8f\xe5\xeb/", "\xfc\xcd"), // 鑈: e99188
            array("/\x8f\xf4\xf1/", "\xfc\xce"), // ���: e99692
            array("/\x8f\xf4\xf3/", "\xfc\xd0"), // ���: efa8a9
            array("/\x8f\xe6\xe8/", "\xfc\xd1"), // 隝: e99a9d
            array("/\x8f\xe6\xef/", "\xfc\xd2"), // 隯: e99aaf
            array("/\x8f\xe7\xac/", "\xfc\xd3"), // 霳: e99cb3
            array("/\x8f\xf4\xf4/", "\xfc\xd4"), // ���: e99cbb
            array("/\x8f\xe7\xae/", "\xfc\xd5"), // 靃: e99d83
            array("/\x8f\xf4\xf5/", "\xfc\xd6"), // ���: e99d8d
            array("/\x8f\xe7\xb1/", "\xfc\xd7"), // 靏: e99d8f
            array("/\x8f\xf4\xf6/", "\xfc\xd8"), // ���: e99d91
            array("/\x8f\xe7\xb2/", "\xfc\xd9"), // 靕: e99d95
            array("/\x8f\xe8\xb1/", "\xfc\xda"), // 顗: e9a197
            array("/\x8f\xe8\xb6/", "\xfc\xdb"), // 顥: e9a1a5
            array("/\x8f\xe8\xdd/", "\xfc\xde"), // 餧: e9a4a7
            array("/\x8f\xf4\xfa/", "\xfc\xe0"), // ���: e9a69e
            array("/\x8f\xe9\xd1/", "\xfc\xe1"), // 驎: e9a98e
            array("/\x8f\xf4\xfb/", "\xfc\xe2"), // ���: e9ab99
            array("/\x8f\xe9\xed/", "\xfc\xe3"), // 髜: e9ab9c
            array("/\x8f\xea\xcd/", "\xfc\xe4"), // 魵: e9adb5
            array("/\x8f\xf4\xfc/", "\xfc\xe5"), // ���: e9adb2
            array("/\x8f\xea\xdb/", "\xfc\xe6"), // 鮏: e9ae8f
            array("/\x8f\xea\xe6/", "\xfc\xe7"), // 鮱: e9aeb1
            array("/\x8f\xea\xea/", "\xfc\xe8"), // 鮻: e9aebb
            array("/\x8f\xeb\xa5/", "\xfc\xe9"), // 鰀: e9b080
            array("/\x8f\xeb\xfb/", "\xfc\xea"), // 鵰: e9b5b0
            array("/\x8f\xeb\xfa/", "\xfc\xeb"), // 鵫: e9b5ab
            array("/\x8f\xec\xd6/", "\xfc\xed"), // 鸙: e9b899
            array("/\x8f\xf4\xfe/", "\xfc\xee"), // ���: e9bb91
        );

        $eucjpwin = mb_convert_encoding($string, "eucJP-win", "UTF-8");
        foreach ($__UTF8_TO_EUCJP_MAP as $map) {
            $eucjpwin = preg_replace($map[0], $map[1], $eucjpwin);
        }
        return $eucjpwin;
    }

}

/**
 * filename meta check
 */
if (!function_exists("is_filename")) {

    function is_filename($filename) {
        return preg_match("/[\/:*?\"<>|'\\\]/", $filename) !== 1;
    }

}

/**
 * Smarty Function
 * プルダウンリスト
 *
 * @param type $params
 * @param type $smarty
 * @return type
 */
function smarty_pulldown($params, &$smarty) {
    $output = '';
    foreach ($params['array'] as $row) {
        $selected = "";
        if (!empty($params['id']) and $row['id'] == $params['id']) {
            $selected = 'selected';
        }
        $output.= sprintf(
            '<option value="%s" %s>%s</option>', htmlspecialchars($row['id'], ENT_QUOTES, 'euc-jp'), $selected, htmlspecialchars($row['name'], ENT_QUOTES, 'euc-jp')
        );
    }
    return $output;
}

/**
 * Debug Log
 */
if (!function_exists("cmx_log")) {

    function cmx_log($mes) {
        error_log(date('[Y-m-d H:i:s] ') . "$mes\n", 3, CMX_BASE_DIR . "/log/error.log");
    }

    function cmx_log_r($mes) {
        error_log(date('[Y-m-d H:i:s] ') . "[" . getmypid() . "]" . print_r($mes, true) . "\n", 3, CMX_BASE_DIR . "/log/error.log");
    }

}

/**
 * Converts PHP variable or array into a "JSON" (JavaScript value expression
 * or "object notation") string.
 *
 * @compat
 *    Output seems identical to PECL versions. "Only" 20x slower than PECL version.
 * @bugs
 *    Doesn't take care with unicode too much - leaves UTF-8 sequences alone.
 *
 * @param  $var mixed  PHP variable/array/object
 * @return string      transformed into JSON equivalent
 */
if (!function_exists("cmx_json_encode")) {

    function cmx_json_encode($var, /* emu_args */ $obj = FALSE) {

        #-- prepare JSON string
        $json = "";

        #-- add array entries
        if (is_array($var) || ($obj = is_object($var))) {

            #-- check if array is associative
            if (!$obj) foreach ((array) $var as $i => $v) {
                    if (!is_int($i)) {
                        $obj = 1;
                        break;
                    }
                }

            #-- concat invidual entries
            foreach ((array) $var as $i => $v) {
                $json .= ($json ? "," : "")    // comma separators
                    . ($obj ? ("\"$i\":") : "")   // assoc prefix
                    . (cmx_json_encode($v));    // value
            }

            #-- enclose into braces or brackets
            $json = $obj ? "{" . $json . "}" : "[" . $json . "]";
        }

        #-- strings need some care
        elseif (is_string($var)) {
            if (!utf8_decode($var)) {
                $var = utf8_encode($var);
            }
            $var = str_replace(array("\\", "\"", "/", "\b", "\f", "\n", "\r", "\t"), array("\\\\", '\"', "\\/", "\\b", "\\f", "\\n", "\\r", "\\t"), $var);
            $json = '"' . $var . '"';
            //@COMPAT: for fully-fully-compliance   $var = preg_replace("/[\000-\037]/", "", $var);
        }

        #-- basic types
        elseif (is_bool($var)) {
            $json = $var ? "true" : "false";
        }
        elseif ($var === NULL) {
            $json = "null";
        }
        elseif (is_int($var) || is_float($var)) {
            $json = "$var";
        }

        #-- something went wrong
        else {
            trigger_error("cmx_json_encode: don't know what a '" . gettype($var) . "' is.", E_USER_ERROR);
        }

        #-- done
        return($json);
    }

}


/**
 * Parses a JSON (JavaScript value expression) string into a PHP variable
 * (array or object).
 *
 * @compat
 *    Behaves similar to PECL version, but is less quiet on errors.
 *    Now even decodes unicode \uXXXX string escapes into UTF-8.
 *    "Only" 27 times slower than native function.
 * @bugs
 *    Might parse some misformed representations, when other implementations
 *    would scream error or explode.
 * @code
 *    This is state machine spaghetti code. Needs the extranous parameters to
 *    process subarrays, etc. When it recursively calls itself, $n is the
 *    current position, and $waitfor a string with possible end-tokens.
 *
 * @param   $json string   JSON encoded values
 * @param   $assoc bool    pack data into php array/hashes instead of objects
 * @return  mixed          parsed into PHP variable/array/object
 */
if (!function_exists("cmx_json_decode")) {

    function cmx_json_decode($json, $assoc = FALSE, $limit = 512, /* emu_args */ $n = 0, $state = 0, $waitfor = 0) {

        #-- result var
        $val = NULL;
        static $lang_eq = array("true" => TRUE, "false" => FALSE, "null" => NULL);
        static $str_eq = array("n" => "\012", "r" => "\015", "\\" => "\\", '"' => '"', "f" => "\f", "b" => "\b", "t" => "\t", "/" => "/");
        if ($limit < 0) return /* __cannot_compensate */;

        #-- flat char-wise parsing
        for (/* n */; $n < strlen($json); /* n */) {
            $c = $json[$n];

            #-= in-string
            if ($state === '"') {

                if ($c == '\\') {
                    $c = $json[++$n];
                    // simple C escapes
                    if (isset($str_eq[$c])) {
                        $val .= $str_eq[$c];
                    }

                    // here we transform \uXXXX Unicode (always 4 nibbles) references to UTF-8
                    elseif ($c == "u") {
                        // read just 16bit (therefore value can't be negative)
                        $hex = hexdec(substr($json, $n + 1, 4));
                        $n += 4;
                        // Unicode ranges
                        if ($hex < 0x80) {    // plain ASCII character
                            $val .= chr($hex);
                        }
                        elseif ($hex < 0x800) {   // 110xxxxx 10xxxxxx
                            $val .= chr(0xC0 + $hex >> 6) . chr(0x80 + $hex & 63);
                        }
                        elseif ($hex <= 0xFFFF) { // 1110xxxx 10xxxxxx 10xxxxxx
                            $val .= chr(0xE0 + $hex >> 12) . chr(0x80 + ($hex >> 6) & 63) . chr(0x80 + $hex & 63);
                        }
                        // other ranges, like 0x1FFFFF=0xF0, 0x3FFFFFF=0xF8 and 0x7FFFFFFF=0xFC do not apply
                    }

                    // no escape, just a redundant backslash
                    //@COMPAT: we could throw an exception here
                    else {
                        $val .= "\\" . $c;
                    }
                }

                // end of string
                elseif ($c == '"') {
                    $state = 0;
                }

                // yeeha! a single character found!!!!1!
                else/* if (ord($c) >= 32) */ { //@COMPAT: specialchars check - but native json doesn't do it?
                    $val .= $c;
                }
            }

            #-> end of sub-call (array/object)
            elseif ($waitfor && (strpos($waitfor, $c) !== false)) {
                return array($val, $n);  // return current value and state
            }

            #-= in-array
            elseif ($state === ']') {
                list($v, $n) = cmx_json_decode($json, $assoc, $limit, $n, 0, ",]");
                $val[] = $v;
                if ($json[$n] == "]") {
                    return array($val, $n);
                }
            }

            #-= in-object
            elseif ($state === '}') {
                list($i, $n) = cmx_json_decode($json, $assoc, $limit, $n, 0, ":");   // this allowed non-string indicies
                list($v, $n) = cmx_json_decode($json, $assoc, $limit, $n + 1, 0, ",}");
                $val[$i] = $v;
                if ($json[$n] == "}") {
                    return array($val, $n);
                }
            }

            #-- looking for next item (0)
            else {

                #-> whitespace
                if (preg_match("/\s/", $c)) {
                    // skip
                }

                #-> string begin
                elseif ($c == '"') {
                    $state = '"';
                }

                #-> object
                elseif ($c == "{") {
                    list($val, $n) = cmx_json_decode($json, $assoc, $limit - 1, $n + 1, '}', "}");

                    if ($val && $n) {
                        $val = $assoc ? (array) $val : (object) $val;
                    }
                }

                #-> array
                elseif ($c == "[") {
                    list($val, $n) = cmx_json_decode($json, $assoc, $limit - 1, $n + 1, ']', "]");
                }

                #-> comment
                elseif (($c == "/") && ($json[$n + 1] == "*")) {
                    // just find end, skip over
                    ($n = strpos($json, "*/", $n + 1)) or ($n = strlen($json));
                }

                #-> numbers
                elseif (preg_match("#^(-?\d+(?:\.\d+)?)(?:[eE]([-+]?\d+))?#", substr($json, $n), $uu)) {
                    $val = $uu[1];
                    $n += strlen($uu[0]) - 1;
                    if (strpos($val, ".")) {  // float
                        $val = (float) $val;
                    }
                    elseif ($val[0] == "0") {  // oct
                        $val = octdec($val);
                    }
                    else {
                        $val = (int) $val;
                    }
                    // exponent?
                    if (isset($uu[2])) {
                        $val *= pow(10, (int) $uu[2]);
                    }
                }

                #-> boolean or null
                elseif (preg_match("#^(true|false|null)\b#", substr($json, $n), $uu)) {
                    $val = $lang_eq[$uu[1]];
                    $n += strlen($uu[1]) - 1;
                }

                #-- parsing error
                else {
                    // PHPs native json_decode() breaks here usually and QUIETLY
                    trigger_error("json_decode: error parsing '$c' at position $n", E_USER_WARNING);
                    return $waitfor ? array(NULL, 1 << 30) : NULL;
                }
            }//state
            #-- next char
            if ($n === NULL) {
                return NULL;
            }
            $n++;
        }//for
        #-- final result
        return ($val);
    }

}

/**
 * CSV一行読込処理 ※ fgetcsvのバグ回避
 * http://yossy.iimp.jp/wp/?p=56
 */
if (!function_exists("fgetcsv_reg")) {

    function fgetcsv_reg(&$handle, $length = null, $d = ',', $e = '"') {
        $d = preg_quote($d);
        $e = preg_quote($e);
        $_line = "";
        $dummy = null;

        $eof = false;

        while (($eof != true) and ( !feof($handle))) {
            $_line .= (empty($length) ? fgets($handle) : fgets($handle, $length));
            $itemcnt = preg_match_all('/' . $e . '/', $_line, $dummy);
            if ($itemcnt % 2 == 0) {
                $eof = true;
            }
        }

        $_csv_line = preg_replace('/(?:\r\n|[\r\n])?$/', $d, trim($_line));
        $_csv_pattern = '/(' . $e . '[^' . $e . ']*(?:' . $e . $e . '[^' . $e . ']*)*' . $e . '|[^' . $d . ']*)' . $d . '/';

        $_csv_matches = array();
        preg_match_all($_csv_pattern, $_csv_line, $_csv_matches);

        $_csv_data = $_csv_matches[1];

        for ($_csv_i = 0; $_csv_i < count($_csv_data); $_csv_i++) {
            $_csv_data[$_csv_i] = preg_replace('/^' . $e . '(.*)' . $e . '$/s', '$1', $_csv_data[$_csv_i]);
            $_csv_data[$_csv_i] = str_replace($e . $e, $e, $_csv_data[$_csv_i]);
        }

        return empty($_line) ? false : $_csv_data;
    }

}
