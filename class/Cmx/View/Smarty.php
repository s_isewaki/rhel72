<?php

require_once('Cmx.php');
require_once('Smarty/Smarty.class.php');
require_once('HTML/FillInForm.class.php');

/**
 * CoMedix Viewクラス
 * Smarty v2のラッパー
 *
 * @link http://www.smarty.net/docsv2/ja/
 */
class Cmx_View extends Smarty {

    /**
     * コンストラクタ
     */
    function Cmx_View($tmpl_dir = '') {
        if (empty($tmpl_dir)) {
            $tmpl_dir = 'templates';
        }

        parent::Smarty();
        $this->template_dir = CMX_BASE_DIR . "/{$tmpl_dir}";
        $this->compile_dir = CMX_BASE_DIR . "/templates_c";
        $this->compile_id = substr(md5($tmpl_dir), 0, 8);
        $this->left_delimiter = '<{';
        $this->right_delimiter = '}>';
        $this->error_reporting = E_ALL & ~E_NOTICE;

        $this->assign('BASE_HREF', CMX_BASE_URL);
        $this->assign('SESSION', $_COOKIE["CMX_AUTH_SESSION"]);
    }

    /**
     * 出力 with HTML::FillInForm
     */
    function output_with_fill($resource_name, $fill) {

        $fill['scalar'] = parent::fetch($resource_name);
        $fill['escape'] = 'h';

        $fif = new HTML_FillInForm;
        return $fif->fill($fill);
    }

    /**
     * 描画 with HTML::FillInForm
     */
    function display_with_fill($resource_name, $fill) {
        print $this->output_with_fill($resource_name, $fill);
    }

}
