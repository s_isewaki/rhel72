<?
require(dirname(dirname(dirname(dirname(__FILE__))))."/conf/conf.inf");
require(dirname(dirname(dirname(dirname(__FILE__))))."/conf/conf_define.inf");
require_once(dirname(__FILE__)."/C2Utils.php"); // 汎用関数群
require_once(dirname(__FILE__)."/C2Env.php"); // 汎用関数群
require_once(dirname(__FILE__)."/C2DB.php"); // DBアクセッサ
c2Trace("[C2APP]","ロード:".__FILE__);

global $c2app;
$c2app = new _c2app();
// グローバルに存在する$c2appを返す。呼び元でglobal $c2appとしなくてよいように。
function c2GetC2App() {
    global $c2app;
    return $c2app;
}



class _c2app {


//*********************************************************************************************************************
// $_LBP
//*********************************************************************************************************************
var $_LBP = "";
function getLabelByProfile() {
    if (!$this->_LBP) {
        global $_label_by_profile;
        require_once("label_by_profile_type.ini");
        $this->_LBP = $_label_by_profile;
    }
    return $this->_LBP;
}



//*********************************************************************************************************************
// $_casTitle
//*********************************************************************************************************************
var $_casTitle = "";
function getCasTitle() {
    if (!$this->_casTitle) {
        require_once("get_cas_title_name.ini");
        $this->_casTitle = get_cas_title_name();
    }
    return $this->_casTitle;
}



//*********************************************************************************************************************
// $_headerIconCount
//*********************************************************************************************************************
// option/o_menu_setting.php
// employee_menu_bulk_setting.php
// employee_menu_setting.php
// employee_menu_group.php
var $_headerIconCount = 50;
function getHeaderIconCount() {
    return $this->_headerIconCount;
}



//*********************************************************************************************************************
// $_erasedAppIds
// 登録は「license/license_app.php」参照
//*********************************************************************************************************************
var $_erasedAppIds = ""; // 自動設定、編集不可
function getErasedAppIds() {
    if (!is_array($this->_erasedAppIds)) $this->_erasedAppIds = explode(",", c2dbGetSystemConfig("license.erased_appids"));
    return $this->_erasedAppIds;
}



//*********************************************************************************************************************
// $_profileRow
//*********************************************************************************************************************
// 【★★★自動アサイン／編集不可★★★】profileテーブルレコード
var $_profileRow = "";
function getProfileRow() {
    if (!$this->_profileRow) {
        $this->_profileRow = c2dbGetTopRow("select * from profile");
        if (!$this->_profileRow["prf_type"]) $this->_profileRow["prf_type"] = "1";
    }
    return $this->_profileRow;
}
function getPrfType() {
    $profileRow = $this->getProfileRow();
    return $profileRow["prf_type"];
}


//*********************************************************************************************************************
// $_intramenuRow
//*********************************************************************************************************************
var $_intramenuRow = "";
function getIntramenuRow() {
    if (!$this->_intramenuRow) {
        $this->_intramenuRow = c2dbGetTopRow("select * from intramenu");
        if ($this->getPrfType() == PROFILE_TYPE_WELFARE) {
            $this->_intramenuRow["menu2_4"] = mbereg_replace("・外来", "", $this->_intramenuRow["menu2_4"]);// 社会福祉法人の場合、「・外来」を削除
        }
    }
    return $this->_intramenuRow;
}


//*********************************************************************************************************************
// $_configRow
//*********************************************************************************************************************
var $_configRow = "";
function getConfigRow() {
    if (!$this->_configRow) $this->_configRow = c2dbGetTopRow("select * from config");
    return $this->_configRow;
}

//*********************************************************************************************************************
// $_optionRow
//*********************************************************************************************************************
var $_optionRow = array();
function getOptionRow($emp_id) {
    if (!$this->_optionRow[$emp_id]) {
        $row = c2dbGetTopRow("select * from option where emp_id = ".c2dbStr($emp_id));
        if (!$row) $row = array("default_page"=>"01"); // レコードが無い場合の初期値
        $this->_optionRow[$emp_id] = $row;
    }
    return $this->_optionRow[$emp_id];
}

//*********************************************************************************************************************
// $_dispgroupRow
//*********************************************************************************************************************
var $_dispgroupRow = array();
function getDispgroupRow($group_id) {
    if (!$this->_dispgroupRow[$group_id]) $this->_dispgroupRow[$group_id] = c2dbGetTopRow("select * from dispgroup where group_id = ".c2dbInt($group_id));
    return $this->_dispgroupRow[$group_id];
}

//*********************************************************************************************************************
// $_licenseRow
//*********************************************************************************************************************
var $_licenseRow = "";
function getlicenseRow() {
    if (!$this->_licenseRow) $this->_licenseRow = c2dbGetTopRow("select * from license");
    return $this->_licenseRow;
}

//*********************************************************************************************************************
// $_classnameRow
//*********************************************************************************************************************
var $_classnameRow = "";
function getClassnameRow() {
    if (!$this->_classnameRow) $this->_classnameRow = c2dbGetTopRow("select class_nm, atrb_nm, dept_nm, room_nm, class_cnt from classname");
    return $this->_classnameRow;
}

//*********************************************************************************************************************
// $_liboptionRow 文書管理オプション設定情報
//*********************************************************************************************************************
var $_liboptionRow = array();
function getLiboptionRow($emp_id) {
    if (!$this->_liboptionRow[$emp_id]) {
        $row = c2dbGetTopRow("select whatsnew_flg, whatsnew_days from liboption where emp_id = ".c2dbStr($emp_id));
        if (!$row) $row = array("lib_whatsnew_flg"=>"1", "lib_whatsnew_days"=>"7"); // レコードが無い場合の初期値
        $this->_liboptionRow[$emp_id] = $row;
    }
    return $this->_liboptionRow[$emp_id];
}

//*********************************************************************************************************************
// $_bbsoptionRow 掲示板オプション設定情報
//*********************************************************************************************************************
var $_bbsoptionRow = array();
function getBbsoptionRow($emp_id) {
    if (!$this->_bbsoptionRow[$emp_id]) {
        $row = c2dbGetTopRow("select whatsnew_flg, whatsnew_days from bbsoption where emp_id = ".c2dbStr($emp_id));
        if (!$row) $row = array("lib_whatsnew_flg"=>"1", "lib_whatsnew_days"=>"7"); // レコードが無い場合の初期値
        $this->_bbsoptionRow[$emp_id] = $row;
    }
    return $this->_bbsoptionRow[$emp_id];
}

//*********************************************************************************************************************
// $_authmstRows
//*********************************************************************************************************************
var $_authmstRows = array();
function getAuthmstRow($emp_id) {
    if (!$this->_authmstRows[$emp_id]) $this->_authmstRows[$emp_id] = c2dbGetTopRow("select * from authmst where emp_id = ".c2dbStr($emp_id));
    return $this->_authmstRows[$emp_id];
}

//*********************************************************************************************************************
// $_authmstRows
//*********************************************************************************************************************
var $_appNewNames = "";
function getAppNewNames() {
    if (!$this->_appNewNames) {
        $this->_appNewNames = unserialize(c2dbGetSystemConfig("license.app_new_names"));
        if (!$this->_appNewNames) $this->_appNewNames = array();
    }
    return $this->_appNewNames;
}







//*********************************************************************************************************************
// $_applications
//
// アプリ一覧
// urlがあるものは、直接URLをたたいて呼び出せる独立機能一覧である。トップページ袖のメニュー一覧であり、ヘッダアイコン指定も可能な一覧
// urlがないものは、ページは無いがアプリとしてみなせる単位である。
// アプリとしてみなせるものは、ライセンスもその単位で付与可能となる。
// 定義順はライセンス管理⇒アプリケーション表示順となる
// jp ライセンス購入画面で利用
// のちのち「lcs_ng」「lcs_max」「app_ng」フィールド追加
//*********************************************************************************************************************
var $_applications = array(
    "mypage"    => array("erasable"=>"",  "auth_flag"=>"ext_mypage_flg",   "icon20"=>"s00", "icon41"=>"h00", "jp"=>"マイページ"            ),
    "webml"     => array("erasable"=>"1", "auth_flag"=>"emp_webml_flg",    "icon20"=>"s01", "icon41"=>"h01", "jp"=>"ウェブメール"          ),
    "schd"      => array("erasable"=>"1", "auth_flag"=>"emp_schd_flg",     "icon20"=>"s02", "icon41"=>"h02", "jp"=>"スケジュール"          ),
    "pjt"       => array("erasable"=>"1", "auth_flag"=>"emp_pjt_flg",      "icon20"=>"s03", "icon41"=>"h03", "jp"=>"委員会・WG"            ),
    "newsuser"  => array("erasable"=>"1", "auth_flag"=>"emp_newsuser_flg", "icon20"=>"s35", "icon41"=>"h35", "jp"=>"お知らせ・回覧板"      ),
    "work"      => array("erasable"=>"1", "auth_flag"=>"emp_work_flg",     "icon20"=>"s04", "icon41"=>"h04", "jp"=>"タスク"                ),
    "phone"     => array("erasable"=>"1", "auth_flag"=>"emp_phone_flg",    "icon20"=>"s05", "icon41"=>"h05", "jp"=>"伝言メモ"              ),
    "resv"      => array("erasable"=>"1", "auth_flag"=>"emp_resv_flg",     "icon20"=>"s06", "icon41"=>"h06", "jp"=>"設備予約"              ),
    "attdcd"    => array("erasable"=>"1", "auth_flag"=>"emp_attdcd_flg",   "icon20"=>"s07", "icon41"=>"h07", "jp"=>"出勤表"                ),
    "aprv"      => array("erasable"=>"1", "auth_flag"=>"emp_aprv_flg",     "icon20"=>"s08", "icon41"=>"h08", "jp"=>"決裁・申請"            ),
    "conf"      => array("erasable"=>"1", "auth_flag"=>"emp_conf_flg",     "icon20"=>"s09", "icon41"=>"h09", "jp"=>"ネットカンファレンス"  ),
    "bbs"       => array("erasable"=>"1", "auth_flag"=>"emp_bbs_flg",      "icon20"=>"s10", "icon41"=>"h10", "jp"=>"掲示板・電子会議室"    ),
    "qa"        => array("erasable"=>"1", "auth_flag"=>"emp_qa_flg",       "icon20"=>"s11", "icon41"=>"h11", "jp"=>"Q&amp;A"               ),
    "lib"       => array("erasable"=>"1", "auth_flag"=>"emp_lib_flg",      "icon20"=>"s12", "icon41"=>"h12", "jp"=>"文書管理"              ),
    "adbk"      => array("erasable"=>"1", "auth_flag"=>"emp_adbk_flg",     "icon20"=>"s13", "icon41"=>"h13", "jp"=>"アドレス帳"            ),
    "link"      => array("erasable"=>"1", "auth_flag"=>"emp_link_flg",     "icon20"=>"s40", "icon41"=>"h40", "jp"=>"リンクライブラリ"      ),
    "ext"       => array("erasable"=>"1", "auth_flag"=>"emp_ext_flg",      "icon20"=>"s36", "icon41"=>"h36", "jp"=>"内線電話帳"            ),
    "pass1"     => array("erasable"=>"1", "auth_flag"=>"emp_pass_flg",     "icon20"=>"s14", "icon41"=>"h14", "jp"=>"パスワード・本人情報"  ),
    "option"    => array("erasable"=>"1", "auth_flag"=>"ext_option_flg",   "icon20"=>"s15", "icon41"=>"h15", "jp"=>"オプション設定"        ),
    "intra"     => array("erasable"=>"1", "auth_flag"=>"emp_intra_flg",    "icon20"=>"s38", "icon41"=>"h38", "jp"=>"イントラネット"        ),
    "search"    => array("erasable"=>"1", "auth_flag"=>"emp_search_flg",   "icon20"=>"s41", "icon41"=>"h41", "jp"=>"検索ちゃん"            ),
    "amb"       => array("erasable"=>"1", "auth_flag"=>"emp_amb_flg",      "icon20"=>"s44", "icon41"=>"h44", "jp"=>"看護支援"              ),
    "ward"      => array("erasable"=>"1", "auth_flag"=>"emp_ward_flg",     "icon20"=>"s17", "icon41"=>"h17", "jp"=>"★穴埋★"              ),// 病床管理/居室管理
    "nlcs"      => array("erasable"=>"1", "auth_flag"=>"emp_nlcs_flg",     "icon20"=>"s44", "icon41"=>"h44", "jp"=>"看護観察記録"          ),
    "inci"      => array("erasable"=>"1", "auth_flag"=>"emp_inci_flg",     "icon20"=>"s20", "icon41"=>"h20", "jp"=>"ファントルくん"        ),
    "fplus"     => array("erasable"=>"1", "auth_flag"=>"emp_fplus_flg",    "icon20"=>"s47", "icon41"=>"h47", "jp"=>"ファントルくん＋"      ),
    "manabu"    => array("erasable"=>"1", "auth_flag"=>"emp_manabu_flg",   "icon20"=>"s45", "icon41"=>"h45", "jp"=>"バリテス"              ),
    "med"       => array("erasable"=>"1", "auth_flag"=>"emp_med_flg",      "icon20"=>"s39", "icon41"=>"h39", "jp"=>"★穴埋★"              ), // メドレポート
    "kview"     => array("erasable"=>"1", "auth_flag"=>"emp_kview_flg",    "icon20"=>"s46", "icon41"=>"h46", "jp"=>"カルテビューワー"      ),
    "shift"     => array("erasable"=>"1", "auth_flag"=>"emp_shift_flg",    "icon20"=>"s43", "icon41"=>"h43", "jp"=>"勤務シフト作成"        ),
    "cas"       => array("erasable"=>"1", "auth_flag"=>"emp_cas_flg",      "icon20"=>"s42", "icon41"=>"h42", "jp"=>"★穴埋★"              ), // CAS get_cas_title_name.php
    "jinji"     => array("erasable"=>"1", "auth_flag"=>"emp_jinji_flg",    "icon20"=>"s50", "icon41"=>"h50", "jp"=>"人事管理"              ),
    "ladder"    => array("erasable"=>"1", "auth_flag"=>"emp_ladder_flg",   "icon20"=>"s51", "icon41"=>"h51", "jp"=>"クリニカルラダー"      ),
    "career"    => array("erasable"=>"1", "auth_flag"=>"emp_career_flg",   "icon20"=>"s53", "icon41"=>"h53", "jp"=>"キャリア開発ラダー"    ),
    "shift2"    => array("erasable"=>"1", "auth_flag"=>"emp_shift2_flg",   "icon20"=>"duty_shift", "icon41"=>"h43", "jp"=>"勤務表"         ),
    "jnl"       => array("erasable"=>"1", "auth_flag"=>"emp_jnl_flg",      "icon20"=>"s48", "icon41"=>"h48", "jp"=>"日報・月報"            ),
    "biz"       => array("erasable"=>"1", "auth_flag"=>"emp_biz_flg",      "icon20"=>"s18", "icon41"=>"h18", "jp"=>"経営支援"              ),
    "news"      => array("erasable"=>"1", "auth_flag"=>"emp_news_flg",     "icon20"=>"s21", "icon41"=>"", "jp"=>"お知らせ・回覧板管理"  ),
    "tmgd"      => array("erasable"=>"1", "auth_flag"=>"emp_tmgd_flg",     "icon20"=>"s22", "icon41"=>"", "jp"=>"★穴埋★"              ),//院内行事登録
    "wkadm"     => array("erasable"=>"1", "auth_flag"=>"emp_wkadm_flg",    "icon20"=>"s23", "icon41"=>"", "jp"=>"勤務管理"              ),
    "wkflw"     => array("erasable"=>"1", "auth_flag"=>"emp_wkflw_flg",    "icon20"=>"s24", "icon41"=>"", "jp"=>"ワークフロー"          ),
    "cmtadm"    => array("erasable"=>"1", "auth_flag"=>"emp_cmtadm_flg",   "icon20"=>"s25", "icon41"=>"", "jp"=>"コミュニティサイト管理"),
    "cmt"       => array("erasable"=>"1", "auth_flag"=>"emp_cmt_flg",      "icon20"=>"", "icon41"=>"", "jp"=>"コミュニティサイト"    ),
    "empif"     => array("erasable"=>"",  "auth_flag"=>"emp_empif_flg",    "icon20"=>"s26", "icon41"=>"", "jp"=>"職員登録"              ),
    "master"    => array("erasable"=>"",  "auth_flag"=>"emp_master_flg",   "icon20"=>"s27", "icon41"=>"", "jp"=>"マスターメンテナンス"  ),
    "config"    => array("erasable"=>"",  "auth_flag"=>"emp_config_flg",   "icon20"=>"s28", "icon41"=>"", "jp"=>"環境設定"              ),
    "lcs"       => array("erasable"=>"",  "auth_flag"=>"emp_lcs_flg",      "icon20"=>"s29", "icon41"=>"", "jp"=>"ライセンス管理"        ),
    "accesslog" => array("erasable"=>"",  "auth_flag"=>"emp_accesslog_flg","icon20"=>"s_dmy_log", "icon41"=>"", "jp"=>"アクセスログ"          ),
    // 新しく追加。これらはURLなし。ヘッダアイコン、サイドメニューには存在しない。
    "memo"      => array("erasable"=>"1", "auth_flag"=>"emp_memo_flg",     "icon20"=>"", "icon41"=>"", "jp"=>"備忘録"                ), //ﾏｲﾍﾟｰｼﾞ
    "cauth"     => array("erasable"=>"",  "auth_flag"=>"emp_cauth_flg",    "icon20"=>"", "icon41"=>"", "jp"=>"端末認証"              ),
    "entity"    => array("erasable"=>"1", "auth_flag"=>"emp_entity_flg",   "icon20"=>"", "icon41"=>"", "jp"=>"★穴埋★"              ), // 診療科
    // カスタム権限。DBに利用権限フラグが単独で存在しないもの（その１）
    "patient"   => array("erasable"=>"1", "auth_flag"=>"ext_patient_flg",  "icon20"=>"s16", "icon41"=>"h16", "jp"=>"★穴埋★"              ), // 患者管理/利用者管理
    // マイページ内アプリ。DBに利用権限が重複で存在
    "schdsrch"  => array("erasable"=>"1", "auth_flag"=>"emp_schd_flg",     "icon20"=>"", "icon41"=>"", "jp"=>"スケジュール検索"      ), //ﾏｲﾍﾟｰｼﾞへの表示機能
    // マイページ内アプリ。DBに利用権限フラグが単独で存在しないもの（その２）
    "freetext"  => array("erasable"=>"1", "auth_flag"=>"ext_freetext_flg", "icon20"=>"", "icon41"=>"", "jp"=>"フリーテキスト"        ), //ﾏｲﾍﾟｰｼﾞへの表示機能
    "tcarderr"  => array("erasable"=>"1", "auth_flag"=>"ext_tcarderr_flg", "icon20"=>"", "icon41"=>"", "jp"=>"打刻エラー"            ), //ﾏｲﾍﾟｰｼﾞへの表示機能
    "whatsnew"  => array("erasable"=>"1", "auth_flag"=>"ext_whatsnew_flg", "icon20"=>"", "icon41"=>"", "jp"=>"最新情報"              ), //ﾏｲﾍﾟｰｼﾞへの表示機能
    "wic"       => array("erasable"=>"1", "auth_flag"=>"ext_wic_flg",      "icon20"=>"", "icon41"=>"", "jp"=>"WICレポート"           ), //ﾏｲﾍﾟｰｼﾞへの表示機能
    // 未来の予備
    "ccusr1"    => array("erasable"=>"1", "auth_flag"=>"emp_ccusr1_flg",   "icon20"=>"s53", "icon41"=>"h53", "jp"=>"スタッフ・ポートフォリオ"),
    "ccusr2"    => array("erasable"=>"1", "auth_flag"=>"emp_ccusr2_flg",   "icon20"=>"s53", "icon41"=>"h53", "jp"=>"未来ｱﾌﾟﾘ2"             ),
    "ccusr3"    => array("erasable"=>"1", "auth_flag"=>"emp_ccusr3_flg",   "icon20"=>"s53", "icon41"=>"h53", "jp"=>"未来ｱﾌﾟﾘ3"             ),
    "ccusr4"    => array("erasable"=>"1", "auth_flag"=>"emp_ccusr4_flg",   "icon20"=>"s53", "icon41"=>"h53", "jp"=>"未来ｱﾌﾟﾘ4"             ),
    "ccusr5"    => array("erasable"=>"1", "auth_flag"=>"emp_ccusr5_flg",   "icon20"=>"s53", "icon41"=>"h53", "jp"=>"未来ｱﾌﾟﾘ5"             ),
    "ccusr6"    => array("erasable"=>"1", "auth_flag"=>"emp_ccusr6_flg",   "icon20"=>"s53", "icon41"=>"h53", "jp"=>"未来ｱﾌﾟﾘ6"             ),
    "ccusr7"    => array("erasable"=>"1", "auth_flag"=>"emp_ccusr7_flg",   "icon20"=>"s53", "icon41"=>"h53", "jp"=>"未来ｱﾌﾟﾘ7"             ),
    "ccusr8"    => array("erasable"=>"1", "auth_flag"=>"emp_ccusr8_flg",   "icon20"=>"s53", "icon41"=>"h53", "jp"=>"未来ｱﾌﾟﾘ8"             ),
    "ccusr9"    => array("erasable"=>"1", "auth_flag"=>"emp_ccusr9_flg",   "icon20"=>"s53", "icon41"=>"h53", "jp"=>"未来ｱﾌﾟﾘ9"             ),
    "ccusr10"   => array("erasable"=>"1", "auth_flag"=>"emp_ccusr10_flg",  "icon20"=>"s53", "icon41"=>"h53", "jp"=>"未来ｱﾌﾟﾘ10"            ),
    "ccusr11"   => array("erasable"=>"1", "auth_flag"=>"emp_ccusr11_flg",  "icon20"=>"s53", "icon41"=>"h53", "jp"=>"未来ｱﾌﾟﾘ11"            ),
    "ccusr12"   => array("erasable"=>"1", "auth_flag"=>"emp_ccusr12_flg",  "icon20"=>"s53", "icon41"=>"h53", "jp"=>"未来ｱﾌﾟﾘ12"            ),
    "ccusr13"   => array("erasable"=>"1", "auth_flag"=>"emp_ccusr13_flg",  "icon20"=>"s53", "icon41"=>"h53", "jp"=>"未来ｱﾌﾟﾘ13"            ),
    "ccusr14"   => array("erasable"=>"1", "auth_flag"=>"emp_ccusr14_flg",  "icon20"=>"s53", "icon41"=>"h53", "jp"=>"未来ｱﾌﾟﾘ14"            ),
    "ccusr15"   => array("erasable"=>"1", "auth_flag"=>"emp_ccusr15_flg",  "icon20"=>"s53", "icon41"=>"h53", "jp"=>"未来ｱﾌﾟﾘ15"            ),
    "omusr1"    => array("erasable"=>"1", "auth_flag"=>"emp_omusr1_flg",   "icon20"=>"s53", "icon41"=>"h53", "jp"=>"★特製1穴埋★"         ),
    "omusr2"    => array("erasable"=>"1", "auth_flag"=>"emp_omusr2_flg",   "icon20"=>"s53", "icon41"=>"h53", "jp"=>"★特製2穴埋★"         ),
    "omusr3"    => array("erasable"=>"1", "auth_flag"=>"emp_omusr3_flg",   "icon20"=>"s53", "icon41"=>"h53", "jp"=>"★特製3穴埋★"         ),
    "omusr4"    => array("erasable"=>"1", "auth_flag"=>"emp_omusr4_flg",   "icon20"=>"s53", "icon41"=>"h53", "jp"=>"★特製4穴埋★"         ),
    "omusr5"    => array("erasable"=>"1", "auth_flag"=>"emp_omusr5_flg",   "icon20"=>"s53", "icon41"=>"h53", "jp"=>"★特製5穴埋★"         ),
  //"ccadm1"    => array("erasable"=>"1", "auth_flag"=>"emp_ccadm1_flg",   "icon20"=>"s53", "icon41"=>"h53", "jp"=>"SPF管理"               ), // 存在しない。コメントアウト
    "ccadm2"    => array("erasable"=>"1", "auth_flag"=>"emp_ccadm2_flg",   "icon20"=>"s53", "icon41"=>"h53", "jp"=>"未来ｱﾌﾟﾘ2管理"         ),
    "ccadm3"    => array("erasable"=>"1", "auth_flag"=>"emp_ccadm3_flg",   "icon20"=>"s53", "icon41"=>"h53", "jp"=>"未来ｱﾌﾟﾘ3管理"         ),
    "ccadm4"    => array("erasable"=>"1", "auth_flag"=>"emp_ccadm4_flg",   "icon20"=>"s53", "icon41"=>"h53", "jp"=>"未来ｱﾌﾟﾘ4管理"         ),
    "ccadm5"    => array("erasable"=>"1", "auth_flag"=>"emp_ccadm5_flg",   "icon20"=>"s53", "icon41"=>"h53", "jp"=>"未来ｱﾌﾟﾘ5管理"         ),
    "ccadm6"    => array("erasable"=>"1", "auth_flag"=>"emp_ccadm6_flg",   "icon20"=>"s53", "icon41"=>"h53", "jp"=>"未来ｱﾌﾟﾘ6管理"         ),
    "ccadm7"    => array("erasable"=>"1", "auth_flag"=>"emp_ccadm7_flg",   "icon20"=>"s53", "icon41"=>"h53", "jp"=>"未来ｱﾌﾟﾘ7管理"         ),
    "ccadm8"    => array("erasable"=>"1", "auth_flag"=>"emp_ccadm8_flg",   "icon20"=>"s53", "icon41"=>"h53", "jp"=>"未来ｱﾌﾟﾘ8管理"         ),
    "ccadm9"    => array("erasable"=>"1", "auth_flag"=>"emp_ccadm9_flg",   "icon20"=>"s53", "icon41"=>"h53", "jp"=>"未来ｱﾌﾟﾘ9管理"         ),
    "ccadm10"   => array("erasable"=>"1", "auth_flag"=>"emp_ccadm10_flg",  "icon20"=>"s53", "icon41"=>"h53", "jp"=>"未来ｱﾌﾟﾘ10管理"        ),
    "ccadm11"   => array("erasable"=>"1", "auth_flag"=>"emp_ccadm11_flg",  "icon20"=>"s53", "icon41"=>"h53", "jp"=>"未来ｱﾌﾟﾘ11管理"        ),
    "ccadm12"   => array("erasable"=>"1", "auth_flag"=>"emp_ccadm12_flg",  "icon20"=>"s53", "icon41"=>"h53", "jp"=>"未来ｱﾌﾟﾘ12管理"        ),
    "ccadm13"   => array("erasable"=>"1", "auth_flag"=>"emp_ccadm13_flg",  "icon20"=>"s53", "icon41"=>"h53", "jp"=>"未来ｱﾌﾟﾘ13管理"        ),
    "ccadm14"   => array("erasable"=>"1", "auth_flag"=>"emp_ccadm14_flg",  "icon20"=>"s53", "icon41"=>"h53", "jp"=>"未来ｱﾌﾟﾘ14管理"        ),
    "ccadm15"   => array("erasable"=>"1", "auth_flag"=>"emp_ccadm15_flg",  "icon20"=>"s53", "icon41"=>"h53", "jp"=>"未来ｱﾌﾟﾘ15管理"        ),
    "omadm1"    => array("erasable"=>"1", "auth_flag"=>"emp_omadm1_flg",   "icon20"=>"s53", "icon41"=>"h53", "jp"=>"★特製1管理穴埋★"     ),
    "omadm2"    => array("erasable"=>"1", "auth_flag"=>"emp_omadm2_flg",   "icon20"=>"s53", "icon41"=>"h53", "jp"=>"★特製2管理穴埋★"     ),
    "omadm3"    => array("erasable"=>"1", "auth_flag"=>"emp_omadm3_flg",   "icon20"=>"s53", "icon41"=>"h53", "jp"=>"★特製3管理穴埋★"     ),
    "omadm4"    => array("erasable"=>"1", "auth_flag"=>"emp_omadm4_flg",   "icon20"=>"s53", "icon41"=>"h53", "jp"=>"★特製4管理穴埋★"     ),
    "omadm5"    => array("erasable"=>"1", "auth_flag"=>"emp_omadm5_flg",   "icon20"=>"s53", "icon41"=>"h53", "jp"=>"★特製5管理穴埋★"     ),
    // カスタム権限。DBに利用権限フラグが単独で存在しないもの（その３）
    "pass2"     => array("erasable"=>"",  "auth_flag"=>"ext_pass_flg",     "icon20"=>"s53", "icon41"=>"h53", "jp"=>"本人情報"              ),
    "openclose" => array("erasable"=>"",  "auth_flag"=>"ext_openclose_flg","icon20"=>"s53", "icon41"=>"h98", "jp"=>"open/close"            ),
    "logout"    => array("erasable"=>"",  "auth_flag"=>"ext_logout_flg",   "icon20"=>"", "icon41"=>"h99", "jp"=>"ログアウト"            )
);
var $_applicationsNameInitialized = 0;
function getApplicationsNames() {
    if ($this->_applicationsNameInitialized) return $this->_applications;
    $erasedAppIds = $this->getErasedAppIds();
    $appNewNames = $this->getAppNewNames();
    foreach($this->_applications as $appid => $row) {
        $this->_applications[$appid]["lcs_ng"] = "";
        $auth_flag = $row["auth_flag"];
        if (in_array($appid, $erasedAppIds)) $this->_applications[$appid]["app_ng"] = "1";
        if ($appNewNames[$appid]) $this->_applications[$appid]["jp"] = $appNewNames[$appid];
    }
    $licenseRow = $this->getLicenseRow();
    $licenseIdToAppId = $this->getLicenseIdToAppId();

    foreach ($licenseRow as $field => $v) {
        if (substr($field, 0, 8) != "lcs_func") continue;
        $lcsidx = substr($field, 8);
        $appid = $licenseIdToAppId[$lcsidx];
        if ($appid && !$v) $this->_applications[$appid]["lcs_ng"] = "1";
    }
    for ($idx=1; $idx<=5; $idx++) {
        $row = unserialize($licenseRow["lcs_om".$idx."_custom"]);
        $this->_applications["omusr".$idx]["icon20"] = $row["app_usr_icon20"];
        $this->_applications["omusr".$idx]["icon41"] = $row["app_usr_icon41"];
        $this->_applications["omadm".$idx]["icon20"] = $row["app_adm_icon20"];
        $this->_applications["omadm".$idx]["icon41"] = $row["app_adm_icon41"];

        if (!$row["kousei"] || $row["kousei"]=="omadm_only") {
            unset($this->_applications["omusr".$idx]);
        }
        if (!$row["kousei"] || $row["kousei"]=="omusr_only") {
            unset($this->_applications["omadm".$idx]);
        }
    }

    $LBP = $this->getLabelByProfile();
    $prf_type = $this->getPrfType();

    $this->_applications["cas"]["jp"]     = $this->getCasTitle(); // CAS
    $this->_applications["ward"]["jp"]    = $LBP["BED_MANAGE"][$prf_type]; // 病床管理
    $this->_applications["med"]["jp"]     = $LBP["MED_REPORT"][$prf_type]; // メドレポート / POレポート
    $this->_applications["tmgd"]["jp"]    = $LBP["EVENT_REG"][$prf_type];  // 院内行事/法人内行事
    $this->_applications["patient"]["jp"] = $LBP["PATIENT_MANAGE"][$prf_type]; // 患者管理/利用者管理
    $this->_applications["entity"]["jp"]  = $LBP["SECTION"][$prf_type]; // 診療科
    $this->_applicationsNameInitialized = 1;
    return $this->_applications;
}
var $_applicationsInitialized = 0;
function getApplications() {
    if ($_applicationsInitialized) return $this->_applications;
    $this->getApplicationsNames();
    $authmstExp = $this->getAuthmstExp("");
    foreach($this->_applications as $appid => $row) {
        $auth_flag = $row["auth_flag"];
        $authmst = $authmstExp[$auth_flag];
        $this->_applications[$appid]["lcs_max"] = $authmst["lcs_max"];
    }
    $this->_applicationsInitialized = 1;
    return $this->_applications;
}
function getApplicationInfo($appid) {
    $applications = $this->getApplications();
    return $applications[$appid];
}
function getApplicationName($appid) {
    $info = $this->getApplicationInfo($appid);
    return $info["jp"];
}


//*********************************************************************************************************************
// $_applicationUrl
//
// アプリURL一覧
// urlがあるものは、直接URLをたたいて呼び出せる独立機能一覧である。トップページ袖のメニュー一覧であり、ヘッダアイコン指定も可能な一覧
// urlがないものは、ページは無いがアプリとしてみなせる単位である。
// 定義順は意味なし
//*********************************************************************************************************************
var $_applicationUrl = array(
    "mypage"    => array("url"=>"left.php"                  ),
    "webml"     => array("url"=>"webmail_menu.php"          ),
    "schd"      => array("url"=>"★穴埋★"                  ),
    "pjt"       => array("url"=>"project_menu.php"          ),
    "newsuser"  => array("url"=>"newsuser_menu.php"         ),
    "work"      => array("url"=>"work_menu.php"             ),
    "phone"     => array("url"=>"dengon/m_receive_list.php" ),
    "resv"      => array("url"=>"★穴埋★"                  ),
    "attdcd"    => array("url"=>"★穴埋★"                  ),
    "aprv"      => array("url"=>"application_menu.php"      ),
    "conf"      => array("url"=>"net_conference_menu.php"   ),
    "bbs"       => array("url"=>"bbsthread_menu.php"        ),
    "qa"        => array("url"=>"qa_category_menu.php"      ,"purl"=>"&qa=1"),
    "lib"       => array("url"=>"library_list_all.php"      ,"purl"=>"&library=1"),
    "adbk"      => array("url"=>"address_menu.php"          ),
    "link"      => array("url"=>"link_menu.php"             ),
    "ext"       => array("url"=>"extension_menu.php"        ,"purl"=>"&extension=1"),
    "pass1"     => array("url"=>"passwd_change.php"         ),
    "option"    => array("url"=>"option/o_register.php"     ),
    "intra"     => array("url"=>"intra_menu.php"            ),
    "search"    => array("url"=>"kensaku_byoumei.php"       ),
    "amb"       => array("url"=>"kangoshien/ks_index.php"   ),
    "ward"      => array("url"=>"bed_menu.php"              ),
    "nlcs"      => array("url"=>"kango_top.php"             ),
    "inci"      => array("url"=>"hiyari_menu.php"           ),
    "fplus"     => array("url"=>"fplus_menu.php"            ),
    "manabu"    => array("url"=>"study/study_menu_user.php" ),
    "med"       => array("url"=>"★穴埋★"                  ),
    "kview"     => array("url"=>"kview_menu.php"            ),
    "shift"     => array("url"=>"duty_shift_results.php"    ),
    "cas"       => array("url"=>"cas_application_menu.php"  ),
    "jinji"     => array("url"=>"jinji_menu.php"            ),
    "ladder"    => array("url"=>"ladder_menu.php"           ),
    "career"    => array("url"=>"ladder/index.php"          ),
    "shift2"    => array("url"=>"shift/index.php"           ),
    "jnl"       => array("url"=>"jnl_application_menu.php"  ),
    "biz"       => array("url"=>"biz_menu.php"              ),
    "news"      => array("url"=>"news_menu.php"             ,"purl"=>"&news=2"),
    "tmgd"      => array("url"=>"time_guide_list.php"       ,"purl"=>"&event=2"),
    "wkadm"     => array("url"=>"work_admin_menu.php"       ,"purl"=>"&work=2"),
    "wkflw"     => array("url"=>"workflow_menu.php"         ,"purl"=>"&workflow=2"),
    "cmtadm"    => array("url"=>"community_admin_menu.php"  ),
    "cmt"       => array("url"=>"community_menu.php"        ),
    "empif"     => array("url"=>"employee_info_menu.php"    ),
    "master"    => array("url"=>"maintenance_menu.php"      ),
    "config"    => array("url"=>"config_register.php"       ),
    "lcs"       => array("url"=>"license/license_menu.php"  ),
    "accesslog" => array("url"=>"aclg_menu.php"             ),
    "patient"   => array("url"=>"patient_info_menu.php"     ),
    "ccusr1"    => array("url"=>"spf/index.php"             ),
    "ccusr2"    => array("url"=>"ccusr2_sample/index.php"   ),
    "ccusr3"    => array("url"=>"ccusr3_sample/index.php"   ),
    "ccusr4"    => array("url"=>"ccusr4_sample/index.php"   ),
    "ccusr5"    => array("url"=>"ccusr5_sample/index.php"   ),
    "ccusr6"    => array("url"=>"ccusr6_sample/index.php"   ),
    "ccusr7"    => array("url"=>"ccusr7_sample/index.php"   ),
    "ccusr8"    => array("url"=>"ccusr8_sample/index.php"   ),
    "ccusr9"    => array("url"=>"ccusr9_sample/index.php"   ),
    "ccusr10"   => array("url"=>"ccusr10_sample/index.php"  ),
    "ccusr11"   => array("url"=>"ccusr11_sample/index.php"  ),
    "ccusr12"   => array("url"=>"ccusr12_sample/index.php"  ),
    "ccusr13"   => array("url"=>"ccusr13_sample/index.php"  ),
    "ccusr14"   => array("url"=>"ccusr14_sample/index.php"  ),
    "ccusr15"   => array("url"=>"ccusr15_sample/index.php"  ),
    "omusr1"    => array("url"=>"omusr1_sample/index.php"   ),
    "omusr2"    => array("url"=>"omusr2_sample/index.php"   ),
    "omusr3"    => array("url"=>"omusr3_sample/index.php"   ),
    "omusr4"    => array("url"=>"omusr4_sample/index.php"   ),
    "omusr5"    => array("url"=>"omusr5_sample/index.php"   ),
  //"ccadm1"    => array("url"=>"spf/index.php"             ), // 存在しないものなのでコメントアウト
    "ccadm2"    => array("url"=>"ccadm2_sample/index.php"   ),
    "ccadm3"    => array("url"=>"ccadm3_sample/index.php"   ),
    "ccadm4"    => array("url"=>"ccadm4_sample/index.php"   ),
    "ccadm5"    => array("url"=>"ccadm5_sample/index.php"   ),
    "ccadm6"    => array("url"=>"ccadm6_sample/index.php"   ),
    "ccadm7"    => array("url"=>"ccadm7_sample/index.php"   ),
    "ccadm8"    => array("url"=>"ccadm8_sample/index.php"   ),
    "ccadm9"    => array("url"=>"ccadm9_sample/index.php"   ),
    "ccadm10"   => array("url"=>"ccadm10_sample/index.php"  ),
    "ccadm11"   => array("url"=>"ccadm11_sample/index.php"  ),
    "ccadm12"   => array("url"=>"ccadm12_sample/index.php"  ),
    "ccadm13"   => array("url"=>"ccadm13_sample/index.php"  ),
    "ccadm14"   => array("url"=>"ccadm14_sample/index.php"  ),
    "ccadm15"   => array("url"=>"ccadm15_sample/index.php"  ),
    "omadm1"    => array("url"=>"omadm1_sample/index.php"   ),
    "omadm2"    => array("url"=>"omadm2_sample/index.php"   ),
    "omadm3"    => array("url"=>"omadm3_sample/index.php"   ),
    "omadm4"    => array("url"=>"omadm4_sample/index.php"   ),
    "omadm5"    => array("url"=>"omadm5_sample/index.php"   ),
    "pass2"     => array("url"=>"passwd_contact_update.php" ),
    "logout"    => array("url"=>"session_delete.php"        ),
    "openclose" => array("onclick"=>"changeSideMenuDisplay();"),
);

function getApplicationUrl($emp_id) {
    $optionRow = $this->getOptionRow($emp_id);

    $url = "schedule_week_menu.php"; // default=2
    if ($optionRow["schedule1_default"]=="1") $url = "schedule_menu.php";
    if ($optionRow["schedule1_default"]=="3") $url = "schedule_month_menu.php";
    $this->_applicationUrl["schd"]["url"] = $url;

    $url = "facility_week.php";
    $purl = "";
    if ($optionRow["facility_default"]=="1") $url = "facility_menu.php";
    if ($optionRow["facility_default"]=="2") $url = "facility_week.php";
    if ($optionRow["facility_default"]=="3") $url = "facility_month.php";
    if ($optionRow["facility_default"]=="4") { $url = "facility_week_chart.php"; $purl = "&cate_id=".$optionRow["default_fclcate"]; }
    $this->_applicationUrl["resv"]["url"] = $url;
    $this->_applicationUrl["resv"]["purl"] = $purl;

    $timecardRow = c2dbGetTopRow("select * from timecard");
    $url = "atdbk_timecard_shift.php";
    if ($timecardRow["menu_view_default"]=="1") $url = "atdbk_timecard_shift.php"; // 出勤簿
    if ($timecardRow["menu_view_default"]=="2") $url = "atdbk_timecard.php"; // 日別修正
    if ($timecardRow["menu_view_default"]=="3") $url = "atdbk_timecard_a4.php"; // 日別修正(A4)
    if ($timecardRow["menu_view_default"]=="4") $url = "atdbk_timecard_all.php"; // 一括修正
    if ($timecardRow["menu_view_default"]=="5") $url = "kintai_ovtmlist.php"; // 出勤簿（超勤区分） 20130321
    if ($timecardRow["menu_view_default"]=="6") $url = "kintai_ovtmlist_detail.php"; // 超勤区分内訳 20130321
    $this->_applicationUrl["attdcd"]["url"] = $url;

    // メドレポート/ＰＯレポート(Problem Oriented)
    $sql = "select menu_flg, worksheet_flg, bldg_cd, ward_cd, ptrm_room_no, team_id from sum_application_option where emp_id=".c2dbStr($emp_id);
    $row = c2dbGetTopRow($sql);

    $menu_flg = (int)$row["menu_flg"];
    $worksheet_flg = (int)$row["worksheet_flg"];

    $url = "summary_menu.php";// 患者検索
    $purl = "&search_mode=1"; // 患者検索
    if ($menu_flg==1) { $url = "summary_menu.php";                 $purl = "";               } // お知らせ
    if ($menu_flg==2) { $url = "summary_menu.php";                 $purl = "&search_mode=1"; } // 患者検索
    if ($menu_flg==3) { $url = "summary_search_bed.php";           $purl = "";               } // 病床検索
    if ($menu_flg==4) { $url = "summary_search_data.php";          $purl = "";               } // 複合検索
    if ($menu_flg==5) { $url = "sum_application_apply_list.php";   $purl = "";               } // 送信一覧
    if ($menu_flg==6) { $url = "sum_application_approve_list.php"; $purl = "";               } // 受信一覧
    if ($menu_flg==7) {
        $url = "sot_worksheet.php";
        $purl =
            "&apply_date_default=on".
            "&sel_bldg_cd=".$row["bldg_cd"].
            "&sel_ward_cd=".$row["ward_cd"].
            "&sel_ptrm_room_no=".$row["ptrm_room_no"].
            "&sel_team_id=".$row["team_id"];
        if ($worksheet_flg==1) $url = "sot_worksheet.php";              // ワークシート
        if ($worksheet_flg==2) $url = "sot_worksheet_keikan_eiyou.php"; // 経管栄養一覧
        if ($worksheet_flg==3) $url = "sot_worksheet_isisiji.php";      // 医師指示一覧
        if ($worksheet_flg==4) $url = "sot_worksheet_ondoban.php";      // 温度板
        if ($worksheet_flg==5) $url = "sot_worksheet_soaplist.php";     // チーム記録
        if ($worksheet_flg==6) $url = "sot_worksheet_fim.php";          // FIM評価表
        if ($worksheet_flg==7) $url = "sot_worksheet_weight.php";       // 体重集計表
    }
    $this->_applicationUrl["med"]["url"]  = $url;
    $this->_applicationUrl["med"]["purl"] = $purl;

    $licenseRow = $this->getLicenseRow();
    for ($idx=1; $idx<=5; $idx++) {
        $row = unserialize($licenseRow["lcs_om".$idx."_custom"]);
        $this->_applicationUrl["omusr".$idx]["url"] = $row["app_usr_url"];
        $this->_applicationUrl["omusr".$idx]["purl"] = $row["app_usr_purl"];
        $this->_applicationUrl["omusr".$idx]["onclick"] = $row["app_usr_onclick"];
        $this->_applicationUrl["omadm".$idx]["url"] = $row["app_adm_url"];
        $this->_applicationUrl["omadm".$idx]["purl"] = $row["app_adm_purl"];
        $this->_applicationUrl["omadm".$idx]["onclick"] = $row["app_adm_onclick"];
    }
    return $this->_applicationUrl;
}













//*********************************************************************************************************************
// $_authmstExp
//
// 拡張authmst(authmst expansion)
// index:about_authority.php互換番号。新アプリには付与しないが、任意追加可能
//       この定義はabout_authorityからも参照される。新アプリでabout_authority.phpを利用するなら、indexを付与してください。
// lcs_max 職員登録⇒権限管理、lcs_unlimitedは無制限
// jp:職員登録⇒権限管理⇒ﾎﾟｯﾌﾟｱｯﾌﾟ画面
// のちのち「auth_ok」「lcs_ng」フィールドが追加される
//
//
//*********************************************************************************************************************
var $_authmstExp = array(
    "emp_webml_flg"     => array("appid"=>"webml"    ,"app_attr"=>"usr", "dbflg"=>1, "index"=>0,  "lcs_max"=>"lcs_basic_user",    "lcsfunc_no"=>""   ),
    "emp_bbs_flg"       => array("appid"=>"bbs"      ,"app_attr"=>"usr", "dbflg"=>1, "index"=>1,  "lcs_max"=>"lcs_basic_user",    "lcsfunc_no"=>""   ),
    "emp_schd_flg"      => array("appid"=>"schd"     ,"app_attr"=>"usr", "dbflg"=>1, "index"=>2,  "lcs_max"=>"lcs_basic_user",    "lcsfunc_no"=>""   ),
    "emp_wkflw_flg"     => array("appid"=>"wkflw"    ,"app_attr"=>"   ", "dbflg"=>1, "index"=>3,  "lcs_max"=>"lcs_basic_user",    "lcsfunc_no"=>""   ),
    "emp_phone_flg"     => array("appid"=>"phone"    ,"app_attr"=>"   ", "dbflg"=>1, "index"=>4,  "lcs_max"=>"lcs_basic_user",    "lcsfunc_no"=>""   ),
    "emp_attdcd_flg"    => array("appid"=>"attdcd"   ,"app_attr"=>"   ", "dbflg"=>1, "index"=>5,  "lcs_max"=>"lcs_basic_user",    "lcsfunc_no"=>""   ),
    "emp_qa_flg"        => array("appid"=>"qa"       ,"app_attr"=>"usr", "dbflg"=>1, "index"=>6,  "lcs_max"=>"lcs_basic_user",    "lcsfunc_no"=>""   ),
    "emp_aprv_flg"      => array("appid"=>"aprv"     ,"app_attr"=>"   ", "dbflg"=>1, "index"=>7,  "lcs_max"=>"lcs_basic_user",    "lcsfunc_no"=>""   ),
    "emp_pass_flg"      => array("appid"=>"pass1"    ,"app_attr"=>"   ", "dbflg"=>1, "index"=>8,  "lcs_max"=>"unlimited",         "lcsfunc_no"=>""   ),// パスワード・本人情報"),
    "emp_adbk_flg"      => array("appid"=>"adbk"     ,"app_attr"=>"usr", "dbflg"=>1, "index"=>9,  "lcs_max"=>"lcs_basic_user",    "lcsfunc_no"=>""   ),
    "emp_conf_flg"      => array("appid"=>"conf"     ,"app_attr"=>"   ", "dbflg"=>1, "index"=>10, "lcs_max"=>"lcs_basic_user",    "lcsfunc_no"=>""   ),
    "emp_resv_flg"      => array("appid"=>"resv"     ,"app_attr"=>"usr", "dbflg"=>1, "index"=>11, "lcs_max"=>"lcs_basic_user",    "lcsfunc_no"=>""   ),
    "emp_tmgd_flg"      => array("appid"=>"tmgd"     ,"app_attr"=>"   ", "dbflg"=>1, "index"=>12, "lcs_max"=>"unlimited",         "lcsfunc_no"=>""   ),// ★穴埋★ //院内行事登録
    "emp_schdplc_flg"   => array("appid"=>"schd"     ,"app_attr"=>"adm", "dbflg"=>1, "index"=>13, "lcs_max"=>"lcs_basic_user",    "lcsfunc_no"=>""   ),
    "emp_ward_flg"      => array("appid"=>"ward"     ,"app_attr"=>"   ", "dbflg"=>1, "index"=>14, "lcs_max"=>"lcs_bed_count",     "lcsfunc_no"=>"2"  ),// ★穴埋★ 病床管理
    "emp_ptif_flg"      => array("appid"=>"ptif"     ,"app_attr"=>"   ", "dbflg"=>1, "index"=>15, "lcs_max"=>"unlimited",         "lcsfunc_no"=>"1"  ),// ★穴埋★
    "emp_master_flg"    => array("appid"=>"master"   ,"app_attr"=>"   ", "dbflg"=>1, "index"=>16, "lcs_max"=>"unlimited",         "lcsfunc_no"=>""   ),
    "emp_attditem_flg"  => array("appid"=>"master"   ,"app_attr"=>"   ", "dbflg"=>1, "index"=>17, "lcs_max"=>"unlimited",         "lcsfunc_no"=>""   ,"jp"=>"マスターメンテナンス(カレンダー)"),
    "emp_empif_flg"     => array("appid"=>"empif"    ,"app_attr"=>"   ", "dbflg"=>1, "index"=>18, "lcs_max"=>"unlimited",         "lcsfunc_no"=>""   ,"jp"=>"職員登録(職員参照)"),
    "emp_reg_flg"       => array("appid"=>"reg"      ,"app_attr"=>"adm", "dbflg"=>1, "index"=>19, "lcs_max"=>"unlimited",         "lcsfunc_no"=>""   ,"jp"=>"職員登録(管理者機能)"),
    "emp_config_flg"    => array("appid"=>"config"   ,"app_attr"=>"adm", "dbflg"=>1, "index"=>20, "lcs_max"=>"unlimited",         "lcsfunc_no"=>""   ),
    "emp_ward_reg_flg"  => array("appid"=>"ward"     ,"app_attr"=>"   ", "dbflg"=>1, "index"=>21, "lcs_max"=>"lcs_bed_count",     "lcsfunc_no"=>"2"  ),// ★穴埋★ //病床登録
    "emp_ptreg_flg"     => array("appid"=>"patient"  ,"app_attr"=>"   ", "dbflg"=>1, "index"=>22, "lcs_max"=>"unlimited",         "lcsfunc_no"=>"1"  ),// ★穴埋★"),
    "emp_dept_flg"      => array("appid"=>"master"   ,"app_attr"=>"   ", "dbflg"=>1, "index"=>23, "lcs_max"=>"unlimited",         "lcsfunc_no"=>""   ,"jp"=>"マスターメンテナンス(組織)"),
    "emp_news_flg"      => array("appid"=>"news"     ,"app_attr"=>"   ", "dbflg"=>1, "index"=>24, "lcs_max"=>"lcs_basic_user",    "lcsfunc_no"=>""   ),
    "emp_biz_flg"       => array("appid"=>"biz"      ,"app_attr"=>"   ", "dbflg"=>1, "index"=>25, "lcs_max"=>"unlimited",         "lcsfunc_no"=>"3"  ),// 経営支援"),
    "emp_job_flg"       => array("appid"=>"job"      ,"app_attr"=>"   ", "dbflg"=>1, "index"=>26, "lcs_max"=>"unlimited",         "lcsfunc_no"=>""   ,"jp"=>"マスターメンテナンス(職種)"),
    "emp_status_flg"    => array("appid"=>"status"   ,"app_attr"=>"   ", "dbflg"=>1, "index"=>27, "lcs_max"=>"unlimited",         "lcsfunc_no"=>""   ,"jp"=>"マスターメンテナンス(役職)"),
    "emp_entity_flg"    => array("appid"=>"entity"   ,"app_attr"=>"   ", "dbflg"=>1, "index"=>28, "lcs_max"=>"unlimited",         "lcsfunc_no"=>"2&6"),// ★穴埋★"),//診療科登録 ward+med両方on時
    "emp_work_flg"      => array("appid"=>"work"     ,"app_attr"=>"   ", "dbflg"=>1, "index"=>30, "lcs_max"=>"lcs_basic_user",    "lcsfunc_no"=>""   ),
    "emp_pjt_flg"       => array("appid"=>"pjt"      ,"app_attr"=>"usr", "dbflg"=>1, "index"=>31, "lcs_max"=>"lcs_basic_user",    "lcsfunc_no"=>""   ),
    "emp_lib_flg"       => array("appid"=>"lib"      ,"app_attr"=>"usr", "dbflg"=>1, "index"=>32, "lcs_max"=>"lcs_basic_user",    "lcsfunc_no"=>""   ),
    "emp_inout_flg"     => array("appid"=>""         ,"app_attr"=>"   ", "dbflg"=>1, "index"=>33, "lcs_max"=>"unlimited",         "lcsfunc_no"=>"1"  ),// ★穴埋★"),
    "emp_dishist_flg"   => array("appid"=>""         ,"app_attr"=>"   ", "dbflg"=>1, "index"=>34, "lcs_max"=>"unlimited",         "lcsfunc_no"=>"1"  ),// ★穴埋★"),//プロブレム
    "emp_outreg_flg"    => array("appid"=>""         ,"app_attr"=>"   ", "dbflg"=>1, "index"=>35, "lcs_max"=>"unlimited",         "lcsfunc_no"=>"1"  ),// ★穴埋★"),
    "emp_disreg_flg"    => array("appid"=>""         ,"app_attr"=>"   ", "dbflg"=>1, "index"=>36, "lcs_max"=>"unlimited",         "lcsfunc_no"=>"1"  ),// ★穴埋★"),//プロブレムリスト
    "emp_hspprf_flg"    => array("appid"=>"master"   ,"app_attr"=>"   ", "dbflg"=>1, "index"=>37, "lcs_max"=>"unlimited",         "lcsfunc_no"=>""   ,"jp"=>"マスターメンテナンス(組織プロフィール)"),
    "emp_lcs_flg"       => array("appid"=>"lcs"      ,"app_attr"=>"   ", "dbflg"=>1, "index"=>38, "lcs_max"=>"unlimited",         "lcsfunc_no"=>""   ),
    "emp_bkup_flg"      => array("appid"=>""         ,"app_attr"=>"   ", "dbflg"=>1, "index"=>39, "lcs_max"=>"unlimited",         "lcsfunc_no"=>""   ,"jp"=>"バックアップ"),
    "emp_svr_flg"       => array("appid"=>""         ,"app_attr"=>"   ", "dbflg"=>1, "index"=>40, "lcs_max"=>"unlimited",         "lcsfunc_no"=>""   ,"jp"=>"サーバー情報管理"),
    "emp_fcl_flg"       => array("appid"=>"resv"     ,"app_attr"=>"adm", "dbflg"=>1, "index"=>41, "lcs_max"=>"lcs_basic_user",    "lcsfunc_no"=>""   ),
    "emp_wkadm_flg"     => array("appid"=>"wkadm"    ,"app_attr"=>"   ", "dbflg"=>1, "index"=>42, "lcs_max"=>"lcs_basic_user",    "lcsfunc_no"=>""   ),
    "emp_cmt_flg"       => array("appid"=>"cmt"      ,"app_attr"=>"usr", "dbflg"=>1, "index"=>43, "lcs_max"=>"lcs_basic_user",    "lcsfunc_no"=>""   ),
    "emp_cmtadm_flg"    => array("appid"=>"cmt"      ,"app_attr"=>"adm", "dbflg"=>1, "index"=>44, "lcs_max"=>"lcs_basic_user",    "lcsfunc_no"=>""   ),
    "emp_memo_flg"      => array("appid"=>"memo"     ,"app_attr"=>"   ", "dbflg"=>1, "index"=>45, "lcs_max"=>"lcs_basic_user",    "lcsfunc_no"=>""   ),
    "emp_newsuser_flg"  => array("appid"=>"newsuser" ,"app_attr"=>"   ", "dbflg"=>1, "index"=>46, "lcs_max"=>"lcs_basic_user",    "lcsfunc_no"=>""   ),
    "emp_inci_flg"      => array("appid"=>"inci"     ,"app_attr"=>"usr", "dbflg"=>1, "index"=>47, "lcs_max"=>"lcs_fantol_user",   "lcsfunc_no"=>"5"  ),
    "emp_rm_flg"        => array("appid"=>"inci"     ,"app_attr"=>"adm", "dbflg"=>1, "index"=>48, "lcs_max"=>"lcs_fantol_user",   "lcsfunc_no"=>"5"  ),
    "emp_ext_flg"       => array("appid"=>"ext"      ,"app_attr"=>"usr", "dbflg"=>1, "index"=>49, "lcs_max"=>"lcs_basic_user",    "lcsfunc_no"=>""   ),
    "emp_extadm_flg"    => array("appid"=>"ext"      ,"app_attr"=>"adm", "dbflg"=>1, "index"=>50, "lcs_max"=>"lcs_basic_user",    "lcsfunc_no"=>""   ),
    "emp_intram_flg"    => array("appid"=>"intra"    ,"app_attr"=>"   ", "dbflg"=>1, "index"=>51, "lcs_max"=>"lcs_intra_user",    "lcsfunc_no"=>""   ,"jp"=>"イントラネット(イントラメニュー)"),
    "emp_stat_flg"      => array("appid"=>"stat"     ,"app_attr"=>"   ", "dbflg"=>1, "index"=>52, "lcs_max"=>"lcs_intra_user",    "lcsfunc_no"=>""   ),// ★穴埋★"),//稼働状況統計設定
    "emp_allot_flg"     => array("appid"=>"allot"    ,"app_attr"=>"   ", "dbflg"=>1, "index"=>53, "lcs_max"=>"lcs_intra_user",    "lcsfunc_no"=>""   ),// ★穴埋★"),//当直・外来分担表設定
    "emp_life_flg"      => array("appid"=>"life"     ,"app_attr"=>"   ", "dbflg"=>1, "index"=>54, "lcs_max"=>"lcs_intra_user",    "lcsfunc_no"=>""   ),// ★穴埋★"),//私たちの生活サポート
    "emp_intra_flg"     => array("appid"=>"intra"    ,"app_attr"=>"   ", "dbflg"=>1, "index"=>55, "lcs_max"=>"lcs_intra_user",    "lcsfunc_no"=>""   ,"jp"=>"イントラネット(ユーザ機能)"),
    "emp_ymstat_flg"    => array("appid"=>"ymstat"   ,"app_attr"=>"   ", "dbflg"=>1, "index"=>56, "lcs_max"=>"lcs_intra_user",    "lcsfunc_no"=>""   ),// ★穴埋★"),//月間・年間稼働状況統計参照
    "emp_med_flg"       => array("appid"=>"med"      ,"app_attr"=>"usr", "dbflg"=>1, "index"=>57, "lcs_max"=>"lcs_report_user",   "lcsfunc_no"=>"6"  ),
    "emp_qaadm_flg"     => array("appid"=>"qa"       ,"app_attr"=>"adm", "dbflg"=>1, "index"=>58, "lcs_max"=>"lcs_basic_user",    "lcsfunc_no"=>""   ),
    "emp_medadm_flg"    => array("appid"=>"med"      ,"app_attr"=>"adm", "dbflg"=>1, "index"=>59, "lcs_max"=>"lcs_report_user",   "lcsfunc_no"=>"6"  ),
    "emp_link_flg"      => array("appid"=>"link"     ,"app_attr"=>"usr", "dbflg"=>1, "index"=>60, "lcs_max"=>"lcs_basic_user",    "lcsfunc_no"=>""   ),
    "emp_linkadm_flg"   => array("appid"=>"link"     ,"app_attr"=>"adm", "dbflg"=>1, "index"=>61, "lcs_max"=>"lcs_basic_user",    "lcsfunc_no"=>""   ),
    "emp_bbsadm_flg"    => array("appid"=>"bbs"      ,"app_attr"=>"adm", "dbflg"=>1, "index"=>62, "lcs_max"=>"lcs_basic_user",    "lcsfunc_no"=>""   ),
    "emp_libadm_flg"    => array("appid"=>"lib"      ,"app_attr"=>"adm", "dbflg"=>1, "index"=>63, "lcs_max"=>"lcs_basic_user",    "lcsfunc_no"=>""   ),
    "emp_adbkadm_flg"   => array("appid"=>"adbk"     ,"app_attr"=>"adm", "dbflg"=>1, "index"=>64, "lcs_max"=>"lcs_basic_user",    "lcsfunc_no"=>""   ),
    "emp_search_flg"    => array("appid"=>"search"   ,"app_attr"=>"   ", "dbflg"=>1, "index"=>65, "lcs_max"=>"unlimited",         "lcsfunc_no"=>"7"  ),
    "emp_webmladm_flg"  => array("appid"=>"webml"    ,"app_attr"=>"adm", "dbflg"=>1, "index"=>66, "lcs_max"=>"lcs_basic_user",    "lcsfunc_no"=>""   ),
    "emp_cas_flg"       => array("appid"=>"cas"      ,"app_attr"=>"usr", "dbflg"=>1, "index"=>67, "lcs_max"=>"unlimited",         "lcsfunc_no"=>"8"  ),// ★穴埋★"),//CAS
    "emp_casadm_flg"    => array("appid"=>"cas"      ,"app_attr"=>"adm", "dbflg"=>1, "index"=>68, "lcs_max"=>"unlimited",         "lcsfunc_no"=>"8"  ),// ★穴埋★"),//CAS
    "emp_shift_flg"     => array("appid"=>"shift"    ,"app_attr"=>"usr", "dbflg"=>1, "index"=>69, "lcs_max"=>"lcs_shift_emps",    "lcsfunc_no"=>"9"  ,"jp"=>"勤務シフト作成(登録職員)"),
    "emp_shiftadm_flg"  => array("appid"=>"shift"    ,"app_attr"=>"adm", "dbflg"=>1, "index"=>70, "lcs_max"=>"lcs_shift_emps",    "lcsfunc_no"=>"9"  ),
    "emp_nlcs_flg"      => array("appid"=>"nlcs"     ,"app_attr"=>"usr", "dbflg"=>1, "index"=>71, "lcs_max"=>"unlimited",         "lcsfunc_no"=>"10" ),
    "emp_nlcsadm_flg"   => array("appid"=>"nlcs"     ,"app_attr"=>"adm", "dbflg"=>1, "index"=>72, "lcs_max"=>"unlimited",         "lcsfunc_no"=>"10" ),
    "emp_kview_flg"     => array("appid"=>"kview"    ,"app_attr"=>"usr", "dbflg"=>1, "index"=>73, "lcs_max"=>"unlimited",         "lcsfunc_no"=>"11" ),
    "emp_kviewadm_flg"  => array("appid"=>"kview"    ,"app_attr"=>"adm", "dbflg"=>1, "index"=>74, "lcs_max"=>"unlimited",         "lcsfunc_no"=>"11" ),
    "emp_manabu_flg"    => array("appid"=>"manabu"   ,"app_attr"=>"usr", "dbflg"=>1, "index"=>75, "lcs_max"=>"lcs_baritess_user", "lcsfunc_no"=>"12" ),
    "emp_manabuadm_flg" => array("appid"=>"manabu"   ,"app_attr"=>"adm", "dbflg"=>1, "index"=>76, "lcs_max"=>"lcs_baritess_user", "lcsfunc_no"=>"12" ),
    "emp_ptadm_flg"     => array("appid"=>"ptadm"    ,"app_attr"=>"   ", "dbflg"=>1, "index"=>77, "lcs_max"=>"unlimited",         "lcsfunc_no"=>"1"  ),// ★穴埋★"), //患者管理
    "emp_fplus_flg"     => array("appid"=>"fplus"    ,"app_attr"=>"usr", "dbflg"=>1, "index"=>78, "lcs_max"=>"lcs_fplus_user",    "lcsfunc_no"=>"13" ),
    "emp_fplusadm_flg"  => array("appid"=>"fplus"    ,"app_attr"=>"adm", "dbflg"=>1, "index"=>79, "lcs_max"=>"lcs_fplus_user",    "lcsfunc_no"=>"13" ),
    "emp_jnl_flg"       => array("appid"=>"jnl"      ,"app_attr"=>"usr", "dbflg"=>1, "index"=>80, "lcs_max"=>"lcs_jnl_user",      "lcsfunc_no"=>"14" ),
    "emp_jnladm_flg"    => array("appid"=>"jnl"      ,"app_attr"=>"adm", "dbflg"=>1, "index"=>81, "lcs_max"=>"lcs_jnl_user",      "lcsfunc_no"=>"14" ),
    "emp_pjtadm_flg"    => array("appid"=>"pjt"      ,"app_attr"=>"adm", "dbflg"=>1, "index"=>82, "lcs_max"=>"lcs_basic_user",    "lcsfunc_no"=>""   ),
    "emp_amb_flg"       => array("appid"=>"amb"      ,"app_attr"=>"usr", "dbflg"=>1, "index"=>83, "lcs_max"=>"lcs_nursing_user",  "lcsfunc_no"=>"15" ),
    "emp_ambadm_flg"    => array("appid"=>"amb"      ,"app_attr"=>"adm", "dbflg"=>1, "index"=>84, "lcs_max"=>"lcs_nursing_user",  "lcsfunc_no"=>"15" ),
    "emp_jinji_flg"     => array("appid"=>"jinji"    ,"app_attr"=>"usr", "dbflg"=>1, "index"=>85, "lcs_max"=>"lcs_jinji_emps",    "lcsfunc_no"=>"16" ),
    "emp_jinjiadm_flg"  => array("appid"=>"jinji"    ,"app_attr"=>"adm", "dbflg"=>1, "index"=>86, "lcs_max"=>"lcs_jinji_emps",    "lcsfunc_no"=>"16" ),
    "emp_ladder_flg"    => array("appid"=>"ladder"   ,"app_attr"=>"usr", "dbflg"=>1, "index"=>87, "lcs_max"=>"lcs_ladder_user",   "lcsfunc_no"=>"17" ),
    "emp_ladderadm_flg" => array("appid"=>"ladder"   ,"app_attr"=>"adm", "dbflg"=>1, "index"=>88, "lcs_max"=>"lcs_ladder_user",   "lcsfunc_no"=>"17" ),
    "emp_shift2_flg"    => array("appid"=>"shift2"   ,"app_attr"=>"usr", "dbflg"=>1, "index"=>89, "lcs_max"=>"lcs_shift_emps",    "lcsfunc_no"=>"18" ,"jp"=>"勤務表(登録職員)"),
    "emp_shift2adm_flg" => array("appid"=>"shift2"   ,"app_attr"=>"adm", "dbflg"=>1, "index"=>90, "lcs_max"=>"lcs_shift_emps",    "lcsfunc_no"=>"18" ),
    "emp_cauth_flg"     => array("appid"=>"cauth"    ,"app_attr"=>"   ", "dbflg"=>1, "index"=>91, "lcs_max"=>"lcs_basic_user",    "lcsfunc_no"=>""   ),
    "emp_accesslog_flg" => array("appid"=>"accesslog","app_attr"=>"   ", "dbflg"=>1, "index"=>92, "lcs_max"=>"unlimited",         "lcsfunc_no"=>""   ),
    "emp_career_flg"    => array("appid"=>"career"   ,"app_attr"=>"usr", "dbflg"=>1, "index"=>93, "lcs_max"=>"lcs_career_user",   "lcsfunc_no"=>"19" ),
    "emp_careeradm_flg" => array("appid"=>"career"   ,"app_attr"=>"adm", "dbflg"=>1, "index"=>94, "lcs_max"=>"lcs_career_user",   "lcsfunc_no"=>"19" ),
    "emp_omusr1_flg"    => array("appid"=>"omusr1"   ,"app_attr"=>"   ", "dbflg"=>1,              "lcs_max"=>"lcs_omusr1_user",   "lcsfunc_no"=>"21" ),// ★特製1穴埋★"),
    "emp_omusr2_flg"    => array("appid"=>"omusr2"   ,"app_attr"=>"   ", "dbflg"=>1,              "lcs_max"=>"lcs_omusr2_user",   "lcsfunc_no"=>"22" ),// ★特製2穴埋★"),
    "emp_omusr3_flg"    => array("appid"=>"omusr3"   ,"app_attr"=>"   ", "dbflg"=>1,              "lcs_max"=>"lcs_omusr3_user",   "lcsfunc_no"=>"23" ),// ★特製3穴埋★"),
    "emp_omusr4_flg"    => array("appid"=>"omusr4"   ,"app_attr"=>"   ", "dbflg"=>1,              "lcs_max"=>"lcs_omusr4_user",   "lcsfunc_no"=>"24" ),// ★特製4穴埋★"),
    "emp_omusr5_flg"    => array("appid"=>"omusr5"   ,"app_attr"=>"   ", "dbflg"=>1,              "lcs_max"=>"lcs_omusr5_user",   "lcsfunc_no"=>"25" ),// ★特製5穴埋★"),
    "emp_omadm1_flg"    => array("appid"=>"omadm1"   ,"app_attr"=>"   ", "dbflg"=>1,              "lcs_max"=>"lcs_omadm1_user",   "lcsfunc_no"=>"26" ),// ★特製1管理穴埋★"),
    "emp_omadm2_flg"    => array("appid"=>"omadm2"   ,"app_attr"=>"   ", "dbflg"=>1,              "lcs_max"=>"lcs_omadm2_user",   "lcsfunc_no"=>"27" ),// ★特製2管理穴埋★"),
    "emp_omadm3_flg"    => array("appid"=>"omadm3"   ,"app_attr"=>"   ", "dbflg"=>1,              "lcs_max"=>"lcs_omadm3_user",   "lcsfunc_no"=>"28" ),// ★特製3管理穴埋★"),
    "emp_omadm4_flg"    => array("appid"=>"omadm4"   ,"app_attr"=>"   ", "dbflg"=>1,              "lcs_max"=>"lcs_omadm4_user",   "lcsfunc_no"=>"29" ),// ★特製4管理穴埋★"),
    "emp_omadm5_flg"    => array("appid"=>"omadm5"   ,"app_attr"=>"   ", "dbflg"=>1,              "lcs_max"=>"lcs_omadm5_user",   "lcsfunc_no"=>"30" ),// ★特製5管理穴埋★"),
    "emp_ccusr1_flg"    => array("appid"=>"ccusr1"   ,"app_attr"=>"usr", "dbflg"=>1,              "lcs_max"=>"lcs_ccusr1_user",   "lcsfunc_no"=>"31" ),
    "emp_ccusr2_flg"    => array("appid"=>"ccusr2"   ,"app_attr"=>"usr", "dbflg"=>1,              "lcs_max"=>"lcs_ccusr2_user",   "lcsfunc_no"=>"32" ),// 予備
    "emp_ccusr3_flg"    => array("appid"=>"ccusr3"   ,"app_attr"=>"usr", "dbflg"=>1,              "lcs_max"=>"lcs_ccusr3_user",   "lcsfunc_no"=>"33" ),// 予備
    "emp_ccusr4_flg"    => array("appid"=>"ccusr4"   ,"app_attr"=>"usr", "dbflg"=>1,              "lcs_max"=>"lcs_ccusr4_user",   "lcsfunc_no"=>"34" ),// 予備
    "emp_ccusr5_flg"    => array("appid"=>"ccusr5"   ,"app_attr"=>"usr", "dbflg"=>1,              "lcs_max"=>"lcs_ccusr5_user",   "lcsfunc_no"=>"35" ),// 予備
    "emp_ccusr6_flg"    => array("appid"=>"ccusr6"   ,"app_attr"=>"usr", "dbflg"=>1,              "lcs_max"=>"lcs_ccusr6_user",   "lcsfunc_no"=>"36" ),// 予備
    "emp_ccusr7_flg"    => array("appid"=>"ccusr7"   ,"app_attr"=>"usr", "dbflg"=>1,              "lcs_max"=>"lcs_ccusr7_user",   "lcsfunc_no"=>"37" ),// 予備
    "emp_ccusr8_flg"    => array("appid"=>"ccusr8"   ,"app_attr"=>"usr", "dbflg"=>1,              "lcs_max"=>"lcs_ccusr8_user",   "lcsfunc_no"=>"38" ),// 予備
    "emp_ccusr9_flg"    => array("appid"=>"ccusr9"   ,"app_attr"=>"usr", "dbflg"=>1,              "lcs_max"=>"lcs_ccusr9_user",   "lcsfunc_no"=>"39" ),// 予備
    "emp_ccusr10_flg"   => array("appid"=>"ccusr10"  ,"app_attr"=>"usr", "dbflg"=>1,              "lcs_max"=>"lcs_ccusr10_user",  "lcsfunc_no"=>"40" ),// 予備
    "emp_ccusr11_flg"   => array("appid"=>"ccusr11"  ,"app_attr"=>"usr", "dbflg"=>1,              "lcs_max"=>"lcs_ccusr11_user",  "lcsfunc_no"=>"41" ),// 予備
    "emp_ccusr12_flg"   => array("appid"=>"ccusr12"  ,"app_attr"=>"usr", "dbflg"=>1,              "lcs_max"=>"lcs_ccusr12_user",  "lcsfunc_no"=>"42" ),// 予備
    "emp_ccusr13_flg"   => array("appid"=>"ccusr13"  ,"app_attr"=>"usr", "dbflg"=>1,              "lcs_max"=>"lcs_ccusr13_user",  "lcsfunc_no"=>"43" ),// 予備
    "emp_ccusr14_flg"   => array("appid"=>"ccusr14"  ,"app_attr"=>"usr", "dbflg"=>1,              "lcs_max"=>"lcs_ccusr14_user",  "lcsfunc_no"=>"44" ),// 予備
    "emp_ccusr15_flg"   => array("appid"=>"ccusr15"  ,"app_attr"=>"usr", "dbflg"=>1,              "lcs_max"=>"lcs_ccusr15_user",  "lcsfunc_no"=>"45" ),// 予備
    "emp_ccadm1_flg"    => array("appid"=>"ccadm1"   ,"app_attr"=>"adm", "dbflg"=>1,              "lcs_max"=>"lcs_ccadm1_user",   "lcsfunc_no"=>"46" ),// 利用していない
    "emp_ccadm2_flg"    => array("appid"=>"ccadm2"   ,"app_attr"=>"adm", "dbflg"=>1,              "lcs_max"=>"lcs_ccadm2_user",   "lcsfunc_no"=>"47" ),// 予備
    "emp_ccadm3_flg"    => array("appid"=>"ccadm3"   ,"app_attr"=>"adm", "dbflg"=>1,              "lcs_max"=>"lcs_ccadm3_user",   "lcsfunc_no"=>"48" ),// 予備
    "emp_ccadm4_flg"    => array("appid"=>"ccadm4"   ,"app_attr"=>"adm", "dbflg"=>1,              "lcs_max"=>"lcs_ccadm4_user",   "lcsfunc_no"=>"49" ),// 予備
    "emp_ccadm5_flg"    => array("appid"=>"ccadm5"   ,"app_attr"=>"adm", "dbflg"=>1,              "lcs_max"=>"lcs_ccadm5_user",   "lcsfunc_no"=>"50" ),// 予備
    "emp_ccadm6_flg"    => array("appid"=>"ccadm6"   ,"app_attr"=>"adm", "dbflg"=>1,              "lcs_max"=>"lcs_ccadm6_user",   "lcsfunc_no"=>"51" ),// 予備
    "emp_ccadm7_flg"    => array("appid"=>"ccadm7"   ,"app_attr"=>"adm", "dbflg"=>1,              "lcs_max"=>"lcs_ccadm7_user",   "lcsfunc_no"=>"52" ),// 予備
    "emp_ccadm8_flg"    => array("appid"=>"ccadm8"   ,"app_attr"=>"adm", "dbflg"=>1,              "lcs_max"=>"lcs_ccadm8_user",   "lcsfunc_no"=>"53" ),// 予備
    "emp_ccadm9_flg"    => array("appid"=>"ccadm9"   ,"app_attr"=>"adm", "dbflg"=>1,              "lcs_max"=>"lcs_ccadm9_user",   "lcsfunc_no"=>"54" ),// 予備
    "emp_ccadm10_flg"   => array("appid"=>"ccadm10"  ,"app_attr"=>"adm", "dbflg"=>1,              "lcs_max"=>"lcs_ccadm10_user",  "lcsfunc_no"=>"55" ),// 予備
    "emp_ccadm11_flg"   => array("appid"=>"ccadm11"  ,"app_attr"=>"adm", "dbflg"=>1,              "lcs_max"=>"lcs_ccadm11_user",  "lcsfunc_no"=>"56" ),// 予備
    "emp_ccadm12_flg"   => array("appid"=>"ccadm12"  ,"app_attr"=>"adm", "dbflg"=>1,              "lcs_max"=>"lcs_ccadm12_user",  "lcsfunc_no"=>"57" ),// 予備
    "emp_ccadm13_flg"   => array("appid"=>"ccadm13"  ,"app_attr"=>"adm", "dbflg"=>1,              "lcs_max"=>"lcs_ccadm13_user",  "lcsfunc_no"=>"58" ),// 予備
    "emp_ccadm14_flg"   => array("appid"=>"ccadm14"  ,"app_attr"=>"adm", "dbflg"=>1,              "lcs_max"=>"lcs_ccadm14_user",  "lcsfunc_no"=>"59" ),// 予備
    "emp_ccadm15_flg"   => array("appid"=>"ccadm15"  ,"app_attr"=>"adm", "dbflg"=>1,              "lcs_max"=>"lcs_ccadm15_user",  "lcsfunc_no"=>"60" ),// 予備
    // ここからDBに存在しない権限拡張フラグ
    "ext_patient_flg"   => array("appid"=>"patinet"  ,"app_attr"=>"   ",                          "lcs_max"=>"",                  "lcsfunc_no"=>""   ),// 仮想患者"),
    "ext_pass_flg"      => array("appid"=>"pass1",    "app_attr"=>"   ",                          "lcs_max"=>"",                  "lcsfunc_no"=>""   ),// 仮想パスワード設定"),
    "ext_openclose_flg" => array("appid"=>"openclose","app_attr"=>"   ",                          "lcs_max"=>"",                  "lcsfunc_no"=>""   ),// 仮想open/closeフラグ"),
    "ext_logout_flg"    => array("appid"=>"logout",   "app_attr"=>"   ",                          "lcs_max"=>"",                  "lcsfunc_no"=>""   ),// 仮想ログアウトフラグ ヘッダメニュー用"),
    "ext_mypage_flg"    => array("appid"=>"mypage",   "app_attr"=>"   ",                          "lcs_max"=>"",                  "lcsfunc_no"=>""   ),// 仮想マイページフラグ"),
    "ext_option_flg"    => array("appid"=>"option",   "app_attr"=>"   ",                          "lcs_max"=>"",                  "lcsfunc_no"=>""   ),// 仮想オプション設定画面フラグ"),
    "ext_freetext_flg"  => array("appid"=>"freetext", "app_attr"=>"   ",                          "lcs_max"=>"",                  "lcsfunc_no"=>""   ),// 仮想フリーテキスト"), //ﾏｲﾍﾟｰｼﾞ
    "ext_tcarderr_flg"  => array("appid"=>"tcarderr", "app_attr"=>"   ",                          "lcs_max"=>"",                  "lcsfunc_no"=>""   ),// 仮想打刻エラー"), //ﾏｲﾍﾟｰｼﾞ
    "ext_whatsnew_flg"  => array("appid"=>"whatsnew", "app_attr"=>"   ",                          "lcs_max"=>"",                  "lcsfunc_no"=>""   ),// 仮想最新情報"), //ﾏｲﾍﾟｰｼﾞ
//  "ext_schdsrch_flg"  => array("appid"=>"",         "app_attr"=>"   ",                          "lcs_max"=>"",                  "lcsfunc_no"=>""   ),// 仮想スケジュール検索"), //ﾏｲﾍﾟｰｼﾞ
    "ext_wic_flg"       => array("appid"=>"",         "app_attr"=>"   ",                          "lcs_max"=>"",                  "lcsfunc_no"=>""   ),// 仮想WICレポート")  //ﾏｲﾍﾟｰｼﾞ
);
var $_empAuthmstExp = array();
function getAuthmstExpFieldNames($isDbFlgOnly) {
    if (!$isDbFlgOnly) return array_keys($this->_authmstExp);
    $ret = array();
    foreach ($this->_authmstExp as $field => $item) {
        if ($item["dbflg"]) $ret[]= $field;
    }
    return $ret;
}
function getEmpAuthority($emp_id) {
    $authmstExp = $this->getAuthmstExp($emp_id);
    $ret = array();
    foreach ($authmstExp as $field => $params) {
        $ret[$field] = $authmstExp[$field]["auth_ok"];
    }
    return $ret;
}

function getAuthmstLicense() {
    $authmstExp = $this->getAuthmstExp("");
    $ret = array();
    foreach ($authmstExp as $field => $params) {
        $ret[$field] = !$authmstExp[$field]["lcs_ng"];
    }
    return $ret;
}
function getAuthmstExp($emp_id) {
    if ($this->_empAuthmstExp[$emp_id]) return $this->_empAuthmstExp[$emp_id];
    $configRow = $this->getConfigRow();
    $authmstRow = ($emp_id=="" ? "" : $this->getAuthmstRow($emp_id));
    $LBP = $this->getLabelByProfile();
    $prf_type = $this->getPrfType();
    $intramenuRow = $this->getIntramenuRow();
    $casTitle = $this->getCasTitle();
    $applications = $this->getApplicationsNames();

    // クローンを作成。本体定義はいじらない。クローン作成は代入するだけでよい。
    $ret = $this->_authmstExp;

    // 環境設定からマスタ変更情報を取得
    $user_change = "";
    if (c2bool($configRow["user_belong_change"])) $user_change = 1;
    if (c2bool($configRow["user_job_change"])) $user_change = 1;
    if (c2bool($configRow["user_st_change"])) $user_change = 1;
    // ライセンス紐つけ
    $licenseRow = $this->getLicenseRow();

    foreach ($ret as $field => $params) {
        if (!$ret[$field]["jp"]) {
            $appid = $ret[$field]["appid"];
            $app_attr = $ret[$field]["app_attr"];
            $app_attr_jp = "";
            if ($app_attr=="usr") $app_attr_jp = "（ユーザ機能）";
            if ($app_attr=="adm") $app_attr_jp = "（管理者機能）";
            $ret[$field]["jp"] = $applications[$appid]["jp"].$app_attr_jp;
        }
        $lcs = explode("&", $ret[$field]["lcsfunc_no"]);
        if ($lcs[0]!="" && !c2bool($licenseRow["lcs_func".$lcs[0]])) $ret[$field]["lcs_ng"] = "1";
        if ($lcs[1]!="" && !c2bool($licenseRow["lcs_func".$lcs[1]])) $ret[$field]["lcs_ng"] = "1";
    }

    for ($idx=1; $idx<=5; $idx++) {
        $row = unserialize($licenseRow["lcs_om".$idx."_custom"]);
        if (!$row["kousei"] || $row["kousei"]=="omadm_only") {
            $this->_authmstExp["emp_omusr".$idx."_flg"]["app_ng"] = "1";
        }
        if (!$row["kousei"] || $row["kousei"]=="omusr_only") {
            $this->_authmstExp["emp_omadm".$idx."_flg"]["app_ng"] = "1";
        }
    }

    // 機能権限紐つけ。$emp_idを指定していなければ、全機能に権限ありとなる。
    $ext_patient_flg_auth = "";
    $ext_pass_flg_auth = "";
    foreach ($ret as $field => $params) {
        $auth = ($emp_id=="" ? "1" : c2bool($authmstRow[$field])); // $emp_id未指定の場合は無条件でOKである
        $ret[$field]["auth_ok"] = $auth;

        if ($auth) {
            if ($field=="emp_ptif_flg" || $field=="emp_ptreg_flg" || $field=="emp_inout_flg"
            || $field=="emp_outreg_flg" || $field=="emp_dishist_flg" || $field=="emp_disreg_flg") {
                $ext_patient_flg_auth = "1";
            }
        }
        if (!$auth && $field=="emp_pass_flg" && $user_change) $ext_pass_flg_auth = "1";
    }
    $ret["ext_logout_flg"]["auth_ok"] = "1";
    $ret["ext_patient_flg"]["auth_ok"] = $ext_patient_flg_auth;
    $ret["ext_pass_flg"]["auth_ok"] = $ext_pass_flg_auth; // $emp_id未指定の場合は、この権限はなくなる
    $ret["ext_mypage_flg"]["auth_ok"] = "1";
    $ret["ext_openclose_flg"]["auth_ok"] = "1";
    $ret["ext_option_flg"]["auth_ok"] = "1";
    $ret["ext_freetext_flg"]["auth_ok"] = "1";
    $ret["ext_tcarderr_flg"]["auth_ok"] = "1";
    $ret["ext_whatsnew_flg"]["auth_ok"] = "1";
//    $ret["ext_schdsrch_flg"]["auth_ok"] = "1";
    $ret["ext_wic_flg"]["auth_ok"] = "1";

    $ret["emp_tmgd_flg"]["jp"] =       $LBP["EVENT_REG"][$prf_type]; // 院内行事/法人内行事
    $ret["emp_ward_flg"]["jp"] =       $LBP["BED_MANAGE"][$prf_type]."(ユーザ機能)"; //病床管理
    $ret["emp_ward_reg_flg"]["jp"] =   $LBP["BED_MANAGE"][$prf_type]."(管理者機能)"; //病床登録
    $ret["emp_entity_flg"]["jp"] =     $LBP["SECTION"][$prf_type]; //診療科登録
    $ret["emp_med_flg"]["jp"] =        $LBP["MED_REPORT"][$prf_type]."(ユーザ機能)";//メドレポート
    $ret["emp_medadm_flg"]["jp"] =     $LBP["MED_REPORT"][$prf_type]."(管理者機能)";//メドレポート
    $ret["emp_cas_flg"]["jp"] =        $casTitle."(ユーザ機能)";//CAS
    $ret["emp_casadm_flg"]["jp"] =     $casTitle."(管理者機能)";//CAS
    $ret["emp_ptreg_flg"]["jp"] =      $LBP["PATIENT_MANAGE"][$prf_type]."(".$LBP["PATIENT_REG"][$prf_type].")";//患者登録
    $ret["emp_ptif_flg"]["jp"] =       $LBP["PATIENT_MANAGE"][$prf_type]."(".$LBP["PATIENT_INFO"][$prf_type].")";//患者/利用者基本情報
    $ret["emp_inout_flg"]["jp"] =      $LBP["PATIENT_MANAGE"][$prf_type]."(".$LBP["IN_OUT"][$prf_type].")"; // 来院登録
    $ret["emp_outreg_flg"]["jp"] =     $LBP["PATIENT_MANAGE"][$prf_type]."(".$LBP["OUT_REG"][$prf_type].")"; // 入院来院履歴
    $ret["emp_disreg_flg"]["jp"] =     $LBP["PATIENT_MANAGE"][$prf_type]."(プロブレムリスト)";
    $ret["emp_dishist_flg"]["jp"] =    $LBP["PATIENT_MANAGE"][$prf_type]."(プロブレム)";
    $ret["emp_ptadm_flg"]["jp"] =      $LBP["PATIENT_MANAGE"][$prf_type]."(管理者権限)";
    $ret["emp_stat_flg"]["jp"] =       "イントラネット(".$intramenuRow["menu1"].")";//稼働状況統計設定
    $ret["emp_allot_flg"]["jp"] =      "イントラネット(".$intramenuRow["menu2_4"].")";//当直・外来分担表設定(値は補正済)
    $ret["emp_life_flg"]["jp"] =       "イントラネット(".$intramenuRow["menu3"].")";//私たちの生活サポート
    $ret["emp_ymstat_flg"]["jp"] =     "イントラネット(月間・年間".$intramenuRow["menu1"]."参照)"; //月間・年間稼働状況統計参照

    $this->_empAuthmstExp[$emp_id] = $ret;
    return $ret;
}





//*********************************************************************************************************************
// 「基本機能」アプリケーションとして、利用者数の集計対象となる機能の一覧
// 職員登録⇒権限管理 employee_auth_list.php のみで利用
// 基本的にはこのままであり、追加も削除もしない。
// プログラム内でも、追加削除編集されることはない。プライベート定数扱い。
//*********************************************************************************************************************
var $_authmstKihonkinouList = array(
    "emp_webml_flg",    // ウェブメール
    "emp_schd_flg",     // スケジュール
    "emp_pjt_flg",      // 委員会・WG
    "emp_newsuser_flg", // お知らせ・回覧板
    "emp_work_flg",     // タスク
    "emp_phone_flg",    // 伝言メモ
    "emp_resv_flg",     // 設備予約
    "emp_attdcd_flg",   // 出勤表
    "emp_aprv_flg",     // 決裁・申請
    "emp_conf_flg",     // ネットカンファレンス
    "emp_bbs_flg",      // 掲示板・電子会議室
    "emp_qa_flg",       // Q&A
    "emp_lib_flg",      // 文書管理
    "emp_adbk_flg",     // アドレス帳
    "emp_ext_flg",      // 内線電話帳
    "emp_link_flg",     // リンクライブラリ
    "emp_cmt_flg",      // コミュニティサイト
    "emp_memo_flg"      // 備忘録
);
function getAuthmstKihonkinouList() {
    return $this->_authmstKihonkinouList;
}







//*********************************************************************************************************************
// $_mypageContents
//
// マイページに表示すべきコンテンツが実装されているもの。プライベート定義扱い
// マイページにコンテンツを追加するときは、以下が必要
// ◆[option][dispgroup]の２テーブルに、「top_xxxx_flg」フィールドを追加する
// ◆ここに追加したフィールドと対となるエントリを追加する
// ◆マイページ(left.php)に、表示コンテンツ自体を実装する
//
// ■影響画面
// ・マイページ自体
// ・オプション設定⇒表示設定⇒マイページチェック群
// ・職員登録⇒一括設定⇒[表示]サブメニュー
// ※auth権限＋license権限で、表示設定可能となる。
// ※auth権限＋license権限＋表示設定オンで、マイページでの表示がアクティブになる。
//
// ■field
// 定義のキー。[option][dispgroup]テーブルのカラムと一対一。
// ■ db_on
// プログラムによって「select from option」「select from dispgroup」のセレクト結果を格納する。trueなら1となる。falseならカラ文字となる。
// 設定画面を開く際、[option]or[dispgroup]にデータがない場合は、以下の初期状態がセットされた設定画面が表示される。
// ※そののちプログラムでは、このdb_onとともに、$_APPLICATIONS[appid]を確認して、auth権限、license権限をチェック。表示状態が決まる。
// ■appid
// 必須。および、$_APPLICATIONS[appid]、も必須。
// $_APPLICATIONS[appid]が無い場合は、適当な名前で追加しないといけない。
//
// ■jp
// マイページ設定のプルダウンでの表記。マイページ自体の表記ではない。
// jpがカラなら、$_APPLICATIONS[appid]から持ってくる。
// ■「disp」「auth」「lcs_ng」「section」「grp_field」
// この構造体は、のちに「disp_leftphp」「auth_ok」「lcs_ng」「section」「app_ng」というフィールドが補完・自動付与される。
// ■定義の順番
// オプション設定保存前のマイページコンテンツ表示順。
//
//*********************************************************************************************************************
var $_mypageContents = array(
    "top_free_text_flg" =>      array("db_on"=>"",  "appid"=>"freetext", "width"=>100), // フリーテキスト
    "top_mail_flg" =>           array("db_on"=>"1", "appid"=>"webml",    "width"=>50),  // ウェブメール
    "top_ext_flg" =>            array("db_on"=>"1", "appid"=>"ext",      "width"=>50,   "jp"=>"一発電話検索"),
    "top_inci_flg" =>           array("db_on"=>"1", "appid"=>"inci",     "width"=>50),  // ファントルくん
    "top_fplus_flg" =>          array("db_on"=>"1", "appid"=>"fplus",    "width"=>50),  // ファントルくん＋
    "top_manabu_flg" =>         array("db_on"=>"",  "appid"=>"manabu",   "width"=>50),  // バリテス
    "top_cas_flg" =>            array("db_on"=>"1", "appid"=>"cas",      "width"=>50),  // CAS
    "top_ladder_flg" =>         array("db_on"=>"1", "appid"=>"ladder",   "width"=>100), // ladder_flg ⇒ top_ladder_flgに統一
    "top_jnl_flg" =>            array("db_on"=>"",  "appid"=>"jnl",      "width"=>50),  // 日報・月報
    "top_timecard_error_flg" => array("db_on"=>"",  "appid"=>"tcarderr", "width"=>50,   "jp"=>"打刻エラー"),
    "top_schd_flg" =>           array("db_on"=>"1", "appid"=>"schd",     "width"=>100), // スケジュール
    "top_event_flg" =>          array("db_on"=>"1", "appid"=>"tmgd",     "width"=>100,  "jp"=>"院内行事"),
    "top_info_flg" =>           array("db_on"=>"1", "appid"=>"newsuser", "width"=>66),  // お知らせ・回覧板
    "top_task_flg" =>           array("db_on"=>"1", "appid"=>"work",     "width"=>33),  // タスク
    "top_msg_flg" =>            array("db_on"=>"1", "appid"=>"whatsnew", "width"=>66,   "jp"=>"最新情報"),//フラグ2個 // ,top_aprv_flg
    "top_schdsrch_flg" =>       array("db_on"=>"1", "appid"=>"schdsrch", "width"=>33),  // スケジュール検索
    "top_fcl_flg" =>            array("db_on"=>"1", "appid"=>"resv",     "width"=>100), // 設備予約
    "top_lib_flg" =>            array("db_on"=>"1", "appid"=>"lib",      "width"=>100), // 文書管理
    "top_bbs_flg" =>            array("db_on"=>"1", "appid"=>"bbs",      "width"=>100,  "jp"=>"掲示板"),
    "top_memo_flg" =>           array("db_on"=>"1", "appid"=>"memo",     "width"=>100), // 備忘録
    "top_link_flg" =>           array("db_on"=>"1", "appid"=>"link",     "width"=>100), // リンクライブラリ
    "top_intra_flg" =>          array("db_on"=>"1", "appid"=>"intra",    "width"=>100), // イントラネット
    "bed_info" =>               array("db_on"=>"",  "appid"=>"ward",     "width"=>100,  "jp"=>"入退院カレンダー"),
    "top_wic_flg" =>            array("db_on"=>"",  "appid"=>"wic",      "width"=>100)  // WICレポート
);
//**********************************************************************************************************************
// マイページ設定の作成
// ・left.php マイページ自体
// ・employee_display_bulk_setting.php
// ・employee_display_group.php
// ・employee_display_setting.php
// ・o_register.php
//
// 個人マイページ設定
// グループ別マイページ設定も兼用
//
//
// 【mypage_layout_info】について
// コンテンツのレイアウト枠定義
// アイテムがカンマ区切りで格納されている。無論、カラデータの場合もあり。
// アイテムをカンマで分割したとき、各アイテムは、２種類の定義が混在している。
// ■セクション定義
//   以下のような半角カンマで３つの属性情報が格納されている。
//   つまり、アイテムを半角コロンで分割すると、第一要素が「コの字カッコでsection」であれば、セクション定義である。
//   ⇒"[section]" ＋ ":" ＋ (セクション番号) ＋ ":" ＋ (幅設定)
//     ⇒例）[section]:1:33_67
//     ⇒例）[section]:2:50_50
//   ◆第１要素：[section]       固定文字
//   ◆第２要素：セクション番号  1〜10。セクションの行順（上から並べる順）
//   ◆第３要素：幅設定          枠のパーセント幅定義。
//                               現在、「100」「33_67」「67_33」「50_50」「33_33_34」 の５パターン。別に増やしてもよい。アンダーバー重要。
//                               ちなみに「33_67」というのは、「セクションは２列構成で、１列目の幅は33%、２列目の幅は67%」と解釈される。
// ■コンテンツ定義
//   こちらも以下のような半角カンマで３つの属性情報が格納されている。セクション情報でなければ、コンテンツ定義である。
//   ⇒(フィールド) ＋ ":" ＋ (セクション位置) ＋ ":" ＋ (幅設定)
//   ◆第１要素：フィールド      top_xxxx_flg。$MYPAGE_SECTIONのキー。
//   ◆第２要素：セクション位置  「N1_N2」形式。N1はセクション番号。N2はセクション内列番号。
//               （ボックス)     例えば「2_3」の場合、「セクション番号２の、３列目に配置すること」である。
//                               仮に対象セクションに３列目が存在しない場合、１列目に配置される。
//                               仮に対象セクションそのものが存在しない場合、セクション１に配置される。セクション１は、必ず存在する。
//   ◆第３要素：幅設定          コンテンツのパーセント幅定義。現在、「33」「50」「66」「100」の４パターン。別に増やしてもよい。
//                               Core2以前のレイアウトとの互換のためもあり、コンテンツ単位でも幅指定が可能となっている。
//                               「セクション定義がなくてもCore2以前のレイアウトが成立させられる」ということ。
// ■データ順
//   「mypage_layout_info」に格納されている順番は重要。
//    同じセクション位置(ボックス)に複数のコンテンツを格納する場合の表示順は、「mypage_layout_info」への登録順である。
//******************************************************************************************************************
function getMypageContentsFieldNames() {
    return array_keys($this->_mypageContents);
}
function getMypageContents($call_filepath, $emp_id, $group_id) {
    $call_from = basename($call_filepath);

    $lib_whatsnew_view_flg = "";
    $bbs_whatsnew_view_flg = "";
    if ($call_from=="left.php" || $call_from=="o_register.php" || $call_from=="employee_display_setting.php") { // マイページ / オプション設定 /
        $optionRow = $this->getOptionRow($emp_id);
        $liboptionRow = $this->GetLiboptionRow(C2_LOGIN_EMP_ID);// 文書管理オプション設定情報
        $lib_whatsnew_flg = c2bool($liboptionRow["whatsnew_flg"]);
        $bbsoptionRow = $this->getBbsoptionRow(C2_LOGIN_EMP_ID);// 掲示板オプション設定情報を取得
        $bbs_whatsnew_flg = c2bool($bbsoptionRow["whatsnew_flg"]);
    }
    else if ($call_from=="employee_display_group.php") {
        $optionRow = $this->getDispgroupRow($group_id);
    }
    else if ($call_from=="employee_display_bulk_setting.php") {
        $optionRow = array();
    }
    $configRow = $this->getConfigRow();
    $configRow_use_web = $configRow["use_web"];
    $applications = $this->getApplications();
    $authmstExp = $this->getAuthmstExp($emp_id);
    $erasedAppIds = $this->getErasedAppIds();

    $this->_mypageContents["top_cas_flg"]["jp"] = $this->getCasTitle();
    // マイページ設定の追加定義を初期化
    foreach ($this->_mypageContents as $field => $item) {
        $this->_mypageContents[$field]["auth_ok"] = ""; // マイページコンテンツに、機能権限なし。(authmst)
        $this->_mypageContents[$field]["disp_leftphp"] = ""; // 実際のマイページには、表示しない
        $this->_mypageContents[$field]["lcs_ng"] = ""; // マイページコンテンツに、ライセンス無しではない（つまりライセンスあり）
        $this->_mypageContents[$field]["section"] = "1_1"; // マイページコンテンツの表示セクションは、1の1。
    }

    $optionRow = $this->getOptionRow($emp_id);
    $mypage_layout_info = $optionRow["mypage_layout_info"];// "[section]:1:33_67,top_ext_flg:1_1:60,top_fplus_flg:1_1:30" みたいな
    $ary = explode(",", $mypage_layout_info); // カンマディバイド
    $new_array = array();
    $mypage_sections = array(0=>"", 1=>"100"); // 初期化。ゼロは意味なし。第一セクションは幅100%、の意味
    foreach ($ary as $item) {
        if ($item=="") continue; // データがカラの場合もある。飛ばす。
        list($field, $section, $width) = explode(":", $item);
        // セクション定義の場合
        if ($field=="[section]") {
            $mypage_sections[(int)$section] = $width; // セクション定義のセクション番号に、幅設定(33_67など)をセット
            continue;
        }
        // コンテンツ定義の場合
        if (!array_key_exists($field, $this->_mypageContents)) continue; // ゴミデータは無いと思うが、あれば飛ばす。
        $this->_mypageContents[$field]["section"] = $section; // セクション位置(1_3など)をセット。不正でもJavaScript側で補正が入る。
        $this->_mypageContents[$field]["width"] = $width;     // 幅設定(33など整数)をセット。不正でもJavaScript側で補正が入る。
        $new_array[$field] = $this->_mypageContents[$field];  // 出現順でボックスへの格納順が決まる。出現順で新しい配列へ格納する。
    }
    // セクションレイアウト定義に登録されていなかったコンテンツを追加する。
    // ボックス設定を指定していないため、最終的には、JavaScriptにより、1_1ボックスの末尾へ追加されることになるだろう。
    // また、幅設定は上書きされていないため、$_mypageContentsの初期設定状態のwidthで表示されるだろう。
    foreach ($this->_mypageContents as $field => $row) {
        if (!array_key_exists($field, $new_array)) $new_array[$field] = $row;
    }
    // 並べ直した定義を、正しい定義として上書き
    $this->_mypageContents = $new_array;

    // 権限系確認
    // ※Core2以前では、最新情報(whatsnew)について、top_msg_flgとtop_aprv_flgの両方のフラグがオンの場合に表示する判定であった。
    // ※しかし、設定画面ではtop_msg_flgとtop_aprv_flg片側をオン、片側をオフ、などとすることはできない。つまり、２つの意味は無い。
    // ※Core2より、以下とする。
    //   ・top_msg_flgのみ参照。top_msg_flgのみで判定。top_aprv_flgは参照しない。
    //   ・top_msg_flgを更新する際は、top_aprv_flgも同じ値で更新。（意味は無いが、どこかで利用しているのかもしれない）
    foreach ($this->_mypageContents as $field => $item) {
        if ($optionRow) {
            $this->_mypageContents[$field]["db_on"] = c2bool($optionRow[$field]); // 1orカラ文字を格納
        }
        // オプション設定が指定されていない場合はバルク設定。
        // Core2以前の設定とするため、定義初期状態から、さらに以下のデフォルトチェックボックスを外す。大した意味はない。
        else {
            if ($field=="top_inci_flg" || $field=="top_fplus_flg" || $field=="top_cas_flg" || $field=="top_ladder_flg" || $field=="top_event_flg") {
                $this->_mypageContents[$field]["db_on"] = "";
            }
        }
        // ライセンス。ライセンス管理対象なのにライセンスが無い場合はオフにする
        $appid = $this->_mypageContents[$field]["appid"];
        if ($applications[$appid]["lcs_ng"]) $this->_mypageContents[$field]["lcs_ng"] = "1";
        if (in_array($appid, $erasedAppIds)) $this->_mypageContents[$field]["app_ng"] = "1";
        if ($emp_id!="") {
            $auth_flag = $applications[$appid]["auth_flag"];
            $this->_mypageContents[$field]["auth_ok"] = $authmstExp[$auth_flag]["auth_ok"];
        }
    }

    // $emp_idは、個人設定のときのみ存在する。
    if ($emp_id!="") {
        $this->_mypageContents["top_free_text_flg"]["auth_ok"] = "1";
//        $this->_mypageContents["schdsrch"]["auth_ok"] = $EAU["emp_schd_flg"];
        $this->_mypageContents["top_event_flg"]["auth_ok"] = "1";
        $this->_mypageContents["top_wic_flg"]["auth_ok"] = (c2bool($configRow_use_web) ? "1" : "");
        if ($authmstExp["emp_phone_flg"]["auth_ok"] || $authmstExp["emp_aprv_flg"]["auth_ok"] || $lib_whatsnew_view_flg || $bbs_whatsnew_view_flg) {
            $this->_mypageContents["top_msg_flg"]["auth_ok"] = 1; // 伝言メモ or 決裁・申請 or 最近登録された文書 or 最近更新された掲示板
        }
        if ($authmstExp["emp_attdcd_flg"]["auth_ok"]) $this->_mypageContents["top_timecard_error_flg"]["auth_ok"] = "1"; // 出勤表
        if ($authmstExp["emp_wkadm_flg"]["auth_ok"]) $this->_mypageContents["top_timecard_error_flg"]["auth_ok"] = "1"; // 勤務管理／出勤表管理者
    }

    foreach ($this->_mypageContents as $field => $item) {
        if (!$item["db_on"]) continue; // マイページ設定で非表示とされている。スルー
        if (!$item["auth_ok"]) continue; // 機能権限が無い。スルー
        if ($item["lcs_ng"]) continue; // ライセンス管理対象なのにライセンスが無い。スルー
        if ($item["app_ng"]) continue; // 利用アプリでない。スルー
        $this->_mypageContents[$field]["disp_leftphp"] = 1; // マイページ画面へ表示確定
    }

    $html = array();
    if ($call_from!="left.php") {
        $fidx=0;
        $erasedAppIds = $this->getErasedAppIds();
        foreach ($this->_mypageContents as $field => $row) {
            $appid = $this->_mypageContents[$field]["appid"];
            if (in_array($appid, $erasedAppIds, true)) continue; // 利用対象外。スルー
            if ($row["lcs_ng"]) continue; // ライセンス無し。スルー
            if ($emp_id!="") {
                if (!$row["auth_ok"]) continue; // 機能権限なし。スルー
            }
            $jp = $row["jp"];
            if (!$jp) {
                $info = $applications[$appid];
                $jp = $info["jp"];
            }
            $fidx++;
            $html[]= ' <div id="mypage_item'.$fidx.'" item_index="'.$fidx.'" field_name="'.$field.'" class="mypage_item"';
            $html[]= ' style="cursor:move; position:relative; margin-top:4px; border-style:solid; border-width:1px; border-color:#ffffff';
            $html[]= ' #8fb0d6 #5279a5 #9ab3cb; background-color:#f6f9ff; padding:2px" section="'.$row["section"].'">';
            $html[]= ' <table cellspacing="0" cellpadding="0"><tr>';
            $html[]= ' <td style="width:150px"><nobr><label>';
            $html[]= '<input type="checkbox" name="chk_'.$field.'" id="chk_'.$field.'" value="1" '.($row["db_on"]?" checked":"").'>'.$jp;
            $html[]= '</label></nobr></td>';
            $html[]= ' <td style="padding-left:10px">';
            $html[]= ' <select name="ddl_'.$field.'" id="ddl_'.$fidx.'">';
            $html[]= ' <option value="33"'.($row["width"]==33 ? ' selected':'').'>1/3</option>';
            $html[]= ' <option value="50"'.($row["width"]==50 ? ' selected':'').'>1/2</option>';
            $html[]= ' <option value="66"'.($row["width"]==66 ? ' selected':'').'>2/3</option>';
            $html[]= ' <option value="100"'.($row["width"]==100 ? ' selected':'').'>フル</option>';
            $html[]= ' </select>';
            $html[]= ' </td>';
            $html[]= ' </tr></table>';
            $html[]= ' </div>';
        }
    }
    return array($this->_mypageContents, $mypage_sections, implode("\n", $html));
}






//******************************************************************************************************************
// $_applicationAuthView
//
// employee_auh_list.php（職員登録⇒権限管理）
// employee_auth_group.php（職員登録⇒権限グループ）
// employee_auth_bulk_setting.php（職員登録⇒一括設定⇒権限リンク⇒設定内容を「個別に設定」ラジオ）
// employee_auth_list.php（職員登録⇒権限管理）
// 「KIHON」「KANRI」「PATIENT」「GYOUMU」「INTRA」
// 下の並びのまま画面に出ます。
// セクション単位で順番並び替え可能
// セクション内並び替え可能
//******************************************************************************************************************
var $_applicationAuthView = array(
    array(
        "section_type"=>"BASIC_THREE_COLS",
        "section_title"=>"基本機能",
        "col_size"=>2,
        "view_data"=> array(
        //    [0]appid    [1] １列目checkbox  [2] ２列目checkbox   [3] 代替タイトル
        //---------------------------------------------------------------------------
        array("webml",    "emp_webml_flg",    "emp_webmladm_flg",  ""),
        array("schd",     "emp_schd_flg",     "emp_schdplc_flg",   ""),
        array("pjt",      "emp_pjt_flg",      "emp_pjtadm_flg",    ""),
        array("newsuser", "emp_newsuser_flg", "emp_news_flg",      ""),
        array("work",     "emp_work_flg",     "",                  ""),
        array("phone",    "emp_phone_flg",    "",                  ""),
        array("resv",     "emp_resv_flg",     "emp_fcl_flg",       ""),
        array("attdcd",   "emp_attdcd_flg",   "emp_wkadm_flg",     "出勤表／勤務管理"),
        array("aprv",     "emp_aprv_flg",     "emp_wkflw_flg",     "決裁・申請／ワークフロー"),
        array("conf",     "emp_conf_flg",     "",                  ""),
        array("bbs",      "emp_bbs_flg",      "emp_bbsadm_flg",    ""),
        array("qa",       "emp_qa_flg",       "emp_qaadm_flg",     ""),
        array("lib",      "emp_lib_flg",      "emp_libadm_flg",    ""),
        array("adbk",     "emp_adbk_flg",     "emp_adbkadm_flg",   ""),
        array("ext",      "emp_ext_flg",      "emp_extadm_flg",    ""),
        array("link",     "emp_link_flg",     "emp_linkadm_flg",   ""),
        array("pass1",    "emp_pass_flg",     "",                  "パスワード変更"),
        array("cmt",      "emp_cmt_flg",      "emp_cmtadm_flg",    ""),
        array("memo",     "emp_memo_flg",     "",                  ""),
        array("cauth",    "emp_cauth_flg",    "",                  "")
    )),
    array(
        "section_type"=>"KANRI",
        "section_title"=>"管理機能",
        "col_size"=>8,
        "view_data"=> array(
        //    [0]appid  [1]該当なし [2]２列目       [3]３列目       [4] 以下同文・・・・
        //------------------------------------------------------------------------
        array("tmgd",      "", "emp_tmgd_flg",      "",              "", "", "", "", ""), // 院内行事登録
        array("empif",     "", "emp_reg_flg",       "emp_empif_flg", "", "", "", "", ""), // 職員登録
        array("master",    "", "",                  "",              "emp_hspprf_flg", "emp_dept_flg", "emp_job_flg", "emp_status_flg", "emp_attditem_flg"),
        array("config",    "", "emp_config_flg",    "",              "", "", "", "", ""), // 環境設定
        array("lcs",       "", "emp_lcs_flg",       "",              "", "", "", "", ""), // ライセンス管理
        array("accesslog", "", "emp_accesslog_flg", "",              "", "", "", "", "")  // アクセスログ
    )),
    array(
        "section_type"=>"PATIENT",
        "section_title"=>"業務機能",
        "col_size"=>7,
        "view_data"=> array(
        //    [0]appid    [1]１列目        [2]２列目       [3] 以下同文・・・・
        //------------------------------------------------------------------------
        array("patient",  "emp_ptreg_flg", "emp_ptif_flg", "emp_outreg_flg", "emp_inout_flg", "emp_disreg_flg", "emp_dishist_flg", "emp_ptadm_flg")
    )),
    array(
        "section_type"=>"BASIC_THREE_COLS",
        "section_title"=>"（差し込みセクションタイトル）ちなみに今後はライセンス無しは非表示となります",
        "col_size"=>2,
        "view_data"=> array(
        //    [0]appid    [1] １列目checkbox  [2] ２列目checkbox   [3] 代替タイトル
        //---------------------------------------------------------------------------
        array("omusr1",   "emp_omusr1_flg",   "emp_omadm1_flg",     ""),
        array("omusr2",   "emp_omusr2_flg",   "emp_omadm2_flg",     ""),
        array("omusr3",   "emp_omusr3_flg",   "emp_omadm3_flg",     ""),
        array("omusr4",   "emp_omusr4_flg",   "emp_omadm4_flg",     ""),
        array("omusr5",   "emp_omusr5_flg",   "emp_omadm5_flg",     ""),
        array("inci",     "emp_inci_flg",     "emp_rm_flg",         ""),
        array("fplus",    "emp_fplus_flg",    "emp_fplusadm_flg",   ""),
        array("shift",    "emp_shift_flg",    "emp_shiftadm_flg",   "")
    )),
    array(
        "section_type"=>"BASIC_THREE_COLS",
        "section_title"=>'<span style="color:#008800">このレイアウトを画面メンテすることはできませんが、リリースごとにDBで指定するようにする事は可能</span>',
        "col_size"=>2,
        "view_data"=> array(
        //    [0]appid    [1] １列目checkbox  [2] ２列目checkbox   [3] 代替タイトル
        //---------------------------------------------------------------------------
        array("ccusr1",   "emp_ccusr1_flg",   "emp_ccadm1_flg",     ""), // 管理画面不要なら[2]を消して下さい
        array("ccusr2",   "emp_ccusr2_flg",   "emp_ccadm2_flg",     ""), // 管理画面不要なら[2]を消して下さい
        array("ccusr3",   "emp_ccusr3_flg",   "emp_ccadm3_flg",     ""), // 管理画面不要なら[2]を消して下さい
        array("ccusr4",   "emp_ccusr4_flg",   "emp_ccadm4_flg",     ""), // 管理画面不要なら[2]を消して下さい
        array("ccusr5",   "emp_ccusr5_flg",   "emp_ccadm5_flg",     ""), // 管理画面不要なら[2]を消して下さい
        array("ccusr6",   "emp_ccusr6_flg",   "emp_ccadm6_flg",     ""),
        array("ccusr7",   "emp_ccusr7_flg",   "emp_ccadm7_flg",     ""),
        array("ccusr8",   "emp_ccusr8_flg",   "emp_ccadm8_flg",     ""),
        array("ccusr9",   "emp_ccusr9_flg",   "emp_ccadm9_flg",     ""),
        array("ccusr10",  "emp_ccusr10_flg",  "emp_ccadm10_flg",    ""),
        array("ccusr11",  "emp_ccusr11_flg",  "emp_ccadm11_flg",    ""),
        array("ccusr12",  "emp_ccusr12_flg",  "emp_ccadm12_flg",    ""),
        array("ccusr13",  "emp_ccusr13_flg",  "emp_ccadm13_flg",    ""),
        array("ccusr14",  "emp_ccusr14_flg",  "emp_ccadm14_flg",    ""),
        array("ccusr15",  "emp_ccusr15_flg",  "emp_ccadm15_flg",    ""),
        array("search",   "emp_search_flg",   "",                   ""),
        array("amb",      "emp_amb_flg",      "emp_ambadm_flg",     ""),
        array("ward",     "emp_ward_flg",     "emp_ward_reg_flg",   ""),
        array("entity",   "",                 "emp_entity_flg",     ""),
        array("nlcs",     "emp_nlcs_flg",     "emp_nlcsadm_flg",    ""),
//      array("inci",     "emp_inci_flg",     "emp_rm_flg",         ""),
        array("manabu",   "emp_manabu_flg",   "emp_manabuadm_flg",  ""),
        array("med",      "emp_med_flg",      "emp_medadm_flg",     ""),
        array("kview",    "emp_kview_flg",    "emp_kviewadm_flg",   ""),
        array("biz",      "emp_biz_flg",      ""),
        array("shift2",   "emp_shift2_flg",   "emp_shift2adm_flg",  ""),
        array("cas",      "emp_cas_flg",      "emp_casadm_flg",     ""),
        array("jinji",    "emp_jinji_flg",    "emp_jinjiadm_flg",   ""),
        array("ladder",   "emp_ladder_flg",   "emp_ladderadm_flg",  ""),
        array("career",   "emp_career_flg",   "emp_careeradm_flg",  ""),
        array("jnl",      "emp_jnl_flg",      "emp_jnladm_flg",     "")
    )),
    array(
        "section_type"=>"INTRA",
        "section_title"=>"",
        "col_size"=>6,
        "view_data"=> array(
        //    [0]appid [1]１列目        [2]２列目         [3]以下同文・・・・
        //---------------------------------------------------------------------------
        array("intra", "emp_intra_flg", "emp_ymstat_flg", "emp_intram_flg", "emp_stat_flg", "emp_allot_flg", "emp_life_flg")
    ))
);
var $_authmstSlave = array(
    "emp_webmladm_flg"  => array("slave"=>"emp_webml_flg"),
    "emp_schdplc_flg"   => array("slave"=>"emp_schd_flg"),
    "emp_pjtadm_flg"    => array("slave"=>"emp_pjt_flg"),
    "emp_fcl_flg"       => array("slave"=>"emp_resv_flg"),
    "emp_bbsadm_flg"    => array("slave"=>"emp_bbs_flg"),
    "emp_qaadm_flg"     => array("slave"=>"emp_qa_flg"),
    "emp_adbkadm_flg"   => array("slave"=>"emp_adbk_flg"),
    "emp_linkadm_flg"   => array("slave"=>"emp_link_flg"),
    "emp_cmtadm_flg"    => array("slave"=>"emp_cmt_flg"),
    "emp_reg_flg"       => array("slave"=>"emp_empif_flg"),
    "emp_ptreg_flg"     => array("slave"=>"emp_ptif_flg"),
    "emp_outreg_flg"    => array("slave"=>"emp_inout_flg"),
    "emp_disreg_flg"    => array("slave"=>"emp_dishist_flg"),
    "emp_rm_flg"        => array("slave"=>"emp_inci_flg"),
    "emp_ambadm_flg"    => array("slave"=>"emp_amb_flg"),
    "emp_ward_reg_flg"  => array("slave"=>"emp_ward_flg"),
    "emp_nlcsadm_flg"   => array("slave"=>"emp_nlcs_flg"),
    "emp_rm_flg"        => array("slave"=>"emp_inci_flg"),
    "emp_fplusadm_flg"  => array("slave"=>"emp_fplus_flg"),
    "emp_manabuadm_flg" => array("slave"=>"emp_manabu_flg"),
    "emp_medadm_flg"    => array("slave"=>"emp_med_flg"),
    "emp_kviewadm_flg"  => array("slave"=>"emp_kview_flg"),
    "emp_shiftadm_flg"  => array("slave"=>"emp_shift_flg"),
    "emp_shift2adm_flg" => array("slave"=>"emp_shift2_flg"),
    "emp_casadm_flg"    => array("slave"=>"emp_cas_flg"),
    "emp_jinjiadm_flg"  => array("slave"=>"emp_jinji_flg"),
    "emp_ladderadm_flg" => array("slave"=>"emp_ladder_flg"),
    "emp_careeradm_flg" => array("slave"=>"emp_career_flg"),
    "emp_jnladm_flg"    => array("slave"=>"emp_jnl_flg"),
    "emp_ymstat_flg"    => array("slave"=>"emp_intra_flg", "or_masters"=>"emp_ymstat_flg,emp_intram_flg,emp_stat_flg,emp_allot_flg,emp_life_flg"),
    "emp_intram_flg"    => array("slave"=>"emp_intra_flg", "or_masters"=>"emp_ymstat_flg,emp_intram_flg,emp_stat_flg,emp_allot_flg,emp_life_flg"),
    "emp_stat_flg"      => array("slave"=>"emp_intra_flg", "or_masters"=>"emp_ymstat_flg,emp_intram_flg,emp_stat_flg,emp_allot_flg,emp_life_flg"),
    "emp_allot_flg"     => array("slave"=>"emp_intra_flg", "or_masters"=>"emp_ymstat_flg,emp_intram_flg,emp_stat_flg,emp_allot_flg,emp_life_flg"),
    "emp_life_flg"      => array("slave"=>"emp_intra_flg", "or_masters"=>"emp_ymstat_flg,emp_intram_flg,emp_stat_flg,emp_allot_flg,emp_life_flg"),
    "emp_omadm1_flg"    => array("slave"=>"emp_omusr1_flg"),
    "emp_omadm2_flg"    => array("slave"=>"emp_omusr2_flg"),
    "emp_omadm3_flg"    => array("slave"=>"emp_omusr3_flg"),
    "emp_omadm4_flg"    => array("slave"=>"emp_omusr4_flg"),
    "emp_omadm5_flg"    => array("slave"=>"emp_omusr5_flg"),
    "emp_ccadm1_flg"    => array("slave"=>"emp_ccusr1_flg"), // あってもなくてもよい
    "emp_ccadm2_flg"    => array("slave"=>"emp_ccusr2_flg"),
    "emp_ccadm3_flg"    => array("slave"=>"emp_ccusr3_flg"),
    "emp_ccadm4_flg"    => array("slave"=>"emp_ccusr4_flg"),
    "emp_ccadm5_flg"    => array("slave"=>"emp_ccusr5_flg"),
    "emp_ccadm6_flg"    => array("slave"=>"emp_ccusr6_flg"),
    "emp_ccadm7_flg"    => array("slave"=>"emp_ccusr7_flg"),
    "emp_ccadm8_flg"    => array("slave"=>"emp_ccusr8_flg"),
    "emp_ccadm9_flg"    => array("slave"=>"emp_ccusr9_flg"),
    "emp_ccadm10_flg"   => array("slave"=>"emp_ccusr10_flg"),
    "emp_ccadm11_flg"   => array("slave"=>"emp_ccusr11_flg"),
    "emp_ccadm12_flg"   => array("slave"=>"emp_ccusr12_flg"),
    "emp_ccadm13_flg"   => array("slave"=>"emp_ccusr13_flg"),
    "emp_ccadm14_flg"   => array("slave"=>"emp_ccusr14_flg"),
    "emp_ccadm15_flg"   => array("slave"=>"emp_ccusr15_flg"),
    // 以下、チェックボックスとしては存在しないスレーブ定義。
    // 権限系登録時に参照され、自動セットされる。
    "emp_hspprf_flg"    => array("slave"=>"emp_master_flg", "slave_is_not_checkbox"=>"1"),
    "emp_dept_flg"      => array("slave"=>"emp_master_flg", "slave_is_not_checkbox"=>"1"),
    "emp_job_flg"       => array("slave"=>"emp_master_flg", "slave_is_not_checkbox"=>"1"),
    "emp_status_flg"    => array("slave"=>"emp_master_flg", "slave_is_not_checkbox"=>"1"),
    "emp_attditem_flg"  => array("slave"=>"emp_master_flg", "slave_is_not_checkbox"=>"1")
);
// employee_auth_bulk_setting.php
// employee_auth_group.php
// 以前存在した、employee_auth_common.php auth_fix_relations()の代替
function getAuthmstSlave() {
    for ($idx=1; $idx<=5; $idx++) {
        $row = unserialize($licenseRow["lcs_om".$idx."_custom"]);
        // 権限設定でのラジオボタンクリック制御のJavaScriptを抹消する。
        if (!$row["relate_usr_and_adm"] && $row["kousei"]!="omusr_and_omadm") {
            $this->_authmstSlave["emp_omadm".$idx."_flg"]["slave"] = "";
        }
    }
    return $this->_authmstSlave;
}
function getApplicationAuthView($emp_id) {
    $authmstExp = $this->getAuthmstExp($emp_id);
    $erasedAppIds = $this->getErasedAppIds();
    $applications = $this->getApplicationsNames();
    $authmstSlave = $this->getAuthmstSlave();
    $prfType = $this->getPrfType();
    $outApplicationAuthView = array();

    $licenseRow = $this->getLicenseRow();
    for ($idx=1; $idx<=5; $idx++) {
        $row = unserialize($licenseRow["lcs_om".$idx."_custom"]);
        // 名前変更。権限まわり
        foreach ($this->_applicationAuthView as $aidx=>$obj) {
            foreach ($obj["view_data"] as $cidx => $ritem) {
                if ($ritem[0]!="omusr".$idx) continue;
                $this->_applicationAuthView[$aidx]["view_data"][$cidx][3] = hh($row["app_name"]);
                break;
            }
        }
    }

    foreach ($this->_applicationAuthView as $idx => $block_data) {
        $newRows = array();
        $onload_javascript = array();
        foreach ($block_data["view_data"] as $idx => $rowItems) {
            $appid = $rowItems[0];
            $fields = array();
            if ($block_data["section_type"]=="BASIC_THREE_COLS" && !$rowItems[3]) { // ３列構成で、最後の要素に代替タイトルが無い。
                $rowItems[3] = $applications[$appid]["jp"]; // 通常タイトルを利用
            }
            $colSize = $block_data["col_size"];
            for ($col=1; $col<=$colSize; $col++) $fields[]= $rowItems[$col];

            if (in_array($appid, $erasedAppIds)) continue; // 利用範囲外機能である。行スルー
            if ($prfType=="2" && $appid=="entity")  continue; // 「診療科」でなく「担当部門」ならスルー

            $checkbox_exist = 0;
            for ($col=1; $col<=$colSize; $col++) {
                $field = $fields[$col-1];
                if ($authmstExp[$field]["app_ng"]) { $rowItems[$col] = ""; continue; } // 特製アプリの構成に無い。アイテムを消してスルー
                if ($authmstExp[$field]["lcs_ng"]) { $rowItems[$col] = ""; continue; } // ライセンスが無い。アイテムを消してスルー

                $slave = $authmstSlave[$field]["slave"];
                if ($slave) { // スレーブあり。つまりチェックボックスは連動して動くもので、その対象が定義されている。
                    if ($authmstSlave[$field]["slave_is_not_checkbox"]) $slave = ""; // スレーブはチェックボックスではない。
                    else if ($authmstExp[$slave]["app_ng"]) $slave = ""; // スレーブは特製アプリの構成に無い。スレーブを無かったことに。
                    else if ($authmstExp[$slave]["lcs_ng"]) $slave = ""; // スレーブにライセンスが無い。スレーブを無かったことに。
                    else { // スレーブあり。かつ有効。
                        $or_masters = $authmstSlave[$field]["or_masters"]; // クリック側の複数条件指定があるか
                        if (!$or_masters) $or_masters = $field; // クリック複数条件指定がなければ、クリック側IDをそのまま格納
                        $onload_javascript[$field] = 'AuthCBoxRelate.doCheck(\''.$or_masters.'\',\''.$slave.'\');'; // マスタ⇒スレーブ動作をさせるJavaScript
                    }
                }
                $checkbox_exist = 1;
            }
            if (!$checkbox_exist) continue; // どこかでスルーされて無効なチェックボックスが無い。よって行自体をスルー
            $newRows[] = $rowItems;
        }
        if (count($newRows)) {
            $outApplicationAuthView[] = array(
                "section_type"    => $block_data["section_type"],
                "section_title"   => $block_data["section_title"],
                "col_size"    => $block_data["col_size"],
                "onload_javascript" => $onload_javascript,
                "view_data"       => $newRows
            );
        }
    }
    return $outApplicationAuthView;
}




//******************************************************************************************************************
// $_extraLabels
//******************************************************************************************************************
var $_extraLabels = array();
function getExtraLabel($field) {
    if (!count($this->_extraLabels)) {
        $intramenuRow = $this->getIntramenuRow();
        $this->_extraLabels["emp_stat_flg"] = $intramenuRow["menu1"];
        $this->_extraLabels["emp_allot_flg"] = $intramenuRow["menu2_4"];
        $this->_extraLabels["emp_life_flg"] = $intramenuRow["menu3"];
        $this->_extraLabels["emp_ymstat_flg"] = "月間・年間".$intramenuRow["menu1"]."参照";
    }
    return $this->_extraLabels[$field];
}







//******************************************************************************************************************
// $_licenseIdToAppId
//
// licenseテーブルのlicence_idに対応するアプリ一覧(lcs_funcxxx)
// 並び順は、ライセンス管理⇒ライセンス登録画面での表示順。
// lcsidx=appid形式
// 手動で紐つけてください
// エントリがなければライセンスフリーとみなされる。
//******************************************************************************************************************
var $_licenseIdToAppId = array(
    "4"=>"intra",     "7"=>"search",    "1"=>"patient",   "15"=>"amb",      "2"=>"ward", // entityはward+med
    "10"=>"nlcs",     "5"=>"inci",      "13"=>"fplus",    "12"=>"manabu",   "6"=>"med",
    "11"=>"kview",    "9"=>"shift",     "18"=>"shift2",   "8"=>"cas",       "16"=>"jinji",
    "17"=>"ladder",   "19"=>"career",   "14"=>"jnl",      "3"=>"biz",
    // 21以上はライセンス発行のためのハッシュ追加登録方法が異なる。license/license_menu.php＋updateLicenseCounts()参照。
    "21"=>"omusr1",  "22"=>"omusr2",  "23"=>"omusr3",  "24"=>"omusr4",  "25"=>"omusr5",
    "26"=>"omadm1",  "27"=>"omadm2",  "28"=>"omadm3",  "29"=>"omadm4",  "30"=>"omadm5",
    "31"=>"ccusr1",  "32"=>"ccusr2",  "33"=>"ccusr3",  "34"=>"ccusr4",  "35"=>"ccusr5",
    "36"=>"ccusr6",  "37"=>"ccusr7",  "38"=>"ccusr8",  "39"=>"ccusr9",  "40"=>"ccusr10",
    "41"=>"ccusr11", "42"=>"ccusr12", "43"=>"ccusr13", "44"=>"ccusr14", "45"=>"ccusr15",
    "46"=>"ccadm1",  "47"=>"ccadm2",  "48"=>"ccadm3",  "49"=>"ccadm4",  "50"=>"ccadm5", //ccadm1アプリは存在しない
    "51"=>"ccadm6",  "52"=>"ccadm7",  "53"=>"ccadm8",  "54"=>"ccadm9",  "55"=>"ccadm10",
    "56"=>"ccadm11", "57"=>"ccadm12", "58"=>"ccadm13", "59"=>"ccadm14", "60"=>"ccadm15"
);
function getLicenseIdToAppId() {
    return $this->_licenseIdToAppId;
}


//******************************************************************************************************************
// ライセンス画面
//
// licenseテーブルのライセンス数とライセンス名情報（個別アプリのみ）
// ライセンス数の存在するアプリと、名前を格納
//******************************************************************************************************************
var $_lcsLabels = array();
function getLcsLabels() {
    if (count($this->_lcsLabels)) return $this->_lcsLabels;
    $LBP = $this->getLabelByProfile();
    $prf_type = $this->getPrfType();

    $applications = $this->getApplications();
    foreach ($applications as $appid => $item) {
        $lcs_max = $item["lcs_max"];
        if (!$lcs_max) continue;
        if ($lcs_max=="unlimited") continue;
        if ($lcs_max=="lcs_basic_user") continue;
        $this->_lcsLabels[$lcs_max] = $item["jp"];
    }
    $this->_lcsLabels["lcs_basic_user"]  = "基本機能";
    $this->_lcsLabels["lcs_report_user"] = $LBP["MED_REPORT"][$prf_type];
    $this->_lcsLabels["lcs_bed_count"]   = $LBP["BED_MANAGE"][$prf_type];
    $this->_lcsLabels["lcs_shift_emps"]  = "勤務シフト作成";

    return $this->_lcsLabels;
}
function getLcsLabel($field) {
    $lcsLabels = $this->getLcsLabels();
    return $lcsLabels[$field];
}


//******************************************************************************************************************
// $_sidemenuOrder
//
// 袖メニューリンクボタンとなり得る一覧、手動で調整のこと
// 本ファイルのみで参照。プログラムによる追加削除修正は行われない。
//******************************************************************************************************************
var $_sidemenuOrders = array(
    "KIHON" => array(
        "mypage", "webml", "schd", "pjt", "newsuser",
        "work", "phone", "resv", "attdcd", "aprv",
        "conf", "bbs", "qa", "lib", "adbk",
        "link", "ext", "pass1", "pass2", "option"
    ),
    "GYOUMU" => array(
        "intra", "search", "patient", "amb", "ward",
        "nlcs", "inci", "fplus", "manabu", "med",
        "kview", "shift", "cas", "jinji", "ladder",
        "career", "shift2", "jnl", "biz",
        "ccusr1", "ccusr2", "ccusr3", "ccusr4", "ccusr5",
        "ccusr6", "ccusr7", "ccusr8", "ccusr9", "ccusr10",
        "ccusr11","ccusr12","ccusr13","ccusr14","ccusr15"
    ),
    "KANRI" => array(
        "news", "tmgd", "wkadm", "wkflw", "cmtadm",
        "empif", "master", "config", "lcs", "accesslog",
        "ccadm1", "ccadm2", "ccadm3", "ccadm4", "ccadm5", // ccadm1は存在しない。
        "ccadm6", "ccadm7", "ccadm8", "ccadm9", "ccadm10",
        "ccadm11","ccadm12","ccadm13","ccadm14","ccadm15"
    )
);
function getSidemenuOrders($where) {
    $orders = $this->_sidemenuOrders; // 複製
    $licenseRow = $this->getLicenseRow();
    for ($idx=1; $idx<=5; $idx++) {
        $row = unserialize($licenseRow["lcs_om".$idx."_custom"]);
        $orders[$row["usr_sidemenu_section"]][] = "omusr".$idx;
        $orders[$row["adm_sidemenu_section"]][] = "omadm".$idx;
    }
    return $orders[$where];
}



//******************************************************************************************************************
// $_l4pLogFields
// config_log.php システムログ。環境設定⇒ログ。とりわけ、ログ⇒オプション。
// のちのち「chkflg」が画面側で付与される
//******************************************************************************************************************
var $_l4pLogFields = array(
    "WEBMAIL"   => array("appid"=>"webml"),
    "SCHEDULE"  => array("appid"=>"schd"),
    "PROJECT"   => array("appid"=>"pjt"),
    "NEWS"      => array("appid"=>"news|newsuser"),
    "TASK"      => array("appid"=>"work"),
    "MESSAGE"   => array("appid"=>"phone"),
    "FACILITY"  => array("appid"=>"resv"),
    "TIMECARD"  => array("appid"=>"attdcd"),
    "WORKFLOW"  => array("appid"=>"aprv"),
    "NETCF"     => array("appid"=>"conf"),
    "BBS"       => array("appid"=>"bbs"),
    "QA"        => array("appid"=>"qa"),
    "LIBRARY"   => array("appid"=>"lib"),
    "ADDRESS"   => array("appid"=>"adbk"),
    "EXT"       => array("appid"=>"ext"),
    "LINK"      => array("appid"=>"link"),
    "PASSWD"    => array("appid"=>"pass1|pass2"),
    "COMM"      => array("appid"=>"cmt|cmtadm"),
    "MEMO"      => array("appid"=>"memo"),
    "EVENT"     => array("appid"=>"tmgd"),
    "EMPLOYEE"  => array("appid"=>"empif"),
    "MASTER"    => array("appid"=>"master"),
    "CONFIG"    => array("appid"=>"config"),
    "LICENSE"   => array("appid"=>"lcs"),
    "PATIENT"   => array("appid"=>"patient"),
    "KENSAKU"   => array("appid"=>"search"),
    "OUTPT"     => array("appid"=>"amb"),
    "BED"       => array("appid"=>"ward"),
    "HPDEPT"    => array("appid"=>"entity"),
    "NURSE_REC" => array("appid"=>"nlcs"),
    "FANTOL"    => array("appid"=>"inci"),
    "FPLUS"     => array("appid"=>"fplus"),
    "E_LEARN"   => array("appid"=>"manabu"),
    "SUMMARY"   => array("appid"=>"med"),
    "KVIEW"     => array("appid"=>"kview"),
    "BIZ"       => array("appid"=>"biz"),
    "SHIFT"     => array("appid"=>"shift"),
    "CAS"       => array("appid"=>"cas"),
    "PERSONNEL" => array("appid"=>"jinji"),
    "JOURNAL"   => array("appid"=>"jnl"),
    "INTRA"     => array("appid"=>"intra"),
    "SQL"       => array("appid"=>"*",             "chkflg"=>"", "jp"=>"データベース"),
    "COMMON"    => array("appid"=>"*",             "chkflg"=>"", "jp"=>"共通")
//    "CONFIG"    => array("appid"=>"*",             "chkflg"=>"", "name"=>"管理")
);
// ログ対象のもののうち、ライセンスの無いものと、非利用アプリを除去して返す。
function getL4PLogFields() {
    $out = array();
    $applications = $this->getApplications();
    $erasedAppIds = $this->getErasedAppIds();
    foreach ($this->_l4pLogFields as $syslogid => $item) {
        list($appid1, $appid2) = explode("|", $item["appid"]);
        $ok = 0;
        if ($appid1) {
            if ($appid1=="*") $ok = 1;
            if (!$applications[$appid1]["lcs_ng"] && !in_array($appid1, $erasedAppIds)) $ok = 1;
        }
        if ($appid2) {
            if ($appid2=="*") $ok = 1;
            if (!$applications[$appid2]["lcs_ng"] && !in_array($appid2, $erasedAppIds)) $ok = 1;
        }
        if (!$ok) continue;
        if ($item["jp"]=="") {
            if ($appid1 && ($appid1!="*" || (!$applications[$appid1]["lcs_ng"] && !in_array($appid1,$erasedAppIds)))) $item["jp"]=$applications[$appid1]["jp"];
            if ($appid2 && ($appid2!="*" || (!$applications[$appid2]["lcs_ng"] && !in_array($appid2,$erasedAppIds)))) $item["jp"]=$applications[$appid2]["jp"];
        }
        $out[$syslogid] = $item;
    }
    return $out;
}



//******************************************************************************************************************
// $_aclgTools
// アクセスログ４画面  日別一覧(ユーザ)、日別一覧(管理画面)、  機能別詳細、    設定
//                     aclg_menu.php     aclg_menu_admin.php   aclg_tools.php  aclg_config.php
//******************************************************************************************************************
var $_aclgTools = array(
    //キーはaclg_config.php                   aclg_menu_admin.phpで  aclg_tools.phpで                      $applicationsで   機能側で利用
    //での識別子appid                         表示するなら"1"        表示するなら"1"                       未定義の日本語    しているコード
    "all"   =>array("appid"=>"*",             "menu_admin.php"=>"",  "tools.php"=>"1", "section"=>"",      "jp"=>"すべて",   "code"=>""    ), // 0
    "login" =>array("appid"=>"*",             "menu_admin.php"=>"",  "tools.php"=>"1", "section"=>"KIHON", "jp"=>"ログイン", "code"=>"0001"), // 1
    "webml" =>array("appid"=>"webml",         "menu_admin.php"=>"1", "tools.php"=>"1", "section"=>"KIHON", "jp"=>"",         "code"=>"0101"), // 2
    "schd"  =>array("appid"=>"schd",          "menu_admin.php"=>"1", "tools.php"=>"1", "section"=>"KIHON", "jp"=>"",         "code"=>"0103"), // 3
    "pjt"   =>array("appid"=>"pjt",           "menu_admin.php"=>"1", "tools.php"=>"1", "section"=>"KIHON", "jp"=>"",         "code"=>"0104"), // 4
    "news"  =>array("appid"=>"news|newsuser", "menu_admin.php"=>"1", "tools.php"=>"1", "section"=>"KIHON", "jp"=>"",         "code"=>"0105"), // 5
    "work"  =>array("appid"=>"work",          "menu_admin.php"=>"",  "tools.php"=>"1", "section"=>"KIHON", "jp"=>"",         "code"=>"0106"), // 6
    "phone" =>array("appid"=>"phone",         "menu_admin.php"=>"",  "tools.php"=>"1", "section"=>"KIHON", "jp"=>"",         "code"=>"0107"), // 7
    "resv"  =>array("appid"=>"resv",          "menu_admin.php"=>"1", "tools.php"=>"1", "section"=>"KIHON", "jp"=>"",         "code"=>"0108"), // 8
    "aprv"  =>array("appid"=>"aprv",          "menu_admin.php"=>"1", "tools.php"=>"1", "section"=>"KIHON", "jp"=>"",         "code"=>"0109"), // 9
    "bbs"   =>array("appid"=>"bbs",           "menu_admin.php"=>"1", "tools.php"=>"1", "section"=>"KIHON", "jp"=>"",         "code"=>"0110"), // 10
    "qa"    =>array("appid"=>"qa",            "menu_admin.php"=>"1", "tools.php"=>"1", "section"=>"KIHON", "jp"=>"",         "code"=>"0111"), // 11
    "lib"   =>array("appid"=>"lib",           "menu_admin.php"=>"1", "tools.php"=>"1", "section"=>"KIHON", "jp"=>"",         "code"=>"0112"), // 12
    "adbk"  =>array("appid"=>"adbk",          "menu_admin.php"=>"1", "tools.php"=>"1", "section"=>"KIHON", "jp"=>"",         "code"=>"0113"), // 13
    "link"  =>array("appid"=>"link",          "menu_admin.php"=>"1", "tools.php"=>"1", "section"=>"KIHON", "jp"=>"",         "code"=>"0114"), // 14
    "ext"   =>array("appid"=>"ext",           "menu_admin.php"=>"1", "tools.php"=>"1", "section"=>"KIHON", "jp"=>"",         "code"=>"0115"), // 15
    "pass1" =>array("appid"=>"pass1|pass2",   "menu_admin.php"=>"",  "tools.php"=>"",  "section"=>"KIHON", "jp"=>"",         "code"=>"0116"), // 16
    "inci"  =>array("appid"=>"inci",          "menu_admin.php"=>"1", "tools.php"=>"1", "section"=>"GYOUMU","jp"=>"",         "code"=>"0201"), // 17
    "empif" =>array("appid"=>"empif",         "menu_admin.php"=>"1", "tools.php"=>"1", "section"=>"KANRI", "jp"=>"",         "code"=>"0002"), // 18
    "master"=>array("appid"=>"master",        "menu_admin.php"=>"1", "tools.php"=>"1", "section"=>"KANRI", "jp"=>"",         "code"=>"0003"), // 19
    "config"=>array("appid"=>"config",        "menu_admin.php"=>"",  "tools.php"=>"",  "section"=>"KANRI", "jp"=>"",         "code"=>"0004"), // 20
    "lcs"   =>array("appid"=>"lcs",           "menu_admin.php"=>"",  "tools.php"=>"",  "section"=>"KANRI", "jp"=>"",         "code"=>"0005"), // 21
    "daily" =>array("appid"=>"*",             "menu_admin.php"=>"",  "tools.php"=>"",  "section"=>"",      "jp"=>"日別一覧", "code"=>"0006")  // 22
);
// ログ対象のもののうち、ライセンスの無いものと、非利用アプリを除去して返す。
function getAclgTools() {
    $tools = array();
    $applications = $this->getApplications();
    $erasedAppIds = $this->getErasedAppIds();
    foreach ($this->_aclgTools as $appid => $item) {
        list($appid1, $appid2) = explode("|", $item["appid"]);
        $ok = 0;
        if ($appid1) {
            if ($appid1=="*") $ok = 1;
            if (!$applications[$appid1]["lcs_ng"] && !in_array($appid1, $erasedAppIds)) $ok = 1;
        }
        if ($appid2) {
            if ($appid2=="*") $ok = 1;
            if (!$applications[$appid2]["lcs_ng"] && !in_array($appid2, $erasedAppIds)) $ok = 1;
        }
        if (!$ok) continue;
        if ($item["jp"]=="") {
            if ($appid1 && ($appid1!="*" || (!$applications[$appid1]["lcs_ng"] && !in_array($appid1,$erasedAppIds)))) $item["jp"]=$applications[$appid1]["jp"];
            if ($appid2 && ($appid2!="*" || (!$applications[$appid2]["lcs_ng"] && !in_array($appid2,$erasedAppIds)))) $item["jp"]=$applications[$appid2]["jp"];
        }
        $tools[$appid] = $item;
    }
    return $tools;
}



//------------------------------------------------------------------------------
// メインメニュー袖メニュー一覧。「基本」「業務」「管理」ごとに排出
//------------------------------------------------------------------------------
function getHtmlMenuLinks($emp_id, $session, $where) { // $where : KIHON or GYOUMU or KANRI
    $applications = $this->getApplications();
    $applicationUrl = $this->getApplicationUrl($emp_id);
    $html = array();
    $erasedAppIds = $this->getErasedAppIds();
    $authmstExp = $this->getAuthmstExp($emp_id);
    foreach ($this->getSidemenuOrders($where) as $appid) {
        if (in_array($appid, $erasedAppIds, true)) continue; // 利用対象外。スルー
        $info = $applications[$appid];
        if (!$info) continue; // 特製アプリは抹消済。スルー
        if ($info["lcs_ng"]) continue; // ライセンス無し。スルー
        if ($info["auth_flag"] && !$authmstExp[$info["auth_flag"]]["auth_ok"]) continue; // 機能権限なし。スルー
        $urlInfo = $applicationUrl[$appid];
        if (!$urlInfo["url"] && !$urlInfo["onclick"]) continue; // URLもonclickも無い。スルー
        $href = $urlInfo["url"];
        if ($href) $href .= "?session=".$session.$urlInfo["purl"];
        else $href = "javascript:void(0)";
        $onclick = ($urlInfo["onclick"] ? ' onclick="'.c2js($urlInfo["onclick"]).'; return false;"' : "");
        $target = ($urlInfo["target"] ? $urlInfo["target"] : "floatingpage");
        $html[]=
        '<tr><td class="side_menu_link"><a href="'.$href.'"'.$onclick.' target="'.$target.'">'.
        '<img src="img/icon/'.$info["icon20"].'.gif" alt="" height="20" />'.$info["jp"].'</a></td></tr>';
    }
    return implode("\n", $html);
}



//------------------------------------------------------------------------------
// ヘッダアイコン部HTML作成（メインメニュー）
//------------------------------------------------------------------------------
// main_menu.php
function getHeaderIconHtml($emp_id, $session) { // $emp_id は C2_LOGIN_EMP_ID しかない
    $headermenuRows = c2dbGetRows("select menu_id from headermenu where emp_id = ".c2dbStr($emp_id)." order by menu_order");

    // ログアウトを明示的に加えていない場合は加える。ただし、環境設定に依存
    if (!in_array("99", $headermenuRows, true)) {
        $headmenu_logout = c2bool(c2dbGetSystemConfig("headmenu.logout"));
        if ($headmenu_logout) $headermenuRows[] = array("menu_id"=>"logout"); // ログアウト
    }
    $icon_html = array();
    $hicon = array();
    $erasedAppIds = $this->getErasedAppIds();
    $applications = $this->getApplications();
    $applicationUrl = $this->getApplicationUrl($emp_id);
    $authmstExp = $this->getAuthmstExp($emp_id);

    foreach ($headermenuRows as $row) {
        $appid = $row["menu_id"];

        if ($appid=="pass") {
            if ($authmstExp["emp_pass_flg"]["auth_ok"]) $appid = "pass1";
            else if ($authmstExp["ext_pass_flg"]["auth_ok"]) $appid = "pass2";
            else continue;
        }
        if (in_array($appid, $erasedAppIds, true)) continue; // 利用対象外。スルー
        $info = $applications[$appid];
        if (!$info) continue; // 特製アプリは抹消済。スルー
        if ($info["lcs_ng"]) continue; // ライセンス無し。スルー
        if ($info["auth_flag"] && !$authmstExp[$info["auth_flag"]]["auth_ok"]) continue; // 機能権限なし。スルー

        $urlInfo = $applicationUrl[$appid];
        if (!$urlInfo["url"] && !$urlInfo["onclick"]) continue; // URLもonclickも無い。スルー
        $href = $urlInfo["url"];
        if ($href) $href .= "?session=".$session.$urlInfo["purl"];
        else $href = "javascript:void(0)";
        $onclick = ($urlInfo["onclick"] ? ' onclick="'.c2js($urlInfo["onclick"]).';return false;"' : "");
        $target = ($urlInfo["target"] ? $urlInfo["target"] : "floatingpage");

        $html = array();
        $html[]='<a class="header_icon_a" href="'.$href.'"'.$onclick.' target="'.$target.'"';
        $html[]=' onmouseover="changeIcon(\''.$appid.'\', this);"';
        $html[]=' onmouseout="changeIcon(\''.$appid.'\', this, 1);"';
        $html[]=">";
        $icon41 = $info["icon41"];
        if (!$icon41) $icon41 = "h53";
        $html[]='<div class="div_icon" style="background-image:url(img/icon/'.$icon41.'.gif)"></div>';
        $html[]='<div class="div_icon" style="background-image:url(img/icon/'.$icon41.'o.gif); display:none" id="header_img_bk_'.$appid.'"></div>';

        $html[]='<div class="app_name" id="header_title_'.$appid.'" style="display:none;">'.$info["jp"].'</div></a>';
        $icon_html[$appid] = implode("\n", $html);
    }
    return implode("\n", $icon_html);
}






function getLauncherOrNoLoginPageUrl($emp_id, $wherefrom, $module, $session) {
    $appid = "";
    if ($wherefrom == "1") return "schedule_menu.php?session=".$session;
    else {
        // ランチャーポップアップからの呼び出し
        if ($module=="newsuser") $appid = "newsuser";
        if ($module=="application") $appid = "aprv";
        if ($module=="webmail") $appid = "webml";
        if ($module=="message") $appid = "phone";
        if ($module=="hiyari") $appid = "inci";
        if ($module=="fplus") $appid = "fplus";
        if ($module=="schedule") $appid = "schd";
        if ($module=="shift") $appid = "shift2";
        if ($module=="dutyshift") $appid = "shift";
        if ($module=="baritess_oshirase") $appid = "manabu";
        if ($module=="baritess_mijukou") $appid = "manabu";
    }
    if (!$appid) return "left.php";
    $erasedAppIds = $this->getErasedAppIds();
    if (in_array($appid, $erasedAppIds, true)) return "left.php"; // 利用対象外。マイページへ

    $applications = $this->getApplications();
    $applicationUrl = $this->getApplicationUrl($emp_id);
    $authmstExp = $this->getAuthmstExp($emp_id);
    $info = $applications[$appid];
    $urlInfo = $applicationUrl[$appid];
    $auth_flag = $info["auth_flag"];
    if ($auth_flag && !$authmstExp[$auth_flag]["auth_ok"]) return "left.php"; // 権限がない。マイページへ。

    $href = $urlInfo["url"];
    if ($appid=="schd") $href = "schedule_confirm.php";
    if ($module=="baritess_oshirase") $href = "study/study_notif_user.php";
    if (!$href) $href = "left.php"; // urlが無い。マイページへ
    return $href."?session=".$session.$urlInfo["purl"];
}



// オプション設定⇒表示設定⇒ログイン時の表示ページプルダウン
function getLoginPageList($emp_id, $default_page_appid) {
    $ret = array();
    $erasedAppIds = $this->getErasedAppIds();
    $applications = $this->getApplications();
    $applicationUrl = $this->getApplicationUrl($emp_id);
    $authmstExp = $this->getAuthmstExp($emp_id);
    foreach ($applications as $appid => $dummy) {
        if ($appid=="logout") continue; // ログアウト。不要。
        if ($appid=="openclose") continue; // 不要。
        if (in_array($appid, $erasedAppIds, true)) continue; // 利用対象外。スルー
        // ページに権限があるかどうか
        $info = $applications[$appid];
        $urlInfo = $applicationUrl[$appid];
        if (!$urlInfo["url"]) continue; // URLがない。スルー。オンクリックのみのものは対象にできない。
        $auth_flag = $info["auth_flag"];
        if ($auth_flag && !$authmstExp[$auth_flag]["auth_ok"]) continue;
        $ret[]= '<option value="'.$appid.'"'.($appid==$default_page_appid?" selected":"").'>'.$info["jp"].'</option>';
    }
    return implode("\n", $ret);
    return "";
}



// 職員登録⇒表示グループ⇒ログイン時の表示ページプルダウン
function getLoginGroupList($default_page_appid) {
    $ret = array();
    $authmstExp = $this->getAuthmstExp("");
    $erasedAppIds = $this->getErasedAppIds();
    $applications = $this->getApplications();
    $applicationUrl = $this->getApplicationUrl("");
    foreach ($applications as $appid => $dummy) {
        if (in_array($appid, $erasedAppIds, true)) continue; // 利用対象外。スルー
//        if ($appid=="pass") $appid = "pass1"; // パスワード・本人情報
        $info = $applications[$appid];
        $urlInfo = $applicationUrl[$appid];
        if (!$urlInfo["url"]) continue; // URLがない。スルー。オンクリックのみのものは対象にできない。
        $auth_flag = $info["auth_flag"]; // emp_xxxxの値
        if ($authmstExp[$auth_flag]["lcs_ng"]) continue; // ライセンスがない。スルー
        $ret[]= '<option value="'.$appid.'"'.($appid==$default_page_appid?" selected":"").'>'.$info["jp"].'</option>';
    }
    return implode("\n", $ret);
    return "";
}





//************************************************************************************************************
// トップヘッダに表示するアイコン設定（メニュー／メニューグループ）
//************************************************************************************************************
// 職員登録⇒メニューグループのヘッダメニューの各プルダウン
// 職員登録⇒一括設定⇒メニューリンクの各プルダウン
function outputMenuGroupPageArea($headermenuRow) {
    $headermenuRows = array();
    for ($idx=1; $idx<=$this->_headerIconCount; $idx++) {
        $headermenuRows[]= $headermenuRow["menu_id_".$idx];
    }
    $this->outputMenuPageArea("", $headermenuRows, "GROUP_TYPE");
}

// オプション設定⇒メニュー設定⇒ヘッダメニューの各プルダウン
// 職員登録⇒メニューグループのヘッダメニューの各プルダウン
// 職員登録⇒個人選択⇒メニュー設定のヘッダメニューの各プルダウン
// グループやバルクの場合は、個人別と異なり、全機能に対して権限があり、選択可能であるとする
function outputMenuPageArea($emp_id, $headermenuRows, $disp_place) {
    $out = array();
    $out[]= '<table cellspacing="0" cellpadding="0" id="table_header_menu_selections"><tr>';
    $rowsize = ceil($this->_headerIconCount / 3);
    $applications = $this->getApplications();
    $applicationUrl = $this->getApplicationUrl($emp_id);
    $erasedAppIds = $this->getErasedAppIds();
    $authmstExp = $this->getAuthmstExp($emp_id); // $emp_idはカラかもしれない
    for ($idx2 = 0; $idx2 <= 2; $idx2++) {
        $out[]= '<td class="hader_menu_selections_vert" style="vertical-align:top">';
        $out[]= '<table cellspacing="0" cellpadding="0">';
        for ($idx = 1; $idx <= $rowsize; $idx++) {
            $oid = $idx+($idx2*$rowsize);
            if ($oid > $this->_headerIconCount) break;
            $out[]= '<tr>';
            $out[]=('<th>'.$oid.'</th>');
            $out[]=("<td>");
            $out[]=('<select name="menu_ids_'.$oid.'" id="menu_ids_'.$oid.'" onchange="c2MenuChanged()">');
            $out[]=("<option value=\"-\"></option>");

            $ret = array();
            foreach ($applications as $appid => $dummy) {
                if (in_array($appid, $erasedAppIds, true)) continue; // 利用対象外。スルー
                $info = $applications[$appid];
                $auth_flag = $info["auth_flag"]; // emp_xxxxの値
                if ($authmstExp[$auth_flag]["lcs_ng"]) continue; // ライセンスが無い。スルー
                $infoUrl = $applicationUrl[$appid];
                if (!$infoUrl["url"] && !$infoUrl["onclick"]) continue; // URLもオンクリックも無い。スルー

                if ($disp_place=="EMPLOYEE_TYPE") {
                    if ($auth_flag && !$authmstExp[$auth_flag]["auth_ok"]) continue; // ページに権限がない。スルー
                }
                $ret[]= '<option value="'.$appid.'"'.($appid==$headermenuRows[$oid-1]?" selected":"").'>'.$info["jp"].'</option>';
            }
            $out[]= implode("\n", $ret);
            $out[]= "</select>";
            $out[]= '</td>';
            $out[]= '</tr>';
        }
        $out[]= '</table>';
        $out[]= '</td>';
    }
    $out[]= '</tr></table>';

    // 重複選択肢は色を変える処理
    $out[]= '<script type="text/javascript">';
    $out[]= '    var c2_menu_selecting = [];';
    $out[]= '    function c2MenuChanged() {';
    $out[]= '        var optlen = ee("menu_ids_1").options.length;';
    $out[]= '        for (var idx=0; idx<=optlen; idx++) c2_menu_selecting[idx] = 0;';
    $out[]= '        for (var idx=1; idx<='.$this->_headerIconCount.'; idx++) {';
    $out[]= '            var sIdx = ee("menu_ids_"+idx).selectedIndex;';
    $out[]= '            if (!c2_menu_selecting[sIdx]) c2_menu_selecting[sIdx] = 0;';
    $out[]= '            c2_menu_selecting[sIdx]++;';
    $out[]= '        }';
    $out[]= '        for (var idx=1; idx<='.$this->_headerIconCount.'; idx++) {';
    $out[]= '            var cmb = ee("menu_ids_"+idx);';
    $out[]= '            for (var sidx=0; sidx<optlen; sidx++) {';
    $out[]= '                cmb.options[sidx].style.color = (c2_menu_selecting[sidx] ? "#aaaaaa" : "#000000");';
    $out[]= '            }';
    $out[]= '            cmb.style.color = (c2_menu_selecting[cmb.selectedIndex]>1 ? "#ff0000" : "");';
    $out[]= '        }';
    $out[]= '    }';
    $out[]= '</script>';
    echo implode("\n", $out);
}







function getLoginDefaultPageUrl($emp_id, $session) {
    $optionRow = $this->getOptionRow($emp_id);
    $appid = $optionRow["default_page"];
    $authmstExp = $this->getAuthmstExp($emp_id);
    if ($appid=="pass1" || $appid=="pass2") {
        if ($authmstExp["emp_pass_flg"]["auth_ok"]) $appid = "pass1";
        else if ($authmstExp["ext_pass_flg"]["auth_ok"]) $appid = "pass2";
        else $appid = "";
    }

    if (!$appid) return "left.php";
    $erasedAppIds = $this->getErasedAppIds();
    if (in_array($appid, $erasedAppIds, true)) return "left.php"; // 利用対象外。マイページへ

    $applications = $this->getApplications();
    $applicationUrl = $this->getApplicationUrl($emp_id);
    $info = $applications[$appid];
    $urlInfo = $applicationUrl[$appid];
    $auth_flag = $info["auth_flag"];
    if ($auth_flag && !$authmstExp[$auth_flag]["auth_ok"]) return "left.php"; // 権限がない。マイページへ
    $href = $urlInfo["url"];
    if ($appid=="pass2") $href = "passwd_belong_change.php";
    //if ($appid=="cmt"  ) $href = "community_menu.php";
    if (!$href) $href = "left.php"; // URLが無い。マイページへ
    return $href."?session=".$session.$urlInfo["purl"];
}






// ライセンス管理⇒ライセンス登録⇒登録処理
// 登録成功時は"ok"を返す。失敗時はエラーメッセージを返す。
// c2ff関数はupdateLicenseCounts()専用の内部関数。
function _ff($input_user_count) {
    if ($input_user_count=="FULL") return 100000; // 比較のための最大値として、適当に十万を返すことに
    return $input_user_count; // そうでないならそのままの値を返す
}
function _isOver($input_user_count, $max_user_count) {
    if ($input_user_count=="FULL") return false;
    if ($max_user_count=="FULL") return false;
    if ($input_user_count > $max_user_count) return true;
    return false;
}
// call_from : license_menu.php or config_other_update_exe.php（環境設定⇒その他）
function checkLicenseKey($post, $input_license_key, $isKeyCheckOnly=0) {
    $gen_license_key = "";
    // 保健医療機関コード
    $profileRow = $this->getProfileRow();
    if (!$profileRow) return "組織プロフィールの契約番号が未登録のため、ライセンス情報を更新できません。";
    $post["prf_org_cd"] = $profileRow["prf_org_cd"];
    if (!$post["prf_org_cd"]) return "組織プロフィールの契約番号が未登録のため、ライセンス情報を更新できません。";
    return $this->_checkOrGenLicenseKey($post, $input_license_key, $isKeyCheckOnly, $gen_license_key);
}
// いささか苦肉の策、いつか正常化させたい
// isKeyCheckOnlyは、画面でパラメータを指定するタイプ。ライセンス管理画面が該当
// そうでない場合は、現在、config_other_update_exe.phpの場合のみ。
function genLicenseKey($post, $isKeyCheckOnly=0) {
    if ($isKeyCheckOnly) {
        $keys = array_keys($post);
        foreach ($keys as $key) {
            if (preg_match("/^lcs_func[0-9]+$/", $key)) unset($post[$key]);
        }
    }
    $gen_license_key = "";
    $errmsg = $this->_checkOrGenLicenseKey($post, "", $isKeyCheckOnly, $gen_license_key);
    if ($gen_license_key) $errmsg = "";
    return array("errmsg"=>$errmsg, "gen_license_key"=>$gen_license_key);
}
function _checkOrGenLicenseKey($post, $input_license_key, $isKeyCheckOnly=0, &$gen_license_key) {
    $ff_lcs_max_user = $this->_ff($post["lcs_max_user"]);

    // 保健医療機関コード
    if (!$post["prf_org_cd"]) return "医療機関コード／契約番号が指定されていません。";
    if (!preg_match("/^[0-9]{10}$/", $post["prf_org_cd"])) return "医療機関コード／契約番号が半角数字10桁で指定されていません。";

    // 入力チェック（ライセンス登録時。ログその他機能からの更新ではチェック不要）
    // 未来機能、特製機能については、チェックしない。カラ登録を行おうとしても
    // どのみち会社が発行したライセンスとは不整合となるだろうから。
    if (!$isKeyCheckOnly) { // 呼出し画面が、「license_menu.php」でないのに「license_menu.php」を指定している箇所あり。
        $msg_is_empty = "利用可能ユーザ数が選択されていません。";
        $msg_is_over = "利用可能ユーザ数が登録可能ユーザ数を超えています。";
        $msg_is_empty2 = "対象者職員数が選択されていません。";
        $msg_is_over2 = "対象者職員数が登録可能ユーザ数を超えています。";
        $lcsLabels = $this->getLcsLabels();
        if ($post["lcs_basic_user"]=="")            return $lcsLabels["lcs_basic_user"].$msg_is_empty;
        if ($this->_isOver($post["lcs_basic_user"], $ff_lcs_max_user))    return $lcsLabels["lcs_basic_user"].$msg_is_over;
        if ($post["lcs_intra_user"]=="")            return $lcsLabels["lcs_intra_user"].$msg_is_empty;
        if ($post["lcs_nursing_user"]=="")          return $lcsLabels["lcs_nursing_user"].$msg_is_empty;
        if ($post["lcs_bed_count"]=="")             return $lcsLabels["lcs_bed_count"]."が選択されていません。";
        if ($post["lcs_fantol_user"]=="")           return $lcsLabels["lcs_fantol_user"].$msg_is_empty;
        if ($this->_isOver($post["lcs_fantol_user"], $ff_lcs_max_user))   return $lcsLabels["lcs_fantol_user"].$msg_is_over;
        if ($post["lcs_fplus_user"]=="")            return $lcsLabels["lcs_fplus_user"].$msg_is_empty;
        if ($this->_isOver($post["lcs_fplus_user"], $ff_lcs_max_user))    return $lcsLabels["lcs_fplus_user"].$msg_is_over;
        if ($post["lcs_baritess_user"]=="")         return $lcsLabels["lcs_baritess_user"].$msg_is_empty;
        if ($this->_isOver($post["lcs_baritess_user"], $ff_lcs_max_user)) return $lcsLabels["lcs_baritess_user"].$msg_is_over;
        if ($post["lcs_report_user"]=="")           return $lcsLabels["lcs_report_user"].$msg_is_empty;
        if ($this->_isOver($post["lcs_report_user"], $ff_lcs_max_user))   return $lcsLabels["lcs_report_user"].$msg_is_over;
        if ($post["lcs_shift_emps"]=="")            return $lcsLabels["lcs_shift_emps"].$msg_is_empty2;
        if ($this->_isOver($post["lcs_shift_emps"], $ff_lcs_max_user))    return $lcsLabels["lcs_shift_emps"].$msg_is_over2;
        if ($post["lcs_ladder_user"]=="")           return $lcsLabels["lcs_ladder_user"].$msg_is_empty;
        if ($this->_isOver($post["lcs_ladder_user"], $ff_lcs_max_user))   return $lcsLabels["lcs_ladder_user"].$msg_is_over;
        if ($post["lcs_career_user"]=="")           return $lcsLabels["lcs_career_user"].$msg_is_empty;
        if ($this->_isOver($post["lcs_career_user"], $ff_lcs_max_user))   return $lcsLabels["lcs_career_user"].$msg_is_over;
        if ($post["lcs_jinji_emps"]=="")            return $lcsLabels["lcs_jinji_emps"].$msg_is_empty2;
        if ($this->_isOver($post["lcs_jinji_emps"], $ff_lcs_max_user))    return $lcsLabels["lcs_jinji_emps"].$msg_is_over2;
        if ($post["lcs_jnl_user"]=="")              return $lcsLabels["lcs_jnl_user"].$msg_is_empty;
        if ($this->_isOver($post["lcs_jnl_user"], $ff_lcs_max_user))      return $lcsLabels["lcs_jnl_user"].$msg_is_over;
        $lcs_expire_date = "";
        $expY = $post["expire_year"];
        $expM = $post["expire_month"];
        $expD = $post["expire_day"];
        if (c2bool($post["lcs_expire_flg"])) {

            if ($expY!="-" && $expM!="-" && $expD!="-" && checkdate($expM, $expD, $expY)) {
                $lcs_expire_date = $expY.$expM.$expD;
            }
            if (!$lcs_expire_date) return "利用期限の日付が不正です。";
        }
        $post["lcs_expire_date"] = $lcs_expire_date;
    }
    $ym = $post["today_year"].$post["today_month"];
    if (strlen($ym)!=6) return "更新年月の値が不正です。";

    // 入力値よりライセンスキー番号を生成してみる
    $str = $post["prf_org_cd"];
    $str .=$ym;
    $str .=$post["lcs_max_user"];
    $str .=$post["lcs_basic_user"];
    $str .=$post["lcs_intra_user"];
    $str .=$post["lcs_fantol_user"];
    $str .=$post["lcs_report_user"];
    $str .= $post["lcs_expire_flg"];
    $str .= $post["lcs_expire_date"];
    // チェックをつけられたものだけ、func番号２桁をつなげる。Core2になる手前20まで。
    // ただし、照合は旧画面での並び順。照合順は重要。
    // 具体的には、1から19までは、4, 7, 1, 15, 2, 10, 5, 13, 12, 6, 11, 9, 18, 8, 16, 17, 19, 14, 3 である。
    $licenseIdToAppId = $this->getLicenseIdToAppId();
    foreach ($licenseIdToAppId as $lcs_func_no => $appid) {
        if ($lcs_func_no > 19) continue; // 19まで。
        if ($post["lcs_func".$lcs_func_no]) $str .= sprintf("%02d", $lcs_func_no);
    }
    // 値があれば、利用数をつなげてゆく。
    if ($this->_ff($post["lcs_shift_emps"]) > 0)    $str .= $post["lcs_shift_emps"];
    if ($this->_ff($post["lcs_fplus_user"]) > 0)    $str .= $post["lcs_fplus_user"];
    if ($this->_ff($post["lcs_jnl_user"]) > 0)      $str .= $post["lcs_jnl_user"];
    if ($this->_ff($post["lcs_bed_count"]) > 0)     $str .= $post["lcs_bed_count"];
    if ($this->_ff($post["lcs_jinji_emps"]) > 0)    $str .= $post["lcs_jinji_emps"];
    if ($this->_ff($post["lcs_ladder_user"]) > 0)   $str .= $post["lcs_ladder_user"];
    if ($this->_ff($post["lcs_career_user"]) > 0)   $str .= $post["lcs_career_user"];
    if ($this->_ff($post["lcs_nursing_user"]) > 0)  $str .= $post["lcs_nursing_user"];
    if ($this->_ff($post["lcs_baritess_user"]) > 0) $str .= $post["lcs_baritess_user"];
    // 新機能を足す。ずれるので数字だけ足すのはやめる。以下の書式で足すこと。
    // ◆◆◆   "セミコロン" + [dbのライセンスフラグフィールド名] + "イコール" + 数(FULLという文字列が入る場合あり）
    for ($idx=1; $idx<=15; $idx++) if ($this->_ff($post["lcs_ccusr".$idx."_user"]) > 0)  $str .= ";lcs_ccusr".$idx."_user=".$post["lcs_ccusr".$idx."_user"];
    for ($idx=1; $idx<=5; $idx++)  if ($this->_ff($post["lcs_omusr".$idx."_user"]) > 0) $str .= ";lcs_omusr".$idx."_user=".$post["lcs_omusr".$idx."_user"];
    // 「購入する業務機能」チェックボックスの値（あるいは既に購入済ならhidden値）。21以上。これもも文字列で足す。
    // ◆◆◆   "セミコロン" + "lcs_func" + [dbのライセンスフラグフィールド番号
    // 新アプリからはチェックはハッシュ生成条件に含めない。
//    for ($idx=21; $idx<=100; $idx++) if ($post["lcs_func".$idx]) $str .= ";lcs_func".$idx;

    // ★ これ以降、別名でアプリを追加する場合は、数字でなくユニークとなる文字列を追加するなら、順不同で追加してよい。どこに追加してもよい。
    // ★ ただしライセンス未取得の場合に、既存ライセンスコードが変化するのはNGである。
    // ★ チェックボックスオフ時や、ユーザ数が無い場合に、文字列が変化しないように。

    // 生成した値
    $gen_license_key = substr(md5($str), 0, 10);
//$gen_license_key = $str;
    // 画面の入力値と比較する
    if ($input_license_key != $gen_license_key) return "ライセンスキー番号が正しくありません。";
    return ""; // 成功時は空文字を返す
}
// ライセンス管理⇒ライセンス登録⇒登録処理からのみ呼ばれる
// 登録成功時は"ok"を返す。失敗時はエラーメッセージを返す。
function updateLicenseCounts($post) {
    $lcs_expire_date = "";
    $expY = $post["expire_year"];
    $expM = $post["expire_month"];
    $expD = $post["expire_day"];
    if (c2bool($post["lcs_expire_flg"])) {
        if ($expY!="-" && $expM!="-" && $expD!="-" && checkdate($expM, $expD, $expY)) {
            $lcs_expire_date = $expY.$expM.$expD;
        }
    }
    $post["lcs_expire_date"] = $lcs_expire_date;
    $errmsg = $this->checkLicenseKey($post, $post["license_key"]); // ★★ライセンスキーチェック
    if ($errmsg) return $errmsg;

    // ライセンステーブルの更新
    $sql_array = array(
        " update license set",
        " lcs_max_user= ".c2dbStr($post["lcs_max_user"]),
        ",lcs_basic_user= ".c2dbStr($post["lcs_basic_user"]),
        ",lcs_intra_user= ".c2dbStr($post["lcs_intra_user"]),
        ",lcs_fantol_user= ".c2dbStr($post["lcs_fantol_user"]),
        ",lcs_report_user= ".c2dbStr($post["lcs_report_user"]),
        ",lcs_expire_flg= ".c2dbBool($post["lcs_expire_flg"]),
        ",lcs_expire_date= ".c2dbStr($lcs_expire_date),
        ",lcs_shift_emps= ".c2dbStr($post["lcs_shift_emps"]),
        ",lcs_fplus_user= ".c2dbStr($post["lcs_fplus_user"]),
        ",lcs_jnl_user= ".c2dbStr($post["lcs_jnl_user"]),
        ",lcs_bed_count= ".c2dbStr($post["lcs_bed_count"]),
        ",lcs_jinji_emps= ".c2dbStr($post["lcs_jinji_emps"]),
        ",lcs_ladder_user= ".c2dbStr($post["lcs_ladder_user"]),
        ",lcs_career_user= ".c2dbStr($post["lcs_career_user"]),
        ",lcs_nursing_user= ".c2dbStr($post["lcs_nursing_user"]),
        ",lcs_baritess_user= ".c2dbStr($post["lcs_baritess_user"])
    );
    // ライセンス追加のみ。
    // ちなみに、ライセンスは一回登録したら、消すことはできない。
    for ($idx=1; $idx<=20; $idx++) {
        if ($post["lcs_func".$idx]) $sql_array[]= ",lcs_func".$idx." = true";
    }
    for ($idx=1; $idx<=15; $idx++) $sql_array[]= ",lcs_ccusr".$idx."_user=".c2dbStr($post["lcs_ccusr".$idx."_user"]);
    for ($idx=1; $idx<=5; $idx++)  $sql_array[]= ",lcs_omusr".$idx."_user=".c2dbStr($post["lcs_omusr".$idx."_user"]);
    for ($idx=1; $idx<=15; $idx++) $sql_array[]= ",lcs_ccadm".$idx."_user=".c2dbStr($post["lcs_ccadm".$idx."_user"]);
    for ($idx=1; $idx<=5; $idx++)  $sql_array[]= ",lcs_omadm".$idx."_user=".c2dbStr($post["lcs_omadm".$idx."_user"]);
    // くどいようだが、ライセンスは一回登録したら、消すことはできない。
    for ($idx=21; $idx<=100; $idx++) if ($post["lcs_func".$idx]) $sql_array[]= ",lcs_func".$idx." =  true";

    c2dbExec(implode("\n", $sql_array));
    return "ok";
}

// ライセンス別登録従業員数
// employee_auth_list.php
// employee_auth_common.php
// 上記ファイルemployee_auth_common.phpは、以下などから利用
// ・employee_attribute_bulk_insert4.php
// ・employee_auth_common.php
// ・employee_bulk_insert2.php
// ・employee_bulk_insert3.php
// ・employee_bulk_insert5.php
// また、これらの処理中は、呼出し側で登録トランザクション中であることに注意
function getEmpCountPerLicense() {
    // 機能別ライセンス付与数
    $licenseRow = $this->getLicenseRow();
    $license_max = array();
    $lcsLabels = $this->getLcsLabels();
    foreach ($lcsLabels as $field => $jp) {
        $license_max[$field] = $licenseRow[$field];
        if ($license_max[$field]=="0") $license_max[$field] = "";
        if ($license_max[$field]=="FULL") $license_max[$field] = "無制限";
    }
    $license_max["unlimited"] = "無制限";

    $arr_func = array();
    for ($idx = 1; $idx <= 19; $idx++) {
        $arr_func[$idx] = $licenseRow["lcs_func".$idx];
    }

    $authmst_res = c2dbGetResult("select * from authmst where emp_del_flg='f'");
    $emp_auth_counter = array();
    $authmstExp = $this->getAuthmstExp("");
    $emp_count = 0;
    foreach ($authmstExp as $field => $info) {
        $emp_count++;
        $emp_auth_counter[$field] = 0;
        if ($info["lcs_max"] && $info["lcs_max"]!="無制限") $lcs_max[$info["lcs_max"]] = 0;
    }
    $kihonkinou_list = $this->getAuthmstKihonkinouList();
    while ($row = c2dbGetNext($authmst_res)) {
        $basic_field_exist = 0;
        foreach ($authmstExp as $field => $info) {
            if ($row[$field]!="t") continue;
            $emp_auth_counter[$field] += 1;
            if (in_array($field, $kihonkinou_list)) $basic_field_exist = 1;
            if ($info["lcs_max"] && $info["lcs_max"]!="lcs_basic_user") $lcs_max[$info["lcs_max"]]++;
        }
        if ($basic_field_exist) $lcs_max["lcs_basic_user"]++;
    }
    // 基本機能全体累計の権限付与数。基本機能ライセンス数を上回れない。
    $lcs_max["lcs_basic_user"] = min($lcs_max["lcs_basic_user"], $licenseRow["lcs_basic_user"]);

    // 基本機能のそれぞれの権限付与数。基本機能全体の付与数を上回れない。
    $field_emp_counter = array();
    $overflow_fields = array();
    if ($licenseRow["lcs_max_user"]!="FULL" && $licenseRow["lcs_max_user"] < $emp_count) { // 登録可能ユーザ数より全従業員数が少ない
        $overflow_fields[]= array("field"=>"lcs_max_user", "jp"=>"登録ユーザ数"); // 「jp」が上限を超えてしまうため、実行できません
    }
    foreach ($authmstExp as $field => $info) {
        $lcs_max_field = $info["lcs_max"];
        if ($lcs_max_field && $lcs_max_field!="") {
            if ($lcs_max_field!="unlimited" && $lcs_max[$lcs_max_field] < $emp_auth_counter[$field]) {
                $overflow_fields[] = array("field"=>$field, "jp"=>$lcsLabels[$lcs_max_field]."利用ユーザ数"); // 「jp」が上限を超えてしまうため、実行できません
            }
            $field_emp_counter[$field]  = min($lcs_max[$lcs_max_field], $emp_auth_counter[$field]);
        }
    }
    // 勤務シフトの場合の職員数
    $field_emp_counter["emp_shift_flg"] = c2dbGetOne("select count(*) from duty_shift_staff");
    $field_emp_counter["emp_shift2_flg"] = $field_emp_counter["emp_shift_flg"];

    return array($field_emp_counter, $lcs_max, $authmstExp, $license_max, $overflow_fields);
}

// 権限グループ登録時のみチェック employee_auth_group.php
function checkAuthGroupRegist($row) {
    if ($row["emp_libadm_flg"] == "t" && $row["emp_lib_flg"] != "t" && $row["emp_intra_flg"] != "t") {
        return "以下の組み合わせでは登録できません。\n文書管理（管理者）権限あり、文書管理（基本）・イントラネット権限なし";
    }
    if ($row["emp_extadm_flg"] == "t" && $row["emp_ext_flg"] != "t" && $row["emp_intra_flg"] != "t") {
        return "以下の組み合わせでは登録できません。\n内線電話帳管理権限あり、内線電話帳・イントラネット権限なし";
    }
    if ($row["emp_entity_flg"]=="t" && $row["emp_ptif_flg"]!="t" && $row["emp_inout_flg"]!="t" &&
        $row["emp_dishist_flg"]!="t" && $row["emp_ward_flg"]!="t" && $row["emp_med_flg"]!="t") {
        return "以下の組み合わせでは登録できません。\n診療科権限あり、患者基本情報・入院来院履歴・プロブレム・病床管理・メドレポート権限なし";
    }
    if ($row["emp_ptadm_flg"] == "t" && $row["emp_ptif_flg"] != "t" && $row["emp_inout_flg"] != "t" && $row["emp_dishist_flg"] != "t") {
        return "以下の組み合わせでは登録できません。\n患者管理管理者権限あり、患者基本情報・入院来院履歴・プロブレム権限なし";
    }
}


function authCheckConsistency($emp_id) {
    // ログインユーザ自身の職員登録権限がなくなった場合はエラーとする
    if (c2dbGetOne("select emp_reg_flg from authmst where emp_id = ".c2dbStr($emp_id)) == "f") {
        return "自身の職員登録権限がなくなってしまうため、処理をキャンセルしました。";
    }

    // 文書管理権限の整合性をチェック
    $sql = "select count(*) from authmst where emp_libadm_flg = 't' and emp_lib_flg = 'f' and emp_intra_flg = 'f'";
    if ((int)c2dbGetOne($sql) > 0) {
        return "次の職員ができてしまうため、処理をキャンセルしました。\n文書管理（管理者）権限あり、文書管理（基本）・イントラネット権限なし";
    }

    // 内線電話帳権限の整合性をチェック
    $sql = "select count(*) from authmst where emp_extadm_flg = 't' and emp_ext_flg = 'f' and emp_intra_flg = 'f'";
    if ((int)c2dbGetOne($sql) > 0) {
        return "次の職員ができてしまうため、処理をキャンセルしました。\n内線電話帳管理権限あり、内線電話帳・イントラネット権限なし";
    }

    // 診療科権限の整合性をチェック
    $sql =
    " select count(*) from authmst".
    " where emp_entity_flg = 't' and emp_ptif_flg = 'f' and emp_inout_flg = 'f' and emp_dishist_flg = 'f' and emp_ward_flg = 'f' and emp_med_flg = 'f'";
    if ((int)c2dbGetOne($sql) > 0) {
        return "次の職員ができてしまうため、処理をキャンセルしました。\n診療科権限あり、患者基本情報・入院来院履歴・プロブレム・病床管理・メドレポート権限なし";
    }

    // 患者管理管理者権限の整合性をチェック
    $sql = "select count(*) from authmst where emp_ptadm_flg = 't' and emp_ptif_flg = 'f' and emp_inout_flg = 'f' and emp_dishist_flg = 'f'";
    if ((int)c2dbGetOne($sql) > 0) {
        return "次の職員ができてしまうため、処理をキャンセルしました。\n患者管理管理者権限あり、患者基本情報・入院来院履歴・プロブレム権限なし";
    }

    // 職員登録権限のある職員がいなくなってしまう場合はエラーとする
    $sql = "select count(*) from authmst where emp_reg_flg = 't' and emp_del_flg = 'f'";
    if ((int)c2dbGetOne($sql) == 0) {
        return "職員登録権限のある職員がいなくなってしまうため、処理できません。";
    }

    // ライセンスのチェック
    list($field_emp_counter, $lcs_max, $authmstExp, $license_max, $overflow_fields) = $this->getEmpCountPerLicense();
    $error_1st = $overflow_fields[0];
    if ($error_1st) {
        return $error_1st["jp"]."が上限を超えてしまうため、実行できません。";
    }
    return ""; // 問題なし。カラを返す
}





// オプション設定⇒表示設定の更新 option/o_regist.php
// employee_display_setting.php
function updateOption($request, $emp_id) {
    // オプション情報が存在しなければ作成
    if (!c2dbGetOne("select count(*) from option where emp_id = ".c2dbStr($emp_id))) {
        c2dbExec("insert into option (emp_id) values (".c2dbStr($emp_id).")");
    }

    $sql1 = "";
    $fieldNames = $this->getMypageContentsFieldNames();
    foreach ($fieldNames as $field) {
        $sql1 .= ",".$field." = ".c2dbBool($request["chk_".$field]);
        if ($field=="top_msg_flg") $sql1 .= ",top_aprv_flg = ".c2dbBool($request["chk_".$field]); //top_msg_flgとtop_aprv_flgは同じにする
    }
    $sql =
    " update option set".
    " mypage_layout_info=".c2dbStr($request["mypage_layout_info"]).
    ",default_page=".c2dbStr($request["default_page"]).
    ",logo_type='1'". // Flashロゴ設定のなごり
    ",schd_type=".c2dbBool($request["schd_type"]).
    ",font_size=".c2dbStr($request["font_size"]).
    $sql1.
    " where emp_id = ".c2dbStr($emp_id);
    c2dbExec($sql);
    return "ok";
}
// employee_display_bulk_setting.php
function updateOptionBulk($request) {
    $disp_group_id = $_REQUEST["disp_group_id"];
    // 「表示グループで設定」の場合は、選択グループデータの複製データを、各従業員に
    if ($action_mode=="1") {
        $drow = $this->getDispgroupRow($disp_group_id);
    }
    // 「個別に設定」の場合
    else {
        $sql1 = "";
        $fieldNames = $this->getMypageContentsFieldNames();
        foreach ($fieldNames as $field) {
            $drow[$field] = $_REQUEST["chk_".$field];
            if ($field=="top_msg_flg") $drow["top_aprv_flg"] = $_REQUEST["chk_".$field]; //top_msg_flgとtop_aprv_flgは同じにする
        }
    }

    c2dbBeginTrans(); // トランザクションを開始
    foreach ($_REQUEST["tgt_job"] as $tmp_job) {
        foreach ($_REQUEST["tgt_st"] as $tmp_st) {
            $sql_array = array();
            $sql_array[]= "update option set";
            $sql_array[]= "mypage_layout_info = ".c2dbStr($_REQUEST["mypage_layout_info"]);
            $sql_array[]= ",default_page = ".c2dbStr($_REQUEST["default_page"]);
            $sql_array[]= ",font_size = ".c2dbStr($_REQUEST["font_size"]);
            $sql_array[]= ",schd_type = ".c2dbBool($_REQUEST["schd_type"]);
            $sql_array[]= ",disp_group_id = ".c2dbInt($disp_group_id);

            $field_sql = array();
            foreach ($drow as $field => $v) {
                $sql_array[]= ",".$field." = ".c2dbBool($v);
            }
            $sql_array[] =
            " where exists (".
            "     select * from empmst".
            "     where empmst.emp_id = option.emp_id".
            "     and empmst.emp_job = ".c2dbInt($tmp_job).
            "     and empmst.emp_st = ".c2dbInt($tmp_st).
            " )".
            " and exists (".
            "     select * from authmst".
            "     where authmst.emp_id = option.emp_id".
            "     and authmst.emp_del_flg = 'f'".
            " )";
            c2dbExec(implode("\n", $sql_array));
        }
    }
    c2dbCommit();
    return "ok";
}

// group単位optionテーブル。
// employee_display_group.php
function updateDispGroup($group_id, $request, $group_nm) {
    $sql1 = "";
    $fieldNames = $this->getMypageContentsFieldNames();
    foreach ($fieldNames as $field) {
        $sql1 .= ",".$field." = ".c2dbBool($request["chk_".$field]);
        if ($field=="top_msg_flg") {
            $sql1 .= ",top_aprv_flg = ".c2dbBool($request["chk_".$field]);
        }
    }
    $sql =
    " update dispgroup set".
    " mypage_layout_info=".c2dbStr($request["mypage_layout_info"]).
    ",default_page=".c2dbStr($request["default_page"]).
    ",schd_type=".c2dbBool($request["schd_type"]).
    ",font_size=".c2dbStr($request["font_size"]).
    ",group_nm=".c2dbStr($group_nm).
    $sql1.
    " where group_id = ".c2dbInt($group_id);
    c2dbExec($sql);
    return "ok";
}




} // end class $c2app

