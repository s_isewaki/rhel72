<?
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// C2FW.php (Core2 Framework)
//
// ★ アプリケーションがリクエストを受けて、
// ★ HTML等のレスポンスを作成し、終了するまでの
// ★ 一連の挙動を定式化するもの
//
// 実際のビジネスアプリケーションを量産するにあたり、
// 現在の盛り込みでは不十分か、不適切であることも、十分予想される。
// 柔軟な拡張を想定ください。
//
// また、PHP4ベースであるため、大枠、こんなところまでであろうと思われる。
//
// C2FWはサーバ側ロジックの集大成的な位置づけにあるが、
// C2FWはプログラムの流れを制御する役割のユーティリティに過ぎず、
// 結果としてすべてのC2系ファイルを利用することにはなっているものの、
// フレームワークの流れが不要な場合や、とある機能のみ利用したい場合は、C2FWをrequireする必要は無い。
// 各C2系ファイルは、単独requireして利用できるようになっている。
// Pear.phpのように、とりあえずrequireするものでは無い。
//
//
// ■目的
//   このフレームワークの存在は、プログラム構造の勉強という要素要望が強いため、
//   いくつかのフレームワークパターンの披露と功罪を検証できる素地を持たせている。
//
//   将来的には、このフレームワークをベースに、CoMedix再構築という想定もあるが、
//   それに耐えうるようにも想定されている。
//
//   構築においては、以下の抽象概念を、物事の基本であり真理であるとしている。
//
//   ◆本来の目的は、期限までの要求仕様の実装に過ぎない。
//   ◆地球上の全てのシステムは、つじつまあわせ（妥協）に過ぎない。
//     つじつまに対するベストな説明を追求しようとしているに過ぎない。
//   ◆なんらかの行動や実装にメリットがあっても、デメリットも必ず存在する。
//     ひとつもデメリットを言及できない場合、
//     そのメリットによる破綻を想定できていないため想定不十分であり、危険性がある。
//   ◆人間の思考は、大局的には近似しているが、局所的には相容れることは無い。
//     まだ見ぬ作業者に対するまだ見ぬシステムへの万能な対策は、地球上に存在しない。
//     また、複数人の理想への感覚の追求の結果は、究極的には具体化させることはできない。
//   ◆頭を使わない、最高のアイデアは存在しない。
//     素晴らしいひらめきや思いつきも、現実的矛盾が解消されている保障が原理的に無く、
//     必ず熟慮作業を通す必要がある。
//   ◆フレームワークや規約を遵守し、維持することと、
//     優れたシステムを製造・維持することは、全く異なる。
//   ◆思想や意見発言は無論、数論的にも物理的事情にも、
//     採用・不採用のファクタが必ず入ることになるため、
//     地球上のすべてのアクションには必ず感情的意図や感情的発想が含まれている。
//     感情的な部分を見出せないアクションは、のちのちの大きな問題に発展する可能性に、
//     気づいていないということである。
//
//   なお、このフレームワークがそうであるように、
//   種々の手製フレームワークパターンが、既にCoMedix内にて活躍中である。
//   ・人事管理      ：リクエスト集約コントローラを儲け、サブティレクトリへプログラム配布
//   ・キャリアラダー：Cmxを正しく活用した例
//   ・バリテス      ：MVC＋Smartyベース
//   ・その他いろいろ
//
//   言い換えれば、about_comedixベースであろうが、Cmxベースであろうが、
//   フレームワークという概念への意識は、さほど重要では無い。
//   コーディングを揃えて生産性とメンテ性が向上できるか否かで、
//   どういう風に書くべきかが決まり、それがフレームワークなだけであるので、
//   決して「フレームワーク主義」になってはいけない。
//
//
// ■特筆機能
// ・URLアクションコントローラ準備あり（index.phpを設け、action=xxx指定でファイルを切り分ける形式）
// ・MVC準備あり (ファイル分割を行うのが目的の場合が多い。実装するイベントをファイルごとにすれば達成される)
// ・イベントドリブン利用可能
// ・独自テンプレートエンジン搭載、Smartyも選択可能（SmartyはCmx_Viewを利用）
//
// ■スコープ規則（重要）
//   以下のC2系は、必ずglobalスコープに存在する。
//   $c2fw, $c2app, $c2db
//
//
// ■命名規則、スコープ規則
//   インスタンスC2FWは、グローバル
//     C2Framework関連ファイルでは、
//     ・DEFINE名は、必ず「C2_」で開始される
//     ・関数名は、以下汎用関数を除き、「c2」で開始される
//
// ■require_once("../class/Cmx/Core2/Core2FW.php");などとして利用
//   カレントディレクトをいじるので、DEFINEがなければURL呼び出しから極力真っ先に呼ぶこと。
// ■リクワイア直後には、以下を実行する。
//   1) もし必要ならカレントディレクトリの調整。adjustCurrentDir()参照。
//   2) ＤＢ接続トライ。認証があるのでDBアクセスが発生しないページはほぼ皆無。URLリクエストがあれば常にDB接続確立させてよい、とする。
//
// ■フレームワークでは、上記の２点のほか、その他機能はすべて「オプション」である。
//   まったくいままで通りのコーディングを行ってもよい。
//   フレームワークを利用すると、以下の恩恵がある。
//
// ■ログのための$fnameのたらい回しを廃止
// ■about_comedix系は一切見ていないので排除可能（必要なら併用可能）
// ■CMX系も排除可能。Cmx.phpはDB定義インクルードのために参照。ただしCmx.php自体がリクワイアするファイル参照していない（必要なら併用可能）
// ■CORE2で認証を行うと、セッション変数、ログイン者はDEFINE化されるため、以下として取得可能。いちいちたらい回しする必要なし
//     C2_SESSION_ID
//     C2_LOGIN_EMP_ID
//     C2_LOGIN_EMP_LT_NM
//     C2_LOGIN_EMP_FT_NM
//
//
//■
//  １）URLにsession不要
//
//
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――


//======================================
// 開発モード
// SQLエラーでエラー画面に強制送還せずに停止させたい場合に、活性化すること。デバッガつきIDE利用者には不要かも
//======================================
define("C2_DEV_MODE", 1);


if (!defined("C2_AJAX_ACTION_NAME")) define("C2_AJAX_ACTION_NAME", "ajax_action");

//======================================
// 内部リビジョン。整数形式で。
// 将来バージョン切り分けによるルーチン切り分けが発生するかもしれない。
// とりわけ何か挙動切り分けが必要な変更があれば、ここに、バージョン番号と施行日をログる
// rev 施行      内容
//--------------------------------------
// 1   201501xx  発足
//======================================
define("C2_FRAMEWORK_REVISION", 1);
define("C2_TPL_ENGINE_INSTANT", "C2_TPL_ENGINE_INSTANT");
define("C2_TPL_ENGINE_SMARTY",  "C2_TPL_ENGINE_SMARTY");



require(dirname(dirname(dirname(dirname(__FILE__))))."/conf/conf.inf");
require(dirname(dirname(dirname(dirname(__FILE__))))."/conf/conf_define.inf");
require_once(dirname(__FILE__)."/C2Utils.php"); // 汎用関数群
require_once(dirname(__FILE__)."/C2Env.php"); // CoMedixシステムならではの汎用関数群
c2env::adjustCurrentDir(); // ★★★必要ならカレントディレクトリ変更
require_once(dirname(__FILE__)."/C2App.php"); // アプリ名管理、アプリURL管理、アプリ権限、アプリライセンス等
require_once(dirname(__FILE__)."/C2DB.php"); // DBアクセッサ
require_once(dirname(__FILE__)."/C2Login.php"); // ログインチェック
//require_once("Cmx.php"); // DB接続定義、conf.inf等の読込みのために必要。

//require_once("conf/error.inf")
//require_once("acls_set.php")


if ($_REQUEST["ajax_action"]) {
    error_reporting(6135);
    ini_set("display_errors", 1);
    ini_set("display_startup_errors", 1);
}





//**************************************************************************************************
// 簡易イベントドリブン
// この関数定義の直後で、この関数を呼んで実行している。
//
// 必要に応じて、必要な箇所の関数を真似てオーバーライドしてください。
//
// イベントのオーバーライドをすべて無視すると、c2env::decodeRequest()のみ行われる。
//
// また、このエディションでは、エラー処理など、自動でフレームワークが何かするわけではないことに注意。
//**************************************************************************************************
class _c2fw {

    var $response = array();
    var $alert = array(); // オンロードのalert()に出すメッセージ、テンプレートエンジン利用時のみ
    var $errors = array();
    var $config = array(
        "TplEngineType"=>"", // instant or smarty
        "TplDir"=>"", // テンプレートの場所
        "TplCompileDir"=>"", // テンプレートのコンバイル先
        "AutoCheckSession"=>true, // 自動セッションチェックフラグ
        "RequestStrict" => false, // パラメータの正当性を確認
        "SystemRequestParams" => array("session", "ajax_action", "submit_action"), // システムパラメータ名
        "ScreenRequestParams" => array(),  // 送信され得るパラメータ名。システムパラメータ名と併せ、それら以外が存在すればエラーnotice。
        "RequireRequestParams" => array(), // 絶対送信されないといけないパラメータ名。これら以外が存在すればエラーnotice。
    );

    var $session = "";
    var $loginEmpmstRow = "";
    var $login_emp_id = "";
    var $login_emp_lt_nm = "";
    var $login_emp_ft_nm = "";
    var $c2tpl = null;
    var $smarty = null;


    function c2StartApplication() {
        if (defined("C2_TPL_ENGINE_TYPE"))           $this->config["TplEngineType"]    = C2_TPL_ENGINE_TYPE;
        if (defined("C2_TEMPLATE_DIR_PATH"))         $this->config["TplDir"]           = C2_TEMPLATE_DIR_PATH;
        if (defined("C2_TEMPLATE_COMPILE_DIR_PATH")) $this->config["TplCompileDir"]    = C2_TEMPLATE_COMPILE_DIR_PATH;
        if (defined("C2_AUTO_CHECK_SESSION"))        $this->config["AutoCheckSession"] = C2_AUTO_CHECK_SESSION;
        if (defined("C2_REQUEST_STRICT"))            $this->config["RequestStrict"]    = C2_REQUEST_STRICT;

        //------------------------------------------------------
        // その０） c2OnError()
        // ◆◆◆ ちなみに、エラーが発生したら、こういう関数を呼びます。
        // ◆◆◆ FALSEを返すと、アプリを即終了します。FALSEを返すのと「exit;」と書くのとは、同じ意味である。
        //------------------------------------------------------
        if (!function_exists("c2OnError")) {
            function c2OnError($this, $event_name, $session, $request, $loginEmpstRow, $response, $errors){ return FALSE; };
        }


        //------------------------------------------------------
        // その１） c2OnStartApplication()
        // ◆◆◆ Core2のAJAXにおいては、EUCを手動エンコードして送信している。⇒ js/c2/common.js serializeUnicodeEntity()
        // ◆◆◆ これは、サーバ側で明示的に復元デコードしなければいけない。
        // ◆◆◆ 放っておくと、自動的に復元デコードが行われる。
        //
        // オーバーライドしてTRUEを返すと、リクエストをデコードしません。確認用として利用するイベントを意図している。
        //------------------------------------------------------
        if (function_exists("c2OnStartApplication")) {
            c2OnStartApplication($this, $_REQUEST, $this->config, $this->response, $this->errors);
            if (count($this->errors)) {
                if (FALSE===c2OnError($this, "c2OnStartApplication", $this->session, $_REQUEST, $this->loginEmpmstRow, $this->response, $this->errors)) die;
            }
        }

        if ($this->config["TplEngineType"] == C2_TPL_ENGINE_INSTANT) {
            require_once(dirname(__FILE__)."/C2Tpl.php");
            $this->c2tpl = c2GetC2Tpl();
        }
        else if ($this->config["TplEngineType"] == C2_TPL_ENGINE_SMARTY) {
            require_once(dirname(dirname(__FILE__))."/View/Smarty.php");
            $this->smarty = new Cmx_View();
        }

        if ($this->config["RequestStrict"]) {
            $errors = c2CheckExistRequestParams($_REQUEST, $this->config);
            if (count($errors)) {
                $this->errors = array_merge($this->errors, $errors);
                c2OnError($this, "c2OnStartApplication", $this->session, $_REQUEST, $this->loginEmpmstRow, $this->response, $this->errors);
            }
        }

        //------------------------------------------------------
        // その２） c2OnSetActionMap()
        // index.phpを据えて、一元的にURLコントロールしたい場合は、
        // そのindex.phpに、この関数を実装してください。
        // $action_map配列を返せば、ここでrequireする。
        // たとえばModelにはOnLoad、ViewにはOnRenderを実装するなどという用途で利用する。
        // $responseを利用して、勝手にModelをアサインしたりする。
        // Logic/Model/Viewそれぞれのファイルは、サブディレクトリに入れてもよい。
        //------------------------------------------------------
        if (function_exists("c2OnSetActionMap")) {
            c2Trace("[C2FW]","アクションマッピング開始");
            $action_map = c2OnSetActionMap();
            $action = $_REQUEST["action"];
            c2Trace("[C2FW]","action=".$action);
            $map = $action_map[$action];
            if ($map["Logic"]) {
                c2Trace("[C2FW]","Logicロード・・・".$map["Logic"]);
                require($map["Logic"]);
            }
            if ($map["Model"]) {
                c2Trace("[C2FW]","Modelロード・・・".$map["Model"]);
                require($map["Model"]);
            }
            if ($map["View"]) {
                c2Trace("[C2FW]","Viewロード・・・".$map["View"]);
                require($map["View"]);
            }
        }

        //------------------------------------------------------
        // その３） c2OnDecodePost()
        // ◆◆◆ Core2のAJAXにおいては、EUCを手動エンコードして送信している。⇒ js/c2/common.js serializeUnicodeEntity()
        // ◆◆◆ これは、サーバ側で明示的に復元デコードしなければいけない。
        // ◆◆◆ 放っておくと、自動的に復元デコードが行われる。
        //
        // オーバーライドしてTRUEを返すと、リクエストをデコードしません。確認用として利用するイベントです。
        //------------------------------------------------------
        $ret = FALSE;
        if (function_exists("c2OnDecodeRequest")) {
            $ret = c2OnDecodeRequest($this, $this->response, $this->errors);
            if (count($this->errors)) {
                if (FALSE===c2OnError($this, "c2OnDecodeRequest", $this->session, $_REQUEST, $this->loginEmpmstRow, $this->response, $this->errors)) die;
            }
        }
        if ($ret==FALSE) c2env::decodeRequest();

        //------------------------------------------------------
        // その４） c2OnValidateSession()
        // ◆◆◆ 毎々行うセッションチェックをイベント化
        // ◆◆◆ ログイン前の画面を利用する場合は、「C2_LOGIN_FREE」というデファインを立てること。
        // ◆◆◆ オーバーライドしないか、オーバーライドしてもFALSEを返す場合、セッションチェックはスルーされる。（デフォルト）
        // ◆◆◆ オーバーライドしてTRUEを返せば、このタイミングでセッションチェック。
        //
        // ◆◆◆ ※ちなみに$_SESSIONスーパーグローバルを利用したい場合は、c2env::prepareGlobalSession()など数個の関数を利用してください。
        //------------------------------------------------------
        $bool = false;
        if (function_exists("c2OnValidateSession")) {
            $isAutoCheckSession = c2OnValidateSession($this, $_REQUEST, $this->response, $this->errors);
            if (count($this->errors)) {
                if (FALSE===c2OnError($this, "OnValidateSession", $this->session, $_REQUEST, $this->loginEmpmstRow, $this->response, $this->errors)) die;
            }
        }
        if ($isAutoCheckSession || $this->config["AutoCheckSession"]) {
			if (!defined("C2_LOGIN_FREE")) {
	            $this->loginEmpmstRow = c2login::validateSession();
	            $this->session = C2_SESSION_ID;
	            $this->login_emp_id = C2_LOGIN_EMP_ID;
	            $this->login_emp_lt_nm = C2_LOGIN_EMP_LT_NM;
	            $this->login_emp_ft_nm = C2_LOGIN_EMP_FT_NM;
	        }
        }

        //------------------------------------------------------
        // その５） c2OnValidateAuth()
        // ◆◆◆ 毎々行うセッションチェックをイベント化
        // ◆◆◆ オーバーライドしないか、オーバーライドしてもFALSEを返す場合、セッションチェックはスルーされる。（デフォルト）
        // ◆◆◆ オーバーライドしてTRUEを返せば、このタイミングでセッションチェック。
        //
        // ◆◆◆ ※ちなみに$_SESSIONスーパーグローバルを利用したい場合は、c2env::prepareGlobalSession()など数個の関数を利用してください。
        //------------------------------------------------------
        if (function_exists("c2OnValidateAuth")) {
            $c2app = c2GetC2App();
            $EAL = $c2app->getAuthmstExp($this->login_emp_id);
            $isValidOK = c2OnValidateAuth($this, $_REQUEST, $this->login_emp_id, $this->loginEmpmstRow, $EAL, $this->response, $this->errors);
            if (!$isValidOK || count($this->errors)) {
                if (FALSE===c2OnError($this, "OnValidateAuth", $this->session, $_REQUEST, $this->loginEmpmstRow, $this->response, $this->errors)) die;
            }
        }

        //------------------------------------------------------
        // その６） c2OnSubmit()
        // ◆◆◆ メイン処理用イベント発行(基本はAJAXアクセス以外)
        // ◆◆◆ ビジネスロジックは、基本的にこれをオーバーライドして実装ください。
        //------------------------------------------------------
        $submit_action = $_REQUEST["submit_action"];
        if ($submit_action) {
            if (function_exists("c2OnSubmit")) {
                c2OnSubmit($this, $submit_action, $this->session, $_REQUEST, $this->login_emp_id, $this->loginEmpmstRow, $this->response, $this->alert, $this->errors);
                if (count($this->errors)) {
                    if (FALSE===c2OnError($this, "OnSubmit", $this->session, $_REQUEST, $this->loginEmpmstRow, $this->response, $this->errors)) die;
                }
            }
        }

        //------------------------------------------------------
        // その７） c2OnLoad()
        // ◆◆◆ メイン処理用イベント発行(基本はAJAXアクセス以外)
        // ◆◆◆ ビジネスロジックは、基本的にこれをオーバーライドして実装ください。
        //------------------------------------------------------
        if (!array_key_exists("ajax_action", $_REQUEST)) {
            if (function_exists("c2OnLoad")) {
                c2OnLoad($this, $this->session, $_REQUEST, $this->login_emp_id, $this->loginEmpmstRow, $this->response, $this->errors);
                if (count($this->errors)) {
                    if (FALSE===c2OnError($this, "OnLoad", $this->session, $_REQUEST, $this->loginEmpmstRow, $this->response, $this->errors)) die;
                }
            }
        }

        //------------------------------------------------------
        // その８） c2OnAjaxLoad()
        // ◆◆◆ メイン処理用イベント発行(基本はAJAXアクセス)
        // ◆◆◆ リクエスト内に"ajax_action"というパラメータが存在すると、ajaxリクエストとみなされます。
        // ◆◆◆ ビジネスロジックは、基本的にこれをオーバーライドして実装ください。
        //------------------------------------------------------
        if (array_key_exists("ajax_action", $_REQUEST)) {
            if (function_exists("c2OnAjaxLoad")) {
                c2OnAjaxLoad($this, $this->session, $_REQUEST, $this->login_emp_id, $this->loginEmpmstRow, $this->response, $this->errors);
                if (count($this->errors)) {
                    if (FALSE===c2OnError($this, "OnAjaxLoad", $this->session, $_REQUEST, $this->loginEmpmstRow, $this->response, $this->errors)) die;
                }
            }
        }

        //------------------------------------------------------
        // その９） c2OnRender()
        // ◆◆◆ HTMLレンダーイベント
        // ◆◆◆ HTMLの出力はここで。あるいはViewコントローラなどを作成する場合は、ここでバインド値のフォーマッティングなどを。
        // ◆◆◆ オーバーライドしてFALSEを返した場合、
        //------------------------------------------------------
        if (function_exists("c2OnTplRender")) {
            $engine = "none";
            if ($this->config["TplEngineType"]==C2_TPL_ENGINE_INSTANT) {
                $engine = $this->c2tpl;
                $engine->setTplDir($this->config["TplDir"]);
            }
            if ($this->config["TplEngineType"]==C2_TPL_ENGINE_SMARTY) {
                $engine = $this->smarty;
                if ($this->config["TplDir"])        $engine->template_dir = $this->config["TplDir"];
                if ($this->config["TplCompileDir"]) $engine->compile_dir  = $this->config["TplCompileDir"];
            }

            c2OnTplRender($this, $engine, $this->session, $_REQUEST, $_emp_info, $this->response, $this->errors);
            if (count($this->errors)) {
                if (FALSE===c2OnError($this, "OnTplRender", $this->session, $_REQUEST, $this->loginEmpmstRow, $this->response, $this->errors)) die;
            }
            if (count($this->alert)) {
                echo '<script type="text/javascript">';
                echo 'alert("'.c2js(implode("\n", $this->alert)).'")';
                echo '</script>';
            }
        }
    }
} // end of class $_c2fw

global $c2fw;
$c2fw = new _c2fw();
function c2GetC2FW() {
    global $c2fw;
    return $c2fw;
}


//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// 実行
//
// ハンディースタート定義がなければ即実行。基本的には定義されていないので即実行。
// 即実行するので、require_once("C2FW.php")を行う前に、各種DEFINEは事前定義が必要。
// また、必要なイベントオーバーライドも、呼出しファイル自体に実装するか、require済みでないといけない。
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
if (!defined("C2_FW_HANDY_START")) {
    $c2fw->c2StartApplication(); // ◆◆◆◆◆◆◆◆◆ 実行開始！
}


