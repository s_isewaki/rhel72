<?
require(dirname(dirname(dirname(dirname(__FILE__))))."/conf/conf.inf");
require(dirname(dirname(dirname(dirname(__FILE__))))."/conf/conf_define.inf");
require_once(dirname(__FILE__)."/C2Utils.php");


//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// CORE 2 Environment Tools
//
// 以下のインフラ系セッティングを扱う。
//
// ◆文字コード補正、ajax通信補助、カレントディレクトリ補正などの、サーバ環境の補助
// ◆CoMedix他機能との橋渡し（aclglog）
// ◆サーバセッション（$_SESSION）の一元管理
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――

define("C2_CS_EUCJP", (phpversion() < '5.2.1' ? "eucJP-win" : "CP51932"));

class c2env {
    //―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
    // カレントディレクトリ制御
    //
    // comedix直下へソースコードを配置しないで良くするためのギミック --- ディレクトリパス調整を利用【chdir】
    //
    // カレントディレクトリを強制変更しないと、comedix直下のLIB系ファイルのロードができない。
    // （2014/09時点ではロードできるようになったが、他の多くの画面では、comedix直下でないと恐らく動作しない）
    //
    // Core2では既存の資産も利用できるように想定されているため、
    // 他機能からの乗り換えで全部書き直さなくてもよいように、Core2自体を柔軟にしておく方針をとっている。
    //
    // クライアントからのURL呼出があって、apacheがphpエンジンを動かして、phpエンジンがphpソースをコールしたとき、
    // コールされたphpソースの場所がカレントディレクトリになるわけだが、
    // もしカレントディレクトリがこのファイルと同じディレクトリなら、ひとつ上にのぼって、comedix直下とする。という話。
    //―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
    function adjustCurrentDir() {
        if (defined("C2_CORE2_DIR")) return;
        define("C2_CORE2_DIR", "class/Cmx/Core2/"); // comedix直下から見たこのフレームワークファイルの場所。未来永劫変更なし。
        $base_folder = "";
        $base_dir = "";
        $fpath = getcwd()."/".C2_CORE2_DIR."C2Env.php";
        if (!is_file($fpath)) {
            $fpath = dirname(getcwd())."/".C2_CORE2_DIR."C2Env.php";
            if (is_file($fpath)) {
                $base_folder = basename(getcwd());
                $base_dir = dirname(getcwd());
                // 上階層にいく前に、このディレクトリをLIBインクルード先の第一優先で追加。
                set_include_path("./".$base_folder . PATH_SEPARATOR . get_include_path());
                chdir("../"); // カレントディレクトリがstudyなら、ひとつ上階層のcomedixへカレントディレクトリ移動
            }
        }
        define("C2_BASE_FOLDER", $base_folder);
        define("C2_BASE_DIR",    $base_dir);
    }



    //―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
    // ログ関連
    //―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――

    // ２個前の実行ファイルのファイル名を返す
    function getCallerFileName($fname) {
        return c2GetCallerFileName($fname);
    }
    // アクセスログの保存
    function aclgRegist($fname="") {
        require_once("aclg_set.php");
        if (!$fname) $fname = c2GetCallerFileName("");
        // アクセスログ
        $con = connect2db($fname);
        aclg_regist(C2_SESSION_ID, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"], $_POST, $con, $_GET);
    }




    //******************************************************************************
    // AJAX対策。
    // 最初にリクエストをデコードする。
    //******************************************************************************
    function _unicodeEntityDecode($str) {
        $ary = explode(";", $str);
        $out = array();
        foreach($ary as $s) {
            if (!strlen($s)) continue;
            if ($s==="&#9") { $out[]= "\t"; continue; }
            if ($s==="&#160") { $out[]= " "; continue; }
            $ss = mb_convert_encoding($s.";", C2_CS_EUCJP, "HTML-ENTITIES");
            if ($ss==="\t") { $out[] = $s.";"; continue; }
            $ss = mb_convert_encoding(mb_convert_encoding($ss, 'sjis-win', 'eucjp'), C2_CS_EUCJP, 'sjis-win');
            if ($ss==="\t") $out[] = $s.";";
            else $out[] = $ss;
        }
        return implode("", $out);
    }
    function decodeRequest() {
        $def = mb_substitute_character(); // たぶんハテナマーク
        mb_substitute_character(0x9); // 適当にタブ文字
        $request_names = array_keys($_REQUEST);
        foreach ($request_names as $key) {
            if (substr($key, 0, 12)!="uni_entity__") continue;
            $val = $_REQUEST[$key];
            if (is_array($val)) {
                foreach ($val as $idx=>$v) $val[$idx] = c2env::_unicodeEntityDecode($v);
                $_REQUEST[substr($key,12)] = $val;
            } else {
                $_REQUEST[substr($key,12)] = c2env::_unicodeEntityDecode($val);
            }
            unset($_REQUEST[$key]);
        }
        $post_names = array_keys($_POST);
        foreach ($post_names as $key) {
            if (substr($key, 0, 12)!="uni_entity__") continue;
            $val = $_POST[$key];
            if (is_array($val)) {
                foreach ($val as $idx=>$v) $val[$idx] = c2env::_unicodeEntityDecode($v);
                $_POST[substr($key,12)] = $val;
            } else {
                $_POST[substr($key,12)] = c2env::_unicodeEntityDecode($val);
            }
            unset($_POST[$key]);
        }
        mb_substitute_character($def); // 戻す
    }


    //******************************************************************************
    //
    //******************************************************************************
    function echoStaticHtmlTag($part, $option=""){
        $out = array();
        if ($part=="HTML5_DOCTYPE_HTML") {
            $out[]= '<!DOCTYPE html>';
            $out[]= '<html lang="ja" '.$option.'>';
        }
        if ($part=="HTML5_HEAD_META") {
            $out[]= '<head '.$option.'>';
            $out[]= '<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1,requiresActiveX=true" />';
            $out[]= '<meta http-equiv="Expires" content="0" />';
            $out[]= '<meta http-equiv="Cache-Control" content="no-cache" />';
            $out[]= '<meta http-equiv="Pragma" content="no-cache" />';
            $out[]= '<meta http-equiv="Content-Type" content="text/html;charset=EUC-JP" />';
        }
        echo implode("\n", $out);
    }


    //******************************************************************************
    // セッション変数管理。アプリ間のセッション汚染を防ぐ。適当に任意に独自にグルーピングして利用のこと。
    // ただしセッションを乱発したりしないように。誰がどこで初期化してしまうかわからない。
    // また、利用時は、利用後の他者の処理などに悪影響を与えていないかも注意が必要なため、原則的には利用しないほうが吉。
    // 用途としては、ブラウザリロード／戻るボタン対策の更新即時リダイレクトを行う場合の成功メッセージ出力などが美しく最適に実装できるのだが、
    // ただし、webmail/config/xxxとか利用すると、セッション定義を書き換えられた上、先に別名でセッションを開始されてしまい、
    // ここでのセッションが利かなくなるので利用できない。
    // 美しい代替案としては、Ajax越しでの更新、が有効だが、日本語更新は工夫が必要である。
    //******************************************************************************
    function _prepareGlobalSession() {
        if (!defined("C2_SESSION_GROUP_NAME")) { echo "セッション利用にはC2_SESSION_GROUP_NAMEデファイン定義が必要です"; die; }
        $session_group = "C2_APP_SESSION_".C2_SESSION_GROUP_NAME;
        session_name("CMX_AUTH_SESSION"); // 本来のPHPSESSIDを書き換える
        session_id(C2_SESSION_ID);
        $params = session_get_cookie_params();
        session_set_cookie_params($params["lifetime"], c2GetContentsRootPath());
        @session_start(); // 2回以上呼ばれても無視される。
        $params["sid"] = session_id();
        return $session_group;
    }
    function setGlobalSession($key, $v) {
        $session_group = c2env::_prepareGlobalSession();
        $_SESSION[$session_group][$key] = $v;
    }
    function getGlobalSession($key) {
        $session_group = c2env::_prepareGlobalSession();
        if ($key!="") return $_SESSION[$session_group][$key];
        return (array)$_SESSION[$session_group];
    }
    function unsetGlobalSession($key) {
        $session_group = c2env::_prepareGlobalSession();
        if ($key!="") unset($_SESSION[$session_group][$key]);
        else unset($_SESSION[$session_group]);
    }

}


