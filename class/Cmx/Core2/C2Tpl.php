<?
require_once(dirname(__FILE__)."/C2Utils.php"); // 汎用関数群


//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// テンプレートエンジン関連
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――

//******************************************************************************
// $smartyに代わる即席テンプレートエンジン
//
// コントロール側の変数が簡単に漏えいしないように、
// あるいは、htmlコンテンツ側にどの変数を引き渡したかを明確にするように、この関数を介する。
// ここで呼ばれるtplは、smartyファイルではない。通常のPHPファイルである。
//
// 第一引数はテンプレート名を指定。テンプレートへのパスも指定してよいが、
// 適宜共通デファインファイルなどを作成し、「C2_TEMPLATE_DIR_PATH」を定義すればそこからのパスとする。
//
// 第二引数は連想配列を指定のこと。extractでこのスコープに展開する。
//
// 最後に結局テンプレートファイルとするPHPファイルを、この関数スコープでrequire_onceしているだけ。
// つまり、デファインや外部関数は利用可能。外部変数はglobal命令で参照してください。
//******************************************************************************
global $c2tpl;
$c2tpl = new _c2tpl();
function c2GetC2Tpl() {
    global $c2tpl;
    return $c2tpl;
}

class _c2tpl {
    var $tpl_vars = array();
    var $tpl_dir = "";
    var $tpl_name = "";

    function setTplDir($dir) {
        $this->tpl_dir = $dir;
    }

    function _array_escape($ary) {
        if (!is_array($ary) && !is_object($ary)) return hh($ary);
        foreach ($ary as $k => $v) {
            $ary[$k] = $this->_array_escape($v);
        }
    }

    function assignHtml($k, $v) {
        $this->tpl_vars[$k] = $this->array_escape($v);
    }

    function assignData($k, $v) {
        $this->tpl_vars[$k] = $v;
    }

    function display($tpl_name, $template_args=array()) {
        $this->tpl_name = $tpl_name;
        $this->_display_execute();
    }

    function _display_execute() {
        extract($this->tpl_vars);
        require_once($this->tpl_dir."/".$this->tpl_name);
    }
}
