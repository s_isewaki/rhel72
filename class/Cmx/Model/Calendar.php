<?php

require_once('Cmx.php');
require_once('Cmx/Core/Model.php');

/**
 * CoMedix：カレンダー
 *
 * @link http://trac.medi-system.co.jp/trac/comedix/wiki/tables/calendar
 */
class Cmx_Calendar extends Model {

    var $_wj;
    var $_start_time;
    var $_end_time;

    /**
     * コンストラクタ
     */
    function Cmx_Calendar() {
        $this->_wj = array('日', '月', '火', '水', '木', '金', '土');
        parent::connectDB();
    }

    /**
     * カレンダーリスト
     *
     * @param $start_date 開始日
     * @param $end_date 終了日
     * @return カレンダーリスト
     */
    function lists($start_date, $end_date) {
        $this->_start_time = strtotime($start_date);
        $this->_end_time = strtotime($end_date);

        $sth = $this->db->prepare("
            SELECT *,
                DATE_PART('year',TO_DATE(date,'YYYYMMDD')) AS y,
                DATE_PART('month',TO_DATE(date,'YYYYMMDD')) AS m,
                DATE_PART('day',TO_DATE(date,'YYYYMMDD')) AS d,
                DATE_PART('dow',TO_DATE(date,'YYYYMMDD')) AS w
            FROM calendar
            WHERE date BETWEEN ? AND ? ORDER BY date
        ");
        $res = $sth->execute(array(date('Ymd', $this->_start_time), date('Ymd', $this->_end_time)));

        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row["wj"] = $this->_wj[$row["w"]];
            $list[$row["date"]] = $row;
        }
        return $list;
    }

    /**
     * カレンダーデータ
     *
     * @param  str $date 取得日
     * @return arr カレンダーデータ
     */
    function date($date) {
        return $this->db->extended->getRow(
                "
            SELECT *,
                DATE_PART('year',TO_DATE(date,'YYYYMMDD')) AS y,
                DATE_PART('month',TO_DATE(date,'YYYYMMDD')) AS m,
                DATE_PART('day',TO_DATE(date,'YYYYMMDD')) AS d,
                DATE_PART('dow',TO_DATE(date,'YYYYMMDD')) AS w
            FROM calendar
            WHERE date = ?
            ",
                null, array($date), array('text'), MDB2_FETCHMODE_ASSOC);
    }

}
