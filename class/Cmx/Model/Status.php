<?php

require_once('Cmx.php');
require_once('Cmx/Core/Model.php');

/**
 * CoMedix 役職
 */
class Cmx_Status extends Model {

    /**
     * コンストラクタ
     */
    function Cmx_Status() {
        parent::connectDB();
    }

    /**
     * 一覧
     * @return Array
     */
    public function lists() {
        $sql = "
            SELECT
                st_id,
                st_nm,
                order_no,
                link_key
            FROM stmst
            WHERE NOT st_del_flg
            ORDER BY order_no
        ";
        $res = $this->db->query($sql);

        if (PEAR::isError($res)) {
            return array();
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row;
        }
        return $lists;
    }

}
