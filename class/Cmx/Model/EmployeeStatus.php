<?php

require_once('Cmx.php');
require_once('Cmx/Core/Model.php');

/**
 * CoMedix：役職(職位)
 *
 * @link http://trac.medi-system.co.jp/trac/comedix/wiki/tables/stmst
 */
class Cmx_EmployeeStatus extends Model {

    var $_row;

    /**
     * コンストラクタ
     *
     * @param Arrayならrowデータと判断、そうでなければ職員IDとして処理
     */
    function Cmx_EmployeeStatus($param = null) {
        parent::connectDB();

        if (is_array($row)) {
            $this->_init($row);
        }
        else if (!is_null($row)) {
            $this->_select($row);
        }
    }

    // _init
    function _init($row) {
        $this->_row = $row;
    }

    // _select
    function _select($row) {
        $sth = $this->db->prepare("
            SELECT *
            FROM stmst
            WHERE st_id= ?
        ");
        $res = $sth->execute($row);
        $this->_init($res->fetchRow(MDB2_FETCHMODE_ASSOC));
    }

    /**
     * リスト
     * @return array
     */
    function lists() {
        $res = $this->db->query("SELECT *, st_id as id, st_nm as name FROM stmst WHERE NOT st_del_flg ORDER BY order_no, st_id");

        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[$row['id']] = $row;
        }
        return $list;
    }

    /**
     * 役職マスタID
     * @return 役職マスタID
     */
    function st_id() {
        return $this->_getter(__FUNCTION__);
    }

    /**
     * 役職名
     * @return 役職名
     */
    function st_name() {
        return $this->_row["st_nm"];
    }

    /**
     * 連携キー
     *
     * @return 連携キー
     */
    function link_key() {
        return $this->_row["link_key"];
    }

}
