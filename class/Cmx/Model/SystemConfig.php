<?php

//
// CoMedix全体の設定の管理クラス
//
//
require_once('Cmx.php');
require_once('Cmx/Core/Model.php');

class Cmx_SystemConfig extends Model {

    var $insert_sth;
    var $update_sth;
    var $select_sth;

    // Constractor
    function Cmx_SystemConfig() {
        parent::connectDB();

        // prepare statements
        $this->insert_sth = $this->db->prepare("INSERT INTO system_config (key,value) VALUES (:key,:value)",
                                               array('text', 'text'), MDB2_PREPARE_MANIP);
        $this->update_sth = $this->db->prepare("UPDATE system_config SET value = :value WHERE key = :key",
                                               array('text', 'text'), MDB2_PREPARE_MANIP);
        $this->select_sth = $this->db->prepare("SELECT value FROM system_config WHERE key = ?");
    }

    // Set Method
    function set($name, $newvalue) {
        $res = $this->select_sth->execute($name);

        // INSERT
        if (!$res->numRows()) {
            $this->db->beginTransaction();
            $this->insert_sth->execute(array('key'   => $name, 'value' => $newvalue));
            $this->db->commit();
        }

        // UPDATE
        if ($newvalue != $res->fetchOne()) {
            $this->db->beginTransaction();
            $this->update_sth->execute(array('key'   => $name, 'value' => $newvalue));
            $this->db->commit();
        }
    }

    // Get Method
    function get($name) {
        $res = $this->select_sth->execute($name);
        if (!$res->numRows()) {
            return null;
        }
        return $res->fetchOne();
    }

}
