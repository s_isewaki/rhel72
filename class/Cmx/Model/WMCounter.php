<?php

require_once('Cmx.php');
require_once('Cmx/Core/Model.php');

/**
 * CoMedix wm_counter
 */
class Cmx_WMCounter extends Model {

    var $insert_sth;
    var $update_sth;

    /**
     * コンストラクタ
     * @param str $session セッションID
     */
    function Cmx_WMCounter() {
        parent::connectDB();

        // prepare statements
        $this->insert_sth = $this->db->prepare("INSERT INTO wm_counter (emp_id, used_emp_id, count_num) VALUES (:emp_id, :used_emp_id, 1)",
                                               array('text', 'text'), MDB2_PREPARE_MANIP);
        $this->update_sth = $this->db->prepare("UPDATE wm_counter SET count_num = count_num + 1 WHERE emp_id = :emp_id AND used_emp_id = :used_emp_id",
                                               array('text', 'text'), MDB2_PREPARE_MANIP);
    }

    // Set Method
    function countup($emp_id, $used_emp_id) {
        $is_data = $this->db->extended->getOne("SELECT COUNT(*) FROM wm_counter WHERE emp_id = :emp_id AND used_emp_id = :used_emp_id", null,
            array('emp_id' => $emp_id, 'used_emp_id' => $used_emp_id), array('text', 'text'));

        // INSERT
        if (!$is_data) {
            $this->db->beginTransaction();
            $this->insert_sth->execute(array('emp_id' => $emp_id, 'used_emp_id' => $used_emp_id));
            $this->db->commit();
        }

        // UPDATE
        else {
            $this->db->beginTransaction();
            $this->update_sth->execute(array('emp_id' => $emp_id, 'used_emp_id' => $used_emp_id));
            $this->db->commit();
        }
    }

}
