<?php

//
// ��̳ɽ�����ߴ�९�饹
//
//
require_once('Cmx.php');
require_once('Cmx/Core/Model.php');

class Cmx_Shift_Institution extends Model
{

    var $_list;
    var $_row;

    // Constractor
    function Cmx_Shift_Institution($row = null)
    {
        parent::connectDB();
        $this->_list = array();

        if (is_array($row)) {
            $this->_init($row);
        }
    }

    // Lists
    function lists()
    {
        $res = $this->db->query("SELECT * FROM duty_shift_institution_standard ORDER BY standard_id");

        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $obj = new Cmx_Shift_Institution($row);
            array_push($list, $obj);
        }
        return $list;
    }

    // _init
    function _init($row)
    {
        $this->_row = $row;
    }

    // get
    function standard_id()
    {
        return $this->_getter(__FUNCTION__);
    }

    function ward_cnt()
    {
        return $this->_getter(__FUNCTION__);
    }

    function sickbed_cnt()
    {
        return $this->_getter(__FUNCTION__);
    }

    function report_kbn()
    {
        return $this->_getter(__FUNCTION__);
    }

    function report_patient_cnt()
    {
        return $this->_getter(__FUNCTION__);
    }

    function hosp_patient_cnt()
    {
        return $this->_getter(__FUNCTION__);
    }

    function nursing_assistance_flg()
    {
        return $this->_getter(__FUNCTION__);
    }

    function prov_start_hhmm()
    {
        return $this->_getter(__FUNCTION__);
    }

    function prov_end_hhmm()
    {
        return $this->_getter(__FUNCTION__);
    }

    function limit_time()
    {
        return $this->_getter(__FUNCTION__);
    }

    function shift_flg()
    {
        return $this->_getter(__FUNCTION__);
    }

    function send_start_hhmm_1()
    {
        return $this->_getter(__FUNCTION__);
    }

    function send_end_hhmm_1()
    {
        return $this->_getter(__FUNCTION__);
    }

    function send_start_hhmm_2()
    {
        return $this->_getter(__FUNCTION__);
    }

    function send_end_hhmm_2()
    {
        return $this->_getter(__FUNCTION__);
    }

    function send_start_hhmm_3()
    {
        return $this->_getter(__FUNCTION__);
    }

    function send_end_hhmm_3()
    {
        return $this->_getter(__FUNCTION__);
    }

    function labor_cnt()
    {
        return $this->_getter(__FUNCTION__);
    }

    function nurse_staffing_flg()
    {
        return $this->_getter(__FUNCTION__);
    }

    function acute_nursing_flg()
    {
        return $this->_getter(__FUNCTION__);
    }

    function acute_nursing_cnt()
    {
        return $this->_getter(__FUNCTION__);
    }

    function nursing_assistance_cnt()
    {
        return $this->_getter(__FUNCTION__);
    }

}
