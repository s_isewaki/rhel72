<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'Cmx/Model/Shift/Staff.php';
require_once 'Cmx/Model/Shift/Ward.php';
require_once 'Cmx/Model/Shift/Table.php';
require_once 'Cmx/Shift/Log.php';

class Cmx_Shift_Approval extends Model
{

    var $log;

    /**
     * コンストラクタ
     */
    public function __construct()
    {
        parent::connectDB();

        $this->log = new Cmx_Shift_Log('approval');
    }

    /**
     * 月次承認
     * @param type $emp_ids
     * @param type $year
     * @param type $month
     * @param type $approval_type
     * @param type $approval_emp_id
     * @throws Exception
     */
    private function approval($emp_ids, $year, $month, $approval_type, $approval_emp_id)
    {
        $sel_sth = $this->db->prepare("SELECT emp_id FROM duty_shift_approval WHERE year = ? AND month = ? AND approval_type = ?", array('integer', 'integer', 'integer'), MDB2_PREPARE_RESULT);
        $ins_sth = $this->db->prepare("INSERT INTO duty_shift_approval (emp_id, year, month, approval_type, approval_emp_id) VALUES (?, ?, ?, ?, ?)", array('text', 'integer', 'integer', 'integer', 'text'), MDB2_PREPARE_MANIP);

        // 承認済み職員を取得
        $res = $sel_sth->execute(array($year, $month, $approval_type));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            cmx_log("ROLLBACK: duty_shift_approval SELECT ERROR: {$year}, {$month}, {$approval_type} " . $res->getDebugInfo());
            throw new Exception("duty_shift_approval SELECT ERROR: {$year}, {$month}, {$approval_type} " . $res->getDebugInfo());
        }
        $emp = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $emp[$row['emp_id']] = 1;
        }

        $this->db->beginNestedTransaction();
        foreach ($emp_ids as $emp_id) {
            if (is_object($emp_id)) {
                $emp_id = $emp_id->emp_id();
            }

            // 承認済みは処理しない
            if ($emp[$emp_id]) {
                continue;
            }
            $res = $ins_sth->execute(array($emp_id, $year, $month, $approval_type, $approval_emp_id));
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                cmx_log("ROLLBACK: duty_shift_approval INSERT ERROR: {$emp_id} " . $res->getDebugInfo());
                throw new Exception("duty_shift_approval INSERT ERROR: {$emp_id} " . $res->getDebugInfo());
            }
        }
        $this->db->completeNestedTransaction();
    }

    /**
     * 月次承認(部署)
     * @param type $group_id
     * @param type $year
     * @param type $month
     * @param type $approval_type
     * @param type $approval_emp_id
     * @throws Exception
     */
    private function approval_group($group_id, $year, $month, $approval_type, $approval_emp_id)
    {
        $ins_sth = $this->db->prepare("INSERT INTO duty_shift_approval_group (group_id, year, month, approval_type, approval_emp_id) VALUES (?, ?, ?, ?, ?)", array('text', 'integer', 'integer', 'integer', 'text'), MDB2_PREPARE_MANIP);

        // 承認済み?
        $is_approval = $this->db->extended->getOne("SELECT COUNT(*) FROM duty_shift_approval_group WHERE group_id = ? AND year = ? AND month = ? AND approval_type = ?", null, array($group_id, $year, $month, $approval_type), array('integer'));

        $this->db->beginNestedTransaction();

        // 承認済みは処理しない
        if ($is_approval) {
            //
        }
        else {
            $res = $ins_sth->execute(array($group_id, $year, $month, $approval_type, $approval_emp_id));
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                cmx_log("ROLLBACK: duty_shift_approval_group INSERT ERROR: {$group_id} " . $res->getDebugInfo());
                throw new Exception("duty_shift_approval_group INSERT ERROR: {$group_id} " . $res->getDebugInfo());
            }
        }

        $this->db->completeNestedTransaction();

        $table = new Cmx_Shift_Table();
        $table->set_ward(new Cmx_Shift_Ward($group_id));
        $table->set_staff(new Cmx_Shift_Staff());
        return $table->month_staff_lists($year, $month);
    }

    /**
     * 本人承認
     * @param type $emp_id
     * @param type $year
     * @param type $month
     * @param type $approval_emp_id
     */
    public function self_approval($emp_id, $year, $month, $approval_emp_id)
    {
        $this->db->beginNestedTransaction();
        $this->approval(array($emp_id), $year, $month, 0, $approval_emp_id);
        $this->db->completeNestedTransaction();

        $this->log->log('本人承認.完了', array('emp_id' => $emp_id, 'year' => $year, 'month' => $month, 'approval_emp_id' => $approval_emp_id));
    }

    /**
     * 所属長承認
     * @param type $emp_id
     * @param type $year
     * @param type $month
     * @param type $approval_emp_id
     */
    public function manager_approval($emp_id, $year, $month, $approval_emp_id)
    {
        $this->db->beginNestedTransaction();
        $this->approval(array($emp_id), $year, $month, 1, $approval_emp_id);
        $this->db->completeNestedTransaction();

        $this->log->log('所属長承認.完了', array('emp_id' => $emp_id, 'year' => $year, 'month' => $month, 'approval_emp_id' => $approval_emp_id));
    }

    /**
     * 所属長承認(部署)
     * @param type $group_id
     * @param type $year
     * @param type $month
     * @param type $approval_emp_id
     */
    public function manager_approval_group($group_id, $year, $month, $approval_emp_id)
    {
        $this->db->beginNestedTransaction();
        $emp_ids = $this->approval_group($group_id, $year, $month, 1, $approval_emp_id);
        $this->approval($emp_ids, $year, $month, 1, $approval_emp_id);
        $this->db->completeNestedTransaction();

        $this->log->log('所属長承認.完了', array('group_id' => $group_id, 'year' => $year, 'month' => $month, 'approval_emp_id' => $approval_emp_id));
    }

    /**
     * 部門長承認
     * @param type $emp_id
     * @param type $year
     * @param type $month
     * @param type $approval_emp_id
     */
    public function boss_approval($emp_id, $year, $month, $approval_emp_id)
    {
        $this->db->beginNestedTransaction();
        $this->approval(array($emp_id), $year, $month, 999, $approval_emp_id);
        $this->db->completeNestedTransaction();

        $this->log->log('部門長承認.完了', array('emp_id' => $emp_id, 'year' => $year, 'month' => $month, 'approval_emp_id' => $approval_emp_id));
    }

    /**
     * 部門長承認(部署)
     * @param type $group_id
     * @param type $year
     * @param type $month
     * @param type $approval_emp_id
     */
    public function boss_approval_group($group_id, $year, $month, $approval_emp_id)
    {
        $this->db->beginNestedTransaction();
        $emp_ids = $this->approval_group($group_id, $year, $month, 999, $approval_emp_id);
        $this->approval($emp_ids, $year, $month, 999, $approval_emp_id);
        $this->db->completeNestedTransaction();

        $this->log->log('部門長承認.完了', array('group_id' => $group_id, 'year' => $year, 'month' => $month, 'approval_emp_id' => $approval_emp_id));
    }

    /**
     * 承認済み職員を取得
     * @param type $emp_id
     * @param type $year
     * @param type $month
     * @return type
     * @throws Exception
     */
    public function approved($emp_id, $year, $month)
    {
        $sth = $this->db->prepare("SELECT approval_type, e.emp_lt_nm || ' ' || e.emp_ft_nm AS approval_name, approval_date FROM duty_shift_approval ap JOIN empmst e ON e.emp_id=approval_emp_id WHERE ap.emp_id = ? AND year = ? AND month = ? ORDER BY approval_type", array('text', 'integer', 'integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array($emp_id, $year, $month));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            cmx_log("ROLLBACK: duty_shift_approval SELECT ERROR: {$emp_id}, {$year}, {$month} " . $res->getDebugInfo());
            throw new Exception("duty_shift_approval SELECT ERROR: {$emp_id}, {$year}, {$month} " . $res->getDebugInfo());
        }
        $data = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $data[$row['approval_type']] = $row;
        }
        return array($data['0'], $data['1'], $data['999']);
    }

    /**
     * 承認済み月の末日を取得
     * @param type $emp_id
     * @return type
     * @throws Exception
     */
    public function last_approved_date($emp_id)
    {
        $data = $this->db->extended->getRow("SELECT year, month FROM duty_shift_approval WHERE emp_id = ? ORDER BY year DESC, month DESC LIMIT 1", null, array($emp_id), array('text'), MDB2_FETCHMODE_ASSOC);
        return date('Ymt', strtotime("{$data['year']}-{$data['month']}-1"));
    }

    /**
     * 承認済み部署を取得
     * @param type $group_id
     * @param type $year
     * @param type $month
     * @return type
     * @throws Exception
     */
    public function approved_group($group_id, $year, $month)
    {
        $sth = $this->db->prepare("SELECT approval_type, approval_date, e.emp_lt_nm || ' ' || e.emp_ft_nm AS approval_name FROM duty_shift_approval_group ap JOIN empmst e ON e.emp_id=approval_emp_id WHERE group_id = ? AND year = ? AND month = ? ORDER BY approval_type", array('text', 'integer', 'integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array($group_id, $year, $month));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            cmx_log("ROLLBACK: duty_shift_approval_group SELECT ERROR: {$group_id}, {$year}, {$month} " . $res->getDebugInfo());
            throw new Exception("duty_shift_approval_group SELECT ERROR: {$group_id}, {$year}, {$month} " . $res->getDebugInfo());
        }
        $data = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $data[$row['approval_type']] = $row;
        }
        return array(array(), $data['1'], $data['999']);
    }

    /**
     * 検索条件の作成
     * @param type $cond
     * @return string
     */
    protected function make_condition($cond = null)
    {
        // 名前
        if (!empty($cond['srh_name'])) {
            $where[] = "e.emp_ft_nm || ' ' || e.emp_lt_nm || ' ' || e.emp_kn_ft_nm || ' ' || e.emp_kn_lt_nm || ' ' || e.emp_personal_id ILIKE :srh_name";
            $types[] = 'text';
            $data['srh_name'] = "%" . $cond['srh_name'] . "%";
        }

        // 開始日
        if (!empty($cond['srh_start_date'])) {
            $where[] = " :srh_start_date <= approval_date";
            $types[] = 'text';
            $data['srh_start_date'] = $cond['srh_start_date'];
        }

        // 終了日
        if (!empty($cond['srh_last_date'])) {
            $where[] = " to_char(approval_date,'YYYY-MM-DD') <= :srh_last_date";
            $types[] = 'text';
            $data['srh_last_date'] = $cond['srh_last_date'];
        }

        if (empty($where)) {
            return array();
        }

        return array(
            'cond' => 'WHERE ' . implode(' AND ', $where),
            'types' => $types,
            'data' => $data,
        );
    }

    /**
     * データ数を取得します
     * @return type
     */
    public function count($cond = null)
    {
        $ret = $this->make_condition($cond);

        $sql = "SELECT count(*) FROM duty_shift_approval JOIN empmst e USING (emp_id) JOIN empmst ap ON ap.emp_id=approval_emp_id ";
        $sql.= $ret['cond'];

        $sth = $this->db->prepare($sql, array_merge(array('integer', 'text'), $ret['types']), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array_merge(array('emp_id' => $this->emp_id), $ret['data']));
        if (PEAR::isError($res)) {
            cmx_log("ROLLBACK: count SELECT ERROR: {$ret['cond']} " . $res->getDebugInfo());
            throw new Exception("count SELECT ERROR: {$ret['cond']} " . $res->getDebugInfo());
        }
        $count = $res->fetchOne();
        $sth->free();
        return $count;
    }

    /**
     * 一覧の取得
     * @param type $st
     * @param type $pageCount
     * @param type $cond
     * @return type
     */
    public function lists($st = null, $pageCount = null, $cond = null)
    {
        $ret = $this->make_condition($cond);

        $sql = "
            SELECT
                e.emp_personal_id,
                e.emp_lt_nm || ' ' || e.emp_ft_nm AS emp_name,
                year,
                month,
                approval_type,
                approval_date,
                ap.emp_personal_id as approval_personal_id,
                ap.emp_lt_nm || ' ' || ap.emp_ft_nm AS approval_emp_name,
                id
            FROM duty_shift_approval
            JOIN empmst e USING (emp_id)
            JOIN empmst ap ON ap.emp_id=approval_emp_id
        ";

        $sql .= $ret['cond'];
        $sql .= " ORDER BY approval_date DESC";

        if (isset($st) && isset($pageCount)) {
            $this->db->setLimit($pageCount, $st);
        }

        $sth = $this->db->prepare($sql, $ret['types'], MDB2_PREPARE_RESULT);
        $res = $sth->execute($ret['data']);
        if (PEAR::isError($res)) {
            cmx_log("ROLLBACK: lists SELECT ERROR: {$ret['cond']} " . $res->getDebugInfo());
            throw new Exception("lists SELECT ERROR: {$ret['cond']} " . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 承認データを取得
     * @param type $id
     * @return type
     */
    public function fetch_approval_by_id($id)
    {
        $data = $this->db->extended->getRow("SELECT group_id, year, month FROM duty_shift_approval_group WHERE id = ?", null, array($id), array('integer'));
        return $data;
    }

    /**
     * 月次承認情報取得
     * @param type $year
     * @param type $month
     */
    public function fetch_month_approval($year, $month)
    {
        $sel_sth = $this->db->prepare("
            SELECT
            g.group_id,
            group_name,
            manager,
            me.emp_lt_nm || ' ' || me.emp_ft_nm AS manager_emp_name,
            approval_emp_id,
            approval_type,
            emp.emp_lt_nm || ' ' || emp.emp_ft_nm AS approval_emp_name,
            approval_date
            FROM duty_shift_group AS g
            LEFT JOIN duty_shift_approval_group AS ag ON ag.group_id = g.group_id AND year = ? AND month = ?
            LEFT JOIN empmst AS emp ON emp_id=approval_emp_id
            LEFT JOIN empmst AS me ON me.emp_id=manager
            ORDER BY manager;
        ", array('integer', 'integer'), MDB2_PREPARE_RESULT);

        $res = $sel_sth->execute(array($year, $month));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            cmx_log("ROLLBACK: fetch_month_approval SELECT ERROR: {$year}, {$month} " . $res->getDebugInfo());
            throw new Exception("fetch_month_approval SELECT ERROR: {$year}, {$month} " . $res->getDebugInfo());
        }
        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            if ($row['approval_type'] === '999') {
                $row['boss_approval_flg'] = true;
                $row['boss_approval_date'] = $row['approval_date'];
                $row['boss_approval_emp_name'] = $row['approval_emp_name'];
            }
            if ($row['approval_type'] === '1') {
                $row['manager_approval_flg'] = true;
                $row['manager_approval_date'] = $row['approval_date'];
                $row['manager_approval_emp_name'] = $row['approval_emp_name'];
            }

            if (empty($lists[$row['manager']][$row['group_id']])) {
                $lists[$row['manager']][$row['group_id']] = $row;
            }
            else {
                $lists[$row['manager']][$row['group_id']] = array_merge($lists[$row['manager']][$row['group_id']], $row);
            }
        }
        return $lists;
    }

    // 日付更新
    function update_approval_date($id, $approval_date)
    {
        // UPDATE
        $sth = $this->db->prepare("UPDATE duty_shift_approval SET approval_date = ? WHERE id = ?", array('date', 'integer'), MDB2_PREPARE_MANIP);
        $res = $sth->execute(array($approval_date, $id));
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log("ROLLBACK: update_approval_date: {$id} {$approval_date} " . $res->getDebugInfo());
            return false;
        }
        return true;
    }

}
