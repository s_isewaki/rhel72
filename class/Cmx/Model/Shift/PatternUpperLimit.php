<?php

//
// 勤務表：連続勤務シフト上限クラス
//
//
require_once('Cmx.php');
require_once('Cmx/Core/Model.php');

class Cmx_Shift_PatternUpperLimit extends Model
{

    var $_list;
    var $_group_id;

    // コンストラクタ
    function Cmx_Shift_PatternUpperLimit($row = null)
    {
        parent::connectDB();
        $this->_list = array();

        if (is_array($row)) {
            $this->_init($row);
        }
        else if (!is_null($row)) {
            $this->_group_id = $row;
        }
    }

    // _init
    function _init($row)
    {
        $this->_row = $row;
    }

    // リスト
    function lists()
    {
        if (array_key_exists($this->_group_id, $this->_list)) {
            return $this->_list[$this->_group_id];
        }

        $sth = $this->db->prepare("
            SELECT * FROM duty_shift_group_upper_limit WHERE group_id= ? ORDER BY no
        ");
        $res = $sth->execute($this->_group_id);

        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            array_push($list, $row);
        }
        $this->_list[$this->_group_id] = $list;
        return $list;
    }

}
