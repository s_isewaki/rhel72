<?php

require_once('Cmx.php');
require_once('Cmx/Core/Model.php');

/**
 * ��̳ɽ�����𥹥��å� Ĺ���ٲ�
 */
class Cmx_Shift_StaffHoliday extends Model
{

    var $_group_id;
    var $_emp_id;
    var $_list;

    /**
     * ���󥹥ȥ饯��
     *
     * @param type $group_id ����ID
     * @param type $emp_id ����ID
     */
    function Cmx_Shift_StaffHoliday($group_id = null, $emp_id = null)
    {
        parent::connectDB();
        $this->_group_id = $group_id;
        $this->_emp_id = $emp_id;
        $this->_list = null;
    }

    /**
     * Ĺ���ٲ˥ꥹ��
     *
     * @return Ĺ���ٲ�
     */
    function lists()
    {
        $sth = $this->db->prepare("
            SELECT * FROM duty_shift_staff_employment_holiday WHERE emp_id = :emp_id
        ");
        $res = $sth->execute(array('emp_id' => $this->_emp_id));

        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            array_push($list, $row);
        }
        $this->_list = $list;
        return $list;
    }

    /**
     * �ǡ�������
     *
     * @param type $post POST�ǡ���
     * @return true: ���� false:����
     */
    function update($post)
    {
        $this->db->beginNestedTransaction();

        $data = array();
        for ($i = 0; $i < count($post["holiday"]); $i++) {
            $data[] = array(
                'group_id' => $post['group_id'],
                'emp_id' => $post['emp_id'],
                'atdptn_ptn_id' => $post["holiday"][$i],
                'start_date' => $post["start_date"][$i],
                'end_date' => $post["end_date"][$i],
            );
        }

        // DELETE
        $sth = $this->db->prepare("DELETE FROM duty_shift_staff_employment_holiday WHERE emp_id = ?", array('text'), MDB2_PREPARE_MANIP);
        $res = $sth->execute(array($post['emp_id']));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            return false;
        }

        // INSERT
        $sth = $this->db->prepare('
           INSERT INTO duty_shift_staff_employment_holiday
           (group_id, emp_id, atdptn_ptn_id, start_date, end_date)
           VALUES (:group_id, :emp_id, :atdptn_ptn_id, :start_date, :end_date)
           ', array('text', 'text', 'int', 'date', 'date'), MDB2_PREPARE_MANIP);
        $res = $this->db->extended->executeMultiple($sth, $data);
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            return false;
        }

        // commit
        $this->db->completeNestedTransaction();
        return true;
    }

}
