<?php

require_once('Cmx.php');
require_once('Cmx/Core/Model.php');
require_once('Cmx/Model/Shift/PatternGroup.php');
require_once('Cmx/Model/Shift/PatternCount.php');

/**
 * 勤務表：勤務パターンクラス
 */
class Cmx_Shift_Pattern extends Model
{

    var $_list;
    var $_row;
    var $_group_id;
    var $_count;

    /**
     * コンストラクタ
     * @param type $row
     */
    function Cmx_Shift_Pattern($row = null)
    {
        parent::connectDB();
        $this->_list = array();

        if (is_array($row)) {
            $this->_init($row);
        }
        else if (!is_null($row)) {
            $this->set_group_id($row);
        }
    }

    // _init
    function _init($row)
    {
        $this->_row = $row;
    }

    // set_group_id
    function set_group_id($group_id)
    {
        $this->_group_id = $group_id;
    }

    /**
     * 勤務パターングループリスト
     * @return array
     */
    function grp_lists()
    {
        $res = $this->db->query("SELECT * FROM wktmgrp ORDER BY group_id");

        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            array_push($list, new Cmx_Shift_PatternGroup($row));
        }
        return $list;
    }

    /**
     * 勤務パターンリスト
     * @return array
     */
    function lists()
    {
        if (array_key_exists($this->_group_id, $this->_list)) {
            return $this->_list[$this->_group_id];
        }

        $sth = $this->db->prepare("
            SELECT
                *,
                a.atdptn_id as atdptn_id,
                a.reason as atd_reason,
                r.reason_id as ptn_reason
            FROM atdptn a
                LEFT JOIN atdbk_reason_mst r ON ((a.atdptn_id=10 OR a.reason=CAST(reason_id AS int)) AND r.display_flag)
                LEFT JOIN duty_shift_pattern d ON (d.pattern_id=a.group_id AND d.atdptn_ptn_id=a.atdptn_id AND (d.reason=r.reason_id OR d.reason='0'))
            WHERE a.group_id= ? AND a.display_flag AND (a.atdptn_id != 10 OR d.reason IS NOT NULL OR a.reason IS NOT NULL)
            ORDER BY d.order_no, a.atdptn_order_no, r.sequence
        ");
        $res = $sth->execute(array($this->_group_id));

        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            array_push($list, new Cmx_Shift_Pattern($row));
        }
        $this->_list[$this->_group_id] = $list;
        return $list;
    }

    /**
     * 勤務パターンハッシュリスト
     * @return type
     */
    function hash()
    {
        $list = $this->lists();
        $hash = array();
        foreach ($list as $ptn) {
            $hash[$ptn->ptn_id()] = $ptn;
            if ($ptn->atd_reason()) {
                $hash[$ptn->atd_reason()] = $ptn;
            }
        }
        return $hash;
    }

    /**
     * 勤務パターンハッシュリスト(key=ptn_id_4)
     * @return type
     */
    function ptn4_hash()
    {
        $list = $this->lists();
        $hash = array();
        foreach ($list as $ptn) {
            $hash[$ptn->ptn_id_2()] = $ptn;
            $hash[$ptn->ptn_id_4()] = $ptn;
            if ($ptn->atd_reason()) {
                $hash[$ptn->atd_reason()] = $ptn;
            }
        }
        return $hash;
    }

    /**
     * 勤務パターンJSON
     * @return type
     */
    function json()
    {
        return cmx_json_encode($this->json_array());
    }

    /**
     * 勤務パターンJSONリスト
     * @return type
     */
    function json_array()
    {
        $list = $this->lists();
        $json = array();
        foreach ($list as $ptn) {
            $style = array();
            if ($ptn->font_color_id()) {
                $style['color'] = $ptn->font_color_id();
            }
            if ($ptn->back_color_id() and $ptn->back_color_id() !== '#ffffff') {
                $style['backgroundColor'] = $ptn->back_color_id();
            }
            else if ($ptn->back_color_id() === '#ffffff') {
                $style['backgroundColor'] = '';
            }
            $style['fontSize'] = $ptn->font_size();

            $json["p" . $ptn->ptn_id()] = array(
                'id' => $ptn->ptn_id(),
                'ptn_name' => $ptn->ptn_name(),
                'cell' => $ptn->font_name(),
                'style' => $style,
                'duty_connect' => $ptn->duty_connection_flag(),
            );
        }
        return $json;
    }

    /**
     * 勤務パターンatdptnリスト
     * @return array
     */
    function atdptn_lists()
    {
        $sth = $this->db->prepare("SELECT * FROM ATDPTN WHERE GROUP_ID = ? ORDER BY ATDPTN_ORDER_NO");
        $res = $sth->execute($this->_group_id);

        $atdptn_list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            array_push($atdptn_list, new Cmx_Shift_Pattern($row));
        }
        return $atdptn_list;
    }

    /**
     * 時間帯データ
     * @return arr 時間帯データ
     */
    function officehours()
    {
        $sth = $this->db->prepare("
            SELECT * FROM officehours WHERE tmcd_group_id= ?
        ");
        $res = $sth->execute($this->_group_id);

        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[$row['pattern']][$row['officehours_id']] = $row;
        }
        return $list;
    }

    /**
     * 勤務記号リスト
     * @return type
     */
    function sign_lists()
    {
        $list = $this->lists();
        $reason = array();
        foreach ($list as $ptn) {
            if ($ptn->atd_reason()) {
                $reason[$ptn->atd_reason()] = 1;
            }
        }
        $sign_lists = array();
        foreach ($list as $ptn) {
            if ($ptn->atdptn_id() != "10" or ! $reason[$ptn->ptn_id()]) {
                $sign_lists[] = $ptn;
            }
        }
        return $sign_lists;
    }

    // 更新
    function update($post)
    {
        $this->db->beginTransaction();

        // DELETE
        $sth = $this->db->prepare("DELETE FROM DUTY_SHIFT_PATTERN WHERE PATTERN_ID = ?", array('integer'), MDB2_PREPARE_MANIP);
        $res = $sth->execute($_POST['pattern_id']);
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_Pattern: update: DELETE FROM DUTY_SHIFT_PATTERN: ' . $res->getDebugInfo());
            return false;
        }

        for ($i = 0; $i < count($post['atdptn_id']); $i++) {
            $data[] = array(
                $_POST['pattern_id'],
                $_POST['atdptn_id'][$i],
                empty($_POST['reason'][$i]) ? 0 : $_POST['reason'][$i],
                $_POST['font_color_id'][$i],
                $_POST['back_color_id'][$i],
                $i + 1,
                $_POST['allotment_order_no'][$i],
                $_POST['font_name'][$i],
                $_POST['sfc_code'][$i],
            );
        }

        $sql = "INSERT INTO DUTY_SHIFT_PATTERN (";
        $sql.="            PATTERN_ID";             //1  int
        $sql.="          , ATDPTN_PTN_ID";          //2  int
        $sql.="          , REASON";                 //3  var
        $sql.="          , FONT_COLOR_ID";          //6  var
        $sql.="          , BACK_COLOR_ID";          //7  var
        $sql.="          , ORDER_NO";               //8  int
        $sql.="          , ALLOTMENT_ORDER_NO";     //9  int
        $sql.="          , FONT_NAME";              //10 var
        $sql.="          , SFC_CODE";               //11 var
        $sql.="   ) VALUES (?,?,?,?,?,?,?,?,?)";

        $sth = $this->db->prepare(
            $sql, array('integer', 'integer', 'text', 'text', 'text', 'integer', 'integer', 'text', 'text'), MDB2_PREPARE_MANIP
        );
        $res = $this->db->extended->executeMultiple($sth, $data);

        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_Pattern: update: INSERT INTO DUTY_SHIFT_PATTERN: ' . $res->getDebugInfo());
            return false;
        }
        else {
            $this->db->commit();
            return true;
        }
    }

    /**
     * 勤務表集計オブジェクト
     * @return type
     */
    function _count()
    {
        if (!empty($this->_count)) {
            return;
        }
        $this->_count = new Cmx_Shift_PatternCount($this->_group_id);
    }

    /**
     * 勤務パターン職種フィルタ
     */
    function count_job_filter()
    {
        $this->_count();
        return $this->_count->job_filter();
    }

    /**
     * 勤務パターン集計行
     */
    function count_row_lists()
    {
        $this->_count();
        return $this->_count->count_row_lists();
    }

    /**
     * 勤務パターン集計列
     */
    function count_col_lists()
    {
        $this->_count();
        return $this->_count->count_col_lists();
    }

    /**
     * 勤務パターン集計値
     */
    function count_values()
    {
        $this->_count();
        return $this->_count->values();
    }

    /**
     * 勤務パターンID
     * @return type
     */
    function ptn_id()
    {
        if ($this->_row["atdptn_id"] !== "10") {
            return $this->_row["atdptn_id"];
        }
        else {
            return sprintf("10%02d", $this->_row["ptn_reason"]);
        }
    }

    /**
     * 勤務パターンID 4桁(reasonなし)
     * @return type
     */
    function ptn_id_2()
    {
        return sprintf("%02d00", $this->_row["atdptn_id"]);
    }

    /**
     * 勤務パターンID 4桁
     * @return type
     */
    function ptn_id_4()
    {
        return sprintf("%02d%02d", $this->_row["atdptn_id"], $this->_row["ptn_reason"]);
    }

    /**
     * 勤務パターンID 4桁
     * @return type
     */
    function ptn_id_4b()
    {
        if ($this->_row["atdptn_id"] !== "10") {
            return sprintf("%02d00", $this->_row["atdptn_id"]);
        }
        else {
            return sprintf("10%02d", $this->_row["ptn_reason"]);
        }
    }

    /**
     * 勤務パターン名
     * @return type
     */
    function ptn_name()
    {
        if ($this->_row["atdptn_id"] !== "10") {
            return $this->_row["atdptn_nm"];
        }
        else if (!empty($this->_row["display_name"])) {
            return $this->_row["display_name"];
        }
        else {
            return $this->_row["default_name"];
        }
    }

    /**
     * 記号のフォントサイズ
     * @return string
     */
    function font_size()
    {
        if (mb_strwidth($this->font_name(), 'eucJP-win') >= 3) {
            return '10px';
        }
        else if (mb_strwidth($this->font_name(), 'eucJP-win') > 6) {
            return '8px';
        }
        else {
            return '13px';
        }
    }

    /**
     * 記号のスタイル
     * @return string
     */
    function style()
    {
        $style = "";
        if ($this->font_color_id()) {
            $style .= "color:" . $this->font_color_id() . ";";
        }
        if ($this->back_color_id() and $this->back_color_id() !== '#ffffff') {
            $style .= "background-color:" . $this->back_color_id() . ";";
        }
        $style .= "font-size:" . $this->font_size() . ";";
        return $style;
    }

    /**
     * 当直連動記号のデータを取得
     * @return ハッシュデータ
     */
    function night_duty_connect()
    {
        $sth = $this->db->prepare("SELECT atdptn_id FROM atdptn WHERE group_id= ? AND display_flag AND duty_connection_flag=1");
        $res = $sth->execute($this->_group_id);

        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[$row['atdptn_id']] = 1;
        }
        return $list;
    }

    /**
     * 記号文字色リスト
     * @return array
     */
    function font_color_list()
    {
        return array(
            array('name' => '黒', 'color' => '#000000'),
            array('name' => '白', 'color' => '#ffffff'),
            array('name' => '赤', 'color' => '#ff0000'),
            array('name' => '青', 'color' => '#0000ff'),
            array('name' => '黄', 'color' => '#ffff00'),
            array('name' => '緑', 'color' => '#00ff00'),
        );
    }

    /**
     * 記号背景色リスト
     * @return array
     */
    function back_color_list()
    {
        return array(
            array('name' => '白', 'color' => '#ffffff'),
            array('name' => '黒', 'color' => '#000000'),
            array('name' => '淡い赤', 'color' => '#ff8080'),
            array('name' => '淡い青', 'color' => '#80ffff'),
            array('name' => '淡い黄', 'color' => '#ffff80'),
            array('name' => '淡い緑', 'color' => '#80ff80'),
            array('name' => 'ピンク', 'color' => '#ff80ff'),
            array('name' => '赤', 'color' => '#ff0000'),
            array('name' => '青', 'color' => '#0000ff'),
            array('name' => '黄', 'color' => '#ffff00'),
            array('name' => '緑', 'color' => '#00ff00'),
        );
    }

    /**
     * atdptn.reason
     * @return null
     */
    function atd_reason()
    {
        if (!$this->_row['atd_reason']) {
            return null;
        }
        return sprintf("10%02d", $this->_row['atd_reason']);
    }

    // duty_shift_pattern
    function pattern_id()
    {
        return $this->_getter(__FUNCTION__);
    }

    /**
     * 法定外残業の計算
     * @return type
     */
    function base_time()
    {
        return ((int)substr($this->_row['base_time'], 0, 2) * 60) + (int)substr($this->_row['base_time'], 2, 2);
    }

    function atdptn_ptn_id()
    {
        return $this->_getter(__FUNCTION__);
    }

    function count_kbn_gyo()
    {
        return $this->_getter(__FUNCTION__);
    }

    function count_kbn_retu()
    {
        return $this->_getter(__FUNCTION__);
    }

    function font_color_id()
    {
        return $this->_getter(__FUNCTION__);
    }

    function back_color_id()
    {
        return $this->_getter(__FUNCTION__);
    }

    function font_name()
    {
        return $this->_getter(__FUNCTION__);
    }

    function sfc_code()
    {
        return $this->_getter(__FUNCTION__);
    }

    function order_no()
    {
        return $this->_getter(__FUNCTION__);
    }

    function allotment_order_no()
    {
        return $this->_getter(__FUNCTION__);
    }

    // atdptn
    function atdptn_id()
    {
        return $this->_getter(__FUNCTION__);
    }

    function atdptn_nm()
    {
        return $this->_getter(__FUNCTION__);
    }

    function group_id()
    {
        return $this->_getter(__FUNCTION__);
    }

    function workday_count()
    {
        return $this->_getter(__FUNCTION__);
    }

    function previous_day_possible_flag()
    {
        return $this->_getter(__FUNCTION__);
    }

    function display_flag()
    {
        return $this->_getter(__FUNCTION__);
    }

    function allowance()
    {
        return $this->_getter(__FUNCTION__);
    }

    function previous_day_flag()
    {
        return $this->_getter(__FUNCTION__);
    }

    function meeting_start_time()
    {
        return $this->_getter(__FUNCTION__);
    }

    function meeting_end_time()
    {
        return $this->_getter(__FUNCTION__);
    }

    function assist_time_flag()
    {
        return $this->_getter(__FUNCTION__);
    }

    function assist_group_id()
    {
        return $this->_getter(__FUNCTION__);
    }

    function assist_start_time()
    {
        return $this->_getter(__FUNCTION__);
    }

    function assist_end_time()
    {
        return $this->_getter(__FUNCTION__);
    }

    function over_24hour_flag()
    {
        return $this->_getter(__FUNCTION__);
    }

    function atdptn_order_no()
    {
        return $this->_getter(__FUNCTION__);
    }

    function auto_clock_in_flag()
    {
        return $this->_getter(__FUNCTION__);
    }

    function auto_clock_out_flag()
    {
        return $this->_getter(__FUNCTION__);
    }

    function out_time_nosubtract_flag()
    {
        return $this->_getter(__FUNCTION__);
    }

    function after_night_duty_flag()
    {
        return $this->_getter(__FUNCTION__);
    }

    function duty_connection_flag()
    {
        return $this->_getter(__FUNCTION__);
    }

    function no_count_flag()
    {
        return $this->_getter(__FUNCTION__);
    }

    // atdbk_reason_mst
    function default_name()
    {
        return $this->_getter(__FUNCTION__);
    }

    function display_name()
    {
        return $this->_getter(__FUNCTION__);
    }

    function day_count()
    {
        return $this->_getter(__FUNCTION__);
    }

    function recital()
    {
        return $this->_getter(__FUNCTION__);
    }

    function kind_flag()
    {
        return $this->_getter(__FUNCTION__);
    }

    function holiday_count()
    {
        return $this->_getter(__FUNCTION__);
    }

    function ptn_reason()
    {
        return $this->_getter(__FUNCTION__);
    }

}
