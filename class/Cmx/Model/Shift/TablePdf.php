<?php

require_once('Cmx.php');
require_once('fpdf153/mbfpdf.php');

/**
 * 勤務表：勤務表帳票クラス
 *
 * 勤務割当表の帳票を生成するクラス
 *
 */
class Cmx_Shift_TablePDF extends MBFPDF
{

    var $MOST_LEFT = 12;
    var $MOST_TOP = 5;
    var $MOST_BTM = 279;
    var $data;

    /**
     * コンストラクタ
     */
    function Cmx_Shift_TablePDF()
    {
        parent::MBFPDF('L', 'mm', 'A3');
        $this->AddMBFont(GOTHIC, "EUC-JP");
        $this->SetMargins($this->MOST_LEFT, $this->MOST_TOP);
        $this->SetFillColor(210);
    }

    /**
     * 開始日をセット
     * @param str $data
     */
    function set_start_date($data)
    {
        $this->data['start_date'] = $data;
    }

    /**
     * 終了日をセット
     * @param str $data
     */
    function set_end_date($data)
    {
        $this->data['end_date'] = $data;
    }

    /**
     * 担当者をセット
     * @param str $data
     */
    function set_admin_name($data)
    {
        $this->data['admin'] = $data;
    }

    /**
     * 部署名をセット
     * @param str $data
     */
    function set_group_name($data)
    {
        $this->data['group'] = $data;
    }

    /**
     * 年月をセット
     * @param str $year
     * @param str $month
     */
    function set_ym($year, $month)
    {
        $this->data['year'] = $year;
        $this->data['month'] = $month;
    }

    /**
     * カレンダーをセット
     * @param str $data
     */
    function set_calendar($data)
    {
        $this->data['cal'] = $data;
    }

    /**
     * 集計行をセット
     * @param str $data
     */
    function set_row_list($data)
    {
        $this->data['row_list'] = $data;
    }

    /**
     * 集計列をセット
     * @param str $data
     */
    function set_col_list($data)
    {
        $this->data['col_list'] = $data;
    }

    /**
     * 要稼働時間をセット
     * @param str $data
     */
    function set_whours($data)
    {
        $this->data['whours'] = $data;
    }

    /**
     * パターン(hash)をセット
     * @param str $data
     */
    function set_pattern($data)
    {
        $this->data['pattern'] = $data;
    }

    /**
     * 部署IDをセット
     * @param str $data
     */
    function set_group_id($data)
    {
        $this->data['group_id'] = $data;
    }

    /**
     * ヘッダ
     */
    function Header()
    {

        $h1 = 5;
        $w1 = 8;
        $w2 = 10;

        // 印鑑欄
        $this->SetFont(GOTHIC, '', 8);
        $this->Cell(340, 3, '', 0, 0, 'C');
        $this->Cell(30, 3, '　 部 長 　', 0, 0, 'C');
        $this->Cell(30, 3, '所 属 長', 0, 1, 'C');
        $this->Cell(340, 3, '', 0, 0, 'C');
        $this->Cell(30, 15, '印', 0, 0, 'C');
        $this->Cell(30, 15, '印', 0, 1, 'C');

        // 部署
        $this->SetFont(GOTHIC, '', 12);
        $this->Cell(70, 10, $this->data['group'], 0, 0, 'L');

        // 年月
        $this->SetFont(GOTHIC, '', 16);
        $this->Cell(30, 10, $this->data['year'] . '/' . $this->data['month'], 0, 0, 'L');

        // 期間
        $this->SetFont(GOTHIC, '', 12);
        $this->Cell(70, 10, $this->data['start_date'] . ' - ' . $this->data['end_date'], 0, 0, 'L');

        // タイトル
        $this->SetFont(GOTHIC, '', 16);
        $this->Cell(120, 10, '看護師勤務割当表', 0, 0, 'L');

        // 管理者
        $this->SetFont(GOTHIC, '', 12);
        $this->Cell(70, 10, '管理者:' . $this->data['admin'], 0, 1, 'L');

        // 一覧ヘッダ
        $this->SetFont(GOTHIC, '', 9);
        $this->Cell($w1, $h1, '', 'LTR', 0, 'C');
        $this->Cell(20, $h1, '職位', 'LTR', 0, 'L');
        $this->Cell(28, $h1, '氏名', 'LTR', 0, 'L');
        $this->Cell($w2, $h1, '', 'LTR', 0, 'C');
        $this->Cell($w2, $h1, '日付', 1, 0, 'C');

        foreach ($this->data['cal'] as $date => $d) {
            $this->bgcolor($d['type'], $d['w']);
            $this->Cell($w1, $h1, $d['d'], 1, 0, 'C', 1);
        }

        $this->Cell($w2, $h1, '前月', 'LTR', 0, 'C');
        $this->Cell($w2, $h1, '当月', 'LTR', 0, 'C');
        $this->Cell($w2, $h1, '実働', 'LTR', 0, 'C');

        foreach ($this->data['row_list'] as $idx => $row_list) {
            $this->Cell($w2, $h1, $row_list['name'], 'LTR', 0, 'C');
        }
        $this->Cell(1, $h1, '', 0, 1, 'C');

        // ２段目
        $this->Cell($w1, $h1, '', 'LBR', 0, 'C');
        $this->Cell(20, $h1, '職種', 'LBR', 0, 'L');
        $this->Cell(28, $h1, '', 'LBR', 0, 'L');
        $this->Cell($w2, $h1, '', 'LBR', 0, 'C');
        $this->Cell($w2, $h1, '曜日', 1, 0, 'C');

        foreach ($this->data['cal'] as $date => $d) {
            $this->bgcolor($d['type'], $d['w']);
            $this->Cell($w1, $h1, $d['wj'], 1, 0, 'C', 1);
        }

        $this->Cell($w2, $h1, '繰越', 'LBR', 0, 'C');
        $this->Cell($w2, $h1, '実働', 'LBR', 0, 'C');
        $this->Cell($w2, $h1, '差', 'LBR', 0, 'C');

        foreach ($this->data['row_list'] as $idx => $row_list) {
            $this->Cell($w2, $h1, '', 'LBR', 0, 'C');
        }
        $this->Cell(1, $h1, '', 0, 1, 'C');
    }

    /**
     * 背景色
     * @param type $type
     * @param type $w
     */
    function bgcolor($type, $w)
    {
        //買取日・日曜・祝日
        if ($type == '8' or $type == '3') {
            $this->SetFillColor(245, 205, 166);
        }
        else {
            //土曜
            if ($w == '6') {
                $this->SetFillColor(157, 204, 224);
            }
            //日曜
            else if ($w == '0') {
                $this->SetFillColor(245, 205, 166);
            }
            //平日
            else {
                $this->SetFillColor(240, 240, 240);
            }
        }
    }

    /**
     * フッタ
     */
    function Footer()
    {
        $this->Ln(3);
        $this->SetFont(GOTHIC, '', 8);
        $this->Cell(0, 5, '出力日時: ' . date('Y-m-d H:i:s'), 0, 1, 'R');
    }

    /**
     * PDF生成
     */
    function Pdf_create($Data, $Ptn)
    {
        $h1 = 5;
        $h2 = 1;
        $w1 = 8;
        $w2 = 10;

        $idx_night = 0;
        $idx_day2 = 0;
        $idx_middle = 0;

        $cnt_nurse = 0;
        $cnt_nurse165 = 0;
        $cnt_staff = 0;
        $cnt_night = 0;
        $cnt_day2 = 0;
        $cnt_middle = 0;
        $cnt_holiday = 0;
        $cnt_workhours = 0;
        $cnt_nighthours = 0;

        // 職員数分ループ
        foreach ($Data['emp'] as $emp_id => $emp) {

            if (!$emp['name']) {
                continue;
            }

            $cnt++;

            //一行目[予定]
            $this->SetFont(GOTHIC, '', 8);
            $this->Cell($w1, $h1, strval($cnt), 'LTR', 0, 'R');
            $this->Cell(20, $h1, $emp['st_name'], 'LTR', 0, 'L');
            $this->Cell(28, $h1, $emp['name'], 'LTR', 0, 'L');
            $this->Cell($w2, $h1, '', 'LTR', 0, 'C');
            $this->Cell($w2, $h1, '予定', 1, 0, 'C');

            foreach ($this->data['cal'] as $date => $dates) {
                $ptn = $emp['dates']['d' . $date];
                $pattern = $ptn['plan_pattern_id'] ? $Ptn[$ptn['plan_pattern_id']]->hash() : $this->data['pattern'];
                $plan = $ptn['plan'];
                $plan_sign = ($plan && $pattern[$plan]) ? $pattern[$plan]->font_name() : "";

                //文字数確認
                if (strlen($plan_sign) > 4) {
                    $this->SetFont(GOTHIC, '', 7);
                }
                else {
                    $this->SetFont(GOTHIC, '', 9);
                }

                $this->bgcolor($dates['type'], $dates['w']);

                // 他部署勤務は表示しない
                if (($emp['assist'] === '1' and $emp['dates']['d' . $date]['assist'] !== $this->data['group_id'])
                    or ( $emp['assist'] !== '1' and ! empty($emp['dates']['d' . $date]['assist']) and $emp['dates']['d' . $date]['assist'] !== $this->data['group_id'])) {
                    $this->Cell($w1, $h1, '', 1, 0, 'C', 1);
                }
                else {
                    $this->Cell($w1, $h1, $plan_sign, 1, 0, 'C', 1);
                }

                // 休暇回数
                if (substr($plan, 0, 2) === '10') {
                    $cnt_holiday++;
                }
            }

            $this->SetFont(GOTHIC, '', 7);
            $this->Cell($w2, $h1, $emp['wh_over']['plan'], 1, 0, 'R');   //[予定]前月繰越
            $this->Cell($w2, $h1, $emp['wh_hour']['plan'], 1, 0, 'R');     //[予定]当月実働
            $this->Cell($w2, $h1, $emp['wh_diff']['plan'], 1, 0, 'R');     //[予定]実働差
            //[予定]日勤?・夜勤・中勤・週休 等
            foreach ($this->data['row_list'] as $idx => $row_list) {
                $count = $row_list['count'];
                $total = '0';
                if (isset($emp['count_row']['c' . $count]['plan'])) {
                    $total = $emp['count_row']['c' . $count]['plan'];
                }
                $this->Cell($w2, $h1, strval($total), 1, 0, 'R');

                if ($row_list['name'] === '夜勤') {
                    $idx_night = $count;
                }
                if ($row_list['name'] === '日勤II') {
                    $idx_day2 = $count;
                }
                if ($row_list['name'] === '中勤') {
                    $idx_middle = $count;
                }
            }
            $this->Cell(1, $h1, '', 0, 1, 'C');

            //二行目[実績]
            $this->SetFont(GOTHIC, '', 8);
            $this->Cell($w1, $h1, '', 'LBR', 0, 'C');
            $this->Cell(20, $h1, $emp['job_name'], 'LBR', 0, 'L');
            $this->Cell(28, $h1, '', 'LBR', 0, 'L');
            $this->Cell($w2, $h1, '', 'LBR', 0, 'C');

            $this->Cell($w2, $h1, '実績', 1, 0, 'C');

            foreach ($this->data['cal'] as $date => $dates) {
                $ptn = $emp['dates']['d' . $date];
                $pattern = $ptn['results_pattern_id'] ? $Ptn[$ptn['results_pattern_id']]->hash() : $this->data['pattern'];
                $results = $ptn['results'];
                $results_sign = ($results && $pattern[$results]) ? $pattern[$results]->font_name() : "";

                if (strlen($results_sign) > 4) {
                    $this->SetFont(GOTHIC, '', 7);
                }
                else {
                    $this->SetFont(GOTHIC, '', 9);
                }

                $this->bgcolor($dates['type'], $dates['w']);

                // 他部署勤務は表示しない
                if (($emp['assist'] === '1' and $emp['dates']['d' . $date]['assist'] !== $this->data['group_id'])
                    or ( $emp['assist'] !== '1' and ! empty($emp['dates']['d' . $date]['assist']) and $emp['dates']['d' . $date]['assist'] !== $this->data['group_id'])) {
                    $this->Cell($w1, $h1, '', 1, 0, 'C', 1);
                }
                else {
                    $this->Cell($w1, $h1, $results_sign, 1, 0, 'C', 1);
                }
            }

            $this->SetFont(GOTHIC, '', 7);
            $this->Cell($w2, $h1, $emp['wh_over']['results'], 1, 0, 'R');     //[実績]前月繰越
            $this->Cell($w2, $h1, $emp['wh_hour']['results'], 1, 0, 'R');     //[実績]当月実働
            $this->Cell($w2, $h1, $emp['wh_diff']['results'], 1, 0, 'R');     //[実績]実働差
            //[実績]日勤II・夜勤・中勤・週休 等
            foreach ($this->data['row_list'] as $idx => $row_list) {
                $count = $row_list['count'];
                $total = '0';
                if (isset($emp['count_row']['c' . $count]['results'])) {
                    $total = $emp['count_row']['c' . $count]['results'];
                }
                $this->Cell($w2, $h1, strval($total), 1, 0, 'R');
            }
            $this->Cell(1, $h1, '', 0, 1, 'C');

            // カウント
            if (in_array($emp['job_code'], array('0001', '0002', '0003', '0004'))) {
                // 夜勤回数
                $cnt_night += (float)$emp['count_row']['c' . $idx_night]['plan'];

                // 日勤II回数
                $cnt_day2 += (float)$emp['count_row']['c' . $idx_day2]['plan'];

                // 中勤回数
                $cnt_middle += (float)$emp['count_row']['c' . $idx_middle]['plan'];
            }

            // 職員数
            $cnt_staff++;

            // 実働(予定)のある看護師数(保健師、助産師、看護師、准看護師)
            if ($emp['wh_hour']['plan'] !== '0.00' and in_array($emp['job_code'], array('0001', '0002', '0003', '0004'))) {
                $cnt_nurse++;

                // 実働時間
                $cnt_workhours += (float)$emp['wh_hour']['plan'];

                // 夜勤16.5時間以上の看護師
                if ((float)$emp['wh_night']['plan'] > 16.5) {
                    $cnt_nurse165++;

                    // 夜勤時間
                    $cnt_nighthours += (float)$emp['wh_night']['plan'];
                }
            }
        }

        $this->Cell($w1 + 20 + 28, $h2, '', 'LTB', 0, 'L');
        $this->Cell($w2, $h2, '', 'TB', 0, 'C');
        $this->Cell($w2, $h2, '', 'TB', 0, 'C');

        foreach ($this->data['cal'] as $date => $dates) {
            $this->bgcolor($dates['type'], $dates['w']);
            $this->Cell($w1, $h2, '', 1, 0, 'C', 1);
        }

        $this->Cell($w2, $h2, '', 'TB', 0, 'C');
        $this->Cell($w2, $h2, '', 'TB', 0, 'C');
        $this->Cell($w2, $h2, '', 'TB', 0, 'C');

        foreach ($this->data['row_list'] as $idx => $row_list) {
            $this->Cell($w2, $h2, '', 1, 0, 'C');
        }
        $this->Cell(1, $h2, '', 0, 1, 'C');

        // 集計列
        foreach ($this->data['col_list'] as $idx => $col_list) {
            $name = $col_list['name'];
            $count = $col_list['count'];

            $this->Cell($w1 + 20, $h1, $name, 1, 0, 'L');
            $this->Cell(28, $h1, '', 1, 0, 'L');
            $this->Cell($w2, $h1, '', 1, 0, 'C');
            $this->Cell($w2, $h1, '', 1, 0, 'C');

            foreach ($this->data['cal'] as $date => $dates) {
                $this->bgcolor($dates['type'], $dates['w']);
                $total = '0';
                if (isset($Data[date]['d' . $date]['count_col']['c' . $count]['plan'])) {
                    $total = $Data[date]['d' . $date]['count_col']['c' . $count]['plan'];
                }
                $this->Cell($w1, $h1, strval($total), 1, 0, 'R', 1);
            }
            $this->Cell($w2, $h1, '', 1, 0, 'C');
            $this->Cell($w2, $h1, '', 1, 0, 'C');
            $this->Cell($w2, $h1, '', 1, 0, 'C');

            foreach ($this->data['row_list'] as $idx => $row_list) {
                $this->Cell($w2, $h1, '', 1, 0, 'C');
            }
            $this->Cell(1, $h1, '', 0, 1, 'C');
        }

        //---------------------------------------------------------------
        // 平均
        //---------------------------------------------------------------
        $this->SetFont(GOTHIC, '', 10);
        $foot_format = '＜要稼働時間＞ %.2f     ＜平均回数＞ 夜勤 ＝ %.2f  日勤II = %.2f  中勤 = %.2f  休暇 = %.2f     ＜平均時間＞ 実働 = %.2f  夜勤 = %.2f';
        $foot = sprintf($foot_format, $this->data['whours']['working_hours'] / 60, ($cnt_nurse !== 0 ? $cnt_night / $cnt_nurse : 0), ($cnt_nurse !== 0 ? $cnt_day2 / $cnt_nurse : 0), ($cnt_nurse !== 0 ? $cnt_middle / $cnt_nurse : 0), ($cnt_staff !== 0 ? $cnt_holiday / $cnt_staff : 0), ($cnt_nurse !== 0 ? $cnt_workhours / $cnt_nurse : 0), ($cnt_nurse165 !== 0 ? $cnt_nighthours / $cnt_nurse165 : 0)
        );
        $this->Ln(3);
        $this->Cell(0, 6, $foot, 0, 1, 'L');
        $this->Cell(0, 6, "夜勤回数: $cnt_night  日勤II回数: $cnt_day2  中勤回数: $cnt_middle  実働合計: $cnt_workhours  夜勤合計: $cnt_nighthours", 0, 1, 'L');
        $this->Cell(0, 6, "職員数: $cnt_staff  看護師数: $cnt_nurse  夜勤16.5時間以上の看護師数: $cnt_nurse165", 0, 1, 'L');
    }

}
