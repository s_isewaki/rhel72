<?php

require_once('Cmx.php');
require_once('Cmx/Core/Model.php');

/**
 * 勤務表：部署・参照可能な職員クラス
 *
 * 勤務表の部署へ参照可能な職員のクラス
 *
 */
class Cmx_Shift_WardAccessPermission extends Model
{

    var $_group_id;

    // コンストラクタ
    function Cmx_Shift_WardAccessPermission($group_id)
    {
        parent::connectDB();
        $this->_group_id = $group_id;
    }

    // リスト
    function lists()
    {
        $sth = $this->db->prepare("
            SELECT * FROM duty_shift_group_access_permission d JOIN duty_shift_group USING (group_id) WHERE group_id= ? ORDER BY id
        ");
        $res = $sth->execute(array($this->_group_id));

        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            array_push($list, $row);
        }
        return $list;
    }

    // リスト
    function access_lists($emp_id)
    {
        $sth = $this->db->prepare("
            SELECT group_id FROM duty_shift_group_access_permission d
            JOIN duty_shift_group USING (group_id)
            JOIN (SELECT emp_class, emp_attribute, emp_dept, emp_room, emp_job, emp_st FROM empmst WHERE emp_id= :emp_id
            UNION SELECT emp_class, emp_attribute, emp_dept, emp_room, null,    emp_st FROM concurrent WHERE emp_id= :emp_id) e
            ON (
                (d.class_id IS NULL OR d.class_id=e.emp_class)
                AND (d.atrb_id IS NULL OR d.atrb_id=e.emp_attribute)
                AND (d.dept_id IS NULL OR d.dept_id=e.emp_dept)
                AND (d.room_id IS NULL OR d.room_id=e.emp_room)
                AND (d.job_id  IS NULL OR d.job_id =e.emp_job)
                AND (d.st_id   IS NULL OR d.st_id  =e.emp_st)
            )
        ");
        $res = $sth->execute(array('emp_id' => $emp_id));

        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            array_push($list, new Cmx_Shift_Ward($row['group_id']));
        }
        return $list;
    }

    /**
     * データ更新
     *
     * @param type $post POSTデータ
     * @return true: 成功 false:失敗
     */
    function update($post)
    {
        $this->db->beginNestedTransaction();

        $data = array();
        for ($i = 0; $i < count($post["access_class"]); $i++) {
            $data[] = array(
                'group_id' => $post['group_id'],
                'class_id' => $post['access_class'][$i],
                'atrb_id' => $post['access_atrb'][$i],
                'dept_id' => $post['access_dept'][$i],
                'room_id' => $post['access_room'][$i],
                'job_id' => $post['access_job'][$i],
                'st_id' => $post['access_status'][$i],
            );
        }

        // DELETE
        $sth = $this->db->prepare(
            "DELETE FROM duty_shift_group_access_permission WHERE group_id = ?", array('text'), MDB2_PREPARE_MANIP);
        $res = $sth->execute(array($post['group_id']));
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_WardAccessPermission: update: DELETE FROM duty_shift_group_access_permission: ' . $res->getDebugInfo());
            return false;
        }

        // INSERT
        $sth = $this->db->prepare(
            '
            INSERT INTO duty_shift_group_access_permission
            (group_id, class_id, atrb_id, dept_id, room_id, job_id, st_id)
            VALUES (:group_id, :class_id, :atrb_id, :dept_id, :room_id, :job_id, :st_id)
           ', array('text', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer'), MDB2_PREPARE_MANIP);
        $res = $this->db->extended->executeMultiple($sth, $data);
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_WardAccessPermission: update: INSERT INTO duty_shift_group_access_permission: ' . $res->getDebugInfo());
            return false;
        }

        // commit
        $this->db->commit();
        return true;
    }

}
