<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'Cmx/Model/Calendar.php';
require_once 'Cmx/Model/Shift/Pattern.php';
require_once 'Cmx/Model/Shift/TablePdf.php';
require_once 'Cmx/Model/Shift/WorkingHours.php';
require_once 'Cmx/Model/Shift/Approval.php';
require_once 'Cmx/Model/Timecard/Overtime.php';
require_once 'Cmx/Model/Timecard/OvertimeReason.php';
require_once 'Cmx/Shift/Utils.php';
require_once 'Cmx/Shift/Log.php';
require_once 'Cmx/Model/Shift/TablePdf.php';

/**
 * 勤務表：勤務表クラス
 *
 * 部署の勤務予定、実績、希望、当直、コメントのデータを取得、更新を行うクラス
 *
 */
class Cmx_Shift_Table extends Model
{

    var $_shift_admin;
    var $_ward;
    var $_group_id;
    var $_pattern;
    var $_year;
    var $_month;
    var $_staff;
    var $_dates;
    var $_tbl;
    var $_staff_in;
    var $_start_ymd;
    var $_end_ymd;
    var $_ptn;
    var $_login_id;
    var $log;

    /**
     * コンストラクタ
     */
    function Cmx_Shift_Table()
    {
        parent::connectDB();
        $this->_staff_in = array();
        $this->log = new Cmx_Shift_Log('table');
    }

    /**
     * ログインIDのセット
     */
    function set_login_id($login_id)
    {
        $this->_login_id = $login_id;
    }

    /**
     * シフト管理者フラグのセット
     */
    function set_shift_admin($shift_admin)
    {
        $this->_shift_admin = $shift_admin;
    }

    /**
     * 部署のセット
     */
    function set_ward($ward)
    {
        $this->_ward = $ward;
        $this->_group_id = $ward->group_id();
        $this->_pattern = $ward->pattern();
        $this->_ptn[$ward->pattern_id()] = $ward->pattern();
    }

    /**
     * 職員のセット
     */
    function set_staff($staff)
    {
        $this->_staff = $staff;
    }

    /**
     * 年のセット
     */
    function set_year($year)
    {
        $this->_year = $year;
    }

    /**
     * 月のセット
     */
    function set_month($month)
    {
        $this->_month = $month;
    }

    /**
     * 開始日
     */
    function start_ymd()
    {
        if (!$this->_start_ymd and $this->_ward and $this->_year and $this->_month) {
            $this->set_start_ymd($this->_ward->start_ymd($this->_year, $this->_month));
        }
        return $this->_start_ymd;
    }

    function set_start_ymd($start_ymd)
    {
        $this->_start_ymd = $start_ymd;
    }

    /**
     * 終了日
     */
    function end_ymd()
    {
        if (!$this->_end_ymd and $this->_ward and $this->_year and $this->_month) {
            $this->set_end_ymd($this->_ward->end_ymd($this->_year, $this->_month));
        }
        return $this->_end_ymd;
    }

    function set_end_ymd($end_ymd)
    {
        $this->_end_ymd = $end_ymd;
    }

    /**
     * 日付リスト
     */
    function dates()
    {
        if (!$this->_dates and $this->_ward and $this->_year and $this->_month) {
            $this->set_dates($this->_ward->dates($this->_year, $this->_month));
        }
        return $this->_dates;
    }

    function set_dates($dates)
    {
        $this->_dates = $dates;
    }

    /**
     * 勤務表データのセット
     */
    function set_table($data)
    {
        $this->_tbl = $data;
    }

    /**
     * 勤務表データのゲット
     */
    function get_table()
    {
        return $this->_tbl;
    }

    /**
     * 勤務表データ(パターン)のゲット
     */
    function get_pattern()
    {
        return $this->_ptn;
    }

    /**
     * 勤務表データ(職員情報)のゲット
     */
    function get_table_emp()
    {
        if ($this->_tbl['emp']) {
            return $this->_tbl['emp'];
        }
        return array();
    }

    /**
     * 勤務表データJSON出力
     */
    function table_json()
    {
        return cmx_json_encode($this->_tbl);
    }

    /**
     * 勤務表テーブルの取得
     */
    function table()
    {
        $staff_list = $this->_staff->lists($this->_group_id, $this->_year, $this->_month);
        if (empty($staff_list)) {
            return array();
        }

        // 日付
        $start_date = $this->start_ymd();
        $end_date = $this->end_ymd();
        $dates = $this->dates();

        // 開始日時点での職員履歴データ
        $history = $this->_staff->history($this->_ward->start_date($this->_year, $this->_month));

        // 残業
        $ovtm = new Cmx_Timecard_Overtime();
        $ovtmrsn = new Cmx_Timecard_OvertimeReason();

        $this->_tbl = array();

        // スタッフ
        $staff_array = array();
        foreach ($staff_list as $row) {
            $staff_array[] = sprintf("'%s'", $row->emp_id());
            $this->_tbl['emp']["e" . $row->emp_id()] = array(
                'name' => $row->emp_name(),
                'kana' => $row->emp_kana(),
                'login' => $row->emp_login_id(),
                'sex' => $row->emp_sex(),
                'job_id' => $row->job_id(),
                'job_name' => $row->job_name(),
                'job_code' => $row->job_code(),
                'st_id' => $row->st_id(),
                'st_name' => $row->st_name(),
                'st_code' => $row->st_code(),
                'class_id' => $row->class_id(),
                'class_name' => $row->class_name(),
                'class_code' => $row->class_code(),
                'atrb_id' => $row->atrb_id(),
                'atrb_name' => $row->atrb_name(),
                'atrb_code' => $row->atrb_code(),
                'dept_id' => $row->dept_id(),
                'dept_name' => $row->dept_name(),
                'dept_code' => $row->dept_code(),
                'room_id' => $row->room_id(),
                'room_name' => $row->room_name(),
                'room_code' => $row->room_code(),
                'team' => $row->team_name(),
                'team_id' => $row->team_id(),
                'carryover' => $row->last_carryover_min(),
                'assist' => $row->assist(),
                'join_date' => $row->join_date(),
                'retire_date' => $row->retire_date(),
            );
            // 履歴があればマージ
            if (isset($history[$row->emp_id()])) {
                $this->_tbl['emp']["e" . $row->emp_id()] = array_merge($this->_tbl['emp']["e" . $row->emp_id()], $history[$row->emp_id()]);
            }
        }
        $staff_in = implode(',', $staff_array);

        $diff_group_ids = array();

        // 希望+応援
        $sth = $this->db->prepare("
            SELECT
                e.emp_id,
                c.date,
                CASE WHEN p.pattern='10' THEN p.pattern||LPAD(p.reason,2,'0') ELSE p.pattern END AS plan,
                p.night_duty AS duty,
                p.tmcd_group_id AS plan_pattern_id,
                CASE WHEN d.atdptn_ptn_id=10 THEN cast(d.atdptn_ptn_id as text)||LPAD(d.reason,2,'0') ELSE cast(d.atdptn_ptn_id as text) END AS draft,
                d.pattern_id AS draft_pattern_id,
                a.main_group_id as assist
            FROM empmst e
            JOIN calendar c ON (date BETWEEN :start_date AND :end_date)
            LEFT JOIN atdbk p ON (p.emp_id=e.emp_id AND p.date=c.date)
            LEFT JOIN duty_shift_plan_draft d ON (d.emp_id=e.emp_id AND d.duty_date=c.date)
            LEFT JOIN duty_shift_plan_assist a ON (a.emp_id=e.emp_id AND a.duty_date=c.date)
            WHERE e.emp_id IN ($staff_in)
        ");
        $res = $sth->execute(array(
            'start_date' => $start_date,
            'end_date' => $end_date,
        ));

        $plan_lists = array();
        $is_plan = false;
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $plan_lists[] = $row;

            // 下書き or 確定 ?
            // 応援勤務は判定の対象外
            if ($row['plan'] and ! $this->is_assist($this->_tbl['emp']["e{$row["emp_id"]}"]['assist'], $row['assist'])) {
                $is_plan = true;
            }
        }

        $is_draft = false;
        foreach ($plan_lists as $row) {
            $pattern = '';
            $pattern_id = '';

            // 管理者は下書きを見れる
            if ($this->_shift_admin) {
                // 確定
                if ($is_plan) {
                    $pattern = $row['plan'];
                    $pattern_id = $row['plan_pattern_id'];
                }
                // 下書き
                else {
                    $pattern = $row['draft'];
                    $pattern_id = $row['draft_pattern_id'];
                    if ($pattern) {
                        $is_draft = true;
                    }
                }
                // 応援
                if ($this->is_assist($this->_tbl['emp']["e{$row["emp_id"]}"]['assist'], $row['assist'])) {
                    $pattern = $row['plan'] ? $row['plan'] : $row['draft'];
                    $pattern_id = $row['plan'] ? $row['plan_pattern_id'] : $row['draft_pattern_id'];
                }
            }
            else {
                $pattern = $row['plan'];
                $pattern_id = $row['plan_pattern_id'];
            }

            if ($pattern) {
                $this->_tbl['emp']["e{$row["emp_id"]}"]['dates']["d{$row["date"]}"]["plan"] = $pattern;

                // 勤務パターングループが異なる場合
                if ($pattern_id !== $this->_ward->pattern_id()) {
                    $this->_tbl['emp']["e{$row["emp_id"]}"]['dates']["d{$row["date"]}"]["plan_pattern_id"] = $pattern_id;
                    $diff_group_ids[$pattern_id] = 1;
                }
            }

            // 当直
            $this->_tbl['emp']["e{$row["emp_id"]}"]['dates']["d{$row["date"]}"]["duty"] = $row["duty"];

            // 応援
            if ($row["assist"] and $row["assist"] !== 'null') {
                $this->_tbl['emp']["e{$row["emp_id"]}"]['dates']["d{$row["date"]}"]["assist"] = $row["assist"];
            }
        }

        // 確定予定
        if ($is_plan) {
            $this->_tbl['display_plan'] = true;
        }
        else if ($is_draft) {
            $this->_tbl['display_draft'] = true;
        }

        // 実績
        $sth = $this->db->prepare("
            SELECT
                *,
                emp_id,
                date,
                CASE WHEN ptn.pattern='10' THEN ptn.pattern||LPAD(ptn.reason,2,'0') ELSE ptn.pattern END AS pattern,
                night_duty AS duty,
                over_start_time AS over_start_time1,
                over_end_time AS over_end_time1,
                over_start_next_day_flag AS over_start_next_day_flag1,
                over_end_next_day_flag AS over_end_next_day_flag1,
                ovtm_reason_id AS ovtm_reason_id1,
                ovtm_reason AS ovtm_reason1
            FROM atdbkrslt ptn
            LEFT JOIN atdbkrslt_ovtmrsn USING (emp_id,date)
            WHERE emp_id IN ($staff_in) AND date BETWEEN :start_date AND :end_date
            ORDER BY emp_id, date
        ");
        $res = $sth->execute(array('group_id' => $this->_group_id, 'start_date' => prev_ymd($start_date), 'end_date' => $end_date));

        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $this->_tbl['emp']["e{$row["emp_id"]}"]['dates']["d{$row["date"]}"]["results"] = $row["pattern"];
            $this->_tbl['emp']["e{$row["emp_id"]}"]['dates']["d{$row["date"]}"]["results_duty"] = $row["duty"];

            // 勤務パターングループが異なる場合
            if ($row["tmcd_group_id"] !== $this->_ward->pattern_id()) {
                $this->_tbl['emp']["e{$row["emp_id"]}"]['dates']["d{$row["date"]}"]["results_pattern_id"] = $row["tmcd_group_id"];
                $diff_group_ids[$row["tmcd_group_id"]] = 1;
            }

            // 休憩時間
            $resttime = restMinutes($row['rest_start_time'], $row['rest_end_time']);
            $this->_tbl['emp']["e{$row["emp_id"]}"]['dates']["d{$row["date"]}"]["resttime"] = $resttime;

            // 勤務時間
            $worktime = workMinutes($row['start_time'], $row['end_time'], $row['next_day_flag'], $resttime);
            $this->_tbl['emp']["e{$row["emp_id"]}"]['dates']["d{$row["date"]}"]["worktime"] = $worktime;

            // (当日)残業時間合計
            $time = 0;
            $reason = '';
            foreach (range(1, 5) as $i) {
                if ($row["over_start_time{$i}"]) {
                    list($over_min1, $over_min2) = $ovtm->overtime($row["over_start_time{$i}"], $row["over_start_next_day_flag{$i}"], $row["over_end_time{$i}"], $row["over_end_next_day_flag{$i}"], $row["rest_start_time{$i}"], $row["rest_start_next_day_flag{$i}"], $row["rest_end_time{$i}"], $row["rest_end_next_day_flag{$i}"]);
                    $time += $over_min1 + $over_min2;

                    $overreason = $ovtmrsn->reason($row["ovtm_reason_id{$i}"], $row["ovtm_reason{$i}"]);
                    if ($overreason) {
                        $reason = $overreason;
                    }
                }
            }
            if ($time) {
                $this->_tbl['emp']["e{$row["emp_id"]}"]['dates']["d{$row["date"]}"]["results_ovtm"] += $time;
                if ($reason) {
                    $this->_tbl['emp']["e{$row["emp_id"]}"]['dates']["d{$row["date"]}"]["results_ovtmrsn"] = mb_substr($reason, 0, 4, 'EUC-JP');
                }
            }

            // (翌日)残業時間合計
            $time2 = 0;
            $reason2 = '';
            foreach (range(1, 5) as $i) {
                if ($row["over_start_time{$i}"] && $row["over_end_next_day_flag{$i}"]) {
                    list($over_min1, $over_min2) = $ovtm->overtime_nextday($row["over_start_time{$i}"], $row["over_start_next_day_flag{$i}"], $row["over_end_time{$i}"], $row["over_end_next_day_flag{$i}"], $row["rest_start_time{$i}"], $row["rest_start_next_day_flag{$i}"], $row["rest_end_time{$i}"], $row["rest_end_next_day_flag{$i}"]);
                    $time2 += $over_min1 + $over_min2;

                    $overreason = $ovtmrsn->reason($row["ovtm_reason_id{$i}"], $row["ovtm_reason{$i}"]);
                    if ($overreason) {
                        $reason2 = $overreason;
                    }
                }
            }
            if ($time2) {
                $next_ymd = next_ymd($row["date"]);
                $this->_tbl['emp']["e{$row["emp_id"]}"]['dates']["d{$next_ymd}"]["results_ovtm"] = $time2;
                if ($reason2) {
                    $this->_tbl['emp']["e{$row["emp_id"]}"]['dates']["d{$next_ymd}"]["results_ovtmrsn"] = mb_substr($reason2, 0, 4, 'EUC-JP');
                }
            }
        }

        // 希望
        $sth = $this->db->prepare("
            SELECT
                emp_id,
                duty_date as date,
                CASE WHEN ptn.atdptn_ptn_id=10 THEN CAST(ptn.atdptn_ptn_id as text)||LPAD(ptn.reason,2,'0') ELSE CAST(ptn.atdptn_ptn_id as text) END AS pattern
            FROM duty_shift_plan_individual ptn
            WHERE emp_id IN ($staff_in) AND duty_date BETWEEN :start_date AND :end_date
        ");
        $res = $sth->execute(array('start_date' => $start_date, 'end_date' => $end_date));
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $this->_tbl['emp']["e{$row["emp_id"]}"]['dates']["d{$row["date"]}"]["hope"] = $row["pattern"];
        }

        // 月次承認
        $sth_approval = $this->db->prepare("SELECT emp_id FROM duty_shift_approval WHERE year = :year AND month = :month AND approval_type = 0");
        $res_approval = $sth_approval->execute(array(
            'year' => $this->_year,
            'month' => $this->_month,
        ));
        while ($row = $res_approval->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            if (!empty($this->_tbl['emp']["e{$row["emp_id"]}"])) {
                $this->_tbl['emp']["e{$row["emp_id"]}"]['approved'] = 1;
            }
        }

        // 長期休暇
        // 勤務希望にセットする
        $sth = $this->db->prepare("
            SELECT
                emp_id,
                atdptn_ptn_id,
                to_char(start_date,'YYYYMMDD') as start_date,
                to_char(end_date, 'YYYYMMDD') as end_date
            FROM duty_shift_staff_employment_holiday
            WHERE emp_id IN ($staff_in) AND start_date <= to_date( :end_date,'YYYYMMDD') AND to_date( :start_date,'YYYYMMDD') <= end_date
        ");
        $res = $sth->execute(array('start_date' => $start_date, 'end_date' => $end_date));
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            foreach ($dates as $d) {
                if ($row['start_date'] <= $d['date'] and $d['date'] <= $row['end_date']) {
                    if (substr($row["atdptn_ptn_id"], 0, 2) !== '10') {
                        $row["atdptn_ptn_id"] = (int)substr($row["atdptn_ptn_id"], 0, 2);
                    }
                    $this->_tbl['emp']["e{$row["emp_id"]}"]['dates']["d{$d['date']}"]["hope"] = $row["atdptn_ptn_id"];
                }
            }
        }

        // コメント
        $sth = $this->db->prepare("
            SELECT
                emp_id,
                duty_date as date,
                comment
            FROM duty_shift_plan_comment
            WHERE emp_id IN ($staff_in) AND duty_date BETWEEN :start_date AND :end_date
        ");
        $res = $sth->execute(array('start_date' => $start_date, 'end_date' => $end_date));
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $this->_tbl['emp']["e{$row["emp_id"]}"]['dates']["d{$row["date"]}"]["comment"] = $row["comment"];
        }

        // 異なる勤務パターン
        foreach (array_keys($diff_group_ids) as $ptn_id) {
            $ptn = new Cmx_Shift_Pattern($ptn_id);
            $this->_tbl['pattern']['ptn' . $ptn_id] = $ptn->json_array();
            $this->_ptn[$ptn_id] = $ptn;
        }
    }

    /**
     * 年度ごとの勤務表テーブルの取得
     */
    function year_table($year, $staffs)
    {
        // 日付
        $calendar = array();

        // パターン
        $pattern[$this->_ward->pattern_id()] = $this->_pattern->hash();

        // スタッフ
        $staff_array = array();
        foreach ($staffs as $staff) {
            $staff_array[] = sprintf("'%s'", $staff->emp_id());
        }
        $staff_in = implode(',', $staff_array);

        // 実績
        $sth = $this->db->prepare("
            SELECT
                emp_id,
                date,
                DATE_PART('month',TO_DATE(date,'YYYYMMDD')) AS m,
                DATE_PART('day',TO_DATE(date,'YYYYMMDD')) AS d,
                base_shorter_minutes,
                CASE WHEN ptn.pattern='10' THEN ptn.pattern||LPAD(ptn.reason,2,'0') ELSE ptn.pattern END AS pattern,
                tmcd_group_id
            FROM atdbkrslt ptn
            WHERE emp_id in ({$staff_in}) AND date BETWEEN :start_date AND :end_date
        ");
        $res = $sth->execute(array('start_date' => $year . '0401', 'end_date' => $year + 1 . '0331'));

        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            if (!empty($row['pattern'])) {
                if (!empty($pattern[$row['tmcd_group_id']])) {
                    $row['sign'] = $pattern[$row['tmcd_group_id']][$row['pattern']]->font_name();
                }
                else {
                    $ptn = new Cmx_Shift_Pattern($row['tmcd_group_id']);
                    $pattern[$row['tmcd_group_id']] = $ptn->hash();
                    $row['sign'] = $pattern[$row['tmcd_group_id']][$row['pattern']]->font_name();
                }

                if (mb_strwidth($row['sign'], 'eucJP-win') > 6) {
                    $row['class'] = 'S';
                }
                else if (mb_strwidth($row['sign'], 'eucJP-win') >= 3) {
                    $row['class'] = 'M';
                }
                else {
                    $row['class'] = 'L';
                }
            }
            $calendar[$row['emp_id']][$row['m']][$row['d']] = $row;
        }

        return $calendar;
    }

    /**
     * 月ごとの勤務表テーブルの取得
     */
    function month_table($year, $month)
    {
        // 月単位のスタッフ一覧
        $staff_list = $this->month_staff_lists($year, $month);
        if (empty($staff_list)) {
            return array();
        }

        // 開始日時点での職員履歴データ
        $start_date = sprintf('%04d-%02d-01', $year, $month);
        $history = $this->_staff->history($start_date);

        // 残業
        $ovtm = new Cmx_Timecard_Overtime();
        $ovtmrsn = new Cmx_Timecard_OvertimeReason();

        $this->_tbl = array();

        // スタッフ
        $staff_array = array();
        foreach ($staff_list as $row) {
            $staff_array[] = sprintf("'%s'", $row->emp_id());
            $this->_tbl['emp']["e" . $row->emp_id()] = array(
                'name' => $row->emp_name(),
                'kana' => $row->emp_kana(),
                'login' => $row->emp_login_id(),
                'sex' => $row->emp_sex(),
                'job_id' => $row->job_id(),
                'job_name' => $row->job_name(),
                'job_code' => $row->job_code(),
                'st_id' => $row->st_id(),
                'st_name' => $row->st_name(),
                'st_code' => $row->st_code(),
                'class_id' => $row->class_id(),
                'class_name' => $row->class_name(),
                'class_code' => $row->class_code(),
                'atrb_id' => $row->atrb_id(),
                'atrb_name' => $row->atrb_name(),
                'atrb_code' => $row->atrb_code(),
                'dept_id' => $row->dept_id(),
                'dept_name' => $row->dept_name(),
                'dept_code' => $row->dept_code(),
                'room_id' => $row->room_id(),
                'room_name' => $row->room_name(),
                'room_code' => $row->room_code(),
                'team' => $row->team_name(),
                'team_id' => $row->team_id(),
                'carryover' => $row->last_carryover_min(),
                'assist' => $row->assist(),
                'join_date' => $row->join_date(),
                'retire_date' => $row->retire_date(),
            );
            // 履歴があればマージ
            if (isset($history[$row->emp_id()])) {
                $this->_tbl['emp']["e" . $row->emp_id()] = array_merge($this->_tbl['emp']["e" . $row->emp_id()], $history[$row->emp_id()]);
            }
        }
        $staff_in = implode(',', $staff_array);

        $diff_group_ids = array();

        // 予定
        $sth = $this->db->prepare("
                SELECT
                    emp_id,
                    date,
                    CASE WHEN ptn.pattern='10' THEN ptn.pattern||LPAD(ptn.reason,2,'0') ELSE ptn.pattern END AS pattern,
                    night_duty AS duty,
                    tmcd_group_id
                FROM atdbk ptn
                WHERE emp_id IN ($staff_in) AND date BETWEEN :start_date AND :end_date
            ");
        $res = $sth->execute(array('start_date' => $this->start_ymd(), 'end_date' => $this->end_ymd()));
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $this->_tbl['emp']["e{$row["emp_id"]}"]['dates']["d{$row["date"]}"]["plan"] = $row["pattern"];
            $this->_tbl['emp']["e{$row["emp_id"]}"]['dates']["d{$row["date"]}"]["duty"] = $row["duty"];

            // 勤務パターングループが異なる場合
            if ($row["tmcd_group_id"] !== $this->_ward->pattern_id()) {
                $this->_tbl['emp']["e{$row["emp_id"]}"]['dates']["d{$row["date"]}"]["plan_pattern_id"] = $row["tmcd_group_id"];
                $diff_group_ids[$row["tmcd_group_id"]] = 1;
            }
        }

        // 実績
        $sth = $this->db->prepare("
            SELECT
                *,
                emp_id,
                date,
                CASE WHEN ptn.pattern='10' THEN ptn.pattern||LPAD(ptn.reason,2,'0') ELSE ptn.pattern END AS pattern,
                night_duty AS duty,
                over_start_time AS over_start_time1,
                over_end_time AS over_end_time1,
                over_start_next_day_flag AS over_start_next_day_flag1,
                over_end_next_day_flag AS over_end_next_day_flag1,
                ovtm_reason_id AS ovtm_reason_id1,
                ovtm_reason AS ovtm_reason1
            FROM atdbkrslt ptn
            LEFT JOIN atdbkrslt_ovtmrsn USING (emp_id,date)
            WHERE emp_id IN ($staff_in) AND date BETWEEN :start_date AND :end_date
        ");
        $res = $sth->execute(array('start_date' => prev_ymd($this->start_ymd()), 'end_date' => $this->end_ymd()));
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $this->_tbl['emp']["e{$row["emp_id"]}"]['dates']["d{$row["date"]}"]["results"] = $row["pattern"];
            $this->_tbl['emp']["e{$row["emp_id"]}"]['dates']["d{$row["date"]}"]["results_duty"] = $row["duty"];

            // 勤務パターングループが異なる場合
            if ($row["tmcd_group_id"] !== $this->_ward->pattern_id()) {
                $this->_tbl['emp']["e{$row["emp_id"]}"]['dates']["d{$row["date"]}"]["results_pattern_id"] = $row["tmcd_group_id"];
                $diff_group_ids[$row["tmcd_group_id"]] = 1;
            }

            // 休憩時間
            $resttime = restMinutes($row['rest_start_time'], $row['rest_end_time']);
            $this->_tbl['emp']["e{$row["emp_id"]}"]['dates']["d{$row["date"]}"]["resttime"] = $resttime;

            // 勤務時間
            $worktime = workMinutes($row['start_time'], $row['end_time'], $row['next_day_flag'], $resttime);
            $this->_tbl['emp']["e{$row["emp_id"]}"]['dates']["d{$row["date"]}"]["worktime"] = $worktime;

            // (当日)残業時間合計
            $time = 0;
            $reason = '';
            foreach (range(1, 5) as $i) {
                if ($row["over_start_time{$i}"]) {
                    list($over_min1, $over_min2) = $ovtm->overtime($row["over_start_time{$i}"], $row["over_start_next_day_flag{$i}"], $row["over_end_time{$i}"], $row["over_end_next_day_flag{$i}"], $row["rest_start_time{$i}"], $row["rest_start_next_day_flag{$i}"], $row["rest_end_time{$i}"], $row["rest_end_next_day_flag{$i}"]);
                    $time += $over_min1 + $over_min2;

                    $overreason = $ovtmrsn->reason($row["ovtm_reason_id{$i}"], $row["ovtm_reason{$i}"]);
                    if ($overreason) {
                        $reason = $overreason;
                    }
                }
            }
            if ($time) {
                $this->_tbl['emp']["e{$row["emp_id"]}"]['dates']["d{$row["date"]}"]["results_ovtm"] += $time;
                if ($reason) {
                    $this->_tbl['emp']["e{$row["emp_id"]}"]['dates']["d{$row["date"]}"]["results_ovtmrsn"] = mb_substr($reason, 0, 4, 'EUC-JP');
                }
            }

            // (翌日)残業時間合計
            $time2 = 0;
            $reason2 = '';
            foreach (range(1, 5) as $i) {
                if ($row["over_start_time{$i}"] && $row["over_end_next_day_flag{$i}"]) {
                    list($over_min1, $over_min2) = $ovtm->overtime_nextday($row["over_start_time{$i}"], $row["over_start_next_day_flag{$i}"], $row["over_end_time{$i}"], $row["over_end_next_day_flag{$i}"], $row["rest_start_time{$i}"], $row["rest_start_next_day_flag{$i}"], $row["rest_end_time{$i}"], $row["rest_end_next_day_flag{$i}"]);
                    $time2 += $over_min1 + $over_min2;

                    $overreason = $ovtmrsn->reason($row["ovtm_reason_id{$i}"], $row["ovtm_reason{$i}"]);
                    if ($overreason) {
                        $reason2 = $overreason;
                    }
                }
            }
            if ($time2) {
                $next_ymd = next_ymd($row["date"]);
                $this->_tbl['emp']["e{$row["emp_id"]}"]['dates']["d{$next_ymd}"]["results_ovtm"] = $time2;
                if ($reason2) {
                    $this->_tbl['emp']["e{$row["emp_id"]}"]['dates']["d{$next_ymd}"]["results_ovtmrsn"] = mb_substr($reason2, 0, 4, 'EUC-JP');
                }
            }
        }

        // 応援
        $sth = $this->db->prepare("
            SELECT
                emp_id,
                duty_date as date,
                main_group_id as assist,
                pattern_id
            FROM duty_shift_plan_assist a
            LEFT JOIN duty_shift_group g ON (a.main_group_id=g.group_id)
            WHERE emp_id IN ($staff_in)
              AND duty_date BETWEEN :start_date AND :end_date
        ");
        $res = $sth->execute(array('group_id' => $this->_group_id, 'start_date' => $this->start_ymd(), 'end_date' => $this->end_ymd()));
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $this->_tbl['emp']["e{$row["emp_id"]}"]['dates']["d{$row["date"]}"]["assist"] = $row["assist"];

            // 勤務パターングループが異なる場合
            if ($row["pattern_id"] !== $this->_ward->pattern_id()) {
                $diff_group_ids[$row["tmcd_group_id"]] = 1;
            }
        }

        // 月次承認
        $sth_approval = $this->db->prepare("SELECT emp_id FROM duty_shift_approval WHERE year = :year AND month = :month AND approval_type = 0");
        $res_approval = $sth_approval->execute(array(
            'year' => $this->_year,
            'month' => $this->_month,
        ));
        while ($row = $res_approval->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            if (!empty($this->_tbl['emp']["e{$row["emp_id"]}"])) {
                $this->_tbl['emp']["e{$row["emp_id"]}"]['approved'] = 1;
            }
        }

        // 異なる勤務パターン
        foreach (array_keys($diff_group_ids) as $ptn_id) {
            $ptn = new Cmx_Shift_Pattern($ptn_id);
            $this->_tbl['pattern']['ptn' . $ptn_id] = $ptn->json_array();
            $this->_ptn[$ptn_id] = $ptn;
        }
    }

    /**
     * 期間の勤務表テーブルの取得
     * (集計表帳票用途)
     */
    function range_table($year, $month, $length)
    {
        $cal = new Cmx_Calendar();

        if ($length > 1) {
            $stime = mktime(0, 0, 0, $month - $length + 1, 1, $year);
            $syear = date('Y', $stime);
            $smonth = date('m', $stime);
            $staff_list = $this->_staff->range_lists($this->_group_id, $syear, $smonth, $year, $month);
            $this->set_start_ymd(date('Ymd', strtotime("$syear-$smonth-1")));
            $this->set_end_ymd(date('Ymt', strtotime("$year-$month-1")));
            $this->set_dates($cal->lists(date('Ymd', strtotime("$syear-$smonth-1")), date('Ymt', strtotime("$year-$month-1"))));
        }
        else {
            $staff_list = $this->_staff->range_lists($this->_group_id, $year, $month);
            $this->set_start_ymd(date('Ymd', strtotime("$year-$month-1")));
            $this->set_end_ymd(date('Ymt', strtotime("$year-$month-1")));
            $this->set_dates($cal->lists(date('Ymd', strtotime("$year-$month-1")), date('Ymt', strtotime("$year-$month-1"))));
        }

        // 月単位のスタッフ一覧
        if (empty($staff_list)) {
            return array();
        }

        $this->_tbl = array();

        // スタッフ
        $staff_array = array();
        foreach ($staff_list as $row) {
            $staff_array[] = sprintf("'%s'", $row->emp_id());
            $this->_tbl['emp']["e" . $row->emp_id()] = array(
                'name' => $row->emp_name(),
                'kana' => $row->emp_kana(),
                'login' => $row->emp_login_id(),
                'sex' => $row->emp_sex(),
                'job_id' => $row->job_id(),
                'job_name' => $row->job_name(),
                'job_code' => $row->job_code(),
                'st_id' => $row->st_id(),
                'st_name' => $row->st_name(),
                'st_code' => $row->st_code(),
                'class_id' => $row->class_id(),
                'class_name' => $row->class_name(),
                'class_code' => $row->class_code(),
                'atrb_id' => $row->atrb_id(),
                'atrb_name' => $row->atrb_name(),
                'atrb_code' => $row->atrb_code(),
                'dept_id' => $row->dept_id(),
                'dept_name' => $row->dept_name(),
                'dept_code' => $row->dept_code(),
                'room_id' => $row->room_id(),
                'room_name' => $row->room_name(),
                'room_code' => $row->room_code(),
                'team' => $row->team_name(),
                'team_id' => $row->team_id(),
                'carryover' => $row->last_carryover_min(),
                'assist' => $row->assist(),
                'join_date' => $row->join_date(),
                'retire_date' => $row->retire_date(),
            );
        }
        $staff_in = implode(',', $staff_array);

        $diff_group_ids = array();

        // 予定
        $sth = $this->db->prepare("
                SELECT
                    emp_id,
                    date,
                    CASE WHEN ptn.pattern='10' THEN ptn.pattern||LPAD(ptn.reason,2,'0') ELSE ptn.pattern END AS pattern,
                    night_duty AS duty,
                    tmcd_group_id
                FROM atdbk ptn
                WHERE emp_id IN ($staff_in) AND date BETWEEN :start_date AND :end_date
            ");
        $res = $sth->execute(array('start_date' => $this->start_ymd(), 'end_date' => $this->end_ymd()));
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $this->_tbl['emp']["e{$row["emp_id"]}"]['dates']["d{$row["date"]}"]["plan"] = $row["pattern"];
            $this->_tbl['emp']["e{$row["emp_id"]}"]['dates']["d{$row["date"]}"]["duty"] = $row["duty"];

            // 勤務パターングループが異なる場合
            if ($row["tmcd_group_id"] !== $this->_ward->pattern_id()) {
                $this->_tbl['emp']["e{$row["emp_id"]}"]['dates']["d{$row["date"]}"]["plan_pattern_id"] = $row["tmcd_group_id"];
                $diff_group_ids[$row["tmcd_group_id"]] = 1;
            }
        }

        // 実績
        $sth = $this->db->prepare("
            SELECT
                *,
                emp_id,
                date,
                CASE WHEN ptn.pattern='10' THEN ptn.pattern||LPAD(ptn.reason,2,'0') ELSE ptn.pattern END AS pattern,
                night_duty AS duty,
                over_start_time AS over_start_time1,
                over_end_time AS over_end_time1,
                over_start_next_day_flag AS over_start_next_day_flag1,
                over_end_next_day_flag AS over_end_next_day_flag1,
                ovtm_reason_id AS ovtm_reason_id1,
                ovtm_reason AS ovtm_reason1
            FROM atdbkrslt ptn
            LEFT JOIN atdbkrslt_ovtmrsn USING (emp_id,date)
            WHERE emp_id IN ($staff_in) AND date BETWEEN :start_date AND :end_date
        ");
        $res = $sth->execute(array('start_date' => $this->start_ymd(), 'end_date' => $this->end_ymd()));
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $this->_tbl['emp']["e{$row["emp_id"]}"]['dates']["d{$row["date"]}"]["results"] = $row["pattern"];
            $this->_tbl['emp']["e{$row["emp_id"]}"]['dates']["d{$row["date"]}"]["results_duty"] = $row["duty"];

            // 勤務パターングループが異なる場合
            if ($row["tmcd_group_id"] !== $this->_ward->pattern_id()) {
                $this->_tbl['emp']["e{$row["emp_id"]}"]['dates']["d{$row["date"]}"]["results_pattern_id"] = $row["tmcd_group_id"];
                $diff_group_ids[$row["tmcd_group_id"]] = 1;
            }
        }

        // 異なる勤務パターン
        foreach (array_keys($diff_group_ids) as $ptn_id) {
            $ptn = new Cmx_Shift_Pattern($ptn_id);
            $this->_tbl['pattern']['ptn' . $ptn_id] = $ptn->json_array();
            $this->_ptn[$ptn_id] = $ptn;
        }
    }

    /**
     * 集計
     */
    function total_count()
    {
        // パターン
        $pattern[$this->_ward->pattern_id()] = $this->_pattern->hash();

        // 時間帯
        $ohours = $this->_pattern->officehours();

        // 集計値
        $cvalues = $this->_pattern->count_values();
        $job_filter = $this->_pattern->count_job_filter();

        // 開始日
        $start_date = $this->start_ymd();

        // 日付リスト
        $dates = $this->dates();

        // 列カウンタリセット
        $this->_tbl['approved'] = true;
        $this->_tbl['date'] = array();
        $this->_tbl['night_nurse_count'] = array();
        $this->_tbl['_night_hours'] = array();
        $this->_tbl['night_hours'] = array();
        $this->_tbl['night_hours_avg'] = array();

        foreach ($this->_tbl['emp'] as $emp_id => $emp) {
            // カウンタリセット
            $this->_tbl['emp'][$emp_id]['count_row'] = array();
            $this->_tbl['emp'][$emp_id]['count_duty'] = 0;
            $this->_tbl['emp'][$emp_id]['count_dutyr'] = 0;
            $this->_tbl['emp'][$emp_id]['total_worktime'] = 0;
            $this->_tbl['emp'][$emp_id]['total_overtime'] = 0;
            $wh = array();

            // 開始日時点での有休数
            list($paid_h_base, $paid_h_use, $paid_h_fuyo, $paid_h_kuri) = $this->_staff->paid_holiday(substr($emp_id, 1), $start_date);
            $paid_h['plan'] = $paid_h['hope'] = $paid_h['results'] = $paid_h_use;

            // 開始日時点での夏休数
            list($paid_s_base, $paid_s_use) = $this->_staff->paid_holiday_summer(substr($emp_id, 1), $start_date);
            $paid_s['plan'] = $paid_s['hope'] = $paid_s['results'] = $paid_s_use;

            // 開始日時点での週休数
            list($paid_w_base, $paid_w_use) = $this->_staff->paid_holiday_week(substr($emp_id, 1), $start_date);
            $paid_w['plan'] = $paid_w['hope'] = $paid_w['results'] = $paid_w_use;

            // 月次承認
            if ($this->_tbl['emp'][$emp_id]['approved'] !== 1) {
                $this->_tbl['approved'] = false;
            }

            // 集計
            foreach ($dates as $date => $cal) {
                // 入職日以前、退職日以後は集計から除外
                if (!empty($emp['join_date']) and $emp['join_date'] !== 'null' and $date < $emp['join_date'] or ! empty($emp['retire_date']) and $emp['retire_date'] !== 'null' and $emp['retire_date'] < $date) {
                    continue;
                }

                $date = 'd' . $date;
                $ptn = $this->_tbl['emp'][$emp_id]['dates'][$date];
                $assist = false;

                // 応援
                if ($this->is_assist($emp['assist'], $ptn['assist'])) {
                    $assist = true;
                }

                // 実績時間
                $this->_tbl['emp'][$emp_id]['total_worktime'] += (int)$ptn["worktime"];

                // 残業時間
                $this->_tbl['emp'][$emp_id]['total_overtime'] += (int)$ptn["results_ovtm"];

                // 予定、実績、希望
                foreach (array('plan', 'results', 'hope') as $type) {
                    // 実績が無ければ予定を使う
                    if (!$ptn[$type] && ( $type !== 'results' || $type === 'results' && !$ptn['plan'])) {
                        continue;
                    }

                    // 記号データ
                    if (empty($ptn["results_pattern_id"])) {
                        $results_pattern = $pattern[$this->_ward->pattern_id()];
                    }
                    else if (!empty($pattern[$ptn["results_pattern_id"]])) {
                        $results_pattern = $pattern[$ptn["results_pattern_id"]];
                    }
                    else {
                        $ptnobj = new Cmx_Shift_Pattern($ptn["results_pattern_id"]);
                        $pattern[$ptn["results_pattern_id"]] = $ptnobj->hash();
                        $results_pattern = $pattern[$ptn["results_pattern_id"]];
                    }
                    $p = $results_pattern[$ptn[$type]];

                    if (!$p) {
                        // 実績が無ければ予定を使う
                        if ($type === 'results') {
                            if (empty($ptn["plan_pattern_id"])) {
                                $results_pattern = $pattern[$this->_ward->pattern_id()];
                            }
                            else if (!empty($pattern[$ptn["plan_pattern_id"]])) {
                                $results_pattern = $pattern[$ptn["plan_pattern_id"]];
                            }
                            else {
                                $ptnobj = new Cmx_Shift_Pattern($ptn["plan_pattern_id"]);
                                $pattern[$ptn["plan_pattern_id"]] = $ptnobj->hash();
                                $results_pattern = $pattern[$ptn["plan_pattern_id"]];
                            }
                            $p = $results_pattern[$ptn['plan']];
                            if (!$p) {
                                continue;
                            }
                        }
                        else {
                            continue;
                        }
                    }
                    // 事由
                    $p_reason = $p->atd_reason() ? $results_pattern[$p->atd_reason()] : null;

                    // 集計
                    if ($assist === false) {
                        if (!empty($cvalues[$p->ptn_id_4()])) {
                            foreach ($cvalues[$p->ptn_id_4()] as $count_id => $value) {
                                // 行集計
                                $this->_tbl['emp'][$emp_id]['count_row']["c{$count_id}"][$type] += (float)$value;

                                // 列集計
                                if (empty($job_filter[$count_id]) or $job_filter[$count_id][$emp['job_id']]) {
                                    $this->_tbl['date'][$date]['count_col']["c{$count_id}"][$type] += (float)$value;
                                }
                            }
                        }
                    }

                    // 休暇数
                    if ($assist === false) {
                        if ($p->atdptn_ptn_id() !== '10') {
                            // 事由があれば休暇集計する
                            if ($p_reason) {
                                // 有休数
                                if ($p_reason->kind_flag() == "1") {
                                    $paid_h[$type] += (float)$p_reason->holiday_count();
                                }

                                // 週休数
                                if ($p_reason->kind_flag() == "2") {
                                    $paid_w[$type] += (float)$p_reason->holiday_count();
                                }

                                // 夏休数
                                if ($p_reason->kind_flag() == "3") {
                                    $paid_s[$type] += (float)$p_reason->holiday_count();
                                }
                            }
                        }
                        else {
                            // 有休数
                            if ($p->kind_flag() == "1") {
                                $paid_h[$type] += (float)$p->holiday_count();
                            }

                            // 週休数
                            if ($p->kind_flag() == "2") {
                                $paid_w[$type] += (float)$p->holiday_count();
                            }

                            // 夏休数
                            if ($p->kind_flag() == "3") {
                                $paid_s[$type] += (float)$p->holiday_count();
                            }
                        }
                    }

                    // 稼働時間
                    if ($type !== 'hope') {
                        switch ($cal['type']) {
                            case '8': //買取日
                                $wh['wh_holi'][$type] += $ohours[$p->atdptn_ptn_id()]['3']['workminutes'];

                                // 夜勤時間
                                $wh['wh_night'][$type] += $ohours[$p->atdptn_ptn_id()]['3']['nightduty_minutes'];
                                break;

                            case '3': //日祝
                                $wh['wh_hour'][$type] += $ohours[$p->atdptn_ptn_id()]['3']['workminutes'];

                                // 夜勤時間
                                $wh['wh_night'][$type] += $ohours[$p->atdptn_ptn_id()]['3']['nightduty_minutes'];
                                break;

                            case '2': //土曜
                                // 稼働時間 7.5時間
                                if ($p->atdptn_ptn_id() !== '10') {
                                    $wh['wh_base'][$type] += 210;
                                }
                                else if ($p->kind_flag() === "1") {
                                    // 土曜の有給の要稼働時間は 3.5h
                                    $wh['wh_base'][$type] += 210;
                                }
                                else {
                                    // 休暇の要稼働時間は 休暇日数 * 3.5h
                                    $wh['wh_base'][$type] += (float)$p->holiday_count() * 210;
                                }

                                // 週休
                                if ($p->kind_flag() === "2") {
                                    $wh['wh_base'][$type] -= (210 * (float)$p->holiday_count());
                                }
                                // 半週休
                                else if ($p_reason and $p_reason->kind_flag() === "2") {
                                    $wh['wh_base'][$type] -= (210 * (float)$p_reason->holiday_count());
                                }

                                if ($p->atdptn_ptn_id() !== '10') {
                                    $wh['wh_hour'][$type] += $ohours[$p->atdptn_ptn_id()]['2']['workminutes'];
                                }
                                else {
                                    // 休暇の実働時間は 支給換算日数 * 3.5h
                                    $wh['wh_hour'][$type] += (float)$p->day_count() * 210;
                                }

                                // 夜勤時間
                                $wh['wh_night'][$type] += $ohours[$p->atdptn_ptn_id()]['2']['nightduty_minutes'];
                                break;

                            default:  //平日
                                // 稼働時間 7.5時間
                                if ($p->atdptn_ptn_id() !== '10') {
                                    $wh['wh_base'][$type] += 450;
                                }
                                // 平日の週休2回の稼働時間は、450分
                                else if ($p->kind_flag() === "2" and $p->holiday_count() === '2.0') {
                                    $wh['wh_base'][$type] += 450;
                                }
                                else {
                                    $wh['wh_base'][$type] += (float)$p->holiday_count() * 450;
                                }

                                // 週休
                                if ($p->kind_flag() === "2") {
                                    $wh['wh_base'][$type] -= (210 * (float)$p->holiday_count());
                                }
                                // 半週休
                                else if ($p_reason and $p_reason->kind_flag() === "2") {
                                    $wh['wh_base'][$type] -= (210 * (float)$p_reason->holiday_count());
                                }

                                if ($p->atdptn_ptn_id() !== '10') {
                                    $wh['wh_hour'][$type] += $ohours[$p->atdptn_ptn_id()]['1']['workminutes'];
                                }
                                else {
                                    $wh['wh_hour'][$type] += (float)$p->day_count() * 450;
                                }

                                // 夜勤時間
                                $wh['wh_night'][$type] += $ohours[$p->atdptn_ptn_id()]['1']['nightduty_minutes'];
                                break;
                        }
                    }
                }

                // 当直
                $this->_tbl['emp'][$emp_id]['count_duty'] += intval($ptn['duty']);
                $this->_tbl['emp'][$emp_id]['count_dutyr'] += intval($ptn['results_duty']);
                $this->_tbl['date'][$date]['count_duty'] += intval($ptn['duty']);
                $this->_tbl['date'][$date]['count_dutyr'] += intval($ptn['results_duty']);
            }

            // 休暇数セット
            foreach (array('plan', 'results', 'hope') as $type) {
                $this->_tbl['emp'][$emp_id]['paid_h_all'][$type] = 0 + $paid_h_base;
                $this->_tbl['emp'][$emp_id]['paid_h_fuyo'][$type] = 0 + $paid_h_fuyo;
                $this->_tbl['emp'][$emp_id]['paid_h_kuri'][$type] = 0 + $paid_h_kuri;
                $this->_tbl['emp'][$emp_id]['paid_h_get'][$type] = 0 + $paid_h[$type];
                $this->_tbl['emp'][$emp_id]['paid_h'][$type] = $paid_h_base - $paid_h[$type];

                $this->_tbl['emp'][$emp_id]['paid_s_all'][$type] = 0 + $paid_s_base;
                $this->_tbl['emp'][$emp_id]['paid_s_get'][$type] = 0 + $paid_s[$type];
                $this->_tbl['emp'][$emp_id]['paid_s'][$type] = $paid_s_base - $paid_s[$type];

                $this->_tbl['emp'][$emp_id]['paid_w_all'][$type] = 0 + $paid_w_base;
                $this->_tbl['emp'][$emp_id]['paid_w_get'][$type] = 0 + $paid_w[$type];
                $this->_tbl['emp'][$emp_id]['paid_w'][$type] = $paid_w_base - $paid_w[$type];
            }

            // 稼働時間セット
            foreach (array('plan', 'results') as $type) {
                $this->_tbl['emp'][$emp_id]['wh_base'][$type] = sprintf('%.2f', $wh['wh_base'][$type] / 60);
                $this->_tbl['emp'][$emp_id]['wh_over'][$type] = sprintf('%.2f', $emp['carryover'] / 60);
                $this->_tbl['emp'][$emp_id]['wh_hour'][$type] = sprintf('%.2f', ($wh['wh_hour'][$type] + $emp['carryover']) / 60);
                $this->_tbl['emp'][$emp_id]['wh_diff'][$type] = sprintf('%.2f', ($wh['wh_hour'][$type] - $wh['wh_base'][$type] + $emp['carryover']) / 60);
                $this->_tbl['emp'][$emp_id]['wh_night'][$type] = sprintf('%.2f', ($wh['wh_night'][$type]) / 60);
                $this->_tbl['emp'][$emp_id]['wh_holi'][$type] = sprintf('%.2f', ($wh['wh_holi'][$type]) / 60);

                // 実働のある看護師数(保健師、助産師、看護師、准看護師)
                if ($wh['wh_hour'][$type] !== 0 and in_array($emp['job_code'], array('0001', '0002', '0003', '0004'))) {

                    // 夜勤16.5時間以上の看護師
                    if ($wh['wh_night'][$type] >= 990) {
                        $this->_tbl['night_nurse_count'][$type] ++;

                        // 夜勤時間
                        $this->_tbl['_night_hours'][$type] += $wh['wh_night'][$type];
                    }
                }
            }
        }

        // 平均時間のフォーマット
        foreach (array('plan', 'results') as $type) {
            $this->_tbl['night_hours'][$type] = sprintf('%.2f', $this->_tbl['_night_hours'][$type] / 60);
            if ($this->_tbl['night_nurse_count'][$type] and $this->_tbl['night_nurse_count'][$type] !== 0) {
                $this->_tbl['night_hours_avg'][$type] = sprintf('%.2f', ($this->_tbl['_night_hours'][$type] / $this->_tbl['night_nurse_count'][$type]) / 60);
            }
            else {
                $this->_tbl['night_hours_avg'][$type] = '0.00';
            }
        }
    }

    /**
     * 日付のハッシュリスト
     * @return 日付のハッシュリスト
     */
    function dates_hash()
    {
        $hash = array();
        foreach ($this->dates() as $date) {
            $hash['d' . $date['date']] = $date;
        }
        return $hash;
    }

    /**
     * スタッフリスト(IN句)
     * @return string
     */
    function staff_in()
    {
        $key = implode(':', array($this->_group_id, $this->_year, $this->_month));
        if ($this->_staff_in[$key]) {
            return $this->_staff_in[$key];
        }

        $staff_list = $this->_staff->lists($this->_group_id, $this->_year, $this->_month);
        if (empty($staff_list)) {
            $this->_staff_in[$key] = '';
            return '';
        }

        $staff_array = array();
        foreach ($staff_list as $row) {
            $staff_array[] = sprintf("'%s'", $row->emp_id());
        }
        $this->_staff_in[$key] = implode(', ', $staff_array);
        return $this->_staff_in[$key];
    }

    /**
     * 月単位の部署スタッフリスト
     * @param str $year
     * @param str $month
     */
    function month_staff_lists($year, $month)
    {
        // 開始日
        $start_ymd = sprintf('%04d%02d01', $year, $month);
        $start_date = sprintf('%04d-%02d-01', $year, $month);
        $this->set_start_ymd($start_ymd);

        // 終了日
        $end_ymd = sprintf('%04d%02d%02d', $year, $month, cal_days_in_month(CAL_GREGORIAN, $month, $year));
        $end_date = sprintf('%04d-%02d-%02d', $year, $month, cal_days_in_month(CAL_GREGORIAN, $month, $year));
        $this->set_end_ymd($end_ymd);

        $sdate = $this->_ward->shift_date();

        // 1クール
        list($first_y, $first_m) = $sdate->months($start_date);
        list($last_y, $last_m) = $sdate->months($end_date);
        // 2クール
        list($second_y, $second_m) = $sdate->months_add($first_y, $first_m, 1);
        // 3クール
        list($third_y, $third_m) = $sdate->months_add($first_y, $first_m, 2);

        // 日付リスト
        $cal = new Cmx_Calendar();
        $this->set_dates($cal->lists($start_date, $end_date));

        // 月単位のスタッフ一覧
        if ($last_y == $second_y and $last_m == $second_m) {
            return $this->_staff->month_lists($this->_group_id, $first_y, $first_m, $second_y, $second_m);
        }
        else {
            return $this->_staff->month_lists($this->_group_id, $first_y, $first_m, $second_y, $second_m, $third_y, $third_m);
        }
    }

    /**
     * 勤務予定保存
     * ・当直連動の記号の場合は、当直フラグも立てる
     *
     * @param $data 保存する勤務表データ
     *
     * @return boolean true:成功 false:失敗
     */
    function save_plan($data)
    {
        $this->set_table($data);

        // ds_plan_staffが無ければ、ds_staffからコピー
        if (!$this->_staff->is_plan_staff($this->_group_id, $this->_year, $this->_month)) {
            $this->_staff->master_staff_copy($this->_group_id, $this->_year, $this->_month);
        }

        // スタッフ
        $staff_in = $this->staff_in();
        if ($staff_in === '') {
            return array();
        }

        // 当直連動の記号
        $pattern = $this->_ward->pattern();
        $duty_connect = $pattern->night_duty_connect();

        // 日付
        $start_date = $this->_ward->start_ymd($this->_year, $this->_month);
        $end_date = $this->_ward->end_ymd($this->_year, $this->_month);

        // 現在の予定
        $sth = $this->db->prepare("
            SELECT
                emp_id,
                date,
                CASE WHEN ptn.pattern='10' THEN ptn.pattern||LPAD(ptn.reason,2,'0') ELSE ptn.pattern END AS pattern,
                night_duty AS duty,
                tmcd_group_id as plan_pattern_id
            FROM atdbk ptn
            WHERE emp_id IN ($staff_in) AND date BETWEEN :start_date AND :end_date
        ");
        $res = $sth->execute(array('start_date' => $start_date, 'end_date' => $end_date));
        $plan = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $plan[$row['emp_id']][$row['date']] = $row;
        }

        // トランザクション開始
        $this->db->beginTransaction();

        // 予定をセット
        $insert = array();
        $update = array();
        $delete = array();
        foreach ($data['emp'] as $emp_id_key => $emp) {
            $emp_id = substr($emp_id_key, 1);

            if (empty($emp['dates'])) {
                continue;
            }
            foreach ($emp['dates'] as $date_key => $p) {
                $date = substr($date_key, 1);

                // start_dateより過去の日付は無視
                if ($start_date > $date) {
                    continue;
                }

                $p['plan'] = ($p['plan'] === 'null') ? 0 : $p['plan'];
                $p['plan_pattern_id'] = ($p['plan_pattern_id'] === 'null') ? 0 : $p['plan_pattern_id'];

                // 応援なら保存対象外
                if ($this->is_assist($emp['assist'], $p['assist'])) {
                    continue;
                }

                // UPDATE
                if ($plan[$emp_id][$date]) {
                    if ($plan[$emp_id][$date]['pattern'] != $p['plan']) {
                        $tmcd_group_id = $p['plan_pattern_id'] ? $p['plan_pattern_id'] : $this->_ward->pattern_id();

                        // 変更前の記号は当直連動かどうか
                        if ($plan[$emp_id][$date]['plan_pattern_id'] and
                            $plan[$emp_id][$date]['plan_pattern_id'] != $tmcd_group_id) {
                            $other_pattern = new Cmx_Shift_Pattern($plan[$emp_id][$date]['plan_pattern_id']);
                            $d_connect = $other_pattern->night_duty_connect();
                            $night_duty_connect = $d_connect[$plan[$emp_id][$date]['pattern']];
                        }
                        else {
                            $night_duty_connect = $duty_connect[$plan[$emp_id][$date]['pattern']];
                        }
                        // 当直連動チェック
                        if ($night_duty_connect) {
                            $night_duty = $duty_connect[(int)substr($p['plan'], 0, 2)];
                        }
                        else {
                            $night_duty = $duty_connect[(int)substr($p['plan'], 0, 2)] ? '1' : $plan[$emp_id][$date]['duty'];
                        }

                        $set = array(
                            'emp_id' => $emp_id,
                            'date' => $date,
                            'tmcd_group_id' => $tmcd_group_id,
                            'pattern' => (int)substr($p['plan'], 0, 2),
                            'reason' => substr($p['plan'], 2, 2) ? substr($p['plan'], 2, 2) : '',
                            'night_duty' => $night_duty,
                        );
                        $update[] = $set;
                        $delete[] = array(
                            'emp_id' => $emp_id,
                            'date' => $date,
                        );
                    }
                }
                // INSERT
                else {
                    $set = array(
                        'emp_id' => $emp_id,
                        'date' => $date,
                        'tmcd_group_id' => $p['plan_pattern_id'] ? $p['plan_pattern_id'] : $this->_ward->pattern_id(),
                        'pattern' => (int)substr($p['plan'], 0, 2),
                        'reason' => substr($p['plan'], 2, 2) ? substr($p['plan'], 2, 2) : '',
                        'night_duty' => $duty_connect[(int)substr($p['plan'], 0, 2)],
                    );
                    $insert[] = $set;
                    $delete[] = array(
                        'emp_id' => $emp_id,
                        'date' => $date,
                    );
                }
            }
        }


        // 下書きを削除
        if ($delete) {
            $sth = $this->db->prepare("DELETE FROM duty_shift_plan_draft WHERE emp_id = :emp_id AND duty_date = :date", array('text', 'text'), MDB2_PREPARE_MANIP);
            $res = $this->db->extended->executeMultiple($sth, $delete);
            if (PEAR::isError($res)) {
                $this->db->rollback();
                cmx_log('ROLLBACK: Cmx_Shift_Table: save_plan: DELETE FROM duty_shift_plan_draft: ' . $res->getDebugInfo());
                return false;
            }
        }

        if ($insert) {
            $sth = $this->db->prepare('
                INSERT INTO atdbk
                (emp_id, date, pattern, reason, night_duty, tmcd_group_id)
                VALUES (:emp_id, :date, :pattern, :reason, :night_duty, :tmcd_group_id)
                ', array('text', 'text', 'text', 'text', 'text', 'integer'), MDB2_PREPARE_MANIP);
            $res = $this->db->extended->executeMultiple($sth, $insert);
            if (PEAR::isError($res)) {
                $this->db->rollback();
                cmx_log('ROLLBACK: Cmx_Shift_Table: save_plan: INSERT INTO atdbk: ' . $res->getDebugInfo());
                return false;
            }
        }
        if ($update) {
            $sth = $this->db->prepare('
                UPDATE atdbk SET
                pattern = :pattern, reason = :reason, night_duty = :night_duty, tmcd_group_id = :tmcd_group_id
                WHERE emp_id = :emp_id AND date = :date
                ', array('text', 'text', 'text', 'integer', 'text', 'text'), MDB2_PREPARE_MANIP);
            $res = $this->db->extended->executeMultiple($sth, $update);
            if (PEAR::isError($res)) {
                $this->db->rollback();
                cmx_log('ROLLBACK: Cmx_Shift_Table: save_plan: UPDATE atdbk: ' . $res->getDebugInfo());
                return false;
            }
        }

        // 応援保存
        $this->save_assist($data);

        // 表示フラグ
        $this->_tbl['display_draft'] = false;
        $this->_tbl['display_plan'] = true;

        // commit
        $this->db->commit();
        $this->log->log('勤務予定.保存', array('group' => $this->_ward->group_name(), 'year' => $this->_year, 'month' => $this->_month, 'start_date' => $start_date, 'end_date' => $end_date));

        //PDF作成
        $this->save_pdf();

        return true;
    }

    /**
     * 下書き保存
     *
     * @param $data 下書き保存する勤務表データ
     *
     * @return boolean true:成功 false:失敗
     */
    function save_draft($data)
    {
        $this->set_table($data);

        // ds_plan_staffが無ければ、ds_staffからコピー
        if (!$this->_staff->is_plan_staff($this->_group_id, $this->_year, $this->_month)) {
            $this->_staff->master_staff_copy($this->_group_id, $this->_year, $this->_month);
        }

        // スタッフ
        $staff_in = $this->staff_in();
        if ($staff_in === '') {
            return array();
        }

        // 日付
        $start_date = $this->_ward->start_ymd($this->_year, $this->_month);

        // トランザクション開始
        $this->db->beginTransaction();

        // 下書きをセット
        $pattern = array();
        $delete = array();
        foreach ($data['emp'] as $emp_id => $emp) {
            foreach ($emp['dates'] as $date_key => $p) {
                $date = substr($date_key, 1);

                // start_dateより過去の日付は無視
                if ($start_date > $date) {
                    continue;
                }

                // 応援なら保存対象外
                if ($this->is_assist($emp['assist'], $p['assist'])) {
                    continue;
                }

                $set = array(
                    'group_id' => $p['assist'] ? $p['assist'] : $this->_ward->group_id(),
                    'emp_id' => substr($emp_id, 1),
                    'duty_date' => $date,
                    'pattern_id' => $p['plan_pattern_id'] ? $p['plan_pattern_id'] : $this->_ward->pattern_id(),
                    'atdptn_ptn_id' => (int)substr($p['plan'], 0, 2),
                    'reason' => substr($p['plan'], 2, 2) ? substr($p['plan'], 2, 2) : '',
                );
                $pattern[] = $set;
                $delete[] = array(
                    'emp_id' => substr($emp_id, 1),
                    'date' => $date,
                );
            }
        }

        // DELETE
        if ($delete) {
            // 下書きを削除
            $sth = $this->db->prepare("DELETE FROM duty_shift_plan_draft WHERE emp_id = :emp_id AND duty_date = :date", array('text', 'text'), MDB2_PREPARE_MANIP);
            $res = $this->db->extended->executeMultiple($sth, $delete);
            if (PEAR::isError($res)) {
                $this->db->rollback();
                return false;
            }

            // 予定を削除
            $sth = $this->db->prepare("DELETE FROM atdbk WHERE emp_id = :emp_id AND date = :date", array('text', 'text'), MDB2_PREPARE_MANIP);
            $res = $this->db->extended->executeMultiple($sth, $delete);
            if (PEAR::isError($res)) {
                $this->db->rollback();
                return false;
            }
        }

        // INSERT
        if ($pattern) {
            $sth = $this->db->prepare('
                    INSERT INTO duty_shift_plan_draft
                    (group_id, emp_id, duty_date, pattern_id, atdptn_ptn_id, reason)
                    VALUES (:group_id, :emp_id, :duty_date, :pattern_id, :atdptn_ptn_id, :reason)
                    ', array('text', 'text', 'text', 'integer', 'integer', 'text'), MDB2_PREPARE_MANIP);
            $res = $this->db->extended->executeMultiple($sth, $pattern);
            if (PEAR::isError($res)) {
                $this->db->rollback();
                return false;
            }
        }

        // 応援保存
        $this->save_assist($data);

        // 表示フラグ
        $this->_tbl['display_draft'] = true;
        $this->_tbl['display_plan'] = false;

        // commit
        $this->db->commit();
        $this->log->log('勤務予定.下書き保存', array('group' => $this->_ward->group_name(), 'year' => $this->_year, 'month' => $this->_month));
        return true;
    }

    /**
     * 勤務実績保存
     * 勤務実績画面で「登録」ボタンを押したときに実行される
     *
     * @param $data 保存する勤務表データ
     *
     * @return boolean true:成功 false:失敗
     */
    function save_results($data)
    {
        $this->set_table($data);

        // パターン
        $pattern = $this->_pattern->hash();

        // 時間帯
        $ohours = $this->_pattern->officehours();

        // 日付
        $dates = $this->dates_hash();

        // スタッフ
        $staff_in = $this->staff_in();
        if ($staff_in === '') {
            return array();
        }

        // 日付
        $start_date = $this->_ward->start_ymd($this->_year, $this->_month);
        $end_date = $this->_ward->end_ymd($this->_year, $this->_month);

        // 現在の実績
        $sth = $this->db->prepare("
            SELECT
                emp_id,
                date,
                CASE WHEN ptn.pattern='10' THEN ptn.pattern||LPAD(ptn.reason,2,'0') ELSE ptn.pattern END AS pattern,
                night_duty AS duty
            FROM atdbkrslt ptn
            WHERE emp_id IN ($staff_in) AND date BETWEEN :start_date AND :end_date
        ");
        $res = $sth->execute(array('start_date' => $start_date, 'end_date' => $end_date));
        $results = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $results[$row['emp_id']][$row['date']] = $row;
        }

        // トランザクション開始
        $this->db->beginNestedTransaction();

        // 実績をセット
        $insert = array();
        $update = array();
        $update_time = array();
        $update_staff = array();
        foreach ($data['emp'] as $emp_id_key => $emp) {
            $emp_id = substr($emp_id_key, 1);

            if (!is_array($emp['dates'])) {
                continue;
            }
            foreach ($emp['dates'] as $date_key => $p) {
                $date = substr($date_key, 1);

                // start_dateより過去の日付は無視
                if ($start_date > $date) {
                    continue;
                }

                $type = $dates[$date_key]['type'] !== '8' ? $dates[$date_key]['type'] : '3';
                $ptn = (int)substr($p['results'], 0, 2);
                $duty = ($p['results_duty'] and $p['results_duty'] !== 'null') ? '1' : '0';

                $start_time = str_replace(':', '', $ohours[$ptn][$type]['officehours2_start']);
                $end_time = str_replace(':', '', $ohours[$ptn][$type]['officehours2_end']);
                $rest_start_time = str_replace(':', '', $ohours[$ptn][$type]['officehours4_start']);
                $rest_end_time = str_replace(':', '', $ohours[$ptn][$type]['officehours4_end']);

                // 更新
                if ($results[$emp_id][$date]) {
                    // 記号の変更が無ければSQLは発行しない
                    if ($results[$emp_id][$date]['pattern'] != $p['results']) {
                        $set = array(
                            'emp_id' => $emp_id,
                            'date' => $date,
                            'tmcd_group_id' => $p['results_pattern_id'] ? $p['results_pattern_id'] : $this->_ward->pattern_id(),
                            'pattern' => (int)substr($p['results'], 0, 2),
                            'reason' => substr($p['results'], 2, 2) ? substr($p['results'], 2, 2) : '',
                            'night_duty' => $duty,
                            'reg_prg_flg' => "3",
                        );
                        $update[] = $set;
                        $update_staff[] = $emp_id;

                        // 時間変更あり
                        if ($results[$emp_id][$date]['start_time'] != $start_time or $results[$emp_id][$date]['end_time'] != $end_time) {
                            // 翌日フラグ
                            // (前日フラグがない AND 出勤時刻＞退勤時刻) OR 24時間以上
                            $next_day_flag = 0;
                            if ($ptn !== 10 and ( !$pattern[$ptn]->previous_day_flag() and $start_time >= $end_time or $pattern[$ptn]->over_24hour_flag())) {
                                $next_day_flag = 1;
                            }

                            $set = array(
                                'emp_id' => $emp_id,
                                'date' => $date,
                                'start_time' => $start_time,
                                'end_time' => $end_time,
                                'rest_start_time' => $rest_start_time,
                                'rest_end_time' => $rest_end_time,
                                'reg_prg_flg' => "3",
                                'next_day_flag' => $next_day_flag,
                            );
                            $update_time[] = $set;
                        }
                    }
                }
                // 新規
                else {
                    // 存在しないパターンは除外
                    if ($ptn !== 10 and ! $pattern[$ptn]) {
                        continue;
                    }

                    // 翌日フラグ
                    // (前日フラグがない AND 出勤時刻＞退勤時刻) OR 24時間以上
                    $next_day_flag = 0;
                    if ($ptn !== 10 and ( !$pattern[$ptn]->previous_day_flag() and $start_time >= $end_time or $pattern[$ptn]->over_24hour_flag())) {
                        $next_day_flag = 1;
                    }

                    $set = array(
                        'emp_id' => $emp_id,
                        'date' => $date,
                        'tmcd_group_id' => $p['results_pattern_id'] ? $p['results_pattern_id'] : $this->_ward->pattern_id(),
                        'pattern' => (int)substr($p['results'], 0, 2),
                        'reason' => substr($p['results'], 2, 2) ? substr($p['results'], 2, 2) : '',
                        'night_duty' => $duty,
                        'start_time' => $start_time,
                        'end_time' => $end_time,
                        'rest_start_time' => $rest_start_time,
                        'rest_end_time' => $rest_end_time,
                        'reg_prg_flg' => "3",
                        'next_day_flag' => $next_day_flag,
                    );
                    $insert[] = $set;
                    $update_staff[] = $emp_id;
                }
            }
        }

        if ($insert) {
            $sth = $this->db->prepare('
                INSERT INTO atdbkrslt
                (emp_id, date, pattern, reason, night_duty, tmcd_group_id, start_time, end_time, reg_prg_flg, next_day_flag, rest_start_time, rest_end_time)
                VALUES (:emp_id, :date, :pattern, :reason, :night_duty, :tmcd_group_id, :start_time, :end_time, :reg_prg_flg, :next_day_flag, :rest_start_time, :rest_end_time)
                ', array('text', 'text', 'text', 'text', 'text', 'integer', 'text', 'text', 'text', 'integer', 'text', 'text'), MDB2_PREPARE_MANIP);
            $res = $this->db->extended->executeMultiple($sth, $insert);
            if (PEAR::isError($res)) {
                $this->db->rollback();
                return false;
            }
            $sth->free();
        }
        if ($update) {
            $sth = $this->db->prepare('
                UPDATE atdbkrslt SET
                pattern = :pattern, reason = :reason, night_duty = :night_duty, tmcd_group_id = :tmcd_group_id, reg_prg_flg = :reg_prg_flg
                WHERE emp_id = :emp_id AND date = :date
                ', array('text', 'text', 'text', 'integer', 'text', 'text', 'text'), MDB2_PREPARE_MANIP);
            $res = $this->db->extended->executeMultiple($sth, $update);
            if (PEAR::isError($res)) {
                $this->db->rollback();
                return false;
            }
            $sth->free();
        }
        if ($update_time) {
            $sth = $this->db->prepare('
                UPDATE atdbkrslt SET
                start_time = :start_time, end_time = :end_time, rest_start_time = :rest_start_time, rest_end_time = :rest_end_time, next_day_flag = :next_day_flag, reg_prg_flg = :reg_prg_flg
                WHERE emp_id = :emp_id AND date = :date
                ', array('text', 'text', 'text', 'text', 'integer', 'text', 'text', 'text'), MDB2_PREPARE_MANIP);
            $res = $this->db->extended->executeMultiple($sth, $update_time);
            if (PEAR::isError($res)) {
                $this->db->rollback();
                return false;
            }
            $sth->free();
        }
        if ($update_staff) {
            foreach ($update_staff as $emp_id) {
                $obj = new Cmx_Shift_Staff($emp_id);
                $obj->set_carryover($this->_year, $this->_month);
            }
        }

        // commit
        $this->db->commit();
        $this->log->log('勤務実績.保存', array('group' => $this->_ward->group_name(), 'year' => $this->_year, 'month' => $this->_month, 'start_date' => $start_date, 'end_date' => $end_date));
        return true;
    }

    /**
     * 勤務希望保存
     * @param $emp_id 希望を保存する職員ID
     * @param $date 希望を保存する日付(YYYYMMDD)
     * @param $pattern 希望するパターン (9999)
     *
     * @return boolean true:成功 false:失敗
     */
    function save_hope($emp_id, $date, $pattern)
    {
        // トランザクション開始
        $this->db->beginTransaction();

        // DELETE してから
        $sth = $this->db->prepare('
                DELETE FROM duty_shift_plan_individual
                WHERE emp_id = :emp_id AND duty_date = :duty_date
                ', array('text', 'text'), MDB2_PREPARE_MANIP);
        $res = $sth->execute(array(
            'emp_id' => $emp_id,
            'duty_date' => $date,
            )
        );
        if (PEAR::isError($res)) {
            $this->db->rollback();
            return false;
        }

        // INSERT する
        $sth = $this->db->prepare('
                INSERT INTO duty_shift_plan_individual
                (group_id, emp_id, duty_date, pattern_id, atdptn_ptn_id, reason)
                VALUES (:group_id, :emp_id, :duty_date, :pattern_id, :atdptn_ptn_id, :reason)
                ', array('text', 'text', 'text', 'integer', 'integer', 'text'), MDB2_PREPARE_MANIP);
        $set = array(
            'group_id' => $this->_ward->group_id(),
            'emp_id' => $emp_id,
            'duty_date' => $date,
            'pattern_id' => $this->_ward->pattern_id(),
            'atdptn_ptn_id' => (int)substr($pattern, 0, 2),
            'reason' => substr($pattern, 2, 2) ? substr($pattern, 2, 2) : '',
        );
        $res = $sth->execute($set);
        if (PEAR::isError($res)) {
            $this->db->rollback();
            return false;
        }

        $this->db->commit();
        $this->log->log('勤務希望.保存', $set);
        return true;
    }

    /**
     * 応援追加保存
     * @param type $data
     * @return boolean
     */
    function save_assist($data)
    {
        // スタッフ
        $staff_in = $this->staff_in();
        if ($staff_in === '') {
            return false;
        }

        // 日付
        $start_date = $this->_ward->start_ymd($this->_year, $this->_month);
        $end_date = $this->_ward->end_ymd($this->_year, $this->_month);

        // トランザクション開始
        $this->db->beginNestedTransaction();

        // 応援を削除
        $sth = $this->db->prepare("DELETE FROM duty_shift_plan_assist WHERE emp_id in ($staff_in) AND duty_date BETWEEN :start_date AND :end_date");
        $res = $sth->execute(array('start_date' => $start_date, 'end_date' => $end_date));
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_Table: save_assist: DELETE FROM duty_shift_plan_assist: ' . $res->getDebugInfo());
            return false;
        }

        // 応援をセット
        $assist = array();
        foreach ($data['emp'] as $emp_id => $emp) {
            if (empty($emp['dates'])) {
                continue;
            }
            foreach ($emp['dates'] as $date => $p) {
                if (!$p['assist'] or $p['assist'] === 'null') {
                    $p['assist'] = $this->_ward->group_id();
                }
                // 予定が「未設定」なら応援登録しない
                if (empty($p['plan']) or $p['plan'] === '0') {
                    continue;
                }
                $set = array(
                    'group_id' => $this->_ward->group_id(),
                    'emp_id' => substr($emp_id, 1),
                    'duty_date' => substr($date, 1),
                    'assist_flg' => 1,
                    'main_group_id' => $p['assist'],
                );
                $assist[] = $set;
            }
        }
        $sth = $this->db->prepare('
                INSERT INTO duty_shift_plan_assist
                (group_id, emp_id, duty_date, assist_flg, main_group_id)
                VALUES (:group_id, :emp_id, :duty_date, :assist_flg, :main_group_id)
                ', array('text', 'text', 'text', 'text', 'text'), MDB2_PREPARE_MANIP);
        $res = $this->db->extended->executeMultiple($sth, $assist);
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_Table: save_assist: INSERT INTO duty_shift_plan_assist: ' . $res->getDebugInfo());
            return false;
        }

        // commit
        $this->db->commit();
        $this->log->log('応援追加.保存', array('group' => $this->_ward->group_name(), 'year' => $this->_year, 'month' => $this->_month, 'start_date' => $start_date, 'end_date' => $end_date));
        return true;
    }

    /**
     * 当直保存
     *
     * @param $data 保存する勤務表データ
     *
     * @return boolean true:成功 false:失敗
     */
    function save_duty($data)
    {
        $this->set_table($data);

        // ds_plan_staffが無ければ、ds_staffからコピー
        if (!$this->_staff->is_plan_staff($this->_group_id, $this->_year, $this->_month)) {
            $this->_staff->master_staff_copy($this->_group_id, $this->_year, $this->_month);
        }

        // スタッフ
        $staff_in = $this->staff_in();
        if ($staff_in === '') {
            return array();
        }

        // 日付
        $start_date = $this->_ward->start_ymd($this->_year, $this->_month);
        $end_date = $this->_ward->end_ymd($this->_year, $this->_month);

        // 現在の予定
        $sth = $this->db->prepare("
            SELECT
                emp_id,
                date,
                night_duty AS duty
            FROM atdbk ptn
            WHERE emp_id IN ($staff_in) AND date BETWEEN :start_date AND :end_date
        ");
        $res = $sth->execute(array('start_date' => $start_date, 'end_date' => $end_date));
        $duty = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $duty[$row['emp_id']][$row['date']] = $row;
        }

        // トランザクション開始
        $this->db->beginTransaction();

        // 当直をセット
        $insert = array();
        $update = array();
        foreach ($data['emp'] as $emp_id_key => $emp) {
            $emp_id = substr($emp_id_key, 1);

            if (empty($emp['dates'])) {
                continue;
            }
            foreach ($emp['dates'] as $date_key => $p) {
                $date = substr($date_key, 1);
                $p['duty'] = $p['duty'] === 'null' ? '' : $p['duty'];

                // UPDATE
                if ($duty[$emp_id][$date]) {
                    if ($duty[$emp_id][$date]['duty'] != $p['duty']) {
                        $set = array(
                            'emp_id' => $emp_id,
                            'date' => $date,
                            'night_duty' => $p['duty'],
                        );
                        $update[] = $set;
                    }
                }
                // INSERT
                else {
                    $set = array(
                        'emp_id' => $emp_id,
                        'date' => $date,
                        'night_duty' => $p['duty'],
                    );
                    $insert[] = $set;
                }
            }
        }

        if ($insert) {
            $sth = $this->db->prepare('
                INSERT INTO atdbk
                (emp_id, date, night_duty)
                VALUES (:emp_id, :date, :night_duty)
                ', array('text', 'text', 'text'), MDB2_PREPARE_MANIP);
            $res = $this->db->extended->executeMultiple($sth, $insert);
            if (PEAR::isError($res)) {
                $this->db->rollback();
                return false;
            }
        }
        if ($update) {
            $sth = $this->db->prepare('
                UPDATE atdbk SET
                night_duty = :night_duty
                WHERE emp_id = :emp_id AND date = :date
                ', array('text', 'text', 'text'), MDB2_PREPARE_MANIP);
            $res = $this->db->extended->executeMultiple($sth, $update);
            if (PEAR::isError($res)) {
                $this->db->rollback();
                return false;
            }
        }

        // commit
        $this->db->commit();
        $this->log->log('当直.保存', array('group' => $this->_ward->group_name(), 'year' => $this->_year, 'month' => $this->_month, 'start_date' => $start_date, 'end_date' => $end_date));
        return true;
    }

    /**
     * 勤務表セル: 職員情報
     * @return type
     */
    function cell_emp()
    {
        $param = func_get_args();
        if (func_num_args() === 2) {
            return $this->_tbl['emp']['e' . $param[0]][$param[1]];
        }
        else {
            return $this->_tbl['emp']['e' . $param[0]][$param[1]][$param[2]];
        }
    }

    /**
     * 勤務表セル: 行集計
     * @return type
     */
    function cell_row_count($emp_id, $count, $key)
    {
        return $this->_tbl['emp']['e' . $emp_id]['count_row']['c' . $count][$key];
    }

    /**
     * 勤務表セル: 列集計
     * @return type
     */
    function cell_col_count($date, $count, $key)
    {
        return $this->_tbl['date']['d' . $date]['count_col']['c' . $count][$key];
    }

    /**
     * 勤務表セル: 行集計(当直)
     * @return type
     */
    function cell_row_count_duty($emp_id, $key)
    {
        return $this->_tbl['emp']['e' . $emp_id]["count_$key"];
    }

    /**
     * 勤務表セル: 列集計(当直)
     * @return type
     */
    function cell_col_count_duty($date, $key)
    {
        return $this->_tbl['date']['d' . $date]["count_$key"];
    }

    /**
     * 勤務表セル: 記号
     * @return type
     */
    function cell_sign($emp_id, $date, $key)
    {
        $ptn = $this->_tbl['emp']['e' . $emp_id]['dates']['d' . $date];
        switch ($key) {
            case 'plan':
            case 'hope':
                $pattern = $ptn['plan_pattern_id'] ? $this->_ptn[$ptn['plan_pattern_id']]->hash() : $this->_pattern->hash();
                if ($ptn[$key] and $ptn[$key] !== '0') {
                    return $pattern[$ptn[$key]]->font_name();
                }
                break;

            case 'results':
                $pattern = $ptn['results_pattern_id'] ? $this->_ptn[$ptn['results_pattern_id']]->hash() : $this->_pattern->hash();
                if ($ptn[$key] && $ptn[$key] !== '0' && is_object($pattern[$ptn[$key]])) {
                    return $pattern[$ptn[$key]]->font_name();
                }
                break;

            case 'duty':
            case 'results_duty':
                if ($ptn[$key] and $ptn[$key] === '1') {
                    return '●';
                }
                break;

            case 'results_ovtm':
            case 'results_ovtmrsn':
                return $ptn[$key];
        }
        return "";
    }

    /**
     * 勤務表セル: 記号スタイル
     * @return type
     */
    function cell_style($emp_id, $date, $key)
    {
        $emp = $this->_tbl['emp']['e' . $emp_id];
        $ptn = $emp['dates']['d' . $date];
        $style = '';
        switch ($key) {
            case 'plan':
            case 'hope':
                $pattern = $ptn['plan_pattern_id'] ? $this->_ptn[$ptn['plan_pattern_id']]->hash() : $this->_pattern->hash();
                if ($ptn[$key] and $ptn[$key] !== '0') {
                    $style .= $pattern[$ptn[$key]]->style();
                }

                // 応援
                if ($this->cell_assist($emp_id, $date, $key)) {
                    $style .= "color:#808080;";
                }
                break;

            case 'results':
                $pattern = $ptn['results_pattern_id'] ? $this->_ptn[$ptn['results_pattern_id']]->hash() : $this->_pattern->hash();
                if ($ptn[$key] && $ptn[$key] !== '0' && is_object($pattern[$ptn[$key]])) {
                    $style .= $pattern[$ptn[$key]]->style();
                }
                break;
        }
        return $style;
    }

    /**
     * 勤務表セル: 応援
     * @return type
     */
    function cell_assist($emp_id, $date, $key)
    {
        $emp = $this->_tbl['emp']['e' . $emp_id];
        $ptn = $emp['dates']['d' . $date];
        switch ($key) {
            case 'plan':
            case 'hope':
                if ($ptn[$key] !== '0' and $ptn[$key] and $this->is_assist($emp['assist'], $ptn['assist'])) {
                    return true;
                }
                break;
        }
        return false;
    }

    /**
     * 下書きフラグ
     * @return type
     */
    function is_draft()
    {
        return $this->_tbl['display_draft'];
    }

    /**
     * 登録フラグ
     * @return type
     */
    function is_plan()
    {
        return $this->_tbl['display_plan'];
    }

    /**
     * 応援チェック
     * @param str $emp_assist 職員応援フラグ
     * @param str $ptn_assist 勤務応援フラグ
     * @return true/false
     */
    function is_assist($emp_assist, $ptn_assist)
    {
        if (($emp_assist === '1' and $ptn_assist !== $this->_group_id)
            or ( $emp_assist !== '1' and $ptn_assist and $ptn_assist !== 'null' and $ptn_assist !== $this->_group_id)) {
            return true;
        }
        return false;
    }

    /**
     * 夜勤従事者数
     * @param type $type
     * @return int
     */
    function night_nurse_count($type)
    {
        if ($type !== 'plan' and $type !== 'results') {
            return 0;
        }
        if (!isset($this->_tbl['night_nurse_count'])) {
            return 0;
        }
        if (!isset($this->_tbl['night_nurse_count'][$type])) {
            return 0;
        }
        return $this->_tbl['night_nurse_count'][$type];
    }

    /**
     * 夜勤従事者の延べ夜勤時間
     * @param type $type
     * @return int
     */
    function night_hours($type)
    {
        if ($type !== 'plan' and $type !== 'results') {
            return '0.00';
        }
        if (!isset($this->_tbl['night_hours'])) {
            return '0.00';
        }
        if (!isset($this->_tbl['night_hours'][$type])) {
            return '0.00';
        }
        return $this->_tbl['night_hours'][$type];
    }

    /**
     * 平均夜勤時間
     * @param type $type
     * @return int
     */
    function night_hours_avg($type)
    {
        if ($type !== 'plan' and $type !== 'results') {
            return '0.00';
        }
        if (!isset($this->_tbl['night_hours_avg'])) {
            return '0.00';
        }
        if (!isset($this->_tbl['night_hours_avg'][$type])) {
            return '0.00';
        }
        return $this->_tbl['night_hours_avg'][$type];
    }

    /**
     * 月次承認可
     * @param type $type
     * @return int
     */
    function is_month_approval()
    {
        return $this->_tbl['approved'];
    }

    /**
     * PDF作成処理
     *
     * @return boolean true:成功 false:失敗
     */
    function save_pdf()
    {
        $adminlist = $this->_ward->administrator();
        $Calendar = $this->_ward->dates($this->_year, $this->_month);
        $wh = new Cmx_Shift_WorkingHours($this->_year, $this->_month);

        // 保存先ディレクトリの生成
        $pdfdir = sprintf('%s/%04d/%02d', $this->_group_id, $this->_year, $this->_month);
        $dir = sprintf('%s/shift/pdf/%s', CMX_BASE_DIR, $pdfdir);
        if (!is_dir($dir)) {
            umask(0);
            mkdir($dir, 0777, true);
        }

        // ファイル名
        $pdf_file = $this->_group_id . '_' . date('Ymd') . '_' . date('His') . '.pdf';

        // PDF生成開始
        $pdf = new Cmx_Shift_TablePDF();
        $pdf->set_group_id($this->_group_id);
        $pdf->set_start_date($this->start_ymd());
        $pdf->set_end_date($this->end_ymd());
        $pdf->set_admin_name($adminlist[0]->emp_name());
        $pdf->set_group_name($this->_ward->group_name());
        $pdf->set_ym($this->_year, $this->_month);
        $pdf->set_calendar($Calendar);
        $pdf->set_row_list($this->_ward->count_row_lists());
        $pdf->set_col_list($this->_ward->count_col_lists());
        $pdf->set_pattern($this->_pattern->hash());
        $pdf->set_whours($wh->fetch());

        $pdf->Open();
        $pdf->AddPage();

        //帳票メイン部生成
        $pdf->Pdf_create($this->get_table());
        $pdf->Output("$dir/$pdf_file", 'F');

        //作成履歴をテーブルに保存
        $this->db->beginTransaction();
        $sth = $this->db->prepare('
                INSERT INTO duty_shift_plan_pdf_history (group_id, year, month, emp_id, create_date, pdf_file)
                VALUES (:group_id, :year, :month, :emp_id, :create_date, :pdf_file)
                '
            , array('text', 'text', 'text', 'text', 'text', 'text')
            , MDB2_PREPARE_MANIP
        );
        $res = $sth->execute(array(
            'group_id' => $this->_group_id,
            'year' => $this->_year,
            'month' => $this->_month,
            'emp_id' => $this->_login_id,
            'create_date' => date('Y-m-d H:i:s'),
            'pdf_file' => "$pdfdir/$pdf_file",
            )
        );
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_Table: save_pdf: INSERT INTO duty_shift_plan_pdf_history: ' . $res->getDebugInfo());
            return false;
        }
        $this->db->commit();

        return true;
    }

    /**
     * コピー元の勤務表
     *
     * @param str $from 開始日
     * @param str $days 日数
     */
    function copy_table($mode, $from, $to, $days)
    {
        $staff_list = $this->_staff->lists($this->_group_id, $this->_year, $this->_month);
        if (empty($staff_list)) {
            return array();
        }

        // 日付
        $start_date = str_replace("-", "", $from);
        $end_date = date('Ymd', strtotime($from) + (86400 * ($days - 1)));

        $this->_tbl = array();

        // スタッフ
        $staff_array = array();
        foreach ($staff_list as $row) {
            $staff_array[] = sprintf("'%s'", $row->emp_id());
        }
        $staff_in = implode(',', $staff_array);

        $sth = $this->db->prepare("
            SELECT
                e.emp_id,
                c.date,
                CASE WHEN p.pattern='10' THEN p.pattern||LPAD(p.reason,2,'0') ELSE p.pattern END AS plan,
                CASE WHEN r.pattern='10' THEN r.pattern||LPAD(r.reason,2,'0') ELSE r.pattern END AS results
            FROM empmst e
            JOIN calendar c ON (date BETWEEN :start_date AND :end_date)
            LEFT JOIN atdbk p ON (p.emp_id=e.emp_id AND p.date=c.date AND p.tmcd_group_id= :pattern_id)
            LEFT JOIN atdbkrslt r ON (r.emp_id=e.emp_id AND r.date=c.date AND r.tmcd_group_id= :pattern_id)
            WHERE e.emp_id IN ($staff_in)
        ");
        $res = $sth->execute(array(
            'start_date' => $start_date,
            'end_date' => $end_date,
            'pattern_id' => $this->_ward->pattern_id(),
        ));

        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $date = date('Ymd', strtotime($to) + strtotime($row["date"]) - strtotime($start_date));
            if ($row[$mode] and $row[$mode] !== '0') {
                $this->_tbl['emp']["e{$row["emp_id"]}"]['dates']["d{$date}"]["plan"] = $row[$mode];
            }
        }
    }

}
