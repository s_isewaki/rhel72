<?php

require_once('Cmx.php');
require_once('Cmx/Core/Model.php');
require_once('Cmx/Model/Employee.php');

/**
 * ��̳ɽ����Ͽ���򥯥饹
 *
 * ��̳ɽ����Ͽ����Υ��饹
 *
 */
class Cmx_Shift_TableHistory extends Model
{

    var $_group_id;
    var $_year;
    var $_month;

    // ���󥹥ȥ饯��
    function Cmx_Shift_TableHistory($group_id, $year, $month)
    {
        parent::connectDB();
        $this->_group_id = $group_id;
        $this->_year = $year;
        $this->_month = $month;
    }

    // �ꥹ��
    function lists()
    {
        $sth = $this->db->prepare("
            SELECT * FROM duty_shift_plan_pdf_history WHERE group_id= ? AND year= ? AND month= ? ORDER BY create_date DESC
        ");
        $res = $sth->execute(array($this->_group_id, $this->_year, $this->_month));

        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['emp'] = new Cmx_Employee($row['emp_id']);
            array_push($list, $row);
        }
        return $list;
    }

}
