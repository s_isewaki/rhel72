<?php

//
// 勤務表：組み合わせシフト クラス
//
//
require_once('Cmx.php');
require_once('Cmx/Core/Model.php');

class Cmx_Shift_PatternOK extends Model
{

    var $_list;
    var $_group_id;

    // コンストラクタ
    function Cmx_Shift_PatternOK($row = null)
    {
        parent::connectDB();
        $this->_list = array();

        if (is_array($row)) {
            $this->_init($row);
        }
        else if (!is_null($row)) {
            $this->_group_id = $row;
        }
    }

    // _init
    function _init($row)
    {
        $this->_row = $row;
    }

    // リスト
    function lists()
    {
        if (array_key_exists($this->_group_id, $this->_list)) {
            return $this->_list[$this->_group_id];
        }

        $sth = $this->db->prepare("
            SELECT * FROM duty_shift_group_seq_pattern WHERE group_id= ? ORDER BY no
        ");
        $res = $sth->execute($this->_group_id);

        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            array_push($list, $row);
        }
        $this->_list[$this->_group_id] = $list;
        return $list;
    }

    // ハッシュリスト
    function hash_lists()
    {
        $hash = array();
        foreach ($this->lists() as $row) {
            $today_ptn = $row['today_atdptn_ptn_id'] != '10' ? $row['today_atdptn_ptn_id'] : sprintf('10%02d', $row['today_reason']);
            $next_ptn = $row['nextday_atdptn_ptn_id'] != '10' ? $row['nextday_atdptn_ptn_id'] : sprintf('10%02d', $row['nextday_reason']);
            $hash["p{$today_ptn}"] = $next_ptn;
        }
        return $hash;
    }

}
