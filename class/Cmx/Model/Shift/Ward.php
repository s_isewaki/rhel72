<?php

require_once('Cmx.php');
require_once('Cmx/Core/Model.php');
require_once('Cmx/Model/Shift/Staff.php');
require_once('Cmx/Model/Shift/Institution.php');
require_once('Cmx/Model/Shift/Pattern.php');
require_once('Cmx/Model/Shift/PatternInterval.php');
require_once('Cmx/Model/Shift/PatternUpperLimit.php');
require_once('Cmx/Model/Shift/PatternOK.php');
require_once('Cmx/Model/Shift/PatternNG.php');
require_once('Cmx/Model/Shift/PatternSex.php');
require_once('Cmx/Model/Shift/PatternHoliday.php');
require_once('Cmx/Model/Shift/PrintStatusNames.php');
require_once('Cmx/Model/Shift/WardAccessUser.php');
require_once('Cmx/Model/Shift/WardAccessPermission.php');
require_once('Cmx/Model/Employee.php');
require_once('Cmx/Model/Calendar.php');
require_once('Cmx/Model/SystemConfig.php');
require_once('Cmx/Shift/Date/Months.php');
require_once('Cmx/Shift/Date/Cours.php');

/**
 * 勤務表：部署クラス
 */
class Cmx_Shift_Ward extends Model
{

    var $_row;
    var $_pattern;
    var $_sdate;
    var $_dates;
    var $_dates_in;
    var $_staff;
    var $_display;

    // コンストラクタ
    function Cmx_Shift_Ward($row = null)
    {
        parent::connectDB();
        $this->_staff = new Cmx_Shift_Staff();
        $this->_display = array();

        if (is_array($row)) {
            $this->_init($row);
        }
        else if (!is_null($row)) {
            $this->_select($row);
        }
    }

    // _init
    function _init($row)
    {
        $this->_row = $row;
    }

    // _select
    function _select($row)
    {
        $sth = $this->db->prepare("SELECT * FROM duty_shift_group WHERE group_id= ?");
        $res = $sth->execute($row);
        $this->_init($res->fetchRow(MDB2_FETCHMODE_ASSOC));
    }

    // Shift Date
    function shift_date($mode = null)
    {
        if ($this->_sdate) {
            return $this->_sdate;
        }
        if ($this->month_type() === '2' && $mode !== 'month') {
            $conf = new Cmx_SystemConfig();
            $base_year = $conf->get('shift.cours.base_year');
            $base_date = $conf->get('shift.cours.base_date');
            $base_count = $conf->get('shift.cours.base_count');
            $this->_sdate = new Cmx_Shift_Date_Cours($base_date, $base_year, $base_count);
        }
        else {
            $this->_sdate = new Cmx_Shift_Date_Months($this->start_month_flg1(), $this->start_day1());
        }
        return $this->_sdate;
    }

    // 部署リスト
    function lists($emp_id = null)
    {
        if (is_null($emp_id) || $emp_id === '000000000000') {
            $res = $this->db->query("SELECT * FROM duty_shift_group ORDER BY order_no, group_id");
        }
        else {
            $sth = $this->db->prepare("
                SELECT * FROM duty_shift_group
                WHERE person_charge_id= :emp_id
                   OR caretaker_id= :emp_id
                   OR caretaker2_id= :emp_id
                   OR caretaker3_id= :emp_id
                   OR caretaker4_id= :emp_id
                   OR caretaker5_id= :emp_id
                   OR caretaker6_id= :emp_id
                   OR caretaker7_id= :emp_id
                   OR caretaker8_id= :emp_id
                   OR caretaker9_id= :emp_id
                   OR caretaker10_id= :emp_id
                ORDER BY order_no, group_id");
            $res = $sth->execute(array('emp_id' => $emp_id));
        }

        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $obj = new Cmx_Shift_Ward($row);
            array_push($list, $obj);
        }
        return $list;
    }

    // 連携対象部署リスト
    function kango_lists()
    {
        $res = $this->db->query("SELECT * FROM duty_shift_group WHERE sfc_group_code <> '' ORDER BY order_no, group_id");

        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $obj = new Cmx_Shift_Ward($row);
            $list[] = $obj;
        }
        return $list;
    }

    // 所属部署
    function staff_ward($emp_id)
    {
        $sth = $this->db->prepare("SELECT d.* FROM duty_shift_group d JOIN duty_shift_staff USING (group_id) WHERE emp_id= :emp_id");
        $res = $sth->execute(array('emp_id' => $emp_id));

        if ($res->numRows()) {
            $row = $res->fetchRow(MDB2_FETCHMODE_ASSOC);
            $obj = new Cmx_Shift_Ward($row);
            return $obj;
        }
        return null;
    }

    // 応援追加部署
    function ouen_ward($emp_id, $year, $month)
    {
        $sth = $this->db->prepare("
            SELECT d.* FROM duty_shift_group d JOIN duty_shift_plan_staff USING (group_id)
            WHERE emp_id= :emp_id AND duty_yyyy= :year AND duty_mm= :month AND group_id != main_group_id");
        $res = $sth->execute(array('emp_id' => $emp_id, 'year' => $year, 'month' => $month));

        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[] = new Cmx_Shift_Ward($row);
        }
        return $list;
    }

    // Update
    function update()
    {
        // pattern_id
        if ($this->_row["pattern_id"] === "") {
            $this->_row["pattern_id"] = null;
        }

        // administrator
        $this->administrator();

        // UPDATE
        if ($this->group_id()) {
            $this->db->beginTransaction();
            $sth = $this->db->prepare(
                "
                UPDATE duty_shift_group SET
                    group_name= :group_name,
                    standard_id= :standard_id,
                    person_charge_id= :person_charge_id,
                    caretaker_id= :caretaker_id,
                    caretaker2_id= :caretaker2_id,
                    caretaker3_id= :caretaker3_id,
                    caretaker4_id= :caretaker4_id,
                    caretaker5_id= :caretaker5_id,
                    caretaker6_id= :caretaker6_id,
                    caretaker7_id= :caretaker7_id,
                    caretaker8_id= :caretaker8_id,
                    caretaker9_id= :caretaker9_id,
                    caretaker10_id= :caretaker10_id,
                    pattern_id= :pattern_id,
                    sfc_group_code= :sfc_group_code,
                    kaitori_day= :kaitori_day,
                    month_type= :month_type,
                    manager= :manager,
                    plan_edit_flg= :plan_edit_flg,
                    result_edit_flg= :result_edit_flg
                WHERE group_id= :group_id
                ", array('text', 'text', 'text', 'text', 'text', 'text', 'text', 'text', 'text', 'text', 'text', 'text', 'text', 'integer', 'text', 'text', 'text', 'text', 'text', 'text', 'text'), MDB2_PREPARE_MANIP
            );
            $res = $sth->execute($this->_row);
            if (PEAR::isError($res)) {
                $this->db->rollback();
                cmx_log('ROLLBACK: Cmx_Shift_Ward: update: UPDATE duty_shift_group: ' . $res->getDebugInfo());
                return false;
            }

            // Cmx_Shift_WardAccessUser
            $access_user = new Cmx_Shift_WardAccessUser($this->group_id());
            $res = $access_user->update($this->_row);
            if (!$res) {
                $this->db->rollback();
                cmx_log('ROLLBACK: Cmx_Shift_Ward: update: Cmx_Shift_WardAccessUser->update(): ' . $res->getDebugInfo());
                return false;
            }

            // Cmx_Shift_WardAccessPermission
            $access_perm = new Cmx_Shift_WardAccessPermission($this->group_id());
            $res = $access_perm->update($this->_row);
            if (!$res) {
                $this->db->rollback();
                cmx_log('ROLLBACK: Cmx_Shift_Ward: update: Cmx_Shift_WardAccessPermission->update(): ' . $res->getDebugInfo());
                return false;
            }

            // commit
            $this->db->commit();
            return true;
        }

        // INSERT
        else {
            $this->db->beginTransaction();
            $this->set_group_id($this->_new_group_id());
            $sth = $this->db->prepare(
                "
                INSERT INTO duty_shift_group (
                    group_id,
                    group_name,
                    standard_id,
                    person_charge_id,
                    caretaker_id,
                    caretaker2_id,
                    caretaker3_id,
                    caretaker4_id,
                    caretaker5_id,
                    caretaker6_id,
                    caretaker7_id,
                    caretaker8_id,
                    caretaker9_id,
                    caretaker10_id,
                    pattern_id,
                    sfc_group_code,
                    kaitori_day,
                    month_type
                ) VALUES (
                    :group_id,
                    :group_name,
                    :standard_id,
                    :person_charge_id,
                    :caretaker_id,
                    :caretaker2_id,
                    :caretaker3_id,
                    :caretaker4_id,
                    :caretaker5_id,
                    :caretaker6_id,
                    :caretaker7_id,
                    :caretaker8_id,
                    :caretaker9_id,
                    :caretaker10_id,
                    :pattern_id,
                    :sfc_group_code,
                    :kaitori_day,
                    :month_type
                )
                ", array('text', 'text', 'text', 'text', 'text', 'text', 'text', 'text', 'text', 'text', 'text', 'text', 'text', 'text', 'integer', 'text', 'text', 'text'), MDB2_PREPARE_MANIP
            );
            $res = $sth->execute($this->_row);
            if (PEAR::isError($res)) {
                $this->db->rollback();
                cmx_log('ROLLBACK: Cmx_Shift_Ward: update: INSERT INTO duty_shift_group: ' . $res->getDebugInfo());
                return false;
            }
            else {
                $this->db->commit();
                return true;
            }
        }
    }

    // Remove
    function remove($group_id)
    {
        $this->db->beginTransaction();
        $sth = $this->db->prepare(
            "DELETE FROM duty_shift_group WHERE group_id= ?", array('text'), MDB2_PREPARE_MANIP
        );
        $res = $sth->execute($group_id);
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_Ward: remove: DELETE FROM duty_shift_group: ' . $res->getDebugInfo());
            return false;
        }
        else {
            $this->db->commit();
            return true;
        }
    }

    // 順番の更新
    function update_order($group_id, $order)
    {
        $this->db->beginTransaction();

        $sth = $this->db->prepare(
            "UPDATE duty_shift_group SET order_no= ? WHERE group_id= ?", array('integer', 'text'), MDB2_PREPARE_MANIP
        );
        $res = $sth->execute(array($order, $group_id));
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_Ward: update_order: UPDATE duty_shift_group: ' . $res->getDebugInfo());
            return false;
        }
        else {
            $this->db->commit();
            return true;
        }
    }

    // 部署設定の更新
    function ward_update()
    {
        // 4週8休開始日
        $this->_row["chk_start_date"] = preg_replace('/(\d\d\d\d)\-(\d\d)\-(\d\d)/', "$1$2$3", $this->_row["chk_start_date"]);

        $this->_row["need_less_chk_flg"] = $this->_row["need_less_chk_flg"] === 't' ? 't' : 'f';
        $this->_row["need_more_chk_flg"] = $this->_row["need_more_chk_flg"] === 't' ? 't' : 'f';

        // 表示項目
        $this->row['display'] = serialize($this->row['display']);

        $this->db->beginTransaction();

        // duty_shift_group
        $sth = $this->db->prepare("
            UPDATE duty_shift_group SET
                close_day= :close_day,
                duty_upper_limit_day= :duty_upper_limit_day,
                start_month_flg1= :start_month_flg1,
                start_day1= :start_day1,
                month_flg1= :month_flg1,
                end_day1= :end_day1,
                reason_setting_flg= :reason_setting_flg,
                four_week_chk_flg= :four_week_chk_flg,
                chk_start_date= :chk_start_date,
                need_less_chk_flg= :need_less_chk_flg,
                need_more_chk_flg= :need_more_chk_flg,
                auto_check_months= :auto_check_months,
                auto_shift_filler= :auto_shift_filler,
                print_title= :print_title,
                team_disp_flg= :team_disp_flg,
                job_disp_flg= :job_disp_flg,
                st_disp_flg= :st_disp_flg,
                display = :display
            WHERE group_id= :group_id", array('text', 'text', 'text', 'int', 'text', 'int', 'bool', 'bool', 'text', 'bool', 'bool', 'text', 'text', 'text', 'bool', 'bool', 'bool', 'text', 'text'), MDB2_PREPARE_MANIP);
        $res = $sth->execute($this->_row);

        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_Ward: ward_update: UPDATE duty_shift_group: ' . $res->getDebugInfo());
            return false;
        }

        // duty_shift_group_interval
        $sth = $this->db->prepare("DELETE FROM duty_shift_group_interval WHERE group_id = ?", array('text'), MDB2_PREPARE_MANIP);
        $res = $sth->execute($this->group_id());
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_Ward: ward_update: DELETE FROM duty_shift_group_interval: ' . $res->getDebugInfo());
            return false;
        }
        $sth = $this->db->prepare('
           INSERT INTO duty_shift_group_interval
           (group_id, no, atdptn_ptn_id, reason, interval_day)
           VALUES (:group_id, :no, :atdptn_ptn_id, :reason, :interval_day)
           ', array('text', 'integer', 'integer', 'integer', 'integer'));
        $res = $this->db->extended->executeMultiple($sth, $_POST['interval']);
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_Ward: ward_update: INSERT INTO duty_shift_group_interval: ' . $res->getDebugInfo());
            return false;
        }

        // duty_shift_group_upper_limit
        $sth = $this->db->prepare("DELETE FROM duty_shift_group_upper_limit WHERE group_id = ?", array('text'), MDB2_PREPARE_MANIP);
        $res = $sth->execute($this->group_id());
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_Ward: ward_update: DELETE FROM duty_shift_group_upper_limit: ' . $res->getDebugInfo());
            return false;
        }
        $sth = $this->db->prepare('
           INSERT INTO duty_shift_group_upper_limit
           (group_id, no, atdptn_ptn_id, reason, upper_limit_day)
           VALUES (:group_id, :no, :atdptn_ptn_id, :reason, :upper_limit_day)
           ', array('text', 'integer', 'integer', 'integer', 'text'));
        $res = $this->db->extended->executeMultiple($sth, $_POST['upper_limit']);
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_Ward: ward_update: INSERT INTO duty_shift_group_upper_limit: ' . $res->getDebugInfo());
            return false;
        }

        // duty_shift_group_seq_pattern
        $sth = $this->db->prepare("DELETE FROM duty_shift_group_seq_pattern WHERE group_id = ?", array('text'), MDB2_PREPARE_MANIP);
        $res = $sth->execute($this->group_id());
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_Ward: ward_update: DELETE FROM duty_shift_group_seq_pattern: ' . $res->getDebugInfo());
            return false;
        }
        $sth = $this->db->prepare('
           INSERT INTO duty_shift_group_seq_pattern
           (group_id, no, today_atdptn_ptn_id, today_reason, nextday_atdptn_ptn_id, nextday_reason)
           VALUES (:group_id, :no, :today_atdptn_ptn_id, :today_reason, :nextday_atdptn_ptn_id, :nextday_reason)
           ', array('text', 'int', 'int', 'int', 'int', 'int'));
        $res = $this->db->extended->executeMultiple($sth, $_POST['pattern_ok']);
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_Ward: ward_update: INSERT INTO duty_shift_group_seq_pattern: ' . $res->getDebugInfo());
            return false;
        }

        // duty_shift_group_ng_pattern
        $sth = $this->db->prepare("DELETE FROM duty_shift_group_ng_pattern WHERE group_id = ?", array('text'), MDB2_PREPARE_MANIP);
        $res = $sth->execute($this->group_id());
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_Ward: ward_update: DELETE FROM duty_shift_group_ng_pattern: ' . $res->getDebugInfo());
            return false;
        }
        $sth = $this->db->prepare('
           INSERT INTO duty_shift_group_ng_pattern
           (group_id, no, today_atdptn_ptn_id, today_reason, nextday_atdptn_ptn_id, nextday_reason)
           VALUES (:group_id, :no, :today_atdptn_ptn_id, :today_reason, :nextday_atdptn_ptn_id, :nextday_reason)
           ', array('text', 'int', 'int', 'int', 'int', 'int'));
        $res = $this->db->extended->executeMultiple($sth, $_POST['pattern_ng']);
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_Ward: ward_update: INSERT INTO duty_shift_group_ng_pattern: ' . $res->getDebugInfo());
            return false;
        }

        // duty_shift_group_sex_pattern
        $sth = $this->db->prepare("DELETE FROM duty_shift_group_sex_pattern WHERE group_id = ?", array('text'), MDB2_PREPARE_MANIP);
        $res = $sth->execute($this->group_id());
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_Ward: ward_update: DELETE FROM duty_shift_group_sex_pattern: ' . $res->getDebugInfo());
            return false;
        }
        $sth = $this->db->prepare('
           INSERT INTO duty_shift_group_sex_pattern
           (group_id, no, ptn_id)
           VALUES (:group_id, :no, :ptn_id)
           ', array('text', 'integer', 'text'));
        $res = $this->db->extended->executeMultiple($sth, $_POST['pattern_sex']);
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_Ward: ward_update: INSERT INTO duty_shift_group_sex_pattern: ' . $res->getDebugInfo());
            return false;
        }

        // duty_shift_group_hol_pattern
        $sth = $this->db->prepare("DELETE FROM duty_shift_group_hol_pattern WHERE group_id = ?", array('text'), MDB2_PREPARE_MANIP);
        $res = $sth->execute($this->group_id());
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_Ward: ward_update: DELETE FROM duty_shift_group_hol_pattern: ' . $res->getDebugInfo());
            return false;
        }
        $sth = $this->db->prepare('
           INSERT INTO duty_shift_group_hol_pattern
           (group_id, no, ptn_id)
           VALUES (:group_id, :no, :ptn_id)
           ', array('text', 'integer', 'text'));
        $res = $this->db->extended->executeMultiple($sth, $_POST['pattern_holiday']);
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_Ward: ward_update: INSERT INTO duty_shift_group_hol_pattern: ' . $res->getDebugInfo());
            return false;
        }

        // duty_shift_group_st_name
        $sth = $this->db->prepare("DELETE FROM duty_shift_group_st_name WHERE group_id = ?", array('text'), MDB2_PREPARE_MANIP);
        $res = $sth->execute($this->group_id());
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_Ward: ward_update: DELETE FROM duty_shift_group_st_name: ' . $res->getDebugInfo());
            return false;
        }
        $sth = $this->db->prepare('
           INSERT INTO duty_shift_group_st_name
           (group_id, no, st_name)
           VALUES (:group_id, :no, :st_name)
           ', array('text', 'integer', 'text'));
        $res = $this->db->extended->executeMultiple($sth, $_POST['print_st_names']);
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_Ward: ward_update: INSERT INTO duty_shift_group_st_name: ' . $res->getDebugInfo());
            return false;
        }

        // commit
        $this->db->commit();
        return true;
    }

    // 職員更新
    function staff_update($post)
    {
        $data = array();
        $no = 1;
        foreach ($post['order'] as $emp_id) {
            $data[] = array(
                'group_id' => $this->group_id(),
                'tmcd_group_id' => $this->pattern_id(),
                'emp_id' => $emp_id,
                'no' => $no++,
                'team_id' => $post['team'][$emp_id],
            );
        }

        $this->db->beginTransaction();

        // DELETE
        $sth = $this->db->prepare("DELETE FROM duty_shift_staff WHERE group_id = ?", array('text'), MDB2_PREPARE_MANIP);
        $res = $sth->execute($this->group_id());
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_Ward: staff_update: DELETE FROM duty_shift_staff: ' . $res->getDebugInfo());
            return false;
        }

        // INSERT
        $sth = $this->db->prepare('
           INSERT INTO duty_shift_staff
           (group_id, emp_id, no, team_id)
           VALUES (:group_id, :emp_id, :no, :team_id)
           ', array('text', 'text', 'int', 'int'), MDB2_PREPARE_MANIP);
        $res = $this->db->extended->executeMultiple($sth, $data);
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_Ward: staff_update: INSERT INTO duty_shift_staff: ' . $res->getDebugInfo());
            return false;
        }

        // empcond UPDATE
        $sth = $this->db->prepare('UPDATE empcond SET tmcd_group_id = :tmcd_group_id WHERE emp_id = :emp_id', array('int', 'text'), MDB2_PREPARE_MANIP);
        $res = $this->db->extended->executeMultiple($sth, $data);
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_Ward: staff_update: UPDATE empcond: ' . $res->getDebugInfo());
            return false;
        }

        // commit
        $this->db->commit();
        return true;
    }

    // _new_group_id
    function _new_group_id()
    {
        $max = $this->db->queryOne("SELECT MAX(substring(group_id from 5)) FROM duty_shift_group");
        return sprintf("%04d%08d", date('ym'), $max + 1);
    }

    // get
    function group_id()
    {
        return $this->_getter(__FUNCTION__);
    }

    function group_name()
    {
        return $this->_getter(__FUNCTION__);
    }

    function standard_id()
    {
        return $this->_getter(__FUNCTION__);
    }

    function pattern_id()
    {
        return $this->_getter(__FUNCTION__);
    }

    function close_day()
    {
        return $this->_getter(__FUNCTION__);
    }

    function duty_upper_limit_day()
    {
        return $this->_getter(__FUNCTION__);
    }

    function order_no()
    {
        return $this->_getter(__FUNCTION__);
    }

    function start_day1()
    {
        return $this->_getter(__FUNCTION__);
    }

    function month_flg1()
    {
        return $this->_getter(__FUNCTION__);
    }

    function end_day1()
    {
        return $this->_getter(__FUNCTION__);
    }

    function start_day2()
    {
        return $this->_getter(__FUNCTION__);
    }

    function month_flg2()
    {
        return $this->_getter(__FUNCTION__);
    }

    function end_day2()
    {
        return $this->_getter(__FUNCTION__);
    }

    function four_week_chk_flg()
    {
        return $this->_getter(__FUNCTION__);
    }

    function chk_start_date()
    {
        return preg_replace("/(\d\d\d\d)(\d\d)(\d\d)/", "$1-$2-$3", $this->_row["chk_start_date"]);
    }

    function start_month_flg1()
    {
        return $this->_getter(__FUNCTION__);
    }

    function need_less_chk_flg()
    {
        return $this->_getter(__FUNCTION__);
    }

    function need_more_chk_flg()
    {
        return $this->_getter(__FUNCTION__);
    }

    function print_title()
    {
        return $this->_getter(__FUNCTION__);
    }

    function reason_setting_flg()
    {
        return $this->_getter(__FUNCTION__);
    }

    function auto_check_months()
    {
        return $this->_getter(__FUNCTION__);
    }

    function auto_shift_filler()
    {
        return $this->_getter(__FUNCTION__);
    }

    function sfc_group_code()
    {
        return $this->_getter(__FUNCTION__);
    }

    function team_disp_flg()
    {
        return $this->_getter(__FUNCTION__);
    }

    function job_disp_flg()
    {
        return $this->_getter(__FUNCTION__);
    }

    function st_disp_flg()
    {
        return $this->_getter(__FUNCTION__);
    }

    function month_type()
    {
        return $this->_getter(__FUNCTION__);
    }

    function kaitori_day()
    {
        return $this->_getter(__FUNCTION__);
    }

    function plan_edit_flg()
    {
        return $this->_getter(__FUNCTION__) === "t";
    }

    function result_edit_flg()
    {
        return $this->_getter(__FUNCTION__) === "t";
    }

    // set
    function set_group_id($value)
    {
        $this->_row["group_id"] = $value;
    }

    // 施設基準
    function standard()
    {
        return new Cmx_Shift_Institution($this->_row["standard_id"]);
    }

    // 勤務パターングループ
    function pattern_group()
    {
        return new Cmx_Shift_PatternGroup($this->_row["pattern_id"]);
    }

    // シフト管理者
    function administrator()
    {
        $taker = array();

        if (is_array($this->_row["administrator"])) {
            $this->set_administrator($this->_row["administrator"]);
        }

        if ($this->_row["person_charge_id"]) {
            array_push($taker, new Cmx_Employee($this->_row["person_charge_id"]));
        }
        if ($this->_row["caretaker_id"]) {
            array_push($taker, new Cmx_Employee($this->_row["caretaker_id"]));
        }
        if ($this->_row["caretaker2_id"]) {
            array_push($taker, new Cmx_Employee($this->_row["caretaker2_id"]));
        }
        if ($this->_row["caretaker3_id"]) {
            array_push($taker, new Cmx_Employee($this->_row["caretaker3_id"]));
        }
        if ($this->_row["caretaker4_id"]) {
            array_push($taker, new Cmx_Employee($this->_row["caretaker4_id"]));
        }
        if ($this->_row["caretaker5_id"]) {
            array_push($taker, new Cmx_Employee($this->_row["caretaker5_id"]));
        }
        if ($this->_row["caretaker6_id"]) {
            array_push($taker, new Cmx_Employee($this->_row["caretaker6_id"]));
        }
        if ($this->_row["caretaker7_id"]) {
            array_push($taker, new Cmx_Employee($this->_row["caretaker7_id"]));
        }
        if ($this->_row["caretaker8_id"]) {
            array_push($taker, new Cmx_Employee($this->_row["caretaker8_id"]));
        }
        if ($this->_row["caretaker9_id"]) {
            array_push($taker, new Cmx_Employee($this->_row["caretaker9_id"]));
        }
        if ($this->_row["caretaker10_id"]) {
            array_push($taker, new Cmx_Employee($this->_row["caretaker10_id"]));
        }
        return $taker;
    }

    // シフト管理者:設定
    function set_administrator($values)
    {
        if (is_array($values)) {
            list(
                $this->_row["person_charge_id"],
                $this->_row["caretaker_id"],
                $this->_row["caretaker2_id"],
                $this->_row["caretaker3_id"],
                $this->_row["caretaker4_id"],
                $this->_row["caretaker5_id"],
                $this->_row["caretaker6_id"],
                $this->_row["caretaker7_id"],
                $this->_row["caretaker8_id"],
                $this->_row["caretaker9_id"],
                $this->_row["caretaker10_id"]
                ) = $values;
        }
    }

    // シフト管理者の判定
    function is_administrator($emp_id)
    {
        if (
            $emp_id === '000000000000' ||
            $emp_id === $this->_row["person_charge_id"] ||
            $emp_id === $this->_row["caretaker_id"] ||
            $emp_id === $this->_row["caretaker2_id"] ||
            $emp_id === $this->_row["caretaker3_id"] ||
            $emp_id === $this->_row["caretaker4_id"] ||
            $emp_id === $this->_row["caretaker5_id"] ||
            $emp_id === $this->_row["caretaker6_id"] ||
            $emp_id === $this->_row["caretaker7_id"] ||
            $emp_id === $this->_row["caretaker8_id"] ||
            $emp_id === $this->_row["caretaker9_id"] ||
            $emp_id === $this->_row["caretaker10_id"]) {
            return true;
        }
        return false;
    }

    // 部門長
    function manager()
    {
        if (!empty($this->_row["manager"])) {
            return (new Cmx_Employee($this->_row["manager"]));
        }
        return false;
    }

    // 勤務パターン
    function pattern()
    {
        if ($this->_pattern) {
            return $this->_pattern;
        }
        $this->_pattern = new Cmx_Shift_Pattern($this->_row["pattern_id"]);
        return $this->_pattern;
    }

    // 勤務パターンリスト
    function pattern_list()
    {
        $pattern = $this->pattern();
        return $pattern->lists();
    }

    // 勤務パターンsignリスト
    function pattern_sign_list()
    {
        $pattern = $this->pattern();
        return $pattern->sign_lists();
    }

    // 勤務パターンJSON
    function pattern_json()
    {
        $pattern = $this->pattern();
        return $pattern->json();
    }

    // 勤務パターン 集計行
    function count_row_lists()
    {
        $pattern = $this->pattern();
        return $pattern->count_row_lists();
    }

    // 勤務パターン 集計列
    function count_col_lists()
    {
        $pattern = $this->pattern();
        return $pattern->count_col_lists();
    }

    // 勤務シフト間隔
    function pattern_interval()
    {
        $obj = new Cmx_Shift_PatternInterval($this->_row["group_id"]);
        return $obj->lists();
    }

    // 連続勤務シフト上限
    function pattern_upper_limit()
    {
        $obj = new Cmx_Shift_PatternUpperLimit($this->_row["group_id"]);
        return $obj->lists();
    }

    // 組み合わせシフト
    function pattern_ok()
    {
        $obj = new Cmx_Shift_PatternOK($this->_row["group_id"]);
        return $obj->lists();
    }

    // 組み合わせシフト(ハッシュ)
    function pattern_ok_hash()
    {
        $obj = new Cmx_Shift_PatternOK($this->_row["group_id"]);
        return $obj->hash_lists();
    }

    // 組み合わせ禁止シフト
    function pattern_ng()
    {
        $obj = new Cmx_Shift_PatternNG($this->_row["group_id"]);
        return $obj->lists();
    }

    // 同時勤務の性別を固定するシフト
    function pattern_sex()
    {
        $obj = new Cmx_Shift_PatternSex($this->_row["group_id"]);
        return $obj->lists();
    }

    // ４週８休シフト種類
    function pattern_holiday()
    {
        $obj = new Cmx_Shift_PatternHoliday($this->_row["group_id"]);
        return $obj->lists();
    }

    // 決済欄役職
    function print_st_names()
    {
        $obj = new Cmx_Shift_PrintStatusNames($this->_row["group_id"]);
        return $obj->lists();
    }

    // is_exists_group_id
    // 部署IDチェック
    function is_exists_group_id($group_id)
    {
        $is = $this->db->extended->getOne(
            "SELECT COUNT(*) FROM duty_shift_group WHERE group_id= ?", null, array($group_id), array('text')
        );
        return $is > 0 ? true : false;
    }

    // is_exists_code
    // 連携コードチェック
    function is_exists_code($code)
    {
        if ($this->group_id()) {
            $is = $this->db->extended->getOne(
                "SELECT COUNT(*) FROM duty_shift_group WHERE group_id!= ? AND sfc_group_code= ?", null, array($this->group_id(), $code), array('text', 'text')
            );
        }
        else {
            $is = $this->db->extended->getOne(
                "SELECT COUNT(*) FROM duty_shift_group WHERE sfc_group_code= ?", null, array($code), array('text')
            );
        }
        return $is > 0 ? true : false;
    }

    // 開始日
    function start_date($year, $month)
    {
        $sdate = $this->shift_date();
        return $sdate->start_date($year, $month);
    }

    function start_ymd($year, $month)
    {
        $sdate = $this->shift_date();
        return $sdate->start_ymd($year, $month);
    }

    // 終了日
    function end_date($year, $month)
    {
        $sdate = $this->shift_date();
        return $sdate->end_date($year, $month);
    }

    function end_ymd($year, $month)
    {
        $sdate = $this->shift_date();
        return $sdate->end_ymd($year, $month);
    }

    // dates
    function dates($year, $month)
    {
        if ($this->_dates["$year/$month"]) {
            return $this->_dates["$year/$month"];
        }
        $sdate = $this->shift_date();
        $cal = new Cmx_Calendar();
        $this->_dates["$year/$month"] = $cal->lists(
            $sdate->start_date($year, $month), $sdate->end_date($year, $month));
        return $this->_dates["$year/$month"];
    }

    // dates_in
    function dates_in($year, $month)
    {
        if ($this->_dates_in["$year/$month"]) {
            return $this->_dates_in["$year/$month"];
        }
        $date_array = array();
        foreach ($this->dates($year, $month) as $row) {
            $date_array[] = sprintf("'%s'", $row["date"]);
        }
        $this->_dates_in["$year/$month"] = implode(',', $date_array);
        return $this->_dates_in["$year/$month"];
    }

    /**
     * 指定年月の下書きシフトが存在するかどうかを確認するメソッド
     *
     * @param type $year    年
     * @param type $month   月
     * @return type true:あり false:なし
     */
    function is_draft($year, $month)
    {
        $is = $this->db->extended->getOne("
            SELECT COUNT(*) FROM duty_shift_plan_draft
            WHERE group_id= :group_id AND duty_date BETWEEN :start_date AND :end_date
        ", null, array(
            'group_id' => $this->group_id(),
            'start_date' => $this->start_ymd($year, $month),
            'end_date' => $this->end_ymd($year, $month),
            ), array('text', 'text', 'text'));
        return $is > 0 ? true : false;
    }

    /**
     * 指定年月の登録シフトが存在するかどうかを確認するメソッド
     *
     * @param type $year    年
     * @param type $month   月
     * @return type true:あり false:なし
     */
    function is_plan($year, $month)
    {
        $is = $this->db->extended->getOne("
            SELECT COUNT(*) FROM duty_shift_plan_staff JOIN atdbk USING (emp_id)
            WHERE group_id= :group_id AND group_id=main_group_id
              AND duty_yyyy= :year AND duty_mm= :month
              AND date BETWEEN :start_date AND :end_date
              AND pattern != '0' AND pattern IS NOT NULL
        ", null, array(
            'group_id' => $this->group_id(),
            'start_date' => $this->start_ymd($year, $month),
            'end_date' => $this->end_ymd($year, $month),
            'year' => $year,
            'month' => $month,
            ), array('text', 'int', 'int', 'text', 'text'));
        return $is > 0 ? true : false;
    }

    /**
     * 予定表の登録ステータス
     *
     * @param type $year    年
     * @param type $month   月
     * @return type 0:未登録 or -1:下書き or 1:登録済
     */
    function status($year, $month)
    {
        if (!$this->_staff->is_plan_staff($this->group_id(), $year, $month)) {
            return 0;
        }
        if ($this->is_plan($year, $month)) {
            return 1;
        }
        else if ($this->is_draft($year, $month)) {
            return -1;
        }
        return 0;
    }

    /**
     * 参照可能な職員
     * @return type
     */
    function access_user()
    {
        $access = new Cmx_Shift_WardAccessUser($this->group_id());
        return $access->lists();
    }

    /**
     * 参照可能な属性
     * @return type
     */
    function access_permission()
    {
        $access = new Cmx_Shift_WardAccessPermission($this->group_id());
        return $access->lists();
    }

    /**
     * 表示項目
     * @param str $display 表示項目キー
     * @return str 表示項目
     */
    function display($key = null)
    {
        if (!$this->_display) {
            $this->_display = unserialize($this->_row['display']);
            $this->_display['team'] = $this->team_disp_flg() === 't' ? '1' : '0';
            $this->_display['job'] = $this->job_disp_flg() === 't' ? '1' : '0';
            $this->_display['status'] = $this->st_disp_flg() === 't' ? '1' : '0';
        }
        if ($key) {
            return $this->_display[$key];
        }
        else {
            return cmx_json_encode($this->_display);
        }
    }

}
