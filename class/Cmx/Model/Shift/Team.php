<?php

//
// 勤務表：部署チームクラス
//
//
require_once('Cmx.php');
require_once('Cmx/Core/Model.php');

class Cmx_Shift_Team extends Model
{

    var $_row;

    // コンストラクタ
    function Cmx_Shift_Team($row = null)
    {
        parent::connectDB();

        if (is_array($row)) {
            $this->_init($row);
        }
        else if (!is_null($row)) {
            $this->_select($row);
        }
    }

    // _init
    function _init($row)
    {
        $this->_row = $row;
    }

    // _select
    function _select($row)
    {
        $sth = $this->db->prepare("
            SELECT *
            FROM duty_shift_staff_team
            WHERE team_id= ?
        ");
        $res = $sth->execute($row);
        $this->_init($res->fetchRow(MDB2_FETCHMODE_ASSOC));
    }

    // 部署チームリスト
    function lists()
    {
        $sth = $this->db->prepare("
                SELECT * FROM duty_shift_staff_team ORDER BY no
            ");
        $res = $sth->execute();

        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            array_push($list, $row);
        }
        return $list;
    }

}
