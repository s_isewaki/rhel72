<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'Cmx/Shift/Log.php';
require_once 'Cmx/Model/EmployeeClass.php';

/*
 * 勤務表：CSVレイアウト
 */

/* CSVレイアウトデータマップ例
 *
 * array(
 *      1 => array(
 *          'encoding' => 'Shift_JIS',
 *          'title' => 'COMPANY月次',
 *          'layout' => array(
 *              array(header => '4', title => '社員番号', type => 'emp_personal_id'),
 *              array(header => 'SDATE', title => '開始日', type => 'start_date'),
 *              array(header => 'EDATE', title => '終了日', type => 'end_date'),
 *              array(header => '2401', title => '有休前年繰越日数', type => 'holiday_kurikoshi'),
 *              array(header => '2445', title => '日勤', type => 'count', count => array(
 *                  array('id' => '01-01-00', coeffient => '1.0'),
 *                  array('id' => '02-01-00', coeffient => '1.0'),
 *              ),
 *          ),
 *      );
 * );
 *
 */

class Cmx_Shift_DataLayout extends Model
{

    /**
     * constractor
     * @param type $emp_id
     */
    function __construct()
    {
        parent::connectDB();
        $this->_log = new Cmx_Shift_Log('data_output');
    }

    /**
     * CSVレイアウト一覧の取得
     * @return type
     */
    function lists()
    {
        $res = $this->db->query("SELECT * FROM duty_shift_data_layout ORDER BY order_no, id");

        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['layout'] = unserialize($row['layout']);
            $list[] = $row;
        }
        return $list;
    }

    /**
     * CSVレイアウトの取得
     * @param type $id
     * @return type
     */
    function layout($id)
    {
        $data = $this->db->extended->getRow("SELECT * FROM duty_shift_data_layout WHERE id = ?", null, array($id), array('integer'), MDB2_FETCHMODE_ASSOC);
        if (empty($data)) {
            return array();
        }

        $data['layout'] = $data['layout'] ? unserialize($data['layout']) : array();
        return $data;
    }

    /**
     * CSVレイアウトのCSV取得
     * @param type $id
     * @return type
     */
    function layout_csv($id)
    {
        $layout = $this->layout($id);

        $type = array();
        foreach ($this->layout_lists() as $data) {
            $type[$data['id']] = $data['name'];
        }

        // CSV出力バッファ
        $fp = fopen('php://temp', 'r+b');

        // ヘッダ
        fputcsv($fp, array('ヘッダ', '名称', '種別'));

        // 本体
        foreach ($layout['layout'] as $row) {
            $csv = array(
                $row['header'],
                $row['name'],
                $type[$row['type']],
            );
            fputcsv($fp, $csv);
        }

        rewind($fp);
        return str_replace(PHP_EOL, "\r\n", stream_get_contents($fp));
    }

    /**
     * CSVレイアウトの保存
     * @param type $layout
     */
    public function save($data)
    {
        $layout = array();
        foreach ($data['order'] as $i) {

            $count = array();
            foreach ($data['count'][$i]['pattern'] as $j => $pattern) {
                $count[] = array(
                    'pattern' => $pattern,
                    'coeffient' => $data['count'][$i]['coeffient'][$j],
                );
            }

            $layout[] = array(
                'header' => $data['header'][$i],
                'name' => $data['name'][$i],
                'type' => $data['type'][$i],
                'count' => $count,
                'format' => $data['format'][$i],
            );
        }
        $data['layout'] = serialize($layout);

        $this->db->beginTransaction();

        // UPDATE
        if (!empty($data['id'])) {
            $sth = $this->db->prepare(
                "UPDATE duty_shift_data_layout SET title= :title, encoding= :encoding, layout= :layout WHERE id= :id", array('text', 'text', 'text', 'integer'), MDB2_PREPARE_MANIP
            );
            $res = $sth->execute($data);
            if (PEAR::isError($res)) {
                $this->db->rollback();
                cmx_log('ROLLBACK: Cmx_Shift_DataLayout: UPDATE: duty_shift_data_layout: ' . $res->getDebugInfo());
                return false;
            }
        }

        // INSERT
        else {
            $sth = $this->db->prepare(
                "INSERT INTO duty_shift_data_layout (title, encoding, layout) VALUES (:title, :encoding, :layout)", array('text', 'text', 'text'), MDB2_PREPARE_MANIP
            );
            $res = $sth->execute($data);
            if (PEAR::isError($res)) {
                $this->db->rollback();
                cmx_log('ROLLBACK: Cmx_Shift_DataLayout: INSERT: duty_shift_data_layout: ' . $res->getDebugInfo());
                return false;
            }
        }
        $this->db->commit();

        $this->_log->log('CSVレイアウトの保存.完了', $data['title']);
    }

    /**
     * CSVレイアウト一覧
     * @return type
     */
    public function layout_lists()
    {
        $ec = new Cmx_EmployeeClass();

        return array(
            array('id' => 'start_date', 'name' => '開始日'),
            array('id' => 'end_date', 'name' => '終了日'),
            array('id' => 'sokyu_stuki', 'name' => '遡及月'),
            array('id' => 'emp_personal_id', 'name' => '職員ID'),
            array('id' => 'emp_name', 'name' => '職員氏名'),
            array('id' => 'emp_kana', 'name' => '職員氏名かな'),
            array('id' => 'group_name', 'name' => '部署'),
            array('id' => 'class_name', 'name' => $ec->class_name()),
            array('id' => 'atrb_name', 'name' => $ec->atrb_name()),
            array('id' => 'dept_name', 'name' => $ec->dept_name()),
            array('id' => 'st_name', 'name' => '役職'),
            array('id' => 'job_name', 'name' => '職種'),
            array('id' => 'wage_name', 'name' => '給与支給区分'),
            array('id' => 'duty_form_name', 'name' => '常勤・非常勤'),
            array('id' => 'holiday_date', 'name' => '有休:付与日'),
            array('id' => 'holiday_mae_kuri', 'name' => '有休:前年繰越日数'),
            array('id' => 'holiday_mae_fuyo', 'name' => '有休:前年付与日数'),
            array('id' => 'holiday_mae_adjust', 'name' => '有休:前年調整日数'),
            array('id' => 'holiday_mae_use', 'name' => '有休:前年取得日数'),
            array('id' => 'holiday_mae_zan', 'name' => '有休:前年残日数'),
            array('id' => 'holiday_kuri', 'name' => '有休:当年繰越日数'),
            array('id' => 'holiday_fuyo', 'name' => '有休:当年付与日数'),
            array('id' => 'holiday_adjust', 'name' => '有休:当年調整日数'),
            array('id' => 'holiday_use', 'name' => '有休:当年取得日数'),
            array('id' => 'summer_date', 'name' => '夏休:付与日'),
            array('id' => 'summer_fuyo', 'name' => '夏休:付与日数'),
            array('id' => 'summer_use', 'name' => '夏休:取得日数'),
            array('id' => 'week_date', 'name' => '週休:付与日'),
            array('id' => 'week_fuyo', 'name' => '週休:付与日数'),
            array('id' => 'week_use', 'name' => '週休:取得日数'),
            array('id' => 'c:totalworktime', 'name' => '勤務時間(残業時間含む)'),
            array('id' => 'c:worktime', 'name' => '勤務時間(残業時間含まない)'),
            array('id' => 'c:overtime', 'name' => '残業時間'),
            array('id' => 'c:over100', 'name' => '時間外 100/100'),
            array('id' => 'c:over125', 'name' => '時間外 125/100'),
            array('id' => 'c:over25', 'name' => '時間外 25/100'),
            array('id' => 'c:over135', 'name' => '時間外 135/100'),
            array('id' => 'c:over150', 'name' => '時間外 150/100'),
            array('id' => 'c:newyear1', 'name' => '年末年始手当 12/31,1/1 勤務時間4h以下の回数'),
            array('id' => 'c:newyear2', 'name' => '年末年始手当 12/31,1/1 勤務時間4h〜10hの回数'),
            array('id' => 'c:newyear3', 'name' => '年末年始手当 12/31,1/1 勤務時間10h〜14hの回数'),
            array('id' => 'c:newyear4', 'name' => '年末年始手当 12/31,1/1 勤務時間14h超えの回数'),
            array('id' => 'c:newyear5', 'name' => '年末年始手当 12/30,1/2,1/3,1/4 勤務時間4h以下の回数'),
            array('id' => 'c:newyear6', 'name' => '年末年始手当 12/30,1/2,1/3,1/4 勤務時間4h〜10hの回数'),
            array('id' => 'c:newyear7', 'name' => '年末年始手当 12/30,1/2,1/3,1/4 勤務時間10h〜14hの回数'),
            array('id' => 'c:newyear8', 'name' => '年末年始手当 12/30,1/2,1/3,1/4 勤務時間14h超えの回数'),
            array('id' => 'c:conference_minutes', 'name' => 'カンファレンス時間'),
            array('id' => 'c:base_shorter_minutes', 'name' => '時短時間(減算前)'),
            array('id' => 'c:real_shorter_minutes', 'name' => '時短時間(減算後)'),
            array('id' => 'c:base_shorter_minutes1', 'name' => '時短時間(減算前):平日'),
            array('id' => 'c:real_shorter_minutes1', 'name' => '時短時間(減算後):平日'),
            array('id' => 'c:base_shorter_minutes6', 'name' => '時短時間(減算前):土曜'),
            array('id' => 'c:real_shorter_minutes6', 'name' => '時短時間(減算後):土曜'),
            array('id' => 'c:workday_count', 'name' => '出勤日数'),
//            array('id' => 'c:absence_days', 'name' => '欠勤日数'),
            array('id' => 'count', 'name' => '勤務記号'),
            array('id' => 'empty', 'name' => '空セル'),
            array('id' => 'zero', 'name' => 'ゼロ'),
        );
    }

    /**
     * CSVレイアウト 日付フォーマット
     * @return type
     */
    public function layout_date_lists()
    {
        return array(
            array('id' => 'Ymd', 'name' => 'YYYYMMDD'),
            array('id' => 'Y/m/d', 'name' => 'YYYY/MM/DD'),
            array('id' => 'Y-m-d', 'name' => 'YYYY-MM-DD'),
        );
    }

    /**
     * CSVレイアウト 勤務記号
     * @return type
     */
    public function pattern_lists()
    {
        $res = $this->db->query("
            SELECT id, name FROM (
                SELECT
                    '0-10-' || r.reason_id AS id,
                    COALESCE(r.display_name, r.default_name) AS name
                FROM
                atdbk_reason_mst r
                JOIN duty_shift_pattern p ON p.pattern_id=1 AND p.atdptn_ptn_id=10 AND p.reason=r.reason_id
                WHERE display_flag
                ORDER BY p.order_no
            ) a

            UNION ALL

            SELECT id, name FROM (
                SELECT
                    g.group_id || '-' || atdptn_id || '-00' AS id,
                    group_name || ' > ' || atdptn_nm AS name
                FROM
                wktmgrp g
                JOIN atdptn p USING (group_id)
                WHERE p.display_flag
                AND   p.atdptn_id != 10
                ORDER BY g.group_id, p.atdptn_order_no
            ) b
        ");

        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[] = $row;
        }
        return $list;
    }

}
