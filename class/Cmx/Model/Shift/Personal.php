<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'Cmx/Model/Calendar.php';
require_once 'Cmx/Model/Shift/Ward.php';
require_once 'Cmx/Model/Shift/Staff.php';
require_once 'Cmx/Model/Shift/Pattern.php';
require_once 'Cmx/Model/Shift/Approval.php';
require_once 'Cmx/Model/Timecard/Overtime.php';
require_once 'Cmx/Model/Timecard/OvertimeReason.php';
require_once 'Cmx/Shift/Date/Months.php';
require_once 'Cmx/Shift/Utils.php';
require_once 'Cmx/Shift/Log.php';

/**
 * 勤務表：個人勤務表クラス
 *
 * 個人の勤務予定、実績、希望、当直、コメントのデータを取得、更新を行うクラス
 *
 */
class Cmx_Shift_Personal extends Model
{

    var $_emp_id;
    var $_shift_admin;
    var $_pattern;
    var $_staff;
    var $_ward;
    var $_tbl;
    var $_sdate;
    var $_login_id;
    var $log;

    /**
     * コンストラクタ
     */
    function Cmx_Shift_Personal($emp_id, $mode = null)
    {
        parent::connectDB();
        $this->_emp_id = $emp_id;
        $this->_staff = new Cmx_Shift_Staff($emp_id);
        $this->_ward = new Cmx_Shift_Ward($this->_staff->group_id());
        if ($this->_staff->group_id()) {
            $this->_sdate = $this->_ward->shift_date($mode);
        }
        else {
            $this->_sdate = new Cmx_Shift_Date_Months(1, 1);
        }

        $this->log = new Cmx_Shift_log('personal');
    }

    /**
     * ログインIDのセット
     */
    function set_login_id($login_id)
    {
        $this->_login_id = $login_id;
    }

    /**
     * 部署データのゲット
     */
    function get_ward()
    {
        return $this->_ward;
    }

    /**
     * 部署スタッフデータのゲット
     */
    function get_staff()
    {
        return $this->_staff;
    }

    /**
     * Cmx_Shift_Dateオブジェクト
     */
    function sdate()
    {
        return $this->_sdate;
    }

    /**
     * 勤務表データのセット
     */
    function set_table($data)
    {
        $this->_tbl = $data;
    }

    /**
     * 勤務表データのゲット
     */
    function get_table()
    {
        return $this->_tbl;
    }

    /**
     * 勤務表データ(職員情報)のゲット
     */
    function get_table_emp()
    {
        if ($this->_tbl['emp']) {
            return $this->_tbl['emp'];
        }
        return array();
    }

    /**
     * 勤務表データJSON出力
     */
    function table_json()
    {
        return cmx_json_encode($this->_tbl);
    }

    /**
     * 個人勤務表テーブルの取得
     */
    function table($date)
    {
        $date_ymd = preg_replace("/^(\d+)\-(\d+)\-(\d+)/", "$1$2$3", $date);

        $this->_tbl = array();

        // スタッフ
        $staff = array(
            'name' => $this->_staff->emp_name(),
            'utf8_name' => mb_convert_encoding($this->_staff->emp_name(), 'utf-8', 'euc-jp'),
            'kana' => $this->_staff->emp_kana(),
            'login' => $this->_staff->emp_login_id(),
            'sex' => $this->_staff->emp_sex(),
            'job_id' => $this->_staff->job_id(),
            'job_name' => $this->_staff->job_name(),
            'job_code' => $this->_staff->job_code(),
            'st_id' => $this->_staff->st_id(),
            'st_name' => $this->_staff->st_name(),
            'st_code' => $this->_staff->st_code(),
            'class_id' => $this->_staff->class_id(),
            'class_name' => $this->_staff->class_name(),
            'class_code' => $this->_staff->class_code(),
            'atrb_id' => $this->_staff->atrb_id(),
            'atrb_name' => $this->_staff->atrb_name(),
            'atrb_code' => $this->_staff->atrb_code(),
            'dept_id' => $this->_staff->dept_id(),
            'dept_name' => $this->_staff->dept_name(),
            'dept_code' => $this->_staff->dept_code(),
            'room_id' => $this->_staff->room_id(),
            'room_name' => $this->_staff->room_name(),
            'room_code' => $this->_staff->room_code(),
            'team' => $this->_staff->team_name(),
            'team_id' => $this->_staff->team_id(),
            'carryover' => $this->_staff->last_carryover_min(),
            'assist' => $this->_staff->assist(),
            'no' => $this->_staff->order_no(),
            'memo' => $this->_staff->memo(),
        );

        // 履歴があればマージ
        $history = $this->_staff->history($date);
        if (isset($history[$this->_emp_id])) {
            $staff = array_merge($staff, $history[$this->_emp_id]);
        }

        // 実績
        $sth = $this->db->prepare("
            SELECT
                *,
                CASE WHEN pattern='10' THEN pattern||LPAD(reason,2,'0') ELSE LPAD(pattern,2,'0')||'00' END AS ptn,
                night_duty AS duty,

                over_start_next_day_flag AS over_start_next_day_flag1,
                over_end_next_day_flag AS over_end_next_day_flag1,

                SUBSTR(start_time,1,2) AS start_hour,
                SUBSTR(start_time,3,2) AS start_min,
                SUBSTR(end_time,1,2) AS end_hour,
                SUBSTR(end_time,3,2) AS end_min,

                SUBSTR(rest_start_time,1,2) AS rest_start_hour,
                SUBSTR(rest_start_time,3,2) AS rest_start_min,
                SUBSTR(rest_end_time,1,2) AS rest_end_hour,
                SUBSTR(rest_end_time,3,2) AS rest_end_min,

                SUBSTR(over_start_time,1,2) AS over_start_hour1,
                SUBSTR(over_start_time,3,2) AS over_start_min1,
                SUBSTR(over_end_time,1,2) AS over_end_hour1,
                SUBSTR(over_end_time,3,2) AS over_end_min1,

                SUBSTR(rest_start_time1,1,2) AS rest_over_start_hour1,
                SUBSTR(rest_start_time1,3,2) AS rest_over_start_min1,
                SUBSTR(rest_end_time1,1,2) AS rest_over_end_hour1,
                SUBSTR(rest_end_time1,3,2) AS rest_over_end_min1,

                SUBSTR(over_start_time2,1,2) AS over_start_hour2,
                SUBSTR(over_start_time2,3,2) AS over_start_min2,
                SUBSTR(over_end_time2,1,2) AS over_end_hour2,
                SUBSTR(over_end_time2,3,2) AS over_end_min2,

                SUBSTR(rest_start_time2,1,2) AS rest_over_start_hour2,
                SUBSTR(rest_start_time2,3,2) AS rest_over_start_min2,
                SUBSTR(rest_end_time2,1,2) AS rest_over_end_hour2,
                SUBSTR(rest_end_time2,3,2) AS rest_over_end_min2,

                SUBSTR(over_start_time3,1,2) AS over_start_hour3,
                SUBSTR(over_start_time3,3,2) AS over_start_min3,
                SUBSTR(over_end_time3,1,2) AS over_end_hour3,
                SUBSTR(over_end_time3,3,2) AS over_end_min3,

                SUBSTR(rest_start_time3,1,2) AS rest_over_start_hour3,
                SUBSTR(rest_start_time3,3,2) AS rest_over_start_min3,
                SUBSTR(rest_end_time3,1,2) AS rest_over_end_hour3,
                SUBSTR(rest_end_time3,3,2) AS rest_over_end_min3,


                SUBSTR(over_start_time4,1,2) AS over_start_hour4,
                SUBSTR(over_start_time4,3,2) AS over_start_min4,
                SUBSTR(over_end_time4,1,2) AS over_end_hour4,
                SUBSTR(over_end_time4,3,2) AS over_end_min4,

                SUBSTR(rest_start_time4,1,2) AS rest_over_start_hour4,
                SUBSTR(rest_start_time4,3,2) AS rest_over_start_min4,
                SUBSTR(rest_end_time4,1,2) AS rest_over_end_hour4,
                SUBSTR(rest_end_time4,3,2) AS rest_over_end_min4,

                SUBSTR(over_start_time5,1,2) AS over_start_hour5,
                SUBSTR(over_start_time5,3,2) AS over_start_min5,
                SUBSTR(over_end_time5,1,2) AS over_end_hour5,
                SUBSTR(over_end_time5,3,2) AS over_end_min5,

                SUBSTR(rest_start_time5,1,2) AS rest_over_start_hour5,
                SUBSTR(rest_start_time5,3,2) AS rest_over_start_min5,
                SUBSTR(rest_end_time5,1,2) AS rest_over_end_hour5,
                SUBSTR(rest_end_time5,3,2) AS rest_over_end_min5,

                ovtm_reason_id AS ovtm_reason_id1,
                ovtm_reason AS ovtm_reason1,

                atdbkrslt_ovtmrsn.emp_id AS ovtmrsn_emp_id
            FROM atdbkrslt
            LEFT JOIN atdbkrslt_ovtmrsn USING (emp_id,date)
            WHERE emp_id = :emp_id AND date = :date
        ");
        $res = $sth->execute(
            array(
                'emp_id' => $this->_emp_id,
                'date' => $date_ymd,
            )
        );
        $results = array();
        if ($res->numRows()) {
            $results = $res->fetchRow(MDB2_FETCHMODE_ASSOC);

            // 翌日
            if ($results['next_day_flag']) {
                $results['end_hour'] += 24;
            }

            // 残業
            foreach (range(1, 5) as $i) {
                // 翌日
                if ($results["over_start_next_day_flag{$i}"]) {
                    $results["over_start_hour{$i}"] += 24;
                }
                if ($results["over_end_next_day_flag{$i}"]) {
                    $results["over_end_hour{$i}"] += 24;
                }
                if ($results["rest_start_next_day_flag{$i}"]) {
                    $results["rest_over_start_hour{$i}"] += 24;
                }
                if ($results["rest_end_next_day_flag{$i}"]) {
                    $results["rest_over_end_hour{$i}"] += 24;
                }

                $results['over'][] = array(
                    "over_start_hour" => $results["over_start_hour{$i}"],
                    "over_start_min" => $results["over_start_min{$i}"],
                    "over_end_hour" => $results["over_end_hour{$i}"],
                    "over_end_min" => $results["over_end_min{$i}"],
                    "rest_over_start_hour" => $results["rest_over_start_hour{$i}"],
                    "rest_over_start_min" => $results["rest_over_start_min{$i}"],
                    "rest_over_end_hour" => $results["rest_over_end_hour{$i}"],
                    "rest_over_end_min" => $results["rest_over_end_min{$i}"],
                    "ovtm_reason_id" => $results["ovtm_reason_id{$i}"],
                    "ovtm_reason" => $results["ovtm_reason{$i}"],
                );

                $results["over_start_hour"][] = $results["over_start_hour{$i}"];
                $results["over_start_min"][] = $results["over_start_min{$i}"];
                $results["over_end_hour"][] = $results["over_end_hour{$i}"];
                $results["over_end_min"][] = $results["over_end_min{$i}"];
                $results["rest_over_start_hour"][] = $results["rest_over_start_hour{$i}"];
                $results["rest_over_start_min"][] = $results["rest_over_start_min{$i}"];
                $results["rest_over_end_hour"][] = $results["rest_over_end_hour{$i}"];
                $results["rest_over_end_min"][] = $results["rest_over_end_min{$i}"];
            }

            //　カンファレンス時間
            if ($results['conference_minutes']) {
                $results['conference_time'] = min2time($results['conference_minutes']);
            }
        }
        else {
            $results['tmcd_group_id'] = $this->_ward->pattern_id();
        }
        $sth->free();

        // 予定
        $sth_plan = $this->db->prepare("
            SELECT
                *,
                CASE WHEN pattern='10' THEN pattern||LPAD(reason,2,'0') ELSE LPAD(pattern,2,'0')||'00' END AS ptn
            FROM atdbk
            WHERE emp_id = :emp_id AND date = :date
        ");
        $res_plan = $sth_plan->execute(
            array(
                'emp_id' => $this->_emp_id,
                'date' => $date_ymd,
            )
        );
        $plan = array();
        if ($res_plan->numRows()) {
            $plan = $res_plan->fetchRow(MDB2_FETCHMODE_ASSOC);
        }
        else {
            $plan['tmcd_group_id'] = $this->_ward->pattern_id();
        }
        $sth_plan->free();

        // tbl
        $this->_tbl['emp_id'] = $this->_emp_id;
        $this->_tbl['date'] = $date;
        $this->_tbl['emp'] = $staff;
        $this->_tbl['results'] = $results;
        $this->_tbl['plan'] = $plan;
    }

    /**
     * 個人勤務表リストの取得
     */
    function lists($year, $month, $mode = null)
    {
        if ($mode !== 'month') {
            $start_date = $this->_sdate->start_ymd($year, $month);
            $end_date = $this->_sdate->end_ymd($year, $month);
        }
        else {
            $sdate = $this->_ward->shift_date($mode);
            $start_date = $sdate->start_ymd($year, $month);
            $end_date = $sdate->end_ymd($year, $month);
        }

        $pattern = new Cmx_Shift_Pattern();
        $ovtm = new Cmx_Timecard_Overtime();
        $ovtmrsn = new Cmx_Timecard_OvertimeReason();

        // 勤務データ
        $sth = $this->db->prepare("
            SELECT
                e.emp_id,
                c.date,
                c.type,
                DATE_PART('dow',TO_DATE(c.date,'YYYYMMDD')) AS week,
                LPAD(p.pattern,2,'0')||LPAD(COALESCE(p.reason,'00'),2,'0') AS plan_id,
                LPAD(r.pattern,2,'0')||LPAD(COALESCE(r.reason,'00'),2,'0') AS results_id,
                LPAD(h.atdptn_ptn_id::text,2,'0')||LPAD(COALESCE(h.reason,'00'),2,'0') AS hope_id,
                p.night_duty AS plan_duty,
                r.night_duty AS results_duty,
                p.tmcd_group_id AS plan_pattern_id,
                r.tmcd_group_id AS results_pattern_id,
                r.pattern AS results_pattern,
                r.reason AS results_reason,
                oh.workminutes,

                CASE WHEN r.start_time      != '' THEN r.start_time      ELSE REPLACE(oh.officehours2_start, ':', '') END AS start_time,
                CASE WHEN r.end_time        != '' THEN r.end_time        ELSE REPLACE(oh.officehours2_end,   ':', '') END AS end_time,
                CASE WHEN r.rest_start_time != '' THEN r.rest_start_time ELSE REPLACE(oh.officehours4_start, ':', '') END AS rest_start_time,
                CASE WHEN r.rest_end_time   != '' THEN r.rest_end_time   ELSE REPLACE(oh.officehours4_end,   ':', '') END AS rest_end_time,
                CASE WHEN r.end_time        != '' THEN r.next_day_flag WHEN oh.officehours2_start <> '' AND oh.officehours2_start >= oh.officehours2_end THEN 1 ELSE 0 END AS next_day_flag,

                r.over_start_time AS over_start_time1,
                r.over_end_time AS over_end_time1,
                r.over_start_next_day_flag AS over_start_next_day_flag1,
                r.over_end_next_day_flag AS over_end_next_day_flag1,
                r.over_start_time2,
                r.over_end_time2,
                r.over_start_next_day_flag2,
                r.over_end_next_day_flag2,
                r.over_start_time3,
                r.over_end_time3,
                r.over_start_next_day_flag3,
                r.over_end_next_day_flag3,
                r.over_start_time4,
                r.over_end_time4,
                r.over_start_next_day_flag4,
                r.over_end_next_day_flag4,
                r.over_start_time5,
                r.over_end_time5,
                r.over_start_next_day_flag5,
                r.over_end_next_day_flag5,

                r.rest_start_time1,
                r.rest_end_time1,
                r.rest_start_next_day_flag1,
                r.rest_end_next_day_flag1,
                r.rest_start_time2,
                r.rest_end_time2,
                r.rest_start_next_day_flag2,
                r.rest_end_next_day_flag2,
                r.rest_start_time3,
                r.rest_end_time3,
                r.rest_start_next_day_flag3,
                r.rest_end_next_day_flag3,
                r.rest_start_time4,
                r.rest_end_time4,
                r.rest_start_next_day_flag4,
                r.rest_end_next_day_flag4,
                r.rest_start_time5,
                r.rest_end_time5,
                r.rest_start_next_day_flag5,
                r.rest_end_next_day_flag5,
                r.memo,

                rsn.ovtm_reason_id AS ovtm_reason_id1,
                rsn.ovtm_reason AS ovtm_reason1,
                rsn.ovtm_reason_id2,
                rsn.ovtm_reason2,
                rsn.ovtm_reason_id3,
                rsn.ovtm_reason3,
                rsn.ovtm_reason_id4,
                rsn.ovtm_reason4,
                rsn.ovtm_reason_id5,
                rsn.ovtm_reason5,

                r2.over_start_time AS r2_over_start_time1,
                r2.over_end_time AS r2_over_end_time1,
                r2.over_start_next_day_flag AS r2_over_start_next_day_flag1,
                r2.over_end_next_day_flag AS r2_over_end_next_day_flag1,
                r2.over_start_time2 AS r2_over_start_time2,
                r2.over_end_time2 AS r2_over_end_time2,
                r2.over_start_next_day_flag2 AS r2_over_start_next_day_flag2,
                r2.over_end_next_day_flag2 AS r2_over_end_next_day_flag2,
                r2.over_start_time3 AS r2_over_start_time3,
                r2.over_end_time3 AS r2_over_end_time3,
                r2.over_start_next_day_flag3 AS r2_over_start_next_day_flag3,
                r2.over_end_next_day_flag3 AS r2_over_end_next_day_flag3,
                r2.over_start_time4 AS r2_over_start_time4,
                r2.over_end_time4 AS r2_over_end_time4,
                r2.over_start_next_day_flag4 AS r2_over_start_next_day_flag4,
                r2.over_end_next_day_flag4 AS r2_over_end_next_day_flag4,
                r2.over_start_time5 AS r2_over_start_time5,
                r2.over_end_time5 AS r2_over_end_time5,
                r2.over_start_next_day_flag5 AS r2_over_start_next_day_flag5,
                r2.over_end_next_day_flag5 AS r2_over_end_next_day_flag5,

                r2.rest_start_time1 AS r2_rest_start_time1,
                r2.rest_end_time1 AS r2_rest_end_time1,
                r2.rest_start_next_day_flag1 AS r2_rest_start_next_day_flag1,
                r2.rest_end_next_day_flag1 AS r2_rest_end_next_day_flag1,
                r2.rest_start_time2 AS r2_rest_start_time2,
                r2.rest_end_time2 AS r2_rest_end_time2,
                r2.rest_start_next_day_flag2 AS r2_rest_start_next_day_flag2,
                r2.rest_end_next_day_flag2 AS r2_rest_end_next_day_flag2,
                r2.rest_start_time3 AS r2_rest_start_time3,
                r2.rest_end_time3 AS r2_rest_end_time3,
                r2.rest_start_next_day_flag3 AS r2_rest_start_next_day_flag3,
                r2.rest_end_next_day_flag3 AS r2_rest_end_next_day_flag3,
                r2.rest_start_time4 AS r2_rest_start_time4,
                r2.rest_end_time4 AS r2_rest_end_time4,
                r2.rest_start_next_day_flag4 AS r2_rest_start_next_day_flag4,
                r2.rest_end_next_day_flag4 AS r2_rest_end_next_day_flag4,
                r2.rest_start_time5 AS r2_rest_start_time5,
                r2.rest_end_time5 AS r2_rest_end_time5,
                r2.rest_start_next_day_flag5 AS r2_rest_start_next_day_flag5,
                r2.rest_end_next_day_flag5 AS r2_rest_end_next_day_flag5,

                rsn2.ovtm_reason_id AS r2_ovtm_reason_id1,
                rsn2.ovtm_reason AS r2_ovtm_reason1,
                rsn2.ovtm_reason_id2 AS r2_ovtm_reason_id2,
                rsn2.ovtm_reason2 AS r2_ovtm_reason2,
                rsn2.ovtm_reason_id3 AS r2_ovtm_reason_id3,
                rsn2.ovtm_reason3 AS r2_ovtm_reason3,
                rsn2.ovtm_reason_id4 AS r2_ovtm_reason_id4,
                rsn2.ovtm_reason4 AS r2_ovtm_reason4,
                rsn2.ovtm_reason_id5 AS r2_ovtm_reason_id5,
                rsn2.ovtm_reason5 AS r2_ovtm_reason5,

                r.conference_minutes,
                r.base_shorter_minutes,
                r.real_shorter_minutes,
                a.assist_flg AS assist,
                ag.group_name AS assist_group_name,
                ag.kaitori_day
            FROM empmst e
            JOIN calendar c ON (date BETWEEN :start_date AND :end_date)
            LEFT JOIN atdbk p ON (e.emp_id=p.emp_id AND c.date=p.date)
            LEFT JOIN atdbkrslt r ON (e.emp_id=r.emp_id AND c.date=r.date)
            LEFT JOIN atdbkrslt_ovtmrsn rsn ON (e.emp_id=rsn.emp_id AND c.date=rsn.date)
            LEFT JOIN atdbkrslt r2 ON (e.emp_id=r2.emp_id AND r2.date=to_char(to_date(c.date, 'YYYYMMDD') - 1, 'YYYYMMDD'))
            LEFT JOIN atdbkrslt_ovtmrsn rsn2 ON (e.emp_id=rsn2.emp_id AND rsn2.date=to_char(to_date(c.date, 'YYYYMMDD') - 1, 'YYYYMMDD'))
            LEFT JOIN duty_shift_plan_individual h ON (e.emp_id=h.emp_id AND c.date=h.duty_date)
            LEFT JOIN duty_shift_plan_assist a ON (e.emp_id=a.emp_id AND c.date=a.duty_date)
            LEFT JOIN duty_shift_group ag ON (ag.group_id=a.main_group_id)
            LEFT JOIN officehours oh ON r.tmcd_group_id=oh.tmcd_group_id AND r.pattern=oh.pattern AND oh.officehours_id=(CASE WHEN c.type='2' THEN '2' WHEN c.type='3' THEN '3' ELSE '1' END)
            WHERE e.emp_id = :emp_id
            ORDER BY c.date
        ");
        $res = $sth->execute(
            array(
                'emp_id' => $this->_emp_id,
                'start_date' => $start_date,
                'end_date' => $end_date,
            )
        );
        $lists = array();
        $total = array();
        $ptn4 = array();
        $over150 = 0;
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['print'] = 0;

            // 勤務記号
            if ($row['plan_pattern_id']) {
                if (!isset($ptn4[$row['plan_pattern_id']])) {
                    $pattern->set_group_id($row['plan_pattern_id']);
                    $ptn4[$row['plan_pattern_id']] = $pattern->ptn4_hash();
                }
            }
            if ($row['results_pattern_id']) {
                if (!isset($ptn4[$row['results_pattern_id']])) {
                    $pattern->set_group_id($row['results_pattern_id']);
                    $ptn4[$row['results_pattern_id']] = $pattern->ptn4_hash();
                }
            }
            else {
                if ((!$this->_staff->join_date() || $this->_staff->join_date() <= $row['date']) && ($row['date'] <= $this->_staff->retire_date() || !$this->_staff->retire_date() )) {
                    $total['is_empty_result'] = 1;
                }
            }

            // 予定
            if (is_object($ptn4[$row['plan_pattern_id']][$row['plan_id']])) {
                $row['plan'] = $ptn4[$row['plan_pattern_id']][$row['plan_id']]->ptn_name();
            }

            // 実績
            if (is_object($ptn4[$row['results_pattern_id']][$row['results_id']])) {
                $row['results'] = $ptn4[$row['results_pattern_id']][$row['results_id']]->ptn_name();
                $row['kigo'] = $ptn4[$row['results_pattern_id']][$row['results_id']]->font_name();
                $row['code'] = $ptn4[$row['results_pattern_id']][$row['results_id']]->sfc_code();
                $row['workday_count'] = $ptn4[$row['results_pattern_id']][$row['results_id']]->workday_count();
                $row['kind_flag'] = $ptn4[$row['results_pattern_id']][$row['results_id']]->kind_flag();
                $row['base_time'] = $ptn4[$row['results_pattern_id']][$row['results_id']]->base_time();
                $row['not135'] = $ptn4[$row['results_pattern_id']][$row['results_id']]->no_count_flag();
                $row['not125'] = $ptn4[$row['results_pattern_id']][$row['results_id']]->out_time_nosubtract_flag();

                // 勤務記号
                if ($row['results_pattern'] === '10') {
                    $pattern_id = sprintf('0-10-%d', $row['results_reason']);
                }
                else {
                    $pattern_id = sprintf('%d-%d-00', $row['results_pattern_id'], $row['results_pattern']);
                }
                $row[$pattern_id] = 1;
            }

            // 希望
            if (is_object($ptn4[$row['plan_pattern_id']][$row['hope_id']])) {
                $row['hope'] = $ptn4[$row['plan_pattern_id']][$row['hope_id']]->ptn_name();
            }

            // 翌日フラグチェック
            if ($row['start_time'] && $row['start_time'] !== '0000' && $row['start_time'] >= $row['end_time']) {
                $row['next_day_flag'] = 1;
            }

            // 休憩時間
            $row['resttime'] = restMinutes($row['rest_start_time'], $row['rest_end_time']);

            // 勤務時間
            $row['worktime'] = workMinutes($row['start_time'], $row['end_time'], $row['next_day_flag'], $row['resttime']);

            // 残業(前日)
            $overtime1 = 0;
            $overtime2 = 0;
            $resttime = 0;
            foreach (range(1, 5) as $i) {
                if ($row["r2_over_start_time{$i}"] && $row["r2_over_end_next_day_flag{$i}"] === '1' && $row["r2_over_end_time{$i}"] > "0000") {

                    $row['ovtm_start_time'][] = ($row["r2_over_start_next_day_flag{$i}"] === '1' ? $row["r2_over_start_time{$i}"] : '0000');
                    $row['ovtm_end_time'][] = $row["r2_over_end_time{$i}"];
                    $row['ovtm_reason'][] = $ovtmrsn->reason($row["r2_ovtm_reason_id{$i}"], $row["r2_ovtm_reason{$i}"]);
                    $row['print'] += 1;

                    //時間
                    list($over_min1, $over_min2, $rest_min1, $rest_min2) = $ovtm->overtime_nextday($row["r2_over_start_time{$i}"], $row["r2_over_start_next_day_flag{$i}"], $row["r2_over_end_time{$i}"], $row["r2_over_end_next_day_flag{$i}"], $row["r2_rest_start_time{$i}"], $row["r2_rest_start_next_day_flag{$i}"], $row["r2_rest_end_time{$i}"], $row["r2_rest_end_next_day_flag{$i}"]);
                    $overtime1 += $over_min1;
                    $overtime2 += $over_min2;
                    $resttime += $rest_min1 + $rest_min2;
                }
            }
            // 買取日
            if ($row['kaitori_day'] && $row['type'] === '8' && $row['results_pattern'] !== '10' && $row['start_time']) {
                $row['ovtm_start_time'][] = $row['start_time'];
                $row['ovtm_end_time'][] = $row['end_time'];
                $row['ovtm_reason'][] = '祝日' . $row['results'];
                $overtime1 += $row['worktime'];
                $resttime += $row['resttime'];
                $row['resttime'] = '';
                $row['worktime'] = '';
                $row['start_time'] = '';
                $row['end_time'] = '';
                $row['print'] += 1;
            }
            else {
                $total['resttime'] += $row['resttime'];
                $total['worktime'] += $row['worktime'];
            }
            // 残業(当日)
            foreach (range(1, 5) as $i) {
                if ($row["over_start_time{$i}"] && $row["over_start_next_day_flag{$i}"] !== '1') {

                    $row['ovtm_start_time'][] = $row["over_start_time{$i}"];
                    $row['ovtm_end_time'][] = ($row["over_end_next_day_flag{$i}"] === '1' ? '2400' : $row["over_end_time{$i}"]);
                    $row['ovtm_reason'][] = $ovtmrsn->reason($row["ovtm_reason_id{$i}"], $row["ovtm_reason{$i}"]);
                    $row['print'] += 1;

                    list($over_min1, $over_min2, $rest_min1, $rest_min2) = $ovtm->overtime($row["over_start_time{$i}"], $row["over_start_next_day_flag{$i}"], $row["over_end_time{$i}"], $row["over_end_next_day_flag{$i}"], $row["rest_start_time{$i}"], $row["rest_start_next_day_flag{$i}"], $row["rest_end_time{$i}"], $row["rest_end_next_day_flag{$i}"]);
                    $overtime1 += $over_min1;
                    $overtime2 += $over_min2;
                    $resttime += $rest_min1 + $rest_min2;
                }
            }
            $row['totalworktime'] = $row['worktime'] + $overtime1 + $overtime2;
            $row['overtime'] = $overtime1 + $overtime2;
            $row['overtime2'] = $overtime1 + $overtime2 - $row['conference_minutes'];
            $row['over_resttime'] = $resttime;

            // 残業時間合計
            $total['overtime'] += $row['overtime'];

            // 小計:カンファレンス時間含む
            $total['overtime2'] += $row['overtime2'];

            // カンファレンス時間
            if ($row['conference_minutes']) {
                $total['conference_minutes'] += $row['conference_minutes'];
                $row['print'] = $row['print'] ? $row['print'] : 1;
            }

            // 残業時間詳細
            // 「法定外残業の計算」時間が設定されている記号は、それをベースとして残業時間詳細を計算する
            if ($row['base_time']) {
                list($row['over100'], $row['over125'], $row['over135'], $row['over25']) = $ovtm->overtime_details($row['base_time'], $overtime1, $overtime2, $this->_ward->kaitori_day(), $row);
            }
            else {
                list($row['over100'], $row['over125'], $row['over135'], $row['over25']) = $ovtm->overtime_details($row['worktime'], $overtime1, $overtime2, $this->_ward->kaitori_day(), $row);
            }

            // 月60時間超
            $over150 += $row['over100'] + $row['over125'];
            if ($over150 > 3600) {
                if ($total['over150'] > 0) {
                    $row['over150'] = $row['over100'] + $row['over125'];
                    $row['over100'] = 0;
                    $row['over125'] = 0;
                }
                else {
                    $diff = $over150 - 3600;
                    $row['over150'] = $diff;
                    if ($row['over125'] > $diff) {
                        $row['over125'] -= $diff;
                    }
                    else {
                        $row['over100'] -= ($diff - $row['over125']);
                        $row['over125'] = 0;
                    }
                }
            }

            $total['over100'] += $row['over100'];
            $total['over125'] += $row['over125'];
            $total['over135'] += $row['over135'];
            $total['over150'] += $row['over150'];
            $total['over25'] += $row['over25'];

            // 時短
            $total['base_shorter_minutes'] += $row['base_shorter_minutes'];
            $total['real_shorter_minutes'] += $row['real_shorter_minutes'];
            if ($row['week'] === '6') {
                $row['base_shorter_minutes6'] = $row['base_shorter_minutes'];
                $row['real_shorter_minutes6'] = $row['real_shorter_minutes'];
            }
            else if ($row['week'] >= '1' && $row['week'] <= '5') {
                $row['base_shorter_minutes1'] = $row['base_shorter_minutes'];
                $row['real_shorter_minutes1'] = $row['real_shorter_minutes'];
            }

            // 夜勤回数
            if ($row['code'] === 'O6') {
                $total['nightduty_count'] ++;
            }

            // 宿直, 日祝宿直, 当直フラグ
            if ($row['code'] === 'O7' || $row['code'] === 'PA') {
                switch ($row['type']) {
                    case '2':
                        $row['ovtm_start_time'][] = '12:00';
                        $row['ovtm_end_time'][] = '08:30';
                        $row['duty1'] = true;
                        $row['duty2'] = true;
                        $row['ovtm_reason'][] = '宿直（土）';
                        break;
                    case '1':
                        $row['ovtm_start_time'][] = '17:00';
                        $row['ovtm_end_time'][] = '08:30';
                        $row['duty1'] = true;
                        $row['ovtm_reason'][] = '宿直（平）';
                        break;
                    case '3':
                    case '8' :
                        $row['ovtm_start_time'][] = '17:00';
                        $row['ovtm_end_time'][] = '08:30';
                        $row['duty1'] = true;
                        $row['ovtm_reason'][] = '宿直（日）';
                        break;
                }
                $row['print'] += 1;
            }

            // 宿直Staff
            if ($row['code'] === 'O8') {
                switch ($row['type']) {
                    case '2':
                        $row['ovtm_start_time'][] = '12:00';
                        $row['ovtm_end_time'][] = '08:30';
                        $row['duty1'] = true;
                        $row['duty2'] = true;
                        $row['ovtm_reason'][] = '宿直（土）';
                        break;
                    case '3' :
                    case '8' :
                        $row['ovtm_start_time'][] = '08:30';
                        $row['ovtm_end_time'][] = '08:30';
                        $row['duty1'] = true;
                        $row['duty3'] = true;
                        $row['ovtm_reason'][] = '宿直（日）';
                        break;
                    default :
                        break;
                }
                $row['print'] += 1;
            }

            // 日直
            if ($row['code'] === 'P9') {
                $row['ovtm_start_time'][] = '08:30';
                $row['ovtm_end_time'][] = '17:00';
                $row['duty3'] = true;
                switch ($row['type']) {
                    case '2':
                        $row['ovtm_reason'][] = '日直（土）';
                        break;
                    case '3' :
                    case '8' :
                        $row['ovtm_reason'][] = '日直（日）';
                        break;
                    default :
                        $row['ovtm_reason'][] = '日直';
                        break;
                }
                $row['print'] += 1;
            }

            // 当直（土）
            if ($row['code'] === 'PG' || $row['code'] === 'PH') {
                $row['duty1'] = true;
                $row['duty2'] = true;
                $row['ovtm_start_time'][] = '12:00';
                $row['ovtm_end_time'][] = '08:30';
                $row['ovtm_reason'][] = '当直（土）';
                $row['print'] +=1;
            }

            // 当直（日）
            if ($row['code'] === '_TC3') {
                $row['duty1'] = true;
                $row['duty3'] = true;
                $row['ovtm_start_time'][] = '08:30';
                $row['ovtm_end_time'][] = '08:30';
                $row['ovtm_reason'][] = '当直（日）';
                $row['print'] +=1;
            }

            // 宿直（土）
            if ($row['code'] === '_SC2') {
                $row['duty1'] = true;
                $row['ovtm_start_time'][] = '17:00';
                $row['ovtm_end_time'][] = '08:30';
                $row['ovtm_reason'][] = '宿直（土）';
                $row['print'] +=1;
            }

            // 半日直
            if (substr($row['code'], 0, 3) === '_HN') {
                switch (substr($row['code'], -1)) {
                    case '2':
                        $row['duty2'] = true;
                        $row['ovtm_start_time'][] = '12:00';
                        $row['ovtm_end_time'][] = '17:00';
                        $row['ovtm_reason'][] = '半日直（土）';
                        break;
                    case '3':
                        $row['duty2'] = true;
                        $row['ovtm_start_time'][] = '08:30';
                        $row['ovtm_end_time'][] = '12:00';
                        $row['ovtm_reason'][] = '半日直（日）';
                        break;
                }
                $row['print'] +=1;
            }

            // ER
            if (substr($row['code'], 0, 3) === '_ER') {
                switch (substr($row['code'], -1)) {
                    case '1':
                        $row['duty1'] = true;
                        $row['ovtm_start_time'][] = '17:00';
                        $row['ovtm_end_time'][] = '08:30';
                        $row['ovtm_reason'][] = 'ER宿直（平）';
                        break;
                    case '2':
                        $row['duty1'] = true;
                        $row['ovtm_start_time'][] = '17:00';
                        $row['ovtm_end_time'][] = '08:30';
                        $row['ovtm_reason'][] = 'ER宿直（土）';
                        break;
                    case '3':
                        $row['duty1'] = true;
                        $row['ovtm_start_time'][] = '19:00';
                        $row['ovtm_end_time'][] = '08:30';
                        $row['ovtm_reason'][] = 'ER宿直（日）';
                        break;
                    case '4':
                        $row['duty2'] = true;
                        $row['ovtm_start_time'][] = '12:00';
                        $row['ovtm_end_time'][] = '19:00';
                        $row['ovtm_reason'][] = 'ER半日直（土）';
                        break;
                    case '5':
                        $row['duty3'] = true;
                        $row['ovtm_start_time'][] = '10:00';
                        $row['ovtm_end_time'][] = '19:00';
                        $row['ovtm_reason'][] = 'ER日直（日）';
                        break;
                }
                $row['print'] +=1;
            }

            // 看護部・待機
            if ($row['code'] === 'O4') {
                switch ($row['type']) {
                    case '2':
                        $row['ovtm_start_time'][] = '12:00';
                        $row['ovtm_end_time'][] = '14:00';
                        $row['ovtm_reason'][] = '小児救急待機（土）';
                        break;
                    case '1' :
                        $row['ovtm_start_time'][] = '17:00';
                        $row['ovtm_end_time'][] = '20:00';
                        $row['ovtm_reason'][] = '小児救急待機（平日）';
                        break;
                }
                $row['taiki'] = true;
                $row['print'] +=1;
            }

            // 待機
            if (substr($row['code'], 0, 3) === '_TA') {
                switch (substr($row['code'], -1)) {
                    case '1':
                        $row['ovtm_reason'][] = '待機（平）';
                        break;
                    case '2':
                        $row['ovtm_reason'][] = '待機（土）';
                        break;
                    case '3':
                        $row['ovtm_reason'][] = '待機（日）';
                        break;
                }
                $row['ovtm_start_time'][] = '';
                $row['ovtm_end_time'][] = '';
                $row['taiki'] = true;
                $row['print'] +=1;
            }

            // 後送
            if (substr($row['code'], 0, 3) === '_GO') {
                switch (substr($row['code'], -1)) {
                    case '1':
                        $row['ovtm_reason'][] = '後送（平）';
                        break;
                    case '2':
                        $row['ovtm_reason'][] = '後送（土）';
                        break;
                    case '3':
                        $row['ovtm_reason'][] = '後送（日）';
                        break;
                }
                $row['ovtm_start_time'][] = '';
                $row['ovtm_end_time'][] = '';
                $row['taiki'] = true;
                $row['print'] += 1;
            }

            // 年末年始手当
            if ($row['totalworktime']) {
                switch (substr($row['date'], 4, 4)) {
                    case '1231':
                    case '0101':
                        if ($row['totalworktime'] <= 240) {
                            $row['newyear1'] = 1;
                        }
                        else if ($row['totalworktime'] <= 600) {
                            $row['newyear2'] = 1;
                        }
                        else if ($row['totalworktime'] <= 840) {
                            $row['newyear3'] = 1;
                        }
                        else {
                            $row['newyear4'] = 1;
                        }
                        $row['newyear'] = true;
                        $row['print'] = $row['print'] ? $row['print'] : 1;
                        break;
                    case '1230':
                    case '0102':
                    case '0103':
                    case '0104':
                        if ($row['totalworktime'] <= 240) {
                            $row['newyear5'] = 1;
                        }
                        else if ($row['totalworktime'] <= 600) {
                            $row['newyear6'] = 1;
                        }
                        else if ($row['totalworktime'] <= 840) {
                            $row['newyear7'] = 1;
                        }
                        else {
                            $row['newyear8'] = 1;
                        }
                        $row['newyear'] = true;
                        $row['print'] = $row['print'] ? $row['print'] : 1;
                        break;
                }
            }

            // ボイラー夜勤
            if ($row['code'] === '_BO') {
                $row['boiler'] = 1;
                $row['ovtm_start_time'][] = '';
                $row['ovtm_end_time'][] = '';
                $row['ovtm_reason'][] = 'ボイラー夜勤';
                $row['print'] += 1;
            }

            // 備考
            if ($row['memo']) {
                $br_count = substr_count(trim($row['memo']), "\n") + 1;
                $row['print'] = $row['print'] < $br_count ? $br_count : $row['print'];
            }

            $total['print'] += $row['print'];

            $lists[] = $row;
        }
        $sth->free();
        return array($lists, $total);
    }

    /**
     * 休暇日数の取得
     * @param type $year
     * @param type $month
     * @return type
     */
    function holiday($year, $month)
    {
        $end_date = next_ymd($this->_sdate->end_ymd($year, $month));

        $data = array();

        // 有休数
        list($paid_h_base, $paid_h_use, $paid_h_fuyo, $paid_h_kuri) = $this->_staff->paid_holiday($this->_emp_id, $end_date);
        $data['holiday']['kuri'] = $paid_h_kuri;
        $data['holiday']['fuyo'] = $paid_h_fuyo;
        $data['holiday']['use'] = $paid_h_use;
        $data['holiday']['zan'] = $paid_h_base - $paid_h_use;

        // 夏休数
        list($paid_s_base, $paid_s_use) = $this->_staff->paid_holiday_summer($this->_emp_id, $end_date);
        $data['summer']['fuyo'] = $paid_s_base;
        $data['summer']['use'] = $paid_s_use;
        $data['summer']['zan'] = $paid_s_base - $paid_s_use;

        // 週休数
        list($paid_w_base, $paid_w_use) = $this->_staff->paid_holiday_week($this->_emp_id, $end_date);
        $data['week']['fuyo'] = $paid_w_base;
        $data['week']['use'] = $paid_w_use;
        $data['week']['zan'] = $paid_w_base - $paid_w_use;

        return $data;
    }

    /**
     * 残業時間の取得
     * @param type $date
     * @return type
     */
    function overtime($date)
    {
        $ovtm = new Cmx_Timecard_Overtime();
        $ovtmrsn = new Cmx_Timecard_OvertimeReason();

        // 勤務データ
        $sth = $this->db->prepare("
            SELECT
                r.over_start_time AS over_start_time1,
                r.over_end_time AS over_end_time1,
                r.over_start_next_day_flag AS over_start_next_day_flag1,
                r.over_end_next_day_flag AS over_end_next_day_flag1,
                r.over_start_time2,
                r.over_end_time2,
                r.over_start_next_day_flag2,
                r.over_end_next_day_flag2,
                r.over_start_time3,
                r.over_end_time3,
                r.over_start_next_day_flag3,
                r.over_end_next_day_flag3,
                r.over_start_time4,
                r.over_end_time4,
                r.over_start_next_day_flag4,
                r.over_end_next_day_flag4,
                r.over_start_time5,
                r.over_end_time5,
                r.over_start_next_day_flag5,
                r.over_end_next_day_flag5,

                r.rest_start_time1,
                r.rest_end_time1,
                r.rest_start_next_day_flag1,
                r.rest_end_next_day_flag1,
                r.rest_start_time2,
                r.rest_end_time2,
                r.rest_start_next_day_flag2,
                r.rest_end_next_day_flag2,
                r.rest_start_time3,
                r.rest_end_time3,
                r.rest_start_next_day_flag3,
                r.rest_end_next_day_flag3,
                r.rest_start_time4,
                r.rest_end_time4,
                r.rest_start_next_day_flag4,
                r.rest_end_next_day_flag4,
                r.rest_start_time5,
                r.rest_end_time5,
                r.rest_start_next_day_flag5,
                r.rest_end_next_day_flag5,

                rsn.ovtm_reason_id AS ovtm_reason_id1,
                rsn.ovtm_reason AS ovtm_reason1,
                rsn.ovtm_reason_id2,
                rsn.ovtm_reason2,
                rsn.ovtm_reason_id3,
                rsn.ovtm_reason3,
                rsn.ovtm_reason_id4,
                rsn.ovtm_reason4,
                rsn.ovtm_reason_id5,
                rsn.ovtm_reason5,

                r2.over_start_time AS r2_over_start_time1,
                r2.over_end_time AS r2_over_end_time1,
                r2.over_start_next_day_flag AS r2_over_start_next_day_flag1,
                r2.over_end_next_day_flag AS r2_over_end_next_day_flag1,
                r2.over_start_time2 AS r2_over_start_time2,
                r2.over_end_time2 AS r2_over_end_time2,
                r2.over_start_next_day_flag2 AS r2_over_start_next_day_flag2,
                r2.over_end_next_day_flag2 AS r2_over_end_next_day_flag2,
                r2.over_start_time3 AS r2_over_start_time3,
                r2.over_end_time3 AS r2_over_end_time3,
                r2.over_start_next_day_flag3 AS r2_over_start_next_day_flag3,
                r2.over_end_next_day_flag3 AS r2_over_end_next_day_flag3,
                r2.over_start_time4 AS r2_over_start_time4,
                r2.over_end_time4 AS r2_over_end_time4,
                r2.over_start_next_day_flag4 AS r2_over_start_next_day_flag4,
                r2.over_end_next_day_flag4 AS r2_over_end_next_day_flag4,
                r2.over_start_time5 AS r2_over_start_time5,
                r2.over_end_time5 AS r2_over_end_time5,
                r2.over_start_next_day_flag5 AS r2_over_start_next_day_flag5,
                r2.over_end_next_day_flag5 AS r2_over_end_next_day_flag5,

                r2.rest_start_time1 AS r2_rest_start_time1,
                r2.rest_end_time1 AS r2_rest_end_time1,
                r2.rest_start_next_day_flag1 AS r2_rest_start_next_day_flag1,
                r2.rest_end_next_day_flag1 AS r2_rest_end_next_day_flag1,
                r2.rest_start_time2 AS r2_rest_start_time2,
                r2.rest_end_time2 AS r2_rest_end_time2,
                r2.rest_start_next_day_flag2 AS r2_rest_start_next_day_flag2,
                r2.rest_end_next_day_flag2 AS r2_rest_end_next_day_flag2,
                r2.rest_start_time3 AS r2_rest_start_time3,
                r2.rest_end_time3 AS r2_rest_end_time3,
                r2.rest_start_next_day_flag3 AS r2_rest_start_next_day_flag3,
                r2.rest_end_next_day_flag3 AS r2_rest_end_next_day_flag3,
                r2.rest_start_time4 AS r2_rest_start_time4,
                r2.rest_end_time4 AS r2_rest_end_time4,
                r2.rest_start_next_day_flag4 AS r2_rest_start_next_day_flag4,
                r2.rest_end_next_day_flag4 AS r2_rest_end_next_day_flag4,
                r2.rest_start_time5 AS r2_rest_start_time5,
                r2.rest_end_time5 AS r2_rest_end_time5,
                r2.rest_start_next_day_flag5 AS r2_rest_start_next_day_flag5,
                r2.rest_end_next_day_flag5 AS r2_rest_end_next_day_flag5,

                rsn2.ovtm_reason_id AS r2_ovtm_reason_id1,
                rsn2.ovtm_reason AS r2_ovtm_reason1,
                rsn2.ovtm_reason_id2 AS r2_ovtm_reason_id2,
                rsn2.ovtm_reason2 AS r2_ovtm_reason2,
                rsn2.ovtm_reason_id3 AS r2_ovtm_reason_id3,
                rsn2.ovtm_reason3 AS r2_ovtm_reason3,
                rsn2.ovtm_reason_id4 AS r2_ovtm_reason_id4,
                rsn2.ovtm_reason4 AS r2_ovtm_reason4,
                rsn2.ovtm_reason_id5 AS r2_ovtm_reason_id5,
                rsn2.ovtm_reason5 AS r2_ovtm_reason5
            FROM atdbkrslt r
            LEFT JOIN atdbkrslt_ovtmrsn rsn ON (r.emp_id=rsn.emp_id AND r.date=rsn.date)
            LEFT JOIN atdbkrslt r2 ON (r.emp_id=r2.emp_id AND r2.date=to_char(to_date(r.date, 'YYYYMMDD') - 1, 'YYYYMMDD'))
            LEFT JOIN atdbkrslt_ovtmrsn rsn2 ON (r.emp_id=rsn2.emp_id AND rsn2.date=to_char(to_date(r.date, 'YYYYMMDD') - 1, 'YYYYMMDD'))
            WHERE r.emp_id = :emp_id
              AND r.date = :date
        ");
        $res = $sth->execute(
            array(
                'emp_id' => $this->_emp_id,
                'date' => $date,
            )
        );

        $row = $res->fetchRow(MDB2_FETCHMODE_ASSOC);

        // 残業(前日)
        $overtime1 = 0;
        $overtime2 = 0;
        $reason = array();
        foreach (range(1, 5) as $i) {
            if ($row["r2_over_start_time{$i}"] && $row["r2_over_end_next_day_flag{$i}"] === '1' && $row["r2_over_end_time{$i}"] > "0000") {

                $row['ovtm_start_time'][] = ($row["r2_over_start_next_day_flag{$i}"] === '1' ? $row["r2_over_start_time{$i}"] : '0000');
                $row['ovtm_end_time'][] = $row["r2_over_end_time{$i}"];
                $reason[] = $ovtmrsn->reason($row["r2_ovtm_reason_id{$i}"], $row["r2_ovtm_reason{$i}"]);

                //時間
                list($over_min1, $over_min2, $rest_min1, $rest_min2) = $ovtm->overtime_nextday($row["r2_over_start_time{$i}"], $row["r2_over_start_next_day_flag{$i}"], $row["r2_over_end_time{$i}"], $row["r2_over_end_next_day_flag{$i}"], $row["r2_rest_start_time{$i}"], $row["r2_rest_start_next_day_flag{$i}"], $row["r2_rest_end_time{$i}"], $row["r2_rest_end_next_day_flag{$i}"]);
                $overtime1 += $over_min1;
                $overtime2 += $over_min2;
                $resttime += $rest_min1 + $rest_min2;
            }
        }

        // 買取日
        if ($this->_ward->kaitori_day() && $row['type'] === '8' && $row['results_pattern'] !== '10' && $row['start_time']) {
            $row['ovtm_start_time'][] = $row['start_time'];
            $row['ovtm_end_time'][] = $row['end_time'];
            $reason[] = '祝日' . $row['results'];
            $overtime1 += $row['worktime'];
            $resttime += $row['resttime'];
        }

        // 残業(当日)
        foreach (range(1, 5) as $i) {
            if ($row["over_start_time{$i}"] && $row["over_start_next_day_flag{$i}"] !== '1') {

                $row['ovtm_start_time'][] = $row["over_start_time{$i}"];
                $row['ovtm_end_time'][] = ($row["over_end_next_day_flag{$i}"] === '1' ? '2400' : $row["over_end_time{$i}"]);
                $reason[] = $ovtmrsn->reason($row["ovtm_reason_id{$i}"], $row["ovtm_reason{$i}"]);

                list($over_min1, $over_min2, $rest_min1, $rest_min2) = $ovtm->overtime($row["over_start_time{$i}"], $row["over_start_next_day_flag{$i}"], $row["over_end_time{$i}"], $row["over_end_next_day_flag{$i}"], $row["rest_start_time{$i}"], $row["rest_start_next_day_flag{$i}"], $row["rest_end_time{$i}"], $row["rest_end_next_day_flag{$i}"]);
                $overtime1 += $over_min1;
                $overtime2 += $over_min2;
                $resttime += $rest_min1 + $rest_min2;
            }
        }

        return array($overtime1 + $overtime2, $reason);
    }

    /**
     * 承認済みリスト
     * @param type $year
     * @param type $month
     * @return type
     */
    function approved($year, $month)
    {
        $appr = new Cmx_Shift_Approval();
        $approved = $appr->approved($this->_emp_id, $year, $month);

        return $approved;
    }

    /**
     * 承認済み月の末日を取得
     * @return type
     */
    function last_approved_date()
    {
        $appr = new Cmx_Shift_Approval();
        $date = $appr->last_approved_date($this->_emp_id);

        return $date;
    }

    /**
     * 月次承認(本人)
     * @param type $year
     * @param type $month
     */
    function save_self_approval($year, $month)
    {
        $appr = new Cmx_Shift_Approval();
        $appr->self_approval($this->_emp_id, $year, $month, $this->_login_id);
    }

    /**
     * 月次承認(所属長)
     * @param type $year
     * @param type $month
     */
    function save_manager_approval($year, $month)
    {
        $appr = new Cmx_Shift_Approval();
        $appr->manager_approval($this->_emp_id, $year, $month, $this->_login_id);
    }

    /**
     * 月次承認(部門長)
     * @param type $year
     * @param type $month
     */
    function save_boss_approval($year, $month)
    {
        $appr = new Cmx_Shift_Approval();
        $appr->boss_approval($this->_emp_id, $year, $month, $this->_login_id);
    }

    /**
     * 勤務予定保存
     *
     * @param str $date   保存する日付
     * @param arr $post   保存する勤務予定データ
     *
     * @return boolean true:成功 false:失敗
     */
    function save_plan($date, $post)
    {
        // emp_id
        $post['emp_id'] = $this->_emp_id;

        // date
        $post['date'] = $date;

        // pattern, reason
        $post['pattern'] = (int)substr($post['ptn'], 0, 2);
        $post['reason'] = (int)substr($post['ptn'], 2, 2);

        // トランザクション開始
        $this->db->beginNestedTransaction();

        // 実績をセット
        if (!empty($this->_tbl['plan']['date'])) {
            $sth = $this->db->prepare(
                "
                UPDATE atdbk SET
                    pattern = :pattern,
                    reason = :reason,
                    tmcd_group_id = :tmcd_group_id
                WHERE emp_id= :emp_id AND date = :date
                ", array('text', 'text', 'text', 'text', 'text'), MDB2_PREPARE_MANIP
            );
        }
        else {
            $sth = $this->db->prepare(
                "
                INSERT INTO atdbk (emp_id, date, pattern, reason, tmcd_group_id)
                VALUES (:emp_id, :date, :pattern, :reason, :tmcd_group_id)
                ", array('text', 'text', 'text', 'text', 'text'), MDB2_PREPARE_MANIP
            );
        }

        $res = $sth->execute($post);
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_Personal: save_plan: INSERT/UPADTE atdbk: ' . $res->getDebugInfo());
            return false;
        }
        $sth->free();

        // commit
        $this->db->commit();
        $this->log->log("予定保存:$date", $post);
        return true;
    }

    /**
     * 勤務実績保存
     * 勤務実績画面で「登録」ボタンを押したときに実行される
     *
     * @param str $date   保存する日付
     * @param arr $data   保存する勤務実績データ
     *
     * @return boolean true:成功 false:失敗
     */
    function save_results($date, $update_emp_id, $post)
    {
        // emp_id
        $post['emp_id'] = $this->_emp_id;

        // date
        $post['date'] = $date;

        // pattern, reason
        $post['pattern'] = (int)substr($post['ptn'], 0, 2);
        $post['reason'] = (int)substr($post['ptn'], 2, 2);

        // 出勤時間
        $post['start_time'] = $post['start_hour'] . $post['start_min'];

        // 退勤時間
        if ($post['end_hour'] >= 24) {
            $post["end_hour"] = sprintf("%02d", $post["end_hour"] - 24);
            $post['next_day_flag'] = 1;
        }
        else {
            $post['next_day_flag'] = 0;
        }
        $post['end_time'] = $post['end_hour'] . $post['end_min'];

        // 休憩時間
        $post['rest_start_time'] = $post['rest_start_hour'] . $post['rest_start_min'];
        $post['rest_end_time'] = $post['rest_end_hour'] . $post['rest_end_min'];

        // カンファレンス時間
        $post['conference_minutes'] = $post['conference_time'] * 60;

        // 時短
        if ($this->_staff->is_end_1y($date)) {
            $post['real_shorter_minutes'] = $post['base_shorter_minutes'] > 60 ? $post['base_shorter_minutes'] - 60 : 0;
        }
        else if ($this->_staff->is_end_3y($date)) {
            $post['real_shorter_minutes'] = $post['base_shorter_minutes'];
        }

        // 残業時間
        $i = 0;
        foreach ($post['over'] as $over) {
            $i++;

            // 残業開始
            if ($over["over_start_hour"] >= 24) {
                $over["over_start_hour"] = sprintf("%02d", $over["over_start_hour"] - 24);
                $post["over_start_next_day_flag{$i}"] = 1;
            }
            else {
                $post["over_start_next_day_flag{$i}"] = 0;
            }
            $post["over_start_time{$i}"] = $over["over_start_hour"] . $over["over_start_min"];

            // 残業終了
            if ($over["over_end_hour"] >= 24) {
                $over["over_end_hour"] = sprintf("%02d", $over["over_end_hour"] - 24);
                $post["over_end_next_day_flag{$i}"] = 1;
            }
            else {
                $post["over_end_next_day_flag{$i}"] = 0;
            }
            $post["over_end_time{$i}"] = $over["over_end_hour"] . $over["over_end_min"];

            // 残業休憩開始
            if ($over["rest_over_start_hour"] >= 24) {
                $over["rest_over_start_hour"] = sprintf("%02d", $over["rest_over_start_hour"] - 24);
                $post["rest_start_next_day_flag{$i}"] = 1;
            }
            else {
                $post["rest_start_next_day_flag{$i}"] = 0;
            }
            $post["rest_start_time{$i}"] = $over["rest_over_start_hour"] . $over["rest_over_start_min"];

            // 残業休憩終了
            if ($over["rest_over_end_hour"] >= 24) {
                $over["rest_over_end_hour"] = sprintf("%02d", $over["rest_over_end_hour"] - 24);
                $post["rest_end_next_day_flag{$i}"] = 1;
            }
            else {
                $post["rest_end_next_day_flag{$i}"] = 0;
            }
            $post["rest_end_time{$i}"] = $over["rest_over_end_hour"] . $over["rest_over_end_min"];

            // 残業理由
            if (!$over["over_start_hour"]) {
                $post["ovtm_reason_id{$i}"] = null;
                $post["ovtm_reason{$i}"] = '';
            }
            else {
                $post["ovtm_reason_id{$i}"] = $over["ovtm_reason_id"];
                $post["ovtm_reason{$i}"] = $over["ovtm_reason"];
            }
        }

        // update_time
        $post['update_time'] = date('YmdHis');

        // $update_emp_id
        $post['update_emp_id'] = $update_emp_id;

        // トランザクション開始
        $this->db->beginNestedTransaction();

        // 実績をセット
        $update_sql = array();
        $insert_sql = array();
        $insert_place = array();
        $types = array();

        // --実績変更SQL
        if ($post['is_result_edit']) {
            $update_sql['result'] = "
                pattern = :pattern,
                reason = :reason,
                tmcd_group_id = :tmcd_group_id,
                night_duty = :night_duty,
                start_time = :start_time,
                end_time = :end_time,
                rest_start_time = :rest_start_time,
                rest_end_time = :rest_end_time,
                previous_day_flag = 0,
                next_day_flag = :next_day_flag,
                conference_minutes = :conference_minutes,
            ";
            $insert_sql['result'] = "
                pattern,
                reason,
                tmcd_group_id,
                night_duty,
                start_time,
                end_time,
                rest_start_time,
                rest_end_time,
                next_day_flag,
                conference_minutes,
            ";
            $insert_place['result'] = "
                :pattern,
                :reason,
                :tmcd_group_id,
                :night_duty,
                :start_time,
                :end_time,
                :rest_start_time,
                :rest_end_time,
                :next_day_flag,
                :conference_minutes,
            ";
            $types = array_merge($types, array(
                'text', //          pattern
                'text', //          reason
                'integer', //       tmcd_group_id
                'text', //          night_duty
                'text', //          start_time
                'text', //          end_time
                'text', //          rest_start_time
                'text', //          rest_end_time
                'integer', //       next_day_flag
                'integer', //       conference_minutes
            ));
        }

        // --残業時間SQL
        if ($post['is_overtime']) {
            $update_sql['overtime'] = "
                over_start_time = :over_start_time1,
                over_end_time = :over_end_time1,
                over_start_next_day_flag = :over_start_next_day_flag1,
                over_end_next_day_flag = :over_end_next_day_flag1,

                rest_start_time1 = :rest_start_time1,
                rest_end_time1 = :rest_end_time1,
                rest_start_next_day_flag1 = :rest_start_next_day_flag1,
                rest_end_next_day_flag1 = :rest_end_next_day_flag1,

                over_start_time2 = :over_start_time2,
                over_end_time2 = :over_end_time2,
                over_start_next_day_flag2 = :over_start_next_day_flag2,
                over_end_next_day_flag2 = :over_end_next_day_flag2,

                rest_start_time2 = :rest_start_time2,
                rest_end_time2 = :rest_end_time2,
                rest_start_next_day_flag2 = :rest_start_next_day_flag2,
                rest_end_next_day_flag2 = :rest_end_next_day_flag2,

                over_start_time3 = :over_start_time3,
                over_end_time3 = :over_end_time3,
                over_start_next_day_flag3 = :over_start_next_day_flag3,
                over_end_next_day_flag3 = :over_end_next_day_flag3,

                rest_start_time3 = :rest_start_time3,
                rest_end_time3 = :rest_end_time3,
                rest_start_next_day_flag3 = :rest_start_next_day_flag3,
                rest_end_next_day_flag3 = :rest_end_next_day_flag3,

                over_start_time4 = :over_start_time4,
                over_end_time4 = :over_end_time4,
                over_start_next_day_flag4 = :over_start_next_day_flag4,
                over_end_next_day_flag4 = :over_end_next_day_flag4,

                rest_start_time4 = :rest_start_time4,
                rest_end_time4 = :rest_end_time4,
                rest_start_next_day_flag4 = :rest_start_next_day_flag4,
                rest_end_next_day_flag4 = :rest_end_next_day_flag4,

                over_start_time5 = :over_start_time5,
                over_end_time5 = :over_end_time5,
                over_start_next_day_flag5 = :over_start_next_day_flag5,
                over_end_next_day_flag5 = :over_end_next_day_flag5,

                rest_start_time5 = :rest_start_time5,
                rest_end_time5 = :rest_end_time5,
                rest_start_next_day_flag5 = :rest_start_next_day_flag5,
                rest_end_next_day_flag5 = :rest_end_next_day_flag5,
            ";
            $insert_sql['overtime'] = "
                over_start_time,
                over_end_time,
                over_start_next_day_flag,
                over_end_next_day_flag,
                rest_start_time1,
                rest_end_time1,
                rest_start_next_day_flag1,
                rest_end_next_day_flag1,
                over_start_time2,
                over_end_time2,
                over_start_next_day_flag2,
                over_end_next_day_flag2,
                rest_start_time2,
                rest_end_time2,
                rest_start_next_day_flag2,
                rest_end_next_day_flag2,
                over_start_time3,
                over_end_time3,
                over_start_next_day_flag3,
                over_end_next_day_flag3,
                rest_start_time3,
                rest_end_time3,
                rest_start_next_day_flag3,
                rest_end_next_day_flag3,
                over_start_time4,
                over_end_time4,
                over_start_next_day_flag4,
                over_end_next_day_flag4,
                rest_start_time4,
                rest_end_time4,
                rest_start_next_day_flag4,
                rest_end_next_day_flag4,
                over_start_time5,
                over_end_time5,
                over_start_next_day_flag5,
                over_end_next_day_flag5,
                rest_start_time5,
                rest_end_time5,
                rest_start_next_day_flag5,
                rest_end_next_day_flag5,
            ";
            $insert_place['overtime'] = "
                :over_start_time1,
                :over_end_time1,
                :over_start_next_day_flag1,
                :over_end_next_day_flag1,
                :rest_start_time1,
                :rest_end_time1,
                :rest_start_next_day_flag1,
                :rest_end_next_day_flag1,
                :over_start_time2,
                :over_end_time2,
                :over_start_next_day_flag2,
                :over_end_next_day_flag2,
                :rest_start_time2,
                :rest_end_time2,
                :rest_start_next_day_flag2,
                :rest_end_next_day_flag2,
                :over_start_time3,
                :over_end_time3,
                :over_start_next_day_flag3,
                :over_end_next_day_flag3,
                :rest_start_time3,
                :rest_end_time3,
                :rest_start_next_day_flag3,
                :rest_end_next_day_flag3,
                :over_start_time4,
                :over_end_time4,
                :over_start_next_day_flag4,
                :over_end_next_day_flag4,
                :rest_start_time4,
                :rest_end_time4,
                :rest_start_next_day_flag4,
                :rest_end_next_day_flag4,
                :over_start_time5,
                :over_end_time5,
                :over_start_next_day_flag5,
                :over_end_next_day_flag5,
                :rest_start_time5,
                :rest_end_time5,
                :rest_start_next_day_flag5,
                :rest_end_next_day_flag5,
            ";
            $types = array_merge($types, array(
                'text', //          over_start_time
                'text', //          over_end_time
                'integer', //       over_start_next_day_flag
                'integer', //       over_end_next_day_flag
                'text', //          rest_start_time1
                'text', //          rest_end_time1
                'integer', //       rest_start_next_day_flag1
                'integer', //       rest_end_next_day_flag1
                'text', //          over_start_time2
                'text', //          over_end_time2
                'integer', //       over_start_next_day_flag2
                'integer', //       over_end_next_day_flag2
                'text', //          rest_start_time2
                'text', //          rest_end_time2
                'integer', //       rest_start_next_day_flag2
                'integer', //       rest_end_next_day_flag2
                'text', //          over_start_time3
                'text', //          over_end_time3
                'integer', //       over_start_next_day_flag3
                'integer', //       over_end_next_day_flag3
                'text', //          rest_start_time3
                'text', //          rest_end_time3
                'integer', //       rest_start_next_day_flag3
                'integer', //       rest_end_next_day_flag3
                'text', //          over_start_time4
                'text', //          over_end_time4
                'integer', //       over_start_next_day_flag4
                'integer', //       over_end_next_day_flag4
                'text', //          rest_start_time4
                'text', //          rest_end_time4
                'integer', //       rest_start_next_day_flag4
                'integer', //       rest_end_next_day_flag4
                'text', //          over_start_time5
                'text', //          over_end_time5
                'integer', //       over_start_next_day_flag5
                'integer', //       over_end_next_day_flag5
                'text', //          rest_start_time5
                'text', //          rest_end_time5
                'integer', //       rest_start_next_day_flag5
                'integer', //       rest_end_next_day_flag5
            ));
        }

        // --時短SQL
        if ($post['is_shorter']) {
            $update_sql['shorter'] = "
                base_shorter_minutes = :base_shorter_minutes,
                real_shorter_minutes = :real_shorter_minutes,
            ";
            $insert_sql['shorter'] = "
                base_shorter_minutes,
                real_shorter_minutes,
            ";
            $insert_place['shorter'] = "
                :base_shorter_minutes,
                :real_shorter_minutes,
            ";
            $types = array_merge($types, array(
                'integer', //       base_shorter_minutes
                'integer', //       real_shorter_minutes
            ));
        }

        $types = array_merge($types, array(
            'text', //          update_emp_id
            'text', //          update_time
            'text', //          emp_id
            'text', //          date
            'text', //          memo
        ));

        if (array_key_exists('pattern', $this->_tbl['results'])) {
            $sth = $this->db->prepare(
                "
                UPDATE atdbkrslt SET
                    {$update_sql['result']}
                    {$update_sql['overtime']}
                    {$update_sql['shorter']}
                    update_emp_id = :update_emp_id,
                    update_time = :update_time,
                    reg_prg_flg = 'S',
                    memo = :memo
                WHERE emp_id= :emp_id AND date = :date
                ", $types, MDB2_PREPARE_MANIP
            );
        }
        else {
            $sth = $this->db->prepare(
                "
                INSERT INTO atdbkrslt (
                    {$insert_sql['result']}
                    {$insert_sql['overtime']}
                    {$insert_sql['shorter']}
                    update_emp_id,
                    update_time,
                    emp_id,
                    date,
                    previous_day_flag,
                    reg_prg_flg,
                    memo
                ) VALUES (
                    {$insert_place['result']}
                    {$insert_place['overtime']}
                    {$insert_place['shorter']}
                    :update_emp_id,
                    :update_time,
                    :emp_id,
                    :date,
                    0,
                    'S',
                    :memo
               )", $types, MDB2_PREPARE_MANIP
            );
        }
        $res = $sth->execute($post);
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_Personal: save_results: INSERT/UPADTE atdbkrslt: ' . $res->getDebugInfo());
            return false;
        }
        $sth->free();

        if ($post['is_overtime']) {
            // 残業理由をセット
            if (!empty($this->_tbl['results']['ovtmrsn_emp_id'])) {
                $sth = $this->db->prepare(
                    "
                UPDATE atdbkrslt_ovtmrsn SET
                    ovtm_reason_id = :ovtm_reason_id1,
                    ovtm_reason = :ovtm_reason1,
                    ovtm_reason_id2 = :ovtm_reason_id2,
                    ovtm_reason2 = :ovtm_reason2,
                    ovtm_reason_id3 = :ovtm_reason_id3,
                    ovtm_reason3 = :ovtm_reason3,
                    ovtm_reason_id4 = :ovtm_reason_id4,
                    ovtm_reason4 = :ovtm_reason4,
                    ovtm_reason_id5 = :ovtm_reason_id5,
                    ovtm_reason5 = :ovtm_reason5
                WHERE emp_id= :emp_id AND date = :date
                ", array(
                    'integer',
                    'text',
                    'integer',
                    'text',
                    'integer',
                    'text',
                    'integer',
                    'text',
                    'integer',
                    'text',
                    'text',
                    'text',
                    ), MDB2_PREPARE_MANIP
                );
            }
            else {
                $sth = $this->db->prepare(
                    "
                INSERT INTO atdbkrslt_ovtmrsn (
                    emp_id,
                    date,
                    ovtm_reason_id,
                    ovtm_reason,
                    ovtm_reason_id2,
                    ovtm_reason2,
                    ovtm_reason_id3,
                    ovtm_reason3,
                    ovtm_reason_id4,
                    ovtm_reason4,
                    ovtm_reason_id5,
                    ovtm_reason5
                ) VALUES (
                    :emp_id,
                    :date,
                    :ovtm_reason_id1,
                    :ovtm_reason1,
                    :ovtm_reason_id2,
                    :ovtm_reason2,
                    :ovtm_reason_id3,
                    :ovtm_reason3,
                    :ovtm_reason_id4,
                    :ovtm_reason4,
                    :ovtm_reason_id5,
                    :ovtm_reason5
               )", array(
                    'text',
                    'text',
                    'integer',
                    'text',
                    'integer',
                    'text',
                    'integer',
                    'text',
                    'integer',
                    'text',
                    'integer',
                    'text',
                    ), MDB2_PREPARE_MANIP
                );
            }

            $res = $sth->execute($post);
            if (PEAR::isError($res)) {
                $this->db->rollback();
                cmx_log('ROLLBACK: Cmx_Shift_Personal: save_results: INSERT/UPDATE atdbkrslt_ovtmrsn: ' . $res->getDebugInfo());
                return false;
            }
            $sth->free();
        }

        // ds_plan_staff
        list($year, $month) = $this->_sdate->cours(date('Y-m-d', strtotime($date)));
        if (!$this->_staff->is_plan_staff($this->_ward->group_id(), $year, $month)) {
            $this->_staff->master_staff_copy($this->_ward->group_id(), $year, $month);
        }

        // 繰越時間更新
        $this->_staff->set_carryover($date);

        // commit
        $this->db->commit();
        $this->log->log("実績保存:$date", $post);
        return true;
    }

    /**
     * 残業保存
     * 個人勤務表:残業入力画面で「登録」ボタンを押したときに実行される
     *
     * @param str $date   保存する日付
     * @param arr $data   保存する勤務実績データ
     *
     * @return boolean true:成功 false:失敗
     */
    function save_overtime($date, $update_emp_id, $post)
    {
        // 実績が無ければ保存しない
        if (empty($this->_tbl['results'])) {
            return;
        }

        // emp_id
        $post['emp_id'] = $this->_emp_id;

        // date
        $post['date'] = $date;

        // time
        $post['over_start_time'] = $post['over_start_hour'] . $post['over_start_min'];
        $post['over_end_time'] = $post['over_end_hour'] . $post['over_end_min'];
        $post['over_start_time2'] = $post['over_start_hour2'] . $post['over_start_min2'];
        $post['over_end_time2'] = $post['over_end_hour2'] . $post['over_end_min2'];

        // flag
        if (!$post['over_start_next_day_flag']) {
            $post['over_start_next_day_flag'] = '0';
        }
        if (!$post['over_end_next_day_flag']) {
            $post['over_end_next_day_flag'] = '0';
        }
        if (!$post['over_start_next_day_flag2']) {
            $post['over_start_next_day_flag2'] = '0';
        }
        if (!$post['over_end_next_day_flag2']) {
            $post['over_end_next_day_flag2'] = '0';
        }

        // overtime
        if (!$post['over_start_time']) {
            $post['ovtm_reason_id'] = null;
            $post['ovtm_reason'] = '';
        }
        if (!$post['over_start_time2']) {
            $post['ovtm_reason_id2'] = null;
            $post['ovtm_reason2'] = '';
        }

        // update_time
        $post['update_time'] = date('YmdHis');

        // $update_emp_id
        $post['update_emp_id'] = $update_emp_id;

        // トランザクション開始
        $this->db->beginNestedTransaction();

        // 実績をセット
        $sth = $this->db->prepare(
            "
                UPDATE atdbkrslt SET
                    over_start_time = :over_start_time,
                    over_end_time = :over_end_time,
                    over_start_next_day_flag = :over_start_next_day_flag,
                    over_end_next_day_flag = :over_end_next_day_flag,
                    over_start_time2 = :over_start_time2,
                    over_end_time2 = :over_end_time2,
                    over_start_next_day_flag2 = :over_start_next_day_flag2,
                    over_end_next_day_flag2 = :over_end_next_day_flag2,
                    update_emp_id = :update_emp_id,
                    update_time = :update_time,
                    reg_prg_flg = 'S'
                WHERE emp_id= :emp_id AND date = :date
                ", array(
            'text',
            'text',
            'integer',
            'integer',
            'text',
            'text',
            'integer',
            'integer',
            'text',
            'text',
            'text',
            'text',
            ), MDB2_PREPARE_MANIP
        );
        $res = $sth->execute($post);
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_Personal: save_overtime: UPDATE atdbkrslt: ' . $res->getDebugInfo());
            return false;
        }
        $sth->free();

        // 残業理由をセット
        if (!empty($this->_tbl['results']['ovtmrsn_emp_id'])) {
            $sth = $this->db->prepare(
                "
                UPDATE atdbkrslt_ovtmrsn SET
                    ovtm_reason_id = :ovtm_reason_id,
                    ovtm_reason = :ovtm_reason,
                    ovtm_reason_id2 = :ovtm_reason_id2,
                    ovtm_reason2 = :ovtm_reason2
                WHERE emp_id= :emp_id AND date = :date
                ", array(
                'integer',
                'text',
                'integer',
                'text',
                'text',
                'text',
                ), MDB2_PREPARE_MANIP
            );
        }
        else {
            $sth = $this->db->prepare(
                "
                INSERT INTO atdbkrslt_ovtmrsn (
                    emp_id,
                    date,
                    ovtm_reason_id,
                    ovtm_reason,
                    ovtm_reason_id2,
                    ovtm_reason2
                ) VALUES (
                    :emp_id,
                    :date,
                    :ovtm_reason_id,
                    :ovtm_reason,
                    :ovtm_reason_id2,
                    :ovtm_reason2
               )", array(
                'text',
                'text',
                'integer',
                'text',
                'integer',
                'text',
                ), MDB2_PREPARE_MANIP
            );
        }

        $res = $sth->execute($post);
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_Personal: save_overtime: INSERT/UPDATE atdbkrslt_ovtmrsn: ' . $res->getDebugInfo());
            return false;
        }
        $sth->free();

        // 繰越時間更新
        $this->_staff->set_carryover($date);

        // commit
        $this->db->commit();
        $this->log->log('save_overtime', $post);
        return true;
    }

    /**
     * 勤務希望保存
     * 個人勤務表画面で「希望登録」ボタンを押したときに実行される
     *
     * @param str $date   保存する日付
     * @param arr $data   保存する勤務実績データ
     *
     * @return boolean true:成功 false:失敗
     */
    function save_hope($post)
    {
        $data = array();
        foreach ($post['hope'] as $date => $pattern) {
            $data[] = array(
                'group_id' => $this->_ward->group_id(),
                'emp_id' => $this->_emp_id,
                'duty_date' => $date,
                'pattern_id' => $this->_ward->pattern_id(),
                'atdptn_ptn_id' => (int)substr($pattern, 0, 2),
                'reason' => (int)substr($pattern, 2, 2) ? substr($pattern, 2, 2) : '',
            );
        }

        // トランザクション開始
        $this->db->beginNestedTransaction();

        // DELETE
        $sth = $this->db->prepare('
            DELETE FROM duty_shift_plan_individual
            WHERE emp_id = :emp_id AND duty_date = :duty_date
            ', array('text', 'text'), MDB2_PREPARE_MANIP);
        $res = $this->db->extended->executeMultiple($sth, $data);
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_Personal: save_hope: DELETE FROM duty_shift_plan_individual: ' . $res->getDebugInfo());
            return false;
        }

        // INSERT
        $sth = $this->db->prepare('
            INSERT INTO duty_shift_plan_individual
            (group_id, emp_id, duty_date, pattern_id, atdptn_ptn_id, reason)
            VALUES (:group_id, :emp_id, :duty_date, :pattern_id, :atdptn_ptn_id, :reason)
            ', array('text', 'text', 'text', 'integer', 'integer', 'text'), MDB2_PREPARE_MANIP);
        $res = $this->db->extended->executeMultiple($sth, $data);
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_Personal: save_hope: INSERT INTO duty_shift_plan_individual: ' . $res->getDebugInfo());
            return false;
        }

        // commit
        $this->db->commit();
        $this->log->log('save_hope', $post);
        return true;
    }

    function validate_results($data)
    {
        $err = array();

        // 実績変更
        if ($data['is_result_edit']) {

            // 勤務実績
            if (empty($data['ptn'])) {
                $err["ptn"] = '選択されていません';
            }

            // 出退勤
            if ((empty($data['start_hour']) || empty($data['start_min']) || empty($data['end_hour']) || empty($data['end_min'])) && !(empty($data['start_hour']) && empty($data['start_min']) && empty($data['end_hour']) && empty($data['end_min']))) {
                $err["work_time"] = '時刻が選択されていません';
            }
            else if ($data['start_hour'] && "{$data['start_hour']}{$data['start_min']}" >= "{$data['end_hour']}{$data['end_min']}" && "{$data['start_hour']}{$data['start_min']}{$data['end_hour']}{$data['end_min']}" !== '00000000') {
                $err["work_time"] = '時刻が正しくありません';
            }

            // 休憩
            if ((empty($data['rest_start_hour']) || empty($data['rest_start_min']) || empty($data['rest_end_hour']) || empty($data['rest_end_min'])) && !(empty($data['rest_start_hour']) && empty($data['rest_start_min']) && empty($data['rest_end_hour']) && empty($data['rest_end_min']))) {
                $err["rest_time"] = '時刻が選択されていません';
            }
            else if ($data['rest_start_hour'] && "{$data['rest_start_hour']}{$data['rest_start_min']}" >= "{$data['rest_end_hour']}{$data['rest_end_min']}" && "{$data['end_hour']}{$data['end_min']}" < "2400") {
                $err["rest_time"] = '時刻が正しくありません';
            }
            else if ($data['rest_start_hour'] && ("{$data['start_hour']}{$data['start_min']}" > "{$data['rest_start_hour']}{$data['rest_start_min']}" || "{$data['end_hour']}{$data['end_min']}" < "{$data['rest_end_hour']}{$data['rest_end_min']}")) {
                $err["rest_time"] = '休憩時刻が出退勤時刻の範囲を超えています';
            }

            // カンファレンス時間
            if ($data['conference_time'] && !is_numeric($data['conference_time'])) {
                $err["conference_time"] = '数字のみ入力してください';
            }
        }

        // 残業入力
        if ($data['is_overtime'] && is_array($data["over"])) {
            foreach ($data["over"] as $i => $value) {

                // 残業時間
                if ((empty($data['over'][$i]["over_start_hour"]) || empty($data['over'][$i]["over_start_min"]) || empty($data['over'][$i]["over_end_hour"]) || empty($data['over'][$i]["over_end_min"])) && !(empty($data['over'][$i]["over_start_hour"]) && empty($data['over'][$i]["over_start_min"]) && empty($data['over'][$i]["over_end_hour"]) && empty($data['over'][$i]["over_end_min"]))) {
                    $err["over_time"][$i] = '残業時刻が選択されていません';
                }
                else if ($data['over'][$i]["over_start_hour"] && "{$data['over'][$i]["over_start_hour"]}{$data['over'][$i]["over_start_min"]}" >= "{$data['over'][$i]["over_end_hour"]}{$data['over'][$i]["over_end_min"]}") {
                    $err["over_time"][$i] = '残業時刻が正しくありません';
                }

                // 残業休憩
                else if ((empty($data['over'][$i]["rest_over_start_hour"]) || empty($data['over'][$i]["rest_over_start_min"]) || empty($data['over'][$i]["rest_over_end_hour"]) || empty($data['over'][$i]["rest_over_end_min"])) && !(empty($data['over'][$i]["rest_over_start_hour"]) && empty($data['over'][$i]["rest_over_start_min"]) && empty($data['over'][$i]["rest_over_end_hour"]) && empty($data['over'][$i]["rest_over_end_min"]))) {
                    $err["over_time"][$i] = '休憩時刻が選択されていません';
                }
                else if ($data['over'][$i]["rest_over_start_hour"] && "{$data['over'][$i]["rest_over_start_hour"]}{$data['over'][$i]["rest_over_start_min"]}" >= "{$data['over'][$i]["rest_over_end_hour"]}{$data['over'][$i]["rest_over_end_min"]}") {
                    $err["over_time"][$i] = '休憩時刻が正しくありません';
                }
                else if ($data['over'][$i]["rest_over_start_hour"] && ("{$data['over'][$i]["over_start_hour"]}{$data['over'][$i]["over_start_min"]}" > "{$data['over'][$i]["rest_over_start_hour"]}{$data['over'][$i]["rest_over_start_min"]}" || "{$data['over'][$i]["over_end_hour"]}{$data['over'][$i]["over_end_min"]}" < "{$data['over'][$i]["rest_over_end_hour"]}{$data['over'][$i]["rest_over_end_min"]}")) {
                    $err["over_time"][$i] = '休憩時刻が残業時刻の範囲を超えています';
                }

                // 残業時間の重なり
                if (empty($err["over_time"][$i]) && is_array($data["over_start_hour"])) {
                    foreach ($data["over_start_hour"] as $j => $value) {
                        if ($i === $j) {
                            continue;
                        }
                        if ($data['over'][$i]["over_start_hour"] && $data["over_start_hour"][$j] && "{$data["over_start_hour"][$j]}{$data["over_start_min"][$j]}" <= "{$data['over'][$i]["over_start_hour"]}{$data['over'][$i]["over_start_min"]}" && "{$data['over'][$i]["over_start_hour"]}{$data['over'][$i]["over_start_min"]}" < "{$data["over_end_hour"][$j]}{$data["over_end_min"][$j]}") {
                            $err["over_time"][$i] = "他の残業時刻と重なっています";
                            break;
                        }
                    }
                }

                // 残業理由
                if (in_array($data['over'][$i]["ovtm_reason_name"], array("その他", "緊急呼出　手術", "緊急呼出　内視鏡", "緊急呼出　カテ"), true) && $data['over'][$i]["over_start_hour"] && isset($data['over'][$i]["ovtm_reason_id"]) && !$data['over'][$i]["ovtm_reason"]) {
                    $err["reason"][$i] = '残業理由もしくは患者名が入力されていません。';
                }
                else if ($data['over'][$i]["over_start_hour"] && $data['over'][$i]["ovtm_reason"] && mb_strlen($data["ovtm_reason"]) > 100) {
                    $err["reason"][$i] = '残業理由は100文字以内におさめてください。';
                }
            }
        }

        return $err;
    }

}
