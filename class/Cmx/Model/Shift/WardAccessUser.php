<?php

require_once('Cmx.php');
require_once('Cmx/Core/Model.php');
require_once('Cmx/Model/Employee.php');
require_once('Cmx/Model/Shift/Ward.php');

/**
 * 勤務表：部署・参照可能な職員クラス
 *
 * 勤務表の部署へ参照可能な職員のクラス
 *
 */
class Cmx_Shift_WardAccessUser extends Model
{

    var $_group_id;

    // コンストラクタ
    function Cmx_Shift_WardAccessUser($group_id)
    {
        parent::connectDB();
        $this->_group_id = $group_id;
    }

    // 職員リスト
    function lists()
    {
        $sth = $this->db->prepare("
            SELECT * FROM duty_shift_group_access_staff JOIN duty_shift_group USING (group_id) WHERE group_id= ? ORDER BY id
        ");
        $res = $sth->execute(array($this->_group_id));

        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            array_push($list, new Cmx_Employee($row['emp_id']));
        }
        return $list;
    }

    // 参照可能部署リスト
    function access_lists($emp_id)
    {
        $sth = $this->db->prepare("
            SELECT group_id FROM duty_shift_group_access_staff JOIN duty_shift_group USING (group_id) WHERE emp_id= ? ORDER BY id
        ");
        $res = $sth->execute(array($emp_id));

        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            array_push($list, new Cmx_Shift_Ward($row['group_id']));
        }
        return $list;
    }

    /**
     * データ更新
     *
     * @param type $post POSTデータ
     * @return true: 成功 false:失敗
     */
    function update($post)
    {
        $this->db->beginNestedTransaction();

        $data = array();
        for ($i = 0; $i < count($post["access_user"]); $i++) {
            $data[] = array(
                'group_id' => $post['group_id'],
                'emp_id' => $post['access_user'][$i],
            );
        }

        // DELETE
        $sth = $this->db->prepare(
            "DELETE FROM duty_shift_group_access_staff WHERE group_id = ?", array('text'), MDB2_PREPARE_MANIP);
        $res = $sth->execute(array($post['group_id']));
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_WardAccessUser: update: DELETE FROM duty_shift_group_access_staff: ' . $res->getDebugInfo());
            return false;
        }

        // INSERT
        $sth = $this->db->prepare(
            '
            INSERT INTO duty_shift_group_access_staff
            (group_id, emp_id)
            VALUES (:group_id, :emp_id)
           ', array('text', 'text'), MDB2_PREPARE_MANIP);
        $res = $this->db->extended->executeMultiple($sth, $data);
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_WardAccessUser: update: INSERT INTO duty_shift_group_access_staff: ' . $res->getDebugInfo());
            return false;
        }

        // commit
        $this->db->commit();
        return true;
    }

}
