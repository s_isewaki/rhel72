<?php

//
// ��̳ɽ���ײ�Ư���֥��饹
//
//
require_once('Cmx.php');
require_once('Cmx/Core/Model.php');

class Cmx_Shift_WorkingHours extends Model
{

    var $_year;
    var $_cours;

    // Constractor
    function Cmx_Shift_WorkingHours($year = null, $cours = null)
    {
        parent::connectDB();
        if ($year) {
            $this->_year = $year;
        }
        if ($cours) {
            $this->_cours = $cours;
        }
    }

    // Fetch
    function fetch($year = null, $cours = null)
    {
        if ($year) {
            $this->_year = $year;
        }
        if ($cours) {
            $this->_cours = $cours;
        }

        $sth = $this->db->prepare("SELECT * FROM duty_shift_cours_working_hours WHERE year= ? AND cours= ?");
        $res = $sth->execute(array($this->_year, $this->_cours));
        if ($res->numRows() <= 0) {
            return null;
        }
        return $res->fetchRow(MDB2_FETCHMODE_ASSOC);
    }

    // Lists
    function lists($year)
    {
        $sth = $this->db->prepare("SELECT * FROM duty_shift_cours_working_hours WHERE year= ? ORDER BY cours");
        $res = $sth->execute($year);

        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[$row["cours"]] = $row;
        }
        $this->_list[$year] = $list;
        return $list;
    }

    // Update
    function update($year, $working_hours, $holiday_count)
    {
        if (empty($this->_list[$year])) {
            $this->lists($year);
        }

        $count = count($working_hours);
        $data = array();
        foreach (range(1, $count) as $cours) {
            $data[] = array($working_hours[$cours] * 60, $holiday_count[$cours], $year, $cours);
        }

        $this->db->beginTransaction();

        if (empty($this->_list[$year])) {
            $sth = $this->db->prepare('INSERT INTO duty_shift_cours_working_hours(working_hours, holiday_count, year, cours) VALUES (?, ?, ?, ?)', array('integer', 'integer', 'text', 'integer'));
            $res = $this->db->extended->executeMultiple($sth, $data);
        }
        else {
            $sth = $this->db->prepare('UPDATE duty_shift_cours_working_hours SET working_hours= ?, holiday_count= ? WHERE year= ? AND cours= ?', array('integer', 'integer', 'text', 'integer'));
            $res = $this->db->extended->executeMultiple($sth, $data);
        }

        if (PEAR::isError($res)) {
            $this->db->rollback();
            return false;
        }
        else {
            $this->db->commit();
            return true;
        }
    }

}
