<?php

require_once('Cmx.php');
require_once('Cmx/Core/Model.php');

/**
 * 勤務表：勤務表集計
 */
class Cmx_Shift_PatternCount extends Model
{

    var $_group_id;
    var $_list;

    /**
     * コンストラクタ
     *
     * @param type $group_id 部署ID
     * @param type $emp_id 職員ID
     */
    function Cmx_Shift_PatternCount($group_id)
    {
        parent::connectDB();
        $this->_group_id = $group_id;
        $this->_list = null;
    }

    /**
     * 集計値
     * @return type
     */
    function values()
    {
        $sth = $this->db->prepare("
            SELECT * FROM duty_shift_sign_count_value WHERE group_id = ?
        ");
        $res = $sth->execute($this->_group_id);

        $values = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $values[$row['pattern']][$row['count_id']] = $row['count_value'];
        }
        return $values;
    }

    /**
     * 職種フィルタ
     * @return type
     */
    function job_filter()
    {
        $sth = $this->db->prepare("
            SELECT * FROM duty_shift_sign_count_name WHERE group_id = ?
        ");
        $res = $sth->execute($this->_group_id);

        $filter = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $filter[$row['count_id']] = unserialize($row['job_filter']);
        }
        return $filter;
    }

    /**
     * 集計名リスト
     * @return int
     */
    function name_lists()
    {
        $sth = $this->db->prepare("
            SELECT *
            FROM duty_shift_sign_count_name WHERE group_id = ? ORDER BY order_no, count_id
        ");
        $res = $sth->execute($this->_group_id);

        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['jobs'] = array_keys(unserialize($row['job_filter']));
            $list[] = $row;
        }
        return $list;
    }

    /**
     * 集計パターンリスト
     * @param $hash 勤務パターンハッシュ
     *
     * @return リスト
     */
    function pattern_lists($hash)
    {
        $sth = $this->db->prepare("SELECT DISTINCT pattern FROM duty_shift_sign_count_value WHERE group_id = ?");
        $res = $sth->execute($this->_group_id);

        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['name'] = $hash[$row['pattern']]->ptn_name();
            $list[] = $row;
        }
        return $list;
    }

    /**
     * 集計行リスト
     * @return リスト
     */
    function count_row_lists()
    {
        $sth = $this->db->prepare("
            SELECT count_id as count, count_name as name
            FROM duty_shift_sign_count_name WHERE count_row_flg AND group_id = ?
        ");
        $res = $sth->execute($this->_group_id);

        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            if (mb_strwidth($row["name"], 'euc-jp') > 4) {
                $row["size"] = 10;
            }
            else if (mb_strwidth($row["name"], 'euc-jp') > 6) {
                $row["size"] = 9;
            }
            $list[$row["count"]] = $row;
        }
        return $list;
    }

    /**
     * 集計列リスト
     * @return リスト
     */
    function count_col_lists()
    {
        $sth = $this->db->prepare("
            SELECT count_id as count, count_name as name
            FROM duty_shift_sign_count_name WHERE count_col_flg AND group_id = ?
        ");
        $res = $sth->execute($this->_group_id);

        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            if (mb_strwidth($row["name"], 'euc-jp') > 4) {
                $row["size"] = 10;
            }
            else if (mb_strwidth($row["name"], 'euc-jp') > 6) {
                $row["size"] = 9;
            }
            $list[$row["count"]] = $row;
        }
        return $list;
    }

    /**
     * データ更新
     *
     * @param type $post POSTデータ
     * @return true: 成功 false:失敗
     */
    function update($post)
    {
        $this->db->beginNestedTransaction();

        $name = array();
        $value = array();
        for ($i = 1; $i <= count($post["name"]); $i++) {
            $count_job = array();
            foreach ($post["count_job"][$i] as $job) {
                if (!$job) {
                    continue;
                }
                $count_job[$job] = 1;
            }

            $name[] = array(
                'group_id' => $post['pattern_id'],
                'count_id' => $i,
                'count_name' => $post['name'][$i],
                'count_row_flg' => $post["count_row"][$i] ? true : false,
                'count_col_flg' => $post["count_col"][$i] ? true : false,
                'job_filter' => serialize($count_job),
                'order_no' => $i,
            );
            foreach ($post['value'] as $ptn => $values) {
                if ($post["value"][$ptn][$i] === '0' or ! $post["value"][$ptn][$i]) {
                    continue;
                }
                $value[] = array(
                    'group_id' => $post['pattern_id'],
                    'count_id' => $i,
                    'pattern' => substr($ptn, 1),
                    'count_value' => $values[$i],
                );
            }
        }

        // DELETE duty_shift_sign_count_name
        $sth = $this->db->prepare("DELETE FROM duty_shift_sign_count_name WHERE group_id = ?", array('integer'), MDB2_PREPARE_MANIP);
        $res = $sth->execute(array($post['pattern_id']));
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_PatternCount: update: DELETE FROM duty_shift_sign_count_name: ' . $res->getDebugInfo());
            return false;
        }

        // DELETE duty_shift_sign_count_value
        $sth = $this->db->prepare("DELETE FROM duty_shift_sign_count_value WHERE group_id = ?", array('integer'), MDB2_PREPARE_MANIP);
        $res = $sth->execute(array($post['pattern_id']));
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_PatternCount: update: DELETE FROM duty_shift_sign_count_value: ' . $res->getDebugInfo());
            return false;
        }

        // INSERT duty_shift_sign_count_name
        $sth = $this->db->prepare('
           INSERT INTO duty_shift_sign_count_name
           (group_id, count_id, count_name, count_row_flg, count_col_flg, order_no, job_filter)
           VALUES (:group_id, :count_id, :count_name, :count_row_flg, :count_col_flg, :order_no, :job_filter)
           ', array('integer', 'integer', 'text', 'bool', 'bool', 'integer', 'text'), MDB2_PREPARE_MANIP);
        $res = $this->db->extended->executeMultiple($sth, $name);
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_PatternCount: update: INSERT INTO duty_shift_sign_count_name: ' . $res->getDebugInfo());
            return false;
        }

        // INSERT duty_shift_sign_count_value
        $sth = $this->db->prepare('
           INSERT INTO duty_shift_sign_count_value
           (group_id, count_id, pattern, count_value)
           VALUES (:group_id, :count_id, :pattern, :count_value)
           ', array('integer', 'integer', 'text', 'text'), MDB2_PREPARE_MANIP);
        $res = $this->db->extended->executeMultiple($sth, $value);
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_PatternCount: update: INSERT INTO duty_shift_sign_count_value: ' . $res->getDebugInfo());
            return false;
        }

        // commit
        $this->db->commit();
        return true;
    }

}
