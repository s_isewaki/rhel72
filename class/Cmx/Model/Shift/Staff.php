<?php

require_once('Cmx.php');
require_once('Cmx/Model/Employee.php');
require_once('Cmx/Model/Shift/StaffPattern.php');
require_once('Cmx/Model/Shift/StaffHoliday.php');
require_once('Cmx/Model/Shift/StaffShorter.php');
require_once('Cmx/Model/Timecard/Overtime.php');
require_once('Cmx/Model/Shift/Ward.php');

/**
 * 勤務表：部署スタッフ
 *
 * @link http://trac.medi-system.co.jp/trac/comedix/wiki/tables/duty_shift_staff
 * @link http://trac.medi-system.co.jp/trac/comedix/wiki/tables/duty_shift_plan_staff
 * @link http://trac.medi-system.co.jp/trac/comedix/wiki/tables/duty_shift_plan_team
 * @link http://trac.medi-system.co.jp/trac/comedix/wiki/tables/duty_shift_staff_employment
 * @link http://trac.medi-system.co.jp/trac/comedix/wiki/tables/duty_shift_staff_team
 */
class Cmx_Shift_Staff extends Cmx_Employee
{

    var $_row;
    var $_is_plan_staff;
    var $_pattern;

    /**
     * コンストラクタ
     *
     * @param Arrayならrowデータと判断、そうでなければ職員IDとして処理
     */
    function Cmx_Shift_Staff($row = null)
    {
        parent::connectDB();
        $this->_pattern = null;
        $this->_is_plan_staff = array();

        if (is_array($row)) {
            $this->_init($row);
        }
        else if (!is_null($row)) {
            $this->_select($row);
        }
    }

    // _init
    function _init($row)
    {
        $this->_row = $row;
    }

    // _select
    function _select($row)
    {
        $sth = $this->db->prepare("
            SELECT
                *,
                dss.group_id as group_id,
                e.emp_id as emp_id,
                jobmst.link_key as job_link_key,
                stmst.link_key as st_link_key,
                classmst.link_key as class_link_key,
                atrbmst.link_key as atrb_link_key,
                deptmst.link_key1 as dept_link_key,
                deptmst.link_key2 as dept_link_key2,
                deptmst.link_key3 as dept_link_key3,
                classroom.link_key as room_link_key,
                dss.no AS order_no,
                duty_shift_staff_employment_shorter.week as shorter_week
            FROM empmst e
            JOIN login USING (emp_id)
            JOIN authmst USING (emp_id)
            LEFT JOIN duty_shift_staff dss USING (emp_id)
            LEFT JOIN empcond USING (emp_id)
            LEFT JOIN duty_shift_staff_employment USING (emp_id)
            LEFT JOIN duty_shift_staff_employment_shorter USING (emp_id)
            LEFT JOIN duty_shift_staff_team t USING (team_id)
            LEFT JOIN jobmst ON jobmst.job_id=emp_job
            LEFT JOIN stmst ON stmst.st_id=emp_st
            LEFT JOIN classmst ON classmst.class_id=emp_class
            LEFT JOIN atrbmst ON atrbmst.atrb_id=emp_attribute
            LEFT JOIN deptmst ON deptmst.dept_id=emp_dept
            LEFT JOIN classroom ON classroom.room_id=emp_room
            WHERE e.emp_id= :emp_id
        ");
        $res = $sth->execute(array('emp_id' => $row));
        $sth->free();
        $this->_init($res->fetchRow(MDB2_FETCHMODE_ASSOC));
    }

    // _plan_select
    function _plan_select($emp_id, $year, $month)
    {
        $sth = $this->db->prepare("
            SELECT
                *,
                dsps.group_id as group_id,
                e.emp_id as emp_id,
                jobmst.link_key as job_link_key,
                stmst.link_key as st_link_key,
                classmst.link_key as class_link_key,
                atrbmst.link_key as atrb_link_key,
                deptmst.link_key1 as dept_link_key,
                deptmst.link_key2 as dept_link_key2,
                deptmst.link_key3 as dept_link_key3,
                classroom.link_key as room_link_key,
                dsps.no AS order_no,
                duty_shift_staff_employment_shorter.week AS shorter_week
            FROM empmst e
            JOIN login USING (emp_id)
            JOIN authmst USING (emp_id)
            LEFT JOIN duty_shift_plan_staff dsps USING (emp_id)
            LEFT JOIN empcond USING (emp_id)
            LEFT JOIN duty_shift_staff_employment USING (emp_id)
            LEFT JOIN duty_shift_staff_employment_shorter USING (emp_id)
            LEFT JOIN jobmst ON jobmst.job_id=emp_job
            LEFT JOIN stmst ON stmst.st_id=emp_st
            LEFT JOIN classmst ON classmst.class_id=emp_class
            LEFT JOIN atrbmst ON atrbmst.atrb_id=emp_attribute
            LEFT JOIN deptmst ON deptmst.dept_id=emp_dept
            LEFT JOIN classroom ON classroom.room_id=emp_room
            WHERE e.emp_id= ? AND dsps.duty_yyyy= ? and dsps.duty_mm= ?
        ");
        $res = $sth->execute(array($emp_id, $year, $month));
        $sth->free();
        $this->_init($res->fetchRow(MDB2_FETCHMODE_ASSOC));
    }

    /**
     * 部署スタッフリスト
     * 指定部署、指定年月の部署スタッフの一覧を返す
     * 指定年月の部署スタッフが未登録の場合、マスタのスタッフ一覧を返す
     *
     * @param $group_id 部署ID
     * @param $year 年
     * @param $month 月(クール)
     * @return array スタッフ一覧
     */
    function lists($group_id, $year, $month)
    {
        // plan_staffの存在チェック
        $is_plan_staff = $this->is_plan_staff($group_id, $year, $month);

        if ($is_plan_staff) {
            $sth = $this->db->prepare("
                SELECT
                    *,
                    dsps.group_id as group_id,
                    dsps.emp_id as emp_id,
                    jobmst.link_key as job_link_key,
                    stmst.link_key as st_link_key,
                    classmst.link_key as class_link_key,
                    atrbmst.link_key as atrb_link_key,
                    deptmst.link_key1 as dept_link_key,
                    deptmst.link_key2 as dept_link_key2,
                    deptmst.link_key3 as dept_link_key3,
                    classroom.link_key as room_link_key,
                    dsps.no AS order_no,
                    duty_shift_staff_employment_shorter.week AS shorter_week
                FROM duty_shift_plan_staff dsps
                JOIN empmst USING (emp_id)
                JOIN login USING (emp_id)
                JOIN authmst USING (emp_id)
                LEFT JOIN empcond USING (emp_id)
                LEFT JOIN duty_shift_staff_employment USING (emp_id)
                LEFT JOIN duty_shift_staff_employment_shorter USING (emp_id)
                LEFT JOIN duty_shift_plan_team dspt ON dspt.group_id=dsps.group_id AND dspt.emp_id=dsps.emp_id AND dspt.duty_yyyy=dsps.duty_yyyy AND dspt.duty_mm=dsps.duty_mm
                LEFT JOIN duty_shift_staff_team USING (team_id)
                LEFT JOIN duty_shift_plan_staff_time dspst ON dspst.emp_id=dsps.emp_id AND dspst.duty_yyyy=dsps.duty_yyyy AND dspst.duty_mm=dsps.duty_mm
                LEFT JOIN jobmst ON jobmst.job_id=emp_job
                LEFT JOIN stmst ON stmst.st_id=emp_st
                LEFT JOIN classmst ON classmst.class_id=emp_class
                LEFT JOIN atrbmst ON atrbmst.atrb_id=emp_attribute
                LEFT JOIN deptmst ON deptmst.dept_id=emp_dept
                LEFT JOIN classroom ON classroom.room_id=emp_room
                WHERE dsps.group_id= ? AND dsps.duty_yyyy= ? and dsps.duty_mm= ?
                ORDER BY dsps.no
            ");
            $res = $sth->execute(array($group_id, $year, $month));
            $sth->free();
        }
        else {
            return $this->master_lists($group_id, $year, $month);
        }

        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            array_push($list, new Cmx_Shift_Staff($row));
        }
        return $list;
    }

    /**
     * 部署スタッフリスト(月単位)
     * 指定部署、指定年月の部署スタッフの一覧を返す
     * 指定年月の部署スタッフが未登録の場合、マスタのスタッフ一覧を返す
     * クール単位の部署で月別のスタッフリストがほしいときに使う
     *
     * @param $group_id 部署ID
     * @param $year 年
     * @param $month 月(クール)
     * @return array スタッフ一覧
     */
    function month_lists()
    {
        list($group_id, $year1, $month1, $year2, $month2, $year3, $month3) = func_get_args();

        $sql = "
                SELECT
                    *,
                    dsps.group_id as group_id,
                    dsps.emp_id as emp_id,
                    jobmst.link_key as job_link_key,
                    stmst.link_key as st_link_key,
                    classmst.link_key as class_link_key,
                    atrbmst.link_key as atrb_link_key,
                    deptmst.link_key1 as dept_link_key,
                    deptmst.link_key2 as dept_link_key2,
                    deptmst.link_key3 as dept_link_key3,
                    classroom.link_key as room_link_key,
                    dsps.no AS order_no,
                    duty_shift_staff_employment_shorter.week AS shorter_week
                FROM duty_shift_plan_staff dsps
                JOIN empmst USING (emp_id)
                JOIN login USING (emp_id)
                JOIN authmst USING (emp_id)
                LEFT JOIN empcond USING (emp_id)
                LEFT JOIN duty_shift_staff_employment USING (emp_id)
                LEFT JOIN duty_shift_staff_employment_shorter USING (emp_id)
                LEFT JOIN duty_shift_plan_team dspt ON dspt.group_id=dsps.group_id AND dspt.emp_id=dsps.emp_id AND dspt.duty_yyyy=dsps.duty_yyyy AND dspt.duty_mm=dsps.duty_mm
                LEFT JOIN duty_shift_staff_team USING (team_id)
                LEFT JOIN duty_shift_plan_staff_time dspst ON dspst.emp_id=dsps.emp_id AND dspst.duty_yyyy=dsps.duty_yyyy AND dspst.duty_mm=dsps.duty_mm
                LEFT JOIN jobmst ON jobmst.job_id=emp_job
                LEFT JOIN stmst ON stmst.st_id=emp_st
                LEFT JOIN classmst ON classmst.class_id=emp_class
                LEFT JOIN atrbmst ON atrbmst.atrb_id=emp_attribute
                LEFT JOIN deptmst ON deptmst.dept_id=emp_dept
                LEFT JOIN classroom ON classroom.room_id=emp_room
            ";

        // 2クール
        if (func_num_args() == 5) {
            $is_plan_staff1 = $this->is_plan_staff($group_id, $year1, $month1);
            $is_plan_staff2 = $this->is_plan_staff($group_id, $year2, $month2);
            if ($is_plan_staff1 or $is_plan_staff2) {
                $sth = $this->db->prepare($sql . "
                    WHERE dsps.group_id= ? AND (dsps.duty_yyyy= ? and dsps.duty_mm= ? OR dsps.duty_yyyy= ? and dsps.duty_mm= ?)
                    ORDER BY dsps.no, dsps.duty_yyyy, dsps.duty_mm
                ");
                $res = $sth->execute(array($group_id, $year1, $month1, $year2, $month2));
                $sth->free();
            }
            else {
                return $this->master_lists($group_id);
            }
        }

        // 3クール
        else {
            $is_plan_staff1 = $this->is_plan_staff($group_id, $year1, $month1);
            $is_plan_staff2 = $this->is_plan_staff($group_id, $year2, $month2);
            $is_plan_staff3 = $this->is_plan_staff($group_id, $year3, $month3);
            if ($is_plan_staff1 or $is_plan_staff2 or $is_plan_staff3) {
                $sth = $this->db->prepare($sql . "
                    WHERE dsps.group_id= ? AND (dsps.duty_yyyy= ? and dsps.duty_mm= ? OR dsps.duty_yyyy= ? and dsps.duty_mm= ? OR dsps.duty_yyyy= ? and dsps.duty_mm= ?)
                    ORDER BY dsps.no, dsps.duty_yyyy, dsps.duty_mm
                ");
                $res = $sth->execute(array($group_id, $year1, $month1, $year2, $month2, $year3, $month3));
                $sth->free();
            }
            else {
                return $this->master_lists($group_id);
            }
        }

        $list = array();
        $dup = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            if ($dup[$row['emp_id']]) {
                continue;
            }
            array_push($list, new Cmx_Shift_Staff($row));
            $dup[$row['emp_id']] = true;
        }
        return $list;
    }

    /**
     * 部署スタッフリスト(月単位)
     * 指定部署、指定年月の部署スタッフの一覧を返す
     * 指定年月の部署スタッフが未登録の場合、マスタのスタッフ一覧を返す
     * クール単位の部署で月別のスタッフリストがほしいときに使う
     *
     * @param $group_id 部署ID
     * @param $year 年
     * @param $month 月(クール)
     * @return array スタッフ一覧
     */
    function range_lists($group_id, $year, $month, $eyear = null, $emonth = null)
    {
        if (is_null($eyear)) {
            $start_date = date('Ymd', strtotime("$year-$month-1"));
            $end_date = date('Ymt', strtotime("$year-$month-1"));
        }
        else {
            $start_date = date('Ymd', strtotime("$year-$month-1"));
            $end_date = date('Ymt', strtotime("$eyear-$emonth-1"));
        }

        // 年月レンジ
        $ward = new Cmx_Shift_Ward($group_id);
        $sdate = $ward->shift_date();
        $range = $sdate->cours_range($start_date, $end_date);

        // 年月SQL
        $or_ym = array();
        $data = array($group_id);
        foreach ($range as $ym) {
            $or_ym[] = 'dsps.duty_yyyy= ? AND dsps.duty_mm= ? ';
            $data[] = $ym[0];
            $data[] = $ym[1];
        }

        $sql = "
            SELECT
                *,
                dsps.group_id as group_id,
                dsps.emp_id as emp_id,
                jobmst.link_key as job_link_key,
                stmst.link_key as st_link_key,
                classmst.link_key as class_link_key,
                atrbmst.link_key as atrb_link_key,
                deptmst.link_key1 as dept_link_key,
                deptmst.link_key2 as dept_link_key2,
                deptmst.link_key3 as dept_link_key3,
                classroom.link_key as room_link_key,
                dsps.no AS order_no,
                duty_shift_staff_employment_shorter.week AS shorter_week
            FROM duty_shift_plan_staff dsps
            JOIN empmst USING (emp_id)
            JOIN login USING (emp_id)
            JOIN authmst USING (emp_id)
            LEFT JOIN empcond USING (emp_id)
            LEFT JOIN duty_shift_staff_employment USING (emp_id)
            LEFT JOIN duty_shift_staff_employment_shorter USING (emp_id)
            LEFT JOIN duty_shift_plan_team dspt ON dspt.group_id=dsps.group_id AND dspt.emp_id=dsps.emp_id AND dspt.duty_yyyy=dsps.duty_yyyy AND dspt.duty_mm=dsps.duty_mm
            LEFT JOIN duty_shift_staff_team USING (team_id)
            LEFT JOIN duty_shift_plan_staff_time dspst ON dspst.emp_id=dsps.emp_id AND dspst.duty_yyyy=dsps.duty_yyyy AND dspst.duty_mm=dsps.duty_mm
            LEFT JOIN jobmst ON jobmst.job_id=emp_job
            LEFT JOIN stmst ON stmst.st_id=emp_st
            LEFT JOIN classmst ON classmst.class_id=emp_class
            LEFT JOIN atrbmst ON atrbmst.atrb_id=emp_attribute
            LEFT JOIN deptmst ON deptmst.dept_id=emp_dept
            LEFT JOIN classroom ON classroom.room_id=emp_room
        ";

        $sth = $this->db->prepare($sql . "WHERE dsps.group_id= ? AND (" . implode(' OR ', $or_ym) . ") ORDER BY dsps.no, dsps.duty_yyyy, dsps.duty_mm");
        $res = $sth->execute($data);
        $sth->free();

        $list = array();
        $dup = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            if ($dup[$row['emp_id']]) {
                continue;
            }
            $list[] = new Cmx_Shift_Staff($row);
            $dup[$row['emp_id']] = true;
        }

        if (empty($list)) {
            return $this->master_lists($group_id, $year, $month);
        }

        return $list;
    }

    /**
     * 部署スタッフリスト(マスタ)
     * 指定部署の部署スタッフ(マスタ)の一覧を返す
     *
     * @param $group_id 部署ID
     * @return array スタッフ一覧
     */
    function master_lists($group_id, $year = null, $month = null)
    {

        // duty_shift_plan_time
        $plan_time = '';
        $param = array();
        if ($year and $month) {
            $plan_time = 'LEFT JOIN duty_shift_plan_staff_time dspst ON dspst.emp_id=dss.emp_id AND dspst.duty_yyyy= :year AND dspst.duty_mm= :month';
            $param['year'] = $year;
            $param['month'] = $month;
        }

        $sth = $this->db->prepare("
                SELECT
                    *,
                    dss.group_id as group_id,
                    dss.emp_id as emp_id,
                    jobmst.link_key as job_link_key,
                    stmst.link_key as st_link_key,
                    classmst.link_key as class_link_key,
                    atrbmst.link_key as atrb_link_key,
                    deptmst.link_key1 as dept_link_key,
                    deptmst.link_key2 as dept_link_key2,
                    deptmst.link_key3 as dept_link_key3,
                    classroom.link_key as room_link_key,
                    dss.no AS order_no,
                    duty_shift_staff_employment_shorter.week AS shorter_week
                FROM duty_shift_staff dss
                JOIN empmst USING (emp_id)
                JOIN login USING (emp_id)
                JOIN authmst USING (emp_id)
                LEFT JOIN empcond USING (emp_id)
                LEFT JOIN duty_shift_staff_employment USING (emp_id)
                LEFT JOIN duty_shift_staff_team t USING (team_id)
                LEFT JOIN duty_shift_staff_employment_shorter USING (emp_id)
                $plan_time
                LEFT JOIN jobmst ON jobmst.job_id=emp_job
                LEFT JOIN stmst ON stmst.st_id=emp_st
                LEFT JOIN classmst ON classmst.class_id=emp_class
                LEFT JOIN atrbmst ON atrbmst.atrb_id=emp_attribute
                LEFT JOIN deptmst ON deptmst.dept_id=emp_dept
                LEFT JOIN classroom ON classroom.room_id=emp_room
                WHERE dss.group_id= :group_id
                ORDER BY dss.no
            ");
        $param['group_id'] = $group_id;
        $res = $sth->execute($param);
        $sth->free();

        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            array_push($list, new Cmx_Shift_Staff($row));
        }
        return $list;
    }

    /**
     * 全部署スタッフリスト(マスタ)
     * 全部署の部署スタッフ(マスタ)の一覧を返す
     *
     * @return array スタッフ一覧
     */
    function all_master_lists()
    {
        $sth = $this->db->prepare("
                SELECT
                    *,
                    dss.group_id as group_id,
                    dss.emp_id as emp_id,
                    jobmst.link_key as job_link_key,
                    stmst.link_key as st_link_key,
                    classmst.link_key as class_link_key,
                    atrbmst.link_key as atrb_link_key,
                    deptmst.link_key1 as dept_link_key,
                    deptmst.link_key2 as dept_link_key2,
                    deptmst.link_key3 as dept_link_key3,
                    classroom.link_key as room_link_key,
                    dss.no AS order_no,
                    duty_shift_staff_employment_shorter.week AS shorter_week
                FROM duty_shift_staff dss
                JOIN empmst USING (emp_id)
                JOIN login USING (emp_id)
                JOIN authmst USING (emp_id)
                LEFT JOIN empcond USING (emp_id)
                LEFT JOIN duty_shift_staff_employment USING (emp_id)
                LEFT JOIN duty_shift_staff_team t USING (team_id)
                LEFT JOIN duty_shift_staff_employment_shorter USING (emp_id)
                LEFT JOIN jobmst ON jobmst.job_id=emp_job
                LEFT JOIN stmst ON stmst.st_id=emp_st
                LEFT JOIN classmst ON classmst.class_id=emp_class
                LEFT JOIN atrbmst ON atrbmst.atrb_id=emp_attribute
                LEFT JOIN deptmst ON deptmst.dept_id=emp_dept
                LEFT JOIN classroom ON classroom.room_id=emp_room
                ORDER BY dss.no
            ");
        $res = $sth->execute();
        $sth->free();

        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[] = new Cmx_Shift_Staff($row);
        }
        return $list;
    }

    /**
     * 指定年月の部署スタッフ存在チェック
     *
     * @param $group_id 部署ID
     * @param $year 年
     * @param $month 月(クール)
     * @return true:あり false:なし
     */
    function is_plan_staff($group_id, $year, $month)
    {
        if (array_key_exists("$group_id/$year/$month", $this->_is_plan_staff)) {
            return $this->_is_plan_staff["$group_id/$year/$month"];
        }

        $is_plan_staff = $this->db->extended->getOne(
            "SELECT COUNT(*) FROM duty_shift_plan_staff WHERE group_id= ? AND duty_yyyy= ? and duty_mm= ?", null, array($group_id, $year, $month), array('text', 'text', 'text')
        );
        $this->_is_plan_staff["$group_id/$year/$month"] = $is_plan_staff;
        return ($is_plan_staff > 0) ? true : false;
    }

    /**
     * 部署スタッフリスト(マスタ)を年月スタッフリストにコピー
     *
     * @param $group_id
     * @param $year
     * @param $month
     * @return boolean
     */
    function master_staff_copy($group_id, $year, $month)
    {
        // トランザクション開始
        $this->db->beginNestedTransaction();

        // plan_staffのdelete
        $sth = $this->db->prepare(
            "DELETE FROM duty_shift_plan_staff WHERE group_id= ? AND duty_yyyy= ? AND duty_mm= ?", array('text', 'integer', 'integer'), MDB2_PREPARE_MANIP
        );
        $res = $sth->execute(array($group_id, $year, $month));
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_Staff: master_staff_copy: DELETE FROM duty_shift_plan_staff: ' . $res->getDebugInfo());
            return false;
        }
        $sth->free();

        // plan_staffのコピー
        $sth = $this->db->prepare(
            "INSERT INTO duty_shift_plan_staff SELECT group_id, emp_id, $year, $month, no, emp_st, group_id FROM duty_shift_staff JOIN empmst USING (emp_id) WHERE group_id= ?", array('text'), MDB2_PREPARE_MANIP
        );
        $res = $sth->execute(array($group_id));
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_Staff: master_staff_copy: INSERT INTO duty_shift_plan_staff: ' . $res->getDebugInfo());
            return false;
        }
        $sth->free();

        // plan_teamのdelete
        $sth = $this->db->prepare(
            "DELETE FROM duty_shift_plan_team WHERE  group_id= ? AND duty_yyyy= ? AND duty_mm= ?", array('text', 'integer', 'integer'), MDB2_PREPARE_MANIP
        );
        $res = $sth->execute(array($group_id, $year, $month));
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_Staff: master_staff_copy: DELETE FROM duty_shift_plan_team: ' . $res->getDebugInfo());
            return false;
        }
        $sth->free();

        // plan_teamのコピー
        $sth = $this->db->prepare(
            "INSERT INTO duty_shift_plan_team SELECT group_id, emp_id, $year, $month, team_id FROM duty_shift_staff WHERE group_id= ?", array('text'), MDB2_PREPARE_MANIP
        );
        $res = $sth->execute(array($group_id));
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_Staff: master_staff_copy: INSERT INTO duty_shift_plan_team: ' . $res->getDebugInfo());
            return false;
        }
        $sth->free();

        $this->db->commit();
        return true;
    }

    /**
     * 部署ID
     * @return 部署ID
     */
    function group_id()
    {
        return $this->_getter(__FUNCTION__);
    }

    /**
     * チーム
     * @return チーム名
     */
    function team_name()
    {
        return $this->_getter(__FUNCTION__);
    }

    /**
     * チームID
     * @return チームID
     */
    function team_id()
    {
        return $this->_getter(__FUNCTION__);
    }

    /**
     * 表示順
     * @return type
     */
    function order_no()
    {
        return $this->_getter(__FUNCTION__);
    }

    /**
     * 元部署ID
     * @return type
     */
    function main_group_id()
    {
        return $this->_row['main_group_id'] ? $this->_row['main_group_id'] : $this->group_id();
    }

    /**
     * 応援職員フラグ
     * @return type
     */
    function assist()
    {
        return $this->group_id() !== $this->main_group_id() ? '1' : '0';
    }

    /**
     * 他部署兼務
     *
     * @return true:あり false:なし
     */
    function other_post_flg()
    {
        return $this->_row["other_post_flg"] === "1" ? true : false;
    }

    /**
     * 他部署兼務(名称)
     *
     * @return true:あり false:なし
     */
    function other_post_name()
    {
        return $this->_row["other_post_flg"] === "1" ? 'あり' : 'なし';
    }

    /**
     * 夜勤の有無
     *
     * @return 0:なし 1:あり、2:夜専
     */
    function night_duty_flg()
    {
        switch ($this->_row["night_duty_flg"]) {
            case 1:
                return "1";
                break;
            case 2:
                return "2";
                break;
            default:
                return "0";
                break;
        }
    }

    /**
     * 夜勤の有無(名称)
     *
     * @return 0:なし 1:あり、2:夜専
     */
    function night_duty_name()
    {
        switch ($this->_row["night_duty_flg"]) {
            case 1:
                return 'あり';
                break;
            case 2:
                return '夜勤専従者';
                break;
            default:
                return 'なし';
                break;
        }
    }

    /**
     * 勤務日（月曜日）
     *
     * @return true:あり false:なし
     */
    function duty_mon_day_flg()
    {
        return $this->_row["duty_mon_day_flg"] === "1" or $this->_row["duty_mon_day_flg"] === null ? true : false;
    }

    /**
     * 勤務日（火曜日）
     *
     * @return true:あり false:なし
     */
    function duty_tue_day_flg()
    {
        return $this->_row["duty_tue_day_flg"] === "1" or $this->_row["duty_tue_day_flg"] === null ? true : false;
    }

    /**
     * 勤務日（水曜日）
     *
     * @return true:あり false:なし
     */
    function duty_wed_day_flg()
    {
        return $this->_row["duty_wed_day_flg"] === "1" or $this->_row["duty_wed_day_flg"] === null ? true : false;
    }

    /**
     * 勤務日（木曜日）
     *
     * @return true:あり false:なし
     */
    function duty_thurs_day_flg()
    {
        return $this->_row["duty_thurs_day_flg"] === "1" or $this->_row["duty_thurs_day_flg"] === null ? true : false;
    }

    /**
     * 勤務日（金曜日）
     *
     * @return true:あり false:なし
     */
    function duty_fri_day_flg()
    {
        return $this->_row["duty_fri_day_flg"] === "1" or $this->_row["duty_fri_day_flg"] === null ? true : false;
    }

    /**
     * 勤務日（土曜日）
     *
     * @return true:あり false:なし
     */
    function duty_sat_day_flg()
    {
        return $this->_row["duty_sat_day_flg"] === "1" or $this->_row["duty_sat_day_flg"] === null ? true : false;
    }

    /**
     * 勤務日（日曜日）
     *
     * @return true:あり false:なし
     */
    function duty_sun_day_flg()
    {
        return $this->_row["duty_sun_day_flg"] === "1" or $this->_row["duty_sun_day_flg"] === null ? true : false;
    }

    /**
     * 勤務日（祝日）
     *
     * @return true:あり false:なし
     */
    function duty_hol_day_flg()
    {
        return $this->_row["duty_hol_day_flg"] === "1" or $this->_row["duty_hol_day_flg"] === null ? true : false;
    }

    /**
     * 勤務可能パターンチェック
     * @return type
     */
    function is_pattern($week, $pattern)
    {
        if (empty($this->_pattern)) {
            $this->_pattern = new Cmx_Shift_StaffPattern($this->_row['group_id'], $this->_row['emp_id']);
        }
        return $this->_pattern->is_pattern($week, $pattern);
    }

    /**
     * 長期休暇リスト
     * @return type
     */
    function holiday()
    {
        $obj = new Cmx_Shift_StaffHoliday($this->_row['group_id'], $this->_row['emp_id']);
        return $obj->lists();
    }

    /**
     * 時短1歳終了日
     * @return type
     */
    function end_1y_date()
    {
        return $this->_row['end_1y_date'];
    }

    /**
     * 時短1歳終了日
     * @return type
     */
    function end_3y_date()
    {
        return $this->_row['end_3y_date'];
    }

    /**
     * 時短設定（曜日別）
     * @return type
     */
    function shorter_week()
    {
        return unserialize($this->_row['shorter_week']);
    }

    /**
     * 時短1歳終了日対現在日付の比較
     * @return type
     */
    function is_end_1y($date)
    {
        if (empty($date)) {
            return false;
        }
        return date('Y-m-d', strtotime($date)) <= $this->_row['end_1y_date'] ? true : false;
    }

    /**
     * 時短3歳終了日対現在日付の比較
     * @return type
     */
    function is_end_3y($date)
    {
        if (empty($date)) {
            return false;
        }
        return date('Y-m-d', strtotime($date)) <= $this->_row['end_3y_date'] ? true : false;
    }

    /**
     * 時短対象判定
     * @return type
     */
    function is_shorter($date)
    {
        if (empty($date)) {
            return false;
        }
        if ($this->is_end_1y($date) || $this->is_end_3y($date)) {
            return true;
        }
        return false;
    }

    /**
     * 備考
     * @return type
     */
    function memo()
    {
        return $this->_getter(__FUNCTION__);
    }

    /**
     * 職員個人勤務条件の更新
     * @param type $post
     * @return boolean
     */
    function employee_update($post)
    {
        $this->db->beginNestedTransaction();

        // DELETE duty_shift_staff_employment
        $sth = $this->db->prepare("DELETE FROM duty_shift_staff_employment WHERE group_id != ? AND emp_id = ?", array('text', 'text'), MDB2_PREPARE_MANIP);
        $res = $sth->execute(array($post['group_id'], $post['emp_id']));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            cmx_log('ROLLBACK: Cmx_Shift_Staff: plan_staff_update: DELETE FROM duty_shift_staff_employment: ' . $res->getDebugInfo());
            return false;
        }
        $sth->free();

        // duty_shift_staff_employment
        $is_employment = $this->db->extended->getOne(
            "SELECT COUNT(*) FROM duty_shift_staff_employment WHERE emp_id= ?", null, array($post['emp_id']), array('text', 'text')
        );
        if ($is_employment) {
            $sth = $this->db->prepare(
                "
                UPDATE duty_shift_staff_employment SET
                    other_post_flg= :other_post_flg,
                    duty_mon_day_flg = :duty_mon_day_flg,
                    duty_tue_day_flg= :duty_tue_day_flg,
                    duty_wed_day_flg= :duty_wed_day_flg,
                    duty_thurs_day_flg= :duty_thurs_day_flg,
                    duty_fri_day_flg= :duty_fri_day_flg,
                    duty_sat_day_flg= :duty_sat_day_flg,
                    duty_sun_day_flg= :duty_sun_day_flg,
                    duty_hol_day_flg= :duty_hol_day_flg,
                    night_duty_flg= :night_duty_flg,
                    night_staff_cnt= 0,
                    group_id= :group_id
                WHERE emp_id = :emp_id
                ", array('integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'text', 'text'), MDB2_PREPARE_MANIP
            );
        }
        else {
            $sth = $this->db->prepare(
                "
                INSERT INTO duty_shift_staff_employment (
                    group_id,
                    emp_id,
                    other_post_flg,
                    duty_mon_day_flg,
                    duty_tue_day_flg,
                    duty_wed_day_flg,
                    duty_thurs_day_flg,
                    duty_fri_day_flg,
                    duty_sat_day_flg,
                    duty_sun_day_flg,
                    duty_hol_day_flg,
                    night_duty_flg,
                    night_staff_cnt
                ) VALUES (
                    :group_id,
                    :emp_id,
                    :other_post_flg,
                    :duty_mon_day_flg,
                    :duty_tue_day_flg,
                    :duty_wed_day_flg,
                    :duty_thurs_day_flg,
                    :duty_fri_day_flg,
                    :duty_sat_day_flg,
                    :duty_sun_day_flg,
                    :duty_hol_day_flg,
                    :night_duty_flg,
                    '0'
               )
               ", array('text', 'text', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer'), MDB2_PREPARE_MANIP
            );
        }
        $res = $sth->execute($post);
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            cmx_log('ROLLBACK: Cmx_Shift_Staff: employee_update: INSERT/UPDATE duty_shift_staff_employment: ' . $res->getDebugInfo());
            return false;
        }
        $sth->free();

        // empcond
        $is_empccond = $this->db->extended->getOne(
            "SELECT COUNT(*) FROM empcond WHERE emp_id= ?", null, array($post['emp_id']), array('text')
        );
        if ($is_empccond) {
            $sth = $this->db->prepare(
                "UPDATE empcond SET duty_form = :duty_form WHERE emp_id = :emp_id", array('integer', 'text'), MDB2_PREPARE_MANIP
            );
        }
        else {
            $sth = $this->db->prepare(
                "INSERT INTO empcond (duty_form, wage, emp_id) VALUES (:duty_form, 1, :emp_id)", array('integer', 'text'), MDB2_PREPARE_MANIP
            );
        }
        $res = $sth->execute($post);
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            cmx_log('ROLLBACK: Cmx_Shift_Staff: employee_update: UPDATE empcond: ' . $res->getDebugInfo());
            return false;
        }
        $sth->free();

        // duty_shift_staff_employment_atdptn
        $atdptn = new Cmx_Shift_StaffPattern();
        $res = $atdptn->update($post);
        if (!$res) {
            $this->db->failNestedTransaction();
            cmx_log('ROLLBACK: Cmx_Shift_Staff: employee_update: Cmx_Shift_StaffPattern->update(): ' . $res->getDebugInfo());
            return false;
        }

        // duty_shift_staff_employment_holiday
        $holiday = new Cmx_Shift_StaffHoliday();
        $res = $holiday->update($post);
        if (!$res) {
            $this->db->failNestedTransaction();
            cmx_log('ROLLBACK: Cmx_Shift_Staff: employee_update: Cmx_Shift_StaffHoliday->update(): ' . $res->getDebugInfo());
            return false;
        }

        // duty_shift_staff_employment_shorter
        $shorter = new Cmx_Shift_StaffShorter();
        $res = $shorter->update($post);
        if (!$res) {
            $this->db->failNestedTransaction();
            cmx_log('ROLLBACK: Cmx_Shift_Staff: employee_update: Cmx_Shift_StaffShorter->update(): ' . $res->getDebugInfo());
            return false;
        }

        // commit
        $this->db->completeNestedTransaction();
        return true;
    }

    /**
     * 当月職員リストの更新
     * @param type $post
     * @return boolean
     */
    function plan_staff_update($post)
    {

        $staff = array();
        $team = array();
        $time = array();
        $emp_ids = array();
        $cnt = 1;
        foreach ($post['order'] as $emp_id) {
            $staff[] = array(
                'group_id' => $post['group_id'],
                'emp_id' => $emp_id,
                'duty_yyyy' => $post['y'],
                'duty_mm' => $post['m'],
                'no' => $cnt++,
                'main_group_id' => $post['main_group_id'][$emp_id] ? $post['main_group_id'][$emp_id] : $post['group_id'],
            );
            $team[] = array(
                'group_id' => $post['group_id'],
                'emp_id' => $emp_id,
                'duty_yyyy' => $post['y'],
                'duty_mm' => $post['m'],
                'team_id' => $post['team_id'][$emp_id],
            );
            $time[] = array(
                'emp_id' => $emp_id,
                'duty_yyyy' => $post['y'],
                'duty_mm' => $post['m'],
                'adjustment' => $post['adjustment'][$emp_id] * 60,
            );
            $emp_ids[] = sprintf("'%s'", $emp_id);
        }

        $this->db->beginNestedTransaction();

        // DELETE duty_shift_plan_staff
        $sth = $this->db->prepare("DELETE FROM duty_shift_plan_staff WHERE group_id = ? AND duty_yyyy = ? AND duty_mm = ?", array('text', 'integer', 'integer'), MDB2_PREPARE_MANIP);
        $res = $sth->execute(array($post['group_id'], $post['y'], $post['m']));
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_Staff: plan_staff_update: DELETE FROM duty_shift_plan_staff: ' . $res->getDebugInfo());
            return false;
        }
        $sth->free();

        // DELETE duty_shift_plan_team
        $sth = $this->db->prepare("DELETE FROM duty_shift_plan_team WHERE group_id = ? AND duty_yyyy = ? AND duty_mm = ?", array('text', 'integer', 'integer'), MDB2_PREPARE_MANIP);
        $res = $sth->execute(array($post['group_id'], $post['y'], $post['m']));
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_Staff: plan_staff_update: DELETE FROM duty_shift_plan_team: ' . $res->getDebugInfo());
            return false;
        }
        $sth->free();

        // DELETE duty_shift_plan_staff_time
        $emp_in = implode(',', $emp_ids);
        $sth = $this->db->prepare("DELETE FROM duty_shift_plan_staff_time WHERE duty_yyyy = ? AND duty_mm = ? AND emp_id IN ($emp_in)", array('integer', 'integer'), MDB2_PREPARE_MANIP);
        $res = $sth->execute(array($post['y'], $post['m']));
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_Staff: plan_staff_update: DELETE FROM duty_shift_plan_staff_time: ' . $res->getDebugInfo());
            return false;
        }
        $sth->free();

        // INSERT duty_shift_plan_staff
        $sth = $this->db->prepare('
           INSERT INTO duty_shift_plan_staff
           (group_id, emp_id, duty_yyyy, duty_mm, no, main_group_id)
           VALUES (:group_id, :emp_id, :duty_yyyy, :duty_mm, :no, :main_group_id)
           ', array('text', 'text', 'integer', 'integer', 'integer', 'text'), MDB2_PREPARE_MANIP);
        $res = $this->db->extended->executeMultiple($sth, $staff);
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_Staff: plan_staff_update: INSERT INTO duty_shift_plan_staff: ' . $res->getDebugInfo());
            return false;
        }
        $sth->free();

        // INSERT duty_shift_plan_team
        $sth = $this->db->prepare('
           INSERT INTO duty_shift_plan_team
           (group_id, emp_id, duty_yyyy, duty_mm, team_id)
           VALUES (:group_id, :emp_id, :duty_yyyy, :duty_mm, :team_id)
           ', array('text', 'text', 'integer', 'integer', 'integer'), MDB2_PREPARE_MANIP);
        $res = $this->db->extended->executeMultiple($sth, $team);
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_Staff: plan_staff_update: INSERT INTO duty_shift_plan_team: ' . $res->getDebugInfo());
            return false;
        }
        $sth->free();

        // INSERT duty_shift_plan_staff_time
        $sth = $this->db->prepare("
           INSERT INTO duty_shift_plan_staff_time
           (emp_id, duty_yyyy, duty_mm, carryover, adjustment)
           VALUES (:emp_id, :duty_yyyy, :duty_mm, '0', :adjustment)
           ", array('text', 'integer', 'integer', 'text'), MDB2_PREPARE_MANIP);
        $res = $this->db->extended->executeMultiple($sth, $time);
        if (PEAR::isError($res)) {
            $this->db->rollback();
            cmx_log('ROLLBACK: Cmx_Shift_Staff: plan_staff_update: INSERT INTO duty_shift_plan_staff_time: ' . $res->getDebugInfo());
            return false;
        }
        $sth->free();

        // 繰越時間再計算
        $staff = new Cmx_Shift_Staff();
        foreach ($time as $row) {
            $staff->_plan_select($row['emp_id'], $post['y'], $post['m']);
            $staff->set_carryover($post['y'], $post['m']);
        }

        // commit
        $this->db->commit();
        return true;
    }

    /**
     * 繰越時間
     * @return type
     */
    function carryover()
    {
        return sprintf('%.2f', $this->_row['carryover'] / 60);
    }

    function carryover_num()
    {
        return $this->_row['carryover'];
    }

    /**
     * 精算時間
     * @return type
     */
    function adjustment()
    {
        return sprintf('%.2f', $this->_row['adjustment'] / 60);
    }

    function adjustment_min()
    {
        return $this->_row['adjustment'];
    }

    /**
     * 前月繰越
     * (繰越時間) - (精算時間)
     * @return type
     */
    function last_carryover()
    {
        return sprintf('%.2f', ($this->_row['carryover'] - $this->_row['adjustment']) / 60);
    }

    function last_carryover_min()
    {
        return ($this->_row['carryover'] - $this->_row['adjustment']);
    }

    /**
     * 繰越時間をセットする
     *   現在のクールの翌クールまでセット
     *   それより先のクールには繰り越しません
     *
     * @param int $start_year  再集計開始年
     * @param int $start_cours 再集計開始月(クール)
     * @return boolean
     */
    function set_carryover($start_year = null, $start_cours = null)
    {
        // 所属部署
        if (!$this->_row['group_id']) {
            return false;
        }
        $ward = new Cmx_Shift_Ward($this->_row['group_id']);
        $sdate = $ward->shift_date();

        // 28日単位部署で無ければ計算不要
        if ($ward->month_type() !== '2') {
            return false;
        }

        // 翌々クール
        list($this_year, $this_cours) = $sdate->cours(date('Y-m-d'));
        list($last_year, $last_cours) = $sdate->cours_add($this_year, $this_cours, 2);
        $last_yc = sprintf('%04d%02d', $last_year, $last_cours);

        // いつから？
        if (!$start_year and ! $start_cours) {
            list($start_year, $start_cours) = $sdate->cours(date('Y-m-d'));
        }
        else if ($start_year and ! $start_cours) {
            list($start_year, $start_cours) = $sdate->cours(preg_replace('/(\d\d\d\d)(\d\d)(\d\d)/', '$1-$2-$3', $start_year));
        }
        else {
            list($start_year, $start_cours) = $sdate->cours_add($start_year, $start_cours, -1);
        }
        $start_date = $sdate->start_ymd($start_year, $start_cours);

        // いつまで？
        $end_date = $sdate->end_ymd($this_year, $this_cours);

        // 実績データ
        $sth = $this->db->prepare("
            SELECT
                c.date,
                c.type,
                CASE WHEN r.pattern='0' OR r.pattern='' OR r.pattern IS NULL THEN r2.pattern        ELSE r.pattern        END AS pattern,
                CASE WHEN r.pattern='0' OR r.pattern='' OR r.pattern IS NULL THEN o2.workminutes    ELSE o.workminutes    END AS workminutes,
                CASE WHEN r.pattern='0' OR r.pattern='' OR r.pattern IS NULL THEN rm2.day_count     ELSE rm.day_count     END AS day_count,
                CASE WHEN r.pattern='0' OR r.pattern='' OR r.pattern IS NULL THEN rm2.kind_flag     ELSE rm.kind_flag     END AS kind_flag,
                CASE WHEN r.pattern='0' OR r.pattern='' OR r.pattern IS NULL THEN rm2.holiday_count ELSE rm.holiday_count END AS holiday_count,
                CASE WHEN p.reason=0 OR p.reason IS NULL THEN '0'                ELSE '1'               END AS reason_p,
                CASE WHEN p.reason=0 OR p.reason IS NULL THEN rmp2.day_count     ELSE rmp.day_count     END AS day_count_p,
                CASE WHEN p.reason=0 OR p.reason IS NULL THEN rmp2.kind_flag     ELSE rmp.kind_flag     END AS kind_flag_p,
                CASE WHEN p.reason=0 OR p.reason IS NULL THEN rmp2.holiday_count ELSE rmp.holiday_count END AS holiday_count_p
            FROM calendar c
            LEFT JOIN atdbkrslt r ON (c.date=r.date AND r.emp_id = :emp_id)
            LEFT JOIN atdptn p ON (CAST(r.pattern AS int)=p.atdptn_id AND r.tmcd_group_id=p.group_id)
            LEFT JOIN atdbk_reason_mst rm ON (to_number('0'||r.reason,'9999')=CAST(rm.reason_id AS int) AND rm.display_flag)
            LEFT JOIN atdbk_reason_mst rmp ON (to_number('0'||p.reason,'9999')=CAST(rmp.reason_id AS int) AND rmp.display_flag)
            LEFT JOIN officehours o on (r.tmcd_group_id=o.tmcd_group_id AND r.pattern=o.pattern AND c.type=o.officehours_id)
            LEFT JOIN atdbk r2 ON (c.date=r2.date AND r2.emp_id = :emp_id)
            LEFT JOIN atdptn p2 ON (CAST(r2.pattern AS int)=p2.atdptn_id AND r2.tmcd_group_id=p2.group_id)
            LEFT JOIN atdbk_reason_mst rm2 ON (to_number('0'||r2.reason,'9999')=CAST(rm2.reason_id AS int) AND rm2.display_flag)
            LEFT JOIN atdbk_reason_mst rmp2 ON (to_number('0'||p2.reason,'9999')=CAST(rmp2.reason_id AS int) AND rmp2.display_flag)
            LEFT JOIN officehours o2 on (r2.tmcd_group_id=o2.tmcd_group_id AND r2.pattern=o2.pattern AND c.type=o2.officehours_id)
            WHERE c.date BETWEEN :start_date AND :end_date
        ");
        $res = $sth->execute(array(
            'emp_id' => $this->emp_id(),
            'start_date' => $start_date,
            'end_date' => $end_date,
        ));
        $sth->free();

        // 計算
        $carryover = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            if (!$row['pattern'] or $row['pattern'] === '0') {
                continue;
            }

            list($y, $c) = $sdate->cours(preg_replace("/(\d\d\d\d)(\d\d)(\d\d)/", "$1-$2-$3", $row["date"]));
            $base = 0;
            switch ($row['type']) {
                case '8': //買取日
                    $diff = 0;
                    break;

                case '3': //日祝
                    $diff = $row['workminutes'];
                    break;

                case '2': //土曜
                    // 稼働時間 7.5時間
                    if ($row['pattern'] !== '10') {
                        $base += 210;
                    }
                    else if ($row['kind_flag'] === "1") {
                        // 土曜の有給の要稼働時間は 3.5h
                        $base += 210;
                    }
                    else {
                        $base += (float)$row['holiday_count'] * 210;
                    }

                    // 週休
                    if ($row['kind_flag'] === "2") {
                        $base -= (210 * (float)$row['holiday_count']);
                    }
                    // 半週休
                    else if ($row['kind_flag_p'] === "2") {
                        $base -= (210 * (float)$row['holiday_count_p']);
                    }

                    if ($row['pattern'] !== '10') {
                        $diff = $row['workminutes'] - $base;
                    }
                    else {
                        // 休暇の実働時間は 支給換算日数 * 3.5h
                        $diff = ((float)$row['day_count'] * 210) - $base;
                    }
                    break;

                default:  //平日
                    // 稼働時間 7.5時間
                    if ($row['pattern'] !== '10') {
                        $base += 450;
                    }
                    // 平日の週休2回の稼働時間は、450分
                    else if ($row['kind_flag'] === "2" and $row['holiday_count'] === '2.0') {
                        $base += 450;
                    }
                    else {
                        $base += (float)$row['holiday_count'] * 450;
                    }

                    // 週休
                    if ($row['kind_flag'] === "2") {
                        $base -= (210 * (float)$row['holiday_count']);
                    }
                    // 半週休
                    else if ($row['reason_p'] === "1" and $row['kind_flag_p'] === "2") {
                        $base -= (210 * (float)$row['holiday_count_p']);
                    }

                    if ($row['pattern'] !== '10') {
                        $diff = $row['workminutes'] - $base;
                    }
                    else {
                        $diff = ((float)$row['day_count'] * 450) - $base;
                    }
                    break;
            }
            $carryover[$y][$c] += $diff;
        }

        // SQL Prepare
        $sel_sth = $this->db->prepare("SELECT * FROM duty_shift_plan_staff_time WHERE emp_id = :emp_id AND duty_yyyy = :year AND duty_mm = :month");
        $ins_sth = $this->db->prepare("INSERT INTO duty_shift_plan_staff_time (emp_id,duty_yyyy,duty_mm,carryover,adjustment) VALUES (:emp_id,:year,:month,:carryover,'0')", array('text', 'integer', 'integer', 'text'), MDB2_PREPARE_MANIP);
        $upd_sth = $this->db->prepare("UPDATE duty_shift_plan_staff_time SET carryover = :carryover WHERE emp_id = :emp_id AND duty_yyyy = :year AND duty_mm = :month", array('text', 'text', 'integer', 'integer'), MDB2_PREPARE_MANIP);

        // 当月の繰越時間等を取得
        $res = $sel_sth->execute(array(
            'emp_id' => $this->emp_id(),
            'year' => $start_year,
            'month' => $start_cours,
        ));
        if ($res->numRows()) {
            $row = $res->fetchRow(MDB2_FETCHMODE_ASSOC);
            $kurikoshi = (int)$carryover[$start_year][$start_cours] + (int)$row['carryover'] - (int)$row['adjustment'];
        }
        else {
            $kurikoshi = $carryover[$start_year][$start_cours];
        }
        list($this_year, $this_cours) = $sdate->cours_add($start_year, $start_cours, 1);
        $this_yc = sprintf('%04d%02d', $this_year, $this_cours);

        // 翌月の繰越時間にセット
        $this->db->beginNestedTransaction();
        while ($this_yc <= $last_yc) {
            $data = array(
                'emp_id' => $this->emp_id(),
                'year' => $this_year,
                'month' => $this_cours,
                'carryover' => $kurikoshi,
            );
            $res = $sel_sth->execute($data);
            if ($res->numRows()) {
                // UPDATE
                $upd_res = $upd_sth->execute($data);
                if (PEAR::isError($upd_res)) {
                    $this->db->rollback();
                    cmx_log('ROLLBACK: Cmx_Shift_Staff: set_carryover: UPDATE duty_shift_plan_staff_time: ' . $res->getDebugInfo());
                    return false;
                }
                $row = $res->fetchRow(MDB2_FETCHMODE_ASSOC);
                $kurikoshi = (int)$carryover[$this_year][$this_cours] + (int)$kurikoshi - (int)$row['adjustment'];
            }
            else {
                // INSERT
                $ins_res = $ins_sth->execute($data);
                if (PEAR::isError($ins_res)) {
                    $this->db->rollback();
                    cmx_log('ROLLBACK: Cmx_Shift_Staff: set_carryover: INSERT INTO duty_shift_plan_staff_time: ' . $res->getDebugInfo());
                    return false;
                }
                $kurikoshi = $carryover[$this_year][$this_cours];
            }
            list($this_year, $this_cours) = $sdate->cours_add($this_year, $this_cours, 1);
            $this_yc = sprintf('%04d%02d', $this_year, $this_cours);
        }

        // commit
        $this->db->commit();
        return true;
    }

    /**
     * 前の職員
     * 該当年月の表示順が1つ前の職員IDを返す
     *
     * @param int $year  該当年
     * @param int $month 該当月
     * @return str 職員ID
     */
    function prev($year, $month)
    {
        // plan_staffの存在チェック
        $is_plan_staff = $this->is_plan_staff($this->group_id(), $year, $month);

        if ($is_plan_staff) {
            $sql = "
                SELECT b.emp_id FROM duty_shift_plan_staff a JOIN duty_shift_plan_staff b USING (group_id,duty_yyyy,duty_mm)
                WHERE a.group_id = ? AND a.emp_id = ? AND a.duty_yyyy = ? AND a.duty_mm = ? AND a.no>b.no ORDER BY b.no DESC LIMIT 1";
            $prev = $this->db->extended->getRow($sql, null, array($this->group_id(), $this->emp_id(), $year, $month), array('text', 'text', 'integer', 'integer'), MDB2_FETCHMODE_ASSOC);
            return $prev['emp_id'] ? $prev['emp_id'] : null;
        }
        $no = $this->order_no() - 1;
        $sql = "SELECT emp_id FROM duty_shift_staff WHERE group_id = ? AND no <= ? ORDER BY no DESC LIMIT 1";
        $prev = $this->db->extended->getRow($sql, null, array($this->group_id(), $no), array('text', 'integer'), MDB2_FETCHMODE_ASSOC);
        return $prev['emp_id'] ? $prev['emp_id'] : null;
    }

    /**
     * 次の職員
     * 該当年月の表示順が1つ次の職員IDを返す
     *
     * @param int $year  該当年
     * @param int $month 該当月
     * @return str 職員ID
     */
    function next($year, $month)
    {
        // plan_staffの存在チェック
        $is_plan_staff = $this->is_plan_staff($this->group_id(), $year, $month);

        if ($is_plan_staff) {
            $sql = "
                SELECT b.emp_id FROM duty_shift_plan_staff a JOIN duty_shift_plan_staff b USING (group_id,duty_yyyy,duty_mm)
                WHERE a.group_id = ? AND a.emp_id = ? AND a.duty_yyyy = ? AND a.duty_mm = ? AND a.no<b.no ORDER BY b.no LIMIT 1";
            $next = $this->db->extended->getRow($sql, null, array($this->group_id(), $this->emp_id(), $year, $month), array('text', 'text', 'integer', 'integer'), MDB2_FETCHMODE_ASSOC);
            return $next['emp_id'] ? $next['emp_id'] : null;
        }
        $no = $this->order_no() + 1;
        $sql = "SELECT emp_id FROM duty_shift_staff WHERE group_id = ? AND no >= ? ORDER BY no LIMIT 1";
        $next = $this->db->extended->getRow($sql, null, array($this->group_id(), $no), array('text', 'integer'), MDB2_FETCHMODE_ASSOC);
        return $next['emp_id'] ? $next['emp_id'] : null;
    }

    /**
     * 時間外帳票用リスト
     * 大阪警察病院対応
     *
     * @param type $year
     * @param type $month
     */
    function overtime_lists($year, $month)
    {
        // 残業
        $ovtm = new Cmx_Timecard_Overtime();

        // 開始日,終了日
        $start_ymd = sprintf('%04d%02d01', $year, $month);
        $end_ymd = sprintf('%04d%02d%02d', $year, $month, cal_days_in_month(CAL_GREGORIAN, $month, $year));

        $sth = $this->db->prepare("
            SELECT
                a.*,
                d.font_name,
                d.sfc_code,
                p.atdptn_nm,
                c.type,
                COALESCE(or1.reason,o.ovtm_reason) AS ovtm_reason1,
                COALESCE(or2.reason,o.ovtm_reason2) AS ovtm_reason2,
                h.workminutes,
                h.officehours2_start AS start_time_o,
                h.officehours2_end AS end_time_o
            FROM atdbkrslt a
            LEFT JOIN calendar c USING (date)
            LEFT JOIN atdbkrslt_ovtmrsn o USING (emp_id, date)
            LEFT JOIN duty_shift_pattern d ON (d.pattern_id=a.tmcd_group_id AND d.atdptn_ptn_id=a.pattern AND (CAST(d.reason AS INT)=to_number('0'||a.reason,'9999') OR a.reason IS NULL OR a.reason=''))
            LEFT JOIN atdptn p ON (p.atdptn_id=a.pattern AND p.group_id=a.tmcd_group_id)
            LEFT JOIN ovtmrsn or1 ON (o.ovtm_reason_id=or1.reason_id)
            LEFT JOIN ovtmrsn or2 ON (o.ovtm_reason_id2=or2.reason_id)
            LEFT JOIN officehours h ON (((c.type!=8 and c.type=h.officehours_id) OR (c.type=8 and h.officehours_id=3)) and h.tmcd_group_id=a.tmcd_group_id and h.pattern=a.pattern)
            WHERE (
                c.type=8 OR
                night_duty=1 OR
                d.sfc_code IN ('O4','O6','O7','O8','P9','PA') OR
                over_start_time != '' OR over_start_time IS NOT NULL OR
                over_start_time2 != '' OR over_start_time2 IS NOT NULL
                )
              AND emp_id = :emp_id
              AND date BETWEEN :start_date AND :end_date
            ORDER BY date
        ");
        $res = $sth->execute(array(
            'emp_id' => $this->emp_id(),
            'start_date' => $start_ymd,
            'end_date' => $end_ymd,
        ));
        $sth->free();

        $list = array();
        $total = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $mstdata = array(
                'day' => (int)substr($row['date'], -2),
                'sign' => $row['font_name'],
            );

            // 夜勤回数(夜勤入りの回数)
            if ($row['sfc_code'] === 'O6') {
                $total['night'] ++;
            }

            // 宿直, 日祝宿直, 当直フラグ
            $stay = false;
            if ($row['sfc_code'] === 'O7' or $row['sfc_code'] === 'PA' or $row['night_duty'] === '1') {
                switch ($row['type']) {
                    case '2':
                        $data = array(
                            'start_time' => '12:00',
                            'end_time' => '08:30',
                            'duty1' => '○',
                            'duty2' => '○',
                            'reason' => '宿直（土）'
                        );
                        break;
                    case '1':
                        $data = array(
                            'start_time' => '17:00',
                            'end_time' => '08:30',
                            'duty1' => '○',
                            'reason' => '宿直'
                        );
                        break;
                    case '3':
                    case '8' :
                        $data = array(
                            'start_time' => '17:00',
                            'end_time' => '08:30',
                            'duty1' => '○',
                            'reason' => '宿直（日）'
                        );
                        break;
                }
                $list[] = array_merge($mstdata, $data);
                $stay = true;
            }

            // 宿直Staff
            if ($row['sfc_code'] === 'O8') {
                switch ($row['type']) {
                    case '2':
                        $data = array(
                            'start_time' => '12:00',
                            'end_time' => '08:30',
                            'duty1' => '○',
                            'duty2' => '○',
                            'reason' => '宿直（土）'
                        );
                        $list[] = array_merge($mstdata, $data);
                        $stay = true;
                        break;
                    case '3' :
                    case '8' :
                        $data = array(
                            'start_time' => '08:30',
                            'end_time' => '08:30',
                            'duty1' => '○',
                            'duty3' => '○',
                            'reason' => '宿直（日）'
                        );
                        $list[] = array_merge($mstdata, $data);
                        $stay = true;
                        break;
                    default :
                        break;
                }
            }

            // 日直
            if ($row['sfc_code'] === 'P9') {
                $data = array(
                    'start_time' => '08:30',
                    'end_time' => '17:00',
                    'duty3' => '○',
                );
                switch ($row['type']) {
                    case '2':
                        $data['reason'] = '日直（土）';
                        break;
                    case '3' :
                    case '8' :
                        $data['reason'] = '日直（日）';
                        break;
                    default :
                        $data['reason'] = '日直';
                        break;
                }
                $list[] = array_merge($mstdata, $data);
                $stay = true;
            }

            // 待機
            if ($row['sfc_code'] === 'O4') {
                switch ($row['type']) {
                    case '2':
                        $data = array(
                            'start_time' => '12:00',
                            'end_time' => '14:00',
                            'reason' => '小児救急待機（土）'
                        );
                        $list[] = array_merge($mstdata, $data);
                        break;
                    case '1' :
                        $data = array(
                            'start_time' => '17:00',
                            'end_time' => '20:00',
                            'reason' => '小児救急待機（平日）'
                        );
                        $list[] = array_merge($mstdata, $data);
                        break;
                }
            }

            // 買取日
            if ($row['type'] === '8' and $stay === false and $row['pattern'] !== '10') {
                // 開始時間(なければ、時間帯で設定されてる時間を使用する)
                if ($row['start_time']) {
                    $start_time = preg_replace('/^(\d\d)(\d\d)/', '$1:$2', $row['start_time']);
                }
                else {
                    $start_time = $row['start_time_o'];
                }
                // 終了時間(なければ、時間帯で設定されてる時間を使用する)
                if ($row['end_time']) {
                    $end_time = preg_replace('/^(\d\d)(\d\d)/', '$1:$2', $row['end_time']);
                }
                else {
                    $end_time = $row['end_time_o'];
                }

                $data = array(
                    'start_time' => $start_time,
                    'end_time' => $end_time,
                    'total' => sprintf('%.2f', $row['workminutes'] / 60 + 0.004),
                    'holiday' => sprintf('%.2f', $row['workminutes'] / 60 + 0.004),
                    'reason' => '祝日' . $row['atdptn_nm'],
                );
                $list[] = array_merge($mstdata, $data);
                $total['total'] += (float)$data['total'];
                $total['holiday'] += (float)$data['total'];
            }

            // 残業1
            if ($row['over_start_time']) {
                $overtime = $ovtm->overtime($row['over_start_time'], $row['over_start_next_day_flag'], $row['over_end_time'], $row['over_end_next_day_flag']);
                $over025 = $ovtm->over025($row['over_start_time'], $row['over_start_next_day_flag'], $row['over_end_time'], $row['over_end_next_day_flag']);
                // 実働7.5時間
                $over100 = '';
                $over125 = '';
                $over100 = $ovtm->over100($row, $row['over_start_time'], $row['over_start_next_day_flag'], $row['over_end_time'], $row['over_end_next_day_flag']);
                $over125 = $ovtm->over125($row, $row['over_start_time'], $row['over_start_next_day_flag'], $row['over_end_time'], $row['over_end_next_day_flag']);

                // 買取日 or OFF
                // の場合は、135/100
                if ($row['type'] === '8' or $row['sfc_code'] === 'OU') {
                    $data = array(
                        'start_time' => preg_replace('/^(\d\d)(\d\d)/', '$1:$2', $row['over_start_time']),
                        'end_time' => preg_replace('/^(\d\d)(\d\d)/', '$1:$2', $row['over_end_time']),
                        'total' => sprintf('%.2f', $overtime / 60 + 0.004),
                        'holiday' => sprintf('%.2f', $overtime / 60 + 0.004),
                        'reason' => $row['ovtm_reason1'],
                    );
                    $list[] = array_merge($mstdata, $data);
                    $total['total'] += (float)$data['total'];
                    $total['holiday'] += (float)$data['total'];
                }
                else {
                    $data = array(
                        'start_time' => preg_replace('/^(\d\d)(\d\d)/', '$1:$2', $row['over_start_time']),
                        'end_time' => preg_replace('/^(\d\d)(\d\d)/', '$1:$2', $row['over_end_time']),
                        'total' => sprintf('%.2f', $overtime / 60 + 0.004),
                        'over100' => $over100 ? sprintf('%.2f', $over100 / 60 + 0.004) : '',
                        'over125' => $over125 ? sprintf('%.2f', $over125 / 60 + 0.004) : '',
                        'over025' => $over025 ? sprintf('%.2f', $over025 / 60 + 0.004) : '',
                        'reason' => $row['ovtm_reason1'],
                    );
                    $list[] = array_merge($mstdata, $data);
                    $total['total'] += (float)$data['total'];
                    $total['over100'] += (float)$data['over100'];
                    $total['over125'] += (float)$data['over125'];
                    $total['over025'] += (float)$data['over025'];
                }
            }

            // 残業2
            if ($row['over_start_time2']) {
                $overtime = $ovtm->overtime($row['over_start_time2'], $row['over_start_next_day_flag2'], $row['over_end_time2'], $row['over_end_next_day_flag2']);
                $over025 = $ovtm->over025($row['over_start_time2'], $row['over_start_next_day_flag2'], $row['over_end_time2'], $row['over_end_next_day_flag2']);
                // 実働7.5時間
                $over100 = '';
                $over125 = '';
                $over100 = $ovtm->over100($row, $row['over_start_time2'], $row['over_start_next_day_flag2'], $row['over_end_time2'], $row['over_end_next_day_flag2']);
                $over125 = $ovtm->over125($row, $row['over_start_time2'], $row['over_start_next_day_flag2'], $row['over_end_time2'], $row['over_end_next_day_flag2']);

                // 買取日 or 実働時間ゼロ
                // の場合は、135/100
                if ($row['type'] === '8' or $row['workminutes'] === '0' or empty($row['workminutes'])) {
                    $data = array(
                        'start_time' => preg_replace('/^(\d\d)(\d\d)/', '$1:$2', $row['over_start_time2']),
                        'end_time' => preg_replace('/^(\d\d)(\d\d)/', '$1:$2', $row['over_end_time2']),
                        'total' => sprintf('%.2f', $overtime / 60 + 0.004),
                        'holiday' => sprintf('%.2f', $overtime / 60 + 0.004),
                        'reason' => $row['ovtm_reason2'],
                    );
                    $list[] = array_merge($mstdata, $data);
                    $total['total'] += (float)$data['total'];
                    $total['holiday'] += (float)$data['total'];
                }
                else {
                    $data = array(
                        'start_time' => preg_replace('/^(\d\d)(\d\d)/', '$1:$2', $row['over_start_time2']),
                        'end_time' => preg_replace('/^(\d\d)(\d\d)/', '$1:$2', $row['over_end_time2']),
                        'total' => sprintf('%.2f', $overtime / 60 + 0.004),
                        'over100' => $over100 ? sprintf('%.2f', $over100 / 60 + 0.004) : '',
                        'over125' => $over125 ? sprintf('%.2f', $over125 / 60 + 0.004) : '',
                        'over025' => $over025 ? sprintf('%.2f', $over025 / 60 + 0.004) : '',
                        'reason' => $row['ovtm_reason2'],
                    );
                    $list[] = array_merge($mstdata, $data);
                    $total['total'] += (float)$data['total'];
                    $total['over100'] += (float)$data['over100'];
                    $total['over125'] += (float)$data['over125'];
                    $total['over025'] += (float)$data['over025'];
                }
            }
        }

        // 精算時間
        $ward = new Cmx_Shift_Ward($this->_row['group_id']);
        $sdate = $ward->shift_date();
        list($cyear, $cmonth) = $sdate->months($end_ymd);
        $sth = $this->db->prepare("
            SELECT * FROM duty_shift_plan_staff_time
            WHERE emp_id = ? AND duty_yyyy = ? AND duty_mm = ?
        ");
        $res = $sth->execute(array($this->emp_id(), $cyear, $cmonth));
        $time = $res->fetchRow(MDB2_FETCHMODE_ASSOC);
        $sth->free();
        if (!empty($time['adjustment'])) {
            $list[] = array(
                'total' => sprintf('%.2f', $time['adjustment'] / 60 + 0.004),
                'reason' => '時間精算のため',
            );
            $total['total'] += (float)sprintf('%.2f', $time['adjustment'] / 60 + 0.004);
        }

        // 夜勤回数
        if ($total['night'] and $total['night'] > 0) {
            $list[] = array(
                'total' => '',
                'reason' => '',
            );
        }

        return array($list, $total);
    }

}
