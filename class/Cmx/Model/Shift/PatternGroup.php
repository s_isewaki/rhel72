<?php

require_once('Cmx.php');
require_once('Cmx/Core/Model.php');

/**
 * 勤務表：勤務パターングループクラス
 */
class Cmx_Shift_PatternGroup extends Model
{

    var $_row;

    // コンストラクタ
    function Cmx_Shift_PatternGroup($row)
    {
        parent::connectDB();

        if (is_array($row)) {
            $this->_init($row);
        }
        else if (!is_null($row)) {
            $this->_select($row);
        }
    }

    // _init
    function _init($row)
    {
        $this->_row = $row;
    }

    // _select
    function _select($group_id)
    {
        $sth = $this->db->prepare("SELECT * FROM wktmgrp WHERE group_id= ?");
        $res = $sth->execute($group_id);
        $this->_init($res->fetchRow(MDB2_FETCHMODE_ASSOC));
    }

    // get
    function group_id()
    {
        return $this->_getter(__FUNCTION__);
    }

    function group_name()
    {
        return $this->_getter(__FUNCTION__);
    }

}
