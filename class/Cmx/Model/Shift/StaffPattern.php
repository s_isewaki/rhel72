<?php

require_once('Cmx.php');
require_once('Cmx/Core/Model.php');

/**
 * 勤務表：部署スタッフ 出勤可能パターン
 */
class Cmx_Shift_StaffPattern extends Model
{

    var $_group_id;
    var $_emp_id;
    var $_list;

    /**
     * コンストラクタ
     *
     * @param type $group_id 部署ID
     * @param type $emp_id 職員ID
     */
    function Cmx_Shift_StaffPattern($group_id = null, $emp_id = null)
    {
        parent::connectDB();
        $this->_group_id = $group_id;
        $this->_emp_id = $emp_id;
        $this->_list = null;
    }

    /**
     * 出勤可能パターンリスト
     *
     * @return 出勤可能パターンリスト
     */
    function lists()
    {
        $sth = $this->db->prepare("
            SELECT * FROM duty_shift_staff_employment_atdptn WHERE group_id= :group_id AND emp_id = :emp_id
        ");
        $res = $sth->execute(array('group_id' => $this->_group_id, 'emp_id' => $this->_emp_id));

        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[$row['week_flg']]["p{$row['atdptn_ptn_id']}"] = $row['use_flg'];
        }
        $this->_list = $list;
        return $list;
    }

    /**
     * データ更新
     *
     * @param type $post POSTデータ
     * @return true: 成功 false:失敗
     */
    function update($post)
    {
        $this->db->beginNestedTransaction();

        $data = array();
        foreach ($post['week_ptn'] as $atdptn_ptn_id) {
            foreach (array(1, 2, 3, 4) as $week_flg) {
                $data[] = array(
                    'group_id' => $post['group_id'],
                    'emp_id' => $post['emp_id'],
                    'week_flg' => $week_flg,
                    'atdptn_ptn_id' => $atdptn_ptn_id,
                    'use_flg' => $post['week_flg'][$week_flg][$atdptn_ptn_id] === "1" ? 1 : 0,
                );
            }
        }

        // DELETE
        $sth = $this->db->prepare("DELETE FROM duty_shift_staff_employment_atdptn WHERE emp_id = ?", array('text'), MDB2_PREPARE_MANIP);
        $res = $sth->execute(array($post['emp_id']));
        if (PEAR::isError($res)) {
            $this->db->rollback();
            return false;
        }

        // INSERT
        $sth = $this->db->prepare('
           INSERT INTO duty_shift_staff_employment_atdptn
           (group_id, emp_id, week_flg, atdptn_ptn_id, use_flg)
           VALUES (:group_id, :emp_id, :week_flg, :atdptn_ptn_id, :use_flg)
           ', array('text', 'text', 'int', 'int', 'text'), MDB2_PREPARE_MANIP);
        $res = $this->db->extended->executeMultiple($sth, $data);
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            return false;
        }

        // commit
        $this->db->completeNestedTransaction();
        return true;
    }

    /**
     * 勤務可能パターンチェック
     * @return type
     */
    function is_pattern($week, $pattern)
    {
        if (is_null($this->_list)) {
            $this->lists();
        }
        if (!array_key_exists("p{$pattern}", $this->_list[$week])) {
            return "1";
        }
        return $this->_list[$week]["p{$pattern}"];
    }

}
