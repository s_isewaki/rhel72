<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'Cmx/Shift/Log.php';
require_once 'Cmx/Model/SystemConfig.php';

/*
 * 勤務表：勤務日クラス
 */

class Cmx_Shift_DutyDate extends Model
{

    var $_emp;
    var $_date;
    var $_datetime;
    var $_duty;
    var $_log;
    var $_cache_plan;
    var $_cache_rslt;

    /**
     * constractor
     * @param type $emp_id
     */
    function __construct($log_mode = 'door_open')
    {
        parent::connectDB();

        $this->_log = new Cmx_Shift_Log($log_mode);

        // 規定の記号マスタ
        $conf = new Cmx_SystemConfig();
        $this->default = unserialize($conf->get('shift.default.kigo'));

        // キャッシュ
        $this->_cache_emp = array();
        $this->_cache_plan = array();
        $this->_cache_rslt = array();

        // SQLキャッシュ
        $this->_plan_insert = array();
        $this->_plan_update = array();
        $this->_base_insert = array();
        $this->_base_update = array();
    }

    public function get_emp($emp_personal_id)
    {
        if ($this->_cache_emp) {
            if (!empty($this->_cache_emp[$emp_personal_id])) {
                return $this->_cache_emp[$emp_personal_id];
            }
            else {
                $this->log()->log('nothing emp_personal_id', $emp_personal_id);
                return false;
            }
        }

        $res = $this->db->query("
            SELECT
                emp_id,
                emp_personal_id,
                pattern_id,
                end_1y_date,
                end_3y_date,
                week
            FROM empmst
            LEFT JOIN duty_shift_staff USING (emp_id)
            LEFT JOIN duty_shift_group USING (group_id)
            LEFT JOIN duty_shift_staff_employment_shorter USING (emp_id)
            WHERE
                emp_id IS NOT NULL
        ");

        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $this->_cache_emp[$row['emp_personal_id']] = $row;
        }

        if (!empty($this->_cache_emp[$emp_personal_id])) {
            return $this->_cache_emp[$emp_personal_id];
        }
        else {
            $this->log()->log('nothing emp_personal_id', $emp_personal_id);
            return false;
        }
    }

    /**
     * セッション作成日時の取得
     * @param type $emp_personal_id
     * @return type
     */
    public function get_session($emp_personal_id)
    {
        $sql = "
            SELECT
                MAX(session_made)
            FROM session
            JOIN empmst USING (emp_id)
            WHERE
                emp_personal_id = ?
        ";

        list($session_made) = $this->db->extended->getCol($sql, null, array($emp_personal_id));
        return $session_made;
    }

    /**
     * 予定の入っていない職員リスト
     * @param type $date
     */
    private function noplan_employee($date)
    {
        $sql = "
            SELECT
                s.emp_id,
                emp_personal_id,
                g.pattern_id,
                end_1y_date,
                end_3y_date,
                week
            FROM duty_shift_staff s
            JOIN duty_shift_group g USING (group_id)
            JOIN empmst e USING (emp_id)
            JOIN authmst a USING (emp_id)
            LEFT JOIN duty_shift_staff_employment_shorter USING (emp_id)
            LEFT JOIN atdbk p ON e.emp_id=p.emp_id AND p.date= :date
            WHERE NOT emp_del_flg
              AND (emp_join='' OR emp_join IS NULL OR emp_join <= :date)
              AND (emp_retire='' OR emp_retire IS NULL OR emp_retire >= :date)
              AND (p.pattern IS NULL OR p.pattern='0')
        ";

        $sth = $this->db->prepare($sql);
        $res = $sth->execute(array('date' => $date));

        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[$row['emp_id']] = $row;
        }

        return $list;
    }

    /**
     * 明け予定で前日実績が入っている職員リスト
     * @param type $date
     */
    private function ake_employee($date)
    {
        $sql = "
            SELECT
                p.emp_id,
                e.emp_personal_id,
                p.tmcd_group_id AS pattern_id,
                end_1y_date,
                end_3y_date,
                week
            FROM atdptn
            JOIN atdbk p ON CAST(atdptn_id AS CHAR)=pattern AND group_id=tmcd_group_id
            JOIN atdbkrslt r ON r.emp_id=p.emp_id AND r.date=TO_CHAR(TO_DATE(p.date, 'YYYYMMDD')- INTERVAL '1 day', 'YYYYMMDD')
            JOIN empmst e on e.emp_id=p.emp_id
            LEFT JOIN duty_shift_staff_employment_shorter s ON s.emp_id=p.emp_id
            WHERE after_night_duty_flag=1
              AND p.date= :date
        ";

        $sth = $this->db->prepare($sql);
        $res = $sth->execute(array('date' => $date));

        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[$row['emp_id']] = $row;
        }

        return $list;
    }

    public function is_connect()
    {
        $dsn = CMX_DB_DSN;
        $dbh = MDB2::connect($dsn);
        if (PEAR::isError($dbh)) {
            return false;
        }
        return true;
    }

    /**
     * 日付/時刻の妥当性チェック
     * @param type $date
     * @return boolean
     */
    public function is_date($date)
    {
        $match = array();
        preg_match("/\A(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})\z/", $date, $match);

        // 日付チェック
        if (!checkdate($match[2], $match[3], $match[1])) {
            return false;
        }

        // 時刻チェック
        if ($match[4] < 0 || $match[4] > 23 || !is_numeric($match[4])) {
            return false;
        }
        if ($match[5] < 0 || $match[5] > 59 || !is_numeric($match[5])) {
            return false;
        }
        if ($match[6] < 0 || $match[6] > 59 || !is_numeric($match[6])) {
            return false;
        }

        return true;
    }

    /**
     * begin transaction
     */
    public function beginTransaction()
    {
        $this->db->beginTransaction();
    }

    /**
     * rollback
     */
    public function rollback()
    {
        $this->db->rollback();
    }

    /**
     * commit
     */
    public function commit()
    {
        $this->db->commit();
    }

    /**
     * log
     * @return type
     */
    public function log()
    {
        return $this->_log;
    }

    /**
     * 勤務予定の取得
     * @return type
     */
    private function fetchPlan()
    {
        if ($this->_cache_plan[$this->_date]) {
            return $this->_cache_plan[$this->_date][$this->_emp['emp_id']];
        }

        $sql = "
            SELECT
                pl.*,
                REPLACE(oh.officehours2_start, ':', '') AS start_time,
                REPLACE(oh.officehours2_end,   ':', '') AS end_time,
                REPLACE(oh.officehours4_start, ':', '') AS rest_start_time,
                REPLACE(oh.officehours4_end,   ':', '') AS rest_end_time
            FROM atdbk pl
            JOIN calendar c USING (date)
            LEFT JOIN officehours oh ON pl.tmcd_group_id=oh.tmcd_group_id AND pl.pattern=oh.pattern AND oh.officehours_id=(CASE WHEN c.type='2' THEN '2' WHEN c.type='3' THEN '3' ELSE '1' END)
            WHERE
                date = :date
        ";

        $sth = $this->db->prepare($sql);
        $res = $sth->execute(array('date' => $this->_date));

        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $this->_cache_plan[$this->_date][$row['emp_id']] = $row;
        }

        return $this->_cache_plan[$this->_date][$this->_emp['emp_id']];
    }

    /**
     * 勤務実績の取得
     * @return type
     */
    private function fetchResult()
    {
        if ($this->_cache_rslt[$this->_date]) {
            return $this->_cache_rslt[$this->_date][$this->_emp['emp_id']];
        }

        $sql = "
            SELECT
                *
            FROM
                atdbkrslt
            WHERE
                date = :date
        ";

        $sth = $this->db->prepare($sql);
        $res = $sth->execute(array('date' => $this->_date));

        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $this->_cache_rslt[$this->_date][$row['emp_id']] = $row;
        }

        return $this->_cache_rslt[$this->_date][$this->_emp['emp_id']];
    }

    /**
     * 出勤打刻処理
     * @param type $date
     */
    public function dakoku($emp_id, $date, $mode = 0)
    {
        if (empty($emp_id) || empty($date)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }

        $this->_emp = $this->get_emp($emp_id);
        if ($this->_emp === false) {
            return true;
        }

        // セッション作成時間の取得
        if ($mode !== 1) {
            $session_made = $this->get_session($emp_id);
            if ($session_made + 60 <= time()) {
                return true;
            }
        }

        $this->_datetime = $date;
        $this->_date = substr($date, 0, 8);

        // 予定実績の取得
        $this->_duty['plan'] = $this->fetchPlan();
        $this->_duty['result'] = $this->fetchResult();

        // 勤務予定が休暇なら終了(なにもしない)
        if ($this->isPlanHoliday()) {
            return true;
        }

        // 勤務実績がすでに入っていれば終了(なにもしない)
        if ($this->isResultExists()) {
            return true;
        }

        // 勤務予定が登録されている?
        if ($this->isPlanExists()) {
            // 勤務予定を勤務実績に入れる
            return $this->copyPlan();
        }

        // 勤務予定が登録されていない?
        else {
            // デフォルト予定を勤務実績に入れる
            return $this->copyBase();
        }
    }

    /**
     * 日祝処理
     * @param type $date
     */
    public function sunday($date)
    {
        $this->_datetime = $date . '000000';
        $this->_date = $date;

        // 日祝で無ければ終了
        $dow = $this->get_dow($this->_date);
        if ($dow !== 0) {
            return false;
        }

        // 予定の入っていない職員
        $noplan_emp = $this->noplan_employee($this->_date);

        // 規定→実績
        foreach ($noplan_emp as $emp) {
            $this->_emp = $emp;

            // 実績の取得
            $this->_duty['result'] = $this->fetchResult();

            // 勤務実績がすでに入っていれば終了(なにもしない)
            if ($this->isResultExists()) {
                continue;
            }

            $this->copyBase();
        }

        return true;
    }

    /**
     * 明け処理
     * @param type $date
     */
    public function ake($date)
    {
        $this->_datetime = $date . '000000';
        $this->_date = $date;

        // 明け予定で前日実績が入っている職員
        $ake_emp = $this->ake_employee($this->_date);

        // 予定→実績
        foreach ($ake_emp as $emp) {
            $this->_emp = $emp;

            // 予定実績の取得
            $this->_duty['plan'] = $this->fetchPlan();
            $this->_duty['result'] = $this->fetchResult();

            // 勤務実績がすでに入っていれば終了(なにもしない)
            if ($this->isResultExists()) {
                continue;
            }

            $this->copyPlan();
        }

        return true;
    }

    /**
     * 勤務予定が休暇かどうか判定
     * @return boolean
     */
    private function isPlanHoliday()
    {
        if ($this->_duty['plan']['pattern'] === '10') {
            return true;
        }
        return false;
    }

    /**
     * 勤務実績入力済みかどうか判定
     * @return boolean
     */
    private function isResultExists()
    {
        if (!empty($this->_duty['result']['pattern'])) {
            return true;
        }
        return false;
    }

    /**
     * 勤務予定入力済みかどうか判定
     * @return boolean
     */
    private function isPlanExists()
    {
        if (!empty($this->_duty['plan']['pattern'])) {
            return true;
        }
        return false;
    }

    /**
     * 勤務予定→勤務実績コピー
     */
    private function copyPlan()
    {
        // 時短
        list($base_shorter, $real_shorter) = $this->get_shorter_minutes();

        $data = array(
            'emp_personal_id' => $this->_emp['emp_personal_id'],
            'emp_id' => $this->_emp['emp_id'],
            'date' => $this->_date,
            'pattern' => $this->_duty['plan']['pattern'],
            'reason' => $this->_duty['plan']['reason'],
            'night_duty' => $this->_duty['plan']['night_duty'],
            'allow_id' => $this->_duty['plan']['allow_id'],
            'tmcd_group_id' => $this->_duty['plan']['tmcd_group_id'],
            'start_time' => $this->_duty['plan']['start_time'],
            'end_time' => $this->_duty['plan']['end_time'],
            'next_day_flag' => ($this->_duty['plan']['end_time'] <= $this->_duty['plan']['start_time'] ? 1 : 0),
            'rest_start_time' => $this->_duty['plan']['rest_start_time'],
            'rest_end_time' => $this->_duty['plan']['rest_end_time'],
            'base_shorter_minutes' => $base_shorter,
            'real_shorter_minutes' => $real_shorter,
            'start_btn_time' => substr($this->_datetime, 8, 4),
            'update_emp_id' => '000000000000',
            'update_time' => date('YmdHis'),
        );

        if ($this->_duty['result']['pattern'] === '0') {
            $this->_plan_update[] = $data;
        }
        else {
            $this->_plan_insert[] = $data;
        }

        return true;
    }

    /**
     * 規程の記号→勤務実績コピー
     */
    private function copyBase()
    {
        // 曜日type
        $dow = $this->get_dow($this->_date);

        // 規定の記号
        $kigo = $this->default[$this->_emp['pattern_id']][$dow];
        if (empty($kigo)) {
            return true;
        }

        // 時短
        list($base_shorter, $real_shorter) = $this->get_shorter_minutes();

        //勤務記号
        if (substr($kigo, 0, 2) !== '10') {
            $ptn = $this->get_pattern($this->_emp['pattern_id'], (int)substr($kigo, 0, 2));

            $data = array(
                'emp_personal_id' => $this->_emp['emp_personal_id'],
                'emp_id' => $this->_emp['emp_id'],
                'date' => $this->_date,
                'pattern' => (int)substr($kigo, 0, 2),
                'reason' => '',
                'tmcd_group_id' => $this->_emp['pattern_id'],
                'start_time' => $ptn['start_time'],
                'end_time' => $ptn['end_time'],
                'next_day_flag' => ($ptn['start_time'] && $ptn['end_time'] <= $ptn['start_time'] ? 1 : 0),
                'rest_start_time' => $ptn['rest_start_time'],
                'rest_end_time' => $ptn['rest_end_time'],
                'start_btn_time' => substr($this->_datetime, 8, 4),
                'base_shorter_minutes' => $base_shorter,
                'real_shorter_minutes' => $real_shorter,
                'update_emp_id' => '000000000000',
                'update_time' => date('YmdHis'),
            );
        }

        // 休暇記号
        else {
            $data = array(
                'emp_personal_id' => $this->_emp['emp_personal_id'],
                'emp_id' => $this->_emp['emp_id'],
                'date' => $this->_date,
                'pattern' => (int)substr($kigo, 0, 2),
                'reason' => (int)substr($kigo, 2, 2),
                'tmcd_group_id' => $this->_emp['pattern_id'],
                'start_time' => '',
                'end_time' => '',
                'next_day_flag' => 0,
                'rest_start_time' => '',
                'rest_end_time' => '',
                'start_btn_time' => substr($this->_datetime, 8, 4),
                'base_shorter_minutes' => $base_shorter,
                'real_shorter_minutes' => $real_shorter,
                'update_emp_id' => '000000000000',
                'update_time' => date('YmdHis'),
            );
        }

        if ($this->_duty['result']['pattern'] === '0') {
            $this->_base_update[] = $data;
        }
        else {
            $this->_base_insert[] = $data;
        }

        return true;
    }

    /**
     * 打刻一括保存実行
     * @return boolean
     */
    public function exec()
    {
        // plan.update
        if (!empty($this->_plan_update)) {
            $sql = "
                UPDATE atdbkrslt
                SET
                    pattern = :pattern,
                    reason = :reason,
                    night_duty = :night_duty,
                    allow_id = :allow_id,
                    tmcd_group_id = :tmcd_group_id,
                    start_time = :start_time,
                    end_time = :end_time,
                    next_day_flag = :next_day_flag,
                    rest_start_time = :rest_start_time,
                    rest_end_time = :rest_end_time,
                    start_btn_time = :start_btn_time,
                    base_shorter_minutes = :base_shorter_minutes,
                    real_shorter_minutes = :real_shorter_minutes,
                    update_emp_id = :update_emp_id,
                    update_time = :update_time
                WHERE
                    emp_id = :emp_id
                AND date = :date
            ";
            $sth = $this->db->prepare($sql, array('text', 'text', 'text', 'integer', 'integer', 'text', 'text', 'integer', 'text', 'text', 'text', 'integer', 'integer', 'text', 'text', 'text', 'text'));
            $res = $this->db->extended->executeMultiple($sth, $this->_plan_update);

            if (PEAR::isError($res)) {
                $this->rollback();
                cmx_log("plan.update: " . $res->getDebugInfo());
                return false;
            }
            $this->log()->log('予定から更新:' . count($this->_plan_update), print_r($this->_plan_update, true));
        }

        // plan.insert
        if (!empty($this->_plan_insert)) {
            $sql = "
                INSERT INTO atdbkrslt
                    (emp_id, date, pattern, reason, night_duty, allow_id, tmcd_group_id, start_time, end_time, start_btn_time, update_emp_id, update_time, next_day_flag, rest_start_time, rest_end_time, base_shorter_minutes, real_shorter_minutes)
                VALUES
                    (:emp_id, :date, :pattern, :reason, :night_duty, :allow_id, :tmcd_group_id, :start_time, :end_time, :start_btn_time, :update_emp_id, :update_time, :next_day_flag, :rest_start_time, :rest_end_time, :base_shorter_minutes, :real_shorter_minutes)
            ";
            $sth = $this->db->prepare($sql, array('text', 'text', 'text', 'text', 'text', 'integer', 'integer', 'text', 'text', 'text', 'text', 'text', 'integer', 'text', 'text', 'integer', 'integer'));
            $res = $this->db->extended->executeMultiple($sth, $this->_plan_insert);

            if (PEAR::isError($res)) {
                $this->db->rollback();
                cmx_log("plan.insert: " . $res->getDebugInfo());
                return false;
            }
            $this->log()->log('予定から追加:' . count($this->_plan_insert), print_r($this->_plan_insert, true));
        }

        // base.update
        if (!empty($this->_base_update)) {
            $sql = "
                UPDATE atdbkrslt
                SET
                    pattern = :pattern,
                    reason = :reason,
                    tmcd_group_id = :tmcd_group_id,
                    start_time = :start_time,
                    end_time = :end_time,
                    next_day_flag = :next_day_flag,
                    rest_start_time = :rest_start_time,
                    rest_end_time = :rest_end_time,
                    start_btn_time = :start_btn_time,
                    base_shorter_minutes = :base_shorter_minutes,
                    real_shorter_minutes = :real_shorter_minutes,
                    update_emp_id = :update_emp_id,
                    update_time = :update_time
                WHERE
                    emp_id = :emp_id
                AND date = :date
            ";
            $sth = $this->db->prepare($sql, array('text', 'text', 'integer', 'text', 'text', 'integer', 'text', 'text', 'text', 'integer', 'integer', 'text', 'text', 'text', 'text'));
            $res = $this->db->extended->executeMultiple($sth, $this->_base_update);

            if (PEAR::isError($res)) {
                $this->rollback();
                cmx_log("base.update: " . $res->getDebugInfo());
                return false;
            }

            $this->log()->log('規定から更新:' . count($this->_base_update), print_r($this->_base_update, true));
        }

        // base.isert
        if (!empty($this->_base_insert)) {
            $sql = "
                INSERT INTO atdbkrslt
                    (emp_id, date, pattern, reason, tmcd_group_id, start_time, end_time, start_btn_time, update_emp_id, update_time, next_day_flag, rest_start_time, rest_end_time, base_shorter_minutes, real_shorter_minutes)
                VALUES
                    (:emp_id, :date, :pattern, :reason, :tmcd_group_id, :start_time, :end_time, :start_btn_time, :update_emp_id, :update_time, :next_day_flag, :rest_start_time, :rest_end_time, :base_shorter_minutes, :real_shorter_minutes)
            ";
            $sth = $this->db->prepare($sql, array('text', 'text', 'text', 'text', 'integer', 'text', 'text', 'text', 'text', 'text', 'integer', 'text', 'text', 'integer', 'integer'));
            $res = $this->db->extended->executeMultiple($sth, $this->_base_insert);

            if (PEAR::isError($res)) {
                $this->rollback();
                cmx_log("base.isert: " . $res->getDebugInfo());
                return false;
            }

            $this->log()->log('規定から追加:' . count($this->_base_insert), print_r($this->_base_insert, true));
        }

        return true;
    }

    /**
     * 曜日の取得
     * @param type $date
     * @return int
     */
    private function get_dow($date)
    {
        // 創立記念日
        // 年末年始
        if (in_array(substr($date, 4, 4), array('0920', '1230', '1231', '0101', '0102', '0103', '0104'))) {
            return 0;
        }

        if ($this->_cache_dow[$date]) {
            $dow = $this->_cache_dow[$date];
        }
        else {
            $dow = $this->db->extended->getRow("SELECT type, date_part('dow', to_date(date, 'YYYYMMDD')) AS dow FROM calendar WHERE date = ?", null, array($date), array('text'), MDB2_FETCHMODE_ASSOC);
            $this->_cache_dow[$date] = $dow;
        }

        switch ($dow['type']) {
            case '2': // 土曜
                return 6;

            case '6': // 祝日
                return 7;

            case '3': // 日祝
            case '7': // 年末年始休暇
            case '8': // 買取日
                return 0;

            default:
                return $dow['dow'];
        }
    }

    /**
     * 勤務記号の取得
     * @param type $group_id
     * @param type $pattern
     * @return boolean
     */
    private function get_pattern($group_id, $pattern)
    {
        if ($this->_cache_pattern[$group_id][$pattern]) {
            return $this->_cache_pattern[$group_id][$pattern];
        }

        $sql = "
                SELECT
                    REPLACE(oh.officehours2_start, ':', '') AS start_time,
                    REPLACE(oh.officehours2_end,   ':', '') AS end_time,
                    REPLACE(oh.officehours4_start, ':', '') AS rest_start_time,
                    REPLACE(oh.officehours4_end,   ':', '') AS rest_end_time
                FROM calendar c
                LEFT JOIN officehours oh ON oh.tmcd_group_id = :group_id AND oh.pattern = :pattern AND oh.officehours_id=(CASE WHEN c.type='2' THEN '2' WHEN c.type='3' THEN '3' ELSE '1' END)
                WHERE
                    date = to_char(CURRENT_DATE, 'YYYYMMDD')
            ";

        $sth = $this->db->prepare($sql);
        $res = $sth->execute(array('group_id' => $group_id, 'pattern' => $pattern));
        if ($res->numRows() <= 0) {
            $this->rollback();
            cmx_log("no fetch officehours");
            return false;
        }

        $ptn = $res->fetchRow(MDB2_FETCHMODE_ASSOC);
        $this->_cache_pattern[$group_id][$pattern] = $ptn;

        return $ptn;
    }

    /**
     * 時短時間の取得
     * @param type $date
     * @return int 減算前時短時間
     * @return int 減算後時短時間
     */
    private function get_shorter_minutes()
    {
        // 対象外
        if (date('Y-m-d', strtotime($this->_date)) > $this->_emp['end_1y_date'] && date('Y-m-d', strtotime($this->_date)) > $this->_emp['end_3y_date']) {
            return array(null, null);
        }

        // 曜日
        $dow = $this->get_dow($this->_date);

        // 時短時間
        $week = unserialize($this->_emp['week']);

        // 時短対象（1歳）
        if (date('Y-m-d', strtotime($this->_date)) <= $this->_emp['end_1y_date']) {
            if ($week[$dow] <= 60) {
                $short_min = 0;
            }
            else {
                $short_min = $week[$dow] - 60;
            }
            return array($week[$dow], $short_min);
        }
        // 時短対象（3歳）
        else if (date('Y-m-d', strtotime($this->_date)) <= $this->_emp['end_3y_date']) {
            return array($week[$dow], $week[$dow]);
        }
    }

}
