<?php

require_once('Cmx.php');
require_once('Cmx/Core/Model.php');

/**
 * 勤務表：部署スタッフ 長期休暇
 */
class Cmx_Shift_StaffShorter extends Model
{

    /**
     * コンストラクタ
     *
     * @param type $emp_id 職員ID
     */
    function Cmx_Shift_StaffShorter()
    {
        parent::connectDB();
    }

    /**
     * データ更新
     *
     * @param type $post POSTデータ
     * @return true: 成功 false:失敗
     */
    function update($post)
    {
        $this->db->beginNestedTransaction();

        $data[] = array(
            'emp_id' => $post['emp_id'],
            'end_1y_date' => $post['end_1y_date'],
            'end_3y_date' => $post['end_3y_date'],
            'week' => serialize($post['week'])
        );
        $is_employment = $this->db->extended->getOne(
            "SELECT COUNT(*) FROM duty_shift_staff_employment_shorter WHERE emp_id= ?", null, array($post['emp_id']), array('text', 'text')
        );
        if ($is_employment) {
            // UPDATE
            $sth = $this->db->prepare("
                UPDATE duty_shift_staff_employment_shorter
                SET end_1y_date = :end_1y_date, end_3y_date = :end_3y_date, week = :week
                WHERE emp_id = :emp_id
                ", array('date', 'date', 'text', 'text'), MDB2_PREPARE_MANIP);
        }
        else {
            // INSERT
            $sth = $this->db->prepare("
               INSERT INTO duty_shift_staff_employment_shorter
               (emp_id, end_1y_date, end_3y_date, week)
               VALUES (:emp_id, :end_1y_date, :end_3y_date, :week)
               ", array('text', 'date', 'date', 'text'), MDB2_PREPARE_MANIP);
        }
        $res = $this->db->extended->executeMultiple($sth, $data);
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            cmx_log('ROLLBACK: Cmx_Shift_Staff: employee_update: INSERT/UPDATE duty_shift_staff_employment_shorter: ' . $res->getDebugInfo());
            return false;
        }
        $sth->free();

        // commit
        $this->db->completeNestedTransaction();
        return true;
    }

}
