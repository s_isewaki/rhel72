<?php

//
// CoMedix全体の設定の管理クラス
// (configテーブルのgetのみ)
//
//
require_once('Cmx.php');
require_once('Cmx/Core/Model.php');

class Cmx_Config extends Model {

    var $_row;

    // Constractor
    function Cmx_Config() {
        parent::connectDB();

        // select
        $sth = $this->db->prepare("SELECT * FROM config");
        $res = $sth->execute();
        $this->_row = $res->fetchRow(MDB2_FETCHMODE_ASSOC);
    }

    // Get Method
    function get($name) {
        if ($this->_row[$name]) {
            return $this->_row[$name];
        }
        return null;
    }

}
