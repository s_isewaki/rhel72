<?php

require_once('Cmx.php');
require_once('Cmx/Core/Model.php');
require_once('Cmx/Model/Employee.php');
require_once("Cmx/Model/SystemConfig.php");

/**
 * CoMedix セッション
 */
class Cmx_Session extends Model {

    var $_session;
    var $_emp_id;

    /**
     * コンストラクタ
     * @param str $session セッションID
     */
    function Cmx_Session($session = null) {
        parent::connectDB();
        $this->_init($session);
    }

    /**
     * 初期化
     *
     * @param  str $session セッションID
     */
    function _init($session = null) {

        if (!is_null($session)) {
            $this->_session = $session;
            return;
        }

        if (!empty($_COOKIE["CMX_AUTH_SESSION"])) {
            $this->_session = $_COOKIE["CMX_AUTH_SESSION"];
            return;
        }

        if (!empty($_POST["session"])) {
            $this->_session = $_POST["session"];
            return;
        }

        if (!empty($_GET["session"])) {
            $this->_session = $_GET["session"];
            return;
        }
    }

    /**
     * セッションID用ハッシュ値を生成する
     *
     * @return str ハッシュ値
     */
    function create_hash() {
        $hash = md5(uniqid(rand(), true));
        if (strlen($hash) == 0) {
            return false;
        }
        return $hash;
    }

    /**
     * セッションの認証を行う
     *
     * @param  int $expire 0ならタイムアウト値を無視する
     * @return bool [TRUE]成功 [FALSE]失敗
     */
    function qualify($expire = null) {
        if (empty($this->_session)) {
            return false;
        }

        $now = time();

        // セッション期限有り
        $timeout_sec = $this->timeoutSec();
        if ($timeout_sec > 0 and $expire !== 0) {
            $sth = $this->db->prepare("SELECT * FROM session WHERE session_id= ? AND session_made between ? and ?");
            $res = $sth->execute(array($this->_session, $now - $timeout_sec, $now));
        }
        // セッション期限無し
        else {
            $sth = $this->db->prepare("SELECT * FROM session WHERE session_id= ?");
            $res = $sth->execute(array($this->_session));
        }

        // 正しくSELECT文が実行されなかった場合 false
        if (PEAR::isError($res)) {
            return false;
        }

        // レコードが1件以外の場合 false
        if ($res->numRows() != 1) {
            return false;
        }

        // 職員ID
        $row = $res->fetchRow(MDB2_FETCHMODE_ASSOC);
        $this->_emp_id = $row["emp_id"];
        $GLOBALS['_session_emp_id'] = $row["emp_id"];

        // セッション時刻の更新
        return $this->update($now);
    }

    /**
     * セッションの更新
     *
     * @param str $now 更新時刻
     * @return bool [TRUE]成功 [FALSE]失敗
     */
    function update($now) {
        if (empty($this->_session)) {
            return false;
        }

        if (!$now) {
            $now = time();
        }

        $this->db->beginTransaction();
        $sth = $this->db->prepare("UPDATE session SET session_made= ? WHERE session_id= ?", array('text', 'text'), MDB2_PREPARE_MANIP);

        $res = $sth->execute(
            array(
                $now,
                $this->_session,
            )
        );

        if (PEAR::isError($res)) {
            $this->db->rollback();
            return false;
        }
        else {
            $this->db->commit();
            return true;
        }
    }

    /**
     * セッションIDを返す
     *
     * @return セッションID
     */
    function id() {
        return $this->_session;
    }

    /**
     * セッションユーザの職員データを返す
     *
     * @return Cmx_Employeeオブジェクト
     */
    function emp() {
        if ($this->_emp_id) {
            return new Cmx_Employee($this->_emp_id);
        }
        return null;
    }

    /**
     * タイムアウト値を返す
     *
     * @return タイムアウト値（秒単位）
     */
    function timeoutSec() {
        $conf = new Cmx_SystemConfig();
        $timeout_minutes = $conf->get('config.timeout_minutes');
        return intval($timeout_minutes) * 60;
    }
}
