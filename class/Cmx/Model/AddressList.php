<?php

require_once('Cmx.php');
require_once('Cmx/Core/Model.php');

/**
 * CoMedix：アドレス帳
 *
 */
class Cmx_AddressList extends Model {
    var $emp_id;

    /**
     * コンストラクタ
     */
    function Cmx_AddressList($emp_id) {
        parent::connectDB();

        $this->emp_id = $emp_id;
    }

    /**
     * 一覧リスト
     * @param type $query
     * @param type $share
     * @return type
     */
    function lists($query='', $share='') {

        // query
        $query = preg_replace("/　/", "", $query);
        $query = preg_replace("/\s+/", "", $query);
        $query = mb_convert_encoding($query, 'EUCjp-win', 'utf-8');
        if (!empty($query)) {
    		$query_sql = "AND (name1 || name2 LIKE :query OR name_kana1 || name_kana2 LIKE :query OR email_pc LIKE :query OR email_mobile LIKE :query) ";
        }

        // share
        if ($share === 'f') {
            $share_sql = "AND emp_id = :emp_id AND shared_flg='f' ";
        }
        else if ($share === 't') {
            $share_sql = "AND shared_flg='t' ";
        }
        else {
            $share_sql = "AND ((emp_id = :emp_id AND shared_flg='f') OR shared_flg='t') ";
        }

        $sth = $this->db->prepare("
            SELECT
                address_id,
                name1 || ' ' || name2 as name,
                email_pc,
                email_mobile,
                shared_flg
            FROM address
            WHERE
                address_del_flg = 'f'
                {$query_sql}
                {$share_sql}
            ORDER BY name_kana1, name_kana2,address_id
        ");
        $res = $sth->execute(array(
            'emp_id' => $this->emp_id,
            'query' => "%{$query}%",
        ));
        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[] = $row;
        }
        return $list;
    }

    /**
     * グループリスト
     * @return array
     */
    function group_lists() {
        $sth = $this->db->prepare("
            SELECT
                g.id,
                g.name,
                (SELECT COUNT(*) FROM adbkgrpmem m LEFT JOIN authmst a ON (m.emp_id = a.emp_id) WHERE (NOT a.emp_del_flg OR a.emp_del_flg IS NULL) AND m.adbkgrp_id = g.id) AS count
            FROM adbkgrp g
            WHERE
                g.emp_id = :emp_id
        ");
        $res = $sth->execute(array('emp_id' => $this->emp_id));
        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[] = $row;
        }
        return $list;
    }

    /**
     * グループに所属するアドレスリスト
     * @return array
     */
    function subgroup_lists($group_id) {
        $sth = $this->db->prepare("
            SELECT CASE WHEN m.type = '1' OR m.type = '2' THEN m.emp_id
                        WHEN m.type = '3' OR m.type = '4' THEN CAST(m.address_id AS TEXT)
                        END AS id,
                   CASE WHEN m.type = '1' OR m.type = '2' THEN TRIM(e.emp_lt_nm || ' ' || e.emp_ft_nm)
                        WHEN m.type = '3' OR m.type = '4' THEN TRIM(a.name1     || ' ' || a.name2)
                        END AS name,
                   CASE m.type WHEN '1' THEN get_mail_login_id(m.emp_id)
                               WHEN '2' THEN e.emp_email2
                               WHEN '3' THEN a.email_pc
                               WHEN '4' THEN a.email_mobile
                        END AS mail,
                   m.type
            FROM adbkgrpmem m
            LEFT JOIN empmst e on m.emp_id = e.emp_id
            LEFT JOIN authmst u on e.emp_id = u.emp_id
            LEFT JOIN address a on m.address_id = a.address_id
            WHERE m.adbkgrp_id = :adbkgrp_id
              AND (NOT u.emp_del_flg OR u.emp_del_flg IS NULL)
            ORDER BY m.order_no
        ");
        $res = $sth->execute(array('adbkgrp_id' => $group_id));
        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[] = $row;
        }
        return $list;
    }
}
