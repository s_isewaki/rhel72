<?php

require_once('Cmx.php');
require_once('Cmx/Core/Model.php');

/**
 * CoMedix：職員名簿
 *
 */
class Cmx_EmployeeList extends Model {
    var $cond;
    var $mcond;
    var $emplist_status_sort_flg;

    /**
     * コンストラクタ
     */
    function Cmx_EmployeeList($mode=null, $webmail=1) {
        parent::connectDB();

        switch ($mode) {
            case 'hiyari':
                $this->cond = 'AND a.emp_inci_flg ';
                break;
            case 'webmail':
                $this->cond = 'AND a.emp_webml_flg ';
                break;
            default :
                break;
        }

        if (($mode === 'webmail' or $mode === 'address_group') and ! $webmail) {
            $this->mcond = "AND e.emp_email2 != '' ";
        }

        global $emplist_status_sort_flg;
        $this->status_sort_flg = $emplist_status_sort_flg;
    }

    /**
     * 名前検索リスト
     * @return array
     */
    function search_lists($keyword) {
        $keyword = preg_replace("/\s+/", "", $keyword);

        if (empty($keyword)) {
            return array();
        }

        if ($this->status_sort_flg) {
            $order = "s.order_no, e.emp_kn_lt_nm, e.emp_kn_ft_nm";
        } else {
            $order = "e.emp_kn_lt_nm, e.emp_kn_ft_nm";
        }
        $sth = $this->db->prepare("
            SELECT
                e.emp_id,
                e.emp_lt_nm || ' ' || e.emp_ft_nm AS name,
                get_mail_login_id(e.emp_id) AS loginid,
                e.emp_email2 AS email,
                c1.class_nm,
                c2.atrb_nm,
                c3.dept_nm,
                c4.room_nm,
                j.job_nm,
                s.st_nm
            FROM empmst e
            INNER JOIN login     l  ON e.emp_id=l.emp_id
            INNER JOIN authmst   a  ON e.emp_id=a.emp_id AND a.emp_del_flg='f'
            INNER JOIN classmst  c1 ON c1.class_id=e.emp_class
            INNER JOIN atrbmst   c2 ON c2.atrb_id=e.emp_attribute
            INNER JOIN deptmst   c3 ON c3.dept_id=e.emp_dept
            LEFT  JOIN classroom c4 ON c4.room_id=e.emp_room
            INNER JOIN jobmst    j  ON j.job_id=e.emp_job
            INNER JOIN stmst     s  ON s.st_id=e.emp_st
            WHERE
                (
                e.emp_lt_nm || e.emp_ft_nm LIKE :query
                OR e.emp_kn_lt_nm || e.emp_kn_ft_nm LIKE :query
                OR l.emp_login_id LIKE :query
                OR e.emp_email2 LIKE :query
                )
                {$this->cond}
                {$this->mcond}
            ORDER BY $order
        ");
        $res = $sth->execute(array('query' => '%'. $keyword . '%'));
        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[] = $row;
        }
        return $list;
    }

    /**
     * よく使う人リスト
     * @return array
     */
    function often_used_lists($emp_id) {
        $sth = $this->db->prepare("
            SELECT
                wc.used_emp_id AS emp_id,
                e.emp_lt_nm || ' ' || e.emp_ft_nm AS name,
                get_mail_login_id(wc.used_emp_id) AS loginid,
                e.emp_email2 AS email,
                wc.count_num,
                c1.class_nm,
                c2.atrb_nm,
                c3.dept_nm,
                c4.room_nm,
                j.job_nm,
                s.st_nm
            FROM wm_counter wc
            INNER JOIN empmst    e  ON e.emp_id=wc.used_emp_id
            INNER JOIN authmst   a  ON e.emp_id=a.emp_id AND a.emp_del_flg='f'
            INNER JOIN classmst  c1 ON c1.class_id=e.emp_class
            INNER JOIN atrbmst   c2 ON c2.atrb_id=e.emp_attribute
            INNER JOIN deptmst   c3 ON c3.dept_id=e.emp_dept
            LEFT  JOIN classroom c4 ON c4.room_id=e.emp_room
            INNER JOIN jobmst    j  ON j.job_id=e.emp_job
            INNER JOIN stmst     s  ON s.st_id=e.emp_st
            WHERE
                wc.emp_id = ?
                {$this->cond}
                {$this->mcond}
            ORDER BY count_num DESC
            LIMIT 20
        ");
        $res = $sth->execute(array($emp_id));
        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[] = $row;
        }
        return $list;
    }

    /**
     * 組織:第1階層に該当する職員リスト
     * @return array
     */
    function emp_class_lists($id) {
        if (empty($id)) {
            return;
        }

        if ($this->status_sort_flg) {
            $order = "s.order_no, e.emp_kn_lt_nm, e.emp_kn_ft_nm";
        } else {
            $order = "e.emp_kn_lt_nm, e.emp_kn_ft_nm";
        }
        $sth = $this->db->prepare("
            SELECT
                ec.emp_id,
                ec.emp_concurrent,
                e.emp_lt_nm || ' ' || e.emp_ft_nm AS name,
                get_mail_login_id(ec.emp_id) AS loginid,
                e.emp_email2 AS email,
                c1.class_nm,
                c2.atrb_nm,
                c3.dept_nm,
                c4.room_nm,
                j.job_nm,
                s.st_nm,
                ec.emp_room
            FROM (
                SELECT emp_id, emp_class, emp_attribute, emp_dept, emp_room, emp_st, 0 AS emp_concurrent FROM empmst WHERE emp_class= :id
                UNION
                SELECT emp_id, emp_class, emp_attribute, emp_dept, emp_room, emp_st, 1 AS emp_concurrent FROM concurrent WHERE emp_class= :id
            ) ec
            INNER JOIN empmst    e  ON ec.emp_id=e.emp_id
            INNER JOIN login     l  ON ec.emp_id=l.emp_id
            INNER JOIN authmst   a  ON ec.emp_id=a.emp_id AND a.emp_del_flg='f'
            INNER JOIN classmst  c1 ON c1.class_id=ec.emp_class
            INNER JOIN atrbmst   c2 ON c2.atrb_id=ec.emp_attribute
            INNER JOIN deptmst   c3 ON c3.dept_id=ec.emp_dept
            LEFT  JOIN classroom c4 ON c4.room_id=ec.emp_room
            INNER JOIN jobmst    j  ON j.job_id=e.emp_job
            INNER JOIN stmst     s  ON s.st_id=ec.emp_st
            WHERE 1=1
                {$this->cond}
                {$this->mcond}
            ORDER BY $order
        ");
        $res = $sth->execute(array('id' => $id));
        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[] = $row;
        }
        if (empty($list)) {
            $list[] = array(
                'id' => '0',
                'name' => '(未登録)',
                'count' => '0',
                'kind' => 'subgroup',
            );
        }

        return $list;
    }

    /**
     * 組織:第2階層に該当する職員リスト
     * @return array
     */
    function emp_atrb_lists($id) {
        if (empty($id)) {
            return;
        }

        if ($this->status_sort_flg) {
            $order = "s.order_no, e.emp_kn_lt_nm, e.emp_kn_ft_nm";
        } else {
            $order = "e.emp_kn_lt_nm, e.emp_kn_ft_nm";
        }
        $sth = $this->db->prepare("
            SELECT
                ec.emp_id,
                ec.emp_concurrent,
                e.emp_lt_nm || ' ' || e.emp_ft_nm AS name,
                get_mail_login_id(ec.emp_id) AS loginid,
                e.emp_email2 AS email,
                c1.class_nm,
                c2.atrb_nm,
                c3.dept_nm,
                c4.room_nm,
                j.job_nm,
                s.st_nm,
                ec.emp_room
            FROM (
                SELECT emp_id, emp_class, emp_attribute, emp_dept, emp_room, emp_st, 0 AS emp_concurrent FROM empmst WHERE emp_attribute= :id
                UNION
                SELECT emp_id, emp_class, emp_attribute, emp_dept, emp_room, emp_st, 1 AS emp_concurrent FROM concurrent WHERE emp_attribute= :id
            ) ec
            INNER JOIN empmst    e  ON ec.emp_id=e.emp_id
            INNER JOIN login     l  ON ec.emp_id=l.emp_id
            INNER JOIN authmst   a  ON ec.emp_id=a.emp_id AND a.emp_del_flg='f'
            INNER JOIN classmst  c1 ON c1.class_id=ec.emp_class
            INNER JOIN atrbmst   c2 ON c2.atrb_id=ec.emp_attribute
            INNER JOIN deptmst   c3 ON c3.dept_id=ec.emp_dept
            LEFT  JOIN classroom c4 ON c4.room_id=ec.emp_room
            INNER JOIN jobmst    j  ON j.job_id=e.emp_job
            INNER JOIN stmst     s  ON s.st_id=ec.emp_st
            WHERE 1=1
                {$this->cond}
                {$this->mcond}
            ORDER BY $order
        ");
        $res = $sth->execute(array('id' => $id));
        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[] = $row;
        }
        if (empty($list)) {
            $list[] = array(
                'id' => '0',
                'name' => '(未登録)',
                'count' => '0',
                'kind' => 'subgroup',
            );
        }

        return $list;
    }

    /**
     * 組織:第3階層に該当する職員リスト
     * @return array
     */
    function emp_dept_lists($id) {
        if (empty($id)) {
            return;
        }

        if ($this->status_sort_flg) {
            $order = "s.order_no, e.emp_kn_lt_nm, e.emp_kn_ft_nm";
        } else {
            $order = "e.emp_kn_lt_nm, e.emp_kn_ft_nm";
        }
        $sth = $this->db->prepare("
            SELECT
                ec.emp_id,
                ec.emp_concurrent,
                e.emp_lt_nm || ' ' || e.emp_ft_nm AS name,
                get_mail_login_id(ec.emp_id) AS loginid,
                e.emp_email2 AS email,
                c1.class_nm,
                c2.atrb_nm,
                c3.dept_nm,
                c4.room_nm,
                j.job_nm,
                s.st_nm,
                ec.emp_room
            FROM (
                SELECT emp_id, emp_class, emp_attribute, emp_dept, emp_room, emp_st, 0 AS emp_concurrent FROM empmst WHERE emp_dept= :id
                UNION
                SELECT emp_id, emp_class, emp_attribute, emp_dept, emp_room, emp_st, 1 AS emp_concurrent FROM concurrent WHERE emp_dept= :id
            ) ec
            INNER JOIN empmst    e  ON ec.emp_id=e.emp_id
            INNER JOIN login     l  ON ec.emp_id=l.emp_id
            INNER JOIN authmst   a  ON ec.emp_id=a.emp_id AND a.emp_del_flg='f'
            INNER JOIN classmst  c1 ON c1.class_id=ec.emp_class
            INNER JOIN atrbmst   c2 ON c2.atrb_id=ec.emp_attribute
            INNER JOIN deptmst   c3 ON c3.dept_id=ec.emp_dept
            LEFT  JOIN classroom c4 ON c4.room_id=ec.emp_room
            INNER JOIN jobmst    j  ON j.job_id=e.emp_job
            INNER JOIN stmst     s  ON s.st_id=ec.emp_st
            WHERE 1=1
                {$this->cond}
                {$this->mcond}
            ORDER BY $order
        ");
        $res = $sth->execute(array('id' => $id));
        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[] = $row;
        }

        // 第4階層
        if ($this->class_count() === "4") {
            $sth = $this->db->prepare("
                SELECT
                    *, room_id as id, room_nm as name,
                    (SELECT COUNT(*) FROM empmst e JOIN authmst a USING (emp_id) WHERE a.emp_del_flg='f' {$this->cond} {$this->mcond} AND e.emp_dept=dept_id AND e.emp_room=room_id) +
                    (SELECT COUNT(*) FROM concurrent cc JOIN authmst a USING (emp_id) WHERE a.emp_del_flg='f' {$this->cond} AND cc.emp_dept=dept_id AND cc.emp_room=room_id) AS count,
                    'subgroup' AS kind,
                    'room' AS type
                FROM classroom
                WHERE
                    NOT room_del_flg
                    AND dept_id= :id
                ORDER BY order_no, room_id
            ");
            $res = $sth->execute(array('id' => $id));
            while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                $list[] = $row;
            }
        }
        if (empty($list)) {
            $list[] = array(
                'id' => '0',
                'name' => '(未登録)',
                'count' => '0',
                'kind' => 'subgroup',
            );
        }

        return $list;
    }

    /**
     * 組織:第4階層に該当する職員リスト
     * @return array
     */
    function emp_room_lists($id) {
        if (empty($id)) {
            return;
        }

        if ($this->status_sort_flg) {
            $order = "s.order_no, e.emp_kn_lt_nm, e.emp_kn_ft_nm";
        } else {
            $order = "e.emp_kn_lt_nm, e.emp_kn_ft_nm";
        }
        $sth = $this->db->prepare("
            SELECT
                ec.emp_id,
                ec.emp_concurrent,
                e.emp_lt_nm || ' ' || e.emp_ft_nm AS name,
                get_mail_login_id(ec.emp_id) AS loginid,
                e.emp_email2 AS email,
                c1.class_nm,
                c2.atrb_nm,
                c3.dept_nm,
                c4.room_nm,
                j.job_nm,
                s.st_nm
            FROM (
                SELECT emp_id, emp_class, emp_attribute, emp_dept, emp_room, emp_st, 0 AS emp_concurrent FROM empmst WHERE emp_room= :id
                UNION
                SELECT emp_id, emp_class, emp_attribute, emp_dept, emp_room, emp_st, 1 AS emp_concurrent FROM concurrent WHERE emp_room= :id
            ) ec
            INNER JOIN empmst    e  ON ec.emp_id=e.emp_id
            INNER JOIN login     l  ON ec.emp_id=l.emp_id
            INNER JOIN authmst   a  ON ec.emp_id=a.emp_id AND a.emp_del_flg='f'
            INNER JOIN classmst  c1 ON c1.class_id=ec.emp_class
            INNER JOIN atrbmst   c2 ON c2.atrb_id=ec.emp_attribute
            INNER JOIN deptmst   c3 ON c3.dept_id=ec.emp_dept
            LEFT  JOIN classroom c4 ON c4.room_id=ec.emp_room
            INNER JOIN jobmst    j  ON j.job_id=e.emp_job
            INNER JOIN stmst     s  ON s.st_id=ec.emp_st
            WHERE 1=1
                {$this->cond}
                {$this->mcond}
            ORDER BY $order
        ");
        $res = $sth->execute(array('id' => $id));
        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[] = $row;
        }

        return $list;
    }

    /**
     * 職種に該当する職員リスト
     * @return array
     */
    function emp_job_lists($id) {
        if (empty($id)) {
            return;
        }

        if ($this->status_sort_flg) {
            $order = "s.order_no, e.emp_kn_lt_nm, e.emp_kn_ft_nm";
        } else {
            $order = "e.emp_kn_lt_nm, e.emp_kn_ft_nm";
        }
        $sth = $this->db->prepare("
            SELECT
                e.emp_id,
                get_emp_name(e.emp_id) AS name,
                get_mail_login_id(e.emp_id) AS loginid,
                get_emp_email(e.emp_id) AS email,
                c1.class_nm,
                c2.atrb_nm,
                c3.dept_nm,
                c4.room_nm,
                j.job_nm,
                s.st_nm
            FROM empmst e
            INNER JOIN login     l  ON e.emp_id=l.emp_id
            INNER JOIN authmst   a  ON e.emp_id=a.emp_id AND a.emp_del_flg='f'
            INNER JOIN classmst  c1 ON c1.class_id=e.emp_class
            INNER JOIN atrbmst   c2 ON c2.atrb_id=e.emp_attribute
            INNER JOIN deptmst   c3 ON c3.dept_id=e.emp_dept
            LEFT  JOIN classroom c4 ON c4.room_id=e.emp_room
            INNER JOIN jobmst    j  ON j.job_id=e.emp_job
            INNER JOIN stmst     s  ON s.st_id=e.emp_st
            WHERE
                e.emp_job = ?
                {$this->cond}
                {$this->mcond}
            ORDER BY $order
        ");
        $res = $sth->execute(array($id));
        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[] = $row;
        }
        return $list;
    }

    /**
     * 役職に該当する職員リスト
     * @return array
     */
    function emp_status_lists($id) {
        if (empty($id)) {
            return;
        }

        $sth = $this->db->prepare("
            SELECT
                ec.emp_id,
                ec.emp_concurrent,
                e.emp_lt_nm || ' ' || e.emp_ft_nm AS name,
                get_mail_login_id(ec.emp_id) AS loginid,
                e.emp_email2 AS email,
                c1.class_nm,
                c2.atrb_nm,
                c3.dept_nm,
                c4.room_nm,
                j.job_nm,
                s.st_nm
            FROM (
                SELECT emp_id, emp_class, emp_attribute, emp_dept, emp_room, emp_st, 0 AS emp_concurrent FROM empmst WHERE emp_st= :id
                UNION
                SELECT emp_id, emp_class, emp_attribute, emp_dept, emp_room, emp_st, 1 AS emp_concurrent FROM concurrent WHERE emp_st= :id
            ) ec
            INNER JOIN empmst    e  ON ec.emp_id=e.emp_id
            INNER JOIN login     l  ON ec.emp_id=l.emp_id
            INNER JOIN authmst   a  ON ec.emp_id=a.emp_id AND a.emp_del_flg='f'
            INNER JOIN classmst  c1 ON c1.class_id=ec.emp_class
            INNER JOIN atrbmst   c2 ON c2.atrb_id=ec.emp_attribute
            INNER JOIN deptmst   c3 ON c3.dept_id=ec.emp_dept
            LEFT  JOIN classroom c4 ON c4.room_id=ec.emp_room
            INNER JOIN jobmst    j  ON j.job_id=e.emp_job
            INNER JOIN stmst     s  ON s.st_id=ec.emp_st
            WHERE 1=1
                {$this->cond}
                {$this->mcond}
            ORDER BY e.emp_kn_lt_nm, e.emp_kn_ft_nm
        ");
        $res = $sth->execute(array('id' => $id));
        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[] = $row;
        }
        return $list;
    }

    /**
     * 委員会に所属する職員リスト
     * @return array
     */
    function emp_project_lists($emp_id, $id) {
        if (empty($emp_id) or empty($id)) {
            return;
        }

        // 責任者
        $sth = $this->db->prepare("
            SELECT
                e.emp_id,
                e.emp_lt_nm || ' ' || e.emp_ft_nm AS name,
                get_mail_login_id(e.emp_id) AS loginid,
                e.emp_email2 AS email,
                c1.class_nm,
                c2.atrb_nm,
                c3.dept_nm,
                c4.room_nm,
                j.job_nm,
                s.st_nm,
                null as emp_room
            FROM project p
            INNER JOIN empmst    e  ON p.pjt_response=e.emp_id
            INNER JOIN login     l  ON e.emp_id=l.emp_id
            INNER JOIN authmst   a  ON e.emp_id=a.emp_id AND a.emp_del_flg='f'
            INNER JOIN classmst  c1 ON c1.class_id=e.emp_class
            INNER JOIN atrbmst   c2 ON c2.atrb_id=e.emp_attribute
            INNER JOIN deptmst   c3 ON c3.dept_id=e.emp_dept
            LEFT  JOIN classroom c4 ON c4.room_id=e.emp_room
            INNER JOIN jobmst    j  ON j.job_id=e.emp_job
            INNER JOIN stmst     s  ON s.st_id=e.emp_st
            WHERE
                p.pjt_id = :id
                {$this->cond}
                {$this->mcond}
        ");
        $res = $sth->execute(array('id' => $id));
        $list = array($res->fetchRow(MDB2_FETCHMODE_ASSOC));

        // メンバー
        if ($this->status_sort_flg) {
            $order = "p.member_kind, s.order_no, e.emp_kn_lt_nm, e.emp_kn_ft_nm";
        } else {
            $order = "p.member_kind, e.emp_kn_lt_nm, e.emp_kn_ft_nm";
        }
        $sth = $this->db->prepare("
            SELECT
                e.emp_id,
                e.emp_lt_nm || ' ' || e.emp_ft_nm AS name,
                get_mail_login_id(e.emp_id) AS loginid,
                e.emp_email2 AS email,
                c1.class_nm,
                c2.atrb_nm,
                c3.dept_nm,
                c4.room_nm,
                j.job_nm,
                s.st_nm,
                null as emp_room
            FROM promember p
            INNER JOIN empmst    e  ON e.emp_id=p.emp_id
            INNER JOIN login     l  ON e.emp_id=l.emp_id
            INNER JOIN authmst   a  ON e.emp_id=a.emp_id AND a.emp_del_flg='f'
            INNER JOIN classmst  c1 ON c1.class_id=e.emp_class
            INNER JOIN atrbmst   c2 ON c2.atrb_id=e.emp_attribute
            INNER JOIN deptmst   c3 ON c3.dept_id=e.emp_dept
            LEFT  JOIN classroom c4 ON c4.room_id=e.emp_room
            INNER JOIN jobmst    j  ON j.job_id=e.emp_job
            INNER JOIN stmst     s  ON s.st_id=e.emp_st
            WHERE
                p.pjt_id = :id
                {$this->cond}
                {$this->mcond}
            ORDER BY $order
        ");
        $res = $sth->execute(array('id' => $id));
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[] = $row;
        }

        // WG
        $sth = $this->db->prepare("
            SELECT
                p.pjt_id AS id,
                p.pjt_name AS name,
                (SELECT COUNT(*) FROM promember m JOIN authmst a USING (emp_id) JOIN empmst e USING (emp_id) WHERE a.emp_del_flg='f' {$this->cond} {$this->mcond} AND p.pjt_id=m.pjt_id) + 1 AS count,
                'subgroup' AS kind,
                'wg' AS type
            FROM project p
            WHERE
                (
                    p.pjt_public_flag = 't'
                    OR p.pjt_response = :emp_id
                    OR EXISTS (SELECT * FROM promember pm WHERE pm.pjt_id=p.pjt_id AND pm.emp_id = :emp_id)
                )
                AND p.pjt_parent_id = :id
                AND p.pjt_delete_flag = 'f'
                AND (p.pjt_start_date IS NULL OR p.pjt_start_date <= CURRENT_DATE)
                AND (p.pjt_real_end_date IS NULL OR p.pjt_real_end_date='' OR p.pjt_real_end_date > to_char(CURRENT_DATE, 'YYYYMMDD'))
            ORDER BY (p.class_id IS NULL) DESC, p.class_id, (p.atrb_id IS NULL) DESC, p.atrb_id, p.pjt_sort_id, p.wg_sort_id, p.pjt_start_date DESC
        ");
        $res = $sth->execute(array('emp_id' => $emp_id, 'id' => $id));
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[] = $row;
        }

        return $list;
    }

    /**
     * WGに該当する職員リスト
     * @return array
     */
    function emp_wg_lists($emp_id, $id) {
        if (empty($emp_id) or empty($id)) {
            return;
        }

        // 責任者
        $sth = $this->db->prepare("
            SELECT
                e.emp_id,
                e.emp_lt_nm || ' ' || e.emp_ft_nm AS name,
                get_mail_login_id(e.emp_id) AS loginid,
                e.emp_email2 AS email,
                c1.class_nm,
                c2.atrb_nm,
                c3.dept_nm,
                c4.room_nm,
                j.job_nm,
                s.st_nm,
                null as emp_room
            FROM project p
            INNER JOIN empmst    e  ON p.pjt_response=e.emp_id
            INNER JOIN login     l  ON e.emp_id=l.emp_id
            INNER JOIN authmst   a  ON e.emp_id=a.emp_id AND a.emp_del_flg='f'
            INNER JOIN classmst  c1 ON c1.class_id=e.emp_class
            INNER JOIN atrbmst   c2 ON c2.atrb_id=e.emp_attribute
            INNER JOIN deptmst   c3 ON c3.dept_id=e.emp_dept
            LEFT  JOIN classroom c4 ON c4.room_id=e.emp_room
            INNER JOIN jobmst    j  ON j.job_id=e.emp_job
            INNER JOIN stmst     s  ON s.st_id=e.emp_st
            WHERE
                p.pjt_id = :id
                {$this->cond}
                {$this->mcond}
        ");
        $res = $sth->execute(array('id' => $id));
        $list = array($res->fetchRow(MDB2_FETCHMODE_ASSOC));

        // メンバー
        if ($this->status_sort_flg) {
            $order = "p.member_kind, s.order_no, e.emp_kn_lt_nm, e.emp_kn_ft_nm";
        } else {
            $order = "p.member_kind, e.emp_kn_lt_nm, e.emp_kn_ft_nm";
        }
        $sth = $this->db->prepare("
            SELECT
                e.emp_id,
                e.emp_lt_nm || ' ' || e.emp_ft_nm AS name,
                get_mail_login_id(e.emp_id) AS loginid,
                e.emp_email2 AS email,
                c1.class_nm,
                c2.atrb_nm,
                c3.dept_nm,
                c4.room_nm,
                j.job_nm,
                s.st_nm,
                null as emp_room
            FROM promember p
            INNER JOIN empmst    e  ON e.emp_id=p.emp_id
            INNER JOIN login     l  ON e.emp_id=l.emp_id
            INNER JOIN authmst   a  ON e.emp_id=a.emp_id AND a.emp_del_flg='f'
            INNER JOIN classmst  c1 ON c1.class_id=e.emp_class
            INNER JOIN atrbmst   c2 ON c2.atrb_id=e.emp_attribute
            INNER JOIN deptmst   c3 ON c3.dept_id=e.emp_dept
            LEFT  JOIN classroom c4 ON c4.room_id=e.emp_room
            INNER JOIN jobmst    j  ON j.job_id=e.emp_job
            INNER JOIN stmst     s  ON s.st_id=e.emp_st
            WHERE
                p.pjt_id = :id
                {$this->cond}
                {$this->mcond}
            ORDER BY $order
        ");
        $res = $sth->execute(array('id' => $id));
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[] = $row;
        }

        return $list;
    }

    /**
     * マイグループに所属する職員リスト
     * @return array
     */
    function emp_mygroup_lists($emp_id, $id) {
        if (empty($emp_id) or empty($id)) {
            return;
        }

        $sth = $this->db->prepare("
            SELECT
                e.emp_id AS emp_id,
                get_emp_name(e.emp_id) AS name,
                get_mail_login_id(e.emp_id) AS loginid,
                get_emp_email(e.emp_id) AS email,
                c1.class_nm,
                c2.atrb_nm,
                c3.dept_nm,
                c4.room_nm,
                j.job_nm,
                s.st_nm
            FROM mygroup g
            INNER JOIN empmst    e  ON e.emp_id=g.member_id
            INNER JOIN authmst   a  ON e.emp_id=a.emp_id AND a.emp_del_flg='f'
            INNER JOIN classmst  c1 ON c1.class_id=e.emp_class
            INNER JOIN atrbmst   c2 ON c2.atrb_id=e.emp_attribute
            INNER JOIN deptmst   c3 ON c3.dept_id=e.emp_dept
            LEFT  JOIN classroom c4 ON c4.room_id=e.emp_room
            INNER JOIN jobmst    j  ON j.job_id=e.emp_job
            INNER JOIN stmst     s  ON s.st_id=e.emp_st
            WHERE
                g.mygroup_id = :id
                AND g.owner_id = :emp_id
                {$this->cond}
                {$this->mcond}
            ORDER BY g.order_no
        ");
        $res = $sth->execute(array('id' => $id, 'emp_id' => $emp_id));
        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[] = $row;
        }
        return $list;
    }

    /**
     * 共通グループに所属する職員リスト
     * @return array
     */
    function emp_comgroup_lists($id) {
        if (empty($id)) {
            return;
        }

        $sth = $this->db->prepare("
            SELECT
                e.emp_id AS emp_id,
                get_emp_name(e.emp_id) AS name,
                get_mail_login_id(e.emp_id) AS loginid,
                get_emp_email(e.emp_id) AS email,
                c1.class_nm,
                c2.atrb_nm,
                c3.dept_nm,
                c4.room_nm,
                j.job_nm,
                s.st_nm
            FROM comgroup g
            INNER JOIN empmst    e  ON e.emp_id=g.emp_id
            INNER JOIN authmst   a  ON e.emp_id=a.emp_id AND a.emp_del_flg='f'
            INNER JOIN classmst  c1 ON c1.class_id=e.emp_class
            INNER JOIN atrbmst   c2 ON c2.atrb_id=e.emp_attribute
            INNER JOIN deptmst   c3 ON c3.dept_id=e.emp_dept
            LEFT  JOIN classroom c4 ON c4.room_id=e.emp_room
            INNER JOIN jobmst    j  ON j.job_id=e.emp_job
            INNER JOIN stmst     s  ON s.st_id=e.emp_st
            WHERE
                g.group_id = ?
                {$this->cond}
                {$this->mcond}
            ORDER BY g.order_no
        ");
        $res = $sth->execute(array($id));
        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[] = $row;
        }
        return $list;
    }

    /**
     * 組織1リスト
     * @return array
     */
    function class_lists() {
        $res = $this->db->query("
            SELECT
                *, class_id as id, class_nm as name,
                (SELECT COUNT(*) FROM empmst e JOIN authmst a USING (emp_id) WHERE a.emp_del_flg='f' {$this->cond} {$this->mcond} AND e.emp_class=class_id) +
                (SELECT COUNT(*) FROM concurrent cc JOIN authmst a USING (emp_id) JOIN empmst e USING (emp_id) WHERE a.emp_del_flg='f' {$this->cond} {$this->mcond} AND cc.emp_class=class_id) AS count
            FROM classmst
            WHERE NOT class_del_flg
            ORDER BY order_no, class_id
        ");
        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[$row['id']] = $row;
        }
        return $list;
    }

    /**
     * 組織2リスト
     * @return array
     */
    function atrb_lists($class_id) {
        $sth = $this->db->prepare("
            SELECT
                *, atrb_id as id, atrb_nm as name,
                (SELECT COUNT(*) FROM empmst e JOIN authmst a USING (emp_id) WHERE a.emp_del_flg='f' {$this->cond} {$this->mcond} AND e.emp_class=class_id AND e.emp_attribute=atrb_id) +
                (SELECT COUNT(*) FROM concurrent cc JOIN authmst a USING (emp_id) JOIN empmst e USING (emp_id) WHERE a.emp_del_flg='f' {$this->cond} {$this->mcond} AND cc.emp_class=class_id AND cc.emp_attribute=atrb_id) AS count
            FROM atrbmst
            WHERE
                NOT atrb_del_flg
                AND class_id= :class_id
            ORDER BY order_no, atrb_id
        ");
        $res = $sth->execute(array('class_id' => $class_id));
        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[$row['id']] = $row;
        }
        if (empty($list)) {
            $list[] = array(
                id => '0',
                name => '(未登録)',
                count => '0',
            );
        }
        return $list;
    }

    /**
     * 組織3リスト
     * @return array
     */
    function dept_lists($artb_id) {
        $sth = $this->db->prepare("
            SELECT
                *, dept_id as id, dept_nm as name,
                (SELECT COUNT(*) FROM empmst e JOIN authmst a USING (emp_id) WHERE a.emp_del_flg='f' {$this->cond} {$this->mcond} AND e.emp_attribute=atrb_id AND e.emp_dept=dept_id) +
                (SELECT COUNT(*) FROM concurrent cc JOIN authmst a USING (emp_id) JOIN empmst e USING (emp_id) WHERE a.emp_del_flg='f' {$this->cond} {$this->mcond} AND cc.emp_attribute=atrb_id AND cc.emp_dept=dept_id) AS count
            FROM deptmst
            WHERE
                NOT dept_del_flg
                AND atrb_id= :atrb_id
            ORDER BY order_no, atrb_id
        ");
        $res = $sth->execute(array('atrb_id' => $artb_id));
        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[$row['id']] = $row;
        }
        if (empty($list)) {
            $list[] = array(
                id => '0',
                name => '(未登録)',
                count => '0',
            );
        }
        return $list;
    }

    /**
     * 組織4リスト
     * @return array
     */
    function room_lists() {
        if ($this->class_count() !== "4") {
            return array();
        }

        $res = $this->db->query("SELECT *, room_id as id, room_nm as name, dept_id as parent FROM classroom WHERE NOT room_del_flg ORDER BY order_no, room_id");
        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[$row['id']] = $row;
        }
        return $list;
    }

    /**
     * 職種リスト
     * @return array
     */
    function job_lists() {
        $res = $this->db->query("
            SELECT
                *, job_id as id, job_nm as name,
                (SELECT COUNT(*) FROM empmst e JOIN authmst a USING (emp_id) WHERE a.emp_del_flg='f' {$this->cond} {$this->mcond} AND e.emp_job=job_id) AS count
            FROM jobmst
            WHERE NOT job_del_flg
            ORDER BY order_no, job_id
        ");
        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[$row['id']] = $row;
        }
        return $list;
    }

    /**
     * 役職リスト
     * @return array
     */
    function status_lists() {
        $res = $this->db->query("
            SELECT
                *, st_id as id, st_nm as name,
                (SELECT COUNT(*) FROM empmst e JOIN authmst a USING (emp_id) WHERE a.emp_del_flg='f' {$this->cond} {$this->mcond} AND e.emp_st=st_id) +
                (SELECT COUNT(*) FROM concurrent cc JOIN authmst a USING (emp_id) JOIN empmst e USING (emp_id) WHERE a.emp_del_flg='f' {$this->cond} {$this->mcond} AND cc.emp_st=st_id) AS count
            FROM stmst
            WHERE NOT st_del_flg
            ORDER BY order_no, st_id
        ");
        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[$row['id']] = $row;
        }
        return $list;
    }

    /**
     * 委員会リスト
     * @return array
     */
    function project_lists($emp_id) {
        $sth = $this->db->prepare("
            SELECT
                p.pjt_id AS id,
                CASE WHEN p.class_id IS NOT NULL THEN class_nm || ' > ' ELSE '' END ||
                CASE WHEN p.atrb_id IS NOT NULL THEN atrb_nm || ' > ' ELSE '' END ||
                p.pjt_name AS name,
                (SELECT COUNT(*) FROM promember m JOIN authmst a USING (emp_id) JOIN empmst e USING (emp_id) WHERE a.emp_del_flg='f' {$this->cond} {$this->mcond} AND p.pjt_id=m.pjt_id) + 1 AS count
            FROM project p
            LEFT JOIN classmst USING (class_id)
            LEFT JOIN atrbmst USING (atrb_id)
            WHERE
                (
                    p.pjt_public_flag = 't'
                    OR p.pjt_response = :emp_id
                    OR EXISTS (SELECT * FROM promember pm WHERE pm.pjt_id=p.pjt_id AND pm.emp_id = :emp_id)
                )
                AND p.pjt_parent_id IS NULL
                AND p.pjt_delete_flag = 'f'
                AND (p.pjt_start_date IS NULL OR p.pjt_start_date <= CURRENT_DATE)
                AND (p.pjt_real_end_date IS NULL OR p.pjt_real_end_date='' OR p.pjt_real_end_date > to_char(CURRENT_DATE, 'YYYYMMDD'))
            ORDER BY (p.class_id IS NULL) DESC, p.class_id, (p.atrb_id IS NULL) DESC, p.atrb_id, p.pjt_sort_id, p.wg_sort_id, p.pjt_start_date DESC
        ");
        $res = $sth->execute(array('emp_id' => $emp_id));
        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[] = $row;
        }
        return $list;
    }

    /**
     * マイグループリスト
     * @return array
     */
    function mygroup_lists($emp_id) {
        $sth = $this->db->prepare("
            SELECT
                m.mygroup_id AS id,
                m.mygroup_nm AS name,
                (SELECT COUNT(*) FROM mygroup mg JOIN authmst a ON (mg.member_id=a.emp_id) JOIN empmst e ON (mg.member_id=e.emp_id) WHERE a.emp_del_flg='f' {$this->cond} {$this->mcond} AND mg.owner_id= :emp_id AND m.mygroup_id=mg.mygroup_id) AS count,
                'mygroup' AS type
            FROM mygroupmst m
            WHERE
                m.owner_id = :emp_id
            UNION ALL
            SELECT
                c.group_id AS id,
                '[共通] ' || c.group_nm AS name,
                (SELECT COUNT(*) FROM comgroup cg JOIN authmst a USING (emp_id) JOIN empmst e USING (emp_id) WHERE a.emp_del_flg='f' {$this->cond} {$this->mcond} AND c.group_id=cg.group_id) AS count,
                'comgroup' AS type
            FROM comgroupmst c
            WHERE
                c.public_flg='t'
                OR EXISTS (SELECT * FROM comgroup g JOIN authmst a USING (emp_id) JOIN empmst e USING (emp_id) WHERE a.emp_del_flg='f' {$this->cond} {$this->mcond} AND c.group_id=g.group_id AND g.emp_id= :emp_id)
        ");
        $res = $sth->execute(array('emp_id' => $emp_id));
        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[] = $row;
        }
        return $list;
    }

    function hiyari_profile($emp_id) {
        require_once 'hiyari_wf_utils.php';

        $sth = $this->db->prepare("
            SELECT
                e.emp_lt_nm || ' ' || e.emp_ft_nm as emp_name,
                c1.class_nm,
                c2.atrb_nm,
                c3.dept_nm,
                c4.room_nm,
                ip.qualification,
                ip.exp_year,
                ip.exp_month,
                ip.dept_year,
                ip.dept_month,
                ip.exp_code,
                ipu.*
            FROM empmst e
            JOIN authmst a USING (emp_id)
            LEFT JOIN inci_profile ip USING (emp_id)
            LEFT JOIN classmst  c1 ON e.emp_class = c1.class_id
            LEFT JOIN atrbmst   c2 ON e.emp_attribute = c2.atrb_id
            LEFT JOIN deptmst   c3 ON e.emp_dept = c3.dept_id
            LEFT JOIN classroom c4 ON e.emp_room = c4.room_id
            , inci_profile_use ipu
            WHERE
                e.emp_id = :emp_id
        ");
        $res = $sth->execute(array('emp_id' => $emp_id));
        $row = $res->fetchRow(MDB2_FETCHMODE_ASSOC);

        // 部署名
        $class_name = '';
        if ($row['class_flg'] === 't') {
            $class_name .= $row["class_nm"];
        }
        if ($row['attribute_flg'] === 't') {
            $class_name .= $row["atrb_nm"];
        }
        if ($row['dept_flg'] === 't') {
            $class_name .= $row["dept_nm"];
        }
        if ($row['room_flg'] === 't') {
            $class_name .= $row["room_nm"];
        }

        // 認定資格
        $qualification = preg_replace("/\r?\n/", "\n", $row["qualification"]);

        // 経験年数
        $experience = get_experience($row);

        return array(
            'emp_name' => $row['emp_name'],
            'class_nm' => $class_name,
            'qualification' => $qualification,
            'exp_year' => $experience["exp_year"],
            'exp_month' => $experience["exp_month"],
            'dept_year' => $experience["dept_year"],
            'dept_month' => $experience["dept_month"],
            'exp_code' => $row['exp_code'],
        );
    }

    /**
     * _classname
     */
    function _classname() {
        $this->_classname = $this->db->queryRow('SELECT * FROM classname', null, MDB2_FETCHMODE_ASSOC);
    }

    /**
     * 組織1の名称
     * @return 組織1の名称
     */
    function class_name() {
        if (!$this->_classname) {
            $this->_classname();
        }
        return $this->_classname['class_nm'];
    }

    /**
     * 組織2の名称
     * @return 組織2の名称
     */
    function atrb_name() {
        if (!$this->_classname) {
            $this->_classname();
        }
        return $this->_classname['atrb_nm'];
    }

    /**
     * 組織3の名称
     * @return 組織3の名称
     */
    function dept_name() {
        if (!$this->_classname) {
            $this->_classname();
        }
        return $this->_classname['dept_nm'];
    }

    /**
     * 組織4の名称
     * @return 組織4の名称
     */
    function room_name() {
        if (!$this->_classname) {
            $this->_classname();
        }
        return $this->_classname['room_nm'];
    }

    /**
     * 組織階層数
     * @return 組織階層数
     */
    function class_count() {
        if (!$this->_classname) {
            $this->_classname();
        }
        return $this->_classname['class_cnt'];
    }

}
