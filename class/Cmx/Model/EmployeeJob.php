<?php

require_once('Cmx.php');
require_once('Cmx/Core/Model.php');

/**
 * CoMedix：職種
 *
 * @link http://trac.medi-system.co.jp/trac/comedix/wiki/tables/jobmst
 */
class Cmx_EmployeeJob extends Model {

    var $_row;

    /**
     * コンストラクタ
     *
     * @param Arrayならrowデータと判断、そうでなければ職員IDとして処理
     */
    function Cmx_EmployeeJob($param = null) {
        parent::connectDB();

        if (is_array($row)) {
            $this->_init($row);
        }
        else if (!is_null($row)) {
            $this->_select($row);
        }
    }

    // _init
    function _init($row) {
        $this->_row = $row;
    }

    // _select
    function _select($row) {
        $sth = $this->db->prepare("
            SELECT *
            FROM jobmst
            WHERE job_id= ?
        ");
        $res = $sth->execute($row);
        $this->_init($res->fetchRow(MDB2_FETCHMODE_ASSOC));
    }

    /**
     * リスト
     * @return array
     */
    function lists() {
        $res = $this->db->query("SELECT *, job_id as id, job_nm as name FROM jobmst WHERE NOT job_del_flg ORDER BY order_no, job_id");

        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[$row['id']] = $row;
        }
        return $list;
    }

    /**
     * 職種マスタID
     * @return 職種マスタID
     */
    function job_id() {
        return $this->_getter(__FUNCTION__);
    }

    /**
     * 職種名
     * @return 職種名
     */
    function job_name() {
        return $this->_row["job_nm"];
    }

    /**
     * 連携キー
     *
     * @return 連携キー
     */
    function link_key() {
        return $this->_row["link_key"];
    }

}
