<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';

/**
 * タイムカード：残業理由マスタ
 */
class Cmx_Timecard_OvertimeReason extends Model
{

    var $_row;
    var $_hash;

    /**
     * コンストラクタ
     * @param str $row
     */
    function Cmx_Timecard_OvertimeReason($row = null)
    {
        parent::connectDB();
        $this->_hash = array();

        if (is_array($row)) {
            $this->_init($row);
        }
        else if (!is_null($row)) {
            $this->_select($row);
        }
    }

    /**
     * _init
     * @param str $row
     */
    function _init($row)
    {
        $this->_row = $row;
    }

    /**
     * _select
     * @param str $reason_id
     */
    function _select($reason_id)
    {
        $sth = $this->db->prepare("SELECT * FROM ovtmrsn WHERE reason_id = ? AND NOT del_flg");
        $res = $sth->execute($reason_id);
        $this->_init($res->fetchRow(MDB2_FETCHMODE_ASSOC));
    }

    /**
     * 理由一覧
     * @param type $tmcd_group_id
     * @return array
     */
    function lists($tmcd_group_id = null)
    {
        if (is_null($tmcd_group_id)){
            $res = $this->db->query("SELECT * FROM ovtmrsn WHERE NOT del_flg ORDER BY sort");
        }
        else {
            $sth = $this->db->prepare("SELECT * FROM ovtmrsn WHERE NOT del_flg AND ( tmcd_group_id IS NULL OR tmcd_group_id = ? ) ORDER BY sort");
            $res = $sth->execute($tmcd_group_id);
        }

        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            array_push($list, $row);
            $this->_hash[$row['reason_id']] = $row;
        }
        return $list;
    }

    /**
     * ハッシュリスト
     * @return arr ハッシュリスト
     */
    function hash()
    {
        if (empty($this->_hash)) {
            $this->lists();
        }
        return $this->_hash;
    }

    /**
     * 残業理由
     * @param str $id 残業理由ID
     * @param str $reason 残業理由その他
     * @return string 残業理由
     */
    function reason($id, $reason)
    {
        // 一覧を返す
        if (empty($this->_hash)) {
            $this->lists();
        }

        if ($id and $this->_hash[$id]) {
            switch ($this->_hash[$id]['reason']) {
                case '緊急呼出　手術':
                case '緊急呼出　内視鏡':
                case '緊急呼出　カテ':
                    return $this->_hash[$id]['reason'] . ": " . $reason;

                default:
                    return $this->_hash[$id]['reason'];
            }
        }
        if ($reason) {
            return $reason;
        }
        return '';
    }

}
