<?php

require_once('Cmx.php');
require_once('Cmx/Core/Model.php');

/**
 * タイムカード：残業
 */
class Cmx_Timecard_Overtime extends Model
{

    var $_row;
    var $_hash;

    /**
     * コンストラクタ
     * @param str $row
     */
    function Cmx_Timecard_Overtime()
    {
        //
    }

    /**
     * 文字列を分に直す
     * @param type $str
     * @return type
     */
    private function to_min($str)
    {
        $hour = (int)substr($str, 0, 2);
        $min = ($hour * 60) + (int)substr($str, 2, 2);
        return $min;
    }

    /**
     * (当日)残業時間計算
     *
     * @param str $start        開始時間
     * @param int $start_flg    開始翌日フラグ
     * @param str $end          終了時間
     * @param int $end_flg      終了翌日フラグ
     * @return int 残業時間(分)
     */
    function overtime($start, $start_flg, $end, $end_flg, $r_start, $r_start_flg, $r_end, $r_end_flg)
    {
        list($over_min1, $over_min2) = $this->to_today_time($start, $start_flg, $end, $end_flg);
        list($rest_min1, $rest_min2) = $this->to_today_time($r_start, $r_start_flg, $r_end, $r_end_flg);

        return array($over_min1 - $rest_min1, $over_min2 - $rest_min2, $rest_min1, $rest_min2);
    }

    /**
     * (翌日)残業時間計算
     *
     * @param str $start        開始時間
     * @param int $start_flg    開始翌日フラグ
     * @param str $end          終了時間
     * @param int $end_flg      終了翌日フラグ
     * @return int 残業時間(分)
     */
    function overtime_nextday($start, $start_flg, $end, $end_flg, $r_start, $r_start_flg, $r_end, $r_end_flg)
    {
        list($over_min1, $over_min2) = $this->to_nextday_time($start, $start_flg, $end, $end_flg);
        list($rest_min1, $rest_min2) = $this->to_nextday_time($r_start, $r_start_flg, $r_end, $r_end_flg);

        return array($over_min1 - $rest_min1, $over_min2 - $rest_min2, $rest_min1, $rest_min2);
    }

    /**
     * NN:NN〜24:00までの時間を割り出す
     *
     * @param str $start        開始時間
     * @param int $start_flg    開始翌日フラグ
     * @param str $end          終了時間
     * @param int $end_flg      終了翌日フラグ
     * @return array 通常帯の時間(分), 深夜帯の時間(分)
     */
    function to_today_time($start, $start_flg, $end, $end_flg)
    {
        if ($start_flg) {
            return array(0, 0);
        }

        //開始時間
        $start_min = $this->to_min($start);

        //終了時間
        if ($end_flg) {
            $end_min = 1440; // 24 * 60
        }
        else {
            $end_min = $this->to_min($end);
        }

        // 5:00(300)以前
        if ($end_min <= 300) {
            return array(0, $end_min - $start_min);
        }

        // 22:00(1320)以降
        if (1320 <= $start_min) {
            return array(0, $end_min - $start_min);
        }

        // 5:00〜22:00の間
        if (300 <= $start_min && $end_min <= 1320) {
            return array($end_min - $start_min, 0);
        }

        // 5:00(300)をまたぐ
        if ($start_min < 300 && 300 < $end_min) {
            $min1 = $end_min - 300;
            $min2 = 300 - $start_min;
            return array($min1, $min2);
        }

        // 22:00(1320)をまたぐ
        if ($start_min < 1320 && 1320 < $end_min) {
            $min1 = 1320 - $start_min;
            $min2 = $end_min - 1320;
            return array($min1, $min2);
        }

        // 5:00(300)〜22:00(1320)をまたぐ
        if ($start_min < 300 && 1320 < $end_min) {
            $min2 = (300 - $start_min) + ($end_min - 1320);
            return array(1020, $min2);
        }
    }

    /**
     * 00:00〜NN:NNまでの時間を割り出す
     *
     * @param str $start        開始時間
     * @param int $start_flg    開始翌日フラグ
     * @param str $end          終了時間
     * @param int $end_flg      終了翌日フラグ
     * @return array 通常帯の時間(分), 深夜帯の時間(分)
     */
    function to_nextday_time($start, $start_flg, $end, $end_flg)
    {
        if (!$end_flg) {
            return array(0, 0);
        }

        //開始時間
        if ($start_flg) {
            $start_min = $this->to_min($start) + 1440;
        }
        else {
            $start_min = 1440; // 24 * 60;
        }

        //終了時間
        $end_min = $this->to_min($end) + 1440;

        // 翌5:00(1740)以前
        if ($end_min <= 1740) {
            return array(0, $end_min - $start_min);
        }

        // 翌22:00(2760)以降
        if (2760 <= $start_min) {
            return array(0, $end_min - $start_min);
        }

        // 翌5:00〜翌22:00の間
        if (1740 <= $start_min && $end_min <= 2760) {
            return array($end_min - $start_min, 0);
        }

        // 翌5:00(1740)をまたぐ
        if ($start_min < 1740 && 1740 < $end_min) {
            $min1 = $end_min - 1740;
            $min2 = 1740 - $start_min;
            return array($min1, $min2);
        }

        // 翌22:00(2760)をまたぐ
        if ($start_min < 2760 && 2760 < $end_min) {
            $min1 = 2760 - $start_min;
            $min2 = $end_min - 2760;
            return array($min1, $min2);
        }

        // 翌5:00(1740)〜翌22:00(2760)をまたぐ
        if ($start_min < 1740 && 2760 < $end_min) {
            $min2 = (1740 - $start_min) + ($end_min - 2760);
            return array(1020, $min2);
        }
    }

    function overtime_details($worktime, $overtime1, $overtime2, $kaitori, $row)
    {
        // 休日
        // 休暇: 公休(kind_flag=4)
        // 買取日部署で、買取日(type=8)
        // 買取日部署以外で、日祝(type=3)買取日(type=8)
        // 日祝宿直(PA)
        // 創立記念日: 9/20
        // 年末年始: 12/30-1/4
        if (
            (
            (int)$row['kind_flag'] === 4 ||
            ($kaitori && $row['type'] === '8') ||
            (!$kaitori && ((int)$row['type'] === 3 || $row['type'] === '8')) ||
            ((int)$row['kind_flag'] === 99 && (int)$row['type'] === 3) ||
            $row['code'] === 'PA' ||
            in_array(substr($row['date'], 4, 4), array('1230', '1231', '0101', '0102', '0103', '0104'))
            ) &&
            !$row['not135'] &&
            !$row['not125']
        ) {
            // 135/100
            $over135 = $overtime1 + $overtime2;
        }

        // 土曜
        // 買取部署以外で、土曜(week=6)
        // 休暇: 週休(kind_flag=2)
        // 土曜(week=6)の残業(OH)
        // 土曜有休(ZZ)
        else if (
            (!$kaitori && (int)$row['week'] === 6 && !$row['not125']) ||
            (int)$row['kind_flag'] === 2 ||
            ((int)$row['kind_flag'] === 99 && (int)$row['week'] === 6) ||
            $row['code'] === 'ZZ' ||
            ($row['code'] === 'OH' && (int)$row['week'] === 6) ||
            (in_array($row['code'], array('ON', 'OO', 'OP', 'OQ', 'OR', 'OS', 'OT', 'OI', 'OJ', 'OK', 'OL')) && (int)$row['week'] === 6)) {
            // 125/100
            $over125 = $overtime1 + $overtime2;
        }

        // 平日
        else {
            // 有休(1)、夏休(3)、特別(5)
            if ((int)$row['kind_flag'] === 1 || (int)$row['kind_flag'] === 3 || (int)$row['kind_flag'] === 5) {
                $worktime = 450;
            }

            // 100/100
            $overtime = $overtime1 + $overtime2;

            // 8時間未満
            if ($worktime < 480) {
                $time100 = 480 - $worktime;
                if ($time100 < $overtime) {
                    $over100 = $time100;
                    $overtime -= $over100;
                }
                else {
                    $over100 = $overtime;
                    $overtime = 0;
                }
            }
            else {
                $over100 = 0;
            }

            // 125/100
            $over125 = $overtime;
        }

        // 25/100
        $over25 = $overtime2;

        return array($over100, $over125, $over135, $over25);
    }

    /**
     * 大阪警察病院対応
     * 時間外 100/100 計算
     *
     * @param str $row
     * @param str $start
     * @param int $start_flg
     * @param str $end
     * @param int $end_flg
     */
    function over100($row, $start, $start_flg, $end, $end_flg)
    {
        $start_hour = (int)substr($start, 0, 2);
        if ($start_flg) {
            $start_hour += 24;
        }
        $start_min = ($start_hour * 60) + (int)substr($start, 2, 2);
        $end_hour = (int)substr($end, 0, 2);
        if ($end_flg) {
            $end_hour += 24;
        }
        $end_min = ($end_hour * 60) + (int)substr($end, 2, 2);

        // 7.5時間の記号かどうか(夜勤明けを除く)
        if ($row['workminutes'] === '450' and $row['sfc_code'] !== 'O5') {
            return ($end_min - $start_min >= 30) ? 30 : $end_min - $start_min;
        }

        // 有休、夏休
        else if (in_array($row['sfc_code'], array('OD', 'OE', 'OF', 'OG', 'OX', 'OY'))) {
            return ($end_min - $start_min >= 30) ? 30 : $end_min - $start_min;
        }

        // 中IIは2.5時間
        else if ($row['sfc_code'] === 'PB') {
            return ($end_min - $start_min >= 150) ? 150 : $end_min - $start_min;
        }

        return 0;
    }

    /**
     * 大阪警察病院対応
     * 時間外 125/100 計算
     *
     * @param str $row
     * @param str $start
     * @param int $start_flg
     * @param str $end
     * @param int $end_flg
     */
    function over125($row, $start, $start_flg, $end, $end_flg)
    {
        $start_hour = (int)substr($start, 0, 2);
        if ($start_flg) {
            $start_hour += 24;
        }
        $start_min = ($start_hour * 60) + (int)substr($start, 2, 2);
        $end_hour = (int)substr($end, 0, 2);
        if ($end_flg) {
            $end_hour += 24;
        }
        $end_min = ($end_hour * 60) + (int)substr($end, 2, 2);

        // 7.5時間の記号かどうか(夜勤明けを除く)
        if ($row['workminutes'] === '450' and $row['sfc_code'] !== 'O5') {
            return ($end_min - $start_min >= 30) ? $end_min - $start_min - 30 : 0;
        }

        // 有休、夏休
        else if (in_array($row['sfc_code'], array('OD', 'OE', 'OF', 'OG', 'OX', 'OY'))) {
            return ($end_min - $start_min >= 30) ? $end_min - $start_min - 30 : 0;
        }

        // 中IIは2.5時間を超えた分
        else if ($row['sfc_code'] === 'PB') {
            return ($end_min - $start_min >= 150) ? $end_min - $start_min - 150 : 0;
        }
        else {
            return $end_min - $start_min;
        }
        return 0;
    }

    /**
     * 大阪警察病院対応
     * 時間外 25/100 計算
     *
     * @param str $start
     * @param int $start_flg
     * @param str $end
     * @param int $end_flg
     */
    function over025($start, $start_flg, $end, $end_flg)
    {
        $start_hour = (int)substr($start, 0, 2);
        if ($start_flg) {
            $start_hour += 24;
        }
        $start_min = ($start_hour * 60) + (int)substr($start, 2, 2);
        $end_hour = (int)substr($end, 0, 2);
        if ($end_flg) {
            $end_hour += 24;
        }
        $end_min = ($end_hour * 60) + (int)substr($end, 2, 2);

        // 22:00-05:00の間に含まれるか？
        if (1320 <= $end_min and $start_min <= 1740) {
            $s = $start_min > 1320 ? $start_min : 1320;
            $e = $end_min < 1740 ? $end_min : 1740;
            return $e - $s;
        }
        return 0;
    }

}
