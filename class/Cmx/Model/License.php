<?php

require_once('Cmx.php');
require_once('Cmx/Core/Model.php');

/**
 * CoMedix：ライセンス
 *
 * @link http://trac.medi-system.co.jp/trac/comedix/wiki/tables/license
 */
class Cmx_License extends Model {

    var $_row;

    /**
     * コンストラクタ
     */
    function Cmx_License() {
        parent::connectDB();
        $this->_select();
    }

    /**
     * _select
     */
    function _select() {
        $this->_row = $this->db->queryRow('SELECT * FROM license', null, MDB2_FETCHMODE_ASSOC);
    }

    /**
     * 勤務シフト作成：最大ユーザ数確認
     *
     * @param $count ユーザ数
     * @return true:OK false:NG
     */
    function is_shift_count($count) {
        return $this->_row['lcs_shift_emps'] >= $count ? true : false;
    }

    /**
     * 勤務シフト作成：現在のユーザ数
     *
     * @return ユーザ数
     */
    function shift_count() {
        $count = $this->db->queryOne('SELECT COUNT(*) FROM duty_shift_staff');
        return $count ? $count : 0;
    }
}
