<?php

require_once('Cmx.php');
require_once('Cmx/Core/Model.php');

/**
 * CoMedix：職員
 *
 * @link http://trac.medi-system.co.jp/trac/comedix/wiki/tables/empmst
 * @link http://trac.medi-system.co.jp/trac/comedix/wiki/tables/login
 * @link http://trac.medi-system.co.jp/trac/comedix/wiki/tables/authmst
 * @link http://trac.medi-system.co.jp/trac/comedix/wiki/tables/empcond
 * @link http://trac.medi-system.co.jp/trac/comedix/wiki/tables/jobmst
 * @link http://trac.medi-system.co.jp/trac/comedix/wiki/tables/stmst
 * @link http://trac.medi-system.co.jp/trac/comedix/wiki/tables/classmst
 * @link http://trac.medi-system.co.jp/trac/comedix/wiki/tables/atrbmst
 * @link http://trac.medi-system.co.jp/trac/comedix/wiki/tables/deptmst
 * @link http://trac.medi-system.co.jp/trac/comedix/wiki/tables/classroom
 */
class Cmx_Employee extends Model
{

    var $_row;
    var $_select_sth;

    /**
     * コンストラクタ
     *
     * @param Arrayならrowデータと判断、そうでなければ職員IDとして処理
     */
    function Cmx_Employee($param = null)
    {
        parent::connectDB();

        // SELECT Prepare
        $this->set_select_sth();

        if (is_array($param)) {
            $this->_init($param);
        }
        else if (!is_null($param)) {
            $this->_select($param);
        }
    }

    // set_select_sth
    function set_select_sth()
    {
        $this->_select_sth = $this->db->prepare("
            SELECT
                *,
                jobmst.link_key as job_link_key,
                stmst.link_key as st_link_key,
                classmst.link_key as class_link_key,
                atrbmst.link_key as atrb_link_key,
                deptmst.link_key1 as dept_link_key,
                deptmst.link_key2 as dept_link_key2,
                deptmst.link_key3 as dept_link_key3,
                classroom.link_key as room_link_key
            FROM empmst
            JOIN login   USING (emp_id)
            JOIN authmst USING (emp_id)
            LEFT JOIN empcond USING (emp_id)
            LEFT JOIN jobmst ON jobmst.job_id=emp_job
            LEFT JOIN stmst ON stmst.st_id=emp_st
            LEFT JOIN classmst ON classmst.class_id=emp_class
            LEFT JOIN atrbmst ON atrbmst.atrb_id=emp_attribute
            LEFT JOIN deptmst ON deptmst.dept_id=emp_dept
            LEFT JOIN classroom ON classroom.room_id=emp_room
            WHERE empmst.emp_id= ?
        ");
    }

    // _select
    function _select($emp_id)
    {
        $res = $this->_select_sth->execute($emp_id);
        $this->_init($res->fetchRow(MDB2_FETCHMODE_ASSOC));
    }

    // _init
    function _init($row)
    {
        $this->_row = $row;
    }

    /**
     * 職員ID
     * @return 職員ID
     */
    function emp_id()
    {
        return $this->_getter(__FUNCTION__);
    }

    /**
     * 職員ID(外部)
     * @return 職員ID(外部)
     */
    function emp_personal_id()
    {
        return $this->_getter(__FUNCTION__);
    }

    /**
     * 職員ログインID
     * @return 職員ログインID
     */
    function emp_login_id()
    {
        return $this->_getter(__FUNCTION__);
    }

    /**
     * 性別
     * @return 0: 不明 1: 男性 2:女性
     */
    function emp_sex()
    {
        return $this->_getter(__FUNCTION__);
    }

    /**
     * 職員の氏名
     * 「姓」「名」を半角スペースでJOINしたものを返す
     *
     * @return 職員の氏名
     */
    function emp_name()
    {
        return $this->_row["emp_lt_nm"] . " " . $this->_row["emp_ft_nm"];
    }

    /**
     * 職員の氏名かな
     * 「姓かな」「名かな」を半角スペースでJOINしたものを返す
     *
     * @return 職員の氏名かな
     */
    function emp_kana()
    {
        return $this->_row["emp_kn_lt_nm"] . " " . $this->_row["emp_kn_ft_nm"];
    }

    /**
     * 職種ID
     *
     * @return 職種ID
     */
    function job_id()
    {
        return $this->_row["emp_job"];
    }

    /**
     * 職種
     *
     * @return 職種名
     */
    function job_name()
    {
        return $this->_row["job_nm"];
    }

    /**
     * 職種コード
     * 職種：外部連携キーを返す
     *
     * @return 職種：外部連携キー
     */
    function job_code()
    {
        return $this->_row["job_link_key"];
    }

    /**
     * 役職（職位）
     *
     * @return 役職名
     */
    function st_name()
    {
        return $this->_row["st_nm"];
    }

    /**
     * 役職（職位）ID
     *
     * @return 役職（職位）ID
     */
    function st_id()
    {
        return $this->_row["emp_st"];
    }

    /**
     * 役職（職位）コード
     * 役職：外部連携キーを返す
     *
     * @return 役職：外部連携キー
     */
    function st_code()
    {
        return $this->_row["st_link_key"];
    }

    /**
     * 組織1ID
     *
     * @return 組織1
     */
    function class_id()
    {
        return $this->_row["emp_class"];
    }

    /**
     * 組織1
     *
     * @return 組織1
     */
    function class_name()
    {
        return $this->_row["class_nm"];
    }

    /**
     * 組織名全部
     *
     * @return 組織組織名
     */
    function class_full_name()
    {
        $class = array(
            $this->_row["class_nm"],
            $this->_row["atrb_nm"],
            $this->_row["dept_nm"],
        );
        if ($this->_row["room_nm"]) {
            $class[] = $this->_row["room_nm"];
        }

        return implode(' > ', $class);
    }

    /**
     * 組織1コード
     * 組織1：外部連携キーを返す
     *
     * @return 組織1：外部連携キー
     */
    function class_code()
    {
        return $this->_row["class_link_key"];
    }

    /**
     * 組織2ID
     *
     * @return 組織2
     */
    function atrb_id()
    {
        return $this->_row["emp_attribute"];
    }

    /**
     * 組織2
     *
     * @return 組織2
     */
    function atrb_name()
    {
        return $this->_row["atrb_nm"];
    }

    /**
     * 組織2コード
     * 組織2：外部連携キーを返す
     *
     * @return 組織2：外部連携キー
     */
    function atrb_code()
    {
        return $this->_row["atrb_link_key"];
    }

    /**
     * 組織3
     *
     * @return 組織3
     */
    function dept_name()
    {
        return $this->_row["dept_nm"];
    }

    /**
     * 組織3ID
     *
     * @return 組織3
     */
    function dept_id()
    {
        return $this->_row["emp_dept"];
    }

    /**
     * 組織3コード1
     * 組織3：外部連携キー1を返す
     *
     * @return 組織3：外部連携キー1
     */
    function dept_code()
    {
        return $this->_row["dept_link_key"];
    }

    /**
     * 組織3コード2
     * 組織3：外部連携キー2を返す
     *
     * @return 組織3：外部連携キー2
     */
    function dept_code2()
    {
        return $this->_row["dept_link_key2"];
    }

    /**
     * 組織3コード3
     * 組織3：外部連携キー3を返す
     *
     * @return 組織3：外部連携キー3
     */
    function dept_code3()
    {
        return $this->_row["dept_link_key3"];
    }

    /**
     * 組織4ID
     *
     * @return 組織4
     */
    function room_id()
    {
        return $this->_row["emp_room"];
    }

    /**
     * 組織4
     *
     * @return 組織4
     */
    function room_name()
    {
        return $this->_row["room_nm"];
    }

    /**
     * 組織4コード
     * 組織4：外部連携キーを返す
     *
     * @return 組織4：外部連携キー
     */
    function room_code()
    {
        return $this->_row["room_link_key"];
    }

    /**
     * 誕生日
     * YYYY-MM-DDの形式で返す
     *
     * @return 誕生日
     */
    function birth_date()
    {
        return preg_replace("/(\d\d\d\d)(\d\d)(\d\d)/", "$1-$2-$3", $this->_row["emp_birth"]);
    }

    /**
     * 入職日
     * YYYY-MM-DDの形式で返す
     *
     * @return 入職日
     */
    function join_date()
    {
        return $this->_row["emp_join"];
    }

    /**
     * 退職日
     * YYYY-MM-DDの形式で返す
     *
     * @return 退職日
     */
    function retire_date()
    {
        return $this->_row["emp_retire"];
    }

    /**
     * 指定日に在職しているか
     * @param str $date 確認日付(YYYYMMDD)
     * @return bool TRUE:在職している FALSE:在職していない
     */
    function is_employed($date)
    {
        return (
            !empty($this->_row["emp_join"]) and $date < $this->_row["emp_join"] or ! empty($this->_row["emp_retire"]) and $this->_row["emp_retire"] < $date
            ) ? false : true;
    }

    /**
     * 勤続年数
     * 入職日から割り出した勤続年数を返す
     *
     * @return 勤続年数
     */
    function duty_years()
    {
        if (empty($this->_row["emp_join"])) {
            return '入職日未定';
        }
        if (date('Ymd') <= $this->_row["emp_join"]) {
            return '0年0ヶ月';
        }
        if (date('m') < (int)substr($this->_row["emp_join"], 4, 2)) {
            return sprintf('%d年%dヶ月', date('Y') - (int)substr($this->_row["emp_join"], 0, 4) - 1, date('m') - (int)substr($this->_row["emp_join"], 4, 2) + 12);
        }
        else {
            return sprintf('%d年%dヶ月', date('Y') - (int)substr($this->_row["emp_join"], 0, 4), date('m') - (int)substr($this->_row["emp_join"], 4, 2));
        }
    }

    /**
     * 支給区分
     *
     * @return 支給区分
     */
    function wage()
    {
        return $this->_row["wage"];
    }

    /**
     * 支給区分(名称)
     *
     * @return '月給制' or '日給制' or '時間給制' or '年俸制'
     */
    function wage_name()
    {
        switch ($this->_row["wage"]) {
            case 1:
                return '月給制';
                break;
            case 3:
                return '日給制';
                break;
            case 4:
                return '時間給制';
                break;
            case 5:
                return '年俸制';
                break;
        }
    }

    /**
     * 勤務形態
     *
     * @return 1: 常勤 2:非常勤
     */
    function duty_form()
    {
        return $this->_row["duty_form"];
    }

    /**
     * 勤務形態(名称)
     *
     * @return '常勤' or '非常勤'
     */
    function duty_form_name()
    {
        switch ($this->_row["duty_form"]) {
            case 1:
                return '常勤';
                break;
            case 2:
                return '非常勤';
                break;
            case 3:
                return '短時間正職員';
                break;
        }
    }

    /**
     * 勤務シフト作成：ユーザ権限
     * @return true:あり false:なし
     */
    function is_shift()
    {
        return $this->_row["emp_shift2_flg"] === 't';
    }

    /**
     * 勤務シフト作成：管理者権限
     * @return true:あり false:なし
     */
    function is_shift_admin()
    {
        return $this->_row["emp_shift2adm_flg"] === 't';
    }

    /**
     * 職員履歴リスト
     * 履歴基準日の時点での職員の氏名、役職、職種、所属のリストを返す
     *
     * @param $date 履歴基準日
     * @return 職員リスト
     */
    function history($date)
    {
        $date .= ' 00:00:00';
        $sql = "
        SELECT
            BD.EMP_ID,
            NH.LT_NM||' '||NH.FT_NM as name,
            NH.KN_LT_NM||' '||NH.KN_FT_NM as kana ,
            CH.CLASS_ID,
            CH.ATRB_ID,
            CH.DEPT_ID,
            CH.ROOM_ID,
            SH.ST_ID,
            JH.JOB_ID,
            jobmst.job_nm as job_name,
            stmst.st_nm as st_name,
            classmst.class_nm as class_name,
            atrbmst.atrb_nm as atrb_name,
            deptmst.dept_nm as dept_name,
            classroom.room_nm as room_name,
            jobmst.link_key as job_link_key,
            stmst.link_key as st_link_key,
            classmst.link_key as class_link_key,
            atrbmst.link_key as atrb_link_key,
            deptmst.link_key1 as dept_link_key,
            deptmst.link_key2 as dept_link_key2,
            deptmst.link_key3 as dept_link_key3,
            classroom.link_key as room_link_key
        FROM (
            SELECT EMP_ID FROM NAME_HISTORY  WHERE HISTDATE >= :date UNION
            SELECT EMP_ID FROM CLASS_HISTORY WHERE HISTDATE >= :date UNION
            SELECT EMP_ID FROM ST_HISTORY    WHERE HISTDATE >= :date UNION
            SELECT EMP_ID FROM JOB_HISTORY   WHERE HISTDATE >= :date
        ) BD
        LEFT JOIN (
            SELECT A.EMP_ID AS EMP_ID1
                , A.HISTDATE AS HISTDATE1
                , A.LT_NM
                , A.FT_NM
                , A.KN_LT_NM
                , A.KN_FT_NM
            FROM NAME_HISTORY A, (
                SELECT EMP_ID, MIN(HISTDATE) AS HISTDATE FROM NAME_HISTORY
                WHERE HISTDATE >= :date GROUP BY EMP_ID
            ) B
            WHERE A.EMP_ID = B.EMP_ID AND A.HISTDATE = B.HISTDATE
        ) NH ON BD.EMP_ID=NH.EMP_ID1
        LEFT JOIN (
            SELECT A.EMP_ID AS EMP_ID2
                , A.HISTDATE AS HISTDATE2
                , A.CLASS_ID
                , A.ATRB_ID
                , A.DEPT_ID
                , A.ROOM_ID
            FROM CLASS_HISTORY A, (
                SELECT EMP_ID, MIN(HISTDATE) AS HISTDATE FROM CLASS_HISTORY
                WHERE HISTDATE >= :date GROUP BY EMP_ID
            ) B
            WHERE A.EMP_ID   = B.EMP_ID AND A.HISTDATE = B.HISTDATE
        ) CH ON BD.EMP_ID=CH.EMP_ID2
        LEFT JOIN (
            SELECT A.EMP_ID AS EMP_ID3
                , A.HISTDATE AS HISTDATE3
                , A.ST_ID
            FROM ST_HISTORY A, (
                SELECT EMP_ID, MIN(HISTDATE) AS HISTDATE FROM ST_HISTORY
                WHERE HISTDATE >= :date GROUP BY EMP_ID
            ) B
            WHERE A.EMP_ID = B.EMP_ID AND A.HISTDATE = B.HISTDATE
        ) SH ON BD.EMP_ID=SH.EMP_ID3
        LEFT JOIN (
            SELECT A.EMP_ID AS EMP_ID4
                , A.HISTDATE AS HISTDATE4
                , A.JOB_ID
            FROM JOB_HISTORY A, (
                SELECT EMP_ID, MIN(HISTDATE) AS HISTDATE FROM JOB_HISTORY
                WHERE HISTDATE >= :date GROUP BY EMP_ID
            ) B
            WHERE A.EMP_ID   = B.EMP_ID AND A.HISTDATE = B.HISTDATE
        ) JH ON BD.EMP_ID=JH.EMP_ID4
        LEFT JOIN classmst USING (class_id)
        LEFT JOIN atrbmst USING (atrb_id)
        LEFT JOIN deptmst USING (dept_id)
        LEFT JOIN classroom USING (room_id)
        LEFT JOIN jobmst USING (job_id)
        LEFT JOIN stmst USING (st_id)
        ";

        $sth = $this->db->prepare($sql);
        $res = $sth->execute(array('date' => $date));

        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            if ($row['name']) {
                $list[$row["emp_id"]]['name'] = $row['name'];
                $list[$row["emp_id"]]['kana'] = $row['kana'];
            }
            if ($row['class_id']) {
                $list[$row["emp_id"]]['class_id'] = $row['class_id'];
                $list[$row["emp_id"]]['class_name'] = $row['class_name'];
                $list[$row["emp_id"]]['class_code'] = $row['class_link_key'];
                $list[$row["emp_id"]]['atrb_id'] = $row['atrb_id'];
                $list[$row["emp_id"]]['atrb_name'] = $row['atrb_name'];
                $list[$row["emp_id"]]['atrb_code'] = $row['atrb_link_key'];
                $list[$row["emp_id"]]['dept_id'] = $row['dept_id'];
                $list[$row["emp_id"]]['dept_name'] = $row['dept_name'];
                $list[$row["emp_id"]]['dept_code'] = $row['dept_link_key'];
                $list[$row["emp_id"]]['dept_code2'] = $row['dept_link_key2'];
                $list[$row["emp_id"]]['dept_code3'] = $row['dept_link_key3'];
                $list[$row["emp_id"]]['room_id'] = $row['room_id'];
                $list[$row["emp_id"]]['room_name'] = $row['room_name'];
                $list[$row["emp_id"]]['room_code'] = $row['room_link_key'];
            }
            if ($row['job_id']) {
                $list[$row["emp_id"]]['job_id'] = $row['job_id'];
                $list[$row["emp_id"]]['job_name'] = $row['job_name'];
                $list[$row["emp_id"]]['job_code'] = $row['job_link_key'];
            }
            if ($row['st_id']) {
                $list[$row["emp_id"]]['st_id'] = $row['st_id'];
                $list[$row["emp_id"]]['st_name'] = $row['st_name'];
                $list[$row["emp_id"]]['st_code'] = $row['st_link_key'];
            }
        }
        return $list;
    }

    /**
     * 給与支給区分履歴リスト
     * 履歴基準日の時点での給与支給区分のリストを返す
     *
     * @param $date 履歴基準日
     * @return 職員リスト
     */
    function wage_history($date)
    {
        $date .= ' 00:00:00';
        $sql = "
            SELECT
                emp_id,
                wage
            FROM empcond_wage_history h
            WHERE histdate = (
                SELECT MIN(histdate) FROM empcond_wage_history h2 WHERE h.emp_id=h2.emp_id AND histdate >= :date GROUP BY emp_id
            )
        ";

        $sth = $this->db->prepare($sql);
        $res = $sth->execute(array('date' => $date));

        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[$row["emp_id"]] = $row['wage'];
        }
        return $list;
    }

    /**
     * 有休残日数取得
     * @param $emp_id 職員ID
     * @param $date 取得基準日
     * @return 最終付与日時点の付与日数
     * @return 最終付与日から基準日までの取得数
     */
    function paid_holiday($emp_id, $date)
    {
        if ($date == "") {
            return array(0, 0);
        }

        // emppaid
        $sql_paid = "
            SELECT
                days1,
                days2,
                adjust_day,
                YEAR || PAID_HOL_ADD_MMDD AS paid_date,
                i.curr_use AS used
            FROM EMPPAID p
            JOIN empmst e USING (emp_id)
            LEFT JOIN timecard_paid_hol_import i ON (i.emp_personal_id=e.emp_personal_id AND REPLACE(i.last_add_date,'/','')=(p.year||p.paid_hol_add_mmdd))
            WHERE EMP_ID = ?
            AND YEAR || PAID_HOL_ADD_MMDD <= ?
            ORDER BY YEAR || PAID_HOL_ADD_MMDD DESC
            OFFSET 0 LIMIT 1
            ";

        $emppaid = $this->db->extended->getRow(
            $sql_paid, null, array($emp_id, $date), array('text', 'text'), MDB2_FETCHMODE_ASSOC
        );
        $paid_holiday = (float)$emppaid['days1'] + (float)$emppaid['days2'] + (float)$emppaid['adjust_day'];
        $paid_honnen = (float)$emppaid['days2'] + (float)$emppaid['adjust_day'];
        $paid_kurikoshi = (float)$emppaid['days1'];
        $paid_date = $emppaid["paid_date"] ? $emppaid["paid_date"] : '19000101';

        // atdbkrslt
        $sql_rslt = "
            SELECT
                SUM(TO_NUMBER(holiday_count,'999.99')) AS holiday
            FROM (
                SELECT
                    c.date,
                    CASE WHEN r.pattern IS NULL OR r.pattern='0' THEN p.pattern ELSE r.pattern END AS pattern,
                    CASE WHEN r.pattern IS NULL OR r.pattern='0' THEN p.reason ELSE r.reason END AS reason,
                    CASE WHEN r.pattern IS NULL OR r.pattern='0' THEN p.tmcd_group_id ELSE r.tmcd_group_id END AS tmcd_group_id
                FROM calendar c
                LEFT JOIN atdbkrslt r ON r.emp_id = :emp_id AND r.date=c.date
                LEFT JOIN atdbk p ON p.emp_id = :emp_id AND p.date=c.date
                WHERE c.date >= :sdate AND c.date < :edate
            ) r
            JOIN atdptn a ON (a.group_id=r.tmcd_group_id AND a.atdptn_id=TO_NUMBER('0'||r.pattern,'999'))
            JOIN atdbk_reason_mst ON (TO_NUMBER('0'||r.reason,'999') = TO_NUMBER('0'||reason_id,'999') OR a.reason = TO_NUMBER('0'||reason_id,'999'))
            WHERE kind_flag = '1'
            GROUP BY kind_flag
        ";

        $atdbkrslt = $this->db->extended->getRow(
            $sql_rslt, null, array('emp_id' => $emp_id, 'sdate' => $paid_date, 'edate' => $date), array('text', 'text', 'text'), MDB2_FETCHMODE_ASSOC
        );
        $used = (float)$emppaid["used"] + (float)$atdbkrslt["holiday"];

        // 前年
        // emppaid
        $lastpaid = $this->db->extended->getRow(
            $sql_paid, null, array($emp_id, prev_ymd($paid_date)), array('text', 'text'), MDB2_FETCHMODE_ASSOC
        );
        if ($emppaid["paid_date"]) {
            $lastpaid_date = $lastpaid["paid_date"] ? $lastpaid["paid_date"] : '19000101';
            $lastrslt = $this->db->extended->getRow(
                $sql_rslt, null, array('emp_id' => $emp_id, 'sdate' => $lastpaid_date, 'edate' => prev_ymd($paid_date)), array('text', 'text', 'text'), MDB2_FETCHMODE_ASSOC
            );
        }

        // 有休データ詳細(データ出力用)
        $details = array(
            'holiday_date' => $paid_date, //最終付与日
            'holiday_mae_kuri' => (float)$lastpaid['days1'], // 前年繰越
            'holiday_mae_fuyo' => (float)$lastpaid['days2'], // 前年付与
            'holiday_mae_adjust' => (float)$lastpaid['adjust_day'], // 前年調整
            'holiday_mae_use' => (float)$lastpaid["used"] + (float)$lastrslt["holiday"], // 前年取得
            'holiday_mae_zan' => (float)$lastpaid['days1'] + (float)$lastpaid['days2'] + (float)$lastpaid['adjust_day'] - (float)$lastpaid["used"] - (float)$lastrslt["holiday"], // 前年残
            'holiday_kuri' => (float)$emppaid['days1'], // 当年繰越
            'holiday_fuyo' => (float)$emppaid['days2'], // 当年付与
            'holiday_adjust' => (float)$emppaid['adjust_day'], // 当年調整
            'holiday_use' => (float)$emppaid["used"] + (float)$atdbkrslt["holiday"], // 当年取得
        );

        return array($paid_holiday, $used, $paid_honnen, $paid_kurikoshi, $details);
    }

    /**
     * 夏休残日数取得
     * @param $emp_id 職員ID
     * @param $date 取得基準日
     * @return boolean
     */
    function paid_holiday_summer($emp_id, $date)
    {
        if ($date == "") {
            return array(0, 0);
        }

        // emppaid
        $sql = "
            SELECT
                days,
                add_date AS paid_date,
                used
            FROM emppaid_summer
            WHERE emp_id = ?
            AND add_date <= ?
            ORDER BY add_date DESC
            OFFSET 0 LIMIT 1
            ";

        $emppaid = $this->db->extended->getRow(
            $sql, null, array($emp_id, $date), array('text', 'text'), MDB2_FETCHMODE_ASSOC
        );
        $paid_holiday = (float)$emppaid['days'];
        $paid_date = $emppaid["paid_date"] ? preg_replace("/^(\d\d\d\d)\-(\d\d)\-(\d\d)$/", "$1$2$3", $emppaid["paid_date"]) : '19000101';

        // atdbkrslt
        $sql = "
            SELECT
                SUM(TO_NUMBER(holiday_count,'999.99')) AS holiday
            FROM (
                SELECT
                    c.date,
                    CASE WHEN r.pattern IS NULL OR r.pattern='0' THEN p.pattern ELSE r.pattern END AS pattern,
                    CASE WHEN r.pattern IS NULL OR r.pattern='0' THEN p.reason ELSE r.reason END AS reason,
                    CASE WHEN r.pattern IS NULL OR r.pattern='0' THEN p.tmcd_group_id ELSE r.tmcd_group_id END AS tmcd_group_id
                FROM calendar c
                LEFT JOIN atdbkrslt r ON r.emp_id = :emp_id AND r.date=c.date
                LEFT JOIN atdbk p ON p.emp_id = :emp_id AND p.date=c.date
                WHERE c.date >= :sdate AND c.date < :edate
            ) r
            JOIN atdptn a ON (a.group_id=r.tmcd_group_id AND a.atdptn_id=TO_NUMBER('0'||r.pattern,'999'))
            JOIN atdbk_reason_mst ON (TO_NUMBER('0'||r.reason,'999') = TO_NUMBER('0'||reason_id,'999') OR a.reason = TO_NUMBER('0'||reason_id,'999'))
            WHERE kind_flag = '3'
            GROUP BY kind_flag
        ";

        $atdbkrslt = $this->db->extended->getRow(
            $sql, null, array('emp_id' => $emp_id, 'sdate' => $paid_date, 'edate' => $date), array('text', 'text', 'text'), MDB2_FETCHMODE_ASSOC
        );
        $used = (float)$emppaid["used"] + (float)$atdbkrslt["holiday"];
        return array($paid_holiday, $used, $paid_date);
    }

    /**
     * 週休残日数取得
     * @param $emp_id 職員ID
     * @param $date 取得基準日
     * @return boolean
     */
    function paid_holiday_week($emp_id, $date)
    {
        if ($date == "") {
            return array(0, 0);
        }

        // emppaid
        $sql = "
            SELECT
                days,
                add_date AS paid_date,
                used
            FROM emppaid_week
            WHERE emp_id = ?
            AND add_date <= ?
            ORDER BY add_date DESC
            OFFSET 0 LIMIT 1
            ";

        $emppaid = $this->db->extended->getRow(
            $sql, null, array($emp_id, $date), array('text', 'text'), MDB2_FETCHMODE_ASSOC
        );
        $paid_holiday = (float)$emppaid['days'];
        $paid_date = $emppaid["paid_date"] ? preg_replace("/^(\d\d\d\d)\-(\d\d)\-(\d\d)$/", "$1$2$3", $emppaid["paid_date"]) : '19000101';

        // atdbkrslt
        $sql = "
            SELECT
                SUM(TO_NUMBER(holiday_count,'999.99')) AS holiday
            FROM (
                SELECT
                    c.date,
                    CASE WHEN r.pattern IS NULL OR r.pattern='0' THEN p.pattern ELSE r.pattern END AS pattern,
                    CASE WHEN r.pattern IS NULL OR r.pattern='0' THEN p.reason ELSE r.reason END AS reason,
                    CASE WHEN r.pattern IS NULL OR r.pattern='0' THEN p.tmcd_group_id ELSE r.tmcd_group_id END AS tmcd_group_id
                FROM calendar c
                LEFT JOIN atdbkrslt r ON r.emp_id = :emp_id AND r.date=c.date
                LEFT JOIN atdbk p ON p.emp_id = :emp_id AND p.date=c.date
                WHERE c.date >= :sdate AND c.date < :edate
            ) r
            JOIN atdptn a ON (a.group_id=r.tmcd_group_id AND a.atdptn_id=TO_NUMBER('0'||r.pattern,'999'))
            JOIN atdbk_reason_mst ON (TO_NUMBER('0'||r.reason,'999') = TO_NUMBER('0'||reason_id,'999') OR a.reason = TO_NUMBER('0'||reason_id,'999'))
            WHERE kind_flag = '2'
            GROUP BY kind_flag
        ";

        $atdbkrslt = $this->db->extended->getRow(
            $sql, null, array('emp_id' => $emp_id, 'sdate' => $paid_date, 'edate' => $date), array('text', 'text', 'text'), MDB2_FETCHMODE_ASSOC
        );
        $used = (float)$emppaid["used"] + (float)$atdbkrslt["holiday"];
        return array($paid_holiday, $used, $paid_date);
    }

}
