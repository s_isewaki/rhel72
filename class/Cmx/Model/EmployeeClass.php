<?php

require_once('Cmx.php');
require_once('Cmx/Core/Model.php');

/**
 * CoMedix������
 *
 * @link http://trac.medi-system.co.jp/trac/comedix/wiki/tables/classmst
 * @link http://trac.medi-system.co.jp/trac/comedix/wiki/tables/atrbmst
 * @link http://trac.medi-system.co.jp/trac/comedix/wiki/tables/deptmst
 * @link http://trac.medi-system.co.jp/trac/comedix/wiki/tables/classroom
 * @link http://trac.medi-system.co.jp/trac/comedix/wiki/tables/classname
 */
class Cmx_EmployeeClass extends Model {

    var $_classname;

    /**
     * ���󥹥ȥ饯��
     */
    function Cmx_EmployeeClass() {
        parent::connectDB();
    }

    /**
     * _classname
     */
    function _classname() {
        $this->_classname = $this->db->queryRow('SELECT * FROM classname', null,
                                                MDB2_FETCHMODE_ASSOC);
    }

    /**
     * �ȿ�1�ꥹ��
     * @return array
     */
    function class_lists() {
        $res = $this->db->query("SELECT *, class_id as id, class_nm as name FROM classmst WHERE NOT class_del_flg ORDER BY order_no, class_id");
        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[$row['id']] = $row;
        }
        return $list;
    }

    /**
     * �ȿ�2�ꥹ��
     * @return array
     */
    function atrb_lists() {
        $res = $this->db->query("SELECT *, atrb_id as id, atrb_nm as name, class_id as parent FROM atrbmst WHERE NOT atrb_del_flg ORDER BY order_no, atrb_id");
        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[$row['id']] = $row;
        }
        return $list;
    }

    /**
     * �ȿ�3�ꥹ��
     * @return array
     */
    function dept_lists() {
        $res = $this->db->query("SELECT *, dept_id as id, dept_nm as name, atrb_id as parent FROM deptmst WHERE NOT dept_del_flg ORDER BY order_no, dept_id");
        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[$row['id']] = $row;
        }
        return $list;
    }

    /**
     * �ȿ�4�ꥹ��
     * @return array
     */
    function room_lists() {
        if ($this->class_count() !== "4") {
            return array();
        }

        $res = $this->db->query("SELECT *, room_id as id, room_nm as name, dept_id as parent FROM classroom WHERE NOT room_del_flg ORDER BY order_no, room_id");
        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[$row['id']] = $row;
        }
        return $list;
    }

    /**
     * �ȿ�1��̾��
     * @return �ȿ�1��̾��
     */
    function class_name() {
        if (!$this->_classname) {
            $this->_classname();
        }
        return $this->_classname['class_nm'];
    }

    /**
     * �ȿ�2��̾��
     * @return �ȿ�2��̾��
     */
    function atrb_name() {
        if (!$this->_classname) {
            $this->_classname();
        }
        return $this->_classname['atrb_nm'];
    }

    /**
     * �ȿ�3��̾��
     * @return �ȿ�3��̾��
     */
    function dept_name() {
        if (!$this->_classname) {
            $this->_classname();
        }
        return $this->_classname['dept_nm'];
    }

    /**
     * �ȿ�4��̾��
     * @return �ȿ�4��̾��
     */
    function room_name() {
        if (!$this->_classname) {
            $this->_classname();
        }
        return $this->_classname['room_nm'];
    }

    /**
     * �ȿ����ؿ�
     * @return �ȿ����ؿ�
     */
    function class_count() {
        if (!$this->_classname) {
            $this->_classname();
        }
        return $this->_classname['class_cnt'];
    }

}

/**
 * Smarty Function
 * ����ץ������ꥹ��
 *
 * @param type $params
 * @param type $smarty
 * @return type
 */
function smarty_class_pulldown($params, &$smarty) {
    $output = '';
    foreach ($params['array'] as $row) {
        $selected = "";
        if (!empty($params['id']) and $row['id'] === $params['id']) {
            $selected = 'selected';
        }
        $output.= sprintf(
            '<option style="display:none;" class="parent%s" value="%s" %s>%s</option>',
            htmlspecialchars($row['parent'], ENT_QUOTES, 'euc-jp'),
                             htmlspecialchars($row['id'], ENT_QUOTES, 'euc-jp'), $selected,
                                              htmlspecialchars($row['name'], ENT_QUOTES, 'euc-jp')
        );
    }
    return $output;
}

