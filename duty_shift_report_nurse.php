<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務シフト作成 | 看護職員表</title>

<!-- ************************************************************************ -->
<!-- JavaScript -->
<!-- ************************************************************************ -->
<?
///ini_set("display_errors","1");
require("about_authority.php");
require("about_session.php");
require("about_postgres.php");

require_once("duty_shift_common_class.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
//セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
//DBコネクション
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
///-----------------------------------------------------------------------------
//初期処理
///-----------------------------------------------------------------------------
	///-----------------------------------------------------------------------------
	//指定日の週を設定
	///-----------------------------------------------------------------------------
	$week_array = array();
	for ($k=1; $k<=$day_cnt; $k++) {
		$tmp_date = mktime(0, 0, 0, $duty_mm, $k, $duty_yyyy);
		$week_array[$k]["name"] = $obj->get_weekday($tmp_date);
	}
	///-----------------------------------------------------------------------------
	//データ取得
	///-----------------------------------------------------------------------------
	$data_array = array();
	for ($i=0; $i<$data_cnt; $i++) {
		$data_array[$i]["staff_id"] = $staff_id[$i];
		$data_array[$i]["staff_name"] = $staff_name[$i];
		$data_array[$i]["job_id"] = $job_id[$i];
		$data_array[$i]["job_name"] = $job_name[$i];
		$data_array[$i]["night_staff_cnt"] = $night_staff_cnt[$i];	//夜勤従事者への計上人数
		$data_array[$i]["night_duty_flg"] = $night_duty_flg[$i];	//夜勤の有無（１：有り、２：夜専）
		$data_array[$i]["duty_form"] = $duty_form[$i];				//勤務形態（1:常勤、2:非常勤）
		$data_array[$i]["other_post_flg"] = $other_post_flg[$i];	//他部署兼務（１：有り）
		for ($k=1; $k<=$day_cnt; $k++) {
			$wk1 = "duty_time_" . $i . "_" . $k;
			$wk2 = "night_time_" . $i . "_" . $k;
			$data_array[$i]["duty_time_$k"] = $$wk1;
			$data_array[$i]["night_time_$k"] = $$wk2;
		}
	}
?>

<!-- ------------------------------------------------------------------------ -->
<!-- 内部関数定義 -->
<!-- ------------------------------------------------------------------------ -->
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
</script>

<!-- ************************************************************************ -->
<!-- HTML -->
<!-- ************************************************************************ -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>

<? //echo("data_cnt=$data_cnt"); ?>
<? //echo("standard_id=$standard_id"); ?>
<? //print_r($atdbkrslt_array); ?>
<? //print_r($data_array); ?>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
	<!-- ************************************************************************ -->
	<!-- 看護職員表 -->
	<!-- ************************************************************************ -->
	<table>
	<tr><td><b><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">《看護職員表》</font></b></td></tr>
	</table>
	<!-- ------------------------------------------------------------------------ -->
	<!-- 勤務シフト計画表1部分START -->
	<!-- ------------------------------------------------------------------------ -->
	<form name="nurseForm" method="post">
	<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
	<!-- ------------------------------------------------------------------------ -->
	<!-- 勤務計画表（タイトル） -->
	<!-- ------------------------------------------------------------------------ -->
	<tr>
	<td bgcolor="#fefcdf" rowspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j9">種<br>別</font></td>
	<td bgcolor="#fefcdf" rowspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j9">番<br>号</font></td>
	<td bgcolor="#fefcdf" rowspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j9">氏名</font></td>
	<td bgcolor="#fefcdf" bgcolor="#fefcdf" rowspan="3" colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j9">雇用・勤務形態</font></td>
	<td bgcolor="#fefcdf" rowspan="3" colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j9">夜勤の有無</font></td>
	<td bgcolor="#fefcdf" rowspan="3" width="50px"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j9">夜勤従事者数への計上</font></td>
	<td bgcolor="#fefcdf" rowspan="3">&nbsp;</td>
	<td bgcolor="#fefcdf" colspan="<? echo($day_cnt); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j9">日付別の勤務時間数</font></td>
	<td bgcolor="#fefcdf" rowspan="3" width="30px"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j9">月勤務時間数（延べ時間数）</font></td>
	<td bgcolor="#fefcdf" rowspan="3" width="40px"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j9">（再掲）夜勤専従者及び月16時間以下の者の夜勤時間数</font></td>
	<td bgcolor="#fefcdf" rowspan="2" colspan="2" width="40px"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j9">職種別月勤務時間数</font></td>
	</tr>

	<tr>
	<? for($i=1; $i<=$day_cnt; $i++) { ?>
		<td bgcolor="#fefcdf"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j9"><? echo($i) ?>日</font></td>
	<? } ?>
	</tr>

	<tr>
	<? for($i=1;$i<=$day_cnt;$i++) { ?>
		<td bgcolor="#fefcdf"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j9"><? echo($week_array[$i]["name"] ) ?>曜</font></td>
	<? } ?>
	<td bgcolor="#fefcdf" width="20px"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j9">看護師</font></td>
	<td bgcolor="#fefcdf" width="20px"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j9">准看護師</font></td>
	</tr>

	<!-- ------------------------------------------------------------------------ -->
	<!-- 勤務計画表（データ） -->
	<!-- ------------------------------------------------------------------------ -->
	<? for($i=0; $i<$data_cnt; $i++) { ?>
		<!-- ------------------------------------------------------------------------ -->
		<!-- 勤務計画表（データ 日勤時間数） -->
		<!-- ------------------------------------------------------------------------ -->
		<tr>
		<td align="left" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j9">
		<?
			//職種
			$wk = $data_array[$i]["job_name"];
			echo($wk);
		?>
		</font></td>

		<td align="right" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j9">
		<?
			//ＮＯ
			echo($i+1);
		?>
		</font></td>

		<td align="left" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j9">
		<?
			//名前
			$wk = $data_array[$i]["staff_name"];
			echo($wk);
		?>
		</font></td>

		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j9">常勤</font></td>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j9">非常勤</font></td>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j9">他部署兼務</font></td>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j9">有</font></td>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j9">無</font></td>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j9">夜専</font></td>
		<td  bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j9">夜勤従事者</font></td>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j9">日勤時間数</font></td>

		<?
			//勤務時間（日勤）
			for($k=1; $k<=$day_cnt; $k++) {
				$wk = $data_array[$i]["duty_time_$k"];
				echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j9\">$wk<font></td> \n");
			}
		?>

		<td bgcolor="#DFFFDC"></td>
		<td bgcolor="#DFFFDC"></td>
		<td bgcolor="#DFFFDC"></td>
		<td bgcolor="#DFFFDC"></td>
		</tr>

		<!-- ------------------------------------------------------------------------ -->
		<!-- 勤務計画表（データ 夜勤時間数） -->
		<!-- ------------------------------------------------------------------------ -->
		<tr>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td bgcolor="#DFFFDC"></td>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j9">夜勤時間数</font></td>

		<?
			//勤務時間（夜勤）
			for($k=1; $k<=$day_cnt; $k++) {
				$wk = $data_array[$i]["night_time_$k"];
				echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j9\">$wk</font></td> \n");
			}
		?>

		<td bgcolor="#DFFFDC"></td>
		<td bgcolor="#DFFFDC"></td>
		<td bgcolor="#DFFFDC"></td>
		<td bgcolor="#DFFFDC"></td>
		</tr>
	<? } ?>
	</table>

	<br>

	<!-- ------------------------------------------------------------------------ -->
	<!-- 勤務シフト計画表1部分END -->
	<!-- ------------------------------------------------------------------------ -->
	<table width="90%" border="0" cellspacing="0" cellpadding="2" class="list">
	<tr>
		<td bgcolor="#f6f9ff"><div align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">夜勤従事者数(夜勤ありの職員数)〔Ｂ〕</font></div></td>
		<td>
			<input type="text" size="20" maxlength="20" name="" value="">
		</td>
		<td bgcolor="#f6f9ff"><div align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">月延べ勤務時間数の計〔Ｃ〕</font></div></td>
		<td>
			<input type="text" size="20" maxlength="20" name="" value="">
		</td>
	</tr>

	<tr>
		<td bgcolor="#f6f9ff" rowspan="2"><div align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">月延べ夜勤時間数〔D−E〕</font></div></td>
		<td rowspan="2">
			<input type="text" size="20" maxlength="20" name="" value="">
		</td>
		<td bgcolor="#f6f9ff"><div align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">月延べ夜勤時間数の計〔Ｄ〕</font></div></td>
		<td>
			<input type="text" size="20" maxlength="20" name="" value="">
		</td>
	</tr>

	<tr>
		<td bgcolor="#f6f9ff"><div align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">夜勤専従者及び月16時間以下の者の夜勤時間数〔E〕</font></div></td>
		<td>
			<input type="text" size="20" maxlength="20" name="" value="">
		</td>
	</tr>

	<tr>
		<td bgcolor="#f6f9ff"><div align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">1日看護配置数〔(A／届出区分の数)×３〕</font></div></td>
		<td>
			<input type="text" size="20" maxlength="20" name="" value="">
		</td>
		<td bgcolor="#f6f9ff"><div align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">月平均１日あたり看護配置数〔C／(日数×８）〕</font></div></td>
		<td>
			<input type="text" size="20" maxlength="20" name="" value="">
		</td>
	</tr>

	</table>
	</form>

</body>
<? pg_close($con); ?>
</html>
