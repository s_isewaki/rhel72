<?php
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_select_values.ini");
require_once("referer_common.ini");
require_once("get_values.ini");
require_once("workflow_common.ini");
require_once("./conf/sql.inf");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 3, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

$con = connect2db($fname);

// 遷移元の取得
$referer = get_referer($con, $session, "workflow", $fname);

// 職員ID取得
$emp_id=get_emp_id($con,$session,$fname);

// ログインID取得
$emp_login_id = get_login_id($con,$emp_id,$fname);

// guest*ユーザはテンプレートを登録できない
$can_regist_flg = true;
if (substr($emp_login_id, 0, 5) == "guest") {
	$can_regist_flg = false;
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>ワークフロー｜テンプレート登録</title>
<script type="text/javascript">

function submitForm() {
	document.wkfw.submit();
}

function referTemplate() {
	window.open('workflow_lib_template_refer.php?session=<? echo($session); ?>', 'newwin', 'width=640,height=480,scrollbars=yes');
}

function submitPreviewForm() {
	document.wkfw.action = "workflow_lib_register.php";
	document.wkfw.preview_flg.value = "1";
	document.wkfw.submit();
}

function show_preview_window(url){
	var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=500,top=100,width=600,height=700";
	window.open(url, 'preview_window',option);
}
</script>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/workflow/workflow.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
p {margin:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($referer == "2") { ?>
<table id="header">
<tr>
<td class="icon"><a href="workflow_menu.php?session=<? echo($session); ?>"><img src="img/icon/b24.gif" width="32" height="32" border="0" alt="ワークフロー"></a></td>
<td><a href="workflow_menu.php?session=<?=$session?>"><b>ワークフロー</b></a></td>
</tr>
</table>
<? } else { ?>
<table id="header">
<tr>
<td class="icon"><a href="application_menu.php?session=<? echo($session); ?>"><img src="img/icon/b08.gif" width="32" height="32" border="0" alt="決裁・申請"></a></td>
<td>
	<a href="application_menu.php?session=<? echo($session); ?>"><b>決裁・申請</b></a> &gt; 
	<a href="workflow_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a> &gt; 
	<a href="workflow_lib_list.php?session=<? echo($session); ?>"><b>テンプレート</b></a>
</td>
<td class="kanri"><a href="application_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></td>
</tr>
</table>
<? } ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td valign="top" width="120"><?show_wkfw_sidemenu($session);?></td>
<td width="5"><img src="img/spacer.gif" width="5" height="2" alt=""></td>
<td valign="top">

<?
show_wkfw_menuitem($session, $fname, "");
?>

<form name="wkfw" action="workflow_lib_insert.php" method="post">
<table width="580" border="0" cellspacing="0" cellpadding="2" class="list">

<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">テンプレート名称</font></td>
<td><input name="wkfw_title" type="text" size="50" maxlength="80" value="<? echo($wkfw_title); ?>" style="ime-mode: active;"></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="3"><br>
<table width="580" border="0" cellspacing="0" cellpadding="2">
<tr>
<td align="left" width="100">

<table width="100" border="0" cellspacing="0" cellpadding="2" class="list">
<tr>
<td align="left" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">本文</font>
&nbsp;
<input type="button" name="referbtn" value="参照" onclick="referTemplate();">
</td>
</tr>
</table>

</td>
<td align="right">
<input type="button" name="prevbtn" value="プレビュー" onclick="submitPreviewForm();">
<input type="button" name="regist" value="登録" onclick="submitForm();"></td>
</tr>
<tr>
<?
// ファイルから本文を読み込む
//$wkfw_content = "";
$savefilename = "workflow/tmp/{$session}_t.php";
if ($back == "t") {
	$fp = fopen($savefilename, "r");
	if ($fp != false){
		$wkfw_content = fread($fp, filesize($savefilename));
		fclose($fp);
	}
}
// テキストエリア中に/textareaがあると表示が崩れるため変換する
$wkfw_content = eregi_replace("/(textarea)", "_\\1", $wkfw_content);

?>
<td colspan="3"><textarea name="wkfw_content" rows="20" cols="80"><? echo($wkfw_content); ?></textarea></td>
</tr>
</table>

<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="preview_flg" value="">
<input type="hidden" name="wkfwlib_filename" value="<? echo($wkfwlib_filename); ?>">
<input type="hidden" name="back" value="">
</form>
</td>
</tr>
</table>
</td>
</tr>
</table>
<script type="text/javascript">
<?
// プレビュー押下時
if ($preview_flg == "1") {
// wkfw_content 保存
$ext = ".php";
$savefilename = "workflow/tmp/{$session}_t{$ext}";

// 内容書き込み
if (!is_dir("workflow")) {mkdir("workflow", 0755);}
if (!is_dir("workflow/tmp")) {mkdir("workflow/tmp", 0755);}
$fp = fopen($savefilename, "w");
fwrite($fp, $wkfw_content, 2000000);

fclose($fp);

$wkfw_title = str_replace("'","\'", $wkfw_title);

?>
	var url = 'workflow_lib_register_preview.php'
		+ '?session=<? echo($session); ?>';
	show_preview_window(url);
<?
}

if ($can_regist_flg == false) {
?>
document.wkfw.regist.disabled = true;
<? } ?>

</script>

</body>
<? pg_close($con); ?>
</html>