<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("./about_session.php");
require("./about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"./js/showpage.js\"></script>");
	echo("<script language=\"javascript\">showLoginPage(window);</script>");
	exit;
}

// ワークフロー権限のチェック
$checkauth = check_authority($session, 3, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"./js/showpage.js\"></script>");
	echo("<script language=\"javascript\">showLoginPage(window);</script>");
	exit;
}

// ファイルアップロードチェック
define(UPLOAD_ERR_OK, 0);
define(UPLOAD_ERR_INI_SIZE, 1);
define(UPLOAD_ERR_FORM_SIZE, 2);
define(UPLOAD_ERR_PARTIAL, 3);
define(UPLOAD_ERR_NO_FILE, 4);

$filename = $_FILES["template_file"]["name"];
if ($filename == "") {
	echo("<script language=\"javascript\">alert('ファイルを選択してください。');</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
}

switch ($_FILES["template_file"]["error"]) {
case UPLOAD_ERR_INI_SIZE:
case UPLOAD_ERR_FORM_SIZE:
	echo("<script language=\"javascript\">alert('ファイルサイズが大きすぎます。');</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
case UPLOAD_ERR_PARTIAL:
case UPLOAD_ERR_NO_FILE:
	echo("<script language=\"javascript\">alert('アップロードに失敗しました。再度実行してください。');</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
}

// ファイル保存用ディレクトリがなければ作成
if (!is_dir("workflow")) {
	mkdir("workflow", 0755);
}
if (!is_dir("workflow/tmp")) {
	mkdir("workflow/tmp", 0755);
}

// 30分以上前に保存されたファイルを削除
foreach (glob("workflow/tmp/*.*") as $tmpfile) {
	if (time() - filemtime($tmpfile) >= 30 * 60) {
		unlink($tmpfile);
	}
}

// アップロードされたファイルを保存
$ext = ".php";
$savefilename = "workflow/tmp/{$session}_t{$ext}";
copy($_FILES["template_file"]["tmp_name"], $savefilename);

// 内容読み込み
if (!$fp = fopen($savefilename, "r")) {
	echo("<script language=\"javascript\">alert('ファイルのオープンができませんでした。');</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
}

$content = fread($fp, filesize($savefilename));

fclose($fp);

$chkarray = array("INSERT", "UPDATE", "DELETE", "CONNECT2DB", "SELECT_FROM_TABLE", "PG_CONNECT", "PG_EXEC", "<FORM");
// 内容チェック
$content_upper = strtoupper($content);
$sqlchkflg = true;

foreach($chkarray as $chkstr){
	if(!(strpos($content_upper, $chkstr) === false)){
		$sqlchkflg = false;
		break;
	}
}

if($sqlchkflg == false){
	echo("<script language=\"javascript\">alert('指定できない文字列があります($chkstr)。');</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
}

$content = str_replace("\\","\\\\", $content);
$content = str_replace("\r\n","\n", $content);
$content = str_replace("\n","\\n", $content);
$content = str_replace("\"","\\\"", $content);
$content = eregi_replace("/(script)", "_\\1", $content);

if (strpos($content, "dump_mem") === false) {
	echo("<script type=\"text/javascript\">alert('テンプレートが最後まで設定されていません。確認してください。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

//$content = "\\\\";
?>
<script type="text/javascript">
opener.document.wkfw.wkfw_content.value = "<? echo($content); ?>";
if (opener.document.wkfw.wkfwlib_filename) { 
	opener.document.wkfw.wkfwlib_filename.value = "<? echo($filename); ?>";
}
opener.document.wkfw.wkfw_content.value = "<? echo($content); ?>";
setTimeout("self.close();", 200);
</script>
