<?
/*
画面パラメータ
$session
	セッションID
$page
	ページ番号。渡らなければ「1」とみなす
*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<?
require_once("about_comedix.php");
require_once("summary_common.ini");
require_once("get_menu_label.ini");

$fname = $PHP_SELF;

$session = qualify_session($session, $fname);
if ($session == "0") {
	js_login_exit();
	exit;
}

$checkauth = check_authority($session, 57, $fname);
if ($checkauth == "0") {
	js_login_exit();
	exit;
}

$con = connect2db($fname);
$menu_label = get_report_menu_label($con, $fname);

//==============================
//検索処理
//==============================
if ($page == "") {$page = "1";}
$limit = 30;
$offset = $limit * ($page - 1);

$sql = "select byoumei, byoumei_kana, icd10, rece_code, count(*) from (";
$parts = array();
for ($i = 1; $i <= 5; $i++) {
	$parts[] = "select b.byoumei, b.byoumei_kana, b.icd10, b.rece_code from medis_byoumei b inner join sum_pt_disease d on b.byoumei = d.byoumei_$i";
}
$sql .= implode(" union all ", $parts);
$sql .= ") b";
$cond = "group by byoumei, icd10, rece_code, byoumei_kana order by 5 desc, 2";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	js_error_exit();
}
pg_close($con);

$record_count = pg_num_rows($sel);
$result = array();
for ($i = $offset, $j = min($offset + $limit, $record_count); $i < $j; $i++) {
	$result[] = array(
		"byoumei"   => pg_fetch_result($sel, $i, "byoumei"),
		"icd10"     => pg_fetch_result($sel, $i, "icd10"),
		"rece_code" => pg_fetch_result($sel, $i, "rece_code")
	);
}

//==========================================================================================
//HTML出力
//==========================================================================================
?>
<title>CoMedix <?=$menu_label?>｜傷病名検索</title>
<script type="text/javascript">
function selectDisease(byoumei, icd10, rece_code, target_table_class, position, link_code) {
	if (opener && !opener.closed && opener.selectDisease) {
		opener.selectDisease(byoumei, icd10, rece_code, target_table_class, position, link_code);
	}
}

function highlightCells(class_name) {
	changeCellsColor(class_name, '#fefc8f');
}
function dehighlightCells(class_name) {
	changeCellsColor(class_name, '');
}
function changeCellsColor(class_name, color) {
	var cells = document.getElementsByTagName('td');
	for (var i = 0, j = cells.length; i < j; i++) {
		if (cells[i].className != class_name) {
			continue;
		}
		cells[i].style.backgroundColor = color;
	}
}
</script>
</head>
<body>

<div style="padding:4px">

<? summary_common_show_dialog_header("傷病名検索"); ?>

<form name="main_form" action="summary_disease_rank.php" method="post">
<input type="hidden" name="session" value="<?=$session?>">

<table style="margin-top:8px">
	<tr>
	<td style="padding:2px" width="100"><b>頻出順</b></td>
	<td style="padding:2px"><a href="summary_disease_search.php?session=<? echo($session); ?>">病名・修飾語検索</a></td>
	</tr>
</table>

<? my_show_page_link($record_count, $limit, $page, $session); ?>

<? if ($record_count > 0) { ?>
<table class="list_table" style="margin-top:-10px;">
	<!-- MEDIS一覧HEADER START -->
	<tr>
	<th style="text-align:center"><nobr>傷病名</nobr></th>
	<th style="text-align:center" width="100"><nobr>ICD10</nobr></th>
	<th style="text-align:center" width="100"><nobr>レセ電算</nobr></th>
	</tr>
	<!-- MEDIS一覧HEADER END -->
	<!-- MEDIS一覧DATA START -->
	<?
	for ($i = 0, $j = count($result); $i < $j; $i++) {
		$byoumei   = $result[$i]["byoumei"];
		$icd10     = $result[$i]["icd10"];
		$rece_code = $result[$i]["rece_code"];

		$line_script = "";
		$line_script .= " class=\"list_line".$i."\"";
		$line_script .= " onmouseover=\"highlightCells(this.className);this.style.cursor = 'pointer';\"";
		$line_script .= " onmouseout=\"dehighlightCells(this.className);this.style.cursor = '';\"";
		$line_script .= " onclick=\"selectDisease('" . h_jsparam($byoumei) . "', '" . h_jsparam($icd10) . "', '" . h_jsparam($rece_code) . "', '1', '', '')\"";

		$bgcolor = @$bgcolor=="#fff" ? "#f1f7fd" : "#fff";
	?>
	<tr style="background-color:<?=$bgcolor?>">
	<td <?=$line_script?> align="left"><nobr><?=h($byoumei)?></nobr></td>
	<td <?=$line_script?> align="left"><nobr><?=h($icd10)?></nobr></td>
	<td <?=$line_script?> align="left"><nobr><?=h($rece_code)?></nobr></td>
	</tr>
	<? } ?>
	<!-- MEDIS一覧DATA END -->
</table>
<? } ?>

</form>

</div>
</body>
</html>
<?
function my_show_page_link($record_count, $limit, $page, $session) {
	if ($record_count == 0) return;

	// 全ページ数
	$page_count = ceil($record_count / $limit);
	if ($page > $page_count) {$page = $page_count;}

	// 現在地より何ページ前まで表示するか
	$page_offset = 5;

	// 全部で何ページ分表示するか
	$link_count = 10;

	// 表示開始ページ
	$page_start = $page - $page_offset;
	if ($page_start < 1) {$page_start = 1;}

	// 表示終了ページ
	$page_end = $page_start + $link_count - 1;
	if ($page_end > $page_count) {$page_end = $page_count;}

	echo "<p style=\"font-size:110%;margin-top:8px;\">ページ";
	if ($page == 1) {
		echo "　<font color=\"silver\">←</font>";
	} else {
		echo "　<a href=\"summary_disease_rank.php?session=$session&amp;page=" . ($page - 1) . "\">←</a>";
	}
	for ($i = $page_start; $i <= $page_end; $i++) {
		if ($i == $page) {
			echo "　[$i]";
		} else {
			echo "　<a href=\"summary_disease_rank.php?session=$session&amp;page=$i\">$i</a>";
		}
	}
	if ($page == $page_count) {
		echo "　<font color=\"silver\">→</font>";
	} else {
		echo "　<a href=\"summary_disease_rank.php?session=$session&amp;page=" . ($page + 1) . "\">→</a>";
	}
	echo "</p>";
}
