<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>リンクライブラリ | リンク名表示設定</title>
<?php
/*************************************************************************
リンクライブラリ管理
  リンク名表示設定・画面
*************************************************************************/
require_once("about_comedix.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック(リンクライブラリ管理[61])
$check_auth = check_authority($session, 61, $fname);
if ($check_auth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// リンクライブラリ管理権限の取得(リンクライブラリ管理[61])
$link_admin_auth = check_authority($session, 61, $fname);

// データベースに接続
$con = connect2db($fname);

// カテゴリー一覧を取得
$sql = "select linkcate_id, linkcate_nm from linkcate";
$cond = "order by linkcate_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// カテゴリー登録済みの場合
if (pg_num_rows($sel) > 0) {
	
	// 取得したカテゴリー情報を配列に格納
	$categories = pg_fetch_all($sel);
	
	define("DEFAULT_ROW_COUNT", 1);
	
	// リンク一覧配列を初期化
	$links = array();
	
	// 初期表示時
	if ($mode == "") {
		
		// カテゴリー未選択の場合は最初のカテゴリーをデフォルトとする
		if ($linkcate_id == "") {
			$linkcate_id = $categories[0]["linkcate_id"];
		}
		
		// リンク一覧を取得
		$con = connect2db($fname);
		$sql = "select *, 1 as exist from link";
		$cond = "where linkcate_id = $linkcate_id order by link_id";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		pg_close($con);
		
		// 取得した情報を配列に格納
		$links = pg_fetch_all($sel);
		
		// 最低表示行数に満たない場合は行を追加
		while (count($links) < DEFAULT_ROW_COUNT) {
			$links[] = add_row();
		}
		
		// 画面再表示時
	} else {
		for ($i = 0, $j = count($link_nm); $i < $j; $i++) {
			if ($mode == "DELETE_ROW" && $i == $del_id) {
				continue;
			}
			
			$links[] = add_row(
					$link_id[$i],
					$link_type[$i],
					$link_nm[$i],
					$link_url[$i],
					$link_note[$i],
					$link_icon[$i],
					$note_flg[$i],
					$show_flg1[$i],
					$order1[$i],
					$icon_flg1[$i],
					$show_flg2[$i],
					$order2[$i],
					$icon_flg2[$i],
					$show_flg3[$i],
					$order3[$i],
					$icon_flg3[$i],
					$exist[$i]
					);
		}
		
		// 「行を追加」ボタン押下時
		if ($mode == "ADD_ROW") {
			$links[] = add_row();
		}
	}
} else {
	$categories = array();
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function changeCategory(linkcate_id) {
	location.href = 'link_admin_link_setting.php?session=<? echo($session); ?>&linkcate_id='.concat(linkcate_id);
}

function addRow() {
	document.mainform.action = 'link_admin_link_setting.php';
	document.mainform.mode.value = 'ADD_ROW';
	document.mainform.submit();
}

function deleteRow(del_id) {
	document.mainform.action = 'link_admin_link_setting.php';
	document.mainform.mode.value = 'DELETE_ROW';
	document.mainform.del_id.value = del_id;
	document.mainform.submit();
}

function updateLinks() {
	if (document.mainform.elements['link_nm[]']) {
		for (var i = 0, j = document.mainform.elements['link_nm[]'].length; i < j; i++) {
			if (document.mainform.elements['link_nm[]'][i].value == '') {
				continue;
			}

			if (document.mainform.elements['link_url[]'][i].value == '') {
				alert('URL・プログラムを入力してください。');
				document.mainform.elements['link_url[]'][i].focus();
				return false;
			}
		}
	}

	document.mainform.action = 'link_admin_link_setting_exe.php';
	return true;
}

// ページ有効/無効でdisabled切り替え
function togglePageDisabled(self, page_id, row_id) {
	document.getElementById('icon_flg'+row_id+'_'+page_id).disabled = ! self.checked;
	document.getElementById('order'+row_id+'_'+page_id).disabled = ! self.checked;
}

// アイコン追加
function attachIcon(link_id) {
	window.open('link_admin_link_icon_attach.php?session=<?e($session)?>&link_id='+link_id, 'newwin', 'width=400,height=200,scrollbars=yes');
}

// アイコン削除
function dettachIcon(link_id) {
	document.mainform.del_id.value = link_id;
	document.mainform.action = 'link_admin_link_icon_attach_remove.php';
	document.mainform.submit();
}

// パラメータ設定
function paramSetting(link_id) {
	window.open('link_admin_link_param.php?session=<?e($session)?>&link_id='+link_id, 'newwin', 'width=600,height=500,scrollbars=yes');
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="link_menu.php?session=<? echo($session); ?>"><img src="img/icon/b40.gif" width="32" height="32" border="0" alt="リンクライブラリ"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="link_menu.php?session=<? echo($session); ?>"><b>リンクライブラリ</b></a> &gt; <a href="link_admin_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="link_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="140" align="center" bgcolor="#bdd1e7"><a href="link_admin_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カテゴリー表示設定</font></a></td>
<td width="5">&nbsp;</td>
<td width="140" align="center" bgcolor="#5279a5"><a href="link_admin_link_setting.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>リンク名表示設定</b></font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<? if (count($categories) == 0) { ?>
<table width="1000" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カテゴリーが登録されていません。「カテゴリー表示設定」タブよりご登録ください。</font></td>
</tr>
</table>
<? } else { ?>
<form name="mainform" method="post" onsubmit="return updateLinks();">
<table width="1000" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td><? write_category_box($categories, $linkcate_id); ?></td>
</tr>
</table>
<table width="1000" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td valign="bottom"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
※編集内容は更新ボタンを押すまで確定されません。<br>
※パラメータ設定、アイコン登録は更新を一度行うと設定可能になります。<br>
※プログラム起動はWindows IE限定です。<br>
※プログラムパスに空白が含まれる場合は、パスの前後をダブルクオートで囲ってください。(例) <code>"C:\Program Files\Mozilla Firefox\firefox.exe"</code><br>
</font></td>
<td align="right"><input type="button" value="行を追加" onclick="addRow();"></td>
</tr>
</table>
<table width="1000" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff" align="center">
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">リンク名</font></td>
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">種類</font></td>
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">パラメータ</font></td>
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">アイコン</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示</font></td>
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">削除</font></td>
</tr>
<tr height="22" bgcolor="#f6f9ff" align="center">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ページ</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">アイコン</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">順番</font></td>
</tr>
<? write_rows($links); ?>
</table>
<table width="1000" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="更新"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="mode" value="">
<input type="hidden" name="del_id" value="">
</form>
<? } ?>
</td>
</tr>
</table>
</body>
</html>
<?
pg_close($con);

// カテゴリ・プルダウンボックス表示
function write_category_box($categories, $selected_id) {
	echo("<select name=\"linkcate_id\" onchange=\"changeCategory(this.value);\">\n");
	foreach ($categories as $category) {
		echo("<option value=\"{$category["linkcate_id"]}\"");
		if ($selected_id == $category["linkcate_id"]) {
			echo(" selected");
		}
		echo(">{$category["linkcate_nm"]}\n");
	}
	echo("</select>\n");
}

// リンクデータ追加
function add_row(
	$link_id="", 
	$link_type=1, 
	$link_nm="", 
	$link_url="", 
	$link_note="", 
	$link_icon="",
	$note_flg="", 
	$show_flg1="t", 
	$order1="", 
	$icon_flg1="",
	$show_flg2="t", 
	$order2="", 
	$icon_flg2="",
	$show_flg3="t", 
	$order3="",
	$icon_flg3="",
	$exist=0
	) {
	return array(
			"link_id"	=> $link_id,
			"link_type" => $link_type,
			"link_nm"	=> $link_nm,
			"link_url"	=> $link_url,
			"link_note" => $link_note,
			"icon"		=> $link_icon,
			"note_flg"	=> $note_flg,
			"show_flg1" => $show_flg1,
			"order1"	=> $order1,
			"icon_flg1" => $icon_flg1,
			"show_flg2" => $show_flg2,
			"order2"	=> $order2,
			"icon_flg2"	=> $icon_flg2,
			"show_flg3" => $show_flg3,
			"order3"	=> $order3,
			"icon_flg3"	=> $icon_flg3,
			"exist"		=> $exist
			);
}

// 一覧行・表示
function write_rows($links) {
	foreach ($links as $i => $link) {
		?>
		<tr height="22">
		<td align="right" rowspan="3">
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
		名称
		<input type="text" name="link_nm[<? eh($i);?>]" value="<? eh($link["link_nm"]);?>" size="60" style="ime-mode:active;"><br>
		URL・プログラム
		<input type="text" name="link_url[<? eh($i);?>]" value="<? eh($link["link_url"]);?>" size="60" style="ime-mode:inactive;"><br>
		備考
		<input type="text" name="link_note[<? eh($i);?>]" value="<? eh($link["link_note"]);?>" size="60" style="ime-mode:active;"><br>
		備考をリンクライブラリ画面に表示<input type="checkbox" name="note_flg[<? eh($i);?>]" value="t" <? if($link["note_flg"]=="t") echo "checked";?>>
		</font>
		</td>
		<td rowspan="3">
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
		<label><input type="radio" name="link_type[<? eh($i);?>]" value="1" <? if($link["link_type"]=="1") echo "checked";?>> URL(GET)</label><br>
		<label><input type="radio" name="link_type[<? eh($i);?>]" value="3" <? if($link["link_type"]=="3") echo "checked";?>> URL(POST)</label><br>
		<label><input type="radio" name="link_type[<? eh($i);?>]" value="2" <? if($link["link_type"]=="2") echo "checked";?>> プログラム起動</label><br>
		</font>
		</td>
		<td rowspan="3" align="center">
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
		<input type="button" value="パラメータ設定" onclick="paramSetting('<? eh($link["link_id"]);?>');" <? if($link["exist"]==0) echo "disabled";?>>
		</font>
		</td>
		<td rowspan="3" align="center">
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
		<? if($link["icon"]) {?><img src="link/icon/<? eh($link["icon"]);?>" border="0" alt="アイコン"><br><? } ?>
		<input type="button" value="登録" onclick="attachIcon('<? eh($link["link_id"]);?>');" <? if($link["exist"]==0) echo "disabled";?>><br>
		<input type="button" value="削除" onclick="dettachIcon('<? eh($link["link_id"]);?>');" <? if($link["exist"]==0 or empty($link["icon"])) echo "disabled";?>><br>
		</font>
		<input type="hidden" name="link_icon[<? eh($i);?>]" value="<? eh($link["icon"]);?>">
		</td>
		
		<td>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
		<label><input type="checkbox" name="show_flg1[<? eh($i);?>]" value="t" <? if($link["show_flg1"]=="t") echo "checked";?> onclick="togglePageDisabled(this,<? eh($i);?>,1);"> ログインページ</label>
		</font>
		</td>
		<td align="center">
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
		<label><input id="icon_flg1_<? eh($i);?>" type="checkbox" name="icon_flg1[<? eh($i);?>]" value="t" <? if($link["icon_flg1"]=="t") echo "checked";?><? if($link["show_flg1"]!="t") echo "disabled";?>>表示</label>
		</font>
		</td>
		<td align="center">
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
		<input id="order1_<? eh($i);?>" type="text" size="3" name="order1[<? eh($i);?>]" value="<? eh($link["order1"]);?>" <? if($link["show_flg1"]!="t") echo "disabled";?>>
		</font>
		</td>
		<td rowspan="3" align="center">
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
		<input type="button" value="削除" onclick="deleteRow('<? eh($i);?>');">
		</font>
		</td>
		</tr>
		<tr>
		<td>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
		<label><input type="checkbox" name="show_flg2[<? eh($i);?>]" value="t" <? if($link["show_flg2"]=="t") echo "checked";?> onclick="togglePageDisabled(this,<? eh($i);?>,2);"> マイページ</label>
		</font>
		</td>
		<td align="center">
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
		<label><input id="icon_flg2_<? eh($i);?>" type="checkbox" name="icon_flg2[<? eh($i);?>]" value="t" <? if($link["icon_flg2"]=="t") echo "checked";?><? if($link["show_flg2"]!="t") echo "disabled";?>>表示</label>
		</font>
		</td>
		<td align="center">
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
		<input id="order2_<? eh($i);?>" type="text" size="3" name="order2[<? eh($i);?>]" value="<? eh($link["order2"]);?>"<? if($link["show_flg2"]!="t") echo "disabled";?>>
		</font>
		</td>
		</tr>		
		<tr>
		<td>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
		<label><input type="checkbox" name="show_flg3[<? eh($i);?>]" value="t" <? if($link["show_flg3"]=="t") echo "checked";?> onclick="togglePageDisabled(this,<? eh($i);?>,3);"> リンクライブラリ</label>
		</font>
		</td>
		<td align="center">
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
		<label><input id="icon_flg3_<? eh($i);?>" type="checkbox" name="icon_flg3[<? eh($i);?>]" value="t" <? if($link["icon_flg3"]=="t") echo "checked";?><? if($link["show_flg3"]!="t") echo "disabled";?>>表示</label>
		</font>
		</td>
		<td align="center">
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
		<input id="order3_<? eh($i);?>" type="text" size="3" name="order3[<? eh($i);?>]" value="<? eh($link["order3"]);?>"<? if($link["show_flg3"]!="t") echo "disabled";?>>
		</font>
		</td>
		</tr>
		<input type="hidden" name="link_id[<? eh($i);?>]" value="<? eh($link["link_id"]);?>">
		<input type="hidden" name="exist[<? eh($i);?>]" value="<? eh($link["exist"]);?>">
		<?
	}
}