<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require_once("about_comedix.php");
require("get_values.ini");
require("sinryo_common.ini");
require("label_by_profile_type.ini");
require_once("get_menu_label.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 59, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
$con = connect2db($fname);

// 職員ID取得
$emp_id=get_emp_id($con,$session,$fname);

// ログインID取得
$emp_login_id = get_login_id($con,$emp_id,$fname);

// guest*ユーザはテンプレートを登録できない
$can_regist_flg = true;
if (substr($emp_login_id, 0, 5) == "guest") {
	$can_regist_flg = false;
}

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// メドレポート/ＰＯレポート(Problem Oriented)
$med_report_title = get_report_menu_label($con, $fname);

if (@$_REQUEST["tmpl_name_for_remark"]){
	$sql = "delete from sum_tmpl_remark where tmpl_name = '".pg_escape_string($_REQUEST["tmpl_name_for_remark"])."'";
	$del = delete_from_table($con, $sql, "", $fname);
	$sql =
		"insert into sum_tmpl_remark (tmpl_name, remark) values (".
		"'".pg_escape_string($_REQUEST["tmpl_name_for_remark"])."', '".pg_escape_string($_REQUEST["remark"])."'";
	$ins = insert_into_table($con, $sql, array(), $fname);
}

?>
<title>CoMedix <? echo($med_report_title); ?>管理｜テンプレートファイル参照</title>
<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript">
function submitSetForm() {

	if (document.tmpl.elements['trashbox[]'] == undefined) {
		alert('選択可能なデータが存在しません。');
		return;
	}

	if (window.opener && !window.opener.closed && window.opener.document.tmpl) {
		;
	} else {
		alert('設定する画面が存在しません。');
		self.close();
		return;
	}
	var check_cnt = 0;
	if (document.tmpl.elements['trashbox[]'].length == undefined) {
		if (document.tmpl.elements['trashbox[]'].checked) {
			check_cnt++;
			document.tmpl.tmpl_file.value = document.tmpl.elements['trashbox[]'].value;
		}
	} else {
		for (var i = 0, j = document.tmpl.elements['trashbox[]'].length; i < j; i++) {
			if (document.tmpl.elements['trashbox[]'][i].checked) {
				check_cnt++;
				document.tmpl.tmpl_file.value = document.tmpl.elements['trashbox[]'][i].value;
			}
		}
	}

	if (check_cnt==0) {
		alert('対象が選択されていません。');
		return;
	}
	if (check_cnt>1) {
		alert('1件だけ選択してください。');
		return;
	}

	document.tmpl.action = "sinryo_tmpl_refer_set.php";
	document.tmpl.submit();
}

function submitDeleteForm() {

	if (document.tmpl.elements['trashbox[]'] == undefined) {
		alert('選択可能なデータが存在しません。');
		return;
	}

	var check_cnt = 0;
	if (document.tmpl.elements['trashbox[]'].length == undefined) {
		if (document.tmpl.elements['trashbox[]'].checked) {
			check_cnt++;
		}
	} else {
		for (var i = 0, j = document.tmpl.elements['trashbox[]'].length; i < j; i++) {
			if (document.tmpl.elements['trashbox[]'][i].checked) {
				check_cnt++;
				break;
			}
		}
	}

	if (check_cnt==0) {
		alert('対象が選択されていません。');
		return;
	}

	document.tmpl.action = "sinryo_tmpl_refer_delete.php";
	if (confirm("削除してよろしいですか？")) {
		document.tmpl.submit();
	}
}
function updateRemark(tmpl_name, remark_id) {
	document.tmpl_remark.tmpl_name_for_remark.value = tmpl_name;
	document.tmpl_remark.remark.value = document.getElementById(remark_id).value;
	document.tmpl_remark.submit();
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<table width="680" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#5279a5">
<td width="6000" height="32" class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>テンプレートファイル参照</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form action="sinryo_tmpl_refer.php?session=<?=$session?>" method="post" name="tmpl_remark">
	<input type="hidden" name="tmpl_name_for_remark" value=""/>
	<input type="hidden" name="remark" value=""/>
</form>
<form action="sinryo_tmpl_refer_add.php" method="post" enctype="multipart/form-data" name="tmpl">
<table width="680" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="120" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">テンプレートファイル</font></td>
<td width="540"><nobr>
<input type="file" name="template_file" size="56">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">※<? echo(ini_get("upload_max_filesize")); ?>まで&nbsp;<input type="submit" value="ファイル追加" name="add"></font></nobr>
</td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="680" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right">
<input type="button" value="選択" onclick="submitSetForm();">&nbsp;&nbsp;
<input type="button" value="削除" onclick="submitDeleteForm();">
</td>
</tr>
<tr>
<td aligh="left">

<table width="100%" border="0" cellspacing="0" cellpadding="1" class="list">
<tr bgcolor="#f6f9ff">
<td width="48" height="22">&nbsp;</td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">テンプレートファイル</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">備考</font></td>
</tr>
<?

$arr_filename = get_tmpl_file_list();
$arr_remark = array();
$sel = select_from_table($con, "select * from sum_tmpl_remark", "", $fname);
while($row = pg_fetch_array($sel)) {
	$arr_remark[$row["tmpl_name"]] = $row["remark"];
}
?>
<? for($i=0; $i<count($arr_filename); $i++) { ?>
<?   $fn = $arr_filename[$i]; ?>
		<tr>
		<td height="22" width="5%" align="center"><input name="trashbox[]" type="checkbox" value="<?=$fn?>"></td>
		<td width="40%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$fn?></font></td>
		<td width="55%" style="white-space:nowrap">
			<input type="text" id="remark_<?=$i?>" value="<?=h(@$arr_remark[$fn])?>" maxlength="200" style="width:350px"/>
			<input type="button" value="備考更新" onclick="updateRemark('<?=$fn?>', 'remark_<?=$i?>');" style="width:64px" />
		</td>
		</tr>
<? } ?>
</table>

</td>
</tr>
</table>
</center>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="mode" value="<? echo($mode); ?>">
<input type="hidden" name="tmpl_file" value="">
</form>
<?
if ($can_regist_flg == false) {
?>
<script type="text/javascript">
document.tmpl.add.disabled = true;
</script>
<? } ?>

</body>
<? pg_close($con); ?>
</html>
