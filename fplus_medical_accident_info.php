<?
ob_start();
require_once("about_comedix.php");
require_once("fplus_common.ini");
require_once("fplus_common_class.php");

ob_end_clean();

$fname = $PHP_SELF;
$session = qualify_session($session,$fname);
$fplusadm_auth = check_authority($session, 79, $fname);
$con = @connect2db($fname);

$obj = new fplus_common_class($con, $fname);

if(!$session || !$fplusadm_auth || !$con){
  echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
  exit;
}


$font = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';

//====================================================================
// パラメータ取得
//====================================================================

//登録ボタン押下時に"1"が入る
$insert_mode = @$_REQUEST["insert_mode"];

// 印刷ボタンを押した時に"1"が入る
$is_print_mode = @$_REQUEST["print_mode"];

//$title = @$_request["hdn_medical_accident_title_".$row_no];
//print_r($_REQUEST);
// 出力ボタンを押した時に"1"が入る
//$is_download_mode = @$_REQUEST["download_mode"];

//====================================================================
// DB操作
//====================================================================
// 登録処理
if($insert_mode == "1"){

	// トランザクション開始
	pg_query($con, "begin");

	$medical_accident_info_id = @$_REQUEST["medical_accident_info_id"];
	$medical_accident_id = @$_REQUEST["medical_accident_id"];

	if($medical_accident_info_id != ""){
		//修正処理
		$set_data = array();
		$set_key = array();
		$set_data[0] = $medical_accident_info_id;
		$set_key[0] = "medical_accident_info_id";
		$set_data[1] = date("Y-m-d H:i:s");
		$set_key[1] = "update_time";
		$set_data[2] = "f";
		$set_key[2] = "del_flg";

		$wcnt0 = 3;

		foreach($_REQUEST as $epi_key => $epi_val)
		{
			$chk_key = substr($epi_key,0, 5);

			if($chk_key == "data_")
			{
				$epi_key = substr($epi_key,5);
				
				if(is_array($epi_val))
				{
					//入力データcheckbox,radio用
					$set_data[$wcnt0] = pg_escape_string($epi_val[0]);
					$set_key[$wcnt0] = $epi_key;
					$wcnt0++;
				}
				else
				{
					//入力データ
					$set_data[$wcnt0] = pg_escape_string($epi_val);
					$setdata .=pg_escape_string($epi_val);
					$set_key[$wcnt0] = $epi_key;
					$wcnt0++;
				}
			}
		}

		//データの修正処理(医療事故情報)
		$obj->update_medical_accident_info($medical_accident_info_id,$set_key,$set_data);

		// トランザクションをコミット
		pg_query($con, "commit");

		echo("<script language=\"javascript\">if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}</script>");
		echo("<script language=\"javascript\">window.close();</script>\n");

	}else{
		// 登録処理
		// 医療事故情報ID採番
		$sql = "SELECT max(medical_accident_info_id) FROM fplus_medical_accident_info";
		$cond = "";
		$info_id_sel = select_from_table($con,$sql,$cond,$fname);
		$info_id = pg_result($info_id_sel,0,"max");
		if($info_id == ""){
			$info_id = 1;
		}else{
			$info_id = $info_id + 1;
		}

		$set_data = array();
		$set_key = array();
		$set_data[0] = $info_id;
		$set_key[0] = "medical_accident_info_id";
		$set_data[1] = @$_REQUEST["medical_accident_id"];
		$set_key[1] = "medical_accident_id";
		$set_data[2] = date("Y-m-d H:i:s");
		$set_key[2] = "update_time";
		$set_data[3] = "f";
		$set_key[3] = "del_flg";

		$wcnt0 = 4;

		foreach($_REQUEST as $epi_key => $epi_val)
		{
			$chk_key = substr($epi_key,0, 5);

			if($chk_key == "data_")
			{
				$epi_key = substr($epi_key,5);
				
				if(is_array($epi_val))
				{
					//入力データcheckbox,radio用
					$set_data[$wcnt0] = pg_escape_string($epi_val[0]);
					$set_key[$wcnt0] = $epi_key;
					$wcnt0++;
				}
				else
				{
					//入力データ
					$set_data[$wcnt0] = pg_escape_string($epi_val);
					$setdata .=pg_escape_string($epi_val);
					$set_key[$wcnt0] = $epi_key;
					$wcnt0++;
				}
			}
		}

		//データの登録処理(医療事故情報)
		$obj->regist_medical_accident_info($set_key,$set_data);

		// トランザクションをコミット
		pg_query($con, "commit");

		echo("<script language=\"javascript\">if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}</script>");
		echo("<script language=\"javascript\">window.close();</script>\n");
	}
}

// 検索
if($medical_accident_info_id != ""){
	// 医療事故報告検索
	$sql = " SELECT ".
		" 	info.* ".
		" 	, list.title ".
		" FROM ".
		" 	(SELECT ".
		" 		am.medical_accident_id ".
		" 		,(CASE ".
		" 			WHEN s.title IS NULL THEN CASE ".
		" 					WHEN f.title IS NULL THEN im.title ".
		" 					ELSE f.title ".
		" 				     END ".
		" 			ELSE s.title ".
		" 		END) as title ".
		" 	FROM ".
		" 		(((fplus_medical_accident_report_management am left outer join (SELECT im.* FROM fplus_immediate_report im, fplusapply apl WHERE im.apply_id = apl.apply_id AND apl.delete_flg = 'f' AND apl.apply_stat = '1') im on am.medical_accident_id = im.medical_accident_id) ".
		" 		  left outer join (SELECT f.* FROM fplus_first_report f, fplusapply apl WHERE f.apply_id = apl.apply_id AND apl.delete_flg = 'f' AND apl.apply_stat = '1') f on am.medical_accident_id = f.medical_accident_id) ".
		" 		  left outer join (SELECT s.* FROM fplus_second_report s, fplusapply apl WHERE s.apply_id = apl.apply_id AND apl.delete_flg = 'f' AND apl.apply_stat = '1') s on am.medical_accident_id = s.medical_accident_id) ".
		" 	WHERE ".
		" 		(im.apply_id is not null ".
		" 		OR f.apply_id is not null ".
		" 		OR s.apply_id is not null) ".
		" 		AND am.del_flg <> 't') list ".
		" 	, fplus_medical_accident_info info ".
		" WHERE ".
		" 	info.medical_accident_id = list.medical_accident_id ".
		" 	AND info.medical_accident_id = '$medical_accident_id' ".
		" 	AND info.medical_accident_info_id = '$medical_accident_info_id' ";

	$sel = @select_from_table($con, $sql, "", $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$result = pg_fetch_all($sel);

}else{
	// 第二報検索
	$sql = " SELECT ".
		" 	s.medical_accident_id ".
		" 	, s.occurrences_date ".
		" 	, s.title ".
		" 	, s.patient_name ".
		" 	, CASE s.patient_sex ".
		" 		WHEN '1' THEN '男性' ".
		" 		ELSE '女性' ".
		" 	  END AS patient_sex ".
		" 	, s.patient_age ".
		" 	, CASE s.patient_class ".
		" 		WHEN '1' THEN '入院' ".
		" 		ELSE '外来' ".
		" 	  END AS patient_class ".
		" 	, mst.item_name AS clinical_departments ".
		" 	, s.main_disease ".
		" 	, s.fact_progress ".
		" 	, s.accident_cope ".
		" 	, s.patient_family_cope ".
		" 	, s.accident_report ".
		" 	, s.occurrences_cause AS occurrences_factor ".
		" 	, s.remedy ".
		" FROM ".
		" 	fplus_second_report s ".
		" 	, fplusapply app ".
		" 	, (SELECT item_cd, item_name ".
		" 	   FROM tmplitem ".
		" 	   WHERE mst_cd = 'A402' AND disp_flg = 't') mst ".
		" WHERE ".
		" 	s.medical_accident_id = '$medical_accident_id' ".
		" 	AND s.apply_id = app.apply_id ".
		" 	AND app.delete_flg = 'f' ".
		" 	AND s.clinical_departments = mst.item_cd ";

	$sel = @select_from_table($con, $sql, "", $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$result = pg_fetch_all($sel);
}

//患者背景の編集
$patient_background = "";
if($medical_accident_info_id != ""){
	$patient_background .= $result[0]["patient_background"];
}else{
	if($result[0]["patient_name"]!=""){
		$patient_background .= "患者氏名：".$result[0]["patient_name"].",";
	}
	if($result[0]["patient_sex"]!=""){
		$patient_background .= "性別：".$result[0]["patient_sex"]."\n";
	}
	if($result[0]["patient_age"]!=""){
		$patient_background .= "年齢：".$result[0]["patient_age"]."歳,";
	}
	if($result[0]["patient_class"]!=""){
		$patient_background .= "入外別：".$result[0]["patient_class"]."\n";
	}
	if($result[0]["clinical_departments"]!=""){
		$patient_background .= "診療科：".$result[0]["clinical_departments"]."\n";
	}
	if($result[0]["main_disease"]!=""){
		$patient_background .= "主たる病名：".$result[0]["main_disease"];
	}
}

$hdn_title = @$_REQUEST["hdn_title"];

//====================================================================
// 印刷処理。
//====================================================================

//====================================================================
// 出力（XLSエクセル出力）の場合。内容を出力してdieする。
// もし失敗すれば、ブラウザ上に内容が表示されるであろう。
//====================================================================

//====================================================================
// HTMLヘッダ
//====================================================================

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ファントルくん＋ | 医療事故情報作成</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list1 {border-collapse:collapse;}
.list1 td {border:#E6B3D4 solid 1px; padding:3px; }
.list2 {border-collapse:collapse;}
.list2 td {border:#dfd95e solid 1px; padding:3px; }
</style>

<? //-------------------------------------------------------------------------?>
<? // JavaScript                                                              ?>
<? //-------------------------------------------------------------------------?>
<script type="text/javascript">

	function load(){
		<? if($hdn_title == ""){ ?>
			document.getElementById("hdn_title").value = opener.document.getElementById("hdn_medical_accident_title_<?=$row_no?>").value;
		<? } ?>
	}

	<? // ************* 出力開始 ************* ?>
	function startDownload(){
		document.frm.action = 'fplus_medical_accident_info_word.php';
		document.frm.print_mode.value = "";
		document.frm.download_mode.value = "1";
		document.frm.insert_mode.value = "";
		document.frm.target = "print_window";
		document.frm.submit();

		return false;
	}
	
	<? // ************* 印刷処理 ************* ?>
	function startPrint() {
		//印刷モードON
		document.frm.action = 'fplus_medical_accident_info_print.php';
		document.frm.print_mode.value = "1";
		document.frm.download_mode.value = "";
		document.frm.insert_mode.value = "";
		document.frm.target = "_self";
		document.frm.submit();
		
		return false;
	}


	<? // ************* 登録処理 ************* ?>
	function startInsert(){
		if (window.InputCheck) {
			if (!InputCheck()) {
				return;
			}
		}

		if (confirm('登録します。よろしいですか？')) {
			//登録モードON
			document.frm.action = 'fplus_medical_accident_info.php';
			document.frm.print_mode.value = "";
			document.frm.download_mode.value = "";
			document.frm.insert_mode.value = "1";
			document.frm.target = "_self";
			document.frm.submit();
			return false;
		}
	}
	<? // ************* 画面の幅の取得 ************* ?>
	function getClientWidth(){
		if (document.body.clientWidth) return document.body.clientWidth;
		if (window.innerWidth) return window.innerWidth;
		if (document.documentElement && document.documentElement.clientWidth) {
			return document.documentElement.clientWidth;
		}
		return 0;
	}
</script>

</head>
<body style="margin:0" onload="load();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#E6B3D4">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>医療事故情報</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>

<? //-------------------------------------------------------------------------?>
<? // 入力フォーム描画開始                                                    ?>
<? //-------------------------------------------------------------------------?>
<form name="frm" id="frm" action="fplus_medical_accident_info.php" method="post" enctype="multipart/form-data">
  <input type="hidden" id="session" name="session" value="<? echo($session); ?>" />
  <input type="hidden" id="print_mode" name="print_mode" value="" />
  <input type="hidden" id="download_mode" name="download_mode" value="" />
  <input type="hidden" id="insert_mode" name="insert_mode" value="" />
  <input type="hidden" id="medical_accident_id" name="medical_accident_id" value="<? echo($medical_accident_id); ?>" />
  <input type="hidden" id="medical_accident_info_id" name="medical_accident_info_id" value="<? echo($medical_accident_info_id); ?>" />
  <input type="hidden" name="hdn_title" id="hdn_title" value="<?=$hidn_title ?>" />

<div style="margin-top:4px"><?=$font?>
	<? // ************* ボタン ******************** ?>
	<div style="float:right;">
		<table>
			<tr>
				<td>
					<input type="button" onclick="startDownload();" value="出力"  />
				</td>
				<td>
					<input type="button" onclick="startPrint();" value="印刷"  />
				</td>
				<td>
					<input type="button" onclick="startInsert();" value="登録"  />
				</td>
			</tr>
		</table>
	</div>
	<br style="clear:both"/>
	</font>
</div>
<table style="width:100%;">
	<tr>
		<td style="width:2%;"></td>
		<td style="width:96%;" align="center">
			<table style="width:100%;" class="list1">
				<tr>
					<td style="width:30%; border-right:0;border-bottom:0;"></td>
					<td style="width:40%; border-left:0;border-right:0;border-bottom:0;" align="center">
						<h2>医療事故情報（案）</h2>
					</td>
					<td style="width:30%;border-left:0;border-bottom:0;">
					</td>
				</tr>
				<tr>
					<td style="width:100%; border-top:0;" colspan="3">
						<table style="width:100%;" border="0">
							<tr>
								<td style="width:2%; border:0;"></td>
								<td style="width:96%; border:0;"><?=$font?>1. 事故概要</font></td>
								<td style="width:2%; border:0;"></td>
							</tr>
							<tr>
								<td style="width:2%; border:0;"></td>
								<td style="width:96%; border:0;"><?=$font?>&nbsp;&nbsp;［患者背景］</font></td>
								<td style="width:2%; border:0;"></td>
							</tr>
							<tr>
								<td style="width:2%; border:0;"></td>
								<td style="width:96%; border:0;">
									<table style="width:100%;" border="0">
										<tr>
											<td style="width:3%; border:0;"></td>
											<td style="width:97%; border:0;"><?=$font?>
												<textarea id="data_patient_background" name="data_patient_background" style="width:100%; height: 80px;"><?=h($patient_background)?></textarea>
											</font></td>
										</tr>
									</table>
								</td>
								<td style="width:2%; border:0;"></td>
							</tr>
							<tr>
								<td style="width:2%; border:0;"></td>
								<td style="width:96%; border:0;"><?=$font?>&nbsp;&nbsp;［事実経過］</font></td>
								<td style="width:2%; border:0;"></td>
							</tr>
							<tr>
								<td style="width:2%; border:0;"></td>
								<td style="width:96%; border:0;">
									<table style="width:100%;" border="0">
										<tr>
											<td style="width:3%; border:0;"></td>
											<td style="width:97%; border:0;"><?=$font?>
												<textarea id="data_fact_progress" name="data_fact_progress" style="width:100%; height: 80px;"><?=h($result[0]["fact_progress"])?></textarea>
											</font></td>
										</tr>
									</table>
								</td>
								<td style="width:2%; border:0;"></td>
							</tr>
							<tr>
								<td style="width:2%; border:0;"></td>
								<td style="width:96%; border:0;"><?=$font?>2. 事故発生後の対応</font></td>
								<td style="width:2%; border:0;"></td>
							</tr>
							<tr>
								<td style="width:2%; border:0;"></td>
								<td style="width:96%; border:0;">
									<table style="width:100%;" border="0">
										<tr>
											<td style="width:3%; border:0;"></td>
											<td style="width:97%; border:0;><?=$font?>
												<textarea id="data_accident_cope" name="data_accident_cope" style="width:100%;height:50px;"><?=h($result[0]["accident_cope"])?></textarea>
											</font></td>
										</tr>
									</table>
								</td>
								<td style="width:2%; border:0;"></td>
							</tr>
							<tr>
								<td style="width:2%; border:0;"></td>
								<td style="width:96%; border:0;"><?=$font?>3. 患者・家族への対応</font></td>
								<td style="width:2%; border:0;"></td>
							</tr>
							<tr>
								<td style="width:2%; border:0;"></td>
								<td style="width:96%; border:0;">
									<table style="width:100%;" border="0">
										<tr>
											<td style="width:3%; border:0;"></td>
											<td style="width:97%; border:0;><?=$font?>
												<textarea id="data_patient_family_cope" name="data_patient_family_cope" style="width:100%;height:50px;"><?=h($result[0]["patient_family_cope"])?></textarea>
											</font></td>
										</tr>
									</table>
								</td>
								<td style="width:2%; border:0;"></td>
							</tr>
							<tr>
								<td style="width:2%; border:0;"></td>
								<td style="width:96%; border:0;"><?=$font?>4. 事故発生要因</font></td>
								<td style="width:2%; border:0;"></td>
							</tr>
							<tr>
								<td style="width:2%; border:0;"></td>
								<td style="width:96%; border:0;">
									<table style="width:100%;" border="0">
										<tr>
											<td style="width:3%; border:0;"></td>
											<td style="width:97%; border:0;"><?=$font?>
												<textarea id="data_occurrences_factor" name="data_occurrences_factor" style="width:100%;height:50px;"><?=h($result[0]["occurrences_factor"])?></textarea>
											</font></td>
										</tr>
									</table>
								</td>
								<td style="width:2%; border:0;"></td>
							</tr>
							<tr>
								<td style="width:2%; border:0;"></td>
								<td style="width:96%; border:0;"><?=$font?>5. 再発防止の改善策</font></td>
								<td style="width:2%; border:0;"></td>
							</tr>
							<tr>
								<td style="width:2%; border:0;"></td>
								<td style="width:96%; border:0;">
									<table style="width:100%;" border="0">
										<tr>
											<td style="width:3%; border:0;"></td>
											<td style="width:97%; border:0;"><?=$font?>
												<textarea id="data_remedy" name="data_remedy" style="width:100%;height:50px;"><?=h($result[0]["remedy"])?></textarea>
											</font></td>
										</tr>
									</table>
								</td>
								<td style="width:2%; border:0;"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
		<td style="width:2%;"></td>
	</tr>
</table>
</form>
</td>
</tr>
</table>
</body>
</html>