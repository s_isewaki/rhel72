<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 職員登録 | 権限管理</title>
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("label_by_profile_type.ini");
require_once("get_cas_title_name.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$empif = check_authority($session,18,$fname);
if($empif == "0"){
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 職員登録権限を取得
$reg = check_authority($session, 19, $fname);

// データベースに接続
$con = connect2db($fname);

// 登録値の取得
$sql = "select * from license";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// グループ一覧を配列に格納
$sql = "select group_id, group_nm from authgroup";
$cond = "order by group_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$groups = array();
while ($row = pg_fetch_array($sel)) {
	$groups[$row["group_id"]] = $row["group_nm"];
}
if ($group_id == "") {
	$group_id = key($groups);
}

// 権限グループを表示する場合
if ($group_id != "") {
	// 表示対象の権限グループ情報を取得
	$sql = "select * from authgroup";
	$cond = "where group_id = $group_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	foreach ($groups as $tmp_group_id => $tmp_group_nm) {
		$sql = "select * from empmst";
		$cond = "left join authmst on authmst.emp_id=empmst.emp_id left join authgroup on authgroup.group_id=authmst.group_id where authmst.emp_del_flg='f' and authmst.group_id=$tmp_group_id";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$tmp_group["$tmp_group_id"] = pg_num_rows($sel);
	}

	// 組織タイプを取得
	$profile_type = get_profile_type($con, $fname);

	// イントラメニュー名を取得
	$sql = "select * from intramenu";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$intra_menu1 = pg_fetch_result($sel, 0, "menu1");
	$intra_menu2_4 = pg_fetch_result($sel, 0, "menu2_4");
	$intra_menu3 = pg_fetch_result($sel, 0, "menu3");

	// 社会福祉法人の場合「当直・外来分担表」を「当直分担表」とする
	if ($profile_type == PROFILE_TYPE_WELFARE) {
		$intra_menu2_4 = str_replace("・外来", "", $intra_menu2_4);
	}
}

// ライセンス情報の取得
$sql = "select * from license";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

$arr_func = array();
$basic_user = pg_fetch_result($sel, 0, "lcs_basic_user");
$fantol_user = pg_fetch_result($sel, 0, "lcs_fantol_user");
$fplus_user = pg_fetch_result($sel, 0, "lcs_fplus_user");
$baritess_user = pg_fetch_result($sel, 0, "lcs_baritess_user");
$report_user = pg_fetch_result($sel, 0, "lcs_report_user");
$intra_user = pg_fetch_result($sel, 0, "lcs_intra_user");
$nursing_user = pg_fetch_result($sel, 0, "lcs_nursing_user");
$bed_count = pg_fetch_result($sel, 0, "lcs_bed_count");
$shift_emps = pg_fetch_result($sel, 0, "lcs_shift_emps");
$jinji_emps = pg_fetch_result($sel, 0, "lcs_jinji_emps");
$ladder_user = pg_fetch_result($sel, 0, "lcs_ladder_user");
$career_user = pg_fetch_result($sel, 0, "lcs_career_user");
$jnl_user = pg_fetch_result($sel, 0, "lcs_jnl_user");
$ccusr1_user = pg_fetch_result($sel, 0, "lcs_ccusr1_user");
for ($i = 1; $i <= 60; $i++) {
	$arr_func[$i] = pg_fetch_result($sel, 0, "lcs_func$i");
}

// countだと処理が遅い
$bsc_clm = array(
				"emp_webml_flg",	// ウェブメール
				"emp_schd_flg",		// スケジュール
				"emp_pjt_flg",		// 委員会・WG
				"emp_newsuser_flg",	// お知らせ・回覧板
				"emp_work_flg",		// タスク
				"emp_phone_flg",	// 伝言メモ
				"emp_resv_flg",		// 設備予約
				"emp_attdcd_flg",	// 出勤表
				"emp_aprv_flg",		// 決裁・申請
				"emp_conf_flg",		// ネットカンファレンス
				"emp_bbs_flg",		// 掲示板・電子会議室
				"emp_qa_flg",		// Q&A
				"emp_lib_flg",		// 文書管理
				"emp_adbk_flg",		// アドレス帳
				"emp_ext_flg",		// 内線電話帳
				"emp_link_flg",		// リンクライブラリ
				"emp_cmt_flg",		// コミュニティサイト
				"emp_memo_flg"		// 備忘録
				);
$basic_usr		= lc_check_user_count($con, $basic_user, $bsc_clm, $fname);		// 基本機能の人数

// 基本機能の現ライセンス使用数を確認
$webml_usr		= lc_check_user_count($con, $basic_user, "emp_webml_flg", $fname);	// ウェブメール
$webmladm_usr	= lc_check_user_count($con, $basic_user, "emp_webmladm_flg", $fname);	// ウェブメール[管理者]
$schd_usr		= lc_check_user_count($con, $basic_user, "emp_schd_flg", $fname);	// スケジュール
$schdplc_usr	= lc_check_user_count($con, $basic_user, "emp_schdplc_flg", $fname);	// スケジュール[管理者]
$pjt_usr		= lc_check_user_count($con, $basic_user, "emp_pjt_flg", $fname);	// 委員会・WG
$pjtadm_usr		= lc_check_user_count($con, $basic_user, "emp_pjtadm_flg", $fname);	// 委員会・WG[管理者]
$newsuser_usr	= lc_check_user_count($con, $basic_user, "emp_newsuser_flg", $fname);	// お知らせ・回覧板
$news_usr		= lc_check_user_count($con, $basic_user, "emp_news_flg", $fname);	// お知らせ・回覧板[管理者]
$work_usr		= lc_check_user_count($con, $basic_user, "emp_work_flg", $fname);	// タスク
$phone_usr		= lc_check_user_count($con, $basic_user, "emp_phone_flg", $fname);	// 伝言メモ
$resv_usr		= lc_check_user_count($con, $basic_user, "emp_resv_flg", $fname);	// 設備予約
$fcl_usr		= lc_check_user_count($con, $basic_user, "emp_fcl_flg", $fname);	// 設備予約[管理者]
$attdcd_usr		= lc_check_user_count($con, $basic_user, "emp_attdcd_flg", $fname);	// 出勤表/勤務管理
$wkadm_usr		= lc_check_user_count($con, $basic_user, "emp_wkadm_flg", $fname);	// 出勤表/勤務管理[管理者]
$aprv_usr		= lc_check_user_count($con, $basic_user, "emp_aprv_flg", $fname);	// 決裁・申請/ワークフロー
$wkflw_usr		= lc_check_user_count($con, $basic_user, "emp_wkflw_flg", $fname);	// 決裁・申請/ワークフロー[管理者]
$conf_usr		= lc_check_user_count($con, $basic_user, "emp_conf_flg", $fname);	// ネットカンファレンス
$bbs_usr		= lc_check_user_count($con, $basic_user, "emp_bbs_flg", $fname);	// 掲示板・電子会議室
$bbsadm_usr		= lc_check_user_count($con, $basic_user, "emp_bbsadm_flg", $fname);	// 掲示板・電子会議室[管理者]
$qa_usr			= lc_check_user_count($con, $basic_user, "emp_qa_flg", $fname);		// Q&A
$qaadm_usr		= lc_check_user_count($con, $basic_user, "emp_qaadm_flg", $fname);	// Q&A[管理者]
$lib_usr		= lc_check_user_count($con, $basic_user, "emp_lib_flg", $fname);	// 文書管理
$libadm_usr		= lc_check_user_count($con, $basic_user, "emp_libadm_flg", $fname);	// 文書管理[管理者]
$adbk_usr		= lc_check_user_count($con, $basic_user, "emp_adbk_flg", $fname);	// アドレス帳
$adbkadm_usr	= lc_check_user_count($con, $basic_user, "emp_adbkadm_flg", $fname);	// アドレス帳[管理者]
$ext_usr		= lc_check_user_count($con, $basic_user, "emp_ext_flg", $fname);	// 内線電話帳
$extadm_usr		= lc_check_user_count($con, $basic_user, "emp_extadm_flg", $fname);	// 内線電話帳[管理者]
$link_usr		= lc_check_user_count($con, $basic_user, "emp_link_flg", $fname);	// リンクライブラリ
$linkadm_usr   	= lc_check_user_count($con, $basic_user, "emp_linkadm_flg", $fname);	// リンクライブラリ[管理者]
$pass_usr		= lc_check_user_count($con, "FULL", "emp_pass_flg", $fname);	// パスワード・本人情報
$cmt_usr		= lc_check_user_count($con, $basic_user, "emp_cmt_flg", $fname);	// コミュニティサイト
$cmtadm_usr		= lc_check_user_count($con, $basic_user, "emp_cmtadm_flg", $fname);	// コミュニティサイト[管理者]
$memo_usr		= lc_check_user_count($con, $basic_user, "emp_memo_flg", $fname);	// 備忘録
$cauth_usr		= lc_check_user_count($con, $basic_user, "emp_cauth_flg", $fname);	// 端末認証

// 管理機能
$tmgd_usr		= lc_check_user_count($con, "FULL", "emp_tmgd_flg", $fname);		// 院内行事登録
$reg_usr		= lc_check_user_count($con, "FULL", "emp_reg_flg", $fname);		// 職員登録（管理者機能）
$empif_usr		= lc_check_user_count($con, "FULL", "emp_empif_flg", $fname);		// 職員登録（職員参照）
$hspprf_usr		= lc_check_user_count($con, "FULL", "emp_hspprf_flg", $fname);		// マスターメンテナンス（組織プロフィール）
$dept_usr		= lc_check_user_count($con, "FULL", "emp_dept_flg", $fname);		// マスターメンテナンス（組織）
$job_usr		= lc_check_user_count($con, "FULL", "emp_job_flg", $fname);		// マスターメンテナンス（職種）
$status_usr		= lc_check_user_count($con, "FULL", "emp_status_flg", $fname);		// マスターメンテナンス（役職）
$attditem_usr	= lc_check_user_count($con, "FULL", "emp_attditem_flg", $fname);	// マスターメンテナンス（カレンダー）
$config_usr		= lc_check_user_count($con, "FULL", "emp_config_flg", $fname);		// 環境設定（管理者機能）
$lcs_usr		= lc_check_user_count($con, "FULL", "emp_lcs_flg", $fname);		// ライセンス管理（管理者機能）
$aclg			= lc_check_user_count($con, "FULL", "emp_accesslog_flg", $fname);		// アクセスログ

// 業務機能の現ライセンス使用数を確認
$ptreg_usr		= lc_check_user_count($con, "FULL", "emp_ptreg_flg", $fname);		// 患者管理（患者登録）
$ptif_usr		= lc_check_user_count($con, "FULL", "emp_ptif_flg", $fname);		// 患者管理（患者基本情報）
$outreg_usr		= lc_check_user_count($con, "FULL", "emp_outreg_flg", $fname);		// 患者管理（来院登録）
$inout_usr		= lc_check_user_count($con, "FULL", "emp_inout_flg", $fname);		// 患者管理（入院来院履歴）
$disreg_usr		= lc_check_user_count($con, "FULL", "emp_disreg_flg", $fname);		// 患者管理（プロブレムリスト）
$dishist_usr	= lc_check_user_count($con, "FULL", "emp_dishist_flg", $fname);		// 患者管理（プロブレム）
$ptadm_usr		= lc_check_user_count($con, "FULL", "emp_ptadm_flg", $fname);		// 患者管理（管理者権限）
$search_usr		= lc_check_user_count($con, "FULL", "emp_search_flg", $fname);		// 検索ちゃん
$amb_usr		= lc_check_user_count($con, "FULL", "emp_amb_flg", $fname);		// 看護支援
$ambadm_usr		= lc_check_user_count($con, "FULL", "emp_ambadm_flg", $fname);		// 看護支援x
$ward_usr		= lc_check_user_count($con, $bed_count, "emp_ward_flg", $fname);	// 病床管理
$ward_reg_usr	= lc_check_user_count($con, $bed_count, "emp_ward_reg_flg", $fname);	// 病床管理[管理者]
$entity_usr		= lc_check_user_count($con, "FULL", "emp_entity_flg", $fname);		// 診療科x
$nlcs_usr		= lc_check_user_count($con, "FULL", "emp_nlcs_flg", $fname);		// 看護観察記録
$nlcsadm_usr	= lc_check_user_count($con, "FULL", "emp_nlcsadm_flg", $fname);		// 看護観察記録[管理者]
$inci_usr		= lc_check_user_count($con, $fantol_user, "emp_inci_flg", $fname);	// ファントルくん
$rm_usr			= lc_check_user_count($con, $fantol_user, "emp_rm_flg", $fname);	// ファントルくん[管理者]
$fplus_usr		= lc_check_user_count($con, $fplus_user, "emp_fplus_flg", $fname);	// ファントルくん＋
$fplusadm_usr	= lc_check_user_count($con, $fplus_user, "emp_fplusadm_flg", $fname);	// ファントルくん＋[管理者]
$manabu_usr		= lc_check_user_count($con, "FULL", "emp_manabu_flg", $fname);		// まなぶくん
$manabuadm_usr	= lc_check_user_count($con, "FULL", "emp_manabuadm_flg", $fname);	// まなぶくん[管理者]
$med_usr		= lc_check_user_count($con, $report_user, "emp_med_flg", $fname);	// メドレポート
$medadm_usr		= lc_check_user_count($con, $report_user, "emp_medadm_flg", $fname);	// メドレポート[管理者]
$kview_usr		= lc_check_user_count($con, "FULL", "emp_kview_flg", $fname);		// カルテビューワー
$kviewadm_usr	= lc_check_user_count($con, "FULL", "emp_kviewadm_flg", $fname);	// カルテビューワー[管理者]
$biz_usr		= lc_check_user_count($con, "FULL", "emp_biz_flg", $fname);		// 経営支援
//$shift_usr		= lc_check_user_count($con, $shift_emps, "emp_shift_flg", $fname);	// 勤務シフト作成
$shiftadm_usr	= lc_check_user_count($con, $shift_emps, "emp_shiftadm_flg", $fname);   // 勤務シフト作成[管理者]
$cas_usr		= lc_check_user_count($con, "FULL", "emp_cas_flg", $fname);		// CAS
$casadm_usr		= lc_check_user_count($con, "FULL", "emp_casadm_flg", $fname);		// CAS[管理者]
$ccusr1_usr		= lc_check_user_count($con, "FULL", "emp_ccusr1_flg", $fname);		// スタッフポートフォリオ
$ccadm1_usr		= lc_check_user_count($con, "FULL", "emp_ccadm1_flg", $fname);		// スタッフポートフォリオ[管理者]

$shift2_usr		= lc_check_user_count($con, "FULL", "emp_shift2_flg", $fname);		// 勤務表
$shift2adm_usr	= lc_check_user_count($con, "FULL", "emp_shift2adm_flg", $fname);	// 勤務表[管理者]
$ladder_usr		= lc_check_user_count($con, "FULL", "emp_ladder_flg", $fname);		// クリニカルラダー
$ladderadm_usr	= lc_check_user_count($con, "FULL", "emp_ladderadm_flg", $fname);	// クリニカルラダー[管理者]
$career_usr		= lc_check_user_count($con, "FULL", "emp_career_flg", $fname);		// キャリア開発ラダー
$careeradm_usr	= lc_check_user_count($con, "FULL", "emp_careeradm_flg", $fname);	// キャリア開発ラダー[管理者]

$jinji_usr		= lc_check_user_count($con, $jinji_emps, "emp_jinji_flg", $fname);	// 人事管理
$jinjiadm_usr	= lc_check_user_count($con, $jinji_emps, "emp_jinjiadm_flg", $fname);	// 人事管理[管理者]
$jnl_usr		= lc_check_user_count($con, $jnl_user, "emp_jnl_flg", $fname);		// 日報・月報
$jnladm_usr		= lc_check_user_count($con, $jnl_user, "emp_jnladm_flg", $fname);	// 日報・月報[管理者]

// イントラネット
$intra_usr		= lc_check_user_count($con, $intra_user, "emp_intra_flg", $fname);	// イントラネット（ユーザ機能）
$ymstat_usr		= lc_check_user_count($con, $intra_user, "emp_ymstat_flg", $fname);	// イントラネット（月間・年間稼動状況統計参照）
$intram_usr		= lc_check_user_count($con, $intra_user, "emp_intram_flg", $fname);	// イントラネット（イントラメニュー）
$stat_usr		= lc_check_user_count($con, $intra_user, "emp_stat_flg", $fname);	// イントラネット（稼働状況統計）
$allot_usr		= lc_check_user_count($con, $intra_user, "emp_allot_flg", $fname);	// イントラネット（当直・外来分担表）
$life_usr		= lc_check_user_count($con, $intra_user, "emp_life_flg", $fname);	// イントラネット（私たちの生活サポート）
function lc_check_user_count($con, $user_count_code, $columns, $fname) {
	// countだと処理が遅い
	$sql = "select authmst.emp_id from authmst";
	$sql .= " left join empmst on empmst.emp_id = authmst.emp_id";
	$sql .= " left join authgroup on authgroup.group_id=authmst.group_id";
	$cond = "where authmst.emp_del_flg='f'";
	if(is_array($columns)) {
		$cond .= " and (" . implode("='t' or ", $columns) . "='t')";
	}
	else {
		$cond .= " and ($columns = 't')";
	}

	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$user_count = pg_num_rows($sel);
	if(($user_count_code == "FULL") || ($user_count <= intval($user_count_code))){
		return "$user_count";
	}
	else if($user_count > intval($user_count_code)) {
		return "<font color=\"red\">{$user_count}</font>";
	}
}

require_once("get_menu_label.ini");
$report_menu_label = get_report_menu_label($con, $fname);
$shift_menu_label = get_shift_menu_label($con, $fname);

// 勤務シフト
$sql = "select * from duty_shift_staff";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$shift_staff_usr = pg_num_rows($sel);

?>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-style:none;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="employee_info_menu.php?session=<?php echo ($session); ?>"><img src="img/icon/b26.gif" width="32" height="32" border="0" alt="職員登録"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="employee_info_menu.php?session=<?php echo($session); ?>"><b>職員登録</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_info_menu.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">検索</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_list.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一覧</font></a></td>
<? if ($reg == 1) { ?>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_register.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">新規登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_bulk_register.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="140" align="center" bgcolor="#bdd1e7"><a href="employee_db_list.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員登録用DB操作</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_auth_group.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限グループ</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_display_group.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示グループ</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_menu_group.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メニューグループ</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_auth_bulk_setting.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括設定</font></a></td>
<? } ?>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#5279a5"><a href="employee_auth_list.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>権限管理</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_field_option.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">項目管理</font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr valign="top">
<td width="15%">
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
権限グループ名<br>
</font></td>
</tr>
<tr height="100" valign="top">
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<?
foreach ($groups as $tmp_group_id => $tmp_group_nm) {
	echo("<tr height=\"22\" valign=\"middle\">\n");
	echo("<td style=\"padding-left:3px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_group_nm <a href=\"javascript:void(0);\" onclick=\"window.open('employee_auth_list_detail.php?session=$session&group_id=$tmp_group_id&name=$tmp_group_nm','WinObj','width=800,height=600,scrollbars=yes');\">({$tmp_group["$tmp_group_id"]})</a></font></td>\n");
	echo("</tr>\n");
}
?>
</table>
</td>
</tr>
</table>
</td>
<td><img src="img/spacer.gif" alt="" width="5" height="1"></td>
<td>
<img src="img/spacer.gif" alt="" width="1" height="10"><br>
<table border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>基本機能</b>(利用ユーザ数：<?php echo($basic_usr); ?>人)</font></td>
</tr>
</table>
<table width="50%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff">
<td></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ユーザ機能</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">管理者機能</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ウェブメール</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($basic_user != '0' || $basic_user == 'FULL'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=webml','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($basic_user == 'FULL') ? ("$webml_usr / 無制限") : ("$webml_usr / $basic_user"); ?>)</a><? } ?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($basic_user != '0' || $basic_user == 'FULL'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=webmladm','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($basic_user == 'FULL') ? ("$webmladm_usr / 無制限") : ("$webmladm_usr / $basic_user"); ?>)<? } ?></a></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">スケジュール</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($basic_user != '0' || $basic_user == 'FULL'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=schd','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($basic_user == 'FULL') ? ("$schd_usr / 無制限") : ("$schd_usr / $basic_user"); ?>)<? } ?></a></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($basic_user != '0' || $basic_user == 'FULL'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=schdplc','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($basic_user == 'FULL') ? ("$schdplc_usr / 無制限") : ("$schdplc_usr / $basic_user"); ?>)<? } ?></a></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">委員会・WG</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($basic_user != '0' || $basic_user == 'FULL'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=pjt','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($basic_user == 'FULL') ? ("$pjt_usr / 無制限") : ("$pjt_usr / $basic_user"); ?>)<? } ?></a></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($basic_user != '0' || $basic_user == 'FULL'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=pjtadm','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($basic_user == 'FULL') ? ("$pjtadm_usr / 無制限") : ("$pjtadm_usr / $basic_user"); ?>)<? } ?></a></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ・回覧板</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($basic_user != '0' || $basic_user == 'FULL'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=newsuser','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($basic_user == 'FULL') ? ("$newsuser_usr / 無制限") : ("$newsuser_usr / $basic_user"); ?>)<? } ?></a></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($basic_user != '0' || $basic_user == 'FULL'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=news','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($basic_user == 'FULL') ? ("$news_usr / 無制限") : ("$news_usr / $basic_user"); ?>)<? } ?></a></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タスク</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($basic_user != '0' || $basic_user == 'FULL'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=work','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($basic_user == 'FULL') ? ("$work_usr / 無制限") : ("$work_usr / $basic_user"); ?>)<? } ?></a></font></td>
<td></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">伝言メモ</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($basic_user != '0' || $basic_user == 'FULL'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=phone','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($basic_user == 'FULL') ? ("$phone_usr / 無制限") : ("$phone_usr / $basic_user"); ?>)<? } ?></a></font></td>
<td></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">設備予約</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($basic_user != '0' || $basic_user == 'FULL'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=resv','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($basic_user == 'FULL') ? ("$resv_usr / 無制限") : ("$resv_usr / $basic_user"); ?>)<? } ?></a></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($basic_user != '0' || $basic_user == 'FULL'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=fcl','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($basic_user == 'FULL') ? ("$fcl_usr / 無制限") : ("$fcl_usr / $basic_user"); ?>)<? } ?></a></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤表／勤務管理</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($basic_user != '0' || $basic_user == 'FULL'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=attdcd','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($basic_user == 'FULL') ? ("$attdcd_usr / 無制限") : ("$attdcd_usr / $basic_user"); ?>)<? } ?></a></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($basic_user != '0' || $basic_user == 'FULL'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=wkadm','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($basic_user == 'FULL') ? ("$wkadm_usr / 無制限") : ("$wkadm_usr / $basic_user"); ?>)<? } ?></a></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">決裁・申請／ワークフロー</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($basic_user != '0' || $basic_user == 'FULL'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=aprv','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($basic_user == 'FULL') ? ("$aprv_usr / 無制限") : ("$aprv_usr / $basic_user"); ?>)<? } ?></a></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($basic_user != '0' || $basic_user == 'FULL'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=wkflw','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($basic_user == 'FULL') ? ("$wkflw_usr / 無制限") : ("$wkflw_usr / $basic_user"); ?>)<? } ?></a></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ネットカンファレンス</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($basic_user != '0' || $basic_user == 'FULL'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=conf','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($basic_user == 'FULL') ? ("$conf_usr / 無制限") : ("$conf_usr / $basic_user"); ?>)<? } ?></a></font></td>
<td></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">掲示板・電子会議室</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($basic_user != '0' || $basic_user == 'FULL'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=bbs','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($basic_user == 'FULL') ? ("$bbs_usr / 無制限") : ("$bbs_usr / $basic_user"); ?>)<? } ?></a></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($basic_user != '0' || $basic_user == 'FULL'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=bbsadm','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($basic_user == 'FULL') ? ("$bbsadm_usr / 無制限") : ("$bbsadm_usr / $basic_user"); ?>)<? } ?></a></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">Q&amp;A</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($basic_user != '0' || $basic_user == 'FULL'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=qa','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($basic_user == 'FULL') ? ("$qa_usr / 無制限") : ("$qa_usr / $basic_user"); ?>)<? } ?></a></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($basic_user != '0' || $basic_user == 'FULL'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=qaadm','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($basic_user == 'FULL') ? ("$qaadm_usr / 無制限") : ("$qaadm_usr / $basic_user"); ?>)<? } ?></a></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文書管理</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($basic_user != '0' || $basic_user == 'FULL'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=lib','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($basic_user == 'FULL') ? ("$lib_usr / 無制限") : ("$lib_usr / $basic_user"); ?>)<? } ?></a></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($basic_user != '0' || $basic_user == 'FULL'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=libadm','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($basic_user == 'FULL') ? ("$libadm_usr / 無制限") : ("$libadm_usr / $basic_user"); ?>)<? } ?></a></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">アドレス帳</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($basic_user != '0' || $basic_user == 'FULL'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=adbk','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($basic_user == 'FULL') ? ("$adbk_usr / 無制限") : ("$adbk_usr / $basic_user"); ?>)<? } ?></a></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($basic_user != '0' || $basic_user == 'FULL'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=adbkadm','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($basic_user == 'FULL') ? ("$adbkadm_usr / 無制限") : ("$adbkadm_usr / $basic_user"); ?>)<? } ?></a></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">内線電話帳</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($basic_user != '0' || $basic_user == 'FULL'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=ext','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($basic_user == 'FULL') ? ("$ext_usr / 無制限") : ("$ext_usr / $basic_user"); ?>)<? } ?></a></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($basic_user != '0' || $basic_user == 'FULL'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=extadm','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($basic_user == 'FULL') ? ("$extadm_usr / 無制限") : ("$extadm_usr / $basic_user"); ?>)<? } ?></a></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">リンクライブラリ</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($basic_user != '0' || $basic_user == 'FULL'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=link','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($basic_user == 'FULL') ? ("$link_usr / 無制限") : ("$link_usr / $basic_user"); ?>)<? } ?></a></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($basic_user != '0' || $basic_user == 'FULL'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=linkadm','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($basic_user == 'FULL') ? ("$linkadm_usr / 無制限") : ("$linkadm_usr / $basic_user"); ?>)<? } ?></a></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">パスワード・本人情報</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=pass','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ("$pass_usr / 無制限"); ?>)</a></font></td>
<td></td>
</tr>
<!-- tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">コミュニティサイト</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($basic_user != '0' || $basic_user == 'FULL'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=cmt','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($basic_user == 'FULL') ? ("$cmt_usr / 無制限") : ("$cmt_usr / $basic_user"); ?>)<? } ?></a></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($basic_user != '0' || $basic_user == 'FULL'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=cmtadm','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($basic_user == 'FULL') ? ("$cmtadm_usr / 無制限") : ("$cmtadm_usr / $basic_user"); ?>)<? } ?></a></font></td>
</tr -->
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">備忘録</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($basic_user != '0' || $basic_user == 'FULL'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=memo','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($basic_user == 'FULL') ? ("$memo_usr / 無制限") : ("$memo_usr / $basic_user"); ?>)<? } ?></a></font></td>
<td></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">端末認証</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=cauth','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ("$cauth_usr / 無制限"); ?>)</a></font></td>
<td></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="10"><br>
<table border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>管理機能</b></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff">
<td></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ユーザ機能</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">管理者機能</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員参照</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">組織プロフィール</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">組織</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職種</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">役職</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カレンダー</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 院内行事登録／法人内行事登録
echo($_label_by_profile["EVENT_REG"][$profile_type]);
?></font></td>
<td></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=tmgd','WinObj','width=800,height=600,scrollbars=yes');">( <?php echo $tmgd_usr; ?> )</a></font></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員登録</font></td>
<td></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=reg','WinObj','width=800,height=600,scrollbars=yes');">( <?php echo $reg_usr; ?> )</a></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=empif','WinObj','width=800,height=600,scrollbars=yes');">( <?php echo $empif_usr; ?> )</a></font></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">マスターメンテナンス</font></td>
<td></td>
<td></td>
<td></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=hspprf','WinObj','width=800,height=600,scrollbars=yes');">( <?php echo $hspprf_usr; ?> )</a></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=dept','WinObj','width=800,height=600,scrollbars=yes');">( <?php echo $dept_usr; ?> )</a></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=job','WinObj','width=800,height=600,scrollbars=yes');">( <?php echo $job_usr; ?> )</a></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=status','WinObj','width=800,height=600,scrollbars=yes');">( <?php echo $status_usr; ?> )</a></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=attditem','WinObj','width=800,height=600,scrollbars=yes');">( <?php echo $attditem_usr; ?> )</a></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">環境設定</font></td>
<td></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=config','WinObj','width=800,height=600,scrollbars=yes');">( <?php echo $config_usr; ?> )</a></font></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ライセンス管理</font></td>
<td></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=lcs','WinObj','width=800,height=600,scrollbars=yes');">( <?php echo $lcs_usr; ?> )</a></font></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">アクセスログ</font></td>
<td></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=accesslog','WinObj','width=800,height=600,scrollbars=yes');">( <?php echo $aclg; ?> )</a></font></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="10"><br>
<table border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>業務機能</b></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff">
<td></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 患者登録／利用者登録
echo($_label_by_profile["PATIENT_REG"][$profile_type]);
?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 患者基本情報／利用者基本情報
echo($_label_by_profile["PATIENT_INFO"][$profile_type]);
?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 来院登録／来所登録
echo($_label_by_profile["OUT_REG"][$profile_type]);
?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 入院来院履歴／入所来所履歴
echo($_label_by_profile["IN_OUT"][$profile_type]);
?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">プロブレムリスト</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">プロブレム</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">管理者権限</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 患者管理／利用者管理
echo($_label_by_profile["PATIENT_MANAGE"][$profile_type]);
?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[1] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=ptreg','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ("$ptreg_usr / 無制限"); ?>)</a><? } ?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[1] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=ptif','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ("$ptif_usr / 無制限"); ?>)</a><? } ?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[1] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=outreg','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ("$outreg_usr / 無制限"); ?>)</a><? } ?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[1] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=inout','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ("$inout_usr / 無制限"); ?>)</a><? } ?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[1] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=disreg','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ("$disreg_usr / 無制限"); ?>)</a><? } ?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[1] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=dishist','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ("$dishist_usr / 無制限"); ?>)</a><? } ?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[1] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=ptadm','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ("$ptadm_usr / 無制限"); ?>)</a><? } ?></font></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="8"><br>
<table width="50%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff">
<td></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ユーザ機能</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">管理者機能</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">検索ちゃん</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[7] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=search','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ("$search_usr / 無制限"); ?>)</a><? } ?></font></td>
<td></td>
</tr>


<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">スタッフポートフォリオ</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[31] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=ccusr1','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($ccusr1_user == 'FULL') ? ("$ccusr1_usr / 無制限") : ("$ccusr1_usr / $ccusr1_user"); ?>)</a><? } ?></font></td>

<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[31] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=ccadm1','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($ccusr1_user == 'FULL') ? ("$ccadm1_usr / 無制限") : ("$ccadm1_usr / $ccusr1_user"); ?>)</a><? } ?></font></td>
</tr>


<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">看護支援</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[15] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=amb','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($amb_user == 'FULL') ? ("$amb_usr / 無制限") : ("$amb_usr / $nursing_user"); ?>)</a><? } ?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[15] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=ambadm','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($ambadm_user == 'FULL') ? ("$ambadm_usr / 無制限") : ("$ambadm_usr / $nursing_user"); ?>)</a><? } ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 病床管理
echo($_label_by_profile["BED_MANAGE"][$profile_type]);
?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[2] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=ward','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($bed_count == 'FULL') ? ("$ward_usr / 無制限") : ("$ward_usr / $bed_count"); ?>)</a><? } ?></font></td>
<td align="center">
<? if ($profile_type != "2") { ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[2] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=ward_reg','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($bed_count == 'FULL') ? ("$ward_reg_usr / 無制限") : ("$ward_reg_usr / $bed_count"); ?>)</a><? } ?></font>
<? } ?>
</td>
</tr>
<? if ($profile_type != "2") { ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 診療科／担当部門
echo($_label_by_profile["SECTION"][$profile_type]);
?></font></td>
<td></td>
<td align="center">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=entity','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ("$entity_usr / 無制限"); ?>)</a></font>
</td>
</tr>
<? } ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">看護観察記録</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[10] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=nlcs','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ("$nlcs_usr / 無制限"); ?>)</a><? } ?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[10] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=nlcsadm','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ("$nlcsadm_usr / 無制限"); ?>)</a><? } ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ファントルくん</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[5] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=inci','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($fantol_user == 'FULL') ? ("$inci_usr / 無制限") : ("$inci_usr / $fantol_user"); ?>)</a><? } ?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[5] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=rm','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($fantol_user == 'FULL') ? ("$rm_usr / 無制限") : ("$rm_usr / $fantol_user"); ?>)</a><? } ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ファントルくん＋</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[13] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=fplus','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($fplus_user == 'FULL') ? ("$fplus_usr / 無制限") : ("$fplus_usr / $fplus_user"); ?>)</a><? } ?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[13] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=fplusadm','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($fplus_user == 'FULL') ? ("$fplusadm_usr / 無制限") : ("$fplusadm_usr / $fplus_user"); ?>)</a><? } ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">バリテス</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[12] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=manabu','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($baritess_user == 'FULL') ? ("$manabu_usr / 無制限") : ("$manabu_usr / $baritess_user"); ?>)</a><? } ?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[12] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=manabuadm','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($baritess_user == 'FULL') ? ("$manabuadm_usr / 無制限") : ("$manabuadm_usr / $baritess_user"); ?>)</a><? } ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// メドレポート／POレポート
echo($report_menu_label);
?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[6] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=med','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($report_user == 'FULL') ? ("$med_usr / 無制限") : ("$med_usr / $report_user"); ?>)</a><? } ?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[6] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=medadm','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($report_user == 'FULL') ? ("$medadm_usr / 無制限") : ("$medadm_usr / $report_user"); ?>)</a><? } ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カルテビューワー</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[11] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=kview','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ("$kview_usr / 無制限"); ?>)</a><? } ?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[11] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=kviewadm','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ("$kviewadm_usr / 無制限"); ?>)</a><? } ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">経営支援</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[3] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=biz','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ("$biz_usr / 無制限"); ?>)</a><? } ?></font></td>
<td></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 勤務シフト作成
echo($shift_menu_label);
?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[9] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=shift_staff','WinObj','width=800,height=600,scrollbars=yes');">シフト登録者：(<?php echo ($shift_emps == 'FULL') ? ("$shift_staff_usr / 無制限") : ("$shift_staff_usr / $shift_emps"); ?>)</a><? } ?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[9] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=shiftadm','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ("$shiftadm_usr / 無制限"); ?>)</a><? } ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務表</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[18] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=shift2_staff','WinObj','width=800,height=600,scrollbars=yes');">勤務表登録者：(<?php echo ($shift_emps == 'FULL') ? ("$shift_staff_usr / 無制限") : ("$shift_staff_usr / $shift_emps"); ?>)</a><? } ?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[18] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=shift2adm','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ("$shift2adm_usr / 無制限"); ?>)</a><? } ?></font></td>
</tr>

<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// CAS
echo(get_cas_title_name());
?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[8] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=cas','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ("$cas_usr / 無制限"); ?>)</a><? } ?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[8] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=casadm','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ("$casadm_usr / 無制限"); ?>)</a><? } ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">人事管理</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[16] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=jinji','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($jinji_emps == 'FULL') ? ("$jinji_usr / 無制限") : ("$jinji_usr / $jinji_emps"); ?>)</a><? } ?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[16] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=jinjiadm','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($jinji_emps == 'FULL') ? ("$jinjiadm_usr / 無制限") : ("$jinjiadm_usr / $jinji_emps"); ?>)</a><? } ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">クリニカルラダー</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[17] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=ladder','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($ladder_user == 'FULL') ? ("$ladder_usr / 無制限") : ("$ladder_usr / $ladder_user"); ?>)</a><? } ?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[17] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=ladderadm','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($ladder_user == 'FULL') ? ("$ladderadm_usr / 無制限") : ("$ladderadm_usr / $ladder_user"); ?>)</a><? } ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">キャリア開発ラダー</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[19] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=career','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($career_user == 'FULL') ? ("$career_usr / 無制限") : ("$career_usr / $career_user"); ?>)</a><? } ?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[19] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=careeradm','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($career_user == 'FULL') ? ("$careeradm_usr / 無制限") : ("$careeradm_usr / $career_user"); ?>)</a><? } ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日報・月報</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[14] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=jnl','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($jnl_user == 'FULL') ? ("$jnl_usr / 無制限") : ("$jnl_usr / $jnl_user"); ?>)</a><? } ?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[14] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=jnladm','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo ($jnl_user == 'FULL') ? ("$jnladm_usr / 無制限") : ("$jnladm_usr / $jnl_user"); ?>)</a><? } ?></font></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="8"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff">
<td></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ユーザ機能</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">月間・年間<?php echo($intra_menu1); ?>参照</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">イントラメニュー</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 稼働状況統計
echo($intra_menu1);
?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 当直・外来分担表
echo($intra_menu2_4);
?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 私たちの生活サポート
echo($intra_menu3);
?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">イントラネット</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[4] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=intra','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo("$intra_usr / 無制限"); ?>)</a><? } ?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[4] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=ymstat','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo("$ymstat_usr / 無制限"); ?>)</a><? } ?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[4] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=intram','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo("$intram_usr / 無制限"); ?>)</a><? } ?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[4] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=stat','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo("$stat_usr / 無制限") ?>)</a><? } ?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[4] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=allot','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo("$allot_usr / 無制限"); ?>)</a><? } ?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if($arr_func[4] == 't'){ ?><a href="javascript:void(0);" onclick="window.open('employee_auth_list_detail.php?session=<?php echo($session); ?>&tool_nm=life','WinObj','width=800,height=600,scrollbars=yes');">(<?php echo("$life_usr / 無制限"); ?>)</a><? } ?></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
