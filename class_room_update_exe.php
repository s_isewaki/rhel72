<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_class_name.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 23, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);
pg_query($con, "begin");

$arr_class_name = get_class_name_array($con, $fname);

// 入力チェック
if ($room_name == "") {
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('{$arr_class_name[3]}名を入力してください');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($room_name) > 60) {
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('{$arr_class_name[3]}名が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($link_key) > 12) {
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('外部連携キーが長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}

// 室名を更新
$sql = "update classroom set";
$set = array("room_nm", "link_key");
$setvalue = array($room_name, $link_key);
$cond = "where room_id = '$room_id'";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// データベース接続を閉じる
pg_query($con, "commit");
pg_close($con);

// 組織一覧画面に遷移
echo("<script type=\"text/javascript\">location.href = 'class_menu.php?session=$session&class_id=$class_id&atrb_id=$atrb_id&dept_id=$dept_id';</script>");
?>
