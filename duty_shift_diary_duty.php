<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務シフト作成｜勤務日誌</title>

<?
require_once("about_authority.php");
require_once("about_session.php");
require_once("about_postgres.php");

require_once("duty_shift_common.ini");
require_once("duty_shift_user_tab_common.ini");
require_once("duty_shift_common_class.php");
require_once("duty_shift_diary_common.ini");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
//セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
///-----------------------------------------------------------------------------
// 権限のチェック
///-----------------------------------------------------------------------------
//ユーザ画面用
$chk_flg = $obj->check_authority_user($session, $fname);
if ($chk_flg == "") {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
//管理画面用
$section_admin_auth = $obj->check_authority_Management($session, $fname);

///-----------------------------------------------------------------------------
//初期処理
///-----------------------------------------------------------------------------
	///-----------------------------------------------------------------------------
	//初期値設定
	///-----------------------------------------------------------------------------
	$err_flg_1 = "";
	$person_charge_id = "";		//シフト管理者ID（責任者）
	$caretaker_id = "";			//シフト管理者ID（代行者）
	$caretaker2_id = "";		//シフト管理者ID（代行者）
	$caretaker3_id = "";		//シフト管理者ID（代行者）
	$caretaker4_id = "";		//シフト管理者ID（代行者）
	$caretaker5_id = "";
	$caretaker6_id = "";
	$caretaker7_id = "";
	$caretaker8_id = "";
	$caretaker9_id = "";
	$caretaker10_id = "";

	// 期間
	if ($start_yr == "") {
		$start_yr = date('Y');
	}
	if ($start_mon == "") {
		$start_mon = date('m');
	}
	if ($start_day == "") {
		$start_day = date('d');
	}
	if ($end_yr == "") {
		$end_yr = date('Y');
	}
	if ($end_mon == "") {
		$end_mon = date('m');
	}
	if ($end_day == "") {
		$end_day = date('d');
	}

if ($kind_flag[0] == "1"){
	$arr_title = array(
		"シフト<br>グループ",	//0
		"年月日",
		"職員ID",
		"職種",
		"役職",
		"氏名",					//5
		"勤務シフト<br>予定",
		"出勤予定",
		"事由予定",
		"出勤<br>時刻",
		"退勤<br>時刻",
		"部署"					//15
	);
}else{
	$arr_title = array(
		"シフト<br>グループ",	//0
		"年月日",
		"職員ID",
		"職種",
		"役職",
		"氏名",					//5
		"勤務シフト<br>実績",
		"出勤実績",				//10
		"事由実績",
		"出勤<br>時刻",
		"退勤<br>時刻",
		"部署"					//15
	);
}
	///-----------------------------------------------------------------------------
	// ログインユーザの職員ID・氏名を取得
	///-----------------------------------------------------------------------------
	$sql = "select emp_id, emp_lt_nm, emp_ft_nm from empmst";
	$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$emp_id = pg_fetch_result($sel, 0, "emp_id");
	$emp_name = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");
	///-----------------------------------------------------------------------------
	//ＤＢ(empmst)より職員情報を取得
	//ＤＢ(wktmgrp)より勤務パターン情報を取得
	///-----------------------------------------------------------------------------
//	$data_emp = $obj->get_empmst_array("");
	$data_emp = array();	//以下の処理で未使用のため
	$data_wktmgrp = $obj->get_wktmgrp_array();
	
	///-----------------------------------------------------------------------------
	// 勤務パターン、休暇事由を登録する
	///-----------------------------------------------------------------------------
	if ($action == "表示") {
		foreach ($pattern_id as $key=> $item){
			if (empty($pattern_id[$key])) {
				unset($pattern_id[$key]);
			}
		}
		
		foreach ($reason_id as $key1=> $item){
			if (empty($reason_id[$key1])) {
				unset($reason_id[$key1]);
			}
		}
				
			
		$tmp_pattern_id = (empty($pattern_id)) ? "" : implode("|", $pattern_id);
		$tmp_reason_id = (empty($reason_id)) ? "" : implode("|", $reason_id); 
		update_shift_diary_config($con, $fname, $emp_id, $tmp_pattern_id, $tmp_reason_id, $group_id, $kind_flag[0]);
	}
	
	// 更新権限のあるシフトグループ数
	$create_flg_cnt = 0;
	///-----------------------------------------------------------------------------
	// 勤務シフトグループ情報を取得
	///-----------------------------------------------------------------------------
	$group_array = $obj->get_duty_shift_group_array("", $data_emp, $data_wktmgrp);
	// 更新権限あり、職員設定されているグループ取得
	$group_array = $obj->get_valid_group_array($group_array, $emp_id, "", "");
	if (count($group_array) <= 0) {
		$err_flg_1 = "1";
	} else {
		//更新権限があるグループを設定
		for ($i=0; $i<count($group_array); $i++) {
			$person_charge_id = $group_array[$i]["person_charge_id"];			//シフト管理者ID（責任者）
			$caretaker_id = $group_array[$i]["caretaker_id"];					//シフト管理者ID（代行者）
			$caretaker2_id = $group_array[$i]["caretaker2_id"];
			$caretaker3_id = $group_array[$i]["caretaker3_id"];
			$caretaker4_id = $group_array[$i]["caretaker4_id"];
			$caretaker5_id = $group_array[$i]["caretaker5_id"];
			$caretaker6_id = $group_array[$i]["caretaker6_id"];
			$caretaker7_id = $group_array[$i]["caretaker7_id"];	
			$caretaker8_id = $group_array[$i]["caretaker8_id"];
			$caretaker9_id = $group_array[$i]["caretaker9_id"];
			$caretaker10_id = $group_array[$i]["caretaker10_id"];	
		
			if (($emp_id == $person_charge_id) || ($emp_id == $caretaker_id) || ($emp_id == $caretaker2_id) || ($emp_id == $caretaker3_id) || ($emp_id == $caretaker4_id) || ($emp_id == $caretaker5_id) || ($emp_id == $caretaker6_id) || ($emp_id == $caretaker7_id) || ($emp_id == $caretaker8_id) || ($emp_id == $caretaker9_id) || ($emp_id == $caretaker10_id)) {
				$group_array[$i]["create_flg"] = "1";
				$create_flg_cnt++;
			}
			
			// パターンＩＤ取得 20140731
			$array_pattern_id[] = $group_array[$i]["pattern_id"];
		}

		///-----------------------------------------------------------------------------
		//勤務シフトグループ情報ＤＢより情報取得
		///-----------------------------------------------------------------------------
		if ($group_id == ""){
			$group_id = $group_array[0]["group_id"];
		}
	}
	
	///-----------------------------------------------------------------------------
	// 設定した勤務パターン、休暇事由を取得 20140730
	///-----------------------------------------------------------------------------
	$arr_info = get_shift_diary_info($con, $fname, $emp_id);
	$list_pattern = (empty($arr_info["pattern"]))? null : explode("|", $arr_info["pattern"]);
	$list_pattern_id = array();
	$list_group_id = array();
	foreach($list_pattern as $item){
		$tmp_item = explode("_", $item);
		$list_group_id[$tmp_item[0]] = $tmp_item[0];
		$list_pattern_id[$tmp_item[1]] = $tmp_item[1];
	}
	$list_reason = (empty($arr_info["reason"]))? null : explode("|", $arr_info["reason"]);
	$list_shift = (empty($arr_info["shift_group_id"]))? $group_id : $arr_info["shift_group_id"];
	$list_kind = $arr_info["kind_flag"];
	
	///-----------------------------------------------------------------------------
	// 勤務パターンを取得 20140730
	///-----------------------------------------------------------------------------
	$str_pattern = implode("','", $array_pattern_id);
	
	$sql_ptn = "select a.group_id, a.atdptn_id, a.atdptn_nm, b.group_name, a.allowance from atdptn a ";
	$sql_ptn .= "left join wktmgrp b on b.group_id = a.group_id ";
	if (!empty($str_pattern)){
		$sql_ptn .= "where a.group_id in ('$str_pattern') ";
	}
	$sql_ptn .= "order by b.group_id, a.atdptn_id ";
	$sel_ptn = select_from_table($con, $sql_ptn, '', $fname);
	if ($sel_ptn == 0) {
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showLoginPage(window);</script>");
		exit;
	}
	
	$pattern_list = array();
	while ($row = pg_fetch_assoc($sel_ptn)) {
		$pattern_list[] = array(
				"id"   => $row["group_id"] . "_" . $row["atdptn_id"],
				"name" => $row["group_name"] . "＞" . $row["atdptn_nm"]
		);
	}
	
	///-----------------------------------------------------------------------------
	// 休暇事由を取得 20140730
	///-----------------------------------------------------------------------------
	$sql_rsn = "select reason_id, default_name, display_name from atdbk_reason_mst ";
	$sql_rsn .= "where display_flag = 't' order by sequence";
	$sel_rsn = select_from_table($con, $sql_rsn, '', $fname);
	if ($sel_rsn == 0) {
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showLoginPage(window);</script>");
		exit;
	}
	
	$reason_list = array();
	while ($row = pg_fetch_assoc($sel_rsn)){
		$tmp_name = (empty($row["display_name"])) ? $row["default_name"] : $row["display_name"];
		$reason_list[] = array(
				"id" => $row["reason_id"],
				"name" => $tmp_name
		);
	}
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	if ($action == "表示") {
		$start_date = $start_yr.$start_mon.$start_day;
		$end_date = $end_yr.$end_mon.$end_day;
		$data_atdptn_all = $obj->get_atdptn_array("");
		$data_pattern_all = $obj->get_duty_shift_pattern_array("", $data_atdptn_all);
		$reason_2_array = $obj->get_reason_2("1");
		
		$str_reason = implode("','", $list_reason);
		
		$data = get_diary_duty($con, $fname, $group_id, $group_array, $start_date, $end_date, $data_pattern_all, $reason_2_array, $data_atdptn_all, $list_pattern, $str_reason, $kind_flag[0]);
	}
	
// オプション作成 20140730
function create_options($info, $id=null) {
	foreach($info as $item) {
		$selected = "";
		if(!empty($id) and $item["id"] === $id) {
			$selected = 'selected';
		}
		
		$arr_item = explode("_" , $item["id"]);
		
		printf('<option %s value="%s">%s</option>', $selected, $item["id"], $item["name"]);
	}
}

// 勤務パターン、休暇事由を登録
function update_shift_diary_config($con, $fname, $id, $pattern_list, $reason_list, $group_id, $kind_flag){
	
	$sql = "delete from duty_shift_diary_config ";
	$cond = "where emp_id = '$id'";
	$del = delete_from_table($con, $sql, "", $fname);
	if ($del == 0){
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	
	$tmp_pattern = (empty($pattern_list)) ? "" : $pattern_list; 
	$tmp_reason = (empty($reason_list)) ? "" : $reason_list; 
	
	$sql = "insert into duty_shift_diary_config(emp_id, pattern, reason, shift_group_id, kind_flag";
	$sql .= ") values (";
	$content = array($id, $tmp_pattern, $tmp_reason, $group_id, intval($kind_flag));
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// 勤務パターン、休暇事由を取得
function get_shift_diary_info($con, $fname, $id){
	
	$sql = "select pattern, reason, shift_group_id, kind_flag from duty_shift_diary_config";
	$cond = "where emp_id = '$id'";
	
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	
	$info = array();
	if (pg_num_rows($sel) > 0){
		$info = pg_fetch_all($sel);
	}	
	return $info[0];
}
?>

<!-- ************************************************************************ -->
<!-- JavaScript -->
<!-- ************************************************************************ -->
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
<script type="text/javascript">
function downloadCSV() {
	if (check_date() == false){
		return false;
	}
	
	document.csv.action = 'duty_shift_diary_duty_csv.php';
	document.csv.start_date.value = "" + document.mainform.start_yr.value + document.mainform.start_mon.value +document.mainform.start_day.value;
	document.csv.end_date.value = "" + document.mainform.end_yr.value + document.mainform.end_mon.value + document.mainform.end_day.value;
	document.csv.submit();
}

$(document).ready(function() {
	// 行を追加する
	$(document).on("click", ".addPattern", function() {
	    $(".diary-pattern > tr").eq(0).clone(true).insertAfter(
	      $(this).parent().parent()
	    );
  	});
	$(document).on("click", ".addReason", function() {
	    $(".diary-reason > tr").eq(0).clone(true).insertAfter(
	      $(this).parent().parent()
	    );
  	});

	// 行を削除する
	$(document).on("click", ".removePattern", function() {
		$(this).parent().parent().empty();
	});
	$(document).on("click", ".removeReason", function() {
		$(this).parent().parent().empty();
	});
});

function add_item(type) {
	if (type === "pattern"){
		var item = $(".diary-pattern > tr").eq(0).clone(true);
		$('#tb_pattern').append(item);
	}

	if (type === "reason"){
		var item = $(".diary-reason > tr").eq(0).clone(true);
		$('#tb_reason').append(item);
	}
}

function check_date() {
	start_yr = $("#start_yr").val();
	start_mon = $("#start_mon").val();
	start_day = $("#start_day").val();

	end_yr = $("#end_yr").val();
	end_mon = $("#end_mon").val();
	end_day = $("#end_day").val();

	start_dt = new Date(start_yr, start_mon-1, start_day);
	end_dt = new Date(end_yr, end_mon-1, end_day);
	kind_flag = $("[name^=kind_flag]:checked").val();

	msg = "";
	if (start_dt.getFullYear() != start_yr || start_dt.getMonth() != start_mon-1 || start_dt.getDate() != start_day) {
		msg += "開始日付が正しく指定されていません" + "\n";
	}

	if (end_dt.getFullYear() != end_yr || end_dt.getMonth() != end_mon-1 || end_dt.getDate() != end_day) {
		msg += "終了日付が正しく指定されていません" + "\n";
	}

	if (!kind_flag){
		msg += "「予定」または「実績」が選択されていません" + "\n";
	}

	var check_flag = true;
	$("[name^=pattern_id]").each(function(){
		if ($(this).val() != ""){
			check_flag = false;
			return false;
		}
	});
	$("[name^=reason_id]").each(function(){
		if ($(this).val() != ""){
			check_flag = false;
			return false;
		}
	});

	if (check_flag){
		msg += "「勤務パターン」または「休暇事由」を指定してください";
	}
	
	if (msg != ""){
		alert(msg);
		return false;
	}
	return true;
}
</script>

<!-- ************************************************************************ -->
<!-- HTML -->
<!-- ************************************************************************ -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}

select.itemhide{ display:none; }
select.expand{ width:96%; }
table.diary-table{
	font-size:14px;
	border-collapse:collapse;
	margin: 5px;
}
table.diary-table th,table.diary-table td{
	border:1px solid #aaa;
	padding:5px 8px;
	text-align:center;
}
table.diary-table th{
	background-color:#D2E9FF;
	color:#750000;
}
table.diary-table input[type="button"]{
	width:80px;
	cursor:pointer;
	background-color:#f0f0f0;
	border:1px solid #aaa;
	border-radius:3px;
	box-shadow:0 1px 2px #999;
}

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
	<!-- ------------------------------------------------------------------------ -->
	<!-- 画面遷移／タブ -->
	<!-- ------------------------------------------------------------------------ -->
	<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
	<?
		// 画面遷移
		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
		show_user_title($session, $section_admin_auth);			//duty_shift_common.ini
		echo("</table>\n");

		// タブ
		if ($group_id == "all") {
			$wk_group_id = "";
		} else {
			$wk_group_id = $group_id;
		}
		$arr_option = "&group_id=" . $wk_group_id;
		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
		show_user_tab_menuitem($session, $fname, $arr_option);	//duty_shift_user_tab_common.ini
		echo("</table>\n");

		// 下線
		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td bgcolor=\"#5279a5\"><img src=\"img/spacer.gif\" width\"1\" height=\"2\" alt=\"\"></td></tr></table><img src=\"img/spacer.gif\" width=\"1\" height=\"5\" alt=\"\"><br>\n");
	?>
	</td></tr></table>
	<!-- ------------------------------------------------------------------------ -->
	<!-- データ不備時 -->
	<!-- ------------------------------------------------------------------------ -->
	<?
	if ($err_flg_1 == "1") {
		echo("勤務シフトグループ情報が未設定です。管理画面で登録してください。");
	} elseif ($create_flg_cnt == 0) {
		echo("更新権限のあるシフトグループがありません。");
	} else {
	?>
	<!-- ------------------------------------------------------------------------ -->
	<!-- 入力エリア -->
	<!-- ------------------------------------------------------------------------ -->
	<form name="mainform" method="post">
		<table width="960" border="0" cellspacing="0" cellpadding="2" class="">
		<tr>
		<td align="left" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="duty_shift_diary_bld_manage.php?session=<?=$session?>">病棟管理日誌</a></font></td>
		<td align="left" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="duty_shift_diary.php?session=<?=$session?>">勤務データ出力</a></font></td>
		<td align="left" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="duty_shift_diary_allotment.php?session=<?=$session?>">勤務分担表</a></font></td>
		<td align="left" width="140"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="duty_shift_diary_sfc.php?session=<?=$session?>">管理日誌（SFC形式）</a></font></td>
		<td align="left" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="duty_shift_diary_list.php?session=<?=$session?>">在院者一覧</a></font></td>
		<td align="left" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>当直者一覧</b></font></td>
	<td align="left" width=""></td>
		</tr>
		</table>
		<!-- ------------------------------------------------------------------------ -->
		<!-- 勤務シフトグループ名 -->
		<!-- ------------------------------------------------------------------------ -->
		<table width="960" border="0" cellspacing="0" cellpadding="2" bgcolor="#F0F0F0">
		<tr>
			<td align="right">
				<!-- 表示ボタン -->
				<input type="submit" name="action" value="表示" onClick="return check_date();">
				<input type="button" value="CSV出力" onclick="downloadCSV()">
			</td>
		</tr>
		<tr height="22">
		<!-- 勤務シフトグループ名 -->
		<td width="960" align="left" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">シフトグループ：</font>
		<select name="group_id">
		<option value="all" <?php echo(($list_shift == "all")? "selected":"") ?>>全て</option>
		<?
		for($i=0;$i<count($group_array);$i++) {
			//更新権限確認
			if ($group_array[$i]["create_flg"] != "1") {
				continue;
			}
			$wk_id= $group_array[$i]["group_id"];
			$wk_name = $group_array[$i]["group_name"];
			echo("<option value=\"$wk_id\"");
			if ($list_shift == $wk_id) {
				echo(" selected");
			}
			echo(">$wk_name\n");
		}
		?>
		</select>&nbsp;&nbsp;&nbsp;
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">期間：</font>
		<select id='start_yr' name='start_yr'><? 
		show_select_years(15, $start_yr, false, true);
		?></select>年
		<select id='start_mon' name='start_mon'><? 
		show_select_months($start_mon, false);
		?></select>月
		<select id='start_day' name='start_day'><? 
		show_select_days($start_day, false);
		?></select>日
		&nbsp;〜&nbsp;
		<select id='end_yr' name='end_yr'><? 
		show_select_years(15, $end_yr, false, true);
		?></select>年
		<select id='end_mon' name='end_mon'><? 
		show_select_months($end_mon, false);
		?></select>月
		<select id='end_day' name='end_day'><? 
		show_select_days($end_day, false);
		?></select>日
		
		&nbsp;&nbsp;&nbsp;<label><input type="radio" name="kind_flag[]" value="1"  <?php echo($list_kind == "1")? "checked" : "" ?>/>予定</label>
  		<label><input type="radio" name="kind_flag[]" value="2" <?php echo($list_kind == "2")? "checked" : "" ?>/>実績</label>
		</td>
		</tr>
		<tr>
			<td>		
				<table width="960" border="0" cellspacing="0" cellpadding="2">
					<tr>
						<td width="420" align="left" >
						</td>
					<tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>		
				<table class="diary-table" width="470" style="float:left;">
					<thead>
						<tr>
						  <th width="58%">勤務パターン</th>
						  <th width="42%">
						  	<input type="button"  value="追加" style="width:50px" onclick="add_item('pattern');" />
						  </th>
						</tr>
					</thead>
					<tfoot class="diary-pattern" style="display:none;">
						<tr>
						  <td>
						    <select name="pattern_id[]" class="expand"><option></option><?php create_options($pattern_list, "") ?></select>
						  </td>
						  <td>
						    <input value="追加" type="button" class="addPattern" />　
						    <input value="削除" type="button" class="removePattern" />
						  </td>
						</tr>
					</tfoot>
					<tbody id="tb_pattern" class="diary-tbody">
						<?php 
							foreach ($list_pattern as $item_pattern) {	
						?>
								<tr>
								  <td>
								    <select name="pattern_id[]" class="expand"><option></option><?php create_options($pattern_list, $item_pattern) ?></select>
								  </td>
								  <td>
								    <input value="追加" type="button" class="addPattern" />　
								    <input value="削除" type="button" class="removePattern" />
								  </td>
								</tr>
						<?php
							} 
						?>
					</tbody>
				</table>
				
				<table class="diary-table" width="470" style="float:left;">
					<thead>
						<tr>
						  <th width="58%">休暇事由</th>
						  <th width="42%">
						  	<input type="button" value="追加" style="width:50px" onclick="add_item('reason');" />
						  </th>
						</tr>
					</thead>
					<tfoot class="diary-reason" style="display:none;">
						<tr>
						  <td>
						    <select name="reason_id[]" class="expand"><option></option><?php create_options($reason_list, "") ?></select>
						  </td>
						  <td>
						    <input value="追加" type="button" class="addReason" />　
						    <input value="削除" type="button" class="removeReason" />
						  </td>
						</tr>
					</tfoot>
					<tbody id="tb_reason" class="diary-tbody">
						<?php 
							foreach ($list_reason as $item_reason) {	
						?>
								<tr>
								  <td>
								    <select name="reason_id[]" class="expand"><option></option><?php create_options($reason_list, $item_reason) ?></select>
								  </td>
								  <td>
								    <input value="追加" type="button" class="addReason" />　
								    <input value="削除" type="button" class="removeReason" />
								  </td>
								</tr>
						<?php
							} 
						?>
					</tbody>
				</table>
			</td>
		</tr>
	</table>
<?
// 年月日
// 
// 表示、検索後
if ($action == "表示") {
?>
		<!-- ------------------------------------------------------------------------ -->
		<!-- 表 -->
		<!-- ------------------------------------------------------------------------ -->
		<table width="960" border="0" cellspacing="0" cellpadding="2" class="list">
			<!-- ------------------------------------------------------------------------ -->
			<!-- パターン一覧（見出し） -->
			<!-- ------------------------------------------------------------------------ -->
			<tr height="22" bgcolor="#f6f9ff">
			<?
			for ($i=0; $i<count($arr_title); $i++) {
			// width=\"8%\"
				echo("<td align=\"left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"> \n");
				echo($arr_title[$i]);
				echo("</font></td> \n");
			}
			?>

				</font></td>
			</tr>
			<!-- ------------------------------------------------------------------------ -->
			<!-- パターン一覧（データ） -->
			<!-- ------------------------------------------------------------------------ -->
			<?
			$cnt=0;
				for ($i=0; $i<count($data); $i++) {
					echo("<tr height=\"22\"> \n");
					for ($j=0; $j<count($arr_title); $j++) {
					// width=\"8%\"
						echo("<td align=\"left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"> \n");
						echo($data[$i][$j]);
						echo("</font></td> \n");
					}
					echo("</tr>\n");
				}
			?>
		</table>
<? } ?>
		<!-- ------------------------------------------------------------------------ -->
		<!-- ＨＩＤＤＥＮ -->
		<!-- ------------------------------------------------------------------------ -->
		<input type="hidden" name="session" value="<? echo($session); ?>">
		<input type="hidden" name="event" value="">

	</form>
	<?
	// target="download"
	?>
	<form name="csv" method="get" target="download">
	<input type="hidden" name="session" value="<? echo($session); ?>">
	<input type="hidden" name="group_id" value="<? echo($group_id); ?>">
	<input type="hidden" name="start_date" value="<? echo($start_date); ?>">
	<input type="hidden" name="end_date" value="<? echo($end_date); ?>">
	</form>
	<iframe name="download" width="0" height="0" frameborder="0"></iframe>

	<? } ?>

</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>


