<?/*

画面パラメータ
	$session
		セッションID
	$pt_id
		患者ID

*/?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<? require("about_authority.php"); ?>
<? require("about_session.php"); ?>
<? require("about_postgres.php"); ?>
<? require("show_kanja_joho.ini"); ?>
<? require("./get_values.ini"); ?>
<? require("./show_problem_input_form.ini"); ?>
<? require("summary_ymd_select_util.ini"); ?>
<? require("summary_common.ini"); ?>
<? require("label_by_profile_type.ini"); ?>
<? require_once("get_menu_label.ini"); ?>
<?

//ページ名
$fname = $PHP_SELF;

//==============================
//セッションのチェック
//==============================
$session = qualify_session($session,$fname);
if($session == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
// 権限チェック
//==============================
$summary = check_authority($session, 57, $fname);
if ($summary == "0")
{
	showLoginPage();
	exit;
}

//==============================
//DBコネクション
//==============================
$con = connect2db($fname);
if($con == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

//==========================================================================================
//HTML出力
//==========================================================================================
//==============================
//スクリプト/スタイルシート/BODY宣言 出力
//==============================
?>
<title><?=get_report_menu_label($con, $fname)?>｜プロブレム選択</title>
<?
//カレンダー外部ファイル
write_yui_calendar_use_file_read();
?>
<script type="text/javascript">

var problem_input_window_open_option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=400,top=200,width=550,height=350";

//プロブレム追加画面を表示します。
function showInsertWindow(insert_default_class)
{
	var url = "summary_problem_input.php?session=<?=$session?>&pt_id=<?=$pt_id?>&mode=insert&default_class=" + insert_default_class;
	window.open(url, 'problem_input_window',problem_input_window_open_option);
}

//プロブレム更新画面を表示します。
function showUpdateWindow(problem_id)
{
	var url = "summary_problem_input.php?session=<?=$session?>&pt_id=<?=$pt_id?>&mode=update&problem_id=" + problem_id;
	window.open(url, 'problem_input_window',problem_input_window_open_option);
}

//プロブレム選択時の処理を行います。
function problem_selected(problem_id,problem_no,problem)
{
	//呼び出し画面に通知します。
	window.opener.problem_list_selected_call_back(problem_id,problem_no,problem)

	//自画面をクローズ。
	window.close();
}

function load_action()
{
	//フォームに対するロード時の処理を行います。
	onload_show_proclem_input_form();
}

<?
//カレンダーJavaScript
write_common_ymd_ctl_script();
write_yui_calendar_script("start_date","start_date_year","start_date_month","start_date_day","start_date_caller","start_date_calendar");
write_yui_calendar_script("end_date","end_date_year","end_date_month","end_date_day","end_date_caller","end_date_calendar");
?>

</script>
</head>
<body onload="load_action();">

<!-- カレンダー用 START -->
<div id="start_date_calendar" style="position:absolute;display:none;z-index:10000;"> </div>
<div id="end_date_calendar" style="position:absolute;display:none;z-index:10000;"> </div>
<!-- カレンダー用 END -->

<div style="padding:4px">

<? //ヘッダー 出力 ?>
<? summary_common_show_dialog_header("プロブレム選択"); ?>

<? //追加選択/一覧選択切り替え ?>
<? summary_common_show_problem_select("追加選択", $pt_id); ?>

<? //一覧 出力 ?>
<div style="margin-top:12px">
<? show_problem_input_form($con,$fname,"insert_select",$session,$pt_id,"",""); ?>
</div>

</div>
</body>
</html>
<? pg_close($con); ?>
