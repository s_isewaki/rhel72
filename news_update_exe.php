<?php
require_once("Cmx.php");
require_once("about_comedix.php");
require_once("get_values.ini");
require_once("news_common.ini");
require_once("aclg_set.php");

$action = ($frompage == "1") ? "newsuser_update.php" : "news_update.php";
?>
<body>
<form name="items" action="<?php echo($action); ?>" method="post">
<input type="hidden" name="news_title" value="<?php echo(h($news_title)); ?>">
<input type="hidden" name="marker" value="<?php echo($marker); ?>">
<input type="hidden" name="news_category" value="<?php echo($news_category); ?>">
<input type="hidden" name="show_login_flg" value="<?php echo($show_login_flg); ?>">
<?php
if (is_array($class_id)) {
    for ($i = 0, $j = count($class_id); $i < $j; $i++) {
        echo("<input type=\"hidden\" name=\"class_id[]\" value=\"$class_id[$i]\">\n");
        echo("<input type=\"hidden\" name=\"atrb_id[]\" value=\"$atrb_id[$i]\">\n");
        echo("<input type=\"hidden\" name=\"dept_id[]\" value=\"$dept_id[$i]\">\n");

        if ($atrb_id[$i] == "0") {$atrb_id[$i] = null;}
        if ($dept_id[$i] == "0") {$dept_id[$i] = null;}
    }
}
if (is_array($job_id)) {
    for ($i = 0, $j = count($job_id); $i < $j; $i++) {
        echo("<input type=\"hidden\" name=\"job_id[]\" value=\"$job_id[$i]\">\n");
    }
}
?>
<input type="hidden" name="notice_id" value="<?php echo($notice_id); ?>">
<input type="hidden" name="notice2_id" value="<?php echo($notice2_id); ?>">
<?php
if (is_array($st_id)) {
    for ($i = 0, $j = count($st_id); $i < $j; $i++) {
        echo("<input type=\"hidden\" name=\"st_id[]\" value=\"$st_id[$i]\">\n");
    }
}
?>
<input type="hidden" name="news" value="<?php echo(h($news)); ?>">
<?php
foreach ($filename as $tmp_filename) {
    echo("<input type=\"hidden\" name=\"filename[]\" value=\"$tmp_filename\">\n");
}
foreach ($file_id as $tmp_file_id) {
    echo("<input type=\"hidden\" name=\"file_id[]\" value=\"$tmp_file_id\">\n");
}
?>
<input type="hidden" name="src_class_id" value="<?php echo($src_class_id); ?>">
<input type="hidden" name="src_atrb_id" value="<?php echo($src_atrb_id); ?>">
<input type="hidden" name="src_dept_id" value="<?php echo($src_dept_id); ?>">
<input type="hidden" name="src_room_id" value="<?php echo($src_room_id); ?>">
<input type="hidden" name="src_emp_id" value="<?php echo($src_emp_id); ?>">
<input type="hidden" name="src_emp_nm" value="<?php echo($hid_src_emp_nm); ?>">
<input type="hidden" name="record_flg" value="<?php echo($record_flg); ?>">
<input type="hidden" name="comment_flg" value="<?php echo($comment_flg); ?>">
<input type="hidden" name="news_begin1" value="<?php echo($news_begin1); ?>">
<input type="hidden" name="news_begin2" value="<?php echo($news_begin2); ?>">
<input type="hidden" name="news_begin3" value="<?php echo($news_begin3); ?>">
<input type="hidden" name="news_end1" value="<?php echo($news_end1); ?>">
<input type="hidden" name="news_end2" value="<?php echo($news_end2); ?>">
<input type="hidden" name="news_end3" value="<?php echo($news_end3); ?>">
<input type="hidden" name="term" value="<?php echo($term); ?>">
<input type="hidden" name="news_login1" value="<?php echo($news_login1); ?>">
<input type="hidden" name="news_login2" value="<?php echo($news_login2); ?>">
<input type="hidden" name="news_login3" value="<?php echo($news_login3); ?>">
<input type="hidden" name="news_login_end1" value="<?php echo($news_login_end1); ?>">
<input type="hidden" name="news_login_end2" value="<?php echo($news_login_end2); ?>">
<input type="hidden" name="news_login_end3" value="<?php echo($news_login_end3); ?>">
<input type="hidden" name="login_term" value="<?php echo($login_term); ?>">
<input type="hidden" name="emp_name" value="<?php echo($emp_name); ?>">
<input type="hidden" name="news_date1" value="<?php echo($news_date1); ?>">
<input type="hidden" name="news_date2" value="<?php echo($news_date2); ?>">
<input type="hidden" name="news_date3" value="<?php echo($news_date3); ?>">
<input type="hidden" name="session" value="<?php echo($session); ?>">
<input type="hidden" name="news_id" value="<?php echo($news_id); ?>">
<input type="hidden" name="page" value="<?php echo($page); ?>">
<input type="hidden" name="target_id_list1" value="<?php echo($target_id_list1); ?>">
<input type="hidden" name="deadline_date1" value="<?php echo($deadline_date1); ?>">
<input type="hidden" name="deadline_date2" value="<?php echo($deadline_date2); ?>">
<input type="hidden" name="deadline_date3" value="<?php echo($deadline_date3); ?>">
<input type="hidden" name="deadline_hour" value="<?php echo($deadline_hour); ?>">
<input type="hidden" name="deadline_min" value="<?php echo($deadline_min); ?>">
<input type="hidden" name="public_flg" value="<?php echo($public_flg); ?>">
<input type="hidden" name="henshin_flg" value="<?php echo($henshin_flg); ?>">
<input type="hidden" name="back" value="t">
<?php
// データ設定、配列は1番目から使用
$question = array();
$select_num = array();
$multi_flg = array();
$answer = array();
$answer_comment_flg = array();
$dst_idx = 1;
for ($src_idx=1; $src_idx<=$qa_cnt; $src_idx++) {
    $v_num_item = "select_changeItem_".$src_idx;
    if($$v_num_item=='0'){
        $v_num_src = "select_num_".$src_idx;
        if ($$v_num_src == "") {
            continue;
        }
        $select_num[$dst_idx] = $$v_num_src;
    
        $type[$dst_idx] = $$v_num_item;
        
        $v_question_src = "question_".$src_idx;
        $question[$dst_idx] = $$v_question_src;

        $v_mult_src = "multi_flg_".$src_idx;
        $multi_flg[$dst_idx] = $$v_mult_src;
        if ($multi_flg[$dst_idx] == "") {
            $multi_flg[$dst_idx] = "f";
        }

        for ($i = 1; $i <= $$v_num_src; $i++) {
            $varname = "answer".$src_idx."_".$i;
            $answer[$dst_idx][$i] = $$varname;
            $v_com_src = "answer_comment_flg" . $src_idx . "_" . $i;
            if ($$v_com_src != 't') {
                $$v_com_src = 'f';
            }
            $answer_comment_flg[$dst_idx][$i] = $$v_com_src;
            
        }
         $dst_idx++;
    } else if($$v_num_item=='1'){
        $select_num[$dst_idx] = "0";
        $v_question_src = "question_".$src_idx;
        $question[$dst_idx] = $$v_question_src;
        $type[$dst_idx] = $$v_num_item;
        $multi_flg[$dst_idx] = "f";
         $dst_idx++;
    }
   
}
$qa_cnt = $dst_idx - 1;
if ($qa_cnt == 0) {
    $qa_cnt = 1;
}
// 質問数分設定
for ($qa_idx=1; $qa_idx<=$qa_cnt; $qa_idx++) {
    echo("<input type=\"hidden\" name=\"question_{$qa_idx}\" value=\"{$question[$qa_idx]}\">\n");
    echo("<input type=\"hidden\" name=\"select_num_{$qa_idx}\" value=\"{$select_num[$qa_idx]}\">\n");
    echo("<input type=\"hidden\" name=\"multi_flg_{$qa_idx}\" value=\"{$multi_flg[$qa_idx]}\">\n");
    echo("<input type=\"hidden\" name=\"select_changeItem_{$qa_idx}\" value=\"{$type[$qa_idx]}\">\n");

    for ($i = 1; $i <= $select_num[$qa_idx]; $i++) {
        echo("<input type=\"hidden\" name=\"answer{$qa_idx}_{$i}\" value=\"{$answer[$qa_idx][$i]}\">\n");
        echo("<input type=\"hidden\" name=\"answer_comment_flg{$qa_idx}_{$i}\" value=\"{$answer_comment_flg[$qa_idx][$i]}\">\n");
    }
}
?>
<input type="hidden" name="qa_cnt" value="<?php echo($qa_cnt); ?>">
</form>
<?php
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$auth_id = ($frompage == "1") ? 46 : 24;
$checkauth = check_authority($session, $auth_id, $fname);
if ($check_auth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// 入力チェック
$news_title = trim($news_title);
if ($news_title == "") {
    echo("<script type=\"text/javascript\">alert('タイトルが入力されていません。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}
if (strlen($news_title) > 100) {
    echo("<script type=\"text/javascript\">alert('タイトルが長すぎます。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}
// 回覧時、対象者チェック
if ($news_category == 10000 && $target_id_list1 == "") {
    echo("<script type=\"text/javascript\">alert('対象者を指定してください。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}

$news = trim($news);
if ($news == "") {
    echo("<script type=\"text/javascript\">alert('内容を入力してください。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}
$qa_input_cnt = 0;
if ($record_flg == "t") {
    // 質問数分確認
    for ($qa_idx=1; $qa_idx<=$qa_cnt; $qa_idx++) {
        if (strlen($question[$qa_idx]) != "") {
            $qa_input_cnt++;
        }
        if (strlen($question[$qa_idx]) > 100) {
            echo("<script type=\"text/javascript\">alert('質問{$qa_idx}が長すぎます。');</script>");
            echo("<script type=\"text/javascript\">document.items.submit();</script>");
            exit;
        }
        $select_changeItem_tmp = "select_changeItem_".$qa_idx;
        
        if ($question[$qa_idx] != "" && $$select_changeItem_tmp == 0) {
            
            if ($select_num[$qa_idx] == 0) {
                echo("<script type=\"text/javascript\">alert('質問{$qa_idx}の選択肢数を選んでください。');</script>");
                echo("<script type=\"text/javascript\">document.items.submit();</script>");
                exit;
            }
            for ($i = 1; $i <= $select_num[$qa_idx]; $i++) {
                if ($answer[$qa_idx][$i] == "") {
                    echo("<script type=\"text/javascript\">alert('質問{$qa_idx}の{$i}番目の回答内容を入力してください。');</script>");
                    echo("<script type=\"text/javascript\">document.items.submit();</script>");
                    exit;
                }
                if (strlen($answer[$qa_idx][$i]) > 40) {
                    echo("<script type=\"text/javascript\">alert('質問{$qa_idx}の{$i}番目の回答内容が長すぎます。');</script>");
                    echo("<script type=\"text/javascript\">document.items.submit();</script>");
                    exit;
                }
            }
        }
    }
}

// アンケートがある場合、締切日の確認
if ($qa_input_cnt > 0) {
    // 開始日より前はエラー
    if ($deadline_date1.$deadline_date2.$deadline_date3 < $news_date1.$news_date2.$news_date3) {
        echo("<script type=\"text/javascript\">alert('締切り日時は掲載期間の開始日以降にしてください。');</script>");
        echo("<script type=\"text/javascript\">document.items.submit();</script>");
        exit;
    }
}

if (!checkdate($news_begin2, $news_begin3, $news_begin1)) {
    echo("<script type=\"text/javascript\">alert('掲載期間（From）が不正です。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}
if (!checkdate($news_end2, $news_end3, $news_end1)) {
    echo("<script type=\"text/javascript\">alert('掲載期間（To）が不正です。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}
$news_begin = "$news_begin1$news_begin2$news_begin3";
$news_end = "$news_end1$news_end2$news_end3";
if ($news_begin > $news_end) {
    echo("<script type=\"text/javascript\">alert('掲載期間が不正です。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}

if (!checkdate($news_login2, $news_login3, $news_login1)) {
    echo("<script type=\"text/javascript\">alert('掲載期間（ログイン画面：From）が不正です。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}
if (!checkdate($news_login_end2, $news_login_end3, $news_login_end1)) {
    echo("<script type=\"text/javascript\">alert('掲載期間（ログイン画面：To）が不正です。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}
$news_login = "$news_login1$news_login2$news_login3";
$news_login_end = "$news_login_end1$news_login_end2$news_login_end3";
if ($news_login > $news_login_end) {
    echo("<script type=\"text/javascript\">alert('掲載期間（ログイン画面）が不正です。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}

// 添付ファイルの確認
if (!is_dir("news")) {
    mkdir("news", 0755);
}
if (!is_dir("news/tmp")) {
    mkdir("news/tmp", 0755);
}
for ($i = 0; $i < count($filename); $i++) {
    $tmp_file_id = $file_id[$i];
    $ext = strrchr($filename[$i], ".");

    $tmp_filename = "news/tmp/{$session}_{$tmp_file_id}{$ext}";
    if (!is_file($tmp_filename)) {
        echo("<script language=\"javascript\">alert('添付ファイルが削除された可能性があります。\\n再度添付してください。');</script>");
        echo("<script language=\"javascript\">document.items.submit();</script>");
        exit;
    }
}

// 登録値の編集
if ($news_category != "3") {
    $job_id = null;
}
$show_login_flg = ($show_login_flg == "t") ? "t" : "f";

if ($notice_id == "0") {$notice_id = null;}
if ($notice2_id == "0") {$notice2_id = null;}
if ($src_class_id == 0) {$src_class_id = null;}
if ($src_atrb_id == 0) {$src_atrb_id = null;}
if ($src_dept_id == 0) {$src_dept_id = null;}
if ($src_room_id == 0) {$src_room_id = null;}

if ($news_category == "10000") {
	$st_id = array();
}
$st_id_cnt = 0;
for ($i = 0, $j = count($st_id); $i < $j; $i++) {
    if ($st_id[$i] != "") {
        $st_id_cnt++;
    }
}

// トランザクションの開始
pg_query($con, "begin");

// ログインユーザの職員IDを取得
$emp_id = get_emp_id($con, $session, $fname);

//20120322 add
if($henshin_flg != "t") {
	$henshin_flg = "f";
}

// お知らせ情報を更新
$sql = "update news set";
$set = array("date", "news_date", "news_begin", "news_end", "news_title", "news_category", "news", "job_id", "show_login_flg", "notice_id", "notice2_id", "src_class_id", "src_atrb_id", "src_dept_id", "src_room_id", "record_flg", "src_emp_id", "marker", "st_id_cnt", "news_add_category", "comment_flg", "public_flg", "henshin_flg","news_login","news_login_end");
$setvalue = array(date("YmdHi"), "$news_date1$news_date2$news_date3", $news_begin, $news_end, pg_escape_string($news_title), "", pg_escape_string($news), null, $show_login_flg, $notice_id, $notice2_id, $src_class_id, $src_atrb_id, $src_dept_id, $src_room_id, $record_flg, $src_emp_id, $marker, $st_id_cnt, $news_category, $comment_flg, $public_flg, $henshin_flg,$news_login,$news_login_end);
if ($qa_input_cnt > 0 ) {
    array_push($set, "deadline");
    $deadline = $deadline_date1.$deadline_date2.$deadline_date3.$deadline_hour.$deadline_min;
    array_push($setvalue, $deadline);
}
$cond = "where news_id = $news_id";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// お知らせ配信先部署情報を更新
$sql = "delete from newsdept";
$cond = "where news_id = $news_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
if ($news_category == "2") {
    for ($i = 0, $j = count($class_id); $i < $j; $i++) {
        $sql = "delete from newsdept";
        $cond = "where news_id = $news_id and class_id = $class_id[$i]";
        if (!is_null($atrb_id[$i])) {
            $cond .= " and atrb_id = $atrb_id[$i]";
        } else {
            $cond .= " and atrb_id is null";
        }
        if (!is_null($dept_id[$i])) {
            $cond .= " and dept_id = $dept_id[$i]";
        } else {
            $cond .= " and dept_id is null";
        }
        $del = delete_from_table($con, $sql, $cond, $fname);
        if ($del == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }

        $sql = "insert into newsdept (news_id, class_id, atrb_id, dept_id) values (";
        $content = array($news_id, $class_id[$i], $atrb_id[$i], $dept_id[$i]);
        $ins = insert_into_table($con, $sql, $content, $fname);
        if ($ins == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }
}

// お知らせ職種情報を登録
if ($news_category == "3") {
    $sql = "delete from newsjob";
    $cond = "where news_id = $news_id";
    $del = delete_from_table($con, $sql, $cond, $fname);
    if ($del == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    for ($i = 0, $j = count($job_id); $i < $j; $i++) {
        $sql = "insert into newsjob (news_id, job_id) values (";
        $content = array($news_id, $job_id[$i]);
        $ins = insert_into_table($con, $sql, $content, $fname);
        if ($ins == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }
}

// 役職情報を更新
$sql = "delete from newsst";
$cond = "where news_id = $news_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
if ($st_id_cnt > 0) {
    for ($i = 0, $j = count($st_id); $i < $j; $i++) {
        if ($st_id[$i] == "") {
            continue;
        }
        $sql = "insert into newsst (news_id, st_id) values (";
        $content = array($news_id, $st_id[$i]);
        $ins = insert_into_table($con, $sql, $content, $fname);
        if ($ins == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }
}

// 添付ファイル情報を削除
$sql = "delete from newsfile";
$cond = "where news_id = '$news_id'";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// 添付ファイル情報を作成
$no = 1;
foreach ($filename as $tmp_filename) {
    $sql = "insert into newsfile (news_id, newsfile_no, newsfile_name) values (";
    $content = array($news_id, $no, pg_escape_string($tmp_filename));
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $no++;
}

// 回答設定を更新
$sql = "delete from newsenquet_q";
$cond = "where news_id = $news_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

$sql = "delete from newsenquet_a";
$cond = "where news_id = $news_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

for ($i = 1; $i <= $qa_cnt; $i++) {
    if ($question[$i] != "") {
        $sql = "insert into newsenquet_q (news_id, question_no, question, select_num, multi_flg,type) values (";
        $content = array($news_id, $i, $question[$i], $select_num[$i], $multi_flg[$i], $type[$i]);
        $ins = insert_into_table($con, $sql, $content, $fname);
        if ($ins == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        for ($j = 1; $j <= $select_num[$i]; $j++) {
            $sql = "insert into newsenquet_a (news_id, question_no, item_no, answer, answer_comment_flg) values (";
            $content = array($news_id, $i, $j, $answer[$i][$j], $answer_comment_flg[$i][$j]);
            $ins = insert_into_table($con, $sql, $content, $fname);
            if ($ins == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
        }
    }
}

// カテゴリが回覧板の場合
if ($news_category == "10000") {
    $arr_emp_id = split(",", $target_id_list1);
    // 元の回覧対象者を取得
    $old_target_id_list1 = get_circular_target($con, $news_id, $fname);
    if ($old_target_id_list1 != "") {
        $arr_old_target_id = split(",", $old_target_id_list1);

        // 回覧対象者にない場合削除
        for ($i = 0, $j = count($arr_old_target_id); $i < $j; $i++) {

            if (!in_array($arr_old_target_id[$i], $arr_emp_id) ) {

                $sql = "delete from newscomment";
                $cond = "where news_id = $news_id and emp_id = '$arr_old_target_id[$i]'";
                $del = delete_from_table($con, $sql, $cond, $fname);
                if ($del == 0) {
                    pg_query($con, "rollback");
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }
            }
        }
    }

    // 回覧対象者を追加
    for ($i = 0, $j = count($arr_emp_id); $i < $j; $i++) {
        // 未登録の場合追加
        if (!in_array($arr_emp_id[$i], $arr_old_target_id)) {
            $sql = "insert into newscomment (news_id, emp_id) values (";
            $content = array($news_id, $arr_emp_id[$i]);
            $ins = insert_into_table($con, $sql, $content, $fname);
            if ($ins == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
        }
    }
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 添付ファイルの移動
foreach (glob("news/{$news_id}_*.*") as $tmpfile) {
    unlink($tmpfile);
}
for ($i = 0; $i < count($filename); $i++) {
    $tmp_file_id = $file_id[$i];
    $tmp_filename = $filename[$i];
    $tmp_fileno = $i + 1;
    $ext = strrchr($tmp_filename, ".");

    $tmp_filename = "news/tmp/{$session}_{$tmp_file_id}{$ext}";
    copy($tmp_filename, "news/{$news_id}_{$tmp_fileno}{$ext}");
}
foreach (glob("news/tmp/{$session}_*.*") as $tmpfile) {
    unlink($tmpfile);
}

// お知らせ一覧画面に遷移
if ($frompage == "1") {
    $list_page = "newsuser_list.php";
} else {
    $list_page = "news_menu.php";
}
echo("<script type=\"text/javascript\">location.href = '$list_page?session=$session&page=$page';</script>");
?>
</body>
