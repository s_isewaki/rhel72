<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix タイムカード | 登録結果</title>
<?php
require_once("about_comedix.php");
require_once("login_message.php");
require_once("news_common.ini");
require_once("show_marker.ini");
require_once("timecard_bean.php");

$con = connect2db($fname);
//タイムカード情報の取得
$timecard_bean = new timecard_bean();
$timecard_bean->select($con, $fname);
$ret1_str = ($timecard_bean->return_icon_flg != "2") ? "退勤後復帰" : "呼出出勤";
$ret2_str = ($timecard_bean->return_icon_flg != "2") ? "復帰後退勤" : "呼出退勤";

// 打刻時間を取得・文言を設定
switch ($wherefrom) {
case "1":  // 出勤
	$col_nm = "start_time";
	$type_nm = "出勤";
	break;
case "2":  // 外出
	$col_nm = "out_time";
	$type_nm = "外出";
	break;
case "3":  // 復帰
	$col_nm = "ret_time";
	$type_nm = "復帰";
	break;
case "4":  // 退勤
	$col_nm = "end_time";
	$type_nm = "退勤";
	break;
case "5":  // 退勤後復帰
	$col_nm = "o_start_time$ret_index";
		$type_nm = $ret1_str;
	break;
case "6":  // 復帰後退勤
	$col_nm = "o_end_time$ret_index";
		$type_nm = $ret2_str;
	break;
}
$sql = "select $col_nm from atdbkrslt";
$cond = "where emp_id = (select emp_id from login where emp_login_id = '$id' and emp_login_pass = '$pass') and date = '$date'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
$time = pg_fetch_result($sel, 0, 0);
$time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $time);

$sql = "select emp_lt_nm, emp_ft_nm, emp_id, emp_job from empmst";
$cond = "where emp_id = (select emp_id from login where emp_login_id = '$id' and emp_login_pass = '$pass')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
$emp_lt_nm = pg_fetch_result($sel, 0, "emp_lt_nm");
$emp_ft_nm = pg_fetch_result($sel, 0, "emp_ft_nm");
$emp_id = pg_fetch_result($sel, 0, "emp_id");
$job_id = pg_fetch_result($sel, 0, "emp_job");

// オプション設定情報を取得
$sql = "select newpost_days from newsoption";
$cond = "where emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if (pg_num_rows($sel) > 0) {
	$newpost_days = pg_fetch_result($sel, 0, "newpost_days");
} else {
	$newpost_days = "3";
}

// お知らせポップアップ設定を取得
$sql = "select popup_news from config";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$popup_news = pg_fetch_result($sel, 0, "popup_news");


// お知らせ一覧を取得
$today = date("Ymd");
$sql = "select news.*, newsref.ref_time, newsref.read_flg, newsnotice.notice_name, (select class_nm from classmst where classmst.class_id = news.src_class_id) as src_class_nm, (select atrb_nm from atrbmst where atrbmst.atrb_id = news.src_atrb_id) as src_atrb_nm, (select dept_nm from deptmst where deptmst.dept_id = news.src_dept_id) as src_dept_nm, (select room_nm from classroom where classroom.room_id = news.src_room_id) as src_room_nm, srcempmst.emp_lt_nm as src_emp_lt_nm, srcempmst.emp_ft_nm as src_emp_ft_nm from ((news left join (select * from newsref where emp_id = '$emp_id') newsref on news.news_id = newsref.news_id) left join newsnotice on news.notice_id = newsnotice.notice_id) left join empmst srcempmst on news.src_emp_id = srcempmst.emp_id left join newscate on news.news_add_category = newscate.newscate_id";
$cond = "where news.news_begin <= '$today' and news.news_end >= '$today' and news.news_del_flg = 'f' and ( ((news.news_add_category = 1 or (news.news_add_category = 2 and exists (select * from newsdept where newsdept.news_id = news.news_id and ((newsdept.class_id = (select emp_class from empmst where emp_id = '$emp_id')) or (exists (select * from concurrent where emp_id = '$emp_id' and concurrent.emp_class = newsdept.class_id))) and ((newsdept.atrb_id = (select emp_attribute from empmst where emp_id = '$emp_id')) or (newsdept.atrb_id is null) or (exists (select * from concurrent where emp_id = '$emp_id' and concurrent.emp_attribute = newsdept.atrb_id))) and ((newsdept.dept_id = (select emp_dept from empmst where emp_id = '$emp_id')) or (newsdept.dept_id is null) or (exists (select * from concurrent where emp_id = '$emp_id' and concurrent.emp_dept = newsdept.dept_id))))) or (news.news_add_category = 3 and news.job_id = $job_id)) ";
// 役職条件追加
$cond .= " and (news.st_id_cnt = 0 or (news.st_id_cnt > 0 and exists (select * from newsst where newsst.news_id = news.news_id and newsst.st_id in (select emp_st from empmst where empmst.emp_id = '$emp_id' union select emp_st from concurrent where concurrent.emp_id = '$emp_id'))))) or (   ";

$cond .= " news.news_add_category >= 4 and (";
$cond .= "  (newscate.standard_cate_id = 1 or (newscate.standard_cate_id = 2 and exists (select * from newscatedept where newscatedept.newscate_id = news.news_add_category and ((newscatedept.class_id = (select emp_class from empmst where emp_id = '$emp_id')) or (exists (select * from concurrent where emp_id = '$emp_id' and concurrent.emp_class = newscatedept.class_id))) and ((newscatedept.atrb_id = (select emp_attribute from empmst where emp_id = '$emp_id')) or (newscatedept.atrb_id is null) or (exists (select * from concurrent where emp_id = '$emp_id' and concurrent.emp_attribute = newscatedept.atrb_id))) and ((newscatedept.dept_id = (select emp_dept from empmst where emp_id = '$emp_id')) or (newscatedept.dept_id is null) or (exists (select * from concurrent where emp_id = '$emp_id' and concurrent.emp_dept = newscatedept.dept_id))))) or (newscate.standard_cate_id = 3 and newscate.job_id = $job_id) ) ";
$cond .= "	  and (newscate.st_id_cnt = 0 or (newscate.st_id_cnt > 0 and exists (select * from newscatest where newscatest.newscate_id = news.news_add_category and newscatest.st_id in (select emp_st from empmst where empmst.emp_id = '$emp_id' union select emp_st from concurrent where concurrent.emp_id = '$emp_id'))))";
$cond .= "  )";
$cond .= " )";
$cond .= ")";

$cond .= " order by news.news_date desc, news.news_id desc";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

//	$arr_category = array("1" => "全館", "2" => "部署", "3" => "職種");
$arr_category = get_category_list_from_db($con, $fname);

$comp_date = date("Ymd", strtotime("-" . ($newpost_days - 1) . " day", mktime(0, 0, 0, date("m"), date("d"), date("Y"))));

$padding = ($font_size == "1") ? "2" : "3";
$table_class = (pg_num_rows($sel) == 0) ? "" : " class=\"block\"";


?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/popup.js"></script>
<script type="text/javascript" src="js/yui/build/yahoo/yahoo-min.js"></script>
<script type="text/javascript" src="js/yui/build/treeview/treeview-min.js"></script>

<script type="text/javascript">
<!--
function login() {
	opener.document.loginform.id.value = '<? echo($id); ?>';
	opener.document.loginform.pass.value = '<? echo($pass); ?>';
	opener.document.loginform.submit();
	self.close();
}

function popupNewsDetail(ref_status, title, category, notice_name, src_class_name, src_emp_name, detail, date, e) {
<? if ($popup_news != "t") { ?>
	return;
<? } ?>
	popupDetailYellow(
		new Array(
			'確認', ref_status,
			'タイトル', title,
			'カテゴリ', category,
			'通達区分', notice_name,
			'発信部署', src_class_name,
			'発信者', src_emp_name,
			'内容', detail,
			'登録日', date
		), 400, 100, e
	);
}

//-->
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}

.block {border-collapse:collapse; border:#5279a5 solid 1px;}
.block .header td {background-color:#bdd1e7}
.block .column td {background-color:#f6f9ff}
.block td, .block td .block td {border:#5279a5 solid 1px;}
.block td td {border-style:none;}

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>登録結果</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<table width="600" border="0" cellspacing="0" cellpadding="1">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">以下の時間で<? echo($type_nm); ?>登録しました。</font></td>

<?
if ($date_change == true){
echo("<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j18' color='red'><b>警告：前日の退勤時刻が未登録です。</b></font></td>");
}
?>

</tr>
</table>
<?
$cnt_login_message = get_cnt_login_message($con, $emp_id, $fname);
if ($cnt_login_message > 0) {
	$wk_colspan = "colspan=\"2\"";
}
?>
<table width="600" border="0" cellspacing="0" cellpadding="4" class="list">
	<tr>
		<td align="left" <?=$wk_colspan?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><b>職員氏名：<? echo($emp_lt_nm."　".$emp_ft_nm); ?>様</b></font></td>
	</tr>
	<tr>
		<td align="center" style="font-size:150px;background-color:#ffffff;color:#5279a5;border:#5279a5 solid 1px"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($time); ?></font></td>
<?
if ($cnt_login_message > 0) {
?>
		<td align="center" style="font-size:50px;background-color:#ffffff;color:#ff0000;border:#5279a5 solid 1px"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">伝言メモ<br>あり！</font></td>
<?
}
?>
	</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="1">
<tr>
<td align="right">
<input type="button" value="閉じる" onclick="opener.document.loginform.id.focus();self.close();" style="font-size:1.1em;">
<? if ($login == "true") { ?>
<input type="button" value="ログイン" onclick="login();" style="font-size:1.1em;">
<? } ?>
</td>
</tr>
</table>


<?
	// お知らせの通達区分関連のJavaScriptを出力する
	insert_notice_javascript();

?>
</td>
</tr>
</table>

<?
	show_login_message($con, $emp_id, $font_size, $session, $fname);
?>
<br>

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="block">
	<tr bgcolor="#bdd1e7">
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="spacing" style="padding-top:1px;padding-bottom:1px;">
						<a href="newsuser_menu.php?session=<?=$session ?>"><img src="img/icon/s35.gif" alt="お知らせ" width="20" height="20" border="0"></a>
					</td>
					<td width="100%">
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
							<b>お知らせ</b>
<?php
// 通達区分のリンクを表示
show_notice_link($con, $fname);
?>
						</font>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td style="padding:2px;">
<?php
	$font_class = "j12";
	if (pg_num_rows($sel) == 0) {
        echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\n");
		echo("<tr height=\"22\">\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">お知らせはありません。</font></td>\n");
		echo("</tr>\n");
	} else {
        echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"block\">\n");
		echo("<tr height=\"22\" style=\"background-color:#f6f9ff;\">\n");
		echo("<td width=\"18%\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">カテゴリ</font></td>\n");
		echo("<td width=\"17%\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">確認</font></td>\n");
		echo("<td width=\"49%\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">タイトル</font></td>\n");
		echo("<td width=\"16%\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">登録日</font></td>\n");
		echo("</tr>\n");

		while ($row = pg_fetch_array($sel)) {
			$tmp_news_id = $row["news_id"];
			$tmp_title = $row["news_title"];
			$tmp_category = $arr_category[$row["news_add_category"]]["newscate_name"];
			$tmp_date = preg_replace("/^(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $row["news_date"]);

			if ($row["record_flg"] == "f") {
/*
				if ($row["read_control"] == "t" && $row["read_flg"] != "t") {
					$tmp_ref_status = "未読";
				} else if ($row["read_flg"] == "t") {
					$tmp_ref_status = "既読";
				} else {
*/
					$tmp_ref_status = "";
/*
				}
*/
			} else {
				if ($row["ref_time"] == "") {
/*
					if ($row["read_control"] == "t" && $row["read_flg"] != "t") {
						$tmp_ref_status = "未読";
					} else if ($row["read_flg"] == "t") {
*/
						$tmp_ref_status = "未確認";
/*
					} else {
						$tmp_ref_status = "";
					}
*/
				} else {
					$tmp_ref_status = "確認済み";
				}
			}

			$tmp_notice_id = $row["notice_id"];
			$tmp_notice_name = $row["notice_name"];
			$tmp_src_class_name = $row["src_class_nm"];
			if ($row["src_atrb_nm"] != "") {
				$tmp_src_class_name .= " &gt; " . $row["src_atrb_nm"];
			}
			if ($row["src_dept_nm"] != "") {
				$tmp_src_class_name .= " &gt; " . $row["src_dept_nm"];
			}
			if ($row["src_room_nm"] != "") {
				$tmp_src_class_name .= " &gt; " . $row["src_room_nm"];
			}
			$tmp_src_emp_name = $row["src_emp_lt_nm"] . " " . $row["src_emp_ft_nm"];
			$tmp_detail = preg_replace("/\r?\n/", "<br>", h_jsparam(preg_replace("/>(\r?\n)+</", "><", $row["news"])));

			$new = (substr($row["news_date"], 0, 8) >= $comp_date && $row["read_flg"] != "t" && $row["ref_time"] == "") ? "<img src=\"img/icon/new.gif\" alt=\"NEW\" width=\"24\" height=\"10\" style=\"margin-left:4px;\">" : "";

			$style = get_style_by_marker($row["marker"]);

			echo("<tr id=\"nid$tmp_notice_id\" height=\"22\" style=\"display:\">\n");
			echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">$tmp_category</font></td>\n");
			echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">$tmp_ref_status</font></td>\n");
			echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\"><a href=\"javascript:void(0);\" onclick=\"window.open('news_detail_from_login.php?session=$session&news_id=$tmp_news_id&emp_id=$emp_id', 'newwin_login', 'width=730,height=480,scrollbars=yes')\" onmousemove=\"popupNewsDetail('$tmp_ref_status', '" . h(h_jsparam($tmp_title)) . "', '$tmp_category', '$tmp_notice_name', '$tmp_src_class_name', '$tmp_src_emp_name', '$tmp_detail', '$tmp_date', event);\" onmouseout=\"closeDetail();\"$style>" . h($tmp_title) . "</a>$new</font></td>\n");
			echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">$tmp_date</font></td>\n");
			echo("</tr>\n");
		}

	}

?>
			</table>
		</td>
	</tr>
</table>

</center>
</body>
<? pg_close($con); ?>
</html>
