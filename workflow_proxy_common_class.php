<?php
/* 代理申請共通部品ＣＬＡＳＳ */
require_once("about_comedix.php");

class workflow_proxy_common_class
{
    var $file_name;	// 呼び出し元ファイル名
    var $_db_con;	// DBコネクション
    
    /*************************************************************************/
    // コンストラクタ
    // @param object $con DBコネクション
    // @param string $fname 画面名
    /*************************************************************************/
    function workflow_proxy_common_class($con, $fname)
    {
        // 呼び出し元ファイル名を保持
        $this->file_name = $fname;
        // DBコネクションを保持
        $this->_db_con = $con;
    }
    
    
    /**
     * 代理申請職員
     *
     * @return array 職員情報の配列、no,職員ID,職員名
     *
     */
    function get_workflow_proxy_staff_array() {
        //-------------------------------------------------------------------
        //代理申請職員より情報取得
        //-------------------------------------------------------------------
        $sql = "select a.no, a.emp_id, b.emp_lt_nm || ' ' || b.emp_ft_nm as name, c.job_nm, d.st_nm from wkfw_proxy a "
            ." left join empmst b on b.emp_id = a.emp_id"
            ." left join jobmst c on c.job_id = b.emp_job"
            ." left join stmst d on d.st_id = b.emp_st";
        $cond = "order by no";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        $data = array();
        $i = 0;
        while ($row = pg_fetch_array($sel)) {
            $data[$i]["no"] = $row["no"];
            $data[$i]["id"] = $row["emp_id"];				//職員ＩＤ
            $data[$i]["name"] = $row["name"];				//職員名
            $data[$i]["job_nm"] = $row["job_nm"];			//職種
            $data[$i]["st_nm"] = $row["st_nm"];			//役職
            $i++;
        }
        return $data;
    }
    
    
    /**
     * 指定職員が全て代理申請可能か確認
     *
     * @param string $emp_id 職員ID
     * @return boolean true:可能、false:不可
     *
     */
    function is_workflow_proxy_staff($emp_id) {
        //-------------------------------------------------------------------
        //代理申請職員より情報取得
        //-------------------------------------------------------------------
        $sql = "select count(*) as cnt from wkfw_proxy ";
        $cond = "where emp_id = '$emp_id' ";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        $cnt = pg_fetch_result($sel, 0, "cnt");
        
        return ($cnt > 0);
    }
    
    /**
     * 代理申請職員名取得
     *
     * @param array $set_array 職員情報の配列
     * @return array 職員IDをキーに職員名を値とする配列を返す
     *name?
     */
    function get_workflow_proxy_staff_name($set_array) {
        
        $data = array();
        if (count($set_array) == 0) {
            return $data;
        }
        
        $emp_id_str = "";
        for ($i=0; $i<count($set_array); $i++) {
            if ($i != 0 ) {
                $emp_id_str .= ",";
            }
            $emp_id_str .= "'".$set_array[$i]["id"]."'";
        }
        //-------------------------------------------------------------------
        //職員マスタより情報取得
        //-------------------------------------------------------------------
        $sql = "select a.emp_id, a.emp_lt_nm || ' ' || a.emp_ft_nm as name, b.job_nm, c.st_nm from empmst a "
            ." left join jobmst b on b.job_id = a.emp_job"
            ." left join stmst c on c.st_id = a.emp_st";
       $cond = "where emp_id in ($emp_id_str) ";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        while ($row = pg_fetch_array($sel)) {
            $wk_emp_id = $row["emp_id"];
            $data["$wk_emp_id"]["name"] = $row["name"];					//職員名
            $data["$wk_emp_id"]["job_nm"] = $row["job_nm"];				//職種
            $data["$wk_emp_id"]["st_nm"] = $row["st_nm"];			//役職
        }
        return $data;
    }
    
    
}