<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("cl_common.ini");
require_once("cl_application_workflow_common_class.php");
require_once("cl_title_name.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	showLoginPage();
	exit;
}

// データベースに接続
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$obj = new cl_application_workflow_common_class($con, $fname);


//======================================
// 職員情報取得
//======================================
$arr_empmst = $obj->get_empmst($session);
$login_emp_id = $arr_empmst[0]["emp_id"];
$emp_room = $arr_empmst[0]["emp_room"];

$hierarchy_div = "3";
if($emp_room != "")
{
    $hierarchy_div = "4";
}

$arr_emp_list = array();

// 看護診断委員会   2011.12.02 Edit by matsuura
//if($level == "3" && ($input_div == "cl_grader1" || $input_div == "cl_grader2"))
if($level == "3" && $nurse_type != "3" && ($input_div == "cl_grader1" || $input_div == "cl_grader2"))
{
    $arr_cl_committee = $obj->get_cl_committee("2");

    foreach($arr_cl_committee as $cl_committee)
    {
        $arr_project_member = $obj->get_project_member($cl_committee["parent_pjt_id"], $cl_committee["child_pjt_id"]);
        $arr_emp_list = $obj->merge_arr_emp_info($arr_emp_list, $arr_project_member);
    }
}
// 師長または師長以外の評価者
else
{
	switch ($input_div) {
	case "cl_grader4":
	case "cl_grader6":
	case "cl_grader6_2":	// 2011.12.01 Add by matsuura
		$eval_approval_div = "3";  // 師長
		break;
	case "cl_grader3":
	case "cl_grader5":
	case "cl_grader5_2":	// 2011.12.01 Add by matsuura
		$eval_approval_div = "2";  // 上司
		break;
	default:
		$eval_approval_div = "1";  // 同僚
	}

    $arr_emp_list = $obj->get_eval_approval_list($login_emp_id, $hierarchy_div, $eval_approval_div, $srch_emp_nm);
}


switch($input_div)
{
    case "cl_grader1":
        $title_nm = "<ナーシングプロセス>";
        if($level == "2")
        {
            $title_nm .= "評価1.同僚評価者１";
        }
        else if($level == "3")
        {
            // 2011.12.01 Add by matsuura
            if($nurse_type == "3"){
	            $title_nm .= "評価1.同僚評価者１";
            }else{
	            $title_nm .= "評価1.看護診断委員会委員長";
            }
        }
        break;

    case "cl_grader2":
        $title_nm = "<ナーシングプロセス>";
        if($level == "2")
        {
            $title_nm .= "評価1.同僚評価者２";
        }
        else if($level == "3")
        {
            // 2011.12.01 Add by matsuura
            if($nurse_type == "3"){
	            $title_nm .= "評価1.同僚評価者２";
            }else{
	        	$title_nm .= "評価1.看護診断委員会副委員長";
            }
        }
        break;

    case "cl_grader3":
        $title_nm = "<ナーシングプロセス>";

    	// 2011.12.01 Edit by matsuura
        if($nurse_type == "3"){
    		$title_nm .= "評価1.上司評価者";
    	}else{
	        $title_nm .= "評価2.上司評価者";
    	}
        break;

    	//----------- 2011.12.01 Add by matsuura start ------------
    case "cl_grader3_1":
        $title_nm = "<ナーシングプロセス>";

        if($nurse_type == "3" || $level == "3"){
    		$title_nm .= "評価1.同僚評価者";
    	}else{
	        $title_nm .= "評価3.同僚評価者１";
    	}
        break;

    case "cl_grader3_2":
        $title_nm = "<ナーシングプロセス>";
        $title_nm .= "評価3.同僚評価者２";
        break;
    	//----------- 2011.12.01 Add by matsuura end ------------

    case "cl_grader4":
        $title_nm = "<教育/自己学習能力>";
        $title_nm .= "評価1.2.3.4. 当該師長";
        break;

    case "cl_grader5":
        $title_nm = "<リーダーシップ能力>";
        // 2011.12.01 Add by matsuura
        if($nurse_type == "3"){
 	       $title_nm .= "評価1.上司評価者１";
        }else{
	        $title_nm .= "評価1.上司評価者";
        }
        break;

        // 2011.12.01 Add by matsuura
    case "cl_grader5_2":
        $title_nm = "<リーダーシップ能力>";
        if($nurse_type == "3"){
 	       $title_nm .= "評価1.上司評価者２";
        }else{
	        $title_nm .= "評価2.上司評価者";
        }
        break;

    case "cl_grader6":
        $title_nm = "<経験年数/経験領域>";
        $title_nm .= "評価1.2.3. 当該師長";
        break;

        // 2011.12.01 Add by matsuura
    case "cl_grader6_2":
        $title_nm = "<経験年数/経験領域>";
        $title_nm .= "評価4. 当該師長";
        break;

}

$cl_title = cl_title_name();

//==============================
// HTML
//==============================
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$cl_title?> | 評価者選択</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script src="js/prototype/dist/prototype.js"></script>
<script language="javascript">


// コピー処理
function copy_emp(emp_id, emp_nm)
{
    if(window.opener && !window.opener.closed && window.opener.add_target_list)
    {
          window.opener.add_target_list('<?=$input_div?>', emp_id, emp_nm);
    }
}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#35B341 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#35B341">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b><?=htmlspecialchars($title_nm);?></b></font></td>
<td>&nbsp</td>
<td width="10">&nbsp</td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" width="10" height="10" alt=""><br>

<form action="cl_grader_list.php" method="get">
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#DFFFDC">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
職員名：<input type="text" name="srch_emp_nm" value="<?=$srch_emp_nm?>" size="30" style="ime-mode:active;">
<input type="submit" value="検索">
</font></td>
</tr>
</table>
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="input_div" value="<?=$input_div?>">
<input type="hidden" name="level" value="<?=$level?>">

<? /* 2011.12.19 Add by matsuura */ ?>
<input type="hidden" name="nurse_type" value="<?=$nurse_type?>">

</form>
<img src="img/spacer.gif" width="10" height="4" alt=""><br>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
<tr>
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td>

<!-- ここから -->
<?

if(count($arr_emp_list) > 0)
{
?>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#DFFFDC">
<td width="50"></td>
<td width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員氏名</font></td>
<td width="*"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属</font></td>
<td width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">役職</font></td>
</tr>
<?
foreach($arr_emp_list as $emp_info) {

$emp_full_nm = $emp_info["emp_lt_nm"]. " ".$emp_info["emp_ft_nm"];

$class_nm = $emp_info["class_nm"]." > ".$emp_info["atrb_nm"]." > ".$emp_info["dept_nm"];
if($emp_info["room_nm"] != "")
{
    $class_nm .= " > " . $emp_info["room_nm"];
}

?>
<tr height="22" bgcolor="#FFFFFF">
<td align="center"><input type="button" value="コピー" onclick="copy_emp('<?=$emp_info["emp_id"]?>','<?=$emp_full_nm?>');"></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=htmlspecialchars($emp_full_nm)?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=htmlspecialchars($class_nm)?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=htmlspecialchars($emp_info["st_nm"])?></font></td>
</tr>
<?
}
?>
</table>
<?
}
else
{
?>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr bgcolor="#F5FFE5"><td><b>該当する職員はいませんでした</b></td></tr>
</table>
<?
}
?>

<!-- ここまで -->

</td>
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>

</body>
</html>





<?
//==============================
//データベース接続を閉じる
//==============================
pg_close($con);
?>
