<?
require("about_session.php");
require("about_authority.php");
require("get_values.ini");
require_once("Cmx.php");
require_once("aclg_set.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 施設・設備権限チェック
$checkauth = check_authority($session, 41, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

$emp_id = get_emp_id($con, $session, $fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// 入力チェック
if ($cate_name == "") {
	echo("<script type=\"text/javascript\">alert('カテゴリ名を入力してください。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if (strlen($cate_name) > 100) {
	echo("<script type=\"text/javascript\">alert('カテゴリ名が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
$chart_start = "$chart_start_hour$chart_start_min";
$chart_end = "$chart_end_hour$chart_end_min";
if ($chart_start >= $chart_end) {
	echo("<script type=\"text/javascript\">alert('タイムチャート表示時間が不正です。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
// 予約開始日妥当性チェック
$start_date = $start_date1.$start_date2.$start_date3;
$start_date = str_replace("-", "", $start_date);
if ($start_date != "" &&
	!checkdate($start_date2, $start_date3, $start_date1)) {
	echo("<script language=\"javascript\">alert(\"予約開始日が正しくありません\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}

// 日付指定の場合
if ($end_date_flg == "1") {
	// 予約期限日妥当性チェック
	$end_date = $end_date1.$end_date2.$end_date3;
	$end_date = str_replace("-", "", $end_date);
	if ($end_date != "" &&
		!checkdate($end_date2, $end_date3, $end_date1)) {
		echo("<script language=\"javascript\">alert(\"予約期限日が正しくありません\");</script>\n");
		echo("<script language=\"javascript\">history.back();</script>\n");
		exit;
	}
	// 開始日、期限日の関連チェック
	if ($start_date != "" && $end_date != "" && $start_date > $end_date) {
		echo("<script language=\"javascript\">alert(\"予約開始日は予約期限日以前にしてください\");</script>\n");
		echo("<script language=\"javascript\">history.back();</script>\n");
	}

	// 月をクリア
	$end_month = "";
} else {
// 期間指定の場合
	// 月が未入力の場合
	if ($end_month == "-") {
		echo("<script language=\"javascript\">alert(\"期間指定の場合は月数を指定してください\");</script>\n");
		echo("<script language=\"javascript\">history.back();</script>\n");
		exit;
	}
	// 予約期限日をクリア
	$end_date = "";
}

// 登録値の編集
if ($pt_flg != "t") {$pt_flg = "f";}

// トランザクションの開始
pg_query($con, "begin transaction");

// カテゴリIDの採番
$sql = "select max(fclcate_id) from fclcate";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$cate_id = intval(pg_fetch_result($sel, 0, 0)) + 1;

// 表示順の決定
$sql = "select max(order_no) from fclcate";
$cond = "where not fclcate_del_flg";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$order_no = intval(pg_fetch_result($sel, 0, 0)) + 1;

// カテゴリレコードを作成
$sql = "insert into fclcate (fclcate_id, fclcate_name, base_min, pt_flg, time_chart_start, time_chart_end, bg_color, show_content, auth_flg, has_admin_flg, ref_dept_st_flg, ref_dept_flg, ref_st_flg, upd_dept_st_flg, upd_dept_flg, upd_st_flg, start_date, end_date_flg, end_date, end_month, double_flg, order_no) values (";
$content = array($cate_id, $cate_name, $base_min, $pt_flg, $chart_start, $chart_end, $bg_color, $show_content, $auth_flg, $has_admin_flg, "f", "1", "1", "f", "1", "1", $start_date, $end_date_flg, $end_date, $end_month, $double_flg, $order_no);
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 登録者を施設・設備管理者として登録
if ($has_admin_flg == "t") {
	$sql = "insert into fclcateadmin (fclcate_id, emp_id) values (";
	$content = array($cate_id, $emp_id);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// トランザクションのコミット
pg_query($con, "commit");

// データベース接続を切断
pg_close($con);

// 画面遷移
echo("<script type=\"text/javascript\">location.href = 'facility_master_menu.php?session=$session';</script>");
?>
