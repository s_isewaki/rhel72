<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 病床管理｜転床患者一覧</title>
<?
define("MAX_ROW", 20);

require("about_session.php");
require("about_authority.php");
require("show_select_values.ini");
require("get_values.ini");
require("inpatient_list_common.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 病床管理権限チェック
$wardreg = check_authority($session,14,$fname);
if($wardreg == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 診療科登録権限を取得
$section_admin_auth = check_authority($session, 28, $fname);

// 病棟登録権限を取得
$ward_admin_auth = check_authority($session, 21, $fname);

// データベースに接続
$con = connect2db($fname);

// 病棟一覧を取得
$sql = "select bldg_cd, ward_cd, ward_name from wdmst";
$cond = "where ward_del_flg = 'f' order by bldg_cd, ward_cd";
$sel_ward = select_from_table($con, $sql, $cond, $fname);
if ($sel_ward == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// デフォルトの並び順を氏名の昇順とする
if ($order == "") {
	$order = "1";
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function initPage() {
	setListDateDisabled();
}

<? show_inpatient_list_common_javascript(); ?>

function downloadCSV() {
	document.csv.submit();
}

function cancelMove(ptif_id) {
	if (!confirm('取消します。よろしいですか？')) {
		return;
	}
	document.cancel.ptif_id.value = ptif_id;
	document.cancel.submit();
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
form {margin:0;}
table.submenu {border-collapse:collapse;}
table.submenu td {border:#5279a5 solid 1px;}
table.submenu td td {border-width:0;}
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="bed_menu.php?session=<? echo($session); ?>"><img src="img/icon/b17.gif" width="32" height="32" border="0" alt="病床管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="bed_menu.php?session=<? echo($session); ?>"><b>病床管理</b></a></font></td>
<? if ($section_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="entity_menu.php?session=<? echo($session); ?>&entity=1"><b>管理画面へ</b></a></font></td>
<? } else if ($ward_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="building_list.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="115" align="center" bgcolor="#bdd1e7"><a href="bed_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床管理トップ</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="ward_reference.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病棟照会</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#5279a5"><a href="inpatient_reserve_register_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>入退院登録</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="bedmake_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ベットメイク</font></a></td>
<td width="5">&nbsp;</td>
<td width="125" align="center" bgcolor="#bdd1e7"><a href="bed_rate_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床利用率管理</font></a>
</td>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#bdd1e7"><a href="bed_date_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">在院日数管理</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="bed_patient_export.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者データ抽出</font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="submenu">
<tr>
<td width="70%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>入院予定〜退院登録</b></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>検索条件設定</b></font></td>
</tr>
<tr>
<td valign="middle">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr height="22">
<td align="center"><form action="inpatient_vacant_search.php" method="get"><input type="submit" value="空床検索"><input type="hidden" name="session" value="<? echo($session); ?>"></form><br><form action="inpatient_waiting_list.php" method="get"><input type="submit" value="待機患者"><input type="hidden" name="session" value="<? echo($session); ?>"></form></td>
<td align="center"><img src="img/right.gif" alt="" width="17" height="17"></td>
<td align="center"><form action="inpatient_reserve_register_list.php" method="get"><input type="submit" value="入院予定"><input type="hidden" name="session" value="<? echo($session); ?>"><input type="hidden" name="mode" value="search"></form></td>
<td align="center"><img src="img/right.gif" alt="" width="17" height="17"></td>
<td align="center"><form action="inpatient_register_list.php" method="get"><input type="submit" value="入院"><input type="hidden" name="session" value="<? echo($session); ?>"></form></td>
<td align="center"><img src="img/right.gif" alt="" width="17" height="17"></td>
<td align="center"><form action="inpatient_go_out_register_list.php" method="get"><input type="submit" value="外出・外泊"><input type="hidden" name="session" value="<? echo($session); ?>"></form></td>
<td align="center"><img src="img/right.gif" alt="" width="17" height="17"></td>
<td align="center"><form action="inpatient_move_register_list.php" method="get"><input type="submit" value="転棟"><input type="hidden" name="session" value="<? echo($session); ?>"></form></td>
<td align="center"><img src="img/right.gif" alt="" width="17" height="17"></td>
<td align="center"><form action="out_inpatient_reserve_register_list.php" method="get">
<input type="submit" value="退院予定"><input type="hidden" name="session" value="<? echo($session); ?>"></form></td>
<td align="center"><img src="img/right.gif" alt="" width="17" height="17"></td>
<td align="center"><form action="out_inpatient_register_list.php" method="get"><input type="submit" value="退院"><input type="hidden" name="session" value="<? echo($session); ?>"></form></td>
</tr>
</table>
</td>
<td valign="middle">
<? show_inpatient_list_form($con, $sel_ward, "E", $bldgwd, $list_sect_id, $list_year, $list_month, $list_day, $list_year2, $list_month2, $list_day2, $session, $fname); ?>
</td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<form name="pt" action="inpatient_delete.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr valign="bottom">
<td align="right"><input type="button" value="CSV出力" onclick="downloadCSV();"></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr><td height="2"></td></tr>
<tr bgcolor="#5279a5"><td height="1"></td></tr>
<tr><td height="2"></td></tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<? show_next($con, $session, $bldgwd, $page, $fname, $order, $list_sect_id, $list_year, $list_month, $list_day, $list_year2, $list_month2, $list_day2); ?>
<? show_patient_list($con, $session, $bldgwd, $page, $fname, $order, $list_sect_id, $list_year, $list_month, $list_day, $list_year2, $list_month2, $list_day2); ?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="bldgwd" value="<? echo($bldgwd); ?>">
<input type="hidden" name="list_sect_id" value="<? echo($list_sect_id); ?>">
<input type="hidden" name="page" value="<? echo($page); ?>">
<input type="hidden" name="order" value="<? echo($order); ?>">
<input type="hidden" name="list_year" value="<? echo($list_year); ?>">
<input type="hidden" name="list_month" value="<? echo($list_month); ?>">
<input type="hidden" name="list_day" value="<? echo($list_day); ?>">
<input type="hidden" name="list_year2" value="<? echo($list_year2); ?>">
<input type="hidden" name="list_month2" value="<? echo($list_month2); ?>">
<input type="hidden" name="list_day2" value="<? echo($list_day2); ?>">
</form>
<form name="csv" action="inpatient_list_csv.php" method="get" target="download">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="bldgwd" value="<? echo($bldgwd); ?>">
<input type="hidden" name="list_sect_id" value="<? echo($list_sect_id); ?>">
<input type="hidden" name="order" value="<? echo($order); ?>">
<input type="hidden" name="list_year" value="<? echo($list_year); ?>">
<input type="hidden" name="list_month" value="<? echo($list_month); ?>">
<input type="hidden" name="list_day" value="<? echo($list_day); ?>">
<input type="hidden" name="list_year2" value="<? echo($list_year2); ?>">
<input type="hidden" name="list_month2" value="<? echo($list_month2); ?>">
<input type="hidden" name="list_day2" value="<? echo($list_day2); ?>">
<input type="hidden" name="type" value="13">
</form>
<iframe name="download" width="0" height="0" frameborder="0"></iframe>
<form name="cancel" action="inpatient_move_cancel.php" method="post">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="bldgwd" value="<? echo($bldgwd); ?>">
<input type="hidden" name="list_sect_id" value="<? echo($list_sect_id); ?>">
<input type="hidden" name="order" value="<? echo($order); ?>">
<input type="hidden" name="list_year" value="<? echo($list_year); ?>">
<input type="hidden" name="list_month" value="<? echo($list_month); ?>">
<input type="hidden" name="list_day" value="<? echo($list_day); ?>">
<input type="hidden" name="list_year2" value="<? echo($list_year2); ?>">
<input type="hidden" name="list_month2" value="<? echo($list_month2); ?>">
<input type="hidden" name="list_day2" value="<? echo($list_day2); ?>">
<input type="hidden" name="ptif_id" value="">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
function show_patient_list($con, $session, $bldgwd, $page, $fname, $order, $list_sect_id, $list_year, $list_month, $list_day, $list_year2, $list_month2, $list_day2) {
	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"list\">");

	// テーブルヘッダの出力
	echo("<tr height='22' bgcolor='#f6f9ff'>\n");
	echo("<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>患者ID</font></td>\n");
	echo("<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>");
	if ($order == "1") {
		echo("<a href=\"$fname?session=$session&order=2&bldgwd=$bldgwd&list_sect_id=$list_sect_id&list_year=$list_year&list_month=$list_month&list_day=$list_day&list_year2=$list_year2&list_month2=$list_month2&list_day2=$list_day2\">");
	} else {
		echo("<a href=\"$fname?session=$session&order=1&bldgwd=$bldgwd&list_sect_id=$list_sect_id&list_year=$list_year&list_month=$list_month&list_day=$list_day&list_year2=$list_year2&list_month2=$list_month2&list_day2=$list_day2\">");
	}
	echo("患者氏名</a>");
	echo("</font></td>\n");
	echo("<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>年齢</font></td>\n");
	echo("<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>診療科</font></td>\n");
	echo("<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>病棟</font></td>\n");
	echo("<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>");
	if ($order == "3") {
		echo("<a href=\"$fname?session=$session&order=4&bldgwd=$bldgwd&list_sect_id=$list_sect_id&list_year=$list_year&list_month=$list_month&list_day=$list_day&list_year2=$list_year2&list_month2=$list_month2&list_day2=$list_day2\">");
	} else {
		echo("<a href=\"$fname?session=$session&order=3&bldgwd=$bldgwd&list_sect_id=$list_sect_id&list_year=$list_year&list_month=$list_month&list_day=$list_day&list_year2=$list_year2&list_month2=$list_month2&list_day2=$list_day2\">");
	}
	echo("病室");
	echo("</font></td>\n");
	echo("<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>詳細</font></td>\n");
	echo("<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>取消</font></td>\n");
	echo("</tr>\n");

	// 入院患者一覧の取得
	$sql = "select inptmst.ptif_id, inptmst.inpt_lt_kj_nm, inptmst.inpt_ft_kj_nm, ptifmst.ptif_birth, sectmst.sect_nm, bldgmst.bldg_name, wdmst.ward_name, ptrmmst.ptrm_name from inptmst inner join ptifmst on ptifmst.ptif_id = inptmst.ptif_id inner join sectmst on sectmst.enti_id = inptmst.inpt_enti_id and sectmst.sect_id = inptmst.inpt_sect_id inner join bldgmst on bldgmst.bldg_cd = inptmst.bldg_cd inner join wdmst on wdmst.bldg_cd = inptmst.bldg_cd and wdmst.ward_cd = inptmst.ward_cd inner join ptrmmst on ptrmmst.bldg_cd = inptmst.bldg_cd and ptrmmst.ward_cd = inptmst.ward_cd and ptrmmst.ptrm_room_no = inptmst.ptrm_room_no";
	$cond = "where exists (select * from inptmove where inptmove.ptif_id = inptmst.ptif_id and inptmove.move_cfm_flg = 't' and inptmove.move_del_flg = 'f' and inptmove.move_dt between '$list_year$list_month$list_day' and '$list_year2$list_month2$list_day2')";
	if ($bldgwd != "") {
		list($bldg_cd, $ward_cd) = split("-", $bldgwd);
		$cond .= " and inptmst.bldg_cd = $bldg_cd and inptmst.ward_cd = $ward_cd";
	}
	if ($list_sect_id != "") {
		$cond .= " and inptmst.inpt_sect_id = $list_sect_id";
	}
	switch ($order) {
	case "1":
		$cond .= " order by inptmst.inpt_keywd";
		break;
	case "2":
		$cond .= " order by inptmst.inpt_keywd desc";
		break;
	case "3":
		$cond .= " order by ptrmmst.ptrm_name";
		break;
	case "4":
		$cond .= " order by ptrmmst.ptrm_name desc";
		break;
	}
	if ($page == "0" || $page == "") {
		$cond .= " limit " . MAX_ROW;
	}else{
		$offset = $page * MAX_ROW;
		$cond .= " limit " . MAX_ROW . " offset $offset";
	}
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

	// 転床患者一覧をループ
	while ($row = pg_fetch_array($sel)) {

		// 転床日時の取得
		$sql = "select inptmove.move_dt, inptmove.move_tm from inptmove";
		$cond = "where inptmove.ptif_id = '{$row["ptif_id"]}' and inptmove.move_cfm_flg = 't' and inptmove.move_del_flg = 'f' and inptmove.move_dt between '$list_year$list_month$list_day' and '$list_year2$list_month2$list_day2' order by inptmove.move_dt desc, inptmove.move_tm desc";
		$sel_move = select_from_table($con, $sql, $cond, $fname);
		if ($sel_move == 0) {
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$tmp_move_dt = pg_fetch_result($sel_move, 0, "move_dt");
		$tmp_move_tm = pg_fetch_result($sel_move, 0, "move_tm");

		// 入院患者情報を出力
		echo("<tr height=\"22\">\n");
		echo("<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>{$row["ptif_id"]}</font></td>\n");
		echo("<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>{$row["inpt_lt_kj_nm"]} {$row["inpt_ft_kj_nm"]}</font></td>\n");
		echo("<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>" . get_age($row["ptif_birth"]) . "歳</font></td>\n");
		echo("<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>{$row["sect_nm"]}</font></td>\n");
		echo("<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>{$row["bldg_name"]}{$row["ward_name"]}</font></td>\n");
		echo("<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>{$row["ptrm_name"]}</font></td>\n");
		echo("<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><a href=\"javascript:void(0);\" onclick=\"window.open('inpatient_move_detail.php?session=$session&pt_id={$row["ptif_id"]}&date=$tmp_move_dt&time=$tmp_move_tm', 'rmdtlsub', 'width=640,height=480,scrollbars=yes');\">詳細</a></font></td>\n");
		echo("<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><a href=\"javascript:cancelMove('{$row["ptif_id"]}');\">取消</a></font></td>\n");
	    echo("</tr>\n");
	}

	echo("</table>\n");
}

function show_next($con, $session, $bldgwd, $page, $fname, $order, $list_sect_id, $list_year, $list_month, $list_day, $list_year2, $list_month2, $list_day2) {

	// 転床患者件数の取得
	$sql = "select count(*) from inptmst";
	$cond = "where exists (select * from inptmove where inptmove.ptif_id = inptmst.ptif_id and inptmove.move_cfm_flg = 't' and inptmove.move_del_flg = 'f' and inptmove.move_dt between '$list_year$list_month$list_day' and '$list_year2$list_month2$list_day2')";
	if ($bldgwd != "") {
		list($bldg_cd, $ward_cd) = split("-", $bldgwd);
		$cond .= " and inptmst.bldg_cd = $bldg_cd and inptmst.ward_cd = $ward_cd";
	}
	if ($list_sect_id != "") {
		$cond .= " and inptmst.inpt_sect_id = $list_sect_id";
	}
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

	// ページング処理不要なら何もせず復帰
	$rec_count = intval(pg_fetch_result($sel, 0, 0));
	$total_page = ceil($rec_count / MAX_ROW);
	if ($total_page <= 1) {
		return;
	}

	echo("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n");
	echo("<tr>\n");
	echo("<td>\n");
	echo("<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">\n");
	echo("<tr>\n");
	echo("<td width=\"20%\"> </td>\n");
	echo("<td width=\"5%\" align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j18\">");
	if ($page > 0) {
		$back = $page - 1;
		echo("<a href=\"$fname?session=$session&order=$order&bldgwd=$bldgwd&list_sect_id=$list_sect_id&list_year=$list_year&list_month=$list_month&list_day=$list_day&list_year2=$list_year2&list_month2=$list_month2&list_day2=$list_day2&page=$back\">←</a>");
	}
	echo("</font></td>\n");
	echo("<td width=\"50%\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j18\">");
	for ($i = 0; $i < $total_page; $i++) {
		$j = $i + 1;
		if ($page != $i) {
			echo("<a href=\"$fname?session=$session&order=$order&bldgwd=$bldgwd&list_sect_id=$list_sect_id&list_year=$list_year&list_month=$list_month&list_day=$list_day&list_year2=$list_year2&list_month2=$list_month2&list_day2=$list_day2&page=$i\">$j</a> ");
		} else {
			echo("[".$j."] ");
		}
	}
	echo("</font></td>\n");
	echo("<td width=\"5%\" align=\"left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j18\">");
	if ($page < $total_page - 1) {
		$next = $page + 1;
		echo("<a href=\"$fname?session=$session&order=$order&bldgwd=$bldgwd&list_sect_id=$list_sect_id&list_year=$list_year&list_month=$list_month&list_day=$list_day&list_year2=$list_year2&list_month2=$list_month2&list_day2=$list_day2&page=$next\">→</a>");
	}
	echo("</font></td>\n");
	echo("<td width=\"20%\"> </td>\n");
	echo("<tr>\n");
  	echo("</table>\n");
	echo("</td>\n");
	echo("</tr>\n");
	echo("</table>\n");
}

function get_age($birth) {
	$yr = date("Y");
	$md = date("md");
	$birth_yr = substr($birth, 0, 4);
	$birth_md = substr($birth, 4, 8);
	$age = $yr - $birth_yr;
	if ($md < $birth_md) {
		$age = $age - 1;
	}
	return $age;
}
?>
