<?
require("about_session.php");
require("about_authority.php");
require("get_values.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
if ($kind_flg == "1") {
	$auth_id = 57;
} else {
	$auth_id = 59;
}
$checkauth = check_authority($session, $auth_id, $fname);
if (!@$checkauth) {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if ($fixedform_sentence == "") {
	echo("<script type=\"text/javascript\">alert('定型文を入力してください。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

$sql =
	" update fixedform set".
	" fixedform_sentence = '".pg_escape_string($fixedform_sentence)."'".
	",sect_id = ". ((int)@$sel_sect_id ? (int)@$sel_sect_id : "null").
	" where fixedform_id = ".(int)$fixedform_id;
$ret = update_set_table($con, $sql, array(), null, "", $fname);

// データベース接続を閉じる
pg_close($con);

// 呼び出し元画面を再表示
if ($kind_flg == "1") {
	echo("<script type=\"text/javascript\">opener.location.reload();window.close();</script>");
} else {
	echo("<script type=\"text/javascript\">if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();} window.close();</script>");
}
?>
