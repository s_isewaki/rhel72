<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?

//ファイルの読み込み
require("about_postgres.php");
require("about_session.php");
require("about_authority.php");
require("about_validation.php");
require("./conf/sql.inf");

//ページ名
$fname = $PHP_SELF;

//セッションチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//病棟情報権限チェック
$ward = check_authority($session,21,$fname);
if($ward == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//----------正規表現チェック----------
for($i=0;$i<count($item_no);$i++){

	if(strlen($item_no[$i][1]) > 200){
		echo("<script language=\"javascript\">alert(\"備考項目の内容が長すぎます。\");</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}
	
}

//**********DB処理**********
//----------ＤＢのコネクション作成----------
$con = connect2db($fname);

//----------Transaction begin----------
pg_exec($con,"begin transaction");

$cond = "where bldg_cd = '$bldg_cd' and ward_cd = '$ward_cd' and ptrm_room_no = '$rm_no'";
$del = delete_from_table($con,$SQL78,$cond,$fname);
	if($del==0){
		pg_exec($con,"rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	
for($i=0;$i<count($item_no);$i++){
	if($item_no[$i][0] != ""){
		$content = array($bldg_cd,$ward_cd,$rm_no,$item_no[$i][0],$item_no[$i][1]);
		$in_eqp = insert_into_table($con,$SQL77,$content,$fname);
		if($in_eqp==0){
			pg_exec($con,"rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}
}
	pg_exec($con, "commit");
	pg_close($con);
	echo("<script language=\"javascript\">location.href=\"room_list.php?session=$session&bldg_cd=$bldg_cd&ward_cd=$ward_cd\"</script>");
?>
