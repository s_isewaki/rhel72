<!-- ************************************************************************ -->
<!-- 勤務シフト作成｜必要人数設定(カレンダー)「登録」-->
<!-- ************************************************************************ -->

<?
require_once("about_session.php");
require_once("about_authority.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
// セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
///-----------------------------------------------------------------------------
// トランザクションを開始
///-----------------------------------------------------------------------------
pg_query($con, "begin transaction");

///-----------------------------------------------------------------------------
//日編集
///-----------------------------------------------------------------------------
$start_date = $duty_date[1];
$end_date = $duty_date[$day_cnt];
///-----------------------------------------------------------------------------
// 指定ＩＤのレコードを全削除
///-----------------------------------------------------------------------------
//職種
$sql = "delete from duty_shift_need_cnt_calendar_job";
$cond = "where group_id = '$group_id' ";
$cond .= "and duty_yyyy = $duty_yyyy ";
$cond .= "and duty_mm = $duty_mm ";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
//データ
$sql = "delete from duty_shift_need_cnt_calendar";
$cond = "where group_id = '$group_id' ";
$cond .= "and duty_date >= '$start_date' and duty_date <= '$end_date' ";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
// 必要人数(カレンダー)情報を登録
///-----------------------------------------------------------------------------
for($i=0; $i<$data_cnt; $i++) {
	///-----------------------------------------------------------------------------
	//職種
	///-----------------------------------------------------------------------------
	//ＳＱＬ
	$sql = "insert into duty_shift_need_cnt_calendar_job (";
	$sql .= "group_id, atdptn_ptn_id, job_id, job_id2, job_id3, duty_yyyy, duty_mm, no, team, sex";
	$sql .= ") values (";
	if ($job_id2[$i] == "") {
		$job_id2[$i]= null;
	}
	if ($job_id3[$i] == "") {
		$job_id3[$i]= null;
	}
	if ($team[$i] == "") {
		$team[$i] = 0;
	}
	$content = array(
			$group_id,
			$atdptn_ptn_id[$i],
			$job_id[$i],
			$job_id2[$i],
			$job_id3[$i],
			$duty_yyyy,
			$duty_mm,
			$i+1,
			$team[$i],
			$sex[$i]
			);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	///-----------------------------------------------------------------------------
	//データ
	///-----------------------------------------------------------------------------
	for($k=1; $k<=$day_cnt; $k++) {
		if ($need_cnt[$k][$i] != "") {
			//ＳＱＬ
			$sql = "insert into duty_shift_need_cnt_calendar (";
			$sql .= "group_id, atdptn_ptn_id, job_id, job_id2, job_id3, duty_date, need_cnt, team, sex";
			$sql .= ") values (";
			if ($job_id2[$i] == "") {
				$job_id2[$i]= null;
			}
			if ($job_id3[$i] == "") {
				$job_id3[$i]= null;
			}
			$content = array(
					$group_id,
					$atdptn_ptn_id[$i],
					$job_id[$i],
					$job_id2[$i],
					$job_id3[$i],
					$duty_date[$k],
					pg_escape_string($need_cnt[$k][$i]),
					$team[$i],
					$sex[$i]
					);
			$ins = insert_into_table($con, $sql, $content, $fname);
			if ($ins == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}
}
///-----------------------------------------------------------------------------
// トランザクションをコミット
///-----------------------------------------------------------------------------
pg_query("commit");
///-----------------------------------------------------------------------------
// データベース接続を閉じる
///-----------------------------------------------------------------------------
pg_close($con);
///-----------------------------------------------------------------------------
// 遷移元画面を再表示
///-----------------------------------------------------------------------------
echo("<script type=\"text/javascript\">location.href = 'duty_shift_need_cnt_calendar.php?session=$session&group_id=$group_id&duty_yyyy=$duty_yyyy&duty_mm=$duty_mm&fullscreen_flg=$fullscreen_flg';</script>");
?>