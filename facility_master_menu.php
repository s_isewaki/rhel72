<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix マスターメンテナンス | 施設・設備 | 施設・設備一覧</title>
<?
require_once("Cmx.php");
require_once("aclg_set.php");

require_once("about_session.php");
require_once("about_authority.php");
require_once("show_facility_list.ini");
require_once("show_facility.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 41, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);
// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// カテゴリ一覧の取得
$sql = "select fclcate_id, fclcate_name, auth_flg, has_admin_flg from fclcate";
$cond = "where fclcate_del_flg = 'f' order by order_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$categories = array();
while ($row = pg_fetch_array($sel)) {
	$categories[$row["fclcate_id"]] = array(
		"name" => $row["fclcate_name"],
		"auth_flg" => $row["auth_flg"],
		"admin_flg" => $row["has_admin_flg"],
		"facilities" => array()
	);
}

// オプション情報を取得
$default_info = get_default_info($con, $session, $fname);
$default_url = $default_info["url"];
?>
<script type="text/javascript" src="js/jquery/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="js/jquery/jquery-ui-1.8.21.core.min.js"></script>
<script type="text/javascript" src="js/jquery/jquery-ui-1.8.21.widget.min.js"></script>
<script type="text/javascript" src="js/jquery/jquery-ui-1.8.21.mouse.min.js"></script>
<script type="text/javascript" src="js/jquery/jquery-ui-1.8.21.sortable.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	function sortableHelper(e, tr) {
		var $originals = tr.children();
		var $helper = tr.clone();
		$helper.children().each(function(index) {
			$(this).width($originals.eq(index).width());
		});
		return $helper;
	}

	$('#container > tbody').sortable({
		items: '> tr',
		handle: '.category',
		axis: 'y',
		cancel: 'input,img,a',
		placeholder: 'dragging',
		opacity: 0.4,
		helper: sortableHelper,
		start: function(e, tr) {
			$('.dragging').html("<td>&nbsp;</td>").height(70);
		}
	});

<? foreach ($categories as $tmp_cate_id => $tmp_category) { ?>

	$('#cate<? echo($tmp_cate_id); ?>_facilities > tbody').sortable({
		items: "> tr[class!='category']",
		axis: 'y',
		cancel: 'input,a',
		placeholder: 'dragging',
		opacity: 0.4,
		helper: sortableHelper,
		start: function(e, tr) {
			$('.dragging').html("<td colspan='5'>&nbsp;</td>").height(40);
		}
	});

<? } ?>

});

function controlList(img, blockID) {
	if (img.className == 'none') {
		return;
	}

	var trDisplay;
	if (img.className == 'close') {
		img.className = 'open';
		trDisplay = '';
	} else {
		img.className = 'close';
		trDisplay = 'none';
	}

	var trs = document.getElementsByTagName('tr');
	for (var i = 0, j = trs.length; i < j; i++) {
		if (trs[i].className == blockID) {
			trs[i].style.display = trDisplay;
		}
	}
}

function deleteFacility() {
	if (document.delform.elements['trashbox[]'] == undefined) {
		alert('削除可能なデータが存在しません');
		return;
	}

	if (document.delform.elements['trashbox[]'].length == undefined) {
		if (!document.delform.elements['trashbox[]'].checked) {
			alert('チェックボックスをオンにしてください。');
			return;
		}
	} else {
		var checked = false;
		for (var i = 0, j = document.delform.elements['trashbox[]'].length; i < j; i++) {
			if (document.delform.elements['trashbox[]'][i].checked) {
				checked = true;
				break;
			}
		}
		if (!checked) {
			alert('チェックボックスをオンにしてください。');
			return;
		}
	}

	if (!confirm("削除してよろしいですか？")) return;

	document.delform.opens.value = getOpens();
	document.delform.submit();
}

function saveOrder() {
	var buf = [$('#container > tbody').sortable('serialize')];
<? foreach ($categories as $tmp_cate_id => $tmp_category) { ?>
	buf.push($('#cate<? echo($tmp_cate_id); ?>_facilities > tbody').sortable('serialize'));
<? } ?>
	buf = buf.join('&');
	buf = buf.split('&');
	for (var i = 0, len = buf.length; i < len; i++) {
		var pair = buf[i].split('=');
		var elm = document.createElement('input');
		elm.type = 'hidden';
		elm.name = pair[0];
		elm.value = pair[1];
		document.orderform.appendChild(elm);
	}

	document.orderform.opens.value = getOpens();
	document.orderform.submit();
}

function getOpens() {
	var opens = [];
	var imgs = document.getElementsByTagName('img');
	for (var i = 0, len = imgs.length; i < len; i++) {
		var img = imgs[i];
		var id = img.id.toString();
		if (img.className !== 'open' || id.indexOf('img-') !== 0) continue;
		opens.push(id.substr(4));
	}
	return opens.join(',');
}
</script>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;border:#5279a5 solid 1px;}
.list td {border:#5279a5 solid 1px;}
img.close {background-image:url("img/icon/plus.gif");}
img.open {background-image:url("img/icon/minus.gif");}
.dragging {background-color:#ffff99;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<? echo($default_url); ?>"><img src="img/icon/b06.gif" width="32" height="32" border="0" alt="設備予約"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<? echo($default_url); ?>"><b>設備予約</b></a> &gt; <a href="facility_master_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="<? echo($default_url); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="120" height="22" align="center" bgcolor="#5279a5"><a href="facility_master_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>施設・設備一覧</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="facility_category_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カテゴリ登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="facility_master_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">施設・設備登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="facility_master_type.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">種別</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="facility_admin_bulk_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="facility_tmpl_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">テンプレート一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="facility_tmpl_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">テンプレート登録</font></a></td>
<td align="right"><input type="button" value="削除" onclick="deleteFacility();"></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form name="orderform" action="facility_master_save_order.php" method="post">
<table border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td><input type="button" value="表示順更新" onclick="saveOrder();"></td>
<td><font size="2" face="ＭＳ Ｐゴシック, Osaka" class="j10">※一覧の行をドラッグ＆ドロップすると表示順の入れ替えができます。入れ替え後には「表示順更新」ボタンをクリックして下さい。</font></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="opens" value="">
</form>
<form name="delform" action="facility_master_delete.php" method="post">
<table width="95%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff">
<td width="4%">&nbsp;</td>
<td width="24%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カテゴリ</font></td>
<td width="24%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">名称</font></td>
<td width="14%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限設定</font></td>
<td width="24%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">管理者設定</font></td>
</tr>
</table>
<? show_facility_list($con, $session, $categories, $opens, $fname); ?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="opens" value="">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
