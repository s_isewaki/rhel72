<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>処理中</title>
</head>
<script type="text/javascript">

//検索結果通知処理を実行します。
function exec_call_back()
{
	//呼び出し元画面に通知
	if(window.opener && !window.opener.closed && window.opener.call_back_patient_search)
	{
		var result = new Object();
		result.target_date = "<?=$target_date?>";
		result.patient_id = "<?=$patient_id?>";
		result.patient_name = "<?=$patient_name?>";
		result.sex = "<?=$sex?>";
		result.age_year = "<?=$age_year?>";
		result.age_month = "<?=$age_month?>";
		result.patient_class1 = "<?=$patient_class1?>";
		result.patient_class2 = "<?=$patient_class2?>";
		result.inpt_date = "<?=$inpt_date?>";
		window.opener.call_back_patient_search('<?=$caller?>',result);
	}
	
	//自画面を終了
	window.close();
}

</script>
</head>
<body onload="exec_call_back()">
</body>
</html>