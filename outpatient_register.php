<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<? require("./about_session.php"); ?>
<? require("./about_authority.php"); ?>
<? require("./about_postgres.php"); ?>
<? require("./show_select_values.ini"); ?>
<? require("./get_values.ini"); ?>
<? require("./conf/sql.inf"); ?>
<? require("label_by_profile_type.ini"); ?>
<?
//ページ名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//権限チェック
$outreg = check_authority($session, 35, $fname);
if ($outreg == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//--------------------------------------------------------------------

//DBへのコネクション作成
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$profile_type = get_profile_type($con, $fname);

$cond = "where ptif_id = '$pt_id' and ptif_del_flg = 'f' order by ptif_id";
$sel = select_from_table($con,$SQL85,$cond,$fname);		//ptifmstから取得

	if($sel == 0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

$lt_kn_nm = pg_result($sel,0,"ptif_lt_kana_nm");
$ft_kn_nm = pg_result($sel,0,"ptif_ft_kana_nm");
$lt_kj_nm = pg_result($sel,0,"ptif_lt_kaj_nm");
$ft_kj_nm = pg_result($sel,0,"ptif_ft_kaj_nm");
$birth    = pg_result($sel,0,"ptif_birth");

$yr = substr($birth,0,4);
$mn = substr($birth,4,2);
$dy = substr($birth,6,2);
$birth = "$yr"."年"."$mn"."月"."$dy"."日";

$sex      = pg_result($sel,0,"ptif_sex");
if($sex == 1){
	$sex = "男性";
}else{
	$sex = "女性";
}

//--------------------------------------------------------------------

// 事業所一覧の取得
$cond = "where enti_del_flg = 'f' order by enti_id";
$sel_enti = select_from_table($con, $SQL91, $cond, $fname);
if ($sel_enti == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

$arr_enti = array();
$arr_entisect = array();
$arr_sectdr = array();

// 全事業所をループ
while ($row = pg_fetch_array($sel_enti)) {
	$enti_id = $row["enti_id"];
	$enti_nm = $row["enti_nm"];
	array_push($arr_enti, array("enti_id" => $enti_id, "enti_nm" => $enti_nm));

	// 診療科一覧の取得
	$cond = "where enti_id = '$enti_id' and sect_del_flg = 'f' order by sect_id";
	$sel_sect = select_from_table($con, $SQL109, $cond, $fname);
	if ($sel_sect == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

	$arr_sect = array();

	// 全診療科をループ
	while ($row = pg_fetch_array($sel_sect)) {
		$sect_id = $row["sect_id"];
		$sect_nm = $row["sect_nm"];
		array_push($arr_sect, array("sect_id" => $sect_id, "sect_nm" => $sect_nm));

		// 主治医一覧の取得
		$cond = "where enti_id = '$enti_id' and sect_id = '$sect_id' and dr_del_flg = 'f' order by dr_id";
		$sel_dr = select_from_table($con, $SQL90, $cond, $fname);
		if ($sel_dr == 0) {
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$arr_dr = array();

		// 全主治医をループ
		while ($row = pg_fetch_array($sel_dr)) {
			$dr_id = $row["dr_id"];
			$dr_nm = $row["dr_nm"];
			array_push($arr_dr, array("dr_id" => $dr_id, "dr_nm" => $dr_nm));
		}

		array_push($arr_sectdr, array("enti_id" => $enti_id, "sect_id" => $sect_id, "drs" => $arr_dr));
	}

	array_push($arr_entisect, array("enti_id" => $enti_id, "sects" => $arr_sect));
}

/*
print_r($arr_enti);
print_r($arr_entisect);
print_r($arr_sectdr);
*/

//--------------------------------------------------------------------

// 検索フォームからの引継ぎ値をurlencode
$ekey1 = urlencode($key1);
$ekey2 = urlencode($key2);

if ($ty == "") {
	$ty = date("Y");
}
if ($tm == "") {
	$tm = date("m");
}
if ($td == "") {
	$td = date("d");
}
if ($hr == "") {
	$hr = date("H");
}
if ($mi == "") {
	$mi = date("i");
}
?>
<title><? echo($_label_by_profile["PATIENT_MANAGE"][$profile_type]); ?> | <? echo($_label_by_profile["OUT_REG"][$profile_type]); ?></title>
<script language="javascript">
function initPage() {

<? if ($enti != "") { ?>
//	document.out.enti.value = '<? echo $enti; ?>';
<? } ?>

	entiOnChange('<? echo $sect; ?>', '<? echo $doc; ?>');
}

function entiOnChange(sect, doc) {

//	var enti_id = document.out.enti.value;
	var enti_id = 1;

	// 診療科セレクトボックスのオプションを全削除
	deleteAllOptions(document.out.sect);

	// 診療科セレクトボックスのオプションを作成
<? foreach ($arr_entisect as $entisect) { ?>
	if (enti_id == '<? echo $entisect["enti_id"]; ?>') {
	<? foreach($entisect["sects"] as $sectitem) { ?>
		addOption(document.out.sect, '<? echo $sectitem["sect_id"]; ?>', '<? echo $sectitem["sect_nm"]; ?>', sect);
	<? } ?>
	}
<? } ?>

	// オプションが1件も作成されなかった場合
	if (document.out.sect.options.length == 0) {
		addOption(document.out.sect, '0', '（未登録）', sect);
	}

	sectOnChange(doc);
}

function sectOnChange(doc) {

//	var enti_id = document.out.enti.value;
	var enti_id = 1;
	var sect_id = document.out.sect.value;

	// 主治医セレクトボックスのオプションを全削除
	deleteAllOptions(document.out.doc);

	// 主治医セレクトボックスのオプションを作成
<? foreach ($arr_sectdr as $sectdr) { ?>
	if (enti_id == '<? echo $sectdr["enti_id"]; ?>' && sect_id == '<? echo $sectdr["sect_id"]; ?>') {
	<? foreach($sectdr["drs"] as $dritem) { ?>
		addOption(document.out.doc, '<? echo $dritem["dr_id"]; ?>', '<? echo $dritem["dr_nm"]; ?>', doc);
	<? } ?>
	}
<? } ?>

	// オプションが1件も作成されなかった場合
	if (document.out.doc.options.length == 0) {
		addOption(document.out.doc, '0', '（未登録）', doc);
	}

}

function deleteAllOptions(box) {

	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}

}

function addOption(box, value, text, selected) {

	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	if (selected == value) {
		opt.selected = true;
	}
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';

}
</script>
<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5"onload="initPage();">
<center>
<form name="out" action="outpatient_insert.php" method="post">
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b><? echo($_label_by_profile["OUT_REG"][$profile_type]); ?></b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="20%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($_label_by_profile["INOUT_DATETIME"][$profile_type]); ?></font></td>
<td width="80%"><select name="ty"><? show_select_years(5,$ty); ?></select><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">/</font><select name="tm"><? show_select_months($tm); ?></select><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">/</font><select name="td"><? show_select_days($td) ?></select>&nbsp;<select name="hr"><? show_select_hrs_0023($hr); ?></select><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">：</font><select name="mi"><? show_select_min($mi); ?></select></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($_label_by_profile["SECTION"][$profile_type]); ?></font></td>
<td><select name="sect" onChange="sectOnChange();"></select></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($_label_by_profile["DOCTOR"][$profile_type]); ?></font></td>
<td><select name="doc"></select></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="pt_id" value="<? echo($pt_id); ?>">
</form>
</center>
</body>
<? pg_close($con); ?>
</html>
<?
function show_select_hrs_0023($hr){

	for($i=0; $i<=23;$i++){

	  $len = strlen($i);
	  if($len <= 1){
	    $k = "0".$i;
	  }else{
	  	$k = $i;
	  }

	  echo("<option value=\"".$k."\"");
	  if($i == $hr){
	  	echo("selected");
	  }
	  $k=(int)$k;
	  echo(" >".$k."</option>\n");
	}
}
?>
