<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" action="fplus_menu.php" method="post">
<input type="hidden" name="apply_title" value="<? echo($apply_title); ?>">
<input type="hidden" name="content" value="<? echo($content); ?>">
<?
foreach ($filename as $tmp_filename) {
	echo("<input type=\"hidden\" name=\"filename[]\" value=\"$tmp_filename\">\n");
}
foreach ($file_id as $tmp_file_id) {
	echo("<input type=\"hidden\" name=\"file_id[]\" value=\"$tmp_file_id\">\n");
}

for ($i = 1; $i <= $approve_num; $i++) {
	$varname = "regist_emp_id$i";
	echo("<input type=\"hidden\" name=\"regist_emp_id$i\" value=\"{$$varname}\">\n");
}

for ($i = 1; $i <= $precond_num; $i++) {
	$varname = "precond_apply_id$i";
	echo("<input type=\"hidden\" name=\"precond_apply_id$i\" value=\"{$$varname}\">\n");
}

for ($i = 1; $i <= $precond_num; $i++) {
	$varname = "precond_wkfw_id$i";
	echo("<input type=\"hidden\" name=\"precond_wkfw_id$i\" value=\"{$$varname}\">\n");
}


/*
for ($i = 1; $i <= $approve_num; $i++) {
	$varname = "approve_name$i";
	echo("<input type=\"hidden\" name=\"approve_name$i\" value=\"{$$varname}\">\n");
}
*/

?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="apply_id" value="<? echo($apply_id); ?>">
<input type="hidden" name="approve_num" value="<? echo($approve_num); ?>">
<input type="hidden" name="precond_num" value="<? echo($precond_num); ?>">
<input type="hidden" name="back" value="t">
<input type="hidden" name="notice_emp_id" id="notice_emp_id" value="<?=$notice_emp_id?>">
<input type="hidden" name="notice_emp_nm" id="notice_emp_nm" value="<?=$notice_emp_nm?>">
<input type="hidden" name="wkfw_history_no" value="<?=$wkfw_history_no?>">
</form>

<?
$fname=$PHP_SELF;

require_once("about_comedix.php");
require_once("get_values.ini");
require_once("fplus_common_class.php");

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 78, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if (strlen($apply_title) > 80) {
	echo("<script type=\"text/javascript\">alert('表題が長すぎます。');</script>");
//	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// 添付ファイルの確認
if (!is_dir("fplus")) {
	mkdir("fplus", 0755);
}
if (!is_dir("fplus/apply")) {
	mkdir("fplus/apply", 0755);
}
if (!is_dir("fplus/apply/tmp")) {
	mkdir("fplus/apply/tmp", 0755);
}
for ($i = 0; $i < count($filename); $i++) {
	$tmp_file_id = $file_id[$i];
	$ext = strrchr($filename[$i], ".");

	$tmp_filename = "fplus/apply/tmp/{$session}_{$tmp_file_id}{$ext}";
	if (!is_file($tmp_filename)) {
		echo("<script language=\"javascript\">alert('添付ファイルが削除された可能性があります。\\n再度添付してください。');</script>");
//		echo("<script language=\"javascript\">document.items.submit();</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}
}

// データベースに接続
$con = connect2db($fname);

$obj = new fplus_common_class($con, $fname);

// トランザクションを開始
pg_query($con, "begin");

// 職員情報取得
$arr_empmst = $obj->get_empmst($session);
$emp_id        = $arr_empmst[0]["emp_id"];

//テンプレートの場合XML形式のテキスト$contentを作成
if ($wkfw_content_type == "2") {
	$ext = ".php";
	$savexmlfilename = "fplus/workflow/tmp/{$session}_x{$ext}";
//	if (is_file($savexmlfilename)) {
//		include( $savexmlfilename );
//	}

	// ワークフロー情報取得
//	$sel_wkfwmst = search_wkfwmst($con, $fname, $wkfw_id);
//	$wkfw_content = pg_fetch_result($sel_wkfwmst, 0, "wkfw_content");

	$arr_wkfw_template_history = $obj->get_wkfw_template_history($wkfw_id, $wkfw_history_no);
	$wkfw_content = $arr_wkfw_template_history["wkfw_content"];
	$wkfw_content_xml = ereg_replace("^.*<\? // XML", "<? // XML", $wkfw_content);
	$fp = fopen($savexmlfilename, "w");
	if (!$fp) {
		echo("<script language='javascript'>alert('一時ファイルがオープンできません。再度、報告してください。$savexmlfilename');</script>");
		echo("<script language='javascript'>history.back();</script>");
	}
	if(!fwrite($fp, $wkfw_content_xml, 2000000)) {
		fclose($fp);
		echo("<script language='javascript'>alert('一時ファイルに書込みできません。再度、報告してください。');</script>");
		echo("<script language='javascript'>history.back();</script>");
	}
	fclose($fp);

	include( $savexmlfilename );


}


$draft_flg = ($draft == "on") ? "t" : "f";
$apply_date = date("YmdHi");

$apply_no = null;
if($draft != "on")
{
	$date = date("Ymd");
	$year = substr($date, 0, 4);
	$md   = substr($date, 4, 4);

	if($md >= "0101" and $md <= "0331")
	{
		$year = $year - 1;
	}
	$max_cnt = $obj->get_apply_cnt_per_year($year);
	$apply_no = $max_cnt + 1;
}




//レポートの種類
//"Episys107A"-エピネット報告書Ａ針刺し・切創
//"Episys107B"-エピネット報告書Ｂ皮膚・粘膜汚染
//"MRSA"-MRSA等耐性菌検出報告書
if($EPINET_REPORT_PIERCE_CUT_INFECTION == "Episys107A")
{

	//"Episys107A"-エピネット報告書Ａ針刺し・切創
	
	//報告書上にdisableの項目があります。その場合に、以前作成されたデータが今回disbleになった場合に
	//$_REQUESTで取得できない（データが上書きされない）ので、
	//一旦	$apply_idでデータを削除してからレコードを正しいデータで再作成します。
	
	
	///////////////////////////////////////////////////////////////////////////////////////////
	//→データ登録処理用に流用しています。（報告書上にチェックボックスが無い場合の更新処理）
	///////////////////////////////////////////////////////////////////////////////////////////
	
	
	$sql=	"delete from fplus_epi_a ";		
	$cond = "where apply_id = $apply_id";
	$sel = delete_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($this->_db_con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	
	$set_data = array();
	$set_key = array();
	$set_data[0] = $apply_id;
	$set_key[0] = "apply_id";
	
	$wcnt0 = 1;
	
	foreach($_REQUEST as $epi_key => $epi_val)
	{
		$chk_key = substr($epi_key,0, 5);
		
		if($chk_key == "data_")
		{
			//エピネット報告書の入力データである
			
			//$set_data[$wcnt0] = $epi_val;
			$set_data[$wcnt0] = pg_escape_string($epi_val);
			$set_key[$wcnt0] = $epi_key;
			$wcnt0++;
		}
	}
	
	
	//集計用データの登録処理
	$obj->regist_Episys107A($set_key,$set_data);
	
	
	//集計用データの登録処理
	//$obj->update_Episys107A($apply_id,$set_key,$set_data);
	
	
}
else if(($EPINET_REPORT_PIERCE_CUT_INFECTION == "Episys107B") || ($MRSA_REPORT == "MRSA"))
{
	
	//MRSA等耐性菌検出報告書
	//"Episys107B"-エピネット報告書Ｂ皮膚・粘膜汚染

	//報告書上にdisableの項目があります。その場合に、以前作成されたデータが今回disbleになった場合に
	//$_REQUESTで取得できない（データが上書きされない）ので、
	//一旦	$apply_idでデータを削除してからレコードを正しいデータで再作成します。
	
	
	///////////////////////////////////////////////////////////////////////////////////////////
	//→データ登録処理用に流用しています。（報告書上にチェックボックスがある場合の更新処理）
	///////////////////////////////////////////////////////////////////////////////////////////
	
	
	if($EPINET_REPORT_PIERCE_CUT_INFECTION == "Episys107B")
	{ 
		//"Episys107B"-エピネット報告書Ｂ皮膚・粘膜汚染

		$sql=	"delete from fplus_epi_b ";		
		$cond = "where apply_id = $apply_id";
		$sel = delete_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
	elseif($MRSA_REPORT == "MRSA")
	{
		//MRSA等耐性菌検出報告書
		
		$sql=	"delete from fplus_infection_mrsa ";		
		$cond = "where apply_id = $apply_id";
		$sel = delete_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	
	
	$set_data = array();
	$set_key = array();
	$set_data[0] = $apply_id;
	$set_key[0] = "apply_id";
	
	$wcnt0 = 1;
	
	foreach($_REQUEST as $epi_key => $epi_val)
	{
		$chk_key = substr($epi_key,0, 5);
		
		if($chk_key == "data_")
		{

			if(is_array($epi_val))
			{
				//エピネット報告書の入力データである(checkbox用)
				
				//$set_data[$wcnt0] = $epi_val[0];
				$set_data[$wcnt0] = pg_escape_string($epi_val[0]);
				$set_key[$wcnt0] = $epi_key;
				$wcnt0++;
			}
			else
			{
				//エピネット報告書の入力データである
				
				//$set_data[$wcnt0] = $epi_val;
				$set_data[$wcnt0] = pg_escape_string($epi_val);
				$set_key[$wcnt0] = $epi_key;
				$wcnt0++;
			}
		}
	}
	

	if($EPINET_REPORT_PIERCE_CUT_INFECTION == "Episys107B")
	{ 
		//集計用データの登録処理("Episys107B"-エピネット報告書Ｂ皮膚・粘膜汚染)
		$obj->regist_Episys107B($set_key,$set_data);

		//集計用データの登録処理
		//$obj->regist_Episys107B($set_key,$set_data);
		
		//集計用データの登録処理
		//$obj->update_Episys107B($apply_id,$set_key,$set_data);
		
	}
	elseif($MRSA_REPORT == "MRSA")
	{
		//集計用データの登録処理(MRSA等耐性菌検出報告書)
		$obj->regist_MRSA($set_key,$set_data);
	}
	
	
	
}else if(IMMEDIATE_REPORT == "Immediaterepo"){
	
	//緊急報告受付

	//検索処理
	$sql=	"SELECT immediate_report_id, medical_accident_id FROM fplus_immediate_report ";
	$cond = "where apply_id = $apply_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($this->_db_con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	//レポートID、リンクIDの引き継ぎ
	$report_id = pg_fetch_result($sel, 0, "immediate_report_id");
	$link_id = pg_fetch_result($sel, 0, "medical_accident_id");

	// 新規報告�盧糧�
	$sql = " SELECT a.short_wkfw_name ".
		" FROM ".
		" 	fpluswkfwmst a ".
		" 	,fplusapply app ".
		" WHERE ".
		" 	a.wkfw_id = app.wkfw_id ".
		" 	AND app.apply_id = '$apply_id' ";
	$cond = "";
	$report_no_sel = select_from_table($con,$sql,$cond,$fname);
	$short_wkfw_name = pg_result($report_no_sel,0,"short_wkfw_name");
	$report_no = $short_wkfw_name."-".$year."".sprintf("%04d", $apply_no);

	//デリート処理
	$sql=	"delete from fplus_immediate_report ";		
	$cond = "where apply_id = $apply_id";
	$sel = delete_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($this->_db_con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	//インサート処理
	$set_data = array();
	$set_key = array();
	$set_data[0] = $apply_id;
	$set_key[0] = "apply_id";
	$set_data[1] = $report_id;
	$set_key[1] = "immediate_report_id";
	$set_data[2] = $link_id;
	$set_key[2] = "medical_accident_id";
	$set_data[3] = $report_no;
	$set_key[3] = "report_no";
	$set_data[4] = date("Y-m-d H:i:s");
	$set_key[4] = "update_time";
	$set_data[5] = "f";
	$set_key[5] = "del_flg";

	$wcnt0 = 6;
	
	foreach($_REQUEST as $epi_key => $epi_val)
	{
		$chk_key = substr($epi_key,0, 5);
		
		if($chk_key == "data_")
		{
			$epi_key = substr($epi_key,5);
			if(is_array($epi_val))
			{
				//入力データcheckbox,radio用
				if($epi_val[0]!=""){
					$set_data[$wcnt0] = pg_escape_string($epi_val[0]);
					$set_key[$wcnt0] = $epi_key;
					$wcnt0++;
				}
			}
			else
			{
				//入力データ
				if($epi_val!=""){
					$set_data[$wcnt0] = pg_escape_string($epi_val);
					$set_key[$wcnt0] = $epi_key;
					$wcnt0++;
				}
			}
		}
	}

	//集計用データの登録処理(緊急報告受付)
	$obj->regist_immediate_repo($set_key,$set_data);
	
}else if($FIRST_REPORT == "Firstrepo"){
	
	//第一報

	//検索処理
	$sql=	"SELECT first_report_id, medical_accident_id FROM fplus_first_report ";
	$cond = "where apply_id = $apply_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($this->_db_con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	//レポートID、リンクIDの引き継ぎ
	$report_id = pg_fetch_result($sel, 0, "first_report_id");
	$link_id = pg_fetch_result($sel, 0, "medical_accident_id");

	// 新規報告�盧糧�
	$sql = " SELECT a.short_wkfw_name ".
		" FROM ".
		" 	fpluswkfwmst a ".
		" 	,fplusapply app ".
		" WHERE ".
		" 	a.wkfw_id = app.wkfw_id ".
		" 	AND app.apply_id = '$apply_id' ";
	$cond = "";
	$report_no_sel = select_from_table($con,$sql,$cond,$fname);
	$short_wkfw_name = pg_result($report_no_sel,0,"short_wkfw_name");
	$report_no = $short_wkfw_name."-".$year."".sprintf("%04d", $apply_no);

	//デリート処理
	$sql=	"delete from fplus_first_report ";
	$cond = "where apply_id = $apply_id";
	$sel = delete_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($this->_db_con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	//インサート処理
	$set_data = array();
	$set_key = array();
	$set_data[0] = $apply_id;
	$set_key[0] = "apply_id";
	$set_data[1] = $report_id;
	$set_key[1] = "first_report_id";
	$set_data[2] = $link_id;
	$set_key[2] = "medical_accident_id";
	$set_data[3] = $report_no;
	$set_key[3] = "report_no";
	$set_data[4] = date("Y-m-d H:i:s");
	$set_key[4] = "update_time";
	$set_data[5] = "f";
	$set_key[5] = "del_flg";

	$wcnt0 = 6;
	
	foreach($_REQUEST as $epi_key => $epi_val)
	{
		$chk_key = substr($epi_key,0, 5);
		
		if($chk_key == "data_")
		{
			$epi_key = substr($epi_key,5);
			if(is_array($epi_val))
			{
				//入力データcheckbox,radio用
				if($epi_val[0]!=""){
					$set_data[$wcnt0] = pg_escape_string($epi_val[0]);
					$set_key[$wcnt0] = $epi_key;
					$wcnt0++;
				}
			}
			else
			{
				//入力データ
				if($epi_val!=""){
					$set_data[$wcnt0] = pg_escape_string($epi_val);
					$set_key[$wcnt0] = $epi_key;
					$wcnt0++;
				}
			}
		}
	}

	//集計用データの登録処理(第一報)
	$obj->regist_first_repo($set_key,$set_data);

}else if($SECOND_REPORT == "Secondrepo"){
	
	//様式２

	//第一報リンクの有無を調べるここから
	$sql= " SELECT ".
		" 	'first' as table_id ".
		" FROM ".
		" 	fplus_second_report s ".
		" 	, fplus_first_report f ".
		" 	, fplusapply app1 ".
		" 	, fplusapply app2 ".
		" WHERE ".
		" 	s.medical_accident_id = f.medical_accident_id ".
		" 	AND s.apply_id = app1.apply_id ".
		" 	AND f.apply_id = app2.apply_id ".
		" 	AND app1.delete_flg = 'f' ".
		" 	AND app2.delete_flg = 'f' ".
		" 	AND s.apply_id = '$apply_id' ".

	$cond = "";
	$sel_link = select_from_table($con, $sql, $cond, $fname);
	$link_data = pg_fetch_all($sel_link);

	$first_flg = 0;
	foreach($link_data as $idx => $row){
		if($row["table_id"] == "first"){
			$first_flg = 1;
		}
	}
	//リンクの有無を調べるここまで

	//検索処理
	$sql=	"SELECT second_report_id, medical_accident_id FROM fplus_second_report ";
	$cond = "where apply_id = $apply_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($this->_db_con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	//レポートID、リンクIDの引き継ぎ(リンクIDは変更がない場合のみ引き継ぐ)
	$report_id = pg_fetch_result($sel, 0, "second_report_id");
	$old_link_id = pg_fetch_result($sel, 0, "medical_accident_id");

	// 新規報告�盧糧�
	$sql = " SELECT a.short_wkfw_name ".
		" FROM ".
		" 	fpluswkfwmst a ".
		" 	,fplusapply app ".
		" WHERE ".
		" 	a.wkfw_id = app.wkfw_id ".
		" 	AND app.apply_id = '$apply_id' ";
	$cond = "";
	$report_no_sel = select_from_table($con,$sql,$cond,$fname);
	$short_wkfw_name = pg_result($report_no_sel,0,"short_wkfw_name");
	$report_no = $short_wkfw_name."-".$year."".sprintf("%04d", $apply_no);

	//デリート処理
	$sql=	"delete from fplus_second_report ";		
	$cond = "where apply_id = $apply_id";
	$sel = delete_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($this->_db_con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	if($hdn_link_id==""){
		// 新規リンク�盧糧�
		$sql = "SELECT max(medical_accident_id) FROM fplus_medical_accident_report_management";
		$cond = "";
		$link_id_sel = select_from_table($con,$sql,$cond,$fname);
		$link_id = pg_result($link_id_sel,0,"max");
		if($link_id == ""){
			$link_id = 1;
		}else{
			$link_id = $link_id + 1;
		}

		//リンクテーブル登録項目ここから
		$set_data_link = array();
		$set_key_link = array();
		$set_data_link[0] = $link_id;
		$set_key_link[0] = "medical_accident_id";
		$set_data_link[1] = date("Y-m-d H:i:s");
		$set_key_link[1] = "update_time";
		$set_data_link[2] = "f";
		$set_key_link[2] = "del_flg";
		//リンクテーブル登録項目ここまで
	}else{
		//引用した第一報のリンクID
		$link_id = $hdn_link_id;
	}

	//第二報登録項目
	$set_data = array();
	$set_key = array();
	$set_data[0] = $apply_id;
	$set_key[0] = "apply_id";
	$set_data[1] = $report_id;
	$set_key[1] = "second_report_id";
	$set_data[2] = $link_id;
	$set_key[2] = "medical_accident_id";
	$set_data[3] = $report_no;
	$set_key[3] = "report_no";
	$set_data[4] = date("Y-m-d H:i:s");
	$set_key[4] = "update_time";
	$set_data[5] = "f";
	$set_key[5] = "del_flg";

	$wcnt0 = 6;
	
	foreach($_REQUEST as $epi_key => $epi_val)
	{
		$chk_key = substr($epi_key,0, 5);
		$check .= $chk_key.",";
		if($chk_key == "data_")
		{
			$epi_key = substr($epi_key,5);
			if(is_array($epi_val))
			{
				//入力データcheckbox,radio用
				if($epi_val[0]!=""){
					$set_data[$wcnt0] = pg_escape_string($epi_val[0]);
					$set_key[$wcnt0] = $epi_key;
					$wcnt0++;
				}
			}
			else
			{
				//入力データ
				if($epi_val!=""){
					$set_data[$wcnt0] = pg_escape_string($epi_val);
					$set_key[$wcnt0] = $epi_key;
					$wcnt0++;
				}
			}
		}
	}

	//リンクテーブル操作ここから
	if($hdn_link_id==""){
		//リンクテーブルの登録処理(様式２)
		$obj->regist_link_table($set_key_link,$set_data_link);
	}else{
		if($first_flg == 0){
			//リンクテーブルデータの削除(論理削除)

			//リンクテーブル登録項目ここから
			$set_data_link = array();
			$set_key_link = array();
			$set_data_link[0] = $old_link_id;
			$set_key_link[0] = "medical_accident_id";
			$set_data_link[1] = date("Y-m-d H:i:s");
			$set_key_link[1] = "update_time";
			$set_data_link[2] = "t";
			$set_key_link[2] = "del_flg";
			//リンクテーブル登録項目ここまで

			$obj->update_medical_accident_report_management($old_link_id,$set_key,$set_data);
		}
	}
	//リンクテーブル操作ここまで

	//第二報データの登録処理(様式２)
	$obj->regist_second_repo($set_key,$set_data);

}else if($CONSULTATION_REQUEST == "Consreq"){
	

	//検索処理
	$sql=	"SELECT consultation_request_id FROM fplus_consultation_request ";
	$cond = "where apply_id = $apply_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($this->_db_con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	//レポートIDの引き継ぎ
	$report_id = pg_fetch_result($sel, 0, "consultation_request_id");

	// 新規報告�盧糧�
	$sql = " SELECT a.short_wkfw_name ".
		" FROM ".
		" 	fpluswkfwmst a ".
		" 	,fplusapply app ".
		" WHERE ".
		" 	a.wkfw_id = app.wkfw_id ".
		" 	AND app.apply_id = '$apply_id' ";
	$cond = "";
	$report_no_sel = select_from_table($con,$sql,$cond,$fname);
	$short_wkfw_name = pg_result($report_no_sel,0,"short_wkfw_name");
	$report_no = $short_wkfw_name."-".$year."".sprintf("%04d", $apply_no);

	//コンサルテーション依頼書
	//デリート処理
	$sql=	"delete from fplus_consultation_request ";		
	$cond = "where apply_id = $apply_id";
	$sel = delete_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($this->_db_con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	if($hdn_link_id == ""){
		$link_id = NULL;
	}else{
		//引用した第一報のリンクID
		$link_id = $hdn_link_id;
	}
	$link_kbn = "";
	if($hdn_link_report_kbn!=""){
		//引用した第一報のリンクID
		$link_kbn = $hdn_link_report_kbn;
	}

	//インサート処理
	$set_data = array();
	$set_key = array();
	$set_data[0] = $apply_id;
	$set_key[0] = "apply_id";
	$set_data[1] = $report_id;
	$set_key[1] = "consultation_request_id";
	$set_data[2] = $report_no;
	$set_key[2] = "report_no";
	$set_data[3] = $link_id;
	$set_key[3] = "report_id";
	$set_data[4] = $link_kbn;
	$set_key[4] = "report_type";
	$set_data[5] = date("Y-m-d H:i:s");
	$set_key[5] = "update_time";
	$set_data[6] = "f";
	$set_key[6] = "del_flg";

	$wcnt0 = 7;
	
	foreach($_REQUEST as $epi_key => $epi_val)
	{
		$chk_key = substr($epi_key,0, 5);
		
		if($chk_key == "data_")
		{
			$epi_key = substr($epi_key,5);
			if(is_array($epi_val)){
				//入力データcheckbox,radio用
				if($epi_val[0]!=""){
					$set_data[$wcnt0] = pg_escape_string($epi_val[0]);
					$set_key[$wcnt0] = $epi_key;
					$wcnt0++;
				}
			}else{
				//入力データ
				if($epi_val!=""){
					$set_data[$wcnt0] = pg_escape_string($epi_val);
					$set_key[$wcnt0] = $epi_key;
					$wcnt0++;
				}
			}
		}
	}

	//集計用データの登録処理("コンサルテーション依頼書)
	$obj->regist_EpisysConsreq($set_key,$set_data);

}else if($CONSULTATION_REPORT == "Consrep"){
	
	//"コンサルテーション報告書

	//検索処理
	$sql=	"SELECT consultation_report_id FROM fplus_consultation_report ";
	$cond = "where apply_id = $apply_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($this->_db_con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	//レポートIDの引き継ぎ
	$report_id = pg_fetch_result($sel, 0, "consultation_report_id");

	// 新規報告�盧糧�
	$sql = " SELECT a.short_wkfw_name ".
		" FROM ".
		" 	fpluswkfwmst a ".
		" 	,fplusapply app ".
		" WHERE ".
		" 	a.wkfw_id = app.wkfw_id ".
		" 	AND app.apply_id = '$apply_id' ";
	$cond = "";
	$report_no_sel = select_from_table($con,$sql,$cond,$fname);
	$short_wkfw_name = pg_result($report_no_sel,0,"short_wkfw_name");
	$report_no = $short_wkfw_name."-".$year."".sprintf("%04d", $apply_no);

	//デリート処理
	$sql=	"delete from fplus_consultation_report ";		
	$cond = "where apply_id = $apply_id";
	$sel = delete_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($this->_db_con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	if($hdn_consultation_request_id == ""){
		$consultation_request_id = NULL;
	}else{
		//引用したレポートID
		$consultation_request_id = $hdn_consultation_request_id;
	}

	//インサート処理
	$set_data = array();
	$set_key = array();
	$set_data[0] = $apply_id;
	$set_key[0] = "apply_id";
	$set_data[1] = $report_id;
	$set_key[1] = "consultation_report_id";
	$set_data[2] = $report_no;
	$set_key[2] = "report_no";
	$set_data[3] = $consultation_request_id;
	$set_key[3] = "consultation_request_id";
	$set_data[4] = date("Y-m-d H:i:s");
	$set_key[4] = "update_time";
	$set_data[5] = "f";
	$set_key[5] = "del_flg";

	$wcnt0 = 6;
	
	foreach($_REQUEST as $epi_key => $epi_val)
	{
		$chk_key = substr($epi_key,0, 5);
		
		if($chk_key == "data_")
		{
			$epi_key = substr($epi_key,5);
			if(is_array($epi_val))
			{
				//入力データcheckbox,radio用
				if($epi_val[0]!=""){
					$set_data[$wcnt0] = pg_escape_string($epi_val[0]);
					$set_key[$wcnt0] = $epi_key;
					$wcnt0++;
				}
			}
			else
			{
				//入力データ
				if($epi_val!=""){
					$set_data[$wcnt0] = pg_escape_string($epi_val);
					$set_key[$wcnt0] = $epi_key;
					$wcnt0++;
				}
			}
		}
	}

	//集計用データの登録処理("コンサルテーション報告書)
	$obj->regist_EpisysConsrep($set_key,$set_data);

}else if($MEDICAL_APPARATUS_FAULT_REPORT == "medicalapparatusfaultreport"){
	
	//医療機器不具合報告

	//検索処理
	$sql=	"SELECT medical_apparatus_fault_report_id FROM fplus_medical_apparatus_fault_report ";
	$cond = "where apply_id = $apply_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($this->_db_con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	//レポートID、リンクIDの引き継ぎ
	$report_id = pg_fetch_result($sel, 0, "medical_apparatus_fault_report_id");

	// 新規報告�盧糧�
	$sql = " SELECT a.short_wkfw_name ".
		" FROM ".
		" 	fpluswkfwmst a ".
		" 	,fplusapply app ".
		" WHERE ".
		" 	a.wkfw_id = app.wkfw_id ".
		" 	AND app.apply_id = '$apply_id' ";
	$cond = "";
	$report_no_sel = select_from_table($con,$sql,$cond,$fname);
	$short_wkfw_name = pg_result($report_no_sel,0,"short_wkfw_name");
	$report_no = $short_wkfw_name."-".$year."".sprintf("%04d", $apply_no);

	//デリート処理
	$sql=	"delete from fplus_medical_apparatus_fault_report ";
	$cond = "where apply_id = $apply_id";
	$sel = delete_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($this->_db_con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	//インサート処理
	$set_data = array();
	$set_key = array();
	$set_data[0] = $apply_id;
	$set_key[0] = "apply_id";
	$set_data[1] = $report_id;
	$set_key[1] = "medical_apparatus_fault_report_id";
	$set_data[2] = $report_no;
	$set_key[2] = "report_no";
	$set_data[3] = date("Y-m-d H:i:s");
	$set_key[3] = "update_time";
	$set_data[4] = "f";
	$set_key[4] = "del_flg";

	$wcnt0 = 5;
	
	foreach($_REQUEST as $epi_key => $epi_val)
	{
		$chk_key = substr($epi_key,0, 5);
		
		if($chk_key == "data_")
		{
			$epi_key = substr($epi_key,5);
			if(is_array($epi_val))
			{
				//入力データcheckbox,radio用
				if($epi_val[0]!=""){
					$set_data[$wcnt0] = pg_escape_string($epi_val[0]);
					$set_key[$wcnt0] = $epi_key;
					$wcnt0++;
				}
			}
			else
			{
				//入力データ
				if($epi_val!=""){
					$set_data[$wcnt0] = pg_escape_string($epi_val);
					$set_key[$wcnt0] = $epi_key;
					$wcnt0++;
				}
			}
		}
	}

	//データの登録処理(医療機器不具合報告)
	$obj->regist_medical_apparatus_fault_report($set_key,$set_data);

}else if($NUMBER_OF_OCCURRENCES == "number_of_occurrences"){
	
	//ヒヤリハット件数表

	// 新規報告�盧糧�
	$sql = " SELECT a.short_wkfw_name ".
		" FROM ".
		" 	fpluswkfwmst a ".
		" 	,fplusapply app ".
		" WHERE ".
		" 	a.wkfw_id = app.wkfw_id ".
		" 	AND app.apply_id = '$apply_id' ";
	$cond = "";
	$report_no_sel = select_from_table($con,$sql,$cond,$fname);
	$short_wkfw_name = pg_result($report_no_sel,0,"short_wkfw_name");
	$report_no = $short_wkfw_name."-".$year."".sprintf("%04d", $apply_no);

	//年度、病院IDで既存データ検索
	$hospital_id = @$_REQUEST["data_hospital_id"];
	$data_occurrences_year = @$_REQUEST["data_occurrences_year"];
	$sql = " SELECT ".
		" 	lev.apply_id ".
		" FROM ".
		" 	fplus_number_of_occurrences_level lev ".
		" 	,fplusapply app  ".
		" WHERE ".
		" 	hospital_id = '$hospital_id' ".
		" 	AND occurrences_year = '$data_occurrences_year' ".
		" 	AND del_flg = 'f' ".
		" 	AND lev.apply_id = app.apply_id ".
		" 	AND app.delete_flg = 'f' ";
	$cond = "";
	$del_apply_id_sel = select_from_table($con,$sql,$cond,$fname);
	$del_apply_id = pg_result($del_apply_id_sel,0,"apply_id");

	if($del_apply_id != ""){
		if($apply_id != $del_apply_id){
			//レベル別発生件数データ削除処理(論理削除)
			$sql=	"update fplus_number_of_occurrences_level set";
			$set =	array("del_flg");
			$cond = "where apply_id = '$del_apply_id' AND del_flg = 'f' ";
			$setvalue = array("t");
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}

			//職種別発生件数データ削除処理(論理削除)
			$sql=	"update fplus_number_of_occurrences set";
			$set =	array("del_flg");
			$cond = "where apply_id = '$del_apply_id' AND del_flg = 'f' ";
			$setvalue = array("t");
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}

			//転倒・転落発生件数データ削除処理(論理削除)
			$sql=	"update fplus_number_of_occurrences_fall_accident set";
			$set =	array("del_flg");
			$cond = "where apply_id = '$del_apply_id' AND del_flg = 'f' ";
			$setvalue = array("t");
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}

			//報告データ削除（論理削除）
			$sql=	"update fplusapply set";
			$set =	array("delete_flg");
			$cond = "where apply_id = '$del_apply_id' AND delete_flg = 'f' ";
			$setvalue = array("t");
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}

	// レベル別発生件数データ
	//検索処理
	$sql=	"SELECT levelt_id FROM fplus_number_of_occurrences_level ";
	$cond = "where apply_id = $apply_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($this->_db_con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	//レポートIDの引き継ぎ
	$report_id = pg_fetch_result($sel, 0, "levelt_id");

	//デリート処理
	$sql=	"delete from fplus_number_of_occurrences_level ";
	$cond = "where apply_id = $apply_id";
	$sel = delete_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($this->_db_con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	//インサート処理
	$set_data = array();
	$set_key = array();
	$set_data[0] = $apply_id;
	$set_key[0] = "apply_id";
	$set_data[1] = $report_id;
	$set_key[1] = "levelt_id";
	$set_data[2] = $report_no;
	$set_key[2] = "report_no";
	$set_data[3] = date("Y-m-d H:i:s");
	$set_key[3] = "update_time";
	$set_data[4] = "f";
	$set_key[4] = "del_flg";

	$wcnt0 = 5;
	
	foreach($_REQUEST as $epi_key => $epi_val)
	{
		$chk_key = substr($epi_key,0, 5);
		
		if($chk_key == "data_" or $chk_key == "dat1_")
		{
			$epi_key = substr($epi_key,5);
			if(is_array($epi_val))
			{
				//入力データcheckbox,radio用
				if($epi_val[0]!=""){
					$set_data[$wcnt0] = pg_escape_string($epi_val[0]);
					$set_key[$wcnt0] = $epi_key;
					$wcnt0++;
				}
			}
			else
			{
				//入力データ
				if($epi_val!=""){
					$set_data[$wcnt0] = pg_escape_string($epi_val);
					$set_key[$wcnt0] = $epi_key;
					$wcnt0++;
				}
			}
		}
	}

	//データの登録処理(発生件数データ)
	$obj->regist_number_of_occurrences_level($set_key,$set_data);

	// 職種別発生件数データ
	//検索処理
	$sql=	"SELECT number_of_occurrences_id FROM fplus_number_of_occurrences ";
	$cond = "where apply_id = $apply_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($this->_db_con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	//レポートID、リンクIDの引き継ぎ
	$report_id = pg_fetch_result($sel, 0, "number_of_occurrences_id");

	// 新規報告�盧糧�
	$sql = " SELECT a.short_wkfw_name ".
		" FROM ".
		" 	fpluswkfwmst a ".
		" 	,fplusapply app ".
		" WHERE ".
		" 	a.wkfw_id = app.wkfw_id ".
		" 	AND app.apply_id = '$apply_id' ";
	$cond = "";
	$report_no_sel = select_from_table($con,$sql,$cond,$fname);
	$short_wkfw_name = pg_result($report_no_sel,0,"short_wkfw_name");
	$report_no = $short_wkfw_name."-".$year."".sprintf("%04d", $apply_no);

	//デリート処理
	$sql=	"delete from fplus_number_of_occurrences ";
	$cond = "where apply_id = $apply_id";
	$sel = delete_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($this->_db_con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	//インサート処理
	$set_data = array();
	$set_key = array();
	$set_data[0] = $apply_id;
	$set_key[0] = "apply_id";
	$set_data[1] = $report_id;
	$set_key[1] = "number_of_occurrences_id";
	$set_data[2] = $report_no;
	$set_key[2] = "report_no";
	$set_data[3] = date("Y-m-d H:i:s");
	$set_key[3] = "update_time";
	$set_data[4] = "f";
	$set_key[4] = "del_flg";

	$wcnt0 = 5;
	
	foreach($_REQUEST as $epi_key => $epi_val)
	{
		$chk_key = substr($epi_key,0, 5);
		
		if($chk_key == "data_" or $chk_key == "dat2_")
		{
			$epi_key = substr($epi_key,5);
			if(is_array($epi_val))
			{
				//入力データcheckbox,radio用
				if($epi_val[0]!=""){
					$set_data[$wcnt0] = pg_escape_string($epi_val[0]);
					$set_key[$wcnt0] = $epi_key;
					$wcnt0++;
				}
			}
			else
			{
				//入力データ
				if($epi_val!=""){
					$set_data[$wcnt0] = pg_escape_string($epi_val);
					$set_key[$wcnt0] = $epi_key;
					$wcnt0++;
				}
			}
		}
	}

	//データの登録処理(発生件数データ)
	$obj->regist_number_of_occurrences($set_key,$set_data);

	// 転倒・転落発生件数データ
	//検索処理
	$sql=	"SELECT fall_accident_id FROM fplus_number_of_occurrences_fall_accident ";
	$cond = "where apply_id = $apply_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($this->_db_con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	//レポートID、リンクIDの引き継ぎ
	$report_id = pg_fetch_result($sel, 0, "fall_accident_id");

	//デリート処理
	$sql=	"delete from fplus_number_of_occurrences_fall_accident ";
	$cond = "where apply_id = $apply_id";
	$sel = delete_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($this->_db_con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	//インサート処理
	$set_data = array();
	$set_key = array();
	$set_data[0] = $apply_id;
	$set_key[0] = "apply_id";
	$set_data[1] = $report_id;
	$set_key[1] = "fall_accident_id";
	$set_data[2] = $report_no;
	$set_key[2] = "report_no";
	$set_data[3] = date("Y-m-d H:i:s");
	$set_key[3] = "update_time";
	$set_data[4] = "f";
	$set_key[4] = "del_flg";

	$wcnt0 = 5;
	
	foreach($_REQUEST as $epi_key => $epi_val)
	{
		$chk_key = substr($epi_key,0, 5);
		
		if($chk_key == "data_" or $chk_key == "dat3_")
		{
			$epi_key = substr($epi_key,5);
			if(is_array($epi_val))
			{
				//入力データcheckbox,radio用
				if($epi_val[0]!=""){
					$set_data[$wcnt0] = pg_escape_string($epi_val[0]);
					$set_key[$wcnt0] = $epi_key;
					$wcnt0++;
				}
			}
			else
			{
				//入力データ
				if($epi_val!=""){
					$set_data[$wcnt0] = pg_escape_string($epi_val);
					$set_key[$wcnt0] = $epi_key;
					$wcnt0++;
				}
			}
		}
	}

	//データの登録処理(転倒・転落発生件数データ)
	$obj->regist_number_of_occurrences_fall_accident($set_key,$set_data);

}




// 申請更新
$obj->update_apply_draft($apply_id, $content, $apply_date, $apply_title, $draft_flg, $apply_no);

// 下書きでない場合、メール送信準備
$to_addresses = array();
if ($draft != "on") {
	$wkfw_send_mail_flg = fplus_get_wkfw_send_mail_flg($con, $wkfw_id, $fname);
} else {
	$wkfw_send_mail_flg = "f";
}

// 承認更新
for($i=1; $i<=$approve_num; $i++)
{
	$varname = "regist_emp_id$i";
	$regist_emp_id = $$varname;

	$varname = "apv_order$i";
	$apv_order = $$varname;

	$varname = "apv_sub_order$i";
	$apv_sub_order = $$varname;

	$varname = "st_div$i";
	$st_div = $$varname;

	$varname = "parent_pjt_id$i";
	$parent_pjt_id = ($$varname == "") ? null : $$varname;

	$varname = "child_pjt_id$i";
	$child_pjt_id = ($$varname == "") ? null : $$varname;


	// 所属、役職も登録する
	$infos = get_empmst($con, $regist_emp_id, $fname);
	$emp_class     = $infos[2];
	$emp_attribute = $infos[3];
	$emp_dept      = $infos[4];
	$emp_st        = $infos[6];
	$emp_room      = $infos[33];
	$emp_email     = $infos[31];

	$arr = array(
	              "apply_id" => $apply_id,
	              "apv_order" => $apv_order,
                  "apv_sub_order" => $apv_sub_order,
                  "emp_id" => $regist_emp_id,
                  "st_div" => ($st_div == "") ? null : $st_div,
                  "emp_class" => $emp_class,
                  "emp_attribute" => $emp_attribute,
                  "emp_dept" => $emp_dept,
                  "emp_st" => $emp_st,
                  "emp_room" => $emp_room,
                  "parent_pjt_id" => $parent_pjt_id,
	              "child_pjt_id" => $child_pjt_id
	             );

	$obj->update_applyapv($arr);

	if (fplus_must_send_mail($wkfw_send_mail_flg, $emp_email, $wkfw_appr, $apv_order)) {
		$to_addresses[] = $emp_email;
	}
}

if($draft != "on")
{
    $obj->delete_applyapv($apply_id);
}

// 添付ファイル削除・登録
$obj->delete_applyfile($apply_id);
$no = 1;
foreach ($filename as $tmp_filename)
{
	$obj->regist_applyfile($apply_id, $no, $tmp_filename);
	$no++;
}

// 申請結果通知削除・登録
$obj->delete_applynotice($apply_id);
if($notice_emp_id != "")
{
	$arr_notice_emp_id = split(",", $notice_emp_id);
    $arr_rslt_ntc_div  = split(",", $rslt_ntc_div);
	for($i=0; $i<count($arr_notice_emp_id); $i++)
	{
		$obj->regist_applynotice($apply_id, $arr_notice_emp_id[$i], $arr_rslt_ntc_div[$i]);
	}
}
// 前提とする申請書(申請用)更新
for($i=0; $i<$precond_num; $i++)
{
	$order = $i + 1;
	$varname = "precond_wkfw_id$order";
	$precond_wkfw_id = $$varname;

	$varname = "precond_apply_id$order";
	$precond_apply_id = $$varname;

	$obj->update_applyprecond($apply_id, $precond_wkfw_id, $order, $precond_apply_id);
}

// メール送信に必要な情報を取得
if (count($to_addresses) > 0) {
	$emp_detail = $obj->get_empmst_detail($emp_id);
	$emp_nm = fplus_format_emp_nm($emp_detail[0]);
	$emp_mail = fplus_format_emp_mail($emp_detail[0]);
	$emp_pos = fplus_format_emp_pos($emp_detail[0]);

	$wkfwmst = $obj->get_wkfwmst($wkfw_id);
	$approve_label = ($wkfwmst[0]["approve_label"] != "2") ? "承認" : "確認";
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を切断
pg_close($con);

// メール送信（後でDB切断後に移動する）
if (count($to_addresses) > 0) {
	mb_internal_encoding("EUC-JP");
	mb_language("Japanese");

	$mail_subject = "[CoMedix] 受付依頼のお知らせ";
	$mail_content = fplus_format_mail_content($wkfw_content_type, $content);
	$mail_separator = str_repeat("-", 60) . "\n";
	$additional_headers = "From: $emp_mail";
	$additional_parameter = "-f$emp_mail";

	foreach ($to_addresses as $to_address) {
		$mail_body = "以下の受付依頼がありました。\n\n";
		$mail_body .= "報告者：{$emp_nm}\n";
		$mail_body .= "所属：{$emp_pos}\n";
		$mail_body .= "表題：{$apply_title}\n";
		if ($mail_content != "") {
			$mail_body .= $mail_separator;
			$mail_body .= "{$mail_content}\n";
		}

		mb_send_mail($to_address, $mail_subject, $mail_body, $additional_headers, $additional_parameter);
	}
}

// 添付ファイルの移動
foreach (glob("fplus/apply/{$apply_id}_*.*") as $tmpfile) {
	unlink($tmpfile);
}
for ($i = 0; $i < count($filename); $i++) {
	$tmp_file_id = $file_id[$i];
	$tmp_filename = $filename[$i];
	$tmp_fileno = $i + 1;
	$ext = strrchr($tmp_filename, ".");

	$tmp_filename = "fplus/apply/tmp/{$session}_{$tmp_file_id}{$ext}";
	copy($tmp_filename, "fplus/apply/{$apply_id}_{$tmp_fileno}{$ext}");
}
foreach (glob("fplus/apply/tmp/{$session}_*.*") as $tmpfile) {
	unlink($tmpfile);
}

if($draft == "on")
{
	echo("<script type=\"text/javascript\">location.href='fplus_menu.php?session=$session&apply_id=$apply_id';</script>");
}
else
{
	echo("<script type=\"text/javascript\">location.href='fplus_apply_list.php?session=$session&mode=search&category=$wkfw_type&workflow=$wkfw_id';</script>");
}


?>
</body>
<?
// ワークフロー情報取得
/*
function search_wkfwmst($con, $fname, $wkfw_id) {

	$sql = "select * from wkfwmst";
	$cond="where wkfw_id='$wkfw_id'";
	$sel=select_from_table($con,$sql,$cond,$fname);
	if($sel==0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	return $sel;
}
*/
?>