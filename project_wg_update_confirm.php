<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" action="project_wg_update.php" method="post">
<input type="hidden" name="project_parent_id" value="<? echo($project_parent_id); ?>">
<input type="hidden" name="project_name" value="<? echo($project_name); ?>">
<input type="hidden" name="project_name_abbrev" value="<? echo($project_name_abbrev); ?>">
<input type="hidden" name="public_flag" value="<? echo($public_flag); ?>">
<input type="hidden" name="s_dt1" value="<? echo($s_dt1); ?>">
<input type="hidden" name="s_dt2" value="<? echo($s_dt2); ?>">
<input type="hidden" name="s_dt3" value="<? echo($s_dt3); ?>">
<input type="hidden" name="e_dt1" value="<? echo($e_dt1); ?>">
<input type="hidden" name="e_dt2" value="<? echo($e_dt2); ?>">
<input type="hidden" name="e_dt3" value="<? echo($e_dt3); ?>">
<input type="hidden" name="real_e_dt1" value="<? echo($real_e_dt1); ?>">
<input type="hidden" name="real_e_dt2" value="<? echo($real_e_dt2); ?>">
<input type="hidden" name="real_e_dt3" value="<? echo($real_e_dt3); ?>">
<input type="hidden" name="incharge" value="<? echo($incharge); ?>">
<input type="hidden" name="incharge_id" value="<? echo($incharge_id); ?>">
<input type="hidden" name="mygroup" value="<? echo($mygroup); ?>">
<input type="hidden" name="target_id_list1" value="<? echo($target_id_list1); ?>">
<input type="hidden" name="target_id_list2" value="<? echo($target_id_list2); ?>">
<input type="hidden" name="target_id_list3" value="<? echo($target_id_list3); ?>">
<input type="hidden" name="project_content" value="<? echo($project_content); ?>">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="pjt_id" value="<? echo($pjt_id); ?>">
<input type="hidden" name="back" value="t">
<input type="hidden" name="schd_emp_id" value="<? echo($schd_emp_id); ?>">
<input type="hidden" name="secretaria_flg" value="<? echo($secretaria_flg); ?>">
<input type="hidden" name="adm_flg" value="<? echo($adm_flg); ?>">
<input type="hidden" name="ovtm_approve_num" value="<? echo($ovtm_approve_num); ?>">
<?
for ($i = 0; $i < $ovtm_approve_num; $i++) {
	$j = $i + 1;


	$str_emp_id = "ovtm_emp_id".$j;
	$str_emp_nm = "ovtm_emp_nm".$j;
	$str_content = "ovtm_approve_content".$j;
	$str_notice = "ovtm_next_notice_div".$j;

	echo("<input type=\"hidden\" name=\"ovtm_approve_content" . $j . "\" value=\"".$$str_content."\">\n");
	echo("<input type=\"hidden\" name=\"ovtm_emp_id" . $j . "\" value=\"".$$str_emp_id."\">\n");
	echo("<input type=\"hidden\" name=\"ovtm_emp_nm" . $j . "\" value=\"".$$str_emp_nm."\">\n");
	echo("<input type=\"hidden\" name=\"ovtm_next_notice_div" . $j . "\" value=\"".$$str_notice."\">\n");
}
?>



</form>

<?
require_once("about_comedix.php");
require_once("Cmx.php");
require_once("aclg_set.php");

$fname = $PHP_SELF;

//セッションチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//委員会・WG権限チェック
$work = check_authority($session,31,$fname);
if($work == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// 入力チェック
if ($project_parent_id == "-") {
	echo("<script language=\"javascript\">alert(\"委員会を選択してください。\");</script>");
	echo("<script language=\"javascript\">document.items.submit();</script>");
	exit;
}
if ($project_name == "") {
	echo("<script language=\"javascript\">alert(\"WG名が入力されていません。\");</script>");
	echo("<script language=\"javascript\">document.items.submit();</script>");
	exit;
}
if (strlen($project_name) > 50) {
	echo("<script language=\"javascript\">alert(\"WG名が長すぎます。\");</script>");
	echo("<script language=\"javascript\">document.items.submit();</script>");
	exit;
}
if ($project_name_abbrev == "") {
	echo("<script language=\"javascript\">alert(\"略称が入力されていません。\");</script>");
	echo("<script language=\"javascript\">document.items.submit();</script>");
	exit;
}
if (strlen($project_name_abbrev) > 50) {
	echo("<script language=\"javascript\">alert(\"略称が長すぎます。\");</script>");
	echo("<script language=\"javascript\">document.items.submit();</script>");
	exit;
}
if ($public_flag == "") {
	echo("<script language=\"javascript\">alert(\"公開または非公開を選択してください。\");</script>");
	echo("<script language=\"javascript\">document.items.submit();</script>");
	exit;
}
if (!checkdate($s_dt2, $s_dt3, $s_dt1)) {
	echo("<script language=\"javascript\">alert(\"開始日が不正です。\");</script>");
	echo("<script language=\"javascript\">document.items.submit();</script>");
	exit;
}
if ($e_dt1 != "-" || $e_dt2 != "-" || $e_dt3 != "-") {
	if (!checkdate($e_dt2, $e_dt3, $e_dt1)) {
		echo("<script language=\"javascript\">alert(\"終了予定日が不正です。\");</script>");
		echo("<script language=\"javascript\">document.items.submit();</script>");
		exit;
	}
}
if ($real_e_dt1 != "-" || $real_e_dt2 != "-" || $real_e_dt3 != "-") {
	if (!checkdate($real_e_dt2, $real_e_dt3, $real_e_dt1)) {
		echo("<script language=\"javascript\">alert(\"終了日が不正です。\");</script>");
		echo("<script language=\"javascript\">document.items.submit();</script>");
		exit;
	}
}
$s_date = "$s_dt1$s_dt2$s_dt3";
if ($e_dt1 != "-" || $e_dt2 != "-" || $e_dt3 != "-") {
	$e_date = "$e_dt1$e_dt2$e_dt3";
	if ($s_date > $e_date) {
		echo("<script language=\"javascript\">alert(\"終了予定日は開始日より後にしてください。\");</script>");
		echo("<script language=\"javascript\">document.items.submit();</script>");
		exit;
	}
} else {
	$e_date = null;
}
if ($real_e_dt1 != "-" || $real_e_dt2 != "-" || $real_e_dt3 != "-") {
	$real_e_date = "$real_e_dt1$real_e_dt2$real_e_dt3";
	if ($s_date > $real_e_date) {
		echo("<script language=\"javascript\">alert(\"終了日は開始日より後にしてください。\");</script>");
		echo("<script language=\"javascript\">document.items.submit();</script>");
		exit;
	}
} else {
	$real_e_date = "";
}
if ($incharge_id == "") {
	echo("<script language=\"javascript\">alert(\"責任者が選択されていません。\");</script>");
	echo("<script language=\"javascript\">document.items.submit();</script>");
	exit;
}
$arr_target = array();
if ($target_id_list1 != "") {
	$arr_target_id = split(",", $target_id_list1);

	for ($i = 0; $i < count($arr_target_id); $i++) {
		array_push($arr_target, array("id" => $arr_target_id[$i], "kind" => "1"));
	}

}

if ($target_id_list2 != "") {
	$arr_target_id = split(",", $target_id_list2);

	for ($i = 0; $i < count($arr_target_id); $i++) {
		array_push($arr_target, array("id" => $arr_target_id[$i], "kind" => "2"));
	}
}

if (strlen($project_content) > 400) {
	echo("<script language=\"javascript\">alert(\"内容が長すぎます。\");</script>");
	echo("<script language=\"javascript\">document.items.submit();</script>");
	exit;
}

$chk_apv_exist_flg = true;
for ($i = 0; $i < $ovtm_approve_num; $i++)
{
	$idx = $i + 1;

	$approve = "ovtm_emp_id$idx";
	if($$approve == "")
	{
		$chk_apv_exist_flg = false;
	}

}
if(!$chk_apv_exist_flg) {
	echo("<script type=\"text/javascript\">alert('承認階層に承認者を設定してください。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}

// トランザクションの開始
pg_query($con, "begin");

// 登録値の編集
if ($public_flag != "t") {
	$public_flag = "f";
	$private_flag = "t";
} else {
	$private_flag = "f";
}


//ソートIDを振りなおすために更新前の親委員会情報を取得しておく

//移動前の委員会のソートIDを振りなおし
$sql = "select pjt_parent_id from project ";
$cond = " where pjt_id=".$pjt_id;
$former_sel = select_from_table($con, $sql, $cond, $fname);
if ($former_sel == "0") {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}



// WG情報を更新
$sql = "update project set";
$set = array("pjt_id", "pjt_name", "pjt_start_date", "pjt_end_date", "pjt_real_end_date", "pjt_response", "pjt_public_flag", "pjt_content", "pjt_name_abbrev", "pjt_parent_id");
$setvalue = array($pjt_id, $project_name, $s_date, $e_date, $real_e_date, $incharge_id, $public_flag, $project_content, $project_name_abbrev, $project_parent_id);
$cond = "where pjt_id = '$pjt_id'";
$up = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($up == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

// メンバー情報を削除
$sql = "delete from promember";
$cond = "where pjt_id = '$pjt_id'";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

// メンバー情報を作成
$mem_id = 1;
foreach ($arr_target as $row) {
	$sql = "insert into promember (pjt_id, pjt_member_id, emp_id, member_kind) values(";
	$content = array($pjt_id, $mem_id, $row["id"], $row["kind"]);
	$in = insert_into_table($con, $sql, $content, $fname);
	if ($in == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

	$mem_id++;
}

// 文書カテゴリ名を更新
$sql = "update libcate set";
$set = array("lib_cate_nm", "private_flg");
$setvalue = array($project_name, $private_flag);
$cond = "where lib_archive = '4' and lib_link_id = '$pjt_id'";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

//→一旦承認者を削除する
$sql = "delete  from prcdaprv_default ";
$cond = "where pjt_id = '$pjt_id'";
$upd = delete_from_table($con, $sql, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

//→一旦承認者階層の種別を削除する
$sql = "delete  from prcdaprv_mng_default ";
$cond = "where pjt_id = '$pjt_id'";
$upd = delete_from_table($con, $sql, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

//→承認者を登録する
for ($i = 1; $i <= $ovtm_approve_num; $i++)
{

	$str_ap_emp_id = "ovtm_emp_id".$i;
	$str_ap_notice_id = "ovtm_next_notice_div".$i;


	//承認階層の種別を登録
	$sql = "insert into prcdaprv_mng_default (pjt_id, prcdaprv_order, next_notice_div) values(";
	$content = array($pjt_id, $i, $$str_ap_notice_id);
	$in = insert_into_table($con, $sql, $content, $fname);
	if ($in == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

	$arr_app_id = split(",", $$str_ap_emp_id);
	$arr_app = array();
	$set_arr_app = array();

	for ($cnt = 0; $cnt < count($arr_app_id); $cnt++)
	{
		array_push($set_arr_app, array("prcdaprv_emp_id" => $arr_app_id[$cnt]));
	}

	//→承認者を登録する
	$no_cnt = 1;
	foreach ($set_arr_app as $row)
	{
		$sql = "insert into prcdaprv_default (pjt_id, prcdaprv_no, prcdaprv_emp_id, prcdaprv_order) values(";
		$content = array($pjt_id, $no_cnt, $row["prcdaprv_emp_id"], $i);
		$in = insert_into_table($con, $sql, $content, $fname);
		if ($in == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$no_cnt++;
	}

}



// トランザクションをコミット
pg_query($con, "commit");



//内容の確定後にソート順番の振りなおし処理

//移動前の委員会のソートIDを振りなおし;
$sql = "select pjt_id from project ";
$cond = " where pjt_parent_id=".pg_fetch_result($former_sel, 0, "pjt_parent_id")." and pjt_delete_flag = 'f' order by wg_sort_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == "0") {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

$update_sort_id=1;
while ($row = pg_fetch_array($sel))
{
	// ソートIDを更新
	$sql = "update project set";
	$set = array("wg_sort_id");
	$setvalue = array($update_sort_id);
	$cond = " where pjt_id=".$row["pjt_id"];
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$update_sort_id++;
}


//移動後の対象委員会のソートIDを振りなおし
if ($prev_project_parent_id != $project_parent_id) {
	$sql = "select max(wg_sort_id) as max_cnt from project ";
	$cond = " where pjt_parent_id=".$project_parent_id." and pjt_delete_flag = 'f'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == "0") {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

	$set_cnt = pg_fetch_result($sel, 0, "max_cnt") + 1;

	// ソートIDを更新
	$sql = "update project set";
	$set = array("wg_sort_id");
	$setvalue = array($set_cnt);
	$cond = " where pjt_id=".$pjt_id;
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
}
// トランザクションをコミット
pg_query($con, "commit");


// データベース接続を切断
pg_close($con);

if($adm_flg=="t")
{
	// 委員会・WG一覧画面に遷移(管理者権限)
	echo("<script type=\"text/javascript\">location.href = 'project_menu_adm.php?session=$session&pjt_id=$pjt_id';</script>");
}
else
{
	// 委員会・WG一覧画面に遷移(一般ユーザ)
	echo("<script type=\"text/javascript\">location.href = 'project_menu.php?session=$session&pjt_id=$pjt_id';</script>");
}
?>
</body>
