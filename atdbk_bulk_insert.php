<?
require_once("about_comedix.php");
require_once("show_timecard_common.ini");
require_once("atdbk_close_class.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
switch ($wherefrom) {
case "1":  // タイムカード入力画面より
	$auth_id = 5;
	break;
case "2":  // タイムカード修正画面より
	$auth_id = ($mmode != "usr") ? 42 : 5;
	break;
default:
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
	break;
}
$checkauth = check_authority($session, $auth_id, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if ($start_hour == "--") {$start_hour = "";}
if ($start_min == "--") {$start_min = "";}
if ($end_hour == "--") {$end_hour = "";}
if ($end_min == "--") {$end_min = "";}
if ($start_hour == "" && $start_min != "" || $start_hour != "" && $start_min == "") {
	echo("<script type=\"text/javascript\">alert('勤務開始予定時刻を正しく選択してください。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
} else {
	$start_time = $start_hour . $start_min;
	if ($start_time == "") {$start_time = null;}
}
if ($end_hour == "" && $end_min != "" || $end_hour != "" && $end_min == "") {
	echo("<script type=\"text/javascript\">alert('勤務開始終了時刻を正しく選択してください。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
} else {
	$end_time = $end_hour . $end_min;
	if ($end_time == "") {$end_time = null;}
}
if ($pattern == "--" || $pattern == "0") {$pattern = null;}
if ($reason == "--") {$reason = "";}

// データベースに接続
$con = connect2db($fname);

// ************ oose add start *****************
//職員本人による修正可否フラグを取得
$sql = "select * from timecard";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$timecard_modify_flg = pg_fetch_result($sel, 0, "modify_flg");
//現在日付の取得
$wk_date = getdate();
$now_yyyy = $wk_date["year"];
$now_mm = $wk_date["mon"];
$now_dd = $wk_date["mday"];
$now_date = sprintf("%04d%02d%02d", $now_yyyy, $now_mm, $now_dd);
// ************ oose add end *****************

// ************ oose add start *****************
// 職員ID未指定の場合はログインユーザの職員IDを取得
if ($emp_id == "") {
	$sql = "select emp_id from session";
	$cond = "where session_id = '$session'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$emp_id = pg_fetch_result($sel, 0, "emp_id");
}
// 勤務条件テーブルより勤務形態（常勤、非常勤）を取得
$sql = "select duty_form from empcond";
$cond = "where emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$duty_form = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "duty_form") : "";
// ************ oose add end *****************

$sql = "select * from timecard";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
//職員本人による修正可否フラグ
$timecard_modify_flg = pg_fetch_result($sel, 0, "modify_flg");

$atdbk_close_class = new atdbk_close_class($con, $fname);
$closing = $atdbk_close_class->get_empcond_closing($emp_id);
$closing_month_flg = $atdbk_close_class->closing_month_flg;

// 締め日が登録されていない場合はエラーとする
if ($closing == "") {
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('締め日が登録されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// 開始日・締め日を算出
if ($yyyymm != "") {
	$year = substr($yyyymm, 0, 4);
	$month = intval(substr($yyyymm, 4, 2));
	$month_set_flg = true; //月が指定された場合
} else {
	$year = date("Y");
	$month = date("n");
	$yyyymm = $year . date("m");
	$month_set_flg = false; //月が未指定の場合
}
$calced = false;
switch ($closing) {
case "1":  // 1日
	$closing_day = 1;
	break;
case "2":  // 5日
	$closing_day = 5;
	break;
case "3":  // 10日
	$closing_day = 10;
	break;
case "4":  // 15日
	$closing_day = 15;
	break;
case "5":  // 20日
	$closing_day = 20;
	break;
case "6":  // 末日
	$start_year = $year;
	$start_month = $month;
	$start_day = 1;
	$end_year = $start_year;
	$end_month = $start_month;
	$end_day = days_in_month($end_year, $end_month);
	$calced = true;
	break;
}
if (!$calced) {
	$day = date("j");
	$start_day = $closing_day + 1;
	$end_day = $closing_day;

	//月が指定された場合
	if ($month_set_flg) {
		//当月〜翌月
		if ($closing_month_flg != "2") {
			$start_year = $year;
			$start_month = $month;
			
			$end_year = $year;
			$end_month = $month + 1;
			if ($end_month == 13) {
				$end_year++;
				$end_month = 1;
			}
		}
		//前月〜当月
		else {
			$start_year = $year;
			$start_month = $month - 1;
			if ($start_month == 0) {
				$start_year--;
				$start_month = 12;
			}
			
			$end_year = $year;
			$end_month = $month;
		}
		
	} else {
		if ($day <= $closing_day) {
			$start_year = $year;
			$start_month = $month - 1;
			if ($start_month == 0) {
				$start_year--;
				$start_month = 12;
			}
			
			$end_year = $year;
			$end_month = $month;
		} else {
			$start_year = $year;
			$start_month = $month;
			
			$end_year = $year;
			$end_month = $month + 1;
			if ($end_month == 13) {
				$end_year++;
				$end_month = 1;
			}
		}
	}
}
$start_date = sprintf("%04d%02d%02d", $start_year, $start_month, $start_day);
$end_date = sprintf("%04d%02d%02d", $end_year, $end_month, $end_day);
//登録期間対応 20140108
if ($reg_months > 1) {
    $wk_reg_months = $reg_months - 1;
    //月末対応
    if ($closing == "6") {
        $end_date = sprintf("%04d%02d%02d", $end_year, $end_month, "01");
        $end_date = date("Ymt", strtotime("+$wk_reg_months month", to_timestamp($end_date)));
    }
    else {
        $end_date = date("Ymd", strtotime("+$wk_reg_months month", to_timestamp($end_date)));
    }
}

// 対象年月の締め状況を取得
$tmp_yyyymm = substr($start_date, 0, 6);
$sql = "select count(*) from atdbkclose";
$cond = "where emp_id = '$emp_id' and yyyymm = '$tmp_yyyymm'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$atdbk_closed = (pg_fetch_result($sel, 0, 0) > 0);

// 締め済みの場合はエラーとする
if ($atdbk_closed) {
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('締め済みのため登録できません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// ************ oose add start *****************
$data_cnt = -1;
$date = getdate(); //現在日付の取得
$now_yyyy = $date["year"];
$now_mm = $date["mon"];
$now_dd = $date["mday"];
$now_date = sprintf("%04d%02d%02d", $now_yyyy, $now_mm,$now_dd);
// ************ oose add end *****************
// 登録処理
$tmp_date = $start_date;
while ($tmp_date <= $end_date) {
// ************ oose add start *****************
	$data_cnt++;
// ************ oose add end *****************

	// 処理日付が指定条件に該当する場合
// ************ oose update start *****************
	$wk_flg = "";
	if (is_target_date($con, $tmp_date, $condition, $fname)) {
		if (($timecard_modify_flg != "f") || ($now_date < $tmp_date)) {
			$wk_flg = "1";
		}
		if ($inp_chk_flg == "") {
			$wk_flg = "1";
		}
	}
	if ($wk_flg == "1") {
// ************ oose update end *****************

		// 予定レコードをDELETE〜INSERT
		$sql = "delete from atdbk";
		$cond = "where emp_id = '$emp_id' and date = '$tmp_date'";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$sql = "insert into atdbk (emp_id, date, tmcd_group_id, pattern, prov_start_time, prov_end_time, reason) values (";
		$content = array($emp_id, $tmp_date, $tmcd_group_id, $pattern, $start_time, $end_time, $reason);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
//出勤予定表画面からは、実績のレコードを更新しない 20090702
/*
		// 処理日付の勤務実績レコード数を取得
		$sql = "select count(*) from atdbkrslt";
		$cond = "where emp_id = '$emp_id' and date = '$tmp_date'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$rec_count = pg_fetch_result($sel, 0, 0);

// ************ oose add start *****************
		$inp_ng_flg = "";
		// タイムカード入力画面より
		if ($wherefrom == "1") {
			if (($timecard_modify_flg == "f") && ($now_date >= $tmp_date)) {
				$inp_ng_flg = "1";
			}
		}
		if ($inp_ng_flg == "") {
// ************ oose add end *****************
			// 勤務実績レコードがなければ作成、あれば更新
			if ($rec_count == 0) {
				$sql = "insert into atdbkrslt (emp_id, date, tmcd_group_id, pattern) values (";
				$content = array($emp_id, $tmp_date, $tmcd_group_id, $pattern);
				$ins = insert_into_table($con, $sql, $content, $fname);
				if ($ins == 0) {
					pg_close($con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
			} else {
				$sql = "update atdbkrslt set";
				$set = array("tmcd_group_id", "pattern");
				$setvalue = array($tmcd_group_id, $pattern);
				$cond = "where emp_id = '$emp_id' and date = '$tmp_date'";
				$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
				if ($upd == 0) {
					pg_query($con, "rollback");
					pg_close($con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
			}
// ************ oose add start *****************
		}
// ************ oose add end *****************
*/
	}

	$tmp_date = next_date($tmp_date);
}

// データベース接続を閉じる
pg_close($con);

// 親画面をリフレッシュ、自画面を閉じる
switch ($wherefrom) {
case "1":  // タイムカード入力画面より
	echo("<script type=\"text/javascript\">opener.location.href = 'atdbk_register.php?session=$session&yyyymm=$yyyymm'; self.close();</script>");
	break;
case "2":  // タイムカード修正画面より
	$url_srch_name = urlencode($srch_name);
        echo("<script type=\"text/javascript\">opener.location.href = 'work_admin_atdbk.php?session=$session&emp_id=$emp_id&yyyymm=$yyyymm&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&page=$page&group_id=$group_id&shift_group_id=$shift_group_id&csv_layout_id=$csv_layout_id&duty_form_jokin=$duty_form_jokin&duty_form_hijokin=$duty_form_hijokin&srch_id=$srch_id&sus_flg=$sus_flg&mmode=$mmode'; self.close();</script>");
	break;
}

//------------------------------------------------------------------------------
// 関数
//------------------------------------------------------------------------------

// 処理日付が条件に該当するかどうか返す
function is_target_date($con, $date, $condition, $fname) {

	// 種別が指定された場合
	if ($condition >= 1 && $condition <= 5) {
		$sql = "select type from calendar";
		$cond = "where date = '$date'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$tmp_type = pg_fetch_result($sel, 0, "type");

		switch ($condition) {
		case "1": // 通常勤務日
			return ($tmp_type == "1");
		case "2": // 勤務日1
			return ($tmp_type == "2");
		case "3": // 勤務日2
			return ($tmp_type == "3");
		case "4": // 法定休日
			return ($tmp_type == "4");
		case "5": // 所定休日
			return ($tmp_type == "5");
		}

	// 曜日が指定された場合
	} else {
		$wd = date("w", to_timestamp($date));

		switch ($condition) {
		case "6": // 毎週日曜日
			return ($wd == 0);
		case "7": // 毎週月曜日
			return ($wd == 1);
		case "8": // 毎週火曜日
			return ($wd == 2);
		case "9": // 毎週水曜日
			return ($wd == 3);
		case "10": // 毎週木曜日
			return ($wd == 4);
		case "11": // 毎週金曜日
			return ($wd == 5);
		case "12": // 毎週土曜日
			return ($wd == 6);
		case "13": // 毎週月、水、金曜日
			return ($wd == 1 || $wd == 3 || $wd == 5);
		case "14": // 毎週火、木曜日
			return ($wd == 2 || $wd == 4);
		case "15": // 毎週月〜金曜日
			return ($wd == 1 || $wd == 2 || $wd == 3 || $wd == 4 || $wd == 5);
		case "16": // 毎週土〜日曜日
			return ($wd == 6 || $wd == 0);
		}
	}

}
?>
