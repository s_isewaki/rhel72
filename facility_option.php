<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>設備予約 | オプション</title>
<?
require("about_session.php");
require("about_authority.php");
require("show_facility.ini");
require("get_values.ini");
require("facility_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 11, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 施設・設備登録権限を取得
$fcl_admin_auth = check_authority($session, 41, $fname);

// 表示範囲が渡されていない場合は「全て」に設定
if ($range == "") {
	$range = "all";
}

// タイムスタンプが渡されていない場合は当日日付のタイムスタンプを取得
if ($date == "") {
	$date = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
}

// データベースに接続
$con = connect2db($fname);

// オプション情報を取得
$default_info = get_default_info($con, $session, $fname);
$default_val = $default_info["val"];
$default_url = $default_info["url"];
$calendar_start = $default_info["calendar_start"];
$default_cate_id = $default_info["default_cate_id"];

// ログインユーザの職員IDを取得
$emp_id = get_emp_id($con, $session, $fname);

// 参照可能なカテゴリ一覧を取得
$categories = get_referable_categories($con, $emp_id, $fname);
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function setCategoryDisabled() {
	var disabled = !document.mainform.default_val[2].checked;
	document.mainform.default_cate_id.disabled = disabled;
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="setCategoryDisabled();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<? echo($default_url); ?>"><img src="img/icon/b06.gif" width="32" height="32" border="0" alt="設備予約"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<? echo($default_url); ?>"><b>設備予約</b></a></font></td>
<? if ($fcl_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="facility_master_menu.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="95" align="center" bgcolor="#bdd1e7"><a href="facility_menu.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="facility_week.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">週（表）</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="facility_week_chart.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">週（チャート）</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="facility_month.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">月</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="facility_year.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">年</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="facility_bulk_register.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#5279a5"><a href="facility_option.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>オプション</b></font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<form name="mainform" action="facility_option_update_exe.php" method="post">
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td valign="bottom"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>初期画面設定</b></font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="140" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">初期画面</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="default_val" value="1"<? if ($default_val == "1") {echo(" checked");} ?> onclick="setCategoryDisabled();">日
<input type="radio" name="default_val" value="2"<? if ($default_val == "2") {echo(" checked");} ?> onclick="setCategoryDisabled();">週（表）
<input type="radio" name="default_val" value="4"<? if ($default_val == "4") {echo(" checked");} ?> onclick="setCategoryDisabled();">週（チャート）
<input type="radio" name="default_val" value="3"<? if ($default_val == "3") {echo(" checked");} ?> onclick="setCategoryDisabled();">月
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">初期選択カテゴリ</font></td>
<td>
<select name="default_cate_id">
<option value="">（設定なし）
<?
foreach ($categories as $tmp_category_id => $tmp_category) {
	echo("<option value=\"$tmp_category_id\"");
	if ($tmp_category_id == $default_cate_id) {
		echo(" selected");
	}
	echo(">{$tmp_category["name"]}");
}
?>
</select>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="red">※週（チャート）のみ反映されます</font>
</td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td valign="bottom"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>カレンダー設定</b></font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="140" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">週の開始曜日</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="calendar_start" value="1"<? if ($calendar_start == "1") {echo(" checked");} ?>>日曜
<input type="radio" name="calendar_start" value="2"<? if ($calendar_start == "2") {echo(" checked");} ?>>月曜
</font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="更新"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="date" value="<? echo($date); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
