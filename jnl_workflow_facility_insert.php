<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if ($facility_nm == "") {
	echo("<script type=\"text/javascript\">alert('施設名が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if (strlen($facility_nm) > 30) {
	echo("<script type=\"text/javascript\">alert('施設名が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// 施設IDを採番

//一件も存在していなければ１から採番
$sql = "select count(*) from jnl_facility_master";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$data_count = intval(pg_fetch_result($sel, 0, 0));

if($data_count < 1)
{
	//新規採番
	$jnl_facility_id = 1;
}
else
{	
	//最大値を取得して採番

	$sql = "select max(jnl_facility_id) from jnl_facility_master";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$jnl_facility_id = intval(pg_fetch_result($sel, 0, 0)) + 1;
}

// 病院・施設名を登録
$sql = "insert into jnl_facility_master (jnl_facility_id,jnl_facility_name,jnl_facility_type) values (";
$content = array($jnl_facility_id, $facility_nm,$facility_type);
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 親画面をリフレッシュし、自画面を閉じる
echo("<script type=\"text/javascript\">opener.location.href = 'jnl_workflow_register_list.php?session=$session&facility_id=$jnl_facility_id&facility_type=$facility_type';</script>");
echo("<script type=\"text/javascript\">self.close();</script>");
?>
