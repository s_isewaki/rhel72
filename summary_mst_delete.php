<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
//ファイルの読み込み
require("about_postgres.php");
require("about_session.php");
require("about_authority.php");
require("about_validation.php");

//ページ名
$fname = $PHP_SELF;

//セッションチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//権限チェック
$dept = check_authority($session,59,$fname);
if($dept == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//ＤＢのコネクション作成
$con = connect2db($fname);

// トランザクションの開始
pg_query($con, "begin");

// 選択されたマスタをループ
foreach ($mst_cds as $tmp_mst_cd)
{
	//マスタ、及びマスタの項目を削除
	$sql = "delete from tmplitemmst";
	$cond = "where mst_cd = '$tmp_mst_cd'";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$sql = "delete from tmplitem";
	$cond = "where mst_cd = '$tmp_mst_cd'";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	
	if($tmp_mst_cd == $mst_cd)
	{
		$mst_cd = "";
	}
}

// トランザクションのコミット
pg_query($con, "commit");

pg_close($con);

echo("<script type=\"text/javascript\">location.href = 'summary_mst.php?session=$session&mst_cd=$mst_cd';</script>");

?>