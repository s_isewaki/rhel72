<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>マスターメンテナンス | 職種更新</title>
<? require("about_authority.php"); ?>
<? require("about_session.php"); ?>
<? require("maintenance_menu_list.ini"); ?>
<?
// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 職種登録権限チェック
$checkauth = check_authority($session,26,$fname);
if($checkauth == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// DBコネクションの作成
$con = connect2db($fname);

// 職種情報を取得
$sql = "select job_nm, link_key from jobmst";
$cond = "where job_id = '$job_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$job_name = pg_fetch_result($sel, 0, "job_nm");
$link_key = pg_fetch_result($sel, 0, "link_key");
?>
<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="maintenance_menu.php?session=<? echo($session); ?>"><img src="img/icon/b27.gif" width="32" height="32" border="0" alt="マスターメンテナンス"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="./maintenance_menu.php?session=<? echo($session); ?>"><b>マスターメンテナンス</b></a>&nbsp;&gt;&nbsp;<a href="./job_menu.php?session=<? echo($session); ?>"><b>職種</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- left -->
<td width="160" valign="top">
<? write_menu_list($session, $fname) ?>
</td>
<!-- left -->
<!-- center -->
<td width="5"><img src="./img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->
<!-- right -->
<td valign="top">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="./job_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職種一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" height="22" align="center" bgcolor="#5279a5"><a href="./job_update.php?session=<? echo($session); ?>&job_id=<? echo($job_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>職種更新</b></font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form action="job_update_exe.php" method="post">
<table width="400" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td bgcolor="#f6f9ff" width="120" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職種名</font></td>
<td width="280"><input type="text" name="job_name" value="<? echo($job_name); ?>" size="25" maxlength="30" style="ime-mode: active;"></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">外部連携キー</font></td>
<td><input type="text" name="link_key" value="<? echo($link_key); ?>" size="25" style="ime-mode:inactive;"></td>
</tr>
</table>
<table width="400" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="更新"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="job_id" value="<? echo($job_id); ?>">
</form>
</td>
<!-- right -->
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
