<?
require_once("cl_application_workflow_common_class.php");

require_once("get_values.ini");
require_once("common_log_class.inc");
require_once("cl_apl_wf_common_class.php");



$log = new cl_common_log_class(basename(__FILE__));
$log->info(basename(__FILE__)." START");

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 申請画面一覧
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function search_application_list($mdb2,$con, $session, $fname, $wkfw_type,
                                 $wkfw_id, $apply_title, $emp_nm,
                                 $apply_yyyy_from, $apply_mm_from, $apply_dd_from,
                                 $apply_yyyy_to, $apply_mm_to, $apply_dd_to,
                                 $class_yyyy_from, $class_mm_from, $class_dd_from,
                                 $class_yyyy_to, $class_mm_to, $class_dd_to,
                                 $apply_stat, $training_div, $page)
{


	global $log;
	$log->info(__FUNCTION__." START");

	$obj = new cl_application_workflow_common_class($con, $fname);
	$apl_wf_common_obj = new cl_apl_wf_common_class($mdb2, $fname);

    //職員ＩＤ取得
    $arr_emp = $obj->get_empmst($session);
	$emp_id  = $arr_emp["0"]["emp_id"];
	$log->debug("職員ＩＤ取得:".$emp_id,__FILE__,__LINE__);

	$arr_cond = array(
	                 "emp_id" => $emp_id,
	                 "wkfw_type" => $wkfw_type,
	                 "wkfw_id" => $wkfw_id,
	                 "apply_title" => $apply_title,
	                 "emp_nm" => $emp_nm,
	                 "apply_yyyy_from" => $apply_yyyy_from,
	                 "apply_mm_from" => $apply_mm_from,
	                 "apply_dd_from" => $apply_dd_from,
	                 "apply_yyyy_to" => $apply_yyyy_to,
	                 "apply_mm_to" => $apply_mm_to,
	                 "apply_dd_to" => $apply_dd_to,
	                 "class_yyyy_from" => $class_yyyy_from,
	                 "class_mm_from" => $class_mm_from,
	                 "class_dd_from" => $class_dd_from,
	                 "class_yyyy_to" => $class_yyyy_to,
	                 "class_mm_to" => $class_mm_to,
	                 "class_dd_to" => $class_dd_to,
	                 "apply_stat" => $apply_stat,
	                 "training_div" => $training_div
	);

	// 件数取得
	$sql  = "select count(*) as cnt from ( ";
	$sql .= $obj->get_applylist_sql($arr_cond);
	$sql .= ") all_list ";

	$log->debug("件数取得:".$sql,__FILE__,__LINE__);

	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

	$apply_list_count = pg_fetch_result($sel, 0, "cnt");
	$log->debug("apply_list_count:".$apply_list_count,__FILE__,__LINE__);

	// 一覧データ取得
	if($apply_list_count > 0)
	{

		if($page == "")
		{
			$page = 1;
		}

		// 一画面内の最大表示件数
        $disp_max_page = 15;

		// 最大ページ数
		if($apply_list_count == 0)
		{
			$page_max  = 1;
		}
		else
		{
			$page_max  = floor( ($apply_list_count-1) / $disp_max_page ) + 1;
		}

		// 指定ページが最大ページ数を超える場合、最終ページを表示
		if ($page > $page_max) {
			$page = $page_max;
		}

		$offset = ($page - 1) * $disp_max_page;

		$sql  = $obj->get_applylist_sql($arr_cond);

		$cond = "order by apply_date desc, apply_id desc ";
		$cond .= "offset $offset limit $disp_max_page ";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$application_list = pg_fetch_all($sel);

		$count = 1;
		foreach($application_list as $application)
		{

			$apply_type       = $application["apply_type"];
			$apply_id         = $application["apply_id"];
			$apply_title      = $application["apply_title"];
			$apply_date       = $application["apply_date"];
			$apply_stat       = $application["apply_stat"];
			$apply_no         = $application["apply_no"];
			$apv_fix_show_flg = $application["apv_fix_show_flg"];
			$apv_bak_show_flg = $application["apv_bak_show_flg"];
			$apv_ng_show_flg  = $application["apv_ng_show_flg"];
			$wkfw_title       = $application["wkfw_title"];
			$wkfw_nm          = $application["wkfw_nm"];
			$short_wkfw_name  = $application["short_wkfw_name"];
			$wkfw_folder_id   = $application["wkfw_folder_id"];
			$pjt_schd_id      = $application["pjt_schd_id"];
			$training_name    = $application["training_name"];
			// 2012/09/21 Yamagawa add(s)
			$levelup_emp_nm      = $application["levelup_emp_lt_nm"]." ".$application["levelup_emp_ft_nm"];
			$levelup_emp_dept_nm = $application["levelup_emp_dept_nm"];
			// 2012/09/21 Yamagawa add(e)

			if ($obj->is_attendance_apply_template($short_wkfw_name)) {
				$training_theme = $application["training_theme"];
				if ($training_theme == "") {
					$training_theme = preg_replace("/.*<course_theme>(.*?)<\/course_theme>.*/", "$1", str_replace("\n", "", $application["apply_content"]));
				}
				if ($training_theme != "") {
					$training_theme = "　　" . $training_theme;
				}
			} else {
				$training_theme = "";
			}

			switch ($apply_type)
			{
				//====================================================================
				// 通常の申請
				//====================================================================
				case "1":

					$log->debug("通常の申請 START",__FILE__,__LINE__);
					// 申請番号
					$year = substr($apply_date, 0, 4);
					$md   = substr($apply_date, 4, 4);
					if($md >= "0101" and $md <= "0331")
					{
						$year = $year - 1;
					}
					$apply_no = $short_wkfw_name."-".$year."".sprintf("%04d", $apply_no);

					$log->debug("apply_no:".$apply_no,__FILE__,__LINE__);
					// カテゴリ名(フォルダパス)取得
					$folder_path = $wkfw_nm;
					if($wkfw_folder_id != "")
					{
						// フォルダ名
						$folder_list = $obj->get_folder_path($wkfw_folder_id);
						foreach($folder_list as $folder)
						{
							if($folder_path != "")
							{
								$folder_path .= " > ";
							}
							$folder_path .= $folder["name"];
						}
					}

					// 申請日
					$apply_date = preg_replace("/^(\d{4})(\d{2})(\d{2}).*$/", "$1/$2/$3", $apply_date);
					$log->debug('$apply_date:'.$apply_date,__FILE__,__LINE__);

					// 承認者情報取得
					//$arr_applyapv = $obj->get_applyapv_for_applylist($apply_id);
					//$apv_cnt = count($arr_applyapv);
					$arr_applyapv=$apl_wf_common_obj->get_applyapv_for_applylist($apply_id,$emp_id);
					$apv_cnt = count($arr_applyapv);
					//$log->debug('承認者情報取得:'.$arr_applyapv,__FILE__,__LINE__);
					$log->debug('承認者情報取得:'.$apv_cnt,__FILE__,__LINE__);

                    // 承認者全員が未承認かチェック
					$disapv_flg = get_disapv_flg($arr_applyapv);
					$log->debug('承認者全員が未承認かチェック:'.$disapv_flg,__FILE__,__LINE__);

                    // 承認がひとつでもあるかチェック
					$apv_1_flg  = get_apv_flg($arr_applyapv);
					$log->debug('承認がひとつでもあるかチェック:'.$apv_1_flg,__FILE__,__LINE__);

                    // 一部承認、承認確定、否認、差戻し告知
					$apv_bgclr_flg = get_apv_bgclr_flg($arr_applyapv, $apply_stat, $apv_fix_show_flg, $apv_ng_show_flg, $apv_bak_show_flg);
					$log->debug('一部承認、承認確定、否認、差戻し告知:'.$apv_bgclr_flg,__FILE__,__LINE__);

                    // 申請状況
					if($apply_stat == '0')
					{
						// 一部承認
						if($apv_1_flg)
						{
							$apply_stat_nm = '一部承認';
						}
						else
						{
							$apply_stat_nm = '申請中';
						}
					}
					else if($apply_stat == '1')
					{
						$apply_stat_nm = '承認確定';
					}
					else if($apply_stat == '2')
					{
						$apply_stat_nm = '否認';
					}
					else if($apply_stat == '3')
					{
						$apply_stat_nm = '差戻し';
					}
					$log->debug('申請状況:'.$apply_stat_nm,__FILE__,__LINE__);

					for($idx=0; $idx<count($arr_applyapv); $idx++)
					{
						// 承認者所属
						$apv_emp_dept = $arr_applyapv[$idx]["dept_nm"];
						$log->debug('承認者所属:'.$apv_emp_dept,__FILE__,__LINE__);

						// 承認者名
						$apv_name = $arr_applyapv[$idx]["emp_lt_nm"]." ".$arr_applyapv[$idx]["emp_ft_nm"];
						$log->debug('承認者名:'.$apv_name,__FILE__,__LINE__);

						// 承認状況
						$apv_stat = $arr_applyapv[$idx]["apv_stat"];
						if($apv_stat == '0')
						{
							$apv_stat_nm = "未承認";
						}
						else if($apv_stat == '1')
						{
							$apv_stat_nm = "承認";
						}
						else if($apv_stat == '2')
						{
							$apv_stat_nm = "否認";
						}
						else if($apv_stat == '3')
						{
							$apv_stat_nm = "差戻し";
						}
						$log->debug('承認状況:'.$apv_stat_nm,__FILE__,__LINE__);


						// 志願者名取得
						$applicant_name=$obj->get_applicant_name($short_wkfw_name,$emp_id,$application["apply_content"]);
						$log->debug('志願者:'.$applicant_name,__FILE__,__LINE__);

						if($idx == 0)
						{

							if($apv_bgclr_flg)
							{
								echo("<tr bgcolor=\"#ffcccc\">\n");
							}
							else
							{
								echo("<tr>\n");
							}

							// 2012/03/12 Yamagawa del(s) 一括処理復活時に復活させる
							/*
							// apply_statが「3」(差戻し)の場合のみ、再申請を可にする。
							if($apply_stat == '3')
							{
								echo("<td align=\"center\" rowspan=\"".$apv_cnt."\" class=\"apply_list_".$count."\" onmouseover=\"highlightCells(this.className);this.style.cursor='pointer';\" onmouseout=\"dehighlightCells(this.className);this.style.cursor='';\"><input type=\"checkbox\" name=\"re_apply_chk[]\" value=\"".$apply_id."\"></td>\n");
							}
							else
							{
								echo("<td align=\"center\" rowspan=\"".$apv_cnt."\" class=\"apply_list_".$count."\" onmouseover=\"highlightCells(this.className);this.style.cursor='pointer';\" onmouseout=\"dehighlightCells(this.className);this.style.cursor='';\"><input type=\"checkbox\" name=\"re_apply_chk[]\" value=\"".$apply_id."\" disabled></td>\n");
							}

							// 承認者がすべて未承認の場合、申請取消を可にする。
							if($disapv_flg)
							{
								echo("<td align=\"center\" rowspan=\"".$apv_cnt."\" class=\"apply_list_".$count."\" onmouseover=\"highlightCells(this.className);this.style.cursor='pointer';\" onmouseout=\"dehighlightCells(this.className);this.style.cursor='';\"><input type=\"checkbox\" name=\"apply_cancel_chk[]\" value=\"".$apply_id."\"></td>\n");
							}
							else
							{
								echo("<td align=\"center\" rowspan=\"".$apv_cnt."\" class=\"apply_list_".$count."\" onmouseover=\"highlightCells(this.className);this.style.cursor='pointer';\" onmouseout=\"dehighlightCells(this.className);this.style.cursor='';\"><input type=\"checkbox\" name=\"apply_cancel_chk[]\" value=\"".$apply_id."\" disabled></td>\n");
							}
							*/
							// 2012/03/12 Yamagawa del(e) 一括処理復活時に復活させる

							echo("<td rowspan=\"".$apv_cnt."\" class=\"apply_list_".$count."\" onmouseover=\"highlightCells(this.className);this.style.cursor='pointer';\" onmouseout=\"dehighlightCells(this.className);this.style.cursor = '';\" onclick=\"window.open('cl_application_apply_detail.php?session=$session&apply_id=$apply_id&mode=show_flg_update', 'newwin', 'left=0, top=0, width=980,height=700,scrollbars=yes');\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".htmlspecialchars($apply_no)."</td>\n");
							echo("<td rowspan=\"".$apv_cnt."\" class=\"apply_list_".$count."\" onmouseover=\"highlightCells(this.className);this.style.cursor='pointer';\" onmouseout=\"dehighlightCells(this.className);this.style.cursor = '';\" onclick=\"window.open('cl_application_apply_detail.php?session=$session&apply_id=$apply_id&mode=show_flg_update', 'newwin', 'left=0, top=0, width=980,height=700,scrollbars=yes');\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".htmlspecialchars($wkfw_title.$training_theme)."</td>\n");
							echo("<td rowspan=\"".$apv_cnt."\" class=\"apply_list_".$count."\" onmouseover=\"highlightCells(this.className);this.style.cursor='pointer';\" onmouseout=\"dehighlightCells(this.className);this.style.cursor = '';\" onclick=\"window.open('cl_application_apply_detail.php?session=$session&apply_id=$apply_id&mode=show_flg_update', 'newwin', 'left=0, top=0, width=980,height=700,scrollbars=yes');\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".htmlspecialchars($training_name)."</td>\n");
							// 2012/09/21 Yamagawa add(s)
							echo("<td rowspan=\"".$apv_cnt."\" align=\"center\" class=\"apply_list_".$count."\" onmouseover=\"highlightCells(this.className);this.style.cursor='pointer';\" onmouseout=\"dehighlightCells(this.className);this.style.cursor = '';\" onclick=\"window.open('cl_application_apply_detail.php?session=$session&apply_id=$apply_id&mode=show_flg_update', 'newwin', 'left=0, top=0, width=980,height=700,scrollbars=yes');\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".htmlspecialchars($levelup_emp_nm)."</td>\n");
							echo("<td rowspan=\"".$apv_cnt."\" align=\"center\" class=\"apply_list_".$count."\" onmouseover=\"highlightCells(this.className);this.style.cursor='pointer';\" onmouseout=\"dehighlightCells(this.className);this.style.cursor = '';\" onclick=\"window.open('cl_application_apply_detail.php?session=$session&apply_id=$apply_id&mode=show_flg_update', 'newwin', 'left=0, top=0, width=980,height=700,scrollbars=yes');\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".htmlspecialchars($levelup_emp_dept_nm)."</td>\n");
							// 2012/09/21 Yamagawa add(e)
							echo("<td align=\"center\" class=\"apply_list_".$count."\" onmouseover=\"highlightCells(this.className);this.style.cursor='pointer';\" onmouseout=\"dehighlightCells(this.className);this.style.cursor = '';\" onclick=\"window.open('cl_application_apply_detail.php?session=$session&apply_id=$apply_id&mode=show_flg_update', 'newwin', 'left=0, top=0, width=980,height=700,scrollbars=yes');\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".htmlspecialchars($apv_name)."</td>\n");
							echo("<td align=\"center\" class=\"apply_list_".$count."\" onmouseover=\"highlightCells(this.className);this.style.cursor='pointer';\" onmouseout=\"dehighlightCells(this.className);this.style.cursor = '';\" onclick=\"window.open('cl_application_apply_detail.php?session=$session&apply_id=$apply_id&mode=show_flg_update', 'newwin', 'left=0, top=0, width=980,height=700,scrollbars=yes');\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".htmlspecialchars($apv_emp_dept)."</td>\n");
							echo("<td align=\"center\" class=\"apply_list_".$count."\" onmouseover=\"highlightCells(this.className);this.style.cursor='pointer';\" onmouseout=\"dehighlightCells(this.className);this.style.cursor = '';\" onclick=\"window.open('cl_application_apply_detail.php?session=$session&apply_id=$apply_id&mode=show_flg_update', 'newwin', 'left=0, top=0, width=980,height=700,scrollbars=yes');\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".$apv_stat_nm."</td>\n");
							echo("<td align=\"center\" rowspan=\"".$apv_cnt."\" class=\"apply_list_".$count."\" onmouseover=\"highlightCells(this.className);this.style.cursor='pointer';\" onmouseout=\"dehighlightCells(this.className);this.style.cursor = '';\" onclick=\"window.open('cl_application_apply_detail.php?session=$session&apply_id=$apply_id&mode=show_flg_update', 'newwin', 'left=0, top=0, width=980,height=700,scrollbars=yes');\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".$apply_date."</td>\n");
							echo("<td align=\"center\" rowspan=\"".$apv_cnt."\" class=\"apply_list_".$count."\" onmouseover=\"highlightCells(this.className);this.style.cursor='pointer';\" onmouseout=\"dehighlightCells(this.className);this.style.cursor = '';\" onclick=\"window.open('cl_application_apply_detail.php?session=$session&apply_id=$apply_id&mode=show_flg_update', 'newwin', 'left=0, top=0, width=980,height=700,scrollbars=yes');\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".$apply_stat_nm."</td>\n");
							echo("</tr>\n");
						}
						else
						{
							if($apv_bgclr_flg)
							{
								echo("<tr bgcolor=\"#ffcccc\">\n");
							}
							else
							{
								echo("<tr>\n");
							}
							echo("<td align=\"center\" class=\"apply_list_".$count."\" onmouseover=\"highlightCells(this.className);this.style.cursor='pointer';\" onmouseout=\"dehighlightCells(this.className);this.style.cursor='';\" onclick=\"window.open('cl_application_apply_detail.php?session=$session&apply_id=$apply_id&mode=show_flg_update', 'newwin', 'left=0, top=0, width=980,height=700,scrollbars=yes');\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".htmlspecialchars($apv_name)."</td>\n");
							echo("<td align=\"center\" class=\"apply_list_".$count."\" onmouseover=\"highlightCells(this.className);this.style.cursor='pointer';\" onmouseout=\"dehighlightCells(this.className);this.style.cursor='';\" onclick=\"window.open('cl_application_apply_detail.php?session=$session&apply_id=$apply_id&mode=show_flg_update', 'newwin', 'left=0, top=0, width=980,height=700,scrollbars=yes');\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".htmlspecialchars($apv_emp_dept)."</td>\n");
							echo("<td align=\"center\" class=\"apply_list_".$count."\" onmouseover=\"highlightCells(this.className);this.style.cursor='pointer';\" onmouseout=\"dehighlightCells(this.className);this.style.cursor='';\" onclick=\"window.open('cl_application_apply_detail.php?session=$session&apply_id=$apply_id&mode=show_flg_update', 'newwin', 'left=0, top=0, width=980,height=700,scrollbars=yes');\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".$apv_stat_nm."</td>\n");
							echo("</tr>\n");
						}
					}
					$log->debug("通常の申請 END",__FILE__,__LINE__);
					break;

            }
			$count++;
		}

		$log->debug("show_page_area() START",__FILE__,__LINE__);
		show_page_area($page_max,$page);
		$log->debug("show_page_area() END",__FILE__,__LINE__);

	}
	$log->info(__FUNCTION__." END");
	}

// 承認者全員が未承認かチェック(通常申請用)
function get_disapv_flg($arr)
{
	for($i=0; $i<count($arr); $i++)
	{
		if($arr[$i]["apv_stat"] != "0")
		{
			return false;
		}
	}
	return true;
}

// 承認があるかチェック(通常申請用)
function get_apv_flg($arr)
{
	for($i=0; $i<count($arr); $i++)
	{
		if($arr[$i]["apv_stat"] == "1")
		{
			return true;
		}
	}
	return false;
}


// 一部承認、承認確定、否認、差戻し告知(通常申請用)
function get_apv_bgclr_flg($arr, $apply_stat, $apv_fix_show_flg, $apv_ng_show_flg, $apv_bak_show_flg)
{

	global $log;
	$log->info(__FUNCTION__." START");

	$log->debug('$apply_stat:'.$apply_stat,__FILE__,__LINE__);

	switch($apply_stat)
	{
		case "0":
			$log->debug('case "0"',__FILE__,__LINE__);
			for($i=0; $i<count($arr); $i++)
			{
				if($arr[$i]["apv_stat"] == "1" && $arr[$i]["apv_fix_show_flg"] == "t")
				{
					$log->info(__FUNCTION__." END");
					return true;
				}
			}
			break;
		case "1":
			$log->debug('case "1"',__FILE__,__LINE__);
			if($apv_fix_show_flg == 't')
			{
				$log->info(__FUNCTION__." END");
				return true;
			}
			break;
		case "2":
			$log->debug('case "2"',__FILE__,__LINE__);
			if($apv_ng_show_flg == 't')
			{
				$log->info(__FUNCTION__." END");
				return true;
			}
			break;
		case "3":
			$log->debug('case "3"',__FILE__,__LINE__);
			if($apv_bak_show_flg == 't')
			{
				$log->info(__FUNCTION__." END");
				return true;
			}
			break;
	}
	$log->info(__FUNCTION__." END");
	return false;
}

// 承認があるかチェック(議事録公開申請)
function get_prcdaprv_flg($arr)
{
	for($i=0; $i<count($arr); $i++)
	{
		if($arr[$i]["prcdaprv_date"] != "")
		{
			return true;
		}
	}
	return false;
}

// 一部承認、承認確定告知(議事録公開申請)
function get_prcdaprv_bgclr_flg($arr, $apply_stat, $apv_fix_show_flg)
{
	if($apply_stat == "1")
	{
		for($i=0; $i<count($arr); $i++)
		{
			if($arr[$i]["prcdaprv_date"] != "" && $arr[$i]["apv_fix_show_flg"] == "t")
			{
				return true;
			}
		}
	}
	else if($apply_stat == "2")
	{
		if($apv_fix_show_flg == "t")
		{
			return true;
		}
	}
	return false;
}


function show_page_area($page_max,$page)
{
	?>
	<input type="hidden" name="page" value="">
	<script type="text/javascript">

	//ページ遷移します。
	function page_change(page)
	{
		document.apply.page.value=page;
		document.apply.submit();
	}

	</script>

	<?
	if($page_max != 1)
	{
	?>
		<table width="100%">
		<tr>
		<td>
		  <table>
		  <tr>
		  <td>
		<?


		if($page == 1)
		{
		?>
		    <nobr>
		    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
		    <span style="color:silver">
		    先頭へ
		    </span>
		    </font>
		    </nobr>
		<?
		}
		else
		{
		?>
		    <nobr>
		    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
		    <a href="javascript:page_change(1);">
		    先頭へ
		    </a>
		    </font>
		    </nobr>
		<?
		}


		?>
		  </td>
		  <td>
		<?


		if($page == 1)
		{
		?>
		    <nobr>
		    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
		    <span style="color:silver">
		    前へ
		    </span>
		    </font>
		    </nobr>
		<?
		}
		else
		{
		?>
		    <nobr>
		    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
		    <a href="javascript:page_change(<?=$page-1?>);">
		    前へ
		    </a>
		    </font>
		    </nobr>
		<?
		}


		?>
		  </td>
		  <td>
		    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
		<?

		for($i=1;$i<=$page_max;$i++)
		{
			if($i == $page)
			{
		?>
		    [<?=$i?>]
		<?
			}
			else
			{
		?>
		    <a href="javascript:page_change(<?=$i?>);">[<?=$i?>]</a>
		<?
			}
		}
		?>
		    </font>
		  </td>
		  <td>
		<?


		if($page == $page_max)
		{
		?>
		    <nobr>
		    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
		    <span style="color:silver">
		    次へ
		    </span>
		    </font>
		    </nobr>
		<?
		}
		else
		{
		?>
		    <nobr>
		    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
		    <a href="javascript:page_change(<?=$page+1?>);">
		    次へ
		    </a>
		    </font>
		    </nobr>
		<?
		}
		?>
		  </td>
		  </tr>
		  </table>
		</td>
		</tr>
		</table>
	<?
	}
	?>
	<script type="text/javascript">
		// 表示中のページ番号を保持する
		document.apply.page.value=<?=$page?>;
	</script>
	<?
}
$log->info(basename(__FILE__)." END");
?>

