<?php
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");
require_once("hiyari_common.ini");
require_once("hiyari_reporting_common.ini");
require_once("hiyari_mail.ini");
require_once("hiyari_db_class.php");
require_once("hiyari_auth_class.php");
require_once("hiyari_report_class.php");

//==============================
//初期処理
//==============================
$font = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// 権限チェック
$summary = check_authority($session, 47, $fname);
if ($summary == "0") {
    showLoginPage();
    exit;
}

//テスト用。
//TODO リリース時はコメントアウト必須。
//ini_set("display_errors","1");

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// DBクラスのインスタンス化
$db = new hiyari_db_class($con, $fname);

// レポートクラスのインスタンス化
$rep_obj = new hiyari_report_class($con, $fname);

//
$auth_obj = new hiyari_auth_class($con, $fname, $session);

if(!empty($a_id)) $analysis_id = $a_id;

$sql = "SELECT * FROM inci_analysis_regist WHERE analysis_id = ".$analysis_id;
$db->query($sql);
$analysis_data = $db->getRow();

// 分析事案からコピーするためのデータを取得
$report_data = $rep_obj->get_report($analysis_data['analysis_problem_id']);
$vals = $report_data['content']['input_items'];
//$vals = $rep_obj->get_easy_item_names_value($vals);
//print_r($vals);

if($_POST['is_postback'] == 'true') {
    // 添付ファイルの確認
    create_analysisfile_folder();
    $analysisfile_folder_name = get_analysisfile_folder_name();
    for ($i = 0; $i < count($filename); $i++) {
        $tmp_file_id = $file_id[$i];
        $ext = strrchr($filename[$i], ".");

        $tmp_filename = "{$analysisfile_folder_name}/tmp/{$session}_{$tmp_file_id}{$ext}";
        if (!is_file($tmp_filename)) {
            echo("<script language=\"javascript\">alert('添付ファイルが削除された可能性があります。\\n再度添付してください。');</script>");
            echo("<script language=\"javascript\">location.href='hiyari_analysis_general.php?session={$session}&analysis_id={$analysis_id}'</script>");
            exit;
        }
    }

    $sql = "SELECT count(*) FROM inci_analysis_general WHERE analysis_id = ".$analysis_id;
    $db->query($sql);
    $count = $db->getOne();

    if($count == 0) {
        $sql = "INSERT INTO inci_analysis_general(analysis_id, analysis_file, analysis_purpose, analysis_passage, analysis_factor, analysis_improvement) VALUES(";
        $content = array($analysis_id, $analysis_file, $analysis_purpose, $analysis_passage, $analysis_factor, $analysis_improvement);
        $db->insert($sql, $content, true);
    } else {
        $sql = "UPDATE inci_analysis_general SET ";
        $set = array('analysis_file', 'analysis_purpose', 'analysis_passage', 'analysis_factor', 'analysis_improvement');
        $dat = array($analysis_file, $analysis_purpose, $analysis_passage, $analysis_factor, $analysis_improvement);
        $cond = "WHERE analysis_id = {$analysis_id}";
        $db->update($sql, $set, $dat, $cond, true);
    }

    $sql = "UPDATE inci_analysis_regist SET ";
    $set = array('analysis_summary');
    $dat = array($analysis_summary);
    $cond = "WHERE analysis_id = {$analysis_id}";
    $db->update($sql, $set, $dat, $cond, true);

    // 添付ファイル情報を削除
    $sql = "delete from inci_analysisfile";
    $cond = "where analysis_id = '$analysis_id'";
    $del = delete_from_table($con, $sql, $cond, $fname);
    if ($del == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    // 添付ファイル情報を作成
    $no = 1;
    foreach ($filename as $tmp_filename) {
        $sql = "insert into inci_analysisfile (analysis_id, analysisfile_no, analysisfile_name) values (";
        $content = array($analysis_id, $no, $tmp_filename);
        $ins = insert_into_table($con, $sql, $content, $fname);
        if ($ins == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $no++;
    }


    // 添付ファイルの移動
    foreach (glob("{$analysisfile_folder_name}/{$analysis_id}_*.*") as $tmpfile) {
        unlink($tmpfile);
    }
    for ($i = 0; $i < count($filename); $i++) {
        $tmp_file_id = $file_id[$i];
        $tmp_filename = $filename[$i];
        $tmp_fileno = $i + 1;
        $ext = strrchr($tmp_filename, ".");

        $tmp_filename = "{$analysisfile_folder_name}/tmp/{$session}_{$tmp_file_id}{$ext}";
        copy($tmp_filename, "{$analysisfile_folder_name}/{$analysis_id}_{$tmp_fileno}{$ext}");
    }
    foreach (glob("{$analysisfile_folder_name}/tmp/{$session}_*.*") as $tmpfile) {
        unlink($tmpfile);
    }



} else {
    $sql = "SELECT * FROM inci_analysis_general WHERE analysis_id = ".$analysis_id;
    $db->query($sql);
    $general_data = $db->getRow();

    $analysis_summary     = $analysis_data['analysis_summary'];
    $analysis_file        = $general_data['analysis_file'];
    $analysis_purpose     = $general_data['analysis_purpose'];
    $analysis_passage     = $general_data['analysis_passage'];
    $analysis_factor      = $general_data['analysis_factor'];
    $analysis_improvement = $general_data['analysis_improvement'];

}

create_analysisfile_folder();
$analysisfile_folder_name = get_analysisfile_folder_name();

// 添付ファイル情報を取得
$sql = "select analysisfile_no, analysisfile_name from inci_analysisfile";
$cond = "where analysis_id = $analysis_id order by analysisfile_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showErrorPage(window);</script>");
    exit;
}
$file_id = array();
$filename = array();
while ($row = pg_fetch_array($sel)) {
    $tmp_file_no = $row["analysisfile_no"];
    $tmp_filename = $row["analysisfile_name"];

    array_push($file_id, $tmp_file_no);
    array_push($filename, $tmp_filename);

    // 一時フォルダにコピー
    $ext = strrchr($tmp_filename, ".");
    copy("{$analysisfile_folder_name}/{$analysis_id}_{$tmp_file_no}{$ext}", "{$analysisfile_folder_name}/tmp/{$session}_{$tmp_file_no}{$ext}");
}

// member_idで編集可能かcheckする
$emp_id  = get_emp_id($con,$session,$fname);

$sql = "SELECT analysis_member_id FROM inci_analysis_regist WHERE analysis_id = " . $analysis_id;
$db->query($sql);
$analysis_member_id = $db->getOne();

$arr_member_id = explode(",", $analysis_member_id);
$update_flg = false;
foreach($arr_member_id as $member_id) {
    if($member_id == $emp_id) $update_flg = true;
}

if($update_flg == false) $update_flg = is_sm_emp($session,$fname,$con);

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ファントルくん | 出来事分析</title>
</head>
<script type="text/javascript" src="js/fontsize.js"></script>
<style type="text/css">
form {margin:0;}
table.list {border-collapse:collapse;}
table.list td {border:solid #35B341 1px;}
</style>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onLoad="bodyResized();loadAction();" onResize="bodyResized()">
<script>

function loadAction() {
    start_auto_session_update();
}

function start_auto_session_update()
{
    //1分(60*1000ms)後に開始
    setTimeout(auto_sesson_update,60000);
}
function auto_sesson_update()
{
    //セッション更新
    document.session_update_form.submit();

    //1分(60*1000ms)後に最呼び出し
    setTimeout(auto_sesson_update,60000);
}

function getClientHeight(){
    if (document.body.clientHeight) return document.body.clientHeight;
    if (window.innerHeight) return window.innerHeight;
    if (document.documentElement && document.documentElement.clientHeight) {
        return document.documentElement.clientHeight;
    }
    return 0;
}

var TOP_TO_LEFT_CONTENT_HEIGHT = 160;
var TOP_TO_RIGHT_CONTENT_HEIGHT = 160 + 32;
function bodyResized(){
    var h = getClientHeight();
    if (h < 1) return;
    document.getElementById("left_scroll_div").style.height = (h - TOP_TO_LEFT_CONTENT_HEIGHT) + "px";
    document.getElementById("right_scroll_div").style.height = (h - TOP_TO_RIGHT_CONTENT_HEIGHT) + "px";

}


function attachFile()
{
    window.open('hiyari_analysis_attach.php?session=<?=$session?>', 'newwin', 'width=640,height=480,scrollbars=yes');
}
function detachFile(e)
{
    if (e == undefined)
    {
        e = window.event;
    }

    var btn_id;
    if (e.target)
    {
        btn_id = e.target.getAttribute('id');
    }
    else
    {
        btn_id = e.srcElement.id;
    }
    var id = btn_id.replace('btn_', '');

    var p = document.getElementById('p_' + id);
    document.getElementById('attach').removeChild(p);
}
function analysisPrint() {
    var url = 'hiyari_analysis_print.php?session=<?=$session?>&analysis_id=<?=$analysis_id?>&method=general';

    window.open(url);
}

function copyProblemData() {
    <? if(!$auth_obj->get_analysis_disp_auth($analysis_data['analysis_problem_id']) && !$update_flg) { ?>
        alert('権限がないためコピーできません');
        return;
    <? } ?>

    document.getElementById("analysis_purpose").value     = "<?=ereg_replace("[\r\n]+", '\n', $vals[500][10][0])?>";
    document.getElementById("analysis_passage").value     = "<?=ereg_replace("[\r\n]+", '\n', $vals[530][40][0])?>";
    document.getElementById("analysis_factor").value      = "<?=ereg_replace("[\r\n]+", '\n', $vals[610][20][0])?>";
    document.getElementById("analysis_improvement").value = "<?=ereg_replace("[\r\n]+", '\n', $vals[620][30][0])?>";
}
</script>

<form name="session_update_form" action="hiyari_session_update.php" method="post" target="session_update_frame">
<input type="hidden" name="session" value="<?=$session?>">
</form>
<iframe name="session_update_frame" width="0" height="0" frameborder="0"></iframe>

<!-- ヘッダー START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr height="32" bgcolor="#35B341">
    <td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>汎用分析画面</b></font></td>
    <td>&nbsp</td>
    <td width="10">&nbsp</td>
    <td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
    </tr>
</table>

<img src="img/spacer.gif" width="10" height="10" alt=""><br>

<!-- ヘッダー END -->




<form name="form_list" action="hiyari_analysis_general.php" method="POST" enctype="multipart/form-data">

<!-- 実行アイコン・分析事業コード -->
<div style="margin-top:20px">

    <?if($update_flg == true) { ?>
    <div style="float:left; width:50px; padding-left:8px; text-align:center">
        <img src="img/hiyari_rp_shitagaki.gif" style="cursor: pointer;" onclick="javascript:document.form_list.submit();alert('保存しました');"><br/>
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">更新</font>
    </div>
    <?}?>


    <!-- 実行アイコン -->
    <div style="float:left; width:50px; padding-left:8px; text-align:center">
        <img src="img/hiyari_rp_print.gif" height="30" style="cursor: pointer;" onclick="javascript:analysisPrint();"><br/>
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">印刷</font>
    </div>


    <!-- 分析事業コード -->
    <div style="float:right; width:220px; padding-right:8px; padding-top:4px">
        <table border="0" cellspacing="0" cellpadding="2" class="list" style="width:100%">
            <tr>
                <td bgcolor="#DFFFDC" style="width64px; text-align:center"><?=$font?>分析番号</font></td>
                <td style="text-align:center; padding:8px"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><?=$analysis_data['analysis_no']?></font></td>
            </tr>
        </table>
    </div>


    <div style="clear:both"></div>

</div>


<!-- メインコンテンツ左右 開始 -->
<table style="width:100%"><tr>

<!-- メインコンテンツ左 -->
<td style="padding:4px; padding-left:8px; vertical-align:top">

    <div><span style="border:1px solid #ff6a55; color:#000; background-color:#e4756b; padding:1px 6px;">
        <?=$font?><span style="color:#fff">報 告 内 容</span></font></span>
        <?=$font?>事案番号</font>
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><?=$analysis_data['analysis_problem']?></font>
    </div>

    <!-- 報告内容 START -->
    <div id="left_scroll_div" style="height:590px; overflow:scroll; border:#CCCCCC solid 1px; padding:6px; margin-top:3px; background-color:#eee;">

        <?
            $contents = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="red">権限がないため表示できません</font>';
            if($auth_obj->get_analysis_disp_auth($analysis_data['analysis_problem_id']) || $update_flg) {
                $contents = get_report_html($con,$fname,$analysis_data['analysis_problem_id']);
            } else if($analysis_data['analysis_problem_id'] == "") {
                $contents = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="red">分析事案を選択してください</font>';
            }
            echo wrapYellowNarrowBox($contents);
        ?>
        <!-- 報告内容内容 END -->


    </div>

</td>
<!-- メインコンテンツ右側 -->
<td style="padding:4px; width:700px">


    <div><span style="border:1px solid #ff6a55; color:#000; background-color:#e4756b; padding:1px 6px;">
        <?=$font?><span style="color:#fff">汎 用 分 析</span></font></span>
    </div>


    <div style="margin-top:3px; height:22px;">
        <table border="0" cellspacing="0" width="100%" class="list">
            <tr>
                <td bgcolor="#DFFFDC" align="center" width="108px"><?=$font?>タイトル</font></td>
                <td bgcolor="#efffee" style="padding-left:4px"><?=$analysis_data['analysis_title']?></td>
            </tr>
        </table>
    </div>


    <!-- スクロール -->
    <div id="right_scroll_div" style="height:558px; overflow:scroll; border:#CCCCCC solid 1px; padding:6px; margin-top:10px; background-color:#eee">


        <table border="0" cellspacing="0" cellpadding="2" width="100%" class="list" style="background-color:#fff">
            <tr>
                <td bgcolor="#DFFFDC" align="center" width="100px"><?=$font?>概要</font></td>
                <td><textarea style="width:100%;" rows="5" name="analysis_summary"><?=$analysis_summary?></textarea></td>
            </tr>
            <tr>
                <td bgcolor="#DFFFDC" align="center"><?=$font?>添付ファイル</font></td>
                <td align="right" bgcolor="#DFFFDC">
                        <input type="button" value="ファイル選択" onClick="attachFile()">
                    <div id="attach">
                    <?
                    for ($i = 0; $i < count($filename); $i++)
                    {
                        $tmp_file_id = $file_id[$i];
                        $tmp_filename = $filename[$i];
                        $ext = strrchr($tmp_filename, ".");

                        ?>
                        <p id="p_<?=$tmp_file_id?>" class="attach">
                        <a href="<?=$analysisfile_folder_name?>/tmp/<?=$session?>_<?=$tmp_file_id?><?=$ext?>" target="_blank"><?=$tmp_filename?></a>
                        <input type="button" id="btn_<?=$tmp_file_id?>" name="btn_<?=$tmp_file_id?>" value="削除" onclick="detachFile(event);">
                        <input type="hidden" name="filename[]" value="<?=$tmp_filename?>">
                        <input type="hidden" name="file_id[]" value="<?=$tmp_file_id?>">
                        </p>
                        <?
                    }
                    ?>
                    </div>
                </td>
            </tr>
            <tr>
                <td bgcolor="#DFFFDC" align="center" width="100px"><?=$font?>分析事案から<br>コピー</font></td>
                <td bgcolor="#DFFFDC" align="right"><input type="button" value="コピー" onClick="copyProblemData();"></td>
            </tr>
        </table>

        <table border="0" cellspacing="0" cellpadding="2" width="100%" class="list" style="background-color:#fff; margin-top:20px">
            <tr>
                <td width="50%" bgcolor="#DFFFDC" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">実施した医療行為の目的</font></td>
                <td width="50%" bgcolor="#DFFFDC" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">インシデント発生時（発見時）の経過</font></td>
            </tr>
            <tr>
                <td><textarea id="analysis_purpose" style="width:100%;" rows="8" name="analysis_purpose"><?=$analysis_purpose?></textarea></td>
                <td><textarea id="analysis_passage" style="width:100%;" rows="8" name="analysis_passage"><?=$analysis_passage?></textarea></td>
            </tr>
            <tr>
                <td bgcolor="#DFFFDC" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">インシデントの背景・要因</font></td>
                <td bgcolor="#DFFFDC" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">改善策（部内処置指示及び再発防止策）</font></td>
            </tr>
            <tr>
                <td><textarea id="analysis_factor" style="width:100%;" rows="8" name="analysis_factor"><?=$analysis_factor?></textarea></td>
                <td><textarea id="analysis_improvement" style="width:100%;" rows="8" name="analysis_improvement"><?=$analysis_improvement?></textarea></td>
            </tr>
        </table>
    <input type="hidden" name="session" value="<?=$session?>">
    <input type="hidden" name="is_postback" value="true">
    <input type="hidden" name="analysis_id" value="<?=$analysis_id?>">
    </div>
</tr>
</td>
</table>
</form>
</body>
</html>
<? function wrapYellowNarrowBox($contents_html){ ?>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
            <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
            <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
        </tr>

        <tr>
            <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
            <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td valign="top" style="background-color:#f5ffe5"><?=$contents_html?></td>
                    </tr>
                </table>
            </td>
            <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
        </tr>

        <tr>
            <td><img src="img/r_3.gif" width="10" height="10"></td>
            <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
            <td><img src="img/r_4.gif" width="10" height="10"></td>
        </tr>
    </table>
<? } ?>
