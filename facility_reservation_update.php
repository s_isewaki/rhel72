<?php
require_once("about_comedix.php");
require_once("show_select_values.ini");
require_once("facility_common.ini");
require_once("show_facility.ini");
require_once("label_by_profile_type.ini");
require_once("yui_calendar_util.ini");
require_once("schedule_repeat_common.php");
require_once("mygroup_common.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 設備予約権限のチェック
$checkauth = check_authority($session, 11, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// データベースに接続
$con = connect2db($fname);

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 予約情報を取得
$table_name = ($timeless != "t") ? "reservation" : "reservation2";
$sql = "select $table_name.*, (select emp_lt_nm || ' ' || emp_ft_nm from empmst where empmst.emp_id = $table_name.emp_id) as reg_emp_name, (select emp_lt_nm || ' ' || emp_ft_nm from empmst where empmst.emp_id = $table_name.upd_emp_id) as upd_emp_name, $table_name.emp_ext from $table_name";
$cond = "where reservation_id = $rsv_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$reg_emp_name = pg_fetch_result($sel, 0, "reg_emp_name");
$upd_emp_name = pg_fetch_result($sel, 0, "upd_emp_name");
if ($back != "t") {
    $cate_id = pg_fetch_result($sel, 0, "fclcate_id");
    $facility_id = pg_fetch_result($sel, 0, "facility_id");

    $ymd = pg_fetch_result($sel, 0, "date");
    $year = substr($ymd, 0, 4);
    $month = substr($ymd, 4, 2);
    $day = substr($ymd, 6, 2);

    $s_time = pg_fetch_result($sel, 0, "start_time");
    $s_hour = substr($s_time, 0, 2);
    $s_minute = substr($s_time, 2, 2);

    $e_time = pg_fetch_result($sel, 0, "end_time");
    $e_hour = substr($e_time, 0, 2);
    $e_minute = substr($e_time, 2, 2);

    $title = pg_fetch_result($sel, 0, "title");
    $content = pg_fetch_result($sel, 0, "content");
    $users = pg_fetch_result($sel, 0, "users");

    $type_ids = array();
    $type_ids[] = pg_fetch_result($sel, 0, "type1");
    $type_ids[] = pg_fetch_result($sel, 0, "type2");
    $type_ids[] = pg_fetch_result($sel, 0, "type3");

    $marker = pg_fetch_result($sel, 0, "marker");

    $repeat_flg = pg_fetch_result($sel, 0, "repeat_flg");
    $sync_flg = pg_fetch_result($sel, 0, "sync_flg");
    $schd_flg = pg_fetch_result($sel, 0, "schd_flg");
    $reg_time = pg_fetch_result($sel, 0, "reg_time");
    $fclhist_id = pg_fetch_result($sel, 0, "fclhist_id");
    $reg_emp_id = pg_fetch_result($sel, 0, "emp_id");
    $emp_ext = pg_fetch_result($sel, 0, "emp_ext");
}

// ログインユーザの職員情報を取得
$sql = "select emp_id, emp_lt_nm, emp_ft_nm from empmst";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
}
$login_emp_id = pg_fetch_result($sel, 0, "emp_id");
$login_emp_name = pg_fetch_result($sel, 0, "emp_lt_nm") . pg_fetch_result($sel, 0, "emp_ft_nm");

// 予約／管理可能なカテゴリ・設備一覧を配列で取得
$categories = get_reservable_categories($con, $login_emp_id, $fname);

// 管理者フラグ確認
$admin_flg = $categories[$cate_id]["facilities"][$facility_id]["admin_flg"];

// 登録者、管理者は登録対象者を更新可能とする
if ($login_emp_id == $reg_emp_id || $admin_flg == true) {
    $reg_emp_flg = "t";
} else {
    $reg_emp_flg = "f";
}

// 種別一覧を取得
$sql = "select * from rsvtype";
$cond = "order by type_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$arr_type = array();
while ($row = pg_fetch_array($sel)) {
    $arr_type[$row["type_id"]] = $row["type_name"];
}

// 初期表示時は予約患者情報を取得
if ($back != "t") {
    // 患者管理機能と連携するカテゴリの場合
    if ($categories[$cate_id]["pt_flg"] == "t") {
        $table_name = ($timeless != "t") ? "rsvpt" : "rsvpt2";
        $sql = "select $table_name.ptif_id, ptifmst.ptif_lt_kaj_nm, ptifmst.ptif_ft_kaj_nm from ptifmst inner join $table_name on ptifmst.ptif_id = $table_name.ptif_id";
        $cond = "where $table_name.reservation_id = $rsv_id order by $table_name.order_no";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $pt_ids = array();
        $pt_nms = array();
        while ($row = pg_fetch_array($sel)) {
            $pt_ids[] = $row["ptif_id"];
            $pt_nms[] = $row["ptif_lt_kaj_nm"] . " " . $row["ptif_ft_kaj_nm"];
        }
        $pt_ids = implode(",", $pt_ids);
        $pt_nms = implode(", ", $pt_nms);
    }
}

// 内容形式によりテキストとテンプレートに表示を変更。
if ($fclhist_id == 0) {
    $content_type = 1;
} else {
    $fclhist_info = get_fclhist($con, $fname, $facility_id, $fclhist_id);
    $content_type = $fclhist_info["fclhist_content_type"];
}
// 設備IDごとの内容形式を取得する。種別表示フラグも取得。
$sql = "select facility.facility_id, fclhist.fclhist_content_type, facility.show_type_flg, facility.show_timeless_flg from facility left join fclhist on fclhist.facility_id = facility.facility_id and fclhist.fclhist_id = facility.fclhist_id";
$cond = "where facility_del_flg = 'f' order by facility.facility_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$arr_content_type = array();
$arr_show_type_flg = array();
$arr_show_timeless_flg = array();
while ($row = pg_fetch_array($sel)) {
    $arr_content_type[$row["facility_id"]] = ($row["fclhist_content_type"] == "") ? "1" : $row["fclhist_content_type"];
    $arr_show_type_flg[$row["facility_id"]] = $row["show_type_flg"];
    $arr_show_timeless_flg[$row["facility_id"]] = $row["show_timeless_flg"];
}

// カテゴリ別テンプレート使用確認
$arr_content_type_cate = array();
foreach ($categories as $tmp_category_id => $tmp_category) {
    $tmp_content_type = "1";
    foreach ($tmp_category["facilities"] as $tmp_facility_id => $tmp_facility) {
        // 設備のうち1件でもある場合、使用ありとする
        if ($arr_content_type["$tmp_facility_id"] == "2") {
            $tmp_content_type = "2";
            break;
        }
    }
    $arr_content_type_cate["$tmp_category_id"] = $tmp_content_type;
}

$arr_target_id = array();
// 個人スケジュール
if ($schd_flg == "t") {
    // 同時予約したスケジュールIDを取得
    $schd_id = get_sync_schd_id($con, $fname, $rsv_id, $timeless);
    // スケジュールデータがある場合
    if ($schd_id != "") {
        // 対象職員情報を配列に格納
        $arr_target_id = get_emp_id_from_schedules($con, $schd_id, $timeless, $fname);
    }

}

$arr_target = array();
for ($i = 0; $i < count($arr_target_id); $i++) {
    $tmp_emp_id = $arr_target_id[$i];
    $sql = "select (emp_lt_nm || ' ' || emp_ft_nm) as emp_name from empmst";
    $cond = "where emp_id = '$tmp_emp_id'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    }
    $tmp_emp_name = pg_fetch_result($sel, 0, "emp_name");
    array_push($arr_target, array("id" => $tmp_emp_id, "name" => $tmp_emp_name));
}

// マイグループと職員名リスト取得
list($mygroups, $employees) = get_mygroups_employees($con, $fname, $login_emp_id);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 設備予約 | 更新</title>
<script type="text/javascript" src="./js/fontsize.js"></script>
<?
require("schedule_registration_javascript.php");
insert_javascript();
?>
<script language="javascript">
// 設備ID別の種別表示フラグ
var arr_show_type_flg = new Array();
var arr_show_timeless_flg = new Array();
<?
foreach($arr_show_type_flg as $tmp_facility_id => $tmp_show_type_flg) {
    echo("arr_show_type_flg['$tmp_facility_id'] = '$tmp_show_type_flg';\n");
}
foreach($arr_show_timeless_flg as $tmp_facility_id => $tmp_show_timeless_flg) {
    echo("arr_show_timeless_flg['$tmp_facility_id'] = '$tmp_show_timeless_flg';\n");
}
?>
function initPage() {
    setDayOptions('<? echo($day); ?>');

    for (var i = 0, j = document.mainform.cate_id.options.length; i < j; i++) {
        if (document.mainform.cate_id.options[i].value == '<? echo($cate_id); ?>') {
            document.mainform.cate_id.options[i].selected = true;
        }
    }
    cateOnChange('<? echo($facility_id); ?>');
<?
    // テンプレート時は設備変更不可とする。
if ($content_type == 2) {
    echo("\tdocument.mainform.cate_id.disabled = true;");
    echo("\tdocument.mainform.facility_id.disabled = true;");
}
?>
    setTimeDisabled();
<?
if ($reg_emp_flg == "t") {
    echo("  update_target_html();\n");
} else {
    echo("  show_target_html();\n");
}
?>
    document.mainform.old_target_id_list.value = document.mainform.target_id_list.value;
    setMailDisabled();
    groupOnChange();

<? if ($repeat_flg == "t") { ?>
    // history.back()で戻ってきたときに、繰返しオプションが「1件のみ」でなかったら日付をdisabledにする
    if (!document.mainform.rptopt[0].checked) {
        setDateDisabled(true);
    }
<? } ?>
<? if ($sync_flg == "t") { ?>
    // history.back()で戻ってきたときに、同時予約オプションが「当施設」でなかったら施設をdisabledにする
    if (!document.mainform.syncopt[0].checked) {
        setFaciDisabled(true);
    }
<? } ?>
}

function setDayOptions(selectedDay) {
    var dayPulldown = document.mainform.day;
    if (!selectedDay) {
        selectedDay = dayPulldown.value;
    }
    deleteAllOptions(dayPulldown);

    var year = parseInt(document.mainform.year.value, 10);
    var month = parseInt(document.mainform.month.value, 10);
    var daysInMonth = new Date(year, month, 0).getDate();

    for (var d = 1; d <= daysInMonth; d++) {
        var tmpDayValue = d.toString();
        if (tmpDayValue.length == 1) {
            tmpDayValue = '0'.concat(tmpDayValue);
        }

        var tmpDayText = d.toString();

        var tmpDate = new Date(year, month - 1, d);
        var tmpWeekDay = tmpDate.getDay();
        switch (tmpWeekDay) {
        case 0:
            tmpDayText = tmpDayText.concat('（日）');
            break;
        case 1:
            tmpDayText = tmpDayText.concat('（月）');
            break;
        case 2:
            tmpDayText = tmpDayText.concat('（火）');
            break;
        case 3:
            tmpDayText = tmpDayText.concat('（水）');
            break;
        case 4:
            tmpDayText = tmpDayText.concat('（木）');
            break;
        case 5:
            tmpDayText = tmpDayText.concat('（金）');
            break;
        case 6:
            tmpDayText = tmpDayText.concat('（土）');
            break;
        }

        addOption(dayPulldown, tmpDayValue, tmpDayText);
    }

    while (parseInt(selectedDay, 10) > daysInMonth) {
        selectedDay = (parseInt(selectedDay, 10) - 1).toString();
    }
    dayPulldown.value = selectedDay;
}

function cateOnChange(facility_id) {

    // 施設・設備セレクトボックスのオプションを全削除
    deleteAllOptions(document.mainform.facility_id);

    // 施設・設備セレクトボックスのオプションを作成
    var category_id = document.mainform.cate_id.value;
<?
foreach ($categories as $tmp_category_id => $tmp_category) {
    echo("\tif (category_id == '$tmp_category_id') {\n");
    foreach ($tmp_category["facilities"] as $tmp_facility_id => $tmp_facility) {
        // 予約の内容形式がテキストの場合、テキスト使用の設備のみ出力。テキストからテンプレートへの変更がありうるので同じ設備ID時も。
        if (($content_type == 1 && $arr_content_type[$tmp_facility_id] == 1) || $content_type == 2 || $tmp_facility_id == $facility_id) {
            echo("\t\taddOption(document.mainform.facility_id, '$tmp_facility_id', '{$tmp_facility["name"]}', facility_id);\n");
        }
    }
    if ($tmp_category["pt_flg"] == "t") {
        echo("\tsetPatientDisplay(true);\n");
    } else {
        echo("\tsetPatientDisplay(false);\n");
    }
    echo("\t}\n");
}
?>

    // オプションが1件も作成されなかった場合
    var opt_cnt = document.mainform.facility_id.options.length;
    if (opt_cnt == 0) {
        addOption(document.mainform.facility_id, '0', '（なし）', facility_id);
    }

    // 施設・設備が未登録の場合、更新ボタンを押下不可にする
    document.mainform.sbmt.disabled = (opt_cnt == 0);
}

function setPatientDisplay(pt_flg) {
    if (pt_flg) {
        document.getElementById('pt_off_row').style.display = 'none';
        document.getElementById('pt_on_row').style.display = '';
    } else {
        document.getElementById('pt_on_row').style.display = 'none';
        <? // 連携を使用しない場合は行を表示しない ?>
        document.getElementById('pt_off_row').style.display = 'none';
    }
}

function deleteAllOptions(box) {

    for (var i = box.length - 1; i >= 0; i--) {
        box.options[i] = null;
    }

}

function addOption(box, value, text, selected) {

    var opt = document.createElement("option");
    opt.value = value;
    opt.text = text;
    if (selected == value) {
        if (window.opera) {
            opt.defaultSelected = true;
        } else {
            opt.selected = true;
        }
    }
    box.options[box.length] = opt;
    try {box.style.fontSize = 'auto';} catch (e) {}
    box.style.overflow = 'auto';

}

function updateReservation() {
    if (document.mainform.target_id_list.value != '') {
        var s_hour = document.mainform.s_hour.value;
        var s_minute = document.mainform.s_minute.value;
        var e_hour = document.mainform.e_hour.value;
        var e_minute = document.mainform.e_minute.value;
        document.mainform.schd_s_time.value = s_hour + ':' + s_minute;
        document.mainform.schd_e_time.value = e_hour + ':' + e_minute;
    }

    if (document.mainform.title.value == '') {
        alert('タイトルが入力されていません。');
        return;
    }

    if (window.InputCheck) {
        if (!InputCheck()) {
            return;
        }
    }

    var with_patient = (document.getElementById('pt_on_row').style.display != 'none');
    if (with_patient) {
        document.mainform.pt_nms.value = document.getElementById('ptname').innerHTML;
    } else {
        document.mainform.pt_ids.value = '';
        document.mainform.pt_nms.value = '';
    }
    document.mainform.cate_id.disabled = false;
    document.mainform.facility_id.disabled = false;
    document.mainform.submit();
}

function deleteReservation() {
    if (confirm('削除してよろしいですか？')) {
        document.mainform.action = 'facility_reservation_delete.php';
        document.mainform.submit();
    }
}

function checkType() {
    var check_count = 0;
    for (var i = 0, j = document.mainform.elements['type_ids[]'].length; i < j; i++) {
        if (document.mainform.elements['type_ids[]'][i].checked) {
            check_count++;
        }
    }
    if (check_count > 3) {
        alert('種別の指定は3つまでです。');
        return false;
    }
    return true;
}

function checkEndMinute() {
    if (document.mainform.e_hour.value == '24' &&
        document.mainform.e_minute.value != '00') {
        document.mainform.e_minute.value = '00';
        alert('終了時刻が24時の場合には00分しか選べません。');
    }
}

function setDateDisabled(disabled) {
    document.mainform.year.disabled = disabled;
    document.mainform.month.disabled = disabled;
    document.mainform.day.disabled = disabled;
}

function setFaciDisabled(disabled) {
    document.mainform.cate_id.disabled = disabled;
    document.mainform.facility_id.disabled = disabled;
}

function dispType() {

// 種別表示フラグにより変更する
    if (arr_show_type_flg[document.mainform.facility_id.value] == 't') {
        document.getElementById('type_row').style.display = '';
        document.mainform.show_type_flg.value = "t";
    } else {
        document.getElementById('type_row').style.display = 'none';
        document.mainform.show_type_flg.value = "f";
    }

// 「指定しない」表示フラグにより変更する
    if (arr_show_timeless_flg[document.mainform.facility_id.value] == 't') {
        document.getElementById('show_timeless').style.display = '';
        var disabled = document.mainform.timeless.checked;
        document.mainform.s_hour.disabled = disabled;
        document.mainform.s_minute.disabled = disabled;
        document.mainform.e_hour.disabled = disabled;
        document.mainform.e_minute.disabled = disabled;
    } else {
        document.getElementById('show_timeless').style.display = 'none';
        document.mainform.timeless.checked = false;
        document.mainform.s_hour.disabled = false;
        document.mainform.s_minute.disabled = false;
        document.mainform.e_hour.disabled = false;
        document.mainform.e_minute.disabled = false;
    }
}

function setTimeDisabled() {
    var disabled = document.mainform.timeless.checked;
    document.mainform.s_hour.disabled = disabled;
    document.mainform.s_minute.disabled = disabled;
    document.mainform.e_hour.disabled = disabled;
    document.mainform.e_minute.disabled = disabled;
}

// 登録対象者表示
function show_target_html()
{
    //登録対象者HTMLを更新
    _show_target_html2(document.getElementById("target_disp_area"),document.getElementById("target_id_list"),document.getElementById("target_name_list"));
}

function _show_target_html2(disp_obj,target_hidden_id,target_hidden_name)
{
    if(m_target_list["ID"].length == 0)
    {
        disp_obj.innerHTML = "";
        target_hidden_id.value = "";
        target_hidden_name.value = "";
    }
    else
    {
        var disp_obj_html = "";
        var target_hidden_id_value = "";
        var target_hidden_name_value = "";
        for(var i=0;i<m_target_list["ID"].length;i++)
        {
            var emp_id   = m_target_list["ID"][i].emp_id;
            var emp_name = m_target_list["ID"][i].emp_name;
            if(i!=0)
            {
                disp_obj_html = disp_obj_html + ",";
                target_hidden_id_value = target_hidden_id_value + ",";
                target_hidden_name_value = target_hidden_name_value + ",";
            }

            disp_obj_html = disp_obj_html + emp_name;

            target_hidden_id_value = target_hidden_id_value + emp_id;
            target_hidden_name_value = target_hidden_name_value + emp_name;
        }
        disp_obj.innerHTML = disp_obj_html;
        target_hidden_id.value = target_hidden_id_value;
        target_hidden_name.value = target_hidden_name_value;
    }
}

var childwin = null;
function openEmployeeList() {
    dx = screen.availWidth - 10;
    dy = screen.top;
    base = 0;
    wx = 720;
    wy = 600;
    var url = './schedule_address_popup.php';
    url += '?session=<?=$session?>';
    url += '&emp_id=<?=$login_emp_id?>';
    url += '&mode=1';
    childwin = window.open(url, 'emplistpopup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

    childwin.focus();
}

function closeEmployeeList() {
    if (childwin != null && !childwin.closed) {
        childwin.close();
    }
    childwin = null;
}

//--------------------------------------------------
//登録対象者一覧
//--------------------------------------------------
<?
    $script = "m_target_list['ID'] = new Array(";
    $is_first = true;
    foreach($arr_target as $row)
    {
        if($is_first)
        {
            $is_first = false;
        }
        else
        {
            $script .= ",";
        }
        $emp_id = $row["id"];
        $emp_name = $row["name"];
        $script .= "new user_info('$emp_id','$emp_name')";
    }
    $script .= ");\n";
    print $script;
?>

function groupOnChange() {
    var group_id = getSelectedValue(document.mainform.mygroup);

    // 職員一覧セレクトボックスのオプションを全削除
    deleteAllOptions(document.mainform.emplist);

    // 職員一覧セレクトボックスのオプションを作成
<? foreach ($employees as $tmp_group_id => $arr) { ?>
    if (group_id == '<? echo $tmp_group_id; ?>') {
    <? foreach($arr as $tmp_emp) { ?>
        addOption(document.mainform.emplist, '<? echo $tmp_emp["emp_id"]; ?>', '<? echo $tmp_emp["name"]; ?>');
    <? } ?>
    }
<? } ?>
}

function setTargetList() {
    document.mainform.target_id_list.value = '';
    for (var i = 0, j = document.mainform.target.options.length; i < j; i++) {
        if (i > 0) {
            document.mainform.target_id_list.value += ',';
        }
        document.mainform.target_id_list.value += document.mainform.target.options[i].value;
    }
    setMailDisabled();
}

function getSelectedValue(sel) {
    return sel.options[sel.selectedIndex].value;
}

function setMailDisabled() {
    var disabled = (
        document.mainform.target_id_list.value == '<? echo($login_emp_id); ?>' ||
        document.mainform.target_id_list.value == '' ||
        document.mainform.target_id_list.value == document.mainform.old_target_id_list.value
    );
    document.mainform.sendmail.disabled = disabled;
}

function addEmp() {
    var emp_id_str = "";
    var emp_name_str = "";
    var first_flg = true;
    for (var i = 0, j = document.mainform.emplist.options.length; i < j; i++) {
        if (document.mainform.emplist.options[i].selected) {
            var emp_id = document.mainform.emplist.options[i].value;
            var emp_name = document.mainform.emplist.options[i].text;
            if (first_flg == true) {
                first_flg = false;
            } else {
                emp_id_str += ", ";
                emp_name_str += ", ";
            }
            emp_id_str += emp_id;
            emp_name_str += emp_name;
        }
    }
    if (emp_id_str != "") {
        set_wm_counter(emp_id_str);
        add_target_list(emp_id_str, emp_name_str);
    }
}

</script>
<?
// カレンダー用JS処理
$num = 0;
if ($content_type == "2") {
    $tmpl_content = $fclhist_info["fclhist_content"];
    $pos = 0;
    while (1) {
        $pos = strpos($tmpl_content, 'show_cal', $pos);
        if ($pos === false) {
            break;
        } else {
            $num++;
        }
        $pos++;
    }
}
if ($num > 0) {
    // 外部ファイルを読み込む
    write_yui_calendar_use_file_read_0_12_2();
}
// カレンダー作成、関数出力
write_yui_calendar_script2($num);

?>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-style:none;}
.non_in_list {border-collapse:collapse;}
.non_in_list td {border:0px;}

table.block {border-collapse:collapse;}
table.block td {border:#5279a5 solid 1px;padding:1px;}
table.block td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#5279a5 solid 0px;}

table.block2 {border-collapse:collapse;}
table.block2 td {border:#5279a5 solid 1px;padding:1px;}
table.block2 td {border-width:1;}

table.block3 {border-collapse:collapse;}
table.block3 td {border:#5279a5 solid 0px;}
table.block3 td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="initPage();<? if ($content_type == "2") { ?>
initcal();
<? } ?>if (window.OnloadSub) { OnloadSub(); }">
<center>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>更新</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form name="mainform" action="facility_reservation_update_exe.php" method="post">
<table width="660" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="20%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">施設・設備</font></td>
<td colspan="3"><select name="cate_id" onchange="cateOnChange();dispType();"><? show_category_options($categories, $cate_id); ?></select><select name="facility_id" onchange="dispType();"></select></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日付</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="year" onchange="setDayOptions();"><? show_select_years_span(min(date("Y"), $year), date("Y") + 10, $year); ?></select>/<select name="month" onchange="setDayOptions();"><? show_select_months($month); ?></select>/<select name="day"></select></font></td>
</tr>
<? // 設備テーブルfacilityの「指定しない」表示フラグにより表示を変更
$show_timeless_disp = ($arr_show_timeless_flg["$facility_id"] == "t") ? "" : "none";
?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時刻</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="s_hour"><? show_select_hrs_0_23($s_hour); ?></select>：<select name="s_minute"><? show_minute_options($s_minute); ?></select>〜<select name="e_hour" onchange="checkEndMinute();"><? show_select_hrs_by_args(0, 24, $e_hour); ?></select>：<select name="e_minute" onchange="checkEndMinute();"><? show_minute_options($e_minute); ?></select>
<span id="show_timeless" style="display:<?=$show_timeless_disp ?>;"><input type="checkbox" name="timeless" value="t"<? if ($timeless == "t") {echo(" checked");} ?> onclick="setTimeDisabled();">指定しない</span></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイトル</font></td>
<td><input type="text" name="title" value="<? eh($title); ?>" size="50" maxlength="100" style="ime-mode:active;"></td>
<td bgcolor="#f6f9ff" width="110" align="right" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ラインマーカー</font></td>
<td>
<select name="marker">
<option value="0" style="background-color:white;color:black;"<? if ($marker == 0) {echo(" selected");} ?>>なし
<option value="1" style="background-color:red;color:white;"<? if ($marker == 1) {echo(" selected");} ?>>赤
<option value="2" style="background-color:aqua;color:blue;"<? if ($marker == 2) {echo(" selected");} ?>>青
<option value="3" style="background-color:yellow;color:blue;"<? if ($marker == 3) {echo(" selected");} ?>>黄
<option value="4" style="background-color:lime;color:blue;"<? if ($marker == 4) {echo(" selected");} ?>>緑
<option value="5" style="background-color:fuchsia;color:white;"<? if ($marker == 5) {echo(" selected");} ?>>ピンク
</select>
</td>
</tr>
<? // 設備テーブルfacilityの種別表示フラグにより表示を変更
$show_type_disp = ($arr_show_type_flg["$facility_id"] == "t") ? "" : "none";
?>
<tr height="22" id="type_row" style="display:<?=$show_type_disp?>;">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">種別<br><font color="red">※3つまで指定可</font></font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
foreach ($arr_type as $type_id => $type_name) {
    echo("<input type=\"checkbox\" name=\"type_ids[]\" value=\"$type_id\"");
    if (in_array($type_id, $type_ids)) {
        echo(" checked");
    }
    echo(" onclick=\"return checkType();\">$type_name");
    if ($type_id % 5 == 0) {echo("<br>");}
    echo("\n");
}
?>
</font></td>
</tr>
<tr height="22" id="pt_off_row" style="display:none;">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="gray">患者氏名</font></td>
<td colspan="3"></td>
</tr>
<tr height="22" id="pt_on_row" style="display:none;">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:void(0);" onclick="window.open('facility_patient_select.php?session=<? echo($session); ?>', 'ptsel', 'width=640,height=480,scrollbars=yes');"><?
// 患者氏名/利用者氏名
echo $_label_by_profile["PATIENT_NAME"][$profile_type];
?></a></font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><span id="ptname"><? echo($pt_nms); ?></span></font></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録者内線番号</font></td>
<td colspan="3"><input name="emp_ext" type="text" size="10" maxlength="10" value="<? echo $emp_ext; ?>" style="ime-mode:inactive;"></td>
</tr>
<tr height="22">

<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">内容</font></td>

<td colspan="4">
<?
// テキストの場合
if ($content_type == 1) {
?>
<textarea name="content" cols="65" rows="5" style="ime-mode:active;"><? eh($content); ?></textarea>
<? } else {
// テンプレートの場合
    $tmpl_content = $fclhist_info["fclhist_content"];

    $tmpl_content_html = preg_replace("/<\? \/\/ XML.*\?>/s", "", $tmpl_content);
    // プレビュー用ファイル保存
    $savefilename = "fcl/tmp/{$session}_p.php";

    if (!$fp = fopen($savefilename, "w")) {
        echo("<script language=\"javascript\">alert('ファイルのオープンができませんでした。');</script>");
        echo("<script language=\"javascript\">history.back();</script>");
        exit;
    }
    if (strlen($tmpl_content_html) == 0) {
        $tmpl_content_html = " ";
    }
    if (!fwrite($fp, $tmpl_content_html, 2000000)) {
        echo("<script language=\"javascript\">alert('ファイルの書き込みができませんでした。');</script>");
        echo("<script language=\"javascript\">history.back();</script>");
        exit;
    }

    fclose($fp);

    include($savefilename);

    if (strlen($content) > 0 ){
        $utf_content = mb_convert_encoding(mb_convert_encoding($content,"sjis-win","eucJP-win"),"UTF-8","sjis-win");
        $utf_content = str_replace("EUC-JP", "UTF-8", $utf_content);
        $utf_content = preg_replace("/[\x01-\x08\x0b\x0c\x0e-\x1f\x7f]+/","",$utf_content);
        $utf_content = str_replace("\0","",$utf_content);
        $doc = domxml_open_mem($utf_content);
        $root = $doc->get_elements_by_tagname('apply');
        $node_array = $root[0]->child_nodes();

        // 項目設定
        echo("<script language='javascript'>\n");

        show_js_set_value($node_array);

        // セレクト項目設定用関数を呼び、その後もう一度値の設定をする
        echo("if(window.OnchangeSub) {\n");
        echo("  OnchangeSub();\n");
        echo("}\n");

        show_js_set_value($node_array);

        echo("</script>\n");
    }
}
?>
</td>
</tr>
<?
// 登録者、管理者は対象者を更新可能、それ以外は表示のみ
$display_target = ($login_emp_id != $reg_emp_id && $admin_flg != true) ? "none" : "";
?>
<tr height="100">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録対象者</font><br>
<span style="display:<?=$display_target?>">
<input type="button" value="職員名簿" style="margin-left:2em;width=5.5em;" onclick="openEmployeeList();"><br>
<input type="button" value="クリア" style="margin-left:2em;width=5.5em;" onclick="clear_target_all();"><br></span></td>
<td colspan="3">
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" class="non_in_list">
<tr>
<td width="350" style="border:#5279a5 solid 1px;">

<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
<span id="target_disp_area"></span>
</font>

</td>
<td style="display:<?=$display_target?>">
<input type="button" value="&lt;&nbsp;追加" style="margin-left:1em;width=4.5em;" onclick="addEmp();">
</td>
<td width="10">
</td>
<td>

<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="1" class="non_in_list">
<tr style="display:<?=$display_target?>">
<td height="45%" align="center" valign="bottom">

<select name="mygroup" onchange="groupOnChange();">
<?
 foreach ($mygroups as $tmp_mygroup_id => $tmp_mygroup) {
?>
<option value="<?=$tmp_mygroup_id?>"
<?
        if ($mygroup == $tmp_mygroup_id) {
            echo(" selected");
        }
?>
><?=$tmp_mygroup?></option>
<?
}
?>
</selelct>
</td></tr>
<tr style="display:<?=$display_target?>"><td>
<select name="emplist" size="7" multiple style="width:150px;">
</select>

</td>
</tr>
<tr style="display:<?=$display_target?>">
<td height="45%" valign="top"><input type="button" value="全て選択" onclick="selectAllEmp();"></td>
</tr>
</table>

</td>
</tr>
</table>

</td>
</tr>
<tr style="display:<?=$display_target?>">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録通知メール</font>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="sendmail" value="t"<? if ($sendmail == "t") {echo(" checked");} ?>>送信する<font color="gray">（自分以外の追加された登録対象者にウェブメールを送信します。）</font></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">利用人数</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="text" name="users" value="<? echo($users); ?>" size="4" maxlength="4" style="ime-mode:inactive;">名</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録者</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($reg_emp_name); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">最終更新者</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($upd_emp_name); ?></font></td>
</tr>
<? if ($repeat_flg == "t") { ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">繰返しオプション</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="rptopt" value="1" <? if ($rptopt == "" || $rptopt == "1") {echo(" checked ");} ?> onclick="setDateDisabled(false);">1件のみ
<input type="radio" name="rptopt" value="2"  <? if ($rptopt == "2") {echo(" checked ");} ?>onclick="setDateDisabled(true);">全て
<input type="radio" name="rptopt" value="3"  <? if ($rptopt == "3") {echo(" checked ");} ?>onclick="setDateDisabled(true);">将来の予定のみ<br>
<font color="red">※この予定は繰返し登録されています。更新／削除時にはこのオプションが有効になります。</font>
</td>
</tr>
<? } ?>
<? if ($sync_flg == "t") { ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">同時予約オプション</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="syncopt" value="1" <? if ($syncopt == "" || $syncopt == "1") {echo(" checked ");} ?> onclick="setFaciDisabled(false);">当施設・設備のみ
<input type="radio" name="syncopt" value="2" <? if ($syncopt == "2") {echo(" checked ");} ?> onclick="setFaciDisabled(true);">全て<br>
<font color="red">※この予定は同時予約されています。更新／削除時にはこのオプションが有効になります。</font>
</td>
</tr>
<? } ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">スケジュール連携</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="schdopt" value="1" <? if ($schdopt == "" || $schdopt == "1") {echo(" checked ");} ?>>個人スケジュールも変更する<br>
<input type="radio" name="schdopt" value="2" <? if ($schdopt == "2") {echo(" checked ");} ?>>個人スケジュールは変更しない
</td>
</tr>
</table>
<table width="660" border="0" cellspacing="0" cellpadding="2">
<tr>
<td align="right"><input type="button" name="sbmt" value="更新" onclick="updateReservation();">&nbsp;<input type="button" value="削除" onclick="deleteReservation();"></td>
</tr>
</table>
<input type="hidden" name="path" value="<? echo($path); ?>">
<input type="hidden" name="range" value="<? echo($range); ?>">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="rsv_id" value="<? echo($rsv_id); ?>">
<input type="hidden" name="pt_ids" value="<? echo($pt_ids); ?>">
<input type="hidden" name="pt_nms" value="">
<input type="hidden" name="repeat_flg" value="<? echo($repeat_flg); ?>">
<input type="hidden" name="sync_flg" value="<? echo($sync_flg); ?>">
<input type="hidden" name="schd_flg" value="<? echo($schd_flg); ?>">
<input type="hidden" name="year_old" value="<? echo($year); ?>">
<input type="hidden" name="month_old" value="<? echo($month); ?>">
<input type="hidden" name="day_old" value="<? echo($day); ?>">
<input type="hidden" name="cate_id_old" value="<? echo($cate_id); ?>">
<input type="hidden" name="facility_id_old" value="<? echo($facility_id); ?>">
<input type="hidden" name="reg_time" value="<? echo($reg_time); ?>">
<input type="hidden" name="schd_s_time">
<input type="hidden" name="schd_e_time">
<input type="hidden" name="content_type" value="<? echo($content_type); ?>">
<input type="hidden" name="fclhist_id" value="<? echo($fclhist_id); ?>">
<input type="hidden" name="show_type_flg" value="<? echo($arr_show_type_flg["$facility_id"]); ?>">
<input type="hidden" name="original_timeless" value="<? echo($timeless); ?>">
<input type="hidden" id="target_id_list" name="target_id_list" value="">
<input type="hidden" id="old_target_id_list" name="old_target_id_list" value="">
<input type="hidden" id="target_name_list" name="target_name_list" value="">
<input type="hidden" name="reg_emp_flg" value="<? echo($reg_emp_flg); ?>">
</form>
</center>
</body>
<? pg_close($con); ?>
</html>
<?
// カテゴリオプションを出力
function show_category_options($categories, $cate_id) {
    foreach ($categories as $tmp_category_id => $tmp_category) {
        echo("<option value=\"$tmp_category_id\">{$tmp_category["name"]}\n");
    }
}

// 分オプションを出力
function show_minute_options($minute) {

    $arr_minute = array();
    for ($i = 0; $i < 60; $i += 5) {
        $arr_minute[] = sprintf("%02d", $i);
    }

    foreach ($arr_minute as $tmp_minute) {
        echo("<option value=\"$tmp_minute\"");
        if ($minute == $tmp_minute) {
            echo " selected";
        }
        echo(">$tmp_minute");
    }

}

// XMLから取得した値をもとに、入力項目の初期設定をするJS出力
function show_js_set_value($node_array) {
    foreach ($node_array as $child) {
        if ($child->node_type() == XML_ELEMENT_NODE) {
            $itemvalue = mb_convert_encoding($child->get_content(), "eucJP-win", "UTF-8");
            // \、"をエスケープする
            if ($type != "checkbox" && $type != "radio") {
                $itemvalue = str_replace("\\", "\\\\", $itemvalue);
                $itemvalue = str_replace("\"", "\\\"", $itemvalue);
            }

            $itemvalue = str_replace("\r\n", "\n", $itemvalue);
            $itemvalue = str_replace("\n", "\\n", $itemvalue);

            $type = $child->get_attribute("type");
            // チェックボックス
            if( $type == "checkbox" ) {
                $id = $child->get_attribute("id");
                echo("document.mainform.elements['$child->tagname[$id]'].checked = true;\n");
            } else if( $type == "radio" ) {
            // ラジオボタン
                echo("for(i=0; i<document.mainform.elements['$child->tagname[]'].length; i++) {\n");

                echo("  if(document.mainform.elements['$child->tagname[]'][i].value == '$itemvalue') {\n");
                echo("      document.mainform.elements['$child->tagname[]'][i].checked = true;\n");
                echo("      break;\n");

                echo("  }\n");
                echo("}\n");

            } else {
            // テキスト、テキストエリア、セレクト
                echo("document.mainform.".$child->tagname.".value = \"".$itemvalue."\";\n");
            }
        }
    }
}