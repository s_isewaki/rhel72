<?php
//==========================================================================
// 必要人数設定
//
//==========================================================================
ob_start();
require_once("about_authority.php");
require_once("about_session.php");
require_once("duty_shift_common.ini");
require_once("duty_shift_user_tab_common.ini");
ob_end_clean();
require_once("about_comedix.php");

//---------------------------------------------------------
ob_start();
//ページ名
$fname = $_SERVER['PHP_SELF'];

// データベースに接続
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// セッションのチェック
$session = qualify_session($_REQUEST["session"], $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 69, $fname);
$adminauth = check_authority($session, 70, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
//---------------------------------------------------------

// 職員データの取得
$sql = "SELECT e.* FROM empmst e JOIN session USING (emp_id) WHERE session_id='{$session}'";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$emp = pg_fetch_assoc($sel);

// 勤務シフトグループデータの取得
$sql = "SELECT * FROM duty_shift_group g
		WHERE person_charge_id='{$emp["emp_id"]}'
		OR caretaker_id='{$emp["emp_id"]}'
		OR caretaker2_id='{$emp["emp_id"]}'
		OR caretaker3_id='{$emp["emp_id"]}'
		OR caretaker4_id='{$emp["emp_id"]}'
		OR caretaker5_id='{$emp["emp_id"]}'
		OR caretaker6_id='{$emp["emp_id"]}'
		OR caretaker7_id='{$emp["emp_id"]}'
		OR caretaker8_id='{$emp["emp_id"]}'
		OR caretaker9_id='{$emp["emp_id"]}'
		OR caretaker10_id='{$emp["emp_id"]}'
		ORDER BY order_no, group_id";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$grouplist = pg_fetch_all($sel);
$tab_menuitem_group_id = "";

if (!empty($grouplist)) {
	// 表示するグループデータの取得
	$group_id = !isset($_REQUEST["group_id"]) ? $grouplist[0]["group_id"] : $_REQUEST["group_id"];
	foreach($grouplist as $data) {
		if ($group_id === $data["group_id"]) {
			$group = $data;
			break;
		}
	}
	if(empty($group)) {
		$group = $grouplist[0];
	}
	$tab_menuitem_group_id = "&group_id={$group["group_id"]}";

	// 職種データの取得
	$sql = "SELECT * FROM jobmst WHERE job_del_flg='f' ORDER BY order_no";
	$sel = select_from_table($con, $sql, "", $fname);
	if($sel == 0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$job = pg_fetch_all($sel) ? pg_fetch_all($sel) : array();

	// 役職データの取得
	$sql = "SELECT * FROM stmst WHERE st_del_flg='f' ORDER BY order_no";
	$sel = select_from_table($con, $sql, "", $fname);
	if($sel == 0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$st = pg_fetch_all($sel) ? pg_fetch_all($sel) : array();

	// チームデータの取得
	$sql = "SELECT * FROM duty_shift_staff_team ORDER BY no";
	$sel = select_from_table($con, $sql, "", $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$team = pg_fetch_all($sel) ? pg_fetch_all($sel) : array();

	// 勤務パターンデータの取得
	$sql = "SELECT atdptn_id as id, atdptn_nm as name FROM atdptn WHERE group_id='{$group["pattern_id"]}' AND atdptn_id<>10 ORDER BY atdptn_id";
	$sel = select_from_table($con, $sql, "", $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$pattern = pg_fetch_all($sel) ? pg_fetch_all($sel) : array();

	// 必要人数データの取得
	$sql = "SELECT * FROM duty_shift_need_cnt_standard WHERE group_id='{$group_id}' AND atdptn_ptn_id<>10 ORDER BY no";
	$sel = select_from_table($con, $sql, "", $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

	$need = pg_fetch_all($sel) ? pg_fetch_all($sel) : array();

	// skillレベル名データ取得。
	// skillレベル(I〜V)を増減する場合は以下のテーブルに対してデータを増減してください。
	// その際は「職員設定」での職員毎のスキル設定にも影響しますので確認してください。
	$sql = "SELECT * FROM duty_shift_skill_name ORDER BY skill_id";
	$sel = select_from_table($con, $sql, "", $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$skill_name_array = pg_fetch_all($sel) ? pg_fetch_all($sel) : array();
}

// データベースを切断
pg_close($con);
ob_end_clean();
//---------------------------------------------------------

function show_ptn_options($ptn, $id=null)
{
	foreach($ptn as $data) {
		$selected = "";
		if(!empty($id) and $data["id"] === $id) {
			$selected = 'selected';
		}
		if($data["id"] === "10"){
			$data["name"] .= '(公休)';
		}
		printf('<option %s value="%s">%s</option>', $selected, $data["id"], $data["name"]);
	}
}

function show_job_options($job, $id=null)
{
	foreach($job as $data) {
		$selected = "";
		if(!empty($id) and $data["job_id"] === $id) {
			$selected = 'selected';
		}
		printf('<option %s value="%s">%s</option>', $selected, $data["job_id"], $data["job_nm"]);
	}
}

function show_st_options($st, $id=null)
{
	foreach($st as $data) {
		$selected = "";
		if(!empty($id) and $data["st_id"] === $id) {
			$selected = 'selected';
		}
		printf('<option %s value="%s">%s</option>', $selected, $data["st_id"], $data["st_nm"]);
	}
}

// 要求スキルレベル
function show_skill_options($skill_name_array, $id=null)
{
	foreach($skill_name_array as $data) {
		$selected = "";
		if(!empty($id) and $data["skill_id"] === $id) {
			$selected = 'selected';
		}
		printf('<option %s value="%s">%s</option>', $selected, $data["skill_id"], $data["skill_name"]);
	}
}
// 要求スキルレベル２
function show_skill2_options($skill_name_array, $id=null)
{
	foreach($skill_name_array as $data) {
		$selected = "";
		if(!empty($id) and $data["skill_id"] === $id) {
			$selected = 'selected';
		}
		printf('<option %s value="%s">%s</option>', $selected, $data["skill_id"], $data["skill_name"]);
	}
}
function show_team_options($team, $id=null)
{
	foreach($team as $data) {
		$selected = "";
		if(!empty($id) and $data["team_id"] === $id) {
			$selected = 'selected';
		}
		printf('<option %s value="%s">%s</option>', $selected, $data["team_id"], $data["team_name"]);
	}
}
//---------------------------------------------------------
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務シフト作成｜必要人数設定</title>
<?
include_js("js/fontsize.js");
include_js("js/jquery/jquery-1.9.1.min.js");
include_js("js/jquery/jquery-ui-1.10.3.min.js");
include_js("js/duty_shift/need_cnt_standard.js");
?>
<script type="text/javascript">
<!--
	var pattern= '<? show_ptn_options($pattern)?>';
	var job = '<? show_job_options($job)?>';
	var team = '<? show_team_options($team)?>';
	var st = '<? show_st_options($st)?>';
	var skill = '<? show_skill_options($skill_name_array)?>';
	var skill2 = '<? show_skill2_options($skill_name_array)?>';
//-->

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
<!--
	#tabmenu {border-bottom: 2px solid #5279a5;}
	.list {border-collapse:collapse; margin:2px;}
	.list thead td {text-align:center;}
	.list td {border:#5279a5 solid 1px; padding:3px; font-size:10px;}
	#comment {font-size:10px;}
	#list tr.draggable {cursor:move;}
	tr.droptarget {background-color:#ffff99; height:60px;}
//-->
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">

<!-- ヘッダ -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<? show_user_title($session, $adminauth); ?>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="tabmenu">
<? show_user_tab_menuitem($session, $fname, $tab_menuitem_group_id); ?>
</table>

<?if (empty($grouplist)) {?>
<p>勤務シフトグループ情報が未設定です。管理画面で登録してください。</p>
<?} else {?>

<form id="form" method="post" action="duty_shift_need_cnt_standard_update_exe.php">
<input type="hidden" id="session" name="session" value="<?eh($session)?>">
<input type="hidden" id="emp_id" name="emp_id" value="<?eh($emp["emp_id"])?>">

<p>シフトグループ(病棟)名:
<select id="group_select" name="group_id">
<? foreach($grouplist as $data) { ?>
<option value="<?eh($data["group_id"])?>" <?echo ($group_id===$data["group_id"])?'selected':'';?>><?eh($data["group_name"])?>
<? } //endforeach ?>
</select>
</p>

<table id="list" class="list">

<thead>
<tr>
	<td colspan="16" style="text-align:right;border:0;"><button type="button" onclick="check_form();">登録</button></td>
</tr>
<tr>
	<td>順番</td>
	<td>出勤<br>パターン</td>
	<td>チーム</td>
	<td>職種</td>
	<td>役職</td>
	<td>スキル</td>
	<td>性別</td>
	<td>月</td>
	<td>火</td>
	<td>水</td>
	<td>木</td>
	<td>金</td>
	<td style="color:blue;">土</td>
	<td style="color:red;">日</td>
	<td style="color:red;">祝</td>
	<td><button type="button" onclick="add_table_row(this,pattern,team,job,st,skill,skill2);">追加</button></td>
	<td>削除</td>

</tr>
</thead>

<tbody id="list_body">
<?
if (!empty($need)) {
	foreach($need as $data) {
?>
<tr class="draggable">
	<td><?=$data["no"]?></td>
	<td><select name="atdptn_ptn_id[]"><?show_ptn_options($pattern, $data["atdptn_ptn_id"])?></select></td>
	<td><select name="team[]"><option></option><?show_team_options($team, $data["team"])?></select></td>
	<td>
		<select name="job_id[]"><option></option><?show_job_options($job, $data["job_id"])?></select>
		<select name="job_id2[]"><option></option><?show_job_options($job, $data["job_id2"])?></select>
		<select name="job_id3[]"><option></option><?show_job_options($job, $data["job_id3"])?></select>
	</td>
	<td><select name="st_id[]"><option></option><?show_st_options($st, @$data["st_id"])?></select></td>
	<td><select name="need_skill[]"><option></option><?show_skill_options($skill_name_array , @$data["need_skill"])?></select>
	<!-- 要求スキルレベル追加 201401 START -->
	又は<select name="need_skill2[]"><option></option><?show_skill_options($skill_name_array , @$data["need_skill2"])?></select></td>
	<!-- 要求スキルレベル追加 END-->
	<td><select name="sex[]"><option></option><option value="1" <?echo $data["sex"]==1?'selected':'';?>>男性</option><option value="2" <?echo $data["sex"]==2?'selected':'';?>>女性</option></select></td>
	<td><input name="mon_cnt[]" type="text" value="<?=$data["mon_cnt"]?$data["mon_cnt"]:$data["week_cnt"]?>" size="3" maxlength="3" style="ime-mode:disabled;"></td>
	<td><input name="tue_cnt[]" type="text" value="<?=$data["tue_cnt"]?$data["tue_cnt"]:$data["week_cnt"]?>" size="3" maxlength="3" style="ime-mode:disabled;"></td>
	<td><input name="wed_cnt[]" type="text" value="<?=$data["wed_cnt"]?$data["wed_cnt"]:$data["week_cnt"]?>" size="3" maxlength="3" style="ime-mode:disabled;"></td>
	<td><input name="thu_cnt[]" type="text" value="<?=$data["thu_cnt"]?$data["thu_cnt"]:$data["week_cnt"]?>" size="3" maxlength="3" style="ime-mode:disabled;"></td>
	<td><input name="fri_cnt[]" type="text" value="<?=$data["fri_cnt"]?$data["fri_cnt"]:$data["week_cnt"]?>" size="3" maxlength="3" style="ime-mode:disabled;"></td>
	<td><input name="sat_cnt[]" type="text" value="<?=$data["sat_cnt"]?>" size="3" maxlength="3" style="ime-mode:disabled;"></td>
	<td><input name="sun_cnt[]" type="text" value="<?=$data["sun_cnt"]?>" size="3" maxlength="3" style="ime-mode:disabled;"></td>
	<td><input name="holiday_cnt[]" type="text" value="<?=$data["holiday_cnt"]?>" size="3" maxlength="3" style="ime-mode:disabled;"></td>
	<td><button type="button" onclick="add_table_row(this,pattern,team,job,st,skill,skill2);">追加</button></td>
	<td><button type="button" onclick="remove_table_row(this);">削除</button></td>
</tr>
<?
	} //endforeach
}
else {
?>
<tr class="draggable">
	<td>-</td>
	<td><select name="atdptn_ptn_id[]"><?show_ptn_options($pattern)?></select></td>
	<td><select name="team[]"><option></option><?show_team_options($team)?></select></td>
	<td>
		<select name="job_id[]"><option></option><?show_job_options($job)?></select>
		<select name="job_id2[]"><option></option><?show_job_options($job)?></select>
		<select name="job_id3[]"><option></option><?show_job_options($job)?></select>
	</td>
	<td><select name="st_id[]"><option></option><?show_st_options($st)?></select></td>
	<td><select name="need_skill[]"><option></option><?show_skill_options($skill_name_array)?></select> <!-- スキル追加 -->
	<!-- 要求スキルレベル追加 201401 START -->
	又は<select name="need_skill2[]"><option></option><?show_skill_options($skill_name_array)?></select></td>
	<!-- 要求スキルレベル追加 END-->
	<td><select name="sex[]"><option></option><option value="1">男性</option><option value="2">女性</option></select></td>
	<td><input name="mon_cnt[]" type="text" size="3" maxlength="3" style="ime-mode:disabled;"></td>
	<td><input name="tue_cnt[]" type="text" size="3" maxlength="3" style="ime-mode:disabled;"></td>
	<td><input name="wed_cnt[]" type="text" size="3" maxlength="3" style="ime-mode:disabled;"></td>
	<td><input name="thu_cnt[]" type="text" size="3" maxlength="3" style="ime-mode:disabled;"></td>
	<td><input name="fri_cnt[]" type="text" size="3" maxlength="3" style="ime-mode:disabled;"></td>
	<td><input name="sat_cnt[]" type="text" size="3" maxlength="3" style="ime-mode:disabled;"></td>
	<td><input name="sun_cnt[]" type="text" size="3" maxlength="3" style="ime-mode:disabled;"></td>
	<td><input name="holiday_cnt[]" type="text" size="3" maxlength="3" style="ime-mode:disabled;"></td>
	<td><button type="button" onclick="add_table_row(this,pattern,team,job,st,skill,skill2);">追加</button></td>
	<td><button type="button" onclick="remove_table_row(this);">削除</button></td>
</tr>
<?} ;//endif ?>
</tbody>

<tfoot>
<tr>
	<td colspan="16" style="text-align:right;border:0;"><button type="button" onclick="check_form();">登録</button></td>
</tr>
</tfoot>

</table>

</form>

<hr size="1">
<div id="comment">
<p>※順番はドラッグ＆ドロップで入れ替えられます。</p>
<p>※「自動シフト作成」では一覧の上から順番に割り当て処理が行われます。</p>
</div>
<?} //endif ?>

</body>
</html>