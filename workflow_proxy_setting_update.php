<?
require_once("about_comedix.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 3, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 設定を更新
///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
///-----------------------------------------------------------------------------
// トランザクションを開始
///-----------------------------------------------------------------------------
pg_query($con, "begin transaction");
///-----------------------------------------------------------------------------
// 管理者情報を全削除
///-----------------------------------------------------------------------------
$sql = "delete from wkfw_proxy";
$cond = "";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
///-----------------------------------------------------------------------------
// スタッフ情報を登録
///-----------------------------------------------------------------------------
if ($emp_id_array != "") {
    //職員データ分を登録
    $emp_id_array = split(",", $emp_id_array);
    $no = 1;
    foreach ($emp_id_array as $emp_id) {
        ///-----------------------------------------------------------------------------
        // スタッフ情報を登録
        ///-----------------------------------------------------------------------------
        $sql = "insert into wkfw_proxy (emp_id, no) values (";
        $content = array($emp_id, $no);
        $ins = insert_into_table($con, $sql, $content, $fname);
        if ($ins == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $no++;
    }
}
///-----------------------------------------------------------------------------
// トランザクションをコミット
///-----------------------------------------------------------------------------
pg_query($con, "commit");
///-----------------------------------------------------------------------------
// データベース接続を閉じる
///-----------------------------------------------------------------------------
pg_close($con);

// 画面をリフレッシュ
echo("<script type=\"text/javascript\">location.href = 'workflow_proxy_setting.php?session=$session';</script>");
