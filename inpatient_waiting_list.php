<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 病床管理｜待機患者</title>
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_select_values.ini");
require_once("inpatient_list_common.php");

define("ROWS_PER_PAGE", 20);

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 診療科登録権限を取得
$section_admin_auth = check_authority($session, 28, $fname);

// 病棟登録権限を取得
$ward_admin_auth = check_authority($session, 21, $fname);

// データベースに接続
$con = connect2db($fname);

// 病棟一覧を取得
$sql = "select bldg_cd, ward_cd, ward_name from wdmst";
$cond = "where ward_del_flg = 'f' order by bldg_cd, ward_cd";
$sel_ward = select_from_table($con, $sql, $cond, $fname);
if ($sel_ward == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

$sql = "select * from bedcheckwait";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
for ($i = 1; $i <= 10; $i++) {
	$varname = "display$i";
	$$varname = pg_fetch_result($sel, 0, "display$i");
}

$sql = "select count(*) from ptwait w";
$cond = "where w.progress = '1'";
$sel_count = select_from_table($con, $sql, $cond, $fname);
if ($sel_count == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$ptwait_count = pg_fetch_result($sel_count, 0, 0);

if ($ptwait_count > 0) {

	// 指定ページに表示対象が存在しなければ最大ページを表示する
	if ($page == "") $page = 1;
	$max_page = ceil($ptwait_count / ROWS_PER_PAGE);
	if ($page > $max_page) $page = $max_page;

	$sql = "select w.ptwait_id, w.ptif_id, (p.ptif_lt_kaj_nm || ' ' || p.ptif_ft_kaj_nm) as ptif_nm, p.ptif_birth, ptif_sex, m.mst_name, w.disease, w.patho_from, w.patho_to, substr(w.reception_time, 1, 8) as reception_date, w.status1, w.status2, r.ptif_id as r_ptif_id, i.ptif_id as i_ptif_id from ptwait w inner join ptifmst p on w.ptif_id = p.ptif_id left join institemmst m on w.intro_inst_cd = m.mst_cd left join inptres r on w.ptif_id = r.ptif_id left join inptmst i on w.ptif_id = i.ptif_id";
	$cond = "where w.progress = '1'";
	$offset = ($page - 1) * ROWS_PER_PAGE;
	$cond .= " order by w.reception_time desc offset $offset limit " . ROWS_PER_PAGE;
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function initPage() {
	setListDateDisabled();
}

function deleteWaitingPatient() {
	var checked = false;

	var boxes = document.delform.elements['ids[]'];
	if (boxes) {
		if (!boxes.length) boxes = [boxes];
		for (var i = 0, j = boxes.length; i < j; i++) {
			if (!boxes[i].checked) continue;
			checked = true;
			break;
		}
	}

	if (!checked) {
		alert('削除対象が選択されていません。');
		return;
	}

	if (confirm('削除します。よろしいですか？')) {
		document.delform.submit();
	}
}

function openDetail(url) {
	window.open(url, 'detail', 'width=640,height=620,scrollbars=yes');
}

function registerWaitingPatient() {
	window.open('patient_waiting_register.php?session=<? echo($session); ?>&path=b', 'wait', 'width=640,height=620,scrollbars=yes');
}

<? show_inpatient_list_common_javascript(); ?>
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
form {margin:0;}
table.submenu {border-collapse:collapse;}
table.submenu td {border:#5279a5 solid 1px;}
table.submenu td td {border-width:0;}
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="bed_menu.php?session=<? echo($session); ?>"><img src="img/icon/b17.gif" width="32" height="32" border="0" alt="病床管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="bed_menu.php?session=<? echo($session); ?>"><b>病床管理</b></a></font></td>
<? if ($section_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="entity_menu.php?session=<? echo($session); ?>&entity=1"><b>管理画面へ</b></a></font></td>
<? } else if ($ward_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="building_list.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="115" align="center" bgcolor="#bdd1e7"><a href="bed_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床管理トップ</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="ward_reference.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病棟照会</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#5279a5"><a href="inpatient_reserve_register_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>入退院登録</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="bedmake_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ベットメイク</font></a></td>
<td width="5">&nbsp;</td>
<td width="125" align="center" bgcolor="#bdd1e7"><a href="bed_rate_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床利用率管理</font></a>
</td>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#bdd1e7"><a href="bed_date_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">在院日数管理</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="bed_patient_export.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者データ抽出</font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="submenu">
<tr>
<td width="70%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>入院予定〜退院登録</b></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>検索条件設定</b></font></td>
</tr>
<tr>
<td valign="middle">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr height="22">
<td align="center"><form action="inpatient_vacant_search.php" method="get"><input type="submit" value="空床検索"><input type="hidden" name="session" value="<? echo($session); ?>"></form><br><form action="inpatient_waiting_list.php" method="get"><input type="submit" value="待機患者" disabled><input type="hidden" name="session" value="<? echo($session); ?>"></form></td>
<td align="center"><img src="img/right.gif" alt="" width="17" height="17"></td>
<td align="center"><form action="inpatient_reserve_register_list.php" method="get"><input type="submit" value="入院予定"><input type="hidden" name="session" value="<? echo($session); ?>"><input type="hidden" name="mode" value="search"></form></td>
<td align="center"><img src="img/right.gif" alt="" width="17" height="17"></td>
<td align="center"><form action="inpatient_register_list.php" method="get"><input type="submit" value="入院"><input type="hidden" name="session" value="<? echo($session); ?>"></form></td>
<td align="center"><img src="img/right.gif" alt="" width="17" height="17"></td>
<td align="center"><form action="inpatient_go_out_register_list.php" method="get"><input type="submit" value="外出・外泊"><input type="hidden" name="session" value="<? echo($session); ?>"></form></td>
<td align="center"><img src="img/right.gif" alt="" width="17" height="17"></td>
<td align="center"><form action="inpatient_move_register_list.php" method="get"><input type="submit" value="転棟"><input type="hidden" name="session" value="<? echo($session); ?>"></form></td>
<td align="center"><img src="img/right.gif" alt="" width="17" height="17"></td>
<td align="center"><form action="out_inpatient_reserve_register_list.php" method="get">
<input type="submit" value="退院予定"><input type="hidden" name="session" value="<? echo($session); ?>"></form></td>
<td align="center"><img src="img/right.gif" alt="" width="17" height="17"></td>
<td align="center"><form action="out_inpatient_register_list.php" method="get"><input type="submit" value="退院"><input type="hidden" name="session" value="<? echo($session); ?>"></form></td>
</tr>
</table>
</td>
<td valign="middle">
<? show_inpatient_list_form($con, $sel_ward, "", "", "", "", "", "", "", "", "", $session, $fname); ?>
</td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<form name="delform" action="inpatient_waiting_delete.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr valign="bottom">
<td>
<input type="button" value="待機登録" onclick="registerWaitingPatient();">
<input type="button" value="キャンセル表示" onclick="location.href = 'inpatient_waiting_cancel_list.php?session=<? echo($session); ?>';">
</td>
<td align="right">
<input type="button" value="CSV出力" onclick="document.csvform.submit();"<? if ($ptwait_count == 0) {echo(" disabled");} ?>>
<input type="button" value="削除" onclick="deleteWaitingPatient();"<? if ($ptwait_count == 0) {echo(" disabled");} ?>>
</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr><td height="2"></td></tr>
<tr bgcolor="#5279a5"><td height="1"></td></tr>
<tr><td height="2"></td></tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<? if ($ptwait_count == 0) { ?>
<p style="margin:0;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">待機患者情報はありません。</font></p>
<? } else { ?>
<? show_navigation($con, $ptwait_count, $page, $session, $fname); ?>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff">
<td width="4%"></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者ID</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者氏名</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">年齢</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">性別</font></td>
<? if ($display2 == "t") { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">紹介元医療機関</font></td>
<? } ?>
<? if ($display6 == "t") { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">傷病名</font></td>
<? } ?>
<? if ($display7 == "t") { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発症日</font></td>
<? } ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">受付日</font></td>
<? if ($display9 == "t") { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">調整状況1</font></td>
<? } ?>
<? if ($display10 == "t") { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">調整状況2</font></td>
<? } ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">詳細</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">予定登録</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院登録</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">キャンセル</font></td>
</tr>
<?
while ($row = pg_fetch_array($sel)) {
	$tmp_ptwait_id = $row["ptwait_id"];
	$tmp_ptif_id = $row["ptif_id"];
	$tmp_ptif_nm = $row["ptif_nm"];
	$tmp_age = calc_age($row["ptif_birth"]);
	$tmp_sex = format_sex($row["ptif_sex"]);
	$tmp_intro_inst_name = $row["mst_name"];
	$tmp_disease = $row["disease"];
	$tmp_patho = format_patho($row["patho_from"], $row["patho_to"]);
	$tmp_reception_date = format_date($row["reception_date"]);
	$tmp_status1 = $row["status1"];
	$tmp_status2 = $row["status2"];
	$tmp_in_res_flg = ($row["r_ptif_id"] != "");
	$tmp_in_flg = ($row["i_ptif_id"] != "");
?>
<tr height="22">
<td align="center"><input type="checkbox" name="ids[]" value="<? echo($tmp_ptwait_id); ?>"></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_ptif_id); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_ptif_nm); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_age); ?>歳</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_sex); ?></font></td>
<? if ($display2 == "t") { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_intro_inst_name); ?></font></td>
<? } ?>
<? if ($display6 == "t") { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_disease); ?></font></td>
<? } ?>
<? if ($display7 == "t") { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_patho); ?></font></td>
<? } ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_reception_date); ?></font></td>
<? if ($display9 == "t") { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_status1); ?></font></td>
<? } ?>
<? if ($display10 == "t") { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_status2); ?></font></td>
<? } ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="patient_waiting_detail.php?session=<? echo($session); ?>&id=<? echo($tmp_ptwait_id); ?>&func=bed" onclick="openDetail(this.href); return false;">詳細</a></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if ($tmp_in_res_flg || $tmp_in_flg) { ?><font color="gray"><? } else { ?><a href="inpatient_reserve_register.php?session=<? echo($session); ?>&pt_id=<? echo($tmp_ptif_id); ?>&ptwait_id=<? echo($tmp_ptwait_id); ?>"><? } ?>予定登録<? if ($tmp_in_res_flg || $tmp_in_flg) { ?></font><? } else { ?></a><? } ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if ($tmp_in_flg) { ?><font color="gray"><? } else { ?><a href="inpatient_register.php?session=<? echo($session); ?>&pt_id=<? echo($tmp_ptif_id); ?>&ptwait_id=<? echo($tmp_ptwait_id); ?>"><? } ?>入院登録<? if ($tmp_in_flg) { ?></font><? } else { ?></a><? } ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="patient_waiting_detail.php?session=<? echo($session); ?>&id=<? echo($tmp_ptwait_id); ?>&func=bed&progress=2" onclick="openDetail(this.href); return false;">キャンセル</a></font></td>
</tr>
<?
}
?>
</table>
<? } ?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="href" value="inpatient_waiting_list.php?session=<? echo($session); ?>&page=<? echo($page); ?>">
</form>
<form name="csvform" action="inpatient_waiting_csv.php" method="get" target="download">
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
<iframe name="download" width="0" height="0" frameborder="0"></iframe>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
function show_navigation($con, $count, $page, $session, $fname) {
	$max_page = ceil($count / ROWS_PER_PAGE);
	$start = ($page - 1) * ROWS_PER_PAGE + 1;
	$end = $page * ROWS_PER_PAGE;
	if ($end > $count) $end = $count;

	echo("<p style=\"margin:0 0 3px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><span style=\"margin-right:15px;\">");
	if ($page > 1) {
		echo("<a href=\"?session=$session&page=" . ($page - 1) . "\">");
	} else {
		echo("<font color=\"gray\">");
	}
	echo("←前のページ");
	if ($page > 1) {
		echo("</a>");
	} else {
		echo("</font>");
	}
	echo("</span>{$count}件中 {$start}");
	if ($start != $end) {
		echo("〜{$end}");
	}
	echo("件目を表示中<span style=\"margin-left:15px;\">");
	if ($page < $max_page) {
		echo("<a href=\"?session=$session&page=" . ($page + 1) . "\">");
	} else {
		echo("<font color=\"gray\">");
	}
	echo("次のページ→");
	if ($page < $max_page) {
		echo("</a>");
	} else {
		echo("</font>");
	}
	echo("</span></font></p>");
}

function calc_age($birth) {
	$age = date("Y") - substr($birth, 0, 4);
	if (date("md") < substr($birth, 4, 8)) $age--;
	return $age;
}

function format_sex($sex) {
	switch ($sex) {
	case "1":
		return "男性";
	case "2":
		return "女性";
	default:
		return "不明";
	}
}

function format_patho($from, $to) {
	if ($from == "" && $to == "") return "";
	$days = array(format_date($from), format_date($to));
	return join(" 〜 ", $days);
}

function format_date($date) {
	return preg_replace("/\A(\d{4})(\d{2})(\d{2})\z/", "$1/$2/$3", $date);
}
