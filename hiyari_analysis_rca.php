<?
ob_start();
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");
require_once("hiyari_common.ini");
require_once("hiyari_reporting_common.ini");
require_once("hiyari_mail.ini");
require_once("hiyari_db_class.php");
require_once("hiyari_auth_class.php");
require_once("hiyari_report_class.php");
require_once("sot_util.php");
ob_end_clean();


//==============================
//初期処理
//==============================

$font = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';


//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// 権限チェック
$summary = check_authority($session, 47, $fname);
if ($summary == "0") {
    showLoginPage();
    exit;
}

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// DBクラスのインスタンス化
$db = new hiyari_db_class($con, $fname);

// レポートクラスのインスタンス化
$rep_obj = new hiyari_report_class($con, $fname);

$auth_obj = new hiyari_auth_class($con, $fname, $session);

if(!empty($a_id)) $analysis_id = $a_id;

$sql = "SELECT * FROM inci_analysis_regist WHERE analysis_id = " . $analysis_id;
$db->query($sql);
$analysis_regist = $db->getRow();

// 分析事案からコピーするためのデータを取得
$report_data = $rep_obj->get_report($analysis_regist['analysis_problem_id']);
$vals = $report_data['content']['input_items'];

@list($cp_year, $cp_month, $cp_day) = explode("/", $vals[100][5][0]);

switch($vals[100][40][0]) {
case 2:
    $cp_hour = 2;
    break;
case 3:
    $cp_hour = 4;
    break;
case 4:
    $cp_hour = 6;
    break;
case 5:
    $cp_hour = 8;
    break;
case 6:
    $cp_hour = 10;
    break;
case 7:
    $cp_hour = 12;
    break;
case 8:
    $cp_hour = 14;
    break;
case 9:
    $cp_hour = 16;
    break;
case 10:
    $cp_hour = 18;
    break;
case 11:
    $cp_hour = 20;
    break;
case 12:
    $cp_hour = 22;
    break;
default :
    $cp_hour = 0;
    break;
}

if($_POST['is_postback'] == true) {
    // 添付ファイルの確認
    create_analysisfile_folder();
    $analysisfile_folder_name = get_analysisfile_folder_name();
    for ($i = 0; $i < count($filename); $i++) {
        $tmp_file_id = $file_id[$i];
        $ext = strrchr($filename[$i], ".");

        $tmp_filename = "{$analysisfile_folder_name}/tmp/{$session}_{$tmp_file_id}{$ext}";
        if (!is_file($tmp_filename)) {
            echo("<script language=\"javascript\">alert('添付ファイルが削除された可能性があります。\\n再度添付してください。');</script>");
            echo("<script language=\"javascript\">location.href='hiyari_analysis_general.php?session={$session}&analysis_id={$analysis_id}'</script>");
            exit;
        }
    }

    if($_POST['mode'] == 'regist') {
        $sql = "SELECT count(*) FROM inci_analysis_rca_date WHERE analysis_id = " . $analysis_id;
        $db->query($sql);
        $count = $db->getOne();

        if($count == 0) {
/*
            foreach($analysis_cause as $key => $value) {
                $sql = "INSERT INTO inci_analysis_rca_form(analysis_id, analysis_order, analysis_cause, analysis_plan) VALUES( "; //)
                $content = array($analysis_id, $key, $analysis_cause[$key], $analysis_plan[$key]);
                $db->insert($sql, $content);
            }
*/
            foreach($analysis_year as $key => $value) {
                $sql = "INSERT INTO inci_analysis_rca_date(analysis_id, analysis_order, analysis_year, analysis_month, analysis_day, analysis_hour, analysis_minute, analysis_phenomenon) VALUES ( "; //)
                $content = array(
                    $analysis_id,
                    $key,
                    $analysis_year[$key],
                    $analysis_month[$key],
                    $analysis_day[$key],
                    $analysis_hour[$key],
                    $analysis_minute[$key],
                    pg_escape_string($analysis_phenomenon[$key])
                );
                $db->insert($sql, $content);
            }
        } else {

            // 日時、事象についてはDELETEしINSERTする
            $sql  = "DELETE FROM inci_analysis_rca_date ";
            $cond = "WHERE analysis_id = " . $analysis_id;
            $db->delete($sql, $cond);

            foreach($analysis_year as $key => $value) {
                $sql = "INSERT INTO inci_analysis_rca_date(analysis_id, analysis_order, analysis_year, analysis_month, analysis_day, analysis_hour, analysis_minute, analysis_phenomenon) VALUES ( "; //)
                $content = array(
                    $analysis_id, 
                    $key, 
                    $analysis_year[$key], 
                    $analysis_month[$key], 
                    $analysis_day[$key], 
                    $analysis_hour[$key], 
                    $analysis_minute[$key], 
                    pg_escape_string($analysis_phenomenon[$key])
                );
                $db->insert($sql, $content);
            }
        }

        $db->delete("DELETE FROM inci_analysis_rca_form WHERE analysis_id = ".$analysis_id, "");

        for ($i= 1; $i <= (int)@$_REQUEST["taisaku_count"]; $i++){
            $emp_id_list = array();
            $emp_name_list = array();
            $emp_idx = 0;
            for(;;){
                $emp_idx++;
                $tmp_emp_id = @$_REQUEST["analysis_rca_emp_id_".$i."_".$emp_idx];
                if (!$tmp_emp_id || $emp_idx > 100) break;
                $emp_id_list[] = $tmp_emp_id;
                $emp_name_list[] = @$_REQUEST["analysis_rca_emp_name_".$i."_".$emp_idx];
            }
            $kijitu =
                sprintf('%04d', @$_REQUEST["analysis_kijitu_y_".$i]).
                sprintf('%02d', @$_REQUEST["analysis_kijitu_m_".$i]).
                sprintf('%02d', @$_REQUEST["analysis_kijitu_d_".$i]);


            $sql =
                " INSERT INTO inci_analysis_rca_form (".
                " analysis_id, analysis_order, analysis_cause, analysis_plan".
                ",analysis_kijitu, analysis_yusenjun, analysis_hiyou".
                ",analysis_sincyoku, analysis_rca_emp_id_list, analysis_rca_emp_name_list".
                " ) VALUES (".
                " ".$analysis_id.
                ",".$i.
                ",'".pg_escape_string(@$_REQUEST["analysis_cause_".$i])."'".
                ",'".pg_escape_string(@$_REQUEST["analysis_plan_".$i])."'".
                ",'".pg_escape_string($kijitu)."'".
                ",'".pg_escape_string(@$_REQUEST["analysis_yusenjun_".$i])."'".
                ",'".pg_escape_string(@$_REQUEST["analysis_hiyou_".$i])."'".
                ",'".pg_escape_string(@$_REQUEST["analysis_sincyoku_".$i])."'".
                ",'".pg_escape_string(implode(",",$emp_id_list))."'".
                ",'".pg_escape_string(implode(",",$emp_name_list))."'".
                ")";
            update_set_table($con, $sql, array(), 0, "", $fname);
        }

        $sql = "UPDATE inci_analysis_regist SET ";
        $set = array('analysis_summary');
        $dat = array($analysis_summary);
        $cond = "WHERE analysis_id = {$analysis_id}";
        $db->update($sql, $set, $dat, $cond, true);

    } else if($_POST['mode'] == 'add') {
        // 行追加の時の処理
        //$analysis_year[count($analysis_year)] = "";
        array_insert($analysis_year,   $cp_year, $key+1);
        array_insert($analysis_month,  $cp_month, $key+1);
        array_insert($analysis_day,    $cp_day, $key+1);
        array_insert($analysis_hour,   $cp_hour, $key+1);
        array_insert($analysis_minute, '', $key+1);
        array_insert($analysis_phenomenon, '', $key+1);
//  } else if($_POST['mode'] == 'add_form') {
//      array_insert($analysis_cause, '', $key+1);
//      array_insert($analysis_plan,  '', $key+1);
//
//  } else if($_POST['mode'] == 'delete') {
//      // 行削除の時の処理
    }

    // 添付ファイル情報を削除
    $sql = "delete from inci_analysisfile";
    $cond = "where analysis_id = '$analysis_id'";
    $del = delete_from_table($con, $sql, $cond, $fname);
    if ($del == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    // 添付ファイル情報を作成
    $no = 1;
    if (is_array($filename)){
        foreach ($filename as $tmp_filename) {
            $sql = "insert into inci_analysisfile (analysis_id, analysisfile_no, analysisfile_name) values (";
            $content = array($analysis_id, $no, $tmp_filename);
            $ins = insert_into_table($con, $sql, $content, $fname);
            if ($ins == 0) {
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
            $no++;
        }
    }


    // 添付ファイルの移動
    foreach (glob("{$analysisfile_folder_name}/{$analysis_id}_*.*") as $tmpfile) {
        unlink($tmpfile);
    }
    for ($i = 0; $i < count($filename); $i++) {
        $tmp_file_id = $file_id[$i];
        $tmp_filename = $filename[$i];
        $tmp_fileno = $i + 1;
        $ext = strrchr($tmp_filename, ".");

        $tmp_filename = "{$analysisfile_folder_name}/tmp/{$session}_{$tmp_file_id}{$ext}";
        copy($tmp_filename, "{$analysisfile_folder_name}/{$analysis_id}_{$tmp_fileno}{$ext}");
    }
    foreach (glob("{$analysisfile_folder_name}/tmp/{$session}_*.*") as $tmpfile) {
        unlink($tmpfile);
    }
} else {
/*
    $sql = "SELECT * FROM inci_analysis_rca_form WHERE analysis_id = {$analysis_id} ORDER BY analysis_order ASC";
    $db->query($sql);
    $rca_form = $db->getAll();
*/
    $analysis_summary = $analysis_regist['analysis_summary'];
/*
    if(!empty($rca_form)) {
        foreach($rca_form as $key => $value) {
            $analysis_cause[$key] = $value['analysis_cause'];
            $analysis_plan[$key]  = $value['analysis_plan'];
        }
    }
*/
    $sql = "SELECT * FROM inci_analysis_rca_date WHERE analysis_id = {$analysis_id} ORDER BY analysis_order ASC";
    $db->query($sql);
    $rca_date = $db->getAll();

    if(!empty($rca_date)) {
        foreach($rca_date as $key => $value) {
            $analysis_year[$key]   = $value['analysis_year'];
            $analysis_month[$key]  = $value['analysis_month'];
            $analysis_day[$key]    = $value['analysis_day'];
            $analysis_hour[$key]   = $value['analysis_hour'];
            $analysis_minute[$key] = $value['analysis_minute'];
            $analysis_phenomenon[$key] = $value['analysis_phenomenon'];
        }
    }
}

create_analysisfile_folder();
$analysisfile_folder_name = get_analysisfile_folder_name();

// 添付ファイル情報を取得
$sql = "select analysisfile_no, analysisfile_name from inci_analysisfile";
$cond = "where analysis_id = $analysis_id order by analysisfile_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showErrorPage(window);</script>");
    exit;
}
$file_id = array();
$filename = array();
while ($row = pg_fetch_array($sel)) {
    $tmp_file_no = $row["analysisfile_no"];
    $tmp_filename = $row["analysisfile_name"];

    array_push($file_id, $tmp_file_no);
    array_push($filename, $tmp_filename);

    // 一時フォルダにコピー
    $ext = strrchr($tmp_filename, ".");
    copy("{$analysisfile_folder_name}/{$analysis_id}_{$tmp_file_no}{$ext}", "{$analysisfile_folder_name}/tmp/{$session}_{$tmp_file_no}{$ext}");
}

// member_idで編集可能かcheckする
$emp_id  = get_emp_id($con,$session,$fname);

$sql = "SELECT analysis_member_id FROM inci_analysis_regist WHERE analysis_id = " . $analysis_id;
$db->query($sql);
$analysis_member_id = $db->getOne();

$arr_member_id = explode(",", $analysis_member_id);
$update_flg = false;
foreach($arr_member_id as $member_id) {
    if($member_id == $emp_id) $update_flg = true;
}

if($update_flg == false) $update_flg = is_sm_emp($session,$fname,$con);


// 日付のセット
$first_year = '2010';
$this_year  = date("Y");

for($i=$this_year; $i>=$first_year; $i--) {
    $year[] = $i;
}

for($i=1; $i<=12; $i++) {
    $month[] = $i;
}

for($i=1; $i<=31; $i++) {
    $day[] = $i;
}

for($i=0; $i<=23; $i++) {
    $hour[] = $i;
}

for($i=0; $i<=59; $i++) {
    $minute[] = sprintf("%02d", $i);
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ファントルくん | 出来事分析</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<style type="text/css">
form {margin:0;}
table.list {border-collapse:collapse;}
tr.list td {border:solid #35B341 1px;border-bottom:0;}
table.list td {border:solid #35B341 1px;}

.table_taisaku td { vertical-align:top}
</style>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="loadAction();bodyResized();" onresize="bodyResized()">

<script>

function loadAction() {
    start_auto_session_update();
}

function start_auto_session_update()
{
    //1分(60*1000ms)後に開始
    setTimeout(auto_sesson_update,60000);
}
function auto_sesson_update()
{
    //セッション更新
    document.session_update_form.submit();

    //1分(60*1000ms)後に最呼び出し
    setTimeout(auto_sesson_update,60000);
}


function getClientHeight(){
    if (document.body.clientHeight) return document.body.clientHeight;
    if (window.innerHeight) return window.innerHeight;
    if (document.documentElement && document.documentElement.clientHeight) {
        return document.documentElement.clientHeight;
    }
    return 0;
}

var TOP_TO_LEFT_CONTENT_HEIGHT = 160;
var TOP_TO_RIGHT_CONTENT_HEIGHT = 160 + 32;
function bodyResized(){
    var h = getClientHeight();
    if (h < 1) return;
    document.getElementById("left_scroll_div").style.height = (h - TOP_TO_LEFT_CONTENT_HEIGHT) + "px";
    document.getElementById("right_scroll_div").style.height = (h - TOP_TO_RIGHT_CONTENT_HEIGHT) + "px";

}

function add_date(OBJ, KEY) {
    OBJ.mode.value = "add";
    OBJ.key.value = KEY;
    OBJ.submit();
}

function delete_date(OBJ, KEY) {
    OBJ.mode.value = "delete";
    OBJ.key.value = KEY;
    OBJ.submit();
}
/*
function add_form(OBJ, KEY) {
    OBJ.mode.value = "add_form";
    OBJ.key.value = KEY;
    OBJ.submit();
}

function delete_form(OBJ, KEY) {
    OBJ.mode.value = "delete_form";
    OBJ.key.value = KEY;
    OBJ.submit();
}
*/

var reference_window = null;
function reference(idx){
    var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,width=600,height=400";
    var url = "hiyari_analysis_rca_copy_from_chart.php?a_id=<?=$analysis_id?>&field_idx="+idx+"&session=<?=$session?>";
    if (reference_window && !reference_window.closed) {
        reference_window.location.href = url;
        reference_window.focus();
        return;
    }
    reference_window = window.open(url, 'hiyari_analysis_rca_copy_from_chart', option);
}

function resultReference(idx, str){
    if (document.getElementById("analysis_cause_"+idx)){
        document.getElementById("analysis_cause_"+idx).value = document.getElementById("analysis_cause_"+idx).value + str;
    }
}

function window_open_rca(isIngaMode)
{
    var h = window.screen.availHeight - 30;
    var w = window.screen.availWidth - 10;
    var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;
    var ingaParam = (isIngaMode ? "&inga_mode=yes" : "");
    var url = "hiyari_analysis_rca_chart.php?a_id=<?=$analysis_id?>&session=<?=$session?>" + ingaParam;
    window.open(url, 'hiyari_analysis_rca_chart', option);
}

</script>

<!-- ヘッダー START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr height="32" bgcolor="#35B341">
    <td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>ＲＣＡ分析画面</b></font></td>
    <td>&nbsp</td>
    <td width="10">&nbsp</td>
    <td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる"
        width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
    </tr>
</table>


<!-- ヘッダー END -->
<script>
    function attachFile() {
        window.open('hiyari_analysis_attach.php?session=<?=$session?>', 'newwin', 'width=640,height=480,scrollbars=yes');
    }
    function detachFile(e) {
        if (e == undefined) e = window.event;
        var btn_id = (e.target) ? e.target.getAttribute('id') : e.srcElement.id;
        var id = btn_id.replace('btn_', '');
        var p = document.getElementById('p_' + id);
        document.getElementById('attach').removeChild(p);
    }
    function analysisPrint() {
        window.open('hiyari_analysis_print.php?session=<?=$session?>&analysis_id=<?=$analysis_id?>&method=rca');
    }
</script>

<form name="session_update_form" action="hiyari_session_update.php" method="post" target="session_update_frame">
<input type="hidden" name="session" value="<?=$session?>">
</form>
<iframe name="session_update_frame" width="0" height="0" frameborder="0"></iframe>


<form name="form_list" action="hiyari_analysis_rca.php" method="POST" enctype="multipart/form-data">



<!-- 実行アイコン・分析事業コード -->
<div style="margin-top:20px">

    <?if($update_flg == true) { ?>
    <div style="float:left; width:50px; padding-left:8px; text-align:center">
        <img src="img/hiyari_rp_shitagaki.gif" style="cursor: pointer;" onclick="javascript:document.form_list.submit();alert('保存しました');"><br/>
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">更新</font>
    </div>
    <?}?>


    <!-- 実行アイコン -->
    <div style="float:left; width:50px; padding-left:8px; text-align:center">
        <img src="img/hiyari_rp_print.gif" height="30" style="cursor: pointer;" onclick="javascript:analysisPrint();"><br/>
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">印刷</font>
    </div>


    <!-- 分析事業コード -->
    <div style="float:right; width:220px; padding-right:8px; padding-top:4px">
        <table border="0" cellspacing="0" cellpadding="2" class="list" style="width:100%">
            <tr>
                <td bgcolor="#DFFFDC" style="width:64px; text-align:center"><?=$font?>分析番号</font></td>
                <td style="text-align:center; padding:8px"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><?=$analysis_regist['analysis_no']?></font></td>
            </tr>
        </table>
    </div>


    <div style="clear:both"></div>

</div>



<!-- メインコンテンツ左右 開始 -->
<table style="width:100%"><tr>


<!-- メインコンテンツ左 -->
<td style="padding:4px; padding-left:8px; vertical-align:top">

    <div style="white-space:nowrap"><span style="border:1px solid #ff6a55; color:#000; background-color:#e4756b; padding:1px 6px;">
        <?=$font?><span style="color:#fff">報 告 内 容</span></font></span>
        <?=$font?>事案番号</font>
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><?=$analysis_regist['analysis_problem']?></font>
    </div>

    <!-- 報告内容 START -->
    <div id="left_scroll_div" style="height:590px; overflow:scroll; border:#CCCCCC solid 1px; padding:6px; margin-top:3px; background-color:#eee;">

        <?
            $contents = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="red">権限がないため表示できません</font>';
            if($auth_obj->get_analysis_disp_auth($analysis_regist['analysis_problem_id']) || $update_flg) {
                $contents = get_report_html($con,$fname,$analysis_regist['analysis_problem_id']);
            } else if($analysis_regist['analysis_problem_id'] == "") {
                $contents = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="red">分析事案を選択してください</font>';
            }
            echo wrapYellowNarrowBox($contents);
        ?>
        <!-- 報告内容内容 END -->


    </div>

</td>



<!-- メインコンテンツ右側 -->
<td style="padding:4px; width:800px">


    <div><span style="border:1px solid #ff6a55; color:#000; background-color:#e4756b; padding:1px 6px;">
        <?=$font?><span style="color:#fff">Ｒ Ｃ Ａ 分 析</span></font></span>
    </div>


    <div style="margin-top:3px; height:22px;">
        <table border="0" cellspacing="0" width="100%" class="list">
            <tr>
                <td bgcolor="#DFFFDC" align="center" width="108px"><?=$font?>タイトル</font></td>
                <td bgcolor="#efffee" style="padding:2px; padding-left:4px"><?=$analysis_regist['analysis_title']?></td>
            </tr>
        </table>
    </div>


    <!-- スクロール -->
    <div id="right_scroll_div" style="height:558px; overflow:scroll; border:#CCCCCC solid 1px; padding:6px; margin-top:10px; background-color:#eee">


        <table border="0" cellspacing="0" cellpadding="2" width="100%" class="list" style="background-color:#fff">
            <tr>
                <td bgcolor="#DFFFDC" align="center" width="100px"><?=$font?>概要</font></td>
                <td><textarea style="width:100%;" rows="5" name="analysis_summary"><?=$analysis_summary?></textarea></td>
            </tr>
            <tr>
                <td bgcolor="#DFFFDC" align="center"><?=$font?>添付ファイル</font></td>
                <td align="right" bgcolor="#DFFFDC">
                        <input type="button" value="ファイル選択" onClick="attachFile()">
                    <div id="attach">
                    <?
                    for ($i = 0; $i < count($filename); $i++)
                    {
                        $tmp_file_id = $file_id[$i];
                        $tmp_filename = $filename[$i];
                        $ext = strrchr($tmp_filename, ".");

                        ?>
                        <p id="p_<?=$tmp_file_id?>" class="attach">
                        <a href="<?=$analysisfile_folder_name?>/tmp/<?=$session?>_<?=$tmp_file_id?><?=$ext?>" target="_blank"><?=$tmp_filename?></a>
                        <input type="button" id="btn_<?=$tmp_file_id?>" name="btn_<?=$tmp_file_id?>" value="削除" onclick="detachFile(event);">
                        <input type="hidden" name="filename[]" value="<?=$tmp_filename?>">
                        <input type="hidden" name="file_id[]" value="<?=$tmp_file_id?>">
                        </p>
                        <?
                    }
                    ?>
                    </div>
                </td>
            </tr>
        </table>

        <div style="margin-top:20px"><?=$font?><b style="color:#555">１．経過表</b></font></div>
        <table border="0" cellspacing="0" cellpadding="2" width="100%" class="list" style="background-color:#fff;">
            <tr>
                <td bgcolor="#DFFFDC" align="center" width="300px"><?=$font?>日時</font></td>
                <td bgcolor="#DFFFDC" align="center"><?=$font?>事象</font></td>
                <td bgcolor="#DFFFDC" width="64px"></td>
            </tr>
<?
for($i=0; $i<count($analysis_year); $i++) {
    //echo 'DK:',$delete_key;
    if("$i" === $key && $mode == 'delete') continue;
?>
            <tr>
                <td>
                    <select name="analysis_year[]">
                        <? foreach($year as $value) { ?><option value='<?=$value?>'<?if($analysis_year[$i] == $value) {?> selected<?}?>><?=$value?></option><?}?>
                    </select>
                    /
                    <select name="analysis_month[]">
                        <? foreach($month as $value) { ?><option value='<?=$value?>'<?if($analysis_month[$i] == $value) {?> selected<?}?>><?=$value?></option><?}?>
                    </select>
                    /
                    <select name="analysis_day[]">
                        <? foreach($day as $value) { ?><option value='<?=$value?>'<?if($analysis_day[$i] == $value) {?> selected<?}?>><?=$value?></option><?}?>
                    </select>
                    <select name="analysis_hour[]" style="margin-left:10px">
                        <? foreach($hour as $value) { ?><option value='<?=$value?>'<?if($analysis_hour[$i] == $value) {?> selected<?}?>><?=$value?></option><?}?>
                    </select>
                    ：
                    <select name="analysis_minute[]">
                        <? foreach($minute as $value) { ?><option value='<?=$value?>'<?if($analysis_minute[$i] == $value) {?> selected<?}?>><?=$value?></option><?}?>
                    </select>
                </td>
                <td><textarea name="analysis_phenomenon[]" rows=2 style="width:100%"><?=$analysis_phenomenon[$i]?></textarea></td>
                <td bgcolor="#DFFFDC" style="text-align:center">
                    <input type="button" value="追加" onClick="add_date(this.form, <?=$i?>);"><br/>
                    <? if ($i>0){ ?>
                    <input type="button" value="削除" onClick="delete_date(this.form, <?=$i?>)" style="margin-top:4px">
                    <? } ?>
                </td>
            </tr>
<?
}
if(count($analysis_year) == 0) {
?>
            <tr>
                <td>
                    <select name="analysis_year[]">
                        <? foreach($year as $value) { ?>
                            <option value="<?=$value?>"<?if($cp_year == $value) {?> selected<?}?>><?=$value?></option>
                        <?} ?>
                    </select>
                    /
                    <select name="analysis_month[]">
                        <? foreach($month as $value) { ?>
                            <option value="<?=$value?>"<?if($cp_month == $value) {?> selected<?}?>><?=$value?></option>
                        <?} ?>
                    </select>
                    /
                    <select name="analysis_day[]">
                        <? foreach($day as $value) { ?>
                            <option value="<?=$value?>"<?if($cp_day == $value) {?> selected<?}?>><?=$value?></option>
                        <?} ?>
                    </select>
                    <select name="analysis_hour[]">
                        <? foreach($hour as $value) { ?>
                            <option value="<?=$value?>"<?if($cp_hour == $value) {?> selected<?}?>><?=$value?></option>
                        <?} ?>
                    </select>
                    :
                    <select name="analysis_minute[]">
                        <? foreach($minute as $value) { echo "<option value='{$value}'>$value</option>"; } ?>
                    </select>
                </td>
                <td><textarea name="analysis_phenomenon[]" rows=2 style="width:100%"></textarea></td>
                <td bgcolor="#DFFFDC">
                    <input type="button" value="追加" onClick="add_date(this.form, <?=$i+1?>);">
                    <!--input type="button" value="削除" onClick="delete_date(this.form, <?=$i+1?>)"-->
                </td>
            </tr>
<?}?>
        </table>





        <div style="margin-top:20px"><?=$font?><b style="color:#555">２．ＲＣＡ図</b></font></div>
        <table border="0" cellspacing="0" cellpadding="2" width="100%" class="list" style="background-color:#fff;">
            <tr>
                <td bgcolor="#DFFFDC" align="right"><input type="button" value="ＲＣＡ図の作成" onClick="window_open_rca('')"><input type="button" value="因果図の作成" onClick="window_open_rca('inga')"></td>
            </tr>
        </table>




        <div style="margin-top:20px"><?=$font?><b style="color:#555">３．根本原因・対策</b></font></div>
        <table border="0" cellspacing="0" cellpadding="2" width="100%" class="list" style="background-color:#fff;">
            <tr>
                <td bgcolor="#DFFFDC" width="35%" align="center"><?=$font?>根本原因</font></td>
                <td bgcolor="#DFFFDC" width="39%" align="center"><?=$font?>対策</font></td>
                <td bgcolor="#DFFFDC" width="15%" align="center"><?=$font?>担当者</font></td>
                <td bgcolor="#DFFFDC" width="11%" align="center"></td>
            </tr>

            <tbody id="tbody_taisaku">
            <? $sql = "SELECT * FROM inci_analysis_rca_form WHERE analysis_id = ".$analysis_id." ORDER BY analysis_order ASC"; ?>
            <? $db->query($sql); ?>
            <? $rca_form = $db->getAll(); ?>
            <? $taisaku_count = max(1, count((array)$rca_form)); ?>
            <? for($i = 1; $i <= $taisaku_count; $i++) { ?>
            <?   $row = @$rca_form[$i-1]; ?>
            <?   $ymd_y = substr(@$row["analysis_kijitu"], 0, 4); ?>
            <?   $ymd_m = substr(@$row["analysis_kijitu"], 4, 2); ?>
            <?   $ymd_d = substr(@$row["analysis_kijitu"], 6, 3); ?>
            <?   if (!$row) {$ymd_y = (date("Y")+1); $ymd_m = date("n"); $ymd_d = date("j"); } ?>
            <?   $emp_id_list = explode(",", $row["analysis_rca_emp_id_list"]); ?>
            <?   $emp_name_list = explode(",", $row["analysis_rca_emp_name_list"]); ?>
            <?   if (!trim($row["analysis_rca_emp_id_list"])) { $emp_id_list = array(); $emp_name_list = array(); } ?>

            <tr id="tr_taisaku_<?=$i?>" class="table_taisaku">
                <td><textarea style="width:100%;" rows="5" id="analysis_cause_<?=$i?>" name="analysis_cause_<?=$i?>"><?=@$row["analysis_cause"]?></textarea></td>
                <td>
                    <div><?=$font?>
                        評価期日
                        <select name="analysis_kijitu_y_<?=$i?>" id="analysis_kijitu_y_<?=$i?>"><?= $c_sot_util->generate_dropdown_options_y($ymd_y); ?></select>
                        /
                        <select name="analysis_kijitu_m_<?=$i?>" id="analysis_kijitu_m_<?=$i?>"><?= $c_sot_util->generate_dropdown_options_m($ymd_m); ?></select>
                        /
                        <select name="analysis_kijitu_d_<?=$i?>" id="analysis_kijitu_d_<?=$i?>"><?= $c_sot_util->generate_dropdown_options_d($ymd_d); ?></select>
                    </font></div>
                    <div style="white-space:nowrap"><?=$font?>
                        優先順位
                        <select id="analysis_yusenjun_<?=$i?>" name="analysis_yusenjun_<?=$i?>">
                            <option value=""></option>
                            <? for ($yusen_idx = 1; $yusen_idx <= 9; $yusen_idx++){ ?>
                                <option value="<?=$yusen_idx?>" <?= $yusen_idx==@$row["analysis_yusenjun"]? "selected": ""?>><?=$yusen_idx?></option>
                            <? } ?>
                        </select>
                        費用
                        <input type="text" id="analysis_hiyou_<?=$i?>" name="analysis_hiyou_<?=$i?>" value="<?=@$row["analysis_hiyou"]?>" style="width:50px;" />
                        進捗
                        <select id="analysis_sincyoku_<?=$i?>" name="analysis_sincyoku_<?=$i?>">
                            <option value=""></option>
                            <option value="分析中" <?= @$row["analysis_sincyoku"]=="分析中" ? "selected": ""?>>分析中</option>
                            <option value="分析済" <?= @$row["analysis_sincyoku"]=="分析済" ? "selected": ""?>>分析済</option>
                            <option value="評価中" <?= @$row["analysis_sincyoku"]=="評価中" ? "selected": ""?>>評価中</option>
                            <option value="評価済" <?= @$row["analysis_sincyoku"]=="評価済" ? "selected": ""?>>評価済</option>
                        </select>
                    </font></div>
                    <textarea style="width:100%;" rows="2" id="analysis_plan_<?=$i?>" name="analysis_plan_<?=$i?>"><?=@$row["analysis_plan"]?></textarea>
                </td>

                <td id="td_analysis_rca_emp_list_<?=$i?>">
                <? for ($emp_idx = 1; $emp_idx <= count($emp_id_list); $emp_idx++){ ?>
                    <div id="div_analysis_rca_emp_<?=$i?>_<?=$emp_idx?>">
                    <?=$font?>
                    <a href="" onclick="deleteRcaEmpEntry(<?=$i?>,<?=$emp_idx?>,this); return false;"><?=h($emp_name_list[$emp_idx-1])?></a>
                    <input type="hidden" name="analysis_rca_emp_id_<?=$i?>_<?=$emp_idx?>" id="analysis_rca_emp_id_<?=$i?>_<?=$emp_idx?>" value="<?=$emp_id_list[$emp_idx-1]?>" />
                    <input type="hidden" name="analysis_rca_emp_name_<?=$i?>_<?=$emp_idx?>" id="analysis_rca_emp_name_<?=$i?>_<?=$emp_idx?>" value="<?=h($emp_name_list[$emp_idx-1])?>" />
                    </font></div>
                <? } ?>
                </td>
                <td bgcolor="#DFFFDC" style="text-align:center;">
                    <input type="button" value="根本原因" onClick="reference(<?=$i?>);">
                    <input type="button" value="職員名簿" onClick="popupEmpSelector(<?=$i?>);">
                    <input type="button" value="追加" onClick="adjustTaisakuArea('add', <?=$i?>);">
                    <? if ($i>1){ ?>
                    <input type="button" value="削除" onClick="adjustTaisakuArea('del', <?=$i?>);">
                    <? } ?>
                </td>
            </tr>
            <? } ?>
            </tbody>

        </table>
        <input type="hidden" name="mode" value="regist">
        <input type="hidden" name="taisaku_count" id="taisaku_count" value="<?=$taisaku_count?>">
        <input type="hidden" name="session" value="<?=$session?>">
        <input type="hidden" name="is_postback" value="true">
        <input type="hidden" name="analysis_id" value="<?=$analysis_id?>">
        <input type="hidden" name="key" value="">
    </div>
</td>

</tr></table>

<script type="text/javascript">

function adjustTaisakuArea(add_or_del, index){
    var tbl = document.getElementById("tbody_taisaku");
    var taisaku_count = document.getElementById("taisaku_count").value;

    <? // 行追加の場合、カラのTRを末尾に追加 ?>
    if (add_or_del=="add"){
        taisaku_count++;
        var td2Options = '<option value=""></option>';
        for (var i = 1; i <= 9; i++) td2Options += '<option value="'+i+'">'+i+'</option>';
        var newTR  = document.createElement("tr");
        var newTD1 = document.createElement("td");
        var newTD2 = document.createElement("td");
        var newTD3 = document.createElement("td");
        var newTD4 = document.createElement("td");
        newTR.id = "tr_taisaku_"+taisaku_count;
        newTD1.innerHTML =
            '<textarea style="width:100%;" rows="5" id="analysis_cause_'+taisaku_count+'" name="analysis_cause_'+taisaku_count+'"></textarea>';
        newTD2.innerHTML =
            '<div><?=$font?>' +
            '評価期日 '+
            '<select name="analysis_kijitu_y_'+taisaku_count+'" id="analysis_kijitu_y_'+taisaku_count+'">'+
            '<?= str_replace("\n","",$c_sot_util->generate_dropdown_options_y("")); ?></select>' +
            '&nbsp;/&nbsp;'+
            '<select name="analysis_kijitu_m_'+taisaku_count+'" id="analysis_kijitu_m_'+taisaku_count+'">'+
            '<?= str_replace("\n","",$c_sot_util->generate_dropdown_options_m("")); ?></select>' +
            '&nbsp;/&nbsp;'+
            '<select name="analysis_kijitu_d_'+taisaku_count+'" id="analysis_kijitu_d_'+taisaku_count+'">'+
            '<?= str_replace("\n","",$c_sot_util->generate_dropdown_options_d("")); ?></select>' +
            '</font></div>' +
            '<div><?=$font?>' +
            ' 優先順位 ' +
            '<select id="analysis_yusenjun_'+taisaku_count+'" name="analysis_yusenjun_'+taisaku_count+'">'+td2Options+'</select>'+
            ' 費用 ' +
            '<input type="text" id="analysis_hiyou_'+taisaku_count+'" name="analysis_hiyou_'+taisaku_count+'" value="" style="width:50px; ime-mode:disabled" />' +
            ' 進捗 ' +
            '<select id="analysis_sincyoku_'+taisaku_count+'" name="analysis_sincyoku_'+taisaku_count+'">'+
            '<option value=""></option>'+
            '<option value="分析中">分析中</option>'+
            '<option value="分析済">分析済</option>'+
            '<option value="評価中">評価中</option>'+
            '<option value="評価済">評価済</option>'+
            '</select>'+
            '</font></div>' +
            '<textarea style="width:100%;" rows="2" id="analysis_plan_'+taisaku_count+'" name="analysis_plan_'+taisaku_count+'"></textarea>';
        newTD4.innerHTML =
            '  <input type="button" value="根本原因" onClick="reference('+taisaku_count+');">' +
            '  <input type="button" value="職員名簿" onClick="popupEmpSelector('+taisaku_count+');">' +
            '  <input type="button" value="追加" onClick="adjustTaisakuArea(\'add\', '+taisaku_count+');">' +
            '  <input type="button" value="削除" onClick="adjustTaisakuArea(\'del\', '+taisaku_count+');">';

        newTD3.id = "td_analysis_rca_emp_list_"+taisaku_count;
        newTD3.style.verticalAlign = "top";
        newTD4.style.textAlign = "center";
        newTD4.style.backgroundColor = "#DFFFDC";
        newTR.appendChild(newTD1);
        newTR.appendChild(newTD2);
        newTR.appendChild(newTD3);
        newTR.appendChild(newTD4);
        tbl.appendChild(newTR);
    }

    <? // それぞれの項目値をずらす ?>
    for (var i = 1; i <= taisaku_count; i++){
        if (add_or_del=="add"){
            var copyFromIdx = taisaku_count-i;
            var copyToIdx   = taisaku_count-i + parseInt(1*1);
        }
        if (add_or_del=="del"){
            var copyFromIdx = i;
            var copyToIdx   = i - 1;
            if (copyToIdx < index) continue;
        }
        document.getElementById("analysis_yusenjun_"+copyToIdx).value = document.getElementById("analysis_yusenjun_"+copyFromIdx).value;
        document.getElementById("analysis_kijitu_y_"+copyToIdx).selectedIndex = document.getElementById("analysis_kijitu_y_"+copyFromIdx).selectedIndex;
        document.getElementById("analysis_kijitu_m_"+copyToIdx).selectedIndex = document.getElementById("analysis_kijitu_m_"+copyFromIdx).selectedIndex;
        document.getElementById("analysis_kijitu_d_"+copyToIdx).selectedIndex = document.getElementById("analysis_kijitu_d_"+copyFromIdx).selectedIndex;
        if (add_or_del=="add" && copyFromIdx==index) {
            document.getElementById("analysis_cause_"+copyToIdx).value = "";
            document.getElementById("analysis_plan_"+copyToIdx).value = "";
            document.getElementById("analysis_hiyou_"+copyToIdx).value = "";
            document.getElementById("td_analysis_rca_emp_list_"+copyToIdx).innerHTML = "";
            document.getElementById("analysis_sincyoku_"+copyToIdx).selectedIndex = 0;
            break;
        }
        document.getElementById("analysis_sincyoku_"+copyToIdx).selectedIndex = document.getElementById("analysis_sincyoku_"+copyFromIdx).selectedIndex;
        document.getElementById("analysis_cause_"+copyToIdx).value = document.getElementById("analysis_cause_"+copyFromIdx).value;
        document.getElementById("analysis_plan_"+copyToIdx).value = document.getElementById("analysis_plan_"+copyFromIdx).value;
        document.getElementById("analysis_hiyou_"+copyToIdx).value = document.getElementById("analysis_hiyou_"+copyFromIdx).value;
    }

    <? // 行削除の場合、末尾TRを削除 ?>
    if (add_or_del == "del") {
        var tr = document.getElementById("tr_taisaku_"+taisaku_count);
        tbl.removeChild(tr);
        delete tr;
        taisaku_count--;
    }
    document.getElementById("taisaku_count").value = taisaku_count;
}


var current_emp_target_index = 0;
function popupEmpSelector(idx){
    current_emp_target_index = idx;
    window.open("hiyari_emp_list.php?&session=<?=$session?>", 'hiyari_analysis_rca_copy_from_chart', "scrollbars=yes,resizable=yes,left=0,top=0,width=720,height=600");
}

function add_emp_list(new_emp_id, new_emp_name){
    var pid = current_emp_target_index;
    var td = document.getElementById("td_analysis_rca_emp_list_"+pid);
    if (!td) return;

    var emp_count = 0;
    for (;;){
        emp_count++;
        var emp_obj = document.getElementById("analysis_rca_emp_id_"+pid+"_"+emp_count);
        if (!emp_obj) break;
        if (emp_obj.value == new_emp_id) {
            document.getElementById("analysis_rca_emp_name_"+pid+"_"+emp_count).value = new_emp_name;
            return;
        }
    }

    td.innerHTML += "" +
        '<div id="div_analysis_rca_emp_'+pid+'_'+emp_count+'">'+
        '<?=$font?>'+
        '<a href="" onclick="deleteRcaEmpEntry('+pid+','+emp_count+',this); return false;">'+new_emp_name+'</a>'+
        '<input type="hidden" name="analysis_rca_emp_id_'+pid+'_'+emp_count+'"'+
        ' id="analysis_rca_emp_id_'+pid+'_'+emp_count+'" value="'+new_emp_id+'" />' +
        '<input type="hidden" name="analysis_rca_emp_name_'+pid+'_'+emp_count+'"'+
        ' id="analysis_rca_emp_name_'+pid+'_'+emp_count+'" value="'+new_emp_name+'" />'+
        '</font></div>';
}


function deleteRcaEmpEntry(pid, emp_index, atag){
    if (!confirm("「"+atag.innerHTML+"」さんを削除します。よろしいですか？")) return;
    var emp_count = emp_index;
    for (;;){
        emp_count++;
        if (!document.getElementById("rca_emp_id_"+pid+"_"+emp_count)) break;
        document.getElementById("analysis_rca_emp_id_"+pid+"_"+(emp_count-1)).value = document.getElementById("analysis_rca_emp_id_"+pid+"_"+emp_count).value;
        document.getElementById("analysis_rca_emp_name_"+pid+"_"+(emp_count-1)).value = document.getElementById("analysis_rca_emp_name_"+pid+"_"+emp_count).value;
    }
    var td = document.getElementById("td_analysis_rca_emp_list_"+pid);
    var div = document.getElementById("div_analysis_rca_emp_"+pid+"_"+(emp_count-1));
    td.removeChild(div);
    delete div;
}


</script>

</form>

</body>
</html>


<?
function array_insert ( &$array, $insert, $pos ) {
    //引数$arrayが配列でない場合はFALSEを返す
    if (!is_array($array)) return false;
    //挿入する位置〜末尾まで
    $last = array_splice($array, $pos);
    //先頭〜挿入前位置までの配列に、挿入する値を追加
    array_push($array, $insert);
    //配列を結合
    $array = array_merge($array, $last);
    return true;
}
?>


<? function wrapYellowNarrowBox($contents_html){ ?>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
            <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
            <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
        </tr>

        <tr>
            <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
            <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td valign="top" style="background-color:#f5ffe5"><?=$contents_html?></td>
                    </tr>
                </table>
            </td>
            <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
        </tr>

        <tr>
            <td><img src="img/r_3.gif" width="10" height="10"></td>
            <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
            <td><img src="img/r_4.gif" width="10" height="10"></td>
        </tr>
    </table>
<? } ?>

