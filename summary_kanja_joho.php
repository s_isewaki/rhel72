<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix メドレポート | 患者情報</title>
<? require("about_authority.php"); ?>
<? require("about_session.php"); ?>
<? require("about_postgres.php"); ?>
<? require("show_patient_next.ini"); ?>
<? require("./show_select_values.ini"); ?>
<? require("./get_values.ini"); ?>
<? require("./conf/sql.inf"); ?>
<? require_once("sot_util.php"); ?>
<?
//ページ名
$fname = $PHP_SELF;
//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$summary = check_authority($session, 57, $fname);
if ($summary == "0") {
	showLoginPage();
	exit;
}

//DBコネクション
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 検索フォームからの引継ぎ値をurlencode
$url_key1 = urlencode($key1);
$url_key2 = urlencode($key2);

?>
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
</head>
<body style="background-color:#f4f4f4">

<?
// 初期表示時は患者情報を取得
$cond = "where ptif_id = '$pt_id' and ptif_del_flg = 'f' order by ptif_id";
$sel = select_from_table($con,$SQL85,$cond,$fname);
if($sel == 0){
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");;
	exit;
}

$lt_nm = pg_result($sel,0,"ptif_lt_kaj_nm");
$ft_nm = pg_result($sel,0,"ptif_ft_kaj_nm");
$lt_kana_nm = pg_result($sel,0,"ptif_lt_kana_nm");
$ft_kana_nm = pg_result($sel,0,"ptif_ft_kana_nm");
$birth = pg_result($sel,0,"ptif_birth");
$birth_yr = substr($birth,0,4);
$birth_mon = substr($birth,4,2);
$birth_day = substr($birth,6,2);
$age = get_age($birth);
$sex = pg_result($sel,0,"ptif_sex");

// 入院してれば表示
$nyuin_prev_next_curr_info = $c_sot_util->get_nyuin_info_with_prev_next($con, $fname, $pt_id, date("Ymd"));

// 現在入院中かどうかチェック
$sql = "select count(*) from inptmst";
$cond = "where ptif_id = '$pt_id'";
$sel = select_from_table($con,$sql,$cond,$fname);
if($sel == 0){
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");;
	exit;
}
$in_flg = (pg_fetch_result($sel, 0, 0) > 0);
?>
<table class="list_table" cellspacing="1">
	<tr>
		<th rowspan="2" align='center' style="text-align:center; white-space:nowrap; width:3%; padding:2px"><nobr>ふりがな</nobr><br>氏名</th>
		<td rowspan="2" style="width:40%; white-space:nowrap; padding:0;">
			<div style="border-bottom:1px solid #9bc8ec; padding-left:2px"><?=$lt_kana_nm." ".$ft_kana_nm?></div>
			<div style="white-space:nowrap; margin-top:2px; padding-left:2px; font-size:18px; font-weight:bold"><? if ($in_flg) { ?><a href="javascript:void(0);" onclick="window.open('inpatient_sub_menu.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&subsys=med', 'newwin', 'width=900,height=640,scrollbars=yes')"><? } ?><?=$lt_nm." ".$ft_nm ?><? if ($in_flg) { ?></a><? } ?></div>
		</td>
		<th align='center' style="white-space:nowrap; width:3%; padding:2px">性別</th>
		<td align='center' style="white-space:nowrap; width:3%; padding:2px">
			<?
			if($sex == 1){
				echo("男性");
			}elseif($sex == 2){
				echo("女性");
			}else{
				echo("不明");
			}
			?>
			</td>
		<th style="white-space:nowrap; text-align:center; padding:2px;">生年月日</th>
<? if ($nyuin_prev_next_curr_info["curr"]){ ?>
		<td style="white-space:nowrap; padding:2px; font-size:12px; width:20%; padding:2px">
			<? $prev_ptif_id = @$nyuin_prev_next_curr_info["prev"]["ptif_id"]; ?>
			<? $next_ptif_id = @$nyuin_prev_next_curr_info["next"]["ptif_id"]; ?>
			<? $link_url = "window.parent.location.href='summary_rireki.php?session=".$session;?>
			<table style="width:100%"><tr>
				<td style="text-align:center; width:50%; white-space:nowrap">
					<? if ($prev_ptif_id){ ?>
					<a href="" class="always_blue" onclick="<?=$link_url?>&pt_id=<?=$prev_ptif_id?>'; return false;">前患者</a>
					<? } else { ?>
					<span style="color:#aaa; white-space:nowrap">前患者</span>
					<? } ?>
				</td>
				<td style="text-align:center; width:50%; white-space:nowrap">
					<? if ($next_ptif_id){ ?>
					<a href="" class="always_blue" onclick="<?=$link_url?>&pt_id=<?=$next_ptif_id?>'; return false;">次患者</a>
					<? } else { ?>
					<span style="color:#aaa; white-space:nowrap">次患者</span>
					<? } ?>
				</td>
			</tr></table>
		</td>
<? } ?>

	</tr>
	<tr>
		<th width="40" align='center' style="white-space:nowrap; padding:2px">年齢</th>
		<td width="40" align='center' style="white-space:nowrap; padding:2px"><?echo($age);?></td>
		<td align='center' style="white-space:nowrap; width:3%; padding:2px"><?echo($birth_yr);?>/<?echo($birth_mon);?>/<?echo($birth_day);?></td>
<? if ($nyuin_prev_next_curr_info["curr"]){ ?>
	<td style="padding:2px; text-align:center; white-space:nowrap; padding:2px"><?=$nyuin_prev_next_curr_info["curr"]["ptrm_name"]?>（<?=$nyuin_prev_next_curr_info["curr"]["inpt_bed_no"]?>）</td>
<? } ?>
	</tr>
</table>
</body>
<? pg_close($con); ?>
</html>

<?
function get_age($birth) {

	$yr = date("Y");
	$md = date("md");
	$birth_yr = substr($birth, 0, 4);
	$birth_md = substr($birth, 4, 8);
	$age = $yr - $birth_yr;
	if ($md < $birth_md) {
		$age = $age - 1;
	}

	return $age;

}
?>
