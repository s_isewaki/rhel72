<?
ob_start();

ini_set("max_execution_time", 0);

require("about_session.php");
require("about_authority.php");
require("show_timecard_common.ini");
require("show_attendance_pattern.ini");
require("holiday.php");
require("timecard_common_class.php");
require_once("timecard_import_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 勤務管理権限チェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 締め日・タイムカード修正可能フラグを取得
$sql = "select closing, closing_parttime, modify_flg, closing_month_flg from timecard";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if (pg_num_rows($sel) > 0) {
	$closing_parttime = pg_fetch_result($sel, 0, "closing_parttime");
	$closing = pg_fetch_result($sel, 0, "closing");
	$modify_flg = pg_fetch_result($sel, 0, "modify_flg");
	//非常勤が未設定時は常勤を使用する 20091214
	if ($closing_parttime == "") {
		$closing_parttime = $closing;
	}
	$closing_month_flg = pg_fetch_result($sel, 0, "closing_month_flg");
} else {
	$closing_parttime = "";
	$closing = "";
	$modify_flg = "t";
	$closing_month_flg = "1";
}

// 締め日が未登録の場合はエラーとする
if ($closing == "" ) {
	echo("<script type=\"text/javascript\">alert('タイムカード締め日が登録されていません。');</script>");
	exit;
}
//非常勤が未設定時は常勤を使用する
if ($closing_parttime == "") {
	$closing_parttime = $closing;
}

$org_yyyymm = $yyyymm;

	if ($yyyymm != "") {
		$year = substr($yyyymm, 0, 4);
		$month = intval(substr($yyyymm, 4, 2));
		$month_set_flg = true; //月が指定された場合
	} else {
		$year = date("Y");
		$month = date("n");
		$yyyymm = $year . date("m");
		$month_set_flg = false; //月が未指定の場合
	}

	$last_month_y = $year;
	$last_month_m = $month - 1;
	if ($last_month_m == 0) {
		$last_month_y--;
		$last_month_m = 12;
	}
	$last_yyyymm = $last_month_y . sprintf("%02d", $last_month_m);

	$next_month_y = $year;
	$next_month_m = $month + 1;
	if ($next_month_m == 13) {
		$next_month_y++;
		$next_month_m = 1;
	}
	$next_yyyymm = $next_month_y . sprintf("%02d", $next_month_m);

	// 開始日・締め日を算出
	$calced = false;
	switch ($closing) {
	case "1":  // 1日
		$closing_day = 1;
		break;
	case "2":  // 5日
		$closing_day = 5;
		break;
	case "3":  // 10日
		$closing_day = 10;
		break;
	case "4":  // 15日
		$closing_day = 15;
		break;
	case "5":  // 20日
		$closing_day = 20;
		break;
	case "6":  // 末日
		$start_year = $year;
		$start_month = $month;
		$start_day = 1;
		$end_year = $start_year;
		$end_month = $start_month;
		$end_day = days_in_month($end_year, $end_month);
		$calced = true;
		break;
	}
	if (!$calced) {
		$day = date("j");
		$start_day = $closing_day + 1;
		$end_day = $closing_day;

		//月が指定された場合
		if ($month_set_flg) {
			//当月〜翌月
			if ($closing_month_flg != "2") {
				$start_year = $year;
				$start_month = $month;
				
				$end_year = $year;
				$end_month = $month + 1;
				if ($end_month == 13) {
					$end_year++;
					$end_month = 1;
				}
			}
			//前月〜当月
			else {
				$start_year = $year;
				$start_month = $month - 1;
				if ($start_month == 0) {
					$start_year--;
					$start_month = 12;
				}
				
				$end_year = $year;
				$end_month = $month;
			}
			
		} else {
			if ($day <= $closing_day) {
				$start_year = $year;
				$start_month = $month - 1;
				if ($start_month == 0) {
					$start_year--;
					$start_month = 12;
				}
				
				$end_year = $year;
				$end_month = $month;
			} else {
				$start_year = $year;
				$start_month = $month;
				
				$end_year = $year;
				$end_month = $month + 1;
				if ($end_month == 13) {
					$end_year++;
					$end_month = 1;
				}
			}
		}
	
	}
	$start_date = sprintf("%04d%02d%02d", $start_year, $start_month, $start_day);
	$end_date = sprintf("%04d%02d%02d", $end_year, $end_month, $end_day);

	// 開始日・締め日を算出
	$calced = false;
	switch ($closing_parttime) {
	case "1":  // 1日
		$closing_day = 1;
		break;
	case "2":  // 5日
		$closing_day = 5;
		break;
	case "3":  // 10日
		$closing_day = 10;
		break;
	case "4":  // 15日
		$closing_day = 15;
		break;
	case "5":  // 20日
		$closing_day = 20;
		break;
	case "6":  // 末日
		$start_year = $year;
		$start_month = $month;
		$start_day = 1;
		$end_year = $start_year;
		$end_month = $start_month;
		$end_day = days_in_month($end_year, $end_month);
		$calced = true;
		break;
	}
	if (!$calced) {
		$day = date("j");
		$start_day = $closing_day + 1;
		$end_day = $closing_day;

		//月が指定された場合
		if ($month_set_flg) {
			//当月〜翌月
			if ($closing_month_flg != "2") {
				$start_year = $year;
				$start_month = $month;
				
				$end_year = $year;
				$end_month = $month + 1;
				if ($end_month == 13) {
					$end_year++;
					$end_month = 1;
				}
			}
			//前月〜当月
			else {
				$start_year = $year;
				$start_month = $month - 1;
				if ($start_month == 0) {
					$start_year--;
					$start_month = 12;
				}
				
				$end_year = $year;
				$end_month = $month;
			}
			
		} else {
			if ($day <= $closing_day) {
				$start_year = $year;
				$start_month = $month - 1;
				if ($start_month == 0) {
					$start_year--;
					$start_month = 12;
				}
				
				$end_year = $year;
				$end_month = $month;
			} else {
				$start_year = $year;
				$start_month = $month;
				
				$end_year = $year;
				$end_month = $month + 1;
				if ($end_month == 13) {
					$end_year++;
					$end_month = 1;
				}
			}
		}
	}
	$start_date_parttime = sprintf("%04d%02d%02d", $start_year, $start_month, $start_day);
	$end_date_parttime = sprintf("%04d%02d%02d", $end_year, $end_month, $end_day);


// 快決シフト用実績データをCSV形式で取得
// ************ oose update start *****************
$csv = get_attendance_book_csv($con, $emp_id_list,
			$start_date, $end_date,
			$start_date_parttime, $end_date_parttime,
			$fname, $modify_flg);
// ************ oose update end *****************

// データベース接続を閉じる
pg_close($con);

// CSVを出力
$file_name = "CtoS.csv";
ob_clean();
header("Content-Disposition: attachment; filename=$file_name");
header("Content-Type: application/octet-stream; name=$file_name");
header("Content-Length: " . strlen($csv));
echo($csv);
ob_end_flush();

//------------------------------------------------------------------------------
// 関数
//------------------------------------------------------------------------------

// 月集計情報をCSV形式で取得
function get_attendance_book_csv($con, $emp_id_list,
			$start_date_fulltime, $end_date_fulltime,
			$start_date_parttime, $end_date_parttime,
			$fname, $modify_flg) {

	//変換情報
	$cnvdata = get_cnvdata_kai($con, $fname, "");
	$arr_cnvtbl = array(); //key[{$tmcd_group_id}_{$atdptn_ptn_id}_{$reason}]["absence_reason" or "duty_ptn"]
	
	for ($i=0; $i<count($cnvdata); $i++) {
		$wk_key = $cnvdata[$i]["tmcd_group_id"]."_".$cnvdata[$i]["atdptn_ptn_id"]."_".$cnvdata[$i]["reason"];
		$arr_cnvtbl[$wk_key]["absence_reason"] = $cnvdata[$i]["absence_reason"];
		$arr_cnvtbl[$wk_key]["duty_ptn"] = $cnvdata[$i]["duty_ptn"];
	}
	// 手当情報取得
	$arr_allowance = get_timecard_allowance($con, $fname);

    // タイムカード設定情報
	$sql = "select * from timecard";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
    $night_workday_count = pg_fetch_result($sel, 0, "night_workday_count");
	$return_icon_flg = pg_fetch_result($sel, 0, "return_icon_flg");
	

	$buf = "";

	// 選択された全職員をループ
	$emp_ids = explode(",", $emp_id_list);
	foreach ($emp_ids as $tmp_emp_id) {

		// 勤務条件テーブルより勤務形態（常勤、非常勤）を取得
		$sql = "select duty_form from empcond";
		$cond = "where emp_id = '$tmp_emp_id'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$duty_form = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "duty_form") : "";
		if ($duty_form == 2) {
			//非常勤
			$yyyymm = substr($start_date_parttime, 0, 6);
			$start_date = $start_date_parttime;
			$end_date = $end_date_parttime;
		} else {
			//常勤
			$yyyymm = substr($start_date_fulltime, 0, 6);
			$start_date = $start_date_fulltime;
			$end_date = $end_date_fulltime;
		}

		// 職員ID・職員氏名を取得
		$sql = "select emp_personal_id, emp_lt_nm, emp_ft_nm, b.link_key from empmst ";
		$sql .= "left join atrbmst b on b.atrb_id = empmst.emp_attribute ";
		$cond = "where emp_id = '$tmp_emp_id'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$tmp_emp_personal_id = pg_fetch_result($sel, 0, "emp_personal_id");
		if ($tmp_emp_personal_id == "") {
			$tmp_emp_personal_id = "&nbsp;";
		}
		$tmp_emp_name = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");
		//外部連携キー、予定データの所属コードが未設定の場合は代わりに出力
		$tmp_link_key = pg_fetch_result($sel, 0, "link_key");
		
		// 対象年月の締め状況を取得
		$sql = "select count(*) from atdbkclose";
		$cond = "where emp_id = '$tmp_emp_id' and yyyymm = '$yyyymm'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$atdbk_closed = (pg_fetch_result($sel, 0, 0) > 0);

		// 締め済みの場合、締めデータから表示対象期間を取得
		if ($atdbk_closed) {
			$sql = "select min(date), max(date) from wktotald";
			$cond = "where emp_id = '$tmp_emp_id' and yyyymm = '$yyyymm'";
			$sel = select_from_table($con, $sql, $cond, $fname);
			if ($sel == 0) {
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}
			$tmp_start_date = pg_fetch_result($sel, 0, 0);
			$tmp_end_date = pg_fetch_result($sel, 0, 1);
		} else {
			$tmp_start_date = $start_date;
			$tmp_end_date = $end_date;
		}

		// 勤務実績をまとめて取得
		$sql =	"SELECT ".
			"a.date, a.tmcd_group_id, a.pattern, a.reason, b.atrb_link_key ".
			"FROM ".
			"atdbkrslt a ".
			"LEFT JOIN atdbk b ON b.emp_id = a.emp_id and b.date = a.date ";
			
//todo:予定に追加する所属コードを取得しCSVの最後の項目へ出力
			/*
		$sql =	"SELECT ".
					"atdbkrslt.*, ".
					"wktmgrp.group_name, ".
					"atdptn.workday_count, ".
					"atdptn.atdptn_nm, ".
					"atdbk_reason_mst.default_name, ".
					"atdbk_reason_mst.display_name ".
				"FROM ".
					"atdbkrslt  ".
						"LEFT JOIN wktmgrp ON atdbkrslt.tmcd_group_id = wktmgrp.group_id ".
						"LEFT JOIN atdptn ON atdbkrslt.tmcd_group_id = atdptn.group_id AND atdbkrslt.pattern = CAST(atdptn.atdptn_id AS varchar) ".
						"LEFT JOIN atdbk_reason_mst ON atdbkrslt.reason = atdbk_reason_mst.reason_id ";
*/
		$cond = "where a.emp_id = '$tmp_emp_id' and a.date >= '$tmp_start_date' and a.date <= '$tmp_end_date'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$arr_atdbkrslt = array();
		while ($row = pg_fetch_array($sel)) {
			$date = $row["date"];
			$arr_atdbkrslt[$date]["tmcd_group_id"] = $row["tmcd_group_id"];
			$arr_atdbkrslt[$date]["pattern"] = $row["pattern"];
			$arr_atdbkrslt[$date]["reason"] = $row["reason"];
			$arr_atdbkrslt[$date]["atrb_link_key"] = $row["atrb_link_key"];
		}

		// 全日付をループ
		$tmp_date = $tmp_start_date;

		//グループ毎のテーブルから勤務時間を取得する
		//$timecard_common_class = new timecard_common_class($con, $fname, $emp_id, $tmp_date, $end_date);

		while ($tmp_date <= $tmp_end_date) {

			// 処理日付の勤務実績を取得
			$tmcd_group_id = $arr_atdbkrslt[$tmp_date]["tmcd_group_id"];
			$pattern = $arr_atdbkrslt[$tmp_date]["pattern"];
			$reason = $arr_atdbkrslt[$tmp_date]["reason"];
			$atrb_link_key = $arr_atdbkrslt[$tmp_date]["atrb_link_key"];
			
			//不在理由,勤務区分取得
			$wk_key = $tmcd_group_id."_".$pattern."_".$reason;
			$absence_reason = $arr_cnvtbl[$wk_key]["absence_reason"];
			$duty_ptn = $arr_cnvtbl[$wk_key]["duty_ptn"];
			
			$buf .= "$tmp_date,"; //日付
			$buf .= sprintf("%08s", $tmp_emp_personal_id).","; //職員コード0埋め8桁固定
			$buf .= "$absence_reason,"; //不在理由
			$buf .= "$duty_ptn,"; //勤務区分
			$buf .= ",,,,,"; //ブランクx5,
			$buf .= ",,"; //開始時刻,終了時刻,
			//出勤予定に快決シフトくんのインポートデータがある場合、所属コードへ出力
			if ($atrb_link_key != "") {
				$buf .= $atrb_link_key;
			} else {
				$buf .= $tmp_link_key; //所属コード
			}
			
			$buf .= "\r\n";

			$tmp_date = next_date($tmp_date);
		}
	}

	return mb_convert_encoding($buf, "SJIS", "EUC-JP");
}
?>
