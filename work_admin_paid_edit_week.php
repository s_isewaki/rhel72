<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務管理｜週休付与修正</title>
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_timecard_common.ini");
require_once("timecard_bean.php");
require_once("atdbk_common_class.php");
require_once("show_select_values.ini");
require_once("work_admin_paid_week_common.php");
require_once("timecard_common_class.php");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session,$fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 勤務管理権限チェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

//--------------------------------------------------------------------------------------------------------------------------
//更新処理
//--------------------------------------------------------------------------------------------------------------------------

if ($update_flg == "1" && $del_flg != "1") {
	
    $add_date = $select_year.'-'.$select_mon.'-'.$select_day;
    $add_date2= $select_year2.'-'.$select_mon2.'-'.$select_day2;
    
	// トランザクションを開始
	pg_query($con, "begin transaction");
	//データ追加
	if ($data_cnt == "0" || $data_cnt == "") {

        $sql = "select max(id)+1 as cnt from emppaid_week";
        $cond= "";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $wk_id = pg_fetch_result($sel, 0, "cnt");
        //一件もデータ無い場合
        if ($wk_id == ""){
            $wk_id = 1;
        }
        
        $sql = "insert into emppaid_week (id, emp_id, add_date, days, lastupdate) values (";
        $content = array($wk_id, $emp_id, $add_date, $days, date("Y-m-d H:i:s"));
     
        $ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	} else {

        //データ更新
		$sql = "update emppaid_week set";
        $set = array("add_date", "days", "lastupdate");
        $setvalue = array($add_date, $days, date("Y-m-d H:i:s"));
        $cond = "where emp_id = '$emp_id' and id = '$id' ";

        $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
		}

    }

    //追加分
	if ($add_flg == "1") {

        $sql = "select max(id)+1 as cnt from emppaid_week";
        $cond= "";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $wk_id = pg_fetch_result($sel, 0, "cnt");
    
        $sql = "insert into emppaid_week (id, emp_id, add_date, days, lastupdate) values (";
        $content = array($wk_id, $emp_id, $add_date2, $days2, date("Y-m-d H:i:s"));
        $ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}
	
	// トランザクションをコミット
	pg_query("commit");
	
	$update_flg = "";

	echo("<script type=\"text/javascript\">opener.document.mainform.submit();</script>");
	
}


//--------------------------------------------------------------------------------------------------------------------------
//削除処理
//--------------------------------------------------------------------------------------------------------------------------
if ($del_flg == "1") {
	// トランザクションを開始
	pg_query($con, "begin transaction");

	$sql = "delete from emppaid_week";
	$cond = "where emp_id = '$emp_id' and id = '$id' ";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	// トランザクションをコミット
	pg_query("commit");

	$update_flg = "";
	$del_flg = "";
	
	echo("<script type=\"text/javascript\">opener.document.mainform.submit();</script>");
}


// 職員ID・氏名を取得
$sql = "select emp_id, emp_lt_nm, emp_ft_nm, emp_personal_id, emp_join from empmst";
$cond = "where emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");
$emp_name = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");
$emp_personal_id = pg_fetch_result($sel, 0, "emp_personal_id");
$emp_join = pg_fetch_result($sel, 0, "emp_join");
$emp_join_str = substr($emp_join, 0, 4)."年".intval(substr($emp_join, 4, 2))."月".intval(substr($emp_join, 6, 2))."日";

//出勤表関連共通クラス
$atdbk_common_class = new atdbk_common_class($con, $fname);

$timecard_common_class = new timecard_common_class($con, $fname, $emp_id, date("Ymd"), "");

//タイムカード情報の取得
$timecard_bean = new timecard_bean();
$timecard_bean->select($con, $fname);

//時間有休
$obj_hol_hour = new timecard_paid_hol_hour_class($con, $fname);
$obj_hol_hour->select();

//所属表示
$str_orgname = get_orgname($con, $fname, $emp_id, $timecard_bean);
$str_orgname = substr($str_orgname, 1);
$str_orgname = str_replace(" ", "＞", $str_orgname);

//週休情報を取得
$sql = "select * from emppaid_week";
$cond = "where emp_id = '$emp_id' order by add_date";

$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$num = pg_num_rows($sel);


//同一年月日のデータを除く
$arr_paid_info =  array();
if ($num > 0) {
	$i = 0;
	$old_key = "";
	while ($row = pg_fetch_array($sel)) {
		$wk_key = $row["add_date"];
		if ($old_key != $wk_key) {
			$arr_paid_info[$i] = $row;
			$i++;
		}
		$old_key = $wk_key;
	}
	$num = count($arr_paid_info);
	$data_cnt = $num;
}


//データが一件も無い場合
if ($num==0){
	$arr_paid_info[0]["add_date"] = date("Y-m-d");  
    $num=1;
}


?>
<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
<script type="text/javascript">
<!--
//行追加
function addLine() {
	document.getElementById("add_line").style.display = "";
	document.getElementById("add_btn").disabled = true;
	document.mainform.add_flg.value = "1";
}
//行削除
function delLine() {
	document.getElementById("add_line").style.display = "none";
	document.getElementById("add_btn").disabled = false;
	document.mainform.add_flg.value = "";
}
//データ削除
function delData() {
	if (confirm('この行の週休付与情報を削除します。よろしいですか？')) {
		document.mainform.del_flg.value = "1";
		document.mainform.submit();
	}
}

//日付の整合性チェック
function isValidDate(y,m,d){
    var di = new Date(y,m-1,d);

    if(di.getFullYear() == y && di.getMonth() == m-1 && di.getDate() == d){
        return 0;
    }

    return -1;
}

//入力チェック
function checkInput() {
    var y=document.getElementById("select_year").value;
    var m=document.getElementById("select_mon").value;
    var d=document.getElementById("select_day").value;

    if (isValidDate(y,m,d) != 0) {
        alert('付与年月日の指定が誤っています。\n' +y+'年'+m+'月'+d+'日は存在しません。' );
		return false;        
    }
    
	days = document.getElementById("days").value;
	if ((days != "" && !days.match(/^\d$|^\d+\.?\d+$/g)) || (days == "") ) {
		alert('当年付与には半角数字を入力してください。');
		return false;
	}

	//行が追加されている場合
	if (document.getElementById("add_flg").value == "1") {

        y=document.getElementById("select_year2").value;
        m=document.getElementById("select_mon2").value;
        d=document.getElementById("select_day2").value;
        if (isValidDate(y,m,d) != 0) {
            alert('付与年月日の指定が誤っています。\n' +y+'年'+m+'月'+d+'日は存在しません。' );
            return false;        
        }

        if (document.getElementById("select_year").value == document.getElementById("select_year2").value &&
			document.getElementById("select_mon").value == document.getElementById("select_mon2").value &&
			document.getElementById("select_day").value == document.getElementById("select_day2").value) {
			alert('付与年月日には同じ日を指定できません。');
			return false;
		}
				
		days2 = document.getElementById("days2").value;
		if ((days2 != "" && !days2.match(/^\d$|^\d+\.?\d+$/g)) || (days2 == "")) {
			alert('当年付与には半角数字を入力してください。');
			return false;
		}
	}
	return true;
}

//-->
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.list {border-collapse:collapse;}
table.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
    <tr height="32" bgcolor="#5279a5">
      <td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>週休付与修正</b></font></td>
      <td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();">
      <img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
    </tr>
  </table>
<?
//--------------------------------------------------------------------------------------------------------------------------
// ヘッダ部
//--------------------------------------------------------------------------------------------------------------------------
?>
<table width="500" border="0" cellspacing="1" cellpadding="0" class="list">
  <tr>
    <td width="100" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員ID</font></td>
    <td width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($emp_personal_id); ?></font></td>
    <td width="100" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">氏名</font></td>
    <td width="200"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($emp_name); ?></font></td>
  </tr>
  <tr>
    <td bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属</font></td>
    <td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($str_orgname); ?></font></td>
  </tr>
  <tr>
    <td bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">採用年月日</font></td>
    <td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?if ($emp_join > 0) {echo($emp_join_str);} ?></font></td>
  </tr>
</table>
<br>
<form name="mainform" action="work_admin_paid_edit_week.php" method="post">
<table width="100%" border="0" cellspacing="1" cellpadding="1" class="list">
  <tr bgcolor="#f6f9ff">
    <td align="center" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">付与年月日</font></td>
    <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">付与日数</font></td>
    <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"></font></td>
  </tr>

<?
//--------------------------------------------------------------------------------------------------------------------------
// 行表示ループ
//--------------------------------------------------------------------------------------------------------------------------
for ($i=0; $i<$num; $i++) {
	$wk_id = $arr_paid_info[$i]["id"];                  //ID
	$wk_add_date = $arr_paid_info[$i]["add_date"];      //付与年月日
	$wk_days = $arr_paid_info[$i]["days"];              //付与数

	$wk_add_ymd = intval(substr($wk_add_date, 0, 4))."年".intval(substr($wk_add_date, 5, 2))."月".intval(substr($wk_add_date, 8, 2))."日";
    $select_year = intval(substr($wk_add_date, 0, 4));
    $select_mon = intval(substr($wk_add_date, 5, 2));
    $select_day = intval(substr($wk_add_date, 8, 2));
    
	echo("<tr>\n");
	//最新行の場合
	if ($i == $num-1) {

        //付与年月日
		if ($wk_year == "") {
			$wk_year = date("Y");
		}
		$wk_start = min($wk_year, date("Y")-1);
		$wk_end = max($wk_year+1, date("Y")+1);

        echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		echo("<select name=\"select_year\" id=\"select_year\">");
		show_select_years_span($wk_start, $wk_end, $select_year, $asc = false);
		echo("</select>年");
		echo("<select name=\"select_mon\" id=\"select_mon\">");
		show_select_months($select_mon, false);
		echo("</select>月");
		echo("<select name=\"select_day\" id=\"select_day\">");
		show_select_days($select_day, false);
		echo("</select>日");
		echo("</font></td>\n");
        echo("<td align=\"center\">\n");
        echo("<input type=\"text\" name=\"days\" id=\"days\" value=\"$wk_days\" size=\"5\" maxlength=\"5\" style=\"text-align:right;ime-mode:inactive;\">日\n");
        echo("<input type=\"hidden\" name=\"id\" value=\"$wk_id\">\n");
        echo("</td>\n");   
        echo("<td align=\"left\">");
        echo("<input type=\"button\" id=\"add_btn\" value=\"追加\" onclick=\"addLine();\">&nbsp;");
        //データが一件も無し状態では削除ボタンは無効
        if ($data_cnt == 0) {
            echo("<input type=\"button\" id=\"del2_btn\" value=\"削除\" onclick=\"delData();\" disabled>");
        }else{
            echo("<input type=\"button\" id=\"del2_btn\" value=\"削除\" onclick=\"delData();\">");
        }

        echo("</td>\n");
	} else {
		echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk_add_ymd</font></td>\n");
		echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".$wk_days."日</font></td>\n");
        echo("<td align=\"center\"></td>");
	}
	echo("</tr>\n");
}

//--------------------------------------------------------------------------------------------------------------------------
// 追加行
//--------------------------------------------------------------------------------------------------------------------------
if ($update_flg == "") {
   
    $select_year2 = intval(substr($wk_add_date, 0, 4));
    $select_mon2 = intval(substr($wk_add_date, 5, 2));
    $select_day2 = intval(substr($wk_add_date, 8, 2));
    
}
echo("<tr id=\"add_line\" style=\"display:none;\">\n");
//付与年月日
if ($wk_year == "") {
	$wk_year = date("Y");
}

$wk_start = min($wk_year, date("Y")-1);
$wk_end = max($wk_year+1, date("Y")+1);


echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
echo("<select name=\"select_year2\" id=\"select_year2\">");
show_select_years_span($wk_start, $wk_end, $select_year2, $asc = false);
echo("</select>年");
echo("<select name=\"select_mon2\" id=\"select_mon2\">");
show_select_months($select_mon2, false);
echo("</select>月");
echo("<select name=\"select_day2\" id=\"select_day2\">");
show_select_days($select_day2, false);
echo("</select>日");
echo("</font></td>\n");
?>

<td align="center"><input type="text" name="days2" id="days2" value="<? echo($days); ?>" size="5" maxlength="5" style="text-align:right;ime-mode:inactive;">日</td>
<td align="left"><input type="button" value="削除" onclick="delLine();"></td>
</tr>
</table>

<table width="100%" border="0" cellspacing="1" cellpadding="1">
<tr>
<td align="right">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="emp_id" value="<? echo($emp_id); ?>">
<input type="hidden" name="data_cnt" value="<? echo($data_cnt); ?>">
<input type="hidden" name="update_flg" value="1">
<input type="hidden" name="add_flg" id="add_flg" value="">
<input type="hidden" name="del_flg" value="">
<input type="submit" value="更新" onclick="return checkInput();">
</td>
</tr>
</table>
</form>
<?
pg_close($con);
?>