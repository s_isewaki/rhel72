<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
//ファイルの読み込み
require("about_postgres.php");
require("about_session.php");
require("about_authority.php");
require("./get_values.ini");

//ページ名
$fname = $PHP_SELF;

//==============================
//セッションチェック
//==============================
$session = qualify_session($session,$fname);
if($session == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
//権限チェック
//==============================
$dept = check_authority($session,57,$fname);
if($dept == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
//送信データをＤＢデータに変換
//==============================
$pt_id      =      $_REQUEST["pt_id"   ] ;
$in_dt      =      $_REQUEST["in_dt"   ] ;
$in_tm      =      $_REQUEST["in_tm"   ] ;
$tbl_nm     =      $_REQUEST["tbl_nm"  ] ;
$disease    = trim($_REQUEST["disease" ]) ;
$patho_from =      $_REQUEST["patho_from_year"] . $_REQUEST["patho_from_month"] . $_REQUEST["patho_from_day"] ;
$patho_to   =      $_REQUEST["patho_to_year"  ] . $_REQUEST["patho_to_month"  ] . $_REQUEST["patho_to_day"  ] ;
$sect_id    =      $_REQUEST["sel_sect"] ;
$dr_id      =      $_REQUEST["sel_dr"  ] ;

//==============================
//入力チェック
//==============================
$patho_from_len = strlen($patho_from) ;
$patho_to_len   = strlen($patho_to  ) ;
$patho_len      = $patho_from_len + $patho_to_len ;
$disease_len    = strlen($disease   ) ;

$errMsgs = array("発症年月日の入力が不正です", "診断名(主病名)を入力してください") ;
$msgIdx = -1 ;
// 発症年月日のfromとtoがともに指定されていないとき
if     ($patho_len ==  6)
{
}
// 発症年月日のfromとtoがともに指定されているとき
elseif ($patho_len == 16)
{
  if     ($patho_from  > $patho_to) $msgIdx = 0 ;
  elseif ($disease_len < 1        ) $msgIdx = 1 ;
}
// 発症年月日のfromとtoのどちらかが指定されているかもしれないとき
elseif ($patho_len == 11)
{
  // 発症年月日のfromとtoのどちらかが指定されているとき(このときの片方は必ず無指定)
  if (($patho_from_len == 8) || ($patho_to_len == 8))
  {
    if ($disease_len < 1) $msgIdx = 1 ;
  }
  else
  {
    $msgIdx = 0 ;
  }
}
else
{
  $msgIdx = 0 ;
}
if ($msgIdx > -1)
{
  echo("<script language=\"javascript\">alert(\"" . $errMsgs[$msgIdx] . "\");</script>\n");
  echo("<script language=\"javascript\">history.back();</script>\n");
  exit;
}

//==============================
//ＤＢのコネクション作成(トランザクション開始)
//==============================
$con = connect2db($fname);
pg_query($con, "begin transaction");

//==============================
//入院情報テーブルを更新
//==============================
$patho_from = str_replace("-","",$patho_from) ;
$patho_to   = str_replace("-","",$patho_to  ) ;
$sect_id    = ($sect_id == "") ? "0" : $sect_id ;
$sql = "update " . $tbl_nm . " set" ;
$set = array("inpt_disease", "inpt_patho_from", "inpt_patho_to", "inpt_sect_id", "dr_id") ;
$setvalue = array($disease, $patho_from, $patho_to, $sect_id, $dr_id);
$cond = "where ptif_id = '" . $pt_id . "' and inpt_in_dt = '" . $in_dt . "' and inpt_in_tm = '" . $in_tm . "'" ;
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if($upd == 0)
{
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

//==============================
// ＤＢコネクション切断(トランザクション終了)
//==============================
pg_query($con, "commit");
pg_close($con);
?>


<? //============================== ?>
<? // 処理完了JavaScript出力        ?>
<? //============================== ?>
<script type="text/javascript">
  window.opener.location.reload();
  window.close();
</script>