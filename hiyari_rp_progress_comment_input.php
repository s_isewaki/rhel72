<?
require_once('about_comedix.php');
require_once("get_values.php");
require_once("hiyari_common.ini");
require_once("hiyari_reporting_common.ini");
require_once("hiyari_mail.ini");

//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// 権限チェック
$summary = check_authority($session, 47, $fname);
if ($summary == "0") {
    showLoginPage();
    exit;
}


//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//==============================
//ユーザーID取得
//==============================
$emp_id = get_emp_id($con,$session,$fname);

//==============================
//画面タイトル
//==============================
$PAGE_TITLE = "確認コメント入力";

//==============================
//報告書進捗に対するユーザーの権限を取得
//==============================
$rep_obj = new hiyari_report_class($con, $fname);
$arr_report_auth = $rep_obj->get_my_inci_auth_list_for_report_progress($report_id, $emp_id);

//==============================
//ポストバック時の処理
//==============================
if(! empty($is_postback))
{
    $comment_list = array();
    $target_auth_arr = split(",", $target_auth_list);
    foreach($target_auth_arr as $auth){
        $key = "comment_".$auth;
        $comment_list[$auth] = $$key;
    }
    $rep_obj->set_report_progress_comment($report_id, $emp_id, $comment_list);
    
    //==============================
    //画面を閉じる
    //==============================
    ?>
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
    <html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
    <script language='javascript'>
    alert("確認コメントを登録しました。");
    if (opener && !opener.closed) {
        opener.location.reload(true);
    }
    window.close();
    </script>
    </head>
    <body>
    </body>
    </html>
    <?
    pg_close($con);
    exit;
}

//==============================
//コメント情報を作成
//==============================

$comment_list = "";

//新規コメント一覧を取得
foreach($arr_report_auth as $report_auth)
{
    $auth = $report_auth['auth'];
    $comment = "";
    $comment_list[$auth]['comment']=$comment;
}

//既に入力済みのコメントの一覧で上書き(※特殊ケース：昔、権限を持っていた場合はこれにヒット)
$sql = "select auth,progress_comment from inci_report_progress_comment where report_id = $report_id and emp_id = '$emp_id'";
$sel = select_from_table($con,$sql,"",$fname);
if($sel == 0){
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$old_comment_arr = pg_fetch_all($sel);
foreach($old_comment_arr as $old_comment)
{
    $auth = $old_comment['auth'];
    $comment = $old_comment['progress_comment'];
    $comment_list[$auth]['comment']=$comment;
}

$target_auth_list = "";
foreach($comment_list as $auth => $comment_info)
{
    if($target_auth_list != "")
    {
        $target_auth_list .= ",";
    }
    $target_auth_list .= $auth;
}

//==============================
//HTML出力
//==============================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$INCIDENT_TITLE?> | <?=$PAGE_TITLE?></title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script language="javascript">
function init_page()
{


}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

.list {border-collapse:collapse;}
.list td {border:#35B341 solid 1px;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#35B341 solid 0px;}
table.block_in td td {border-width:1;}

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="init_page();">
<form name="mainform" action="" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="is_postback" value="true">
<input type="hidden" name="report_id" value="<?=$report_id?>">
<input type="hidden" name="target_auth_list" value="<?=$target_auth_list?>">

<!-- BODY全体 START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<!-- ヘッダー START -->
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
<?
    show_hiyari_header_for_sub_window($PAGE_TITLE);
?>
</table>
<!-- ヘッダー END -->


</td></tr><tr><td>


<!-- 本体 START -->
<table border="0" cellspacing="0" cellpadding="0" width="700">
    <tr>
        <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
        <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
        <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
    </tr>
    <tr>
        <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
        <td bgcolor="#F5FFE5">

<?
$is_first_line = true;
foreach($comment_list as $auth => $comment_info)
{
    $comment = $comment_info['comment'];
    $auth_name = get_auth_name($auth,$fname,$con);

    if($is_first_line)
    {
        $is_first_line = false;
    }
    else
    {
?>
            <img src="img/spacer.gif" width="1" height="10" alt="">
<?
    }
?>
            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                <tr>
                    <td align="left" width="150" bgcolor="#DFFFDC">
                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><?=h($auth_name)?></font>
                    </td>
                </tr>
                <tr>
                    <td align="left" bgcolor="#FFFFFF">
                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
                        <textarea name="comment_<?=$auth?>" style="width:100%;height:100px;ime-mode:active"><?=h($comment)?></textarea>
                        </font>
                    </td>
                </tr>
            </table>
<?
}
?>

            <table width="100%" border="0" cellspacing="0" cellpadding="5">
                <tr>
                    <td align="right">
                        <input type="submit" value="設定">
                    </td>
                </tr>
            </table>

        </td>
        <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
    </tr>
    <tr>
        <td><img src="img/r_3.gif" width="10" height="10"></td>
        <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
        <td><img src="img/r_4.gif" width="10" height="10"></td>
    </tr>
</table>
<!-- 本体 END -->


</td>
</tr>
</table>
<!-- BODY全体 END -->

</body>
</html>
<?
//==============================
//DBコネクション終了
//==============================
pg_close($con);
?>

