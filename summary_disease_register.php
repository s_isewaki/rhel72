<?
require_once("about_comedix.php");
require_once("show_select_values.ini");
require_once("yui_calendar_util.ini");

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	js_login_exit();
}

// 権限のチェック
$checkauth = check_authority($session, 57, $fname);
if ($checkauth == "0") {
	js_login_exit();
}

$con = connect2db($fname);

$errors = array();
if ($mode == "register") {
	if ($disease_name === "") {
		$errors[] = "傷病名を入力してください。";
	}

	if ($input_mode !== "2") {
		$searched = false;
		for ($i = 1; $i <= 5; $i++) {
			if ($_POST["target_table_class_$i"] === "1") {
				$searched = true;
			}
		}
		if (!$searched) {
			$errors[] = "病名を検索してください。";
		}
	}

	$date1_selected = 0;
	$date1_selected += ($date_y1 !== "-") ? 1 : 0;
	$date1_selected += ($date_m1 !== "-") ? 1 : 0;
	$date1_selected += ($date_d1 !== "-") ? 1 : 0;
	if (($date1_selected >= 1 && $date1_selected <= 2) || ($date1_selected === 3 && !checkdate($date_m1, $date_d1, $date_y1))) {
		$errors[] = "開始年月日を正しく選択してください。";
	}
	$date1 = ($date1_selected === 3) ? "$date_y1$date_m1$date_d1" : null;

	$date2_selected = 0;
	$date2_selected += ($date_y2 !== "-") ? 1 : 0;
	$date2_selected += ($date_m2 !== "-") ? 1 : 0;
	$date2_selected += ($date_d2 !== "-") ? 1 : 0;
	if (($date2_selected >= 1 && $date2_selected <= 2) || ($date2_selected === 3 && !checkdate($date_m2, $date_d2, $date_y2))) {
		$errors[] = "終了年月日を正しく選択してください。";
	}
	$date2 = ($date2_selected === 3) ? "$date_y2$date_m2$date_d2" : null;

	if (!is_null($date1) && !is_null($date2) && $date1 > $date2) {
		$errors[] = "終了年月日は開始年月日より後にしてください。";
	}

	if (count($errors) === 0) {
		$sql = "select max(no) from sum_pt_disease";
		$cond = q("where ptif_id = '%s'", $pt_id);
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			js_error_exit();
		}
		$no = intval(pg_fetch_result($sel, 0, 0)) + 1;
	}

	if (count($errors) === 0) {
		if ($is_primary !== "t") {
			$is_primary = "f";
		}
		if ($is_specified !== "t") {
			$is_specified = "f";
		}
		if ($outcome == "") {
			$outcome = "";
		}
		$sql = "insert into sum_pt_disease (ptif_id, no, input_mode, disease_name, full_rece_code, start_ymd, is_primary, is_specified, end_ymd, outcome, icd10, rece_code, byoumei_1, icd10_1, rece_code_1, target_table_class_1, position_1, link_code_1, byoumei_2, icd10_2, rece_code_2, target_table_class_2, position_2, link_code_2, byoumei_3, icd10_3, rece_code_3, target_table_class_3, position_3, link_code_3, byoumei_4, icd10_4, rece_code_4, target_table_class_4, position_4, link_code_4, byoumei_5, icd10_5, rece_code_5, target_table_class_5, position_5, link_code_5) values (";
		$content = array($pt_id, $no, $input_mode, $disease_name, $full_rece_code, $date1, $is_primary, $is_specified, $date2, $outcome, $icd10, $rece_code, $byoumei_1, $icd10_1, $rece_code_1, $target_table_class_1, $position_1, $link_code_1, $byoumei_2, $icd10_2, $rece_code_2, $target_table_class_2, $position_2, $link_code_2, $byoumei_3, $icd10_3, $rece_code_3, $target_table_class_3, $position_3, $link_code_3, $byoumei_4, $icd10_4, $rece_code_4, $target_table_class_4, $position_4, $link_code_4, $byoumei_5, $icd10_5, $rece_code_5, $target_table_class_5, $position_5, $link_code_5);
		$ins = insert_into_table($con, $sql, q($content), $fname);
		if ($ins == 0) {
			pg_close($con);
			js_error_exit();
		}
		my_js_reload_exit();
	}

	for ($i = 1; $i <= 5; $i++) {
		$diseases[$i] = array(
			"byoumei" => (isset($_POST["byoumei_$i"]) ? $_POST["byoumei_$i"] : ""),
			"icd10" => (isset($_POST["icd10_$i"]) ? $_POST["icd10_$i"] : ""),
			"rece_code" => (isset($_POST["rece_code_$i"]) ? $_POST["rece_code_$i"] : ""),
			"target_table_class" => (isset($_POST["target_table_class_$i"]) ? $_POST["target_table_class_$i"] : ""),
			"position" => (isset($_POST["position_$i"]) ? $_POST["position_$i"] : ""),
			"link_code" => (isset($_POST["link_code_$i"]) ? $_POST["link_code_$i"] : "")
		);
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP"/>
<title>CoMedix メドレポート | 傷病名追加</title>
<link rel="stylesheet" type="text/css" href="css/main_summary.css"/>
<script type="text/javascript">
var diseases;

function load_action() {
	initcal();
	changeInputMode(true);
	loadDiseases();
	updateCal1();
	updateCal2();
<? if (count($errors) > 0) { ?>
	alert('<? eh($errors[0]); ?>');
<? } ?>
<? if ($mode == "") { ?>
	openSearchWindow();
<? } ?>
}

function changeInputMode(onInitPage) {
	var isSearchMode = (document.mainform.input_mode[0].checked);
	if (isSearchMode) {
		document.mainform.disease_name.readOnly = true;
		document.getElementById('search_button').style.display = '';
		document.getElementById('suspicion_button').style.display = '';
		document.getElementById('clear_button').style.display = '';
		if (!onInitPage) {
			clearDisease();
		}
	} else {
		document.mainform.disease_name.readOnly = false;
		document.getElementById('search_button').style.display = 'none';
		document.getElementById('suspicion_button').style.display = 'none';
		document.getElementById('clear_button').style.display = 'none';
		if (!onInitPage) {
			var diseaseName = document.mainform.disease_name.value;
			clearDisease();
			showDiseaseName(diseaseName);
		}
	}
}

function loadDiseases() {
	diseases = [];
	for (var i = 1; i <= 5; i++) {
		var byoumei = document.mainform.elements['byoumei_'.concat(i)].value;
		if (byoumei == '') {
			continue;
		}

		var icd10 = document.mainform.elements['icd10_'.concat(i)].value;
		var rece_code = document.mainform.elements['rece_code_'.concat(i)].value;
		var target_table_class = document.mainform.elements['target_table_class_'.concat(i)].value;

		var position = null;
		var link_code = null;
		if (target_table_class == '2') {
			position = document.mainform.elements['position_'.concat(i)].value;
			link_code = document.mainform.elements['link_code_'.concat(i)].value;
		}

		selectDisease(byoumei, icd10, rece_code, target_table_class, position, link_code);
	}
}

function selectDisease(byoumei, icd10, rece_code, target_table_class, position, link_code) {
	var disease = {
		byoumei: byoumei,
		icd10: icd10,
		rece_code: rece_code,
		target_table_class: target_table_class
	};
	if (disease.target_table_class == '2') {
		disease.position = position - 0;  // 数値化
		disease.link_code = link_code;
	}
	addDisease(disease);
	showDisease();
}

function addDisease(disease) {
	if (disease.target_table_class == '1') {
		var doInit = false;
		for (var i = 0, j = diseases.length; i < j; i++) {
			if (diseases[i].target_table_class == '1') {
				doInit = true;
				break;
			}
		}
		if (doInit) {
			diseases = [];
		}
	}

	if (diseases.length < 5) {
		diseases.push(disease);
	}
}

function showDisease() {
	diseases.sort(function(a, b) {

		// 修飾語同士であれば、接続位置区分が大きいほど前に来る
		// 接続位置区分が同じ場合、修飾語交換用コードが小さいほど前に来る
		if (a.target_table_class == '2' && b.target_table_class == '2') {
			if (a.position != b.position) {
				return b.position - a.position;
			} else if (a.link_code < b.link_code) {
				return -1;
			} else if (a.link_code > b.link_code) {
				return 1;
			} else {
				return 0;
			}
		}

		// 接続位置区分が10以上なら病名の前に来る
		if (a.target_table_class == '1') {
			return (b.position >= 10) ? 1 : -1;
		} else {
			return (a.position >= 10) ? -1 : 1;
		}
	});

	var names = [];
	var full_rece_codes = [];
	var icd10 = '';
	var rece_code = '';
	for (var i = 0, j = diseases.length; i < j; i++) {
		names.push(diseases[i].byoumei);
		full_rece_codes.push(diseases[i].rece_code);
		if (diseases[i].target_table_class == '1') {
			icd10 = diseases[i].icd10;
			rece_code = diseases[i].rece_code;
		}
	}
	showDiseaseName(names.join(''));
	showFullReceCode(full_rece_codes.join(','));
	showICD10(icd10);
	showReceCode(rece_code);
}

function onSubmit() {
	for (var i = 1; i <= 5; i++) {
		document.mainform.elements['byoumei_'.concat(i)].value = '';
		document.mainform.elements['icd10_'.concat(i)].value = '';
		document.mainform.elements['rece_code_'.concat(i)].value = '';
		document.mainform.elements['target_table_class_'.concat(i)].value = '';
		document.mainform.elements['position_'.concat(i)].value = '';
		document.mainform.elements['link_code_'.concat(i)].value = '';
	}

	for (var i = 0, j = diseases.length; i < j; i++) {
		document.mainform.elements['byoumei_'.concat(i + 1)].value = diseases[i].byoumei;
		document.mainform.elements['icd10_'.concat(i + 1)].value = diseases[i].icd10;
		document.mainform.elements['rece_code_'.concat(i + 1)].value = diseases[i].rece_code;
		document.mainform.elements['target_table_class_'.concat(i + 1)].value = diseases[i].target_table_class;
		if (diseases[i].target_table_class == '2') {
			document.mainform.elements['position_'.concat(i + 1)].value = diseases[i].position;
			document.mainform.elements['link_code_'.concat(i + 1)].value = diseases[i].link_code;
		}
	}

	document.mainform.submit();
}

function clearDisease() {
	diseases = [];
	showDisease();
}

function showDiseaseName(name) {
	document.mainform.disease_name.value = name;
}

function showFullReceCode(full_rece_code) {
	document.mainform.full_rece_code.value = full_rece_code;
	document.getElementById('full_rece_code').innerHTML = full_rece_code;
}

function showICD10(icd10) {
	document.mainform.icd10.value = icd10;
	document.getElementById('icd10').innerHTML = icd10;
}

function showReceCode(rece_code) {
	document.mainform.rece_code.value = rece_code;
	document.getElementById('rece_code').innerHTML = rece_code;
}

function clearOutcome() {
	for (var i = 0; i < 4; i++) {
		document.mainform.outcome[i].checked = false;
	}
}

var searchWindow;
function openSearchWindow() {
	var left = screen.width - 680;
	if (left > 640) left = 640;
	searchWindow = window.open('summary_disease_rank.php?session=<? eh($session); ?>', 'disease_search', 'directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=' + left + ',top=0,width=680,height=700');
}
function closeSearchWindow() {
	if (searchWindow && !searchWindow.closed) {
		searchWindow.close();
	}
}
</script>
<?
write_yui_calendar_use_file_read_0_12_2();
write_yui_calendar_script2(2);
?>
</head>
<body onload="load_action();" onunload="closeSearchWindow();">
<div style="padding:4px">

<!-- ヘッダエリア -->
<script type="text/javascript" src="js/swapimg.js" charset="euc-jp"></script>
<div>
<table class="dialog_titletable">
<tr>
<th>傷病名追加</th>
<td><a href="javascript:window.close()"><img src="img/icon/close.gif" alt="閉じる" onmouseover="swapimg(this,'img/icon/closeo.gif');" onmouseout="swapimg(this,'img/icon/close.gif');"/></a></td>
</tr>
</table>
</div>

<form name="mainform" action="summary_disease_register.php" method="post">
<table class="prop_table" style="margin-top:10px">
<tr>
<th align="left"><nobr>傷病名</nobr></th>
<td>
<label><input type="radio" name="input_mode" value="1"<? if ($input_mode !== "2") {echo(" checked=\"checked\"");} ?> onclick="changeInputMode();"/>標準病名</label>　　<label><input type="radio" name="input_mode" value="2"<? if ($input_mode === "2") {echo(" checked=\"checked\"");} ?> onclick="changeInputMode();"/>手入力</label>
<table border="0" cellpadding="2">
<tr>
<td><input type="text" name="disease_name" value="<? eh($disease_name); ?>" size="70" readonly="readonly"/></td>
<td><input type="button" id="search_button" value="傷病名検索" onclick="openSearchWindow();"/></td>
</tr>
<tr>
<td><span id="full_rece_code"><? eh($full_rece_code); ?></span><input type="hidden" name="full_rece_code" value="<? eh($full_rece_code); ?>"></td>
<td>
<input type="button" id="suspicion_button" value="の疑い" onclick="selectDisease('の疑い', '', '8002', '2', 1, '5395');"/>
<input type="button" id="clear_button" value="クリア" onclick="clearDisease();"/>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<th align="left"><nobr>開始年月日</nobr></th>
<td><div style="float:left;"><select id="date_y1" name="date_y1"><? show_select_years(date("Y") - 1970 + 1, $date_y1, true); ?></select>/<select id="date_m1" name="date_m1"><? show_select_months($date_m1, true); ?></select>/<select id="date_d1" name="date_d1"><? show_select_days($date_d1, true); ?></select></div>
<div style="float:left;margin-left:2px;margin-right:3px;"><img src="img/calendar_link.gif" style="left:3px;z-index:1;cursor:pointer;" onclick="show_cal1();"><br><div id="cal1Container" style="position:absolute;display:none;z-index:10000;"></div></div></td>
</tr>
<tr>
<th></th>
<td><label><input type="checkbox" name="is_primary" value="t"<? if ($is_primary === "t") {echo(" checked=\"checked\"");} ?>/>主病名</label>　　<label><input type="checkbox" name="is_specified" value="t"<? if ($is_specified === "t") {echo(" checked=\"checked\"");} ?>/>特定疾患</label></td>
</tr>
<tr>
<th align="left"><nobr>終了年月日</nobr></th>
<td><div style="float:left;"><select id="date_y2" name="date_y2"><? show_select_years(date("Y") - 1970 + 1, $date_y2, true); ?></select>/<select id="date_m2" name="date_m2"><? show_select_months($date_m2, true); ?></select>/<select id="date_d2" name="date_d2"><? show_select_days($date_d2, true); ?></select></div>
<div style="float:left;margin-left:2px;margin-right:3px;"><img src="img/calendar_link.gif" style="left:3px;z-index:1;cursor:pointer;" onclick="show_cal2();"><br><div id="cal2Container" style="position:absolute;display:none;z-index:10000;"></div></div></td>
</tr>
<tr>
<th align="left"><nobr>転帰</nobr></th>
<td><label><input type="radio" name="outcome" value="1"<? if ($outcome === "1") {echo(" checked=\"checked\"");} ?>/>治癒</label>　　<label><input type="radio" name="outcome" value="2"<? if ($outcome === "2") {echo(" checked=\"checked\"");} ?>/>死亡</label>　　<label><input type="radio" name="outcome" value="3"<? if ($outcome === "3") {echo(" checked=\"checked\"");} ?>/>中止</label>　　<label><input type="radio" name="outcome" value="4"<? if ($outcome === "4") {echo(" checked=\"checked\"");} ?>/>移行</label>　　<input type="button" value="クリア" onclick="clearOutcome();"/></td>
</tr>
<tr>
<th align="left"><nobr>ICD10</nobr></th>
<td><span id="icd10"></span><input type="hidden" name="icd10" value=""/></td>
</tr>
<tr>
<th align="left"><nobr>レセ電算コード</nobr></th>
<td><span id="rece_code"></span><input type="hidden" name="rece_code" value=""/></td>
</tr>
</table>

<table cellspacing="0" cellpadding="0" width="100%" style="margin-top:5px">
<tr>
<td align="right"><input type="button" value="登録" onclick="onSubmit();"/></td>
</tr>
</table>

<input type="hidden" name="session" value="<? eh($session); ?>"/>
<input type="hidden" name="pt_id" value="<? eh($pt_id); ?>"/>
<input type="hidden" name="mode" value="register"/>

<? for ($i = 1; $i <= 5; $i++) { ?>
<input type="hidden" name="byoumei_<? echo($i); ?>" value="<? eh($diseases["$i"]["byoumei"]); ?>"/>
<input type="hidden" name="icd10_<? echo($i); ?>" value="<? eh($diseases["$i"]["icd10"]); ?>"/>
<input type="hidden" name="rece_code_<? echo($i); ?>" value="<? eh($diseases["$i"]["rece_code"]); ?>"/>
<input type="hidden" name="target_table_class_<? echo($i); ?>" value="<? eh($diseases["$i"]["target_table_class"]); ?>"/>
<input type="hidden" name="position_<? echo($i); ?>" value="<? eh($diseases["$i"]["position"]); ?>"/>
<input type="hidden" name="link_code_<? echo($i); ?>" value="<? eh($diseases["$i"]["link_code"]); ?>"/>
<? } ?>

</form>

</div>
</body>
<? pg_close($con); ?>
</html>
<?
function my_js_reload_exit() {
?>
<script type="text/javascript">opener.location.reload(true); self.close();</script>
<?
	exit;
}
