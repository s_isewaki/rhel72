<?
///*****************************************************************************
// 勤務シフト作成 | 看護補助者表「ＥＸＣＥＬ」 
///*****************************************************************************

ini_set("max_execution_time", 0);
ob_start();
require_once("about_authority.php");
require_once("about_session.php");
require_once("about_postgres.php");

require_once("duty_shift_common_class.php");
require_once("duty_shift_report_common_class.php");
require_once("duty_shift_report2_common_class.php");

///-----------------------------------------------------------------------------
//画面名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
//セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}
///-----------------------------------------------------------------------------
//DBコネクション取得
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
$obj_report = new duty_shift_report_common_class($con, $fname);
$obj_report2 = new duty_shift_report2_common_class($con, $fname);
///-----------------------------------------------------------------------------
//処理
///-----------------------------------------------------------------------------
    ///-----------------------------------------------------------------------------
    //初期値設定
    ///-----------------------------------------------------------------------------
    $sum_night_times = array(); //「夜勤専従者及び月１６時間以下」の合計
    $excel_flg = "1";           //ＥＸＣＥＬ表示フラグ（１：ＥＸＣＥＬ表示）
    $download_data = "";        //ＥＸＣＥＬデータ
    ///-----------------------------------------------------------------------------
    //作成日付(現在日付)の取得
    ///-----------------------------------------------------------------------------
    $date = getdate();
    $create_yyyy = $date["year"];
    $create_mm = $date["mon"];
    $create_dd = $date["mday"];
    ///-----------------------------------------------------------------------------
    //表示年／月設定
    ///-----------------------------------------------------------------------------
    if ($duty_yyyy == "") { $duty_yyyy = $create_yyyy; }
    if ($duty_mm == "") { $duty_mm = $create_mm; }
    $day_cnt = $obj->days_in_month($duty_yyyy, $duty_mm);
    ///-----------------------------------------------------------------------------
    //終了年月
    ///-----------------------------------------------------------------------------
    if ($night_start_day == "") {
        $night_start_day = "1";
    }
    $end_yyyy = (int)$duty_yyyy;
    $end_mm = (int)$duty_mm;
    $end_dd = (int)$day_cnt;
    if ($recomputation_flg == "2") {
        $wk_day = (int)$night_start_day + 27;
        $tmp_time = mktime(0, 0, 0, $duty_mm, $wk_day, $duty_yyyy);
        $tmp_date = date("Ymd", $tmp_time);
        $end_yyyy = (int)substr($tmp_date,0,4);
        $end_mm = (int)substr($tmp_date,4,2);
        $end_dd = (int)substr($tmp_date,6,2);
        $day_cnt = 28;
    }
    ///-----------------------------------------------------------------------------
    // カレンダー(calendar)情報を取得
    ///-----------------------------------------------------------------------------
    $start_date = sprintf("%04d%02d%02d",$duty_yyyy, $duty_mm, $night_start_day);
    $end_date = sprintf("%04d%02d%02d",$end_yyyy, $end_mm, $end_dd);
    $calendar_array = $obj->get_calendar_array($start_date, $end_date);
    ///-----------------------------------------------------------------------------
    //指定月の全曜日を設定
    ///-----------------------------------------------------------------------------
    $week_array = array();
    //4週計算時の曜日不具合対応 20120323
    $wk = (int)$night_start_day;
    $tmp_date = mktime(0, 0, 0, $duty_mm, $wk, $duty_yyyy);
    for ($k=1; $k<=$day_cnt; $k++) {
        $week_array[$k]["name"] = $obj->get_weekday($tmp_date);
        $tmp_date += 86400;
    }
    ///-----------------------------------------------------------------------------
    //施設基準情報の取得
    ///-----------------------------------------------------------------------------
    $standard_array = $obj->get_duty_shift_institution_history_array($standard_id, $duty_yyyy, $duty_mm);
    //個別に病棟が選択されていた場合は、病棟毎の１日入院患者数をセットする
    if ($group_id != "0") {
        $group_array = $obj->get_duty_shift_group_array($group_id, "", array());
        $standard_array[0]["hosp_patient_cnt"] = $group_array[0]["hosp_patient_cnt"];
        $hosp_patient_cnt = $group_array[0]["hosp_patient_cnt"];
    }
    ///-----------------------------------------------------------------------------
    // 様式９用　看護職員情報取得
    ///-----------------------------------------------------------------------------
    $draft_reg_flg = isset($draft_reg_flg) ? $draft_reg_flg : "";    //下書きデータ登録状態
    $data = $obj_report->get_report_array(
            $standard_id,
            $duty_yyyy,
            $duty_mm,
            $night_start_day,
            $end_yyyy,
            $end_mm,
            $end_dd,
            $day_cnt,
            $standard_array[0],
            $group_id,
            $plan_result_flag,
            $draft_reg_flg
    );
    $data_array = $data["data_array"];
    $data_sub_array = $data["data_sub_array"];

    $nurse_cnt = $data["nurse_cnt"];                        //看護師数
    $sub_nurse_cnt = $data["sub_nurse_cnt"];                //准看護師数
    $assistance_nurse_cnt = $data["assistance_nurse_cnt"];  //看護補助者数

    $nurse_sumtime = $data["nurse_sumtime"];                        //看護師総勤務時間
    $sub_nurse_sumtime = $data["sub_nurse_sumtime"];                //准看護師総勤務時間
    $assistance_nurse_sumtime = $data["assistance_nurse_sumtime"];  //看護補助者総勤務時間

    //看護補助者が存在しない場合、メッセージを表示して終了
    if ($assistance_nurse_cnt == 0) {
        $wk = "看護補助者が存在しません。";
        echo("<script language='javascript'>alert('$wk');</script>");
        echo("<script type=\"text/javascript\">history.back();</script>");
        exit;
    }

    ///*****************************************************************************
    //ＨＴＭＬ作成（看護補助者表）
    ///*****************************************************************************
    ///-----------------------------------------------------------------------------
    // ＨＴＭＬ作成（見出し）
    ///-----------------------------------------------------------------------------
    $btn_show_flg = ""; //１：ボタン表示
    // metaタグで文字コード設定必須（ない場合にはMacで文字化けするため）
    $download_data_1 = "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Shift_JIS\">";
    $download_data_1 .= "<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">\n";
    $download_data_1 .= $obj_report2->showNurseTitle_1($btn_show_flg,
                                                    $recomputation_flg,         //１ヵ月計算４週計算フラグ（１：１ヵ月計算、２：４週計算）
                                                    $date_y1,                   //カレンダー用年
                                                    $date_m1,                   //カレンダー用月
                                                    $date_d1,                   //カレンダー用日
                                                    $day_cnt,               //日数
                                                    $group_id,
                                                    $create_yyyy,           //作成年
                                                    $create_mm,             //作成月
                                                    $create_dd,             //作成日
                                                    $plan_result_flag,      //予定実績フラグ 20120209
                                                    $duty_yyyy,
                                                    $duty_mm
                                                    );
    
    ///-----------------------------------------------------------------------------
    // ＨＴＭＬ作成（タイトル）
    ///-----------------------------------------------------------------------------
$wk_array1 = $obj_report2->showNurseData_1($excel_flg, $week_array, $calendar_array, $day_cnt, $group_id, $duty_yyyy, $duty_mm);
    $download_data_1 .= $wk_array1["data"];
    ///-----------------------------------------------------------------------------
    // ＨＴＭＬ作成（データ） 
    ///-----------------------------------------------------------------------------
$wk_array2 = $obj_report2->showNurseData_2($excel_flg, $data_sub_array, $day_cnt, $standard_array, $group_id, $duty_yyyy, $duty_mm);
    for ($i=0; $i<count($data_sub_array); $i++) {
        $download_data_2 .= $wk_array2[$i]["data"];
    }
    ///-----------------------------------------------------------------------------
    // ＨＴＭＬ作成（合計　日勤時間数）
    // ＨＴＭＬ作成（合計　夜勤時間数）
    ///-----------------------------------------------------------------------------
$wk_array3 = $obj_report2->showNurseData_3($excel_flg, $data_sub_array, $day_cnt, $wk_array2["sum_night_times"], $group_id, $hosp_patient_cnt, $standard_array, $duty_yyyy, $duty_mm);
    $download_data_3 .= $wk_array3["data"];
    $download_data_3 .= "</table>\n";
    ///-----------------------------------------------------------------------------
    //データEXCEL出力
    ///-----------------------------------------------------------------------------
    ob_clean();
///-----------------------------------------------------------------------------
//ファイル名
///-----------------------------------------------------------------------------
if ($group_id == "0") {
    $wk_mm = sprintf("%02d", $duty_mm);
    $filename = "youshiki_9_2_$duty_yyyy$wk_mm.xls";
} else {
    $group_name = $obj->get_group_name($group_id);
    $filename = 'youshiki_9_2_'.$group_name.'.xls';
}
$ua = $_SERVER['HTTP_USER_AGENT'];
$filename = mb_convert_encoding($filename, 'UTF-8', mb_internal_encoding());

if(!preg_match("/safari/i",$ua)){
    header('Content-Type: application/octet-stream');
}
else {
    header('Content-Type: application/vnd.ms-excel');
}

if (preg_match("/MSIE/i", $ua)) {
    if (strlen(rawurlencode($filename)) > 21 * 3 * 3) {
        $filename = mb_convert_encoding($filename, "SJIS-win", "UTF-8");
        $filename = str_replace('#', '%23', $filename);
    }
    else {
        $filename = rawurlencode($filename);
    }
}
elseif (preg_match("/Trident/i", $ua)) {// IE11
    $filename = rawurlencode($filename);
}
elseif (preg_match("/chrome/i", $ua)) {// Google Chromeには『Safari』という字が含まれる
}
elseif (preg_match("/safari/i", $ua)) {// Safariでファイル名を指定しない場合
    $filename = "";
}
    header('Content-Disposition: attachment; filename=' . $filename);
    header('Expires: 0');
    header('Cache-Control: must-revalidate, post-check=0,pre-check=0');
    header('Pragma: public');
    echo mb_convert_encoding(nl2br($download_data_1), 'sjis', mb_internal_encoding());
    echo mb_convert_encoding(nl2br($download_data_2), 'sjis', mb_internal_encoding());
    echo mb_convert_encoding(nl2br($download_data_3), 'sjis', mb_internal_encoding());
    ob_end_flush();

    pg_close($con);
?>

