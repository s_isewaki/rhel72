<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 病床管理 | マスタ管理 | マスタ作成</title>
<?
require_once("about_comedix.php");
require_once("summary_tmplitemrireki.ini");

$fname = $PHP_SELF;

// セッションをチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限をチェック
$checkauth = check_authority($session, 21, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 診療科登録権限を取得
$section_admin_auth = check_authority($session, 28, $fname);

$default_url = ($section_admin_auth == "1") ? "entity_menu.php" : "building_list.php";

// データベースに接続
$con = connect2db($fname);

//==============================
// マスタ一覧を取得
//==============================
$sql = "select mst_cd, mst_name from tmplitemmst";
$cond = "order by mst_cd";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$mst_list = pg_fetch_all($sel);

//マスタ管理コードが指定されていない場合は先頭マスタを指定
if($mst_cd == "" && $mst_list != "")
{
	$mst_cd = $mst_list[0]["mst_cd"];
}

//==============================
//ポストバック時
//==============================
if($postback == "true")
{
	if($postback_mode == "item_delete")
	{
		// トランザクションの開始
		pg_query($con, "begin");

		$delete_target_list = "";
		foreach($item_select as $item_cd)
		{
			if($delete_target_list != "")
			{
				$delete_target_list .= ",";
			}
			$delete_target_list .= "'".pg_escape_string($item_cd)."'";
		}
		$sql = "delete from tmplitem where mst_cd = '$mst_cd' and item_cd in($delete_target_list)";
		$del = delete_from_table($con, $sql, "", $fname);
		if ($del == 0)
		{
			pg_query($con,"rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		//履歴更新
		entry_tmplitemrireki($con,$fname,$mst_cd);

		// トランザクションのコミット
		pg_query($con, "commit");
	}
	elseif($postback_mode == "item_disp_update")
	{
		// トランザクションの開始
		pg_query($con, "begin");

		foreach($list_item_cd as $item_cd)
		{
			$param_name = "disp_order_".$item_cd;
			$disp_order = $$param_name;
			$param_name = "disp_flg_".$item_cd;
			$disp_flg = $$param_name;

			$sql = "update tmplitem set";
			$set = array("disp_flg", "disp_order");
			$setvalue = array(
				$disp_flg,
				intval($disp_order)
				);
			$cond = "where mst_cd = '$mst_cd' and item_cd = '$item_cd'";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con,"rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}

		}

		//履歴更新
		entry_tmplitemrireki($con,$fname,$mst_cd);

		// トランザクションのコミット
		pg_query($con, "commit");
	}
}

//==============================
//項目一覧を取得
//==============================
$item_list = "";
if($mst_cd != "")
{
	$sql = "select * from tmplitem";
	$cond = "where mst_cd = '$mst_cd' order by disp_order";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$item_list = pg_fetch_all($sel);
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
<!--
function registMst() {

	window.open('bed_master_registration.php?session=<?=$session?>', 'newwin', 'width=760,height=400,scrollbars=yes');
//	location.href = 'bbsthread_register.php?session=<? echo($session); ?>&sort=<? echo($sort); ?>&show_content=<? echo($show_content); ?>';
}

function deleteMst() {
	var checked_ids = getCheckedIds(document.delmst.elements['mst_cds[]']);
	if (checked_ids.length == 0) {
		alert('マスタが選択されていません。');
		return;
	}

	if (!confirm('選択されたマスタを削除します。よろしいですか？')) {
		return;
	}

	document.delmst.submit();
}

function updateMst() {
	document.updmst.submit();
}

function getCheckedIds(boxes) {
	var checked_ids = new Array();
	if (boxes) {
		if (!boxes.length) {
			if (boxes.checked) {
				checked_ids.push(boxes.value);
			}
		} else {
			for (var i = 0, j = boxes.length; i < j; i++) {
				if (boxes[i].checked) {
					checked_ids.push(boxes[i].value);
				}
			}
		}
	}
	return checked_ids;
}



function registItem()
{
	window.open('bed_master_item_input.php?session=<?=$session?>&mode=insert&mst_cd=<?=$mst_cd?>', 'newwin', 'width=760,height=400,scrollbars=yes');
}
function updateItem(item_cd)
{
	window.open('bed_master_item_input.php?session=<?=$session?>&mode=update&mst_cd=<?=$mst_cd?>&item_cd='+item_cd , 'newwin', 'width=760,height=400,scrollbars=yes');
}
function deleteItem()
{
	var is_selected = false;
	var objs = document.getElementsByName("item_select[]");
	for(var i=0;i<objs.length;i++)
	{
		if(objs[i].checked)
		{
			is_selected = true;
			break;
		}
	}

	if(!is_selected)
	{
		alert("選択されていません。");
		return;
	}


	if (!confirm('選択された項目を削除します。よろしいですか？')) {
		return;
	}


	document.itemform.postback_mode.value = "item_delete";
	document.itemform.submit();
}
function updateItemDisp()
{
	var objs = document.getElementsByName("item_select[]");
	for(var i=0;i<objs.length;i++)
	{
		var item_cd = objs[i].value;
		var objs = document.getElementsByName("disp_order_"+item_cd);
		var val = objs[0].value;
		if(val == "" || !val.match(/^[0-9]+$/))
		{
			alert("表示順は数値を入力してください。");
			return;
		}
	}

	document.itemform.postback_mode.value = "item_disp_update";
	document.itemform.submit();
}

function reload_page()
{
	location.href = "bed_master_menu.php?session=<?=$session?>&mst_cd=<?=$mst_cd?>";
}

//-->
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
.list2 {border-collapse:collapse;}
.list2 td {border:#5279a5 solid 1px;}
.list2_in1 {border-collapse:collapse;}
.list2_in1 td {border-width:0;}
.list2_in2 {border-collapse:collapse;}
.list2_in2 td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="bed_menu.php?session=<? echo($session); ?>"><img src="img/icon/b17.gif" width="32" height="32" border="0" alt="病床管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="bed_menu.php?session=<? echo($session); ?>"><b>病床管理</b></a> &gt; <a href="<? echo($default_url); ?>?session=<? echo($session); ?>"><b>管理画面</b></a> &gt; <a href="bed_master_menu.php?session=<? echo($session); ?>"><b>マスタ管理</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="bed_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- left -->
<td width="120" valign="top">
<table width="120" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width="118" valign="top">
<table width="118" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#5279A5">
<td height="22" class="spacing" colspan="2"><b><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff">管理項目</font></b></td>
</tr>
<? if ($section_admin_auth == "1") { ?>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="entity_menu.php?session=<? echo($session); ?>">診療科</a></font></td>
</tr>
<? } ?>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="building_list.php?session=<? echo($session); ?>">病棟</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_display_setting.php?session=<? echo($session); ?>">表示設定</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_check_setting.php?session=<? echo($session); ?>">管理項目設定</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_bulk_inpatient_register.php?session=<? echo($session); ?>">一括登録</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_master_menu.php?session=<? echo($session); ?>">マスタ管理</a></font></td>
</tr>
</table>
</td>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
</td>
<!-- left -->
<!-- center -->
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->
<!-- right -->
<td valign="top">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="85" align="center" bgcolor="#5279a5"><a href="bed_master_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>マスタ作成</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="bed_master_disease.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病名</font></a></td>
<td width="5">&nbsp;</td>
<td width="105" align="center" bgcolor="#bdd1e7"><a href="bed_exception_setting.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">計算除外理由</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="bed_master_decubation.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">回復期リハ算定</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="bed_master_rehabilitation.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">リハビリ算定</font></a></td>
<td width="5">&nbsp;</td>
<td width="115" align="center" bgcolor="#bdd1e7"><a href="bed_master_institution.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">医療機関・施設</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="3"><br>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="30%" valign="top">
<!-- theme list start -->
<form name="delmst" action="bed_master_delete.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr bgcolor="#f6f9ff">
<td class="spacing" height="24"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">マスタ一覧</font></td>
<td align="right">
<input type="button" value="作成" onclick="registMst();">
<input type="button" value="削除" onclick="deleteMst();">
</td>
</tr>
</table>
</tr>
</td>
<tr height="400">
<td valign="top">
<?
//$theme_info = show_theme_list($con, $theme_id, $emp_id, $sort, $newpost_days, $show_content, $session, $fname);
show_mst_list($con, $mst_cd, $session, $fname);
?>
</td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="mst_cd" value="<? echo($mst_cd); ?>">
</form>
<!-- theme list end -->
</td>
<td><img src="img/spacer.gif" alt="" width="2" height="1"></td>
<td width="70%" valign="top">
<!-- theme detail start -->
<form name="updmst" action="bed_master_update_exe.php" method="post">
<?
//$mst_cd = "0001";
if ($mst_cd != "") {
	show_mst_detail($con, $mst_cd, $fname);
?>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<?
}
?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="mst_cd" value="<? echo($mst_cd); ?>">
</form>
<!-- theme detail end -->
<!-- comment view start -->
<form name="itemform" action="bed_master_menu.php" method="post">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="mst_cd" value="<? echo($mst_cd); ?>">
<input type="hidden" name="postback" value="true">
<input type="hidden" name="postback_mode" value="">

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list2">
<tr>
<td>

<table width="100%" border="0" cellspacing="0" cellpadding="1" class="list2_in1">
<tr bgcolor="#f6f9ff">
<td width="20%" class="spacing" height="24"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">管理項目一覧</font></td>
<td align="right" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
if ($mst_cd != "")
{
	if($item_list != "")
	{
?>
<input type="button" value="削除"     onclick="deleteItem()"     style="width:80px">
<?
	}
?>
<input type="button" value="項目追加" onclick="registItem()"     style="width:80px">
<?
	if($item_list != "")
	{
?>
<input type="button" value="表示更新" onclick="updateItemDisp()" style="width:80px">
<?
	}
}
?>
</font>
</td>
</tr>
</table>

</td>
</tr>
<tr height="328">
<td valign="top" style="padding:2px;">

<?
if($item_list == "")
{
?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録済みの項目はありません。</font>
<?
}
else
{
?>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="list2_in2">
<tr>
<td bgcolor="#f6f9ff" width="30">
	 <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">&nbsp</font>
</td>
<td bgcolor="#f6f9ff" width="70">
	 <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">項目コード</font>
</td>
<td bgcolor="#f6f9ff" width="*">
	 <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">項目名称</font>
</td>
<td bgcolor="#f6f9ff" width="100">
	 <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示区分</font>
</td>
<td bgcolor="#f6f9ff" width="70">
	 <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示順</font>
</td>
</tr>
<?
	foreach($item_list as $item_list1)
	{
		$item_cd = $item_list1['item_cd'];
		$item_name = $item_list1['item_name'];
		$disp_flg = $item_list1['disp_flg'];
		$disp_order = $item_list1['disp_order'];
?>
<input type="hidden" name="list_item_cd[]" value="<?=$item_cd?>">
<tr>
<td bgcolor="#ffffff" align="center">
	 <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
	 <input type="checkbox" name="item_select[]" value="<?=$item_cd?>">
	 </font>
</td>
<td bgcolor="#ffffff">
	 <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
	 <?=$item_cd?>
	 </font>
</td>
<td bgcolor="#ffffff">
	 <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
	 <a href="javascript:updateItem('<?=$item_cd?>')">
	 <?=h($item_name) ?>
	 </a>
	 </font>
</td>
<td bgcolor="#ffffff">
	 <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
		<select name="disp_flg_<?=$item_cd?>">
		<option value="t" <?if($disp_flg != "f"){?>selected<?}?> >表示</option>
		<option value="f" <?if($disp_flg == "f"){?>selected<?}?> >非表示</option>
		</select>
	 </font>
</td>
<td bgcolor="#ffffff">
	 <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
		<input type="text" name="disp_order_<?=$item_cd?>" value="<?=$disp_order?>" maxlength="4" size="4" style="ime-mode: inactive;">
	 </font>
</td>
</tr>
<?
	}
?>
</table>
<?
}
?>
</td>
</tr>
</table>
</form>
<!-- comment view end -->
</td>
</tr>
</table>

</td>
<!-- right -->
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
// マスタ情報内容を表示
function show_mst_detail($con, $mst_cd, $fname) {

	// マスタ情報を取得
//	$sql = "select a.*, b.emp_lt_nm, b.emp_ft_nm from tmplitemmst a left join empmst b on a.create_emp_id = b.emp_id";
	$sql = "select a.* from tmplitemmst a";
	$cond = "where a.mst_cd = '$mst_cd'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$mst_name = pg_fetch_result($sel, 0, "mst_name");
	$mst_content = pg_fetch_result($sel, 0, "mst_content");
//	$emp_nm = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");//初期データの場合は空欄となる。
	$date = pg_fetch_result($sel, 0, "create_time");
	$date = preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})/", "$1/$2/$3 $4:$5", $date);
	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"list\">\n");
	echo("<tr height=\"22\">\n");
	echo("<td width=\"22%\" align=\"right\" bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">マスタ管理コード</font></td>\n");
	echo("<td width=\"10%\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".h($mst_cd)."</font></td>\n");
	echo("<td width=\"15%\" align=\"right\" bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">マスタ名称</font></td>\n");
	echo("<td><input type=\"text\" name=\"mst_name\" value=\"".h($mst_name)."\" maxlength=\"40\" size=\"40\" style=\"ime-mode: active;width:100%\"></td>\n");
	echo("<td width=\"10%\" align=\"center\"><input type=\"button\" value=\"更新\" onclick=\"updateMst();\"></td>\n");
	echo("</tr>\n");
	echo("<tr height=\"22\">\n");
	echo("<td align=\"right\" bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">内容</font></td>\n");
	echo("<td colspan=\"4\"><textarea name=\"mst_content\" style=\"ime-mode: active;width:100%\" rows=\"3\">".h($mst_content)."</textarea></td>\n");
//	echo("<td colspan=\"4\"><input type=\"text\" name=\"mst_content\" value=\"$mst_content\" maxlength=\"100\" size=\"60\" style=\"ime-mode: active;width:100%\"></td>\n");
	echo("</tr>\n");
/*
	echo("<tr height=\"22\">\n");
	echo("<td width=\"14%\" align=\"right\" bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">作成者</font></td>\n");
	echo("<td width=\"\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$emp_nm</font></td>\n");
	echo("<td width=\"18%\" align=\"right\" bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">作成日時</font></td>\n");
	echo("<td colspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$date</font></td>\n");
	echo("</tr>\n");
*/
	echo("</table>\n");

}

// マスタ一覧を表示
function show_mst_list($con, $mst_cd, $session, $fname) {

	$ret_mst_cd = "";

	// マスタ一覧を取得
	$sql = "select mst_cd, mst_name from tmplitemmst";
	$cond = "order by mst_cd";

	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	if (pg_num_rows($sel) == 0) {
		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">\n");
		echo("<tr height=\"22\">\n");
		echo("<td class=\"spacing\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">登録済みのマスタはありません。</font></td>\n");
		echo("</tr>\n");
		echo("</table>\n");
		return $ret_mst_cd;
	}

	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">\n");

	while ($row = pg_fetch_array($sel)) {

		$tmp_mst_cd = $row["mst_cd"];
		$tmp_mst_name = $row["mst_name"];
		$tmp_title = $tmp_mst_cd ." ". $tmp_mst_name;

		if ($mst_cd == "") {
			$mst_cd = $tmp_mst_cd;
		}

		$cbox = "<input type=\"checkbox\" name=\"mst_cds[]\" value=\"$tmp_mst_cd\">";
		$label = ($tmp_mst_cd == $mst_cd) ? $tmp_title : "<a href=\"bed_master_menu.php?session=$session&mst_cd=$tmp_mst_cd\">$tmp_title</a>";
		echo("<tr height=\"22\">\n");
		echo("<td>\n");
		echo("<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
		echo("<tr>\n");
		if ($tmp_mst_cd == $mst_cd) {
			echo("<td><img src=\"img/right_red.gif\" alt=\"\" width=\"17\" height=\"17\"></td>\n");
		} else {
			echo("<td><img src=\"img/spacer.gif\" alt=\"\" width=\"17\" height=\"17\"></td>\n");
		}
		echo("<td valign=\"top\">$cbox</td>\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$label</font></td>\n");
		echo("</tr>\n");
		echo("</table>\n");
		echo("</td>\n");
		echo("</tr>\n");

		if ($tmp_mst_cd == $mst_cd) {
			$ret_mst_cd = $tmp_mst_cd;
		}
	}
	echo("</table>\n");

	return $ret_mst_cd;
}
?>
