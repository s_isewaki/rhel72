<?
require_once("about_comedix.php");
require_once("show_select_values.ini");
require_once("fplus_application_list.ini");
require_once("yui_calendar_util.ini");
require_once("fplus_common.ini");

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック
//====================================
$checkauth=check_authority($session,78,$fname);
if($checkauth=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
// 管理者権限の取得
$fplusadm_auth = check_authority($session, 79, $fname);

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
// 初期化処理
//====================================
$arr_wkfwcatemst = array();
$sel_wkfwcate = search_wkfwcatemst($con, $fname);
while($row_cate = pg_fetch_array($sel_wkfwcate)) {
	$tmp_wkfw_type = $row_cate["wkfw_type"];
	$tmp_wkfw_nm = $row_cate["wkfw_nm"];

	$arr_wkfwmst_tmp = array();
	$sel_wkfwmst = search_wkfwmst($con, $fname, $tmp_wkfw_type);
	while($row_wkfwmst = pg_fetch_array($sel_wkfwmst)) {
		$tmp_wkfw_id = $row_wkfwmst["wkfw_id"];
		$wkfw_title = $row_wkfwmst["wkfw_title"];

		$arr_wkfwmst_tmp[$tmp_wkfw_id] = $wkfw_title;
	}
	$arr_wkfwcatemst[$tmp_wkfw_type] = array("name" => $tmp_wkfw_nm, "wkfw" => $arr_wkfwmst_tmp);
}

//オプション設定の取得
$conf = new Cmx_SystemConfig();
$arrise_date_use = $conf->get('fplus.arrise_date_use');


/*
// 「CoMedix」というカテゴリ追加
$arr_comedix_tmp["1"] = "議事録公開申請（委員会・WG）";
$arr_comedix_tmp["2"] = "残業申請";
$arr_comedix_tmp["3"] = "勤務時間修正申請";
$arr_comedix_tmp["4"] = "退勤後復帰申請";
$arr_wkfwcatemst["0"] = array("name" => "CoMedix", "wkfw" => $arr_comedix_tmp);
*/

// 申請日セット(本日日付から過去２週間分)
// 申請タブをクリックした場合のみ。
if($apply_date_defalut == "on")
{
	$today = date("Y/m/d", strtotime("today"));

	$arr_today = split("/", $today);
	$date_y2 = $arr_today[0];
	$date_m2 = $arr_today[1];
	$date_d2 = $arr_today[2];

	// ２週間前の日付取得
	$two_weeks_ago = date("Y/m/d",strtotime("-2 week" ,strtotime($today)));

	$arr_two_weeks_ago = split("/", $two_weeks_ago);
	$date_y1 = $arr_two_weeks_ago[0];
	$date_m1 = $arr_two_weeks_ago[1];
	$date_d1 = $arr_two_weeks_ago[2];
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ファントルくん＋ | 報告一覧</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<?
// 外部ファイルを読み込む
write_yui_calendar_use_file_read_0_12_2();

// カレンダー作成、関数出力
write_yui_calendar_script2(2);
?>

<script type="text/javascript">

function reload_page() {
	document.apply.action="fplus_apply_list.php?session=<?=$session?>";
	document.apply.page.value = '<? echo($page); ?>';
	document.apply.submit();
}

function initPage() {

	cateOnChange();
}

function cateOnChange() {

	var obj_cate = document.apply.category;
	var obj_wkfw = document.apply.workflow;

	var cate_id = getSelectedValue(obj_cate);
	// 申請書セレクトボックスのオプションを全削除
	deleteAllOptions(obj_wkfw);

	// 申請書セレクトボックスのオプションを作成
	addOption(obj_wkfw, '-', 'すべて');

<? foreach ($arr_wkfwcatemst as $tmp_cate_id => $arr) { ?>
	if (cate_id == '<? echo $tmp_cate_id; ?>') {
	<? foreach($arr["wkfw"] as $tmp_wkfw_id => $tmp_wkfw_nm)
	   {
           $tmp_wkfw_nm = h($tmp_wkfw_nm, ENT_QUOTES);
           $tmp_wkfw_nm = str_replace("&#039;", "\'", $tmp_wkfw_nm);
           $tmp_wkfw_nm = str_replace("&quot;", "\"", $tmp_wkfw_nm);
    ?>
		addOption(obj_wkfw, '<?=$tmp_wkfw_id?>', '<?=$tmp_wkfw_nm?>',  '<?=$workflow?>');
	<? } ?>
	}
<? } ?>
}



function getSelectedValue(sel) {
	return sel.options[sel.selectedIndex].value;
}


function deleteAllOptions(box) {
	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}
}

function addOption(box, value, text, selected) {

	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	if (selected == value) {
		opt.selected = true;
	}
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
}

function highlightCells(class_name) {
	changeCellsColor(class_name, '#ffff66');
}

function dehighlightCells(class_name) {
	changeCellsColor(class_name, '');
}

function changeCellsColor(class_name, color) {
	var cells = document.getElementsByTagName('td');
	for (var i = 0, j = cells.length; i < j; i++) {
		if (cells[i].className != class_name) {
			continue;
		}
		cells[i].style.backgroundColor = color;
	}
}


function apply_search() {

	document.apply.action="fplus_apply_list.php?session=<?=$session?>";
	document.apply.submit();

}

function re_apply() {

	if (document.apply.elements['re_apply_chk[]'] == undefined) {
		alert('「再報告」するデータを選択してくだい。');
		return;
	}

	if (document.apply.elements['re_apply_chk[]'].length == undefined) {
		if (!document.apply.elements['re_apply_chk[]'].checked) {
			alert('「再報告」するデータを選択してくだい。');
			return;
		}
	} else {
		var checked = false;
		for (var i = 0, j = document.apply.elements['re_apply_chk[]'].length; i < j; i++) {
			if (document.apply.elements['re_apply_chk[]'][i].checked) {
				checked = true;
				break;
			}
		}
		if (!checked) {
			alert('「再報告」するデータを選択してくだい。');
			return;
		}
	}

	if (confirm('再報告します。よろしいですか？')) {
		document.apply.action="fplus_application_list_re_apply.php?session=<?=$session?>";
		document.apply.submit();
	}

}


function apply_cancel() {

	if (document.apply.elements['apply_cancel_chk[]'] == undefined) {
		alert('「報告取消」するデータを選択してくだい。');
		return;
	}

	if (document.apply.elements['apply_cancel_chk[]'].length == undefined) {
		if (!document.apply.elements['apply_cancel_chk[]'].checked) {
			alert('「報告取消」するデータを選択してくだい。');
			return;
		}
	} else {
		var checked = false;
		for (var i = 0, j = document.apply.elements['apply_cancel_chk[]'].length; i < j; i++) {
			if (document.apply.elements['apply_cancel_chk[]'][i].checked) {
				checked = true;
				break;
			}
		}
		if (!checked) {
			alert('「報告取消」するデータを選択してくだい。');
			return;
		}
	}

	if (confirm('報告取消します。よろしいですか？')) {
		document.apply.action="fplus_application_list_apply_cancel.php?session=<?=$session?>";
		document.apply.submit();
	}
}


</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

.list {border-collapse:collapse;}
.list td {border:#E6B3D4 solid 1px;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#E6B3D4 solid 0px;}


</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();initcal();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="fplus_menu.php?session=<? echo($session); ?>"><img src="img/icon/b47.gif" width="32" height="32" border="0" alt="ファントルくん＋"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="fplus_menu.php?session=<? echo($session); ?>"><b>ファントルくん＋</b></a> &gt; <a href="fplus_apply_list.php?session=<? echo($session); ?>"><b>報告一覧</b></a></font></td>
<? if ($fplusadm_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="fplusadm_menu.php?session=<? echo($session); ?>&workflow=1"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<?
show_applycation_menuitem($session,$fname,"");
?>

</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#E6B3D4"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>

<form name="apply" action="#" method="post">
<input type="hidden" name="session" value="<? echo($session); ?>">

<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="100%" border="0" cellspacing="1" cellpadding="1" class="list">
<tr>
<td>
<table width="100%" border="0" cellspacing="1" cellpadding="1" class="list">
<tr>
<td width="10%" bgcolor="#fefcdf"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">フォルダ</font></td>
<td width="50%" ><select name="category" onchange="cateOnChange();"><? show_cate_options($arr_wkfwcatemst, $category); ?></select></td>
<td width="10%" bgcolor="#fefcdf"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告書様式</font>
<td width="30%" ><select name="workflow"></select></td>
</tr>
<tr>
<td bgcolor="#fefcdf"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表題</font></td>
<td><input type="text" size="40" maxlength="40" name="apply_title" value="<?=h($apply_title)?>"></td>
<td bgcolor="#fefcdf"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">受付者</font></td>
<td><input type="text" size="30" maxlength="30" name="approve_emp_nm" value="<?=h($approve_emp_nm)?>"></td>
</tr>
<tr>
<td bgcolor="#fefcdf"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告日</font></td>
<td>

<table border="0" cellspacing="0" cellpadding="0" class="block_in">
<tr><td>
<select id="date_y1" name="date_y1"><? show_select_years(10, $date_y1, true); ?></select>/<select id="date_m1" name="date_m1"><? show_select_months($date_m1, true); ?></select>/<select id="date_d1" name="date_d1"><? show_select_days($date_d1, true); ?></select>
</td><td>
	<img src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal1();"/><br>

<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
		<div id="cal1Container" style="position:absolute;display:none;z-index:10000;"></div>
</font>
</td><td>
&nbsp;〜<select id="date_y2" name="date_y2"><? show_select_years(10, $date_y2, true); ?></select>/<select id="date_m2" name="date_m2"><? show_select_months($date_m2, true); ?></select>/<select id="date_d2" name="date_d2"><? show_select_days($date_d2, true); ?></select>

</td><td>
	<img src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal2()"/><br>
<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
		<div id="cal2Container" style="position:absolute;display:none;z-index:10000;"></div>
</font>
</td></tr>
</table>

</td>
<td bgcolor="#fefcdf"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告状況</font></td>
<td>
<select name="apply_stat"><? show_apply_stat_options($apply_stat); ?></select>
</td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="block_in">
<tr align="right"><td>
<input type="button" value="検索" onclick="apply_search();">
</td></tr>
</table>

</td>
</tr>
</table>

<img src="img/spacer.gif" alt="" width="1" height="1">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
<td align="right">
<input type="button" value="再報告" onclick="re_apply()">
<input type="button" value="報告取消" onclick="apply_cancel()">
</td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="list">
<tr bgcolor="#FFFBFF">
<td width="5%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">再報告</font></td>
<td width="4%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告<br>取消</font></td>
<td width="10%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告書番号<br>(管理ＣＤ-連番)</font></td>
<td width="15%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">フォルダ</font></td>
<td width="15%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告書様式</font></td>
<td width="*" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表題</font></td>
<td width="10%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">受付者</font></td>
<td width="7%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">受付<BR>状況</font></td>
<? if($arrise_date_use==1){ ?>
    <td width="8%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発生日</font></td>
<? } ?>
<td width="8%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告日</font></td>
<td width="7%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告<BR>状況</font></td>
</tr>

<?
search_application_list($con, $session, $fname, $category, $workflow, pg_escape_string($apply_title), pg_escape_string($approve_emp_nm), $date_y1, $date_m1, $date_d1, $date_y2, $date_m2, $date_d2, $apply_stat, $page);
?>
</table>

</form>
</td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
</body>
<? pg_close($con); ?>
</html>
<?
// カテゴリ名称を取得
function search_wkfwcatemst($con, $fname) {

	$sql = "select * from fpluscatemst";
	$cond = "where wkfwcate_del_flg = 'f' order by wkfw_type";
	$sel=select_from_table($con,$sql,$cond,$fname);
	if($sel==0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	return $sel;
}

// ワークフロー情報取得
function search_wkfwmst($con, $fname, $wkfw_type) {

	$sql = "select * from fpluswkfwmst";
	$cond="where wkfw_type='$wkfw_type' and wkfw_del_flg = 'f' order by wkfw_id asc";
	$sel=select_from_table($con,$sql,$cond,$fname);
	if($sel==0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	return $sel;
}


// カテゴリオプションを出力
function show_cate_options($arr_cate, $cate) {
	echo("<option value=\"-\">すべて");
	foreach ($arr_cate as $tmp_cate_id => $arr) {
		$tmp_cate_name = $arr["name"];
		echo("<option value=\"$tmp_cate_id\"");
		if($cate != "" && $cate != '-') {
			if ($cate == $tmp_cate_id) {
				echo(" selected");
			}
		}
		echo(">$tmp_cate_name\n");
	}
}


// 申請状況オプションを出力
function show_apply_stat_options($stat) {

	$arr_apply_stat_nm = array("受付待ち","受付済み","不受理","差戻し");
	$arr_apply_stat_id = array("0","1","2","3");

	echo("<option value=\"-\">すべて");
	for($i=0;$i<count($arr_apply_stat_nm);$i++) {

		echo("<option value=\"$arr_apply_stat_id[$i]\"");
		if($stat == $arr_apply_stat_id[$i]) {
			echo(" selected");
		}
		echo(">$arr_apply_stat_nm[$i]\n");
	}
}
?>