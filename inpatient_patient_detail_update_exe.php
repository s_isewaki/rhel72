<?
require_once("about_comedix.php");
require_once("patient_common.ini");
require_once("get_values.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 22, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// 入力チェック
if ($new_pt_id == "") {
	pg_close($con);
	pg_query($con,"rollback");
	echo("<script language=\"javascript\">alert(\"患者IDを入力してください\");</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (preg_match("/[^0-9a-zA-Z]/", $new_pt_id) > 0) {
	pg_close($con);
	pg_query($con,"rollback");
	echo("<script type=\"text/javascript\">alert(\"患者IDに使える文字は半角英数字のみです。\");</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($new_pt_id) > 12) {
	pg_close($con);
	pg_query($con,"rollback");
	echo("<script language=\"javascript\">alert(\"患者IDが長すぎます\");</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strcmp($new_pt_id, $pt_id) != 0) {
	$sql = "select count(*) from ptifmst";
	$cond = "where ptif_id = '$new_pt_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel==0) {
		pg_query($con,"rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (intval(pg_fetch_result($sel, 0, 0)) > 0) {
		pg_query($con,"rollback");
		pg_close($con);
		echo("<script language=\"javascript\">alert(\"指定された患者IDは既に使用されています\");</script>\n");
		echo("<script type=\"text/javascript\">history.back();</script>\n");
		exit;
	}
}
if ($lt_nm == "") {
	pg_close($con);
	pg_query($con,"rollback");
	echo("<script type=\"text/javascript\">alert('姓（漢字）を入力してください。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($lt_nm) > 20) {
	pg_close($con);
	pg_query($con,"rollback");
	echo("<script type=\"text/javascript\">alert('姓（漢字）が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if ($ft_nm == "") {
	pg_close($con);
	pg_query($con,"rollback");
	echo("<script type=\"text/javascript\">alert('名前（漢字）を入力してください。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($ft_nm) > 20) {
	pg_close($con);
	pg_query($con,"rollback");
	echo("<script type=\"text/javascript\">alert('名前（漢字）が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if ($lt_kana_nm == "") {
	pg_close($con);
	pg_query($con,"rollback");
	echo("<script type=\"text/javascript\">alert('姓（かな）を入力してください。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
$lt_kana_nm = mb_convert_kana($lt_kana_nm, "HVc");
if (strlen($lt_kana_nm) > 20) {
	pg_close($con);
	pg_query($con,"rollback");
	echo("<script type=\"text/javascript\">alert('姓（かな）が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if ($ft_kana_nm == "") {
	pg_close($con);
	pg_query($con,"rollback");
	echo("<script type=\"text/javascript\">alert('名前（かな）を入力してください。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
$ft_kana_nm = mb_convert_kana($ft_kana_nm, "HVc");
if (strlen($ft_kana_nm) > 20) {
	pg_close($con);
	pg_query($con,"rollback");
	echo("<script type=\"text/javascript\">alert('名前（かな）が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (!checkdate($birth_mon, $birth_day, $birth_yr)) {
	pg_close($con);
	pg_query($con,"rollback");
	echo("<script type=\"text/javascript\">alert('生年月日が不正です。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
$birth = "$birth_yr$birth_mon$birth_day";
if ($birth > date("Ymd")) {
	pg_close($con);
	pg_query($con,"rollback");
	echo("<script type=\"text/javascript\">alert('生年月日が未来日付です。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($job) > 100) {
	pg_close($con);
	pg_query($con,"rollback");
	echo("<script type=\"text/javascript\">alert('職業が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}

// 入力値の編集
$keywd = get_keyword($lt_kana_nm);

// 患者情報を更新
$sql = "update ptifmst set";
$set = array("ptif_id", "ptif_lt_kana_nm", "ptif_ft_kana_nm", "ptif_lt_kaj_nm", "ptif_ft_kaj_nm", "ptif_keywd", "ptif_birth", "ptif_sex", "ptif_buis");
$setvalue = array($new_pt_id, $lt_kana_nm, $ft_kana_nm, $lt_nm, $ft_nm, $keywd, $birth, $sex, $job);
$cond = "where ptif_id = '$pt_id'";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 入院患者マスタの更新
$tables = array("inptmst", "inptres", "inpthist", "inptcancel");
$set = array("ptif_id", "inpt_lt_kn_nm", "inpt_ft_kn_nm", "inpt_lt_kj_nm", "inpt_ft_kj_nm", "inpt_keywd");
$setvalue = array($new_pt_id, $lt_kana_nm, $ft_kana_nm, $lt_nm, $ft_nm, $keywd);
$cond = "where ptif_id = '$pt_id'";
foreach ($tables as $table) {
	$sql = "update $table set";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// 患者IDを更新
if (strcmp($new_pt_id, $pt_id) != 0) {
	update_patient_id($con, $pt_id, $new_pt_id, $fname);
}

// トランザクションの終了
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 画面をリフレッシュ
$url_key1 = urlencode($key1);
$url_key2 = urlencode($key2);
if ($path == "A" || $path == "B") {
	$url_pt_nm = urlencode($pt_nm);
	echo("<script type=\"text/javascript\">location.href = 'bed_menu_inpatient_patient.php?session=$session&pt_id=$new_pt_id&path=$path&pt_nm=$url_pt_nm&search_sect=$search_sect&search_doc=$search_doc';</script>");
} else {
	echo("<script type=\"text/javascript\">location.href = 'inpatient_patient_detail.php?session=$session&pt_id=$new_pt_id&key1=$url_key1&key2=$url_key2&path=$path&ward=$ward&ptrm=$ptrm&bed=$bed&bedundec1=$bedundec1&bedundec2=$bedundec2&bedundec3=$bedundec3&in_dt=$in_dt&in_tm=$in_tm&ccl_id=$ccl_id&ptwait_id=$ptwait_id';</script>");
}
?>
