<?
require_once("about_comedix.php");
require_once("fplus_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// インシデント権限のチェック
$checkauth = check_authority($session, 78, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

//検索ボタン押下時に１が入る
$serch = @$_REQUEST["search_mode"];

// 指定された日付
if($arisedate == ""){
	$arisedate = @$_REQUEST["arise_date"];
}

$hospital_id = @$_REQUEST["hospital_id"];

//検索ボタン押下後
if($serch == "1"){
	$sql = " SELECT ".
		" 	a.eid_id ".
		" 	, a.report_id ".
		" 	, max(CASE a.grp_code ".
		" 		WHEN 90 THEN CASE a.easy_item_code ".
		" 				WHEN 10 THEN (SELECT  ".
		" 							d.easy_name ".
		" 						FROM ".
		" 							inci_report_materials d ".
		" 						WHERE ".
		" 							d.easy_code = a.input_item ".
		" 							and d.grp_code = a.grp_code ".
		" 							and d.easy_item_code = a.easy_item_code) ".
		" 				ELSE '' ".
		" 			      END ".
		" 		ELSE '' ".
		" 	  END) as input_level ".
		" 	, max(CASE a.grp_code ".
		" 		WHEN 100 THEN CASE a.easy_item_code ".
		" 				WHEN 5 THEN a.input_item ".
		" 				ELSE '' ".
		" 			      END ".
		" 		ELSE '' ".
		" 	  END) as input_arise_date ".
		" 	, max(CASE a.grp_code ".
		" 		WHEN 210 THEN CASE a.easy_item_code ".
		" 				WHEN 20 THEN a.input_item ".
		" 				ELSE '' ".
		" 			      END ".
		" 		ELSE '' ".
		" 	  END) as input_patient_name ".
		" 	, max(CASE a.grp_code ".
		" 		WHEN 210 THEN CASE a.easy_item_code ".
		" 				WHEN 30 THEN a.input_item ".
		" 				ELSE '' ".
		" 			      END ".
		" 		ELSE '' ".
		" 	  END) as input_sex_code ".
		" 	, max(CASE a.grp_code ".
		" 		WHEN 210 THEN CASE a.easy_item_code ".
		" 				WHEN 30 THEN (SELECT  ".
		" 							d.easy_name ".
		" 						FROM ".
		" 							inci_report_materials d ".
		" 						WHERE ".
		" 							d.easy_code = a.input_item ".
		" 							and d.grp_code = a.grp_code ".
		" 							and d.easy_item_code = a.easy_item_code) ".
		" 				ELSE '' ".
		" 			      END ".
		" 		ELSE '' ".
		" 	  END) as input_sex ".
		" 	, max(CASE a.grp_code ".
		" 		WHEN 210 THEN CASE a.easy_item_code ".
		" 				WHEN 40 THEN a.input_item ".
		" 				ELSE '' ".
		" 			      END ".
		" 		ELSE '' ".
		" 	  END) as input_age_code ".
		" 	, max(CASE a.grp_code ".
		" 		WHEN 210 THEN CASE a.easy_item_code ".
		" 				WHEN 40 THEN (SELECT  ".
		" 							d.easy_name ".
		" 						FROM ".
		" 							inci_report_materials d ".
		" 						WHERE ".
		" 							d.easy_code = a.input_item ".
		" 							and d.grp_code = a.grp_code ".
		" 							and d.easy_item_code = a.easy_item_code) ".
		" 				ELSE '' ".
		" 			      END ".
		" 		ELSE '' ".
		" 	  END) as input_age ".
		" 	, max(CASE a.grp_code ".
		" 		WHEN 230 THEN CASE a.easy_item_code ".
		" 				WHEN 60 THEN a.input_item ".
		" 				ELSE '' ".
		" 			      END ".
		" 		ELSE '' ".
		" 	  END) as input_hos_vis_kbn_code ".
		" 	, max(CASE a.grp_code ".
		" 		WHEN 230 THEN CASE a.easy_item_code ".
		" 				WHEN 60 THEN (SELECT  ".
		" 							d.easy_name ".
		" 						FROM ".
		" 							inci_report_materials d ".
		" 						WHERE ".
		" 							d.easy_code = a.input_item ".
		" 							and d.grp_code = a.grp_code ".
		" 							and d.easy_item_code = a.easy_item_code) ".
		" 				ELSE '' ".
		" 			      END ".
		" 		ELSE '' ".
		" 	  END) as input_hos_vis_kbn ".
		" 	, max(CASE a.grp_code ".
		" 		WHEN 250 THEN CASE a.easy_item_code ".
		" 				WHEN 80 THEN a.input_item ".
		" 				ELSE '' ".
		" 			      END ".
		" 		ELSE '' ".
		" 	  END) as input_disease ".
		" 	, max(c.report_title) as title ".
		" 	, max(CASE a.grp_code ".
		" 		WHEN 900 THEN CASE a.easy_item_code ".
		" 				WHEN 10 THEN a.input_item ".
		" 				ELSE '' ".
		" 			      END ".
		" 		ELSE '' ".
		" 	  END) as input_outline_code ".
		" 	, max(CASE a.grp_code ".
		" 		WHEN 900 THEN CASE a.easy_item_code ".
		" 				WHEN 10 THEN (SELECT  ".
		" 							d.super_item_name ".
		" 						FROM ".
		" 							inci_super_item_2010 d ".
		" 						WHERE ".
		" 							d.super_item_id = a.input_item) ".
		" 				ELSE '' ".
		" 			      END ".
		" 		ELSE '' ".
		" 	  END) as input_outline ".
		" 	, max(CASE a.grp_code ".
		" 		WHEN 3100 THEN CASE a.easy_item_code ".
		" 				WHEN 40 THEN a.input_item ".
		" 				ELSE '' ".
		" 			      END ".
		" 		ELSE '' ".
		" 	  END) as input_party_name ".
		" 	, max(CASE a.grp_code ".
		" 		WHEN 520 THEN CASE a.easy_item_code ".
		" 				WHEN 30 THEN a.input_item ".
		" 				ELSE '' ".
		" 			      END ".
		" 		ELSE '' ".
		" 	  END) as input_fact_progress ".
		" 	, max(CASE a.grp_code ".
		" 		WHEN 610 THEN CASE a.easy_item_code ".
		" 				WHEN 20 THEN a.input_item ".
		" 				ELSE '' ".
		" 			      END ".
		" 		ELSE '' ".
		" 	  END) as input_occurrences_factor ".
		" 	, max(CASE a.grp_code ".
		" 		WHEN 3050 THEN CASE a.easy_item_code ".
		" 				WHEN 30 THEN lpad(a.input_item, 2, '0') ".
		" 				ELSE '' ".
		" 			      END ".
		" 		ELSE '' ".
		" 	  END) as input_party_job_code ".
		" 	, max(CASE a.grp_code ".
		" 		WHEN 3050 THEN CASE a.easy_item_code ".
		" 				WHEN 30 THEN (SELECT  ".
		" 							d.easy_name ".
		" 						FROM ".
		" 							inci_report_materials d ".
		" 						WHERE ".
		" 							d.easy_code = a.input_item ".
		" 							and d.grp_code = a.grp_code ".
		" 							and d.easy_item_code = a.easy_item_code) ".
		" 				ELSE '' ".
		" 			      END ".
		" 		ELSE '' ".
		" 	  END) as input_party_job ".
		" 	, max(CASE a.grp_code ".
		" 		WHEN 3250 THEN CASE a.easy_item_code ".
		" 				WHEN 70 THEN a.input_item ".
		" 				ELSE '' ".
		" 			      END ".
		" 		ELSE '' ".
		" 	  END) as input_exp_year_code ".
		" 	, max(CASE a.grp_code ".
		" 		WHEN 3250 THEN CASE a.easy_item_code ".
		" 				WHEN 70 THEN (SELECT  ".
		" 							d.easy_name ".
		" 						FROM ".
		" 							inci_report_materials d ".
		" 						WHERE ".
		" 							d.easy_code = a.input_item ".
		" 							and d.grp_code = a.grp_code ".
		" 							and d.easy_item_code = a.easy_item_code) ".
		" 				ELSE '' ".
		" 			      END ".
		" 		ELSE '' ".
		" 	  END) as input_exp_year ".
		" 	, max(CASE a.grp_code ".
		" 		WHEN 3250 THEN CASE a.easy_item_code ".
		" 				WHEN 80 THEN a.input_item ".
		" 				ELSE '' ".
		" 			      END ".
		" 		ELSE '' ".
		" 	  END) as input_exp_month_code ".
		" 	, max(CASE a.grp_code ".
		" 		WHEN 3250 THEN CASE a.easy_item_code ".
		" 				WHEN 80 THEN (SELECT  ".
		" 							d.easy_name ".
		" 						FROM ".
		" 							inci_report_materials d ".
		" 						WHERE ".
		" 							d.easy_code = a.input_item ".
		" 							and d.grp_code = a.grp_code ".
		" 							and d.easy_item_code = a.easy_item_code) ".
		" 				ELSE '' ".
		" 			      END ".
		" 		ELSE '' ".
		" 	  END) as input_exp_month ".
		" 	, max(CASE a.grp_code ".
		" 		WHEN 3300 THEN CASE a.easy_item_code ".
		" 				WHEN 90 THEN a.input_item ".
		" 				ELSE '' ".
		" 			      END ".
		" 		ELSE '' ".
		" 	  END) as input_emp_year_code ".
		" 	, max(CASE a.grp_code ".
		" 		WHEN 3300 THEN CASE a.easy_item_code ".
		" 				WHEN 90 THEN (SELECT  ".
		" 							d.easy_name ".
		" 						FROM ".
		" 							inci_report_materials d ".
		" 						WHERE ".
		" 							d.easy_code = a.input_item ".
		" 							and d.grp_code = a.grp_code ".
		" 							and d.easy_item_code = a.easy_item_code) ".
		" 				ELSE '' ".
		" 			      END ".
		" 		ELSE '' ".
		" 	  END) as input_emp_year ".
		" 	, max(CASE a.grp_code ".
		" 		WHEN 3300 THEN CASE a.easy_item_code ".
		" 				WHEN 100 THEN a.input_item ".
		" 				ELSE '' ".
		" 			      END ".
		" 		ELSE '' ".
		" 	  END) as input_emp_month_code ".
		" 	, max(CASE a.grp_code ".
		" 		WHEN 3300 THEN CASE a.easy_item_code ".
		" 				WHEN 100 THEN (SELECT  ".
		" 							d.easy_name ".
		" 						FROM ".
		" 							inci_report_materials d ".
		" 						WHERE ".
		" 							d.easy_code = a.input_item ".
		" 							and d.grp_code = a.grp_code ".
		" 							and d.easy_item_code = a.easy_item_code) ".
		" 				ELSE '' ".
		" 			      END ".
		" 		ELSE '' ".
		" 	  END) as input_emp_month ".
		" FROM ".
		" 	((inci_easyinput_data a left join (inci_report c left join fplus_hospital_master hos on c.registrant_class = hos.class_id AND c.registrant_attribute = hos.attribute_id) on a.report_id = c.report_id and a.eid_id = c.eid_id) ".
		" 				left join inci_easyinput_item_mst b on a.grp_code = b.grp_code and a.easy_item_code = b.easy_item_code) ".
		" 	,inci_easyinput_data d ".
		" WHERE ".
		" 	d.grp_code = 100 ".
		" 	AND d.easy_item_code = 5 ".
		" 	AND d.input_item = '$arisedate' ".
		" 	AND a.eid_id = d.eid_id ".
		" 	AND a.eis_id = d.eis_id ".
		" 	AND a.report_id = d.report_id ".
		" 	AND hos.hospital_id = '$hospital_id' ".
		" GROUP BY ".
		" 	a.eid_id ".
		" 	,a.report_id ".
		" ORDER by ".
		" 	a.report_id ".
		" 	,a.eid_id ";

	$sel = @select_from_table($con, $sql, "", $fname);
	$result = pg_fetch_all($sel);
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>CoMedix 検索 | 事故内容</title>
</head>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list1 {border-collapse:collapse;}
.list1 td {border:#E6B3D4 solid 1px; padding:3px; }
</style>
<script type="text/javascript">

<? //検索結果を返す ?>
function setData(rowidx) {
	opener.document.apply.elements['data_occurrences_date'].value = document.getElementById("hdn_arize_date_" + rowidx).value;
	opener.document.apply.elements['data_patient_name'].value = document.getElementById("hdn_patient_name_" + rowidx).value;
	var sex = opener.document.apply.elements['data_patient_sex[]'];
	for(var i = 0; i < sex.length; i++){
		if(document.getElementById("hdn_sex_" + rowidx).value==sex[i].value){
			sex.item(i).checked = true;
		}
	}
	opener.document.apply.elements['data_patient_age'].value = Number(document.getElementById("hdn_age_" + rowidx).value);
	var hos_vis = opener.document.apply.elements['data_patient_class[]'];
	for(var i = 0; i < hos_vis.length; i++){
		if(document.getElementById("hdn_hos_vis_kbn_" + rowidx).value==hos_vis[i].value){
			hos_vis[i].checked = true;
		}
	}
	opener.document.apply.elements['data_main_disease'].value = document.getElementById("hdn_disease_" + rowidx).value;
	opener.document.apply.elements['data_title'].value = document.getElementById("hdn_title_" + rowidx).value;
	var outline = opener.document.apply.elements['data_outline[]'];
	for(var i = 0; i < outline.length; i++){
		if(document.getElementById("hdn_outline_" + rowidx).value==outline[i].value){
			outline[i].checked = true;
		}
	}
	opener.document.apply.elements['data_fact_progress'].value = document.getElementById("hdn_fact_progress_" + rowidx).value;
	opener.document.apply.elements['data_occurrences_factor'].value = document.getElementById("hdn_occurrences_factor_" + rowidx).value;
	opener.document.apply.elements['data_person1'].value = document.getElementById("hdn_party_name_" + rowidx).value;
	opener.document.apply.elements['data_person_job1'].value = document.getElementById("hdn_party_job_" + rowidx).value;
	opener.document.apply.elements['data_experience_year1'].value = Number(document.getElementById("hdn_exp_year_" + rowidx).value);
	opener.document.apply.elements['data_experience_month1'].value = Number(document.getElementById("hdn_exp_month_" + rowidx).value);
	opener.document.apply.elements['data_assign_year1'].value = Number(document.getElementById("hdn_emp_year_" + rowidx).value);
	opener.document.apply.elements['data_assign_month1'].value = Number(document.getElementById("hdn_emp_month_" + rowidx).value);

	self.close();
}

//今日ボタン押下処理
function getDateLabel() {

	currentDateObject = new Date();

	wYear = currentDateObject.getYear();

	currentFullYear = (wYear < 2000) ? wYear + 1900 : wYear;
	currentMonth = to2String(currentDateObject.getMonth() + 1);
	currentDate = to2String(currentDateObject.getDate());

	dateString = currentFullYear + "/" + currentMonth + "/" + currentDate;

	var obj_ymd = document.getElementById("arise_date");
	obj_ymd.value = dateString;
}

function to2String(value) {
	label = "" + value;
	if (label.length < 2) {
		label = "0" + label;
	}
	return label;
}

//カレンダーからの戻り処理を行います。
function call_back_fplus_calendar(caller,ymd)
{
	var obj_ymd = document.getElementById("arise_date");
	obj_ymd.value = ymd;
}

function dateClear(call) {
	var obj_ymd = document.getElementById("arise_date");
	obj_ymd.value = "";
}

function clickselbutton() {
	if(document.getElementById("arise_date").value == "") {
		alert("検索条件を入力してください。");
		return false;
	}else{
		document.sel.action = 'fplus_selhiyari_repo_pop.php';
		document.sel.search_mode.value = "1";
		document.sel.target = "_self";
		document.sel.submit();
		return false;
	}
}

function highlightCells(class_name) {
	changeCellsColor(class_name, '#ffff66');
}

function dehighlightCells(class_name) {
	changeCellsColor(class_name, '');
}

function changeCellsColor(class_name, color) {
	var cells = document.getElementsByTagName('td');
	for (var i = 0, j = cells.length; i < j; i++) {
		if (cells[i].className != class_name) {
			continue;
		}
		cells[i].style.backgroundColor = color;
	}
}

</script>
<style type="text/css">
form {margin:0;}
/*table.list {border-collapse:collapse;}*/
table.list td {border:solid #5279a5 1px;}
</style>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<center>
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
<?
	show_fplus_header_for_sub_window("事故内容検索");
?>
</table>
<form name="sel" action="#" method="post">
	<input type="hidden" name="session" id="session" value="<? echo($session); ?>">
	<input type="hidden" name="search_mode" id="search_mode" value="">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
<tr>
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td  bgcolor="#F5FFE5">
<? //検索条件入力フォーム　ここから-------------------- ?>
<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td><font class="j12"><span>発生年月日</span></font>
			<font class="j12">
					<input type="text" name="arise_date" id="arise_date" size="15" readonly="readonly" value="<?=$arisedate?>" />
					<input type="button" value="今日" onclick="getDateLabel();" />
					<img id="Img4" src="img/calendar_link.gif" style="position:relative;top:3px;z-index:1;cursor:pointer; left: 0px;" alt="カレンダー" onclick="window.open('fplus_calendar.php?session=<? echo($session); ?>&caller=arise_date', 'epinet_arise_calendar', 'width=640,height=480,scrollbars=yes');"/>
					<input type="button" value="クリア" onclick="dateClear('a');" />
					<input id="Button2" type="button" value="検索" style="width:100px;" onclick="clickselbutton();" />
			</font>			
		</td>
	</tr>
</table>
<? //検索条件入力フォーム　ここまで-------------------- ?>
<? //検索結果一覧表示　ここから-------------- ?>
<? //show_calendars($caller,$year, $month); ?>
<table border="0" cellpadding="0" cellspacing="0" class="list1" style="background-color:#fff;">
<input type="hidden" id="hospital_id" name="hospital_id" value="<?=$hospital_id?>"/>
	<tr  style="background-color:#ffdfff;">
		<td rowspan="2" align="center" style="width:80px;"><font class="j12"><span>発生年月日</span></font></td>
		<td rowspan="2" align="center" style="width:150px;"><font class="j12"><span>表題</span></font></td>
		<td rowspan="2" align="center" style="width:90px;"><font class="j12"><span>概要</span></font></td>
		<td colspan="5" align="center" style="width:300px;"><font class="j12"><span>対象患者</span></font></td>
		<td colspan="4" align="center" style="width:300px;"><font class="j12"><span>医療関係者</span></font></td>
	</tr>
	<tr  style="background-color:#ffdfff;">
		<td align="center" style="width:80px;"><font class="j12"><span>氏名</span></font></td>
		<td align="center" style="width:40px;"><font class="j12"><span>性別</span></font></td>
		<td align="center" style="width:40px;"><font class="j12"><span>年齢</span></font></td>
		<td align="center" style="width:50px;"><font class="j12"><span>入外別</span></font></td>
		<td align="center" style="width:90px;"><font class="j12"><span>主たる病名</span></font></td>
		<td align="center" style="width:75px;"><font class="j12"><span>氏名</span></font></td>
		<td align="center" style="width:75px;"><font class="j12"><span>職種</span></font></td>
		<td align="center" style="width:75px;"><font class="j12"><span>経験年数</span></font></td>
		<td align="center" style="width:75px;"><font class="j12"><span>配属年数</span></font></td>
	</tr>
	<? if($serch=="1"){ ?>
		<? if($result[0]["report_id"]==""){ ?>
		<? //検索結果が0件 ?>
			<tr>
				<td colspan="12" align="center">データが存在しません</td>
			</tr>
		<? }else{ ?>
		<? //検索結果が1件以上 ?>
			<? $count=0 ?>
			<? foreach($result as $idx => $row){ ?>
			<tr>
				<td class=<?="\"apply_list_".$count."\"" ?> onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="setData(<?=$count?>);" align="center"><font class="j12"><?=$row["input_arise_date"]?></font></td>
				<td class=<?="\"apply_list_".$count."\"" ?> onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="setData(<?=$count?>);" align="left"><font class="j12"><?=$row["title"]?></font></td>
				<td class=<?="\"apply_list_".$count."\"" ?> onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="setData(<?=$count?>);" align="center"><font class="j12"><?=$row["input_outline"]?></font></td>
				<td class=<?="\"apply_list_".$count."\"" ?> onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="setData(<?=$count?>);" align="left"><font class="j12"><?=$row["input_patient_name"]?></font></td>
				<td class=<?="\"apply_list_".$count."\"" ?> onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="setData(<?=$count?>);" align="center"><font class="j12"><?=$row["input_sex"]?></font></td>
				<td class=<?="\"apply_list_".$count."\"" ?> onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="setData(<?=$count?>);" align="center"><font class="j12"><?=$row["input_age"]?></font></td>
				<td class=<?="\"apply_list_".$count."\"" ?> onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="setData(<?=$count?>);" align="center"><font class="j12"><?=$row["input_hos_vis_kbn"]?></font></td>
				<td class=<?="\"apply_list_".$count."\"" ?> onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="setData(<?=$count?>);" align="left"><font class="j12"><?=$row["input_disease"]?></font></td>
				<td class=<?="\"apply_list_".$count."\"" ?> onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="setData(<?=$count?>);" align="left"><font class="j12"><?=$row["input_party_name"]?></font></td>
				<td class=<?="\"apply_list_".$count."\"" ?> onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="setData(<?=$count?>);" align="left"><font class="j12"><?=$row["input_party_job"]?></font></td>
				<td class=<?="\"apply_list_".$count."\"" ?> onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="setData(<?=$count?>);" align="left"><font class="j12"><?=$row["input_exp_year"]." ".$row["input_exp_month"]?></font></td>
				<td class=<?="\"apply_list_".$count."\"" ?> onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="setData(<?=$count?>);" align="left"><font class="j12"><?=$row["input_emp_year"]." ".$row["input_emp_month"]?></font></td>
				<? //hiddenにデータ格納 ?>
				<input type="hidden" id="hdn_arize_date_<?=$count?>" value="<?=$row["input_arise_date"]?>" />
				<input type="hidden" id="hdn_title_<?=$count?>" value="<?=$row["title"]?>" />
				<input type="hidden" id="hdn_outline_<?=$count?>" value="<?=$row["input_outline_code"]?>" />
				<input type="hidden" id="hdn_patient_name_<?=$count?>" value="<?=$row["input_patient_name"]?>" />
				<input type="hidden" id="hdn_sex_<?=$count?>" value="<?=$row["input_sex_code"]?>" />
				<input type="hidden" id="hdn_age_<?=$count?>" value="<?=$row["input_age_code"]?>" />
				<input type="hidden" id="hdn_hos_vis_kbn_<?=$count?>" value="<?=$row["input_hos_vis_kbn_code"]?>" />
				<input type="hidden" id="hdn_disease_<?=$count?>" value="<?=$row["input_disease"]?>" />
				<input type="hidden" id="hdn_fact_progress_<?=$count?>" value="<?=$row["input_fact_progress"]?>" />
				<input type="hidden" id="hdn_occurrences_factor_<?=$count?>" value="<?=$row["input_occurrences_factor"]?>" />
				<input type="hidden" id="hdn_party_name_<?=$count?>" value="<?=$row["input_party_name"]?>" />
				<input type="hidden" id="hdn_party_job_<?=$count?>" value="<?=$row["input_party_job_code"]?>" />
				<input type="hidden" id="hdn_exp_year_<?=$count?>" value="<?=$row["input_exp_year_code"]?>" />
				<input type="hidden" id="hdn_exp_month_<?=$count?>" value="<?=$row["input_exp_month_code"]?>" />
				<input type="hidden" id="hdn_emp_year_<?=$count?>" value="<?=$row["input_emp_year_code"]?>" />
				<input type="hidden" id="hdn_emp_month_<?=$count?>" value="<?=$row["input_emp_month_code"]?>" />
			</tr>
			<? $count++; ?>
			<?}?>
		<? } ?>
	<? } ?>
</table>
<? //検索結果一覧表示　ここまで-------------- ?>
</td>
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>
</form>
</center>
</body>
<? pg_close($con); ?>
</html>
