<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" method="post" action="inpatient_move_reserve_register.php">
<input type="hidden" name="back" value="t">
<input type="hidden" name="enti" value="<? echo($enti); ?>">
<input type="hidden" name="ward" value="<? echo($ward); ?>">
<input type="hidden" name="ptrm" value="<? echo($ptrm); ?>">
<input type="hidden" name="bed" value="<? echo($bed); ?>">
<input type="hidden" name="mv_yr" value="<? echo($mv_yr); ?>">
<input type="hidden" name="mv_mon" value="<? echo($mv_mon); ?>">
<input type="hidden" name="mv_day" value="<? echo($mv_day); ?>">
<input type="hidden" name="mv_hr" value="<? echo($mv_hr); ?>">
<input type="hidden" name="mv_min" value="<? echo($mv_min); ?>">
<input type="hidden" name="sect" value="<? echo($sect); ?>">
<input type="hidden" name="doc" value="<? echo($doc); ?>">
<input type="hidden" name="nurse" value="<? echo($nurse); ?>">
<input type="hidden" name="reason" value="<? echo($reason); ?>">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="pt_id" value="<? echo($pt_id); ?>">
<input type="hidden" name="direct" value="<? echo($direct); ?>">
</form>
<?
require("about_session.php");
require("about_authority.php");
require("inpatient_common.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// チェック情報を取得
$sql = "select required1 from bedcheck";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$required1 = pg_fetch_result($sel, 0, "required1");

// 入力チェック
if (!checkdate($mv_mon, $mv_day, $mv_yr)) {
	echo("<script type=\"text/javascript\">alert('日付が不正です。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}
if ($required1 == "t" && ($nurse == "" || $nurse == 0)) {
	echo("<script type=\"text/javascript\">alert('担当看護師を選択してください。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

// 登録値の編集
$date = "$mv_yr$mv_mon$mv_day";
$time = sprintf("%02d%02d", $mv_hr, $mv_min);
list($bldg_cd, $ward_cd) = split("-", $ward);

// トランザクションを開始
pg_query($con, "begin");

// 関連チェック
$sql = "select move_dt, move_tm from inptmove";
$cond = "where ptif_id = '$pt_id' and move_del_flg = 'f' and (move_dt > '$date' or (move_dt = '$date' and move_tm >= '$time')) order by move_dt desc, move_tm desc";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if (pg_num_rows($sel) > 0) {
	pg_query($con, "rollback");
	pg_close($con);
	$selected_move_date = preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $date);
	$selected_move_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $time);
	$last_move_date = preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", pg_fetch_result($sel, 0, "move_dt"));
	$last_move_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", pg_fetch_result($sel, 0, "move_tm"));
	echo("<script type=\"text/javascript\">alert('指定の転床日時（{$selected_move_date} {$selected_move_time}）が\\n最新の転床日時（{$last_move_date} {$last_move_time}）を越えないため、登録できません。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

// 入院状況レコードを更新
update_inpatient_condition($con, $pt_id, $date, $time, "3", $session, $fname, true);

// 入院情報を取得
$sql = "select * from inptmst";
$cond = "where ptif_id = '$pt_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$from_enti_id = pg_fetch_result($sel, 0, "inpt_enti_id");
$from_bldg_cd = pg_fetch_result($sel, 0, "bldg_cd");
$from_ward_cd = pg_fetch_result($sel, 0, "ward_cd");
$from_ptrm_room_no = pg_fetch_result($sel, 0, "ptrm_room_no");
$from_bed_no = pg_fetch_result($sel, 0, "inpt_bed_no");
$from_sect_id = pg_fetch_result($sel, 0, "inpt_sect_id");
$from_dr_id = pg_fetch_result($sel, 0, "dr_id");
$from_nurse_id = pg_fetch_result($sel, 0, "nurse_id");

// 移転情報を作成
$sql = "insert into inptmove (ptif_id, from_enti_id, from_bldg_cd, from_ward_cd, from_ptrm_room_no, from_bed_no, from_sect_id, from_dr_id, from_nurse_id, to_enti_id, to_bldg_cd, to_ward_cd, to_ptrm_room_no, to_bed_no, to_sect_id, to_dr_id, to_nurse_id, move_dt, move_tm, move_reason, move_cfm_flg, move_del_flg, updated) values (";
$content = array($pt_id, $from_enti_id, $from_bldg_cd, $from_ward_cd, $from_ptrm_room_no, $from_bed_no, $from_sect_id, $from_dr_id, $from_nurse_id, $enti, $bldg_cd, $ward_cd, $ptrm, $bed, $sect, $doc, $nurse, $date, $time, $reason, "f", "f", date("YmdHis"));
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 親画面をリフレッシュし、自画面を閉じる
echo "<script type=\"text/javascript\">opener.location.reload(); self.close();</script>";
?>
</body>
