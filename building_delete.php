<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_authority.php");
require("about_session.php");
require("about_postgres.php");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 病棟登録権限チェック
$ward = check_authority($session,21,$fname);
if($ward == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションの開始
pg_query($con, "begin transaction");

// チェックされた棟をループ
if (is_array($del_bldg)) {
	foreach ($del_bldg as $bldg_cd) {

		// 棟名の取得
		$sql = "select bldg_name from bldgmst";
		$cond = "where bldg_cd = '$bldg_cd'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$bldg_name = pg_fetch_result($sel, 0, "bldg_name");

		// 指定棟に属する病棟の数を取得
		$sql = "select count(*) from wdmst";
		$cond = "where bldg_cd = '$bldg_cd' and ward_del_flg = 'f'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$ward_count = pg_fetch_result($sel, 0, 0);

		// 病棟が存在する場合はエラーとする
		if ($ward_count > 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script language=\"javascript\">alert('{$bldg_name}は病棟が登録されているため削除できません。');</script>");
			echo("<script language=\"javascript\">history.back();</script>");
			exit;
		}

		// 棟情報の論理削除
		$sql = "update bldgmst set";
		$set = array("bldg_del_flg");
		$setvalue = array("t");
		$cond = "where bldg_cd = '$bldg_cd'";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}
}


// トランザクションのコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 棟一覧画面を再表示
	echo("<script language=\"javascript\">location.href = 'building_list.php?session=$session'</script>");
?>
