<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>経営支援｜トップ</title>
<?
require("about_authority.php");
require("about_session.php");
require("show_calc_bed_by_type.ini");
require("get_values.ini");
require("get_ward_div.ini");
require("label_by_profile_type.ini");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 経営支援権限チェック
$auth = check_authority($session,25,$fname);
if($auth == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 算出期間の設定
$y = date("Y");
$m = date("m");
$d = date("d");
$from_ym = date("Ym", strtotime("-2 month", get_time("{$y}{$m}15")));
$from_date = "{$from_ym}01";
$to_ym = "$y$m";
$to_date = "$to_ym$d";

//DBコネクションの作成
$con = connect2db($fname);

// 表示設定値の取得
$emp_id = get_emp_id($con, $session, $fname);
$sql = "select * from option";
$cond = "where emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$rate_visible = (pg_fetch_result($sel, 0, "biz_bed_rate") == "t");
$avg_rate_visible = (pg_fetch_result($sel, 0, "biz_bed_avg_rate") == "t");
$active_rate_visible = (pg_fetch_result($sel, 0, "biz_bed_actv_rate") == "t");
$avg_days_visible = (pg_fetch_result($sel, 0, "biz_bed_avg_days") == "t");
$simulation_visible = (pg_fetch_result($sel, 0, "biz_bed_sml_days") == "t");

// 病床利用率の取得
if ($rate_visible) {
	$arr_rate = get_arr_bed_rate($con, $fname);
}

// 平均在院日数目標値・シミュレーション結果の取得
if ($simulation_visible) {
	$sql = "select * from bedconf";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) == 1) {
		for ($i = 1; $i <= 5; $i++) {
			$var_name = "target$i";
			$$var_name = pg_fetch_result($sel, 0, "target_days$i");
			if ($$var_name == 0) {
				$$var_name = "";
			}
		}
	}

	$target_date = date("Ymd", strtotime("+7 day"));
	$arr_avg_in_days = get_arr_avg_in_days_future($con, $target_date, $fname);
}

// 病棟区分名を取得
$arr_ward_div = get_ward_div($con, $fname);
// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 病床/居室
$bed_title = $_label_by_profile["BED"][$profile_type];

// 病棟/施設
$ward_title = $_label_by_profile["WARD"][$profile_type];

// 在院/在所
$stay_hospital_title = $_label_by_profile["STAY_HOSPITAL"][$profile_type];

?>
<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.block {border-collapse:collapse;}
table.block td {border:#5279a5 solid 1px;}
table.block td td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="biz_menu.php?session=<? echo($session); ?>"><img src="img/icon/b18.gif" width="32" height="32" border="0" alt="経営支援"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="biz_menu.php?session=<? echo($session); ?>"><b>経営支援</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="120" height="22" align="center" bgcolor="#5279a5"><a href="./biz_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>経営支援</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="biz_option.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示設定</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<? if ($rate_visible) { ?>
<!-- 病床利用率 -->
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
<td height="22"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b><?=$bed_title?>利用率</b></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="block">
<tr bgcolor="#f6f9ff">
<td width="17%" height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["1"]); ?></font></td>
<td width="17%" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["2"]); ?></font></td>
<td width="17%" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["3"]); ?></font></td>
<td width="17%" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["4"]); ?></font></td>
<td width="17%" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["5"]); ?></font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">全体</font></td>
</tr>
<tr>
<td height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_rate["1"]["rate"]); ?>%</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_rate["2"]["rate"]); ?>%</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_rate["3"]["rate"]); ?>%</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_rate["4"]["rate"]); ?>%</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_rate["5"]["rate"]); ?>%</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_rate["ALL"]["rate"]); ?>%</font></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<? } ?>

<?
// 平均病床利用率（過去3か月）
if ($avg_rate_visible) {
	show_avg_bed_rate($con, $from_date, $to_date, $arr_ward_div, $session, $fname);
}

// 病床稼動率（過去3か月）
if ($active_rate_visible) {
	show_bed_active_rate($con, $from_date, $to_date, $arr_ward_div, $session, $fname);
}

// 平均在院日数（過去3か月）
if ($avg_days_visible) {
	show_avg_in_days($con, $from_ym, $to_ym, $arr_ward_div, $session, $fname);
}
?>

<? if ($simulation_visible) { ?>
<!-- 平均在院日数（シミュレーション） -->
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
<td height="22"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>平均<?=$stay_hospital_title?>日数（シミュレーション）</b></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="block">
<tr bgcolor="#f6f9ff">
<td width="25%" height="22" align="right">&nbsp;</td>
<td width="25%" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">目標値</font></td>
<td width="25%" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">1週間後の予想平均<?=$stay_hospital_title?>日数</font></td>
<td width="25%" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">差異</font></td>
</tr>
<tr>
<td height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["1"]); ?></font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if (strlen($target1) > 0) {echo("{$target1}日");} ?></font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_avg_in_days["1"]); ?>日</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(calc_diff($target1, $arr_avg_in_days["1"])); ?></font></td>
</tr>
<tr>
<td height="22" align="right" ><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["2"]); ?></font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if (strlen($target2) > 0) {echo("{$target2}日");} ?></font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_avg_in_days["2"]); ?>日</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(calc_diff($target2, $arr_avg_in_days["2"])); ?></font></td>
</tr>
<tr>
<td height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["3"]); ?></font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if (strlen($target3) > 0) {echo("{$target3}日");} ?></font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_avg_in_days["3"]); ?>日</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(calc_diff($target3, $arr_avg_in_days["3"])); ?></font></td>
</tr>
<tr>
<td height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["4"]); ?></font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if (strlen($target4) > 0) {echo("{$target4}日");} ?></font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_avg_in_days["4"]); ?>日</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(calc_diff($target4, $arr_avg_in_days["4"])); ?></font></td>
</tr>
<tr>
<td height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["5"]); ?></font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if (strlen($target5) > 0) {echo("{$target5}日");} ?></font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_avg_in_days["5"]); ?>日</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(calc_diff($target5, $arr_avg_in_days["5"])); ?></font></td>
</tr>
<tr>
<td height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">全体</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">-</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_avg_in_days["ALL"]); ?>日</font></td>
<td></td>
</tr>
</table>
<? } ?>

<?
// 表示項目なし
if (!$rate_visible && !$avg_rate_visible && !$active_rate_visible && !$avg_days_visible && !$simulation_visible) {
	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\n");
	echo("<tr>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">すべての表示項目が「表示しない」に設定されています。設定は表示設定画面で変更できます。</font></td>\n");
	echo("</tr>\n");
	echo("</table>\n");
}
?>

</body>
<? pg_close($con); ?>
</html>
<?
//==============================================================================
// 関数
//==============================================================================

//------------------------------------------------------------------------------
// 平均病床利用率の出力
//------------------------------------------------------------------------------
function show_avg_bed_rate($con, $from_date, $to_date, $arr_ward_div, $session, $fname) {
	global $bed_title;
	
	$arr_rate = get_arr_avg_bed_rate($con, $from_date, $to_date, $fname);
?>
<!-- 平均病床利用率（過去3か月） -->
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
<td height="22"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>平均<?=$bed_title?>利用率（過去3か月）</b></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="block">
<tr bgcolor="#f6f9ff">
<td height="22">&nbsp;</td>
<td width="14%" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["1"]); ?></font></td>
<td width="14%" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["2"]); ?></font></td>
<td width="14%" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["3"]); ?></font></td>
<td width="14%" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["4"]); ?></font></td>
<td width="14%" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["5"]); ?></font></td>
<td width="14%" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">全体</font></td>
</tr>
<?
	for ($i = 0; $i < count($arr_rate); $i++) {
		$tmp_title = ($i == count($arr_rate) - 1) ? "3か月平均" : $arr_rate[$i]["title"];
?>
<tr>
<td height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_title); ?></font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_rate[$i]["1"]); ?>%</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_rate[$i]["2"]); ?>%</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_rate[$i]["3"]); ?>%</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_rate[$i]["4"]); ?>%</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_rate[$i]["5"]); ?>%</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_rate[$i]["ALL"]); ?>%</font></td>
</tr>
<?
	}
?>
</table>
<img src="./img/spacer.gif" width="1" height="5"><br>
<?
}

//------------------------------------------------------------------------------
// 病床稼動率の出力
//------------------------------------------------------------------------------
function show_bed_active_rate($con, $from_date, $to_date, $arr_ward_div, $session, $fname) {
	global $bed_title;

	$arr_rate = get_arr_bed_active_rate($con, $from_date, $to_date, $fname);
?>
<!-- 病床稼動率（過去3か月） -->
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
<td height="22"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b><?=$bed_title?>稼動率（過去3か月）</b></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="block">
<tr bgcolor="#f6f9ff">
<td height="22">&nbsp;</td>
<td width="14%" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["1"]); ?></font></td>
<td width="14%" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["2"]); ?></font></td>
<td width="14%" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["3"]); ?></font></td>
<td width="14%" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["4"]); ?></font></td>
<td width="14%" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["5"]); ?></font></td>
<td width="14%" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">全体</font></td>
</tr>
<?
	for ($i = 0; $i < count($arr_rate); $i++) {
		$tmp_title = ($i == count($arr_rate) - 1) ? "3か月平均" : $arr_rate[$i]["title"];
?>
<tr>
<td height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_rate[$i]["title"]); ?></font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_rate[$i]["1"]); ?>%</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_rate[$i]["2"]); ?>%</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_rate[$i]["3"]); ?>%</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_rate[$i]["4"]); ?>%</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_rate[$i]["5"]); ?>%</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_rate[$i]["ALL"]); ?>%</font></td>
</tr>
<?
	}
?>
</table>
<img src="./img/spacer.gif" width="1" height="5"><br>
<?
}

//------------------------------------------------------------------------------
// 平均在院日数の出力
//------------------------------------------------------------------------------
function show_avg_in_days($con, $from_ym, $to_ym, $arr_ward_div, $session, $fname) {
	global $stay_hospital_title;

	$arr_days = get_arr_avg_in_days($con, $from_ym, $to_ym, $fname);
?>
<!-- 平均在院日数（過去3か月） -->
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
<td height="22"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>平均<?=$stay_hospital_title?>日数（過去3か月）</b></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="block">
<tr bgcolor="#f6f9ff">
<td height="22">&nbsp;</td>
<td width="14%" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["1"]); ?></font></td>
<td width="14%" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["2"]); ?></font></td>
<td width="14%" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["3"]); ?></font></td>
<td width="14%" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["4"]); ?></font></td>
<td width="14%" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["5"]); ?></font></td>
<td width="14%" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">全体</font></td>
</tr>
<?
	for ($i = 0; $i < count($arr_days); $i++) {
?>
<tr>
<td height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_days[$i]["title"]); ?></font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_days[$i]["1"]); ?>日</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_days[$i]["2"]); ?>日</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_days[$i]["3"]); ?>日</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_days[$i]["4"]); ?>日</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_days[$i]["5"]); ?>日</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_days[$i]["ALL"]); ?>日</font></td>
</tr>
<?
	}
?>
</table>
</body>
</html>
<img src="./img/spacer.gif" width="1" height="5"><br>
<?
}

//------------------------------------------------------------------------------
// 平均在院日数シミュレーション
//------------------------------------------------------------------------------
function get_arr_avg_in_days_future($con, $date, $fname) {

	// 直近3か月の平均病床利用率を取得
	$to_date_rate = date("Ymd");
	$from_date_rate = get_3_month_ago($to_date_rate);
	$arr_avg_bed_rate = get_arr_avg_bed_rate($con, $from_date_rate, $to_date_rate, $fname);
	$arr_avg_bed_rate = $arr_avg_bed_rate[4];  // 通期のみ使用

	// 病床数の取得
	$arr_bed_count = array();
	for ($i = 1; $i <= 5; $i++) {
		$arr_bed_count[$i] = get_bed_count($con, $i, $fname);
	}
	$arr_bed_count["ALL"] = get_bed_count($con, "", $fname);

	// 算出期間内日数の取得（指定日−指定日の3か月前）
	$to_date_diff = $date;
	$from_date_diff = get_3_month_ago($date);
	$days = calc_days($from_date_diff, $to_date_diff);

	// 在院患者延日数の算出
	$arr_sum_in_days = array();
	for ($i = 1; $i <= 5; $i++) {
		$arr_sum_in_days[$i] = calc_sum_in_days($arr_bed_count[$i], $arr_avg_bed_rate[$i], $days);
	}
	$arr_sum_in_days["ALL"] = calc_sum_in_days($arr_bed_count["ALL"], $arr_avg_bed_rate["ALL"], $days);

	// 新入棟患者数・新退棟患者数の取得（指定日の3か月前〜システム日付）
	$from_date_pt = $from_date_diff;
	$to_date_pt = $to_date_rate;
	$arr_in_ward_count = get_arr_in_ward_count($con, $from_date_pt, $to_date_pt, true, $fname);
	$arr_out_ward_count = get_arr_out_ward_count($con, $from_date_pt, $to_date_pt, true, $fname);

	// 1週間の平均入棟患者数・平均退棟患者数を算出
	$sql = "select * from bedconf";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$arr_avg_in_ward_count = array();
	$arr_avg_out_ward_count = array();
	if (pg_num_rows($sel) == 1) {
		for ($i = 1; $i <= 5; $i++) {
			$arr_avg_in_ward_count[$i] += pg_fetch_result($sel, 0, "in_weekday$i") * 5;
			$arr_avg_in_ward_count[$i] += pg_fetch_result($sel, 0, "in_saturday$i");
			$arr_avg_in_ward_count[$i] += pg_fetch_result($sel, 0, "in_sunday$i");
			$arr_avg_in_ward_count["ALL"] += $arr_avg_in_ward_count[$i];
			$arr_avg_out_ward_count[$i] += pg_fetch_result($sel, 0, "out_weekday$i") * 5;
			$arr_avg_out_ward_count[$i] += pg_fetch_result($sel, 0, "out_saturday$i");
			$arr_avg_out_ward_count[$i] += pg_fetch_result($sel, 0, "out_sunday$i");
			$arr_avg_out_ward_count["ALL"] += $arr_avg_out_ward_count[$i];
		}
	}

	// 直近3か月の新入棟患者数・新退棟患者数を算出（上記の合計）
	for ($i = 1; $i <= 5; $i++) {
		$arr_in_ward_count[$i] += $arr_avg_in_ward_count[$i];
		$arr_out_ward_count[$i] += $arr_avg_out_ward_count[$i];
	}
	$arr_in_ward_count["ALL"] += $arr_avg_in_ward_count["ALL"];
	$arr_out_ward_count["ALL"] += $arr_avg_out_ward_count["ALL"];

	// 平均在院日数の算出
	$arr_avg_in_days = array();
	for ($i = 1; $i <= 5; $i++) {
		$arr_avg_in_days[$i] = calc_avg_in_days($arr_sum_in_days[$i], $arr_in_ward_count[$i], $arr_out_ward_count[$i]);
	}
	$arr_avg_in_days["ALL"] = calc_avg_in_days($arr_sum_in_days["ALL"], $arr_in_ward_count["ALL"], $arr_out_ward_count["ALL"]);

	return $arr_avg_in_days;

}

//------------------------------------------------------------------------------
// 平均在院日数目標値とシミュレーション結果の差異
//------------------------------------------------------------------------------
function calc_diff($target, $expectation) {
	if (strlen($target) == 0) {
		return;
	}

	$diff = $expectation - $target;

	if ($diff > 0) {
		return "<font color='red'>+{$diff}日</font>";
	} else {
		return "{$diff}日";
	}
}
?>
