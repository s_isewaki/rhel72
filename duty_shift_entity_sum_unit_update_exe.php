<!-- ************************************************************************ -->
<!-- 勤務シフト集計単位変更「更新」 -->
<!-- ************************************************************************ -->

<?
require_once("about_session.php");
require_once("about_authority.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
// セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
///-----------------------------------------------------------------------------
// トランザクションを開始
///-----------------------------------------------------------------------------
pg_query($con, "begin transaction");
///-----------------------------------------------------------------------------
// 入力データ収集、ＤＢ更新。ＤＢは一旦DELETEしてからINSERT
///-----------------------------------------------------------------------------
// 単位名

$sum_sw = "summary_style";
$sum_sw = $$sum_sw;
if ( $sum_sw =="" ){
	$sum_sw_status = 0;
}else{
	$sum_sw_status = 1;
}

// 「集計方法変更を有効にする」チェックボックスのスイッチ
$sql = "DELETE FROM duty_shift_entity_sum_change_sw ";
$cond = "WHERE group_id = '$group_id'";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

$sql = "INSERT INTO duty_shift_entity_sum_change_sw (group_id,sum_change_sw) VALUES (";
$content = array($group_id , $sum_sw_status);
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 集計単位名
$sql = "DELETE FROM duty_shift_entity_sum_unit_name ";
$cond = "WHERE group_id = '$group_id'";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

$wk = array();
for($c=1; $c<=13;$c++) {
	$unit_name = "unit_name_$c";
	$unit_name = $$unit_name;
	$sql = "INSERT INTO duty_shift_entity_sum_unit_name (sum_unit_name_id , group_id , sum_unit_name) VALUES (";
	$content = array($c , $group_id , $unit_name);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// 集計変更単位値
$sql = "DELETE FROM duty_shift_entity_sum_unit_value ";
$cond = "WHERE group_id = '$group_id'";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// NULL値は0(zero)へ置き換える
for($r=0; $r<$data_cnt; $r++){
	$atdptn_id = "atdptn_ptn_id_$r";
	$atdptn_id = $$atdptn_id ;
	$font_name = "font_name_$r";
	$font_name = $$font_name;
	$reason = "reason_$r";
	$reason = $$reason ;
	if ($reason==""){$reason=0;}
	for($c=1; $c<=13; $c++) {
		$unit_value = "unit_value_".$r."_".$c;
		$unit_value = $$unit_value;
//		$sql = "INSERT INTO duty_shift_entity_sum_unit_value (group_id , atdptn_id , reason , sum_unit_name_id , sum_unit_value) VALUES (";
		$sql = "INSERT INTO duty_shift_entity_sum_unit_value (group_id , font_name , reason , sum_unit_name_id , sum_unit_value) VALUES (";
		$content = array($group_id , $font_name , $reason , $c , $unit_value);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
}

///-----------------------------------------------------------------------------
// トランザクションをコミット
///-----------------------------------------------------------------------------
pg_query("commit");
///-----------------------------------------------------------------------------
// データベース接続を閉じる
///-----------------------------------------------------------------------------
pg_close($con);
///-----------------------------------------------------------------------------
// 遷移元画面を再表示
///-----------------------------------------------------------------------------
echo("<script type=\"text/javascript\">location.href = 'duty_shift_entity_sum_unit.php?session=$session&pattern_id=$pattern_id';</script>");
?>

