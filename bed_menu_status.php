<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>病床管理｜病棟状況一覧表</title>
<?
require_once("about_authority.php");
require_once("about_session.php");
require_once("show_select_values.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 診療科登録権限を取得
$section_admin_auth = check_authority($session, 28, $fname);

// 病棟登録権限を取得
$ward_admin_auth = check_authority($session, 21, $fname);

// データベースに接続
$con = connect2db($fname);

// 棟一覧の取得
$sql = "select bldg_cd, bldg_name from bldgmst";
$cond = "where bldg_del_flg = 'f' order by bldg_cd";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 棟一覧を配列に格納
$buildings = array();
while ($row = pg_fetch_array($sel)) {
	$buildings[$row["bldg_cd"]] = $row["bldg_name"];
}

// 掲示板に当日の書き込みがあるかどうかチェック
$sql = "select max(date) from bedbbs";
$cond = "where bbs_del_flg = 'f'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if (pg_num_rows($sel) > 0) {
	$bbs_show_flg = (substr(pg_fetch_result($sel, 0, 0), 0, 8) == date("Ymd"));
} else {
	$bbs_show_flg = false;
}

// 日時の初期値を設定
if ($year == "") {$year = date("Y");}
if ($month == "") {$month = date("m");}
if ($day == "") {$day = date("d");}
if ($hour == "") {$hour = date("H");}
if ($minute == "") {$minute = (date("i") <= 29) ? 0 : 30;}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function showBBS() {
	var left = screen.availWidth - 650;
	window.open('bed_menu_status_bbs.php?session=<? echo($session); ?>&show_content=t', 'bbs', 'width=640,height=600,scrollbars=yes,left=' + left);
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
form {margin:0;}
table.submenu {border-collapse:collapse;}
table.submenu td {border:#5279a5 solid 1px;}
table.list {border-collapse:collapse;}
table.list td {border:solid #5279a5 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"<? if ($bbs_show_flg && $bldg_cd != "") {echo(" onload=\"showBBS();\"");} ?>>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="bed_menu.php?session=<? echo($session); ?>"><img src="img/icon/b17.gif" width="32" height="32" border="0" alt="病床管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="bed_menu.php?session=<? echo($session); ?>"><b>病床管理</b></a></font></td>
<? if ($section_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="entity_menu.php?session=<? echo($session); ?>&entity=1"><b>管理画面へ</b></a></font></td>
<? } else if ($ward_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="building_list.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="115" align="center" bgcolor="#5279a5"><a href="bed_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>病床管理トップ</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="ward_reference.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病棟照会</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="inpatient_reserve_register_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入退院登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="bedmake_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ベットメイク</font></a></td>
<td width="5">&nbsp;</td>
<td width="125" align="center" bgcolor="#bdd1e7"><a href="bed_rate_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床利用率管理</font></a>
</td>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#bdd1e7"><a href="bed_date_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">在院日数管理</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="bed_patient_export.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者データ抽出</font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="submenu">
<tr height="22">
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu.php?session=<? echo($session); ?>">お知らせ</a></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病棟状況一覧表</font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_schedule.php?session=<? echo($session); ?>">入退院カレンダー</a></font></td>
<td width="17%" align="center"><a href="bed_menu_inpatient_search.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院患者検索</font></a></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_vacant_search.php?session=<? echo($session); ?>">空床検索</a></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_occupation.php?session=<? echo($session); ?>">病床利用状況</a></font></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<!-- 病棟状況一覧表 -->
<form name="mainform" action="bed_menu_status.php" method="get">
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22" bgcolor="#f6f9ff">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
棟（事業所）：<select name="bldg_cd">
<?
foreach ($buildings as $tmp_bldg_cd => $tmp_bldg_name) {
	echo("<option value=\"$tmp_bldg_cd\"");
	if ($tmp_bldg_cd == $bldg_cd) {
		echo(" selected");
	}
	echo(">$tmp_bldg_name\n");
}
?>
</select>
<img src="img/spacer.gif" width="10" height="1" alt="">
日時：<select name="year"><? show_select_years_span(1960, date("Y") + 1, $year); ?></select>/<select name="month"><? show_select_months($month); ?></select>/<select name="day"><? show_select_days($day); ?></select>&nbsp;<select name="hour"><? show_select_hrs_0_23($hour); ?></select>：<select name="minute"><option value="00"<? if ($minute == "00") {echo(" selected");} ?>>00<option value="30"<? if ($minute == "30") {echo(" selected");} ?>>30</select>
<img src="img/spacer.gif" width="40" height="1" alt="">
<input type="submit" value="表示">
</font></td>
<? if ($bldg_cd != "") { ?>
<td align="left"><input type="button" value="掲示板表示" onclick="showBBS();"></td>
<td align="right"><input type="button" value="EXCEL出力" onclick="document.excelform.submit();"></td>
<? } ?>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="mode" value="show">
<?
if ($bldg_cd != "") {
	show_building_status($con, $bldg_cd, $year, $month, $day, $hour, $minute, $session, $fname);
}
?>
</form>
<form name="excelform" action="bed_menu_status_excel.php" method="get">
<input type="hidden" name="bldg_cd" value="<? echo($bldg_cd); ?>">
<input type="hidden" name="year" value="<? echo($year); ?>">
<input type="hidden" name="month" value="<? echo($month); ?>">
<input type="hidden" name="day" value="<? echo($day); ?>">
<input type="hidden" name="hour" value="<? echo($hour); ?>">
<input type="hidden" name="minute" value="<? echo($minute); ?>">
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
function show_building_status($con, $bldg_cd, $year, $month, $day, $hour, $minute, $session, $fname) {

	// 合計行用の変数を初期化
	$sum_bed_count = 0;
	$sum_in_patient_count = 0;
	$sum_in_res_patient_count = 0;
	$sum_move_in_patient_count = 0;
	$sum_move_out_patient_count = 0;
	$sum_out_res_patient_count = 0;
	$sum_in_res_patient_count_week = 0;
	$sum_out_res_patient_count_week = 0;
	$sum_go_out_count = 0;
	$sum_stop_out_count = 0;
	$sum_move1_count = 0;
	$sum_move2_count = 0;
	$sum_move3_count = 0;
	$sum_family_count = 0;
	$sum_ventilator_count = 0;

	// 病棟一覧を配列に格納
	$sql = "select * from wdmst";
	$cond = "where bldg_cd = $bldg_cd and ward_del_flg = 'f' order by ward_cd";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$wards[$row["ward_cd"]] = array(
			"ward_name" => $row["ward_name"],
			"bed_count" => 0,
			"in_patient_count" => 0,
			"in_res_patient_count" => 0,
			"move_in_patient_count" => 0,
			"move_out_patient_count" => 0,
			"out_res_patient_count" => 0,
			"in_res_patient_count_week" => 0,
			"out_res_patient_count_week" => 0,
			"go_out_count" => 0,
			"stop_out_count" => 0,
			"move1_count" => 0,
			"move2_count" => 0,
			"move3_count" => 0,
			"family_count" => 0,
			"ventilator_count" => 0
		);
	}

	// 病室一覧を取得
	$sql = "select ward_cd, ptrm_room_no, ptrm_name, ptrm_bed_cur from ptrmmst";
	$cond = "where bldg_cd = $bldg_cd and ptrm_del_flg = 'f' order by ptrm_room_no";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$tmp_ward_cd = $row["ward_cd"];
		$tmp_bed_count = intval($row["ptrm_bed_cur"]);

		$wards[$tmp_ward_cd]["bed_count"] += $tmp_bed_count;
		$sum_bed_count += $tmp_bed_count;

		$wards[$tmp_ward_cd]["rooms"][$row["ptrm_room_no"]] = array(
			"name" => $row["ptrm_name"],
			"bed_count" => $tmp_bed_count,
			"male" => 0,
			"female" => 0,
			"unknown" => 0
		);
	}

	// 指定日に在院していた患者一覧を取得
	$sql = " select i.ptif_id, i.inpt_in_dt, i.inpt_in_tm, i.inpt_out_dt, i.inpt_out_tm, i.bldg_cd, i.ward_cd, i.ptrm_room_no, ptifmst.ptif_sex from inptmst i inner join ptifmst on ptifmst.ptif_id = i.ptif_id where i.inpt_in_dt is not null and ((i.inpt_in_dt = '$year$month$day' and i.inpt_in_tm <= '$hour$minute') or (i.inpt_in_dt < '$year$month$day')) ";
	$sql .= " union select i.ptif_id, i.inpt_in_dt, i.inpt_in_tm, i.inpt_out_dt, i.inpt_out_tm, i.bldg_cd, i.ward_cd, i.ptrm_room_no, ptifmst.ptif_sex from inpthist i inner join ptifmst on ptifmst.ptif_id = i.ptif_id where ((i.inpt_in_dt = '$year$month$day' and i.inpt_in_tm <= '$hour$minute') or (i.inpt_in_dt < '$year$month$day')) and ((i.inpt_out_dt = '$year$month$day' and i.inpt_out_tm >= '$hour$minute') or (i.inpt_out_dt > '$year$month$day')) ";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// 患者一覧をループ
	while ($row = pg_fetch_array($sel)) {
		$tmp_ptif_id = $row["ptif_id"];
		$tmp_in_dt = $row["inpt_in_dt"];
		$tmp_in_tm = $row["inpt_in_tm"];
		$tmp_out_dt = $row["inpt_out_dt"];
		$tmp_out_tm = $row["inpt_out_tm"];
		$tmp_ptif_sex = $row["ptif_sex"];

		// 当該患者の指定日直近の確定済み移転情報を取得
		$sql = "select * from inptmove";
		$cond = "where ptif_id = '$tmp_ptif_id' and move_cfm_flg = 't' and ((move_dt = '$tmp_in_dt' and move_tm >= '$tmp_in_tm') or (move_dt > '$tmp_in_dt')) and ((move_dt = '$year$month$day' and move_tm >= '$hour$minute') or (move_dt > '$year$month$day'))";
		if ($tmp_out_dt != "" && $tmp_out_tm != "") {
			$cond .= " and ((move_dt = '$tmp_out_dt' and move_tm < '$tmp_out_tm') or (move_dt < '$tmp_out_dt'))";
		}
		$cond .= " order by move_dt, move_tm limit 1";
		$sel2 = select_from_table($con, $sql, $cond, $fname);
		if ($sel2 == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 病室を確定
		if (pg_num_rows($sel2) == 0) {
			$tmp_bldg_cd = $row["bldg_cd"];
			$tmp_ward_cd = $row["ward_cd"];
			$tmp_ptrm_room_no = $row["ptrm_room_no"];
		} else {
			$tmp_bldg_cd = pg_fetch_result($sel2, 0, "from_bldg_cd");
			$tmp_ward_cd = pg_fetch_result($sel2, 0, "from_ward_cd");
			$tmp_ptrm_room_no = pg_fetch_result($sel2, 0, "from_ptrm_room_no");
		}

		// 棟が違う場合は次の患者へ
		if ($tmp_bldg_cd != $bldg_cd) {
			continue;
		}

		// 在院患者数をカウント
		$wards[$tmp_ward_cd]["in_patient_count"]++;
		$sum_in_patient_count++;
		switch ($tmp_ptif_sex) {
		case "1":
			$wards[$tmp_ward_cd]["rooms"][$tmp_ptrm_room_no]["male"]++;
			break;
		case "2":
			$wards[$tmp_ward_cd]["rooms"][$tmp_ptrm_room_no]["female"]++;
			break;
		default:
			$wards[$tmp_ward_cd]["rooms"][$tmp_ptrm_room_no]["unknown"]++;
			break;
		}

		// 外泊情報を取得
		$sql = "select count(*) from inptgoout";
		$cond = "where ptif_id = '$tmp_ptif_id' and out_type = '2' and out_date <= '$year$month$day' and ret_date >= '$year$month$day'";
		$sel2 = select_from_table($con, $sql, $cond, $fname);
		if ($sel2 == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$tmp_go_out_count = intval(pg_fetch_result($sel2, 0, 0));
		$wards[$tmp_ward_cd]["go_out_count"] += $tmp_go_out_count;
		$sum_go_out_count += $tmp_go_out_count;

		// 外出情報を取得
		$sql = "select count(*) from inptgoout";
		$cond = "where ptif_id = '$tmp_ptif_id' and out_type = '1' and out_date = '$year$month$day' and out_time <= '$hour$minute' and ret_time >= '$hour$minute'";
		$sel2 = select_from_table($con, $sql, $cond, $fname);
		if ($sel2 == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$tmp_stop_out_count = intval(pg_fetch_result($sel2, 0, 0));
		$wards[$tmp_ward_cd]["stop_out_count"] += $tmp_stop_out_count;
		$sum_stop_out_count += $tmp_stop_out_count;

		// 移動手段情報・人工呼吸器情報を取得
		$sql = "select ptsubif_move_flg, ptsubif_move1, ptsubif_move2, ptsubif_move3, ptsubif_move4, ptsubif_move5, ptsubif_ventilator from ptsubif";
		$cond = "where ptif_id = '$tmp_ptif_id'";
		$sel2 = select_from_table($con, $sql, $cond, $fname);
		if ($sel2 == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		if (pg_num_rows($sel2) > 0) {
			if (pg_fetch_result($sel2, 0, "ptsubif_move5") == "t") {
				$wards[$tmp_ward_cd]["move1_count"]++;
				$sum_move1_count++;
			} else if (pg_fetch_result($sel2, 0, "ptsubif_move1") == "t" ||
			           pg_fetch_result($sel2, 0, "ptsubif_move2") == "t" ||
			           pg_fetch_result($sel2, 0, "ptsubif_move3") == "t" ||
			           pg_fetch_result($sel2, 0, "ptsubif_move4") == "t") {
				$wards[$tmp_ward_cd]["move2_count"]++;
				$sum_move2_count++;
			} else if (pg_fetch_result($sel2, 0, "ptsubif_move_flg") == "t") {
				$wards[$tmp_ward_cd]["move3_count"]++;
				$sum_move3_count++;
			}

			if (pg_fetch_result($sel2, 0, "ptsubif_ventilator") == "t") {
				$wards[$tmp_ward_cd]["ventilator_count"]++;
				$sum_ventilator_count++;
			}
		}

		// 家族付き添い情報を取得
		$sql = "select sum(head_count) from inptfamily";
		$cond = "where ptif_id = '$tmp_ptif_id' and from_date <= '$year$month$day' and to_date >= '$year$month$day'";
		$sel2 = select_from_table($con, $sql, $cond, $fname);
		if ($sel2 == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$tmp_family_count = intval(pg_fetch_result($sel2, 0, 0));
		$wards[$tmp_ward_cd]["family_count"] += $tmp_family_count;
		$sum_family_count += $tmp_family_count;
	}

	// 入院予定患者数を取得
	$six_days_after = date("Ymd", strtotime("+6 days", mktime(0, 0, 0, $month, $day, $year)));
	$sql = "select ward_cd, inpt_in_res_dt, count(*) as in_res_patient_count from inptres";
	$cond = "where bldg_cd = $bldg_cd and inpt_in_res_dt between '$year$month$day' and '$six_days_after' group by ward_cd, inpt_in_res_dt";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$tmp_ward_cd = $row["ward_cd"];
		$tmp_in_res_patient_count = intval($row["in_res_patient_count"]);

		if ($row["inpt_in_res_dt"] == "$year$month$day") {
			$wards[$tmp_ward_cd]["in_res_patient_count"] = $tmp_in_res_patient_count;
			$sum_in_res_patient_count += $tmp_in_res_patient_count;
		}

		$wards[$tmp_ward_cd]["in_res_patient_count_week"] += $tmp_in_res_patient_count;
		$sum_in_res_patient_count_week += $tmp_in_res_patient_count;
	}

	// 転入予定患者数を取得
	$sql = "select to_ward_cd, count(*) as move_in_patient_count from inptmove";
	$cond = "where to_bldg_cd = $bldg_cd and move_dt = '$year$month$day' and move_cfm_flg = 'f' and move_del_flg = 'f' and (to_bldg_cd <> from_bldg_cd or to_ward_cd <> from_ward_cd) group by to_ward_cd";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$tmp_ward_cd = $row["to_ward_cd"];
		$tmp_move_in_patient_count = intval($row["move_in_patient_count"]);

		$wards[$tmp_ward_cd]["move_in_patient_count"] = $tmp_move_in_patient_count;
		$sum_move_in_patient_count += $tmp_move_in_patient_count;
	}

	// 転出予定患者数を取得
	$sql = "select from_ward_cd, count(*) as move_out_patient_count from inptmove";
	$cond = "where from_bldg_cd = $bldg_cd and move_dt = '$year$month$day' and move_cfm_flg = 'f' and move_del_flg = 'f' and (to_bldg_cd <> from_bldg_cd or to_ward_cd <> from_ward_cd) group by from_ward_cd";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$tmp_ward_cd = $row["from_ward_cd"];
		$tmp_move_out_patient_count = intval($row["move_out_patient_count"]);

		$wards[$tmp_ward_cd]["move_out_patient_count"] = $tmp_move_out_patient_count;
		$sum_move_out_patient_count += $tmp_move_out_patient_count;
	}

	// 退院予定患者数を取得
	$sql = "select ward_cd, inpt_out_res_dt, count(*) as out_res_patient_count from inptmst";
	$cond = "where bldg_cd = $bldg_cd and inpt_out_res_dt between '$year$month$day' and '$six_days_after' group by ward_cd, inpt_out_res_dt";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$tmp_ward_cd = $row["ward_cd"];
		$tmp_out_res_patient_count = intval($row["out_res_patient_count"]);

		if ($row["inpt_out_res_dt"] == "$year$month$day") {
			$wards[$tmp_ward_cd]["out_res_patient_count"] = $tmp_out_res_patient_count;
			$sum_out_res_patient_count += $tmp_out_res_patient_count;
		}

		$wards[$tmp_ward_cd]["out_res_patient_count_week"] += $tmp_out_res_patient_count;
		$sum_out_res_patient_count_week += $tmp_out_res_patient_count;
	}

	// 状況一覧表を表示
?>
<img src="img/spacer.gif" width="1" height="2" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff" align="center" valign="top">
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病棟</font></td>
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">在院患者</font></td>
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院予定</font></td>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">転棟予定</font></td>
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退院予定</font></td>
<td rowspan="2" colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">空床（3人部屋以上）</font></td>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一週間以内予定</font></td>
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">個室</font></td>
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">2人部屋</font></td>
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">外泊</font></td>
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">外出</font></td>
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">担送</font></td>
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">護送</font></td>
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">独歩</font></td>
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">家族</font></td>
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">人工R器</font></td>
</tr>
<tr height="22" bgcolor="#f6f9ff" align="center" valign="top">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">転入</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">転出</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退院</font></td>
</tr>
<?
	foreach ($wards as $tmp_ward_cd => $tmp_ward) {
		$tmp_ward_name = $tmp_ward["ward_name"];
		$tmp_bed_count = $tmp_ward["bed_count"];
		$tmp_in_patient_count = $tmp_ward["in_patient_count"];
		$tmp_in_res_patient_count = $tmp_ward["in_res_patient_count"];
		$tmp_move_in_patient_count = $tmp_ward["move_in_patient_count"];
		$tmp_move_out_patient_count = $tmp_ward["move_out_patient_count"];
		$tmp_out_res_patient_count = $tmp_ward["out_res_patient_count"];
		$tmp_in_res_patient_count_week = $tmp_ward["in_res_patient_count_week"];
		$tmp_out_res_patient_count_week = $tmp_ward["out_res_patient_count_week"];
		$tmp_go_out_count = $tmp_ward["go_out_count"];
		$tmp_stop_out_count = $tmp_ward["stop_out_count"];
		$tmp_move1_count = $tmp_ward["move1_count"];
		$tmp_move2_count = $tmp_ward["move2_count"];
		$tmp_move3_count = $tmp_ward["move3_count"];
		$tmp_family_count = $tmp_ward["family_count"];
		$tmp_ventilator_count = $tmp_ward["ventilator_count"];

		// 空床リストの作成
		$tmp_male_rooms = array();
		$tmp_female_rooms = array();
		$tmp_sum_male_bed_count = 0;
		$tmp_sum_female_bed_count = 0;
		$tmp_single_rooms = array();
		$tmp_double_rooms = array();
		foreach ($tmp_ward["rooms"] as $tmp_ptrm_room_no => $tmp_room) {
			$tmp_room_bed_count = $tmp_room["bed_count"];
			$tmp_room_male_count = $tmp_room["male"];
			$tmp_room_female_count = $tmp_room["female"];

			$tmp_vacant_bed_count = $tmp_room_bed_count
			                      - $tmp_room_male_count
			                      - $tmp_room_female_count
			                      - $tmp_room["unknown"];
			if ($tmp_vacant_bed_count == 0) {
				continue;
			}

			// 個室の場合
			if ($tmp_room_bed_count == 1) {
				$tmp_single_rooms[] = $tmp_room["name"];

			// 2人部屋の場合
			} else if ($tmp_room_bed_count == 2) {
				if ($tmp_room_male_count == 1) {
					$tmp_room_sex = "male";
				} else if ($tmp_room_female_count == 1) {
					$tmp_room_sex = "female";
				} else {
					$tmp_room_sex = "";
				}

				$tmp_double_rooms[] = array(
					"name" => $tmp_room["name"],
					"sex" => $tmp_room_sex
				);

			// 3人部屋以上の場合
			} else if ($tmp_room_bed_count >= 3) {

				// 男部屋の場合
				if ($tmp_room_male_count >= $tmp_room_female_count) {
					$tmp_male_rooms[] = array(
						"name" => $tmp_room["name"],
						"bed_count" => $tmp_vacant_bed_count,
						"mixed" => ($tmp_room_male_count > 0 && $tmp_room_female_count > 0)
					);
					$tmp_sum_male_bed_count += $tmp_vacant_bed_count;

				// 女部屋の場合
				} else {
					$tmp_female_rooms[] = array(
						"name" => $tmp_room["name"],
						"bed_count" => $tmp_vacant_bed_count,
						"mixed" => ($tmp_room_male_count > 0 && $tmp_room_female_count > 0)
					);
					$tmp_sum_female_bed_count += $tmp_vacant_bed_count;
				}
			}
		}
?>
<tr height="22" align="center" valign="top">
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_ward_name); ?><br>(<? echo($tmp_bed_count); ?>)</font></td>
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
if ($tmp_in_patient_count > 0) {
	echo("<a href=\"bed_menu_status_detail.php?bldg_cd=$bldg_cd&ward_cd=$tmp_ward_cd&year=$year&month=$month&day=$day&hour=$hour&minute=$minute&session=$session&type=1\">");
}
echo($tmp_in_patient_count);
if ($tmp_in_patient_count > 0) {
	echo("</a>");
}
?>
</font></td>
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
if ($tmp_in_res_patient_count > 0) {
	echo("<a href=\"bed_menu_status_detail.php?bldg_cd=$bldg_cd&ward_cd=$tmp_ward_cd&year=$year&month=$month&day=$day&hour=$hour&minute=$minute&session=$session&type=2\">");
}
echo($tmp_in_res_patient_count);
if ($tmp_in_res_patient_count > 0) {
	echo("</a>");
}
?>
</font></td>
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
if ($tmp_move_in_patient_count > 0) {
	echo("<a href=\"bed_menu_status_detail.php?bldg_cd=$bldg_cd&ward_cd=$tmp_ward_cd&year=$year&month=$month&day=$day&hour=$hour&minute=$minute&session=$session&type=13\">");
}
echo($tmp_move_in_patient_count);
if ($tmp_move_in_patient_count > 0) {
	echo("</a>");
}
?>
</font></td>
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
if ($tmp_move_out_patient_count > 0) {
	echo("<a href=\"bed_menu_status_detail.php?bldg_cd=$bldg_cd&ward_cd=$tmp_ward_cd&year=$year&month=$month&day=$day&hour=$hour&minute=$minute&session=$session&type=14\">");
}
echo($tmp_move_out_patient_count);
if ($tmp_move_out_patient_count > 0) {
	echo("</a>");
}
?>
</font></td>
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
if ($tmp_out_res_patient_count > 0) {
	echo("<a href=\"bed_menu_status_detail.php?bldg_cd=$bldg_cd&ward_cd=$tmp_ward_cd&year=$year&month=$month&day=$day&hour=$hour&minute=$minute&session=$session&type=3\">");
}
echo($tmp_out_res_patient_count);
if ($tmp_out_res_patient_count > 0) {
	echo("</a>");
}
?>
</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">男</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_sum_male_bed_count); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
		foreach ($tmp_male_rooms as $tmp_room) {
			if ($tmp_room["mixed"]) {
				echo("<font color=\"red\">");
			}
			echo("{$tmp_room["name"]}({$tmp_room["bed_count"]})<br>");
			if ($tmp_room["mixed"]) {
				echo("</font>");
			}
		}
?>
</font></td>
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
if ($tmp_in_res_patient_count_week > 0) {
	echo("<a href=\"bed_menu_status_detail.php?bldg_cd=$bldg_cd&ward_cd=$tmp_ward_cd&year=$year&month=$month&day=$day&hour=$hour&minute=$minute&session=$session&type=4\">");
}
echo($tmp_in_res_patient_count_week);
if ($tmp_in_res_patient_count_week > 0) {
	echo("</a>");
}
?>
</font></td>
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
if ($tmp_out_res_patient_count_week > 0) {
	echo("<a href=\"bed_menu_status_detail.php?bldg_cd=$bldg_cd&ward_cd=$tmp_ward_cd&year=$year&month=$month&day=$day&hour=$hour&minute=$minute&session=$session&type=5\">");
}
echo($tmp_out_res_patient_count_week);
if ($tmp_out_res_patient_count_week > 0) {
	echo("</a>");
}
?>
</font></td>
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
		foreach ($tmp_single_rooms as $tmp_room_name) {
			echo("$tmp_room_name<br>");
		}
?>
</font></td>
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
		foreach ($tmp_double_rooms as $tmp_room) {
			echo("{$tmp_room["name"]}<br>");
			if ($tmp_room["sex"] == "male") {
				echo("(男1)");
			} else if ($tmp_room["sex"] == "female") {
				echo("(女1)");
			}
		}
?>
</font></td>
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
if ($tmp_go_out_count > 0) {
	echo("<a href=\"bed_menu_status_detail.php?bldg_cd=$bldg_cd&ward_cd=$tmp_ward_cd&year=$year&month=$month&day=$day&hour=$hour&minute=$minute&session=$session&type=6\">");
}
echo($tmp_go_out_count);
if ($tmp_go_out_count > 0) {
	echo("</a>");
}
?>
</font></td>
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
if ($tmp_stop_out_count > 0) {
	echo("<a href=\"bed_menu_status_detail.php?bldg_cd=$bldg_cd&ward_cd=$tmp_ward_cd&year=$year&month=$month&day=$day&hour=$hour&minute=$minute&session=$session&type=7\">");
}
echo($tmp_stop_out_count);
if ($tmp_stop_out_count > 0) {
	echo("</a>");
}
?>
</font></td>
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
if ($tmp_move1_count > 0) {
	echo("<a href=\"bed_menu_status_detail.php?bldg_cd=$bldg_cd&ward_cd=$tmp_ward_cd&year=$year&month=$month&day=$day&hour=$hour&minute=$minute&session=$session&type=8\">");
}
echo($tmp_move1_count);
if ($tmp_move1_count > 0) {
	echo("</a>");
}
?>
</font></td>
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
if ($tmp_move2_count > 0) {
	echo("<a href=\"bed_menu_status_detail.php?bldg_cd=$bldg_cd&ward_cd=$tmp_ward_cd&year=$year&month=$month&day=$day&hour=$hour&minute=$minute&session=$session&type=9\">");
}
echo($tmp_move2_count);
if ($tmp_move2_count > 0) {
	echo("</a>");
}
?>
</font></td>
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
if ($tmp_move3_count > 0) {
	echo("<a href=\"bed_menu_status_detail.php?bldg_cd=$bldg_cd&ward_cd=$tmp_ward_cd&year=$year&month=$month&day=$day&hour=$hour&minute=$minute&session=$session&type=10\">");
}
echo($tmp_move3_count);
if ($tmp_move3_count > 0) {
	echo("</a>");
}
?>
</font></td>
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
if ($tmp_family_count > 0) {
	echo("<a href=\"bed_menu_status_detail.php?bldg_cd=$bldg_cd&ward_cd=$tmp_ward_cd&year=$year&month=$month&day=$day&hour=$hour&minute=$minute&session=$session&type=11\">");
}
echo($tmp_family_count);
if ($tmp_family_count > 0) {
	echo("</a>");
}
?>
</font></td>
<td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
if ($tmp_ventilator_count > 0) {
	echo("<a href=\"bed_menu_status_detail.php?bldg_cd=$bldg_cd&ward_cd=$tmp_ward_cd&year=$year&month=$month&day=$day&hour=$hour&minute=$minute&session=$session&type=12\">");
}
echo($tmp_ventilator_count);
if ($tmp_ventilator_count > 0) {
	echo("</a>");
}
?>
</font></td>
</tr>
<tr height="22" align="center" valign="top">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">女</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_sum_female_bed_count); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
		foreach ($tmp_female_rooms as $tmp_room) {
			if ($tmp_room["mixed"]) {
				echo("<font color=\"red\">");
			}
			echo("{$tmp_room["name"]}({$tmp_room["bed_count"]})<br>");
			if ($tmp_room["mixed"]) {
				echo("</font>");
			}
		}
?>
</font></td>
</tr>
<?
	}
?>
<tr height="22" align="center" valign="top">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">合計<br>(<? echo($sum_bed_count); ?>)</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($sum_in_patient_count); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($sum_in_res_patient_count); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($sum_move_in_patient_count); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($sum_move_out_patient_count); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($sum_out_res_patient_count); ?></font></td>
<td colspan="3"></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($sum_in_res_patient_count_week); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($sum_out_res_patient_count_week); ?></font></td>
<td></td>
<td></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($sum_go_out_count); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($sum_stop_out_count); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($sum_move1_count); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($sum_move2_count); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($sum_move3_count); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($sum_family_count); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($sum_ventilator_count); ?></font></td>
</tr>
</table>
<?
}
?>
