<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 52, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if (!is_array($item_ids) && !is_array($assist_ids)) {
	echo("<script type=\"text/javascript\">alert('削除対象が選択されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// データベースへ接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// チェックされた管理項目をループ
foreach ($item_ids as $tmp_item_id) {

	// 配下の補助項目に、計算式で使われているものがあればエラーとする
	$sql = "select count(*) from statassist";
	$cond = "where statitem_id = $tmp_item_id and exists (select * from statassist child where (child.calc_var1_assist_id = statassist.statitem_id or child.calc_var2_assist_id = statassist.statitem_id) and child.del_flg = 'f')";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (intval(pg_fetch_result($sel, 0, 0)) > 0) {
		$sql = "select name from statitem";
		$cond = "where statitem_id = $tmp_item_id";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$tmp_item_name = pg_fetch_result($sel, 0, "name");

		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('管理項目「{$tmp_item_name}」は、\\n配下の補助項目が他の計算式に使用されているため削除できません。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}

	// 管理項目を論理削除
	$sql = "update statitem set";
	$set = array("del_flg");
	$setvalue = array("t");
	$cond = "where statitem_id = $tmp_item_id";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// チェックされた補助項目をループ
foreach ($assist_ids as $tmp_assist_id) {

	// 補助項目が計算式で使われているたらエラーとする
	$sql = "select count(*) from statassist";
	$cond = "where (calc_var1_assist_id = $tmp_assist_id or calc_var2_assist_id = $tmp_assist_id) and del_flg = 'f'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (intval(pg_fetch_result($sel, 0, 0)) > 0) {
		$sql = "select statassist_name from statassist";
		$cond = "where statassist_id = $tmp_assist_id";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$tmp_assist_name = pg_fetch_result($sel, 0, "statassist_name");

		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('補助項目「{$tmp_assist_name}」は、\\n他の計算式に使用されているため削除できません。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}

	// 補助項目を論理削除
	$sql = "update statassist set";
	$set = array("del_flg");
	$setvalue = array("t");
	$cond = "where statassist_id = $tmp_assist_id";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 管理項目一覧画面に遷移
echo("<script type=\"text/javascript\">location.href = 'stat_master_item_list.php?session=$session'</script>");
?>
