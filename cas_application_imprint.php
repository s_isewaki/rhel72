<?
require("about_session.php");
require("about_authority.php");
require("about_postgres.php");
require("show_select_values.ini");
require("referer_common.ini");
require("get_values.ini");
require("cas_application_imprint_common.ini");
require("./conf/sql.inf");
require_once("cas_application_common.ini");
require_once("cas_common.ini");
require_once("cas_application_workflow_common_class.php");
require_once("get_cas_title_name.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session, $CAS_MENU_AUTH, $fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
// ワークフロー権限の取得
$workflow_auth = check_authority($session, $CAS_MANAGE_AUTH, $fname);

$con = connect2db($fname);

// 職員ID取得 
$emp_id=get_emp_id($con,$session,$fname);


?>

<?
// 印影機能フラグ取得
$db_imprint_flg = get_imprint_flg($con, $emp_id, $fname);

$delbtn_flg = "f";

// 画像登録
if ($regist_flg == "1") {
	// ファイルアップロードチェック
	define(UPLOAD_ERR_OK, 0);
	define(UPLOAD_ERR_INI_SIZE, 1);
	define(UPLOAD_ERR_FORM_SIZE, 2);
	define(UPLOAD_ERR_PARTIAL, 3);
	define(UPLOAD_ERR_NO_FILE, 4);

	$filename = $_FILES["imprint"]["name"];

	if ($filename != "") {
		$delbtn_flg = "t";
		// gif, jpgチェック
		$pos = strrpos($filename, ".");
		$ext = "";
		if ($pos > 0 ) {
			$ext = substr($filename, $pos+1, 3);
			$ext = strtolower($ext);
		}

		if ($pos === false || ($ext != "gif" && $ext != "jpg")) {
			echo("<script language=\"javascript\">alert('拡張子はgifかjpgのファイルを指定してください。');</script>");
			echo("<script language=\"javascript\">history.back();</script>");
			exit;
		}

		switch ($_FILES["imprint"]["error"]) {
		case UPLOAD_ERR_INI_SIZE:
		case UPLOAD_ERR_FORM_SIZE:
			echo("<script language=\"javascript\">alert('ファイルサイズが大きすぎます。');</script>");
			echo("<script language=\"javascript\">history.back();</script>");
			exit;
		case UPLOAD_ERR_PARTIAL:
		case UPLOAD_ERR_NO_FILE:
			echo("<script language=\"javascript\">alert('アップロードに失敗しました。再度実行してください。');</script>");
			echo("<script language=\"javascript\">history.back();</script>");
			exit;
		}

		// ファイル保存用ディレクトリがなければ作成
		if (!is_dir("cas_workflow")) {
			mkdir("cas_workflow", 0755);
		}
		if (!is_dir("cas_workflow/imprint")) {
			mkdir("cas_workflow/imprint", 0755);
		}

		
		// 画像がある場合は削除
		$filename1 = "cas_workflow/imprint/$emp_id.gif";
		$filename2 = "cas_workflow/imprint/$emp_id.jpg";

		if (is_file($filename1)) {
			unlink($filename1);
		}
		if (is_file($filename2)) {
			unlink($filename2);
		}
		// アップロードされたファイルを保存
		$savefilename = "cas_workflow/imprint/{$emp_id}.{$ext}";
		$ret = copy($_FILES["imprint"]["tmp_name"], $savefilename);

		if ($ret == false) {
			echo("<script language=\"javascript\">alert('ファイルのコピーに失敗しました。再度実行してください。');</script>");
			echo("<script language=\"javascript\">history.back();</script>");
			exit;
		}
	}

	// 印影機能フラグ登録
	$insert_update_flg = "insert";
	if ($db_imprint_flg != "") {
		$insert_update_flg = "update";
	}
	regist_imprint_flg($con, $fname, $emp_id, $imprint_flg, $insert_update_flg);
} else if ($regist_flg == "2") {
	$delbtn_flg = "f";
	// 画像がある場合は削除
	$filename1 = "cas_workflow/imprint/$emp_id.gif";
	$filename2 = "cas_workflow/imprint/$emp_id.jpg";

	if (is_file($filename1)) {
		unlink($filename1);
	}
	if (is_file($filename2)) {
		unlink($filename2);
	}
	// 印影機能レコード削除
	regist_imprint_flg($con, $fname, $emp_id, "", "delete");
}

if ($imprint_flg == "") {
	if ($db_imprint_flg != "") {
		$imprint_flg = $db_imprint_flg;
	} else {
		$imprint_flg = "f";
	}
}

$obj = new cas_application_workflow_common_class($con, $fname);


$cas_title = get_cas_title_name();

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$cas_title?>｜印影登録</title>
<script type="text/javascript">
function submitForm(flg) {
	document.wkfw.regist_flg.value = flg;
	document.wkfw.submit();
}

function referTemplate() {
	window.open('cas_workflow_template_refer.php?session=<? echo($session); ?>', 'newwin', 'width=640,height=480,scrollbars=yes');
}
</script>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#A0D25A solid 1px;}
p {margin:0;}

table.block3 {border-collapse:collapse;}
table.block3 td {border:#A0D25A solid 0px;}
table.block3 td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="cas_application_menu.php?session=<? echo($session); ?>"><img src="img/icon/s42.gif" width="32" height="32" border="0" alt="<? echo($cas_title); ?>"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="cas_application_menu.php?session=<? echo($session); ?>"><b><? echo($cas_title); ?></b></a> &gt; <a href="cas_application_imprint.php?session=<? echo($session); ?>"><b>印影登録</b></a></font></td>
<? if ($workflow_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="cas_workflow_menu.php?session=<? echo($session); ?>&workflow=1"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>

<?
show_applycation_menuitem($session,$fname,"");
?>


<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#A0D25A"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form name="wkfw" action="cas_application_imprint.php" method="post" enctype="multipart/form-data">
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="160" align="right" bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">印影機能を利用する</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="imprint_flg" value="t"<? if ($imprint_flg == "t") {echo(" checked");} ?>>はい
<input type="radio" name="imprint_flg" value="f"<? if ($imprint_flg == "f") {echo(" checked");} ?>>いいえ
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">印影画像指定(Jpeg,gif)</font></td>
<td>
<input type="file" name="imprint" size="60">

<table border="0" cellspacing="0" cellpadding="1" class="block3">
<tr>
<td>
プレビュー
</td>
<td>
<?
// 画像がある場合は表示

$filename = "";
$filename1 = "cas_workflow/imprint/$emp_id.gif";
$filename2 = "cas_workflow/imprint/$emp_id.jpg";

if (is_file($filename1)) {
	$filename = $filename1;
} else if (is_file($filename2)) {
	$filename = $filename2;
}

if ($filename == "") { ?>
<img src="img/spacer.gif" width="48" height="48" border="1">
<? } else {
	// 印影表示関数
	show_imprint_image($session, $emp_id, 1, "t", "t");
	$delbtn_flg = "t";
} ?>

</td>
</tr>
</table>

</td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr>
<td align="right">
<?
if ($delbtn_flg == "t") {
?>
<input type="button" name="del" value="削除" onclick="if(confirm('削除します。よろしいですか？')){submitForm('2');}">
<? } ?>
<input type="button" name="regist" value="登録" onclick="submitForm('1');">
</td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="regist_flg" value="">
</form>
</td>
</tr>
</table>

</body>
<? pg_close($con); ?>

</html>
