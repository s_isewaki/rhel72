<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("hiyari_common.ini");
require_once("hiyari_post_select_box.ini");
require_once("hiyari_report_class.php");
require_once("Cmx.php");
require_once("aclg_set.php");

//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$summary = check_authority($session, 48, $fname);
if ($summary == "0") {
	showLoginPage();
	exit;
}

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con,$_GET);

switch($mode)
{
	case "profile_search":
	case "profile_regist":
	case "page_select":

		if($mode == "profile_regist")
		{
			set_profile($con, $fname, $_POST);
		}

		$arr_post = array();

		if($mode == "profile_search")
		{
			if($search_emp_class != "")
			{
				array_push($arr_post, $search_emp_class);
			}
				if($search_emp_attribute != "")
			{
				array_push($arr_post, $search_emp_attribute);
			}
			if($search_emp_dept != "")
			{
				array_push($arr_post, $search_emp_dept);
			}
			if($search_emp_room != "")
			{
				array_push($arr_post, $search_emp_room);
			}

			$search_emp_class_hidden = $search_emp_class;
			$search_emp_attribute_hidden = $search_emp_attribute;
			$search_emp_dept_hidden = $search_emp_dept;
			$search_emp_room_hidden = $search_emp_room;
		}
 		else
		{
			if($search_emp_class_hidden != "")
			{
				array_push($arr_post, $search_emp_class_hidden);
			}
				if($search_emp_attribute_hidden != "")
			{
				array_push($arr_post, $search_emp_attribute_hidden);
			}
			if($search_emp_dept_hidden != "")
			{
				array_push($arr_post, $search_emp_dept_hidden);
			}
			if($search_emp_room_hidden != "")
			{
				array_push($arr_post, $search_emp_room_hidden);
			}

			$search_emp_class = $search_emp_class_hidden;
			$search_emp_attribute = $search_emp_attribute_hidden;
			$search_emp_dept = $search_emp_dept_hidden;
			$search_emp_room = $search_emp_room_hidden;

		}

		$list_data_array = get_emp_profile($con, $fname, $arr_post);

		break;

	default:
		break;
}


//==============================
//ページングに関する情報
//==============================
//page省略時は先頭ページを対象とする。
if($page == "")
{
	$page = 1;
}

//一画面内の最大表示件数
$disp_count_max_in_page = 30;

//表示開始/終了インデックス
$disp_start_index = ($page - 1)*$disp_count_max_in_page;
$disp_end_index   = $disp_start_index + $disp_count_max_in_page - 1;

//全データ件数
$rec_count = count($list_data_array);
if($rec_count == 1 && $list_data_array[0] == "")
{
	$rec_count = 0;
}

//最大ページ数
if($rec_count == 0)
{
	$page_max  = 1;
}
else
{
	$page_max  = floor( ($rec_count-1) / $disp_count_max_in_page ) + 1;
}

//==============================
//ページングによる絞込み
//==============================

$list_data_array_work = array();
for($i=$disp_start_index;$i<=$disp_end_index;$i++)
{
	if($i >= $rec_count)
	{
		break;
	}
	$list_data_array_work[] = $list_data_array[$i];
}
$list_data_array = $list_data_array_work;


$emp_ids = "";
foreach($list_data_array as $list)
{
	if($emp_ids != "")
	{
		$emp_ids .= ",";
	}
	$emp_ids .= $list["emp_id"];

}


$rep_obj = new hiyari_report_class($con, $fname);
// プロフィール職種情報取得
$exp_list = get_exp_list($con,$fname);

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$INCIDENT_TITLE?> | プロフィール設定</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">

function profile_search()
{
	document.list_form.page.value = "1";
	document.list_form.action="hiyari_profile_list.php?session=<?=$session?>&mode=profile_search";
	document.list_form.submit();
}


function profile_regist()
{
	if(chk_form())
	{
		document.list_form.action="hiyari_profile_list.php?session=<?=$session?>&mode=profile_regist";
		document.list_form.submit();
	}
}

function chk_form()
{

	emp_ids = document.list_form.emp_ids.value;
	arr_emp_ids = emp_ids.split(",");
	for(i=0;i<arr_emp_ids.length;i++)
	{
		exp_year = "exp_year_" + arr_emp_ids[i];
		exp_year = document.getElementById(exp_year).value;

		exp_month = "exp_month_" + arr_emp_ids[i];
		exp_month = document.getElementById(exp_month).value;

		dept_year = "dept_year_" + arr_emp_ids[i];
		dept_year = document.getElementById(dept_year).value;

		dept_month = "dept_month_" + arr_emp_ids[i];
		dept_month = document.getElementById(dept_month).value;

		if ((exp_year == '' && exp_month != '') || (exp_year != '' && exp_month == '') || (dept_year == '' && dept_month != '') || (dept_year != '' && dept_month == '')) {
			alert('年月は両方指定してください。');
			return false;
		}
	}
	return true;
}


</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
<!--
.list {border-collapse:collapse;}
.list td {border:#35B341 solid 1px;}
// -->
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<form name="list_form" action="#" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="page" value="<?=$page?>">
<input type="hidden" name="emp_ids" value="<?=$emp_ids?>">
<input type="hidden" name="search_emp_class_hidden" value="<?=$search_emp_class_hidden?>">
<input type="hidden" name="search_emp_attribute_hidden" value="<?=$search_emp_attribute_hidden?>">
<input type="hidden" name="search_emp_dept_hidden" value="<?=$search_emp_dept_hidden?>">
<input type="hidden" name="search_emp_room_hidden" value="<?=$search_emp_room_hidden?>">


<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<!-- ヘッダー -->
<?
show_hiyari_header($session,$fname,true);
?>
<!-- ヘッダー -->
<img src="img/spacer.gif" width="1" height="5" alt=""><br>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
<tr>
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td valign="top" bgcolor="#F5FFE5">

<!-- 検索領域start -->
<table border="0" cellspacing="0" cellpadding="0">
<td>
<?
show_post_select_box_for_profile($con, $fname, $search_emp_class, $search_emp_attribute, $search_emp_dept, $search_emp_room);
?>
</td>
<td width="100" align="right">
<input type="button" value="検索" onclick="profile_search();">
</td>
</td>
</table>
<!-- 検索領域end -->

<img src="img/spacer.gif" width="1" height="5" alt=""><br>


<!-- 一覧領域start -->


<?if($list_data_array[0] != ""){?>



<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td align="right">
<input type="button" value="登録" onclick="profile_regist();">
</td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>

<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">

<tr height="22" bgcolor="#DFFFDC">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員ID</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">氏名</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職種</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職種(標準コード)</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職種開始年月</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">部署開始年月</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">専門医・認定医等</font></td>
</tr>


<?foreach($list_data_array as $list_data) {?>
<tr bgcolor="#FFFFFF">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$list_data["emp_personal_id"]?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$list_data["emp_lt_nm"]." ".$list_data["emp_ft_nm"]?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$list_data["job_nm"]?></font></td>
<?
$exp_maerge_list = get_exp_merge_list($con,$fname,$list_data["exp_code"],$rep_obj,$exp_list);
?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select id="exp_code_<?=$list_data["emp_id"]?>" name="exp_code_<?=$list_data["emp_id"]?>"> <?=show_job_options($exp_maerge_list, $list_data["exp_code"], true);?></select></font></td>
<td nowrap>
	<select id="exp_year_<?=$list_data["emp_id"]?>" name="exp_year_<?=$list_data["emp_id"]?>"> <? show_select_years(41, $list_data["exp_year"], true); ?></select>
	<select id="exp_month_<?=$list_data["emp_id"]?>" name="exp_month_<?=$list_data["emp_id"]?>"><? show_select_months($list_data["exp_month"], true); ?></select>
</td>
<td nowrap>
	<select id="dept_year_<?=$list_data["emp_id"]?>" name="dept_year_<?=$list_data["emp_id"]?>"> <? show_select_years(40, $list_data["dept_year"], true); ?></select>
	<select id="dept_month_<?=$list_data["emp_id"]?>" name="dept_month_<?=$list_data["emp_id"]?>"><? show_select_months($list_data["dept_month"], true); ?></select>
</td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><textarea name="qualification_<?=$list_data["emp_id"]?>"><?=$list_data["qualification"]?></textarea></font></td>
</tr>
<?}?>
</table>

<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td align="left">
<?
show_page_select_area($session,$fname,$con,$page_max,$page)
?>
</td>
<td align="right">
<input type="button" value="登録" onclick="profile_regist();">
</td>
</tr>
</table>
<?}?>
<!-- 一覧領域end -->

</td>
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>


<!-- フッター START -->
<?
show_hiyari_footer($session,$fname,true);
?>
<!-- フッター END -->

</td>
</tr>
</table>

</form>
</body>
</html>

<?
//==============================
//職員のプロフィール情報取得
//==============================
function get_emp_profile($con, $fname, $arr_post)
{

	$sql  = "select a.emp_id, a.emp_personal_id, a.emp_lt_nm, a.emp_ft_nm, c.job_nm, d.exp_code, d.exp_year, d.exp_month, d.dept_year, d.dept_month, d.qualification ";
//	$sql .= "from empmst a ";
	$sql .= "from (select * from empmst natural inner join (select emp_id from authmst where emp_inci_flg) usable) a ";
	$sql .= "inner join authmst b on a.emp_id = b.emp_id ";
	$sql .= "left join jobmst c on a.emp_job = c.job_id ";
	$sql .= "left join inci_profile d on a.emp_id = d.emp_id ";

	$cond = "where not b.emp_del_flg ";

	$post_cnt = count($arr_post);
	if($post_cnt > 0) {
		$cond .= "and a.emp_class = $arr_post[0] ";
	}
	if($post_cnt > 1) {
		$cond .= "and a.emp_attribute = $arr_post[1] ";
	}
	if($post_cnt > 2) {
		$cond .= "and a.emp_dept = $arr_post[2] ";
	}
	if($post_cnt > 3) {
		$cond .= "and a.emp_room = $arr_post[3] ";
	}

	$cond .= "order by a.emp_kn_lt_nm, a.emp_kn_ft_nm ";

	$sel = select_from_table($con,$sql,$cond,$fname);
	if($sel == 0){
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$array = pg_fetch_all($sel);
	return $array;
}	

//==============================
//プロフィール職種情報取得
//==============================
function get_exp_list($con,$fname)
{

	$arr = array();
	$sql = "select easy_code, easy_name from inci_report_materials";
	$cond = "where grp_code = 3050 and easy_item_code = 30 and easy_flag = '1' order by easy_num";
	$sel = select_from_table($con,$sql,$cond,$fname);
	if($sel == 0){
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	while($row = pg_fetch_array($sel)) {

		$code = $row["easy_code"];
		$name = $row["easy_name"];
		$arr[$code] = $name;
	}	
	return $arr;
}

//==============================
//プロフィール情報セット
//==============================
function set_profile($con,$fname,$inputs)
{

	// トランザクションの開始
	pg_query($con, "begin transaction");

	$emp_ids = $inputs["emp_ids"];
	$arr_emp_ids = split(",", $emp_ids);
	// プロフィール情報削除
	delete_profile($con,$fname,$arr_emp_ids);

	// プロフィール情報登録
	foreach($arr_emp_ids as $val)
	{

		$qualification = $inputs["qualification_".$val];
		$exp_year = $inputs["exp_year_".$val];
		$exp_month = $inputs["exp_month_".$val];
		$dept_year = $inputs["dept_year_".$val];
		$dept_month = $inputs["dept_month_".$val];
		$exp_code = $inputs["exp_code_".$val];

		regist_profile($con,$fname,$val,$qualification,$exp_year,$exp_month,$dept_year,$dept_month,$exp_code);
	}


	// トランザクションをコミット
	pg_query($con, "commit");
}

//==============================
//プロフィール情報削除
//==============================
function delete_profile($con,$fname,$arr_emp_ids)
{

	$cond_emp_id = "";
	foreach($arr_emp_ids as $val)
	{
		if($cond_emp_id != "") {
			$cond_emp_id .= ",";
		}
		$cond_emp_id .= "'";
		$cond_emp_id .= $val;
		$cond_emp_id .= "'";
	}

	$sql = "delete from inci_profile";
	$cond = "where emp_id in ($cond_emp_id)";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

//==============================
//プロフィール情報登録
//==============================
function regist_profile($con,$fname,$emp_id,$qualification,$exp_year,$exp_month,$dept_year,$dept_month,$exp_code)
{

	$sql = "insert into inci_profile(emp_id, qualification, exp_year, exp_month, dept_year, dept_month, exp_code) values (";
	$content = array($emp_id, pg_escape_string($qualification), $exp_year, $exp_month, $dept_year, $dept_month, $exp_code);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}


}

//=================================
//職種オプションを出力
//=================================
function show_job_options($arr_info, $selected_code,$blank_flg) {
	if ($blank_flg) {
		echo("<option value=\"\">\n");
	}
	foreach ($arr_info as $tmp_code => $tmp_name) {
		echo("<option value=\"$tmp_code\"");
		if ($selected_code == $tmp_code) {
			echo(" selected");
		}
		$tmp_name = h($tmp_name);
		echo(">$tmp_name\n");
	}
}

//========================================================================
// 年オプションを出力
//========================================================================
function show_select_years($num, $fix, $blank_flg) {
	if ($blank_flg) {
		echo("<option value=\"\">\n");
	}

	$now = date("Y");

	if ($fix > 0) {
		$num = ($now - $num + 1 > $fix) ? $now - $fix + 1 : $num;
	}

	for ($i = 0; $i < $num; $i++) {
		$yr = $now - $i;
		echo("<option value=\"$yr\"");
		if ($fix == $yr) {
			echo(" selected");
		}
		echo(">$yr</option>\n");
	}
}

//========================================================================
// 月オプションを出力
//========================================================================
function show_select_months($fix, $blank_flg) {
	if ($blank_flg) {
		echo("<option value=\"\">\n");
	}

	for ($i = 1; $i <= 12; $i++) {
		$val = sprintf("%02d", $i);
		echo("<option value=\"$val\"");
		if ($i == $fix) {
			echo(" selected");
		}
		echo(">$val</option>\n");
	}
}


//========================================================================
// ページ出力
//========================================================================
function show_page_select_area($session,$fname,$con,$page_max,$page)
{
	if($page_max != 1)
	{
	?>
		<table width="100%">
		<tr>
		<td>
		  <table>
		  <tr>
		  <td>
		<?
		
		
		if($page == 1)
		{
		?>
		    <nobr>
		    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
		    <span style="color:silver">
		    先頭へ
		    </span>
		    </font>
		    </nobr>
		<?
		}
		else
		{
		?>
		    <nobr>
		    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
		    <a href="javascript:page_change(1);">
		    先頭へ
		    </a>
		    </font>
		    </nobr>
		<?
		}
		
		
		?>
		  </td>
		  <td>
		<?
		
		
		if($page == 1)
		{
		?>
		    <nobr>
		    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
		    <span style="color:silver">
		    前へ
		    </span>
		    </font>
		    </nobr>
		<?
		}
		else
		{
		?>
		    <nobr>
		    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
		    <a href="javascript:page_change(<?=$page-1?>);">
		    前へ
		    </a>
		    </font>
		    </nobr>
		<?
		}
		
		
		?>
		  </td>
		  <td>
		    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
		<?
		
		for($i=1;$i<=$page_max;$i++)
		{
			if($i == $page)
			{
		?>
		    [<?=$i?>]
		<?
			}
			else
			{
		?>
		    <a href="javascript:page_change(<?=$i?>);">[<?=$i?>]</a>
		<?
			}
		}
		?>
		    </font>
		  </td>
		  <td>
		<?
		
		
		if($page == $page_max)
		{
		?>
		    <nobr>
		    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
		    <span style="color:silver">
		    次へ
		    </span>
		    </font>
		    </nobr>
		<?
		}
		else
		{
		?>
		    <nobr>
		    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
		    <a href="javascript:page_change(<?=$page+1?>);">
		    次へ
		    </a>
		    </font>
		    </nobr>
		<?
		}
		
		
		?>
		  </td>
		  </tr>
		  </table>
		</td>
		</tr>
		</table>
	<?
	}
	?>
	<?
}

//==============================
//プロフィールリスト項目取得
//==============================
function get_exp_merge_list($con,$fname,$exp_code,$rep_obj,$exp_list)
{
	//項目要素非表示情報
	$item_element_no_disp = $rep_obj->get_item_element_no_disp();

	//職種リスト項目マージ処理
	$exp_list_temp = array();
	foreach($exp_list as $key => $val)
	{
		if(!in_array($key, $item_element_no_disp["3050"]["30"]) || $key == $exp_code)
		{
			$exp_list_temp[$key] = $val;
		}
	}	
	$exp_list = $exp_list_temp;

	return $exp_list;
}


?>
<script type="text/javascript">
//ページ遷移します。
function page_change(page)
{

	document.list_form.page.value=page;
	document.list_form.action="hiyari_profile_list.php?session=<?=$session?>&mode=page_select";
	document.list_form.submit();
}
</script>

<?
//==============================
//DBコネクション終了
//==============================
pg_close($con);
?>