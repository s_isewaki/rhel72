<?php
require_once("Cmx.php");
require_once('Cmx/Model/SystemConfig.php');
require_once("about_comedix.php");

class timecard_tantou_class
{

	var $file_name;	// 呼び出し元ファイル名
	var $_db_con;	// DBコネクション

	/** コンストラクタ
	 * @param $con          DBコネクション
	 * @param $fname        画面名
	 *
	 */
	function timecard_tantou_class($con, $fname)
    {
        // 呼び出し元ファイル名を保持
        $this->file_name = $fname;
        // DBコネクションを保持
        $this->_db_con = $con;

    }


    /**
     *  get_tantou_branches担当所属を取得
     *
     * @param mixed $emp_id 職員ID
     * @return mixed 担当所属の配列
     *
     */
    function get_tantou_branches($emp_id) {
		$timecard_tantou_branches = array();
		$sql = "select * from timecard_tantou_branches ";
		$cond = "where emp_id = '$emp_id'";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
		$rows = pg_fetch_all($sel);
		foreach ($rows as $row) {
		    if ($row["class_id"]) $timecard_tantou_branches[]= array(
		        "class_id"=>$row["class_id"], "atrb_id"=>$row["atrb_id"], "dept_id"=>$row["dept_id"], "room_id"=>$row["room_id"]
		    );
		}
		return $timecard_tantou_branches;

    }
    
	// 組織情報を取得し、配列に格納し返す（プルダウンメニュー用）
	// param $timecard_tantou_branches 担当所属
	// param $mmode 
	// 
	// return array($arr_cls, $arr_clsatrb, $arr_atrbdept)
    function get_clsatrb_info($timecard_tantou_branches, $mmode) {
		//所属設定がないのにusr側で呼び出された場合、通常はないはずだが念のための対応
		if (count($timecard_tantou_branches) == 0 && $mmode == "usr") {
			return array(array(), array(), array());
		}

		$arr_cls = array();
		$arr_clsatrb = array();
		$arr_atrbdept = array();
		$sql = "select class_id, class_nm from classmst";
		$cond = "where class_del_flg = 'f' ";
		if (count($timecard_tantou_branches) > 0) {
		    $cond .= " and (1 <> 1";
		    foreach ($timecard_tantou_branches as $r) {
		        $cond .= " or class_id = ". $r["class_id"];
		    }
		    $cond .= ")";
		}
		$cond .= " order by order_no";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		while ($row = pg_fetch_array($sel)) {
			$tmp_class_id = $row["class_id"];
			$tmp_class_nm = $row["class_nm"];
			array_push($arr_cls, array("id" => $tmp_class_id, "name" => $tmp_class_nm));

			$sql2 = "select atrb_id, atrb_nm from atrbmst";
		    $cond2 = "where class_id = '$tmp_class_id' and atrb_del_flg = 'f' ";
			if (count($timecard_tantou_branches) > 0) {
			    $cond2 .= " and (1 <> 1";
			    foreach ($timecard_tantou_branches as $r) {
			        $cond2 .= " or (class_id = ". $r["class_id"];
			        $cond2 .= ($r["atrb_id"] ? " and atrb_id = ". $r["atrb_id"] : "").
		            " )";
			    }
			    $cond2 .= ")";
			}
			$cond2 .= " order by order_no";
	        $sel2 = select_from_table($this->_db_con, $sql2, $cond2, $this->file_name);
			if ($sel2 == 0) {
	            pg_close($this->_db_con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}
			$arr_atrb = array();
			while ($row2 = pg_fetch_array($sel2)) {
				$tmp_atrb_id = $row2["atrb_id"];
				$tmp_atrb_nm = $row2["atrb_nm"];
				array_push($arr_atrb, array("id" => $tmp_atrb_id, "name" => $tmp_atrb_nm));

				$sql3 = "select dm.dept_id, dm.dept_nm from deptmst dm".
		        " left outer join atrbmst am on (am.atrb_id = dm.atrb_id)".
		        " left outer join classmst cm on (cm.class_id = am.class_id)";
		        $cond3 = "where dm.atrb_id = '$tmp_atrb_id' and dm.dept_del_flg = 'f' ";
				if (count($timecard_tantou_branches) > 0) {
				    $cond3 .= " and (1 <> 1";
				    foreach ($timecard_tantou_branches as $r) {
				        $cond3 .= " or (am.class_id = ". $r["class_id"];
				        $cond3 .= ($r["atrb_id"] ? " and dm.atrb_id = ". $r["atrb_id"] : "");
				        $cond3 .= ($r["dept_id"] ? " and dm.dept_id = ". $r["dept_id"] : "").
		                " )";
				    }
				    $cond3 .= ")";
				}
				$cond3 .= " order by dm.order_no";
		        $sel3 = select_from_table($this->_db_con, $sql3, $cond3, $this->file_name);
				if ($sel3 == 0) {
					pg_close($this->_db_con);
					echo("<script type='text/javascript' src='./js/showpage.js'></script>");
					echo("<script language='javascript'>showErrorPage(window);</script>");
					exit;
				}
				$arr_dept = array();
				while ($row3 = pg_fetch_array($sel3)) {
					$tmp_dept_id = $row3["dept_id"];
					$tmp_dept_nm = $row3["dept_nm"];
					array_push($arr_dept, array("id" => $tmp_dept_id, "name" => $tmp_dept_nm));
				}
				array_push($arr_atrbdept, array("atrb_id" => $tmp_atrb_id, "depts" => $arr_dept));
			}
			array_push($arr_clsatrb, array("class_id" => $tmp_class_id, "atrbs" => $arr_atrb));
		}
		return array($arr_cls, $arr_clsatrb, $arr_atrbdept);
    }

}
?>