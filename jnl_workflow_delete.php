<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_authority.php");
require("about_session.php");
require_once("jnl_application_workflow_common_class.php");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// ワークフロー権限のチェック
$check_auth = check_authority($session, 81, $fname);
if ($check_auth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

//--------------------------------
// パラメータ
// $cate_ids
// $folder_ids
// $file_ids
// $file_ids_real
//--------------------------------
$cate_ids      = (!is_array($cate_ids)) ? array() : $cate_ids;
$folder_ids    = (!is_array($folder_ids)) ? array() : $folder_ids;
$file_ids      = (!is_array($file_ids)) ? array() : $file_ids;
$file_ids_real = (!is_array($file_ids_real)) ? array() : $file_ids_real;

// データベースに接続
$con = connect2db($fname);
$obj = new jnl_application_workflow_common_class($con, $fname);


// チェック処理
delete_check($con, $fname, $cate_ids, $folder_ids);


// トランザクションの開始
pg_query($con, "begin");

// 論理削除処理


// カテゴリの場合
foreach($cate_ids as $cate_id)
{
	update_delflg_wkfwcatemst($con, $fname, $cate_id);
}

// フォルダの場合
foreach($folder_ids as $folder_id)
{
	update_delflg_wkfwfolder($con, $fname, $folder_id);
	update_delflg_wkfwtree($con, $fname, $folder_id);
}

// エイリアスワークフローの場合
foreach($file_ids as $file_id)
{
	update_delflg_wkfwmst($con, $fname, $file_id);

	update_delflg_wkfwaliasmng_for_alias($con, $fname, $file_id);
}

// 本体ワークフローの場合
foreach($file_ids_real as $file_id_real)
{
	update_delflg_wkfwmst_real($con, $fname, $file_id_real);
	$arr_alias_wkfw_id = $obj->get_alias_wkfw_id($file_id_real);
	foreach($arr_alias_wkfw_id as $alias)
	{
		$alias_wkfw_id = $alias["alias_wkfw_id"];
		update_delflg_wkfwmst($con, $fname, $alias_wkfw_id);
	}

	update_delflg_wkfwaliasmng_for_real($con, $fname, $file_id_real);
}

// トランザクションのコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 一覧画面を再表示
echo("<script type=\"text/javascript\">location.href = 'jnl_workflow_menu.php?session=$session'</script>");


// 削除チェック(カテゴリ、フォルダ用)
function delete_check($con, $fname, $arr_cate_id, $arr_folder_id)
{

	// カテゴリ
	foreach($arr_cate_id as $cate_id)
	{
		// フォルダ存在チェック
		$folder_num = get_wkfw_folder_cnt($con, $fname, $cate_id);

		// ワークフロー存在チェック
		$wkfw_num = get_wkfw_mst_cnt($con, $fname, $cate_id, "");

		if($folder_num > 0 || $wkfw_num > 0)
		{
			echo("<script type=\"text/javascript\">alert('フォルダまたはファイルが存在するため削除できません。');</script>");
			echo("<script type=\"text/javascript\">history.back();</script>");
			exit;
		}
	}

	// フォルダ
	foreach($arr_folder_id as $folder_id)
	{

		// フォルダ存在チェック
		$folder_num = get_wkfw_tree_cnt($con, $fname, $folder_id);

		// ワークフロー存在チェック
		$wkfw_num = get_wkfw_mst_cnt($con, $fname, "", $folder_id);

		if($folder_num > 0 || $wkfw_num > 0)
		{
			echo("<script type=\"text/javascript\">alert('フォルダまたはファイルが存在するため削除できません。');</script>");
			echo("<script type=\"text/javascript\">history.back();</script>");
			exit;
		}
	}

}

// フォルダカウント(カテゴリ用)
function get_wkfw_folder_cnt($con, $fname, $wkfw_type)
{
	$sql  = "select count(*) as cnt from jnl_wkfwfolder";
	$cond = "where wkfw_type = $wkfw_type and not wkfw_folder_del_flg";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0)
	{
		pg_close($con);
		ajax_server_erorr();
		exit;
	}

	$num = pg_fetch_result($sel, 0, "cnt");

	return $num;
}

// フォルダカウント(フォルダ用)
function get_wkfw_tree_cnt($con, $fname, $wkfw_folder_id)
{
	$sql  = "select count(*) as cnt from jnl_wkfwtree";
	$cond = "where not wkfw_tree_del_flg and wkfw_parent_id = $wkfw_folder_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0)
	{
		pg_close($con);
		ajax_server_erorr();
		exit;
	}

	$num = pg_fetch_result($sel, 0, "cnt");

	return $num;
}

// ワークフローカウント
function get_wkfw_mst_cnt($con, $fname, $wkfw_type, $wkfw_folder_id)
{
	$sql  = "select count(*) as cnt from jnl_wkfwmst";
	$cond = "where not wkfw_del_flg ";

	if($wkfw_type != "")
	{
		$cond .= "and wkfw_type = $wkfw_type ";
	}

	if($wkfw_folder_id != "")
	{
		$cond .= "and wkfw_folder_id = $wkfw_folder_id ";
	}

	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0)
	{
		pg_close($con);
		ajax_server_erorr();
		exit;
	}

	$num = pg_fetch_result($sel, 0, "cnt");

	return $num;
}


// カテゴリ論理削除
function update_delflg_wkfwcatemst($con, $fname, $wkfw_type)
{

/*

	$sql = "update jnl_wkfwcatemst set";
	$set = array("wkfwcate_del_flg");
	$setvalue = array("t");
	$cond = "where wkfw_type = $wkfw_type";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

*/

	$sql = "delete from jnl_wkfwcatemst ";
	$cond = "where wkfw_type = $wkfw_type";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) 
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	
	
}


// フォルダ論理削除
function update_delflg_wkfwfolder($con, $fname, $wkfw_folder_id)
{
	$sql = "update jnl_wkfwfolder set";
	$set = array("wkfw_folder_del_flg");
	$setvalue = array("t");
	$cond = "where wkfw_folder_id = $wkfw_folder_id";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// フォルダツリー論理削除
function update_delflg_wkfwtree($con, $fname, $wkfw_parent_id)
{
	$sql = "update jnl_wkfwtree set";
	$set = array("wkfw_tree_del_flg");
	$setvalue = array("t");
	$cond = "where wkfw_parent_id = $wkfw_parent_id";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// エイリアスワークフロー論理削除
function update_delflg_wkfwmst($con, $fname, $wkfw_id)
{
	$sql = "update jnl_wkfwmst set";
	$set = array("wkfw_del_flg");
	$setvalue = array("t");
	$cond = "where wkfw_id = $wkfw_id";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// 本体ワークフロー論理削除
function update_delflg_wkfwmst_real($con, $fname, $wkfw_id)
{
	$sql = "update jnl_wkfwmst_real set";
	$set = array("wkfw_del_flg");
	$setvalue = array("t");
	$cond = "where wkfw_id = $wkfw_id";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// エイリアス管理論理削除
function update_delflg_wkfwaliasmng_for_real($con, $fname, $real_wkfw_id)
{
	$sql = "update jnl_wkfwaliasmng set";
	$set = array("delete_flg");
	$setvalue = array("t");
	$cond = "where real_wkfw_id = $real_wkfw_id";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// エイリアス管理論理削除
function update_delflg_wkfwaliasmng_for_alias($con, $fname, $alias_wkfw_id)
{
	$sql = "update jnl_wkfwaliasmng set";
	$set = array("delete_flg");
	$setvalue = array("t");
	$cond = "where alias_wkfw_id = $alias_wkfw_id";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}


?>
