<?php
///*****************************************************************************
// 勤務シフト作成 | 管理帳票EXCEL出力 PHP5.1.6 ネイティブExcel
// duty_shift_mprint_excel.php
///*****************************************************************************
ob_start();
require_once("Cmx.php");
require_once('Cmx/Model/SystemConfig.php');
require_once("about_comedix.php");
require_once("duty_shift_common_class.php");
require_once("duty_shift_mprint_common_class.php");
require_once("get_values.ini");
require_once("duty_shift_menu_excel_5_workshop.php");
require_once("work_admin_csv_download_common.php");
require_once("atdbk_common_class.php");
require_once("timecard_paid_hol_hour_class.php");
require_once("timecard_bean.php");
require_once("calendar_name_class.php");

///-----------------------------------------------------------------------------
//画面名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
//セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}
///-----------------------------------------------------------------------------
//DBコネクション取得
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
$obj_mprint = new duty_shift_mprint_common_class($con, $fname);
//出勤表関連共通クラス
$atdbk_common_class = new atdbk_common_class($con, $fname);
//タイムカード情報の取得
$timecard_bean = new timecard_bean();
$timecard_bean->select($con, $fname);
//時間有休
$obj_hol_hour = new timecard_paid_hol_hour_class($con, $fname);
$obj_hol_hour->select();
//カレンダー名、所定労働時間履歴用
$calendar_name_class = new calendar_name_class($con, $fname);

//帳票名称
$arr_layout_mst = $obj_mprint->get_duty_shift_layout_mst_one($csv_layout_id);
$print_title = $arr_layout_mst["layout_name"];
if ($shift_group_id == "-") {
    $group_name = "全て";
}
else {
    $group_array = $obj->get_duty_shift_group_array($shift_group_id, "", array());
    $group_name = $group_array[0]["group_name"];
}
    //期間
    $start_date = $select_start_yr.$select_start_mon.$select_start_day;
    $end_date = $select_end_yr.$select_end_mon.$select_end_day;
    $start_date_parttime = $start_date;
    $end_date_parttime = $end_date;

    $arr_config = get_timecard_csv_config($con, $fname, $csv_layout_id, "2");
    $header_flg = $arr_config["csv_header_flg"];
    $csv_ext_name = $arr_config["csv_ext_name"];

    //データ取得、期間が1ヶ月以外の場合等
$arr_emp_id_layout = get_arr_emp_id_for_mprint($con, $fname, $shift_group_id, $arr_config, 0, 0, $start_date, $end_date, $total_flg);

    $arr_csv_list = get_csv_list($con, $fname, $csv_layout_id, "2");
     //CSV休日出勤回数出力職種ID取得
    $arr_job_id_list = get_job_id_list($con, $fname, $csv_layout_id, "2");

    $closing = $timecard_bean->closing;
$closing_month_flg = $timecard_bean->closing_month_flg;
$arrData = get_attendance_book_csv($con, $emp_id_list,
        $start_date, $end_date,
        $start_date_parttime, $end_date_parttime,
        $c_yyyymm, $ovtmscr_tm, $fname, $arr_reason_id, $arr_disp_flg,
        $group_id, $atdbk_common_class, $arr_csv_list, $header_flg, $arr_job_id_list, $closing, $closing_month_flg, $arr_emp_id_layout, $timecard_bean, $shift_group_id, $csv_layout_id, $obj_hol_hour, $calendar_name_class, "2", "0", $total_flg);


///-----------------------------------------------------------------------------
// Excelオブジェクト生成
///-----------------------------------------------------------------------------
$excelObj = new ExcelWorkShop();

///-----------------------------------------------------------------------------
// Excel列名
///-----------------------------------------------------------------------------
$excelColumnName = array();
for ($i=0; $i<256; $i++) {
    if ($i < 26) {
        $wkCol = chr(65 + $i); //"A"-"Z"
    }
    else {
        $wkCol = chr(64 + ($i / 26)).chr(65 + ($i % 26)); //"AA"-"IV"
    }
    $excelColumnName[] = $wkCol;
}

//帳票名

$duty_yyyy = date("Y");
$duty_mm = date("m");

///-----------------------------------------------------------------------------
//ファイル名
///-----------------------------------------------------------------------------
$filename = $start_date."_".$end_date."_".$print_title;
//「全て」でない場合
if ($shift_group_id != "-") {
    $filename .= "_".$group_name;
}
$filename .= ".xls";

///-----------------------------------------------------------------------------
// Excel シート名
///----------------------------------------------------------------------------
$excelObj->SetSheetName($start_date."_".$end_date);


showHeader($err_msg_2,
        $group_name,
        $select_start_yr,
        $select_start_mon,
        $select_start_day,
        $select_end_yr,
        $select_end_mon,
        $select_end_day,
        $print_title,
        $excelColumnName,
        $excelObj);

$excelTitleColPos = 0;

//幅
$defWidth = 8.38;

$row = 4;
for ($empIdx=0; $empIdx<count($arrData); $empIdx++) {

    $excelTitleColPos = 0;
    for ($i=0; $i<count($arr_csv_list); $i++) {

        $excelObj->SetArea($excelColumnName[$excelTitleColPos].$row);
        $excelObj->SetValueJP($arrData[$empIdx][$excelTitleColPos]);
        $excelObj->SetColDim($excelColumnName[$excelTitleColPos],$defWidth);
        //職員ID、氏名、所属は左詰めとする
        if ($empIdx != 0 && (($arr_csv_list[$i] >= "1_0" && $arr_csv_list[$i] <= "1_4") || $arr_csv_list[$i] == "1_7")) {
            $wk_align = "LEFT";
        }
        else {
            $wk_align = "CENTER";
        }
        $excelObj->SetPosH($wk_align);
        $excelTitleColPos ++;

    }
    $row++;
}
//
$excelObj->SetArea("A4:". $excelColumnName[$excelTitleColPos-1].($row-1) );
$excelObj->SetBorder( "THIN" , "000000" , 'all' );
$excelObj->SetPosV('CENTER');

///-----------------------------------------------------------------------------
//データEXCEL出力
///-----------------------------------------------------------------------------

$excel_mleft = "2.5";
$excel_mright = "2.5";
$excel_mtop = "2.5";
$excel_mbottom = "2.5";
$excel_mhead = "2.5";
$excel_mfoot = "2.0";

ob_clean();

$excel_paper_size = $arr_config["paper_size"];
if ($arr_config["orientation"] == "1") {
    $orientation = "virtical";
}
else {
    $orientation = "horizontal";
}
$excelObj->PageSetUp($orientation); // 向き
$excelObj->PaperSize( $excel_paper_size ); // 用紙サイズ
// 倍率と自動或いは利用者指定
//$excelObj->PageRatio( $excel_setfitpage , $excel_ratio );

// 余白、左右上下頭足
$excelObj->SetMargin($excel_mleft,$excel_mright,$excel_mtop,$excel_mbottom,$excel_mhead,$excel_mfoot);

//繰り返し印刷の指定（１−４行目をページの先頭に印刷）
$excelObj->SetRowsToRepeatAtTopByStartAndEnd(1, 4);

$create_date = "作成日:".date("Y")."年".date("n")."月".date("j")."日";
$excelObj->SetPrintHeader("&R$create_date");
$excelObj->SetPrintFooter("");

// ダウンロード
$ua = $_SERVER['HTTP_USER_AGENT'];
$filename = mb_convert_encoding($filename, 'UTF-8', mb_internal_encoding());

if(!preg_match("/safari/i",$ua)){
    header('Content-Type: application/octet-stream');
}
else {
    header('Content-Type: application/vnd.ms-excel');
}

if (preg_match("/MSIE/i", $ua)) {
    if (strlen(rawurlencode($filename)) > 21 * 3 * 3) {
        $filename = mb_convert_encoding($filename, "SJIS-win", "UTF-8");
        $filename = str_replace('#', '%23', $filename);
    }
    else {
        $filename = rawurlencode($filename);
    }
}
elseif (preg_match("/Trident/i", $ua)) {// IE11
    $filename = rawurlencode($filename);
}
elseif (preg_match("/chrome/i", $ua)) {// Google Chromeには『Safari』という字が含まれる
}
elseif (preg_match("/safari/i", $ua)) {// Safariでファイル名を指定しない場合
    $filename = "";
}
header('Content-Disposition: attachment; filename="'.$filename.'"');
header('Cache-Control: max-age=0');

$excelObj->OutPut();

ob_end_flush();

pg_close($con);


/*************************************************************************/
//
// シート初期値
//
/*************************************************************************/

/**
 * 見出し部出力
 *
 * @param mixed $err_msg エラーメッセージ
 * @param mixed $group_name シフトグループ名
 * @param mixed $start_yyyy 開始年
 * @param mixed $start_mm 開始月
 * @param mixed $start_dd 開始日
 * @param mixed $end_yyyy 終了年
 * @param mixed $end_mm 終了月
 * @param mixed $end_dd 終了日
 * @param mixed $print_title 見出し
 * @param mixed $excelColumnName Excel列名
 * @param mixed $excelObj Excelオブジェクト
 * @return なし
 *
 */
function showHeader($err_msg,
    $group_name,
    $start_yyyy,
    $start_mm,
    $start_dd,
    $end_yyyy,
    $end_mm,
    $end_dd,
    $print_title,
    $excelColumnName,
    $excelObj){

    //デフォルト幅でA4縦のサイズになる件数
    $wk_colspan = 9;

    // 行の高さ
    $excelObj->SetRowDim('1' , 13.5);
    $excelObj->SetRowDim('2' , 22.5);
    $excelObj->SetRowDim('3' , 13.5);
    $excelObj->SetRowDim('4' , 16.5);
    if ($err_msg != "") {

        $excelObj->SetArea('A3');
        $excelObj->SetPosH('CENTER'); // ←中央→
        $excelObj->SetPosV('CENTER'); // ↑↓中央
        $excelObj->SetCharColor('FF0000'); // 赤文字
        $excelObj->SetValueJP($err_msg);

        return;
    }

    $wk_title = $start_yyyy."年";
    $wk_title .= intval($start_mm)."月";
    $wk_title .= intval($start_dd)."日〜";
    $wk_title .= $end_yyyy."年";
    $wk_title .= intval($end_mm)."月";
    $wk_title .= intval($end_dd)."日";

    $excelObj->SetArea('A2'); // セル位置
    //タイトル、期間、グループ名の表示
    $excelObj->SetValueJP($print_title."　".$wk_title."　　".$group_name);
    $excelObj->SetFont(mb_convert_encoding('ＭＳ Ｐゴシック','UTF-8','EUC-JP'),11);

    $excelCellPositionR = $excelColumnName[$wk_colspan - 1] . '2';	// colspan数の代わりにセルの左端から右までの位置
    $excelObj->CellMerge('A2:'.$excelCellPositionR);		// セル結合
    $excelObj->SetPosH('LEFT'); // ←左→
    $excelObj->SetPosV('CENTER'); // ↑↓中央

    return ;

}
