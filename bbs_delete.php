<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require_once("Cmx.php");
require("about_session.php");
require("about_authority.php");
require("get_values.ini");
require_once("aclg_set.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$auth_id = ($path == "2") ? 62 : 1;
$check_auth = check_authority($session, 1, $fname);
if ($check_auth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// トランザクションを開始
pg_query($con, "begin");

// 自己削除かどうかチェック
$sql = "select emp_id from bbs";
$cond = "where bbsthread_id = $theme_id and bbs_id = $post_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$reg_emp_id = pg_fetch_result($sel, 0, "emp_id");
$emp_id = get_emp_id($con, $session, $fname);

// 投稿情報を論理削除
$sql = "update bbs set";
$set = array("bbs_del_flg");
$setvalue = array("t");
if ($reg_emp_id == $emp_id) {
	$set[] = "del_by_self";
	$setvalue[] = "t";
}
$cond = "where bbsthread_id = $theme_id and bbs_id = $post_id";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 添付ファイル情報を削除
$sql = "delete from bbsfile";
$cond = "where bbsthread_id = $theme_id and bbs_id = $post_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 添付ファイルの削除
foreach (glob("bbs/{$theme_id}_{$post_id}_*.*") as $tmpfile) {
	unlink($tmpfile);
}

// 30分以上前に保存された一時ファイルを削除
foreach (glob("bbs/tmp/*.*") as $tmpfile) {
	if (time() - filemtime($tmpfile) >= 30 * 60) {
		unlink($tmpfile);
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 一覧画面に遷移
if ($path == "2") {
	echo("<script type=\"text/javascript\">location.href = 'bbs_admin_menu.php?session=$session&theme_id=$theme_id&sort=$sort&show_content=$show_content';</script>");
} else {
	echo("<script type=\"text/javascript\">location.href = 'bbsthread_menu.php?session=$session&theme_id=$theme_id&sort=$sort&show_content=$show_content';</script>");
}
?>
