<?
//ini_set( 'display_errors', 1 );
require_once("cl_common_log_class.inc");
require_once("cl_common_apply.inc");
require_once(dirname(__FILE__) . "/Cmx.php");
require_once('MDB2.php');
require_once('./cl/common/cl_workflow_api.php');


$log = new cl_common_log_class(basename(__FILE__));
$log->info(basename(__FILE__)." START");
?>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" action="ladder_menu.php" method="post">
<input type="hidden" name="apply_title" value="<? echo($apply_title); ?>">
<input type="hidden" name="content" value="<? echo($content); ?>">

<?
foreach ($filename as $tmp_filename) {
	echo("<input type=\"hidden\" name=\"filename[]\" value=\"$tmp_filename\">\n");
}
foreach ($file_id as $tmp_file_id) {
	echo("<input type=\"hidden\" name=\"file_id[]\" value=\"$tmp_file_id\">\n");
}

for ($i = 1; $i <= $approve_num; $i++) {
	$varname = "regist_emp_id$i";
	echo("<input type=\"hidden\" name=\"regist_emp_id$i\" value=\"{$$varname}\">\n");
}

for ($i = 1; $i <= $precond_num; $i++) {
	$varname = "precond_apply_id$i";
	echo("<input type=\"hidden\" name=\"precond_apply_id$i\" value=\"{$$varname}\">\n");
}

for ($i = 1; $i <= $precond_num; $i++) {
	$varname = "precond_wkfw_id$i";
	echo("<input type=\"hidden\" name=\"precond_wkfw_id$i\" value=\"{$$varname}\">\n");
}


//for ($i = 1; $i <= $approve_num; $i++) {
//	$varname = "approve_name$i";
//	echo("<input type=\"hidden\" name=\"approve_name$i\" value=\"{$$varname}\">\n");
//}

?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="wkfw_id" value="<? echo($wkfw_id); ?>">
<input type="hidden" name="wkfw_type" value="<? echo($wkfw_type); ?>">
<input type="hidden" name="approve_num" value="<? echo($approve_num); ?>">
<input type="hidden" name="precond_num" value="<? echo($precond_num); ?>">
<input type="hidden" name="back" value="t">
<input type="hidden" name="apply_title_disp_flg" value="<?=$apply_title_disp_flg?>">
<input type="hidden" name="notice_emp_id" id="notice_emp_id" value="<?=$notice_emp_id?>">
<input type="hidden" name="notice_emp_nm" id="notice_emp_nm" value="<?=$notice_emp_nm?>">
<input type="hidden" name="wkfw_history_no" value="<?=$wkfw_history_no?>">
<input type="hidden" name="wkfwfile_history_no" value="<?=$wkfwfile_history_no?>">
<input type="hidden" name="short_wkfw_name" value="<?=$short_wkfw_name?>">
</form>
<?

$fname=$PHP_SELF;
$log->debug('$fname:'.$fname,__FILE__,__LINE__);

require_once("./about_session.php");
require_once("about_authority.php");
require_once("get_values.ini");
require_once("cl_application_workflow_common_class.php");
require_once("cl_common.ini");
require_once("cl_common_apply.inc");

//------------------------------------------------------------------------------
// セッションのチェック
//------------------------------------------------------------------------------
$log->debug('セッションのチェック開始',__FILE__,__LINE__);
$session = qualify_session($session, $fname);
if ($session == "0") {
	$log->error('セッションのチェックエラー',__FILE__,__LINE__);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
$log->debug('セッションのチェック終了',__FILE__,__LINE__);

//------------------------------------------------------------------------------
// 決裁・申請権限のチェック
//------------------------------------------------------------------------------
$checkauth = check_authority($session, $CAS_MENU_AUTH, $fname);
if ($checkauth == "0") {
	$log->error('決裁・申請権限のチェックエラー',__FILE__,__LINE__);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
$log->debug('決裁・申請権限のチェック終了',__FILE__,__LINE__);

//------------------------------------------------------------------------------
// 入力チェック
//------------------------------------------------------------------------------
$log->debug('入力チェック開始',__FILE__,__LINE__);
if (strlen($apply_title) > 80) {
	$log->error('入力チェックエラー',__FILE__,__LINE__);
	echo("<script type=\"text/javascript\">alert('表題が長すぎます。');</script>");
//	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
$log->debug('入力チェック終了',__FILE__,__LINE__);

//------------------------------------------------------------------------------
// 添付ファイルの確認
//------------------------------------------------------------------------------
if (!is_dir("cl/apply")) {
	mkdir("cl/apply", 0755);
}
if (!is_dir("cl/apply/tmp")) {
	mkdir("cl/apply/tmp", 0755);
}
for ($i = 0; $i < count($filename); $i++) {
	$tmp_file_id = $file_id[$i];
	$ext = strrchr($filename[$i], ".");

	$tmp_filename = "cl/apply/tmp/{$session}_{$tmp_file_id}{$ext}";
	if (!is_file($tmp_filename)) {
		$log->warn('添付ファイル削除警告',__FILE__,__LINE__);
		echo("<script language=\"javascript\">alert('添付ファイルが削除された可能性があります。\\n再度添付してください。');</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}
}
$log->debug('添付ファイルの確認終了',__FILE__,__LINE__);


//------------------------------------------------------------------------------
// データベースに接続
//------------------------------------------------------------------------------
$log->debug('データベースに接続(con)　開始',__FILE__,__LINE__);
$con = connect2db($fname);
$log->debug('データベースに接続(con)　終了',__FILE__,__LINE__);

$log->debug('データベースに接続(mdb2)　開始',__FILE__,__LINE__);
$mdb2 = MDB2::connect(CMX_DB_DSN);
if (PEAR::isError($mdb2)) {
	$log->error("MDB2オブジェクト取得エラー：".$mdb2->getDebugInfo(),__FILE__,__LINE__);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
$log->debug('データベースに接続(mdb2)　終了',__FILE__,__LINE__);

$obj = new cl_application_workflow_common_class($con, $fname);

// 2012/05/31 Yamagawa add(s)
$wkfw_api = new cl_workflow_api($mdb2);
$wkfw_api->setArrEmpmstBySession($session);
// 2012/05/31 Yamagawa add(e)

//------------------------------------------------------------------------------
// 職員情報取得
//------------------------------------------------------------------------------
// 2012/05/31 Yamagawa upd(s)
/*
$arr_empmst = $obj->get_empmst($session);
$emp_id        = $arr_empmst[0]["emp_id"];
$log->debug('★１　職員情報取得　$emp_id:'.$emp_id,__FILE__,__LINE__);
$emp_class     = $arr_empmst[0]["emp_class"];
$emp_attribute = $arr_empmst[0]["emp_attribute"];
$emp_dept      = $arr_empmst[0]["emp_dept"];
$emp_room      = $arr_empmst[0]["emp_room"];
*/
$arr_empmst 	= $wkfw_api->get_emp_info($session);
$emp_id			= $arr_empmst["emp_id"];
$emp_class		= $arr_empmst["emp_class"];
$emp_attribute	= $arr_empmst["emp_attribute"];
$emp_dept		= $arr_empmst["emp_dept"];
$emp_room		= $arr_empmst["emp_room"];
// 2012/05/31 Yamagawa upd(e)

// ログインユーザの職員IDを保持
$login_emp_id = $emp_id;


//------------------------------------------------------------------------------
// 申請の場合、未指定の承認者を許可する
//------------------------------------------------------------------------------
$apv_info=$wkfw_api->get_apv_info($_REQUEST);
if ($draft != "on") {
	/*
	$tmp_arr_apv = array();
	$tmp_apv_order = 0;
	$tmp_pre_apv_order = 0;
	for ($i = 1; $i <= $approve_num; $i++) {
		$tmp_regist_emp_id_var = "regist_emp_id$i";
		$tmp_st_div_var = "st_div$i";
		$tmp_parent_pjt_id_var = "parent_pjt_id$i";
		$tmp_child_pjt_id_var = "child_pjt_id$i";
		$tmp_apv_order_var = "apv_order$i";
		$tmp_apv_sub_order_var = "apv_sub_order$i";
		$tmp_multi_apv_flg_var = "multi_apv_flg$i";
		$tmp_next_notice_div_var = "next_notice_div$i";

		if ($$tmp_regist_emp_id_var != "") {
			if ($$tmp_apv_order_var != $tmp_pre_apv_order) {
				$tmp_apv_order++;
				$tmp_apv_sub_order = 1;
				$tmp_pre_apv_order = $$tmp_apv_order_var;
			} else {
				$tmp_apv_sub_order += 1;
			}

			if ($tmp_apv_sub_order == 2) {
				$tmp_arr_apv[count($tmp_arr_apv) - 1]["apv_sub_order"] = 1;
			}

			$tmp_arr_apv[] = array(
				"regist_emp_id" => $$tmp_regist_emp_id_var,
				"st_div" => $$tmp_st_div_var,
				"parent_pjt_id" => $$tmp_parent_pjt_id_var,
				"child_pjt_id" => $$tmp_child_pjt_id_var,
				"apv_order" => $tmp_apv_order,
				"apv_sub_order" => (($tmp_apv_sub_order == 1) ? null : $tmp_apv_sub_order),
				"multi_apv_flg" => $$tmp_multi_apv_flg_var,
				"next_notice_div" => $$tmp_next_notice_div_var
			);
		}

		unset($$tmp_regist_emp_id_var);
		unset($$tmp_st_div_var);
		unset($$tmp_parent_pjt_id_var);
		unset($$tmp_child_pjt_id_var);
		unset($$tmp_apv_order_var);
		unset($$tmp_apv_sub_order_var);
		unset($$tmp_multi_apv_flg_var);
		unset($$tmp_next_notice_div_var);
	}
	$approve_num = count($tmp_arr_apv);
	$i = 1;
	foreach ($tmp_arr_apv as $tmp_apv) {
		$tmp_regist_emp_id_var = "regist_emp_id$i";
		$tmp_st_div_var = "st_div$i";
		$tmp_parent_pjt_id_var = "parent_pjt_id$i";
		$tmp_child_pjt_id_var = "child_pjt_id$i";
		$tmp_apv_order_var = "apv_order$i";
		$tmp_apv_sub_order_var = "apv_sub_order$i";
		$tmp_multi_apv_flg_var = "multi_apv_flg$i";
		$tmp_next_notice_div_var = "next_notice_div$i";

		$$tmp_regist_emp_id_var = $tmp_apv["regist_emp_id"];
		$$tmp_st_div_var = $tmp_apv["st_div"];
		$$tmp_parent_pjt_id_var = $tmp_apv["parent_pjt_id"];
		$$tmp_child_pjt_id_var = $tmp_apv["child_pjt_id"];
		$$tmp_apv_order_var = $tmp_apv["apv_order"];
		$$tmp_apv_sub_order_var = $tmp_apv["apv_sub_order"];
		$$tmp_multi_apv_flg_var = $tmp_apv["multi_apv_flg"];
		$$tmp_next_notice_div_var = $tmp_apv["next_notice_div"];

		$i++;
	}
	unset($tmp_arr_apv);
*/
	$apv_info=$wkfw_api->edit_apv_info($apv_info);
}
$log->debug("apv_info：".print_r($apv_info,true));


//------------------------------------------------------------------------------
// トランザクション開始
//------------------------------------------------------------------------------
$log->debug("トランザクション開始(con) START",__FILE__,__LINE__);
pg_query($con, "begin");
$log->debug("トランザクション開始(con) END",__FILE__,__LINE__);

$log->debug("トランザクション開始(mdb2) START",__FILE__,__LINE__);
$mdb2->beginTransaction();
$log->debug("トランザクション開始(mdb2) END",__FILE__,__LINE__);

//------------------------------------------------------------------------------
// 新規申請(apply_id)採番
//------------------------------------------------------------------------------
//if($mode=="apply_regist"){
// 2012/05/31 Yamagawa upd(s)
/*
	require_once(dirname(__FILE__) . "/cl/model/sequence/cl_apply_id_seq_model.php");
	$cl_apply_id_model = new cl_apply_id_seq_model($mdb2,$login_emp_id);
	$apply_id=$cl_apply_id_model->getApplyId();
	$log->debug("■NEW apply_id:".$apply_id,__FILE__,__LINE__);
*/
$apply_id = $wkfw_api->get_apply_id();
// 2012/05/31 Yamagawa upd(e)
//}

$apply_no = null;
if($draft != "on")
{
	$apply_no = $wkfw_api->get_apply_no();
}

//------------------------------------------------------------------------------
// ●申請登録
//------------------------------------------------------------------------------
$log->debug('★２　●申請登録前　$emp_id:'.$emp_id,__FILE__,__LINE__);
// 2012/05/31 Yamagawa upd(s)
/*
$arr = array(
				"apply_id"				=> $apply_id,
				"wkfw_id"				=> $wkfw_id,
				"apply_content"			=> $content,
				"emp_id"				=> $emp_id,
				"apply_stat"			=> "0",
				"apply_date"			=> date("YmdHi"),
				//"delete_flg"			=> "f",
				"apply_title"			=> $apply_title,
				"re_apply_id"			=> null,
				"apv_fix_show_flg"		=> "t",
				"apv_bak_show_flg"		=> "t",
				"emp_class"				=> $emp_class,
				"emp_attribute"			=> $emp_attribute,
				"emp_dept"				=> $emp_dept,
				"apv_ng_show_flg"		=> "t",
				"emp_room"				=> $emp_room,
				"draft_flg"				=> ($draft == "on") ? "t" : "f",
				"wkfw_appr"				=> $wkfw_appr,
				"wkfw_content_type"		=> $wkfw_content_type,
				"apply_title_disp_flg"	=> $apply_title_disp_flg,
				"apply_no"				=> $apply_no,
				"notice_sel_flg"		=> $rslt_ntc_div2_flg,
				"wkfw_history_no"		=> ($wkfw_history_no == "") ? null : $wkfw_history_no,
				"wkfwfile_history_no"	=> ($wkfwfile_history_no == "") ? null : $wkfwfile_history_no
			 );
//$obj->regist_apply($arr);
require_once(dirname(__FILE__) . "/cl/model/workflow/cl_apply_model.php");
$cl_apply_model = new cl_apply_model($mdb2,$login_emp_id);
$cl_apply_model->insert($arr);
*/

// ワークフロー情報取得
$arr_wkfw = $wkfw_api->get_wkfw_info_by_req($_REQUEST);

// 申請登録
$wkfw_api->regist_apply(
	$apply_id
	, $wkfw_id
	, $content
	, $apply_title
	, $draft
	, $apply_no
	, $rslt_ntc_div2_flg
	, $wkfw_history_no
	, $wkfwfile_history_no
	, $arr_empmst
	, $arr_wkfw);
// 2012/05/31 Yamagawa upd(e)

$log->debug('★２　●申請登録後　$emp_id:'.$emp_id,__FILE__,__LINE__);

//------------------------------------------------------------------------------
// ●承認登録
//------------------------------------------------------------------------------
$apvteacher_info = $wkfw_api->get_apvteacher_info($_REQUEST);
$apvteacher_num = $_POST['apvteacher_num'];
// 2012/07/19 Yamagawa add(s)
$council_info = $wkfw_api->get_council_info($_REQUEST);
$council_num = $_POST['council_num'];
// 2012/07/19 Yamagawa add(e)
$log->debug("apvteacher_info：".print_r($apvteacher_info,true));

// 2012/07/19 Yamagawa upd(s)
//$wkfw_api->regist_approval_data($wkfw_id,$apply_id,$draft,$apv_info,$apvteacher_num,$apvteacher_info);
$wkfw_api->regist_approval_data(
	$wkfw_id
	,$apply_id
	,$draft
	,$apv_info
	,$apvteacher_num
	,$apvteacher_info
	,$council_num
	,$council_info
);
// 2012/07/19 Yamagawa upd(e)
/*
$log->debug('★３　●承認登録前　$emp_id:'.$emp_id,__FILE__,__LINE__);
$log->debug('approve_num：'.$approve_num,__FILE__,__LINE__);
require_once(dirname(__FILE__) . "/cl/model/workflow/cl_applyapv_model.php");
$cl_applyapv_model = new cl_applyapv_model($mdb2,$login_emp_id);
$apvteacher_num = $_POST['apvteacher_num'];
for($i=1; $i<=$approve_num; $i++)
{

	$varname = "apv_order$i";
	$apv_order = ($$varname == "") ? null : $$varname;

	if ($apv_order_key != $apv_order){
		$apv_sub_order_wk = 1;
		$apv_order_key = $apv_order;
	}

	$varname = "st_div$i";
	$st_div = $$varname;

	$varname = "multi_apv_flg$i";
	$multi_apv_flg = ($$varname == "") ? "f" : $$varname;

	$varname = "next_notice_div$i";
	$next_notice_div = ($$varname == "") ? null : $$varname;

	$varname = "parent_pjt_id$i";
	$parent_pjt_id = ($$varname == "") ? null : $$varname;

	$varname = "child_pjt_id$i";
	$child_pjt_id = ($$varname == "") ? null : $$varname;

	if ($st_div == 6 && $multi_apv_flg == "t" && $apvteacher_num > 1){
		$varname = "check_emp_id".$i."_";
		$arr_field_name = array_keys($_POST);
		foreach($arr_field_name as $field_name){
			$pos = strpos($field_name,$varname);
			if ($pos === 0){
				$apv_emp_id = $_POST[$field_name];

				// 所属、役職も登録する
				$infos = get_empmst($con, $apv_emp_id, $fname);

				$emp_class     = $infos[2];
				$emp_attribute = $infos[3];
				$emp_dept      = $infos[4];
				$emp_st        = $infos[6];
				$emp_room      = $infos[33];

				$arr = array(
					"wkfw_id"			 =>	$wkfw_id,
					"apply_id"			 =>	$apply_id,
					"apv_order"			 =>	$apv_order,
					//"emp_id"			 =>	$emp_id,
					"emp_id"			 =>	$apv_emp_id,
					"apv_stat"			 =>	"0",
					"apv_date"			 =>	"",
					//"delete_flg"		 =>	"f",
					"apv_comment"		 =>	"",
					"st_div"			 =>	$st_div,
					"deci_flg"			 =>	"t",
					"emp_class"			 =>	$emp_class,
					"emp_attribute"		 =>	$emp_attribute,
					"emp_dept"			 =>	$emp_dept,
					"emp_st"			 =>	$emp_st,
					"apv_fix_show_flg"	 =>	"t",
					"emp_room"			 =>	$emp_room,
					//"apv_sub_order"	 =>	$apv_sub_order,
					"apv_sub_order"		 =>	$apv_sub_order_wk,
					"multi_apv_flg"		 =>	$multi_apv_flg,
					"next_notice_div"	 =>	$next_notice_div,
					"parent_pjt_id"		 =>	$parent_pjt_id,
					"child_pjt_id"		 =>	$child_pjt_id,
					"other_apv_flg"		 =>	"f",
					"draft_flg"			 =>	($draft == "on") ? "t" : "f",
					"eval_content"		 =>	null
				);

				$cl_applyapv_model->insert($arr);

				$apv_sub_order_wk++;
			}
		}

	} else {
		$varname = "regist_emp_id$i";
		//$emp_id = ($$varname == "") ? null : $$varname;
		$apv_emp_id = ($$varname == "") ? null : $$varname;

		// 所属、役職も登録する
		//$infos = get_empmst($con, $emp_id, $fname);
		$infos = get_empmst($con, $apv_emp_id, $fname);

		$emp_class     = $infos[2];
		$emp_attribute = $infos[3];
		$emp_dept      = $infos[4];
		$emp_st        = $infos[6];
		$emp_room      = $infos[33];

		$arr = array(
			"wkfw_id"			 =>	$wkfw_id,
			"apply_id"			 =>	$apply_id,
			"apv_order"			 =>	$apv_order,
		//	"emp_id"			 =>	$emp_id,
			"emp_id"			 =>	$apv_emp_id,
			"apv_stat"			 =>	"0",
			"apv_date"			 =>	"",
			//"delete_flg"		 =>	"f",
			"apv_comment"		 =>	"",
			"st_div"			 =>	$st_div,
			"deci_flg"			 =>	"t",
			"emp_class"			 =>	$emp_class,
			"emp_attribute"		 =>	$emp_attribute,
			"emp_dept"			 =>	$emp_dept,
			"emp_st"			 =>	$emp_st,
			"apv_fix_show_flg"	 =>	"t",
			"emp_room"			 =>	$emp_room,
			//"apv_sub_order"	 =>	$apv_sub_order,
			"apv_sub_order"		 =>	$apv_sub_order_wk,
			"multi_apv_flg"		 =>	$multi_apv_flg,
			"next_notice_div"	 =>	$next_notice_div,
			"parent_pjt_id"		 =>	$parent_pjt_id,
			"child_pjt_id"		 =>	$child_pjt_id,
			"other_apv_flg"		 =>	"f",
			"draft_flg"			 =>	($draft == "on") ? "t" : "f",
			"eval_content"		 => null
		);

		$cl_applyapv_model->insert($arr);

		$apv_sub_order_wk++;
	}


}
$log->debug('★３　●承認登録後　$emp_id:'.$emp_id,__FILE__,__LINE__);
*/

//------------------------------------------------------------------------------
// ●承認者候補登録
//------------------------------------------------------------------------------
$log->debug('承認者候補登録　開始',__FILE__,__LINE__);

// 承認者候補取得
$arr_applyapvemp = $wkfw_api->get_applyapvemp_by_req($_REQUEST);

// 承認者候補登録
$wkfw_api->regist_applyapvemp($apply_id, $arr_applyapvemp);
/*
$log->debug('★４　●承認者候補登録前　$emp_id:'.$emp_id,__FILE__,__LINE__);
require_once(dirname(__FILE__) . "/cl/model/workflow/cl_applyapvemp_model.php");
$cl_applyapvemp_model = new cl_applyapvemp_model($mdb2,$login_emp_id);
for($i=1; $i <= $approve_num; $i++)
{
	$varname = "apv_order$i";
	$apv_order = $$varname;
	$log->debug('$apv_order:'.$apv_order,__FILE__,__LINE__);

	$varname = "pst_approve_num$apv_order";
	$pst_approve_num = $$varname;
	$log->debug('$pst_approve_num:'.$pst_approve_num,__FILE__,__LINE__);

	for($j=1; $j<=$pst_approve_num; $j++)
	{
		$varname = "pst_emp_id$apv_order";
		$varname .= "_$j";
		$pst_emp_id = $$varname;

		$log->debug('$pst_emp_id:'.$pst_emp_id,__FILE__,__LINE__);

		$varname = "pst_st_div$apv_order";
		$varname .= "_$j";
		$pst_st_div = $$varname;

		$log->debug('$pst_st_div:'.$pst_st_div,__FILE__,__LINE__);


		$varname = "pst_parent_pjt_id$apv_order";
		$varname .= "_$j";
	    $pst_parent_pjt_id = ($$varname == "") ? null : $$varname;

		$log->debug('$pst_parent_pjt_id:'.$pst_parent_pjt_id,__FILE__,__LINE__);

		$varname = "pst_child_pjt_id$apv_order";
		$varname .= "_$j";
    	$pst_child_pjt_id = ($$varname == "") ? null : $$varname;

		$log->debug('$pst_child_pjt_id:'.$pst_child_pjt_id,__FILE__,__LINE__);

		//$obj->regist_applyapvemp($apply_id, $apv_order, $j, $pst_emp_id, $pst_st_div, $pst_parent_pjt_id, $pst_child_pjt_id);
    	$arr = array(

			"apply_id"			 => $apply_id			,
			"apv_order"			 => $apv_order			,
			"person_order"		 => $j					,
			"emp_id"			 => $pst_emp_id			,
			"st_div"			 => $pst_st_div			,
			"parent_pjt_id"		 => $pst_parent_pjt_id	,
			"child_pjt_id"		 => $pst_child_pjt_id
		);
		$cl_applyapvemp_model->insert($arr);

	}
}
*/
$log->debug('承認者候補登録　終了',__FILE__,__LINE__);
//$log->debug('★４　●承認者候補登録後　$emp_id:'.$emp_id,__FILE__,__LINE__);

//------------------------------------------------------------------------------
// ●添付ファイル登録
//------------------------------------------------------------------------------
$log->debug('添付ファイル登録　開始',__FILE__,__LINE__);
$wkfw_api->regist_applyfile_data($apply_id, $filename);

/*
require_once(dirname(__FILE__) . "/cl/model/workflow/cl_applyfile_model.php");
$cl_applyfile_model = new cl_applyfile_model($mdb2,$login_emp_id);

$no = 1;
foreach ($filename as $tmp_filename)
{

	//$obj->regist_applyfile($apply_id, $no, $tmp_filename);
	$param=array(
			"apply_id"			=>	$apply_id		,
			"applyfile_no"		=>	$no				,
			"applyfile_name"	=>	$tmp_filename
	);
	$cl_applyfile_model->insert($param);

	$no++;
}
*/
$log->debug('添付ファイル登録　終了',__FILE__,__LINE__);

//------------------------------------------------------------------------------
// ●非同期・同期受信登録
//------------------------------------------------------------------------------
$log->debug('非同期・同期受信登録　開始',__FILE__,__LINE__);
if ($wkfw_appr == "2") {
	// 講師数取得
	$apvteacher_num = $_POST['apvteacher_num'];

	// 2012/07/19 Yamagawa add(s)
	// 審議会数取得
	$council_num = $_POST['council_num'];
	// 2012/07/19 Yamagawa add(e)

	// 非同期・同期受信取得
	// 2012/07/19 Yamagawa upd(s)
	//$arr_applyasyncrecv = $wkfw_api->get_applyasyncrecv_info($apvteacher_num, $_REQUEST);
	$arr_applyasyncrecv = $wkfw_api->get_applyasyncrecv_info($apvteacher_num, $_REQUEST, $council_num);
	// 2012/07/19 Yamagawa upd(e)

	// 非同期・同期受信登録
	$wkfw_api->regist_applyasyncrecv($apply_id, $arr_applyasyncrecv);
}
/*
require_once(dirname(__FILE__) . "/cl/model/workflow/cl_applyasyncrecv_model.php");
$cl_applyasyncrecv_model = new cl_applyasyncrecv_model($mdb2,$login_emp_id);

if($wkfw_appr == "2")
{
	$previous_apv_order = "";
	$arr_apv = array();
	$arr_apv_sub_order = array();
	// 2012/05/29 Yamagawa add(s)
	// 対象研修に対する講師人数
	$apvteacher_num = $_POST['apvteacher_num'];
	// 2012/05/29 Yamagawa add(e)
	for($i=1; $i <= $approve_num; $i++)
	{

		$varname = "apv_order$i";
		$apv_order = $$varname;

		// 2012/05/29 Yamagawa add(s)
		// 承認者区分
		$varname = "st_div$i";
		$st_div = $$varname;
		// 2012/05/29 Yamagawa add(e)

		// 2012/05/29 Yamagawa del(s)
		//$varname = "apv_sub_order$i";
		//$apv_sub_order = ($$varname == "") ? null : $$varname;
		// 2012/05/29 Yamaagwa del(e)

		$varname = "multi_apv_flg$i";
		$multi_apv_flg = ($$varname == "") ? "f" : $$varname;

		$varname = "next_notice_div$i";
		$next_notice_div = ($$varname == "") ? null : $$varname;

		if($previous_apv_order != $apv_order)
		{
			$arr_apv_sub_order = array();
			// 2012/05/29 Yamagawa add(s)
			$apv_sub_order_wk = 1;
			// 2012/05/29 Yamagawa add(e)
		}

		// 2012/05/29 Yamagawa upd(s)
		//$arr_apv_sub_order[] = $apv_sub_order;

		// 承認者が講師　かつ
		// 複数承認可　かつ　
		// 研修に対する講師が複数人設定されている場合
		// 選択した講師分登録する
		if ($st_div == 6 && $multi_apv_flg == "t" && $apvteacher_num > 1){
			$varname = "check_emp_id".$i."_";
			$arr_field_name = array_keys($_POST);
			foreach($arr_field_name as $field_name){
				$pos = strpos($field_name,$varname);
				if ($pos === 0){
					$arr_apv_sub_order[] = $apv_sub_order_wk;
					$apv_sub_order_wk++;
				}
			}
		} else {
			$arr_apv_sub_order[] = $apv_sub_order_wk;
			$apv_sub_order_wk++;
		}
		// 2012/05/29 Yamagawa upd(e)

		$arr_apv[$apv_order] = array("multi_apv_flg" => $multi_apv_flg, "next_notice_div" => $next_notice_div, "apv_sub_order" => $arr_apv_sub_order);

		$previous_apv_order = $apv_order;
	}

	$arr_send_apv_sub_order = array();
	foreach($arr_apv as $apv_order => $apv_info)
	{
		$multi_apv_flg = $apv_info["multi_apv_flg"];
		$next_notice_div = $apv_info["next_notice_div"];
		$arr_apv_sub_order = $apv_info["apv_sub_order"];

		foreach($arr_send_apv_sub_order as $send_apv_order => $arr_send_apv_sub)
		{
			// 非同期通知
			if($arr_send_apv_sub != null)
			{
				foreach($arr_send_apv_sub as $send_apv_sub_order)
				{
					foreach($arr_apv_sub_order as $recv_apv_sub_order)
					{
						//$obj->regist_applyasyncrecv($apply_id, $send_apv_order, $send_apv_sub_order, $apv_order, $recv_apv_sub_order);
						$param=array(
								"apply_id"				=>	$apply_id			,
								"send_apv_order"		=>	$send_apv_order		,
								"send_apv_sub_order"	=>	$send_apv_sub_order	,
								"recv_apv_order"		=>	$apv_order			,
								"recv_apv_sub_order"	=>	$recv_apv_sub_order	,
								"send_apved_order"		=>	null				,
								"apv_show_flg"			=>	"f"
						);
						$cl_applyasyncrecv_model->insert($param);
					}
				}
			}
			// 同期通知
			else
			{
				foreach($arr_apv_sub_order as $recv_apv_sub_order)
				{
					//$obj->regist_applyasyncrecv($apply_id, $send_apv_order, null, $apv_order, $recv_apv_sub_order);
					$param=array(
							"apply_id"				=>	$apply_id			,
							"send_apv_order"		=>	$send_apv_order		,
							"send_apv_sub_order"	=>	null				,
							"recv_apv_order"		=>	$apv_order			,
							"recv_apv_sub_order"	=>	$recv_apv_sub_order	,
							"send_apved_order"		=>	null				,
							"apv_show_flg"			=>	"f"
					);
					$cl_applyasyncrecv_model->insert($param);
				}
			}

		}
		$arr_send_apv_sub_order = array();

		// 非同期通知の場合
		if($multi_apv_flg == "t" && $next_notice_div == "1" && count($arr_apv_sub_order) > 1)
		{
			$arr_send_apv_sub_order[$apv_order] = $arr_apv_sub_order;
		}
		// 同期通知または権限並列通知の場合
		else if($multi_apv_flg == "t" && $next_notice_div != "1" && count($arr_apv_sub_order) > 1)
		{
			$arr_send_apv_sub_order[$apv_order] = null;
		}
	}
}
*/
$log->debug('非同期・同期受信登録　終了',__FILE__,__LINE__);

//------------------------------------------------------------------------------
// ●申請結果通知登録
//------------------------------------------------------------------------------
$log->debug('申請結果通知登録　開始',__FILE__,__LINE__);
if ($notice_emp_id != "") {
	$arr_notice_emp_id	= explode(",", $notice_emp_id);
	$arr_rslt_ntc_div	= explode(",", $rslt_ntc_div);
	$wkfw_api->regist_applynotice($apply_id, $arr_notice_emp_id, $arr_rslt_ntc_div);
}
/*
 	if($notice_emp_id != "")
{
	$arr_notice_emp_id = split(",", $notice_emp_id);
    $arr_rslt_ntc_div  = split(",", $rslt_ntc_div);

	require_once(dirname(__FILE__) . "/cl/model/workflow/cl_applynotice_model.php");
	$cl_applynotice_model = new cl_applynotice_model($mdb2,$login_emp_id);


    for($i=0; $i<count($arr_notice_emp_id); $i++)
	{
		//$obj->regist_applynotice($apply_id, $arr_notice_emp_id[$i], $arr_rslt_ntc_div[$i]);
		$param=array(
			"apply_id"		=>	$apply_id				,
			"recv_emp_id"	=>	$arr_notice_emp_id[$i]	,
			"confirmed_flg"	=>	"f"						,
			"send_emp_id"	=>	null					,
			"send_date"		=>	null					,
			"rslt_ntc_div"	=>	$arr_rslt_ntc_div[$i]
		);
		$cl_applynotice_model->insert($param);


	}
}
*/
$log->debug('申請結果通知登録　終了',__FILE__,__LINE__);

//------------------------------------------------------------------------------
// ●前提とする申請書(申請用)登録
//------------------------------------------------------------------------------
$log->debug('前提とする申請書(申請用)登録　開始',__FILE__,__LINE__);
$arr_applyprecond = $wkfw_api->get_applyprecond_info($_REQUEST);
$wkfw_api->regist_applyprecond($apply_id, $arr_applyprecond);
/*
require_once(dirname(__FILE__) . "/cl/model/workflow/cl_applyprecond_model.php");
$cl_applyprecond_model = new cl_applyprecond_model($mdb2,$login_emp_id);
for($i=0; $i<$precond_num; $i++)
{
	$order = $i + 1;
	$varname = "precond_wkfw_id$order";
	$precond_wkfw_id = $$varname;

	$varname = "precond_apply_id$order";
	$precond_apply_id = $$varname;

	//$obj->regist_applyprecond($apply_id, $precond_wkfw_id, $order, $precond_apply_id);

	$param=array(
		"apply_id"			=> $apply_id			,
		"precond_wkfw_id"	=> $precond_wkfw_id		,
		"precond_order"		=> $order				,
		"precond_apply_id"	=> $precond_apply_id
	);
	$cl_applyprecond_model->insert($param);


}
*/
$log->debug('前提とする申請書(申請用)登録　終了',__FILE__,__LINE__);


//------------------------------------------------------------------------------
// 添付ファイルの移動
//------------------------------------------------------------------------------
$log->debug('添付ファイルの移動　開始',__FILE__,__LINE__);
$wkfw_api->move_applyfile($apply_id ,$filename ,$file_id ,$session);
/*
for ($i = 0; $i < count($filename); $i++) {
	$tmp_file_id = $file_id[$i];
	$tmp_filename = $filename[$i];
	$tmp_fileno = $i + 1;
	$ext = strrchr($tmp_filename, ".");

	$tmp_filename = "cl/apply/tmp/{$session}_{$tmp_file_id}{$ext}";
	copy($tmp_filename, "cl/apply/{$apply_id}_{$tmp_fileno}{$ext}");
}
foreach (glob("cl/apply/tmp/{$session}_*.*") as $tmpfile) {
	unlink($tmpfile);
}
*/
$log->debug('添付ファイルの移動　終了',__FILE__,__LINE__);

// テンプレートの項目をパラメータに設定
$param = $wkfw_api->get_template_param($_REQUEST);

// ハンドラー呼出
$wkfw_api->load_handler($mode, $apply_id, $wkfw_id, $short_wkfw_name, $param);
/*
//------------------------------------------------------------------------------
// テンプレートの項目のみをパラメータに設定
//------------------------------------------------------------------------------
$param=template_parameter_pre_proccess($_POST);

//------------------------------------------------------------------------------
// パラメータに申請ＩＤを追加
//------------------------------------------------------------------------------
$log->debug('★９　●パラメータに申請ＩＤを追加前　$emp_id:'.$emp_id,__FILE__,__LINE__);
$param['apply_id'] = $apply_id;
$param['emp_id'] = $login_emp_id;

//------------------------------------------------------------------------------
// テンプレート独自の申請処理を呼び出す
//------------------------------------------------------------------------------
//ハンドラーモジュールロード
$log->debug('ハンドラーモジュールロード　開始　管理番号：'.$short_wkfw_name,__FILE__,__LINE__);
$handler_script=dirname(__FILE__) . "/cl/handler/".$short_wkfw_name."_handler.php";
$log->debug('$handler_script：'.$handler_script,__FILE__,__LINE__);
require_once($handler_script);
$log->debug('ハンドラーモジュールロード　終了',__FILE__,__LINE__);

$log->debug('★cl_application_submit.php ●$draft:'.$draft.' ●$mode:'.$mode.' ●$wkfw_id:'.$wkfw_id.' ●$apply_id:'.$apply_id,__FILE__,__LINE__);

// 2012/04/21 Yamagawa add(s)
// ハンドラー　クラス化対応
$log->debug('ハンドラークラスインスタンス化　開始',__FILE__,__LINE__);
$handler_name = $short_wkfw_name."_handler";
$handler = new $handler_name();
$log->debug('ハンドラークラスインスタンス化　終了',__FILE__,__LINE__);
// 2012/04/21 Yamagawa add(e)

//------------------------------------------------------------------------------
// 新規申請　申請の場合
//------------------------------------------------------------------------------
if( $mode == "apply_regist"){

	$log->debug('■ハンドラー新規申請　申請処理　開始',__FILE__,__LINE__);
	// 2012/04/21 Yamagawa upd(s)
	//NewApl_Regist($mdb2, $login_emp_id, $param);
	$handler->NewApl_Regist($mdb2, $login_emp_id, $param);
	// 2012/04/21 Yamagawa upd(e)
	$log->debug('■ハンドラー新規申請　申請処理　終了',__FILE__,__LINE__);

}
//------------------------------------------------------------------------------
// 新規申請　一時保存の場合
//------------------------------------------------------------------------------
elseif($mode == "draft_regist"){

	$log->debug('■ハンドラー新規申請　一時保存処理　開始',__FILE__,__LINE__);
	// 2012/04/21 Yamagawa upd(s)
	//NewApl_Draft($mdb2, $login_emp_id, $param);
	$handler->NewApl_Draft($mdb2, $login_emp_id, $param);
	// 2012/04/21 Yamagawa upd(e)
	$log->debug('■ハンドラー新規申請　一時保存処理　終了',__FILE__,__LINE__);

}
//------------------------------------------------------------------------------
// その他
//------------------------------------------------------------------------------
else{

}
*/
$log->debug('★９　●パラメータに申請ＩＤを追加後　$emp_id:'.$emp_id,__FILE__,__LINE__);

//------------------------------------------------------------------------------
// トランザクションをコミット
//------------------------------------------------------------------------------
pg_query($con, "commit");

$log->debug("トランザクション確定 START",__FILE__,__LINE__);
$mdb2->commit();
$log->debug("トランザクション確定 END",__FILE__,__LINE__);


// 2012/04/21 Yamagawa add(s)
//------------------------------------------------------------------------------
// 新規申請　申請の場合 自動承認処理暫定対応
//------------------------------------------------------------------------------
/*
if ($mode == "apply_regist") {

	if(in_array($short_wkfw_name, array("c400", "c410", "c420", "c430"))){
		//------------------------------------------------------------------------------
		// トランザクションを開始
		//------------------------------------------------------------------------------
		$log->debug("トランザクション開始(mdb2) START",__FILE__,__LINE__);
		$mdb2->beginTransaction();
		$log->debug("トランザクション開始(mdb2) END",__FILE__,__LINE__);

		//------------------------------------------------------------------------------
		// 自動承認処理（共通）
		//------------------------------------------------------------------------------
		$log->debug("自動承認処理モジュール呼出 START",__FILE__,__LINE__);
		require_once("cl_auto_application_approve.php");
		$log->debug("自動承認処理モジュール呼出 END",__FILE__,__LINE__);

		$log->debug("自動承認処理(共通) START",__FILE__,__LINE__);
		auto_application_approve($mdb2, $apply_id, $login_emp_id);
		$log->debug("自動承認処理(共通) END",__FILE__,__LINE__);

		//------------------------------------------------------------------------------
		// 自動承認処理（独自）
		//------------------------------------------------------------------------------
		$log->debug('■ハンドラー新規申請　自動承認処理　開始',__FILE__,__LINE__);
		$handler->AutoApv_Regist($mdb2, $login_emp_id, $param);
		$log->debug('■ハンドラー新規申請　自動承認処理　終了',__FILE__,__LINE__);

		//------------------------------------------------------------------------------
		// トランザクションをコミット
		//------------------------------------------------------------------------------
		$log->debug("トランザクション確定 START",__FILE__,__LINE__);
		$mdb2->commit();
		$log->debug("トランザクション確定 END",__FILE__,__LINE__);
	}

}
*/
// 2012/04/21 Yamagawa add(e)

//------------------------------------------------------------------------------
// データベース接続を閉じる
//------------------------------------------------------------------------------
pg_close($con);

$log->debug("データベース切断 START",__FILE__,__LINE__);
$mdb2->disconnect();
$log->debug("データベース切断 END",__FILE__,__LINE__);

if($draft == "on")
{
	echo("<script type=\"text/javascript\">location.href='ladder_menu.php?session=$session&apply_id=$apply_id';</script>");
}
else
{
	echo("<script type=\"text/javascript\">location.href='cl_application_apply_list.php?session=$session&mode=search&category=$wkfw_type&workflow=$wkfw_id';</script>");
}


$log->info(basename(__FILE__)." END");
?>
</body>
