<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
//ファイルの読み込み
require_once("about_session.php");
require_once("about_authority.php");

//ページ名
$fname = $PHP_SELF;

//セッションチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//権限チェック
$dept = check_authority($session,21,$fname);
if($dept == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//ＤＢのコネクション作成
$con = connect2db($fname);

//入力チェック
if($mst_name ==""){
	echo("<script language=\"javascript\">alert(\"マスタ名称を入力してください\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}

// マスタ名称、長さチェック
if (strlen($mst_name) > 40)
{
	echo("<script language=\"javascript\">alert(\"マスタ名称を全角20文字以内で入力してください\");</script>");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}

// 重複チェック
$sql  = "select count(*) as cnt from tmplitemmst";
$cond = "where mst_name = '$mst_name' and mst_cd != '$mst_cd'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$cnt = pg_fetch_result($sel, 0, "cnt");
if ($cnt > 0) {
	echo("<script language=\"javascript\">alert(\"マスタ名称「".$mst_name."」は既に使用されております。\");</script>");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}

// 内容、長さチェック
if (strlen($mst_content) > 100)
{
	echo("<script language=\"javascript\">alert(\"内容を全角50文字以内で入力してください\");</script>");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

//マスタ情報を更新する
$sql = "update tmplitemmst set";
$set = array("mst_name", "mst_content", "update_time", "update_emp_id");
$setvalue = array($mst_name, $mst_content, date("YmdHis"), $emp_id);
$cond = "where mst_cd = '$mst_cd'";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

pg_close($con);

echo("<script type=\"text/javascript\">location.href = 'bed_master_menu.php?session=$session&mst_cd=$mst_cd';</script>");

?>