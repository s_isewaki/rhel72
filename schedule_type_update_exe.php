<?
require("about_session.php");
require("about_authority.php");
require_once("Cmx.php");
require_once("aclg_set.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 13, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);


// 入力チェック
$arr_name1 = array();
$arr_name2 = array();
foreach ($_POST as $key => $val) {
	if (preg_match("/^schd(\d*)$/", $key, $matches)) {
		$tmp_id = $matches[1];
		if ($val == "") {
			echo("<script type=\"text/javascript\">alert('種別{$tmp_id}は必ず入力して下さい。');</script>");
			echo("<script type=\"text/javascript\">history.back();</script>");
			exit;
		}
		if (strlen($val) > 14) {
			echo("<script type=\"text/javascript\">alert('種別{$tmp_id}が長すぎます。');</script>");
			echo("<script type=\"text/javascript\">history.back();</script>");
			exit;
		}
		$arr_name1[$tmp_id] = $val;
	} 
}

// トランザクションを開始
pg_query($con, "begin");

// 種別レコードを更新（スケジュール）
foreach ($arr_name1 as $tmp_id => $tmp_name) {
	$sql = "update schdtype set";
	$set = array("type_name");
	$setvalue = array($tmp_name);
	$cond = "where type_id = '$tmp_id'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を切断
pg_close($con);

// 種別画面を再表示
echo("<script type=\"text/javascript\">location.href = 'schedule_type_update.php?session=$session'</script>");
?>
