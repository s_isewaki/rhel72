<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("get_values.php");
require_once("hiyari_common.ini");
require_once("Cmx.php");
require_once("aclg_set.php");

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
// 権限チェック
//====================================
$auth_chk = check_authority($session, 48, $fname);
if ($auth_chk == '0') { 
	showLoginPage();
	exit;
}


//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// 更新処理
if($mode == 'update') {
	// トランザクションの開始
	pg_query($con, "begin transaction");

	$arr_auth = split(",", $auths);
	for ( $i=0; $i<count($arr_auth); $i++) {

		$arr_btn_show_flg = array();
		for ( $j=0; $j<8; $j++) {
			
			$flg_idx = $j + 1;
			$flg_nm = $arr_auth[$i]."_B".$flg_idx;
			if ($$flg_nm != "") {
				$tmp_flg[$j] = "t";
			} 
            else {
				$tmp_flg[$j] = "f";
			}
			array_push($arr_btn_show_flg, $tmp_flg[$j]);
		}
		update_inci_btn_show_flg($con, $fname, $arr_auth[$i], $arr_btn_show_flg);
	}

	// トランザクションをコミット
	pg_query($con, "commit");

}

// データ取得
$arr_btn_flg = search_inci_btn_show_flg($con, $fname);


//==============================
//HTML出力
//==============================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title><?=$INCIDENT_TITLE?> | ボタン設定</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
<!--

function update() {

	mainform.mode.value = "update";
	document.mainform.submit();

}

// -->
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
<!--
.list {border-collapse:collapse;}
.list td {border:#35B341 solid 1px;}
// -->
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<form name="mainform" action="hiyari_btn_show_flg_register.php" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="mode" value="">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<!-- ヘッダー -->
<?
show_hiyari_header($session,$fname,true);
?>
<!-- ヘッダー -->

<img src="img/spacer.gif" width="1" height="5" alt=""><br>

<table width="985" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
<tr>
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td bgcolor="#F5FFE5">

<!-- ここから -->

<table width="100%" border="0" cellspacing="0" cellpadding="3"  class="list">
<tr>
<td width="250" align="center" bgcolor="#DFFFDC" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">担当者名称</font></td>
<td width="700" align="center" bgcolor="#DFFFDC" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示するボタン</font></td>
</tr>

<?
$auths = "";
$i = 0;
foreach($arr_btn_flg as $auth => $arr) {
	if($auth == 'GU' || is_usable_auth($auth,$fname,$con)) {
?>
<tr>
<td bgcolor="#FFFFFF"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><?=h($arr['auth_name'])?></font></td>
<td bgcolor="#FFFFFF">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
<input type="checkbox" name="<?=$auth?>_B1" value="t" <?if($arr['create_btn_show_flg'] == 't') echo(" checked");?>>作成&nbsp;&nbsp;
<input type="checkbox" name="<?=$auth?>_B2" value="t" <?if($arr['return_btn_show_flg'] == 't') echo(" checked");?>>返信&nbsp;&nbsp;
<input type="checkbox" name="<?=$auth?>_B3" value="t" <?if($arr['forward_btn_show_flg'] == 't') echo(" checked");?>>転送&nbsp;&nbsp;
<input type="checkbox" name="<?=$auth?>_B4" value="t" <?if($arr['print_btn_show_flg'] == 't') echo(" checked");?>>印刷&nbsp;&nbsp;
<input type="checkbox" name="<?=$auth?>_B5" value="t" <?if($arr['excel_btn_show_flg'] == 't') echo(" checked");?>>Excel出力
<input type="checkbox" name="<?=$auth?>_B6" value="t" <?if($arr['collect_excel_btn_show_flg'] == 't') echo(" checked");?>>(統計分析)Excel出力
<input type="checkbox" name="<?=$auth?>_B7" value="t" <?if($arr['address_btn_show_flg'] == 't') echo(" checked");?>>宛先
<input type="checkbox" name="<?=$auth?>_B8" value="t" <?if($arr['list_btn_show_flg'] == 't') echo(" checked");?>>同一人表示
</font>
</td>
</tr>
<?
		//変更対象の権限リスト作成
		if ($i != 0) {
			$auths .= ",";
		}
		$auths .= $auth;
		$i++;
	}
}
?>
</table>

<img src="img/spacer.gif" width="1" height="5" alt=""><br>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
<td align="right">
<input type="button" value="更新" onclick="update();">
</td>
</tr>
</table>


<table width="500" border="0" cellspacing="0" cellpadding="1">
<tr>
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#FF0000">
※Excel出力設定は報告ファイルのExcel出力のみ対象となります。
</font>
</td>
</tr>
</table>


<!-- ここまで -->

</td>
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<input type="hidden" name="auths" value="<?=$auths?>">

</td>
</tr>
</table>
</form>
</body>
</html>

<?
//==============================
//ボタン表示フラグ取得
//==============================
function search_inci_btn_show_flg($con, $fname) {

	$arr_inci = array();

	$sql = "select a.auth, case when a.auth = 'GU' then '一般ユーザ' else b.auth_name end, create_btn_show_flg, return_btn_show_flg, forward_btn_show_flg, print_btn_show_flg, excel_btn_show_flg, collect_excel_btn_show_flg, address_btn_show_flg,list_btn_show_flg from inci_btn_show_flg a left outer join inci_auth_mst b on a.auth = b.auth order by auth_rank is not null, auth_rank asc";
	$sel = select_from_table($con, $sql, "", $fname);

	if ($sel == 0)
    {
		pg_close($con);
		echo("<script type='text/javascript' src='js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

	while ($row = pg_fetch_array($sel))
	{
		$tmp_auth = $row["auth"];
		$tmp_auth_nm = $row["auth_name"];
		$tmp_flg1 = $row["create_btn_show_flg"];
		$tmp_flg2 = $row["return_btn_show_flg"];
		$tmp_flg3 = $row["forward_btn_show_flg"];
		$tmp_flg4 = $row["print_btn_show_flg"];
		$tmp_flg5 = $row["excel_btn_show_flg"];
		$tmp_flg6 = $row["collect_excel_btn_show_flg"];
		$tmp_flg7 = $row["address_btn_show_flg"];
                $tmp_flg8 = $row["list_btn_show_flg"];
		$tmp_arr = array(
            'auth_name'                  => $tmp_auth_nm, 
            'create_btn_show_flg'        => $tmp_flg1, 
            'return_btn_show_flg'        => $tmp_flg2, 
            'forward_btn_show_flg'       => $tmp_flg3, 
            'print_btn_show_flg'         => $tmp_flg4, 
            'excel_btn_show_flg'         => $tmp_flg5, 
            'collect_excel_btn_show_flg' => $tmp_flg6,
            'address_btn_show_flg'       => $tmp_flg7,
            'list_btn_show_flg'       => $tmp_flg8,

        );
		$arr_inci[$tmp_auth] = $tmp_arr;
	}
	return $arr_inci;

}

//==============================
//ボタン表示フラグ更新
//==============================
function update_inci_btn_show_flg($con, $fname, $auth, $arr_btn_show_flg) {
	$sql = "update inci_btn_show_flg set";
	$set = array(
        "create_btn_show_flg", 
        "return_btn_show_flg", 
        "forward_btn_show_flg", 
        "print_btn_show_flg", 
        "excel_btn_show_flg", 
        "collect_excel_btn_show_flg", 
        "address_btn_show_flg", 
        "list_btn_show_flg",
    );
	$setvalue = $arr_btn_show_flg;
	$cond = "where auth = '$auth'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);

	if ($upd == 0) {
        pg_exec($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

//==============================
//DBコネクション終了
//==============================
pg_close($con);

?>
