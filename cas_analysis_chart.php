<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("cas_common.ini");
require_once("get_cas_title_name.ini");

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$summary = check_authority($session, $CAS_MENU_AUTH, $fname);
if ($summary == "0") {
	showLoginPage();
	exit;
}

$cas_title = get_cas_title_name();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$cas_title?>｜グラフ分析表示</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" onload="moveBy(-25, 0); resizeBy(100, 0);">
<strong><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">グラフ分析</font></strong><br>
<?
include "charts/charts.php";

// 2011/12/19 Yamagawa add(s)
if ($nurse_type == "") {
	$nurse_type = "1";
}
// 2011/12/19 Yamagawa add(e)

// 2011/12/1 Yamagawa upd(s)
//echo InsertChart ( "charts/charts.swf", "charts/charts_library", "cas_analysis_chart_data.php?apply_id=".$apply_id."&level=".$level, 600, 500, "ffffff", false);
echo InsertChart ( "charts/charts.swf", "charts/charts_library", "cas_analysis_chart_data.php?apply_id=".$apply_id."&level=".$level."&nurse_type=".$nurse_type, 650, 500, "ffffff", false);
// 2011/12/1 Yamagawa upd(e)

?>

</body>
</html>

