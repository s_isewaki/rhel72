<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_class_name.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 23, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

$arr_class_name = get_class_name_array($con, $fname);

// 入力チェック
if ($class_id == "") {
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('{$arr_class_name[0]}を選択してください');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if ($atrb_name =="") {
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('{$arr_class_name[1]}名を入力してください');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($atrb_name) > 60) {
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('{$arr_class_name[1]}名が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($link_key) > 12) {
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('外部連携キーが長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}

// 所属IDを採番
$sql = "select max(atrb_id) from atrbmst";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$atrb_id = intval(pg_fetch_result($sel, 0, 0)) + 1;

// 表示順の決定
$sql = "select max(order_no) from atrbmst";
$cond = "where class_id = $class_id and not atrb_del_flg";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$order_no = intval(pg_fetch_result($sel, 0, 0)) + 1;

// トランザクションの開始
pg_query($con, "begin");

// 所属情報を登録
$sql  = "insert into atrbmst (atrb_id, class_id, atrb_nm, link_key, order_no) values (";
$content = array($atrb_id, $class_id, $atrb_name, $link_key, $order_no);
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// データベース接続を閉じる
pg_query($con, "commit");
pg_close($con);

// 組織一覧画面に遷移
echo("<script type=\"text/javascript\">location.href='class_menu.php?session={$session}&class_id={$class_id}';</script>");