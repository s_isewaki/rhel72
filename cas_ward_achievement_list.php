<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("show_select_values.ini");
require_once("cas_application_workflow_common_class.php");
require_once("cas_common.ini");
require_once("get_cas_title_name.ini");


//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,$CAS_MENU_AUTH,$fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$obj = new cas_application_workflow_common_class($con, $fname);


//====================================
// 初期処理
//====================================
// ログインユーザの職員IDを取得
$arr_empmst = $obj->get_empmst($session);
$emp_id = $arr_empmst[0]["emp_id"];


$arr_achievement_period = array();
$tmp_arr_achievement_period = $obj->get_cas_achievement_period($emp_id, "", $status, $level);

foreach($tmp_arr_achievement_period as $achievement_period)
{
    $arr_approval_emp_ids = split(",", $achievement_period["approval_emp_ids"]);

    $tmp_emp_name = "";
    for($i=0; $i<count($arr_approval_emp_ids); $i++)
    {
        if($tmp_emp_name != "")
        {
            $tmp_emp_name .= ",";
        } 
        $arr_emp = $obj->get_empmst_detail($arr_approval_emp_ids[$i]);
        $tmp_emp_name .= $arr_emp[0]["emp_lt_nm"]." ".$arr_emp[0]["emp_ft_nm"];
    }

    array_push($arr_achievement_period, array(
                                               "achievement_order" => $achievement_period["achievement_order"],
                                               "start_period" => $achievement_period["start_period"],
                                               "end_period" => $achievement_period["end_period"],
                                               "approval_emp_ids" => $achievement_period["approval_emp_ids"],
                                               "approval_emp_names" => $tmp_emp_name
                                              ));
}

$cas_title = get_cas_title_name();

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title><?=$cas_title?>｜病棟評価・達成期間選択</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<script type="text/javascript">


function initPage()
{
	<?
	foreach($arr_achievement_period as $achievement_period)
	{
        $arr_approval_emp_names = split(",", $achievement_period["approval_emp_names"]);
        $achievement_order = $achievement_period["achievement_order"];

        $approval_area = "";
        for($i=0; $i<count($arr_approval_emp_names); $i++)
        {
            if($approval_area != "")
		    {
			    $approval_area  .= ",";
		    }
            $name = $arr_approval_emp_names[$i];
		    $approval_area .= $name;
        }
    ?>
		document.getElementById('approval_area_<?=$achievement_order?>').innerHTML   = '<?=$approval_area?>';
    <?
	}
	?>
}



function set_achivement_period(achievement_order, start_year, start_month, end_year, end_month)
{
    opener.document.getElementById('start_year').value = start_year;
    opener.document.getElementById('start_month').value = start_month;
    opener.document.getElementById('end_year').value = end_year;
    opener.document.getElementById('end_month').value = end_month;
    
    opener.document.getElementById('achievement_order').value = achievement_order;
    opener.reload_apply_page();
    self.close();
}
</script>
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#A0D25A solid 1px;}
.list td td {border-width:0;}

table.block {border-collapse:collapse;}
table.block td {border:#A0D25A solid 1px;padding:1px;}
table.block td td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#A0D25A solid 0px;}

.non_in_list {border-collapse:collapse;}
.non_in_list td {border:0px;}

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="initPage();">
<form name="apply" action="#" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="emp_id" value="<?=$emp_id?>">
<input type="hidden" id="line_cnt" name="line_cnt" value="<?=$line_cnt?>">

<center>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#A0D25A">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>病棟評価・達成期間選択</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#A0D25A"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>

<?
if(count($arr_achievement_period) == 0)
{
?>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病棟評価表画面で達成期間を登録してください。</font>
</td>
</tr>
</table>
<?
}
else
{
?>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="list">
<tr bgcolor="#E5F6CD" align="center">
<td width="50">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">選択</font>
</td>
<td width="50">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">順番</font>
</td>
<td width="150">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">達成期間</font>
</td>
<td width="350">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">指導者</font>
</td>
</tr>



<?
foreach($arr_achievement_period as $period)
{
    $achievement_order = $period["achievement_order"];
    $start_date_y = substr($period["start_period"], 0, 4);
    $start_date_m = sprintf("%d", substr($period["start_period"], 4, 2));
    $end_date_y = substr($period["end_period"], 0, 4);
    $end_date_m = sprintf("%d", substr($period["end_period"], 4, 2));
?>
<tr height="50">
<td align="center">
<input type="button" value="選択" onclick="set_achivement_period('<?=$achievement_order?>', '<?=$start_date_y?>', '<?=$start_date_m?>', '<?=$end_date_y?>', '<?=$end_date_m?>');">
</td>
<td align="center">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$achievement_order?></font>
</td>

<td align="center">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?=$start_date_y?>/<?=$start_date_m?>〜<?=$end_date_y?>/<?=$end_date_m?>
</font>
</td>

<td>
<table width="100%" height="100%" border="0" cellspacing="1" cellpadding="0" class="non_in_list">
<tr>
<td width="100%" style="border:#A0D25A solid 1px;">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><span id="approval_area_<?=$achievement_order?>"></span></font>
</td>
</tr>
</table>
</td>

</tr>

<?
}
?>

</table>
<?
}
?>

<center>
</body>
</form>
</html>


<?
pg_close($con);
?>