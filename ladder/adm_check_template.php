<?php

require_once 'class/Base.php';
require_once 'class/CheckTemplate.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_mst_inside($emp);

//----------------------------------------------------------
// object
//----------------------------------------------------------
$check = new Ldr_CheckTemplate();

//----------------------------------------------------------
// 削除
//----------------------------------------------------------
if ($_POST['mode'] === 'remove') {
    if (!empty($_POST['id'])) {
        try {
            $check->remove($_POST['id']);
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
        }
    }
}

//----------------------------------------------------------
// データ
//----------------------------------------------------------
$list = $check->lists();

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign(Ldr_Util::get_common_info('check_template'));
$view->assign('emp', $emp);
$view->assign('list', $list);
$view->display('adm_check_template.tpl');
