<?php

require_once 'class/Base.php';
require_once 'class/Validation.php';
require_once 'class/Apply.php';
require_once 'class/Assessment.php';

//----------------------------------------------------------
// ���å����
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// ����
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_ladder($emp);

//----------------------------------------------------------
// object
//----------------------------------------------------------
$apply = new Ldr_Apply(array('apply_emp_id' => $_POST["target_emp_id"], 'accept_emp_id' => $emp->emp_id(), 'apply_id' => $_POST['id'], 'mode' => $_POST['menu']));
$target_emp = new Ldr_Employee($_POST["target_emp_id"]);
$as = new Ldr_Assessment();
$ladder = $apply->ladder();

//----------------------------------------------------------
// �ΰ�ꥹ��
//----------------------------------------------------------
$group1_list = $as->lists('group1', array('form7' => true, 'guideline_revision' => $apply->guideline_revision()));

// �����������
$form1 = $apply->find_form1($_POST['id']);

//----------------------------------------------------------
// �����ӥ塼
// ��[��λ]
// ��[ɾ�����Ԥ�] and (ǧ��Ѱ� or �ʲ�� or ����ɾ����)
//----------------------------------------------------------
if (
    $apply->get_status() === "0" // [��λ]
    || $apply->get_status() >= 9 && ($emp->is_committee() || $apply->is_facilitator($emp->emp_id()) || $apply->is_total_appraiser($emp->emp_id())) // [ɾ�����Ԥ�] and (ǧ��Ѱ� or �ʲ�� or ����ɾ����)
) {
    // �ֽ���
    if ($ladder['config']['flow'] === '1') {
        $form7 = $apply->lists_form7();
        $assessment_list = $apply->assessment_list_all();
        $form1['appr_list_all'][] = array('emp_id' => $target_emp->emp_id(), 'emp_name' => $target_emp->emp_name());
        $form1['appr_list_all'] = array_merge($form1['appr_list_all'], $form1['appr_list']);

        foreach ($group1_list as $key => $value) {
            $group1_list[$key]["item_cnt"] = count($assessment_list[$value['guideline_group1']]) * count($form1['appr_list_all']);
        }
        $active = "sec1";
        $tmp_tpl = "form_7view_all.tpl";
    }
    // ɸ�ࡦ��ŷƲ
    else {
        $form7 = $apply->lists_form7();
        $assessment_list = $apply->assessment_list_all();
        $form1['appr_list_all'][] = array('emp_id' => $target_emp->emp_id(), 'emp_name' => $target_emp->emp_name());
        if ($ladder['config']['anonymous'] !== '1' || $apply->is_total_appraiser($emp->emp_id()) || $emp->is_committee()) {
            $form1['appr_list_all'] = array_merge($form1['appr_list_all'], $form1['appr_list']);
        }
        $form1['appr_list_all'][] = array('emp_id' => 'total', 'emp_name' => '');

        foreach ($group1_list as $key => $value) {
            $group1_list[$key]["item_cnt"] = count($assessment_list[$value['guideline_group1']]) * count($form1['appr_list_all']);
        }

        // �ܿͥ�����
        $apply2 = new Ldr_Apply(array('apply_emp_id' => $_POST["target_emp_id"], 'apply_id' => $_POST['id'], 'mode' => 'apply'));
        $apply_form7 = $apply2->find_form7();

        // ����ɾ����
        $total_appraiser = $apply->total_appraiser();

        $tmp_tpl = "form_8view_total.tpl";
    }
}

//----------------------------------------------------------
// �����ԥӥ塼
// ��[��Ĺ������]����
// ��[Ʊνɾ����] and (��ǧ�� or ǧ��Ѱ���)
// ��[Ʊνɾ����]�ʹ� and (�ܿ� or ��ݡ���ɾ����)
//----------------------------------------------------------
else if (
    $apply->get_status() <= 7 // [��Ĺ������]����
    || $apply->get_status() === '8' && ($apply->is_approver($emp->emp_id()) || $emp->is_committee())  // [Ʊνɾ����] and (��ǧ�� or ǧ��Ѱ���)
    || ($apply->get_status() >= 8 && ($_POST["target_emp_id"] === $emp->emp_id() || $apply->is_reporter($emp->emp_id()))) // [Ʊνɾ����]�ʹ� and (�ܿ� or ��ݡ���ɾ����)
) {
    $apply->set_mode('apply');
    $form7 = $apply->find_form7();
    $assessment_list = $apply->assessment_list();

    $active = $group1_list[0]["guideline_group1"];
    $tmp_tpl = "form_7view_applicant.tpl";
}

//----------------------------------------------------------
// ɾ���ԥӥ塼
// �������ӥ塼�������ԥӥ塼�ʳ��ξ��
//----------------------------------------------------------
else {
    // �ֽ���
    if ($ladder['config']['flow'] === '1') {
        $form7 = $apply->find_form7();
        $assessment_list = $apply->assessment_list();

        $active = "sec1";
        $tmp_tpl = "form_7view_appraiser.tpl";
    }
    // ɸ�ࡦ��ŷƲ
    else {
        $form7 = $apply->find_form7();
        $assessment_list = $apply->assessment_list();
        $apply2 = new Ldr_Apply(array('apply_emp_id' => $_POST["target_emp_id"], 'apply_id' => $_POST['id'], 'mode' => 'apply'));
        $apply_form7 = $apply2->find_form7();
        $tmp_tpl = "form_7view_appraiser_normal.tpl";
    }
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $emp);
$view->assign('target_emp', $target_emp);
$view->assign(Ldr_Util::get_common_info($_POST['menu'], array('referer_url' => $_POST['owner_url'])));
$view->assign('apply', $apply);
$view->assign('ldr', $ladder);
$view->assign('form1', $form1);
$view->assign('form7', $form7);
$view->assign('apply_form7', $apply_form7);
$view->assign('total_appraiser', $total_appraiser);
$view->assign('group1_list', $group1_list);
$view->assign('assessment_list', $assessment_list);
$view->assign('active', $active);

//----------------------------------------------------------
// ����
//----------------------------------------------------------
if ($_POST['mode'] === 'print') {
    require_once 'class/PDF.php';
    $html = $view->fetch('print/' . $tmp_tpl);
    $pdf = new Ldr_PDF('P', 'mm', 'A4', true, 'UTF-8', false);
    $pdf->setHeaderTitle("[{$ladder['config']['form_7']['title']}] {$ladder['config']['form_7']['detailL']}");
    $pdf->setEmployee($target_emp);
    $pdf->setPrinter($session->emp());
    $pdf->AddPage();
    $pdf->writeHTML(to_utf8($html), true, 0, false, 0);
    $pdf->Output("form_7view.pdf", "I");
    exit;
}

//----------------------------------------------------------
// ɽ��
//----------------------------------------------------------
$view->display_with_fill($tmp_tpl, array('fdat' => $_POST));
