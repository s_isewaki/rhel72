<?php

require_once 'class/Base.php';
require_once 'class/Inside.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

$emp = $session->emp();
//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_ladder($emp);

//----------------------------------------------------------
// オブジェクト
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$inside = new Ldr_Inside($emp->emp_id());

//----------------------------------------------------------
// データ
//----------------------------------------------------------
$lists = $inside->lists_applied(array("inside_date_id" => $_GET['inside_date_id']));
//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
header('Cache-Control: no-cache, must-revalidate');
header("Cache-Control: post-check=0, pre-check=0", false);
header('Pragma: no-cache');
$view->assign('lists', $lists);

$view->display("modal/trn_emp_list.tpl");
