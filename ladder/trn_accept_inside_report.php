<?php

require_once 'class/Base.php';
require_once 'class/Inside.php';
require_once 'class/Pager.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('inside_report');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_inside($emp);

//----------------------------------------------------------
// object
//----------------------------------------------------------
$inside = new Ldr_Inside($emp->emp_id());
$perPage = Ldr_Const::SRH_PERPAGE;
$view = new Cmx_View('ladder/templates');
$mst = new Ldr_InsideMst();

//----------------------------------------------------------
// パラメータチェック
//----------------------------------------------------------
if (!isset($_POST['id'])) {
    cmx_log(basename($_SERVER["PHP_SELF"]) . " parameter error");
    header('Location: dashboard.php');
    exit;
}

//----------------------------------------------------------
// 研修マスタ情報
//----------------------------------------------------------
list($inside_id, $contents_id) = explode('_', $_POST['id']);
$train = $mst->find_contents($inside_id, $contents_id);

//----------------------------------------------------------
// 印刷
//----------------------------------------------------------
if ($_POST['mode'] === 'print') {
    require_once 'class/PDF.php';

    $cond = array_merge(array('id' => $contents_id, 'list_type' => 'report'), (array) $_SESSION['srh_condition']);

    $title = ($contents['before_work'] === 't') ? '院内研修 前課題' : '院内研修 後課題';

    $view->assign(array(
        'trn' => $train,
        'list' => $inside->lists_accept_by_contents($cond),
    ));

    $pdf = new Ldr_PDF('P', 'mm', 'A4', true, 'UTF-8', false);
    $pdf->setHeaderTitle($title);
    $pdf->setPrinter($session->emp());
    $pdf->AddPage();
    $html = $view->fetch('print/trn_accept_inside_work.tpl');
    $pdf->writeHTML(to_utf8($html), true, 0, false, 0);
    $pdf->Output('trn_accept_inside_work.pdf', "I");
    exit;
}

//----------------------------------------------------------
// 遷移状態判別
//----------------------------------------------------------
// ページャーから移動
if (Ldr_Pager::is_pager($_POST['move_pager'])) {
    //検索条件を取得
    $_SESSION['page_no'] = Ldr_Pager::get_page_no($_POST['page_no']);
    $srh_condition = $_SESSION['srh_condition'];
}
// 検索ボタン
else if ($_POST['search']) {
    $srh_condition["srh_work"] = $_POST['srh_work'];
    $srh_condition["srh_report"] = $_POST['srh_report'];

    $_POST["page_no"] = 1;

    // 検索条件を設定
    $_SESSION['srh_condition'] = $srh_condition;
}
// 外部から移動してきた場合
else {

    // セッション情報の初期化
    $_SESSION = array();
    $_POST["page_no"] = 1;
    $_SESSION['page_no'] = 1;
    $_SESSION['srh_condition'] = null;
    $srh_condition = null;

    if ($train['contents']['before_work'] === 't') {
        $_POST['srh_work'] = 'before';
    }
    else if ($train['contents']['after_work'] === 't') {
        $_POST['srh_work'] = 'after';
    }
    else {
        unset($_POST['srh_work']);
    }

    $srh_condition["srh_work"] = $_POST['srh_work'];
}

$_SESSION['ldr_return'] = null;
$_SESSION['menu'] = 'taccept_inside';

//----------------------------------------------------------
// 一覧
//----------------------------------------------------------
$cond = array_merge(array('id' => $contents_id, 'list_type' => 'report'), (array) $srh_condition);

$dataCount = $inside->lists_accept_by_contents_count($cond);
$current = Ldr_Pager::get_page_no($_POST['page_no'], $dataCount, $perPage);
$start = Ldr_Pager::calc_page_index($current, $perPage);

$data = array(
    'trn' => $train,
    'list' => $inside->lists_accept_by_contents($cond, $start, $perPage),
);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view->assign('emp', $emp);
$view->assign($data);
$view->assign(Ldr_Pager::get_pager_info($_POST['page_no'], $dataCount, $perPage));
$view->assign(Ldr_Util::get_common_info($_SESSION['menu']));
$view->display('trn_accept_inside_report.tpl');
