<?php

require_once 'class/Base.php';
require_once 'class/Outside.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
    exit;
}

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('adm_outside');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_mst_outside($emp);

//----------------------------------------------------------
// object
//----------------------------------------------------------
$outside = new Ldr_Outside($emp->emp_id());
$conf = new Ldr_Config();

//----------------------------------------------------------
// 保存
//----------------------------------------------------------
if ($_POST['btn_save'] === 'save' || $_POST['btn_save'] === 'copy') {

    $_POST['mst_only'] = true;

    // バリデーション
    $error = $outside->validate_apply($_POST);

    // エラーが無ければ保存
    if (empty($error)) {
        try {
            if ($_POST['rd_organizer'] === "1") {
                $_POST['organizer'] = "日本看護協会";
            }
            else if ($_POST['rd_organizer'] === "2") {
                $_POST['organizer'] = $conf->na_name();
            }
            $dt = $_POST;
            $dt['outside_id'] = $_POST['btn_save'] === 'copy' ? '' : $_POST['outside_id'];

            $outside->save($dt);
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
        }

        $_SESSION['ldr_return'] = true;
        header('Location:' . Ldr_Util::get_referer_url());
        exit;
    }
}
if ($_POST['btn_save'] === 'remove') {
    try {
        $outside->delete_mst($_POST['outside_id']);
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }

    $_SESSION['ldr_return'] = true;
    header('Location:' . Ldr_Util::get_referer_url());
    exit;
}
//----------------------------------------------------------
// データの取得
//----------------------------------------------------------
if ($_POST['mode'] === 'edit') {

    $fdat = $outside->find_mst($_POST['id']);

    $_SESSION['edit'] = $_POST['mode'];
}
//----------------------------------------------------------
// 区分、経験年数の初期値を設定
//----------------------------------------------------------
if ($_POST['mode'] === 'new' || $_POST['mode'] === 'apply') {
    $_SESSION['edit'] = $_POST['mode'];
    if (Validation::is_date($emp->nurse_date())) {
        $diff = Ldr_Util::diff_th_year($emp->nurse_date(), date("Y-m-d"));
        $career = $diff < 0 ? '' : $diff;
        $fdat['job_year'] = $career;
    }
    $fdat['type'] = 1;
}
//----------------------------------------------------------
// データの変換
//----------------------------------------------------------
$organizer_def = $conf->na_name();

if (!isset($fdat['mst_read'])) {
    if ($fdat["organizer"] === "日本看護協会") {
        $fdat["rd_organizer"] = "1";
        $fdat["organizer"] = "";
    }
    else if ($fdat["organizer"] === $organizer_def) {
        $fdat["rd_organizer"] = "2";
        $fdat["organizer"] = "";
    }
    else if (!is_null($fdat["organizer"])) {
        $fdat["rd_organizer"] = '3';
    }
    else {
        $fdat['rd_organizer'] = 1;
    }
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $emp);
$view->assign('edit', $_SESSION['edit']);
$view->assign('type', 'mst');
$view->assign('count', $fdat['count']);
$view->assign('hd_title', '院外研修マスタ');
$view->assign('organizer_def', $organizer_def);
$view->assign(Ldr_Util::get_common_info($_SESSION['menu']));

if (empty($error)) {
    $view->assign('readonly', $fdat['mst_read']);
    $view->display_with_fill('trn_outside_apply.tpl', array('fdat' => $fdat));
}
else {
    $view->assign('error', $error);
    $view->assign('readonly', $_POST['mst_read']);
    $view->display_with_fill('trn_outside_apply.tpl', array('fdat' => $_POST));
}
