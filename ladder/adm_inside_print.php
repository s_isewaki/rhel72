<?php

require_once 'class/Base.php';
require_once 'class/Pager.php';
require_once 'class/InsideMst.php';
require_once 'class/Guideline.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('adm_inside');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_mst_inside($session->emp());

//----------------------------------------------------------
// Obj
//----------------------------------------------------------
$mst = new Ldr_InsideMst();
$gl = new Ldr_Guideline();
$conf = new Ldr_Config();

//----------------------------------------------------------
// list
//----------------------------------------------------------
if ($_POST['id']) {
    $inside[] = $mst->find_date($_POST['id']);
}
else {
    $lists = $mst->lists(0, 0, $_SESSION['srh_condition']);
    foreach ($lists as $data) {
        $inside[] = $mst->find_date($data['inside_id']);
    }
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('group1', $gl->lists('group1', array('guideline_revision' => 1)));
$view->assign('emp', $session->emp());
$view->assign('data', $inside);
$view->assign('hospital_type', $conf->hospital_type());
$view->assign('ojt_name', $conf->ojt_name());
$view->assign('roman_num', Ldr_Const::get_roman_num());

require_once 'class/PDF.php';

$pdf = new Ldr_PDF('P', 'mm', 'A4', true, 'UTF-8', false);
$pdf->setHeaderTitle('院内研修');
$pdf->setPrinter($session->emp());
$html = $view->fetch('print/adm_inside.tpl');
$pdf->writeHTML(to_utf8($html), true, 0, false, 0);

$pdf->Output("adm_inside.pdf", "I");
