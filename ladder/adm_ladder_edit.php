<?php

require_once 'class/Base.php';
require_once 'class/Ladder.php';
require_once 'class/Guideline.php';
require_once 'class/Classmst.php';
require_once 'class/Jobmst.php';
require_once 'class/Statusmst.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_admin($session->emp());

//----------------------------------------------------------
// オブジェクト
//----------------------------------------------------------
$ladder = new Ldr_Ladder();
$gl = new Ldr_Guideline();

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('adm_ladder');

//----------------------------------------------------------
// 保存
//----------------------------------------------------------
switch ($_POST['btn_save']) {
    // 設定保存
    case 'btn_save':
        // バリデーション
        $error = $ladder->validate($_POST);

        // エラーが無ければ保存
        if (empty($error)) {
            // 保存処理
            try {
                $ladder->save($_POST);
            }
            catch (Exception $e) {
                cmx_log($e->getMessage());
                js_error_exit();
                exit;
            }

            // 一覧に戻る
            $_SESSION['ldr_return'] = true;
            header('Location: adm_ladder.php');
            exit;
        }
        break;

    // 対象保存
    case 'cover_save':
        // バリデーション
        $error = $ladder->cover_validate($_POST);

        // エラーが無ければ保存
        if (empty($error)) {
            // 保存処理
            try {
                $ladder->cover_save($_POST);
            }
            catch (Exception $e) {
                cmx_log($e->getMessage());
                js_error_exit();
                exit;
            }

            // 一覧に戻る
            $_SESSION['ldr_return'] = true;
            header('Location: adm_ladder.php');
            exit;
        }
        break;

    // 承認者保存
    case 'approver_save':
        // バリデーション
        $error = $ladder->approver_validate($_POST);

        // エラーが無ければ保存
        if (empty($error)) {
            // 保存処理
            try {
                $ladder->approver_save($_POST);
            }
            catch (Exception $e) {
                cmx_log($e->getMessage());
                js_error_exit();
                exit;
            }

            // 一覧に戻る
            $_SESSION['ldr_return'] = true;
            header('Location: adm_ladder.php');
            exit;
        }
        break;
}

//----------------------------------------------------------
// データの取得
//----------------------------------------------------------
if (!empty($_POST['mode'])) {
    $data = $ladder->find($_POST['id']);
}
else {
    $data = $_POST;
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $session->emp());
$view->assign(Ldr_Util::get_common_info('adm_ladder'));
$view->assign('revision_list', $gl->lists('revision'));
$view->assign('data', $data);
$view->assign('error', $error);

switch ($_POST['mode']) {
    case 'cover':
        $view->assign('class', new Ldr_Classmst());
        $view->assign('job', new Ldr_Jobmst());
        $view->assign('status', new Ldr_Statusmst());
        $view->display_with_fill('adm_ladder_cover.tpl', array('fdat' => $data));
        break;
    case 'approver':
        $view->assign('covered_lists', $ladder->covered_lists($_POST['id']));
        $view->assign('duplicate_lists', $ladder->covered_duplicate_lists($_POST['id']));
        $view->display_with_fill('adm_ladder_approver.tpl', array('fdat' => $data));
        break;
    default:
        $view->assign('status', new Ldr_Statusmst());
        $view->display_with_fill('adm_ladder_edit.tpl', array('fdat' => $data));
        break;
}
