<?php

require_once 'class/Base.php';
require_once 'class/Novice.php';
require_once 'class/NoviceAssessment.php';
require_once 'class/Config.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_ladder($session->emp());

//----------------------------------------------------------
// オブジェクト
//----------------------------------------------------------
$novice = new Ldr_Novice($session->emp()->emp_id());
$as = new Ldr_NoviceAssessment($session->emp()->emp_id());
$conf = new Ldr_Config();

//----------------------------------------------------------
// 版数
//----------------------------------------------------------
$current_revision = $as->current_revision();

//----------------------------------------------------------
// 一覧
//----------------------------------------------------------
$group1_list = $as->lists('group1', array('revision_id' => $current_revision));
foreach ($group1_list as $g1) {
    $group2_list[$g1['group1']] = $as->lists('group2', array('revision_id' => $current_revision, 'group1' => $g1['group1']));
}

//----------------------------------------------------------
// year
//----------------------------------------------------------
$year_list = $as->facilitate_year_lists();
$year = $_REQUEST['year'] ? $_REQUEST['year'] : Ldr_Util::th_year();
if (!Validation::is_year($year)) {
    header('Location: dashboard.php');
    exit;
}

//----------------------------------------------------------
// date
//----------------------------------------------------------
$date_list = $novice->date_lists($year);

//----------------------------------------------------------
// analysis
//----------------------------------------------------------
$analysis = $as->assessment_analysis($year);

//----------------------------------------------------------
// 評価の数字
//----------------------------------------------------------
if ($conf->novice_number() === '1') {
    $num1 = 'I';
    $num2 = 'II';
    $num3 = 'III';
    $num4 = 'IV';
}
else {
    $num1 = '1';
    $num2 = '2';
    $num3 = '3';
    $num4 = '4';
}

//----------------------------------------------------------
// fdat
//----------------------------------------------------------
$fdat = array(
    'year' => $year,
);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $session->emp());
$view->assign(Ldr_Util::get_common_info('novice_analysis'));

$view->assign(array(
    'current_revision' => $current_revision,
    'year_list' => $year_list,
    'date_list' => $date_list,
    'group1_list' => $group1_list,
    'group2_list' => $group2_list,
    'analysis' => $analysis,
    'facilitators' => $facilitators,
    'year' => $year,
));

//----------------------------------------------------------
// Excel
//----------------------------------------------------------
if ($_POST['mode'] === 'excel') {
    require_once 'phpexcel/Classes/PHPExcel.php';
    require_once 'phpexcel/Classes/PHPExcel/IOFactory.php';

    $num = $_POST['number'];
    $reader = PHPExcel_IOFactory::createReader('Excel2007');
    $excel = new PHPExcel();
    $excel->setActiveSheetIndex(0);
    $sheet = $excel->getActiveSheet();

    // 回数
    $sheet->setCellValue('A1', to_utf8("第{$num}回"));
    $sheet->setCellValue('C1', to_utf8("本人評価"));
    $sheet->setCellValue('G1', to_utf8("他者評価"));
    $sheet->setCellValue('K1', to_utf8("差分"));
    $sheet->mergeCells('A1:B1');
    $sheet->mergeCells('C1:F1');
    $sheet->mergeCells('G1:J1');
    $sheet->mergeCells('K1:N1');

    // グループ1
    $y = 2;
    foreach ($group1_list as $g1) {
        // 見出し
        $sheet->setCellValueByColumnAndRow(0, $y, to_utf8($g1['name']));
        $sheet->mergeCellsByColumnAndRow(0, $y, 1, $y);

        $sheet->setCellValueByColumnAndRow(2, $y, $num1);
        $sheet->setCellValueByColumnAndRow(3, $y, $num2);
        $sheet->setCellValueByColumnAndRow(4, $y, $num3);
        $sheet->setCellValueByColumnAndRow(5, $y, $num4);

        $sheet->setCellValueByColumnAndRow(6, $y, $num1);
        $sheet->setCellValueByColumnAndRow(7, $y, $num2);
        $sheet->setCellValueByColumnAndRow(8, $y, $num3);
        $sheet->setCellValueByColumnAndRow(9, $y, $num4);

        $sheet->setCellValueByColumnAndRow(10, $y, $num1);
        $sheet->setCellValueByColumnAndRow(11, $y, $num2);
        $sheet->setCellValueByColumnAndRow(12, $y, $num3);
        $sheet->setCellValueByColumnAndRow(13, $y, $num4);
        $y++;

        foreach ($group2_list[$g1['group1']] as $g2) {
            $sheet->setCellValueByColumnAndRow(0, $y, to_utf8($g2['name']));
            $sheet->mergeCellsByColumnAndRow(0, $y, 0, $y + $g2['count'] - 1);

            foreach ($analysis[$g1['group1']][$g2['group2']] as $data) {
                $sheet->setCellValueByColumnAndRow(1, $y, to_utf8($data['guideline']));

                $sheet->setCellValueByColumnAndRow(2, $y, $data[$num]['a1']);
                $sheet->setCellValueByColumnAndRow(3, $y, $data[$num]['a2']);
                $sheet->setCellValueByColumnAndRow(4, $y, $data[$num]['a3']);
                $sheet->setCellValueByColumnAndRow(5, $y, $data[$num]['a4']);

                $sheet->setCellValueByColumnAndRow(6, $y, $data[$num]['f1']);
                $sheet->setCellValueByColumnAndRow(7, $y, $data[$num]['f2']);
                $sheet->setCellValueByColumnAndRow(8, $y, $data[$num]['f3']);
                $sheet->setCellValueByColumnAndRow(9, $y, $data[$num]['f4']);

                $sheet->setCellValueByColumnAndRow(10, $y, $data[$num]['d1']);
                $sheet->setCellValueByColumnAndRow(11, $y, $data[$num]['d2']);
                $sheet->setCellValueByColumnAndRow(12, $y, $data[$num]['d3']);
                $sheet->setCellValueByColumnAndRow(13, $y, $data[$num]['d4']);
                $y++;
            }
        }
        $y++;
    }
    $sheet->getStyle("A1:N{$y}")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

    $sheet->getColumnDimension('A')->setWidth(30);
    $sheet->getColumnDimension('B')->setWidth(60);
    foreach (range('C', 'N') as $cell) {
        $sheet->getColumnDimension($cell)->setWidth(5);
    }
    $sheet->getStyle("A1:B{$y}")->getAlignment()->setWrapText(true);

    // 生成
    // 5.2.0以下ではZipArchiveがないのでExcel2007ではエラーになる
    if (version_compare(PHP_VERSION, '5.2.0') >= 0) {
        $filename = 'novice_analysis.xlsx';
        $excelversion = 'Excel2007';
    }
    else {
        $filename = 'novice_analysis.xls';
        $excelversion = 'Excel5';
    }
    $tmpname = tempnam('/tmp', 'xlsx');
    $writer = PHPExcel_IOFactory::createWriter($excel, $excelversion);
    $writer->save($tmpname);

    // 出力
    header("Content-Type: application/octet-stream");
    header("Content-Disposition: attachment; filename={$filename}");
    header("Content-Length:" . filesize($tmpname));
    readfile($tmpname);
    exit;
}

$view->display_with_fill('novice_analysis.tpl', array('fdat' => $fdat));
