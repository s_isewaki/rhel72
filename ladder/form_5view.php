<?php

require_once 'class/Base.php';
require_once 'class/Form51.php';
require_once 'class/Form52.php';
require_once 'class/Form53.php';
require_once 'class/Form54.php';
require_once 'class/Employee.php';
require_once 'class/Apply.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('form5_view');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_ladder($session->emp());

//----------------------------------------------------------
// Object
//----------------------------------------------------------
$target_emp = new Ldr_Employee($_POST["target_emp_id"]);
$form51 = new Ldr_Form51($target_emp->emp_id());
$form52 = new Ldr_Form52($target_emp->emp_id());
$form53 = new Ldr_Form53($target_emp->emp_id());
$form54 = new Ldr_Form54($target_emp->emp_id());
$emp = $session->emp();

//----------------------------------------------------------
// 初期情報設定
//----------------------------------------------------------
if ($_POST['mode'] === 'view') {
    $_POST['srh_start_date'] = null;
    $_POST['srh_last_date'] = null;
    $_POST['mode'] = null;
}

//----------------------------------------------------------
// 様式情報取得
//----------------------------------------------------------
if ($_POST['mode'] === 'print') {
    $cond = $_SESSION['cond'];
}
else {
    $cond['srh_start_date'] = $_POST['srh_start_date'];
    $cond['srh_last_date'] = $_POST['srh_last_date'];
    $_SESSION['cond'] = $cond;
}

$cond["order_by"][0] = "start_date desc";
$lists51 = $form51->lists(null, null, $cond);
$lists52 = $form52->lists(null, null, $cond);
$lists53 = $form53->lists(null, null, $cond);
$lists54 = $form54->lists(null, null, $cond);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $emp);
$view->assign(Ldr_Util::get_common_info($_POST['menu'], array('referer_url' => $_POST['owner_url'])));
$view->assign('target_emp', $target_emp);

# list
$view->assign('list51', $lists51);
$view->assign('list52', $lists52);
$view->assign('list53', $lists53);
$view->assign('list54', $lists54);

# type
$view->assign('f52_types', $form52->types);
$view->assign('f53_types', $form53->types);

//----------------------------------------------------------
// 印刷
//----------------------------------------------------------
if ($_POST['mode'] === 'print') {
    require_once 'class/PDF.php';
    $conf = new Ldr_Config();
    $title = $conf->titles();
    $html = $view->output_with_fill('print/form_5view.tpl', array('fdat' => $_POST));

    $pdf = new Ldr_PDF('P', 'mm', 'A4', true, 'UTF-8', false);
    $pdf->setHeaderTitle("[{$title['form_5']['title']}] {$title['form_5']['detailL']}");
    $pdf->setEmployee($target_emp);
    $pdf->setPrinter($session->emp());
    $pdf->AddPage();
    $pdf->writeHTML(to_utf8($html), true, 0, false, 0);
    $pdf->Output("form_5view.pdf", "I");
    exit;
}

//----------------------------------------------------------
// 表示
//----------------------------------------------------------
$apply = new Ldr_Apply(array('apply_emp_id' => $target_emp->emp_id(), 'apply_id' => $_POST['id']));
$view->assign('apply', $apply);
$view->assign('ldr', $apply->ladder());
$view->assign('active', empty($_POST['fm5_active']) ? 'form_51' : $_POST['fm5_active']);
$view->display_with_fill('form_5view.tpl', array('fdat' => $_POST));
