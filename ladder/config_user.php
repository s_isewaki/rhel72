<?php

require_once 'class/Base.php';
require_once 'class/Employee.php';
require_once 'class/Ladder.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// セッション初期化
//----------------------------------------------------------
Ldr_Util::session_init('adm_permission');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_ladder($session->emp());

//----------------------------------------------------------
// Object
//----------------------------------------------------------
$employee = new Ldr_Employee();
$ladder = new Ldr_Ladder();
$emp = $session->emp();
$conf = new Ldr_Config();
$effective_post = Ldr_Util::use_tiket();

//----------------------------------------------------------
// 保存
//----------------------------------------------------------
if ($effective_post && isset($_POST['btn_save'])) {
    // バリデーション
    $error = $employee->validate($_POST);

    // エラーが無ければ保存
    if (empty($error)) {
        // 保存処理
        try {
            $employee->save_config($_POST);
            $completed = true;
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
        }
    }
}

//----------------------------------------------------------
// 編集
//----------------------------------------------------------
if (empty($error)) {
    try {
        $fdat = $employee->find($emp->emp_id());
    }
    catch (Exception $e) {
        header('Location: dashboard.php');
        exit;
    }
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign(Ldr_Util::get_common_info('config_user'));
$view->assign('emp', $emp);
$view->assign('staff', $employee);
$view->assign('ladder', $ladder);
$view->assign('updated_on', $employee->updated_on());
$view->assign('completed', $completed);
$view->assign('required', $conf->required());

if (empty($error)) {
    $view->display_with_fill('config_user.tpl', array('fdat' => $fdat));
}
else {
    $view->assign('error', $error);
    $view->display_with_fill('config_user.tpl', array('fdat' => $_POST));
}
