<?php

require_once 'class/Base.php';
require_once 'class/Assessment.php';
require_once "libs/pChart2/class/pData.class.php";
require_once "libs/pChart2/class/pDraw.class.php";
require_once "libs/pChart2/class/pRadar.class.php";
require_once "libs/pChart2/class/pImage.class.php";

//----------------------------------------------------------
// SIZE
//----------------------------------------------------------
$w = 500;
$h = 300;

//----------------------------------------------------------
// get
//----------------------------------------------------------
$year = $_GET['year'];
$level = $_GET['level'];

//----------------------------------------------------------
// font
//----------------------------------------------------------
if (file_exists('/usr/share/fonts/ipa-gothic/ipag.ttf')) {
    $font = "/usr/share/fonts/ipa-gothic/ipag.ttf";
}
else if (file_exists('/usr/share/fonts/ipa-pgothic/ipagp.ttf')) {
    $font = "/usr/share/fonts/ipa-pgothic/ipagp.ttf";
}
else if (file_exists('/usr/share/fonts/japanese/TrueType/sazanami-gothic.ttf')) {
    $font = "/usr/share/fonts/japanese/TrueType/sazanami-gothic.ttf";
}
else if (file_exists('/usr/share/fonts/ja/TrueType/kochi-gothic-subst.ttf')) {
    $font = "/usr/share/fonts/ja/TrueType/kochi-gothic-subst.ttf";
}

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    print cmx_json_encode(array());
    exit;
}
$emp = $session->emp();

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_ladder($session->emp());

//----------------------------------------------------------
// emp_id
//----------------------------------------------------------
if ($_GET['emp_id']) {
    $emp_id = $_GET['emp_id'];
}
else {
    $emp_id = $emp->emp_id();
}
//----------------------------------------------------------
// オブジェクト
//----------------------------------------------------------
$as = new Ldr_Assessment($emp_id);
$ladder = new Ldr_Ladder();

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('assessment');

//----------------------------------------------------------
// ladder
//----------------------------------------------------------
$ladder_id = $_SESSION['ladder_id'];
$this_ladder = $ladder->find($ladder_id);
$revision = $this_ladder['guideline_revision'];

//----------------------------------------------------------
// 一覧
//----------------------------------------------------------
$radar_label = $as->radar_label($level, $revision);

//----------------------------------------------------------
// assessment
//----------------------------------------------------------
$years = range($year - 2, $year);
$colors = array(array("R" => 255, "G" => 204, "B" => 255), array("R" => 255, "G" => 153, "B" => 204), array("R" => 255, "G" => 0, "B" => 102));
$assessment = $as->radar($year, $level, $revision);

//----------------------------------------------------------
// pData
//----------------------------------------------------------
$data = array();
$label = array();
$MyData = new pData();
foreach ($radar_label as $row) {
    $label[] = to_utf8(mb_substr($row['g2_name'], 0, 10, 'EUC-JP'));

    $asm = $assessment[$row['guideline_group2']];
        $data['y' . $year][] = $asm['point'] / $asm['total'] * 100;
        $data['y' . ($year - 1)][] = $asm['point2'] / $asm['total'] * 100;
        $data['y' . ($year - 2)][] = $asm['point3'] / $asm['total'] * 100;

}

foreach ($years as $y) {
    $MyData->addPoints($data["y$y"], "y$y");
    $MyData->setSerieDescription("y$y", to_utf8("{$y}年度"));
    $MyData->setPalette("y$y", array_shift($colors));
}
$MyData->addPoints($label, "Labels");
$MyData->setAbscissa("Labels");

$myPicture = new pImage($w, $h, $MyData);
if (!$_GET['antialias']) {
    $myPicture->Antialias = FALSE;
}

$Settings = array("StartR" => 255, "StartG" => 255, "StartB" => 255, "EndR" => 255, "EndG" => 255, "EndB" => 255, "Alpha" => 255);
$myPicture->drawGradientArea(10, 10, $w - 10, $h - 10, DIRECTION_VERTICAL, $Settings);

$myPicture->setFontProperties(array("FontName" => $font, "FontSize" => 8));

$SplitChart = new pRadar();

$myPicture->setGraphArea(10, 10, $w - 10, $h - 10);
$SplitChart->drawRadar($myPicture, $MyData, array("DrawPoly" => TRUE, "LabelPos" => RADAR_LABELS_HORIZONTAL, "Segments" => 10, "FixedMax" => 100, "AxisRotation" => -90));

$myPicture->setFontProperties(array("FontName" => $font, "FontSize" => 8));
$myPicture->drawLegend(10, 10, array("Style" => LEGEND_BOX, "Mode" => LEGEND_VERTICAL));

$myPicture->setFontProperties(array("FontName" => $font, "FontSize" => 8));
$myPicture->drawText($w - 70, 20, to_utf8('レベル ') . Ldr_Const::get_roman_num($level), array("DrawBox" => TRUE, "R" => 0, "G" => 0, "B" => 0, "Angle" => 0, "FontSize" => 9));

$myPicture->Stroke();
