<?php

require_once 'class/Base.php';
require_once 'class/Pager.php';
require_once 'class/Inside.php';


//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}
//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('inside_apply');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_ladder($emp);

//----------------------------------------------------------
// object
//----------------------------------------------------------
$inside = new Ldr_Inside($emp->emp_id());
$effective_post = Ldr_Util::use_tiket();

//----------------------------------------------------------
// データの取得
//----------------------------------------------------------
$dt = $inside->find($_POST['id']);

$data = array(
    'trn' => $dt,
    'hd_title' => ($_POST['mode'] === 'before') ? '院内研修 前課題' : '院内研修 後課題',
    'work' => ($_POST['mode'] === 'before') ? $dt['before_work'] : $dt['after_work'],
);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $session->emp());
$view->assign($dt);
$view->assign($data);
$view->assign(Ldr_Util::get_common_info('inside'));
$view->display('trn_inside_work_view.tpl');
