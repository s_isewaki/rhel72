<?php

require_once 'class/Base.php';
require_once 'class/Apply.php';
require_once 'class/Employee.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_ladder($session->emp());

//----------------------------------------------------------
// Object
//----------------------------------------------------------
$emp = $session->emp();
$target_emp = new Ldr_Employee($_POST["target_emp_id"]);
$apply = new Ldr_Apply(array('apply_id' => $_POST['id'], 'apply_emp_id' => $_POST["target_emp_id"], 'accept_emp_id' => $emp->emp_id()));
$ladder = $apply->ladder();
$form1 = $apply->find_form1($_POST['id']);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $emp);
$view->assign('target_emp', $target_emp);
$view->assign('apply', $apply);
$view->assign('ldr', $ladder);
$view->assign('form1', $form1);
$view->assign(Ldr_Util::get_common_info($_POST['menu'], array('referer_url' => $_POST['owner_url'])));

//----------------------------------------------------------
// 印刷
//----------------------------------------------------------
if ($_POST['mode'] === 'print') {
    require_once 'class/PDF.php';
    $pdf = new Ldr_PDF('P', 'mm', 'A4', true, 'UTF-8', false);
    $pdf->setHeaderTitle("[{$ladder['config']['headmenu']}]");
    $pdf->setEmployee($target_emp);
    $pdf->setPrinter($session->emp());

    $pdf->AddPage();

    $html = $view->output_with_fill('print/form_1view.tpl', array('fdat' => $_POST));
    $pdf->writeHTML(to_utf8($html), true, 0, false, 0);
    $pdf->Output("form_1view.pdf", "I");
    exit;
}

$view->display_with_fill('form_1view.tpl', array('fdat' => $_POST));
