<?php

require_once 'class/Base.php';
require_once 'class/Pager.php';
require_once 'class/Employee.php';
require_once 'class/AnalysisLadder.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
    exit;
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_analysis($session->emp());

//----------------------------------------------------------
// object
//----------------------------------------------------------
$analysis = new Ldr_AnalysisLadder();
$ladder = new Ldr_Ladder();

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('ldr_common');

//----------------------------------------------------------
// year
//----------------------------------------------------------
$year_list = range(date('Y') + 1, 2007);
$year = $_POST['year'] ? $_POST['year'] : ($_SESSION['year'] ? $_SESSION['year'] : Ldr_Util::th_year());

//----------------------------------------------------------
// ladder_id
//----------------------------------------------------------
foreach ($ladder->lists() as $ldr) {
    $ladder_id_1st = $ldr['ladder_id'];
    break;
}
$ladder_id = $_POST['ladder_id'] ? $_POST['ladder_id'] : ($_SESSION['ladder_id'] ? $_SESSION['ladder_id'] : $ladder_id_1st);

//----------------------------------------------------------
// 遷移状態判別
//----------------------------------------------------------
// 戻る
if (isset($_POST['ldr_return'])) {
    $_POST["page_no"] = $_SESSION['page_no'];
}

// ページャーから移動
else if (Ldr_Pager::is_pager($_POST['move_pager'])) {
    //検索条件を取得
    $_SESSION['page_no'] = Ldr_Pager::get_page_no($_POST['page_no']);
}

// 検索ボタン
else if ($_POST['btn_search']) {
    $_SESSION["year"] = $year;
    $_SESSION["ladder_id"] = $ladder_id;
    $_POST["page_no"] = 1;
}

// ソート
else if ($_REQUEST['order']) {
    $_POST["page_no"] = 1;

    $desc = $_REQUEST['desc'] ? 'DESC' : 'ASC';

    switch ($_REQUEST['order']) {
        case 'st_order_no':
            $order = "st_order_no $desc, ldr_level.date, CAST(ldr_level.number AS INT), ldr_level.level DESC";
            break;

        case 'ldr_level.level':
            $order = "ldr_level.level $desc, ldr_level.date, ldr_level.number";
            break;

        case 'ldr_level.date':
            $order = "ldr_level.date $desc, CAST(ldr_level.number AS INT), ldr_level.level DESC";
            break;

        case 'ldr_level.number':
            $order = "CAST(ldr_level.number AS INT) $desc, ldr_level.date, ldr_level.level DESC";
            break;

        default:
            $order = "{$_REQUEST['order']} IS NULL ASC, {$_REQUEST['order']} $desc, ldr_level.date, CAST(ldr_level.number AS INT), ldr_level.level DESC";
            break;
    }
}

// 外部から移動してきた場合
else {
    // セッション情報の初期化
    $_POST["page_no"] = 1;
    $_SESSION['page_no'] = 1;
}

$_SESSION['ldr_return'] = null;

//----------------------------------------------------------
// 職員リスト
//----------------------------------------------------------
$options = array(
    'order_by' => $order ? "$order, empmst.emp_kn_lt_nm, empmst.emp_kn_ft_nm" : 'ldr_level.date, ldr_level.number, ldr_level.level DESC, empmst.emp_kn_lt_nm, empmst.emp_kn_ft_nm',
    'year' => $year,
    'ladder_id' => $ladder_id,
);
$lists = $emp->certification_lists($options);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign(Ldr_Util::get_common_info('staff_certification'));
$view->assign('emp', $session->emp());
$view->assign('ladder', $ladder);
$view->assign('year', (int)$year);
$view->assign('ladder_id', $ladder_id);
$view->assign('ldr', $ladder->find($ladder_id));

//----------------------------------------------------------
// 印刷
//----------------------------------------------------------
if ($_POST['mode'] === 'print') {
    $view->assign('list', $emp->certification_lists($options));
    require_once 'class/PDF.php';
    $html = $view->fetch('print/staff_certification.tpl');
    $pdf = new Ldr_PDF('P', 'mm', 'A4', true, 'UTF-8', false);
    $pdf->setHeaderTitle("認定者一覧");
    $pdf->setPrinter($session->emp());
    $pdf->AddPage();
    $pdf->writeHTML(to_utf8($html), true, 0, false, 0);
    $pdf->Output("staff_certification.pdf", "I");
    exit;
}

//----------------------------------------------------------
// 表示
//----------------------------------------------------------
$view->assign(Ldr_Pager::get_pager_info($_POST['page_no'], $count, $perPage));
$view->assign('order', $_REQUEST['order']);
$view->assign('desc', $_REQUEST['desc']);
$view->assign('year_list', $year_list);
$view->assign('list', $lists);
$view->display_with_fill('staff_certification.tpl', array('fdat' => $options));
