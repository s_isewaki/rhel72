<?php

require_once 'class/Base.php';
require_once 'class/Outside.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('trn_approve');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_ladder($emp);

//----------------------------------------------------------
// object
//----------------------------------------------------------
$outside = new Ldr_Outside($emp->emp_id());

//----------------------------------------------------------
// データの取得
//----------------------------------------------------------
if (!isset($_POST['id'])) {
    cmx_log(basename($_SERVER["PHP_SELF"]) . " parameter error");
    header('Location: dashboard.php');
    exit;
}

$fdat = $outside->find($_POST['id']);
$target_emp = new Ldr_Employee($fdat['emp_id']);

$data = array(
    'type_text' => Ldr_Const::get_outside_type(),
    'trn' => $fdat,
    'target_emp' => $target_emp,
    'view' => true,
    'hd_title' => '院外研修 確認',
);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $emp);
$view->assign($data);
$view->assign(Ldr_Util::get_common_info('outside'));
$view->display('trn_approve_outside_edit.tpl');
