<?php

require_once 'class/Base.php';
require_once 'class/Assessment.php';
require_once 'class/AnalysisLadder.php';
require_once 'class/AnalysisAssessment.php';
require_once 'class/Classmst.php';
require_once 'class/Ladder.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}
$emp = $session->emp();

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_analysis($session->emp());

//----------------------------------------------------------
// object
//----------------------------------------------------------
$analysis = new Ldr_AnalysisLadder();
$assessment = new Ldr_AnalysisAssessment();
$ladder = new Ldr_Ladder();
$ladder_id = $_POST['ladder_id'] ? $_POST['ladder_id'] : 1;

//----------------------------------------------------------
// year
//----------------------------------------------------------
$year_list = range(date('Y') + 1, 2014);
$year = $_POST['year'] ? $_POST['year'] : Ldr_Util::th_year();

//----------------------------------------------------------
// ladder
//----------------------------------------------------------
$this_ladder = $ladder->find($ladder_id);

//----------------------------------------------------------
// 部署リスト
//----------------------------------------------------------
$full = false;
if ($emp->is_analysis() || $emp->committee() || $emp->is_admin()) {
    $class_lists = $analysis->lists('class');
    $srh_class3 = $_POST['srh_class3'];
    $full = true;
}
else {
    $class_lists = $analysis->lists_class_approver($emp->emp_id());
    $srh_class3 = $_POST['srh_class3'] ? $_POST['srh_class3'] : implode("_", array($emp->dept_id(), $emp->room_id()));
}

//----------------------------------------------------------
// csv出力
//----------------------------------------------------------
if ($_POST['mode'] === 'csv') {
    $list = $assessment->lists($year, $this_ladder, $srh_class3);
    $csv = to_sjis($assessment->export_csv($list, $ladder, $_POST['ladder_id'], $year));
    $filename = "analysis_assessment.csv";

    // output
    ob_clean();
    header("Content-Disposition: attachment; filename=$filename");
    header("Content-Type: application/octet-stream; name=$filename");
    header("Content-Length: " . strlen($csv));
    echo($csv);
    ob_end_flush();
    exit;
}

//----------------------------------------------------------
// list
//----------------------------------------------------------
if ($srh_class3) {
    $list = $assessment->lists($year, $this_ladder, $srh_class3);
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign(Ldr_Util::get_common_info('analysis_assessment'));
$view->assign('emp', $session->emp());
$view->assign('ladder', $ladder);
$view->assign('year', (int)$year);
$view->assign('class_lists', $class_lists);
$view->assign('full', $full);
$view->assign('year_list', $year_list);
$view->assign('lists', $list);
$view->assign('this_ladder', $this_ladder);
$view->display_with_fill('analysis_assessment.tpl', array('fdat' => $_POST));
