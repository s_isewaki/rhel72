<?php

require_once 'class/Base.php';
require_once 'class/Committee.php';
require_once 'class/Apply.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_ladder($emp);

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
Ldr_Util::session_init('accept_auditor');

if ($_POST['mode'] === "new" || $_POST['mode'] === "retry" || $_POST['mode'] === "cancel") {
    // 初期情報を退避
    $_SESSION['id'] = $_POST['id'];
    $_SESSION['target_emp_id'] = $_POST['target_emp_id'];
    $_SESSION['rtn_apply_view'] = Ldr_Util::get_referer_url();
}
else {
    $_POST['id'] = $_SESSION['id'];
    $_POST['target_emp_id'] = $_SESSION['target_emp_id'];
}

//----------------------------------------------------------
// オブジェクト
//----------------------------------------------------------
$apply = new Ldr_Apply(array('apply_emp_id' => $_POST["target_emp_id"], 'accept_emp_id' => $emp->emp_id(), 'apply_id' => $_POST['id'], 'mode' => 'accept'));
$target_emp = new Ldr_Employee($_POST["target_emp_id"]);
$ladder = $apply->ladder();
$effective_post = Ldr_Util::use_tiket();
$comm = new Ldr_Committee();

//----------------------------------------------------------
// 設定
//----------------------------------------------------------
if ($_POST['form_submit'] === 'save') {
    $error = $apply->validate_auditor($_POST);
    if (empty($error)) {
        try {
            if ($effective_post) {
                $apply->accept_auditor($_POST);
            }
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
        }
        header('Location:' . Ldr_Util::get_referer_url());
        exit;
    }
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign(Ldr_Util::get_common_info('accept', array('referer_url' => $_SESSION['rtn_apply_view'])));
$view->assign('emp', $session->emp());
$view->assign('target_emp', $target_emp);
$view->assign('apply', $apply);
$view->assign('ldr', $ladder);
$view->assign('auditor', $apply->get_auditor());
$view->assign('comm_list', $comm->lists());
$view->assign('id', $_POST['id']);
$view->assign('menu', 'accept');
$view->assign('mode', $_POST['mode']);
$view->assign('target_emp_id', $target_emp->emp_id());
$view->assign($apply->form_updated_on($_POST['id']));
$view->assign('error', $error);
$view->display('accept_auditor.tpl');
