<?php

require_once 'class/Base.php';
require_once 'class/Approver.php';

//----------------------------------------------------------
// Obj
//----------------------------------------------------------
$appr = new Ldr_Approver();

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
header('X-Content-Type-Options: nosniff');
header('Cache-Control: no-cache, must-revalidate');
header("Cache-Control: post-check=0, pre-check=0", false);
header('Pragma: no-cache');
print cmx_json_encode(
        $appr->lists()
);
