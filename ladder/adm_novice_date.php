<?php

require_once 'class/Base.php';
require_once 'class/Novice.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_admin($emp);

//----------------------------------------------------------
// object
//----------------------------------------------------------
$novice = new Ldr_Novice();
$effective_post = Ldr_Util::use_tiket();

//----------------------------------------------------------
// 保存
//----------------------------------------------------------
if (isset($_POST['btn_save'])) {
    // バリデーション
    $error = $novice->validate_date($_POST);

    // エラーが無ければ保存
    if (empty($error)) {
        try {
            if ($effective_post) {
                $novice->save_date($_POST);
                $completed = true;
            }
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
        }
    }
}

//----------------------------------------------------------
// 年リスト
//----------------------------------------------------------
$year_list = $novice->year_lists();
$year = !empty($_POST['year']) ? $_POST['year'] : Ldr_Util::th_year();

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $emp);
$view->assign(Ldr_Util::get_common_info('adm_novice_date'));
$view->assign('year_list', $year_list);
$view->assign('year', $year);
$view->assign('list', $novice->date_lists($year));
$view->assign('completed', $completed);
$view->assign('error', $error);
$view->display_with_fill('adm_novice_date.tpl', array('fdat' => $_POST));
