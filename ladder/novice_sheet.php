<?php

require_once 'class/Base.php';
require_once 'class/Novice.php';
require_once 'class/NoviceAssessment.php';
require_once 'class/Config.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_ladder($session->emp());

//----------------------------------------------------------
// オブジェクト
//----------------------------------------------------------
$novice = new Ldr_Novice($session->emp()->emp_id());
$as = new Ldr_NoviceAssessment($_POST['emp_id']);
$conf = new Ldr_Config();

//----------------------------------------------------------
// 版数
//----------------------------------------------------------
$current_revision = $as->current_revision();

//----------------------------------------------------------
// 一覧
//----------------------------------------------------------
$group1_list = $as->lists('group1', array('revision_id' => $current_revision));
foreach ($group1_list as $g1) {
    $group2_list[$g1['group1']] = $as->lists('group2', array('revision_id' => $current_revision, 'group1' => $g1['group1']));
}

//----------------------------------------------------------
// year
//----------------------------------------------------------
$year_list = $as->novice_year_lists();
$year = $_POST['year'] ? $_POST['year'] : $year_list[0];
if (!Validation::is_year($year)) {
    header('Location: dashboard.php');
    exit;
}

//----------------------------------------------------------
// date
//----------------------------------------------------------
$date_list = $novice->date_lists($year);

//----------------------------------------------------------
// assessment
//----------------------------------------------------------
$assessment = $as->full_assessment($_POST['emp_id'], $year);

//----------------------------------------------------------
// 本人
//----------------------------------------------------------
$target_emp = new Ldr_Employee($_POST['emp_id']);

//----------------------------------------------------------
// 評価者
//----------------------------------------------------------
$facilitators = $novice->facilitate_lists($_POST['emp_id'], $year);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $session->emp());
$view->assign(Ldr_Util::get_common_info($_POST['menu']));
$view->assign('novice_number', $conf->novice_number());

$view->assign(array(
    'current_revision' => $current_revision,
    'year_list' => $year_list,
    'date_list' => $date_list,
    'group1_list' => $group1_list,
    'group2_list' => $group2_list,
    'assessment_list' => $assessment,
    'facilitators' => $facilitators,
    'year' => $year,
    'novice' => $target_emp,
));

//----------------------------------------------------------
// 印刷
//----------------------------------------------------------
if ($_POST['mode'] === 'print') {
    require_once 'class/PDF.php';
    $view->assign('target_ass', substr($_POST['link'], 0, 1));
    $view->assign('target_num', substr($_POST['link'], 1, 1));
    $view->assign('text_width', 440 - (count($date_list) * 30));
    $html = $view->fetch('print/novice_sheet.tpl');
    $title = $conf->titles();

    $pdf = new Ldr_PDF('P', 'mm', 'A4', true, 'UTF-8', false);
    $pdf->setHeaderTitle("{$title['novice']}: 評価シート");
    $pdf->setEmployee($target_emp);
    $pdf->setPrinter($session->emp());
    $pdf->AddPage();
    $pdf->writeHTML(to_utf8($html), true, 0, false, 0);
    $pdf->Output("novice_sheet.pdf", "I");
    exit;
}

//----------------------------------------------------------
// チャート印刷
//----------------------------------------------------------
else if ($_POST['mode'] === 'chart') {
    require_once 'class/PDF.php';
    $view->assign('comedix', basename(str_replace('/ladder', '', dirname(__FILE__))));
    $view->assign('number', $_POST['number']);
    $html = $view->fetch('print/novice_chart.tpl');
    $title = $conf->titles();

    $pdf = new Ldr_PDF('P', 'mm', 'A4', true, 'UTF-8', false);
    $pdf->setHeaderTitle("{$title['novice']}: 評価シート");
    $pdf->setEmployee($target_emp);
    $pdf->setPrinter($session->emp());
    $pdf->AddPage();
    $pdf->writeHTML(to_utf8($html), true, 0, false, 0);
    $pdf->Output("novice_chart.pdf", "I");
    exit;
}

$view->display('novice_sheet.tpl');
