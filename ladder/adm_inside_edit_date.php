<?php

require_once 'class/Base.php';
require_once 'class/InsideMst.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('adm_inside');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_mst_inside($session->emp());

//----------------------------------------------------------
// Obj
//----------------------------------------------------------
$mst = new Ldr_InsideMst();
$effective_post = Ldr_Util::use_tiket();
$conf = new Ldr_Config();

//----------------------------------------------------------
// 保存
//----------------------------------------------------------
if ($_POST['btn_save'] === 'save') {
// バリデーション
    $error = $mst->validate_date($_POST);

// エラーが無ければ保存
    if (empty($error)) {
        try {
            if ($effective_post) {
                $mst->save_date($_POST);
            }
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
        }

        header('Location:' . 'adm_inside_date.php?id=' . $_POST['id']);
        exit;
    }
}
//----------------------------------------------------------
// 削除
//----------------------------------------------------------
else if ($_POST['btn_save'] === 'remove') {
    try {
        if ($effective_post) {
            $mst->remove_date($_POST['inside_date_id']);
        }
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }

    header('Location:' . 'adm_inside_date.php?id=' . $_POST['id']);
    exit;
}
//----------------------------------------------------------
// データの取得
//----------------------------------------------------------
$data = $mst->find_date($_POST['id'], $_POST['inside_date_id']);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $session->emp());
$view->assign(Ldr_Util::get_common_info('adm_inside'));
$view->assign($data);
$view->assign('ojt_name', $conf->ojt_name());

if ($error) {
    $view->assign('error', $error);
    $view->assign('type', $_POST['type']);
    $view->display_with_fill('adm_inside_edit_date.tpl', array('fdat' => $_POST));
}
else {
    $view->assign('type', $data['date']['type']);
    $view->display_with_fill('adm_inside_edit_date.tpl', array('fdat' => $data['date']));
}