<?php

require_once 'class/Base.php';
require_once 'class/NextPlan.php';
require_once 'class/Base.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();

//----------------------------------------------------------
// データ対象者
//----------------------------------------------------------
$target_emp_id = !empty($target_emp_id) ? $target_emp_id : $emp->emp_id();
$target_emp = new Ldr_Employee($target_emp_id);

//----------------------------------------------------------
// オブジェクト
//----------------------------------------------------------
$plan = new Ldr_NextPlan($target_emp->emp_id());
$conf = new Ldr_Config();

$effective_post = Ldr_Util::use_tiket();

$year = !empty($_POST['year']) ? $_POST['year'] : Ldr_Util::th_year();

//----------------------------------------------------------
// 保存
//----------------------------------------------------------
if ($effective_post && isset($_POST['btn_save'])) {
    // バリデーション
    $error = $plan->validate_next_plan($_POST);

    // エラーが無ければ保存
    if (empty($error)) {
        try {
            $list = $_POST;
            $plan->save($list);
            $completed = true;
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
        }
    }
}

$list = $plan->find($year);
$list['university_transfer_destination'] = explode(",", $list['university_transfer_destination']);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $emp);
$view->assign('target_emp', $target_emp);
$view->assign(Ldr_Util::get_common_info('next_plan'));
$view->assign('year', $year);
$view->assign('year_list', $plan->year_lists());
$view->assign('university_list', array(1 => '浦安', 2 => '越谷', 3 => '高齢者医療センター', 4 => '練馬', 5 => '静岡'));
$view->assign('list', $list);

$view->assign('error', $error);

//----------------------------------------------------------
// 印刷
//----------------------------------------------------------
if ($_POST['mode'] === 'print') {
    require_once 'class/PDF.php';
    $html = $view->fetch('print/next_plan.tpl');
    $titles = $conf->titles();

    $pdf = new Ldr_PDF('P', 'mm', 'A4', true, 'UTF-8', false);
    $pdf->setHeaderTitle($titles['next_plan']);
    $pdf->setEmployee($target_emp);
    $pdf->setPrinter($session->emp());
    $pdf->AddPage();
    $pdf->writeHTML(to_utf8($html), true, 0, false, 0);
    $pdf->Output("next_plan.pdf", "I");
    exit;
}

$view->display('next_plan.tpl');