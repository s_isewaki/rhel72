<?php

require_once 'class/Base.php';
require_once 'class/Validation.php';
require_once 'class/Ladder.php';
require_once 'class/Apply.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_ladder($emp);

//----------------------------------------------------------
// Apply
//----------------------------------------------------------
$apply = new Ldr_Apply(array('apply_emp_id' => $emp->emp_id(), 'apply_id' => $_POST['id']));

//----------------------------------------------------------
// ラダー
//----------------------------------------------------------
if (empty($_POST['id'])) {
    $ladder = new Ldr_Ladder();
    $ladder_id = $_REQUEST['ldr'];
    if (!$ladder->is_ladder_id($ladder_id)) {
        header('Location: dashboard.php');
        exit;
    }
    $apply->set_ladder($ladder_id);
}

//----------------------------------------------------------
// Obj
//----------------------------------------------------------
$effective_post = Ldr_Util::use_tiket();

//----------------------------------------------------------
// referer_url
//----------------------------------------------------------
if (!empty($_POST['owner_url'])) {
    $_POST['referer_url'] = null;
    $option['referer_url'] = $_POST['owner_url'];
}
else {
    $option['referer_url'] = 'apply.php';
}

//----------------------------------------------------------
// 保存
//----------------------------------------------------------
if (isset($_POST['btn_save'])) {
    // バリデーション
    $error = $apply->validate_form1($_POST);

    // エラーが無ければ保存
    if (empty($error)) {
        try {
            if ($effective_post) {
                $apply->create_apply($_POST);
            }
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
        }

        header('Location:' . $option['referer_url']);
        exit;
    }

    $data = $_POST;
}

//----------------------------------------------------------
// 更新
//----------------------------------------------------------
else if ($_POST['mode'] === "edit") {
    $data = $apply->find_form1($_POST['id']);
}

//----------------------------------------------------------
// 新規登録
//----------------------------------------------------------
else {
    $data = array(
        'level' => ($emp->level($ladder_id) > 0 ? intval($emp->level($ladder_id)) + 1 : 1),
        'ladder_id' => $ladder_id,
        'apply_date' => date("Y-m-d"),
    );

    if (Validation::is_date($emp->nurse_date())) {
        $diff = Ldr_Util::diff_th_year($emp->nurse_date(), date("Y-m-d"));
        $data['career'] = $diff < 0 ? '' : $diff;
    }
    if (Validation::is_date($emp->hospital_date())) {
        $diff = Ldr_Util::diff_th_year($emp->hospital_date(), date("Y-m-d"));
        $data['career_hospital'] = $diff < 0 ? '' : $diff;
    }
    if (Validation::is_date($emp->ward_date())) {
        $diff = Ldr_Util::diff_th_year($emp->ward_date(), date("Y-m-d"));
        $data['career_ward'] = $diff < 0 ? '' : $diff;
    }

    // メニューを出さない
    unset($_POST['owner_url']);
}

//----------------------------------------------------------
// fdat
//----------------------------------------------------------
$fdat = array_merge($_POST, $data);
$fdat['target_emp_id'] = $emp->emp_id();
$menu = empty($_POST['menu']) ? 'form_1' : $_POST['menu'];

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $emp);
$view->assign('apply', $apply);
$view->assign('ldr', $apply->ladder());
$view->assign('level', $apply->level());
$view->assign('facilitator', $fdat['facilitator']);
$view->assign('appraiser', $fdat['appraiser']);
$view->assign(Ldr_Util::get_common_info($menu, $option));

$view->assign('error', $error);
$view->display_with_fill('apply_entry.tpl', array('fdat' => $fdat));
