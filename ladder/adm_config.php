<?php

require_once 'class/Base.php';
require_once 'class/Classmst.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_admin($session->emp());

//----------------------------------------------------------
// オブジェクト
//----------------------------------------------------------
$conf = new Ldr_Config();
$classmst = new Ldr_Classmst();
$effective_post = Ldr_Util::use_tiket();

//----------------------------------------------------------
// 保存
//----------------------------------------------------------
if ($effective_post && isset($_POST['btn_save'])) {
    switch ($_POST['btn_save']) {
        // 基本設定
        case 'basic':
            try {
                $conf->save_basic($_POST);
                $completed = true;
            }
            catch (Exception $e) {
                cmx_log($e->getMessage());
                js_error_exit();
            }
            break;

        // 利用機能
        case 'functions':
            try {
                $conf->save_functions($_POST);
                $completed = true;
            }
            catch (Exception $e) {
                cmx_log($e->getMessage());
                js_error_exit();
            }
            break;

        // 名称変更
        case 'titles':
            try {
                $conf->save_titles($_POST);
                $completed = true;
            }
            catch (Exception $e) {
                cmx_log($e->getMessage());
                js_error_exit();
            }
            break;

        // 表示順
        case 'sort':
            try {
                $conf->save_sort($_POST);
                $completed = true;
            }
            catch (Exception $e) {
                cmx_log($e->getMessage());
                js_error_exit();
            }
            break;

        // 必須項目
        case 'required':
            try {
                $conf->save_required($_POST);
                $completed = true;
            }
            catch (Exception $e) {
                cmx_log($e->getMessage());
                js_error_exit();
            }
            break;

        default:
            break;
    }
}

//----------------------------------------------------------
// data
//----------------------------------------------------------
$active = isset($_POST['tab']) ? $_POST['tab'] : 'basic';

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $session->emp());
$view->assign(Ldr_Util::get_common_info('adm_config'));
$view->assign($classmst->_classname());
$view->assign('hospital_name', $conf->hospital_name());
$view->assign('na_name', $conf->na_name());
$view->assign('jrc_branch_name', $conf->jrc_branch_name());
$view->assign('view_class', $conf->view_class());
$view->assign('titles', $conf->titles());
$view->assign('functions', $conf->functions());
$view->assign('sortset', $conf->sortset());
$view->assign('required', $conf->required());
$view->assign('completed', $completed);
$view->assign('active', $active);
$view->display('adm_config.tpl');
