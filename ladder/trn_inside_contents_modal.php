<?php

require_once 'class/Base.php';
require_once 'class/Inside.php';
require_once 'class/Guideline.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}
//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_ladder($emp);

//----------------------------------------------------------
// Obj
//----------------------------------------------------------
$inside = new Ldr_Inside($emp->emp_id());
$gl = new Ldr_Guideline();

$view = new Cmx_View('ladder/templates');

//----------------------------------------------------------
// データ
//----------------------------------------------------------
if (!isset($_GET['id'])) {
    cmx_log(basename($_SERVER["PHP_SELF"]) . " parameter error");
    js_error_exit();
}
$data = $inside->find_course_date($_GET['id']);
$gp1 = $gl->find('group1', $data['guideline_group1']);

$data2 = array(
    'gp1' => $gp1,
    'roman_num' => Ldr_Const::get_roman_num(),
);
//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
header('Cache-Control: no-cache, must-revalidate');
header("Cache-Control: post-check=0, pre-check=0", false);
header('Pragma: no-cache');
$view->assign($data);
$view->assign($data2);
$view->display("modal/inside_contents.tpl");
