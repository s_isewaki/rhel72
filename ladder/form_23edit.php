<?php

require_once 'class/Base.php';
require_once 'class/Form23.php';
require_once 'class/Relief.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('form23');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_ladder($session->emp());

//----------------------------------------------------------
// Object
//----------------------------------------------------------
$emp = $session->emp();
$form = new Ldr_Form23($emp->emp_id());

$relief = new Ldr_Relief();
$subject_lists = $relief->lists('subject');
$contents_lists = $relief->lists('contents_all');
$effective_post = Ldr_Util::use_tiket();
//----------------------------------------------------------
// 保存
//----------------------------------------------------------
if (isset($_POST['btn_save'])) {
    // バリデーション
    $error = $form->validate($_POST);

    // エラーが無ければ保存
    if (empty($error)) {
        $tmp = explode("_", $_POST["unit"], 2);
        $unit = $tmp[1];
        foreach ($contents_lists as $value) {
            if ($value['relief_id'] === $_POST['subject'] && $unit == $value['unit']) {
                $_POST['subject'] = $value['subject'];
                $_POST['unit'] = $value['unit'];
                $_POST['contents'] = $value['contents'];
                break;
            }
        }

        // 保存処理
        try {
            if ($effective_post) {
                $form->save($_POST);
            }
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
            exit;
        }
        // 一覧に戻る
        $_SESSION['ldr_return'] = true;
        header('Location: form_23.php');
        exit;
    }
}
//----------------------------------------------------------
// 編集
//----------------------------------------------------------
elseif (isset($_POST["inner_id"]) && $_POST["inner_id"] !== "") {
    try {
        $fdat = $form->find($_POST["inner_id"]);
        if (empty($fdat)) {
            $not_found = true;
        }
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
        exit;
    }
    if (!empty($fdat)) {
        foreach ($contents_lists as $value) {
            if ($fdat['subject'] === $value['subject'] && $fdat['unit'] == $value['unit']) {
                $fdat['subject'] = $value['relief_id'];
                $fdat['unit'] = $value['relief_id'] . '_' . $value['unit'];
                $fdat['unit_clone'] = $fdat['unit'];
                break;
            }
        }
    }
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $session->emp());
$view->assign(Ldr_Util::get_common_info('form_23'));
$view->assign('subject_lists', $subject_lists);
$view->assign('contents_lists', $contents_lists);
$view->assign('not_found', $not_found);

if (empty($error)) {
    $view->display_with_fill('form_23edit.tpl', array('fdat' => $fdat));
}
else {
    $view->assign('error', $error);
    $view->display_with_fill('form_23edit.tpl', array('fdat' => $_POST));
}
exit;
