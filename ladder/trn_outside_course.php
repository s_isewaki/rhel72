<?php

require_once 'class/Base.php';
require_once 'class/Pager.php';
require_once 'class/Outside.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('trn_outside');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_ladder($emp);

//----------------------------------------------------------
// object
//----------------------------------------------------------
$outside = new Ldr_Outside($emp->emp_id());
$perPage = Ldr_Const::SRH_PERPAGE;

//----------------------------------------------------------
// 遷移状態判別
//----------------------------------------------------------

// ページャーから移動
if (Ldr_Pager::is_pager($_POST['move_pager'])) {
    //検索条件を取得
    $_SESSION['page_no'] = Ldr_Pager::get_page_no($_POST['page_no']);
}
// 編集画面から戻ってきた場合
else if (isset($_POST['ldr_return']) || isset($_SESSION['ldr_return'])) {
    // ページ番号の復旧
    $_POST["page_no"] = $_SESSION['page_no'];
}
// 外部から移動してきた場合
else {
    // セッション情報の初期化
    $_SESSION = array();
    $_POST["page_no"] = 1;
    $_SESSION['page_no'] = 1;
}
$_SESSION['ldr_return'] = null;
$_SESSION['menu'] = 'outside_course';
//----------------------------------------------------------
// データ
//----------------------------------------------------------
$dataCount = $outside->count_mst();
$current = Ldr_Pager::get_page_no($_POST['page_no'], $dataCount, $perPage);
$start = Ldr_Pager::calc_page_index($current, $perPage);
$list = $outside->lists_mst($start, $perPage);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $emp);
$view->assign('list', $list);

$view->assign(Ldr_Util::get_common_info('outside_course'));
$view->assign(Ldr_Pager::get_pager_info($_POST['page_no'], $dataCount, $perPage));
$view->display('trn_outside_course.tpl');
