<?php

require_once 'class/Base.php';
require_once 'class/InsideMst.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// Obj
//----------------------------------------------------------
$mst = new Ldr_InsideMst();
$view = new Cmx_View('ladder/templates');

//----------------------------------------------------------
// データ
//----------------------------------------------------------
if (!isset($_GET['id']) || !isset($_GET['number'])) {
    cmx_log(basename($_SERVER["PHP_SELF"]) . " parameter error");
    js_error_exit();
}
$data = $mst->find_date($_GET['id'], $_GET['date_id']);

foreach ($data['contents'] as $value) {
    if ($value['number'] === $_GET['number']) {
        $fdat = $value;
        break;
    }
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
header('Cache-Control: no-cache, must-revalidate');
header("Cache-Control: post-check=0, pre-check=0", false);
header('Pragma: no-cache');
$view->assign($fdat);
$view->assign('date', $data['date']);
$view->display("modal/inside_number.tpl");
