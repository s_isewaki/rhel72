<?php

require_once 'class/Base.php';
require_once 'class/NoviceGuideline.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_admin($session->emp());

//----------------------------------------------------------
// オブジェクト
//----------------------------------------------------------
$gl = new Ldr_NoviceGuideline();
$view = new Cmx_View('ladder/templates');

//----------------------------------------------------------
// データ
//----------------------------------------------------------
switch ($_REQUEST['mode']) {
    case 'revision':
        $data = $gl->find('revision', $_REQUEST['id']);
        break;

    case 'group1':
        $data = $gl->find('group1', $_REQUEST['id']);
        if (empty($data)) {
            $data['revision_id'] = $_COOKIE['pulldown_revision'];
        }
        $view->assign('revision_list', $gl->lists('revision'));
        break;

    case 'group2':
        $data = $gl->find('group2', $_REQUEST['id']);
        if (empty($data)) {
            $data['revision_id'] = $_COOKIE['pulldown_revision'];
            $data['group1'] = $_COOKIE['pulldown_group1'];
        }
        $view->assign('revision_list', $gl->lists('revision'));
        $view->assign('group1_list', $gl->lists('group1', $data));
        break;

    case 'guideline':
        $data = $gl->find('guideline', $_REQUEST['id']);
        if (empty($data)) {
            $data['revision_id'] = $_COOKIE['pulldown_revision'];
            $data['group1'] = $_COOKIE['pulldown_group1'];
            $data['group2'] = $_COOKIE['pulldown_group2'];
        }
        $view->assign('revision_list', $gl->lists('revision'));
        $view->assign('group1_list', $gl->lists('group1', $data));
        $view->assign('group2_list', $gl->lists('group2', $data));
        break;

    default:
        js_error_exit();
        break;
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
header('Cache-Control: no-cache, must-revalidate');
header("Cache-Control: post-check=0, pre-check=0", false);
header('Pragma: no-cache');
$view->display_with_fill("adm_novice_guideline/modal_{$_REQUEST['mode']}.tpl", array('fdat' => $data));
exit;
