<?php

require_once 'class/Base.php';
require_once 'class/Validation.php';
require_once 'class/Apply.php';
require_once 'class/Assessment.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// セッション初期化
//----------------------------------------------------------
Ldr_Util::session_init('ldr_common');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_ladder($emp);

//----------------------------------------------------------
// object
//----------------------------------------------------------
$as = new Ldr_Assessment();
$apply = new Ldr_Apply(array('apply_emp_id' => $_POST["target_emp_id"], 'accept_emp_id' => $emp->emp_id(), 'apply_id' => $_POST['id'], 'mode' => 'accept'));
$ladder = $apply->ladder();
$target_emp = new Ldr_Employee($_POST["target_emp_id"]);
$effective_post = Ldr_Util::use_tiket();

//----------------------------------------------------------
// デフォルト領域
//----------------------------------------------------------
$active = $_POST['active'] ? $_POST['active'] : 'sec1';
unset($_POST['active']);

//----------------------------------------------------------
// 様式7-1 保存
//----------------------------------------------------------
if ($effective_post && $_POST['btn_save'] === '71_save') {
    // バリデーション
    $error = $apply->validate_form71($_POST, $ladder['config']['rc']);

    // エラーが無ければ保存
    if (empty($error)) {
        try {
            $apply->save_form71($_POST);
            $completed = '保存しました。<br/>評価を終えるには「評価を完了する」ボタンをクリックしてください。';
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
        }
    }
}

//----------------------------------------------------------
// 様式7-2 保存
//----------------------------------------------------------
else if ($effective_post && $_POST['btn_save'] === '72_save') {
    try {
        $apply->save_form72($_POST);
        $completed = '保存しました。<br/>評価を終えるには「評価を完了する」ボタンをクリックしてください。';
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }
}

//----------------------------------------------------------
// 様式7-3 保存
//----------------------------------------------------------
else if ($effective_post && $_POST['btn_save'] === '73_save') {
    // バリデーション
    $error = $apply->validate_form73($_POST);

    // エラーが無ければ保存
    if (empty($error)) {
        try {
            $apply->save_form73($_POST);
            $completed = '保存しました。<br/>評価を終えるには「評価を完了する」ボタンをクリックしてください。';
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
        }
    }
}

//----------------------------------------------------------
// 様式7-1 自動保存
//----------------------------------------------------------
else if ($_POST['save'] === 'form71') {
    try {
        $apply->save_form71($_POST, 'auto');
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }
    exit;
}

//----------------------------------------------------------
// 様式7-2 自動保存
//----------------------------------------------------------
else if ($_POST['save'] === 'form72') {
    try {
        $apply->save_form72($_POST, 'auto');
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }
    exit;
}

//----------------------------------------------------------
// 様式7-3 自動保存
//----------------------------------------------------------
else if ($_POST['save'] === 'form73') {
    try {
        $apply->save_form73($_POST, 'auto');
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }
    exit;
}

//----------------------------------------------------------
// 評価完了
//----------------------------------------------------------
else if ($_POST['btn_save'] === 'complete') {
    $error = $apply->validate_appraiser();

    // エラーが無ければ保存
    if (empty($error)) {
        try {
            $ret = $apply->update_appraiser_completed();
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
        }
        if ($ret) {
            // 全員完了なら１ページ目
            unset($_SESSION['ldr_return']);
        }
        else {
            // 未完了なら元のページ
            $_SESSION['ldr_return'] = true;
        }
        header('Location:' . Ldr_Util::get_referer_url());
        exit;
    }
}

//----------------------------------------------------------
// 様式7データ
//----------------------------------------------------------
$find_data = $apply->find_form7();
unset($_POST['save']);
if (!empty($find_data)) {
    $fdat = array_merge((array)$find_data, (array)$_POST);
}
else {
    $fdat = $_POST;
}

//----------------------------------------------------------
// 本人コメント
//----------------------------------------------------------
$apply2 = new Ldr_Apply(array('apply_emp_id' => $_POST["target_emp_id"], 'apply_id' => $_POST['id'], 'mode' => 'apply'));
$apply_form7 = $apply2->find_form7();

//----------------------------------------------------------
// 領域リスト
//----------------------------------------------------------
$group1_list = $as->lists('group1', array('form7' => true, 'guideline_revision' => $apply->guideline_revision()));

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign(Ldr_Util::get_common_info($_POST['menu'], array('referer_url' => $_POST['owner_url'])));
$view->assign('emp', $emp);
$view->assign('target_emp', $target_emp);
$view->assign('apply', $apply);
$view->assign('ldr', $ladder);
$view->assign('level', $apply->level());
$view->assign('active', $active);
$view->assign('group1_list', $group1_list);
$view->assign('assessment_list', $apply->assessment_list());
$view->assign('padleft', count($ladder['config']['select']) <= 2 ? '60px' : count($ladder['config']['select']) * 34 + 10 . 'px');
$view->assign('is_completed', $apply->is_appraiser_completed($emp->emp_id()));
$view->assign('completed', $completed);
$view->assign('error', $error);

// 赤十字
if ($ladder['config']['flow'] === '1') {
    $view->display_with_fill('form_7edit_appraiser.tpl', array('fdat' => $fdat));
}
// 標準・順天堂
else {
    $view->assign('apply_form7', $apply_form7);
    $view->display_with_fill('form_7edit_appraiser_normal.tpl', array('fdat' => $fdat));
}
