<?php

require_once 'class/Base.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_inside($emp);

//----------------------------------------------------------
// オブジェクト
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
header('Cache-Control: no-cache, must-revalidate');
header("Cache-Control: post-check=0, pre-check=0", false);
header('Pragma: no-cache');

$id = explode('_', $_GET['id']);

$fdat = array('id' => $_GET['id'], 'status' => $_GET['status'], 'date_id' => $id[1]);
$view->display_with_fill("modal/inside_date_status.tpl", array('fdat' => $fdat));
