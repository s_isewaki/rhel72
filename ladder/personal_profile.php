<?php

require_once 'class/Base.php';
require_once 'class/Employee.php';
require_once 'class/Ladder.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_analysis($session->emp());

//----------------------------------------------------------
// Object
//----------------------------------------------------------
$employee = new Ldr_Employee();
$emp = $session->emp();
$ladder = new Ldr_Ladder();

//----------------------------------------------------------
// employee
//----------------------------------------------------------
$employee->find($_POST["target_emp_id"]);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $emp);
$view->assign('staff', $employee);
$view->assign('ladder', $ladder);
$view->assign(Ldr_Util::get_common_info('staff'));

$view->display('personal_profile.tpl');
