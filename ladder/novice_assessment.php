<?php

require_once 'class/Base.php';
require_once 'class/Novice.php';
require_once 'class/NoviceAssessment.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_ladder($session->emp());

//----------------------------------------------------------
// オブジェクト
//----------------------------------------------------------
$novice = new Ldr_Novice();
$as = new Ldr_NoviceAssessment($session->emp()->emp_id());
$conf = new Ldr_Config();

//----------------------------------------------------------
// 保存ボタン
//----------------------------------------------------------
if ($_POST['btn_save'] === 'btn_save') {
    try {
        $as->save($session->emp()->emp_id(), $_POST);
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }
}

//----------------------------------------------------------
// 自動保存
//----------------------------------------------------------
else if ($_POST['mode'] === 'save') {
    try {
        $as->save($session->emp()->emp_id(), $_POST, 'auto');
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }
    exit;
}

//----------------------------------------------------------
// コピーボタン
//----------------------------------------------------------
if ($_POST['btn_save'] === 'copy') {
    try {
        $as->copy($session->emp()->emp_id(), $_POST);
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }
}

//----------------------------------------------------------
// 版数
//----------------------------------------------------------
$current_revision = $as->current_revision();

//----------------------------------------------------------
// 一覧
//----------------------------------------------------------
$group1_list = $as->lists('group1', array('revision_id' => $current_revision));

//----------------------------------------------------------
// year
//----------------------------------------------------------
$year_list = $as->novice_year_lists();
$year = $_REQUEST['year'] ? $_REQUEST['year'] : $year_list[0];
if (!Validation::is_year($year)) {
    header('Location: dashboard.php');
    exit;
}

//----------------------------------------------------------
// number
//----------------------------------------------------------
$date_list = $as->date_lists($year);
$number = $_REQUEST['number'] ? $_REQUEST['number'] : $date_list[0]['number'];
if (!is_numeric($number)) {
    header('Location: dashboard.php');
    exit;
}

//----------------------------------------------------------
// fdat
//----------------------------------------------------------
$fdat = array(
    'year' => $year,
    'number' => $number,
);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $session->emp());
$view->assign(Ldr_Util::get_common_info('novice_assessment'));

$view->assign(array(
    'current_revision' => $current_revision,
    'group1_list' => $group1_list,
    'year_list' => $year_list,
    'date_list' => $date_list,
    'number' => $number,
));

$view->assign('active', $_POST['g1'] ? $_POST['g1'] : 'assessment');
$view->assign('g1', $_POST['g1']);
$view->assign('g2', $_POST['g2']);
$view->display_with_fill('novice_assessment.tpl', array('fdat' => $fdat));
