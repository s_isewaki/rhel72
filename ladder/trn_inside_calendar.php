<?php

require_once 'class/Base.php';
require_once 'class/InsideMst.php';


//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}
//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('inside_apply');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_ladder($emp);

//----------------------------------------------------------
// object
//----------------------------------------------------------
$inside = new Ldr_InsideMst($emp->emp_id());
$list = $inside->lists_calendar();

if (isset($_POST['ldr_return']) || isset($_SESSION['ldr_return'])) {
    $ret_flg = true;
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $session->emp());
$view->assign('list', $list);
$view->assign('return', $ret_flg);
$view->assign(Ldr_Util::get_common_info('inside_calendar'));

$view->display_with_fill('trn_inside_calendar.tpl', array('fdat' => $_POST));