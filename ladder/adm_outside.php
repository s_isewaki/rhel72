<?php

require_once 'class/Base.php';
require_once 'class/Pager.php';
require_once 'class/Outside.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
    exit;
}

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('adm_outside');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_mst_outside($emp);

//----------------------------------------------------------
// object
//----------------------------------------------------------
$outside = new Ldr_Outside($emp->emp_id());
$perPage = Ldr_Const::SRH_PERPAGE;

//----------------------------------------------------------
// 遷移状態判別
//----------------------------------------------------------

// ページャーから移動
if (Ldr_Pager::is_pager($_POST['move_pager'])) {
    //検索条件を取得
    $_SESSION['page_no'] = Ldr_Pager::get_page_no($_POST['page_no']);
}
// 検索
else if ($_POST['search']) {
    $srh_condition["srh_start_date"] = $_POST['srh_start_date'];
    $srh_condition["srh_last_date"] = $_POST['srh_last_date'];

    $_POST["page_no"] = 1;

    // 検索条件を設定
    $_SESSION['srh_condition'] = $srh_condition;
}
// 編集画面から戻ってきた場合
else if (isset($_POST['ldr_return']) || isset($_SESSION['ldr_return'])) {
    // 検索条件の復旧
    $srh_condition = $_SESSION['srh_condition'];
    $_POST['srh_start_date'] = $srh_condition['srh_start_date'];
    $_POST['srh_last_date'] = $srh_condition['srh_last_date'];

    // ページ番号の復旧
    $_POST["page_no"] = $_SESSION['page_no'];
}
// 外部から移動してきた場合
else {
    // セッション情報の初期化
    $_SESSION = array();
    $_POST["page_no"] = 1;
    $_SESSION['page_no'] = 1;

    // 検索日の初期値を設定
    $_POST['srh_start_date'] = date("Y-m-d");
    $srh_condition["srh_start_date"] = $_POST['srh_start_date'];

    $_SESSION['srh_condition'] = $srh_condition;
}
$_SESSION['ldr_return'] = null;
$_SESSION['menu'] = 'adm_outside';
//----------------------------------------------------------
// データ
//----------------------------------------------------------
$srh_condition['mst'] = true;
$dataCount = $outside->count_mst($srh_condition);
$current = Ldr_Pager::get_page_no($_POST['page_no'], $dataCount, $perPage);
$start = Ldr_Pager::calc_page_index($current, $perPage);
$list = $outside->lists_mst($start, $perPage, $srh_condition);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $emp);
$view->assign('list', $list);

$view->assign(Ldr_Util::get_common_info($_SESSION['menu']));
$view->assign(Ldr_Pager::get_pager_info($_POST['page_no'], $dataCount, $perPage));
$view->display('adm_outside.tpl');


