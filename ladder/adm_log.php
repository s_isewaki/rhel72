<?php

require_once 'class/Base.php';
require_once 'class/Pager.php';

//１ページの表示件数
$perPage = Ldr_Const::SRH_PERPAGE;

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('adm_log');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_admin($session->emp());

//----------------------------------------------------------
// Object
//----------------------------------------------------------
$log = new Ldr_Log('log');

//----------------------------------------------------------
// 遷移状態判別
//----------------------------------------------------------

// ページャーから移動
if (Ldr_Pager::is_pager($_POST['move_pager'])) {
    //検索条件を取得
    $_SESSION['page_no'] = Ldr_Pager::get_page_no($_POST['page_no']);
    $srh_condition = $_SESSION['srh_condition'];
}

// 検索ボタン
else if ($_POST['btn_search']) {

    $_SESSION["srh_name"] = $srh_condition["srh_name"] = $_POST['srh_name'];
    $_SESSION["srh_function"] = $srh_condition["srh_function"] = $_POST['srh_function'];
    $_SESSION["srh_start_date"] = $srh_condition["srh_start_date"] = $_POST['srh_start_date'];
    $_SESSION["srh_last_date"] = $srh_condition["srh_last_date"] = $_POST['srh_last_date'];

    $_POST["page_no"] = 1;

    // 検索条件を設定
    $_SESSION['srh_condition'] = $srh_condition;
}

// 外部から移動してきた場合
else {
    // セッション情報の初期化
    $_POST["page_no"] = 1;
    $_SESSION['page_no'] = 1;
    $_SESSION['srh_condition'] = null;
}
$_SESSION['ldr_return'] = null;

//----------------------------------------------------------
// 一覧
//----------------------------------------------------------
// データ数の取得
$dataCount = $log->count($srh_condition);
$current = Ldr_Pager::get_page_no($_POST['page_no'], $dataCount, $perPage);
$start = Ldr_Pager::calc_page_index($current, $perPage);

// データの取得
$list = $log->lists($start, $perPage, $srh_condition);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $session->emp());
$view->assign(Ldr_Util::get_common_info('adm_log'));
$view->assign(Ldr_Pager::get_pager_info($_POST['page_no'], $dataCount, $perPage));
$view->assign('list', $list);
$view->assign('logfunction', $log->function_name());
$view->assign('search', $_SESSION['srh_condition'] ? true : false);

$view->display_with_fill('adm_log.tpl', array('fdat' => $_POST));
