<?php

require_once 'class/Base.php';
require_once 'class/Employee.php';
require_once 'class/Pager.php';
require_once 'class/Classmst.php';
require_once 'class/Ladder.php';

//１ページの表示件数
$perPage = Ldr_Const::SRH_PERPAGE;

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('adm_employee');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_admin($session->emp());

//----------------------------------------------------------
// オブジェクト
//----------------------------------------------------------
$emp = new Ldr_Employee();
$conf = new Ldr_Config();
$classmst = new Ldr_Classmst();
$ladder = new Ldr_Ladder();

//----------------------------------------------------------
// エクスポート
//----------------------------------------------------------
if ($_POST['mode'] === 'export') {
    $csv = to_sjis($emp->export_csv());

    // output
    ob_clean();
    header("Content-Disposition: attachment; filename=ladder_staff.csv");
    header("Content-Type: application/octet-stream; name=ladder_staff.csv");
    header("Content-Length: " . strlen($csv));
    echo($csv);
    ob_end_flush();
    exit;
}

//----------------------------------------------------------
// インポート
//----------------------------------------------------------
elseif ($_POST['mode'] === 'import') {
    if (array_key_exists("csvfile", $_FILES)) {
        if ($_FILES["csvfile"]["error"] !== 0 or $_FILES["csvfile"]["size"] <= 0) {
            header('X-Content-Type-Options: nosniff');
            print cmx_json_encode(array(
                'error' => true,
                'msg' => 'ファイルのアップロードに失敗しました',
            ));
            exit;
        }
    }
    try {
        $res = $emp->import_csv($_FILES["csvfile"]);
    }
    catch (Exception $e) {
        header('X-Content-Type-Options: nosniff');
        print cmx_json_encode(array(
            'error' => true,
            'msg' => 'CSVのインポートに失敗しました',
            'result' => explode(',', $e->getMessage())
        ));
        exit;
    }

    header('X-Content-Type-Options: nosniff');
    print cmx_json_encode(array(
        'error' => false,
        'msg' => $res,
    ));
    exit;
}

//----------------------------------------------------------
// CoMedix連携
//----------------------------------------------------------
if ($_POST['mode'] === 'comedix') {
    try {
        $res = $emp->comedix_import($_POST["status"]);
    }
    catch (Exception $e) {
        exit;
    }
}

//----------------------------------------------------------
// 遷移状態判別
//----------------------------------------------------------

// ページャーから移動
if (Ldr_Pager::is_pager($_POST['move_pager'])) {
    //検索条件を取得
    $_SESSION['page_no'] = Ldr_Pager::get_page_no($_POST['page_no']);
    $srh_condition = $_SESSION['srh_condition'];
}

// 検索ボタン
else if ($_POST['btn_search']) {
    $srh_condition["srh_keyword"] = $_POST['srh_keyword'];
    $srh_condition["srh_class1"] = $_POST['srh_class1'];
    $srh_condition["srh_class3"] = $_POST['srh_class3'];

    $_POST["page_no"] = 1;

    // 検索条件を設定
    $_SESSION['srh_condition'] = $srh_condition;
}

// 削除/編集画面から戻ってきた場合
else if (isset($_POST['ldr_return']) || isset($_SESSION['ldr_return']) || (isset($_POST['del_inner_id']) && $_POST['del_inner_id'] !== "")) {
    // ページ番号の復旧
    $_POST["page_no"] = $_SESSION['page_no'];
    // 検索条件の復旧
    $srh_condition = $_SESSION['srh_condition'];
}

// 外部から移動してきた場合
else {
    // セッション情報の初期化
    $_POST["page_no"] = 1;
    $_SESSION['page_no'] = 1;
    $_SESSION['srh_condition'] = null;
}
$_SESSION['ldr_return'] = null;
$fdat = array_merge((array)$_POST, (array)$srh_condition);

//----------------------------------------------------------
// 一覧
//----------------------------------------------------------
$count = $emp->count($srh_condition);
$current = Ldr_Pager::get_page_no($_POST['page_no'], $count, $perPage);
$start = Ldr_Pager::calc_page_index($current, $perPage);
$condition = array(
    'order_by' => 'ldr_employee.updated_on IS NULL asc, ldr_employee.updated_on desc, empmst.emp_id asc',
    'per_page' => $perPage,
    'start' => $start
);
$options = array_merge((array)$condition, (array)$srh_condition);
$lists = $emp->lists($options);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $session->emp());
$view->assign('ladder', $ladder);
$view->assign('view_class', $conf->view_class());
$view->assign(Ldr_Util::get_common_info('adm_employee'));
$view->assign(Ldr_Pager::get_pager_info($_POST['page_no'], $count, $perPage));
$view->assign('list', $lists);
$view->assign('class_list', $classmst->lists());
$view->assign('class12_list', $classmst->class12_lists());
$view->assign('srh_class3', $fdat['srh_class3']);
$view->display_with_fill('adm_employee.tpl', array('fdat' => $fdat));
