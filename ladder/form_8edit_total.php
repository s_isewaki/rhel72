<?php

require_once 'class/Base.php';
require_once 'class/Validation.php';
require_once 'class/Apply.php';
require_once 'class/Assessment.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// セッション初期化
//----------------------------------------------------------
Ldr_Util::session_init('ldr_common');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_ladder($emp);

//----------------------------------------------------------
// object
//----------------------------------------------------------
$as = new Ldr_Assessment();
$apply = new Ldr_Apply(array('apply_emp_id' => $_POST["target_emp_id"], 'accept_emp_id' => 'total', 'apply_id' => $_POST['id'], 'mode' => 'accept'));
$ladder = $apply->ladder();
$target_emp = new Ldr_Employee($_POST["target_emp_id"]);

//----------------------------------------------------------
// デフォルト領域
//----------------------------------------------------------
$active = $_POST['active'];
unset($_POST['active']);

//----------------------------------------------------------
// 様式7-2 保存
//----------------------------------------------------------
if ($_POST['btn_save'] === '72_save') {
    try {
        $apply->save_form72($_POST);
        $completed = '保存しました。<br/>評価を終えるには「評価を完了する」ボタンをクリックしてください。';
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }
}

//----------------------------------------------------------
// 様式7-3 保存
//----------------------------------------------------------
else if ($_POST['btn_save'] === '73_save') {
    try {
        $apply->save_form73($_POST);
        $completed = '保存しました。<br/>評価を終えるには「評価を完了する」ボタンをクリックしてください。';
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }
}

//----------------------------------------------------------
// 様式7-2 自動保存
//----------------------------------------------------------
else if ($_POST['save'] === 'form72') {
    try {
        $apply->save_form72($_POST, 'auto');
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }
    exit;
}

//----------------------------------------------------------
// 様式7-3 自動保存
//----------------------------------------------------------
else if ($_POST['save'] === 'form73') {
    try {
        $apply->save_form73($_POST, 'auto');
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }
    exit;
}

//----------------------------------------------------------
// 評価完了
//----------------------------------------------------------
else if ($_POST['btn_save'] === 'complete') {
    $error = $apply->validate_total();

    // エラーが無ければ保存
    if (empty($error)) {
        try {
            $apply->update_total_completed($emp->emp_id());
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
        }
        unset($_SESSION['ldr_return']);
        header('Location:' . Ldr_Util::get_referer_url());
        exit;
    }
}

//----------------------------------------------------------
// 様式7データ
//----------------------------------------------------------
$form1 = $apply->find_form1($_POST['id']);
$form7 = $apply->lists_form7();
$assessment_list = $apply->assessment_list();
$assessment_list_all = $apply->assessment_list_all();
$form1['appr_list_all'][] = array('emp_id' => $target_emp->emp_id(), 'emp_name' => $target_emp->emp_name());
$form1['appr_list_all'] = array_merge($form1['appr_list_all'], $form1['appr_list']);
unset($_POST['save']);
if (!empty($form7)) {
    $fdat = array_merge((array)$form7['total'], (array)$_POST);
}
else {
    $fdat = $_POST;
}

//----------------------------------------------------------
// 本人コメント
//----------------------------------------------------------
$apply2 = new Ldr_Apply(array('apply_emp_id' => $_POST["target_emp_id"], 'apply_id' => $_POST['id'], 'mode' => 'apply'));
$apply_form7 = $apply2->find_form7();

//----------------------------------------------------------
// 領域リスト
//----------------------------------------------------------
$group1_list = $as->lists('group1', array('form7' => true, 'guideline_revision' => $apply->guideline_revision()));

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign(Ldr_Util::get_common_info($_POST['menu'], array('referer_url' => $_POST['owner_url'])));
$view->assign('emp', $emp);
$view->assign('target_emp', $target_emp);
$view->assign('apply', $apply);
$view->assign('ldr', $ladder);
$view->assign('level', $apply->level());
$view->assign('form1', $form1);
$view->assign('form7', $form7);
$view->assign('apply_form7', $apply_form7);
$view->assign('active', $active);
$view->assign('group1_list', $group1_list);
$view->assign('assessment_list', $assessment_list);
$view->assign('assessment_list_all', $assessment_list_all);
$view->assign('padleft', count($ladder['config']['select']) <= 2 ? '60px' : count($ladder['config']['select']) * 34 + 10 . 'px');
$view->assign('is_completed', $apply->is_total_completed());
$view->assign('completed', $completed);
$view->assign('error', $error);
$view->display_with_fill('form_8edit_total.tpl', array('fdat' => $fdat));
