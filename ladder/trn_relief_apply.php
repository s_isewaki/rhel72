<?php

require_once 'class/Base.php';
require_once 'class/Relief.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
    exit;
}

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('trn_relief');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_ladder($emp);

//----------------------------------------------------------
// object
//----------------------------------------------------------
$relief = new Ldr_Relief($emp->emp_id());
$conf = new Ldr_Config();


//----------------------------------------------------------
// 保存
//----------------------------------------------------------
if ($_POST['btn_save'] === 'save') {
    // バリデーション
    $error = $relief->validate('apply', $_POST);

    // エラーが無ければ保存
    if (empty($error)) {
        try {
            if ($_POST['rd_branch'] === '1') {
                $_POST['branch'] = $conf->jrc_branch_name();
            }
            if ($_POST['rd_hospital'] === '1') {
                $_POST['hospital'] = $conf->hospital_name();
            }

            $relief->save('apply', $_POST);
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
        }

        $_SESSION['ldr_return'] = true;
        header('Location: trn_relief.php');
        exit;
    }
}
if ($_POST['btn_save'] === 'cancel') {
    try {
        $relief->save('apply_cancel', $_POST);
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }

    $_SESSION['ldr_return'] = true;
    header('Location:' . Ldr_Util::get_referer_url());
    exit;
}
//----------------------------------------------------------
// データの取得
//----------------------------------------------------------
if ($_POST['mode'] === 'apply') {
    $_SESSION['edit'] = $_POST['mode'];
    $id = explode('_', $_POST['id']);
    if (count($id) !== 2) {
        cmx_log("trn_relief_apply.php pram error id={$_POST['id']}");
        header("Location: dashboard.php");
    }

    $_POST['relief_id'] = $id[0];
    $_POST['unit'] = $id[1];
}
else if ($_POST['mode'] === 'edit' || $_POST['mode'] === 'cancel') {
    $_SESSION['edit'] = $_POST['mode'];

    if (empty($_POST['id'])) {
        cmx_log("trn_relief_apply.php pram error id={$_POST['id']}");
        header("Location: dashboard.php");
    }

    $fdat = $relief->find('apply', $_POST['id']);
}

if (empty($fdat)) {
    $fdat = $relief->find('contents', $_POST['relief_id'], $_POST['unit']);
}

//----------------------------------------------------------
// データの変換
//----------------------------------------------------------
$data = array(
    'branch_def' => $conf->jrc_branch_name(),
    'hospital_def' => $conf->hospital_name(),
);

if ($fdat["branch"] === $data['branch_def']) {
    $fdat["rd_branch"] = "1";
    $fdat["branch"] = "";
}
else if (!is_null($fdat["branch"])) {
    $fdat["rd_branch"] = '2';
}
else {
    $fdat['rd_branch'] = 1;
}
if ($fdat["hospital"] === $data['hospital_def']) {
    $fdat["rd_hospital"] = "1";
    $fdat["hospital"] = "";
}
else if (!is_null($fdat["hospital"])) {
    $fdat["rd_hospital"] = '2';
}
else {
    $fdat['rd_hospital'] = 1;
}
//----------------------------------------------------------
// 承認者
//----------------------------------------------------------
$approver = $emp->approver();

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $session->emp());
$view->assign(Ldr_Util::get_common_info('relief_course'));
$view->assign('relief', $fdat);
$view->assign('edit', $_SESSION['edit']);
$view->assign($data);
$view->assign('approver', empty($approver) ? null : $approver);
if (empty($error)) {
    $view->assign('readonly', $fdat['mst_read']);
    $view->display_with_fill('trn_relief_apply.tpl', array('fdat' => $fdat));
}
else {
    $view->assign('error', $error);
    $view->display_with_fill('trn_relief_apply.tpl', array('fdat' => $_POST));
}
