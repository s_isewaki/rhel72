<?php

require_once 'class/Base.php';
require_once 'class/Pager.php';
require_once 'class/Employee.php';
require_once 'class/AnalysisLadder.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
    exit;
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_analysis($session->emp());

//----------------------------------------------------------
// object
//----------------------------------------------------------
$analysis = new Ldr_AnalysisLadder();
$ladder = new Ldr_Ladder();

//----------------------------------------------------------
// 遷移状態判別
//----------------------------------------------------------
Ldr_Util::session_init('ldr_common');

// 戻る
if (isset($_POST['ldr_return'])) {
    $srh_condition = $_SESSION['srh_condition'];
    $_POST["page_no"] = $_SESSION['page_no'];
}

// ページャーから移動
else if (Ldr_Pager::is_pager($_POST['move_pager'])) {
    //検索条件を取得
    $_SESSION['page_no'] = Ldr_Pager::get_page_no($_POST['page_no']);
    $srh_condition = $_SESSION['srh_condition'];
}

// 検索ボタン
else if ($_POST['btn_search']) {
    $srh_condition["srh_keyword"] = $_POST['srh_keyword'];
    $srh_condition["srh_class3"] = $_POST['srh_class3'];
    $_POST["page_no"] = 1;

    // 検索条件を設定
    $_SESSION['srh_condition'] = $srh_condition;
}

// ソート
else if ($_REQUEST['order']) {
    $_POST["page_no"] = 1;

    $desc = $_REQUEST['desc'] ? 'DESC' : 'ASC';

    switch ($_REQUEST['order']) {
        case 'st_order_no':
            $order = "st_order_no $desc, ldr1.level IS NULL ASC, ldr1.level DESC";
            break;

        default:
            $order = "{$_REQUEST['order']} IS NULL ASC, {$_REQUEST['order']} $desc";
            break;
    }
}

// 外部から移動してきた場合
else {
    // セッション情報の初期化
    $_POST["page_no"] = 1;
    $_SESSION['page_no'] = 1;
    $_SESSION['srh_condition'] = array();
}

$_SESSION['ldr_return'] = null;
$fdat = array_merge((array)$_POST, (array)$srh_condition);

//----------------------------------------------------------
// 部署リスト
//----------------------------------------------------------
$full = false;
if ($emp->is_analysis() || $emp->committee() || $emp->is_admin()) {
    $class_lists = $analysis->lists('class');
    $full = true;
}
else {
    $class_lists = $analysis->lists_class_approver($emp->emp_id());
    if (empty($srh_condition['srh_class3'])) {
        $srh_condition['srh_class3'] = implode("_", array($emp->dept_id(), $emp->room_id()));
    }
}

//----------------------------------------------------------
// 職員リスト
//----------------------------------------------------------
$perPage = 50;
$count = $emp->count($srh_condition);
$current = Ldr_Pager::get_page_no($_POST['page_no'], $count, $perPage);
$start = Ldr_Pager::calc_page_index($current, $perPage);
$condition = array(
    'order_by' => $order ? "$order, empmst.emp_kn_lt_nm, empmst.emp_kn_ft_nm" : 'st_order_no, ldr1.level IS NULL ASC, ldr1.level DESC, empmst.emp_kn_lt_nm, empmst.emp_kn_ft_nm',
    'per_page' => $perPage,
    'start' => $start
);
$options = array_merge((array)$condition, (array)$srh_condition);
$lists = $emp->lists($options);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign(Ldr_Util::get_common_info('staff'));
$view->assign(Ldr_Pager::get_pager_info($_POST['page_no'], $count, $perPage));
$view->assign('emp', $session->emp());
$view->assign('ladder', $ladder);
$view->assign('class_lists', $class_lists);
$view->assign('full', $full);
$view->assign('data', $_POST);
$view->assign('list', $lists);
$view->assign('order', $_REQUEST['order']);
$view->assign('desc', $_REQUEST['desc']);
$view->display_with_fill('staff.tpl', array('fdat' => $fdat));
