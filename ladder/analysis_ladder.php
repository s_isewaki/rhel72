<?php

require_once 'class/Base.php';
require_once 'class/AnalysisLadder.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
    exit;
}

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('als_ladder');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_analysis($session->emp());

//----------------------------------------------------------
// object
//----------------------------------------------------------
$analysis = new Ldr_AnalysisLadder();

//----------------------------------------------------------
// 検索処理
//----------------------------------------------------------
if ($_POST['mode'] === 'search') {
    $lists = $analysis->lists('ladder', $_POST);
    $_SESSION['ANALYSYS_LADDER_POST'] = $_POST;
    $_SESSION['ANALYSYS_LADDER_LIST'] = $lists;
}

//----------------------------------------------------------
// csv出力
//----------------------------------------------------------
else if ($_POST['mode'] === 'csv' and ! empty($_SESSION['ANALYSYS_LADDER_POST'])) {
    $csv = to_sjis($analysis->export_csv($_SESSION['ANALYSYS_LADDER_LIST'], $_SESSION['ANALYSYS_LADDER_POST']));
    $filename = "analysis_ladder_{$_SESSION['ANALYSYS_LADDER_POST']['y']}_{$_SESSION['ANALYSYS_LADDER_POST']['x']}.csv";

    // output
    ob_clean();
    header("Content-Disposition: attachment; filename=$filename");
    header("Content-Type: application/octet-stream; name=$filename");
    header("Content-Length: " . strlen($csv));
    echo($csv);
    ob_end_flush();
    exit;
}

//----------------------------------------------------------
// 部署リスト
//----------------------------------------------------------
if ($emp->is_analysis() or $emp->committee() or $emp->is_admin()) {
    $class_lists = $analysis->lists('class');
}
else {
    $class_lists[] = array(
        'dept_id' => $emp->dept_id(),
        'name' => $emp->class_full_name(),
    );
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $session->emp());
$view->assign(Ldr_Util::get_common_info('analysis_ladder'));
$view->assign('axis_lists', $analysis->axis_lists());
$view->assign('class_lists', $class_lists);

//----------------------------------------------------------
// 印刷
//----------------------------------------------------------
if ($_POST['mode'] === 'print' and ! empty($_SESSION['ANALYSYS_LADDER_POST'])) {
    require_once 'class/PDF.php';

    list($pl, $size) = explode(':', $_POST['id']);

    $pdf = new Ldr_PDF($pl, 'mm', $size, true, 'UTF-8', false);
    $pdf->setHeaderTitle('ラダー分析');
    $pdf->setEmployee($target_emp);
    $pdf->setPrinter($session->emp());

    $pdf->AddPage();

    $view->assign('data', $_SESSION['ANALYSYS_LADDER_POST']);
    $view->assign('lists', $_SESSION['ANALYSYS_LADDER_LIST']);
    $html = $view->fetch('print/analysis_ladder.tpl');

    $pdf->writeHTML(to_utf8($html), true, 0, false, 0);
    $pdf->Output("analysis_ladder.pdf", "I");
    exit;
}

//----------------------------------------------------------
// HTML
//----------------------------------------------------------
$view->assign('data', $_POST);
$view->assign('lists', $lists);
$view->display('analysis_ladder.tpl');
