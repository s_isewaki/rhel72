<?php

require_once 'class/Base.php';
require_once 'class/Validation.php';
require_once 'class/Apply.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_ladder($emp);

//----------------------------------------------------------
// object
//----------------------------------------------------------
$apply = new Ldr_Apply(array('apply_emp_id' => $_POST["target_emp_id"], 'accept_emp_id' => $emp->emp_id(), 'apply_id' => $_POST['id']));
$target_emp = new Ldr_Employee($_POST["target_emp_id"]);
$form8 = $apply->find_form8();
$ladder = $apply->ladder();

//----------------------------------------------------------
// 申請情報取得
//----------------------------------------------------------
$form1 = $apply->find_form1($_POST['id']);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign(Ldr_Util::get_common_info($_POST['menu'], array('referer_url' => $_POST['owner_url'])));
$view->assign('emp', $emp);
$view->assign('target_emp', $target_emp);
$view->assign('apply', $apply);
$view->assign('ldr', $ladder);
$view->assign('form1', $form1);
$view->assign('form8', $form8);

//----------------------------------------------------------
// 印刷
//----------------------------------------------------------
if ($_POST['mode'] === 'print') {
    require_once 'class/PDF.php';
    $conf = new Ldr_Config();
    $title = $conf->titles();
    $html = $view->output_with_fill('print/form_8view.tpl', array('fdat' => $_POST));

    $pdf = new Ldr_PDF('P', 'mm', 'A4', true, 'UTF-8', false);
    $pdf->setHeaderTitle("[{$ladder['config']['form_8']['title']}] {$ladder['config']['form_8']['detailL']}");
    $pdf->setEmployee($target_emp);
    $pdf->setPrinter($session->emp());
    $pdf->AddPage();
    $pdf->writeHTML(to_utf8($html), true, 0, false, 0);
    $pdf->Output("form_8view.pdf", "I");
    exit;
}

//----------------------------------------------------------
// 表示
//----------------------------------------------------------
$view->display_with_fill('form_8view.tpl', array('fdat' => $_POST));
