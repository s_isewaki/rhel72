<?php

require_once 'class/Base.php';
require_once 'class/Apply.php';
require_once 'class/Ladder.php';
require_once 'class/Information.php';
require_once 'class/Inside.php';
require_once 'class/Outside.php';
require_once 'class/Relief.php';

//----------------------------------------------------------
// ���å����
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

$emp = $session->emp();

//----------------------------------------------------------
// ����
//----------------------------------------------------------
Ldr_Util::qualify_ladder($emp);

//----------------------------------------------------------
// PHP���å����
//----------------------------------------------------------
Ldr_Util::session_init('ldr_common');
$_SESSION = array();

//----------------------------------------------------------
// object
//----------------------------------------------------------
$apply = new Ldr_Apply(array('apply_emp_id' => $emp->emp_id(), 'apply_id' => 'recently'));
$ladder = new Ldr_Ladder();
$info = new Ldr_Information($emp, $apply);
$inside = new Ldr_Inside($emp->emp_id());
$outside = new Ldr_Outside($emp->emp_id());
$relief = new Ldr_Relief($emp->emp_id());

//----------------------------------------------------------
// �������Ľ
//----------------------------------------------------------
if ($emp->committee()) {
    $ldr_prog1 = array(
        'one_week_ago' => date("Y-m-d", strtotime("-1 week")),
    );
    $ldr_prog2 = $apply->count_apply_status();
    $ldr_progress_info = array_merge((array)$ldr_prog1, (array)$ldr_prog2);
}

//----------------------------------------------------------
// ����
//----------------------------------------------------------
$form_updated_on = $apply->form_updated_on($apply->apply_id());
$apply_list = $apply->lists_apply(null, null, array('recently' => 'on'));

//----------------------------------------------------------
// ����
//----------------------------------------------------------
$accept_list = $apply->lists_accept(0, 10, array('active_only' => 'on'));

//----------------------------------------------------------
// ���⸦��
//----------------------------------------------------------
$srh_cond_inside['accepted'] = true;
$list_inside = $inside->lists_apply(null, null, $srh_cond_inside);

//----------------------------------------------------------
// ��������
//----------------------------------------------------------
$srh_cond_outside['accepted'] = true;
$list_outside = $outside->lists($srh_cond_outside);

//----------------------------------------------------------
// �߸������
//----------------------------------------------------------
$srh_cond_relief['accepted'] = true;
$list_relief = $relief->lists("apply", null, null, null, $srh_cond_relief);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign(Ldr_Util::get_common_info('dashboard'));
$view->assign('emp', $session->emp());
$view->assign('apply', $apply);
$view->assign('ldr', $apply->ladder());
$view->assign($ldr_progress_info);
$view->assign($form_updated_on);
$view->assign('info', $info->lists());
$view->assign('apply_list', $apply_list);
$view->assign('accept_list', $accept_list);
$view->assign('list_inside', $list_inside);
$view->assign('list_outside', $list_outside);
$view->assign('list_relief', $list_relief);
$view->assign('status_lists', $ladder->status_lists());
$view->assign('is_report', $ladder->is_report());
$view->display('dashboard.tpl');
