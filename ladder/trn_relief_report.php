<?php

require_once 'class/Base.php';
require_once 'class/Relief.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
    exit;
}

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('trn_relief');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_ladder($emp);

//----------------------------------------------------------
// object
//----------------------------------------------------------
$relief = new Ldr_Relief($emp->emp_id());

//----------------------------------------------------------
// 保存
//----------------------------------------------------------
if (isset($_POST['btn_save'])) {
    // バリデーション
    $error = $relief->validate('report', $_POST);

    // エラーが無ければ保存
    if (empty($error)) {
        try {
            $relief->save('report', $_POST);
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
        }

        $_SESSION['ldr_return'] = true;
        header('Location: ' . Ldr_Util::get_referer_url());

        exit;
    }
}

//----------------------------------------------------------
// データ
//----------------------------------------------------------
$trn = $relief->find('apply', $_POST['id']);
$fdat = array_merge((array)$_POST, (array)$trn);
$data = array(
    'trn' => $trn,
);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $session->emp());
$view->assign(Ldr_Util::get_common_info('relief'));
$view->assign($data);

if (empty($error)) {
    $view->display_with_fill('trn_relief_report.tpl', array('fdat' => $fdat));
}
else {
    $view->assign('error', $error);
    $view->display_with_fill('trn_relief_report.tpl', array('fdat' => $_POST));
}
