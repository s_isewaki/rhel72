<?php

require_once 'class/Base.php';
require_once 'class/Item.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_committee($emp);

//----------------------------------------------------------
// object
//----------------------------------------------------------
$item = new Ldr_Item();

//----------------------------------------------------------
// パラメータ
//----------------------------------------------------------
$type = $_REQUEST['type'] ? $_REQUEST['type'] : '21title';

//----------------------------------------------------------
// CSV取り出し
//----------------------------------------------------------
if ($_POST['mode'] === 'export') {

    $csv = to_sjis($item->export_csv($type));

    // output
    ob_clean();
    header("Content-Disposition: attachment; filename=$type.csv");
    header("Content-Type: application/octet-stream; name=$type.csv");
    header("Content-Length: " . strlen($csv));
    echo($csv);
    ob_end_flush();
    exit;
}

//----------------------------------------------------------
// CSV取り込み
//----------------------------------------------------------
if ($_POST['mode'] === 'import') {
    // ファイルのチェック
    $file_err = Ldr_Util::exist_upload_file();
    if (!empty($file_err)) {
        print cmx_json_encode($file_err);
        exit;
    }

    // インポート
    try {
        $res = $item->import_csv($type, $_FILES["csvfile"]);
    }
    catch (Exception $e) {
        print cmx_json_encode(array(
            'error' => true,
            'msg' => 'CSVのインポートに失敗しました',
            'result' => explode(',', $e->getMessage())
        ));
        exit;
    }
    print cmx_json_encode(array(
        'error' => false,
        'msg' => $res,
    ));
    exit;
}

//----------------------------------------------------------
// 更新
//----------------------------------------------------------
if ($_POST['mode'] === 'update') {
    if (!empty($_POST['id'])) {
        try {
            $item->update($_POST);
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
        }
    }
}

//----------------------------------------------------------
// 削除
//----------------------------------------------------------
if ($_POST['mode'] === 'remove') {
    if (!empty($_POST['id'])) {
        try {
            $item->remove($_POST['id']);
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
        }
    }
}

//----------------------------------------------------------
// 並べ替え
//----------------------------------------------------------
if ($_POST['mode'] === 'sort') {
    if (!empty($_POST['order'])) {
        try {
            $item->sort($_POST);
            $completed = true;
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
        }
    }
}

//----------------------------------------------------------
// 記録取り込み
//----------------------------------------------------------
if ($_POST['mode'] === 'import_record') {
    try {
        $item->import_record($type);
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign(Ldr_Util::get_common_info('adm_item_mst'));
$view->assign('emp', $emp);
$view->assign('item', $item);
$view->assign('type', $type);
$view->assign('completed', $completed);
$view->display_with_fill('adm_item_mst.tpl', array('fdat' => array('type' => $type)));
