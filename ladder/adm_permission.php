<?php

require_once 'class/Base.php';
require_once 'class/Approver.php';
require_once 'class/Committee.php';
require_once 'class/PermissionOutside.php';
require_once 'class/PermissionRelief.php';
require_once 'class/PermissionAnalysis.php';
require_once 'class/PermissionInside.php';
require_once 'class/PermissionTraining.php';
require_once 'class/Classmst.php';
require_once 'class/Config.php';

//----------------------------------------------------------
// ���å����
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// PHP���å����
//----------------------------------------------------------
Ldr_Util::session_init('adm_permission');

//----------------------------------------------------------
// ����
//----------------------------------------------------------
Ldr_Util::qualify_admin($session->emp());

//----------------------------------------------------------
// ���֥�������
//----------------------------------------------------------
$conf = new Ldr_Config();
$appr = new Ldr_Approver();
$comm = new Ldr_Committee();
$inside = new Ldr_PermissionInside();
$outside = new Ldr_PermissionOutside();
$relief = new Ldr_PermissionRelief();
$training = new Ldr_PermissionTraining();
$analysis = new Ldr_PermissionAnalysis();
$classmst = new Ldr_Classmst();

$effective_post = Ldr_Util::use_tiket();

//----------------------------------------------------------
// ��¸
//----------------------------------------------------------
if ($effective_post && $_POST['mode'] === 'save') {
    // �Ѱ��������Ͽ
    if ($_POST['tab'] === 'committee') {
        // ��¸����
        try {
            $comm->save($_POST);
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
        }
    }

    // ������ǧ����Ͽ
    else if ($_POST['tab'] === 'approver') {
        // ��¸����
        try {
            $appr->save($_POST);
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
        }
    }
    // ���⸦��
    else if ($_POST['tab'] === 'inside') {
        // ��¸����
        try {
            $inside->save($_POST);
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
        }
    }
    // ��������
    else if ($_POST['tab'] === 'outside') {
        // ��¸����
        try {
            $outside->save($_POST);
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
        }
    }
    // �߸������
    else if ($_POST['tab'] === 'relief') {
        // ��¸����
        try {
            $relief->save($_POST);
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
        }
    }
    // ����������
    else if ($_POST['tab'] === 'training') {
        // ��¸����
        try {
            $training->save($_POST);
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
        }
    }
    // ʬ��ô��
    else if ($_POST['tab'] === 'analysis') {
        // ��¸����
        try {
            $analysis->save($_POST);
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
        }
    }
    $completed = true;
}
$active = isset($_POST['tab']) ? $_POST['tab'] : 'approver';
unset($_POST['tab']);

//----------------------------------------------------------
// save
//----------------------------------------------------------
$_POST['mode'] = 'save';

//----------------------------------------------------------
// analysis_placement
//----------------------------------------------------------
$_POST['analysis_placement'] = $conf->analysis_placement();

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign(Ldr_Util::get_common_info('adm_permission'));
$view->assign('emp', $session->emp());
$view->assign('appr_list', $appr->lists($_POST['pulldown_class']));
$view->assign('appr_pulldown_list', $classmst->class12_lists());
$view->assign('comm_list', $comm->lists());
$view->assign('inside_list', $inside->lists());
$view->assign('outside_list', $outside->lists());
$view->assign('relief_list', $relief->lists());
$view->assign('training_list', $training->lists());
$view->assign('analysis_list', $analysis->lists());
$view->assign('completed', $completed);
$view->assign('active', $active);
$view->display_with_fill('adm_permission.tpl', array('fdat' => $_POST));
