<?php

require_once 'class/Base.php';
require_once 'class/Pager.php';
require_once 'class/Apply.php';
require_once 'class/Ladder.php';

$perPage = Ldr_Const::SRH_PERPAGE;

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_ladder($session->emp());

//----------------------------------------------------------
// Object
//----------------------------------------------------------
$apply = new Ldr_Apply(array('accept_emp_id' => $session->emp()->emp_id()));
$ladder = new Ldr_Ladder();

//----------------------------------------------------------
// 初期値
//----------------------------------------------------------
$_POST['menu'] = "accept";
$_POST['owner_url'] = "accept_objection.php";

//----------------------------------------------------------
// ページ遷移
//----------------------------------------------------------
Ldr_Util::session_init('ldr_common');

// 戻る/コメント記入後のリロード
if (isset($_POST['ldr_return']) || isset($_SESSION['ldr_return'])) {
    $_POST["page_no"] = $_SESSION['page_no'];
    $srh_condition = $_SESSION['srh_condition'];
}

// ページャーから移動
else if (Ldr_Pager::is_pager($_POST['move_pager'])) {
    //検索条件を取得
    $_SESSION['page_no'] = Ldr_Pager::get_page_no($_POST['page_no']);
    $srh_condition = $_SESSION['srh_condition'];
}

// それ以外
else {
    // セッション情報の初期化
    $_SESSION = array();

    $_POST["page_no"] = 1;
    $_SESSION['page_no'] = 1;
    $_SESSION['srh_condition'] = null;
    $srh_condition = null;

    if ($_REQUEST['unread'] === '1') {
        $srh_condition['unread'] = 1;
        $_SESSION['srh_condition'] = $srh_condition;
    }
}

//----------------------------------------------------------
// 一覧
//----------------------------------------------------------
$_SESSION['ldr_return'] = null;
$fdat = array_merge((array)$_REQUEST, (array)$srh_condition);

//----------------------------------------------------------
// データ数の取得
//----------------------------------------------------------
$dataCount = $apply->count_objection($srh_condition);
$current = Ldr_Pager::get_page_no($_POST['page_no'], $dataCount, $perPage);
$start = Ldr_Pager::calc_page_index($current, $perPage);

//----------------------------------------------------------
// 一覧の取得
//----------------------------------------------------------
$list = $apply->lists_objection($start, $perPage, $srh_condition);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $emp);
$view->assign(Ldr_Util::get_common_info('accept_objection'));
$view->assign(Ldr_Pager::get_pager_info($_POST['page_no'], $dataCount, $perPage));
$view->assign('list', $list);
$view->display_with_fill('accept_objection.tpl', array('fdat' => $fdat));
