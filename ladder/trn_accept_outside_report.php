<?php

require_once 'class/Base.php';
require_once 'class/Outside.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('accept_outside');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_outside($emp);

//----------------------------------------------------------
// object
//----------------------------------------------------------
$outside = new Ldr_Outside($emp->emp_id());

//----------------------------------------------------------
// データ
//----------------------------------------------------------
if (!isset($_POST['id'])) {
    cmx_log(basename($_SERVER["PHP_SELF"]) . " parameter error");
    header('Location: dashboard.php');
    exit;
}

$cond = array_merge((array) $_POST, array('list_type' => 'report'));
$data = array(
    "trn" => $outside->find_mst($_POST['id']),
    'list' => $outside->lists_accept_emp($cond),
);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $emp);
$view->assign($data);

$view->assign(Ldr_Util::get_common_info('taccept_outside'));
$view->display('trn_accept_outside_report.tpl');
