<?php

require_once 'class/Base.php';
require_once 'class/InsideMst.php';
require_once 'class/Guideline.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('adm_inside');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_mst_inside($session->emp());

//----------------------------------------------------------
// Obj
//----------------------------------------------------------
$mst = new Ldr_InsideMst();
$gl = new Ldr_Guideline();
$conf = new Ldr_Config();
$effective_post = Ldr_Util::use_tiket();

//----------------------------------------------------------
// hospital_type
//----------------------------------------------------------
$hospital_type = $conf->hospital_type();

//----------------------------------------------------------
// 保存
//----------------------------------------------------------
if (isset($_POST['btn_save'])) {
    // バリデーション
    $error = $mst->validate($_POST, $hospital_type);

    // エラーが無ければ保存
    if (empty($error)) {
        try {
            if ($effective_post) {
                $mst->save($_POST);
            }
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
        }

        $_SESSION['ldr_return'] = true;
        header('Location:' . 'adm_inside.php');
        exit;
    }
}

//----------------------------------------------------------
// 削除
//----------------------------------------------------------
else if (isset($_POST['btn_remove'])) {
    if (!empty($_POST['inside_id'])) {
        try {
            $mst->remove($_POST['inside_id']);
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
        }

        $_SESSION['ldr_return'] = true;
        header('Location:' . 'adm_inside.php');
        exit;
    }
}

//----------------------------------------------------------
// データの取得
//----------------------------------------------------------
if ($_POST['mode'] === 'edit') {
    $data = $mst->find($_POST['id']);
    $data['id'] = $_POST['id'];
}
else {
    $data = array('year' => Ldr_Util::th_year());
}

//----------------------------------------------------------
// group1
//----------------------------------------------------------
$group1 = $gl->lists('group1', array('guideline_revision' => 1));

//----------------------------------------------------------
// year
//----------------------------------------------------------
$years = $mst->year_lists();

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $session->emp());
$view->assign(Ldr_Util::get_common_info('adm_inside'));

$view->assign('years', $years);
$view->assign('group1', $group1);

if ($error) {
    $view->assign('planner', $_POST['emp_planner']);
    $view->assign('coach', $_POST['emp_coach']);
    $view->assign('guideline', $_POST['guideline']);
    $view->assign('error', $error);
    $view->display_with_fill('adm_inside_edit.tpl', array('fdat' => $_POST));
}
else {
    $view->assign('planner', $data['emp_planner']);
    $view->assign('coach', $data['emp_coach']);
    $view->assign('guideline', $data['guideline']);
    $view->display_with_fill('adm_inside_edit.tpl', array('fdat' => $data));
}