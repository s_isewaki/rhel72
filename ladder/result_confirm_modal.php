<?php

require_once 'class/Base.php';
require_once 'class/Apply.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_ladder($emp);

//----------------------------------------------------------
// ID
//----------------------------------------------------------
if (!isset($_GET['id'])) {
    js_error_exit();
}

//----------------------------------------------------------
// オブジェクト
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$apply = new Ldr_Apply(array('apply_id' => $_GET['id']));
$apply_data = $apply->apply();
$objection = $apply->find_objection();

//----------------------------------------------------------
// データ
//----------------------------------------------------------
$target_emp = new Ldr_Employee($apply->apply_emp_id());
$view->assign('emp', $emp);
$view->assign('target_emp', $target_emp);
$view->assign('apply', $apply);
$view->assign('ldr', $ladder);
$view->assign('result', $apply_data['result']);
$view->assign('objection', $objection);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
header('Cache-Control: no-cache, must-revalidate');
header("Cache-Control: post-check=0, pre-check=0", false);
header('Pragma: no-cache');

$view->display("modal/result_confirm.tpl");
