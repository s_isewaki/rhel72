<?php

require_once 'class/Base.php';
require_once 'class/Pager.php';
require_once 'class/Employee.php';
require_once 'class/AnalysisLadder.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
    exit;
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_analysis($session->emp());

//----------------------------------------------------------
// object
//----------------------------------------------------------
$analysis = new Ldr_AnalysisLadder();
$ladder = new Ldr_Ladder();

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('ldr_common');

//----------------------------------------------------------
// year
//----------------------------------------------------------
$year_list = range(date('Y') + 1, 2014);
$year = $_POST['year'] ? $_POST['year'] : ($_SESSION['year'] ? $_SESSION['year'] : Ldr_Util::th_year());

//----------------------------------------------------------
// ladder_id
//----------------------------------------------------------
foreach ($ladder->lists() as $ldr) {
    $ladder_id_1st = $ldr['ladder_id'];
    break;
}
$ladder_id = $_POST['ladder_id'] ? $_POST['ladder_id'] : ($_SESSION['ladder_id'] ? $_SESSION['ladder_id'] : $ladder_id_1st);

//----------------------------------------------------------
// 部署リスト
//----------------------------------------------------------
$full = false;
if ($emp->is_analysis() || $emp->committee() || $emp->is_admin()) {
    $class_lists = $analysis->lists('class');
    $srh_class3 = array_key_exists('srh_class3', $_POST) ? $_POST['srh_class3'] : ($_SESSION['srh_class3'] ? $_SESSION['srh_class3'] : null);
    $full = true;
}
else {
    $class_lists = $analysis->lists_class_approver($emp->emp_id());
    $srh_class3 = $_POST['srh_class3'] ? $_POST['srh_class3'] : ($_SESSION['srh_class3'] ? $_SESSION['srh_class3'] : implode("_", array($emp->dept_id(), $emp->room_id())));
}

//----------------------------------------------------------
// 遷移状態判別
//----------------------------------------------------------
// 戻る
if (isset($_POST['ldr_return'])) {
    $_POST["page_no"] = $_SESSION['page_no'];
}

// ページャーから移動
else if (Ldr_Pager::is_pager($_POST['move_pager'])) {
    //検索条件を取得
    $_SESSION['page_no'] = Ldr_Pager::get_page_no($_POST['page_no']);
}

// 検索ボタン
else if ($_POST['btn_search']) {
    $_SESSION["year"] = $year;
    $_SESSION["ladder_id"] = $ladder_id;
    $_SESSION["srh_class3"] = $srh_class3;
    $_POST["page_no"] = 1;
}

// ソート
else if ($_REQUEST['order']) {
    $_POST["page_no"] = 1;

    $desc = $_REQUEST['desc'] ? 'DESC' : 'ASC';

    switch ($_REQUEST['order']) {
        case 'st_order_no':
            $order = "st_order_no $desc, ldr1.level IS NULL ASC, ldr1.level DESC";
            break;

        default:
            $order = "{$_REQUEST['order']} IS NULL ASC, {$_REQUEST['order']} $desc";
            break;
    }
}

// 外部から移動してきた場合
else {
    // セッション情報の初期化
    $_POST["page_no"] = 1;
    $_SESSION['page_no'] = 1;
}

$_SESSION['ldr_return'] = null;

//----------------------------------------------------------
// 職員リスト
//----------------------------------------------------------
$options = array(
    'order_by' => $order ? "$order, empmst.emp_kn_lt_nm, empmst.emp_kn_ft_nm" : 'st_order_no, ldr1.level IS NULL ASC, ldr1.level DESC, empmst.emp_kn_lt_nm, empmst.emp_kn_ft_nm',
    'year' => $year,
    'ladder_id' => $ladder_id,
    'srh_class3' => $srh_class3,
);
$perPage = 50;
$count = $emp->levelup_count($options);
$current = Ldr_Pager::get_page_no($_POST['page_no'], $count, $perPage);
$start = Ldr_Pager::calc_page_index($current, $perPage);
$pages = array(
    'per_page' => $perPage,
    'start' => $start,
);
$lists = $emp->levelup_lists(array_merge($options, $pages));

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign(Ldr_Util::get_common_info('staff_levelup'));
$view->assign('emp', $session->emp());
$view->assign('ladder', $ladder);
$view->assign('year', (int)$year);
$view->assign('ladder_id', $ladder_id);
$view->assign('ldr', $ladder->find($ladder_id));
$view->assign('srh_class3', $srh_class3);
$view->assign('class_lists', $class_lists);
$view->assign('full', $full);

//----------------------------------------------------------
// 印刷
//----------------------------------------------------------
if ($_POST['mode'] === 'print') {
    $view->assign('list', $emp->levelup_lists($options));
    require_once 'class/PDF.php';
    $html = $view->fetch('print/staff_levelup.tpl');
    $pdf = new Ldr_PDF('P', 'mm', 'A4', true, 'UTF-8', false);
    $pdf->setHeaderTitle("レベルアップ状況");
    $pdf->setPrinter($session->emp());
    $pdf->AddPage();
    $pdf->writeHTML(to_utf8($html), true, 0, false, 0);
    $pdf->Output("staff_levelup.pdf", "I");
    exit;
}

//----------------------------------------------------------
// 表示
//----------------------------------------------------------
$view->assign(Ldr_Pager::get_pager_info($_POST['page_no'], $count, $perPage));
$view->assign('order', $_REQUEST['order']);
$view->assign('desc', $_REQUEST['desc']);
$view->assign('year_list', $year_list);
$view->assign('list', $lists);
$view->display_with_fill('staff_levelup.tpl', array('fdat' => $options));
