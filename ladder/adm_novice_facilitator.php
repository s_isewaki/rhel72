<?php

require_once 'class/Base.php';
require_once 'class/Novice.php';
require_once 'class/AnalysisLadder.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_approver_admin($emp);

//----------------------------------------------------------
// object
//----------------------------------------------------------
$novice = new Ldr_Novice();
$analysis = new Ldr_AnalysisLadder();
$effective_post = Ldr_Util::use_tiket();

//----------------------------------------------------------
// CSV取り込み
//----------------------------------------------------------
if ($_POST['mode'] === 'import') {

    // ファイルのチェック
    $file_err = Ldr_Util::exist_upload_file();
    if (!empty($file_err)) {
        header('Cache-Control: no-cache, must-revalidate');
        header("Cache-Control: post-check=0, pre-check=0", false);
        header('Pragma: no-cache');
        header('X-Content-Type-Options: nosniff');
        print cmx_json_encode($file_err);
        exit;
    }

    // インポート
    try {
        $res = $novice->import_facilitator_csv($_REQUEST['year'], $_REQUEST['importmode'], $_FILES["csvfile"]);
    }
    catch (Exception $e) {
        header('Cache-Control: no-cache, must-revalidate');
        header("Cache-Control: post-check=0, pre-check=0", false);
        header('Pragma: no-cache');
        header('X-Content-Type-Options: nosniff');
        print cmx_json_encode(array(
            'error' => true,
            'msg' => 'CSVのインポートに失敗しました',
            'result' => explode(',', $e->getMessage())
        ));
        exit;
    }

    header('Cache-Control: no-cache, must-revalidate');
    header("Cache-Control: post-check=0, pre-check=0", false);
    header('Pragma: no-cache');
    header('X-Content-Type-Options: nosniff');
    print cmx_json_encode(array(
        'error' => false,
        'msg' => $res,
    ));
    exit;
}

//----------------------------------------------------------
// 保存
//----------------------------------------------------------
if (isset($_POST['btn_save'])) {
    // バリデーション
    $error = $novice->validate_facilitator($_POST);

    // エラーが無ければ保存
    if (empty($error)) {
        try {
            if ($effective_post) {
                $novice->save_facilitator($_POST);
                $completed = true;
            }
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
        }
    }
}

//----------------------------------------------------------
// 年リスト
//----------------------------------------------------------
$year_list = $novice->year_lists();
$year = !empty($_POST['year']) ? $_POST['year'] : Ldr_Util::th_year();

//----------------------------------------------------------
// 部署リスト
//----------------------------------------------------------
$class_lists = $analysis->lists('class');

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign(Ldr_Util::get_common_info('adm_novice_facilitator'));
$view->assign('emp', $emp);
$view->assign('year_list', $year_list);
$view->assign('year', $year);
$view->assign('facilitate_list', $novice->lists($year, $_POST['srh_class3']));
$view->assign('completed', $completed);
$view->assign('class_lists', $class_lists);
$view->assign('error', $error);
$view->display_with_fill('adm_novice_facilitator.tpl', array('fdat' => array('year' => $year, 'srh_class3' => $_POST['srh_class3'])));
