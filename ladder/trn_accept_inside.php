<?php

require_once 'class/Base.php';
require_once 'class/Pager.php';
require_once 'class/Inside.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('accept_inside');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_inside($emp);

//----------------------------------------------------------
// object
//----------------------------------------------------------
$inside = new Ldr_Inside($emp->emp_id());
$perPage = Ldr_Const::SRH_PERPAGE;
$conf = new Ldr_Config();

//----------------------------------------------------------
// ソート
//----------------------------------------------------------
$desc = $_REQUEST['desc'] ? 'DESC' : 'ASC';
switch ($_REQUEST['order']) {
    case 'no':
        $order_by[] = "substring(m.no from '^[^0-9]*') $desc";
        $order_by[] = "CASE WHEN substring(m.no from '[0-9\-]*$') = '' THEN 0 ELSE to_number(replace(substring(m.no from '[0-9\-]*$'), '-', ''), '9999999999') END $desc";
        break;

    case 'accept_date':
        $order_by[] = "d.accept_date $desc";
        break;

    case 'deadline_date':
        $order_by[] = "d.deadline_date $desc";
        break;

    default:
        $order_by[] = "d.type $desc";
        $order_by[] = "d.training_date $desc";
        $order_by[] = "d.training_start_time $desc";
        break;
}

//----------------------------------------------------------
// 遷移状態判別
//----------------------------------------------------------
// ページャーから移動
if (Ldr_Pager::is_pager($_POST['move_pager'])) {
    //検索条件を取得
    $_SESSION['page_no'] = Ldr_Pager::get_page_no($_POST['page_no']);
    $srh_condition = $_SESSION['srh_condition'];
}

// 検索
else if ($_POST['search']) {
    $srh_condition['srh_year'] = $_POST['srh_year'];
    $srh_condition['srh_nonaccept'] = $_POST['srh_nonaccept'];
    $srh_condition['srh_planner'] = $_POST['srh_planner'];
    $srh_condition["srh_start_date"] = $_POST['srh_start_date'];
    $srh_condition["srh_last_date"] = $_POST['srh_last_date'];
    $srh_condition["srh_keyword"] = $_POST['srh_keyword'];
    $srh_condition["order_by"] = $order_by;

    $_POST["page_no"] = 1;

    // 検索条件を設定
    $_SESSION['srh_condition'] = $srh_condition;
}

// 編集画面から戻ってきた場合
else if (isset($_POST['ldr_return']) || isset($_SESSION['ldr_return'])) {
    // ページ番号の復旧
    $_POST["page_no"] = $_SESSION['page_no'];
    // 検索条件の復旧
    $srh_condition = $_SESSION['srh_condition'];
}

// ソート
else if ($_REQUEST['order']) {
    $srh_condition = $_SESSION['srh_condition'];
    $srh_condition["order_by"] = $order_by;
}

// 外部から移動してきた場合
else {
    // セッション情報の初期化
    $_SESSION = array();
    $_POST["page_no"] = 1;
    $_SESSION['page_no'] = 1;
    $_SESSION['srh_condition'] = null;
    $srh_condition = null;

    if (isset($_REQUEST['status'])) {
        $srh_condition['srh_nonaccept'] = '1';
    }

    // 検索日の初期値を設定
    $srh_condition["srh_start_date"] = date("Y-m-d");
    $srh_condition["srh_year"] = Ldr_Util::th_year();

    // ソート
    $srh_condition["order_by"] = $order_by;

    $_SESSION['srh_condition'] = $srh_condition;
}

$_SESSION['ldr_return'] = null;
$_SESSION['menu'] = 'taccept_inside';

$fdat = array_merge((array)$_POST, (array)$srh_condition);

//----------------------------------------------------------
// データ
//----------------------------------------------------------
$dataCount = $inside->count_accept($srh_condition);
$current = Ldr_Pager::get_page_no($_POST['page_no'], $dataCount, $perPage);
$start = Ldr_Pager::calc_page_index($current, $perPage);
$list = $inside->lists_accept($start, $perPage, $srh_condition);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign(Ldr_Util::get_common_info($_SESSION['menu']));
$view->assign(Ldr_Pager::get_pager_info($_POST['page_no'], $dataCount, $perPage));
$view->assign('emp', $emp);
$view->assign('list', $list);
$view->assign('year', $_POST['srh_year']);
$view->assign('year_list', $inside->year_lists());
$view->assign('ojt_name', $conf->ojt_name());
$view->assign('order', $_REQUEST['order']);
$view->assign('desc', $_REQUEST['desc']);
$view->display_with_fill('trn_accept_inside.tpl', array('fdat' => $fdat));
