<?php

require_once 'class/Base.php';
require_once 'class/InsideMst.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('adm_inside_sort_number');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_mst_inside($session->emp());

//----------------------------------------------------------
// Obj
//----------------------------------------------------------
$mst = new Ldr_InsideMst();
$effective_post = Ldr_Util::use_tiket();

//----------------------------------------------------------
// 並べ替え
//----------------------------------------------------------
if ($effective_post && isset($_POST['btn_save'])) {
    try {
        $mst->sort_contents($_POST['id'], $_POST['number']);
        $completed = true;
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }
}
if (isset($_POST['ldr_return']) || isset($_SESSION['ldr_return'])) {
    // ソート画面から戻ってきた場合
    $id = $_SESSION['id'];
}
else {
    $_SESSION['id'] = $_POST['id'];
}
unset($_SESSION['ldr_return']);

//----------------------------------------------------------
// データの取得
//----------------------------------------------------------
$data = $mst->find_contents($id);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $session->emp());
$view->assign(Ldr_Util::get_common_info('adm_inside', array('referer_url' => "adm_inside_edit_number.php")));
$view->assign($data);
$view->assign('completed', $completed);
$view->display('adm_inside_sort_number.tpl');
