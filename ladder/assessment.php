<?php

require_once 'class/Base.php';
require_once 'class/Assessment.php';
require_once 'class/Ladder.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_ladder($session->emp());

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('assessment');

//----------------------------------------------------------
// オブジェクト
//----------------------------------------------------------
$as = new Ldr_Assessment();
$ladder = new Ldr_Ladder();

//----------------------------------------------------------
// 保存ボタン
//----------------------------------------------------------
if ($_POST['btn_save'] === 'btn_save') {
    try {
        $as->save($session->emp()->emp_id(), $_POST);
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }
}

//----------------------------------------------------------
// 自動保存
//----------------------------------------------------------
else if ($_POST['mode'] === 'save') {
    try {
        $as->save($session->emp()->emp_id(), $_POST, 'auto');
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }
    exit;
}

//----------------------------------------------------------
// 版数
//----------------------------------------------------------
if ($_POST['ladder_id']) {
    $ladder_id = $_POST['ladder_id'];
    $this_ladder = $ladder->find($_POST['ladder_id']);
}
else {
    $ladder_lists = $ladder->lists();
    $this_ladder = $ladder_lists[0];
    $ladder_id = $this_ladder['ladder_id'];
}
$_SESSION['ladder_id'] = $ladder_id;

//----------------------------------------------------------
// 一覧
//----------------------------------------------------------
$group1_list = $as->lists('group1', array('guideline_revision' => $this_ladder['guideline_revision']));

//----------------------------------------------------------
// year
//----------------------------------------------------------
$year = $_POST['year'] ? $_POST['year'] : Ldr_Util::th_year();
if (!Validation::is_year($year)) {
    header('Location: dashboard.php');
    exit;
}

//----------------------------------------------------------
// level
//----------------------------------------------------------
if (empty($_POST['level'])) {
    $level = $session->emp()->level() !== '5' ? $session->emp()->level() + 1 : 5;
}
else {
    $level = $_POST['level'];
}

//----------------------------------------------------------
// fdat
//----------------------------------------------------------
$fdat = array(
    'ladder_id' => $ladder_id,
    'level' => $level,
    'year' => $year,
);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign(Ldr_Util::get_common_info('assessment'));
$view->assign('emp', $session->emp());
$view->assign('ladder', $ladder);
$view->assign('this_ladder', $this_ladder);
$view->assign('year', $year);

$view->assign(array(
    'group1_list' => $group1_list,
    'year_list' => $as->year_lists(),
));

$view->assign('active', $_POST['g1'] ? $_POST['g1'] : 'assessment');
$view->assign('g1', $_POST['g1']);
$view->assign('g2', $_POST['g2']);
$view->display_with_fill('assessment.tpl', array('fdat' => $fdat));
