<?php

require_once 'class/Base.php';
require_once 'class/Assessment.php';
require_once 'class/AnalysisPlacement.php';
require_once 'class/AnalysisLadder.php';
require_once 'class/Classmst.php';
require_once 'class/Ladder.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}
$emp = $session->emp();

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('analysis_placement');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_analysis($session->emp());

//----------------------------------------------------------
// object
//----------------------------------------------------------
$placement = new Ldr_AnalysisPlacement($emp->emp_id());
$analysis = new Ldr_AnalysisLadder();
$ladder = new Ldr_Ladder();
$view = new Cmx_View('ladder/templates');
if ($_POST['mode'] !== 'print') {
    $effective_post = Ldr_Util::use_tiket();
}
else {
    $_POST['pattern_id'] = $_POST['id'];
}

//----------------------------------------------------------
// 処理
//----------------------------------------------------------
if ($effective_post) {
    switch ($_POST['mode2']) {
        // バリデーション
        case 'validate':
            $euc = Ldr_Util::from_utf8($_POST);
            $error = $placement->validate($euc);
            header('X-Content-Type-Options: nosniff');
            print cmx_json_encode(array(
                'error' => !empty($error),
                'msg' => $error,
                'mode' => $_POST['mode'],
            ));
            exit;
            break;

        // 保存
        case 'save':
            $error = $placement->validate($_POST);
            // エラーが無ければ保存
            if (empty($error)) {
                // 保存処理
                try {
                    $_POST['basic_date'] = $_SESSION['basic_date'];
                    $_POST['pattern_id'] = $placement->save('new', $_POST);
                }
                catch (Exception $e) {
                    cmx_log($e->getMessage());
                    js_error_exit();
                }
            }
            else {
                js_error_exit();
            }
            break;
    }

    if ($_POST['btn_save'] === "delete") {
        try {
            $placement->delete($_POST['pattern_id']);
            unset($_POST['pattern_id']);
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
        }
    }
    else if ($_POST['btn_save'] === "save") {
        try {
            $_POST['pattern_id'] = $placement->save('update', $_POST);
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
        }
    }
}

//----------------------------------------------------------
// selecting data
//----------------------------------------------------------
$data = array(
    'list' => $placement->lists($_POST['pattern_id']),
    'list_st' => $analysis->lists('st', true),
    'list_class' => $analysis->lists('class'),
    'list_job' => $analysis->lists('job'),
    'list_pattern' => $placement->lists_pattern()
);

//----------------------------------------------------------
// 基準日
//----------------------------------------------------------
if (empty($_POST['pattern_id'])) {
    $_POST['basic_date'] = date("Y-m-d");
}
else {
    foreach ($data['list_pattern'] as $value) {
        if ($value['pattern_id'] === $_POST['pattern_id']) {
            $_POST['basic_date'] = $value['basic_date'];
            $_POST['updated_on'] = $value['updated_on'];
            break;
        }
    }
}

// 新規保存にmodalを経由するのでsessionに退避
$_SESSION['basic_date'] = $_POST['basic_date'];

//----------------------------------------------------------
// 集計
//----------------------------------------------------------
$ladders = $ladder->lists();
$max_status = array();
foreach ($data['list_class'] as $id => $class) {
    $level = array();
    foreach ($data['list_st'] as $st) {
        $st_p['emp_list'] = $data['list'][$class['key']][$st['st_id']];
        foreach ($st_p['emp_list'] as $emp_p) {
            foreach ($ladders as $ldr) {
                $lv = "level{$ldr['ladder_id']}";
                if ($emp_p[$lv] >= 0 && $emp_p[$lv] <= 6) {
                    $level["ldr{$ldr['ladder_id']}"]["lv{$emp_p[$lv]}"] ++;
                }
                else {
                    $level["ldr{$ldr['ladder_id']}"]['lv'] ++;
                }
            }
            $level['sum'] ++;
        }
        // 各役職の最大人数
        if ($max_status[$st['st_id']] < count($data['list'][$class['key']][$st['st_id']])) {
            $max_status[$st['st_id']] = count($data['list'][$class['key']][$st['st_id']]);
        }
    }
    $data['list_class'][$id]['cnt'] = $level;
}

//----------------------------------------------------------
// 印刷
//----------------------------------------------------------
if ($_POST['mode'] === 'print') {
    require_once 'class/PDF.php';

    // 1ページの最大行数
    $max_line = 62;

    // 合計欄の行数
    $sum_line = 3;
    foreach ($ladders as $ldr) {
        $sum_line += count($ldr['config']['level']);
    }

    // 役職ごとの最大職員数
    $max_status_line = 0;
    $max_data = array();
    foreach ($max_status as $count) {
        $max_status_line += $count;
    }

    // 1ページに表示できる限界行数
    $limit_line = $max_line - $sum_line;

    // 行数調整
    // (最大職員数が限界行数より大きい場合)
    if ($max_status_line > $limit_line) {
        // 調整が必要な行数
        $rest_status_line = $max_status_line - $limit_line;

        // 数が大きい役職から1ずつ減らしていく
        // 調整が必要な行数が無くなるまで繰り返す
        // (5より大きいもののみ)
        arsort($max_status);
        while ($rest_status_line >= 1) {
            foreach ($max_status as $st_id => $count) {
                if ($count <= 5) {
                    continue;
                }
                $max_status[$st_id] --;
                $rest_status_line --;
                if ($rest_status_line < 1) {
                    break;
                }
            }
        }
    }

    foreach ($data['list_class'] as $id => $class) {
        foreach ($data['list_st'] as $st) {
            $cell = intval((count($data['list'][$class['key']][$st['st_id']]) - 1 ) / $max_status[$st['st_id']]) + 1;
            if ($data['list_class'][$id]['cell'] < $cell) {
                $data['list_class'][$id]['cell'] = $cell;
            }
        }
    }

    // パターン名
    if (empty($_POST['pattern_id'])) {
        $ptn_name = "";
    }
    else {
        foreach ($data['list_pattern'] as $value) {
            if ($value['pattern_id'] === $_POST['pattern_id']) {
                $ptn_name = "　　パターン名[" . $value['pattern'] . "]";
                break;
            }
        }
    }

    $pdf = new Ldr_PDF('L', 'mm', 'A3', true, 'UTF-8', false);
    $pdf->setHeaderTitle('配置表' . $ptn_name . "　　基準日[" . $_POST['basic_date'] . "]");
    $pdf->setPrinter($emp);
    $pdf->SetFont('kozgopromedium', 'B', 9);
    $view->assign('ladder', $ladder);
    $view->assign('roman_num', Ldr_Const::get_roman_num());
    $view->assign("st_count", count($data['list_st']));
    $view->assign("max_status", $max_status);
    $view->assign("ptn_name", $ptn_name);

    $print_data = array(
        'list_st' => $data['list_st'],
        'list_class' => array(),
        'list' => array(),
    );
    $class_count = 0;
    foreach ($data['list_class'] as $class) {

        // 12列毎に印刷
        if ($class_count + $class['cell'] > 12) {
            $pdf->AddPage();

            $view->assign($print_data);
            $html = $view->fetch('print/placement.tpl');
            $pdf->writeHTML(to_utf8($html), true, 0, false, 0);

            unset($print_data['list']);
            unset($print_data['list_class']);
            $class_count = 0;
        }

        $print_data['list_class'][] = $class;
        $print_data['list'][$class['key']] = $data['list'][$class['key']];

        $class_count += $class['cell'];
    }

    // 余ったデータを印刷
    if ($class_count !== 0) {
        $pdf->AddPage();
        $view->assign($print_data);
        $html = $view->output_with_fill('print/placement.tpl', array('fdat' => $_POST));
        $pdf->writeHTML(to_utf8($html), true, 0, false, 0);
    }

    $pdf->Output("placement.pdf", "I");
    exit;
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view->assign(Ldr_Util::get_common_info('analysis_placement'));
$view->assign('emp', $emp);
$view->assign('ladder', $ladder);
$view->assign($data);
$view->display('analysis_placement.tpl');
