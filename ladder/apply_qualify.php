<?php

require_once 'class/Base.php';
require_once 'class/Apply.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_ladder($emp);

//----------------------------------------------------------
// セッションデータ
//----------------------------------------------------------
Ldr_Util::session_init('apply_qualify');

if ($_POST['mode'] === "new" || $_POST['mode'] === "retry" || $_POST['mode'] === "cancel") {
// 初期情報を退避
    $_SESSION['id'] = $_POST['id'];
    $_SESSION['rtn_apply_view'] = Ldr_Util::get_referer_url();
    $_SESSION['mode'] = $_POST['mode'];
}
else {
    $_POST['id'] = $_SESSION['id'];
}

//----------------------------------------------------------
// IDなければダッシュボード
//----------------------------------------------------------
if (empty($_POST['id'])) {
    header("Location: dashboard.php");
    exit;
}

//----------------------------------------------------------
// Object
//----------------------------------------------------------
$apply = new Ldr_Apply(array('apply_emp_id' => $emp->emp_id(), 'apply_id' => $_POST['id']));
$target_emp = $emp;
$effective_post = Ldr_Util::use_tiket();
$conf = new Ldr_Config();

//----------------------------------------------------------
// 申請
//----------------------------------------------------------
if ($_POST['form_submit'] === 'save') {
    try {
        if ($effective_post) {
            $apply->apply_qualify($_POST);
        }
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }
    header('Location:' . Ldr_Util::get_referer_url());
    exit;
}

//----------------------------------------------------------
// 取り下げ
//----------------------------------------------------------
else if ($_POST['form_submit'] === 'cancel') {
    // バリデーション
    $error = $apply->validate_comment($_POST);

    // エラーが無ければ
    if (empty($error)) {
        try {
            $apply->apply_qualify_cancel($_POST);
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
        }
        header('Location:' . Ldr_Util::get_referer_url());
        exit;
    }
}

//----------------------------------------------------------
// 必須様式のチェック
//----------------------------------------------------------
if ($_SESSION['mode'] !== "cancel") {
    $error_qualify = $apply->validate_qualify();
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $session->emp());
$view->assign('apply', $apply);
$view->assign('ldr', $apply->ladder());
$view->assign('form1', $apply->find_form1($_POST['id']));
$view->assign('id', $_POST['id']);
$view->assign('menu', 'apply');
$view->assign('mode', $_SESSION['mode']);
$view->assign('target_emp_id', $target_emp->emp_id());
$view->assign(Ldr_Util::get_common_info('apply', array('referer_url' => $_SESSION['rtn_apply_view'])));
$view->assign($apply->form_updated_on($_POST['id']));
$view->assign('error', $error);
$view->assign('error_qualify', $error_qualify);
$view->display('apply_qualify.tpl');
