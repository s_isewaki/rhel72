<?php

require_once 'class/Base.php';
require_once 'class/Relief.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('trn_approve_relief');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_ladder($emp);

//----------------------------------------------------------
// object
//----------------------------------------------------------
$relief = new Ldr_Relief($emp->emp_id());

//----------------------------------------------------------
// データの取得
//----------------------------------------------------------
if (!isset($_POST['id'])) {
    cmx_log(basename($_SERVER["PHP_SELF"]) . " parameter error");
    header('Location: dashboard.php');
    exit;
}

$fdat = $relief->find('apply', $_POST['id']);
$target_emp = new Ldr_Employee($fdat['emp_id']);
$data = array(
    'type_text' => Ldr_Const::get_outside_type(),
    'trn' => $fdat,
    'target_emp' => $target_emp,
    'view' => true,
    'hd_title' => '救護員研修 確認',
);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $emp);
$view->assign($data);
$view->assign(Ldr_Util::get_common_info($_SESSION['menu']));
$view->display('trn_approve_relief_edit.tpl');
