<?php

require_once 'class/Base.php';
require_once 'class/Ladder.php';
require_once 'class/Guideline.php';
require_once 'class/Statusmst.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('adm_ladder');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_admin($session->emp());

//----------------------------------------------------------
// Obj
//----------------------------------------------------------
$ladder = new Ldr_Ladder();
$gl = new Ldr_Guideline();

//----------------------------------------------------------
// 並べ替え
//----------------------------------------------------------
if ($_POST['mode'] === 'sort') {
    try {
        $ladder->sort($_POST);
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }
    header('Location:' . 'adm_ladder.php');
    exit;
}

//----------------------------------------------------------
// 削除
//----------------------------------------------------------
else if ($_POST['mode'] === 'remove') {
    try {
        $ladder->remove($_POST['id']);
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }
    header('Location:' . 'adm_ladder.php');
    exit;
}

//----------------------------------------------------------
// 印刷
//----------------------------------------------------------
else if ($_POST['mode'] === 'print') {
    $view = new Cmx_View('ladder/templates');
    $view->assign('data', $ladder->find($_POST['id']));
    $view->assign('revision_list', $gl->lists('revision'));
        $view->assign('status', new Ldr_Statusmst());
    $html = $view->fetch('print/adm_ladder.tpl');

    require_once 'class/PDF.php';
    $pdf = new Ldr_PDF('P', 'mm', 'A4', true, 'UTF-8', false);
    $pdf->setHeaderTitle("ラダー設定");
    $pdf->setPrinter($session->emp());
    $pdf->AddPage();
    $pdf->writeHTML(to_utf8($html), true, 0, false, 0);
    $pdf->Output("ladder_setting_{$_POST['id']}.pdf", "I");
    exit;
}

//----------------------------------------------------------
// データの取得
//----------------------------------------------------------
$list = $ladder->lists();

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $session->emp());
$view->assign('list', $list);
$view->assign(Ldr_Util::get_common_info('adm_ladder'));

if ($error) {
    $view->assign('error', $error);
    $view->display_with_fill('adm_ladder.tpl', array('fdat' => $_POST));
}
else {
    $view->display_with_fill('adm_ladder.tpl', array('fdat' => $data));
}