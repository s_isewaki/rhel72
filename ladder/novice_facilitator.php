<?php

require_once 'class/Base.php';
require_once 'class/Novice.php';
require_once 'class/NoviceAssessment.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_ladder($session->emp());

//----------------------------------------------------------
// オブジェクト
//----------------------------------------------------------
$novice = new Ldr_Novice();
$as = new Ldr_NoviceAssessment($session->emp()->emp_id());

//----------------------------------------------------------
// year
//----------------------------------------------------------
$year_list = $as->facilitate_year_lists();
$year = $_REQUEST['year'] ? $_REQUEST['year'] : $year_list[0];
if (!Validation::is_year($year)) {
    header('Location: dashboard.php');
    exit;
}

//----------------------------------------------------------
// number
//----------------------------------------------------------
$date_list = $novice->date_lists($year);

//----------------------------------------------------------
// 一覧
//----------------------------------------------------------
$lists = $as->facilitate($year);

//----------------------------------------------------------
// fdat
//----------------------------------------------------------
$fdat = array(
    'year' => $year,
);
//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $session->emp());
$view->assign(Ldr_Util::get_common_info('novice_facilitator'));
$view->assign('list', $lists);
$view->assign('year_list', $year_list);
$view->assign('date_list', $date_list);
$view->display_with_fill('novice_facilitator.tpl', array('fdat' => $fdat));
