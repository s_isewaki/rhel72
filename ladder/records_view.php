<?php

require_once 'class/Base.php';
require_once 'class/Form21.php';
require_once 'class/Form22.php';
require_once 'class/Form23.php';
require_once 'class/Form3.php';
require_once 'class/Form4.php';
require_once 'class/Form51.php';
require_once 'class/Form52.php';
require_once 'class/Form53.php';
require_once 'class/Form54.php';
require_once 'class/FormCarrier.php';
require_once 'class/FormTransfer.php';
require_once 'class/FormPresentation.php';
require_once 'class/FormArticle.php';
require_once 'class/Employee.php';
require_once 'class/Apply.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('form2_view');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_ladder($session->emp());

//----------------------------------------------------------
// Object
//----------------------------------------------------------
$target_emp = new Ldr_Employee($_POST["target_emp_id"]);
$form21 = new Ldr_Form21($target_emp->emp_id());
$form22 = new Ldr_Form22($target_emp->emp_id());
$form23 = new Ldr_Form23($target_emp->emp_id());
$form3 = new Ldr_Form3($target_emp->emp_id());
$form4 = new Ldr_Form4($target_emp->emp_id());
$form51 = new Ldr_Form51($target_emp->emp_id());
$form52 = new Ldr_Form52($target_emp->emp_id());
$form53 = new Ldr_Form53($target_emp->emp_id());
$form54 = new Ldr_Form54($target_emp->emp_id());
$formcarrier = new Ldr_FormCarrier($target_emp->emp_id());
$formtransfer = new Ldr_FormTransfer($target_emp->emp_id());
$formpresent = new Ldr_FormPresentation($target_emp->emp_id());
$formarticle = new Ldr_FormArticle($target_emp->emp_id());
$conf = new Ldr_Config();
$emp = $session->emp();

//----------------------------------------------------------
// 初期情報設定
//----------------------------------------------------------
if ($_POST['mode'] === 'view') {
    $_POST['srh_start_date'] = null;
    $_POST['srh_last_date'] = null;
    $_POST['mode'] = null;
}

//----------------------------------------------------------
// 様式情報取得
//----------------------------------------------------------
if ($_POST['mode'] === 'print') {
    $cond = $_SESSION['cond'];
}
else {
    $cond['srh_start_date'] = $_POST['srh_start_date'];
    $cond['srh_last_date'] = $_POST['srh_last_date'];
    $_SESSION['cond'] = $cond;
}

//----------------------------------------------------------
// sort order
//----------------------------------------------------------
# start_date
$cond["order_by"][0] = "start_date asc";
$lists21 = $form21->lists(null, null, $cond);
$listscarrier = $formcarrier->lists(null, null, $cond);
$lists51 = $form51->lists(null, null, $cond);
$lists52 = $form52->lists(null, null, $cond);
$lists53 = $form53->lists(null, null, $cond);
$lists54 = $form54->lists(null, null, $cond);

# period
$cond["order_by"][0] = "period asc";
$lists23 = $form23->lists(null, null, $cond);
$lists3 = $form3->lists(null, null, $cond);
$lists4 = $form4->lists(null, null, $cond);
$liststransfer = $formtransfer->lists(null, null, $cond);
$listspresent = $formpresent->lists(null, null, $cond);
$listsarticle = $formarticle->lists(null, null, $cond);

$cond['srh_flag'] = 1;
$lists22_1 = $form22->lists(null, null, $cond);

$cond['srh_flag'] = 2;
$lists22_2 = $form22->lists(null, null, $cond);

$cond['srh_flag'] = 3;
$lists22_3 = $form22->lists(null, null, $cond);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $emp);
$view->assign(Ldr_Util::get_common_info($_POST['menu'], array('referer_url' => $_POST['owner_url'])));
$view->assign('target_emp', $target_emp);

# list
$view->assign('list21', $lists21);
$view->assign('list22_1', $lists22_1);
$view->assign('list22_2', $lists22_2);
$view->assign('list22_3', $lists22_3);
$view->assign('list23', $lists23);
$view->assign('listcarrier', $listscarrier);
$view->assign('listtransfer', $liststransfer);
$view->assign('listpresent', $listspresent);
$view->assign('listarticle', $listsarticle);
$view->assign('list3', $lists3);
$view->assign('list4', $lists4);
$view->assign('list51', $lists51);
$view->assign('list52', $lists52);
$view->assign('list53', $lists53);
$view->assign('list54', $lists54);

# type
$view->assign('f3_types', $form3->types);
$view->assign('f52_types', $form52->types);
$view->assign('f53_types', $form53->types);
$view->assign('fp_types', $formpresent->types);
$view->assign('fp_units', $formpresent->units);

//----------------------------------------------------------
// 印刷
//----------------------------------------------------------
if ($_POST['mode'] === 'print') {

    require_once 'class/PDF.php';
    $conf = new Ldr_Config();
    $title = $conf->titles();
    $pdf = new Ldr_PDF('P', 'mm', 'A4', true, 'UTF-8', false);
    $pdf->setHeaderTitle("[{$title['record']}]");
    $pdf->setEmployee($target_emp);
    $pdf->setPrinter($session->emp());
    $pdf->AddPage();
    $html = $view->output_with_fill('print/records_view.tpl', array('fdat' => $_POST));
    $pdf->writeHTML(to_utf8($html), true, 0, false, 0);
    $pdf->Output("records_view.pdf", "I");
    exit;
}

//----------------------------------------------------------
// 表示
//----------------------------------------------------------
$apply = new Ldr_Apply(array('apply_emp_id' => $target_emp->emp_id(), 'apply_id' => $_POST['id']));
$view->assign('apply', $apply);
$view->assign('ldr', $apply->ladder());
$view->assign('active', $_POST['fm2_active']);
$view->display_with_fill('records_view.tpl', array('fdat' => $_POST));
