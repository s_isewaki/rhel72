<div id="<{$modal_id}>" class="modal large hide fade" tabindex="-1" role="dialog" aria-labelledby="<{$modal_id}>Label" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icon-remove-circle"></i></button>
        <h3 id="<{$modal_id}>Label"><{$modal_title}></h3>
    </div>
    <div class="modal-body"></div>
    <div class="modal-footer">
        <button type="button" class="js-modal-save btn btn-small btn-primary" data-target="#form_<{$modal_id}>"><i class="icon-save"></i> 保存する</button>
        <button class="btn btn-small" data-dismiss="modal" aria-hidden="true"><i class="icon-remove-sign"></i> 閉じる</button>
    </div>
</div>

