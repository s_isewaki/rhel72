<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | <{$hd_title}></title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_sub_title=''}>

            <div class="container-fluid">
                <form method="post" id="inside_form">
                    <input type="hidden" id="mode" name="mode" value="" />
                    <input type="hidden" id="id" name="id" value="" />
                    <input type="hidden" id="wk_status" name="wk_status" value="" />
                    <div class="well well-small">
                        <fieldset>
                            <legend>
                                <a
                                    href="trn_inside_contents_modal.php?id=<{$trn.inside_id|escape}>"
                                    data-toggle="modal"
                                    data-backdrop="true"
                                    data-target="#inside_contents">
                                    <{if $trn.no}>(<{$trn.no|escape}>) <{/if}><{$trn.title|escape}>
                                </a>
                                <a
                                    href="trn_inside_number_modal.php?id=<{$trn.inside_id|escape}>&number=<{$trn.number|escape}>&date_id=<{$trn.inside_date_id|escape}>"
                                    data-toggle="modal"
                                    data-backdrop="true"
                                    data-target="#inside_number">
                                    第<{$trn.number|escape}>回 / 全<{$trn.number_of|escape}>回
                                </a>
                            </legend>

                            <div class="control-group">
                                <label class="control-label" for="work"><strong>課題</strong></label>
                                <div class="controls">
                                    <span class="input-maxlarge ldr-uneditable"><{$work|escape|nl2br}></span>
                                </div>
                            </div>

                            <div class="btn-toolbar">
                                <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
        <{include file="__modal_view.tpl" modal_id="inside_contents" modal_title="研修内容"}>
        <{include file="__modal_view.tpl" modal_id="inside_number" modal_title="研修内容"}>
        <{include file="__js.tpl"}>
        <script type="text/javascript">

        </script>
    </body>
</html>
