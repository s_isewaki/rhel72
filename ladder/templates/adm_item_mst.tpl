<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 名称マスタ管理</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='名称マスタ管理' hd_sub_title=''}>

            <div class="container-fluid">
                <form id="form" action="" method="post">

                    <div class="form-inline">
                        <select id="type" name="type" class="input-xlarge">
                            <{foreach from=$item->types() key=form item=fields}>
                                <{foreach from=$fields key=field item=row}>
                                    <option value="<{$row.type|escape}>">
                                        <{$titles[$form].title|default:$row.form|escape}>: <{$row.field|escape}>
                                        <{if !$items[$row.type].use}>(使用しない)<{/if}>
                                    </option>
                                <{/foreach}>
                            <{/foreach}>
                        </select>

                        <button type="button" class="js-export-csv btn btn-small btn-primary"><i class="icon-cloud-download"></i> CSVを取り出す</button>
                        <a href="#upload" data-toggle="modal" class="btn btn-small btn-danger ldr_upload"><i class="icon-cloud-upload"></i> CSVを取り込む</a>
                        <button type="button" class="js-import-record btn btn-small btn-success"><i class="icon-download"></i> <{$titles.record|default:'記録'|escape}>から取り込む</button>
                    </div>

                    <hr />
                    <{include file="__save_message.tpl"}>

                    <{if $item->lists($type)}>
                        <table class="table table-hover table-striped table-condensed ldr-table">
                            <thead>
                                <tr>
                                    <th style="text-align: left !important;">並べ替え</th>
                                    <th style="text-align: left !important;">名称</th>
                                    <th style="text-align: left !important;">名称かな</th>
                                    <th class="pull-right">登録件数</th>
                                    <th>編集</th>
                                    <th>削除</th>
                                </tr>
                            </thead>
                            <tbody class="sortable">
                                <{foreach item=row from=$item->lists($type)}>
                                    <tr>
                                        <td class="dragHandle">&nbsp;<i class="icon-sort"></i><input type="hidden" name="order[]" value="<{$row.inner_id|escape}>" /></td>
                                        <td class="ldr-td-text-left ldr-item-name" data-value="<{$row.name|escape}>"><{$row.name|escape}></td>
                                        <td class="ldr-td-text-left ldr-item-kana" data-value="<{$row.kana|escape}>"><{$row.kana|escape}></td>
                                        <td class="ldr-td-number"><{$row.counts|default:0|escape}></td>
                                        <td class="ldr-td-name" data-id="<{$row.inner_id|escape}>">
                                            <button type="button" class="js-edit-row btn btn-small btn-primary"><i class="icon-pencil"></i></button>
                                        </td>

                                        <td class="ldr-td-name">
                                            <button type="button" data-id="<{$row.inner_id|escape}>" class="js-remove-row btn btn-small btn-danger">削除</button>
                                        </td>
                                    </tr>
                                <{/foreach}>
                            </tbody>
                        </table>

                        <div class="btn-toolbar">
                            <button type="button" class="btn btn-small btn-primary js-table-sort"><i class="icon-ok-sign"></i> 順番を並べ替える</button>
                        </div>
                    <{else}>
                        <div class="alert alert-info">マスタはありません</div>
                    <{/if}>
                </form>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <{include up_title="CSVを取り込む" up_mode="import" file_nav="CSVファイル" up_php="adm_item_mst.php?type=`$type`" file="__fileupload.tpl"}>
        <script type="text/javascript">
            $(function() {
                // プルダウン切り替え
                $('#type').on('change', function() {
                    $('#form').submit();
                });

                /**
                 * CSVエクスポート
                 */
                $('.js-export-csv').click(function() {
                    $('<form action="" method="post"></form>')
                        .append($('<input></input>').attr({type: 'hidden', name: 'type', value: $('#type').val()}))
                        .append($('<input></input>').attr({type: 'hidden', name: 'mode', value: 'export'}))
                        .appendTo(document.body)
                        .submit()
                        .remove();
                });

                /**
                 * 記録取り込み
                 */
                $('.js-import-record').click(function() {
                    if (confirm("全職員の<{$titles.record|escape}>の入力内容をマスタデータに取り込んでもよろしいですか？")) {
                        $('<form action="" method="post"></form>')
                            .append($('<input></input>').attr({type: 'hidden', name: 'type', value: $('#type').val()}))
                            .append($('<input></input>').attr({type: 'hidden', name: 'mode', value: 'import_record'}))
                            .appendTo(document.body)
                            .submit()
                            .remove();
                        return true;
                    }
                    return false;
                });

                //順番変更
                $('.js-table-sort').click(function() {
                    if (!confirm('順番を変更してもよろしいですか？')) {
                        return false;
                    }

                    $('#form')
                        .append($('<input></input>').attr({type: 'hidden', name: 'type', value: $('#type').val()}))
                        .append($('<input></input>').attr({type: 'hidden', name: 'mode', value: 'sort'}))
                        .submit();
                    return true;
                });

                var editrow = null;

                /**
                 * 編集
                 */
                $('body').on('click', '.js-edit-row', function() {
                    if (editrow) {
                        cancelRow(editrow);
                    }

                    editrow = $(this).parent();

                    $name = editrow.parent().find('td.ldr-item-name');
                    $kana = editrow.parent().find('td.ldr-item-kana');
                    $name.html('<input type="text" id="item_name" style="margin:0;" class="input-large" value="' + $name.text() + '" />');
                    $kana.html('<input type="text" id="item_kana" style="margin:0;" class="input-large" value="' + $kana.text() + '" /> ');
                    editrow.html('<button type="button" class="btn btn-small btn-danger js-save-row">更新</button> <button type="button" class="btn btn-small btn-primary js-cancel-row">戻す</button>');

                    $('#item_name').focus();
                });

                /**
                 * 削除
                 */
                $('body').on('click', '.js-remove-row', function() {
                    if (!confirm("削除してもよろしいですか？")) {
                        return false;
                    }

                    $('<form action="" method="post"></form>')
                        .append($('<input></input>').attr({type: 'hidden', name: 'id', value: $(this).data('id')}))
                        .append($('<input></input>').attr({type: 'hidden', name: 'type', value: $('#type').val()}))
                        .append($('<input></input>').attr({type: 'hidden', name: 'mode', value: 'remove'}))
                        .appendTo(document.body)
                        .submit()
                        .remove();
                    return true;
                });

                /**
                 * 更新
                 */
                $('body').on('click', '.js-save-row', function() {
                    if (confirm("更新してもよろしいですか？")) {
                        $('<form action="" method="post"></form>')
                            .append($('<input></input>').attr({type: 'hidden', name: 'id', value: $(this).parent().data('id')}))
                            .append($('<input></input>').attr({type: 'hidden', name: 'type', value: $('#type').val()}))
                            .append($('<input></input>').attr({type: 'hidden', name: 'name', value: $('#item_name').val()}))
                            .append($('<input></input>').attr({type: 'hidden', name: 'kana', value: $('#item_kana').val()}))
                            .append($('<input></input>').attr({type: 'hidden', name: 'mode', value: 'update'}))
                            .appendTo(document.body)
                            .submit()
                            .remove();
                        return true;
                    }
                    return false;
                });

                /**
                 * 戻す
                 */
                $('body').on('click', '.js-cancel-row', function() {
                    cancelRow($(this).parent());
                });

                /**
                 * アップロード閉じるイベント(画面リロード)
                 */
                $('#upload').on('hidden', function() {
                    $('<form action="" method="post"></form>')
                        .append($('<input></input>').attr({type: 'hidden', name: 'type', value: $('#type').val()}))
                        .appendTo(document.body)
                        .submit()
                        .remove();
                });
            });

            /**
             * 行を戻す
             * @param {type} row
             */
            function cancelRow(row) {
                $name = row.parent().find('td.ldr-item-name');
                $kana = row.parent().find('td.ldr-item-kana');
                $name.html($name.data('value'));
                $kana.html($kana.data('value'));
                row.html('<button type="button" class="js-edit-row btn btn-small btn-primary"><i class="icon-pencil"></i></button>');
                editrow = null;
            }
        </script>
    </body>
</html>