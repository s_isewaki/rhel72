<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | <{$ldr.config.status[21]|default:'レポート評価者設定'|escape}></title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title=$ldr.config.status[21] hd_sub_title=''}>

            <div class="container-fluid">
                <div class="row-fluid">
                    <form id="apply_form" method="post">
                        <input type="hidden" name="mode" value="" />
                        <input type="hidden" name="menu" value="accept" />
                        <input type="hidden" name="id" value="<{$id}>" />
                        <input type="hidden" name="owner_url" value="accept_reporter.php" />
                        <input type="hidden" name="target_emp_id" value="<{$target_emp->emp_id()}>" />

                        <div class="btn-toolbar">
                            <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                        </div>

                        <div class="block">
                            <div class="block-body collapse in">
                                <{include file="__form_btn.tpl"}>
                            </div>
                        </div>

                        <div class="well well-small" id="form_commission">
                            <{if !$comm_list}>
                                <div class="alert alert-error"><{$titles.committee|default:'認定委員会'|escape}>が設定されていません。管理者にお問い合わせください。</div>
                            <{else}>

                                <div class="form-group">
                                    <label>レポート評価者を選択してください。</label>
                                    <div>
                                        <button type="button" class="btn btn-small btn-success js-btn-emplist" data-id="reporters"><i class="icon-book"></i> レポート評価者 を選択してください</button>
                                    </div>
                                    <div class="controls clearfix">
                                        <div id="reporters" class="ldr-empbox <{if !$reporter}>hide<{/if}>">
                                            <{foreach from=$reporter key=id item=name}>
                                                <div class="label label-info"><{$name|escape}><input type="hidden" name="reporter[]" value="<{$id|escape}>" /></div>
                                            <{/foreach}>
                                        </div>
                                    </div>
                                    <{foreach from=$error.reporter item=msg}>
                                        <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                    <{/foreach}>
                                </div>
                                <div class="btn-toolbar">
                                    <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>

                                    <{if !$error.configured}>
                                        <button type="submit" name="form_submit" class="btn btn-small btn-primary js-submit-setting" value="save"><i class="icon-edit"></i> 設定する</button>
                                    <{/if}>
                                </div>
                            <{/if}>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <script type="text/javascript">
            function closeEmployeeList() {
                if (childwin !== null && !childwin.closed) {
                    childwin.close();
                }
                childwin = null;
            }

            // 職員の追加
            function add_target_list(item_id, emp_id, emp_name) {
                var emp = emp_id.split(", ");
                if (emp.length !== 1) {
                    return;
                }
                if ($('#e' + emp_id).size() === 0) {
                    if (String(emp_id) === '<{$target_emp->emp_id()}>') {
                        alert("申請者を評価者には設定できません");
                        return false;
                    }

                    $('#reporters').html('<div id="e' + emp_id + '" class="label label-info">' + emp_name + '<input type="hidden" name="reporter[]" value="' + emp_id + '" /></div>').show();
                }
            }
        </script>
    </body>
</html>