<table class="table table-bordered table-hover table-condensed ldr-table">
    <thead>
        <tr>
            <th>更新日時</th>
            <th>申請日</th>
            <th>ラダー</th>
            <th>Lv</th>
                <{if $hospital_type === '1'}>
                <th>評価会日時</th>
                <th>会場</th>
                <{/if}>
            <th>ステータス</th>
            <th>コメント</th>
            <th>師長申込</th>
            <th>様式</th>
        </tr>
    </thead>
    <tbody>
        <{foreach item=row from=$list}>
            <tr>
                <td class="ldr-td-date">
                    <span title="<{$row.updated_on|date_format_jp:"%Y年%-m月%-d日(%a)"}>">
                        <{$row.updated_on|date_format_jp:"%-m月%-d日(%a)"}>
                    </span><br/>
                    <{$row.updated_on|date_format_jp:"%R"}>
                </td>

                <td class="ldr-td-date">
                    <span class="ldr-td-date-bold" title="<{$row.apply_date|date_format_jp:"%Y年%-m月%-d日(%a)"}>">
                        <{$row.apply_date|date_format_jp:"%-m月%-d日(%a)"}>
                    </span>
                </td>

                <td class="ldr-td-date">
                    <{$row.ladder.title|escape}>
                </td>

                <td class="ldr-td-level">
                    <{$roman_num[$row.level]|default:$row.ladder.config.level0|escape}>
                </td>

                <{if $hospital_type === '1'}>
                    <td class="ldr-td-date">
                        <{if $row.meeting_date}>
                            <span class="ldr-td-date-bold" title="<{$row.meeting_date|date_format_jp:"%Y年%-m月%-d日(%a)"}>"><{$row.meeting_date|date_format_jp:"%-m月%-d日(%a)"}></span><br/>
                            <{$row.meeting_start_time|date_format_jp:"%R"}>〜<{$row.meeting_last_time|date_format_jp:"%R"}>
                        <{else}>
                            (未設定)
                        <{/if}>
                    </td>

                    <td class="ldr-td-name"><{$row.meeting_room|default:'(未設定)'|escape}></td>
                <{/if}>
                <td class="ldr-td-status">
                    <{if $row.ladder.config.anonymous === '1' && $emp->emp_id() === $row.emp_id}>
                        <{include file="__table_status_text.tpl"}>
                    <{else}>
                        <a
                            href="apply_status_modal.php?id=<{$row.ldr_apply_id|escape}>"
                            data-toggle="modal"
                            data-backdrop="true"
                            data-target="#modal_status">
                            <{include file="__table_status_text.tpl"}>
                        </a>
                    <{/if}>
                    <{if $row.status === '0'}>
                        <br/>
                        <a href="result_confirm_modal.php?id=<{$row.ldr_apply_id|escape}>" data-toggle="modal" data-target="#result_confirm">(結果確認)</a>
                    <{/if}>
                </td>

                <td class="ldr-td-button">
                    <a
                        href="comment_modal.php?id=<{$row.ldr_apply_id|escape}>"
                        data-toggle="modal"
                        data-backdrop="true"
                        data-target="#modal_comment"
                        class="js-comment-modal btn btn-small <{if $row.unread}>btn-warning<{else}>btn-success<{/if}>"
                        <{if $row.unread}>
                            rel="tooltip" title="未読が<{$row.unread|escape}>件あります"
                        <{/if}>
                        ><{$row.comment|escape}></a>
                </td>

                <{*師長申込*}>
                <{if $row.status === '4'}>
                    <td class="ldr-td-button"><button type="button" data-id="<{$row.ldr_apply_id|escape}>" data-mode="new" class="js-apply-qualify btn btn-primary btn-small">申請</button></td>
                <{elseif ($row.status === '5' && !$row.report_status) || $row.status === '7'}>
                    <td class="ldr-td-button"><button type="button" data-id="<{$row.ldr_apply_id|escape}>" data-mode="cancel" class="js-apply-qualify btn btn-danger btn-small">取り下げ</button></td>
                <{elseif $row.status === '6'}>
                    <td class="ldr-td-button"><button type="button" data-id="<{$row.ldr_apply_id|escape}>" data-mode="retry" class="js-apply-qualify btn btn-primary btn-small">再申請</button></td>
                <{else}>
                    <td class="ldr-td-date"><span class="ldr-td-date-bold" title="<{$row.qualify_date|date_format_jp:"%Y年%-m月%-d日(%a)"}>"><{$row.qualify_date|date_format_jp:"%-m月%-d日(%a)"}></span><br/><{$row.qualify_date|date_format_jp:"%R"}></td>
                    <{/if}>

                <{*様式*}>
                <td class="ldr-td-form">
                    <{if 1 <= $row.status && $row.status <= 4 || $row.status === '6'}>
                        <button type="button" data-id="<{$row.ldr_apply_id|escape}>" data-emp="<{$row.emp_id|escape}>" data-php="apply_entry.php" data-mode="edit" class="js-form-button btn btn-danger btn-small"><{$row.ladder.config.form_1.btnS|escape}></button>
                    <{elseif $row.status === '0' || $row.status >= 5 || $row.status === '-1' || $row.status === '-2'}>
                        <button type="button" data-id="<{$row.ldr_apply_id|escape}>" data-emp="<{$row.emp_id|escape}>" data-php="form_1view.php" data-mode="view" class="js-form-button btn btn-info btn-small"><{$row.ladder.config.form_1.btnS|escape}></button>
                    <{else}>
                        <button type="button" class="btn btn-small" disabled="disabled"><{$row.ladder.config.form_1.btnS}></button>
                    <{/if}>

                    <{if $row.ladder.config.flow === '1'}>
                        <{if $function.form_2}>
                            <button type="button" data-id="<{$row.ldr_apply_id|escape}>" data-emp="<{$row.emp_id|escape}>" data-php="form_2view.php" data-mode="view" class="js-form-button btn btn-info btn-small"><{$titles.form_2.btnS|escape}></button>
                        <{/if}>

                        <{if $function.form_3}>
                            <button type="button" data-id="<{$row.ldr_apply_id|escape}>" data-emp="<{$row.emp_id|escape}>" data-php="form_3view.php" data-mode="view" class="js-form-button btn btn-info btn-small"><{$titles.form_3.btnS|escape}></button>
                        <{/if}>

                        <{if $function.form_4}>
                            <button type="button" data-id="<{$row.ldr_apply_id|escape}>" data-emp="<{$row.emp_id|escape}>" data-php="form_4view.php" data-mode="view" class="js-form-button btn btn-info btn-small"><{$titles.form_4.btnS|escape}></button>
                        <{/if}>

                        <{if $function.form_5}>
                            <button type="button" data-id="<{$row.ldr_apply_id|escape}>" data-emp="<{$row.emp_id|escape}>" data-php="form_5view.php" data-mode="view" class="js-form-button btn btn-info btn-small"><{$titles.form_5.btnS|escape}></button>
                        <{/if}>
                    <{else}>
                        <{if $function.record_view}>
                            <button type="button" data-id="<{$row.ldr_apply_id|escape}>" data-emp="<{$row.emp_id|escape}>" data-php="records_view.php" data-mode="view" class="js-form-button btn btn-info btn-small"><{$titles.form_2.btnS|escape}></button>
                        <{/if}>
                    <{/if}>
                    <{if $row.status === '4' || $row.status === '6'}>
                        <button type="button" data-id="<{$row.ldr_apply_id|escape}>" data-emp="<{$row.emp_id|escape}>" data-php="form_6edit.php" data-mode="edit" class="js-form-button btn btn-danger btn-small"><{$row.ladder.config.form_6.btnS|escape}></button>
                        <button type="button" data-id="<{$row.ldr_apply_id|escape}>" data-emp="<{$row.emp_id|escape}>" data-php="form_7edit.php" data-mode="edit" class="js-form-button btn btn-danger btn-small"><{$row.ladder.config.form_7.btnS|escape}></button>
                    <{elseif $row.status <= "-2" || $row.status === '0' || $row.status >= 5}>
                        <button type="button" data-id="<{$row.ldr_apply_id|escape}>" data-emp="<{$row.emp_id|escape}>" data-php="form_6view.php" data-mode="view" class="js-form-button btn btn-info btn-small"><{$row.ladder.config.form_6.btnS|escape}></button>
                        <button type="button" data-id="<{$row.ldr_apply_id|escape}>" data-emp="<{$row.emp_id|escape}>" data-php="form_7view.php" data-mode="view" class="js-form-button btn btn-info btn-small"><{$row.ladder.config.form_7.btnS|escape}></button>
                    <{else}>
                        <button type="button" class="btn btn-small" disabled="disabled"><{$row.ladder.config.form_6.btnS|escape}></button>
                        <button type="button" class="btn btn-small" disabled="disabled"><{$row.ladder.config.form_7.btnS|escape}></button>
                    <{/if}>

                    <{if $row.ladder.config.flow === '1'}>
                        <{if $row.status === '0'}>
                            <button type="button" data-id="<{$row.ldr_apply_id|escape}>" data-emp="<{$row.emp_id|escape}>" data-php="form_8view.php" data-mode="view" class="js-form-button btn btn-info btn-small"><{$row.ladder.config.form_8.btnS|escape}></button>
                        <{else}>
                            <button class="btn btn-small" disabled="disabled"><{$row.ladder.config.form_8.btnS|escape}></button>
                        <{/if}>
                    <{/if}>

                    <{if $row.ladder.config.flow === '1'}>
                        <{if $row.status === '0'}>
                            <button type="button" data-id="<{$row.ldr_apply_id|escape}>" data-emp="<{$row.emp_id|escape}>" data-php="form_9view.php" data-mode="view" class="js-form-button btn btn-info btn-small"><{$row.ladder.config.form_9.btnS|escape}></button>
                        <{else}>
                            <button class="btn btn-small" disabled="disabled"><{$row.ladder.config.form_9.btnS|escape}></button>
                        <{/if}>
                    <{/if}>

                    <{if $row.ladder.config.flow === '1'}>
                        <{if $function.form_carrier}>
                            <{*他院での研修*}>
                            <button type="button" data-id="<{$row.ldr_apply_id}>" data-emp="<{$row.emp_id|escape}>" data-php="form_carrier_view.php" data-mode="view" class="js-form-button btn btn-info btn-small"><{$titles.form_carrier.btnS|escape}></button>
                        <{/if}>
                        <{if $function.form_transfer}>
                            <{*当院での異動*}>
                            <button type="button" data-id="<{$row.ldr_apply_id}>" data-emp="<{$row.emp_id|escape}>" data-php="form_transfer_view.php" data-mode="view" class="js-form-button btn btn-info btn-small"><{$titles.form_transfer.btnS|escape}></button>
                        <{/if}>
                        <{if $function.form_presentation}>
                            <{*学会発表記録*}>
                            <button type="button" data-id="<{$row.ldr_apply_id}>" data-emp="<{$row.emp_id|escape}>" data-php="form_presentation_view.php" data-mode="view" class="js-form-button btn btn-info btn-small"><{$titles.form_presentation.btnS|escape}></button>
                        <{/if}>
                        <{if $function.form_article}>
                            <{*学会掲載記録*}>
                            <button type="button" data-id="<{$row.ldr_apply_id}>" data-emp="<{$row.emp_id|escape}>" data-php="form_article_view.php" data-mode="view" class="js-form-button btn btn-info btn-small"><{$titles.form_article.btnS|escape}></button>
                        <{/if}>

                    <{/if}>
                </td>
            </tr>
        <{/foreach}>
    </tbody>
</table>
