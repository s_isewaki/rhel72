<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 院内研修マスタ | 研修日程</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='院内研修マスタ 研修日程' hd_sub_title=''}>

            <div class="container-fluid">
                <{include file="__save_message.tpl" msg_text="ステータスを変更しました"}>
                <div class="row-fluid">
                    <fieldset>
                        <legend>
                            <a
                                href="trn_inside_contents_modal.php?id=<{$inside_id|escape}>"
                                data-toggle="modal"
                                data-backdrop="true"
                                data-target="#inside_contents">
                                <{if $no}>(<{$no|escape}>) <{/if}><{$title|escape}>
                            </a>
                        </legend>
                        <form method="post" id="inside_form">
                            <input type="hidden" name="id" value="<{$smarty.post.id}>"/>
                            <div class="control-group">
                                <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                                <button type="button" data-php="adm_inside_edit_date.php" data-id="<{$inside_id|escape}>" data-date-id="" class="js-edit-button btn btn-small btn-primary"><i class="icon-plus"></i> 日程を追加する</button>
                            </div>

                            <{if $dates}>
                                <div class="form-inline">
                                    <select id="status" name="status" class="input-small">
                                        <option value="1">受付中</option>
                                        <option value="0">終了</option>
                                        <option value="2">準備中</option>
                                    </select>
                                    <button type="submit" name="btn_save" id="btn_save" value="status" class="btn btn-small btn-primary"><i class="icon-save"></i> ステータスを変更する</button>
                                </div>
                                <{include file="__pager.tpl"}>
                                <table class="table table-bordered table-hover table-condensed ldr-table">
                                    <thead>
                                        <tr>
                                            <th><button type="button" id="js-btn-checkbox" class="btn btn-mini"><i class="icon-fixed-width icon-check-sign"></i></button></th>
                                            <th nowrap>研修回</th>
                                            <th>研修内容</th>
                                            <th nowrap>開催場所</th>
                                            <th>最大</th>
                                            <th nowrap>超過</th>
                                            <th>開催日時</th>
                                            <th>受付期間</th>
                                            <th>ステー<br/>タス</th>
                                            <th>編集</th>
                                            <th>更新</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <{foreach item=row from=$dates}>
                                            <tr>
                                                <td class="ldr-td-name"><input type="checkbox" class="ck-id" name="training[]" value="<{$row.inside_date_id}>"/></td>
                                                <td class="ldr-td-name">第<{$row.number|escape}>回</td>
                                                <td class="ldr-td-text-left"><{$row.contents|escape}></td>
                                                <{if $row.type === '1'}>
                                                    <td class="ldr-td-name"><{$row.place|escape}></td>
                                                    <td class="ldr-td-number"><{$row.maximum|escape}>名</td>
                                                    <td class="ldr-td-name"><{if $row.overflow === 't'}>OK<{else}>NG<{/if}></td>
                                                    <td class="ldr-td-date"><{$row.training_date|date_format_jp:"%Y年%-m月%-d日(%a)"}><br /><{$row.training_start_time|date_format:"%R"}>〜<{$row.training_last_time|date_format:"%R"}></td>
                                                <{else}>
                                                    <td class="ldr-td-name" colspan="4"><span class="label label-success"><{$ojt_name|escape}></span></td>
                                                    <{/if}>
                                                <td class="ldr-td-number"><{$row.accept_date|date_format_jp:"%-m月%-d日(%a) %R"}><br>〜<{$row.deadline_date|date_format_jp:"%-m月%-d日(%a) %R"}></td>
                                                <td class="ldr-td-name">
                                                    <{if $row.reception==='0'}>
                                                        <{assign var='lbl' value='label-info'}>
                                                    <{elseif $row.reception < '0'}>
                                                        <{assign var='lbl' value='label-warning'}>
                                                    <{else}>
                                                        <{assign var='lbl' value='label-important'}>
                                                    <{/if}>
                                                    <span class="label <{$lbl}>"><{$row.reception_text|escape}></span>
                                                </td>
                                                <td class="ldr-td-button">
                                                    <button type="button" data-php="adm_inside_edit_date.php" data-mode="edit" data-id="<{$row.inside_id|escape}>" data-date-id="<{$row.inside_date_id|escape}>" class="js-edit-button btn btn-small btn-danger">編集</button>
                                                </td>
                                                <td class="ldr-td-date"><{$row.update_emp_name|escape}><br/><{$row.updated_on|date_format_jp:"%-m月%-d日(%a) %R"}></td>
                                            </tr>
                                        <{/foreach}>
                                    </tbody>
                                </table>
                            <{else}>
                                <div class="alert alert-error">日程が登録されていません</div>
                            <{/if}>
                        </form>
                    </fieldset>
                </div>
            </div>
        </div>
        <{include file="__modal_view.tpl" modal_id="inside_contents" modal_title="研修内容"}>
        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {
                check_status();
                $('.js-edit-button').click(function() {
                    $(this).parents('form')
                        .attr({method: 'post', action: $(this).data('php')})
                        .append($('<input></input>').attr({type: 'hidden', name: 'id', value: $(this).data('id')}))
                        .append($('<input></input>').attr({type: 'hidden', name: 'inside_date_id', value: $(this).data('date-id')}))
                        .submit();
                });
                $('#js-btn-checkbox').click(function() {
                    if ($(this).children('i').hasClass('icon-check-sign')) {
                        $('input.ck-id').prop('checked', true);
                    }
                    else {
                        $('input.ck-id').prop('checked', false);
                    }
                    $(this).children('i').toggleClass('icon-check-sign').toggleClass('icon-check-empty');
                    check_status();
                });
                $('.ck-id').change(function() {
                    check_status();
                });
            });
            function check_status() {
                if ($(".ck-id:checked").length > 0) {
                    $("#btn_save").prop("disabled", false);
                } else {
                    $("#btn_save").prop("disabled", true);
                }
            }
        </script>
    </body>
</html>
