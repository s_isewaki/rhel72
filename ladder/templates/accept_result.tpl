<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 合否判定</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='合否判定' hd_sub_title=''}>

            <div class="container-fluid">
                <form id="apply_form" method="post">
                    <input type="hidden" name="mode" value="" />
                    <input type="hidden" name="menu" value="accept" />
                    <input type="hidden" name="id" value="<{$id}>" />
                    <input type="hidden" name="owner_url" value="accept_result.php" />
                    <input type="hidden" name="target_emp_id" value="<{$target_emp->emp_id()}>" />

                    <div class="btn-toolbar">
                        <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                    </div>
                    <{if !$is_nodata}>

                        <div class="block">
                            <div class="block-body collapse in">
                                <{include file="__form_btn.tpl"}>
                            </div>
                        </div>

                        <div class="well well-small">
                            <div class="control-group">
                                <label class="control-label"><strong>申請者</strong></label>
                                <div class="controls"><span class="ldr-uneditable input-medium"><{$target_emp->emp_name()|escape}></span></div>
                            </div>
                            <div class="control-group">
                                <label class="control-label"><strong>申請者部署</strong></label>
                                <div class="controls"><span class="ldr-uneditable input-xxlarge"><{$target_emp->class_full_name()|escape}></span></div>
                            </div>
                            <div class="control-group">
                                <label class="control-label"><strong>申請日</strong></label>
                                <div class="controls"><span class="ldr-uneditable input-medium"><{$form1.apply_date|date_format_jp:"%Y年%-m月%-d日(%a)"}></span></div>
                            </div>
                            <div class="control-group">
                                <label class="control-label"><strong>申請レベル</strong></label>
                                <div class="controls">
                                    <span class="ldr-uneditable"><{$ldr.title|escape}></span>
                                    <span class="ldr-uneditable input-mini ldr-level"><{$apply->level_text()|escape}></span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label"><strong>当面のあなたの目標</strong></label>
                                <div class="controls">
                                    <span class="ldr-uneditable input-maxlarge"><{$form1.objective|escape|nl2br}></span>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">
                                    <strong>総合レベル</strong>
                                    <span class="badge badge-important required">必須</span>
                                </label>
                                <div>
                                    <label class="radio"><input type="radio" name="result" class="result level" value="1" <{if $result === '1'}>checked<{/if}>/>
                                        <strong>あなたの看護実践に対し申請レベルを認めます。</strong>
                                    </label>
                                    <label class="radio"><input type="radio" name="result" class="result level" value="2" <{if $result === '2'}>checked<{/if}>/>
                                        <strong>今回の申請レベルは保留にします。再チャレンジしてください。</strong>
                                    </label>
                                </div>
                                <{foreach from=$error.result item=msg}>
                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                <{/foreach}>
                            </div>

                            <div class="control-group">
                                <label class="control-label"><strong>認定日</strong></label>
                                <{include file="__datepicker.tpl" date_id="result_date" date_name="result_date" date_value=$result_date}>
                                <{foreach from=$error.result_date item=msg}>
                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                <{/foreach}>
                            </div>
                            <div class="control-group">
                                <label class="control-label"><strong>認定番号登録</strong></label>
                                <div class="controls">
                                    <div class="input-prepend">
                                        <span class="add-on"><i class="icon-trophy"></i></span>
                                        <input type="text" name="approved_number" maxlength="20" class="res-num input-large" value="<{$approved_number|escape}>" <{if $result==='2'}>disabled<{/if}> />
                                    </div>
                                </div>
                                <{foreach from=$error.approved_number item=msg}>
                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                <{/foreach}>
                            </div>

                            <{foreach from=$error.configured item=msg}>
                                <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                            <{/foreach}>
                        </div>
                    <{else}>
                        <div class="alert alert-error">申請が見つかりませんでした</div>
                    <{/if}>

                    <div class="btn-toolbar">
                        <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                        <{if !$is_nodata and !$error.configured}>
                            <button type="submit" name="form_submit" id="btn_save" class="btn btn-small btn-primary js-submit-decision" value="btn_completed"><i class="icon-save"></i> 評価を確定する</button>
                        <{/if}>
                    </div>
            </div>
            </form>
        </div>
        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {
                $(".result").change(function() {
                    if ($(this).val() === "1") {
                        $('.res-num').prop("disabled", false);
                        $('#result_date').prop("disabled", false);
                    } else {
                        $('.res-num').val("").prop("disabled", true);
                        $('#result_date').val('').prop("disabled", true);
                    }
                });
            });
        </script>
    </body>
</html>