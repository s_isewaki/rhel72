<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | <{$titles.form_carrier.title|escape}></title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title=$titles.form_carrier.title hd_sub_title=$titles.form_carrier.detailL}>

            <div class="container-fluid">
                <{*印刷ボタン*}>
                <form id="form_print" method="post" class="pull-right">
                    <button type="button" data-id="<{$smarty.post.id}>" data-emp="<{$smarty.post.target_emp_id}>" data-php="form_carrier_view.php" data-target="formcarrier" class="pull-right js-form-print btn btn-primary btn-small"><i class="icon-print"></i> 印刷する</button>
                </form>

                <{*様式共通ボタン*}>
                <form id="form_edit" method="post">
                    <{include file='__form_view.tpl' op_srh='true' tmp_id='form_carrier_view'}>
                </form>

                <div class="well well-small">

                    <div class="control-group">
                        <div class="input-prepend" style="margin-bottom:0;">
                            <span class="add-on">申請レベル</span>
                            <span class="ldr-uneditable input-mini ldr-level" style="margin-bottom:0;"><{$apply->level_text()|escape}></span>
                        </div>
                        <div class="input-prepend" style="margin-bottom:0;">
                            <span class="add-on">申請者</span>
                            <span class="ldr-uneditable input-large" style="margin-bottom:0;"><{$target_emp->emp_name()|escape}> (<{$target_emp->class_full_name()|escape}>)</span>
                        </div>
                    </div>
                    <hr/>

                    <table class="table table-striped table-hover table-bordered table-condensed ldr-table">
                        <thead>
                            <tr>
                                <th>年月日</th>
                                <th>施設名</th>
                                <th>職務内容</th>
                            </tr>
                        </thead>
                        <tbody>
                            <{foreach item=row from=$list}>
                                <tr>
                                    <td class="ldr-td-date-left"><{$row.start_date|date_format_jp:"%Y年%-m月%-d日(%a)"}> 〜 <{$row.last_date|date_format_jp:"%Y年%-m月%-d日(%a)"}></td>
                                    <td class="ldr-td-text-left"><{$row.institution |escape}></td>
                                    <td class="ldr-td-text-left"><{$row.detail |escape}></td>
                                </tr>
                            <{/foreach}>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
    </body>
</html>