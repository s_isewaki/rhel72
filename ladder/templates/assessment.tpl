<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | <{$titles.assessment|escape}></title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title=$titles.assessment hd_sub_title=''}>

            <div class="container-fluid">
                <div class="row-fluid">
                    <form id="form_assessment" method="post">
                        <select id="pulldown_ladder" name="ladder_id" class="input-large js-pulldown-ladder">
                            <{foreach from=$ladder->lists() item=ldr}>
                                <option value="<{$ldr.ladder_id|escape}>"><{$ldr.title|escape}></option>
                            <{/foreach}>
                        </select>

                        <select id="pulldown_year" name="year" class="input-medium js-pulldown-assessment">
                            <{foreach item=yy from=$year_list}>
                                <option value="<{$yy|escape}>"><{$yy|escape}>年度</option>
                            <{/foreach}>
                        </select>

                        <select id="pulldown_level" name="level" class="ldr-level input-mini js-pulldown-assessment">
                            <{foreach from=$this_ladder.config.level item=lv}>
                                <option value="<{$lv|escape}>"><{$roman_num[$lv]|default:$this_ladder.config.level0|escape}></option>
                            <{/foreach}>
                        </select>
                    </form>
                    <ul class="nav nav-tabs" id="group1Tab">
                        <li class="<{if $active === 'assessment'}>active<{/if}>"><a href="#assessment" data-toggle="tab" id="chart_tab"><i class="icon-bar-chart"></i> 統計</a></li>
                            <{foreach item=row from=$group1_list}>
                            <li class="<{if $active === "g1-`$row.guideline_group1`"}>active<{/if}>"><a href="#g1-<{$row.guideline_group1|escape}>" data-toggle="tab" data-url="assessment_tab.php?group1=<{$row.guideline_group1|escape}><{if $g1 === "g1-`$row.guideline_group1`"}>&active=<{$g2}><{/if}>"><i class="icon-folder-close"></i> <{$row.name|escape}></a></li>
                            <{/foreach}>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane <{if $active === 'assessment'}>active<{else}>fade<{/if}>" id="assessment">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#chart" data-toggle="tab">チャート</a></li>
                                <li><a href="#table" data-toggle="tab" id="table_tab">集計</a></li>
                            </ul>

                            <div class="tab-content">
                                <div class="tab-pane active" id="chart">
                                    <div class="well well-small">
                                        <{foreach from=$this_ladder.config.level item=lv}>
                                            <img class="chart" style="vertical-align:text-top;" src="assessment_charts.php?year=<{$year|escape}>&level=<{$lv|escape}>&now=0" border="0" class="img-polaroid" />
                                        <{/foreach}>
                                    </div>
                                </div>
                                <div class="tab-pane" id="table">
                                    <div class="well well-small">
                                        <div id="d_table">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <{foreach item=row from=$group1_list}>
                            <div class="tab-pane <{if $active === "g1-`$row.guideline_group1`"}>active<{else}>fade<{/if}>" id="g1-<{$row.guideline_group1|escape}>"></div>
                        <{/foreach}>
                    </div>
                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {
                var gp2;
                $('#group1Tab a').on('shown', function(e) {
                    e.preventDefault();

                    var url = $(this).data("url");
                    if (!url) {
                        if ($('#chart').hasClass("active")) {
                            loadChart();
                        }
                        if ($('#table').hasClass("active")) {
                            loadTable();
                        }
                        return;
                    }
                    url += '&level=' + $('#pulldown_level').val();
                    url += '&year=' + $('#pulldown_year').val();

                    // ajax load from data-url
                    $(this.hash).load(url, function() {
                        $(this).tab('show');

                        if (gp2) {
                            $(gp2).trigger("click");
                            gp2 = "";
                        }

                        // 自動保存
                        $(this).find('form').autosave({
                            callbacks: {
                                trigger: 'change',
                                scope: 'all',
                                save: {
                                    method: 'ajax',
                                    options: {
                                        url: 'assessment.php',
                                        method: 'post'
                                    }
                                }
                            }
                        });
                    });
                });

                $('#group1Tab li.active a').trigger('shown');

                $('body').on('click', 'button.js-button-assessment', function() {
                    $(this).tooltip('destroy');

                    var input = $($(this).data('input'));
                    var alert = $($(this).data('alert'));

            <{if $this_ladder.config.select|@count <= 2}>
                    var value = '';
                <{foreach item=sel from=$this_ladder.config.select|@array_reverse}>
                    if (input.val() + '' !== '<{$sel.value|escape:'javascript'}>') {
                        $(this)
                            .html('<{$sel.button|escape:'javascript'}>')
                            .attr('class', 'js-button-assessment btn btn-large btn-<{$sel.color|escape:'javascript'}>');
                        alert.attr('class', 'alert alert-<{$sel.color|escape:'javascript'}>');
                        value = '<{$sel.value|escape:'javascript'}>';
                    }
                <{/foreach}>
                    input.val(value).change();
            <{/if}>
                });

                $('body').on('click', 'button.js-radio-assessment', function() {
                    $(this).tooltip('destroy');

                    var input = $($(this).data('input'));
                    var alert = $($(this).data('alert'));
                    input.val($(this).attr('value')).change();

                    $(this).siblings().removeClass('btn-primary');
                    $(this).siblings().removeClass('btn-info');
                    $(this).siblings().removeClass('btn-success');
                    $(this).siblings().removeClass('btn-warning');
                    $(this).siblings().removeClass('btn-danger');
                    $(this).addClass('btn-' + $(this).data('color'));
                    alert.attr('class', 'alert alert-' + $(this).data('color'));
                });

                $('body').on('submit', 'form.js-form-assessment', function() {
                    // validate
                    var error = false;
                    var selected = $('.btn-group button.btn.active').val();
                    $assessment = selected;
                    $(this).find('input.input-assessment').each(function(idx, input) {
                        var inp = $(input);
                        if (inp.val() === '') {
                            $(inp.data('pop')).tooltip('show');
                            error = true;

                        }
                    });
                    if (error) {
                        return false;
                    }
                });

                // プルダウン選択
                $('.js-pulldown-ladder').change(function() {
                    $('#form_assessment').submit();
                });

                // プルダウン選択
                $('.js-pulldown-assessment').change(function() {
                    $('#group1Tab li.active a').trigger('shown');
                });

                $('#chart_tab').on('shown', function(e) {
                    loadChart();
                });

                $('#table_tab').on('shown', function(e) {
                    loadTable();
                });

                $(document).on('click', '.js-move', function() {
                    $('#pulldown_year').val($(this).data('year'));
                    gp2 = "a[href=#g2-" + $(this).data('gp2') + "]";
                    $("a[href=#g1-" + $(this).data('gp1') + "]").trigger("click");
                });
            });

            /**
             * 集計
             */
            function loadTable() {
                var url = 'assessment_table.php?year=' + $('#pulldown_year').val();
                $('#d_table').load(url);
            }

            /**
             * チャート更新
             */
            function loadChart() {
                var now = new Date().getTime();
                $('.chart').each(function() {
                    var src = $(this).prop('src').replace(/now=\d+/, 'now=' + now);
                    $(this).prop('src', src).addClass("img-polaroid");
                });
            }
        </script>
    </body>
</html>
