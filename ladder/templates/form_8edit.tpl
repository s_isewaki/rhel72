<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | <{$ldr.config.form_8.title|escape}>入力</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title="`$ldr.config.form_8.title`入力" hd_sub_title=$ldr.config.form_8.detailL}>

            <div class="container-fluid">
                <div class="row-fluid">

                    <form class="form" id="edit_form" method="POST">
                        <{include file='__form_view.tpl' op_srh='false' tmp_id='form_8edit'}>
                    </form>

                    <form action="form_8edit.php" method="POST">
                        <input type="hidden" name="save" value="ajax" />
                        <input type="hidden" name="mode" value="" />
                        <input type="hidden" name="menu" value="" />
                        <input type="hidden" name="id" value="" />
                        <input type="hidden" name="target_emp_id" value=""/>
                        <input type="hidden" name="owner_url" value="" />

                        <div class="well well-small">
                            <div class="control-group">
                                <div class="input-prepend" style="margin-bottom:0;">
                                    <span class="add-on">申請レベル</span>
                                    <span class="ldr-uneditable input-mini ldr-level" style="margin-bottom:0;"><{$apply->level_text()|escape}></span>
                                </div>
                                <div class="input-prepend" style="margin-bottom:0;">
                                    <span class="add-on">申請者</span>
                                    <span class="ldr-uneditable input-medium" style="margin-bottom:0;"><{$target_emp->emp_name()|escape}> (<{$target_emp->class_full_name()|escape}>)</span>
                                </div>
                                <div class="input-prepend" style="margin-bottom:0;">
                                    <span class="add-on">評価者</span>
                                    <{foreach from=$form1.appr_list item=row}>
                                        <span class="ldr-uneditable input-medium" style="margin-bottom:0;">
                                            <{$row.emp_name|escape}>
                                        </span>
                                    <{/foreach}>
                                </div>
                            </div>
                            <hr/>

                            <div class="control-group">
                                <label class="control-label" for="clinical">
                                    <strong>評価日</strong>
                                    <span class="badge badge-important required">必須</span>
                                </label>
                                <{include file="__datepicker.tpl" date_id="appraise_date" date_name="appraise_date" date_value="" date_remove="on"}>
                                <{foreach from=$error.appraise_date item=msg}>
                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                <{/foreach}>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="clinical">
                                    <strong><{if $ldr.config.rc === '1'}>臨床看護実践<{else}>赤十字<{/if}></strong>
                                    <span class="badge badge-important required">必須</span>
                                </label>
                                <{if $ldr.config.rc === '1'}>
                                    <label class="control-label">　<small>『知識』『判断』『行為』『行為の結果』について評価会で確認した内容、フィードバックされた内容を具体的に記述してください</small></label>
                                <{/if}>
                                <div class="controls">
                                    <textarea name="clinical" rows="3" class="input-maxlarge js-autoheight"></textarea>
                                </div>
                                <{foreach from=$error.clinical item=msg}>
                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                <{/foreach}>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="management">
                                    <strong><{if $ldr.config.rc === '1'}>マネジメント<{else}>管理過程<{/if}></strong>
                                    <span class="badge badge-important required">必須</span>
                                </label>
                                <div class="controls">
                                    <textarea name="management" rows="3" class="input-maxlarge js-autoheight"></textarea>
                                </div>
                                <{foreach from=$error.management item=msg}>
                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                <{/foreach}>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="education">
                                    <strong><{if $ldr.config.rc === '1'}>教育・研究<{else}>意思決定<{/if}></strong>
                                    <span class="badge badge-important required">必須</span>
                                </label>
                                <div class="controls">
                                    <textarea name="education" rows="3" class="input-maxlarge js-autoheight"></textarea>
                                </div>
                                <{foreach from=$error.education item=msg}>
                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                <{/foreach}>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="comment5">
                                    <strong><{if $ldr.config.rc === '1'}>赤十字活動<{else}>質保証<{/if}></strong>
                                    <span class="badge badge-important required">必須</span>
                                </label>
                                <div class="controls">
                                    <textarea name="redcross" rows="3" class="input-maxlarge js-autoheight"></textarea>
                                </div>
                                <{foreach from=$error.redcross item=msg}>
                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                <{/foreach}>
                            </div>

                            <{if $ldr.config.rc === '2'}>
                                <div class="control-group">
                                    <label class="control-label" for="comment5">
                                        <strong>人材育成</strong>
                                        <span class="badge badge-important required">必須</span>
                                    </label>
                                    <div class="controls">
                                        <textarea name="comment5" rows="3" class="input-maxlarge js-autoheight"></textarea>
                                    </div>
                                    <{foreach from=$error.comment5 item=msg}>
                                        <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                    <{/foreach}>
                                </div>

                                <div class="control-group">
                                    <label class="control-label" for="comment6">
                                        <strong>対人関係</strong>
                                        <span class="badge badge-important required">必須</span>
                                    </label>
                                    <div class="controls">
                                        <textarea name="comment6" rows="3" class="input-maxlarge js-autoheight"></textarea>
                                    </div>
                                    <{foreach from=$error.comment6 item=msg}>
                                        <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                    <{/foreach}>
                                </div>

                                <div class="control-group">
                                    <label class="control-label" for="comment7">
                                        <strong>セルフマネジメント</strong>
                                        <span class="badge badge-important required">必須</span>
                                    </label>
                                    <div class="controls">
                                        <textarea name="comment7" rows="3" class="input-maxlarge js-autoheight"></textarea>
                                    </div>
                                    <{foreach from=$error.comment7 item=msg}>
                                        <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                    <{/foreach}>
                                </div>
                            <{else}>
                                <input type="hidden" name="comment5" value="" />
                                <input type="hidden" name="comment6" value="" />
                                <input type="hidden" name="comment7" value="" />
                            <{/if}>

                            <div class="control-group">
                                <label class="control-label" for="level">
                                    <strong>総合レベル</strong>
                                    <span class="badge badge-important required">必須</span>
                                </label>
                                <label class="control-label">　<small>いずれか当てはまる項目にチェックを入れましょう。</small></label>
                                <div class="ldr-xxlarge">
                                    <label class="radio"><input type="radio" name="level" class="level" value="1" />
                                        <strong>あなたの<{if $ldr.config.rc === '1'}>看護実践<{else}>看護管理実践<{/if}>に対し申請レベルを認めます。</strong>
                                    </label>
                                    <label class="radio"><input type="radio" name="level" class="level" value="2" />
                                        <strong>今回の申請レベルは保留にします。再チャレンジしてください。</strong>
                                    </label>
                                </div>
                                <{foreach from=$error.level item=msg}>
                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                <{/foreach}>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="objective">
                                    <strong>キャリアを高める目標</strong>
                                    <span class="badge badge-important required">必須</span>
                                </label>
                                <div class="controls">
                                    <textarea name="objective" rows="3" class="input-maxlarge js-autoheight"></textarea>
                                </div>
                                <{foreach from=$error.objective item=msg}>
                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                <{/foreach}>
                            </div>
                            <div class="btn-toolbar">
                                <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                                <{if $smarty.post.mode === 'edit'}>
                                    <button type="submit" name="btn_save" class="btn btn-small btn-primary js-submit-decision" value="completed"><i class="icon-save"></i> 評価を確定する</button>
                                <{elseif $smarty.post.mode === 'reedit'}>
                                    <button type="submit" name="btn_save" class="btn btn-small btn-primary js-submit-save" value="edit"><i class="icon-save"></i> 保存する</button>
                                <{/if}>
                            </div>
                            <p>
                                <small>
                                    <i class="icon-exclamation-sign"></i>
                                    入力内容は自動的に<{if $smarty.post.mode === 'edit'}>一時<{/if}>保存されます。
                                    <{if $smarty.post.mode === 'reedit'}>
                                        明示的に保存したい場合は<b>[保存する]ボタン</b>で保存してください。
                                    <{/if}>
                                </small>
                            </p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {
                // 自動保存
                $('form').autosave({
                    callbacks: {
                        trigger: 'change',
                        scope: 'all',
                        save: {
                            method: 'ajax',
                            options: {
                                url: 'form_8edit.php',
                                method: 'post'
                            }
                        }
                    }
                });
            });
        </script>
    </body>
</html>