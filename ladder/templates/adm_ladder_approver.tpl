<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | ラダー設定: 承認者</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='ラダー設定: 承認者' hd_sub_title=''}>

            <div class="container-fluid">
                <form action="adm_ladder_edit.php" method="post">
                    <input type="hidden" name="ladder_id" value="" />

                    <div class="btn-toolbar">
                        <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                        <button type="submit" name="btn_save" id="btn_save" class="btn btn-small btn-primary" value="approver_save"><i class="icon-save"></i> 保存する</button>
                        <button type="button" class="btn btn-small btn-success js-approver-import"><i class="icon-group"></i> <{$titles.approver|default:'申請承認者'|escape}>の設定を一括取り込み</button>
                    </div>

                    <div class="well well-small">
                        <div class="control-group">
                            <label class="control-label"><strong>ラダー名称</strong></label>
                            <div class="controls">
                                <span class="input-xlarge ldr-uneditable"><{$data.title|escape}></span>
                            </div>
                        </div>
                        <hr/>

                        <div class="control-group">
                            <label class="control-label"><strong>対象部署の承認者を設定してください</strong></label>
                            <div class="controls">
                                <ul>
                                    <{foreach key=class item=emplist from=$covered_lists}>
                                        <{if !$data.approver[$class]}>
                                            <li class="text-error">
                                                <{$emplist[0].class_full_name|escape}> の承認者が未設定です
                                            </li>
                                        <{/if}>
                                    <{/foreach}>
                                    <{foreach item=emp from=$duplicate_lists}>
                                        <{if !$data.approver2[$emp.emp_id]}>
                                            <li class="text-error">
                                                対象職員と承認者が同一です。<{$emp.emp_lt_nm}> <{$emp.emp_ft_nm}>
                                            </li>
                                        <{/if}>
                                    <{/foreach}>
                                </ul>
                            </div>
                        </div>
                        <hr/>

                        <div class="form-group">
                            <table class="table table-condensed table-hover">
                                <thead>
                                    <tr>
                                        <th nowrap>部署</th>
                                        <th>対象職員</th>
                                        <th>承認者</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody id="js-select-table">
                                    <{foreach key=class item=emplist from=$covered_lists}>
                                        <tr>
                                            <td nowrap>
                                                <{$emplist[0].class_full_name|escape}>
                                            </td>
                                            <td>
                                                <{foreach item=emp from=$emplist}>
                                                    <span class="label label-info"><{$emp.emp_lt_nm}> <{$emp.emp_ft_nm}></span>
                                                <{/foreach}>
                                            </td>
                                            <td id="e<{$class|escape}>">
                                                <{foreach item=emp from=$data.approver[$class]}>
                                                    <div id="appr<{$emp.emp_id|escape}>" class="label label-important">
                                                        <{$emp.emp_lt_nm}> <{$emp.emp_ft_nm}>
                                                        <input type="hidden" name="appr[<{$class|escape}>][]" value="<{$emp.emp_id|escape}>" />
                                                        <a href="javascript:void(0);" class="ldr-emp-remove js-emp-remove">&times;</a>
                                                    </div>
                                                <{/foreach}>
                                            </td>
                                            <td>
                                                <button type="button" class="btn btn-small btn-success js-btn-emplist" data-id="<{$class|escape}>"><i class="icon-book"></i> 承認者の追加</button>
                                            </td>
                                        </tr>
                                    <{/foreach}>
                                </tbody>
                            </table>
                        </div>
                        <hr/>

                        <{if $duplicate_lists}>
                            <div class="form-group">
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th nowrap>上記の設定で承認者と重複している職員</th>
                                            <th>承認者</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody id="js-select-table">
                                        <{foreach item=dup from=$duplicate_lists}>
                                            <tr>
                                                <td nowrap>
                                                    <{$dup.emp_lt_nm}> <{$dup.emp_ft_nm}>
                                                </td>
                                                <td id="e<{$dup.emp_id|escape}>">
                                                    <{foreach item=emp from=$data.approver2[$dup.emp_id]}>
                                                        <div id="appr2<{$dup.emp_id|escape}>" class="label label-important">
                                                            <{$emp.emp_lt_nm}> <{$emp.emp_ft_nm}>
                                                            <input type="hidden" name="appr2[<{$dup.emp_id|escape}>][]" value="<{$emp.emp_id|escape}>" />
                                                            <a href="javascript:void(0);" class="ldr-emp-remove js-emp-remove">&times;</a>
                                                        </div>
                                                    <{/foreach}>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-small btn-success js-btn-emplist" data-id="<{$dup.emp_id|escape}>"><i class="icon-book"></i> 承認者の追加</button>
                                                </td>
                                            </tr>
                                        <{/foreach}>
                                    </tbody>
                                </table>
                            </div>
                        <{/if}>
                    </div>

                    <div class="btn-toolbar">
                        <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                        <button type="submit" name="btn_save" id="btn_save" class="btn btn-small btn-primary" value="approver_save"><i class="icon-save"></i> 保存する</button>
                    </div>
                </form>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {

                // 職員削除
                $('body').on('click', '.js-emp-remove', function() {
                    $(this).parent().remove();
                });

                // 申請承認者インポート
                $('body').on('click', '.js-approver-import', function() {
                    $.ajax({
                        url: 'approver_ajax.php',
                        type: 'post',
                        dataType: 'json'
                    }).done(function(data) {
                        $.each(data, function(i, appr) {
                            var item_id = appr['class_id'] + '-' + appr['atrb_id'] + '-' + appr['dept_id'] + '-' + (appr['room_id'] ? appr['room_id'] : '');

                            // 新人チェック
                            if ($('#e' + appr['emp_id']).size() !== 0) {
                                return;
                            }

                            // 重複チェック
                            var flg = false;
                            $('input[name="appr[' + item_id + '][]"]').each(function(j, e) {
                                if ($(e).val() === appr['emp_id']) {
                                    flg = true;
                                    return;
                                }
                            });
                            if (flg) {
                                return;
                            }

                            var html = [
                                '<div id="appr' + appr['emp_id'] + '" class="label label-important">',
                                appr['emp_name'],
                                '<input type="hidden" name="appr[' + item_id + '][]" value="' + appr['emp_id'] + '" /> ',
                                '<a href="javascript:void(0);" class="ldr-emp-remove js-emp-remove">&times;</a></div> '
                            ].join('');
                            $('#e' + item_id).append(html);
                        });
                    });
                });
            });

            /**
             * 職員の追加
             * @param item_id
             * @param emp_id
             * @param emp_name
             * @returns
             */
            function add_target_list(item_id, emp_id, emp_name) {
                var emp = emp_id.split(", ");
                var empname = emp_name.split(", ");
                if (emp.length !== 1) {
                    return;
                }

                // 重複チェック
                var flg = false;
                $('input[name="appr[' + item_id + '][]"]').each(function(idx, e) {
                    if ($(e).val() === emp[0]) {
                        flg = true;
                        return;
                    }
                });
                $('input[name="appr2[' + item_id + '][]"]').each(function(idx, e) {
                    if ($(e).val() === emp[0]) {
                        flg = true;
                        return;
                    }
                });
                if (flg) {
                    return;
                }

                if (item_id === emp[0]) {
                    alert('対象職員を承認者に設定することはできません');
                    return;
                }

                // appr or appr2
                var appr = 'appr';
                if (item_id.length === 12) {
                    appr = 'appr2';
                }

                var html = [
                    '<div id="' + appr + emp[0] + '" class="label label-important">',
                    empname[0],
                    '<input type="hidden" name="' + appr + '[' + item_id + '][]" value="' + emp[0] + '" /> ',
                    '<a href="javascript:void(0);" class="ldr-emp-remove js-emp-remove">&times;</a></div> '
                ].join('');
                $('#e' + item_id).append(html);
            }
        </script>
    </body>
</html>
