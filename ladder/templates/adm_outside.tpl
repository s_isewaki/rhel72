<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 院外研修マスタ</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='院外研修マスタ' hd_sub_title=''}>

            <div class="container-fluid">
                <div class="row-fluid">
                    <div class="row-fluid">
                        <form method="post" id="outside_form">
                            <div class="control-group">
                                <button type="button" data-php="adm_outside_apply.php" data-mode="new" class="js-form-button btn btn-small btn-primary"><i class="icon-plus"></i> 講座を登録する</button>
                            </div>
                            <div class="form-inline">
                                <{include file="__datepicker.tpl" date_id="srh_start_date" date_name="srh_start_date" date_value=$smarty.post.srh_start_date|escape}>
                                <span>〜</span>
                                <{include file="__datepicker.tpl" date_id="srh_last_date" date_name="srh_last_date" date_value=$smarty.post.srh_last_date|escape}>
                                &nbsp;
                                <button type="submit" id="search" name="search" value="search" class="btn btn-small btn-primary"><i class="icon-search"></i> 検索する</button>
                            </div>
                            <{if $list}>
                                <{include file="__pager.tpl"}>
                                <table class="table table-bordered table-hover table-condensed ldr-table">
                                    <thead>
                                        <tr>
                                            <th>開催日</th>
                                            <th>主催</th>
                                            <th>(研修番号)</th>
                                            <th>研修名</th>
                                            <th>開催地</th>
                                            <th>開催場所</th>
                                            <th style="white-space: nowrap">申請数</th>
                                            <th>編集</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <{foreach item=row from=$list}>
                                            <tr>
                                                <td class="ldr-td-date">
                                                    <span title="<{$row.start_date|date_format_jp:"%Y年%-m月%-d日(%a)"}>"><{$row.start_date|date_format_jp:"%-m月%-d日(%a)"}></span>
                                                    <{if $row.start_date <> $row.last_date}>
                                                        〜 <span title="<{$row.last_date|date_format_jp:"%Y年%-m月%-d日(%a)"}>"><{$row.last_date|date_format_jp:"%-m月%-d日(%a)"}></span>
                                                    <{/if}>
                                                </td>
                                                <td class="ldr-td-text-center"><{$row.organizer|escape}></td>
                                                <td class="ldr-td-text-left">(<{$row.no|escape}>)</td>
                                                <td class="ldr-td-text-left"><{$row.contents|escape}></td>
                                                <td class="ldr-td-text-left"><{$row.pref|default:'(未設定)'|escape}></td>
                                                <td class="ldr-td-text-left"><{$row.place|default:'(未設定)'|escape}></td>
                                                <td class="ldr-td-name"><{$row.count_all|escape}></td>
                                                <td class="ldr-td-name">
                                                    <button type="button" data-php="adm_outside_apply.php" data-mode="edit" data-id="<{$row.outside_id}>" class="js-form-button btn btn-small btn-danger">編集</button>
                                                </td>
                                            </tr>
                                        <{/foreach}>
                                    </tbody>
                                </table>

                            <{else}>
                                <div class="control-group">
                                    <{*余白調整*}>
                                </div>
                                <div class="alert alert-error">院外研修はありません</div>
                            <{/if}>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <{include form_selector="#outside_form" file="__pager_js.tpl"}>
    </body>
</html>


