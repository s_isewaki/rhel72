<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 権限管理</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='権限管理' hd_sub_title=''}>
            <div class="container-fluid">
                <{include file="__save_message.tpl"}>
                <div class="row-fluid">
                    <ul class="nav nav-tabs">
                        <li class="<{if $active === 'approver'}>active<{/if}>"><a href="#approver" data-toggle="tab"><i class="icon-group"></i> <{$titles.approver|default:'申請承認者'|escape}></a></li>
                        <li class="<{if $active === 'committee'}>active<{/if}>"><a href="#committee" data-toggle="tab"><i class="icon-group"></i> <{$titles.committee|default:'認定委員会'|escape}></a></li>
                        <li class="<{if $active === 'inside'}>active<{/if}>"><a href="#inside" data-toggle="tab"><i class="icon-group"></i> 院内研修担当</a></li>
                        <li class="<{if $active === 'outside'}>active<{/if}>"><a href="#outside" data-toggle="tab"><i class="icon-group"></i> 院外研修担当</a></li>
                        <li class="<{if $active === 'relief'}>active<{/if}>"><a href="#relief" data-toggle="tab"><i class="icon-group"></i> 救護員研修担当</a></li>
                        <li class="<{if $active === 'training'}>active<{/if}>"><a href="#training" data-toggle="tab"><i class="icon-group"></i> 研修管理者</a></li>
                        <li class="<{if $active === 'analysis'}>active<{/if}>"><a href="#analysis" data-toggle="tab"><i class="icon-group"></i> 分析</a></li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane <{if $active === 'approver'}>active<{else}>fade<{/if}> in" id="approver">
                            <form action="adm_permission.php" method="post" id="form_approver">
                                <input type="hidden" id="approver_mode" name="mode" value="save" />
                                <input type="hidden" name="tab" value="approver" />
                                <div class="well well-small" id="form_commission">
                                    <div class="form-group">
                                        <fieldset>
                                            <legend><{$titles.approver|default:'申請承認者'|escape}></legend>
                                            <label>各部署の<{$titles.approver|default:'申請承認者'|escape}>を設定してください。</label>
                                            <div class="btn-group">
                                                <select name="pulldown_class" class="js-pulldown-approver" data-target="#form_approver">
                                                    <option value="">設定済み</option>
                                                    <{foreach item=row from=$appr_pulldown_list}>
                                                        <option value="<{$row.class_id|escape}>-<{$row.atrb_id|escape}>"><{$row.class_nm|escape}> &gt; <{$row.atrb_nm|escape}></option>
                                                    <{/foreach}>
                                                </select>
                                            </div>
                                            <{if $appr_list}>
                                                <table class="table table-striped table-hover table-condensed">
                                                    <thead>
                                                        <tr>
                                                            <th>部署</th>
                                                            <th><{$titles.approver|default:'申請承認者'|escape}></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="appr_list">
                                                        <{foreach item=row from=$appr_list}>
                                                            <tr>
                                                                <td>
                                                                    <{$row.class_full_name|escape}>
                                                                    <input type="hidden" name="class[]" value="<{$row.class_id|escape}>-<{$row.atrb_id|escape}>-<{$row.dept_id|escape}><{if $row.room_id}>-<{$row.room_id|escape}><{/if}>" />
                                                                </td>
                                                                <td>
                                                                    <div class="input-prepend">
                                                                        <span class="add-on"><i class="icon-user-md"></i></span>
                                                                        <span
                                                                            class="input-medium ldr-uneditable js-approver-emplist pointer-cursor"
                                                                            id="appr-<{$row.class_id|escape}>-<{$row.atrb_id|escape}>-<{$row.dept_id|escape}><{if $row.room_id}>-<{$row.room_id|escape}><{/if}>-name"
                                                                            data-id="appr-<{$row.class_id|escape}>-<{$row.atrb_id|escape}>-<{$row.dept_id|escape}><{if $row.room_id}>-<{$row.room_id|escape}><{/if}>"><{$row.emp_name|escape}></span>
                                                                        <input
                                                                            type="hidden"
                                                                            id="appr-<{$row.class_id|escape}>-<{$row.atrb_id|escape}>-<{$row.dept_id|escape}><{if $row.room_id}>-<{$row.room_id|escape}><{/if}>"
                                                                            name="approver[<{$row.class_id|escape}>-<{$row.atrb_id|escape}>-<{$row.dept_id|escape}><{if $row.room_id}>-<{$row.room_id|escape}><{/if}>]"
                                                                            value="<{$row.emp_id|escape}>" />
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        <{/foreach}>
                                                    </tbody>
                                                </table>
                                                <div class="btn-toolbar">
                                                    <button type="submit" name="btn_save" id="btn_save" class="btn btn-small btn-primary" value="btn_save"><i class="icon-save"></i> 保存する</button>
                                                </div>
                                            <{else}>
                                                <div class="alert alert-error">
                                                    <{$titles.approver|default:'申請承認者'|escape}>が設定されていません
                                                </div>
                                            <{/if}>
                                        </fieldset>
                                    </div>
                                </div>
                            </form>

                        </div>

                        <div class="tab-pane <{if $active === 'committee'}>active<{else}>fade<{/if}> in" id="committee">
                            <form action="adm_permission.php" method="post">
                                <input type="hidden" name="mode" value="save" />
                                <input type="hidden" name="tab" value="committee" />
                                <div class="well well-small" id="form_commission">
                                    <div class="form-group">
                                        <fieldset>
                                            <legend><{$titles.committee|default:'認定委員会'|escape}></legend>
                                            <label><{$titles.committee|default:'認定委員会'|escape}>のメンバーを選択してください。</label>
                                            <div class="btn-toolbar">
                                                <button type="button" data-id="committee" class="js-btn-emplist btn btn-small btn-success"><i class="icon-book"></i> 職員名簿</button>
                                            </div>
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>メンバー</th>
                                                        <th><button type="button" id="js-btn-checkbox" class="btn btn-mini"><i class="icon-check-sign"></i></button> 監査者設定権限</th>
                                                        <th>所属</th>
                                                        <th>役職</th>
                                                        <th class="pull-right"><i class='icon-remove'></i></th>
                                                    </tr>
                                                </thead>
                                                <tbody id="comm_list">
                                                    <{foreach item=row from=$comm_list}>
                                                        <tr id="e<{$row->emp_id()|escape}>">
                                                            <td><{$row->emp_name()|escape}><input type="hidden" name="emp_id[]" value="<{$row->emp_id()|escape}>" /></td>
                                                            <td><label class="checkbox"><input type="checkbox" class="ck-auditor" name="auditor[<{$row->emp_id()|escape}>]" value="1" <{if $row->auditor()}>checked<{/if}>/></label></td>
                                                            <td><{$row->class_full_name()|escape}></td>
                                                            <td><{$row->st_name()|escape}></td>
                                                            <td><a class="pull-right js-row-remove" href="javascript:void(0);"><i class="icon-remove"></i></a></td>
                                                        </tr>
                                                    <{/foreach}>
                                                </tbody>
                                            </table>
                                            <div class="btn-toolbar">
                                                <button type="submit" name="btn_save" id="btn_save" class="btn btn-small btn-primary" value="btn_save"><i class="icon-save"></i> 保存する</button>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane <{if $active === 'inside'}>active<{else}>fade<{/if}> in" id="inside">
                            <form action="adm_permission.php" method="post">
                                <input type="hidden" name="mode" value="save" />
                                <input type="hidden" name="tab" value="inside" />
                                <div class="well well-small" id="form_commission">
                                    <div class="form-group">
                                        <fieldset>
                                            <legend>院内研修担当</legend>
                                            <label>院内研修担当のメンバーを選択してください。</label>
                                            <div class="btn-toolbar">
                                                <button type="button" data-id="inside" class="js-btn-emplist btn btn-small btn-success"><i class="icon-book"></i> 職員名簿</button>
                                            </div>
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>メンバー</th>
                                                        <th>所属</th>
                                                        <th>役職</th>
                                                        <th class="pull-right"><i class='icon-remove'></i></th>
                                                    </tr>
                                                </thead>
                                                <tbody id="inside_list">
                                                    <{foreach item=row from=$inside_list}>
                                                        <tr id="i<{$row->emp_id()|escape}>">
                                                            <td><{$row->emp_name()|escape}><input type="hidden" name="emp_id[]" value="<{$row->emp_id()|escape}>" /></td>
                                                            <td><{$row->class_full_name()|escape}></td>
                                                            <td><{$row->st_name()|escape}></td>
                                                            <td><a class="pull-right js-row-remove" href="javascript:void(0);"><i class="icon-remove"></i></a></td>
                                                        </tr>
                                                    <{/foreach}>
                                                </tbody>
                                            </table>
                                            <div class="btn-toolbar">
                                                <button type="submit" name="btn_save" id="btn_save" class="btn btn-small btn-primary" value="btn_save"><i class="icon-save"></i> 保存する</button>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane <{if $active === 'outside'}>active<{else}>fade<{/if}> in" id="outside">
                            <form action="adm_permission.php" method="post">
                                <input type="hidden" name="mode" value="save" />
                                <input type="hidden" name="tab" value="outside" />
                                <div class="well well-small" id="form_commission">
                                    <div class="form-group">
                                        <fieldset>
                                            <legend>院外研修担当</legend>
                                            <label>院外研修担当のメンバーを選択してください。</label>
                                            <div class="btn-toolbar">
                                                <button type="button" data-id="outside" class="js-btn-emplist btn btn-small btn-success"><i class="icon-book"></i> 職員名簿</button>
                                            </div>
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>メンバー</th>
                                                        <th>所属</th>
                                                        <th>役職</th>
                                                        <th class="pull-right"><i class='icon-remove'></i></th>
                                                    </tr>
                                                </thead>
                                                <tbody id="outside_list">
                                                    <{foreach item=row from=$outside_list}>
                                                        <tr id="o<{$row->emp_id()|escape}>">
                                                            <td><{$row->emp_name()|escape}><input type="hidden" name="emp_id[]" value="<{$row->emp_id()|escape}>" /></td>
                                                            <td><{$row->class_full_name()|escape}></td>
                                                            <td><{$row->st_name()|escape}></td>
                                                            <td><a class="pull-right js-row-remove" href="javascript:void(0);"><i class="icon-remove"></i></a></td>
                                                        </tr>
                                                    <{/foreach}>
                                                </tbody>
                                            </table>
                                            <div class="btn-toolbar">
                                                <button type="submit" name="btn_save" id="btn_save" class="btn btn-small btn-primary" value="btn_save"><i class="icon-save"></i> 保存する</button>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="tab-pane <{if $active === 'relief'}>active<{else}>fade<{/if}> in" id="relief">
                            <form action="adm_permission.php" method="post">
                                <input type="hidden" name="mode" value="save" />
                                <input type="hidden" name="tab" value="relief" />
                                <div class="well well-small" id="form_commission">
                                    <div class="form-group">
                                        <fieldset>
                                            <legend>救護員研修担当</legend>
                                            <label>救護員研修担当のメンバーを選択してください。</label>
                                            <div class="btn-toolbar">
                                                <button type="button" data-id="relief" class="js-btn-emplist btn btn-small btn-success"><i class="icon-book"></i> 職員名簿</button>
                                            </div>
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>メンバー</th>
                                                        <th>所属</th>
                                                        <th>役職</th>
                                                        <th class="pull-right"><i class='icon-remove'></i></th>
                                                    </tr>
                                                </thead>
                                                <tbody id="relief_list">
                                                    <{foreach item=row from=$relief_list}>
                                                        <tr id="r<{$row->emp_id()|escape}>">
                                                            <td><{$row->emp_name()|escape}><input type="hidden" name="emp_id[]" value="<{$row->emp_id()|escape}>" /></td>
                                                            <td><{$row->class_full_name()|escape}></td>
                                                            <td><{$row->st_name()|escape}></td>
                                                            <td><a class="pull-right js-row-remove" href="javascript:void(0);"><i class="icon-remove"></i></a></td>
                                                        </tr>
                                                    <{/foreach}>
                                                </tbody>
                                            </table>
                                            <div class="btn-toolbar">
                                                <button type="submit" name="btn_save" id="btn_save" class="btn btn-small btn-primary" value="btn_save"><i class="icon-save"></i> 保存する</button>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="tab-pane <{if $active === 'training'}>active<{else}>fade<{/if}> in" id="training">
                            <form action="adm_permission.php" method="post">
                                <input type="hidden" name="mode" value="save" />
                                <input type="hidden" name="tab" value="training" />
                                <div class="well well-small" id="form_commission">
                                    <div class="form-group">
                                        <fieldset>
                                            <legend>研修管理者</legend>
                                            <label>研修管理者のメンバーを選択してください。</label>
                                            <div class="btn-toolbar">
                                                <button type="button" data-id="training" class="js-btn-emplist btn btn-small btn-success"><i class="icon-book"></i> 職員名簿</button>
                                            </div>
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>メンバー</th>
                                                        <th>所属</th>
                                                        <th>役職</th>
                                                        <th class="pull-right"><i class='icon-remove'></i></th>
                                                    </tr>
                                                </thead>
                                                <tbody id="training_list">
                                                    <{foreach item=row from=$training_list}>
                                                        <tr id="t<{$row->emp_id()|escape}>">
                                                            <td><{$row->emp_name()|escape}><input type="hidden" name="emp_id[]" value="<{$row->emp_id()|escape}>" /></td>
                                                            <td><{$row->class_full_name()|escape}></td>
                                                            <td><{$row->st_name()|escape}></td>
                                                            <td><a class="pull-right js-row-remove" href="javascript:void(0);"><i class="icon-remove"></i></a></td>
                                                        </tr>
                                                    <{/foreach}>
                                                </tbody>
                                            </table>
                                            <div class="btn-toolbar">
                                                <button type="submit" name="btn_save" id="btn_save" class="btn btn-small btn-primary" value="btn_save"><i class="icon-save"></i> 保存する</button>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="tab-pane <{if $active === 'analysis'}>active<{else}>fade<{/if}>" id="analysis">
                            <form action="adm_permission.php" method="post">
                                <input type="hidden" name="mode" value="save" />
                                <input type="hidden" name="tab" value="analysis" />
                                <div class="well well-small" id="form_commission">
                                    <div class="form-group">
                                        <fieldset>
                                            <legend>分析担当</legend>
                                            <label>分析担当のメンバーを選択してください。</label>
                                            <div class="btn-toolbar">
                                                <button type="button" data-id="analysis" class="js-btn-emplist btn btn-small btn-success"><i class="icon-book"></i> 職員名簿</button>
                                            </div>
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>メンバー</th>
                                                        <th>所属</th>
                                                        <th>役職</th>
                                                        <th class="pull-right"><i class='icon-remove'></i></th>
                                                    </tr>
                                                </thead>
                                                <tbody id="analysis_list">
                                                    <{foreach item=row from=$analysis_list}>
                                                        <tr id="a<{$row->emp_id()|escape}>">
                                                            <td><{$row->emp_name()|escape}><input type="hidden" name="emp_id[]" value="<{$row->emp_id()|escape}>" /></td>
                                                            <td><{$row->class_full_name()|escape}></td>
                                                            <td><{$row->st_name()|escape}></td>
                                                            <td><a class="pull-right js-row-remove" href="javascript:void(0);"><i class="icon-remove"></i></a></td>
                                                        </tr>
                                                    <{/foreach}>
                                                </tbody>
                                            </table>

                                            <hr/>
                                            <label>配置表の利用権限</label>
                                            <div class="controls">
                                                <label class="inline radio">
                                                    <input type="radio" name="analysis_placement" value="1" />ラダー管理者、委員会、分析担当
                                                </label>
                                                <label class="inline radio">
                                                    <input type="radio" name="analysis_placement" value="2" />分析担当のみ
                                                </label>
                                            </div>

                                            <div class="btn-toolbar">
                                                <button type="submit" name="btn_save" id="btn_save" class="btn btn-small btn-primary" value="btn_save"><i class="icon-save"></i> 保存する</button>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <script type="text/javascript">

            function closeEmployeeList() {
                if (childwin != null && !childwin.closed) {
                    childwin.close();
                }
                childwin = null;
            }

            // 職員の追加
            function add_target_list(item_id, emp_id, emp_name) {
                var emp = emp_id.split(", ");
                var empname = emp_name.split(", ");

                if (item_id === 'committee') {
                    for (i = 0; i < emp.length; i++) {
                        if ($('#e' + emp[i]).size() === 0) {
                            $('#comm_list').append('<tr id="e' + emp[i] + '"><td>' + empname[i] + '<input type="hidden" name="emp_id[]" value="' + emp[i] + '" /></td><td><label class="checkbox"><input type="checkbox" class="ck-auditor" name="auditor[' + emp[i] + ']" value="1"/></label></td><td class="text-warning"><small>(保存してください)</small></td><td class="text-warning"><small>(保存してください)</small></td><td><a class="pull-right js-row-remove" href="javascript:void(0);"><i class="icon-remove"></i></a></td></tr>');
                        }
                    }
                }
                else if (item_id === 'inside') {
                    // 院内院外研修
                    add_tr('#inside_list', 'i', emp, empname);
                }
                else if (item_id === 'outside') {
                    // 院内院外研修
                    add_tr('#outside_list', 'o', emp, empname);
                }
                else if (item_id === 'relief') {
                    // 救護員研修
                    add_tr('#relief_list', 'r', emp, empname);
                }
                else if (item_id === 'training') {
                    // 研修管理者
                    add_tr('#training_list', 't', emp, empname);
                }
                else if (item_id === 'analysis') {
                    // 分析担当
                    add_tr('#analysis_list', 'a', emp, empname);
                }
                else {
                    $('#' + item_id).val(emp[0]);
                    $('#' + item_id + '-name').html(empname[0]);
                }
            }
            // 職員をtableに追加
            function add_tr(selector, identifier, emp, empname) {
                for (i = 0; i < emp.length; i++) {
                    if ($('#' + identifier + emp[i]).size() === 0) {
                        $(selector).append('<tr id="' + identifier + emp[i] + '"><td>' + empname[i] + '<input type="hidden" name="emp_id[]" value="' + emp[i] + '" /></td><td class="text-warning"><small>(保存してください)</small></td><td class="text-warning"><small>(保存してください)</small></td><td><a class="pull-right js-row-remove" href="javascript:void(0);"><i class="icon-remove"></i></a></td></tr>');
                    }
                }
            }
            $(function() {
                // チェックボックス
                $('#js-btn-checkbox').click(function() {
                    if ($(this).children('i').hasClass('icon-check-sign')) {
                        $('input.ck-auditor').prop('checked', true);
                    }
                    else {
                        $('input.ck-auditor').prop('checked', false);
                    }
                    $(this).children('i').toggleClass('icon-check-sign').toggleClass('icon-check-empty');
                });

                // 職員の削除
                $('body').on('click', 'a.js-row-remove', function() {
                    $(this).parent().parent().remove();
                });

                // 承認者部署プルダウン選択
                $('select.js-pulldown-approver').change(function() {
                    $('#approver_mode').val('');
                    $($(this).data('target')).submit();
                });

                // 承認者 職員名簿
                $('.js-approver-emplist').click(function() {
                    open_emplist($(this).data('id'));
                });
            });
        </script>
    </body>
</html>
