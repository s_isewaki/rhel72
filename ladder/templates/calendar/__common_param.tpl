
editable: false,
header: {
left: 'prev,next today',
center: 'title',
right: 'month,basicWeek,agendaDay'
},
// スクロール開始時間
firstHour: 9,
// 最小時間
minTime: 8,
// 最大時間
maxTime: 19,
// スロットの時間の書式
axisFormat: 'H(:mm)',
// 時間の書式
timeFormat: 'H(:mm)',
// タイトルの書式
titleFormat: {
month: 'yyyy年M月', // 2013年9月
week: "yyyy年M月d日{ 〜 }{[yyyy年]}{[M月]d日}", // 2013年9月7日 ~ 13日
day: "yyyy年M月d日'('ddd')'"                  // 2013年9月7日(火)
},
// ボタン文字列
buttonText: {
prev: '&lsaquo;', // <
next: '&rsaquo;', // >
prevYear: '&laquo;', // <<
nextYear: '&raquo;', // >>
today: '今日',
month: '月',
week: '週',
day: '日'
},
// 月名称
monthNames: ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月'],
// 月略称
monthNamesShort: ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月'],
// 曜日名称
dayNames: ['日曜日', '月曜日', '火曜日', '水曜日', '木曜日', '金曜日', '土曜日'],
// 曜日略称
dayNamesShort: ['日', '月', '火', '水', '木', '金', '土'],
