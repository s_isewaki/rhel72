<{* dashboard.tplに書くとインデントが崩れしまうので外に出しました。 *}>

var date = new Date();
var d = date.getDate();
var m = date.getMonth();
var y = date.getFullYear();

$('#calendar').fullCalendar({
<{include file="calendar/__common_param.tpl"}>

// 高さ(px)
height: 400,
// カレンダーの縦横比(比率が大きくなると高さが縮む) defalut 1.35
aspectRatio: 3.00,

events: [
<{foreach item=inside from=$list_inside}>
    {
    title: '(<{$inside.no|escape}>)<{$inside.title|escape}> 第<{$inside.number|escape}>回',
    start: '<{$inside.training_date|escape}> <{$inside.training_start_time|escape}>',
    end: '<{$inside.training_date|escape}> <{$inside.training_last_time|escape}>',
    color: 'blue',
    textColor: 'white',
    allDay: false
    },
<{/foreach}>
<{foreach item=outside from=$list_outside}>
    {
    title: '(<{$outside.no|escape}>)<{$outside.contents|escape}>',
    start: '<{$outside.start_date|escape}>',
    end: '<{$outside.last_date|escape}>',
    color: 'orange',
    textColor: 'white',
    allDay: true
    },
<{/foreach}>
<{foreach item=relif from=$list_relief}>
    {
    title: '<{$relif.subject|escape}> <{$roman_num[$relif.unit]|escape}>',
    start: '<{$relif.training_day|escape}>',
    color: 'green',
    textColor: 'white',
    allDay: true
    },
<{/foreach}>

<{* ie8エラー対策の空カッコ*}>
{
}
]
});