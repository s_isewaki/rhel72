<{* dashboard.tplに書くとインデントが崩れしまうので外に出しました。 *}>

var date = new Date();
var d = date.getDate();
var m = date.getMonth();
var y = date.getFullYear();
var init = true;
$('#calendar').fullCalendar({
<{include file="calendar/__common_param.tpl"}>


aspectRatio: 2.25,
events: [
<{foreach item=inside from=$list}>
    {
    title: '<{if $inside.no}>(<{$inside.no|escape}>)<{/if}><{$inside.title|escape}> 第<{$inside.number|escape}>回',
    start: '<{$inside.training_date|escape}> <{$inside.training_start_time|escape}>',
    end: '<{$inside.training_date|escape}> <{$inside.training_last_time|escape}>',

    <{if $inside.level === '1'}>
        color: '#3a87ad',
    <{elseif $inside.level === '2'}>
        color: '#333333',
    <{elseif $inside.level === '3'}>
        color: '#468847',
    <{elseif $inside.level === '4'}>
        color: '#f89406',
    <{elseif $inside.level === '5'}>
        color: '#b94a48',
    <{/if}>
    textColor: 'white',
    allDay: false,
    url: 'trn_inside_apply.php?id=<{$inside.inside_id|escape}>'
    },
<{/foreach}>

<{* ie8エラー対策の空カッコ*}>
{
}
],
viewRender : function( view, element ) {
<{if $return}>
    if(!init) {
    var dt = new Date(view.start);
    var dc = dt.getFullYear()+ "," + dt.getMonth() + "," + dt.getDate()+","+view.name;
    $.cookie("ldr_inside_calendar", dc, {expires: cookie_expires});
    }
    init = false;
<{else}>
    var dt = new Date(view.start);
    var dc = dt.getFullYear()+ "," + dt.getMonth() + "," + dt.getDate()+","+view.name;
    $.cookie("ldr_inside_calendar", dc, {expires: cookie_expires});
<{/if}>
},
});
<{if $return}>
    var cookieValue = $.cookie("ldr_inside_calendar");
    if(cookieValue) {
    var dt = cookieValue.split(",");
    $('#calendar').fullCalendar('changeView',dt[3]);
    $('#calendar').fullCalendar('gotoDate', dt[0],dt[1],dt[2] );
    }
<{/if}>

