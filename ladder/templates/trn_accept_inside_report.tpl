<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 院内研修課題一覧</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='院内研修課題一覧' hd_sub_title=''}>

            <div class="container-fluid">
                <form method="post" id="inside_form">
                    <input type='hidden' id='id' name='id' value='<{$smarty.post.id}>' />
                    <div class="well well-small">
                        <fieldset>
                            <legend>
                                <a
                                    href="trn_inside_contents_modal.php?id=<{$trn.inside_id|escape}>"
                                    data-toggle="modal"
                                    data-backdrop="true"
                                    data-target="#inside_contents">
                                    <{if $trn.no}>(<{$trn.no|escape}>) <{/if}><{$trn.title|escape}>
                                </a>
                                <a
                                    href="trn_inside_number_modal.php?id=<{$trn.inside_id|escape}>&number=<{$trn.contents.number|escape}>"
                                    data-toggle="modal"
                                    data-backdrop="true"
                                    data-target="#inside_number">
                                    第<{$trn.contents.number|escape}>回 / 全<{$trn.number_of|escape}>回
                                </a>
                            </legend>
                            <{if $smarty.post.srh_work}>
                                <div class="control-group">
                                    <div class="form-inline">
                                        <select name="srh_work" id="srh_work" class="srh input-medium">
                                            <{if $trn.contents.before_work === 't'}>
                                                <option value="before">前課題</option>
                                            <{/if}>
                                            <{if $trn.contents.after_work === 't'}>
                                                <option value="after">後課題</option>
                                            <{/if}>

                                        </select>
                                        &nbsp;&nbsp;
                                        <span>
                                            <label class="radio inline"><input type="radio" id="srh_report_0" name="srh_report" value="" class="srh"/>すべて</label>
                                            <label class="radio inline"><input type="radio" id="srh_report_1" name="srh_report" value="reported" class="srh"/>提出済み</label>
                                            <label class="radio inline"><input type="radio" id="srh_report_2" name="srh_report" value="nonreport" class="srh"/>未提出</label>
                                        </span>
                                    </div>
                                </div>
                                <button type="button" data-id="<{$smarty.post.id}>" data-target="inside_report" data-emp="" data-php="trn_accept_inside_report.php" class="js-form-print btn btn-primary btn-small"><i class="icon-print"></i> 印刷する</button>
                            <{else}>
                                <div class="alert alert-error">課題の提出はありません</div>
                            <{/if}>
                            <{if $list}>
                                <{include file="__pager.tpl"}>
                                <table class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th nowrap>部署</th>
                                            <th nowrap>職員ID</th>
                                            <th nowrap>名前</th>
                                            <th nowrap width="60px" style="text-align: center">出欠</th>

                                            <{if $smarty.post.srh_work === 'before'}>
                                                <th nowrap style="text-align: center">提出日時</th>
                                                <th nowrap>前課題</th>

                                            <{elseif $smarty.post.srh_work === 'after'}>
                                                <th nowrap style="text-align: center">提出日時</th>
                                                <th nowrap>後課題</th>

                                            <{/if}>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <{foreach item=row from=$list}>
                                            <tr>
                                                <td class="ldr-td-text-left">
                                                    <{$row.class_full_name|escape}>
                                                </td>
                                                <td class="ldr-td-text"><{$row.emp_personal_id}></td>
                                                <td class="ldr-td-text"><{$row.name|escape}></td>
                                                <td class="ldr-td-name">
                                                    <{if $row.attendance === '1'}>
                                                        <span class="label label-success">出席</span>
                                                    <{elseif $row.attendance === '2'}>
                                                        <span class="label label-inverse">欠席</span>
                                                    <{else}>
                                                        <span class="label label-important">未設定</span>
                                                    <{/if}>
                                                </td>
                                                <{if $smarty.post.srh_work === 'before' or $smarty.post.srh_work === 'after'}>
                                                    <td class="ldr-td-date">
                                                        <{if $row.r_status === '1'}>
                                                            <{$row.update|date_format_jp:"%Y年%-m月%-d日(%a)<br/>%R"}>
                                                        <{else}>
                                                            <span class="label label-important">未提出</span>
                                                        <{/if}>

                                                    </td>
                                                    <td class="ldr-td-text-left">
                                                        <{if $row.r_status === '1'}>
                                                            <{$row.report|escape|nl2br}>
                                                        <{/if}>
                                                    </td>
                                                <{/if}>

                                            </tr>
                                        <{/foreach}>
                                    </tbody>
                                </table>

                            <{else}>
                                <div class="control-group"></div>
                                <div class="alert alert-error">対象者がいません</div>
                            <{/if}>
                        </fieldset>
                        <div class="btn-toolbar">
                            <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <{include file="__modal_view.tpl" modal_id="inside_contents" modal_title="研修内容"}>
        <{include file="__modal_view.tpl" modal_id="inside_number" modal_title="研修内容"}>
        <{include file="__js.tpl"}>
        <{include form_selector="#inside_form" file="__pager_js.tpl"}>
        <script type="text/javascript">
            $(function() {

            <{if $smarty.post.srh_report === "reported"}>
                $('#srh_report_1').prop('checked', true);
            <{elseif $smarty.post.srh_report === "nonreport"}>
                $('#srh_report_2').prop('checked', true);
            <{else}>
                $('#srh_report_0').prop('checked', true);
            <{/if}>

                $('#srh_work').val("<{$smarty.post.srh_work}>");

                $('.srh').change(function() {
                    $('#inside_form')
                        .append($('<input></input>').attr({type: 'hidden', name: 'search', value: 'on'}))
                        .submit();
                });
            });
        </script>
    </body>
</html>
