<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix ����ꥢ��ȯ����� | <{$ldr.config.form_7.title|escape}></title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- �ᥤ�󥨥ꥢ -->
        <div class="content">
            <{include file="__header.tpl" hd_title=$ldr.config.form_7.title hd_sub_title=$ldr.config.form_7.detailL}>

            <div class="container-fluid">
                <div class="row-fluid">
                    <{*�����ܥ���*}>
                    <form id="form_print" method="post" class="pull-right">
                        <input type="hidden" name="mode" value="" />
                        <input type="hidden" name="menu" value="" />
                        <input type="hidden" name="id" value="" />
                        <input type="hidden" name="target_emp_id" value=""/>
                        <input type="hidden" name="owner_url" value="" />
                        <{if $emp->emp_id() === $smarty.post.target_emp_id}>
                            &nbsp;<button type="button" data-id="<{$smarty.post.id}>" data-emp="<{$smarty.post.target_emp_id}>" data-php="form_7edit.php" data-mode="edit" class="js-form-button btn btn-danger btn-small"><i class="icon-edit"></i> �Խ�����</button>
                        <{else}>
                            &nbsp;<button type="button" data-id="<{$smarty.post.id}>" data-emp="<{$smarty.post.target_emp_id}>" data-php="form_7edit_appraiser.php" data-mode="edit" class="js-form-button btn btn-danger btn-small"><i class="icon-edit"></i> �Խ�����</button>
                        <{/if}>
                        &nbsp;<button type="button" data-id="<{$smarty.post.id}>" data-emp="<{$smarty.post.target_emp_id}>" data-php="form_7view.php" data-target="form7" class="js-form-print btn btn-primary btn-small"><i class="icon-print"></i> ��������</button>
                    </form>

                    <{*�ͼ����̥ܥ���*}>
                    <form id="form_edit" method="post">
                        <{include file='__form_view.tpl' op_srh='false' tmp_id='form_7view'}>

                        <input type="hidden" name="active" id="active" value="" />

                        <div class="well well-small">
                            <div class="control-group">
                                <div class="input-prepend" style="margin-bottom:0;">
                                    <span class="add-on">������٥�</span>
                                    <span class="ldr-uneditable input-mini ldr-level" style="margin-bottom:0;"><{$apply->level_text()|escape}></span>
                                </div>
                                <div class="input-prepend" style="margin-bottom:0;">
                                    <span class="add-on">������</span>
                                    <span class="ldr-uneditable input-large" style="margin-bottom:0;"><{$target_emp->emp_name()|escape}> (<{$target_emp->class_full_name()|escape}>)</span>
                                </div>
                            </div>
                            <hr/>

                            <ul id="tabs" class="nav nav-tabs">
                                <li class="<{if $active === 'sec1'}>active<{/if}>">
                                    <a href="#sec1" data-sec="sec1" data-toggle="tab">
                                        �׾��Ǹ����
                                    </a>
                                </li>
                                <{foreach item=row from=$group1_list}>
                                    <li class="<{if $active === $row.guideline_group1}>active<{/if}>">
                                        <a href="#g1-<{$row.guideline_group1|escape}>" data-toggle="tab"><{$row.name|escape}></a>
                                    </li>
                                <{/foreach}>
                                <li class="<{if $active === 'sec3'}>active<{/if}>">
                                    <a href="#sec3" data-sec="sec3" data-toggle="tab">
                                        �����٥�
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content" style="overflow:hidden;">
                                <div class="tab-pane <{if $active === 'sec1'}>active<{else}>fade<{/if}>" id="sec1">
                                    <div class="control-group" >
                                        <{* form71 tab���� *}>
                                        <table class="table table-striped table-hover table-bordered table-condensed ldr-table">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>��̾</th>
                                                    <th>ɾ������</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <{foreach item=row from=$form1.appr_list name=loopname}>
                                                    <tr>
                                                        <{if $smarty.foreach.loopname.first}>
                                                            <td class="ldr-td-vertical-label" rowspan="<{$form1.appr_list|@count}>" style='background-color:#fff'>
                                                                ��<br/>��
                                                            </td>
                                                        <{/if}>
                                                        <td class="ldr-td-text-left"><{$row.emp_name|escape}></td>
                                                        <td class="ldr-td-text-left"><{$form7[$row.emp_id].knowledge|escape|nl2br}></td>
                                                    </tr>
                                                <{/foreach}>
                                                <{foreach item=row from=$form1.appr_list name=loopname}>
                                                    <tr>
                                                        <{if $smarty.foreach.loopname.first}>
                                                            <td class="ldr-td-vertical-label" rowspan="<{$form1.appr_list|@count}>" style='background-color:#fff'>
                                                                Ƚ<br/>��
                                                            </td>
                                                        <{/if}>
                                                        <td class="ldr-td-text-left"><{$row.emp_name}></td>
                                                        <td class="ldr-td-text-left"><{$form7[$row.emp_id].decision|escape|nl2br}></td>
                                                    </tr>
                                                <{/foreach}>
                                                <{foreach item=row from=$form1.appr_list name=loopname}>
                                                    <tr>
                                                        <{if $smarty.foreach.loopname.first}>
                                                            <td class="ldr-td-vertical-label" rowspan="<{$form1.appr_list|@count}>" style='background-color:#fff'>
                                                                ��<br/>��
                                                            </td>
                                                        <{/if}>
                                                        <td class="ldr-td-text-left"><{$row.emp_name}></td>
                                                        <td class="ldr-td-text-left"><{$form7[$row.emp_id].action|escape|nl2br}></td>
                                                    </tr>
                                                <{/foreach}>
                                                <{foreach item=row from=$form1.appr_list name=loopname}>
                                                    <tr>
                                                        <{if $smarty.foreach.loopname.first}>
                                                            <td class="ldr-td-vertical-label" rowspan="<{$form1.appr_list|@count}>" style='background-color:#fff'>
                                                                ��<br/>��<br/>��<br/>��<br/>��
                                                            </td>
                                                        <{/if}>
                                                        <td class="ldr-td-text-left"><{$row.emp_name}></td>
                                                        <td class="ldr-td-text-left"><{$form7[$row.emp_id].result|escape|nl2br}></td>
                                                    </tr>
                                                <{/foreach}>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <{* form72 tab����*}>
                                <{foreach item=g1 from=$group1_list}>
                                    <div class="tab-pane <{if $active === $g1.guideline_group1}>active<{else}>fade<{/if}>" id="g1-<{$g1.guideline_group1|escape}>" style="overflow:hidden;">

                                        <table class="table table-striped table-hover table-bordered table-condensed ldr-table">
                                            <thead>
                                                <tr>
                                                    <th>��ư��ɸ</th>
                                                    <th>ɾ����</th>
                                                    <th width="40px">ɾ��</th>
                                                    <{if $ldr.config.reason === '1'}><th>����</th><{/if}>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <{foreach item=asm from=$assessment_list[$g1.guideline_group1]}>
                                                    <{foreach item=row from=$form1.appr_list_all name=loopname}>
                                                        <tr <{if $smarty.foreach.loopname.first}>class="warning"<{elseif $asm[$row.emp_id].assessment>='1'}>class="success"<{elseif $asm[$row.emp_id].assessment=='0'}>class="error"<{else}>class="info"<{/if}>>
                                                            <{if $smarty.foreach.loopname.first}>
                                                                <td class="ldr-td-text-left" rowspan="<{$form1.appr_list_all|@count}>" style="background-color:#fff;">
                                                                    <{foreach item=gl from=$asm name=loopgl}>
                                                                        <{if $smarty.foreach.loopgl.first}>
                                                                            <{$gl.guideline|escape}>
                                                                        <{/if}>
                                                                    <{/foreach}>
                                                                </td>
                                                            <{/if}>
                                                            <td class="ldr-td-text-left"><{$row.emp_name|escape}></td>
                                                            <td class="ldr-td-text-center">

                                                                <{if is_null($asm[$row.emp_id].assessment)}>
                                                                    ?
                                                                <{else}>
                                                                    <{foreach item=sel from=$ldr.config.select}>
                                                                        <{if $asm[$row.emp_id].assessment|escape == $sel.value}>
                                                                            <{$sel.button}>
                                                                        <{/if}>
                                                                    <{/foreach}>
                                                                <{/if}>
                                                            </td>
                                                            <{if $ldr.config.reason === '1'}><td class="ldr-td-text-left"><{$asm[$row.emp_id].reason|escape|nl2br}></td><{/if}>
                                                        </tr>
                                                    <{/foreach}>
                                                <{/foreach}>
                                            </tbody>
                                        </table>
                                    </div>
                                <{/foreach}>

                                <{* form73 tab����*}>
                                <div class="tab-pane <{if $active === 'sec3'}>active<{else}>fade<{/if}>" id="sec3">
                                    <div class="control-group">
                                        <div class="control-group">
                                            <table class="table table-striped table-hover table-bordered table-condensed ldr-table">
                                                <thead>
                                                    <tr>
                                                        <th>��̾</th>
                                                        <th width="70px">�����٥�</th>
                                                        <th>���ɥХ���</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <{foreach item=row from=$form1.appr_list name=loopname}>
                                                        <tr>
                                                            <td class="ldr-td-text-left"><{$row.emp_name}></td>
                                                            <td class="ldr-td-text-center">
                                                                <{if $form7[$row.emp_id].level=='1'}>
                                                                    ǧ��
                                                                <{elseif $form7[$row.emp_id].level=='2'}>
                                                                    ��α
                                                                <{/if}>
                                                            </td>
                                                            <td class="ldr-td-text-left"><{$form7[$row.emp_id].advice|escape|nl2br}></td>
                                                        </tr>
                                                    <{/foreach}>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="btn-toolbar">
                            <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> ���</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {
                $('#tabs a').click(function(e) {
                    $('#active').val($(this).data("sec"));
                });
            });
        </script>
    </body>
</html>