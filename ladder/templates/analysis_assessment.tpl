<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 自己評価分析</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='自己評価分析' hd_sub_title=''}>

            <div class="container-fluid">
                <div class="row-fluid">
                    <form method="POST">
                        <div class="control-group form-inline">
                            <select id="pulldown_year" name="year" class="input-medium js-pulldown-novice">
                                <{foreach item=asyear from=$year_list}>
                                    <option value="<{$asyear|escape}>" <{if $year == $asyear}>selected<{/if}>><{$asyear|escape}>年度</option>
                                <{/foreach}>
                            </select>
                            <select id="ladder" name="ladder_id" class="input-large js-pulldown-change">
                                <{foreach from=$ladder->lists() item=ldr}>
                                    <option value="<{$ldr.ladder_id|escape}>" <{if $ldr.ladder_id === $this_ladder.ladder_id}>selected<{/if}>><{$ldr.title|escape}></option>
                                <{/foreach}>
                            </select>
                            <div class="input-prepend">
                                <span class="add-on"><i class="icon-sitemap"></i> 部署</span>
                                <{if count($class_lists) > 1}>
                                    <select name="srh_class3" class="input-large" id="class3">
                                        <{if $full}><option value="">すべて</option><{/if}>
                                        <{foreach item=row from=$class_lists}>
                                            <option class="js-class-select select<{$row.class_id|escape}>_<{$row.atrb_id|escape}>" value="<{$row.dept_id|escape}><{if $row.room_id}>_<{$row.room_id|escape}><{/if}>">
                                                <{$row.dept_nm|escape}><{if $row.room_id}> &gt; <{$row.room_nm|escape}><{/if}>
                                            </option>
                                        <{/foreach}>
                                    </select>
                                <{else}>
                                    <input type="hidden" name="srh_class3" value="<{$class_lists[0].dept_id|escape}>"/><span class="uneditable-input"><{$class_lists[0].name|escape}></span>
                                <{/if}>
                            </div>
                            <button type="submit" name="mode" value="view" class="btn btn-small btn-primary">更新</button>
                            <button type="submit" name="mode" value="csv" class="btn btn-small btn-default pull-right"><i class="icon-cloud-download"></i> CSVエクスポート</button>
                        </div>
                    </form>
                    <hr/>

                    <{if !$full || $smarty.post.srh_class3}>
                        <table class="table table-condensed table-bordered ldr-table table-hover">
                            <thead>
                                <tr>
                                    <th rowspan="2">職員名</th>
                                    <th rowspan="2">部署</th>
                                    <th rowspan="2">役職</th>
                                        <{foreach from=$ladder->lists() item=ldr}>
                                        <th colspan="<{$ldr.config.level|@count}>"><{$ldr.title|escape}></th>
                                        <{/foreach}>
                                    <th rowspan="2">評価合計値</th>
                                    <th rowspan="2">入力状況</th>
                                    <th colspan="<{$this_ladder.config.level|@count}>">レベルごとの合計値</th>
                                </tr>
                                <tr>
                                    <{foreach from=$ladder->lists() item=ldr}>
                                        <{foreach from=$ldr.config.level item=lv}>
                                            <th><{$roman_num[$lv]|escape}></th>

                                        <{/foreach}>
                                    <{/foreach}>
                                    <{foreach from=$this_ladder.config.level item=lv}>
                                        <th><{$roman_num[$lv]|escape}></th>

                                    <{/foreach}>
                                </tr>
                            </thead>
                            <tbody>
                                <{foreach name=table item=row from=$lists}>
                                    <tr>
                                        <td class="ldr-td-name"><{$row.emp_name|escape}></td>
                                        <td class="ldr-td-name"><{$row.class_full_name|escape}></td>
                                        <td class="ldr-td-name"><{$row.st_nm|escape}></td>
                                        <{foreach from=$ladder->lists() item=ldr}>
                                            <{foreach from=$ldr.config.level item=lv}>
                                                <td class="ldr-td-level"><{if $row.level[$ldr.ladder_id].$lv.date}><{$roman_num[$lv]|escape}><{/if}></td>

                                            <{/foreach}>
                                        <{/foreach}>
                                        <td class="ldr-td-number bar"><{$row.assessment.sum|default:0|escape}> / <{$row.guideline_count|escape}><span class="barX" style="width:<{$row.sum_percent|escape}>%;"></span></td>
                                        <td class="ldr-td-number bar"><{$row.percent|escape}>%<span class="barX" style="width:<{$row.percent|escape}>%;"></span></td>

                                        <{foreach from=$this_ladder.config.level item=lv}>
                                            <td class="ldr-td-number bar"><{$row.assessment.level[$lv].sum|default:0|escape}> / <{$row.level_count[$lv]|escape}><span class="barX" style="width:<{$row.assessment.level[$lv].sum/$row.level_count[$lv]*100}>%;"></span> </td>

                                        <{/foreach}>
                                    </tr>
                                <{/foreach}>
                            </tbody>
                        </table>
                    <{/if}>
                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {

            });
        </script>
    </body>
</html>


