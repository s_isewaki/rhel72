<table class="table table-striped table-condensed table-bordered">
    <thead>
        <tr>
            <th rowspan="2">グループ1</th>
            <th rowspan="2">グループ2</th>

            <{foreach item=d from=$date}>
                <th colspan="4">
                    第<{$d.number|escape}>回<br/><{$d.last_date|date_format_jp:"%-m月%-d日(%a)"}>
                </th>
            <{/foreach}>
        </tr>
        <tr>
            <{foreach item=d from=$date}>
                <th class="ldr-td-number"><{$nlv[1]}></th>
                <th class="ldr-td-number"><{$nlv[2]}></th>
                <th class="ldr-td-number"><{$nlv[3]}></th>
                <th class="ldr-td-number"><{$nlv[4]}></th>

            <{/foreach}>
        </tr>
    </thead>
    <tbody>
        <{foreach item=g1 from=$group1}>
            <{foreach name=g2 item=g2 from=$group2[$g1.group1]}>

                <tr>
                    <{if $smarty.foreach.g2.first}>
                        <td rowspan="<{$g1.g2count}>"><{$g1.name|escape}></td>
                    <{/if}>
                    <td><{$g2.name|escape}></td>
                    <{foreach item=d from=$date}>
                        <td class="ldr-td-number"><{$data[$d.number][$g2.group2].1|default:0}></td>
                        <td class="ldr-td-number"><{$data[$d.number][$g2.group2].2|default:0}></td>
                        <td class="ldr-td-number"><{$data[$d.number][$g2.group2].3|default:0}></td>
                        <td class="ldr-td-number"><{$data[$d.number][$g2.group2].4|default:0}></td>

                    <{/foreach}>
                </tr>
            <{/foreach}>
        <{/foreach}>
    </tbody>
</table>
