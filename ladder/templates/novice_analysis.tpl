<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | <{$titles.novice|escape}>: 評価集計</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title="`$titles.novice`: 評価集計" hd_sub_title=''}>

            <div class="container-fluid">
                <div class="row-fluid">
                    <form action="" method="post" id="form_pulldown_year">
                        <select id="pulldown_year" name="year" class="input-medium js-pulldown-year">
                            <{foreach item=year from=$year_list}>
                                <option value="<{$year|escape}>"><{$year|escape}>年度</option>
                            <{/foreach}>
                        </select>

                        <div class="btn-group pull-right">
                            <a class="btn btn-small dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="icon-file-alt"></i> Excel
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <{foreach item=d from=$date_list}>
                                    <li>
                                        <a tabindex="-1" href="#" data-number="<{$d.number|escape}>" data-php="novice_analysis.php" class="js-download-excel">第<{$d.number|escape}>回</a>
                                    </li>
                                <{/foreach}>
                            </ul>
                        </div> &nbsp;
                    </form>
                    <hr/>
                    <{foreach item=g1 from=$group1_list}>
                        <div>
                            <div class="alert alert-info">
                                <{$g1.name|escape}>
                                <div class=" pull-right">
                                    <button class="btn btn-mini btn-primary js-change-ass active">本人評価</button>
                                    <button class="btn btn-mini btn-info js-change-fass">他者評価</button>
                                    <button class="btn btn-mini btn-info js-change-diff">差分</button>
                                </div>
                            </div>
                            <table class="table table-bordered table-hover table-condensed">
                                <thead>
                                    <tr>
                                        <td rowspan="2" style="width:100px;"></td>
                                        <td rowspan="2"></td>
                                        <{foreach item=d from=$date_list}>
                                            <td colspan="4" style="text-align: center;">
                                                第<{$d.number|escape}>回<br/><{$d.last_date|date_format_jp:"%Y年%-m月"}>
                                            </td>
                                        <{/foreach}>
                                    </tr>
                                    <tr>
                                        <{foreach item=d from=$date_list}>
                                            <td class="ldr-td-level" style="width:20px;"><{$nlv[1]}></td>
                                            <td class="ldr-td-level" style="width:20px;"><{$nlv[2]}></td>
                                            <td class="ldr-td-level" style="width:20px;"><{$nlv[3]}></td>
                                            <td class="ldr-td-level" style="width:20px;"><{$nlv[4]}></td>
                                        <{/foreach}>
                                    </tr>
                                </thead>
                                <tbody>
                                    <{foreach name=group2 item=g2 from=$group2_list[$g1.group1]}>
                                        <{foreach name=ana item=data from=$analysis[$g1.group1][$g2.group2]}>
                                            <tr>
                                                <{if $smarty.foreach.ana.first}>
                                                    <td rowspan="<{$g2.count|escape}>"><{$g2.name|escape}></td>
                                                <{/if}>
                                                <td><{$data.guideline|escape}></td>
                                                <{foreach item=d from=$date_list}>
                                                    <td nowrap class="ldr-td-number">
                                                        <span class="js-novice-analysis-ass">
                                                            <{if $data[$d.number].a1 > 0}>
                                                                <{$data[$d.number].a1|string_format:"%d"}>
                                                            <{else}>
                                                                <span style="color:silver;">0</span>
                                                            <{/if}>
                                                        </span>
                                                        <span class="js-novice-analysis-fass hide">
                                                            <{if $data[$d.number].f1 > 0}>
                                                                <{$data[$d.number].f1|string_format:"%d"}>
                                                            <{else}>
                                                                <span style="color:silver;">0</span>
                                                            <{/if}>
                                                        </span>
                                                        <span class="js-novice-analysis-diff hide">
                                                            <{if $data[$d.number].d1 > 0}>
                                                                <{$data[$d.number].d1|string_format:"%+d"}>
                                                            <{elseif $data[$d.number].d1 < 0}>
                                                                <span style="color:red;"><{$data[$d.number].d1|string_format:"%+d"}></span>
                                                            <{else}>
                                                                <span style="color:silver;">0</span>
                                                            <{/if}>
                                                        </span>
                                                    </td>
                                                    <td nowrap class="ldr-td-number">
                                                        <span class="js-novice-analysis-ass">
                                                            <{if $data[$d.number].a2 > 0}>
                                                                <{$data[$d.number].a2|string_format:"%d"}>
                                                            <{else}>
                                                                <span style="color:silver;">0</span>
                                                            <{/if}>
                                                        </span>
                                                        <span class="js-novice-analysis-fass hide">
                                                            <{if $data[$d.number].f2 > 0}>
                                                                <{$data[$d.number].f2|string_format:"%d"}>
                                                            <{else}>
                                                                <span style="color:silver;">0</span>
                                                            <{/if}>
                                                        </span>
                                                        <span class="js-novice-analysis-diff hide">
                                                            <{if $data[$d.number].d2 > 0}>
                                                                <{$data[$d.number].d2|string_format:"%+d"}>
                                                            <{elseif $data[$d.number].d2 < 0}>
                                                                <span style="color:red;"><{$data[$d.number].d2|string_format:"%+d"}></span>
                                                            <{else}>
                                                                <span style="color:silver;">0</span>
                                                            <{/if}>
                                                        </span>
                                                    </td>
                                                    <td nowrap class="ldr-td-number">
                                                        <span class="js-novice-analysis-ass">
                                                            <{if $data[$d.number].a3 > 0}>
                                                                <{$data[$d.number].a3|string_format:"%d"}>
                                                            <{else}>
                                                                <span style="color:silver;">0</span>
                                                            <{/if}>
                                                        </span>
                                                        <span class="js-novice-analysis-fass hide">
                                                            <{if $data[$d.number].f3 > 0}>
                                                                <{$data[$d.number].f3|string_format:"%d"}>
                                                            <{else}>
                                                                <span style="color:silver;">0</span>
                                                            <{/if}>
                                                        </span>
                                                        <span class="js-novice-analysis-diff hide">
                                                            <{if $data[$d.number].d3 > 0}>
                                                                <{$data[$d.number].d3|string_format:"%+d"}>
                                                            <{elseif $data[$d.number].d3 < 0}>
                                                                <span style="color:red;"><{$data[$d.number].d3|string_format:"%+d"}></span>
                                                            <{else}>
                                                                <span style="color:silver;">0</span>
                                                            <{/if}>
                                                        </span>
                                                    </td>
                                                    <td nowrap class="ldr-td-number">
                                                        <span class="js-novice-analysis-ass">
                                                            <{if $data[$d.number].a4 > 0}>
                                                                <{$data[$d.number].a4|string_format:"%d"}>
                                                            <{else}>
                                                                <span style="color:silver;">0</span>
                                                            <{/if}>
                                                        </span>
                                                        <span class="js-novice-analysis-fass hide">
                                                            <{if $data[$d.number].f4 > 0}>
                                                                <{$data[$d.number].f4|string_format:"%d"}>
                                                            <{else}>
                                                                <span style="color:silver;">0</span>
                                                            <{/if}>
                                                        </span>
                                                        <span class="js-novice-analysis-diff hide">
                                                            <{if $data[$d.number].d4 > 0}>
                                                                <{$data[$d.number].d4|string_format:"%+d"}>
                                                            <{elseif $data[$d.number].d4 < 0}>
                                                                <span style="color:red;"><{$data[$d.number].d4|string_format:"%+d"}></span>
                                                            <{else}>
                                                                <span style="color:silver;">0</span>
                                                            <{/if}>
                                                        </span>
                                                    </td>
                                                <{/foreach}>
                                            </tr>
                                        <{/foreach}>
                                    <{/foreach}>
                                </tbody>
                            </table>
                            <hr/>
                        </div>
                    <{/foreach}>
                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {
                // プルダウン選択
                $('.js-pulldown-year').change(function() {
                    $('#form_pulldown_year').submit();
                });

                // 本人評価ボタン
                $('.js-change-ass').click(function() {
                    $('.js-novice-analysis-fass').hide();
                    $('.js-novice-analysis-diff').hide();
                    $('.js-novice-analysis-ass').show();
                    $('.js-change-ass').removeClass('btn-info').addClass('btn-primary').addClass('active');
                    $('.js-change-fass').removeClass('btn-primary').removeClass('active').addClass('btn-info');
                    $('.js-change-diff').removeClass('btn-primary').removeClass('active').addClass('btn-info');
                });

                // 他者評価ボタン
                $('.js-change-fass').click(function() {
                    $('.js-novice-analysis-ass').hide();
                    $('.js-novice-analysis-diff').hide();
                    $('.js-novice-analysis-fass').show();
                    $('.js-change-fass').removeClass('btn-info').addClass('btn-primary').addClass('active');
                    $('.js-change-ass').removeClass('btn-primary').removeClass('active').addClass('btn-info');
                    $('.js-change-diff').removeClass('btn-primary').removeClass('active').addClass('btn-info');
                });

                // 差分ボタン
                $('.js-change-diff').click(function() {
                    $('.js-novice-analysis-ass').hide();
                    $('.js-novice-analysis-fass').hide();
                    $('.js-novice-analysis-diff').show();
                    $('.js-change-diff').removeClass('btn-info').addClass('btn-primary').addClass('active');
                    $('.js-change-ass').removeClass('btn-primary').removeClass('active').addClass('btn-info');
                    $('.js-change-fass').removeClass('btn-primary').removeClass('active').addClass('btn-info');
                });

                /**
                 * Excel
                 */
                $('.js-download-excel').click(function() {
                    $('<form action="" method="post"></form>')
                        .append($('<input></input>').attr({type: 'hidden', name: 'number', value: $(this).data('number')}))
                        .append($('<input></input>').attr({type: 'hidden', name: 'mode', value: 'excel'}))
                        .append($('<input></input>').attr({type: 'hidden', name: 'year', value: $(this).data('year')}))
                        .appendTo(document.body)
                        .submit()
                        .remove();
                });

            });
        </script>
    </body>
</html>
