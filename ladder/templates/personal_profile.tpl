<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix ����ꥢ��ȯ����� | �����ץ��ե�����</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- �ᥤ�󥨥ꥢ -->
        <div class="content">
            <{include file="__header.tpl" hd_title="�����ץ��ե�����" hd_sub_title=""}>

            <div class="container-fluid">
                <div class="control-group">
                    <button type="button" class="js-btn-back btn btn-small" data-back="staff.php"><i class="icon-circle-arrow-left"></i> ���</button>
                </div>

                <div class="well well-small">
                    <div class="control-group">
                        <label class="control-label"><strong>����̾</strong></label>
                        <div class="lead"><{$staff->emp_name()|escape}> (ID: <{$staff->emp_personal_id()|escape}>)</div>
                    </div>

                    <{if $updated_on}>
                        <div class="control-group">
                            <label class="control-label"><strong>��������</strong></label>
                            <div class="ldr-uneditable"><{$staff->updated_on()|date_format_jp:"%Yǯ%-m��%-d��(%a) %R"}></div>
                        </div>
                    <{/if}>


                    <{foreach from=$ladder->lists() item=ldr}>
                        <div class="control-group">
                            <label class="control-label"><strong><{$ldr.title|escape}></strong></label>
                            <div class="well well-small">
                                <table class="table table-striped table-condensed">
                                    <thead>
                                        <tr>
                                            <th>��٥�</th>
                                            <th>ǧ���ֹ�</th>
                                            <th>ǧ����</th>
                                            <th>�������Ż���</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <{foreach from=$ldr.config.level item=level}>
                                            <tr>
                                                <td class="ldr-level"><{if $level}><{$roman_num[$level]}><{else}><{$ldr.config.level0}><{/if}></td>
                                                <td><{$staff->lv_number($ldr.ladder_id, $level)|escape}></td>
                                                <td><{$staff->lv_date($ldr.ladder_id, $level)|escape}></td>
                                                <td><{$staff->lv_hospital($ldr.ladder_id, $level)|escape}></td>
                                            </tr>
                                        <{/foreach}>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    <{/foreach}>

                    <div class="control-group">
                        <label class="control-label" for=""><strong>´��ǯ (�пȹ�)</strong></label>
                        <div class="controls">
                            <div class="input-append">
                                <div class="ldr-uneditable"><{$staff->graduation_year()|escape|default:'(̤����)'}></div>
                                <span class="add-on">ǯ</span>
                            </div>
                            <div class="input-prepend">
                                <span class="add-on"><i class="icon-building"></i></span>
                                <div class="ldr-uneditable input-xlarge"><{$staff->school()|escape|default:'(̤����)'}></div>
                            </div>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for=""><strong>�Ǹ���ȵ�����ǯ��</strong></label>
                        <div class="controls">
                            <div class="ldr-uneditable"><{$staff->license_date()|date_format_jp:"%Yǯ%-m��"|default:'(̤����)'}></div>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for=""><strong>�׾�����ǯ��</strong></label>
                        <div class="controls">
                            <div class="ldr-uneditable"><{$staff->nurse_date()|date_format_jp:"%Yǯ%-m��"|default:'(̤����)'}></div>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for=""><strong>���� ��°ǯ��</strong></label>
                        <div class="controls">
                            <div class="ldr-uneditable"><{$staff->hospital_date()|date_format_jp:"%Yǯ%-m��"|default:'(̤����)'}></div>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for=""><strong>���� ��°ǯ��</strong></label>
                        <div class="controls">
                            <div class="ldr-uneditable"><{$staff->ward_date()|date_format_jp:"%Yǯ%-m��"|default:'(̤����)'}></div>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="jna_id"><strong>JNA����ֹ� <small>(����8��)</small></strong></label>
                        <div class="controls">
                            <div class="input-prepend">
                                <span class="add-on"><i class="icon-credit-card"></i></span>
                                <div class="ldr-uneditable input-large"><{$staff->jna_id()|escape|default:'(̤����)'}></div>
                            </div>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="pref_na_id"><strong>��ƻ�ܸ��Ǹ�� ����ֹ� <small>(����6��)</small></strong></label>
                        <div class="controls">
                            <div class="input-prepend">
                                <span class="add-on"><i class="icon-credit-card"></i></span>
                                <div class="ldr-uneditable input-large"><{$staff->pref_na_id()|escape|default:'(̤����)'}></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
    </body>
</html>