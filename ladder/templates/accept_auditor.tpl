<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 監査者設定</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='監査者設定' hd_sub_title=''}>

            <div class="container-fluid">
                <div class="row-fluid">
                    <form id="apply_form" method="post">
                        <input type="hidden" name="mode" value="" />
                        <input type="hidden" name="menu" value="accept" />
                        <input type="hidden" name="id" value="<{$id}>" />
                        <input type="hidden" name="owner_url" value="accept_auditor.php" />
                        <input type="hidden" name="target_emp_id" value="<{$target_emp->emp_id()}>" />

                        <div class="btn-toolbar">
                            <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                        </div>

                        <div class="block">
                            <div class="block-body collapse in">
                                <{include file="__form_btn.tpl"}>
                            </div>
                        </div>

                        <div class="well well-small" id="form_commission">
                            <{if !$comm_list}>
                                <div class="alert alert-error"><{$titles.committee|default:'認定委員会'|escape}>が設定されていません。管理者にお問い合わせください。</div>
                            <{else}>

                                <div class="form-group">
                                    <label>監査者を選択してください。</label>
                                    <table class="table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>メンバー</th>
                                                <th><span class="badge badge-important required">必須</span><br/>監査者</th>
                                                <th>所属</th>
                                                <th>役職</th>
                                            </tr>
                                        </thead>
                                        <tbody id="comm_list">
                                            <{foreach item=row from=$comm_list}>
                                                <tr>
                                                    <td><{$row->emp_name()|escape}><input type="hidden" name="emp_id[]" value="<{$row->emp_id()|escape}>" /></td>
                                                    <td><label class="checkbox"><input type="checkbox" class="ck-auditor" name="auditor[]" value="<{$row->emp_id()|escape}>"
                                                                                       <{if $row->emp_id()|in_array:$auditor}> checked<{/if}>>
                                                        </label></td>
                                                    <td><{$row->class_full_name()|escape}></td>
                                                    <td><{$row->st_name()|escape}></td>
                                                </tr>
                                            <{/foreach}>
                                        </tbody>
                                    </table>
                                    <{foreach from=$error.auditor item=msg}>
                                        <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                    <{/foreach}>
                                    <{foreach from=$error.configured item=msg}>
                                        <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                    <{/foreach}>
                                </div>
                                <div class="btn-toolbar">
                                    <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>

                                    <{if !$error.configured}>
                                        <button type="submit" name="form_submit" class="btn btn-small btn-primary js-submit-setting" value="save"><i class="icon-edit"></i> 設定する</button>
                                    <{/if}>
                                </div>
                            <{/if}>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
    </body>
</html>