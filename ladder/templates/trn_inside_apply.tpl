<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 院内研修 講座</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='院内研修 講座' hd_sub_title=''}>

            <div class="container-fluid">
                <div class="container-fluid">
                    <div class="btn-toolbar">
                        <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                    </div>

                    <div class="well well-small">
                        <div class="row-fluid">
                            <strong class="ldr-trn-title"><{if $no}>(<{$no|escape}>) <{/if}><{$title|escape}></strong>
                        </div>

                        <{if $theme}>
                            <div class="row-fluid ldr-trn-row">
                                <div class="span12 ldr-trn-block">
                                    <label class="control-label">
                                        <strong>テーマ</strong>
                                    </label>
                                    <div class="ldr-uneditable input-large-modal"><{$theme|escape|nl2br|default:'(未設定)'}></div>
                                </div>
                            </div>
                        <{/if}>

                        <div class="row-fluid ldr-trn-row">
                            <div class="span5 ldr-trn-block">
                                <label class="control-label">
                                    <{if $hospital_type === '1'}>
                                        <strong>めざすレベル</strong>
                                    <{else}>
                                        <strong>対象レベル</strong>
                                    <{/if}>
                                </label>
                                <{if $level}>
                                    <span class="ldr-uneditable input-large-modal ldr-level"><{$roman_num[$level]|escape}></span>
                                <{else}>
                                    <span class="ldr-uneditable input-large-modal">なし</span>
                                <{/if}>
                            </div>
                            <div class="span5 ldr-trn-block">
                                <label class="control-label">
                                    <strong>カテゴリ</strong>
                                </label>
                                <span class="ldr-uneditable input-large-modal"><{$gp1.name|escape}></span>
                            </div>
                            <div class="span2 ldr-trn-block">
                                <label class="control-label">
                                    <strong>外部参加</strong>
                                </label>
                                <span class="ldr-uneditable input-large-modal">
                                    <{if $public==='t'}>
                                        可
                                    <{else}>
                                        不可
                                    <{/if}>
                                </span>
                            </div>
                        </div>

                        <{if $guideline || $guideline_text}>
                            <div class="row-fluid ldr-trn-row">
                                <div class="span12 ldr-trn-block">
                                    <label class="control-label"><strong>めざす状態</strong></label>
                                    <div class="controls">
                                        <div class="ldr-glbox-modal">
                                            <{foreach from=$guideline key=id item=name}>
                                                <div class="alert alert-info"><{$name|escape}></div>
                                            <{/foreach}>

                                            <{if $guideline_text}>
                                                <div class="alert alert-info"><{$guideline_text|escape|nl2br}></div>
                                            <{/if}>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <{/if}>

                        <div class="row-fluid ldr-trn-row">
                            <div class="span12 ldr-trn-block">
                                <label class="control-label">
                                    <strong>実施上の留意点</strong>
                                </label>
                                <div class="ldr-uneditable input-large-modal"><{$notes|escape|nl2br|default:'(未設定)'}></div>
                            </div>
                        </div>

                        <div class="row-fluid ldr-trn-row">
                            <div class="span6 ldr-trn-block">
                                <label class="control-label">
                                    <strong>企画担当</strong>
                                </label>
                                <div class="ldr-uneditable input-large-modal">
                                    <{if $planner}>
                                        <div><{$planner|escape|default:'(未設定)'}></div>
                                    <{/if}>
                                    <{foreach from=$emp_planner key=id item=name}>
                                        <div class="label label-info"><{$name|escape}></div>
                                    <{/foreach}>
                                </div>
                            </div>
                            <div class="span6 ldr-trn-block">
                                <label class="control-label">
                                    <strong>講師</strong>
                                </label>
                                <div class="ldr-uneditable input-large-modal">
                                    <{if $coach}>
                                        <div><{$coach|escape|default:'(未設定)'}></div>
                                    <{/if}>
                                    <{foreach from=$emp_coach key=id item=name}>
                                        <div class="label label-info"><{$name|escape}></div>
                                    <{/foreach}>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row-fluid">
                        <form method="post" id="inside_form">
                            <input type="hidden" name="id" value="" />
                            <input type="hidden" name="approve" value="<{$approve}>" />

                            <{if $dates}>
                                <{include file="__pager.tpl"}>
                                <table class="table table-bordered table-hover table-condensed ldr-table">
                                    <thead>
                                        <tr>
                                            <th nowrap>研修回</th>
                                            <th>研修目標</th>
                                            <th>研修内容</th>
                                            <th><i class="icon-ok-circle" /></th>
                                            <th nowrap>ステータス</th>
                                            <th>開催日時</th>
                                            <th>受付期間</th>
                                            <th nowrap>申請人数<br>(定員)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <{foreach item=row from=$dates}>
                                            <{if $row.reception==='0'}>
                                                <{assign var='lbl' value='label-info'}>
                                            <{elseif $row.reception < '0'}>
                                                <{assign var='lbl' value='label-warning'}>
                                            <{else}>
                                                <{assign var='lbl' value='label-important'}>
                                            <{/if}>
                                            <tr>
                                                <{if $row.number !== $back}>
                                                    <td class="ldr-td-name" rowspan="<{$cnt[$row.number]}>" style="background-color:#fff">

                                                        <a
                                                            href="trn_inside_number_modal.php?id=<{$row.inside_id|escape}>&number=<{$row.number|escape}>"
                                                            data-toggle="modal"
                                                            data-backdrop="true"
                                                            data-target="#inside_number"
                                                            class="btn btn-small btn-success">
                                                            <i class="icon-external-link"></i> 第<{$row.number|escape}>回
                                                        </a>
                                                    </td>
                                                    <td class="ldr-td-text-left" rowspan="<{$cnt[$row.number]}>" style="background-color:#fff;"><{$row.objective|escape}></td>
                                                    <td class="ldr-td-text-left" rowspan="<{$cnt[$row.number]}>" style="background-color:#fff;"><{$row.contents|escape}></td>
                                                <{/if}>
                                                <td class="ldr-td-text-center">
                                                    <{if $apply[$row.number] > '0'}>
                                                        <{if $row.applied > '0'}>
                                                            <span class="label label-warning">申請済</span>
                                                        <{/if}>
                                                    <{else}>
                                                        <{if $row.reception === '0'}>
                                                            <input type="radio" name="num_ary[<{$row.number}>]" value="<{$row.inside_date_id}>"/>
                                                        <{/if}>
                                                    <{/if}>
                                                </td>
                                                <td class="ldr-td-name"><span class="label <{$lbl|escape}>"><{$reception_text[$row.reception]|escape}></span></td>
                                                <td class="ldr-td-date">
                                                    <{if $row.type === "2"}>
                                                        <span class="label label-success"><{$ojt_name|escape}></span>
                                                    <{elseif $row.reception === "-2"}>
                                                        <{$row.training_date|date_format:"%Y年%-m月頃"}>
                                                    <{else}>
                                                        <span class="ldr-td-date-bold" title="<{$row.training_date|date_format_jp:"%Y年%-m月%-d日(%a)"}>"><{$row.training_date|date_format_jp:"%-m月%-d日(%a)"}></span><br /><{$row.training_start_time|date_format:"%R"}>〜<{$row.training_last_time|date_format:"%R"}>
                                                    <{/if}>
                                                </td>
                                                <td class="ldr-td-date">
                                                    <{if $row.reception === "-2"}>
                                                        <{$row.accept_date|date_format:"%Y年%-m月頃"}>
                                                    <{else}>
                                                        <{$row.accept_date|date_format_jp:"%-m月%-d日(%a) %R"}>〜<{$row.deadline_date|date_format_jp:"%-m月%-d日(%a) %R"}>
                                                    <{/if}>
                                                </td>
                                                <td class="ldr-td-name">
                                                    <{if $row.reception === "-2"}>
                                                    <{else}>
                                                        <{$row.count|escape}>名<br/>
                                                        <{if $row.type !== "2"}>
                                                            (<{$row.maximum|escape}>名)
                                                        <{/if}>
                                                    <{/if}>
                                                </td>
                                            </tr>
                                            <{assign var="back" value=$row.number}>
                                        <{/foreach}>
                                    </tbody>
                                </table>
                                <{foreach from=$error.number item=msg}>
                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                <{/foreach}>
                            <{else}>
                                <div class="alert alert-error">日程が登録されていません</div>
                            <{/if}>
                            <div class="control-group">
                                <label class="control-label"><strong>承認者</strong></label>
                                <{if $need_approve === 't'}>
                                    <{if isset($approver)}>
                                        <div class="controls"><span class="ldr-uneditable input-large"><{$approver->emp_name()|escape}></span></div>
                                        <{else}>
                                        <div class="alert alert-error">承認者が設定されていません。管理者にお問い合わせください。</div>
                                    <{/if}>
                                <{else}>
                                    <div class="controls">
                                        <span class="ldr-uneditable input-large">承認不要</span>
                                    </div>
                                <{/if}>
                            </div>

                            <div class="btn-toolbar">
                                <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                                <{if $enable_apply and ((isset($approver) and $need_approve === 't') or $need_approve === 'f')}>
                                    <button type="submit" name="btn_save" id="btn_save" class="js-submit-apply btn btn-small btn-primary" value="save"><i class="icon-save"></i> 申請する</button>
                                <{/if}>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <{include file="__modal_view.tpl" modal_id="inside_number" modal_title="研修内容"}>

        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {

                $('#btn-dt').click(function() {
                    $(this).find("i").toggleClass('icon-minus');
                    $(this).find("i").toggleClass('icon-plus');
                    $("#details").toggle(500);
                });
            });
        </script>
    </body>
</html>


