<div class="sidebar-nav">
    <a href="dashboard.php" class="nav-header"><i class="icon-fixed-width icon-dashboard"></i> ���å���ܡ���</a>
    <hr style="border:1px solid #999999; margin:0;" />

    <{if $function.assessment}>
        <a href="assessment.php" class="nav-header">
            <i class="icon-fixed-width icon-check"></i> <{$titles.assessment|default:'����ɾ��'|escape}>
        </a>
    <{/if}>

    <{if $function.novice && ($emp->is_novice_tab() || $emp->is_admin())}>
        <a href="#novice-menu" class="nav-header" data-toggle="collapse"><i class="icon-fixed-width icon-heart"></i> <{$titles.novice|default:'����ɾ��'|escape}></a>
        <ul id="novice-menu" class="nav nav-list collapse <{$novice_menu}>">
            <{if $emp->is_novice()}><li class="<{$novice_assessment_li}>"><a href="novice_assessment.php">�ܿ�ɾ��</a></li><{/if}>
            <{if $emp->is_facilitator()}><li class="<{$novice_facilitator_li}>"><a href="novice_facilitator.php">¾��ɾ��</a></li><{/if}>
            <{if $emp->is_facilitator() || $emp->is_admin()}><li class="<{$novice_analysis_li}>"><a href="novice_analysis.php">ɾ������</a></li><{/if}>
            <{if $emp->is_facilitator() || $emp->is_admin()}><li class="<{$novice_analysis_assessment_li}>"><a href="novice_analysis_assessment.php">ɾ��ʬ��</a></li><{/if}>
        </ul>
    <{/if}>

    <{if $function.record}>
        <a href="#record-menu" class="nav-header" data-toggle="collapse"><i class="icon-fixed-width icon-pencil"></i> <{$titles.record|default:'��Ͽ'|escape}></a>
        <ul id="record-menu" class="nav nav-list collapse <{$record_menu|escape}>">
            <{foreach from=$recordset key=i item=rec}>
                <{if ($rec.php !== 'form_2.php' && $rec.php !== 'form_5.php') || $hospital_type !== '1'}>
                    <li class="<{if $rec.php === "`$page_li`.php"}>active<{/if}>">
                        <a href="<{$rec.php|escape}>"><{$rec.title|escape}><br><small><{$rec.detail|escape}></small></a>
                    </li>
                <{/if}>
            <{/foreach}>
        </ul>
    <{/if}>

    <{if $function.next_plan}>
        <a href="next_plan.php" class="nav-header"><i class="icon-fixed-width icon-pencil"></i> <{$titles.next_plan|default:'��ǯ�ٷײ�'|escape}></a>
    <{/if}>

    <{if $function.ladder}>
        <a href="#apply-menu" class="nav-header" data-toggle="collapse"><i class="icon-fixed-width icon-edit"></i> <{$titles.ladder|default:'�����'|escape}>����</a>
        <ul id="apply-menu" class="nav nav-list collapse <{$apply_menu}>">
            <li class="<{$apply_li}>"><a href="apply.php">��������</a></li>

            <{foreach from=$emp->ladders() item=row}>
                <{if $row.config.open === '1' && (!$row.config.start || $row.config.start <= $now) && (!$row.config.end || $row.config.end >= $now)}>
                    <li class="apply<{$row.ladder_id|escape}>">
                        <a href="apply_entry.php?ldr=<{$row.ladder_id|escape}>"><{$row.config.sidemenu|escape}>
                            <{if $row.config.sidesubmenu}>
                                <br><small><{$row.config.sidesubmenu|escape}></small>
                            <{/if}>
                        </a>
                    </li>
                <{/if}>
            <{/foreach}>
        </ul>

        <{if $emp->committee()}>
            <a href="#accept-menu" class="nav-header" data-toggle="collapse"><i class="icon-fixed-width icon-edit"></i> <{$titles.ladder|default:'�����'|escape}>����</a>
            <ul id="accept-menu" class="nav nav-list collapse <{$accept_menu}>">
                <li class="<{$accept_li}>"><a href="accept.php">���հ���</a></li>
                <li class="<{$ladder_progress_li}>"><a href="ladder_progress.php">��Ľ����</a></li>
                <li class="<{$accept_objection_li}>"><a href="accept_objection.php">�۵Ŀ���Ω�ư���</a></li>
            </ul>
        <{else}>
            <a href="accept.php" class="nav-header"><i class="icon-fixed-width icon-inbox"></i> <{$titles.ladder|default:'�����'|escape}>����</a>
        <{/if}>
    <{/if}>

    <{if $function.inside}>
        <a href="#inside-menu" class="nav-header" data-toggle="collapse"><i class="icon-fixed-width icon-hospital"></i> ���⸦��</a>
        <ul id="inside-menu" class="nav nav-list collapse <{$inside_menu}>">
            <li class="<{$inside_course_li}>"><a href="trn_inside_course.php">�����ֺ�</a></li>
            <li class="<{$inside_calendar_li}>"><a href="trn_inside_calendar.php">������������</a></li>
            <li class="<{$inside_li}>"><a href="trn_inside.php">�����Ѱ���</a></li>
        </ul>
    <{/if}>

    <{if $function.outside}>
        <a href="#outside-menu" class="nav-header" data-toggle="collapse"><i class="icon-fixed-width icon-plus-sign-alt"></i> ��������</a>
        <ul id="outside-menu" class="nav nav-list collapse <{$outside_menu}>">
            <li class="<{$outside_course_li}>"><a href="trn_outside_course.php">�����ֺ�</a></li>
            <li class="<{$outside_li}>"><a href="trn_outside.php">�����Ѱ���</a></li>
        </ul>
    <{/if}>

    <{if $function.relief}>
        <a href="#relief-menu" class="nav-header" data-toggle="collapse"><i class="icon-fixed-width icon-medkit"></i> �߸������</a>
        <ul id="relief-menu" class="nav nav-list collapse <{$relief_menu}>">
            <li class="<{$relief_course_li}>"><a href="trn_relief_course.php">�����ֺ�</a></li>
            <li class="<{$relief_li}>"><a href="trn_relief.php">�����Ѱ���</a></li>
        </ul>
    <{/if}>

    <{if $function.training && $emp->is_approver()}>
        <a href="#tapprove-menu" class="nav-header" data-toggle="collapse"><i class="icon-fixed-width icon-inbox"></i> ������ǧ</a>
        <ul id="tapprove-menu" class="nav nav-list collapse <{$tapprove_menu}>">

            <{if $function.inside}>
                <li class="<{$tapprove_inside_li}>">
                    <a href="trn_approve_inside.php">���⸦����ǧ</a>
                </li>
            <{/if}>
            <{if $function.outside}>
                <li class="<{$tapprove_outside_li}>">
                    <a href="trn_approve_outside.php">����������ǧ</a>
                </li>
            <{/if}>
            <{if $function.relief}>
                <li class="<{$tapprove_relief_li}>">
                    <a href="trn_approve_relief.php">�߸��������ǧ</a>
                </li>
            <{/if}>
        </ul>
    <{/if}>

    <{if $function.training && $emp->is_trn_accept()}>
        <a href="#taccept-menu" class="nav-header" data-toggle="collapse"><i class="icon-fixed-width icon-inbox"></i> ��������</a>
        <ul id="taccept-menu" class="nav nav-list collapse <{$taccept_menu}>">
            <{if $function.inside && ($emp->is_inside() || $emp->is_inside_planner())}>
                <li class="<{$taccept_inside_li}>">
                    <a href="trn_accept_inside.php">���⸦������</a>
                </li>
            <{/if}>
            <{if $function.outside && $emp->is_outside()}>
                <li class="<{$taccept_outside_li}>">
                    <a href="trn_accept_outside.php">������������</a>
                </li>
            <{/if}>
            <{if $function.outside && $emp->is_relief()}>
                <li class="<{$taccept_relief_li}>">
                    <a href="trn_accept_relief.php">�߸����������</a>
                </li>
            <{/if}>
        </ul>
    <{/if}>

    <{if $emp->is_approver() || $emp->is_analysis() || $emp->committee() || $emp->is_admin()}>
        <a href="#staff-menu" class="nav-header" data-toggle="collapse"><i class="icon-fixed-width icon-user"></i> ��������</a>
        <ul id="staff-menu" class="nav nav-list collapse <{$staff_menu}>">
            <li class="<{$staff_li}>">
                <a href="staff.php">��������</a>
            </li>
            <li class="<{$staff_levelup_li}>">
                <a href="staff_levelup.php">��٥륢�å׾���</a>
            </li>
            <li class="<{$staff_certification_li}>">
                <a href="staff_certification.php">ǧ��԰���</a>
            </li>
        </ul>

        <a href="#analysis-menu" class="nav-header" data-toggle="collapse"><i class="icon-fixed-width icon-magic"></i> ����ʬ��</a>
        <ul id="analysis-menu" class="nav nav-list collapse <{$analysis_menu}>">
            <{if $function.assessment}>
                <li class="<{$analysis_assessment_li}>">
                    <a href="analysis_assessment.php">����ɾ��ʬ��</a>
                </li>
            <{/if}>
            <{if $function.ladder}>
                <li class="<{$analysis_ladder_li}>">
                    <a href="analysis_ladder.php">�����ʬ��</a>
                </li>
            <{/if}>
            <{if $emp->is_analysis_placement()}>
                <li class="<{$analysis_placement_li}>">
                    <a href="analysis_placement.php">����ɽ</a>
                </li>
            <{/if}>
        </ul>
    <{/if}>

    <{if $emp->is_admin() || $emp->is_inside() || $emp->is_outside() || $emp->is_relief() || $emp->is_approver()}>
        <hr style="border:1px solid #999999; margin:0;" />
    <{/if}>

    <{if $emp->is_admin()}>
        <a href="#admin-menu" class="nav-header" data-toggle="collapse"><i class="icon-fixed-width icon-cogs"></i> ��������</a>
        <ul id="admin-menu" class="nav nav-list collapse <{$admin_menu}>">
            <li class="<{$adm_ladder_li}>"><a href="adm_config.php">����</a></li>
            <li class="<{$adm_employee_li}>"><a href="adm_employee.php">����</a></li>
            <li class="<{$adm_permission_li}>"><a href="adm_permission.php">����</a></li>
            <li class="<{$adm_analysis_li}>"><a href="adm_analysis.php">ʬ��</a></li>
            <li class="<{$adm_log_li}>"><a href="adm_log.php">�����ƥ����</a></li>
        </ul>

        <a href="#ldradmin-menu" class="nav-header" data-toggle="collapse"><i class="icon-fixed-width icon-cog"></i> ���������</a>
        <ul id="ldradmin-menu" class="nav nav-list collapse <{$ldradmin_menu}>">
            <li class="<{$adm_ladder_li}>"><a href="adm_ladder.php">����</a></li>
            <li class="<{$adm_guideline_li}>"><a href="adm_guideline.php">��ư��ɸ</a></li>
        </ul>
    <{/if}>
    <{if $emp->is_admin() || $emp->is_inside() || $emp->is_outside() || $emp->is_relief()}>
        <a href="#trnadmin-menu" class="nav-header" data-toggle="collapse"><i class="icon-fixed-width icon-cog"></i> ��������</a>
        <ul id="trnadmin-menu" class="nav nav-list collapse <{$trnadmin_menu}>">
            <{if $emp->is_admin() || $emp->is_inside()}>
                <li class="<{$adm_inside_li}>">
                    <a href="adm_inside.php">���⸦��</a>
                </li>
            <{/if}>

            <{if $emp->is_admin() || $emp->is_outside()}>
                <li class="<{$adm_outside_li}>">
                    <a href="adm_outside.php">��������</a>
                </li>
            <{/if}>
            <{if $emp->is_admin() || $emp->is_relief()}>
                <li class="<{$adm_relief_li}>">
                    <a href="adm_relief.php">�߸������</a>
                </li>
            <{/if}>

            <{if $emp->is_admin() || $emp->is_inside()}>
                <li class="<{$check_template_li}>">
                    <a href="adm_check_template.php">�����֤�ƥ�ץ졼��</a>
                </li>
                <li class="<{$adm_training_li}>">
                    <a href="adm_training.php">����</a>
                </li>
            <{/if}>
        </ul>
    <{/if}>

    <{if $emp->is_admin() || $emp->is_approver()}>
        <a href="#novadmin-menu" class="nav-header" data-toggle="collapse"><i class="icon-fixed-width icon-cog"></i> ����ɾ������</a>
        <ul id="novadmin-menu" class="nav nav-list collapse <{$novadmin_menu}>">
            <{if $emp->is_admin()}>
                <li class="<{$adm_novice_guideline_li}>">
                    <a href="adm_novice_guideline.php">��ã��ɸ</a>
                </li>
            <{/if}>

            <li class="<{$adm_novice_li}>"><a href="adm_novice.php">��������</a></li>
            <li class="<{$adm_novice_facilitator_li}>"><a href="adm_novice_facilitator.php">����ô��������</a></li>

            <{if $emp->is_admin()}>
                <li class="<{$adm_novice_date_li}>">
                    <a href="adm_novice_date.php">ɾ������</a>
                </li>
                <li class="<{$adm_novice_set_li}>">
                    <a href="adm_novice_setting.php">����</a>
                </li>
            <{/if}>
        </ul>
    <{/if}>

    <{if $emp->is_admin() || $emp->committee()}>
        <a href="#item-menu" class="nav-header" data-toggle="collapse"><i class="icon-fixed-width icon-cog"></i> ̾�δ���</a>
        <ul id="item-menu" class="nav nav-list collapse <{$item_menu}>">
            <li class="<{$adm_item_set_li}>"><a href="adm_item_setting.php">̾������</a></li>
            <li class="<{$adm_item_mst_li}>"><a href="adm_item_mst.php">̾�Υޥ���</a></li>
        </ul>
    <{/if}>

    <hr style="border:1px solid #999999; margin:0;" />
    <a href="#" class="nav-header ldr_close" ><i class="icon-fixed-width icon-off"></i> �Ĥ���</a>

</div>
