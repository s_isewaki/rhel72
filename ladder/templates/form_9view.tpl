<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | <{$ldr.config.form_9.title|escape}></title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title=$ldr.config.form_9.title hd_sub_title=$ldr.config.form_9.detailL}>

            <div class="container-fluid">
                <div class="row-fluid">

                    <{*印刷ボタン*}>
                    <form id="form_print" method="post" class="pull-right">
                        <input type="hidden" name="mode" value="" />
                        <input type="hidden" name="menu" value="" />
                        <input type="hidden" name="id" value="" />
                        <input type="hidden" name="target_emp_id" value=""/>
                        <input type="hidden" name="owner_url" value="" />
                        <{if $is_auditor || $emp->committee()}>
                            &nbsp;<button type="button" data-id="<{$smarty.post.id}>" data-emp="<{$smarty.post.target_emp_id}>" data-php="form_9edit.php" data-mode="reedit" class="js-form-button btn btn-danger btn-small"><i class="icon-edit"></i> 編集する</button>
                        <{/if}>
                        &nbsp;<button type="button" data-id="<{$smarty.post.id}>" data-emp="<{$smarty.post.target_emp_id}>" data-php="form_9view.php" data-target="print" class="pull-right js-form-print btn btn-primary btn-small"><i class="icon-print"></i> 印刷する</button>
                    </form>

                    <{*様式共通ボタン*}>
                    <form id="form_edit" method="post">
                        <{include file='__form_view.tpl' op_srh='false' tmp_id='form_9view'}>
                    </form>

                    <div class="well well-small">
                        <div class="control-group">
                            <div class="input-prepend" style="margin-bottom:0;">
                                <span class="add-on">申請レベル</span>
                                <span class="ldr-uneditable input-mini ldr-level" style="margin-bottom:0;"><{$apply->level_text()|escape}></span>
                            </div>
                            <div class="input-prepend" style="margin-bottom:0;">
                                <span class="add-on">申請者</span>
                                <span class="ldr-uneditable input-large" style="margin-bottom:0;"><{$target_emp->emp_name()|escape}> (<{$target_emp->class_full_name()|escape}>)</span>
                            </div>
                        </div>
                        <hr/>

                        <div class="control-group">
                            <label class="control-label">
                                <strong>監査者</strong>
                            </label>
                            <div class="controls">
                                <{foreach from=$auditor_list item=aprll}>
                                    <span class="ldr-uneditable input-medium"><{$aprll|escape}></span>
                                <{/foreach}>
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label"><strong>申請レベル</strong></label>
                            <div class="controls">
                                <span class="ldr-uneditable input-mini ldr-level"><{$roman_num[$form1.level]|escape}></span>
                            </div>
                        </div>

                        <table class="table table-striped table-hover table-bordered table-condensed ldr-table">
                            <thead>
                                <tr>
                                    <th>領域</th>
                                    <th>基準</th>
                                    <th width="50px">評価</th>
                                    <th width="40%">備考</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="ldr-td-text-left" rowspan="2" style="background-color: #fff;"><{if $ldr.config.rc === '1'}>臨床看護実践<{else}>赤十字<{/if}></td>
                                    <td class="ldr-td-text-left">
                                        <{if $ldr.config.rc === '1'}>
                                            1) 評価は、指標の「知識」、「判断」、「行為」、「行為の結果」に則って実施されているか
                                        <{else}>
                                            1) 評価は、指標に則っているか
                                        <{/if}>
                                    </td>
                                    <td class="ldr-td-text-center">
                                        <{if $form9.clinical1==='t'}>
                                            <i class="icon-circle-blank"></i>
                                        <{else}>
                                            <i class="icon-remove"></i>
                                        <{/if}>
                                    </td>
                                    <td class="ldr-td-text-left"><{$form9.clinical1_note|escape|nl2br}></td>
                                </tr>
                                <tr>
                                    <td class="ldr-td-text-left">2) 評価内容に飛躍がないか</td>
                                    <td class="ldr-td-text-center">
                                        <{if $form9.clinical2==='t'}>
                                            <i class="icon-circle-blank"></i>
                                        <{else}>
                                            <i class="icon-remove"></i>
                                        <{/if}>
                                    </td>
                                    <td class="ldr-td-text-left"><{$form9.clinical2_note|escape|nl2br}></td>
                                </tr>
                                <tr>
                                    <td class="ldr-td-text-left" rowspan="2" style="background-color: #fff;"><{if $ldr.config.rc === '1'}>マネジメント<{else}>管理過程<{/if}></td>
                                    <td class="ldr-td-text-left">1) 評価は、指標に則っているか</td>
                                    <td class="ldr-td-text-center">
                                        <{if $form9.management1==='t'}>
                                            <i class="icon-circle-blank"></i>
                                        <{else}>
                                            <i class="icon-remove"></i>
                                        <{/if}>
                                    </td>
                                    <td class="ldr-td-text-left"><{$form9.management1_note|escape|nl2br}></td>
                                </tr>
                                <tr>
                                    <td class="ldr-td-text-left">2) 評価内容に飛躍がないか</td>
                                    <td class="ldr-td-text-center">
                                        <{if $form9.management2==='t'}>
                                            <i class="icon-circle-blank"></i>
                                        <{else}>
                                            <i class="icon-remove"></i>
                                        <{/if}>
                                    </td>
                                    <td class="ldr-td-text-left"><{$form9.management2_note|escape|nl2br}></td>
                                </tr>
                                <tr>
                                    <td class="ldr-td-text-left" rowspan="2" style="background-color: #fff;"><{if $ldr.config.rc === '1'}>教育・研究<{else}>意思決定<{/if}></td>
                                    <td class="ldr-td-text-left">1) 評価は、指標に則っているか</td>
                                    <td class="ldr-td-text-center">
                                        <{if $form9.education1==='t'}>
                                            <i class="icon-circle-blank"></i>
                                        <{else}>
                                            <i class="icon-remove"></i>
                                        <{/if}>
                                    </td>
                                    <td class="ldr-td-text-left"><{$form9.education1_note|escape|nl2br}></td>
                                </tr>
                                <tr>
                                    <td class="ldr-td-text-left">2) 評価内容に飛躍がないか</td>
                                    <td class="ldr-td-text-center">
                                        <{if $form9.education2==='t'}>
                                            <i class="icon-circle-blank"></i>
                                        <{else}>
                                            <i class="icon-remove"></i>
                                        <{/if}>
                                    </td>
                                    <td class="ldr-td-text-left"><{$form9.education2_note|escape|nl2br}></td>
                                </tr>
                                <tr>
                                    <td class="ldr-td-text-left" rowspan="2" style="background-color: #fff;"><{if $ldr.config.rc === '1'}>赤十字活動<{else}>質保証<{/if}></td>
                                    <td class="ldr-td-text-left">1) 評価は、指標に則っているか</td>
                                    <td class="ldr-td-text-center">
                                        <{if $form9.redcross1==='t'}>
                                            <i class="icon-circle-blank"></i>
                                        <{else}>
                                            <i class="icon-remove"></i>
                                        <{/if}>
                                    </td>
                                    <td class="ldr-td-text-left"><{$form9.redcross1_note|escape|nl2br}></td>
                                </tr>
                                <tr>
                                    <td class="ldr-td-text-left">2) 評価内容に飛躍がないか</td>
                                    <td class="ldr-td-text-center">
                                        <{if $form9.redcross2==='t'}>
                                            <i class="icon-circle-blank"></i>
                                        <{else}>
                                            <i class="icon-remove"></i>
                                        <{/if}>
                                    </td>
                                    <td class="ldr-td-text-left"><{$form9.redcross2_note|escape|nl2br}></td>
                                </tr>

                                <{if $ldr.config.rc === '2'}>
                                    <tr>
                                        <td class="ldr-td-text-left" rowspan="2" style="background-color: #fff;">人材育成</td>
                                        <td class="ldr-td-text-left">1) 評価は、指標に則っているか</td>
                                        <td class="ldr-td-text-center">
                                            <{if $form9.comment51==='t'}>
                                                <i class="icon-circle-blank"></i>
                                            <{else}>
                                                <i class="icon-remove"></i>
                                            <{/if}>
                                        </td>
                                        <td class="ldr-td-text-left"><{$form9.comment51_note|escape|nl2br}></td>
                                    </tr>
                                    <tr>
                                        <td class="ldr-td-text-left">2) 評価内容に飛躍がないか</td>
                                        <td class="ldr-td-text-center">
                                            <{if $form9.comment52==='t'}>
                                                <i class="icon-circle-blank"></i>
                                            <{else}>
                                                <i class="icon-remove"></i>
                                            <{/if}>
                                        </td>
                                        <td class="ldr-td-text-left"><{$form9.comment52_note|escape|nl2br}></td>
                                    </tr>
                                    <tr>
                                        <td class="ldr-td-text-left" rowspan="2" style="background-color: #fff;">対人関係</td>
                                        <td class="ldr-td-text-left">1) 評価は、指標に則っているか</td>
                                        <td class="ldr-td-text-center">
                                            <{if $form9.comment61==='t'}>
                                                <i class="icon-circle-blank"></i>
                                            <{else}>
                                                <i class="icon-remove"></i>
                                            <{/if}>
                                        </td>
                                        <td class="ldr-td-text-left"><{$form9.comment61_note|escape|nl2br}></td>
                                    </tr>
                                    <tr>
                                        <td class="ldr-td-text-left">2) 評価内容に飛躍がないか</td>
                                        <td class="ldr-td-text-center">
                                            <{if $form9.comment62==='t'}>
                                                <i class="icon-circle-blank"></i>
                                            <{else}>
                                                <i class="icon-remove"></i>
                                            <{/if}>
                                        </td>
                                        <td class="ldr-td-text-left"><{$form9.comment62_note|escape|nl2br}></td>
                                    </tr>
                                    <tr>
                                        <td class="ldr-td-text-left" rowspan="2" style="background-color: #fff;">セルフマネジメント</td>
                                        <td class="ldr-td-text-left">1) 評価は、指標に則っているか</td>
                                        <td class="ldr-td-text-center">
                                            <{if $form9.comment71==='t'}>
                                                <i class="icon-circle-blank"></i>
                                            <{else}>
                                                <i class="icon-remove"></i>
                                            <{/if}>
                                        </td>
                                        <td class="ldr-td-text-left"><{$form9.comment71_note|escape|nl2br}></td>
                                    </tr>
                                    <tr>
                                        <td class="ldr-td-text-left">2) 評価内容に飛躍がないか</td>
                                        <td class="ldr-td-text-center">
                                            <{if $form9.comment72==='t'}>
                                                <i class="icon-circle-blank"></i>
                                            <{else}>
                                                <i class="icon-remove"></i>
                                            <{/if}>
                                        </td>
                                        <td class="ldr-td-text-left"><{$form9.comment72_note|escape|nl2br}></td>
                                    </tr>
                                <{/if}>
                            </tbody>
                        </table>

                        <div class="control-group">
                            <label class="control-label">
                                <strong>認定委員会としての活動や検討事項</strong>
                            </label>
                            <div class="controls">
                                <span class="ldr-uneditable input-maxlarge"><{$form9.problem|escape|nl2br}></span>
                            </div>
                        </div>
                    </div>

                    <div class="btn-toolbar">
                        <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                    </div>
                </div>
            </div>
            <{include file="__js.tpl"}>
    </body>
</html>