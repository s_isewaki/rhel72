<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 院内研修マスタ | 研修内容 並べ替え</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='院内研修マスタ 研修内容' hd_sub_title=''}>

            <div class="container-fluid">
                <{include file="__save_message.tpl"}>
                <div class="row-fluid">
                    <fieldset>
                        <legend>
                            <a
                                href="trn_inside_contents_modal.php?id=<{$inside_id|escape}>"
                                data-toggle="modal"
                                data-backdrop="true"
                                data-target="#inside_contents">
                                <{if $no}>(<{$no|escape}>) <{/if}><{$title|escape}>
                            </a>
                        </legend>

                        <form method="post">
                            <input type="hidden" class="inside_id" name="id" value="<{$inside_id|escape}>" />

                            <table class="table table-bordered table-hover table-condensed ldr-table">
                                <thead>
                                    <tr>
                                        <th><i class="icon-sort"></i></th>
                                        <th>研修回</th>
                                        <th>研修のねらい</th>
                                        <th>研修目標</th>
                                        <th>研修内容</th>
                                        <th>研修方法</th>
                                        <th>振り返り</th>
                                        <th>プレビュー</th>
                                    </tr>
                                </thead>
                                <tbody class="sortable">
                                    <{foreach from=$contents item=row}>
                                        <tr>
                                            <td class="ldr-td-button dragHandle"><i class="icon-sort"></i><input type="hidden" name="number[]" value="<{$row.inside_contents_id|escape}>" /></td>
                                            <td class="ldr-td-name">第<{$row.number|escape|nl2br}>回</td>
                                            <td class="ldr-td-text-left"><{$row.aim|escape|nl2br}></td>
                                            <td class="ldr-td-text-left"><{$row.objective|escape|nl2br}></td>
                                            <td class="ldr-td-text-left"><{$row.contents|escape|nl2br}></td>
                                            <td class="ldr-td-text-left"><{$row.method|escape|nl2br}></td>
                                            <td class="ldr-td-name">
                                                <{if $row.looking_back === 't'}>
                                                    <button type="button" data-php="adm_inside_check.php" data-id="<{$row.inside_contents_id}>" class="js-form-button btn btn-small btn-danger">振り返り</button>
                                                <{else}>
                                                    -
                                                <{/if}>
                                            </td>
                                            <td class="ldr-td-name">
                                                <{if $row.looking_back === 't'}>
                                                    <button type="button" data-php="adm_inside_check_preview.php" data-id="<{$row.inside_contents_id}>" class="js-form-button btn btn-small btn-primary">プレビュー</button>
                                                <{else}>
                                                    -
                                                <{/if}>
                                            </td>
                                        </tr>
                                    <{/foreach}>
                                </tbody>
                            </table>
                            <div class="btn-toolbar">
                                <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                                <button type="submit" name="btn_save" value="btn_save" class="btn btn-small btn-primary js-table-sort"><i class="icon-ok-sign"></i> 順番を並べ替える</button>
                            </div>
                        </form>
                    </fieldset>
                </div>
            </div>
        </div>
        <{include file="__modal_view.tpl" modal_id="inside_contents" modal_title="研修内容"}>
        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {

                //順番変更
                $('.js-table-sort').click(function() {
                    if (!confirm('順番を変更してもよろしいですか？')) {
                        return false;
                    }
                    return true;
                });
            });
        </script>
    </body>
</html>
