<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | <{$titles.form_23.title|escape}>入力</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title="`$titles.form_23.title`入力" hd_sub_title=$titles.form_23.detailL}>

            <div class="container-fluid">
                <div class="well well-small">
                    <form id="form_edit" action="form_23edit.php" method="post">
                        <input type="hidden" name="inner_id" value="" />
                        <{if $not_found === true}>
                            <div class="alert alert-error">記録が見つかりませんでした</div>
                            <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                        <{else}>
                            <{if !$subject_lists}>
                                <div class="alert alert-error">救護員研修が設定されていません。管理者にお問い合わせください</div>
                                <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                            <{else}>
                                <div class="control-group">
                                    <label class="control-label" for="">
                                        <strong>年月日</strong>
                                        <span class="badge badge-important required">必須</span>
                                    </label>
                                    <div class="controls">
                                        <{include file="__datepicker.tpl" date_id="period" date_remove="off" date_name="period" date_option = 'required'}>
                                        <{foreach from=$error.period item=msg}>
                                            <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                        <{/foreach}>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="subject">
                                        <strong>科目</strong>
                                        <span class="badge badge-important required">必須</span>
                                    </label>
                                    <select name="subject" id="subject" class="input-large">
                                        <{foreach from=$subject_lists item=item}>
                                            <option value="<{$item.relief_id}>"><{$item.subject}></option>
                                        <{/foreach}>
                                    </select>
                                    <div class="controls">
                                        <{foreach from=$error.subject item=msg}>
                                            <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                        <{/foreach}>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="subject">
                                        <strong>教科内容を選択してください</strong>
                                        <span class="badge badge-important required">必須</span>
                                    </label>
                                    <table class="table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>単元</th>
                                                <th>教科内容</th>
                                                <th>詳細</th>
                                                <th>時間</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <{foreach item=row from=$contents_lists}>
                                                <tr class="tr-relief tr-relief-<{$row.relief_id|escape}>" style="display:none">
                                                    <td>
                                                        <label class="checkbox"><input type="radio" class="rd rd_<{$row.relief_id|escape}>" name="unit" value="<{$row.relief_id|escape}>_<{$row.unit|escape}>" /></label>
                                                    </td>
                                                    <td class="ldr-level"><{$roman_num[$row.unit]|escape}></td>
                                                    <td class="ldr-td-text-left"><{$row.contents|escape}></td>
                                                    <td class="ldr-td-text-left"><{$row.detail|escape|nl2br}></td>
                                                    <td class="ldr-td-text-left"><{$row.time|escape|nl2br}></td>
                                                </tr>
                                            <{/foreach}>
                                        </tbody>
                                    </table>
                                    <div class="controls">
                                        <{foreach from=$error.unit item=msg}>
                                            <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                        <{/foreach}>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label" for="organizer">
                                        <strong>主催</strong>
                                        <span class="badge badge-important required">必須</span>
                                    </label>
                                    <div class="controls">
                                        <input type="text" id="organizer" name="organizer" class="input-xlarge" maxlength="200" value="" required="required"/>
                                        <{foreach from=$error.organizer item=msg}>
                                            <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                        <{/foreach}>
                                    </div>
                                </div>
                                <div class="btn-toolbar">
                                    <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                                    <button type="submit" name="btn_save" id="btn_save" class="btn btn-small btn-primary" value="btn_save"><i class="icon-save"></i> 保存する</button>
                                </div>
                            <{/if}>

                        <{/if}>
                    </form>
                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {
                $("#subject").change(function() {
                    $(".tr-relief").hide();
                    $(".tr-relief-" + $(this).val()).show();
                    var value = $('input[name=unit]:checked').val();
                    if (value) {
                        var ary = value.split("_");
                    }
                    if (!value || ary[0] !== $(this).val()) {
                        $("input[name=unit]").attr("checked", false);
                        $("input[name=unit]").val([$(".rd_" + $(this).val()).eq(0).val()]);
                    }

                }).trigger('change');
            });
        </script>

    </body>
</html>