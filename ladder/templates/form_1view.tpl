<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | <{$ldr.config.headmenu|escape}></title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <!-- hd_title='様式１' -->
        <div class="content">
            <{include file="__header.tpl" hd_title=$ldr.config.headmenu hd_sub_title=$ldr.config.headsubmenu}>

            <div class="container-fluid">
                <div class="row-fluid">
                    <div class="control-group">
                        <{*印刷ボタン*}>
                        <form id="form_print" method="post" class="pull-right">
                            <button type="button" data-id="<{$smarty.post.id|escape}>" data-emp="<{$smarty.post.target_emp_id|escape}>" data-php="form_1view.php" data-target="form1" class="pull-right js-form-print btn btn-primary btn-small"><i class="icon-print"></i> 印刷する</button>
                        </form>

                        <{*様式共通ボタン*}>
                        <form class="form" id="edit_form" method="POST">
                            <{include file='__form_view.tpl' op_srh='false' tmp_id='form_1view'}>
                        </form>
                    </div>

                    <div class="well well-small">
                        <div class="control-group">
                            <div class="input-prepend" style="margin-bottom:0;">
                                <span class="add-on">申請レベル</span>
                                <span class="ldr-uneditable input-mini ldr-level" style="margin-bottom:0;"><{$apply->level_text()|escape}></span>
                            </div>
                            <div class="input-prepend" style="margin-bottom:0;">
                                <span class="add-on">申請者</span>
                                <span class="ldr-uneditable" style="margin-bottom:0;"><{$target_emp->emp_name()|escape}> (<{$target_emp->class_full_name()|escape}>)</span>
                            </div>
                        </div>
                        <hr/>

                        <{if $target_emp->level($apply->ladder_id()) !== ''}>
                            <div class="control-group">
                                <label class="control-label"><strong>既得しているレベル</strong></label>
                                <div class="well well-small">
                                    <dl class="dl-horizontal">
                                        <dt>レベル</dt>
                                        <dd><strong class="ldr-level"><{$target_emp->level_roman($apply->ladder_id())|default:$ldr.config.level0|escape}></strong></dd>
                                        <dt>認定日</dt>
                                        <dd><{$target_emp->lv_date($apply->ladder_id())|date_format_jp:"%Y年%-m月%-d日(%a)"}></dd>
                                        <dt>取得施設</dt>
                                        <dd><{$target_emp->lv_hospital($apply->ladder_id())|escape}></dd>
                                    </dl>
                                </div>
                            </div>
                        <{else}>
                            <div class="control-group">
                                <label class="control-label">
                                    <strong>既得しているレベル</strong>
                                </label>
                                <div class="controls">
                                    <span class="input-mini ldr-uneditable">なし</span>
                                </div>
                            </div>
                        <{/if}>
                        <div class="control-group">
                            <label class="control-label">
                                <strong>申請年月日</strong>
                            </label>
                            <div class="controls">
                                <span class="ldr-uneditable"><{$form1.apply_date|date_format_jp:"%Y年%-m月%-d日(%a)"}></span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                <strong>臨床経験年数</strong>
                            </label>
                            <div class="controls">
                                <div class="input-append">
                                    <span class="input-mini ldr-uneditable text-right"><{$form1.career|escape}></span>
                                    <span class="add-on">年目</span>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                <strong>当院での臨床経験年数</strong>
                            </label>
                            <div class="controls">
                                <div class="input-append">
                                    <span class="input-mini ldr-uneditable text-right"><{$form1.career_hospital|escape}></span>
                                    <span class="add-on">年目</span>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                <strong>当該病棟での臨床経験年数</strong>
                            </label>
                            <div class="controls">
                                <div class="input-append">
                                    <span class="input-mini ldr-uneditable text-right"><{$form1.career_ward|escape}></span>
                                    <span class="add-on">年目</span>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                <strong>当面のあなたの目標</strong>
                            </label>
                            <div class="controls">
                                <span class="input-maxlarge ldr-uneditable"><{$form1.objective|escape|nl2br}></span>
                            </div>
                        </div>
                        <{if $ldr.config.flow === '1' && $ldr.config.meeting === '1'}>
                            <div class="control-group">
                                <label class="control-label">
                                    <strong>評価会日時</strong>
                                </label>
                                <div class="controls">
                                    <span class="ldr-uneditable"><{$form1.meeting_date|date_format_jp:"%Y年%-m月%-d日(%a)"|default:'(未設定)'}></span>
                                    <span class="ldr-uneditable"><{$form1.meeting_start_time|default:'(未設定)'|escape}></span> 〜
                                    <span class="ldr-uneditable"><{$form1.meeting_last_time|default:'(未設定)'|escape}></span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    <strong>評価会会場</strong>
                                </label>
                                <div class="controls">
                                    <span class="input-xlarge ldr-uneditable"><{$form1.meeting_room|default:'(未設定)'|escape}></span>
                                </div>
                            </div>
                        <{/if}>
                        <{if $ldr.config.appr === '1'}>
                            <{if $ldr.config.flow === '1'}>
                                <div class="control-group">
                                    <label class="control-label">
                                        <strong>司会者</strong>
                                    </label>
                                    <div class="controls clearfix">
                                        <div id="facilitator" class="ldr-empbox" style="margin-top:3px !important;">
                                            <{foreach from=$form1.facilitator key=id item=name}>
                                                <div class="label label-info"><{$name|escape}></div>
                                            <{/foreach}>
                                        </div>
                                    </div>
                                </div>
                            <{/if}>
                            <div class="control-group">
                                <label class="control-label">
                                    <strong>評価者</strong>
                                </label>
                                <div class="controls clearfix">
                                    <div id="facilitator" class="ldr-empbox" style="margin-top:3px !important;">
                                        <{foreach from=$form1.appraiser key=id item=name}>
                                            <div class="label label-info"><{$name|escape}></div>
                                        <{/foreach}>
                                    </div>
                                </div>
                            </div>
                        <{/if}>
                    </div>
                    <div class="btn-toolbar">
                        <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                    </div>
                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
    </body>
</html>
