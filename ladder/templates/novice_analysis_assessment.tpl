<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix ����ꥢ��ȯ����� | ����ɾ��ʬ��</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- �ᥤ�󥨥ꥢ -->
        <div class="content">
            <{include file="__header.tpl" hd_title='����ɾ��ʬ��' hd_sub_title=''}>

            <div class="container-fluid">
                <div class="row-fluid">
                    <form method="POST">
                        <div class="control-group form-inline">
                            <select id="pulldown_year" name="year" class="input-medium js-pulldown-novice">
                                <{foreach item=asyear from=$year_list}>
                                    <option value="<{$asyear|escape}>" <{if $year == $asyear}>selected<{/if}>><{$asyear|escape}>ǯ��</option>
                                <{/foreach}>
                            </select>
                            <select id="pulldown_date" name="number" class="input-large js-pulldown-assessment">
                                <{foreach item=date from=$date_list}>
                                    <option value="<{$date.number|escape}>">��<{$date.number|escape}>��: <{$date.last_date|date_format_jp:"%Yǯ%-m��%-d��"}></option>
                                <{/foreach}>
                            </select>
                            <button type="submit" name="mode" value="view" class="btn btn-small btn-primary">����</button>
                            <button type="submit" name="mode" value="csv" class="btn btn-small btn-default pull-right"><i class="icon-cloud-download"></i> CSV�������ݡ���</button>
                        </div>
                    </form>
                    <hr/>

                    <{if count($class_lists) <= 1 || $smarty.post.srh_class3}>
                        <table class="table table-hover table-condensed table-bordered ldr-table">
                            <thead>
                                <tr>
                                    <th rowspan="2">����̾</th>
                                    <th rowspan="2">����</th>
                                    <th rowspan="2">����</th>
                                    <th colspan="2">����ɾ��</th>
                                    <th colspan="3">¾��ɾ��</th>
                                </tr>
                                <tr>
                                    <th>ɾ�������<br><small><{$nlv[1]}> �� 4�� / <{$nlv[2]}> �� 3�� / <{$nlv[3]}> �� 2�� / <{$nlv[4]}> �� 1��</small></th>
                                    <th>���Ͼ���</th>
                                    <th>�ǽ�ɾ����</th>
                                    <th>ɾ�������</th>
                                    <th>���Ͼ���</th>
                                </tr>
                            </thead>
                            <tbody>
                                <{foreach name=table item=row from=$lists}>
                                    <tr>
                                        <td class="ldr-td-name"><{$row.emp_name|escape}></td>
                                        <td class="ldr-td-name"><{$row.class_full_name|escape}></td>
                                        <td class="ldr-td-name"><{$row.job_nm|escape}></td>
                                        <td class="ldr-td-number bar"><{$row.assessment.sum|default:0|escape}><span class="barX" style="width:<{$row.sum_percent|escape}>%;"></span></td>
                                        <td class="ldr-td-number bar"><{$row.percent|escape}>%<span class="barX" style="width:<{$row.percent|escape}>%;"></span></td>
                                        <td class="ldr-td-name"><{$row.fassessment.facilitator|escape}><br><{$row.fassessment.updated_on|date_format_jp:"%Y/%m/%d %R"}></td>
                                        <td class="ldr-td-number bar"><{$row.fassessment.sum|default:0|escape}><span class="barX" style="width:<{$row.sum_percentf|escape}>%;"></span></td>
                                        <td class="ldr-td-number bar"><{$row.percentf|escape}>%<span class="barX" style="width:<{$row.percentf|escape}>%;"></span></td>
                                    </tr>
                                <{/foreach}>
                            </tbody>
                        </table>
                    <{/if}>
                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {

            });
        </script>
    </body>
</html>
