<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 認定者一覧</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='認定者一覧' hd_sub_title=''}>

            <div class="container-fluid">
                <div class="row-fluid">
                    <form method="POST">
                        <div class="control-group form-inline">
                            <select id="year" name="year" class="input-medium" style="margin-bottom:0;" data-emp="<{$emp->emp_id()|escape}>" data-menu="<{$smarty.post.menu|escape}>" data-url="<{$smarty.post.owner_url|escape}>">
                                <{foreach item=y from=$year_list}>
                                    <option value="<{$y|escape}>" <{if $y === $year}>selected<{/if}>><{$y|escape}>年度</option>
                                <{/foreach}>
                            </select>
                            <select id="ladder_id" name="ladder_id" class="input-xlarge" style="margin-bottom:0;" data-emp="<{$emp->emp_id()|escape}>" data-menu="<{$smarty.post.menu|escape}>" data-url="<{$smarty.post.owner_url|escape}>">
                                <{foreach item=ldr from=$ladder->lists()}>
                                    <option value="<{$ldr.ladder_id|escape}>"><{$ldr.title|escape}></option>
                                <{/foreach}>
                            </select>
                            <button type="submit" id="btn_search" name="btn_search" value="search" class="btn btn-small btn-primary"><i class="icon-search"></i> 検索する</button>
                            <div class="pull-right">
                                &nbsp;<button id="btn-print" type="button" class="js-staff-levelup-print btn btn-small btn-primary"><i class="icon-print"></i> 印刷する</button>
                            </div>
                        </div>
                    </form>

                    <form action="#" method="post" id="emp_form">
                        <input type="hidden" id="menu" name="menu" value="accept" />
                        <input type="hidden" id="owner_url" name="owner_url" value="staff_levelup.php" />

                        <div class="well well-small" id="form_search">
                            <table class="table table-striped table-hover table-condensed table-bordered ldr-table">
                                <thead>
                                    <tr>
                                        <th>部署</th>
                                        <th>職員</th>
                                        <th><a href="?order=st_order_no&desc=<{if $order === "st_order_no" && $desc === '1'}>0<{else}>1<{/if}>">役職</a></th>
                                        <th><a href="?order=nurse_date&desc=<{if $order === "nurse_date" && $desc === '1'}>0<{else}>1<{/if}>">看護師経験年数</a></th>

                                        <th><a href="?order=ldr_level.level&desc=<{if $order === "ldr_level.level" && $desc === '1'}>0<{else}>1<{/if}>">認定レベル</a></th>
                                        <th><a href="?order=ldr_level.date&desc=<{if $order === "ldr_level.date" && $desc === '1'}>0<{else}>1<{/if}>">認定日</a></th>
                                        <th><a href="?order=ldr_level.number&desc=<{if $order === "ldr_level.number" && $desc === '1'}>0<{else}>1<{/if}>">認定番号</a></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <{foreach item=row from=$list}>
                                        <tr>
                                            <td class="ldr-td-name">
                                                <{$row->class_full_name()|escape|replace:' &gt; ':'<br/>'}>
                                            </td>
                                            <td class="ldr-td-name"><b title="<{$row->emp_personal_id()|escape}>"><{$row->emp_name()|escape}></b></td>
                                            <td class="ldr-td-name"><{$row->st_name()|escape}></td>
                                            <td class="ldr-td-name"><{if $row->nurse_age()}><{$row->nurse_age()|escape}>年目<{/if}></td>

                                            <td class="ldr-td-level">
                                                <{if $row->level($ladder_id) === '0'}>
                                                    <{$ladder->level0($ladder_id)|escape}>
                                                <{else}>
                                                    <{$row->level_roman($ladder_id)|escape}>
                                                <{/if}>
                                            </td>
                                            <td class="ldr-td-date">
                                                <{$row->_getter('date')|date_format_jp:"%Y年%-m月%-d日"}>
                                            </td>
                                            <td class="ldr-td-name">
                                                <{$row->_getter('number')}>
                                            </td>
                                        </tr>
                                    <{/foreach}>
                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>
            </div>

        </div>
        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {
                /**
                 * 印刷
                 */
                $('.js-staff-levelup-print').click(function() {
                    $('<form action="" method="post" target="sheet" onsubmit="return openPDF(this);"></form>')
                        .append($('<input></input>').attr({type: 'hidden', name: 'mode', value: 'print'}))
                        .appendTo(document.body)
                        .submit()
                        .remove();
                });
            });
        </script>
    </body>
</html>