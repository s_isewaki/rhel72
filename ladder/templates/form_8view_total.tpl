<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | <{$ldr.config.form_8.title|escape}></title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title=$ldr.config.form_8.title hd_sub_title=$ldr.config.form_8.detailL}>

            <div class="container-fluid">
                <div class="row-fluid">
                    <{*印刷ボタン*}>
                    <form id="form_print" method="post" class="pull-right">
                        <input type="hidden" name="mode" value="" />
                        <input type="hidden" name="menu" value="" />
                        <input type="hidden" name="id" value="" />
                        <input type="hidden" name="target_emp_id" value=""/>
                        <input type="hidden" name="owner_url" value="" />
                        <{if $emp->emp_id() === $smarty.post.target_emp_id}>
                            &nbsp;<button type="button" data-id="<{$smarty.post.id}>" data-emp="<{$smarty.post.target_emp_id}>" data-php="form_7edit.php" data-mode="edit" class="js-form-button btn btn-danger btn-small"><i class="icon-edit"></i> 編集する</button>
                        <{elseif $apply->is_total_appraiser($emp->emp_id())}>
                            &nbsp;<button type="button" data-id="<{$smarty.post.id}>" data-emp="<{$smarty.post.target_emp_id}>" data-php="form_8edit_total.php" data-mode="edit" class="js-form-button btn btn-danger btn-small"><i class="icon-edit"></i> 編集する</button>
                        <{/if}>
                        &nbsp;<!--button type="button" data-id="<{$smarty.post.id}>" data-emp="<{$smarty.post.target_emp_id}>" data-php="form_7view.php" data-target="form7" class="js-form-print btn btn-primary btn-small"><i class="icon-print"></i> 印刷する</button-->
                    </form>

                    <{*様式共通ボタン*}>
                    <form id="form_edit" method="post">
                        <{include file='__form_view.tpl' op_srh='false' tmp_id='form_7view'}>

                        <input type="hidden" name="active" id="active" value="" />

                        <div class="well well-small">
                            <div class="control-group">
                                <div class="input-prepend" style="margin-bottom:0;">
                                    <span class="add-on">申請レベル</span>
                                    <span class="ldr-uneditable input-mini ldr-level" style="margin-bottom:0;"><{$apply->level_text()|escape}></span>
                                </div>
                                <div class="input-prepend" style="margin-bottom:0;">
                                    <span class="add-on">申請者</span>
                                    <span class="ldr-uneditable" style="margin-bottom:0;"><{$target_emp->emp_name()|escape}> (<{$target_emp->class_full_name()|escape}>)</span>
                                </div>
                            </div>
                            <hr/>

                            <ul id="tabs" class="nav nav-tabs">
                                <{foreach item=row from=$group1_list}>
                                    <li class="<{if $active === $row.guideline_group1}>active<{/if}>">
                                        <a href="#g1-<{$row.guideline_group1|escape}>" data-toggle="tab"><{$row.name|escape}></a>
                                    </li>
                                <{/foreach}>
                                <li class="comment">
                                    <a href="#comment" data-toggle="tab">
                                        レベルアップ
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content" style="overflow:hidden;">
                                <{* form72 tab内容*}>
                                <{foreach item=g1 from=$group1_list}>
                                    <div class="tab-pane <{if $active === $g1.guideline_group1}>active<{else}>fade<{/if}>" id="g1-<{$g1.guideline_group1|escape}>" style="overflow:hidden;">

                                        <table class="table table-striped table-hover table-bordered table-condensed ldr-table">
                                            <thead>
                                                <tr>
                                                    <th>行動指標</th>
                                                    <th>評価者</th>
                                                    <th width="40px">評価</th>
                                                    <{if $ldr.config.reason === '1'}><th>根拠</th><{/if}>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <{foreach item=asm from=$assessment_list[$g1.guideline_group1]}>
                                                    <{foreach item=row from=$form1.appr_list_all name=loopname}>
                                                        <tr
                                                            <{if $smarty.foreach.loopname.first}>
                                                                class="warning"
                                                            <{elseif $asm[$row.emp_id].assessment>='1'}>
                                                                class="success"
                                                            <{elseif $asm[$row.emp_id].assessment=='0'}>
                                                                class="error"
                                                            <{else}>
                                                                class="info"
                                                            <{/if}>>
                                                            <{if $smarty.foreach.loopname.first}>
                                                                <td class="ldr-td-text-left" rowspan="<{$form1.appr_list_all|@count}>" style="background-color:#fff;">

                                                                    <{foreach item=gl from=$asm name=loopgl}>
                                                                    <{if $smarty.foreach.loopgl.first}><{$gl.guideline|escape}><{if $gl.criterion}><br><{$gl.criterion|escape}><{/if}><{/if}>
                                                                    <{/foreach}>
                                                            </td>
                                                        <{/if}>
                                                        <td class="ldr-td-text-left">
                                                            <{if $smarty.foreach.loopname.first}>
                                                                <{$row.emp_name|escape}> (本人)
                                                            <{elseif $row.emp_id === 'total'}>
                                                                総合評価(<{$total_appraiser.completed.emp_name|escape}>)
                                                            <{else}>
                                                                <{$row.emp_name|escape}>
                                                            <{/if}>
                                                        </td>
                                                        <td class="ldr-td-text-center">

                                                            <{if is_null($asm[$row.emp_id].assessment)}>
                                                                ?
                                                            <{else}>
                                                                <{foreach item=sel from=$ldr.config.select}>
                                                                    <{if $asm[$row.emp_id].assessment|escape == $sel.value}>
                                                                        <{$sel.button}>
                                                                    <{/if}>
                                                                <{/foreach}>
                                                            <{/if}>
                                                        </td>
                                                        <{if $ldr.config.reason === '1'}><td class="ldr-td-text-left"><{$asm[$row.emp_id].reason|escape|nl2br}></td><{/if}>
                                                    </tr>
                                                <{/foreach}>
                                            <{/foreach}>
                                        </tbody>
                                    </table>
                                </div>
                            <{/foreach}>

                            <{* form73 tab内容*}>
                            <div class="tab-pane <{if $active === 'sec3'}>active<{else}>fade<{/if}>" id="comment">
                                <div class="control-group">
                                    <label class="control-label">
                                        <strong><{$ldr.config.asm_comment|default:'コメント'|escape}></strong>
                                    </label>
                                    <div class="controls">
                                        <span class="input-maxlarge ldr-uneditable"><{$apply_form7.knowledge|escape|nl2br}></span>
                                    </div>
                                </div>
                                <{foreach item=row from=$form1.appr_list name=loopname}>
                                    <div class="control-group">
                                        <label class="control-label">
                                            <strong>評価者(<{$row.emp_name|escape}>) コメント</strong>
                                        </label>
                                        <div class="controls">
                                            <span class="input-maxlarge ldr-uneditable"><{$form7[$row.emp_id].advice|escape|nl2br}></span>
                                        </div>
                                    </div>
                                <{/foreach}>
                                <div class="control-group">
                                    <label class="control-label">
                                        <strong>総合評価(<{$total_appraiser.completed.emp_name|escape}>) コメント</strong>
                                    </label>
                                    <div class="controls">
                                        <span class="input-maxlarge ldr-uneditable"><{$form7.total.advice|escape|nl2br}></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">
                                        <strong>総合レベル</strong>
                                    </label>
                                    <div class="ldr-xxlarge">
                                        <div class="well well-small">
                                            <{if $form7.total.level === '1'}>
                                                申請レベルを認めます
                                            <{elseif $form7.total.level === '2'}>
                                                申請レベルは保留にします。再チャレンジしてください
                                            <{/if}>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="btn-toolbar">
                        <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <{include file="__js.tpl"}>
    <script type="text/javascript">
        $(function() {

            /**
             * タブアクティブ
             */
            if ($('ul.nav-tabs').find('li.active').length === 0) {
                $('ul.nav-tabs').find('a:first').tab('show');
            }

            $('#tabs a').click(function(e) {
                $('#active').val($(this).data("sec"));
            });
        });
    </script>
</body>
</html>