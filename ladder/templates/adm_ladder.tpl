<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | ラダー設定</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='ラダー設定' hd_sub_title=''}>

            <div class="container-fluid">
                <div class="row-fluid">
                    <div class="row-fluid">
                        <form method="post" id="form">
                            <div class="control-group">
                                <button type="button" data-php="adm_ladder_edit.php" data-mode="new" class="js-form-button btn btn-small btn-primary"><i class="icon-plus"></i> ラダーを追加する</button>
                            </div>

                            <{if $list}>
                                <table class="table table-bordered table-hover table-condensed table-striped ldr-table">
                                    <thead>
                                        <tr>
                                            <th><i class="icon-sort"></i></th>
                                            <th>ラダー</th>
                                            <th>設定</th>
                                            <th>受付</th>
                                            <th>指標</th>
                                            <th>レベル</th>
                                            <th>選択肢</th>
                                            <th>対象</th>
                                            <th>承認者</th>
                                            <th>最終更新日時</th>
                                            <th>削除</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody class="sortable">
                                        <{foreach item=row from=$list}>
                                            <tr>
                                                <td class="ldr-td-name dragHandle"><i class="icon-sort"></i><input type="hidden" name="order[]" value="<{$row.ladder_id|escape}>" /></td>
                                                <td class="ldr-td-name"><{$row.title|escape}></td>
                                                <td class="ldr-td-name">
                                                    <button type="button" data-php="adm_ladder_edit.php" data-mode="edit" data-id="<{$row.ladder_id|escape}>" class="js-form-button btn btn-small btn-danger">設定</button>
                                                </td>
                                                <td class="ldr-td-name">
                                                    <{if $row.config.open === '1'}><span class="badge badge-success">OK</span><{else}><span class="badge badge-important">NG</span><{/if}>
                                                    <{if $row.config.start || $row.config.end}>
                                                        <br/>
                                                        <{$row.config.start|date_format_jp:"%Y年%-m月%-d日"}> 〜 <{$row.config.end|date_format_jp:"%Y年%-m月%-d日"}>
                                                    <{/if}>
                                                </td>
                                                <td class="ldr-td-name"><{$row.revision_name|escape}></td>
                                                <td class="ldr-td-name">
                                                    <{foreach item=level from=$row.config.level}>
                                                        <{if $level === '0'}>
                                                            <{$row.config.level0|default:0}>&nbsp;
                                                        <{else}>
                                                            <{$level|escape}>&nbsp;
                                                        <{/if}>
                                                    <{/foreach}>
                                                </td>
                                                <td class="ldr-td-name">
                                                    <{foreach item=select from=$row.config.select}>
                                                        <button type="button" class="btn btn-mini btn-<{$select.color|escape}>" disabled><{$select.button|escape}></button>&nbsp;
                                                    <{/foreach}>
                                                </td>
                                                <td class="ldr-td-name">
                                                    <button type="button" data-php="adm_ladder_edit.php" data-mode="cover" data-id="<{$row.ladder_id|escape}>" class="js-form-button btn btn-small btn-danger">対象</button>
                                                </td>
                                                <td class="ldr-td-name">
                                                    <button type="button" data-php="adm_ladder_edit.php" data-mode="approver" data-id="<{$row.ladder_id|escape}>" class="js-form-button btn btn-small btn-danger">承認者</button>
                                                </td>
                                                <td class="ldr-td-date"><{$row.updated_on|date_format_jp:"%Y年%-m月%-d日(%a) %R"}></td>
                                                <td class="ldr-td-name">
                                                    <button type="button" data-php="adm_ladder.php" data-mode="remove" data-id="<{$row.ladder_id|escape}>" class="js-row-remove btn btn-small btn-danger">削除</button>
                                                </td>
                                                <td class="ldr-td-name">
                                                    <button type="button" data-php="adm_ladder.php" data-mode="print" data-id="<{$row.ladder_id|escape}>" class="js-form-button btn btn-small btn-primary"><i class="icon-print"></i></button>
                                                </td>
                                            </tr>
                                        <{/foreach}>
                                    </tbody>
                                </table>

                                <div class="btn-toolbar">
                                    <button type="button" class="btn btn-small btn-primary js-table-sort"><i class="icon-ok-sign"></i> 順番を並べ替える</button>
                                </div>

                            <{else}>
                                <div class="control-group">
                                    <{*余白調整*}>
                                </div>
                                <div class="alert alert-info">ラダー設定はありません</div>
                            <{/if}>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {
                //項目の削除
                $('.js-row-remove').click(function() {
                    if (!confirm('削除してもよろしいですか？')) {
                        return false;
                    }
                    $(this).parents('form')
                        .attr({method: 'post', action: $(this).data('php')})
                        .append($('<input></input>').attr({type: 'hidden', name: 'id', value: $(this).data('id')}))
                        .append($('<input></input>').attr({type: 'hidden', name: 'mode', value: $(this).data('mode')}))
                        .submit();
                });

                //順番変更
                $('.js-table-sort').click(function() {
                    if (!confirm('順番を変更してもよろしいですか？')) {
                        return false;
                    }

                    $('#form')
                        .append($('<input></input>').attr({type: 'hidden', name: 'mode', value: 'sort'}))
                        .submit();
                    return true;
                });
            });
        </script>
    </body>
</html>
