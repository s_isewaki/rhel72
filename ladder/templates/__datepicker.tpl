<span class="input-append input-prepend dt-owner">
    <span class="<{if $date_type==='year'}>yearpicker<{elseif $date_type==='month'}>monthpicker<{elseif $date_type==='time'}>timepicker<{else}>datepicker<{/if}> date">
        <span class="add-on" style="background-color:#fff;">
            <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="pointer-cursor"></i>
        </span>
        <input type="text" id="<{$date_id|escape}>" name="<{$date_name|escape}>" class="js-datetime-input <{if $date_type==='year'}>input-mini<{else}>input-small<{/if}>" value="<{$date_value|escape}>" <{$date_option}> readonly style="background-color:#fafafa; cursor: pointer;" />
    </span>
    <{if $date_remove !== "off"}>
        <span class="add-on" style="background-color:#fff;"><a href="javascript:void(0);" onclick="$(this).parent().parent().find('input').val('');"><i class="icon-remove"></i></a>
        </span>
    <{/if}>
</span>
