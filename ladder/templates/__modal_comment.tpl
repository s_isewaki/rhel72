<div id="modal_comment" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="modal_comment_label" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icon-remove-circle"></i></button>
        <h3 id="modal_comment_label">コメント</h3>
    </div>
    <div class="modal-body"></div>
    <div class="modal-footer">
        <form id="form_comment" action="comment_modal.php" method="post">
            <div id="modal_comment_comment" class="control-group">
                <div>
                    <textarea name="comment" class="input-xxlarge" rows="2" placeholder="コメントを入力してください"></textarea>
                </div>
                <div id="error_comment_comment" class="help-block hide"></div>
            </div>
            <input type="hidden" id="mode" name="mode" value="comment" />
            <input type="hidden" id="mode2_comment" name="mode2" value="validate" />
        </form>
        <button type="button" class="js-modal-save btn btn-small btn-primary" data-target="#form_comment"><i class="icon-comment"></i> コメントする</button>
        <button class="btn btn-small" data-dismiss="modal" aria-hidden="true"><i class="icon-remove-sign"></i> 閉じる</button>
    </div>
</div>
