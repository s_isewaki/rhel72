<form id="form_g2_modal" action="adm_guideline.php" method="post">
    <div class="form-horizontal">
        <div id="modal_group2_guideline_revision" class="control-group">
            <label class="control-label">
                <span class="badge badge-important required">必須</span>
                <strong>版</strong>
            </label>
            <div class="controls">
                <select name="guideline_revision" class="js-select-revision" data-target="#select_g2_g1">
                    <{foreach item=row from=$revision_list}>
                        <option value="<{$row.guideline_revision|escape}>"><{$row.name|escape}></option>
                    <{/foreach}>
                </select>
                <div id="error_group2_guideline_revision" class="help-block hide"></div>
            </div>
        </div>
        <div id="modal_group2_guideline_group1" class="control-group">
            <label class="control-label">
                <span class="badge badge-important required">必須</span>
                <strong>領域</strong>
            </label>
            <div class="controls">
                <select name="guideline_group1" id="select_g2_g1">
                    <{foreach item=row from=$group1_list}>
                        <option value="<{$row.guideline_group1|escape}>"><{$row.name|escape}></option>
                    <{/foreach}>
                </select>
                <div id="error_group2_guideline_group1" class="help-block hide"></div>
            </div>
        </div>
        <div id="modal_group2_name" class="control-group">
            <label class="control-label">
                <span class="badge badge-important required">必須</span>
                <strong>分類</strong>
            </label>
            <div class="controls">
                <input type="text" name="name" value="" class="input-xxlarge" placeholder="分類を入力してください" />
                <div id="error_group2_name" class="help-block hide"></div>
            </div>
        </div>
        <div id="modal_group2_comment0" class="control-group">
            <label class="control-label">
                <strong>レベル0コメント</strong>
            </label>
            <div class="controls">
                <textarea name="comment0" class="input-large-modal" rows="2" placeholder="レベル0コメントを入力してください"></textarea>
                <div id="error_group2_comment0" class="help-block hide"></div>
            </div>
        </div>
        <div id="modal_group2_comment1" class="control-group">
            <label class="control-label">
                <strong>レベル1コメント</strong>
            </label>
            <div class="controls">
                <textarea name="comment1" class="input-large-modal" rows="2" placeholder="レベル1コメントを入力してください"></textarea>
                <div id="error_group2_comment1" class="help-block hide"></div>
            </div>
        </div>
        <div id="modal_group2_comment2" class="control-group">
            <label class="control-label">
                <strong>レベル2コメント</strong>
            </label>
            <div class="controls">
                <textarea name="comment2" class="input-large-modal" rows="2" placeholder="レベル2コメントを入力してください"></textarea>
                <div id="error_group2_comment2" class="help-block hide"></div>
            </div>
        </div>
        <div id="modal_group2_comment3" class="control-group">
            <label class="control-label">
                <strong>レベル3コメント</strong>
            </label>
            <div class="controls">
                <textarea name="comment3" class="input-large-modal" rows="2" placeholder="レベル3コメントを入力してください"></textarea>
                <div id="error_group2_comment3" class="help-block hide"></div>
            </div>
        </div>
        <div id="modal_group2_comment4" class="control-group">
            <label class="control-label">
                <strong>レベル4コメント</strong>
            </label>
            <div class="controls">
                <textarea name="comment4" class="input-large-modal" rows="2" placeholder="レベル4コメントを入力してください"></textarea>
                <div id="error_group2_comment4" class="help-block hide"></div>
            </div>
        </div>
        <div id="modal_group2_comment5" class="control-group">
            <label class="control-label">
                <strong>レベル5コメント</strong>
            </label>
            <div class="controls">
                <textarea name="comment5" class="input-large-modal" rows="2" placeholder="レベル5コメントを入力してください"></textarea>
                <div id="error_group2_comment5" class="help-block hide"></div>
            </div>
        </div>
        <div id="modal_group2_comment6" class="control-group">
            <label class="control-label">
                <strong>レベル6コメント</strong>
            </label>
            <div class="controls">
                <textarea name="comment6" class="input-large-modal" rows="2" placeholder="レベル6コメントを入力してください"></textarea>
                <div id="error_group2_comment6" class="help-block hide"></div>
            </div>
        </div>
    </div>
    <input type="hidden" name="id" value="" />
    <input type="hidden" name="mode" value="group2" />
    <input type="hidden" id="mode2_group2" name="mode2" value="validate" />
</form>