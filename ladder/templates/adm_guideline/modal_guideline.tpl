<form id="form_gl_modal" action="adm_guideline.php" method="post">
    <div class="form-horizontal">
        <div id="modal_guideline_guideline_revision" class="control-group">
            <label class="control-label">
                <span class="badge badge-important required">ɬ��</span>
                <strong>��</strong>
            </label>
            <div class="controls">
                <select name="guideline_revision" class="js-select-revision" data-target="#select_gl_g1">
                    <{foreach item=row from=$revision_list}>
                        <option value="<{$row.guideline_revision|escape}>"><{$row.name|escape}></option>
                    <{/foreach}>
                </select>
                <div id="error_guideline_guideline_revision" class="help-block hide"></div>
            </div>
        </div>
        <div id="modal_guideline_guideline_group1" class="control-group">
            <label class="control-label">
                <span class="badge badge-important required">ɬ��</span>
                <strong>�ΰ�</strong>
            </label>
            <div class="controls">
                <select name="guideline_group1" class="js-select-group1 input-xxlarge" data-target="#select_gl_g2" id="select_gl_g1">
                    <{foreach item=row from=$group1_list}>
                        <option value="<{$row.guideline_group1|escape}>"><{$row.name|escape}></option>
                    <{/foreach}>
                </select>
                <div id="error_guideline_guideline_group1" class="help-block hide"></div>
            </div>
        </div>
        <div id="modal_guideline_guideline_group2" class="control-group">
            <label class="control-label">
                <span class="badge badge-important required">ɬ��</span>
                <strong>ʬ��</strong>
            </label>
            <div class="controls">
                <select id="select_gl_g2" name="guideline_group2" class="input-xxlarge">
                    <{foreach item=row from=$group2_list}>
                        <option value="<{$row.guideline_group2|escape}>"><{$row.name|escape}></option>
                    <{/foreach}>
                </select>
                <div id="error_guideline_guideline_group2" class="help-block hide"></div>
            </div>
        </div>
        <div id="modal_guideline_level" class="control-group">
            <label class="control-label">
                <span class="badge badge-important required">ɬ��</span>
                <strong>��٥�</strong>
            </label>
            <div class="controls">
                <label class="radio inline"><input type="radio" name="level" value="0" />�ʤ�</label>
                <label class="radio inline"><input type="radio" name="level" value="1" />I</label>
                <label class="radio inline"><input type="radio" name="level" value="2" />II</label>
                <label class="radio inline"><input type="radio" name="level" value="3" />III</label>
                <label class="radio inline"><input type="radio" name="level" value="4" />IV</label>
                <label class="radio inline"><input type="radio" name="level" value="5" />V</label>
                <label class="radio inline"><input type="radio" name="level" value="6" />VI</label>
                <div id="error_guideline_level" class="help-block hide"></div>
            </div>
        </div>
        <div id="modal_guideline_guideline" class="control-group">
            <label class="control-label">
                <span class="badge badge-important required">ɬ��</span>
                <strong>��ư��ɸ</strong>
            </label>
            <div class="controls">
                <textarea name="guideline" class="input-large-modal" rows="4"></textarea>
                <div id="error_guideline_guideline" class="help-block hide"></div>
            </div>
        </div>
        <div id="modal_guideline_criterion" class="control-group">
            <label class="control-label">
                <strong>Ƚ�Ǵ��</strong>
            </label>
            <div class="controls">
                <textarea name="criterion" class="input-large-modal" rows="4"></textarea>
                <div id="error_guideline_criterion" class="help-block hide"></div>
            </div>
        </div>
    </div>
    <input type="hidden" name="id" value="" />
    <input type="hidden" name="mode" value="guideline" />
    <input type="hidden" id="mode2_guideline" name="mode2" value="validate" />
</form>
