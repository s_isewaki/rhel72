<form id="form_g1_modal" action="adm_guideline.php" method="post">
    <div class="form-horizontal">
        <div id="modal_group1_guideline_revision" class="control-group">
            <label class="control-label">
                <span class="badge badge-important required">必須</span>
                <strong>版</strong>
            </label>
            <div class="controls">
                <select name="guideline_revision">
                    <{foreach item=row from=$revision_list}>
                        <option value="<{$row.guideline_revision|escape}>"><{$row.name|escape}></option>
                    <{/foreach}>
                </select>
                <div id="error_group1_guideline_revision" class="help-block hide"></div>
            </div>
        </div>
        <div id="modal_group1_name" class="control-group">
            <label class="control-label">
                <span class="badge badge-important required">必須</span>
                <strong>領域</strong>
            </label>
            <div class="controls">
                <input type="text" name="name" value="" class="input-xxlarge" placeholder="領域を入力してください" />
                <div id="error_group1_name" class="help-block hide"></div>
            </div>
        </div>
        <div id="modal_group1_comment" class="control-group">
            <label class="control-label">
                <strong>コメント</strong>
            </label>
            <div class="controls">
                <textarea name="comment" class="input-large-modal" rows="3" placeholder="コメントを入力してください"></textarea>
                <div id="error_group1_comment" class="help-block hide"></div>
            </div>
        </div>
        <div id="modal_group1_form7" class="control-group">
            <label class="control-label">
                <span class="badge badge-important required">必須</span>
                <strong>様式7の対象</strong>
            </label>
            <div class="controls">
                <label><input type="radio" name="form7" value="t" /> 評価対象である</label>
                <label><input type="radio" name="form7" value="f" /> 評価対象でない</label>
                <div id="error_group1_form7" class="help-block hide"></div>
            </div>
        </div>
    </div>
    <input type="hidden" name="id" value="" />
    <input type="hidden" name="mode" value="group1" />
    <input type="hidden" id="mode2_group1" name="mode2" value="validate" />
</form>