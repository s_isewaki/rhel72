<{javascript src="js/bootstrap-paginator-1.0.min.js"}>

<script type="text/javascript">
    $(function() {
        var options = {
            currentPage: "<{$page_no}>",
            totalPages: "<{$page_count}>",
            useBootstrapTooltip: true,
            itemContainerClass: function(type, page, current) {
                return (page === current) ? "active" : "pointer-cursor";
            },
            onPageClicked: function(e, originalEvent, type, page) {
                if (page === Number('<{$page_no}>')) {
                    return;
                }
                $('#move_pager').val(true);
                $('#page_no').val(page);
                $('<{$form_selector}>').submit();
                e.stopImmediatePropagation();
            },
            shouldShowPage: function(type, page, current) {
                switch (type)
                {
                    default:
                        return true;
                }
            }

        };

        $('#pager').bootstrapPaginator(options);
        $('#move_pager').val('');
    });
</script>