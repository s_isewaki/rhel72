<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | ラダー設定</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='ラダー設定' hd_sub_title=''}>

            <div class="container-fluid">
                <form action="adm_ladder_edit.php" method="post">
                    <input type="hidden" name="ladder_id" value="" />

                    <div class="btn-toolbar">
                        <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                        <button type="submit" name="btn_save" id="btn_save" class="btn btn-small btn-primary" value="btn_save"><i class="icon-save"></i> 保存する</button>
                    </div>
                    <hr/>

                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#basic" data-toggle="tab">基本設定</a></li>
                        <li><a href="#title" data-toggle="tab">名称</a></li>
                        <li><a href="#level" data-toggle="tab">レベル</a></li>
                        <li><a href="#checklist" data-toggle="tab">文言</a></li>
                        <li><a href="#select" data-toggle="tab">評価の選択肢</a></li>
                        <li><a href="#appr" data-toggle="tab">他者評価</a></li>
                        <li><a href="#assessment" data-toggle="tab">評価</a></li>
                        <li><a href="#meeting" data-toggle="tab">評価会</a></li>
                        <li><a href="#appraiser" data-toggle="tab">司会者</a></li>
                        <li><a href="#report" data-toggle="tab">レポート</a></li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active in" id="basic">
                            <div class="well well-small">
                                <div class="control-group">
                                    <label class="control-label"><strong>ラダー名称</strong></label>
                                    <div class="controls">
                                        <input type="text" name="title" class="input-xlarge" required />
                                    </div>
                                    <{foreach from=$error.title item=msg}>
                                        <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                    <{/foreach}>
                                </div>
                                <hr/>

                                <div class="control-group">
                                    <label class="control-label"><strong>ラダー名称(小)</strong></label>
                                    <div class="controls">
                                        <input type="text" name="short" class="input-small" />
                                    </div>
                                    <{foreach from=$error.short item=msg}>
                                        <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                    <{/foreach}>
                                </div>
                                <hr/>

                                <div class="control-group">
                                    <label class="control-label"><strong>申請の受付</strong></label>
                                    <div class="controls">
                                        <label class="radio inline"><input type="radio" name="config[open]" value="1" <{if $data.config.open === '1' || !$data.config.open}>checked<{/if}>/> OK</label>
                                        <label class="radio inline"><input type="radio" name="config[open]" value="2" <{if $data.config.open === '2'}>checked<{/if}>/> NG</label>
                                    </div>
                                    <{foreach from=$error.open item=msg}>
                                        <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                    <{/foreach}>
                                </div>
                                <hr/>

                                <div class="control-group">
                                    <label class="control-label"><strong>申請の受付期間</strong></label>
                                    <div class="controls">
                                        <{include file="__datepicker.tpl" date_id="start_date" date_name="config[start]" date_value=$data.config.start}>
                                        〜
                                        <{include file="__datepicker.tpl" date_id="last_date" date_name="config[end]" date_value=$data.config.end}>
                                    </div>
                                    <p>受付OKの場合のみ期間設定は有効となります。<br/>受付NGの場合は、期間内であっても受付は行われません。</p>
                                    <{foreach from=$error.date item=msg}>
                                        <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                    <{/foreach}>
                                </div>
                                <hr/>

                                <div class="control-group">
                                    <label class="control-label"><strong>サイドメニュー</strong></label>
                                    <div class="controls">
                                        <input type="text" name="config[sidemenu]" class="input-medium" maxlength="9" value="<{$data.config.sidemenu|default:$data.title|escape}>" />
                                        <span class="help-inline">9文字以内に収めてください</span>
                                    </div>
                                    <div class="controls">
                                        <input type="text" name="config[sidesubmenu]" class="input-xlarge" maxlength="12" value="<{$data.config.sidesubmenu|escape}>" />
                                        <span class="help-inline">(メニュー説明文) 12文字以内に収めてください</span>
                                    </div>
                                    <{foreach from=$error.sidemenu item=msg}>
                                        <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                    <{/foreach}>
                                </div>
                                <hr/>

                                <div class="control-group">
                                    <label class="control-label"><strong>ヘッダメニュー</strong></label>
                                    <div class="controls">
                                        <input type="text" name="config[headmenu]" class="input-xlarge" value="<{$data.config.headmenu|default:$data.title|escape}>" />
                                    </div>
                                    <div class="controls">
                                        <input type="text" name="config[headsubmenu]" class="input-xxlarge" value="<{$data.config.headsubmenu|escape}>" />
                                    </div>
                                    <{foreach from=$error.sidemenu item=msg}>
                                        <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                    <{/foreach}>
                                </div>
                                <hr/>

                                <div class="control-group">
                                    <label class="control-label"><strong>行動指標</strong></label>
                                    <div class="controls">
                                        <select name="guideline_revision">
                                            <{foreach item=row from=$revision_list}>
                                                <option value="<{$row.guideline_revision|escape}>"><{$row.name|escape}></option>
                                            <{/foreach}>
                                        </select>
                                    </div>
                                    <{foreach from=$error.guideline_revision item=msg}>
                                        <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                    <{/foreach}>
                                </div>
                                <hr/>

                                <div class="control-group">
                                    <label class="control-label"><strong>承認フロー</strong></label>
                                    <div class="controls">
                                        <label class="radio inline"><input type="radio" name="config[flow]" value="2" <{if $data.config.flow === '2' || !$data.config.flow}>checked<{/if}>/> 標準</label>
                                        <label class="radio inline" title="赤十字"><input type="radio" name="config[flow]" value="1" <{if $data.config.flow === '1'}>checked<{/if}>/> タイプRC</label>
                                        <label class="radio inline" title="順天堂"><input type="radio" name="config[flow]" value="3" <{if $data.config.flow === '3'}>checked<{/if}>/> タイプJ</label>
                                    </div>
                                    <{if $data.config.flow === '1'}>
                                        <div class="controls">
                                            <label class="radio inline"><input type="radio" name="config[rc]" value="1" <{if $data.config.rc === '1' || !$data.config.rc}>checked<{/if}>/> 実践者</label>
                                            <label class="radio inline"><input type="radio" name="config[rc]" value="2" <{if $data.config.rc === '2'}>checked<{/if}>/> 管理者</label>
                                        </div>
                                    <{/if}>
                                    <{foreach from=$error.flow item=msg}>
                                        <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                    <{/foreach}>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane in" id="level">
                            <div class="well well-small">
                                <div class="control-group">
                                    <label class="control-label"><strong>使用するレベル</strong></label>
                                    <div class="controls">
                                        <label class="checkbox inline"><input type="checkbox" name="config[level][]" value="0" <{if in_array('0', $data.config.level)}>checked<{/if}>/> レベル0</label>
                                        <label class="checkbox inline"><input type="checkbox" name="config[level][]" value="1" <{if in_array('1', $data.config.level)}>checked<{/if}>/> 1</label>
                                        <label class="checkbox inline"><input type="checkbox" name="config[level][]" value="2" <{if in_array('2', $data.config.level)}>checked<{/if}>/> 2</label>
                                        <label class="checkbox inline"><input type="checkbox" name="config[level][]" value="3" <{if in_array('3', $data.config.level)}>checked<{/if}>/> 3</label>
                                        <label class="checkbox inline"><input type="checkbox" name="config[level][]" value="4" <{if in_array('4', $data.config.level)}>checked<{/if}>/> 4</label>
                                        <label class="checkbox inline"><input type="checkbox" name="config[level][]" value="5" <{if in_array('5', $data.config.level)}>checked<{/if}>/> 5</label>
                                        <label class="checkbox inline"><input type="checkbox" name="config[level][]" value="6" <{if in_array('6', $data.config.level)}>checked<{/if}>/> 6</label>
                                    </div>
                                    <{foreach from=$error.level item=msg}>
                                        <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                    <{/foreach}>
                                </div>
                                <hr/>

                                <div class="control-group">
                                    <label class="control-label"><strong>レベル0の名称</strong></label>
                                    <div class="controls">
                                        <input type="text" name="config[level0]" class="input-xlarge" value="<{$data.config.level0|escape}>" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane in" id="appr">
                            <div class="well well-small">
                                <div class="control-group">
                                    <label class="control-label"><strong>他者評価の名称</strong></label>
                                    <div class="controls">
                                        <input type="text" name="config[appr_name]" class="input-large" value="<{$data.config.appr_name|default:'他者評価'|escape}>" />
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label"><strong>他者評価者の設定</strong></label>
                                    <p>他者評価者を設定するタイミングを設定してください</p>
                                    <div class="controls">
                                        <label class="inline radio">
                                            <input type="radio" name="config[appr]" value="1" <{if $data.config.appr === '1'}>checked<{/if}>/>本人が申請時に設定する
                                        </label>
                                        <label class="inline radio">
                                            <input type="radio" name="config[appr]" value="2" <{if $data.config.appr === '2'}>checked<{/if}>/>師長が承認時に設定する
                                        </label>
                                    </div>
                                </div>
                                <hr/>

                                <div class="control-group">
                                    <label class="control-label"><strong>匿名評価者</strong></label>
                                    <p>他者評価者を匿名にしますか？</p>
                                    <div class="controls">
                                        <label class="inline radio">
                                            <input type="radio" name="config[anonymous]" value="1" <{if $data.config.anonymous === '1'}>checked<{/if}>/>匿名にする
                                        </label>
                                        <label class="inline radio">
                                            <input type="radio" name="config[anonymous]" value="2" <{if $data.config.anonymous === '2'}>checked<{/if}>/>匿名にしない
                                        </label>
                                    </div>
                                </div>
                                <hr/>

                                <div class="control-group">
                                    <label class="control-label"><strong>他者評価時の本人評価</strong></label>
                                    <p>他者評価時に本人評価の内容を表示しますか？</p>
                                    <div class="controls">
                                        <label class="inline radio">
                                            <input type="radio" name="config[appr_view]" value="1" <{if $data.config.appr_view === '1'}>checked<{/if}>/>表示する
                                        </label>
                                        <label class="inline radio">
                                            <input type="radio" name="config[appr_view]" value="2" <{if $data.config.appr_view === '2'}>checked<{/if}>/>表示しない
                                        </label>
                                    </div>
                                </div>
                                <hr/>

                                <div class="control-group">
                                    <label class="control-label"><strong>他者評価者の人数</strong></label>
                                    <{if $data.config.level}>
                                        <table class="table table-condensed ldr-table">
                                            <thead>
                                                <tr>
                                                    <th>レベル</th>
                                                    <th>他者評価者の人数</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <{foreach item=level from=$data.config.level}>
                                                    <tr>
                                                        <td class="ldr-td-level">
                                                            <{if $level === '0'}>
                                                                <{$data.config.level0|default:0|escape}>
                                                            <{else}>
                                                                <{$roman_num[$level]|escape}>
                                                            <{/if}>
                                                        </td>
                                                        <td class="ldr-td-date">
                                                            <div class="input-append" style="margin-bottom:0;">
                                                                <input type="text" class="input input-mini text-right onlynum" name="config[appr_min][<{$level|escape}>]" value="<{$data.config.appr_min[$level]|default:'3'|escape}>" maxlength="2" />
                                                                <span class="add-on">人</span>
                                                            </div>
                                                            〜
                                                            <div class="input-append" style="margin-bottom:0;">
                                                                <input type="text" class="input input-mini text-right onlynum" name="config[appr_max][<{$level|escape}>]" value="<{$data.config.appr_max[$level]|default:'3'|escape}>" maxlength="2" />
                                                                <span class="add-on">人</span>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                <{/foreach}>
                                            </tbody>
                                        </table>
                                    <{else}>
                                        レベルの設定を先に行ってください
                                    <{/if}>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane in" id="meeting">
                            <div class="well well-small">
                                <p>※タイプRCの場合のみ有効な設定です</p>
                                <hr/>

                                <div class="control-group">
                                    <label class="control-label"><strong>評価会会場・日時</strong></label>
                                    <p>評価会会場・日時の入力欄を使用しますか？</p>
                                    <div class="controls">
                                        <label class="inline radio">
                                            <input type="radio" name="config[meeting]" value="1" <{if $data.config.meeting === '1'}>checked<{/if}>/>使用する
                                        </label>
                                        <label class="inline radio">
                                            <input type="radio" name="config[meeting]" value="2" <{if $data.config.meeting === '2'}>checked<{/if}>/>使用しない
                                        </label>
                                    </div>
                                </div>
                                <hr/>

                                <div class="control-group">
                                    <label class="control-label"><strong>申請日と評価会の間隔</strong></label>
                                    <p>ラダー申請日から評価会の日程までに最低限必要な日数を設定してください</p>
                                    <div class="controls">
                                        <div class="input-append">
                                            <input type="text" name="config[appr_interval]" class="input-mini onlynum text-right" maxlength="2" value="<{$data.config.appr_interval|default:15|escape}>" />
                                            <span class="add-on">日以上</span>
                                        </div>
                                    </div>
                                    <div class="controls">
                                        <{foreach from=$error.appr_interval item=msg}>
                                            <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                        <{/foreach}>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane in" id="assessment">
                            <div class="well well-small">
                                <div class="control-group">
                                    <label class="control-label"><strong>本人評価のコメント</strong></label>
                                    <p>本人評価のコメントを利用しますか？</p>
                                    <div class="controls">
                                        <label class="inline radio">
                                            <input type="radio" name="config[asm_comment_used]" value="1" <{if $data.config.asm_comment_used === '1'}>checked<{/if}>/>利用する
                                        </label>
                                        <label class="inline radio">
                                            <input type="radio" name="config[asm_comment_used]" value="0" <{if $data.config.asm_comment_used === '0'}>checked<{/if}>/>利用しない
                                        </label>
                                    </div>
                                </div>
                                <hr/>

                                <div class="control-group">
                                    <label class="control-label"><strong>本人評価のコメントの必須設定</strong></label>
                                    <p>本人評価のコメントを必須入力としますか？</p>
                                    <div class="controls">
                                        <label class="inline radio">
                                            <input type="radio" name="config[asm_comment_required]" value="1" <{if $data.config.asm_comment_required === '1'}>checked<{/if}>/>必須
                                        </label>
                                        <label class="inline radio">
                                            <input type="radio" name="config[asm_comment_required]" value="0" <{if $data.config.asm_comment_required === '0'}>checked<{/if}>/>必須ではない
                                        </label>
                                    </div>
                                </div>
                                <hr/>

                                <div class="control-group">
                                    <label class="control-label"><strong>本人評価コメントの名称</strong></label>
                                    <div class="controls">
                                        <input type="text" name="config[asm_comment]" class="input-xlarge" value="<{$data.config.asm_comment|default:'コメント'|escape}>" />
                                    </div>
                                </div>
                                <hr/>

                                <div class="control-group">
                                    <label class="control-label"><strong>本人評価コメントの説明</strong></label>
                                    <div class="controls">
                                        <textarea name="config[asm_comment_guide]" class="input-xxlarge js-autoheight"><{$data.config.asm_comment_guide|escape}></textarea>
                                    </div>
                                </div>
                                <hr/>

                                <div class="control-group">
                                    <label class="control-label"><strong>本人評価のコメント2</strong></label>
                                    <p>本人評価のコメント2を利用しますか？</p>
                                    <div class="controls">
                                        <label class="inline radio">
                                            <input type="radio" name="config[asm_comment2_used]" value="1" <{if $data.config.asm_comment2_used === '1'}>checked<{/if}>/>利用する
                                        </label>
                                        <label class="inline radio">
                                            <input type="radio" name="config[asm_comment2_used]" value="0" <{if $data.config.asm_comment2_used === '0'}>checked<{/if}>/>利用しない
                                        </label>
                                    </div>
                                </div>
                                <hr/>

                                <div class="control-group">
                                    <label class="control-label"><strong>本人評価のコメント2の必須設定</strong></label>
                                    <p>本人評価のコメント2を必須入力としますか？</p>
                                    <div class="controls">
                                        <label class="inline radio">
                                            <input type="radio" name="config[asm_comment2_required]" value="1" <{if $data.config.asm_comment2_required === '1'}>checked<{/if}>/>必須
                                        </label>
                                        <label class="inline radio">
                                            <input type="radio" name="config[asm_comment2_required]" value="0" <{if $data.config.asm_comment2_required === '0'}>checked<{/if}>/>必須ではない
                                        </label>
                                    </div>
                                </div>
                                <hr/>

                                <div class="control-group">
                                    <label class="control-label"><strong>本人評価コメント2の名称</strong></label>
                                    <div class="controls">
                                        <input type="text" name="config[asm_comment2]" class="input-xlarge" value="<{$data.config.asm_comment2|default:'自己の課題'|escape}>" />
                                    </div>
                                </div>
                                <hr/>

                                <div class="control-group">
                                    <label class="control-label"><strong>本人評価コメント2の説明</strong></label>
                                    <div class="controls">
                                        <textarea name="config[asm_comment2_guide]" class="input-xxlarge js-autoheight"><{$data.config.asm_comment2_guide|escape}></textarea>
                                    </div>
                                </div>
                                <hr/>

                                <div class="control-group">
                                    <label class="control-label"><strong>評価の根拠</strong></label>
                                    <p>評価の根拠の入力欄を表示しますか？</p>
                                    <div class="controls">
                                        <label class="inline radio">
                                            <input type="radio" name="config[reason]" value="1" <{if $data.config.reason === '1'}>checked<{/if}>/>表示する
                                        </label>
                                        <label class="inline radio">
                                            <input type="radio" name="config[reason]" value="0" <{if $data.config.reason === '0'}>checked<{/if}>/>表示しない
                                        </label>
                                    </div>
                                </div>
                                <hr/>

                                <div class="control-group">
                                    <label class="control-label"><strong>同僚評価の根拠必須</strong></label>
                                    <p>本人評価と同僚評価が異なった場合、根拠の入力を必須としますか？</p>
                                    <div class="controls">
                                        <label class="inline radio">
                                            <input type="radio" name="config[diff_reason]" value="1" <{if $data.config.diff_reason === '1'}>checked<{/if}>/>必須とする
                                        </label>
                                        <label class="inline radio">
                                            <input type="radio" name="config[diff_reason]" value="0" <{if $data.config.diff_reason === '0'}>checked<{/if}>/>必須としない
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane in" id="report">
                            <div class="well well-small">
                                <p>※タイプJの場合のみ有効な設定です</p>
                                <hr/>

                                <div class="control-group">
                                    <label class="control-label"><strong>レポート評価の指標</strong></label>
                                    <div class="controls">
                                        <select name="config[report_guideline]">
                                            <{foreach item=row from=$revision_list}>
                                                <option value="<{$row.guideline_revision|escape}>" <{if $row.guideline_revision === $data.config.report_guideline}>selected<{/if}>><{$row.name|escape}></option>
                                            <{/foreach}>
                                        </select>
                                    </div>
                                    <{foreach from=$error.guideline_revision item=msg}>
                                        <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                    <{/foreach}>
                                </div>
                                <hr />
                                <div class="form-group">
                                    <p>レポート評価の選択肢を設定してください</p>
                                    <div class="btn-toolbar">
                                        <button type="button" id="js-select-add-rep" class="btn btn-small btn-primary"><i class="icon-plus"></i> 選択肢を追加する</button>
                                    </div>
                                    <table class="table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th><i class="icon-sort"></i></th>
                                                <th>ボタン</th>
                                                <th>値</th>
                                                <th>ボタン色</th>
                                                <th class="pull-right"><i class='icon-remove'></i></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody id="js-select-table-rep" class="sortable">
                                            <{foreach name=select item=row from=$data.config.report_select}>
                                                <tr>
                                                    <td class="dragHandle"><i class="icon-sort"></i></td>
                                                    <td><input type="text" class="input-mini jpn" name="config[report_select][button][]" value="<{$row.button|escape}>" /></td>
                                                    <td><input type="text" class="input-mini num" name="config[report_select][value][]" value="<{$row.value|escape}>" /></td>
                                                    <td>
                                                        <select name="config[report_select][color][]">
                                                            <option style="background-color:#006dcc;" value="primary" <{if $row.color === 'primary'}>selected="selected"<{/if}>>青</option>
                                                            <option style="background-color:#49afcd;" value="info" <{if $row.color === 'info'}>selected="selected"<{/if}>>水色</option>
                                                            <option style="background-color:#5bb75b;" value="success" <{if $row.color === 'success'}>selected="selected"<{/if}>>緑</option>
                                                            <option style="background-color:#faa732;" value="warning" <{if $row.color === 'warning'}>selected="selected"<{/if}>>オレンジ</option>
                                                            <option style="background-color:#da4f49;" value="danger" <{if $row.color === 'danger'}>selected="selected"<{/if}>>赤</option>
                                                            <option style="background-color:#dbadff;" value="purple" <{if $row.color === 'purple'}>selected="selected"<{/if}>>紫</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <a class="pull-right js-row-remove" href="javascript:void(0);"><i class="icon-remove"></i></a>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                            <{/foreach}>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade in" id="select">
                            <div class="well well-small">
                                <div class="form-group">
                                    <p>評価の選択肢を設定してください</p>
                                    <div class="btn-toolbar">
                                        <button type="button" id="js-select-add" class="btn btn-small btn-primary"><i class="icon-plus"></i> 選択肢を追加する</button>
                                    </div>
                                    <table class="table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th><i class="icon-sort"></i></th>
                                                <th>ボタン</th>
                                                <th>値</th>
                                                <th>ボタン色</th>
                                                <th class="pull-right"><i class='icon-remove'></i></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody id="js-select-table" class="sortable">
                                            <{foreach name=select item=row from=$data.config.select}>
                                                <tr>
                                                    <td class="dragHandle"><i class="icon-sort"></i></td>
                                                    <td><input type="text" class="input-mini jpn" name="config[select][button][]" value="<{$row.button|escape}>" /></td>
                                                    <td><input type="text" class="input-mini num" name="config[select][value][]" value="<{$row.value|escape}>" /></td>
                                                    <td>
                                                        <select name="config[select][color][]">
                                                            <option style="background-color:#006dcc;" value="primary" <{if $row.color === 'primary'}>selected="selected"<{/if}>>青</option>
                                                            <option style="background-color:#49afcd;" value="info" <{if $row.color === 'info'}>selected="selected"<{/if}>>水色</option>
                                                            <option style="background-color:#5bb75b;" value="success" <{if $row.color === 'success'}>selected="selected"<{/if}>>緑</option>
                                                            <option style="background-color:#faa732;" value="warning" <{if $row.color === 'warning'}>selected="selected"<{/if}>>オレンジ</option>
                                                            <option style="background-color:#da4f49;" value="danger" <{if $row.color === 'danger'}>selected="selected"<{/if}>>赤</option>
                                                            <option style="background-color:#dbadff;" value="purple" <{if $row.color === 'purple'}>selected="selected"<{/if}>>紫</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <a class="pull-right js-row-remove" href="javascript:void(0);"><i class="icon-remove"></i></a>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                            <{/foreach}>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade in" id="appraiser">
                            <div class="well well-small">
                                <p>※タイプRCの場合のみ有効な設定です</p>
                                <hr/>

                                <div class="form-group">
                                    <div class="control-group">
                                        <label class="control-label"><strong>司会者の役職</strong></label>
                                        <{if $data.config.level}>
                                            <table class="table table-condensed ldr-table">
                                                <thead>
                                                    <tr>
                                                        <th nowrap>レベル</th>
                                                        <th>司会者の役職</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="faci_list">
                                                    <{foreach item=level from=$data.config.level}>
                                                        <tr>
                                                            <td class="ldr-td-level">
                                                                <{if $level === '0'}>
                                                                    <{$data.config.level0|default:0|escape}>
                                                                <{else}>
                                                                    <{$roman_num[$level]|escape}>
                                                                <{/if}>
                                                            </td>
                                                            <td class="ldr-td-text-left">
                                                                <{foreach item=st from=$status->lists()}>
                                                                    <label class="checkbox inline">
                                                                        <input type="checkbox" name="config[appr_st][<{$level|escape}>][]" value="<{$st.id|escape}>"
                                                                               <{if $data.config.appr_st[$level] && in_array($st.id, $data.config.appr_st[$level])}>checked<{/if}>
                                                                               ><{$st.name|escape}>
                                                                    </label>
                                                                <{/foreach}>
                                                            </td>
                                                        </tr>
                                                    <{/foreach}>
                                                </tbody>
                                            </table>
                                        <{else}>
                                            レベルの設定を先に行ってください
                                        <{/if}>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade in" id="checklist">
                            <div class="well well-small">
                                <div class="form-group">
                                    <div class="control-group">
                                        <label class="control-label"><strong><{$data.config.form_6.title|escape}>の説明文</strong></label>
                                        <div class="controls">
                                            <textarea style="margin-bottom:0;" name="config[report_text]" rows="2" class="input-large-modal js-autoheight"><{$data.config.report_text}></textarea>
                                        </div>
                                    </div>
                                    <hr />

                                    <div class="control-group">
                                        <label class="control-label"><strong><{$data.config.form_7.title|escape}>:レベルごとの説明文</strong></label>
                                        <{if $data.config.level}>
                                            <table class="table table-condensed ldr-table">
                                                <thead>
                                                    <tr>
                                                        <th nowrap>レベル</th>
                                                        <th>説明文</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="faci_list">
                                                    <{foreach item=level from=$data.config.level}>
                                                        <tr>
                                                            <td class="ldr-td-level">
                                                                <{if $level === '0'}>
                                                                    <{$data.config.level0|default:0|escape}>
                                                                <{else}>
                                                                    <{$roman_num[$level]|escape}>
                                                                <{/if}>
                                                            </td>
                                                            <td class="ldr-td-text-left">
                                                                <textarea style="margin-bottom:0;" name="config[word][<{$level|escape}>]" rows="2" class="input-large-modal js-autoheight"><{$data.config.word[$level]|escape}></textarea>
                                                            </td>
                                                        </tr>
                                                    <{/foreach}>
                                                </tbody>
                                            </table>
                                        <{else}>
                                            レベルの設定を先に行ってください
                                        <{/if}>
                                    </div>
                                    <hr />

                                    <div class="control-group">
                                        <label class="control-label"><strong>レベルごとのチェック項目</strong></label>
                                        <{if $data.config.level}>
                                            <table class="table table-condensed ldr-table">
                                                <thead>
                                                    <tr>
                                                        <th nowrap>レベル</th>
                                                        <th>チェック項目<br/>(1行1項目となります)</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="faci_list">
                                                    <{foreach item=level from=$data.config.level}>
                                                        <tr>
                                                            <td class="ldr-td-level">
                                                                <{if $level === '0'}>
                                                                    <{$data.config.level0|default:0|escape}>
                                                                <{else}>
                                                                    <{$roman_num[$level]|escape}>
                                                                <{/if}>
                                                            </td>
                                                            <td class="ldr-td-text-left">
                                                                <textarea style="margin-bottom:0;" name="config[checklist][<{$level|escape}>]" rows="2" class="input-large-modal js-autoheight"><{$data.config.checklist[$level]|escape}></textarea>
                                                            </td>
                                                        </tr>
                                                    <{/foreach}>
                                                </tbody>
                                            </table>
                                        <{else}>
                                            レベルの設定を先に行ってください
                                        <{/if}>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade in in" id="title">
                            <div class="well well-small">
                                <p>変更したい項目の名称を変更してください。</p>
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <td>項目</td>
                                            <td>名称</td>
                                            <td>短い説明</td>
                                            <td>長い説明</td>
                                            <td>大ボタン</td>
                                            <td>小ボタン</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td nowrap>ラダー申請書</td>
                                            <td><span title="[基本設定]サイドメニューで設定してください" class="input-medium ldr-uneditable"><{$data.config.sidemenu|default:$data.title|escape}></span></td>
                                            <td><span title="[基本設定]サイドメニュー(メニュー説明文)で設定してください" class="input-large ldr-uneditable"><{$data.config.sidesubmenu|escape}></span></td>
                                            <td><span title="[基本設定]ヘッダメニュー(メニュー説明文)で設定してください" class="input-xlarge ldr-uneditable"><{$data.config.headsubmenu|escape}></span></td>
                                            <td><input type="text" class="input-small" name="config[form_1][btn]" value="<{$data.config.form_1.btn|default:'様式1'|escape}>"></td>
                                            <td><input type="text" class="input-mini" name="config[form_1][btnS]" value="<{$data.config.form_1.btnS|default:'1'|escape}>"></td>
                                        </tr>
                                        <tr>
                                            <td nowrap>ナラティブ</td>
                                            <td><input type="text" class="input-medium" name="config[form_6][title]" value="<{$data.config.form_6.title|default:'様式6'|escape}>"></td>
                                            <td><input type="text" class="input-large" name="config[form_6][detail]" value="<{$data.config.form_6.detail|default:'事例記入用紙'|escape}>"></td>
                                            <td><input type="text" class="input-xlarge" name="config[form_6][detailL]" value="<{$data.config.form_6.detailL|default:'事例記入用紙'|escape}>"></td>
                                            <td><input type="text" class="input-small" name="config[form_6][btn]" value="<{$data.config.form_6.btn|default:'様式6'|escape}>"></td>
                                            <td><input type="text" class="input-mini" name="config[form_6][btnS]" value="<{$data.config.form_6.btnS|default:'6'|escape}>"></td>
                                        </tr>
                                        <tr>
                                            <td nowrap>評価表</td>
                                            <td><input type="text" class="input-medium" name="config[form_7][title]" value="<{$data.config.form_7.title|default:'様式7'|escape}>"></td>
                                            <td><input type="text" class="input-large" name="config[form_7][detail]" value="<{$data.config.form_7.detail|default:'キャリア開発ラダー評価表'}>"></td>
                                            <td><input type="text" class="input-xlarge" name="config[form_7][detailL]" value="<{$data.config.form_7.detailL|default:'赤十字医療施設のキャリア開発ラダー評価表'|escape}>"></td>
                                            <td><input type="text" class="input-small" name="config[form_7][btn]" value="<{$data.config.form_7.btn|default:'様式7'|escape}>"></td>
                                            <td><input type="text" class="input-mini" name="config[form_7][btnS]" value="<{$data.config.form_7.btnS|default:'7'|escape}>"></td>
                                        </tr>
                                        <tr>
                                            <td nowrap>総合評価</td>
                                            <td><input type="text" class="input-medium" name="config[form_8][title]" value="<{$data.config.form_8.title|default:'様式8'|escape}>"></td>
                                            <td><input type="text" class="input-large" name="config[form_8][detail]" value="<{$data.config.form_8.detail|default:'キャリア開発ラダー評価表・総括'|escape}>"></td>
                                            <td><input type="text" class="input-xlarge" name="config[form_8][detailL]" value="<{$data.config.form_8.detailL|default:'赤十字医療施設のキャリア開発ラダー評価表・総括'|escape}>"></td>
                                            <td><input type="text" class="input-small" name="config[form_8][btn]" value="<{$data.config.form_8.btn|default:'様式8'|escape}>"></td>
                                            <td><input type="text" class="input-mini" name="config[form_8][btnS]" value="<{$data.config.form_8.btnS|default:'8'|escape}>"></td>
                                        </tr>
                                        <tr>
                                            <td nowrap>監査表</td>
                                            <td><input type="text" class="input-medium" name="config[form_9][title]" value="<{$data.config.form_9.title|default:'様式9'|escape}>"></td>
                                            <td><input type="text" class="input-large" name="config[form_9][detail]" value="<{$data.config.form_9.detail|default:'レベル認定基準用紙'|escape}>"></td>
                                            <td><input type="text" class="input-xlarge" name="config[form_9][detailL]" value="<{$data.config.form_9.detailL|default:'レベル認定基準用紙'|escape}>"></td>
                                            <td><input type="text" class="input-small" name="config[form_9][btn]" value="<{$data.config.form_9.btn|default:'様式9'|escape}>"></td>
                                            <td><input type="text" class="input-mini" name="config[form_9][btnS]" value="<{$data.config.form_9.btnS|default:'9'|escape}>"></td>
                                        </tr>
                                        <tr>
                                            <td nowrap>師長申込前</td>
                                            <td><input type="text" class="input-medium" name="config[status][4]" value="<{$data.config.status[4]|default:'師長申込前'|escape}>"></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td nowrap>師長申込中</td>
                                            <td><input type="text" class="input-medium" name="config[status][5]" value="<{$data.config.status[5]|default:'師長申込中'|escape}>"></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td nowrap>申込差し戻し</td>
                                            <td><input type="text" class="input-medium" name="config[status][6]" value="<{$data.config.status[6]|default:'申込差し戻し'|escape}>"></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td nowrap>同僚評価中</td>
                                            <td><input type="text" class="input-medium" name="config[status][8]" value="<{$data.config.status[8]|default:'同僚評価中'|escape}>"></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td nowrap>評価会待ち</td>
                                            <td><input type="text" class="input-medium" name="config[status][9]" value="<{$data.config.status[9]|default:'評価会待ち'|escape}>"></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td nowrap>監査者設定</td>
                                            <td><input type="text" class="input-medium" name="config[status][10]" value="<{$data.config.status[10]|default:'監査者設定'|escape}>"></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td nowrap>監査中</td>
                                            <td><input type="text" class="input-medium" name="config[status][11]" value="<{$data.config.status[11]|default:'監査中'|escape}>"></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td nowrap><{$titles.committee|escape}></td>
                                            <td><input type="text" class="input-medium" name="config[status][12]" value="<{$data.config.status[12]|default:$titles.committee|escape}>"></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td nowrap>終了</td>
                                            <td><input type="text" class="input-medium" name="config[status][0]" value="<{$data.config.status[0]|default:'終了'|escape}>"></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td nowrap>申込取り下げ</td>
                                            <td><input type="text" class="input-medium" name="config[status][m2]" value="<{$data.config.status.m2|default:'申込取り下げ'|escape}>"></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <{if $data.config.flow === '3'}>
                                            <tr>
                                                <td nowrap>レポート評価者設定</td>
                                                <td><input type="text" class="input-medium" name="config[status][21]" value="<{$data.config.status[21]|default:'レポート評価者設定'|escape}>"></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td nowrap>レポート評価</td>
                                                <td><input type="text" class="input-medium" name="config[status][22]" value="<{$data.config.status[22]|default:'レポート評価'|escape}>"></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td nowrap>レポート評価確認</td>
                                                <td><input type="text" class="input-medium" name="config[status][23]" value="<{$data.config.status[23]|default:'レポート評価確認'|escape}>"></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        <{/if}>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="btn-toolbar">
                        <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                        <button type="submit" name="btn_save" id="btn_save" class="btn btn-small btn-primary" value="btn_save"><i class="icon-save"></i> 保存する</button>
                    </div>
                </form>
            </div>
        </div>
        </div>
        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {

                // 選択肢追加
                $('#js-select-add').on('click', function() {
                    $('#js-select-table').append(tr('select'));
                });
                $('#js-select-add-rep').on('click', function() {
                    $('#js-select-table-rep').append(tr('report_select'));
                });

                // 行の削除
                $('body').on('click', '.js-row-remove', function() {
                    $(this).parent().parent().remove();
                });

            });

            function tr(type) {
                return [
                    '<tr>',
                    '<td class="dragHandle"><i class="icon-sort"></i></td>',
                    '<td><input type="text" class="input-mini jpn" name="config[' + type + '][button][]" /></td>',
                    '<td><input type="text" class="input-mini num" name="config[' + type + '][value][]" /></td>',
                    '<td>',
                    '<select name="config[' + type + '][color][]">',
                    '<option style="background-color:#006dcc;" value="primary">青</option>',
                    '<option style="background-color:#49afcd;" value="info">水色</option>',
                    '<option style="background-color:#5bb75b;" value="success">緑</option>',
                    '<option style="background-color:#faa732;" value="warning">オレンジ</option>',
                    '<option style="background-color:#da4f49;" value="danger">赤</option>',
                    '<option style="background-color:#dbadff;" value="purple">紫</option>',
                    '</select>',
                    '</td>',
                    '<td><a class="pull-right js-row-remove" href="javascript:void(0);"><i class="icon-remove"></i></a></td>',
                    '<td></td>',
                    '</tr>'
                ].join('');
            }
        </script>
    </body>
</html>
