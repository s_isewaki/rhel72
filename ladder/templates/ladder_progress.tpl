<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | <{$titles.ladder|default:'ラダー'|escape}>進捗</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title="`$titles.ladder`進捗" hd_sub_title=''}>

            <div class="container-fluid">
                <div class="row-fluid">
                    <form name="accept_form" id="accept_form" class="form" method="POST">
                        <input type="hidden" id="mode" name="mode" value="" />
                        <input type="hidden" id="id" name="id" value="" />
                        <input type="hidden" id="menu" name="menu" value="accept" />
                        <input type="hidden" id="emp_id" name="emp_id" value="" />
                        <input type="hidden" id="target_emp_id" name="target_emp_id" value="" />
                        <input type="hidden" id="owner_url" name="owner_url" value="ladder_progress.php" />

                        <div class="control-group">
                            <div class="form-inline">
                                <{include file="__datepicker.tpl" date_id="srh_start_date" date_name="srh_start_date" date_value=$srh_start_date|escape}>
                                <span>〜</span>
                                <{include file="__datepicker.tpl" date_id="srh_last_date" date_name="srh_last_date" date_value=$srh_last_date|escape}>
                                <select name="srh_level" id="srh_level" class="input-small">
                                    <option value="">レベル</option>
                                    <option value="0"><{$level0|escape}></option>
                                    <option value="1">I</option>
                                    <option value="2">II</option>
                                    <option value="3">III</option>
                                    <option value="4">IV</option>
                                    <option value="5">V</option>
                                    <option value="6">VI</option>
                                </select>
                                <select name="srh_status" id="srh_status" class="input-xlarge">
                                    <option value="">ステータス</option>
                                    <{foreach key=num item=status_text from=$status_lists}>
                                        <{if $num === 4}>
                                            <option value="4,6"><{$status_text|escape}>・<{$status_lists.6|escape}></option>
                                        <{elseif $num != "6"}>
                                            <option value="<{$num|escape}>"><{$status_text|escape}></option>
                                        <{/if}>
                                    <{/foreach}>
                                </select>
                            </div>
                        </div>
                        <div class="control-group">
                            <div class="form-inline">
                                <div class="input-prepend">
                                    <span class="add-on"><i class="icon-user-md"></i></span>
                                    <input type="text" id="srh_name" name="srh_name" class="input-xlarge" data-click="#btn_search" placeholder="職員名を入力してください" value="" />
                                </div>
                                <button type="submit" id="btn_search" name="btn_search" value="search" class="btn btn-small btn-primary"><i class="icon-search"></i> 検索する</button>
                                <{if $emp->committee()}>
                                    <button type="submit" id="btn_committee" name="btn_search" value="search" class="btn btn-small btn-primary"><i class="icon-inbox"></i> 受付を確認する</button>
                                <{/if}>
                            </div>
                        </div>
                        <{include file="__pager.tpl"}>
                        <{include file="__table_progress.tpl"}>
                    </form>
                </div>
            </div>
        </div>
        <{include file="__modal_status.tpl"}>
        <{include file="__modal_comment.tpl"}>

        <{include file="__js.tpl"}>
        <{include file="__modal_form_js.tpl"}>
        <{include form_selector="#accept_form" file="__pager_js.tpl"}>
        <script type="text/javascript">
            $('#srh_name').keypress(function(ev) {
                if ((ev.which && ev.which === 13) || (ev.keyCode && ev.keyCode === 13)) {
                    $('#btn_search').trigger("click");
                }
            });
            $('#btn_committee').click(function() {
                $(this).parents('form').attr("action", "accept.php");
            });
        </script>
    </body>
</html>