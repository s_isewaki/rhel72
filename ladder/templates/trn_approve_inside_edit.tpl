<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | <{$hd_title}></title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" emp_name=$target_emp->emp_name() emp_class=$target_emp->class_full_name()}>

            <div class="container-fluid">
                <div class="well well-small">
                    <div class="row-fluid">
                        <strong class="ldr-trn-title">(<{$mst.no|escape}>) <{$mst.title|escape}></strong>
                    </div>

                    <{if $mst.theme}>
                        <div class="row-fluid ldr-trn-row">
                            <div class="span12 ldr-trn-block">
                                <label class="control-label">
                                    <strong>テーマ</strong>
                                </label>
                                <div class="ldr-uneditable input-large-modal"><{$mst.theme|escape|nl2br|default:'(未設定)'}></div>
                            </div>
                        </div>
                    <{/if}>

                    <div class="row-fluid ldr-trn-row">
                        <div class="span3 ldr-trn-block">
                            <label class="control-label">
                                <strong>研修回</strong>
                            </label>
                            <span class="ldr-uneditable input-large-modal">第<{$trn.number|escape}>回 (全<{$trn.number_of|escape}>回)</span>
                        </div>
                        <div class="span3 ldr-trn-block">
                            <label class="control-label">
                                <{if $hospital_type === '1'}>
                                    <strong>めざすレベル</strong>
                                <{else}>
                                    <strong>対象レベル</strong>
                                <{/if}>
                            </label>
                            <{if $mst.level}>
                                <span class="ldr-uneditable input-large-modal ldr-level"><{$roman_num[$mst.level]|escape}></span>
                            <{else}>
                                <span class="ldr-uneditable input-large-modal">なし</span>
                            <{/if}>
                        </div>
                        <div class="span3 ldr-trn-block">
                            <label class="control-label">
                                <strong>カテゴリ</strong>
                            </label>
                            <span class="ldr-uneditable input-large-modal"><{$gp1.name|escape}></span>
                        </div>
                        <div class="span2 ldr-trn-block">
                            <label class="control-label">
                                <strong>外部参加</strong>
                            </label>
                            <span class="ldr-uneditable input-large-modal">
                                <{if $public==='t'}>
                                    可
                                <{else}>
                                    不可
                                <{/if}>
                            </span>
                        </div>
                    </div>
                    <{if $guideline || $guideline_text}>
                        <div class="row-fluid ldr-trn-row">
                            <div class="span12 ldr-trn-block">
                                <label class="control-label"><strong>めざす状態</strong></label>
                                <div class="controls">
                                    <div class="ldr-glbox-modal">
                                        <{foreach from=$mst.guideline key=id item=name}>
                                            <div class="alert alert-info"><{$name|escape}></div>
                                        <{/foreach}>

                                        <{if $guideline_text}>
                                            <div class="alert alert-info"><{$guideline_text|escape|nl2br}></div>
                                        <{/if}>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <{/if}>
                    <div class="row-fluid ldr-trn-row">
                        <div class="span12 ldr-trn-block">
                            <label class="control-label">
                                <strong>実施上の留意点</strong>
                            </label>
                            <div class="ldr-uneditable input-large-modal"><{$mst.notes|escape|nl2br|default:'(未設定)'}></div>
                        </div>
                    </div>

                    <div class="row-fluid ldr-trn-row">
                        <div class="span6 ldr-trn-block">
                            <label class="control-label">
                                <strong>企画担当</strong>
                            </label>
                            <div class="ldr-uneditable input-large-modal">
                                <{if $mst.planner}>
                                    <div><{$mst.planner|escape|default:'(未設定)'}></div>
                                <{/if}>
                                <{foreach from=$mst.emp_planner key=id item=name}>
                                    <div class="label label-info"><{$name|escape}></div>
                                <{/foreach}>
                            </div>
                        </div>
                        <div class="span6 ldr-trn-block">
                            <label class="control-label">
                                <strong>講師</strong>
                            </label>
                            <div class="ldr-uneditable input-large-modal">
                                <{if $mst.coach}>
                                    <div><{$mst.coach|escape|default:'(未設定)'}></div>
                                <{/if}>
                                <{foreach from=$mst.emp_coach key=id item=name}>
                                    <div class="label label-info"><{$name|escape}></div>
                                <{/foreach}>
                            </div>
                        </div>
                    </div>

                    <{if $mst.date.type === '1'}>
                        <div class="row-fluid ldr-trn-row">
                            <div class="span6 ldr-trn-block">
                                <label class="control-label">
                                    <strong>開催日時</strong>
                                </label>
                                <div class="ldr-uneditable input-large-modal">
                                    <{$mst.date.training_date|date_format_jp:"%Y年%-m月%-d日(%a)"}> <{$mst.date.training_start_time|date_format:"%R"}> 〜 <{$mst.date.training_last_time|date_format:"%R"}>
                                </div>
                            </div>
                            <div class="span6 ldr-trn-block">
                                <label class="control-label">
                                    <strong>開催場所</strong>
                                </label>
                                <div class="ldr-uneditable input-large-modal">
                                    <{$mst.date.place|escape}>
                                </div>
                            </div>
                        </div>
                    <{/if}>
                    <form id="form_edit" method="post">
                        <input type="hidden" name="training_id" value="<{$trn.training_id|escape}>" />

                        <div class="btn-toolbar">
                            <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                            <{if !$view}>
                                <button type="submit" name="btn_save" class="js-ot-btn js-submit-approve btn btn-small btn-primary" value="save"><i class="icon-edit"></i> 承認する</button>
                                <button type="submit" name="btn_save" id="btn_save" class="js-ot-btn js-submit-withdraw btn btn-small btn-danger" value="cancel"><i class="icon-remove-sign"></i> 取り下げる</button>
                            <{/if}>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
    </body>
</html>
