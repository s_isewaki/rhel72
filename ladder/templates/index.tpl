<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー</title>
        <!--[if (gt IE 7)|!(IE)]><!-->
        <script type="text/javascript">
            function openDashboard() {
                var h = window.screen.availHeight - 35;
                var w = window.screen.availWidth - 10;
                var option = "scrollbars=yes,resizable=yes,location=no,toolbar=no,menubar=no,status=no,left=0,top=0,width=" + w + ",height=" + h;
                var win = window.open('dashboard.php', 'Ladder', option);

                var userAgent = window.navigator.userAgent.toLowerCase();
                if (userAgent.indexOf('chrome') !== -1) {
                    win.moveTo(0, 0);
                    win.resizeTo(window.screen.availWidth, window.screen.availHeight);
                }
            }
            openDashboard();
        </script>
        <!--<![endif]-->
    </head>

    <body>
        <div class="container">
            <!--[if lte IE 7.0]>
            <strong>IE8以上を使用してください。<strong>
            <![endif]-->
            <!--[if (gt IE 7)|!(IE)]><!-->
            <button
                type="button"
                onclick="openDashboard();"
                class="btn btn-success btn-lg"
                style="margin-top: 30px;"
                >キャリア開発ラダーを開く</button>
            <!--<![endif]-->
        </div>

        <{include file="__js.tpl"}>
    </body>

</html>
