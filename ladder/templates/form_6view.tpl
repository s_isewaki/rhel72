<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | <{$ldr.config.form_6.title|escape}></title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title=$ldr.config.form_6.title hd_sub_title=$ldr.config.form_6.detailL}>

            <div class="container-fluid">
                <div class="row-fluid">

                    <{*印刷ボタン*}>
                    <form id="form_print" method="post" class="pull-right">
                        <input type="hidden" name="mode" value="" />
                        <input type="hidden" name="menu" value="" />
                        <input type="hidden" name="id" value="" />
                        <input type="hidden" name="target_emp_id" value=""/>
                        <input type="hidden" name="owner_url" value="" />
                        <{if $smarty.post.target_emp_id === $emp->emp_id()}>
                            &nbsp;<button type="button" data-id="<{$smarty.post.id}>" data-emp="<{$smarty.post.target_emp_id}>" data-php="form_6edit.php" data-mode="edit" class="js-form-button btn btn-danger btn-small"><i class="icon-edit"></i> 編集する</button>
                        <{/if}>
                        &nbsp;<button type="button" data-id="<{$smarty.post.id}>" data-emp="<{$smarty.post.target_emp_id}>" data-php="form_6view.php" data-target="form6" class="js-form-print btn btn-primary btn-small"><i class="icon-print"></i> 印刷する</button>
                    </form>

                    <{*様式共通ボタン*}>
                    <form id="form_edit" method="post">
                        <{include file='__form_view.tpl' op_srh='false' tmp_id='form_6view'}>
                    </form>

                    <div class="well well-small">
                        <div class="control-group">
                            <div class="input-prepend" style="margin-bottom:0;">
                                <span class="add-on">申請レベル</span>
                                <span class="ldr-uneditable input-mini ldr-level" style="margin-bottom:0;"><{$apply->level_text()|escape}></span>
                            </div>
                            <div class="input-prepend" style="margin-bottom:0;">
                                <span class="add-on">申請者</span>
                                <span class="ldr-uneditable" style="margin-bottom:0;"><{$target_emp->emp_name()|escape}> (<{$target_emp->class_full_name()|escape}>)</span>
                            </div>
                        </div>
                        <hr/>

                        <div class="control-group">
                            <label class="control-label">
                                <strong>評価日</strong>
                            </label>
                            <div class="controls">
                                <span class="ldr-uneditable input-medium"><{$form6.updated_on|date_format_jp:"%Y年%-m月%-d日(%a)"}></span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                <strong>事例内容</strong>
                            </label>
                            <div class="controls">
                                <span class="input-maxlarge ldr-uneditable"><{$form6.narrative|escape|nl2br}></span>
                            </div>
                        </div>
                    </div>

                    <div class="btn-toolbar">
                        <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                    </div>
                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
    </body>
</html>