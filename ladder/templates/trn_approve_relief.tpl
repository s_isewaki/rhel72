<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 救護員研修承認</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='救護員研修承認' hd_sub_title=''}>

            <div class="container-fluid">
                <form id="relief_form" method="post">

                    <{if $completed === "save"}>
                        <div class="alert alert-success fade in js-alert-message">
                            <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>
                            承認しました
                        </div>
                    <{elseif $completed === "cancel"}>
                        <div class="alert alert-success fade in js-alert-message">
                            <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>
                            取り下げました
                        </div>
                    <{/if}>
                    <div class="control-group">
                        <span class="form-inline">
                            <select name="srh_relief" id="srh_outside" class="srh input-xlarge">
                                <option value="">すべて</option>
                                <{foreach item=row from=$list_relief}>
                                    <{assign var="rf" value=`$row.relief_id`_`$row.unit`}>}>
                                    <option value="<{$rf}>" <{if $smarty.post.srh_relief===$rf}>selected<{/if}> ><{$row.subject|escape}> <{$roman_num[$row.unit]|escape}></option>
                                <{/foreach}>
                            </select>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <label class="control-label">
                                <input type="checkbox" class="srh" id="srh_nonapprove" name="srh_nonapprove" value="1" <{if $smarty.post.srh_nonapprove}>checked<{/if}>/>
                                <strong>承認が必要な申請のみ表示する</strong>
                            </label>
                        </span>
                    </div>
                    <div class="control-group form-inline">
                        <{include file="__datepicker.tpl" date_id="srh_start_date" date_name="srh_start_date" date_value=$smarty.post.srh_start_date|escape}>
                        <span>〜</span>
                        <{include file="__datepicker.tpl" date_id="srh_last_date" date_name="srh_last_date" date_value=$smarty.post.srh_last_date|escape}>
                        &nbsp;
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-user-md"></i></span>
                            <input type="text" id="srh_keyword" name="srh_keyword" class="input-large" value="<{$smarty.post.srh_keyword}>" placeholder="職員名を入力してください">
                        </div>
                        <button type="submit" id="search" name="search" value="search" class="btn btn-small btn-primary"><i class="icon-search"></i> 検索する</button>
                    </div>
                    <{if $list}>
                        <{include file="__pager.tpl"}>
                        <table class="table table-bordered table-hover table-condensed ldr-table">
                            <thead>
                                <tr>
                                    <th>開催日</th>
                                    <th>支部名</th>
                                    <th>施設名</th>
                                    <th>科目</th>
                                    <th>教科内容</th>
                                    <th>氏名</th>
                                    <th width="35px"><button type="button" id="js-btn-training" class="btn btn-mini"><i class="icon-check-sign"></i></button></th>
                                    <th>承認</th>
                                </tr>
                            </thead>
                            <tbody>
                                <{foreach item=row from=$list}>
                                    <tr>
                                        <td class="ldr-td-date"><{$row.training_day|date_format_jp:"%Y年%-m月%-d日(%a)"}></td>
                                        <td class="ldr-td-text-center"><{$row.branch|escape}></td>
                                        <td class="ldr-td-text-center"><{$row.hospital|escape}></td>
                                        <td class="ldr-td-text-center">
                                            <{$row.subject|escape}>　<span class="ldr-roman"><{$roman_num[$row.unit]|escape}></span>
                                        </td>
                                        <td class="ldr-td-text-center">
                                            <div
                                                <{if $row.detail}>
                                                    rel="popover" data-trigger="hover" data-html="true" data-placement="top" data-animation="true"
                                                    data-content="<div class='ldr-popover'><strong><{$row.detail|escape|nl2br}></strong></div>"
                                                <{/if}>
                                                >
                                                <{$row.contents|escape}>
                                            </div>
                                        </td>
                                        <td class="ldr-td-name"><{$row.name|escape}></td>
                                        <td class="ldr-td-name">
                                            <{if $row.status==='1'}>
                                                <label >
                                                    <input type="checkbox" class="ck-training" name="training_id[]" value="<{$row.training_id}>" />
                                                </label>
                                            <{/if}>
                                        </td>
                                        <td class="ldr-td-name">
                                            <{if $row.status==='1'}>
                                                <button type="button" data-php="trn_approve_relief_edit.php" data-mode="edit" data-id="<{$row.training_id}>" class="js-form-button btn btn-small btn-danger">承認</button>
                                            <{elseif $row.status < 0}>
                                                <span class="label label-important"><{$row.status_text}></span>
                                            <{elseif $row.status === '0'}>
                                                <span class="label label-success"><{$row.status_text}></span>
                                            <{else}>
                                                <span class="label label-info"><{$row.status_text}></span>
                                            <{/if}>
                                        </td>
                                    </tr>
                                <{/foreach}>
                            </tbody>
                        </table>
                        <div class="btn-toolbar">
                            <button type="submit" name="btn_save" class="js-ot-btn js-submit-approve btn btn-small btn-primary" value="save"><i class="icon-edit"></i> 承認する</button>
                            <button type="submit" name="btn_save" id="btn_save" class="js-ot-btn js-submit-withdraw btn btn-small btn-danger" value="cancel"><i class="icon-remove-sign"></i> 取り下げる</button>
                        </div>
                    <{else}>
                        <div class="control-group">
                        </div>
                        <div class="alert alert-error">承認待ちの申請はありません</div>
                    <{/if}>

                </form>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <{include form_selector="#relief_form" file="__pager_js.tpl"}>
        <script type="text/javascript">
            $(function() {
                check_status();

                $(".js-ot-btn").prop("disabled", true);
                $('#srh_keyword').keypress(function(ev) {
                    if ((ev.which && ev.which === 13) || (ev.keyCode && ev.keyCode === 13)) {
                        $('#search').trigger("click");
                    }
                });
                $(".srh").change(function() {
                    $(this).parents('form')
                        .append($('<input></input>').attr({type: 'hidden', name: 'search', value: 'true'}))
                        .submit();
                });
                $(".ck-training").change(function() {
                    check_status();
                });

                $('#js-btn-training').click(function() {
                    if ($(this).children('i').hasClass('icon-check-sign')) {
                        $('input.ck-training').prop('checked', true);
                    }
                    else {
                        $('input.ck-training').prop('checked', false);
                    }
                    $(this).children('i').toggleClass('icon-check-sign').toggleClass('icon-check-empty');
                    check_status();
                });

            });
            function check_status() {
                if ($(".ck-training:checked").length > 0) {
                    $(".js-ot-btn").prop("disabled", false);
                } else {
                    $(".js-ot-btn").prop("disabled", true);
                }
            }
        </script>

    </body>
</html>
