<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 救護員研修受付</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='救護員研修受付' hd_sub_title=''}>

            <div class="container-fluid">
                <form id="relief_form" method="post">
                    <div class="control-group form-inline">
                        <label class="control-label">
                            <input type="checkbox" id="srh_nonaccept" name="srh_nonaccept" value="1" class="srh"/>
                            <strong>受付が必要な講座のみ表示する</strong>
                        </label>
                    </div>
                    <div class="control-group form-inline">
                        <{include file="__datepicker.tpl" date_id="srh_start_date" date_name="srh_start_date" date_value=$srh_start_date|escape}>
                        <span>〜</span>
                        <{include file="__datepicker.tpl" date_id="srh_last_date" date_name="srh_last_date" date_value=$srh_last_date|escape}>
                        &nbsp;
                        <input type="text" id="srh_keyword" name="srh_keyword" class="input-large" value='<{$srh_keyword|escape}>' placeholder="検索ワードを入力してください" />
                        <button type="submit" id="btn_search" name="btn_search" value="search" class="btn btn-small btn-primary"><i class="icon-search"></i> 検索する</button>
                    </div>
                    <{if $list}>
                        <{include file="__pager.tpl"}>
                        <table class="table table-bordered table-hover table-condensed ldr-table">
                            <thead>
                                <tr>
                                    <th>開催日</th>
                                    <th>支部名</th>
                                    <th>施設名</th>
                                    <th>科目</th>
                                    <th>教科内容</th>
                                    <th>申込人数</th>
                                    <th>印刷</th>
                                    <th>受付</th>
                                    <th>報告</th>
                                </tr>
                            </thead>
                            <tbody>
                                <{foreach item=row from=$list}>
                                    <tr>
                                        <td class="ldr-td-date"><{$row.training_day|date_format_jp:"%Y年%-m月%-d日(%a)"}></td>
                                        <td class="ldr-td-text-center"><{$row.branch|escape}></td>
                                        <td class="ldr-td-text-center"><{$row.hospital|escape}></td>
                                        <td class="ldr-td-text-center">
                                            <{$row.subject|escape}>　<span class="ldr-roman"><{$roman_num[$row.unit]|escape}></span>
                                        </td>
                                        <td class="ldr-td-text-center">
                                            <div
                                                <{if $row.detail}>
                                                    rel="popover" data-trigger="hover" data-html="true" data-placement="top" data-animation="true"
                                                    data-content="<div class='ldr-popover'><strong><{$row.detail|escape|nl2br}></strong></div>"
                                                <{/if}>
                                                >
                                                <{$row.contents|escape}>
                                            </div>
                                            <td class="ldr-td-name">
                                                <div rel="popover" data-trigger="hover" data-html="true" data-placement="top" data-animation="true"
                                                     <{if $row.count > 0}>
                                                         data-content="
                                                         <div class='ldr-popover label label-important'>受付待: <strong><{$row.count_2|escape}></strong>名</div><br/>
                                                         <div class='ldr-popover label label-warning'>取下げ: <strong><{$row.count_3c|escape}></strong>名</div><br/>
                                                         <div class='ldr-popover label label-info'>受付済: <strong><{$row.count_3|escape}></strong>名</div><br/>
                                                         <div class='ldr-popover label label-success'>報告済: <strong><{$row.count_0|escape}></strong>名</div><br/>"
                                                     <{/if}>
                                                     >
                                                    <{$row.count|escape}>名
                                                    <{if $row.count_2 > 0}>
                                                        <br/><span class="label label-important">受付待 <{$row.count_2|escape}>名</span>
                                                    <{elseif $row.count > 0 and $row.count_2 === '0'}>
                                                        <{if $row.count_0 > 0}>
                                                            <br/><span class="label label-success">報告済 <{$row.count_0|escape}>名</span>
                                                        <{else}>
                                                            <br/><span class="label label-info">受付待 <{$row.count_2|escape}>名</span>
                                                        <{/if}>
                                                    <{/if}>
                                                </div>
                                            </td>
                                            <td class="ldr-td-name">
                                                <a href="javascript:void(0);" name="btn_print" data-target="relief_print" data-form="new" data-id="<{$row.relief_id|escape}>_<{$row.unit|escape}>_<{$row.training_day|escape}>" data-branch="<{$row.branch|escape}>" data-hospital="<{$row.hospital|escape}>" data-php="trn_accept_relief.php" class="js-relief-print i_link"><i class="icon-print"></i></a>
                                            </td>
                                            <td class="ldr-td-name">
                                                <a href="javascript:void(0);" data-php="trn_accept_relief_edit.php" data-mode="edit" data-id="<{$row.relief_id|escape}>_<{$row.unit|escape}>_<{$row.training_day|escape}>" data-branch="<{$row.branch|escape}>" data-hospital="<{$row.hospital|escape}>" class="js-relief-button i_link"><i class="icon-cog"></i></a>
                                            </td>
                                            <td class="ldr-td-name">
                                                <a href="javascript:void(0);" data-php="trn_accept_relief_report.php" data-mode="view" data-id="<{$row.relief_id|escape}>_<{$row.unit|escape}>_<{$row.training_day|escape}>" data-branch="<{$row.branch|escape}>" data-hospital="<{$row.hospital|escape}>" class="js-relief-button i_link"><i class="icon-file-text"></i></a>
                                            </td>
                                    </tr>
                                <{/foreach}>
                            </tbody>
                        </table>
                    <{else}>
                        <div class="alert alert-error">院外研修はありません</div>
                    <{/if}>
                </form>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <{include form_selector="#relief_form" file="__pager_js.tpl"}>
        <script type="text/javascript">
            $('#srh_keyword').keypress(function(ev) {
                if ((ev.which && ev.which === 13) || (ev.keyCode && ev.keyCode === 13)) {
                    $('#btn_search').trigger("click");
                }
            });
            $('.srh').change(function() {
                $('#relief_form')
                    .append($('<input></input>').attr({type: 'hidden', name: 'btn_search', value: 'on'}))
                    .submit();
            });
            $('.js-relief-button').click(function() {
                $(this).parents('form')
                    .attr({method: 'post', action: $(this).data('php')})
                    .append($('<input></input>').attr({type: 'hidden', name: 'id', value: $(this).data('id')}))
                    .append($('<input></input>').attr({type: 'hidden', name: 'branch', value: $(this).data('branch')}))
                    .append($('<input></input>').attr({type: 'hidden', name: 'hospital', value: $(this).data('hospital')}))
                    .append($('<input></input>').attr({type: 'hidden', name: 'mode', value: $(this).data('mode')}))
                    .append($('<input></input>').attr({type: 'hidden', name: 'target_emp_id', value: $(this).data('emp')}))
                    .submit();
            });
            $('.js-relief-print').click(function() {
                $("#_js-print").remove();
                var form = $('<form id="_js-print" class="js-form-print"></form>');
                form.appendTo(document.body);

                form.append($('<input></input>').attr({type: 'hidden', name: 'id', value: $(this).data('id')}))
                    .append($('<input></input>').attr({type: 'hidden', name: 'branch', value: $(this).data('branch')}))
                    .append($('<input></input>').attr({type: 'hidden', name: 'hospital', value: $(this).data('hospital')}))
                    .append($('<input></input>').attr({type: 'hidden', name: 'mode', value: 'print'}))
                    .append($('<input></input>').attr({type: 'hidden', name: 'target_emp_id', value: $(this).data('emp')}));
                form.attr({
                    method: 'post',
                    action: $(this).data('php'),
                    target: $(this).data('target'),
                    onsubmit: 'return openPDF(this)'});
                form.submit();
            });
        </script>
    </body>
</html>
