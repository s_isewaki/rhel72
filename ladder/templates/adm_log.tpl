<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>
        <title>CoMedix キャリア開発ラダー | システムログ</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>
        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title="システムログ"}>

            <div class="container-fluid">
                <div class="row-fluid">
                    <form id="search" class="form" method="POST">

                        <div class="control-group">
                            <div class="form-inline">
                                <{include file="__datepicker.tpl" date_id="srh_start_date" date_name="srh_start_date" date_value=''}>
                                〜
                                <{include file="__datepicker.tpl" date_id="srh_last_date" date_name="srh_last_date" date_value=''}>
                                <div class="input-prepend">
                                    <span class="add-on"><i class="icon-user-md"></i></span>
                                    <input type="text" id="srh_name" name="srh_name" class="input-medium" value='<{$srh_name|escape}>' placeholder="職員ID, 職員名" />
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <div class="form-inline">
                                <select name="srh_function">
                                    <option value="">すべて</option>
                                    <{foreach key=value item=name from=$logfunction}>
                                        <option value="<{$value|escape}>"><{$name|escape}></option>
                                    <{/foreach}>
                                </select>
                                <button type="submit" name="btn_search" value="search" class="btn btn-small btn-primary"><i class="icon-search"></i> 検索</button>
                            </div>
                        </div>
                </div>

                <div class="well well-small">
                    <{if $list}>
                        <{include file="__pager.tpl"}>
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th nowrap>日時</th>
                                    <th nowrap>職員ID</th>
                                    <th nowrap>職員名</th>
                                    <th nowrap>機能</th>
                                    <th nowrap>ログ</th>
                                    <th nowrap>パラメータ</th>
                                </tr>
                            </thead>
                            <tbody>
                                <{foreach item=row from=$list}>
                                    <tr>
                                        <td nowrap><{$row.timestamp|date_format_jp:"%Y年%-m月%-d日(%a) %R"}></td>
                                        <td nowrap><{$row.emp_personal_id|escape}></td>
                                        <td nowrap><{$row.emp_name|escape}></td>
                                        <td nowrap><{$logfunction[$row.function]|default:$row.function|escape}></td>
                                        <td nowrap><{$row.message|escape}></td>
                                        <td><{$row.param|escape}></td>
                                    </tr>
                                <{/foreach}>
                            </tbody>
                        </table>
                    <{else}>
                        <div class="alert alert-error">ログがありません</div>
                    <{/if}>
                </div>
                </form>
            </div>
        </div>
        </div>

        <{include file="__js.tpl"}>
        <{include form_selector="#search" file="__pager_js.tpl"}>
    </body>
</html>