<div class="navbar">
    <div class="navbar-inner">
        <ul class="nav pull-right">

            <li id="fat-menu" class="dropdown">
                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">
                    <{foreach from=$emp->ladders() item=ldr}>
                        <{if $emp->current_level($ldr.ladder_id, 'level') === '0'}>
                            <span class="badge badge-info"><{$ldr.short|escape}><{$ldr.config.level0|default:0|escape}></span>
                        <{elseif $emp->current_level($ldr.ladder_id, 'level') === '1'}>
                            <span class="badge badge-info"><{$ldr.short|escape}><span class="ldr-level">I</span></span>
                        <{elseif $emp->current_level($ldr.ladder_id, 'level') === '2'}>
                            <span class="badge badge-inverse"><{$ldr.short|escape}><span class="ldr-level">II</span></span>
                        <{elseif $emp->current_level($ldr.ladder_id, 'level') === '3'}>
                            <span class="badge badge-success"><{$ldr.short|escape}><span class="ldr-level">III</span></span>
                        <{elseif $emp->current_level($ldr.ladder_id, 'level') === '4'}>
                            <span class="badge badge-warning"><{$ldr.short|escape}><span class="ldr-level">IV</span></span>
                        <{elseif $emp->current_level($ldr.ladder_id, 'level') === '5'}>
                            <span class="badge badge-important"><{$ldr.short|escape}><span class="ldr-level">V</span></span>
                        <{elseif $emp->current_level($ldr.ladder_id, 'level') === '6'}>
                            <span class="badge badge-important"><{$ldr.short|escape}><span class="ldr-level">VI</span></span>
                        <{/if}>
                    <{foreachelse}>
                        <i class="icon-user-md"></i>
                    <{/foreach}>
                    <{$emp->emp_name()|escape}>
                    <i class="icon-caret-down"></i>
                </a>

                <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">
                    <{if $emp->is_admin()}>
                        <li><a tabindex="-1" href="#" class="text-right"><strong>�����������</strong></a>
                        </li>
                    <{/if}>
                    <{if $emp->is_approver()}>
                        <li><a tabindex="-1" href="#" class="text-right"><strong><{$titles.approver|default:"������ǧ��"|escape}></strong></a>
                        </li>
                    <{/if}>
                    <{if $emp->committee()}>
                        <li><a tabindex="-1" href="#" class="text-right"><strong><{$titles.committee|default:"ǧ��Ѱ�"|escape}></strong></a>
                        </li>
                    <{/if}>
                    <{if $emp->is_inside()}>
                        <li><a tabindex="-1" href="#" class="text-right"><strong>���⸦��ô��</strong></a>
                        </li>
                    <{/if}>
                    <{if $emp->is_outside()}>
                        <li><a tabindex="-1" href="#" class="text-right"><strong>��������ô��</strong></a>
                        </li>
                    <{/if}>
                    <{if $emp->is_relief()}>
                        <li><a tabindex="-1" href="#" class="text-right"><strong>�߸������ô��</strong></a>
                        </li>
                    <{/if}>
                    <{if $emp->is_training()}>
                        <li><a tabindex="-1" href="#" class="text-right"><strong>����������</strong></a>
                        </li>
                    <{/if}>
                    <{if $emp->is_analysis()}>
                        <li><a tabindex="-1" href="#" class="text-right"><strong>ʬ��ô��</strong></a>
                        </li>
                    <{/if}>
                    <{if !$emp->is_admin() && !$emp->is_approver() && !$emp->committee() && !$emp->is_inside() && !$emp->is_outside() && !$emp->is_relief() && !$emp->is_analysis()}>
                        <li><a tabindex="-1" href="#" class="text-right"><strong>����</strong></a>
                        </li>
                    <{/if}>
                    <li class="divider"></li>
                    <li><a tabindex="-1" href="#" class="text-right"><small><{$emp->class_full_name()|escape}></small></a></li>
                    <li><a tabindex="-1" href="#" class="text-right"><small><{$emp->st_name()|escape}></small></a></li>
                    <li><a tabindex="-1" href="#" class="text-right"><small><{$emp->job_name()|escape}></small></a></li>
                    <li class="divider"></li>
                    <li><a tabindex="-1" class="visible-phone" href="config_user.php">�桼������</a></li>

                    <{if $emp->emp_id() === '000000000000'}>
                        <li class="divider"></li>
                        <li><a tabindex="-1" href="#" class="text-right">Ver. 20150727</a>
                        </li>
                    <{/if}>
                </ul>
            </li>

        </ul>
        <a class="brand" href="dashboard.php"><i class="icon-hospital"></i> Career Ladder</a>
    </div>
</div>
