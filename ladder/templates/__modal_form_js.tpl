<script type="text/javascript">
    $(function() {
        $('.js-modal-save').click(function() {
            $('.control-group').removeClass('error');
            $('.help-block').hide();

            var form = $($(this).data('target'));
            var url = form.attr('action');
            var data = form.serialize();

            $(this).prop("disabled", true);
            // validation
            $.post(url, data, function(data) {
                if (data.error) {
                    $(".js-modal-save").prop("disabled", false);
                    $.each(data.msg, function(name, msg) {
                        $('#modal_' + data.mode + '_' + name).addClass('error');
                        $('#error_' + data.mode + '_' + name).text(msg).show();
                    });
                }
                else {
                    $('#mode2_' + data.mode).val('save');
                    if (form.find('#parent_form').val()) {
                        // 親のformのデータもsubmitしたい場合
                        parent_form = $(form.find('#parent_form').val());
                        form.find('input').each(function() {
                            parent_form.append($(this).clone());
                        });
                        parent_form.submit();
                    } else {
    <{if $ldr_ticket}>
                        form.append($('<input></input>').attr({type: 'hidden', name: 'ldr_ticket', value: '<{$ldr_ticket}>'}));
    <{/if}>
                        form.submit();
                    }
                }
            }, 'json');
        });

    <{*display_with_fillによる上書き回避*}>
        $('#form_comment').find("#mode").val("comment");
    });
</script>