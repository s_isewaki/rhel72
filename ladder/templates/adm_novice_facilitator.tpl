<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 教育担当者設定</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title="教育担当者設定"}>

            <div class="container-fluid">
                <div class="row-fluid">
                    <{include file="__save_message.tpl" }>
                    <form class="form" id="form_novice" method="POST">

                        <p>新人の教育担当者を選択してください。</p>
                        <div id="alert" class="alert alert-danger hide"></div>

                        <div class="form-inline">
                            <select id="pulldown_year" name="year" class="input-medium js-pulldown-novice">
                                <{foreach item=asyear from=$year_list}>
                                    <option value="<{$asyear|escape}>"><{$asyear|escape}>年度</option>
                                <{/foreach}>
                            </select>
                            <div class="input-prepend">
                                <span class="add-on"><i class="icon-sitemap"></i> 部署</span>
                                <select name="srh_class3" class="input-large js-pulldown-novice" id="class3">
                                    <option value="">すべて</option>
                                    <{foreach item=row from=$class_lists}>
                                        <option class="js-class-select select<{$row.class_id|escape}>_<{$row.atrb_id|escape}>" value="<{$row.dept_id|escape}><{if $row.room_id}>_<{$row.room_id|escape}><{/if}>">
                                            <{$row.dept_nm|escape}><{if $row.room_id}> &gt; <{$row.room_nm|escape}><{/if}>
                                        </option>
                                    <{/foreach}>
                                </select>
                            </div>
                            <a href="#upload" data-toggle="modal" class="btn btn-small btn-danger ldr_upload"><i class="icon-cloud-upload"></i> CSVから職員をインポートする</a>
                        </div>
                        <hr/>

                        <{if $facilitate_list}>
                            <table class="table table-striped table-hover <{if ! $apr_id}>}hide<{/if}>">
                                <thead>
                                    <tr>
                                        <th>新人職員</th>
                                        <th>部署</th>
                                        <th>教育担当</th>
                                    </tr>
                                </thead>
                                <tbody id="emp_list">
                                    <{foreach item=row from=$facilitate_list}>
                                        <tr>
                                            <td nowrap><{$row.name|escape}></td>
                                            <td nowrap>
                                                <{$row.emp->class_full_name()|escape}>
                                            </td>
                                            <td id="e<{$row.emp_id|escape}>">
                                                <{foreach from=$row.facilitators item=facilitator}>
                                                    <div id="facilitate<{$facilitator.emp_id|escape}>" class="label label-info">
                                                        <{$facilitator.name|escape}>
                                                        <input type="hidden" name="facilitate_emp_id[<{$row.emp_id|escape}>][]" value="<{$facilitator.emp_id|escape}>" />
                                                        <a href="javascript:void(0);" class="ldr-emp-remove js-emp-remove">&times;</a>
                                                    </div>
                                                <{/foreach}>
                                            </td>
                                            <td>
                                                <button type="button" class="btn btn-small btn-success js-btn-emplist" data-id="<{$row.emp_id|escape}>"><i class="icon-book"></i> 担当者の追加</button>
                                            </td>
                                        </tr>
                                    <{/foreach}>
                                </tbody>
                            </table>

                            <{foreach from=$error.error item=msg}>
                                <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                            <{/foreach}>

                            <div class="btn-toolbar">
                                <button type="submit" name="btn_save" id="btn_save" class="btn btn-small btn-primary" value="btn_save"><i class="icon-save"></i> 保存する</button>
                            </div>
                        <{else}>
                            <p>新人職員を先に設定してください</p>
                        <{/if}>
                    </form>
                </div>
            </div>
        </div>
        <{capture name=layout}>
            <label class="control-label"><strong>インポート方法</strong></label>
            <div class="control-group">
                <div class="controls">
                    <label class="radio"><input type="radio" name="importmode" value="1" checked="checked" />登録済みに追加</label>
                    <label class="radio"><input type="radio" name="importmode" value="2" />登録済みを全て削除して入れ直し</label>
                </div>
            </div>
            <hr/>
            <label class="control-label"><strong>CSVレイアウト</strong></label>
            <table class="table table-condensed table-bordered table-striped ldr-table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>名称</th>
                        <th>必須</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="ldr-td-number">1</td>
                        <td>新人職員ID</td>
                        <td>必須</td>
                    </tr>
                    <tr>
                        <td class="ldr-td-number">2</td>
                        <td>新人職員名(識別用)</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="ldr-td-number">3</td>
                        <td>教育担当職員ID</td>
                        <td>必須</td>
                    </tr>
                    <tr>
                        <td class="ldr-td-number">4</td>
                        <td>教育担当職員名(識別用)</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="ldr-td-number"></td>
                        <td colspan="2">以降、教育担当者が複数いる場合は「職員ID」「職員名」を続けてください</td>
                    </tr>
                </tbody>
            </table>
            <hr/>
            <label class="control-label"><strong>CSV例</strong></label>
            <table class="table table-condensed table-bordered ldr-table">
                <tbody>
                    <tr>
                        <td>123001</td>
                        <td>新人 Ａ子</td>
                        <td>456001</td>
                        <td>教育 Ｃ子</td>
                        <td colspan="4"></td>
                    </tr>
                    <tr>
                        <td>123002</td>
                        <td>田中 Ｂ子</td>
                        <td>456002</td>
                        <td>担当 Ｄ子</td>
                        <td>456003</td>
                        <td>山本 Ｅ子</td>
                        <td>456004</td>
                        <td>鈴木 Ｆ子</td>
                    </tr>
                </tbody>
            </table>
        <{/capture}>

        <{include file="__js.tpl"}>
        <{include up_title="CSVから職員をインポートする" up_mode="import" file_nav="CSVファイル" up_php="adm_novice_facilitator.php?year=`$year`" file="__fileupload.tpl" layout=$smarty.capture.layout}>
        <script type="text/javascript">
            $(function() {
                // プルダウン選択
                $('.js-pulldown-novice').change(function() {
                    $('#form_novice').submit();
                });

                // 職員削除
                $('body').on('click', '.js-emp-remove', function() {
                    $(this).parent().remove();
                });

                /**
                 * アップロード閉じるイベント(画面リロード)
                 */
                $('#upload').on('hidden', function() {
                    $('<form action="" method="post"></form>')
                        .append($('<input></input>').attr({type: 'hidden', name: 'year', value: $('#pulldown_year').val()}))
                        .append($('<input></input>').attr({type: 'hidden', name: 'srh_class3', value: $('#class3').val()}))
                        .appendTo(document.body)
                        .submit()
                        .remove();
                });
            });

            // 職員の追加
            function add_target_list(item_id, emp_id, emp_name) {
                var emp = emp_id.split(", ");
                var empname = emp_name.split(", ");

                if (emp.length !== 1) {
                    return;
                }

                // 重複チェック
                var flg = false;
                $('input[name="facilitate_emp_id[' + item_id + '][]"]').each(function(idx, e) {
                    if ($(e).val() === emp[0]) {
                        flg = true;
                        return;
                    }
                });
                if (flg) {
                    return;
                }

                if ($('#e' + emp_id).size() !== 0) {
                    alert('新人職員を教育担当者に設定することはできません');
                    return;
                }

                var html = [
                    '<div id="facilitate' + emp[0] + '" class="label label-info">',
                    empname[0],
                    '<input type="hidden" name="facilitate_emp_id[' + item_id + '][]" value="' + emp[0] + '" /> ',
                    '<a href="javascript:void(0);" class="ldr-emp-remove js-emp-remove">&times;</a></div> '
                ].join('');
                $('#e' + item_id).append(html);
            }
        </script>
    </body>
</html>
