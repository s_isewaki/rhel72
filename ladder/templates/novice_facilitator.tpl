<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | <{$titles.novice|escape}>: 他者評価</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title="`$titles.novice`: 他者評価" hd_sub_title=''}>

            <div class="container-fluid">
                <div class="row-fluid">
                    <form method="post" action="" id="form_facilitator" class="form-inline">
                        <select id="pulldown_year" name="year" class="input-medium js-pulldown-year">
                            <{foreach item=year from=$year_list}>
                                <option value="<{$year|escape}>"><{$year|escape}>年度</option>
                            <{/foreach}>
                        </select>
                    </form>

                    <{if $list}>
                        <table class="table table-hover table-condensed table-striped ldr-table">
                            <thead>
                                <tr>
                                    <th>担当新人</th>
                                        <{foreach item=d from=$date_list}>
                                        <th>
                                            第<{$d.number|escape}>回<br/><{$d.last_date|date_format_jp:"%Y年%-m月"}>
                                        </th>
                                    <{/foreach}>
                                </tr>
                            </thead>
                            <tbody>
                                <{foreach item=data key=emp from=$list}>
                                    <tr>
                                        <td class="ldr-td-name"><a href="javascript:void(0);" class="js-novice-sheet" data-emp="<{$emp|escape}>"><{$data.emp_name|escape}></a></td>
                                            <{foreach item=d from=$date_list}>
                                            <td class="ldr-td-name">
                                                <{if $data[$d.number].guideline === $data[$d.number].assessment}>
                                                    <button class="btn btn-info btn-medium js-novice-facilitate" type="button" data-emp="<{$emp|escape}>" data-number="<{$d.number|escape}>">評価する</button>
                                                <{elseif $data[$d.number].assessment === '0'}>
                                                    本人未評価
                                                <{else}>
                                                    本人評価中<br/>(最終更新: <{$data[$d.number].lastupdate|date_format_jp:"%-m月%-d日(%a) %H:%M"}>)
                                                <{/if}>
                                            </td>
                                        <{/foreach}>
                                    </tr>
                                <{/foreach}>
                            </tbody>
                        </table>

                    <{else}>
                        <div class="alert alert-error">評価する新人職員はありません</div>
                    <{/if}>
                </div>
            </div>
        </div>

        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {
                // プルダウン選択
                $('.js-pulldown-year').change(function() {
                    $('#form_facilitator').submit();
                });

                // 評価する
                $('.js-novice-facilitate').click(function() {
                    $('#form_facilitator')
                        .attr({method: 'post', action: 'novice_facilitate_assessment.php'})
                        .append($('<input></input>').attr({type: 'hidden', name: 'emp_id', value: $(this).data('emp')}))
                        .append($('<input></input>').attr({type: 'hidden', name: 'number', value: $(this).data('number')}))
                        .submit();
                });

                // 評価シート
                $('.js-novice-sheet').click(function() {
                    $('<form action="novice_sheet.php" method="post"></form>')
                        .append($('<input></input>').attr({type: 'hidden', name: 'emp_id', value: $(this).data('emp')}))
                        .append($('<input></input>').attr({type: 'hidden', name: 'year', value: $('#pulldown_year').val()}))
                        .appendTo(document.body)
                        .submit();
                });
            });
        </script>
    </body>
</html>