<{if $completed}>
    <div class="alert alert-success fade in js-alert-message">
        <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>

        <{if $msg_text}>
            <{$msg_text}>
        <{else}>
            保存しました
        <{/if}>
    </div>
<{/if}>
