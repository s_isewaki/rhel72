<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | <{$titles.novice|escape}>: 他者評価</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title="`$titles.novice`: 他者評価" hd_sub_title=''}>

            <div class="container-fluid">
                <div class="row-fluid">
                    <div class="well well-small">
                        <b class="ldr-uneditable" style="margin-bottom:0;"><{$year|escape}>年度</b>
                        <b class="ldr-uneditable" style="margin-bottom:0;">第<{$number|escape}>回</b>
                        <div class="input-prepend" style="margin-bottom:0;">
                            <span class="add-on">評価対象</span>
                            <span class="ldr-uneditable input-medium" style="margin-bottom:0;"><{$novice->emp_name()|escape}></span>
                        </div>
                        <div class="input-prepend" style="margin-bottom:0;">
                            <span class="add-on">最終評価者</span>
                            <span class="ldr-uneditable input-large" style="margin-bottom:0;"><{if $facilitator}><{$facilitator->emp_name()|escape}> (<{$f_updated_on|date_format_jp:"%-m月%-d日 %R"}>)<{/if}></span>
                        </div>
                    </div>

                    <ul class="nav nav-tabs" id="group1Tab">
                        <{foreach item=row from=$group1_list}>
                            <li class="<{if $active === "g1-`$row.group1`"}>active<{/if}>"><a href="#g1-<{$row.group1|escape}>" data-toggle="tab"><i class="icon-folder-close"></i> <{$row.name|escape}></a></li>
                            <{/foreach}>
                    </ul>

                    <div class="tab-content">
                        <{foreach item=g1 from=$group1_list}>
                            <div class="tab-pane <{if $active === $g1.group1}>active<{else}>fade<{/if}>" id="g1-<{$g1.group1|escape}>">
                                <div class="well well-small">
                                    <form class="js-form-assessment" action="novice_facilitate_assessment.php" method="POST">
                                        <input type="hidden" name="save" value="ajax" />
                                        <input type="hidden" name="active" id="active" value="<{$g1.group1|escape}>" />
                                        <input type="hidden" name="year" value="<{$year|escape}>" />
                                        <input type="hidden" name="number" value="<{$number|escape}>" />
                                        <input type="hidden" name="emp_id" value="<{$emp_id|escape}>" />

                                        <{foreach item=g2 from=$group2_list[$g1.group1]}>
                                            <p><i class="icon-star"></i> <b><{$g2.name|escape}></b></p>

                                            <{foreach item=asm from=$assessment_list[$g1.group1][$g2.group2]}>
                                                <div class="ldr-indent">
                                                    <div class="alert alert-info">
                                                        <{$asm.guideline|escape}>
                                                    </div>
                                                    <div id="as-<{$asm.guideline_id|escape}>" class="inline">
                                                        <div class="pull-left">
                                                            本人評価：
                                                            <div class="btn-group">
                                                                <button type="button" class="ldr-level btn btn-medium btn-info" disabled="disabled">
                                                                    <{if $novice_number === '1'}>
                                                                        <{$roman_num[$asm.assessment]}>
                                                                    <{else}>
                                                                        <{$asm.assessment}>
                                                                    <{/if}>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix" style="margin: 0 0 0 150px;">
                                                        あなたの評価：
                                                        <div class="btn-group">

                                                            <button type="button"
                                                                    title="できる" rel="tooltip" data-placement="bottom"
                                                                    class="ldr-level btn btn-medium js-radio-assessment <{if $asm.f_assessment === '1'}>btn-danger active<{/if}>"
                                                                    name="assessment[<{$asm.guideline_id|escape}>]"
                                                                    data-input="#in-<{$asm.guideline_id|escape}>"
                                                                    value="1"
                                                                    ><{$nlv[1]}></button>
                                                            <button type="button"
                                                                    title="指導の下でできる" rel="tooltip" data-placement="bottom"
                                                                    class="ldr-level btn btn-medium js-radio-assessment <{if $asm.f_assessment === '2'}>btn-danger active<{/if}>"
                                                                    name="assessment[<{$asm.guideline_id|escape}>]"
                                                                    data-input="#in-<{$asm.guideline_id|escape}>"
                                                                    value="2"
                                                                    ><{$nlv[2]}></button>
                                                            <button type="button"
                                                                    title="演習でできる" rel="tooltip" data-placement="bottom"
                                                                    class="ldr-level btn btn-medium js-radio-assessment <{if $asm.f_assessment === '3'}>btn-danger active<{/if}>"
                                                                    name="assessment[<{$asm.guideline_id|escape}>]"
                                                                    data-input="#in-<{$asm.guideline_id|escape}>"
                                                                    value="3"
                                                                    ><{$nlv[3]}></button>
                                                            <button type="button"
                                                                    title="知識としてわかる" rel="tooltip" data-placement="bottom"
                                                                    class="ldr-level btn btn-medium js-radio-assessment <{if $asm.f_assessment === '4'}>btn-danger active<{/if}>"
                                                                    name="assessment[<{$asm.guideline_id|escape}>]"
                                                                    data-input="#in-<{$asm.guideline_id|escape}>"
                                                                    value="4"
                                                                    ><{$nlv[4]}></button>
                                                        </div>
                                                    </div>
                                                    <input type="hidden" id="in-<{$asm.guideline_id|escape}>" name="assessment[<{$asm.guideline_id|escape}>]" value="<{$asm.f_assessment|escape}>" />
                                                </div>
                                                <hr/>
                                            <{/foreach}>
                                        <{/foreach}>
                                        <div class="btn-toolbar">
                                            <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                                            <button type="submit" name="btn_save" class="btn btn-small btn-primary" value="btn_save"><i class="icon-save"></i> 保存する</button>
                                        </div>
                                        <p><small><i class="icon-exclamation-sign"></i> 入力内容は自動保存されます。明示的に保存したい場合は<b>[保存する]ボタン</b>で保存してください。</small></p>
                                    </form>
                                </div>
                            </div>
                        <{/foreach}>
                    </div>
                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {
                if ($('ul.nav-tabs').find('li.active').length === 0) {
                    $('ul.nav-tabs').find('a:first').tab('show');
                }

                // 自動保存
                $('form').autosave({
                    callbacks: {
                        trigger: 'change',
                        scope: 'all',
                        save: {
                            method: 'ajax',
                            options: {
                                url: 'novice_facilitate_assessment.php',
                                method: 'post'
                            }
                        }
                    }
                });

                $('body').on('click', 'button.js-radio-assessment', function() {
                    var input = $($(this).data('input'));
                    input.val($(this).attr('value')).change();

                    $(this).siblings().removeClass('btn-danger');
                    $(this).addClass('btn-danger');
                });
            });
        </script>
    </body>
</html>
