<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 名称設定</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='名称設定' hd_sub_title=''}>

            <div class="container-fluid">

                <{include file="__save_message.tpl"}>

                <form action="adm_item_setting.php" method="post">
                    <div class="well well-small">
                        <p>名称マスタの設定</p>
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <td>入力フィールド</td>
                                    <td>名称マスタを使う</td>
                                    <td>フリー入力を許可する</td>
                                    <!--
                                    <td>入力内容をマスタに追加する</td>
                                    -->
                                </tr>
                            </thead>
                            <tbody>
                                <{foreach from=$item->types() key=form item=fields}>
                                    <{foreach from=$fields key=field item=row}>
                                        <tr>
                                            <td nowrap><{$titles[$form].title|default:$row.form|escape}>: <{$row.field|escape}></td>
                                            <td nowrap><label class="checkbox"><input type="checkbox" name="item[<{$row.type|escape}>][use]" value="1" class="js-item-parent" <{if $items[$row.type].use}>checked="checked"<{/if}>/> 使う</label></td>
                                            <td nowrap><label class="checkbox"><input type="checkbox" name="item[<{$row.type|escape}>][free]" value="1" class="js-item-child" <{if !$items[$row.type].use}>disabled="disabled"<{/if}> <{if $items[$row.type].use && $items[$row.type].free}>checked="checked"<{/if}>/> 許可する</label></td>
                                            <{*
                                            <td nowrap><label class="checkbox"><input type="checkbox" name="item[<{$row.type|escape}>][add]" value="1" class="js-item-child" <{if !$items[$row.type].use}>disabled="disabled"<{/if}> <{if $items[$row.type].use && $items[$row.type].add}>checked="checked"<{/if}>/> 追加する</label></td>
                                            *}>
                                        </tr>
                                    <{/foreach}>
                                <{/foreach}>
                            </tbody>
                        </table>

                        <div class="btn-toolbar">
                            <button type="submit" name="btn_save" class="btn btn-small btn-primary" value="titles"><i class="icon-save"></i> 保存する</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {
                $('.js-item-parent').on('click', function() {
                    if ($(this).prop('checked') === false) {
                        $(this).parent().parent().parent().find('input.js-item-child').prop('checked', false).prop('disabled', true);
                    }
                    else {
                        $(this).parent().parent().parent().find('input.js-item-child').prop('disabled', false);
                    }
                });

                // 全てON
                $('#function-all-on').click(function() {
                    $('input[name="function[]"]').prop('checked', true);
                    $('input.func-record').prop('disabled', false);
                    $('input.func-training').prop('disabled', false);
                });

                // 全てOFF
                $('#function-all-off').click(function() {
                    $('input[name="function[]"]').prop('checked', false);
                    $('input.func-record').prop('disabled', true);
                    $('input.func-training').prop('disabled', true);
                });
            });
        </script>
    </body>
</html>
