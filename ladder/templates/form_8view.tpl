<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | <{$ldr.config.form_8.title|escape}></title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title=$ldr.config.form_8.title hd_sub_title=$ldr.config.form_8.detailL}>

            <div class="container-fluid">
                <div class="row-fluid">

                    <{*印刷ボタン*}>
                    <form id="form_print" method="post" class="pull-right">
                        <input type="hidden" name="mode" value="" />
                        <input type="hidden" name="menu" value="" />
                        <input type="hidden" name="id" value="" />
                        <input type="hidden" name="target_emp_id" value=""/>
                        <input type="hidden" name="owner_url" value="" />
                        <{if $apply->is_facilitator($emp->emp_id()) || $emp->committee()}>
                            &nbsp;<button type="button" data-id="<{$smarty.post.id}>" data-emp="<{$smarty.post.target_emp_id}>" data-php="form_8edit.php" data-mode="reedit" class="js-form-button btn btn-danger btn-small"><i class="icon-edit"></i> 編集する</button>
                        <{/if}>
                        &nbsp;<button type="button" data-id="<{$smarty.post.id}>" data-emp="<{$smarty.post.target_emp_id}>" data-php="form_8view.php" data-target="form8" class="js-form-print btn btn-primary btn-small"><i class="icon-print"></i> 印刷する</button>
                    </form>

                    <{*様式共通ボタン*}>
                    <form id="form_edit" method="post">
                        <{include file='__form_view.tpl' op_srh='false' tmp_id='form_8view'}>
                    </form>

                    <div class="well well-small">
                        <div class="control-group">
                            <div class="input-prepend" style="margin-bottom:0;">
                                <span class="add-on">申請レベル</span>
                                <span class="ldr-uneditable input-mini ldr-level" style="margin-bottom:0;"><{$apply->level_text()|escape}></span>
                            </div>
                            <div class="input-prepend" style="margin-bottom:0;">
                                <span class="add-on">申請者</span>
                                <span class="ldr-uneditable input-large" style="margin-bottom:0;"><{$target_emp->emp_name()|escape}> (<{$target_emp->class_full_name()|escape}>)</span>
                            </div>
                        </div>
                        <hr/>

                        <div class="control-group">
                            <label class="control-label">
                                <strong>評価日</strong>
                            </label>
                            <div class="controls">
                                <span class="ldr-uneditable input-medium"><{$form8.appraise_date|date_format_jp:"%Y年%-m月%-d日(%a)"}></span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                <strong>評価者</strong>
                            </label>
                            <div class="controls">
                                <{foreach from=$form1.appr_list item=row}>
                                    <span class="ldr-uneditable input-medium"><{$row.emp_name|escape}><{if $row.facilitator === 't'}> (司会者)<{/if}></span>
                                <{/foreach}>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                <strong><{if $ldr.config.rc === '1'}>臨床看護実践<{else}>赤十字<{/if}></strong>
                            </label>
                            <div class="controls">
                                <span class="ldr-uneditable input-maxlarge"><{$form8.clinical|escape|nl2br}></span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                <strong><{if $ldr.config.rc === '1'}>マネジメント<{else}>管理過程<{/if}></strong>
                            </label>
                            <div class="controls">
                                <span class="ldr-uneditable input-maxlarge"><{$form8.management|escape|nl2br}></span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                <strong><{if $ldr.config.rc === '1'}>教育・研究<{else}>意思決定<{/if}></strong>
                            </label>
                            <div class="controls">
                                <span class="ldr-uneditable input-maxlarge"><{$form8.education|escape|nl2br}></span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                <strong><{if $ldr.config.rc === '1'}>赤十字活動<{else}>質保証<{/if}></strong>
                            </label>
                            <div class="controls">
                                <span class="ldr-uneditable input-maxlarge"><{$form8.redcross|escape|nl2br}></span>
                            </div>
                        </div>
                        <{if $ldr.config.rc === '2'}>
                            <div class="control-group">
                                <label class="control-label">
                                    <strong>人材育成</strong>
                                </label>
                                <div class="controls">
                                    <span class="ldr-uneditable input-maxlarge"><{$form8.comment5|escape|nl2br}></span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    <strong>対人関係</strong>
                                </label>
                                <div class="controls">
                                    <span class="ldr-uneditable input-maxlarge"><{$form8.comment6|escape|nl2br}></span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    <strong>セルフマネジメント</strong>
                                </label>
                                <div class="controls">
                                    <span class="ldr-uneditable input-maxlarge"><{$form8.comment7|escape|nl2br}></span>
                                </div>
                            </div>
                        <{/if}>
                        <div class="control-group">
                            <label class="control-label">
                                <strong>総合レベル</strong>
                            </label>
                            <div class="controls">
                                <span class="ldr-uneditable input-maxlarge">
                                    <{if $form8.level === "1"}>
                                        あなたの<{if $ldr.config.rc === '1'}>看護実践<{else}>看護管理実践<{/if}>に対し申請レベルを認めます。
                                    <{elseif $form8.level === "2"}>
                                        今回の申請レベルは保留にします。再チャレンジしてください。
                                    <{/if}>
                                </span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                <strong>キャリアを高める目標</strong>
                            </label>
                            <div class="controls">
                                <span class="ldr-uneditable input-maxlarge"><{$form8.objective|escape|nl2br}></span>
                            </div>
                        </div>
                    </div>
                    <div class="btn-toolbar">
                        <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                    </div>
                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
    </body>
</html>