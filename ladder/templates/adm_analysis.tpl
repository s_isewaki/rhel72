<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix ����ꥢ��ȯ����� | ʬ��</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- �ᥤ�󥨥ꥢ -->
        <div class="content">
            <{include file="__header.tpl" hd_title='ʬ��' hd_sub_title=''}>

            <div class="container-fluid">
                <{include file="__save_message.tpl"}>
                <div class="row-fluid">
                    <ul class="nav nav-tabs">
                        <li class="<{if $active === 'class'}>active<{/if}>"><a href="#class" data-toggle="tab"><i class="icon-sitemap"></i> ʬ���о�����</a></li>
                        <li class="<{if $active === 'st'}>active<{/if}>"><a href="#st" data-toggle="tab"><i class="icon-user-md"></i> ʬ���о���</a></li>
                        <li class="<{if $active === 'job'}>active<{/if}>"><a href="#job" data-toggle="tab"><i class="icon-user-md"></i> ʬ���оݿ���</a></li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane <{if $active === 'class'}>active<{else}>fade<{/if}> in" id="class">
                            <form method="post" id="form_class">
                                <input type="hidden" id="approver_mode" name="mode" value="save" />
                                <input type="hidden" name="tab" value="class" />
                                <div class="well well-small" id="form_commission">
                                    <div class="form-group">
                                        <fieldset>
                                            <legend>ʬ���о�����</legend>
                                            <div class="control-group" >
                                                <select name="pulldown_class" id="pulldown_class" class="input-xlarge">
                                                    <option value="">����Ѥ�</option>
                                                    <{foreach item=row from=$class_pull_list}>
                                                        <option value="<{$row.class_id|escape}>-<{$row.atrb_id|escape}>"><{$row.class_nm|escape}> &gt; <{$row.atrb_nm|escape}></option>
                                                    <{/foreach}>
                                                </select>
                                            </div>
                                            <div class="control-group" >
                                                <table class="table table-striped table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>����</th>
                                                            <th><button type="button" id="js-btn-checkbox" class="btn btn-mini">
                                                                    <{if $smarty.post.pulldown_class}>
                                                                        <i class="icon-fixed-width icon-check-sign"></i>
                                                                    <{else}>
                                                                        <i class="icon-fixed-width icon-check-empty"></i>
                                                                    <{/if}>
                                                                </button> ����ʬ������
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <{foreach item=row from=$class_list}>
                                                            <tr>
                                                                <td>
                                                                    <{$row.class_full_name|escape}>
                                                                    <input type="hidden" name="class[]" value="<{$row.class_id|escape}>-<{$row.atrb_id|escape}>-<{$row.dept_id|escape}><{if $row.room_id}>-<{$row.room_id|escape}><{/if}>" />
                                                                </td>
                                                                <td>
                                                                    &nbsp;<input type="checkbox" class="ck-auditor" name="auth[<{$row.class_id|escape}>-<{$row.atrb_id|escape}>-<{$row.dept_id|escape}><{if $row.room_id}>-<{$row.room_id|escape}><{/if}>]" value="1" <{if $row.auth_flg==='t'}>checked<{/if}> />
                                                                </td>
                                                            </tr>
                                                        <{/foreach}>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="btn-toolbar">
                                                <button type="submit" name="btn_save" id="btn_save" class="btn btn-small btn-primary" value="class"><i class="icon-save"></i> ��¸����</button>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </form>

                        </div>

                        <div class="tab-pane <{if $active === 'st'}>active<{else}>fade<{/if}> in" id="st">
                            <form method="post">
                                <input type="hidden" name="mode" value="save" />
                                <input type="hidden" name="tab" value="st" />
                                <div class="well well-small" id="form_st">
                                    <div class="form-group">
                                        <fieldset>
                                            <legend>ʬ���о���</legend>
                                        </fieldset>
                                        <div class="control-group" >
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>��</th>
                                                        <th><button type="button" id="js-btn-checkbox_st" class="btn btn-mini">
                                                                <i class="icon-fixed-width icon-check-sign"></i>
                                                            </button> ��ʬ������
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <{foreach item=row from=$st_list}>
                                                        <tr>
                                                            <td>
                                                                <{$row.st_nm|escape}>
                                                                <input type="hidden" name="st[]" value="<{$row.st_id|escape}>" />
                                                            </td>
                                                            <td>
                                                                &nbsp;<input type="checkbox" class="ck-st" name="auth[<{$row.st_id|escape}>]" value="1" <{if $row.auth_flg==='t'}>checked<{/if}> />
                                                            </td>
                                                        </tr>
                                                    <{/foreach}>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="btn-toolbar">
                                            <button type="submit" name="btn_save" id="btn_save" class="btn btn-small btn-primary" value="st"><i class="icon-save"></i> ��¸����</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="tab-pane <{if $active === 'job'}>active<{else}>fade<{/if}> in" id="job">
                            <form method="post">
                                <input type="hidden" name="mode" value="save" />
                                <input type="hidden" name="tab" value="job" />
                                <div class="well well-small" id="form_job">
                                    <div class="form-group">
                                        <fieldset>
                                            <legend>ʬ���оݿ���</legend>
                                        </fieldset>
                                        <div class="control-group" >
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>����</th>
                                                        <th><button type="button" id="js-btn-checkbox_job" class="btn btn-mini">
                                                                <i class="icon-fixed-width icon-check-sign"></i>
                                                            </button> ����ʬ������
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <{foreach item=row from=$job_list}>
                                                        <tr>
                                                            <td>
                                                                <{$row.job_nm|escape}>
                                                                <input type="hidden" name="job[]" value="<{$row.job_id|escape}>" />
                                                            </td>
                                                            <td>
                                                                &nbsp;<input type="checkbox" class="ck-job" name="auth[<{$row.job_id|escape}>]" value="1" <{if $row.auth_flg==='t'}>checked<{/if}> />
                                                            </td>
                                                        </tr>
                                                    <{/foreach}>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="btn-toolbar">
                                            <button type="submit" name="btn_save" id="btn_save" class="btn btn-small btn-primary" value="job"><i class="icon-save"></i> ��¸����</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function () {
                $("#pulldown_class").change(function () {
                    $(this).parents('form').submit();
                });
                $('#js-btn-checkbox').click(function () {
                    if ($(this).children('i').hasClass('icon-check-sign')) {
                        $('input.ck-auditor').prop('checked', true);
                    }
                    else {
                        $('input.ck-auditor').prop('checked', false);
                    }
                    $(this).children('i').toggleClass('icon-check-sign').toggleClass('icon-check-empty');
                });
                $('#js-btn-checkbox_st').click(function () {
                    if ($(this).children('i').hasClass('icon-check-sign')) {
                        $('input.ck-st').prop('checked', true);
                    }
                    else {
                        $('input.ck-st').prop('checked', false);
                    }
                    $(this).children('i').toggleClass('icon-check-sign').toggleClass('icon-check-empty');
                });
                $('#js-btn-checkbox_job').click(function () {
                    if ($(this).children('i').hasClass('icon-check-sign')) {
                        $('input.ck-job').prop('checked', true);
                    }
                    else {
                        $('input.ck-job').prop('checked', false);
                    }
                    $(this).children('i').toggleClass('icon-check-sign').toggleClass('icon-check-empty');
                });
            });

        </script>
    </body>
</html>
