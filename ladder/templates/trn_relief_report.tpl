<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 救護員研修 報告</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='救護員研修 報告' hd_sub_title=''}>

            <div class="container-fluid">
                <div class="row-fluid">
                    <div class="well well-small">
                        <form id="relief_edit" method="post">
                            <input type='hidden' id='id' name='id' />

                            <{* form23更新用 *}>
                            <input type='hidden' name='period' value="<{$trn.training_day}>"/>
                            <input type='hidden' name='subject' value="<{$trn.subject}>"/>
                            <input type='hidden' name='unit' value="<{$trn.utni}>"/>
                            <input type='hidden' name='contents' value="<{$trn.contents}>"/>
                            <input type='hidden' name='organizer' value="<{$trn.branch}>"/>
                            <input type='hidden' name='training_id' value="<{$trn.training_id}>"/>

                            <fieldset>
                                <legend >
                                    <span
                                        rel="popover" data-trigger="hover" data-html="true" data-placement="bottom" data-animation="true"
                                        data-content="
                                        <div class='ldr-popover'>支　部: <strong><{$trn.branch|escape}></strong></div>
                                        <div class='ldr-popover'>施　設: <strong><{$trn.hospital|escape}></strong></div>
                                        <div class='ldr-popover'>開催日: <strong><{$trn.training_day|escape}></strong></div>"
                                        >
                                        <{$trn.subject|escape}><span class="ldr-roman"><{$roman_num[$trn.unit]|escape}></span>　<{$trn.contents|escape}>
                                    </span>
                                </legend>

                                <div class="control-group">
                                    <div class="form-inline">
                                        <label class="radio">
                                            <input type="radio" name="report" id="report1" value="1"><strong>出張報告提出済</strong>
                                        </label>
                                        <label class="radio">
                                            <input type="radio" name="report" id="report2" value="2"><strong>出張報告なし</strong>
                                        </label>
                                        <label class="radio">
                                            <input type="radio" name="report" id="report3" value="3"><strong>欠席</strong>
                                        </label>
                                    </div>
                                    <{foreach from=$error.report item=msg}>
                                        <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                    <{/foreach}>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="learning"><strong>学んだこと</strong></label>
                                    <div class="controls">
                                        <textarea name="learning" rows="20" class="input-maxlarge"></textarea>
                                    </div>
                                    <{foreach from=$error.learning item=msg}>
                                        <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                    <{/foreach}>
                                </div>
                            </fieldset>

                            <div class="btn-toolbar">
                                <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                                <button type="submit" name="btn_save" id="btn_save" class="js-submit-report btn btn-small btn-primary" value="btn_save"><i class="icon-save"></i> 報告する</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
    </body>
</html>


