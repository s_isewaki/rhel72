<div class="ldr-form-widget-container">
    <{*様式１*}>
    <div class="ldr-form-widget">
        <div class="ldr-form-button">
            <{if $smarty.post.menu === "apply" || $menu === "apply"}>
                <{if 1 <= $apply->get_status() && $apply->get_status() <= 4 || $apply->get_status() === '6'}>
                    <p class="title"><button type="button" data-id="<{$ldr_apply_id|default:$smarty.post.id}>" data-emp="<{$target_emp_id}>" data-php="apply_entry.php" data-mode="edit" class="js-form-button btn btn-small btn-danger"><i class="icon-pencil"></i> <{$ldr.config.form_1.btn|escape}></button></p>
                <{else}>
                    <p class="title"><button type="button" data-id="<{$ldr_apply_id|default:$smarty.post.id}>" data-emp="<{$target_emp_id}>" data-php="form_1view.php" data-mode="view" class="js-form-button btn btn-small btn-info"><i class="icon-pencil"></i> <{$ldr.config.form_1.btn|escape}></button></p>
                <{/if}>
            <{elseif $smarty.post.menu === "accept" || $menu === "accept"}>
                <p class="title"><button type="button" data-id="<{$ldr_apply_id|default:$smarty.post.id}>" data-emp="<{$target_emp_id}>" data-php="form_1view.php" data-mode="view" class="js-form-button btn btn-small btn-info"><i class="icon-pencil"></i> <{$ldr.config.form_1.btn|escape}></button></p>
            <{/if}>

            <p class="detail"><span title="<{$update1|date_format_jp:"%Y年%-m月%-d日(%a) %R"}>"><{$update1|date_format_jp:"%-m月%-d日(%a) %R"|replace:' ':'<br/>'}></span></p>
        </div>
    </div>

    <{if $ldr.config.flow === '1'}>
        <{*様式２*}>
        <{if $function.form_2}>
            <div class="ldr-form-widget">
                <div class="ldr-form-button">
                    <p class="title"><button type="button" data-id="<{$ldr_apply_id|default:$smarty.post.id}>" data-emp="<{$target_emp_id}>" data-php="form_2view.php" data-mode="view" class="js-form-button btn btn-small btn-info"><i class="icon-pencil"></i> <{$titles.form_2.btn|escape}></button></p>
                    <p class="detail"><span title="<{$update2|date_format_jp:"%Y年%-m月%-d日(%a) %R"}>"><{$update2|date_format_jp:"%-m月%-d日(%a) %R"|replace:' ':'<br/>'}></span></p>
                </div>
            </div>
        <{/if}>

        <{*様式３*}>
        <{if $function.form_3}>
            <div class="ldr-form-widget">
                <div class="ldr-form-button">
                    <p class="title"><button type="button" data-id="<{$ldr_apply_id|default:$smarty.post.id}>" data-emp="<{$target_emp_id}>" data-php="form_3view.php" data-mode="view" class="js-form-button btn btn-small btn-info"><i class="icon-pencil"></i> <{$titles.form_3.btn|escape}></button></p>
                    <p class="detail"><span title="<{$update3|date_format_jp:"%Y年%-m月%-d日(%a) %R"}>"><{$update3|date_format_jp:"%-m月%-d日(%a) %R"|replace:' ':'<br/>'}></span></p>
                </div>
            </div>
        <{/if}>

        <{*様式４*}>
        <{if $function.form_4}>
            <div class="ldr-form-widget">
                <div class="ldr-form-button">
                    <p class="title"><button type="button" data-id="<{$ldr_apply_id|default:$smarty.post.id}>" data-emp="<{$target_emp_id}>" data-php="form_4view.php" data-mode="view" class="js-form-button btn btn-small btn-info"><i class="icon-pencil"></i> <{$titles.form_4.btn|escape}></button></p>
                    <p class="detail"><span title="<{$update4|date_format_jp:"%Y年%-m月%-d日(%a) %R"}>"><{$update4|date_format_jp:"%-m月%-d日(%a) %R"|replace:' ':'<br/>'}></span></p>
                </div>
            </div>
        <{/if}>

        <{*様式５*}>
        <{if $function.form_5}>
            <div class="ldr-form-widget">
                <div class="ldr-form-button">
                    <p class="title"><button type="button" data-id="<{$ldr_apply_id|default:$smarty.post.id}>" data-emp="<{$target_emp_id}>" data-php="form_5view.php" data-mode="view" class="js-form-button btn btn-small btn-info"><i class="icon-pencil"></i> <{$titles.form_5.btn|escape}></button></p>
                    <p class="detail"><span title="<{$update5|date_format_jp:"%Y年%-m月%-d日(%a) %R"}>"><{$update5|date_format_jp:"%-m月%-d日(%a) %R"|replace:' ':'<br/>'}></span></p>
                </div>
            </div>
        <{/if}>
    <{else}>
        <{if $function.record_view}>
            <div class="ldr-form-widget">
                <div class="ldr-form-button">
                    <p class="title"><button type="button" data-id="<{$ldr_apply_id|default:$smarty.post.id}>" data-emp="<{$target_emp_id}>" data-php="records_view.php" data-mode="view" class="js-form-button btn btn-small btn-info"><i class="icon-pencil"></i> <{$titles.form_2.btn|escape}></button></p>
                    <p class="detail"><span title="<{$update_record|date_format_jp:"%Y年%-m月%-d日(%a) %R"}>"><{$update_record|date_format_jp:"%-m月%-d日(%a) %R"|replace:' ':'<br/>'}></span></p>
                </div>
            </div>

        <{/if}>
    <{/if}>
    <{*様式６*}>
    <div class="ldr-form-widget">
        <div class="ldr-form-button">
            <{if $smarty.post.menu === "apply" || $menu === "apply"}>
                <{if $apply->get_status() === '4' || $apply->get_status() === '6'}>
                    <p class="title"><button type="button" data-id="<{$ldr_apply_id|default:$smarty.post.id}>" data-emp="<{$target_emp_id}>" data-php="form_6edit.php" data-mode="edit" class="js-form-button btn btn-small btn-danger"><i class="icon-pencil"></i> <{$ldr.config.form_6.btn|escape}></button></p>
                <{elseif $apply->get_status() >= 5}>
                    <p class="title"><button type="button" data-id="<{$ldr_apply_id|default:$smarty.post.id}>" data-emp="<{$target_emp_id}>" data-php="form_6view.php" data-mode="view" class="js-form-button btn btn-small btn-info"><i class="icon-pencil"></i> <{$ldr.config.form_6.btn|escape}></button></p>
                <{else}>
                    <p class="title"><button type="button" class="btn btn-small" disabled="disabled"><i class="icon-pencil"></i> <{$ldr.config.form_6.btn|escape}></button></p>
                <{/if}>
            <{else}>
                <{if $apply->get_status() === '0' || 4 <= $apply->get_status()}>
                    <p class="title"><button type="button" data-id="<{$ldr_apply_id|default:$smarty.post.id}>" data-emp="<{$target_emp_id}>" data-php="form_6view.php" data-mode="view" class="js-form-button btn btn-small btn-info"><i class="icon-pencil"></i> <{$ldr.config.form_6.btn|escape}></button></p>
                <{else}>
                    <p class="title"><button type="button" class="btn btn-small" disabled="disabled"><i class="icon-pencil"></i> <{$ldr.config.form_6.btn|escape}></button></p>
                <{/if}>
            <{/if}>
            <p class="detail"><span title="<{$update6|date_format_jp:"%Y年%-m月%-d日(%a) %R"}>"><{$update6|date_format_jp:"%-m月%-d日(%a) %R"|replace:' ':'<br/>'}></span></p>
        </div>
    </div>

    <{*様式７*}>
    <div class="ldr-form-widget">
        <div class="ldr-form-button">
            <{if $smarty.post.menu === "apply" || $menu === "apply"}>
                <{if $apply->get_status() === '4' || $apply->get_status() === '6'}>
                    <p class="title"><button type="button" data-id="<{$ldr_apply_id|default:$smarty.post.id}>" data-emp="<{$target_emp_id}>" data-php="form_7edit.php" data-mode="edit" class="js-form-button btn btn-small btn-danger"><i class="icon-pencil"></i> <{$ldr.config.form_7.btn|escape}></button></p>
                <{elseif $apply->get_status() >= 5}>
                    <p class="title"><button type="button" data-id="<{$ldr_apply_id|default:$smarty.post.id}>" data-emp="<{$target_emp_id}>" data-php="form_7view.php" data-mode="view" class="js-form-button btn btn-small btn-info"><i class="icon-pencil"></i> <{$ldr.config.form_7.btn|escape}></button></p>
                <{else}>
                    <p class="title"><button type="button" class="btn btn-small" disabled="disabled"><i class="icon-pencil"></i> <{$ldr.config.form_7.btn|escape}></button></p>
                <{/if}>
            <{else}>
                <{if $apply->get_status() === '0' || 4 <= $apply->get_status()}>
                    <p class="title"><button type="button" data-id="<{$ldr_apply_id|default:$smarty.post.id}>" data-emp="<{$target_emp_id}>" data-php="form_7view.php" data-mode="view" class="js-form-button btn btn-small btn-info"><i class="icon-pencil"></i> <{$ldr.config.form_7.btn|escape}></button></p>
                <{else}>
                    <p class="title"><button type="button" class="btn btn-small" disabled="disabled"><i class="icon-pencil"></i> <{$ldr.config.form_7.btn|escape}></button></p>
                <{/if}>
            <{/if}>
            <p class="detail"><span title="<{$update7|date_format_jp:"%Y年%-m月%-d日(%a) %R"}>"><{$update7|date_format_jp:"%-m月%-d日(%a) %R"|replace:' ':'<br/>'}></span></p>
        </div>
    </div>

    <{*様式８*}>
    <{if $ldr.config.flow === '1'}>
        <div class="ldr-form-widget">
            <div class="ldr-form-button">
                <{if $smarty.post.menu === "apply" || $menu === "apply"}>
                    <{if $apply->get_status() === '0'}>
                        <p class="title"><button type="button" data-id="<{$ldr_apply_id|default:$smarty.post.id}>" data-emp="<{$target_emp_id}>" data-php="form_8view.php" data-mode="view" class="js-form-button btn btn-small btn-info"><i class="icon-pencil"></i> <{$ldr.config.form_8.btn|escape}></button></p>
                    <{else}>
                        <p class="title"><button type="button" class="btn btn-small" disabled="disabled"><i class="icon-pencil"></i> <{$ldr.config.form_8.btn|escape}></button></p>
                    <{/if}>
                <{else}>
                    <{if $apply->get_status() === '9' && $apply->is_facilitator($emp->emp_id())}>
                        <p class="title"><button type="button" data-id="<{$ldr_apply_id|default:$smarty.post.id}>" data-emp="<{$target_emp_id}>" data-php="form_8edit.php" data-mode="edit" class="js-form-button btn btn-small btn-danger"><i class="icon-pencil"></i> <{$ldr.config.form_8.btn|escape}></button></p>
                    <{elseif $apply->get_status() === '0' || $apply->get_status() > '9'}>
                        <p class="title"><button type="button" data-id="<{$ldr_apply_id|default:$smarty.post.id}>" data-emp="<{$target_emp_id}>" data-php="form_8view.php" data-mode="view" class="js-form-button btn btn-small btn-info"><i class="icon-pencil"></i> <{$ldr.config.form_8.btn|escape}></button></p>
                    <{else}>
                        <p class="title"><button type="button" class="btn btn-small" disabled="disabled"><i class="icon-pencil"></i><{$ldr.config.form_8.btn|escape}></button></p>

                    <{/if}>
                <{/if}>
                <p class="detail"><span title="<{$update8|date_format_jp:"%Y年%-m月%-d日(%a) %R"}>"><{$update8|date_format_jp:"%-m月%-d日(%a) %R"|replace:' ':'<br/>'}></span></p>
            </div>
        </div>
    <{/if}>

    <{*様式９*}>
    <{if $ldr.config.flow === '1'}>
        <div class="ldr-form-widget">
            <div class="ldr-form-button">
                <{if $smarty.post.menu === "apply" || $menu === "apply"}>
                    <{if $apply->get_status() === '0'}>
                        <p class="title"><button type="button" data-id="<{$ldr_apply_id|default:$smarty.post.id}>" data-emp="<{$target_emp_id}>" data-php="form_9view.php" data-mode="view" class="js-form-button btn btn-small btn-info"><i class="icon-pencil"></i> <{$ldr.config.form_9.btn|escape}></button></p>
                    <{else}>
                        <p class="title"><button type="button" class="btn btn-small" disabled="disabled"><i class="icon-pencil"></i> <{$ldr.config.form_9.btn|escape}></button></p>
                    <{/if}>
                <{else}>
                    <{if $apply->get_status() === '0' || $apply->get_status() > '11'}>
                        <p class="title"><button type="button" data-id="<{$ldr_apply_id|default:$smarty.post.id}>" data-emp="<{$target_emp_id}>" data-php="form_9view.php" data-mode="view" class="js-form-button btn btn-small btn-info"><i class="icon-pencil"></i> <{$ldr.config.form_9.btn|escape}></button></p>
                    <{else}>
                        <p class="title"><button type="button" class="btn btn-small" disabled="disabled"><i class="icon-pencil"></i><{$ldr.config.form_9.btn|escape}></button></p>
                            <{/if}>
                        <{/if}>
                <p class="detail"><span title="<{$update9|date_format_jp:"%Y年%-m月%-d日(%a) %R"}>"><{$update9|date_format_jp:"%-m月%-d日(%a) %R"|replace:' ':'<br/>'}></span></p>
            </div>
        </div>
    <{/if}>

    <{if $ldr.config.flow === '1'}>
        <{if $function.form_carrier}>
            <div class="ldr-form-widget">
                <div class="ldr-form-button">
                    <p class="title"><button type="button" data-id="<{$ldr_apply_id|default:$smarty.post.id}>" data-emp="<{$target_emp_id}>" data-php="form_carrier_view.php" data-mode="view" class="js-form-button btn btn-small btn-info"><i class="icon-pencil"></i> <{$titles.form_carrier.btn|escape}></button></p>
                    <p class="detail"><span title="<{$update_carrier|date_format_jp:"%Y年%-m月%-d日(%a) %R"}>"><{$update_carrier|date_format_jp:"%-m月%-d日(%a) %R"|replace:' ':'<br/>'}></span></p>
                </div>
            </div>
        <{/if}>
        <{if $function.form_transfer}>
            <div class="ldr-form-widget">
                <div class="ldr-form-button">
                    <p class="title"><button type="button" data-id="<{$ldr_apply_id|default:$smarty.post.id}>" data-emp="<{$target_emp_id}>" data-php="form_transfer_view.php" data-mode="view" class="js-form-button btn btn-small btn-info"><i class="icon-pencil"></i> <{$titles.form_transfer.btn|escape}></button></p>
                    <p class="detail"><span title="<{$update_transfer|date_format_jp:"%Y年%-m月%-d日(%a) %R"}>"><{$update_transfer|date_format_jp:"%-m月%-d日(%a) %R"|replace:' ':'<br/>'}></span></p>
                </div>
            </div>
        <{/if}>

        <{if $function.form_presentation}>
            <div class="ldr-form-widget">
                <div class="ldr-form-button">
                    <p class="title"><button type="button" data-id="<{$ldr_apply_id|default:$smarty.post.id}>" data-emp="<{$target_emp_id}>" data-php="form_presentation_view.php" data-mode="view" class="js-form-button btn btn-small btn-info"><i class="icon-pencil"></i> <{$titles.form_presentation.btn|escape}></button></p>
                    <p class="detail"><span title="<{$update_presentation|date_format_jp:"%Y年%-m月%-d日(%a) %R"}>"><{$update_presentation|date_format_jp:"%-m月%-d日(%a) %R"|replace:' ':'<br/>'}></span></p>
                </div>
            </div>
        <{/if}>

        <{if $function.form_article}>
            <div class="ldr-form-widget">
                <div class="ldr-form-button">
                    <p class="title"><button type="button" data-id="<{$ldr_apply_id|default:$smarty.post.id}>" data-emp="<{$target_emp_id}>" data-php="form_article_view.php" data-mode="view" class="js-form-button btn btn-small btn-info"><i class="icon-pencil"></i> <{$titles.form_article.btn|escape}></button></p>
                    <p class="detail"><span title="<{$update_article|date_format_jp:"%Y年%-m月%-d日(%a) %R"}>"><{$update_article|date_format_jp:"%-m月%-d日(%a) %R"|replace:' ':'<br/>'}></span></p>
                </div>
            </div>
        <{/if}>
    <{/if}>

</div>
