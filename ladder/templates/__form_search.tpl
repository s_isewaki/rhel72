<{* form 検索条件 共通 *}>
<input type="hidden" name="del_inner_id" id="del_inner_id" />
<input type="hidden" name="inner_id" id="inner_id" />

<div class="control-group">
    <div class="form-inline">
        <button type="button" class="btn btn-small btn-primary" id="btn_new"><i class="icon-plus"></i> 記録を追加する</button>
        <{if $list}>
            <button type="button" class="js-form-search btn btn-small btn-primary" data-target="#search_form"><i class="icon-search"></i> 検索する</button>
        <{/if}>
        <a href="#upload" data-toggle="modal" class="btn btn-small btn-default ldr_upload"><i class="icon-cloud-upload"></i> インポート</a>
        <{if $list}>
            <button type="button" id="export_csv" class="btn btn-small btn-default"><i class="icon-cloud-download"></i> CSVエクスポート</button>
        <{/if}>
    </div>
</div>
<div class="control-group <{if ! $search}>hide<{/if}>" id="search_form">
    <div class="form-inline">
        <{include file="__datepicker.tpl" date_id="srh_start_date" date_name="srh_start_date" date_value=$srh_start_date|escape}>
        〜
        <{include file="__datepicker.tpl" date_id="srh_last_date" date_name="srh_last_date" date_value=$srh_last_date|escape}>
        <{if $formname === 'form22'}>
            <select name="srh_flag" id="srh_flag" class="input-small">
                <option value="">すべて</option>
                <option value="1">院内</option>
                <option value="2">院外</option>
                <option value="3">バリテス</option>
            </select>
        <{/if}>
        <input type="text" id="srh_keyword" name="srh_keyword" class="input-large" value='<{$srh_keyword|escape}>' placeholder="検索ワードを入力してください" />
        <button type="submit" id="btn_search" name="btn_search" value="search" class="btn btn-small btn-primary"><i class="icon-search"></i> 検索</button>
    </div>
</div>