<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 振り返りテンプレート: 作成</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='振り返りテンプレート: 作成' hd_sub_title=''}>

            <div class="container-fluid">
                <div class="row-fluid">
                    <form class="form" action="adm_check_template_edit.php" method="post">
                        <input type="hidden" id="tmpl_id" name="tmpl_id" value="" />

                        <div class="control-group">
                            <label class="control-label">
                                <strong>テンプレート名</strong>
                                <span class="badge badge-important required">必須</span>
                            </label>
                            <div class="controls form-inline">
                                <input type="text" id="title" name="title" class="input-xxlarge" maxlength="100" placeholder="テンプレート名を入力してください" />
                            </div>
                            <{foreach from=$error.title item=msg}>
                                <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                            <{/foreach}>
                        </div>

                        <hr/>

                        <{include file="adm_check/__check.tpl"}>

                        <hr/>

                        <div class="btn-toolbar">
                            <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                            <button type="submit" name="btn_save" id="btn_save" class="btn btn-small btn-primary js-check-save" value="btn_save"><i class="icon-save"></i> 保存する</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <{include file="adm_check/__check_js.tpl"}>
        <script type="text/javascript">
            $(function() {
                $('.js-check-save').click(function() {
                    if ($('#tmpl_id').val()) {
                        alert('保存しました。\nテンプレートの更新は、すでに院内研修講座に設定した振り返りには反映されません。');
                    }
                });
            });
        </script>
    </body>
</html>
