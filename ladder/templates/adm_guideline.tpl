<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 行動指標マスタ管理</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='行動指標マスタ管理' hd_sub_title=''}>

            <div class="container-fluid">
                <div class="row-fluid">

                    <ul class="nav nav-tabs">
                        <li class="<{if $active === 'guideline'}>active<{/if}>"><a href="#guideline" data-toggle="tab" data-active="guideline"><i class="icon-compass"></i> 行動指標</a></li>
                        <li class="<{if $active === 'revision'}>active<{/if}>"><a href="#revision" data-toggle="tab" data-active="revision"><i class="icon-book"></i> 版</a></li>
                        <li class="<{if $active === 'group1'}>active<{/if}>"><a href="#group1" data-toggle="tab" data-active="group1"><i class="icon-folder-close"></i> 領域</a></li>
                        <li class="<{if $active === 'group2'}>active<{/if}>"><a href="#group2" data-toggle="tab" data-active="group2"><i class="icon-folder-close-alt"></i> 分類</a></li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane <{if $active === 'guideline'}>active<{else}>fade<{/if}>" id="guideline">
                            <div class="well well-small">
                                <div class="form-group">
                                    <fieldset>
                                        <legend>行動指標</legend>
                                        <form action="adm_guideline.php" method="post" id="form_guideline">
                                            <input type="hidden" name="mode" value="guideline" />
                                            <{if ! $revision_list}>
                                                <div class="alert alert-error">版を登録してください</div>
                                            <{else}>
                                                <div class="controls">
                                                    <select id="pulldown_guideline_revision" name="guideline_revision" class="js-pulldown-guideline js-select-revision" data-target="#pulldown_guideline_group1" data-change="revision">
                                                        <{foreach item=row from=$revision_list}>
                                                            <option value="<{$row.guideline_revision|escape}>"><{$row.name|escape}></option>
                                                        <{/foreach}>
                                                    </select>
                                                </div>
                                            <{/if}>
                                            <{if ! $group1_list}>
                                                <div class="alert alert-error">領域を登録してください</div>
                                            <{else}>
                                                <select id="pulldown_guideline_group1" name="guideline_group1" class="js-pulldown-guideline" data-change="group1">
                                                    <{foreach item=row from=$group1_list}>
                                                        <option value="<{$row.guideline_group1|escape}>"><{$row.name|escape}></option>
                                                    <{/foreach}>
                                                </select>
                                            <{/if}>

                                            <{if ! $group2_list}>
                                                <div class="alert alert-error">分類を登録してください</div>
                                            <{else}>
                                                <select id="pulldown_level" name="level" class="input-small js-pulldown-guideline">
                                                    <option value="0">なし</option>
                                                    <option value="1">I</option>
                                                    <option value="2">II</option>
                                                    <option value="3">III</option>
                                                    <option value="4">IV</option>
                                                    <option value="5">V</option>
                                                    <option value="6">VI</option>
                                                </select>

                                                <{if $guideline_list}>

                                                    <table class="table table-striped table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th><i class='icon-pencil'></i></th>
                                                                <th class="pull-right"><i class="icon-sort"></i></th>
                                                                <th nowrap>領域</th>
                                                                <th nowrap>分類</th>
                                                                <th nowrap>レベル</th>
                                                                <th nowrap>行動指標</th>
                                                                <th nowrap>判断基準</th>
                                                                <th class="pull-right"><i class='icon-remove'></i></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody class="sortable">
                                                            <{foreach item=row from=$guideline_list}>
                                                                <tr>
                                                                    <td><a href="adm_guideline_modal.php?mode=guideline&id=<{$row.guideline_id|escape}>" data-toggle="modal" data-target="#gl_modal"><i class='icon-pencil'></i></a></td>
                                                                    <td class="dragHandle"><span class="pull-right"><i class="icon-sort"></i><input type="hidden" name="order[]" value="<{$row.guideline_id|escape}>" /></span></td>
                                                                    <td class="ldr-td-text"><{$row.group1_name|escape}></td>
                                                                    <td class="ldr-td-text"><{$row.group2_name|escape}></td>
                                                                    <td class="ldr-td-level"><{$row.level_roman}></td>
                                                                    <td class="ldr-td-text-left"><{$row.guideline|escape|nl2br}></td>
                                                                    <td class="ldr-td-text-left"><{$row.criterion|escape|nl2br}></td>
                                                                    <td>
                                                                        <{if $row.used !== 't'}>
                                                                            <a class="pull-right js-row-remove" href="javascript:void(0);" data-target="#form_guideline" data-id="<{$row.guideline_id|escape}>">
                                                                                <i class="icon-remove"></i>
                                                                            </a>
                                                                        <{/if}>
                                                                    </td>
                                                                </tr>
                                                            <{/foreach}>
                                                        </tbody>
                                                    </table>
                                                    <hr/>
                                                <{else}>
                                                    <div class="alert alert-error">行動指標を登録してください</div>
                                                <{/if}>

                                                <div class="btn-toolbar">
                                                    <a href="adm_guideline_modal.php?mode=guideline" data-toggle="modal" data-target="#gl_modal" class="btn btn-small btn-primary"><i class="icon-plus"></i> 追加する</a>
                                                    <{if $guideline_list}>
                                                        <button type="button" class="btn btn-small btn-primary js-table-sort" data-target="#form_guideline"><i class="icon-ok-sign"></i> 順番を並べ替える</button>
                                                    <{/if}>
                                                </div>
                                            <{/if}>
                                        </form>
                                        <{include file="__modal_form.tpl" modal_id="gl_modal" modal_title="行動指標の登録"}>
                                    </fieldset>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane <{if $active === 'revision'}>active<{else}>fade<{/if}>" id="revision">
                            <div class="well well-small">
                                <div class="form-group">
                                    <fieldset>
                                        <legend>版</legend>
                                        <{if $revision_list}>
                                            <form action="adm_guideline.php" method="post" id="form_revision">
                                                <input type="hidden" name="mode" value="revision" />
                                                <table class="table table-striped table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th><i class='icon-pencil'></i></th>
                                                            <th>版の名称</th>
                                                            <th>指標数</th>
                                                            <th class="pull-right"><i class='icon-remove'></i></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <{foreach item=row from=$revision_list}>
                                                            <tr>
                                                                <td><a href="adm_guideline_modal.php?mode=revision&id=<{$row.guideline_revision|escape}>" data-toggle="modal" data-target="#rv_modal"><i class='icon-pencil'></i></a></td>
                                                                <td> <{if $current_revision === $row.guideline_revision}><i class="icon-star"></i> <{/if}><{$row.name|escape}></td>
                                                                <td class="text-right"><{$row.count|escape}></td>
                                                                <td>
                                                                    <{if $row.count === '0'}>
                                                                        <a class="pull-right js-row-remove" href="javascript:void(0);" data-target="#form_revision" data-id="<{$row.guideline_revision|escape}>">
                                                                            <i class="icon-remove"></i>
                                                                        </a>
                                                                    <{/if}>
                                                                </td>
                                                            </tr>
                                                        <{/foreach}>
                                                    </tbody>
                                                </table>
                                            </form>
                                            <hr/>
                                        <{else}>
                                            <div class="alert alert-error">版を登録してください</div>
                                        <{/if}>

                                        <div class="btn-toolbar">
                                            <a href="adm_guideline_modal.php?mode=revision" data-toggle="modal" data-target="#rv_modal" class="btn btn-small btn-primary"><i class="icon-plus"></i> 追加する</a>
                                        </div>
                                        <{include file="__modal_form.tpl" modal_id="rv_modal" modal_title="版の登録"}>
                                    </fieldset>
                                    <{if $revision_list}>
                                        <hr/>
                                        <fieldset>
                                            <legend>CSVデータのエクスポート</legend>
                                            <form action="adm_guideline.php" method="post" class="form-inline">
                                                <input type="hidden" name="mode" value="revision" />
                                                <input type="hidden" name="mode2" value="download" />
                                                <select name="guideline_revision">
                                                    <{foreach item=row from=$revision_list}>
                                                        <option value="<{$row.guideline_revision|escape}>" <{if $current_revision === $row.guideline_revision}>selected<{/if}>><{$row.name|escape}></option>
                                                    <{/foreach}>
                                                </select>
                                                <button type="submit" class="btn btn-small btn-success"><i class="icon-cloud-download"></i> CSVエクスポート</button>
                                            </form>
                                        </fieldset>

                                        <hr/>
                                        <fieldset>
                                            <legend>CSVデータのインポート</legend>
                                            <form action="#" method="post" class="form-inline">
                                                <div id="form_import">
                                                    <input type="hidden" name="mode" value="revision" />
                                                    <input type="hidden" name="mode2" value="upload" />
                                                    <select name="guideline_revision">
                                                        <{foreach item=row from=$revision_list}>
                                                            <option value="<{$row.guideline_revision|escape}>" <{if $current_revision === $row.guideline_revision}>selected<{/if}>><{$row.name|escape}></option>
                                                        <{/foreach}>
                                                    </select>
                                                    <input type="file" id="csvfile" name="csvfile" />
                                                    <button type="button" id="btn_import" class="btn btn-small btn-success"><i class="icon-cloud-upload"></i> CSVインポート</button>
                                                </div>
                                            </form>
                                            <div id="alert_csv" class="alert" style="overflow:auto; display: none;"></div>
                                        </fieldset>

                                        <hr/>
                                        <fieldset>
                                            <legend>運用する版の切り替え</legend>
                                            <form action="adm_guideline.php" method="post" class="form-inline">
                                                <input type="hidden" name="mode" value="revision" />
                                                <input type="hidden" name="mode2" value="current_revision" />
                                                <select name="current_revision">
                                                    <{foreach item=row from=$revision_list}>
                                                        <option value="<{$row.guideline_revision|escape}>" <{if $current_revision === $row.guideline_revision}>selected<{/if}>><{$row.name|escape}></option>
                                                    <{/foreach}>
                                                </select>
                                                <button type="submit" class="btn btn-small btn-primary"><i class="icon-ok-sign"></i> この版を使用する</button>
                                            </form>
                                        </fieldset>
                                    <{/if}>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane <{if $active === 'group1'}>active<{else}>fade<{/if}>" id="group1">
                            <div class="well well-small">
                                <div class="form-group">
                                    <fieldset>
                                        <legend>領域</legend>
                                        <form action="adm_guideline.php" method="post" id="form_group1" name ="form_group1">
                                            <input type="hidden" name="mode" value="group1" />
                                            <select id="pulldown_group1_revision" name="guideline_revision" class="js-pulldown-group1" data-change="revision">
                                                <{foreach item=row from=$revision_list}>
                                                    <option value="<{$row.guideline_revision|escape}>"><{$row.name|escape}></option>
                                                <{/foreach}>
                                            </select>
                                            <{if $group1_list}>
                                                <table class="table table-striped table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th><i class='icon-pencil'></i></th>
                                                            <th class="pull-right"><i class="icon-sort"></i></th>
                                                            <th>名称</th>
                                                            <th>コメント</th>
                                                            <th>様式7の評価対象</th>
                                                            <th class="pull-right"><i class='icon-remove'></i></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody class="sortable">
                                                        <{foreach item=row from=$group1_list}>
                                                            <tr>
                                                                <td><a href="adm_guideline_modal.php?mode=group1&id=<{$row.guideline_group1|escape}>" data-toggle="modal" data-target="#g1_modal"><i class='icon-pencil'></i></a></td>
                                                                <td class="dragHandle"><span class="pull-right"><i class="icon-sort"></i><input type="hidden" name="order[]" value="<{$row.guideline_group1|escape}>" /></span></td>
                                                                <td><{$row.name|escape}></td>
                                                                <td><{$row.comment|escape|nl2br}></td>
                                                                <td><{if $row.form7 === 't'}>対象である<{else}>-<{/if}></td>
                                                                <td>
                                                                    <{if $row.used !== 't'}>
                                                                        <a class="pull-right js-row-remove" href="javascript:void(0);" data-target="#form_group1" data-id="<{$row.guideline_group1|escape}>">
                                                                            <i class="icon-remove"></i>
                                                                        </a>
                                                                    <{/if}>
                                                                </td>
                                                            </tr>
                                                        <{/foreach}>
                                                    </tbody>
                                                </table>
                                                <hr/>
                                            <{else}>
                                                <div class="alert alert-error">領域を登録してください</div>
                                            <{/if}>

                                            <div class="btn-toolbar">
                                                <a href="adm_guideline_modal.php?mode=group1" data-toggle="modal" data-target="#g1_modal" class="btn btn-small btn-primary"><i class="icon-plus"></i> 追加する</a>
                                                <{if $group1_list}>
                                                    <button type="button" class="btn btn-small btn-primary js-table-sort" data-target="#form_group1"><i class="icon-ok-sign"></i> 順番を並べ替える</button>
                                                <{/if}>
                                            </div>
                                        </form>
                                        <{include file="__modal_form.tpl" modal_id="g1_modal" modal_title="領域の登録"}>
                                    </fieldset>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane <{if $active === 'group2'}>active<{else}>fade<{/if}>" id="group2">
                            <div class="well well-small">
                                <div class="form-group">
                                    <fieldset>
                                        <legend>分類</legend>
                                        <form action="adm_guideline.php" method="post" id="form_group2" name="form_group2">
                                            <input type="hidden" name="mode" value="group2" />
                                            <div class="controls">
                                                <select id="pulldown_group2_revision" name="guideline_revision" class="js-pulldown-group2" data-change="revision">
                                                    <{foreach item=row from=$revision_list}>
                                                        <option value="<{$row.guideline_revision|escape}>"><{$row.name|escape}></option>
                                                    <{/foreach}>
                                                </select>
                                            </div>
                                            <{if $group2_list}>
                                                <table class="table table-striped table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th><i class='icon-pencil'></i></th>
                                                            <th class="pull-right"><i class="icon-sort"></i></th>
                                                            <th>分類</th>
                                                            <th>コメント</th>
                                                            <th>領域</th>
                                                            <th class="pull-right"><i class='icon-remove'></i></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody class="sortable">
                                                        <{foreach item=row from=$group2_list}>
                                                            <tr>
                                                                <td nowrap><a href="adm_guideline_modal.php?mode=group2&id=<{$row.guideline_group2|escape}>" data-toggle="modal" data-target="#g2_modal"><i class='icon-pencil'></i></a></td>
                                                                <td nowrap class="dragHandle"><span class="pull-right"><i class="icon-sort"></i><input type="hidden" name="order[]" value="<{$row.guideline_group2|escape}>" /></span></td>
                                                                <td><{$row.name|escape}></td>
                                                                <td>
                                                                    <ol>
                                                                        <{if $row.comment[0]}><li value="0"><{$row.comment[0]|escape|nl2br}></li><{/if}>
                                                                        <{if $row.comment[1]}><li value="1"><{$row.comment[1]|escape|nl2br}></li><{/if}>
                                                                        <{if $row.comment[2]}><li value="2"><{$row.comment[2]|escape|nl2br}></li><{/if}>
                                                                        <{if $row.comment[3]}><li value="3"><{$row.comment[3]|escape|nl2br}></li><{/if}>
                                                                        <{if $row.comment[4]}><li value="4"><{$row.comment[4]|escape|nl2br}></li><{/if}>
                                                                        <{if $row.comment[5]}><li value="5"><{$row.comment[5]|escape|nl2br}></li><{/if}>
                                                                        <{if $row.comment[6]}><li value="6"><{$row.comment[6]|escape|nl2br}></li><{/if}>
                                                                    </ol>
                                                                </td>
                                                                <td nowrap><{$row.group1_name|escape}></td>
                                                                <td nowrap>
                                                                    <{if $row.used !== 't'}>
                                                                        <a class="pull-right js-row-remove" href="javascript:void(0);" data-target="#form_group2" data-id="<{$row.guideline_group2|escape}>">
                                                                            <i class="icon-remove"></i>
                                                                        </a>
                                                                    <{/if}>
                                                                </td>
                                                            </tr>
                                                        <{/foreach}>
                                                    </tbody>
                                                </table>
                                                <hr/>
                                            <{else}>
                                                <div class="alert alert-error">分類を登録してください</div>
                                            <{/if}>

                                            <div class="btn-toolbar">
                                                <a href="adm_guideline_modal.php?mode=group2" data-toggle="modal" data-target="#g2_modal" class="btn btn-small btn-primary"><i class="icon-plus"></i> 追加する</a>
                                                <{if $group2_list}>
                                                    <button type="button" class="btn btn-small btn-primary js-table-sort" data-target="#form_group2"><i class="icon-ok-sign"></i> 順番を並べ替える</button>
                                                <{/if}>
                                            </div>
                                        </form>
                                        <{include file="__modal_form.tpl" modal_id="g2_modal" modal_title="分類の登録"}>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <{include file="__modal_form_js.tpl"}>
        <{javascript src="../js/jquery/jquery.cookie.js"}>
        <{javascript src="../js/jquery/jquery-upload-1.0.2.min.js"}>
        <script type="text/javascript">
            $(function() {
                //項目の削除
                $('.js-row-remove').click(function() {
                    if (!confirm('削除してもよろしいですか？')) {
                        return false;
                    }
                    $($(this).data('target'))
                        .append($('<input></input>').attr({type: 'hidden', name: 'id', value: $(this).data('id')}))
                        .append($('<input></input>').attr({type: 'hidden', name: 'mode2', value: 'delete'}))
                        .submit();
                });

                //順番変更
                $('.js-table-sort').click(function() {
                    if (!confirm('順番を変更してもよろしいですか？')) {
                        return false;
                    }
                    $($(this).data('target'))
                        .append($('<input></input>').attr({type: 'hidden', name: 'mode2', value: 'sort'}))
                        .submit();
                });

                // セレクト連動
                $('body').on('change', 'select.js-select-revision', function() {
                    var target = $(this).data('target');
                    var id = $(this).val();
                    $.ajax({
                        url: 'adm_guideline_pulldown.php',
                        dataType: 'json',
                        type: 'post',
                        data: {
                            mode: 'group1',
                            guideline_revision: id
                        }
                    }).done(function(obj) {
                        $(target).empty();
                        $.each(obj, function(i, row) {
                            $(target).append('<option value="' + row.guideline_group1 + '">' + row.name + '</option>');
                        });
                        $(target).change();
                    });
                });
                $('body').on('change', 'select.js-select-group1', function() {
                    var target = $(this).data('target');
                    var id = $(this).val();
                    $.ajax({
                        url: 'adm_guideline_pulldown.php',
                        dataType: 'json',
                        type: 'post',
                        data: {
                            mode: 'group2',
                            guideline_group1: id
                        }
                    }).done(function(obj) {
                        $(target).empty();
                        $.each(obj, function(i, row) {
                            $(target).append('<option value="' + row.guideline_group2 + '">' + row.name + '</option>');
                        });
                        $(target).change();
                    });
                });

                // グループ1プルダウン選択
                $('.js-pulldown-group1').change(function() {
                    $.cookie('pulldown_revision', $('#pulldown_group1_revision').val());
                    $('#form_group1').submit();
                });

                // グループ2プルダウン選択
                $('.js-pulldown-group2').change(function() {
                    $.cookie('pulldown_revision', $('#pulldown_group2_revision').val());
                    $('#form_group2').submit();
                });


                // 行動指標プルダウン選択
                $('.js-pulldown-guideline').change(function() {
                    $.cookie('pulldown_revision', $('#pulldown_guideline_revision').val());
                    $.cookie('pulldown_group1', $('#pulldown_guideline_group1').val());
                    $.cookie('pulldown_level', $('#pulldown_level').val());
                    $('#form_guideline').submit();
                });

                // タブ選択
                $('a[data-toggle="tab"]').on('shown', function(e) {
                    $.cookie('active', $(e.target).data('active'));
                });

                // インポート
                $('#btn_import').click(function() {
                    $('#alert_csv')
                        .hide()
                        .css('height', '')
                        .removeClass('alert-error alert-info');

                    if (!$('#csvfile').val()) {
                        $('#alert_csv')
                            .html('<h4><i class="icon-warning-sign"></i> ファイルを選択してください</h4>')
                            .addClass('alert-error')
                            .show();
                        return false;
                    }
                    $('#form_import').upload('adm_guideline.php', {}, function(res) {
                        if (res.error) {
                            var result = '';
                            $.each(res.result, function(i, data) {
                                result += '<li>' + data + '</li>';
                            });
                            $('#alert_csv')
                                .html('<h4><i class="icon-warning-sign"></i> ' + res.msg + '</h4>' + (result ? '<ul>' + result + '</ul>' : ''))
                                .addClass('alert-error')
                                .show();
                        }
                        else {
                            $('#alert_csv')
                                .html('<h4><i class="icon-thumbs-up"></i> ' + res.msg + '</h4>')
                                .addClass('alert-info')
                                .show();
                        }
                    }, 'json');
                });
            });
        </script>
    </body>
</html>
