<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 院内研修マスタ 振り返り</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='院内研修マスタ 振り返り' hd_sub_title=''}>

            <div class="container-fluid">
                <div class="row-fluid">
                    <form id="inside_form" class="form"  method="post">
                        <input type="hidden" id="id" name="id" value=""/>
                        <{if $list}>
                            <label class="control-label"><strong>テンプレートを選んでください</strong></label>
                            <div class="form-inline">
                                <select id="tmpl_id" name="tmpl_id" class="input-xlarge js-pulldown-inside">
                                    <option value="">登録されている振り返り</option>
                                    <{foreach item=row from=$list}>
                                        <option value="<{$row.tmpl_id|escape}>"><{$row.title|escape}></option>
                                    <{/foreach}>
                                </select>
                                <button type="submit" name="btn_template" id="btn_template" class="btn btn-small btn-primary" value="btn_save"><i class="icon-file"></i> 取得する</button>
                            </div>
                        <{/if}>
                        <hr/>

                        <{include file="adm_check/__check.tpl"}>

                        <hr/>

                        <div class="btn-toolbar">
                            <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                            <button type="submit" name="btn_save" id="btn_save" class="btn btn-small btn-primary js-submit-save" value="btn_save"><i class="icon-save"></i> 保存する</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <{include file="adm_check/__check_js.tpl"}>
        <script type="text/javascript">
            $(function() {
            });
        </script>
    </body>
</html>
