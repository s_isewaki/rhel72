<style type="text/css">
    .label {
        width:170;
        text-align: right;
    }
    .data {
        width:380;
    }
    .level {
        font-family: kozminproregular;
    }
</style>
<table border="1" cellpadding="10" cellspacing="0">
    <tbody>
        <tr>
            <td class="label">既得しているレベル:</td>
            <td class="data level"><{$target_emp->level_roman($apply->ladder_id())|default:$ldr.config.level0|default:'なし'|escape}></td>
        </tr>
        <{if $target_emp->level($apply->ladder_id())}>
            <tr>
                <td class="label">認定日:</td>
                <td class="data"><{$target_emp->lv_date($apply->ladder_id())|date_format_jp:'%Y年 %-m月 %-d日 (%a)'}></td>
            </tr>
            <tr>
                <td class="label">取得施設:</td>
                <td class="data"><{$target_emp->lv_hospital($apply->ladder_id())|escape}></td>
            </tr>
        <{/if}>
</table>
<br>
<br>
<table border="1" cellpadding="10" cellspacing="0">
    <tbody>
        <tr>
            <td class="label">申請年月日:</td>
            <td class="data"><{$form1.apply_date|date_format_jp:'%Y年 %-m月 %-d日 (%a)'}></td>
        </tr>
        <tr>
            <td class="label">申請するレベル:</td>
            <td class="data level"><{$roman_num[$form1.level]|default:$ldr.config.level0|escape}></td>
        </tr>
        <tr>
            <td class="label">臨床経験年数:</td>
            <td class="data"><{$form1.career|escape}>　年目</td>
        </tr>
        <tr>
            <td class="label">当院での臨床経験年数:</td>
            <td class="data"><{$form1.career_hospital|escape}>　年目</td>
        </tr>
        <tr>
            <td class="label">当該部署での臨床経験年数:</td>
            <td class="data"><{$form1.career_ward|escape}>　年目</td>
        </tr>
        <tr>
            <td class="label">当面のあなたの目標:</td>
            <td class="data"><{$form1.objective|escape|nl2br}></td>
        </tr>
        <{if $ldr.config.flow === '1' && $ldr.config.meeting === '1'}>
            <tr>
                <td class="label">評価会日時:</td>
                <td class="data">
                    <{if $form1.meeting_date}>
                        <{$form1.meeting_date|date_format_jp:'%Y年 %-m月 %-d日 (%a)'|cat:'　'|cat:$form1.meeting_start_time|cat:'　〜　'|cat:$form1.meeting_last_time}>
                    <{/if}>
                </td>
            </tr>
        <{/if}>
        <{if $ldr.config.appr === '1'}>
            <{if $ldr.config.flow === '1'}>
                <tr>
                    <td class="label">司会者:</td>
                    <td class="data">
                        <{foreach from=$form1.facilitator item=name}>
                            <{$name|escape}>　
                        <{/foreach}>
                    </td>
                </tr>
            <{/if}>
            <tr>
                <td class="label">評価者:</td>
                <td class="data">
                    <{foreach from=$form1.appraiser item=name}>
                        <{$name|escape}>　
                    <{/foreach}>
                </td>
            </tr>
        <{/if}>
    </tbody>
</table>
