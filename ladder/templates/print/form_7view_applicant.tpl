<style type="text/css">
    .label {
        width:17;
        text-align: center;
    }
    .assessment {
        width:30;
        text-align: center;
        font-size: 9pt;
    }
    .guideline {
        <{if $ldr.config.reason === '1'}>
            width:253;
        <{else}>
            width:493;
        <{/if}>
        font-size: 9pt;
    }
    .reason {
        width:240;
        font-size: 9pt;
    }
    table th {
        text-align: center;
    }
</style>
<table border="1" cellpadding="2" cellspacing="0">
    <thead>
        <tr>
            <th class="label"></th>
            <th class="guideline">��ư��ɸ</th>
            <th class="assessment">ɾ��</th>
            <{if $ldr.config.reason === '1'}><th class="reason">����</th><{/if}>
        </tr>
    </thead>
    <tbody>
        <{foreach item=g1 from=$group1_list}>
            <{foreach item=asm from=$assessment_list[$g1.guideline_group1] name=loopname}>
                <{assign var='asmkey' value="_`$asm.apply_assessment`"}>
                <tr>
                    <{if $smarty.foreach.loopname.first}>
                        <td class="label" rowspan="<{$assessment_list[$g1.guideline_group1]|@count}>"><{$g1.name}></td>
                    <{/if}>
                    <td class="guideline"><{$asm.guideline|escape}><{if $asm.criterion}><br/><{$asm.criterion|escape}><{/if}></td>
                    <td class="assessment"><{$ldr.config.select[$asmkey].button|default:'?'}></td>
                    <{if $ldr.config.reason === '1'}><td class="reason"><{$asm.apply_reason|escape|nl2br}></td><{/if}>
                </tr>
            <{/foreach}>
        <{/foreach}>
    </tbody>
</table>

<{if $ldr.config.asm_comment_used === '1'}>
    <br/>
    <br/>
    <table border="1" cellpadding="5" cellspacing="0">
        <thead>
            <tr>
                <th width="540" style="text-align:center;"><{$ldr.config.asm_comment|default:'������'|escape}></th>
            </tr>
        </thead>
        <tr>
            <td width="540"><{$form7.knowledge|escape|nl2br}></td>
        </tr>
    </table>
<{/if}>

<{if $ldr.config.asm_comment2_used === '1'}>
    <br/>
    <br/>
    <table border="1" cellpadding="5" cellspacing="0">
        <thead>
            <tr>
                <th width="540" style="text-align:center;"><{$ldr.config.asm_comment2|default:'���ʤβ���'|escape}></th>
            </tr>
        </thead>
        <tr>
            <td width="540"><{$form7.decision|escape|nl2br}></td>
        </tr>
    </table>
<{/if}>
