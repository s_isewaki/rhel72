<table border="1" cellpadding="3" cellspacing="0">
    <tr>
        <td width="50" style="text-align: center;"><{$year|escape}>年度</td>
        <td width="200"><{$ladder->title($ladder_id)|escape}></td>
    </tr>
</table>
<br/>
<br/>

<{if $list}>
    <table border="1" cellpadding="2" cellspacing="0">
        <thead>
            <tr style="background-color:#d3d3d3;color:#000000;">
                <th width="120" align="center">職員</th>
                <th width="80" align="center">役職</th>
                <th width="70" align="center">経験年数</th>
                <th width="70" align="center">認定Lv</th>
                <th width="70" align="center">認定日</th>
                <th width="70" align="center">認定番号</th>
            </tr>
        </thead>
        <tbody>
            <{foreach item=row from=$list}>
                <tr>
                    <td width="120"><{$row->emp_name()|escape}></td>
                    <td width="80"><{$row->st_name()|escape}></td>
                    <td width="70" align="right"><{if $row->nurse_age()}><{$row->nurse_age()|escape}>年目<{/if}></td>
                    <td width="70" align="center">
                        <{if $row->level($ladder_id) === '0'}>
                            <{$ladder->level0($ladder_id)|escape}>
                        <{else}>
                            <{$row->level_roman($ladder_id)|escape}>
                        <{/if}>
                    </td>
                    <td width="70" align="center"><{$row->_getter('date')}></td>
                    <td width="70" align="right"><{$row->_getter('number')}></td>
                </tr>
            <{/foreach}>
        </tbody>
    </table>
<{/if}>
