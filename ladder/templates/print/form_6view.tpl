<table border="1" cellpadding="5" cellspacing="0">
    <tbody>
        <tr>
            <td width="70"  style="text-align: center;">申請レベル</td>
            <td width="50"  style="text-align: center; font-family: kozminproregular;"><{$apply->level_text()|escape}></td>
            <td width="50"  style="text-align: center;">評価日</td>
            <td width="120"  style="text-align: center;"><{$form6.updated_on|date_format_jp:'%Y年 %-m月 %-d日 (%a)'}></td>
        </tr>
    </tbody>
</table>
<br/>
<br/>

<table border="1" cellpadding="5" cellspacing="0">
    <thead>
        <tr>
            <th width="540" style="text-align:center;">事例内容</th>
        </tr>
    </thead>
    <tr>
        <td width="540"><{$form6.narrative|escape|nl2br}></td>
    </tr>
</table>
