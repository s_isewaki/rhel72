<style type="text/css">
    .data {
        width:500;
    }
    .label {
        width:20;
        text-align: center;
    }
    .assessment {
        width:30;
        text-align: center;
        font-size: 9pt;
    }
    .appraiser72 {
        width:90;
        font-size: 9pt;
    }
    .guideline {
        <{if $ldr.config.reason === '1'}>
            width:210;
        <{else}>
            width:400;
        <{/if}>
        font-size: 9pt;
    }
    .reason72 {
        width:190;
        font-size: 9pt;
    }
    .roman {
        font-family: "Times New Roman", serif;
    }
    .evaluation {
        width:520;
    }
    table th {
        text-align: center;
    }

</style>
<table border="1" cellpadding="3" cellspacing="0">
    <tr>
        <td width="50" style="text-align: center;">ɾ����</td>
        <td width="490"><{$emp->emp_name()|escape}></td>
    </tr>
</table>
<br/>
<br/>

<table border="1" cellpadding="2">
    <thead>
        <tr>
            <th class="label"></th>
            <th class="guideline">��ư��ɸ</th>
            <th class="appraiser72">ɾ����</th>
            <th class="assessment">ɾ��</th>
            <{if $ldr.config.reason === '1'}><th class="reason72">����</th><{/if}>
        </tr>
    </thead>
    <tbody>
        <{foreach item=g1 from=$group1_list name=loopg1}>
            <{foreach item=asm from=$assessment_list[$g1.guideline_group1] name=loopasm}>
                <{assign var='asmkey' value="_`$asm.apply_assessment`"}>
                <{assign var='acckey' value="_`$asm.accept_assessment`"}>
                <{if $ldr.config.app_view === '1'}>
                    <tr>
                        <{if $smarty.foreach.loopasm.first}>
                            <td class="label" rowspan="<{math equation="x * y" x=$assessment_list[$g1.guideline_group1]|@count y=2}>"><{$g1.name|escape}></td>
                        <{/if}>
                        <td class="guideline" rowspan="2"><{$asm.guideline|escape}><{if $asm.criterion}><br/><{$asm.criterion|escape}><{/if}></td>
                        <td class="appraiser72"><{$target_emp->emp_name()|escape}></td>

                        <td class="assessment"><{$ldr.config.select[$asmkey].button|default:'?'}></td>
                        <{if $ldr.config.reason === '1'}><td class="reason72"><{$asm.apply_reason|escape|nl2br}></td><{/if}>

                    </tr>
                    <tr>
                        <td class="appraiser72"><{$emp->emp_name()|escape}></td>
                        <td class="assessment"><{$ldr.config.select[$acckey].button|default:'?'}></td>
                        <{if $ldr.config.reason === '1'}><td class="reason72"><{$asm.accept_reason|escape|nl2br}></td><{/if}>
                    </tr>
                <{else}>
                    <tr>
                        <{if $smarty.foreach.loopasm.first}>
                            <td class="label" rowspan="<{$assessment_list[$g1.guideline_group1]|@count}>"><{$g1.name|escape}></td>
                        <{/if}>
                        <td class="guideline"><{$asm.guideline|escape}><{if $asm.criterion}><br/><{$asm.criterion|escape}><{/if}></td>
                        <td class="appraiser72"><{$emp->emp_name()|escape}></td>
                        <td class="assessment"><{$ldr.config.select[$acckey].button|default:'?'}></td>
                        <{if $ldr.config.reason === '1'}><td class="reason72"><{$asm.accept_reason|escape|nl2br}></td><{/if}>
                    </tr>
                <{/if}>
            <{/foreach}>
        <{/foreach}>
    </tbody>
</table>
<br>
<br>
<{if $ldr.config.app_view === '1'}>
    <table border="1" cellpadding="5" cellspacing="0">
        <thead>
            <tr>
                <th width="540" style="text-align:center;"><{$ldr.config.asm_comment|default:'������'|escape}></th>
            </tr>
        </thead>
        <tr>
            <td width="540"><{$apply_form7.knowledge|escape|nl2br}></td>
        </tr>
    </table>
    <br>
    <br>
<{/if}>
<table border="1" cellpadding="5" cellspacing="0">
    <thead>
        <tr>
            <th width="540" style="text-align:center;">ɾ���ԥ�����</th>
        </tr>
    </thead>
    <tr>
        <td width="540"><{$form7.advice|escape|nl2br}></td>
    </tr>
</table>
