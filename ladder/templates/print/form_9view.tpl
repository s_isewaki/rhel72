<style type="text/css">
    .label {
        width:70;
        text-align: left;
    }
    .data {
        width:440;
    }
    .level {
        font-family: "Times New Roman", serif;
    }
    .evaluation {
        width:540;
    }
    table th {
        text-align: center;
    }

    .domain {
        width:80;
    }
    .criterion {
        width:210;
    }
    .check {
        width:40;
        text-align: center
    }
    .note {
        width:210;
    }
</style>
<table border="1" cellpadding="5" cellspacing="0">
    <tbody>
        <tr>
            <td width="70"  style="text-align: center;">申請レベル</td>
            <td width="50"  style="text-align: center; font-family: kozminproregular;"><{$apply->level_text()|escape}></td>
            <td width="50"  style="text-align: center;">監査者</td>
            <td width="100"><{foreach item=aprll from=$auditor_list}><br><{$aprll|escape}><{/foreach}></td>
        </tr>
    </tbody>
</table>
<br/>
<br/>

<table border="1" cellpadding="2">
    <thead>
        <tr>
            <th class="domain">領域</th>
            <th class="criterion">基準</th>
            <th class="check">評価</th>
            <th class="note">備考</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="domain" rowspan="2"><{if $ldr.config.rc === '1'}>臨床看護実践<{else}>赤十字<{/if}></td>
            <td class="criterion">
                <{if $ldr.config.rc === '1'}>
                    1) 評価は、指標の「知識」、「判断」、「行為」、「行為の結果」に則って実施されているか
                <{else}>
                    1) 評価は、指標に則っているか
                <{/if}>
            </td>
            <td class="check"><{if $form9.clinical1==="t"}>○<{/if}></td>
            <td class="note"><{$form9.clinical1_note|escape|nl2br}></td>
        </tr>
        <tr>
            <td class="criterion">
                2) 評価内容に飛躍がないか
            </td>
            <td class="check"><{if $form9.clinical2==="t"}>○<{/if}></td>
            <td class="note"><{$form9.clinical2_note|escape|nl2br}></td>
        </tr>
        <tr>
            <td class="domain" rowspan="2"><{if $ldr.config.rc === '1'}>マネジメント<{else}>管理過程<{/if}></td>
            <td class="criterion">
                1) 評価は、指標に則っているか
            </td>
            <td class="check"><{if $form9.management1==="t"}>○<{/if}></td>
            <td class="note"><{$form9.management1_note|escape|nl2br}></td>
        </tr>
        <tr>
            <td class="criterion">
                2) 評価内容に飛躍がないか
            </td>
            <td class="check"><{if $form9.management2==="t"}>○<{/if}></td>
            <td class="note"><{$form9.management2_note|escape|nl2br}></td>
        </tr>
        <tr>
            <td class="domain" rowspan="2"><{if $ldr.config.rc === '1'}>教育・研究<{else}>意思決定<{/if}></td>
            <td class="criterion">
                1) 評価は、指標に則っているか
            </td>
            <td class="check"><{if $form9.education1==="t"}>○<{/if}></td>
            <td class="note"><{$form9.education1_note|escape|nl2br}></td>
        </tr>
        <tr>
            <td class="criterion">
                2) 評価内容に飛躍がないか
            </td>
            <td class="check"><{if $form9.education2==="t"}>○<{/if}></td>
            <td class="note"><{$form9.education2_note|escape|nl2br}></td>
        </tr>
        <tr>
            <td class="domain" rowspan="2"><{if $ldr.config.rc === '1'}>赤十字活動<{else}>質保証<{/if}></td>
            <td class="criterion">
                1) 評価は、指標に則っているか
            </td>
            <td class="check"><{if $form9.redcross1==="t"}>○<{/if}></td>
            <td class="note"><{$form9.redcross1_note|escape|nl2br}></td>
        </tr>
        <tr>
            <td class="criterion">
                2) 評価内容に飛躍がないか
            </td>
            <td class="check"><{if $form9.redcross2==="t"}>○<{/if}></td>
            <td class="note"><{$form9.redcross2_note|escape|nl2br}></td>
        </tr>

        <{if $ldr.config.rc === '2'}>
            <tr>
                <td class="domain" rowspan="2">人材育成</td>
                <td class="criterion">
                    1) 評価は、指標に則っているか
                </td>
                <td class="check"><{if $form9.comment51==="t"}>○<{/if}></td>
                <td class="note"><{$form9.comment51_note|escape|nl2br}></td>
            </tr>
            <tr>
                <td class="criterion">
                    2) 評価内容に飛躍がないか
                </td>
                <td class="check"><{if $form9.comment52==="t"}>○<{/if}></td>
                <td class="note"><{$form9.comment52_note|escape|nl2br}></td>
            </tr>

            <tr>
                <td class="domain" rowspan="2">対人関係</td>
                <td class="criterion">
                    1) 評価は、指標に則っているか
                </td>
                <td class="check"><{if $form9.comment61==="t"}>○<{/if}></td>
                <td class="note"><{$form9.comment61_note|escape|nl2br}></td>
            </tr>
            <tr>
                <td class="criterion">
                    2) 評価内容に飛躍がないか
                </td>
                <td class="check"><{if $form9.comment62==="t"}>○<{/if}></td>
                <td class="note"><{$form9.comment62_note|escape|nl2br}></td>
            </tr>

            <tr>
                <td class="domain" rowspan="2">セルフマネジメント</td>
                <td class="criterion">
                    1) 評価は、指標に則っているか
                </td>
                <td class="check"><{if $form9.comment71==="t"}>○<{/if}></td>
                <td class="note"><{$form9.comment71_note|escape|nl2br}></td>
            </tr>
            <tr>
                <td class="criterion">
                    2) 評価内容に飛躍がないか
                </td>
                <td class="check"><{if $form9.comment72==="t"}>○<{/if}></td>
                <td class="note"><{$form9.comment72_note|escape|nl2br}></td>
            </tr>
        <{/if}>
    </tbody>
</table>
<br/>
<br/>
<br/>
<table border="1" cellpadding="5">
    <thead>
        <tr>
            <th style="width:540">認定委員会としての活動や検討事項</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td style="width:540"><{$form9.problem|escape|nl2br}></td>
        </tr>
    </tbody>
</table>
