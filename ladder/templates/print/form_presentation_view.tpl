
<table border="1" cellpadding="2" cellspacing="0">
    <thead>
        <tr style="background-color:#d3d3d3;color:#000000;">
            <th width="100" align="center">演題</th>
            <th width="90" align="center">学会名</th>
            <th width="70" align="center">発表年月日</th>
            <th width="30" align="center">種別</th>
            <th width="30" align="center">単独<br/>共同</th>
            <th width="50" align="center">抄録<br/>ページ</th>
            <th width="170" align="center">概要</th>
        </tr>
    </thead>
    <tbody>
        <{if $list}>
            <{foreach item=row from=$list}>
                <tr>
                    <td width="100"><{$row.subject|escape}></td>
                    <td width="90"><{$row.title|escape}></td>
                    <td width="70" align="center"><{$row.period|escape}></td>
                    <td width="30"><{$types[$row.type]|escape}></td>
                    <td width="30"><{$units[$row.unit]|escape}></td>
                    <td width="50"><{$row.page|escape}></td>
                    <td width="170"><{$row.note|escape|nl2br}></td>
                </tr>
            <{/foreach}>
        <{else}>
            <tr>
                <td width="100"></td>
                <td width="90"></td>
                <td width="70"></td>
                <td width="30"></td>
                <td width="30"></td>
                <td width="50"></td>
                <td width="170"></td>
            </tr>
        <{/if}>
    </tbody>
</table>
