<style type="text/css">
    .level {
        font-family: "Times New Roman", serif;
    }
    td {
        font-size: 9pt;
    }
    td.title {
        text-align: center;
        background-color: #E0E0E0;
    }
</style>
<{foreach item=inside from=$data}>
    <tcpdf method="AddPage" />
    <{$inside.year}>年度 研修番号: <{$inside.no}><br/>
    <div align="right" style="width:550px;">
        企画者:
        <{$inside.planner}>
        <{if ! $inside.planner}>
            <{foreach item=row from=$inside.emp_planner name=planner}>
                <{if $row.method}>
                    <{if ! $smarty.foreach.planner.first}>,<{/if}>
                    <{$row}>
                <{/if}>
            <{/foreach}>
        <{/if}>
    　</div>
    <table border="1" cellpadding="2" cellspacing="0">
        <tbody>
            <tr>
                <td width="80" class="title">研修会名</td>
                <td colspan="3" width="470"><{$inside.title|escape}></td>
            </tr>

            <{if $inside.theme}>
                <tr>
                    <td class="title">テーマ</td>
                    <td colspan="3"><{$inside.theme|escape}></td>
                </tr>
            <{/if}>

            <tr>
                <td class="title">
                    <{if $hospital_type === '1'}>
                        めざすレベル
                    <{else}>
                        対象レベル
                    <{/if}>
                </td>
                <td colspan="3"><{if $inside.level}><span class="level"><{$roman_num[$inside.level]}></span><{else}>なし<{/if}></td>
            </tr>

            <tr>
                <td class="title">めざす状態</td>
                <td colspan="3">
                    <{foreach item=guideline from=$inside.guideline name=guideline}>
                        <{$guideline|escape}><br>
                    <{/foreach}>
                    <{if $inside.guideline_text}>
                        <{$inside.guideline_text|nl2br}>
                    <{/if}>
                </td>
            </tr>

            <tr>
                <td class="title">研修のねらい</td>
                <td colspan="3">
                    <{foreach item=row from=$inside.contents name=aim}>
                        <{if $row.aim}>
                            <{$row.number|escape}> . <{$row.aim|nl2br}>
                            <{if ! $smarty.foreach.aim.last}><br><{/if}>
                        <{/if}>
                    <{/foreach}>
                </td>
            </tr>

            <tr>
                <td class="title">研修目標</td>
                <td colspan="3">
                    <{foreach item=row from=$inside.contents name=objective}>
                        <{if $row.objective}>
                            <{$row.number|escape}> . <{$row.objective|replace:"\n":"\n　 . "|nl2br}>
                            <{if ! $smarty.foreach.objective.last}><br><{/if}>
                        <{/if}>
                    <{/foreach}>
                </td>
            </tr>

            <tr>
                <td class="title">研修内容</td>
                <td colspan="3">
                    <{foreach item=row from=$inside.contents name=contents}>
                        <{if $row.contents}>
                            <{$row.number|escape}> . <{$row.contents|replace:"\n":"\n　 . "|nl2br}>
                            <{if ! $smarty.foreach.contents.last}><br><{/if}>
                        <{/if}>
                    <{/foreach}>
                </td>
            </tr>

            <tr>
                <td class="title">研修方法</td>
                <td colspan="3">
                    <{foreach item=row from=$inside.contents name=method}>
                        <{if $row.method}>
                            <{$row.number|escape}> . <{$row.method|replace:"\n":"\n　 . "|nl2br}>
                            <{if ! $smarty.foreach.method.last}><br><{/if}>
                        <{/if}>
                    <{/foreach}>
                </td>
            </tr>

            <tr>
                <td class="title">指導者</td>
                <td colspan="3">
                    <{$inside.coach}>
                    <{if ! $inside.coach}>
                        <{foreach item=row from=$inside.emp_coach name=coach}>
                            <{if $row.method}>
                                <{if ! $smarty.foreach.coach.first}>,<{/if}>
                                <{$row}>
                            <{/if}>
                        <{/foreach}>
                    <{/if}>
                </td>
            </tr>

            <tr>
                <td width="80" class="title">時期・時間</td>
                <td width="300">
                    <{assign var=back_number value=0}>
                    <{foreach item=row from=$inside.dates}>
                        <{if $back_number != $row.number}>
                            <{$row.number|escape}> .
                        <{else}>　<{/if}>
                        <{if $row.type === '1'}>
                            <{$row.training_date|date_format_jp:"%-m月%-d日"}> <{$row.training_start_time|date_format:"%R"}> 〜 <{$row.training_last_time|date_format:"%R"}><br>
                        <{else}>
                            <{$row.accept_date|date_format_jp:"%-m月"}> (<{$ojt_name|escape}>)<br>
                        <{/if}>
                        <{assign var=back_number value=$row.number}>
                    <{/foreach}>
                </td>
                <td width="50" class="title">外部参加</td>
                <td width="120"><{if $inside.open}>可<{else}>不可<{/if}></td>
            </tr>

            <tr>
                <td class="title">実施上の留意点</td>
                <td colspan="3"><{$inside.notes|nl2br}></td>
            </tr>

            <tr>
                <td class="title">評価</td>
                <td colspan="3"><{$inside.assessment|nl2br}></td>
            </tr>
        </tbody>
    </table>
<{/foreach}>