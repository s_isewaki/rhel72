<style type="text/css">
    .level {
        font-family: "Times New Roman", serif;
    }
</style>

<{if $function.form_51}>
    <span class="level">I</span>. <{$titles.form_51.detail|escape}><br/>
    <table border="1" cellpadding="2" cellspacing="0">
        <thead>
            <tr style="background-color:#d3d3d3;color:#000000;">
                <th width="140" align="center">����</th>
                <th width="150" align="center">��°����</th>
                <th width="250" align="center">���</th>
            </tr>
        </thead>
        <tbody>
            <{if $list51}>
                <{foreach item=row from=$list51}>
                    <tr>
                        <td width="140" align="center"><{$row.start_date|escape}> �� <{$row.last_date|escape}></td>
                        <td width="150"><{$row.class|escape}></td>
                        <td width="250"><{$row.role|escape|nl2br}></td>
                    </tr>
                <{/foreach}>
            <{else}>
                <tr>
                    <td width="140"></td>
                    <td width="150"></td>
                    <td width="250"></td>
                </tr>
            <{/if}>
        </tbody>
    </table>
    <br/>
    <br/>
<{/if}>

<{if $function.form_52}>
    <span class="level">II</span>. <{$titles.form_52.detail|escape}><br/>
    <table border="1" cellpadding="2" cellspacing="0">
        <thead>
            <tr style="background-color:#d3d3d3;color:#000000;">
                <th width="140" align="center">����</th>
                <th width="40" align="center">����</th>
                <th width="130" align="center">�о�</th>
                <th width="230" align="center">�ơ��ޡ�����</th>
            </tr>
        </thead>
        <tbody>
            <{if $list52}>
                <{foreach item=row from=$list52}>
                    <tr>
                        <td width="140" align="center"><{$row.start_date|escape}> �� <{$row.last_date|escape}></td>
                        <td width="40" align="center"><{$f52_types[$row.type]|escape}></td>
                        <td width="130"><{$row.subject|escape}></td>
                        <td width="230"><{$row.theme|escape|nl2br}></td>
                    </tr>
                <{/foreach}>
            <{else}>
                <tr>
                    <td width="140"></td>
                    <td width="40"></td>
                    <td width="130"></td>
                    <td width="230"></td>
                </tr>
            <{/if}>
        </tbody>
    </table>
    <br/>
    <br/>
<{/if}>

<{if $function.form_53}>
    <span class="level">III</span>. <{$titles.form_53.detail|escape}><br/>
    <table border="1" cellpadding="2" cellspacing="0">
        <thead>
            <tr style="background-color:#d3d3d3;color:#000000;">
                <th width="140" align="center">����</th>
                <td width="40" align="center">����</td>
                <th width="360" align="center">̾��</th>
            </tr>
        </thead>
        <tbody>
            <{if $list53}>
                <{foreach item=row from=$list53}>
                    <tr>
                        <td width="140" align="center"><{$row.start_date|escape}> �� <{$row.last_date|escape}></td>
                        <td width="40" align="center"><{$f53_types[$row.type]|escape}></td>
                        <td width="360"><{$row.title|escape|nl2br}></td>
                    </tr>
                <{/foreach}>
            <{else}>
                <tr>
                    <td width="140"></td>
                    <td width="40"></td>
                    <td width="360"></td>
                </tr>
            <{/if}>
        </tbody>
    </table>
    <br/>
    <br/>
<{/if}>

<{if $function.form_54}>
    <span class="level">IV</span>. <{$titles.form_54.detail|escape}><br/>
    <table border="1" cellpadding="2" cellspacing="0">
        <thead>
            <tr style="background-color:#d3d3d3;color:#000000;">
                <th width="140" align="center">����</th>
                <th width="400" align="center">̾��</th>
            </tr>
        </thead>
        <tbody>
            <{if $list54}>
                <{foreach item=row from=$list54}>
                    <tr>
                        <td width="140" align="center"><{$row.start_date|escape}> �� <{$row.last_date|escape}></td>
                        <td width="400"><{$row.title|escape|nl2br}></td>
                    </tr>
                <{/foreach}>
            <{else}>
                <tr>
                    <td width="140"></td>
                    <td width="400"></td>
                </tr>
            <{/if}>
        </tbody>
    </table>
<{/if}>