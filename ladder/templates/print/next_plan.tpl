<table border="1" cellpadding="3" cellspacing="0">
    <tr>
        <td width="50" style="text-align: center;"><{$year|escape}>年度</td>
    </tr>
</table>
<br/>
<br/>

<b>現在の役割・仕事</b><br>
<table border="1" cellpadding="2" cellspacing="0">
    <tr>
        <td width="140">現在の役割</td>
        <td width="400"><{$list.role|escape|nl2br}></td>
    </tr>
    <tr>
        <td width="140">自分の得意とすること</td>
        <td width="400"><{$list.special_skill|escape|nl2br}></td>
    </tr>
    <tr>
        <td width="140">強化したいこと</td>
        <td width="400"><{$list.strengthen_skill|escape|nl2br}></td>
    </tr>
    <tr>
        <td width="140">今、学習していること</td>
        <td width="400"><{$list.studying_skill|escape|nl2br}></td>
    </tr>
</table>
<br/>
<br/>

<b>来年度の希望</b><br>
<table border="1" cellpadding="2" cellspacing="0">
    <{if $list.current_department !== null}>
        <tr>
            <td width="140">現在の部署で継続したいこと</td>
            <td width="400"><{$list.current_department|escape|nl2br}></td>
        </tr>
    <{/if}>
    <{if $list.hospital_transfer !== null}>
        <tr>
            <td width="140">院内異動してしたいこと</td>
            <td width="400"><{$list.hospital_transfer|escape|nl2br}></td>
        </tr>
    <{/if}>
    <{if $list.university_transfer !== null}>
        <tr>
            <td width="140" rowspan="2">学内異動してしたいこと</td>
            <td width="400">
                <{foreach item=value from=$list.university_transfer_destination}>
                    <{$university_list.$value|escape}>&nbsp;&nbsp;&nbsp;
                <{/foreach}>
            </td>
        </tr>
        <tr>
            <td width="400"><{$list.university_transfer|escape|nl2br}></td>
        </tr>
    <{/if}>
    <tr>
        <td width="140">その他</td>
        <td width="400"><{$list.etc|escape|nl2br}></td>
    </tr>
</table>
<br/>
<br/>

<b>未来の自分ビジョン（こういう自分であるためにどう行動するか？）</b><br/>
<table border="1" cellpadding="2" cellspacing="0">
    <tr>
        <td width="540"><{$list.action|escape|nl2br}></td>
    </tr>
</table>
