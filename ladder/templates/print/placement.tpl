<style type="text/css">
    .level {
        font-family: "Times New Roman", serif;
    }
    .class1 {
        font-size: 8;
        width:90;
    }
    .class2 {
        font-size: 8;
        width:180;
    }
    .class3 {
        font-size: 8;
        width:270;
    }
    .class4 {
        font-size: 8;
        width:360;
    }
    .head {
        width:65;
        font-size: 7;
        text-align: center;
        background-color:#d3d3d3;
        color:#000000;
    }
    .head2 {
        width:25;
        font-size: 7;
        text-align: center;
        background-color:#d3d3d3;
        color:#000000;
    }
    .head3 {
        width:40;
        font-size: 9;
        text-align: center;
        background-color:#d3d3d3;
        color:#000000;
    }
    .num {
        text-align: right;
        font-size: 9;
    }
    table {
        margin:0;
    }
    td {
        /* font-size:0.7em; */
    }
</style>

<table border="1" cellpadding="3" cellspacing="0">
    <thead>
        <tr style="background-color:#d3d3d3;color:#000000;">
            <th class="head" colspan="2">
            </th>
            <{foreach item=class from=$list_class}>
                <th align="center" class="class<{$class.cell}>">
                    <{$class.name|escape}>
                </th>
            <{/foreach}>
        </tr>
    </thead>
    <tbody>
        <{foreach item=st from=$list_st}>
            <tr>
                <th class="head" colspan="2">
                    <strong><{$st.st_nm|truncate:12:''|escape}></strong>
                    <{section name=br loop=$max_status[$st.st_id] start=1}><br/><{/section}>
                </th>
                <{foreach item=class from=$list_class}>
                    <td class="class<{$class.cell}>">
                        <{if $max_status[$st.st_id] < count($list[$class.key][$st.st_id])}>
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <{foreach item=row from=$list[$class.key][$st.st_id] name=emp}>
                                            <{if !$smarty.foreach.emp.first && ($smarty.foreach.emp.index % $max_status[$st.st_id] === 0)}>
                                            </td>
                                            <td>
                                            <{/if}>
                                            <{if !$smarty.foreach.emp.first || ($smarty.foreach.emp.index % $max_status[$st.st_id] !== 0)}><br><{/if}>
                                            <{foreach item=ldr from=$ladder->lists()}>
                                                <{assign var="level" value="level`$ldr.ladder_id`"}>
                                                <{assign var="lvn" value=$row.$level}>
                                                <{if $lvn}>[<{$ldr.short}><{$lvn}>]<{/if}>
                                            <{/foreach}>
                                            <{$row.name}>
                                        <{/foreach}>
                                    </td>
                                </tr>
                            </table>
                        <{else}>
                            <{foreach item=row from=$list[$class.key][$st.st_id] name=emp}>
                                <{if !$smarty.foreach.emp.first}><br><{/if}>
                                <{foreach item=ldr from=$ladder->lists()}>
                                    <{assign var="level" value="level`$ldr.ladder_id`"}>
                                    <{assign var="lvn" value=$row.$level}>
                                    <{if $lvn}>[<{$ldr.short}><{$lvn}>]<{/if}>
                                <{/foreach}>
                                <{$row.name}>
                            <{/foreach}>
                        <{/if}>
                    </td>
                <{/foreach}>
            </tr>
        <{/foreach}>

        <{foreach item=ldr from=$ladder->lists()}>
            <{assign var="level" value="level`$ldr.ladder_id`"}>
            <{assign var="lvn" value=$row.$level}>
            <tr>
                <th class="head2">
                    <{$ldr.title|escape}>

                </th>
                <th class="head3">
                    <{foreach item=lv from=$ldr.config.level|@array_reverse}>
                        <br><{$roman_num[$lv]|default:$ldr.config.level0|default:'0'|escape}>
                    <{/foreach}>
                    <br>無し
                </th>
                <{foreach item=class from=$list_class}>
                    <td class="class<{$class.cell}> num">
                        <{assign var="ldrn" value="ldr`$ldr.ladder_id`"}>
                        <{foreach name=lvcount item=lv from=$ldr.config.level|@array_reverse}>
                            <{assign var="lvn" value="lv`$lv`"}>
                            <br><{$class.cnt.$ldrn.$lvn|default:0|escape}> 名
                        <{/foreach}>
                        <br><{$class.cnt.$ldrn.lv|default:0|escape}> 名
                    </td>
                <{/foreach}>
            </tr>
        <{/foreach}>
        <tr>
            <th class="head">
                合計
            </th>
            <{foreach item=class from=$list_class}>
                <td class="class<{$class.cell}> num">
                    <{$class.cnt.sum}> 名
                </td>
            <{/foreach}>
        </tr>
    </tbody>
</table>
