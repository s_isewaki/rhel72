<style type="text/css">
    .label {
        width:100;
        text-align: center;
    }
    .assessment {
        width:30;
        text-align: center;
        font-size: 9pt;
    }
    .guideline {
        width:410;
        font-size: 9pt;
    }
    table th {
        text-align: center;
    }
</style>
<table border="1" cellpadding="5" cellspacing="0">
    <tbody>
        <tr>
            <td width="70"  style="text-align: center;">申請レベル</td>
            <td width="50"  style="text-align: center; font-family: kozminproregular;"><{$apply->level_text()|escape}></td>
            <td width="50"  style="text-align: center;">評価日</td>
            <td width="120"  style="text-align: center;"><{$form6.updated_on|date_format_jp:'%Y年 %-m月 %-d日 (%a)'}></td>
        </tr>
    </tbody>
</table>
<br/>
<br/>

<table border="1" cellpadding="5" cellspacing="0">
    <thead>
        <tr>
            <th width="540" style="text-align:center;">事例内容</th>
        </tr>
    </thead>
    <tr>
        <td width="540"><{$form6.narrative|escape|nl2br}></td>
    </tr>
</table>
<tcpdf method="AddPage" />
<table border="1" cellpadding="5" cellspacing="0">
    <tbody>
        <tr>
            <td width="50"  style="text-align: center;">評価者</td>
            <td width="100"  style="text-align: center;"><{$assessment.emp_name|escape}></td>
            <td width="50"  style="text-align: center;">評価日</td>
            <td width="120"  style="text-align: center;"><{$assessment.updated_on|date_format_jp:'%Y年 %-m月 %-d日 (%a)'}></td>
        </tr>
    </tbody>
</table>
<br/>
<br/>

<table border="1" cellpadding="2" cellspacing="0">
    <thead>
        <tr>
            <th class="label">項目</th>
            <th class="guideline">内容</th>
            <th class="assessment">評価</th>
        </tr>
    </thead>
    <tbody>
        <{foreach item=asm from=$assessment.list name=loopname}>
            <{assign var='asmkey' value="_`$asm.apply_assessment`"}>
            <tr>
                <{if $asm.guideline_group2 !== $back_g2}>
                    <td class="label" rowspan="<{$asm.group2_count|escape}>"><{$asm.group2_name|escape}></td>
                    <{assign var="back_g2" value=`$asm.guideline_group2`}>
                <{/if}>
                <td class="guideline"><{$asm.guideline|escape}></td>
                <td class="assessment"><{$ldr.config.report_select[$asmkey].button|default:'?'}></td>
            </tr>
        <{/foreach}>
    </tbody>
</table>
<br>
<br>
<table border="1" cellpadding="5" cellspacing="0">
    <thead>
        <tr>
            <th width="540" style="text-align:center;">評価者からのメッセージ</th>
        </tr>
    </thead>
    <tr>
        <td width="540"><{$assessment.comment|escape|nl2br}></td>
    </tr>
</table>
<br>
<br>
<table border="1" cellpadding="5" cellspacing="0">
    <thead>
        <tr>
            <th width="540" style="text-align:center;">教育課からのメッセージ</th>
        </tr>
    </thead>
    <tr>
        <td width="540"><{$assessment.admin_comment|escape|nl2br}></td>
    </tr>
</table>
