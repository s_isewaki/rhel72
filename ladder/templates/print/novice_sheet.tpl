<style type="text/css">
    .title {
        font-size: 10;
        font-weight: bold;
    }
    .level {
        font-family: "Times New Roman", serif;
    }
    .text {
        font-size:7;
    }
    td {
        font-size:8;
    }
</style>
<table border="1" cellpadding="3" cellspacing="0">
    <tr>
        <td width="50" style="text-align: center;"><{$year|escape}>年度</td>
        <td width="490"> 教育担当者：
            <{foreach item=facilitator from=$facilitators}>
                <{$facilitator.name|escape}>&nbsp;&nbsp;
            <{/foreach}>
        </td>
    </tr>
</table>
<br/>
<br/>
<{foreach item=g1 from=$group1_list}>
    <span class="title"><{$g1.name|escape}></span>
    <div class="text" style="text-align:right;">
        <span class="level"><{$nlv[1]}></span>:できる&nbsp;&nbsp;
        <span class="level"><{$nlv[2]}></span>:指導の下でできる&nbsp;&nbsp;
        <span class="level"><{$nlv[3]}></span>:演習でできる&nbsp;&nbsp;
        <span class="level"><{$nlv[4]}></span>:知識としてわかる
    </div>
    <table border="1" cellpadding="3" cellspacing="0">
        <thead>
            <tr>
                <td rowspan="2" width="60"></td>
                <td rowspan="2" width="<{$text_width}>"></td>
                <td rowspan="2" width="40"></td>
                <{foreach item=d from=$date_list}>
                    <td colspan="2" width="30" style="text-align: center;">第<{$d.number|escape}>回<br/><{$d.last_date|date_format_jp:"%y/%-m"}></td>

                <{/foreach}>
            </tr>
            <tr>
                <{foreach item=d from=$date_list}>
                    <td width="15" style="text-align: center; <{if $d.number === $target_num && $target_ass === 'a'}>font-weight:bold;<{/if}>">自</td>
                    <td width="15" style="text-align: center; <{if $d.number === $target_num && $target_ass === 'f'}>font-weight:bold;<{/if}>">他</td>
                <{/foreach}>
            </tr>
        </thead>
        <tbody>
            <{foreach name=group2 item=g2 from=$group2_list[$g1.group1]}>
                <{foreach name=asm item=asm from=$assessment_list[$g1.group1][$g2.group2]}>
                    <tr>
                        <{if $smarty.foreach.asm.first}>
                            <td rowspan="<{$g2.count|escape}>" width="60" class="text"><{$g2.name|escape}></td>
                        <{/if}>
                        <td width="<{$text_width}>" class="text"><{$asm[1].guideline|escape}></td>
                        <{if $target_ass === 'a'}>
                            <{assign var=bar value=$asm[$target_num].assessment}>
                        <{else}>
                            <{assign var=bar value=$asm[$target_num].f_assessment}>
                        <{/if}>
                        <{math equation="5 - bar" bar=$bar assign=bar2}>
                        <td width="40"><{section name="bar" loop=$bar2}>■<{/section}></td>
                        <{foreach item=d from=$date_list}>
                            <{assign var=ass value=$asm[$d.number].assessment}>
                            <{assign var=fass value=$asm[$d.number].f_assessment}>
                            <td width="15" class="level text" style="text-align: center; <{if $d.number === $target_num && $target_ass === 'a'}>font-weight:bold;<{/if}>"><{if $novice_number === '1'}><{$roman_num[$ass]}><{else}><{$ass}><{/if}></td>
                            <td width="15" class="level text" style="text-align: center; <{if $d.number === $target_num && $target_ass === 'f'}>font-weight:bold;<{/if}>"><{if $novice_number === '1'}><{$roman_num[$fass]}><{else}><{$fass}><{/if}></td>
                        <{/foreach}>
                    </tr>
                <{/foreach}>
            <{/foreach}>
        </tbody>
    </table>
    <br/>
    <br/>
<{/foreach}>