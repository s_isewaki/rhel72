<style type="text/css">
    .level {
        font-family: "Times New Roman", serif;
    }
</style>

<{if $function.form_21}>
    <span class="level">I</span>. <{$titles.form_21.detailL|escape}><br/>
    <table border="1" cellpadding="2" cellspacing="0">
        <thead>
            <tr style="background-color:#d3d3d3;color:#000000;">
                <th width="140" align="center">����</th>
                <th width="200" align="center">���򡦸���̾��</th>
                <th width="200" align="center">���</th>
            </tr>
        </thead>
        <tbody>
            <{if $list21}>
                <{foreach item=row from=$list21}>
                    <tr>
                        <td width="140" align="center"><{$row.start_date|escape}> �� <{$row.last_date|escape}></td>
                        <td width="200"><{$row.title|escape}></td>
                        <td width="200"><{$row.organizer|escape}></td>
                    </tr>
                <{/foreach}>
            <{else}>
                <tr>
                    <td width="140"></td>
                    <td width="200"></td>
                    <td width="200"></td>
                </tr>
            <{/if}>
        </tbody>
    </table>
    <br/>
    <br/>
<{/if}>

<{if $function.form_22}>
    <span class="level">II</span>. ���⸦�����ְ���<br/>
    <table border="1" cellpadding="2" cellspacing="0">
        <thead>
            <tr style="background-color:#d3d3d3;color:#000000;">
                <th width="60" align="center">ǯ����</th>
                <th width="40" align="center">Lv</th>
                <th width="210" align="center">����̾</th>
                <th width="140" align="center">���</th>
                <th width="90" align="center">���ƥ���</th>
            </tr>
        </thead>
        <tbody>
            <{if $list22_1}>
                <{foreach item=row from=$list22_1}>
                    <tr>
                        <td width="60" align="center"><{$row.period|escape}></td>
                        <td width="40" align="center"><{if $row.training_id}><{$row.level_roman|default:'�ʤ�'|escape}><{/if}></td>
                        <td width="210"><{$row.title|escape}><{if $row.training_id<>0}>*<{/if}></td>
                        <td width="140"><{$row.organizer|escape}></td>
                        <td width="90"><{$row.category|escape}></td>
                    </tr>
                <{/foreach}>
            <{else}>
                <tr>
                    <td width="60"></td>
                    <td width="40"></td>
                    <td width="210"></td>
                    <td width="140"></td>
                    <td width="90"></td>
                </tr>
            <{/if}>
        </tbody>
    </table>
    <br/>
    <br/>
    <span class="level">II</span>. �����������ְ���<br/>
    <table border="1" cellpadding="2" cellspacing="0">
        <thead>
            <tr style="background-color:#d3d3d3;color:#000000;">
                <th width="70" align="center">ǯ����</th>
                <th width="220" align="center">����̾</th>
                <th width="150" align="center">���</th>
                <th width="100" align="center">���ƥ���</th>
            </tr>
        </thead>
        <tbody>
            <{if $list22_2}>
                <{foreach item=row from=$list22_2}>
                    <tr>
                        <td width="70" align="center"><{$row.period|escape}></td>
                        <td width="220"><{$row.title|escape}><{if $row.training_id<>0}>*<{/if}></td>
                        <td width="150"><{$row.organizer|escape}></td>
                        <td width="100"><{$row.category|escape}></td>
                    </tr>
                <{/foreach}>
            <{else}>
                <tr>
                    <td width="70"></td>
                    <td width="220"></td>
                    <td width="150"></td>
                    <td width="100"></td>
                </tr>
            <{/if}>
        </tbody>
    </table>
    <br/>
    <br/>
    <span class="level">II</span>. �Х�ƥ����ְ���<br/>
    <table border="1" cellpadding="2" cellspacing="0">
        <thead>
            <tr style="background-color:#d3d3d3;color:#000000;">
                <th width="70" align="center">ǯ����</th>
                <th width="270" align="center">����̾</th>
                <th width="200" align="center">���</th>
            </tr>
        </thead>
        <tbody>
            <{if $list22_3}>
                <{foreach item=row from=$list22_3}>
                    <tr>
                        <td width="70" align="center"><{$row.period|escape}></td>
                        <td width="270"><{$row.title|escape}></td>
                        <td width="200"><{$row.organizer|escape}></td>
                    </tr>
                <{/foreach}>
            <{else}>
                <tr>
                    <td width="70"></td>
                    <td width="270"></td>
                    <td width="200"></td>
                </tr>
            <{/if}>
        </tbody>
    </table>
    <br/>
    <br/>
<{/if}>

<{if $function.form_23}>
    <span class="level">III</span>. <{$titles.form_23.detailL|escape}><br/>
    <table border="1" cellpadding="2" cellspacing="0">
        <thead>
            <tr style="background-color:#d3d3d3;color:#000000;">
                <th width="70" align="center">ǯ����</th>
                <th width="110" align="center">����</th>
                <th width="50" align="center">ñ��</th>
                <th width="170" align="center">��������</th>
                <th width="140" align="center">���</th>
            </tr>
        </thead>
        <tbody>
            <{if $list23}>
                <{foreach item=row from=$list23}>
                    <tr>
                        <td width="70" align="center"><{$row.period|escape}></td>
                        <td width="110" align="center"><{$row.subject|escape}></td>
                        <td width="50" align="center" class="level"><{$row.unit_roman|escape}></td>
                        <td width="170"><{$row.contents|escape}></td>
                        <td width="140"><{$row.organizer|escape}></td>
                    </tr>
                <{/foreach}>
            <{else}>
                <tr>
                    <td width="70" align="center"></td>
                    <td width="110" align="center"></td>
                    <td width="50" align="center"></td>
                    <td width="170"></td>
                    <td width="140"></td>
                </tr>
            <{/if}>
        </tbody>
    </table>
<{/if}>
