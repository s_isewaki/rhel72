<style type="text/css">
    .level {
        font-family: "Times New Roman", serif;
    }
</style>
<b>�����ץ��ե�����</b><br/>
<table border="1" cellpadding="3" cellspacing="0">
    <tbody>
        <tr>
            <td width="150">´��ǯ (�пȹ�)</td>
            <td width="390"><{if $target_emp->graduation_year()}><{$target_emp->graduation_year()|escape}>ǯ<{/if}><{if $target_emp->school()}> (<{$target_emp->school()|escape}>)<{/if}></td>
        </tr>
        <tr>
            <td width="150">�Ǹ���ȵ�����ǯ��</td>
            <td width="390"><{if $target_emp->license_date()}><{$target_emp->license_date()|date_format_jp:"%Yǯ%-m��"}><{/if}></td>
        </tr>
        <tr>
            <td width="150">�׾�����ǯ��</td>
            <td width="390"><{if $target_emp->nurse_date()}><{$target_emp->nurse_date()|date_format_jp:"%Yǯ%-m��"}><{/if}></td>
        </tr>
        <tr>
            <td width="150">������°ǯ��</td>
            <td width="390"><{if $target_emp->hospital_date()}><{$target_emp->hospital_date()|date_format_jp:"%Yǯ%-m��"}><{/if}></td>
        </tr>
        <tr>
            <td width="150">������°ǯ��</td>
            <td width="390"><{if $target_emp->ward_date()}><{$target_emp->ward_date()|date_format_jp:"%Yǯ%-m��"}><{/if}></td>
        </tr>
        <tr>
            <td width="150">JNA����ֹ�</td>
            <td width="390"><{$target_emp->jna_id()|escape}></td>
        </tr>
        <tr>
            <td width="150">��ƻ�ܸ��Ǹ�����ֹ�</td>
            <td width="390"><{$target_emp->pref_na_id()|escape}></td>
        </tr>
    </tbody>
</table>
<br/>
<br/>

<{foreach from=$recordset key=key item=rec}>
    <{if $rec.php == 'form_21.php'}>
        <b><{$titles[$key].detailL|escape}></b><br/>
        <table border="1" cellpadding="2" cellspacing="0">
            <thead>
                <tr style="background-color:#d3d3d3;color:#000000;">
                    <th width="140" align="center">����</th>
                    <th width="200" align="center">���򡦸���̾��</th>
                    <th width="200" align="center">���</th>
                </tr>
            </thead>
            <tbody>
                <{if $list21}>
                    <{foreach item=row from=$list21}>
                        <tr>
                            <td width="140" align="center"><{$row.start_date|escape}> �� <{$row.last_date|escape}></td>
                            <td width="200"><{$row.title|escape}></td>
                            <td width="200"><{$row.organizer|escape}></td>
                        </tr>
                    <{/foreach}>
                <{else}>
                    <tr>
                        <td width="140"></td>
                        <td width="200"></td>
                        <td width="200"></td>
                    </tr>
                <{/if}>
            </tbody>
        </table>
        <br/>
        <br/>
    <{/if}>

    <{if $rec.php == 'form_22.php'}>
        <b>���⸦�����ְ���</b><br/>
        <table border="1" cellpadding="2" cellspacing="0">
            <thead>
                <tr style="background-color:#d3d3d3;color:#000000;">
                    <th width="60" align="center">ǯ����</th>
                    <th width="40" align="center">Lv</th>
                    <th width="210" align="center">����̾</th>
                    <th width="140" align="center">���</th>
                    <th width="90" align="center">���ƥ���</th>
                </tr>
            </thead>
            <tbody>
                <{if $list22_1}>
                    <{foreach item=row from=$list22_1}>
                        <tr>
                            <td width="60" align="center"><{$row.period|escape}></td>
                            <td width="40" align="center"><{if $row.training_id}><{$row.level_roman|default:'�ʤ�'|escape}><{/if}></td>
                            <td width="210"><{$row.title|escape}><{if $row.training_id<>0}>*<{/if}></td>
                            <td width="140"><{$row.organizer|escape}></td>
                            <td width="90"><{$row.category|escape}></td>
                        </tr>
                    <{/foreach}>
                <{else}>
                    <tr>
                        <td width="60"></td>
                        <td width="40"></td>
                        <td width="210"></td>
                        <td width="140"></td>
                        <td width="90"></td>
                    </tr>
                <{/if}>
            </tbody>
        </table>
        <br/>
        <br/>
        <b>�����������ְ���</b><br/>
        <table border="1" cellpadding="2" cellspacing="0">
            <thead>
                <tr style="background-color:#d3d3d3;color:#000000;">
                    <th width="70" align="center">ǯ����</th>
                    <th width="220" align="center">����̾</th>
                    <th width="150" align="center">���</th>
                    <th width="100" align="center">���ƥ���</th>
                </tr>
            </thead>
            <tbody>
                <{if $list22_2}>
                    <{foreach item=row from=$list22_2}>
                        <tr>
                            <td width="70" align="center"><{$row.period|escape}></td>
                            <td width="220"><{$row.title|escape}><{if $row.training_id<>0}>*<{/if}></td>
                            <td width="150"><{$row.organizer|escape}></td>
                            <td width="100"><{$row.category|escape}></td>
                        </tr>
                    <{/foreach}>
                <{else}>
                    <tr>
                        <td width="70"></td>
                        <td width="220"></td>
                        <td width="150"></td>
                        <td width="100"></td>
                    </tr>
                <{/if}>
            </tbody>
        </table>
        <br/>
        <br/>
        <b>�Х�ƥ����ְ���</b><br/>
        <table border="1" cellpadding="2" cellspacing="0">
            <thead>
                <tr style="background-color:#d3d3d3;color:#000000;">
                    <th width="70" align="center">ǯ����</th>
                    <th width="270" align="center">����̾</th>
                    <th width="200" align="center">���</th>
                </tr>
            </thead>
            <tbody>
                <{if $list22_3}>
                    <{foreach item=row from=$list22_3}>
                        <tr>
                            <td width="70" align="center"><{$row.period|escape}></td>
                            <td width="270"><{$row.title|escape}></td>
                            <td width="200"><{$row.organizer|escape}></td>
                        </tr>
                    <{/foreach}>
                <{else}>
                    <tr>
                        <td width="70"></td>
                        <td width="270"></td>
                        <td width="200"></td>
                    </tr>
                <{/if}>
            </tbody>
        </table>
        <br/>
        <br/>
    <{/if}>

    <{if $rec.php == 'form_23.php'}>
        <b><{$titles[$key].detailL|escape}></b><br/>
        <table border="1" cellpadding="2" cellspacing="0">
            <thead>
                <tr style="background-color:#d3d3d3;color:#000000;">
                    <th width="70" align="center">ǯ����</th>
                    <th width="110" align="center">����</th>
                    <th width="50" align="center">ñ��</th>
                    <th width="170" align="center">��������</th>
                    <th width="140" align="center">���</th>
                </tr>
            </thead>
            <tbody>
                <{if $list23}>
                    <{foreach item=row from=$list23}>
                        <tr>
                            <td width="70" align="center"><{$row.period|escape}></td>
                            <td width="110" align="center"><{$row.subject|escape}></td>
                            <td width="50" align="center" class="level"><{$row.unit_roman|escape}></td>
                            <td width="170"><{$row.contents|escape}></td>
                            <td width="140"><{$row.organizer|escape}></td>
                        </tr>
                    <{/foreach}>
                <{else}>
                    <tr>
                        <td width="70" align="center"></td>
                        <td width="110" align="center"></td>
                        <td width="50" align="center"></td>
                        <td width="170"></td>
                        <td width="140"></td>
                    </tr>
                <{/if}>
            </tbody>
        </table>
        <br/>
        <br/>
    <{/if}>


    <{if $rec.php == 'form_3.php'}>
        <b><{$titles[$key].detailL|escape}></b><br/>
        <table border="1" cellpadding="2" cellspacing="0">
            <thead>
                <tr style="background-color:#d3d3d3;color:#000000;">
                    <th width="70" align="center">ǯ��</th>
                    <th width="50" align="center">����</th>
                    <th width="250" align="center">����ȯɽ��(�ز�)̾</th>
                    <th width="170" align="center">���</th>
                </tr>
            </thead>
            <tbody>
                <{if $list3}>
                    <{foreach item=row from=$list3}>
                        <tr>
                            <td width="70" align="center"><{$row.period|escape}></td>
                            <td width="50" align="center"><{$f3_types[$row.type]|escape}></td>
                            <td width="250"><{$row.title|escape}></td>
                            <td width="170"><{$row.organizer|escape}></td>
                        </tr>
                    <{/foreach}>
                <{else}>
                    <tr>
                        <td width="70"></td>
                        <td width="50"></td>
                        <td width="250"></td>
                        <td width="170"></td>
                    </tr>
                <{/if}>
            </tbody>
        </table>
        <br/>
        <br/>
    <{/if}>


    <{if $rec.php == 'form_4.php'}>
        <b><{$titles[$key].detailL|escape}></b><br/>
        <table border="1" cellpadding="2" cellspacing="0">
            <thead>
                <tr style="background-color:#d3d3d3;color:#000000;">
                    <th width="70" align="center">ǯ����</th>
                    <th width="470" align="center">��̳������ȯɽ����Ƶ�Ͽ</th>
                </tr>
            </thead>
            <tbody>
                <{if $list4}>
                    <{foreach item=row from=$list4}>
                        <tr>
                            <td width="70" align="center"><{$row.period|escape}></td>
                            <td width="470"><{$row.record|escape|nl2br}></td>
                        </tr>
                    <{/foreach}>
                <{else}>
                    <tr>
                        <td width="70" align="center"></td>
                        <td width="470"></td>
                    </tr>
                <{/if}>
            </tbody>
        </table>
        <br/>
        <br/>
    <{/if}>


    <{if $rec.php == 'form_51.php'}>
        <b><{$titles[$key].detailL|escape}></b><br/>
        <table border="1" cellpadding="2" cellspacing="0">
            <thead>
                <tr style="background-color:#d3d3d3;color:#000000;">
                    <th width="140" align="center">����</th>
                    <th width="150" align="center">��°����</th>
                    <th width="250" align="center">���</th>
                </tr>
            </thead>
            <tbody>
                <{if $list51}>
                    <{foreach item=row from=$list51}>
                        <tr>
                            <td width="140" align="center"><{$row.start_date|escape}> �� <{$row.last_date|escape}></td>
                            <td width="150"><{$row.class|escape}></td>
                            <td width="250"><{$row.role|escape|nl2br}></td>
                        </tr>
                    <{/foreach}>
                <{else}>
                    <tr>
                        <td width="140"></td>
                        <td width="150"></td>
                        <td width="250"></td>
                    </tr>
                <{/if}>
            </tbody>
        </table>
        <br/>
        <br/>
    <{/if}>

    <{if $rec.php == 'form_52.php'}>
        <b><{$titles[$key].detailL|escape}></b><br/>
        <table border="1" cellpadding="2" cellspacing="0">
            <thead>
                <tr style="background-color:#d3d3d3;color:#000000;">
                    <th width="140" align="center">����</th>
                    <th width="40" align="center">����</th>
                    <th width="130" align="center">�о�</th>
                    <th width="230" align="center">�ơ��ޡ�����</th>
                </tr>
            </thead>
            <tbody>
                <{if $list52}>
                    <{foreach item=row from=$list52}>
                        <tr>
                            <td width="140" align="center"><{$row.start_date|escape}> �� <{$row.last_date|escape}></td>
                            <td width="40" align="center"><{$f52_types[$row.type]|escape}></td>
                            <td width="130"><{$row.subject|escape}></td>
                            <td width="230"><{$row.theme|escape|nl2br}></td>
                        </tr>
                    <{/foreach}>
                <{else}>
                    <tr>
                        <td width="140"></td>
                        <td width="40"></td>
                        <td width="130"></td>
                        <td width="230"></td>
                    </tr>
                <{/if}>
            </tbody>
        </table>
        <br/>
        <br/>
    <{/if}>

    <{if $rec.php == 'form_53.php'}>
        <b><{$titles[$key].detailL|escape}></b><br/>
        <table border="1" cellpadding="2" cellspacing="0">
            <thead>
                <tr style="background-color:#d3d3d3;color:#000000;">
                    <th width="140" align="center">����</th>
                    <th width="40" align="center">����</th>
                    <th width="360" align="center">̾��</th>
                </tr>
            </thead>
            <tbody>
                <{if $list53}>
                    <{foreach item=row from=$list53}>
                        <tr>
                            <td width="140" align="center"><{$row.start_date|escape}> �� <{$row.last_date|escape}></td>
                            <td width="40" align="center"><{$f53_types[$row.type]|escape}></td>
                            <td width="360"><{$row.title|escape|nl2br}></td>
                        </tr>
                    <{/foreach}>
                <{else}>
                    <tr>
                        <td width="140"></td>
                        <td width="40"></td>
                        <td width="360"></td>
                    </tr>
                <{/if}>
            </tbody>
        </table>
        <br/>
        <br/>
    <{/if}>

    <{if $rec.php == 'form_54.php'}>
        <b><{$titles[$key].detailL|escape}></b><br/>
        <table border="1" cellpadding="2" cellspacing="0">
            <thead>
                <tr style="background-color:#d3d3d3;color:#000000;">
                    <th width="140" align="center">����</th>
                    <th width="400" align="center">̾��</th>
                </tr>
            </thead>
            <tbody>
                <{if $list54}>
                    <{foreach item=row from=$list54}>
                        <tr>
                            <td width="140" align="center"><{$row.start_date|escape}> �� <{$row.last_date|escape}></td>
                            <td width="400"><{$row.title|escape|nl2br}></td>
                        </tr>
                    <{/foreach}>
                <{else}>
                    <tr>
                        <td width="140"></td>
                        <td width="400"></td>
                    </tr>
                <{/if}>
            </tbody>
        </table>
        <br/>
        <br/>
    <{/if}>

    <{if $rec.php == 'form_carrier.php'}>
        <b><{$titles[$key].detailL|escape}></b><br/>
        <table border="1" cellpadding="2" cellspacing="0">
            <thead>
                <tr style="background-color:#d3d3d3;color:#000000;">
                    <th width="140" align="center">����</th>
                    <th width="200" align="center">����̾</th>
                    <th width="200" align="center">��̳����</th>
                </tr>
            </thead>
            <tbody>
                <{if $listcarrier}>
                    <{foreach item=row from=$listcarrier}>
                        <tr>
                            <td width="140" align="center"><{$row.start_date|escape}> �� <{$row.last_date|escape}></td>
                            <td width="200"><{$row.institution|escape}></td>
                            <td width="200"><{$row.detail|escape}></td>
                        </tr>
                    <{/foreach}>
                <{else}>
                    <tr>
                        <td width="140"></td>
                        <td width="200"></td>
                        <td width="200"></td>
                    </tr>
                <{/if}>
            </tbody>
        </table>
        <br/>
        <br/>
    <{/if}>

    <{if $rec.php == 'form_transfer.php'}>
        <b><{$titles[$key].detailL|escape}></b><br/>
        <table border="1" cellpadding="2" cellspacing="0">
            <thead>
                <tr style="background-color:#d3d3d3;color:#000000;">
                    <th width="70" align="center">ǯ��</th>
                    <th width="470" align="center">��ư��</th>
                </tr>
            </thead>
            <tbody>
                <{if $listtransfer}>
                    <{foreach item=row from=$listtransfer}>
                        <tr>
                            <td width="70" align="center"><{$row.period|escape}></td>
                            <td width="470"><{$row.transfer|escape}></td>
                        </tr>
                    <{/foreach}>
                <{else}>
                    <tr>
                        <td width="70" align="center"></td>
                        <td width="470"></td>
                    </tr>
                <{/if}>
            </tbody>
        </table>
        <br/>
        <br/>
    <{/if}>

    <{if $rec.php == 'form_presentation.php'}>
        <b><{$titles[$key].detailL|escape}></b><br/>
        <table border="1" cellpadding="2" cellspacing="0">
            <thead>
                <tr style="background-color:#d3d3d3;color:#000000;">
                    <th width="100" align="center">����</th>
                    <th width="90" align="center">�ز�̾</th>
                    <th width="70" align="center">ȯɽǯ����</th>
                    <th width="30" align="center">����</th>
                    <th width="30" align="center">ñ��<br/>��Ʊ</th>
                    <th width="50" align="center">��Ͽ<br/>�ڡ���</th>
                    <th width="170" align="center">����</th>
                </tr>
            </thead>
            <tbody>
                <{if $listpresent}>
                    <{foreach item=row from=$listpresent}>
                        <tr>
                            <td width="100"><{$row.subject|escape}></td>
                            <td width="90"><{$row.title|escape}></td>
                            <td width="70" align="center"><{$row.period|escape}></td>
                            <td width="30"><{$fp_types[$row.type]|escape}></td>
                            <td width="30"><{$fp_units[$row.unit]|escape}></td>
                            <td width="50"><{$row.page|escape}></td>
                            <td width="170"><{$row.note|escape|nl2br}></td>
                        </tr>
                    <{/foreach}>
                <{else}>
                    <tr>
                        <td width="100"></td>
                        <td width="90"></td>
                        <td width="70"></td>
                        <td width="30"></td>
                        <td width="30"></td>
                        <td width="50"></td>
                        <td width="170"></td>
                    </tr>
                <{/if}>
            </tbody>
        </table>
        <br/>
        <br/>
    <{/if}>

    <{if $rec.php == 'form_article.php'}>
        <b><{$titles[$key].detailL|escape}></b><br/>
        <table border="1" cellpadding="2" cellspacing="0">
            <thead>
                <tr style="background-color:#d3d3d3;color:#000000;">
                    <th width="100" align="center">����</th>
                    <th width="90" align="center">�Ǻܻ�</th>
                    <th width="70" align="center">�Ǻ�ǯ����</th>
                    <th width="30" align="center">ñ��<br/>��Ʊ</th>
                    <th width="50" align="center">�Ǻ�<br/>�ڡ���</th>
                    <th width="200" align="center">����</th>
                </tr>
            </thead>
            <tbody>
                <{if $listarticle}>
                    <{foreach item=row from=$listarticle}>
                        <tr>
                            <td width="100"><{$row.subject|escape}></td>
                            <td width="90"><{$row.title|escape}></td>
                            <td width="70" align="center"><{$row.period|escape}></td>
                            <td width="30"><{$fa_units[$row.unit]|escape}></td>
                            <td width="50"><{$row.page|escape}></td>
                            <td width="200"><{$row.note|escape|nl2br}></td>
                        </tr>
                    <{/foreach}>
                <{else}>
                    <tr>
                        <td width="100"></td>
                        <td width="90"></td>
                        <td width="70"></td>
                        <td width="30"></td>
                        <td width="50"></td>
                        <td width="200"></td>
                    </tr>
                <{/if}>
            </tbody>
        </table>
        <br/>
        <br/>
    <{/if}>
<{/foreach}>
