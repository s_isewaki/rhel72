<style type="text/css">
    .label {
        width:150;
        text-align: center;
        background-color: #cccccc;
    }
    .data {
        width:390;
    }
    .level {
        width:20;
    }
    .data2 {
        width:370;
    }
    .tdhead {
        text-align: center;
        background-color: #cccccc;
    }
    td.td1 {
        width:100;
        text-align: center;
        background-color: #cccccc;
    }
    td.td2 {
        width:120;
    }
    td.td3 {
        width:230;
    }
    td.td4 {
        width:90;
    }
</style>

<b>基本設定</b><br/>
<table border="1" cellpadding="3" cellspacing="0">
    <tbody>
        <tr>
            <td class="label">ラダー名称</td>
            <td class="data"><{$data.title|escape}></td>
        </tr>
        <tr>
            <td class="label">ラダー名称(小)</td>
            <td class="data"><{$data.short|escape}></td>
        </tr>
        <tr>
            <td class="label">申請の受付</td>
            <td class="data"><{if $data.config.open === '1' || !$data.config.open}>OK<{else}>NG<{/if}></td>
        </tr>
        <tr>
            <td class="label">申請の受付期間</td>
            <td class="data"><{$data.config.start}> 〜 <{$data.config.end}></td>
        </tr>
        <tr>
            <td class="label">サイドメニュー</td>
            <td class="data"><{$data.config.sidemenu}></td>
        </tr>
        <tr>
            <td class="label">サイドメニュー説明文</td>
            <td class="data"><{$data.config.sidesubmenu}></td>
        </tr>
        <tr>
            <td class="label">ヘッダメニュー</td>
            <td class="data"><{$data.config.headmenu}></td>
        </tr>
        <tr>
            <td class="label">ヘッダメニュー説明文</td>
            <td class="data"><{$data.config.headsubmenu}></td>
        </tr>
        <tr>
            <td class="label">行動指標</td>
            <td class="data"><{foreach item=row from=$revision_list}><{if $row.guideline_revision === $data.guideline_revision}><{$row.name|escape}><{/if}><{/foreach}></td>
        </tr>
        <tr>
            <td class="label">承認フロー</td>
            <td class="data"><{if $data.config.flow === '2' || !$data.config.flow}>標準<{elseif $data.config.flow === '1'}>タイプRC<{elseif $data.config.flow === '3'}>タイプJ<{/if}></td>
        </tr>
    </tbody>
</table>
<br/>
<br/>

<b>名称</b><br/>
<table border="1" cellpadding="2" cellspacing="0">
    <thead>
        <tr>
            <td class="td1 tdhead">項目</td>
            <td class="td2 tdhead">名称</td>
            <td class="td3 tdhead">短い説明<br/>長い説明</td>
            <td class="td4 tdhead">大ボタン<br/>小ボタン</td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="td1">ラダー申請書</td>
            <td class="td2"><{$data.config.sidemenu|escape}></td>
            <td class="td3"><{$data.config.sidesubmenu|escape}><br/><{$data.config.headsubmenu|escape}></td>
            <td class="td4"><{$data.config.form_1.btn|escape}><br/><{$data.config.form_1.btnS|escape}></td>
        </tr>
        <tr>
            <td class="td1">ナラティブ</td>
            <td class="td2"><{$data.config.form_6.title|escape}></td>
            <td class="td3"><{$data.config.form_6.detail|escape}><br/><{$data.config.form_6.detailL|escape}></td>
            <td class="td4"><{$data.config.form_6.btn|escape}><br/><{$data.config.form_6.btnS|escape}></td>
        </tr>
        <tr>
            <td class="td1">評価表</td>
            <td class="td2"><{$data.config.form_7.title|escape}></td>
            <td class="td3"><{$data.config.form_7.detail|escape}><br/><{$data.config.form_7.detailL|escape}></td>
            <td class="td4"><{$data.config.form_7.btn|escape}><br/><{$data.config.form_7.btnS|escape}></td>
        </tr>
        <tr>
            <td class="td1">総合評価</td>
            <td class="td2"><{$data.config.form_8.title|escape}></td>
            <td class="td3"><{$data.config.form_8.detail|escape}><br/><{$data.config.form_8.detailL|escape}></td>
            <td class="td4"><{$data.config.form_8.btn|escape}><br/><{$data.config.form_8.btnS|escape}></td>
        </tr>
        <tr>
            <td class="td1">監査表</td>
            <td class="td2"><{$data.config.form_9.title|escape}></td>
            <td class="td3"><{$data.config.form_9.detail|escape}><br/><{$data.config.form_9.detailL|escape}></td>
            <td class="td4"><{$data.config.form_9.btn|escape}><br/><{$data.config.form_9.btnS|escape}></td>
        </tr>
        <tr>
            <td class="td1">師長申込前</td>
            <td class="td2"><{$data.config.status[4]|escape}></td>
            <td class="td3"></td>
            <td class="td4"></td>
        </tr>
        <tr>
            <td class="td1">師長申込中</td>
            <td class="td2"><{$data.config.status[5]|escape}></td>
            <td class="td3"></td>
            <td class="td4"></td>
        </tr>
        <tr>
            <td class="td1">申込差し戻し</td>
            <td class="td2"><{$data.config.status[6]|escape}></td>
            <td class="td3"></td>
            <td class="td4"></td>
        </tr>
        <tr>
            <td class="td1">同僚評価中</td>
            <td class="td2"><{$data.config.status[8]|escape}></td>
            <td class="td3"></td>
            <td class="td4"></td>
        </tr>
        <tr>
            <td class="td1">評価会待ち</td>
            <td class="td2"><{$data.config.status[9]|escape}></td>
            <td class="td3"></td>
            <td class="td4"></td>
        </tr>
        <tr>
            <td class="td1">監査者設定</td>
            <td class="td2"><{$data.config.status[10]|escape}></td>
            <td class="td3"></td>
            <td class="td4"></td>
        </tr>
        <tr>
            <td class="td1">監査中</td>
            <td class="td2"><{$data.config.status[11]|escape}></td>
            <td class="td3"></td>
            <td class="td4"></td>
        </tr>
        <tr>
            <td class="td1">認定委員会</td>
            <td class="td2"><{$data.config.status[12]|escape}></td>
            <td class="td3"></td>
            <td class="td4"></td>
        </tr>
        <tr>
            <td class="td1">終了</td>
            <td class="td2"><{$data.config.status[0]|escape}></td>
            <td class="td3"></td>
            <td class="td4"></td>
        </tr>
        <tr>
            <td class="td1">申込取り下げ</td>
            <td class="td2"><{$data.config.status.m2|escape}></td>
            <td class="td3"></td>
            <td class="td4"></td>
        </tr>
        <{if $data.config.flow === '3'}>
            <tr>
                <td class="td1">レポート評価者設定</td>
                <td class="td2"><{$data.config.status[21]|escape}></td>
                <td class="td3"></td>
                <td class="td4"></td>
            </tr>
            <tr>
                <td class="td1">レポート評価</td>
                <td class="td2"><{$data.config.status[22]|escape}></td>
                <td class="td3"></td>
                <td class="td4"></td>
            </tr>
            <tr>
                <td class="td1">レポート評価確認</td>
                <td class="td2"><{$data.config.status[23]|escape}></td>
                <td class="td3"></td>
                <td class="td4"></td>
            </tr>
        <{/if}>
    </tbody>
</table>
<br/>
<br/>

<b>レベル</b><br/>
<table border="1" cellpadding="3" cellspacing="0">
    <tbody>
        <tr>
            <td class="label">使用するレベル</td>
            <td class="data"><{foreach item=level from=$data.config.level}>[<{$level|escape}>]&nbsp;&nbsp;<{/foreach}></td>
        </tr>
        <tr>
            <td class="label">レベル0の名称</td>
            <td class="data"><{$data.config.level0|escape}></td>
        </tr>
    </tbody>
</table>

<tcpdf method="AddPage" />
<b>文言</b><br/>
<table border="1" cellpadding="3" cellspacing="0">
    <tbody>
        <tr>
            <td class="label"><{$data.config.form_6.title|escape}>の説明文</td>
            <td class="data"><{$data.config.report_text|escape|nl2br}></td>
        </tr>
        <{foreach name=level item=level from=$data.config.level}>
            <tr>
                <{if $smarty.foreach.level.first}>
                    <td class="label" rowspan="<{$data.config.level|@count}>"><{$data.config.form_7.title|escape}><br/>レベルごとの説明文</td>
                    <{/if}>
                <td class="level">[<{$level|escape}>]</td>
                <td class="data2"><{$data.config.word[$level]|escape|nl2br}></td>
            </tr>
        <{/foreach}>
        <{foreach name=level item=level from=$data.config.level}>
            <tr>
                <{if $smarty.foreach.level.first}>
                    <td class="label" rowspan="<{$data.config.level|@count}>"><{$data.config.form_7.title|escape}><br/>レベルごとのチェック項目</td>
                    <{/if}>
                <td class="level">[<{$level|escape}>]</td>
                <td class="data2"><{$data.config.checklist[$level]|escape|nl2br}></td>
            </tr>
        <{/foreach}>
    </tbody>
</table>
<br/>
<br/>

<b>評価の選択肢</b><br/>
<table border="1" cellpadding="3" cellspacing="0">
    <thead>
        <tr>
            <td class="tdhead">ボタン</td>
            <td class="tdhead">値</td>
            <td class="tdhead">ボタン色</td>
        </tr>
    </thead>
    <tbody>
        <{foreach name=select item=row from=$data.config.select}>
            <tr>
                <td align="center"><{$row.button|escape}></td>
                <td align="center"><{$row.value|escape}></td>
                <td align="center"><{if $row.color === 'primary'}>青<{/if}>
                    <{if $row.color === 'info'}>水色<{/if}>
                    <{if $row.color === 'success'}>緑<{/if}>
                    <{if $row.color === 'warning'}>オレンジ<{/if}>
                    <{if $row.color === 'danger'}>赤<{/if}>
                    <{if $row.color === 'purple'}>紫<{/if}>
                </td>
            </tr>
        <{/foreach}>
    </tbody>
</table>
<br/>
<br/>

<b>他者評価</b><br/>
<table border="1" cellpadding="3" cellspacing="0">
    <tbody>
        <tr>
            <td class="label">他者評価の名称</td>
            <td class="data"><{$data.config.appr_name|escape}></td>
        </tr>
        <tr>
            <td class="label">他者評価者の設定</td>
            <td class="data"><{if $data.config.appr === '1'}>本人が申請時に設定する<{elseif $data.config.appr === '2'}>師長が承認時に設定する<{/if}></td>
        </tr>
        <tr>
            <td class="label">匿名評価者</td>
            <td class="data"><{if $data.config.anonymous === '1'}>匿名にする<{elseif $data.config.anonymous === '2'}>匿名にしない<{/if}></td>
        </tr>
        <{foreach name=level item=level from=$data.config.level}>
            <tr>
                <{if $smarty.foreach.level.first}>
                    <td class="label" rowspan="<{$data.config.level|@count}>">他者評価者の人数</td>
                <{/if}>
                <td class="level">[<{$level|escape}>]</td>
                <td class="data2"><{$data.config.appr_min[$level]|escape}>人 〜 <{$data.config.appr_max[$level]|escape}>人</td>
            </tr>
        <{/foreach}>
    </tbody>
</table>
<br/>
<br/>

<b>評価</b><br/>
<table border="1" cellpadding="3" cellspacing="0">
    <tbody>
        <tr>
            <td class="label">本人評価のコメント</td>
            <td class="data"><{if $data.config.asm_comment_required === '1'}>必須<{elseif $data.config.asm_comment_required === '0'}>必須ではない<{/if}></td>
        </tr>
        <tr>
            <td class="label">本人評価コメントの名称</td>
            <td class="data"><{$data.config.asm_comment|escape}></td>
        </tr>
        <tr>
            <td class="label">評価の根拠</td>
            <td class="data"><{if $data.config.reason === '1'}>表示する<{elseif $data.config.reason === '0'}>表示しない<{/if}></td>
        </tr>
        <tr>
            <td class="label">同僚評価の根拠必須</td>
            <td class="data"><{if $data.config.diff_reason === '1'}>必須とする<{elseif $data.config.diff_reason === '0'}>必須としない<{/if}></td>
        </tr>
    </tbody>
</table>
<br/>
<br/>

<{if $data.config.flow === '1'}><b>評価会</b><br/>
    <table border="1" cellpadding="3" cellspacing="0">
        <tbody>
            <tr>
                <td class="label">評価会会場・日時</td>
                <td class="data"><{if $data.config.meeting === '1'}>使用する<{elseif $data.config.meeting === '2'}>使用しない<{/if}></td>
            </tr>
            <tr>
                <td class="label">申請日と評価会の間隔</td>
                <td class="data"><{$data.config.appr_interval|escape}>日以上</td>
            </tr>
        </tbody>
    </table>
    <br/>
    <br/>
<{/if}>

<{if $data.config.flow === '1'}><b>司会者</b><br/>
    <table border="1" cellpadding="3" cellspacing="0">
        <thead>
            <tr>
                <th class="tdhead">レベル</th>
                <th class="tdhead">司会者の役職</th>
            </tr>
        </thead>
        <tbody>
            <{foreach item=level from=$data.config.level}>
                <tr>
                    <td align="center">[<{$level|escape}>]</td>
                    <td>
                        <{foreach item=st from=$status->lists()}>
                            <{if $data.config.appr_st[$level] && in_array($st.id, $data.config.appr_st[$level])}>[<{$st.name|escape}>]&nbsp;&nbsp;<{/if}>
                        <{/foreach}>
                    </td>
                </tr>
            <{/foreach}>
        </tbody>
    </table>
    <br/>
    <br/>
<{/if}>

<{if $data.config.flow === '3'}><b>レポート</b><br/>
    <table border="1" cellpadding="3" cellspacing="0">
        <tbody>
            <tr>
                <td class="label">レポート評価の指標</td>
                <td class="data"><{foreach item=row from=$revision_list}><{if $row.guideline_revision === $data.config.report_guideline}><{$row.name|escape}><{/if}><{/foreach}></td>
            </tr>
        </tbody>
    </table>
    <br/>
    <br/>
    <table border="1" cellpadding="3" cellspacing="0">
        <thead>
            <tr>
                <td class="tdhead">ボタン</td>
                <td class="tdhead">値</td>
                <td class="tdhead">ボタン色</td>
            </tr>
        </thead>
        <tbody>
            <{foreach name=select item=row from=$data.config.report_select}>
                <tr>
                    <td align="center"><{$row.button|escape}></td>
                    <td align="center"><{$row.value|escape}></td>
                    <td align="center"><{if $row.color === 'primary'}>青<{/if}>
                        <{if $row.color === 'info'}>水色<{/if}>
                        <{if $row.color === 'success'}>緑<{/if}>
                        <{if $row.color === 'warning'}>オレンジ<{/if}>
                        <{if $row.color === 'danger'}>赤<{/if}>
                        <{if $row.color === 'purple'}>紫<{/if}>
                    </td>
                </tr>
            <{/foreach}>
        </tbody>
    </table>
<{/if}>
