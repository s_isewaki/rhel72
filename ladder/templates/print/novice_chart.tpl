<style type="text/css">
    .title {
        font-size: 10;
        font-weight: bold;
    }
    .level {
        font-family: "Times New Roman", serif;
    }
    .text {
        font-size:7;
    }
    td {
        font-size:8;
    }
</style>
<table border="1" cellpadding="3" cellspacing="0">
    <tr>
        <td width="50" style="text-align: center;"><{$year|escape}>年度</td>
        <td width="50" style="text-align: center;">第<{$number|escape}>回</td>
        <td width="440"> 教育担当者：
            <{foreach item=facilitator from=$facilitators}>
                <{$facilitator.name|escape}>&nbsp;&nbsp;
            <{/foreach}>
        </td>
    </tr>
</table>
<br/>
<br/>
<br/>
<br/>
<img src="http://localhost/<{$comedix}>/ladder/novice_chart.php?emp_id=<{$novice->emp_id()|escape}>&year=<{$year|escape}>&number=<{$number|escape}>&antialias=1" width="950" height="500" border="0" />
