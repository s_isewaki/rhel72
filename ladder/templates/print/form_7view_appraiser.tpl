<style type="text/css">
    .data {
        width:500;
    }
    .label {
        width:20;
        text-align: center;
    }
    .assessment {
        width:30;
        text-align: center;
        font-size: 9pt;
    }
    .appraiser72 {
        width:90;
        font-size: 9pt;
    }
    .guideline {
        <{if $ldr.config.reason === '1'}>
            width:210;
        <{else}>
            width:400;
        <{/if}>
        font-size: 9pt;
    }
    .reason72 {
        width:190;
        font-size: 9pt;
    }
    .roman {
        font-family: "Times New Roman", serif;
    }
    .evaluation {
        width:520;
    }
    table th {
        text-align: center;
    }

</style>
<table border="1" cellpadding="3" cellspacing="0">
    <tr>
        <td width="50" style="text-align: center;">評価者</td>
        <td width="490"><{$emp->emp_name()|escape}></td>
    </tr>
</table>
<br/>
<br/>

<b><{if $ldr.config.rc === '1'}>臨床看護実践<{else}>看護管理実践<{/if}></b><br/>
<table  border="1" cellpadding="2" cellspacing="0">
    <thead>
        <tr>
            <th class="label"></th>
            <th class="evaluation">評価内容</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="label"><{if $ldr.config.rc === '1'}>知識<{else}>赤十字<{/if}></td>
            <td class="evaluation"><{$form7.knowledge|escape|nl2br}></td>
        </tr>
        <tr>
            <td class="label"><{if $ldr.config.rc === '1'}>判断<{else}>管理過程<{/if}></td>
            <td class="evaluation"><{$form7.decision|escape|nl2br}></td>
        </tr>
        <tr>
            <td class="label"><{if $ldr.config.rc === '1'}>行為<{else}>意思決定<{/if}></td>
            <td class="evaluation"><{$form7.action|escape|nl2br}></td>
        </tr>
        <tr>
            <td class="label"><{if $ldr.config.rc === '1'}>行為の結果<{else}>質保証<{/if}></td>
            <td class="evaluation"><{$form7.result|escape|nl2br}></td>
        </tr>

        <{if $ldr.config.rc === '2'}>
            <tr>
                <td class="label">人材育成</td>
                <td class="evaluation"><{$form7.comment5|escape|nl2br}></td>
            </tr>
            <tr>
                <td class="label">対人関係</td>
                <td class="evaluation"><{$form7.comment6|escape|nl2br}></td>
            </tr>
            <tr>
                <td class="label">セルフマネジメント</td>
                <td class="evaluation"><{$form7.comment7|escape|nl2br}></td>
            </tr>
        <{/if}>

    </tbody>
</table>
<tcpdf method="AddPage" />

<table border="1" cellpadding="3" cellspacing="0">
    <tr>
        <td width="50" style="text-align: center;">評価者</td>
        <td width="490"><{$emp->emp_name()|escape}></td>
    </tr>
</table>
<br/>
<br/>

<b>マネジメント、教育・研究、赤十字活動評価表</b>
<br/>
<table border="1" cellpadding="2">
    <thead>
        <tr>
            <th class="label"></th>
            <th class="guideline">行動指標</th>
            <th class="appraiser72">評価者</th>
            <th class="assessment">評価</th>
            <{if $ldr.config.reason === '1'}><th class="reason72">根拠</th><{/if}>
        </tr>
    </thead>
    <tbody>
        <{foreach item=g1 from=$group1_list name=loopg1}>
            <{foreach item=asm from=$assessment_list[$g1.guideline_group1] name=loopasm}>
                <{assign var='asmkey' value="_`$asm.apply_assessment`"}>
                <{assign var='acckey' value="_`$asm.accept_assessment`"}>
                <tr>
                    <{if $smarty.foreach.loopasm.first}>
                        <td class="label" rowspan="<{math equation="x * y" x=$assessment_list[$g1.guideline_group1]|@count y=2}>"><{$g1.name|escape}></td>
                    <{/if}>
                    <td class="guideline" rowspan="2"><{$asm.guideline|escape}><{if $asm.criterion}><br/><{$asm.criterion|escape}><{/if}></td>
                    <td class="appraiser72"><{$target_emp->emp_name()|escape}></td>
                    <td class="assessment"><{$ldr.config.select[$asmkey].button|default:'?'}></td>
                    <{if $ldr.config.reason === '1'}><td class="reason72"><{$asm.apply_reason|escape|nl2br}></td><{/if}>
                </tr>
                <tr>
                    <td class="appraiser72"><{$emp->emp_name()|escape}></td>
                    <td class="assessment"><{$ldr.config.select[$acckey].button|default:'?'}></td>
                    <{if $ldr.config.reason === '1'}><td class="reason72"><{$asm.accept_reason|escape|nl2br}></td><{/if}>
                </tr>
            <{/foreach}>
        <{/foreach}>
    </tbody>
</table>
<tcpdf method="AddPage" />

<table border="1" cellpadding="3" cellspacing="0">
    <tr>
        <td width="50" style="text-align: center;">評価者</td>
        <td width="490"><{$emp->emp_name()|escape}></td>
    </tr>
</table>
<br/>
<br/>

<b>総合レベルと評価者から申請者へのアドバイス</b>
<br/>
<br/>
総合レベル<br/>
<table  border="1" cellpadding="5">
    <tbody>
        <tr>
            <{if $form7.level === "1"}>
                <td>あなたの看護実践に対し申請レベルを認めます。</td>
            <{else}>
                <td>今回の申請レベルは保留にします。再チャレンジしてください。</td>
            <{/if}>
        </tr>
    </tbody>
</table>
<br/>
<br/>
<br/>
評価者から申請者へのアドバイス<br/>
<table  border="1" cellpadding="5">
    <thead>
        <tr>
            <th>さらにキャリアを高めるための努力目標</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><{$form7.advice|escape|nl2br}></td>
        </tr>
    </tbody>
</table>
