<style type="text/css">
    .appraiser71 {
        width:90;
    }
    .reason71 {
        width:430;
    }
    .appraiser73 {
        width:90;
    }
    .level73 {
        width:70;
        text-align: center;
    }
    .advice73 {
        width:380;
    }

    .label {
        width:20;
        text-align: center;
    }
    .assessment {
        width:30;
        text-align: center;
        font-size: 9pt;
    }
    .appraiser72 {
        width:90;
        font-size: 9pt;
    }
    .guideline {
        <{if $ldr.config.reason === '1'}>
            width:210;
        <{else}>
            width:400;
        <{/if}>
        font-size: 9pt;
    }
    .reason72 {
        width:190;
        font-size: 9pt;
    }
    table th {
        text-align: center;
    }
    .roman {
        font-family: "Times New Roman", serif;
    }
</style>
<b>�׾��Ǹ����</b>
<br/>
<table border="1" cellpadding="2">
    <thead>
        <tr>
            <th class="label"></th>
            <th class="appraiser71">ɾ����</th>
            <th class="reason71">ɾ������</th>
        </tr>
    </thead>
    <tbody>
        <{foreach item=row from=$form1.appr_list name=loopname}>
            <tr>
                <{if $smarty.foreach.loopname.first}>
                    <td class="label" rowspan="<{$form1.appr_list|@count}>">���Ρ�����</td>
                <{/if}>
                <td class="appraiser71"><{$row.emp_name|escape}></td>
                <td class="reason71"><{$form7[$row.emp_id].knowledge|escape|nl2br}></td>
            </tr>
        <{/foreach}>
        <{foreach item=row from=$form1.appr_list name=loopname}>
            <tr>
                <{if $smarty.foreach.loopname.first}>
                    <td class="label" rowspan="<{$form1.appr_list|@count}>">��Ƚ���ǡ�</td>
                <{/if}>
                <td class="appraiser71"><{$row.emp_name|escape}></td>
                <td class="reason71"><{$form7[$row.emp_id].decision|escape|nl2br}></td>
            </tr>
        <{/foreach}>
        <{foreach item=row from=$form1.appr_list name=loopname}>
            <tr>
                <{if $smarty.foreach.loopname.first}>
                    <td class="label" rowspan="<{$form1.appr_list|@count}>">���ԡ��١�</td>
                <{/if}>
                <td class="appraiser71"><{$row.emp_name|escape}></td>
                <td class="reason71"><{$form7[$row.emp_id].action|escape|nl2br}></td>
            </tr>
        <{/foreach}>
        <{foreach item=row from=$form1.appr_list name=loopname}>
            <tr>
                <{if $smarty.foreach.loopname.first}>
                    <{if $smarty.foreach.loopname.first}>
                        <td class="label" rowspan="<{$form1.appr_list|@count}>">�԰٤η��</td>
                    <{/if}>
                <{/if}>
                <td class="appraiser71"><{$row.emp_name|escape}></td>
                <td class="reason71"><{$form7[$row.emp_id].result|escape|nl2br}></td>
            </tr>
        <{/foreach}>
    </tbody>
</table>
<tcpdf method="AddPage" />
<b>�ޥͥ����ȡ����顦���桢�ֽ�����ưɾ��ɽ</b>
<br/>
<table border="1" cellpadding="2">
    <thead>
        <tr>
            <th class="label"></th>
            <th class="guideline">��ư��ɸ</th>
            <th class="appraiser72">ɾ����</th>
            <th class="assessment">ɾ��</th>
            <{if $ldr.config.reason === '1'}><th class="reason72">����</th><{/if}>
        </tr>
    </thead>
    <tbody>
        <{foreach item=g1 from=$group1_list}>
            <{foreach item=asm from=$assessment_list[$g1.guideline_group1] name=loopas}>
                <{foreach item=row from=$form1.appr_list_all name=loopname}>
                    <{assign var='asmkey' value="_`$asm[$row.emp_id].assessment`"}>
                    <tr>
                        <{if $smarty.foreach.loopas.first && $smarty.foreach.loopname.first}>
                            <td class="label" rowspan="<{$g1.item_cnt}>"><{$g1.name|escape}></td>
                        <{/if}>
                        <{if $smarty.foreach.loopname.first}>
                            <td class="guideline" rowspan="<{$form1.appr_list_all|@count}>"><{$asm[$row.emp_id].guideline|escape}></td>
                        <{/if}>
                        <td class="appraiser72"><{$row.emp_name|escape}></td>
                        <td class="assessment"><{$ldr.config.select[$asmkey].button|default:'?'}></td>
                        <{if $ldr.config.reason === '1'}><td class="reason72"><{$asm[$row.emp_id].reason|escape|nl2br}></td><{/if}>
                    </tr>
                <{/foreach}>
            <{/foreach}>
        <{/foreach}>
    </tbody>
</table>
<tcpdf method="AddPage" />
<b>�����٥��ɾ���Ԥ��鿽���ԤؤΥ��ɥХ���</b>
<br/>
<table border="1" cellpadding="2">
    <thead>
        <tr>
            <th class="appraiser73">ɾ����</th>
            <th class="level73">�����٥�</th>
            <th class="advice73">���ɥХ���</th>
        </tr>
    </thead>
    <tbody>
        <{foreach item=row from=$form1.appr_list name=loopname}>
            <tr>
                <td class="appraiser73"><{$row.emp_name|escape}></td>
                <td class="level73">
                    <{if $form7[$row.emp_id].level=='1'}>
                        ǧ��
                    <{elseif $form7[$row.emp_id].level=='2'}>
                        ��α
                    <{/if}>
                </td>
                <td class="advice73"><{$form7[$row.emp_id].advice|escape|nl2br}></td>
            </tr>
        <{/foreach}>
    </tbody>
</table>
