<table border="1" cellpadding="3" cellspacing="0">
    <tr>
        <td width="50" style="text-align: center;"><{$year|escape}>ǯ��</td>
        <td width="200"><{$ladder->title($ladder_id)|escape}></td>
        <td width="200"> ����
            <{foreach item=row from=$class_lists}>
                <{if $srh_class3 === $row.dept_id || $srh_class3 === "`$row.dept_id`_`$row.room_id`"}>
                    <{$row.dept_nm|escape}><{if $row.room_id}> &gt; <{$row.room_nm|escape}><{/if}>
                <{/if}>
            <{foreachelse}>
                ���٤�
            <{/foreach}>
        </td>
    </tr>
</table>
<br/>
<br/>

<{if $list}>
    <table border="1" cellpadding="2" cellspacing="0">
        <thead>
            <tr style="background-color:#d3d3d3;color:#000000;">
                <th width="100" align="center">����</th>
                <th width="80" align="center">��</th>
                <th width="50" align="center">�и�ǯ��</th>
                <th width="45" align="center">����Lv</th>
                <th width="63" align="center">������</th>
                <th width="45" align="center">����Lv</th>
                <th width="37" align="center">���</th>
                <th width="65" align="center">ǧ����</th>
                <th width="55" align="center">ǧ���ֹ�</th>
            </tr>
        </thead>
        <tbody>
            <{foreach item=row from=$list}>
                <tr>
                    <td width="100"><{$row->emp_name()|escape}></td>
                    <td width="80"><{$row->st_name()|escape}></td>
                    <td width="50" align="right"><{if $row->nurse_age()}><{$row->nurse_age()|escape}>ǯ��<{/if}></td>
                    <td width="45" align="center">
                        <{if $row->level($ladder_id) === '0'}>
                            <{$ladder->level0($ladder_id)|escape}>
                        <{else}>
                            <{$row->level_roman($ladder_id)|escape}>
                        <{/if}>
                    </td>
                    <td width="63" align="center"><{$row->_getter('apply_date')}></td>
                    <td width="45" align="center">
                        <{if $row->apply_level() === '0'}>
                            <{$ladder->level0($ladder_id)|escape}>
                        <{else}>
                            <{$row->apply_level_roman()|escape}>
                        <{/if}>
                    </td>
                    <td width="37" align="center">
                        <{if $row->_getter('status') === '0'}>
                            <{if $row->_getter('result') === 't'}>
                                ��
                            <{elseif $row->_getter('result') === 'f'}>
                                ��
                            <{/if}>
                        <{elseif $row->_getter('status') === '12'}>
                            <{if $ldr.config.flow === '1' && $row->_getter('f8_result') === '1' || $ldr.config.flow !== '1' && $row->_getter('f7_result') === '1'}>
                                ��(��)
                            <{else}>
                                ��(��)
                            <{/if}>
                        <{/if}>
                    </td>
                    <td width="65" align="center">
                        <{if $row->_getter('apply_level')}>
                            <{$row->lv_date($ladder_id, $row->_getter('apply_level'))}>
                        <{/if}>
                    </td>
                    <td width="55" align="right">
                        <{if $row->_getter('apply_level')}>
                            <{$row->lv_number($ladder_id, $row->_getter('apply_level'))}>
                        <{/if}>
                    </td>
                </tr>
            <{/foreach}>
        </tbody>
    </table>
<{/if}>
