<style type="text/css">
    .roman {
        font-family: "Times New Roman", serif;
    }
</style>
<table border="0" cellpadding="2">
    <tbody>
        <tr>
            <td width="80">����:</td>
            <td width="460"><{$list[0].branch|escape}></td>
        </tr>
        <tr>
            <td>����:</td>
            <td><{$list[0].hospital|escape}></td>
        </tr>
        <tr>
            <td>������:</td>
            <td><{$list[0].training_day|escape}></td>
        </tr>
        <tr>
            <td>����:</td>
            <td><{$sb.subject|escape}> <span class="roman"><{$roman_num[$ct.unit]|escape}></span></td>
        </tr>
        <tr>
            <td>��������:</td>
            <td><{$ct.contents|escape}></td>
        </tr>
    </tbody>
</table>
<br/>
<br/>
<br/>
<table border="1" cellpadding="2" cellspacing="0">
    <thead>
        <tr style="background-color:#d3d3d3;color:#000000;">
            <th width="230" align="center">����</th>
            <th width="60" align="center">����ID</th>
            <th width="100" align="center">̾��</th>
            <th width="90" align="center">������</th>
            <th width="60" align="center">���ơ�����</th>
        </tr>
    </thead>
    <tbody>
        <{if $list}>

            <{foreach item=row from=$list}>
                <tr>
                    <td width="230">
                        <{$row.class_full_name|escape}>
                    </td>
                    <td width="60"><{$row.emp_personal_id|escape}></td>
                    <td width="100" ><{$row.name|escape}></td>
                    <td width="90" align="center"><{$row.created_on|date_format_jp:"%Yǯ%-m��%-d��(%a) %R"}></td>
                    <td width="60" align="center"><{$row.status_text}></td>
                </tr>
            <{/foreach}>
        <{else}>
            <tr>
                <td width="230"></td>
                <td width="60"></td>
                <td width="100"></td>
                <td width="90"></td>
                <td width="60"></td>
            </tr>
        <{/if}>
    </tbody>
</table>
<br/>
<br/>
