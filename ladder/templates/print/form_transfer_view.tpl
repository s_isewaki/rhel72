
<table border="1" cellpadding="2" cellspacing="0">
    <thead>
        <tr style="background-color:#d3d3d3;color:#000000;">
            <th width="70" align="center">年月</th>
            <th width="470" align="center">異動先</th>
        </tr>
    </thead>
    <tbody>
        <{if $list}>
            <{foreach item=row from=$list}>
                <tr>
                    <td width="70" align="center"><{$row.period|escape}></td>
                    <td width="470"><{$row.transfer|escape}></td>
                </tr>
            <{/foreach}>
        <{else}>
            <tr>
                <td width="70" align="center"></td>
                <td width="470"></td>
            </tr>
        <{/if}>
    </tbody>
</table>
