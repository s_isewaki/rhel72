
<table border="1" cellpadding="2" cellspacing="0">
    <thead>
        <tr style="background-color:#d3d3d3;color:#000000;">
            <th width="70" align="center">年月</th>
            <th width="50" align="center">種別</th>
            <th width="250" align="center">研究発表会(学会)名</th>
            <th width="170" align="center">主催</th>
        </tr>
    </thead>
    <tbody>
        <{if $list}>
            <{foreach item=row from=$list}>
                <tr>
                    <td width="70" align="center"><{$row.period|escape}></td>
                    <td width="50" align="center"><{$types[$row.type]|escape}></td>
                    <td width="250"><{$row.title|escape}></td>
                    <td width="170"><{$row.organizer|escape}></td>
                </tr>
            <{/foreach}>
        <{else}>
            <tr>
                <td width="70"></td>
                <td width="50"></td>
                <td width="250"></td>
                <td width="170"></td>
            </tr>
        <{/if}>
    </tbody>
</table>
