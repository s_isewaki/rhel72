<style type="text/css">
    .label {
        width:100;
        text-align: left;
    }
    .data {
        width:440;
    }
    .level {
        font-family: "Times New Roman", serif;
    }
    .evaluation {
        width:540;
    }
</style>
<table border="1" cellpadding="5" cellspacing="0">
    <tbody>
        <tr>
            <td width="70"  style="text-align: center;">申請レベル</td>
            <td width="50"  style="text-align: center; font-family: kozminproregular;"><{$apply->level_text()|escape}></td>
            <td width="50"  style="text-align: center;">評価者</td>
            <td width="100"><{foreach from=$form1.appr_list item=row}><br><{$row.emp_name|escape}><{/foreach}></td>
            <td width="50"  style="text-align: center;">評価日</td>
            <td width="120" style="text-align: center;"><{$form8.appraise_date|date_format_jp:'%Y年 %-m月 %-d日 (%a)'}></td>
        </tr>
    </tbody>
</table>
<br/>
<br/>
&nbsp;&nbsp;<{if $ldr.config.rc === '1'}>臨床看護実践<{else}>赤十字<{/if}><br/>
<table  border="1" cellpadding="5">
    <thead>
        <tr><th style="text-align:center">評価内容</th></tr>
    </thead>
    <tbody>
        <tr><td><{$form8.clinical|escape|nl2br}></td></tr>
    </tbody>
</table>
<br/>
<br/>
&nbsp;&nbsp;<{if $ldr.config.rc === '1'}>マネジメント<{else}>管理過程<{/if}><br/>
<table  border="1" cellpadding="5">
    <thead>
        <tr><th style="text-align:center">評価内容</th></tr>
    </thead>
    <tbody>
        <tr><td><{$form8.management|escape|nl2br}></td></tr>
    </tbody>
</table>
<br/>
<br/>
&nbsp;&nbsp;<{if $ldr.config.rc === '1'}>教育・研究<{else}>意思決定<{/if}><br/>
<table  border="1" cellpadding="5">
    <thead>
        <tr><th style="text-align:center">評価内容</th></tr>
    </thead>
    <tbody>
        <tr><td><{$form8.education|escape|nl2br}></td></tr>
    </tbody>
</table>
<br/>
<br/>
&nbsp;&nbsp;<{if $ldr.config.rc === '1'}>赤十字活動<{else}>質保証<{/if}><br/>
<table  border="1" cellpadding="5">
    <thead>
        <tr><th style="text-align:center">評価内容</th></tr>
    </thead>
    <tbody>
        <tr><td><{$form8.redcross|escape|nl2br}></td></tr>
    </tbody>
</table>
<br/>
<br/>
<{if $ldr.config.rc === '2'}>
    &nbsp;&nbsp;人材育成<br/>
    <table  border="1" cellpadding="5">
        <thead>
            <tr><th style="text-align:center">評価内容</th></tr>
        </thead>
        <tbody>
            <tr><td><{$form8.comment5|escape|nl2br}></td></tr>
        </tbody>
    </table>
    <br/>
    <br/>
    &nbsp;&nbsp;対人関係<br/>
    <table  border="1" cellpadding="5">
        <thead>
            <tr><th style="text-align:center">評価内容</th></tr>
        </thead>
        <tbody>
            <tr><td><{$form8.comment6|escape|nl2br}></td></tr>
        </tbody>
    </table>
    <br/>
    <br/>
    &nbsp;&nbsp;セルフマネジメント<br/>
    <table  border="1" cellpadding="5">
        <thead>
            <tr><th style="text-align:center">評価内容</th></tr>
        </thead>
        <tbody>
            <tr><td><{$form8.comment7|escape|nl2br}></td></tr>
        </tbody>
    </table>
    <br/>
    <br/>
<{/if}>
&nbsp;&nbsp;総合レベル<br/>
<table  border="1" cellpadding="5">
    <tbody>
        <tr>
            <{if $form8.level === "1"}>
                <td>あなたの看護実践に対し申請レベルを認めます。</td>
            <{elseif $form8.level === "2"}>
                <td>今回の申請レベルは保留にします。再チャレンジしてください。</td>
            <{/if}>
        </tr>
    </tbody>
</table>
<br/>
<br/>
&nbsp;&nbsp;キャリアを高める目標<br/>
<table  border="1" cellpadding="5" >
    <tbody>
        <tr><td><{$form8.objective|escape|nl2br}></td></tr>
    </tbody>
</table>