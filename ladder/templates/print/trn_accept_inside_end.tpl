<style type="text/css">
    table.head td {
        font-size:8pt;
    }
    table.list th,
    table.list td {
        font-size:6pt;
    }
</style>

<table border="0" cellpadding="2" class="head">
    <tbody>
        <tr>
            <td width="80">ǯ�� :</td>
            <td width="460"><{$trn.year|escape}>ǯ��</td>
        </tr>
        <tr>
            <td width="80">����̾ :</td>
            <td width="460"><{if $trn.no}>(<{$trn.no|escape}>) <{/if}> <{$trn.title|escape}></td>
        </tr>
        <tr>
            <td>������ :</td>
            <td>��<{$trn.date.number|escape}>�� / ��<{$trn.number_of|escape}>��</td>
        </tr>
        <{if $trn.date.type === '1'}>
            <tr>
                <td>���ž�� :</td>
                <td><{$trn.date.place|escape}></td>
            </tr>
            <tr>
                <td>���Ŵ��� :</td>
                <td><{$trn.date.training_date|date_format_jp:"%Yǯ%-m��%-d��(%a)"}> <{$trn.date.training_start_time|date_format:"%R"|escape}> �� <{$trn.date.training_last_time|date_format:"%R"|escape}></td>
            </tr>
        <{/if}>
    </tbody>
</table>
<br/>
<br/>
<table border="1" cellpadding="2" cellspacing="0" class="list">
    <thead>
        <tr style="background-color:#d3d3d3;color:#000000;">
            <th width="30" align="center">No</th>
            <th width="125" align="center">����</th>
            <th width="50" align="center">����ID</th>
            <th width="60" align="center">���ʼ�</th>
            <td width="10"></td>
            <th width="30" align="center">No</th>
            <th width="125" align="center">����</th>
            <th width="50" align="center">����ID</th>
            <th width="60" align="center">���ʼ�</th>
        </tr>
    </thead>
    <tbody>
        <{if $list}>

            <{foreach item=row from=$list name=loop}>
                <{if $smarty.foreach.loop.index % 2 === 0}>
                    <tr>
                    <{else}>
                        <td width="10"></td>
                    <{/if}>

                    <td width="30" align="center"><{$smarty.foreach.loop.iteration}></td>
                    <td width="125"><{$row.class_full_name|escape}></td>
                    <td width="50" align="center"><{$row.emp_personal_id}></td>
                    <td width="60" align="center"><{$row.name|escape}></td>

                    <{if $smarty.foreach.loop.index % 2 === 1 || $smarty.foreach.loop.last}>
                    </tr>
                <{/if}>
            <{/foreach}>
        <{else}>
            <tr>
                <td width="30"></td>
                <td width="125"></td>
                <td width="50"></td>
                <td width="60"></td>
                <td width="10"></td>
                <td width="30"></td>
                <td width="125"></td>
                <td width="50"></td>
                <td width="60"></td>
            </tr>
        <{/if}>
    </tbody>
</table>
<br/>
<br/>
