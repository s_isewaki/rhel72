<style type="text/css">
    table.head td {
        font-size:9pt;
    }
    table.list th,
    table.list td {
        font-size:8pt;
    }
</style>

<table border="0" cellpadding="2" class="head">
    <tbody>
        <tr>
            <td width="80">����̾:</td>
            <td width="460"><{if $trn.no}>(<{$trn.no|escape}>) <{/if}> <{$trn.title|escape}></td>
        </tr>
        <tr>
            <td>������:</td>
            <td>��<{$trn.date.number|escape}>�� / ��<{$trn.number_of|escape}>��</td>
        </tr>
        <tr>
            <td>���ž��:</td>
            <td><{$trn.date.place|escape}></td>
        </tr>
        <tr>
            <td>���Ŵ���:</td>
            <td><{$trn.date.training_date|date_format_jp:"%Yǯ%-m��%-d��(%a)"}> <{$trn.date.training_start_time|date_format:"%R"|escape}> �� <{$trn.date.training_last_time|date_format:"%R"|escape}></td>
        </tr>
    </tbody>
</table>
<br/>
<br/>
<table border="1" cellpadding="2" cellspacing="0" class="list">
    <thead>
        <tr style="background-color:#d3d3d3;color:#000000;">
            <th width="50" align="center">No</th>
            <th width="170" align="center">����</th>
            <th width="70" align="center">����ID</th>
            <th width="100" align="center">������</th>
            <th width="80" align="center">���ơ�����</th>
            <th width="70" align="center">����</th>
        </tr>
    </thead>
    <tbody>
        <{if $list}>

            <{foreach item=row from=$list name=loop}>
                <tr>
                    <td width="50" align="center"><{$smarty.foreach.loop.iteration}></td>
                    <td width="170"><{$row.class_full_name|escape}></td>
                    <td width="70" align="center"><{$row.emp_personal_id}></td>
                    <td width="100" align="center"><{$row.name|escape}></td>
                    <td width="80" align="center"><{$row.status_text}></td>
                    <td width="70" align="center"></td>
                </tr>
            <{/foreach}>
        <{else}>
            <tr>
                <td width="50"></td>
                <td width="170"></td>
                <td width="70"></td>
                <td width="100"></td>
                <td width="80"></td>
                <td width="70"></td>
            </tr>
        <{/if}>
    </tbody>
</table>
<br/>
<br/>
