<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | <{$ldr.config.form_7.title|escape}></title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title=$ldr.config.form_7.title hd_sub_title=$ldr.config.form_7.detailL}>

            <div class="container-fluid">
                <div class="row-fluid">
                    <{*印刷ボタン*}>
                    <form id="form_print" method="post" class="pull-right">
                        <input type="hidden" name="mode" value="" />
                        <input type="hidden" name="menu" value="" />
                        <input type="hidden" name="id" value="" />
                        <input type="hidden" name="target_emp_id" value=""/>
                        <input type="hidden" name="owner_url" value="" />
                        &nbsp;<button type="button" data-id="<{$smarty.post.id}>" data-emp="<{$smarty.post.target_emp_id}>" data-php="form_7edit_appraiser.php" data-mode="edit" class="js-form-button btn btn-danger btn-small"><i class="icon-edit"></i> 編集する</button>
                        &nbsp;<button type="button" data-id="<{$smarty.post.id}>" data-emp="<{$smarty.post.target_emp_id}>" data-php="form_7view.php" data-menu="accept" data-target="form7" class="js-form-print btn btn-primary btn-small"><i class="icon-print"></i> 印刷する</button>
                    </form>

                    <{*様式共通ボタン*}>
                    <form id="form_edit" method="post">
                        <{include file='__form_view.tpl' op_srh='false' tmp_id='form_7view'}>

                        <input type="hidden" name="active" id="active" value="" />

                        <div class="well well-small">

                            <div class="control-group">
                                <div class="input-prepend" style="margin-bottom:0;">
                                    <span class="add-on">申請レベル</span>
                                    <span class="ldr-uneditable input-mini ldr-level" style="margin-bottom:0;"><{$apply->level_text()|escape}></span>
                                </div>
                                <div class="input-prepend" style="margin-bottom:0;">
                                    <span class="add-on">申請者</span>
                                    <span class="ldr-uneditable" style="margin-bottom:0;"><{$target_emp->emp_name()|escape}> (<{$target_emp->class_full_name()|escape}>)</span>
                                </div>
                            </div>
                            <hr/>

                            <ul id="tabs" class="nav nav-tabs">
                                <{foreach item=row from=$group1_list}>
                                    <li class="<{if $active === $row.guideline_group1}>active<{/if}>">
                                        <a href="#g1-<{$row.guideline_group1|escape}>" data-toggle="tab"><{$row.name|escape}></a>
                                    </li>
                                <{/foreach}>
                                <li>
                                    <a href="#comment" data-toggle="tab"><i class="icon-comment"></i> コメント</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <{* 7-2 *}>
                                <{foreach item=g1 from=$group1_list}>
                                    <div class="tab-pane <{if $active === $g1.guideline_group1}>active<{else}>fade<{/if}>" id="g1-<{$g1.guideline_group1|escape}>" style="overflow:hidden;">

                                        <table class="table table-striped table-hover table-bordered table-condensed ldr-table">
                                            <thead>
                                                <tr>
                                                    <th>行動指標</th>
                                                    <th>評価者</th>
                                                    <th width="40px">評価</th>
                                                    <{if $ldr.config.reason === '1'}><th>根拠</th><{/if}>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <{foreach item=asm from=$assessment_list[$g1.guideline_group1]}>
                                                    <{if $ldr.config.app_view === '1'}>
                                                        <tr <{if $asm.apply_assessment>='1'}>class="success"<{elseif $asm.apply_assessment=='0'}>class="error"<{else}>class="info"<{/if}>>
                                                            <td class="ldr-td-text-left" rowspan="2" style='background-color:#fff'><{$asm.guideline|escape}><{if $asm.criterion}><br/><{$asm.criterion|escape}><{/if}></td>
                                                            <td class="ldr-td-text-left"><{$target_emp->emp_name()}></td>
                                                            <td class="ldr-td-text-center">

                                                                <{if is_null($asm.apply_assessment)}>
                                                                    ?
                                                                <{else}>
                                                                    <{foreach item=sel from=$ldr.config.select}>
                                                                        <{if $asm.apply_assessment|escape == $sel.value}>
                                                                            <{$sel.button}>
                                                                        <{/if}>
                                                                    <{/foreach}>
                                                                <{/if}>
                                                            </td>
                                                            <{if $ldr.config.reason === '1'}><td class="ldr-td-text-left"><{$asm.apply_reason|escape|nl2br}></td><{/if}>
                                                        </tr>
                                                    <{/if}>
                                                    <tr <{if $asm.accept_assessment>='1'}>class="success"<{elseif $asm.accept_assessment=='0'}>class="error"<{else}>class="info"<{/if}>>
                                                        <{if $ldr.config.app_view !== '1'}>
                                                            <td class="ldr-td-text-left" style='background-color:#fff'><{$asm.guideline|escape}><{if $asm.criterion}><br/><{$asm.criterion|escape}><{/if}></td>
                                                            <{/if}>
                                                        <td class="ldr-td-text-left"><{$emp->emp_name()}></td>
                                                        <td class="ldr-td-text-center">
                                                            <{if is_null($asm.accept_assessment)}>
                                                                ?
                                                            <{else}>
                                                                <{foreach item=sel from=$ldr.config.select}>

                                                                    <{if $asm.accept_assessment|escape == $sel.value}>
                                                                        <{$sel.button}>
                                                                    <{/if}>
                                                                <{/foreach}>
                                                            <{/if}>
                                                        </td>
                                                        <{if $ldr.config.reason === '1'}><td class="ldr-td-text-left"><{$asm.accept_reason|escape|nl2br}></td><{/if}>
                                                    </tr>
                                                <{/foreach}>
                                            </tbody>
                                        </table>
                                    </div>
                                <{/foreach}>

                                <div class="tab-pane" id="comment">
                                    <{if $ldr.config.app_view === '1'}>
                                        <div class="control-group">
                                            <label class="control-label">
                                                <strong><{$ldr.config.asm_comment|default:'コメント'|escape}></strong>
                                            </label>
                                            <div class="controls">
                                                <span class="input-maxlarge ldr-uneditable"><{$apply_form7.knowledge|escape|nl2br}></span>
                                            </div>
                                        </div>
                                    <{/if}>

                                    <div class="control-group">
                                        <label class="control-label">
                                            <strong>評価者コメント</strong>
                                        </label>
                                        <div class="controls">
                                            <span class="input-maxlarge ldr-uneditable"><{$form7.advice|escape|nl2br}></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {
                /**
                 * タブアクティブ
                 */
                if ($('ul.nav-tabs').find('li.active').length === 0) {
                    $('ul.nav-tabs').find('a:first').tab('show');
                }
            });
        </script>
    </body>
</html>