<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | <{$titles.form_4.title|escape}></title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>
        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title=$titles.form_4.title hd_sub_title=$titles.form_4.detailL}>

            <div class="container-fluid">
                <div class="row-fluid">
                    <form id="form_print"  method="post" class="pull-right">
                        <button type="button" data-id="" data-emp="<{$emp->emp_id()|escape}>" data-php="<{if $hospital_type === '1'}>form_4view.php<{else}>records_view.php<{/if}>" data-target="form4" class="pull-right js-form-print btn btn-primary btn-small"><i class="icon-print"></i> 印刷する</button>
                    </form>

                    <form name="form_search" id="form_search" class="form" method="POST">

                        <{include file="__form_search.tpl"}>

                        <div class="well well-small">
                            <{if $list}>
                                <{include file="__pager.tpl"}>
                                <table class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th><i class="icon-pencil"></i></th>
                                            <th>年月日</th>
                                            <th>記録</th>
                                            <th><i class="icon-remove"></i></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <{foreach item=row from=$list}>
                                            <tr>
                                                <td><a href='javascript:void(0);' id='<{$row.inner_id}>' class='i_mod'><i class="icon-pencil"></i></a></td>
                                                <td class="ldr-td-text-left"><{$row.period|date_format_jp:"%Y年%-m月%-d日(%a)"}></td>
                                                <td class="ldr-td-text-left"><{$row.record|escape|nl2br}></td>
                                                <td><a href='javascript:void(0);' id='<{$row.inner_id}>' class='i_remove'><i class='icon-remove'></i></a></td>
                                            </tr>
                                        <{/foreach}>
                                    </tbody>
                                </table>
                            <{else}>
                                <div class="alert alert-error">記録が登録されていません</div>
                            <{/if}>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <{include file="__js.tpl"}>
        <{include form_selector="#form_search" file="__pager_js.tpl"}>
        <{include up_title="インポート" up_mode="import" file_nav="CSVファイル" up_php="form_4.php" file="__fileupload.tpl"}>
        <{javascript src="js/form-list.js"}>
        <script type="text/javascript">
            $(function() {
                // 新規登録
                $('#btn_new').click(function() {
                    $('#inner_id').val('');
                    $("#form_search").attr('action', 'form_4edit.php').submit();
                });
                // 更新
                $('.i_mod').click(function() {
                    $('#inner_id').val($(this).attr('id'));
                    $("#form_search").attr('action', 'form_4edit.php').submit();
                });
                //CSVエクスポート
                $('#export_csv').click(function() {
                    location.href = 'form_4.php?mode=export_csv';
                });

            });

        </script>
    </body>
</html>