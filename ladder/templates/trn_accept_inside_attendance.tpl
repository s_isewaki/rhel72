<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 院内研修出欠</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='院内研修出欠' hd_sub_title=''}>

            <div class="container-fluid">
                <form method="post" id="inside_form">
                    <input type="hidden" id="id" name="id" value="" />
                    <input type="hidden" id="title" name="title" value="" />
                    <input type="hidden" id="training_date" name="training_date" value="" />
                    <input type="hidden" id="type" name="type" value="" />
                    <div class="well well-small">
                        <fieldset>
                            <legend>
                                <a
                                    href="trn_inside_contents_modal.php?id=<{$trn.inside_id|escape}>"
                                    data-toggle="modal"
                                    data-backdrop="true"
                                    data-target="#inside_contents">
                                    <{if $trn.no}>(<{$trn.no|escape}>) <{/if}><{$trn.title|escape}>
                                </a>
                                <a
                                    href="trn_inside_number_modal.php?id=<{$trn.inside_id|escape}>&number=<{$trn.date.number|escape}>&date_id=<{$date_id|escape}>"
                                    data-toggle="modal"
                                    data-backdrop="true"
                                    data-target="#inside_number">
                                    第<{$trn.date.number|escape}>回 / 全<{$trn.number_of|escape}>回
                                </a>
                            </legend>
                            <div class="form-inline">
                                <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                                <select name="srh_class1" class="input-large" id="class1">
                                    <option value="">すべて</option>
                                    <{foreach item=row from=$class12_list}>
                                        <option value="<{$row.class_id|escape}>_<{$row.atrb_id|escape}>"><{$row.class_nm|escape}> &gt; <{$row.atrb_nm|escape}></option>
                                    <{/foreach}>
                                </select>
                                <select name="srh_class3" class="input-large" id="class3">
                                    <option value="">すべて</option>
                                </select>
                                <select id="class3_clone" style="display:none;">
                                    <{foreach item=row from=$class_list}>
                                        <option class="js-class-select select<{$row.class_id|escape}>_<{$row.atrb_id|escape}>" value="<{$row.dept_id|escape}><{if $row.room_id}>_<{$row.room_id|escape}><{/if}>">
                                            <{$row.dept_nm|escape}><{if $row.room_id}> &gt; <{$row.room_nm|escape}><{/if}>
                                        </option>
                                    <{/foreach}>
                                </select>
                                <select name="srh_attendance" class="input-medium">
                                    <option value="">すべて</option>
                                    <option value="1">出席</option>
                                    <option value="2">欠席</option>
                                    <option value="0">未設定</option>
                                </select>
                                <button type="submit" id="btn_search" name="btn_search" value="search" class="btn btn-small btn-primary"><i class="icon-search"></i> 検索する</button>
                                <button type="button" class="js-btn-emplist btn btn-small btn-success"><i class="icon-book"></i> 参加職員の追加</button>
                            </div>
                            <hr/>
                            <{if ! $list}>
                                <div id="alert_emp_list" class="alert alert-error">申請者がいません</div>
                            <{/if}>
                            <table id="table_emp_list" class="table table-striped table-hover <{if ! $list}>hide<{/if}>">
                                <thead>
                                    <tr>
                                        <th>部署</th>
                                        <th>職員ID</th>
                                        <th>名前</th>
                                        <th>申請日</th>
                                        <th>設定
                                            <button type="button" id="btn_on" class="btn btn-mini">すべて出席</button>
                                            <button type="button" id="btn_off" class="btn btn-mini">すべて欠席</button>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody id="emp_list">
                                    <{foreach item=row from=$list}>
                                        <tr id="e<{$row.emp_id|escape}>">
                                            <td class="ldr-td-text-left">
                                                <{$row.class_full_name|escape}>
                                            </td>
                                            <td class="ldr-td-text"><{$row.emp_personal_id|escape}></td>
                                            <td class="ldr-td-text-left"><{$row.name|escape}></td>
                                            <td class="ldr-td-text-left"><{$row.created_on|date_format_jp:"%Y年%-m月%-d日(%a) %R"}></td>
                                            <td class="ldr-td-text-left">
                                                <input type="hidden" name="emp_id[<{$row.training_id|escape}>]" value="<{$row.emp_id|escape}>" />
                                                <{if $row.status==='3' or $row.status==='0'}>
                                                    <span class="form-inline">
                                                        <label class="radio ldr-td-text">
                                                            <input type="radio" class="r1" name="training_id[<{$row.training_id|escape}>]" value="1" <{if $row.attendance==='1'}>checked<{/if}> /> 出席
                                                        </label>
                                                        <label class="radio ldr-td-text">
                                                            <input type="radio" class="r2" name="training_id[<{$row.training_id|escape}>]" value="2" <{if $row.attendance==='2'}>checked<{/if}> /> 欠席
                                                        </label>
                                                        <label class="radio ldr-td-text">
                                                            <input type="radio" class="r0" name="training_id[<{$row.training_id|escape}>]" value="0" <{if $row.attendance==='0' or $row.attendance==''}>checked<{/if}> /> 未設定
                                                        </label>
                                                    </span>
                                                <{elseif $row.status < 0}>
                                                    <span class="label label-important"><{$row.status_text|escape}></span>
                                                <{else}>
                                                    <span class="label label-info"><{$row.status_text|escape}></span>
                                                <{/if}>
                                            </td>
                                            <td></td>
                                        </tr>
                                    <{/foreach}>
                                </tbody>
                            </table>

                        </fieldset>
                        <div class="btn-toolbar">
                            <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                            <button type="submit" name="btn_save" class="btn btn-small btn-primary js-submit-attendance" value="save"><i class="icon-edit"></i> 設定する</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="hide">
            <{foreach item=row from=$contents_list}>
                <span id="ee<{$row.emp_id|escape}>" data-tid="<{$row.training_id|escape}>" data-dt="<{$row.training_date|date_format_jp:"%-m月%-d日(%a)"}> <{$row.training_start_time|date_format:"%R"}>"></span>
            <{/foreach}>
        </div>
        <{include file="__modal_view.tpl" modal_id="inside_contents" modal_title="研修内容"}>
        <{include file="__modal_view.tpl" modal_id="inside_number" modal_title="研修内容"}>
        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {
                $('#btn_on').click(function() {
                    $('input.r1').prop("checked", true);
                });
                $('#btn_off').click(function() {
                    $('input.r2').prop("checked", true);
                });

                // 部署切り替え
                $('#class1').change(function() {
                    $('#class3').empty().append('<option value="">すべて</option>');
                    $('#class3_clone').find('option.select' + $(this).val()).each(function() {
                        $('#class3').append($(this).clone());
                    });
                    $('#class3').find('option[value=<{$srh_class3|escape}>]').attr('selected', 'selected');
                }).trigger('change');

                // 職員名簿
                $('.js-btn-emplist').click(function() {
                    open_emplist('inside_attendance');
                });

                // 職員の削除
                $('body').on('click', 'a.js-row-remove', function() {
                    $(this).parent().parent().remove();
                    if ($('#emp_list').children().length <= 0) {
                        $('#alert_emp_list').show();
                        $('#table_emp_list').hide();
                    }
                });
            });

            function closeEmployeeList() {
                if (childwin != null && !childwin.closed) {
                    childwin.close();
                }
                childwin = null;
            }

            // 職員の追加
            function add_target_list(item_id, emp_id, emp_name) {
                var emp = emp_id.split(", ");
                var empname = emp_name.split(", ");

                // 一括登録は無視
                if (emp.length !== 1) {
                    return;
                }

                // 登録済みは無視
                if ($('#e' + emp_id).size() > 0) {
                    return;
                }

                // 別日程に登録されている場合
                var moved = 0;
                if ($('#ee' + emp_id).size() > 0) {
                    if (confirm(empname + ' は[' + $('#ee' + emp_id).data('dt') + '〜]の同じ研修に申し込んでいますが、こちらに登録してよろしいですか？')) {
                        moved = $('#ee' + emp_id).data('tid');
                    }
                    else {
                        return;
                    }
                }

                // 一覧tableが表示されていない場合は表示する
                if ($('#alert_emp_list').size() > 0) {
                    $('#alert_emp_list').hide();
                    $('#table_emp_list').show();
                }

                $('#emp_list').append('<tr id="e' + emp_id + '"><td class="ldr-td-text-left">(保存してください)</td><td class="ldr-td-text">(保存してください)</td><td class="ldr-td-text-left"><b>' + emp_name + '</b></td><td class="ldr-td-text-left">(保存してください)</td><td class="ldr-td-text-left"><input type="hidden" name="new_emp_id[]" value="' + emp_id + '" /><input type="hidden" name="moved[' + emp_id + ']" value="' + moved + '" />(未設定)</td><td><a class="pull-right js-row-remove" href="javascript:void(0);"><i class="icon-remove"></i></a></td></tr>');

            }
        </script>
    </body>
</html>
