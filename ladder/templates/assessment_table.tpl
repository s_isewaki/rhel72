<div class="tab-content">
    <table class="table table-condensed table-bordered ldr-table">
        <thead>
            <tr>
                <th></th>
                <th></th>

                <{foreach item=lv from=$ldr.config.level}>
                    <th colspan="2">
                        <{$roman_num[$lv]|default:$ldr.config.level0|escape}>
                    </th>
                <{/foreach}>
            </tr>
        </thead>
        <tbody id="appr_list">
            <{assign var=back_g1 value=0}>
            <{foreach item=row from=$label}>
                <tr>
                    <{if $back_g1 != $row.guideline_group1}>
                        <td rowspan="<{$row.rowspan|escape}>">
                            <{$row.g1_name}>
                            <{assign var=back_g1 value=$row.guideline_group1}>
                        </td>
                    <{/if}>
                    <td>
                        <{$row.g2_name}>
                    </td>

                    <{foreach item=lv from=$ldr.config.level}>
                        <{assign var=lvn value="lv`$lv`"}>
                        <{if $row.$lvn > 0}>
                            <td class="ldr-td-number">
                                <{$list[$row.guideline_group2][$lv].assessment|default:0|escape}>
                            </td>
                            <td class="ldr-td-number">
                                <{$list[$row.guideline_group2][$lv].percent|default:'0.0'|escape}>%
                            </td>
                        <{else}>
                            <td colspan="2"></td>
                        <{/if}>
                    <{/foreach}>
                </tr>
            <{/foreach}>
        </tbody>
    </table>
</div>


