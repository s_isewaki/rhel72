<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | <{$titles.form_2.title}></title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title=$titles.form_2.title hd_sub_title=$titles.form_2.detailL}>

            <div class="container-fluid">
                <{*印刷ボタン*}>
                <form id="form_print" method="post" class="pull-right">
                    <button type="button" data-id="<{$smarty.post.id|escape}>" data-emp="<{$smarty.post.target_emp_id|escape}>" data-php="form_2view.php" data-target="form2" class="pull-right js-form-print btn btn-primary btn-small"><i class="icon-print"></i> 印刷する</button>
                </form>

                <{*様式共通ボタン*}>
                <form id="form_edit" method="post">
                    <{include file='__form_view.tpl' op_srh='true' tmp_id='form_2view'}>
                    <input type="hidden" id="fm2_active" name="fm2_active" value="">
                </form>

                <div class="well well-small">

                    <div class="control-group">
                        <div class="input-prepend" style="margin-bottom:0;">
                            <span class="add-on">申請レベル</span>
                            <span class="ldr-uneditable input-mini ldr-level" style="margin-bottom:0;"><{$apply->level_text()|escape}></span>
                        </div>
                        <div class="input-prepend" style="margin-bottom:0;">
                            <span class="add-on">申請者</span>
                            <span class="ldr-uneditable input-large" style="margin-bottom:0;"><{$target_emp->emp_name()|escape}> (<{$target_emp->class_full_name()|escape}>)</span>
                        </div>
                    </div>
                    <hr/>

                    <ul class="nav nav-tabs">
                        <{if $function.form_21}>
                            <li class="<{if $active === 'form_21'}>active<{/if}>">
                                <a href="#form_21" id="a_21" data-toggle="tab"><span class="ldr-roman">I.</span><{$titles.form_21.detail|escape}></a>
                            </li>
                        <{/if}>
                        <{if $function.form_22}>
                            <li class="<{if $active === 'form_22_1'}>active<{/if}>">
                                <a href="#form_22_1" id="a_22_1" data-toggle="tab"><span class="ldr-roman">II.</span>院内研修受講一覧</a>
                            </li>
                            <li class="<{if $active === 'form_22_2'}>active<{/if}>">
                                <a href="#form_22_2" id="a_22_2" data-toggle="tab"><span class="ldr-roman">II.</span>院外研修受講一覧</a>
                            </li>
                            <li class="<{if $active === 'form_22_3'}>active<{/if}>">
                                <a href="#form_22_3" id="a_22_3" data-toggle="tab"><span class="ldr-roman">II.</span>バリテス受講一覧</a>
                            </li>
                        <{/if}>
                        <{if $function.form_23}>
                            <li class="<{if $active === 'form_23'}>active<{/if}>">
                                <a href="#form_23" id="a_23" data-toggle="tab"><span class="ldr-roman">III.</span><{$titles.form_23.detail|escape}></a>
                            </li>
                        <{/if}>
                    </ul>

                    <div class="tab-content">
                        <{if $function.form_21}>
                            <div class="tab-pane <{if $active === 'form_21'}>active<{else}>fade<{/if}>" id="form_21">
                                <div class="control-group">
                                    <h5><span class="ldr-roman">I.</span> <{$titles.form_21.detail|escape}></h5>
                                </div>
                                <table class="table table-striped table-hover table-bordered table-condensed ldr-table">
                                    <thead>
                                        <tr>
                                            <th>期間</th>
                                            <th>学歴・研修名等</th>
                                            <th>主催</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <{foreach item=row from=$list21}>
                                            <tr>
                                                <td class="ldr-td-view-from-to"><{$row.start_date|date_format_jp:"%Y年%-m月%-d日(%a)"}> 〜 <{$row.last_date|date_format_jp:"%Y年%-m月%-d日(%a)"}></td>
                                                <td class="ldr-td-text-left"><{$row.title|escape}></td>
                                                <td class="ldr-td-text-left"><{$row.organizer|escape}></td>
                                            </tr>
                                        <{/foreach}>
                                    </tbody>
                                </table>
                            </div>
                        <{/if}>

                        <{if $function.form_22}>
                            <div class="tab-pane <{if $active === 'form_22_1'}>active<{/if}>" id="form_22_1">
                                <div class="control-group">
                                    <h5><span class="ldr-roman">II.</span> 院内研修受講一覧</h5>
                                </div>
                                <table class="table table-striped table-hover table-bordered table-condensed ldr-table">
                                    <thead>
                                        <tr>
                                            <th>年月日</th>
                                            <th>研修名</th>
                                            <th>主催</th>
                                            <th>カテゴリ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <{foreach item=row from=$list22_1}>
                                            <tr>
                                                <td class="ldr-td-view-period"><{$row.period|date_format_jp:"%Y年%-m月%-d日(%a)"}></td>
                                                <td class="ldr-td-text-left"><{$row.title|escape}><{if $row.training_id<>0}>*<{/if}></td>
                                                <td class="ldr-td-text-left"><{$row.organizer|escape}></td>
                                                <td class="ldr-td-text-left"><{$row.category|escape}></td>
                                            </tr>
                                        <{/foreach}>
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane <{if $active === 'form_22_2'}>active<{/if}>" id="form_22_2">
                                <div class="control-group">
                                    <h5><span class="ldr-roman">II.</span> 院外研修受講一覧</h5>
                                </div>
                                <table class="table table-striped table-hover table-bordered table-condensed ldr-table">
                                    <thead>
                                        <tr>
                                            <th>年月日</th>
                                            <th>研修名</th>
                                            <th>主催</th>
                                            <th>カテゴリ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <{foreach item=row from=$list22_2}>
                                            <tr>
                                                <td class="ldr-td-view-period"><{$row.period|date_format_jp:"%Y年%-m月%-d日(%a)"}></td>
                                                <td class="ldr-td-text-left"><{$row.title|escape}><{if $row.training_id<>0}>*<{/if}></td>
                                                <td class="ldr-td-text-left"><{$row.organizer|escape}></td>
                                                <td class="ldr-td-text-left"><{$row.category|escape}></td>
                                            </tr>
                                        <{/foreach}>
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane <{if $active === 'form_22_3'}>active<{/if}>" id="form_22_3">
                                <div class="control-group">
                                    <h5><span class="ldr-roman">II.</span> バリテス受講一覧</h5>
                                </div>
                                <table class="table table-striped table-hover table-bordered table-condensed ldr-table">
                                    <thead>
                                        <tr>
                                            <th>年月日</th>
                                            <th>研修名</th>
                                            <th>主催</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <{foreach item=row from=$list22_3}>
                                            <tr>
                                                <td class="ldr-td-view-period"><{$row.period|date_format_jp:"%Y年%-m月%-d日(%a)"}></td>
                                                <td class="ldr-td-text-left"><{$row.title|escape}></td>
                                                <td class="ldr-td-text-left"><{$row.organizer|escape}></td>
                                            </tr>
                                        <{/foreach}>
                                    </tbody>
                                </table>
                            </div>
                        <{/if}>

                        <{if $function.form_23}>
                            <div class="tab-pane <{if $active === 'form_23'}>active<{/if}>" id="form_23">
                                <div class="control-group">
                                    <h5><span class="ldr-roman">III.</span> <{$titles.form_23.detail|escape}></h5>
                                </div>
                                <table class="table table-striped table-hover table-bordered table-condensed ldr-table">
                                    <thead>
                                        <tr>
                                            <th>年月日</th>
                                            <th>科目</th>
                                            <th>単元</th>
                                            <th>教科内容</th>
                                            <th>主催</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <{foreach item=row from=$list23}>
                                            <tr>
                                                <td class="ldr-td-view-period"><{$row.period|date_format_jp:"%Y年%-m月%-d日(%a)"}></td>
                                                <td class="ldr-td-text-left"><{$row.subject|escape}></td>
                                                <td class="ldr-td-text-center ldr-level"><{$row.unit_roman|escape}></td>
                                                <td class="ldr-td-text-left"><{$row.contents|escape}></td>
                                                <td class="ldr-td-text-left"><{$row.organizer|escape}></td>
                                            </tr>
                                        <{/foreach}>
                                    </tbody>
                                </table>
                            </div>
                        <{/if}>
                    </div>
                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {
                $('#a_21').click(function() {
                    $('#fm2_active').val("form_21");
                });
                $('#a_22_1').click(function() {
                    $('#fm2_active').val("form_22_1");
                });
                $('#a_22_2').click(function() {
                    $('#fm2_active').val("form_22_2");
                });
                $('#a_23').click(function() {
                    $('#fm2_active').val("form_23");
                });

                if ($('ul.nav-tabs').find('li.active').length === 0) {
                    $('ul.nav-tabs').find('a:first').tab('show');
                }
            });
        </script>
    </body>
</html>