<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | <{$titles.form_51.title|escape}>入力</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title="`$titles.form_51.title`入力" hd_sub_title=$titles.form_51.detailL}>

            <div class="container-fluid">
                <div class="well well-small">
                    <form id="form_edit" action="form_51edit.php" method="post">
                        <input type="hidden" name="inner_id" value="" />
                        <{if $not_found === true}>
                            <div class="alert alert-error">記録が見つかりませんでした</div>
                            <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                        <{else}>
                            <div class="control-group">
                                <label class="control-label" for="">
                                    <strong>期間</strong>
                                    <span class="badge badge-important required">必須　開始日</span>
                                </label>
                                <{include file="__datepicker.tpl" date_id="start_date" date_name="start_date" date_value='' date_option = 'required'}>
                                〜
                                <{include file="__datepicker.tpl" date_id="last_date" date_name="last_date" date_value=''}>
                                <{foreach from=$error.date item=msg}>
                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                <{/foreach}>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="class">
                                    <strong>所属部署</strong>
                                    <span class="badge badge-important required">必須</span>
                                </label>
                                <div class="controls">
                                    <div class="input-append">
                                        <input type="text" id="class" name="class"
                                               class="input-xlarge <{if $items.51class.use}>js-typeahead-item<{/if}>"
                                               data-item-type="51class"
                                               <{if $items.51class.use && !$items.51class.free}>readonly="readonly"<{/if}>
                                               minlength="2" maxlength="200" value="" required="required"/>
                                        <span class="add-on js-ldr-erase"><a href="javascript:void(0);"><i class="icon-eraser"></i></a></span>
                                    </div>
                                    <{foreach from=$error.class item=msg}>
                                        <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                    <{/foreach}>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="role">
                                    <strong>役割</strong>
                                    <span class="badge badge-important required">必須</span>
                                </label>
                                <div class="controls">
                                    <div class="input-append">
                                        <{if $items.51role.use}>
                                            <input type="text" id="role" name="role" class="input-xxlarge js-typeahead-item"
                                                   data-item-type="51role"
                                                   <{if !$items.51role.free}>readonly="readonly"<{/if}>
                                                   />
                                        <{else}>
                                            <textarea id="role" name="role" rows="6" class="input-xxlarge"
                                                      data-item-type="51role"
                                                      ></textarea>
                                        <{/if}>
                                        <span class="add-on js-ldr-erase"><a href="javascript:void(0);"><i class="icon-eraser"></i></a></span>
                                    </div>
                                    <{foreach from=$error.role item=msg}>
                                        <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                    <{/foreach}>
                                </div>
                            </div>
                            <div class="btn-toolbar">
                                <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                                <button type="submit" name="btn_save" id="btn_save" class="btn btn-small btn-primary" value="btn_save"><i class="icon-save"></i> 保存する</button>
                            </div>
                        <{/if}>
                    </form>
                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>

    </body>
</html>