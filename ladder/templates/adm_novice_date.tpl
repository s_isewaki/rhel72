<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 新人評価日程</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title="新人評価日程"}>

            <div class="container-fluid">
                <div class="row-fluid">
                    <{include file="__save_message.tpl" }>
                    <form class="form" id="form_novice" method="POST">

                        <p>新人評価の評価日程を設定してください。</p>
                        <div id="alert" class="alert alert-danger hide"></div>

                        <div class="form-inline">
                            <select id="pulldown_year" name="year" class="input-medium js-pulldown-novice">
                                <{foreach item=asyear from=$year_list}>
                                    <option value="<{$asyear|escape}>" <{if $year == $asyear}>selected<{/if}>><{$asyear|escape}>年度</option>
                                <{/foreach}>
                            </select>
                        </div>
                        <hr/>

                        <p><b>日程</b></p>
                        <div id="date_list">
                            <{foreach item=row from=$list}>
                                <div class="form-inline" style="margin-bottom:5px;">
                                    <{include file="__datepicker.tpl" date_id="last_date" date_name="last_date[]" date_value=$row.last_date date_remove='off'}>
                                    <button type="button" class="btn btn-default btn-small js-row-add"><i class="icon-plus"></i></button>
                                    <button type="button" class="btn btn-default btn-small js-row-remove"><i class="icon-minus"></i></button>
                                </div>
                            <{/foreach}>
                            <div class="form-inline" style="margin-bottom:5px;">
                                <{include file="__datepicker.tpl" date_id="last_date" date_name="last_date[]" date_remove='off'}>
                                <button type="button" class="btn btn-default btn-small js-row-add"><i class="icon-plus"></i></button>
                                <button type="button" class="btn btn-default btn-small js-row-remove"><i class="icon-minus"></i></button>
                            </div>
                        </div>

                        <{foreach from=$error.error item=msg}>
                            <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                        <{/foreach}>

                        <div class="btn-toolbar">
                            <button type="submit" name="btn_save" id="btn_save" class="btn btn-small btn-primary" value="btn_save"><i class="icon-save"></i> 保存する</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>

    <{capture name="datepicker"}><{include file="__datepicker.tpl" date_id="last_date" date_name="last_date[]" date_remove="off"}><{/capture}>
    <{include file="__js.tpl"}>

    <script type="text/javascript">
        $(function() {
            // 削除
            $('body').on('click', '.js-row-remove', function() {
                if (!confirm('削除してもよろしいですか？')) {
                    return false;
                }
                $(this).parent().remove();
            });

            // 追加
            $('body').on('click', '.js-row-add', function() {
                var html = [
                    '<div class="form-inline" style="margin-bottom:5px;">',
                    '<{$smarty.capture.datepicker|regex_replace:"/[\r\n]/":""}> ',
                    '<button type="button" class="btn btn-default btn-small js-row-add"><i class="icon-plus"></i></button> ',
                    '<button type="button" class="btn btn-default btn-small js-row-remove"><i class="icon-minus"></i></button> ',
                    '</div>'
                ].join("");
                $('#date_list').append(html);
                $('.datepicker').each(datepicker_function);
            });

            // プルダウン選択
            $('.js-pulldown-novice').change(function() {
                $('#form_novice').submit();
            });
        });
    </script>
</body>
</html>
