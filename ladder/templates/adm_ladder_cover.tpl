<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | ラダー設定: 対象</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='ラダー設定: 対象' hd_sub_title=''}>

            <div class="container-fluid">
                <form action="adm_ladder_edit.php" method="post">
                    <input type="hidden" name="ladder_id" value="" />

                    <div class="btn-toolbar">
                        <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                        <button type="submit" name="btn_save" id="btn_save" class="btn btn-small btn-primary" value="cover_save"><i class="icon-save"></i> 保存する</button>
                    </div>

                    <div class="well well-small">
                        <div class="control-group">
                            <label class="control-label"><strong>ラダー名称</strong></label>
                            <div class="controls">
                                <span class="input-xlarge ldr-uneditable"><{$data.title|escape}></span>
                            </div>
                        </div>
                        <hr/>

                        <ul>
                            <li>評価対象範囲を設定してください</li>
                            <li>対象範囲に該当する職員のみ、ラダー評価機能が有効となります</li>
                            <li>対象範囲が複数ある場合は、どれが1つに該当すれば、対象となります</li>
                        </ul>
                        <hr/>

                        <div class="form-group">
                            <div class="btn-toolbar">
                                <button type="button" id="js-select-add" class="btn btn-small btn-primary"><i class="icon-plus"></i> 対象範囲を追加する</button>
                            </div>
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>部署</th>
                                        <th>職種</th>
                                        <th>役職</th>
                                        <th class="pull-right"><i class='icon-remove'></i></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody id="js-select-table">
                                    <{foreach item=row from=$data.cover}>
                                        <tr>
                                            <td>
                                                <select name="class12[]" class="js-select-class12">
                                                    <option value="">すべて</option>
                                                    <{foreach item=r from=$class->class12_lists()}>
                                                        <option value="<{$r.class_id|escape}>_<{$r.atrb_id|escape}>" <{if $r.class_id === $row.class_id && $r.atrb_id === $row.atrb_id}>selected<{/if}>>
                                                            <{$r.class_nm|escape}> &gt; <{$r.atrb_nm|escape}>
                                                        </option>
                                                    <{/foreach}>
                                                </select>
                                                <select name="class34[]" class="js-select-class34">
                                                    <option value="">すべて</option>
                                                    <{foreach item=r from=$class->class34_lists($row.class_id, $row.atrb_id)}>
                                                        <option value="<{$r.dept_id|escape}><{if $r.room_id}>_<{$r.room_id|escape}><{/if}>" <{if $r.dept_id === $row.dept_id && $r.room_id === $row.room_id}>selected<{/if}>>
                                                            <{$r.dept_nm|escape}><{if $r.room_id}> &gt; <{$r.room_nm|escape}><{/if}>
                                                        </option>
                                                    <{/foreach}>

                                                </select>
                                            </td>
                                            <td>
                                                <select name="job[]" class="js-select-job">
                                                    <option value="">すべて</option>
                                                    <{foreach item=r from=$job->lists()}>
                                                        <option value="<{$r.id|escape}>" <{if $r.id === $row.job_id}>selected<{/if}>>
                                                            <{$r.name|escape}>
                                                        </option>
                                                    <{/foreach}>
                                                </select>
                                            </td>
                                            <td>
                                                <select name="status[]" class="js-select-status">
                                                    <option value="">すべて</option>
                                                    <{foreach item=r from=$status->lists()}>
                                                        <option value="<{$r.id|escape}>" <{if $r.id === $row.st_id}>selected<{/if}>>
                                                            <{$r.name|escape}>
                                                        </option>
                                                    <{/foreach}>
                                                </select>
                                            </td>
                                            <td><a class="pull-right js-row-remove" href="javascript:void(0);"><i class="icon-remove"></i></a></td>
                                            <td></td>
                                        </tr>
                                    <{/foreach}>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="btn-toolbar">
                        <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                        <button type="submit" name="btn_save" id="btn_save" class="btn btn-small btn-primary" value="cover_save"><i class="icon-save"></i> 保存する</button>
                    </div>
                </form>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {
                var $class12 = fetch_class12();
                var $job = fetch_job();
                var $status = fetch_status();

                // 選択肢追加
                $('#js-select-add').on('click', function() {
                    var tr = [
                        '<tr>',
                        '<td><select name="class12[]" class="js-select-class12"><option value="">すべて</option>' + $class12 + '</select> ',
                        '<select name="class34[]" class="js-select-class34"></select></td>',
                        '<td><select name="job[]" class="js-select-job"><option value="">すべて</option>' + $job + '</select></td>',
                        '<td><select name="status[]" class="js-select-status"><option value="">すべて</option>' + $status + '</select></td>',
                        '<td><a class="pull-right js-row-remove" href="javascript:void(0);"><i class="icon-remove"></i></a></td>',
                        '<td></td>',
                        '</tr>'
                    ].join('');
                    $('#js-select-table').append(tr);
                });

                // 行の削除
                $('body').on('click', '.js-row-remove', function() {
                    $(this).parent().parent().remove();
                });

                // セレクト連動
                $('body').on('change', 'select.js-select-class12', function() {
                    var target = $(this).next('select');
                    var id = $(this).val();
                    $.ajax({
                        url: 'employee_pulldown.php',
                        dataType: 'json',
                        type: 'post',
                        data: {
                            mode: 'class34',
                            id: id
                        }
                    }).done(function(obj) {
                        target.empty().append('<option value="">すべて</option>');
                        $.each(obj, function(i, row) {
                            if (!row.room_id) {
                                value = row.dept_id;
                                name = row.dept_nm;
                            }
                            else {
                                value = row.dept_id + '_' + row.room_id;
                                name = row.dept_nm + ' &gt; ' + row.room_nm;
                            }
                            target.append('<option value="' + value + '">' + name + '</option>');
                        });
                        target.change();
                    });
                });
            });

            /**
             * class 12
             */
            function fetch_class12() {
                var $options = '';
                $.ajax({
                    url: 'employee_pulldown.php',
                    dataType: 'json',
                    type: 'post',
                    async: false,
                    data: {
                        mode: 'class12'
                    }
                }).done(function(obj) {
                    $.each(obj, function(i, row) {
                        $options += '<option value="' + row.class_id + '_' + row.atrb_id + '">' + row.class_nm + ' &gt; ' + row.atrb_nm + '</option>';
                    });
                });
                return $options;
            }

            /**
             * job
             */
            function fetch_job() {
                var $options = '';
                $.ajax({
                    url: 'employee_pulldown.php',
                    dataType: 'json',
                    type: 'post',
                    async: false,
                    data: {
                        mode: 'job'
                    }
                }).done(function(obj) {
                    $.each(obj, function(i, row) {
                        $options += '<option value="' + row.id + '">' + row.name + '</option>';
                    });
                });
                return $options;
            }

            /**
             * status
             */
            function fetch_status() {
                var $options = '';
                $.ajax({
                    url: 'employee_pulldown.php',
                    dataType: 'json',
                    type: 'post',
                    async: false,
                    data: {
                        mode: 'status'
                    }
                }).done(function(obj) {
                    $.each(obj, function(i, row) {
                        $options += '<option value="' + row.id + '">' + row.name + '</option>';
                    });
                });
                return $options;
            }
        </script>
    </body>
</html>
