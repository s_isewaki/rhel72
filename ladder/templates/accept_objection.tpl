<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 異議申し立て一覧</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title="異議申し立て一覧" hd_sub_title=''}>

            <div class="container-fluid">
                <div class="row-fluid">
                    <form name="accept_form" id="accept_form" class="form" method="POST">
                        <input type="hidden" id="mode" name="mode" value="" />
                        <input type="hidden" id="id" name="id" value="" />
                        <input type="hidden" id="menu" name="menu" value="accept" />
                        <input type="hidden" id="emp_id" name="emp_id" value="" />
                        <input type="hidden" id="target_emp_id" name="target_emp_id" value="" />
                        <input type="hidden" id="owner_url" name="owner_url" value="accept_objection.php" />
                        <input type="hidden" id="unread" name="unread" value="" />

                        <{include file="__pager.tpl"}>
                        <table width="100%" class="table table-bordered table-hover table-condensed ldr-table">
                            <thead>
                                <tr>
                                    <th>更新日時</th>
                                    <th>申請者</th>
                                    <th>申請日</th>
                                    <th>ラダー</th>
                                    <th>Lv</th>
                                    <th>評価者</th>
                                    <th>評価</th>
                                    <th>異議申し立て</th>
                                    <th>申請を見る</th>
                                </tr>
                            </thead>
                            <tbody>
                                <{foreach item=row from=$list}>
                                    <tr>
                                        <td class="ldr-td-date"><span title="<{$row.updated_on|date_format_jp:"%Y年%-m月%-d日(%a)"}>"><{$row.updated_on|date_format_jp:"%-m月%-d日(%a)"}></span><br/><{$row.updated_on|date_format_jp:"%R"}></td>
                                        <td class="ldr-td-status"><{$row.emp_name|escape}></td>
                                        <td class="ldr-td-date"><span class="ldr-td-date-bold" title="<{$row.apply_date|date_format_jp:"%Y年%-m月%-d日(%a)"}>"><{$row.apply_date|date_format_jp:"%-m月%-d日(%a)"}></span></td>
                                        <td class="ldr-td-name"><{$row.ladder.title|escape}></td>
                                        <td class="ldr-td-level"><{$roman_num[$row.level]|default:$row.ladder.config.level0|escape}></td>
                                        <td  class="ldr-td-status">
                                            <{foreach item=apr from=$row.appraiser_list}>
                                                <{if $apr.completed === 't'}>
                                                    <span class="label label-important" title="評価済み(<{$apr.update7|date_format_jp:"%-m月%-d日(%a) %R"}>)"><{$apr.emp_name|escape}></span><br/>
                                                <{else}>
                                                    <span class="label label-info" title="未評価"><{$apr.emp_name|escape}></span><br/>
                                                <{/if}>
                                            <{/foreach}>
                                        </td>
                                        <td class="ldr-td-status">
                                            <{if $row.result === '1'}>
                                                <i class="icon-circle"></i>
                                            <{else}>
                                                <i class="icon-remove"></i>
                                            <{/if}>
                                        </td>
                                        <td class="ldr-td-button">
                                            <{if $row.read === 't'}>
                                                <a href="accept_objection_modal.php?id=<{$row.ldr_apply_id|escape}>"
                                                   class="btn btn-small btn-success"
                                                   data-toggle="modal"
                                                   data-backdrop="true"
                                                   data-target="#objection">コメント済み</a>
                                            <{else}>
                                                <a href="accept_objection_modal.php?id=<{$row.ldr_apply_id|escape}>"
                                                   class="btn btn-small btn-danger"
                                                   data-toggle="modal"
                                                   data-backdrop="true"
                                                   data-target="#objection">未読</a>
                                            <{/if}>
                                        </td>
                                        <td class="ldr-td-button">
                                            <button type="button" data-id="<{$row.ldr_apply_id|escape}>" data-emp="<{$row.emp_id|escape}>" data-php="form_1view.php" data-mode="view" class="js-form-button btn btn-info btn-small">申請を見る</button>
                                        </td>
                                    </tr>
                                <{/foreach}>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>

        <{include file="__js.tpl"}>
        <{include file="__modal_view.tpl" modal_id="objection" modal_title="異議申し立て"}>
        <{include file="__modal_form_js.tpl"}>
        <{include form_selector="#accept_form" file="__pager_js.tpl"}>
        <script type="text/javascript">
            $(function() {

                /**
                 * モーダル閉じる(画面リロード)
                 */
                $('#objection').on('hidden', function() {
                    $('<form action="" method="post"></form>')
                        .append($('<input></input>').attr({type: 'hidden', name: 'unread', value: $('#unread').val()}))
                        .appendTo(document.body)
                        .submit()
                        .remove();
                });
            });
        </script>
    </body>
</html>