<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>
        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>
        <title>CoMedix キャリア開発ラダー | <{$titles.next_plan|escape}></title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title="`$titles.next_plan`"}>
            <div class="container-fluid">
                <div class="row-fluid">
                    <form class="form" id="form_next_plan" method="POST">
                        <div class="control-group">

                            <button type="button" class="js-btn-back btn btn-small" data-back="<{$smarty.post.owner_url|escape}>" style="margin-bottom: 10px;"><i class="icon-circle-arrow-left"></i> 戻る</button>

                            <select id="year" name="year" data-emp="<{$target_emp->emp_id()}>" class="input-medium js-pulldown-plan">
                                <{foreach item=asyear from=$year_list}>
                                    <option value="<{$asyear|escape}>" <{if $year == $asyear}>selected<{/if}>><{$asyear|escape}>年度</option>
                                <{/foreach}>
                            </select>
                            <div class="input-prepend">
                                <span class="add-on">本人</span>
                                <span class="ldr-uneditable input-medium" style="margin-bottom:0;"><{$target_emp->emp_name()|escape}></span>
                            </div>

                            <button type="button" class="js-next_year-sheet-print btn btn-small btn-primary pull-right" data-emp="<{$target_emp->emp_id()|escape}>"><i class="icon-print"></i> 印刷する</button>
                        </div>

                        <{foreach from=$error item=msg}>
                            <p class="text-error"><i class="icon-warning-sign"></i> <{$msg.0|escape}></p>
                        <{/foreach}>

                        <div class="well well-small">
                            <div class="alert alert-info">現在の役割・仕事</div>
                            <div class="ldr-indent">
                                <div class="control-group">
                                    <label class="control-label"><strong>自分の役割</strong></label>
                                    <textarea class="input-maxlarge js-autoheight" name="role"><{$list.role}></textarea>
                                </div>

                                <div class="control-group">
                                    <label class="control-label"><strong>自分の得意とすること</strong></label>
                                    <textarea class="input-maxlarge js-autoheight" name="special_skill"><{$list.special_skill}></textarea>
                                </div>

                                <div class="control-group">
                                    <label class="control-label"><strong>強化したいこと</strong></label>
                                    <textarea class="input-maxlarge js-autoheight" name="strengthen_skill"><{$list.strengthen_skill}></textarea>
                                </div>

                                <div class="control-group">
                                    <label class="control-label"><strong>今、学習していること</strong></label>
                                    <textarea class="input-maxlarge js-autoheight" name="studying_skill"><{$list.studying_skill}></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="well well-small">
                            <div class="alert alert-info">来年度の希望</div>
                            <div class="ldr-indent">
                                <div class="control-group">
                                    <label class="control-label"><strong>現在の部署で継続したいこと</strong></label>
                                    <textarea class="input-maxlarge js-autoheight" name="current_department"><{$list.current_department}></textarea><br/>
                                </div>

                                <div class="control-group">
                                    <label class="control-label"><strong>院内異動してしたいこと</strong></label>
                                    <textarea class="input-maxlarge js-autoheight" name="hospital_transfer"><{$list.hospital_transfer}></textarea>
                                </div>

                                <div class="control-group">
                                    <label class="control-label"><strong>学内異動してしたいこと</strong></label>
                                    <div class="controls">
                                        <{foreach from=$university_list item=var key=key}>
                                            <label class="checkbox inline">
                                                <input type="checkbox" name="university_transfer_destination[]" value="<{$key}>" <{if in_array($key, $list.university_transfer_destination)}>checked="checked"<{/if}>><{$var}>
                                            </label>
                                        <{/foreach}>
                                    </div>
                                    <textarea class="input-maxlarge js-autoheight" name="university_transfer"><{$list.university_transfer}></textarea>
                                </div>

                                <div class="control-group">
                                    <label class="control-label"><strong>その他</strong></label>
                                    <textarea class="input-maxlarge js-autoheight" name="etc"><{$list.etc}></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="well well-small">
                            <div class="alert alert-info">未来の自分ビジョン（こういう自分であるためにどう行動するか？）</div>
                            <div class="ldr-indent">
                                <div class="control-group">
                                    <textarea class="input-maxlarge js-autoheight" name="action"><{$list.action}></textarea>
                                </div>
                            </div>
                        </div>
                        <{if $emp->emp_id() === $target_emp->emp_id()}>
                            <div class="btn-toolbar">
                                <button type="submit" name="btn_save" id="btn_save" class="btn btn-small btn-primary" data-emp="<{$target_emp->emp_id()|escape}>" value="class"><i class="icon-save"></i> 保存する</button>
                            </div>
                        <{/if}>
                    </form>
                </div>
            </div>
        </div>

        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {
                /**
                 * 印刷
                 */
                $('.js-next_year-sheet-print').click(function() {
                    $('<form action="" method="post" target="next_plan" onsubmit="return openPDF(this);"></form>')
                        .append($('<input></input>').attr({type: 'hidden', name: 'target_emp_id', value: $(this).data('emp')}))
                        .append($('<input></input>').attr({type: 'hidden', name: 'mode', value: 'print'}))
                        .append($('<input></input>').attr({type: 'hidden', name: 'year', value: $('#year').val()}))
                        .appendTo(document.body)
                        .submit()
                        .remove();
                });

                /*
                 * 保存
                 */
                $('#btn_save').click(function() {
                    $('#form_next_plan')
                        .append($('<input></input>').attr({type: 'hidden', name: 'target_emp_id', value: $(this).data('emp')}))
                        .submit();
                });

                /*
                 * プルダウン選択
                 */
                $('.js-pulldown-plan').change(function() {
                    $('#form_next_plan')
                        .append($('<input></input>').attr({type: 'hidden', name: 'target_emp_id', value: $(this).data('emp')}))
                        .submit();
                });
            });
        </script>
    </body>
</html>
