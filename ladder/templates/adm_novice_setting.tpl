<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 新人評価管理：設定</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='新人評価管理：設定' hd_sub_title=''}>

            <div class="container-fluid">
                <{include file="__save_message.tpl"}>
                <div class="row-fluid">

                    <div class="well well-small">
                        <form action="adm_novice_setting.php" method="post">
                            <input type="hidden" name="tab" value="config" />
                            <div class="control-group">
                                <label class="control-label"><strong>評価の数字</strong></label>
                                <p>評価の数字のタイプを選んでください</p>
                                <div class="controls">
                                    <label class="inline radio">
                                        <input type="radio" name="novice_number" value="1" <{if $novice_number === '1'}>checked <{/if}>/>ローマ数字(<span class="ldr-roman">I</span>, <span class="ldr-roman">II</span>, <span class="ldr-roman">III</span>, <span class="ldr-roman">IV</span>)
                                    </label>
                                    <label class="inline radio">
                                        <input type="radio" name="novice_number" value="2" <{if $novice_number === '2'}>checked <{/if}> />アラビア数字(1, 2, 3, 4)
                                    </label>
                                </div>
                                <hr/>
                            </div>

                            <div class="btn-toolbar">
                                <button type="submit" name="btn_save" id="btn_save" class="btn btn-small btn-primary" value="btn_save"><i class="icon-save"></i> 保存する</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <script type="text/javascript">

            $(function() {

            });
        </script>
    </body>
</html>
