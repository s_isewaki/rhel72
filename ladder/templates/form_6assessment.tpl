<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | <{$ldr.config.status[22]|escape}>入力</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title=$ldr.config.status[22]}>

            <div class="container-fluid">
                <form method="post">
                    <{include file="__form_view.tpl" op_srh='false' tmp_id="form_6view"}>
                </form>
                <div class="well well-small">

                    <ul class="nav nav-tabs">
                        <li class="<{if $active !== 'assessment'}>active<{/if}>"><a href="#report" data-toggle="tab"><i class="icon-folder-close"></i> レポート</a></li>
                        <li class="<{if $active === 'assessment'}>active<{/if}>"><a href="#assessment" data-toggle="tab"><i class="icon-folder-close"></i> 評価</a></li>
                    </ul>

                    <div class="control-group">
                        <div class="input-prepend" style="margin-bottom:0;">
                            <span class="add-on">申請レベル</span>
                            <span class="ldr-uneditable input-mini ldr-level" style="margin-bottom:0;"><{$apply->level_text()|escape}></span>
                        </div>
                        <div class="input-prepend" style="margin-bottom:0;">
                            <span class="add-on">申請者</span>
                            <span class="ldr-uneditable input-large" style="margin-bottom:0;"><{$target_emp->emp_name()|escape}> (<{$target_emp->class_full_name()|escape}>)</span>
                        </div>
                    </div>

                    <div class="tab-content">
                        <div class="tab-pane <{if $active !== 'assessment'}>active<{else}>fade<{/if}>" id="report">
                            <hr style="margin:8px 0;"/>

                            <div class="control-group">
                                <label class="control-label">
                                    <strong>評価日</strong>
                                </label>
                                <div class="controls">
                                    <span class="ldr-uneditable input-medium"><{$form6.updated_on|date_format_jp:"%Y年%-m月%-d日(%a)"}></span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    <strong>事例内容</strong>
                                </label>
                                <div class="controls">
                                    <span class="input-maxlarge ldr-uneditable"><{$form6.narrative|escape|nl2br}></span>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane <{if $active === 'assessment'}>active<{else}>fade<{/if}>" id="assessment">

                            <{include file="__save_message.tpl" msg_text='確定しました'}>

                            <{foreach from=$error.assessment item=msg}>
                                <p class="alert alert-danger"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                            <{/foreach}>

                            <form class="js-form-assessment" action="" method="POST">
                                <input type="hidden" name="save" value="ajax" />
                                <{*画面遷移用*}>
                                <input type="hidden" name="mode" value="" />
                                <input type="hidden" name="menu" value="" />
                                <input type="hidden" name="id" value="" />
                                <input type="hidden" name="target_emp_id" value=""/>
                                <input type="hidden" name="owner_url" value="" />

                                <{foreach item=asm from=$assessment.list}>
                                    <{assign var='asmkey' value="_`$asm.apply_assessment`"}>
                                    <{if $asm.guideline_group2 !== $back_g2}>
                                        <hr style="margin:8px 0;"/>
                                        <label><strong><{$asm.group2_name|escape}></strong></label>
                                        <{assign var="back_g2" value=`$asm.guideline_group2`}>
                                    <{/if}>
                                    <div id="as-<{$asm.guideline_id|escape}>" class="inline">
                                        <div class="pull-left">
                                            <input
                                                type="hidden"
                                                class="input-assessment"
                                                id="in-<{$asm.guideline_id|escape}>"
                                                name="assessment[<{$asm.guideline_id|escape}>]"
                                                value="<{$asm.apply_assessment|escape}>"
                                                data-pop="#bt-<{$asm.guideline_id|escape}>"
                                                />
                                            <{if $ldr.config.report_select|@count > 2}>
                                                <div
                                                    class="btn-group"
                                                    style="margin-top:5px;"
                                                    id="bt-<{$asm.guideline_id|escape}>"
                                                    data-toggle="buttons-radio"
                                                    data-placement="bottom"
                                                    data-title="評価してください"
                                                    data-trigger="manual"
                                                    data-toggle="tooltip"
                                                    >
                                                    <{foreach item=sel from=$ldr.config.report_select}>
                                                        <button type="button"
                                                                class="btn btn-medium js-radio-assessment <{if $asm.apply_assessment === $sel.value}>btn-<{$sel.color}> active<{/if}>"
                                                                data-input="#in-<{$asm.guideline_id|escape}>"
                                                                data-alert="#al-<{$asm.guideline_id|escape}>"
                                                                data-container="#as-<{$asm.guideline_id|escape}>"
                                                                data-color="<{$sel.color|escape}>"
                                                                value="<{$sel.value|escape}>"
                                                                >
                                                            <{$sel.button|escape}>
                                                        </button>
                                                    <{/foreach}>
                                                </div>
                                            <{else}>
                                                <button
                                                    type="button"
                                                    id="bt-<{$asm.guideline_id|escape}>"
                                                    class="btn btn-large js-button-assessment btn-<{$ldr.config.report_select[$asmkey].color|default:'default'}>"
                                                    data-input="#in-<{$asm.guideline_id|escape}>"
                                                    data-alert="#al-<{$asm.guideline_id|escape}>"
                                                    data-trigger="manual"
                                                    data-placement="bottom"
                                                    data-container="#as-<{$asm.guideline_id|escape}>"
                                                    data-title="評価してください"
                                                    data-toggle="tooltip">
                                                    <{$ldr.config.report_select[$asmkey].button|default:'<i class="icon-question"></i>'}>
                                                </button>
                                            <{/if}>
                                        </div>
                                        <div class="clearfix" style="padding: 1px 0 0 <{$padleft|escape}>;">
                                            <div
                                                id="al-<{$asm.guideline_id|escape}>"
                                                class="alert alert-<{$ldr.config.report_select[$asmkey].color|default:'default'}>">
                                                <{$asm.guideline|escape}>
                                            </div>
                                        </div>
                                    </div>
                                <{/foreach}>

                                <hr style="margin:8px 0;"/>

                                <div class="control-group">
                                    <label>
                                        <strong>評価者からのメッセージ</strong>
                                        <span class="badge badge-important required">必須</span>
                                    </label>
                                    <div class="controls">
                                        <textarea name="comment" rows="3" class="input-maxlarge js-autoheight"></textarea>
                                    </div>
                                </div>

                                <div class="btn-toolbar">
                                    <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                                    <{if $assessment.completed !== 't'}>
                                        <button type="submit" name="btn_save" id="btn_save" class="btn btn-small btn-primary" value="completed"><i class="icon-save"></i> 評価を確定する</button>
                                    <{else}>
                                        <button type="button" class="btn btn-small btn-primary" disabled="disabled"><i class="icon-save"></i> 評価完了済</button>
                                    <{/if}>
                                </div>
                                <p><small><i class="icon-exclamation-sign"></i> 入力内容は自動保存されます。評価を終える場合は<b>[評価を確定する]ボタン</b>で保存してください。</small></p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {
                $('body').on('click', 'button.js-button-assessment', function() {
                    $(this).tooltip('destroy');

                    var input = $($(this).data('input'));
                    var alert = $($(this).data('alert'));

            <{if $ldr.config.report_select|@count <= 2}>
                    var value = '';
                <{foreach item=sel from=$ldr.config.report_select|@array_reverse}>
                    if (input.val() + '' !== '<{$sel.value|escape:'javascript'}>') {
                        $(this)
                            .html('<{$sel.button|escape:'javascript'}>')
                            .attr('class', 'js-button-assessment btn btn-large btn-<{$sel.color|escape:'javascript'}>');
                        alert.attr('class', 'alert alert-<{$sel.color|escape:'javascript'}>');
                        value = '<{$sel.value|escape:'javascript'}>';
                    }
                <{/foreach}>
                    input.val(value).change();
            <{/if}>
                });

                $('body').on('click', 'button.js-radio-assessment', function() {
                    $(this).parent('div').tooltip('destroy');

                    var input = $($(this).data('input'));
                    var alert = $($(this).data('alert'));
                    input.val($(this).attr('value')).change();

                    $(this).siblings().removeClass('btn-primary');
                    $(this).siblings().removeClass('btn-info');
                    $(this).siblings().removeClass('btn-success');
                    $(this).siblings().removeClass('btn-warning');
                    $(this).siblings().removeClass('btn-danger');
                    $(this).addClass('btn-' + $(this).data('color'));
                    alert.attr('class', 'alert alert-' + $(this).data('color'));
                });

                $('body').on('submit', 'form.js-form-assessment', function() {
                    // validate
                    var error = false;
                    $(this).find('input.input-assessment').each(function(idx, input) {
                        var inp = $(input);
                        if (inp.val() === '') {
                            $(inp.data('pop')).tooltip('show');
                            error = true;

                        }
                    });
                    if (error) {
                        return false;
                    }

                    if (confirm("評価を確定します。よろしいですか？")) {
                        return true;
                    }
                    return false;
                });

                // 自動保存
                $('form').autosave({
                    callbacks: {
                        trigger: 'change',
                        scope: 'all',
                        save: {
                            method: 'ajax',
                            options: {
                                url: 'form_6assessment.php',
                                method: 'post'
                            }
                        }
                    }
                });
                $('form').autosave({
                    callbacks: {
                        trigger: 'modify',
                        scope: 'all',
                        save: {
                            method: 'ajax',
                            options: {
                                url: 'form_6assessment.php',
                                method: 'post'
                            }
                        }
                    }
                });
            });
        </script>
    </body>
</html>