<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 申込受付</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">

            <{include file="__header.tpl" hd_title='申込受付' hd_sub_title=''}>

            <div class="container-fluid">
                <form id="apply_form" method="post">
                    <input type="hidden" name="mode" value="" />
                    <input type="hidden" name="menu" value="accept" />
                    <input type="hidden" name="id" value="<{$id}>" />
                    <input type="hidden" name="owner_url" value="accept_qualify.php" />
                    <input type="hidden" name="target_emp_id" value="<{$target_emp->emp_id()}>" />
                    <div class="btn-toolbar">
                        <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                    </div>

                    <{if $form1}>

                        <div class="block">
                            <div class="block-body collapse in">
                                <{include file="__form_btn.tpl"}>
                            </div>
                        </div>

                        <div class="well well-small">
                            <input type="hidden" name="ldr_apply_id" value="" />
                            <div class="control-group">
                                <label class="control-label"><strong>申請者</strong></label>
                                <div class="controls"><span class="ldr-uneditable input-medium"><{$target_emp->emp_name()|escape}></span></div>
                            </div>
                            <div class="control-group">
                                <label class="control-label"><strong>申請者部署</strong></label>
                                <div class="controls"><span class="ldr-uneditable input-xxlarge"><{$target_emp->class_full_name()|escape}></span></div>
                            </div>
                            <div class="control-group">
                                <label class="control-label"><strong>申請日</strong></label>
                                <div class="controls"><span class="ldr-uneditable"><{$form1.apply_date|date_format_jp:"%Y年%-m月%-d日(%a)"}>
                                        <input type="hidden" name="apply_date" value="<{$form1.apply_date}>" /></span></div>
                            </div>
                            <div class="control-group">
                                <label class="control-label"><strong>申請レベル</strong></label>
                                <input type="hidden" name="level" class="level" value="<{$level|escape}>" />
                                <div class="controls"><span class="ldr-uneditable input-mini ldr-level"><{$apply->level_text()|escape}></span></div>
                            </div>
                            <div class="control-group">
                                <label class="control-label"><strong>当面のあなたの目標</strong></label>
                                <div class="controls">
                                    <span class="ldr-uneditable input-maxlarge"><{$form1.objective|escape|nl2br}></span>
                                </div>
                            </div>
                            <{if $ldr.config.flow === '1'}>
                                <{if $ldr.config.meeting === '1'}>
                                    <div class="control-group">
                                        <label class="control-label">
                                            <strong>評価会日時</strong>
                                        </label>
                                        <div class="controls">
                                            <span class="ldr-uneditable"><{$form1.meeting_date|date_format_jp:"%Y年%-m月%-d日(%a)"|default:'(未設定)'}></span>
                                            <span class="ldr-uneditable"><{$form1.meeting_start_time|default:'(未設定)'|escape}></span> 〜
                                            <span class="ldr-uneditable"><{$form1.meeting_last_time|default:'(未設定)'|escape}></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">
                                            <strong>評価会会場</strong>
                                        </label>
                                        <div class="controls">
                                            <span class="input-xlarge ldr-uneditable"><{$form1.meeting_room|default:'(未設定)'|escape}></span>
                                        </div>
                                    </div>
                                <{/if}>
                            <{/if}>
                            <{if $ldr.config.appr_max[$level] > 0}>
                                <{if $ldr.config.appr === '2'}>
                                    <div class="control-group">
                                        <label class="control-label">
                                            <strong>評価者</strong>
                                            <{if $apply->get_status() >= '4'}>
                                                <span class="badge badge-important required">必須</span>
                                            <{/if}>
                                        </label>

                                        <{if $ldr.config.flow === '1'}>
                                            <div>
                                                <button type="button" class="btn btn-small btn-success js-btn-emplist" data-id="facilitator"><i class="icon-book"></i> 司会者 を選択してください</button>
                                            </div>
                                            <div class="controls clearfix">
                                                <div id="facilitator" class="ldr-empbox <{if !$facilitator}>hide<{/if}>">
                                                    <{foreach from=$facilitator key=id item=name}>
                                                        <div class="label label-info"><{$name|escape}>(司会者)<input type="hidden" name="facilitator[<{$id|escape}>]" value="<{$name|escape}>" /> <a href="javascript:void(0);" class="ldr-emp-remove js-emp-remove">&times;</a></div>
                                                    <{/foreach}>
                                                </div>
                                            </div>
                                            <{foreach from=$error.facilitator item=msg}>
                                                <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                            <{/foreach}>
                                            <div class="btn-toolbar">
                                            </div>
                                        <{/if}>

                                        <div>
                                            <button type="button" class="btn btn-small btn-success js-btn-emplist" data-id="appraiser"><i class="icon-book"></i> <{$ldr.config.appr_name|escape}>者 を選択してください</button>
                                        </div>
                                        <div class="controls clearfix">
                                            <div id="appraiser" class="ldr-empbox <{if !$appraiser}>hide<{/if}>">
                                                <{foreach from=$appraiser key=id item=name}>
                                                    <div class="label label-info"><{$name|escape}><input type="hidden" name="appraiser[<{$id|escape}>]" value="<{$name|escape}>" /> <a href="javascript:void(0);" class="ldr-emp-remove js-emp-remove">&times;</a></div>
                                                <{/foreach}>
                                            </div>
                                        </div>
                                        <{foreach from=$error.appraiser item=msg}>
                                            <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                        <{/foreach}>
                                    </div>
                                <{else}>
                                    <{if $ldr.config.flow === '1'}>
                                        <div class="control-group">
                                            <label class="control-label">
                                                <strong>司会者</strong>
                                            </label>
                                            <div class="controls clearfix">
                                                <div id="facilitator" class="ldr-empbox" style="margin-top:3px !important;">
                                                    <{foreach from=$form1.facilitator key=id item=name}>
                                                        <div class="label label-info"><{$name|escape}></div>
                                                    <{/foreach}>
                                                </div>
                                            </div>
                                        </div>
                                    <{/if}>
                                    <div class="control-group">
                                        <label class="control-label">
                                            <strong>評価者</strong>
                                        </label>
                                        <div class="controls clearfix">
                                            <div id="facilitator" class="ldr-empbox" style="margin-top:3px !important;">
                                                <{foreach from=$form1.appraiser key=id item=name}>
                                                    <div class="label label-info"><{$name|escape}></div>
                                                <{/foreach}>
                                            </div>
                                        </div>
                                    </div>
                                <{/if}>
                            <{/if}>

                            <{if $ldr.config.checklist[$level]}>
                                <div class="control-group">
                                    <label class="control-label"><strong>チェックシート</strong></label>
                                    <div class="controls">
                                        <{assign var=checklist value="\n"|explode:$ldr.config.checklist[$level]}>
                                        <{foreach from=$checklist item=check}>
                                            <label class="checkbox"><input type="checkbox" class="checklist" /><{$check|escape}></label>
                                        <{/foreach}>
                                    </div>
                                </div>
                            <{/if}>

                            <div class="control-group">
                                <label class="control-label" for="comment"><strong>コメント</strong></label>
                                <div class="controls">
                                    <textarea id="comment" name="comment" rows="2" class="input-xxlarge js-autoheight"></textarea>
                                    <{foreach from=$error.comment item=msg}>
                                        <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                    <{/foreach}>
                                </div>
                            </div>
                        </div>
                    <{else}>
                        <div class="alert alert-error">申請が見つかりませんでした</div>
                    <{/if}>
                    <div class="btn-toolbar">
                        <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                        <{if $form1}>
                            <button type="submit" name="form_submit" class="btn btn-small btn-primary js-submit-accept" value="save"><i class="icon-edit"></i> 受付する</button>
                            <button type="submit" name="form_submit" class="btn btn-small btn-danger js-submit-sendback" value="back"><i class="icon-backward"></i> 申請を差し戻す</button>
                        <{/if}>
                    </div>
                </form>

            </div>
            <{include file="__js.tpl"}>
            <script type="text/javascript">
                $(function() {

                    $(document).on("click", ".eva_icon", function() {
                        id = $(this).attr("id").replace('i_', '');
                        open_emplist(id);
                    });

                    // 評価会開始時間を終了時間にコピー
                    if ($('#meeting_start_time').get(0)) {
                        $('#meeting_start_time').parent(".timepicker").on('changeDate', function(e) {
                            if ($('#meeting_start_time').val() > $('#meeting_last_time').val()) {
                                $('#meeting_last_time').val($('#meeting_start_time').val());
                                $('#meeting_last_time').change();
                            }
                        });
                    }

                    // サブミットイベント
                    $("#fm1").submit(function() {
                        var dt = new Date();
                        var y = dt.getFullYear();
                        var m = dt.getMonth() + 1;
                        var d = dt.getDate();
                        var tmp1 = new Date(y + "/" + m + "/" + d + " " + $('#meeting_start_time').val());
                        var tmp2 = new Date(y + "/" + m + "/" + d + " " + $('#meeting_last_time').val());
                        if (tmp1 >= tmp2) {
                            alert("開始時間 ＜ 終了時間 で設定してください。");
                            return false;
                        }

                        $('#fm1').submit();
                    });

                    // 評価日は申請日の15日以降
                    $('#apply_date').parent(".datepicker").on('changeDate', function(e) {
                        var startDate = e.localDate ? new Date(e.localDate.getTime()) : new Date($('#apply_date').val().replace('-', '/'));
                        startDate.setMinutes(startDate.getMinutes() - startDate.getTimezoneOffset());
                        startDate.setDate(startDate.getDate() + <{$ldr.config.appr_interval|default:15}>);
                        if ($('#meeting_date').get(0)) {
                            $('#meeting_date').parent(".datepicker").data('datetimepicker').setStartDate(startDate);
                        }
                    });
                    $('#apply_date').parent(".datepicker").trigger('changeDate');

                    /*
                     * 差し戻しの場合はコメント必須
                     */
                    $('.js-submit-sendback').click(function() {
                        if ($('#comment').val() === '') {
                            alert('差し戻しの場合は、コメントを必ず入力してください');
                            return false;
                        }
                        return true;
                    });

                    /*
                     * 受付前にチェックシートのチェック
                     */
                    $('.js-submit-accept').click(function() {
                        var check = true;
                        $('input.checklist').each(function() {
                            if (!$(this).prop('checked')) {
                                check = false;
                                return;
                            }
                        });
                        if (!check) {
                            alert('チェックシートに未チェックの項目があります');
                            return false;
                        }
                        return true;
                    });

                    // 職員削除
                    $('body').on('click', '.js-emp-remove', function() {
                        $(this).parent().remove();
                    });

                });

                function closeEmployeeList() {
                    if (childwin !== null && !childwin.closed) {
                        childwin.close();
                    }
                    childwin = null;
                }

                // 職員の追加
                function add_target_list(item_id, emp_id, emp_name) {
                    var emp = emp_id.split(", ");
                    if (emp.length !== 1) {
                        return;
                    }
                    if ($('#e' + emp_id).size() === 0) {
                        if (String(emp_id) === '<{$target_emp->emp_id()}>') {
                            alert("申請者を評価者には設定できません");
                            return false;
                        }

                        if (item_id === 'facilitator') {
                            $('#facilitator').html('<div id="e' + emp_id + '" class="label label-info">' + emp_name + '(司会者) <input type="hidden" name="facilitator[' + emp_id + ']" value="' + emp_name + '" /> <a href="javascript:void(0);" class="ldr-emp-remove js-emp-remove">&times;</a></div>').show();
                        }
                        else {
                            var add = '<div id="e' + emp_id + '" class="label label-info">' + emp_name + '<input type="hidden" name="appraiser[' + emp_id + ']" value="' + emp_name + '" /> <a href="javascript:void(0);" class="ldr-emp-remove js-emp-remove">&times;</a></div> ';
                            $('#appraiser').append(add).show();
                        }
                    }
                }
            </script>
    </body>
</html>