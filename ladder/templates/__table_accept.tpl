<table width="100%" class="table table-bordered table-hover table-condensed ldr-table">
    <thead>
        <tr>
            <th><a href="?order=updated_on&desc=<{if $order === "training_date" && $desc === '0'}>1<{else}>0<{/if}>">受付日時</a></th>
            <th><a href="?order=apply_updated_on&desc=<{if $order === "apply_updated_on" && $desc === '0'}>1<{else}>0<{/if}>">更新日時</a></th>
            <th>申請者</th>
            <th><a href="?order=apply_date&desc=<{if $order === "apply_date" && $desc === '0'}>1<{else}>0<{/if}>">申請日</a></th>
            <th>ラダー</th>
            <th><a href="?order=level&desc=<{if $order === "level" && $desc === '0'}>1<{else}>0<{/if}>">Lv</a></th>
            <th>ステータス</th>
            <th>コメント</th>
            <th>受付</th>
            <th>評価者</th>

            <{if $hospital_type === '1'}><th>監査者</th><{/if}>

            <th>様式</th>
        </tr>
    </thead>
    <tbody>
        <{foreach item=row from=$list}>
            <tr>
                <td class="ldr-td-date"><span title="<{$row.updated_on|date_format_jp:"%Y年%-m月%-d日(%a)"}>"><{$row.updated_on|date_format_jp:"%-m月%-d日(%a)"}></span><br/><{$row.updated_on|date_format_jp:"%R"}></td>
                <td class="ldr-td-date"><span title="<{$row.apply_updated_on|date_format_jp:"%Y年%-m月%-d日(%a)"}>"><{$row.apply_updated_on|date_format_jp:"%-m月%-d日(%a)"}></span><br/><{$row.apply_updated_on|date_format_jp:"%R"}></td>
                <td class="ldr-td-status"><{$row.emp_name|escape}></td>
                <td class="ldr-td-date"><span class="ldr-td-date-bold" title="<{$row.apply_date|date_format_jp:"%Y年%-m月%-d日(%a)"}>"><{$row.apply_date|date_format_jp:"%-m月%-d日(%a)"}></span></td>
                <td class="ldr-td-name"><{$row.ladder.title|escape}></td>
                <td class="ldr-td-level"><{$roman_num[$row.level]|default:$row.ladder.config.level0|escape}></td>
                <td class="ldr-td-status">
                    <a
                        href="apply_status_modal.php?id=<{$row.ldr_apply_id|escape}>"
                        data-toggle="modal"
                        data-backdrop="true"
                        data-target="#modal_status">
                        <{include file="__table_status_text.tpl"}>
                    </a>
                    <{if $row.status === '0'}>
                        <br/>
                        <a href="result_confirm_modal.php?id=<{$row.ldr_apply_id|escape}>" data-toggle="modal" data-target="#result_confirm">(結果確認)</a>
                    <{/if}>
                </td>
                <td  class="ldr-td-button">
                    <a
                        href="comment_modal.php?id=<{$row.ldr_apply_id|escape}>"
                        data-toggle="modal"
                        data-backdrop="true"
                        data-target="#modal_comment"
                        class="js-comment-modal btn btn-small <{if $row.unread}>btn-warning<{else}>btn-success<{/if}>"
                        <{if $row.unread}>
                            rel="tooltip" title="未読が<{$row.unread|escape}>件あります"
                        <{/if}>
                        ><{$row.comment|escape}></a>
                </td>
                <td class="ldr-td-button">
                    <{if $row.status === '5' && !$row.report_status}>
                        <button type="button" data-id="<{$row.ldr_apply_id|escape}>" data-emp="<{$row.emp_id|escape}>" class="js-accept-qualify btn btn-danger btn-small">申込受付</button>
                    <{elseif $row.status === '8' && $row.appraiser}>
                        <button type="button" data-id="<{$row.ldr_apply_id|escape}>" data-emp="<{$row.emp_id|escape}>" data-php="form_7edit_appraiser.php" data-mode="edit" class="js-form-button btn btn-danger btn-small"><{$row.ladder.config.form_7.btn|escape}>入力</button>
                    <{elseif $row.status === '9' && $row.ladder.config.flow === '1' && $row.facilitator === "t"}>
                        <button type="button" data-id="<{$row.ldr_apply_id|escape}>" data-emp="<{$row.emp_id|escape}>" data-php="form_8edit.php" data-mode="edit" class="js-form-button btn btn-danger btn-small"><{$row.ladder.config.form_8.btn|escape}>入力</button>
                    <{elseif $row.status === '9' && $row.ladder.config.flow !== '1' && $row.total_appraiser}>
                        <button type="button" data-id="<{$row.ldr_apply_id|escape}>" data-emp="<{$row.emp_id|escape}>" data-php="form_8edit_total.php" data-mode="edit" class="js-form-button btn btn-danger btn-small"><{$row.ladder.config.form_8.btn|escape}>入力</button>
                    <{elseif $row.status === '10' && $row.permission_auditor === "t"}>
                        <button type="button" data-id="<{$row.ldr_apply_id|escape}>" data-emp="<{$row.emp_id|escape}>" class="js-accept-auditor btn btn-danger btn-small">監査者設定</button>
                    <{elseif $row.status === '11'}>
                        <{if $row.permission_auditor === "t"}>
                            <button type="button" data-id="<{$row.ldr_apply_id|escape}>" data-emp="<{$row.emp_id|escape}>" class="js-accept-auditor btn btn-danger btn-small">監査者再設定</button>
                        <{/if}>
                        <{if $row.auditor === $emp->emp_id()}>
                            <button type="button" data-id="<{$row.ldr_apply_id|escape}>" data-emp="<{$row.emp_id|escape}>" data-php="form_9edit.php" data-mode="edit" class="js-form-button btn btn-danger btn-small"><{$row.ladder.config.form_9.btn|escape}>入力</button>
                        <{/if}>
                    <{elseif $row.status === '12' && $row.committee}>
                        <button type="button" data-id="<{$row.ldr_apply_id|escape}>" data-emp="<{$row.emp_id|escape}>" class="js-accept-result btn btn-danger btn-small">合否認定</button>
                    <{elseif $row.report_status === '21' && $row.committee}>
                        <button type="button" data-id="<{$row.ldr_apply_id|escape}>" data-emp="<{$row.emp_id|escape}>" class="js-accept-reporter btn btn-danger btn-small"><{$data.config.status[21]|default:'レポート評価者設定'|escape}></button>
                    <{elseif $row.report_status === '22'}>
                        <{if $row.reporter}>
                            <button type="button" data-id="<{$row.ldr_apply_id|escape}>" data-emp="<{$row.emp_id|escape}>" class="js-accept-report btn btn-danger btn-small"><{$data.config.status[22]|default:'レポート評価'|escape}></button>
                        <{/if}>
                        <{if $row.committee}>
                            <button type="button" data-id="<{$row.ldr_apply_id|escape}>" data-emp="<{$row.emp_id|escape}>" class="js-accept-reporter btn btn-danger btn-small"><{$data.config.status[21]|default:'レポート評価者設定'|escape}></button>
                        <{/if}>
                    <{elseif $row.report_status === '23'}>
                        <{if $row.reporter}>
                            <button type="button" data-id="<{$row.ldr_apply_id|escape}>" data-emp="<{$row.emp_id|escape}>" class="js-accept-report btn btn-danger btn-small"><{$data.config.status[22]|default:'レポート評価'|escape}></button>
                        <{/if}>
                        <{if $row.committee}>
                            <button type="button" data-id="<{$row.ldr_apply_id|escape}>" data-emp="<{$row.emp_id|escape}>" class="js-accept-report-confirm btn btn-danger btn-small"><{$data.config.status[23]|default:'レポート評価確認'|escape}></button>
                        <{/if}>
                    <{else}>
                        -
                    <{/if}>
                </td>
                <td  class="ldr-td-status">
                    <{if $row.ladder.config.anonymous !== '1' || $row.committee || $row.total_appraiser}>
                        <{foreach item=apr from=$row.appraiser_list}>
                            <{if $apr.completed === 't'}>
                                <span class="label label-important" title="評価済み(<{$apr.update7|date_format_jp:"%-m月%-d日(%a) %R"}>)"><{$apr.emp_name|escape}></span><br/>
                            <{else}>
                                <span class="label label-info" title="未評価"><{$apr.emp_name|escape}></span><br/>
                            <{/if}>
                        <{/foreach}>
                    <{/if}>
                </td>

                <{if $hospital_type === '1'}>
                    <td class="ldr-td-status">
                        <{foreach item=aprll from=$row.auditor_list}>
                            <span class="label label-info"><{$aprll|escape}></span><br/>
                        <{/foreach}>
                    </td>
                <{/if}>

                <td class="ldr-td-form">
                    <button type="button" data-id="<{$row.ldr_apply_id|escape}>" data-emp="<{$row.emp_id|escape}>" data-php="form_1view.php" data-mode="view" class="js-form-button btn btn-info btn-small"><{$row.ladder.config.form_1.btnS|escape}></button>


                    <{if $row.ladder.config.flow === '1'}>
                        <{if $function.form_2}>
                            <button type="button" data-id="<{$row.ldr_apply_id|escape}>" data-emp="<{$row.emp_id|escape}>" data-php="form_2view.php" data-mode="view" class="js-form-button btn btn-info btn-small"><{$titles.form_2.btnS|escape}></button>
                        <{/if}>
                        <{if $function.form_3}>
                            <button type="button" data-id="<{$row.ldr_apply_id|escape}>" data-emp="<{$row.emp_id|escape}>" data-php="form_3view.php" data-mode="view" class="js-form-button btn btn-info btn-small"><{$titles.form_3.btnS|escape}></button>
                        <{/if}>

                        <{if $function.form_4}>
                            <button type="button" data-id="<{$row.ldr_apply_id|escape}>" data-emp="<{$row.emp_id|escape}>" data-php="form_4view.php" data-mode="view" class="js-form-button btn btn-info btn-small"><{$titles.form_4.btnS|escape}></button>
                        <{/if}>

                        <{if $function.form_5}>
                            <button type="button" data-id="<{$row.ldr_apply_id|escape}>" data-emp="<{$row.emp_id|escape}>" data-php="form_5view.php" data-mode="view" class="js-form-button btn btn-info btn-small"><{$titles.form_5.btnS|escape}></button>
                        <{/if}>
                    <{else}>
                        <{if $function.record_view}>
                            <button type="button" data-id="<{$row.ldr_apply_id|escape}>" data-emp="<{$row.emp_id|escape}>" data-php="records_view.php" data-mode="view" class="js-form-button btn btn-info btn-small"><{$titles.form_2.btnS|escape}></button>
                        <{/if}>
                    <{/if}>

                    <{*様式6*}>
                    <{if $row.status <= '-2' || $row.status === '0' || $row.status >= '4'}>
                        <button type="button" data-id="<{$row.ldr_apply_id|escape}>" data-emp="<{$row.emp_id|escape}>" data-php="form_6view.php" data-mode="view" class="js-form-button btn btn-info btn-small"><{$row.ladder.config.form_6.btnS|escape}></button>
                    <{else}>
                        <button class="btn btn-small" disabled="disabled"><{$row.ladder.config.form_6.btnS|escape}></button>
                    <{/if}>

                    <{*様式7*}>
                    <{if $row.status === '8' && $row.appraiser}>
                        <button type="button" data-id="<{$row.ldr_apply_id|escape}>" data-emp="<{$row.emp_id|escape}>" data-php="form_7edit_appraiser.php" data-mode="edit" class="js-form-button btn btn-danger btn-small"><{$row.ladder.config.form_7.btnS|escape}></button>
                    <{elseif $row.status <= '-2' || $row.status === '0' || $row.status >= '4'}>
                        <button type="button" data-id="<{$row.ldr_apply_id|escape}>" data-emp="<{$row.emp_id|escape}>" data-php="form_7view.php" data-mode="view" class="js-form-button btn btn-info btn-small"><{$row.ladder.config.form_7.btnS|escape}></button>
                    <{else}>
                        <button class="btn btn-small" disabled="disabled"><{$row.ladder.config.form_7.btnS|escape}></button>
                    <{/if}>

                    <{*様式8*}>
                    <{if $row.ladder.config.flow === '1'}>
                        <{if $row.status === '9' && $row.facilitator === "t"}>
                            <button type="button" data-id="<{$row.ldr_apply_id|escape}>" data-emp="<{$row.emp_id|escape}>" data-php="form_8edit.php" data-mode="edit" class="js-form-button btn btn-danger btn-small"><{$row.ladder.config.form_8.btnS|escape}></button>
                        <{elseif $row.status === '0' || $row.status > '9'}>
                            <button type="button" data-id="<{$row.ldr_apply_id|escape}>" data-emp="<{$row.emp_id|escape}>" data-php="form_8view.php" data-mode="view" class="js-form-button btn btn-info btn-small"><{$row.ladder.config.form_8.btnS|escape}></button>
                        <{else}>
                            <button class="btn btn-small" disabled="disabled"><{$row.ladder.config.form_8.btnS|escape}></button>
                        <{/if}>
                    <{/if}>

                    <{*様式9*}>
                    <{if $row.ladder.config.flow === '1'}>
                        <{if $row.status === '11' && $row.auditor === $emp->emp_id()}>
                            <button type="button" data-id="<{$row.ldr_apply_id|escape}>" data-emp="<{$row.emp_id|escape}>" data-php="form_9edit.php" data-mode="edit" class="js-form-button btn btn-danger btn-small"><{$row.ladder.config.form_9.btnS|escape}></button>
                        <{elseif $row.status === '0' || $row.status > '11'}>
                            <button type="button" data-id="<{$row.ldr_apply_id|escape}>" data-emp="<{$row.emp_id|escape}>" data-php="form_9view.php" data-mode="view" class="js-form-button btn btn-info btn-small"><{$row.ladder.config.form_9.btnS|escape}></button>
                        <{else}>
                            <button class="btn btn-small" disabled="disabled"><{$row.ladder.config.form_9.btnS|escape}></button>
                        <{/if}>
                    <{/if}>

                    <{if $row.ladder.config.flow === '1'}>
                        <{if $function.form_carrier}>
                            <{*他院での研修*}>
                            <button type="button" data-id="<{$row.ldr_apply_id}>" data-emp="<{$row.emp_id|escape}>" data-php="form_carrier_view.php" data-mode="view" class="js-form-button btn btn-info btn-small"><{$titles.form_carrier.btnS|escape}></button>
                        <{/if}>
                        <{if $function.form_transfer}>
                            <{*当院での異動*}>
                            <button type="button" data-id="<{$row.ldr_apply_id}>" data-emp="<{$row.emp_id|escape}>" data-php="form_transfer_view.php" data-mode="view" class="js-form-button btn btn-info btn-small"><{$titles.form_transfer.btnS|escape}></button>
                        <{/if}>
                        <{if $function.form_presentation}>
                            <{*学会発表記録*}>
                            <button type="button" data-id="<{$row.ldr_apply_id}>" data-emp="<{$row.emp_id|escape}>" data-php="form_presentation_view.php" data-mode="view" class="js-form-button btn btn-info btn-small"><{$titles.form_presentation.btnS|escape}></button>
                        <{/if}>
                        <{if $function.form_article}>
                            <{*学会掲載記録*}>
                            <button type="button" data-id="<{$row.ldr_apply_id}>" data-emp="<{$row.emp_id|escape}>" data-php="form_article_view.php" data-mode="view" class="js-form-button btn btn-info btn-small"><{$titles.form_article.btnS|escape}></button>
                        <{/if}>

                    <{/if}>
                </td>
            </tr>
        <{/foreach}>
    </tbody>
</table>
