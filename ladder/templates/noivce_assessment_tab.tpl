<form action="novice_assessment.php" method="post" class="js-form-assessment">
    <input type="hidden" name="mode" value="save" />
    <input type="hidden" name="year" value="<{$year|escape}>" />
    <input type="hidden" name="number" value="<{$number|escape}>" />
    <input type="hidden" name="g1" value="g1-<{$group1|escape}>" />
    <div class="well well-small">
        <{foreach item=row from=$group2_list}>
            <p><i class="icon-star"></i> <b><{$row.name|escape}></b></p>
            <{foreach item=asm from=$assessment[$row.group2]}>
                <div id="as-<{$asm.guideline_id|escape}>" class="inline">
                    <div class="pull-left">
                        <input type="hidden" id="in-<{$asm.guideline_id|escape}>" name="assessment[<{$asm.guideline_id|escape}>]" value="<{$asm.assessment|escape}>" />
                        <div class="btn-group" data-toggle="buttons-radio" style="margin-top:5px;">
                            <button type="button"
                                    title="できる" rel="tooltip" data-placement="bottom"
                                    class="ldr-level btn btn-medium js-radio-assessment <{if $asm.assessment === '1'}>btn-info active<{/if}>"
                                    name="assessment[<{$asm.guideline_id|escape}>]"
                                    data-input="#in-<{$asm.guideline_id|escape}>"
                                    data-alert="#al-<{$asm.guideline_id|escape}>"
                                    data-container="#as-<{$asm.guideline_id|escape}>"
                                    value="1"
                                    ><{$nlv[1]}></button>
                            <button type="button"
                                    title="指導の下でできる" rel="tooltip" data-placement="bottom"
                                    class="ldr-level btn btn-medium js-radio-assessment <{if $asm.assessment === '2'}>btn-info active<{/if}>"
                                    name="assessment[<{$asm.guideline_id|escape}>]"
                                    data-input="#in-<{$asm.guideline_id|escape}>"
                                    data-alert="#al-<{$asm.guideline_id|escape}>"
                                    data-container="#as-<{$asm.guideline_id|escape}>"
                                    value="2"
                                    ><{$nlv[2]}></button>
                            <button type="button"
                                    title="演習でできる" rel="tooltip" data-placement="bottom"
                                    class="ldr-level btn btn-medium js-radio-assessment <{if $asm.assessment === '3'}>btn-info active<{/if}>"
                                    name="assessment[<{$asm.guideline_id|escape}>]"
                                    data-input="#in-<{$asm.guideline_id|escape}>"
                                    data-alert="#al-<{$asm.guideline_id|escape}>"
                                    data-container="#as-<{$asm.guideline_id|escape}>"
                                    value="3"
                                    ><{$nlv[3]}></button>
                            <button type="button"
                                    title="知識としてわかる" rel="tooltip" data-placement="bottom"
                                    class="ldr-level btn btn-medium js-radio-assessment <{if $asm.assessment === '4'}>btn-info active<{/if}>"
                                    name="assessment[<{$asm.guideline_id|escape}>]"
                                    data-input="#in-<{$asm.guideline_id|escape}>"
                                    data-alert="#al-<{$asm.guideline_id|escape}>"
                                    data-container="#as-<{$asm.guideline_id|escape}>"
                                    value="4"
                                    ><{$nlv[4]}></button>
                        </div>
                    </div>
                    <div class="clearfix" style="padding: 1px 0 0 155px;">
                        <div
                            id="al-<{$asm.guideline_id|escape}>"
                            style="margin-bottom: 0;"
                            class="alert <{if $asm.assessment >= '1'}>alert-info<{else}>alert-default<{/if}>">
                            <{$asm.guideline|escape}>
                        </div>
                    </div>
                </div>
                <hr style="margin:8px 0;"/>
            <{/foreach}>
        <{/foreach}>
        <div class="btn-toolbar">
            <button type="submit" name="btn_save" id="btn_save" class="btn btn-small btn-primary" value="btn_save"><i class="icon-save"></i> 保存する</button>
        </div>
        <p><small><i class="icon-exclamation-sign"></i> 入力内容は自動保存されます。明示的に保存したい場合は<b>[保存する]ボタン</b>で保存してください。</small></p>
    </div>
</form>
