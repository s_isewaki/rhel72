<form id="form_mst_relief" action="adm_relief.php" method="post">
    <div class="form-horizontal">
        <div id="modal_subject_subject" class="control-group">
            <label class="control-label">
                <span class="badge badge-important required">必須</span>
                <strong>科目</strong>
            </label>
            <div class="controls">
                <input type="text" name="subject" value="" maxlength="200" class="input-xlarge" placeholder="科目を入力してください" />
                <div id="error_subject_subject" class="help-block hide"></div>
            </div>
        </div>
    </div>
    <input type="hidden" name="id" value="" />
    <input type="hidden" name="mode" value="subject" />
    <input type="hidden" id="mode2_subject" name="mode2" value="validate" />
</form>
