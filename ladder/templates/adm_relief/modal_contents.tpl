<form id="form_mst_contents" action="adm_relief.php" method="post">
    <div class="form-horizontal">
        <div id="modal_contents_unit" class="control-group">
            <label class="control-label">
                <span class="badge badge-important required">必須</span>
                <strong>単元</strong>
            </label>
            <div class="controls">
                <select name="unit" id="unit" class="input-small ldr-level">
                    <option value="1">I</option>
                    <option value="2">II</option>
                    <option value="3">III</option>
                    <option value="4">IV</option>
                    <option value="5">V</option>
                    <option value="6">VI</option>
                    <option value="7">VII</option>
                    <option value="8">VIII</option>
                    <option value="9">IX</option>
                    <option value="10">X</option>
                </select>
                <div id="error_contents_unit" class="help-block hide"></div>
            </div>
            <input type="hidden" name="old_unit" value=""  />
        </div>
        <div id="modal_contents_contents" class="control-group">
            <label class="control-label">
                <span class="badge badge-important required">必須</span>
                <strong>教科内容</strong>
            </label>
            <div class="controls">
                <input type="text" name="contents" value="" class="input-xlarge" maxlength="200" placeholder="教科内容を入力してください" />
                <div id="error_contents_contents" class="help-block hide"></div>
            </div>
        </div>
        <div id="modal_contents_detail" class="control-group">
            <label class="control-label"><strong>詳細</strong></label>
            <div class="controls">
                <textarea id="detail" name="detail" rows="5" class="input-xlarge" placeholder="詳細を入力してください"></textarea>
                <div id="error_contents_detail" class="help-block hide"></div>
            </div>
        </div>
        <div id="modal_contents_time" class="control-group">
            <label class="control-label">
                <span class="badge badge-important required">必須</span>
                <strong>時間</strong>
            </label>
            <div class="controls">
                <input type="text" name="time" value="" class="input-xlarge" maxlength="200" placeholder="時間を入力してください" />
                <div id="error_contents_time" class="help-block hide"></div>
            </div>
        </div>
    </div>
    <input type="hidden" name="id" value="" />
    <input type="hidden" name="mode" value="contents" />
    <input type="hidden" id="mode2_contents" name="mode2" value="validate" />
</form>
