<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | <{$titles.novice|escape}>: 本人評価</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title="`$titles.novice`: 本人評価" hd_sub_title=''}>

            <div class="container-fluid">
                <div class="row-fluid">
                    <form method="post" action="" id="form_assessment" class="form-inline">
                        <select id="pulldown_year" name="year" class="input-medium js-pulldown-assessment">
                            <{foreach item=year from=$year_list}>
                                <option value="<{$year|escape}>"><{$year|escape}>年度</option>
                            <{/foreach}>
                        </select>

                        <select id="pulldown_date" name="number" class="input-large js-pulldown-assessment">
                            <{foreach item=date from=$date_list}>
                                <option value="<{$date.number|escape}>">第<{$date.number|escape}>回: <{$date.last_date|date_format_jp:"%Y年%-m月%-d日"}></option>
                            <{/foreach}>
                        </select>
                        <{if $number > 1}>
                            <button type="submit" name="btn_save" value="copy" class="js-form-copy btn btn-small btn-primary"><i class="icon-copy"></i> 前回評価をコピーする</button>
                        <{/if}>
                        <button type="button" value="copy" class="js-novice-sheet btn btn-small btn-success pull-right" data-emp="<{$emp->emp_id()|escape}>"><i class="icon-file-text"></i> 評価シート</button>
                    </form>
                    <ul class="nav nav-tabs" id="group1Tab">
                        <li class="<{if $active === 'assessment'}>active<{/if}>"><a href="#assessment" data-toggle="tab"><i class="icon-bar-chart"></i> 統計</a></li>
                            <{foreach item=row from=$group1_list}>
                            <li class="<{if $active === "g1-`$row.group1`"}>active<{/if}>"><a href="#g1-<{$row.group1|escape}>" data-toggle="tab" data-url="novice_assessment_tab.php?group1=<{$row.group1|escape}><{if $g1 === "g1-`$row.group1`"}>&active=<{$g2}><{/if}>"><i class="icon-folder-close"></i> <{$row.name|escape}></a></li>
                            <{/foreach}>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane <{if $active === 'assessment'}>active<{else}>fade<{/if}>" id="assessment">

                            <div class="well well-small">
                                <div id="d_table">
                                </div>
                            </div>

                        </div>
                        <{foreach item=row from=$group1_list}>
                            <div class="tab-pane <{if $active === "g1-`$row.group1`"}>active<{else}>fade<{/if}>" id="g1-<{$row.group1|escape}>"></div>
                        <{/foreach}>
                    </div>
                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {
                $('#group1Tab a').on('shown', function(e) {
                    e.preventDefault();

                    var url = $(this).data("url");
                    if (!url) {
                        loadTable();
                        return;
                    }
                    url += '&number=' + $('#pulldown_date').val();
                    url += '&year=' + $('#pulldown_year').val();

                    // ajax load from data-url
                    $(this.hash).load(url, function() {
                        $(this).tab('show');

                        // 自動保存
                        $(this).find('form').autosave({
                            callbacks: {
                                trigger: 'change',
                                scope: 'all',
                                save: {
                                    method: 'ajax',
                                    options: {
                                        url: 'novice_assessment.php',
                                        method: 'post'
                                    }
                                }
                            }
                        });
                    });
                });

                $('#group1Tab li.active a').trigger('shown');

                $('body').on('click', 'button.js-radio-assessment', function() {
                    $(this).tooltip('destroy');

                    var input = $($(this).data('input'));
                    var alert = $($(this).data('alert'));
                    input.val($(this).attr('value')).change();

                    $(this).siblings().removeClass('btn-info');
                    $(this).addClass('btn-info');
                    alert.attr('class', 'alert alert-info');
                });

                $('body').on('submit', 'form.js-form-assessment', function() {
                    // validate
                    var error = false;
                    var selected = $('.btn-group button.btn.active').val();
                    $assessment = selected;
                    $(this).find('input.input-assessment').each(function(idx, input) {
                        var inp = $(input);
                        if (inp.val() === '') {
                            $(inp.data('pop')).tooltip('show');
                            error = true;

                        }
                    });
                    if (error) {
                        return false;
                    }
                });

                $('button.js-form-copy').click(function() {
                    if (confirm('前回の評価をコピーします。\n入力済みの評価が上書きされますがよろしいですか？')) {
                        return true;
                    }
                    return false;
                });

                // プルダウン選択
                $('.js-pulldown-assessment').change(function() {
                    $('#form_assessment').submit();
                });

                $('#assessment').on('shown', function(e) {
                    loadTable();
                });

                // 評価シート
                $('.js-novice-sheet').click(function() {
                    $('<form action="novice_sheet.php" method="post"></form>')
                        .append($('<input></input>').attr({type: 'hidden', name: 'emp_id', value: $(this).data('emp')}))
                        .append($('<input></input>').attr({type: 'hidden', name: 'year', value: $('#pulldown_year').val()}))
                        .append($('<input></input>').attr({type: 'hidden', name: 'menu', value: 'novice_assessment'}))
                        .append($('<input></input>').attr({type: 'hidden', name: 'owner_url', value: 'novice_assessment.php'}))
                        .appendTo(document.body)
                        .submit();
                });
            });

            /**
             * 集計
             * @returns {undefined}
             */
            function loadTable() {
                var url = 'novice_assessment_table.php';
                url += '?year=' + $('#pulldown_year').val();

                $('#d_table').load(url);
            }
        </script>
    </body>
</html>
