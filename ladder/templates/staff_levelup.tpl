<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | レベルアップ一覧</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='レベルアップ一覧' hd_sub_title=''}>

            <div class="container-fluid">
                <div class="row-fluid">
                    <form method="POST">
                        <div class="control-group form-inline">
                            <select id="year" name="year" class="input-medium" style="margin-bottom:0;" data-emp="<{$emp->emp_id()|escape}>" data-menu="<{$smarty.post.menu|escape}>" data-url="<{$smarty.post.owner_url|escape}>">
                                <{foreach item=y from=$year_list}>
                                    <option value="<{$y|escape}>" <{if $y === $year}>selected<{/if}>><{$y|escape}>年度</option>
                                <{/foreach}>
                            </select>
                            <select id="ladder_id" name="ladder_id" class="input-xlarge" style="margin-bottom:0;" data-emp="<{$emp->emp_id()|escape}>" data-menu="<{$smarty.post.menu|escape}>" data-url="<{$smarty.post.owner_url|escape}>">
                                <{foreach item=ldr from=$ladder->lists()}>
                                    <option value="<{$ldr.ladder_id|escape}>"><{$ldr.title|escape}></option>
                                <{/foreach}>
                            </select>
                            <div class="input-prepend">
                                <span class="add-on"><i class="icon-sitemap"></i> 部署</span>
                                <{if count($class_lists) > 1}>
                                    <select name="srh_class3" class="input-large" id="class3">
                                        <{if $full}><option value="">すべて</option><{/if}>
                                        <{foreach item=row from=$class_lists}>
                                            <option class="js-class-select select<{$row.class_id|escape}>_<{$row.atrb_id|escape}>" value="<{$row.dept_id|escape}><{if $row.room_id}>_<{$row.room_id|escape}><{/if}>">
                                                <{$row.dept_nm|escape}><{if $row.room_id}> &gt; <{$row.room_nm|escape}><{/if}>
                                            </option>
                                        <{/foreach}>
                                    </select>
                                <{else}>
                                    <input type="hidden" name="class[]" value="<{$class_lists[0].dept_id|escape}>"/><span class="uneditable-input"><{$class_lists[0].name|escape}></span>
                                <{/if}>
                            </div>
                            <button type="submit" id="btn_search" name="btn_search" value="search" class="btn btn-small btn-primary"><i class="icon-search"></i> 検索する</button>
                            <{if $srh_class3 || count($class_lists) === 1}>
                                <div class="pull-right">
                                    &nbsp;<button id="btn-print" type="button" class="js-staff-levelup-print btn btn-small btn-primary"><i class="icon-print"></i> 印刷する</button>
                                </div>
                            <{/if}>
                        </div>
                    </form>

                    <form action="#" method="post" id="emp_form">
                        <input type="hidden" id="menu" name="menu" value="accept" />
                        <input type="hidden" id="owner_url" name="owner_url" value="staff_levelup.php" />

                        <div class="well well-small" id="form_search">
                            <{include file="__pager.tpl"}>
                            <table class="table table-striped table-hover table-condensed table-bordered ldr-table">
                                <thead>
                                    <tr>
                                        <th>部署</th>
                                        <th>職員</th>
                                        <th><a href="?order=st_order_no&desc=<{if $order === "st_order_no" && $desc === '1'}>0<{else}>1<{/if}>">役職</a></th>
                                        <th><a href="?order=nurse_date&desc=<{if $order === "nurse_date" && $desc === '1'}>0<{else}>1<{/if}>">看護師経験年数</a></th>

                                        <th><a href="?order=ldr<{$ladder_id|escape}>.level&desc=<{if $order === "ldr`$ladder_id`.level" && $desc === '1'}>0<{else}>1<{/if}>">現在のレベル</a></th>
                                        <th>申請日</th>
                                        <th><a href="?order=apply_level&desc=<{if $order === "apply_level" && $desc === '1'}>0<{else}>1<{/if}>">申請レベル</a></th>
                                        <th>結果</th>
                                        <th>認定日</th>
                                        <th>認定番号</th>
                                        <th>申請内容</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <{foreach item=row from=$list}>
                                        <tr>
                                            <td class="ldr-td-name">
                                                <{$row->class_full_name()|escape|replace:' &gt; ':'<br/>'}>
                                            </td>
                                            <td class="ldr-td-name"><b title="<{$row->emp_personal_id()|escape}>"><{$row->emp_name()|escape}></b></td>
                                            <td class="ldr-td-name"><{$row->st_name()|escape}></td>
                                            <td class="ldr-td-name"><{if $row->nurse_age()}><{$row->nurse_age()|escape}>年目<{/if}></td>

                                            <td class="ldr-td-level">
                                                <{if $row->level($ladder_id) === '0'}>
                                                    <{$ladder->level0($ladder_id)|escape}>
                                                <{else}>
                                                    <{$row->level_roman($ladder_id)|escape}>
                                                <{/if}>
                                            </td>
                                            <td class="ldr-td-date">
                                                <{$row->_getter('apply_date')|date_format_jp:"%Y年%-m月%-d日"}>
                                            </td>
                                            <td class="ldr-td-level">
                                                <{if $row->apply_level() === '0'}>
                                                    <{$ladder->level0($ladder_id)|escape}>
                                                <{else}>
                                                    <{$row->apply_level_roman()|escape}>
                                                <{/if}>
                                            </td>
                                            <td class="ldr-td-name">
                                                <{if $row->_getter('status') === '0'}>
                                                    <{if $row->_getter('result') === 't'}>
                                                        <i class="icon-circle-blank"></i>
                                                    <{elseif $row->_getter('result') === 'f'}>
                                                        <i class="icon-remove"></i>
                                                    <{/if}>
                                                <{elseif $row->_getter('status') === '12'}>
                                                    <{if $ldr.config.flow === '1' && $row->_getter('f8_result') === '1' || $ldr.config.flow !== '1' && $row->_getter('f7_result') === '1'}>
                                                        <i class="icon-circle-blank"></i><br/>(総合評価)
                                                    <{else}>
                                                        <i class="icon-remove"></i><br/>(総合評価)
                                                    <{/if}>
                                                <{/if}>
                                            </td>
                                            <td class="ldr-td-date">
                                                <{if $row->_getter('apply_level')}>
                                                    <{$row->lv_date($ladder_id, $row->_getter('apply_level'))|date_format_jp:"%Y年%-m月%-d日"}>
                                                <{/if}>
                                            </td>
                                            <td class="ldr-td-name">
                                                <{if $row->_getter('apply_level')}>
                                                    <{$row->lv_number($ladder_id, $row->_getter('apply_level'))}>
                                                <{/if}>
                                            </td>
                                            <td class="ldr-td-button">
                                                <{if $row->_getter('ldr_apply_id')}>
                                                    <button type="button" data-id="<{$row->_getter('ldr_apply_id')}>" data-emp="<{$row->emp_id()|escape}>" data-php="form_1view.php" data-mode="view" class="js-form-button btn btn-info btn-small">申請を見る</button>
                                                <{/if}>
                                            </td>
                                        </tr>
                                    <{/foreach}>
                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>
            </div>

        </div>
        <{include file="__js.tpl"}>
        <{include form_selector="#emp_form" file="__pager_js.tpl"}>
        <script type="text/javascript">
            $(function() {
                /**
                 * 印刷
                 */
                $('.js-staff-levelup-print').click(function() {
                    $('<form action="" method="post" target="sheet" onsubmit="return openPDF(this);"></form>')
                        .append($('<input></input>').attr({type: 'hidden', name: 'mode', value: 'print'}))
                        .appendTo(document.body)
                        .submit()
                        .remove();
                });
            });
        </script>
    </body>
</html>