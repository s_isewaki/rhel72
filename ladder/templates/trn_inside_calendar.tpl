<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 院内研修 カレンダー</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='院内研修 カレンダー' hd_sub_title=''}>

            <div class="container-fluid">

                <div class="form-inline" style="margin-bottom: 10px;">
                    <a href="trn_inside_course.php" class="btn btn-small btn-primary"><i class="icon-list"></i> 研修講座</a>

                    <div class="pull-right">
                        <label class="control-label">
                            <{if $hospital_type === '1'}>
                                <strong>めざすレベル</strong>
                            <{else}>
                                <strong>対象レベル</strong>
                            <{/if}>
                        </label>
                        <span class="badge badge-info ldr-roman">I</span>
                        <span class="badge badge-inverse ldr-roman">II</span>
                        <span class="badge badge-success ldr-roman">III</span>
                        <span class="badge badge-warning ldr-roman">IV</span>
                        <span class="badge badge-important ldr-roman">V</span>
                    </div>

                </div>

                <div id='calendar'></div>
            </div>
        </div>


        <{include file="__js.tpl"}>
        <script type="text/javascript">
            <{include file="calendar/__trn_inside.tpl"}>
        </script>
    </body>
</html>
