<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | <{$ldr.config.headmenu|escape}></title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title=$ldr.config.headmenu hd_sub_title=$ldr.config.headsubmenu}>

            <div class="container-fluid">
                <div class="row-fluid">

                    <form class="form" id="edit_form" method="POST">
                        <{include file="__form_view.tpl" op_srh='false' tmp_id="apply_entry"}>

                        <div class="well well-small">
                            <input type="hidden" id="menu" name="menu" value="apply" />
                            <input type="hidden" name="ladder_id" value="" />
                            <input type="hidden" name="ldr_apply_id" value="" />

                            <div class="control-group">
                                <label class="control-label"><strong>既得しているレベル</strong></label>
                                <{if $emp->level($ldr.ladder_id) !== ''}>
                                    <div class="well well-small">
                                        <dl class="dl-horizontal">
                                            <dt>レベル </dt>
                                            <dd><strong class="ldr-level"><{$emp->level_roman($ldr.ladder_id)|default:$ldr.config.level0}></strong></dd>
                                            <dt>認定日</dt>
                                            <dd><{$emp->lv_date($ldr.ladder_id)|date_format_jp:"%Y年%-m月%-d日(%a)"}></dd>
                                            <dt>取得施設</dt>
                                            <dd><{$emp->lv_hospital($ldr.ladder_id)|escape}></dd>
                                        </dl>
                                    </div>
                                <{else}>
                                    <div class="well well-small">
                                        <strong>なし</strong>
                                    </div>
                                <{/if}>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="apply_date">
                                    <strong>申請年月日</strong>
                                    <span class="badge badge-important required">必須</span>
                                </label>
                                <{include file="__datepicker.tpl" date_type="date" date_id="apply_date" date_name="apply_date" date_remove="off"}>

                                <{foreach from=$error.apply_date item=msg}>
                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                <{/foreach}>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="level">
                                    <strong>申請するレベル</strong>
                                    <span class="badge badge-important required">必須</span>
                                </label>
                                <div class="controls">
                                    <{foreach from=$ldr.config.level item=level}>
                                        <label class="radio inline ldr-level"><input type="radio" name="level" class="level" value="<{$level|escape}>" /><{$roman_num[$level]|default:$ldr.config.level0}></label>
                                    <{/foreach}>
                                </div>
                                <{foreach from=$error.level item=msg}>
                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                <{/foreach}>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="career">
                                    <strong>臨床経験年数</strong>
                                    <span class="badge badge-important required">必須</span>
                                </label>
                                <div class="controls">
                                    <div class="input-append">
                                        <input type="text" id="career" name="career" class="input-mini onlynum text-right" maxlength="2"/>
                                        <span class="add-on">年目</span>
                                    </div>
                                </div>
                                <{foreach from=$error.career item=msg}>
                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                <{/foreach}>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="career_hospital">
                                    <strong>当院での臨床経験年数</strong>
                                    <span class="badge badge-important required">必須</span>
                                </label>
                                <div class="controls">
                                    <div class="input-append">
                                        <input type="text" id="career_hospital" name="career_hospital" class="input-mini onlynum text-right" maxlength="2"/>
                                        <span class="add-on">年目</span>
                                    </div>
                                </div>
                                <{foreach from=$error.career_hospital item=msg}>
                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                <{/foreach}>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="career_ward">
                                    <strong>当該部署での臨床経験年数</strong>
                                    <span class="badge badge-important required">必須</span>
                                </label>
                                <div class="controls">
                                    <div class="input-append">
                                        <input type="text" id="career_ward" name="career_ward" class="input-mini onlynum text-right" maxlength="2" />
                                        <span class="add-on">年目</span>
                                    </div>
                                </div>
                                <{foreach from=$error.career_ward item=msg}>
                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                <{/foreach}>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="objective">
                                    <strong>当面のあなたの目標</strong>
                                    <span class="badge badge-important required">必須</span>
                                </label>
                                <div class="controls">
                                    <textarea id="objective" name="objective" class="input-xxlarge js-autoheight" placeholder="あなたの目標を入力してください"></textarea>
                                </div>
                                <{foreach from=$error.objective item=msg}>
                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                <{/foreach}>
                            </div>

                            <{if ($ldr.config.flow === '1' && $ldr.config.meeting === '1') || $ldr.config.appr === '1'}>
                                <hr/>
                            <{/if}>

                            <{if $ldr.config.flow === '1' && $ldr.config.meeting === '1'}>
                                <div class="control-group">
                                    <label class="control-label" for="" >
                                        <strong>評価会日時</strong>
                                        <{if $apply->get_status() >= '4'}>
                                            <span class="badge badge-important required">必須</span>
                                        <{/if}>
                                    </label>
                                    <div class="controls">
                                        <{include file="__datepicker.tpl" date_type="date" date_id="meeting_date" date_name="meeting_date" date_remove="on" date_value=$form_data.meeting_date}>

                                        <{include file="__datepicker.tpl" date_type="time" date_id="meeting_start_time" date_name="meeting_start_time" date_remove="on" date_value=$form_data.meeting_date}>
                                        <span>&nbsp;&nbsp;〜&nbsp;&nbsp;</span>
                                        <{include file="__datepicker.tpl" date_type="time" date_id="meeting_last_time" date_name="meeting_last_time" date_remove="on" date_value=$form_data.meeting_date}>
                                    </div>
                                    <{foreach from=$error.meeting_date item=msg}>
                                        <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                    <{/foreach}>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="meeting_room">
                                        <strong>評価会会場</strong>
                                        <{if $apply->get_status() >= '4'}>
                                            <span class="badge badge-important required">必須</span>
                                        <{/if}>
                                    </label>
                                    <div class="controls">
                                        <input type="text" id="meeting_room" name="meeting_room" class="input-xlarge" maxlength="200" placeholder="評価会会場を入力してください" />
                                    </div>
                                    <{foreach from=$error.meeting_room item=msg}>
                                        <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                    <{/foreach}>
                                </div>
                                <hr/>
                            <{else}>
                                <input type="hidden" name="meeting_date" value ="" />
                                <input type="hidden" name="meeting_start_time" value ="" />
                                <input type="hidden" name="meeting_last_time" value ="" />
                                <input type="hidden" name="meeting_room" value ="" />
                            <{/if}>

                            <{if $ldr.config.appr_max[$level] > 0}>
                                <{if $ldr.config.appr === '1'}>
                                    <div class="control-group">
                                        <label class="control-label">
                                            <strong>評価者</strong>
                                            <{if $apply->get_status() >= '4'}>
                                                <span class="badge badge-important required">必須</span>
                                            <{/if}>
                                        </label>

                                        <{if $ldr.config.flow === '1'}>
                                            <div>
                                                <button type="button" class="btn btn-small btn-success js-btn-emplist" data-id="facilitator"><i class="icon-book"></i> 司会者 を選択してください</button>
                                            </div>
                                            <div class="controls clearfix">
                                                <div id="facilitator" class="ldr-empbox <{if !$facilitator}>hide<{/if}>">
                                                    <{foreach from=$facilitator key=id item=name}>
                                                        <div class="label label-info"><{$name|escape}>(司会者)<input type="hidden" name="facilitator[<{$id|escape}>]" value="<{$name|escape}>" /> <a href="javascript:void(0);" class="ldr-emp-remove js-emp-remove">&times;</a></div>
                                                    <{/foreach}>
                                                </div>
                                            </div>
                                            <{foreach from=$error.facilitator item=msg}>
                                                <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                            <{/foreach}>
                                            <div class="btn-toolbar">
                                            </div>
                                        <{/if}>

                                        <div>
                                            <button type="button" class="btn btn-small btn-success js-btn-emplist" data-id="appraiser"><i class="icon-book"></i> <{$ldr.config.appr_name|escape}>者 を選択してください</button>
                                        </div>
                                        <div class="controls clearfix">
                                            <div id="appraiser" class="ldr-empbox <{if !$appraiser}>hide<{/if}>">
                                                <{foreach from=$appraiser key=id item=name}>
                                                    <div class="label label-info"><{$name|escape}><input type="hidden" name="appraiser[<{$id|escape}>]" value="<{$name|escape}>" /> <a href="javascript:void(0);" class="ldr-emp-remove js-emp-remove">&times;</a></div>
                                                <{/foreach}>
                                            </div>
                                        </div>
                                        <{foreach from=$error.appraiser item=msg}>
                                            <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                        <{/foreach}>
                                    </div>
                                <{/if}>
                            <{/if}>
                        </div>
                        <div class="btn-toolbar">
                            <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                            <button type="submit" name="btn_save" id="btn_save" class="btn btn-small btn-primary" value="btn_save"><i class="icon-save"></i> 保存する</button>
                        </div>
                </div>
                </form>
            </div>
        </div>
        </div>
        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {

                $(document).on("click", ".eva_icon", function() {
                    id = $(this).attr("id").replace('i_', '');
                    open_emplist(id);
                });

                // 評価会開始時間を終了時間にコピー
                if ($('#meeting_start_time').get(0)) {
                    $('#meeting_start_time').parent(".timepicker").on('changeDate', function(e) {
                        if ($('#meeting_start_time').val() > $('#meeting_last_time').val()) {
                            $('#meeting_last_time').val($('#meeting_start_time').val());
                            $('#meeting_last_time').change();
                        }
                    });
                }

                // サブミットイベント
                $("#fm1").submit(function() {
                    var dt = new Date();
                    var y = dt.getFullYear();
                    var m = dt.getMonth() + 1;
                    var d = dt.getDate();
                    var tmp1 = new Date(y + "/" + m + "/" + d + " " + $('#meeting_start_time').val());
                    var tmp2 = new Date(y + "/" + m + "/" + d + " " + $('#meeting_last_time').val());
                    if (tmp1 >= tmp2) {
                        alert("開始時間 ＜ 終了時間 で設定してください。");
                        return false;
                    }

                    $('#fm1').submit();
                });

                // 職員削除
                $('body').on('click', '.js-emp-remove', function() {
                    $(this).parent().remove();
                });

                // 評価日は申請日の15日以降
                $('#apply_date').parent(".datepicker").on('changeDate', function(e) {
                    var startDate = e.localDate ? new Date(e.localDate.getTime()) : new Date($('#apply_date').val().replace('-', '/'));
                    startDate.setMinutes(startDate.getMinutes() - startDate.getTimezoneOffset());
                    startDate.setDate(startDate.getDate() + <{$ldr.config.appr_interval|default:15}>);
                    if ($('#meeting_date').get(0)) {
                        $('#meeting_date').parent(".datepicker").data('datetimepicker').setStartDate(startDate);
                    }
                });
                $('#apply_date').parent(".datepicker").trigger('changeDate');

                $('body').on('click', 'button.js-button-assessment', function() {
                    $(this).tooltip('destroy');

                    var input = $($(this).data('input'));
                    var alert = $($(this).data('alert'));
                    input.val($(this).attr('value')).change();
                });

            });

            function closeEmployeeList() {
                if (childwin !== null && !childwin.closed) {
                    childwin.close();
                }
                childwin = null;
            }

            // 職員の追加
            function add_target_list(item_id, emp_id, emp_name) {
                var emp = emp_id.split(", ");
                if (emp.length !== 1) {
                    return;
                }
                if ($('#e' + emp_id).size() === 0) {
                    if (String(emp_id) === '<{$emp->emp_id()}>') {
                        alert("自分を評価者には設定できません");
                        return false;
                    }

                    if (item_id === 'facilitator') {
                        $('#facilitator').html('<div id="e' + emp_id + '" class="label label-info">' + emp_name + '(司会者) <input type="hidden" name="facilitator[' + emp_id + ']" value="' + emp_name + '" /> <a href="javascript:void(0);" class="ldr-emp-remove js-emp-remove">&times;</a></div>').show();
                    }
                    else {
                        var add = '<div id="e' + emp_id + '" class="label label-info">' + emp_name + '<input type="hidden" name="appraiser[' + emp_id + ']" value="' + emp_name + '" /> <a href="javascript:void(0);" class="ldr-emp-remove js-emp-remove">&times;</a></div> ';
                        $('#appraiser').append(add).show();
                    }
                }
            }
        </script>
    </body>
</html>
