<script type="text/javascript">
    $(function() {

        /**
         * 並べ替え
         */
        $("#form").sortable({
            axis: 'y',
            cursor: 'move'
        });

        /**
         * コピー
         */
        $(document).on('click', '.js-tmpl-copy', function() {
            var $parent = $(this).parent();
            var $clone = $(create_formhtml(random_str(), $(this).data('type')));

            // 質問文のコピー
            var $p1 = $parent.find('.tmpl-name');
            $clone.find('.tmpl-name').val($p1.val() + " 〜 コピー");

            // フォーム幅のコピー
            var $p2 = $parent.find('.tmpl-size');
            if ($p2.length) {
                $clone.find('.tmpl-size').val($p2.val());
            }

            // 高さのコピー
            var $p3 = $parent.find('.tmpl-rows');
            if ($p3.length) {
                $clone.find('.tmpl-rows').val($p3.val());
            }

            // 選択肢のコピー
            var $p4 = $parent.find('.tmpl-select');
            if ($p4.length) {
                $clone.find('.tmpl-select').val($p4.val());
            }

            // 必須のコピー
            if ($parent.find('.tmpl-required').prop('checked')) {
                $clone.find('.tmpl-required').prop('checked', true);
            }

            // 挿入
            $clone.insertAfter($parent);
            //$('#form').append($clone);
        });

        /**
         * 削除
         */
        $(document).on('click', '.js-tmpl-remove', function() {
            // フェードアウトして消える
            $(this).parent().fadeOut(500).queue(function() {
                $(this).remove();
            });
        });

        /**
         * テキスト変更
         */
        $(document).on('change', '.js-tmpl-type-change', function() {
            if ($(this).val() === 'text') {
                $($(this).data('id')).hide();
            }
            else {
                $($(this).data('id')).show();
            }
        });

        /**
         * テキストフォーム作成
         */
        $('#js-add-text').click(function() {
            $('#form').append(create_formhtml(random_str(), 'text'));
        });

        /**
         * 段落テキストフォーム作成
         */
        $('#js-add-textarea').click(function() {
            $('#form').append(create_formhtml(random_str(), 'textarea'));
        });

        /**
         * ラジオボタンフォーム作成
         */
        $('#js-add-radio').click(function() {
            $('#form').append(create_formhtml(random_str(), 'radio'));
        });

        /**
         * チェックボックスフォーム作成
         */
        $('#js-add-checkbox').click(function() {
            $('#form').append(create_formhtml(random_str(), 'checkbox'));
        });

        /**
         * プルダウン作成フォーム作成
         */
        $('#js-add-select').click(function() {
            $('#form').append(create_formhtml(random_str(), 'select'));
        });
    });

    /**
     * フォーム作成
     * @param {type} id
     * @param {type} type
     * @returns {undefined}
     */
    function create_formhtml(id, type) {
        var $form = '<div class="form-horizontal well well-small">';

        // フォーム種類
        $form += '<div class="control-group">';
        switch (type) {
            case 'text':
                $form += '<select name="form[' + id + '][type]" data-id="#rows-' + id + '" class="tmpl-size input-large js-tmpl-type-change">';
                $form += '<option value="text" selected="selected">文字入力</option>';
                $form += '<option value="textarea">複数行入力</option>';
                $form += '</select>';
                break;
            case 'textarea':
                $form += '<select name="form[' + id + '][type]" data-id="#rows-' + id + '" class="tmpl-size input-large js-tmpl-type-change">';
                $form += '<option value="text">文字入力</option>';
                $form += '<option value="textarea" selected="selected">複数行入力</option>';
                $form += '</select>';
                break;
            case 'radio':
                $form += '<select name="form[' + id + '][type]" class="tmpl-size input-large">';
                $form += '<option value="radio" selected="selected">ラジオボタン(1つ選択させる)</option>';
                $form += '<option value="checkbox">チェックボックス(複数選択させる)</option>';
                $form += '<option value="select">プルダウン選択(1つ選択させる)</option>';
                $form += '</select>';
                break;
            case 'checkbox':
                $form += '<select name="form[' + id + '][type]" class="tmpl-size input-large">';
                $form += '<option value="radio">ラジオボタン(1つ選択させる)</option>';
                $form += '<option value="checkbox" selected="selected">チェックボックス(複数選択させる)</option>';
                $form += '<option value="select">プルダウン選択(1つ選択させる)</option>';
                $form += '</select>';
                break;
            case 'select':
                $form += '<select name="form[' + id + '][type]" class="tmpl-size input-large">';
                $form += '<option value="radio">ラジオボタン(1つ選択させる)</option>';
                $form += '<option value="checkbox">チェックボックス(複数選択させる)</option>';
                $form += '<option value="select" selected="selected">プルダウン選択(1つ選択させる)</option>';
                $form += '</select>';
                break;
        }
        $form += '</div>';

        // 質問文
        $form += '<div class="control-group">';
        $form += '<label class="control-label">質問</label>';
        $form += '<div class="controls">';
        $form += '<input type="text" name="form[' + id + '][name]" class="tmpl-name input-xxlarge" placeholder="質問文を入力してください" />';
        $form += '</div>';
        $form += '</div>';

        // 必須
        $form += '<div class="control-group">';
        $form += '<label class="control-label">必須</label>';
        $form += '<div class="controls">';
        $form += '<label class="checkbox"><input type="checkbox" class="tmpl-required" name="form[' + id + '][required]" value="1" /> 必須である</label>';
        $form += '</div>';
        $form += '</div>';

        if (type === 'text' || type === 'textarea') {

            // 入力幅
            $form += '<div class="control-group">';
            $form += '<label class="control-label">入力幅</label>';
            $form += '<div class="controls">';
            $form += '<select name="form[' + id + '][size]" class="tmpl-size input-medium">';
            $form += '<option value="mini">1 (4字程度)</option>';
            $form += '<option value="small">2 (6字程度)</option>';
            $form += '<option value="medium">3 (12字程度)</option>';
            $form += '<option value="large">4 (16字程度)</option>';
            $form += '<option value="xlarge">5 (21字程度)</option>';
            $form += '<option value="xxlarge">6 (42字程度)</option>';
            $form += '</select>';
            $form += '</div>';
            $form += '</div>';

            // 入力行
            if (type === 'text') {
                $form += '<div class="control-group hide" id="rows-' + id + '">';
            }
            else {
                $form += '<div class="control-group" id="rows-' + id + '">';
            }
            $form += '<label class="control-label">入力行</label>';
            $form += '<div class="controls">';
            $form += '<input type="number" name="form[' + id + '][rows]" value="3" class="tmpl-rows input-mini" /> 行';
            $form += '</div>';
            $form += '</div>';
        }

        // 選択肢
        if (type === 'radio' || type === 'checkbox' || type === 'select') {
            $form += '<div class="control-group">';
            $form += '<label class="control-label">選択肢</label>';
            $form += '<div class="controls">';
            $form += '<textarea name="form[' + id + '][select]" class="tmpl-select input-xlarge" rows="10"></textarea>';
            $form += '</div>';
            $form += '</div>';
        }

        $form += '<button type="button" data-type="' + type + '" class="js-tmpl-copy btn btn-mini"><i class="icon-copy"></i> コピー</button>';
        $form += '&nbsp;<button type="button" class="js-tmpl-remove btn btn-mini"><i class="icon-trash"></i> 削除</button>';
        $form += '</div>';

        return $form;
    }

</script>