<div id="form">
    <{foreach from=$form key=form_id item=row}>
        <{if $row.type === 'text'}>
            <div class="form-horizontal well well-small">
                <div class="control-group">
                    <select name="form[<{$form_id|escape}>][type]" data-id="#rows-<{$form_id|escape}>" class="tmpl-size input-large js-tmpl-type-change">
                        <option value="text" selected="selected">文字入力</option>
                        <option value="textarea">複数行入力</option>
                    </select>
                </div>

                <div class="control-group">
                    <label class="control-label">質問</label>
                    <div class="controls">
                        <input type="text" name="form[<{$form_id|escape}>][name]" class="tmpl-name input-xxlarge" placeholder="質問文を入力してください" value="<{$row.name|escape}>" />
                        <{foreach from=$error[$form_id].name item=msg}>
                            <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                        <{/foreach}>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">必須</label>
                    <div class="controls">
                        <label class="checkbox"><input type="checkbox" class="tmpl-required" name="form[<{$form_id|escape}>][required]" value="1" <{if $row.required === '1'}>checked<{/if}> /> 必須である</label>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">入力幅</label>
                    <div class="controls">
                        <select name="form[<{$form_id|escape}>][size]" class="tmpl-size input-medium">
                            <option <{if $row.size === 'mini'}>selected<{/if}> value="mini">1 (4字程度)</option>
                            <option <{if $row.size === 'small'}>selected<{/if}> value="small">2 (6字程度)</option>
                            <option <{if $row.size === 'medium'}>selected<{/if}> value="medium">3 (12字程度)</option>
                            <option <{if $row.size === 'large'}>selected<{/if}> value="large">4 (16字程度)</option>
                            <option <{if $row.size === 'xlarge'}>selected<{/if}> value="xlarge">5 (21字程度)</option>
                            <option <{if $row.size === 'xxlarge'}>selected<{/if}> value="xxlarge">6 (42字程度)</option>
                        </select>
                        <{foreach from=$error[$form_id].size item=msg}>
                            <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                        <{/foreach}>
                    </div>
                </div>
                <div class="control-group hide" id="rows-<{$form_id|escape}>">
                    <label class="control-label">入力行</label>
                    <div class="controls">
                        <input type="number" name="form[<{$form_id|escape}>][rows]" value="<{$row.rows|default:'3'|escape}>" class="tmpl-rows input-mini" /> 行
                        <{foreach from=$error[$form_id].rows item=msg}>
                            <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                        <{/foreach}>
                    </div>
                </div>
                <button type="button" data-type="text" class="js-tmpl-copy btn btn-mini"><i class="icon-copy"></i> コピー</button>
                <button type="button" class="js-tmpl-remove btn btn-mini"><i class="icon-trash"></i> 削除</button>
            </div>
        <{/if}>

        <{if $row.type === 'textarea'}>
            <div class="form-horizontal well well-small ldr-check-tmpl">
                <div class="control-group">
                    <select name="form[<{$form_id|escape}>][type]" data-id="#rows-<{$form_id|escape}>" class="tmpl-size input-large js-tmpl-type-change">
                        <option value="text">文字入力</option>
                        <option value="textarea" selected="selected">複数行入力</option>
                    </select>
                </div>

                <div class="control-group">
                    <label class="control-label">質問</label>
                    <div class="controls">
                        <input type="text" name="form[<{$form_id|escape}>][name]" class="tmpl-name input-xxlarge" placeholder="質問文を入力してください" value="<{$row.name|escape}>" />
                        <{foreach from=$error[$form_id].name item=msg}>
                            <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                        <{/foreach}>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">必須</label>
                    <div class="controls">
                        <label class="checkbox"><input type="checkbox" class="tmpl-required" name="form[<{$form_id|escape}>][required]" value="1" <{if $row.required === '1'}>checked<{/if}> /> 必須である</label>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">入力幅</label>
                    <div class="controls">
                        <select name="form[<{$form_id|escape}>][size]" class="tmpl-size input-medium">
                            <option <{if $row.size === 'mini'}>selected<{/if}> value="mini">1 (4字程度)</option>
                            <option <{if $row.size === 'small'}>selected<{/if}> value="small">2 (6字程度)</option>
                            <option <{if $row.size === 'medium'}>selected<{/if}> value="medium">3 (12字程度)</option>
                            <option <{if $row.size === 'large'}>selected<{/if}> value="large">4 (16字程度)</option>
                            <option <{if $row.size === 'xlarge'}>selected<{/if}> value="xlarge">5 (21字程度)</option>
                            <option <{if $row.size === 'xxlarge'}>selected<{/if}> value="xxlarge">6 (42字程度)</option>
                        </select>
                        <{foreach from=$error[$form_id].size item=msg}>
                            <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                        <{/foreach}>
                    </div>
                </div>
                <div class="control-group" id="rows-<{$form_id|escape}>">
                    <label class="control-label">入力行</label>
                    <div class="controls">
                        <input type="number" name="form[<{$form_id|escape}>][rows]" value="<{$row.rows|escape}>" class="tmpl-rows input-mini" /> 行
                        <{foreach from=$error[$form_id].rows item=msg}>
                            <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                        <{/foreach}>
                    </div>
                </div>
                <button type="button" data-type="textarea" class="js-tmpl-copy btn btn-mini"><i class="icon-copy"></i> コピー</button>
                <button type="button" class="js-tmpl-remove btn btn-mini"><i class="icon-trash"></i> 削除</button>
            </div>
        <{/if}>

        <{if $row.type === 'radio'}>
            <div class="form-horizontal well well-small ldr-check-tmpl">
                <div class="control-group">
                    <select name="form[<{$form_id|escape}>][type]" class="tmpl-size input-large">
                        <option value="radio" selected="selected">ラジオボタン(1つ選択させる)</option>
                        <option value="checkbox">チェックボックス(複数選択させる)</option>
                        <option value="select">プルダウン選択(1つ選択させる)</option>
                    </select>
                </div>

                <div class="control-group">
                    <label class="control-label">質問</label>
                    <div class="controls">
                        <input type="text" name="form[<{$form_id|escape}>][name]" class="tmpl-name input-xxlarge" placeholder="質問文を入力してください" value="<{$row.name|escape}>" />
                        <{foreach from=$error[$form_id].name item=msg}>
                            <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                        <{/foreach}>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">必須</label>
                    <div class="controls">
                        <label class="checkbox"><input type="checkbox" class="tmpl-required" name="form[<{$form_id|escape}>][required]" value="1" <{if $row.required === '1'}>checked<{/if}> /> 必須である</label>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">選択肢</label>
                    <div class="controls">
                        <textarea name="form[<{$form_id|escape}>][select]" class="tmpl-select input-xlarge" rows="10"><{$row.select|escape}></textarea>
                        <{foreach from=$error[$form_id].select item=msg}>
                            <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                        <{/foreach}>
                    </div>
                </div>
                <button type="button" data-type="radio" class="js-tmpl-copy btn btn-mini"><i class="icon-copy"></i> コピー</button>
                <button type="button" class="js-tmpl-remove btn btn-mini"><i class="icon-trash"></i> 削除</button>
            </div>
        <{/if}>

        <{if $row.type === 'checkbox'}>
            <div class="form-horizontal well well-small ldr-check-tmpl">
                <div class="control-group">
                    <select name="form[<{$form_id|escape}>][type]" class="tmpl-size input-large">
                        <option value="radio">ラジオボタン(1つ選択させる)</option>
                        <option value="checkbox" selected="selected">チェックボックス(複数選択させる)</option>
                        <option value="select">プルダウン選択(1つ選択させる)</option>
                    </select>
                </div>

                <div class="control-group">
                    <label class="control-label">質問</label>
                    <div class="controls">
                        <input type="text" name="form[<{$form_id|escape}>][name]" class="tmpl-name input-xxlarge" placeholder="質問文を入力してください" value="<{$row.name|escape}>" />
                        <{foreach from=$error[$form_id].name item=msg}>
                            <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                        <{/foreach}>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">必須</label>
                    <div class="controls">
                        <label class="checkbox"><input type="checkbox" class="tmpl-required" name="form[<{$form_id|escape}>][required]" value="1" <{if $row.required === '1'}>checked<{/if}> /> 必須である</label>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">選択肢</label>
                    <div class="controls">
                        <textarea name="form[<{$form_id|escape}>][select]" class="tmpl-select input-xlarge" rows="10"><{$row.select|escape}></textarea>
                        <{foreach from=$error[$form_id].select item=msg}>
                            <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                        <{/foreach}>
                    </div>
                </div>
                <button type="button" data-type="checkbox" class="js-tmpl-copy btn btn-mini"><i class="icon-copy"></i> コピー</button>
                <button type="button" class="js-tmpl-remove btn btn-mini"><i class="icon-trash"></i> 削除</button>
            </div>
        <{/if}>

        <{if $row.type === 'select'}>
            <div class="form-horizontal well well-small ldr-check-tmpl">
                <div class="control-group">
                    <select name="form[<{$form_id|escape}>][type]" class="tmpl-size input-large">
                        <option value="radio">ラジオボタン(1つ選択させる)</option>
                        <option value="checkbox">チェックボックス(複数選択させる)</option>
                        <option value="select" selected="selected">プルダウン選択(1つ選択させる)</option>
                    </select>
                </div>

                <div class="control-group">
                    <label class="control-label">質問</label>
                    <div class="controls">
                        <input type="text" name="form[<{$form_id|escape}>][name]" class="tmpl-name input-xxlarge" placeholder="質問文を入力してください" value="<{$row.name|escape}>" />
                        <{foreach from=$error[$form_id].name item=msg}>
                            <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                        <{/foreach}>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">必須</label>
                    <div class="controls">
                        <label class="checkbox"><input type="checkbox" class="tmpl-required" name="form[<{$form_id|escape}>][required]" value="1" <{if $row.required === '1'}>checked<{/if}> /> 必須である</label>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">選択肢</label>
                    <div class="controls">
                        <textarea name="form[<{$form_id|escape}>][select]" class="tmpl-select input-xlarge" rows="10"><{$row.select|escape}></textarea>
                        <{foreach from=$error[$form_id].select item=msg}>
                            <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                        <{/foreach}>
                    </div>
                </div>
                <button type="button" data-type="select" class="js-tmpl-copy btn btn-mini"><i class="icon-copy"></i> コピー</button>
                <button type="button" class="js-tmpl-remove btn btn-mini"><i class="icon-trash"></i> 削除</button>
            </div>
        <{/if}>
    <{/foreach}>
</div>
<{foreach from=$error.form item=msg}>
    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
<{/foreach}>


<div class="btn-toolbar">
    <button type="button" id="js-add-text" class="btn btn-mini btn-success"><i class="icon-edit"></i> 文字入力</button>
    <button type="button" id="js-add-textarea" class="btn btn-mini btn-success"><i class="icon-edit"></i> 複数行入力</button>
    <button type="button" id="js-add-radio" class="btn btn-mini btn-success"><i class="icon-circle-blank"></i> ラジオボタン</button>
    <button type="button" id="js-add-checkbox" class="btn btn-mini btn-success"><i class="icon-check"></i> チェックボックス</button>
    <button type="button" id="js-add-select" class="btn btn-mini btn-success"><i class="icon-collapse"></i> プルダウン選択</button>
</div>
<small><i class="icon-lightbulb"></i> 項目の順番はドラッグ＆ドロップで並べ替えられます。</small>