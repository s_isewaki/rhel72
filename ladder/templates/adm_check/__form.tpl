<div id="form">
    <{foreach from=$form key=form_id item=row}>
        <div class="control-group">
            <label class="control-label">
                <strong>
                    <{$row.name|escape}>
                </strong>
                <{if $row.required}>
                    <span class="badge badge-important required">ɬ��</span>
                <{/if}>
            </label>
            <div class="controls">
                <{if $row.type === 'text'}>
                    <input type="text" name="form[<{$form_id|escape}>][value]" value="<{$form_data.$form_id.value|escape}>" class="input-<{$row.size|escape}>" />
                <{/if}>

                <{if $row.type === 'textarea'}>
                    <textarea name="form[<{$form_id|escape}>][value]" class="input-<{$row.size|escape}>" rows="<{$row.rows|escape}>"><{$form_data.$form_id.value|escape}></textarea>
                <{/if}>

                <{if $row.type === 'radio'}>
                    <{assign var=select_loop value="\n"|explode:$row.select}>
                    <{foreach from=$select_loop item=select}>
                        <label class="radio inline">
                            <input type="radio" name="form[<{$form_id|escape}>][value]" value="<{$select|escape|strip:""}>" <{if $form_data.$form_id.value === $select|escape|strip:""}>checked<{/if}> /><{$select|escape}>
                        </label>
                    <{/foreach}>
                <{/if}>

                <{if $row.type === 'checkbox'}>
                    <{assign var=select_loop value="\n"|explode:$row.select}>
                    <{foreach from=$select_loop item=select}>
                        <label class="checkbox inline">
                            <input type="checkbox" name="form[<{$form_id|escape}>][value][]" value="<{$select|escape|strip:""}>"
                                   <{foreach from=$form_data.$form_id.value item=ck}>
                                       <{if $ck === $select|escape|strip:""}>
                                           checked
                                       <{/if}>
                                   <{/foreach}>
                                   />
                            <{$select|escape}>
                        </label>
                    <{/foreach}>
                <{/if}>

                <{if $row.type === 'select'}>
                    <{assign var=select_loop value="\n"|explode:$row.select}>
                    <select name="form[<{$form_id|escape}>][value]">
                        <{foreach from=$select_loop item=select}>
                            <option value="<{$select|escape}>" <{if $form_data.$form_id.value === $select}>selected<{/if}>><{$select|escape}></option>
                        <{/foreach}>
                    </select>
                <{/if}>

                <{foreach from=$error[$form_id] item=msg}>
                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                <{/foreach}>
            </div>
        </div>

    <{/foreach}>
</div>