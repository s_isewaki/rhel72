<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 職員一覧</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='職員一覧' hd_sub_title=''}>

            <div class="container-fluid">
                <div class="row-fluid">
                    <form method="POST">
                        <div class="control-group form-inline">
                            <div class="input-prepend">
                                <span class="add-on"><i class="icon-sitemap"></i> 部署</span>
                                <{if count($class_lists) > 1}>
                                    <select name="srh_class3" class="input-large" id="class3">
                                        <{if $full}><option value="">すべて</option><{/if}>
                                        <{foreach item=row from=$class_lists}>
                                            <option class="js-class-select select<{$row.class_id|escape}>_<{$row.atrb_id|escape}>" value="<{$row.dept_id|escape}><{if $row.room_id}>_<{$row.room_id|escape}><{/if}>">
                                                <{$row.dept_nm|escape}><{if $row.room_id}> &gt; <{$row.room_nm|escape}><{/if}>
                                            </option>
                                        <{/foreach}>
                                    </select>
                                <{else}>
                                    <input type="hidden" name="class[]" value="<{$class_lists[0].dept_id|escape}>"/><span class="uneditable-input"><{$class_lists[0].name|escape}></span>
                                <{/if}>
                            </div>
                            <div class="input-prepend">
                                <span class="add-on"><i class="icon-user-md"></i></span>
                                <input type="text" id="srh_keyword" name="srh_keyword" class="input-large" value="" placeholder="職員名を入力してください">
                            </div>
                            <button type="submit" id="btn_search" name="btn_search" value="search" class="btn btn-small btn-primary"><i class="icon-search"></i> 検索する</button>
                        </div>
                    </form>

                    <form action="#" method="post" id="emp_form">
                        <div class="well well-small" id="form_search">
                            <{include file="__pager.tpl"}>
                            <table class="table table-striped table-hover ldr-table">
                                <thead>
                                    <tr>
                                        <th rowspan="2">部署</th>
                                        <th rowspan="2">職員</th>
                                        <th rowspan="2"><a href="?order=st_order_no&desc=<{if $order === "st_order_no" && $desc === '1'}>0<{else}>1<{/if}>">役職</a></th>
                                        <th rowspan="2"><a href="?order=nurse_date&desc=<{if $order === "nurse_date" && $desc === '1'}>0<{else}>1<{/if}>">看護師経験年数</a></th>

                                        <th colspan="<{$ladder->lists()|@count}>">現在のレベル</th>

                                        <th rowspan="2">プロフィール</th>

                                        <{if $function.assessment}>
                                            <th rowspan="2"><{$titles.assessment|escape}>
                                            </th>
                                        <{/if}>

                                        <{if $function.novice}>
                                            <th rowspan="2"><{$titles.novice|escape}>
                                            </th>
                                        <{/if}>

                                        <{if $function.record}>
                                            <th rowspan="2"><{$titles.record|escape}>
                                            </th>
                                        <{/if}>

                                        <{if $function.next_plan}>
                                            <th rowspan="2"><{$titles.next_plan|escape}>
                                            </th>
                                        <{/if}>
                                    </tr>
                                    <tr>
                                        <{foreach from=$ladder->lists() item=ldr}>
                                            <th>
                                                <a href="?order=ldr<{$ldr.ladder_id|escape}>.level&desc=<{if $order === "ldr`$ldr.ladder_id`.level" && $desc === '1'}>0<{else}>1<{/if}>"><{$ldr.title|escape}></a>
                                            </th>
                                        <{/foreach}>
                                    </tr>
                                </thead>
                                <tbody>
                                    <{foreach item=row from=$list}>
                                        <tr>
                                            <td class="ldr-td-name">
                                                <{$row->class_full_name()|escape|replace:' &gt; ':'<br/>'}>
                                            </td>
                                            <td class="ldr-td-name"><b title="<{$row->emp_personal_id()|escape}>"><{$row->emp_name()|escape}></b></td>
                                            <td class="ldr-td-name"><{$row->st_name()|escape}></td>
                                            <td class="ldr-td-number"><{if $row->nurse_age()}><{$row->nurse_age()|escape}>年目<{/if}></td>

                                            <{foreach from=$ladder->lists() item=ldr}>
                                                <td class="ldr-td-level">
                                                    <{if $row->lv_date($ldr.ladder_id)}>
                                                        <span rel="tooltip"
                                                              data-html="true"
                                                              title="取得日: <{$row->lv_date($ldr.ladder_id)}><br>
                                                              医療機関: <{$row->lv_hospital($ldr.ladder_id)}><br>
                                                              認定番号: <{$row->lv_number($ldr.ladder_id)}>">
                                                            <{$row->level_roman($ldr.ladder_id)|default:$ldr.config.level0}>
                                                        </span>
                                                    <{/if}>
                                                </td>
                                            <{/foreach}>

                                            <td class="ldr-td-date">
                                                <button type="button" class="btn btn-small btn-info js-personal-button" data-php="personal_profile.php" data-target="<{$row->emp_id()|escape}>">プロフィール</button>
                                            </td>
                                            <{if $function.assessment}>
                                                <td class="ldr-td-date">
                                                    <button type="button" class="btn btn-small btn-info js-personal-button" data-php="personal_assessment.php" data-emp="<{$row->emp_id()|escape}>"><{$titles.assessment|escape}></button>
                                                </td>
                                            <{/if}>
                                            <{if $function.novice}>
                                                <td class="ldr-td-date">
                                                    <{if $row->is_novice()}>
                                                        <button type="button" class="btn btn-small btn-info js-personal-button" data-php="novice_sheet.php" data-emp="<{$row->emp_id()|escape}>" data-menu="staff"><{$titles.novice|escape}></button>
                                                    <{/if}>
                                                </td>
                                            <{/if}>
                                            <{if $function.record}>
                                                <td class="ldr-td-date">
                                                    <button type="button" class="btn btn-small btn-info js-personal-button" data-php="records_view.php" data-target="<{$row->emp_id()|escape}>" data-menu="staff"><{$titles.record|escape}></button>
                                                </td>
                                            <{/if}>
                                            <{if $function.next_plan}>
                                                <td class="ldr-td-date">
                                                    <button type="button" class="btn btn-small btn-info js-personal-button" data-php="next_plan.php" data-target="<{$row->emp_id()|escape}>" data-menu="staff"><{$titles.next_plan|escape}></button>
                                                </td>
                                            <{/if}>
                                        </tr>
                                    <{/foreach}>
                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>
            </div>

        </div>
        <{include file="__js.tpl"}>
        <{include form_selector="#emp_form" file="__pager_js.tpl"}>
        <script type="text/javascript">
            $(function() {

                /**
                 * ボタン
                 */
                $('.js-personal-button').click(function() {
                    $('<form></form>')
                        .attr({method: 'post', action: $(this).data('php')})
                        .append($('<input></input>').attr({type: 'hidden', name: 'emp_id', value: $(this).data('emp')}))
                        .append($('<input></input>').attr({type: 'hidden', name: 'target_emp_id', value: $(this).data('target')}))
                        .append($('<input></input>').attr({type: 'hidden', name: 'menu', value: $(this).data('menu')}))
                        .append($('<input></input>').attr({type: 'hidden', name: 'owner_url', value: 'staff.php'}))
                        .appendTo(document.body)
                        .submit()
                        .remove();
                });

            });
        </script>
    </body>
</html>