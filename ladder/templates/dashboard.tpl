<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | ダッシュボード</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='ダッシュボード' hd_sub_title=''}>

            <div class="container-fluid">
                <div class="row-fluid">
                    <div class="block ui-state-default">
                        <p class="block-heading" data-target="#info">インフォメーション<span class="label label-warning"><{$info|@count}></span></p>
                        <div class="ldr-ds-contents">
                            <div id="info" class="js-collapse block-body">
                                <{foreach item=row from=$info}>
                                    <div class="ldr-info alert <{$row.alert|escape}>"><{if $row.href}><a href="<{$row.href|escape}>"><{/if}><{$row.text|escape}><{if $row.href}></a><{/if}>
                                    </div>
                                <{foreachelse}>
                                    <div class="ldr-info alert alert-info">お知らせはありません</div>
                                <{/foreach}>
                            </div>
                        </div>
                    </div>
                    <div class="sortable" style="display:none">

                        <{if $function.ladder && $emp->committee()}>
                            <div id="2" class=" block ui-state-default">
                                <p class="js-head block-heading" data-target="#ldrstats"><{$titles.ladder|default:'ラダー':escape}>進捗</p>
                                <div class="ldr-ds-contents">
                                    <div id="ldrstats" class="js-collapse block-body">
                                        <form method="post">
                                            <div class="ldr-stats-widget-container clearfix">

                                                <div class="ldr-stats-widget">
                                                    <div class="ldr-stats-widget-header"><span title="<{$status_lists.4|escape}>"><{$status_lists.4|regex_replace:"/ \/ .*/":""|escape}></span></div>
                                                    <div class="ldr-stats-widget-body">
                                                        <span data-status="4,6" class="js-progress"><{$st4_count|escape}></span>
                                                        <div data-status="4,6" data-date="<{$one_week_ago|escape}>" class="js-progress">(<{$st4_count_week_ago|escape}>)</div>
                                                    </div>
                                                </div>

                                                <div class="ldr-stats-widget">
                                                    <div class="ldr-stats-widget-header"><span title="<{$status_lists.5|escape}>"><{$status_lists.5|regex_replace:"/ \/ .*/":""|escape}></span></div>
                                                    <div class="ldr-stats-widget-body">
                                                        <span data-status="5" class="js-progress"><{$st5_count|escape}></span>
                                                        <div data-status="5" data-date="<{$one_week_ago|escape}>" class="js-progress">(<{$st5_count_week_ago|escape}>)</div>
                                                    </div>
                                                </div>

                                                <div class="ldr-stats-widget">
                                                    <div class="ldr-stats-widget-header"><span title="<{$status_lists.8|escape}>"><{$status_lists.8|regex_replace:"/ \/ .*/":""|escape}></span></div>
                                                    <div class="ldr-stats-widget-body">
                                                        <span data-status="8" class="js-progress"><{$st8_count|escape}></span>
                                                        <div data-status="8" data-date="<{$one_week_ago|escape}>" class="js-progress">(<{$st8_count_week_ago|escape}>)</div>
                                                    </div>
                                                </div>

                                                <{if $hospital_type !== '1' && $is_report}>
                                                    <div class="ldr-stats-widget">
                                                        <div class="ldr-stats-widget-header"><span title="<{$status_lists.21|escape}>"><{$status_lists.21|regex_replace:"/ \/ .*/":""|escape}></span></div>
                                                        <div class="ldr-stats-widget-body">
                                                            <span data-status="21" class="js-progress"><{$st21_count|escape}></span>
                                                            <div data-status="21" data-date="<{$one_week_ago|escape}>" class="js-progress">(<{$st21_count_week_ago|escape}>)</div>
                                                        </div>
                                                    </div>

                                                    <div class="ldr-stats-widget">
                                                        <div class="ldr-stats-widget-header"><span title="<{$status_lists.22|escape}>"><{$status_lists.22|regex_replace:"/ \/ .*/":""|escape}></span></div>
                                                        <div class="ldr-stats-widget-body">
                                                            <span data-status="22" class="js-progress"><{$st22_count|escape}></span>
                                                            <div data-status="22" data-date="<{$one_week_ago|escape}>" class="js-progress">(<{$st22_count_week_ago|escape}>)</div>
                                                        </div>
                                                    </div>

                                                    <div class="ldr-stats-widget">
                                                        <div class="ldr-stats-widget-header"><span title="<{$status_lists.23|escape}>"><{$status_lists.23|regex_replace:"/ \/ .*/":""|escape}></span></div>
                                                        <div class="ldr-stats-widget-body">
                                                            <span data-status="23" class="js-progress"><{$st23_count|escape}></span>
                                                            <div data-status="23" data-date="<{$one_week_ago|escape}>" class="js-progress">(<{$st23_count_week_ago|escape}>)</div>
                                                        </div>
                                                    </div>
                                                <{/if}>

                                                <div class="ldr-stats-widget">
                                                    <div class="ldr-stats-widget-header"><span title="<{$status_lists.9|escape}>"><{$status_lists.9|regex_replace:"/ \/ .*/":""|escape}></span></div>
                                                    <div class="ldr-stats-widget-body">
                                                        <span data-status="9" class="js-progress"><{$st9_count|escape}></span>
                                                        <div data-status="9" data-date="<{$one_week_ago|escape}>" class="js-progress">(<{$st9_count_week_ago|escape}>)</div>
                                                    </div>
                                                </div>

                                                <{if $hospital_type === '1'}>
                                                    <div class="ldr-stats-widget">
                                                        <div class="ldr-stats-widget-header"><span title="<{$status_lists.10|escape}>"><{$status_lists.10|regex_replace:"/ \/ .*/":""|escape}></span></div>
                                                        <div class="ldr-stats-widget-body">
                                                            <span data-status="10" class="js-progress"><{$st10_count|escape}></span>
                                                            <div data-status="10" data-date="<{$one_week_ago|escape}>" class="js-progress">(<{$st10_count_week_ago|escape}>)</div>
                                                        </div>
                                                    </div>

                                                    <div class="ldr-stats-widget">
                                                        <div class="ldr-stats-widget-header"><span title="<{$status_lists.11|escape}>"><{$status_lists.11|regex_replace:"/ \/ .*/":""|escape}></span></div>
                                                        <div class="ldr-stats-widget-body">
                                                            <span data-status="11" class="js-progress"><{$st11_count|escape}></span>
                                                            <div data-status="11" data-date="<{$one_week_ago|escape}>" class="js-progress">(<{$st11_count_week_ago|escape}>)</div>
                                                        </div>
                                                    </div>
                                                <{/if}>

                                                <div class="ldr-stats-widget">
                                                    <div class="ldr-stats-widget-header"><span title="<{$status_lists.12|escape}>"><{$status_lists.12|regex_replace:"/ \/ .*/":""|escape}></span></div>
                                                    <div class="ldr-stats-widget-body">
                                                        <span data-status="12" class="js-progress"><{$st12_count|escape}></span>
                                                        <div data-status="12" data-date="<{$one_week_ago|escape}>" class="js-progress">(<{$st12_count_week_ago|escape}>)</div>

                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <small class="pull-right">※括弧内の数字は、1週間以上処理されていない申請件数です</small>
                                    </div>
                                </div>
                            </div>
                        <{/if}>

                        <{if $function.ladder}>
                            <div id="3" class="block ui-state-default">
                                <p class="js-head block-heading" data-target="#ldrapply"><{$titles.ladder|default:'ラダー':escape}>申請</p>
                                <div class="ldr-ds-contents">
                                    <div id="ldrapply" class="js-collapse block-body">
                                        <{if $apply_list}>
                                            <{assign var=list value=$apply_list}>
                                            <form method="post">
                                                <input type="hidden" name="mode" value="" />
                                                <input type="hidden" name="menu" value="apply" />
                                                <input type="hidden" name="id" value="" />
                                                <input type="hidden" name="target_emp_id" value="" />
                                                <input type="hidden" name="owner_url" value="dashboard.php" />
                                                <{include file="__table_apply.tpl"}>
                                            </form>
                                        <{else}>
                                            <div class="ldr-info alert alert-info">申請はありません</div>
                                        <{/if}>
                                    </div>
                                </div>
                            </div>
                            <div id="4" class="block ui-state-default">
                                <p class="js-head block-heading" data-target="#ldraccept"><{$titles.ladder|default:'ラダー':escape}>受付<{if $list}><span class="label label-warning"><{$list|@count}></span><{/if}></p>
                                <div class="ldr-ds-contents">
                                    <div id="ldraccept" class="js-collapse block-body">
                                        <{if $accept_list}>
                                            <{assign var=list value=$accept_list}>
                                            <form method="post">
                                                <input type="hidden" name="mode" value="" />
                                                <input type="hidden" name="menu" value="accept" />
                                                <input type="hidden" name="id" value="" />
                                                <input type="hidden" name="target_emp_id" value="" />
                                                <input type="hidden" name="owner_url" value="dashboard.php" />
                                                <{include file="__table_accept.tpl"}>
                                            </form>
                                        <{else}>
                                            <div class="ldr-info alert alert-info">受付はありません</div>
                                        <{/if}>
                                    </div>
                                </div>
                            </div>
                        <{/if}>

                        <{if $function.training}>
                            <div id="5" class="block ui-state-default">
                                <p class="js-head block-heading" data-target="#trastats">受講予定</p>
                                <div class="ldr-ds-contents">
                                    <div id="trastats" class="js-collapse block-body">
                                        <div id='calendar'></div>
                                    </div>
                                </div>
                            </div>
                        <{/if}>

                        <div class="control-group">
                            <button id="js-init" type="button" class="btn btn-mini btn-warning">並びを初期化する</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <{include file="__modal_status.tpl"}>
        <{include file="__modal_comment.tpl"}>
        <{include file="__modal_view.tpl" modal_id="result_confirm" modal_title="結果確認"}>

        <{include file="__js.tpl"}>
        <{include file="__modal_form_js.tpl"}>
        <script type="text/javascript">
            var sort = "ldr_ds_<{$emp->emp_id()}>_sort";
            $(function() {
                $(".sortable").sortable();
                $(".sortable").disableSelection();
                $(".sortable").sortable({
                    update: function(ev, ui) {
                        var updateArray = $(".sortable").sortable("toArray").join(",");
                        $.cookie(sort, updateArray, {expires: cookie_expires});
                    }
                });
                if ($.cookie(sort)) {
                    var cookieValue = $.cookie(sort).split(",").reverse();
                    $.each(
                        cookieValue,
                        function(index, value) {
                            $('#' + value).prependTo(".sortable");
                        }
                    );
                }
                $(".sortable").show();

            <{include file="calendar/__dashboard.tpl"}>

                $(".ldr-ds-contents").hover(
                    function() {
                        $(".sortable").sortable("disable");
                    },
                    function() {
                        $(".sortable").sortable("enable");
                    }
                );

                $('.js-progress').click(function() {
                    $(this).parents('form')
                        .attr({method: 'post', action: "ladder_progress.php"})
                        .append($('<input></input>').attr({type: 'hidden', name: 'btn_search', value: "search"}))
                        .append($('<input></input>').attr({type: 'hidden', name: 'srh_status', value: $(this).data('status')}));
                    if ($(this).data('date')) {
                        $(this).parents('form').append($('<input></input>').attr({type: 'hidden', name: 'srh_last_date', value: $(this).data('date')}));
                    }
                    $(this).parents('form').submit();
                });
                $("#js-init").click(function() {
                    $.removeCookie(sort);
                    window.location = "dashboard.php";
                });
            });
        </script>
    </body>
</html>
