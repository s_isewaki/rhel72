<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 新人評価：到達目標</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='新人評価：到達目標'}>

            <div class="container-fluid">
                <div class="row-fluid">

                    <ul class="nav nav-tabs">
                        <li class="<{if $active === 'guideline'}>active<{/if}>"><a href="#guideline" data-toggle="tab" data-active="guideline"><i class="icon-compass"></i> 到達目標</a></li>
                        <li class="<{if $active === 'revision'}>active<{/if}>"><a href="#revision" data-toggle="tab" data-active="revision"><i class="icon-book"></i> 版</a></li>
                        <li class="<{if $active === 'group1'}>active<{/if}>"><a href="#group1" data-toggle="tab" data-active="group1"><i class="icon-folder-close"></i> グループ1</a></li>
                        <li class="<{if $active === 'group2'}>active<{/if}>"><a href="#group2" data-toggle="tab" data-active="group2"><i class="icon-folder-close-alt"></i> グループ2</a></li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane <{if $active === 'guideline'}>active<{else}>fade<{/if}>" id="guideline">
                            <div class="well well-small">
                                <div class="form-group">
                                    <fieldset>
                                        <legend>到達目標</legend>
                                        <form action="adm_novice_guideline.php" method="post" id="form_guideline">
                                            <input type="hidden" name="mode" value="guideline" />
                                            <{if ! $revision_list}>
                                                <div class="alert alert-error">版を登録してください</div>
                                            <{else}>
                                                <div class="controls">
                                                    <select id="pulldown_revision" name="revision_id" class="js-pulldown-guideline js-select-revision" data-target="#pulldown_group1" data-change="revision">
                                                        <{foreach item=row from=$revision_list}>
                                                            <option value="<{$row.revision_id|escape}>"><{$row.name|escape}></option>
                                                        <{/foreach}>
                                                    </select>
                                                </div>
                                            <{/if}>

                                            <{if ! $group1_list}>
                                                <div class="alert alert-error">グループ1を登録してください</div>
                                            <{else}>
                                                <select id="pulldown_group1" name="group1" class="js-select-group1 js-pulldown-guideline" data-target="#pulldown_group2" data-change="group1">
                                                    <{foreach item=row from=$group1_list}>
                                                        <option value="<{$row.group1|escape}>"><{$row.name|escape}></option>
                                                    <{/foreach}>
                                                </select>
                                            <{/if}>

                                            <{if ! $group2_list}>
                                                <div class="alert alert-error">グループ2を登録してください</div>
                                            <{else}>
                                                <select id="pulldown_group2" name="group2" class="js-pulldown-guideline input-xxlarge" data-change="group2">
                                                    <{foreach item=row from=$group2_list}>
                                                        <option value="<{$row.group2|escape}>"><{$row.name|escape}></option>
                                                    <{/foreach}>
                                                </select>

                                                <{if $guideline_list}>

                                                    <table class="table table-striped table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th><i class='icon-pencil'></i></th>
                                                                <th class="pull-right"><i class="icon-sort"></i></th>
                                                                <th nowrap>到達目標</th>
                                                                <th nowrap>厚労省</th>
                                                                <th class="pull-right"><i class='icon-remove'></i></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody class="sortable">
                                                            <{foreach item=row from=$guideline_list}>
                                                                <tr>
                                                                    <td><a href="adm_novice_guideline_modal.php?mode=guideline&id=<{$row.guideline_id|escape}>" data-toggle="modal" data-target="#gl_modal"><i class='icon-pencil'></i></a></td>
                                                                    <td class="dragHandle"><span class="pull-right"><i class="icon-sort"></i><input type="hidden" name="order[]" value="<{$row.guideline_id|escape}>" /></span></td>
                                                                    <td><{$row.guideline|escape}></td>
                                                                    <td><{if $row.mhlw === 't'}>厚労省<{else}>オリジナル<{/if}></td>
                                                                    <td>
                                                                        <{if $row.used !== 't'}>
                                                                            <a class="pull-right js-row-remove" href="javascript:void(0);" data-target="#form_guideline" data-id="<{$row.guideline_id|escape}>">
                                                                                <i class="icon-remove"></i>
                                                                            </a>
                                                                        <{/if}>
                                                                    </td>
                                                                </tr>
                                                            <{/foreach}>
                                                        </tbody>
                                                    </table>
                                                    <hr/>
                                                <{else}>
                                                    <div class="alert alert-error">到達目標を登録してください</div>
                                                <{/if}>

                                                <div class="btn-toolbar">
                                                    <a href="adm_novice_guideline_modal.php?mode=guideline" data-toggle="modal" data-target="#gl_modal" class="btn btn-small btn-primary"><i class="icon-plus"></i> 追加する</a>
                                                    <{if $guideline_list}>
                                                        <button type="button" class="btn btn-small btn-primary js-table-sort" data-target="#form_guideline"><i class="icon-ok-sign"></i> 順番を並べ替える</button>
                                                    <{/if}>
                                                </div>
                                            <{/if}>
                                        </form>
                                        <{include file="__modal_form.tpl" modal_id="gl_modal" modal_title="到達目標の登録"}>
                                    </fieldset>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane <{if $active === 'revision'}>active<{else}>fade<{/if}>" id="revision">
                            <div class="well well-small">
                                <div class="form-group">
                                    <fieldset>
                                        <legend>版</legend>
                                        <{if $revision_list}>
                                            <form action="adm_novice_guideline.php" method="post" id="form_revision">
                                                <input type="hidden" name="mode" value="revision" />
                                                <table class="table table-striped table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th><i class='icon-pencil'></i></th>
                                                            <th>版の名称</th>
                                                            <th>指標数</th>
                                                            <th class="pull-right"><i class='icon-remove'></i></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <{foreach item=row from=$revision_list}>
                                                            <tr>
                                                                <td><a href="adm_novice_guideline_modal.php?mode=revision&id=<{$row.revision_id|escape}>" data-toggle="modal" data-target="#rv_modal"><i class='icon-pencil'></i></a></td>
                                                                <td> <{if $current_revision === $row.revision_id}><i class="icon-star"></i> <{/if}><{$row.name|escape}></td>
                                                                <td class="text-right"><{$row.count|escape}></td>
                                                                <td>
                                                                    <{if $row.count === '0'}>
                                                                        <a class="pull-right js-row-remove" href="javascript:void(0);" data-target="#form_revision" data-id="<{$row.revision_id|escape}>">
                                                                            <i class="icon-remove"></i>
                                                                        </a>
                                                                    <{/if}>
                                                                </td>
                                                            </tr>
                                                        <{/foreach}>
                                                    </tbody>
                                                </table>
                                            </form>
                                            <hr/>
                                        <{else}>
                                            <div class="alert alert-error">版を登録してください</div>
                                        <{/if}>

                                        <div class="btn-toolbar">
                                            <a href="adm_novice_guideline_modal.php?mode=revision" data-toggle="modal" data-target="#rv_modal" class="btn btn-small btn-primary"><i class="icon-plus"></i> 追加する</a>
                                        </div>
                                        <{include file="__modal_form.tpl" modal_id="rv_modal" modal_title="版の登録"}>
                                    </fieldset>
                                    <{if $revision_list}>
                                        <hr/>
                                        <fieldset>
                                            <legend>運用する版の切り替え</legend>
                                            <form action="adm_novice_guideline.php" method="post" class="form-inline">
                                                <input type="hidden" name="mode" value="revision" />
                                                <input type="hidden" name="mode2" value="current_revision" />
                                                <select name="current_revision">
                                                    <{foreach item=row from=$revision_list}>
                                                        <option value="<{$row.revision_id|escape}>" <{if $current_revision === $row.revision_id}>selected<{/if}>><{$row.name|escape}></option>
                                                    <{/foreach}>
                                                </select>
                                                <button type="submit" class="btn btn-small btn-primary"><i class="icon-ok-sign"></i> この版を使用する</button>
                                            </form>
                                        </fieldset>
                                    <{/if}>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane <{if $active === 'group1'}>active<{else}>fade<{/if}>" id="group1">
                            <div class="well well-small">
                                <div class="form-group">
                                    <fieldset>
                                        <legend>グループ1</legend>
                                        <form action="adm_novice_guideline.php" method="post" id="form_group1" name ="form_group1">
                                            <input type="hidden" name="mode" value="group1" />
                                            <select id="pulldown_group1_revision" name="revision_id" class="js-pulldown-group1">
                                                <{foreach item=row from=$revision_list}>
                                                    <option value="<{$row.revision_id|escape}>"><{$row.name|escape}></option>
                                                <{/foreach}>
                                            </select>
                                            <{if $group1_list}>
                                                <table class="table table-striped table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th><i class='icon-pencil'></i></th>
                                                            <th class="pull-right"><i class="icon-sort"></i></th>
                                                            <th>グループ1の名称</th>
                                                            <th class="pull-right"><i class='icon-remove'></i></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody class="sortable">
                                                        <{foreach item=row from=$group1_list}>
                                                            <tr>
                                                                <td><a href="adm_novice_guideline_modal.php?mode=group1&id=<{$row.group1|escape}>" data-toggle="modal" data-target="#g1_modal"><i class='icon-pencil'></i></a></td>
                                                                <td class="dragHandle"><span class="pull-right"><i class="icon-sort"></i><input type="hidden" name="order[]" value="<{$row.group1|escape}>" /></span></td>
                                                                <td><{$row.name|escape}></td>
                                                                <td>
                                                                    <{if $row.used !== 't'}>
                                                                        <a class="pull-right js-row-remove" href="javascript:void(0);" data-target="#form_group1" data-id="<{$row.group1|escape}>">
                                                                            <i class="icon-remove"></i>
                                                                        </a>
                                                                    <{/if}>
                                                                </td>
                                                            </tr>
                                                        <{/foreach}>
                                                    </tbody>
                                                </table>
                                            <{else}>
                                                <div class="alert alert-error">グループ1を登録してください</div>
                                            <{/if}>
                                        </form>
                                        <hr/>

                                        <div class="btn-toolbar">
                                            <a href="adm_novice_guideline_modal.php?mode=group1" data-toggle="modal" data-target="#g1_modal" class="btn btn-small btn-primary"><i class="icon-plus"></i> 追加する</a>
                                            <{if $group1_list}>
                                                <button type="button" class="btn btn-small btn-primary js-table-sort" data-target="#form_group1"><i class="icon-ok-sign"></i> 順番を並べ替える</button>
                                            <{/if}>
                                        </div>
                                        <{include file="__modal_form.tpl" modal_id="g1_modal" modal_title="グループ1の登録"}>
                                    </fieldset>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane <{if $active === 'group2'}>active<{else}>fade<{/if}>" id="group2">
                            <div class="well well-small">
                                <div class="form-group">
                                    <fieldset>
                                        <legend>グループ2</legend>
                                        <form action="adm_novice_guideline.php" method="post" id="form_group2" name="form_group2">
                                            <input type="hidden" name="mode" value="group2" />
                                            <div class="controls">
                                                <select id="pulldown_group2_revision" name="revision_id" class="js-pulldown-group2" data-change="revision">
                                                    <{foreach item=row from=$revision_list}>
                                                        <option value="<{$row.revision_id|escape}>"><{$row.name|escape}></option>
                                                    <{/foreach}>
                                                </select>
                                                <select id="pulldown_group2_group1" name="group1" class="js-pulldown-group2" data-change="group1">
                                                    <{foreach item=row from=$group1_list}>
                                                        <option value="<{$row.group1|escape}>"><{$row.name|escape}></option>
                                                    <{/foreach}>
                                                </select>
                                            </div>
                                            <{if $group2_list}>
                                                <table class="table table-striped table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th><i class='icon-pencil'></i></th>
                                                            <th class="pull-right"><i class="icon-sort"></i></th>
                                                            <th>グループ2の名称</th>
                                                            <th class="pull-right"><i class='icon-remove'></i></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody class="sortable">
                                                        <{foreach item=row from=$group2_list}>
                                                            <tr>
                                                                <td><a href="adm_novice_guideline_modal.php?mode=group2&id=<{$row.group2|escape}>" data-toggle="modal" data-target="#g2_modal"><i class='icon-pencil'></i></a></td>
                                                                <td class="dragHandle"><span class="pull-right"><i class="icon-sort"></i><input type="hidden" name="order[]" value="<{$row.group2|escape}>" /></span></td>
                                                                <td><{$row.name|escape}></td>
                                                                <td>
                                                                    <{if $row.used !== 't'}>
                                                                        <a class="pull-right js-row-remove" href="javascript:void(0);" data-target="#form_group2" data-id="<{$row.group2|escape}>">
                                                                            <i class="icon-remove"></i>
                                                                        </a>
                                                                    <{/if}>
                                                                </td>
                                                            </tr>
                                                        <{/foreach}>
                                                    </tbody>
                                                </table>
                                            <{else}>
                                                <div class="alert alert-error">グループ2を登録してください</div>
                                            <{/if}>
                                        </form>
                                        <hr/>

                                        <div class="btn-toolbar">
                                            <a href="adm_novice_guideline_modal.php?mode=group2" data-toggle="modal" data-target="#g2_modal" class="btn btn-small btn-primary"><i class="icon-plus"></i> 追加する</a>
                                            <{if $group2_list}>
                                                <button type="button" class="btn btn-small btn-primary js-table-sort" data-target="#form_group2"><i class="icon-ok-sign"></i> 順番を並べ替える</button>
                                            <{/if}>
                                        </div>
                                        <{include file="__modal_form.tpl" modal_id="g2_modal" modal_title="グループ2の登録"}>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <{include file="__modal_form_js.tpl"}>
        <{javascript src="../js/jquery/jquery.cookie.js"}>
        <script type="text/javascript">
            $(function() {
                //項目の削除
                $('.js-row-remove').click(function() {
                    if (!confirm('削除してもよろしいですか？')) {
                        return false;
                    }
                    $($(this).data('target'))
                        .append($('<input></input>').attr({type: 'hidden', name: 'id', value: $(this).data('id')}))
                        .append($('<input></input>').attr({type: 'hidden', name: 'mode2', value: 'delete'}))
                        .submit();
                });

                //順番変更
                $('.js-table-sort').click(function() {
                    if (!confirm('順番を変更してもよろしいですか？')) {
                        return false;
                    }
                    $($(this).data('target'))
                        .append($('<input></input>').attr({type: 'hidden', name: 'mode2', value: 'sort'}))
                        .submit();
                });

                // セレクト連動
                $('body').on('change', 'select.js-select-revision', function() {
                    var target = $(this).data('target');
                    var id = $(this).val();
                    $.ajax({
                        url: 'adm_novice_guideline_pulldown.php',
                        dataType: 'json',
                        type: 'post',
                        data: {
                            mode: 'group1',
                            revision_id: id
                        }
                    }).done(function(obj) {
                        $(target).empty();
                        $.each(obj, function(i, row) {
                            $(target).append('<option value="' + row.group1 + '">' + row.name + '</option>');
                        });
                        $(target).change();
                    });
                });
                $('body').on('change', 'select.js-select-group1', function() {
                    var target = $(this).data('target');
                    var id = $(this).val();
                    $.ajax({
                        url: 'adm_novice_guideline_pulldown.php',
                        dataType: 'json',
                        type: 'post',
                        data: {
                            mode: 'group2',
                            group1: id,
                        }
                    }).done(function(obj) {
                        $(target).empty();
                        $.each(obj, function(i, row) {
                            $(target).append('<option value="' + row.group2 + '">' + row.name + '</option>');
                        });
                        $(target).change();
                    });
                });

                // グループ1プルダウン選択
                $('.js-pulldown-group1').change(function() {
                    $.cookie('pulldown_revision', $('#pulldown_group1_revision').val());
                    $.cookie('pulldown_group1', '');
                    $.cookie('pulldown_group2', '');
                    $('#form_group1').submit();
                });

                // グループ2プルダウン選択
                $('.js-pulldown-group2').change(function() {
                    $.cookie('pulldown_revision', $('#pulldown_group2_revision').val());

                    if ($(this).data('change') === 'group1') {
                        $.cookie('pulldown_group1', $('#pulldown_group2_group1').val());
                    }
                    else {
                        $.cookie('pulldown_group1', '');
                    }

                    $.cookie('pulldown_group2', '');
                    $('#form_group2').submit();
                });


                // 到達目標プルダウン選択
                $('.js-pulldown-guideline').change(function() {
                    $.cookie('pulldown_revision', $('#pulldown_revision').val());

                    switch ($(this).data('change')) {
                        case 'group1':
                            $.cookie('pulldown_group1', $('#pulldown_group1').val());
                            $.cookie('pulldown_group2', '');
                            break;

                        case 'group2':
                            $.cookie('pulldown_group1', $('#pulldown_group1').val());
                            $.cookie('pulldown_group2', $('#pulldown_group2').val());
                            break;

                        default:
                            $.cookie('pulldown_group1', '');
                            $.cookie('pulldown_group2', '');
                            break;
                    }

                    $('#form_revision').submit();
                });

                // タブ選択
                $('a[data-toggle="tab"]').on('shown', function(e) {
                    $.cookie('active', $(e.target).data('active'));
                });
            });
        </script>
    </body>
</html>
