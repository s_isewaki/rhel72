<ul class="nav nav-tabs">
    <li class="active"><a href="#c1" data-toggle="tab"><i class="icon-medkit"></i> 研修目標</a></li>
    <li><a href="#c2" data-toggle="tab"><i class="icon-book"></i> 研修内容</a></li>
    <li><a href="#c3" data-toggle="tab"><i class="icon-group"></i> 担当</a></li>
</ul>
<div class="tab-content">
    <div class="tab-pane active" id="c1">
        <div class="control-group">
            <label class="control-label">
                <{if $hospital_type === '1'}>
                    <strong>めざすレベル</strong>
                <{else}>
                    <strong>対象レベル</strong>
                <{/if}>
            </label>
            <{if $level}>
                <span class="ldr-uneditable input-large-modal ldr-level"><{$roman_num[$level]|escape}></span>
            <{else}>
                <span class="ldr-uneditable input-large-modal">(未設定)</span>
            <{/if}>
        </div>
        <{if $guideline || $guideline_text}>
            <div class="control-group">
                <label class="control-label"><strong>めざす状態</strong></label>
                <div class="controls">
                    <div class="ldr-glbox-modal">
                        <{foreach from=$guideline key=id item=name}>
                            <div id="gl<{$id|escape}>" class="alert alert-info">
                                <{$name|escape}><input type="hidden" name="guideline[<{$id|escape}>]" value="<{$name|escape}>" />
                            </div>
                        <{/foreach}>
                        <{if $guideline_text}>
                            <div class="alert alert-info"><{$guideline_text|escape|nl2br}></div>
                        <{/if}>
                    </div>

                </div>
            </div>
        <{/if}>
    </div>
    <div class="tab-pane" id="c2">
        <{if $theme}>
            <div class="control-group">
                <label class="control-label">
                    <strong>テーマ</strong>
                </label>
                <div class="controls">
                    <span class="ldr-uneditable input-large-modal"><{$theme|escape|nl2br|default:'(未設定)'}></span>
                </div>
            </div>
        <{/if}>
        <div class="control-group">
            <label class="control-label">
                <strong>カテゴリ</strong>
            </label>
            <span class="ldr-uneditable input-large-modal"><{$gp1.name|escape}></span>
        </div>
        <div class="control-group">
            <label class="control-label">
                <strong>外部参加</strong>
            </label>
            <span class="ldr-uneditable input-large-modal">
                <{if $public==='t'}>
                    可
                <{else}>
                    不可
                <{/if}>
            </span>
        </div>
    </div>

    <div class="tab-pane" id="c3">
        <div class="control-group">
            <label class="control-label">
                <strong>企画担当</strong>
            </label>
            <{if $planner}>
                <span class="ldr-uneditable input-large-modal">
                    <{$planner|escape|default:'(未設定)'}>
                </span>
            <{/if}>
            <div id="planner" class="input-large-modal">
                <{foreach from=$emp_planner key=id item=name}>
                    <div id="coach<{$id|escape}>" class="label label-info">
                        <{$name|escape|default:'(未設定)'}>
                    </div>
                <{/foreach}>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label">
                <strong>講師</strong>
            </label>
            <{if $coach}>
                <span class="ldr-uneditable input-large-modal">
                    <{$coach|escape|default:'(未設定)'}>
                </span>
            <{/if}>
            <div id="coach" class="input-large-modal">
                <{foreach from=$emp_coach key=id item=name}>
                    <div id="coach<{$id|escape}>" class="label label-info">
                        <{$name|escape}>
                    </div>
                <{/foreach}>
            </div>
        </div>
    </div>
</div>