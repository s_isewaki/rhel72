<form id="form_mdl_pattern" action="analysis_placement.php" method="post">
    <div class="form-horizontal">
        <div id="modal_pattern_pattern" class="control-group">
            <label class="control-label">パターン名</label>
            <div class="controls">
                <input type="text" name="pattern" value="" maxlength="200" class="input-xlarge" placeholder="パターン名を入力してください" />
                <div id="error_pattern_pattern" class="help-block hide"></div>
            </div>
        </div>
    </div>
    <input type="hidden" name="id" value="" />
    <input type="hidden" name="mode" value="pattern" />
    <input type="hidden" id="mode2_pattern" name="mode2" value="validate" />
    <input type="hidden" id="parent_form" name="parent_form" value="#form_placement" />
</form>
