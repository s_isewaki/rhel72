<div class="control-group">
    <div class="input-prepend" style="margin-bottom:0;">
        <span class="add-on">申請レベル</span>
        <span class="ldr-uneditable input-mini ldr-level" style="margin-bottom:0;"><{$apply->level_text()|escape}></span>
    </div>
    <div class="input-prepend" style="margin-bottom:0;">
        <span class="add-on">申請者</span>
        <span class="ldr-uneditable" style="margin-bottom:0;"><{$target_emp->emp_name()|escape}> (<{$target_emp->class_full_name()|escape}>)</span>
    </div>
</div>

<div class="control-group">
    <div class="controls">
        <{if $result === 't'}>
            <div class="alert alert-success">
                あなたに対し申請レベルを認めます。
            </div>
        <{else}>
            <div class="alert alert-danger">
                今回の申請レベルは保留にします。再チャレンジしてください。
            </div>
        <{/if}>
    </div>
</div>
<{if $result === 't'}>
    <div class="control-group">
        <label class="control-label">
            <strong>認定日</strong>
        </label>
        <div class="controls">
            <span class="ldr-uneditable input-xlarge"><{$target_emp->lv_date($apply->ladder_id())|date_format_jp:"%Y年%-m月%-d日(%a)"}></span>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">
            <strong>取得医療施設</strong>
        </label>
        <div class="controls">
            <span class="ldr-uneditable input-xlarge"><{$target_emp->lv_hospital($apply->ladder_id())|escape|default:'(未設定)'}></span>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">
            <strong>認定番号</strong>
        </label>
        <div class="controls">
            <span class="ldr-uneditable input-xlarge"><{$target_emp->lv_number($apply->ladder_id())|escape|default:'(未設定)'}></span>
        </div>
    </div>
<{elseif $target_emp->emp_id() === $emp->emp_id()}>
    <hr/>
    <form id="form_objection">
        <div class="control-group">
            <label class="control-label">
                <strong>結果に対して異議申し立てを行います</strong>
            </label>
            <div class="controls">
                <textarea id="comment" name="comment" rows="4" class="input-maxlarge js-autoheight" placeholder="異議申し立て内容"><{$objection.comment|escape}></textarea>
            </div>
        </div>
        <div>
            <input type="hidden" name="ldr_apply_id" value="<{$apply->apply_id()}>" />
            <button type="button" class="btn btn-small btn-danger js-modal-objection">異議申し立てを行う <{if $objection}>(更新)<{/if}></button>
        </div>
    </form>
    <script type="text/javascript">
        $(function() {
            $('.js-modal-objection').click(function() {
                if ($('#comment').val() === '') {
                    alert('異議申し立ての内容を必ず記載してください。');
                    return false;
                }
                $.post('apply_objection_comment.php', $("#form_objection").serialize());
                $('#result_confirm').modal('hide');
            });

            $('.js-autoheight').click();
        });
    </script>
<{/if}>
