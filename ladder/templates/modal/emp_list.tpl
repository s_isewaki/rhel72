<{if !$lists}>
    <div class="alert alert-error">対象者が存在しませんでした。</div>
<{else}>
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>No</th>
                <th>氏名</th>
                <th>部署</th>
            </tr>
        </thead>
        <tbody>
            <{foreach item=row from=$lists name=loop}>
                <tr>
                    <td class="ldr-td-text"><{$smarty.foreach.loop.index+1}></td>
                    <td class="ldr-td-text"><{$row.name|escape}></td>
                    <td class="ldr-td-text"><{$row.class_full_name|escape}></td>
                </tr>
            <{/foreach}>
        </tbody>
    </table>
<{/if}>

