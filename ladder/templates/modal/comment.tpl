<{if $list}>
    <{foreach item=row from=$list}>
        <{if $row.del_flg === 'f'}>
            <{if $row.emp_id === $emp->emp_id()}>
                <div class="ldr-comment pull-right">
                    <div class="ldr-comment-name text-right">
                        <span class="label label-warning"><{$ldr.config.status[$row.status]|escape}></span>
                        <span class="ldr-comment-date"><{$row.created_on|date_format_jp:"%Y年%-m月%-d日(%a) %R"}></span>
                    </div>
                    <div class="ldr-comment-body ldr-comment-self"><{$row.comment|escape|nl2br}></div>
                </div>
            <{else}>
                <div class="ldr-comment pull-left">
                    <div class="ldr-comment-name text-left">
                        <{$row.emp_name|escape}>
                        <span class="label label-warning"><{$ldr.config.status[$row.status]|escape}></span>
                        <span class="ldr-comment-date"><{$row.created_on|date_format_jp:"%Y年%-m月%-d日(%a) %R"}></span>
                    </div>
                    <div class="ldr-comment-body ldr-comment-other"><{$row.comment|escape|nl2br}></div>
                </div>
            <{/if}>
        <{else}>
            <div class="alert alert-error">このコメントは削除されました</div>
        <{/if}>
        <div class="clearfix"></div>
        <hr class="ldr-hr"/>
    <{/foreach}>
    <div id="scroll"></div>
    <script type="text/javascript">
        var posi = $('#scroll').offset().top;
        $('#modal_comment').find('.modal-body').scrollTop(posi);
    </script>
<{else}>
    <div class="alert alert-error">コメントはありません</div>
<{/if}>