<div class="control-group">
    <div class="input-prepend" style="margin-bottom:0;">
        <span class="add-on">申請レベル</span>
        <span class="ldr-uneditable input-mini ldr-level" style="margin-bottom:0;"><{$apply->level_text()|escape}></span>
    </div>
    <div class="input-prepend" style="margin-bottom:0;">
        <span class="add-on">申請者</span>
        <span class="ldr-uneditable input-large" style="margin-bottom:0;"><{$target_emp->emp_name()|escape}> (<{$target_emp->class_full_name()|escape}>)</span>
    </div>
</div>

<div class="control-group">
    <div class="controls">
        <{if $result === 't'}>
            <div class="alert alert-success">
                あなたの看護実践に対し申請レベルを認めます。
            </div>
        <{else}>
            <div class="alert alert-danger">
                今回の申請レベルは保留にします。再チャレンジしてください。
            </div>
        <{/if}>
    </div>
</div>
<{if $result === 't'}>
    <div class="control-group">
        <label class="control-label">
            <strong>認定日</strong>
        </label>
        <div class="controls">
            <span class="ldr-uneditable input-xlarge"><{$target_emp->lv_date($apply->ladder_id())|date_format_jp:"%Y年%-m月%-d日(%a)"}></span>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">
            <strong>取得医療施設</strong>
        </label>
        <div class="controls">
            <span class="ldr-uneditable input-xlarge"><{$target_emp->lv_hospital($apply->ladder_id())|escape|default:'(未設定)'}></span>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">
            <strong>認定番号</strong>
        </label>
        <div class="controls">
            <span class="ldr-uneditable input-xlarge"><{$target_emp->lv_number($apply->ladder_id())|escape|default:'(未設定)'}></span>
        </div>
    </div>
<{/if}>

<hr/>
<form id="form_objection">
    <div class="control-group">
        <label class="control-label">
            <strong>異議申し立て内容</strong>
        </label>
        <div class="controls">
            <span class="ldr-uneditable input-maxlarge"><{$objection.comment|escape|nl2br}></span>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">
            <strong><{$titles.committee|escape}>のコメント</strong>
        </label>
        <div class="controls">
            <textarea id="comment" name="admin_comment" rows="4" class="input-maxlarge js-autoheight" placeholder="コメント"><{$objection.admin_comment|escape}></textarea>
        </div>
    </div>
    <div>
        <input type="hidden" name="ldr_apply_id" value="<{$apply->apply_id()}>" />
        <{if $objection.read === 't'}>
            <button type="button" class="btn btn-small btn-success js-modal-objection">異議申し立て受理済 (コメントを更新する)</button>
        <{else}>
            <button type="button" class="btn btn-small btn-danger js-modal-objection">異議申し立てを受理する</button>
        <{/if}>
    </div>
</form>
<script type="text/javascript">
    $(function() {
        $('.js-modal-objection').click(function() {
            if ($('#comment').val() === '') {
                alert('コメントを必ず記載してください。');
                return false;
            }
            $.post('accept_objection_comment.php', $("#form_objection").serialize());
            $('#objection').modal('hide');
        });

        $('.js-autoheight').click();
    });
</script>
