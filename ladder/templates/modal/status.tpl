<{if $apply->status_history()}>
    <table width="100%" class="table table-striped table-hover table-condensed ldr-table">
        <thead>
            <tr>
                <th>受付日時</th>
                <th>ステータス</th>
                <th>受付者</th>
            </tr>
        </thead>
        <tbody>
            <{foreach item=row from=$apply->status_history()}>
                <{if $ldr.config.anonymous !== '1' || $row.status !== '8' || $apply->is_total_appraiser($emp->emp_id()) || $emp->is_committee()}>
                    <tr>
                        <td class="ldr-td-date"><{$row.updated_on|date_format_jp:"%Y年%-m月%-d日(%a) %R"}></td>
                        <td class="ldr-td-status"><{$ldr.config.status[$row.status]|escape}></td>
                        <td class="ldr-td-name"><{$row.emp_name|escape}></td>
                    </tr>
                <{/if}>
            <{/foreach}>
        </tbody>
    </table>
<{else}>
    <div class="alert alert-error">受付履歴はありません</div>
<{/if}>