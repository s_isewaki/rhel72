<{if !$list}>
    <div class="alert alert-error">更新履歴が見つかりませんでした</div>
<{else}>
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th style="text-align: center">更新日時</th>
                <th>氏名</th>
                <th>内容</th>
            </tr>
        </thead>
        <tbody>
            <{foreach item=row from=$list name=loop}>
                <tr>
                    <td class="ldr-td-date">
                        <{$row.updated_on|date_format_jp:"%Y年%-m月%-d日(%a) %R"}>
                    </td>
                    <td class="ldr-td-text"><{$row.name|escape}></td>
                    <td class="ldr-td-text-left"><{$row.log|escape}></td>
                </tr>
            <{/foreach}>
        </tbody>
    </table>
<{/if}>

