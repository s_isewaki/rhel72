<div style="text-align: center;margin: 0 0 15px 0">
    <h5>(<{$no}>)<{$title}></h5>
</div>


<form id="form_mst_copy" action="adm_inside.php" method="post">
    <div class="form-horizontal">
        <div id="modal_copy_year" class="control-group">
            <label class="control-label">
                <span class="badge badge-important required">必須</span>
                <strong>年度</strong>
            </label>
            <div class="controls">
                <select name="year" class="input-medium">
                    <{foreach item=year from=$years}>
                        <option value="<{$year|escape}>"><{$year|escape}>年度</option>
                    <{/foreach}>
                </select>
                <div id="error_copy_year" class="help-block hide"></div>
            </div>
        </div>
        <div id="modal_copy_number" class="control-group">
            <label class="control-label">
                <span class="badge badge-important required">必須</span>
                <strong>回数</strong>
            </label>
            <div class="controls">
                <div class="form-inline">
                    <label class="radio">
                        <input type="radio" name="number" id="number_copy" value="copy" checked class="js-number-modal"><strong>する</strong>
                    </label>
                    <label class="radio">
                        <input type="radio" name="number" id="number_ignore" value="ignore" class="js-number-modal"><strong>しない</strong>
                    </label>
                </div>
                <div id="error_copy_number" class="help-block hide"></div>
            </div>
        </div>
        <div id="modal_copy_date" class="control-group">
            <label class="control-label">
                <span class="badge badge-important required">必須</span>
                <strong>日程</strong>
            </label>
            <div class="controls">
                <div class="form-inline">
                    <label class="radio">
                        <input type="radio" name="date" id="date_copy" value="copy" checked class="js-date-modal"><strong>する</strong>
                    </label>
                    <label class="radio">
                        <input type="radio" name="date" id="date_ignore" value="ignore" class="js-date-modal"><strong>しない</strong>
                    </label>
                </div>
                <div id="error_copy_date" class="help-block hide"></div>
            </div>
        </div>

    </div>
    <input type="hidden" name="id" value="" />
    <input type="hidden" name="mode" value="copy" />
    <input type="hidden" id="mode2_copy" name="mode2" value="validate" />
</form>
