<ul class="nav nav-tabs">
    <li class="active">
        <a href="#contents" data-toggle="tab"><i class="icon-book"></i> 研修内容</a>
    </li>
    <{if $date}>
        <li>
            <a href="#schedule" data-toggle="tab"><i class="icon-calendar"></i> 日程</a>
        </li>
    <{/if}>
    <li>
        <a href="#before_work" data-toggle="tab"><i class="icon-pencil"></i> 前課題</a>
    </li>
    <li class="">
        <a href="#after_work" data-toggle="tab"><i class="icon-pencil"></i> 後課題</a>
    </li>
    <li class="">
        <a href="#looking_back" data-toggle="tab"><i class="icon-pencil"></i> 振り返り</a>
    </li>
</ul>
<{assign var="rows" value="4"}>
<div class="tab-content">
    <div class="tab-pane active" id="contents">
        <{if $aim}>
            <div class="control-group">
                <label class="control-label">
                    <strong>研修のねらい</strong>
                </label>
                <div class="controls">
                    <span class="ldr-uneditable input-large-modal"><{$aim|escape|nl2br}></span>
                </div>
            </div>
        <{/if}>
        <{if $objective}>
            <div class="control-group">
                <label class="control-label">
                    <strong>研修目標</strong>
                </label>
                <div class="controls">
                    <span class="ldr-uneditable input-large-modal"><{$objective|escape|nl2br}></span>
                </div>
            </div>
        <{/if}>
        <div class="control-group">
            <label class="control-label">
                <strong>研修内容</strong>
            </label>
            <div class="controls">
                <span class="ldr-uneditable input-large-modal"><{$contents|escape|nl2br}></span>
            </div>
        </div>
        <{if $method}>
            <div class="control-group">
                <label class="control-label">
                    <strong>研修方法</strong>
                </label>
                <div class="controls">
                    <span class="ldr-uneditable input-large-modal"><{$method|escape|nl2br}></span>
                </div>
            </div>
        <{/if}>
    </div>
    <{if $date}>
        <div class="tab-pane" id="schedule">
            <div class="control-group">
                <label class="control-label">
                    <strong>研修回</strong>
                </label>
                <div class="controls">
                    <span class="ldr-uneditable input-medium" style="color:#000;">第<{$number|escape}>回</span>
                </div>
            </div>
            <{if $date.type === '1'}>
                <div class="control-group">
                    <label class="control-label">
                        <strong>開催日時</strong>
                    </label>
                    <div class="controls">
                        <span class="ldr-uneditable"><{$date.training_date|date_format_jp:'%Y年%-m月%-d日(%a)'}></span>
                        <span class="ldr-uneditable"><{$date.training_start_time|date_format:'%R'}></span> 〜 <span class="ldr-uneditable"><{$date.training_last_time|date_format:'%R'}></span>
                    </div>
                </div>
            <{/if}>
            <div class="control-group">
                <label class="control-label">
                    <strong>受付日時</strong>
                </label>
                <div class="controls">
                    <span class="ldr-uneditable"><{$date.accept_date|date_format_jp:'%Y年%-m月%-d日(%a) %R'}></span> 〜 <span class="ldr-uneditable"><{$date.deadline_date|date_format_jp:'%Y年%-m月%-d日(%a) %R'}></span>
                </div>
            </div>
            <{if $date.type === '1'}>
                <div class="control-group">
                    <label class="control-label">
                        <strong>開催場所</strong>
                    </label>
                    <div class="controls">
                        <span class="ldr-uneditable input-large-modal"><{$date.place|escape}></span>
                    </div>
                </div>
            <{/if}>
        </div>
    <{/if}>
    <div class="tab-pane" id="before_work">
        <{if $before_work === 't'}>
            <div class="control-group">
                <label class="control-label">
                    <strong>課題内容</strong>
                </label>
                <div class="controls">
                    <span class="ldr-uneditable input-large-modal"><{$before_work_contents|escape|nl2br}></span>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">
                    <strong>課題方法</strong>
                </label>
                <div class="controls">
                    <span class="ldr-uneditable input-large-modal"><{$before_work_submit|escape|nl2br}></span>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">
                    <strong>備考</strong>
                </label>
                <div class="controls">
                    <span class="ldr-uneditable input-large-modal"><{$before_work_notes|escape|nl2br}></span>
                </div>
            </div>

        <{else}>
            <div class="alert alert-info">研修前の課題はありません</div>
        <{/if}>
    </div>

    <div class="tab-pane" id="after_work">
        <{if $after_work === 't'}>
            <div class="control-group">
                <label class="control-label">
                    <strong>課題内容</strong>
                </label>
                <div class="controls">
                    <span class="ldr-uneditable input-large-modal"><{$after_work_contents|escape|nl2br}></span>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">
                    <strong>課題方法</strong>
                </label>
                <div class="controls">
                    <span class="ldr-uneditable input-large-modal"><{$after_work_submit|escape|nl2br}></span>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">
                    <strong>備考</strong>
                </label>
                <div class="controls">
                    <span class="ldr-uneditable input-large-modal"><{$after_work_notes|escape|nl2br}></span>
                </div>
            </div>
        <{else}>
            <div class="alert alert-info">研修後の課題はありません</div>
        <{/if}>
    </div>

    <div class="tab-pane" id="looking_back">
        <div class="control-group">
            <label class="control-label">
                <strong>振り返り</strong>
            </label>
            <div class="controls">
                <span class="ldr-uneditable input-large-modal">
                    <{if $looking_back === 't'}>
                        有り
                    <{else}>
                        無し
                    <{/if}>
                </span>
            </div>
        </div>
    </div>

</div>