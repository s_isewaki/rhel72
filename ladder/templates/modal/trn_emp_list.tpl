<{if !$lists}>
    <div class="alert alert-error">申込はありません。</div>
<{else}>
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>No</th>
                <th>部署</th>
                <th>氏名</th>
                <th>ステータス</th>
            </tr>
        </thead>
        <tbody>
            <{foreach item=row from=$lists name=loop}>
                <tr>
                    <td class="ldr-td-text"><{$smarty.foreach.loop.index+1}></td>
                    <td class="ldr-td-text">
                        <{$row.class_full_name|escape}>
                    </td>
                    <td class="ldr-td-text"><{$row.name}></td>
                    <td class="ldr-td-text">
                        <{$row.status_text}>
                    </td>
                </tr>
            <{/foreach}>
        </tbody>
    </table>
<{/if}>

