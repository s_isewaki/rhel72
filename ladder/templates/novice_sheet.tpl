<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | <{$titles.novice|escape}>: 評価シート</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title="`$titles.novice`: 評価シート" hd_sub_title=''}>

            <div class="container-fluid">
                <div class="control-group">
                    <button type="button" class="js-btn-back btn btn-small" data-back="<{$smarty.post.owner_url|escape}>"><i class="icon-circle-arrow-left"></i> 戻る</button>
                    <{if $year_list|@count > 1}>
                        <select id="year" name="year" class="input-medium" style="margin-bottom:0;" data-emp="<{$emp->emp_id()|escape}>" data-menu="<{$smarty.post.menu|escape}>" data-url="<{$smarty.post.owner_url|escape}>">
                            <{foreach item=y from=$year_list}>
                                <option value="<{$y|escape}>" <{if $y === $year}>selected<{/if}>><{$y|escape}>年度</option>
                            <{/foreach}>
                        </select>
                    <{else}>
                        <b class="ldr-uneditable" style="margin-bottom:0;"><{$year|escape}>年度</b>
                        <input type="hidden" id="year" name="year" value="<{$year|escape}>" />
                    <{/if}>
                    <div class="input-prepend" style="margin-bottom:0;">
                        <span class="add-on">本人</span>
                        <span class="ldr-uneditable input-medium" style="margin-bottom:0;"><{$novice->emp_name()|escape}></span>
                    </div>
                    <div class="input-prepend" style="margin-bottom:0;">
                        <span class="add-on">教育担当者</span>
                        <{foreach item=facilitator from=$facilitators}>
                            <span class="ldr-uneditable input-medium" style="margin-bottom:0;"><{$facilitator.name|escape}></span>
                        <{/foreach}>
                    </div>
                    <div class="pull-right">
                        &nbsp;<a role="button" class="btn btn-small btn-primary" data-toggle="modal" data-target="#chart"><i class="icon-bar-chart"></i> チャート</a>
                        &nbsp;<button id="btn-print" type="button" class="js-novice-sheet-print btn btn-small btn-primary " data-emp="<{$novice->emp_id()|escape}>" data-target="a1"><i class="icon-print"></i> 印刷する</button>
                    </div>
                </div>
                <div class="well well-small">
                    <{foreach item=g1 from=$group1_list}>
                        <div>
                            <div class="alert alert-info"><{$g1.name|escape}></div>
                            <small>
                                <span class="ldr-level"><{$nlv[1]}></span>:できる&nbsp;&nbsp;
                                <span class="ldr-level"><{$nlv[2]}></span>:指導の下でできる&nbsp;&nbsp;
                                <span class="ldr-level"><{$nlv[3]}></span>:演習でできる&nbsp;&nbsp;
                                <span class="ldr-level"><{$nlv[4]}></span>:知識としてわかる&nbsp;&nbsp;
                            </small>
                            <table class="table table-bordered table-hover table-condensed">
                                <thead>
                                    <tr>
                                        <td rowspan="2" style="width:100px;"></td>
                                        <td rowspan="2"></td>
                                        <{foreach item=d from=$date_list}>
                                            <td colspan="2" style="text-align: center;">
                                                第<{$d.number|escape}>回<br/><{$d.last_date|date_format_jp:"%y年%-m月"}>
                                            </td>
                                        <{/foreach}>
                                    </tr>
                                    <tr>
                                        <{foreach item=d from=$date_list}>
                                            <td style="text-align: center; width:30px;" class="js-bar-change bar-change" data-target="a<{$d.number|escape}>">自己</td>
                                            <td style="text-align: center; width:30px;" class="js-bar-change bar-change" data-target="f<{$d.number|escape}>">他者</td>
                                        <{/foreach}>
                                    </tr>
                                </thead>
                                <tbody>
                                    <{foreach name=group2 item=g2 from=$group2_list[$g1.group1]}>
                                        <{foreach name=asm item=asm from=$assessment_list[$g1.group1][$g2.group2]}>
                                            <tr>
                                                <{if $smarty.foreach.asm.first}>
                                                    <td rowspan="<{$g2.count|escape}>"><{$g2.name|escape}></td>
                                                <{/if}>
                                                <td class="bar"><{$asm[1].guideline|escape}><span class="bar<{$asm[1].assessment|escape}>"></span></td>
                                                    <{foreach item=d from=$date_list}>
                                                        <{assign var=ass value=$asm[$d.number].assessment}>
                                                        <{assign var=fass value=$asm[$d.number].f_assessment}>
                                                    <td nowrap class="ldr-td-level a<{$d.number|escape}>" data-ass="<{$ass}>"><{if $novice_number === '1'}><{$roman_num[$ass]}><{else}><{$ass}><{/if}></td>
                                                    <td nowrap class="ldr-td-level f<{$d.number|escape}>" data-ass="<{$fass}>"><{if $novice_number === '1'}><{$roman_num[$fass]}><{else}><{$fass}><{/if}></td>
                                                <{/foreach}>
                                            </tr>
                                        <{/foreach}>
                                    <{/foreach}>
                                </tbody>
                            </table>
                            <hr/>
                        </div>
                    <{/foreach}>
                </div>
            </div>
        </div>
        <div id="chart" class="modal hide fade" style="width:1000px; left:0; margin-left:200px;" tabindex="-1" role="dialog" aria-labelledby="chartLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icon-remove-circle"></i></button>
                <h3 id="chartLabel">チャート</h3>
            </div>
            <div id="modalBody">
                <ul class="nav nav-tabs">
                    <{foreach name=d1 item=d from=$date_list}>
                        <li class="<{if $smarty.foreach.d1.first}>active<{/if}>">
                            <a href="#nab<{$d.number|escape}>" data-toggle="tab">第<{$d.number}>回</a>
                        </li>
                    <{/foreach}>
                </ul>
                <div class="tab-content">
                    <{foreach name=d2 item=d from=$date_list}>
                        <div class="tab-pane <{if $smarty.foreach.d2.first}>active<{/if}>" id="nab<{$d.number|escape}>">
                            <img style="vertical-align:text-top;" src="novice_chart.php?emp_id=<{$novice->emp_id()|escape}>&year=<{$year|escape}>&number=<{$d.number|escape}>" width="950" height="500" border="0" />
                            <button class="btn btn-small btn-primary js-novice-chart-print" data-emp="<{$novice->emp_id()|escape}>" data-number="<{$d.number|escape}>"><i class="icon-print"></i></button>
                        </div>
                    <{/foreach}>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-small" data-dismiss="modal" aria-hidden="true"><i class="icon-remove-sign"></i> 閉じる</button>
            </div>
        </div>

        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {
                /**
                 * 印刷
                 */
                $('.js-novice-sheet-print').click(function() {
                    $('<form action="" method="post" target="sheet" onsubmit="return openPDF(this);"></form>')
                        .append($('<input></input>').attr({type: 'hidden', name: 'emp_id', value: $(this).data('emp')}))
                        .append($('<input></input>').attr({type: 'hidden', name: 'link', value: $(this).data('target')}))
                        .append($('<input></input>').attr({type: 'hidden', name: 'mode', value: 'print'}))
                        .append($('<input></input>').attr({type: 'hidden', name: 'year', value: $('#year').val()}))
                        .appendTo(document.body)
                        .submit()
                        .remove();
                });

                /**
                 * チャート印刷
                 */
                $('.js-novice-chart-print').on('click', function() {
                    $('<form action="" method="post" target="chart" onsubmit="return openPDF(this);"></form>')
                        .append($('<input></input>').attr({type: 'hidden', name: 'emp_id', value: $(this).data('emp')}))
                        .append($('<input></input>').attr({type: 'hidden', name: 'number', value: $(this).data('number')}))
                        .append($('<input></input>').attr({type: 'hidden', name: 'mode', value: 'chart'}))
                        .append($('<input></input>').attr({type: 'hidden', name: 'year', value: $('#year').val()}))
                        .appendTo(document.body)
                        .submit()
                        .remove();
                });

                /**
                 * 年プルダウン
                 */
                $('#year').change(function() {
                    $('<form action="" method="post"></form>')
                        .append($('<input></input>').attr({type: 'hidden', name: 'emp_id', value: $(this).data('emp')}))
                        .append($('<input></input>').attr({type: 'hidden', name: 'year', value: $('#year').val()}))
                        .append($('<input></input>').attr({type: 'hidden', name: 'menu', value: $(this).data('menu')}))
                        .append($('<input></input>').attr({type: 'hidden', name: 'owner_url', value: $(this).data('url')}))
                        .appendTo(document.body)
                        .submit()
                        .remove();
                });

                /**
                 * データバーの切り替え
                 */
                $('.js-bar-change').on('click', function() {
                    var target = $(this).data('target');
                    $('#btn-print').data('target', target);
                    $('.' + target).each(function() {
                        $(this).parent().find('td.bar>span').prop('class', 'bar' + $(this).data('ass'));
                    });
                });
            });
        </script>
    </body>
</html>
