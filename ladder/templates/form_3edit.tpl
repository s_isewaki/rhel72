<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | <{$titles.form_3.title|escape}>入力</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title="`$titles.form_3.title`入力" hd_sub_title=$titles.form_3.detailL}>

            <div class="container-fluid">
                <div class="well well-small">
                    <form id="form_edit" action="form_3edit.php" method="post">
                        <input type="hidden" name="inner_id" value="" />
                        <{if $not_found === true}>
                            <div class="alert alert-error">記録が見つかりませんでした</div>
                            <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                        <{else}>
                            <div class="control-group">
                                <label class="control-label" for="">
                                    <strong>年月日</strong>
                                    <span class="badge badge-important required">必須</span>
                                </label>
                                <div class="controls">
                                    <{include file="__datepicker.tpl" date_id="period" date_remove="off" date_name="period" date_option = 'required'}>
                                    <{foreach from=$error.period item=msg}>
                                        <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                    <{/foreach}>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="type">
                                    <strong>種別</strong>
                                </label>
                                <div class="form-inline">
                                    <{foreach from=$types key=i item=type}>
                                        <label class="radio">
                                            <input type="radio" name="type" value="<{$i|escape}>"><strong><{$type|escape}></strong>
                                        </label>
                                    <{/foreach}>
                                </div>
                                <{foreach from=$error.type item=msg}>
                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                <{/foreach}>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="title">
                                    <strong>学会名</strong>
                                    <span class="badge badge-important required">必須</span>
                                </label>
                                <div class="controls">
                                    <div class="input-append">
                                        <input type="text" id="title" name="title"
                                               class="input-xlarge <{if $items.3title.use}>js-typeahead-item<{/if}>"
                                               data-item-type="3title"
                                               <{if $items.3title.use && !$items.3title.free}>readonly="readonly"<{/if}>
                                               minlength="2" maxlength="200" value="" required="required"/>
                                        <span class="add-on js-ldr-erase"><a href="javascript:void(0);"><i class="icon-eraser"></i></a></span>
                                    </div>
                                    <{foreach from=$error.title item=msg}>
                                        <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                    <{/foreach}>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="organizer">
                                    <strong>主催</strong>
                                    <span class="badge badge-important required">必須</span>
                                </label>
                                <div class="controls">
                                    <div class="input-append">
                                        <input type="text" id="organizer" name="organizer"
                                               class="input-xlarge <{if $items.3organizer.use}>js-typeahead-item<{/if}>"
                                               data-item-type="3organizer"
                                               <{if $items.3organizer.use && !$items.3organizer.free}>readonly="readonly"<{/if}>
                                               minlength="2" maxlength="200" value="" required="required"/>
                                        <span class="add-on js-ldr-erase"><a href="javascript:void(0);"><i class="icon-eraser"></i></a></span>
                                    </div>
                                    <{foreach from=$error.organizer item=msg}>
                                        <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                    <{/foreach}>
                                </div>
                            </div>
                            <div class="btn-toolbar">
                                <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                                <button type="submit" name="btn_save" id="btn_save" class="btn btn-small btn-primary" value="btn_save"><i class="icon-save"></i> 保存する</button>
                            </div>
                        <{/if}>
                    </form>
                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>

    </body>
</html>