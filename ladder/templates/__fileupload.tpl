<div class="modal large hide fade" id="upload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form action="#" method="post" id="__fileupload">
        <input type="hidden" name="mode" value="<{$up_mode}>" />
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i></button>
            <h3 id="myModalLabel"><{$up_title}></h3>
        </div>
        <div class="modal-body">
            <div id="form_import" class="control-group">
                <label class="control-label" for=""><strong><{$file_nav}></strong></label>
                <div class="controls">
                    <input style="width:100%" id="inp_upload" onchange="file_change();" name="csvfile" type="file" />
                </div>
            </div>
            <div id="alert_csv" class="alert" style="overflow:auto; display: none;"></div>
            <{if $layout}>
                <hr />
                <{$layout}>
            <{/if}>
        </div>
    </form>
    <div class="modal-footer">
        <button type="button" id="btn_import" class="btn btn-small btn-primary"><i class="icon-cloud-upload"></i> インポート</button>
        <button type="button" id="btn_close" class="btn btn-small" data-dismiss="modal" aria-hidden="true"><i class="icon-remove-sign"></i> 閉じる</button>
    </div>
</div>

<{javascript src="../js/jquery/jquery-upload-1.0.2.min.js"}>
<script type="text/javascript">
    function file_change() {
        $('#btn_import').prop("disabled", false);
        $('#alert_csv').html('').hide().removeClass('alert-error alert-info');
    }
    $(function() {
        $('.ldr_upload').click(function() {
            $("#inp_upload").replaceWith('<input type="file" style="width:100%" id="inp_upload" onchange="file_change();" name="csvfile">');
            $('#btn_import').prop("disabled", false);
            $('#alert_csv').html('').hide().removeClass('alert-error alert-info');
        });
        $('#btn_import').click(function() {
            $('#alert_csv')
                .hide()
                .css('height', '')
                .removeClass('alert-error alert-info');

            if (!$('#inp_upload').val()) {
                $('#alert_csv')
                    .html('<h4><i class="icon-warning-sign"></i> ファイルを選択してください</h4>')
                    .addClass('alert-error')
                    .show();
                return false;
            }

            $('#form_import').upload('<{$up_php}>', $('#__fileupload').serialize(), function(res) {
                if (res.error) {
                    var result = '';
                    if (res.result !== void 0) {
                        $.each(res.result, function(i, data) {
                            result += '<li>' + data + '</li>';
                        });
                    }
                    $('#alert_csv')
                        .html('<h4><i class="icon-warning-sign"></i> ' + res.msg + '</h4>' + (result ? '<ul>' + result + '</ul>' : ''))
                        .addClass('alert-error')
                        .show();
                }
                else {
                    $('#btn_import').prop("disabled", true);
                    $('#alert_csv')
                        .html('<h4><i class="icon-thumbs-up"></i> ' + res.msg + '</h4>')
                        .addClass('alert-info')
                        .show();
                }
            }, 'json');

            return false;
        });

    });
</script>
