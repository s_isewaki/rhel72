<{if $group1.comment}>
    <div style="margin:5px; padding: 5px 10px;"><i class="icon-folder-open"></i> <b><{$group1.comment|escape}></b></div>
<{/if}>

<ul class="nav nav-tabs" id="group2Tab">
    <{foreach item=row from=$group2_list}>
        <li class="<{if $active === "g2-`$row.guideline_group2`"}>active<{/if}>"><a href="#g2-<{$row.guideline_group2|escape}>" data-toggle="tab"><i class="icon-folder-close-alt"></i> <{$row.name|escape}></a></li>
        <{/foreach}>
</ul>

<div class="tab-content">
    <{foreach item=row from=$group2_list}>
        <div class="tab-pane <{if $active === "g2-`$row.guideline_group2`"}>active<{else}>fade<{/if}>" id="g2-<{$row.guideline_group2|escape}>">
            <{if $row.comment[$level]}>
                <div style="margin:5px; padding: 5px 10px;"><i class="icon-folder-open-alt"></i> <b><{$row.comment[$level]|escape}></b></div>
            <{/if}>

            <form action="assessment.php" method="post" class="js-form-assessment">
                <input type="hidden" name="mode" value="save" />
                <input type="hidden" name="level" value="<{$level|escape}>" />
                <input type="hidden" name="year" value="<{$year|escape}>" />
                <input type="hidden" name="g1" value="g1-<{$row.guideline_group1|escape}>" />
                <input type="hidden" name="g2" value="g2-<{$row.guideline_group2|escape}>" />
                <div class="well well-small">
                    <{foreach item=asm from=$assessment[$row.guideline_group2]}>
                        <{assign var='asmkey' value="_`$asm.assessment`"}>
                        <div id="as-<{$asm.guideline_id|escape}>" class="inline">
                            <div class="pull-left">
                                <input type="hidden" id="in-<{$asm.guideline_id|escape}>" name="assessment[<{$asm.guideline_id|escape}>]" value="<{$asm.assessment|escape}>" />
                                <{if $assessment_select|@count > 2}>
                                    <div class="btn-group" data-toggle="buttons-radio" style="margin-top:5px;">
                                        <{foreach item=sel from=$assessment_select}>
                                            <button type="button"
                                                    class="btn btn-medium js-radio-assessment <{if $asm.assessment === $sel.value}>btn-<{$sel.color}> active<{/if}>"
                                                    name="assessment[<{$asm.guideline_id|escape}>]"
                                                    id="<{$asm.guideline_id|escape}>"
                                                    data-input="#in-<{$asm.guideline_id|escape}>"
                                                    data-alert="#al-<{$asm.guideline_id|escape}>"
                                                    data-trigger="manual"
                                                    data-placement="bottom"
                                                    data-container="#as-<{$asm.guideline_id|escape}>"
                                                    data-title="評価してください"
                                                    data-color="<{$sel.color|escape}>"
                                                    value="<{$sel.value|escape}>"
                                                    >
                                                <{$sel.button|escape}>
                                            </button>
                                        <{/foreach}>
                                    </div>
                                <{else}>
                                    <button
                                        type="button"
                                        id="bt-<{$asm.guideline_id|escape}>"
                                        class="btn btn-large js-button-assessment btn-<{$assessment_select[$asmkey].color|default:'default'}>"
                                        data-input="#in-<{$asm.guideline_id|escape}>"
                                        data-alert="#al-<{$asm.guideline_id|escape}>"
                                        data-trigger="manual"
                                        data-placement="bottom"
                                        data-container="#as-<{$asm.guideline_id|escape}>"
                                        data-title="評価してください"
                                        rel="tooltip">
                                        <{$assessment_select[$asmkey].button|default:'<i class="icon-question"></i>'}>
                                    </button>
                                <{/if}>

                            </div>
                            <div class="clearfix" style="padding: 1px 0 0 <{$padleft|escape}>;">
                                <div
                                    id="al-<{$asm.guideline_id|escape}>"
                                    class="alert alert-<{$assessment_select[$asmkey].color|default:'default'}>">
                                    <{$asm.guideline|escape|nl2br}>
                                </div>
                                <{if $asm.criterion}>
                                    <label class="control-label"><strong>判断基準</strong></label>
                                    <p class="alert alert-default"><{$asm.criterion|escape|nl2br}></p>
                                <{/if}>
                                <label class="control-label"><strong>根拠</strong></label>
                                <textarea class="input-maxlarge js-autoheight" name="reason[<{$asm.guideline_id|escape}>]" placeholder="根拠を入力してください"><{$asm.reason|escape}></textarea>
                            </div>
                        </div>
                        <hr/>
                    <{/foreach}>
                    <div class="btn-toolbar">
                        <button type="submit" name="btn_save" id="btn_save" class="btn btn-small btn-primary" value="btn_save"><i class="icon-save"></i> 保存する</button>
                    </div>
                    <p><small><i class="icon-exclamation-sign"></i> 入力内容は自動保存されます。明示的に保存したい場合は<b>[保存する]ボタン</b>で保存してください。</small></p>
                </div>
            </form>
        </div>
    <{/foreach}>
</div>
<script type="text/javascript">
    $('textarea.js-autoheight').click();
</script>