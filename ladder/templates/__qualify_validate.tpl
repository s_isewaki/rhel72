<{if isset($error_qualify.form1)}>
    <div class="control-group">
        <div class="alert alert-error">
            <button type="button" data-details="#form1_err" class="form_err_btn btn btn-danger btn-mini"><i class="icon-plus"></i></button>
            <strong><{$ldr.config.form_1.btn|escape}> に未入力の項目が存在します。</strong>

            <button type="button" data-id="<{$smarty.post.id}>" data-emp="<{$target_emp_id}>" data-php="apply_entry.php" data-mode="edit" class="js-form-button btn btn-small btn-danger" ><i class="icon-pencil"></i> 編集する</button>
            <div id="form1_err" class="control-group">
                <hr/>
                <{foreach from=$error_qualify.form1 item=value}>
                    <{foreach from=$value item=msg}>
                        <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                    <{/foreach}>
                <{/foreach}>
            </div>
        </div>
    </div>
<{/if}>

<{if isset($error_qualify.form6)}>
    <div class="control-group">
        <div class="alert alert-error">
            <button type="button" data-details="#form6_err" class="form_err_btn btn btn-danger btn-mini"><i class="icon-plus"></i></button>
            <strong><{$ldr.config.form_6.btn|escape}> に未入力の項目が存在します。</strong>
            <button type="button" data-id="<{$smarty.post.id}>" data-emp="<{$target_emp_id}>" data-php="form_6edit.php" data-mode="edit" class="js-form-button btn btn-small btn-danger" ><i class="icon-pencil"></i> 編集する</button>
            <div id="form6_err" class="control-group">
                <hr/>
                <{foreach from=$error_qualify.form6 item=value}>
                    <{foreach from=$value item=msg}>
                        <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                    <{/foreach}>
                <{/foreach}>
            </div>
        </div>
    </div>
<{/if}>

<{if isset($error_qualify.form7)}>
    <div class="control-group">
        <div class="alert alert-error">
            <button type="button" data-details="#form7_err" class="form_err_btn btn btn-danger btn-mini"><i class="icon-plus"></i></button>
            <strong><{$ldr.config.form_7.btn|escape}> に未入力の項目が存在します。</strong>
            <button type="button" data-id="<{$smarty.post.id}>" data-emp="<{$target_emp_id}>" data-php="form_7edit.php" data-mode="edit" class="js-form-button btn btn-small btn-danger" ><i class="icon-pencil"></i> 編集する</button>
            <div id="form7_err" class="control-group">
                <hr/>
                <{foreach from=$error_qualify.form7 item=value}>
                    <{foreach from=$value item=msg}>
                        <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                    <{/foreach}>
                <{/foreach}>
            </div>
        </div>
    </div>
<{/if}>