<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 院内研修 申込済一覧</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='院内研修 申込済一覧' hd_sub_title=''}>
            <{assign var='today' value=$smarty.now|date_format:"%Y-%m-%d"|escape}>

            <div class="container-fluid">
                <form method="post" id="inside_form">
                    <div class="form-inline">
                        <select name="srh_year" class="input-medium js-pulldown-inside">
                            <{foreach item=year from=$year_list}>
                                <option value="<{$year|escape}>"><{$year|escape}>年度</option>
                            <{/foreach}>
                        </select>
                        <select name="srh_training" class="input-large js-pulldown-inside">
                            <option value="1">これからの研修</option>
                            <option value="2">すべての研修</option>
                            <option value="4"><{$ojt_name|escape}></option>
                            <option value="3">振り返り未入力の研修</option>
                        </select>
                        <input type="text" id="srh_keyword" name="srh_keyword" class="input-large" value='<{$srh_keyword|escape}>' placeholder="検索ワードを入力してください" />
                        <button type="submit" id="btn_search" name="search" value="on" class="btn btn-small btn-primary"><i class="icon-search"></i> 検索する</button>
                        <br/>
                    </div>
                    <{if $list}>
                        <div class="control-group">
                            <{include file="__pager.tpl"}>
                        </div>
                        <table class="table table-bordered table-hover table-condensed ldr-table">
                            <thead>
                                <tr>
                                    <th>開催日時</th>
                                    <th>開催<wbr/>場所</th>
                                    <th>(研修番号) <br/>研修名</th>
                                    <th>研修回<br/>(回数)</th>
                                    <th width="35px"><button type="button" id="js-btn-training" class="btn btn-mini"><i class="icon-check-sign"></i></button></th>
                                    <th>師長<wbr/>承認</th>
                                    <th>受付</th>
                                    <th>前課題</th>
                                    <th>後課題</th>
                                    <th>振り返り</th>
                                </tr>
                            </thead>
                            <tbody>
                                <{foreach item=row from=$list}>
                                    <tr>
                                        <{if $row.type === '1'}>
                                            <td class="ldr-td-date"><span class="ldr-td-date-bold" title="<{$row.training_date|date_format_jp:"%Y年%-m月%-d日(%a)"}>"><{$row.training_date|date_format_jp:"%-m月%-d日(%a)"}></span><br/><{$row.training_start_time|date_format:"%R"}>〜<{$row.training_last_time|date_format:"%R"}></td>
                                            <td class="ldr-td-text-center"><{$row.place|escape}></td>
                                        <{else}>
                                            <td class="ldr-td-name" colspan="2"><span class="label label-success"><{$ojt_name|escape}></span></td>
                                            <{/if}>
                                        <td class="ldr-td-text-left">
                                            <a
                                                href="trn_inside_contents_modal.php?id=<{$row.inside_id|escape}>"
                                                data-toggle="modal"
                                                data-backdrop="true"
                                                data-target="#inside_contents">
                                                <i class="icon-external-link-sign"></i> <{if $row.no}>(<{$row.no|escape}>)<br/><{/if}>
                                                <{$row.title|escape}>
                                            </a>
                                        </td>
                                        <td class="ldr-td-name">
                                            <a
                                                class="btn btn-small btn-success"
                                                href="trn_inside_number_modal.php?id=<{$row.inside_id|escape}>&number=<{$row.number|escape}>&date_id=<{$row.inside_date_id|escape}>"
                                                data-toggle="modal"
                                                data-backdrop="true"
                                                data-target="#inside_number">
                                                <i class="icon-external-link"></i> 第<{$row.number|escape}>回
                                            </a>
                                            <br/>(全<{$row.number_of|escape}>回)
                                        </td>
                                        <td class="ldr-td-name">
                                            <{if ($row.approve === 't' and $row.status==='1') or ($row.approve === 'f' and $row.status==='2')}>
                                                <label>
                                                    <input type="checkbox" class="ck-training" name="training_id[]" value="<{$row.training_id}>" />
                                                </label>
                                            <{elseif $row.status==='-1'}>
                                                <span class="label label-warning"><strong>取下</strong></span>
                                            <{else}>
                                                -
                                            <{/if}>
                                        </td>
                                        <td class="ldr-td-name">
                                            <{if $row.approve === 't'}>
                                                <{if $row.status === '1'}>
                                                    <span class="label label-important"><strong>待ち</strong></span>
                                                <{elseif $row.status==='0' or $row.status >= '2' or $row.status < '-2'}>
                                                    <span class="label label-info"><strong>済</strong></span>
                                                <{elseif $row.status === '-2'}>
                                                    <span class="label label-warning"><strong>取下</strong></span>
                                                <{/if}>
                                            <{else}>
                                                <{if $row.status==='0' or $row.status >= '2' or $row.status <= '-2'}>
                                                    -
                                                <{/if}>
                                            <{/if}>
                                        </td>
                                        <td class="ldr-td-name">
                                            <{if $row.status === '2'}>
                                                <span class="label label-important"><strong>待ち</strong></span>
                                            <{elseif $row.status==='0' or $row.status >= '3'  or $row.status < '-3'}>
                                                <span class="label label-info"><strong>済</strong></span>
                                            <{elseif $row.status === '-3'}>
                                                <span class="label label-warning"><strong>取下</strong></span>
                                            <{else}>

                                            <{/if}>
                                        </td>
                                        <td class="ldr-td-name">
                                            <{if $row.status >= '0' and $row.before_work === 't'}>
                                                <{if $row.status!=='0' and $row.status < '3'}>
                                                    <{assign var='st_b' value='disabled'}>
                                                <{else}>
                                                    <{assign var='st_b' value=''}>
                                                <{/if}>

                                                <{if $row.before_status === '1'}>
                                                    <button type="button" data-php="trn_inside_work_edit.php" data-id="<{$row.training_id}>" data-mode = "before" class="js-form-button btn btn-small btn-danger" <{$st_b|escape}>>修正</button>
                                                <{else}>
                                                    <button type="button" data-php="trn_inside_work_edit.php" data-id="<{$row.training_id}>" data-mode = "before" class="js-form-button btn btn-small btn-danger" <{$st_b|escape}>>入力</button>
                                                <{/if}>
                                            <{else}>
                                                -
                                            <{/if}>
                                        </td>
                                        <{if ($row.status!=='0' and $row.status < '3') or $row.training_date > $today}>
                                            <{assign var='st_a' value='disabled'}>
                                        <{else}>
                                            <{assign var='st_a' value=''}>
                                        <{/if}>
                                        <td class="ldr-td-name">
                                            <{if $row.status >= '0' and $row.after_work === 't'}>
                                                <{if $row.after_status === '1'}>
                                                    <button type="button" data-php="trn_inside_work_edit.php" data-id="<{$row.training_id}>" data-mode = "after" class="js-form-button btn btn-small btn-danger" <{$st_a|escape}>>修正</button>
                                                <{else}>
                                                    <button type="button" data-php="trn_inside_work_edit.php" data-id="<{$row.training_id}>" data-mode = "after" class="js-form-button btn btn-small btn-danger" <{$st_a|escape}>>入力</button>

                                                <{/if}>
                                            <{else}>
                                                -
                                            <{/if}>
                                        </td>
                                        <td class="ldr-td-name">
                                            <{if $row.status >= '0' and $row.looking_back==='t'}>
                                                <{if $row.check_status === '1'}>
                                                    <button type="button" data-php="trn_inside_check_edit.php" data-id="<{$row.training_id}>" data-mode = "after" class="js-form-button btn btn-small btn-danger" <{$st_a|escape}>>修正</button>
                                                <{else}>
                                                    <button type="button" data-php="trn_inside_check_edit.php" data-id="<{$row.training_id}>" data-mode = "after" class="js-form-button btn btn-small btn-danger" <{$st_a|escape}>>入力</button>
                                                <{/if}>

                                            <{else}>
                                                -
                                            <{/if}>
                                        </td>
                                    </tr>
                                <{/foreach}>
                            </tbody>
                        </table>
                        <div class="btn-toolbar">

                            <button type="submit" name="btn_save" id="btn_save" class="js-ot-btn js-submit-withdraw btn btn-small btn-danger" value="cancel"><i class="icon-remove-sign"></i> 取り下げる</button>
                        </div>
                    <{else}>
                        <div class="control-group">
                        </div>
                        <div class="alert alert-error">院内研修の申請はありません</div>
                    <{/if}>
                </form>
            </div>
        </div>
        <{include file="__modal_view.tpl" modal_id="inside_contents" modal_title="研修内容"}>
        <{include file="__modal_view.tpl" modal_id="inside_number" modal_title="研修内容"}>

        <{include file="__js.tpl"}>
        <{include form_selector="#inside_form" file="__pager_js.tpl"}>
        <script type="text/javascript">
            $(function() {
                check_status();

                $(".js-ot-btn").prop("disabled", true);
                $('#srh_keyword').keypress(function(ev) {
                    if ((ev.which && ev.which === 13) || (ev.keyCode && ev.keyCode === 13)) {
                        $('#search').trigger("click");
                    }
                });
                $(".srh").change(function() {
                    $(this).parents('form')
                        .append($('<input></input>').attr({type: 'hidden', name: 'search', value: 'true'}))
                        .submit();
                });
                $(".ck-training").change(function() {
                    check_status();
                });

                $('#js-btn-training').click(function() {
                    if ($(this).children('i').hasClass('icon-check-sign')) {
                        $('input.ck-training').prop('checked', true);
                    }
                    else {
                        $('input.ck-training').prop('checked', false);
                    }
                    $(this).children('i').toggleClass('icon-check-sign').toggleClass('icon-check-empty');
                    check_status();
                });

                $('#srh_keyword').keypress(function(ev) {
                    if ((ev.which && ev.which === 13) || (ev.keyCode && ev.keyCode === 13)) {
                        $('#btn_search').trigger("click");
                    }
                });
                // プルダウン選択
                $('.js-pulldown-inside').change(function() {
                    $('#inside_form')
                        .append($('<input></input>').attr({type: 'hidden', name: 'search', value: 'on'}))
                        .submit();
                });
            });
            function check_status() {
                if ($(".ck-training:checked").length > 0) {
                    $(".js-ot-btn").prop("disabled", false);
                } else {
                    $(".js-ot-btn").prop("disabled", true);
                }
            }
        </script>
    </body>
</html>
