<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 振り返りテンプレート</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='振り返りテンプレート' hd_sub_title=''}>

            <div class="container-fluid">
                <div class="row-fluid">
                    <div class="row-fluid">
                        <form method="post" id="outside_form">
                            <div class="control-group">
                                <button type="button" data-php="adm_check_template_edit.php" data-mode="new" class="js-form-button btn btn-small btn-primary"><i class="icon-plus"></i> テンプレートを作成する</button>
                            </div>

                            <{if $list}>
                                <table class="table table-bordered table-hover table-condensed ldr-table">
                                    <thead>
                                        <tr>
                                            <th>テンプレート</th>
                                            <th>更新日</th>
                                            <th>プレビュー</th>
                                            <th>編集</th>
                                            <th>削除</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <{foreach item=row from=$list}>
                                            <tr>
                                                <td class="ldr-td-name"><{$row.title|escape}></td>
                                                <td class="ldr-td-date"><{$row.updated_on|date_format_jp:"%Y年%-m月%-d日(%a) %R"}></td>
                                                <td class="ldr-td-name">
                                                    <button type="button" data-php="adm_check_template_preview.php" data-id="<{$row.tmpl_id|escape}>" class="js-form-button btn btn-small btn-primary">プレビュー</button>
                                                </td>
                                                <td class="ldr-td-name">
                                                    <button type="button" data-php="adm_check_template_edit.php" data-mode="edit" data-id="<{$row.tmpl_id|escape}>" class="js-form-button btn btn-small btn-danger">編集</button>
                                                </td>
                                                <td class="ldr-td-name">
                                                    <button type="button" data-php="adm_check_template.php" data-mode="remove" data-id="<{$row.tmpl_id|escape}>" class="js-row-remove btn btn-small btn-danger">削除</button>
                                                </td>
                                            </tr>
                                        <{/foreach}>
                                    </tbody>
                                </table>

                            <{else}>
                                <div class="control-group">
                                    <{*余白調整*}>
                                </div>
                                <div class="alert alert-info">テンプレートはありません</div>
                            <{/if}>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {
                //項目の削除
                $('.js-row-remove').click(function() {
                    if (!confirm('削除してもよろしいですか？')) {
                        return false;
                    }
                    $(this).parents('form')
                        .attr({method: 'post', action: $(this).data('php')})
                        .append($('<input></input>').attr({type: 'hidden', name: 'id', value: $(this).data('id')}))
                        .append($('<input></input>').attr({type: 'hidden', name: 'mode', value: $(this).data('mode')}))
                        .submit();
                });
            });
        </script>
    </body>
</html>