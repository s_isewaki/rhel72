<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | <{$hd_title}></title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_sub_title=''}>

            <div class="container-fluid">
                <form method="post" id="inside_form">
                    <input type="hidden" id="mode" name="mode" value="" />
                    <input type="hidden" id="id" name="id" value="" />
                    <input type="hidden" id="title" name="title" value="" />
                    <input type="hidden" id="training_id" name="training_id" value="" />
                    <input type="hidden" id="training_date" name="training_date" value="" />
                    <input type="hidden" id="type" name="type" value="" />
                    <input type="hidden" id="check_status" name="check_status" value="" />
                    <div class="well well-small">
                        <fieldset>
                            <legend>
                                <a
                                    href="trn_inside_contents_modal.php?id=<{$trn.inside_id|escape}>"
                                    data-toggle="modal"
                                    data-backdrop="true"
                                    data-target="#inside_contents">
                                    <{if $trn.no}>(<{$trn.no|escape}>) <{/if}><{$trn.title|escape}>
                                </a>
                                <a
                                    href="trn_inside_number_modal.php?id=<{$trn.inside_id|escape}>&number=<{$trn.number|escape}>&date_id=<{$trn.inside_date_id|escape}>"
                                    data-toggle="modal"
                                    data-backdrop="true"
                                    data-target="#inside_number">
                                    第<{$trn.number|escape}>回 / 全<{$trn.number_of|escape}>回
                                </a>
                            </legend>
                            <{if $form}>
                                <{include file="adm_check/__form.tpl"}>
                                <div class="btn-toolbar">
                                    <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                                    <{if $check_status==='1'}>
                                        <button type="submit" name="btn_save" id="btn_save" class="btn btn-small btn-primary js-submit-work" value="btn_completed"><i class="icon-save"></i> 修正する</button>
                                    <{else}>
                                        <button type="submit" name="btn_save" id="btn_save" class="js-save btn btn-small btn-primary" value="btn_save"><i class="icon-save"></i> 一時保存する</button>
                                        <button type="submit" name="btn_save" id="btn_save" class="js-save btn btn-small btn-primary js-submit-check" value="btn_completed"><i class="icon-save"></i> 提出する</button>
                                    <{/if}>
                                </div>

                            <{else}>
                                <div class="alert alert-error">振り返りが設定されていません。管理者にお問い合わせください。</div>
                                <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>

                            <{/if}>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
        <{include file="__modal_view.tpl" modal_id="inside_contents" modal_title="研修内容"}>
        <{include file="__modal_view.tpl" modal_id="inside_number" modal_title="研修内容"}>
        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {

            });
        </script>
    </body>
</html>
