<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 新人設定</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title="新人設定"}>

            <div class="container-fluid">
                <div class="row-fluid">
                    <{include file="__save_message.tpl" }>
                    <form class="form" id="form_novice" method="POST">

                        <p>新人評価を行う職員を選択してください。</p>

                        <div class="form-inline">
                            <select id="pulldown_year" name="year" class="input-medium js-pulldown-novice">
                                <{foreach item=asyear from=$year_list}>
                                    <option value="<{$asyear|escape}>" <{if $year == $asyear}>selected<{/if}>><{$asyear|escape}>年度</option>
                                <{/foreach}>
                            </select>
                            <button type="button" data-id="novice" class="js-btn-emplist btn btn-small btn-success"><i class="icon-book"></i> 職員名簿</button>
                            <a href="#upload" data-toggle="modal" class="btn btn-small btn-danger ldr_upload"><i class="icon-cloud-upload"></i> CSVから職員をインポートする</a>
                        </div>
                        <hr/>

                        <table class="table table-striped table-hover <{if ! $novice_list}>hide<{/if}>">
                            <thead>
                                <tr>
                                    <th>新人職員</th>
                                    <th>部署</th>
                                    <th>入職日</th>
                                    <th class="pull-right"><i class='icon-remove'></i></th>
                                </tr>
                            </thead>
                            <tbody id="emp_list">
                                <{foreach item=row from=$novice_list}>
                                    <tr id="e<{$row.emp_id|escape}>">
                                        <td>
                                            <{$row.name|escape}>
                                            <input type="hidden" name="emp_id[]" value="<{$row.emp_id|escape}>" />
                                        </td>
                                        <td>
                                            <{$row.emp->class_full_name()|escape}>
                                        </td>
                                        <td>
                                            <{$row.emp->join_date()|regex_replace:'/^(\d\d\d\d)(\d\d)(\d\d)/':'$1-$2-$3'|date_format_jp:"%Y年%-m月%-d日"}>
                                        </td>
                                        <td><a class="pull-right js-row-remove" href="javascript:void(0);" ><i class="icon-remove"></i></a></td>
                                    </tr>
                                <{/foreach}>
                            </tbody>
                        </table>

                        <{foreach from=$error.erroe item=msg}>
                            <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                        <{/foreach}>

                        <div class="btn-toolbar">
                            <button type="submit" name="btn_save" id="btn_save" class="btn btn-small btn-primary" value="btn_save"><i class="icon-save"></i> 保存する</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
        <{capture name=layout}>
            <label class="control-label"><strong>CSVレイアウト</strong></label>
            <table class="table table-condensed table-bordered table-striped ldr-table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>名称</th>
                        <th>必須</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="ldr-td-number">1</td>
                        <td>職員ID</td>
                        <td>必須</td>
                    </tr>
                    <tr>
                        <td class="ldr-td-number">2</td>
                        <td>職員名</td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        <{/capture}>
        <{include file="__js.tpl"}>
        <{include up_title="CSVから職員をインポートする" up_mode="import" file_nav="CSVファイル" up_php="adm_novice.php?year=`$year`" file="__fileupload.tpl" layout=$smarty.capture.layout}>
        <script type="text/javascript">
            $(function() {
                // 職員の削除
                $('body').on('click', '.js-row-remove', function() {
                    if (!confirm('削除してもよろしいですか？')) {
                        return false;
                    }
                    $(this).parent().parent().remove();
                });

                // プルダウン選択
                $('.js-pulldown-novice').change(function() {
                    $('#form_novice').submit();
                });

                /**
                 * アップロード閉じるイベント(画面リロード)
                 */
                $('#upload').on('hidden', function() {
                    $('<form action="" method="post"></form>')
                        .append($('<input></input>').attr({type: 'hidden', name: 'year', value: $('#pulldown_year').val()}))
                        .appendTo(document.body)
                        .submit()
                        .remove();
                });
            });

            // 職員の追加
            function add_target_list(item_id, emp_id, emp_name) {
                var emp = emp_id.split(", ");
                if (emp.length !== 1) {
                    return;
                }
                if ($('#e' + emp_id).size() === 0) {
                    var add_input = '<input type="hidden" name="emp_id[]" value="' + emp_id + '" />';
                    $('#emp_list').append('<tr id="e' + emp_id + '"><td>' + emp_name + add_input + '</td><td></td><td></td><td><a class="pull-right js-row-remove" href="javascript:void(0);"><i class="icon-remove"></i></a></td></tr>');
                    $('#emp_list').parent().show();
                }
            }
        </script>
    </body>
</html>
