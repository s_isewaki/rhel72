<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 師長申込</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">

            <{include file="__header.tpl" hd_title='師長申込' hd_sub_title=''}>

            <div class="container-fluid">
                <form id="apply_form" method="post">
                    <input type="hidden" name="menu" value="apply" />
                    <input type="hidden" name="id" value="<{$id|escape}>" />
                    <input type="hidden" name="owner_url" value="apply_qualify.php" />
                    <input type="hidden" name="status" value="<{$apply->get_status()|escape}>" />
                    <input type="hidden" name="target_emp_id" value="<{$target_emp_id}>" />


                    <div class="btn-toolbar">
                        <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                    </div>

                    <{if $form1}>

                        <div class="block">
                            <div class="block-body collapse in">
                                <{include file="__form_btn.tpl"}>
                            </div>
                        </div>

                        <div class="well well-small">
                            <{include file="__qualify_validate.tpl"}>
                            <div class="control-group">
                                <label class="control-label"><strong>申請日</strong></label>
                                <div class="controls"><span class="ldr-uneditable input-medium"><{$form1.apply_date|date_format_jp:"%Y年%-m月%-d日(%a)"}></span></div>
                            </div>
                            <div class="control-group">
                                <label class="control-label"><strong>申請レベル</strong></label>
                                <div class="controls"><span class="ldr-uneditable input-mini ldr-level"><{$apply->level_text()|escape}></span></div>
                            </div>
                            <div class="control-group">
                                <label class="control-label"><strong>当面のあなたの目標</strong></label>
                                <div class="controls">
                                    <span class="ldr-uneditable input-xxlarge"><{$form1.objective|escape|nl2br}></span>
                                </div>
                            </div>

                            <{if $ldr.config.flow === '1' && $ldr.config.meeting === '1'}>
                                <div class="control-group">
                                    <label class="control-label"><strong>評価会日時</strong></label>
                                    <div class="controls">
                                        <span class="ldr-uneditable input-medium"><{$form1.meeting_date|default:'(未設定)'|escape}></span>
                                        <span class="ldr-uneditable input-mini"><{$form1.meeting_start_time|default:'(未設定)'|escape}></span> 〜
                                        <span class="ldr-uneditable input-mini"><{$form1.meeting_last_time|default:'(未設定)'|escape}></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label"><strong>評価会会場</strong></label>
                                    <div class="controls"><span class="ldr-uneditable"><{$form1.meeting_room|default:'(未設定)'|escape}></span></div>
                                </div>
                            <{/if}>

                            <{if $ldr.config.appr === '1'}>
                                <div class="control-group">
                                    <label class="control-label"><strong>評価者</strong></label>
                                    <div class="controls">
                                        <{foreach item=row from=$apply->appraiser_lists()}>
                                            <span class="ldr-uneditable input-medium"><{$row.emp_name|escape}><{if $row.facilitator === 't'}> (司会者)<{/if}></span>
                                        <{foreachelse}>
                                            <span class="ldr-uneditable input-medium">(未設定)</span>
                                        <{/foreach}>
                                    </div>
                                </div>
                            <{/if}>

                            <hr/>
                            <div class="control-group">
                                <label class="control-label" for="theme"><strong>承認者</strong></label>
                                <div class="controls">
                                    <{foreach item=row from=$apply->approver($target_emp_id)}>
                                        <span class="ldr-uneditable input-medium"><{$row->emp_name()|escape}></span>
                                    <{foreachelse}>
                                        <span class="alert alert-danger">承認者が設定されていません。管理者に連絡してください。</span>
                                    <{/foreach}>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="theme"><strong>コメント</strong></label>
                                <div class="controls">
                                    <textarea id="comment" name="comment" rows="2" class="input-xxlarge js-autoheight"></textarea>
                                    <{foreach from=$error.comment item=msg}>
                                        <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                    <{/foreach}>
                                </div>
                            </div>
                        </div>
                    <{else}>
                        <div class="alert alert-error">申請が見つかりません</div>
                    <{/if}>

                    <div class="btn-toolbar">
                        <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                        <{if $mode !== 'cancel' && $form1 && $apply->approver($target_emp_id)}>
                            <{if $mode === 'new' && !$error_qualify}>
                                <button type="submit" name="form_submit" class="btn btn-small btn-primary js-submit-apply" value="save"><i class="icon-edit"></i> 申請する</button>
                            <{/if}>
                            <{if $mode === 'retry'}>
                                <button type="submit" name="form_submit" class="btn btn-small btn-primary js-submit-apply" value="save"><i class="icon-edit"></i> 再申請する</button>
                            <{/if}>
                        <{/if}>
                        <button type="submit" name="form_submit" class="btn btn-small btn-danger js-submit-withdraw js-submit-cancel" value="cancel"><i class="icon-remove-sign"></i> 申請を取り下げる</button>
                    </div>
                </form>

            </div>
            <{include file="__js.tpl"}>
            <script type="text/javascript">
                $(function() {
                    $("#form7_err").css("display", "none");
                    $("#form1_err").css("display", "none");
                    $("#form6_err").css("display", "none");

                    $(".form_err_btn").click(function() {
                        $(this).find("i").toggleClass('icon-minus');
                        $(this).find("i").toggleClass('icon-plus');
                        $($(this).data("details")).toggle(300);
                    });

                    /*
                     * 取り下げの場合はコメント必須
                     */
                    $('.js-submit-cancel').click(function() {
                        if ($('#comment').val() === '') {
                            alert('取り下げの場合は、コメントを必ず入力してください');
                            return false;
                        }
                        return true;
                    })
                });
            </script>

    </body>
</html>
