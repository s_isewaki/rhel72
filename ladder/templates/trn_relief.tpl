<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 救護員研修 申込済一覧</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='救護員研修 申込済一覧' hd_sub_title=''}>

            <div class="container-fluid">
                <div class="row-fluid">
                    <form method="post" id="relief_form">
                        <{if $list}>
                            <div class="control-group">
                                <{include file="__pager.tpl"}>
                            </div>
                            <table class="table table-bordered table-hover table-condensed ldr-table">
                                <thead>
                                    <tr>
                                        <th>更新日時</th>
                                        <th>開催日</th>
                                        <th>支部</th>
                                        <th>施設</th>
                                        <th>科目</th>
                                        <th>申込</th>
                                        <th>師長<br />承認</th>
                                        <th>受付</th>
                                        <th>報告</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <{foreach item=row from=$list}>
                                        <tr>
                                            <td class="ldr-td-date"><{$row.updated_on|date_format_jp:"%Y年%-m月%-d日(%a) %R"|replace:' ':'<br/>'}></td>
                                            <td class="ldr-td-date"><{$row.training_day|date_format_jp:"%Y年%-m月%-d日(%a)"}></td>
                                            <td class="ldr-td-text-center"><{$row.branch|escape}></td>
                                            <td class="ldr-td-text-center"><{$row.hospital|escape}></td>
                                            <td class="ldr-td-text-center">
                                                <{$row.subject|escape}>&nbsp;<span class="ldr-roman"><{$roman_num[$row.unit]|escape}></span>
                                            </td>
                                            <td class="ldr-td-name">
                                                <{if $row.status === '1'}>
                                                    <button type="button" data-php="trn_relief_apply.php" data-mode="edit" data-id="<{$row.training_id}>" class="js-form-button btn btn-small btn-danger">修正</button>
                                                <{elseif $row.status === '-1'}>
                                                    <span class="label label-warning"><strong>取下</strong></span>
                                                <{else}>
                                                    <button type="button" data-php="trn_relief_apply_view.php" data-mode="view" data-id="<{$row.training_id}>" class="js-form-button btn btn-small btn-primary">済</button>
                                                <{/if}>
                                            </td>
                                            <td class="ldr-td-name">
                                                <{if $row.status === '1'}>
                                                    <span class="label label-important"><strong>待ち</strong></span>
                                                <{elseif $row.status==='0' or $row.status >= '2' or $row.status < '-2'}>
                                                    <span class="label label-info"><strong>済</strong></span>
                                                <{elseif $row.status === '-2'}>
                                                    <span class="label label-warning"><strong>取下</strong></span>
                                                <{/if}>
                                            </td>
                                            <td class="ldr-td-name">
                                                <{if $row.status === '2'}>
                                                    <span class="label label-important"><strong>待ち</strong></span>
                                                <{elseif $row.status==='0' or $row.status >= '3'  or $row.status < '-3'}>
                                                    <span class="label label-info"><strong>済</strong></span>
                                                <{elseif $row.status === '-3'}>
                                                    <span class="label label-warning"><strong>取下</strong></span>
                                                <{/if}>
                                            </td>
                                            <td class="ldr-td-name">
                                                <{if $row.status === '3'}>
                                                    <button type="button" data-php="trn_relief_report.php" data-mode="new" data-id="<{$row.training_id}>" class="js-form-button btn btn-small btn-danger">記入</button>
                                                <{elseif $row.status === '0'}>
                                                    <button type="button" data-php="trn_relief_report.php" data-mode="edit" data-id="<{$row.training_id}>" class="js-form-button btn btn-small btn-danger">修正</button>
                                                <{/if}>
                                            </td>
                                        </tr>
                                    <{/foreach}>
                                </tbody>
                            </table>

                        <{else}>
                            <div class="alert alert-error">救護員研修の申請はありません</div>
                        <{/if}>
                    </form>
                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <{include form_selector="#relief_form" file="__pager_js.tpl"}>

    </body>
</html>
