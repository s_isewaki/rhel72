<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 職員管理</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='職員管理' hd_sub_title=''}>

            <div class="container-fluid">
                <div class="row-fluid">

                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#search" data-toggle="tab"><i class="icon-search"></i> 検索</a></li>
                        <li><a href="#import" data-toggle="tab"><i class="icon-signin"></i> インポート</a></li>
                        <li><a href="#export" data-toggle="tab"><i class="icon-signout"></i> エクスポート</a></li>
                        <li><a href="#comedix" data-toggle="tab"><i class="icon-star"></i> CoMedix連携</a></li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active in" id="search">
                            <form action="#" method="post" id="emp_form">
                                <div class="well" id="form_search">

                                    <div class="form-inline">
                                        <select name="srh_class1" class="input-large" id="class1">
                                            <option value="">すべて</option>
                                            <{foreach item=row from=$class12_list}>
                                                <option value="<{$row.class_id|escape}>_<{$row.atrb_id|escape}>"><{$row.class_nm|escape}> &gt; <{$row.atrb_nm|escape}></option>
                                            <{/foreach}>
                                        </select>
                                        <select name="srh_class3" class="input-large" id="class3">
                                            <option value="">すべて</option>
                                        </select>
                                        <select id="class3_clone" style="display:none;">
                                            <{foreach item=row from=$class_list}>
                                                <option class="js-class-select select<{$row.class_id|escape}>_<{$row.atrb_id|escape}>" value="<{$row.dept_id|escape}><{if $row.room_id}>_<{$row.room_id|escape}><{/if}>">
                                                    <{$row.dept_nm|escape}><{if $row.room_id}> &gt; <{$row.room_nm|escape}><{/if}>
                                                </option>
                                            <{/foreach}>
                                        </select>
                                        <div class="input-prepend">
                                            <span class="add-on"><i class="icon-user-md"></i></span>
                                            <input type="text" id="srh_keyword" name="srh_keyword" class="input-large" value="" placeholder="職員名を入力してください">
                                        </div>
                                        <button type="submit" id="btn_search" name="btn_search" value="search" class="btn btn-small btn-primary"><i class="icon-search"></i> 検索する</button>
                                    </div>

                                    <{include file="__pager.tpl"}>
                                    <table class="table table-striped table-hover table-bordered ldr-table">
                                        <thead>
                                            <tr>
                                                <th>部署</th>
                                                <th>職員</th>
                                                    <{foreach from=$ladder->lists() item=ldr}>
                                                    <th colspan="<{$ldr.config.level|@count}>"><{$ldr.title|escape}></th>
                                                    <{/foreach}>
                                                <th>出身校<br />(卒業年)</th>
                                                <th>臨床開始日<br /><small>当院</small><br /><small>部署</small></th>
                                                <th>免許取得日</th>
                                                <th>JNA番号<br />(都道府県)</th>
                                                <th>更新日時</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <{foreach item=row from=$list}>
                                                <tr>
                                                    <td class="ldr-td-name">
                                                        <{$row->class_full_name()|escape|replace:' &gt; ':'<br/>'}>
                                                    </td>
                                                    <td class="ldr-td-name"><a href="adm_employee_edit.php?emp_id=<{$row->emp_id()|escape}>"><{$row->emp_personal_id()|escape}><br /><{$row->emp_name()|escape}></a></td>
                                                        <{foreach from=$ladder->lists() item=ldr}>
                                                            <{foreach from=$ldr.config.level item=level}>
                                                            <td class="ldr-td-level">
                                                                <{if $row->lv_date($ldr.ladder_id, $level)}>
                                                                    <span rel="tooltip"
                                                                          data-html="true"
                                                                          title="取得日: <{$row->lv_date($ldr.ladder_id, $level)}><br>
                                                                          医療機関: <{$row->lv_hospital($ldr.ladder_id, $level)}><br>
                                                                          認定番号: <{$row->lv_number($ldr.ladder_id, $level)}>">
                                                                        <{$roman_num[$level]|default:$ldr.config.level0}>
                                                                    </span>
                                                                <{/if}>
                                                            </td>
                                                        <{/foreach}>
                                                    <{/foreach}>
                                                    <td class="ldr-td-date">
                                                        <{$row->school()|escape}><br />
                                                        <{if $row->graduation_year()}>
                                                            (<{$row->graduation_year()|escape}>)
                                                        <{/if}>
                                                    </td>
                                                    <td class="ldr-td-date"><{$row->nurse_date()|date_format_jp:"%Y年%-m月"}><br /><{$row->hospital_date()|date_format_jp:"%Y年%-m月"}><br /><{$row->ward_date()|date_format_jp:"%Y年%-m月"}></td>
                                                    <td class="ldr-td-date"><{$row->license_date()|date_format_jp:"%Y年%-m月"}></td>
                                                    <td class="ldr-td-date">
                                                        <{$row->jna_id()|escape}><br />
                                                        <{if $row->pref_na_id()}>
                                                            (<{$row->pref_na_id()|escape}>)
                                                        <{/if}>
                                                    </td>
                                                    <td class="ldr-td-date" title="<{$row->updated_on()|date_format_jp:"%Y年%-m月%-d日 %R"}>"><{$row->updated_on()|date_format_jp:"%-m月%-d日<br/>%R"}></td>
                                                </tr>
                                            <{/foreach}>
                                        </tbody>
                                    </table>

                                </div>
                            </form>
                        </div>

                        <div class="tab-pane fade" id="import">
                            <form action="#" method="post">
                                <div class="well well-small" id="form_import">
                                    <div class="control-group">
                                        <label class="control-label"><strong>CSVファイル</strong></label>
                                        <div class="controls">
                                            <input class="input-medium" id="csvfile" name="csvfile" type="file" />
                                        </div>
                                    </div>
                                    <div class="btn-toolbar">
                                        <button type="button" id="btn_import" class="btn btn-small btn-danger"><i class="icon-upload-alt"></i> インポートする</button>
                                    </div>
                                    <div id="alert_csv" class="alert" style="overflow:auto; display: none;"></div>
                                    <hr/>

                                    <label class="control-label"><strong>CSVレイアウト</strong></label>
                                    <table class="table table-condensed table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <td>No</td>
                                                <td>名称</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="ldr-td-number">1</td>
                                                <td>職員ID</td>
                                            </tr>
                                            <tr>
                                                <td class="ldr-td-number">2</td>
                                                <td>職員名</td>
                                            </tr>
                                            <tr>
                                                <td class="ldr-td-number">3</td>
                                                <td>卒業年</td>
                                            </tr>
                                            <tr>
                                                <td class="ldr-td-number">4</td>
                                                <td>出身校</td>
                                            </tr>
                                            <tr>
                                                <td class="ldr-td-number">5</td>
                                                <td>看護師免許取得年月</td>
                                            </tr>
                                            <tr>
                                                <td class="ldr-td-number">6</td>
                                                <td>臨床開始年月</td>
                                            </tr>
                                            <tr>
                                                <td class="ldr-td-number">7</td>
                                                <td>当院配属年月</td>
                                            </tr>
                                            <tr>
                                                <td class="ldr-td-number">8</td>
                                                <td>病棟配属年月</td>
                                            </tr>
                                            <tr>
                                                <td class="ldr-td-number">9</td>
                                                <td>JNA会員番号</td>
                                            </tr>
                                            <tr>
                                                <td class="ldr-td-number">10</td>
                                                <td>都道府県看護協会 会員番号</td>
                                            </tr>
                                            <{counter start=10 print=false}>
                                            <{foreach from=$ladder->lists() item=ldr}>
                                                <{foreach from=$ldr.config.level item=level}>
                                                    <{if $level < 1}>
                                                        <{assign var=level_text value=$ldr.config.level0}>
                                                    <{else}>
                                                        <{assign var=level_text value=$roman_num[$level]}>
                                                    <{/if}>
                                                    <tr>
                                                        <td class="ldr-td-number"><{counter}></td>
                                                        <td><{$ldr.title|escape}>レベル<{$level_text|escape}>: 認定日</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ldr-td-number"><{counter}></td>
                                                        <td><{$ldr.title|escape}>レベル<{$level_text|escape}>: 取得施設</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ldr-td-number"><{counter}></td>
                                                        <td><{$ldr.title|escape}>レベル<{$level_text|escape}>: 認定番号</td>
                                                    </tr>
                                                <{/foreach}>
                                            <{/foreach}>
                                        </tbody>
                                    </table>
                                </div>
                            </form>
                        </div>

                        <div class="tab-pane fade" id="export">
                            <form action="adm_employee.php" method="post">
                                <input type="hidden" name="mode" value="export" />
                                <div class="well well-small">
                                    <div class="btn-toolbar">
                                        <button type="submit" class="btn btn-small btn-danger"><i class="icon-download-alt"></i> 職員データCSVをダウンロードする</button>
                                    </div>
                                    <hr/>

                                    <label class="control-label"><strong>CSVレイアウト</strong></label>
                                    <table class="table table-condensed table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <td>No</td>
                                                <td>名称</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="ldr-td-number">1</td>
                                                <td>職員ID</td>
                                            </tr>
                                            <tr>
                                                <td class="ldr-td-number">2</td>
                                                <td>職員名</td>
                                            </tr>
                                            <tr>
                                                <td class="ldr-td-number">3</td>
                                                <td>卒業年</td>
                                            </tr>
                                            <tr>
                                                <td class="ldr-td-number">4</td>
                                                <td>出身校</td>
                                            </tr>
                                            <tr>
                                                <td class="ldr-td-number">5</td>
                                                <td>看護師免許取得年月</td>
                                            </tr>
                                            <tr>
                                                <td class="ldr-td-number">6</td>
                                                <td>臨床開始年月</td>
                                            </tr>
                                            <tr>
                                                <td class="ldr-td-number">7</td>
                                                <td>当院配属年月</td>
                                            </tr>
                                            <tr>
                                                <td class="ldr-td-number">8</td>
                                                <td>病棟配属年月</td>
                                            </tr>
                                            <tr>
                                                <td class="ldr-td-number">9</td>
                                                <td>JNA会員番号</td>
                                            </tr>
                                            <tr>
                                                <td class="ldr-td-number">10</td>
                                                <td>都道府県看護協会 会員番号</td>
                                            </tr>
                                            <{counter start=10 print=false}>
                                            <{foreach from=$ladder->lists() item=ldr}>
                                                <{foreach from=$ldr.config.level item=level}>
                                                    <{if $level < 1}>
                                                        <{assign var=level_text value=$ldr.config.level0}>
                                                    <{else}>
                                                        <{assign var=level_text value=$roman_num[$level]}>
                                                    <{/if}>
                                                    <tr>
                                                        <td class="ldr-td-number"><{counter}></td>
                                                        <td><{$ldr.title|escape}>レベル<{$level_text|escape}>: 認定日</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ldr-td-number"><{counter}></td>
                                                        <td><{$ldr.title|escape}>レベル<{$level_text|escape}>: 取得施設</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ldr-td-number"><{counter}></td>
                                                        <td><{$ldr.title|escape}>レベル<{$level_text|escape}>: 認定番号</td>
                                                    </tr>
                                                <{/foreach}>
                                            <{/foreach}>
                                        </tbody>
                                    </table>
                                </div>
                            </form>
                        </div>

                        <div class="tab-pane fade" id="comedix">
                            <form action="adm_employee.php" method="post">
                                <input type="hidden" name="mode" value="comedix" />
                                <div class="well well-small">
                                    <div class="control-group" style="margin-bottom: 20px;">
                                        <label class="control-label"><strong>CoMedix連携</strong></label>
                                        <div>以下の項目の連携を行います。(値が未設定の場合は無視されます)</div>
                                    </div>
                                    <div class="row-fluid">
                                        <span class="span3">職員マスタ:入職日</span><span class="span1"><i class="icon-arrow-right"></i></span><span class="span2">当院配属年月</span>
                                    </div>
                                    <div class="row-fluid">
                                        <span class="span3">ファントルくん:プロフィール:職種開始年月</span><span class="span1"><i class="icon-arrow-right"></i></span><span class="span2">臨床開始年月</span>
                                    </div>
                                    <div class="row-fluid">
                                        <span class="span3">ファントルくん:プロフィール:部署開始年月</span><span class="span1"><i class="icon-arrow-right"></i></span><span class="span2">部署配属年月</span>
                                    </div>
                                    <div>
                                        <label class="radio inline"><input value="1" name="status" type="radio" />設定済みの項目も上書きで更新する</label>
                                        <label class="radio inline"><input value="2" name="status" type="radio" class="inline" checked />設定済みの項目は更新しない</label>
                                    </div>
                                    <div class="btn-toolbar">
                                        <button type="submit" class="btn btn-small btn-danger"><i class="icon-star-empty"></i> CoMedixデータ連携を行う</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <{include form_selector="#emp_form" file="__pager_js.tpl"}>
        <{javascript src="../js/jquery/jquery-upload-1.0.2.min.js"}>
        <script type="text/javascript">
            $(function() {

                // インポート
                $('#btn_import').click(function() {
                    $('#alert_csv')
                        .hide()
                        .css('height', '')
                        .removeClass('alert-error alert-info');

                    if (!$('#csvfile').val()) {
                        $('#alert_csv')
                            .html('<h4><i class="icon-warning-sign"></i> ファイルを選択してください</h4>')
                            .addClass('alert-error')
                            .show();
                        return false;
                    }
                    $('#form_import').upload('adm_employee.php', {mode: 'import'}, function(res) {
                        if (res.error) {
                            var result = '';
                            $.each(res.result, function(i, data) {
                                result += '<li>' + data + '</li>';
                            });
                            $('#alert_csv')
                                .html('<h4><i class="icon-warning-sign"></i> ' + res.msg + '</h4>' + (result ? '<ul>' + result + '</ul>' : ''))
                                .addClass('alert-error')
                                .css('height', '220px')
                                .show();
                        }
                        else {
                            $('#alert_csv')
                                .html('<h4><i class="icon-thumbs-up"></i> ' + res.msg + '</h4>')
                                .addClass('alert-info')
                                .show();
                        }
                    }, 'json');

                });

                // 部署切り替え
                $('#class1').change(function() {
                    $('#class3').empty().append('<option value="">すべて</option>');
                    $('#class3_clone').find('option.select' + $(this).val()).each(function() {
                        $('#class3').append($(this).clone());
                    });
                    $('#class3').find('option[value=<{$srh_class3|escape}>]').attr('selected', 'selected');
                }).trigger('change');

                // キーワード入力
                $('#srh_keyword').keypress(function(ev) {
                    if ((ev.which && ev.which === 13) || (ev.keyCode && ev.keyCode === 13)) {
                        $('#btn_search').trigger("click");
                    }
                });
            });
        </script>
    </body>
</html>