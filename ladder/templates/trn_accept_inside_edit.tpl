<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 院内研修受付</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='院内研修受付' hd_sub_title=''}>

            <div class="container-fluid">
                <form method="post" id="inside_form">
                    <input type='hidden' id='id' name='id' value='' />
                    <div class="well well-small">
                        <fieldset>
                            <legend>
                                <a
                                    href="trn_inside_contents_modal.php?id=<{$trn.inside_id|escape}>"
                                    data-toggle="modal"
                                    data-backdrop="true"
                                    data-target="#inside_contents">
                                    <{if $trn.no}>(<{$trn.no|escape}>) <{/if}><{$trn.title|escape}>
                                </a>
                                <a
                                    href="trn_inside_number_modal.php?id=<{$trn.inside_id|escape}>&number=<{$trn.date.number|escape}>&date_id=<{$date_id|escape}>"
                                    data-toggle="modal"
                                    data-backdrop="true"
                                    data-target="#inside_number">
                                    第<{$trn.date.number|escape}>回 / 全<{$trn.number_of|escape}>回
                                </a>
                            </legend>
                            <div class="control-group form-inline">
                                <span class="control-label"><strong>ステータス</strong></span>
                                &nbsp;
                                <{if $trn.date.reception==='0'}>
                                    <{assign var='lbl' value='label-info'}>
                                <{elseif $trn.date.reception < '0'}>
                                    <{assign var='lbl' value='label-warning'}>
                                <{else}>
                                    <{assign var='lbl' value='label-important'}>
                                <{/if}>

                                <a href="trn_inside_date_status_modal.php?status=<{$trn.date.status}>&id=<{$smarty.post.id}>" data-toggle="modal" data-target="#mst_date_status">
                                    <span class="label <{$lbl}>" rel="tooltip" title="ステータスを変更する"><{$trn.date.reception_text|escape}></span>
                                </a>
                            </div>
                            <div class="form-inline">
                                <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                                <select name="srh_class1" class="input-large" id="class1">
                                    <option value="">すべて</option>
                                    <{foreach item=row from=$class12_list}>
                                        <option value="<{$row.class_id|escape}>_<{$row.atrb_id|escape}>"><{$row.class_nm|escape}> &gt; <{$row.atrb_nm|escape}></option>
                                    <{/foreach}>
                                </select>
                                <select name="srh_class3" class="input-large" id="class3">
                                    <option value="">すべて</option>
                                </select>
                                <select id="class3_clone" style="display:none;">
                                    <{foreach item=row from=$class_list}>
                                        <option class="js-class-select select<{$row.class_id|escape}>_<{$row.atrb_id|escape}>" value="<{$row.dept_id|escape}><{if $row.room_id}>_<{$row.room_id|escape}><{/if}>">
                                            <{$row.dept_nm|escape}><{if $row.room_id}> &gt; <{$row.room_nm|escape}><{/if}>
                                        </option>
                                    <{/foreach}>
                                </select>
                                <select name="srh_status" class="input-medium">
                                    <option value="">すべて</option>
                                    <option value="3">受付済み</option>
                                    <option value="-3">取り下げ</option>
                                    <option value="2">未設定</option>
                                    <option value="1">承認待ち</option>
                                </select>
                                <button type="submit" id="btn_search" name="btn_search" value="search" class="btn btn-small btn-primary"><i class="icon-search"></i> 検索する</button>
                            </div>
                            <hr/>
                            <{if $list}>

                                <table class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th>部署</th>
                                            <th>職員ID</th>
                                            <th>名前</th>
                                            <th>申請日</th>
                                            <th>設定
                                                <button type="button" id="btn_on" class="btn btn-mini">すべて受付済み</button>
                                                <button type="button" id="btn_off" class="btn btn-mini">すべて取り下げ</button>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <{foreach item=row from=$list}>
                                            <tr>
                                                <td class="ldr-td-text-left">
                                                    <{$row.class_full_name|escape}>
                                                </td>
                                                <td class="ldr-td-tex"><{$row.emp_personal_id|escape}></td>
                                                <td class="ldr-td-text-left"><{$row.name|escape}></td>
                                                <td class="ldr-td-text-left"><{$row.created_on|date_format_jp:"%Y年%-m月%-d日(%a) %R"}></td>
                                                <td class="ldr-td-text-left">
                                                    <{if $row.status==='3' or $row.status==='2' or $row.status==='-3'}>
                                                        <span class="form-inline">
                                                            <label class="radio ldr-td-text">
                                                                <input type="radio" class="r1" name="training_id[<{$row.training_id|escape}>]" value="3" <{if $row.status==='3'}>checked<{/if}> /> 受付済み
                                                            </label>
                                                            <label class="radio ldr-td-text">
                                                                <input type="radio" class="r2" name="training_id[<{$row.training_id|escape}>]" value="-3" <{if $row.status==='-3'}>checked<{/if}> /> 取り下げ
                                                            </label>
                                                            <label class="radio ldr-td-text">
                                                                <input type="radio" class="r3" name="training_id[<{$row.training_id|escape}>]" value="2" <{if $row.status==='2'}>checked<{/if}>/> 未設定
                                                            </label>
                                                        </span>
                                                    <{elseif $row.status < 0}>
                                                        <span class="label label-important"><{$row.status_text|escape}></span>
                                                    <{elseif $row.status==='0'}>
                                                        <span class="label label-success"><{$row.status_text|escape}></span>
                                                    <{elseif $row.status==='1'}>
                                                        <{if $emp->is_training()}>
                                                            <span class="form-inline">
                                                                <label class="radio ldr-td-text">
                                                                    <input type="radio" class="r3" name="training_id[<{$row.training_id|escape}>]" value="1" <{if $row.status==='1'}>checked<{/if}>/> <span class="label label-info">承認待ち</span>
                                                                </label>
                                                                <label class="radio ldr-td-text">
                                                                    <input type="radio" class="r1" name="training_id[<{$row.training_id|escape}>]" value="3" <{if $row.status==='3'}>checked<{/if}> /> 受付済み
                                                                </label>
                                                                <label class="radio ldr-td-text">
                                                                    <input type="radio" class="r2" name="training_id[<{$row.training_id|escape}>]" value="-3" <{if $row.status==='-3'}>checked<{/if}> /> 取り下げ
                                                                </label>
                                                            </span>
                                                        <{else}>
                                                            <span class="label label-info"><{$row.status_text|escape}></span>
                                                        <{/if}>
                                                    <{else}>
                                                        <span class="label label-info"><{$row.status_text|escape}></span>
                                                    <{/if}>
                                                </td>
                                            </tr>
                                        <{/foreach}>
                                    </tbody>
                                </table>

                            <{else}>
                                <div class="alert alert-error">申請者がいません</div>
                            <{/if}>
                        </fieldset>
                        <div class="btn-toolbar">
                            <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                            <button type="submit" name="btn_save" class="btn btn-small btn-primary js-submit-accept" value="save"><i class="icon-edit"></i> 受付する</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <{include file="__modal_form.tpl" modal_id="mst_date_status" modal_title="ステータスの変更"}>
        <{include file="__modal_view.tpl" modal_id="inside_contents" modal_title="研修内容"}>
        <{include file="__modal_view.tpl" modal_id="inside_number" modal_title="研修内容"}>
        <{include file="__js.tpl"}>
        <{include file="__modal_form_js.tpl"}>
        <script type="text/javascript">
            $(function() {
                $('#btn_on').click(function() {
                    $('input.r1').prop("checked", true);
                });
                $('#btn_off').click(function() {
                    $('input.r2').prop("checked", true);
                });

                // 部署切り替え
                $('#class1').change(function() {
                    $('#class3').empty().append('<option value="">すべて</option>');
                    $('#class3_clone').find('option.select' + $(this).val()).each(function() {
                        $('#class3').append($(this).clone());
                    });
                    $('#class3').find('option[value=<{$srh_class3|escape}>]').attr('selected', 'selected');
                }).trigger('change');
            });
        </script>
    </body>
</html>
