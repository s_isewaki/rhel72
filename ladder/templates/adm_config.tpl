<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 管理設定</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='管理: 設定' hd_sub_title=''}>

            <div class="container-fluid">
                <div class="row-fluid">
                    <ul class="nav nav-tabs">
                        <li class="<{if $active === 'basic'}>active<{/if}>"><a href="#basic" data-toggle="tab"><i class="icon-gear"></i> 基本設定</a></li>
                        <li class="<{if $active === 'titles'}>active<{/if}>"><a href="#titles" data-toggle="tab"><i class="icon-pencil"></i> 名称設定</a></li>
                        <li class="<{if $active === 'func'}>active<{/if}>"><a href="#func" data-toggle="tab"><i class="icon-check-sign"></i> 利用機能</a></li>
                        <li class="<{if $active === 'sortset'}>active<{/if}>"><a href="#sortset" data-toggle="tab"><i class="icon-sort-by-order"></i> 表示順</a></li>
                        <li class="<{if $active === 'required'}>active<{/if}>"><a href="#required" data-toggle="tab"><i class="icon-check-sign"></i> 必須項目</a></li>
                    </ul>

                    <{include file="__save_message.tpl"}>

                    <div class="tab-content">
                        <div class="tab-pane <{if $active === 'basic'}>active<{else}>fade<{/if}> in" id="basic">
                            <form action="adm_config.php" method="post">
                                <input type="hidden" name="tab" value="basic" />
                                <div class="well well-small">
                                    <div class="control-group">
                                        <label class="control-label"><strong>病院タイプ</strong></label>
                                        <div class="controls">
                                            <label class="inline radio">
                                                <input type="radio" name="hospital_type" value="1" <{if $hospital_type === '1'}>checked="checked"<{/if}> />赤十字病院
                                            </label>
                                            <label class="inline radio">
                                                <input type="radio" name="hospital_type" value="2" <{if $hospital_type === '2'}>checked="checked"<{/if}> />それ以外
                                            </label>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label"><strong>病院名</strong></label>
                                        <div class="controls">
                                            <div class="input-prepend">
                                                <span class="add-on"><i class="icon-hospital"></i></span>
                                                <input type="text" name="hospital_name" class="input-xlarge" value="<{$hospital_name|escape}>" placeholder="病院名を入力してください" />
                                            </div>
                                        </div>
                                        <div class="controls">
                                            <{foreach from=$error.hospital_name item=msg}>
                                                <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                            <{/foreach}>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label"><strong>都道府県看護協会名</strong></label>
                                        <div class="controls">
                                            <div class="input-prepend">
                                                <span class="add-on"><i class="icon-building"></i></span>
                                                <input type="text" name="na_name" class="input-xlarge" value="<{$na_name|escape}>" placeholder="都道府県看護協会名を入力してください" />
                                            </div>
                                        </div>
                                        <div class="controls">
                                            <{foreach from=$error.na_name item=msg}>
                                                <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                            <{/foreach}>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label"><strong>日本赤十字社支部名</strong></label>
                                        <div class="controls">
                                            <div class="input-prepend">
                                                <span class="add-on"><i class="icon-building"></i></span>
                                                <input type="text" name="jrc_branch_name" class="input-xlarge" value="<{$jrc_branch_name|escape}>" placeholder="日本赤十字社支部名を入力してください" />
                                            </div>
                                        </div>
                                        <div class="controls">
                                            <{foreach from=$error.jrc_branch_name item=msg}>
                                                <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                            <{/foreach}>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label"><strong>部署表示範囲</strong></label>
                                        <div class="controls">
                                            <select name="view_class" class="input-xxlarge">
                                                <option value="1" <{if $view_class === '1'}>selected<{/if}>>第1階層: <{$emp->class_name()|escape}> &gt; <{$emp->atrb_name()|escape}> &gt; <{$emp->dept_name()|escape}><{if $class_cnt === '4'}> &gt; <{$emp->room_name|default:$room_nm|escape}><{/if}></option>
                                                <option value="2" <{if $view_class === '2'}>selected<{/if}>>第2階層: <{$emp->atrb_name()|escape}> &gt; <{$emp->dept_name()|escape}><{if $class_cnt === '4'}> &gt; <{$emp->room_name|default:$room_nm|escape}><{/if}></option>
                                                <option value="3" <{if $view_class === '3'}>selected<{/if}>>第3階層: <{$emp->dept_name()|escape}><{if $class_cnt === '4'}> &gt; <{$emp->room_name|default:$room_nm|escape}><{/if}></option>
                                            </select>
                                        </div>
                                        <div class="controls">
                                            <{foreach from=$error.jrc_branch_name item=msg}>
                                                <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                            <{/foreach}>
                                        </div>
                                    </div>

                                    <div class="btn-toolbar">
                                        <button type="submit" name="btn_save" class="btn btn-small btn-primary" value="basic"><i class="icon-save"></i> 保存する</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane <{if $active === 'func'}>active<{else}>fade<{/if}> in" id="func">

                            <form action="adm_config.php" method="post">
                                <input type="hidden" name="tab" value="func" />
                                <input type="hidden" name="function[]" value="dummy" />
                                <div class="well well-small">
                                    <p>ご利用になる機能にチェックを入れてください。</p>

                                    <div class="btn-toolbar">
                                        <button class="btn btn-small btn-primary" type="button" id="function-all-on"><i class="icon-check"></i> 全てON</button>
                                        <button class="btn btn-small btn-primary" type="button" id="function-all-off"><i class="icon-check-empty"></i> 全てOFF</button>
                                    </div>

                                    <table class="table table-hover">
                                        <tr>
                                            <td><{$titles.assessment|escape}></td>
                                            <td><label class="checkbox"><input type="checkbox" name="function[]" value="assessment" <{if $function.assessment}>checked="checked"<{/if}>/> 利用する</label></td>
                                            <td>職員の自己評価(行動指標)を入力する機能です</td>
                                        </tr>
                                        <tr>
                                            <td><{$titles.novice|escape}></td>
                                            <td><label class="checkbox"><input type="checkbox" name="function[]" value="novice" <{if $function.novice}>checked="checked"<{/if}>/> 利用する</label></td>
                                            <td>新人看護職員研修ガイドラインの到達目標を入職する機能です</td>
                                        </tr>
                                        <tr>
                                            <td><{$titles.record|escape}></td>
                                            <td><label class="checkbox"><input type="checkbox" name="function[]" value="record" id="function-record" <{if $function.record}>checked="checked"<{/if}>/> 利用する</label></td>
                                            <td>職員の研修や学会、社会活動を記録する機能です</td>
                                        </tr>
                                        <tr>
                                            <td style="padding-left:2em;"><{$titles.form_21.title|escape}></td>
                                            <td><label class="checkbox"><input type="checkbox" class="func-record" name="function[]" value="form_21" <{if $function.form_21}>checked="checked"<{/if}> <{if !$function.record}>disabled="disabled"<{/if}>/> 利用する</label></td>
                                            <td><{$titles.form_21.detailL|escape}></td>
                                        </tr>
                                        <tr>
                                            <td style="padding-left:2em;"><{$titles.form_22.title|escape}></td>
                                            <td><label class="checkbox"><input type="checkbox" class="func-record" name="function[]" value="form_22" <{if $function.form_22}>checked="checked"<{/if}> <{if !$function.record}>disabled="disabled"<{/if}>/> 利用する</label></td>
                                            <td><{$titles.form_22.detailL|escape}></td>
                                        </tr>
                                        <tr>
                                            <td style="padding-left:2em;"><{$titles.form_23.title|escape}></td>
                                            <td><label class="checkbox"><input type="checkbox" class="func-record" name="function[]" value="form_23" <{if $function.form_23}>checked="checked"<{/if}> <{if !$function.record}>disabled="disabled"<{/if}>/> 利用する</label></td>
                                            <td><{$titles.form_23.detailL|escape}></td>
                                        </tr>
                                        <tr>
                                            <td style="padding-left:2em;"><{$titles.form_3.title|escape}></td>
                                            <td><label class="checkbox"><input type="checkbox" class="func-record" name="function[]" value="form_3" <{if $function.form_3}>checked="checked"<{/if}> <{if !$function.record}>disabled="disabled"<{/if}>/> 利用する</label></td>
                                            <td><{$titles.form_3.detailL|escape}></td>
                                        </tr>
                                        <tr>
                                            <td style="padding-left:2em;"><{$titles.form_4.title|escape}></td>
                                            <td><label class="checkbox"><input type="checkbox" class="func-record" name="function[]" value="form_4" <{if $function.form_4}>checked="checked"<{/if}> <{if !$function.record}>disabled="disabled"<{/if}>/> 利用する</label></td>
                                            <td><{$titles.form_4.detailL|escape}></td>
                                        </tr>
                                        <tr>
                                            <td style="padding-left:2em;"><{$titles.form_51.title|escape}></td>
                                            <td><label class="checkbox"><input type="checkbox" class="func-record" name="function[]" value="form_51" <{if $function.form_51}>checked="checked"<{/if}> <{if !$function.record}>disabled="disabled"<{/if}>/> 利用する</label></td>
                                            <td><{$titles.form_51.detailL|escape}></td>
                                        </tr>
                                        <tr>
                                            <td style="padding-left:2em;"><{$titles.form_52.title|escape}></td>
                                            <td><label class="checkbox"><input type="checkbox" class="func-record" name="function[]" value="form_52" <{if $function.form_52}>checked="checked"<{/if}> <{if !$function.record}>disabled="disabled"<{/if}>/> 利用する</label></td>
                                            <td><{$titles.form_52.detailL|escape}></td>
                                        </tr>
                                        <tr>
                                            <td style="padding-left:2em;"><{$titles.form_53.title|escape}></td>
                                            <td><label class="checkbox"><input type="checkbox" class="func-record" name="function[]" value="form_53" <{if $function.form_53}>checked="checked"<{/if}> <{if !$function.record}>disabled="disabled"<{/if}>/> 利用する</label></td>
                                            <td><{$titles.form_53.detailL|escape}></td>
                                        </tr>
                                        <tr>
                                            <td style="padding-left:2em;"><{$titles.form_54.title|escape}></td>
                                            <td><label class="checkbox"><input type="checkbox" class="func-record" name="function[]" value="form_54" <{if $function.form_54}>checked="checked"<{/if}> <{if !$function.record}>disabled="disabled"<{/if}>/> 利用する</label></td>
                                            <td><{$titles.form_54.detailL|escape}></td>
                                        </tr>
                                        <tr>
                                            <td style="padding-left:2em;"><{$titles.form_carrier.title|escape}></td>
                                            <td><label class="checkbox"><input type="checkbox" class="func-record" name="function[]" value="form_carrier" <{if $function.form_carrier}>checked="checked"<{/if}> <{if !$function.record}>disabled="disabled"<{/if}>/> 利用する</label></td>
                                            <td><{$titles.form_carrier.detailL|escape}></td>
                                        </tr>
                                        <tr>
                                            <td style="padding-left:2em;"><{$titles.form_transfer.title|escape}></td>
                                            <td><label class="checkbox"><input type="checkbox" class="func-record" name="function[]" value="form_transfer" <{if $function.form_transfer}>checked="checked"<{/if}> <{if !$function.record}>disabled="disabled"<{/if}>/> 利用する</label></td>
                                            <td><{$titles.form_transfer.detailL|escape}></td>
                                        </tr>
                                        <tr>
                                            <td style="padding-left:2em;"><{$titles.form_presentation.title|escape}></td>
                                            <td><label class="checkbox"><input type="checkbox" class="func-record" name="function[]" value="form_presentation" <{if $function.form_presentation}>checked="checked"<{/if}> <{if !$function.record}>disabled="disabled"<{/if}>/> 利用する</label></td>
                                            <td><{$titles.form_presentation.detailL|escape}></td>
                                        </tr>
                                        <tr>
                                            <td style="padding-left:2em;"><{$titles.form_article.title|escape}></td>
                                            <td><label class="checkbox"><input type="checkbox" class="func-record" name="function[]" value="form_article" <{if $function.form_article}>checked="checked"<{/if}> <{if !$function.record}>disabled="disabled"<{/if}>/> 利用する</label></td>
                                            <td><{$titles.form_article.detailL|escape}></td>
                                        </tr>
                                        <tr>
                                            <td><{$titles.next_plan|escape}></td>
                                            <td><label class="checkbox"><input type="checkbox" name="function[]" value="next_plan" <{if $function.next_plan}>checked="checked"<{/if}>/> 利用する</label></td>
                                            <td>次年度の計画入力、参照を行う機能です</td>
                                        </tr>
                                        <tr>
                                            <td>ラダー</td>
                                            <td><label class="checkbox"><input type="checkbox" name="function[]" value="ladder" <{if $function.ladder}>checked="checked"<{/if}>/> 利用する</label></td>
                                            <td>キャリア開発ラダーの申請、承認、評価、認定を行う機能です</td>
                                        </tr>
                                        <tr>
                                            <td>研修</td>
                                            <td><label class="checkbox"><input type="checkbox" name="function[]" value="training" id="function-training" <{if $function.training}>checked="checked"<{/if}>/> 利用する</label></td>
                                            <td>研修の受講申請、受付、管理を行う機能です</td>
                                        </tr>
                                        <tr>
                                            <td style="padding-left:2em;">院内研修</td>
                                            <td><label class="checkbox"><input type="checkbox" class="func-training" name="function[]" value="inside" <{if $function.inside}>checked="checked"<{/if}> <{if !$function.training}>disabled="disabled"<{/if}>/> 利用する</label></td>
                                            <td>院内の研修の受講申請、受付、管理を行う機能</td>
                                        </tr>
                                        <tr>
                                            <td style="padding-left:2em;">院外研修</td>
                                            <td><label class="checkbox"><input type="checkbox" class="func-training" name="function[]" value="outside" <{if $function.outside}>checked="checked"<{/if}> <{if !$function.training}>disabled="disabled"<{/if}>/> 利用する</label></td>
                                            <td>院外の研修の受講申請、受付、管理を行う機能</td>
                                        </tr>
                                        <tr>
                                            <td style="padding-left:2em;">救護員研修</td>
                                            <td><label class="checkbox"><input type="checkbox" class="func-training" name="function[]" value="relief" <{if $function.relief}>checked="checked"<{/if}> <{if !$function.training}>disabled="disabled"<{/if}>/> 利用する</label></td>
                                            <td>赤十字救護員研修の受講申請、受付、管理を行う機能</td>
                                        </tr>
                                    </table>

                                    <div class="btn-toolbar">
                                        <button type="submit" name="btn_save" class="btn btn-small btn-primary" value="functions"><i class="icon-save"></i> 保存する</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane <{if $active === 'titles'}>active<{else}>fade<{/if}> in" id="titles">
                            <form action="adm_config.php" method="post">
                                <input type="hidden" name="tab" value="titles" />
                                <input type="hidden" name="titles[]" value="dummy" />
                                <div class="well well-small">
                                    <p>変更したい項目の名称を変更してください。</p>
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <td>項目</td>
                                                <td>名称</td>
                                                <td>短い説明</td>
                                                <td>長い説明</td>
                                                <td>大ボタン</td>
                                                <td>小ボタン</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td nowrap>自己評価</td>
                                                <td><input type="text" class="input-medium" name="titles[assessment]" value="<{$titles.assessment|default:'自己評価'|escape}>"></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td nowrap>新人評価</td>
                                                <td><input type="text" class="input-medium" name="titles[novice]" value="<{$titles.novice|default:'新人評価'|escape}>"></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>

                                            <tr>
                                                <td nowrap>ラダー</td>
                                                <td><input type="text" class="input-medium" name="titles[ladder]" value="<{$titles.ladder|default:'ラダー'|escape}>"></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td nowrap>次年度計画</td>
                                                <td><input type="text" class="input-medium" name="titles[next_plan]" value="<{$titles.next_plan|default:'次年度計画'|escape}>"></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>

                                            <tr>
                                                <td nowrap>記録</td>
                                                <td><input type="text" class="input-medium" name="titles[record]" value="<{$titles.record|default:'記録'|escape}>"></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>

                                            <tr>
                                                <td nowrap>継続教育</td>
                                                <td><input type="text" class="input-medium" name="titles[form_2][title]" value="<{$titles.form_2.title|default:'様式2'|escape}>"></td>
                                                <td><input type="text" class="input-large" name="titles[form_2][detail]" value="<{$titles.form_2.detail|default:'継続教育'|escape}>"></td>
                                                <td><input type="text" class="input-xlarge" name="titles[form_2][detailL]" value="<{$titles.form_2.detailL|default:'継続教育受講記録'|escape}>"></td>
                                                <td><input type="text" class="input-small" name="titles[form_2][btn]" value="<{$titles.form_2.btn|default:'様式2'|escape}>"></td>
                                                <td><input type="text" class="input-mini" name="titles[form_2][btnS]" value="<{$titles.form_2.btnS|default:'2'|escape}>"></td>
                                            </tr>
                                            <tr>
                                                <td nowrap>卒後教育</td>
                                                <td><input type="text" class="input-medium" name="titles[form_21][title]" value="<{$titles.form_21.title|default:'様式2-I'|escape}>"></td>
                                                <td><input type="text" class="input-large" name="titles[form_21][detail]" value="<{$titles.form_21.detail|default:'1ヶ月以上の研修受講'|escape}>"></td>
                                                <td><input type="text" class="input-xlarge" name="titles[form_21][detailL]" value="<{$titles.form_21.detailL|default:'1ヶ月以上の研修受講或いは卒後教育一覧（大学院、設定看護師コース、教員育成コース、実習指導者養成コース等）'|escape}>"></td>
                                                <td><input type="text" class="input-small" name="titles[form_21][btn]" value="<{$titles.form_21.btn|default:'様式2-I'|escape}>"></td>
                                                <td><input type="text" class="input-mini" name="titles[form_21][btnS]" value="<{$titles.form_21.btnS|default:'2-I'|escape}>"></td>
                                            </tr>
                                            <tr>
                                                <td nowrap>院内・院外研修</td>
                                                <td><input type="text" class="input-medium" name="titles[form_22][title]" value="<{$titles.form_22.title|default:'様式2-II'|escape}>"></td>
                                                <td><input type="text" class="input-large" name="titles[form_22][detail]" value="<{$titles.form_22.detail|default:'院内・院外研修'|escape}>"></td>
                                                <td><input type="text" class="input-xlarge" name="titles[form_22][detailL]" value="<{$titles.form_22.detailL|default:'院内・院外研修受講'|escape}>"></td>
                                                <td><input type="text" class="input-small" name="titles[form_22][btn]" value="<{$titles.form_22.btn|default:'様式2-II'|escape}>"></td>
                                                <td><input type="text" class="input-mini" name="titles[form_22][btnS]" value="<{$titles.form_22.btnS|default:'2-II'|escape}>"></td>
                                            </tr>
                                            <tr>
                                                <td nowrap>救護員研修</td>
                                                <td><input type="text" class="input-medium" name="titles[form_23][title]" value="<{$titles.form_23.title|default:'様式2-III'|escape}>"></td>
                                                <td><input type="text" class="input-large" name="titles[form_23][detail]" value="<{$titles.form_23.detail|default:'救護員研修'|escape}>"></td>
                                                <td><input type="text" class="input-xlarge" name="titles[form_23][detailL]" value="<{$titles.form_23.detailL|default:'救護員研修受講'|escape}>"></td>
                                                <td><input type="text" class="input-small" name="titles[form_23][btn]" value="<{$titles.form_23.btn|default:'様式2-III'|escape}>"></td>
                                                <td><input type="text" class="input-mini" name="titles[form_23][btnS]" value="<{$titles.form_23.btnS|default:'2-III'|escape}>"></td>
                                            </tr>
                                            <tr>
                                                <td nowrap>学会参加</td>
                                                <td><input type="text" class="input-medium" name="titles[form_3][title]" value="<{$titles.form_3.title|default:'様式3'|escape}>"></td>
                                                <td><input type="text" class="input-large" name="titles[form_3][detail]" value="<{$titles.form_3.detail|default:'院内研究発表会・学会一般参加'|escape}>"></td>
                                                <td><input type="text" class="input-xlarge" name="titles[form_3][detailL]" value="<{$titles.form_3.detailL|default:'院内研究発表会及び学会一般参加記録'|escape}>"></td>
                                                <td><input type="text" class="input-small" name="titles[form_3][btn]" value="<{$titles.form_3.btn|default:'様式3'|escape}>"></td>
                                                <td><input type="text" class="input-mini" name="titles[form_3][btnS]" value="<{$titles.form_3.btnS|default:'3'|escape}>"></td>
                                            </tr>
                                            <tr>
                                                <td nowrap>研究発表</td>
                                                <td><input type="text" class="input-medium" name="titles[form_4][title]" value="<{$titles.form_4.title|default:'様式4'|escape}>"></td>
                                                <td><input type="text" class="input-large" name="titles[form_4][detail]" value="<{$titles.form_4.detail|default:'業務改善・研究発表・雑誌投稿'|escape}>"></td>
                                                <td><input type="text" class="input-xlarge" name="titles[form_4][detailL]" value="<{$titles.form_4.detailL|default:'業務改善・研究発表及び雑誌投稿'|escape}>"></td>
                                                <td><input type="text" class="input-small" name="titles[form_4][btn]" value="<{$titles.form_4.btn|default:'様式4'|escape}>"></td>
                                                <td><input type="text" class="input-mini" name="titles[form_4][btnS]" value="<{$titles.form_4.btnS|default:'4'|escape}>"></td>
                                            </tr>
                                            <{if $hospital_type === '1'}>
                                                <tr>
                                                    <td nowrap>委員会・教育活動記録</td>
                                                    <td><input type="text" class="input-medium" name="titles[form_5][title]" value="<{$titles.form_5.title|default:'様式5'|escape}>"></td>
                                                    <td><input type="text" class="input-large" name="titles[form_5][detail]" value="<{$titles.form_5.detail|default:'委員会・教育活動記録'|escape}>"></td>
                                                    <td><input type="text" class="input-xlarge" name="titles[form_5][detailL]" value="<{$titles.form_5.detailL|default:'各種委員会・教育活動等記録'|escape}>"></td>
                                                    <td><input type="text" class="input-small" name="titles[form_5][btn]" value="<{$titles.form_5.btn|default:'様式5'|escape}>"></td>
                                                    <td><input type="text" class="input-mini" name="titles[form_5][btnS]" value="<{$titles.form_5.btnS|default:'5'|escape}>"></td>
                                                </tr>
                                            <{else}>
                                                <input type="hidden" name="titles[form_5][title]" value="<{$titles.form_5.title|default:'様式5'|escape}>"/>
                                                <input type="hidden" name="titles[form_5][detail]" value="<{$titles.form_5.detail|default:'委員会・教育活動記録'|escape}>"/>
                                                <input type="hidden" name="titles[form_5][detailL]" value="<{$titles.form_5.detailL|default:'各種委員会・教育活動等記録'|escape}>"/>
                                                <input type="hidden" name="titles[form_5][btn]" value="<{$titles.form_5.btn|default:'様式5'|escape}>"/>
                                                <input type="hidden" name="titles[form_5][btnS]" value="<{$titles.form_5.btnS|default:'5'|escape}>"/>
                                            <{/if}>
                                            <tr>
                                                <td nowrap>病棟内の役割</td>
                                                <td><input type="text" class="input-medium" name="titles[form_51][title]" value="<{$titles.form_51.title|default:'様式5-I'|escape}>"></td>
                                                <td><input type="text" class="input-large" name="titles[form_51][detail]" value="<{$titles.form_51.detail|default:'病棟内の役割'|escape}>"></td>
                                                <td><input type="text" class="input-xlarge" name="titles[form_51][detailL]" value="<{$titles.form_51.detailL|default:'病院内の役割（病院内の係、プリセプター、チームリーダー等）'|escape}>"></td>
                                                <td><input type="text" class="input-small" name="titles[form_51][btn]" value="<{$titles.form_51.btn|default:'様式5-I'|escape}>"></td>
                                                <td><input type="text" class="input-mini" name="titles[form_51][btnS]" value="<{$titles.form_51.btnS|default:'5-I'|escape}>"></td>
                                            </tr>
                                            <tr>
                                                <td nowrap>教育活動</td>
                                                <td><input type="text" class="input-medium" name="titles[form_52][title]" value="<{$titles.form_52.title|default:'様式5-II'|escape}>"></td>
                                                <td><input type="text" class="input-large" name="titles[form_52][detail]" value="<{$titles.form_52.detail|default:'院内外での教育活動'|escape}>"></td>
                                                <td><input type="text" class="input-xlarge" name="titles[form_52][detailL]" value="<{$titles.form_52.detailL|default:'院内外での教育活動（研修企画運営、院外研修講師等）'|escape}>"></td>
                                                <td><input type="text" class="input-small" name="titles[form_52][btn]" value="<{$titles.form_52.btn|default:'様式5-II'|escape}>"></td>
                                                <td><input type="text" class="input-mini" name="titles[form_52][btnS]" value="<{$titles.form_52.btnS|default:'5-II'|escape}>"></td>
                                            </tr>
                                            <tr>
                                                <td nowrap>委員会活動</td>
                                                <td><input type="text" class="input-medium" name="titles[form_53][title]" value="<{$titles.form_53.title|default:'様式5-III'|escape}>"></td>
                                                <td><input type="text" class="input-large" name="titles[form_53][detail]" value="<{$titles.form_53.detail|default:'院内外での委員会活動'|escape}>"></td>
                                                <td><input type="text" class="input-xlarge" name="titles[form_53][detailL]" value="<{$titles.form_53.detailL|default:'院内外の委員会活動（委員会、各種プロジェクトチーム、看護協会委員等）'|escape}>"></td>
                                                <td><input type="text" class="input-small" name="titles[form_53][btn]" value="<{$titles.form_53.btn|default:'様式5-III'|escape}>"></td>
                                                <td><input type="text" class="input-mini" name="titles[form_53][btnS]" value="<{$titles.form_53.btnS|default:'5-III'|escape}>"></td>
                                            </tr>
                                            <tr>
                                                <td nowrap>赤十字活動</td>
                                                <td><input type="text" class="input-medium" name="titles[form_54][title]" value="<{$titles.form_54.title|default:'様式5-IV'|escape}>"></td>
                                                <td><input type="text" class="input-large" name="titles[form_54][detail]" value="<{$titles.form_54.detail|default:'赤十字活動'|escape}>"></td>
                                                <td><input type="text" class="input-xlarge" name="titles[form_54][detailL]" value="<{$titles.form_54.detailL|default:'赤十字活動（赤十字行事への参加、国内救護活動・国際救援活動、ボランティア活動、その他社会貢献等）'|escape}>"></td>
                                                <td><input type="text" class="input-small" name="titles[form_54][btn]" value="<{$titles.form_54.btn|default:'様式5-IV'|escape}>"></td>
                                                <td><input type="text" class="input-mini" name="titles[form_54][btnS]" value="<{$titles.form_54.btnS|default:'5-IV'|escape}>"></td>
                                            </tr>
                                            <tr>
                                                <td nowrap>他院看護経験</td>
                                                <td><input type="text" class="input-medium" name="titles[form_carrier][title]" value="<{$titles.form_carrier.title|default:'他院看護経験'|escape}>"></td>
                                                <td><input type="text" class="input-large" name="titles[form_carrier][detail]" value="<{$titles.form_carrier.detail|default:'他院・他施設の看護経験'|escape}>"></td>
                                                <td><input type="text" class="input-xlarge" name="titles[form_carrier][detailL]" value="<{$titles.form_carrier.detailL|default:'他院・他施設の看護経験'|escape}>"></td>
                                                <td><input type="text" class="input-small" name="titles[form_carrier][btn]" value="<{$titles.form_carrier.btn|default:'他院歴'|escape}>"></td>
                                                <td><input type="text" class="input-mini" name="titles[form_carrier][btnS]" value="<{$titles.form_carrier.btnS|default:'他院歴'|escape}>"></td>
                                            </tr>
                                            <tr>
                                                <td nowrap>院内異動履歴</td>
                                                <td><input type="text" class="input-medium" name="titles[form_transfer][title]" value="<{$titles.form_transfer.title|default:'院内異動履歴'|escape}>"></td>
                                                <td><input type="text" class="input-large" name="titles[form_transfer][detail]" value="<{$titles.form_transfer.detail|default:'院内での異動履歴'|escape}>"></td>
                                                <td><input type="text" class="input-xlarge" name="titles[form_transfer][detailL]" value="<{$titles.form_transfer.detailL|default:'院内での異動履歴'|escape}>"></td>
                                                <td><input type="text" class="input-small" name="titles[form_transfer][btn]" value="<{$titles.form_transfer.btn|default:'異動歴'|escape}>"></td>
                                                <td><input type="text" class="input-mini" name="titles[form_transfer][btnS]" value="<{$titles.form_transfer.btnS|default:'異動歴'|escape}>"></td>
                                            </tr>
                                            <tr>
                                                <td nowrap>学会発表記録</td>
                                                <td><input type="text" class="input-medium" name="titles[form_presentation][title]" value="<{$titles.form_presentation.title|default:'学会発表記録'|escape}>"></td>
                                                <td><input type="text" class="input-large" name="titles[form_presentation][detail]" value="<{$titles.form_presentation.detail|default:'学会発表記録'|escape}>"></td>
                                                <td><input type="text" class="input-xlarge" name="titles[form_presentation][detailL]" value="<{$titles.form_presentation.detailL|default:'学会発表記録'|escape}>"></td>
                                                <td><input type="text" class="input-small" name="titles[form_presentation][btn]" value="<{$titles.form_presentation.btn|default:'発表記録'|escape}>"></td>
                                                <td><input type="text" class="input-mini" name="titles[form_presentation][btnS]" value="<{$titles.form_presentation.btnS|default:'発表記録'|escape}>"></td>
                                            </tr>
                                            <tr>
                                                <td nowrap>学会掲載記録</td>
                                                <td><input type="text" class="input-medium" name="titles[form_article][title]" value="<{$titles.form_article.title|default:'学会掲載記録'|escape}>"></td>
                                                <td><input type="text" class="input-large" name="titles[form_article][detail]" value="<{$titles.form_article.detail|default:'学会掲載記録'|escape}>"></td>
                                                <td><input type="text" class="input-xlarge" name="titles[form_article][detailL]" value="<{$titles.form_article.detailL|default:'学会掲載記録'|escape}>"></td>
                                                <td><input type="text" class="input-small" name="titles[form_article][btn]" value="<{$titles.form_article.btn|default:'掲載記録'|escape}>"></td>
                                                <td><input type="text" class="input-mini" name="titles[form_article][btnS]" value="<{$titles.form_article.btnS|default:'掲載記録'|escape}>"></td>
                                            </tr>

                                            <tr>
                                                <td nowrap>承認者</td>
                                                <td><input type="text" class="input-medium" name="titles[approver]" value="<{$titles.approver|default:'承認者'|escape}>"></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td nowrap>認定委員会</td>
                                                <td><input type="text" class="input-medium" name="titles[committee]" value="<{$titles.committee|default:'認定委員会'|escape}>"></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <div class="btn-toolbar">
                                        <button type="submit" name="btn_save" class="btn btn-small btn-primary" value="titles"><i class="icon-save"></i> 保存する</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane <{if $active === 'sortset'}>active<{else}>fade<{/if}> in" id="sortset">
                            <form action="adm_config.php" method="post">
                                <input type="hidden" name="tab" value="sortset" />
                                <div class="well well-small">
                                    <div class="form-group">
                                        <{if lists}>
                                            <fieldset>
                                                <p>記録項目の表示順を変更できます。</p>
                                                <table class="table table-striped table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th class="pull-right"><i class="icon-sort"></i></th>
                                                            <th>記録項目</th>
                                                            <th>詳細</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody class="sortable">
                                                        <{foreach item=key from=$sortset}>
                                                            <tr>
                                                                <td class="dragHandle"><span class="pull-right"><i class="icon-sort"></i><input type="hidden" name="order[]" value="<{$key|escape}>" /></span></td>
                                                                <td class="text-right">
                                                                    <{$titles[$key].title|escape}>
                                                                </td>
                                                                <td>
                                                                    <{$titles[$key].detailL|escape}>
                                                                </td>
                                                            </tr>
                                                        <{/foreach}>
                                                        <{foreach key=key item=value from=$functions}>
                                                            <{if $key|regex_replace:'/^form_.*/i':'form_match' === 'form_match' && ! in_array($key, $sortset)}>
                                                                <tr>
                                                                    <td class="dragHandle"><span class="pull-right"><i class="icon-sort"></i><input type="hidden" name="order[]" value="<{$key|escape}>" /></span></td>
                                                                    <td class="text-right">
                                                                        <{$titles[$key].title|escape}>
                                                                    </td>
                                                                    <td>
                                                                        <{$titles[$key].detailL|escape}>
                                                                    </td>
                                                                </tr>
                                                            <{/if}>
                                                        <{/foreach}>
                                                    </tbody>
                                                </table>
                                                <hr/>

                                                <div class="btn-toolbar">
                                                    <button type="submit" name="btn_save" class="btn btn-small btn-primary" value="sort"><i class="icon-save"></i> 順番を並べ替える</button>
                                                </div>
                                            </fieldset>
                                        <{/if}>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="tab-pane <{if $active === 'required'}>active<{else}>fade<{/if}> in" id="required">
                            <div class="well well-small">
                                <p>入力必須とするユーザ項目を設定してください</p>
                                <form action="adm_config.php" method="post">
                                    <input type="hidden" name="tab" value="required" />
                                    <input type="hidden" name="required[dummy]" value="1" />

                                    <div class="ldr-indent">
                                        <div class="control-group">
                                            <label class="checkbox"><input type="checkbox" name="required[graduation_year]" value="1" <{if $required.graduation_year}>checked="checked"<{/if}>/>卒業年</label>
                                        </div>
                                        <div class="control-group">
                                            <label class="checkbox"><input type="checkbox" name="required[school]" value="1" <{if $required.school}>checked="checked"<{/if}>/>出身校</label>
                                        </div>
                                        <div class="control-group">
                                            <label class="checkbox"><input type="checkbox" name="required[license_date]" value="1" <{if $required.license_date}>checked="checked"<{/if}>/>看護師免許取得年月</label>
                                        </div>
                                        <div class="control-group">
                                            <label class="checkbox"><input type="checkbox" name="required[nurse_date]" value="1" <{if $required.nurse_date}>checked="checked"<{/if}>/>臨床開始年月</label>
                                        </div>
                                        <div class="control-group">
                                            <label class="checkbox"><input type="checkbox" name="required[hospital_date]" value="1" <{if $required.hospital_date}>checked="checked"<{/if}>/>当院配属年月</label>
                                        </div>
                                        <div class="control-group">
                                            <label class="checkbox"><input type="checkbox" name="required[ward_date]" value="1" <{if $required.ward_date}>checked="checked"<{/if}>/>部署配属年月</label>
                                        </div>
                                        <div class="control-group">
                                            <label class="checkbox"><input type="checkbox" name="required[jna_id]" value="1" <{if $required.jna_id}>checked="checked"<{/if}>/>JNA会員番号</label>
                                        </div>
                                        <div class="control-group">
                                            <label class="checkbox"><input type="checkbox" name="required[pref_na_id]" value="1" <{if $required.pref_na_id}>checked="checked"<{/if}>/>都道府県看護協会 会員番号</label>
                                        </div>
                                    </div>
                                    <hr/>

                                    <div class="btn-toolbar">
                                        <button type="submit" name="btn_save" class="btn btn-small btn-primary" value="required"><i class="icon-save"></i> 保存する</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {
                // 全てON
                $('#function-all-on').click(function() {
                    $('input[name="function[]"]').prop('checked', true);
                    $('input.func-record').prop('disabled', false);
                    $('input.func-training').prop('disabled', false);
                });

                // 全てOFF
                $('#function-all-off').click(function() {
                    $('input[name="function[]"]').prop('checked', false);
                    $('input.func-record').prop('disabled', true);
                    $('input.func-training').prop('disabled', true);
                });

                // Table sort
                $('tbody.sortable').sortable({
                    items: '> tr',
                    handle: '.dragHandle',
                    axis: 'y',
                    opacity: 0.4,
                    cursor: 'move',
                    helper: function(r, tr) {
                        var origin = tr.children();
                        var helper = tr.clone();
                        helper.children().each(function(index) {
                            $(this).width(origin.eq(index).width());
                        });
                        return helper;
                    }
                });
            });
        </script>
    </body>
</html>
