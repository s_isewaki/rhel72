<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | ラダー分析</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='ラダー分析' hd_sub_title='ラダーレベルを基に職員の分布を分析できます'}>

            <div class="container-fluid">
                <div class="row-fluid">
                    <form id="form_print" method="post" class="pull-right">
                        <div class="btn-group pull-right">
                            <a class="btn btn-small dropdown-toggle <{if !$lists}>disabled<{/if}>" data-toggle="dropdown" href="#">
                                <i class="icon-print"></i> 印刷
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a tabindex="-1" href="#" data-id="P:A4" data-php="analysis_ladder.php" data-target="print" class="js-form-print">A4 縦</a></li>
                                <li><a tabindex="-1" href="#" data-id="L:A4" data-php="analysis_ladder.php" data-target="print" class="js-form-print">A4 横</a></li>
                                <li><a tabindex="-1" href="#" data-id="P:A3" data-php="analysis_ladder.php" data-target="print" class="js-form-print">A3 縦</a></li>
                                <li><a tabindex="-1" href="#" data-id="L:A3" data-php="analysis_ladder.php" data-target="print" class="js-form-print">A3 横</a></li>
                            </ul>
                        </div> &nbsp;
                    </form>
                    <form method="POST">
                        <div class="control-group form-inline">
                            <div class="input-prepend">
                                <span class="add-on"><i class="icon-sitemap"></i> 部署</span>
                                <{if count($class_lists) > 1}>
                                    <select class="multiselect-all" multiple="multiple" name="class[]">
                                        <{foreach item=row from=$class_lists}>
                                            <option value="<{$row.dept_id|escape}>" <{if $row.dept_id|in_array:$data.class or empty($data.class)}>selected="selected"<{/if}>><{$row.name|escape}></option>
                                        <{/foreach}>
                                    </select>
                                <{else}>
                                    <input type="hidden" name="class[]" value="<{$class_lists[0].dept_id|escape}>"/><span class="uneditable-input"><{$class_lists[0].name|escape}></span>
                                <{/if}>
                            </div>
                            <button type="submit" name="mode" value="csv" class="btn btn-small btn-default pull-right" <{if !$lists}>disabled<{/if}>><i class="icon-cloud-download"></i> CSVエクスポート</button>
                        </div>

                        <div class="control-group form-inline">
                            <div class="input-prepend">
                                <span class="add-on"><i class="icon-long-arrow-down"></i> 縦軸</span>
                                <select name="y" class="input-large">
                                    <{foreach key=id item=row from=$axis_lists}>
                                        <option value="<{$id|escape}>" <{if $id === $data.y}>selected="selected"<{/if}>><{$row.title|escape}></option>
                                    <{/foreach}>
                                </select>
                            </div>
                            <div class="input-prepend">
                                <span class="add-on"><i class="icon-long-arrow-right"></i> 横軸</span>
                                <select name="x" class="input-large">
                                    <option value="level">レベル</option>
                                    <{foreach key=id item=row from=$axis_lists}>
                                        <option value="<{$id|escape}>" <{if $id === $data.x}>selected="selected"<{/if}>><{$row.title|escape}></option>
                                    <{/foreach}>
                                </select>
                            </div>
                            <button type="submit" name="mode" value="search" class="btn btn-small btn-primary"><i class="icon-search"></i>検索する</button>
                        </div>
                    </form>
                    <hr/>

                    <{if isset($data.x)}>
                        <div id="div-res" class="well well-small">
                            <{if $data.x === 'level'}>
                                <{include file="analysis_ladder/result_level.tpl"}>
                            <{else}>
                                <{include file="analysis_ladder/result_ladder.tpl"}>
                            <{/if}>
                        </div>
                    <{/if}>
                </div>
            </div>
        </div>
        <{include file="__modal_view.tpl" modal_id="mst_contents" modal_title="職員一覧"}>
        <{include file="__js.tpl"}>
        <{include file="__modal_form_js.tpl"}>
    </body>
</html>