<{if !$lists}>
    <div class="alert alert-error">検索結果が見つかりませんでした。</div>
<{else}>
    <table class="table table-striped table-condensed table-hover">
        <thead>
            <tr>
                <th>-</th>
                <th class="ldr-td-text-center">レベル</th>
                    <{foreach item=row from=$lists.x}>
                    <th class="ldr-td-number">
                        <{$row.label|default:'(不明)'|escape}>
                    </th>
                <{/foreach}>
            </tr>
        </thead>
        <tbody id="appr_list">
            <{foreach name=table key=y_id item=label from=$lists.y}>
                <tr>
                    <td class="ldr-td-text" rowspan="6">
                        <strong><{$label|default:'(不明)'|escape}></strong>
                    </td>

                    <td class="ldr-td-text-center">無し</td>

                    <{foreach key=x_id item=x from=$lists.x}>
                        <td class="ldr-td-number">
                            <{if $x.y_id === 'total'}>
                                <strong><{$lists.list[$y_id][0][$x.y_id].count|default:0}></strong>
                            <{elseif empty($lists.list[$y_id][0][$x.y_id].count)}>
                                <span style="color:silver;">0</span>
                            <{else}>
                                <a href="analysis_ladder_modal.php?level=&amp;y_id=<{$y_id|escape}>&amp;x_id=<{$x.y_id|escape}>" data-toggle="modal" data-target="#mst_contents"><{$lists.list[$y_id][0][$x.y_id].count}></a>
                            <{/if}>
                        </td>
                    <{/foreach}>
                </tr>

                <tr>
                    <td class="ldr-td-level">I</td>

                    <{foreach key=x_id item=x from=$lists.x}>
                        <td class="ldr-td-number">
                            <{if $x.y_id === 'total'}>
                                <strong><{$lists.list[$y_id][1][$x.y_id].count|default:0}></strong>
                            <{elseif empty($lists.list[$y_id][1][$x.y_id].count)}>
                                <span style="color:silver;">0</span>
                            <{else}>
                                <a href="analysis_ladder_modal.php?level=1&amp;y_id=<{$y_id|escape}>&amp;x_id=<{$x.y_id|escape}>" data-toggle="modal" data-target="#mst_contents"><{$lists.list[$y_id][1][$x.y_id].count}></a>
                            <{/if}>
                        </td>
                    <{/foreach}>
                </tr>

                <tr>
                    <td class="ldr-td-level">II</td>

                    <{foreach key=x_id item=x from=$lists.x}>
                        <td class="ldr-td-number">
                            <{if $x.y_id === 'total'}>
                                <strong><{$lists.list[$y_id][2][$x.y_id].count|default:0}></strong>
                            <{elseif empty($lists.list[$y_id][2][$x.y_id].count)}>
                                <span style="color:silver;">0</span>
                            <{else}>
                                <a href="analysis_ladder_modal.php?level=2&amp;y_id=<{$y_id|escape}>&amp;x_id=<{$x.y_id|escape}>" data-toggle="modal" data-target="#mst_contents"><{$lists.list[$y_id][2][$x.y_id].count}></a>
                            <{/if}>
                        </td>
                    <{/foreach}>
                </tr>

                <tr>
                    <td class="ldr-td-level">III</td>

                    <{foreach key=x_id item=x from=$lists.x}>
                        <td class="ldr-td-number">
                            <{if $x.y_id === 'total'}>
                                <strong><{$lists.list[$y_id][3][$x.y_id].count|default:0}></strong>
                            <{elseif empty($lists.list[$y_id][3][$x.y_id].count)}>
                                <span style="color:silver;">0</span>
                            <{else}>
                                <a href="analysis_ladder_modal.php?level=3&amp;y_id=<{$y_id|escape}>&amp;x_id=<{$x.y_id|escape}>" data-toggle="modal" data-target="#mst_contents"><{$lists.list[$y_id][3][$x.y_id].count}></a>
                            <{/if}>
                        </td>
                    <{/foreach}>
                </tr>

                <tr>
                    <td class="ldr-td-level">IV</td>

                    <{foreach key=x_id item=x from=$lists.x}>
                        <td class="ldr-td-number">
                            <{if $x.y_id === 'total'}>
                                <strong><{$lists.list[$y_id][4][$x.y_id].count|default:0}></strong>
                            <{elseif empty($lists.list[$y_id][4][$x.y_id].count)}>
                                <span style="color:silver;">0</span>
                            <{else}>
                                <a href="analysis_ladder_modal.php?level=4&amp;y_id=<{$y_id|escape}>&amp;x_id=<{$x.y_id|escape}>" data-toggle="modal" data-target="#mst_contents"><{$lists.list[$y_id][4][$x.y_id].count}></a>
                            <{/if}>
                        </td>
                    <{/foreach}>
                </tr>

                <tr>
                    <td class="ldr-td-level">V</td>

                    <{foreach key=x_id item=x from=$lists.x}>
                        <td class="ldr-td-number">
                            <{if $x.y_id === 'total'}>
                                <strong><{$lists.list[$y_id][5][$x.y_id].count|default:0}></strong>
                            <{elseif empty($lists.list[$y_id][5][$x.y_id].count)}>
                                <span style="color:silver;">0</span>
                            <{else}>
                                <a href="analysis_ladder_modal.php?level=5&amp;y_id=<{$y_id|escape}>&amp;x_id=<{$x.y_id|escape}>" data-toggle="modal" data-target="#mst_contents"><{$lists.list[$y_id][5][$x.y_id].count}></a>
                            <{/if}>
                        </td>
                    <{/foreach}>
                </tr>
            <{/foreach}>
        </tbody>
    </table>
<{/if}>