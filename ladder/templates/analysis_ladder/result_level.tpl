<{if !$lists}>
    <div class="alert alert-error">検索結果が見つかりませんでした。</div>
<{else}>
    <table class="table table-striped table-condensed table-hover">
        <thead>
            <tr>
                <th>-</th>
                <th class="ldr-td-number">無し</th>
                <th class="ldr-td-number ldr-level">I</th>
                <th class="ldr-td-number ldr-level">II</th>
                <th class="ldr-td-number ldr-level">III</th>
                <th class="ldr-td-number ldr-level">IV</th>
                <th class="ldr-td-number ldr-level">V</th>
                <th class="ldr-td-number">計</th>
            </tr>
        </thead>
        <tbody id="appr_list">
            <{foreach name=table item=row from=$lists}>
                <tr <{if $smarty.foreach.table.last}>class="info"<{/if}>>
                    <td class="ldr-td-text">
                        <strong><{$row.label|default:'(不明)'|escape}></strong>
                    </td>
                    <td class="ldr-td-number">
                        <{if $smarty.foreach.table.last}>
                            <strong><{$row.lv0count}></strong>
                        <{elseif $row.lv0count === '0'}>
                            <span style="color:silver;">0</span>
                        <{else}>
                            <a href="analysis_ladder_modal.php?level=&amp;y_id=<{$row.y_id|escape}>" data-toggle="modal" data-target="#mst_contents"><{$row.lv0count}></a>
                        <{/if}>
                    </td>
                    <td class="ldr-td-number">
                        <{if $smarty.foreach.table.last}>
                            <strong><{$row.lv1count}></strong>
                        <{elseif $row.lv1count === '0'}>
                            <span style="color:silver;">0</span>
                        <{else}>
                            <a href="analysis_ladder_modal.php?level=1&amp;y_id=<{$row.y_id|escape}>" data-toggle="modal" data-target="#mst_contents"><{$row.lv1count}></a>
                        <{/if}>
                    </td>
                    <td class="ldr-td-number">
                        <{if $smarty.foreach.table.last}>
                            <strong><{$row.lv2count}></strong>
                        <{elseif $row.lv2count === '0'}>
                            <span style="color:silver;">0</span>
                        <{else}>
                            <a href="analysis_ladder_modal.php?level=2&amp;y_id=<{$row.y_id|escape}>" data-toggle="modal" data-target="#mst_contents"><{$row.lv2count}></a>
                        <{/if}>
                    </td>
                    <td class="ldr-td-number">
                        <{if $smarty.foreach.table.last}>
                            <strong><{$row.lv3count}></strong>
                        <{elseif $row.lv3count === '0'}>
                            <span style="color:silver;">0</span>
                        <{else}>
                            <a href="analysis_ladder_modal.php?level=3&amp;y_id=<{$row.y_id|escape}>" data-toggle="modal" data-target="#mst_contents"><{$row.lv3count}></a>
                        <{/if}>
                    </td>
                    <td class="ldr-td-number">
                        <{if $smarty.foreach.table.last}>
                            <strong><{$row.lv4count}></strong>
                        <{elseif $row.lv4count === '0'}>
                            <span style="color:silver;">0</span>
                        <{else}>
                            <a href="analysis_ladder_modal.php?level=4&amp;y_id=<{$row.y_id|escape}>" data-toggle="modal" data-target="#mst_contents"><{$row.lv4count}></a>
                        <{/if}>
                    </td>
                    <td class="ldr-td-number">
                        <{if $smarty.foreach.table.last}>
                            <strong><{$row.lv5count}></strong>
                        <{elseif $row.lv5count === '0'}>
                            <span style="color:silver;">0</span>
                        <{else}>
                            <a href="analysis_ladder_modal.php?level=5&amp;y_id=<{$row.y_id|escape}>" data-toggle="modal" data-target="#mst_contents"><{$row.lv5count}></a>
                        <{/if}>
                    </td>
                    <td class="ldr-td-number"><strong><{$row.total}></strong></td>
                </tr>
            <{/foreach}>
        </tbody>
    </table>
<{/if}>