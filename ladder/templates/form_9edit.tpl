<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | <{$ldr.config.form_9.title|escape}>入力</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title="`$ldr.config.form_9.title`入力" hd_sub_title=$ldr.config.form_9.detailL}>

            <div class="container-fluid">
                <div class="row-fluid">

                    <form class="form" id="edit_form" method="POST">
                        <{include file='__form_view.tpl' op_srh='false' tmp_id='form_9edit'}>
                    </form>

                    <form action="form_9edit.php" method="POST">
                        <input type="hidden" name="save" value="ajax" />
                        <input type="hidden" name="mode" value="" />
                        <input type="hidden" name="menu" value="" />
                        <input type="hidden" name="id" value="" />
                        <input type="hidden" name="target_emp_id" value=""/>
                        <input type="hidden" name="owner_url" value="" />

                        <div class="well well-small">
                            <div class="control-group">
                                <div class="input-prepend" style="margin-bottom:0;">
                                    <span class="add-on">申請レベル</span>
                                    <span class="ldr-uneditable input-mini ldr-level" style="margin-bottom:0;"><{$apply->level_text()|escape}></span>
                                </div>
                                <div class="input-prepend" style="margin-bottom:0;">
                                    <span class="add-on">申請者</span>
                                    <span class="ldr-uneditable input-medium" style="margin-bottom:0;"><{$target_emp->emp_name()|escape}> (<{$target_emp->class_full_name()|escape}>)</span>
                                </div>
                                <div class="input-prepend" style="margin-bottom:0;">
                                    <span class="add-on">監査者</span>
                                    <{foreach from=$auditor_list item=aprll}>
                                        <span class="ldr-uneditable input-medium" style="margin-bottom:0;">
                                            <{$aprll|escape}>
                                        </span>
                                    <{/foreach}>
                                </div>
                            </div>
                            <hr/>

                            <div class="alert">
                                <i class="icon-info-sign"></i> 基準を満たしていると判断したらチェックをいれること<br/>
                                <i class="icon-info-sign"></i> 基準を満たしていなければ、備考欄にその理由を記載
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="clinical1">
                                    <strong><{if $ldr.config.rc === '1'}>臨床看護実践<{else}>赤十字<{/if}></strong>
                                    <span class="badge badge-important required">必須</span>
                                </label>
                                <label class="checkbox">
                                    <input type="checkbox" id ="clinical1" name="clinical1" class="ck-auditor" value="t" />
                                    <{if $ldr.config.rc === '1'}>
                                        1) 評価は、指標の「知識」、「判断」、「行為」、「行為の結果」に則って実施されているか
                                    <{else}>
                                        1) 評価は、指標に則っているか
                                    <{/if}>
                                </label>
                                <label class="control-label" for="">備考</label>
                                <textarea name="clinical1_note" rows="3" class="input-maxlarge js-autoheight"></textarea>
                                <{foreach from=$error.clinical1 item=msg}>
                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                <{/foreach}>
                                <label class="checkbox">
                                    <input type="checkbox" id ="clinical2" name="clinical2" class="ck-auditor" value="t" />
                                    2) 評価内容に飛躍がないか
                                </label>
                                <label class="control-label" for="">備考</label>
                                <textarea name="clinical2_note" rows="3" class="input-maxlarge js-autoheight"></textarea>
                                <{foreach from=$error.clinical2 item=msg}>
                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                <{/foreach}>
                            </div>
                            <hr/>

                            <div class="control-group">
                                <label class="control-label" for="management1">
                                    <strong><{if $ldr.config.rc === '1'}>マネジメント<{else}>管理過程<{/if}></strong>
                                    <span class="badge badge-important required">必須</span>
                                </label>
                                <label class="checkbox">
                                    <input type="checkbox" id ="management1" name="management1" class="ck-auditor" value="t" />
                                    1) 評価は、指標に則っているか
                                </label>
                                <label class="control-label" for="">備考</label>
                                <textarea name="management1_note" rows="3" class="input-maxlarge js-autoheight"></textarea>
                                <{foreach from=$error.management1 item=msg}>
                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                <{/foreach}>
                                <label class="checkbox">
                                    <input type="checkbox" id ="management2" name="management2" class="ck-auditor" value="t" />
                                    2) 評価内容に飛躍がないか
                                </label>
                                <label class="control-label" for="">備考</label>
                                <textarea name="management2_note" rows="3" class="input-maxlarge js-autoheight"></textarea>
                                <{foreach from=$error.management2 item=msg}>
                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                <{/foreach}>
                            </div>
                            <hr/>

                            <div class="control-group">
                                <label class="control-label" for="education1">
                                    <strong><{if $ldr.config.rc === '1'}>教育・研究<{else}>意思決定<{/if}></strong>
                                    <span class="badge badge-important required">必須</span>
                                </label>
                                <label class="checkbox">
                                    <input type="checkbox" id ="education1" name="education1" class="ck-auditor" value="t" />
                                    1) 評価は、指標に則っているか
                                </label>
                                <label class="control-label" for="">備考</label>
                                <textarea name="education1_note" rows="3" class="input-maxlarge js-autoheight"></textarea>
                                <{foreach from=$error.education1 item=msg}>
                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                <{/foreach}>
                                <label class="checkbox">
                                    <input type="checkbox" id ="education2" name="education2" class="ck-auditor" value="t" />
                                    2) 評価内容に飛躍がないか
                                </label>
                                <label class="control-label" for="">備考</label>
                                <textarea name="education2_note" rows="3" class="input-maxlarge js-autoheight"></textarea>
                                <{foreach from=$error.education2 item=msg}>
                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                <{/foreach}>
                            </div>
                            <hr/>

                            <div class="control-group">
                                <label class="control-label" for="redcross1">
                                    <strong><{if $ldr.config.rc === '1'}>赤十字活動<{else}>質保証<{/if}></strong>
                                    <span class="badge badge-important required">必須</span>
                                </label>
                                <label class="checkbox">
                                    <input type="checkbox" id ="redcross1" name="redcross1" class="ck-auditor" value="t" />
                                    1) 評価は、指標に則っているか
                                </label>
                                <label class="control-label" for="">備考</label>
                                <textarea name="redcross1_note" rows="3" class="input-maxlarge js-autoheight"></textarea>
                                <{foreach from=$error.redcross1 item=msg}>
                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                <{/foreach}>
                                <label class="checkbox">
                                    <input type="checkbox" id ="redcross2" name="redcross2" class="ck-auditor" value="t" />
                                    2) 評価内容に飛躍がないか
                                </label>
                                <label class="control-label" for="">備考</label>
                                <textarea name="redcross2_note" rows="3" class="input-maxlarge js-autoheight"></textarea>
                                <{foreach from=$error.redcross2 item=msg}>
                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                <{/foreach}>
                            </div>
                            <hr/>

                            <{if $ldr.config.rc === '2'}>
                                <div class="control-group">
                                    <label class="control-label" for="comment51">
                                        <strong>人材育成</strong>
                                        <span class="badge badge-important required">必須</span>
                                    </label>
                                    <label class="checkbox">
                                        <input type="checkbox" id ="comment51" name="comment51" class="ck-auditor" value="t" />
                                        1) 評価は、指標に則っているか
                                    </label>
                                    <label class="control-label" for="">備考</label>
                                    <textarea name="comment51_note" rows="3" class="input-maxlarge js-autoheight"></textarea>
                                    <{foreach from=$error.comment51 item=msg}>
                                        <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                    <{/foreach}>
                                    <label class="checkbox">
                                        <input type="checkbox" id ="comment52" name="comment52" class="ck-auditor" value="t" />
                                        2) 評価内容に飛躍がないか
                                    </label>
                                    <label class="control-label" for="">備考</label>
                                    <textarea name="comment52_note" rows="3" class="input-maxlarge js-autoheight"></textarea>
                                    <{foreach from=$error.comment52 item=msg}>
                                        <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                    <{/foreach}>
                                </div>
                                <hr/>

                                <div class="control-group">
                                    <label class="control-label" for="comment61">
                                        <strong>対人関係</strong>
                                        <span class="badge badge-important required">必須</span>
                                    </label>
                                    <label class="checkbox">
                                        <input type="checkbox" id ="comment61" name="comment61" class="ck-auditor" value="t" />
                                        1) 評価は、指標に則っているか
                                    </label>
                                    <label class="control-label" for="">備考</label>
                                    <textarea name="comment61_note" rows="3" class="input-maxlarge js-autoheight"></textarea>
                                    <{foreach from=$error.comment61 item=msg}>
                                        <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                    <{/foreach}>
                                    <label class="checkbox">
                                        <input type="checkbox" id ="comment62" name="comment62" class="ck-auditor" value="t" />
                                        2) 評価内容に飛躍がないか
                                    </label>
                                    <label class="control-label" for="">備考</label>
                                    <textarea name="comment62_note" rows="3" class="input-maxlarge js-autoheight"></textarea>
                                    <{foreach from=$error.comment62 item=msg}>
                                        <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                    <{/foreach}>
                                </div>
                                <hr/>

                                <div class="control-group">
                                    <label class="control-label" for="comment71">
                                        <strong>セルフマネジメント</strong>
                                        <span class="badge badge-important required">必須</span>
                                    </label>
                                    <label class="checkbox">
                                        <input type="checkbox" id ="comment71" name="comment71" class="ck-auditor" value="t" />
                                        1) 評価は、指標に則っているか
                                    </label>
                                    <label class="control-label" for="">備考</label>
                                    <textarea name="comment71_note" rows="3" class="input-maxlarge js-autoheight"></textarea>
                                    <{foreach from=$error.comment71 item=msg}>
                                        <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                    <{/foreach}>
                                    <label class="checkbox">
                                        <input type="checkbox" id ="comment72" name="comment72" class="ck-auditor" value="t" />
                                        2) 評価内容に飛躍がないか
                                    </label>
                                    <label class="control-label" for="">備考</label>
                                    <textarea name="comment72_note" rows="3" class="input-maxlarge js-autoheight"></textarea>
                                    <{foreach from=$error.comment72 item=msg}>
                                        <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                    <{/foreach}>
                                </div>
                                <hr/>
                            <{else}>
                                <input type="hidden" name="comment51" value="" />
                                <input type="hidden" name="comment51_note" value="" />
                                <input type="hidden" name="comment52" value="" />
                                <input type="hidden" name="comment52_note" value="" />
                                <input type="hidden" name="comment61" value="" />
                                <input type="hidden" name="comment61_note" value="" />
                                <input type="hidden" name="comment62" value="" />
                                <input type="hidden" name="comment62_note" value="" />
                                <input type="hidden" name="comment71" value="" />
                                <input type="hidden" name="comment71_note" value="" />
                                <input type="hidden" name="comment72" value="" />
                                <input type="hidden" name="comment72_note" value="" />
                            <{/if}>

                            <div class="control-group">
                                <label class="control-label" for="problem">
                                    <strong>認定委員会としての活動や検討事項</strong>
                                    <span class="badge badge-important required">必須</span>
                                </label>
                                <textarea id="problem" name="problem" rows="3" class="input-maxlarge js-autoheight"></textarea>
                                <{foreach from=$error.problem item=msg}>
                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                <{/foreach}>
                            </div>
                            <p>
                                <small>
                                    <i class="icon-exclamation-sign"></i>
                                    入力内容は自動的に<{if $smarty.post.mode === 'edit'}>一時<{/if}>保存されます。
                                    <{if $smarty.post.mode === 'reedit'}>
                                        明示的に保存したい場合は<b>[保存する]ボタン</b>で保存してください。
                                    <{/if}>
                                </small>
                            </p>
                        </div>
                        <div class="btn-toolbar">
                            <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                            <{if $smarty.post.mode === 'edit'}>
                                <button type="submit" name="btn_save" class="btn btn-small btn-primary js-submit-decision" value="completed"><i class="icon-save"></i> 評価を確定する</button>
                            <{elseif $smarty.post.mode === 'reedit'}>
                                <button type="submit" name="btn_save" class="btn btn-small btn-primary js-submit-save" value="edit"><i class="icon-save"></i> 保存する</button>
                            <{/if}>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {
                // 自動保存
                $('form').autosave({
                    callbacks: {
                        trigger: 'change',
                        scope: 'all',
                        save: {
                            method: 'ajax',
                            options: {
                                url: 'form_9edit.php',
                                method: 'post'
                            }
                        }
                    }
                });
            });
        </script>
    </body>
</html>
