<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 救護員研修受付</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='救護員研修受付' hd_sub_title=''}>

            <div class="container-fluid">
                <form method="post" id="relief_form">
                    <input type='hidden' id='id' name='id' value='' />
                    <input type='hidden' id='branch' name='branch' value='' />
                    <input type='hidden' id='hospital' name='hospital' value='' />
                    <div class="well well-small">
                        <fieldset>
                            <legend >
                                <span
                                    <{if $list}>
                                        rel="popover" data-trigger="hover" data-html="true" data-placement="bottom" data-animation="true"
                                        data-content="
                                        <div class='ldr-popover'>支　部: <strong><{$list[0].branch|escape}></strong></div>
                                        <div class='ldr-popover'>施　設: <strong><{$list[0].hospital|escape}></strong></div>"
                                    <{/if}>
                                    >
                                    <{$sb.subject|escape}><span class="ldr-roman"><{$roman_num[$ct.unit]|escape}></span>　<{$ct.contents|escape}>
                                </span>
                            </legend>

                            <{if $list}>
                                <select name="srh_class" class="input-xlarge js_class">
                                    <option value="">すべて</option>
                                    <{foreach item=row from=$class_list}>
                                        <option value="<{$row.key|escape}>"><{$row.class_full_name|escape}></option>
                                    <{/foreach}>
                                </select>
                                <table class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th>部署</th>
                                            <th>職員ID</th>
                                            <th>名前</th>
                                            <th>申請日</th>
                                            <th>設定
                                                <button type="button" id="btn_on" class="btn btn-mini">すべて受付済み</button>
                                                <button type="button" id="btn_off" class="btn btn-mini">すべて取り下げ</button>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <{foreach item=row from=$list}>
                                            <tr>
                                                <td class="ldr-td-text-left">
                                                    <{$row.class_full_name|escape}>
                                                </td>
                                                <td class="ldr-td-text"><{$row.emp_personal_id|escape}></td>
                                                <td class="ldr-td-text-left"><{$row.name|escape}></td>
                                                <td class="ldr-td-text"><{$row.created_on|date_format_jp:"%Y年%-m月%-d日(%a) %R"}></td>
                                                <td class="ldr-td-text-left">
                                                    <{if $row.status==='3' or $row.status==='2' or $row.status==='-3'}>
                                                        <span class="form-inline">
                                                            <label class="radio ldr-td-text">
                                                                <input type="radio" class="r1" name="training_id[<{$row.training_id}>]" value="3" <{if $row.status==='3'}>checked<{/if}> /> 受付済み
                                                            </label>
                                                            <label class="radio ldr-td-text">
                                                                <input type="radio" class="r2" name="training_id[<{$row.training_id}>]" value="-3" <{if $row.status==='-3'}>checked<{/if}> /> 取り下げ
                                                            </label>
                                                            <label class="radio ldr-td-text">
                                                                <input type="radio" class="r3" name="training_id[<{$row.training_id}>]" value="2" <{if $row.status==='2'}>checked<{/if}>/> 後で設定
                                                            </label>
                                                        </span>
                                                    <{elseif $row.status < 0}>
                                                        <span class="label label-important"><{$row.status_text}></span>
                                                    <{elseif $row.status==='0'}>
                                                        <span class="label label-success"><{$row.status_text}></span>
                                                    <{else}>
                                                        <span class="label label-info"><{$row.status_text}></span>
                                                    <{/if}>
                                                </td>
                                            </tr>
                                        <{/foreach}>
                                    </tbody>
                                </table>

                            <{else}>
                                <div class="alert alert-error">申請者がいません</div>
                            <{/if}>
                        </fieldset>
                        <div class="btn-toolbar">
                            <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                            <button type="submit" name="btn_save" class="btn btn-small btn-primary js-submit-accept" value="save"><i class="icon-edit"></i> 受付する</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {
                $('#btn_on').click(function() {
                    $('.r1').prop("checked", true);
                });
                $('#btn_off').click(function() {
                    $('.r2').prop("checked", true);
                });
                $('.js_class').change(function() {
                    $('#relief_form').submit();
                });
            });
        </script>
    </body>
</html>
