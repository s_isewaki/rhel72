<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 救護員マスタ管理</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='救護員管理マスタ管理'}>

            <div class="container-fluid">
                <{include file="__save_message.tpl"}>
                <div class="row-fluid">
                    <ul class="nav nav-tabs">
                        <li class="<{if $active === 'subject'}>active<{/if}>"><a href="#subject" data-toggle="tab"><i class="icon-book"></i> 学科</a></li>
                        <li class="<{if $active === 'contents'}>active<{/if}>"><a href="#contents" data-toggle="tab"><i class="icon-pencil"></i> 教科内容</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane <{if $active === 'subject'}>active<{else}>fade<{/if}>" id="subject">
                            <div class="well well-small">
                                <div class="form-group">
                                    <fieldset>
                                        <legend>科目</legend>
                                        <{if $lists}>
                                            <form action="adm_relief.php" method="post" id="form_adm_relief">
                                                <input type="hidden" id="mode" name="mode" value="subject" />
                                                <table class="table table-striped table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th><i class='icon-pencil'></i></th>
                                                            <th class="pull-right"><i class="icon-sort"></i></th>
                                                            <th>科目</th>
                                                            <th class="pull-right"><i class='icon-remove'></i></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody class="sortable">
                                                        <{foreach item=row from=$lists}>
                                                            <tr>
                                                                <td><a href="adm_relief_modal.php?mode=relief&id=<{$row.relief_id|escape}>" data-toggle="modal" data-target="#mst_relief"><i class='icon-pencil'></i></a></td>
                                                                <td class="dragHandle"><span class="pull-right"><i class="icon-sort"></i><input type="hidden" name="order[]" value="<{$row.relief_id|escape}>" /></span></td>
                                                                <td class="text-right">
                                                                    <a class="js-row-contents" href="javascript:void(0);" data-target="#form_relief" data-id="<{$row.relief_id|escape}>"><{$row.subject|escape}></a>
                                                                </td>
                                                                <td>
                                                                    <a class="pull-right js-row-remove" href="javascript:void(0);" data-target="#form_relief" data-id="<{$row.relief_id|escape}>"><i class="icon-remove"></i></a>
                                                                </td>
                                                            </tr>
                                                        <{/foreach}>
                                                    </tbody>
                                                </table>
                                            </form>
                                            <hr/>
                                        <{else}>
                                            <div class="alert alert-error">科目を登録してください</div>
                                        <{/if}>

                                        <div class="btn-toolbar">
                                            <a href="adm_relief_modal.php?mode=relief" data-toggle="modal" data-target="#mst_relief" class="btn btn-small btn-primary"><i class="icon-plus"></i> 追加する</a>
                                            <button type="button" class="btn btn-small btn-primary js-table-sort" data-target="#form_adm_relief"><i class="icon-ok-sign"></i> 順番を並べ替える</button>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane <{if $active === 'contents'}>active<{else}>fade<{/if}>" id="contents">
                            <div class="well well-small">
                                <div class="form-group">
                                    <fieldset>
                                        <form action="adm_relief.php" method="post" id="form_adm_contents">
                                            <input type="hidden" id="mode" name="mode" value="contents" />
                                            <legend>教科内容</legend>
                                            <{if $lists}>
                                                <div class="controls">
                                                    <select id="relief_id" name="relief_id" class="js-pulldown-relief">
                                                        <{foreach item=row from=$lists}>
                                                            <option value="<{$row.relief_id|escape}>"><{$row.subject|escape}></option>
                                                        <{/foreach}>
                                                    </select>
                                                </div>
                                                <{if $lists_contents}>
                                                    <table class="table table-striped table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th><i class='icon-pencil'></i></th>
                                                                <th class="pull-right"><i class="icon-sort"></i></th>
                                                                <th>単元</th>
                                                                <th>教科内容</th>
                                                                <th>詳細</th>
                                                                <th>時間</th>
                                                                <th class="pull-right"><i class='icon-remove'></i></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody class="sortable">
                                                            <{foreach item=row from=$lists_contents}>
                                                                <tr>
                                                                    <td><a href="adm_relief_modal.php?mode=contents&id=<{$row.relief_id|escape}>&unit=<{$row.unit|escape}>" data-toggle="modal" data-target="#mst_contents"><i class='icon-pencil'></i></a></td>
                                                                    <td class="dragHandle"><span class="pull-right"><i class="icon-sort"></i><input type="hidden" name="order[]" value="<{$row.unit|escape}>" /></span></td>
                                                                    <td class="ldr-level"><{$roman_num[$row.unit]|escape}></td>
                                                                    <td class="ldr-td-text-left"><{$row.contents|escape}></td>
                                                                    <td class="ldr-td-text-left"><{$row.detail|escape|nl2br}></td>
                                                                    <td class="ldr-td-text-left"><{$row.time|escape}></td>
                                                                    <td>
                                                                        <a class="pull-right js-detail-row-remove" href="javascript:void(0);" data-target="#form_relief" data-unit="<{$row.unit|escape}>"><i class="icon-remove"></i></a>
                                                                    </td>
                                                                </tr>
                                                            <{/foreach}>
                                                        </tbody>
                                                    </table>
                                                    <hr/>
                                                <{else}>
                                                    <div class="alert alert-error">教科内容を登録してください</div>
                                                <{/if}>
                                            <{else}>
                                                <div class="alert alert-error">科目を登録してください</div>
                                            <{/if}>
                                        </form>
                                    </fieldset>
                                    <div class="btn-toolbar">
                                        <a href="adm_relief_modal.php?mode=contents&id=<{$smarty.post.relief_id}>" data-toggle="modal" data-target="#mst_contents" class="btn btn-small btn-primary"><i class="icon-plus"></i> 追加する</a>
                                        <button type="button" class="btn btn-small btn-primary js-table-sort" data-target="#form_adm_contents"><i class="icon-ok-sign"></i> 順番を並べ替える</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <{include file="__modal_form.tpl" modal_id="mst_contents" modal_title="教科内容"}>
                <{include file="__modal_form.tpl" modal_id="mst_relief" modal_title="科目の登録"}>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <{include file="__modal_form_js.tpl"}>
        <{javascript src="../js/jquery/jquery-ui-1.10.3.min.js"}>
        <script type="text/javascript">
            $(function() {
                $('#form_adm_relief').find('#mode').val('subject');
                $('#form_adm_contents').find('#mode').val('contents');

                // Table sort
                $('tbody.sortable').sortable({
                    items: '> tr',
                    handle: '.dragHandle',
                    axis: 'y',
                    opacity: 0.4,
                    cursor: 'move',
                    helper: function(r, tr) {
                        var origin = tr.children();
                        var helper = tr.clone();
                        helper.children().each(function(index) {
                            $(this).width(origin.eq(index).width());
                        });
                        return helper;
                    }
                });
                //項目の削除
                $('.js-row-remove').click(function() {
                    if (!confirm('削除してもよろしいですか？')) {
                        return false;
                    }
                    $(this).parents('form')
                        .append($('<input></input>').attr({type: 'hidden', name: 'id', value: $(this).data('id')}))
                        .append($('<input></input>').attr({type: 'hidden', name: 'mode2', value: 'delete'}))
                        .submit();
                });
                $('.js-detail-row-remove').click(function() {
                    if (!confirm('削除してもよろしいですか？')) {
                        return false;
                    }
                    $(this).parents('form')
                        .append($('<input></input>').attr({type: 'hidden', name: 'id', value: $('#relief_id').val()}))
                        .append($('<input></input>').attr({type: 'hidden', name: 'unit', value: $(this).data('unit')}))
                        .append($('<input></input>').attr({type: 'hidden', name: 'mode2', value: 'delete'}))
                        .submit();
                });
                $('.js-row-contents').click(function() {
                    $(this).parents('form')
                        .append($('<input></input>').attr({type: 'hidden', name: 'mode', value: 'contents'}))
                        .append($('<input></input>').attr({type: 'hidden', name: 'relief_id', value: $(this).data('id')}))
                        .append($('<input></input>').attr({type: 'hidden', name: 'mode2', value: ''}))
                        .submit();
                });
                //順番変更
                $('.js-table-sort').click(function() {
                    if (!confirm('順番を変更してもよろしいですか？')) {
                        return false;
                    }
                    $($(this).data('target'))
                        .append($('<input></input>').attr({type: 'hidden', name: 'mode2', value: 'sort'}))
                        .submit();
                });

                // 行動指標プルダウン選択
                $('.js-pulldown-relief').change(function() {
                    $(this).parents('form')
                        .append($('<input></input>').attr({type: 'hidden', name: 'mode2', value: ''}))
                        .submit();
                });

            });
        </script>
    </body>
</html>


