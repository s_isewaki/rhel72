<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 自己評価</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title="自己評価" hd_sub_title=""}>

            <div class="container-fluid">
                <div class="control-group">
                    <button type="button" class="js-btn-back btn btn-small" data-back="staff.php"><i class="icon-circle-arrow-left"></i> 戻る</button>
                    <select id="ladder" name="ladder_id" class="input-large js-pulldown-change" style="margin-bottom:0;">
                        <{foreach from=$ladder->lists() item=ldr}>
                            <option value="<{$ldr.ladder_id|escape}>" <{if $ldr.ladder_id === $this_ladder.ladder_id}>selected<{/if}>><{$ldr.title|escape}></option>
                        <{/foreach}>
                    </select>
                    <{if $year_list|@count > 1}>
                        <select id="year" name="year" class="input-medium js-pulldown-change" style="margin-bottom:0;" data-emp="<{$emp_id|escape}>">
                            <{foreach item=y from=$year_list}>
                                <option value="<{$y|escape}>" <{if $y === $year}>selected<{/if}>><{$y|escape}>年度</option>
                            <{/foreach}>
                        </select>
                    <{else}>
                        <b class="ldr-uneditable" style="margin-bottom:0;"><{$year|escape}>年度</b>
                    <{/if}>
                    <div class="input-prepend" style="margin-bottom:0;">
                        <span class="add-on"><{$emp_personal_id|escape}></span>
                        <span class="ldr-uneditable input-medium" style="margin-bottom:0;"><{$emp_lt_nm|escape}> <{$emp_ft_nm|escape}></span>
                    </div>
                </div>

                <div class="well well-small">

                    <ul id="tabs" class="nav nav-tabs">
                        <li><a href="#chart" data-toggle="tab">チャート</a></li>

                        <{foreach item=row from=$group1_list}>
                            <li>
                                <a href="#g1-<{$row.guideline_group1|escape}>" data-toggle="tab"><{$row.name|escape}></a>
                            </li>
                        <{/foreach}>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane" id="chart">
                            <div class="well well-small">
                                <{foreach from=$this_ladder.config.level item=lv}>
                                    <img style="vertical-align:text-top;" src="assessment_charts.php?emp_id=<{$emp_id|escape}>&year=<{$year|escape}>&level=<{$lv|escape}>&now=0" border="0" class="img-polaroid" />
                                <{/foreach}>
                            </div>
                        </div>

                        <{foreach item=g1 from=$group1_list}>
                            <div class="tab-pane" id="g1-<{$g1.guideline_group1|escape}>">

                                <table class="table table-bordered table-hover table-condensed table-striped">
                                    <thead>
                                        <tr>
                                            <th>分類</th>
                                            <th>レベル</th>
                                            <th>行動指標</th>
                                            <th>評価</th>
                                            <th>根拠</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <{foreach item=lv from=$this_ladder.config.level}>
                                            <{foreach item=asm from=$assessment_list[$g1.guideline_group1][$lv]}>
                                                <{assign var='asmkey' value="_`$asm.assessment`"}>
                                                <tr class="<{$this_ladder.config.select[$asmkey].color|default:'default'}>">
                                                    <td class="ldr-td-text-left"><{$asm.group2_name|escape}></td>
                                                    <td class="ldr-td-level"><{$roman_num[$lv]}></td>
                                                    <td class="ldr-td-text-left"><{$asm.guideline|escape}></td>
                                                    <td class="ldr-td-date"><{$this_ladder.config.select[$asmkey].button|default:'<i class="icon-question"></i>'}></td>
                                                    <td class="ldr-td-text-left"><{$asm.reason|escape|nl2br}></td>
                                                </tr>
                                            <{/foreach}>
                                        <{/foreach}>
                                    </tbody>
                                </table>
                            </div>
                        <{/foreach}>

                    </div>

                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {
                /**
                 * デフォルトタブ
                 */
                if ($('ul.nav-tabs').find('li.active').length === 0) {
                    $('ul.nav-tabs').find('a:first').tab('show');
                }

                /**
                 * プルダウン
                 */
                $('.js-pulldown-change').change(function() {
                    $('<form action="" method="post"></form>')
                        .append($('<input></input>').attr({type: 'hidden', name: 'emp_id', value: $(this).data('emp')}))
                        .append($('<input></input>').attr({type: 'hidden', name: 'year', value: $('#year').val()}))
                        .append($('<input></input>').attr({type: 'hidden', name: 'ladder_id', value: $('#ladder').val()}))
                        .appendTo(document.body)
                        .submit()
                        .remove();
                });
            });
        </script>
    </body>
</html>