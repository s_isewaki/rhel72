<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 院内研修 講座</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='院内研修 講座' hd_sub_title=''}>

            <div class="container-fluid">
                <form method="post" id="form_inside">
                    <div class="form-inline">
                        <select id="pulldown_year" name="srh_year" class="input-medium js-pulldown-inside">
                            <{foreach item=year from=$year_list}>
                                <option value="<{$year|escape}>"><{$year|escape}>年度</option>
                            <{/foreach}>
                        </select>
                        <select name="srh_level" id="srh_level" class="input-small js-pulldown-inside">
                            <option value="">レベル</option>
                            <option value="0">なし</option>
                            <option value="1">I</option>
                            <option value="2">II</option>
                            <option value="3">III</option>
                            <option value="4">IV</option>
                            <option value="5">V</option>
                        </select>
                        <select name="srh_guideline" class="input-medium js-pulldown-inside">
                            <option value="">カテゴリ</option>
                            <{foreach item=row from=$group1}>
                                <option value="<{$row.guideline_group1|escape}>"><{$row.name|escape}></option>
                            <{/foreach}>
                        </select>
                        <a href="trn_inside_calendar.php" class="btn btn-small btn-primary"><i class="icon-calendar"></i> 研修カレンダー</a>
                    </div>

                    <{if $list}>
                        <{include file="__pager.tpl"}>
                        <table class="table table-bordered table-hover table-condensed ldr-table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>研修名</th>
                                    <th>企画担当</th>
                                    <th>Lv</th>
                                    <th>カテゴリ</th>
                                    <th>講師</th>
                                    <th>回数</th>
                                    <th>申込状況</th>
                                    <th>申込</th>
                                </tr>
                            </thead>
                            <tbody>
                                <{foreach item=row from=$list}>
                                    <tr>
                                        <td class="ldr-td-name"><{$row.no|escape}></td>
                                        <td class="ldr-td-text-center"><{$row.title|escape}></td>
                                        <td class="ldr-td-text-center">
                                            <{if $row.planner}>
                                                <div rel="popover" data-trigger="hover" data-html="true" data-placement="bottom" data-animation="true"
                                                     data-content="
                                                     <{foreach item=rowp from=$row.emp_planner}>
                                                         <span class='label label-info'><{$rowp.emp_name|escape}></span><br/>
                                                     <{/foreach}>
                                                     "><{$row.planner|escape}>
                                                </div>
                                            <{else}>
                                                <{foreach item=rowp from=$row.emp_planner}>
                                                    <span class='label label-info'><{$rowp.emp_name|escape}></span>
                                                <{/foreach}>
                                            <{/if}>
                                        </td>
                                        <td class="ldr-td-level"><{$roman_num[$row.level]|escape}></td>
                                        <td class="ldr-td-name"><{$row.category|escape}></td>
                                        <td class="ldr-td-text-center">
                                            <{if $row.coach}>
                                                <{if $row.emp_coach}>
                                                    <div rel="popover" data-trigger="hover" data-html="true" data-placement="bottom" data-animation="true"
                                                         data-content="
                                                         <{foreach item=rowc from=$row.emp_coach}>
                                                             <span class='label label-info'><{$rowc.emp_name|escape}></span><br/>
                                                         <{/foreach}>
                                                         "><{$row.coach|escape}>
                                                    </div>
                                                <{else}>
                                                    <{$row.coach|escape}>
                                                <{/if}>
                                            <{else}>
                                                <{foreach item=rowc from=$row.emp_coach}>
                                                    <span class='label label-info'><{$rowc.emp_name|escape}></span>
                                                <{/foreach}>
                                            <{/if}>
                                        </td>
                                        <td class="ldr-td-button">
                                            <{if $row.number_of > 0}>
                                                全<{$row.number_of|escape}>回
                                            <{else}>
                                                未設定
                                            <{/if}>
                                        </td>
                                        <td class="ldr-td-text-center">
                                            <{if $row.course.course_applied}>
                                                <span class='label label-important'>申込あり</span>
                                            <{else}>
                                                <span class='label label-info'>申込なし</span>
                                            <{/if}>
                                        </td>
                                        <td class="ldr-td-button">
                                            <{if $row.number_of > 0}>
                                                <button type="button" data-php="trn_inside_apply.php" data-id="<{$row.inside_id}>" data-mode = "apply" class="js-form-button btn btn-small btn-danger">申込</button>
                                            <{else}>
                                                -
                                            <{/if}>
                                        </td>
                                    </tr>
                                <{/foreach}>
                            </tbody>
                        </table>
                    <{else}>
                        <div style="margin-top:10px;" class="alert alert-error">検索条件に該当する院内研修は見つかりませんでした</div>
                    <{/if}>
                </form>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <{include form_selector="#form_inside" file="__pager_js.tpl"}>
        <script type="text/javascript">
            // プルダウン選択
            $('.js-pulldown-inside').change(function() {
                $('#form_inside')
                    .append($('<input></input>').attr({type: 'hidden', name: 'search', value: 'search'}))
                    .submit();
            });
        </script>
    </body>
</html>
