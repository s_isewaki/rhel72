<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 職員管理</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title="職員管理" hd_sub_title=""}>

            <div class="container-fluid">
                <form id="form_edit" action="adm_employee_edit.php" method="post">
                    <input type="hidden" name="emp_id" value="" />
                    <input type="hidden" name="ldr_updated_on" value="" />

                    <div class="btn-toolbar">
                        <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                        <button type="submit" name="btn_save" id="btn_save" class="btn btn-small btn-primary" value="btn_save"><i class="icon-save"></i> 保存する</button>
                    </div>

                    <div class="well well-small">
                        <div class="control-group">
                            <label class="control-label"><strong>職員名</strong></label>
                            <div class="lead"><{$staff->emp_name()|escape}> (ID: <{$staff->emp_personal_id()|escape}>)</div>
                        </div>
                        <{if $staff->updated_on()}>
                            <div class="control-group">
                                <label class="control-label"><strong>更新日時</strong></label>
                                <div><{$staff->updated_on()|date_format_jp:"%Y年%-m月%-d日(%a) %R"}></div>
                            </div>
                        <{/if}>
                    </div>

                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#basic" data-toggle="tab">基本</a></li>
                            <{foreach from=$ladder->lists() item=ldr}>
                            <li><a href="#ladder<{$ldr.ladder_id|escape}>" data-toggle="tab"><{$ldr.title|escape}></a></li>
                            <{/foreach}>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active in" id="basic">
                            <div class="well well-small">

                                <div class="control-group">
                                    <label class="control-label" for="graduation_year"><strong>卒業年(出身校)</strong></label>
                                    <div class="controls">
                                        <{include file="__datepicker.tpl" date_type="year" date_id="graduation_year" date_name="graduation_year" date_value=""}>
                                        <div class="input-prepend">
                                            <span class="add-on"><i class="icon-building"></i></span>
                                            <input type="text" id="school" name="school" class="input-xlarge" minlength="2" maxlength="200" value="" placeholder="出身校を入力してください"  />
                                        </div>
                                    </div>
                                    <div class="controls">
                                        <{foreach from=$error.school item=msg}>
                                            <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                        <{/foreach}>
                                        <{foreach from=$error.graduation_year item=msg}>
                                            <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                        <{/foreach}>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for=""><strong>看護師免許取得年月</strong></label>
                                    <div class="controls">
                                        <{include file="__datepicker.tpl" date_type="month" date_id="license_date" date_name="license_date" date_value=""}>
                                        <{foreach from=$error.license_date item=msg}>
                                            <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                        <{/foreach}>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for=""><strong>臨床開始年月</strong></label>
                                    <div class="controls">
                                        <{include file="__datepicker.tpl" date_type="month" date_id="nurse_date" date_name="nurse_date" date_value=""}>
                                        <{foreach from=$error.nurse_date item=msg}>
                                            <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                        <{/foreach}>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label" for=""><strong>当院 配属年月</strong></label>
                                    <div class="controls">
                                        <{include file="__datepicker.tpl" date_type="month" date_id="hospital_date" date_name="hospital_date" date_value=""}>
                                        <{foreach from=$error.hospital_date item=msg}>
                                            <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                        <{/foreach}>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label" for=""><strong>部署 配属年月</strong></label>
                                    <div class="controls">
                                        <{include file="__datepicker.tpl" date_type="month" date_id="ward_date" date_name="ward_date" date_value=""}>
                                        <{foreach from=$error.ward_date item=msg}>
                                            <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                        <{/foreach}>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label" for="jna_id"><strong>JNA会員番号 <small>(数字8桁)</small></strong></label>
                                    <div class="controls">
                                        <div class="input-prepend">
                                            <span class="add-on"><i class="icon-credit-card"></i></span>
                                            <input type="text" id="school" name="jna_id" class="input-xlarge onlynum" minlength="2" maxlength="8" value="" placeholder="会員番号を入力してください"  />
                                        </div>
                                    </div>
                                    <div class="controls">
                                        <{foreach from=$error.jna_id item=msg}>
                                            <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                        <{/foreach}>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="pref_na_id"><strong>都道府県看護協会 会員番号 <small>(数字6桁)</small></strong></label>
                                    <div class="controls">
                                        <div class="input-prepend">
                                            <span class="add-on"><i class="icon-credit-card"></i></span>
                                            <input type="text" id="school" name="pref_na_id" class="input-xlarge onlynum" minlength="2" maxlength="6" value="" placeholder="会員番号を入力してください"  />
                                        </div>
                                    </div>
                                    <div class="controls">
                                        <{foreach from=$error.pref_na_id item=msg}>
                                            <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                        <{/foreach}>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <{foreach from=$ladder->lists() item=ldr}>
                            <div class="tab-pane" id="ladder<{$ldr.ladder_id|escape}>">
                                <div class="well well-small">
                                    <{foreach from=$ldr.config.level item=level}>
                                        <div class="control-group">
                                            <label class="control-label" for=""><strong>レベル<span class="ldr-level"><{if $level}><{$roman_num[$level]}><{else}><{$ldr.config.level0}><{/if}></span> 認定日 (取得医療施設)</strong></label>
                                            <input type="hidden" name="level[<{$ldr.ladder_id|escape}>][<{$level}>]" value="1" />
                                            <div class=" controls">
                                                <{include file="__datepicker.tpl" date_name="lv_date[`$ldr.ladder_id`][$level]" date_value=$staff->lv_date($ldr.ladder_id, $level)}>
                                                <div class="input-prepend">
                                                    <span class="add-on"><i class="icon-hospital"></i></span>
                                                    <input type="text" name="lv_hospital[<{$ldr.ladder_id|escape}>][<{$level}>]" class="input-large" minlength="2" maxlength="200" value="<{$staff->lv_hospital($ldr.ladder_id, $level)}>" placeholder="取得医療施設を入力してください"  />
                                                </div>
                                                <div class="input-prepend">
                                                    <span class="add-on"><i class="icon-trophy"></i></span>
                                                    <input type="text" name="lv_number[<{$ldr.ladder_id|escape}>][<{$level}>]" class="input-large" maxlength="20" value="<{$staff->lv_number($ldr.ladder_id, $level)}>" placeholder="認定番号を入力してください" />
                                                </div>
                                            </div>
                                            <div class="controls">
                                                <{foreach from=$error.lv_date[$ldr.ladder_id][$level] item=msg}>
                                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                                <{/foreach}>
                                                <{foreach from=$error.lv_hospital[$ldr.ladder_id][$level] item=msg}>
                                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                                <{/foreach}>
                                                <{foreach from=$error.lv_number[$ldr.ladder_id][$level] item=msg}>
                                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                                <{/foreach}>
                                            </div>
                                        </div>
                                    <{/foreach}>
                                </div>
                            </div>
                        <{/foreach}>
                    </div>
                    <div class="btn-toolbar">
                        <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                        <button type="submit" name="btn_save" id="btn_save" class="btn btn-small btn-primary" value="btn_save"><i class="icon-save"></i> 保存する</button>
                    </div>

                </form>
            </div>
        </div>
        <{include file="__js.tpl"}>

    </body>
</html>
