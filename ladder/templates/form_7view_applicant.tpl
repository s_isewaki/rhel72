<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | <{$ldr.config.form_7.title|escape}></title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title=$ldr.config.form_7.title hd_sub_title=$ldr.config.form_7.detailL}>

            <div class="container-fluid">
                <div class="row-fluid">
                    <{*印刷ボタン*}>
                    <form id="form_print" method="post" class="pull-right">
                        <button type="button" data-id="<{$smarty.post.id}>" data-emp="<{$smarty.post.target_emp_id}>" data-php="form_7view.php" data-target="form7" class="pull-right js-form-print btn btn-primary btn-small"><i class="icon-print"></i> 印刷する</button>
                    </form>

                    <{*様式共通ボタン*}>
                    <form id="form_edit" method="post">
                        <{include file='__form_view.tpl' op_srh='false' tmp_id='form_7view'}>

                        <input type="hidden" name="active" id="active" value="" />

                        <div class="well well-small">

                            <div class="control-group">
                                <div class="input-prepend" style="margin-bottom:0;">
                                    <span class="add-on">申請レベル</span>
                                    <span class="ldr-uneditable input-mini ldr-level" style="margin-bottom:0;"><{$apply->level_text()|escape}></span>
                                </div>
                                <div class="input-prepend" style="margin-bottom:0;">
                                    <span class="add-on">申請者</span>
                                    <span class="ldr-uneditable" style="margin-bottom:0;"><{$target_emp->emp_name()|escape}> (<{$target_emp->class_full_name()|escape}>)</span>
                                </div>
                            </div>
                            <hr/>

                            <ul id="tabs" class="nav nav-tabs">
                                <{foreach item=row from=$group1_list}>
                                    <li class="<{if $active === $row.guideline_group1}>active<{/if}>">
                                        <a href="#g1-<{$row.guideline_group1|escape}>" data-toggle="tab"><{$row.name|escape}></a>
                                    </li>
                                <{/foreach}>
                                <{if $ldr.config.asm_comment_used === '1'}>
                                    <li class="<{if $active === 'comment'}>active<{/if}>">
                                        <a href="#comment" data-toggle="tab"><i class="icon-comment"></i> <{$ldr.config.asm_comment|default:'コメント'|escape}></a>
                                    </li>
                                <{/if}>
                            </ul>
                            <div class="tab-content">

                                <{foreach item=g1 from=$group1_list}>
                                    <div class="tab-pane <{if $active === $g1.guideline_group1}>active<{else}>fade<{/if}>" id="g1-<{$g1.guideline_group1|escape}>">

                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>行動指標</th>
                                                    <th>評価</th>
                                                    <{if $ldr.config.reason === '1'}><th>根拠</th><{/if}>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <{foreach item=asm from=$assessment_list[$g1.guideline_group1]}>
                                                    <{assign var='asmkey' value="_`$asm.apply_assessment`"}>
                                                    <tr class="<{$ldr.config.select[$asmkey].color|default:'default'}>">
                                                        <td class="ldr-td-text-left">
                                                            <{$asm.guideline|escape}>
                                                            <{if $asm.criterion}>
                                                                <br/><{$asm.criterion|escape}>
                                                            <{/if}>
                                                        </td>
                                                        <td><{$ldr.config.select[$asmkey].button|default:'<i class="icon-question"></i>'}></td>
                                                        <{if $ldr.config.reason === '1'}>
                                                            <td class="ldr-td-text-left"><{$asm.apply_reason|escape|nl2br}></td>
                                                        <{/if}>
                                                    </tr>
                                                <{/foreach}>
                                            </tbody>
                                        </table>
                                    </div>
                                <{/foreach}>
                                <{if $ldr.config.asm_comment_used === '1'}>
                                    <div class="tab-pane <{if $active === 'comment'}>active<{else}>fade<{/if}>" id="comment">
                                        <div class="control-group">
                                            <label class="control-label">
                                                <strong><{$ldr.config.asm_comment|default:'コメント'|escape}></strong>
                                            </label>
                                            <div class="controls">
                                                <span class="input-maxlarge ldr-uneditable"><{$form7.knowledge|escape|nl2br}></span>
                                            </div>
                                        </div>
                                        <{if $ldr.config.asm_comment2_used === '1'}>
                                            <div class="control-group">
                                                <label class="control-label">
                                                    <strong><{$ldr.config.asm_comment2|default:'コメント'|escape}></strong>
                                                </label>
                                                <div class="controls">
                                                    <span class="input-maxlarge ldr-uneditable"><{$form7.decision|escape|nl2br}></span>
                                                </div>
                                            </div>
                                        <{/if}>
                                    </div>
                                <{/if}>
                            </div>
                        </div>
                        <div class="btn-toolbar">
                            <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
    </body>
</html>