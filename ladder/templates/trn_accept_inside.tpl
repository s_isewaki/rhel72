<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 院内研修受付</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='院内研修受付' hd_sub_title=''}>

            <div class="container-fluid">
                <form method="post" id="inside_form">
                    <div class="control-group form-inline">
                        <select id="pulldown_year" name="srh_year" class="input-medium srh">
                            <{foreach item=year from=$year_list}>
                                <option value="<{$year|escape}>"><{$year|escape}>年度</option>
                            <{/foreach}>
                        </select>
                        <label class="checkbox inline" style="margin-left:10px;">
                            <input type="checkbox" id="srh_nonaccept" name="srh_nonaccept" value="1" class="js-srh-filter" />
                            <strong>受付待ちのみ</strong>
                        </label>
                        <{if $emp->is_inside()}>
                            <label class="checkbox inline">
                                <input type="checkbox" id="srh_planner" name="srh_planner" value="1" class="js-srh-filter" />
                                <strong>担当講座のみ</strong>
                            </label>
                        <{/if}>
                    </div>
                    <div class="control-group form-inline">
                        <{include file="__datepicker.tpl" date_id="srh_start_date" date_name="srh_start_date" date_value=$smarty.post.srh_start_date|escape}>
                        <span>〜</span>
                        <{include file="__datepicker.tpl" date_id="srh_last_date" date_name="srh_last_date" date_value=$smarty.post.srh_last_date|escape}>
                        &nbsp;
                        <input type="text" id="srh_keyword" name="srh_keyword" class="input-large" value='<{$srh_keyword|escape}>' placeholder="検索ワードを入力してください" />
                        <button type="submit" id="search" name="search" value="search" class="btn btn-small btn-primary"><i class="icon-search"></i> 検索する</button>
                    </div>
                    <{if $list}>
                        <{include file="__pager.tpl"}>
                        <table class="table table-bordered table-hover table-condensed ldr-table">
                            <thead>
                                <tr>
                                    <th><a href="?order=no&desc=<{if $order === "no" && $desc === '0'}>1<{else}>0<{/if}>">(研修番号)</a><br/>研修名</th>
                                    <th>研修回<br/>(回数)</th>
                                    <th><a href="?order=training_date&desc=<{if $order === "training_date" && $desc === '0'}>1<{else}>0<{/if}>">開催<wbr/>日時</a></th>
                                    <th>
                                        <a href="?order=accept_date&desc=<{if $order === "accept_date" && $desc === '0'}>1<{else}>0<{/if}>">受付期間</a><wbr/>
                                        <a href="?order=deadline_date&desc=<{if $order === "deadline_date" && $desc === '0'}>1<{else}>0<{/if}>">(締切期間)</a>
                                    </th>
                                    <th>開催<wbr/>場所</th>
                                    <th>申込<wbr/>人数</th>
                                    <th>印刷</th>
                                    <th>受付</th>
                                    <th>出欠</th>
                                    <th>課題</th>
                                    <th>振返</th>
                                </tr>
                            </thead>
                            <tbody>
                                <{foreach item=row from=$list}>
                                    <tr>
                                        <td class="ldr-td-text-left">
                                            <a
                                                href="trn_inside_contents_modal.php?id=<{$row.inside_id|escape}>"
                                                data-toggle="modal"
                                                data-backdrop="true"
                                                data-target="#inside_contents">
                                                <i class="icon-external-link-sign"></i> <{if $row.no}>(<{$row.no|escape}>)<br/><{/if}>
                                                <{$row.title|escape}>
                                            </a>
                                        </td>
                                        <td class="ldr-td-name">
                                            <a
                                                href="trn_inside_number_modal.php?id=<{$row.inside_id|escape}>&number=<{$row.number|escape}>&date_id=<{$row.inside_date_id|escape}>"
                                                class="btn btn-small btn-success"
                                                data-toggle="modal"
                                                data-backdrop="true"
                                                data-target="#inside_number">
                                                第<{$row.number|escape}>回
                                            </a>
                                            <br/>(全<{$row.number_of|escape}>回)
                                        </td>
                                        <{if $row.type === '1'}>
                                            <td class="ldr-td-date"><span class="ldr-td-date-bold" title="<{$row.training_date|date_format_jp:"%Y年%-m月%-d日(%a)"}>"><{$row.training_date|date_format_jp:"%-m月%-d日(%a)"}></span><br /><{$row.training_start_time|date_format:"%R"}>〜<{$row.training_last_time|date_format:"%R"}></td>
                                        <{else}>
                                            <td class="ldr-td-name"><span class="label label-success"><{$ojt_name|escape}></span></td>
                                            <{/if}>
                                        <td class="ldr-td-date">
                                            <span title="<{$row.accept_date|date_format_jp:"%Y年%-m月%-d日(%a)"}>"><{$row.accept_date|date_format_jp:"%-m月%-d日(%a) %R"}></span> 〜 <span title="<{$row.deadline_date|date_format_jp:"%Y年%-m月%-d日(%a)"}>"><{$row.deadline_date|date_format_jp:"%-m月%-d日(%a) %R"}></span>
                                            <br/>
                                            <{if $row.reception==='0'}>
                                                <{assign var='lbl' value='label-info'}>
                                            <{elseif $row.reception < '0'}>
                                                <{assign var='lbl' value='label-warning'}>
                                            <{else}>
                                                <{assign var='lbl' value='label-important'}>
                                            <{/if}>
                                            <span class="label <{$lbl}>"><{$row.reception_text|escape}></span>
                                        </td>
                                        <{if $row.type === '1'}>
                                            <td class="ldr-td-text-center"><{$row.place|escape}></td>
                                        <{else}>
                                            <td class="ldr-td-name"><span class="label label-success"><{$ojt_name|escape}></span></td>
                                            <{/if}>

                                        <td class="ldr-td-name">
                                            <div rel="popover" data-trigger="hover" data-html="true" data-placement="top" data-animation="true"
                                                 <{if $row.count > 0}>
                                                     data-content="
                                                     <div class='ldr-popover label label-important'>受付待: <strong><{$row.count_2|escape}></strong>名</div><br/>
                                                     <div class='ldr-popover label label-warning'>取下げ: <strong><{$row.count_3c|escape}></strong>名</div><br/>
                                                     <div class='ldr-popover label label-info'>受付済: <strong><{$row.count_3|escape}></strong>名</div><br/>
                                                     <div class='ldr-popover label label-success'>出席数: <strong><{$row.count_0_1|escape}></strong>名</div><br/>
                                                     <div class='ldr-popover label label-inverse'>欠席数: <strong><{$row.count_0_2|escape}></strong>名</div><br/>
                                                     <div class='ldr-popover label label-important'>未承認: <strong><{$row.count_1|escape}></strong>名</div><br/>
                                                     "
                                                 <{/if}>
                                                 >
                                                <{$row.count|escape}>名
                                                <{if $row.count_2 > 0}>
                                                    <br/><span class="label label-important">受付待 <{$row.count_2|escape}>名</span>
                                                <{elseif $row.count > 0 and $row.count_2 === '0'}>
                                                    <{if $row.reception === '4'}>
                                                        <{if $row.count_3 > 0}>
                                                            <br/><span class="label label-success">出欠待 <{$row.count_3|escape}>名</span>
                                                        <{else}>
                                                            <br/><span class="label label-success">完了</span>
                                                        <{/if}>
                                                    <{else}>
                                                        <br/><span class="label label-info">受付待 <{$row.count_2|escape}>名</span>
                                                    <{/if}>
                                                <{/if}>
                                            </div>
                                        </td>
                                        <td class="ldr-td-name">
                                            <{if $row.count > 0}>
                                                <a href="javascript:void(0);" name="btn_print" data-target="inside_print" data-form="new" data-id="<{$row.inside_id}>_<{$row.inside_date_id}>" data-php="trn_accept_inside_print.php" class="js-form-print i_link"><i class="icon-print"></i>
                                                </a>
                                            <{else}>
                                                -
                                            <{/if}>
                                        </td>
                                        <td class="ldr-td-name">
                                            <{if $emp->is_training() or $row.is_planner}>
                                                <a href="javascript:void(0);" data-php="trn_accept_inside_edit.php" data-mode="edit" data-id="<{$row.inside_id}>_<{$row.inside_date_id}>" class="js-form-button i_link"><i class="icon-cog"></i>
                                                </a>
                                            <{else}>
                                                -
                                            <{/if}>
                                        </td>
                                        <td class="ldr-td-name">
                                            <{if $emp->is_training() or $row.is_planner}>
                                                <a href="javascript:void(0);" data-php="trn_accept_inside_attendance.php" data-mode="edit" data-id="<{$row.inside_id}>_<{$row.inside_date_id}>" class="js-form-button i_link"><i class="icon-group"></i>
                                                </a>
                                            <{else}>
                                                -
                                            <{/if}>
                                        </td>
                                        <td class="ldr-td-name">
                                            <{if $row.before_work === 't' or $row.after_work === 't'}>
                                                <a href="javascript:void(0);" data-php="trn_accept_inside_report.php" data-mode="view" data-id="<{$row.inside_id}>_<{$row.inside_contents_id}>" class="js-form-button i_link"><i class="icon-file-text"></i>
                                                </a>
                                            <{else}>
                                                -
                                            <{/if}>
                                        </td>
                                        <td class="ldr-td-name">
                                            <{if $row.looking_back === 't'}>
                                                <a href="javascript:void(0);" data-php="trn_accept_inside_check.php" data-mode="view" data-id="<{$row.inside_id}>_<{$row.inside_contents_id}>" class="js-form-button i_link"><i class="icon-file"></i></a>
                                                <{else}>
                                                -
                                            <{/if}>

                                        </td>
                                    </tr>
                                <{/foreach}>
                            </tbody>
                        </table>
                    <{else}>
                        <div class="control-group">
                        </div>
                        <div class="alert alert-error">院内研修はありません</div>
                    <{/if}>
                </form>
            </div>
        </div>
        <{include file="__modal_view.tpl" modal_id="inside_contents" modal_title="研修内容"}>
        <{include file="__modal_view.tpl" modal_id="inside_number" modal_title="研修内容"}>
        <{include file="__js.tpl"}>
        <{include form_selector="#inside_form" file="__pager_js.tpl"}>

        <script type="text/javascript">
            $(function() {

                $('#srh_keyword').keypress(function(ev) {
                    if ((ev.which && ev.which === 13) || (ev.keyCode && ev.keyCode === 13)) {
                        $('#search').trigger("click");
                    }
                });

                $('.js-srh-filter').change(function() {
                    $('#inside_form')
                        .append($('<input></input>').attr({type: 'hidden', name: 'search', value: 'on'}))
                        .submit();
                });
            });

        </script>

    </body>
</html>
