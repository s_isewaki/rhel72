<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 院内研修マスタ | 研修日程入力</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='院内研修マスタ 研修日程入力' hd_sub_title=''}>

            <div class="container-fluid">
                <div class="row-fluid">
                    <fieldset>
                        <legend>
                            <a
                                href="trn_inside_contents_modal.php?id=<{$inside_id|escape}>"
                                data-toggle="modal"
                                data-backdrop="true"
                                data-target="#inside_contents">
                                <{if $no}>(<{$no|escape}>) <{/if}><{$title|escape}>
                            </a>
                        </legend>

                        <form class="form" method="post">
                            <input type="hidden" id="inside_id" name="id" value="<{$inside_id|escape}>" />
                            <input type="hidden" id="inside_date_id" name="inside_date_id" value="" />
                            <input type="hidden" id="applied_count" name="applied_count" value="" />

                            <div class="control-group">
                                <label class="control-label">
                                    <strong>研修回</strong>
                                    <span class="badge badge-important required">必須</span>
                                </label>
                                <div class="controls form-inline">
                                    <select id="inside_contents_id" name="inside_contents_id">
                                        <{foreach item=row from=$contents}>
                                            <option value="<{$row.inside_contents_id|escape}>">第<{$row.number|escape}>回 <{$row.contents|escape}></option>
                                        <{/foreach}>
                                    </select>
                                </div>
                                <{foreach from=$error.inside_contents_id item=msg}>
                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                <{/foreach}>
                            </div>

                            <div class="control-group">
                                <label class="control-label">
                                    <strong>研修の種類</strong>
                                    <span class="badge badge-important required">必須</span>
                                </label>
                                <div class="controls">
                                    <label class="radio"><input type="radio" name="type" value="1" class="js-type-click" />集合研修(開催場所、開催日時を設定する)</label>
                                    <label class="radio"><input type="radio" name="type" value="2" class="js-type-click" /><{$ojt_name|escape}>(開催場所、開催日時を設定しない)</label>
                                </div>
                                <{foreach from=$error.type item=msg}>
                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                <{/foreach}>
                            </div>

                            <div class="control-group js-type1 <{if $type === '2'}>hide<{/if}>">
                                <label class="control-label">
                                    <strong>開催場所</strong>
                                    <span class="badge badge-important required">必須</span>
                                </label>
                                <div class="controls form-inline">
                                    <input type="text" name="place" />
                                </div>
                                <{foreach from=$error.place item=msg}>
                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                <{/foreach}>
                            </div>

                            <div class="control-group js-type1 <{if $type === '2'}>hide<{/if}>">
                                <label class="control-label">
                                    <strong>最大人数</strong>
                                    <span class="badge badge-important required">必須</span>
                                </label>
                                <div class="input-append">
                                    <input type="text" name="maximum" class="input-small onlynum text-right" maxlength="4"/>
                                    <span class="add-on">人</span>
                                </div>
                                <{foreach from=$error.maximum item=msg}>
                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                <{/foreach}>
                            </div>

                            <div class="control-group js-type1 <{if $type === '2'}>hide<{/if}>">
                                <label class="control-label" for="meeting_date">
                                    <strong>開催日時</strong>
                                    <span class="badge badge-important required">必須</span>
                                </label>
                                <div class="controls">
                                    <{include file="__datepicker.tpl" date_type="date" date_id="training_date" date_name="training_date" date_remove="off"}>

                                    <{include file="__datepicker.tpl" date_type="time" date_id="training_start_time" date_name="training_start_time" date_remove="off"}>
                                    <span>&nbsp;〜&nbsp;</span>
                                    <{include file="__datepicker.tpl" date_type="time" date_id="training_last_time" date_name="training_last_time" date_remove="off"}>
                                </div>
                                <{foreach from=$error.training_date item=msg}>
                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                <{/foreach}>
                            </div>

                            <div class="control-group">
                                <label class="control-label">
                                    <strong>受付開始日時</strong>
                                    <span class="badge badge-important required">必須</span>
                                </label>
                                <div class="controls">
                                    <{include file="__datepicker.tpl" date_type="date" date_id="accept_date" date_name="accept_date" date_remove="off"}>
                                    <{include file="__datepicker.tpl" date_type="time" date_id="accept_time" date_name="accept_time" date_remove="off" date_value="00:00"}>
                                </div>
                                <{foreach from=$error.accept_date item=msg}>
                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                <{/foreach}>
                            </div>

                            <div class="control-group">
                                <label class="control-label">
                                    <strong>締切日時</strong>
                                    <span class="badge badge-important required">必須</span>
                                </label>
                                <div class="controls">
                                    <{include file="__datepicker.tpl" date_type="date" date_id="deadline_date" date_name="deadline_date" date_remove="off"}>
                                    <{include file="__datepicker.tpl" date_type="time" date_id="deadline_time" date_name="deadline_time" date_remove="off" date_value="12:00"}>
                                </div>
                                <{foreach from=$error.deadline_date item=msg}>
                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                <{/foreach}>
                            </div>

                            <div class="control-group js-type1 <{if $type === '2'}>hide<{/if}>">
                                <label class="control-label">
                                    <strong>受付人数が最大人数を超えた場合</strong>
                                    <span class="badge badge-important required">必須</span>
                                </label>
                                <div class="controls form-inline">
                                    <label class="radio inline"><input type="radio" name="overflow" value="t" />受付を続ける</label>
                                    <label class="radio inline"><input type="radio" name="overflow" value="f" />受け付けないで終了とする</label>
                                </div>
                                <{foreach from=$error.overflow item=msg}>
                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                <{/foreach}>
                            </div>

                            <div class="control-group">
                                <label class="control-label">
                                    <strong>ステータス</strong>
                                    <span class="badge badge-important required">必須</span>
                                </label>
                                <div class="controls form-inline">
                                    <label class="radio inline"><input type="radio" name="status" value="1" />受付可</label>
                                    <label class="radio inline"><input type="radio" name="status" value="0" />終了(受付期間内でも受付不可)</label>
                                    <label class="radio inline"><input type="radio" name="status" value="2" />準備中</label>
                                </div>
                                <{foreach from=$error.accept_status item=msg}>
                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                <{/foreach}>
                            </div>

                            <div class="btn-toolbar">
                                <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                                <{if $smarty.post.inside_date_id}>
                                    <button type="submit" name="btn_save" id="btn_update" value="save" class="btn btn-primary btn-small" ><i class="icon-save"></i> 変更する</button>
                                    <button type="button" id="btn_copy" class="btn btn-danger btn-small"><i class="icon-copy"></i> この内容をコピーする</button>
                                    <button type="submit" id="btn_remove" name="btn_save" value="remove" class="btn btn-small btn-danger"><i class="icon-trash"></i> 削除する</button>
                                    <a id="emp_list" href="trn_inside_emp_list_modal.php?inside_date_id=<{$smarty.post.inside_date_id}>" data-toggle="modal" data-target="#emp_modal" class="btn btn-small btn-warning"><i class="icon-list-ol"></i> 申請者を確認する</a>
                                <{else}>
                                    <button type="submit" name="btn_save" id="btn_save" value="save" class="btn btn-primary btn-small" ><i class="icon-save"></i> 登録する</button>
                                <{/if}>
                            </div>
                        </form>
                    </fieldset>
                </div>
            </div>
        </div>
        <{include file="__modal_view.tpl" modal_id="inside_contents" modal_title="研修内容"}>
        <{include file="__modal_view.tpl" modal_id="emp_modal" modal_title="職員一覧"}>
        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {
                $('.js-type-click').click(function() {
                    if ($(this).val() === '1') {
                        $('.js-type1').show();
                    }
                    else {
                        $('.js-type1').hide();
                    }
                });

                $('#btn_update').click(function() {
                    if ($('#applied_count').val() > 0) {
            <{if !$error}>
                        if (!confirm("申請者が存在します。変更してもよろしいですか？")) {
                            return false;
                        }
            <{/if}>
                    }
                    return true;
                });

                $('#btn_remove').click(function() {
                    if ($('#applied_count').val() > 0) {
                        if (!confirm("申請者が存在します。削除してもよろしいですか？")) {
                            return false;
                        }
                    } else {
                        if (!confirm("削除してもよろしいですか？")) {
                            return false;
                        }
                    }
                    return true;
                });

                $('#training_start_time').parent(".timepicker").on('changeDate', function(e) {
                    if ($('#training_start_time').val() > $('#training_last_time').val()) {
                        $('#training_last_time').val($('#training_start_time').val());
                        $('#training_last_time').change();
                    }
                });

                // コピー
                $('#btn_copy').click(function() {
                    $('#inside_date_id').val('');
                    $('#inside_contents_id').val('');
                    $('#applied_count').val('');
                    $('#btn_update').html('<i class="icon-save"></i> 登録する');
                    $('#btn_copy').remove();
                    $('#btn_remove').remove();
                    $('#emp_list').remove();
                });
            });
        </script>
    </body>
</html>
