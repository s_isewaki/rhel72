<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 院外研修 講座</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='院外研修 講座' hd_sub_title=''}>

            <div class="container-fluid">
                <div class="row-fluid">
                    <form method="post" id="outside_form">
                        <button type="button" data-php="trn_outside_apply.php" data-mode="new" class="js-form-button btn btn-small btn-primary"><i class="icon-plus"></i> 院外研修を申し込む</button>
                        <{if $list}>
                            <{include file="__pager.tpl"}>
                            <table class="table table-bordered table-hover table-condensed ldr-table">
                                <thead>
                                    <tr>
                                        <th>開催日</th>
                                        <th>(研修番号)<br/>研修名</th>
                                        <th>主催</th>
                                        <th>申込人数</th>
                                        <th>申込</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <{foreach item=row from=$list}>
                                        <tr>
                                            <td class="ldr-td-date">
                                                <span title="<{$row.start_date|date_format_jp:"%Y年%-m月%-d日(%a)"}>"><{$row.start_date|date_format_jp:"%-m月%-d日(%a)"}></span>
                                                <{if $row.start_date <> $row.last_date}>
                                                    〜 <span title="<{$row.last_date|date_format_jp:"%Y年%-m月%-d日(%a)"}>"><{$row.last_date|date_format_jp:"%-m月%-d日(%a)"}></span>
                                                <{/if}>
                                            </td>
                                            <td class="ldr-td-text-left">
                                                <span rel="popover" data-trigger="hover" data-html="true" data-placement="top" data-animation="true"
                                                      data-content="
                                                      <div class='ldr-popover'>開催地: <strong><{$row.pref|default:'(未設定)'|escape}></strong></div><br/>
                                                      <div class='ldr-popover'>開催場所: <strong><{$row.place|default:'(未設定)'|escape}></strong></div>">
                                                    (<{$row.no|escape}>)<br/>
                                                </span>
                                                <{$row.contents|escape}>
                                            </td>
                                            <td class="ldr-td-text-center"><{$row.organizer|escape}></td>
                                            <td class="ldr-td-name"><{$row.count|escape}>名</td>
                                            <td class="ldr-td-name">
                                                <{if $row.applied >'0'}>
                                                    <span class="label label-info">申請済</span>
                                                <{else}>
                                                    <button type="button" data-php="trn_outside_apply.php" data-mode="apply" data-id="<{$row.outside_id}>" class="js-form-button btn btn-small btn-danger">申込</button>
                                                <{/if}>
                                            </td>
                                        </tr>
                                    <{/foreach}>
                                </tbody>
                            </table>

                        <{else}>
                            <div class="control-group">
                                <{*余白調整*}>
                            </div>
                            <div class="alert alert-error">受付中の院外研修はありません</div>
                        <{/if}>
                    </form>
                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <{include form_selector="#outside_form" file="__pager_js.tpl"}>
    </body>
</html>
