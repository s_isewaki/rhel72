<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 救護員研修 申込</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='救護員研修 申込' hd_sub_title=''}>

            <div class="container-fluid">
                <div class="row-fluid">
                    <form method="post">
                        <input type='hidden' id='id' name='id' value='' />
                        <input type='hidden' id='training_id' name='training_id' value='' />
                        <input type='hidden' id='relief_id' name='relief_id' value='' />
                        <input type='hidden' id='unit' name='unit' value='' />

                        <div class="well well-small">
                            <fieldset>
                                <legend >
                                    <{$relief.contents|escape}>  <span class="ldr-roman"><{$roman_num[$relief.unit]|escape}></span>
                                </legend>
                            </fieldset>
                            <div class="control-group">
                                <label class="control-label" for="rd_branch">
                                    <strong>支部名</strong>
                                    <span class="badge badge-important required">必須</span>
                                </label>
                                <div class="controls form-inline">
                                    <label class="radio inline"><input type="radio" name="rd_branch" value="1" /><{$branch_def}></label>
                                    <label class="radio inline"><input type="radio" name="rd_branch" value="2" />その他</label>
                                    &nbsp;<input type="text" id="branch" name="branch" class="inline input-large" maxlength="200"/>
                                </div>
                                <{foreach from=$error.branch item=msg}>
                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                <{/foreach}>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="rd_hospital">
                                    <strong>施設名</strong>
                                    <span class="badge badge-important required">必須</span>
                                </label>
                                <div class="controls form-inline">
                                    <label class="radio inline"><input type="radio" name="rd_hospital" value="1" /><{$hospital_def}></label>
                                    <label class="radio inline"><input type="radio" name="rd_hospital" value="2" />その他</label>
                                    &nbsp;<input type="text" id="hospital" name="hospital" class="inline input-large" maxlength="200"/>
                                </div>
                                <{foreach from=$error.hospital item=msg}>
                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                <{/foreach}>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="training_day">
                                    <strong>開催日</strong>
                                    <span class="badge badge-important required">必須</span>
                                </label>
                                <div class="controls form-inline">
                                    <{include file="__datepicker.tpl" date_id="training_day" date_name="training_day"}>
                                </div>
                                <{foreach from=$error.training_day item=msg}>
                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                <{/foreach}>
                            </div>
                            <{if $relief.detail}>
                                <div class="control-group">
                                    <label class="control-label">
                                        <strong>教科内容</strong>
                                    </label>
                                    <div class="controls">
                                        <span class="ldr-uneditable input-xxlarge"><{$relief.detail|escape|nl2br}></span>
                                    </div>
                                </div>
                            <{/if}>
                            <div class="control-group">
                                <label class="control-label">
                                    <strong>時間</strong>
                                </label>
                                <div class="controls">
                                    <span class="ldr-uneditable input-large"><{$relief.time|escape}></span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label"><strong>承認者</strong></label>
                                <{if isset($approver)}>
                                    <div class="controls"><span class="ldr-uneditable input-large"><{$approver->emp_name()|escape}></span></div>
                                    <{else}>
                                    <div class="alert alert-error">承認者が設定されていません。管理者にお問い合わせください。</div>
                                <{/if}>
                            </div>
                            <div class="btn-toolbar">
                                <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                                <{if $edit === 'apply'}>
                                    <{if isset($approver)}>
                                        <button type="submit" name="btn_save" id="btn_save" class="js-submit-apply btn btn-small btn-primary" value="save"><i class="icon-save"></i> 申請する</button>
                                    <{/if}>
                                <{elseif $edit === 'edit'}>
                                    <{if isset($approver)}>
                                        <button type="submit" name="btn_save" id="btn_save" class="js-submit-modify btn btn-small btn-primary" value="save"><i class="icon-save"></i> 修正する</button>
                                    <{/if}>
                                    <button type="submit" name="btn_save" id="btn_save" class="js-submit-withdraw btn btn-small btn-danger" value="cancel"><i class="icon-remove-sign"></i> 取り下げる</button>
                                <{/if}>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {
                $("input[name=rd_branch]").change(function() {
                    branch_status();
                });
                $("input[name=rd_hospital]").change(function() {
                    hospital_status();
                });

                branch_status();
                hospital_status();
            });
            function branch_status() {
                if ($("input[name=rd_branch]:checked").val() === '2') {
                    $('#branch').prop("disabled", false);
                } else {
                    $('#branch').prop("disabled", true);
                    $('#branch').val("");
                }
            }
            function hospital_status() {
                if ($("input[name=rd_hospital]:checked").val() === '2') {
                    $('#hospital').prop("disabled", false);
                } else {
                    $('#hospital').prop("disabled", true);
                    $('#hospital').val("");
                }
            }
        </script>
    </body>
</html>


