<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | <{$ldr.config.form_7.title|escape}>入力</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title="`$ldr.config.form_7.title`入力" hd_sub_title=$ldr.config.form_7.detailL}>

            <div class="container-fluid">
                <div class="row-fluid">
                    <div class="well well-small">
                        <form method="post">
                            <{include file="__form_view.tpl" op_srh='false' tmp_id="form_7edit"}>
                        </form>

                        <{if $ldr.config.word[$level]}>
                            <div class="well well-small">
                                <b><{$ldr.config.word[$level]|escape|nl2br}></b>
                            </div>
                        <{/if}>

                        <div class="tab-content">
                            <ul class="nav nav-tabs" id="group1Tab">
                                <{foreach item=row from=$group1_list}>
                                    <li class="<{if $active === $row.guideline_group1}>active<{/if}>">
                                        <a href="#g1-<{$row.guideline_group1|escape}>" data-toggle="tab">
                                            <i class="icon-folder-close"></i> <{$row.name|escape}>
                                        </a>
                                    </li>
                                <{/foreach}>
                                <{if $ldr.config.asm_comment_used === '1'}>
                                    <li class="<{if $active === 'comment'}>active<{/if}>">
                                        <a href="#comment" data-toggle="tab"><i class="icon-comment"></i> <{$ldr.config.asm_comment|default:'コメント'|escape}></a>
                                    </li>
                                <{/if}>
                                <form action="form_7edit.php" method="POST" class="js-form-import">
                                    <{*画面遷移用*}>
                                    <input type="hidden" name="mode" value="" />
                                    <input type="hidden" name="menu" value="" />
                                    <input type="hidden" name="id" value="" />
                                    <input type="hidden" name="target_emp_id" value=""/>
                                    <input type="hidden" name="owner_url" value="" />

                                    <{if $function.assessment}>
                                        <button type="submit" name="btn_save" value="import" class="btn btn-small btn-primary pull-right"><i class="icon-download-alt"></i> 自己評価からインポートする</button>
                                    <{/if}>

                                </form>
                            </ul>
                            <div class="tab-content">
                                <{foreach item=g1 from=$group1_list}>
                                    <div class="tab-pane <{if $active === $g1.guideline_group1}>active<{else}>fade<{/if}>" id="g1-<{$g1.guideline_group1|escape}>">
                                        <{if $g1.comment}>
                                            <div><i class="icon-folder-open"></i> <b><{$g1.comment|escape}></b></div>
                                            <hr/>
                                        <{/if}>

                                        <form class="js-form-assessment" action="form_7edit.php" method="POST">
                                            <input type="hidden" name="save" value="form72" />
                                            <{*画面遷移用*}>
                                            <input type="hidden" name="mode" value="" />
                                            <input type="hidden" name="menu" value="" />
                                            <input type="hidden" name="id" value="" />
                                            <input type="hidden" name="target_emp_id" value=""/>
                                            <input type="hidden" name="owner_url" value="" />
                                            <{*固有画面*}>
                                            <input type="hidden" name="active" value="<{$g1.guideline_group1|escape}>" />

                                            <{foreach item=asm from=$assessment_list[$g1.guideline_group1]}>
                                                <{assign var='asmkey' value="_`$asm.apply_assessment`"}>
                                                <{if $asm.guideline_group2 !== $back_g2}>
                                                    <label><strong><{$asm.group2_name|escape}></strong></label>
                                                    <{if $asm.group2_comment}> <div style="margin-left:1em; font-size:13px;"><{$asm.group2_comment|escape}></div><{/if}>
                                                    <{assign var="back_g2" value=`$asm.guideline_group2`}>
                                                <{/if}>
                                                <div id="as-<{$asm.guideline_id|escape}>" class="inline">
                                                    <div class="pull-left">
                                                        <input type="hidden" id="in-<{$asm.guideline_id|escape}>" class="input-assessment" name="assessment[<{$asm.guideline_id|escape}>]" value="<{$asm.apply_assessment|escape}>" data-pop="#bt-<{$asm.guideline_id|escape}>"/>
                                                        <{if $ldr.config.select|@count > 2}>
                                                            <div id="bt-<{$asm.guideline_id|escape}>" class="btn-group" data-toggle="buttons-radio" style="margin-top:5px;" data-toggle="tooltip" data-trigger="manual" data-placement="bottom" data-title="評価してください">
                                                                <{foreach item=sel from=$ldr.config.select}>
                                                                    <button type="button"
                                                                            class="btn btn-medium js-radio-assessment <{if $asm.apply_assessment === $sel.value}>btn-<{$sel.color}> active<{/if}>"
                                                                            data-input="#in-<{$asm.guideline_id|escape}>"
                                                                            data-alert="#al-<{$asm.guideline_id|escape}>"
                                                                            data-container="#as-<{$asm.guideline_id|escape}>"
                                                                            data-color="<{$sel.color|escape}>"
                                                                            value="<{$sel.value|escape}>"
                                                                            >
                                                                        <{$sel.button|escape}>
                                                                    </button>
                                                                <{/foreach}>
                                                            </div>
                                                        <{else}>
                                                            <button
                                                                type="button"
                                                                id="bt-<{$asm.guideline_id|escape}>"
                                                                class="btn btn-large js-button-assessment btn-<{$ldr.config.select[$asmkey].color|default:'default'}>"
                                                                data-input="#in-<{$asm.guideline_id|escape}>"
                                                                data-alert="#al-<{$asm.guideline_id|escape}>"
                                                                data-trigger="manual"
                                                                data-placement="bottom"
                                                                data-container="#as-<{$asm.guideline_id|escape}>"
                                                                data-title="評価してください"
                                                                data-toggle="tooltip">
                                                                <{$ldr.config.select[$asmkey].button|default:'<i class="icon-question"></i>'}>
                                                            </button>
                                                        <{/if}>

                                                    </div>
                                                    <div class="clearfix" style="padding: 1px 0 0 <{$padleft|escape}>;">
                                                        <div
                                                            id="al-<{$asm.guideline_id|escape}>"
                                                            class="alert alert-<{$ldr.config.select[$asmkey].color|default:'default'}>">
                                                            <{$asm.guideline|escape}>
                                                        </div>
                                                        <{if $asm.criterion}>
                                                            <label class="control-label"><strong>判断基準</strong></label>
                                                            <p class="alert alert-default"><{$asm.criterion|escape}></p>
                                                        <{/if}>
                                                        <{if $ldr.config.reason === '1'}>
                                                            <label class="control-label"><strong>根拠</strong></label>
                                                            <textarea class="input-maxlarge js-autoheight" name="reason[<{$asm.guideline_id|escape}>]" placeholder="根拠を入力してください"><{$asm.apply_reason|escape}></textarea>
                                                        <{/if}>
                                                    </div>
                                                </div>

                                                <hr style="margin:8px 0;"/>
                                            <{/foreach}>
                                            <div class="btn-toolbar">
                                                <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                                                <button type="submit" name="btn_save" id="btn_save" class="btn btn-small btn-primary" value="72_save"><i class="icon-save"></i> 保存する</button>
                                            </div>
                                            <p><small><i class="icon-exclamation-sign"></i> 入力内容は自動保存されます。明示的に保存したい場合は<b>[保存する]ボタン</b>で保存してください。</small></p>
                                        </form>
                                    </div>
                                <{/foreach}>
                                <{if $ldr.config.asm_comment_used === '1'}>
                                    <div class="tab-pane <{if $active === 'comment'}>active<{else}>fade<{/if}>" id="comment">
                                        <form id="form71" class="form" method="POST">
                                            <input type="hidden" name="save" value="form71" />
                                            <{*画面遷移用*}>
                                            <input type="hidden" name="mode" value="" />
                                            <input type="hidden" name="menu" value="" />
                                            <input type="hidden" name="id" value="" />
                                            <input type="hidden" name="target_emp_id" value=""/>
                                            <input type="hidden" name="owner_url" value="" />
                                            <input type="hidden" name="decision" value="" />
                                            <input type="hidden" name="action" value="" />
                                            <input type="hidden" name="result" value="" />
                                            <input type="hidden" name="comment5" value="" />
                                            <input type="hidden" name="comment6" value="" />
                                            <input type="hidden" name="comment7" value="" />
                                            <{*固有画面*}>
                                            <input type="hidden" name="active" value="comment" />
                                            <div class="control-group">
                                                <label class="control-label" for="knowledge">
                                                    <strong><{$ldr.config.asm_comment|default:'コメント'|escape}></strong>
                                                    <{if $ldr.config.asm_comment_required}><span class="badge badge-important required">必須</span><{/if}>
                                                </label>
                                                <p><{$ldr.config.asm_comment_guide|escape}></p>
                                                <textarea id="knowledge" name="knowledge" rows="2" class="input-maxlarge js-autoheight" placeholder="<{$ldr.config.asm_comment|default:'コメント'|escape}>を記入してください" <{if $ldr.config.asm_comment_required}>required<{/if}>></textarea>
                                                <{foreach from=$error.knowledge item=msg}>
                                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                                <{/foreach}>
                                            </div>

                                            <{if $ldr.config.asm_comment2_used === '1'}>
                                                <div class="control-group">
                                                    <label class="control-label" for="decision">
                                                        <strong><{$ldr.config.asm_comment2|default:'自己の課題'|escape}></strong>
                                                        <{if $ldr.config.asm_comment2_required}><span class="badge badge-important required">必須</span><{/if}>
                                                    </label>
                                                    <p><{$ldr.config.asm_comment2_guide|escape}></p>
                                                    <textarea id="decision" name="decision" rows="2" class="input-maxlarge js-autoheight" placeholder="<{$ldr.config.asm_comment2|default:'自己の課題'|escape}>を記入してください" <{if $ldr.config.asm_comment2_required}>required<{/if}>></textarea>
                                                    <{foreach from=$error.decision item=msg}>
                                                        <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                                    <{/foreach}>
                                                </div>
                                            <{/if}>

                                            <div class="btn-toolbar">
                                                <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                                                <button type="submit" name="btn_save" id="btn_save" class="btn btn-small btn-primary <{if $ldr.config.asm_comment_required || $ldr.config.asm_comment2_required}>js-submit-required<{/if}>" value="71_save"><i class="icon-save"></i> 保存する</button>
                                            </div>
                                            <p><small><i class="icon-exclamation-sign"></i> 入力内容は自動保存されます。明示的に保存したい場合は<b>[保存する]ボタン</b>で保存してください。</small></p>
                                        </form>
                                    </div>
                                <{/if}>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {
                $('body').on('click', 'button.js-button-assessment', function() {
                    $(this).tooltip('destroy');

                    var input = $($(this).data('input'));
                    var alert = $($(this).data('alert'));

            <{if $ldr.config.select|@count <= 2}>
                    var value = '';
                <{foreach item=sel from=$ldr.config.select|@array_reverse}>
                    if (input.val() + '' !== '<{$sel.value|escape:'javascript'}>') {
                        $(this)
                            .html('<{$sel.button|escape:'javascript'}>')
                            .attr('class', 'js-button-assessment btn btn-large btn-<{$sel.color|escape:'javascript'}>');
                        alert.attr('class', 'alert alert-<{$sel.color|escape:'javascript'}>');
                        value = '<{$sel.value|escape:'javascript'}>';
                    }
                <{/foreach}>
                    input.val(value).change();
            <{/if}>
                });

                $('body').on('click', 'button.js-radio-assessment', function() {
                    $(this).parent('div').tooltip('destroy');

                    var input = $($(this).data('input'));
                    var alert = $($(this).data('alert'));
                    input.val($(this).attr('value')).change();

                    $(this).siblings().removeClass('btn-primary');
                    $(this).siblings().removeClass('btn-info');
                    $(this).siblings().removeClass('btn-success');
                    $(this).siblings().removeClass('btn-warning');
                    $(this).siblings().removeClass('btn-danger');
                    $(this).addClass('btn-' + $(this).data('color'));
                    alert.attr('class', 'alert alert-' + $(this).data('color'));
                });

                $('body').on('submit', 'form.js-form-assessment', function() {
                    // validate
                    var error = false;
                    $(this).find('input.input-assessment').each(function(idx, input) {
                        var inp = $(input);
                        if (inp.val() === '') {
                            $(inp.data('pop')).tooltip('show');
                            error = true;

                        }
                    });
                    if (error) {
                        return false;
                    }
                });

                $('form.js-form-import').submit(function() {
                    if (confirm('入力済みの評価が上書きされますがよろしいですか？')) {
                        return true;
                    }
                    return false;
                });

                /*
                 * コメント必須
                 */
                $('.js-submit-required').click(function() {
            <{if $ldr.config.asm_comment_required}>
                    if ($('#knowledge').val() === '') {
                        alert('<{$ldr.config.asm_comment|default:'コメント'|escape}>を必ず入力してください');
                        return false;
                    }
            <{/if}>
            <{if $ldr.config.asm_comment2_required}>
                    if ($('#decision').val() === '') {
                        alert('<{$ldr.config.asm_comment2|default:'自己の課題'|escape}>を必ず入力してください');
                        return false;
                    }
            <{/if}>
                    return true;
                });

                // 自動保存
                $('#form71').autosave({
                    callbacks: {
                        trigger: 'modify',
                        scope: 'all',
                        save: {
                            method: 'ajax',
                            options: {
                                url: 'form_7edit.php',
                                method: 'post'
                            }
                        }
                    }
                });
                $('form.js-form-assessment').autosave({
                    callbacks: {
                        trigger: 'change',
                        scope: 'all',
                        save: {
                            method: 'ajax',
                            options: {
                                url: 'form_7edit.php',
                                method: 'post'
                            }
                        }
                    }
                });
            });
        </script>
    </body>
</html>