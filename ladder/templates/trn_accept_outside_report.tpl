<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 院外研修報告一覧</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='院外研修報告一覧' hd_sub_title=''}>

            <div class="container-fluid">
                <form method="post" id="outside_form">
                    <input type='hidden' id='id' name='id' value='<{$smarty.post.id}>' />
                    <div class="well well-small">
                        <fieldset>
                            <legend >
                                <span  rel="popover" data-trigger="hover" data-html="true" data-placement="bottom" data-animation="true"
                                       data-content="
                                       <div class='ldr-popover'>主　催: <strong><{$trn.organizer|escape}></strong></div>
                                       <div class='ldr-popover'>開催地: <strong><{$trn.pref|default:'(未設定)'|escape}></strong></div>
                                       <div class='ldr-popover'>場　所: <strong><{$trn.place|default:'(未設定)'|escape}></strong></div>
                                       <div class='ldr-popover'>開催日: <strong><{$trn.start_date|escape}>〜<{$trn.last_date|escape}></strong></div>
                                       " >
                                    (<{$trn.no|escape}>)
                                    <{$trn.contents|escape}>
                                </span>
                            </legend>
                            <div class="control-group">
                                <div class="controls">
                                    <label class="radio inline"><input type="radio" id="srh_report_0" name="srh_report" value="" class="srh"/>すべて</label>
                                    <label class="radio inline"><input type="radio" id="srh_report_1" name="srh_report" value="reported" class="srh"/>報告済み</label>
                                    <label class="radio inline"><input type="radio" id="srh_report_2" name="srh_report" value="nonreport" class="srh"/>未報告</label>
                                </div>
                            </div>
                            <{if $list}>
                                <table class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th>部署</th>
                                            <th>職員ID</th>
                                            <th>名前</th>
                                            <th>更新日</th>
                                            <th width="60px" style="text-align: center">出張報告</th>
                                            <th>学んだこと</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <{foreach item=row from=$list}>
                                            <tr>
                                                <td class="ldr-td-text-left">
                                                    <{$row.class_full_name|escape}>
                                                </td>
                                                <td class="ldr-td-text"><{$row.emp_personal_id|escape}></td>
                                                <td class="ldr-td-text-left"><{$row.name|escape}></td>
                                                <td class="ldr-td-text-left"><{$row.updated_on|date_format_jp:"%Y年%-m月%-d日(%a) %R"}></td>
                                                <td class="ldr-td-name">
                                                    <{if $row.report === '1'}>
                                                        <span class="label label-success">提出</span>
                                                    <{elseif $row.report === '2'}>
                                                        <span class="label label-warning">無し</span>
                                                    <{elseif $row.report === '3'}>
                                                        <span class="label label-inverse">欠席</span>
                                                    <{else}>
                                                        <span class="label label-important">未報告</span>
                                                    <{/if}>
                                                </td>
                                                <td class="ldr-td-text-left"><{$row.learning|escape|nl2br}></td>
                                            </tr>
                                        <{/foreach}>
                                    </tbody>
                                </table>

                            <{else}>
                                <div class="alert alert-error">対象者がいません</div>
                            <{/if}>
                        </fieldset>
                        <div class="btn-toolbar">
                            <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {
            <{if $smarty.post.srh_report === "reported"}>
                $('#srh_report_1').prop('checked', true);
            <{elseif $smarty.post.srh_report === "nonreport"}>
                $('#srh_report_2').prop('checked', true);
            <{else}>
                $('#srh_report_0').prop('checked', true);
            <{/if}>
            });

            $(function() {
                $('.srh').change(function() {
                    $('#outside_form').submit();
                });
            });
        </script>
    </body>
</html>
