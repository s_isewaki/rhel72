<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | <{$ldr.config.status[23]|escape}>入力</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title=$ldr.config.status[23]}>

            <div class="container-fluid">
                <form method="post">
                    <{include file="__form_view.tpl" op_srh='false' tmp_id="form_6view"}>
                </form>
                <div class="well well-small">

                    <ul class="nav nav-tabs">
                        <li class="<{if $active !== 'assessment'}>active<{/if}>"><a href="#report" data-toggle="tab"><i class="icon-file-text-alt"></i> レポート</a></li>
                        <li class="<{if $active === 'assessment'}>active<{/if}>"><a href="#assessment" data-toggle="tab"><i class="icon-check"></i> 評価</a></li>
                    </ul>

                    <div class="control-group">
                        <div class="input-prepend" style="margin-bottom:0;">
                            <span class="add-on">申請レベル</span>
                            <span class="ldr-uneditable input-mini ldr-level" style="margin-bottom:0;"><{$apply->level_text()|escape}></span>
                        </div>
                        <div class="input-prepend" style="margin-bottom:0;">
                            <span class="add-on">申請者</span>
                            <span class="ldr-uneditable input-large" style="margin-bottom:0;"><{$target_emp->emp_name()|escape}> (<{$target_emp->class_full_name()|escape}>)</span>
                        </div>
                    </div>

                    <div class="tab-content">
                        <div class="tab-pane <{if $active !== 'assessment'}>active<{else}>fade<{/if}>" id="report">
                            <hr style="margin:8px 0;"/>

                            <div class="control-group">
                                <label class="control-label">
                                    <strong>評価日</strong>
                                </label>
                                <div class="controls">
                                    <span class="ldr-uneditable input-medium"><{$form6.updated_on|date_format_jp:"%Y年%-m月%-d日(%a)"}></span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    <strong>事例内容</strong>
                                </label>
                                <div class="controls">
                                    <span class="input-maxlarge ldr-uneditable"><{$form6.narrative|escape|nl2br}></span>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane <{if $active === 'assessment'}>active<{else}>fade<{/if}>" id="assessment">

                            <{include file="__save_message.tpl" msg_text='確定しました'}>

                            <{foreach from=$error.assessment item=msg}>
                                <p class="alert alert-danger"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                            <{/foreach}>

                            <div class="control-group">
                                <div class="input-prepend" style="margin-bottom:0;">
                                    <span class="add-on">評価者</span>
                                    <span class="ldr-uneditable input-large" style="margin-bottom:0;"><{$assessment.emp_name|escape}></span>
                                </div>
                                <div class="input-prepend" style="margin-bottom:0;">
                                    <span class="add-on">評価日</span>
                                    <span class="ldr-uneditable input-medium" style="margin-bottom:0;"><{$assessment.updated_on|date_format_jp:"%Y年%-m月%-d日(%a)"}></span>
                                </div>
                            </div>

                            <form action="" method="POST">
                                <input type="hidden" name="save" value="ajax" />
                                <{*画面遷移用*}>
                                <input type="hidden" name="mode" value="" />
                                <input type="hidden" name="menu" value="" />
                                <input type="hidden" name="id" value="" />
                                <input type="hidden" name="target_emp_id" value=""/>
                                <input type="hidden" name="owner_url" value="" />

                                <table class="table table-condensed table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th>項目</th>
                                            <th>内容</th>
                                            <th>評価</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <{foreach item=asm from=$assessment.list}>
                                            <{assign var='asmkey' value="_`$asm.apply_assessment`"}>
                                            <tr class="<{$ldr.config.report_select[$asmkey].color|default:'default'}>">
                                                <{if $asm.guideline_group2 !== $back_g2}>
                                                    <td class="ldr-td-text-left" rowspan="<{$asm.group2_count|escape}>"><{$asm.group2_name|escape}></td>
                                                    <{assign var="back_g2" value=`$asm.guideline_group2`}>
                                                <{/if}>
                                                <td class="ldr-td-text-left"><{$asm.guideline|escape}></td>
                                                <td><{$ldr.config.report_select[$asmkey].button|default:'<i class="icon-question"></i>'}></td>
                                            </tr>
                                        <{/foreach}>
                                    </tbody>
                                </table>

                                <hr style="margin:8px 0;"/>
                                <div class="control-group">
                                    <label>
                                        <strong>評価者からのメッセージ</strong>
                                    </label>
                                    <div class="controls">
                                        <span class="input-maxlarge ldr-uneditable"><{$assessment.comment|escape|nl2br}></span>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label>
                                        <strong>教育課からのメッセージ</strong>
                                        <span class="badge badge-important required">必須</span>
                                    </label>
                                    <div class="controls">
                                        <textarea name="admin_comment" rows="3" class="input-maxlarge js-autoheight"></textarea>
                                    </div>
                                </div>

                                <div class="btn-toolbar">
                                    <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                                    <{if $assessment.admin_completed !== 't'}>
                                        <button type="submit" name="btn_save" id="btn_save" class="btn btn-small btn-primary js-submit-decision" value="completed"><i class="icon-save"></i> 確認を確定する</button>
                                    <{else}>
                                        <button type="button" class="btn btn-small btn-primary" disabled="disabled"><i class="icon-save"></i> 確認完了済</button>
                                    <{/if}>
                                </div>
                                <p><small><i class="icon-exclamation-sign"></i> 入力内容は自動保存されます。確認を終える場合は<b>[確認を確定する]ボタン</b>で保存してください。</small></p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {
                // 自動保存
                $('form').autosave({
                    callbacks: {
                        trigger: 'modify',
                        scope: 'all',
                        save: {
                            method: 'ajax',
                            options: {
                                url: 'form_6assessment_confirm.php',
                                method: 'post'
                            }
                        }
                    }
                });
            });
        </script>
    </body>
</html>