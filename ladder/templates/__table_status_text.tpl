<{if $row.status === '8' && $row.report_status}>
    <{$row.ladder.config.status[$row.status]|escape}>
    <br/>
    (<{$row.ladder.config.status[$row.report_status]|escape}>)
<{elseif $row.report_status}>
    <{$row.ladder.config.status[$row.report_status]|escape}>
<{else}>
    <{$row.ladder.config.status[$row.status]|escape}>
<{/if}>
