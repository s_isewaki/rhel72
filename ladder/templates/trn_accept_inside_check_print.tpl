<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>
        <{css href="css/print.css"}>
        <{javascript src="js/flotr2.js"}>
        <!--[if lte IE 8]>
        <{javascript src="js/excanvas.js"}>
        <{javascript src="js/html5shiv-printshiv.min.js"}>
        <![endif]-->

        <title>CoMedix キャリア開発ラダー | 院内研修振り返り集計</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->

        <!-- メインエリア -->
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="pull-left">
                    <{if $trn.no}>(<{$trn.no|escape}>) <{/if}> <{$trn.title|escape}>
                    第<{$trn.contents.number|escape}>回 / 全<{$trn.number_of|escape}>回
                </div>
                <div class="pull-right text-right">
                    開催日時: <{foreach item=date from=$dates.dates}><{$date.training_date|date_format_jp:'%Y年%-m月%-d日(%a)'}> <{$date.training_start_time|date_format:'%R'}><br/><{/foreach}>
                </div>
            </div>

            <{foreach item=c from=$count}>
                <{if $c.form.type === 'textarea'}>
                    <{capture name="textarea"}>
                        <div class="block textarea">
                            <a href="#<{$c.key|escape}>" class="block-heading"><{$c.form.name|escape}></a>
                            <div id="<{$c.key|escape}>" class="block-body row-fluid">
                                <div class="span11">
                                    <table class="table table-condensed table-bordered">
                                        <tbody>
                                            <{foreach item=row from=$c.table name="table"}>
                                                <tr>
                                                    <td class="ldr-td-text-left"><strong><{$row.label|escape}></strong></td>
                                                    <td class="ldr-td-number" style="width:30px;"><{$row.value|escape}></td>
                                                </tr>
                                            <{/foreach}>
                                        </tbody>
                                        <tfoot>
                                            <tr class="info">
                                                <th class="ldr-td-text">合計</th>
                                                <td class="ldr-td-number" style="width:30px;"><{$c.total|escape}></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                            <div class="cleafix" style="display:none;"></div>
                        </div>
                    <{/capture}>
                    <{assign var="textarea" value="`$textarea``$smarty.capture.textarea`"}>
                <{else}>
                    <div class="block">
                        <a href="#<{$c.key|escape}>" class="block-heading"><{$c.form.name|escape}></a>
                        <div id="<{$c.key|escape}>" class="block-body row-fluid">
                            <div class="<{if $c.form.type !== 'textarea'}>span4<{else}>span11<{/if}>">
                                <table class="table table-condensed table-bordered">
                                    <tbody>
                                        <{foreach item=row from=$c.table name="table"}>
                                            <tr>
                                                <td class="ldr-td-text"><strong><{$row.label|escape}></strong></td>
                                                <td class="ldr-td-number" style="width:30px;"><{$row.value|escape}></td>
                                                <td class="ldr-td-number" style="width:35px;"><{math equation="x / y * 100" x=$row.value y=$c.count assign="p"}><{$p|string_format:"%.1f"}>%</td>
                                            </tr>
                                        <{/foreach}>
                                    </tbody>
                                    <tfoot>
                                        <tr class="info">
                                            <th class="ldr-td-text">合計</th>
                                            <td class="ldr-td-number" style="width:30px;"><{$c.total|escape}></td>
                                            <td class="ldr-td-number" style="width:35px;">-</td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div class="span6">
                                <div class="canvas" data-key="<{$c.key|escape}>" data-id="<{$trn.contents.inside_contents_id|escape}>"></div>
                            </div>
                        </div>
                        <div class="cleafix" style="display:none;"></div>
                    </div>
                <{/if}>
            <{/foreach}>
            <{if $textarea}>
                <{$textarea}>
            <{/if}>
        </div>

        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {
                var canvas = $('div.canvas');

                // チャート
                canvas.each(function(index) {
                    var canvas = $(this);
                    var url = 'trn_accept_inside_check_charts.php';
                    url += '?key=' + $(this).data('key');
                    url += '&id=' + $(this).data('id');
                    // chart
                    $.ajax({
                        url: url,
                        dataType: 'json',
                        type: 'post'
                    }).done(function(data) {
                        Flotr.draw(canvas.get(0), data.data, data.option);

                        if (index === canvas.length) {
                            setTimeout(function() {
                                window.print();
                                window.close();
                            }, 3000);
                        }
                    });
                });

                $('body').onafterprint = function() {
                    window.close();
                };
            });
        </script>
    </body>
</html>