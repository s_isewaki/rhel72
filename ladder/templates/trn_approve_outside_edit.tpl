<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | <{$hd_title}></title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" emp_name=$target_emp->emp_name() emp_class=$target_emp->class_full_name()}>

            <div class="container-fluid">
                <div class="well well-small">
                    <form id="form_edit" method="post">
                        <input type='hidden' id='training_id' name='training_id' value='<{$trn.training_id}>' />

                        <div class="control-group">
                            <label class="control-label">
                                <strong>主催</strong>
                            </label>
                            <div class="controls">
                                <span class="ldr-uneditable input-xlarge"><{$trn.organizer|escape}></span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                <strong>区分</strong>
                            </label>
                            <div class="controls">
                                <span class="ldr-uneditable input-medium"><{$type_text[$trn.type]|escape}></span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                <strong>経験年数</strong>
                            </label>
                            <div class="controls">
                                <span class="input-append">
                                    <span class="ldr-uneditable input-mini text-right"><{$trn.job_year|escape}></span>
                                    <span class="add-on">年</span>
                                </span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                <strong>開催日</strong>
                            </label>
                            <div class="controls">
                                <span class="ldr-uneditable"><{$trn.start_date|date_format_jp:"%Y年%-m月%-d日(%a)"}></span>
                                <{if $trn.start_date <> $trn.last_date}>
                                    〜
                                    <span class="ldr-uneditable"><{$trn.last_date|date_format_jp:"%Y年%-m月%-d日(%a)"}></span>
                                <{/if}>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                <strong>開催地</strong>
                            </label>
                            <div class="controls">
                                <span class="ldr-uneditable input-xlarge"><{$trn.pref|escape}></span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                <strong>開催場所</strong>
                            </label>
                            <div class="controls">
                                <span class="ldr-uneditable input-xlarge"><{$trn.place|escape}></span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                <strong>研修番号</strong>
                            </label>
                            <div class="controls">
                                <span class="input-prepend">
                                    <span class="add-on">No.</span>
                                    <span class="ldr-uneditable input-mini"><{$trn.no|escape}></span>
                                </span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                <strong>研修名</strong>
                            </label>
                            <div class="controls">
                                <span class="ldr-uneditable input-xxlarge"><{$trn.contents|escape}></span>
                            </div>
                        </div>

                        <div class="btn-toolbar">
                            <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                            <{if !$view}>
                                <button type="submit" name="btn_save" class="js-ot-btn js-submit-approve btn btn-small btn-primary" value="save"><i class="icon-edit"></i> 承認する</button>
                                <button type="submit" name="btn_save" id="btn_save" class="js-ot-btn js-submit-withdraw btn btn-small btn-danger" value="cancel"><i class="icon-remove-sign"></i> 取り下げる</button>
                            <{/if}>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
    </body>
</html>
