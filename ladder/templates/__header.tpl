<{* ヘッダー部分のテンプレート *}>
<div class="header breadcrumb">
    <h1 class="page-title"><{$hd_title|escape}></h1>
    <{if $hd_sub_title !== '' && $hd_title !== $hd_sub_title}>
        <p><{$hd_sub_title|escape}></p>
    <{/if}>
</div>
<{if $emp_name}>
    <div class="emp-header breadcrumb">
        <div class="emp-name"><{$emp_name|escape}></div>
        <div class="emp-class"><{$emp_class|escape}></div>
    </div>
<{/if}>
