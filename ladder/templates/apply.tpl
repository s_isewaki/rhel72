<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | <{$titles.ladder|default:'ラダー'|escape}>申請一覧</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">

            <{include file="__header.tpl" hd_title="`$titles.ladder`申請一覧" hd_sub_title=''}>

            <div class="container-fluid">
                <div class="row-fluid">
                    <form name="apply_form" id="apply_form" method="post">
                        <input type="hidden" id="mode" name="mode" value="" />
                        <input type="hidden" id="id" name="id" value="" />
                        <input type="hidden" id="menu" name="menu" value="apply" />
                        <input type="hidden" id="emp_id" name="emp_id" value="" />
                        <input type="hidden" id="target_emp_id" name="target_emp_id" value="" />
                        <input type="hidden" id="owner_url" name="owner_url" value="apply.php" />
                        <!--button type="button" data-php="apply_entry.php" data-mode="new" class="js-form-button btn btn-small btn-primary"><i class="icon-plus"></i> 申込書を書く</button-->
                        <{if $list}>
                            <{include file="__pager.tpl"}>
                            <{include file="__table_apply.tpl"}>
                        <{else}>
                            <div class="control-group">
                            </div>
                            <div class="alert alert-error">申請はありません</div>
                        <{/if}>
                    </form>
                </div>
            </div>
        </div>
        <{include file="__modal_status.tpl"}>
        <{include file="__modal_comment.tpl"}>
        <{include file="__modal_view.tpl" modal_id="result_confirm" modal_title="結果確認"}>

        <{include file="__js.tpl"}>
        <{include file="__modal_form_js.tpl"}>
        <{include form_selector="#apply_form" file="__pager_js.tpl"}>
    </body>
</html>