<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | <{$titles.form_5.title|escape}></title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title=$titles.form_5.title hd_sub_title=$titles.form_5.detailL}>

            <div class="container-fluid">
                <{*印刷ボタン*}>
                <form id="form_print" method="post" class="pull-right">
                    <button type="button" data-id="<{$smarty.post.id}>" data-emp="<{$smarty.post.target_emp_id}>" data-php="form_5view.php" data-target="form5" class="pull-right js-form-print btn btn-primary btn-small"><i class="icon-print"></i> 印刷する</button>
                </form>

                <{*様式共通ボタン*}>
                <form id="form_edit" method="post">
                    <{include file='__form_view.tpl' op_srh='true' tmp_id='form_5view'}>
                    <input type="hidden" id="fm5_active" name="fm5_active" value="">
                </form>


                <div class="well well-small">

                    <div class="control-group">
                        <div class="input-prepend" style="margin-bottom:0;">
                            <span class="add-on">申請レベル</span>
                            <span class="ldr-uneditable input-mini ldr-level" style="margin-bottom:0;"><{$apply->level_text()|escape}></span>
                        </div>
                        <div class="input-prepend" style="margin-bottom:0;">
                            <span class="add-on">申請者</span>
                            <span class="ldr-uneditable input-large" style="margin-bottom:0;"><{$target_emp->emp_name()|escape}> (<{$target_emp->class_full_name()|escape}>)</span>
                        </div>
                    </div>
                    <hr/>

                    <ul class="nav nav-tabs">
                        <{if $function.form_51}>
                            <li class="<{if $active === 'form_51'}>active<{/if}>">
                                <a href="#form_51" id="a_51" data-toggle="tab"><span class="ldr-roman">I.</span><{$titles.form_51.detail|escape}></a>
                            </li>
                        <{/if}>

                        <{if $function.form_52}>
                            <li class="<{if $active === 'form_52'}>active<{/if}>">
                                <a href="#form_52" id="a_52" data-toggle="tab"><span class="ldr-roman">II.</span><{$titles.form_52.detail|escape}></a>
                            </li>
                        <{/if}>

                        <{if $function.form_53}>
                            <li class="<{if $active === 'form_53'}>active<{/if}>">
                                <a href="#form_53" id="a_53" data-toggle="tab"><span class="ldr-roman">III.</span><{$titles.form_53.detail|escape}></a>
                            </li>
                        <{/if}>

                        <{if $function.form_54}>
                            <li class="<{if $active === 'form_54'}>active<{/if}>">
                                <a href="#form_54" id="a_54" data-toggle="tab"><span class="ldr-roman">IV.</span><{$titles.form_54.detail|escape}></a>
                            </li>
                        <{/if}>
                    </ul>
                    <div class="tab-content">
                        <{if $function.form_51}>
                            <div class="tab-pane <{if $active === 'form_51'}>active<{/if}>" id="form_51">
                                <div class="control-group">
                                    <h5><span class="ldr-roman">I.</span> <{$titles.form_51.detail|escape}></h5>
                                </div>
                                <table class="table table-striped table-hover table-bordered table-condensed ldr-table">
                                    <thead>
                                        <tr>
                                            <th>期間</th>
                                            <th nowrap>所属部署</th>
                                            <th>役割</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <{foreach item=row from=$list51}>
                                            <tr>
                                                <td class="ldr-td-view-from-to"><{$row.start_date|date_format_jp:"%Y年%-m月%-d日(%a)"}> 〜 <{$row.last_date|date_format_jp:"%Y年%-m月%-d日(%a)"}></td>
                                                <td class="ldr-td-text-center"><{$row.class|escape}></td>
                                                <td class="ldr-td-text-left"><{$row.role|escape|nl2br}></td>
                                            </tr>
                                        <{/foreach}>
                                    </tbody>
                                </table>
                            </div>
                        <{/if}>

                        <{if $function.form_52}>
                            <div class="tab-pane <{if $active === 'form_52'}>active<{/if}>" id="form_52">
                                <div class="control-group">
                                    <h5><span class="ldr-roman">II.</span> <{$titles.form_52.detail|escape}></h5>
                                </div>
                                <table class="table table-striped table-hover table-bordered table-condensed ldr-table">
                                    <thead>
                                        <tr>
                                            <th>期間</th>
                                            <th nowrap>種別</th>
                                            <th>対象</th>
                                            <th>テーマ／内容</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <{foreach item=row from=$list52}>
                                            <tr>
                                                <td class="ldr-td-view-from-to"><{$row.start_date|date_format_jp:"%Y年%-m月%-d日(%a)"}> 〜 <{$row.last_date|date_format_jp:"%Y年%-m月%-d日(%a)"}></td>
                                                <td class="ldr-td-text-center" nowrap><{$f52_types[$row.type]|escape}></td>
                                                <td class="ldr-td-text-left"><{$row.subject|escape}></td>
                                                <td class="ldr-td-text-left"><{$row.theme|escape}></td>
                                            </tr>
                                        <{/foreach}>
                                    </tbody>
                                </table>
                            </div>
                        <{/if}>

                        <{if $function.form_53}>
                            <div class="tab-pane <{if $active === 'form_53'}>active<{/if}>" id="form_53">
                                <div class="control-group">
                                    <h5><span class="ldr-roman">III.</span> <{$titles.form_53.detail|escape}></h5>
                                </div>
                                <table class="table table-striped table-hover table-bordered table-condensed ldr-table">
                                    <thead>
                                        <tr>
                                            <th>期間</th>
                                            <th nowrap>種別</th>
                                            <th>名称</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <{foreach item=row from=$list53}>
                                            <tr>
                                                <td class="ldr-td-view-from-to"><{$row.start_date|date_format_jp:"%Y年%-m月%-d日(%a)"}> 〜 <{$row.last_date|date_format_jp:"%Y年%-m月%-d日(%a)"}></td>
                                                <td class="ldr-td-text-center" nowrap><{$f53_types[$row.type]|escape}></td>
                                                <td class="ldr-td-text-left"><{$row.title|escape}></td>
                                            </tr>
                                        <{/foreach}>
                                    </tbody>
                                </table>
                            </div>
                        <{/if}>

                        <{if $function.form_54}>
                            <div class="tab-pane <{if $active === 'form_54'}>active<{/if}>" id="form_54">
                                <div class="control-group">
                                    <h5><span class="ldr-roman">IV.</span> <{$titles.form_54.detail|escape}></h5>
                                </div>
                                <table class="table table-striped table-hover table-bordered table-condensed ldr-table">
                                    <thead>
                                        <tr>
                                            <th>期間</th>
                                            <th>名称</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <{foreach item=row from=$list54}>
                                            <tr>
                                                <td class="ldr-td-view-from-to"><{$row.start_date|date_format_jp:"%Y年%-m月%-d日(%a)"}> 〜 <{$row.last_date|date_format_jp:"%Y年%-m月%-d日(%a)"}></td>
                                                <td class="ldr-td-text-left"><{$row.title|escape}></td>
                                            </tr>
                                        <{/foreach}>
                                    </tbody>
                                </table>
                            </div>
                        <{/if}>
                    </div>
                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {
                $('#a_51').click(function() {
                    $('#fm5_active').val("form_51");
                });
                $('#a_52').click(function() {
                    $('#fm5_active').val("form_52");
                });
                $('#a_53').click(function() {
                    $('#fm5_active').val("form_53");
                });
                $('#a_54').click(function() {
                    $('#fm5_active').val("form_54");
                });

                if ($('ul.nav-tabs').find('li.active').length === 0) {
                    $('ul.nav-tabs').find('a:first').tab('show');
                }
            });
        </script>
    </body>
</html>