<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | <{$titles.record|escape}></title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title=$titles.record hd_sub_title=''}>

            <div class="container-fluid">
                <{*印刷ボタン*}>
                <form id="form_print" method="post" class="pull-right">
                    <button type="button" data-id="<{$smarty.post.id}>" data-emp="<{$smarty.post.target_emp_id}>" data-php="records_view.php" data-target="form2" class="pull-right js-form-print btn btn-primary btn-small"><i class="icon-print"></i> 印刷する</button>
                </form>

                <{*様式共通ボタン*}>
                <form id="form_edit" method="post">
                    <{include file='__form_view.tpl' op_srh='true' tmp_id='form_2view'}>
                    <input type="hidden" id="fm2_active" name="fm2_active" value="" />
                </form>

                <div class="well well-small">

                    <div class="control-group">
                        <div class="input-prepend" style="margin-bottom:0;">
                            <span class="add-on">申請レベル</span>
                            <span class="ldr-uneditable input-mini ldr-level" style="margin-bottom:0;"><{$apply->level_text()|escape}></span>
                        </div>
                        <div class="input-prepend" style="margin-bottom:0;">
                            <span class="add-on">申請者</span>
                            <span class="ldr-uneditable" style="margin-bottom:0;"><{$target_emp->emp_name()|escape}> (<{$target_emp->class_full_name()|escape}>)</span>
                        </div>
                    </div>
                    <hr/>

                    <ul class="nav nav-tabs">
                        <{foreach from=$recordset key=key item=rec}>
                            <{if $rec.php === 'form_22.php'}>
                                <li class="<{if $active === 'form_22_1'}>active<{/if}>">
                                    <a href="#form_22_1" data-toggle="tab" data-active="form_22_1">院内研修</a>
                                </li>
                                <li class="<{if $active === 'form_22_2'}>active<{/if}>">
                                    <a href="#form_22_2" data-toggle="tab" data-active="form_22_2">院外研修</a>
                                </li>
                                <li class="<{if $active === 'form_22_3'}>active<{/if}>">
                                    <a href="#form_22_3" data-toggle="tab" data-active="form_22_3">バリテス</a>
                                </li>
                            <{else}>
                                <li class="<{if $active === $rec.php|replace:'.php':''}>active<{/if}>">
                                    <a href="#<{$rec.php|replace:'.php':''}>" data-toggle="tab" data-active="<{$rec.php|replace:'.php':''}>"><{$titles[$key].btn|escape}></a>
                                </li>
                            <{/if}>
                        <{/foreach}>
                    </ul>

                    <div class="tab-content">
                        <{if $function.form_21}>
                            <div class="tab-pane <{if $active === 'form_21'}>active<{else}>fade<{/if}>" id="form_21">
                                <div class="control-group">
                                    <h5><{$titles.form_21.detailL|escape}></h5>
                                </div>
                                <table class="table table-striped table-hover table-bordered table-condensed ldr-table">
                                    <thead>
                                        <tr>
                                            <th>期間</th>
                                            <th>学歴・研修名等</th>
                                            <th>主催</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <{foreach item=row from=$list21}>
                                            <tr>
                                                <td class="ldr-td-view-from-to"><{$row.start_date|date_format_jp:"%Y年%-m月%-d日(%a)"}> 〜 <{$row.last_date|date_format_jp:"%Y年%-m月%-d日(%a)"}></td>
                                                <td class="ldr-td-text-left"><{$row.title|escape}></td>
                                                <td class="ldr-td-text-left"><{$row.organizer|escape}></td>
                                            </tr>
                                        <{/foreach}>
                                    </tbody>
                                </table>
                            </div>
                        <{/if}>

                        <{if $function.form_22}>
                            <div class="tab-pane <{if $active === 'form_22_1'}>active<{/if}>" id="form_22_1">
                                <div class="control-group">
                                    <h5>院内研修受講一覧</h5>
                                </div>
                                <table class="table table-striped table-hover table-bordered table-condensed ldr-table">
                                    <thead>
                                        <tr>
                                            <th>年月日</th>
                                            <th style="text-align:center;">Lv</th>
                                            <th>研修名</th>
                                            <th>主催</th>
                                            <th>カテゴリ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <{foreach item=row from=$list22_1}>
                                            <tr>
                                                <td class="ldr-td-view-period"><{$row.period|date_format_jp:"%Y年%-m月%-d日(%a)"}></td>
                                                <td class="ldr-td-level"><{if $row.training_id}><{$row.level_roman|default:'なし'|escape}><{/if}></td>
                                                <td class="ldr-td-text-left"><{$row.title|escape}><{if $row.training_id<>0}>*<{/if}></td>
                                                <td class="ldr-td-text-left"><{$row.organizer|escape}></td>
                                                <td class="ldr-td-text-left"><{$row.category|escape}></td>
                                            </tr>
                                        <{/foreach}>
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane <{if $active === 'form_22_2'}>active<{/if}>" id="form_22_2">
                                <div class="control-group">
                                    <h5>院外研修受講一覧</h5>
                                </div>
                                <table class="table table-striped table-hover table-bordered table-condensed ldr-table">
                                    <thead>
                                        <tr>
                                            <th>年月日</th>
                                            <th>研修名</th>
                                            <th>主催</th>
                                            <th>カテゴリ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <{foreach item=row from=$list22_2}>
                                            <tr>
                                                <td class="ldr-td-view-period"><{$row.period|date_format_jp:"%Y年%-m月%-d日(%a)"}></td>
                                                <td class="ldr-td-text-left"><{$row.title|escape}><{if $row.training_id<>0}>*<{/if}></td>
                                                <td class="ldr-td-text-left"><{$row.organizer|escape}></td>
                                                <td class="ldr-td-text-left"><{$row.category|escape}></td>
                                            </tr>
                                        <{/foreach}>
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane <{if $active === 'form_22_3'}>active<{/if}>" id="form_22_3">
                                <div class="control-group">
                                    <h5>バリテス受講一覧</h5>
                                </div>
                                <table class="table table-striped table-hover table-bordered table-condensed ldr-table">
                                    <thead>
                                        <tr>
                                            <th>年月日</th>
                                            <th>研修名</th>
                                            <th>主催</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <{foreach item=row from=$list22_3}>
                                            <tr>
                                                <td class="ldr-td-view-period"><{$row.period|date_format_jp:"%Y年%-m月%-d日(%a)"}></td>
                                                <td class="ldr-td-text-left"><{$row.title|escape}></td>
                                                <td class="ldr-td-text-left"><{$row.organizer|escape}></td>
                                            </tr>
                                        <{/foreach}>
                                    </tbody>
                                </table>
                            </div>
                        <{/if}>

                        <{if $function.form_23}>
                            <div class="tab-pane <{if $active === 'form_23'}>active<{/if}>" id="form_23">
                                <div class="control-group">
                                    <h5><{$titles.form_23.detailL|escape}></h5>
                                </div>
                                <table class="table table-striped table-hover table-bordered table-condensed ldr-table">
                                    <thead>
                                        <tr>
                                            <th>年月日</th>
                                            <th>科目</th>
                                            <th>単元</th>
                                            <th>教科内容</th>
                                            <th>主催</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <{foreach item=row from=$list23}>
                                            <tr>
                                                <td class="ldr-td-view-period"><{$row.period|date_format_jp:"%Y年%-m月%-d日(%a)"}></td>
                                                <td class="ldr-td-text-left"><{$row.subject|escape}></td>
                                                <td class="ldr-td-text-center ldr-level"><{$row.unit_roman|escape}></td>
                                                <td class="ldr-td-text-left"><{$row.contents|escape}></td>
                                                <td class="ldr-td-text-left"><{$row.organizer|escape}></td>
                                            </tr>
                                        <{/foreach}>
                                    </tbody>
                                </table>
                            </div>
                        <{/if}>

                        <{if $function.form_carrier}>
                            <div class="tab-pane <{if $active === 'form_carrier'}>active<{else}>fade<{/if}>" id="form_carrier">
                                <div class="control-group">
                                    <h5><{$titles.form_carrier.detailL|escape}></h5>
                                </div>
                                <{include file="__pager.tpl"}>
                                <table class="table table-striped table-hover table-bordered table-condensed ldr-table">
                                    <thead>
                                        <tr>
                                            <th>期間</th>
                                            <th>施設名</th>
                                            <th>職務内容</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <{foreach item=row from=$listcarrier}>
                                            <tr>
                                                <td class="ldr-td-view-from-to"><{$row.start_date|date_format_jp:"%Y年%-m月%-d日(%a)"}> 〜 <{$row.last_date|date_format_jp:"%Y年%-m月%-d日(%a)"}></td>
                                                <td class="ldr-td-text-left"><{$row.institution|escape}></td>
                                                <td class="ldr-td-text-left"><{$row.detail|escape}></td>
                                            </tr>
                                        <{/foreach}>
                                    </tbody>
                                </table>
                            </div>
                        <{/if}>

                        <{if $function.form_transfer}>
                            <div class="tab-pane <{if $active === 'form_transfer'}>active<{/if}>" id="form_transfer">
                                <div class="control-group">
                                    <h5><{$titles.form_transfer.detailL|escape}></h5>
                                </div>
                                <{include file="__pager.tpl"}>
                                <table class="table table-striped table-hover table-bordered table-condensed ldr-table">
                                    <thead>
                                        <tr>
                                            <th>年月日</th>
                                            <th>異動先</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <{foreach item=row from=$listtransfer}>
                                            <tr>
                                                <td class="ldr-td-view-period"><{$row.period|date_format_jp:"%Y年%-m月%-d日(%a)"}></td>
                                                <td class="ldr-td-text-left"><{$row.transfer|escape}></td>
                                            </tr>
                                        <{/foreach}>
                                    </tbody>
                                </table>
                            </div>
                        <{/if}>

                        <{if $function.form_3}>
                            <div class="tab-pane <{if $active === 'form_3'}>active<{/if}>" id="form_3">
                                <div class="control-group">
                                    <h5><{$titles.form_3.detailL|escape}></h5>
                                </div>
                                <{include file="__pager.tpl"}>
                                <table class="table table-striped table-hover table-bordered table-condensed ldr-table">
                                    <thead>
                                        <tr>
                                            <th>年月日</th>
                                            <th>種別</th>
                                            <th>研究発表会（学会）名</th>
                                            <th>主催</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <{foreach item=row from=$list3}>
                                            <tr>
                                                <td class="ldr-td-view-period"><{$row.period|date_format_jp:"%Y年%-m月%-d日(%a)"}></td>
                                                <td class="ldr-td-text-left"><{$f3_types[$row.type]|escape}></td>
                                                <td class="ldr-td-text-left"><{$row.title|escape}></td>
                                                <td class="ldr-td-text-left"><{$row.organizer|escape}></td>
                                            </tr>
                                        <{/foreach}>
                                    </tbody>
                                </table>
                            </div>
                        <{/if}>

                        <{if $function.form_4}>
                            <div class="tab-pane <{if $active === 'form_4'}>active<{/if}>" id="form_4">
                                <div class="control-group">
                                    <h5><{$titles.form_4.detailL|escape}></h5>
                                </div>
                                <{include file="__pager.tpl"}>
                                <table class="table table-striped table-hover table-bordered table-condensed ldr-table">
                                    <thead>
                                        <tr>
                                            <th>年月日</th>
                                            <th>業務改善・発表・投稿記録</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <{foreach item=row from=$list4}>
                                            <tr>
                                                <td class="ldr-td-view-period"><{$row.period|date_format_jp:"%Y年%-m月%-d日(%a)"}></td>
                                                <td class="ldr-td-text-left"><{$row.record|escape|nl2br}></td>
                                            </tr>
                                        <{/foreach}>
                                    </tbody>
                                </table>
                            </div>
                        <{/if}>

                        <{if $function.form_51}>
                            <div class="tab-pane <{if $active === 'form_51'}>active<{/if}>" id="form_51">
                                <div class="control-group">
                                    <h5><{$titles.form_51.detailL|escape}></h5>
                                </div>
                                <table class="table table-striped table-hover table-bordered table-condensed ldr-table">
                                    <thead>
                                        <tr>
                                            <th>期間</th>
                                            <th nowrap>所属部署</th>
                                            <th>役割</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <{foreach item=row from=$list51}>
                                            <tr>
                                                <td class="ldr-td-view-from-to"><{$row.start_date|date_format_jp:"%Y年%-m月%-d日(%a)"}> 〜 <{$row.last_date|date_format_jp:"%Y年%-m月%-d日(%a)"}></td>
                                                <td class="ldr-td-text-left"><{$row.class|escape}></td>
                                                <td class="ldr-td-text-left"><{$row.role|escape|nl2br}></td>
                                            </tr>
                                        <{/foreach}>
                                    </tbody>
                                </table>
                            </div>
                        <{/if}>

                        <{if $function.form_52}>
                            <div class="tab-pane <{if $active === 'form_52'}>active<{/if}>" id="form_52">
                                <div class="control-group">
                                    <h5><{$titles.form_52.detailL|escape}></h5>
                                </div>
                                <table class="table table-striped table-hover table-bordered table-condensed ldr-table">
                                    <thead>
                                        <tr>
                                            <th>期間</th>
                                            <th nowrap>種別</th>
                                            <th>対象</th>
                                            <th>テーマ／内容</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <{foreach item=row from=$list52}>
                                            <tr>
                                                <td class="ldr-td-view-from-to"><{$row.start_date|date_format_jp:"%Y年%-m月%-d日(%a)"}> 〜 <{$row.last_date|date_format_jp:"%Y年%-m月%-d日(%a)"}></td>
                                                <td class="ldr-td-text-center"><{$f52_types[$row.type]|escape}></td>
                                                <td class="ldr-td-text-left"><{$row.subject|escape}></td>
                                                <td class="ldr-td-text-left"><{$row.theme|escape}></td>
                                            </tr>
                                        <{/foreach}>
                                    </tbody>
                                </table>
                            </div>
                        <{/if}>

                        <{if $function.form_53}>
                            <div class="tab-pane <{if $active === 'form_53'}>active<{/if}>" id="form_53">
                                <div class="control-group">
                                    <h5><{$titles.form_53.detailL|escape}></h5>
                                </div>
                                <table class="table table-striped table-hover table-bordered table-condensed ldr-table">
                                    <thead>
                                        <tr>
                                            <th>期間</th>
                                            <th>種別</th>
                                            <th>名称</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <{foreach item=row from=$list53}>
                                            <tr>
                                                <td class="ldr-td-view-from-to"><{$row.start_date|date_format_jp:"%Y年%-m月%-d日(%a)"}> 〜 <{$row.last_date|date_format_jp:"%Y年%-m月%-d日(%a)"}></td>
                                                <td class="ldr-td-text-center"><{$f53_types[$row.type]|escape}></td>
                                                <td class="ldr-td-text-left"><{$row.title|escape}></td>
                                            </tr>
                                        <{/foreach}>
                                    </tbody>
                                </table>
                            </div>
                        <{/if}>

                        <{if $function.form_54}>
                            <div class="tab-pane <{if $active === 'form_54'}>active<{/if}>" id="form_54">
                                <div class="control-group">
                                    <h5><{$titles.form_54.detailL|escape}></h5>
                                </div>
                                <table class="table table-striped table-hover table-bordered table-condensed ldr-table">
                                    <thead>
                                        <tr>
                                            <th>期間</th>
                                            <th>名称</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <{foreach item=row from=$list54}>
                                            <tr>
                                                <td class="ldr-td-view-from-to"><{$row.start_date|date_format_jp:"%Y年%-m月%-d日(%a)"}> 〜 <{$row.last_date|date_format_jp:"%Y年%-m月%-d日(%a)"}></td>
                                                <td class="ldr-td-text-left"><{$row.title|escape}></td>
                                            </tr>
                                        <{/foreach}>
                                    </tbody>
                                </table>
                            </div>
                        <{/if}>

                        <{if $function.form_presentation}>
                            <div class="tab-pane <{if $active === 'form_presentation'}>active<{/if}>" id="form_presentation">
                                <div class="control-group">
                                    <h5><{$titles.form_presentation.detailL|escape}></h5>
                                </div>
                                <{include file="__pager.tpl"}>
                                <table class="table table-striped table-hover table-bordered table-condensed ldr-table">
                                    <thead>
                                        <tr>
                                            <th>演題</th>
                                            <th>学会名</th>
                                            <th>発表年月日</th>
                                            <th>種別</th>
                                            <th>単独<br/>共同</th>
                                            <th>抄録<br/>ページ</th>
                                            <th>概要</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <{foreach item=row from=$listpresent}>
                                            <tr>
                                                <td class="ldr-td-text-left"><{$row.subject|escape}></td>
                                                <td class="ldr-td-text-left"><{$row.title|escape}></td>
                                                <td class="ldr-td-view-period"><{$row.period|date_format_jp:"%Y年%-m月%-d日(%a)"}></td>
                                                <td class="ldr-td-text-left"><{$fp_types[$row.type]|escape}></td>
                                                <td class="ldr-td-text-left"><{$fp_units[$row.unit]|escape}></td>
                                                <td class="ldr-td-text-left"><{$row.page|escape}></td>
                                                <td class="ldr-td-text-left"><{$row.note|escape|nl2br}></td>
                                            </tr>
                                        <{/foreach}>
                                    </tbody>
                                </table>
                            </div>
                        <{/if}>

                        <{if $function.form_article}>
                            <div class="tab-pane <{if $active === 'form_article'}>active<{/if}>" id="form_article">
                                <div class="control-group">
                                    <h5><{$titles.form_article.detailL|escape}></h5>
                                </div>
                                <{include file="__pager.tpl"}>
                                <table class="table table-striped table-hover table-bordered table-condensed ldr-table">
                                    <thead>
                                        <tr>
                                            <th>演題</th>
                                            <th>掲載誌</th>
                                            <th>掲載年月日</th>
                                            <th>単独<br/>共同</th>
                                            <th>掲載<br/>ページ</th>
                                            <th>概要</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <{foreach item=row from=$listarticle}>
                                            <tr>
                                                <td class="ldr-td-text-left"><{$row.subject|escape}></td>
                                                <td class="ldr-td-text-left"><{$row.title|escape}></td>
                                                <td class="ldr-td-view-period"><{$row.period|date_format_jp:"%Y年%-m月%-d日(%a)"}></td>
                                                <td class="ldr-td-text-left"><{$fa_units[$row.unit]|escape}></td>
                                                <td class="ldr-td-text-left"><{$row.page|escape}></td>
                                                <td class="ldr-td-text-left"><{$row.note|escape|nl2br}></td>
                                            </tr>
                                        <{/foreach}>
                                    </tbody>
                                </table>
                            </div>
                        <{/if}>

                    </div>
                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {
                $('ul.nav-tabs').find('a').click(function() {
                    $('#fm2_active').val($(this).data('active'));
                });
                if ($('ul.nav-tabs').find('li.active').length === 0) {
                    $('ul.nav-tabs').find('a:first').tab('show');
                }
            });
        </script>
    </body>
</html>
