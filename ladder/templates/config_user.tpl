<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | ユーザ設定</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title="ユーザ設定" hd_sub_title=""}>

            <div class="container-fluid">
                <{include file="__save_message.tpl"}>
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#config" data-toggle="tab"><i class="icon-cog"></i> 設定</a></li>
                    <li><a href="#level" data-toggle="tab"><i class="icon-level-up"></i> レベル</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active in" id="config">
                        <div class="well well-small">
                            <form id="form_edit" method="post">
                                <input type="hidden" name="emp_id" value="" />
                                <input type="hidden" name="ldr_updated_on" value="" />
                                <{if $staff->updated_on()}>
                                    <div class="control-group">
                                        <label class="control-label"><strong>更新日時</strong></label>
                                        <div><{$staff->updated_on()|date_format_jp:"%Y年%-m月%-d日(%a) %R"}></div>
                                    </div>
                                <{/if}>
                                <div class="control-group">
                                    <label class="control-label">
                                        <strong>卒業年</strong>
                                        <{if $required.graduation_year}><span class="badge badge-important required">必須</span><{/if}>
                                    </label>
                                    <div class="controls">
                                        <{include file="__datepicker.tpl" date_type="year" date_id="graduation_year" date_name="graduation_year" date_value=""}>
                                    </div>
                                    <div class="controls">
                                        <{foreach from=$error.graduation_year item=msg}>
                                            <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                        <{/foreach}>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">
                                        <strong>出身校</strong>
                                        <{if $required.school}><span class="badge badge-important required">必須</span><{/if}>
                                    </label>
                                    <div class="controls">
                                        <div class="input-prepend">
                                            <span class="add-on"><i class="icon-building"></i></span>
                                            <input type="text" name="school" class="input-xlarge" minlength="2" maxlength="200" value="" placeholder="出身校を入力してください"  <{if $required.school}>required="required"<{/if}> />
                                        </div>
                                    </div>
                                    <div class="controls">
                                        <{foreach from=$error.school item=msg}>
                                            <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                        <{/foreach}>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">
                                        <strong>看護師免許取得年月</strong>
                                        <{if $required.license_date}><span class="badge badge-important required">必須</span><{/if}>
                                    </label>
                                    <div class="controls">
                                        <{include file="__datepicker.tpl" date_type="month" date_id="license_date" date_name="license_date" date_value=""}>
                                        <{foreach from=$error.license_date item=msg}>
                                            <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                        <{/foreach}>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label">
                                        <strong>臨床開始年月</strong>
                                        <{if $required.nurse_date}><span class="badge badge-important required">必須</span><{/if}>
                                    </label>
                                    <div class="controls">
                                        <{include file="__datepicker.tpl" date_type="month" date_id="nurse_date" date_name="nurse_date" date_value=""}>
                                        <{foreach from=$error.nurse_date item=msg}>
                                            <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                        <{/foreach}>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label">
                                        <strong>当院 配属年月</strong>
                                        <{if $required.hospital_date}><span class="badge badge-important required">必須</span><{/if}>
                                    </label>
                                    <div class="controls">
                                        <{include file="__datepicker.tpl" date_type="month" date_id="hospital_date" date_name="hospital_date" date_value=""}>
                                        <{foreach from=$error.hospital_date item=msg}>
                                            <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                        <{/foreach}>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label">
                                        <strong>部署 配属年月</strong>
                                        <{if $required.ward_date}><span class="badge badge-important required">必須</span><{/if}>
                                    </label>
                                    <div class="controls">
                                        <{include file="__datepicker.tpl" date_type="month" date_id="ward_date" date_name="ward_date" date_value=""}>
                                        <{foreach from=$error.ward_date item=msg}>
                                            <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                        <{/foreach}>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="jna_id">
                                        <strong>JNA会員番号 <small>(数字8桁)</small></strong>
                                        <{if $required.jna_id}><span class="badge badge-important required">必須</span><{/if}>
                                    </label>
                                    <div class="controls">
                                        <div class="input-prepend">
                                            <span class="add-on"><i class="icon-credit-card"></i></span>
                                            <input type="text" name="jna_id" class="input-xlarge onlynum" minlength="2" maxlength="8" value="" placeholder="会員番号を入力してください" <{if $required.jna_id}>required="required"<{/if}> />
                                        </div>
                                    </div>
                                    <div class="controls">
                                        <{foreach from=$error.jna_id item=msg}>
                                            <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                        <{/foreach}>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="pref_na_id">
                                        <strong>都道府県看護協会 会員番号 <small>(数字6桁)</small></strong>
                                        <{if $required.pref_na_id}><span class="badge badge-important required">必須</span><{/if}>
                                    </label>
                                    <div class="controls">
                                        <div class="input-prepend">
                                            <span class="add-on"><i class="icon-credit-card"></i></span>
                                            <input type="text" name="pref_na_id" class="input-xlarge onlynum" minlength="2" maxlength="6" value="" placeholder="会員番号を入力してください" <{if $required.pref_na_id}>required="required"<{/if}> />
                                        </div>
                                    </div>
                                    <div class="controls">
                                        <{foreach from=$error.pref_na_id item=msg}>
                                            <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                        <{/foreach}>
                                    </div>
                                </div>
                                <div class="btn-toolbar">
                                    <button type="submit" name="btn_save" id="btn_save" class="btn btn-small btn-primary" value="btn_save"><i class="icon-save"></i> 保存する</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="tab-pane" id="level">
                        <div class="well well-small">
                            <{foreach from=$ladder->lists() item=ldr}>
                                <div class="control-group">
                                    <label class="control-label"><strong><{$ldr.title|escape}></strong></label>

                                    <table class="table table-striped table-condensed">
                                        <thead>
                                            <tr>
                                                <th>レベル</th>
                                                <th>認定番号</th>
                                                <th>認定日</th>
                                                <th>取得医療施設</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <{foreach from=$ldr.config.level item=level}>
                                                <tr>
                                                    <td class="ldr-level"><{if $level}><{$roman_num[$level]}><{else}><{$ldr.config.level0}><{/if}></td>
                                                    <td><{$staff->lv_number($ldr.ladder_id, $level)|escape}></td>
                                                    <td><{$staff->lv_date($ldr.ladder_id, $level)|escape}></td>
                                                    <td><{$staff->lv_hospital($ldr.ladder_id, $level)|escape}></td>
                                                </tr>
                                            <{/foreach}>
                                        </tbody>
                                    </table>
                                </div>
                            <{/foreach}>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
    </body>
</html>
