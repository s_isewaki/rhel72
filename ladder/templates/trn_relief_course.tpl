<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 救護員研修 講座</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='救護員研修 講座' hd_sub_title=''}>

            <div class="container-fluid">
                <form>
                    <{if $list}>
                        <table class="table table-bordered table-hover table-condensed ldr-table">
                            <thead>
                                <tr>
                                    <th>科目</th>
                                    <th>単元</th>
                                    <th>教科内容</th>
                                    <th>詳細</th>
                                    <th>時間</th>
                                    <th>申込</th>
                                </tr>
                            </thead>
                            <tbody>
                                <{foreach item=row from=$list name=rf}>
                                    <tr>
                                        <{if $row.relief_id !== $prev.relief_id}>
                                            <td class="ldr-td-text-left" rowspan="<{$row.cnt}>" style="background-color: #fff"><{$row.subject|escape}></td>
                                        <{/if}>
                                        <td class="ldr-td-level"><{$roman_num[$row.unit]|escape}></td>
                                        <td class="ldr-td-text-left"><{$row.contents|escape}></td>
                                        <td class="ldr-td-text-left"><{$row.detail|escape|nl2br}></td>
                                        <td class="ldr-td-text-left"><{$row.time|escape}></td>
                                        <td class="ldr-td-name">
                                            <button type="button" data-php="trn_relief_apply.php" data-mode="apply" data-id="<{$row.relief_id}>_<{$row.unit}>" class="js-form-button btn btn-small btn-danger">申込</button>
                                        </td>
                                    </tr>
                                    <{assign var="prev" value=$row}>
                                <{/foreach}>
                            </tbody>
                        </table>

                    <{else}>
                        <div class="alert alert-error">救護員研修が設定されていません。管理者にお問い合わせください</div>
                    <{/if}>
                </form>
            </div>
        </div>
        <{include file="__js.tpl"}>
    </body>
</html>
