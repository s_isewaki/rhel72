<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | <{$ldr.config.form_7.title|escape}>入力</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title="`$ldr.config.form_7.title`入力" hd_sub_title=$ldr.config.form_7.detailL}>

            <div class="container-fluid">
                <{include file="__save_message.tpl" msg_text=$completed}>
                <div class="row-fluid">
                    <button type="button" data-id="<{$smarty.post.id}>" data-emp="<{$smarty.post.target_emp_id}>" data-php="form_7view.php" data-target="form7" data-menu="accept" class="js-form-print btn btn-primary btn-small pull-right"><i class="icon-print"></i> 印刷する</button>
                    <form method="post">
                        <{include file="__form_view.tpl" op_srh='false' tmp_id="form_7edit_appraiser"}>
                    </form>
                    <div class="well well-small">
                        <{if !$is_completed}>
                            <form class="form" method="POST">
                                <{*画面遷移用*}>
                                <input type="hidden" name="mode" value="" />
                                <input type="hidden" name="menu" value="" />
                                <input type="hidden" name="id" value="" />
                                <input type="hidden" name="target_emp_id" value=""/>
                                <input type="hidden" name="owner_url" value="" />
                                <button type="submit" name="btn_save" value="complete" class="js-submit-decision btn btn-small btn-primary pull-right">評価を完了する</button>
                            </form>
                        <{else}>
                            <button type="button" disabled="disabled" class="btn btn-small btn-primary pull-right">評価完了済</button>
                        <{/if}>

                        <div class="control-group">
                            <div class="input-prepend" style="margin-bottom:0;">
                                <span class="add-on">申請レベル</span>
                                <span class="ldr-uneditable input-mini ldr-level" style="margin-bottom:0;"><{$apply->level_text()|escape}></span>
                            </div>
                            <div class="input-prepend" style="margin-bottom:0;">
                                <span class="add-on">申請者</span>
                                <span class="ldr-uneditable" style="margin-bottom:0;"><{$target_emp->emp_name()|escape}> (<{$target_emp->class_full_name()|escape}>)</span>
                            </div>
                        </div>
                        <hr/>

                        <{if $ldr.config.word[$level]}>
                            <div class="well well-small">
                                <b><{$ldr.config.word[$level]|escape|nl2br}></b>
                            </div>
                        <{/if}>

                        <{if $error.form7}>
                            <div class="alert alert-danger">
                                <{foreach from=$error.form7 item=msg}>
                                    <i class="icon-warning-sign"></i> <{$msg|escape}><br/>
                                <{/foreach}>
                            </div>
                        <{/if}>

                        <ul id="tabs" class="nav nav-tabs">
                            <li class="<{if $active === 'sec1'}>active<{/if}>">
                                <a href="#sec1" data-sec="sec1" data-toggle="tab"><{if $ldr.config.rc === '1'}>臨床看護実践<{else}>看護管理実践<{/if}></a>
                            </li>

                            <{foreach item=row from=$group1_list}>
                                <li class="<{if $active === $row.guideline_group1}>active<{/if}>">
                                    <a href="#g1-<{$row.guideline_group1|escape}>" data-toggle="tab" class="js-tab-form72"><{$row.name|escape}></a>
                                </li>
                            <{/foreach}>

                            <li class="<{if $active === 'sec3'}>active<{/if}>">
                                <a href="#sec3" data-sec="sec3" data-toggle="tab">総合レベル</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane <{if $active === 'sec1'}>active<{else}>fade<{/if}>" id="sec1">
                                <form id="form71" class="form" method="POST">
                                    <input type="hidden" name="save" value="form71" />
                                    <{*画面遷移用*}>
                                    <input type="hidden" name="mode" value="" />
                                    <input type="hidden" name="menu" value="" />
                                    <input type="hidden" name="id" value="" />
                                    <input type="hidden" name="target_emp_id" value=""/>
                                    <input type="hidden" name="owner_url" value="" />
                                    <{*固有画面*}>
                                    <input type="hidden" name="active" value="sec1" />
                                    <div class="control-group">
                                        <label class="control-label" for="knowledge">
                                            <strong><{if $ldr.config.rc === '1'}>知識<{else}>赤十字<{/if}></strong>
                                            <span class="badge badge-important required">必須</span>
                                        </label>
                                        <textarea id="knowledge" name="knowledge" rows="2" class="input-maxlarge js-autoheight" placeholder="評価内容を記入してください"></textarea>
                                        <{foreach from=$error.knowledge item=msg}>
                                            <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                        <{/foreach}>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="decision">
                                            <strong><{if $ldr.config.rc === '1'}>判断<{else}>管理過程<{/if}></strong>
                                            <span class="badge badge-important required">必須</span>
                                        </label>
                                        <textarea id="decision" name="decision" rows="2" class="input-maxlarge js-autoheight" placeholder="評価内容を記入してください"></textarea>
                                        <{foreach from=$error.decision item=msg}>
                                            <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                        <{/foreach}>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="action">
                                            <strong><{if $ldr.config.rc === '1'}>行為<{else}>意思決定<{/if}></strong>
                                            <span class="badge badge-important required">必須</span>
                                        </label>
                                        <textarea id="action" name="action" rows="2" class="input-maxlarge js-autoheight" placeholder="評価内容を記入してください"></textarea>
                                        <{foreach from=$error.action item=msg}>
                                            <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                        <{/foreach}>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="result">
                                            <strong><{if $ldr.config.rc === '1'}>行為の結果<{else}>質保証<{/if}></strong>
                                            <span class="badge badge-important required">必須</span>
                                        </label>
                                        <textarea id="result" name="result" rows="2" class="input-maxlarge js-autoheight" placeholder="評価内容を記入してください"></textarea>
                                        <{foreach from=$error.result item=msg}>
                                            <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                        <{/foreach}>
                                    </div>

                                    <{if $ldr.config.rc === '2'}>
                                        <div class="control-group">
                                            <label class="control-label" for="comment5">
                                                <strong>人材育成</strong>
                                                <span class="badge badge-important required">必須</span>
                                            </label>
                                            <textarea id="comment5" name="comment5" rows="2" class="input-maxlarge js-autoheight" placeholder="評価内容を記入してください"></textarea>
                                            <{foreach from=$error.comment5 item=msg}>
                                                <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                            <{/foreach}>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label" for="comment6">
                                                <strong>対人関係</strong>
                                                <span class="badge badge-important required">必須</span>
                                            </label>
                                            <textarea id="comment6" name="comment6" rows="2" class="input-maxlarge js-autoheight" placeholder="評価内容を記入してください"></textarea>
                                            <{foreach from=$error.comment6 item=msg}>
                                                <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                            <{/foreach}>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label" for="comment7">
                                                <strong>セルフマネジメント</strong>
                                                <span class="badge badge-important required">必須</span>
                                            </label>
                                            <textarea id="comment7" name="comment7" rows="2" class="input-maxlarge js-autoheight" placeholder="評価内容を記入してください"></textarea>
                                            <{foreach from=$error.comment7 item=msg}>
                                                <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                            <{/foreach}>
                                        </div>
                                    <{else}>
                                        <input type="hidden" name="comment5" value="" />
                                        <input type="hidden" name="comment6" value="" />
                                        <input type="hidden" name="comment7" value="" />
                                    <{/if}>

                                    <div class="btn-toolbar">
                                        <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                                        <button type="submit" name="btn_save" id="btn_save" class="btn btn-small btn-primary" value="71_save"><i class="icon-save"></i> 保存する</button>
                                    </div>
                                    <p><small><i class="icon-exclamation-sign"></i> 入力内容は自動保存されます。明示的に保存したい場合は<b>[保存する]ボタン</b>で保存してください。</small></p>
                                </form>
                            </div>

                            <{foreach item=g1 from=$group1_list}>
                                <div class="tab-pane <{if $active === $g1.guideline_group1}>active<{else}>fade<{/if}>" id="g1-<{$g1.guideline_group1|escape}>">
                                    <{if $g1.comment}>
                                        <div><i class="icon-folder-open"></i> <b><{$g1.comment|escape}></b></div>
                                        <hr/>
                                    <{/if}>

                                    <form class="form72 js-form-assessment" action="form_7edit_appraiser.php" method="POST">
                                        <input type="hidden" name="save" value="form72" />
                                        <{*画面遷移用*}>
                                        <input type="hidden" name="mode" value="" />
                                        <input type="hidden" name="menu" value="" />
                                        <input type="hidden" name="id" value="" />
                                        <input type="hidden" name="target_emp_id" value=""/>
                                        <input type="hidden" name="owner_url" value="" />
                                        <{*固有画面*}>
                                        <input type="hidden" name="active" value="<{$g1.guideline_group1|escape}>" />

                                        <{foreach item=asm from=$assessment_list[$g1.guideline_group1]}>
                                            <{assign var='asmkey' value="_`$asm.apply_assessment`"}>
                                            <{assign var='acckey' value="_`$asm.accept_assessment`"}>
                                            <{if $asm.guideline_group2 !== $back_g2}>
                                                <label><strong><{$asm.group2_name|escape}></strong></label>
                                                <{if $asm.group2_comment}> <div style="margin:0 0 1em 1em; font-size:13px;"><{$asm.group2_comment|escape}></div><{/if}>
                                                <{assign var="back_g2" value=`$asm.guideline_group2`}>
                                            <{/if}>

                                            <div class="ldr-indent">
                                                <div class="alert alert-<{$ldr.config.select[$acckey].color|default:'default'}>" id="al-<{$asm.guideline_id|escape}>"><{$asm.guideline|escape}></div>
                                                <{if $asm.criterion}>
                                                    <label class="control-label"><strong>判断基準</strong></label>
                                                    <p class="alert alert-default"><{$asm.criterion|escape}></p>
                                                <{/if}>

                                                <div id="as-<{$asm.guideline_id|escape}>" class="inline">
                                                    <label>本人の評価</label>
                                                    <div class="pull-left">
                                                        <{if $ldr.config.select|@count > 2}>
                                                            <div class="btn-group" style="margin-top:5px;">
                                                                <{foreach item=sel from=$ldr.config.select}>
                                                                    <button type="button"
                                                                            class="btn btn-medium <{if $asm.apply_assessment === $sel.value}>btn-<{$sel.color}> active<{/if}>"
                                                                            disabled="disabled"
                                                                            >
                                                                        <{$sel.button|escape}>
                                                                    </button>
                                                                <{/foreach}>
                                                            </div>
                                                        <{else}>
                                                            <button type="button" class="btn btn-large btn-<{$ldr.config.select[$asmkey].color|default:'default'}>" disabled="disabled">
                                                                <{$ldr.config.select[$asmkey].button|default:'<i class="icon-question"></i>'}>
                                                            </button>
                                                        <{/if}>
                                                    </div>
                                                    <div class="clearfix" style="padding: 1px 0 0 <{$padleft|escape}>;">
                                                        <{if $ldr.config.reason === '1'}>
                                                            <div
                                                                class="alert alert-<{$ldr.config.select[$asmkey].color|default:'default'}>">
                                                                <{$asm.apply_reason|default:'本人根拠入力無し'|escape|nl2br}>
                                                            </div>
                                                        <{/if}>
                                                    </div>
                                                </div>

                                                <div id="as-<{$asm.guideline_id|escape}>" class="inline">
                                                    <label>あなたの評価</label>

                                                    <div class="pull-left">
                                                        <input
                                                            type="hidden"
                                                            class="input-assessment"
                                                            id="in-<{$asm.guideline_id|escape}>"
                                                            name="assessment[<{$asm.guideline_id|escape}>]"
                                                            value="<{$asm.accept_assessment|escape}>"
                                                            data-pop="#bt-<{$asm.guideline_id|escape}>"
                                                            data-textarea="#re-<{$asm.guideline_id|escape}>"
                                                            data-al="#al-<{$asm.guideline_id|escape}>"
                                                            data-apply="<{$asm.apply_assessment|escape}>"
                                                            />
                                                        <{if $ldr.config.select|@count > 2}>
                                                            <div class="btn-group" data-toggle="buttons-radio" style="margin-top:5px;">
                                                                <{foreach item=sel from=$ldr.config.select}>
                                                                    <button type="button"
                                                                            class="btn btn-medium js-radio-assessment <{if $asm.accept_assessment === $sel.value}>btn-<{$sel.color}> active<{/if}>"
                                                                            name="assessment[<{$asm.guideline_id|escape}>]"
                                                                            id="bt-<{$asm.guideline_id|escape}>-<{$sel.value|escape}>"
                                                                            data-input="#in-<{$asm.guideline_id|escape}>"
                                                                            data-alert="#al-<{$asm.guideline_id|escape}>"
                                                                            data-textarea="#re-<{$asm.guideline_id|escape}>"
                                                                            data-trigger="manual"
                                                                            data-placement="bottom"
                                                                            data-container="#as-<{$asm.guideline_id|escape}>"
                                                                            data-title="評価してください"
                                                                            data-color="<{$sel.color|escape}>"
                                                                            value="<{$sel.value|escape}>"
                                                                            >
                                                                        <{$sel.button|escape}>
                                                                    </button>
                                                                <{/foreach}>
                                                            </div>
                                                        <{else}>
                                                            <button
                                                                type="button"
                                                                id="bt-<{$asm.guideline_id|escape}>"
                                                                class="btn btn-large js-button-assessment btn-<{$ldr.config.select[$acckey].color|default:'default'}>"
                                                                data-input="#in-<{$asm.guideline_id|escape}>"
                                                                data-alert="#al-<{$asm.guideline_id|escape}>"
                                                                data-textarea="#re-<{$asm.guideline_id|escape}>"
                                                                data-trigger="manual"
                                                                data-placement="bottom"
                                                                data-container="#as-<{$asm.guideline_id|escape}>"
                                                                data-title="評価してください"
                                                                rel="tooltip">
                                                                <{$ldr.config.select[$acckey].button|default:'<i class="icon-question"></i>'}>
                                                            </button>
                                                        <{/if}>
                                                    </div>
                                                    <div class="clearfix" style="padding: 1px 0 0 <{$padleft|escape}>;">
                                                        <{if $ldr.config.reason === '1'}>
                                                            <textarea
                                                                id="re-<{$asm.guideline_id|escape}>"
                                                                class="input-maxlarge js-autoheight js-reason-assessment"
                                                                name="reason[<{$asm.guideline_id|escape}>]"
                                                                rows="2"
                                                                placeholder="根拠を入力してください"
                                                                <{if $ldr.config.diff_reason === '1'}>
                                                                    data-trigger="manual"
                                                                    data-placement="bottom"
                                                                    data-container="#as-<{$asm.guideline_id|escape}>"
                                                                    data-title="本人評価と異なる場合は根拠を必ず入力してください"
                                                                    rel="tooltip"
                                                                <{/if}>
                                                                ><{$asm.accept_reason|escape}></textarea>
                                                        <{/if}>
                                                    </div>
                                                </div>
                                                <hr style="margin:8px 0;"/>
                                            </div>
                                        <{/foreach}>
                                        <div class="btn-toolbar">
                                            <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                                            <button type="submit" name="btn_save" id="btn_save" class="btn btn-small btn-primary" value="72_save"><i class="icon-save"></i> 保存する</button>
                                        </div>
                                        <p><small><i class="icon-exclamation-sign"></i> 入力内容は自動保存されます。明示的に保存したい場合は<b>[保存する]ボタン</b>で保存してください。</small></p>
                                    </form>
                                </div>
                            <{/foreach}>

                            <div class="tab-pane <{if $active === 'sec3'}>active<{else}>fade<{/if}>" id="sec3">
                                <form id="form73" class="form" method="POST">
                                    <input type="hidden" name="save" value="form73" />
                                    <{*画面遷移用*}>
                                    <input type="hidden" name="mode" value="" />
                                    <input type="hidden" name="menu" value="" />
                                    <input type="hidden" name="id" value="" />
                                    <input type="hidden" name="target_emp_id" value=""/>
                                    <input type="hidden" name="owner_url" value="" />
                                    <{*固有画面*}>
                                    <input type="hidden" name="active" value="sec3" />
                                    <div class="control-group">
                                        <label class="control-label" for="management">
                                            <strong>総合レベル</strong>
                                            <span class="badge badge-important required">必須</span>
                                        </label>
                                        <label class="control-label">　<small>いずれか当てはまる項目にチェックを入れましょう。</small></label>
                                        <div class="ldr-xxlarge">
                                            <label class="radio"><input type="radio" name="level" class="level" value="1" />
                                                <strong>あなたの看護実践に対し申請レベルを認めます。</strong>
                                            </label>
                                            <label class="radio"><input type="radio" name="level" class="level" value="2" />
                                                <strong>今回の申請レベルは保留にします。再チャレンジしてください。</strong>
                                            </label>
                                        </div>
                                        <{foreach from=$error.level item=msg}>
                                            <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                        <{/foreach}>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="advice">
                                            <strong>評価者から申請者へのアドバイス</strong>
                                            <span class="badge badge-important required">必須</span>
                                        </label>
                                        <label class="control-label">　<small>さらにキャリアを高めるための努力目標</small></label>
                                        <div class="controls">
                                            <textarea id="advice" name="advice" rows="3" class="input-maxlarge js-autoheight" placeholder='アドバイスを記入してください'></textarea>
                                        </div>
                                        <{foreach from=$error.advice item=msg}>
                                            <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                        <{/foreach}>
                                    </div>
                                    <div class="btn-toolbar">
                                        <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                                        <button type="submit" name="btn_save" id="btn_save" class="btn btn-small btn-primary" value="73_save"><i class="icon-save"></i> 保存する</button>
                                    </div>
                                    <p><small><i class="icon-exclamation-sign"></i> 入力内容は自動保存されます。明示的に保存したい場合は<b>[保存する]ボタン</b>で保存してください。</small></p>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <{if $ldr.config.reason === '1' && $ldr.config.diff_reason === '1'}>
            <div class="hide" id="diff_reason" data-value="1"></div>
        <{/if}>
        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {

                // 根拠チェック
                $('body').on('shown', '.js-tab-form72', function(e) {
                    if ($('#diff_reason').data('value') === 1) {
                        $('input.input-assessment').each(function(idx, input) {
                            var inp = $(input);
                            if (inp.val() && parseInt(inp.val()) !== parseInt(inp.data('apply')) && $(inp.data('textarea')).val() === '') {
                                $(inp.data('textarea')).tooltip('show');
                            }
                        });
                    }
                });

                $('body').on('click', 'button.js-button-assessment', function() {
                    $(this).tooltip('destroy');
                    $($(this).data('textarea')).tooltip('destroy');

                    var input = $($(this).data('input'));
                    var alert = $($(this).data('alert'));

            <{if $ldr.config.select|@count <= 2}>
                    var value = '';
                <{foreach item=sel from=$ldr.config.select|@array_reverse}>
                    if (input.val() + '' !== '<{$sel.value|escape:'javascript'}>') {
                        $(this)
                            .html('<{$sel.button|escape:'javascript'}>')
                            .attr('class', 'js-button-assessment btn btn-large btn-<{$sel.color|escape:'javascript'}>');
                        alert.attr('class', 'alert alert-<{$sel.color|escape:'javascript'}>');
                        value = '<{$sel.value|escape:'javascript'}>';
                    }
                <{/foreach}>
                    input.val(value).change();
            <{/if}>
                    if (parseInt(input.val()) !== parseInt(input.data('apply')) && $(input.data('textarea')).val() === '' && $('#diff_reason').data('value') === 1) {
                        $(input.data('textarea')).tooltip('show');
                    }
                });

                $('body').on('click', 'button.js-radio-assessment', function() {
                    $(this).tooltip('destroy');
                    $($(this).data('textarea')).tooltip('destroy');

                    var input = $($(this).data('input'));
                    var alert = $($(this).data('alert'));
                    input.val($(this).attr('value')).change();

                    $(this).siblings().removeClass('btn-primary');
                    $(this).siblings().removeClass('btn-info');
                    $(this).siblings().removeClass('btn-success');
                    $(this).siblings().removeClass('btn-warning');
                    $(this).siblings().removeClass('btn-danger');
                    $(this).addClass('btn-' + $(this).data('color'));
                    alert.attr('class', 'alert alert-' + $(this).data('color'));

                    if (parseInt(input.val()) !== parseInt(input.data('apply')) && $(input.data('textarea')).val() === '' && $('#diff_reason').data('value') === 1) {
                        $(input.data('textarea')).tooltip('show');
                    }
                });

                $('body').on('keyup', 'textarea.js-reason-assessment', function() {
                    if ($(this).val() !== '') {
                        $(this).tooltip('destroy');
                    }
                    else if (parseInt(inp.val()) !== parseInt(inp.data('apply')) && $(inp.data('textarea')).val() === '' && $('#diff_reason').data('value') === 1) {
                        $(inp.data('textarea')).tooltip('show');
                        error = true;
                    }
                });
                $('body').on('submit', 'form.js-form-assessment', function() {
                    // validate
                    var error = false;
                    var sc = false;
                    $(this).find('input.input-assessment').each(function(idx, input) {
                        var inp = $(input);
                        if (inp.val() === '') {
                            $(inp.data('pop')).tooltip('show');
                            error = true;
                        }
                        else if (parseInt(inp.val()) !== parseInt(inp.data('apply')) && $(inp.data('textarea')).val() === '' && $('#diff_reason').data('value') === 1) {
                            $(inp.data('textarea')).tooltip('show');
                            error = true;
                        }

                        // 最初のエラー項目にスクロールさせる
                        if (error && !sc) {
                            var p = $(inp.data('al')).offset().top;
                            $('html,body').animate({scrollTop: p}, 'fast');
                            sc = true;
                        }
                    });
                    if (error) {
                        return false;
                    }
                });

                // 自動保存
                $('#form71').autosave({
                    callbacks: {
                        trigger: 'modify',
                        scope: 'all',
                        save: {
                            method: 'ajax',
                            options: {
                                url: 'form_7edit_appraiser.php',
                                method: 'post'
                            }
                        }
                    }
                });
                $('form.form72').autosave({
                    callbacks: {
                        trigger: 'change',
                        scope: 'all',
                        save: {
                            method: 'ajax',
                            options: {
                                url: 'form_7edit_appraiser.php',
                                method: 'post'
                            }
                        }
                    }
                });
                $('#form73').autosave({
                    callbacks: {
                        trigger: 'change',
                        scope: 'all',
                        save: {
                            method: 'ajax',
                            options: {
                                url: 'form_7edit_appraiser.php',
                                method: 'post'
                            }
                        }
                    }
                });
                $('#form73').autosave({
                    callbacks: {
                        trigger: 'modify',
                        scope: 'all',
                        save: {
                            method: 'ajax',
                            options: {
                                url: 'form_7edit_appraiser.php',
                                method: 'post'
                            }
                        }
                    }
                });
            });
        </script>
    </body>
</html>