<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 院内研修マスタ | 研修内容</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='院内研修マスタ 研修内容' hd_sub_title=''}>

            <div class="container-fluid">
                <{include file="__save_message.tpl"}>
                <div class="row-fluid">
                    <div class="btn-toolbar">
                        <button type="button" name="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                        <{if $number_of > 0}>
                            <button class="js-contents-add btn btn-small btn-primary" data-number="<{$contents|@count}>" data-count="<{$contents|@count}>" data-mode="new"><i class="icon-plus"></i> 回の追加</button>
                            <button class="js-contents-sort btn btn-small btn-primary" data-id="<{$inside_id|escape}>" style="margin-left:2px;"><i class="icon-list"></i> 研修内容一覧</button>
                        <{/if}>
                    </div>

                    <fieldset>
                        <legend>
                            <a
                                href="trn_inside_contents_modal.php?id=<{$inside_id|escape}>"
                                data-toggle="modal"
                                data-backdrop="true"
                                data-target="#inside_contents">
                                <{if $no}>(<{$no|escape}>) <{/if}><{$title|escape}>
                            </a>
                        </legend>

                        <ul id="inside_contents_nav" class="nav nav-tabs">
                            <{foreach from=$contents item=row}>
                                <li class="<{if $active === $row.number}>active<{/if}>"><a href="#number<{$row.number|escape}>" data-toggle="tab">第<{$row.number|escape}>回</a>
                                </li>
                            <{/foreach}>
                        </ul>

                        <div id="inside_contents_tab" class="tab-content">
                            <{foreach from=$contents item=row}>
                                <div class="tab-pane <{if $active === $row.number}>active<{else}>fade<{/if}> in" id="number<{$row.number|escape}>">

                                    <form class="form" method="post">
                                        <input type="hidden" class="inside_contents_id" name="inside_contents_id" value="<{$row.inside_contents_id|escape}>" />
                                        <input type="hidden" class="inside_id" name="id" value="<{$inside_id|escape}>" />
                                        <input type="hidden" class="number" name="number" value="<{$row.number|escape}>" />

                                        <div class="control-group">
                                            <label class="control-label">
                                                <strong>研修のねらい</strong>
                                            </label>
                                            <div class="controls form-inline">
                                                <textarea name="aim" rows="3" class="input-xxlarge js-reset-target"><{$row.aim|escape}></textarea>
                                            </div>
                                            <{if $active === $row.number}>
                                                <{foreach from=$error.aim item=msg}>
                                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                                <{/foreach}>
                                            <{/if}>
                                        </div>

                                        <div class="control-group">
                                            <label class="control-label">
                                                <strong>研修目標</strong>
                                            </label>
                                            <div class="controls form-inline">
                                                <textarea name="objective" rows="3" class="input-xxlarge js-reset-target"><{$row.objective|escape}></textarea>
                                            </div>
                                            <{if $active === $row.number}>
                                                <{foreach from=$error.objective item=msg}>
                                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                                <{/foreach}>
                                            <{/if}>
                                        </div>

                                        <div class="control-group">
                                            <label class="control-label">
                                                <strong>研修内容</strong>
                                                <span class="badge badge-important required">必須</span>
                                            </label>
                                            <div class="controls form-inline">
                                                <textarea name="contents" rows="3" class="contents input-xxlarge js-reset-target"><{$row.contents|escape}></textarea>
                                            </div>
                                            <{if $active === $row.number}>
                                                <{foreach from=$error.contents item=msg}>
                                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                                <{/foreach}>
                                            <{/if}>
                                        </div>

                                        <div class="control-group">
                                            <label class="control-label">
                                                <strong>研修方法</strong>
                                                <span class="badge badge-important required">必須</span>
                                            </label>
                                            <div class="controls form-inline">
                                                <textarea name="method" rows="2" class="input-xxlarge js-reset-target"><{$row.method|escape}></textarea>
                                            </div>
                                            <{if $active === $row.number}>
                                                <{foreach from=$error.method item=msg}>
                                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                                <{/foreach}>
                                            <{/if}>
                                        </div>

                                        <div class="control-group">
                                            <label class="control-label">
                                                <strong>研修前の課題</strong>
                                                <span class="badge badge-important required">必須</span>
                                            </label>
                                            <div class="controls form-inline">
                                                <label class="radio inline"><input type="radio" class="js-check js-reset-true" data-id="#before_work<{$row.number|escape}>" name="before_work" value="t" <{if $row.before_work === 't'}>checked<{/if}> />有り</label>
                                                <label class="radio inline"><input type="radio" class="js-check" data-id="#before_work<{$row.number|escape}>" name="before_work" value="f" <{if $row.before_work === 'f'}>checked<{/if}> />無し</label>
                                            </div>
                                            <{if $active === $row.number}>
                                                <{foreach from=$error.before_work item=msg}>
                                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                                <{/foreach}>
                                            <{/if}>
                                        </div>

                                        <div id="before_work<{$row.number|escape}>" style="margin-left:20px;">
                                            <div class="control-group">
                                                <label class="control-label"><strong>課題内容</strong></label>
                                                <div class="controls form-inline">
                                                    <textarea name="before_work_contents" rows="3" class="input-xxlarge js-reset-target" <{if $row.before_work === 'f'}>disabled<{/if}>><{$row.before_work_contents|escape}></textarea>
                                                </div>
                                                <{if $active === $row.number}>
                                                    <{foreach from=$error.before_work_contents item=msg}>
                                                        <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                                    <{/foreach}>
                                                <{/if}>
                                            </div>

                                            <div class="control-group">
                                                <label class="control-label"><strong>提出方法</strong></label>
                                                <div class="controls form-inline">
                                                    <textarea name="before_work_submit" rows="3" class="input-xxlarge js-reset-target" <{if $row.before_work === 'f'}>disabled<{/if}>><{$row.before_work_submit|escape}></textarea>
                                                </div>
                                                <{if $active === $row.number}>
                                                    <{foreach from=$error.before_work_submit item=msg}>
                                                        <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                                    <{/foreach}>
                                                <{/if}>
                                            </div>

                                            <div class="control-group">
                                                <label class="control-label"><strong>備考</strong></label>
                                                <div class="controls form-inline">
                                                    <textarea name="before_work_notes" rows="3" class="input-xxlarge js-reset-target" <{if $row.before_work === 'f'}>disabled<{/if}>><{$row.before_work_notes|escape}></textarea>
                                                </div>
                                                <{if $active === $row.number}>
                                                    <{foreach from=$error.before_work_notes item=msg}>
                                                        <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                                    <{/foreach}>
                                                <{/if}>
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label class="control-label">
                                                <strong>研修後の課題</strong>
                                                <span class="badge badge-important required">必須</span>
                                            </label>
                                            <div class="controls form-inline">
                                                <label class="radio inline"><input type="radio" class="js-check js-reset-true" data-id="#after_work<{$row.number|escape}>" name="after_work" value="t" <{if $row.after_work === 't'}>checked<{/if}> />有り</label>
                                                <label class="radio inline"><input type="radio" class="js-check" data-id="#after_work<{$row.number|escape}>" name="after_work" value="f" <{if $row.after_work === 'f'}>checked<{/if}> />無し</label>
                                            </div>
                                            <{if $active === $row.number}>
                                                <{foreach from=$error.after_work item=msg}>
                                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                                <{/foreach}>
                                            <{/if}>
                                        </div>

                                        <div id="after_work<{$row.number|escape}>" style="margin-left:20px;">
                                            <div class="control-group">
                                                <label class="control-label"><strong>課題内容</strong></label>
                                                <div class="controls form-inline">
                                                    <textarea name="after_work_contents" rows="3" class="input-xxlarge js-reset-target" <{if $row.after_work === 'f'}>disabled<{/if}>><{$row.after_work_contents|escape}></textarea>
                                                </div>
                                                <{if $active === $row.number}>
                                                    <{foreach from=$error.after_work_contents item=msg}>
                                                        <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                                    <{/foreach}>
                                                <{/if}>
                                            </div>

                                            <div class="control-group">
                                                <label class="control-label"><strong>提出方法</strong></label>
                                                <div class="controls form-inline">
                                                    <textarea name="after_work_submit" rows="3" class="input-xxlarge js-reset-target" <{if $row.after_work === 'f'}>disabled<{/if}>><{$row.after_work_submit|escape}></textarea>
                                                </div>
                                                <{if $active === $row.number}>
                                                    <{foreach from=$error.after_work_submit item=msg}>
                                                        <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                                    <{/foreach}>
                                                <{/if}>
                                            </div>

                                            <div class="control-group">
                                                <label class="control-label"><strong>備考</strong></label>
                                                <div class="controls form-inline">
                                                    <textarea name="after_work_notes" rows="3" class="input-xxlarge js-reset-target" <{if $row.after_work === 'f'}>disabled<{/if}>><{$row.after_work_notes|escape}></textarea>
                                                </div>
                                                <{if $active === $row.number}>
                                                    <{foreach from=$error.after_work_notes item=msg}>
                                                        <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                                    <{/foreach}>
                                                <{/if}>
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label class="control-label">
                                                <strong>振り返り</strong>
                                                <span class="badge badge-important required">必須</span>
                                            </label>
                                            <div id="ts" class="controls form-inline">
                                                <label class="radio inline"><input type="radio" name="looking_back" data-id="#tmpl_work<{$row.number|escape}>" class="js-check js-reset-true" value="t" <{if $row.looking_back === 't'}>checked<{/if}> />有り</label>
                                                <label class="radio inline"><input type="radio" name="looking_back" data-id="#tmpl_work<{$row.number|escape}>" class="js-check" value="f" <{if $row.looking_back === 'f'}>checked<{/if}> />無し</label>
                                            </div>
                                            <{if $active === $row.number}>
                                                <{foreach from=$error.looking_back item=msg}>
                                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                                <{/foreach}>
                                            <{/if}>
                                        </div>
                                        <div id="tmpl_work<{$row.number|escape}>" style="margin-left:20px;">
                                            <label class="control-label">
                                                <strong>テンプレート</strong>
                                            </label>

                                            <select id="tmpl_id" name="tmpl_id" class="input-xlarge js-pulldown-inside" <{if $row.looking_back === 'f'}>disabled<{/if}>>
                                                <{if $row.form}>
                                                    <option value="">登録済み</option>
                                                <{/if}>
                                                <{foreach item=tpl from=$list_template}>
                                                    <option value="<{$tpl.tmpl_id|escape}>" <{if $tpl.tmpl_id == $row.tmpl_id}>selected<{/if}>><{$tpl.title|escape}></option>
                                                <{/foreach}>
                                            </select>
                                            <{* validateエラーの時に登録済みが消えないようにする *}>
                                            <input type="hidden" id="form" name="form" value="<{if $row.form}>1<{/if}>">
                                        </div>

                                        <div class="btn-toolbar">
                                            <button type="button" name="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                                            <{if $row.inside_contents_id}>
                                                <button type="submit" name="btn_save" class="btn_save btn btn-small btn-primary" value="btn_save"><i class="icon-save"></i> 変更する</button>
                                                <button type="button" class="js-contents-add btn btn-small btn-danger" data-number="<{$row.number|escape}>" data-count="<{$contents|@count}>" data-mode="copy"><i class="icon-copy"></i> この内容をコピーする</button>
                                                <button type="submit" name="btn_remove" class="btn_remove btn btn-small btn-danger js-remove" value="btn_remove"><i class="icon-trash"></i> 削除する</button>
                                            <{else}>
                                                <button type="submit" name="btn_save" class="btn btn-small btn-primary" value="btn_save"><i class="icon-save"></i> 登録する</button>
                                            <{/if}>
                                        </div>
                                    </form>
                                </div>
                            <{/foreach}>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
        <{include file="__modal_view.tpl" modal_id="inside_contents" modal_title="研修内容"}>
        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {
            <{if $error}>
                $('.js-contents-add').remove();
            <{/if}>

                // 研修課題のチェック
                $('body').on('click', 'input.js-check', function() {
                    if ($(this).val() === 't') {
                        $($(this).data('id')).find('textarea').prop('disabled', false);
                        $($(this).data('id')).find('select').prop('disabled', false);
                    }
                    else {
                        $($(this).data('id')).find('textarea').prop('disabled', true);
                        $($(this).data('id')).find('select').prop('disabled', true);
                    }
                });

                //並べ替え
                $('.js-contents-sort').click(function() {
                    $('body').append('<form id="contents-sort"></form>');
                    $('#contents-sort')
                        .attr({method: 'post', action: 'adm_inside_sort_number.php'})
                        .append($('<input></input>').attr({type: 'hidden', name: 'id', value: $(this).data('id')}))
                        .submit();
                });
                // 回を追加
                $('.js-contents-add').click(function() {
                    var base = $(this).data('number');
                    var number = $(this).data('count') + 1;
                    var nav = $('#inside_contents_nav');
                    var tab = $('#inside_contents_tab');

                    nav.find('li').removeClass('active');
                    nav.append('<li class="active"><a href="#number' + number + '" data-toggle="tab">第' + number + '回</a></li>');

                    tab.find('div.tab-pane').removeClass('active').addClass('fade');
                    tab.append('<div class="tab-pane active in" id="number' + number + '"></div>');
                    var form = $('#number' + base).html().replace(/\_work[0-9]+/g, '_work' + number);
                    $('#number' + number).append(form);

                    var newform = $('#number' + number).find('form');
                    newform.find('input.inside_contents_id').val('');
                    newform.find('input.number').val(number);
                    if ($(this).data('mode') === 'new') {
                        newform.find('.js-reset-target').val('');
                        newform.find('.js-reset-true').click();
                    }
                    else {
                        var contents = newform.find('textarea.contents').first();
                        contents.val('コピー 〜 ' + contents.val());
                    }

                    newform.find('#tmpl_id').children("option[value='']").remove();
                    newform.find('#form').val('');

            <{* html()だと動的に変更したradioが反映されない。とりあえずイベントを発生させて状態を合わせる *}>
                    newform.find("input.js-check:checked").trigger("click");

                    $('.js-contents-add').remove();
                    newform.find('.btn_save').html('<i class="icon-save"></i> 登録する');
                    newform.find('.btn_remove').remove();
                });

            });
        </script>
    </body>
</html>
