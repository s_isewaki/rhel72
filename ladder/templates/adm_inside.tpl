<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 院内研修マスタ</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='院内研修マスタ' hd_sub_title=''}>

            <div class="container-fluid">
                <{include file="__save_message.tpl" msg_text="コピーしました"}>
                <div class="row-fluid">
                    <form method="post" id="form_inside">
                        <div class="control-group">
                            <button type="button" data-php="adm_inside_edit.php" data-mode="new" class="js-form-button btn btn-small btn-primary"><i class="icon-plus"></i> 講座を登録する</button>
                            <button type="button" data-php="adm_inside_print.php" data-target="form2" class="js-form-print btn btn-small pull-right"><i class="icon-print"></i> 印刷する</button>
                        </div>
                        <select id="pulldown_year" name="srh_year" class="input-medium js-pulldown-inside">
                            <{foreach item=year from=$year_list}>
                                <option value="<{$year|escape}>"><{$year|escape}>年度</option>
                            <{/foreach}>
                        </select>
                        <select name="srh_level" id="srh_level" class="input-small js-pulldown-inside">
                            <option value="">レベル</option>
                            <option value="0">なし</option>
                            <option value="1">I</option>
                            <option value="2">II</option>
                            <option value="3">III</option>
                            <option value="4">IV</option>
                            <option value="5">V</option>
                        </select>
                        <select name="srh_guideline" class="input-medium js-pulldown-inside">
                            <option value="">カテゴリ</option>
                            <{foreach item=row from=$group1}>
                                <option value="<{$row.guideline_group1|escape}>"><{$row.name|escape}></option>
                            <{/foreach}>
                        </select>
                        <{if $list}>
                            <{include file="__pager.tpl"}>
                            <table class="table table-bordered table-hover table-condensed ldr-table">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>研修名</th>
                                        <th>企画担当</th>
                                        <th>Lv</th>
                                        <th>カテゴリ</th>
                                        <th>講師</th>
                                        <th>編集</th>
                                        <th>コピー</th>
                                        <th>回数</th>
                                        <th>日程</th>
                                        <th>印刷</th>
                                        <th>更新</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <{foreach item=row from=$list}>
                                        <tr>
                                            <td class="ldr-td-name"><{$row.no|escape}></td>
                                            <td class="ldr-td-text-center"><{$row.title|escape}></td>
                                            <td class="ldr-td-text-left">
                                                <{if $row.planner}>
                                                    <div
                                                        <{if $row.emp_planner}>
                                                            rel="popover" data-trigger="hover" data-html="true" data-placement="bottom" data-animation="true"
                                                            data-content="
                                                            <{foreach item=rowp from=$row.emp_planner}>
                                                                <span class='label label-info'><{$rowp.emp_name|escape}></span><br/>
                                                            <{/foreach}>"
                                                        <{/if}>
                                                        ><{$row.planner|escape}></div>
                                                <{else}>
                                                    <{foreach item=rowp from=$row.emp_planner}>
                                                        <span class='label label-info'><{$rowp.emp_name|escape}></span>
                                                    <{/foreach}>
                                                <{/if}>
                                            </td>
                                            <td class="ldr-td-level"><{$roman_num[$row.level]|escape}></td>
                                            <td class="ldr-td-name"><{$row.category|escape}></td>
                                            <td class="ldr-td-text-left">
                                                <{if $row.coach}>
                                                    <div
                                                        <{if $row.emp_coach}>
                                                            rel="popover" data-trigger="hover" data-html="true" data-placement="bottom" data-animation="true"
                                                            data-content="
                                                            <{foreach item=rowc from=$row.emp_coach}>
                                                                <span class='label label-info'><{$rowc.emp_name|escape}></span><br/>
                                                            <{/foreach}>"
                                                        <{/if}>
                                                        ><{$row.coach|escape}></div>
                                                <{else}>
                                                    <{foreach item=rowc from=$row.emp_coach}>
                                                        <span class='label label-info'><{$rowc.emp_name|escape}></span>
                                                    <{/foreach}>
                                                <{/if}>
                                            </td>
                                            <td class="ldr-td-button">
                                                <button type="button" data-php="adm_inside_edit.php" data-mode="edit" data-id="<{$row.inside_id}>" class="js-form-button btn btn-small btn-danger">編集</button>
                                            </td>
                                            <td class="ldr-td-button">
                                                <a href="adm_inside_copy_modal.php?id=<{$row.inside_id}>" data-toggle="modal" data-target="#mst_copy" class="btn btn-small btn-danger">コピー</a>
                                            </td>
                                            <td class="ldr-td-button">
                                                <button type="button" data-php="adm_inside_edit_number.php" data-id="<{$row.inside_id}>" class="js-form-button btn btn-small btn-primary">
                                                    <{if $row.number_of > 0}>
                                                        <{$row.number_of|escape}>回
                                                    <{else}>
                                                        未設定
                                                    <{/if}>
                                                </button>
                                            </td>
                                            <td class="ldr-td-button">
                                                <{if $row.number_of > 0}>
                                                    <button type="button" data-php="adm_inside_date.php" data-id="<{$row.inside_id}>" class="js-form-button btn btn-small btn-primary">
                                                        <{if $row.count_date > 0}>
                                                            <{$row.count_date|escape}>日程
                                                        <{else}>
                                                            未設定
                                                        <{/if}>
                                                    </button>
                                                <{else}>
                                                    -
                                                <{/if}>
                                            </td>
                                            <td class="ldr-td-button"><button type="button" data-php="adm_inside_print.php" data-id="<{$row.inside_id}>" data-target="form2" class="js-form-print btn btn-small"><i class="icon-print"></i></button></td>
                                            <td class="ldr-td-date">
                                                <a href="adm_inside_history_modal.php?id=<{$row.inside_id|escape}>"
                                                   data-toggle="modal"
                                                   data-backdrop="true"
                                                   data-target="#inside_history">
                                                    <{$row.update_emp_name|escape}><br/><{$row.updated_on|date_format_jp:"%-m月%-d日(%a) %R"}>
                                                </a>
                                            </td>
                                        </tr>
                                    <{/foreach}>
                                </tbody>
                            </table>

                        <{else}>
                            <div class="alert alert-error">検索条件に該当する院内研修は見つかりませんでした</div>
                        <{/if}>

                    </form>
                </div>
            </div>
        </div>
        <{include file="__modal_form.tpl" modal_id="mst_copy" modal_title="講座コピー"}>

        <{include file="__modal_view.tpl" modal_id="inside_history" modal_title="更新履歴"}>
        <{include file="__js.tpl"}>
        <{include file="__modal_form_js.tpl"}>
        <{include form_selector="#form_inside" file="__pager_js.tpl"}>
        <script type="text/javascript">
            // プルダウン選択
            $('.js-pulldown-inside').change(function() {
                $('#form_inside')
                    .append($('<input></input>').attr({type: 'hidden', name: 'search', value: 'search'}))
                    .submit();
            });
            $(document).on('click', ".js-number-modal", function() {
                if ($("#number_ignore:checked").val()) {
                    $("#date_ignore").prop('checked', true);
                }
            });
            $(document).on('click', ".js-date-modal", function() {
                if ($("#date_copy:checked").val()) {
                    $("#number_copy").prop('checked', true);
                }
            });
        </script>
    </body>
</html>
