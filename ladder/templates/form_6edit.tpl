<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | <{$ldr.config.form_6.title|escape}>入力</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title="`$ldr.config.form_6.title`入力" hd_sub_title=$ldr.config.form_6.detailL}>

            <div class="container-fluid">
                <div class="row-fluid">

                    <form method="POST">
                        <{include file='__form_view.tpl' op_srh='false' tmp_id='form_6edit'}>
                    </form>

                    <form class="form" id="edit_form" method="POST">
                        <input type="hidden" name="id" value="" />
                        <input type="hidden" name="save" value="ajax" />
                        <div class="well well-small">
                            <{if $ldr.config.report_text}>
                                <div><{$ldr.config.report_text|nl2br}></div>
                                <hr />
                            <{/if}>

                            <div class="control-group">
                                <label class="control-label" for="narrative">
                                    <strong>事例内容</strong>
                                    <span class="badge badge-important required">必須</span>
                                </label>
                                <div class="controls">
                                    <textarea name="narrative" rows="10" class="input-maxlarge js-autoheight"></textarea>
                                </div>
                                <{foreach from=$error.narrative item=msg}>
                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                <{/foreach}>
                            </div>

                            <div class="btn-toolbar">
                                <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                                <button type="submit" name="btn_save" id="btn_save" class="btn btn-small btn-primary" value="btn_save"><i class="icon-save"></i> 保存する</button>
                            </div>
                            <p><small><i class="icon-exclamation-sign"></i> 入力内容は自動保存されます。明示的に保存したい場合は<b>[保存する]ボタン</b>で保存してください。</small></p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {

                // 自動保存
                $('form').autosave({
                    callbacks: {
                        trigger: 'modify',
                        scope: 'all',
                        save: {
                            method: 'ajax',
                            options: {
                                url: 'form_6edit.php',
                                method: 'post'
                            }
                        }
                    }
                });
            });
        </script>
    </body>
</html>