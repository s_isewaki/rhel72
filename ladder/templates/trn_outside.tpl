<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 院外研修 申込済一覧</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='院外研修 申込済一覧' hd_sub_title=''}>

            <div class="container-fluid">
                <div class="row-fluid">
                    <form method="post" id="outside_form">
                        <{if $list}>
                            <div class="control-group">
                                <{include file="__pager.tpl"}>
                            </div>
                            <table class="table table-bordered table-hover table-condensed ldr-table">
                                <thead>
                                    <tr>
                                        <th>更新日時</th>
                                        <th>開催日</th>
                                        <th>主催</th>
                                        <th>(研修番号)<br/>研修名</th>
                                        <th>区分</th>
                                        <th>申込</th>
                                        <th>師長<br />承認</th>
                                        <th>受付</th>
                                        <th>報告</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <{foreach item=row from=$list}>
                                        <tr>
                                            <td class="ldr-td-date"><span title="<{$row.updated_on|date_format_jp:"%Y年%-m月%-d日(%a) %R"}>"><{$row.updated_on|date_format_jp:"%-m月%-d日(%a)<br/>%R"}></span></td>
                                            <td class="ldr-td-date">
                                                <span title="<{$row.start_date|date_format_jp:"%Y年%-m月%-d日(%a)"}>"><{$row.start_date|date_format_jp:"%-m月%-d日(%a)"}></span>
                                                <{if $row.start_date <> $row.last_date}>
                                                    〜 <span title="<{$row.last_date|date_format_jp:"%Y年%-m月%-d日(%a)"}>"><{$row.last_date|date_format_jp:"%-m月%-d日(%a)"}></span>
                                                <{/if}>
                                            </td>
                                            <td class="ldr-td-text-center"><{$row.organizer|escape}></td>
                                            <td class="ldr-td-text-left">
                                                <span rel="popover" data-trigger="hover" data-html="true" data-placement="top" data-animation="true"
                                                      data-content="
                                                      <div class='ldr-popover'>開催地: <strong><{$row.pref|default:'(未設定)'|escape}></strong></div><br/>
                                                      <div class='ldr-popover'>開催場所: <strong><{$row.place|default:'(未設定)'|escape}></strong></div>">
                                                    (<{$row.no|escape}>)<br/>
                                                </span>
                                                <{$row.contents|escape}>
                                            </td>
                                            <td class="ldr-td-name"><{$type_text[$row.type]|escape}></td>
                                            <td class="ldr-td-name">
                                                <{if $row.status === '1'}>
                                                    <button type="button" data-php="trn_outside_apply.php" data-mode="edit" data-id="<{$row.training_id}>" class="js-form-button btn btn-small btn-danger">修正</button>
                                                <{elseif $row.status === '-1'}>
                                                    <span class="label label-warning"><strong>取下</strong></span>
                                                <{else}>
                                                    <button type="button" data-php="trn_outside_apply_view.php" data-mode="view" data-id="<{$row.training_id}>" class="js-form-button btn btn-small btn-primary">済</button>
                                                <{/if}>
                                            </td>
                                            <td class="ldr-td-name">
                                                <{if $row.status === '1'}>
                                                    <span class="label label-important"><strong>待ち</strong></span>
                                                <{elseif $row.status==='0' or $row.status >= '2' or $row.status < '-2'}>
                                                    <span class="label label-info"><strong>済</strong></span>
                                                <{elseif $row.status === '-2'}>
                                                    <span class="label label-warning"><strong>取下</strong></span>
                                                <{/if}>
                                            </td>
                                            <td class="ldr-td-name">
                                                <{if $row.status === '2'}>
                                                    <span class="label label-important"><strong>待ち</strong></span>
                                                <{elseif $row.status==='0' or $row.status >= '3'  or $row.status < '-3'}>
                                                    <span class="label label-info"><strong>済</strong></span>
                                                <{elseif $row.status === '-3'}>
                                                    <span class="label label-warning"><strong>取下</strong></span>
                                                <{/if}>
                                            </td>
                                            <td class="ldr-td-name">
                                                <{if $row.status === '3'}>
                                                    <button type="button" data-php="trn_outside_report.php" data-mode="view" data-id="<{$row.training_id}>" class="js-form-button btn btn-small btn-danger">記入</button>
                                                <{elseif $row.status === '0'}>
                                                    <button type="button" data-php="trn_outside_report.php" data-mode="view" data-id="<{$row.training_id}>" class="js-form-button btn btn-small btn-danger">修正</button>
                                                <{/if}>
                                            </td>
                                        </tr>
                                    <{/foreach}>
                                </tbody>
                            </table>

                        <{else}>
                            <div class="alert alert-error">院外研修の申請はありません</div>
                        <{/if}>
                    </form>
                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <{include form_selector="#outside_form" file="__pager_js.tpl"}>
    </body>
</html>
