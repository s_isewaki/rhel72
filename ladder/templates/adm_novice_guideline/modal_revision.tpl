<form id="form_rv_modal" action="adm_novice_guideline.php" method="post">
    <div class="form-horizontal">
        <div id="modal_revision_name" class="control-group">
            <label class="control-label">
                <span class="badge badge-important required">必須</span>
                <strong>版名</strong>
            </label>
            <div class="controls">
                <input type="text" name="name" value="" class="input-large" placeholder="版名を入力してください" />
                <div id="error_revision_name" class="help-block hide"></div>
            </div>
        </div>
    </div>
    <input type="hidden" name="id" value="" />
    <input type="hidden" name="mode" value="revision" />
    <input type="hidden" id="mode2_revision" name="mode2" value="validate" />
</form>