<form id="form_gl_modal" action="adm_novice_guideline.php" method="post">
    <div class="form-horizontal">
        <div id="modal_guideline_revision" class="control-group">
            <label class="control-label">
                <span class="badge badge-important required">ɬ��</span>
                <strong>��</strong>
            </label>
            <div class="controls">
                <select name="revision_id" class="js-select-revision" data-target="#select_gl_g1">
                    <{foreach item=row from=$revision_list}>
                        <option value="<{$row.revision_id|escape}>"><{$row.name|escape}></option>
                    <{/foreach}>
                </select>
                <div id="error_guideline_revision" class="help-block hide"></div>
            </div>
        </div>
        <div id="modal_guideline_group1" class="control-group">
            <label class="control-label">
                <span class="badge badge-important required">ɬ��</span>
                <strong>���롼��1</strong>
            </label>
            <div class="controls">
                <select name="group1" class="js-select-group1 input-xlarge" data-target="#select_gl_g2" id="select_gl_g1">
                    <{foreach item=row from=$group1_list}>
                        <option value="<{$row.group1|escape}>"><{$row.name|escape}></option>
                    <{/foreach}>
                </select>
                <div id="error_guideline_group1" class="help-block hide"></div>
            </div>
        </div>
        <div id="modal_guideline_group2" class="control-group">
            <label class="control-label">
                <span class="badge badge-important required">ɬ��</span>
                <strong>���롼��2</strong>
            </label>
            <div class="controls">
                <select id="select_gl_g2" name="group2" class="input-xlarge">
                    <{foreach item=row from=$group2_list}>
                        <option value="<{$row.group2|escape}>"><{$row.name|escape}></option>
                    <{/foreach}>
                </select>
                <div id="error_guideline_guideline_group2" class="help-block hide"></div>
            </div>
        </div>
        <div id="modal_guideline_guideline" class="control-group">
            <label class="control-label">
                <span class="badge badge-important required">ɬ��</span>
                <strong>��ã��ɸ</strong>
            </label>
            <div class="controls">
                <textarea name="guideline" class="input-xlarge" rows="5"></textarea>
                <div id="error_guideline_guideline" class="help-block hide"></div>
            </div>
        </div>
        <div id="modal_guideline_mhlw" class="control-group">
            <label class="control-label">
                <strong>��ϫ�ʥ����ɥ饤��</strong>
            </label>
            <div class="controls">
                <label class="radio"><input type="radio" name="mhlw" value="t" /> ����ϫƯ�ʥ����ɥ饤�����</label>
                <label class="radio"><input type="radio" name="mhlw" value="f" /> �±����ꥸ�ʥ����</label>
                <div id="error_guideline_mhlw" class="help-block hide"></div>
            </div>
        </div>
    </div>
    <input type="hidden" name="id" value="" />
    <input type="hidden" name="mode" value="guideline" />
    <input type="hidden" id="mode2_guideline" name="mode2" value="validate" />
</form>
