<form id="form_g1_modal" action="adm_novice_guideline.php" method="post">
    <div class="form-horizontal">
        <div id="modal_group1_revision" class="control-group">
            <label class="control-label">
                <span class="badge badge-important required">必須</span>
                <strong>版</strong>
            </label>
            <div class="controls">
                <select name="revision_id">
                    <{foreach item=row from=$revision_list}>
                        <option value="<{$row.revision_id|escape}>"><{$row.name|escape}></option>
                    <{/foreach}>
                </select>
                <div id="error_group1_revision" class="help-block hide"></div>
            </div>
        </div>
        <div id="modal_group1_name" class="control-group">
            <label class="control-label">
                <span class="badge badge-important required">必須</span>
                <strong>グループ1</strong>
            </label>
            <div class="controls">
                <textarea name="name" class="input-xlarge" rows="3" placeholder="グループ1を入力してください"></textarea>
                <div id="error_group1_name" class="help-block hide"></div>
            </div>
        </div>
    </div>
    <input type="hidden" name="id" value="" />
    <input type="hidden" name="mode" value="group1" />
    <input type="hidden" id="mode2_group1" name="mode2" value="validate" />
</form>
