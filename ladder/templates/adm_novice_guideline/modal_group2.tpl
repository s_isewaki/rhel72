<form id="form_g2_modal" action="adm_novice_guideline.php" method="post">
    <div class="form-horizontal">
        <div id="modal_group2_revision" class="control-group">
            <label class="control-label">
                <span class="badge badge-important required">必須</span>
                <strong>版</strong>
            </label>
            <div class="controls">
                <select name="revision_id" class="js-select-revision" data-target="#select_g2_g1">
                    <{foreach item=row from=$revision_list}>
                        <option value="<{$row.revision_id|escape}>"><{$row.name|escape}></option>
                    <{/foreach}>
                </select>
                <div id="error_group2_revision" class="help-block hide"></div>
            </div>
        </div>
        <div id="modal_group2_group1" class="control-group">
            <label class="control-label">
                <span class="badge badge-important required">必須</span>
                <strong>グループ1</strong>
            </label>
            <div class="controls">
                <select name="group1" id="select_g2_g1">
                    <{foreach item=row from=$group1_list}>
                        <option value="<{$row.group1|escape}>"><{$row.name|escape}></option>
                    <{/foreach}>
                </select>
                <div id="error_group2_group1" class="help-block hide"></div>
            </div>
        </div>
        <div id="modal_group2_name" class="control-group">
            <label class="control-label">
                <span class="badge badge-important required">必須</span>
                <strong>グループ2</strong>
            </label>
            <div class="controls">
                <textarea name="name" class="input-xlarge" rows="3" placeholder="グループ2を入力してください"></textarea>
                <div id="error_group2_name" class="help-block hide"></div>
            </div>
        </div>
    </div>
    <input type="hidden" name="id" value="" />
    <input type="hidden" name="mode" value="group2" />
    <input type="hidden" id="mode2_group2" name="mode2" value="validate" />
</form>
