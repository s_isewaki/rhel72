<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>
        <{javascript src="js/flotr2.js"}>
        <!--[if lte IE 8]>
        <{javascript src="js/excanvas.js"}>
        <![endif]-->

        <title>CoMedix キャリア開発ラダー | 院内研修振り返り一覧</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='院内研修振り返り一覧' hd_sub_title=''}>

            <div class="container-fluid">
                <form method="post" id="inside_form">
                    <input type='hidden' id='id' name='id' value='<{$smarty.post.id}>' />
                    <div id="contents" class="well well-small">
                        <div class="btn-toolbar">
                            <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                            <button type="submit" id="export_csv" name="export_csv" class="btn btn-small btn-default"><i class="icon-cloud-download"></i> CSVエクスポート</button>
                            <button type="button" onclick="printing();" class="btn btn-small btn-primary pull-right"><i class="icon-print"></i> 印刷する</button>
                        </div>
                        <fieldset>
                            <legend>
                                <a
                                    href="trn_inside_contents_modal.php?id=<{$trn.inside_id|escape}>"
                                    data-toggle="modal"
                                    data-backdrop="true"
                                    data-target="#inside_contents">
                                    <{if $trn.no}>(<{$trn.no|escape}>) <{/if}><{$trn.title|escape}>
                                </a>
                                <a
                                    href="trn_inside_number_modal.php?id=<{$trn.inside_id|escape}>&number=<{$trn.contents.number|escape}>"
                                    data-toggle="modal"
                                    data-backdrop="true"
                                    data-target="#inside_number">
                                    第<{$trn.contents.number|escape}>回 / 全<{$trn.number_of|escape}>回
                                </a>
                            </legend>

                            <{foreach item=c from=$count}>
                                <div class="block">
                                    <a href="#<{$c.key|escape}>" class="block-heading"><{$c.form.name|escape}></a>
                                    <div id="<{$c.key|escape}>" class="block-body row-fluid">
                                        <div class="<{if $c.form.type !== 'textarea'}>span5<{else}>span11<{/if}>">
                                            <table class="table table-condensed table-hover">
                                                <tbody>
                                                    <{foreach item=row from=$c.table name="table"}>
                                                        <tr>
                                                            <td class="ldr-td-text-left"><strong><{$row.label|escape}></strong></td>
                                                            <td class="ldr-td-number" style="width:30px;"><{$row.value|escape}></td>
                                                            <{if $c.form.type !== 'textarea'}>
                                                                <td class="ldr-td-number" style="width:30px;"><{math equation="x / y * 100" x=$row.value y=$c.count assign="p"}><{$p|string_format:"%.1f"}>%</td>
                                                            <{/if}>
                                                        </tr>
                                                    <{/foreach}>
                                                </tbody>
                                                <tfoot>
                                                    <tr class="info">
                                                        <th class="ldr-td-text">合計</th>
                                                        <td class="ldr-td-number" style="width:30px;"><{$c.total|escape}></td>
                                                        <{if $c.form.type !== 'textarea'}>
                                                            <td class="ldr-td-number" style="width:30px;">-</td>
                                                        <{/if}>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>

                                        <{if $c.form.type !== 'textarea'}>
                                            <div class="span6">
                                                <div class="canvas" data-key="<{$c.key|escape}>" data-id="<{$trn.contents.inside_contents_id|escape}>"></div>
                                            </div>
                                        <{/if}>
                                    </div>
                                </div>
                            <{/foreach}>


                        </fieldset>
                        <div class="btn-toolbar">
                            <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                            <button type="submit" id="export_csv" name="export_csv" class="btn btn-small btn-default"><i class="icon-cloud-download"></i> CSVエクスポート</button>
                            <button type="button" onclick="printing();" class="btn btn-small btn-primary pull-right"><i class="icon-print"></i> 印刷する</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <{include file="__modal_view.tpl" modal_id="inside_contents" modal_title="研修内容"}>
        <{include file="__modal_view.tpl" modal_id="inside_number" modal_title="研修内容"}>
        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {
                // チャート
                $('div.canvas').each(function() {
                    var canvas = $(this);
                    var url = 'trn_accept_inside_check_charts.php';
                    url += '?key=' + $(this).data('key');
                    url += '&id=' + $(this).data('id');

                    // chart
                    $.ajax({
                        url: url,
                        dataType: 'json',
                        type: 'post',
                        success: function(data) {
                            Flotr.draw(canvas.get(0), data.data, data.option);
                        }
                    });
                });

            });

            function printing() {
                var h = 600;
                var w = 800;
                var option = "scrollbars=yes,width=" + w + ",height=" + h;
                var win = window.open('trn_accept_inside_check.php?id=<{$smarty.post.id}>&print=1', 'LadderPrint', option);
            }
        </script>
    </body>
</html>