<{javascript src="../js/jquery/jquery-1.11.2.min.js"}>
<{javascript src="../js/jquery/jquery-ui-1.11.2.min.js"}>
<{javascript src="../js/jquery/jquery.cookie.js"}>
<{javascript src="js/bootstrap-2.3.2.min.js"}>
<{javascript src="js/bootstrap-datetimepicker-0.0.11.min.js"}>
<{javascript src="js/bootstrap-datepicker-0.0.11.ja.js"}>
<{javascript src="js/bootstrap-multiselect.js"}>
<{javascript src="js/fullcalendar-1.6.4.min.js"}>
<{javascript src="js/jquery.autosave.js"}>
<{javascript src="js/bloodhound.min.js"}>
<{javascript src="js/typeahead.jquery.js"}>
<{javascript src="js/common.js"}>

<script type="text/javascript">
    $(function() {
    <{if $ldr_ticket}>
        <{* multipostチェック用の one time passward*}>
        $('form').append($('<input></input>').attr({type: 'hidden', name: 'ldr_ticket', value: '<{$ldr_ticket}>'}));
    <{/if}>
    <{if $referer_url}>
        <{* 戻るの制御用 *}>
        $('form').append($('<input></input>').attr({type: 'hidden', id: 'referer_url', name: 'referer_url', value: '<{$referer_url}>'}));
    <{/if}>
    });
</script>
