<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 院内研修マスタ</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title='院内研修マスタ' hd_sub_title=''}>

            <div class="container-fluid">
                <div class="row-fluid">
                    <form class="form" method="post">
                        <input type="hidden" id="id" name="id" value="" />
                        <input type="hidden" id="inside_id" name="inside_id" value="" />

                        <div class="control-group">
                            <label class="control-label">
                                <strong>年度</strong>
                                <span class="badge badge-important required">必須</span>
                            </label>
                            <div class="controls form-inline">
                                <select name="year" class="input-medium">
                                    <{foreach item=year from=$years}>
                                        <option value="<{$year|escape}>"><{$year|escape}>年度</option>
                                    <{/foreach}>
                                </select>
                            </div>
                            <{foreach from=$error.year item=msg}>
                                <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                            <{/foreach}>
                        </div>

                        <div class="control-group">
                            <label class="control-label">
                                <strong>研修番号</strong>
                                <{if $hospital_type === '1'}><span class="badge badge-important required">必須</span><{/if}>
                            </label>
                            <div class="controls form-inline">
                                <div class="input-prepend">
                                    <span class="add-on">No.</span>
                                    <input type="text" id="no" name="no" class="input-small" maxlength="20" placeholder="研修番号" />
                                </div>
                            </div>
                            <{foreach from=$error.no item=msg}>
                                <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                            <{/foreach}>
                        </div>

                        <div class="control-group">
                            <label class="control-label">
                                <strong>研修名</strong>
                                <span class="badge badge-important required">必須</span>
                            </label>
                            <div class="controls form-inline">
                                <input type="text" id="title" name="title" class="input-xlarge" maxlength="200" placeholder="研修名を入力してください" />
                            </div>
                            <{foreach from=$error.title item=msg}>
                                <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                            <{/foreach}>
                        </div>

                        <div class="control-group">
                            <label class="control-label"><strong>テーマ</strong></label>
                            <div class="controls form-inline">
                                <textarea name="theme" rows="3" class="input-xxlarge" placeholder="テーマを入力してください"></textarea>
                            </div>
                            <{foreach from=$error.theme item=msg}>
                                <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                            <{/foreach}>
                        </div>

                        <div class="control-group">
                            <label class="control-label">
                                <strong>企画担当</strong>
                                <span class="badge badge-important required">職員選択は必須</span>
                            </label>
                            <div class="controls form-inline">
                                <div class="input-prepend">
                                    <span class="add-on">名称</span>
                                    <input type="text" name="planner" class="input-xlarge" maxlength="200" placeholder="企画担当を入力してください" />
                                </div>
                                <button type="button" class="btn btn-small btn-success js-btn-emplist" data-id="planner"><i class="icon-book"></i> 職員名簿</button>
                            </div>
                            <div class="controls clearfix">
                                <div id="planner" class="ldr-empbox <{if !$planner}>hide<{/if}>">
                                    <{foreach from=$planner key=id item=name}>
                                        <div id="planner<{$id|escape}>" class="label label-info"><{$name|escape}><input type="hidden" name="emp_planner[<{$id|escape}>]" value="<{$name|escape}>" /> <a href="javascript:void(0);" class="ldr-emp-remove js-emp-remove">&times;</a></div>
                                    <{/foreach}>
                                </div>
                            </div>
                            <{foreach from=$error.planner item=msg}>
                                <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                            <{/foreach}>
                        </div>

                        <div class="control-group">
                            <label class="control-label">
                                <{if $hospital_type === '1'}>
                                    <strong>めざすレベル</strong>
                                <{else}>
                                    <strong>対象レベル</strong>
                                <{/if}>
                                <span class="badge badge-important required">必須</span>
                            </label>
                            <div class="controls form-inline">
                                <select name="level" class="ldr-level">
                                    <option value="">なし</option>
                                    <option value="1">I</option>
                                    <option value="2">II</option>
                                    <option value="3">III</option>
                                    <option value="4">IV</option>
                                    <option value="5">V</option>
                                    <option value="12">I / II</option>
                                    <option value="123">I / II / III</option>
                                    <option value="1234">I / II / III / IV</option>
                                    <option value="23">II / III</option>
                                    <option value="234">II / III / IV</option>
                                    <option value="2345">II / III / IV / V</option>
                                    <option value="34">III / IV</option>
                                    <option value="345">III / IV / V</option>
                                    <option value="45">IV / V</option>
                                </select>
                            </div>
                            <{foreach from=$error.level item=msg}>
                                <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                            <{/foreach}>
                        </div>

                        <div class="control-group">
                            <label class="control-label">
                                <strong>めざす状態</strong>
                                <{if $hospital_type === '1'}><span class="badge badge-important required">必須</span><{/if}>
                            </label>
                            <div class="controls form-inline">
                                <div class="input-prepend">
                                    <span class="add-on">指標</span>
                                    <input type="text" id="guideline_search" class="input-xxlarge" autocomplete="off" placeholder="行動指標の番号、キーワードを入力してください" />
                                </div>
                            </div>
                            <div class="controls">
                                <div id="guideline" class="ldr-glbox">
                                    <{foreach from=$guideline key=id item=name}>
                                        <div id="gl<{$id|escape}>" class="alert alert-info"><{$name|escape}><input type="hidden" name="guideline[<{$id|escape}>]" value="<{$name|escape}>" /> <a href="javascript:void(0);" class="close js-gl-remove">&times;</a></div>
                                    <{/foreach}>
                                </div>
                            </div>
                            <{foreach from=$error.guideline item=msg}>
                                <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                            <{/foreach}>
                            <div class="controls form-inline">
                                <div class="input-prepend input-prepend-multi">
                                    <span class="add-on">その他</span>
                                    <textarea rows="3" name="guideline_text" id="guideline_text" class="input-xxlarge" placeholder="めざす状態を入力してください"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label" for="job_year">
                                <strong>カテゴリ</strong>
                                <span class="badge badge-important required">必須</span>
                            </label>
                            <div class="controls form-inline">
                                <select name="guideline_group1">
                                    <{foreach item=row from=$group1}>
                                        <option value="<{$row.guideline_group1|escape}>"><{$row.name|escape}></option>
                                    <{/foreach}>
                                </select>
                            </div>
                            <{foreach from=$error.guideline_group1 item=msg}>
                                <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                            <{/foreach}>
                        </div>

                        <div class="control-group">
                            <label class="control-label">
                                <strong>講師</strong>
                                <span class="badge badge-important required">名称または職員選択</span>
                            </label>
                            <div class="controls form-inline">
                                <div class="input-prepend">
                                    <span class="add-on">名称</span>
                                    <input type="text" name="coach" class="input-xlarge" maxlength="200" placeholder="講師を入力してください" />
                                </div>
                                <button type="button" class="btn btn-small btn-success js-btn-emplist" data-id="coach"><i class="icon-book"></i> 職員名簿</button>
                            </div>
                            <div class="controls clearfix">
                                <div id="coach" class="ldr-empbox <{if !$coach}>hide<{/if}>">
                                    <{foreach from=$coach key=id item=name}>
                                        <div id="coach<{$id|escape}>" class="label label-info"><{$name|escape}><input type="hidden" name="emp_coach[<{$id|escape}>]" value="<{$name|escape}>" /> <a href="javascript:void(0);" class="ldr-emp-remove js-emp-remove">&times;</a></div>
                                    <{/foreach}>
                                </div>
                            </div>
                            <{foreach from=$error.coach item=msg}>
                                <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                            <{/foreach}>
                        </div>

                        <div class="control-group">
                            <label class="control-label">
                                <strong>外部参加</strong>
                                <span class="badge badge-important required">必須</span>
                            </label>
                            <div class="controls form-inline">
                                <label class="radio inline"><input type="radio" name="public" value="f" />不可</label>
                                <label class="radio inline"><input type="radio" name="public" value="t" />可</label>
                            </div>
                            <{foreach from=$error.public item=msg}>
                                <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                            <{/foreach}>
                        </div>

                        <div class="control-group">
                            <label class="control-label">
                                <strong>実施上の留意点</strong>
                                <span class="badge badge-important required">必須</span>
                            </label>
                            <div class="controls form-inline">
                                <textarea name="notes" rows="5" class="input-xxlarge" placeholder="実施上の留意点を入力してください"></textarea>
                            </div>
                            <{foreach from=$error.notes item=msg}>
                                <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                            <{/foreach}>
                        </div>

                        <div class="control-group">
                            <label class="control-label">
                                <strong rel="tooltip" title="申込者には表示されません" data-placement="right">評価</strong>
                                <span class="badge badge-important required">必須</span>
                            </label>
                            <div class="controls form-inline">
                                <textarea name="assessment" rows="5" class="input-xxlarge" placeholder="評価を入力してください"></textarea>
                            </div>
                            <{foreach from=$error.assessment item=msg}>
                                <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                            <{/foreach}>
                        </div>

                        <div class="control-group">
                            <label class="control-label">
                                <strong>師長承認</strong>
                                <span class="badge badge-important required">必須</span>
                            </label>
                            <div class="controls form-inline">
                                <label class="radio inline"><input type="radio" name="approve" value="t" />研修受講には師長の承認が必要</label>
                                <label class="radio inline"><input type="radio" name="approve" value="f" />不要</label>
                            </div>
                            <{foreach from=$error.approve item=msg}>
                                <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                            <{/foreach}>
                        </div>

                        <div class="btn-toolbar">
                            <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                            <{if $smarty.post.id}>
                                <button type="submit" name="btn_save" id="btn_save" class="btn btn-small btn-primary" value="btn_save"><i class="icon-save"></i> 変更する</button>
                                <button type="button" id="btn_copy" class="btn btn-small btn-danger"><i class="icon-copy"></i> この内容をコピーする</button>
                                <button type="submit" id="btn_remove" name="btn_remove" class="btn btn-small btn-danger js-remove" value="btn_remove"><i class="icon-trash"></i> 削除する</button>
                            <{else}>
                                <button type="submit" name="btn_save" id="btn_save" class="btn btn-small btn-primary" value="btn_save"><i class="icon-save"></i> 登録する</button>
                            <{/if}>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {

                // 職員削除
                $('body').on('click', '.js-emp-remove', function() {
                    var p = $(this).parent();
                    var pp = p.parent();
                    p.remove();
                    if (pp.children().length <= 0) {
                        pp.hide();
                    }
                });

                // 指標検索
                var typeaheadItems = new Bloodhound({
                    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
                    queryTokenizer: Bloodhound.tokenizers.whitespace,
                    remote: {
                        url: 'adm_inside_edit_guideline.php?query=%QUERY',
                        replace: function(url, query) {
                            return url.replace('%QUERY', query);
                        }
                    }
                });
                $('#guideline_search').typeahead({
                    minLength: 1,
                    highLight: true
                }, {
                    name: 'guideline',
                    limit: 15,
                    displayKey: 'value',
                    source: typeaheadItems.ttAdapter()
                });
                $('#guideline_search').bind('typeahead:select', function(e, data) {
                    if ($('#gl' + data.id).size() === 0) {
                        $('#guideline')
                            .append('<div id="gl' + data.id + '" class="alert alert-info">' + data.value + '<input type="hidden" name="guideline[' + data.id + ']" value="' + data.value + '" /> <a href="javascript:void(0);" class="close js-gl-remove">&times;</a></div>');
                    }
                    $(this).typeahead('val', '');
                    $(this).typeahead('close');
                });

                // 指標削除
                $('body').on('click', '.js-gl-remove', function() {
                    $(this).parent().remove();
                });

                // コピー
                $('#btn_copy').click(function() {
                    $('#inside_id').val('');
                    $('#no').val('');
                    $('#id').val('');
                    $('#title').val('コピー 〜 ' + $('#title').val());
                    $('#btn_save').html('<i class="icon-save"></i> 登録する');
                    $('#btn_copy').remove();
                    $('#btn_remove').remove();
                });
            });

            // 職員の追加
            function add_target_list(item_id, emp_id, emp_name) {
                var emp = emp_id.split(", ");
                var empname = emp_name.split(", ");
                for (i = 0; i < emp.length; i++) {
                    if ($('#' + item_id + emp[i]).size() === 0) {
                        $('#' + item_id)
                            .append('<div id="' + item_id + emp[i] + '" class="label label-info">' + empname[i] + '<input type="hidden" name="emp_' + item_id + '[' + emp[i] + ']" value="' + empname[i] + '" /> <a href="javascript:void(0);" class="ldr-emp-remove js-emp-remove">&times;</a></div> ')
                            .show();
                    }
                }
            }
        </script>
    </body>
</html>
