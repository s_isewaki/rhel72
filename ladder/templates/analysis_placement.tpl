<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | 配置表</title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <div id="overlay">
            <div id="nowload">
                <b>Now Loading...</b>
            </div>
        </div>
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>
        <{css href="css/drag.css"}>

        <!-- メインエリア -->
        <div class="content" style="min-height: 0 !important;">
            <{include file="__header.tpl" hd_title='配置表' hd_sub_title=''}>
            <div class="container-fluid">
                <div class="row-fluid">
                    <div id="drag" style="background-color: #fff">
                        <form method="POST" id="form_placement" onsubmit="return onsub();">
                            <input type="hidden" name="basic_date" />
                            <input type="hidden" id="update_flg" name="update_flg" value="" />
                            <div class="control-group">
                                <div class="form-inline">
                                    <select name="pattern_id" id="pattern_id" class="input-xlarge">
                                        <option value="">現在の配置</option>
                                        <{foreach item=ptn from=$list_pattern}>
                                            <option value="<{$ptn.pattern_id|escape}>" <{if $smarty.post.pattern_id === $ptn.pattern_id}>selected<{/if}>>
                                                <{$ptn.pattern|escape}>
                                            </option>
                                        <{/foreach}>
                                    </select>
                                    <button type="submit" name="btn_load" class="btn btn-small btn-info" onclick="return save();" value="save"><i class="icon-download"></i> 読込</button>
                                </div>
                            </div>
                            <div class="control-group">
                                <button type="submit" name="btn_save" class="btn btn-small btn-primary js-ptn" onclick="return save();" value="save"><i class="icon-save"></i> パターンを更新する</button>
                                <a href="analysis_placement_modal.php?mode=new" data-toggle="modal" data-target="#mdl_pattern" class="btn btn-small btn-warning"><i class='icon-plus'></i> 新しいパターンを追加する</a>
                                <button type="submit" name="btn_save" class="js-remove btn btn-small btn-danger js-ptn" value="delete"><i class="icon-remove"></i> パターンを削除する</button>
                                <button type="button" data-id="<{$smarty.post.pattern_id|escape}>" data-emp="" data-php="analysis_placement.php" data-target="placement" class="js-form-print btn btn-info btn-small"><i class="icon-print"></i> 印刷する</button>
                            </div>
                            <div class="form-inline" style="margin:2px">
                                <label class="control-label">基準日:</label>
                                <strong title="<{$smarty.post.basic_date|date_format_jp:"%Y年%-m月%-d日(%a)"}>"><{$smarty.post.basic_date|date_format_jp:"%-m月%-d日(%a)"}></strong>
                                <{if $smarty.post.updated_on}>
                                    &nbsp;
                                    <label class="control-label">更新日:</label>
                                    <strong title="<{$smarty.post.updated_on|date_format_jp:"%Y年%-m月%-d日(%a) %R"}>"><{$smarty.post.updated_on|date_format_jp:"%-m月%-d日(%a) %R"}></strong>
                                <{/if}>
                            </div>
                            <div class="well well-small">
                                <{foreach item=ldr from=$ladder->lists()}>
                                    <{$ldr.title|escape}>：
                                    <{foreach item=lv from=$ldr.config.level}>
                                        <span class="ldr-roman badge badge-ldr<{$ldr.ladder_id|escape}>-lv<{$lv|escape}>">
                                            <{$ldr.short}><{$roman_num[$lv]|default:$ldr.config.level0}>
                                        </span>
                                    <{/foreach}>
                                    &nbsp;&nbsp;
                                <{/foreach}>
                            </div>
                            <table class="table table-bordered table-condensed ldr-table">
                                <thead>
                                    <tr>
                                        <th colspan="2" class="mark">
                                        </th>
                                        <{foreach item=class from=$list_class}>
                                            <th class="mark">
                                                <{$class.name|escape}>
                                            </th>
                                        <{/foreach}>
                                    </tr>
                                </thead>
                                <tbody>
                                    <{foreach item=st from=$list_st}>
                                        <tr>
                                            <td colspan="2" class="ldr-td-name mark">
                                                <strong><{$st.st_nm|escape}></strong>
                                            </td>
                                            <{foreach item=class from=$list_class}>
                                                <td id="<{$class.key|escape}>_<{$st.st_id|escape}>" class="js-target ldr-td-text <{$class.key|escape}> st_<{$st.st_id|escape}>" style="vertical-align: top !important">
                                                    <{foreach item=row from=$list[$class.key][$st.st_id]}>
                                                        <div class="drag" data="<{$row.emp_id|escape}>">
                                                            <span id="s_<{$row.emp_id|escape}>" class="label label-ldr2-lv<{$row.level2|escape}> label-ldr1-lv<{$row.level1|escape}>" style="font-weight:normal;">
                                                                <{$row.name|escape}>
                                                            </span>
                                                            <{foreach item=ldr from=$ladder->lists()}>
                                                                <{assign var="level" value="level`$ldr.ladder_id`"}>
                                                                <{assign var="lvn" value=$row.$level}>
                                                                <span class="lv ldr-roman badge badge-ldr<{$ldr.ladder_id|escape}>-lv<{$lvn|escape}> ldr<{$ldr.ladder_id|escape}>-lv<{$lvn|escape}> <{if !$lvn}>hide<{/if}>" style="font-size:8px; font-weight:normal;" data-ldr="<{$ldr.ladder_id|escape}>" data-lv="<{$lvn|escape}>">
                                                                    <{$ldr.short}><{$roman_num[$lvn]}>
                                                                </span>
                                                            <{/foreach}>
                                                        </div>
                                                    <{/foreach}>
                                                </td>
                                            <{/foreach}>
                                        </tr>
                                    <{/foreach}>
                                    <{foreach item=ldr from=$ladder->lists()}>
                                        <{assign var="ldrn" value="ldr`$ldr.ladder_id`"}>
                                        <{foreach name=lvcount item=lv from=$ldr.config.level|@array_reverse}>
                                            <{assign var="lvn" value="lv`$lv`"}>
                                            <tr style="background-color:#FFFAFA;">
                                                <{if $smarty.foreach.lvcount.first}>
                                                    <{assign var='count' value=$ldr.config.level|@count}>
                                                    <td class="mark ldr-td-name" rowspan="<{$count+1}>">
                                                        <strong><{$ldr.title|escape}><br>合計</strong>
                                                    </td>
                                                <{/if}>
                                                <{foreach name=class item=class from=$list_class}>
                                                    <{if $smarty.foreach.class.first}>
                                                        <td class="ldr-td-level">
                                                            <{$roman_num[$lv]|default:$ldr.config.level0|default:'0'|escape}>
                                                        </td>
                                                    <{/if}>
                                                    <td class="mark ldr-td-number" id="<{$class.key|escape}>_<{$ldrn}>_<{$lvn}>">
                                                        <{$class.cnt.$ldrn.$lvn|default:0|escape}>
                                                    </td>
                                                <{/foreach}>
                                            </tr>
                                        <{/foreach}>
                                        <tr style="background-color:#FFFAFA;">
                                            <{foreach name=class item=class from=$list_class}>
                                                <{if $smarty.foreach.class.first}>
                                                    <td class="ldr-td-level">無し</td>
                                                <{/if}>
                                                <td class="mark ldr-td-number" id="<{$class.key|escape}>_<{$ldrn}>_lv">
                                                    <{$class.cnt.$ldrn.lv|default:0|escape}>
                                                </td>
                                            <{/foreach}>
                                        </tr>
                                    <{/foreach}>
                                    <tr class="info">
                                        <td colspan="2" class="mark ldr-td-name">
                                            <strong>合計</strong>
                                        </td>
                                        <{foreach item=class from=$list_class}>
                                            <td class="mark ldr-td-name">
                                                <strong><span id="<{$class.key|escape}>_sum"><{$class.cnt.sum}></span> 名</strong>
                                            </td>
                                        <{/foreach}>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                        <table>
                            <tr>
                                <td class="mark">
                                    <input type="text" id="new_name" name="new_name" class="input-large js_new" maxlength="40" value="" placeholder="職員名を入力してください"/>
                                    <br/>
                                    <{foreach item=ldr from=$ladder->lists()}>
                                        <select name="level<{$ldr.ladder_id|escape}>" class="input-small js_new new_level" data-ldr="<{$ldr.ladder_id|escape}>" data-short="<{$ldr.short|escape}>">
                                            <option value="">無し</option>
                                            <{foreach item=lv from=$ldr.config.level}>
                                                <option value="<{$lv}>"><{$roman_num[$lv]|default:$ldr.config.level0|escape}></option>
                                            <{/foreach}>
                                        </select>
                                    <{/foreach}>
                                </td>
                                <td id="new_area" class="ldr-td-name mark" style="width: 100px; background-color: #C3C3C3;">
                                    <div id="st_new" class="drag clone" data="i_new">
                                        <span id="new_nm" class="label">新しい職員</span>
                                    </div>
                                </td>
                                <td id="work_erea" class="ldr-td-name" style="border: 1px; width: 120px; height: 50px; background-color: #AAffAA;">
                                    <strong>退避エリア<br><small>(保存すると削除されます)</small></strong>
                                </td>
                                <td class="trash ldr-td-name" style="font-size: 20px; border: 1px; width: 120px; height: 50px; background-color: orange;">
                                    <i class="icon-trash"></i>
                                </td>
                                <td class="mark ldr-td-name" style="width: 50px;"></td>
                            </tr>
                        </table>
                        <div class="control-group">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <{include file="__modal_form.tpl" modal_id="mdl_pattern" modal_title="配置パターン"}>
        <{include file="__js.tpl"}>
        <{include file="__modal_form_js.tpl"}>
        <{javascript src="js/redips-drag-5.0.8-min.js"}>
        <script type="text/javascript">
            $(function() {
                //REDIPS.drag.init()
                ptn_status();
                rd_init();

                $("#overlay").fadeOut();

                /**
                 * 配置パターンプルダウンのonChange
                 */
                $("#pattern_id").change(function() {
                    $("#form_placement").submit();
                });

                /**
                 * 新しい職員の更新
                 */
                $('.js_new').change(function(e) {
                    // 名前
                    var new_name = "新しい職員";
                    if ($('#new_name').val()) {
                        new_name = $('#new_name').val();
                    }

                    // レベル
                    $("#new_nm").removeClass().addClass('label').nextAll().remove();
                    $('.new_level').each(function() {
                        if ($(this).val() === '0' || $(this).val() === '') {
                            return;
                        }
                        $("#new_nm").addClass('label-ldr' + $(this).data('ldr') + '-lv' + $(this).val());

                        switch ($(this).val()) {
                            case "1":
                                roman_num = 'I';
                                break;
                            case "2":
                                roman_num = 'II';
                                break;
                            case "3":
                                roman_num = 'III';
                                break;
                            case "4":
                                roman_num = 'IV';
                                break;
                            case "5":
                                roman_num = 'V';
                                break;
                            case "6":
                                roman_num = 'VI';
                                break;
                            default :
                                roman_num = '';
                                break;
                        }

                        var badge = [
                            ' <span class="lv ldr-roman badge badge-ldr',
                            $(this).data('ldr'),
                            '-lv',
                            $(this).val(),
                            ' ldr',
                            $(this).data('ldr'),
                            '-lv',
                            $(this).val(),
                            '" style="font-size:8px; font-weight:normal;" data-ldr="',
                            $(this).data('ldr'),
                            '" data-lv="',
                            $(this).val(),
                            '">',
                            $(this).data('short'),
                            roman_num,
                            '</span>'
                        ].join('');

                        // badgeがあれば、その末尾に付ける
                        if ($("#new_nm").parent().children('.badge')) {
                            $("#new_nm").parent().children().filter(':last').after(badge);
                        }
                        else {
                            $("#new_nm").after(badge);
                        }

                    });
                    $("#new_nm").text(new_name);
                });

                $("#new_area").hover(
                    function() {
                        $("#new_name").trigger("blur");
                    },
                    function() {
                    }
                );
            });
            /**
             * ボタンの制御
             */
            function ptn_status() {
                if ($("#pattern_id").val() !== "") {
                    $(".js-ptn").prop("disabled", false);
                } else {
                    $(".js-ptn").prop("disabled", true);
                }
            }

            /**
             * 集計
             * @param {type} lv
             * @returns {undefined}
             */
            function calc_l(obj) {
                if (!obj) {
                    return;
                }

                var lvs = $(rd.obj).find('span.lv').each(function() {
                    var ldr = $(this).data('ldr');
                    var lv = $(this).data('lv');
            <{foreach item=class from=$list_class}>
                    $("#<{$class.key}>_ldr" + ldr + "_lv" + lv).html($("td.<{$class.key}>").find("span.ldr" + ldr + "-lv" + lv).size());
                    $("#<{$class.key}>_sum").html($("td.<{$class.key}>").find("span.label").size());
            <{/foreach}>
                });
            }

            /**
             * REDIPSの初期化
             * @returns {rd_init}
             */
            function rd_init() {
                redipsInit = function() {
                    rd = REDIPS.drag;
                    rd.init();
                    $("#overlay").fadeOut();

                    /**
                     * DROPイベント
                     * @returns {undefined}
                     */
                    rd.event.dropped = function() {
                        var e_id = rd.obj.getAttribute("data");
                        $(".js-form-print").prop("disabled", true);
                        var txt = $.trim($("#s_" + e_id).text());
                        if (0 !== txt.indexOf('★')) {
                            $("#s_" + e_id).text('★ ' + txt);
                        }
                        calc_l(rd.obj);
                    };
                    rd.event.dblClicked = function() {
                        var e_id = rd.obj.getAttribute("data");
                        $(".js-form-print").prop("disabled", true);
                        var txt = $.trim($("#s_" + e_id).text());
                        if (0 === txt.indexOf('★')) {
                            $("#s_" + e_id).text(txt.substr(1));
                        } else {
                            $("#s_" + e_id).text('★ ' + txt);
                        }
                        calc_l(rd.obj);
                    };
                    /**
                     * クローンの作製
                     * @returns {undefined}
                     */
                    rd.event.cloned = function() {
                        var tmp = String(jQuery.now());
                        var new_id = 'PL' + tmp.substring(3, 13);
                        var nodes = rd.obj.childNodes;
                        var num = nodes.length;
                        for (var i = 0; i < num; i++) {
                            if (nodes[i].id === 'new_nm') {
                                nodes[i].id = "s_" + new_id;
                            }
                        }
                        rd.obj.setAttribute("data", new_id);
                    };
                };
                // add onload event listener
                if (window.addEventListener) {
                    window.addEventListener('load', redipsInit, false);
                }
                else if (window.attachEvent) {
                    window.attachEvent('onload', redipsInit);
                }
            }

            /**
             * サブミットイベント
             * @returns {undefined}
             */
            function onsub() {
                $("#overlay").show();
                if ($("#form_placement").find("#update_flg").val() === "1" || $("#form_placement").find("input[name=mode2]").val() === "save") {
                    if ($("#work_erea").find('div').size() > 0) {

                    }
                    makedata();
                }
                return true;
            }
            function save() {
                $("#update_flg").val("1");
            }

            /**
             * データの作成
             * 普通にhiddenタグを使うとie8で極端に遅くなるのでsubmit時に作成する
             * @returns {undefined}
             */
            function makedata() {
                $('.js-target').each(function() {
                    td_id = $(this).attr("id");
                    $("#" + td_id).find('div').each(function() {
                        var level = "";
                        $(this).find('span.lv').each(function() {
                            lv = $.isNumeric($(this).data('lv')) ? $(this).data('lv') : '0';
                            level = lv + level;
                        });
                        var data = td_id + "_" + $(this).attr("data") + "_" + level + "_" + $("#s_" + $(this).attr("data")).text();
                        $("#form_placement")
                            .append($('<input></input>').attr({type: 'hidden', name: 'placement[]', value: data}));
                    });
                });
            }
        </script>
    </body>
</html>