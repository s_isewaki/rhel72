<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | <{$hd_title}></title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl"  hd_sub_title=''}>

            <div class="container-fluid">
                <div class="row-fluid">
                    <form class="form" method="POST">
                        <input type='hidden' id='commit_flg' name='commit_flg' value='' />
                        <input type='hidden' id='outside_id' name='outside_id' value='' />
                        <input type='hidden' id='training_id' name='training_id' value='' />
                        <input type='hidden' id='mst_read' name='mst_read' value='' />
                        <input type='hidden' id='id' name='id' value="<{$smarty.post.id}>" />

                        <div class="well well-small">

                            <div class="control-group">
                                <label class="control-label" for="rd_organizer">
                                    <strong>主催</strong>
                                    <{if !$readonly}>
                                        <span class="badge badge-important required">必須</span>
                                    <{/if}>
                                </label>
                                <div class="controls form-inline">
                                    <{if $readonly}>
                                        <span class="ldr-uneditable input-large"><{$data.organizer|escape|nl2br}></span>
                                        <input type="hidden" id="organizer" name="organizer" />
                                        <input type="hidden" id="organizer" name="rd_organizer" value="3"/>
                                    <{else}>
                                        <label class="radio inline"><input type="radio" name="rd_organizer" value="1" />日本看護協会</label>
                                        <label class="radio inline"><input type="radio" name="rd_organizer" value="2" /><{$organizer_def}></label>
                                        <label class="radio inline"><input type="radio" name="rd_organizer" value="3" />その他</label>
                                        &nbsp;<input type="text" id="organizer" name="organizer" class="inline input-large" maxlength="50"/>
                                    <{/if}>
                                </div>
                                <{foreach from=$error.organizer item=msg}>
                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                <{/foreach}>
                            </div>
                            <{if $type === 'mst'}>
                                <input type='hidden' name='type' value='0' />
                                <input type='hidden' name='job_year' value='0' />
                            <{else}>
                                <div class="control-group">
                                    <label class="control-label" for="type">
                                        <strong>区分</strong>
                                        <span class="badge badge-important required">必須</span>
                                    </label>
                                    <div class="controls form-inline">
                                        <label class="radio inline"><input type="radio" name="type" value="1" />日程出張</label>
                                        <label class="radio inline"><input type="radio" name="type" value="2" />出張</label>
                                        <label class="radio inline"><input type="radio" name="type" value="3" />その他</label>
                                    </div>
                                    <{foreach from=$error.type item=msg}>
                                        <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                    <{/foreach}>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="job_year">
                                        <strong>経験年数</strong>
                                        <span class="badge badge-important required">必須</span>
                                    </label>
                                    <div class="controls form-inline">
                                        <span class="input-append">
                                            <input type="text" id="job_year" name="job_year" class="input-mini onlynum text-right" maxlength="2" />
                                            <span class="add-on">年</span>
                                        </span>
                                    </div>
                                    <{foreach from=$error.job_year item=msg}>
                                        <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                    <{/foreach}>
                                </div>
                            <{/if}>
                            <div class="control-group">
                                <label class="control-label" for="start_date">
                                    <strong>開催日</strong>
                                    <{if !$readonly}>
                                        <span class="badge badge-important required">必須</span>
                                    <{/if}>
                                </label>
                                <{if $readonly}>
                                    <span class="ldr-uneditable"><{$data.start_date|date_format_jp:"%Y年%-m月%-d日(%a)"}></span>
                                    <{if $data.start_date <> $data.last_date}>
                                        〜
                                        <span class="ldr-uneditable"><{$data.last_date|date_format_jp:"%Y年%-m月%-d日(%a)"}></span>
                                    <{/if}>
                                    <input type="hidden" id="start_date" name="start_date"/>
                                    <input type="hidden" id="last_date" name="last_date"/>
                                <{else}>
                                    <div class="controls form-inline">
                                        <{include file="__datepicker.tpl" date_id="start_date" date_name="start_date"}>
                                        〜
                                        <{include file="__datepicker.tpl" date_id="last_date" date_name="last_date"}>
                                    </div>
                                    <{foreach from=$error.date item=msg}>
                                        <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                    <{/foreach}>
                                <{/if}>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="pref"><strong>開催地</strong></label>
                                <div class="controls">
                                    <{if $readonly}>
                                        <span class="ldr-uneditable input-xlarge"><{$data.pref|escape|nl2br}></span>
                                        <input type="hidden" id="pref" name="pref"/>
                                    <{else}>
                                        <input type="text" id="pref" name="pref" class="input-xlarge" maxlength="100"/>
                                    <{/if}>
                                </div>
                                <{foreach from=$error.pref item=msg}>
                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                <{/foreach}>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="place"><strong>開催場所</strong></label>
                                <div class="controls">
                                    <{if $readonly}>
                                        <span class="ldr-uneditable input-xlarge"><{$data.place|escape|nl2br}></span>
                                        <input type="hidden" id="place" name="place" />
                                    <{else}>
                                        <input type="text" id="place" name="place" class="input-xlarge" maxlength="100"/>
                                    <{/if}>
                                </div>
                                <{foreach from=$error.pref item=msg}>
                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                <{/foreach}>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="no">
                                    <strong>研修番号</strong>
                                    <{if !$readonly}>
                                        <span class="badge badge-important required">必須</span>
                                    <{/if}>
                                </label>
                                <div class="controls">
                                    <{if $readonly}>
                                        <span class="input-prepend">
                                            <span class="add-on">No.</span>
                                            <span class="ldr-uneditable input-small"><{$data.no|escape|nl2br}></span>
                                        </span>
                                        </span>
                                        <input type="hidden" id="no" name="no"/>
                                    <{else}>
                                        <span class="input-prepend">
                                            <span class="add-on">No.</span>
                                            <input type="text" id="no" name="no" class="input-small" maxlength="20"/>
                                        </span>
                                    <{/if}>
                                </div>
                                <{foreach from=$error.no item=msg}>
                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                <{/foreach}>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="contents">
                                    <strong>研修名</strong>
                                    <{if !$readonly}>
                                        <span class="badge badge-important required">必須</span>
                                    <{/if}>
                                </label>
                                <div class="controls">
                                    <{if $readonly}>
                                        <span class="ldr-uneditable input-xxlarge"><{$data.contents|escape|nl2br}></span>
                                        <input type="hidden" id="contents" name="contents" />
                                    <{else}>
                                        <input type="text" id="contents" name="contents" class="input-xxlarge"  maxlength="200"/>
                                    <{/if}>
                                </div>
                                <{foreach from=$error.contents item=msg}>
                                    <p class="text-error"><i class="icon-warning-sign"></i> <{$msg|escape}></p>
                                <{/foreach}>
                            </div>

                            <{if $type !== 'mst'}>
                                <div class="control-group">
                                    <label class="control-label"><strong>承認者</strong></label>
                                    <{if isset($approver)}>
                                        <div class="controls"><span class="ldr-uneditable input-large"><{$approver->emp_name()|escape}></span></div>
                                        <{else}>
                                        <div class="alert alert-error">承認者が設定されていません。管理者にお問い合わせください。</div>
                                    <{/if}>
                                </div>
                            <{/if}>
                            <{if $type === 'mst'}>
                                <div class="btn-toolbar">
                                    <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                                    <{if $edit === "new"}>
                                        <button type="submit" name="btn_save" id="btn_save" class="btn btn-small btn-primary" value="save"><i class="icon-save"></i> 登録する</button>
                                    <{elseif $edit === 'edit'}>
                                        <button type="submit" name="btn_save" id="btn_save" value="save" class="btn btn-small btn-primary"><i class="icon-save"></i> 変更する</button>
                                        <button type="submit" name="btn_save" value="copy" class="btn btn-small btn-danger"><i class="icon-copy"></i> 新しい講座を登録する</button>
                                        <{if $count === '0'}>
                                            <button type="submit" name="btn_save" value="remove" class="btn btn-small btn-danger js-remove"><i class="icon-trash"></i> 削除する</button>
                                        <{/if}>
                                    <{/if}>
                                </div>
                            <{else}>
                                <div class="btn-toolbar">
                                    <button type="button" name="btn_back" id="btn_back" class="btn_back btn btn-small"><i class="icon-circle-arrow-left"></i> 戻る</button>
                                    <{if $edit === 'apply' or $edit === "new"}>
                                        <{if isset($approver)}>
                                            <button type="submit" name="btn_save" id="btn_save" class="js-submit-apply btn btn-small btn-primary" value="save"><i class="icon-save"></i> 申請する</button>
                                        <{/if}>
                                    <{elseif $edit === 'edit'}>
                                        <{if isset($approver)}>
                                            <button type="submit" name="btn_save" id="btn_save" class="js-submit-modify btn btn-small btn-primary" value="save"><i class="icon-save"></i> 修正する</button>
                                        <{/if}>
                                        <button type="submit" name="btn_save" id="btn_save" class="js-submit-withdraw btn btn-small btn-danger" value="cancel"><i class="icon-remove-sign"></i> 取り下げる</button>
                                    <{/if}>
                                </div>
                            <{/if}>

                        </div>
                    </form>
                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
        <script type="text/javascript">
            $(function() {
                $("input[name=rd_organizer]").change(function() {
                    organizer_status();
                });

                organizer_status();
            });
            function organizer_status() {
            <{if !$readonly}>
                if ($("input[name=rd_organizer]:checked").val() === '3') {
                    $('#organizer').prop("disabled", false);
                } else {
                    $('#organizer').prop("disabled", true);
                }
            <{/if}>
            }
            $('#start_date').parent(".datepicker").on('changeDate', function(e) {
                if (!$('#last_date').val()) {
                    $('#last_date').val($('#start_date').val());
                    $('#last_date').change();
                }
            });

        </script>
    </body>
</html>
