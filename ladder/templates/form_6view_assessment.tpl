<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>

        <{include file="__meta.tpl"}>
        <{include file="__css.tpl"}>

        <title>CoMedix キャリア開発ラダー | <{$ldr.config.form_6.title|escape}></title>
    </head>

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <body>
        <!--<![endif]-->
        <{include file="__navbar.tpl"}>
        <{include file="__sidebar.tpl"}>

        <!-- メインエリア -->
        <div class="content">
            <{include file="__header.tpl" hd_title=$ldr.config.form_6.title hd_sub_title=$ldr.config.form_6.detailL}>

            <div class="container-fluid">
                <{*印刷ボタン*}>
                <form id="form_print" method="post" class="pull-right">
                    <input type="hidden" name="mode" value="" />
                    <input type="hidden" name="menu" value="" />
                    <input type="hidden" name="id" value="" />
                    <input type="hidden" name="target_emp_id" value=""/>
                    <input type="hidden" name="owner_url" value="" />
                    <{if $smarty.post.target_emp_id === $emp->emp_id()}>
                        &nbsp;<button type="button" data-id="<{$smarty.post.id}>" data-emp="<{$smarty.post.target_emp_id}>" data-php="form_6edit.php" data-mode="edit" class="js-form-button btn btn-danger btn-small"><i class="icon-edit"></i> 編集する</button>
                    <{/if}>
                    &nbsp;<button type="button" data-id="<{$smarty.post.id}>" data-emp="<{$smarty.post.target_emp_id}>" data-php="form_6view.php" data-target="form6" class="js-form-print btn btn-primary btn-small"><i class="icon-print"></i> 印刷する</button>
                </form>

                <form method="post">
                    <{include file="__form_view.tpl" op_srh='false' tmp_id="form_6view"}>
                </form>

                <div class="well well-small">

                    <div class="control-group">
                        <div class="input-prepend" style="margin-bottom:0;">
                            <span class="add-on">申請レベル</span>
                            <span class="ldr-uneditable input-mini ldr-level" style="margin-bottom:0;"><{$apply->level_text()|escape}></span>
                        </div>
                        <div class="input-prepend" style="margin-bottom:0;">
                            <span class="add-on">申請者</span>
                            <span class="ldr-uneditable input-large" style="margin-bottom:0;"><{$target_emp->emp_name()|escape}> (<{$target_emp->class_full_name()|escape}>)</span>
                        </div>
                    </div>
                    <hr/>

                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#report" data-toggle="tab"><i class="icon-file-text-alt"></i> レポート</a></li>
                        <li><a href="#assessment" data-toggle="tab"><i class="icon-check"></i> 評価</a></li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active" id="report">
                            <div class="control-group">
                                <label class="control-label">
                                    <strong>評価日</strong>
                                </label>
                                <div class="controls">
                                    <span class="ldr-uneditable input-medium"><{$form6.updated_on|date_format_jp:"%Y年%-m月%-d日(%a)"}></span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    <strong>事例内容</strong>
                                </label>
                                <div class="controls">
                                    <span class="input-maxlarge ldr-uneditable"><{$form6.narrative|escape|nl2br}></span>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="assessment">

                            <div class="control-group">
                                <div class="input-prepend" style="margin-bottom:0;">
                                    <span class="add-on">評価者</span>
                                    <span class="ldr-uneditable input-large" style="margin-bottom:0;"><{$assessment.emp_name|escape}></span>
                                </div>
                                <div class="input-prepend" style="margin-bottom:0;">
                                    <span class="add-on">評価日</span>
                                    <span class="ldr-uneditable input-medium" style="margin-bottom:0;"><{$assessment.updated_on|date_format_jp:"%Y年%-m月%-d日(%a)"}></span>
                                </div>
                            </div>

                            <table class="table table-condensed table-striped table-hover table-bordered ldr-table">
                                <thead>
                                    <tr>
                                        <th>項目</th>
                                        <th>内容</th>
                                        <th>評価</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <{foreach item=asm from=$assessment.list}>
                                        <{assign var='asmkey' value="_`$asm.apply_assessment`"}>
                                        <tr class="<{$ldr.config.report_select[$asmkey].color|default:'default'}>">
                                            <{if $asm.guideline_group2 !== $back_g2}>
                                                <td class="ldr-td-text-left" rowspan="<{$asm.group2_count|escape}>"><{$asm.group2_name|escape}></td>
                                                <{assign var="back_g2" value=`$asm.guideline_group2`}>
                                            <{/if}>
                                            <td class="ldr-td-text-left"><{$asm.guideline|escape}></td>
                                            <td class="ldr-td-name"><{$ldr.config.report_select[$asmkey].button|default:'<i class="icon-question"></i>'}></td>
                                        </tr>
                                    <{/foreach}>
                                </tbody>
                            </table>

                            <hr style="margin:8px 0;"/>
                            <div class="control-group">
                                <label>
                                    <strong>評価者からのメッセージ</strong>
                                </label>
                                <div class="controls">
                                    <span class="input-maxlarge ldr-uneditable"><{$assessment.comment|escape|nl2br}></span>
                                </div>
                            </div>

                            <div class="control-group">
                                <label>
                                    <strong>教育課からのメッセージ</strong>
                                </label>
                                <div class="controls">
                                    <span class="input-maxlarge ldr-uneditable"><{$assessment.admin_comment|escape|nl2br}></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <{include file="__js.tpl"}>
    </body>
</html>