<?php

require_once 'class/Base.php';
require_once 'class/Relief.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('accept_relief');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_relief($emp);

//----------------------------------------------------------
// object
//----------------------------------------------------------
$relief = new Ldr_Relief($emp->emp_id());

//----------------------------------------------------------
// データの取得
//----------------------------------------------------------
if (!isset($_POST['id'])) {
    header('Location: dashboard.php');
    exit;
}

$id = explode('_', $_POST['id']);
$_POST['relief_id'] = $id[0];
$_POST['unit'] = $id[1];
$_POST['training_day'] = $id[2];
$cond = array_merge((array) $_POST, array('list_type' => 'report'));
$data = array(
    'sb' => $relief->find('subject', $_POST['relief_id']),
    'ct' => $relief->find('contents', $_POST['relief_id'], $_POST['unit']),
    'list' => $relief->lists('accept_emp', null, null, null, $cond),
);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $emp);
$view->assign($data);
$view->assign(Ldr_Util::get_common_info('taccept_relief'));
$view->display('trn_accept_relief_report.tpl');
