<?php

require_once 'class/Base.php';
require_once 'class/Pager.php';
require_once 'class/Inside.php';
require_once 'class/InsideMst.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('inside_approve');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_approver($emp);

//----------------------------------------------------------
// object
//----------------------------------------------------------
$inside = new Ldr_Inside($emp->emp_id());
$conf = new Ldr_Config();
$effective_post = Ldr_Util::use_tiket();
$perPage = Ldr_Const::SRH_PERPAGE;

//----------------------------------------------------------
// 保存
//----------------------------------------------------------
if ($effective_post && $_POST['btn_save'] === "save") {
    try {
        $inside->save_approve($_POST);
        $completed = 'save';
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }
}
else if ($effective_post && $_POST['btn_save'] === "cancel") {
    try {
        $inside->cancel_approve($_POST);
        $completed = 'cancel';
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }
}

//----------------------------------------------------------
// 遷移状態判別
//----------------------------------------------------------
// ページャーから移動
if (Ldr_Pager::is_pager($_POST['move_pager']) || !empty($completed)) {
    //検索条件を取得
    $_SESSION['page_no'] = Ldr_Pager::get_page_no($_POST['page_no']);
    $srh_condition = $_SESSION['srh_condition'];
}
// 検索
else if ($_POST['search'] || $_POST['btn_save'] === "save" || $_POST['btn_save'] === "cancel") {
    $srh_condition['srh_year'] = $_POST['srh_year'];
    $srh_condition["srh_nonapprove"] = $_POST['srh_nonapprove'];
    $srh_condition["srh_start_date"] = $_POST['srh_start_date'];
    $srh_condition["srh_last_date"] = $_POST['srh_last_date'];
    $srh_condition["srh_keyword"] = $_POST['srh_keyword'];

    $_POST["page_no"] = 1;

    // 検索条件を設定
    $_SESSION['srh_condition'] = $srh_condition;
}
// 編集画面から戻ってきた場合
else if (isset($_POST['ldr_return']) || isset($_SESSION['ldr_return'])) {
    // 検索条件の復旧
    $srh_condition = $_SESSION['srh_condition'];


    //   承認/取消があった場合はリストから消えるのでページを戻す
    $_POST["page_no"] = (isset($_SESSION['ldr_return']) && !empty($_POST['srh_nonapprove'])) ? 1 : $_SESSION['page_no'];
}
// 外部から移動してきた場合
else {

    // セッション情報の初期化
    $_SESSION = array();
    $_POST["page_no"] = 1;

    $_SESSION['page_no'] = 1;
    $_SESSION['srh_condition'] = null;
    $srh_condition = null;

    if (isset($_REQUEST['status'])) {
        // ダッシュボードから移動
        $srh_condition['srh_nonapprove'] = '1';
    }

    // 検索日の初期値を設定
    $srh_condition["srh_start_date"] = date("Y-m-d");
    $srh_condition["srh_year"] = Ldr_Util::th_year();

    $_SESSION['srh_condition'] = $srh_condition;
}
$_SESSION['ldr_return'] = null;
$_SESSION['menu'] = 'tapprove_inside';
$fdat = array_merge((array) $_POST, (array) $srh_condition);

//----------------------------------------------------------
// データ
//----------------------------------------------------------
$dataCount = $inside->count_approve($srh_condition);
$current = Ldr_Pager::get_page_no($_POST['page_no'], $dataCount, $perPage);
$start = Ldr_Pager::calc_page_index($current, $perPage);
$list = $inside->lists_approve($start, $perPage, $srh_condition);

$data = array(
    "list" => $list,
    "completed" => $completed,
    'year' => $_POST['srh_year'],
    'year_list' => $inside->year_lists(),
);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $emp);
$view->assign($data);
$view->assign(Ldr_Pager::get_pager_info($_POST['page_no'], $dataCount, $perPage));
$view->assign(Ldr_Util::get_common_info($_SESSION['menu']));
$view->assign('ojt_name', $conf->ojt_name());
$view->display_with_fill('trn_approve_inside.tpl', array('fdat' => $fdat));
