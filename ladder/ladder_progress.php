<?php

require_once 'class/Base.php';
require_once 'class/Pager.php';
require_once 'class/Apply.php';
require_once 'class/Ladder.php';

$perPage = Ldr_Const::SRH_PERPAGE;

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('ldr_common');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_committee($emp);

//----------------------------------------------------------
// Object
//----------------------------------------------------------
$apply = new Ldr_Apply(array('accept_emp_id' => $session->emp()->emp_id()));
$ladder = new Ldr_Ladder();

//----------------------------------------------------------
// 初期値
//----------------------------------------------------------
$_POST['menu'] = "accept";
$_POST['owner_url'] = "ladder_progress.php";

//----------------------------------------------------------
// ソート
//----------------------------------------------------------
$desc = $_REQUEST['desc'] ? 'DESC' : 'ASC';
switch ($_REQUEST['order']) {
    case 'updated_on':
        $order_by[] = "ap.updated_on $desc";
        break;

    case 'apply_date':
        $order_by[] = "f1.apply_date $desc";
        break;

    case 'level':
        $order_by[] = "ap.level $desc";
        break;

    default:
        $desc = 'DESC';
        $order_by[] = "ap.updated_on desc";
        break;
}

//----------------------------------------------------------
// ページ遷移
//----------------------------------------------------------

// 戻る/コメント記入後のリロード
if (isset($_POST['ldr_return']) || isset($_SESSION['ldr_return']) || isset($_SESSION['commnet_reload'])) {
    unset($_SESSION['commnet_reload']);
    $_POST["page_no"] = $_SESSION['page_no'];
    $srh_condition = $_SESSION['srh_condition'];

    $_POST['srh_start_date'] = $srh_condition["srh_start_date"];
    $_POST['srh_last_date'] = $srh_condition["srh_last_date"];
    $_POST['srh_level'] = $srh_condition["srh_level"];
    $_POST['srh_status'] = $srh_condition["srh_status"];
    $_POST['srh_name'] = $srh_condition["srh_name"];
}

// ページャーから移動
else if (Ldr_Pager::is_pager($_POST['move_pager'])) {
    //検索条件を取得
    $_SESSION['page_no'] = Ldr_Pager::get_page_no($_POST['page_no']);
    $srh_condition = $_SESSION['srh_condition'];
}

// 検索ボタン
else if ($_POST['btn_search']) {
    $srh_condition["srh_start_date"] = $_POST['srh_start_date'];
    $srh_condition["srh_last_date"] = $_POST['srh_last_date'];
    $srh_condition["srh_level"] = $_POST['srh_level'];
    $srh_condition["srh_status"] = $_POST['srh_status'];
    $srh_condition["srh_name"] = $_POST['srh_name'];
    $srh_condition["order_by"] = $order_by;

    $_POST["page_no"] = 1;

    // 検索条件を設定
    $_SESSION['srh_condition'] = $srh_condition;
    // 入力中の項目を退避
    $_SESSION['srh_start_date'] = $_POST['srh_start_date'];
    $_SESSION['srh_last_date'] = $_POST['srh_last_date'];
    $_SESSION['srh_level'] = $_POST['srh_level'];
    $_SESSION['srh_status'] = $_POST['srh_status'];
    $_SESSION['srh_name'] = $_POST['srh_name'];
}

// ソート
else if ($_REQUEST['order']) {
    $srh_condition = $_SESSION['srh_condition'];
    $srh_condition["order_by"] = $order_by;
}

// それ以外
else {
    // セッション情報の初期化
    $_SESSION = array();

    $_POST["page_no"] = 1;
    $_SESSION['page_no'] = 1;
    $_SESSION['srh_condition'] = null;
    $srh_condition = null;

    // ソート
    $srh_condition["order_by"] = $order_by;
}

//----------------------------------------------------------
// 一覧
//----------------------------------------------------------
$_SESSION['ldr_return'] = null;
$fdat = array_merge((array)$_POST, (array)$srh_condition);

//----------------------------------------------------------
// データ数の取得
//----------------------------------------------------------
$dataCount = $apply->count_apply_all($srh_condition);
$current = Ldr_Pager::get_page_no($_POST['page_no'], $dataCount, $perPage);
$start = Ldr_Pager::calc_page_index($current, $perPage);

//----------------------------------------------------------
// 一覧の取得
//----------------------------------------------------------
$list = $apply->lists_apply_all($start, $perPage, $srh_condition);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $emp);
$view->assign(Ldr_Util::get_common_info('ladder_progress'));
$view->assign(Ldr_Pager::get_pager_info($_POST['page_no'], $dataCount, $perPage));
$view->assign('list', $list);
$view->assign('status_lists', $ladder->status_lists());
$view->assign('level0', $ladder->level0_texts());
$view->assign('order', $_REQUEST['order']);
$view->assign('desc', $_REQUEST['desc']);
$view->display_with_fill('ladder_progress.tpl', array('fdat' => $fdat));
