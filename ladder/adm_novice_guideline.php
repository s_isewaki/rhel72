<?php

require_once 'class/Base.php';
require_once 'class/NoviceGuideline.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_admin($session->emp());

//----------------------------------------------------------
// オブジェクト
//----------------------------------------------------------
$gl = new Ldr_NoviceGuideline();

//----------------------------------------------------------
// 処理
//----------------------------------------------------------
switch ($_POST['mode2']) {
    // バリデーション
    case 'validate':
        $error = $gl->validate($_POST['mode'], Ldr_Util::from_utf8($_POST));
        header('X-Content-Type-Options: nosniff');
        print cmx_json_encode(array(
            'error' => !empty($error),
            'msg' => $error,
            'mode' => $_POST['mode'],
        ));
        exit;

    // 保存
    case 'save':
        $error = $gl->validate($_POST['mode'], $_POST);

        // エラーが無ければ保存
        if (empty($error)) {
            // 保存処理
            try {
                $gl->save($_POST['mode'], $_POST);
            }
            catch (Exception $e) {
                cmx_log($e->getMessage());
                js_error_exit();
            }
        }
        else {
            js_error_exit();
        }

        // 行動指標
        if ($_POST['mode'] === 'guideline') {
            $_COOKIE['pulldown_revision'] = $_POST['revision_id'];
            $_COOKIE['pulldown_group1'] = $_POST['group1'];
            $_COOKIE['pulldown_group2'] = $_POST['group2'];
        }

        break;

    // 削除
    case 'delete':
        $gl->delete($_POST['mode'], $_POST['id']);
        break;

    // 並べ替え
    case 'sort':
        $gl->sort($_POST['mode'], $_POST);
        break;

    // 版の切り替え
    case 'current_revision':
        $gl->current_revision($_POST['current_revision']);
        break;
}

//----------------------------------------------------------
// 版
//----------------------------------------------------------
$current_revision = $gl->current_revision();
$revision_list = $gl->lists('revision');

//----------------------------------------------------------
// グループ1
//----------------------------------------------------------
if (!empty($_COOKIE['pulldown_revision'])) {
    $cond['revision_id'] = $_COOKIE['pulldown_revision'];
}
else {
    $cond['revision_id'] = $current_revision;
}
$group1_list = $gl->lists('group1', $cond);

//----------------------------------------------------------
// グループ2
//----------------------------------------------------------
if (!empty($_COOKIE['pulldown_group1'])) {
    $cond['group1'] = $_COOKIE['pulldown_group1'];
}
else {
    $cond['group1'] = $group1_list[0]['group1'];
}
$group2_list = $gl->lists('group2', $cond);

//----------------------------------------------------------
// 行動指標
//----------------------------------------------------------
if (!empty($_COOKIE['pulldown_group2'])) {
    $cond['group2'] = $_COOKIE['pulldown_group2'];
}
else {
    $cond['group2'] = $group2_list[0]['group2'];
}
$guideline_list = $gl->lists('guideline', $cond);

//----------------------------------------------------------
// active
//----------------------------------------------------------
if (!empty($_COOKIE['active'])) {
    $active = $_COOKIE['active'];
}
else if (!empty($_POST['mode'])) {
    $active = $_POST['mode'];
}
else {
    $active = 'guideline';
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $session->emp());
$view->assign(Ldr_Util::get_common_info('adm_novice_guideline'));

$view->assign(array(
    'current_revision' => $current_revision,
    'guideline_list' => $guideline_list,
    'revision_list' => $revision_list,
    'group1_list' => $group1_list,
    'group2_list' => $group2_list,
    'group1' => $cond['group1'],
));

$view->assign('active', $active);
$view->display_with_fill('adm_novice_guideline.tpl', array('fdat' => $cond));
exit;
