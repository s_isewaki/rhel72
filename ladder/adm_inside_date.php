<?php

require_once 'class/Base.php';
require_once 'class/InsideMst.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('adm_inside_date');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_mst_inside($session->emp());

//----------------------------------------------------------
// ID
//----------------------------------------------------------
$id = $_REQUEST['id'];
if (!is_numeric($id)) {
    header('Location: dashboard.php');
    exit;
}

//----------------------------------------------------------
// Obj
//----------------------------------------------------------
$mst = new Ldr_InsideMst();
$effective_post = Ldr_Util::use_tiket();
$conf = new Ldr_Config();

//----------------------------------------------------------
// 保存
//----------------------------------------------------------
if ($effective_post && $_POST['btn_save'] === 'status') {
    try {
        $mst->change_date_status($_POST['training'], $_POST['status']);
        $completed = true;
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }
}

//----------------------------------------------------------
// データの取得
//----------------------------------------------------------
$data = $mst->find_date($id);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign(Ldr_Util::get_common_info('adm_inside', array('referer_url' => 'adm_inside.php')));
$view->assign('emp', $session->emp());
$view->assign('completed', $completed);
$view->assign($data);
$view->assign('ojt_name', $conf->ojt_name());
$view->display('adm_inside_date.tpl');
