<?php

require_once 'class/Base.php';
require_once 'class/Validation.php';
require_once 'class/Apply.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// セッション初期化
//----------------------------------------------------------
Ldr_Util::session_init('ldr_common');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_ladder($emp);

//----------------------------------------------------------
// object
//----------------------------------------------------------
$apply = new Ldr_Apply(array('apply_emp_id' => $_POST["target_emp_id"], 'accept_emp_id' => $emp->emp_id(), 'apply_id' => $_POST['id']));
$target_emp = new Ldr_Employee($_POST["target_emp_id"]);
$effective_post = Ldr_Util::use_tiket();
$ladder = $apply->ladder();

//----------------------------------------------------------
// 確定
//----------------------------------------------------------
if ($_POST['btn_save'] === 'completed' || $_POST['btn_save'] === 'edit') {
    // バリデーション
    $error = $apply->validate_form8($_POST, $ladder['config']['rc']);

    // エラーが無ければ保存
    if (empty($error)) {
        try {
            if ($effective_post) {
                $apply->save_form8($_POST, $_POST['btn_save']);
            }
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
        }
        unset($_SESSION['ldr_return']); //確定の場合は1ページ目に戻す
        header('Location:' . Ldr_Util::get_referer_url());
        exit;
    }
}

//----------------------------------------------------------
// 自動保存
//----------------------------------------------------------
else if ($_POST['save'] === 'ajax') {
    try {
        $apply->save_form8($_POST, 'auto');
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }
    exit;
}

//----------------------------------------------------------
// 更新
//----------------------------------------------------------
$form8 = $apply->find_form8();
if (!empty($form8)) {
    $fdat = array_merge((array)$_POST, (array)$form8);
}
else {
    $fdat = array_merge((array)$_POST, array('appraise_date' => date("Y-m-d")));
}

//----------------------------------------------------------
// 申請情報取得
//----------------------------------------------------------
$form1 = $apply->find_form1($_POST['id']);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign(Ldr_Util::get_common_info($_POST['menu'], array('referer_url' => $_POST['owner_url'])));
$view->assign('emp', $emp);
$view->assign('target_emp', $target_emp);
$view->assign('apply', $apply);
$view->assign('ldr', $apply->ladder());
$view->assign('form1', $form1);

if (empty($error)) {
    $view->display_with_fill('form_8edit.tpl', array('fdat' => $fdat));
}
else {
    $view->assign('error', $error);
    $view->display_with_fill('form_8edit.tpl', array('fdat' => $_POST));
}
