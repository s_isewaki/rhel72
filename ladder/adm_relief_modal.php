<?php

require_once 'class/Base.php';
require_once 'class/Relief.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_mst_relief($session->emp());

//----------------------------------------------------------
// オブジェクト
//----------------------------------------------------------
$relief = new Ldr_Relief();
$view = new Cmx_View('ladder/templates');

//----------------------------------------------------------
// データ
//----------------------------------------------------------
switch ($_GET['mode']) {
    case 'relief':
        if (isset($_GET['id'])) {
            $data = $relief->find('subject', $_GET['id']);
        }
        break;
    case 'contents':
        if (isset($_GET['id'])) {
            $data = $relief->find('contents', $_GET['id'], $_GET['unit']);
            $data['id'] = $_GET['id'];
            $data['old_unit'] = $data['unit'];
        }
        break;

    default:
        js_error_exit();
        break;
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
header('Cache-Control: no-cache, must-revalidate');
header("Cache-Control: post-check=0, pre-check=0", false);
header('Pragma: no-cache');
$view->assign('relief_id', $data['id']);
$view->display_with_fill("adm_relief/modal_{$_GET['mode']}.tpl", array('fdat' => $data));
