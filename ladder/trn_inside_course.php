<?php

require_once 'class/Base.php';
require_once 'class/Pager.php';
require_once 'class/Inside.php';
require_once 'class/Guideline.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}
//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('inside_apply');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_ladder($emp);

//----------------------------------------------------------
// Obj
//----------------------------------------------------------
$inside = new Ldr_Inside($emp->emp_id());
$gl = new Ldr_Guideline();
$conf = new Ldr_Config();
$perPage = Ldr_Const::SRH_PERPAGE;

//----------------------------------------------------------
// hospital_type
//----------------------------------------------------------
$hospital_type = $conf->hospital_type();

//----------------------------------------------------------
// 遷移状態判別
//----------------------------------------------------------
// ページャーから移動
if (Ldr_Pager::is_pager($_POST['move_pager'])) {
    //検索条件を取得
    $_SESSION['page_no'] = Ldr_Pager::get_page_no($_POST['page_no']);
    $srh_condition = $_SESSION['srh_condition'];
}
// 検索ボタン
else if ($_POST['search']) {
    $srh_condition["srh_year"] = $_POST['srh_year'];
    $srh_condition["srh_level"] = $_POST['srh_level'];
    $srh_condition["srh_guideline"] = $_POST['srh_guideline'];

    $_POST["page_no"] = 1;

    // 検索条件を設定
    $_SESSION['srh_condition'] = $srh_condition;
}
//次ページから戻ってきた場合
else if (isset($_POST['ldr_return']) || isset($_SESSION['ldr_return'])) {
    // ページ番号の復旧
    $_POST["page_no"] = $_SESSION['page_no'];

    // 検索条件の復旧
    $srh_condition = $_SESSION['srh_condition'];
}
// 外部から移動してきた場合
else {
    // セッション情報の初期化
    $_SESSION = array();
    $_POST["page_no"] = 1;
    $_SESSION['page_no'] = 1;
    $_SESSION['srh_condition'] = null;
    $srh_condition = null;

    $srh_condition["srh_year"] = Ldr_Util::th_year();

    $level = $emp->level();
    if ($hospital_type === '1') {
        $srh_condition["srh_level"] = min(5, empty($level) ? '1' : $level + 1);
    }
    else {
        $srh_condition["srh_level"] = min(5, empty($level) ? '1' : $level);
    }
    $_SESSION['srh_condition'] = $srh_condition;
}
$_SESSION['ldr_return'] = null;

//----------------------------------------------------------
// list
//----------------------------------------------------------
$srh_condition['enable_number_of'] = true;
$dataCount = $inside->count_course($srh_condition);
$current = Ldr_Pager::get_page_no($_POST['page_no'], $dataCount, $perPage);
$start = Ldr_Pager::calc_page_index($current, $perPage);
$list = $inside->lists_course($start, $perPage, $srh_condition);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $emp);
$view->assign('year_list', $inside->year_lists());
$view->assign('group1', $gl->lists('group1', array('guideline_revision' => 1)));
$view->assign('list', $list);
$view->assign(Ldr_Pager::get_pager_info($_POST['page_no'], $dataCount, $perPage));
$view->assign(Ldr_Util::get_common_info('inside_course'));
$view->display_with_fill('trn_inside_course.tpl', array('fdat' => $srh_condition));
