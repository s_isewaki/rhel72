<?php

require_once 'class/Base.php';
require_once 'class/Classmst.php';
require_once 'class/AnalysisLadder.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('adm_analysis');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_admin($session->emp());

//----------------------------------------------------------
// オブジェクト
//----------------------------------------------------------
$classmst = new Ldr_Classmst();
$analysis = new Ldr_AnalysisLadder();
$effective_post = Ldr_Util::use_tiket();

//----------------------------------------------------------
// 保存
//----------------------------------------------------------
if ($effective_post && isset($_POST['btn_save'])) {
    // 分析対象部署
    if ($_POST['btn_save'] === 'class') {
        // 保存処理
        try {
            $analysis->save('analysis', $_POST);
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
        }
    }
    // 分析対象役職
    else if ($_POST['btn_save'] === 'st') {
        try {
            $analysis->save('st', $_POST);
            $completed = true;
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
        }
    }
    // 分析対象職種
    else if ($_POST['btn_save'] === 'job') {
        try {
            $analysis->save('job', $_POST);
            $completed = true;
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
        }
    }
    $completed = true;
}
$active = isset($_POST['tab']) ? $_POST['tab'] : 'class';
unset($_POST['tab']);

//----------------------------------------------------------
// data
//----------------------------------------------------------
if (!empty($_POST['pulldown_class'])) {
    $class = explode('-', $_POST['pulldown_class']);
    $class_data = array('class' => $class[0], 'atrb' => $class[1]);
}
else {
    $class_data = null;
}

$class_list = $analysis->lists('class', $class_data);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $session->emp());
$view->assign(Ldr_Util::get_common_info('adm_analysis'));
$view->assign(array(
    'class_pull_list' => $classmst->class12_lists(),
    'class_list' => $class_list,
    'st_list' => $analysis->lists('st'),
    'job_list' => $analysis->lists('job'),
    'completed' => $completed,
));
$view->assign('active', $active);
$view->display_with_fill('adm_analysis.tpl', array('fdat' => $_POST));
