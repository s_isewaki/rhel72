<?php

require_once 'class/Base.php';
require_once 'class/InsideMstCheck.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('adm_inside_sort_number');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_mst_inside($session->emp());

//----------------------------------------------------------
// Obj
//----------------------------------------------------------
$check = new Ldr_InsideMstCheck();
$effective_post = Ldr_Util::use_tiket();

//----------------------------------------------------------
// 保存
//----------------------------------------------------------
if (isset($_POST['btn_save'])) {

    // バリデーション
    $error = $check->validate($_POST);

    // エラーが無ければ保存
    if (empty($error)) {
        try {
            if ($effective_post) {
                $check->save($_POST);
            }
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
        }
        $_SESSION['ldr_return'] = true;
        header('Location:' . 'adm_inside_sort_number.php');
        exit;
    }
}

//----------------------------------------------------------
// データの取得
//----------------------------------------------------------
$list = $check->lists_template();

if (empty($error)) {
    $list = $check->lists_template();
    if (empty($_POST['tmpl_id'])) {
        $data = $check->find($_POST['id']);
    }
    else {
        $data = $check->find_template($_POST['tmpl_id']);
    }

    $data['id'] = $_POST['id'];
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $session->emp());
$view->assign('list', $list);
$view->assign(Ldr_Util::get_common_info('adm_inside'));

if ($error) {
    $view->assign('error', $error);
    $view->assign('form', $_POST['form']);
    $view->display_with_fill('adm_inside_check.tpl', array('fdat' => $_POST));
}
else {
    $view->assign('form', $data['form']);
    $view->display_with_fill('adm_inside_check.tpl', array('fdat' => $data));
}