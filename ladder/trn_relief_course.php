<?php

require_once 'class/Base.php';
require_once 'class/Relief.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_ladder($emp);

//----------------------------------------------------------
// object
//----------------------------------------------------------
$relief = new Ldr_Relief();
$list = $relief->lists('contents_all');

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $session->emp());
$view->assign('list', $list);
$view->assign(Ldr_Util::get_common_info('relief_course'));
$view->display('trn_relief_course.tpl');
