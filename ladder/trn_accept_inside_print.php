<?php

require_once 'class/Base.php';
require_once 'class/Inside.php';
require_once 'class/PDF.php';

//----------------------------------------------------------
// ���å����
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// ����
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_inside($emp);

//----------------------------------------------------------
// object
//----------------------------------------------------------
$inside = new Ldr_Inside($emp->emp_id());
$conf = new Ldr_Config();
$mst = new Ldr_InsideMst();

//----------------------------------------------------------
// ID
//----------------------------------------------------------
if (!isset($_POST['id'])) {
    cmx_log(basename($_SERVER["PHP_SELF"]) . " parameter error");
    header('Location: dashboard.php');
    exit;
}
$id = explode('_', $_POST['id']);
$mst_date = $mst->find_date($id[0], $id[1]);

//----------------------------------------------------------
// Template
//----------------------------------------------------------
// ������
if (date('Y-m-d H:i') < "{$mst_date['date']['training_date']} {$mst_date['date']['training_last_time']}") {
    $template = 'print/trn_accept_inside.tpl';
    $list = $inside->lists_accept_by_date(array('id' => $id[1]));
}
// ���Ž�λ
else {
    $template = 'print/trn_accept_inside_end.tpl';
    $list = $inside->lists_accept_by_date(array('id' => $id[1], 'srh_attendance' => 1));
}

//----------------------------------------------------------
// view
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('trn', $mst_date);
$view->assign('list', $list);
$view->assign('date_id', $id[1]);
$html = $view->fetch($template);

//----------------------------------------------------------
// PDF
//----------------------------------------------------------
$pdf = new Ldr_PDF('P', 'mm', 'A4', true, 'UTF-8', false);
$pdf->setHeaderTitle('���⸦������̾��');
$pdf->setPrinter($session->emp());
$pdf->AddPage();
$pdf->writeHTML(to_utf8($html), true, 0, false, 0);
$pdf->Output("trn_accept_inside.pdf", "I");
