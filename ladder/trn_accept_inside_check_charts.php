<?php

require_once 'class/Base.php';
require_once 'class/InsideCheck.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    print cmx_json_encode(array());
    exit;
}

//----------------------------------------------------------
// パラメータチェック
//----------------------------------------------------------
if (!isset($_GET['id'])) {
    cmx_log(basename($_SERVER["PHP_SELF"]) . " parameter error");
    print cmx_json_encode(array());
    exit;
}

//----------------------------------------------------------
// object
//----------------------------------------------------------
$mstcheck = new Ldr_InsideMstCheck();
$check = new Ldr_InsideCheck($_GET['id']);

//----------------------------------------------------------
// レーダーチャート
//----------------------------------------------------------
$data = $check->charts($_GET['key']);

//----------------------------------------------------------
// JSON
//----------------------------------------------------------
header('X-Content-Type-Options: nosniff');
header('Cache-Control: no-cache, must-revalidate');
header("Cache-Control: post-check=0, pre-check=0", false);
header('Pragma: no-cache');
print cmx_json_encode($data);
