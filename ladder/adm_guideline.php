<?php

require_once 'class/Base.php';
require_once 'class/Guideline.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_admin($session->emp());

//----------------------------------------------------------
// オブジェクト
//----------------------------------------------------------
$gl = new Ldr_Guideline();

//----------------------------------------------------------
// 処理
//----------------------------------------------------------
switch ($_POST['mode2']) {
    // バリデーション
    case 'validate':
        $error = $gl->validate($_POST['mode'], Ldr_Util::from_utf8($_POST));
        header('X-Content-Type-Options: nosniff');
        print cmx_json_encode(array(
            'error' => !empty($error),
            'msg' => $error,
            'mode' => $_POST['mode'],
        ));
        exit;

    // 保存
    case 'save':
        $error = $gl->validate($_POST['mode'], $_POST);

        // エラーが無ければ保存
        if (empty($error)) {
            // 保存処理
            try {
                $gl->save($_POST['mode'], $_POST);
            }
            catch (Exception $e) {
                cmx_log($e->getMessage());
                js_error_exit();
            }
        }
        else {
            js_error_exit();
        }

        // 行動指標
        if ($_POST['mode'] === 'guideline') {
            $_COOKIE['pulldown_revision'] = $_POST['guideline_revision'];
            $_COOKIE['pulldown_group1'] = $_POST['guideline_group1'];
            $_COOKIE['pulldown_group2'] = $_POST['guideline_group2'];
            $_COOKIE['pulldown_level'] = $_POST['level'];
        }

        break;

    // 削除
    case 'delete':
        $gl->delete($_POST['mode'], $_POST['id']);
        break;

    // 並べ替え
    case 'sort':
        $gl->sort($_POST['mode'], $_POST);
        break;

    // 版の切り替え
    case 'current_revision':
        $gl->current_revision($_POST['current_revision']);
        break;

    // インポート
    case 'upload':
        if (array_key_exists("csvfile", $_FILES)) {
            if ($_FILES["csvfile"]["error"] !== 0 or $_FILES["csvfile"]["size"] <= 0) {
                header('X-Content-Type-Options: nosniff');
                print cmx_json_encode(array(
                    'error' => true,
                    'msg' => 'ファイルのアップロードに失敗しました',
                ));
                exit;
            }
        }
        try {
            $res = $gl->import_csv($_POST['guideline_revision'], $_FILES["csvfile"]);
        }
        catch (Exception $e) {
            header('X-Content-Type-Options: nosniff');
            print cmx_json_encode(array(
                'error' => true,
                'msg' => 'CSVのインポートに失敗しました',
                'result' => explode(',', $e->getMessage())
            ));
            exit;
        }

        header('X-Content-Type-Options: nosniff');
        print cmx_json_encode(array(
            'error' => false,
            'msg' => $res,
        ));
        exit;

    // エクスポート
    case 'download':
        $csv = to_sjis($gl->export_csv($_POST['guideline_revision']));
        $filename = "guideline_{$_POST['guideline_revision']}.csv";

        // output
        ob_clean();
        header("Content-Disposition: attachment; filename=$filename");
        header("Content-Type: application/octet-stream; name=$filename");
        header("Content-Length: " . strlen($csv));
        echo($csv);
        ob_end_flush();
        exit;
}

//----------------------------------------------------------
// 版
//----------------------------------------------------------
$current_revision = $gl->current_revision();
$revision_list = $gl->lists('revision');
if (!empty($_COOKIE['pulldown_revision'])) {
    $cond['guideline_revision'] = $_COOKIE['pulldown_revision'];
}
else {
    $cond['guideline_revision'] = $current_revision;
}

//----------------------------------------------------------
// レベル
//----------------------------------------------------------
if (isset($_POST['level']) && $_POST['level'] !== '') {
    $cond['level'] = $_POST['level'];
}
else {
    $cond['level'] = $_COOKIE['pulldown_level'] !== '' ? $_COOKIE['pulldown_level'] : 1;
}

//----------------------------------------------------------
// 領域
//----------------------------------------------------------
$group1_list = $gl->lists('group1', $cond);
if (!empty($_POST['guideline_group1'])) {
    $cond['guideline_group1'] = $_POST['guideline_group1'];
}
else {
    $cond['guideline_group1'] = $_COOKIE['pulldown_group1'] ? $_COOKIE['pulldown_group1'] : $group1_list[0]['group1'];
}

//----------------------------------------------------------
// 分類
//----------------------------------------------------------
$group2_list = $gl->lists('group2', $cond);

//----------------------------------------------------------
// 行動指標
//----------------------------------------------------------
$guideline_list = $gl->lists('guideline', $cond);

//----------------------------------------------------------
// active
//----------------------------------------------------------
if (!empty($_POST['mode'])) {
    $active = $_POST['mode'];
}
else if (!empty($_COOKIE['active'])) {
    $active = $_COOKIE['active'];
}
else {
    $active = 'guideline';
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $session->emp());
$view->assign(Ldr_Util::get_common_info('adm_guideline'));

$view->assign(array(
    'current_revision' => $current_revision,
    'guideline_list' => $guideline_list,
    'revision_list' => $revision_list,
    'group1_list' => $group1_list,
    'group2_list' => $group2_list,
    'guideline_group1' => $cond['guideline_group1'],
));

$view->assign('active', $active);
$view->display_with_fill('adm_guideline.tpl', array('fdat' => $cond));
