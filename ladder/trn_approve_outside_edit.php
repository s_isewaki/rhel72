<?php

require_once 'class/Base.php';
require_once 'class/Outside.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('trn_approve');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_ladder($emp);

//----------------------------------------------------------
// object
//----------------------------------------------------------
$outside = new Ldr_Outside($emp->emp_id());

//----------------------------------------------------------
// 保存
//----------------------------------------------------------
if ($_POST['btn_save'] === "save") {
    try {
        $outside->save_approve($_POST);
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }
    $_SESSION['ldr_return'] = true;
    header('Location: ' . Ldr_Util::get_referer_url());
    exit;
}
else if ($_POST['btn_save'] === "cancel") {
    try {
        $outside->cancel_approve($_POST);
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }
    $_SESSION['ldr_return'] = true;
    header('Location: ' . Ldr_Util::get_referer_url());
    exit;
}

//----------------------------------------------------------
// データの取得
//----------------------------------------------------------
if (!isset($_POST['id'])) {
    cmx_log(basename($_SERVER["PHP_SELF"]) . " parameter error");
    header('Location: dashboard.php');
    exit;
}

$fdat = $outside->find($_POST['id']);
$target_emp = new Ldr_Employee($fdat['emp_id']);
$data = array(
    'type_text' => Ldr_Const::get_outside_type(),
    'trn' => $fdat,
    'target_emp' => $target_emp,
    'hd_title' => '院外研修 承認',
);
//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $emp);
$view->assign($data);
$view->assign(Ldr_Util::get_common_info('tapprove_outside'));
$view->display('trn_approve_outside_edit.tpl');
