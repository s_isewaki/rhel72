<?php

require_once 'class/Base.php';
require_once 'class/Inside.php';
require_once 'class/InsideMst.php';
require_once 'class/Classmst.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('accept_inside');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_inside($emp);

//----------------------------------------------------------
// object
//----------------------------------------------------------
$inside = new Ldr_Inside($emp->emp_id());
$mst = new Ldr_InsideMst();
$classmst = new Ldr_Classmst();

$effective_post = Ldr_Util::use_tiket();

//----------------------------------------------------------
// 保存
//----------------------------------------------------------
if ($_POST['btn_save'] === "save") {
    try {
        if ($effective_post) {
            $inside->save_attendance($_POST);
        }
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }
    $_SESSION['ldr_return'] = true;
    header('Location: ' . Ldr_Util::get_referer_url());
    exit;
}

//----------------------------------------------------------
// データの取得
//----------------------------------------------------------
if (!isset($_POST['id'])) {
    cmx_log(basename($_SERVER["PHP_SELF"]) . " parameter error");
    header('Location: dashboard.php');
    exit;
}

$id = explode('_', $_POST['id']);
$mst_date = $mst->find_date($id[0], $id[1]);

$cond = array(
    'id' => $id[1],
    'srh_class1' => $_POST['srh_class1'],
    'srh_class3' => $_POST['srh_class3'],
    'srh_attendance' => $_POST['srh_attendance'],
);

$fdat = array(
    'id' => $_POST['id'],
    'title' => "{$mst_date['title']} 第{$mst_date['date']['number']}回 / 全{$mst_date['number_of']}回",
    'type' => $mst_date['date']['type'],
    'training_date' => $mst_date['date']['training_date'],
    'srh_class1' => $_POST['srh_class1'],
    'srh_attendance' => $_POST['srh_attendance'],
);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $emp);
$view->assign('trn', $mst_date);
$view->assign('date_id', $id[1]);
$view->assign('list', $inside->lists_accept_by_date($cond));
$view->assign('contents_list', $inside->lists_accept_by_contents(array('id' => $mst_date['date']['inside_contents_id'], 'without_date_id' => $id[1])));
$view->assign('class_list', $classmst->lists());
$view->assign('class12_list', $classmst->class12_lists());
$view->assign('srh_class3', $_POST['srh_class3']);

$view->assign(Ldr_Util::get_common_info($_SESSION['menu'], array('referer_url' => 'trn_accept_inside.php')));
$view->display_with_fill('trn_accept_inside_attendance.tpl', array('fdat' => $fdat));
