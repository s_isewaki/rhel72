<?php

require_once 'class/Base.php';
require_once 'class/Novice.php';
require_once 'class/NoviceAssessment.php';
require_once 'class/Config.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_ladder($session->emp());

//----------------------------------------------------------
// オブジェクト
//----------------------------------------------------------
$novice = new Ldr_Novice();
$as = new Ldr_NoviceAssessment($session->emp()->emp_id());
$conf = new Ldr_Config();

//----------------------------------------------------------
// 保存ボタン
//----------------------------------------------------------
if ($_POST['btn_save'] === 'btn_save') {
    try {
        $as->save_facilitate($_POST);
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }
}

//----------------------------------------------------------
// 自動保存
//----------------------------------------------------------
else if ($_POST['save'] === 'ajax') {
    try {
        $as->save_facilitate($_POST, 'auto');
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }
    exit;
}

//----------------------------------------------------------
// 版数
//----------------------------------------------------------
$current_revision = $as->current_revision();

//----------------------------------------------------------
// 一覧
//----------------------------------------------------------
$group1_list = $as->lists('group1', array('revision_id' => $current_revision));
foreach ($group1_list as $g1) {
    $group2_list[$g1['group1']] = $as->lists('group2', array('revision_id' => $current_revision, 'group1' => $g1['group1']));
}

//----------------------------------------------------------
// year
//----------------------------------------------------------
$year = $_REQUEST['year'];
if (!Validation::is_year($year)) {
    header('Location: dashboard.php');
    exit;
}

//----------------------------------------------------------
// number
//----------------------------------------------------------
$number = $_REQUEST['number'];
if (!is_numeric($number)) {
    header('Location: dashboard.php');
    exit;
}

//----------------------------------------------------------
// assessment
//----------------------------------------------------------
list($assessment, $f_emp_id, $f_updated_on) = $as->facilitate_assessment($_REQUEST['emp_id'], $year, $number);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $session->emp());
$view->assign(Ldr_Util::get_common_info('novice_facilitator'));
$view->assign('novice_number', $conf->novice_number());

$view->assign(array(
    'current_revision' => $current_revision,
    'group1_list' => $group1_list,
    'group2_list' => $group2_list,
    'assessment_list' => $assessment,
    'year' => $year,
    'number' => $number,
    'emp_id' => $_REQUEST['emp_id'],
    'active' => $_REQUEST['active'],
    'novice' => new Ldr_Employee($_REQUEST['emp_id']),
    'facilitator' => $f_emp_id ? new Ldr_Employee($f_emp_id) : null,
    'f_updated_on' => $f_updated_on,
));

$view->display('novice_facilitate_assessment.tpl');
