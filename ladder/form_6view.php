<?php

require_once 'class/Base.php';
require_once 'class/Validation.php';
require_once 'class/Apply.php';
require_once 'ladder/class/Guideline.php';

//----------------------------------------------------------
// ���å����
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// ����
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_ladder($emp);

//----------------------------------------------------------
// object
//----------------------------------------------------------
$target_emp = new Ldr_Employee($_POST["target_emp_id"]);
$apply = new Ldr_Apply(array('apply_emp_id' => $_POST["target_emp_id"], 'accept_emp_id' => $emp->emp_id(), 'apply_id' => $_POST['id']));
$ladder = $apply->ladder();
$form6 = $apply->find_form6();
$assessment = $apply->report_assessment();

//----------------------------------------------------------
// ɾ���򸫤��
//  ��ݡ���ɾ���ԡ�ɾ����ǧ��
//  ����ݡ�ɾ����ǧ��
//  ��ǧ�ԡ�ɾ����ǧ��
//  �����ԡ��ե�����λ
//----------------------------------------------------------
$tmpl = 'form_6view.tpl';

if ($assessment['admin_completed'] === 't') {
    if ($emp->is_committee() || $apply->is_reporter($emp->emp_id()) || $apply->is_approver($emp->emp_id()) || $apply->get_status() === '0') {
        $tmpl = 'form_6view_assessment.tpl';
    }
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $emp);
$view->assign('target_emp', $target_emp);
$view->assign(Ldr_Util::get_common_info($_POST['menu'], array('referer_url' => $_POST['owner_url'])));
$view->assign('apply', $apply);
$view->assign('ldr', $ladder);
$view->assign('form6', $form6);
$view->assign('assessment', $assessment);

//----------------------------------------------------------
// ����
//----------------------------------------------------------
if ($_POST['mode'] === 'print') {
    require_once 'class/PDF.php';
    $html = $view->output_with_fill("print/$tmpl", array('fdat' => $_POST));
    $pdf = new Ldr_PDF('P', 'mm', 'A4', true, 'UTF-8', false);
    $pdf->setHeaderTitle("[{$ladder['config']['form_6']['title']}] {$ladder['config']['form_6']['detailL']}");
    $pdf->setEmployee($target_emp);
    $pdf->setPrinter($session->emp());
    $pdf->AddPage();
    $pdf->writeHTML(to_utf8($html), true, 0, false, 0);
    $pdf->Output("form_6view.pdf", "I");
    exit;
}

//----------------------------------------------------------
// ɽ��
//----------------------------------------------------------
$view->display_with_fill($tmpl, array('fdat' => $_POST));
