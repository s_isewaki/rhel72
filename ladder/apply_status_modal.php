<?php

require_once 'class/Base.php';
require_once 'class/Apply.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// オブジェクト
//----------------------------------------------------------
$apply = new Ldr_Apply(array('apply_emp_id' => $session->emp()->emp_id(), 'apply_id' => $_GET['id']));
$view = new Cmx_View('ladder/templates');

//----------------------------------------------------------
// データ
//----------------------------------------------------------
if ($_GET['id']) {
    $view->assign('emp', $session->emp());
    $view->assign('apply', $apply);
    $view->assign('ldr', $apply->ladder());
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
header('Cache-Control: no-cache, must-revalidate');
header("Cache-Control: post-check=0, pre-check=0", false);
header('Pragma: no-cache');
$view->display("modal/status.tpl");
