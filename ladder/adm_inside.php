<?php

require_once 'class/Base.php';
require_once 'class/Pager.php';
require_once 'class/InsideMst.php';
require_once 'class/Guideline.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('adm_inside');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_mst_inside($session->emp());

//----------------------------------------------------------
// Obj
//----------------------------------------------------------
$mst = new Ldr_InsideMst();
$gl = new Ldr_Guideline();
$perPage = Ldr_Const::SRH_PERPAGE;
$effective_post = Ldr_Util::use_tiket();

//----------------------------------------------------------
// Obj
//----------------------------------------------------------
if ($effective_post) {
    switch ($_POST['mode2']) {
        // バリデーション
        case 'validate':
            $euc = Ldr_Util::from_utf8($_POST);
            $error = $mst->validate_copy($euc);
            header('X-Content-Type-Options: nosniff');
            print cmx_json_encode(array(
                'error' => !empty($error),
                'msg' => $error,
                'mode' => $_POST['mode'],
            ));
            exit;

        // 保存
        case 'save':
            $error = $mst->validate_copy($_POST);

            // エラーが無ければ保存
            if (empty($error)) {
                // 保存処理
                try {
                    $mst->save_copy($_POST);
                    $completed = true;
                }
                catch (Exception $e) {
                    cmx_log($e->getMessage());
                    js_error_exit();
                }
            }
            else {
                js_error_exit();
            }
            break;
    }
}

//----------------------------------------------------------
// year
//----------------------------------------------------------
$year_list = $mst->year_lists();

//----------------------------------------------------------
// 遷移状態判別
//----------------------------------------------------------
// ページャーから移動
if (Ldr_Pager::is_pager($_POST['move_pager'])) {
    //検索条件を取得
    $_SESSION['page_no'] = Ldr_Pager::get_page_no($_POST['page_no']);
    $srh_condition = $_SESSION['srh_condition'];
}
// 検索ボタン
else if ($_POST['search']) {
    $srh_condition["srh_year"] = $_POST['srh_year'];
    $srh_condition["srh_level"] = $_POST['srh_level'];
    $srh_condition["srh_guideline"] = $_POST['srh_guideline'];

    $_POST["page_no"] = 1;

    // 検索条件を設定
    $_SESSION['srh_condition'] = $srh_condition;
}
//次ページから戻ってきた場合
else if ($completed || isset($_POST['ldr_return']) || isset($_SESSION['ldr_return'])) {
    // ページ番号の復旧
    $_POST["page_no"] = $_SESSION['page_no'];

    // 検索条件の復旧
    $srh_condition = $_SESSION['srh_condition'];
}
// 外部から移動してきた場合
else {
    // セッション情報の初期化
    $_SESSION = array();
    $_POST["page_no"] = 1;
    $_SESSION['page_no'] = 1;
    $_SESSION['srh_condition'] = null;
    $srh_condition = null;


    $srh_condition["srh_year"] = count($year_list) === 1 ? $year_list[0] : Ldr_Util::th_year();
    $_SESSION['srh_condition'] = $srh_condition;
}
$_SESSION['ldr_return'] = null;

//----------------------------------------------------------
// list
//----------------------------------------------------------
$dataCount = $mst->count($srh_condition);
$current = Ldr_Pager::get_page_no($_POST['page_no'], $dataCount, $perPage);
$start = Ldr_Pager::calc_page_index($current, $perPage);
$list = $mst->lists($start, $perPage, $srh_condition);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign(Ldr_Pager::get_pager_info($_POST['page_no'], $dataCount, $perPage));
$view->assign(Ldr_Util::get_common_info('adm_inside'));
$view->assign('emp', $session->emp());
$view->assign('year_list', $year_list);
$view->assign('completed', $completed);
$view->assign('group1', $gl->lists('group1', array('guideline_revision' => 1)));
$view->assign('list', $list);
$view->display_with_fill('adm_inside.tpl', array('fdat' => $srh_condition));
