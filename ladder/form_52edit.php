<?php

require_once 'class/Base.php';
require_once 'class/Form52.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('form52');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_ladder($session->emp());

//----------------------------------------------------------
// Object
//----------------------------------------------------------
$emp = $session->emp();
$form = new Ldr_Form52($emp->emp_id());
$effective_post = Ldr_Util::use_tiket();

//----------------------------------------------------------
// 保存
//----------------------------------------------------------
if (isset($_POST['btn_save'])) {
    // バリデーション
    $error = $form->validate($_POST);

    // エラーが無ければ保存
    if (empty($error)) {
        // 保存処理
        try {
            if ($effective_post) {
                $form->save($_POST);
            }
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
            exit;
        }

        // 一覧に戻る
        $_SESSION['ldr_return'] = true;
        header('Location: form_52.php');
        exit;
    }
}
//----------------------------------------------------------
// 編集
//----------------------------------------------------------
elseif (isset($_POST["inner_id"]) && $_POST["inner_id"] !== "") {
    try {
        $fdat = $form->find($_POST["inner_id"]);
        if (empty($fdat)) {
            $not_found = true;
        }
    }
    catch (Exception $e) {
        header('Location: dashboard.php');
        exit;
    }
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $session->emp());
$view->assign(Ldr_Util::get_common_info('form_52'));
$view->assign('not_found', $not_found);
$view->assign('types', $form->types);

if (empty($error)) {
    $view->display_with_fill('form_52edit.tpl', array('fdat' => $fdat));
}
else {
    $view->assign('error', $error);
    $view->display_with_fill('form_52edit.tpl', array('fdat' => $_POST));
}
