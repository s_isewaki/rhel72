<?php

require_once 'class/Base.php';
require_once 'class/NoviceAssessment.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_ladder($session->emp());

//----------------------------------------------------------
// オブジェクト
//----------------------------------------------------------
$as = new Ldr_NoviceAssessment();
$view = new Cmx_View('ladder/templates');

//----------------------------------------------------------
// バリデーション
//----------------------------------------------------------
$group1 = $as->find('group1', $_GET['group1']);
if (empty($group1)) {
    header('dashboard.php');
    exit;
}

//----------------------------------------------------------
// revision
//----------------------------------------------------------
$revision = $as->current_revision();

//----------------------------------------------------------
// group2
//----------------------------------------------------------
$group2_list = $as->group2_lists(array(
    'revision' => $revision,
    'group1' => $_GET['group1'],
    ));
$view->assign('group2_list', $group2_list);

//----------------------------------------------------------
// assessment
//----------------------------------------------------------
$assessment = $as->assessment(array(
    'emp_id' => $session->emp()->emp_id(),
    'group1' => $_GET['group1'],
    'year' => $_GET['year'],
    'number' => $_GET['number'],
    ));
$view->assign('assessment', $assessment);

//----------------------------------------------------------
// assign
//----------------------------------------------------------
$view->assign('group1', $_GET['group1']);
$view->assign('year', $_GET['year']);
$view->assign('number', $_GET['number']);
$view->assign(Ldr_Util::get_common_info());

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
header('Cache-Control: no-cache, must-revalidate');
header("Cache-Control: post-check=0, pre-check=0", false);
header('Pragma: no-cache');
$view->display("noivce_assessment_tab.tpl");
