<?php

require_once 'class/Base.php';
require_once 'class/Novice.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_approver_admin($emp);

//----------------------------------------------------------
// object
//----------------------------------------------------------
$novice = new Ldr_Novice();
$effective_post = Ldr_Util::use_tiket();

//----------------------------------------------------------
// 年リスト
//----------------------------------------------------------
$year_list = $novice->year_lists();
$year = !empty($_REQUEST['year']) ? $_REQUEST['year'] : Ldr_Util::th_year();

//----------------------------------------------------------
// CSV取り込み
//----------------------------------------------------------
if ($_POST['mode'] === 'import') {
    // ファイルのチェック
    $file_err = Ldr_Util::exist_upload_file();
    if (!empty($file_err)) {
        header('Cache-Control: no-cache, must-revalidate');
        header("Cache-Control: post-check=0, pre-check=0", false);
        header('Pragma: no-cache');
        header('X-Content-Type-Options: nosniff');
        print cmx_json_encode($file_err);
        exit;
    }

    // インポート
    try {
        $res = $novice->import_csv($_REQUEST['year'], $_FILES["csvfile"]);
    }
    catch (Exception $e) {
        header('Cache-Control: no-cache, must-revalidate');
        header("Cache-Control: post-check=0, pre-check=0", false);
        header('Pragma: no-cache');
        header('X-Content-Type-Options: nosniff');
        print cmx_json_encode(array(
            'error' => true,
            'msg' => 'CSVのインポートに失敗しました',
            'result' => explode(',', $e->getMessage())
        ));
        exit;
    }

    header('Cache-Control: no-cache, must-revalidate');
    header("Cache-Control: post-check=0, pre-check=0", false);
    header('Pragma: no-cache');
    header('X-Content-Type-Options: nosniff');
    print cmx_json_encode(array(
        'error' => false,
        'msg' => $res,
    ));
    exit;
}

//----------------------------------------------------------
// 保存
//----------------------------------------------------------
if (isset($_POST['btn_save'])) {
    // バリデーション
    $error = $novice->validate_novice($_POST);

    // エラーが無ければ保存
    if (empty($error)) {
        try {
            if ($effective_post) {
                $novice->save_novice($_POST);
                $completed = true;
            }
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
        }
    }
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $emp);
$view->assign(Ldr_Util::get_common_info('adm_novice'));
$view->assign('year_list', $year_list);
$view->assign('year', $year);
$view->assign('novice_list', $novice->lists($year));
$view->assign('completed', $completed);
$view->assign('error', $error);
$view->display('adm_novice.tpl');
