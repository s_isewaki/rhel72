<?php

require_once 'class/Base.php';
require_once 'Cmx/Model/Status.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_admin($session->emp());

//----------------------------------------------------------
// オブジェクト
//----------------------------------------------------------
$conf = new Ldr_Config();

//----------------------------------------------------------
// 保存
//----------------------------------------------------------
if ($_POST['btn_save']) {
    try {
        $conf->save_novice($_POST);
        $completed = true;
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $session->emp());
$view->assign(Ldr_Util::get_common_info('adm_novice_setting'));
$view->assign('novice_number', $conf->novice_number());
$view->assign('completed', $completed);
$view->display('adm_novice_setting.tpl');
