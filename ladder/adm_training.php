<?php

require_once 'class/Base.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_mst_inside($session->emp());

//----------------------------------------------------------
// オブジェクト
//----------------------------------------------------------
$conf = new Ldr_Config();

//----------------------------------------------------------
// 保存
//----------------------------------------------------------
if ($_POST['btn_save'] === 'btn_save') {
    // 保存処理
    try {
        $conf->save_training($_POST);
        $completed = true;
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $session->emp());
$view->assign(Ldr_Util::get_common_info('adm_training'));
$view->assign('ojt_name', $conf->ojt_name());
$view->assign('completed', $completed);
$view->display('adm_training.tpl');
