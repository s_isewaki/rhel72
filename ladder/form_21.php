<?php

require_once 'class/Base.php';
require_once 'class/Form21.php';
require_once 'class/Pager.php';

//１ページの表示件数
$perPage = Ldr_Const::SRH_PERPAGE;

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('form21');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_ladder($session->emp());

//----------------------------------------------------------
// Object
//----------------------------------------------------------
$emp = $session->emp();
$form = new Ldr_Form21($emp->emp_id());
$conf = new Ldr_Config();
//----------------------------------------------------------
// export
//----------------------------------------------------------
if ($_GET['mode'] === 'export_csv') {

    $csv = to_sjis($form->export_csv());

    // output
    ob_clean();
    header("Content-Disposition: attachment; filename=form21.csv");
    header("Content-Type: application/octet-stream; name=form21.csv");
    header("Content-Length: " . strlen($csv));
    echo($csv);
    ob_end_flush();
    exit;
}

//----------------------------------------------------------
// import
//----------------------------------------------------------
if ($_POST['mode'] === 'import') {
    // ファイルのチェック
    $file_err = Ldr_Util::exist_upload_file();
    if (!empty($file_err)) {
        print cmx_json_encode($file_err);
        exit;
    }

    // インポート
    try {
        $res = $form->import_csv($_FILES["csvfile"]);
    }
    catch (Exception $e) {
        print cmx_json_encode(array(
            'error' => true,
            'msg' => 'CSVのインポートに失敗しました',
            'result' => explode(',', $e->getMessage())
        ));
        exit;
    }
    print cmx_json_encode(array(
        'error' => false,
        'msg' => $res,
    ));
    exit;
}

//----------------------------------------------------------
// 遷移状態判別
//----------------------------------------------------------

// ページャーから移動
if (Ldr_Pager::is_pager($_POST['move_pager'])) {
    //検索条件を取得
    $_SESSION['page_no'] = Ldr_Pager::get_page_no($_POST['page_no']);
    $srh_condition = $_SESSION['srh_condition'];
}
// 検索ボタン
else if ($_POST['btn_search']) {

    $srh_condition["srh_keyword"] = $_POST['srh_keyword'];
    $srh_condition["srh_start_date"] = $_POST['srh_start_date'];
    $srh_condition["srh_last_date"] = $_POST['srh_last_date'];

    $_POST["page_no"] = 1;

    // 検索条件を設定
    $_SESSION['srh_condition'] = $srh_condition;
}
// 削除/編集画面から戻ってきた場合
else if (isset($_POST['ldr_return']) || isset($_SESSION['ldr_return']) || (isset($_POST['del_inner_id']) && $_POST['del_inner_id'] !== "" )) {
    // ページ番号の復旧
    $_POST["page_no"] = $_SESSION['page_no'];

    // 検索条件の復旧
    $srh_condition = $_SESSION['srh_condition'];
}
// 外部から移動してきた場合
else {
    // セッション情報の初期化
    $_SESSION = array();
    $_POST["page_no"] = 1;
    $_SESSION['page_no'] = 1;
    $_SESSION['srh_condition'] = null;
    $srh_condition = null;
}
$_SESSION['ldr_return'] = null;
$fdat = array_merge((array)$_POST, (array)$srh_condition);

//----------------------------------------------------------
// 削除処理
//----------------------------------------------------------
if (isset($_POST['del_inner_id']) && $_POST['del_inner_id'] !== "") {
    $form->delete($_POST['del_inner_id']);
}
$_POST['del_inner_id'] = '';

//----------------------------------------------------------
// 一覧
//----------------------------------------------------------
// データ数の取得
$dataCount = $form->count($srh_condition);
$current = Ldr_Pager::get_page_no($_POST['page_no'], $dataCount, $perPage);
$start = Ldr_Pager::calc_page_index($current, $perPage);
$is_cond = $_SESSION['srh_condition'] ? true : false;
// データの取得
$order = array('order_by' => array('start_date desc'));
$list = $form->lists($start, $perPage, array_merge((array)$srh_condition, (array)$order));

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $session->emp());
$view->assign(Ldr_Util::get_common_info('form_21'));
$view->assign(Ldr_Pager::get_pager_info($_POST['page_no'], $dataCount, $perPage));
$view->assign('list', $list);
$view->assign('search', $is_cond);
$view->display_with_fill('form_21.tpl', array('fdat' => $fdat));
