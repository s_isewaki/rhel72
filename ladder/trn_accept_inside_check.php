<?php

require_once 'class/Base.php';
require_once 'class/Inside.php';
require_once 'class/InsideCheck.php';
require_once 'class/InsideMstCheck.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('inside_report');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_inside($emp);

//----------------------------------------------------------
// object
//----------------------------------------------------------
$inside = new Ldr_Inside($emp->emp_id());
$view = new Cmx_View('ladder/templates');
$mst = new Ldr_InsideMst();
$mstcheck = new Ldr_InsideMstCheck();

//----------------------------------------------------------
// パラメータチェック
//----------------------------------------------------------
if (!( isset($_POST['id']) or ( isset($_GET['id']) and isset($_GET['print'])))) {
    cmx_log(basename($_SERVER["PHP_SELF"]) . " parameter error");
    header('Location: dashboard.php');
    exit;
}

//----------------------------------------------------------
// 研修マスタ情報
//----------------------------------------------------------
list($inside_id, $contents_id) = explode('_', $_REQUEST['id']);
$train = $mst->find_contents($inside_id, $contents_id);
$dates = $mst->find_date($inside_id);
$check = new Ldr_InsideCheck($contents_id);

//----------------------------------------------------------
// CSV出力
//----------------------------------------------------------
if (isset($_POST['export_csv'])) {
    $csv = to_sjis($check->export_csv());
    $filename = 'inside_check.csv';

    // output
    ob_clean();
    header("Content-Disposition: attachment; filename=$filename");
    header("Content-Type: application/octet-stream; name=$filename");
    header("Content-Length: " . strlen($csv));
    echo($csv);
    ob_end_flush();
    exit;
}

//----------------------------------------------------------
// 集計データ
//----------------------------------------------------------
try {
    $count = $check->count();
}
catch (Exception $e) {
    cmx_log($e->getMessage());
    js_error_exit();
    exit;
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view->assign('emp', $emp);
$view->assign('trn', $train);
$view->assign('dates', $dates);
$view->assign('count', $count);

if ($_GET['print']) {
    $view->display('trn_accept_inside_check_print.tpl');
}
else {
    $view->assign(Ldr_Util::get_common_info('taccept_inside'));
    $view->display('trn_accept_inside_check.tpl');
}