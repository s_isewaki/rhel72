<?php

require_once 'class/Base.php';
require_once 'class/Pager.php';
require_once 'class/Apply.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_ladder($emp);

//----------------------------------------------------------
// Object
//----------------------------------------------------------
$apply = new Ldr_Apply(array('apply_emp_id' => $emp->emp_id()));

//----------------------------------------------------------
// ページ遷移
//----------------------------------------------------------
Ldr_Util::session_init('ldr_common');

// 戻る/コメント記入後のリロード
if (isset($_POST['ldr_return']) || isset($_SESSION['commnet_reload'])) {
    unset($_SESSION['commnet_reload']);

    $_POST["page_no"] = $_SESSION['pager_no'];
}

// ページャーから移動
else if (Ldr_Pager::is_pager($_POST['move_pager'])) {
    //検索条件を取得
    $_SESSION['pager_no'] = Ldr_Pager::get_page_no($_POST['page_no']);
}

// それ以外
else {
    $_SESSION = array();
}

//----------------------------------------------------------
// データ数の取得
//----------------------------------------------------------
$dataCount = $apply->count_apply($srh_condition);
$perPage = Ldr_Const::SRH_PERPAGE;
$current = Ldr_Pager::get_page_no($_POST['page_no'], $dataCount, $perPage);
$start = Ldr_Pager::calc_page_index($current, $perPage);

//----------------------------------------------------------
// 一覧の取得
//----------------------------------------------------------
$list = $apply->lists_apply($start, $perPage, null);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign(Ldr_Pager::get_pager_info($_POST['page_no'], $dataCount, $perPage));
$view->assign('emp', $emp);
$view->assign(Ldr_Util::get_common_info('apply'));
$view->assign('list', $list);
$view->display('apply.tpl');
