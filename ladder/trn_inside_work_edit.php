<?php

require_once 'class/Base.php';
require_once 'class/Pager.php';
require_once 'class/Inside.php';


//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}
//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('inside_apply');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_ladder($emp);

//----------------------------------------------------------
// object
//----------------------------------------------------------
$inside = new Ldr_Inside($emp->emp_id());
$effective_post = Ldr_Util::use_tiket();

//----------------------------------------------------------
// 一時保存
//----------------------------------------------------------
if ($_POST['btn_save'] === 'btn_save') {
    try {
        if ($effective_post) {
            $_POST['status'] = 0;
            $inside->save_work($_POST);
        }
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }
    $_SESSION['ldr_return'] = true;
    header('Location:' . Ldr_Util::get_referer_url());
    exit;
}

//----------------------------------------------------------
// 確定
//----------------------------------------------------------
else if ($_POST['btn_save'] === 'btn_completed') {
    // バリデーション
    $error = $inside->validate_work($_POST);

    // エラーが無ければ保存
    if (empty($error)) {
        try {
            if ($effective_post) {
                $_POST['status'] = 1;
                $inside->save_work($_POST);
            }
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
        }
        //unset($_SESSION['ldr_return']); //確定の場合は1ページ目に戻す
        $_SESSION['ldr_return'] = true;
        header('Location:' . Ldr_Util::get_referer_url());
        exit;
    }
}
//----------------------------------------------------------
// データの取得
//----------------------------------------------------------
$dt = $inside->find($_POST['id']);
$status = ($_POST['mode'] === 'before') ? $dt['before_status'] : $dt['after_status'];
if (empty($error)) {
    $fdat = array(
        'mode' => $_POST['mode'],
        'id' => $_POST['id'],
        'work' => ($_POST['mode'] === 'before') ? $dt['before_work'] : $dt['after_work'],
        'wk_status' => $status,
    );
}

$data = array(
    'trn' => $dt,
    'hd_title' => ($_POST['mode'] === 'before') ? '院内研修 前課題入力' : '院内研修 後課題入力',
    'status' => $status,
);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $session->emp());
$view->assign($dt);
$view->assign($data);
$view->assign(Ldr_Util::get_common_info('inside'));

if (empty($error)) {
    $view->display_with_fill('trn_inside_work_edit.tpl', array('fdat' => $fdat));
}
else {
    $view->assign('error', $error);
    $view->display_with_fill('trn_inside_work_edit.tpl', array('fdat' => $_POST));
}
