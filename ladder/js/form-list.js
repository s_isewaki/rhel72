$(function() {
    //項目の削除
    $('.i_remove').click(function() {
        if (!confirm('削除してもよろしいですか？')) {
            return;
        }
        $('#del_inner_id').val($(this).attr('id'));
        $(this).parents('form').submit();
    });

    $('#srh_keyword').keypress(function(ev) {
        if ((ev.which && ev.which === 13) || (ev.keyCode && ev.keyCode === 13)) {
            $('#btn_search').trigger("click");
        }
    });
});
