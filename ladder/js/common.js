// grobal parameters
var cookie_expires = 365;

// jquery function
$(function() {
    /**
     * キャッシュしない
     */
    $.ajaxSetup({cache: false});

    /**
     * popover
     */
    $('[rel=popover]').popover();

    /**
     * tooltip
     * IE7/8 workaround: https://github.com/twbs/bootstrap/issues/7193
     */
    $(document).on('mouseenter', '[rel=tooltip]', function() {
        $(this).tooltip({
            container: 'body',
            trigger: 'manual'
        }).tooltip('show');
    });
    $(document).on('mouseleave', '[rel=tooltip]', function() {
        $(this).tooltip('hide');
    });

    /**
     * multiselect
     */
    $('.multiselect').multiselect({
        nonSelectedText: "選択してください",
        numberDisplayed: 20
    });
    $('.multiselect-all').multiselect({
        includeSelectAllOption: true,
        selectAllText: "全選択",
        selectAllValue: 0,
        nonSelectedText: "選択してください",
        allSelectedText: "すべて",
        numberDisplayed: 10
    });

    /**
     * 削除の確認
     */
    $('.js-remove').click(function() {
        if (confirm("削除してもよろしいですか？")) {
            return true;
        }
        return false;
    });

    /**
     * 復旧の確認
     */
    $('.js-recover').click(function() {
        if (confirm("復旧してもよろしいですか？")) {
            return true;
        }
        return false;
    });

    /**
     * ラダーを閉じる
     */
    $('.ldr_close').click(function() {
        if (confirm("キャリア開発ラダーを閉じてもよろしいですか？")) {
            window.close();
        }
        return false;
    });

    /**
     * 申請ボタン
     */
    $('.js-submit-apply').click(function() {
        if (confirm("申請を実行します。よろしいですか？")) {
            return true;
        }
        return false;
    });
    /**
     * 受付
     */
    $('.js-submit-accept').click(function() {
        if (confirm("申請を受け付けます。よろしいですか？")) {
            return true;
        }
        return false;
    });
    /**
     * 相談ボタン
     */
    $('.js-submit-preview').click(function() {
        if (confirm("相談を申請します。よろしいですか？")) {
            return true;
        }
        return false;
    });

    /**
     * 評価確定
     */
    $('.js-submit-decision').click(function() {
        if (confirm("評価を確定します。よろしいですか？")) {
            return true;
        }
        return false;
    });

    /**
     * 研修 課題提出
     */
    $('.js-submit-work').click(function() {
        if (confirm("課題を提出します。よろしいですか？")) {
            return true;
        }
        return false;
    });
    /**
     * 研修 課題提出
     */
    $('.js-submit-check').click(function() {
        if (confirm("振り返りを提出します。よろしいですか？")) {
            return true;
        }
        return false;
    });
    /**
     * 差し戻しボタン
     */
    $('.js-submit-sendback').click(function() {
        if (confirm("差し戻しを実行します。よろしいですか？")) {
            return true;
        }
        return false;
    });
    $('.js-submit-setting').click(function() {
        if (confirm("設定します。よろしいですか？")) {
            return true;
        }
        return false;
    });

    /**
     * 取り下げボタン
     */
    $('.js-submit-withdraw').click(function() {
        if (confirm("取り下げを実行します。よろしいですか？")) {
            return true;
        }
        return false;
    });

    /**
     * 修正ボタン
     */
    $('.js-submit-modify').click(function() {
        if (confirm("修正を実行します。よろしいですか？")) {
            return true;
        }
        return false;
    });

    /**
     * 承認ボタン
     */
    $('.js-submit-approve').click(function() {
        if (confirm("承認を実行します。よろしいですか？")) {
            return true;
        }
        return false;
    });

    /**
     * 出欠
     */
    $('.js-submit-attendance').click(function() {
        if (confirm("出欠を設定します。よろしいですか？")) {
            return true;
        }
        return false;
    });
    /**
     * 保存
     */
    $('.js-submit-save').click(function() {
        if (confirm("保存します。よろしいですか？")) {
            return true;
        }
        return false;
    });
    /**
     * 報告
     */
    $('.js-submit-report').click(function() {
        if (confirm("報告を提出します。よろしいですか？")) {
            return true;
        }
        return false;
    });

    /**
     * 師長申込 申請
     */
    $('.js-apply-qualify').click(function() {
        $(this).parents('form')
            .attr({method: 'post', action: 'apply_qualify.php'})
            .append($('<input></input>').attr({type: 'hidden', name: 'id', value: $(this).data('id')}))
            .append($('<input></input>').attr({type: 'hidden', name: 'mode', value: $(this).data('mode')}))
            .submit();
    });

    /**
     * 師長申込 受付
     */
    $('.js-accept-qualify').click(function() {
        $(this).parents('form')
            .attr({method: 'post', action: 'accept_qualify.php'})
            .append($('<input></input>').attr({type: 'hidden', name: 'id', value: $(this).data('id')}))
            .append($('<input></input>').attr({type: 'hidden', name: 'mode', value: 'new'}))
            .append($('<input></input>').attr({type: 'hidden', name: 'target_emp_id', value: $(this).data('emp')}))
            .submit();
    });

    /**
     * 監査者設定
     */
    $('.js-accept-auditor').click(function() {
        $(this).parents('form')
            .attr({method: 'post', action: 'accept_auditor.php'})
            .append($('<input></input>').attr({type: 'hidden', name: 'id', value: $(this).data('id')}))
            .append($('<input></input>').attr({type: 'hidden', name: 'mode', value: 'new'}))
            .append($('<input></input>').attr({type: 'hidden', name: 'target_emp_id', value: $(this).data('emp')}))
            .submit();
    });

    /**
     * 合否判定
     */
    $('.js-accept-result').click(function() {
        $(this).parents('form')
            .attr({method: 'post', action: 'accept_result.php'})
            .append($('<input></input>').attr({type: 'hidden', name: 'id', value: $(this).data('id')}))
            .append($('<input></input>').attr({type: 'hidden', name: 'mode', value: 'new'}))
            .append($('<input></input>').attr({type: 'hidden', name: 'target_emp_id', value: $(this).data('emp')}))
            .submit();
    });

    /**
     * 総合評価
     */
    $('.js-accept-total').click(function() {
        $(this).parents('form')
            .attr({method: 'post', action: 'form_8edit_total.php'})
            .append($('<input></input>').attr({type: 'hidden', name: 'id', value: $(this).data('id')}))
            .append($('<input></input>').attr({type: 'hidden', name: 'mode', value: 'new'}))
            .append($('<input></input>').attr({type: 'hidden', name: 'target_emp_id', value: $(this).data('emp')}))
            .submit();
    });

    /**
     * レポート評価者設定
     */
    $('.js-accept-reporter').click(function() {
        $(this).parents('form')
            .attr({method: 'post', action: 'accept_reporter.php'})
            .append($('<input></input>').attr({type: 'hidden', name: 'id', value: $(this).data('id')}))
            .append($('<input></input>').attr({type: 'hidden', name: 'mode', value: 'new'}))
            .append($('<input></input>').attr({type: 'hidden', name: 'target_emp_id', value: $(this).data('emp')}))
            .submit();
    });

    /**
     * レポート評価
     */
    $('.js-accept-report').click(function() {
        $(this).parents('form')
            .attr({method: 'post', action: 'form_6assessment.php'})
            .append($('<input></input>').attr({type: 'hidden', name: 'id', value: $(this).data('id')}))
            .append($('<input></input>').attr({type: 'hidden', name: 'mode', value: 'new'}))
            .append($('<input></input>').attr({type: 'hidden', name: 'target_emp_id', value: $(this).data('emp')}))
            .submit();
    });

    /**
     * レポート評価
     */
    $('.js-accept-report-confirm').click(function() {
        $(this).parents('form')
            .attr({method: 'post', action: 'form_6assessment_confirm.php'})
            .append($('<input></input>').attr({type: 'hidden', name: 'id', value: $(this).data('id')}))
            .append($('<input></input>').attr({type: 'hidden', name: 'mode', value: 'new'}))
            .append($('<input></input>').attr({type: 'hidden', name: 'target_emp_id', value: $(this).data('emp')}))
            .submit();
    });

    /**
     * 様式呼び出し
     */
    $('.js-form-button').click(function() {
        $(this).parents('form')
            .attr({method: 'post', action: $(this).data('php')})
            .append($('<input></input>').attr({type: 'hidden', name: 'id', value: $(this).data('id')}))
            .append($('<input></input>').attr({type: 'hidden', name: 'mode', value: $(this).data('mode')}))
            .append($('<input></input>').attr({type: 'hidden', name: 'target_emp_id', value: $(this).data('emp')}))
            .submit();
    });

    /**
     * 様式印刷
     */
    $('.js-form-print').click(function() {
        $("#_js-print").remove();
        var form = $('<form id="_js-print" class="js-form-print"></form>');
        form.appendTo(document.body);

        form.append($('<input></input>').attr({type: 'hidden', name: 'id', value: $(this).data('id')}))
            .append($('<input></input>').attr({type: 'hidden', name: 'mode', value: 'print'}))
            .append($('<input></input>').attr({type: 'hidden', name: 'menu', value: $(this).data('menu')}))
            .append($('<input></input>').attr({type: 'hidden', name: 'target_emp_id', value: $(this).data('emp')}));
        form.attr({
            method: 'post',
            action: $(this).data('php'),
            target: $(this).data('target'),
            onsubmit: 'return openPDF(this)'});
        form.submit();
    });

    /**
     * 検索するボタン
     */
    $('button.js-form-search').click(function() {
        $(this).toggleClass('active');
        $($(this).data('target')).slideToggle();
    });

    /**
     * 数値のみ
     */
    $(".onlynum").attr({
        autocomplete: "off"
    }).css({
        "ime-mode": "disabled"
    }).keypress(function(event) {
        st = String.fromCharCode(event.which);
        if ("0123456789\b\r\t".indexOf(st, 0) < 0) {
            return false;
        }
        return true;
    }).bind('paste', function(e) {
        e.preventDefault();
    }).change(function() {
        var text = $(this).val();
        if (text.match(/[^0-9０-９]+/)) {
            // 数値以外があればクリア
            $(this).val("");
            return;
        }
        // 全角→半角
        var hen = text.replace(/[０-９]/g, function(s) {
            return String.fromCharCode(s.charCodeAt(0) - 0xFEE0);
        });
        $(this).val(hen);
    });

    /**
     * 入力欄のenterによりsubmitしてしまうのを抑止
     */
    $(document).on('keypress', 'input', function(ev) {
        if ((ev.which && ev.which === 13) || (ev.keyCode && ev.keyCode === 13)) {
            return false;
        } else {
            return true;
        }
    });

    /**
     * 戻る(_form_view)
     */
    $(document).on('click', '.btn_back', function() {
        if ($('#referer_url').val() !== "") {
            var form = $('#referer_url').parent("form");
            $('<input>').attr({
                type: 'hidden',
                id: 'ldr_return',
                name: 'ldr_return',
                value: '1'
            }).appendTo(form);

            form.attr('action', $('#referer_url').val()).submit();
        } else {
            history.back();
        }
    });

    /**
     * 戻る(新バージョン)
     */
    $('.js-btn-back').click(function() {
        if ($(this).data('back') !== "") {
            $('<form></form>')
                .attr({method: 'post', action: $(this).data('back')})
                .append($('<input>').attr({type: 'hidden', id: 'ldr_return', name: 'ldr_return', value: '1'}))
                .appendTo(document.body)
                .submit();
        } else {
            history.back();
        }
    });

    /**
     * 日付/時間コントロール関連初期化
     */
    $('.yearpicker').each(function() {
        var api = $(this).datetimepicker({
            format: 'yyyy',
            language: 'ja',
            viewMode: 2,
            minViewMode: 2,
            pickTime: false
        }).data('datetimepicker');
        api.widget.on('click', '.year', function() {
            if (!$(this).hasClass("old")) {
                api.hide();
            }
        });
    });

    $('.monthpicker').each(function() {
        var api = $(this).datetimepicker({
            format: 'yyyy-MM',
            language: 'ja',
            viewMode: 1,
            minViewMode: 1,
            pickTime: false
        }).data('datetimepicker');
        api.widget.on('click', '.month', function() {
            api.hide();
        });
    });

    $('.datepicker').each(datepicker_function);

    $('.timepicker').each(function() {
        var api = $(this).datetimepicker({
            format: 'hh:mm',
            pickDate: false,
            pickSeconds: false,
            language: 'ja',
            pickTime: true
        }).data('datetimepicker');
        api.widget.on('click', 'table.table-condensed', function() {
            api.hide();
            api.$element.trigger({type: "changeDate", date: api.getDate(), localDate: api.getLocalDate()});
        });
    });

    $('.datepicker,.monthpicker,.timepicker,.yearpicker').keyup(function(e) {
        if (e.keyCode === 46) {
            $(this).find('input').val('');
        }
    });

    $('.js-datetime-input').click(function() {
        $(this).parents('.dt-owner').find('i').trigger('click');
    });

    /**
     * Modalを閉じたときにキャッシュをクリアする
     */
    $('div.modal').on('hidden', function() {
        $(this).removeData('modal');
        $(this).find('.modal-body').empty();
    });

    /**
     * コメントModalを開いたときに既読にする
     */
    $('.js-comment-modal').on('click', function() {
        $(this).attr({rel: '', title: ''}).removeClass('btn-warning').addClass('btn-success');
        $(this).tooltip('destroy');
    });

    /**
     * Table sort
     */
    $('tbody.sortable').sortable({
        items: '> tr',
        handle: '.dragHandle',
        axis: 'y',
        opacity: 0.4,
        cursor: 'move',
        helper: function(r, tr) {
            var origin = tr.children();
            var helper = tr.clone();
            helper.children().each(function(index) {
                $(this).width(origin.eq(index).width());
            });
            return helper;
        }
    });

    /**
     * 職員名簿
     */
    $('.js-btn-emplist').click(function() {
        open_emplist($(this).data('id'));
    });

    /**
     * テキスト項目をトリムする
     */
    $('form').submit(function() {
        $(this).find(":text").each(function() {
            $(this).val(jQuery.trim($(this).val()));
        });
    });

    /**
     * textareaとadd-onの高さ合わせ
     */
    $('.input-prepend-multi').each(function() {
        var $textarea = $(this).find('textarea');
        var $span = $(this).find('span.add-on:eq(0)');
        var $height = $textarea.outerHeight()
            - (parseInt($span.css('padding-top'))
                + parseInt($span.css('padding-bottom'))
                + parseInt($span.css('margin-top') === 'auto' ? 0 : $span.css('margin-top'))
                + parseInt($span.css('margin-bottom') === 'auto' ? 0 : $span.css('margin-bottom'))
                + (parseInt($span.css('border-width')) * 2));
        $span.height($height);
    });

    // 指標検索
    var typeaheadItems = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: 'typeahead_items.php?query=%QUERY',
            replace: function(url, query) {
                return url.replace('%QUERY', query) + '&type=' + $(document.activeElement).data('item-type');
            }
        }
    });
    $('.js-typeahead-item').typeahead({
        minLength: 0,
        highLight: true
    }, {
        name: 'search',
        limit: 100,
        displayKey: 'value',
        template: '<div>{{value}}</div>',
        source: typeaheadItems.ttAdapter()
    });

    // 消しゴム
    $('.js-ldr-erase > a').on('click', function() {
        var $input = $(this).parent().parent().find('input, textarea');
        $input.val('').focus();
        if ($input.hasClass('js-typeahead-item')) {
            $input.typeahead('val', '');
        }
    });

    // textarea高さ調整
    $('body').on("input click keyup paste", "textarea.js-autoheight", function(evt) {
        if (evt.target.scrollHeight > evt.target.offsetHeight) {
            $(evt.target).height(evt.target.scrollHeight);
        } else if (evt.target.scrollHeight > $(evt.target).height() + 10) {
            var lineHeight = Number($(evt.target).css("lineHeight").split("px")[0]);
            while (true) {
                $(evt.target).height($(evt.target).height() - lineHeight);
                if (evt.target.scrollHeight > evt.target.offsetHeight) {
                    $(evt.target).height(evt.target.scrollHeight);
                    break;
                }
            }
        }
    }).click();

    // textarea高さ調整
    $('body').on('shown', 'a[data-toggle="tab"]', function() {
        $('textarea.js-autoheight').click();
    });

});

/**
 * datepicker_function
 * @returns {undefined}
 */
function datepicker_function() {
    var api = $(this).datetimepicker({
        format: 'yyyy-MM-dd',
        language: 'ja',
        pickTime: false
    }).data('datetimepicker');
    api.widget.on('click', 'td.day', function() {
        api.hide();
    });
}

/**
 * 職員選択呼び出し
 * @returns {undefined}
 */
var ctrl_id = "";
var childwin = null;
function open_emplist(id) {
    ctrl_id = id;
    var dx = screen.availWidth - 10;
    var wx = 720;
    var url = '../emplist/emplist.php?mode=career&item_id=' + id;
    childwin = window.open(url, 'emplist', 'left=' + (dx - wx) + ',top=0,width=' + wx + ',height=600,scrollbars=yes,resizable=yes');
    childwin.focus();
}

/**
 * 職員選択結果
 * @param {type} emp_id
 * @param {type} name
 * @returns {undefined}
 */
function add_target_list(emp_id, name) {
    $("#" + ctrl_id).val(name);
}

/**
 * console.log無効化(対IE)
 *
 **/
if (!('console' in window)) {
    window.console = {};
    window.console.log = function(str) {
        return str;
    };
}

/**
 * PDFウィンドウを開く
 * @param {type} f
 * @returns {undefined}
 */
function openPDF(f) {
    var h = window.screen.availHeight - 35;
    var w = window.screen.availWidth - 10;
    var option = "scrollbars=yes,left=0,top=0,width=" + w + ",height=" + h;
    window.open('about:blank', f.target, option);
    return true;
}

/**
 * メッセージを自動で閉じる
 */
window.setTimeout(function() {
    $(".js-alert-message").alert('close');
}, 10000);

/**
 * ランダムな文字列
 * @returns {String}
 */
function random_str() {
    var str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

    var p = '';
    for (var i = 0; i < 8; i++) {
        var rnd = Math.floor(Math.random() * str.length);
        p += str.charAt(rnd);
    }

    return p;
}