<?php

require_once 'class/Base.php';
require_once 'class/Assessment.php';
require_once 'class/AnalysisLadder.php';
require_once 'class/NoviceAssessment.php';
require_once 'class/NoviceAnalysisAssessment.php';
require_once 'class/Classmst.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}
$emp = $session->emp();

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_analysis($session->emp());

//----------------------------------------------------------
// object
//----------------------------------------------------------
$analysis = new Ldr_AnalysisLadder();
$as = new Ldr_NoviceAssessment($session->emp()->emp_id());
$assessment = new Ldr_NoviceAnalysisAssessment();

//----------------------------------------------------------
// year
//----------------------------------------------------------
$year_list = range(date('Y') + 1, 2014);
$year = $_POST['year'] ? $_POST['year'] : Ldr_Util::th_year();

//----------------------------------------------------------
// number
//----------------------------------------------------------
$date_list = $as->date_lists($year);
$number = $_REQUEST['number'] ? $_REQUEST['number'] : $date_list[0]['number'];
if (!is_numeric($number)) {
    header('Location: dashboard.php');
    exit;
}

//----------------------------------------------------------
// csv出力
//----------------------------------------------------------
if ($_POST['mode'] === 'csv') {
    $csv = to_sjis($assessment->export_csv($year, $number));
    $filename = "novice_analysis_assessment.csv";

    // output
    ob_clean();
    header("Content-Disposition: attachment; filename=$filename");
    header("Content-Type: application/octet-stream; name=$filename");
    header("Content-Length: " . strlen($csv));
    echo($csv);
    ob_end_flush();
    exit;
}

//----------------------------------------------------------
// list
//----------------------------------------------------------
$list = $assessment->lists($year, $number);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $session->emp());
$view->assign(Ldr_Util::get_common_info('novice_analysis_assessment'));
$view->assign('year', (int)$year);
$view->assign('year_list', $year_list);
$view->assign('date_list', $date_list);
$view->assign('lists', $list);

$view->display_with_fill('novice_analysis_assessment.tpl', array('fdat' => $_POST));
