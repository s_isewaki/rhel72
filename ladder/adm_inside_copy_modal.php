<?php

require_once 'class/Base.php';
require_once 'class/InsideMst.php';
//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_mst_inside($session->emp());

//----------------------------------------------------------
// オブジェクト
//----------------------------------------------------------
$mst = new Ldr_InsideMst();
$view = new Cmx_View('ladder/templates');

//----------------------------------------------------------
// year
//----------------------------------------------------------
$years = $mst->year_lists();

//----------------------------------------------------------
// data
//----------------------------------------------------------
$data = $mst->find_date($id);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
header('Cache-Control: no-cache, must-revalidate');
header("Cache-Control: post-check=0, pre-check=0", false);
header('Pragma: no-cache');

$view->assign('years', $years);
$view->assign($data);

$view->display_with_fill("modal/adm_inside_copy.tpl", array('fdat' => array('id' => $_GET['id'])));
