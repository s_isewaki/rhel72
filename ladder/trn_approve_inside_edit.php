<?php

require_once 'class/Base.php';
require_once 'class/Inside.php';
require_once 'class/InsideMst.php';
require_once 'class/Guideline.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('inside_approve');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_ladder($emp);

//----------------------------------------------------------
// object
//----------------------------------------------------------
$inside = new Ldr_Inside($emp->emp_id());
$insidemst = new Ldr_InsideMst();
$gl = new Ldr_Guideline();

//----------------------------------------------------------
// 保存
//----------------------------------------------------------
if ($_POST['btn_save'] === "save") {
    try {
        $inside->save_approve($_POST);
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }
    $_SESSION['ldr_return'] = true;
    header('Location: ' . Ldr_Util::get_referer_url());
    exit;
}
else if ($_POST['btn_save'] === "cancel") {
    try {
        $inside->cancel_approve($_POST);
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }
    $_SESSION['ldr_return'] = true;
    header('Location: ' . Ldr_Util::get_referer_url());
    exit;
}

//----------------------------------------------------------
// データの取得
//----------------------------------------------------------
if (!isset($_POST['id'])) {
    cmx_log(basename($_SERVER["PHP_SELF"]) . " parameter error");
    header('Location: dashboard.php');
    exit;
}

$data = $inside->find($_POST['id']);
$mst = $insidemst->find_date($data['inside_id'], $data['inside_date_id']);
$gp1 = $gl->find('group1', $data['guideline_group1']);

$target_emp = new Ldr_Employee($data['emp_id']);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $emp);
$view->assign(array(
    'trn' => $data,
    'mst' => $mst,
    'gp1' => $gp1,
    'target_emp' => $target_emp,
    'hd_title' => '院内研修 承認',
));
$view->assign(Ldr_Util::get_common_info($_SESSION['menu']));
$view->display('trn_approve_inside_edit.tpl');
