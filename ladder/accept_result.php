<?php

require_once 'class/Base.php';
require_once 'class/Apply.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_ladder($emp);

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
Ldr_Util::session_init('accept_result');

if ($_POST['mode'] === "new" || $_POST['mode'] === "retry" || $_POST['mode'] === "cancel") {
    // 初期情報を退避
    $_SESSION['id'] = $_POST['id'];
    $_SESSION['target_emp_id'] = $_POST['target_emp_id'];
    $_SESSION['rtn_apply_view'] = Ldr_Util::get_referer_url();
}
else {
    $_POST['id'] = $_SESSION['id'];
    $_POST['target_emp_id'] = $_SESSION['target_emp_id'];
}

//----------------------------------------------------------
// IDなければダッシュボード
//----------------------------------------------------------
if (empty($_POST['id'])) {
    header("Location: dashboard.php");
    exit;
}

//----------------------------------------------------------
// オブジェクト
//----------------------------------------------------------
$apply = new Ldr_Apply(array('apply_emp_id' => $_POST["target_emp_id"], 'accept_emp_id' => $emp->emp_id(), 'apply_id' => $_POST['id'], 'mode' => 'accept'));
$target_emp = new Ldr_Employee($_POST["target_emp_id"]);
$ladder = $apply->ladder();
$effective_post = Ldr_Util::use_tiket();

//----------------------------------------------------------
// 設定
//----------------------------------------------------------
if ($_POST['form_submit'] === 'btn_completed') {
    $error = $apply->validate_result($_POST);
    if (empty($error)) {
        try {
            if ($effective_post) {
                $apply->accept_result($_POST);
            }
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
        }
        header('Location:' . Ldr_Util::get_referer_url());
        exit;
    }
}

//----------------------------------------------------------
// form
//----------------------------------------------------------
if ($ladder['config']['flow'] === '1') {
    $form8 = $apply->find_form8();
}
else {
    $apply2 = new Ldr_Apply(array('apply_emp_id' => $_POST["target_emp_id"], 'accept_emp_id' => 'total', 'apply_id' => $_POST['id'], 'mode' => 'accept'));
    $form8 = $apply2->find_form7();
}

//----------------------------------------------------------
// result
//----------------------------------------------------------
$result = isset($_POST['result']) ? $_POST['result'] : $form8['level'];
if ($result === '1') {
    $result_date = isset($_POST['result_date']) ? $_POST['result_date'] : date('Y-m-d');
    $approved_number = $_POST['approved_number'];
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign(Ldr_Util::get_common_info('accept', array('referer_url' => $_SESSION['rtn_apply_view'])));
$view->assign('emp', $session->emp());
$view->assign('target_emp', $target_emp);
$view->assign('apply', $apply);
$view->assign('ldr', $ladder);
$view->assign('form1', $apply->find_form1($_POST['id']));
$view->assign('id', $_POST['id']);
$view->assign('menu', 'accept');
$view->assign('mode', $_POST['mode']);
$view->assign('target_emp_id', $target_emp->emp_id());
$view->assign('result', $result);
$view->assign('result_date', $result_date);
$view->assign('approved_number', $approved_number);
$view->assign($apply->form_updated_on($_POST['id']));
$view->assign('error', $error);
$view->display('accept_result.tpl');
