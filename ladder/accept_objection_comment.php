<?php

require_once 'class/Base.php';
require_once 'class/Apply.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_committee($emp);

//----------------------------------------------------------
// Object
//----------------------------------------------------------
$apply = new Ldr_Apply(array('apply_emp_id' => $emp->emp_id(), 'apply_id' => $_POST['ldr_apply_id']));

//----------------------------------------------------------
// 保存
//----------------------------------------------------------
try {
    $apply->accept_objection($_POST);
}
catch (Exception $e) {
    cmx_log($e->getMessage());
    js_error_exit();
}
