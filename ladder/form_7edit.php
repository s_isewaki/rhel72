<?php

require_once 'class/Base.php';
require_once 'class/Validation.php';
require_once 'class/Apply.php';
require_once 'class/Assessment.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_ladder($emp);

//----------------------------------------------------------
// object
//----------------------------------------------------------
$apply = new Ldr_Apply(array('apply_emp_id' => $_POST["target_emp_id"], 'accept_emp_id' => $emp->emp_id(), 'apply_id' => $_POST['id']));
$ladder = $apply->ladder();
$target_emp = new Ldr_Employee($_POST["target_emp_id"]);
$as = new Ldr_Assessment($_POST["target_emp_id"]);
$effective_post = Ldr_Util::use_tiket();
$conf = new Ldr_Config();

//----------------------------------------------------------
// インポート
//----------------------------------------------------------
if ($effective_post && $_POST['btn_save'] === 'import') {
    try {
        $apply->import_form7($ladder['guideline_revision']);
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }
}

//----------------------------------------------------------
// 様式7-1 保存
//----------------------------------------------------------
if ($effective_post && $_POST['btn_save'] === '71_save') {
    try {
        $apply->save_form71($_POST);
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }
}

//----------------------------------------------------------
// 様式7-2 保存
//----------------------------------------------------------
else if ($effective_post && $_POST['btn_save'] === '72_save') {
    try {
        $apply->save_form72($_POST);
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }
}

//----------------------------------------------------------
// 様式7-1 自動保存
//----------------------------------------------------------
else if ($_POST['save'] === 'form71') {
    try {
        $apply->save_form71($_POST, 'auto');
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }
    exit;
}

//----------------------------------------------------------
// 様式7-2 自動保存
//----------------------------------------------------------
else if ($_POST['save'] === 'form72') {
    try {
        $apply->save_form72($_POST, 'auto');
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }
    exit;
}

//----------------------------------------------------------
// 様式7データ
//----------------------------------------------------------
$find_data = $apply->find_form7();
unset($_POST['save']);
if (!empty($find_data)) {
    $fdat = array_merge((array)$find_data, (array)$_POST);
}
else {
    $fdat = $_POST;
}

//----------------------------------------------------------
// 領域リスト
//----------------------------------------------------------
$hospital_type = $conf->hospital_type();
if ($hospital_type === '1') {
    $form7 = true;
}
else {
    $form7 = false;
}
$group1_list = $as->lists('group1', array('form7' => $form7, 'guideline_revision' => $apply->guideline_revision()));

//----------------------------------------------------------
// デフォルト領域
//----------------------------------------------------------
$active = $_POST['active'] ? $_POST['active'] : $group1_list[0]['guideline_group1'];
unset($_POST['active']);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign(Ldr_Util::get_common_info($_POST['menu'], array('referer_url' => $_POST['owner_url'])));
$view->assign('emp', $emp);
$view->assign('target_emp', $target_emp);
$view->assign('active', $active);
$view->assign('group1_list', $group1_list);
$view->assign('assessment_list', $apply->assessment_list());
$view->assign('padleft', count($ladder['config']['select']) <= 2 ? '60px' : count($ladder['config']['select']) * 34 + 10 . 'px');
$view->assign('apply', $apply);
$view->assign('level', $apply->level());
$view->assign('ldr', $ladder);
$view->display_with_fill('form_7edit.tpl', array('fdat' => $fdat));
