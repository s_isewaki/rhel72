<?php

require_once 'class/Base.php';
require_once 'class/Inside.php';
require_once 'class/Guideline.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
    exit;
}
//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('inside_apply');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_ladder($emp);

//----------------------------------------------------------
// ID
//----------------------------------------------------------
$id = $_REQUEST['id'];
if (!is_numeric($id)) {
    header('Location: dashboard.php');
    exit;
}
$_POST['id'] = $_REQUEST['id'];

//----------------------------------------------------------
// Obj
//----------------------------------------------------------
$inside = new Ldr_Inside($emp->emp_id());
$gl = new Ldr_Guideline();
$conf = new Ldr_Config();
$effective_post = Ldr_Util::use_tiket();

//----------------------------------------------------------
// 保存
//----------------------------------------------------------
if ($effective_post && $_POST['btn_save'] === 'save') {

    // バリデーション
    $error = $inside->validate_apply($_POST);

    // エラーが無ければ保存
    if (empty($error)) {
        try {
            $inside->save_apply($_POST);
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
        }

        $_SESSION['ldr_return'] = true;
        header('Location:trn_inside.php');
        exit;
    }
}
if ($_POST['mode'] === 'apply' || $_POST['mode'] === 'edit') {
    $_SESSION['edit'] = $_POST['mode'];
}
//----------------------------------------------------------
// データの取得
//----------------------------------------------------------
$data = $inside->find_course_date($id);
$gp1 = $gl->find('group1', $data['guideline_group1']);

$cnt = array();
$apply = array();
foreach ($data['dates'] as $value) {
    $cnt[$value['number']] ++;
    $apply[$value['number']] += $value['applied'];
}

$approver = $emp->approver();
$data2 = array(
    'edit' => $_SESSION['edit'],
    'cnt' => $cnt,
    'apply' => $apply,
    'gp1' => $gp1,
    'need_approve' => $data['approve'],
    'approver' => empty($approver) ? null : $approver,
    'reception_text' => Ldr_Const::get_inside_reception(),
);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $emp);
$view->assign($data);
$view->assign($data2);
$view->assign(Ldr_Util::get_common_info('inside_course'));
$view->assign('ojt_name', $conf->ojt_name());

if (empty($error)) {
    $view->display_with_fill('trn_inside_apply.tpl', array('fdat' => $_POST));
}
else {
    $view->assign('error', $error);
    $view->display_with_fill('trn_inside_apply.tpl', array('fdat' => $_POST));
}