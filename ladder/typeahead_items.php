<?php

require_once 'class/Base.php';
require_once 'class/Item.php';

//----------------------------------------------------------
// Obj
//----------------------------------------------------------
$item = new Ldr_Item();

//----------------------------------------------------------
// param
//----------------------------------------------------------
$type = $_GET['type'];
$query = $_GET['query'];

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
header('X-Content-Type-Options: nosniff');
header('Cache-Control: no-cache, must-revalidate');
header("Cache-Control: post-check=0, pre-check=0", false);
header('Pragma: no-cache');
print cmx_json_encode(
        $item->typeahead($type, $query)
);
