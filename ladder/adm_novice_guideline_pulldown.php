<?php

require_once 'class/Base.php';
require_once 'class/NoviceGuideline.php';

//----------------------------------------------------------
// オブジェクト
//----------------------------------------------------------
$gl = new Ldr_NoviceGuideline();

//----------------------------------------------------------
// データ
//----------------------------------------------------------
switch ($_POST['mode']) {

    case 'revision':
        $list = $gl->lists('revision');
        break;

    case 'group1':
        $list = $gl->lists('group1', $_POST);
        break;

    case 'group2':
        $list = $gl->lists('group2', $_POST);
        break;

    case 'guideline':
        $list = $gl->lists('guideline', $_POST);
        break;

    default:
        js_error_exit();
        break;
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
header('Cache-Control: no-cache, must-revalidate');
header("Cache-Control: post-check=0, pre-check=0", false);
header('Pragma: no-cache');
print cmx_json_encode($list);
exit;
