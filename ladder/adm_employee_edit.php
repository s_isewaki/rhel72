<?php

require_once 'class/Base.php';
require_once 'class/Employee.php';
require_once 'class/Ladder.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('adm_employee');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_admin($session->emp());

//----------------------------------------------------------
// Object
//----------------------------------------------------------
$employee = new Ldr_Employee();
$ladder = new Ldr_Ladder();

//----------------------------------------------------------
// 保存
//----------------------------------------------------------
if (isset($_POST['btn_save'])) {
    // バリデーション
    $error = $employee->validate($_POST);

    // エラーが無ければ保存
    if (empty($error)) {
        // 保存処理
        try {
            $employee->save($_POST);
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
            exit;
        }

        // 一覧に戻る
        $_SESSION['ldr_return'] = true;
        header('Location: adm_employee.php');
        exit;
    }
}

//----------------------------------------------------------
// 編集
//----------------------------------------------------------
if (isset($_GET["emp_id"]) and $_GET["emp_id"] !== "") {
    try {
        $fdat = $employee->find($_GET["emp_id"]);
        $_SESSION['emp_id'] = $_GET["emp_id"];
    }
    catch (Exception $e) {
        header('Location: dashboard.php');
        exit;
    }
}
else if (!empty($error)) {
    try {
        $fdat = $employee->find($_SESSION['emp_id']);
    }
    catch (Exception $e) {
        header('Location: dashboard.php');
        exit;
    }
}
else {
    header('Location: dashboard.php');
    exit;
}

if (strstr($_SERVER['HTTP_REFERER'], 'ldr_employee.php')) {
    $_SESSION['srh_keyword'] = $_POST['srh_keyword'];
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $session->emp());
$view->assign('staff', $employee);
$view->assign('ladder', $ladder);
$view->assign(Ldr_Util::get_common_info('adm_employee'));

if (empty($error)) {
    $view->display_with_fill('adm_employee_edit.tpl', array('fdat' => $fdat));
}
else {
    $view->assign('error', $error);
    $view->display_with_fill('adm_employee_edit.tpl', array('fdat' => $_POST));
}
