<?php

require_once 'class/Base.php';
require_once 'class/Validation.php';
require_once 'class/Apply.php';
require_once 'class/Assessment.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_ladder($emp);

//----------------------------------------------------------
// object
//----------------------------------------------------------
$apply = new Ldr_Apply(array('apply_emp_id' => $_POST["target_emp_id"], 'accept_emp_id' => $emp->emp_id(), 'apply_id' => $_POST['id']));
$ladder = $apply->ladder();
$target_emp = new Ldr_Employee($_POST["target_emp_id"]);
$effective_post = Ldr_Util::use_tiket();
$conf = new Ldr_Config();

//----------------------------------------------------------
// 確定保存
//----------------------------------------------------------
if ($effective_post && $_POST['btn_save'] === 'completed') {
    // 保存してから
    try {
        $apply->save_report_assessment($_POST);
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }

    // 未入力チェック
    $error = $apply->validate_report_assessment();

    // エラーが無ければ確定
    if (empty($error)) {
        try {
            $apply->update_report_completed();
            $completed = true;
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
        }
    }
    $active = 'assessment';
}

//----------------------------------------------------------
// 自動保存
//----------------------------------------------------------
else if ($_POST['save'] === 'ajax') {
    try {
        $apply->save_report_assessment($_POST, 'auto');
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }
    exit;
}

//----------------------------------------------------------
// fdat
//----------------------------------------------------------
$assessment = $apply->report_assessment();
$fdat = array_merge((array)$assessment, (array)$_POST);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign(Ldr_Util::get_common_info($_POST['menu'], array('referer_url' => $_POST['owner_url'])));
$view->assign('emp', $emp);
$view->assign('target_emp', $target_emp);
$view->assign('assessment', $assessment);
$view->assign('padleft', count($ladder['config']['report_select']) <= 2 ? '60px' : count($ladder['config']['report_select']) * 34 + 10 . 'px');
$view->assign('apply', $apply);
$view->assign('ldr', $ladder);
$view->assign('level', $apply->level());
$view->assign('form6', $apply->find_form6());
$view->assign('active', $active);
$view->assign('completed', $completed);
$view->assign('error', $error);
$view->display_with_fill('form_6assessment.tpl', array('fdat' => $fdat));
