<?php

require_once 'class/Base.php';
require_once 'class/Inside.php';
require_once 'class/InsideMst.php';
require_once 'class/Classmst.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('accept_inside');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_inside($emp);

//----------------------------------------------------------
// object
//----------------------------------------------------------
$inside = new Ldr_Inside($emp->emp_id());
$mst = new Ldr_InsideMst();
$classmst = new Ldr_Classmst();

$effective_post = Ldr_Util::use_tiket();

//----------------------------------------------------------
// 保存
//----------------------------------------------------------
if ($_POST['btn_save'] === "save") {
    try {
        if ($effective_post) {
            $inside->save_accept($_POST);
        }
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }
    $_SESSION['ldr_return'] = true;
    header('Location: ' . Ldr_Util::get_referer_url());
    exit;
}

if ($effective_post) {
    switch ($_POST['mode2']) {
        // バリデーション
        case 'validate':
            $euc = Ldr_Util::from_utf8($_POST);
            $error = $mst->validate_date_status($euc);
            header('X-Content-Type-Options: nosniff');
            print cmx_json_encode(array(
                'error' => !empty($error),
                'msg' => $error,
                'mode' => $_POST['mode'],
            ));
            exit;
            break;

        // 保存
        case 'save':
            $error = $mst->validate_date_status($_POST);
            // エラーが無ければ保存
            if (empty($error)) {
                // 保存処理
                try {
                    $mst->change_date_status($_POST['date_id'], $_POST['status']);
                }
                catch (Exception $e) {
                    cmx_log($e->getMessage());
                    js_error_exit();
                }
            }
            else {
                js_error_exit();
            }
            break;
    }
}
//----------------------------------------------------------
// データの取得
//----------------------------------------------------------
if (!isset($_POST['id'])) {
    cmx_log(basename($_SERVER["PHP_SELF"]) . " parameter error");
    header('Location: dashboard.php');
    exit;
}

$id = explode('_', $_POST['id']);
$mst_date = $mst->find_date($id[0], $id[1]);

$cond = array(
    'id' => $id[1],
    'srh_class1' => $_POST['srh_class1'],
    'srh_class3' => $_POST['srh_class3'],
    'srh_status' => $_POST['srh_status'],
);

$fdat = array(
    'id' => $_POST['id'],
    'srh_class1' => $_POST['srh_class1'],
    'srh_status' => $_POST['srh_status'],
);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $emp);
$view->assign('trn', $mst_date);
$view->assign('date_id', $id[1]);
$view->assign('list', $inside->lists_accept_by_date($cond));
$view->assign('class_list', $classmst->lists());
$view->assign('class12_list', $classmst->class12_lists());
$view->assign('srh_class3', $_POST['srh_class3']);

$view->assign(Ldr_Util::get_common_info($_SESSION['menu'], array('referer_url' => 'trn_accept_inside.php')));
$view->display_with_fill('trn_accept_inside_edit.tpl', array('fdat' => $fdat));
