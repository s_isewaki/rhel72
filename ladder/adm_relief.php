<?php

require_once 'class/Base.php';
require_once 'class/Relief.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('releif');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_mst_relief($session->emp());

//----------------------------------------------------------
// Object
//----------------------------------------------------------
$relief = new Ldr_Relief();
$effective_post = Ldr_Util::use_tiket();

//----------------------------------------------------------
// 処理
//----------------------------------------------------------
if ($effective_post) {
    switch ($_POST['mode2']) {
        // バリデーション
        case 'validate':
            $euc = Ldr_Util::from_utf8($_POST);
            $error = $relief->validate($_POST['mode'], $euc);
            header('X-Content-Type-Options: nosniff');
            print cmx_json_encode(array(
                'error' => !empty($error),
                'msg' => $error,
                'mode' => $_POST['mode'],
            ));
            exit;
            break;

        // 保存
        case 'save':
            $error = $relief->validate($_POST['mode'], $_POST);

            // エラーが無ければ保存
            if (empty($error)) {
                // 保存処理
                try {
                    $relief->save($_POST['mode'], $_POST);
                }
                catch (Exception $e) {
                    cmx_log($e->getMessage());
                    js_error_exit();
                }
            }
            else {
                js_error_exit();
            }
            break;

        // 削除
        case 'delete':
            $relief->delete($_POST['mode'], $_POST['id'], $_POST['unit']);
            break;

        // 並べ替え
        case 'sort':
            $relief->sort($_POST['mode'], $_POST);
            $completed = true;
            break;
    }
}

$lists = $relief->lists('subject');
if ($_POST['mode'] === "contents" && empty($_POST['relief_id'])) {
    $_POST['relief_id'] = $_POST['id'];
}

if (empty($_POST['relief_id']) && !empty($lists)) {
    $_POST['relief_id'] = $lists[0]['relief_id'];
}
if (!empty($_POST['relief_id'])) {
    $lists_contents = $relief->lists('contents', $_POST['relief_id']);
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $session->emp());
$view->assign(Ldr_Util::get_common_info('adm_relief'));
$view->assign('lists', $lists);
$view->assign('lists_contents', $lists_contents);
$view->assign('completed', $completed);
$view->assign('active', $_POST['mode'] ? $_POST['mode'] : 'subject');

$view->display_with_fill('adm_relief.tpl', array('fdat' => $_POST));
