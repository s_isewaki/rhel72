<?php

require_once 'class/Base.php';
require_once 'class/Relief.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('accept_relief');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_relief($emp);

//----------------------------------------------------------
// object
//----------------------------------------------------------
$relief = new Ldr_Relief($emp->emp_id());

//----------------------------------------------------------
// 保存
//----------------------------------------------------------
if ($_POST['btn_save'] === "save") {
    try {
        $relief->save('accept', $_POST);
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }
    $_SESSION['ldr_return'] = true;
    header('Location: ' . Ldr_Util::get_referer_url());
    exit;
}

//----------------------------------------------------------
// データの取得
//----------------------------------------------------------
if (!isset($_POST['id'])) {
    cmx_log(basename($_SERVER["PHP_SELF"]) . " parameter error");
    header('Location: dashboard.php');
    exit;
}

$id = explode('_', $_POST['id']);
$_POST['relief_id'] = $id[0];
$_POST['unit'] = $id[1];
$_POST['training_day'] = $id[2];

$data = array(
    'sb' => $relief->find('subject', $_POST['relief_id']),
    'ct' => $relief->find('contents', $_POST['relief_id'], $_POST['unit']),
    'class_list' => $relief->lists('accept_class', null, null, null, $_POST),
    'list' => $relief->lists('accept_emp', null, null, null, $_POST),
);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $emp);
$view->assign($data);
$view->assign(Ldr_Util::get_common_info('taccept_relief'));
$view->display_with_fill('trn_accept_relief_edit.tpl', array('fdat' => $_POST));
