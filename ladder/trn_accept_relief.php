<?php

require_once 'class/Base.php';
require_once 'class/Pager.php';
require_once 'class/Relief.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('accept_relief');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_relief($emp);

//----------------------------------------------------------
// object
//----------------------------------------------------------
$relief = new Ldr_Relief($emp->emp_id());
$perPage = Ldr_Const::SRH_PERPAGE;
$view = new Cmx_View('ladder/templates');

//----------------------------------------------------------
// 印刷
//----------------------------------------------------------
if ($_POST['mode'] === 'print') {
    if (!isset($_POST['id'])) {
        cmx_log(basename($_SERVER["PHP_SELF"]) . " parameter error");
        header('Location: dashboard.php');
        exit;
    }

    require_once 'class/PDF.php';

    $id = explode('_', $_POST['id']);
    $_POST['relief_id'] = $id[0];
    $_POST['unit'] = $id[1];
    $_POST['training_day'] = $id[2];
    $cond = array_merge((array) $_POST, array('list_type' => 'report'));
    $data = array(
        'sb' => $relief->find('subject', $_POST['relief_id']),
        'ct' => $relief->find('contents', $_POST['relief_id'], $_POST['unit']),
        'list' => $relief->lists('accept_emp', null, null, null, $cond),
    );
    $view->assign('roman_num', Ldr_Const::get_roman_num());
    $view->assign($data);
    $pdf = new Ldr_PDF('P', 'mm', 'A4', true, 'UTF-8', false);
    $pdf->setHeaderTitle('救護員研修受付');
    $pdf->setPrinter($session->emp());
    $pdf->AddPage();
    $html = $view->output_with_fill('print/trn_accept_relief.tpl', array('fdat' => $_POST));
    $pdf->writeHTML(to_utf8($html), true, 0, false, 0);
    $pdf->Output("trn_accept_relief.pdf", "I");
    exit;
}
//----------------------------------------------------------
// 遷移状態判別
//----------------------------------------------------------
// ページャーから移動
if (Ldr_Pager::is_pager($_POST['move_pager'])) {
    //検索条件を取得
    $_SESSION['page_no'] = Ldr_Pager::get_page_no($_POST['page_no']);
    $srh_condition = $_SESSION['srh_condition'];
}
// 検索ボタン
else if ($_POST['btn_search']) {
    $srh_condition['srh_nonaccept'] = $_POST['srh_nonaccept'];
    $srh_condition["srh_keyword"] = $_POST['srh_keyword'];
    $srh_condition["srh_start_date"] = $_POST['srh_start_date'];
    $srh_condition["srh_last_date"] = $_POST['srh_last_date'];

    $_POST["page_no"] = 1;

    // 検索条件を設定
    $_SESSION['srh_condition'] = $srh_condition;
}
// 編集画面から戻ってきた場合
else if (isset($_POST['ldr_return']) || isset($_SESSION['ldr_return'])) {
    // ページ番号の復旧
    $_POST["page_no"] = $_SESSION['page_no'];

    // 検索条件の復旧
    $srh_condition = $_SESSION['srh_condition'];
}
// 外部から移動してきた場合
else {
    // セッション情報の初期化
    $_SESSION = array();
    $_POST["page_no"] = 1;
    $_SESSION['page_no'] = 1;
    $_SESSION['srh_condition'] = null;
    $srh_condition = null;

    if (isset($_REQUEST['status'])) {
        $srh_condition['srh_nonaccept'] = '1';
    }

    // 過去のデータがすべて出てしまうので検索条件を設定
    $srh_condition["srh_start_date"] = date("Y-m-d", strtotime("-1 month"));
    $_SESSION['srh_condition'] = $srh_condition;
}
$_SESSION['ldr_return'] = null;
$fdat = array_merge((array) $_POST, (array) $srh_condition);

//----------------------------------------------------------
// データ
//----------------------------------------------------------
$dataCount = $relief->count('accept', $srh_condition);
$current = Ldr_Pager::get_page_no($_POST['page_no'], $dataCount, $perPage);
$start = Ldr_Pager::calc_page_index($current, $perPage);
$list = $relief->lists('accept', null, $start, $perPage, $srh_condition);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view->assign('emp', $emp);
$view->assign('list', $list);
$view->assign(Ldr_Util::get_common_info('taccept_relief'));
$view->assign(Ldr_Pager::get_pager_info($_POST['page_no'], $dataCount, $perPage));
$view->display_with_fill('trn_accept_relief.tpl', array('fdat' => $fdat));
