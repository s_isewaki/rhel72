<?php

require_once 'class/Base.php';
require_once 'class/Apply.php';
require_once 'class/Validation.php';
require_once 'class/Log.php';
require_once 'class/Appraiser.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_ladder($emp);

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
Ldr_Util::session_init('accept_qualify');

// 初期情報を退避
if ($_POST['mode'] === "new" || $_POST['mode'] === "retry" || $_POST['mode'] === "cancel") {
    $_SESSION['id'] = $_POST['id'];
    $_SESSION['target_emp_id'] = $_POST['target_emp_id'];
    $_SESSION['rtn_apply_view'] = Ldr_Util::get_referer_url();
}
else {
    $_POST['id'] = $_SESSION['id'];
    $_POST['target_emp_id'] = $_SESSION['target_emp_id'];
}

//----------------------------------------------------------
// IDなければダッシュボード
//----------------------------------------------------------
if (empty($_POST['id'])) {
    header("Location: dashboard.php");
    exit;
}

//----------------------------------------------------------
// Object
//----------------------------------------------------------
$apply = new Ldr_Apply(array('apply_emp_id' => $_POST["target_emp_id"], 'accept_emp_id' => $emp->emp_id(), 'apply_id' => $_POST['id'], 'mode' => 'accept'));
$target_emp = new Ldr_Employee($_POST["target_emp_id"]);
$effective_post = Ldr_Util::use_tiket();
$ladder = $apply->ladder();

//----------------------------------------------------------
// 受付
//----------------------------------------------------------
if ($_POST['form_submit'] === 'save') {
    $error = null;
    if ($ladder['config']['meeting'] !== '1' || $ladder['config']['appr'] !== '1') {
        // バリデーション
        $error = $apply->validate_accept_qualify($_POST);
    }
    // エラーが無ければ保存
    if (empty($error)) {
        try {
            if ($effective_post) {
                $apply->accept_qualify($_POST);
            }
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
        }

        header('Location:' . Ldr_Util::get_referer_url());
        exit;
    }
    $data = $_POST;
}

//----------------------------------------------------------
// 差し戻し
//----------------------------------------------------------
else if ($_POST['form_submit'] === 'back') {
    // バリデーション
    $error = $apply->validate_comment($_POST);

    // エラーが無ければ
    if (empty($error)) {
        try {
            if ($effective_post) {
                $apply->accept_qualify_back($_POST);
            }
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
        }
        header('Location:' . Ldr_Util::get_referer_url());
        exit;
    }
}
//----------------------------------------------------------
// 新規登録
//----------------------------------------------------------
else {
    $data = array(
        'level' => ($emp->level() > 0 ? intval($emp->level()) + 1 : 1),
        'apply_date' => date("Y-m-d"),
    );

    if (Validation::is_date($emp->nurse_date())) {
        $diff = Ldr_Util::diff_th_year($emp->nurse_date(), date("Y-m-d"));
        $data['career'] = $diff < 0 ? '' : $diff;
    }
    if (Validation::is_date($emp->hospital_date())) {
        $diff = Ldr_Util::diff_th_year($emp->hospital_date(), date("Y-m-d"));
        $data['career_hospital'] = $diff < 0 ? '' : $diff;
    }
    if (Validation::is_date($emp->ward_date())) {
        $diff = Ldr_Util::diff_th_year($emp->ward_date(), date("Y-m-d"));
        $data['career_ward'] = $diff < 0 ? '' : $diff;
    }
    // メニューを出さない
    unset($_POST['owner_url']);
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign(Ldr_Util::get_common_info('accept', array('referer_url' => $_SESSION['rtn_apply_view'])));
$view->assign('emp', $session->emp());
$view->assign('target_emp', $target_emp);
$view->assign('apply', $apply);
$view->assign('ldr', $ladder);
$view->assign('level', $apply->level());
$view->assign('form1', $apply->find_form1($_POST['id']));
$view->assign('id', $_POST['id']);
$view->assign('menu', 'accept');
$view->assign('mode', $_POST['mode']);
$view->assign('appraiser', $_POST['appraiser']);
$view->assign('target_emp_id', $target_emp->emp_id());
$view->assign($apply->form_updated_on($_POST['id']));
$view->assign('error', $error);
$view->display('accept_qualify.tpl');
