<?php

require_once 'class/Base.php';
require_once 'class/Guideline.php';

//----------------------------------------------------------
// Obj
//----------------------------------------------------------
$gl = new Ldr_Guideline();

//----------------------------------------------------------
// query
//----------------------------------------------------------
$query = $_GET['query'];

//----------------------------------------------------------
// list
//----------------------------------------------------------
$list = $gl->full_lists($query);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
header('X-Content-Type-Options: nosniff');
header('Cache-Control: no-cache, must-revalidate');
header("Cache-Control: post-check=0, pre-check=0", false);
header('Pragma: no-cache');
print cmx_json_encode($list);
