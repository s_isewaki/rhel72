<?php

require_once 'class/Base.php';
require_once 'class/Assessment.php';
require_once 'class/Ladder.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_ladder($session->emp());

//----------------------------------------------------------
// オブジェクト
//----------------------------------------------------------
$as = new Ldr_Assessment();
$view = new Cmx_View('ladder/templates');
$conf = new Ldr_Config();
$ladder = new Ldr_Ladder();

//----------------------------------------------------------
// バリデーション
//----------------------------------------------------------
$group1 = $as->find('group1', $_GET['group1']);
if (empty($group1)) {
    header('dashboard.php');
    exit;
}

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('assessment');

//----------------------------------------------------------
// ladder
//----------------------------------------------------------
$ladder_id = $_SESSION['ladder_id'];
$this_ladder = $ladder->find($ladder_id);
$revision = $this_ladder['guideline_revision'];
$assessment_select = $this_ladder['config']['select'];

//----------------------------------------------------------
// group1
//----------------------------------------------------------
$view->assign('group1', $group1);

//----------------------------------------------------------
// group2
//----------------------------------------------------------
$group2_list = $as->group2_lists(array(
    'revision' => $revision,
    'level' => $_GET['level'],
    'group1' => $_GET['group1'],
    ));
$view->assign('group2_list', $group2_list);

//----------------------------------------------------------
// assessment
//----------------------------------------------------------
$assessment = $as->assessment(array(
    'emp_id' => $session->emp()->emp_id(),
    'level' => $_GET['level'],
    'group1' => $_GET['group1'],
    'year' => $_GET['year'],
    'revision' => $revision,
    ));
$view->assign('assessment', $assessment);

//----------------------------------------------------------
// assign
//----------------------------------------------------------
$view->assign('year', $_GET['year']);
$view->assign('level', $_GET['level']);
$view->assign('assessment_select', $assessment_select);
$view->assign('padleft', count($assessment_select) <= 2 ? '60px' : count($assessment_select) * 34 + 10 . 'px');
$view->assign('active', $_GET['active'] ? $_GET['active'] : "g2-{$group2_list[0]['guideline_group2']}");

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
header('Cache-Control: no-cache, must-revalidate');
header("Cache-Control: post-check=0, pre-check=0", false);
header('Pragma: no-cache');
$view->display("assessment_tab.tpl");
