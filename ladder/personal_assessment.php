<?php

require_once 'class/Base.php';
require_once 'class/Assessment.php';
require_once 'class/Employee.php';
require_once 'class/Ladder.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_analysis($session->emp());

//----------------------------------------------------------
// Object
//----------------------------------------------------------
$emp = $session->emp();
$employee = new Ldr_Employee();
$as = new Ldr_Assessment($emp->emp_id());
$ladder = new Ldr_Ladder();

//----------------------------------------------------------
// 版数
//----------------------------------------------------------
if ($_POST['ladder_id']) {
    $ladder_id = $_POST['ladder_id'];
    $this_ladder = $ladder->find($_POST['ladder_id']);
}
else {
    $ladder_lists = $ladder->lists();
    $this_ladder = $ladder_lists[0];
    $ladder_id = $this_ladder['ladder_id'];
}

//----------------------------------------------------------
// year
//----------------------------------------------------------
$year_list = $as->year_lists();
$year = $_POST['year'] ? $_POST['year'] : Ldr_Util::th_year();
if (!Validation::is_year($year)) {
    header('Location: dashboard.php');
    exit;
}

//----------------------------------------------------------
// 領域リスト
//----------------------------------------------------------
$group1_list = $as->lists('group1', array('guideline_revision' => $this_ladder['guideline_revision']));
$group2_list = $as->group2_full_lists();

//----------------------------------------------------------
// assessment
//----------------------------------------------------------
$assessment = $as->full_assessment(array(
    'emp_id' => $_POST["emp_id"],
    'year' => $year,
    'revision' => $this_ladder['guideline_revision'],
    ));

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign(Ldr_Util::get_common_info('staff'));
$view->assign('emp', $emp);
$view->assign('ladder', $ladder);
$view->assign('this_ladder', $this_ladder);
$view->assign($employee->find($_POST["emp_id"]));
$view->assign('year', (int)$year);
$view->assign('assessment_list', $assessment);
$view->assign('year_list', $year_list);
$view->assign('group1_list', $group1_list);

$view->display('personal_assessment.tpl');
