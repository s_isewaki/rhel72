<?php

require_once 'class/Base.php';
require_once 'class/InsideMst.php';
require_once 'class/InsideMstCheck.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('adm_inside_edit_number');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_mst_inside($session->emp());

//----------------------------------------------------------
// Obj
//----------------------------------------------------------
$mst = new Ldr_InsideMst();
$check = new Ldr_InsideMstCheck();
$effective_post = Ldr_Util::use_tiket();

//----------------------------------------------------------
// 保存
//----------------------------------------------------------
if ($effective_post && isset($_POST['btn_save'])) {
    // バリデーション
    $error = $mst->validate_contents($_POST);

    // エラーが無ければ保存
    if (empty($error)) {
        try {
            $mst->save_contents($_POST);
            $completed = true;
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
        }
    }
}

//----------------------------------------------------------
// 削除
//----------------------------------------------------------
else if (isset($_POST['btn_remove'])) {
    if (!empty($_POST['inside_contents_id'])) {
        try {
            $mst->remove_contents($_POST['inside_contents_id'], $_POST['id']);
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
        }
    }
}

//----------------------------------------------------------
// active tab
//----------------------------------------------------------
if (isset($_POST['ldr_return'])) {
    // ソート画面から戻ってきた場合
    $active = "1";
}
else {
    $active = empty($_POST['number']) ? "1" : $_POST['number'];
}

//----------------------------------------------------------
// データの取得
//----------------------------------------------------------
$data = $mst->find_contents($_POST['id']);
if ($error) {
    if ($_POST['inside_contents_id']) {
        $data['contents'][$_POST['number']] = $_POST;
    }
    else {
        $data['contents'][] = $_POST;
    }
}
else if (empty($data['contents'])) {
    $data['contents'][] = array('number' => "1", 'before_work' => 't', 'after_work' => 't', 'looking_back' => 't');
}
//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $session->emp());
$view->assign(Ldr_Util::get_common_info('adm_inside', array('referer_url' => 'adm_inside.php')));
$view->assign($data);
$view->assign('contents', $data['contents']);
$view->assign('active', $active);
$view->assign('completed', $completed);
$view->assign('list_template', $check->lists_template());
if ($error) {
    $view->assign('error', $error);
    $view->display('adm_inside_edit_number.tpl');
}
else {
    $view->display('adm_inside_edit_number.tpl');
}