<?php

require_once 'class/Base.php';
require_once 'class/Comment.php';
require_once 'class/Apply.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// セッション初期化
//----------------------------------------------------------
Ldr_Util::session_init('ldr_common');

//----------------------------------------------------------
// ID
//----------------------------------------------------------
if ($_GET['id']) {
    $_SESSION['id'] = $_GET['id'];
}

if (is_null($_SESSION['id'])) {
    js_error_exit();
}

//----------------------------------------------------------
// オブジェクト
//----------------------------------------------------------
$apply = new Ldr_Apply(array('apply_id' => $_SESSION['id']));
$emp = $session->emp();
$comm = new Ldr_Comment($emp->emp_id());
$view = new Cmx_View('ladder/templates');

//----------------------------------------------------------
// 処理
//----------------------------------------------------------
switch ($_POST['mode2']) {
// バリデーション
    case 'validate':
        $error = $comm->validate(Ldr_Util::from_utf8($_POST));
        header('X-Content-Type-Options: nosniff');
        print cmx_json_encode(array(
            'error' => !empty($error),
            'msg' => $error,
            'mode' => $_POST['mode'],
        ));
        exit;
        break;

    // 保存
    case 'save':
        $error = $comm->validate($_POST);
        // エラーが無ければ保存
        if (empty($error)) {
            // 保存処理
            try {
                $data['ldr_apply_id'] = $_SESSION['id'];
                $data['status'] = $apply->get_status();
                $data['from_emp_id'] = $emp->emp_id();
                $data['comment'] = $_POST['comment'];
                $comm->save($data);
            }
            catch (Exception $e) {
                cmx_log($e->getMessage());
                js_error_exit();
            }
        }
        else {
            js_error_exit();
        }
        unset($_SESSION['id']);
        $_SESSION['commnet_reload'] = true;
        header("Location: " . $_SERVER['HTTP_REFERER']);
        exit;
}

//----------------------------------------------------------
// データ
//----------------------------------------------------------
$view->assign('list', $comm->lists($_SESSION['id']));

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
header('Cache-Control: no-cache, must-revalidate');
header("Cache-Control: post-check=0, pre-check=0", false);
header('Pragma: no-cache');
$view->assign('emp', $emp);
$view->assign('ldr', $apply->ladder());
$view->display("modal/comment.tpl");
