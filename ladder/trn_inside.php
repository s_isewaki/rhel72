<?php

require_once 'class/Base.php';
require_once 'class/Pager.php';
require_once 'class/Inside.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}
//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('inside_apply');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_ladder($emp);

//----------------------------------------------------------
// object
//----------------------------------------------------------
$inside = new Ldr_Inside($emp->emp_id());
$conf = new Ldr_Config();
$effective_post = Ldr_Util::use_tiket();
$perPage = Ldr_Const::SRH_PERPAGE;

//----------------------------------------------------------
// 保存
//----------------------------------------------------------
if ($effective_post && $_POST['btn_save'] === "cancel") {
    try {
        $inside->cancel_apply($_POST);
        $completed = 'cancel';
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }
}
//----------------------------------------------------------
// 遷移状態判別
//----------------------------------------------------------
// ページャーから移動
if (Ldr_Pager::is_pager($_POST['move_pager'])) {
    //検索条件を取得
    $_SESSION['page_no'] = Ldr_Pager::get_page_no($_POST['page_no']);
    $srh_condition = $_SESSION['srh_condition'];
}
// 検索ボタン
else if ($_POST['search']) {
    $srh_condition["srh_keyword"] = $_POST['srh_keyword'];
    $srh_condition["srh_year"] = $_POST['srh_year'];
    $srh_condition["srh_training"] = $_POST['srh_training'];

    $_POST["page_no"] = 1;

    // 検索条件を設定
    $_SESSION['srh_condition'] = $srh_condition;
}
// 編集画面から戻ってきた場合
else if (isset($_POST['ldr_return']) || isset($_SESSION['ldr_return'])) {
    // ページ番号の復旧
    $_POST["page_no"] = $_SESSION['page_no'];

    // 検索条件の復旧
    $srh_condition = $_SESSION['srh_condition'];

    // 入力中の項目を復旧
    $_POST['srh_keyword'] = $srh_condition['srh_keyword'];
    $_POST['srh_year'] = $srh_condition['srh_year'];
    $_POST['srh_training'] = $srh_condition['srh_training'];
}
// 外部から移動してきた場合
else {
    // セッション情報の初期化
    $_SESSION = array();
    $_POST["page_no"] = 1;
    $_SESSION['page_no'] = 1;
    $_SESSION['srh_condition'] = null;

    $_POST['srh_year'] = Ldr_Util::th_year();
    $srh_condition = array(
        "srh_year" => $_POST['srh_year'],
        "srh_training" => $_GET['training'] ? $_GET['training'] : 1,
    );
    $_POST['srh_training'] = $srh_condition['srh_training'];
    $_SESSION['srh_condition'] = $srh_condition;
}
$_SESSION['ldr_return'] = null;

//----------------------------------------------------------
// データの取得
//----------------------------------------------------------
$dataCount = $inside->count_apply($srh_condition);
$current = Ldr_Pager::get_page_no($_POST['page_no'], $dataCount, $perPage);
$start = Ldr_Pager::calc_page_index($current, $perPage);
$list = $inside->lists_apply($start, $perPage, $srh_condition);

$data = array(
    'list' => $list,
    'completed' => $completed,
    'year' => $_POST['srh_year'],
    'year_list' => $inside->year_lists(),
);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $session->emp());
$view->assign(Ldr_Util::get_common_info('inside'));
$view->assign(Ldr_Pager::get_pager_info($_POST['page_no'], $dataCount, $perPage));
$view->assign($data);
$view->assign('ojt_name', $conf->ojt_name());

$view->display_with_fill('trn_inside.tpl', array('fdat' => $_POST));
