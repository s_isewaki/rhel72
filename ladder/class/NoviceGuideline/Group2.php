<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'ladder/class/Validation.php';
require_once 'ladder/class/Log.php';

class Ldr_NoviceGuidelineGroup2 extends Model
{

    var $log;

    /**
     * コンストラクタ
     */
    public function Ldr_NoviceGuidelineGroup2()
    {
        parent::connectDB();
        $this->log = new Ldr_log('novice_guideline');
    }

    /**
     * validate
     * @param Array $data
     * @return Array
     */
    public function validate($data)
    {
        $error = array();
        // 版
        if (Validation::is_empty($data['revision_id'])) {
            $error['revision_id'][] = '版が選択されていません';
        }
        else {
            $rv = new Ldr_NoviceGuidelineRevision();
            $row = $rv->find($data['revision_id']);
            if (empty($row)) {
                $error['revision_id'][] = '不正な版が選択されています';
            }
        }
        // グループ1
        if (Validation::is_empty($data['group1'])) {
            $error['group1'][] = 'グループ1が選択されていません';
        }
        else {
            $g1 = new Ldr_NoviceGuidelineGroup1();
            $fd = $g1->find($data['group1']);
            if (empty($fd)) {
                $error['group1'][] = '不正なグループ1が選択されています';
            }
        }

        // グループ2
        if (Validation::is_empty($data['name'])) {
            $error['name'][] = 'グループ2が入力されていません';
        }
        elseif (mb_strlen($data['name'], 'EUC-JP') > 200) {
            $error['name'][] = 'グループ2は200文字以内で入力してください';
        }

        return $error;
    }

    /**
     * 一覧の取得
     * @return Array
     */
    public function lists($cond)
    {
        $sql = "
            SELECT
                group2,
                g2.name,
                g2.revision_id,
                g2.group1,
                EXISTS (SELECT * FROM ldr_novice_guideline gl WHERE gl.group2 = g2.group2 AND NOT gl.del_flg) AS used,
                (SELECT COUNT(*) FROM ldr_novice_guideline gl WHERE gl.group2 = g2.group2 AND NOT gl.del_flg) AS count
            FROM ldr_novice_guideline_group2 g2
            WHERE NOT g2.del_flg
            AND   g2.group1 = :group1
            ORDER BY g2.order_no, group2
        ";

        $sth = $this->db->prepare($sql, array('integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute($cond);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 保存
     * @param Array $data
     */
    public function save($data)
    {
        $this->db->beginNestedTransaction();

        // INSERT
        if (empty($data['id'])) {
            $sth = $this->db->prepare("INSERT INTO ldr_novice_guideline_group2 (name, group1, revision_id) VALUES (:name, :group1, :revision_id)", array('text', 'integer', 'integer'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($data);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
            }
            $sth->free();
            $data['id'] = $this->db->lastInsertID('ldr_novice_guideline_group2', 'group2');
        }

        // UPDATE
        else {
            $sth = $this->db->prepare("UPDATE ldr_novice_guideline_group2 SET name= :name, group1= :group1, revision_id= :revision_id  WHERE group2= :id", array('text', 'integer', 'integer', 'integer'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($data);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " id={$data['id']} ERROR: " . $res->getDebugInfo());
            }
            $sth->free();
        }
        $this->db->completeNestedTransaction();

        // Log
        $this->log->log("グループ2を保存しました", "group2={$data['id']} group1={$data['group1']} name={$data['name']}");
    }

    /**
     * 削除
     * @param type $id
     * @throws Exception
     */
    public function delete($id)
    {
        $data = $this->find($id);
        if (isset($data)) {
            $this->db->beginNestedTransaction();
            $sth = $this->db->prepare("UPDATE ldr_novice_guideline_group2 SET del_flg = true WHERE group2= ?", array('integer'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($id);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " gp2={$data['group2']} ERROR: " . $res->getDebugInfo());
            }
            $this->db->completeNestedTransaction();
            $sth->free();
        }

        // Log
        $this->log->log("グループ2を削除しました", "group2=$id");
    }

    /**
     * 1件取得
     * @param Integer $id
     * @return Array
     */
    public function find($id)
    {
        if (is_null($id)) {
            return array();
        }

        $sql = "SELECT group2 AS id, name, group1,revision_id FROM ldr_novice_guideline_group2 WHERE group2= :group2";
        $row = $this->db->extended->getRow(
            $sql, null, array('group2' => $id), array('integer'), MDB2_FETCHMODE_ASSOC
        );
        if (empty($row)) {
            throw new Exception();
        }
        return $row;
    }

    /**
     * 並べ替え
     * @param type $data
     * @throws Exception
     */
    public function sort($data)
    {
        $this->db->beginNestedTransaction();
        $sth = $this->db->prepare("UPDATE ldr_novice_guideline_group2 SET order_no= ? WHERE group2= ?", array('integer', 'integer'), MDB2_PREPARE_MANIP);
        $cnt = 1;
        foreach ($data['order'] as $id) {
            $res = $sth->execute(array($cnt++, $id));
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " id={$id} ERROR: " . $res->getDebugInfo());
            }
        }
        $this->db->completeNestedTransaction();
        $sth->free();

        // Log
        $this->log->log("グループ2を並べ替えました", implode(" ", $data['order']));
    }

}
