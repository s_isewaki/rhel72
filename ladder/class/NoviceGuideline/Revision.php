<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'ladder/class/Validation.php';
require_once 'ladder/class/Log.php';

class Ldr_NoviceGuidelineRevision extends Model
{

    var $log;

    /**
     * コンストラクタ
     */
    public function Ldr_NoviceGuidelineRevision()
    {
        parent::connectDB();
        $this->log = new Ldr_log('novice_guideline');
    }

    /**
     * validate
     * @param Array $data
     * @return Array
     */
    public function validate($data)
    {
        $error = array();

        // 研修名
        if (Validation::is_empty($data['name'])) {
            $error['name'][] = '版名が入力されていません';
        }
        elseif (mb_strlen($data['name'], 'EUC-JP') > 200) {
            $error['name'][] = '版名は200文字以内で入力してください';
        }

        return $error;
    }

    /**
     * 一覧の取得
     * @return Array
     */
    public function lists()
    {
        $sql = "SELECT
          revision_id,
          name,
          (SELECT COUNT(*) FROM ldr_novice_guideline g WHERE g.revision_id=r.revision_id AND NOT del_flg) AS count
          FROM ldr_novice_guideline_revision r
          WHERE NOT del_flg ORDER BY updated_on desc";
        $sth = $this->db->prepare($sql, array('integer', 'text'), MDB2_PREPARE_RESULT);
        $res = $sth->execute();
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 保存
     * @param Array $data
     */
    public function save($data)
    {
        $this->db->beginNestedTransaction();

        // INSERT
        if (empty($data['id'])) {
            $sth = $this->db->prepare("INSERT INTO ldr_novice_guideline_revision (name) VALUES (:name)", array('text'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($data);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
            }
            $sth->free();
            $data['id'] = $this->db->lastInsertID('ldr_novice_guideline_revision', 'revision_id');
        }

        // UPDATE
        else {
            $sth = $this->db->prepare("UPDATE ldr_novice_guideline_revision SET name= :name WHERE revision_id= :id", array('text', 'integer'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($data);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . "id={$data['id']} ERROR: " . $res->getDebugInfo());
            }
            $sth->free();
        }
        $this->db->completeNestedTransaction();

        // Log
        $this->log->log("版を保存しました", "novice_guideline_revision={$data['id']} name={$data['name']}");
    }

    /**
     * 削除
     * @param type $id
     * @throws Exception
     */
    public function delete($id)
    {
        $data = $this->find($id);
        if (isset($data)) {
            $this->db->beginNestedTransaction();
            $sth = $this->db->prepare("UPDATE ldr_novice_guideline_revision SET del_flg = true WHERE revision_id= ?", array('integer'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($id);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . "novice_guideline_revision={$data['revision_id']} ERROR: " . $res->getDebugInfo());
            }
            $this->db->completeNestedTransaction();
            $sth->free();

            // Log
            $this->log->log("版を削除しました", "novice_guideline_revision=$id");
        }
    }

    /**
     * 1件取得
     * @param Integer $id
     * @return Array
     */
    public function find($id)
    {
        if (is_null($id)) {
            return array();
        }

        $sql = "SELECT revision_id AS id, name FROM ldr_novice_guideline_revision WHERE revision_id= :revision_id";
        $row = $this->db->extended->getRow(
            $sql, null, array('revision_id' => $id), array('integer'), MDB2_FETCHMODE_ASSOC
        );
        if (PEAR::isError($row)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $row->getDebugInfo());
        }
        return $row;
    }

}
