<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'ladder/class/Const.php';
require_once 'ladder/class/Validation.php';
require_once 'ladder/class/Log.php';

class Ldr_NoviceGuidelineGuideline extends Model
{
    var $log;

    /**
     * コンストラクタ
     */
    public function Ldr_NoviceGuidelineGuideline()
    {
        parent::connectDB();
        $this->log = new Ldr_log('noviceguideline');
    }

    /**
     * validate
     * @param Array $data
     * @return Array
     */
    public function validate($data)
    {
        $error = array();

        // 版
        if (Validation::is_empty($data['revision_id'])) {
            $error['revision_id'][] = '版が選択されていません';
        }
        else {
            $rv = new Ldr_NoviceGuidelineRevision();
            $row = $rv->find($data['revision_id']);
            if (empty($row)) {
                $error['revision_id'][] = '不正な版が選択されています';
            }
        }

        // 領域
        if (Validation::is_empty($data['group1'])) {
            $error['group1'][] = 'グループ1が選択されていません';
        }
        else {
            $g1 = new Ldr_NoviceGuidelineGroup1();
            $row = $g1->find($data['group1']);
            if (empty($row)) {
                $error['group1'][] = '不正なグループ1が選択されています';
            }
        }

        // 分類
        if (Validation::is_empty($data['group2'])) {
            $error['group2'][] = 'グループ2が選択されていません';
        }
        else {
            $g2 = new Ldr_NoviceGuidelineGroup2();
            $row = $g2->find($data['group2']);
            if (empty($row)) {
                $error['group2'][] = '不正なグループ2が選択されています';
            }
        }

        // 到達目標
        if (Validation::is_empty($data['guideline'])) {
            $error['guideline'][] = '到達目標が入力されていません';
        }

        return $error;
    }

    /**
     * 一覧の取得
     * @return Array
     */
    public function lists($cond)
    {
        $sql = "
            SELECT
                guideline_id,
                gl.revision_id,
                gl.group1,
                gl.group2,
                guideline,
                mhlw
            FROM ldr_novice_guideline gl
            JOIN ldr_novice_guideline_group1 g1 USING (group1)
            JOIN ldr_novice_guideline_group2 g2 USING (group2)
            WHERE NOT gl.del_flg
              AND gl.revision_id = :revision_id
              AND gl.group1 = :group1
              AND gl.group2 = :group2
            ORDER BY gl.order_no, gl.guideline_id
         ";
        $sth = $this->db->prepare($sql, array('integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute($cond);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 保存
     * @param Array $data
     */
    public function save($data)
    {
        $data['mhlw'] = $data['mhlw'] === 't' ? true : false;

        $this->db->beginNestedTransaction();

        // INSERT
        if (empty($data['id'])) {
            $sth = $this->db->prepare("INSERT INTO ldr_novice_guideline(revision_id, group1, group2, guideline, mhlw)
                    VALUES (:revision_id, :group1, :group2, :guideline, :mhlw)", array('integer', 'integer', 'integer', 'text', 'boolean'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($data);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
            }
            $sth->free();
            $data['id'] = $this->db->lastInsertID('ldr_novice_guideline', 'guideline_id');
        }

        // UPDATE
        else {
            $sth = $this->db->prepare("UPDATE ldr_novice_guideline SET revision_id= :revision_id, group1= :group1, group2= :group2, guideline= :guideline, mhlw= :mhlw WHERE guideline_id= :id", array('integer', 'integer', 'integer', 'text', 'boolean', 'integer'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($data);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " id={$data['id']} ERROR: " . $res->getDebugInfo());
            }
            $sth->free();
        }
        $this->db->completeNestedTransaction();

        // Log
        $this->log->log("到達目標を保存しました", "id={$data['id']} revison={$data['revision_id']} group1={$data['group1']} group2={$data['group2']} guideline={$data['guideline']} mhlw={$data['mhlw']}");
    }

    /**
     * 削除
     * @param type $id
     * @throws Exception
     */
    public function delete($id)
    {
        $data = $this->find($id);
        if (isset($data)) {
            $this->db->beginNestedTransaction();
            $sth = $this->db->prepare("UPDATE ldr_novice_guideline SET del_flg = true WHERE guideline_id= ?", array('integer'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($id);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " id={$data['guideline_id']} ERROR: " . $res->getDebugInfo());
            }
            $this->db->completeNestedTransaction();
            $sth->free();
        }

        // Log
        $this->log->log("到達目標を削除しました", "id=$id");
    }

    /**
     * 1件取得
     * @param Integer $id
     * @return Array
     */
    public function find($id)
    {
        if (is_null($id)) {
            return array();
        }

        $sql = "SELECT guideline_id AS id, revision_id, group1, group2, guideline, mhlw FROM ldr_novice_guideline WHERE guideline_id= :guideline_id";
        $row = $this->db->extended->getRow(
            $sql, null, array('guideline_id' => $id), array('integer'), MDB2_FETCHMODE_ASSOC
        );
        if (empty($row)) {
            throw new Exception();
        }
        return $row;
    }

    /**
     * 並べ替え
     * @param type $data
     * @throws Exception
     */
    public function sort($data)
    {
        $this->db->beginNestedTransaction();
        $sth = $this->db->prepare("UPDATE ldr_novice_guideline SET order_no= ? WHERE guideline_id= ?", array('integer', 'integer'), MDB2_PREPARE_MANIP);
        $cnt = 1;
        foreach ($data['order'] as $id) {
            $res = $sth->execute(array($cnt++, $id));
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " id={$id} ERROR: " . $res->getDebugInfo());
            }
        }
        $this->db->completeNestedTransaction();
        $sth->free();

        // Log
        $this->log->log("到達目標を並べ替えました", implode(" ", $data['order']));
    }

}
