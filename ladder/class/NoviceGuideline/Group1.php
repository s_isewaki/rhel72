<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'ladder/class/Validation.php';
require_once 'ladder/class/Log.php';

class Ldr_NoviceGuidelineGroup1 extends Model
{

    var $log;

    /**
     * コンストラクタ
     */
    public function Ldr_NoviceGuidelineGroup1()
    {
        parent::connectDB();
        $this->log = new Ldr_log('novice_guideline');
    }

    /**
     * validate
     * @param Array $data
     * @return Array
     */
    public function validate($data)
    {
        $error = array();
        // 版
        if (Validation::is_empty($data['revision_id'])) {
            $error['revision_id'][] = '版が選択されていません';
        }
        else {
            $rv = new Ldr_NoviceGuidelineRevision();
            $row = $rv->find($data['revision_id']);
            if (empty($row)) {
                $error['revision_id'][] = '不正な版が選択されています';
            }
        }

        //グループ名１
        if (Validation::is_empty($data['name'])) {
            $error['name'][] = 'グループ1が入力されていません';
        }
        elseif (mb_strlen($data['name'], 'EUC-JP') > 200) {
            $error['name'][] = 'グループ1は200文字以内で入力してください';
        }

        return $error;
    }

    /**
     * 一覧の取得
     * @return Array
     */
    public function lists($cond)
    {
        $sql = "
            SELECT
                g1.group1,
                g1.revision_id,
                g1.name,
                EXISTS (SELECT * FROM ldr_novice_guideline_group2 g2 WHERE g2.group1=g1.group1 AND NOT g2.del_flg) AS used,
                (SELECT COUNT(*) FROM ldr_novice_guideline_group2 g2 WHERE g2.group1=g1.group1 AND NOT g2.del_flg) AS g2count
            FROM ldr_novice_guideline_group1 g1
            WHERE NOT g1.del_flg
              AND g1.revision_id = :revision_id
            ORDER BY g1.order_no, g1.group1
        ";
        $sth = $this->db->prepare($sql, array('integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute($cond);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 保存
     * @param Array $data
     */
    public function save($data)
    {
        $this->db->beginNestedTransaction();

        // INSERT
        if (empty($data['id'])) {
            $sth = $this->db->prepare("INSERT INTO ldr_novice_guideline_group1 (name,revision_id) VALUES (:name,:revision_id)", array('text', 'integer'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($data);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
            }
            $sth->free();
            $data['id'] = $this->db->lastInsertID('ldr_novice_guideline_group1', 'group1');
        }

        // UPDATE
        else {
            $sth = $this->db->prepare("UPDATE ldr_novice_guideline_group1 SET name= :name, revision_id= :revision_id WHERE group1= :id", array('text', 'integer', 'integer'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($data);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
            }
            $sth->free();
        }
        $this->db->completeNestedTransaction();

        // Log
        $this->log->log("グループ1を保存しました", "group1={$data['id']} name={$data['name']}");
    }

    /**
     * 削除
     * @param type $id
     * @throws Exception
     */
    public function delete($id)
    {
        $data = $this->find($id);
        if (isset($data)) {
            $this->db->beginNestedTransaction();
            $sth = $this->db->prepare("UPDATE ldr_novice_guideline_group1 SET del_flg = true WHERE group1= ?", array('integer'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($id);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
            }
            $this->db->completeNestedTransaction();
            $sth->free();
        }

        // Log
        $this->log->log("グループ1を削除しました", "group1=$id");
    }

    /**
     * 1件取得
     * @param Integer $id
     * @return Array
     */
    public function find($id)
    {
        if (is_null($id)) {
            return array();
        }

        $sql = "SELECT group1 AS id, name, revision_id FROM ldr_novice_guideline_group1 WHERE group1= :group1";
        $row = $this->db->extended->getRow(
            $sql, null, array('group1' => $id), array('integer'), MDB2_FETCHMODE_ASSOC
        );
        if (empty($row)) {
            throw new Exception();
        }
        return $row;
    }

    /**
     * 並べ替え
     * @param type $data
     * @throws Exception
     */
    public function sort($data)
    {
        $this->db->beginNestedTransaction();
        $sth = $this->db->prepare("UPDATE ldr_novice_guideline_group1 SET order_no= ? WHERE group1= ?", array('integer', 'integer'), MDB2_PREPARE_MANIP);
        $cnt = 1;
        foreach ($data['order'] as $id) {
            $res = $sth->execute(array($cnt++, $id));
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " id={$id} ERROR: " . $res->getDebugInfo());
            }
        }
        $this->db->completeNestedTransaction();
        $sth->free();

        // Log
        $this->log->log("グループ1を並べ替えました", implode(" ", $data['order']));
    }

}
