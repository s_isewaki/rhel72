<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'ladder/class/Log.php';
require_once 'ladder/class/Config.php';

/**
 * 申請承認者
 */
class Ldr_Approver extends Model
{

    var $log;

    /**
     * コンストラクタ
     */
    public function Ldr_Approver()
    {
        parent::connectDB();
        $this->log = new Ldr_log('permission');
    }

    /**
     * 一覧の取得
     * @return Array
     */
    public function lists($class = null)
    {
        $conf = new Ldr_Config();
        $class_sql = Ldr_Util::class_full_name_sql($conf->view_class());
        $base_sql = "
            SELECT
                c1.class_id,
                c2.atrb_id,
                c3.dept_id,
                c4.room_id,
                $class_sql,
                class_nm,
                atrb_nm,
                dept_nm,
                room_nm,
                a.emp_id,
                emp_lt_nm || ' ' || emp_ft_nm as emp_name
            FROM classmst c1
            JOIN atrbmst c2 ON c1.class_id=c2.class_id AND NOT atrb_del_flg
            JOIN deptmst c3 ON c3.atrb_id=c2.atrb_id AND NOT dept_del_flg
            LEFT JOIN classroom c4 ON c4.dept_id=c3.dept_id AND NOT room_del_flg
            LEFT JOIN ldr_approver a ON c1.class_id=a.class_id AND c2.atrb_id=a.atrb_id AND c3.dept_id=a.dept_id AND (c4.room_id=a.room_id OR c4.room_id IS NULL)
            LEFT JOIN empmst e ON a.emp_id=e.emp_id
            WHERE NOT class_del_flg
        ";

        if (empty($class)) {
            $sql = $base_sql . "
                  AND a.emp_id IS NOT NULL
                ORDER BY c1.order_no, c2.order_no, c3.order_no, c4.order_no, c1.class_id, c2.atrb_id, c3.dept_id, c4.room_id
            ";
            $res = $this->db->query($sql);
        }
        else {
            $class_array = explode('-', $class);
            $sql = $base_sql . "
                AND c1.class_id= ?
                AND c2.atrb_id= ?
              ORDER BY c3.order_no, c4.order_no, c3.dept_id, c4.room_id
            ";
            $sth = $this->db->prepare($sql, array('integer', 'integer'), MDB2_PREPARE_RESULT);
            $res = $sth->execute($class_array);
        }

        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row;
        }
        return $lists;
    }

    /**
     * プルダウン用部署一覧の取得
     * @return Array
     */
    public function pulldown_class_lists()
    {
        $sql = "
            SELECT
                c1.class_id,
                class_nm,
                atrb_id,
                atrb_nm
            FROM classmst c1
            JOIN atrbmst c2 ON c1.class_id=c2.class_id AND NOT atrb_del_flg
            WHERE NOT class_del_flg
            ORDER BY c1.order_no, c2.order_no, c1.class_id, c2.atrb_id
            ";
        $res = $this->db->query($sql);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row;
        }
        return $lists;
    }

    /**
     * 保存
     * @param String $emp_id
     * @param Array $data
     */
    public function save($data)
    {
        $this->db->beginNestedTransaction();

        // UPDATE
        $log = '';
        $sth = $this->db->prepare("SELECT update_ldr_approver(?, ?, ?, ?, ?)", array('integer', 'integer', 'integer', 'integer', 'text'), MDB2_PREPARE_MANIP);
        foreach ($data['class'] as $class) {
            list($c1, $c2, $c3, $c4) = explode("-", $class);
            $res = $sth->execute(array($c1, $c2, $c3, $c4, $data['approver'][$class]));
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
            }
            $log .= "$class={$data['approver'][$class]} ";
        }
        $this->db->completeNestedTransaction();
        $sth->free();

        // Log
        $this->log->log('申請承認者を保存しました', $log);
    }

}
