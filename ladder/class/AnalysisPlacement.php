<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'ladder/class/Validation.php';
require_once 'ladder/class/Log.php';
require_once 'ladder/class/Config.php';
require_once 'ladder/class/Ladder.php';

class Ldr_AnalysisPlacement extends Model
{

    var $log;

    /**
     * コンストラクタ
     */
    public function Ldr_AnalysisPlacement()
    {
        parent::connectDB();
        $this->ladder = new Ldr_Ladder();
        $this->log = new Ldr_log('analysis_placement');
    }

    /**
     * validate
     * @param type $data
     * @return string
     */
    public function validate($data)
    {
        $error = array();

        if (Validation::is_empty($data['pattern'])) {
            $error['pattern'][] = "パターン名が入力されていません";
        }
        else {
            if (mb_strlen($data['pattern'], 'EUC-JP') > 200) {
                $error['pattern'][] = 'パターン名は200文字以内で入力してください';
            }
            else if ($this->exist_pattern($data['pattern'])) {
                $error['pattern'][] = '登録済みのパターン名です';
            }
        }

        return $error;
    }

    /**
     * パターンの存在チェック
     * @param type $id
     * @return type
     * @throws Exception
     */
    public function exist_pattern($pattern)
    {
        $sql = "SELECT count(*) FROM ldr_placement_pattern WHERE pattern = :pattern AND NOT del_flg ";
        $types = array('text');
        $data = array('pattern' => $pattern);
        $sth = $this->db->prepare($sql, $types, MDB2_PREPARE_RESULT);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }
        $count = 0;
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $count = $row["count"];
        }
        $sth->free();
        return ($count > 0);
    }

    /**
     * 保存
     * @param type $type
     * @param type $data
     */
    public function save($type, $data)
    {
        $this->db->beginNestedTransaction();
        if ($type === "new") {
            $data['pattern_id'] = $this->save_pattern($data);
            $msg_ptn = "pattern=" . $data['pattern'];
        }
        else {
            $this->update_pattern($data['pattern_id']);
        }

        $this->delete_placement($data['pattern_id']);

        foreach ($data['placement'] as $value) {
            $pl = array();
            $pl['pattern_id'] = $data['pattern_id'];
            list($pl['class_id'], $pl['atrb_id'], $pl['dept_id'], $pl['room_id'], $pl['st_id'], $pl['emp_id'], $pl['level'], $pl['name']) = explode('_', $value, 8);
            $pl['name'] = trim($pl['name']);
            $this->save_placement($pl);
        }

        $this->db->completeNestedTransaction();

        $this->log->log("保存しました", "id={$data['pattern_id']} " . $msg_ptn);
        return $data['pattern_id'];
    }

    /**
     * 配置表の保存
     * @param type $data
     * @return type
     * @throws Exception
     */
    private function save_placement($data)
    {
        $sth = $this->db->prepare("INSERT INTO
                ldr_placement
                (pattern_id,emp_id,name,level,class_id,atrb_id,dept_id,room_id,st_id)
                VALUES
                (:pattern_id,:emp_id,:name,:level,:class_id,:atrb_id,:dept_id,:room_id,:st_id)", array('text', 'integer', 'integer', 'integer', 'integer', 'integer'), MDB2_PREPARE_MANIP);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }

        $sth->free();
        return;
    }

    /**
     * パターンの保存
     * @param type $data
     * @return type
     * @throws Exception
     */
    private function save_pattern($data)
    {
        $this->db->beginNestedTransaction();

        if (empty($data['id'])) {
            $sth = $this->db->prepare("INSERT INTO
                ldr_placement_pattern
                (pattern,basic_date)
                VALUES
                (:pattern,:basic_date)", array('text', 'text'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($data);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
            }
            $id = $this->db->lastInsertId('ldr_placement_pattern', 'pattern_id');

            $sth->free();
        }

        // UPDATE
        else {

            $sth = $this->db->prepare("UPDATE ldr_placement_pattern SET
                pattern= :pattern,
                WHERE pattern_id= :id AND NOT del_flg", array('text', 'integer'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($data);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
            }
            $id = $data['id'];
            $sth->free();
        }
        $this->db->completeNestedTransaction();

        return (string)$id;
    }

    /**
     * 削除
     * @param type $id
     */
    public function delete($id)
    {
        $this->db->beginNestedTransaction();

        $this->delete_pattern($id);
        $this->delete_placement($id);


        $this->db->completeNestedTransaction();
        $this->log->log("削除しました", "id={$id}");
    }

    /**
     * パターン名称の削除
     * @param type $id
     * @throws Exception
     */
    public function delete_pattern($id)
    {
        $this->db->beginNestedTransaction();
        $sth = $this->db->prepare("UPDATE ldr_placement_pattern SET del_flg = true WHERE pattern_id= :id", array('integer'), MDB2_PREPARE_MANIP);
        $res = $sth->execute(array('id' => $id));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }
        $this->db->completeNestedTransaction();

        $sth->free();
    }

    /**
     * パターンの更新
     * @param type $id
     * @throws Exception
     */
    private function update_pattern($id)
    {
        $this->db->beginNestedTransaction();
        $sth = $this->db->prepare("UPDATE ldr_placement_pattern SET updated_on = current_timestamp WHERE pattern_id= :id AND NOT del_flg", array('integer'), MDB2_PREPARE_MANIP);
        $res = $sth->execute(array('id' => $id));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }
        $this->db->completeNestedTransaction();

        $sth->free();
    }

    /**
     * 配置の削除
     * @param type $id
     * @throws Exception
     */
    public function delete_placement($id)
    {
        $this->db->beginNestedTransaction();
        $sth = $this->db->prepare("DELETE FROM ldr_placement WHERE pattern_id= :id", array('integer'), MDB2_PREPARE_MANIP);
        $res = $sth->execute(array('id' => $id));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }
        $this->db->completeNestedTransaction();

        $sth->free();
    }

    /**
     * パターン一覧の取得
     * @return type
     * @throws Exception
     */
    public function lists_pattern()
    {
        $sql = "SELECT pattern_id,pattern,basic_date,updated_on FROM ldr_placement_pattern WHERE NOT del_flg ORDER by basic_date desc,updated_on desc,pattern_id asc";
        $sth = $this->db->prepare($sql, null, MDB2_PREPARE_RESULT);
        $res = $sth->execute(null);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }
        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 配置一覧
     * @param type $id
     */
    public function lists($id)
    {
        if (empty($id)) {
            return $this->lists_current();
        }
        else {
            return $this->lists_placement($id);
        }
    }

    /**
     * 配置表
     * @param type $id
     * @return type
     * @throws Exception
     */
    private function lists_placement($id)
    {
        $sql = "
            SELECT
                pl.pattern_id,
                pl.emp_id,
                pl.name,
                pl.st_id as emp_st,
                pl.level,
                c1.class_id,
                c2.atrb_id,
                c3.dept_id,
                c4.room_id,
                class_nm,
                atrb_nm,
                dept_nm,
                room_nm
            FROM classmst c1
            JOIN atrbmst c2 ON c1.class_id=c2.class_id AND NOT atrb_del_flg
            JOIN deptmst c3 ON c3.atrb_id=c2.atrb_id AND NOT dept_del_flg
            LEFT JOIN classroom c4 ON c4.dept_id=c3.dept_id AND NOT room_del_flg
            JOIN ldr_placement pl ON c1.class_id=pl.class_id
                AND c2.atrb_id=pl.atrb_id
                AND c3.dept_id=pl.dept_id
                AND (c4.room_id=pl.room_id OR c4.room_id IS NULL)
            WHERE
                NOT class_del_flg
                AND pl.pattern_id = :id
            ORDER BY c1.order_no, c2.order_no, c3.order_no, c4.order_no, (pl.level is not null) desc, pl.level desc
        ";
        $sth = $this->db->prepare($sql, array('integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array('id' => $id));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }
        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $key1 = $row['class_id'] . "_" . $row['atrb_id'] . "_" . $row['dept_id'] . "_" . $row['room_id'];
            $levels = array_reverse(str_split(strval($row['level'])));
            $lv = 1;
            foreach ($levels as $level) {
                $row['level' . $lv++] = $level;
            }
            $lists[$key1][$row['emp_st']][] = $row;
        }

        $sth->free();
        return $lists;
    }

    /**
     * 配置一覧（現在の配置）
     * @param type $cond
     * @return type
     * @throws Exception
     */
    private function lists_current()
    {
        // ラダーレベル
        list($level_sql, $level_join) = $this->ladder->level_sql();

        $sql = "
            SELECT
                {$level_sql},
                e.emp_id,
                e.emp_lt_nm || ' ' || e.emp_ft_nm as name,
                e.emp_st,
                c1.class_id,
                c2.atrb_id,
                c3.dept_id,
                c4.room_id,
                class_nm,
                atrb_nm,
                dept_nm,
                room_nm
            FROM classmst c1
            JOIN atrbmst c2 ON c1.class_id=c2.class_id AND NOT atrb_del_flg
            JOIN deptmst c3 ON c3.atrb_id=c2.atrb_id AND NOT dept_del_flg
            LEFT JOIN classroom c4 ON c4.dept_id=c3.dept_id AND NOT room_del_flg
            JOIN ldr_analysis_authority auth ON auth.class_id = c1.class_id
                AND auth.atrb_id = c2.atrb_id
                AND auth.dept_id = c3.dept_id
                AND (auth.room_id=c4.room_id OR (auth.room_id IS NULL AND c4.room_id IS NULL))
                AND auth.auth_flg
                AND NOT auth.del_flg
            JOIN empmst e ON c1.class_id=e.emp_class
                AND c2.atrb_id=e.emp_attribute
                AND c3.dept_id=e.emp_dept
                AND (c4.room_id=e.emp_room OR c4.room_id IS NULL)
            JOIN authmst au USING (emp_id)
            JOIN ldr_analysis_authority_st  aas ON aas.st_id  = e.emp_st  AND aas.auth_flg AND NOT aas.del_flg
            JOIN ldr_analysis_authority_job aaj ON aaj.job_id = e.emp_job AND aaj.auth_flg AND NOT aaj.del_flg
            {$level_join}
            WHERE NOT class_del_flg
              AND NOT au.emp_del_flg
            ORDER BY c1.order_no, c2.order_no, c3.order_no, c4.order_no, (ldr1.level is not null) desc, ldr1.level desc, emp_kn_lt_nm, emp_kn_ft_nm
        ";

        $sth = $this->db->prepare($sql, null, MDB2_PREPARE_RESULT);
        $res = $sth->execute(null);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }
        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $key1 = $row['class_id'] . "_" . $row['atrb_id'] . "_" . $row['dept_id'] . "_" . $row['room_id'];
            $lists[$key1][$row['emp_st']][] = $row;
        }
        $sth->free();
        return $lists;
    }

}
