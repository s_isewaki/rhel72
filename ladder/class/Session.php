<?php

require_once 'Cmx.php';
require_once 'Cmx/Model/Session.php';
require_once 'ladder/class/Employee.php';

class Ldr_Session extends Cmx_Session
{

    var $emp;

    /**
     * 職員情報を取得する
     * @return Ldr_Employee
     */
    function emp()
    {
        if ($this->_emp_id) {
            if (empty($this->emp)) {
                $this->emp = new Ldr_Employee($this->_emp_id);
            }
            return $this->emp;
        }
        return null;
    }

}
