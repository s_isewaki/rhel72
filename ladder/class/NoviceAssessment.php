<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'Cmx/Model/SystemConfig.php';
require_once 'ladder/class/Util.php';
require_once 'ladder/class/NoviceGuideline.php';
require_once 'ladder/class/Log.php';

class Ldr_NoviceAssessment extends Ldr_NoviceGuideline
{

    var $emp_id;
    var $log;

    /**
     * コンストラクタ
     */
    public function Ldr_NoviceAssessment($emp_id = null)
    {
        parent::connectDB();
        parent::Ldr_NoviceGuideline();
        $this->emp_id = $emp_id;
        $this->log = new Ldr_log('novice_assessment');
    }

    /**
     * 分類一覧の取得
     * @param type $data
     * @return Array
     */
    public function group2_lists($data)
    {
        $sql = "
            SELECT
                group1,
                group2,
                g2.name
            FROM ldr_novice_guideline_group2 g2
            WHERE NOT g2.del_flg
              AND group1= :group1
              AND EXISTS (SELECT * FROM ldr_novice_guideline g WHERE g.group2=g2.group2 AND revision_id = :revision)
            ORDER BY g2.order_no, group2
            ";
        $sth = $this->db->prepare($sql, array('integer', 'integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 自己評価データの取得
     * @param type $data
     * @return type
     */
    public function assessment($data)
    {
        $revision = $this->current_revision();
        $sql = "
            SELECT
                g.guideline_id,
                g.group2,
                g.guideline,
                number,
                assessment
            FROM ldr_novice_guideline g
            LEFT JOIN ldr_novice_assessment a ON a.guideline_id=g.guideline_id AND a.emp_id= :emp_id AND a.year= :year AND number=:number
            WHERE NOT g.del_flg
              AND g.group1= :group1
              AND revision_id = :revision
            ORDER BY g.order_no, g.guideline_id
            ";
        $sth = $this->db->prepare($sql, array('text', 'integer', 'integer', 'integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array_merge($data, array('revision' => $revision)));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[$row['group2']][] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 自己評価データの保存
     * @param type $emp_id
     * @param type $data
     * @throws Exception
     */
    public function save($emp_id, $data, $mode)
    {
        $this->db->beginNestedTransaction();

        $sth = $this->db->prepare("SELECT update_ldr_novice_assessment(:emp_id, :guideline_id, :year, :number, :assessment)", array('text', 'integer', 'integer', 'integer', 'integer'), MDB2_PREPARE_MANIP);

        // LOOP
        foreach ($data['assessment'] as $gid => $assessment) {
            // FIELD DATA
            $field = array(
                'emp_id' => $emp_id,
                'guideline_id' => $gid,
                'year' => $data['year'],
                'assessment' => $assessment,
                'number' => $data['number'],
            );

            // UPDATE or INSERT
            $res = $sth->execute($field);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " guideline_id = {$gid} ERROR: " . $res->getDebugInfo());
            }
        }

        $sth->free();
        $this->db->completeNestedTransaction();

        // Log
        if ($mode !== 'auto') {
            $this->log->log('保存しました', "year={$data['year']} guideline_id=" . implode(",", array_keys($data['assessment'])));
        }
    }

    /**
     * 前回評価のコピー
     * @param type $emp_id
     * @param type $data
     * @throws Exception
     */
    public function copy($emp_id, $data)
    {
        // 第1回ならコピーしない
        if ($data['number'] <= 1) {
            return false;
        }

        $sql = "
            SELECT
                a.guideline_id,
                a.assessment
            FROM ldr_novice_assessment a
            JOIN ldr_novice_guideline g USING (guideline_id)
            WHERE NOT a.del_flg
              AND NOT g.del_flg
              AND a.emp_id= :emp_id
              AND a.year= :year
              AND a.number = :number
            ";
        $sth = $this->db->prepare($sql, array('text', 'integer', 'integer', 'integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array(
            'emp_id' => $emp_id,
            'year' => $data['year'],
            'revision' => $data['revision'],
            'number' => $data['number'] - 1,
            )
        );
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }

        $this->db->beginNestedTransaction();

        $sql_upd = 'SELECT update_ldr_novice_assessment(:emp_id, :guideline_id, :year, :number, :assessment)';
        $sth_upd = $this->db->prepare($sql_upd, array('text', 'integer', 'integer', 'integer', 'integer'), MDB2_PREPARE_MANIP);
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $res_upd = $sth_upd->execute(array(
                'emp_id' => $emp_id,
                'guideline_id' => $row['guideline_id'],
                'year' => $data['year'],
                'number' => $data['number'],
                'assessment' => $row['assessment'],
            ));
            if (PEAR::isError($res_upd)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " ERROR: " . $res_upd->getDebugInfo());
            }
        }

        $this->db->completeNestedTransaction();
        $this->log->log("前回評価をコピーしました", "emp_id={$data['emp_id']} number={$data['number']}");
    }

    /**
     * 新人評価する年のリストを取得する
     * @return type
     */
    public function novice_year_lists()
    {
        $sth = $this->db->prepare('SELECT year FROM ldr_novice WHERE emp_id = ? ORDER BY year DESC', array('text'), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array($this->emp_id));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row['year'];
        }
        return $lists;
    }

    /**
     * 新人教育担当をする年のリストを取得する
     * @return type
     */
    public function facilitate_year_lists()
    {
        $sth = $this->db->prepare('SELECT year FROM ldr_facilitator WHERE facilitate_emp_id = ? GROUP BY year ORDER BY year DESC', array('text'), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array($this->emp_id));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row['year'];
        }

        if (empty($lists)) {
            return range(date('Y') + 1, 2015);
        }

        return $lists;
    }

    /*
     * 日程一覧の取得
     * @return Array
     */

    public function date_lists($year)
    {
        $sql = "SELECT number, last_date FROM ldr_novice_date WHERE NOT del_flg AND year = ? ORDER BY number";
        $sth = $this->db->prepare($sql, array('integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array($year));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }
        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row;
            if ($row['last_date'] >= date('Y-m-d')) {
                break;
            }
        }
        return array_reverse($lists);
    }

    /**
     * 本人評価有無チェック
     * @param type $emp_id
     * @return boolean
     */
    public function is_novice_exists()
    {
        $year = Ldr_Util::th_year();
        $revision = $this->current_revision();
        $date_lists = $this->date_lists($year);
        $number = $date_lists[0]['number'];

        $sql = "
            SELECT
                n.year,
                number,
                COUNT(*) AS count
            FROM ldr_novice n
            JOIN ldr_novice_guideline ON revision_id= :revision AND NOT ldr_novice_guideline.del_flg
            JOIN ldr_novice_date d USING (year)
            LEFT JOIN ldr_novice_assessment a USING (emp_id, guideline_id, year, number)
            WHERE emp_id = :emp_id
              AND (year = :year AND d.number <= :number OR year + 1 = :year)
              AND a.assessment IS NULL
            GROUP BY n.year, d.number
            ORDER BY d.number
        ";
        $sth = $this->db->prepare($sql, array('integer', 'text', 'integer', 'integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array('emp_id' => $this->emp_id, 'year' => $year, 'number' => $number, 'revision' => $revision));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }
        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row;
        }

        return $lists;
    }

    /**
     * 他者評価有無チェック
     * @param type $emp_id
     * @return boolean
     */
    public function is_facilitate_exists()
    {
        $year = Ldr_Util::th_year();
        $revision = $this->current_revision();
        $date_lists = $this->date_lists($year);
        $number = $date_lists[0]['number'];

        $sql = "
            SELECT
                f.emp_id,
                e.emp_lt_nm || ' ' || e.emp_ft_nm AS emp_name,
                f.year,
                number,
                COUNT(*) AS count
            FROM ldr_facilitator f
            JOIN empmst e USING (emp_id)
            JOIN ldr_novice_guideline ON revision_id= :revision AND NOT ldr_novice_guideline.del_flg
            JOIN ldr_novice_date d USING (year)
            LEFT JOIN ldr_novice_facilitate_assessment fa USING (emp_id, guideline_id, year, number)
            WHERE f.facilitate_emp_id = :emp_id
              AND (year = :year AND d.number <= :number OR year + 1 = :year)
              AND NOT EXISTS (
                SELECT * FROM ldr_novice_guideline gg
                LEFT JOIN ldr_novice_assessment aa ON gg.revision_id= :revision AND emp_id=f.emp_id AND gg.guideline_id=aa.guideline_id AND year=f.year AND number=d.number
                WHERE assessment IS NULL
              )
              AND fa.assessment IS NULL
            GROUP BY f.emp_id, e.emp_lt_nm, e.emp_ft_nm, f.year, d.number
            ORDER BY d.number
        ";
        $sth = $this->db->prepare($sql);
        $res = $sth->execute(array('emp_id' => $this->emp_id, 'year' => $year, 'number' => $number, 'revision' => $revision));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }
        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row;
        }

        return $lists;
    }

    /**
     * 教育担当データ
     * @param type $emp_id
     * @return boolean
     */
    public function facilitate($year)
    {
        $revision = $this->current_revision();

        $sql = "
            SELECT
                f.emp_id,
                e.emp_lt_nm || ' ' || e.emp_ft_nm AS emp_name,
                number,
                COUNT(g.guideline_id) AS guideline,
                COUNT(a.guideline_id) AS assessment,
                MAX(a.updated_on) AS lastupdate
            FROM ldr_facilitator f
            JOIN empmst e USING (emp_id)
            JOIN ldr_novice_guideline g ON revision_id= :revision AND NOT g.del_flg
            JOIN ldr_novice_date d USING (year)
            LEFT JOIN ldr_novice_assessment a USING (emp_id, guideline_id, year, number)
            WHERE facilitate_emp_id = :emp_id
              AND year = :year
            GROUP BY f.emp_id, e.emp_lt_nm, e.emp_ft_nm, d.number
            ORDER BY d.number
        ";
        $sth = $this->db->prepare($sql, array('integer', 'text', 'integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array('emp_id' => $this->emp_id, 'year' => $year, 'revision' => $revision));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }
        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[$row['emp_id']]['emp_name'] = $row['emp_name'];
            $lists[$row['emp_id']][$row['number']] = $row;
        }
        return $lists;
    }

    /**
     * 集計
     * @param type $year
     * @return type
     * @throws Exception
     */
    public function table($year)
    {
        $sql = "
            SELECT
                number,
                group2,
                assessment,
                COUNT(*)
            FROM ldr_novice_assessment
            JOIN ldr_novice_guideline USING (guideline_id)
            WHERE emp_id = :emp_id
              AND year = :year
              AND NOT ldr_novice_guideline.del_flg
            GROUP BY number, group2, assessment
        ";
        $sth = $this->db->prepare($sql, array('text', 'integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array('emp_id' => $this->emp_id, 'year' => $year));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $data = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            // data
            $data[$row['number']][$row['group2']][$row['assessment']] = $row['count'];
        }

        return $data;
    }

    /**
     * 他者評価データの取得
     * @param type $id
     * @param type $level
     * @param type $apply_emp_id
     * @param type $accept_emp_id
     * @return type
     */
    public function facilitate_assessment($emp_id, $year, $number)
    {
        $revision = $this->current_revision();

        $sql = "
            SELECT
                g.guideline_id,
                g.group1,
                g.group2,
                g.guideline,
                a.assessment as assessment,
                b.assessment as f_assessment,
                g2.name as group2_name,
                b.facilitate_emp_id AS f_emp_id,
                b.updated_on AS f_updated_on
            FROM ldr_novice_guideline g
            JOIN ldr_novice_guideline_group1 g1 ON g.revision_id = g1.revision_id AND g.group1=g1.group1 AND NOT g1.del_flg
            JOIN ldr_novice_guideline_group2 g2 ON g.revision_id = g2.revision_id AND g.group2=g2.group2 AND NOT g2.del_flg
            LEFT JOIN ldr_novice_assessment a ON a.emp_id= :emp_id AND a.guideline_id=g.guideline_id AND a.year = :year AND a.number = :number
            LEFT JOIN ldr_novice_facilitate_assessment b ON b.emp_id= a.emp_id AND b.guideline_id=g.guideline_id AND b.year = a.year AND b.number = a.number
            WHERE NOT g.del_flg
              AND g.revision_id = :revision
            ORDER BY g1.order_no, g2.order_no, g.order_no, g.guideline_id
            ";
        $sth = $this->db->prepare($sql);
        $res = $sth->execute(array(
            'emp_id' => $emp_id,
            'year' => $year,
            'number' => $number,
            'revision' => $revision,
            )
        );
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }

        $lists = array();
        $f_emp_id = '';
        $f_updated_on = '';
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[$row['group1']][$row['group2']][] = $row;
            $f_emp_id = $row['f_emp_id'];
            $f_updated_on = $row['f_updated_on'];
        }
        $sth->free();
        return array($lists, $f_emp_id, $f_updated_on);
    }

    /**
     * 他者評価データの保存
     * @param type $data
     * @param type $mode
     * @throws Exception
     */
    public function save_facilitate($data, $mode)
    {
        $this->db->beginNestedTransaction();

        $sth = $this->db->prepare("SELECT update_ldr_novice_facilitate_assessment(:emp_id, :f_emp_id, :guideline_id, :year, :number, :assessment)");

        // LOOP
        foreach ($data['assessment'] as $gid => $assessment) {
            // FIELD DATA
            $field = array(
                'emp_id' => $data['emp_id'],
                'f_emp_id' => $this->emp_id,
                'guideline_id' => $gid,
                'year' => $data['year'],
                'assessment' => $assessment,
                'number' => $data['number'],
            );

            // UPDATE or INSERT
            $res = $sth->execute($field);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " guideline_id = {$gid} ERROR: " . $res->getDebugInfo());
            }
        }

        $sth->free();
        $this->db->completeNestedTransaction();

        // Log
        if ($mode !== 'auto') {
            $this->log->log('保存しました', "year={$data['year']} guideline_id=" . implode(",", array_keys($data['assessment'])));
        }
    }

    /**
     * 評価データの取得
     * @param type $emp_id
     * @param type $year
     * @return type
     */
    public function full_assessment($emp_id, $year)
    {
        $revision = $this->current_revision();

        $sql = "
            SELECT
                g.guideline_id,
                g.group1,
                g.group2,
                g.guideline,
                a.assessment as assessment,
                a.number,
                b.assessment as f_assessment
            FROM ldr_novice_guideline g
            JOIN ldr_novice_guideline_group1 g1 ON g.revision_id = g1.revision_id AND g.group1=g1.group1 AND NOT g1.del_flg
            JOIN ldr_novice_guideline_group2 g2 ON g.revision_id = g2.revision_id AND g.group2=g2.group2 AND NOT g2.del_flg
            LEFT JOIN ldr_novice_assessment a ON a.emp_id= :emp_id AND a.guideline_id=g.guideline_id AND a.year = :year
            LEFT JOIN ldr_novice_facilitate_assessment b ON b.emp_id= a.emp_id AND b.guideline_id=g.guideline_id AND b.year = a.year AND b.number = a.number
            WHERE NOT g.del_flg
              AND g.revision_id = :revision
            ORDER BY g1.order_no, g2.order_no, g.order_no, g.guideline_id
            ";
        $sth = $this->db->prepare($sql);
        $res = $sth->execute(array(
            'emp_id' => $emp_id,
            'year' => $year,
            'revision' => $revision,
            )
        );
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            if (empty($row['number'])) {
                $row['number'] = 1;
            }
            $lists[$row['group1']][$row['group2']][$row['guideline_id']][$row['number']] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 評価データの集計
     * @param type $year
     * @return type
     */
    public function assessment_analysis($year)
    {
        $revision = $this->current_revision();

        $sql = "
            SELECT
                g.guideline_id,
                g.group1,
                g.group2,
                a.number,
                g.guideline,
                SUM(CASE WHEN a.assessment=1 THEN 1 ELSE 0 END) as a1,
                SUM(CASE WHEN a.assessment=2 THEN 1 ELSE 0 END) as a2,
                SUM(CASE WHEN a.assessment=3 THEN 1 ELSE 0 END) as a3,
                SUM(CASE WHEN a.assessment=4 THEN 1 ELSE 0 END) as a4,
                SUM(CASE WHEN b.assessment=1 THEN 1 ELSE 0 END) as f1,
                SUM(CASE WHEN b.assessment=2 THEN 1 ELSE 0 END) as f2,
                SUM(CASE WHEN b.assessment=3 THEN 1 ELSE 0 END) as f3,
                SUM(CASE WHEN b.assessment=4 THEN 1 ELSE 0 END) as f4,
                SUM(CASE WHEN a.assessment=1 THEN 1 ELSE 0 END) - SUM(CASE WHEN b.assessment=1 THEN 1 ELSE 0 END) as d1,
                SUM(CASE WHEN a.assessment=2 THEN 1 ELSE 0 END) - SUM(CASE WHEN b.assessment=2 THEN 1 ELSE 0 END) as d2,
                SUM(CASE WHEN a.assessment=3 THEN 1 ELSE 0 END) - SUM(CASE WHEN b.assessment=3 THEN 1 ELSE 0 END) as d3,
                SUM(CASE WHEN a.assessment=4 THEN 1 ELSE 0 END) - SUM(CASE WHEN b.assessment=4 THEN 1 ELSE 0 END) as d4
            FROM ldr_novice_guideline g
            LEFT JOIN ldr_novice_assessment a ON a.guideline_id=g.guideline_id AND a.year = :year
            LEFT JOIN ldr_novice_facilitate_assessment b ON b.emp_id= a.emp_id AND b.guideline_id=g.guideline_id AND b.year = a.year AND b.number = a.number
            WHERE NOT g.del_flg
              AND g.revision_id = :revision
            GROUP BY g.group1, g.group2, g.guideline_id, a.number, g.guideline
            ORDER BY g.group1, g.group2, g.guideline_id, a.number
        ";
        $sth = $this->db->prepare($sql);
        $res = $sth->execute(array(
            'year' => $year,
            'revision' => $revision,
            )
        );
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            if (empty($row['number'])) {
                $row['number'] = 1;
            }
            $lists[$row['group1']][$row['group2']][$row['guideline_id']]['guideline'] = $row['guideline'];
            $lists[$row['group1']][$row['group2']][$row['guideline_id']][$row['number']] = $row;
        }
        $sth->free();
        return $lists;
    }

}
