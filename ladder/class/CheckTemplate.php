<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'ladder/class/Log.php';

/**
 * 振り返りテンプレート
 */
class Ldr_CheckTemplate extends Model
{

    var $log;

    /**
     * コンストラクタ
     */
    public function Ldr_CheckTemplate()
    {
        parent::connectDB();
        $this->log = new Ldr_log('check_template');
    }

    /**
     * validate
     * @param type $data
     * @return string
     */
    public function validate($data)
    {
        $error = array();

        // テンプレート名
        if (Validation::is_empty($data['title'])) {
            $error['title'][] = 'テンプレート名が入力されていません';
        }

        // フォーム
        if (count($data['form']) <= 0) {
            $error['form'][] = 'フォームが選択されていません';
        }
        foreach ($data['form'] as $form_id => $row) {
            if (Validation::is_empty($row['name'])) {
                $error[$form_id]['name'][] = '質問文が入力されていません';
            }
            if (Validation::is_empty($row['size']) && ( $row['type'] === 'text' || $row['type'] === 'textarea')) {
                $error[$form_id]['size'][] = '入力幅が選択されていません';
            }
            if ($row['type'] === 'textarea') {
                if (Validation::is_empty($row['rows'])) {
                    $error[$form_id]['rows'][] = '入力行が入力されていません';
                }
                if ($row['rows'] < 2 || $row['rows'] > 10) {
                    $error[$form_id]['rows'][] = '入力行は2〜10の範囲内で入力してください';
                }
            }

            if (Validation::is_empty(trim($row['select'])) && ( $row['type'] === 'radio' || $row['type'] === 'checkbox' || $row['type'] === 'select')) {
                $error[$form_id]['select'][] = '選択肢が入力されていません';
            }
        }

        return $error;
    }

    /**
     * 保存
     * @param type $data
     * @throws Exception
     */
    public function save($data)
    {
        $this->db->beginNestedTransaction();

        // 選択肢の行頭行末の改行を取り除く
        foreach ($data['form'] as $form_id => $row) {
            $data['form'][$form_id]['select'] = trim($row['select']);
        }

        $data['form'] = serialize($data['form']);

        // UPDATE or INSERT
        $sth = $this->db->prepare("SELECT update_ldr_check_template (:tmpl_id,:title,:form)", array('integer', 'text', 'text'), MDB2_PREPARE_MANIP);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }
        if (empty($data['tmpl_id'])) {
            $data['tmpl_id'] = $id = $this->db->lastInsertId('ldr_check_template', 'tmpl_id');
        }
        $sth->free();
        $this->db->completeNestedTransaction();
        $this->log->log("保存しました", "tmpl_id={$data['tmpl_id']} title={$data['title']}");
    }

    /**
     * 一覧
     * @return type
     */
    public function lists()
    {
        $sql = "
            SELECT
                tmpl_id,
                title,
                form,
                updated_on
            FROM ldr_check_template
            WHERE
                NOT del_flg
            ORDER BY updated_on DESC
        ";

        $res = $this->db->query($sql);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['form'] = unserialize($row['form']);
            $lists[] = $row;
        }

        return $lists;
    }

    /**
     * 一件取得
     * @param type $id tmpl_id
     * @return type
     * @throws Exception
     */
    public function find($id)
    {
        if (is_null($id)) {
            return array();
        }

        $sql = "
            SELECT
                tmpl_id,
                title,
                form,
                updated_on
            FROM ldr_check_template
            WHERE
                NOT del_flg
                AND tmpl_id= :tmpl_id
        ";
        $row = $this->db->extended->getRow(
            $sql, null, array('tmpl_id' => $id), array('integer'), MDB2_FETCHMODE_ASSOC
        );
        if (PEAR::isError($row)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $row->getDebugInfo());
        }

        $row['form'] = unserialize($row['form']);
        return $row;
    }

    /**
     * 削除
     * @param type $id
     * @throws Exception
     */
    public function remove($id)
    {
        $this->db->beginNestedTransaction();
        $sth = $this->db->prepare("UPDATE ldr_check_template SET del_flg='t' WHERE tmpl_id = ?", array('integer'), MDB2_PREPARE_MANIP);
        $res = $sth->execute(array($id));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }
        $sth->free();
        $this->db->completeNestedTransaction();

        // Log
        $this->log->log("削除しました", "tmpl_id=$id");
    }

}
