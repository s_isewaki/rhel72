<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'ladder/class/Validation.php';
require_once 'ladder/class/Log.php';
require_once 'ladder/class/Config.php';

class Ldr_AnalysisLadder extends Model
{

    var $log;
    var $class_sql;
    var $view_class;

    /**
     * コンストラクタ
     */
    public function Ldr_AnalysisLadder()
    {
        parent::connectDB();
        $this->log = new Ldr_log('analysis_ladder');

        $conf = new Ldr_Config();
        $this->view_class = $conf->view_class();
        $this->class_sql = Ldr_Util::class_full_name_sql($this->view_class);
    }

    /**
     * 軸リスト
     * @param type $axis
     * @return type
     */
    public function axis_lists($axis = null)
    {
        $list = array(
            'class' => array(
                'title' => '部署',
                'id' => 'dept_id',
                'name' => 'class_full_name',
                'order' => 'c1_order_no, c2_order_no, c3_order_no, c4_order_no',
                'group' => 'dept_id, class_full_name, c1_order_no, c2_order_no, c3_order_no, c4_order_no',
            ),
            'status' => array(
                'title' => '役職',
                'id' => 'st_id',
                'name' => 'st_nm',
                'order' => 'st_order_no',
                'group' => 'st_id, st_nm, st_order_no',
            ),
            'job' => array(
                'title' => '職種',
                'id' => 'job_id',
                'name' => 'job_nm',
                'order' => 'job_order_no',
                'group' => 'job_id, job_nm, job_order_no',
            ),
            'sex' => array(
                'title' => '性別',
                'id' => 'emp_sex',
                'name' => "CASE emp_sex WHEN '1' THEN '男' WHEN '2' THEN '女' ELSE '不明' END",
                'order' => 'emp_sex',
                'group' => 'emp_sex',
            ),
            'age' => array(
                'title' => '年齢',
                'id' => 'age5',
                'name' => "CASE WHEN age5 IS NOT NULL THEN age5||'歳 〜 '||age5+4||'歳' ELSE '不明' END",
                'order' => 'age5',
                'group' => 'age5',
            ),
            'level_year' => array(
                'title' => '認定年度',
                'id' => 'level_year',
                'name' => "level_year||'年度'",
                'order' => 'level_year',
                'group' => 'level_year',
            ),
            'level_age' => array(
                'title' => '認定後経過年数',
                'id' => 'level_age',
                'name' => "level_age||'年 〜'",
                'order' => 'level_age',
                'group' => 'level_age',
            ),
            'nurse_age' => array(
                'title' => '臨床経験年数',
                'id' => 'nurse_age5',
                'name' => "nurse_age5||'年 〜 '||nurse_age5+4||'年'",
                'order' => 'nurse_age5',
                'group' => 'nurse_age5',
            ),
            'hospital_age' => array(
                'title' => '当院経過年数',
                'id' => 'hospital_age5',
                'name' => "hospital_age5||'年 〜 '||hospital_age5+4||'年'",
                'order' => 'hospital_age5',
                'group' => 'hospital_age5',
            ),
            'ward_age' => array(
                'title' => '部署経過年数',
                'id' => 'ward_age5',
                'name' => "ward_age5||'年 〜 '||ward_age5+4||'年'",
                'order' => 'ward_age5',
                'group' => 'ward_age5',
            ),
            'license_age' => array(
                'title' => '免許取得経過年数',
                'id' => 'license_age5',
                'name' => "license_age5||'年 〜 '||license_age5+4||'年'",
                'order' => 'license_age5',
                'group' => 'license_age5',
            ),
        );

        if ($axis) {
            return $list[$axis];
        }
        else {
            return $list;
        }
    }

    /**
     * CSV出力
     * @param type $cond
     * @return type
     */
    public function export_csv($lists, $cond)
    {
        if ($cond['x'] === 'level') {
            return $this->export_csv_level($lists, $cond);
        }
        else {
            return $this->export_csv_ladder($lists, $cond);
        }
    }

    /**
     * csv出力(X軸=レベル)
     * @param type $cond
     * @return type
     */
    private function export_csv_level($lists, $cond)
    {
        // 軸
        $y_axis = $this->axis_lists($cond['y']);

        $fp = fopen('php://temp', 'r+b');
        foreach ($lists as $data) {
            fputcsv($fp, array(
                $data['label'],
                $data['lv0count'],
                $data['lv1count'],
                $data['lv2count'],
                $data['lv3count'],
                $data['lv4count'],
                $data['lv5count'],
                $data['total'],
            ));
        }
        rewind($fp);

        $csv = "{$y_axis['title']},無し,I,II,III,IV,V,合計\r\n";
        $csv .= str_replace(PHP_EOL, "\r\n", stream_get_contents($fp));

        // Log
        $this->log->log("CSVをエクスポートしました({$cond['y']},level)");

        return $csv;
    }

    /**
     * csv出力
     * @param type $cond
     * @return type
     */
    private function export_csv_ladder($data, $cond)
    {
        // 軸
        $y_axis = $this->axis_lists($cond['y']);

        $fp = fopen('php://temp', 'r+b');

        // 見出し
        $label = array($y_axis['title'], 'レベル');
        foreach ($data['x'] as $x) {
            $label[] = $x['label'];
        }
        fputcsv($fp, $label);

        // CSV
        foreach ($data['y'] as $y_id => $y_label) {

            foreach (range(0, 5) as $lv) {
                $row = array($y_label, $lv);

                foreach ($data['x'] as $x) {
                    $row[] = $data['list'][$y_id][$lv][$x['y_id']]['count'] + 0;
                }

                fputcsv($fp, $row);
            }
        }
        rewind($fp);

        $csv = str_replace(PHP_EOL, "\r\n", stream_get_contents($fp));

        // Log
        $this->log->log("CSVをエクスポートしました({$cond['y']},{$cond['x']})");

        return $csv;
    }

    /**
     * 一覧の取得
     * @param type $type
     * @param type $cond
     * @return type
     */
    public function lists($type, $cond = null)
    {
        switch ($type) {
            case 'ladder':
                if ($cond['x'] === 'level') {
                    return $this->lists_level($cond);
                }
                else {
                    return $this->lists_ladder($cond);
                }

            case 'class':
                return $this->lists_class($cond);

            case 'st':
                return $this->lists_st($cond);

            case 'job':
                return $this->lists_job($cond);
        }
    }

    /**
     * ラダー集計(X軸=レベル)
     * @param type $cond
     * @return type
     */
    private function lists_level($cond)
    {
        // 部署絞り込み
        if ($cond['class']) {
            $cond_class = implode(",", array_map('Ldr_Util::map_quote', $cond['class']));
        }
        else {
            return array();
        }

        // 軸
        $y_axis = $this->axis_lists($cond['y']);

        $sql = "
            SELECT
                SUM(CASE WHEN ldr.level>=1 then 0 else 1 end) AS lv0count,
                SUM(CASE WHEN ldr.level=1 then 1 else 0 end) AS lv1count,
                SUM(CASE WHEN ldr.level=2 then 1 else 0 end) AS lv2count,
                SUM(CASE WHEN ldr.level=3 then 1 else 0 end) AS lv3count,
                SUM(CASE WHEN ldr.level=4 then 1 else 0 end) AS lv4count,
                SUM(CASE WHEN ldr.level=5 then 1 else 0 end) AS lv5count,
                {$y_axis['id']} as y_id,
                {$y_axis['name']} as label
            FROM (
                SELECT
                    l.level,
                    emp_sex,
                    c3.dept_id,
                    {$this->class_sql},
                    st.st_id,
                    st.st_nm,
                    job.job_id,
                    job.job_nm,
                    c1.order_no AS c1_order_no,
                    c2.order_no AS c2_order_no,
                    c3.order_no AS c3_order_no,
                    c4.order_no AS c4_order_no,
                    st.order_no AS st_order_no,
                    job.order_no AS job_order_no,
                    CASE WHEN emp_birth<>'' THEN trunc(extract(year from age(current_date, to_date(emp_birth, 'YYYYMMDD'))) / 5) * 5 ELSE null END as age5,
                    CASE level
                        WHEN 1 THEN extract(year from lv1_date - interval '3 month')
                        WHEN 2 THEN extract(year from lv2_date - interval '3 month')
                        WHEN 3 THEN extract(year from lv3_date - interval '3 month')
                        WHEN 4 THEN extract(year from lv4_date - interval '3 month')
                        WHEN 5 THEN extract(year from lv5_date - interval '3 month')
                        ELSE null
                    END as level_year,
                    CASE level
                        WHEN 1 THEN extract(year from age(current_date, lv1_date))
                        WHEN 2 THEN extract(year from age(current_date, lv2_date))
                        WHEN 3 THEN extract(year from age(current_date, lv3_date))
                        WHEN 4 THEN extract(year from age(current_date, lv4_date))
                        WHEN 5 THEN extract(year from age(current_date, lv5_date))
                        ELSE null
                    END as level_age,
                    trunc(extract(year from age(current_date, nurse_date)) / 5) * 5 as nurse_age5,
                    trunc(extract(year from age(current_date, hospital_date)) / 5) * 5 as hospital_age5,
                    trunc(extract(year from age(current_date, ward_date)) / 5) * 5 as ward_age5,
                    trunc(extract(year from age(current_date, license_date)) / 5) * 5 as license_age5
                FROM empmst e
                JOIN authmst a USING (emp_id)
                JOIN classmst c1 ON c1.class_id = e.emp_class
                JOIN atrbmst c2 ON c2.class_id = e.emp_class AND c2.atrb_id = e.emp_attribute AND NOT atrb_del_flg
                JOIN deptmst c3 ON c3.atrb_id = e.emp_attribute AND c3.dept_id = e.emp_dept AND NOT dept_del_flg
                LEFT JOIN classroom c4 ON c4.dept_id = e.emp_dept AND (c4.room_id = e.emp_room OR c4.room_id IS NULL) AND NOT room_del_flg
                JOIN jobmst job ON job.job_id = e.emp_job AND NOT job_del_flg
                JOIN stmst st ON st.st_id = e.emp_st AND NOT st_del_flg
                JOIN ldr_analysis_authority ac ON ac.class_id = c1.class_id
                    AND ac.atrb_id = c2.atrb_id
                    AND ac.dept_id = c3.dept_id
                    AND (ac.room_id=c4.room_id OR (ac.room_id IS NULL AND c4.room_id IS NULL))
                    AND ac.auth_flg
                    AND NOT ac.del_flg
                JOIN ldr_analysis_authority_st  aas ON aas.st_id  = e.emp_st  AND aas.auth_flg AND NOT aas.del_flg
                JOIN ldr_analysis_authority_job aaj ON aaj.job_id = e.emp_job AND aaj.auth_flg AND NOT aaj.del_flg
                LEFT JOIN ldr_employee l USING (emp_id)
                WHERE
                    c3.dept_id IN ({$cond_class})
                AND a.emp_del_flg='f'
            ) ldr
            GROUP BY {$y_axis['group']}
            ORDER BY {$y_axis['order']}
        ";

        $sth = $this->db->prepare($sql, array(), MDB2_PREPARE_RESULT);
        $res = $sth->execute();
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }
        $lists = array();
        $total = array('y_id' => 'total', 'label' => '合計');
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['total'] = $row['lv0count'] + $row['lv1count'] + $row['lv2count'] + $row['lv3count'] + $row['lv4count'] + $row['lv5count'];
            $lists[] = $row;
            $total['lv0count'] += $row['lv0count'];
            $total['lv1count'] += $row['lv1count'];
            $total['lv2count'] += $row['lv2count'];
            $total['lv3count'] += $row['lv3count'];
            $total['lv4count'] += $row['lv4count'];
            $total['lv5count'] += $row['lv5count'];
            $total['total'] += $row['total'];
        }
        $lists[] = $total;
        return $lists;
    }

    /**
     * ラダー集計
     * @param type $cond
     * @return type
     */
    private function lists_ladder($cond)
    {
        // 部署絞り込み
        if ($cond['class']) {
            $cond_class = implode(",", array_map('Ldr_Util::map_quote', $cond['class']));
        }
        else {
            return array();
        }

        // 軸
        $x_axis = $this->axis_lists($cond['x']);
        $y_axis = $this->axis_lists($cond['y']);

        $sql = "
            SELECT
                COUNT(*) AS count,
                level,
                {$x_axis['id']} as x_id,
                {$y_axis['id']} as y_id,
                {$y_axis['name']} as label
            FROM (
                SELECT
                    COALESCE(l.level,0) AS level,
                    emp_sex,
                    c3.dept_id,
                    {$this->class_sql},
                    st.st_id,
                    st.st_nm,
                    job.job_id,
                    job.job_nm,
                    c1.order_no AS c1_order_no,
                    c2.order_no AS c2_order_no,
                    c3.order_no AS c3_order_no,
                    c4.order_no AS c4_order_no,
                    st.order_no AS st_order_no,
                    job.order_no AS job_order_no,
                    CASE WHEN emp_birth<>'' THEN trunc(extract(year from age(current_date, to_date(emp_birth, 'YYYYMMDD'))) / 5) * 5 ELSE null END as age5,
                    CASE level
                        WHEN 1 THEN extract(year from lv1_date - interval '3 month')
                        WHEN 2 THEN extract(year from lv2_date - interval '3 month')
                        WHEN 3 THEN extract(year from lv3_date - interval '3 month')
                        WHEN 4 THEN extract(year from lv4_date - interval '3 month')
                        WHEN 5 THEN extract(year from lv5_date - interval '3 month')
                        ELSE null
                    END as level_year,
                    CASE level
                        WHEN 1 THEN extract(year from age(current_date, lv1_date))
                        WHEN 2 THEN extract(year from age(current_date, lv2_date))
                        WHEN 3 THEN extract(year from age(current_date, lv3_date))
                        WHEN 4 THEN extract(year from age(current_date, lv4_date))
                        WHEN 5 THEN extract(year from age(current_date, lv5_date))
                        ELSE null
                    END as level_age,
                    trunc(extract(year from age(current_date, nurse_date)) / 5) * 5 as nurse_age5,
                    trunc(extract(year from age(current_date, hospital_date)) / 5) * 5 as hospital_age5,
                    trunc(extract(year from age(current_date, ward_date)) / 5) * 5 as ward_age5,
                    trunc(extract(year from age(current_date, license_date)) / 5) * 5 as license_age5
                FROM empmst e
                JOIN authmst a USING (emp_id)
                JOIN classmst c1 ON c1.class_id = e.emp_class
                JOIN atrbmst c2 ON c2.class_id = e.emp_class AND c2.atrb_id = e.emp_attribute AND NOT atrb_del_flg
                JOIN deptmst c3 ON c3.atrb_id = e.emp_attribute AND c3.dept_id = e.emp_dept AND NOT dept_del_flg
                LEFT JOIN classroom c4 ON c4.dept_id = e.emp_dept AND (c4.room_id = e.emp_room OR c4.room_id IS NULL) AND NOT room_del_flg
                JOIN jobmst job ON job.job_id = e.emp_job AND NOT job_del_flg
                JOIN stmst st ON st.st_id = e.emp_st AND NOT st_del_flg
                JOIN ldr_analysis_authority ac ON ac.class_id = c1.class_id
                    AND ac.atrb_id = c2.atrb_id
                    AND ac.dept_id = c3.dept_id
                    AND (ac.room_id=c4.room_id OR (ac.room_id IS NULL AND c4.room_id IS NULL))
                    AND ac.auth_flg
                    AND NOT ac.del_flg
                JOIN ldr_analysis_authority_st  aas ON aas.st_id  = e.emp_st  AND aas.auth_flg AND NOT aas.del_flg
                JOIN ldr_analysis_authority_job aaj ON aaj.job_id = e.emp_job AND aaj.auth_flg AND NOT aaj.del_flg
                LEFT JOIN ldr_employee l USING (emp_id)
                WHERE
                    c3.dept_id IN ({$cond_class})
                AND a.emp_del_flg='f'
            ) ldr
            GROUP BY {$x_axis['group']}, {$y_axis['group']}, level
            ORDER BY {$y_axis['order']}
        ";

        $sth = $this->db->prepare($sql, array(), MDB2_PREPARE_RESULT);
        $res = $sth->execute();
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }
        $lists = array();
        $xlabel = $this->lists_level(array('y' => $cond['x'], 'class' => $cond['class']));
        $ylabel = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $ylabel[$row['y_id']] = $row['label'];
            $lists[$row['y_id']][$row['level']][$row['x_id']] = $row;
            $lists[$row['y_id']][$row['level']]['total']['count'] += $row['count'];
        }

        return array('x' => $xlabel, 'y' => $ylabel, 'list' => $lists);
    }

    /**
     * 職員一覧の取得
     * @param type $type
     * @param type $cond
     * @return type
     */
    public function emp_lists($cond)
    {
        if ($cond['x'] === 'level') {
            return $this->emp_lists_level($cond);
        }
        else {
            return $this->emp_lists_ladder($cond);
        }
    }

    /**
     * 職員一覧(X軸=レベル)の取得
     * @param type $cond
     * @return type
     */
    private function emp_lists_level($cond)
    {
        // レベル
        if (empty($cond['level'])) {
            $where = "ldr.level is NULL";
        }
        else {
            $where = "ldr.level = :level";
        }

        // 部署絞り込み
        if ($cond['class']) {
            $cond_class = implode(",", array_map('Ldr_Util::map_quote', $cond['class']));
        }
        else {
            return array();
        }

        // 無し
        if ($cond['y_id'] === '') {
            $cond['y_id'] = -1;
        }

        // 軸
        $y_axis = $this->axis_lists($cond['y']);
        $where .= " AND {$y_axis['id']} = :{$y_axis['id']}";

        $sql = "
            SELECT
                emp_id,
                emp_name AS name,
                level,
                class_full_name
            FROM (
                SELECT
                    e.emp_id,
                    e.emp_lt_nm || ' ' || e.emp_ft_nm AS emp_name,
                    l.level,
                    emp_sex,
                    c3.dept_id,
                    {$this->class_sql},
                    st.st_id,
                    st.st_nm,
                    job.job_id,
                    job.job_nm,
                    c1.order_no AS c1_order_no,
                    c2.order_no AS c2_order_no,
                    c3.order_no AS c3_order_no,
                    c4.order_no AS c4_order_no,
                    st.order_no AS st_order_no,
                    job.order_no AS job_order_no,
                    CASE WHEN emp_birth<>'' THEN trunc(extract(year from age(current_date, to_date(emp_birth, 'YYYYMMDD'))) / 5) * 5 ELSE -1 END as age5,
                    CASE level
                        WHEN 1 THEN extract(year from lv1_date - interval '3 month')
                        WHEN 2 THEN extract(year from lv2_date - interval '3 month')
                        WHEN 3 THEN extract(year from lv3_date - interval '3 month')
                        WHEN 4 THEN extract(year from lv4_date - interval '3 month')
                        WHEN 5 THEN extract(year from lv5_date - interval '3 month')
                        ELSE -1
                    END as level_year,
                    CASE level
                        WHEN 1 THEN extract(year from age(current_date, lv1_date))
                        WHEN 2 THEN extract(year from age(current_date, lv2_date))
                        WHEN 3 THEN extract(year from age(current_date, lv3_date))
                        WHEN 4 THEN extract(year from age(current_date, lv4_date))
                        WHEN 5 THEN extract(year from age(current_date, lv5_date))
                        ELSE -1
                    END as level_age,
                    CASE WHEN nurse_date IS NOT NULL THEN trunc(extract(year from age(current_date, nurse_date)) / 5) * 5 ELSE -1 END as nurse_age5,
                    CASE WHEN hospital_date IS NOT NULL THEN trunc(extract(year from age(current_date, hospital_date)) / 5) * 5 ELSE -1 END as hospital_age5,
                    CASE WHEN ward_date IS NOT NULL THEN trunc(extract(year from age(current_date, ward_date)) / 5) * 5 ELSE -1 END as ward_age5,
                    CASE WHEN license_date IS NOT NULL THEN trunc(extract(year from age(current_date, license_date)) / 5) * 5 ELSE -1 END as license_age5
                FROM empmst e
                JOIN authmst a USING (emp_id)
                JOIN classmst c1 ON c1.class_id = e.emp_class
                JOIN atrbmst c2 ON c2.class_id = e.emp_class AND c2.atrb_id = e.emp_attribute AND NOT atrb_del_flg
                JOIN deptmst c3 ON c3.atrb_id = e.emp_attribute AND c3.dept_id = e.emp_dept AND NOT dept_del_flg
                LEFT JOIN classroom c4 ON c4.dept_id = e.emp_dept AND (c4.room_id = e.emp_room OR c4.room_id IS NULL) AND NOT room_del_flg
                JOIN jobmst job ON job.job_id = e.emp_job AND NOT job_del_flg
                JOIN stmst st ON st.st_id = e.emp_st AND NOT st_del_flg
                JOIN ldr_analysis_authority ac ON ac.class_id = c1.class_id
                    AND ac.atrb_id = c2.atrb_id
                    AND ac.dept_id = c3.dept_id
                    AND (ac.room_id=c4.room_id OR (ac.room_id IS NULL AND c4.room_id IS NULL))
                    AND ac.auth_flg
                    AND NOT ac.del_flg
                JOIN ldr_analysis_authority_st  aas ON aas.st_id  = e.emp_st  AND aas.auth_flg AND NOT aas.del_flg
                JOIN ldr_analysis_authority_job aaj ON aaj.job_id = e.emp_job AND aaj.auth_flg AND NOT aaj.del_flg
                LEFT JOIN ldr_employee l USING (emp_id)
                WHERE
                    c3.dept_id IN ({$cond_class})
                AND a.emp_del_flg='f'
            ) ldr
            WHERE {$where}
            ORDER BY c1_order_no, c2_order_no, c3_order_no, c4_order_no, st_order_no, job_order_no
        ";

        $sth = $this->db->prepare($sql, array(), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array('level' => $cond['level'], $y_axis['id'] => $cond['y_id']));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }
        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row;
        }
        return $lists;
    }

    /**
     * 職員一覧の取得
     * @param type $cond
     * @return type
     */
    private function emp_lists_ladder($cond)
    {
        // レベル
        if (empty($cond['level'])) {
            $where = "ldr.level is NULL";
        }
        else {
            $where = "ldr.level = :level";
        }

        // 部署絞り込み
        if ($cond['class']) {
            $cond_class = implode(",", array_map('Ldr_Util::map_quote', $cond['class']));
        }
        else {
            return array();
        }

        // 無し
        if ($cond['y_id'] === '') {
            $cond['y_id'] = -1;
        }

        // 軸
        $x_axis = $this->axis_lists($cond['x']);
        $y_axis = $this->axis_lists($cond['y']);
        $where .= " AND {$x_axis['id']} = :{$x_axis['id']} AND {$y_axis['id']} = :{$y_axis['id']}";

        $sql = "
            SELECT
                emp_id,
                emp_name AS name,
                level,
                class_full_name
            FROM (
                SELECT
                    e.emp_id,
                    e.emp_lt_nm || ' ' || e.emp_ft_nm AS emp_name,
                    l.level,
                    emp_sex,
                    c3.dept_id,
                    {$this->class_sql},
                    st.st_id,
                    st.st_nm,
                    job.job_id,
                    job.job_nm,
                    c1.order_no AS c1_order_no,
                    c2.order_no AS c2_order_no,
                    c3.order_no AS c3_order_no,
                    c4.order_no AS c4_order_no,
                    st.order_no AS st_order_no,
                    job.order_no AS job_order_no,
                    CASE WHEN emp_birth<>'' THEN trunc(extract(year from age(current_date, to_date(emp_birth, 'YYYYMMDD'))) / 5) * 5 ELSE -1 END as age5,
                    CASE level
                        WHEN 1 THEN extract(year from lv1_date - interval '3 month')
                        WHEN 2 THEN extract(year from lv2_date - interval '3 month')
                        WHEN 3 THEN extract(year from lv3_date - interval '3 month')
                        WHEN 4 THEN extract(year from lv4_date - interval '3 month')
                        WHEN 5 THEN extract(year from lv5_date - interval '3 month')
                        ELSE -1
                    END as level_year,
                    CASE level
                        WHEN 1 THEN extract(year from age(current_date, lv1_date))
                        WHEN 2 THEN extract(year from age(current_date, lv2_date))
                        WHEN 3 THEN extract(year from age(current_date, lv3_date))
                        WHEN 4 THEN extract(year from age(current_date, lv4_date))
                        WHEN 5 THEN extract(year from age(current_date, lv5_date))
                        ELSE -1
                    END as level_age,
                    CASE WHEN nurse_date IS NOT NULL THEN trunc(extract(year from age(current_date, nurse_date)) / 5) * 5 ELSE -1 END as nurse_age5,
                    CASE WHEN hospital_date IS NOT NULL THEN trunc(extract(year from age(current_date, hospital_date)) / 5) * 5 ELSE -1 END as hospital_age5,
                    CASE WHEN ward_date IS NOT NULL THEN trunc(extract(year from age(current_date, ward_date)) / 5) * 5 ELSE -1 END as ward_age5,
                    CASE WHEN license_date IS NOT NULL THEN trunc(extract(year from age(current_date, license_date)) / 5) * 5 ELSE -1 END as license_age5
                FROM empmst e
                JOIN authmst a USING (emp_id)
                JOIN classmst c1 ON c1.class_id = e.emp_class
                JOIN atrbmst c2 ON c2.class_id = e.emp_class AND c2.atrb_id = e.emp_attribute AND NOT atrb_del_flg
                JOIN deptmst c3 ON c3.atrb_id = e.emp_attribute AND c3.dept_id = e.emp_dept AND NOT dept_del_flg
                LEFT JOIN classroom c4 ON c4.dept_id = e.emp_dept AND (c4.room_id = e.emp_room OR c4.room_id IS NULL) AND NOT room_del_flg
                JOIN jobmst job ON job.job_id = e.emp_job AND NOT job_del_flg
                JOIN stmst st ON st.st_id = e.emp_st AND NOT st_del_flg
                JOIN ldr_analysis_authority ac ON ac.class_id = c1.class_id
                    AND ac.atrb_id = c2.atrb_id
                    AND ac.dept_id = c3.dept_id
                    AND (ac.room_id=c4.room_id OR (ac.room_id IS NULL AND c4.room_id IS NULL))
                    AND ac.auth_flg
                    AND NOT ac.del_flg
                JOIN ldr_analysis_authority_st  aas ON aas.st_id  = e.emp_st  AND aas.auth_flg AND NOT aas.del_flg
                JOIN ldr_analysis_authority_job aaj ON aaj.job_id = e.emp_job AND aaj.auth_flg AND NOT aaj.del_flg
                LEFT JOIN ldr_employee l USING (emp_id)
                WHERE
                    c3.dept_id IN ({$cond_class})
                AND a.emp_del_flg='f'
            ) ldr
            WHERE {$where}
            ORDER BY c1_order_no, c2_order_no, c3_order_no, c4_order_no, st_order_no, job_order_no
        ";

        $sth = $this->db->prepare($sql, array(), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array('level' => $cond['level'], $y_axis['id'] => $cond['y_id'], $x_axis['id'] => $cond['x_id']));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }
        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row;
        }
        return $lists;
    }

    /**
     * 担当部署一覧の取得
     * @param type $cond
     * @return type
     * @throws Exception
     */
    public function lists_class_approver($emp_id)
    {
        $sql = "
            SELECT
                c1.class_id,
                c2.atrb_id,
                c3.dept_id,
                c4.room_id,
                {$this->class_sql},
                class_nm,
                atrb_nm,
                dept_nm,
                room_nm
            FROM ldr_approver e
            JOIN classmst c1 USING (class_id)
            JOIN atrbmst c2 USING (atrb_id)
            JOIN deptmst c3 USING (dept_id)
            LEFT JOIN classroom c4 USING (room_id)
            WHERE emp_id = :emp_id
            ORDER BY c1.order_no, c2.order_no, c3.order_no, c4.order_no
        ";
        $sth = $this->db->prepare($sql, null, MDB2_PREPARE_RESULT);
        $res = $sth->execute(array('emp_id' => $emp_id));

        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }
        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['key'] = $row['class_id'] . "_" . $row['atrb_id'] . "_" . $row['dept_id'] . "_" . $row['room_id'];
            $row['name'] = empty($row['room_nm']) ? $row['dept_nm'] : $row['room_nm'];
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 部署一覧の取得
     * @param type $cond
     * @return type
     * @throws Exception
     */
    private function lists_class($cond = null)
    {
        if (empty($cond)) {
            $sql = "AND a.auth_flg";
            $types = array();
            $data = array();
        }
        else {
            $sql = "AND e.class_id = :class AND e.atrb_id = :atrb";
            $types = array('integer', 'integer');
            $data = array('class' => $cond['class'], 'atrb' => $cond['atrb']);
        }

        $sql = "
            SELECT
                a.auth_flg,
                c1.class_id,
                c2.atrb_id,
                c3.dept_id,
                c4.room_id,
                {$this->class_sql},
                class_nm,
                atrb_nm,
                dept_nm,
                room_nm
            FROM (SELECT emp_class AS class_id, emp_attribute AS atrb_id, emp_dept AS dept_id, emp_room AS room_id FROM empmst GROUP BY emp_class, emp_attribute, emp_dept, emp_room) e
            JOIN classmst c1 USING (class_id)
            JOIN atrbmst c2 USING (atrb_id)
            JOIN deptmst c3 USING (dept_id)
            LEFT JOIN classroom c4 USING (room_id)
            LEFT JOIN ldr_analysis_authority a ON e.class_id = a.class_id AND e.atrb_id = a.atrb_id AND e.dept_id = a.dept_id AND (e.room_id=a.room_id OR (e.room_id IS NULL AND a.room_id IS NULL)) AND NOT a.del_flg
            WHERE true
                {$sql}
            ORDER BY c1.order_no, c2.order_no, c3.order_no, c4.order_no
        ";
        $sth = $this->db->prepare($sql, $types, MDB2_PREPARE_RESULT);
        $res = $sth->execute($data);

        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }
        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['key'] = $row['class_id'] . "_" . $row['atrb_id'] . "_" . $row['dept_id'] . "_" . $row['room_id'];
            $row['name'] = empty($row['room_nm']) ? $row['dept_nm'] : $row['room_nm'];
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 役職一覧
     * @param type $cond
     * @return type
     * @throws Exception
     */
    private function lists_st($cond = null)
    {
        if (!empty($cond)) {
            $cond_auth = "AND a.auth_flg";
        }

        $sql = "
            SELECT
                a.auth_flg,
                s.st_id,
                s.st_nm
            FROM stmst s
            LEFT JOIN ldr_analysis_authority_st a USING (st_id)
            WHERE
                NOT st_del_flg
                {$cond_auth}
            ORDER BY s.order_no asc
        ";
        $sth = $this->db->prepare($sql, null, MDB2_PREPARE_RESULT);
        $res = $sth->execute(null);

        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }
        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 職種一覧
     * @param type $cond
     * @return type
     * @throws Exception
     */
    private function lists_job()
    {

        $sql = "
            SELECT
                a.auth_flg,
                s.job_id,
                s.job_nm
            FROM jobmst s
            LEFT JOIN ldr_analysis_authority_job a ON s.job_id = a.job_id AND NOT a.del_flg
            WHERE
                NOT job_del_flg
            ORDER BY s.order_no asc
        ";
        $sth = $this->db->prepare($sql, null, MDB2_PREPARE_RESULT);
        $res = $sth->execute(null);

        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }
        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row;
        }
        return $lists;
    }

    /**
     * 保存
     * @param type $type
     * @param type $data
     * @return type
     */
    public function save($type, $data = null)
    {
        switch ($type) {
            case 'analysis':
                return $this->save_analysis_auth($data);
            case 'st':
                return $this->save_analysis_auth_st($data);
            case 'job':
                return $this->save_analysis_auth_job($data);
        }
    }

    /**
     * 分析対象部署の保存
     * @param type $data
     * @throws Exception
     */
    private function save_analysis_auth($data)
    {
        $this->db->beginNestedTransaction();
        // UPDATE
        $sth = $this->db->prepare("SELECT update_ldr_analysis_authority(?, ?, ?, ?, ?)", array('integer', 'integer', 'integer', 'integer', 'boolean'), MDB2_PREPARE_MANIP);
        foreach ($data['class'] as $class) {
            list($c1, $c2, $c3, $c4) = explode("-", $class);
            $c5 = isset($data['auth'][$class]);
            $res = $sth->execute(array($c1, $c2, $c3, $c4, $c5));
            $msg .= $class . "=" . (isset($data['auth'][$class]) ? '1' : '0') . " ";
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
            }
        }
        $this->db->completeNestedTransaction();
        $sth->free();

        // Log
        $this->log->log('保存しました[分析対象部署]', $msg);
    }

    /**
     * 分析対象役職の保存
     * @param type $data
     * @throws Exception
     */
    private function save_analysis_auth_st($data)
    {
        $this->db->beginNestedTransaction();
        // UPDATE
        $sth = $this->db->prepare("SELECT update_ldr_analysis_authority_st(?, ?)", array('integer', 'boolean'), MDB2_PREPARE_MANIP);
        foreach ($data['st'] as $st) {
            $flg = isset($data['auth'][$st]);
            $res = $sth->execute(array($st, $flg));
            $msg .= $st . "=" . (isset($data['auth'][$st]) ? '1' : '0') . " ";
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
            }
        }
        $this->db->completeNestedTransaction();
        $sth->free();

        // Log
        $this->log->log('保存しました[分析対象役職]', $msg);
    }

    /**
     * 分析対象職種の保存
     * @param type $data
     * @throws Exception
     */
    private function save_analysis_auth_job($data)
    {
        $this->db->beginNestedTransaction();
        // UPDATE
        $sth = $this->db->prepare("SELECT update_ldr_analysis_authority_job(?, ?)", array('integer', 'boolean'), MDB2_PREPARE_MANIP);
        foreach ($data['job'] as $job) {
            $flg = isset($data['auth'][$job]);
            $res = $sth->execute(array($job, $flg));
            $msg .= $job . "=" . (isset($data['auth'][$job]) ? '1' : '0') . " ";
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
            }
        }
        $this->db->completeNestedTransaction();
        $sth->free();

        // Log
        $this->log->log('保存しました[分析対象職種]', $msg);
    }

}
