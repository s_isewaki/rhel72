<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'ladder/class/Validation.php';
require_once 'ladder/class/NoviceGuideline.php';
require_once 'ladder/class/Log.php';

class Ldr_NoviceAnalysisAssessment extends Model
{

    var $log;

    /**
     * コンストラクタ
     */
    public function __construct()
    {
        parent::connectDB();
        $this->log = new Ldr_log('novice_analysis_assessment');
        $this->gl = new Ldr_NoviceGuideline();
    }


    /**
     * 一覧
     * @param type $year
     * @param type $number
     * @return type
     * @throws Exception
     */
    public function lists($year, $number)
    {
        // ガイドライン一覧
        $guideline = $this->guideline_lists();

        // 評価一覧
        $assessments = $this->assessments($year, $number);
        $fassessments = $this->fassessments($year, $number);

        // SELECT
        $sql = "
            SELECT
                e.emp_id,
                e.emp_personal_id,
                e.emp_lt_nm || ' ' || e.emp_ft_nm as emp_name,
                emp_sex,
                emp_birth,
                graduation_year,
                school,
                license_date,
                nurse_date,
                hospital_date,
                ward_date,
                dept_nm || CASE WHEN room_nm IS NOT NULL THEN ' > ' || room_nm ELSE '' END AS class_full_name,
                job_nm ,
                st_nm
            FROM ldr_novice n
            JOIN empmst e USING (emp_id)
            JOIN authmst a USING (emp_id)
            JOIN classmst c1 ON c1.class_id = e.emp_class
            JOIN atrbmst c2 ON c2.class_id = e.emp_class AND c2.atrb_id = e.emp_attribute AND NOT atrb_del_flg
            JOIN deptmst c3 ON c3.atrb_id = e.emp_attribute AND c3.dept_id = e.emp_dept AND NOT dept_del_flg
            LEFT JOIN classroom c4 ON c4.dept_id = e.emp_dept AND (c4.room_id = e.emp_room OR c4.room_id IS NULL) AND NOT room_del_flg
            JOIN jobmst job ON job.job_id = e.emp_job AND NOT job_del_flg
            JOIN stmst st ON st.st_id = e.emp_st AND NOT st_del_flg
            LEFT JOIN ldr_employee l USING (emp_id)
        ";

        // cond
        $sql .= "WHERE NOT a.emp_del_flg AND n.year = :year ";

        $sth = $this->db->prepare($sql, null, MDB2_PREPARE_RESULT);
        $res = $sth->execute(array(
            'year' => $year,
        ));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['guideline_count'] = count($guideline);
            $row['assessment'] = $assessments[$row['emp_id']];
            $row['fassessment'] = $fassessments[$row['emp_id']];
            $row['sum_percent'] = sprintf("%.1f", $row['assessment']['sum'] / (count($guideline) * 4) * 100);
            $row['percent'] = sprintf("%.1f", $row['assessment']['count'] / count($guideline) * 100);
            $row['sum_percentf'] = sprintf("%.1f", $row['fassessment']['sum'] / (count($guideline) * 4) * 100);
            $row['percentf'] = sprintf("%.1f", $row['fassessment']['count'] / count($guideline) * 100);
            $list[] = $row;
        }
        return $list;
    }

    /**
     * CSV出力
     * @param type $year
     * @param type $number
     * @return type
     */
    public function export_csv($year, $number)
    {
        // 一覧
        $list = $this->lists($year, $number);

        // ガイドライン一覧
        $guideline = $this->guideline_lists();

        $fp = fopen('php://temp', 'r+b');

        // 見出し
        $label = array(
            '年度',
            '評価回',
            '職員ID',
            '職員名',
            '性別',
            '生年月日',
            '卒業年',
            '出身校',
            '免許取得日',
            '臨床開始日',
            '病院開始日',
            '部署開始日',
            '部署',
            '職種',
            '役職',
        );
        foreach ($guideline as $g) {
            $label[] = $g['value'];
        }
        $label[] = '合計値';
        $label[] = '評価入力件数';

        $label[] = '他者評価者';
        $label[] = '他者評価日時';
        foreach ($guideline as $g) {
            $label[] = '他者:' . $g['value'];
        }
        $label[] = '他者:合計値';
        $label[] = '他者:評価入力件数';

        fputcsv($fp, $label);

        foreach ($list as $row) {
            $data = array(
                $year,
                $number,
                $row['emp_personal_id'],
                $row['emp_name'],
                $row['emp_sex'],
                $row['emp_birth'],
                $row['graduation_year'],
                $row['school'],
                $row['license_date'],
                $row['nurse_date'],
                $row['hospital_date'],
                $row['ward_date'],
                $row['class_full_name'],
                $row['job_nm'],
                $row['st_nm'],
            );

            // 評価見出し
            foreach ($guideline as $g) {
                $data[] = $row['assessment'][$g['id']]['assessment'] ? $row['assessment'][$g['id']]['assessment'] : 0;
            }
            $data[] = $row['assessment']['sum'];
            $data[] = $row['assessment']['count'];

            // 他者評価
            $data[] = $row['fassessment']['facilitator'];
            $data[] = $row['fassessment']['updated_on'];
            foreach ($guideline as $g) {
                $data[] = $row['fassessment'][$g['id']]['assessment'] ? $row['fassessment'][$g['id']]['assessment'] : 0;
            }
            $data[] = $row['fassessment']['sum'];
            $data[] = $row['fassessment']['count'];

            fputcsv($fp, $data);
        }
        rewind($fp);

        $csv = str_replace(PHP_EOL, "\r\n", stream_get_contents($fp));

        return $csv;
    }

    /**
     * ガイドライン一覧
     * @return type
     * @throws Exception
     */
    private function guideline_lists()
    {
        $revision = $this->gl->current_revision();
        $sql = "
            SELECT
                guideline_id as id,
                guideline as value
            FROM ldr_novice_guideline
            WHERE
                NOT del_flg
                AND revision_id = ?
            ORDER BY order_no, guideline_id
        ";
        $sth = $this->db->prepare($sql, array('integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array($revision));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 自己評価一覧を作る
     * @param type $ladder
     * @return type
     * @throws Exception
     */
    private function assessments($year, $number)
    {
        $revision = $this->gl->current_revision();
        $sql = "
            SELECT
                *
            FROM ldr_novice_guideline g
            JOIN ldr_novice_assessment a USING (guideline_id)
            WHERE NOT g.del_flg
              AND g.revision_id = :revision
              AND a.year = :year
              AND a.number = :number
            ORDER BY g.order_no, g.guideline_id
        ";
        $sth = $this->db->prepare($sql, array('integer', 'integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array(
            'revision' => $revision,
            'year' => $year,
            'number' => $number,
        ));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $assessments = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $assessments[$row['emp_id']][$row['guideline_id']] = $row;
            $assessments[$row['emp_id']]['count'] ++;
            $assessments[$row['emp_id']]['sum'] += $row['assessment'] ? (5 - $row['assessment']) : 0;
        }
        $sth->free();
        return $assessments;
    }

    /**
     * 他者評価一覧を作る
     * @param type $ladder
     * @return type
     * @throws Exception
     */
    private function fassessments($year, $number)
    {
        $revision = $this->gl->current_revision();
        $sql = "
            SELECT
                g.*,
                a.*,
                e.emp_lt_nm || ' ' || e.emp_ft_nm as emp_name
            FROM ldr_novice_guideline g
            JOIN ldr_novice_facilitate_assessment a USING (guideline_id)
            JOIN empmst e ON e.emp_id=a.facilitate_emp_id
            WHERE NOT g.del_flg
              AND g.revision_id = :revision
              AND a.year = :year
              AND a.number = :number
            ORDER BY g.order_no, g.guideline_id
        ";
        $sth = $this->db->prepare($sql, array('integer', 'integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array(
            'revision' => $revision,
            'year' => $year,
            'number' => $number,
        ));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $assessments = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $assessments[$row['emp_id']][$row['guideline_id']] = $row;
            $assessments[$row['emp_id']]['count'] ++;
            $assessments[$row['emp_id']]['sum'] += $row['assessment'] ? (5 - $row['assessment']) : 0;
            $assessments[$row['emp_id']]['facilitator'] = $row['emp_name'];
            $assessments[$row['emp_id']]['updated_on'] = $row['updated_on'];
        }
        $sth->free();
        return $assessments;
    }

}
