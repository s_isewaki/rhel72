<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'ladder/class/Validation.php';
require_once 'ladder/class/Guideline.php';
require_once 'ladder/class/Log.php';

class Ldr_AnalysisAssessment extends Model
{

    var $log;

    /**
     * コンストラクタ
     */
    public function Ldr_AnalysisAssessment()
    {
        parent::connectDB();
        $this->log = new Ldr_log('analysis_assessment');
        $this->gl = new Ldr_Guideline();
    }

    /**
     * 職員一覧データ
     * @param type $year
     * @param type $ladder
     * @param type $cond_class
     * @return type
     * @throws Exception
     */
    public function lists($year, $ladder, $cond_class)
    {
        // ガイドライン一覧
        $guideline = $this->guideline_lists($ladder['guideline_revision']);

        // レベルごとのガイドライン数
        $level_count = array();
        foreach ($guideline as $g) {
            $level_count[$g['level']] ++;
        }

        // レベル一覧
        $levels = $this->levels();

        // 評価一覧
        $assessments = $this->assessments($year, $ladder);

        // SELECT
        $sql = "
            SELECT
                e.emp_id,
                e.emp_personal_id,
                e.emp_lt_nm || ' ' || e.emp_ft_nm as emp_name,
                emp_sex,
                emp_birth,
                graduation_year,
                school,
                license_date,
                nurse_date,
                hospital_date,
                ward_date,
                dept_nm || CASE WHEN room_nm IS NOT NULL THEN ' > ' || room_nm ELSE '' END AS class_full_name,
                job_nm ,
                st_nm
            FROM empmst e
            JOIN authmst a USING (emp_id)
            JOIN classmst c1 ON c1.class_id = e.emp_class
            JOIN atrbmst c2 ON c2.class_id = e.emp_class AND c2.atrb_id = e.emp_attribute AND NOT atrb_del_flg
            JOIN deptmst c3 ON c3.atrb_id = e.emp_attribute AND c3.dept_id = e.emp_dept AND NOT dept_del_flg
            LEFT JOIN classroom c4 ON c4.dept_id = e.emp_dept AND (c4.room_id = e.emp_room OR c4.room_id IS NULL) AND NOT room_del_flg
            JOIN jobmst job ON job.job_id = e.emp_job AND NOT job_del_flg
            JOIN stmst st ON st.st_id = e.emp_st AND NOT st_del_flg
            JOIN ldr_analysis_authority ac ON ac.class_id = c1.class_id
                AND ac.atrb_id = c2.atrb_id
                AND ac.dept_id = c3.dept_id
                AND (ac.room_id=c4.room_id OR c4.room_id IS NULL)
                AND ac.auth_flg
                AND NOT ac.del_flg
            JOIN ldr_analysis_authority_st  aas ON aas.st_id  = e.emp_st  AND aas.auth_flg AND NOT aas.del_flg
            JOIN ldr_analysis_authority_job aaj ON aaj.job_id = e.emp_job AND aaj.auth_flg AND NOT aaj.del_flg
            LEFT JOIN ldr_employee l USING (emp_id)
        ";

        // cond
        $sql .= "WHERE NOT a.emp_del_flg ";
        if ($cond_class) {
            list($dept_id, $room_id) = explode('_', $cond_class);
            $sql .= "AND emp_dept = :dept_id ";
            if ($room_id) {
                $sql .= "AND emp_room = :room_id ";
            }
        }
        $sql .= 'ORDER BY st.order_no, e.emp_kn_lt_nm, e.emp_kn_ft_nm, e.emp_personal_id';

        $sth = $this->db->prepare($sql, null, MDB2_PREPARE_RESULT);
        $res = $sth->execute(array(
            'year' => $year,
            'dept_id' => $dept_id,
            'room_id' => $room_id,
        ));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['guideline_count'] = count($guideline);
            $row['level'] = $levels[$row['emp_id']];
            $row['assessment'] = $assessments[$row['emp_id']];
            $row['guideline_count'] = count($guideline);
            $row['level_count'] = $level_count;
            $row['sum_percent'] = sprintf("%.1f", $row['assessment']['sum'] / count($guideline) * 100);
            $row['percent'] = sprintf("%.1f", $row['assessment']['count'] / count($guideline) * 100);
            $list[] = $row;
        }
        return $list;
    }

    /**
     * CSVデータ作成
     * @param type $year
     * @param type $cond_class
     * @return type
     */
    public function export_csv($list, $ladder, $ladder_id, $year)
    {
        // ガイドライン一覧
        $this_ladder = $ladder->find($ladder_id);
        $guideline = $this->guideline_lists($this_ladder['guideline_revision']);

        $fp = fopen('php://temp', 'r+b');

        // 見出し
        $label = array(
            '年度',
            'ラダー',
            '職員ID',
            '職員名',
            '性別',
            '生年月日',
            '卒業年',
            '出身校',
            '免許取得日',
            '臨床開始日',
            '病院開始日',
            '部署開始日',
            '部署',
            '職種',
            '役職',
        );

        // レベル見出し
        foreach ($ladder->lists() as $ldr) {
            foreach ($ldr['config']['level'] as $lv) {
                $label[] = sprintf("%sレベル%s%s", $ldr['title'], Ldr_Const::get_roman_num($lv), "認定日");
                $label[] = sprintf("%sレベル%s%s", $ldr['title'], Ldr_Const::get_roman_num($lv), "認定施設");
                $label[] = sprintf("%sレベル%s%s", $ldr['title'], Ldr_Const::get_roman_num($lv), "認定番号");
            }
        }

        // 評価見出し
        foreach ($guideline as $g) {
            $label[] = $g['value'];
        }
        $label[] = '合計値';
        $label[] = '評価入力件数';

        // レベルごとの評価見出し
        foreach ($this_ladder['config']['level'] as $lv) {
            $label[] = sprintf("%sレベル%sの合計値", $this_ladder['title'], Ldr_Const::get_roman_num($lv));
        }

        fputcsv($fp, $label);

        foreach ($list as $row) {
            $data = array(
                $year,
                $this_ladder['title'],
                $row['emp_personal_id'],
                $row['emp_name'],
                $row['emp_sex'],
                $row['emp_birth'],
                $row['graduation_year'],
                $row['school'],
                $row['license_date'],
                $row['nurse_date'],
                $row['hospital_date'],
                $row['ward_date'],
                $row['class_full_name'],
                $row['job_nm'],
                $row['st_nm'],
            );

            // レベル
            foreach ($ladder->lists() as $ldr) {
                foreach ($ldr['config']['level'] as $lv) {
                    $data[] = $row['level'][$ldr['ladder_id']][$lv]['date'];
                    $data[] = $row['level'][$ldr['ladder_id']][$lv]['hispital'];
                    $data[] = $row['level'][$ldr['ladder_id']][$lv]['number'];
                }
            }

            // 評価
            foreach ($guideline as $g) {
                $data[] = $row['assessment'][$g['id']]['assessment'] || 0;
            }
            $data[] = $row['assessment']['sum'];
            $data[] = $row['assessment']['count'];

            // レベルごとの評価
            foreach ($this_ladder['config']['level'] as $lv) {
                $data[] = $row['assessment']['level'][$lv]['sum'];
            }

            fputcsv($fp, $data);
        }
        rewind($fp);

        $csv = str_replace(PHP_EOL, "\r\n", stream_get_contents($fp));

        return $csv;
    }

    /**
     * ガイドライン一覧
     * @param type $ladder
     * @return type
     * @throws Exception
     */
    private function guideline_lists($revision)
    {
        $sql = "
            SELECT
                guideline_id as id,
                guideline || ' ' || COALESCE(criterion, '') as value,
                level
            FROM ldr_guideline
            WHERE
                NOT del_flg
                AND guideline_revision = ?
            ORDER BY order_no, guideline_id
        ";
        $sth = $this->db->prepare($sql, array('integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array($revision));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * レベル一覧を作る
     * @param type $ladder
     * @return type
     * @throws Exception
     */
    private function levels()
    {
        $sql = "SELECT * FROM ldr_level";
        $sth = $this->db->prepare($sql, null, MDB2_PREPARE_RESULT);
        $res = $sth->execute();
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $levels = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $levels[$row['emp_id']][$row['ladder_id']][$row['level']] = $row;
        }
        $sth->free();
        return $levels;
    }

    /**
     * 評価一覧を作る
     * @param type $ladder
     * @return type
     * @throws Exception
     */
    private function assessments($year, $ladder)
    {
        $revision = $ladder['guideline_revision'];
        $sql = "
            SELECT
                emp_id,
                guideline_id,
                level,
                assessment
            FROM ldr_guideline g
            JOIN ldr_assessment a USING (guideline_id)
            WHERE NOT g.del_flg
              AND g.guideline_revision = :revision
              AND a.year = :year
            ORDER BY g.order_no, g.guideline_id
        ";
        $sth = $this->db->prepare($sql, array('integer', 'integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array(
            'revision' => $revision,
            'year' => $year,
        ));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $assessments = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $assessments[$row['emp_id']][$row['guideline_id']] = $row;
            $assessments[$row['emp_id']]['count'] ++;
            $assessments[$row['emp_id']]['sum'] += $row['assessment'];
            $assessments[$row['emp_id']]['level'][$row['level']]['count'] ++;
            $assessments[$row['emp_id']]['level'][$row['level']]['sum'] += $row['assessment'];
        }
        $sth->free();
        return $assessments;
    }

}
