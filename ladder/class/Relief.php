<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'ladder/class/Validation.php';
require_once 'ladder/class/Relief/Relief.php';
require_once 'ladder/class/Relief/TrnRelief.php';
require_once 'ladder/class/Relief/Detail.php';
require_once 'ladder/class/Log.php';

class Ldr_Relief extends Model
{

    var $log;
    var $relief;
    var $detail;
    var $trn_relief;
    var $emp_id;

    /**
     * コンストラクタ
     * @param type $emp_id 職員ID
     */
    public function Ldr_Relief($emp_id = null)
    {
        parent::connectDB();
        $this->log = new Ldr_log('relief');
        $this->relief = new Ldr_ReliefRelief();
        $this->detail = new Ldr_ReliefDetail();
        $this->emp_id = $emp_id;
        if (!is_null($emp_id)) {
            $this->trn_relief = new Ldr_TrnRelief($emp_id);
        }
    }

    /**
     * validation
     * @param type $type  subject(科目)/contents(内容)
     * @param type $data 更新データ
     * @return type
     */
    public function validate($type, $data)
    {
        switch ($type) {
            case 'subject':
                return $this->relief->validate($data);
            case 'contents':
                return $this->detail->validate($data);
            case 'apply':
                return $this->trn_relief->validate($data);
            case 'report':
                return $this->trn_relief->validate_report($data);
        }
    }

    /**
     * 一覧件数の取得
     * @param type $type
     * @return type
     */
    public function count($type, $cond = null)
    {
        switch ($type) {
            case "apply":
                return $this->trn_relief->count_apply();
            case "approve":
                return $this->trn_relief->count_approve($cond);
            case "accept":
                return $this->trn_relief->count_accept($cond);
        }
    }

    /**
     * 一覧の取得
     * @param type $type subject(科目)/contents(教科内容)/contents_all(すべて)
     * @return type
     */
    public function lists($type, $relief_id = null, $st = null, $pageCount = null, $cond = null)
    {
        switch ($type) {
            case "subject":
                return $this->relief->lists();
            case "contents":
                return $this->detail->lists($relief_id);
            case "contents_all":
                return $this->relief->lists_contents();
            case "apply":
                return $this->trn_relief->lists_apply($st, $pageCount);
            case "approve":
                return $this->trn_relief->lists_approve($st, $pageCount, $cond);
            case "accept":
                return $this->trn_relief->lists_accept($st, $pageCount, $cond);
            case "accept_emp":
                return $this->trn_relief->lists_accept_emp($cond);
            case "accept_class":
                return $this->trn_relief->lists_accept_class($cond);
        }
    }

    /**
     * １件検索
     * @param type $type  subject(科目)/contents(内容)
     * @param type $id
     * @return type
     */
    public function find($type, $id, $unit = null)
    {
        switch ($type) {
            case "subject":
                return $this->relief->find($id);
            case "contents":
                return $this->detail->find($id, $unit);
            case "apply":
                return $this->trn_relief->find_apply($id);
        }
    }

    /**
     * 保存
     * @param type $type  subject(科目)/contents(内容)
     * @param type $data
     * @return type
     */
    public function save($type, $data)
    {
        switch ($type) {
            case "subject":
                return $this->relief->save($data);
            case "contents":
                return $this->detail->save($data);
            case "apply":
                return $this->trn_relief->save_apply($data);
            case "apply_cancel":
                return $this->trn_relief->cancel_apply($data['training_id']);
            case "approve":
                return $this->trn_relief->save_approve($data['training_id']);
            case "approve_cancel":
                return $this->trn_relief->cancel_approve($data['training_id']);
            case "accept":
                return $this->trn_relief->save_accept($data['training_id']);
            case "report":
                return $this->trn_relief->save_report($data);
        }
    }

    /**
     * 削除
     * @param type $type  subject(科目)/contents(内容)
     * @param type $id
     * @return type
     */
    public function delete($type, $id, $unit = null)
    {
        switch ($type) {
            case "subject":
                return $this->delete_relief_id($id);
            case "contents":
                return $this->detail->delete($id, $unit);
        }
    }

    /**
     * リレーションで削除
     * @param type $id
     * @return type
     */
    private function delete_relief_id($id)
    {
        $this->db->beginNestedTransaction();

        try {
            $this->detail->delete_relief_id($id);
            $this->relief->delete($id);
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            $this->db->failNestedTransaction();
            return;
        }
        $this->db->completeNestedTransaction();
    }

    /**
     * ソート
     * @param type $type  subject(科目)/contents(内容)
     * @param type $sort
     * @return type
     */
    public function sort($type, $sort)
    {
        switch ($type) {
            case "subject":
                return $this->relief->sort($sort);
            case "contents":
                return $this->detail->sort($sort);
        }
    }

    /**
     * 受付件数（ステータス毎）
     * @return Array
     * @throws Exception
     */
    public function count_accept_status()
    {
        if (empty($this->emp_id)) {
            throw new Exception('Ldr_Relief->count_accept_status() METHOD error.');
        }

        $sql = "SELECT
                (SELECT COUNT(*)
                    FROM ldr_trn_releif trn
                    JOIN ldr_mst_relief r USING(relief_id)
                    JOIN ldr_mst_relief_detail d ON NOT d.del_flg AND trn.relief_id = d.relief_id AND trn.unit = d.unit
                    JOIN empmst e USING(emp_id)
                    JOIN ldr_approver app
                        ON app.emp_id = :emp_id
                        AND app.class_id = e.emp_class
                        AND app.atrb_id = e.emp_attribute
                        AND app.dept_id = e.emp_dept
                        AND COALESCE(app.room_id,0) = COALESCE(e.emp_room,0)
                    WHERE trn.status=1 AND NOT trn.del_flg
                ) AS st1_count,
                (SELECT COUNT(*)
                    FROM ldr_trn_releif trn
                    JOIN ldr_mst_relief r USING(relief_id)
                    JOIN ldr_mst_relief_detail d ON NOT d.del_flg AND trn.relief_id = d.relief_id AND trn.unit = d.unit
                    WHERE trn.status=2 AND NOT trn.del_flg
                ) AS st2_count";
        $row = $this->db->extended->getRow($sql, null, array('emp_id' => $this->emp_id), array('text'), MDB2_FETCHMODE_ASSOC);
        if (PEAR::isError($row)) {
            throw new Exception('ldr_relief count_accept_status ERROR: ' . $row->getDebugInfo());
        }

        return $row;
    }

}
