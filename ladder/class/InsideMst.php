<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'ladder/class/Util.php';
require_once 'ladder/class/Log.php';
require_once 'ladder/class/CheckTemplate.php';

/**
 * 院内研修マスタ
 */
class Ldr_InsideMst extends Model
{

    var $log;

    /**
     * コンストラクタ
     */
    public function Ldr_InsideMst()
    {
        parent::connectDB();
        $this->log = new Ldr_log('inside_mst');
    }

    /**
     * validate
     * @param type $data
     * @return string
     */
    public function validate($data, $type = null)
    {
        $error = array();

        // 年度
        if (Validation::is_empty($data['year'])) {
            $error['year'][] = '年度が選択されていません';
        }
        else if (!Validation::is_year($data['year'])) {
            $error['year'][] = '年度が正しくありません';
        }

        // 研修番号
        if ($type === '1' && Validation::is_empty($data['no'])) {
            $error['no'][] = '研修番号が入力されていません';
        }
        else {
            if (mb_strlen($data['no'], 'EUC-JP') > 20) {
                $error['no'][] = '研修番号は20文字以内で入力してください';
            }
        }

        // 研修名
        if (Validation::is_empty($data['title'])) {
            $error['title'][] = '研修名が入力されていません';
        }

        // 企画担当
        if (Validation::is_empty($data['emp_planner'])) {
            $error['planner'][] = '企画担当職員が選択されていません';
        }

        // めざす状態
        if ($type === '1' && Validation::is_empty($data['guideline'])) {
            $error['guideline'][] = 'めざす状態が選択されていません';
        }

        // めざすレベル
        if (!Validation::is_empty($data['level']) && !Validation::is_level($data['level'])) {
            $error['level'][] = '不正なレベルが選択されています';
        }

        // カテゴリ
        if (Validation::is_empty($data['guideline_group1'])) {
            $error['guideline_group1'][] = 'カテゴリが選択されていません';
        }

        // 講師
        if (Validation::is_empty($data['coach']) && Validation::is_empty($data['emp_coach'])) {
            $error['coach'][] = '講師が入力されていません';
        }

        // 外部参加
        if (Validation::is_empty($data['public'])) {
            $error['public'][] = '外部参加が選択されていません';
        }

        // 実施上の留意点
        if (Validation::is_empty($data['notes'])) {
            $error['notes'][] = '実施上の留意点が入力されていません';
        }

        // 評価
        if (Validation::is_empty($data['assessment'])) {
            $error['assessment'][] = '評価が入力されていません';
        }

        // 師長承認
        if (Validation::is_empty($data['approve'])) {
            $error['approve'][] = '師長承認が選択されていません';
        }

        return $error;
    }

    /**
     * validate
     * @param type $data
     * @return string
     */
    public function validate_contents($data)
    {
        $error = array();

        // 研修内容
        if (Validation::is_empty($data['contents'])) {
            $error['contents'][] = '研修内容が入力されていません';
        }

        // 研修方法
        if (Validation::is_empty($data['method'])) {
            $error['method'][] = '研修方法が入力されていません';
        }

        // 研修前の課題
        if (Validation::is_empty($data['before_work'])) {
            $error['before_work'][] = '研修前の課題が選択されていません';
        }

        // 研修前 課題内容
        if ($data['before_work'] === 't' and Validation::is_empty($data['before_work_contents'])) {
            $error['before_work_contents'][] = '研修前 課題内容が入力されていません';
        }

        // 研修前 提出方法
        if ($data['before_work'] === 't' and Validation::is_empty($data['before_work_submit'])) {
            $error['before_work_submit'][] = '研修前 提出方法が入力されていません';
        }

        // 研修後の課題
        if (Validation::is_empty($data['after_work'])) {
            $error['after_work'][] = '研修後の課題が選択されていません';
        }

        // 研修後 課題内容
        if ($data['after_work'] === 't' and Validation::is_empty($data['after_work_contents'])) {
            $error['after_work_contents'][] = '研修後 課題内容が入力されていません';
        }

        // 研修後 提出方法
        if ($data['after_work'] === 't' and Validation::is_empty($data['after_work_submit'])) {
            $error['after_work_submit'][] = '研修後 提出方法が入力されていません';
        }

        // 振り返り
        if (Validation::is_empty($data['looking_back'])) {
            $error['looking_back'][] = '振り返りが選択されていません';
        }

        return $error;
    }

    /**
     * 研修日程 validate
     * @param type $data
     * @return string
     */
    public function validate_date($data)
    {
        $error = array();

        // 研修回
        if (Validation::is_empty($data['inside_contents_id'])) {
            $error['inside_contents_id'][] = '研修回が選択されていません';
        }

        // 研修の種類
        if (Validation::is_empty($data['type'])) {
            $error['type'][] = '研修の種類が選択されていません';
        }

        // 受付開始日時
        if (Validation::is_empty($data['accept_date'])) {
            $error['accept_date'][] = '受付開始日が入力されていません';
        }
        else if (!Validation::is_date($data['accept_date'])) {
            $error['accept_date'][] = '受付開始日が正しくありません';
        }
        if (Validation::is_empty($data['accept_time'])) {
            $error['accept_date'][] = "受付開始時間が入力されていません";
        }
        else if (!Validation::is_time($data['accept_time'])) {
            $error['accept_date'][] = "受付開始時間が正しくありません";
        }
        if ("{$data['deadline_date']} {$data['deadline_time']}" < "{$data['accept_date']} {$data['accept_time']}") {
            $error['accept_date'][] = '締切日より未来の日時になっています';
        }

        // 締切日時
        if (Validation::is_empty($data['deadline_date'])) {
            $error['deadline_date'][] = '締切日が入力されていません';
        }
        else if (!Validation::is_date($data['deadline_date'])) {
            $error['deadline_date'][] = '締切日が正しくありません';
        }
        if (Validation::is_empty($data['deadline_time'])) {
            $error['deadline_date'][] = "締切時間が入力されていません";
        }
        else if (!Validation::is_time($data['deadline_time'])) {
            $error['deadline_date'][] = "締切時間が正しくありません";
        }
        if ("{$data['deadline_date']} {$data['deadline_time']}" < "{$data['accept_date']} {$data['accept_time']}") {
            $error['accept_date'][] = '受付開始日より過去の日時になっています';
        }

        // 集合研修の場合
        if ($data['type'] !== '2') {

            // 開催場所
            if (Validation::is_empty($data['place'])) {
                $error['place'][] = '開催場所が入力されていません';
            }

            // 最大人数
            if (Validation::is_empty($data['maximum'])) {
                $error['maximum'][] = '最大人数が入力されていません';
            }

            // 開催日時
            if (Validation::is_empty($data['training_date'])) {
                $error['training_date'][] = "開催日が入力されていません";
            }
            else if (!Validation::is_date($data['training_date'])) {
                $error['training_date'][] = "開催日が正しくありません";
            }

            if (Validation::is_empty($data['training_start_time'])) {
                $error['training_date'][] = "開催日時の開始時間が入力されていません";
            }
            else if (!Validation::is_time($data['training_start_time'])) {
                $error['training_date'][] = "開催日時の開始時間が正しくありません";
            }

            if (Validation::is_empty($data['training_last_time'])) {
                $error['training_date'][] = "開催日時の終了時間が入力されていません";
            }
            else if (!Validation::is_time($data['training_last_time'])) {
                $error['training_date'][] = "開催日時の終了時間が正しくありません";
            }

            if (empty($error['training_date']) and $data['training_start_time'] > $data['training_last_time']) {
                $error['training_date'][] = "開始時間より終了時間が遅い時刻です";
            }

            // 最大人数超過
            if (Validation::is_empty($data['overflow'])) {
                $error['overflow'][] = '最大人数を超えた場合が選択されていません';
            }

            // 受付開始日時
            if ("{$data['training_date']} {$data['training_start_time']}" < "{$data['accept_date']} {$data['accept_time']}") {
                $error['accept_date'][] = '開催日より未来の日時になっています';
            }

            // 締切日時
            if ("{$data['training_date']} {$data['training_start_time']}" < "{$data['deadline_date']} {$data['deadline_time']}") {
                $error['deadline_date'][] = '開催日より未来の日時になっています';
            }
        }

        // ステータス
        if (Validation::is_empty($data['status'])) {
            $error['accept_status'][] = 'ステータスが選択されていません';
        }

        return $error;
    }

    /**
     * validate(日程のステータス)
     * @param type $data
     * @return string
     */
    public function validate_date_status($data)
    {
        $error = array();

        // ステータス
        if (Validation::is_empty($data['status'])) {
            $error['status'][] = 'ステータスを選択してください';
        }

        return $error;
    }

    /**
     * 研修コピー validate
     * @param type $data
     * @return string
     */
    public function validate_copy($data)
    {
        $error = array();

        // 年度
        if (Validation::is_empty($data['year'])) {
            $error['year'][] = '年度が選択されていません';
        }

        // 回数
        if (Validation::is_empty($data['number'])) {
            $error['number'][] = '回数が選択されていません';
        }

        // 日程
        if (Validation::is_empty($data['date'])) {
            $error['date'][] = '日程が選択されていません';
        }
        return $error;
    }

    /**
     * 保存
     * @param Array $data
     */
    public function save($data)
    {
        $this->db->beginNestedTransaction();

        // emp_id
        $data['update_emp_id'] = $GLOBALS['_session_emp_id'];

        // UPDATE or INSERT
        $sth = $this->db->prepare(
            "SELECT update_ldr_mst_inside(:inside_id, :year, :no, :title, :theme, :planner, :level, :guideline_group1, :coach, :public, :notes, :assessment, :approve, :guideline_text,:update_emp_id)", array('integer', 'integer', 'text', 'text', 'text', 'text', 'integer', 'integer', 'integer', 'text', 'boolean', 'text', 'text', 'boolean', 'text', 'text'), MDB2_PREPARE_MANIP);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }
        $sth->free();

        if (empty($data['inside_id'])) {
            $data['inside_id'] = $this->db->lastInsertID('ldr_mst_inside', 'inside_id');
        }

        // planner
        $this->save_planner($data['inside_id'], array_keys($data['emp_planner']));

        // coach
        $this->save_coach($data['inside_id'], array_keys($data['emp_coach']));

        // guideline
        $this->save_guideline($data['inside_id'], array_keys($data['guideline']));

        // history
        $this->save_history($data['inside_id'], 'マスタを保存', $data['update_emp_id']);

        $this->db->completeNestedTransaction();
        $this->log->log("保存しました", "inside_id={$data['inside_id']} title={$data['title']}");
    }

    /**
     * 企画担当職員 保存
     * @param Array $data
     */
    private function save_planner($id, $data)
    {
        $this->db->beginNestedTransaction();

        // DELETE
        $del_sth = $this->db->prepare("DELETE FROM ldr_mst_inside_planner WHERE inside_id = ?", array('integer'), MDB2_PREPARE_MANIP);
        $res = $del_sth->execute(array($id));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }
        $del_sth->free();

        // INSERT
        $ins_sth = $this->db->prepare("INSERT INTO ldr_mst_inside_planner (inside_id, emp_id) VALUES (?, ?)", array('integer', 'text'), MDB2_PREPARE_MANIP);
        foreach ($data as $emp_id) {
            $res = $ins_sth->execute(array($id, $emp_id));
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
            }
        }
        $ins_sth->free();

        $this->db->completeNestedTransaction();
    }

    /**
     * 講師 保存
     * @param Array $data
     */
    private function save_coach($id, $data)
    {
        $this->db->beginNestedTransaction();

        // DELETE
        $del_sth = $this->db->prepare("DELETE FROM ldr_mst_inside_coach WHERE inside_id = ?", array('integer'), MDB2_PREPARE_MANIP);
        $res = $del_sth->execute(array($id));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }
        $del_sth->free();

        // INSERT
        $ins_sth = $this->db->prepare("INSERT INTO ldr_mst_inside_coach (inside_id, emp_id) VALUES (?, ?)", array('integer', 'text'), MDB2_PREPARE_MANIP);
        foreach ($data as $emp_id) {
            $res = $ins_sth->execute(array($id, $emp_id));
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
            }
        }
        $ins_sth->free();

        $this->db->completeNestedTransaction();
    }

    /**
     * めざす状態 保存
     * @param Array $data
     */
    private function save_guideline($id, $data)
    {
        $this->db->beginNestedTransaction();

        // DELETE
        $del_sth = $this->db->prepare("DELETE FROM ldr_mst_inside_guideline WHERE inside_id = ?", array('integer'), MDB2_PREPARE_MANIP);
        $res = $del_sth->execute(array($id));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }
        $del_sth->free();

        // INSERT
        $ins_sth = $this->db->prepare("INSERT INTO ldr_mst_inside_guideline (inside_id, guideline_id) VALUES (?, ?)", array('integer', 'integer'), MDB2_PREPARE_MANIP);
        foreach ($data as $guideline_id) {
            $res = $ins_sth->execute(array($id, $guideline_id));
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
            }
        }
        $ins_sth->free();

        $this->db->completeNestedTransaction();
    }

    /**
     * 研修内容 保存
     * @param Array $data
     */
    public function save_contents($data)
    {
        $this->db->beginNestedTransaction();

        // emp_id
        $data['update_emp_id'] = $GLOBALS['_session_emp_id'];

        // missing placeholder
        foreach (array('before_work_contents', 'before_work_submit', 'before_work_notes', 'after_work_contents', 'after_work_submit', 'after_work_notes') as $key) {
            if (!array_key_exists($key, $data)) {
                $data[$key] = '';
            }
        }

        // UPDATE or INSERT
        $sth = $this->db->prepare(
            "SELECT update_ldr_mst_inside_contents
                (:inside_contents_id, :id, :number, :aim, :objective, :contents, :method,
                 :before_work, :before_work_contents, :before_work_submit, :before_work_notes,
                 :after_work, :after_work_contents, :after_work_submit, :after_work_notes, :looking_back)
            ", array('integer', 'integer', 'text', 'text', 'text', 'text', 'boolean', 'text', 'text', 'text', 'boolean', 'text', 'text', 'text', 'boolean'), MDB2_PREPARE_MANIP);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }
        $sth->free();


        if (empty($data['inside_contents_id'])) {
            $data['inside_contents_id'] = $this->db->lastInsertID('ldr_mst_inside_contents', 'inside_contents_id');
        }

        if ($data['looking_back'] === "t" && !empty($data['tmpl_id'])) {
            $this->save_check($data);
        }

        // history
        $this->save_history($data['id'], "研修内容を保存", $data['update_emp_id']);

        $this->db->completeNestedTransaction();
        $this->log->log("研修内容を保存しました", "inside_id={$data['id']} contents_id={$data['inside_contents_id']} number={$data['number']} contents={$data['contents']}");
    }

    /**
     * 振り返りの保存
     * @param type $data
     */
    private function save_check($data)
    {
        $this->db->beginNestedTransaction();

        // UPDATE or INSERT
        $sql = "SELECT update_ldr_mst_inside_check (
                :inside_contents_id,
                (SELECT form FROM ldr_check_template WHERE NOT del_flg AND tmpl_id= :tmpl_id)
                )";
        $sth = $this->db->prepare($sql, array('integer', 'integer'), MDB2_PREPARE_MANIP);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }
        $sth->free();
        $this->db->completeNestedTransaction();
    }

    /**
     * 研修日程 保存
     * @param Array $data
     */
    public function save_date($data)
    {
        $this->db->beginNestedTransaction();

        // emp_id
        $data['update_emp_id'] = $GLOBALS['_session_emp_id'];

        // accept_date
        $data['accept_date'] .= " {$data['accept_time']}";

        // deadline_date
        $data['deadline_date'] .= " {$data['deadline_time']}";

        // 個別研修
        if ($data['type'] === '2') {
            $data['overflow'] = 't';
        }

        // UPDATE or INSERT
        $sth = $this->db->prepare(
            "SELECT update_ldr_mst_inside_date
                (:inside_date_id, :id, :inside_contents_id, :training_date, :training_start_time, :training_last_time, :place, :maximum,
                 :overflow, :accept_date, :deadline_date, :type, :status, :update_emp_id)
            ", array('integer', 'integer', 'integer', 'date', 'time', 'time', 'text', 'integer', 'boolean', 'date', 'date', 'integer', 'integer', 'text'), MDB2_PREPARE_MANIP);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }
        $sth->free();

        if (empty($data['inside_date_id'])) {
            $data['inside_date_id'] = $this->db->lastInsertID('ldr_mst_inside_date', 'inside_date_id');
        }

        // history
        $this->save_history($data['id'], "研修日程を保存", $data['update_emp_id']);


        $this->db->completeNestedTransaction();
        $this->log->log("研修日程を保存しました", "inside_id={$data['id']} conetnts_id={$data['inside_contents_id']} date_id={$data['inside_date_id']} type={$data['type']} status={$data['status']} place={$data['place']} training_date={$data['training_date']}");
    }

    /**
     * 変更履歴 保存
     * @param type $data
     */
    public function save_history($id, $log, $update_emp_id)
    {
        $this->db->beginNestedTransaction();

        $sth = $this->db->prepare("INSERT INTO ldr_mst_inside_history (inside_id, log, update_emp_id) VALUES (:inside_id, :log, :update_emp_id)", array('integer', 'text', 'text'), MDB2_PREPARE_MANIP);

        $data = array(
            'inside_id' => $id,
            'log' => $log,
            'update_emp_id' => $update_emp_id,
        );
        $res = $sth->execute($data);

        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }
        $sth->free();

        $this->db->completeNestedTransaction();
    }

    /**
     * 研修のコピー
     * @param type $cond
     */
    public function save_copy($cond)
    {
        $this->db->beginNestedTransaction();

        // emp_id
        $data['update_emp_id'] = $GLOBALS['_session_emp_id'];

        $data = array(
            'inside_id' => $cond['id'],
            'year' => $cond['year'],
            'number' => ($cond['number'] === 'copy') ? true : false,
            'date' => ($cond['date'] === 'copy') ? true : false,
            'update_emp_id' => $GLOBALS['_session_emp_id'],
        );
        $sth = $this->db->prepare(
            "SELECT copy_ldr_mst_inside(:inside_id, :year, :update_emp_id,:number, :date)", array('integer', 'integer', 'text', 'boolean', 'boolean'), MDB2_PREPARE_MANIP);

        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }
        $sth->free();

        $new_insert_id = $this->db->lastInsertID('ldr_mst_inside', 'inside_id');

        // history
        $this->save_history($new_insert_id, "inside_id={$cond['id']} をコピー", $GLOBALS['_session_emp_id']);

        $this->db->completeNestedTransaction();
        $this->log->log("コピーしました", "コピー元 inside_id={$cond['id']} コピー先 inside_id={$new_insert_id} ");
    }

    /**
     * 削除
     * @param type $id
     * @throws Exception
     */
    public function remove($id)
    {
        $this->db->beginNestedTransaction();
        $sth = $this->db->prepare("UPDATE ldr_mst_inside SET del_flg='t' WHERE inside_id = ?", array('integer'), MDB2_PREPARE_MANIP);
        $res = $sth->execute(array($id));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }
        $sth->free();
        $this->db->completeNestedTransaction();

        // Log
        $this->log->log("削除しました", "inside_id=$id");
    }

    /**
     * 研修内容 削除
     * @param type $id
     * @throws Exception
     */
    public function remove_contents($id, $inside_id)
    {
        $this->db->beginNestedTransaction();

        $sth = $this->db->prepare("UPDATE ldr_mst_inside_contents SET del_flg='t' WHERE inside_contents_id = ?", array('integer'), MDB2_PREPARE_MANIP);
        $res = $sth->execute(array($id));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }
        $sth->free();

        $sth_sel = $this->db->prepare("SELECT inside_contents_id FROM ldr_mst_inside_contents WHERE NOT del_flg AND inside_id = ? ORDER BY number", array('integer'), MDB2_PREPARE_RESULT);
        $sth_upd = $this->db->prepare("UPDATE ldr_mst_inside_contents SET number = ? WHERE inside_contents_id = ?", array('integer', 'integer'), MDB2_PREPARE_MANIP);

        $res_sel = $sth_sel->execute(array($inside_id));
        if (PEAR::isError($res_sel)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res_sel->getDebugInfo());
        }

        $number = 1;
        while ($row = $res_sel->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $res = $sth_upd->execute(array($number, $row['inside_contents_id']));
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
            }
            $number++;
        }

        $sth_nof = $this->db->prepare("UPDATE ldr_mst_inside SET number_of=(SELECT COUNT(*) FROM ldr_mst_inside_contents WHERE NOT del_flg AND inside_id= :inside_id) WHERE inside_id= :inside_id", array('integer', 'integer'), MDB2_PREPARE_MANIP);
        $res_nof = $sth_nof->execute(array('inside_id' => $inside_id));
        if (PEAR::isError($res_nof)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . ' ERROR: ' . $res_nof->getDebugInfo());
        }

        $this->db->completeNestedTransaction();

        // history
        $this->save_history($inside_id, "研修内容を削除", $GLOBALS['_session_emp_id']);

        // Log
        $this->log->log("研修内容を削除しました", "inside_id=$inside_id contents_id=$id");
    }

    /**
     * 研修日程の削除
     * @param type $id
     * @throws Exception
     */
    public function remove_date($id)
    {
        $this->db->beginNestedTransaction();
        $sth = $this->db->prepare("UPDATE ldr_mst_inside_date SET del_flg='t' WHERE inside_date_id = ?", array('integer'), MDB2_PREPARE_MANIP);
        $res = $sth->execute(array($id));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }
        $sth->free();
        $this->db->completeNestedTransaction();

        // Log
        $this->log->log("研修日程を削除しました", "inside_date_id=$id");
    }

    /**
     * 研修内容 並べ替え
     * @param type $contents_ids
     */
    public function sort_contents($inside_id, $contents_ids)
    {
        $this->db->beginNestedTransaction();
        $sth_upd = $this->db->prepare("UPDATE ldr_mst_inside_contents SET number = ? WHERE inside_contents_id = ?", array('integer', 'integer'), MDB2_PREPARE_MANIP);

        $number = 1;
        foreach ($contents_ids as $inside_contents_id) {
            $res = $sth_upd->execute(array($number, $inside_contents_id));
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
            }
            $number++;
        }

        $this->db->completeNestedTransaction();

        // history
        $this->save_history($inside_id, "研修内容を削除", $GLOBALS['_session_emp_id']);

        // Log
        $this->log->log("研修内容を並べ替えしました");
    }

    /**
     * 講座一覧の検索条件
     * @param type $cond
     * @return type
     */
    protected function make_condition($cond = null)
    {
        $cond_ret = "";
        $types = array();
        $data = array();

        // 年度
        if (!empty($cond['srh_year'])) {
            $cond_ret .= " AND mst.year = :srh_year ";
            $types[] = 'integer';
            $data['srh_year'] = $cond['srh_year'];
        }

        // レベル
        if ($cond['srh_level'] === "0") {
            $cond_ret .= " AND mst.level is null ";
        }
        else if (!empty($cond['srh_level'])) {
            switch ($cond['srh_level']) {
                case '1':
                    $cond_ret .= " AND mst.level in (1,12,123,1234) ";
                    break;
                case '2':
                    $cond_ret .= " AND mst.level in (2,12,123,1234,23,234,2345) ";
                    break;
                case '3':
                    $cond_ret .= " AND mst.level in (3,123,1234,23,234,2345,34,345) ";
                    break;
                case '4':
                    $cond_ret .= " AND mst.level in (4,1234,234,34,45) ";
                    break;
                case '5':
                    $cond_ret .= " AND mst.level in (5,2345,345,45) ";
                    break;
                default :
                    break;
            }
        }

        // カテゴリ
        if (!empty($cond['srh_guideline'])) {
            $cond_ret .= " AND mst.guideline_group1 = :srh_guideline ";
            $types[] = 'integer';
            $data['srh_guideline'] = $cond['srh_guideline'];
        }

        // 回数
        if (!empty($cond['enable_number_of'])) {
            $cond_ret .= " AND mst.number_of != 0 ";
        }

        $ret['cond'] = $cond_ret;
        $ret['types'] = $types;
        $ret['data'] = $data;
        return $ret;
    }

    /**
     * 講座一覧件数
     * @param type $cond
     * @return type
     * @throws Exception
     */
    public function count($cond = null)
    {
        $ret = $this->make_condition($cond);

        $sql = "SELECT
                    COUNT(*)
                FROM ldr_mst_inside mst
                JOIN empmst e ON e.emp_id=mst.update_emp_id
                JOIN ldr_guideline_group1 g1 USING (guideline_group1)
                WHERE
                    NOT mst.del_flg
                    {$ret['cond']}
                ";

        $sth = $this->db->prepare($sql, $ret['types'], MDB2_PREPARE_RESULT);
        $res = $sth->execute($ret['data']);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }
        $count = 0;
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $count = $row["count"];
        }
        $sth->free();
        return $count;
    }

    /**
     * 一覧
     * @return type
     */
    public function lists($st = null, $pageCount = null, $cond = null)
    {
        $ret = $this->make_condition($cond);

        $sql = "
            SELECT
                mst.*,
                (SELECT COUNT(*) FROM ldr_mst_inside_planner p WHERE mst.inside_id=p.inside_id) as count_planner,
                (SELECT COUNT(*) FROM ldr_mst_inside_coach c WHERE mst.inside_id=c.inside_id) as count_coach,
                (SELECT COUNT(*) FROM ldr_mst_inside_date d WHERE mst.inside_id=d.inside_id AND NOT d.del_flg) as count_date,
                e.emp_lt_nm || ' ' || e.emp_ft_nm as update_emp_name,
                g1.name as category
            FROM ldr_mst_inside mst
            JOIN empmst e ON e.emp_id=mst.update_emp_id
            JOIN ldr_guideline_group1 g1 USING (guideline_group1)
            WHERE
                NOT mst.del_flg
                {$ret['cond']}
            ORDER BY substring(no from '^[^0-9]*'), CASE WHEN substring(no from '[0-9\-]*$') = '' THEN 0 ELSE to_number(replace(substring(no from '[0-9\-]*$'), '-', ''), '9999999999') END
            ";
        $sth_p = $this->db->prepare("SELECT emp_id, e.emp_lt_nm || ' ' || e.emp_ft_nm as emp_name FROM ldr_mst_inside_planner JOIN empmst e USING(emp_id) WHERE inside_id = ? ORDER BY order_no, emp_kn_ft_nm", array('integer'), MDB2_PREPARE_RESULT);
        $sth_c = $this->db->prepare("SELECT emp_id, e.emp_lt_nm || ' ' || e.emp_ft_nm as emp_name FROM ldr_mst_inside_coach   JOIN empmst e USING(emp_id) WHERE inside_id = ? ORDER BY order_no, emp_kn_ft_nm", array('integer'), MDB2_PREPARE_RESULT);
        if (isset($st) && isset($pageCount)) {
            $this->db->setLimit($pageCount, $st);
        }
        $sth = $this->db->prepare($sql, $ret['types'], MDB2_PREPARE_RESULT);
        $res = $sth->execute($ret['data']);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {

            // planner
            $res_p = $sth_p->execute(array($row['inside_id']));
            if (PEAR::isError($res_p)) {
                continue;
            }
            while ($row_p = $res_p->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                $row['emp_planner'][] = $row_p;
            }

            // coach
            $res_c = $sth_c->execute(array($row['inside_id']));
            if (PEAR::isError($res_c)) {
                continue;
            }
            while ($row_c = $res_c->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                $row['emp_coach'][] = $row_c;
            }

            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 研修日程一覧（カレンダー用）
     * @return type
     * @throws Exception
     */
    public function lists_calendar()
    {

        $sql = "SELECT
                    d.inside_id,
                    d.inside_contents_id,
                    d.inside_date_id,
                    d.training_date,
                    d.training_start_time,
                    d.training_last_time,
                    m.year,
                    m.no,
                    m.title,
                    m.number_of,
                    m.level,
                    c.number,
                    d.updated_on
                FROM ldr_mst_inside_date d
                JOIN ldr_mst_inside m ON m.inside_id = d.inside_id AND NOT m.del_flg
                JOIN ldr_mst_inside_contents c ON c.inside_contents_id = d.inside_contents_id AND NOT c.del_flg
                WHERE
                    NOT d.del_flg
                    AND d.status != '2'
                ORDER by d.training_date,d.training_start_time
                ";
        $sth = $this->db->prepare($sql, null, MDB2_PREPARE_RESULT);
        $res = $sth->execute(null);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 更新履歴の取得
     * @param type $id
     * @return type
     * @throws Exception
     */
    public function lists_history($id)
    {

        $sql = "SELECT
                    e.emp_lt_nm || ' ' || e.emp_ft_nm as name,
                    c1.class_id,
                    c2.atrb_id,
                    c3.dept_id,
                    c4.room_id,
                    c1.class_nm,
                    c2.atrb_nm,
                    c3.dept_nm,
                    c4.room_nm,
                    his.history_id,
                    his.inside_id,
                    his.log,
                    his.update_emp_id,
                    his.updated_on
                FROM ldr_mst_inside_history his
                JOIN empmst e ON e.emp_id = his.update_emp_id
                JOIN classmst c1 ON e.emp_class = c1.class_id AND NOT c1.fantol_del_flg
                JOIN atrbmst c2 ON e.emp_attribute = c2.atrb_id AND NOT c2.atrb_del_flg
                JOIN deptmst c3 ON e.emp_dept = c3.dept_id AND NOT c3.dept_del_flg
                LEFT JOIN classroom c4 ON COALESCE(e.emp_room,0) = COALESCE(c4.room_id,0) AND NOT c4.room_del_flg
                WHERE
                    his.inside_id = :id
                    ORDER BY his.updated_on desc
                ";
        $sth = $this->db->prepare($sql, array('integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array('id' => $id));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 一件取得
     *
     * @param type $id
     */
    public function find($id)
    {
        if (is_null($id)) {
            return array();
        }

        // ldr_mst_inside
        $sql = "SELECT * FROM ldr_mst_inside WHERE NOT del_flg AND inside_id= ?";
        $data = $this->db->extended->getRow(
            $sql, null, array($id), array('integer'), MDB2_FETCHMODE_ASSOC
        );
        if (PEAR::isError($data)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $data->getDebugInfo());
        }

        // ldr_mst_inside_planner
        $sql_pln = "SELECT p.emp_id, emp_lt_nm || ' ' || emp_ft_nm as emp_name FROM ldr_mst_inside_planner p JOIN empmst USING (emp_id) WHERE inside_id= ? ORDER BY order_no";
        $sth_pln = $this->db->prepare($sql_pln, array('integer'), MDB2_PREPARE_RESULT);
        $res_pln = $sth_pln->execute(array($id));
        if (PEAR::isError($res_pln)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res_pln->getDebugInfo());
        }
        while ($row = $res_pln->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $data['emp_planner'][$row['emp_id']] = $row['emp_name'];
        }
        $sth_pln->free();

        // ldr_mst_inside_coach
        $sql_coa = "SELECT c.emp_id, emp_lt_nm || ' ' || emp_ft_nm as emp_name FROM ldr_mst_inside_coach c JOIN empmst USING (emp_id) WHERE inside_id= ? ORDER BY order_no";
        $sth_coa = $this->db->prepare($sql_coa, array('integer'), MDB2_PREPARE_RESULT);
        $res_coa = $sth_coa->execute(array($id));
        if (PEAR::isError($res_coa)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res_coa->getDebugInfo());
        }
        while ($row = $res_coa->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $data['emp_coach'][$row['emp_id']] = $row['emp_name'];
        }
        $sth_coa->free();

        // ldr_mst_inside_guideline
        $sql_gui = "SELECT g.guideline_id, guideline FROM ldr_mst_inside_guideline g JOIN ldr_guideline USING (guideline_id) WHERE inside_id= ? ORDER BY order_no";
        $sth_gui = $this->db->prepare($sql_gui, array('integer'), MDB2_PREPARE_RESULT);
        $res_gui = $sth_gui->execute(array($id));
        if (PEAR::isError($res_gui)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res_gui->getDebugInfo());
        }
        while ($row = $res_gui->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $data['guideline'][$row['guideline_id']] = $row['guideline'];
        }
        $sth_gui->free();

        return $data;
    }

    /**
     * 一件取得 (研修内容付き)
     *
     * @param type $id
     */
    public function find_contents($id, $contents_id = null)
    {
        if (is_null($id)) {
            return array();
        }

        $data = $this->find($id);

        // ldr_mst_inside_contents
        if (is_null($contents_id)) {
            $sql = "
                SELECT
                    c.*,
                    ch.form
                FROM ldr_mst_inside_contents c
                LEFT JOIN ldr_mst_inside_check ch ON ch.inside_contents_id = c.inside_contents_id AND NOT ch.del_flg
                WHERE
                  NOT c.del_flg
                  AND inside_id= ?
                ORDER BY number
            ";
            $sth = $this->db->prepare($sql, array('integer'), MDB2_PREPARE_RESULT);
            $res = $sth->execute(array($id));
            if (PEAR::isError($res)) {
                throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
            }
            while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                $data['contents'][$row['number']] = $row;
            }
            $sth->free();
        }
        else {
            $sql = "
                SELECT
                    *
                FROM ldr_mst_inside_contents
                WHERE
                  NOT del_flg
                  AND inside_contents_id= ?
            ";
            $row = $this->db->extended->getRow(
                $sql, null, array($contents_id), array('integer'), MDB2_FETCHMODE_ASSOC
            );
            if (PEAR::isError($row)) {
                throw new Exception(__METHOD__ . ' ERROR: ' . $row->getDebugInfo());
            }
            $data['contents'] = $row;
        }

        return $data;
    }

    /**
     * 一件取得 (研修日程付き)
     *
     * @param type $id
     */
    public function find_date($id, $date_id = null)
    {
        if (is_null($id)) {
            return array();
        }

        $data = $this->find_contents($id);

        // ldr_mst_inside_date
        if (is_null($date_id)) {
            $sql = "
                SELECT
                    d.*,
                    c.number,
                    c.contents,
                    e.emp_lt_nm || ' ' || e.emp_ft_nm as update_emp_name,
                    (SELECT count(*) FROM ldr_trn_inside trn WHERE trn.inside_date_id=d.inside_date_id AND trn.status = 3 AND NOT trn.del_flg) as count
                FROM ldr_mst_inside_date d
                JOIN ldr_mst_inside_contents c USING (inside_contents_id)
                JOIN empmst e ON e.emp_id=d.update_emp_id
                WHERE
                  NOT d.del_flg
                  AND d.inside_id= ?
                ORDER BY c.number,training_date, training_start_time, inside_date_id
            ";
            $sth = $this->db->prepare($sql, array('integer'), MDB2_PREPARE_RESULT);
            $res = $sth->execute(array($id));
            if (PEAR::isError($res)) {
                throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
            }
            while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                $row['reception'] = Ldr_Util::get_inside_reception($row);
                $row['reception_text'] = Ldr_Const::get_inside_reception($row['reception']);
                $data['dates'][] = $row;
            }
            $sth->free();
        }
        else {
            $sql = "
                SELECT
                    d.*,
                    to_char(d.training_start_time,'HH24:MI') as training_start_time,
                    to_char(d.training_last_time,'HH24:MI') as training_last_time,
                    to_char(d.accept_date,'YYYY-MM-DD') as accept_date,
                    to_char(d.accept_date,'HH24:MI') as accept_time,
                    to_char(d.deadline_date,'YYYY-MM-DD') as deadline_date,
                    to_char(d.deadline_date,'HH24:MI') as deadline_time,
                    c.number,
                    c.contents,
                    (SELECT count(*) FROM ldr_trn_inside trn WHERE trn.inside_date_id=d.inside_date_id AND trn.status = 3 AND NOT trn.del_flg) as count,
                    (SELECT count(*) FROM ldr_trn_inside trn WHERE trn.inside_date_id=d.inside_date_id AND trn.status >= 0 AND NOT trn.del_flg) as applied_count
                FROM ldr_mst_inside_date d
                JOIN ldr_mst_inside_contents c USING (inside_contents_id)
                WHERE
                  NOT d.del_flg
                  AND d.inside_date_id= ?
            ";
            $row = $this->db->extended->getRow(
                $sql, null, array($date_id), array('integer'), MDB2_FETCHMODE_ASSOC
            );
            if (PEAR::isError($row)) {
                throw new Exception(__METHOD__ . ' ERROR: ' . $row->getDebugInfo());
            }

            $row['reception'] = Ldr_Util::get_inside_reception($row);
            $row['reception_text'] = Ldr_Const::get_inside_reception($row['reception']);
            $data['date'] = $row;
        }

        return $data;
    }

    /**
     * 年のリストを取得する
     * @return type
     */
    public function year_lists()
    {
        $th_year = Ldr_Util::th_year();
        $min_year = $this->db->queryOne('SELECT MIN(year) FROM ldr_mst_inside WHERE NOT del_flg');
        $max_year = $th_year + 1;

        if (!$min_year) {
            $min_year = $th_year;
        }
        else if ($min_year > $th_year) {
            $min_year = $th_year;
        }

        if ($min_year > $max_year) {
            return array_reverse(range($max_year, $min_year));
        }
        else {
            return array_reverse(range($min_year, $max_year));
        }
    }

    /**
     * 研修日程の変更
     * @param type $id
     * @param type $status
     * @throws Exception
     */
    public function change_date_status($id, $status)
    {
        $this->db->beginNestedTransaction();
        if (is_array($id)) {
            $holder = implode(',', array_fill(0, count($id), '?'));
            $sql_cond = " inside_date_id IN($holder) ";
            $data = array_merge(array($status), $id);
            $types = array_merge(array('integer'), array_fill(0, count($id), 'integer'));
            $msg = implode(",", $id);
        }
        else {
            $sql_cond = "inside_date_id= ?";
            $types = array('integer', 'integer');
            $data = array($status, $id);

            $msg = $id;
        }

        $sql = "UPDATE ldr_mst_inside_date
                SET
                    status = ?
                WHERE
                    $sql_cond
                    AND NOT del_flg";
        $sth = $this->db->prepare($sql, $types, MDB2_PREPARE_MANIP);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $sth->free();

        $this->db->completeNestedTransaction();

        $this->log->log("研修日程のステータスを変更しました", "status={$status} id={$msg}");
    }

}
