<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'ladder/class/Validation.php';
require_once 'ladder/class/Apply.php';
require_once 'ladder/class/Log.php';

class Ldr_Auditor extends Model
{

    var $log;

    /**
     * コンストラクタ
     * @param type $emp_id
     */
    public function Ldr_Auditor()
    {
        parent::connectDB();
        $this->log = new Ldr_log('auditor');
    }

    /**
     * validate
     * @param type $data
     * @return string
     */
    public function validate($data)
    {
        $error = array();
        if (Validation::is_empty($data['auditor'])) {
            $error['auditor'][] = '監査者が設定されていません';
        }
        return $error;
    }

    /**
     * 一件検索 & 複数件検索
     * @param type $id 申請ID
     * @return type
     * @throws Exception
     */
    public function find($id)
    {
        if (is_null($id)) {
            return array();
        }

        $sql = "SELECT emp_id
              FROM ldr_auditor WHERE ldr_apply_id=:ldr_apply_id";

        $sth = $this->db->prepare($sql, array('integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array('ldr_apply_id' => $id));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }
        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row['emp_id'];
        }
        $sth->free();

        return $lists;
    }

    /**
     * 保存
     * @param Array $data
     */
    public function save($data)
    {
        $this->db->beginNestedTransaction();

        //同じidr_apply_idを消す。
        $sth = $this->db->prepare("DELETE FROM ldr_auditor WHERE ldr_apply_id = :id ", array('integer'), MDB2_PREPARE_MANIP);
        $res = $sth->execute($data);

        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception('ldr_auditor INSERT or UPDATE ERROR: ' . $res->getDebugInfo());
        }

        //同じidr_apply_idを追加する
        foreach ($data['auditor'] as $auditor) {
            $data = array('id' => $data['id'], 'auditor' => $auditor);
            $sth = $this->db->prepare("INSERT INTO ldr_auditor (ldr_apply_id, emp_id) VALUES (:id, :auditor) ", array('integer', 'text'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($data);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception('ldr_auditor INSERT or UPDATE ERROR: ' . $res->getDebugInfo());
            }
        }

        $this->db->completeNestedTransaction();
        $this->log->log("保存しました", "id={$data['id']}");
    }

}
