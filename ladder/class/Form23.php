<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'ladder/class/Validation.php';
require_once 'ladder/class/Log.php';
require_once 'ladder/class/Const.php';

class Ldr_Form23 extends Model
{

    var $emp_id;
    var $log;

    /**
     * コンストラクタ
     * @param type $emp_id 職員ID
     */
    public function Ldr_Form23($emp_id)
    {
        parent::connectDB();
        $this->emp_id = $emp_id;
        $this->log = new Ldr_log('form23');
    }

    /**
     * validate
     * @param Array $data
     * @return Array
     */
    public function validate($data)
    {
        $error = array();

        // 期間
        if (Validation::is_empty($data['period'])) {
            $error['period'][] = '年月日が入力されていません';
        }
        else {
            if (!Validation::is_date($data['period'])) {
                $error['period'][] = '年月日が正しくありません';
            }
        }

        // 科目
        if (Validation::is_empty($data['subject'])) {
            $error['subject'][] = '科目が入力されていません';
        }

        // 教科内容
        if (Validation::is_empty($data['unit'])) {
            $error['unit'][] = '教科内容が入力されていません';
        }

        // 研修名
        if (Validation::is_empty($data['organizer'])) {
            $error['organizer'][] = '主催が入力されていません';
        }
        elseif (mb_strlen($data['organizer'], 'EUC-JP') > 200) {
            $error['organizer'][] = '主催は200文字以内で入力してください';
        }

        return $error;
    }

    /**
     * csv用のテキストを作成
     * @return type
     */
    public function export_csv()
    {
        $list = $this->lists();
        $fp = fopen('php://temp', 'r+b');
        foreach ($list as $data) {
            fputcsv($fp, array(
                $data['period'],
                $data['subject'],
                $data['unit'],
                $data['contents'],
                $data['organizer']
            ));
        }
        rewind($fp);

        $csv = "年月日,科目,単元,教科内容,主催\r\n";
        $csv .= str_replace(PHP_EOL, "\r\n", stream_get_contents($fp));

        // Log
        $this->log->log("CSVをエクスポートしました");

        return $csv;
    }

    /**
     * データのインポート
     * @param type $file
     * @return type
     * @throws Exception
     */
    public function import_csv($file)
    {
        $fp = Ldr_Util::file_to_euc($file['tmp_name']);
        $line = 0;
        $cnt = 0;
        $error_all = array();
        $this->db->beginNestedTransaction();
        while ($row = fgetcsv($fp)) {
            $line++;
            $data = array();
            list($data['period'], $data['subject'], $data['unit'], $data['contents'], $data['organizer']) = $row;
            if ($data['period'] === '年月日') {
                continue;
            }

            $er = $this->validate($data);
            foreach ($er as $tmp) {
                foreach ($tmp as $value) {
                    $error_all[] = $line . "行目： " . $value;
                }
            }
            if (empty($er)) {
                try {
                    $this->save($data);
                }
                catch (Exception $e) {
                    $error_all[] = $line . "行目： 更新エラー";
                    cmx_log($e->getMessage());
                }
            }
            $cnt++;
        }
        if (!empty($error_all)) {
            $this->db->failNestedTransaction();
            throw new Exception(implode(',', $error_all));
        }
        $this->db->completeNestedTransaction();

        // Log
        $this->log->log("{$cnt}件のデータをインポートしました");

        return $cnt . "件のデータをインポートしました";
    }

    /**
     * 検索条件の作成
     * @param type $cond
     * @return string
     */
    protected function make_condition($cond = null)
    {
        $cond_ret = "";
        $types = array();
        $data = array();
        if (!empty($cond)) {
            if (!empty($cond['srh_keyword'])) {
                $keywords = preg_split(Ldr_Const::SRH_DELIMITERS, $cond['srh_keyword'], -1, PREG_SPLIT_NO_EMPTY);
                $cond_key.= "AND ( ";
                foreach ($keywords as $key => $value) {
                    if ($key != 0) {
                        $cond_key.= " AND ";
                    }

                    $label_key = "key" . $key;
                    $cond_key.= "(subject || ' ' || unit || ' ' || contents || ' ' || organizer) ILIKE :" . $label_key . " ";
                    $types[] = 'text';
                    $data[$label_key] = "%" . $value . "%";
                }
                $cond_key.= ") ";
                $cond_ret .= $cond_key;
            }
            if (!empty($cond['srh_start_date'])) {
                $cond_ret .= " AND ( :srh_start_date <= period) ";
                $types[] = 'text';
                $data['srh_start_date'] = $cond['srh_start_date'];
            }
            if (!empty($cond['srh_last_date'])) {
                $cond_ret .= " AND (period <= :srh_last_date ) ";
                $types[] = 'text';
                $data['srh_last_date'] = $cond['srh_last_date'];
            }
        }
        $ret['cond'] = $cond_ret;
        $ret['types'] = $types;
        $ret['data'] = $data;
        return $ret;
    }

    /**
     * データ数を取得します
     * @return type
     */
    public function count($cond = null)
    {
        $sql = "SELECT count(*) FROM ldr_form2_2_3 WHERE emp_id = :emp_id AND NOT del_flg ";
        $ret = $this->make_condition($cond);
        $sql.= $ret['cond'];
        $types = array_merge(array('integer', 'text'), $ret['types']);
        $data = array_merge(array('emp_id' => $this->emp_id), $ret['data']);
        $count = $this->db->extended->getOne($sql, null, $data, $types);
        if (PEAR::isError($count)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $count->getDebugInfo());
        }

        return $count;
    }

    /**
     * 一覧の取得
     * @param type $st
     * @param type $pageCount
     * @param type $cond
     * @return type
     */
    public function lists($st = null, $pageCount = null, $cond = null)
    {
        $sql = "SELECT inner_id, emp_id, period, subject, unit, contents, organizer,training_id FROM ldr_form2_2_3 WHERE emp_id = :emp_id AND NOT del_flg ";
        $ret = $this->make_condition($cond);
        $sql .= $ret['cond'];
        $cond['order_by'][] = "inner_id";
        $order = "";
        foreach ($cond['order_by'] as $value) {
            $order.= empty($order) ? "order by " : ", ";
            $order.= $value;
        }
        $sql .= $order;

        if (isset($st) && isset($pageCount)) {
            $this->db->setLimit($pageCount, $st);
        }

        $types = array_merge(array('integer', 'text'), $ret['types']);
        $data = array_merge(array('emp_id' => $this->emp_id), $ret['data']);
        $sth = $this->db->prepare($sql, $types, MDB2_PREPARE_RESULT);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['unit_roman'] = Ldr_Const::get_roman_num($row['unit']);
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 保存
     * @param String $emp_id
     * @param Array $data
     */
    public function save($data)
    {
        $data['emp_id'] = $this->emp_id;

        $this->db->beginNestedTransaction();

        // INSERT
        if (empty($data['inner_id'])) {
            $sth = $this->db->prepare("INSERT INTO ldr_form2_2_3
                (emp_id, period, subject, unit, contents, organizer)
                VALUES
                (:emp_id, :period, :subject, :unit, :contents, :organizer)", array('text', 'text', 'text', 'text', 'text'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($data);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
            }
            $sth->free();
            $data['inner_id'] = $this->db->lastInsertID('ldr_form2_2_3', 'inner_id');
        }

        // UPDATE
        else {
            $sth = $this->db->prepare("UPDATE ldr_form2_2_3
                SET
                period= :period,
                subject= :subject ,
                unit= :unit,
                contents= :contents,
                organizer= :organizer
                WHERE
                inner_id= :inner_id
                AND emp_id = :emp_id", array('text', 'integer', 'text', 'text', 'text', 'integer', 'text'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($data);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
            }
            $sth->free();
        }

        $this->db->completeNestedTransaction();

        // Log
        $this->log->log("保存しました", "id={$data['inner_id']} subject={$data['subject']} unit={$data['unit']}");
    }

    /**
     * 削除
     * @param type $id     inner_id
     * @throws Exception
     */
    public function delete($id)
    {
        $this->db->beginNestedTransaction();
        $data = $this->find($id);
        if (isset($data)) {
            // UPDATE
            $data["del_flg"] = TRUE;

            $sth = $this->db->prepare("UPDATE ldr_form2_2_3 SET del_flg = true WHERE inner_id= :inner_id AND emp_id = :emp_id", array('boolean'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($data);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
            }
            $this->db->completeNestedTransaction();
        }

        $sth->free();

        // Log
        $this->log->log("削除しました", "id=$id");
    }

    /**
     * inner_idに該当するデータを取得
     * @param Integer $id
     * @return Array
     */
    public function find($id)
    {
        $sql = "SELECT inner_id, emp_id,period, subject, unit, contents, organizer, training_id FROM ldr_form2_2_3 WHERE inner_id= :inner_id AND emp_id = :emp_id AND NOT del_flg";
        $row = $this->db->extended->getRow(
            $sql, null, array('inner_id' => $id, 'emp_id' => $this->emp_id), array('integer', 'text'), MDB2_FETCHMODE_ASSOC
        );
        if (PEAR::isError($row)) {
            throw new Exception(__METHOD__ . " ERROR: " . $row->getDebugInfo());
        }
        return $row;
    }

    /**
     * 最終更新日の取得
     * @return type
     */
    public function last_updated()
    {
        $sql = "SELECT to_char( max(updated_on),'YYYY-MM-DD HH24:MI:SS') as updated_on FROM ldr_form2_2_3
                WHERE emp_id = :emp_id AND NOT del_flg";
        $row = $this->db->extended->getRow(
            $sql, null, array('emp_id' => $this->emp_id), array('text'), MDB2_FETCHMODE_ASSOC
        );
        if (empty($row)) {
            return null;
        }

        return $row['updated_on'];
    }

    /**
     * 削除（研修用）
     * flagとtraining_idから削除を行う
     * @param type $flag
     * @param type $training_id
     * @throws Exception
     */
    public function delete_training($training_id)
    {
        if (empty($training_id)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }
        $this->db->beginNestedTransaction();

        // UPDATE
        $sth = $this->db->prepare("UPDATE ldr_form2_2_3 SET del_flg = true WHERE training_id = :training_id AND NOT del_flg", array('boolean'), MDB2_PREPARE_MANIP);
        $res = $sth->execute(array('training_id' => $training_id));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }

        $sth->free();

        $this->db->completeNestedTransaction();
    }

    /**
     * 研修から保存
     * @param array $data
     * @throws Exception
     */
    public function save_training($data)
    {
        $this->db->beginNestedTransaction();
        $data['emp_id'] = $this->emp_id;

        // UPDATE or INSERT
        $sth = $this->db->prepare("SELECT update_ldr_form2_2_3_training(:emp_id, :training_id, :period, :subject, :unit, :contents, :organizer)", array('text', 'integer', 'integer', 'date', 'text', 'text', 'text', 'text'), MDB2_PREPARE_MANIP);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }
        $sth->free();

        $this->db->completeNestedTransaction();
    }

}
