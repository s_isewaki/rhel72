<?php

require_once 'Cmx.php';
require_once 'Cmx/Model/Employee.php';
require_once 'ladder/class/Const.php';
require_once 'ladder/class/Validation.php';
require_once 'ladder/class/Ladder.php';
require_once 'ladder/class/Log.php';
require_once 'ladder/class/Config.php';

/**
 * キャリア開発ラダー職員
 */
class Ldr_Employee extends Cmx_Employee
{

    private $log;
    private $view_class;
    private $ladder;
    private $ladders;

    /**
     * コンストラクタ
     */
    public function Ldr_Employee($param = null)
    {
        $this->ladder = new Ldr_Ladder();

        $this->conf = new Ldr_Config();
        $this->view_class = $this->conf->view_class();

        $this->log = new Ldr_log('employee');

        parent::Cmx_Employee($param);
    }

    /**
     * SELECTのstatement handle設定
     *   findメソッドで使われるSELECT文を設定する
     *   _selectメソッド(Cmx_Employee)で呼び出される
     */
    public function set_select_sth()
    {
        // ラダー
        list($level_sql, $level_join) = $this->level_sql();

        $this->_select_sth = $this->db->prepare("
            SELECT
                *,
                {$level_sql}
                ldr_employee.updated_on as ldr_updated_on,
                CASE WHEN ldr_approver.emp_id IS NULL THEN false ELSE true END as approver,
                CASE WHEN ldr_mst_inside_planner.emp_id IS NULL OR ldr_mst_inside.del_flg THEN false ELSE true END as inside_planner,
                CASE WHEN ldr_committee.emp_id IS NULL THEN false ELSE true END as committee,
                CASE WHEN ldr_permission_analysis.emp_id IS NULL THEN false ELSE true END as analysis,
                CASE WHEN ldr_permission_relief.emp_id IS NULL THEN false ELSE true END as relief,
                CASE WHEN ldr_permission_inside.emp_id IS NULL THEN false ELSE true END as inside,
                CASE WHEN ldr_permission_outside.emp_id IS NULL THEN false ELSE true END as outside,
                CASE WHEN ldr_permission_training.emp_id IS NULL THEN false ELSE true END as training,
                (SELECT COUNT(*) FROM ldr_novice a WHERE a.emp_id=empmst.emp_id) AS novice,
                (SELECT COUNT(*) FROM ldr_facilitator a WHERE a.facilitate_emp_id=empmst.emp_id) AS facilitator
            FROM empmst
            JOIN login   USING (emp_id)
            JOIN authmst USING (emp_id)
            LEFT JOIN ldr_approver USING (emp_id)
            LEFT JOIN ldr_mst_inside_planner USING (emp_id)
            LEFT JOIN ldr_mst_inside USING (inside_id)
            LEFT JOIN ldr_employee USING (emp_id)
            LEFT JOIN ldr_committee USING (emp_id)
            LEFT JOIN ldr_permission_analysis USING (emp_id)
            LEFT JOIN ldr_permission_relief USING (emp_id)
            LEFT JOIN ldr_permission_inside USING (emp_id)
            LEFT JOIN ldr_permission_outside USING (emp_id)
            LEFT JOIN ldr_permission_training USING (emp_id)
            LEFT JOIN empcond USING (emp_id)
            LEFT JOIN jobmst ON jobmst.job_id=emp_job
            LEFT JOIN stmst ON stmst.st_id=emp_st
            LEFT JOIN classmst ON classmst.class_id=emp_class
            LEFT JOIN atrbmst ON atrbmst.atrb_id=emp_attribute
            LEFT JOIN deptmst ON deptmst.dept_id=emp_dept
            LEFT JOIN classroom ON classroom.room_id=emp_room
            {$level_join}
            WHERE empmst.emp_id= ?
        ");
    }

    /**
     * 職員取得
     * @param String $emp_id 職員ID
     * @return Array
     */
    public function find($emp_id)
    {
        $this->_select($emp_id);
        $this->_row['license_date'] = Ldr_Util::date_to_month($this->_row['license_date']);
        $this->_row['nurse_date'] = Ldr_Util::date_to_month($this->_row['nurse_date']);
        $this->_row['hospital_date'] = Ldr_Util::date_to_month($this->_row['hospital_date']);
        $this->_row['ward_date'] = Ldr_Util::date_to_month($this->_row['ward_date']);

        return $this->_row;
    }

    /**
     * 検索条件
     * @param array $cond 検索条件
     * @return Array
     */
    protected function make_condition($cond = array())
    {
        if (empty($cond)) {
            return array();
        }

        $cond_ret = "";
        $types = array();
        $data = array();

        // キーワード
        if (!empty($cond['srh_keyword'])) {
            $keywords = preg_split(Ldr_Const::SRH_DELIMITERS, $cond['srh_keyword'], -1, PREG_SPLIT_NO_EMPTY);
            $cond_key.= "AND ( ";
            foreach ($keywords as $key => $value) {
                if ($key !== 0) {
                    $cond_key.= " AND ";
                }

                $label_key = "key" . $key;
                $cond_key.= "(empmst.emp_personal_id || ' ' || empmst.emp_lt_nm || ' ' || empmst.emp_ft_nm || ' ' || empmst.emp_kn_lt_nm || ' ' || empmst.emp_kn_ft_nm) ILIKE :" . $label_key . " ";
                $types[] = 'text';
                $data[$label_key] = "%" . $value . "%";
            }
            $cond_key.= ") ";
            $cond_ret .= $cond_key;
        }

        // 組織1,2
        if (!empty($cond['srh_class1'])) {
            list($data['class_id'], $data['atrb_id']) = explode("_", $cond['srh_class1']);
            $cond_ret .= " AND emp_class = :class_id AND emp_attribute = :atrb_id";
            $types[] = "integer";
            $types[] = "integer";
        }

        // 組織3,4
        if (!empty($cond['srh_class3'])) {
            list($data['dept_id'], $data['room_id']) = explode("_", $cond['srh_class3']);
            $cond_ret .= " AND emp_dept = :dept_id AND COALESCE(emp_room,0) = COALESCE(:room_id,0)";
            $types[] = "integer";
            $types[] = "integer";
        }

        $ret['cond'] = $cond_ret;
        $ret['types'] = $types;
        $ret['data'] = $data;

        return $ret;
    }

    /**
     * 件数
     * @param type $option
     * @return type
     */
    public function count($option)
    {
        $cond = $this->make_condition($option);
        if (isset($cond['cond'])) {
            $keyword = $cond['cond'];
        }

        $sth = $this->db->prepare("
            SELECT
                count(*)
            FROM empmst
            JOIN login   USING (emp_id)
            JOIN authmst USING (emp_id)
            WHERE NOT authmst.emp_del_flg
            $keyword
        ", $cond['types'], MDB2_PREPARE_RESULT);
        $res = $sth->execute($cond['data']);
        $sth->free();
        $count = $res->fetchOne();
        return $count;
    }

    /**
     * 職員一覧
     * @param type $option
     * @return \Ldr_Employee
     */
    public function lists($option = array())
    {
        // ラダー
        list($level_sql, $level_join) = $this->level_sql();

        // ソート
        if (isset($option['order_by'])) {
            $order_by = "ORDER BY {$option['order_by']}";
        }

        // 表示件数
        if (isset($option['start']) && isset($option['per_page'])) {
            $this->db->setLimit($option['per_page'], $option['start']);
        }

        // 条件
        $cond = $this->make_condition($option);
        if (isset($cond['cond'])) {
            $keyword = $cond['cond'];
        }

        $sth = $this->db->prepare("
            SELECT
                *,
                {$level_sql}
                jobmst.order_no as job_order_no,
                stmst.order_no as st_order_no,
                classmst.order_no as class_order_no,
                atrbmst.order_no as atrb_order_no,
                deptmst.order_no as dept_order_no,
                classroom.order_no as room_order_no,
                ldr_employee.updated_on as ldr_updated_on,
                CASE WHEN ldr_committee.emp_id IS NULL THEN false ELSE true END as committee,
                CASE WHEN ldr_permission_analysis.emp_id IS NULL THEN false ELSE true END as analysis,
                CASE WHEN ldr_permission_relief.emp_id IS NULL THEN false ELSE true END as relief,
                CASE WHEN ldr_permission_outside.emp_id IS NULL THEN false ELSE true END as outside,
                CASE WHEN ldr_permission_training.emp_id IS NULL THEN false ELSE true END as training,
                (SELECT COUNT(*) FROM ldr_novice a WHERE a.emp_id=empmst.emp_id) AS novice,
                (SELECT COUNT(*) FROM ldr_facilitator a WHERE a.facilitate_emp_id=empmst.emp_id) AS facilitator
            FROM empmst
            JOIN login   USING (emp_id)
            JOIN authmst USING (emp_id)
            LEFT JOIN ldr_employee USING (emp_id)
            LEFT JOIN ldr_committee USING (emp_id)
            LEFT JOIN ldr_permission_analysis USING (emp_id)
            LEFT JOIN ldr_permission_relief USING (emp_id)
            LEFT JOIN ldr_permission_outside USING (emp_id)
            LEFT JOIN ldr_permission_training USING (emp_id)
            LEFT JOIN empcond USING (emp_id)
            LEFT JOIN jobmst ON jobmst.job_id=emp_job
            LEFT JOIN stmst ON stmst.st_id=emp_st
            LEFT JOIN classmst ON classmst.class_id=emp_class
            LEFT JOIN atrbmst ON atrbmst.atrb_id=emp_attribute
            LEFT JOIN deptmst ON deptmst.dept_id=emp_dept
            LEFT JOIN classroom ON classroom.room_id=emp_room
            JOIN ldr_analysis_authority ac ON ac.class_id = emp_class
                AND ac.atrb_id = emp_attribute
                AND ac.dept_id = emp_dept
                AND (ac.room_id = emp_room OR (ac.room_id IS NULL AND emp_room IS NULL))
                AND ac.auth_flg
                AND NOT ac.del_flg
            {$level_join}
            WHERE NOT authmst.emp_del_flg
            $keyword
            $order_by
        ", $cond['types'], MDB2_PREPARE_RESULT);
        $res = $sth->execute($cond['data']);
        $sth->free();
        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[] = new Ldr_Employee($row);
        }
        return $list;
    }

    /**
     * CSVエクスポート
     * @return type
     */
    public function export_csv()
    {
        $list = $this->lists(array('order_by' => 'classmst.order_no, atrbmst.order_no, deptmst.order_no, classroom.order_no, empmst.emp_id'));
        $fp = fopen('php://temp', 'r+b');

        foreach ($list as $data) {
            $row = array(
                $data->emp_personal_id(),
                $data->emp_name(),
                $data->graduation_year(),
                $data->school(),
                Ldr_Util::month_to_date($data->license_date()),
                Ldr_Util::month_to_date($data->nurse_date()),
                Ldr_Util::month_to_date($data->hospital_date()),
                Ldr_Util::month_to_date($data->ward_date()),
                $data->jna_id(),
                $data->pref_na_id(),
            );

            foreach ($this->ladder->lists() as $ldr) {
                $levels = $data->levels($ldr['ladder_id']);
                foreach ($ldr['config']['level'] as $level) {
                    $row[] = $levels[$level]['date'];
                    $row[] = $levels[$level]['hospital'];
                    $row[] = $levels[$level]['number'];
                }
            }

            fputcsv($fp, $row);
        }
        rewind($fp);

        $csv = "職員ID,職員名,卒業年,出身校,看護師免許取得年月,臨床開始年月,当院配属年月,部署配属年月,JNA会員番号,都道府県看護協会 会員番号";
        foreach ($this->ladder->lists() as $ldr) {
            foreach ($ldr['config']['level'] as $level) {
                if ($level < 1) {
                    $level_text = $ldr['config']['level0'];
                }
                else {
                    $level_text = Ldr_Const::get_roman_num($level);
                }
                $csv .= ",{$ldr['title']}レベル{$level_text} 認定日,{$ldr['title']}レベル{$level_text} 取得施設,{$ldr['title']}レベル{$level_text} 認定番号";
            }
        }
        $csv .= "\r\n";
        $csv .= str_replace(PHP_EOL, "\r\n", stream_get_contents($fp));

        // Log
        $this->log->log('CSVをエクスポートしました');

        return $csv;
    }

    /**
     * CSVインポート
     * @param type $file
     * @return type
     * @throws Exception
     */
    public function import_csv($file)
    {
        // empmst fetch
        $emp = array();
        $emp_res = $this->db->query('SELECT emp_id, emp_personal_id, ldr_employee.emp_id AS ldr_emp_id FROM empmst JOIN authmst USING (emp_id) LEFT JOIN ldr_employee USING (emp_id) WHERE NOT emp_del_flg');
        while ($row = $emp_res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $emp[$row['emp_personal_id']] = array('emp_id' => $row['emp_id'], 'ldr_emp_id' => $row['ldr_emp_id']);
        }

        $update_sth = $this->db->prepare("SELECT update_ldr_employee_csv(:emp_id, :nurse_date, :hospital_date, :ward_date, :jna_id, :pref_na_id, :school, :graduation_year, :license_date)", array('text', 'text', 'text', 'text', 'text', 'text', 'text', 'text', 'text'), MDB2_PREPARE_MANIP);
        $level_sth = $this->db->prepare("SELECT update_ldr_employee_level(:emp_id, :ladder_id, :level, :date, :hospital, :number)", array('text', 'integer', 'integer', 'text', 'text', 'text'), MDB2_PREPARE_MANIP);

        $line = 0;
        $error_all = array();
        $fp = fopen($file['tmp_name'], 'r');
        $this->db->beginNestedTransaction();
        while (($csv = fgetcsv_reg($fp)) !== false) {
            mb_convert_variables('EUC-JP', 'SJIS-Win', $csv);

            $error = array();

            if ($csv[0] === '職員ID') {
                $header = $csv;
                continue;
            }
            $line++;

            // 空データは飛ばす
            if (empty($csv[0])) {
                continue;
            }

            if (!isset($emp[$csv[0]])) {
                $error[] = "{$line}行目: {$csv[0]}({$csv[1]}) に該当する職員が見つかりません";
            }

            // 会員番号ゼロ埋め
            if (!empty($csv[8])) {
                $csv[8] = str_pad($csv[8], 8, "0", STR_PAD_LEFT);
            }
            if (!empty($csv[9])) {
                $csv[9] = str_pad($csv[9], 6, "0", STR_PAD_LEFT);
            }

            $valid = $this->import_csv_validate($csv, $header, $line);
            $error = array_merge($error, $valid);

            if (empty($error)) {
                // data
                $data = array(
                    'emp_id' => $emp[$csv[0]]['emp_id'],
                    'graduation_year' => $csv[2],
                    'school' => $csv[3],
                    'license_date' => Ldr_Util::month_to_date($csv[4]),
                    'nurse_date' => Ldr_Util::month_to_date($csv[5]),
                    'hospital_date' => Ldr_Util::month_to_date($csv[6]),
                    'ward_date' => Ldr_Util::month_to_date($csv[7]),
                    'jna_id' => $csv[8],
                    'pref_na_id' => $csv[9],
                );

                // level
                $levels = array();
                $idx = 10;
                foreach ($this->ladder->lists() as $ldr) {
                    foreach ($ldr['config']['level'] as $level) {
                        $levels[] = array(
                            'emp_id' => $emp[$csv[0]]['emp_id'],
                            'ladder_id' => $ldr['ladder_id'],
                            'level' => $level,
                            'date' => $csv[$idx++],
                            'hospital' => $csv[$idx++],
                            'number' => $csv[$idx++],
                        );
                    }
                }

                // INSERT or UPDATE
                $res = $update_sth->execute($data);
                if (PEAR::isError($res)) {
                    cmx_log(__METHOD__ . " ERROR: " . $res->getDebugInfo());
                    $error[] = "{$line}行目: {$csv[0]}({$csv[1]}) 登録更新エラー";
                }
                $res = $this->db->extended->executeMultiple($level_sth, $levels);
                if (PEAR::isError($res)) {
                    cmx_log(__METHOD__ . " ERROR: " . $res->getDebugInfo());
                    $error[] = "{$line}行目: {$csv[0]}({$csv[1]}) 登録更新エラー";
                }
            }

            $error_all = array_merge($error_all, $error);
        }

        if (!empty($error_all)) {
            $this->db->failNestedTransaction();
            $update_sth->free();
            throw new Exception(implode(',', $error_all));
        }
        $this->db->completeNestedTransaction();
        $update_sth->free();

        // Log
        $this->log->log("{$line}件のデータをインポートしました");

        return "{$line}件のデータをインポートしました";
    }

    /**
     * CoMedix連携
     * @param type $file
     * @return type
     * @throws Exception
     */
    public function comedix_import($status)
    {

        $this->db->beginNestedTransaction();

        // 入職日→当院配属年月 UPDATE
        if ($status === '1') {
            $res = $this->db->query("UPDATE ldr_employee SET hospital_date=TO_DATE(emp_join,'YYYYMMDD') FROM empmst WHERE empmst.emp_id=ldr_employee.emp_id AND emp_join != ''");
        }
        else {
            $res = $this->db->query("UPDATE ldr_employee SET hospital_date=TO_DATE(emp_join,'YYYYMMDD') FROM empmst WHERE empmst.emp_id=ldr_employee.emp_id AND hospital_date IS NULL AND emp_join != ''");
        }
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }

        // ファントルくんプロフィール:職種開始年月→臨床開始年月 UPDATE
        if ($status === '1') {
            $res = $this->db->query("UPDATE ldr_employee SET nurse_date=TO_DATE(exp_year||exp_month||'01','YYYYMMDD') FROM inci_profile WHERE inci_profile.emp_id=ldr_employee.emp_id AND exp_year != '' AND exp_month != ''");
        }
        else {
            $res = $this->db->query("UPDATE ldr_employee SET nurse_date=TO_DATE(exp_year||exp_month||'01','YYYYMMDD') FROM inci_profile WHERE inci_profile.emp_id=ldr_employee.emp_id AND nurse_date IS NULL AND exp_year != '' AND exp_month != ''");
        }
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }

        // ファントルくんプロフィール:部署開始年月→部署開始年月 UPDATE
        if ($status === '1') {
            $res = $this->db->query("UPDATE ldr_employee SET ward_date=TO_DATE(dept_year||dept_month||'01','YYYYMMDD') FROM inci_profile WHERE inci_profile.emp_id=ldr_employee.emp_id AND dept_year != '' AND dept_month != ''");
        }
        else {
            $res = $this->db->query("UPDATE ldr_employee SET ward_date=TO_DATE(dept_year||dept_month||'01','YYYYMMDD') FROM inci_profile WHERE inci_profile.emp_id=ldr_employee.emp_id AND ward_date IS NULL AND dept_year != '' AND dept_month != ''");
        }
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }

        // INSERT
        $sql = "
            INSERT INTO ldr_employee (emp_id, hospital_date, nurse_date, ward_date)
            SELECT
                emp_id,
                CASE WHEN emp_join != '' THEN TO_DATE(emp_join,'YYYYMMDD') ELSE NULL END,
                CASE WHEN exp_year !='' THEN TO_DATE(exp_year||exp_month||'01','YYYYMMDD') ELSE NULL END,
                CASE WHEN dept_year !='' THEN TO_DATE(dept_year||dept_month||'01','YYYYMMDD') ELSE NULL END
                FROM empmst
                LEFT JOIN inci_profile USING (emp_id)
                LEFT JOIN ldr_employee USING (emp_id)
                WHERE ldr_employee.emp_id IS NULL
                  AND (emp_join != '' OR exp_year != ''  OR dept_year != '')
                ";
        $res = $this->db->query($sql);
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }

        $this->db->completeNestedTransaction();

        // Log
        $this->log->log("CoMedix連携を行いました");
    }

    /**
     * CSVバリデーション
     * @param type $csv
     * @param type $line
     * @return type
     */
    public function import_csv_validate($csv, $head, $line)
    {
        $error = array();

        // graduation_year
        if ($csv[2] !== '') {
            if (!Validation::is_year($csv[2])) {
                $error[] = "{$line}行目: {$csv[0]}({$csv[1]})「{$head[2]}」が不正です → {$csv[2]}";
            }
        }
        // school
        if ($csv[3] !== '') {
            if (mb_strlen($csv[3], 'EUC-JP') > 200) {
                $error[] = "{$line}行目: {$csv[0]}({$csv[1]})「{$head[3]}」は200文字以内にしてください";
            }
        }
        // license_date
        if ($csv[4] !== '') {
            if (!Validation::is_month($csv[4])) {
                $error[] = "{$line}行目: {$csv[0]}({$csv[1]})「{$head[4]}」が不正です → {$csv[4]}";
            }
        }

        // nurse_date
        if ($csv[5] !== '') {
            if (!Validation::is_month($csv[5])) {
                $error[] = "{$line}行目: {$csv[0]}({$csv[1]})「{$head[5]}」が不正です → {$csv[5]}";
            }
        }

        // hospital_date
        if ($csv[6] !== '') {
            if (!Validation::is_month($csv[6])) {
                $error[] = "{$line}行目: {$csv[0]}({$csv[1]})「{$head[6]}」が不正です → {$csv[6]}";
            }
        }

        // ward_date
        if ($csv[7] !== '') {
            if (!Validation::is_month($csv[7])) {
                $error[] = "{$line}行目: {$csv[0]}({$csv[1]})「{$head[7]}」が不正です → {$csv[7]}";
            }
        }

        // jna_id
        if ($csv[8] !== '') {
            if (!is_numeric($csv[8]) || mb_strlen($csv[8], 'EUC-JP') !== 8) {
                $error[] = "{$line}行目: {$csv[0]}({$csv[1]})「{$head[8]}」は数値8桁にしてください";
            }
        }

        // pref_na_id
        if ($csv[9] !== '') {
            if (!is_numeric($csv[9]) || mb_strlen($csv[9], 'EUC-JP') !== 6) {
                $error[] = "{$line}行目: {$csv[0]}({$csv[1]})「{$head[9]}」は数値6桁にしてください";
            }
        }

        $idx = 10;
        foreach ($this->ladder->lists() as $ldr) {
            foreach ($ldr['config']['level'] as $level) {

                // lv_date
                if ($csv[$idx] !== '') {
                    if (!Validation::is_date($csv[$idx])) {
                        $error[] = "{$line}行目: {$csv[0]}({$csv[1]})「{$head[$idx]}」が不正です → {$csv[$idx]}";
                    }
                }
                $idx++;

                // lv_hospital
                if ($csv[$idx] !== '') {
                    if (mb_strlen($csv[$idx], 'EUC-JP') > 200) {
                        $error[] = "{$line}行目: {$csv[0]}({$csv[1]})「{$head[$idx]}」は200文字以内にしてください";
                    }
                }
                $idx++;

                // lv_number
                if ($csv[$idx] !== '') {
                    if (mb_strlen($csv[$idx], 'EUC-JP') > 20) {
                        $error[] = "{$line}行目: {$csv[0]}({$csv[1]})「{$head[$idx]}」は20文字以内にしてください";
                    }
                }
                $idx++;
            }
        }

        return $error;
    }

    /**
     * レベル
     * @return type
     */
    public function level($ladder_id)
    {
        return $this->current_level($ladder_id, 'level');
    }

    /**
     * レベル(ローマ数字)
     * @return type
     */
    public function level_roman($ladder_id)
    {
        return Ldr_Const::get_roman_num($this->level($ladder_id));
    }

    /**
     * 臨床開始年月日
     * @return type
     */
    public function nurse_date()
    {
        return $this->_getter(__FUNCTION__);
    }

    /**
     * 当院 臨床開始年月日
     * @return type
     */
    public function hospital_date()
    {
        return $this->_getter(__FUNCTION__);
    }

    /**
     * 部署 臨床開始年月日
     * @return type
     */
    public function ward_date()
    {
        return $this->_getter(__FUNCTION__);
    }

    /**
     * JNA会員番号
     * @return type
     */
    public function jna_id()
    {
        return $this->_getter(__FUNCTION__);
    }

    /**
     * 都道府県看護協会 会員番号
     * @return type
     */
    public function pref_na_id()
    {
        return $this->_getter(__FUNCTION__);
    }

    /**
     * 出身校
     * @return type
     */
    public function school()
    {
        return $this->_getter(__FUNCTION__);
    }

    /**
     * 卒業年
     * @return type
     */
    public function graduation_year()
    {
        return $this->_getter(__FUNCTION__);
    }

    /**
     * 看護師免許取得日
     * @return type
     */
    public function license_date()
    {
        return $this->_getter(__FUNCTION__);
    }

    /**
     * 作成日時
     * @return type
     */
    public function created_on()
    {
        return $this->_getter(__FUNCTION__);
    }

    /**
     * 更新日時
     * @return type
     */
    public function updated_on()
    {
        return $this->_getter('ldr_updated_on');
    }

    /**
     * 認定委員
     * @return type
     */
    public function committee()
    {
        return $this->_row['committee'] === 't';
    }

    /**
     * 監査者設定権限
     * @return type
     */
    public function auditor()
    {
        return $this->_row['auditor'] === 't';
    }

    /**
     * 指定レベルの認定日付を取得
     * @param type $level レベル 省略した場合は現在のレベル
     * @return null
     */
    public function lv_date($ladder_id, $level = null)
    {
        if (empty($this->levels)) {
            $this->init_levels();
        }

        if (is_null($level)) {
            return $this->current_level($ladder_id, 'date');
        }
        return $this->levels[$ladder_id][$level]['date'];
    }

    /**
     * 指定レベルの取得医療施設を取得
     * @param type $level レベル 省略した場合は現在のレベル
     * @return null
     */
    public function lv_hospital($ladder_id, $level = null)
    {
        if (empty($this->levels)) {
            $this->init_levels();
        }

        if (is_null($level)) {
            return $this->current_level($ladder_id, 'hospital');
        }
        return $this->levels[$ladder_id][$level]['hospital'];
    }

    /**
     * 指定レベルの認定番号を取得
     * @param type $level レベル 省略した場合は現在のレベル
     * @return null
     */
    public function lv_number($ladder_id, $level = null)
    {
        if (empty($this->levels)) {
            $this->init_levels();
        }

        if (is_null($level)) {
            return $this->current_level($ladder_id, 'number');
        }
        return $this->levels[$ladder_id][$level]['number'];
    }

    /**
     * キャリア開発ラダー利用者権限
     * @return type
     */
    public function is_ladder()
    {
        return $this->_row['emp_career_flg'] === 't';
    }

    /**
     * キャリア開発ラダー管理者権限
     * @return type
     */
    public function is_admin()
    {
        return $this->_row['emp_careeradm_flg'] === 't';
    }

    /**
     * 認定委員会権限
     * @return type
     */
    public function is_committee()
    {
        return $this->_row['committee'] === 't';
    }

    /**
     * 分析担当権限
     * @return type
     */
    public function is_analysis()
    {
        return $this->_row['analysis'] === 't';
    }

    /**
     * 配置表権限
     * @return type
     */
    public function is_analysis_placement()
    {
        $flg = $this->conf->analysis_placement();

        if ($flg !== '2') {
            return $this->_row['analysis'] === 't' || $this->_row['committee'] === 't' || $this->_row['emp_careeradm_flg'] === 't';
        }
        else {
            return $this->_row['analysis'] === 't';
        }
    }

    /**
     * 申請承認者権限
     * @return type
     */
    public function is_approver()
    {
        return $this->_row['approver'] === 't';
    }

    /**
     * 院内研修企画担当権限
     * @return type
     */
    public function is_inside_planner()
    {
        return $this->_row['inside_planner'] === 't';
    }

    /**
     * 院内研修担当権限
     * @return type
     */
    public function is_inside()
    {
        return $this->_row['inside'] === 't';
    }

    /**
     * 救護員担当権限
     * @return type
     */
    public function is_relief()
    {
        return $this->_row['relief'] === 't';
    }

    /**
     * 院外研修担当権限
     * @return type
     */
    public function is_outside()
    {
        return $this->_row['outside'] === 't';
    }

    /**
     * 研修管理者権限
     * @return type
     */
    public function is_training()
    {
        return $this->_row['training'] === 't';
    }

    /**
     * 研修受付権限
     */
    public function is_trn_accept()
    {
        return ($this->is_inside() || $this->is_inside_planner() || $this->is_relief() || $this->is_outside() || $this->is_training());
    }

    /**
     * 新人評価対象
     * @return type
     */
    public function is_novice()
    {
        return $this->_row['novice'] > 0;
    }

    /**
     * 新人教育担当者対象
     * @return type
     */
    public function is_facilitator()
    {
        return $this->_row['facilitator'] > 0;
    }

    /**
     * 新人評価機能
     * @return type
     */
    public function is_novice_tab()
    {
        return ($this->is_novice() || $this->is_facilitator());
    }

    /**
     * バリデーション
     * @param Array $data
     * @return Array
     */
    public function validate($data)
    {
        $error = array();

        // 卒業年
        if (!empty($data['graduation_year']) && !Validation::is_year($data['graduation_year'])) {
            $error['graduation_year'][] = '卒業年が正しくありません';
        }

        // 出身校
        if ($data['school'] && mb_strlen($data['school'], 'EUC-JP') > 200) {
            $error['school'][] = '出身校は200文字以内で入力してください';
        }

        // 免許取得日
        if ($data['license_date'] && !Validation::is_month($data['license_date'])) {
            $error['license_date'][] = '看護師免許取得年月が正しくありません';
        }

        // 臨床開始年月日
        if ($data['nurse_date'] && !Validation::is_month($data['nurse_date'])) {
            $error['nurse_date'][] = '臨床開始年月が正しくありません';
        }

        // 当院 臨床開始年月日
        if ($data['hospital_date'] && !Validation::is_month($data['hospital_date'])) {
            $error['hospital_date'][] = '当院 配属年月が正しくありません';
        }

        // 部署 臨床開始年月日
        if ($data['ward_date'] and ! Validation::is_month($data['ward_date'])) {
            $error['ward_date'][] = '部署 配属年月が正しくありません';
        }

        foreach ($data['lv_date'] as $ladder_id => $lv_date) {
            foreach ($lv_date as $level => $date) {
                // レベル 認定日
                if ($date && !Validation::is_date($date)) {
                    $error['lv_date'][$ladder_id][$level][] = '認定日が正しくありません';
                }

                // レベル 取得医療施設
                if ($data['lv_hospital'][$ladder_id][$level] && mb_strlen($data['lv_hospital'][$ladder_id][$level], 'EUC-JP') > 200) {
                    $error['lv_hospital'][$ladder_id][$level][] = '取得医療施設は200文字以内で入力してください';
                }

                // レベル 認定番号
                if ($data['lv_number'][$ladder_id][$level] && mb_strlen($data['lv_number'][$ladder_id][$level], 'EUC-JP') > 20) {
                    $error['lv_number'][$ladder_id][$level][] = '認定番号は20文字以内で入力してください';
                }
            }
        }

        // JNA会員番号
        if ($data['jna_id'] && (!is_numeric($data['jna_id']) || mb_strlen($data['jna_id'], 'EUC-JP') !== 8)) {
            $error['jna_id'][] = 'JNA会員番号は数値8桁で入力してください';
        }
        // 都道府県看護協会 会員番号
        if ($data['pref_na_id'] && (!is_numeric($data['pref_na_id']) || mb_strlen($data['pref_na_id'], 'EUC-JP') !== 6)) {
            $error['pref_na_id'][] = '都道府県看護協会 会員番号は数値6桁で入力してください';
        }

        return $error;
    }

    /**
     * 保存
     * @param String $emp_id
     * @param Array $data
     */
    public function save($data)
    {
        $data['license_date'] = Ldr_Util::month_to_date($data['license_date']);
        $data['nurse_date'] = Ldr_Util::month_to_date($data['nurse_date']);
        $data['hospital_date'] = Ldr_Util::month_to_date($data['hospital_date']);
        $data['ward_date'] = Ldr_Util::month_to_date($data['ward_date']);

        $levels = array();
        // lv_date
        foreach ($data['level'] as $ladder_id => $lv) {
            foreach ($lv as $level => $tmp) {
                $levels[] = array(
                    'emp_id' => $data['emp_id'],
                    'ladder_id' => $ladder_id,
                    'level' => $level,
                    'date' => $data['lv_date'][$ladder_id][$level],
                    'hospital' => $data['lv_hospital'][$ladder_id][$level],
                    'number' => $data['lv_number'][$ladder_id][$level],
                );
            }
        }
        // INSERT or UPDATE
        $sth = $this->db->prepare("SELECT update_ldr_employee(:emp_id, null, :nurse_date, :hospital_date, :ward_date, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, :jna_id, :pref_na_id, :school, :graduation_year, :license_date)", array('text', 'text', 'text', 'text', 'text', 'text', 'text', 'text', 'text'), MDB2_PREPARE_MANIP);
        $level_sth = $this->db->prepare("SELECT update_ldr_employee_level(:emp_id, :ladder_id, :level, :date, :hospital, :number)", array('text', 'integer', 'integer', 'text', 'text', 'text'), MDB2_PREPARE_MANIP);

        $this->db->beginNestedTransaction();

        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception('ldr_employee INSERT or UPDATE ERROR: ' . $res->getDebugInfo());
        }

        $res = $this->db->extended->executeMultiple($level_sth, $levels);
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception('ldr_employee-level INSERT or UPDATE ERROR: ' . $res->getDebugInfo());
        }

        $this->db->completeNestedTransaction();
        $sth->free();

        // Log
        $this->log->log('保存しました', "emp_id={$data['emp_id']}");
    }

    /**
     * レベル認定の保存
     * @param Array $data
     */
    public function save_level($data)
    {
        $this->db->beginNestedTransaction();

        // INSERT or UPDATE
        $level_sth = $this->db->prepare("SELECT update_ldr_employee_level(:emp_id, :ladder_id, :level, :date, :hospital, :number)", array('text', 'integer', 'integer', 'text', 'text', 'text'), MDB2_PREPARE_MANIP);
        $res = $level_sth->execute($data);
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception('ldr_employee-level INSERT or UPDATE ERROR: ' . $res->getDebugInfo());
        }

        $level_sth->free();

        // Log
        $this->log->log('レベル認定を保存しました', "emp_id={$data['emp_id']}");

        $this->db->completeNestedTransaction();
    }

    /**
     * 保存(ユーザ設定)
     * @param type $data
     */
    public function save_config($data)
    {
        $this->log = new Ldr_log('config_user');
        $this->db->beginNestedTransaction();
        $old_data = $this->find($data['emp_id']);
        $new_data = array_merge((array)$old_data, (array)$data);
        $this->save($new_data);
        $this->db->completeNestedTransaction();
    }

    /**
     *  申請承認者
     * @return \Ldr_Employee
     */
    public function approver()
    {
        $sth = $this->db->prepare("SELECT emp_id FROM ldr_approver WHERE class_id= ? AND atrb_id= ? AND dept_id= ? AND (room_id= ? OR room_id IS NULL)", array('integer', 'integer', 'integer', 'integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array($this->class_id(), $this->atrb_id(), $this->dept_id(), $this->room_id()));
        $sth->free();
        $emp_id = $res->fetchOne();

        if (empty($emp_id)) {
            return array();
        }
        else {
            return new Ldr_Employee($emp_id);
        }
    }

    /**
     * 指定部署の職員一覧を取得する。
     * @return type
     */
    public function section_lists($data)
    {
        $sql = "(SELECT emp_personal_id,empmst.emp_id,emp_lt_nm || ' ' || emp_ft_nm as name
            FROM empmst
            JOIN authmst au ON empmst.emp_id = au.emp_id AND NOT au.emp_del_flg
            WHERE
              emp_class = :class_id
              AND emp_attribute = :atrb_id
              AND emp_dept = :dept_id
              AND (emp_room IS NULL OR emp_room = :room_id) )
            ORDER BY emp_class,emp_attribute,emp_dept,emp_room
        ";
        $sth = $this->db->prepare($sql, array('integer', 'integer', 'integer', 'integer'), MDB2_PREPARE_RESULT);

        $res = $sth->execute($data);

        if (PEAR::isError($res)) {
            throw new Exception('empmst SELECT ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row;
        }
        return $lists;
    }

    /**
     * ユーザ設定確認
     */
    public function is_user_setting()
    {
        $conf = new Ldr_Config();
        $req = $conf->required();

        //卒業年
        if ($req['graduation_year'] && Validation::is_empty($this->graduation_year())) {
            return false;
        }

        //出身校
        if ($req['school'] && Validation::is_empty($this->school())) {
            return false;
        }

        //看護師免許取得日
        if ($req['license_date'] && Validation::is_empty($this->license_date())) {
            return false;
        }

        //臨床開始日
        if ($req['nurse_date'] && Validation::is_empty($this->nurse_date())) {
            return false;
        }

        //当院臨床開始日
        if ($req['hospital_date'] && Validation::is_empty($this->hospital_date())) {
            return false;
        }

        //部署臨床開始日
        if ($req['ward_date'] && Validation::is_empty($this->ward_date())) {
            return false;
        }

        //日本看護協会 会員番号
        if ($req['jna_id'] && Validation::is_empty($this->jna_id())) {
            return false;
        }

        //都道府県看護協会 会員番号
        if ($req['pref_na_id'] && Validation::is_empty($this->pref_na_id())) {
            return false;
        }

        return true;
    }

    /**
     * クラス名の取得
     * ※ldr用にoverride
     * @return type
     */
    function class_full_name()
    {
        return Ldr_Util::class_full_name($this->_row, $this->view_class);
    }

    /**
     * 対象ラダーリスト
     * @param type $emp_id
     */
    public function ladders()
    {
        if (!empty($this->ladders)) {
            return $this->ladders;
        }

        $this->ladders = $this->ladder->emp_ladder_lists($this->emp_id());
        return $this->ladders;
    }

    /**
     * 全取得レベルリスト
     * @param type $ladder_id
     */
    public function levels($ladder_id)
    {
        if (empty($this->levels)) {
            $this->init_levels();
        }

        return $this->levels[$ladder_id];
    }

    /**
     * 現在の取得レベル
     * @param type $ladder_id
     */
    public function current_level($ladder_id, $key = null)
    {
        if (empty($this->current_level)) {
            $this->init_levels();
        }

        if ($key) {
            return $this->current_level[$ladder_id][$key];
        }
        return $this->current_level[$ladder_id];
    }

    /**
     * レベル取得
     */
    private function init_levels()
    {
        $sql = "
            SELECT
                *
            FROM ldr_level
            WHERE emp_id = ?
            ORDER BY level DESC
        ";
        $sth = $this->db->prepare($sql, array('text'), MDB2_PREPARE_RESULT);

        $res = $sth->execute(array($this->emp_id()));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $this->levels[$row['ladder_id']][$row['level']] = $row;

            if (empty($this->current_level[$row['ladder_id']]) && $row['date']) {
                $this->current_level[$row['ladder_id']] = $row;
            }
        }
    }

    /**
     * レベル取得用SQL, JOIN
     * @return type
     */
    private function level_sql()
    {
        $sql = '';
        foreach ($this->ladder->lists() as $ladder) {
            $join .= "LEFT JOIN (SELECT emp_id, MAX(level) AS level FROM ldr_level WHERE ladder_id={$ladder['ladder_id']} AND date IS NOT NULL GROUP BY emp_id) ldr{$ladder['ladder_id']} USING (emp_id) ";
        }

        return array($sql, $join);
    }

    /**
     * 職員ID変換
     * @param type $emp_personal_id
     * @return boolean|\Ldr_Employee
     */
    public function from_personal_id($emp_personal_id)
    {
        $row = $this->db->extended->getRow('SELECT emp_id FROM empmst WHERE emp_personal_id = ?', null, array($emp_personal_id), array('text'), MDB2_FETCHMODE_ASSOC);
        if (!empty($row['emp_id'])) {
            return $row['emp_id'];
        }
        return false;
    }

    /**
     * 看護師経験年数
     * @param type $emp_personal_id
     * @return boolean|\Ldr_Employee
     */
    public function nurse_age()
    {
        $date = $this->nurse_date();
        if (empty($date)) {
            return '';
        }
        $diff = Ldr_Util::diff_th_year($date, date("Y-m-d"));
        return $diff < 0 ? '' : $diff;
    }

    /**
     * レベルアップ件数
     * @param type $option
     * @return type
     */
    public function levelup_count($option)
    {
        $cond = $this->levelup_make_condition($option);
        if (isset($cond['cond'])) {
            $keyword = $cond['cond'];
        }

        $sth = $this->db->prepare("
            SELECT
                count(*)
            FROM empmst
            JOIN login   USING (emp_id)
            JOIN authmst USING (emp_id)
            WHERE NOT authmst.emp_del_flg
            $keyword
        ", $cond['types'], MDB2_PREPARE_RESULT);
        $res = $sth->execute($cond['data']);
        $sth->free();
        $count = $res->fetchOne();
        return $count;
    }

    /**
     * レベルアップ検索条件
     * @param array $cond 検索条件
     * @return Array
     */
    protected function levelup_make_condition($cond = array())
    {
        if (empty($cond)) {
            return array();
        }

        $cond_ret = "";
        $types = array();
        $data = array();

        // 組織3,4
        if (!empty($cond['srh_class3'])) {
            list($data['dept_id'], $data['room_id']) = explode("_", $cond['srh_class3']);
            $cond_ret .= " AND emp_dept = :dept_id AND COALESCE(emp_room,0) = COALESCE(:room_id,0)";
            $types[] = "integer";
            $types[] = "integer";
        }

        $ret['cond'] = $cond_ret;
        $ret['types'] = $types;
        $ret['data'] = $data;

        return $ret;
    }

    /**
     * レベルアップ状況一覧
     * @param type $option
     * @return \Ldr_Employee
     */
    public function levelup_lists($option = array())
    {
        // レベル
        list($level_sql, $level_join) = $this->level_sql();

        if (isset($option['order_by'])) {
            $order_by = "ORDER BY {$option['order_by']}";
        }
        if (isset($option['start']) && isset($option['per_page'])) {
            $this->db->setLimit($option['per_page'], $option['start']);
        }

        $cond = $this->make_condition($option);
        if (isset($cond['cond'])) {
            $keyword = $cond['cond'];
        }

        // 年
        $cond['data']['start_date'] = sprintf('%s-04-01', $option['year']);
        $cond['data']['end_date'] = sprintf('%s-03-31', $option['year'] + 1);
        $cond['types'][] = 'integer';
        $cond['types'][] = 'integer';

        // ラダー
        $cond['data']['ladder_id'] = $option['ladder_id'];
        $cond['types'][] = 'integer';

        $sth = $this->db->prepare("
            SELECT
                *,
                {$level_sql}
                jobmst.order_no as job_order_no,
                stmst.order_no as st_order_no,
                classmst.order_no as class_order_no,
                atrbmst.order_no as atrb_order_no,
                deptmst.order_no as dept_order_no,
                classroom.order_no as room_order_no,
                ldr_employee.updated_on as ldr_updated_on,
                CASE WHEN ldr_committee.emp_id IS NULL THEN false ELSE true END as committee,
                CASE WHEN ldr_permission_analysis.emp_id IS NULL THEN false ELSE true END as analysis,
                CASE WHEN ldr_permission_relief.emp_id IS NULL THEN false ELSE true END as relief,
                CASE WHEN ldr_permission_outside.emp_id IS NULL THEN false ELSE true END as outside,
                CASE WHEN ldr_permission_training.emp_id IS NULL THEN false ELSE true END as training,
                (SELECT COUNT(*) FROM ldr_novice a WHERE a.emp_id=empmst.emp_id) AS novice,
                (SELECT COUNT(*) FROM ldr_facilitator a WHERE a.facilitate_emp_id=empmst.emp_id) AS facilitator
            FROM empmst
            JOIN login   USING (emp_id)
            JOIN authmst USING (emp_id)
            LEFT JOIN ldr_employee USING (emp_id)
            LEFT JOIN ldr_committee USING (emp_id)
            LEFT JOIN ldr_permission_analysis USING (emp_id)
            LEFT JOIN ldr_permission_relief USING (emp_id)
            LEFT JOIN ldr_permission_outside USING (emp_id)
            LEFT JOIN ldr_permission_training USING (emp_id)
            LEFT JOIN empcond USING (emp_id)
            LEFT JOIN jobmst ON jobmst.job_id=emp_job
            LEFT JOIN stmst ON stmst.st_id=emp_st
            LEFT JOIN classmst ON classmst.class_id=emp_class
            LEFT JOIN atrbmst ON atrbmst.atrb_id=emp_attribute
            LEFT JOIN deptmst ON deptmst.dept_id=emp_dept
            LEFT JOIN classroom ON classroom.room_id=emp_room
            LEFT JOIN (
                SELECT
                    ap.emp_id,
                    ap.ldr_apply_id,
                    ap.level as apply_level,
                    ldr_form7.level as f7_result,
                    ldr_form8.level as f8_result,
                    ap.result,
                    apply_date,
                    status
                FROM ldr_apply ap
                JOIN ldr_form1 USING (ldr_apply_id)
                LEFT JOIN ldr_form8 USING (ldr_apply_id)
                LEFT JOIN ldr_form7 ON ap.ldr_apply_id=ldr_form7.ldr_apply_id AND ldr_form7.emp_id='total'
                WHERE NOT ap.del_flg
                  AND ladder_id= :ladder_id
                  AND apply_date BETWEEN :start_date AND :end_date
            ) apply USING (emp_id)
            JOIN ldr_analysis_authority ac ON ac.class_id = emp_class
                AND ac.atrb_id = emp_attribute
                AND ac.dept_id = emp_dept
                AND (ac.room_id = emp_room OR (ac.room_id IS NULL AND emp_room IS NULL))
                AND ac.auth_flg
                AND NOT ac.del_flg
            JOIN ldr_ladder_cover lc ON lc.ladder_id= :ladder_id
                AND (lc.class_id = emp_class OR lc.class_id IS NULL)
                AND (lc.atrb_id = emp_attribute OR lc.atrb_id IS NULL)
                AND (lc.dept_id = emp_dept OR lc.dept_id IS NULL)
                AND (lc.room_id = emp_room OR lc.room_id IS NULL)
                AND (lc.job_id = emp_job OR lc.job_id IS NULL)
                AND (lc.st_id = emp_st OR lc.st_id IS NULL)
            {$level_join}
            WHERE NOT authmst.emp_del_flg
            $keyword
            $order_by
        ", $cond['types'], MDB2_PREPARE_RESULT);
        $res = $sth->execute($cond['data']);
        $sth->free();
        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['license_date'] = Ldr_Util::date_to_month($row['license_date']);
            $row['nurse_date'] = Ldr_Util::date_to_month($row['nurse_date']);
            $row['hospital_date'] = Ldr_Util::date_to_month($row['hospital_date']);
            $row['ward_date'] = Ldr_Util::date_to_month($row['ward_date']);

            $list[] = new Ldr_Employee($row);
        }
        return $list;
    }

    /**
     * 認定者一覧
     * @param type $option
     * @return \Ldr_Employee
     */
    public function certification_lists($option = array())
    {
        // レベル
        list($level_sql, $level_join) = $this->level_sql();

        if (isset($option['order_by'])) {
            $order_by = "ORDER BY {$option['order_by']}";
        }

        // 年
        $cond['data']['start_date'] = sprintf('%s-04-01', $option['year']);
        $cond['data']['end_date'] = sprintf('%s-03-31', $option['year'] + 1);
        $cond['types'][] = 'integer';
        $cond['types'][] = 'integer';

        // ラダー
        $cond['data']['ladder_id'] = $option['ladder_id'];
        $cond['types'][] = 'integer';

        $sth = $this->db->prepare("
            SELECT
                *,
                jobmst.order_no as job_order_no,
                stmst.order_no as st_order_no,
                classmst.order_no as class_order_no,
                atrbmst.order_no as atrb_order_no,
                deptmst.order_no as dept_order_no,
                classroom.order_no as room_order_no,
                ldr_employee.updated_on as ldr_updated_on,
                CASE WHEN ldr_committee.emp_id IS NULL THEN false ELSE true END as committee,
                CASE WHEN ldr_permission_analysis.emp_id IS NULL THEN false ELSE true END as analysis,
                CASE WHEN ldr_permission_relief.emp_id IS NULL THEN false ELSE true END as relief,
                CASE WHEN ldr_permission_outside.emp_id IS NULL THEN false ELSE true END as outside,
                CASE WHEN ldr_permission_training.emp_id IS NULL THEN false ELSE true END as training,
                (SELECT COUNT(*) FROM ldr_novice a WHERE a.emp_id=empmst.emp_id) AS novice,
                (SELECT COUNT(*) FROM ldr_facilitator a WHERE a.facilitate_emp_id=empmst.emp_id) AS facilitator
            FROM ldr_level
            JOIN empmst USING (emp_id)
            JOIN authmst USING (emp_id)
            LEFT JOIN ldr_employee USING (emp_id)
            LEFT JOIN ldr_committee USING (emp_id)
            LEFT JOIN ldr_permission_analysis USING (emp_id)
            LEFT JOIN ldr_permission_relief USING (emp_id)
            LEFT JOIN ldr_permission_outside USING (emp_id)
            LEFT JOIN ldr_permission_training USING (emp_id)
            LEFT JOIN empcond USING (emp_id)
            LEFT JOIN jobmst ON jobmst.job_id=emp_job
            LEFT JOIN stmst ON stmst.st_id=emp_st
            LEFT JOIN classmst ON classmst.class_id=emp_class
            LEFT JOIN atrbmst ON atrbmst.atrb_id=emp_attribute
            LEFT JOIN deptmst ON deptmst.dept_id=emp_dept
            LEFT JOIN classroom ON classroom.room_id=emp_room
            WHERE NOT authmst.emp_del_flg
              AND ldr_level.ladder_id = :ladder_id
              AND ldr_level.date BETWEEN :start_date AND :end_date
            $order_by
        ", $cond['types'], MDB2_PREPARE_RESULT);
        $res = $sth->execute($cond['data']);
        $sth->free();
        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['license_date'] = Ldr_Util::date_to_month($row['license_date']);
            $row['nurse_date'] = Ldr_Util::date_to_month($row['nurse_date']);
            $row['hospital_date'] = Ldr_Util::date_to_month($row['hospital_date']);
            $row['ward_date'] = Ldr_Util::date_to_month($row['ward_date']);

            $list[] = new Ldr_Employee($row);
        }
        return $list;
    }

    /**
     * 申請レベル
     * @return type
     */
    public function apply_level()
    {
        return $this->_getter(__FUNCTION__);
    }

    /**
     * 申請レベル(ローマ数字)
     * @return type
     */
    public function apply_level_roman()
    {
        return Ldr_Const::get_roman_num($this->apply_level());
    }

}
