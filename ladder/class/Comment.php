<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'ladder/class/Log.php';

class Ldr_Comment extends Model
{

    var $log;
    var $emp_id;

    /**
     * コンストラクタ
     */
    public function Ldr_Comment($emp_id)
    {
        parent::connectDB();
        $this->emp_id = $emp_id;
        $this->log = new Ldr_log('comment');
    }

    /**
     * 一覧の取得
     * @return Array
     */
    public function lists($apply_id)
    {
        $sql = "
            SELECT
                c.ldr_comment_id,
                c.status,
                c.from_emp_id as emp_id,
                e.emp_lt_nm || ' ' || e.emp_ft_nm as emp_name,
                c.comment,
                c.del_flg,
                c.created_on
            FROM ldr_comment c
            JOIN empmst e ON e.emp_id=c.from_emp_id
            WHERE
                ldr_apply_id = :ldr_apply_id
            ORDER BY c.created_on
            ";
        $sth = $this->db->prepare($sql, array('integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array('ldr_apply_id' => $apply_id));
        if (PEAR::isError($res)) {
            throw new Exception('ldr_comment SELECT ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row;
        }
        $sth->free();

        // unread
        $this->unread($apply_id);

        return $lists;
    }

    /**
     * 保存
     * @param Array $data
     */
    public function save($data)
    {
        $this->db->beginNestedTransaction();

        // INSERT
        if (empty($data['ldr_comment_id'])) {
            $sth = $this->db->prepare("INSERT INTO ldr_comment (ldr_apply_id, status, from_emp_id, comment) VALUES (:ldr_apply_id, :status, :from_emp_id, :comment)", array('integer', 'integer', 'text', 'text'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($data);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception('ldr_comment INSERT ERROR: ' . $res->getDebugInfo());
            }
            $sth->free();
        }

        // UPDATE
        else {
            $sth = $this->db->prepare("UPDATE ldr_comment SET ldr_apply_id= :ldr_apply_id, status= :status, from_emp_id= :from_emp_id, comment= :comment WHERE ldr_comment_id= :ldr_comment_id", array('integer', 'integer', 'text', 'text', 'integer'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($data);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception("ldr_comment UPDATE ERROR: {$data['ldr_comment_id']} " . $res->getDebugInfo());
            }
            $sth->free();
        }

        // unread
        $this->unread($data['ldr_apply_id']);

        $this->db->completeNestedTransaction();

        // Log
        $this->log->log("保存しました", "ldr_apply_id={$data['ldr_apply_id']} status={$data['status']}");
    }

    /**
     * 削除
     * @param type $id
     * @throws Exception
     */
    public function delete($id)
    {
        $data = $this->find($id);
        if (isset($data)) {
            $this->db->beginNestedTransaction();
            $sth = $this->db->prepare("UPDATE ldr_guideline_group1 SET del_flg = true WHERE guideline_group1= ?", array('integer'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($id);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception("ldr_guideline_group1 DELETE ERROR: {$data['guideline_group1']} " . $res->getDebugInfo());
            }
            $this->db->completeNestedTransaction();
            $sth->free();
        }

        // Log
        $this->log->log("削除しました", $id);
    }

    /**
     * 既読更新
     * @param type $apply_id
     * @param type $emp_id
     */
    public function unread($apply_id)
    {
        $this->db->beginNestedTransaction();

        // unread
        $sth = $this->db->prepare("SELECT update_ldr_comment_read (?, ?)", array('integer', 'text'), MDB2_PREPARE_MANIP);
        $res = $sth->execute(array($apply_id, $this->emp_id));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception('ldr_comment_read INSERT or UPDATE ERROR: ' . $res->getDebugInfo());
        }
        $sth->free();

        $this->db->completeNestedTransaction();
    }

    /**
     * バリデーション
     * @param type $data
     */
    public function validate($data)
    {
        $error = array();

        if (Validation::is_empty($data['comment'])) {
            $error['comment'][] = 'コメントが入力されていません';
        }

        return $error;
    }

    /**
     * 未読コメント数
     * @return type
     * @throws Exception
     */
    public function count_unread()
    {
        $sql = "
            SELECT SUM(apply) AS apply_count, SUM(accept) AS accept_count FROM (
                SELECT
                    (SELECT COUNT(updated_on) FROM ldr_comment WHERE a.ldr_apply_id=ldr_apply_id AND type='ap' AND (updated_on > cr.timestamp OR cr.timestamp IS NULL)) as apply,
                    (SELECT COUNT(updated_on) FROM ldr_comment WHERE a.ldr_apply_id=ldr_apply_id AND type='ac' AND (updated_on > cr.timestamp OR cr.timestamp IS NULL)) as accept
                FROM
                    (
                        SELECT ldr_apply_id, 'ap' AS type FROM ldr_apply WHERE emp_id = :emp_id AND NOT del_flg
                        UNION
                        SELECT ldr_apply_id, 'ac' AS type FROM ldr_accept JOIN ldr_apply ap USING (ldr_apply_id) WHERE accept_emp_id = :emp_id AND NOT ap.del_flg
                    ) a
                LEFT JOIN ldr_comment_read cr on cr.ldr_apply_id=a.ldr_apply_id AND emp_id = :emp_id
            ) unread
        ";
        $count = $this->db->extended->getRow($sql, null, array('emp_id' => $this->emp_id), array('text'));

        if (PEAR::isError($count)) {
            throw new Exception('ldr_comment COUNT UNREAD ERROR: ' . $count->getDebugInfo());
        }

        return $count;
    }

}
