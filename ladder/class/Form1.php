<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'ladder/class/Validation.php';
require_once 'ladder/class/Apply.php';
require_once 'ladder/class/Log.php';
require_once 'ladder/class/Appraiser.php';
require_once 'ladder/class/Config.php';
require_once 'ladder/class/Ladder.php';
require_once 'Cmx/Model/Status.php';

class Ldr_Form1 extends Model
{

    private $emp_id;
    private $ladder;
    private $log;
    private $conf;

    /**
     * コンストラクタ
     * @param type $emp_id
     */
    public function Ldr_Form1($emp_id, $ladder)
    {
        parent::connectDB();
        $this->emp_id = $emp_id;
        $this->ladder = $ladder;
        $this->log = new Ldr_log('form1');
        $this->conf = new Ldr_Config();
    }

    /**
     * 師長申請時のvalidate
     * @param type $data
     */
    public function validate_qualify($data)
    {
        // 通常validate
        $error = $this->validate($data);

        // 評価会日時・評価会会場 (赤十字フローのみ)
        if ($this->ladder['config']['flow'] === '1' && $this->ladder['config']['meeting'] === '1') {
            // 評価会日時
            if (empty($error['meeting_date']) && (Validation::is_empty($data['meeting_date']) || Validation::is_empty($data['meeting_start_time']) || Validation::is_empty($data['meeting_last_time']))) {
                $error['meeting_date'][] = "評価会日時が入力されていません";
            }

            // 評価会会場
            if (empty($error['meeting_room']) && Validation::is_empty($data['meeting_room'])) {
                $error['meeting_room'][] = '評価会会場が入力されていません';
            }
        }

        // 評価者
        $min = $this->ladder['config']['appr_min'][$data['level']];
        $max = $this->ladder['config']['appr_max'][$data['level']];
        if ($this->ladder['config']['appr'] === '1' && $max > 0) {
            $count = 0;

            // 司会者 (赤十字フローのみ)
            if ($this->ladder['config']['flow'] === '1') {
                $count++;
                $appr_text = '(司会者含む)';

                if (Validation::is_empty($data['facilitator'])) {
                    $error['facilitator'][] = "司会者が設定されていません";
                }
                else {
                    $appr = new Ldr_Appraiser();

                    if (!Validation::is_empty($data['level']) && empty($this->ladder['config']['appr_st'][$data['level']])) {
                        $error['facilitator'][] = "司会者役職が設定されていません。管理者にお問い合わせください";
                    }
                    else if (!$appr->is_facilitator_status($data['facilitator'], $this->ladder['config']['appr_st'][$data['level']])) {
                        $error['facilitator'][] = "司会者の職員(役職)が正しくありません";
                    }
                }
            }

            // 評価者
            if (Validation::is_empty($data['appraiser'])) {
                $error['appraiser'][] = "評価者が設定されていません";
            }
            else {
                $count += count($data['appraiser']);

                if (!Validation::is_empty($data['level']) && !empty($min) && ($count < $min || $max < $count)) {
                    if ($min === $max) {
                        $error['appraiser'][] = "評価者{$appr_text}は{$min}人設定してください";
                    }
                    else {
                        $error['appraiser'][] = "評価者{$appr_text}は{$min}〜{$max}人設定してください";
                    }
                }
            }
        }

        return $error;
    }

    /**
     * 師長受付時のvalidate
     * @param type $data
     */
    public function validate_accept_qualify($data)
    {
        // 通常validate
        $error = $this->validate($data);

        // unset
        unset($error['apply_date']);
        unset($error['level']);
        unset($error['career']);
        unset($error['career_hospital']);
        unset($error['career_ward']);
        unset($error['objective']);

        // 評価者
        $min = $this->ladder['config']['appr_min'][$data['level']];
        $max = $this->ladder['config']['appr_max'][$data['level']];
        if ($this->ladder['config']['appr'] === '2' && $max > 0) {
            $count = 0;

            // 司会者 (赤十字フローのみ)
            if ($this->ladder['config']['flow'] === '1') {
                $count++;
                $appr_text = '(司会者含む)';

                if (Validation::is_empty($data['facilitator'])) {
                    $error['facilitator'][] = "司会者が設定されていません";
                }
                else {
                    $appr = new Ldr_Appraiser();

                    if (!Validation::is_empty($data['level']) && empty($this->ladder['config']['appr_st'][$data['level']])) {
                        $error['facilitator'][] = "司会者役職が設定されていません。管理者にお問い合わせください";
                    }
                    else if (!$appr->is_facilitator_status($data['facilitator'], $this->ladder['config']['appr_st'][$data['level']])) {
                        $error['facilitator'][] = "司会者の職員(役職)が正しくありません";
                    }
                }
            }

            // 評価者
            if (Validation::is_empty($data['appraiser'])) {
                $error['appraiser'][] = "評価者が設定されていません";
            }
            else {
                $count += count($data['appraiser']);

                if (!Validation::is_empty($data['level']) && !empty($min) && ($count < $min || $max < $count)) {
                    if ($min === $max) {
                        $error['appraiser'][] = "評価者{$appr_text}は{$min}人設定してください";
                    }
                    else {
                        $error['appraiser'][] = "評価者{$appr_text}は{$min}〜{$max}人設定してください";
                    }
                }
            }
        }

        return $error;
    }

    /**
     * validate
     * @param type $data
     * @return string
     */
    public function validate($data)
    {
        // memo 評価会日時の以降の項目の必須チェックは師長申請時に行う。
        //      整合のチェックは保存時に行う

        $error = array();

        // 申請年月日
        if (Validation::is_empty($data['apply_date'])) {
            $error['apply_date'][] = '申請年月日が入力されていません';
        }
        else {
            if (!Validation::is_date($data['apply_date'])) {
                $error['apply_date'][] = '申請年月日が正しくありません';
            }
        }

        // レベル
        if (Validation::is_empty($data['level'])) {
            $error['level'][] = '申請レベルが入力されていません';
        }
        else if (!Validation::is_level($data['level'])) {
            $error['level'][] = '不正なレベルが選択されています';
        }

        // 臨床経験年数
        $carrer = array(
            'career' => '臨床経験年数',
            'career_hospital' => '当院での臨床経験年数',
            'career_ward' => '当該病棟での臨床経験年数',
        );
        foreach ($carrer as $key => $text) {
            if (Validation::is_empty($data[$key])) {
                $error[$key][] = "{$text}が入力されていません";
            }
            else {
                if (is_numeric($data[$key])) {
                    if ($data[$key] < 0 || 99 < $data[$key]) {
                        $error[$key][] = "{$text}は0〜99で入力してください";
                    }
                }
                else {
                    $error[$key][] = "{$text}が数値で入力されていません";
                }
            }
        }

        // 当面のあなたの目標
        if (Validation::is_empty($data['objective'])) {
            $error['objective'][] = '当面のあなたの目標が入力されていません';
        }

        // 評価会日時・評価会会場 (赤十字フローのみ)
        if ($this->ladder['config']['flow'] === '1') {
            // 評価会日時
            if (!Validation::is_empty($data['meeting_date']) && !Validation::is_empty($data['meeting_start_time']) && !Validation::is_empty($data['meeting_last_time'])) {
                if (!Validation::is_date($data['meeting_date'])) {
                    $error['meeting_date'][] = "評価会日時の日付が正しくありません";
                }
                if (!Validation::is_time($data['meeting_start_time'])) {
                    $error['meeting_date'][] = "評価会日時の開始時間が正しくありません";
                }
                if (!Validation::is_time($data['meeting_last_time'])) {
                    $error['meeting_date'][] = "評価会日時の終了時間が正しくありません";
                }
                if (empty($error['meeting_date'])) {
                    if ($data['meeting_start_time'] > $data['meeting_last_time']) {
                        $error['meeting_date'][] = "開始時間より終了時間が遅い時刻です";
                    }
                    if (strtotime($data['meeting_date']) - strtotime($data['apply_date']) < ($this->ladder['config']['appr_interval'] * 86400)) {
                        $error['meeting_date'][] = "申請日と評価日の間隔は{$this->ladder['config']['appr_interval']}日以上空けてください";
                    }
                }
            }

            // 評価会会場
            if (mb_strlen($data['meeting_room'], 'EUC-JP') > 200) {
                $error['meeting_room'][] = '評価会会場は200文字以内で入力してください';
            }
        }

        // 評価者のタイミングは本人申請時
        if ($this->ladder['config']['appr'] === '1') {
            $count = 0;

            // 司会者 (赤十字フローのみ)
            if ($this->ladder['config']['flow'] === '1') {
                $count++;
                $appr_text = '(司会者含む)';

                if (!Validation::is_empty($data['facilitator'])) {
                    $appr = new Ldr_Appraiser();

                    if (!Validation::is_empty($data['level']) && empty($this->ladder['config']['appr_st'][$data['level']])) {
                        $error['facilitator'][] = "司会者役職が設定されていません。管理者にお問い合わせください";
                    }
                    else if (!$appr->is_facilitator_status($data['facilitator'], $this->ladder['config']['appr_st'][$data['level']])) {
                        $error['facilitator'][] = "司会者の職員(役職)が正しくありません";
                    }
                }
            }

            // 評価者
            if (!Validation::is_empty($data['appraiser'])) {
                $count += count($data['appraiser']);
                $min = $this->ladder['config']['appr_min'][$data['level']];
                $max = $this->ladder['config']['appr_max'][$data['level']];

                if (!Validation::is_empty($data['level']) && !empty($min) && ($count < $min || $max < $count)) {
                    if ($min === $max) {
                        $error['appraiser'][] = "評価者{$appr_text}は{$min}人設定してください";
                    }
                    else {
                        $error['appraiser'][] = "評価者{$appr_text}は{$min}〜{$max}人設定してください";
                    }
                }
            }
        }

        return $error;
    }

    /**
     * 1件取得
     * @param Integer $id
     * @return Array
     */
    public function find($id)
    {
        if (is_null($id)) {
            return array();
        }
        $sql = "SELECT
                    ldr_apply_id,
                    apply_date,
                    level,
                    career,
                    career_hospital,
                    career_ward,
                    qualify_level,
                    objective,
                    meeting_date,
                    to_char(meeting_start_time,'HH24:MI') as meeting_start_time,
                    to_char(meeting_last_time,'HH24:MI') as meeting_last_time,
                    meeting_room,
                    del_flg,
                    to_char(updated_on,'YYYY-MM-DD HH24:MI:SS') as updated_on
                FROM ldr_form1 WHERE ldr_apply_id= :ldr_apply_id";
        $row = $this->db->extended->getRow(
            $sql, null, array('ldr_apply_id' => $id), array('integer'), MDB2_FETCHMODE_ASSOC
        );
        if (PEAR::isError($row)) {
            throw new Exception(__METHOD__ . " ERROR: " . $row->getDebugInfo());
        }
        return $row;
    }

    /**
     * 保存
     * @param Array $data
     */
    public function save($data)
    {
        $this->db->beginNestedTransaction();

        // UPDATE or INSERT
        $sth = $this->db->prepare("SELECT update_ldr_form1(:ldr_apply_id, :apply_date, :level, :career, :career_hospital, :career_ward, null, :objective, :meeting_date, :meeting_start_time, :meeting_last_time, :meeting_room)", array('integer', 'text', 'integer', 'integer', 'integer', 'integer', 'text', 'text', 'text', 'text'), MDB2_PREPARE_MANIP);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }
        $sth->free();
        $this->log->log("保存しました", "id={$data['ldr_apply_id']}");
        $this->db->completeNestedTransaction();
    }

    /**
     * 評価会を後から保存
     * @param Array $data
     */
    public function save_meeting($data)
    {
        $this->db->beginNestedTransaction();

        $sql = "UPDATE ldr_form1 SET meeting_date =:meeting_date, meeting_start_time =:meeting_start_time, meeting_last_time =:meeting_last_time, meeting_room =:meeting_room WHERE ldr_apply_id= :ldr_apply_id";
        $sth = $this->db->prepare($sql, array('text', 'text', 'text', 'text', 'integer'), MDB2_PREPARE_MANIP);
        $res = $sth->execute(array(
            'meeting_date' => $data['meeting_date'],
            'meeting_start_time' => $data['meeting_start_time'],
            'meeting_last_time' => $data['meeting_last_time'],
            'meeting_room' => $data['meeting_room'],
            'ldr_apply_id' => $this->apply_id,
        ));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }
        $sth->free();

        $this->db->completeNestedTransaction();
    }

}
