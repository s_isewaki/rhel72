<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'ladder/class/Validation.php';
require_once 'ladder/class/Apply.php';
require_once 'ladder/class/Log.php';

class Ldr_Form8 extends Model
{

    var $emp_id;
    var $log;

    /**
     * コンストラクタ
     * @param type $emp_id
     */
    public function Ldr_Form8($emp_id)
    {
        parent::connectDB();
        $this->emp_id = $emp_id;
        $this->log = new Ldr_log('form8');
    }

    /**
     * validate
     * @param type $data
     * @return string
     */
    public function validate($data, $rc = null)
    {
        $error = array();

        // 評価日
        if (Validation::is_empty($data['appraise_date'])) {
            $error['appraise_date'][] = '評価日が入力されていません';
        }
        else {
            if (!Validation::is_date($data['appraise_date'])) {
                $error['appraise_date'][] = '評価日が正しくありません';
            }
        }
        if (Validation::is_empty($data['clinical'])) {
            $error['clinical'][] = '記入されていません';
        }
        if (Validation::is_empty($data['management'])) {
            $error['management'][] = '記入されていません';
        }
        if (Validation::is_empty($data['education'])) {
            $error['education'][] = '記入されていません';
        }
        if (Validation::is_empty($data['redcross'])) {
            $error['redcross'][] = '記入されていません';
        }
        if ($rc === '2') {
            if (Validation::is_empty($data['comment5']) && Validation::is_empty($data['comment5'])) {
                $error['comment5'][] = '記入されていません';
            }
            if (Validation::is_empty($data['comment6']) && Validation::is_empty($data['comment6'])) {
                $error['comment6'][] = '記入されていません';
            }
            if (Validation::is_empty($data['comment7']) && Validation::is_empty($data['comment7'])) {
                $error['comment7'][] = '記入されていません';
            }
        }
        if (Validation::is_empty($data['level'])) {
            $error['level'][] = '総合レベルが入力されていません';
        }
        else {
            $level = intval($data['level']);
            if ($level !== 1 && $level !== 2) {
                $error['level'][] = '総合レベルが正しくありません';
            }
        }
        if (Validation::is_empty($data['objective'])) {
            $error['objective'][] = 'キャリアを高める目標が入力されていません';
        }

        return $error;
    }

    /**
     * 1件取得
     * @param Integer $id
     * @return Array
     */
    public function find($id)
    {
        if (is_null($id)) {
            return array();
        }
        $sql = "SELECT
                    ldr_apply_id,
                    emp_id,
                    appraise_date,
                    clinical,
                    management,
                    education,
                    redcross,
                    comment5,
                    comment6,
                    comment7,
                    level,
                    objective,
                    updated_on
                FROM
                    ldr_form8
                WHERE
                    ldr_apply_id= :ldr_apply_id AND NOT del_flg";
        $row = $this->db->extended->getRow(
            $sql, null, array('ldr_apply_id' => $id), array('integer'), MDB2_FETCHMODE_ASSOC
        );
        if (PEAR::isError($row)) {
            throw new Exception(__METHOD__ . " ERROR: " . $row->getDebugInfo());
        }
        return $row;
    }

    /**
     * 保存
     * @param Array $data
     */
    public function save($data, $mode = null)
    {
        if ($mode === 'auto') {
            $data['clinical'] = from_utf8($data['clinical']);
            $data['management'] = from_utf8($data['management']);
            $data['education'] = from_utf8($data['education']);
            $data['redcross'] = from_utf8($data['redcross']);
            $data['objective'] = from_utf8($data['objective']);
            $data['comment5'] = from_utf8($data['comment5']);
            $data['comment6'] = from_utf8($data['comment6']);
            $data['comment7'] = from_utf8($data['comment7']);
        }

        $this->db->beginNestedTransaction();

        $data['emp_id'] = $this->emp_id;

        if (empty($data['level'])) {
            $data['level'] = 0;
        }

        // INSERT or UPDATE
        $sql = "SELECT update_ldr_form8(:id, :emp_id, :appraise_date, :clinical, :management, :education, :redcross, :level, :objective, :comment5, :comment6, :comment7) ";
        $sth = $this->db->prepare($sql, array('integer', 'text', 'text', 'text', 'text', 'text', 'text', 'integer', 'text', 'text', 'text', 'text'), MDB2_PREPARE_MANIP);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }
        $sth->free();

        $this->db->completeNestedTransaction();

        if ($mode !== 'auto') {
            $this->log->log("保存しました", "id={$data['id']}");
        }
    }

}
