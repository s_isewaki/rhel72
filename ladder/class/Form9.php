<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'ladder/class/Validation.php';
require_once 'ladder/class/Apply.php';
require_once 'ladder/class/Log.php';

class Ldr_Form9 extends Model
{

    var $emp_id;
    var $log;

    /**
     * コンストラクタ
     * @param type $emp_id
     */
    public function Ldr_Form9($emp_id)
    {
        parent::connectDB();
        $this->emp_id = $emp_id;
        $this->log = new Ldr_log('form9');
    }

    /**
     * validate
     * @param type $data
     * @return string
     */
    public function validate($data, $rc = null)
    {
        $error = array();

        // 臨床看護実践 1)
        if (Validation::is_empty($data['clinical1']) && Validation::is_empty($data['clinical1_note'])) {
            $error['clinical1'][] = '備考が記入されていません';
        }
        // 臨床看護実践 2)
        if (Validation::is_empty($data['clinical2']) && Validation::is_empty($data['clinical2_note'])) {
            $error['clinical2'][] = '備考が記入されていません';
        }
        // マネジメント 1)
        if (Validation::is_empty($data['management1']) && Validation::is_empty($data['management1_note'])) {
            $error['management1'][] = '備考が記入されていません';
        }
        // マネジメント 2)
        if (Validation::is_empty($data['management2']) && Validation::is_empty($data['management2_note'])) {
            $error['management2'][] = '備考が記入されていません';
        }
        // 教育・研究 1)
        if (Validation::is_empty($data['education1']) && Validation::is_empty($data['education1_note'])) {
            $error['education1'][] = '備考が記入されていません';
        }
        // 教育・研究 2)
        if (Validation::is_empty($data['education2']) && Validation::is_empty($data['education2_note'])) {
            $error['education2'][] = '備考が記入されていません';
        }
        // 赤十字活動 1)
        if (Validation::is_empty($data['redcross1']) && Validation::is_empty($data['redcross1_note'])) {
            $error['redcross1'][] = '備考が記入されていません';
        }
        // 赤十字活動 2)
        if (Validation::is_empty($data['redcross2']) && Validation::is_empty($data['redcross2_note'])) {
            $error['redcross2'][] = '備考が記入されていません';
        }

        if ($rc === '2') {
            if (Validation::is_empty($data['comment51']) && Validation::is_empty($data['comment51_note'])) {
                $error['comment51'][] = '備考が記入されていません';
            }
            if (Validation::is_empty($data['comment52']) && Validation::is_empty($data['comment52_note'])) {
                $error['comment52'][] = '備考が記入されていません';
            }

            if (Validation::is_empty($data['comment61']) && Validation::is_empty($data['comment61_note'])) {
                $error['comment61'][] = '備考が記入されていません';
            }
            if (Validation::is_empty($data['comment62']) && Validation::is_empty($data['comment62_note'])) {
                $error['comment62'][] = '備考が記入されていません';
            }

            if (Validation::is_empty($data['comment71']) && Validation::is_empty($data['comment71_note'])) {
                $error['comment71'][] = '備考が記入されていません';
            }
            if (Validation::is_empty($data['comment72']) && Validation::is_empty($data['comment72_note'])) {
                $error['comment72'][] = '備考が記入されていません';
            }
        }

        if (Validation::is_empty($data['problem'])) {
            $error['problem'][] = '認定委員としての課題や検討が記入されていません';
        }

        return $error;
    }

    /**
     * 1件取得
     * @param Integer $id
     * @return Array
     */
    public function find($id)
    {
        if (is_null($id)) {
            return array();
        }
        $sql = "SELECT
                    clinical1,
                    clinical1_note,
                    clinical2,
                    clinical2_note,
                    management1,
                    management1_note,
                    management2,
                    management2_note,
                    education1,
                    education1_note,
                    education2,
                    education2_note,
                    redcross1,
                    redcross1_note,
                    redcross2,
                    redcross2_note,
                    comment51,
                    comment51_note,
                    comment52,
                    comment52_note,
                    comment61,
                    comment61_note,
                    comment62,
                    comment62_note,
                    comment71,
                    comment71_note,
                    comment72,
                    comment72_note,
                    problem,
                    updated_on
                FROM
                    ldr_form9
                WHERE
                    ldr_apply_id= :ldr_apply_id AND NOT ldr_form9.del_flg ";
        $row = $this->db->extended->getRow(
            $sql, null, array('ldr_apply_id' => $id), array('integer'), MDB2_FETCHMODE_ASSOC
        );
        if (PEAR::isError($row)) {
            throw new Exception(__METHOD__ . " ERROR: " . $row->getDebugInfo());
        }
        return $row;
    }

    /**
     * 保存
     * @param Array $data
     */
    public function save($data, $mode)
    {
        if ($mode === 'auto') {
            $data['clinical1_note'] = from_utf8($data['clinical1_note']);
            $data['clinical2_note'] = from_utf8($data['clinical2_note']);
            $data['management1_note'] = from_utf8($data['management1_note']);
            $data['management2_note'] = from_utf8($data['management2_note']);
            $data['education1_note'] = from_utf8($data['education1_note']);
            $data['education2_note'] = from_utf8($data['education2_note']);
            $data['redcross1_note'] = from_utf8($data['redcross1_note']);
            $data['redcross2_note'] = from_utf8($data['redcross2_note']);
            $data['comment51_note'] = from_utf8($data['comment51_note']);
            $data['comment52_note'] = from_utf8($data['comment52_note']);
            $data['comment61_note'] = from_utf8($data['comment61_note']);
            $data['comment62_note'] = from_utf8($data['comment62_note']);
            $data['comment71_note'] = from_utf8($data['comment71_note']);
            $data['comment72_note'] = from_utf8($data['comment72_note']);
            $data['problem'] = from_utf8($data['problem']);
        }

        $this->db->beginNestedTransaction();

        $data['clinical1'] = isset($data['clinical1']);
        $data['clinical2'] = isset($data['clinical2']);
        $data['management1'] = isset($data['management1']);
        $data['management2'] = isset($data['management2']);
        $data['education1'] = isset($data['education1']);
        $data['education2'] = isset($data['education2']);
        $data['redcross1'] = isset($data['redcross1']);
        $data['redcross2'] = isset($data['redcross2']);
        $data['comment51'] = isset($data['comment51']);
        $data['comment52'] = isset($data['comment52']);
        $data['comment61'] = isset($data['comment61']);
        $data['comment62'] = isset($data['comment62']);
        $data['comment71'] = isset($data['comment71']);
        $data['comment72'] = isset($data['comment72']);

        // INSERT or UPDATE
        $sql = "SELECT update_ldr_form9(:id,
            :clinical1, :clinical1_note, :clinical2, :clinical2_note,
            :management1, :management1_note, :management2, :management2_note,
            :education1, :education1_note, :education2, :education2_note,
            :redcross1, :redcross1_note, :redcross2, :redcross2_note,
            :problem,
            :comment51, :comment51_note, :comment52, :comment52_note,
            :comment61, :comment61_note, :comment62, :comment62_note,
            :comment71, :comment71_note, :comment72, :comment72_note
            ) ";
        $sth = $this->db->prepare($sql, array('integer',
            'boolean', 'text', 'boolean', 'text',
            'boolean', 'text', 'boolean', 'text',
            'boolean', 'text', 'boolean', 'text',
            'boolean', 'text', 'boolean', 'text',
            'text',
            'boolean', 'text', 'boolean', 'text',
            'boolean', 'text', 'boolean', 'text',
            'boolean', 'text', 'boolean', 'text',
            ), MDB2_PREPARE_MANIP);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }
        $sth->free();

        $this->db->completeNestedTransaction();

        if ($mode !== 'auto') {
            $this->log->log("保存しました", "id={$data['id']}");
        }
    }

}
