<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'ladder/class/Validation.php';
require_once 'ladder/class/Apply.php';
require_once 'ladder/class/Log.php';
require_once 'ladder/class/Employee.php';
require_once 'Cmx/Model/Status.php';

class Ldr_Novice extends Model
{

    var $emp_id;
    var $log;

    /**
     * コンストラクタ
     * @param type $emp_id
     */
    public function Ldr_Novice($emp_id)
    {
        parent::connectDB();
        $this->emp_id = $emp_id;
        $this->log = new Ldr_log('adm_novice');
    }

    /**
     * validate_novice
     * @param type $data
     * @return string
     */
    public function validate_novice($data)
    {
        $error = array();
        // 年度
        if (Validation::is_empty($data['year'])) {
            $error['error'][] = '年度が指定されていません';
        }
        return $error;
    }

    /**
     * validate_facilitator
     * @param type $data
     * @return string
     */
    public function validate_facilitator($data)
    {
        $error = array();
        // 年度
        if (Validation::is_empty($data['year'])) {
            $error['error'][] = '年度が指定されていません';
        }
        // 担当者
        if (Validation::is_empty($data['facilitate_emp_id'])) {
            $error['error'][] = '担当者が指定されていません';
        }
        return $error;
    }

    /**
     * validate_date
     * @param type $data
     * @return string
     */
    public function validate_date($data)
    {
        $error = array();
        // 年度
        if (Validation::is_empty($data['year'])) {
            $error['error'][] = '年度が指定されていません';
        }
        // 日付
        if (Validation::is_empty($data['last_date'])) {
            $error['error'][] = '日程が指定されていません';
        }
        return $error;
    }

    /**
     * 一覧の取得
     * @return Array
     */
    public function lists($year, $class = null)
    {
        if ($class) {
            list($dept_id, $room_id) = explode("_", $class);
            $class_sql = " AND emp_dept = :dept_id ";
            if ($room_id) {
                $class_sql .= "AND emp_room= :room_id";
            }
        }
        $sql = "
            SELECT
                n.emp_id,
                n.year,
                e.emp_lt_nm || ' ' || e.emp_ft_nm AS name
            FROM ldr_novice n
            JOIN empmst e USING (emp_id)
            WHERE NOT n.del_flg
              AND n.year = :year
              {$class_sql}
            ORDER BY e.emp_kn_lt_nm, e.emp_kn_ft_nm
        ";
        $sth = $this->db->prepare($sql, null, MDB2_PREPARE_RESULT);
        $res = $sth->execute(array('year' => $year, 'dept_id' => $dept_id, 'room_id' => $room_id));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }
        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['facilitators'] = $this->facilitate_lists($row['emp_id'], $row['year']);
            $row['emp'] = new Ldr_Employee($row['emp_id']);
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 教育担当者の取得
     * @return Array
     */
    public function facilitate_lists($emp_id, $year)
    {
        $sql = "
            SELECT
                f.facilitate_emp_id AS emp_id,
                e.emp_lt_nm || ' ' || e.emp_ft_nm AS name
            FROM ldr_facilitator f
            JOIN empmst e ON f.facilitate_emp_id=e.emp_id
            WHERE
                f.emp_id = :emp_id AND f.year = :year
            ORDER BY e.emp_kn_lt_nm, e.emp_kn_ft_nm
        ";
        $sth = $this->db->prepare($sql, array('integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array('emp_id' => $emp_id, 'year' => $year));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }
        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /*
     * 日程一覧の取得
     * @return Array
     */

    public function date_lists($year)
    {
        $sql = "
            SELECT
                year,
                number,
                last_date
            FROM ldr_novice_date
            WHERE NOT del_flg
              AND year = :year
            ORDER BY number
        ";
        $sth = $this->db->prepare($sql, array('integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array('year' => $year));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }
        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row;
        }
        return $lists;
    }

    /**
     * 新人職員:保存
     * @param Array $data
     */
    public function save_novice($data)
    {
        $this->db->beginNestedTransaction();

        // DELETE
        $sth_d = $this->db->prepare("DELETE FROM ldr_novice WHERE year = :year", array('integer'), MDB2_PREPARE_MANIP);
        $res_d = $sth_d->execute($data);
        if (PEAR::isError($res_d)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . " ERROR: " . $res_d->getDebugInfo());
        }

        // INSERT
        foreach ($data['emp_id'] as $emp_id) {
            $sth = $this->db->prepare("INSERT INTO ldr_novice (emp_id, year) VALUES(:emp_id, :year)", array('text', 'integer'), MDB2_PREPARE_MANIP);
            $res = $sth->execute(array('emp_id' => $emp_id, 'year' => $data['year']));
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
            }
        }
        $this->db->completeNestedTransaction();
        $this->log->log("新人職員を保存しました", $data['year']);
    }

    /**
     * 教育担当者:保存
     * @param Array $data
     */
    public function save_facilitator($data)
    {
        $this->db->beginNestedTransaction();

        // DELETE
        $sth_d = $this->db->prepare("DELETE FROM ldr_facilitator WHERE year = :year", array('integer'), MDB2_PREPARE_MANIP);
        $res_d = $sth_d->execute($data);
        if (PEAR::isError($res_d)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . " ERROR: " . $res_d->getDebugInfo());
        }

        // INSERT
        $sth = $this->db->prepare("INSERT INTO ldr_facilitator (emp_id, year, facilitate_emp_id) VALUES (:emp_id, :year, :facilitate_emp_id)", array('text', 'integer', 'text'), MDB2_PREPARE_MANIP);
        foreach ($data['facilitate_emp_id'] as $emp_id => $facilitators) {
            foreach ($facilitators as $facilitate_emp_id) {
                $res = $sth->execute(array('emp_id' => $emp_id, 'year' => $data['year'], 'facilitate_emp_id' => $facilitate_emp_id));
                if (PEAR::isError($res)) {
                    $this->db->failNestedTransaction();
                    throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
                }
            }
        }
        $this->db->completeNestedTransaction();
        $this->log->log("教育担当者を保存しました", $data['year']);
    }

    /**
     * 評価日程：保存
     * @param Array $data
     */
    public function save_date($data)
    {
        $this->db->beginNestedTransaction();

        // DELETE
        $sth_d = $this->db->prepare("DELETE FROM ldr_novice_date WHERE year = :year", array('integer'), MDB2_PREPARE_MANIP);
        $res_d = $sth_d->execute($data);
        if (PEAR::isError($res_d)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . " ERROR: " . $res_d->getDebugInfo());
        }

        // INSERT
        $sth = $this->db->prepare("INSERT INTO ldr_novice_date (year, number ,last_date) VALUES(:year, :number, :last_date)", array('integer', 'integer', 'date'), MDB2_PREPARE_MANIP);
        foreach ($data['last_date'] as $number => $last_date) {
            if (!empty($last_date)) {
                //追加
                $res = $sth->execute(array('year' => $data['year'], 'number' => $number + 1, 'last_date' => $last_date));
                if (PEAR::isError($res)) {
                    $this->db->failNestedTransaction();
                    throw new Exception(__METHOD__ . " ERROR : " . $res->getDebugInfo());
                }
            }
        }

        $this->db->completeNestedTransaction();
        $this->log->log("評価日程を保存しました", $data['year']);
    }

    /**
     * 存在する年のリストを取得する
     * @return type
     */
    public function year_lists()
    {
        $min_year = $this->db->queryOne('SELECT year FROM ldr_novice_assessment WHERE NOT del_flg ORDER BY year LIMIT 1');
        if (!$min_year) {
            $min_year = Ldr_Util::th_year();
        }
        $th_year = Ldr_Util::th_year() + 1;

        if ($min_year > $th_year) {
            return array_reverse(range($th_year, $min_year));
        }
        else {
            return array_reverse(range($min_year, $th_year));
        }
    }

    /**
     * データのインポート
     * @param type $file
     * @return type
     * @throws Exception
     */
    public function import_csv($year, $file)
    {
        $emp = new Ldr_Employee();
        $fp = Ldr_Util::file_to_euc($file['tmp_name']);
        $line = 0;
        $cnt = 0;
        $error_all = array();
        $none = '';

        $this->db->beginNestedTransaction();

        while ($row = fgetcsv($fp)) {
            $line++;
            list($emp_personal_id, $emp_name) = $row;

            $emp_id = $emp->from_personal_id($emp_personal_id);
            $is_novice = $this->is_novice($emp_id, $year);

            if ($emp_id && !$is_novice) {
                try {
                    $this->import($emp_id, $year);
                    $cnt++;
                }
                catch (Exception $e) {
                    $error_all[] = $line . "行目： 更新エラー";
                    cmx_log($e->getMessage());
                }
            }
            else {
                if (!$emp_id) {
                    $none .= "{$emp_personal_id}({$emp_name}) に該当する職員なし<br>";
                }
                else {
                    $none .= "{$emp_personal_id}({$emp_name}) は登録済みです<br>";
                }
            }
        }

        if (!empty($error_all)) {
            $this->db->failNestedTransaction();
            throw new Exception(implode(',', $error_all));
        }
        $this->db->completeNestedTransaction();

        // Log
        $this->log->log("{$cnt}件のデータをインポートしました");

        return $cnt . "件のデータをインポートしました<br>$none";
    }

    /**
     * 教育担当者データのインポート
     * @param type $file
     * @return type
     * @throws Exception
     */
    public function import_facilitator_csv($year, $mode, $file)
    {
        $emp = new Ldr_Employee();
        $fp = Ldr_Util::file_to_euc($file['tmp_name']);
        $line = 0;
        $cnt = 0;
        $none = '';
        $insert = array();
        $dup = array();

        $this->db->beginNestedTransaction();

        // 登録済みを全て削除して入れ直し
        if ($mode === '2') {
            $del_sth = $this->db->prepare('DELETE FROM ldr_facilitator WHERE year = ?', array('integer'), MDB2_PREPARE_MANIP);
            $res = $del_sth->execute(array($year));
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
            }
        }
        // 登録済みに追加
        // 既存データから重複チェックテーブル($dup)を作成
        else {
            $sel_sth = $this->db->prepare('SELECT * FROM ldr_facilitator WHERE year = ?', array('integer'), MDB2_PREPARE_RESULT);
            $res = $sel_sth->execute(array($year));
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
            }
            while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                $dup[$row['emp_id']][$row['facilitate_emp_id']] = 1;
            }
        }

        while ($row = fgetcsv($fp)) {
            if (empty($row[0])) {
                continue;
            }

            $line++;

            list($emp_personal_id, $emp_name) = array_splice($row, 0, 2);
            $emp_id = $emp->from_personal_id($emp_personal_id);
            $is_novice = $this->is_novice($emp_id, $year);

            if ($emp_id && $is_novice) {
                while ($row) {
                    list($fcl_personal_id, $fcl_name) = array_splice($row, 0, 2);

                    $fcl_emp_id = $emp->from_personal_id($fcl_personal_id);
                    $is_fcl_novice = $this->is_novice($fcl_emp_id, $year);

                    if ($fcl_emp_id && !$is_fcl_novice) {
                        if (!$dup[$emp_id][$fcl_emp_id]) {
                            $insert[] = array('emp_id' => $emp_id, 'facilitate_emp_id' => $fcl_emp_id, 'year' => $year);
                            $dup[$emp_id][$fcl_emp_id] = 1;
                            $cnt ++;
                        }
                    }
                    else {
                        if (!$fcl_emp_id) {
                            $none .= "{$emp_name} の教育担当者: {$fcl_personal_id}({$fcl_name}) に該当する職員なし<br>";
                        }
                        else {
                            $none .= "{$emp_name} の教育担当者: {$fcl_personal_id}({$fcl_name}) は新人職員として登録済み<br>";
                        }
                    }
                }
            }
            else {
                $none .= "{$emp_personal_id}({$emp_name}) は新人職員として登録されていません<br>";
            }
        }

        if (!empty($insert)) {
            $ins_sth = $this->db->prepare("INSERT INTO ldr_facilitator (emp_id, facilitate_emp_id, year) VALUES (:emp_id, :facilitate_emp_id, :year)", array('integer', 'integer', 'integer'), MDB2_PREPARE_MANIP);
            $ins_res = $this->db->extended->executeMultiple($ins_sth, $insert);
            if (PEAR::isError($ins_res)) {
                $this->db->failNestedTransaction();
                throw new Exception('ldr_facilitator INSERT Multiple ERROR' . $ins_res->getDebugInfo());
            }
            $ins_sth->free();
        }

        $this->db->completeNestedTransaction();

        // Log
        $this->log->log("{$cnt}件の教育担当者データをインポートしました");

        return $cnt . "件のデータをインポートしました<br>$none";
    }

    /**
     * 新人職員:1件インポート
     * @param Array $data
     */
    private function import($emp_id, $year)
    {
        $this->db->beginNestedTransaction();

        // INSERT
        $sth = $this->db->prepare("INSERT INTO ldr_novice (emp_id, year) VALUES(:emp_id, :year)", array('text', 'integer'), MDB2_PREPARE_MANIP);
        $res = $sth->execute(array('emp_id' => $emp_id, 'year' => $year));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }

        $this->db->completeNestedTransaction();
    }

    /**
     * 新人職員判定
     * @return boolean|\Ldr_Employee
     */
    public function is_novice($emp_id, $year)
    {
        $row = $this->db->extended->getRow('SELECT COUNT(*) AS count FROM ldr_novice WHERE emp_id = ? AND year = ?', null, array($emp_id, $year), array('text', 'integer'), MDB2_FETCHMODE_ASSOC);
        if ($row['count'] > 0) {
            return true;
        }
        return false;
    }

}
