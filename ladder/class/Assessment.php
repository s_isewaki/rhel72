<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'Cmx/Model/SystemConfig.php';
require_once 'ladder/class/Util.php';
require_once 'ladder/class/Guideline.php';
require_once 'ladder/class/Config.php';
require_once 'ladder/class/Log.php';

class Ldr_Assessment extends Ldr_Guideline
{

    var $emp_id;
    var $log;

    /**
     * コンストラクタ
     */
    public function Ldr_Assessment($emp_id)
    {
        parent::connectDB();
        parent::Ldr_Guideline();
        $this->emp_id = $emp_id;
        $this->log = new Ldr_log('assessment');
    }

    /**
     * 分類一覧の取得
     * @param type $data
     * @return Array
     */
    public function group2_lists($data)
    {
        $sql = "
            SELECT
                guideline_group1,
                guideline_group2,
                g2.name,
                g2.comment
            FROM ldr_guideline_group2 g2
            WHERE NOT g2.del_flg
              AND guideline_group1= :group1
              AND EXISTS (SELECT * FROM ldr_guideline g WHERE g.guideline_group2=g2.guideline_group2 AND level = :level AND guideline_revision = :revision)
            ORDER BY g2.order_no, guideline_group2
            ";
        $sth = $this->db->prepare($sql, array('integer', 'integer', 'integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['comment'] = unserialize($row['comment']);
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * すべての分類一覧の取得
     * @param type $data
     * @return Array
     */
    public function group2_full_lists()
    {
        $data['revision'] = $this->current_revision();
        $sql = "
            SELECT
                guideline_group1,
                guideline_group2,
                g2.name,
                g2.comment
            FROM ldr_guideline_group2 g2
            WHERE NOT g2.del_flg
              AND EXISTS (SELECT * FROM ldr_guideline g WHERE g.guideline_group2=g2.guideline_group2 AND guideline_revision = :revision)
            ORDER BY g2.order_no, guideline_group2
            ";
        $sth = $this->db->prepare($sql, array('integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['comment'] = unserialize($row['comment']);
            $lists[$row['guideline_group1']][] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 自己評価データの取得
     * @param type $data
     * @return type
     */
    public function assessment($data)
    {
        $sql = "
            SELECT
                g.guideline_id,
                g.guideline_group2,
                g.guideline,
                assessment,
                criterion,
                reason
            FROM ldr_guideline g
            LEFT JOIN ldr_assessment a ON a.guideline_id=g.guideline_id AND a.emp_id= :emp_id AND a.year= :year
            WHERE NOT g.del_flg
              AND g.guideline_group1= :group1
              AND level= :level
              AND guideline_revision= :revision
            ORDER BY g.order_no, g.guideline_id
            ";
        $sth = $this->db->prepare($sql, array('text', 'integer', 'integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[$row['guideline_group2']][] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 評価データの取得
     * @param type $emp_id
     * @param type $year
     * @return type
     */
    public function full_assessment($data)
    {
        $sql = "
            SELECT
                g.guideline_id,
                g.guideline_group1,
                g.guideline_group2,
                g.level,
                g.guideline,
                assessment,
                criterion,
                reason,
                g2.name as group2_name
            FROM ldr_guideline g
            JOIN ldr_guideline_group1 g1 ON g.guideline_group1=g1.guideline_group1 AND NOT g1.del_flg
            JOIN ldr_guideline_group2 g2 ON g.guideline_group2=g2.guideline_group2 AND NOT g2.del_flg
            LEFT JOIN ldr_assessment a ON a.guideline_id=g.guideline_id AND a.emp_id= :emp_id AND a.year= :year
            WHERE NOT g.del_flg
              AND g.guideline_revision= :revision
            ORDER BY g1.order_no, g.level, g2.order_no, g.order_no, g.guideline_id
            ";
        $sth = $this->db->prepare($sql);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[$row['guideline_group1']][$row['level']][] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 自己評価データの保存
     * @param type $emp_id
     * @param type $data
     * @throws Exception
     */
    public function save($emp_id, $data, $mode)
    {
        $this->db->beginNestedTransaction();

        $sth = $this->db->prepare("SELECT update_ldr_assessment(:emp_id, :guideline_id, :year, :assessment, :reason)", array('text', 'integer', 'integer', 'integer', 'text'), MDB2_PREPARE_MANIP);

        // LOOP
        foreach ($data['assessment'] as $gid => $assessment) {
            // FIELD DATA
            $field = array(
                'emp_id' => $emp_id,
                'guideline_id' => $gid,
                'year' => $data['year'],
                'assessment' => $assessment,
                'reason' => ($mode !== 'auto' ? $data['reason'][$gid] : from_utf8($data['reason'][$gid])),
            );

            // UPDATE or INSERT
            $res = $sth->execute($field);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " guideline_id = {$gid} ERROR: " . $res->getDebugInfo());
            }
        }

        $sth->free();
        $this->db->completeNestedTransaction();

        // Log
        if ($mode !== 'auto') {
            $this->log->log('保存しました', "year={$data['year']} guideline_id=" . implode(",", array_keys($data['assessment'])));
        }
    }

    /**
     * チャートラベル
     * @return type
     */
    public function radar_label($level, $revision)
    {
        $sql = "
            SELECT
                g.guideline_group1,
                g.guideline_group2,
                g1.name AS g1_name,
                g2.name AS g2_name
            FROM (SELECT guideline_group1, guideline_group2 FROM ldr_guideline WHERE NOT del_flg AND guideline_revision = :revision AND level = :level GROUP BY guideline_group1, guideline_group2) g
            JOIN ldr_guideline_group1 g1 USING (guideline_group1)
            JOIN ldr_guideline_group2 g2 USING (guideline_group2)
            WHERE NOT g2.del_flg
              AND NOT g1.del_flg
            ORDER BY g1.order_no, g2.order_no, g.guideline_group1, g.guideline_group2
        ";
        $sth = $this->db->prepare($sql, array('integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array(
            'revision' => $revision,
            'level' => $level,
        ));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * ラダーチャートデータ
     * @param type $year
     * @param type $level
     * @return type
     */
    public function radar($year, $level, $revision)
    {
        $sql = "
            SELECT
                g.guideline_group1,
                g.guideline_group2,
                COUNT(g.guideline_id) as total,
                SUM(a.assessment) as point,
                SUM(a2.assessment) as point2,
                SUM(a3.assessment) as point3
            FROM ldr_guideline g
            JOIN ldr_guideline_group1 g1 USING (guideline_group1)
            JOIN ldr_guideline_group2 g2 USING (guideline_group2)
            LEFT JOIN ldr_assessment a USING (guideline_id)
            LEFT JOIN ldr_assessment a2 ON a2.guideline_id=g.guideline_id AND a2.emp_id=a.emp_id AND a2.year=a.year-1
            LEFT JOIN ldr_assessment a3 ON a3.guideline_id=g.guideline_id AND a3.emp_id=a.emp_id AND a3.year=a.year-2
            WHERE NOT g.del_flg
              AND NOT g2.del_flg
              AND NOT g1.del_flg
              AND g.guideline_revision = :revision
              AND g.level = :level
              AND a.emp_id = :emp_id
              AND a.year = :year
            GROUP BY g.guideline_group1, g.guideline_group2, g1.order_no, g2.order_no
        ";
        $sth = $this->db->prepare($sql, array('text', 'integer', 'integer', 'integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array(
            'emp_id' => $this->emp_id,
            'year' => $year,
            'revision' => $revision,
            'level' => $level,
        ));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $radar = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
//            $point = $row['point'] / ($row['total'] * $max) * 100;
            $radar[$row['guideline_group2']] = $row;
        }
        $sth->free();
        return $radar;
    }

    /**
     * 集計
     * @param type $year
     * @param type $level
     * @return type
     * @throws Exception
     */
    public function table($year, $revision)
    {
        $sql = "
            SELECT
                guideline_group2,
                level,
                SUM(assessment) AS assessment,
                COUNT(guideline_id) AS count
            FROM ldr_guideline g
            LEFT JOIN ldr_assessment a USING (guideline_id)
            WHERE
                NOT g.del_flg
                AND g.guideline_revision = :revision
                AND a.emp_id = :emp_id
                AND a.year = :year
			GROUP BY guideline_group2, level
        ";
        $sth = $this->db->prepare($sql, null, MDB2_PREPARE_RESULT);
        $res = $sth->execute(array(
            'emp_id' => $this->emp_id,
            'year' => $year,
            'revision' => $revision,
        ));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['percent'] = sprintf("%.1f", $row['assessment'] / $row['count'] * 100);
            $lists[$row['guideline_group2']][$row['level']] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 集計表ラベル
     * @return type
     */
    public function table_label($revision)
    {
        $sql = "
            SELECT
                guideline_group1,
                guideline_group2,
                g1.name AS g1_name,
                g2.name AS g2_name,
                (SELECT COUNT(*) FROM ldr_guideline_group2 WHERE guideline_group1=g2.guideline_group1) AS rowspan,
                (SELECT COUNT(*) FROM ldr_guideline WHERE guideline_group2=g2.guideline_group2 AND level=0) AS lv0,
                (SELECT COUNT(*) FROM ldr_guideline WHERE guideline_group2=g2.guideline_group2 AND level=1) AS lv1,
                (SELECT COUNT(*) FROM ldr_guideline WHERE guideline_group2=g2.guideline_group2 AND level=2) AS lv2,
                (SELECT COUNT(*) FROM ldr_guideline WHERE guideline_group2=g2.guideline_group2 AND level=3) AS lv3,
                (SELECT COUNT(*) FROM ldr_guideline WHERE guideline_group2=g2.guideline_group2 AND level=4) AS lv4,
                (SELECT COUNT(*) FROM ldr_guideline WHERE guideline_group2=g2.guideline_group2 AND level=5) AS lv5,
                (SELECT COUNT(*) FROM ldr_guideline WHERE guideline_group2=g2.guideline_group2 AND level=6) AS lv6
            FROM ldr_guideline_group2 g2
            JOIN ldr_guideline_group1 g1 USING (guideline_group1)
            WHERE NOT g2.del_flg
              AND NOT g1.del_flg
              AND g2.guideline_revision = :revision
            ORDER BY g1.order_no, g2.order_no, guideline_group1, guideline_group2
        ";
        $sth = $this->db->prepare($sql, array('integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array('revision' => $revision));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 解析用データ（職員）
     * @param type $year
     * @return type
     */
    public function analysis_emp($year)
    {
        $revision = $this->current_revision();
        $sql = "
            SELECT
                SUM(CASE WHEN level=1 THEN 1 ELSE 0 END) AS lv1_count,
                SUM(CASE WHEN level=1 AND a.assessment=1 THEN 1 ELSE 0 END) AS lv1_sum,
                SUM(CASE WHEN level=2 THEN 1 ELSE 0 END) AS lv2_count,
                SUM(CASE WHEN level=2 AND a.assessment=1 THEN 1 ELSE 0 END) AS lv2_sum,
                SUM(CASE WHEN level=3 THEN 1 ELSE 0 END) AS lv3_count,
                SUM(CASE WHEN level=3 AND a.assessment=1 THEN 1 ELSE 0 END) AS lv3_sum,
                SUM(CASE WHEN level=4 THEN 1 ELSE 0 END) AS lv4_count,
                SUM(CASE WHEN level=4 AND a.assessment=1 THEN 1 ELSE 0 END) AS lv4_sum,
                SUM(CASE WHEN level=5 THEN 1 ELSE 0 END) AS lv5_count,
                SUM(CASE WHEN level=5 AND a.assessment=1 THEN 1 ELSE 0 END) AS lv5_sum
            FROM ldr_guideline g
            LEFT JOIN ldr_assessment a ON a.guideline_id=g.guideline_id AND a.emp_id= :emp_id AND a.year= :year AND NOT a.del_flg
            WHERE guideline_revision= :revision
            AND NOT g.del_flg
        ";
        $sth = $this->db->prepare($sql, array('text', 'integer', 'integer', 'integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array(
            'emp_id' => $this->emp_id,
            'year' => $year,
            'revision' => $revision,
        ));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists['lv1'] = ($row['lv1_sum'] === '0') ? '' : round($row['lv1_sum'] / $row['lv1_count'] * 100);
            $lists['lv2'] = ($row['lv2_sum'] === '0') ? '' : round($row['lv2_sum'] / $row['lv2_count'] * 100);
            $lists['lv3'] = ($row['lv3_sum'] === '0') ? '' : round($row['lv3_sum'] / $row['lv3_count'] * 100);
            $lists['lv4'] = ($row['lv4_sum'] === '0') ? '' : round($row['lv4_sum'] / $row['lv4_count'] * 100);
            $lists['lv5'] = ($row['lv5_sum'] === '0') ? '' : round($row['lv5_sum'] / $row['lv5_count'] * 100);
        }

        $sth->free();
        return $lists;
    }

    /**
     * 部門別評価済みリスト
     * @param type $year      年
     * @param type $class     部署情報(class_id/atrb_id/dept_id/room_id)
     * @return type
     */
    public function analysis_class($year, $class)
    {
        $revision = $this->current_revision();
        $sql = "
            SELECT
                a.emp_id,
                g.level
            FROM empmst e
            JOIN ldr_assessment a ON a.year = :year AND a.emp_id = e.emp_id
            JOIN ldr_guideline g ON a.guideline_id = g.guideline_id AND g.guideline_revision = :revision AND NOT g.del_flg
            WHERE
                e.emp_class =:class_id AND e.emp_attribute = :atrb_id AND e.emp_dept= :dept_id AND (e.emp_room is null OR e.emp_room = :room_id)
                AND a.emp_id = e.emp_id AND NOT a.del_flg
            GROUP BY a.emp_id,g.level
            ";
        $sth = $this->db->prepare($sql, array('integer', 'integer', 'integer', 'integer', 'integer'), MDB2_PREPARE_RESULT);
        $ary = array_merge($class, array('year' => $year, 'revision' => $revision));
        $res = $sth->execute($ary);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $lists = array('lv1' => '', 'lv2' => '', 'lv3' => '', 'lv4' => '', 'lv5' => '');
        $ary_emp = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $key = "lv" . $row['level'];
            $lists[$key] ++;
            $ary_emp[] = $row['emp_id'];
        }
        $lists['emp_count'] = count(array_unique($ary_emp));
        $sth->free();
        return $lists;
    }

    /**
     * 存在する年のリストを取得する
     * @return type
     */
    public function year_lists()
    {
        $min_year = $this->db->queryOne('SELECT year FROM ldr_assessment WHERE NOT del_flg ORDER BY year LIMIT 1');
        if (!$min_year) {
            $min_year = Ldr_Util::th_year();
        }
        $th_year = Ldr_Util::th_year() + 1;

        if ($min_year > $th_year) {
            return array_reverse(range($th_year, $min_year));
        }
        else {
            return array_reverse(range($min_year, $th_year));
        }
    }

    /**
     * 自己評価有無チェック
     * @param type $emp_id
     * @return boolean
     */
    public function is_exist($emp_id)
    {
        $year = Ldr_Util::th_year();
        $count = $this->db->extended->getOne(
            'SELECT COUNT(*) FROM ldr_guideline LEFT JOIN ldr_assessment ON ldr_guideline.guideline_id = ldr_assessment.guideline_id AND ldr_assessment.emp_id = ? AND ldr_assessment.year = ? AND NOT ldr_assessment.del_flg WHERE ldr_assessment.assessment IS NULL', null, array($emp_id, $year), array('text', 'integer')
        );
        if ($count > 0) {
            return false;
        }

        return true;
    }

}
