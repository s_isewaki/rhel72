<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'ladder/class/Validation.php';
require_once 'ladder/class/Config.php';
require_once 'ladder/class/Guideline.php';
require_once 'ladder/class/Committee.php';
require_once 'ladder/class/Assessment.php';
require_once 'ladder/class/Inside.php';
require_once 'ladder/class/Outside.php';
require_once 'ladder/class/Relief.php';
require_once 'ladder/class/NoviceAssessment.php';
require_once 'ladder/class/Ladder.php';
require_once 'ladder/class/Objection.php';

class Ldr_Information extends Model
{

    var $emp;
    var $apply;

    /**
     * コンストラクタ
     */
    public function Ldr_Information($emp, $apply)
    {
        parent::connectDB();
        $this->emp = $emp;
        $this->apply = $apply;
        $this->conf = new Ldr_Config();
    }

    /**
     * お知らせ一覧
     */
    public function lists()
    {
        $list = array();
        $func = $this->conf->functions();

        // 管理者
        if ($this->emp->is_admin()) {
            // 管理全般:設定
            if (($item = $this->info_adm_config()) !== true) {
                $list[] = $item;
            }

            // 認定委員会
            if (($item = $this->info_committee()) !== true) {
                $list[] = $item;
            }
        }

        // ユーザ設定未入力
        if (!$this->emp->is_user_setting()) {
            $list[] = $this->info_user_setting();
        }

        // 承認者
        if (!$this->emp->approver()) {
            $list[] = array(
                'text' => sprintf("申請承認者が設定されていません。管理者に連絡してください"),
                'alert' => 'alert-danger',
            );
        }

        // 自己評価
        if ($func['assessment'] && ( $item = $this->info_assessment()) !== true) {
            $list[] = $item;
        }

        // 新人評価
        if ($func['novice'] && ( $item = $this->info_novice()) !== true) {
            array_splice($list, count($list), 0, $item);
        }
        if ($func['novice'] && ( $item = $this->info_facilitator()) !== true) {
            array_splice($list, count($list), 0, $item);
        }

        // 振り返り未入力（院内研修）
        if ($func['inside'] && ( $item = $this->info_trn_inside_uncheck())) {
            array_splice($list, count($list), 0, $item);
        }

        // 申請状態
        if ($func['ladder'] && ( $item = $this->info_apply())) {
            array_splice($list, count($list), 0, $item);
        }

        // 受付状態
        if ($func['ladder'] && ( $item = $this->info_accept())) {
            array_splice($list, count($list), 0, $item);
        }

        // 未読コメント数
        if ($func['ladder'] && ( $item = $this->info_comment_unread())) {
            array_splice($list, count($list), 0, $item);
        }

        // 受付状態（院内研修）
        if ($func['inside'] && ( $item = $this->info_trn_inside_accept())) {
            array_splice($list, count($list), 0, $item);
        }

        // 受付状態（院外研修）
        if ($func['outside'] && ( $item = $this->info_trn_outside_accept())) {
            array_splice($list, count($list), 0, $item);
        }

        // 受付状態（救護員研修）
        if ($func['relief'] && ( $item = $this->info_trn_relief_accept())) {
            array_splice($list, count($list), 0, $item);
        }

        // 認定委員会・教育課
        if ($this->emp->committee()) {

            // 異議申し立て
            if ($func['ladder'] && (($item = $this->info_objection()) !== true)) {
                $list[] = $item;
            }
        }

        return $list;
    }

    /**
     * 未入力のユーザ設定があります
     */
    private function info_user_setting()
    {
        $item = array();
        $req = $this->conf->required();

        //卒業年
        if ($req['graduation_year'] && Validation::is_empty($this->emp->graduation_year())) {
            $item[] = '卒業年';
        }

        //出身校
        if ($req['school'] && Validation::is_empty($this->emp->school())) {
            $item[] = '出身校';
        }

        //看護師免許取得日
        if ($req['license_date'] && Validation::is_empty($this->emp->license_date())) {
            $item[] = '看護師免許取得日';
        }

        //臨床開始日
        if ($req['nurse_date'] && Validation::is_empty($this->emp->nurse_date()) ||
            $req['hospital_date'] && Validation::is_empty($this->emp->hospital_date()) ||
            $req['ward_date'] && Validation::is_empty($this->emp->ward_date())) {
            $item[] = '臨床開始日';
        }

        //日本看護協会 会員番号
        if ($req['jna_id'] && Validation::is_empty($this->emp->jna_id()) ||
            $req['pref_na_id'] && Validation::is_empty($this->emp->pref_na_id())) {
            $item[] = '看護協会会員番号';
        }

        return array(
            'text' => sprintf("未入力のユーザ設定があります(%s)", implode(', ', $item)),
            'href' => 'config_user.php',
            'alert' => 'alert-danger',
        );
    }

    /**
     * 未入力の管理設定があります
     * @return boolean
     */
    private function info_adm_config()
    {
        $item = array();

        //病院名
        if (Validation::is_empty($this->conf->hospital_name())) {
            $item[] = '病院名';
        }

        //都道府県看護協会名
        if (Validation::is_empty($this->conf->na_name())) {
            $item[] = '都道府県看護協会名';
        }

        //日本赤十字社支部名
        if (Validation::is_empty($this->conf->jrc_branch_name())) {
            $item[] = '日本赤十字社支部名';
        }

        //名称設定
        $title = $this->conf->titles();
        if (empty($title)) {
            $item[] = '名称設定';
        }

        //利用機能
        $func = $this->conf->functions();
        if (empty($func)) {
            $item[] = '利用機能';
        }

        if (empty($item)) {
            return true;
        }

        return array(
            'text' => sprintf("未入力の管理設定があります(%s)", implode(', ', $item)),
            'href' => 'adm_config.php',
            'alert' => 'alert-danger',
        );
    }

    /**
     * 認定委員会が設定されていません
     * @return boolean
     */
    private function info_committee()
    {
        $comm = new Ldr_Committee();

        if (!$comm->lists()) {
            return array(
                'text' => sprintf("認定委員会が設定されていません"),
                'href' => 'adm_permission.php',
                'alert' => 'alert-danger',
            );
        }

        return true;
    }

    /**
     * 自己評価
     * @return boolean
     */
    private function info_assessment()
    {
        $ass = new Ldr_Assessment();
        if (!$ass->is_exist($this->emp->emp_id())) {
            return array(
                'text' => sprintf("未入力の自己評価があります"),
                'href' => 'assessment.php',
                'alert' => '',
            );
        }

        return true;
    }

    /**
     * 新人評価：本人評価
     * @return boolean
     */
    private function info_novice()
    {
        $ass = new Ldr_NoviceAssessment($this->emp->emp_id());
        $lists = $ass->is_novice_exists();

        $item = array();
        foreach ($lists as $row) {
            $item[] = array(
                'text' => sprintf("%d年度 第%d回の新人評価が未入力です (あと%d件)", $row['year'], $row['number'], $row['count']),
                'href' => sprintf('novice_assessment.php?year=%d&number=%d', $row['year'], $row['number']),
                'alert' => '',
            );
        }

        return $item;
    }

    /**
     * 新人評価：他者評価
     * @return boolean
     */
    private function info_facilitator()
    {
        $ass = new Ldr_NoviceAssessment($this->emp->emp_id());
        $lists = $ass->is_facilitate_exists();

        $item = array();
        foreach ($lists as $row) {
            $item[] = array(
                'text' => sprintf("%d年度 第%d回の%sの他者評価が未入力です (あと%d件)", $row['year'], $row['number'], $row['emp_name'], $row['count']),
                'href' => sprintf('novice_facilitate_assessment.php?year=%d&number=%d&emp_id=%s', $row['year'], $row['number'], $row['emp_id']),
                'alert' => '',
            );
        }

        return $item;
    }

    /**
     * 未読コメント数
     * @return boolean
     */
    private function info_comment_unread()
    {
        $comm = new Ldr_Comment($this->emp->emp_id());
        list($apply, $accept) = $comm->count_unread();

        $item = array();
        if ($apply > 0) {
            $item[] = array(
                'text' => sprintf("未読の申請コメントが %d件 あります", $apply),
                'href' => 'apply.php',
                'alert' => '',
            );
        }
        if ($accept > 0) {
            $item[] = array(
                'text' => sprintf("未読の受付コメントが %d件 あります", $accept),
                'href' => 'accept.php?comment=1',
                'alert' => '',
            );
        }

        return $item;
    }

    /**
     * ラダー申請状態
     * @return type
     */
    private function info_apply()
    {
        $lists = $this->apply->lists_apply_myself();
        $title = $this->conf->titles();

        $item = array();

        foreach ($lists as $row) {
            switch ($row['status']) {
                case '6':
                    $item[] = array(
                        'text' => sprintf("%s %s {$title['ladder']}申請が差し戻されました", $row['ladder']['title'], $row['level'] ? Ldr_Const::get_roman_num($row['level']) : $row['ladder']['config']['level0']),
                        'href' => 'apply.php',
                        'alert' => '',
                    );
                    break;

                case '4':
                    $item[] = array(
                        'text' => sprintf("申込前の %s %s {$title['ladder']}申請があります", $row['ladder']['title'], $row['level'] ? Ldr_Const::get_roman_num($row['level']) : $row['ladder']['config']['level0']),
                        'href' => 'apply.php',
                        'alert' => '',
                    );
                    break;
            }
        }

        return $item;
    }

    /**
     * ラダー受付状態
     * @return type
     */
    private function info_accept()
    {
        $count = $this->apply->count_accept_status();
        $title = $this->conf->titles();
        $ladder = new Ldr_Ladder();
        $status_lists = $ladder->status_lists();

        $item = array();
        if ($count['st5_count'] > 0) {
            $item[] = array(
                'text' => sprintf("未処理の{$title['ladder']}申込が %d件 あります", $count['st5_count']),
                'href' => 'accept.php?status=5',
                'alert' => '',
            );
        }

        if ($count['st8_count'] > 0) {
            $item[] = array(
                'text' => sprintf("未処理の{$title['ladder']}評価依頼が %d件 あります", $count['st8_count']),
                'href' => 'accept.php?status=8',
                'alert' => '',
            );
        }

        if ($count['st9_count'] > 0) {
            $item[] = array(
                'text' => sprintf("未処理の「{$status_lists[9]}」が %d件 あります", $count['st9_count']),
                'href' => 'accept.php?status=9',
                'alert' => '',
            );
        }

        if ($count['st10_count'] > 0) {
            $item[] = array(
                'text' => sprintf("未処理の「{$status_lists[10]}」が %d件 あります", $count['st10_count']),
                'href' => 'accept.php?status=10',
                'alert' => '',
            );
        }

        if ($count['st11_count'] > 0) {
            $item[] = array(
                'text' => sprintf("未処理の「{$status_lists[11]}」が %d件 あります", $count['st11_count']),
                'href' => 'accept.php?status=11',
                'alert' => '',
            );
        }

        if ($count['st12_count'] > 0) {
            $item[] = array(
                'text' => sprintf("未処理の「{$status_lists[12]}」が %d件 あります", $count['st12_count']),
                'href' => 'accept.php?status=12',
                'alert' => '',
            );
        }

        if ($count['st21_count'] > 0) {
            $item[] = array(
                'text' => sprintf("未処理の「{$status_lists[21]}」が %d件 あります", $count['st21_count']),
                'href' => 'accept.php?status=21',
                'alert' => '',
            );
        }

        if ($count['st22_count'] > 0) {
            $item[] = array(
                'text' => sprintf("未処理の「{$status_lists[22]}」が %d件 あります", $count['st22_count']),
                'href' => 'accept.php?status=22',
                'alert' => '',
            );
        }

        if ($count['st23_count'] > 0) {
            $item[] = array(
                'text' => sprintf("未処理の「{$status_lists[23]}」が %d件 あります", $count['st23_count']),
                'href' => 'accept.php?status=23',
                'alert' => '',
            );
        }

        return $item;
    }

    /**
     * 院内研修受付状態
     * @return type
     */
    private function info_trn_inside_accept()
    {
        $inside = new Ldr_Inside($this->emp->emp_id());
        $count = $inside->count_accept_status();
        $item = array();

        if ($this->emp->is_approver()) {
            if ($count['st1_count'] > 0) {
                $item[] = array(
                    'text' => sprintf('未処理の院内研修承認が %d件 あります', $count['st1_count']),
                    'href' => 'trn_approve_inside.php?status=1',
                    'alert' => '',
                );
            }
        }
        if ($this->emp->is_inside() || $this->emp->is_inside_planner()) {
            if ($count['st2_count'] > 0) {
                $item[] = array(
                    'text' => sprintf('未処理の院内研修受付が %d件 あります', $count['st2_count']),
                    'href' => 'trn_accept_inside.php?status=2',
                    'alert' => '',
                );
            }
        }

        return $item;
    }

    /**
     * 院外研修受付状態
     * @return string
     */
    private function info_trn_outside_accept()
    {
        $outside = new Ldr_Outside($this->emp->emp_id());
        $count = $outside->count_accept_status();
        $item = array();

        if ($this->emp->is_approver()) {
            if ($count['st1_count'] > 0) {
                $item[] = array(
                    'text' => sprintf('未処理の院外研修承認が %d件 あります', $count['st1_count']),
                    'href' => 'trn_approve_outside.php?status=1',
                    'alert' => '',
                );
            }
        }
        if ($this->emp->is_outside()) {
            if ($count['st2_count'] > 0) {
                $item[] = array(
                    'text' => sprintf('未処理の院外研修受付が %d件 あります', $count['st2_count']),
                    'href' => 'trn_accept_outside.php?status=2',
                    'alert' => '',
                );
            }
        }

        return $item;
    }

    /**
     * 救護員研修受付状態
     * @return type
     */
    private function info_trn_relief_accept()
    {
        $relief = new Ldr_Relief($this->emp->emp_id());
        $count = $relief->count_accept_status();
        $item = array();

        if ($this->emp->is_approver()) {
            if ($count['st1_count'] > 0) {
                $item[] = array(
                    'text' => sprintf('未処理の救護員研修承認が %d件 あります', $count['st1_count']),
                    'href' => 'trn_approve_relief.php?status=1',
                    'alert' => '',
                );
            }
        }
        if ($this->emp->is_relief()) {
            if ($count['st2_count'] > 0) {
                $item[] = array(
                    'text' => sprintf('未処理の救護員研修受付が %d件 あります', $count['st2_count']),
                    'href' => 'trn_accept_relief.php?status=2',
                    'alert' => '',
                );
            }
        }

        return $item;
    }

    /**
     * 振り返り未入力
     * @return type
     */
    private function info_trn_inside_uncheck()
    {
        $inside = new Ldr_Inside($this->emp->emp_id());
        $count = $inside->count_apply(array(
            "srh_year" => Ldr_Util::th_year(),
            "srh_training" => 3,
        ));

        $item = array();

        if ($count > 0) {
            $item[] = array(
                'text' => sprintf('未入力の研修振り返りが %d件 あります', $count),
                'href' => 'trn_inside.php?training=3',
                'alert' => 'alert-danger',
            );
        }

        return $item;
    }

    /**
     * 異議申し立て
     * @return boolean
     */
    private function info_objection()
    {
        $obj = new Ldr_Objection();
        $count = $obj->count();

        if ($count > 0) {
            return array(
                'text' => sprintf('未処理の異議申し立てが %d件 あります', $count),
                'href' => 'accept_objection.php?unread=1',
                'alert' => '',
            );
        }

        return true;
    }

}
