<?php

require_once 'Cmx.php';
require_once 'Cmx/Model/SystemConfig.php';
require_once 'ladder/class/Log.php';

class Ldr_Config
{

    var $conf;
    var $log;

    /**
     * コンストラクタ
     */
    public function Ldr_Config()
    {
        $this->conf = new Cmx_SystemConfig();
        $this->log = new Ldr_log('config');
    }

    /**
     * 基本設定:保存
     * @param type $data
     */
    public function save_basic($data)
    {
        $this->hospital_name($data['hospital_name']);
        $this->na_name($data['na_name']);
        $this->jrc_branch_name($data['jrc_branch_name']);
        $this->view_class($data['view_class']);
        $this->hospital_type($data['hospital_type']);
        $this->log->log("基本設定を保存しました");
    }

    /**
     * 利用機能:保存
     * @param type $data
     */
    public function save_functions($data)
    {
        $this->functions($data['function']);
        $this->log->log("利用機能を保存しました");
    }

    /**
     * 名称変更機能:保存
     * @param type $data
     */
    public function save_titles($data)
    {
        $this->titles($data['titles']);
        $this->log->log("名称設定を保存しました");
    }

    /**
     * ソート
     * @param type $type
     * @param type $sort
     * @return type
     */
    public function save_sort($data)
    {
        $this->sortset($data['order']);
        $this->log->log("表示順を保存しました");
    }

    /**
     * 必須項目
     * @param type $type
     * @param type $sort
     * @return type
     */
    public function save_required($data)
    {
        $this->required($data['required']);
        $this->log->log("必須項目を保存しました");
    }

    /**
     * 研修設定 保存
     * @param type $data
     */
    public function save_training($data)
    {
        $this->ojt_name($data['ojt_name']);
        $this->log->log("研修設定を保存しました");
    }

    /**
     * 新人評価設定 保存
     * @param type $data
     */
    public function save_novice($data)
    {
        $this->novice_number($data['novice_number']);
        $this->log->log("新人評価設定を保存しました");
    }

    /**
     * 名称マスタ設定:保存
     * @param type $data
     */
    public function save_items($data)
    {
        $this->items($data['item']);
        $this->log->log("名称マスタ設定を保存しました");
    }

    /**
     * 病院選択
     * @param type $hospital_type
     * @return type
     */
    function hospital_type($hospital_type = null)
    {
        if (!empty($hospital_type)) {
            $this->conf->set('ldr.hospital_type', $hospital_type);
            return $hospital_type;
        }

        $get_hospital_type = $this->conf->get('ldr.hospital_type');
        if (empty($get_hospital_type)) {
            return '1';
        }
        return $get_hospital_type;
    }

    /**
     * 病院名
     * @param type $hospital_name
     * @return type
     */
    function hospital_name($hospital_name = null)
    {
        if (!empty($hospital_name)) {
            $this->conf->set('ldr.hospital_name', $hospital_name);
            return $hospital_name;
        }

        return $this->conf->get('ldr.hospital_name');
    }

    /**
     * 看護協会名
     * @param type $na_name
     * @return type
     */
    function na_name($na_name = null)
    {
        if (!empty($na_name)) {
            $this->conf->set('ldr.na_name', $na_name);
            return $na_name;
        }

        return $this->conf->get('ldr.na_name');
    }

    /**
     * 日赤支部名
     * @param type $jrc_branch_name
     * @return type
     */
    function jrc_branch_name($jrc_branch_name = null)
    {
        if (!empty($jrc_branch_name)) {
            $this->conf->set('ldr.jrc_branch_name', $jrc_branch_name);
            return $jrc_branch_name;
        }

        return $this->conf->get('ldr.jrc_branch_name');
    }

    /**
     * 部署表示
     * @param type $view_class
     * @return type
     */
    function view_class($view_class = null)
    {
        if (!empty($view_class)) {
            $this->conf->set('ldr.view_class', $view_class);
            return $view_class;
        }

        return $this->conf->get('ldr.view_class');
    }

    /**
     * 個別研修の名称
     * @param type $ojt_name
     * @return type
     */
    function ojt_name($ojt_name = null)
    {
        if (!empty($ojt_name)) {
            $this->conf->set('ldr.ojt_name', $ojt_name);
            return $ojt_name;
        }

        $ojt_name = $this->conf->get('ldr.ojt_name');
        if (empty($ojt_name)) {
            return "個別研修";
        }
        return $ojt_name;
    }

    /**
     * 新人評価の数字
     * @param type $novice_number
     * @return type
     */
    function novice_number($novice_number = null)
    {
        if (!empty($novice_number)) {
            $this->conf->set('ldr.novice_number', $novice_number);
            return $novice_number;
        }

        $novice_number = $this->conf->get('ldr.novice_number');
        if (empty($novice_number)) {
            return 1;
        }
        return $novice_number;
    }

    /**
     * 利用機能
     * @param type $function
     * @return type
     */
    function functions($function = null)
    {
        if (!empty($function)) {
            $data = array();
            foreach ($function as $f) {
                $data[$f] = 1;
            }
            $this->conf->set('ldr.function', serialize($data));
            return $function;
        }

        $functions = unserialize($this->conf->get('ldr.function'));

        // form 2
        if ($functions['form_21'] || $functions['form_22'] || $functions['form_23']) {
            $functions['form_2'] = true;
        }
        // form 5
        if ($functions['form_51'] || $functions['form_52'] || $functions['form_53'] || $functions['form_54']) {
            $functions['form_5'] = true;
        }
        // record view
        if ($functions['form_21'] || $functions['form_22'] || $functions['form_23'] ||
            $functions['form_3'] || $functions['form_4'] ||
            $functions['form_51'] || $functions['form_52'] || $functions['form_53'] || $functions['form_54'] ||
            $functions['form_carrier'] || $functions['form_transfer'] || $functions['form_presentaion'] || $functions['form_article']
        ) {
            $functions['record_view'] = true;
        }
        return $functions;
    }

    /**
     * 名称設定機能
     * @param type $titles
     * @return type
     */
    function titles($titles = null)
    {
        if (!empty($titles)) {
            $this->conf->set('ldr.title', serialize($titles));
            return $titles;
        }
        $titles = unserialize($this->conf->get('ldr.title'));
        return $titles;
    }

    /**
     * 並び替え設定機能
     * @param type $sortset
     * @return type
     */
    function sortset($sortset = null)
    {
        if (!empty($sortset)) {
            $this->conf->set('ldr.sortset', serialize($sortset));
            return $sortset;
        }

        $get_sortset = $this->conf->get('ldr.sortset');
        if (!$get_sortset) {
            return Ldr_Const::get_default_sortset();
        }
        return unserialize($get_sortset);
    }

    /**
     * 記録の情報
     */
    public function recordset()
    {
        $functions = $this->functions();
        $titles = $this->titles();

        $data = array();
        foreach ($this->sortset() as $key) {
            if (!$functions[$key]) {
                continue;
            }
            $data[$key] = array(
                'php' => "{$key}.php",
            );
            $data[$key] = array_merge($data[$key], $titles[$key]);
        }
        return $data;
    }

    /**
     * 名称マスタ設定機能
     * @param type $data
     * @return type
     */
    public function items($data = null)
    {
        if (!empty($data)) {
            $this->conf->set('ldr.item', serialize($data));
            return $data;
        }
        $data = unserialize($this->conf->get('ldr.item'));
        return $data;
    }

    /**
     * 必須項目設定機能
     * @param type $required
     * @return type
     */
    function required($required = null)
    {
        if (!empty($required)) {
            $this->conf->set('ldr.required', serialize($required));
            return $required;
        }

        $get_required = $this->conf->get('ldr.required');
        return unserialize($get_required);
    }

    /**
     * 配置表権限
     * @param type $set
     * @return type
     */
    function analysis_placement($set = null)
    {
        if (!empty($set)) {
            $this->conf->set('ldr.analysis_placement', $set);
            return $set;
        }

        $get = $this->conf->get('ldr.analysis_placement');
        return $get ? $get : '1';
    }

}
