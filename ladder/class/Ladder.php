<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'ladder/class/Log.php';
require_once 'ladder/class/Config.php';

/**
 * ラダー設定
 */
class Ldr_Ladder extends Model
{

    private $log;
    private $lists;
    private $cache;

    /**
     * コンストラクタ
     */
    public function __construct()
    {
        parent::connectDB();
        $this->log = new Ldr_log('ladder');
    }

    /**
     * validate
     * @param type $data
     * @return string
     */
    public function validate($data)
    {
        $error = array();
        return $error;
    }

    /**
     * 保存
     * @param array $data
     * @throws Exception
     */
    public function save($data)
    {
        $this->db->beginNestedTransaction();

        // 選択肢
        $select = array();
        foreach ($data['config']['select']['button'] as $i => $button) {
            $select['_' . $data['config']['select']['value'][$i]] = array(
                'button' => $button,
                'value' => $data['config']['select']['value'][$i],
                'color' => $data['config']['select']['color'][$i],
            );
        }
        $data['config']['select'] = $select;

        // レポート選択肢
        $report_select = array();
        foreach ($data['config']['report_select']['button'] as $i => $button) {
            $report_select['_' . $data['config']['report_select']['value'][$i]] = array(
                'button' => $button,
                'value' => $data['config']['report_select']['value'][$i],
                'color' => $data['config']['report_select']['color'][$i],
            );
        }
        $data['config']['report_select'] = $report_select;

        $data['config'] = serialize($data['config']);

        // UPDATE or INSERT
        $sth = $this->db->prepare("SELECT update_ldr_ladder (:ladder_id, :title, :short, :guideline_revision, :config)", array('integer', 'text', 'text', 'integer', 'text'), MDB2_PREPARE_MANIP);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception('?update_ldr_ladder UPDATE or INSERT ERROR ' . $res->getDebugInfo());
        }
        $sth->free();
        $this->db->completeNestedTransaction();
        $this->log->log("保存しました", "ladder_id={$data['ladder_id']}");
    }

    /**
     * 一覧
     * @return type
     */
    public function lists()
    {
        if (!empty($this->lists)) {
            return $this->lists;
        }

        $sql = "
            SELECT
                ladder_id,
                title,
                short,
                guideline_revision,
                gr.name AS revision_name,
                config,
                ldr.updated_on
            FROM ldr_ladder ldr
            JOIN ldr_guideline_revision gr USING (guideline_revision)
            WHERE
                NOT ldr.del_flg
            ORDER BY ldr.order_no, ladder_id
        ";

        $res = $this->db->query($sql);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $this->lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['config'] = unserialize($row['config']);
            $this->lists[] = $row;
        }

        return $this->lists;
    }

    /**
     * ID検査
     * @param type $id
     * @return type
     * @throws Exception
     */
    public function is_ladder_id($id)
    {
        if (is_null($id)) {
            return false;
        }
        $count = $this->db->extended->getOne(
            'SELECT COUNT(*) FROM ldr_ladder WHERE NOT del_flg AND ladder_id= :ladder_id', null, array($id), array('integer')
        );
        if ($count > 0) {
            return true;
        }
        return false;
    }

    /**
     * 並べ替え
     * @param type $data
     * @throws Exception
     */
    public function sort($data)
    {
        $this->db->beginNestedTransaction();
        $sth = $this->db->prepare("UPDATE ldr_ladder SET order_no= ? WHERE ladder_id= ?", array('integer', 'integer'), MDB2_PREPARE_MANIP);
        $cnt = 1;
        foreach ($data['order'] as $id) {
            $res = $sth->execute(array($cnt++, $id));
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " id={$id} ERROR: " . $res->getDebugInfo());
            }
        }
        $sth->free();
        $this->db->completeNestedTransaction();

        // Log
        $this->log->log("並べ替えました", implode(" ", $data['order']));
    }

    /**
     * 取得
     * @param type $id
     * @return type
     * @throws Exception
     */
    public function find($id)
    {
        if (is_null($id)) {
            return array();
        }

        if (!empty($this->cache[$id])) {
            return $this->cache[$id];
        }

        $sql = "
            SELECT
                ladder_id,
                title,
                short,
                guideline_revision,
                config,
                updated_on
            FROM ldr_ladder
            WHERE
                NOT del_flg
                AND ladder_id= :ladder_id
        ";
        $row = $this->db->extended->getRow(
            $sql, null, array('ladder_id' => $id), array('integer'), MDB2_FETCHMODE_ASSOC
        );
        if (PEAR::isError($row)) {
            throw new Exception('ldr_ladder SELECT ERROR: ' . $row->getDebugInfo());
        }

        $row['config'] = unserialize($row['config']);
        $row['config']['status'][-2] = $row['config']['status']['m2'];
        $row['cover'] = $this->cover_lists($row['ladder_id']);
        $row['approver'] = $this->approver_lists($row['ladder_id']);
        $row['approver2'] = $this->approver2_lists($row['ladder_id']);

        $this->cache[$id] = $row;
        return $row;
    }

    /**
     * 対象validate
     * @param type $data
     * @return string
     */
    public function cover_validate($data)
    {
        $error = array();
        return $error;
    }

    /**
     * 対象保存
     * @param array $data
     * @throws Exception
     */
    public function cover_save($data)
    {
        $this->db->beginNestedTransaction();

        // DELETE
        $sth = $this->db->prepare("DELETE FROM ldr_ladder_cover WHERE ladder_id = :ladder_id", array('integer'), MDB2_PREPARE_MANIP);
        $res = $sth->execute($data);

        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception('ldr_ladder_cover DELETE ERROR: ' . $res->getDebugInfo());
        }

        // INSERT
        foreach ($data['class12'] as $i => $class12) {
            $row = array($data['ladder_id']);
            list($row[1], $row[2]) = explode('_', $class12);
            list($row[3], $row[4]) = explode('_', $data['class34'][$i]);
            $row[] = $data['job'][$i];
            $row[] = $data['status'][$i];

            $sth = $this->db->prepare("INSERT INTO ldr_ladder_cover (ladder_id, class_id, atrb_id, dept_id, room_id, job_id, st_id) VALUES (?, ?, ?, ?, ?, ?, ?) ", array('integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($row);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception('ldr_ladder_cover INSERT ERROR: ' . $res->getDebugInfo());
            }
        }
        $this->db->completeNestedTransaction();
        $this->log->log("対象を保存しました", "ladder_id={$data['ladder_id']}");
    }

    /**
     * 対象一覧
     * @return type
     */
    private function cover_lists($id)
    {
        $sql = "
            SELECT
                *
            FROM ldr_ladder_cover
            WHERE
                ladder_id = ?
            ORDER BY ladder_id
       ";

        $sth = $this->db->prepare($sql);
        $res = $sth->execute(array($id));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row;
        }
        return $lists;
    }

    /**
     * 対象職員一覧
     * @return type
     */
    public function covered_lists($id)
    {
        $conf = new Ldr_Config();
        $class_sql = Ldr_Util::class_full_name_sql($conf->view_class());

        $sql = "
            SELECT
                apr.*,
                $class_sql,
                class_nm,
                atrb_nm,
                dept_nm,
                room_nm,
                job_nm,
                st_nm
            FROM (
                SELECT
                  emp_id
                  , emp_lt_nm
                  , emp_ft_nm
                  , emp_class AS class_id
                  , emp_attribute AS atrb_id
                  , emp_dept AS dept_id
                  , emp_room AS room_id
                  , emp_job AS job_id
                  , emp_st AS st_id
                FROM empmst
                JOIN authmst USING (emp_id)
                JOIN ldr_ladder_cover
                    ON class_id IS NULL
                    OR (
                      class_id = emp_class
                      AND atrb_id = emp_attribute
                      AND (
                        dept_id IS NULL
                        OR (
                          dept_id = emp_dept
                          AND (room_id IS NULL OR room_id = emp_room)
                        )
                      )
                      AND (job_id IS NULL OR job_id = emp_job)
                      AND (st_id IS NULL OR st_id = emp_st)
                    )
                WHERE ladder_id = ?
                  AND NOT emp_del_flg
                GROUP BY
                  emp_id
                  , emp_lt_nm
                  , emp_ft_nm
                  , emp_class
                  , emp_attribute
                  , emp_dept
                  , emp_room
                  , emp_job
                  , emp_st
            ) apr
            JOIN classmst c1 USING (class_id)
            JOIN atrbmst c2 USING (atrb_id)
            JOIN deptmst c3 USING (dept_id)
            LEFT JOIN classroom c4 USING (room_id)
            JOIN jobmst job USING (job_id)
            JOIN stmst st USING (st_id)
            ORDER BY c1.order_no, c2.order_no, c3.order_no, c4.order_no, job.order_no, st.order_no
        ";

        $sth = $this->db->prepare($sql);
        $res = $sth->execute(array($id));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists["{$row['class_id']}-{$row['atrb_id']}-{$row['dept_id']}-{$row['room_id']}"][] = $row;
        }

        return $lists;
    }

    /**
     * 対象職員と承認者がかぶってる一覧
     * @return type
     */
    public function covered_duplicate_lists($id)
    {
        $sql = "
            SELECT
                *
            FROM ldr_ladder_approver a
            JOIN (
                SELECT
                  emp_id
                  , emp_lt_nm
                  , emp_ft_nm
                  , emp_class AS class_id
                  , emp_attribute AS atrb_id
                  , emp_dept AS dept_id
                  , emp_room AS room_id
                  , emp_job AS job_id
                  , emp_st AS st_id
                FROM empmst
                JOIN authmst USING (emp_id)
                JOIN ldr_ladder_cover
                    ON class_id IS NULL
                    OR (
                      class_id = emp_class
                      AND atrb_id = emp_attribute
                      AND (
                        dept_id IS NULL
                        OR (
                          dept_id = emp_dept
                          AND (room_id IS NULL OR room_id = emp_room)
                        )
                      )
                      AND (job_id IS NULL OR job_id = emp_job)
                      AND (st_id IS NULL OR st_id = emp_st)
                    )
                WHERE ladder_id = :ladder_id
                  AND NOT emp_del_flg
                GROUP BY
                  emp_id
                  , emp_lt_nm
                  , emp_ft_nm
                  , emp_class
                  , emp_attribute
                  , emp_dept
                  , emp_room
                  , emp_job
                  , emp_st
            ) apr on a.emp_id=apr.emp_id AND a.class_id=apr.class_id AND a.atrb_id=apr.atrb_id AND a.dept_id=apr.dept_id AND (a.room_id=apr.room_id OR (a.room_id IS NULL AND apr.room_id IS NULL))
            WHERE a.ladder_id = :ladder_id
        ";

        $sth = $this->db->prepare($sql);
        $res = $sth->execute(array('ladder_id' => $id));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row;
        }

        return $lists;
    }

    /**
     * 承認者validate
     * @param type $data
     * @return string
     */
    public function approver_validate($data)
    {
        $error = array();
        return $error;
    }

    /**
     * 承認者保存
     * @param array $data
     * @throws Exception
     */
    public function approver_save($data)
    {
        $this->db->beginNestedTransaction();

        // appr DELETE
        $sth = $this->db->prepare("DELETE FROM ldr_ladder_approver WHERE ladder_id = :ladder_id", array('integer'), MDB2_PREPARE_MANIP);
        $res = $sth->execute($data);

        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception('ldr_ladder_approver DELETE ERROR: ' . $res->getDebugInfo());
        }

        // appr INSERT
        $rows = array();
        foreach ($data['appr'] as $id => $emp_ids) {
            foreach ($emp_ids as $emp_id) {
                $ids = explode('-', $id);
                $rows[] = array($data['ladder_id'], $ids[0], $ids[1], $ids[2], $ids[3], $emp_id);
            }
        }
        $sth = $this->db->prepare("INSERT INTO ldr_ladder_approver (ladder_id, class_id, atrb_id, dept_id, room_id, emp_id) VALUES (?, ?, ?, ?, ?, ?) ", array('integer', 'integer', 'integer', 'integer', 'integer', 'text'), MDB2_PREPARE_MANIP);
        $res = $this->db->extended->executeMultiple($sth, $rows);
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception('ldr_ladder_approver INSERT ERROR: ' . $res->getDebugInfo());
        }


        // appr2 DELETE
        $sth2 = $this->db->prepare("DELETE FROM ldr_ladder_approver2 WHERE ladder_id = :ladder_id", array('integer'), MDB2_PREPARE_MANIP);
        $res2 = $sth2->execute($data);

        if (PEAR::isError($res2)) {
            $this->db->failNestedTransaction();
            throw new Exception('ldr_ladder_approver2 DELETE ERROR: ' . $res2->getDebugInfo());
        }

        // appr2 INSERT
        $rows = array();
        foreach ($data['appr2'] as $id => $emp_ids) {
            foreach ($emp_ids as $emp_id) {
                $rows[] = array($data['ladder_id'], $id, $emp_id);
            }
        }
        $sth2 = $this->db->prepare("INSERT INTO ldr_ladder_approver2 (ladder_id, apply_emp_id, emp_id) VALUES (?, ?, ?) ", array('integer', 'text', 'text'), MDB2_PREPARE_MANIP);
        $res2 = $this->db->extended->executeMultiple($sth2, $rows);
        if (PEAR::isError($res2)) {
            $this->db->failNestedTransaction();
            throw new Exception('ldr_ladder_approver2 INSERT ERROR: ' . $res2->getDebugInfo());
        }

        $this->db->completeNestedTransaction();
        $this->log->log("承認者を保存しました", "ladder_id={$data['ladder_id']}");
    }

    /**
     * 承認者一覧
     * @return type
     */
    private function approver_lists($id)
    {
        $sql = "
            SELECT
                *
            FROM ldr_ladder_approver
            JOIN empmst USING (emp_id)
            WHERE
                ladder_id = ?
            ORDER BY ladder_id
       ";

        $sth = $this->db->prepare($sql);
        $res = $sth->execute(array($id));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists["{$row['class_id']}-{$row['atrb_id']}-{$row['dept_id']}-{$row['room_id']}"][] = $row;
            $lists[] = $row;
        }

        return $lists;
    }

    /**
     * 承認者2一覧
     * @return type
     */
    private function approver2_lists($id)
    {
        $sql = "
            SELECT
                *
            FROM ldr_ladder_approver2
            JOIN empmst USING (emp_id)
            WHERE
                ladder_id = ?
            ORDER BY ladder_id
       ";

        $sth = $this->db->prepare($sql);
        $res = $sth->execute(array($id));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[$row['apply_emp_id']][] = $row;
        }

        return $lists;
    }

    /**
     * 対象ラダーリスト
     * @param type $emp_id
     */
    public function emp_ladder_lists($emp_id)
    {
        $sql = "
            SELECT
                ladder_id
            FROM ldr_ladder
            JOIN (
                SELECT
                    ladder_id
                FROM empmst
                JOIN ldr_ladder_cover
                        ON class_id IS NULL
                        OR (
                          class_id = emp_class
                          AND atrb_id = emp_attribute
                          AND (
                            dept_id IS NULL
                            OR (
                              dept_id = emp_dept
                              AND (room_id IS NULL OR room_id = emp_room)
                            )
                          )
                          AND (job_id IS NULL OR job_id = emp_job)
                          AND (st_id IS NULL OR st_id = emp_st)
                        )
                WHERE
                    emp_id = ?
                GROUP BY ladder_id
            ) emp_ladder USING (ladder_id)
            ORDER BY order_no, ldr_ladder.ladder_id
       ";

        $sth = $this->db->prepare($sql);
        $res = $sth->execute(array($emp_id));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $this->find($row['ladder_id']);
        }

        return $lists;
    }

    /**
     *  承認者
     * @return \Ldr_Employee
     */
    public function approver($ladder_id, $emp_id)
    {
        // appr2があればそれが優先
        $sql = "
            SELECT
                emp_id
            FROM ldr_ladder_approver2
            WHERE ladder_id= ? AND apply_emp_id = ?
        ";
        $sth = $this->db->prepare($sql, array('integer', 'text'), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array($ladder_id, $emp_id));

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = new Ldr_Employee($row['emp_id']);
        }

        if ($lists) {
            return $lists;
        }

        // appr
        $sql = "
            SELECT
                a.emp_id
            FROM ldr_ladder_approver a
            JOIN empmst ON class_id=emp_class AND atrb_id=emp_attribute AND dept_id=emp_dept AND (room_id=emp_room OR (room_id IS NULL AND emp_room IS NULL))
            WHERE a.ladder_id= ? AND empmst.emp_id = ?
        ";
        $sth = $this->db->prepare($sql, array('integer', 'text'), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array($ladder_id, $emp_id));

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = new Ldr_Employee($row['emp_id']);
        }

        return $lists;
    }

    /**
     * すべてのラダーステータスの一覧
     * ラダー受付画面のプルダウンに使用
     * @return type
     */
    public function status_lists()
    {
        if (!empty($this->status_lists)) {
            return $this->status_lists;
        }

        if (empty($this->lists)) {
            $this->lists();
        }

        $text = array();
        foreach ($this->lists as $row) {
            foreach (array(4, 5, 6, 8, 9, 10, 11, 12, 0, 21, 22, 23) as $num) {
                if ($row['config']['status'][$num]) {
                    $text[$num][] = $row['config']['status'][$num];
                }
            }
            if ($row['config']['status']['m2']) {
                $text['-2'][] = $row['config']['status']['m2'];
            }
        }

        $status = array();
        foreach (array(4, 5, 6, 8, 9, 10, 11, 12, 0, '-2', 21, 22, 23) as $num) {
            $status_text = implode(" / ", array_unique($text[$num]));
            if ($status_text) {
                $status[$num] = $status_text;
            }
        }

        $this->status_lists = $status;
        return $status;
    }

    /**
     * すべてのラダーレベル0の名前
     * ラダー受付画面のプルダウンに使用
     * @return type
     */
    public function level0_texts()
    {
        if (!empty($this->level0_texts)) {
            return $this->level0_texts;
        }

        if (empty($this->lists)) {
            $this->lists();
        }

        $text = array();
        foreach ($this->lists as $row) {
            if ($row['config']['level0']) {
                $text[] = $row['config']['level0'];
            }
        }

        $this->level0_texts = implode(" / ", array_unique($text));
        return $this->level0_texts;
    }

    /**
     * レポート評価するか否か
     * @return boolean
     */
    public function is_report()
    {
        if (empty($this->lists)) {
            $this->lists();
        }

        foreach ($this->lists as $row) {
            if ($row['config']['flow'] === '3') {
                return true;
            }
        }
        return false;
    }

    /**
     * ラダー名称
     * @param type $ladder_id
     * @return type
     */
    public function title($ladder_id)
    {
        $ladder = $this->find($ladder_id);
        return $ladder['title'];
    }

    /**
     * レベル0名称
     * @param type $ladder_id
     * @return type
     */
    public function level0($ladder_id)
    {
        $ladder = $this->find($ladder_id);
        return $ladder['config']['level0'];
    }

    /**
     * レベル取得用SQL, JOIN
     * @return type
     */
    public function level_sql()
    {
        foreach ($this->lists() as $ladder) {
            $sql .= "ldr{$ladder['ladder_id']}.level AS level{$ladder['ladder_id']},";
            $join .= "LEFT JOIN (SELECT emp_id, MAX(level) AS level FROM ldr_level WHERE ladder_id={$ladder['ladder_id']} AND date IS NOT NULL GROUP BY emp_id) ldr{$ladder['ladder_id']} USING (emp_id) ";
        }
        $sql = rtrim($sql, ",");

        return array($sql, $join);
    }

    /**
     * 削除
     * @param type $id
     * @throws Exception
     */
    public function remove($id)
    {
        $this->db->beginNestedTransaction();

        // ldr_ladder
        $sth = $this->db->prepare("UPDATE ldr_ladder SET del_flg='t' WHERE ladder_id= ?", array('integer'), MDB2_PREPARE_MANIP);
        $res = $sth->execute(array($id));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }
        $sth->free();

        $this->db->completeNestedTransaction();

        // Log
        $this->log->log("削除しました", "ladder_id=$id");
    }

}
