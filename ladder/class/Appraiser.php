<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'Cmx/Model/SystemConfig.php';
require_once 'ladder/class/Log.php';

class Ldr_Appraiser extends Model
{

    private $log;
    private $_appraiser;

    /**
     * ���󥹥ȥ饯��
     */
    public function __construct()
    {
        parent::connectDB();
        $this->_appraiser = array();
        $this->log = new Ldr_log('appraiser');
    }

    /**
     * select
     * @param Integer $id
     * @return Array
     */
    private function _select($id)
    {
        $sql = "SELECT a.emp_id, e.emp_lt_nm|| ' ' || e.emp_ft_nm as emp_name, facilitator, update7, completed FROM ldr_appraiser a JOIN empmst e USING (emp_id) WHERE ldr_apply_id = ? ORDER BY facilitator DESC, emp_kn_lt_nm, emp_kn_ft_nm";
        $sth = $this->db->prepare($sql, array('integer'), MDB2_PREPARE_RESULT);
        return $sth->execute(array($id));
    }

    /**
     * �ǡ�������
     * @param Integer $id
     * @return Array
     */
    public function find($id)
    {
        if (empty($id)) {
            return array();
        }

        $res = $this->_select($id);
        if (PEAR::isError($res)) {
            throw new Exception('ldr_appraiser SELECT ERROR: ' . $res->getDebugInfo());
        }

        $data = array();
        $list = array();
        $data["full_completed"] = true;
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[] = $row;

            // emp_id
            $data["emp_ids"][] = $row["emp_id"];

            // ɾ���ԡ��ʲ��
            if ($row["facilitator"] === 't') {
                $data["facilitator"][$row["emp_id"]] = $row["emp_name"];
            }
            else {
                $data["appraiser"][$row["emp_id"]] = $row["emp_name"];
            }

            // ɾ����λ
            if ($row["completed"] === 't') {
                $data["completed"][] = $row["emp_id"];
            }
            else {
                $data["full_completed"] = false;
            }
        }
        $data['appr_list'] = $list;

        return $data;
    }


    /**
     * ����ɾ���ǡ�������
     * @param Integer $id
     * @return Array
     */
    public function total_find($id)
    {
        if (empty($id)) {
            return array();
        }

        $sql = "SELECT a.emp_id, e.emp_lt_nm|| ' ' || e.emp_ft_nm as emp_name, completed FROM ldr_total_appraiser a JOIN empmst e USING (emp_id) WHERE ldr_apply_id = ? ORDER BY emp_kn_lt_nm, emp_kn_ft_nm";
        $sth = $this->db->prepare($sql, array('integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array($id));
        if (PEAR::isError($res)) {
            throw new Exception('ldr_total_appraiser SELECT ERROR: ' . $res->getDebugInfo());
        }

        $data = array();
        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[$row["emp_id"]] = $row;
            $data['completed'] = $row;
        }
        $data['appr_list'] = $list;

        return $data;
    }

    /**
     * �����ǡ�������
     * @param Integer $id
     * @return Array
     */
    public function lists($id)
    {
        if (empty($id)) {
            return array();
        }

        $res = $this->_select($id);
        if (PEAR::isError($res)) {
            throw new Exception('ldr_appraiser SELECT ERROR: ' . $res->getDebugInfo());
        }
        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[] = $row;
        }
        return $list;
    }

    /**
     * ��¸
     * @param Array $data
     */
    public function create($data)
    {
        $appr = array();
        foreach (array_keys($data['facilitator']) as $emp_id) {
            $appr[] = array(
                'ldr_apply_id' => $data['ldr_apply_id'],
                'emp_id' => $emp_id,
                'facilitator' => true,
            );
        }
        foreach (array_keys($data['appraiser']) as $emp_id) {
            $appr[] = array(
                'ldr_apply_id' => $data['ldr_apply_id'],
                'emp_id' => $emp_id,
                'facilitator' => false,
            );
        }

        $this->db->beginNestedTransaction();

        // DELETE
        $del_sth = $this->db->prepare("DELETE FROM ldr_appraiser WHERE ldr_apply_id = :ldr_apply_id", array('integer'), MDB2_PREPARE_MANIP);
        $res = $del_sth->execute(array('ldr_apply_id' => $data['ldr_apply_id']));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception('ldr_appraiser DELETE ERROR' . $res->getDebugInfo());
        }
        $del_sth->free();

        // INSERT
        $ins_sth = $this->db->prepare("INSERT INTO ldr_appraiser (ldr_apply_id, emp_id, facilitator) VALUES (:ldr_apply_id, :emp_id, :facilitator)", array('integer', 'text', 'boolean'), MDB2_PREPARE_MANIP);
        $ins_res = $this->db->extended->executeMultiple($ins_sth, $appr);
        if (PEAR::isError($ins_res)) {
            $this->db->failNestedTransaction();
            throw new Exception('ldr_appraiser INSERT ERROR' . $ins_res->getDebugInfo());
        }
        $ins_sth->free();

        $this->db->completeNestedTransaction();
    }

    /**
     * ��¸
     * @param Array $data
     */
    public function total_create($ldr_apply_id, $approver)
    {
        $appr = array();
        foreach ($approver as $emp) {
            $appr[] = array(
                'ldr_apply_id' => $ldr_apply_id,
                'emp_id' => $emp->emp_id(),
            );
        }

        $this->db->beginNestedTransaction();

        // DELETE
        $del_sth = $this->db->prepare("DELETE FROM ldr_total_appraiser WHERE ldr_apply_id = :ldr_apply_id", array('integer'), MDB2_PREPARE_MANIP);
        $res = $del_sth->execute(array('ldr_apply_id' => $ldr_apply_id));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception('ldr_total_appraiser DELETE ERROR' . $res->getDebugInfo());
        }
        $del_sth->free();

        // INSERT
        $ins_sth = $this->db->prepare("INSERT INTO ldr_total_appraiser (ldr_apply_id, emp_id) VALUES (:ldr_apply_id, :emp_id)", array('integer', 'text'), MDB2_PREPARE_MANIP);
        $ins_res = $this->db->extended->executeMultiple($ins_sth, $appr);
        if (PEAR::isError($ins_res)) {
            $this->db->failNestedTransaction();
            throw new Exception('ldr_total_appraiser INSERT ERROR' . $ins_res->getDebugInfo());
        }
        $ins_sth->free();

        $this->db->completeNestedTransaction();
    }

    /**
     * �ʲ��Ƚ��
     * @param type $emp
     * @param type $status
     * @return type
     * @throws Exception
     */
    public function is_facilitator_status($emp, $status)
    {
        $emp_id = array_shift(array_keys($emp));

        $sql = "SELECT emp_st FROM empmst WHERE emp_id = ?";
        $row = $this->db->extended->getRow($sql, null, array($emp_id), array('text'), MDB2_FETCHMODE_ASSOC);
        if (PEAR::isError($row)) {
            throw new Exception(__METHOD__ . " ERROR: " . $row->getDebugInfo());
        }

        if (in_array($row['emp_st'], $status)) {
            return true;
        }
        return false;
    }

    /**
     * �ʲ��Ƚ��
     * @param type $id
     * @param type $emp_id
     * @return type
     */
    public function is_facilitator($id, $emp_id)
    {
        if (empty($this->_appraiser[$id])) {
            $this->_appraiser[$id] = $this->find($id);
        }

        if ($this->_appraiser[$id]['facilitator'] && in_array($emp_id, array_keys($this->_appraiser[$id]['facilitator']))) {
            return true;
        }

        return false;
    }

    /**
     * ɾ����Ƚ��
     * @param type $id
     * @param type $emp_id
     * @return type
     */
    public function is_appraiser($id, $emp_id)
    {
        if (empty($this->_appraiser[$id])) {
            $this->_appraiser[$id] = $this->find($id);
        }

        if ($this->_appraiser[$id]['appraiser'] && in_array($emp_id, array_keys($this->_appraiser[$id]['appraiser']))) {
            return true;
        }

        return false;
    }

    /**
     * ����ɾ�� ɾ����Ƚ��
     * @param type $id
     * @param type $emp_id
     * @return type
     */
    public function is_total_appraiser($id, $emp_id)
    {
        $sql = "SELECT COUNT(*) as count FROM ldr_total_appraiser WHERE ldr_apply_id = ? AND emp_id = ?";
        $count = $this->db->extended->getRow($sql, null, array($id, $emp_id), array('integer', 'text'), MDB2_FETCHMODE_ASSOC);

        if ($count['count'] > 0) {
            return true;
        }
        return false;
    }

    /**
     * ɾ���Ѥ�Ƚ��
     * @param type $id
     * @param type $emp_id
     * @return type
     */
    public function is_completed($id, $emp_id)
    {
        if (empty($this->_appraiser[$id])) {
            $this->_appraiser[$id] = $this->find($id);
        }

        if ($this->_appraiser[$id]['completed'] && in_array($emp_id, $this->_appraiser[$id]['completed'])) {
            return true;
        }

        return false;
    }

    /**
     * ����ɾ�� ɾ���Ѥ�Ƚ��
     * @param type $id
     * @param type $emp_id
     * @return type
     */
    public function is_total_completed($id)
    {
        $sql = "SELECT COUNT(*) as count FROM ldr_total_appraiser WHERE ldr_apply_id = ? AND completed";
        $count = $this->db->extended->getRow($sql, null, array($id), array('integer'), MDB2_FETCHMODE_ASSOC);

        if ($count['count'] > 0) {
            return true;
        }
        return false;
    }

    /**
     * ɾ����λ
     * @param type $id        ����ID
     * @param type $emp_id    ����ID
     * @throws Exception
     */
    public function updated_completed($id, $emp_id)
    {
        $this->db->beginNestedTransaction();

        $sth = $this->db->prepare("UPDATE ldr_appraiser SET completed = true, update7 = CURRENT_TIMESTAMP WHERE ldr_apply_id= :ldr_apply_id AND emp_id = :emp_id ", array('integer', 'text'), MDB2_PREPARE_MANIP);
        $res = $sth->execute(array('ldr_apply_id' => $id, 'emp_id' => $emp_id));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }
        $sth->free();
        $this->db->completeNestedTransaction();
    }

    /**
     * ����ɾ����λ
     * @param type $id        ����ID
     * @param type $emp_id    ����ID
     * @throws Exception
     */
    public function updated_total_completed($id, $emp_id)
    {
        $this->db->beginNestedTransaction();

        $sth = $this->db->prepare("UPDATE ldr_total_appraiser SET completed = true WHERE ldr_apply_id= :ldr_apply_id AND emp_id = :emp_id ", array('integer', 'text'), MDB2_PREPARE_MANIP);
        $res = $sth->execute(array('ldr_apply_id' => $id, 'emp_id' => $emp_id));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }
        $sth->free();
        $this->db->completeNestedTransaction();
    }

}
