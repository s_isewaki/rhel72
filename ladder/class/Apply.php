<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'ladder/class/Form1.php';
require_once 'ladder/class/Form6.php';
require_once 'ladder/class/Form7.php';
require_once 'ladder/class/Form8.php';
require_once 'ladder/class/Form9.php';
require_once 'ladder/class/Appraiser.php';
require_once 'ladder/class/Comment.php';
require_once 'ladder/class/Log.php';
require_once 'ladder/class/Guideline.php';
require_once 'ladder/class/Assessment.php';
require_once 'ladder/class/Committee.php';
require_once 'ladder/class/Auditor.php';
require_once 'ladder/class/Config.php';
require_once 'ladder/class/Ladder.php';
require_once 'ladder/class/Report.php';
require_once 'ladder/class/Objection.php';

class Ldr_Apply extends Model
{

    private $apply_emp_id;
    private $accept_emp_id;
    private $apply_id;
    private $ladder;
    private $ladder_id;
    private $ladder_data;
    private $mode;
    private $apply;
    private $form1;
    private $form6;
    private $form7;
    private $form8;
    private $form9;
    private $appraiser;
    private $comment;
    private $auditor;
    private $log;

    /**
     * コンストラクタ
     * @param type $param 職員ID
     */
    public function Ldr_Apply($param)
    {
        parent::connectDB();
        $this->log = new Ldr_log('apply');
        $this->ladder = new Ldr_Ladder();

        // ラダーID
        $this->set_ladder($param['ladder_id']);

        // 申請者 職員ID
        $this->set_apply_emp_id($param['apply_emp_id']);

        // 申請ID
        if ($param['apply_id'] === "recently") {
            $this->set_apply_recently();
        }
        else {
            $this->set_apply($param['apply_id']);
        }

        // 受付者 職員ID
        if (empty($param['accept_emp_id'])) {
            $this->set_accept_emp_id($param['apply_emp_id']);
        }
        else {
            $this->set_accept_emp_id($param['accept_emp_id']);
        }

        // 申請/受付モード
        $this->set_mode($param['mode']);
    }

    /**
     * ラダー承認者
     */
    public function approver($emp_id)
    {
        $ladder_id = $this->ladder_id();
        if (empty($ladder_id)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }
        return $this->ladder->approver($ladder_id, $emp_id);
    }

    /**
     * ラダー承認者判定
     * @return type
     */
    public function is_approver($emp_id)
    {
        $approver = $this->approver($this->apply_emp_id);
        foreach ($approver as $emp) {
            if ($emp_id === $emp->emp_id()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Appraiser Object
     */
    public function appraiser()
    {
        if (empty($this->appraiser)) {
            $this->appraiser = new Ldr_Appraiser();
        }
        return $this->appraiser;
    }

    /**
     * Auditor Object
     */
    public function auditor()
    {
        if (empty($this->auditor)) {
            $this->auditor = new Ldr_Auditor();
        }
        return $this->auditor;
    }

    /**
     * Report Object
     */
    public function report()
    {
        if (empty($this->report)) {
            $ladder = $this->ladder();
            $this->report = new Ldr_Report($this->apply_id, $ladder['config']['report_guideline']);
        }
        return $this->report;
    }

    /**
     * Commment Object
     */
    public function comment()
    {
        if (empty($this->apply_emp_id) || empty($this->accept_emp_id)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }
        if (empty($this->comment)) {
            if ($this->mode === 'apply') {
                $this->comment = new Ldr_Comment($this->apply_emp_id);
            }
            else {
                $this->comment = new Ldr_Comment($this->accept_emp_id);
            }
        }
        return $this->comment;
    }

    /**
     * Form1 Object
     */
    public function form1()
    {
        if (empty($this->apply_emp_id)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }

        if (empty($this->form1)) {
            $this->form1 = new Ldr_Form1($this->apply_emp_id, $this->ladder());
        }
        return $this->form1;
    }

    /**
     * Form6 Object
     */
    public function form6()
    {
        if (empty($this->apply_emp_id)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }
        if (empty($this->form6)) {
            $this->form6 = new Ldr_Form6($this->apply_emp_id);
        }
        return $this->form6;
    }

    /**
     * Form7 Object
     */
    public function form7()
    {
        // 申請
        if ($this->mode === 'apply') {
            if (empty($this->apply_emp_id)) {
                throw new Exception(__METHOD__ . " METHOD Param error.");
            }
            if (empty($this->form7)) {
                $this->form7 = new Ldr_Form7($this->apply_emp_id);
            }
        }
        // 受付
        else {
            if (empty($this->accept_emp_id)) {
                throw new Exception(__METHOD__ . " METHOD Param error.");
            }
            if (empty($this->form7)) {
                $this->form7 = new Ldr_Form7($this->accept_emp_id);
            }
        }
        return $this->form7;
    }

    /**
     * Form8 Object
     */
    public function form8()
    {
        if (empty($this->accept_emp_id)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }
        if (empty($this->form8)) {
            $this->form8 = new Ldr_Form8($this->accept_emp_id);
        }
        return $this->form8;
    }

    /**
     * Form9 Object
     */
    public function form9()
    {
        if (empty($this->apply_emp_id)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }
        if (empty($this->form9)) {
            $this->form9 = new Ldr_Form9($this->apply_emp_id);
        }
        return $this->form9;
    }

    /**
     * ラダーIDのセット
     * @param type $ladder_id
     */
    public function set_ladder($ladder_id)
    {
        $this->ladder_id = $ladder_id;
        $this->ladder_data = $this->ladder->find($ladder_id);
    }

    /**
     * ラダーデータを取得
     * @return type
     */
    public function ladder()
    {
        if (!empty($this->apply['ladder'])) {
            return $this->apply['ladder'];
        }
        return $this->ladder_data;
    }

    /**
     * ラダーIDを取得
     * @return type
     */
    public function ladder_id()
    {
        if (!empty($this->apply['ladder'])) {
            return $this->apply['ladder']['ladder_id'];
        }
        return $this->ladder_id;
    }

    /**
     * 申請者の職員IDのセット
     * @param type $emp_id
     */
    private function set_apply_emp_id($emp_id)
    {
        $this->apply_emp_id = $emp_id;
    }

    /**
     * 受付者の職員IDのセット
     * @param type $emp_id
     */
    private function set_accept_emp_id($emp_id)
    {
        $this->accept_emp_id = $emp_id;
    }

    /**
     * 申請/受付モードのセット
     * デフォルトは申請(apply)
     * @param type $mode
     */
    public function set_mode($mode)
    {
        if (empty($mode)) {
            $mode = 'apply';
        }
        $this->mode = $mode;
    }

    /**
     * 申請IDのセット
     * @param type $apply_id
     */
    private function set_apply($apply_id)
    {
        $this->apply_id = $apply_id;
        $this->apply = $this->find_apply();
    }

    /**
     * 直近の申請をセット
     * @param type $apply_id
     */
    private function set_apply_recently($apply_id)
    {
        $this->apply = $this->find_apply_recently();

        if (!empty($this->apply)) {
            $this->apply_id = $this->apply['ldr_apply_id'];
        }
    }

    /**
     * 申請IDを取得
     * @return type
     */
    public function apply_id()
    {
        return $this->apply_id;
    }

    /**
     * 申請者職員IDを取得
     * @return type
     */
    public function apply_emp_id()
    {
        if (empty($this->apply_emp_id)) {
            $this->apply_emp_id = $this->apply['emp_id'];
        }
        return $this->apply_emp_id;
    }

    /**
     * 申請データを取得
     * @return type
     */
    public function apply()
    {
        return $this->apply;
    }

    /**
     * 様式1の入力バリデーション
     * @param type $data
     * @return type
     */
    public function validate_form1($data)
    {
        return $this->form1()->validate($data);
    }

    /**
     * 様式6の入力バリデーション
     * @param type $data
     * @return type
     */
    public function validate_form6($data)
    {
        return $this->form6()->validate($data);
    }

    /**
     * 様式7-1の入力バリデーション
     * @param type $data
     * @return type
     */
    public function validate_form71($data, $rc = null)
    {
        return $this->form7()->validate_form71($data, $rc);
    }

    /**
     * 様式7-3の入力バリデーション
     * @param type $data
     * @return type
     */
    public function validate_form73($data)
    {
        return $this->form7()->validate_form73($data);
    }

    /**
     * 様式8の入力バリデーション
     * @param type $data
     * @return type
     */
    public function validate_form8($data, $rc = null)
    {
        return $this->form8()->validate($data, $rc);
    }

    /**
     * 様式9の入力バリデーション
     * @param type $data
     * @return type
     */
    public function validate_form9($data, $rc = null)
    {
        return $this->form9()->validate($data, $rc);
    }

    /**
     * コメントの入力バリデーション
     * @param type $data
     * @return type
     */
    public function validate_comment($data)
    {
        return $this->comment()->validate($data);
    }

    /**
     * 師長　会場時間入力　評価者の入力バリデーション
     */
    public function validate_accept_qualify($data)
    {
        return $this->form1()->validate_accept_qualify($data);
    }

    /**
     * 師長申込時の入力バリデーション
     */
    public function validate_qualify()
    {
        $error = array();

        // 様式1
        $form1 = $this->find_form1($this->apply_id);
        $fm1_error = $this->form1()->validate_qualify($form1);
        if (!empty($fm1_error)) {
            $error['form1'] = $fm1_error;
        }

        // 様式6
        $form6 = $this->find_form6();
        $fm6_error = $this->form6()->validate($form6);
        if (!empty($fm6_error)) {
            $error['form6'] = $fm6_error;
        }

        // 様式7
        $as = new Ldr_Assessment();
        $group1_list = $as->lists('group1', array('form7' => true, 'guideline_revision' => $this->guideline_revision()));
        foreach ($group1_list as $value) {
            $gp[$value['guideline_group1']] = $value['name'];
        }
        $as_list = $this->assessment_list();
        foreach ($as_list as $value) {
            foreach ($value as $value2) {
                if (is_null($value2['accept_assessment'])) {
                    $error['form7'][$value2['guideline_group1']][] = $gp[$value2['guideline_group1']] . "が評価されていません";
                    break;
                }
            }
        }

        // 様式7コメント
        $ldr = $this->ladder();
        $form7 = $this->find_form7();
        if ($ldr['config']['asm_comment_used'] && $ldr['config']['asm_comment_required']) {
            if (Validation::is_empty($form7['knowledge'])) {
                $error['form7']['comment'][] = $ldr['config']['asm_comment'] . "が入力されていません";
            }
        }
        if ($ldr['config']['asm_comment2_used'] && $ldr['config']['asm_comment2_required']) {
            if (Validation::is_empty($form7['decision'])) {
                $error['form7']['comment'][] = $ldr['config']['asm_comment2'] . "が入力されていません";
            }
        }

        return $error;
    }

    /**
     * 評価完了の入力バリデーション
     */
    public function validate_appraiser()
    {
        $error = array();
        $form7 = $this->find_form7();
        $ldr = $this->ladder();

        // 7-I (赤十字のみ)
        if ($ldr['config']['flow'] === '1') {
            $error71 = $this->form7()->validate_form71($form7, $ldr['config']['rc']);
            if (!empty($error71)) {
                if ($ldr['config']['rc'] === '2') {
                    $error['form7'][] = '看護管理実践に未記入の項目があります';
                }
                else {
                    $error['form7'][] = '臨床看護実践に未記入の項目があります';
                }
            }
        }

        // 7-II
        $as = new Ldr_Assessment();
        $group1_list = $as->lists('group1', array('form7' => true, 'guideline_revision' => $this->guideline_revision()));
        foreach ($group1_list as $value) {
            $gp[$value['guideline_group1']] = $value['name'];
        }
        $as_list = $this->assessment_list();
        foreach ($as_list as $value) {
            foreach ($value as $value2) {
                if (is_null($value2['accept_assessment'])) {
                    $error['form7'][] = $gp[$value2['guideline_group1']] . "に未評価の項目があります";
                    break;
                }
            }
            if ($ldr['config']['diff_reason'] === '1') {
                foreach ($value as $value2) {
                    if ($value2['apply_assessment'] !== $value2['accept_assessment'] && empty($value2['accept_reason'])) {
                        $error['form7'][] = $gp[$value2['guideline_group1']] . "に未記入の根拠があります";
                        break;
                    }
                }
            }
        }

        // 7-III (赤十字のみ)
        if ($ldr['config']['flow'] === '1') {
            $error73 = $this->form7()->validate_form73($form7);
            if (!empty($error73)) {
                $error['form7'][] = '総合レベルに未記入の項目があります';
            }
        }

        return $error;
    }

    /**
     * 総合評価完了の入力バリデーション
     */
    public function validate_total()
    {
        $error = array();
        $form7 = $this->find_form7();
        $ldr = $this->ladder();

        // 7-II
        $as = new Ldr_Assessment();
        $group1_list = $as->lists('group1', array('form7' => true, 'guideline_revision' => $this->guideline_revision()));
        foreach ($group1_list as $value) {
            $gp[$value['guideline_group1']] = $value['name'];
        }
        $as_list = $this->assessment_list();
        foreach ($as_list as $value) {
            foreach ($value as $value2) {
                if (is_null($value2['accept_assessment'])) {
                    $error['form8'][] = $gp[$value2['guideline_group1']] . "に未評価の項目があります";
                    break;
                }
            }
            if ($ldr['config']['diff_reason'] === '1') {
                foreach ($value as $value2) {
                    if ($value2['apply_assessment'] !== $value2['accept_assessment'] && empty($value2['accept_reason'])) {
                        $error['form8'][] = $gp[$value2['guideline_group1']] . "に未記入の根拠があります";
                        break;
                    }
                }
            }
        }

        // 評価者コメント
        if (Validation::is_empty($form7['advice'])) {
            $error['form8'][] = "コメントが入力されていません";
        }

        return $error;
    }

    /**
     * 監査者設定時の入力バリデーション
     * @param type $data
     */
    public function validate_auditor($data)
    {
        $st = (int)$this->get_status();
        if ($st !== 10 && $st !== 11) {
            $error = array();
            $error['configured'][] = '監査者は設定済みです。受付一覧のステータスを確認してください。';
        }
        else {
            $error = $this->auditor()->validate($data);
        }

        return $error;
    }

    /**
     * レポート評価者設定時の入力バリデーション
     * @param type $data
     */
    public function validate_reporter($data)
    {
        $st = (int)$this->get_report_status();
        if ($st !== 21 && $st !== 22) {
            $error = array();
            $error['configured'][] = 'レポート評価者は設定済みです。受付一覧のステータスを確認してください。';
        }
        else {
            $error = $this->report()->reporter_validate($data);
        }

        return $error;
    }

    /**
     * 合否判定の入力バリデーション
     * @param type $data
     */
    public function validate_result($data)
    {
        $error = array();

        $st = (int)$this->get_status();
        if ($st !== 12) {
            $error['configured'][] = '合否判定は設定済みです。受付一覧のステータスを確認してください。';
        }
        else {
            if (Validation::is_empty($data['result'])) {
                $error['result'][] = "合否判定が設定されていません";
            }
            if ($data['result'] === '1' && Validation::is_empty($data['result_date'])) {
                $error['result_date'][] = "認定日が設定されていません";
            }
            if (mb_strlen($data['approved_number'], 'EUC-JP') > 20) {
                $error['approved_number'][] = '認定番号は20文字以内で入力してください';
            }
        }

        return $error;
    }

    /**
     * 様式6の保存
     * @param type $data
     * @return type
     */
    public function save_form6($data, $mode = null)
    {
        $this->form6()->save($data, $mode);
        $this->updated_date(6);
    }

    /**
     * 様式7-1の保存
     * @param type $data
     * @return type
     */
    public function save_form71($data, $mode = null)
    {
        $this->form7()->save_form71($data, $mode);
        $this->updated_date('7_1');
    }

    /**
     * 様式7-2の保存
     * @param type $data
     * @return type
     */
    public function save_form72($data, $mode = null)
    {
        $this->form7()->save_form72($data, $mode);
        $this->updated_date('7_2');
    }

    /**
     * 様式7-3の保存
     * @param type $data
     * @return type
     */
    public function save_form73($data, $mode = null)
    {
        $this->form7()->save_form73($data, $mode);
        $this->updated_date('7_3');
    }

    /**
     * 様式8の保存
     * @param type $data
     * @return type
     */
    public function save_form8($data, $mode = null)
    {
        $this->form8()->save($data, $mode);
        $this->updated_date(8);

        // 確定
        if ($mode === 'completed') {
            // 監査者権限
            $comm = new Ldr_Committee();
            $auditor = $comm->auditor();

            // ステータスの変更
            $this->status_change(10, $auditor);
        }
    }

    /**
     * 様式9の保存
     * @param type $data
     * @return type
     */
    public function save_form9($data, $mode = null)
    {
        $this->form9()->save($data, $mode);
        $this->updated_date(9);

        // 確定
        if ($mode === 'completed') {
            // 認定委員会
            $comm = new Ldr_Committee();
            $committee = $comm->committee();

            // ステータスの変更
            $this->status_change(12, $committee);
        }
    }

    /**
     * 評価完了
     * @return boolean true:全員完了 false:未完了者あり
     */
    public function update_appraiser_completed()
    {
        $ldr = $this->ladder();

        // 完了フラグ更新
        $this->appraiser()->updated_completed($this->apply_id, $this->accept_emp_id);

        // 評価者の完了状況を確認
        $appr = $this->appraiser()->find($this->apply_id);
        if ($appr['full_completed']) {
            // 赤十字
            if ($ldr['config']['flow'] === '1') {
                // 全員完了しているのでstatusを更新する
                $this->status_change(9, $appr["emp_ids"]);
            }
            // 標準・順天堂
            else {
                // 承認者
                $approver = $this->approver($this->apply_emp_id);

                // 総合評価者の登録
                $this->appraiser()->total_create($this->apply_id, $approver);
                $this->status_change(9, $approver);
            }
            return true;
        }
        return false;
    }

    /**
     * 総合評価完了
     */
    public function update_total_completed($emp_id)
    {
        // 完了フラグ更新
        $this->appraiser()->updated_total_completed($this->apply_id, $emp_id);
        $this->updated_date(8);

        // 認定委員会
        $comm = new Ldr_Committee();
        $committee = $comm->committee();

        // ステータスの変更
        $this->status_change(12, $committee);
    }

    /**
     * 申請データ(+様式1)の作成
     * @param type $data
     */
    public function create_apply($data)
    {
        if (empty($this->apply_emp_id)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }

        $this->db->beginNestedTransaction();

        // ldr_apply INSERT
        if (empty($data['ldr_apply_id'])) {
            $sth = $this->db->prepare("INSERT INTO ldr_apply (emp_id, ladder_id, level, status, guideline_revision, update1) VALUES (:emp_id, :ladder_id, :level, 4, :guideline_revision, CURRENT_TIMESTAMP)", array('text', 'integer', 'integer', 'integer'), MDB2_PREPARE_MANIP);
            $res = $sth->execute(array(
                'emp_id' => $this->apply_emp_id,
                'ladder_id' => $this->ladder_id,
                'level' => $data['level'],
                'guideline_revision' => $this->ladder_data['guideline_revision'],
            ));
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
            }
            $sth->free();
            $data['ldr_apply_id'] = $this->db->lastInsertId();

            // ステータス履歴
            $this->set_apply($data['ldr_apply_id']);
            $this->add_status_history(4);
        }
        // ldr_apply UPDATE
        else {
            $sth = $this->db->prepare("UPDATE ldr_apply SET level= :level, update1=CURRENT_TIMESTAMP WHERE ldr_apply_id= :ldr_apply_id", array('integer', 'integer'), MDB2_PREPARE_MANIP);
            $res = $sth->execute(array(
                'ldr_apply_id' => $data['ldr_apply_id'],
                'level' => $data['level'],
            ));
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
            }
            $sth->free();
        }

        // Form1 save
        $this->form1()->save($data);

        // Appraiser save
        $this->appraiser()->create($data);

        $this->db->completeNestedTransaction();
    }

    /**
     * update日付の更新
     * @param type $form
     * @return null
     * @throws Exception
     */
    public function updated_date($form)
    {
        if (empty($this->apply_id)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }

        switch ($form) {
            case 6:
            case '6':
                $field = 'update6';
                break;
            case '7_1':
            case '7_2':
            case '7_3':
                $field = 'update7';
                break;
            case 8:
            case '8':
                $field = 'update8';
                break;
            case 9:
            case '9':
                $field = 'update9';
                break;
            default:
                return null;
        }

        $sth = $this->db->prepare("UPDATE ldr_apply SET $field=CURRENT_TIMESTAMP WHERE ldr_apply_id= ?", array('integer'), MDB2_PREPARE_MANIP);
        $res = $sth->execute(array($this->apply_id));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }
        $sth->free();
    }

    /**
     * 1件取得
     * @param Integer $id
     * @return Array
     */
    private function find_apply()
    {
        $sql = "
            SELECT
                emp_id,
                ladder_id,
                level,
                status,
                report_status,
                result,
                guideline_revision,
                update1,
                update6,
                update7,
                update8,
                update9
            FROM ldr_apply
            WHERE ldr_apply_id= :ldr_apply_id
        ";
        $res = $this->db->extended->getRow(
            $sql, null, array('ldr_apply_id' => $this->apply_id), array('integer'), MDB2_FETCHMODE_ASSOC
        );
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }

        $res['ladder'] = $this->ladder->find($res['ladder_id']);

        return $res;
    }

    /**
     * 更新があった最新の申請を検索する
     * @return type
     * @throws Exception
     */
    private function find_apply_recently()
    {
        $sql = "
            SELECT
                ap.ldr_apply_id,
                ap.emp_id,
                ap.ladder_id,
                level,
                status,
                report_status,
                result,
                guideline_revision,
                update1,
                update6,
                update7,
                update8,
                update9,
                (SELECT MAX(updated_on) FROM ldr_accept_history ah WHERE ldr_apply_id=ap.ldr_apply_id AND status=8) as qualify_date,
                (SELECT COUNT(*) FROM ldr_comment WHERE ldr_apply_id=ap.ldr_apply_id) as comment,
                (SELECT COUNT(updated_on) FROM ldr_comment WHERE ldr_apply_id=ap.ldr_apply_id AND (updated_on > cr.timestamp OR cr.timestamp IS NULL)) as unread
            FROM ldr_apply ap
            LEFT JOIN ldr_comment_read cr ON cr.ldr_apply_id=ap.ldr_apply_id AND cr.emp_id=ap.emp_id
            WHERE
            ap.emp_id = :emp_id
            AND ap.updated_on = (select max(updated_on) from ldr_apply where emp_id = :emp_id AND NOT del_flg)
            AND NOT del_flg
        ";
        $res = $this->db->extended->getRow(
            $sql, null, array('emp_id' => $this->apply_emp_id), array('text', 'text'), MDB2_FETCHMODE_ASSOC
        );
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }

        $res['ladder'] = $this->ladder->find($res['ladder_id']);

        return $res;
    }

    /**
     * 様式１情報取得
     * @param type $id
     * @return type
     */
    public function find_form1($id)
    {
        $data = $this->form1()->find($id);
        $appr = $this->appraiser()->find($id);
        return array_merge($data, $appr);
    }

    /**
     * 様式6情報取得
     * @return type
     */
    public function find_form6()
    {
        return $this->form6()->find($this->apply_id);
    }

    /**
     * 様式7情報取得
     * @return type
     */
    public function find_form7()
    {
        return $this->form7()->find($this->apply_id);
    }

    /**
     * 様式8情報取得
     * @return type
     */
    public function find_form8()
    {
        return $this->form8()->find($this->apply_id);
    }

    /**
     * 様式9情報取得
     * @return type
     */
    public function find_form9()
    {
        return $this->form9()->find($this->apply_id);
    }

    /**
     * 総合評価者情報取得
     * @return type
     */
    public function total_appraiser()
    {
        return $this->appraiser()->total_find($this->apply_id);
    }

    /**
     * 申請件数
     * @param type $cond
     */
    public function count_apply($cond = null)
    {
        if (empty($this->apply_emp_id)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }

        $ret = $this->apply_condition($cond);

        $sql = "SELECT count(*) FROM ldr_apply LEFT JOIN ldr_form1 USING(ldr_apply_id) WHERE emp_id = :emp_id AND NOT ldr_apply.del_flg ";
        $sql .= $ret['cond'];

        $types = array_merge(array('text'), $ret['types']);
        $data = array_merge(array('emp_id' => $this->apply_emp_id), $ret['data']);

        $count = $this->db->extended->getOne($sql, null, $data, $types);
        if (PEAR::isError($count)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $count->getDebugInfo());
        }

        return $count;
    }

    /**
     * 申請一覧の検索条件
     * @param type $cond
     * @return string
     */
    private function apply_condition($cond = null)
    {
        if ($cond['recently'] === 'on') {
            $cond_ret .= ' AND ap.updated_on IN (SELECT MAX(updated_on) FROM ldr_apply WHERE emp_id = :emp_id GROUP BY ladder_id) ';
        }

        return array(
            'cond' => $cond_ret,
            'types' => array(),
            'data' => array(),
        );
    }

    /**
     * 様式7情報取得
     * @return type
     */
    public function lists_form7()
    {
        return $this->form7()->lists($this->apply_id);
    }

    /**
     * 申請一覧を取得
     * @param type $st
     * @param type $pageCount
     * @param type $cond
     */
    public function lists_apply($st = null, $pageCount = null, $cond = null)
    {
        if (empty($this->apply_emp_id)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }

        $ret = $this->apply_condition($cond);

        $sql = "
            SELECT
                ap.ldr_apply_id,
                ap.ladder_id,
                ap.emp_id,
                ap.level,
                f1.apply_date,
                f1.meeting_date,
                f1.meeting_start_time,
                f1.meeting_last_time,
                f1.meeting_room,
                status,
                report_status,
                result,
                guideline_revision,
                update1,
                update6,
                update7,
                update8,
                update9,
                ap.updated_on,
                (SELECT COUNT(*) FROM ldr_comment WHERE ldr_apply_id=ap.ldr_apply_id) as comment,
                (SELECT COUNT(updated_on) FROM ldr_comment WHERE ldr_apply_id=ap.ldr_apply_id AND (updated_on > cr.timestamp OR cr.timestamp IS NULL)) as unread,
                (SELECT MAX(updated_on) FROM ldr_accept_history ah WHERE ldr_apply_id=ap.ldr_apply_id AND status=8) as qualify_date
            FROM ldr_apply ap
            LEFT JOIN ldr_form1 f1 USING (ldr_apply_id)
            LEFT JOIN ldr_comment_read cr ON cr.ldr_apply_id=ap.ldr_apply_id AND cr.emp_id=ap.emp_id
            WHERE ap.emp_id = :emp_id
              AND NOT ap.del_flg
            ";
        $sql .= $ret['cond'];
        $sql .= " ORDER BY ap.updated_on desc";

        if (isset($st) && isset($pageCount)) {
            $this->db->setLimit($pageCount, $st);
        }

        $types = array_merge(array('integer', 'text'), $ret['types']);
        $data = array_merge(array('emp_id' => $this->apply_emp_id), $ret['data']);
        $sth = $this->db->prepare($sql, $types, MDB2_PREPARE_RESULT);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['ladder'] = $this->ladder->find($row['ladder_id']);
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 申請一覧(進行中)を取得
     */
    public function lists_apply_myself()
    {
        if (empty($this->apply_emp_id)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }

        $sql = "
            SELECT
                ap.ldr_apply_id,
                ap.ladder_id,
                ap.emp_id,
                ap.level,
                f1.apply_date,
                f1.meeting_date,
                f1.meeting_start_time,
                f1.meeting_last_time,
                f1.meeting_room,
                status,
                report_status,
                result,
                guideline_revision,
                update1,
                update6,
                update7,
                update8,
                update9,
                ap.updated_on,
                (SELECT COUNT(*) FROM ldr_comment WHERE ldr_apply_id=ap.ldr_apply_id) as comment,
                (SELECT COUNT(updated_on) FROM ldr_comment WHERE ldr_apply_id=ap.ldr_apply_id AND (updated_on > cr.timestamp OR cr.timestamp IS NULL)) as unread,
                (SELECT MAX(updated_on) FROM ldr_accept_history ah WHERE ldr_apply_id=ap.ldr_apply_id AND status=8) as qualify_date
            FROM ldr_apply ap
            LEFT JOIN ldr_form1 f1 USING (ldr_apply_id)
            LEFT JOIN ldr_comment_read cr ON cr.ldr_apply_id=ap.ldr_apply_id AND cr.emp_id=ap.emp_id
            WHERE ap.emp_id = :emp_id
              AND ap.status >= 4
              AND NOT ap.del_flg
            ";
        $sql .= " ORDER BY ap.updated_on desc";

        $sth = $this->db->prepare($sql, array('text'), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array('emp_id' => $this->apply_emp_id));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['ladder'] = $this->ladder->find($row['ladder_id']);
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 受付件数
     * @param type $cond
     */
    public function count_accept($cond = null)
    {
        if (empty($this->accept_emp_id)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }

        $ret = $this->accept_condition($cond);

        $sql = "SELECT count(*)
                FROM ldr_accept ac
                LEFT JOIN ldr_apply ap USING(ldr_apply_id)
                LEFT JOIN empmst e ON ap.emp_id = e.emp_id
                LEFT JOIN ldr_form1 f1 USING(ldr_apply_id)
                LEFT JOIN ldr_comment_read cr ON cr.ldr_apply_id=ac.ldr_apply_id AND cr.emp_id=ac.accept_emp_id
                WHERE
                ac.accept_emp_id = :accept_emp_id AND NOT ap.del_flg";
        $sql .= $ret['cond'];

        $types = array_merge(array('integer'), $ret['types']);
        $data = array_merge(array('accept_emp_id' => $this->accept_emp_id), $ret['data']);

        $count = $this->db->extended->getOne($sql, null, $data, $types);
        if (PEAR::isError($count)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $count->getDebugInfo());
        }

        return $count;
    }

    /**
     * 受付一覧の検索条件
     * @param type $cond
     * @return string
     */
    protected function accept_condition($cond = null)
    {
        $cond_ret = "";
        $types = array();
        $data = array();
        if (!empty($cond)) {

            if (!empty($cond['srh_name'])) {
                $keywords = preg_split(Ldr_Const::SRH_DELIMITERS, $cond['srh_name'], -1, PREG_SPLIT_NO_EMPTY);

                $cond_key.= " AND ( ";
                foreach ($keywords as $key => $value) {
                    if ($key != 0) {
                        $cond_key.= " AND ";
                    }

                    $label_key = "key" . $key;
                    $cond_key.= "(e.emp_lt_nm || ' ' || e.emp_ft_nm || ' ' || e.emp_kn_lt_nm || ' ' || e.emp_kn_ft_nm) ILIKE :" . $label_key . " ";
                    $types[] = 'text';
                    $data[$label_key] = "%" . $value . "%";
                }
                $cond_key.= ") ";
                $cond_ret .= $cond_key;
            }
            if (!empty($cond['srh_start_date'])) {
                $cond_ret .= " AND ( :srh_start_date <= to_char(ac.updated_on,'YYYY-MM-DD')) ";
                $types[] = 'text';
                $data['srh_start_date'] = $cond['srh_start_date'];
            }
            if (!empty($cond['srh_last_date'])) {
                $cond_ret .= " AND (to_char(ac.updated_on,'YYYY-MM-DD') <= :srh_last_date ) ";
                $types[] = 'text';
                $data['srh_last_date'] = $cond['srh_last_date'];
            }
            if ($cond['srh_level'] === "0" || !empty($cond['srh_level'])) {
                $cond_ret .= " AND ap.level = :srh_level ";
                $types[] = 'integer';
                $data['srh_level'] = $cond['srh_level'];
            }
            if ($cond['srh_status'] === "0" || !empty($cond['srh_status'])) {
                // ステータス
                if ($cond['srh_status'] !== "21" && $cond['srh_status'] !== "22" && $cond['srh_status'] !== "23") {
                    $status = preg_split(Ldr_Const::SRH_DELIMITERS, $cond['srh_status'], -1, PREG_SPLIT_NO_EMPTY);
                    $cond_key.= " AND ( ";
                    foreach ($status as $key => $value) {
                        if ($key != 0) {
                            $cond_key.= " OR ";
                        }

                        $label_key = "status" . $key;
                        $cond_key.= "ap.status = :" . $label_key . " ";
                        $types[] = 'integer';
                        $data[$label_key] = $value;
                    }
                    $cond_key.= ") ";
                    $cond_ret .= $cond_key;
                }
                // レポートステータス
                else {
                    $cond_ret .= " AND ap.report_status = :srh_status ";
                    $types[] = 'integer';
                    $data['srh_status'] = $cond['srh_status'];
                }
            }
            if ($cond['active_only'] === "on") {
                $cond_ret .= " AND ap.status > 0 ";
            }
            if (!empty($cond['srh_comment'])) {
                $cond_ret .= " AND (SELECT COUNT(updated_on) FROM ldr_comment WHERE ldr_apply_id=ac.ldr_apply_id AND (updated_on > cr.timestamp OR cr.timestamp IS NULL)) > 0 ";
            }
        }

        $ret['cond'] = $cond_ret;
        $ret['types'] = $types;
        $ret['data'] = $data;
        return $ret;
    }

    /**
     * 受付一覧を取得
     * @param type $st
     * @param type $pageCount
     * @param type $cond
     * @return type
     */
    public function lists_accept($st = null, $pageCount = null, $cond = null)
    {
        if (empty($this->accept_emp_id)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }

        $ret = $this->accept_condition($cond);

        $sql = "
            SELECT
                ac.ldr_apply_id,
                ap.status,
                ac.status as accept_status,
                ap.report_status,
                ap.emp_id,
                ap.ladder_id,
                ac.accept_emp_id,
                ac.updated_on,
                ap.updated_on as apply_updated_on,
                e.emp_lt_nm || ' ' || e.emp_ft_nm as emp_name,
                f1.apply_date,
                ap.level,
                (SELECT COUNT(*) FROM ldr_comment WHERE ldr_apply_id=ac.ldr_apply_id) as comment,
                (SELECT COUNT(updated_on) FROM ldr_comment WHERE ldr_apply_id=ac.ldr_apply_id AND (updated_on > cr.timestamp OR cr.timestamp IS NULL)) as unread,
                coalesce(apr.ldr_apply_id, 0) as appraiser,
                apr.facilitator,
                coalesce(tapr.ldr_apply_id, 0) as total_appraiser,
                coalesce(com.emp_id, '') as committee,
                com.auditor as permission_auditor,
                coalesce(aud.emp_id, '') as auditor,
                coalesce(rep.emp_id, '') as reporter
            FROM ldr_accept ac
            JOIN ldr_apply ap USING (ldr_apply_id)
            JOIN ldr_form1 f1 USING (ldr_apply_id)
            JOIN empmst e ON ap.emp_id = e.emp_id
            LEFT JOIN ldr_appraiser apr ON apr.ldr_apply_id=ac.ldr_apply_id AND apr.emp_id=ac.accept_emp_id
            LEFT JOIN ldr_total_appraiser tapr ON tapr.ldr_apply_id=ac.ldr_apply_id AND tapr.emp_id=ac.accept_emp_id
            LEFT JOIN ldr_committee com ON com.emp_id=ac.accept_emp_id
            LEFT JOIN ldr_auditor aud ON aud.ldr_apply_id=ac.ldr_apply_id AND aud.emp_id=ac.accept_emp_id
            LEFT JOIN ldr_reporter rep ON rep.ldr_apply_id=ac.ldr_apply_id AND rep.emp_id=ac.accept_emp_id
            LEFT JOIN ldr_comment_read cr ON cr.ldr_apply_id=ac.ldr_apply_id AND cr.emp_id=ac.accept_emp_id
                WHERE ac.accept_emp_id = :accept_emp_id
                  AND NOT ap.del_flg
            ";
        $sql .= $ret['cond'];

        $order = "";
        foreach ($cond['order_by'] as $value) {
            $order.= empty($order) ? "order by " : ", ";
            $order.= $value;
        }
        $sql .= $order;

        if (isset($st) && isset($pageCount)) {
            $this->db->setLimit($pageCount, $st);
        }

        $types = array_merge(array('text'), $ret['types']);
        $data = array_merge(array('accept_emp_id' => $this->accept_emp_id), $ret['data']);

        $sth = $this->db->prepare($sql, $types, MDB2_PREPARE_RESULT);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['appraiser_list'] = $this->appraiser()->lists($row['ldr_apply_id']);
            $this->apply_id = $row['ldr_apply_id'];
            $row['auditor_list'] = $this->auditor_list();
            $row['ladder'] = $this->ladder->find($row['ladder_id']);
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * すべての申請の検索条件を作成
     * @param type $cond
     * @return type
     */
    private function apply_all_condition($cond = null)
    {
        $cond_ret = "";
        $types = array();
        $data = array();
        if (!empty($cond)) {
            if (!empty($cond['srh_name'])) {
                $keywords = preg_split(Ldr_Const::SRH_DELIMITERS, $cond['srh_name'], -1, PREG_SPLIT_NO_EMPTY);

                $cond_key.= " AND ( ";
                foreach ($keywords as $key => $value) {
                    if ($key != 0) {
                        $cond_key.= " AND ";
                    }

                    $label_key = "key" . $key;
                    $cond_key.= "(e.emp_lt_nm || ' ' || e.emp_ft_nm || ' ' || e.emp_kn_lt_nm || ' ' || e.emp_kn_ft_nm) ILIKE :" . $label_key . " ";
                    $types[] = 'text';
                    $data[$label_key] = "%" . $value . "%";
                }
                $cond_key.= ") ";
                $cond_ret .= $cond_key;
            }
            if (!empty($cond['srh_start_date'])) {
                $cond_ret .= " AND ( :srh_start_date <= to_char(ap.updated_on,'YYYY-MM-DD')) ";
                $types[] = 'text';
                $data['srh_start_date'] = $cond['srh_start_date'];
            }
            if (!empty($cond['srh_last_date'])) {
                $cond_ret .= " AND (to_char(ap.updated_on,'YYYY-MM-DD') <= :srh_last_date ) ";
                $types[] = 'text';
                $data['srh_last_date'] = $cond['srh_last_date'];
            }
            if ($cond['srh_level'] === "0" || !empty($cond['srh_level'])) {
                $cond_ret .= " AND ap.level = :srh_level ";
                $types[] = 'integer';
                $data['srh_level'] = $cond['srh_level'];
            }
            if ($cond['srh_status'] === "0" || !empty($cond['srh_status'])) {
                $status = preg_split(Ldr_Const::SRH_DELIMITERS, $cond['srh_status'], -1, PREG_SPLIT_NO_EMPTY);
                $cond_key.= " AND ( ";
                foreach ($status as $key => $value) {
                    if ($key != 0) {
                        $cond_key.= " OR ";
                    }

                    $label_key = "status" . $key;
                    // レポート評価(21〜23)の場合はreport_statusを見る
                    if ($value >= '21' && $value <= '23') {
                        $cond_key.= "ap.report_status = :" . $label_key . " ";
                    }
                    else {
                        $cond_key.= "ap.status = :" . $label_key . " ";
                    }
                    $types[] = 'integer';
                    $data[$label_key] = $value;
                }
                $cond_key.= ") ";
                $cond_ret .= $cond_key;
            }
        }

        $ret['cond'] = $cond_ret;
        $ret['types'] = $types;
        $ret['data'] = $data;
        return $ret;
    }

    /**
     * すべての申請の件数を取得
     * @param type $cond
     */
    public function count_apply_all($cond = null)
    {
        if (empty($this->accept_emp_id)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }
        $ret = $this->apply_all_condition($cond);

        $sql = "SELECT
                    count(*)
                FROM ldr_apply ap
                LEFT JOIN ldr_form1 USING(ldr_apply_id)
                JOIN empmst e ON ap.emp_id = e.emp_id
                WHERE NOT ap.del_flg ";
        $sql .= $ret['cond'];

        $types = array_merge(array('text'), $ret['types']);
        $data = array_merge(array('emp_id' => $this->apply_emp_id), $ret['data']);

        $count = $this->db->extended->getOne($sql, null, $data, $types);
        if (PEAR::isError($count)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $count->getDebugInfo());
        }

        return $count;
    }

    /**
     * 申請件数（ステータス毎）
     * @return Array
     * @throws Exception
     */
    public function count_apply_status()
    {

        $sql = "
            SELECT
                (SELECT COUNT(*) FROM ldr_apply WHERE status in (4,6) AND NOT del_flg) AS st4_count,
                (SELECT COUNT(*) FROM ldr_apply WHERE status in (4,6) AND NOT del_flg AND updated_on < current_date - interval '6 day') AS st4_count_week_ago,
                (SELECT COUNT(*) FROM ldr_apply WHERE status=5 AND report_status IS NULL AND NOT del_flg) AS st5_count,
                (SELECT COUNT(*) FROM ldr_apply WHERE status=5 AND report_status IS NULL AND NOT del_flg AND updated_on < current_date - interval '6 day') AS st5_count_week_ago,
                (SELECT COUNT(*) FROM ldr_apply WHERE status=8 AND NOT del_flg) AS st8_count,
                (SELECT COUNT(*) FROM ldr_apply WHERE status=8 AND NOT del_flg AND updated_on < current_date - interval '6 day') AS st8_count_week_ago,
                (SELECT COUNT(*) FROM ldr_apply WHERE status=9 AND NOT del_flg) AS st9_count,
                (SELECT COUNT(*) FROM ldr_apply WHERE status=9 AND NOT del_flg AND updated_on < current_date - interval '6 day') AS st9_count_week_ago,
                (SELECT COUNT(*) FROM ldr_apply WHERE status=10 AND NOT del_flg) AS st10_count,
                (SELECT COUNT(*) FROM ldr_apply WHERE status=10 AND NOT del_flg AND updated_on < current_date - interval '6 day') AS st10_count_week_ago,
                (SELECT COUNT(*) FROM ldr_apply WHERE status=11 AND NOT del_flg) AS st11_count,
                (SELECT COUNT(*) FROM ldr_apply WHERE status=11 AND NOT del_flg AND updated_on < current_date - interval '6 day') AS st11_count_week_ago,
                (SELECT COUNT(*) FROM ldr_apply WHERE status=12 AND NOT del_flg) AS st12_count,
                (SELECT COUNT(*) FROM ldr_apply WHERE status=12 AND NOT del_flg AND updated_on < current_date - interval '6 day') AS st12_count_week_ago,
                (SELECT COUNT(*) FROM ldr_apply WHERE report_status=21 AND NOT del_flg) AS st21_count,
                (SELECT COUNT(*) FROM ldr_apply WHERE report_status=21 AND NOT del_flg AND updated_on < current_date - interval '6 day') AS st21_count_week_ago,
                (SELECT COUNT(*) FROM ldr_apply WHERE report_status=22 AND NOT del_flg) AS st22_count,
                (SELECT COUNT(*) FROM ldr_apply WHERE report_status=22 AND NOT del_flg AND updated_on < current_date - interval '6 day') AS st22_count_week_ago,
                (SELECT COUNT(*) FROM ldr_apply WHERE report_status=23 AND NOT del_flg) AS st23_count,
                (SELECT COUNT(*) FROM ldr_apply WHERE report_status=23 AND NOT del_flg AND updated_on < current_date - interval '6 day') AS st23_count_week_ago
        ";
        $row = $this->db->extended->getRow($sql, null, null, null, MDB2_FETCHMODE_ASSOC);
        if (PEAR::isError($row)) {
            throw new Exception(__METHOD__ . " ERROR: " . $row->getDebugInfo());
        }
        return $row;
    }

    /**
     * 受付件数（ステータス毎）
     * @return Array
     * @throws Exception
     */
    public function count_accept_status()
    {
        if (empty($this->accept_emp_id)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }

        $sql = "
            SELECT
                (SELECT COUNT(*) FROM ldr_accept ac JOIN ldr_apply ap USING (ldr_apply_id) WHERE ac.accept_emp_id = :accept_emp_id AND ap.status=5 AND report_status IS NULL AND NOT ap.del_flg) AS st5_count,
                (SELECT COUNT(*) FROM ldr_accept ac JOIN ldr_apply ap USING (ldr_apply_id) JOIN ldr_appraiser ar ON ar.ldr_apply_id = ac.ldr_apply_id AND ar.emp_id = ac.accept_emp_id AND (ar.completed IS NULL OR NOT ar.completed) WHERE ac.accept_emp_id = :accept_emp_id AND ap.status=8 AND NOT ap.del_flg) AS st8_count,
--                (SELECT COUNT(*) FROM ldr_accept ac JOIN ldr_apply ap USING (ldr_apply_id) JOIN ldr_appraiser ar ON ar.ldr_apply_id = ac.ldr_apply_id AND ar.emp_id = ac.accept_emp_id AND ar.facilitator WHERE ac.accept_emp_id = :accept_emp_id AND ap.status=9 AND NOT ap.del_flg) AS st9_count,
                (SELECT COUNT(*) FROM ldr_accept ac JOIN ldr_apply ap USING (ldr_apply_id) WHERE ac.accept_emp_id = :accept_emp_id AND ap.status=9 AND ac.status=9 AND NOT ap.del_flg) AS st9_count,
                (SELECT COUNT(*) FROM ldr_accept ac JOIN ldr_apply ap USING (ldr_apply_id) JOIN ldr_committee com ON com.emp_id = ac.accept_emp_id AND com.auditor WHERE ac.accept_emp_id = :accept_emp_id AND ap.status=10 AND NOT ap.del_flg) AS st10_count,
                (SELECT COUNT(*) FROM ldr_accept ac JOIN ldr_apply ap USING (ldr_apply_id) JOIN ldr_auditor au ON au.ldr_apply_id = ac.ldr_apply_id AND au.emp_id = ac.accept_emp_id WHERE ac.accept_emp_id = :accept_emp_id AND ap.status=11 AND NOT ap.del_flg) AS st11_count,
                (SELECT COUNT(*) FROM ldr_accept ac JOIN ldr_apply ap USING (ldr_apply_id) JOIN ldr_committee com ON com.emp_id = ac.accept_emp_id WHERE ac.accept_emp_id = :accept_emp_id AND ap.status=12 AND NOT ap.del_flg) AS st12_count,
                (SELECT COUNT(*) FROM ldr_accept ac JOIN ldr_apply ap USING (ldr_apply_id) JOIN ldr_committee com ON com.emp_id = ac.accept_emp_id WHERE ac.accept_emp_id = :accept_emp_id AND ap.report_status=21 AND NOT ap.del_flg) AS st21_count,
                (SELECT COUNT(*) FROM ldr_accept ac JOIN ldr_apply ap USING (ldr_apply_id) JOIN ldr_reporter rp ON rp.ldr_apply_id = ac.ldr_apply_id AND rp.emp_id = ac.accept_emp_id WHERE ac.accept_emp_id = :accept_emp_id AND ap.report_status=22 AND NOT ap.del_flg) AS st22_count,
                (SELECT COUNT(*) FROM ldr_accept ac JOIN ldr_apply ap USING (ldr_apply_id) JOIN ldr_committee com ON com.emp_id = ac.accept_emp_id WHERE ac.accept_emp_id = :accept_emp_id AND ap.report_status=23 AND NOT ap.del_flg) AS st23_count
        ";
        $row = $this->db->extended->getRow($sql, null, array('accept_emp_id' => $this->accept_emp_id), array('text'), MDB2_FETCHMODE_ASSOC);
        if (PEAR::isError($row)) {
            throw new Exception(__METHOD__ . " ERROR: " . $row->getDebugInfo());
        }

        return $row;
    }

    /**
     * すべての申請を取得
     * @param type $st
     * @param type $pageCount
     * @param type $cond
     * @return type
     * @throws Exception
     */
    public function lists_apply_all($st = null, $pageCount = null, $cond = null)
    {
        if (empty($this->accept_emp_id)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }
        $ret = $this->apply_all_condition($cond);

        $sql = "
            SELECT
                ap.ldr_apply_id,
                ap.ladder_id,
                ap.emp_id,
                ap.level,
                f1.apply_date,
                status,
                report_status,
                result,
                guideline_revision,
                update1,
                update6,
                ap.update7,
                update8,
                update9,
                ap.updated_on,
                e.emp_lt_nm || ' ' || e.emp_ft_nm as emp_name,
                (SELECT COUNT(*) FROM ldr_comment WHERE ldr_apply_id=ap.ldr_apply_id) as comment,
                (SELECT COUNT(updated_on) FROM ldr_comment WHERE ldr_apply_id=ap.ldr_apply_id AND (updated_on > cr.timestamp OR cr.timestamp IS NULL)) as unread,
                coalesce(apr.ldr_apply_id, 0) as appraiser
            FROM ldr_apply ap
            JOIN empmst e ON ap.emp_id = e.emp_id
            LEFT JOIN ldr_form1 f1 USING (ldr_apply_id)
            LEFT JOIN ldr_appraiser apr ON apr.ldr_apply_id=ap.ldr_apply_id AND apr.emp_id= :emp_id
            LEFT JOIN ldr_comment_read cr ON cr.ldr_apply_id=ap.ldr_apply_id AND cr.emp_id = :emp_id
            WHERE
              NOT ap.del_flg
            ";
        $sql .= $ret['cond'];

        $order = "";
        foreach ($cond['order_by'] as $value) {
            $order.= empty($order) ? "order by " : ", ";
            $order.= $value;
        }
        $sql .= $order;

        if (isset($st) && isset($pageCount)) {
            $this->db->setLimit($pageCount, $st);
        }

        $types = array_merge(array('text'), $ret['types']);
        $data = array_merge(array('emp_id' => $this->accept_emp_id), $ret['data']);

        $sth = $this->db->prepare($sql, $types, MDB2_PREPARE_RESULT);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['appraiser_list'] = $this->appraiser()->lists($row['ldr_apply_id']);
            $this->apply_id = $row['ldr_apply_id'];
            $row['auditor_list'] = $this->auditor_list();
            $row['ladder'] = $this->ladder->find($row['ladder_id']);
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 結果設定
     * @param type $result
     * @throws Exception
     */
    public function update_result($result)
    {
        if (empty($this->apply_emp_id) || empty($this->apply_id)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }

        $this->db->beginNestedTransaction();

        $app_sth = $this->db->prepare("UPDATE ldr_apply SET result= :result WHERE ldr_apply_id= :ldr_apply_id", array('boolean', 'integer'), MDB2_PREPARE_MANIP);
        $res = $app_sth->execute(array(
            'result' => $result,
            'ldr_apply_id' => $this->apply_id,
        ));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }
        $app_sth->free();

        $this->db->completeNestedTransaction();
    }

    /**
     * ステータス変更
     * @param type $status
     */
    public function status_change($status, $accept = array())
    {
        if (empty($this->apply_emp_id) || empty($this->apply_id)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }

        $this->db->beginNestedTransaction();

        // 申請
        $app_sth = $this->db->prepare("UPDATE ldr_apply SET status= :status WHERE ldr_apply_id= :ldr_apply_id", array('integer', 'integer'), MDB2_PREPARE_MANIP);
        $res = $app_sth->execute(array(
            'ldr_apply_id' => $this->apply_id,
            'status' => $status,
        ));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }
        $app_sth->free();

        // 受付
        if (!empty($accept)) {
            $acc_sth = $this->db->prepare("SELECT update_ldr_accept(:ldr_apply_id, :accept_emp_id, :status)", array('integer', 'text', 'integer'), MDB2_PREPARE_MANIP);
            foreach ($accept as $accept_emp_id) {
                $res = $acc_sth->execute(array(
                    'ldr_apply_id' => $this->apply_id,
                    'accept_emp_id' => is_object($accept_emp_id) ? $accept_emp_id->emp_id() : $accept_emp_id,
                    'status' => $status,
                ));
                if (PEAR::isError($res)) {
                    $this->db->failNestedTransaction();
                    throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
                }
            }
            $acc_sth->free();
        }

        $this->db->completeNestedTransaction();

        // Log
        $this->log->log("ステータスを変更しました", "ldr_apply_id={$this->apply_id} status={$status}");
    }

    /**
     * レポートステータス変更
     * @param type $status
     */
    public function report_status_change($status, $accept = array())
    {
        if (empty($this->apply_emp_id) || empty($this->apply_id)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }

        $this->db->beginNestedTransaction();

        // 申請
        $app_sth = $this->db->prepare("UPDATE ldr_apply SET report_status= :status WHERE ldr_apply_id= :ldr_apply_id", array('integer', 'integer'), MDB2_PREPARE_MANIP);
        $res = $app_sth->execute(array(
            'ldr_apply_id' => $this->apply_id,
            'status' => $status,
        ));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }
        $app_sth->free();

        // 受付
        if (!empty($accept)) {
            $acc_sth = $this->db->prepare("SELECT update_ldr_accept(:ldr_apply_id, :accept_emp_id, :status)", array('integer', 'text', 'integer'), MDB2_PREPARE_MANIP);
            foreach ($accept as $accept_emp_id) {
                $res = $acc_sth->execute(array(
                    'ldr_apply_id' => $this->apply_id,
                    'accept_emp_id' => is_object($accept_emp_id) ? $accept_emp_id->emp_id() : $accept_emp_id,
                    'status' => $status,
                ));
                if (PEAR::isError($res)) {
                    $this->db->failNestedTransaction();
                    throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
                }
            }
            $acc_sth->free();
        }

        $this->db->completeNestedTransaction();

        // Log
        $this->log->log("レポートステータスを変更しました", "ldr_apply_id={$this->apply_id} status={$status}");
    }

    /**
     * 師長申込 申請
     * @param type $data
     * @throws Exception
     */
    public function apply_qualify($data)
    {
        if (empty($this->apply_emp_id) || empty($this->apply_id)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }

        $this->db->beginNestedTransaction();

        // 承認者
        $approver = $this->approver($this->apply_emp_id);

        // ステータスの変更
        $this->status_change(5, $approver);

        // コメント
        if (!empty($data['comment'])) {
            $this->comment()->save(array(
                'ldr_apply_id' => $this->apply_id,
                'status' => $data['status'],
                'from_emp_id' => $this->apply_emp_id,
                'comment' => $data['comment'],
            ));
        }

        $this->db->completeNestedTransaction();
    }

    /**
     * 師長申込 申請 取り下げ
     * @param type $data
     * @throws Exception
     */
    public function apply_qualify_cancel($data)
    {
        if (empty($this->apply_emp_id) || empty($this->accept_emp_id) || empty($this->apply_id)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }

        $this->db->beginNestedTransaction();

        // ステータスの変更
        $this->status_change(-2);
        $this->add_status_history(-2);

        // コメント
        if (!empty($data['comment'])) {
            $this->comment()->save(array(
                'ldr_apply_id' => $this->apply_id,
                'status' => $data['status'],
                'from_emp_id' => $this->apply_emp_id,
                'comment' => $data['comment'],
            ));
        }

        $this->db->completeNestedTransaction();
    }

    /**
     * 師長申込 受付
     * @param array $data
     * @return type
     */
    public function accept_qualify($data)
    {
        if (empty($this->apply_emp_id) || empty($this->accept_emp_id) || empty($this->apply_id)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }

        $this->db->beginNestedTransaction();
        $ldr = $this->ladder();

        // 評価者の登録
        if ($ldr['config']['appr'] === '2') {
            $data['ldr_apply_id'] = $this->apply_id;
            $this->appraiser()->create($data);
        }

        // 赤十字
        if ($ldr['config']['flow'] === '1') {
            // 評価者
            $appr = $this->appraiser()->find($this->apply_id);

            // ステータスの変更
            $this->status_change(8, $appr['emp_ids']);
        }

        // 標準
        else if ($ldr['config']['flow'] === '2') {
            // 他者評価者
            $appr = $this->appraiser()->find($this->apply_id);

            // 承認者
            $approver = $this->approver($this->apply_emp_id);

            // 総合評価者の登録
            $this->appraiser()->total_create($this->apply_id, $approver);

            // ステータスの変更
            if (!empty($appr['emp_ids'])) {
                $this->status_change(8, $appr['emp_ids']);
            }
            else {
                $this->status_change(9, $approver);
            }
        }

        // 順天堂
        else if ($ldr['config']['flow'] === '3') {
            // 他者評価者
            $appr = $this->appraiser()->find($this->apply_id);

            // ステータスの変更
            if (!empty($appr['emp_ids'])) {
                $this->status_change(8, $appr['emp_ids']);
            }

            // レベル1以下
            if ($this->level() <= 1) {
                // 自分を評価者に入れる
                $this->report()->reporter_save(array('id' => $this->apply_id, 'reporter' => array($this->accept_emp_id)));

                // ステータスの変更
                $this->report_status_change(22, array($this->accept_emp_id));
            }
            // レベル2以上
            else {
                // 認定委員会
                $comm = new Ldr_Committee();
                $committee = $comm->committee();

                // ステータスの変更
                $this->report_status_change(21, $committee);
            }
        }

        // コメント
        if (!empty($data['comment'])) {
            $this->comment()->save(array(
                'ldr_apply_id' => $this->apply_id,
                'status' => 7,
                'from_emp_id' => $this->accept_emp_id,
                'comment' => $data['comment'],
            ));
        }

        $this->db->completeNestedTransaction();
    }

    /**
     * 師長申込 受付 差し戻し
     * @param array $data
     * @return type
     */
    public function accept_qualify_back($data)
    {
        if (empty($this->apply_emp_id) || empty($this->accept_emp_id) || empty($this->apply_id)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }

        $this->db->beginNestedTransaction();

        // ステータスの変更
        $this->status_change(6, array($this->accept_emp_id));

        // コメント
        if (!empty($data['comment'])) {
            $this->comment()->save(array(
                'ldr_apply_id' => $this->apply_id,
                'status' => 5,
                'from_emp_id' => $this->accept_emp_id,
                'comment' => $data['comment'],
            ));
        }

        $this->db->completeNestedTransaction();
    }

    /**
     * 監査者設定
     * @param type $data
     * @throws Exception
     */
    public function accept_auditor($data)
    {
        if (empty($this->apply_emp_id) || empty($this->accept_emp_id) || empty($this->apply_id)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }

        $this->db->beginNestedTransaction();
        $this->auditor()->save($data);

        // ステータスの変更

        $this->status_change(11, $data['auditor']);

        $this->db->completeNestedTransaction();
    }

    /**
     * レポート評価者設定
     * @param type $data
     * @throws Exception
     */
    public function accept_reporter($data)
    {
        if (empty($this->apply_emp_id) || empty($this->accept_emp_id) || empty($this->apply_id)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }

        $this->db->beginNestedTransaction();
        $this->report()->reporter_save($data);

        // ステータスの変更
        $this->report_status_change(22, $data['reporter']);

        $this->db->completeNestedTransaction();
    }

    /**
     * 合否判定設定
     * @param type $data
     * @throws Exception
     */
    public function accept_result($data)
    {
        if (empty($this->apply_emp_id) || empty($this->accept_emp_id) || empty($this->apply_id)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }

        $this->db->beginNestedTransaction();

        // 合否結果の登録
        $result = ($data['result'] === "1") ? true : false;
        $this->update_result($result);

        // ステータスの変更 申請者へ通知
        $this->status_change(0);
        $this->add_status_history(0);

        // 職員情報を更新する
        if ($result) {
            $conf = new Ldr_Config();
            $employee = new Ldr_Employee($this->apply_emp_id);
            $user = array(
                'emp_id' => $this->apply_emp_id,
                'ladder_id' => $this->ladder_id(),
                'level' => $this->apply['level'],
                'date' => $data['result_date'],
                'hospital' => $conf->hospital_name(),
                'number' => $data['approved_number'],
            );
            $employee->save_level($user);
        }

        // Log
        $log = new Ldr_log('apply_result');
        $log->log("合否判定しました", "result={$result} emp_id={$this->apply_emp_id} level={$this->apply['level']} date={$data['result_date']} number={$data['approved_number']}");

        $this->db->completeNestedTransaction();
    }

    /**
     * ステータス履歴の追加
     * @param type $status
     * @throws Exception
     */
    public function add_status_history($status)
    {
        if (empty($this->accept_emp_id) || empty($this->apply_id)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }

        $this->db->beginNestedTransaction();

        $sth = $this->db->prepare("INSERT INTO ldr_accept_history (ldr_apply_id, accept_emp_id, status, created_on, updated_on) VALUES (:ldr_apply_id, :accept_emp_id, :status, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)", array('integer', 'text', 'integer'), MDB2_PREPARE_MANIP);
        $res = $sth->execute(array(
            'ldr_apply_id' => $this->apply_id,
            'accept_emp_id' => $this->accept_emp_id,
            'status' => $status,
        ));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }
        $sth->free();

        $this->db->completeNestedTransaction();
    }

    /**
     * ステータス履歴
     * @param type $apply_id
     */
    public function status_history()
    {
        if (empty($this->apply_emp_id) || empty($this->apply_id)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }

        if (!empty($this->status_history)) {
            return $this->status_history;
        }

        $sql = "
            SELECT
                ah.accept_emp_id as emp_id,
                e.emp_lt_nm || ' ' || e.emp_ft_nm as emp_name,
                ah.status,
                ah.created_on,
                ah.updated_on
            FROM ldr_accept_history ah
            JOIN empmst e ON e.emp_id=ah.accept_emp_id
            WHERE
               ah.ldr_apply_id = :ldr_apply_id
            ORDER BY ah.updated_on DESC
            ";

        $sth = $this->db->prepare($sql, array('integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array('ldr_apply_id' => $this->apply_id));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row;
        }
        $sth->free();
        $this->status_history = $lists;
        return $lists;
    }

    /**
     * 様式の更新日時取得
     * @return type
     */
    public function form_updated_on($apply_id)
    {
        if (empty($this->apply_emp_id)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }

        // 様式1,6〜9
        $sql1 = "SELECT update1, update6, update8, update9, update7 FROM ldr_apply WHERE ldr_apply_id = ?";
        $row1 = $this->db->extended->getRow(
            $sql1, null, array($apply_id), array('integer'), MDB2_FETCHMODE_ASSOC
        );

        // 様式2〜5
        $sql2 = "
            SELECT
                MAX(f2.updated_on) as update2,
                MAX(f3.updated_on) as update3,
                MAX(f4.updated_on) as update4,
                MAX(f5.updated_on) as update5,
                MAX(f_transfer.updated_on) as update_transfer,
                MAX(f_carrier.updated_on) as update_carrier,
                MAX(f_presentation.updated_on) as update_presentation,
                MAX(f_article.updated_on) as update_article,
                MAX(f_all.updated_on) as update_record
            FROM
            (
                SELECT MAX(updated_on) as updated_on FROM ldr_form2_1 WHERE emp_id = :emp_id AND NOT del_flg
                UNION
                SELECT MAX(updated_on) as updated_on FROM ldr_form2_2 WHERE emp_id = :emp_id AND NOT del_flg
                UNION
                SELECT MAX(updated_on) as updated_on FROM ldr_form2_2_3 WHERE emp_id = :emp_id AND NOT del_flg
            ) f2,
            (
                SELECT MAX(updated_on) as updated_on FROM ldr_form3 WHERE emp_id = :emp_id AND NOT del_flg
            ) f3,
            (
                SELECT MAX(updated_on) as updated_on FROM ldr_form4 WHERE emp_id = :emp_id AND NOT del_flg
            ) f4,
            (
                SELECT MAX(updated_on) as updated_on FROM ldr_form5_1 WHERE emp_id = :emp_id AND NOT del_flg
                UNION
                SELECT MAX(updated_on) as updated_on FROM ldr_form5_2 WHERE emp_id = :emp_id AND NOT del_flg
                UNION
                SELECT MAX(updated_on) as updated_on FROM ldr_form5_3 WHERE emp_id = :emp_id AND NOT del_flg
                UNION
                SELECT MAX(updated_on) as updated_on FROM ldr_form5_4 WHERE emp_id = :emp_id AND NOT del_flg
            ) f5,
            (
                SELECT MAX(updated_on) as updated_on FROM ldr_form_transfer WHERE emp_id = :emp_id AND NOT del_flg
            ) f_transfer,
            (
                SELECT MAX(updated_on) as updated_on FROM ldr_form_carrier WHERE emp_id = :emp_id AND NOT del_flg
            ) f_carrier,
            (
                SELECT MAX(updated_on) as updated_on FROM ldr_form_presentation WHERE emp_id = :emp_id AND NOT del_flg
            ) f_presentation,
            (
                SELECT MAX(updated_on) as updated_on FROM ldr_form_article WHERE emp_id = :emp_id AND NOT del_flg
            ) f_article,
            (
                SELECT MAX(updated_on) as updated_on FROM ldr_form2_1 WHERE emp_id = :emp_id AND NOT del_flg
                UNION
                SELECT MAX(updated_on) as updated_on FROM ldr_form2_2 WHERE emp_id = :emp_id AND NOT del_flg
                UNION
                SELECT MAX(updated_on) as updated_on FROM ldr_form2_2_3 WHERE emp_id = :emp_id AND NOT del_flg
                UNION
                SELECT MAX(updated_on) as updated_on FROM ldr_form3 WHERE emp_id = :emp_id AND NOT del_flg
                UNION
                SELECT MAX(updated_on) as updated_on FROM ldr_form4 WHERE emp_id = :emp_id AND NOT del_flg
                UNION
                SELECT MAX(updated_on) as updated_on FROM ldr_form5_1 WHERE emp_id = :emp_id AND NOT del_flg
                UNION
                SELECT MAX(updated_on) as updated_on FROM ldr_form5_2 WHERE emp_id = :emp_id AND NOT del_flg
                UNION
                SELECT MAX(updated_on) as updated_on FROM ldr_form5_3 WHERE emp_id = :emp_id AND NOT del_flg
                UNION
                SELECT MAX(updated_on) as updated_on FROM ldr_form5_4 WHERE emp_id = :emp_id AND NOT del_flg
                UNION
                SELECT MAX(updated_on) as updated_on FROM ldr_form_carrier WHERE emp_id = :emp_id AND NOT del_flg
                UNION
                SELECT MAX(updated_on) as updated_on FROM ldr_form_presentation WHERE emp_id = :emp_id AND NOT del_flg
                UNION
                SELECT MAX(updated_on) as updated_on FROM ldr_form_presentation WHERE emp_id = :emp_id AND NOT del_flg
                UNION
                SELECT MAX(updated_on) as updated_on FROM ldr_form_article WHERE emp_id = :emp_id AND NOT del_flg
            ) f_all
        ";
        $row2 = $this->db->extended->getRow(
            $sql2, null, array('emp_id' => $this->apply_emp_id), array('text'), MDB2_FETCHMODE_ASSOC
        );

        return array_merge($row1, $row2);
    }

    /**
     * 評価者一覧
     */
    public function appraiser_lists()
    {
        if (empty($this->apply_id)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }
        return $this->appraiser()->lists($this->apply_id);
    }

    /**
     * 職員IDが司会者に該当するか判定
     * @param type $emp_id
     */
    public function is_facilitator($emp_id)
    {
        if (empty($this->apply_id)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }
        return $this->appraiser()->is_facilitator($this->apply_id, $emp_id);
    }

    /**
     * 職員IDが評価者に該当するか判定
     * @param type $emp_id
     */
    public function is_appraiser($emp_id)
    {
        if (empty($this->apply_id)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }

        if ($this->appraiser()->is_facilitator($this->apply_id, $emp_id)) {
            return true;
        }

        return $this->appraiser()->is_appraiser($this->apply_id, $emp_id);
    }

    /**
     * 職員IDが総合評価者に該当するか判定
     * @param type $emp_id
     */
    public function is_total_appraiser($emp_id)
    {
        return $this->appraiser()->is_total_appraiser($this->apply_id, $emp_id);
    }

    /**
     * 評価済み判定
     * @param type $emp_id
     */
    public function is_appraiser_completed($emp_id)
    {
        if (empty($this->apply_id)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }
        return $this->appraiser()->is_completed($this->apply_id, $emp_id);
    }

    /**
     * 総合評価 評価済み判定
     * @param type $emp_id
     */
    public function is_total_completed()
    {
        if (empty($this->apply_id)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }
        return $this->appraiser()->is_total_completed($this->apply_id);
    }

    /**
     * 様式7-2評価リストの取得
     * @param type $level
     * @param type $group1
     * @return type
     * @throws Exception
     */
    public function assessment_list()
    {
        if (empty($this->apply_emp_id) || empty($this->accept_emp_id) || empty($this->apply_id)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }

        return $this->form7()->assessment_lists($this->apply_id, $this->level(), $this->apply_emp_id, $this->accept_emp_id);
    }

    /**
     * 様式7-2評価リストの取得(申請者/評価者)
     * @return type
     * @throws Exception
     */
    public function assessment_list_all()
    {
        if (empty($this->apply_emp_id) || empty($this->accept_emp_id) || empty($this->apply_id)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }

        return $this->form7()->assessment_lists_all($this->apply_id, $this->level());
    }

    /**
     * 監査者一覧
     * @return type
     * @throws Exception
     */
    public function auditor_list()
    {

        if ($this->apply_id > 0) {

            $this->db->beginNestedTransaction();
            $sql = "SELECT (e.emp_lt_nm|| ' ' || e.emp_ft_nm) as emp_name
              FROM ldr_auditor a JOIN empmst e
              USING (emp_id)  WHERE ldr_apply_id =:ldr_apply_id";

            $this->db->completeNestedTransaction();
            $sth = $this->db->prepare($sql, array('integer'), MDB2_PREPARE_RESULT);
            $res = $sth->execute(array('ldr_apply_id' => $this->apply_id));
            if (PEAR::isError($res)) {
                throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
            }
            $lists = array();
            while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                $lists[] = $row['emp_name'];
            }
            $sth->free();
            $this->db->completeNestedTransaction();
            return $lists;
        }
    }

    /**
     * 自己評価インポート
     * @param type $data
     * @throws Exception
     */
    public function import_form7($revision)
    {
        if (empty($this->apply_emp_id) || empty($this->apply_id)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }
        $this->form7()->import($this->apply_id, $this->apply['level'], $this->apply_emp_id, $revision);
        $this->updated_date('7_2');
    }

    /**
     * レベルの取得
     * @return type
     * @throws Exception
     */
    public function level()
    {
        if (empty($this->apply)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }
        return $this->apply['level'];
    }

    /**
     * 指標：版の取得
     * @return type
     * @throws Exception
     */
    public function guideline_revision()
    {
        if (empty($this->apply)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }
        return $this->apply['guideline_revision'];
    }

    /**
     * レベル(テキスト)の取得
     * @return type
     * @throws Exception
     */
    public function level_text()
    {
        if (empty($this->apply)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }
        if ($this->apply['level']) {
            return Ldr_Const::get_roman_num($this->apply['level']);
        }

        $ldr = $this->ladder();
        return $ldr['config']['level0'];
    }

    /**
     * validation
     * @param type $type 種別
     * @param type $data 更新データ
     * @return type
     */
    public function validate($type, $data)
    {
        switch ($type) {
            case 'form8':
                return $this->form8()->validate($data);
            case 'form9':
                return $this->form9()->validate($data);
            default:
                return array();
        }
    }

    /**
     * データの取得(一件)
     * @param type $type
     * @param type $st
     * @param type $pageCount
     * @param type $cond
     * @return type
     */
    public function find($type, $id)
    {
        switch ($type) {
            case 'form8':
                return $this->form8()->find($id);
            case 'form9':
                return $this->form9()->find($id);
        }
    }

    /**
     * 保存
     * @param type $type 種別
     * @param type $data 更新データ
     * @return type
     */
    public function save($type, $data)
    {
        switch ($type) {
            case 'form8':
                return $this->form8()->save($data);
            case 'form9':
                return $this->form9()->save($data);
        }
    }

    /**
     * ステータスの取得
     */
    public function get_status()
    {
        return empty($this->apply) ? '' : $this->apply['status'];
    }

    /**
     * レポートステータスの取得
     */
    public function get_report_status()
    {
        return empty($this->apply) ? '' : $this->apply['report_status'];
    }

    /**
     * 監査者取得
     * @return type
     */
    public function get_auditor()
    {
        $this->_auditor = $this->auditor()->find($this->apply_id);

        return $this->_auditor;
    }

    /**
     * レポート評価者取得
     * @return type
     */
    public function reporter()
    {
        $this->_reporter = $this->report()->reporter_find();

        return $this->_reporter;
    }

    /**
     * レポート評価者判定
     * @return type
     */
    public function is_reporter($emp_id)
    {
        if (empty($this->_reporter)) {
            $this->reporter();
        }

        if (in_array($emp_id, array_keys($this->_reporter))) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * 監査者判定
     * @return type
     */
    public function is_auditor($emp_id)
    {
        if (empty($this->_auditor)) {
            $this->get_auditor();
        }

        if (in_array($emp_id, $this->_auditor)) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * 申請強制削除
     * @param type $data
     * @throws Exception
     */
    public function remove()
    {
        if (empty($this->apply_id)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }

        $this->db->beginNestedTransaction();

        $app_sth = $this->db->prepare("UPDATE ldr_apply SET del_flg='t' WHERE ldr_apply_id= :ldr_apply_id", array('integer'), MDB2_PREPARE_MANIP);
        $res = $app_sth->execute(array(
            'ldr_apply_id' => $this->apply_id,
        ));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }

        $this->db->completeNestedTransaction();
        $this->log->log("削除しました", "id={$this->apply_id}");
    }

    /**
     * 取り下げられた申請の復旧
     * @param type $data
     * @throws Exception
     */
    public function withdraw_recover()
    {
        if (empty($this->apply_id)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }

        // ステータスの変更
        $this->status_change(4);

        $this->log->log("復旧しました", "id={$this->apply_id}");
    }

    /**
     * レポート評価の保存
     * @param type $data
     * @return type
     */
    public function save_report_assessment($data, $mode = null)
    {
        $data['emp_id'] = $this->accept_emp_id;
        $this->report()->save_report_comment($data, $mode);
        $this->report()->save_report_assessment($data, $mode);
    }

    /**
     * レポート評価確認の保存
     * @param type $data
     * @return type
     */
    public function save_report_assessment_admin($data, $mode = null)
    {
        $this->report()->save_report_admin_comment($data, $mode);
    }

    /**
     * レポート評価取得
     * @return type
     */
    public function report_assessment()
    {
        if (empty($this->apply_id)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }

        return $this->report()->assessment();
    }

    /**
     * レポート評価validate
     * @return type
     */
    public function validate_report_assessment()
    {
        if (empty($this->apply_id)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }

        return $this->report()->validate_report_assessment();
    }

    /**
     * レポート評価validate admin
     * @return type
     */
    public function validate_report_assessment_admin()
    {
        if (empty($this->apply_id)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }

        return $this->report()->validate_report_assessment_admin();
    }

    /**
     * レポート評価完了
     * @return
     */
    public function update_report_completed()
    {
        // 完了フラグ更新
        $this->report()->assessment_completed();

        // レベル1以下
        if ($this->level() <= 1) {
            // レポート評価確認完了
            $this->update_report_admin_completed();
        }
        // レベル2以上
        else {
            // 認定委員会
            $comm = new Ldr_Committee();
            $committee = $comm->committee();

            // ステータスの変更
            $this->report_status_change(23, $committee);
        }
    }

    /**
     * レポート評価確認完了
     * @return
     */
    public function update_report_admin_completed()
    {
        // 完了フラグ更新
        $this->report()->assessment_admin_completed();

        // 承認者
        $approver = $this->approver($this->apply_emp_id);

        // 総合評価者の登録
        $this->appraiser()->total_create($this->apply_id, $approver);

        // ステータスの変更
        $this->status_change(9, $approver);
        $this->report_status_change(0);
    }

    /**
     * Objection Object
     */
    public function objection()
    {
        if (empty($this->objection)) {
            $this->objection = new Ldr_Objection($this->apply_id);
        }
        return $this->objection;
    }

    /**
     * 異議申し立て取得
     * @throws Exception
     */
    public function find_objection()
    {
        if (empty($this->apply_id)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }

        return $this->objection()->find();
    }

    /**
     * 異議申し立て
     * @param type $data
     * @throws Exception
     */
    public function apply_objection($data)
    {
        if (empty($this->apply_emp_id) || empty($this->apply_id)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }

        $this->objection()->objection_save($data);
    }

    /**
     * 異議申し立て受理
     * @param type $data
     * @throws Exception
     */
    public function accept_objection($data)
    {
        if (empty($this->apply_id)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }

        $this->objection()->admin_save($data);
    }

    /**
     * 異議申し立て一覧を取得
     * @param type $st
     * @param type $pageCount
     * @param type $cond
     * @return type
     */
    public function lists_objection($st = null, $pageCount = null, $cond = null)
    {
        $sql = "
            SELECT
                obj.ldr_apply_id,
                ap.status,
                ap.report_status,
                ap.emp_id,
                ap.ladder_id,
                ap.level,
                ap.result,
                f1.apply_date,
                e.emp_lt_nm || ' ' || e.emp_ft_nm as emp_name,
                obj.*
            FROM ldr_apply_objection obj
            JOIN ldr_apply ap USING (ldr_apply_id)
            JOIN ldr_form1 f1 USING (ldr_apply_id)
            JOIN empmst e ON ap.emp_id = e.emp_id
                WHERE NOT ap.del_flg
        ";

        if ($cond['unread']) {
            $sql .= 'AND NOT read';
        }

        $sql .= " ORDER BY obj.updated_on DESC, ap.updated_on DESC";

        if (isset($st) && isset($pageCount)) {
            $this->db->setLimit($pageCount, $st);
        }

        $sth = $this->db->prepare($sql, null, MDB2_PREPARE_RESULT);
        $res = $sth->execute();
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['appraiser_list'] = $this->appraiser()->lists($row['ldr_apply_id']);
            $this->apply_id = $row['ldr_apply_id'];
            $row['ladder'] = $this->ladder->find($row['ladder_id']);
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 異議申し立て件数を取得
     * @param type $st
     * @param type $pageCount
     * @param type $cond
     * @return type
     */
    public function count_objection($cond = null)
    {
        $sql = "
            SELECT
                COUNT(*)
            FROM ldr_apply_objection obj
            JOIN ldr_apply ap USING (ldr_apply_id)
                WHERE NOT ap.del_flg
        ";

        if ($cond['unread']) {
            $sql .= 'AND NOT read';
        }

        $count = $this->db->extended->getOne($sql);
        if (PEAR::isError($count)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $count->getDebugInfo());
        }

        return $count;
    }

}
