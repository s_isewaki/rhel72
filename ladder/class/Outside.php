<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'ladder/class/Form22.php';
require_once 'ladder/class/Log.php';
require_once 'ladder/class/Config.php';

/**
 * 院外研修
 */
class Ldr_Outside extends Model
{

    var $log;
    var $emp_id;
    var $class_sql;

    /**
     * コンストラクタ
     */
    public function Ldr_Outside($emp_id)
    {
        parent::connectDB();
        $this->emp_id = $emp_id;
        $this->log = new Ldr_log('outside');

        $conf = new Ldr_Config();
        $this->class_sql = Ldr_Util::class_full_name_sql($conf->view_class());
    }

    /**
     * 院外研修申込validate
     * @param type $data
     * @return string
     */
    public function validate_apply($data)
    {
        $error = array();

        if (Validation::is_empty($data['rd_organizer'])) {
            $error['organizer'][] = '主催が設定されていません';
        }
        else if ($data['rd_organizer'] === '3' && Validation::is_empty($data['organizer'])) {
            $error['organizer'][] = '主催が設定されていません';
        }

        if (!$data['mst_only'] && Validation::is_empty($data['type'])) {
            $error['type'][] = '区分が設定されていません';
        }

        if (!$data['mst_only'] && Validation::is_empty($data['job_year'])) {
            $error['job_year'][] = '経験年数が入力されていません';
        }
        // 期間
        if (Validation::is_empty($data['start_date']) || Validation::is_empty($data['last_date'])) {
            $error['date'][] = '期間が入力されていません';
        }
        else {
            if (!Validation::is_date($data['start_date'])) {
                $error['date'][] = '期間の開始日が正しくありません';
            }
            if (!Validation::is_date($data['last_date'])) {
                $error['date'][] = '期間の開始日が正しくありません';
            }
        }
        if (!Validation::is_empty($data['last_date'])) {
            if (!Validation::is_date($data['last_date'])) {
                $error['date'][] = '期間の終了日が正しくありません';
            }
            else if (empty($error['date']) and strtotime($data['start_date']) > strtotime($data['last_date'])) {
                $error['date'][] = '開始日より終了日が古い日付です';
            }
        }

        if (Validation::is_empty($data['no'])) {
            $error['no'][] = '研修番号が入力されていません';
        }
        else {
            if (mb_strlen($data['no'], 'EUC-JP') > 20) {
                $error['no'][] = '研修番号は20文字以内で入力してください';
            }
        }

        if (Validation::is_empty($data['contents'])) {
            $error['contents'][] = '研修名が入力されていません';
        }
        else {
            if (mb_strlen($data['contents'], 'EUC-JP') > 200) {
                $error['contents'][] = '研修名は200文字以内で入力してください';
            }
        }


        return $error;
    }

    /**
     * 報告のvalidate
     * @param type $data
     * @return string
     */
    public function validate_report($data)
    {
        $error = array();

        if (Validation::is_empty($data['report'])) {
            $error['report'][] = '報告が設定されていません';
        }
        else if ($data['report'] === '2' && Validation::is_empty($data['learning'])) {
            $error['learning'][] = '出張報告なしの場合は学んだことを入力してください';
        }

        return $error;
    }

    /**
     * 1件取得(マスタ)
     * @param Integer $id
     * @return Array
     */
    public function find_mst($id)
    {
        if (is_null($id)) {
            return array();
        }

        $sql = "SELECT
                    (SELECT
                        count(*)
                     FROM ldr_trn_outside
                     WHERE NOT del_flg
                     AND ldr_trn_outside.outside_id = ldr_mst_outside.outside_id) AS count,
                    outside_id,
                    organizer,
                    start_date,
                    last_date,
                    pref,
                    place,
                    no,
                    contents,
                    update_emp_id,
                    commit_flg
                FROM ldr_mst_outside
                WHERE
                outside_id= :outside_id AND NOT del_flg";
        $row = $this->db->extended->getRow(
            $sql, null, array('outside_id' => $id), array('integer'), MDB2_FETCHMODE_ASSOC
        );
        if (PEAR::isError($row)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $row->getDebugInfo());
        }
        return $row;
    }

    /**
     * 保存
     * @param type $data
     * @throws Exception
     */
    public function save($data)
    {
        $this->db->beginNestedTransaction();

        $data['update_mst'] = ($data['mst_read'] === 'readonly') ? false : true;
        $data['update_trn'] = (isset($data['mst_only'])) ? false : true;

        $data['emp_id'] = $this->emp_id;
        // UPDATE or INSERT
        $sql = "SELECT update_ldr_outside (:outside_id,:emp_id,:organizer,:start_date,:last_date,:pref,:place,:no,:contents,:training_id,:type,:job_year,:update_mst,:update_trn)";
        $types = array('integer', 'text', 'text', 'date', 'date', 'text', 'text', 'text', 'text', 'integer', 'integer', 'integer', 'boolean', 'boolean');
        $sth = $this->db->prepare($sql, $types, MDB2_PREPARE_MANIP);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }
        if ($data['update_mst']) {
            $outside_id = empty($data['outside_id']) ? $this->db->lastInsertID('ldr_mst_outside', 'outside_id') : $data['outside_id'];
            $msg .= "outside_id={$outside_id} ";
        }
        if ($data['update_trn']) {
            $training_id = empty($data['training_id']) ? $this->db->lastInsertID('ldr_trn_outside', 'training_id') : $data['training_id'];
            $msg .= "training_id={$training_id} ";
        }

        $sth->free();
        $this->db->completeNestedTransaction();
        $this->log->log("保存しました", $msg);
    }

    /**
     * 研修講座(マスタ)検索条件
     * @param type $cond
     * @return type
     */
    protected function make_mst_condition($cond = null)
    {

        $cond_ret = "";
        $types = array();
        $data = array();
        if (empty($cond['mst'])) {
            // 設定されていないときに条件を設定
            $cond_ret .= " AND to_char(start_date ,'YYYY-MM') >= :valid_m ";
            $types[] = 'text';
            $data['valid_m'] = date('Y-m');
        }
        if (!empty($cond['srh_start_date'])) {
            $cond_ret .= " AND ( :srh_start_date <= mst.start_date) ";
            $types[] = 'text';
            $data['srh_start_date'] = $cond['srh_start_date'];
        }
        if (!empty($cond['srh_last_date'])) {
            $cond_ret .= " AND (mst.last_date <= :srh_last_date ) ";
            $types[] = 'text';
            $data['srh_last_date'] = $cond['srh_last_date'];
        }
        $ret['cond'] = $cond_ret;
        $ret['types'] = $types;
        $ret['data'] = $data;
        return $ret;
    }

    /**
     * データ数を取得します(マスタ)
     * @return type
     */
    public function count_mst($cond = null)
    {
        $ret = $this->make_mst_condition($cond);
        $sql_cond = $ret['cond'];
        $types = array_merge(array('text'), $ret['types']);
        $data = array_merge(array('emp_id' => $this->emp_id), $ret['data']);

        $sql = "SELECT count(*)
                FROM ldr_mst_outside mst
                WHERE
                    NOT del_flg
                    $sql_cond
                ";
        $sth = $this->db->prepare($sql, $types, MDB2_PREPARE_RESULT);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }
        $count = 0;
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $count = $row["count"];
        }
        $sth->free();
        return $count;
    }

    /**
     * 一覧
     * @param type $st
     * @param type $pageCount
     * @param type $cond
     * @return type
     */
    public function lists_mst($st = null, $pageCount = null, $cond = null)
    {

        $ret = $this->make_mst_condition($cond);
        $sql_cond = $ret['cond'];
        $types = array_merge(array('text'), $ret['types']);
        $data = array_merge(array('emp_id' => $this->emp_id), $ret['data']);

        $sql = "SELECT
                    outside_id, organizer, start_date, last_date, pref, place, no, contents, commit_flg, update_emp_id,
                    (SELECT count(*) FROM ldr_trn_outside trn WHERE trn.outside_id=mst.outside_id AND NOT del_flg) as count_all,
                    (SELECT count(*) FROM ldr_trn_outside trn WHERE trn.outside_id=mst.outside_id AND trn.status >= 0 AND NOT del_flg) as count,
                    (SELECT count(*) FROM ldr_trn_outside trn WHERE trn.outside_id=mst.outside_id AND trn.status >= 0 AND trn.emp_id=:emp_id AND NOT del_flg) as applied
                FROM ldr_mst_outside mst
                WHERE
                    NOT del_flg
                    $sql_cond
                ";
        $cond['order_by'][] = "start_date asc";
        $cond['order_by'][] = "outside_id asc";
        $order = "";
        foreach ($cond['order_by'] as $value) {
            $order.= empty($order) ? "order by " : ", ";
            $order.= $value;
        }
        $sql .= $order;

        if (isset($st) && isset($pageCount)) {
            $this->db->setLimit($pageCount, $st);
        }

        $sth = $this->db->prepare($sql, $types, MDB2_PREPARE_RESULT);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['closing'] = Ldr_Util::outside_closing_date($row['start_date']);
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 一覧の件数を取得します
     * @param type $cond
     * @return type
     */
    public function count($cond = null)
    {
        $sql = "SELECT
                    count(*)
                FROM ldr_trn_outside trn
                JOIN ldr_mst_outside mst ON NOT mst.del_flg AND trn.outside_id = mst.outside_id
                WHERE
                    NOT trn.del_flg
                    AND emp_id = :emp_id
                ";
        $types = array('text');
        $data = array('emp_id' => $this->emp_id);

        $count = $this->db->extended->getOne($sql, null, $data, $types);
        if (PEAR::isError($count)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $count->getDebugInfo());
        }

        return $count;
    }

    /**
     * 一覧
     * @param type $st
     * @param type $pageCount
     * @param type $cond
     * @return type
     */
    public function lists($st = null, $pageCount = null, $cond = null)
    {
        $sql = "SELECT
                    emp_id,status,
                    trn.outside_id,
                    type,job_year,
                    report,
                    learning,
                    mst.organizer,
                    mst.pref,
                    mst.place,
                    mst.start_date,
                    mst.last_date,
                    mst.no,
                    mst.contents,
                    trn.training_id,
                    trn.updated_on
                FROM ldr_trn_outside trn
                JOIN ldr_mst_outside mst ON NOT mst.del_flg AND trn.outside_id = mst.outside_id
                WHERE
                    NOT trn.del_flg
                    AND emp_id = :emp_id
                ";
        $cond['order_by'][] = "trn.updated_on desc";
        $order = "";
        foreach ($cond['order_by'] as $value) {
            $order.= empty($order) ? "ORDER BY " : ", ";
            $order.= $value;
        }
        $sql .= $order;
        if (isset($st) && isset($pageCount)) {
            $this->db->setLimit($pageCount, $st);
        }

        $types = array('text');
        $data = array('emp_id' => $this->emp_id);
        $sth = $this->db->prepare($sql, $types, MDB2_PREPARE_RESULT);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 一件取得
     * @param type $id training_id
     * @return type
     * @throws Exception
     */
    public function find($id)
    {
        if (is_null($id)) {
            return array();
        }

        $sql = "SELECT
                    training_id,
                    emp_id,
                    status,
                    type,
                    job_year,
                    report,
                    learning,
                    trn.outside_id,
                    mst.organizer,
                    mst.start_date,
                    mst.last_date,
                    mst.pref,
                    mst.place,
                    mst.no,
                    mst.contents,
                    mst.update_emp_id,
                    mst.commit_flg
                FROM ldr_trn_outside trn
                JOIN ldr_mst_outside mst ON NOT mst.del_flg AND trn.outside_id = mst.outside_id
                WHERE
                    NOT trn.del_flg
                    AND trn.training_id= :training_id";
        $row = $this->db->extended->getRow(
            $sql, null, array('training_id' => $id), array('integer'), MDB2_FETCHMODE_ASSOC
        );
        if (PEAR::isError($row)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $row->getDebugInfo());
        }
        return $row;
    }

    /**
     * 申請の取下げ
     * @param type $id    training_id
     */
    public function apply_cancel($id)
    {
        $this->status_change($id, "-1");
        $this->log->log("申請を取り下げました", "training_id={$id}");
    }

    /**
     * 承認する
     * @param type $id
     */
    public function save_approve($data)
    {
        $id = $data['training_id'];
        $this->status_change($id, "2");
        if (is_array($id)) {
            $this->log->log("申請を承認しました", "training_id=[" . implode(" ", $id) . "]");
        }
        else {
            $this->log->log("申請を承認しました", "training_id={$id}");
        }
    }

    /**
     * 申請を取り下げる
     * @param type $data
     */
    public function cancel_approve($data)
    {
        $id = $data['training_id'];
        $this->status_change($id, "-2");
        if (is_array($id)) {
            $this->log->log("申請を取り下げました", "training_id=[" . implode(" ", $id) . "]");
        }
        else {
            $this->log->log("申請を取り下げました", "training_id={$id}");
        }
    }

    /**
     * ステータス更新
     * @param type $id        training_id
     * @param type $status    status
     * @throws Exception
     */
    private function status_change($id, $status)
    {
        $this->db->beginNestedTransaction();
        if (is_array($id)) {
            $holder = implode(',', array_fill(0, count($id), '?'));
            $sql_cond = " training_id IN($holder) ";
            $data = array_merge(array($status), $id);
            $types = array_merge(array('integer'), array_fill(0, count($id), 'integer'));
        }
        else {
            $sql_cond = "training_id= ?";
            $types = array('integer', 'integer');
            $data = array($status, $id);
        }

        $sql = "UPDATE ldr_trn_outside
                SET
                    status = ?
                WHERE
                    $sql_cond
                    AND NOT del_flg";
        $sth = $this->db->prepare($sql, $types, MDB2_PREPARE_MANIP);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $sth->free();

        $this->db->completeNestedTransaction();
    }

    /**
     * 院外研修承認（一覧）の検索条件を作成
     * @param type $cond
     * @return type
     */
    protected function make_approve_condition($cond = null)
    {
        $cond_ret = "";
        $types = array();
        $data = array();
        if (!empty($cond)) {
            if (!empty($cond['srh_keyword'])) {
                $keywords = preg_split(Ldr_Const::SRH_DELIMITERS, $cond['srh_keyword'], -1, PREG_SPLIT_NO_EMPTY);
                $cond_key.= "AND ( ";
                foreach ($keywords as $key => $value) {
                    if ($key != 0) {
                        $cond_key.= " AND ";
                    }

                    $label_key = "key" . $key;
                    $cond_key.= "(mst.organizer || ' ' ||mst.contents|| ' ' || mst.no || ' ' || e.emp_lt_nm || ' ' || e.emp_ft_nm ||' ' || e.emp_kn_lt_nm || ' ' || e.emp_kn_ft_nm ) ILIKE :" . $label_key . " ";
                    $types[] = 'text';
                    $data[$label_key] = "%" . $value . "%";
                }
                $cond_key.= ") ";
                $cond_ret .= $cond_key;
            }
            if (!empty($cond['srh_start_date'])) {
                $cond_ret .= " AND ( :srh_start_date <= mst.start_date) ";
                $types[] = 'text';
                $data['srh_start_date'] = $cond['srh_start_date'];
            }
            if (!empty($cond['srh_last_date'])) {
                $cond_ret .= " AND (mst.last_date <= :srh_last_date ) ";
                $types[] = 'text';
                $data['srh_last_date'] = $cond['srh_last_date'];
            }
            if (!empty($cond['srh_outside'])) {
                $cond_ret .= " AND trn.outside_id = :outside_id ";
                $types[] = 'integer';
                $data['outside_id'] = $cond['srh_outside'];
            }
            if (!empty($cond['srh_nonapprove'])) {
                $cond_ret .= " AND trn.status = 1 ";
            }
        }
        $ret['cond'] = $cond_ret;
        $ret['types'] = $types;
        $ret['data'] = $data;
        return $ret;
    }

    /**
     * 承認待ちの件数
     * @param type $cond
     * @return type
     */
    public function count_approve($cond = null)
    {

        $ret = $this->make_approve_condition($cond);

        $sql = "SELECT
                    count(*)
                FROM ldr_trn_outside trn
                JOIN empmst e USING(emp_id)
                JOIN ldr_approver app
                    ON app.emp_id = :emp_id
                    AND app.class_id = e.emp_class
                    AND app.atrb_id = e.emp_attribute
                    AND app.dept_id = e.emp_dept
                    AND COALESCE(app.room_id,0) = COALESCE(e.emp_room,0)
                JOIN ldr_mst_outside mst ON mst.outside_id=trn.outside_id AND NOT mst.del_flg
                WHERE
                    NOT trn.del_flg
                    AND status != '-1'
                    ";
        $sql.= $ret['cond'];
        $types = array_merge(array('text'), $ret['types']);
        $data = array_merge(array('emp_id' => $this->emp_id), $ret['data']);
        $count = $this->db->extended->getOne($sql, null, $data, $types);
        if (PEAR::isError($count)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $count->getDebugInfo());
        }

        return $count;
    }

    /**
     * 承認待ちの一覧
     * @param type $st
     * @param type $pageCount
     * @param array $cond
     * @return type
     * @throws Exception
     */
    public function lists_approve($st = null, $pageCount = null, $cond = null)
    {

        $ret = $this->make_approve_condition($cond);

        $sql = "SELECT
                    trn.emp_id,
                    e.emp_lt_nm || ' ' || e.emp_ft_nm as name,
                    training_id,
                    status,
                    type,
                    job_year,
                    trn.outside_id,
                    mst.organizer,
                    mst.contents,
                    mst.start_date,
                    mst.last_date,
                    mst.pref,
                    mst.place,
                    mst.no,
                    mst.commit_flg
                FROM ldr_trn_outside trn
                JOIN empmst e USING(emp_id)
                JOIN ldr_approver app
                    ON app.emp_id = :emp_id
                    AND app.class_id = e.emp_class
                    AND app.atrb_id = e.emp_attribute
                    AND app.dept_id = e.emp_dept
                    AND COALESCE(app.room_id,0) = COALESCE(e.emp_room,0)
                JOIN ldr_mst_outside mst ON mst.outside_id=trn.outside_id AND NOT mst.del_flg
                WHERE
                    NOT trn.del_flg
                    AND status != '-1'
                ";
        $sql.= $ret['cond'];
        $types = array_merge(array('text'), $ret['types']);
        $data = array_merge(array('emp_id' => $this->emp_id), $ret['data']);
        $cond['order_by'][] = "mst.start_date asc";
        $cond['order_by'][] = "outside_id asc";
        $cond['order_by'][] = "training_id";
        $order = "";
        foreach ($cond['order_by'] as $value) {
            $order.= empty($order) ? "ORDER BY " : ", ";
            $order.= $value;
        }
        $sql .= $order;
        if (isset($st) && isset($pageCount)) {
            $this->db->setLimit($pageCount, $st);
        }

        $sth = $this->db->prepare($sql, $types, MDB2_PREPARE_RESULT);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        $date = date('Y-m-d');
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['closing'] = Ldr_Util::outside_closing_date($row['start_date']);
            $row['close_status'] = ($date > $row['closing']) ? -1 : 1;
            $row['status_text'] = Ldr_Const::get_training_status($row['status']);
            $row['type_text'] = Ldr_Const::get_outside_type($row['type']);

            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 受付一覧検索条件
     * @param type $cond
     * @return type
     */
    protected function make_condition_accept($cond = null)
    {
        $cond_ret = "";
        $types = array();
        $data = array();
        if (!empty($cond)) {
            if (!empty($cond['srh_keyword'])) {
                $keywords = preg_split(Ldr_Const::SRH_DELIMITERS, $cond['srh_keyword'], -1, PREG_SPLIT_NO_EMPTY);

                $cond_key.= "AND ( ";
                foreach ($keywords as $key => $value) {
                    if ($key != 0) {
                        $cond_key.= " AND ";
                    }

                    $label_key = "key" . $key;
                    $cond_key.= "(mt.organizer || ' ' || ' ' || mt.no || ' ' || mt.contents) ILIKE :" . $label_key . " ";
                    $types[] = 'text';
                    $data[$label_key] = "%" . $value . "%";
                }
                $cond_key.= ") ";
                $cond_ret .= $cond_key;
            }
            if (!empty($cond['srh_start_date'])) {
                $cond_ret .= " AND ( :srh_start_date <= mt.start_date) ";
                $types[] = 'text';
                $data['srh_start_date'] = $cond['srh_start_date'];
            }
            if (!empty($cond['srh_last_date'])) {
                $cond_ret .= " AND (mt.last_date <= :srh_last_date ) ";
                $types[] = 'text';
                $data['srh_last_date'] = $cond['srh_last_date'];
            }
            if (!empty($cond['srh_nonaccept'])) {
                $cond_ret .= " AND (SELECT count(*) FROM ldr_trn_outside trn WHERE trn.outside_id=mt.outside_id AND trn.status = 2 AND NOT del_flg) > 0 ";
            }
            if (!empty($cond['accepted'])) {
                $cond_ret .= " AND trn.status >= '3' ";
            }
        }
        $ret['cond'] = $cond_ret;
        $ret['types'] = $types;
        $ret['data'] = $data;
        return $ret;
    }

    /**
     * 受付一覧件数
     * @return type
     */
    public function count_accept($cond = null)
    {
        $sql = "SELECT count(*)
                FROM ldr_mst_outside mt
                WHERE
                    NOT del_flg
                ";
        $ret = $this->make_condition_accept($cond);
        $sql.= $ret['cond'];
        $types = array_merge(array(), $ret['types']);
        $data = array_merge(array(), $ret['data']);
        $count = $this->db->extended->getOne($sql, null, $data, $types);
        if (PEAR::isError($count)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $count->getDebugInfo());
        }
        return $count;
    }

    /**
     * 受付一覧
     * @param type $st
     * @param type $pageCount
     * @param type $cond
     * @return type
     */
    public function lists_accept($st = null, $pageCount = null, $cond = null)
    {

        $sql = "SELECT
                    outside_id, organizer, start_date, last_date, pref, place, no, contents, commit_flg, update_emp_id,
                    (SELECT count(*)
                     FROM ldr_trn_outside trn
                     WHERE
                     trn.outside_id=mt.outside_id
                     AND (trn.status = 0 OR trn.status >= 2 OR trn.status < -2)
                     AND NOT del_flg) as count,
                    (SELECT count(*) FROM ldr_trn_outside trn WHERE trn.outside_id=mt.outside_id AND trn.status = 2 AND NOT del_flg) as count_2,
                    (SELECT count(*) FROM ldr_trn_outside trn WHERE trn.outside_id=mt.outside_id AND trn.status = 3 AND NOT del_flg) as count_3,
                    (SELECT count(*) FROM ldr_trn_outside trn WHERE trn.outside_id=mt.outside_id AND trn.status = -3 AND NOT del_flg) as count_3c,
                    (SELECT count(*) FROM ldr_trn_outside trn WHERE trn.outside_id=mt.outside_id AND trn.status = 0 AND NOT del_flg) as count_0
                FROM ldr_mst_outside mt
                WHERE
                    NOT del_flg
                ";
        $ret = $this->make_condition_accept($cond);
        $sql.= $ret['cond'];
        $types = array_merge(array(), $ret['types']);
        $data = array_merge(array(), $ret['data']);

        $cond['order_by'][] = "start_date asc";
        $cond['order_by'][] = "outside_id asc";
        $order = "";
        foreach ($cond['order_by'] as $value) {
            $order.= empty($order) ? "order by " : ", ";
            $order.= $value;
        }
        $sql .= $order;

        if (isset($st) && isset($pageCount)) {
            $this->db->setLimit($pageCount, $st);
        }
        $sth = $this->db->prepare($sql, $types, MDB2_PREPARE_RESULT);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        $date = date('Y-m-d');
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['closing'] = Ldr_Util::outside_closing_date($row['start_date']);
            $row['close_status'] = ($date > $row['closing']) ? -1 : 1;
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 部署一覧
     * @param type $cond
     * @return string
     * @throws Exception
     */
    public function lists_accept_class($cond)
    {
        $types = array("integer");
        $data = array('id' => $cond['id']);

        $sql = "SELECT
                    c1.class_id,
                    c2.atrb_id,
                    c3.dept_id,
                    c4.room_id,
                    $this->class_sql,
                    class_nm,
                    atrb_nm,
                    dept_nm,
                    room_nm
                FROM ldr_trn_outside trn
                JOIN empmst e USING(emp_id)
                JOIN classmst c1 ON c1.class_id = e.emp_class
                JOIN atrbmst c2 ON c2.class_id = e.emp_class AND c2.atrb_id = e.emp_attribute AND NOT atrb_del_flg
                JOIN deptmst c3 ON c3.atrb_id = e.emp_attribute AND c3.dept_id = e.emp_dept AND NOT dept_del_flg
                LEFT JOIN classroom c4 ON c4.dept_id = e.emp_dept AND(c4.room_id = e.emp_room OR c4.room_id IS NULL) AND NOT room_del_flg
                WHERE
                    NOT trn.del_flg
                    AND trn.outside_id = :id
                    AND (trn.status = 0 OR trn.status >= 2 OR trn.status < -2)
                GROUP BY c1.order_no,c1.class_id,c2.order_no,c2.atrb_id,c3.order_no,c3.dept_id,c4.order_no,c4.room_id,class_nm,atrb_nm,dept_nm,room_nm
                ORDER BY c1.order_no asc,c1.class_id asc,c2.order_no asc,c2.atrb_id asc,c3.order_no asc,c3.dept_id asc,c4.order_no asc,c4.room_id asc";

        $sth = $this->db->prepare($sql, $types, MDB2_PREPARE_RESULT);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }
        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['key'] = $row['class_id'] . "_" . $row['atrb_id'] . "_" . $row['dept_id'] . "_" . $row['room_id'];
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 申請したidの職員一覧
     * @param type $data
     * @return type
     */
    public function lists_accept_emp($cond)
    {
        if ($cond['list_type'] === 'report') {
            $sql_status = "AND (trn.status = 0 OR trn.status >= 2 OR trn.status < -2)";
            $types = array("integer");
            $data = array('id' => $cond['id']);

            if ($cond['srh_report'] === 'reported') {
                $sql_status .= " AND trn.report > 0";
            }
            else if ($cond['srh_report'] === 'nonreport') {
                $sql_status .= " AND trn.report IS NULL";
            }
        }
        else {
            $sql_status = "AND (trn.status = 0 OR trn.status >= 2 OR trn.status < -2)";
            if (!empty($cond['srh_class'])) {
                $class = explode('_', $cond['srh_class']);
                $sql_status .= " AND e.emp_class = :class AND e.emp_attribute = :attribute AND e.emp_dept = :dept";
                if (!empty($class[3])) {
                    $sql_status .= " AND e.emp_room = :room";
                }
                $types = array("integer", "integer", "integer", "integer", "integer");
                $data = array('id' => $cond['id'], 'class' => $class[0], 'attribute' => $class[1], 'dept' => $class[2], 'room' => $class[3]);
            }
            else {
                $types = array("integer");
                $data = array('id' => $cond['id']);
            }
        }
        $sql_orderby = "ORDER BY c1.order_no asc,c1.class_id asc,c2.order_no asc,c2.atrb_id asc,c3.order_no asc,c3.dept_id asc,c4.order_no asc,c4.room_id asc,e.emp_personal_id asc";

        $sql = "SELECT
                    training_id,
                    trn.outside_id,
                    trn.emp_id,
                    e.emp_personal_id,
                    trn.status,
                    trn.type,
                    trn.report,
                    trn.learning,
                    e.emp_lt_nm || ' ' || e.emp_ft_nm as name,
                    trn.created_on,
                    $this->class_sql,
                    class_nm,
                    atrb_nm,
                    dept_nm,
                    room_nm,
                    trn.updated_on
                FROM ldr_trn_outside trn
                JOIN empmst e USING(emp_id)
                JOIN classmst c1 ON c1.class_id = e.emp_class
                JOIN atrbmst c2 ON c2.class_id = e.emp_class AND c2.atrb_id = e.emp_attribute AND NOT atrb_del_flg
                JOIN deptmst c3 ON c3.atrb_id = e.emp_attribute AND c3.dept_id = e.emp_dept AND NOT dept_del_flg
                LEFT JOIN classroom c4 ON c4.dept_id = e.emp_dept AND(c4.room_id = e.emp_room OR c4.room_id IS NULL) AND NOT room_del_flg
                WHERE
                    NOT trn.del_flg
                    AND trn.outside_id = :id
                    $sql_status
                $sql_orderby";

        $sth = $this->db->prepare($sql, $types, MDB2_PREPARE_RESULT);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['status_text'] = Ldr_Const::get_training_status($row['status']);
            $row['type_text'] = Ldr_Const::get_outside_type($row['type']);
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 受付の保存
     * @param type $data
     */
    public function save_accept($data)
    {
        $ary2 = array();
        $ary3 = array();
        $ary3c = array();
        foreach ($data['training_id'] as $key => $value) {
            switch ($value) {
                case "2":
                    $ary2[] = $key;
                    break;
                case "3":
                    $ary3[] = $key;
                    break;
                case "-3":
                    $ary3c[] = $key;
                    break;
            }
        }
        if (!empty($ary2)) {
            $this->status_change($ary2, "2");
        }
        if (!empty($ary3)) {
            $this->status_change($ary3, "3");
        }
        if (!empty($ary3c)) {
            $this->status_change($ary3c, "-3");
        }

        $this->log->log("受付しました", "training_id=完了[" . implode(" ", $ary3) . "] 取下[" . implode(" ", $ary3c) . "] 未設定[" . implode(" ", $ary2) . "]");
    }

    /**
     * 院外研修の報告
     * @param type $data
     * @return type
     * @throws Exception
     */
    public function save_report($data)
    {
        if (empty($data['id']) || empty($data['flag']) || empty($data['training_id'])) {
            throw new Exception(__METHOD__ . 'METHOD Param error');
        }
        $this->db->beginNestedTransaction();

        $this->update_report($data);

        $f22 = new Ldr_Form22($this->emp_id);
        if ($data['report'] === '3') {
            // 念のため消す
            $f22->delete_training($data['flag'], $data['training_id']);
        }
        else {
            $f22->save_training($data);
        }

        $this->db->completeNestedTransaction();
        $this->log->log("報告しました", "id={$data['id']}");
    }

    /**
     * 院外研修の報告
     * @param type $data
     * @return type
     * @throws Exception
     */
    private function update_report($data)
    {
        $this->db->beginNestedTransaction();

        // UPDATE
        $sth = $this->db->prepare("UPDATE ldr_trn_outside SET status = 0,report= :report, learning= :learning WHERE training_id= :id ", array('integer', 'text', 'integer'), MDB2_PREPARE_MANIP);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }
        $sth->free();

        $this->db->completeNestedTransaction();
    }

    /**
     * 削除
     * @param type $id     outside_id
     * @throws Exception
     */
    public function delete_mst($id)
    {
        $this->db->beginNestedTransaction();

        $sth = $this->db->prepare("UPDATE ldr_mst_outside SET del_flg = true WHERE outside_id= :outside_id", array('boolean'), MDB2_PREPARE_MANIP);
        $res = $sth->execute(array('outside_id' => $id));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }
        $this->db->completeNestedTransaction();

        $sth->free();

        // Log
        $this->log->log("削除しました", "outside_id={$id}");
    }

    /**
     * 受付件数（ステータス毎）
     * @return Array
     * @throws Exception
     */
    public function count_accept_status()
    {
        if (empty($this->emp_id)) {
            throw new Exception(__METHOD__ . 'METHOD Param error');
        }

        $sql = "SELECT
                (SELECT COUNT(*)
                    FROM ldr_trn_outside trn
                    JOIN empmst e USING(emp_id)
                    JOIN ldr_approver app
                        ON app.emp_id = :emp_id
                        AND app.class_id = e.emp_class
                        AND app.atrb_id = e.emp_attribute
                        AND app.dept_id = e.emp_dept
                        AND COALESCE(app.room_id,0) = COALESCE(e.emp_room,0)
                    JOIN ldr_mst_outside mst ON mst.outside_id=trn.outside_id AND NOT mst.del_flg
                    WHERE trn.status=1 AND NOT trn.del_flg
                ) AS st1_count,
                (SELECT COUNT(*)
                    FROM ldr_trn_outside trn

                    JOIN ldr_mst_outside mst ON mst.outside_id=trn.outside_id AND NOT mst.del_flg
                    WHERE trn.status=2 AND NOT trn.del_flg
                ) AS st2_count";
        $row = $this->db->extended->getRow($sql, null, array('emp_id' => $this->emp_id), array('text'), MDB2_FETCHMODE_ASSOC);
        if (PEAR::isError($row)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $row->getDebugInfo());
        }

        return $row;
    }

}
