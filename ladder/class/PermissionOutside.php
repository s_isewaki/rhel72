<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'ladder/class/Employee.php';
require_once 'ladder/class/Log.php';

class Ldr_PermissionOutside extends Model
{

    var $log;

    /**
     * コンストラクタ
     */
    public function Ldr_PermissionOutside()
    {
        parent::connectDB();
        $this->log = new Ldr_log('permission');
    }

    /**
     * 一覧の取得
     * @return Array
     */
    public function lists()
    {
        $sql = "SELECT emp_id FROM ldr_permission_outside JOIN empmst USING (emp_id) JOIN stmst ON emp_st=st_id ORDER BY stmst.order_no, empmst.emp_kn_lt_nm, emp_id";
        $sth = $this->db->prepare($sql, null, MDB2_PREPARE_RESULT);
        $res = $sth->execute();
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = new Ldr_Employee($row['emp_id']);
        }
        $sth->free();
        return $lists;
    }

    /**
     * 保存
     * @param Array $data
     */
    public function save($data)
    {
        $this->db->beginNestedTransaction();

        // DELETE
        $res_d = $this->db->exec('DELETE FROM ldr_permission_outside');
        if (PEAR::isError($res_d)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . " ERROR: " . $res_d->getDebugInfo());
        }

        // INSERT
        $sth = $this->db->prepare("INSERT INTO ldr_permission_outside (emp_id) VALUES (?)", array('text'), MDB2_PREPARE_MANIP);
        foreach ($data['emp_id'] as $emp_id) {
            $res = $sth->execute(array($emp_id));
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " emp_id={$emp_id} ERROR: " . $res->getDebugInfo());
            }
        }
        $this->db->completeNestedTransaction();
        $sth->free();

        // Log
        $this->log->log('院外研修担当を保存しました', "emp_id=[" . implode(" ", $data['emp_id']) . "]");
    }

}
