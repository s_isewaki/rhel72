<?php

require_once 'Cmx.php';
require_once 'ladder/class/Util.php';

/**
 * キャリア開発ラダーPagerクラス
 */
class Ldr_Pager
{

    /**
     * ページャの情報を設定する
     * @param type $page_no      ページ番号
     * @param type $dataCount    データ総数
     * @param type $perPage      １ページ毎の表示数
     * @return type
     */
    public static function get_pager_info($page_no, $dataCount, $perPage)
    {
        $ret = array();
        $currentPageID = Ldr_Pager::get_page_no($page_no, $dataCount, $perPage);
        $pagecnt = Ldr_Pager::get_page_count($dataCount, $perPage);
        $ret['page_no'] = $currentPageID;
        if ($currentPageID > $pagecnt) {
            $currentPageID = $pagecnt;
            $ret['page_no'] = $pagecnt;
        }
        $ret['page_count'] = $pagecnt;
        return $ret;
    }

    /**
     * 現在のページ番号を取得
     * @param type $page_no      ページ番号
     * @param type $dataCount    データ総数
     * @param type $perPage      １ページ毎の表示数
     * @return type
     */
    public static function get_page_no($page_no, $dataCount = -1, $perPage = -1)
    {
        $currentPageID = 1;
        if (!empty($page_no)) {
            $currentPageID = $page_no;
        }

        if ($dataCount == -1 && $perPage == -1) {
            return $currentPageID;
        }

        $pagecnt = Ldr_Pager::get_page_count($dataCount, $perPage);
        if ($currentPageID > $pagecnt) {
            $currentPageID = $pagecnt;
        }
        return $currentPageID;
    }

    /**
     * 開始項目のindexを計算
     * @param type $current    現在のページ
     * @param type $perPage    １ページ毎の表示数
     * @return type
     */
    public static function calc_page_index($current, $perPage)
    {
        return ($current - 1) * $perPage;
    }

    /**
     * 総ページ数の取得
     * @param type $dataCount    データ総数
     * @param type $perPage    １ページ毎の表示数
     * @return int
     */
    protected static function get_page_count($dataCount, $perPage)
    {
        $pagecnt = ceil($dataCount / $perPage);
        if ($pagecnt == 0) {
            $pagecnt = 1;
        }
        return (int)$pagecnt;
    }

    /**
     * ページャでの画面移動か判定する
     * @return boolean ture:ページャから遷移
     */
    public static function is_pager($move_pager)
    {
        if ($move_pager) {
            return true;
        }
        return false;
    }

}
