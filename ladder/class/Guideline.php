<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'Cmx/Model/SystemConfig.php';
require_once 'ladder/class/Guideline/Guideline.php';
require_once 'ladder/class/Guideline/Revision.php';
require_once 'ladder/class/Guideline/Group1.php';
require_once 'ladder/class/Guideline/Group2.php';
require_once 'ladder/class/Log.php';

class Ldr_Guideline extends Model
{

    var $conf;
    var $gl;
    var $rv;
    var $g1;
    var $g2;
    var $log;

    /**
     * コンストラクタ
     */
    public function Ldr_Guideline()
    {
        parent::connectDB();
        $this->conf = new Cmx_SystemConfig();
        $this->gl = new Ldr_GuidelineGuideline();
        $this->rv = new Ldr_GuidelineRevision();
        $this->g1 = new Ldr_GuidelineGroup1();
        $this->g2 = new Ldr_GuidelineGroup2();
        $this->log = new Ldr_Log('guideline');
    }

    /**
     * validate
     * @param type $type
     * @param type $data
     * @return type
     */
    public function validate($type, $data)
    {
        switch ($type) {
            case 'guideline':
                return $this->gl->validate($data);
                break;

            case 'revision':
                return $this->rv->validate($data);
                break;

            case 'group1':
                return $this->g1->validate($data);
                break;

            case 'group2':
                return $this->g2->validate($data);
                break;
        }
    }

    /**
     * 一覧の取得
     * @param type $type
     * @return type
     */
    public function lists($type, $data = null)
    {
        switch ($type) {
            case 'guideline':
                return $this->gl->lists($data);

            case 'revision':
                return $this->rv->lists();

            case 'group1':
                return $this->g1->lists($data);

            case 'group2':
                return $this->g2->lists($data);
        }
    }

    /**
     * 保存
     * @param type $type
     * @param type $data
     * @return type
     */
    public function save($type, $data)
    {
        switch ($type) {
            case 'guideline':
                return $this->gl->save($data);

            case 'revision':
                return $this->rv->save($data);

            case 'group1':
                return $this->g1->save($data);

            case 'group2':
                return $this->g2->save($data);
        }
    }

    /**
     * 削除
     * @param type $type
     * @param type $id
     * @return type
     */
    public function delete($type, $id)
    {
        switch ($type) {
            case 'guideline':
                return $this->gl->delete($id);
                break;

            case 'revision':
                return $this->rv->delete($id);
                break;

            case 'group1':
                return $this->g1->delete($id);
                break;

            case 'group2':
                return $this->g2->delete($id);
                break;
        }
    }

    /**
     * 1件取得
     * @param type $type
     * @param type $id
     * @return type
     */
    public function find($type, $id)
    {
        switch ($type) {
            case 'guideline':
                return $this->gl->find($id);
                break;

            case 'revision':
                return $this->rv->find($id);
                break;

            case 'group1':
                return $this->g1->find($id);
                break;

            case 'group2':
                return $this->g2->find($id);
                break;
        }
    }

    /**
     * 並べ替え
     * @param type $type
     * @param type $data
     * @return type
     */
    public function sort($type, $data)
    {
        switch ($type) {
            case 'guideline':
                return $this->gl->sort($data);
                break;

            case 'group1':
                return $this->g1->sort($data);
                break;

            case 'group2':
                return $this->g2->sort($data);
                break;

            default:
                return array();
                break;
        }
    }

    /**
     * 現在運用中の版数の取得、切り替え
     * @param type $current_revision
     * @return type
     */
    function current_revision($current_revision = null)
    {
        if (!empty($current_revision)) {
            $find = $this->rv->find($current_revision);
            if (!empty($find)) {
                $this->conf->set('ladder.current_revision', $current_revision);

                // Log
                $this->log->log('版を切り替えました', "revision=$current_revision");

                return $current_revision;
            }
        }
        return $this->conf->get('ladder.current_revision');
    }

    /**
     * 行動指標全リスト
     */
    function full_lists($query)
    {
        $revision = 1;
        return $this->gl->full_lists($revision, $query);
    }

    /**
     * 行動指標有無チェック
     */
    function is_lists()
    {
        $revision = $this->current_revision();
        return $this->gl->is_lists($revision);
    }

    /**
     * CSV出力
     * @param type $revision
     * @return type
     */
    public function export_csv($revision)
    {
        return $this->gl->export_csv($revision);
    }

    /**
     * CSVインポート
     * @param type $file
     * @return type
     * @throws Exception
     */
    public function import_csv($revision, $file)
    {
        $line = 0;
        $error_all = array();
        $fp = fopen($file['tmp_name'], 'r');
        $this->db->beginNestedTransaction();
        while (($csv = fgetcsv_reg($fp)) !== false) {
            mb_convert_variables('EUC-JP', 'SJIS-Win', $csv);

            $error = array();

            // 空データは飛ばす
            if (empty($csv)) {
                continue;
            }

            if ($csv[0] === '行動指標ID') {
                $header = $csv;
                continue;
            }
            $line++;

            $valid = $this->import_csv_validate($csv, $header, $line);
            $error = array_merge($error, $valid);

            if (empty($error)) {
                // 領域
                $group1 = $this->g1->find_or_create($csv[2], $revision);

                // 分類
                $group2 = $this->g2->find_or_create($csv[3], $revision, $group1);

                // CSV投入
                $data = array(
                    'id' => $csv[0],
                    'level' => $csv[1],
                    'guideline_group1' => $group1,
                    'guideline_group2' => $group2,
                    'guideline' => $csv[4],
                    'criterion' => $csv[5],
                    'guideline_revision' => $revision,
                );
                $this->gl->save($data);
            }

            $error_all = array_merge($error_all, $error);
        }

        if (!empty($error_all)) {
            $this->db->failNestedTransaction();
            throw new Exception(implode(',', $error_all));
        }
        $this->db->completeNestedTransaction();

        // Log
        $this->log->log("{$line}件のデータをインポートしました");

        return "{$line}件のデータをインポートしました";
    }

    /**
     * CSVバリデーション
     * @param type $csv
     * @param type $line
     * @return type
     */
    public function import_csv_validate($csv, $head, $line)
    {
        $error = array();

        // レベル
        if (!Validation::is_level($csv[1])) {
            $error[] = "{$line}行目: 「{$head[1]}」が不正です → {$csv[1]}";
        }

        // 領域
        if (Validation::is_empty($csv[2])) {
            $error[] = "{$line}行目: 「{$head[2]}」が設定されていません → {$csv[2]}";
        }

        // 分類
        if (Validation::is_empty($csv[3])) {
            $error[] = "{$line}行目: 「{$head[3]}」が設定されていません → {$csv[3]}";
        }

        // ガイドライン
        if (Validation::is_empty($csv[4])) {
            $error[] = "{$line}行目: 「{$head[4]}」が設定されていません → {$csv[4]}";
        }

        return $error;
    }

}
