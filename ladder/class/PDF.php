<?php

require_once 'ladder/class/Base.php';
require_once 'tcpdf/tcpdf.php';
require_once 'ladder/class/Config.php';

class Ldr_PDF extends TCPDF
{

    protected $emp;
    protected $print;
    protected $hospital;

    /**
     * コンストラクタ
     */
    public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false)
    {
        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache);

        $conf = new Ldr_Config();
        $this->hospital = $conf->hospital_name($_POST['hospital_name']);

        $this->SetFont('kozgopromedium', '', 10);
    }

    /**
     * Page header
     */
    public function Header()
    {
        // Logo
        $image_file = CMX_BASE_DIR . '/ladder/img/comedix.gif';
        $this->y = 5;
        $this->Image($image_file, '', '', '', 10, 'GIF', '', 'T', true, 300, 'R', false, false, 0, false, false, false);

        // Title
        $this->x = 10;
        $this->y = 8;
        $this->SetFont('kozgopromedium', 'B', 14);
        $this->Cell(0, 7, to_utf8($this->header_title), 0, 1, 'L', false, '', 0, false, 'T', 'T');

        // Line
        $this->hr();

        // Employee
        if (!empty($this->emp)) {
            $this->y = 16;
            $this->SetFont('kozgopromedium', '', 10);
            $this->Cell(0, 5, to_utf8($this->emp->class_full_name()), 0, 1, 'R', false, '', 0, false, 'T', 'T');
            $this->SetFont('kozgopromedium', 'B', 11);
            $this->Cell(0, 11, to_utf8($this->emp->emp_name()), 0, 1, 'R', false, '', 0, false, 'T', 'T');
            $this->SetTopMargin(28);
        }
        else {
            $this->SetTopMargin(18);
        }
    }

    /**
     * Page footer
     */
    public function Footer()
    {
        // Position at 15 mm from bottom
        $this->SetY(-15);

        // Line
        $this->hr();

        // Page number
        $this->SetFont('kozgopromedium', '', 8);
        $this->Cell(100, 4, 'Page: ' . $this->getAliasNumPage() . ' / ' . $this->getAliasNbPages(), 0, 0, 'L', false, '', 0, false, 'T', 'M');

        // Hospital
        $this->SetFont('kozgopromedium', '', 8);
        $this->Cell(0, 4, to_utf8($this->hospital), 0, 1, 'R', 0, '', 0, false, 'T', 'M');

        // Info
        $this->SetFont('kozgopromedium', '', 8);
        $this->SetTextColor(192, 192, 192);
        $this->Cell(0, 6, 'Print: ' . to_utf8($this->print->emp_name()) . '  Date: ' . date('Y-m-d H:i:s'), 0, 1, 'R', false, '', 0, false, 'T', 'M');
    }

    /**
     * Set ヘッダータイトル
     * @param type $title
     */
    public function setHeaderTitle($title)
    {
        $this->header_title = $title;
    }

    /**
     * Set 職員データ
     * @param type $emp
     */
    public function setEmployee($emp)
    {
        $this->emp = $emp;
    }

    /**
     * Set 印刷職員データ
     * @param type $emp
     */
    public function setPrinter($emp)
    {
        $this->print = $emp;
    }

    /**
     * 水平線
     */
    public function hr()
    {
        $this->Line($this->lMargin - 2, $this->y, $this->w - $this->lMargin + 4, $this->y);
    }

}
