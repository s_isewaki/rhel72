<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'ladder/class/Validation.php';
require_once 'ladder/class/Log.php';

class Ldr_ReliefRelief extends Model {

    var $log;

    /**
     * コンストラクタ
     * @param type $emp_id 職員ID
     */
    public function Ldr_ReliefRelief() {
        parent::connectDB();
        $this->log = new Ldr_log('adm_relief');
    }

    /**
     * validate
     * @param type $data
     * @return string
     */
    public function validate($data) {
        $error = array();
        // 科目
        if (Validation::is_empty($data['subject'])) {
            $error['subject'][] = '科目が入力されていません';
        }
        elseif (mb_strlen($data['subject'], 'EUC-JP') > 200) {
            $error['subject'][] = '科目は200文字以内で入力してください';
        }

        return $error;
    }

    /**
     * 科目一覧の取得
     * @param type $st
     * @param type $pageCount
     * @param type $cond
     * @return type
     */
    public function lists() {
        $sql = "SELECT relief_id,subject
                FROM ldr_mst_relief
                WHERE NOT del_flg
                order by order_no asc,relief_id";
        $sth = $this->db->prepare($sql, null, MDB2_PREPARE_RESULT);
        $res = $sth->execute(null);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 教科内容一覧の取得
     * @param type $st
     * @param type $pageCount
     * @param type $cond
     * @return type
     */
    public function lists_contents() {
        $sql = "SELECT r.relief_id,subject,unit,contents,detail,time,
                (SELECT count(*) FROM ldr_mst_relief_detail s WHERE r.relief_id = s.relief_id AND NOT s.del_flg) as cnt
                FROM
                ldr_mst_relief r
                LEFT JOIN ldr_mst_relief_detail d ON d.relief_id = r.relief_id AND NOT d.del_flg
                WHERE NOT r.del_flg
                order by r.order_no asc,r.relief_id asc,d.order_no asc,d.unit asc";
        $sth = $this->db->prepare($sql, null, MDB2_PREPARE_RESULT);
        $res = $sth->execute(null);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * １件検索
     * @param type $id
     * @return type
     * @throws Exception
     */
    public function find($id) {
        $sql = "SELECT relief_id,relief_id AS id, subject,order_no
                FROM ldr_mst_relief
                WHERE relief_id= :relief_id AND NOT del_flg";
        $row = $this->db->extended->getRow(
            $sql, null, array('relief_id' => $id), array('integer'), MDB2_FETCHMODE_ASSOC
        );
        if (PEAR::isError($row)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $row->getDebugInfo());
        }

        return $row;
    }

    /**
     * 保存
     * @param String $emp_id
     * @param Array $data
     */
    public function save($data) {
        $this->db->beginNestedTransaction();
        $item = $this->find($data['id']);

        if (empty($item)) {
            $sth = $this->db->prepare("INSERT INTO
                ldr_mst_relief
                (subject)
                VALUES
                (:subject)", array('text'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($data);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
            }
            $data['id'] = $this->db->lastInsertID('ldr_mst_relief', 'relief_id');
            $sth->free();
        }

        // UPDATE
        else {
            $up = array_merge($item, $data);
            $sth = $this->db->prepare("UPDATE ldr_mst_relief SET
                subject= :subject,
                order_no= :order_no
                WHERE relief_id= :relief_id AND NOT del_flg", array('text', 'integer', 'integer'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($up);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
            }

            $sth->free();
        }
        $this->db->completeNestedTransaction();

        // Log
        $this->log->log("保存しました", "relief_id={$data['id']} subject={$data['subject']}");
    }

    /**
     * 削除
     * @param type $id
     * @throws Exception
     */
    public function delete($id) {
        $this->db->beginNestedTransaction();
        $sth = $this->db->prepare("UPDATE ldr_mst_relief
            SET del_flg = true
            WHERE
            relief_id = :relief_id AND NOT del_flg", array('integer'), MDB2_PREPARE_MANIP);
        $res = $sth->execute(array('relief_id' => $id));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }
        $sth->free();
        $this->db->completeNestedTransaction();

        // Log
        $this->log->log("削除しました", "relief_id=$id");
    }

    /**
     * ソート
     * @param type $data
     * @throws Exception
     */
    public function sort($data) {
        $this->db->beginNestedTransaction();
        $sth = $this->db->prepare("UPDATE ldr_mst_relief SET order_no= ? WHERE relief_id= ? AND NOT del_flg", array('integer', 'integer'), MDB2_PREPARE_MANIP);
        $cnt = 1;
        foreach ($data['order'] as $id) {
            $res = $sth->execute(array($cnt++, $id));
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " id={$id} ERROR: " . $res->getDebugInfo());
            }
        }
        $this->db->completeNestedTransaction();
        $sth->free();

        // Log
        $this->log->log("並べ替えました");
    }

}
