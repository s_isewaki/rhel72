<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'ladder/class/Validation.php';
require_once 'ladder/class/Log.php';

class Ldr_ReliefDetail extends Model {

    var $log;

    /**
     * コンストラクタ
     */
    public function Ldr_ReliefDetail() {
        parent::connectDB();
        $this->log = new Ldr_log('adm_relief_detail');
    }

    /**
     * validate
     * @param type $data
     * @return string
     */
    public function validate($data) {
        $error = array();

        if (Validation::is_empty($data['unit'])) {
            $error['unit'][] = '単元が入力されていません';
        }
        elseif ($data['unit'] < 1 or $data['unit'] > 10) {
            $error['unit'][] = '単元は1〜10以内で入力してください';
        }
        else {
            $new_unit = trim($data['unit']);
            if ($data['old_unit'] !== $new_unit) {
                $row = $this->find($data['id'], $new_unit);
                if (!empty($row)) {
                    $error['unit'][] = '単元が重複しています';
                }
            }
        }
        // 教科内容
        if (Validation::is_empty($data['contents'])) {
            $error['contents'][] = '教科内容が入力されていません';
        }
        elseif (mb_strlen($data['contents'], 'EUC-JP') > 200) {
            $error['contents'][] = '教科内容は200文字以内で入力してください';
        }

        // 時間
        if (Validation::is_empty($data['time'])) {
            $error['time'][] = '時間が入力されていません';
        }
        elseif (mb_strlen($data['time'], 'EUC-JP') > 200) {
            $error['time'][] = '時間は200文字以内で入力してください';
        }
        return $error;
    }

    /**
     * 一覧の取得
     * @param type $id
     * @return type
     */
    public function lists($id) {
        $sql = "SELECT relief_id,unit,contents,detail,time
                FROM ldr_mst_relief_detail
                WHERE relief_id = :relief_id AND NOT del_flg
                order by order_no asc,unit";
        $sth = $this->db->prepare($sql, array('integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array('relief_id' => $id));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * １件検索
     * @param type $id     relief_id
     * @param type $unit   単元
     * @return type
     * @throws Exception
     */
    public function find($id, $unit) {
        $sql = "SELECT relief_id,unit,contents,detail,time
                FROM ldr_mst_relief_detail
                WHERE relief_id= :relief_id AND unit= :unit AND NOT del_flg";
        $row = $this->db->extended->getRow(
            $sql, null, array('relief_id' => $id, 'unit' => $unit), array('integer', 'text'), MDB2_FETCHMODE_ASSOC
        );
        if (PEAR::isError($row)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $row->getDebugInfo());
        }

        return $row;
    }

    /**
     * 保存
     * @param Array $data
     */
    public function save($data) {
        $this->db->beginNestedTransaction();
        $item = $this->find($data['id'], $data['old_unit']);

        if (empty($item)) {
            $sth = $this->db->prepare("INSERT INTO
                ldr_mst_relief_detail
                (relief_id,unit,contents,detail,time)
                VALUES
                (:id, :unit, :contents, :detail, :time)", array('integer', 'integer', 'text', 'text', 'text'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($data);
            if (PEAR::isError($res)) {
                throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
            }
            $sth->free();
        }

        // UPDATE
        else {
            $up = array_merge($item, $data);
            $sth = $this->db->prepare("UPDATE ldr_mst_relief_detail SET
                unit= :unit,
                contents= :contents,
                detail= :detail,
                time= :time
                WHERE relief_id= :id AND unit= :old_unit AND NOT del_flg", array('integer', 'text', 'text', 'text', 'integer', 'text'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($up);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
            }
            $sth->free();
        }
        $this->db->completeNestedTransaction();

        // Log
        $this->log->log("保存しました", "relief_id={$data['id']} unit={$data['unit']} contents={$data['contents']}");
    }

    /**
     * 削除
     * @param type $id
     * @param type $unit
     * @throws Exception
     */
    public function delete($id, $unit) {
        $this->db->beginNestedTransaction();
        $sth = $this->db->prepare("UPDATE ldr_mst_relief_detail SET del_flg = true
            WHERE
            relief_id = :relief_id AND unit=:unit AND NOT del_flg", array('integer'), MDB2_PREPARE_MANIP);
        $res = $sth->execute(array('relief_id' => $id, 'unit' => $unit));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }
        $sth->free();
        $this->db->completeNestedTransaction();

        // Log
        $this->log->log("削除しました", "id={$id} unit={$unit}");
    }

    /**
     * 削除relief_idの単位で削除
     * @param type $id
     * @throws Exception
     */
    public function delete_relief_id($id) {
        $this->db->beginNestedTransaction();
        $sth = $this->db->prepare("UPDATE ldr_mst_relief_detail
            SET del_flg = true
            WHERE
            relief_id = :relief_id AND NOT del_flg", array('integer'), MDB2_PREPARE_MANIP);
        $res = $sth->execute(array('relief_id' => $id));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }
        $sth->free();
        $this->db->completeNestedTransaction();

        // Log
        $this->log->log("削除しました", "id={$id}");
    }

    /**
     * ソート
     * @param type $data
     * @throws Exception
     */
    public function sort($data) {
        $this->db->beginNestedTransaction();
        $sth = $this->db->prepare("UPDATE ldr_mst_relief_detail SET order_no= ? WHERE relief_id= ? AND unit= ? AND NOT del_flg", array('integer', 'integer', 'text'), MDB2_PREPARE_MANIP);
        $cnt = 1;
        foreach ($data['order'] as $unit) {
            $res = $sth->execute(array($cnt++, $data["relief_id"], $unit));
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
            }
        }
        $this->db->completeNestedTransaction();
        $sth->free();

        // Log
        $this->log->log("並べ替えました");
    }

}
