<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'ladder/class/Validation.php';
require_once 'ladder/class/Form23.php';
require_once 'ladder/class/Log.php';
require_once 'ladder/class/Config.php';

class Ldr_TrnRelief extends Model {

    var $log;
    var $emp_id;
    var $class_sql;

    /**
     * コンストラクタ
     * @param type $emp_id 職員ID
     */
    public function Ldr_TrnRelief($emp_id) {
        parent::connectDB();
        $this->log = new Ldr_log('relief');
        $this->emp_id = $emp_id;

        $conf = new Ldr_Config();
        $this->class_sql = Ldr_Util::class_full_name_sql($conf->view_class());
    }

    /**
     * validate
     * @param type $data
     * @return string
     */
    public function validate($data) {
        $error = array();

        if (Validation::is_empty($data['rd_branch'])) {
            $error['branch'][] = '支部名が設定されていません';
        }
        else if ($data['rd_branch'] === '2' && Validation::is_empty($data['branch'])) {
            $error['branch'][] = '支部名が設定されていません';
        }

        if (Validation::is_empty($data['rd_hospital'])) {
            $error['hospital'][] = '施設名が設定されていません';
        }
        else if ($data['rd_hospital'] === '2' && Validation::is_empty($data['hospital'])) {
            $error['hospital'][] = '施設名が設定されていません';
        }
        // 申請年月日
        if (Validation::is_empty($data['training_day'])) {
            $error['training_day'][] = '開催日が入力されていません';
        }
        else {
            if (!Validation::is_date($data['training_day'])) {
                $error['training_day'][] = '開催日が正しくありません';
            }
        }
        return $error;
    }

    /**
     * 報告のvalidate
     * @param type $data
     * @return string
     */
    public function validate_report($data) {
        $error = array();

        if (Validation::is_empty($data['report'])) {
            $error['report'][] = '報告が設定されていません';
        }
        else if ($data['report'] === '2' && Validation::is_empty($data['learning'])) {
            $error['learning'][] = '出張報告なしの場合は学んだことを入力してください';
        }

        return $error;
    }

    /**
     * 申請(１件)の取得
     * @param type $id
     * @return type
     * @throws Exception
     */
    public function find_apply($id) {
        $sql = "SELECT
                    emp_id,
                    training_id,
                    status,
                    trn.relief_id,
                    trn.unit,
                    training_day,
                    trn.updated_on,
                    branch,
                    hospital,
                    report,
                    learning,
                    r.subject,
                    d.contents,
                    d.detail,
                    d.time
                FROM ldr_trn_releif trn
                JOIN ldr_mst_relief r USING(relief_id)
                JOIN ldr_mst_relief_detail d ON NOT d.del_flg AND trn.relief_id = d.relief_id AND trn.unit = d.unit
                WHERE
                    NOT trn.del_flg
                    AND trn.training_id = :id";
        $row = $this->db->extended->getRow(
            $sql, null, array('id' => $id), array('integer'), MDB2_FETCHMODE_ASSOC
        );
        if (PEAR::isError($row)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $row->getDebugInfo());
        }

        return $row;
    }

    /**
     * 申請数
     * @return type
     */
    public function count_apply() {
        $sql = "SELECT count(*)
                FROM ldr_trn_releif trn
                JOIN ldr_mst_relief r USING(relief_id)
                JOIN ldr_mst_relief_detail d ON NOT d.del_flg AND trn.relief_id = d.relief_id AND trn.unit = d.unit
                WHERE
                    NOT trn.del_flg
                    AND trn.emp_id = :emp_id";

        $types = array('text');
        $count = $this->db->extended->getOne($sql, null, array('emp_id' => $this->emp_id), $types);
        if (PEAR::isError($count)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $count->getDebugInfo());
        }

        return $count;
    }

    /**
     * 申請一覧の取得
     * @param type $st
     * @param type $pageCount
     * @param type $cond
     * @return type
     */
    public function lists_apply($st = null, $pageCount = null, $cond = null) {
        $sql = "SELECT
                    emp_id,
                    training_id,
                    status,
                    trn.relief_id,
                    trn.unit,
                    training_day,
                    trn.updated_on,
                    branch,
                    hospital,
                    report,
                    learning,
                    r.subject,
                    d.contents,
                    d.detail,
                    d.time
                FROM ldr_trn_releif trn
                JOIN ldr_mst_relief r USING(relief_id)
                JOIN ldr_mst_relief_detail d ON NOT d.del_flg AND trn.relief_id = d.relief_id AND trn.unit = d.unit
                WHERE
                    NOT trn.del_flg
                    AND trn.emp_id = :emp_id
                    ORDER BY trn.updated_on desc";

        if (isset($st) && isset($pageCount)) {
            $this->db->setLimit($pageCount, $st);
        }

        $sth = $this->db->prepare($sql, array('text'), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array('emp_id' => $this->emp_id));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['status_text'] = Ldr_Const::get_training_status($row['status']);
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 救護員研修申請
     * @param type $data
     * @throws Exception
     */
    public function save_apply($data) {
        $this->db->beginNestedTransaction();
        $data['emp_id'] = $this->emp_id;
        $data['status'] = 1;

        if (empty($data['training_id'])) {
            $types = array('text', 'integer', 'integer', 'integer', 'date', 'text', 'text');
            $sth = $this->db->prepare("INSERT INTO
                ldr_trn_releif
                (emp_id, status, relief_id, unit, training_day,branch, hospital)
                VALUES
                (:emp_id, :status, :relief_id, :unit, :training_day,:branch, :hospital)", $types, MDB2_PREPARE_MANIP);
            $res = $sth->execute($data);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
            }
            $data['training_id'] = $this->db->lastInsertID('ldr_trn_releif', 'training_id');
            $sth->free();
        }
        // UPDATE
        else {
            $types = array('text', 'integer', 'integer', 'integer', 'date', 'text', 'text', 'integer');
            $sth = $this->db->prepare("UPDATE ldr_trn_releif SET
                emp_id= :emp_id,
                status= :status,
                relief_id= :relief_id,
                unit= :unit,
                training_day= :training_day,
                branch= :branch,
                hospital= :hospital
                WHERE training_id= :training_id AND NOT del_flg", $types, MDB2_PREPARE_MANIP);
            $res = $sth->execute($data);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
            }
            $sth->free();
        }
        $this->db->completeNestedTransaction();

        // Log
        $this->log->log("保存しました", "id={$data['training_id']}");
    }

    /**
     * 申請の承認
     * @param type $id    training_id
     */
    public function save_approve($id) {
        $this->status_change($id, "2");
        if (is_array($id)) {
            $this->log->log("承認しました", count($id) . "人");
        }
        else {
            $this->log->log("承認しました", "training_id={$id}");
        }
    }

    /**
     * 申請の取下げ
     * @param type $id    training_id
     */
    public function cancel_apply($id) {
        $this->status_change($id, "-1");
        if (is_array($id)) {
            $this->log->log("申請を取り下げました", count($id) . "人");
        }
        else {
            $this->log->log("申請を取り下げました", "training_id={$id}");
        }
    }

    /**
     * 申請の取下げ(師長承認)
     * @param type $id    training_id
     */
    public function cancel_approve($id) {
        $this->status_change($id, "-2");
        if (is_array($id)) {
            $this->log->log("承認を取り下げました", count($id) . "人");
        }
        else {
            $this->log->log("承認を取り下げました", "training_id={$id}");
        }
    }

    /**
     * ステータス更新
     * @param type $id        training_id
     * @param type $status    status
     * @throws Exception
     */
    private function status_change($id, $status) {
        $this->db->beginNestedTransaction();
        if (is_array($id)) {
            $holder = implode(',', array_fill(0, count($id), '?'));
            $sql_cond = " training_id IN($holder) ";
            $data = array_merge(array($status), $id);
            $types = array_merge(array('integer'), array_fill(0, count($id), 'integer'));
        }
        else {
            $sql_cond = "training_id= ?";
            $types = array('integer', 'integer');
            $data = array($status, $id);
        }

        $sql = "UPDATE ldr_trn_releif
                SET
                    status = ?
                WHERE
                    $sql_cond
                    AND NOT del_flg";
        $sth = $this->db->prepare($sql, $types, MDB2_PREPARE_MANIP);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $sth->free();

        $this->db->completeNestedTransaction();
    }

    /**
     * 承認一覧検索条件
     * @param type $cond
     * @return type
     */
    protected function make_approve_condition($cond = null) {
        $cond_ret = "";
        $types = array();
        $data = array();
        if (!empty($cond)) {
            if (!empty($cond['srh_keyword'])) {
                $keywords = preg_split(Ldr_Const::SRH_DELIMITERS, $cond['srh_keyword'], -1, PREG_SPLIT_NO_EMPTY);
                $cond_key.= "AND ( ";
                foreach ($keywords as $key => $value) {
                    if ($key != 0) {
                        $cond_key.= " AND ";
                    }

                    $label_key = "key" . $key;
                    $cond_key.= "(e.emp_lt_nm || ' ' || e.emp_ft_nm ||' ' || e.emp_kn_lt_nm || ' ' || e.emp_kn_ft_nm ) ILIKE :" . $label_key . " ";
                    $types[] = 'text';
                    $data[$label_key] = "%" . $value . "%";
                }
                $cond_key.= ") ";
                $cond_ret .= $cond_key;
            }
            if (!empty($cond['srh_relief'])) {
                $id = explode('_', $cond['srh_relief']);
                $cond_ret .= " AND trn.relief_id = :relief_id AND trn.unit = :unit ";
                $types[] = 'integer';
                $types[] = 'integer';
                $data['relief_id'] = $id[0];
                $data['unit'] = $id[1];
            }
            if (!empty($cond['srh_start_date'])) {
                $cond_ret .= " AND ( :srh_start_date <= trn.training_day) ";
                $types[] = 'text';
                $data['srh_start_date'] = $cond['srh_start_date'];
            }
            if (!empty($cond['srh_last_date'])) {
                $cond_ret .= " AND (trn.training_day <= :srh_last_date ) ";
                $types[] = 'text';
                $data['srh_last_date'] = $cond['srh_last_date'];
            }
            if (!empty($cond['srh_nonapprove'])) {
                $cond_ret .= " AND trn.status = 1 ";
            }
        }
        $ret['cond'] = $cond_ret;
        $ret['types'] = $types;
        $ret['data'] = $data;
        return $ret;
    }

    /**
     * 承認件数の取得
     * @param type $cond
     * @return type
     */
    public function count_approve($cond) {

        $ret = $this->make_approve_condition($cond);

        $sql = "SELECT
                    count(*)
                FROM ldr_trn_releif trn
                JOIN ldr_mst_relief r USING(relief_id)
                JOIN ldr_mst_relief_detail d ON NOT d.del_flg AND trn.relief_id = d.relief_id AND trn.unit = d.unit
                JOIN empmst e USING(emp_id)
                JOIN ldr_approver app
                    ON app.emp_id = :emp_id
                    AND app.class_id = e.emp_class
                    AND app.atrb_id = e.emp_attribute
                    AND app.dept_id = e.emp_dept
                    AND COALESCE(app.room_id,0) = COALESCE(e.emp_room,0)
                WHERE
                    NOT trn.del_flg
                    AND status != '-1'";
        $sql.= $ret['cond'];
        $types = array_merge(array('text'), $ret['types']);
        $data = array_merge(array('emp_id' => $this->emp_id), $ret['data']);
        $count = $this->db->extended->getOne($sql, null, $data, $types);
        if (PEAR::isError($count)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $count->getDebugInfo());
        }

        return $count;
    }

    /**
     * 承認待ちの一覧
     * @param type $st
     * @param type $pageCount
     * @param array $cond
     * @return type
     * @throws Exception
     */
    public function lists_approve($st = null, $pageCount = null, $cond = null) {

        $ret = $this->make_approve_condition($cond);
        $sql = "SELECT
                    trn.emp_id,
                    e.emp_lt_nm || ' ' || e.emp_ft_nm as name,
                    training_id,
                    status,
                    trn.relief_id,
                    trn.unit,
                    training_day,
                    trn.updated_on,
                    branch,
                    hospital,
                    report,
                    learning,
                    r.subject,
                    d.contents,
                    d.detail,
                    d.time
                FROM ldr_trn_releif trn
                JOIN ldr_mst_relief r USING(relief_id)
                JOIN ldr_mst_relief_detail d ON NOT d.del_flg AND trn.relief_id = d.relief_id AND trn.unit = d.unit
                JOIN empmst e USING(emp_id)
                JOIN ldr_approver app
                    ON app.emp_id = :emp_id
                    AND app.class_id = e.emp_class
                    AND app.atrb_id = e.emp_attribute
                    AND app.dept_id = e.emp_dept
                    AND COALESCE(app.room_id,0) = COALESCE(e.emp_room,0)
                WHERE
                    NOT trn.del_flg
                    AND status != '-1'";
        $sql.= $ret['cond'];
        $types = array_merge(array('text'), $ret['types']);
        $data = array_merge(array('emp_id' => $this->emp_id), $ret['data']);
        $cond['order_by'][] = "trn.training_day asc";
        $cond['order_by'][] = "trn.updated_on asc";
        $cond['order_by'][] = "trn.training_id";
        $order = "";
        foreach ($cond['order_by'] as $value) {
            $order.= empty($order) ? "ORDER BY " : ", ";
            $order.= $value;
        }
        $sql .= $order;
        if (isset($st) && isset($pageCount)) {
            $this->db->setLimit($pageCount, $st);
        }

        $sth = $this->db->prepare($sql, $types, MDB2_PREPARE_RESULT);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['status_text'] = Ldr_Const::get_training_status($row['status']);
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 受付一覧検索条件
     * @param type $cond
     * @return type
     */
    protected function make_accept_condition($cond = null) {
        $cond_ret = "";
        $types = array();
        $data = array();
        if (!empty($cond)) {
            if (!empty($cond['srh_keyword'])) {
                $keywords = preg_split(Ldr_Const::SRH_DELIMITERS, $cond['srh_keyword'], -1, PREG_SPLIT_NO_EMPTY);
                $cond_key.= "AND ( ";
                foreach ($keywords as $key => $value) {
                    if ($key != 0) {
                        $cond_key.= " AND ";
                    }

                    $label_key = "key" . $key;
                    $cond_key.= "(trn.unit || ' ' || branch ||' ' || hospital || ' ' || r.subject || ' ' || d.contents) ILIKE :" . $label_key . " ";
                    $types[] = 'text';
                    $data[$label_key] = "%" . $value . "%";
                }
                $cond_key.= ") ";
                $cond_ret .= $cond_key;
            }
            if (!empty($cond['srh_start_date'])) {
                $cond_ret .= " AND ( :srh_start_date <= trn.training_day) ";
                $types[] = 'text';
                $data['srh_start_date'] = $cond['srh_start_date'];
            }
            if (!empty($cond['srh_last_date'])) {
                $cond_ret .= " AND (trn.training_day <= :srh_last_date ) ";
                $types[] = 'text';
                $data['srh_last_date'] = $cond['srh_last_date'];
            }
            if (!empty($cond['srh_nonaccept'])) {
                $cond_ret .= " AND trn.status='2' ";
            }
            if (!empty($cond['accepted'])) {
                $cond_ret .= " AND trn.status >= '3' ";
            }
        }
        $ret['cond'] = $cond_ret;
        $ret['types'] = $types;
        $ret['data'] = $data;
        return $ret;
    }

    /**
     * 受付件数
     * @param type $cond
     * @return type
     */
    public function count_accept($cond) {

        $ret = $this->make_accept_condition($cond);

        $sql = "SELECT COUNT(*)
                FROM(
                    SELECT COUNT(*)
                    FROM ldr_trn_releif trn
                    JOIN ldr_mst_relief r USING(relief_id)
                    JOIN ldr_mst_relief_detail d ON NOT d.del_flg AND trn.relief_id = d.relief_id AND trn.unit = d.unit
                    WHERE
                        NOT trn.del_flg
                        AND (trn.status = 0 OR trn.status >= 2 OR trn.status < -2)
                        {$ret['cond']}
                    GROUP BY trn.relief_id,trn.unit,trn.training_day,branch,hospital,r.subject,d.contents,d.detail,d.time
                ) CNT";
        $types = array_merge(array('text'), $ret['types']);
        $data = array_merge(array('emp_id' => $this->emp_id), $ret['data']);

        $count = $this->db->extended->getOne($sql, null, $data, $types);
        if (PEAR::isError($count)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $count->getDebugInfo());
        }

        return $count;
    }

    /**
     * 受付一覧
     * @param type $st
     * @param type $pageCount
     * @param array $cond
     * @return type
     * @throws Exception
     */
    public function lists_accept($st = null, $pageCount = null, $cond = null) {
        $ret = $this->make_accept_condition($cond);
        $sql.= $ret['cond'];
        $sql = "SELECT
                    SUM(1) AS count,
                    SUM(CASE WHEN trn.status=2 THEN 1 ELSE 0 END) AS count_2,
                    SUM(CASE WHEN trn.status=3 THEN 1 ELSE 0 END) AS count_3,
                    SUM(CASE WHEN trn.status=-3 THEN 1 ELSE 0 END) AS count_3c,
                    SUM(CASE WHEN trn.status=0 THEN 1 ELSE 0 END) AS count_0,
                    trn.relief_id,
                    trn.unit,
                    trn.training_day,
                    branch,
                    hospital,
                    r.subject,
                    d.contents,
                    d.detail,
                    d.time
                FROM ldr_trn_releif trn
                JOIN ldr_mst_relief r USING(relief_id)
                JOIN ldr_mst_relief_detail d ON NOT d.del_flg AND trn.relief_id = d.relief_id AND trn.unit = d.unit
                WHERE
                    NOT trn.del_flg
                    AND (trn.status = 0 OR trn.status >= 2 OR trn.status < -2)
                    {$ret['cond']}
                GROUP BY trn.relief_id,trn.unit,trn.training_day,branch,hospital,r.subject,d.contents,d.detail,d.time ";
        $types = array_merge(array(), $ret['types']);
        $data = array_merge(array(), $ret['data']);
        $cond['order_by'][] = "trn.training_day asc";
        $order = "";
        foreach ($cond['order_by'] as $value) {
            $order.= empty($order) ? "ORDER BY " : ", ";
            $order.= $value;
        }
        $sql .= $order;
        if (isset($st) && isset($pageCount)) {
            $this->db->setLimit($pageCount, $st);
        }

        $sth = $this->db->prepare($sql, $types, MDB2_PREPARE_RESULT);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['status_text'] = Ldr_Const::get_training_status($row['status']);
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 部署一覧
     * @param type $cond
     * @return type
     * @throws Exception
     */
    public function lists_accept_class($cond) {
        $types = array('integer', 'integer', 'date', 'text', 'text');
        $data = array('relief_id' => $cond['relief_id'],
            'unit' => $cond['unit'],
            'training_day' => $cond['training_day'],
            'branch' => $cond['branch'],
            'hospital' => $cond['hospital']
        );

        $sql = "SELECT
                     c1.class_id,
                    c2.atrb_id,
                    c3.dept_id,
                    c4.room_id,
                    $this->class_sql,
                    class_nm,
                    atrb_nm,
                    dept_nm,
                    room_nm
                FROM ldr_trn_releif trn
                JOIN empmst e USING(emp_id)
                JOIN classmst c1 ON c1.class_id = e.emp_class
                JOIN atrbmst c2 ON c2.class_id = e.emp_class AND c2.atrb_id = e.emp_attribute AND NOT atrb_del_flg
                JOIN deptmst c3 ON c3.atrb_id = e.emp_attribute AND c3.dept_id = e.emp_dept AND NOT dept_del_flg
                LEFT JOIN classroom c4 ON c4.dept_id = e.emp_dept AND(c4.room_id = e.emp_room OR c4.room_id IS NULL) AND NOT room_del_flg
                WHERE
                    NOT trn.del_flg
                    AND trn.relief_id = :relief_id
                    AND trn.training_day = :training_day
                    AND trn.branch = :branch
                    AND trn.hospital = :hospital
                    AND (trn.status = 0 OR trn.status >= 2 OR trn.status < -2)
                GROUP BY c1.order_no,c1.class_id,c2.order_no,c2.atrb_id,c3.order_no,c3.dept_id,c4.order_no,c4.room_id,class_nm,atrb_nm,dept_nm,room_nm
                ORDER BY c1.order_no asc,c1.class_id asc,c2.order_no asc,c2.atrb_id asc,c3.order_no asc,c3.dept_id asc,c4.order_no asc,c4.room_id asc";
        $sth = $this->db->prepare($sql, $types, MDB2_PREPARE_RESULT);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['key'] = $row['class_id'] . "_" . $row['atrb_id'] . "_" . $row['dept_id'] . "_" . $row['room_id'];
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 申請したidの職員一覧
     * @param type $data
     * @return type
     */
    public function lists_accept_emp($cond) {

        if ($cond['list_type'] === 'report') {
            $sql_status = "AND (trn.status = 0 OR trn.status >= 2 OR trn.status < -2)";

            if ($cond['srh_report'] === 'reported') {
                $sql_status .= " AND trn.report > 0";
            }
            else if ($cond['srh_report'] === 'nonreport') {
                $sql_status .= " AND trn.report IS NULL";
            }
        }
        else {
            $sql_status = "AND (trn.status = 0 OR trn.status >= 2 OR trn.status < -2)";

            if (!empty($cond['srh_class'])) {
                $class = explode('_', $cond['srh_class']);
                $sql_status .= " AND e.emp_class = :class AND e.emp_attribute = :attribute AND e.emp_dept = :dept";
                if (!empty($class[3])) {
                    $sql_status .= " AND e.emp_room = :room";
                }
                $types2 = array("integer", "integer", "integer", "integer", "integer");
                $data2 = array('class' => $class[0], 'attribute' => $class[1], 'dept' => $class[2], 'room' => $class[3]);
            }
        }
        $sql_orderby = "ORDER BY c1.order_no asc,c1.class_id asc,c2.order_no asc,c2.atrb_id asc,c3.order_no asc,c3.dept_id asc,c4.order_no asc,c4.room_id asc,e.emp_personal_id asc";

        $types1 = array('integer', 'integer', 'date', 'text', 'text');
        $data1 = array('relief_id' => $cond['relief_id'],
            'unit' => $cond['unit'],
            'training_day' => $cond['training_day'],
            'branch' => $cond['branch'],
            'hospital' => $cond['hospital']
        );
        $types = array_merge((array) $types1, (array) $types2);
        $data = array_merge((array) $data1, (array) $data2);

        $sql = "SELECT
                    training_id,
                    trn.relief_id,
                    trn.unit,
                    trn.emp_id,
                    e.emp_personal_id,
                    trn.status,
                    trn.report,
                    trn.learning,
                    trn.training_day,
                    trn.branch,
                    trn.hospital,
                    e.emp_lt_nm || ' ' || e.emp_ft_nm as name,
                    trn.created_on,
                    $this->class_sql,
                    class_nm,
                    atrb_nm,
                    dept_nm,
                    room_nm,
                    trn.updated_on
                FROM ldr_trn_releif trn
                JOIN ldr_mst_relief r USING(relief_id)
                JOIN ldr_mst_relief_detail d ON NOT d.del_flg AND trn.relief_id = d.relief_id AND trn.unit = d.unit
                JOIN empmst e USING(emp_id)
                JOIN classmst c1 ON c1.class_id = e.emp_class
                JOIN atrbmst c2 ON c2.class_id = e.emp_class AND c2.atrb_id = e.emp_attribute AND NOT atrb_del_flg
                JOIN deptmst c3 ON c3.atrb_id = e.emp_attribute AND c3.dept_id = e.emp_dept AND NOT dept_del_flg
                LEFT JOIN classroom c4 ON c4.dept_id = e.emp_dept AND(c4.room_id = e.emp_room OR c4.room_id IS NULL) AND NOT room_del_flg
                WHERE
                    NOT trn.del_flg
                    AND trn.relief_id = :relief_id
                    AND trn.unit = :unit
                    AND trn.training_day = :training_day
                    AND trn.branch = :branch
                    AND trn.hospital = :hospital
                    $sql_status
                $sql_orderby";
        $sth = $this->db->prepare($sql, $types, MDB2_PREPARE_RESULT);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['status_text'] = Ldr_Const::get_training_status($row['status']);
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 受付の保存
     * @param type $data
     */
    public function save_accept($id) {
        $ary2 = array();
        $ary3 = array();
        $ary3c = array();
        foreach ($id as $key => $value) {
            switch ($value) {
                case "2":
                    $ary2[] = $key;
                    break;
                case "3":
                    $ary3[] = $key;
                    break;
                case "-3":
                    $ary3c[] = $key;
                    break;
            }
        }
        if (!empty($ary2)) {
            $this->status_change($ary2, "2");
        }
        if (!empty($ary3)) {
            $this->status_change($ary3, "3");
        }
        if (!empty($ary3c)) {
            $this->status_change($ary3c, "-3");
        }
        $cnt2 = count($ary2);
        $cnt3 = count($ary3);
        $cnt3c = count($ary3c);

        $this->log->log("受付しました", "完了{$cnt3}人 取下{$cnt3c}人 未設定{$cnt2}人");
    }

    /**
     * 報告
     * @param type $data
     * @throws Exception
     */
    public function save_report($data) {
        if (empty($data['id']) || empty($data['training_id'])) {
            throw new Exception(__METHOD__ . 'METHOD Param error');
        }
        $this->db->beginNestedTransaction();

        $this->update_report($data);

        $f23 = new Ldr_Form23($this->emp_id);
        if ($data['report'] === '3') {
            // 念のため消す
            $f23->delete_training($data['training_id']);
        }
        else {
            $f23->save_training($data);
        }

        $this->db->completeNestedTransaction();
        $this->log->log("報告しました", "id={$data['id']} report={$data['report']}");
    }

    /**
     * 救護員研修の報告
     * @param type $data
     * @return type
     * @throws Exception
     */
    private function update_report($data) {
        $this->db->beginNestedTransaction();

        // UPDATE
        $sth = $this->db->prepare("UPDATE ldr_trn_releif SET status = 0,report= :report, learning= :learning WHERE training_id= :id ", array('integer', 'text', 'integer'), MDB2_PREPARE_MANIP);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }
        $sth->free();

        $this->db->completeNestedTransaction();
    }

}
