<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'ladder/class/Log.php';

class Ldr_Objection extends Model
{

    private $log;
    private $apply_id;

    /**
     * コンストラクタ
     * @param type $apply_id
     */
    public function __construct($apply_id = null)
    {
        parent::connectDB();
        $this->apply_id = $apply_id;
        $this->log = new Ldr_log('objection');
    }

    /**
     * 異議申し立て保存
     * @param Array $data
     */
    public function objection_save($data)
    {
        $data['comment'] = from_utf8($data['comment']);

        $this->db->beginNestedTransaction();

        $row = $this->db->extended->getRow("SELECT COUNT(*) AS count FROM ldr_apply_objection WHERE ldr_apply_id= :ldr_apply_id", null, $data, array('integer'), MDB2_FETCHMODE_ASSOC);

        if ($row['count'] > 0) {
            $sth = $this->db->prepare("UPDATE ldr_apply_objection SET comment = :comment WHERE ldr_apply_id= :ldr_apply_id", array('text', 'integer'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($data);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception('ldr_apply_objection INSERT or UPDATE ERROR: ' . $res->getDebugInfo());
            }
        }
        else {
            $sth = $this->db->prepare("INSERT INTO ldr_apply_objection (ldr_apply_id, comment) VALUES (:ldr_apply_id, :comment) ", array('integer', 'text'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($data);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception('ldr_apply_objection INSERT or UPDATE ERROR: ' . $res->getDebugInfo());
            }
        }

        $this->db->completeNestedTransaction();
        $this->log->log("保存しました", "ldr_apply_id={$data['ldr_apply_id']}");
    }

    /**
     * 異議申し立て受理
     * @param Array $data
     */
    public function admin_save($data)
    {
        $data['admin_comment'] = from_utf8($data['admin_comment']);

        $this->db->beginNestedTransaction();

        $row = $this->db->extended->getRow("SELECT COUNT(*) AS count FROM ldr_apply_objection WHERE ldr_apply_id= :ldr_apply_id", null, $data, array('integer'), MDB2_FETCHMODE_ASSOC);

        if ($row['count'] > 0) {
            $sth = $this->db->prepare("UPDATE ldr_apply_objection SET admin_comment = :admin_comment, read='t' WHERE ldr_apply_id= :ldr_apply_id", array('text', 'integer'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($data);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception('ldr_apply_objection INSERT or UPDATE ERROR: ' . $res->getDebugInfo());
            }
        }
        else {
            $sth = $this->db->prepare("INSERT INTO ldr_apply_objection (ldr_apply_id, admin_comment, read) VALUES (:ldr_apply_id, :admin_comment, 't') ", array('integer', 'text'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($data);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception('ldr_apply_objection INSERT or UPDATE ERROR: ' . $res->getDebugInfo());
            }
        }

        $this->db->completeNestedTransaction();
        $this->log->log("受理しました", "ldr_apply_id={$data['ldr_apply_id']}");
    }

    /**
     * 検索
     * @return type
     * @throws Exception
     */
    public function find()
    {
        if (is_null($this->apply_id)) {
            return array();
        }

        $row = $this->db->extended->getRow("SELECT * FROM ldr_apply_objection WHERE ldr_apply_id= ?", null, array($this->apply_id), array('integer'), MDB2_FETCHMODE_ASSOC);

        if (PEAR::isError($row)) {
            throw new Exception(__METHOD__ . " ERROR: " . $row->getDebugInfo());
        }

        return $row;
    }

    /**
     * 未処理件数
     * @return type
     * @throws Exception
     */
    public function count()
    {
        $row = $this->db->extended->getRow("SELECT COUNT(*) AS count FROM ldr_apply_objection WHERE NOT read", null, null, null, MDB2_FETCHMODE_ASSOC);

        if (PEAR::isError($row)) {
            throw new Exception(__METHOD__ . " ERROR: " . $row->getDebugInfo());
        }

        return $row['count'];
    }

}
