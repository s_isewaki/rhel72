<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'ladder/class/Employee.php';
require_once 'ladder/class/Log.php';

class Ldr_Committee extends Model
{

    var $log;

    /**
     * コンストラクタ
     */
    public function Ldr_Committee()
    {
        parent::connectDB();
        $this->log = new Ldr_log('permission');
    }

    /**
     * 一覧の取得
     * @return Array
     */
    public function lists()
    {
        $sql = "SELECT emp_id, auditor FROM ldr_committee JOIN empmst USING (emp_id) JOIN stmst ON emp_st=st_id ORDER BY stmst.order_no, empmst.emp_kn_lt_nm, emp_id";
        $sth = $this->db->prepare($sql, array('text', 'boolean'), MDB2_PREPARE_RESULT);
        $res = $sth->execute();
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = new Ldr_Employee($row['emp_id']);
        }
        $sth->free();
        return $lists;
    }

    /**
     * 認定委員会メンバの取得
     * @return Array
     */
    public function committee()
    {
        $sql = "SELECT emp_id FROM ldr_committee";
        $res = $this->db->query($sql);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row['emp_id'];
        }
        return $lists;
    }

    /**
     * 監査者設定権限者の取得
     * @return Array
     */
    public function auditor()
    {
        $sql = "SELECT emp_id FROM ldr_committee WHERE auditor";
        $res = $this->db->query($sql);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row['emp_id'];
        }
        return $lists;
    }

    /**
     * 保存
     * @param Array $data
     */
    public function save($data)
    {
        $this->db->beginNestedTransaction();

        // DELETE
        $res_d = $this->db->exec('DELETE FROM ldr_committee');
        if (PEAR::isError($res_d)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . " ERROR: " . $res_d->getDebugInfo());
        }

        // INSERT
        $sth = $this->db->prepare("INSERT INTO ldr_committee (emp_id, auditor) VALUES (?, ?)", array('text', 'boolean'), MDB2_PREPARE_MANIP);
        foreach ($data['emp_id'] as $emp_id) {
            $auditor = $data['auditor'][$emp_id] === '1' ? true : false;
            $msg .= $emp_id . "=" . ($data['auditor'][$emp_id] === '1' ? '1' : '0') . " ";
            $res = $sth->execute(array($emp_id, $auditor));
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
            }
        }
        $this->db->completeNestedTransaction();
        $sth->free();

        // Log
        $this->log->log('認定委員会を保存しました', $msg);
    }

}
