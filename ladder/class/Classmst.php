<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'Cmx/Model/EmployeeClass.php';
require_once 'ladder/class/Config.php';

/**
 * 部署情報
 */
class Ldr_Classmst extends Cmx_EmployeeClass
{

    var $log;
    var $_classname;

    /**
     * コンストラクタ
     */
    public function __construct()
    {
        parent::connectDB();
    }

    /**
     * 部署一覧の取得
     * @return Array
     */
    public function lists()
    {
        $sql = "
            SELECT
                c1.class_id,
                c2.atrb_id,
                c3.dept_id,
                c4.room_id,
                class_nm,
                atrb_nm,
                dept_nm,
                room_nm
            FROM classmst c1
            JOIN atrbmst c2 ON c1.class_id=c2.class_id AND NOT atrb_del_flg
            JOIN deptmst c3 ON c3.atrb_id=c2.atrb_id AND NOT dept_del_flg
            LEFT JOIN classroom c4 ON c4.dept_id=c3.dept_id AND NOT room_del_flg
            WHERE NOT class_del_flg
            ORDER BY c1.order_no, c2.order_no, c3.order_no, c4.order_no, c1.class_id, c2.atrb_id, c3.dept_id, c4.room_id
        ";
        $res = $this->db->query($sql);

        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }
        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row;
        }
        return $lists;
    }

    /**
     * 分析対象部署一覧の取得
     * @return Array
     */
    public function analysis_lists()
    {
        $conf = new Ldr_Config();
        $view_class = $conf->view_class();
        $class = Ldr_Util::class_full_name_sql($view_class);
        $sql = "
            SELECT
                c1.class_id,
                c2.atrb_id,
                c3.dept_id,
                c4.room_id,
                $class,
                class_nm,
                atrb_nm,
                dept_nm,
                room_nm
            FROM classmst c1
            JOIN atrbmst c2 ON c1.class_id=c2.class_id AND NOT atrb_del_flg
            JOIN deptmst c3 ON c3.atrb_id=c2.atrb_id AND NOT dept_del_flg
            LEFT JOIN classroom c4 ON c4.dept_id=c3.dept_id AND NOT room_del_flg
            JOIN ldr_analysis_authority auth ON auth.class_id = c1.class_id
                AND auth.atrb_id = c2.atrb_id
                AND auth.dept_id = c3.dept_id
                AND (auth.room_id=c4.room_id OR (auth.room_id IS NULL AND c4.room_id IS NULL))
                AND auth.auth_flg
                AND NOT auth.del_flg
            WHERE NOT class_del_flg
            ORDER BY c1.order_no, c2.order_no, c3.order_no, c4.order_no, c1.class_id, c2.atrb_id, c3.dept_id, c4.room_id
        ";
        $res = $this->db->query($sql);

        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }
        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row;
        }
        return $lists;
    }

    /**
     * プルダウン用組織1,2一覧の取得
     * @return Array
     */
    public function class12_lists()
    {
        $sql = "
            SELECT
                c1.class_id,
                class_nm,
                atrb_id,
                atrb_nm
            FROM classmst c1
            JOIN atrbmst c2 ON c1.class_id=c2.class_id AND NOT atrb_del_flg
            WHERE NOT class_del_flg
            ORDER BY c1.order_no, c2.order_no, c1.class_id, c2.atrb_id
            ";
        $res = $this->db->query($sql);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row;
        }
        return $lists;
    }

    /**
     * プルダウン用組織3,4一覧の取得
     * @return Array
     */
    public function class34_lists($class1_id, $class2_id = null)
    {
        if (is_null($class2_id)) {
            list($class1_id, $class2_id) = explode('_', $class1_id);
        }

        $sql = "
            SELECT
                c3.dept_id,
                dept_nm,
                room_id,
                room_nm
            FROM deptmst c3
            LEFT JOIN classroom c4 ON c3.dept_id=c4.dept_id AND NOT room_del_flg
            WHERE NOT dept_del_flg
              AND c3.atrb_id = ?
            ORDER BY c3.order_no, c4.order_no, c3.dept_id, c4.room_id
            ";
        $sth = $this->db->prepare($sql);
        $res = $sth->execute(array($class2_id));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row;
        }
        return $lists;
    }

}
