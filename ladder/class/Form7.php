<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'ladder/class/Validation.php';
require_once 'ladder/class/Apply.php';
require_once 'ladder/class/Guideline.php';
require_once 'ladder/class/Log.php';

class Ldr_Form7 extends Model
{

    var $emp_id;
    var $log;

    /**
     * コンストラクタ
     * @param type $emp_id
     */
    public function Ldr_Form7($emp_id)
    {
        parent::connectDB();
        $this->emp_id = $emp_id;
        $this->log = new Ldr_log('form7');
    }

    /**
     * 様式7-1 validate
     * @param type $data
     * @return string
     */
    public function validate_form71($data, $rc = null)
    {
        $error = array();

        // 知識
        if (Validation::is_empty($data['knowledge']) && Validation::is_empty($data['knowledge'])) {
            $error['knowledge'][] = ($rc === '2' ? '赤十字' : '知識') . 'が記入されていません';
        }
        // 判断
        if (Validation::is_empty($data['decision']) && Validation::is_empty($data['decision'])) {
            $error['decision'][] = ($rc === '2' ? '管理過程' : '判断') . 'が記入されていません';
        }
        // 行為
        if (Validation::is_empty($data['action']) && Validation::is_empty($data['action'])) {
            $error['action'][] = ($rc === '2' ? '意思決定' : '行為') . 'が記入されていません';
        }
        // 行為の結果
        if (Validation::is_empty($data['result']) && Validation::is_empty($data['result'])) {
            $error['result'][] = ($rc === '2' ? '質保証' : '行為の結果') . 'が記入されていません';
        }
        if ($rc === '2') {
            if (Validation::is_empty($data['comment5']) && Validation::is_empty($data['comment5'])) {
                $error['comment5'][] = '人材育成が記入されていません';
            }
            if (Validation::is_empty($data['comment6']) && Validation::is_empty($data['comment6'])) {
                $error['comment6'][] = '対人関係が記入されていません';
            }
            if (Validation::is_empty($data['comment7']) && Validation::is_empty($data['comment7'])) {
                $error['comment7'][] = 'セルフマネジメントが記入されていません';
            }
        }

        return $error;
    }

    /**
     * 様式7-3 validate
     * @param type $data
     * @return string
     */
    public function validate_form73($data)
    {
        $error = array();

        // 総合レベル
        if (Validation::is_empty($data['level'])) {
            $error['level'][] = '総合レベルが入力されていません';
        }
        else {
            $level = intval($data['level']);
            if ($level !== 1 && $level !== 2) {
                $error['level'][] = '総合レベルが正しくありません';
            }
        }
        // アドバイス
        if (Validation::is_empty($data['advice']) && Validation::is_empty($data['advice'])) {
            $error['advice'][] = 'アドバイスが記入されていません';
        }

        return $error;
    }

    /**
     * 1件取得
     * @param Integer $id
     * @return Array
     */
    public function find($id)
    {
        if (is_null($id)) {
            return array();
        }

        $sql = "SELECT
                    emp_id,
                    knowledge,
                    decision,
                    action,
                    result,
                    level,
                    advice,
                    updated_on,
                    comment5,
                    comment6,
                    comment7
                FROM
                    ldr_form7
                WHERE
                    ldr_apply_id= :ldr_apply_id
                    AND emp_id= :emp_id
                    AND NOT del_flg";

        $row = $this->db->extended->getRow(
            $sql, null, array('ldr_apply_id' => $id, 'emp_id' => $this->emp_id), array('integer'), MDB2_FETCHMODE_ASSOC
        );
        if (PEAR::isError($row)) {
            throw new Exception(__METHOD__ . " ERROR: " . $row->getDebugInfo());
        }
        return $row;
    }

    /**
     * 一覧取得
     * @param type $id 申請ID
     * @return type
     */
    public function lists($id)
    {
        $sql = "SELECT
                    emp_id,
                    knowledge,
                    decision,
                    action,
                    result,
                    level,
                    advice,
                    updated_on,
                    comment5,
                    comment6,
                    comment7
                FROM
                    ldr_form7
                WHERE
                    ldr_apply_id= :ldr_apply_id
                    AND NOT del_flg";

        $sth = $this->db->prepare($sql, array('integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array('ldr_apply_id' => $id));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[$row['emp_id']] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 様式7-1 保存
     * @param Array $data
     */
    public function save_form71($data, $mode = null)
    {
        if ($mode === 'auto') {
            $data['knowledge'] = from_utf8($data['knowledge']);
            $data['decision'] = from_utf8($data['decision']);
            $data['action'] = from_utf8($data['action']);
            $data['result'] = from_utf8($data['result']);
            $data['comment5'] = from_utf8($data['comment5']);
            $data['comment6'] = from_utf8($data['comment6']);
            $data['comment7'] = from_utf8($data['comment7']);
        }

        $this->db->beginNestedTransaction();

        $sth = $this->db->prepare("SELECT update_ldr_form7_1(:id, :emp_id, :knowledge, :decision, :action, :result, :comment5, :comment6, :comment7)", array('integer', 'text', 'text', 'text', 'text', 'text', 'text', 'text', 'text'), MDB2_PREPARE_MANIP);
        $res = $sth->execute(array_merge($data, array('emp_id' => $this->emp_id)));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }
        $sth->free();

        $this->db->completeNestedTransaction();

        if ($mode !== 'auto') {
            $this->log->log("様式7-1 保存しました", "id={$data['id']}");
        }
    }

    /**
     * 様式7-3 保存
     * @param Array $data
     */
    public function save_form73($data, $mode = null)
    {
        if ($mode === 'auto') {
            $data['level'] = $data['level'] ? $data['level'] : null;
            $data['advice'] = from_utf8($data['advice']);
        }

        $this->db->beginNestedTransaction();

        $sth = $this->db->prepare("SELECT update_ldr_form7_3(:id, :emp_id, :level, :advice)", array('integer', 'text', 'integer', 'text'), MDB2_PREPARE_MANIP);
        $res = $sth->execute(array_merge($data, array('emp_id' => $this->emp_id)));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }
        $sth->free();

        $this->db->completeNestedTransaction();

        if ($mode !== 'auto') {
            $this->log->log("様式7-3 保存しました", "id={$data['id']}");
        }
    }

    /**
     * 評価データの取得
     * @param type $id
     * @param type $level
     * @param type $apply_emp_id
     * @param type $accept_emp_id
     * @return type
     */
    public function assessment_lists($id, $level, $apply_emp_id, $accept_emp_id)
    {
        $sql = "
            SELECT
                g.guideline_id,
                g.guideline_group1,
                g.guideline_group2,
                g.guideline,
                g.criterion,
                a.assessment as apply_assessment,
                a.reason as apply_reason,
                b.assessment as accept_assessment,
                b.reason as accept_reason,
                g2.name as group2_name,
                g2.comment as group2_comment
            FROM ldr_guideline g
            JOIN ldr_apply apply USING (guideline_revision)
            JOIN ldr_guideline_group1 g1 ON g.guideline_group1=g1.guideline_group1 AND NOT g1.del_flg AND g1.form7
            JOIN ldr_guideline_group2 g2 ON g.guideline_group2=g2.guideline_group2 AND NOT g2.del_flg
            LEFT JOIN ldr_form7_2 a ON a.ldr_apply_id= apply.ldr_apply_id AND a.guideline_id=g.guideline_id AND a.emp_id= :apply_emp_id
            LEFT JOIN ldr_form7_2 b ON b.ldr_apply_id= apply.ldr_apply_id AND b.guideline_id=g.guideline_id AND b.emp_id= :accept_emp_id
            WHERE NOT g.del_flg
              AND g.level= :level
              AND apply.ldr_apply_id= :ldr_apply_id
            ORDER BY g.order_no, g.guideline_id
            ";
        $sth = $this->db->prepare($sql, null, MDB2_PREPARE_RESULT);
        $res = $sth->execute(array(
            'ldr_apply_id' => $id,
            'apply_emp_id' => $apply_emp_id,
            'accept_emp_id' => $accept_emp_id,
            'level' => $level,
            )
        );
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $group2_comment = unserialize($row['group2_comment']);
            $row['group2_comment'] = $group2_comment[$level];
            $lists[$row['guideline_group1']][] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 評価データの取得(評価者/申請者)
     * @param type $id
     * @param type $level
     * @return type
     */
    public function assessment_lists_all($id, $level)
    {
        $sql = "
            SELECT
                a.emp_id,
                g.guideline_id,
                g.guideline_group1,
                g.guideline_group2,
                g.guideline,
                g.criterion,
                a.assessment as assessment,
                a.reason as reason,
                g2.name as group2_name,
                g2.comment as group2_comment
            FROM ldr_guideline g
            JOIN ldr_apply apply USING (guideline_revision)
            JOIN ldr_guideline_group1 g1 ON g.guideline_group1=g1.guideline_group1 AND NOT g1.del_flg AND g1.form7
            JOIN ldr_guideline_group2 g2 ON g.guideline_group2=g2.guideline_group2 AND NOT g2.del_flg
            LEFT JOIN ldr_form7_2 a ON a.ldr_apply_id= apply.ldr_apply_id AND a.guideline_id=g.guideline_id
            WHERE NOT g.del_flg
              AND g.level= :level
              AND apply.ldr_apply_id= :ldr_apply_id
            ORDER BY g.order_no, g.guideline_id
            ";
        $sth = $this->db->prepare($sql, array('integer', 'integer', 'integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array(
            'ldr_apply_id' => $id,
            'level' => $level,
            )
        );
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['group2_comment'] = unserialize($row['group2_comment']);
            $lists[$row['guideline_group1']][$row['guideline_id']][$row['emp_id']] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 自己評価からのインポート
     * @param type $id
     * @param type $level
     * @param type $emp_id
     * @throws Exception
     */
    public function import($id, $level, $emp_id, $revision)
    {
        // 年度
        if (date('m') < 4) {
            $year = date('Y') - 1;
        }
        else {
            $year = date('Y');
        }

        $sql = "
            SELECT
                a.guideline_id,
                a.assessment,
                a.reason
            FROM ldr_assessment a
            JOIN ldr_guideline g USING (guideline_id)
            WHERE NOT a.del_flg
              AND NOT g.del_flg
              AND a.emp_id= :emp_id
              AND a.year= :year
              AND g.guideline_revision= :revision
              AND g.level= :level
            ";
        $sth = $this->db->prepare($sql, array('text', 'integer', 'integer', 'integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array(
            'emp_id' => $emp_id,
            'year' => $year,
            'revision' => $revision,
            'level' => $level,
            )
        );
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }

        $this->db->beginNestedTransaction();

        $sql_upd = 'SELECT update_ldr_form7_2(:ldr_apply_id, :emp_id, :guideline_id, :assessment, :reason)';
        $sth_upd = $this->db->prepare($sql_upd, array('integer', 'text', 'integer', 'integer', 'text'), MDB2_PREPARE_MANIP);
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $res_upd = $sth_upd->execute(array(
                'ldr_apply_id' => $id,
                'emp_id' => $emp_id,
                'guideline_id' => $row['guideline_id'],
                'assessment' => $row['assessment'],
                'reason' => $row['reason'],
            ));
            if (PEAR::isError($res_upd)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " ERROR: " . $res_upd->getDebugInfo());
            }
        }

        $this->db->completeNestedTransaction();
        $this->log->log("自己評価をインポートしました", "id={$id} level={$level}");
    }

    /**
     * 様式7-2保存
     * @param type $data
     * @throws Exception
     */
    public function save_form72($data, $mode = null)
    {

        $this->db->beginNestedTransaction();

        $sql_upd = 'SELECT update_ldr_form7_2(:ldr_apply_id, :emp_id, :guideline_id, :assessment, :reason)';
        $sth_upd = $this->db->prepare($sql_upd, array('integer', 'text', 'integer', 'integer', 'text'), MDB2_PREPARE_MANIP);

        foreach ($data['assessment'] as $guideline_id => $assessment) {
            $res_upd = $sth_upd->execute(array(
                'ldr_apply_id' => $data['id'],
                'emp_id' => $this->emp_id,
                'guideline_id' => $guideline_id,
                'assessment' => $assessment,
                'reason' => ($mode !== 'auto' ? $data['reason'][$guideline_id] : from_utf8($data['reason'][$guideline_id])),
            ));
            if (PEAR::isError($res_upd)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " ERROR: " . $res_upd->getDebugInfo());
            }
        }

        $this->db->completeNestedTransaction();

        if ($mode !== 'auto') {
            $this->log->log("様式7-2を保存しました", "id={$data['id']}");
        }
    }

}
