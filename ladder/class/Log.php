<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';

class Ldr_Log extends Model
{

    var $function;
    var $ins_sth;

    /**
     * ���󥹥ȥ饯��
     */
    public function Ldr_Log($function)
    {
        parent::connectDB();
        $this->function = $function;
        $this->ins_sth = $this->db->prepare("INSERT INTO ldr_log (emp_id, function, message, param) VALUES (?, ?, ?, ?)", array('text', 'text', 'text', 'text'), MDB2_PREPARE_MANIP);
    }

    /**
     * ����
     * @param type $message
     * @param type $param
     * @throws Exception
     */
    public function Log($message, $param = null)
    {
        $this->db->beginNestedTransaction();

        $res = $this->ins_sth->execute(array($GLOBALS['_session_emp_id'], $this->function, $message, $param));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception("ldr_log INSERT ERROR: {$GLOBALS['_session_emp_id']} " . $res->getDebugInfo());
        }

        $this->db->completeNestedTransaction();
    }

    /**
     * �������κ���
     * @param type $cond
     * @return string
     */
    protected function make_condition($cond = null)
    {
        // ̾��
        if (!empty($cond['srh_name'])) {
            $where[] = "e.emp_ft_nm || ' ' || e.emp_lt_nm || ' ' || e.emp_kn_ft_nm || ' ' || e.emp_kn_lt_nm || ' ' || e.emp_personal_id ILIKE :srh_name";
            $types[] = 'text';
            $data['srh_name'] = "%" . $cond['srh_name'] . "%";
        }

        // ��ǽ
        if (!empty($cond['srh_function'])) {
            $where[] = " function = :srh_function";
            $types[] = 'text';
            $data['srh_function'] = $cond['srh_function'];
        }

        // ������
        if (!empty($cond['srh_start_date'])) {
            $where[] = " :srh_start_date <= timestamp";
            $types[] = 'text';
            $data['srh_start_date'] = $cond['srh_start_date'];
        }

        // ��λ��
        if (!empty($cond['srh_last_date'])) {
            $where[] = " to_char(timestamp,'YYYY-MM-DD') <= :srh_last_date";
            $types[] = 'text';
            $data['srh_last_date'] = $cond['srh_last_date'];
        }

        if (empty($where)) {
            return array();
        }


        return array(
            'cond' => 'WHERE ' . implode(' AND ', $where),
            'types' => $types,
            'data' => $data,
        );
    }

    /**
     * �ǡ�������������ޤ�
     * @return type
     */
    public function count($cond = null)
    {
        $ret = $this->make_condition($cond);

        $sql = "SELECT count(*) FROM ldr_log JOIN empmst e USING (emp_id) ";
        $sql.= $ret['cond'];

        $sth = $this->db->prepare($sql, array_merge(array('integer', 'text'), $ret['types']), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array_merge(array('emp_id' => $this->emp_id), $ret['data']));
        if (PEAR::isError($res)) {
            throw new Exception('ldr_log SELECT ERROR: ' . $res->getDebugInfo());
        }
        $count = $res->fetchOne();
        $sth->free();
        return $count;
    }

    /**
     * �����μ���
     * @param type $st
     * @param type $pageCount
     * @param type $cond
     * @return type
     */
    public function lists($st = null, $pageCount = null, $cond = null)
    {
        $ret = $this->make_condition($cond);

        $sql = "SELECT timestamp, emp_personal_id, e.emp_lt_nm || ' ' || emp_ft_nm AS emp_name, function, message, param FROM ldr_log JOIN empmst e USING (emp_id) ";
        $sql .= $ret['cond'];
        $sql .= " ORDER BY timestamp DESC";

        if (isset($st) && isset($pageCount)) {
            $this->db->setLimit($pageCount, $st);
        }

        $sth = $this->db->prepare($sql, $ret['types'], MDB2_PREPARE_RESULT);
        $res = $sth->execute($ret['data']);
        if (PEAR::isError($res)) {
            throw new Exception('ldr_log SELECT ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * ��ǽ̾��������������
     * @return type
     */
    public function function_name()
    {
        return array(
            'assessment' => '����ɾ��',
            'novice' => '����ɾ��',
            'form1' => '�ͼ�1',
            'form21' => '�ͼ�2-I',
            'form22' => '�ͼ�2-II',
            'form23' => '�ͼ�2-III',
            'form3' => '�ͼ�3',
            'form4' => '�ͼ�4',
            'form51' => '�ͼ�5-I',
            'form52' => '�ͼ�5-II',
            'form53' => '�ͼ�5-III',
            'form54' => '�ͼ�5-IV',
            'form6' => '�ͼ�6',
            'form7' => '�ͼ�7',
            'form8' => '�ͼ�8',
            'form9' => '�ͼ�9',
            'formcarrier' => '�����ư����',
            'formtransfer' => '¾���Ǹ�и�',
            'formpresentation' => '�ز�ȯɽ��Ͽ',
            'formarticle' => '�ز�Ǻܵ�Ͽ',
            'apply_result' => '����Ƚ��',
            'apply' => '����',
            'accept' => '����',
            'comment' => '������',
            'auditor' => '�ƺ�������',
            'inside' => '���⸦��',
            'outside' => '��������',
            'relief' => '�߸������',
            'appraiser' => 'ɾ����',
            'report' => '��ݡ���ɾ��',
            'analysis_assessment' => 'ɾ��ʬ��',
            'analysis_ladder' => '�����ʬ��',
            'analysis_placement' => '����ɽ',
            'config_user' => '�桼������',
            'config' => '[��������]����',
            'employee' => '[��������]����',
            'permission' => '[��������]����',
            'ladder' => '[���������]����',
            'guideline' => '[���������]��ư��ɸ',
            'adm_novice' => '[����ɾ������]',
            'item' => '[̾�δ���]',
            'adm_relief' => '[��������]�߸������:�ز�',
            'adm_relief_detail' => '[��������]�߸������:��������',
            'check_template' => '[��������]�����֤�ƥ�ץ졼��',
            'inside_mst' => '[��������]���⸦���ޥ���',
            'inside_check' => '[��������]���⸦�������֤�',
        );
    }

}
