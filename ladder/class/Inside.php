<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'ladder/class/Log.php';
require_once 'ladder/class/InsideMst.php';
require_once 'ladder/class/Config.php';
require_once 'ladder/class/Validation.php';
require_once 'ladder/class/Form22.php';

/**
 * 院内研修
 */
class Ldr_Inside extends Model
{

    var $log;
    var $emp_id;
    var $mst;
    var $class_sql;
    var $hospital_name;

    /**
     * コンストラクタ
     * @param type $emp_id 職員ID
     */
    public function Ldr_Inside($emp_id)
    {
        parent::connectDB();
        $this->emp_id = $emp_id;
        $this->log = new Ldr_log('inside');
        $this->mst = new Ldr_InsideMst();
        $conf = new Ldr_Config();
        $this->class_sql = Ldr_Util::class_full_name_sql($conf->view_class());
    }

    /**
     * validate(申請)
     * @param type $data
     * @return string
     */
    public function validate_apply($data)
    {
        $error = array();

        // 評価
        if (Validation::is_empty($data['num_ary'])) {
            $error['number'][] = '申し込む講座を選択してください';
        }

        return $error;
    }

    /**
     * validate(課題)
     * @param type $data
     * @return string
     */
    public function validate_work($data)
    {
        $error = array();

        // 評価
        if (Validation::is_empty($data['work'])) {
            $error['work'][] = '課題が入力されていません';
        }

        return $error;
    }

    /**
     * validate(振り返り)
     * @param type $forms
     * @param type $data
     * @return string
     */
    public function validate_check($forms, $data)
    {
        $error = array();

        foreach ($forms as $key => $form) {
            if (!empty($form['required']) && Validation::is_empty($data['form'][$key]['value'])) {
                $error[$key][] = $form['name'] . 'が入力されていません';
            }
        }

        return $error;
    }

    /**
     * 年度リスト
     * @return type
     */
    public function year_lists()
    {
        return $this->mst->year_lists();
    }

    /**
     * 一件取得
     * @param type $id training_id
     * @return type
     * @throws Exception
     */
    public function find($id)
    {
        if (is_null($id)) {
            return array();
        }

        $sql = "SELECT
                    trn.training_id,
                    trn.emp_id,
                    trn.status,
                    trn.attendance,
                    trn.inside_date_id,
                    trn.updated_on,
                    bf.status as before_status,
                    bf.work as before_work,
                    af.status as after_status,
                    af.work as after_work,
                    ck.data as form_data,
                    ck.status as check_status,
                    mck.form,
                    d.inside_id,
                    d.place,
                    d.maximum,
                    d.training_date,
                    d.training_start_time,
                    d.training_last_time,
                    d.type,
                    c.inside_contents_id,
                    c.number,
                    m.planner,
                    m.coach,
                    m.approve,
                    m.year,
                    m.no,
                    m.title,
                    m.level,
                    m.number_of,
                    m.guideline_group1
                FROM ldr_trn_inside trn
                JOIN ldr_mst_inside_date d ON trn.inside_date_id = d.inside_date_id AND NOT d.del_flg
                JOIN ldr_mst_inside_contents c ON d.inside_contents_id = c.inside_contents_id AND NOT c.del_flg
                JOIN ldr_mst_inside m ON m.inside_id = d.inside_id AND NOT m.del_flg
                LEFT JOIN ldr_mst_inside_check mck ON c.inside_contents_id = mck.inside_contents_id
                LEFT JOIN ldr_trn_inside_before_work bf ON trn.training_id = bf.training_id AND NOT bf.del_flg
                LEFT JOIN ldr_trn_inside_after_work af ON trn.training_id = af.training_id AND NOT af.del_flg
                LEFT JOIN ldr_trn_inside_check ck ON trn.training_id = ck.training_id AND NOT ck.del_flg
                WHERE
                    NOT trn.del_flg
                    AND trn.training_id= :training_id";

        $row = $this->db->extended->getRow(
            $sql, null, array('training_id' => $id), array('integer'), MDB2_FETCHMODE_ASSOC
        );
        if (PEAR::isError($row)) {
            throw new Exception(__METHOD__ . " ERROR: " . $row->getDebugInfo());
        }
        $sth_p = $this->db->prepare("SELECT emp_id, e.emp_lt_nm || ' ' || e.emp_ft_nm as emp_name FROM ldr_mst_inside_planner JOIN empmst e USING(emp_id) WHERE inside_id = ? ORDER BY order_no, emp_kn_ft_nm", array('integer'), MDB2_PREPARE_RESULT);
        $sth_c = $this->db->prepare("SELECT emp_id, e.emp_lt_nm || ' ' || e.emp_ft_nm as emp_name FROM ldr_mst_inside_coach   JOIN empmst e USING(emp_id) WHERE inside_id = ? ORDER BY order_no, emp_kn_ft_nm", array('integer'), MDB2_PREPARE_RESULT);

        // planner
        $res_p = $sth_p->execute(array($row['inside_id']));
        if (PEAR::isError($res_p)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res_p->getDebugInfo());
        }
        while ($row_p = $res_p->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['emp_planner'][] = $row_p;
        }

        // coach
        $res_c = $sth_c->execute(array($row['inside_id']));
        if (PEAR::isError($res_c)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res_c->getDebugInfo());
        }
        while ($row_c = $res_c->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['emp_coach'][] = $row_c;
        }
        if (!empty($row['form'])) {
            $row['form'] = unserialize($row['form']);
        }
        if (!empty($row['form_data'])) {
            $row['form_data'] = unserialize($row['form_data']);
        }
        return $row;
    }

    /**
     * 一件取得 (研修日程付き)
     *
     * @param type $id
     */
    public function find_course_date($id)
    {
        if (is_null($id)) {
            return array();
        }
        $data = $this->mst->find_contents($id);

        // ldr_mst_inside_date
        $sql = "
                SELECT
                    d.*,
                    c.number,
                    c.contents,
                    c.objective,
                    e.emp_lt_nm || ' ' || e.emp_ft_nm as update_emp_name,
                    (SELECT count(*)
                     FROM ldr_trn_inside trn
                     WHERE trn.inside_date_id=d.inside_date_id
                        AND trn.status >= 0 AND trn.emp_id=:emp_id AND NOT del_flg) as applied,
                    (SELECT count(*)
                     FROM ldr_trn_inside trn
                     WHERE trn.inside_date_id=d.inside_date_id AND trn.status >= 0 AND NOT trn.del_flg) as count
                FROM ldr_mst_inside_date d
                JOIN ldr_mst_inside_contents c USING (inside_contents_id)
                JOIN empmst e ON e.emp_id=d.update_emp_id
                WHERE
                  NOT d.del_flg
                  AND d.inside_id= :id
                ORDER BY c.number asc,training_date, training_start_time, inside_date_id
            ";
        $sth = $this->db->prepare($sql, array('integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array('emp_id' => $this->emp_id, 'id' => $id));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }

        $apply = array();
        $reception_m1 = 0; //準備中件数
        $reception_0 = 0; //受付中件数

        $course_applied = false;
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['reception'] = Ldr_Util::get_inside_reception($row);
            $row['reception_text'] = Ldr_Const::get_inside_reception($row['reception']);
            $data['dates'][] = $row;

            $apply[$row['number']] += $row['applied'];
            if ($row['reception'] === '-1') {
                $reception_m1++;
            }
            else if ($row['reception'] === '0') {
                $reception_0++;
            }
            if ($row['applied'] > 0) {
                $course_applied = true;
            }
        }

        $enable_apply = false;
        foreach ($data['dates'] as $value) {
            if ($apply[$value['number']] == 0 && $value['reception'] === '0') {
                $enable_apply = true;
            }
        }
        //申請の可能状態
        $data['enable_apply'] = $enable_apply;

        //申請済みの有無
        $data['course_applied'] = $course_applied;
        $sth->free();

        return $data;
    }

    /**
     * 申請の保存
     * @param type $data
     */
    public function save_apply($data)
    {
        $this->db->beginNestedTransaction();
        $status = ($data['approve'] === 't') ? 1 : 2;
        foreach ($data['num_ary'] as $key => $value) {
            $parm = array('number' => $key, 'inside_date_id' => $value, 'status' => $status);
            $this->save($parm);
        }
        $this->db->completeNestedTransaction();
    }

    /**
     * 保存
     * @param array $data
     * @throws Exception
     */
    public function save($data)
    {
        if (!$data['emp_id']) {
            $data['emp_id'] = $this->emp_id;
        }

        $this->db->beginNestedTransaction();

        // INSERT
        if (empty($data['training_id'])) {
            $sth = $this->db->prepare("INSERT INTO ldr_trn_inside (emp_id, status, inside_date_id) VALUES (:emp_id, :status, :inside_date_id)", array('text', 'integer', 'integer'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($data);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
            }
            $data['training_id'] = $this->db->lastInsertID('ldr_trn_inside', 'training_id');
            $sth->free();
        }
        else {
            $sth = $this->db->prepare("UPDATE ldr_trn_inside SET emp_id= :emp_id, status= :status ,inside_date_id= :inside_date_id WHERE training_id= :training_id", array('text', 'integer', 'integer', 'integer'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($data);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
            }
            $sth->free();
        }

        $this->db->completeNestedTransaction();

        // Log
        $this->log->log("保存しました", "inside_date_id={$data['inside_date_id']} status={$data['status']} training_id={$data['training_id']} ");
    }

    /**
     * 申請の検索条件
     * @param type $cond
     * @return type
     */
    protected function make_apply_condition($cond = null)
    {
        if (empty($cond)) {
            return array('cond' => '', 'types' => array(), 'data' => array());
        }

        $cond_ret = "";
        $types = array();
        $data = array();

        if (!empty($cond['srh_keyword'])) {
            $keywords = preg_split(Ldr_Const::SRH_DELIMITERS, $cond['srh_keyword'], -1, PREG_SPLIT_NO_EMPTY);
            $cond_key.= "AND ( ";
            foreach ($keywords as $key => $value) {
                if ($key !== 0) {
                    $cond_key.= " AND ";
                }

                $label_key = "key" . $key;
                $cond_key.= "(d.place || ' ' || m.title || ' ' || m.no) ILIKE :" . $label_key . " ";
                $types[] = 'text';
                $data[$label_key] = "%" . $value . "%";
            }
            $cond_key.= ") ";
            $cond_ret .= $cond_key;
        }

        if (!empty($cond['srh_year'])) {
            $types[] = 'integer';
            $cond_ret .= " AND m.year = :srh_year ";
            $data['srh_year'] = $cond['srh_year'];
        }

        if (!empty($cond['srh_training'])) {
            switch ($cond['srh_training']) {
                case 2:
                    break;
                // 振り返り未入力
                case 3:
                    $cond_ret .= " AND trn.status in (0,3) AND c.looking_back AND (d.type = 2 OR d.training_date + d.training_last_time < current_timestamp) AND (trn.attendance IS NULL OR trn.attendance != 2) AND (ck.status is null OR ck.status = 0) ";
                    break;
                case 4:
                    $cond_ret .= " AND d.type = 2 ";
                    break;
                case 1:
                default:
                    $cond_ret .= " AND ((d.training_date + d.training_last_time >= current_timestamp) OR (d.type = 2 AND trn.attendance IS NULL)) ";
                    break;
            }
        }

        if (!empty($cond['accepted'])) {
            $cond_ret .= " AND trn.status in (0, 3) ";
        }
        return array('cond' => $cond_ret, 'types' => $types, 'data' => $data);
    }

    /**
     * 申請件数
     * @param type $cond
     * @return type
     * @throws Exception
     */
    public function count_apply($cond = null)
    {
        $ret = $this->make_apply_condition($cond);

        $sql = "SELECT
                    count(*)
                FROM ldr_trn_inside trn
                JOIN ldr_mst_inside_date d ON trn.inside_date_id = d.inside_date_id AND NOT d.del_flg
                JOIN ldr_mst_inside_contents c ON d.inside_contents_id = c.inside_contents_id AND NOT c.del_flg
                JOIN ldr_mst_inside m ON m.inside_id = d.inside_id AND NOT m.del_flg
                LEFT JOIN ldr_trn_inside_check ck ON trn.training_id = ck.training_id AND NOT ck.del_flg
                WHERE
                    NOT trn.del_flg
                    AND trn.emp_id=:emp_id
                ";
        $sql.= $ret['cond'];
        $types = array_merge(array('text'), $ret['types']);
        $data = array_merge(array('emp_id' => $this->emp_id), $ret['data']);
        $count = $this->db->extended->getOne($sql, null, $data, $types);
        if (PEAR::isError($count)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $count->getDebugInfo());
        }

        return $count;
    }

    /**
     * 申請一覧
     * @param type $st
     * @param type $pageCount
     * @param type $cond
     * @return type
     * @throws Exception
     */
    public function lists_apply($st = null, $pageCount = null, $cond = null)
    {
        $ret = $this->make_apply_condition($cond);

        $sql = "SELECT
                    trn.training_id,
                    trn.emp_id,
                    trn.status,
                    trn.inside_date_id,
                    trn.updated_on,
                    d.status as d_status,
                    d.inside_id,
                    d.place,
                    d.type,
                    d.maximum,
                    d.training_date,
                    d.training_start_time,
                    d.training_last_time,
                    c.number,
                    c.before_work,
                    c.after_work,
                    c.looking_back,
                    bf.status as before_status,
                    af.status as after_status,
                    ck.status as check_status,
                    m.approve,
                    m.year,
                    m.no,
                    m.title,
                    m.level,
                    m.number_of
                FROM ldr_trn_inside trn
                JOIN ldr_mst_inside_date d
                    ON NOT d.del_flg
                    AND trn.inside_date_id = d.inside_date_id
                JOIN ldr_mst_inside_contents c
                    ON NOT c.del_flg
                    AND d.inside_contents_id = c.inside_contents_id
                JOIN ldr_mst_inside m
                    ON NOT m.del_flg
                    AND m.inside_id = d.inside_id
                LEFT JOIN ldr_trn_inside_before_work bf ON trn.training_id = bf.training_id AND NOT bf.del_flg
                LEFT JOIN ldr_trn_inside_after_work af ON trn.training_id = af.training_id AND NOT af.del_flg
                LEFT JOIN ldr_trn_inside_check ck ON trn.training_id = ck.training_id AND NOT ck.del_flg
                WHERE
                    NOT trn.del_flg
                    AND trn.emp_id=:emp_id
                ";
        $sql.= $ret['cond'];

        $types = array_merge(array('text'), $ret['types']);
        $data = array_merge(array('emp_id' => $this->emp_id), $ret['data']);
        $cond['order_by'][] = "d.training_date";
        $cond['order_by'][] = "d.training_start_time";
        $cond['order_by'][] = "m.no asc";
        $cond['order_by'][] = "c.number asc";
        $order = "";
        foreach ($cond['order_by'] as $value) {
            $order.= empty($order) ? "ORDER BY " : ", ";
            $order.= $value;
        }
        $sql .= $order;
        if (isset($st) && isset($pageCount)) {
            $this->db->setLimit($pageCount, $st);
        }

        $sth = $this->db->prepare($sql, $types, MDB2_PREPARE_RESULT);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['reception'] = Ldr_Util::get_inside_reception($row);
            $row['reception_text'] = Ldr_Const::get_inside_reception($row['reception']);
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 申請を取り下げる
     * @param type $data
     */
    public function cancel_apply($data)
    {
        $id = $data['training_id'];
        $this->status_change($id, "-1");

        $txt = is_array($id) ? implode(',', $id) : $id;
        $this->log->log("申請を取り下げました", "training_id={$txt}");
    }

    /**
     * ステータス更新
     * @param type $id        training_id
     * @param type $status    status
     * @throws Exception
     */
    private function status_change($id, $status)
    {
        $this->db->beginNestedTransaction();
        if (is_array($id)) {
            $holder = implode(',', array_fill(0, count($id), '?'));
            $sql_cond = " training_id IN($holder) ";
            $data = array_merge(array($status), $id);
            $types = array_merge(array('integer'), array_fill(0, count($id), 'integer'));
        }
        else {
            $sql_cond = "training_id= ?";
            $types = array('integer', 'integer');
            $data = array($status, $id);
        }

        $sql = "UPDATE ldr_trn_inside
                SET
                    status = ?
                WHERE
                    $sql_cond
                    AND NOT del_flg";
        $sth = $this->db->prepare($sql, $types, MDB2_PREPARE_MANIP);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }

        $sth->free();

        $this->db->completeNestedTransaction();
    }

    /**
     * 承認待ちの検索条件
     * @param type $cond
     * @return type
     */
    protected function make_approve_condition($cond = null)
    {
        $cond_ret = "";
        $types = array();
        $data = array();

        // 年度
        if (!empty($cond['srh_year'])) {
            $types[] = 'integer';
            $cond_ret .= " AND m.year = :srh_year ";
            $data['srh_year'] = $cond['srh_year'];
        }

        // キーワード
        if (!empty($cond['srh_keyword'])) {
            $keywords = preg_split(Ldr_Const::SRH_DELIMITERS, $cond['srh_keyword'], -1, PREG_SPLIT_NO_EMPTY);
            $cond_key.= "AND ( ";
            foreach ($keywords as $key => $value) {
                if ($key != 0) {
                    $cond_key.= " AND ";
                }
                $label_key = "key" . $key;
                $cond_key.= "(m.title || ' ' || m.no || ' ' || d.place || ' ' || e.emp_lt_nm || ' ' || e.emp_ft_nm ||' ' || e.emp_kn_lt_nm || ' ' || e.emp_kn_ft_nm ) ILIKE :" . $label_key . " ";
                $types[] = 'text';
                $data[$label_key] = "%" . $value . "%";
            }
            $cond_key.= ") ";
            $cond_ret .= $cond_key;
        }

        // 開始期間
        if (!empty($cond['srh_start_date'])) {
            $cond_ret .= " AND ( :srh_start_date <= d.training_date OR d.type = 2) ";
            $types[] = 'text';
            $data['srh_start_date'] = $cond['srh_start_date'];
        }

        // 終了期間
        if (!empty($cond['srh_last_date'])) {
            $cond_ret .= " AND (d.training_date <= :srh_last_date OR d.type = 2) ";
            $types[] = 'text';
            $data['srh_last_date'] = $cond['srh_last_date'];
        }

        // 研修
        if (!empty($cond['srh_inside'])) {
            $cond_ret .= " AND m.inside_id = :inside_id ";
            $types[] = 'integer';
            $data['inside_id'] = $cond['srh_inside'];
        }

        // 未承認
        if (!empty($cond['srh_nonapprove'])) {
            $cond_ret .= " AND trn.status = 1 ";
        }

        $ret['cond'] = $cond_ret;
        $ret['types'] = $types;
        $ret['data'] = $data;
        return $ret;
    }

    /**
     * 承認待ちの件数
     * @param type $cond
     * @return type
     */
    public function count_approve($cond = null)
    {
        $ret = $this->make_approve_condition($cond);

        $sql = "SELECT
                    COUNT(*)
                FROM ldr_trn_inside trn
                JOIN ldr_mst_inside_date d
                    ON NOT d.del_flg
                    AND trn.inside_date_id = d.inside_date_id
                JOIN ldr_mst_inside_contents c
                    ON NOT c.del_flg
                    AND d.inside_contents_id = c.inside_contents_id
                JOIN ldr_mst_inside m
                    ON NOT m.del_flg
                    AND m.inside_id = d.inside_id
                JOIN empmst e USING(emp_id)
                JOIN ldr_approver app
                    ON app.emp_id = :emp_id
                    AND app.class_id = e.emp_class
                    AND app.atrb_id = e.emp_attribute
                    AND app.dept_id = e.emp_dept
                    AND COALESCE(app.room_id,0) = COALESCE(e.emp_room,0)
                WHERE
                    NOT trn.del_flg
                    AND trn.status != '-1'";
        $sql.= $ret['cond'];
        $types = array_merge(array('text'), $ret['types']);
        $data = array_merge(array('emp_id' => $this->emp_id), $ret['data']);
        $count = $this->db->extended->getOne($sql, null, $data, $types);
        if (PEAR::isError($count)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $count->getDebugInfo());
        }
        return $count;
    }

    /**
     * 承認
     * @param type $data
     */
    public function save_approve($data)
    {
        $id = $data['training_id'];
        $this->status_change($id, "2");
        if (is_array($id)) {
            $this->log->log("申請を承認しました", "training_id=[" . implode(" ", $id) . "]");
        }
        else {
            $this->log->log("申請を承認しました", "training_id={$id}");
        }
    }

    /**
     * 申請を取り下げる
     * @param type $data
     */
    public function cancel_approve($data)
    {
        $id = $data['training_id'];
        $this->status_change($id, "-2");
        if (is_array($id)) {
            $this->log->log("申請を取り下げました", "training_id=[" . implode(" ", $id) . "]");
        }
        else {
            $this->log->log("申請を取り下げました", "training_id={$id}");
        }
    }

    /**
     * 承認待ちの一覧
     * @param type $st
     * @param type $pageCount
     * @param array $cond
     * @return type
     * @throws Exception
     */
    public function lists_approve($st = null, $pageCount = null, $cond = null)
    {

        $ret = $this->make_approve_condition($cond);

        $sql = "SELECT
                    trn.training_id,
                    e.emp_lt_nm || ' ' || e.emp_ft_nm as name,
                    trn.emp_id,
                    trn.status,
                    trn.inside_date_id,
                    trn.updated_on,
                    d.inside_id,
                    d.place,
                    d.maximum,
                    d.status as d_status,
                    d.type,
                    d.training_date,
                    d.training_start_time,
                    d.training_last_time,
                    d.accept_date,
                    d.deadline_date,
                    c.number,
                    m.approve,
                    m.year,
                    m.no,
                    m.title,
                    m.level,
                    m.number_of
                FROM ldr_trn_inside trn
                JOIN ldr_mst_inside_date d
                    ON NOT d.del_flg
                    AND trn.inside_date_id = d.inside_date_id
                JOIN ldr_mst_inside_contents c
                    ON NOT c.del_flg
                    AND d.inside_contents_id = c.inside_contents_id
                JOIN ldr_mst_inside m
                    ON NOT m.del_flg
                    AND m.inside_id = d.inside_id
                JOIN empmst e USING(emp_id)
                JOIN ldr_approver app
                    ON app.emp_id = :emp_id
                    AND app.class_id = e.emp_class
                    AND app.atrb_id = e.emp_attribute
                    AND app.dept_id = e.emp_dept
                    AND COALESCE(app.room_id,0) = COALESCE(e.emp_room,0)
                WHERE
                    NOT trn.del_flg
                    AND trn.status != '-1'";
        $sql.= $ret['cond'];
        $types = array_merge(array('text'), $ret['types']);
        $data = array_merge(array('emp_id' => $this->emp_id), $ret['data']);
        $cond['order_by'][] = "d.training_start_time asc";
        $cond['order_by'][] = "d.inside_id asc";
        $cond['order_by'][] = "m.no asc";
        $cond['order_by'][] = "training_id";
        $order = "";
        foreach ($cond['order_by'] as $value) {
            $order.= empty($order) ? "ORDER BY " : ", ";
            $order.= $value;
        }
        $sql .= $order;
        if (isset($st) && isset($pageCount)) {
            $this->db->setLimit($pageCount, $st);
        }

        $sth = $this->db->prepare($sql, $types, MDB2_PREPARE_RESULT);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['status_text'] = Ldr_Const::get_training_status($row['status']);
            $row['reception'] = Ldr_Util::get_inside_reception($row);
            $row['reception_text'] = Ldr_Const::get_inside_reception($row['reception']);
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 受付一覧検索条件
     * @param type $cond
     * @return type
     */
    protected function make_accept_condition($cond = null)
    {
        $cond_ret = "";
        $types = array();
        $data = array();

        if (empty($cond['srh_planner'])) {
            $cond_ret .= " AND (EXISTS (SELECT * FROM ldr_mst_inside_planner WHERE inside_id=m.inside_id AND emp_id = :emp_id)
                           OR EXISTS (SELECT * FROM ldr_permission_inside WHERE emp_id = :emp_id))";
        }
        else {
            $cond_ret .= " AND EXISTS (SELECT * FROM ldr_mst_inside_planner WHERE inside_id=m.inside_id AND emp_id = :emp_id)";
        }

        if (!empty($cond['srh_year'])) {
            $types[] = 'integer';
            $cond_ret .= " AND m.year = :srh_year ";
            $data['srh_year'] = $cond['srh_year'];
        }

        if (!empty($cond['srh_keyword'])) {
            $keywords = preg_split(Ldr_Const::SRH_DELIMITERS, $cond['srh_keyword'], -1, PREG_SPLIT_NO_EMPTY);
            $cond_key.= "AND ( ";
            foreach ($keywords as $key => $value) {
                if ($key != 0) {
                    $cond_key.= " AND ";
                }

                $label_key = "key" . $key;
                $cond_key.= "(m.no || ' ' || m.title ||' ' || d.place ) ILIKE :" . $label_key . " ";
                $types[] = 'text';
                $data[$label_key] = "%" . $value . "%";
            }
            $cond_key.= ") ";
            $cond_ret .= $cond_key;
        }

        if (!empty($cond['srh_start_date'])) {
            $cond_ret .= " AND ( :srh_start_date <= d.training_date OR d.type = 2) ";
            $types[] = 'text';
            $data['srh_start_date'] = $cond['srh_start_date'];
        }

        if (!empty($cond['srh_last_date'])) {
            $cond_ret .= " AND (d.training_date <= :srh_last_date OR d.type = 2) ";
            $types[] = 'text';
            $data['srh_last_date'] = $cond['srh_last_date'];
        }

        if (!empty($cond['srh_nonaccept'])) {
            $cond_ret .= " AND (SELECT count(*) FROM ldr_trn_inside trn WHERE trn.inside_date_id=d.inside_date_id AND trn.status = 2 AND NOT del_flg) > 0 ";
        }

        $ret['cond'] = $cond_ret;
        $ret['types'] = $types;
        $ret['data'] = $data;
        return $ret;
    }

    /**
     * 受付一覧件数
     * @return type
     */
    public function count_accept($cond = null)
    {
        $sql = "SELECT
                    COUNT(*)
                FROM ldr_mst_inside m
                JOIN ldr_mst_inside_contents c ON c.inside_id = m.inside_id AND NOT c.del_flg
                JOIN ldr_mst_inside_date d ON d.inside_id = m.inside_id AND d.inside_contents_id = c.inside_contents_id AND NOT d.del_flg
                WHERE
                  NOT m.del_flg";

        $ret = $this->make_accept_condition($cond);
        $sql.= $ret['cond'];
        $types = array_merge(array('text'), $ret['types']);
        $data = array_merge(array('emp_id' => $this->emp_id), $ret['data']);
        $count = $this->db->extended->getOne($sql, null, $data, $types);
        if (PEAR::isError($count)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $count->getDebugInfo());
        }
        return $count;
    }

    /**
     * 受付一覧
     * @param type $st
     * @param type $pageCount
     * @param type $cond
     * @return type
     * @throws Exception
     */
    public function lists_accept($st = null, $pageCount = null, $cond = null)
    {
        $sql = "SELECT
                    m.approve,
                    m.year,
                    m.no,
                    m.title,
                    c.number,
                    m.number_of,
                    d.inside_date_id,
                    c.inside_contents_id,
                    d.inside_id,
                    d.accept_date,
                    d.deadline_date,
                    d.type,
                    d.status,
                    d.place,
                    d.maximum,
                    d.training_date,
                    d.training_start_time,
                    d.training_last_time,
                    c.number,
                    c.after_work,
                    c.before_work,
                    c.looking_back,
                    m.level,
                    (SELECT count(*)
                     FROM ldr_trn_inside trn
                     WHERE
                     trn.inside_date_id=d.inside_date_id
                     AND (trn.status = 0 OR trn.status >= 2 OR trn.status < -2)
                     AND NOT del_flg) as count,
                    (SELECT count(*) FROM ldr_trn_inside trn WHERE trn.inside_date_id=d.inside_date_id AND trn.status = 1 AND NOT del_flg) as count_1,
                    (SELECT count(*) FROM ldr_trn_inside trn WHERE trn.inside_date_id=d.inside_date_id AND trn.status = 2 AND NOT del_flg) as count_2,
                    (SELECT count(*) FROM ldr_trn_inside trn WHERE trn.inside_date_id=d.inside_date_id AND trn.status = 3 AND NOT del_flg) as count_3,
                    (SELECT count(*) FROM ldr_trn_inside trn WHERE trn.inside_date_id=d.inside_date_id AND trn.status = -3 AND NOT del_flg) as count_3c,
                    (SELECT count(*) FROM ldr_trn_inside trn WHERE trn.inside_date_id=d.inside_date_id AND trn.status = 0 AND trn.attendance = 1 AND NOT del_flg) as count_0_1,
                    (SELECT count(*) FROM ldr_trn_inside trn WHERE trn.inside_date_id=d.inside_date_id AND trn.status = 0 AND trn.attendance = 2 AND NOT del_flg) as count_0_2,
                    (SELECT count(*) FROM ldr_mst_inside_planner ip WHERE ip.inside_id=m.inside_id AND ip.emp_id = :emp_id) as is_planner
                FROM ldr_mst_inside m
                JOIN ldr_mst_inside_contents c ON c.inside_id = m.inside_id AND NOT c.del_flg
                JOIN ldr_mst_inside_date d ON d.inside_id = m.inside_id AND d.inside_contents_id = c.inside_contents_id AND NOT d.del_flg
                WHERE
                  NOT m.del_flg
                 ";

        $ret = $this->make_accept_condition($cond);
        $sql.= $ret['cond'];
        $types = array_merge(array('text'), $ret['types'], $ret['types']);
        $data = array_merge(array('emp_id' => $this->emp_id), $ret['data']);

        $cond['order_by'][] = "m.no asc";
        $order = "";
        foreach ($cond['order_by'] as $value) {
            $order.= empty($order) ? "order by " : ", ";
            $order.= $value;
        }
        $sql .= $order;

        if (isset($st) && isset($pageCount)) {
            $this->db->setLimit($pageCount, $st);
        }

        $sth = $this->db->prepare($sql, $types, MDB2_PREPARE_RESULT);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['status_text'] = Ldr_Const::get_training_status($row['status']);
            $row['reception'] = Ldr_Util::get_inside_reception($row);
            $row['reception_text'] = Ldr_Const::get_inside_reception($row['reception']);
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 部署一覧
     * @param type $cond
     * @return string
     * @throws Exception
     */
    public function lists_accept_class($cond)
    {
        $types = array("integer");
        $data = array('id' => $cond['id']);

        $sql = "SELECT
                    c1.class_id,
                    c2.atrb_id,
                    c3.dept_id,
                    c4.room_id,
                    $this->class_sql,
                    class_nm,
                    atrb_nm,
                    dept_nm,
                    room_nm
                FROM ldr_trn_inside trn
                JOIN empmst e USING(emp_id)
                JOIN classmst c1 ON c1.class_id = e.emp_class
                JOIN atrbmst c2 ON c2.class_id = e.emp_class AND c2.atrb_id = e.emp_attribute AND NOT atrb_del_flg
                JOIN deptmst c3 ON c3.atrb_id = e.emp_attribute AND c3.dept_id = e.emp_dept AND NOT dept_del_flg
                LEFT JOIN classroom c4 ON c4.dept_id = e.emp_dept AND(c4.room_id = e.emp_room OR c4.room_id IS NULL) AND NOT room_del_flg
                WHERE
                    NOT trn.del_flg
                    AND trn.inside_date_id = :id
                    AND (trn.status = 0 OR trn.status >= 2 OR trn.status < -2)
                GROUP BY c1.order_no,c1.class_id,c2.order_no,c2.atrb_id,c3.order_no,c3.dept_id,c4.order_no,c4.room_id,class_nm,atrb_nm,dept_nm,room_nm
                ORDER BY c1.order_no asc,c1.class_id asc,c2.order_no asc,c2.atrb_id asc,c3.order_no asc,c3.dept_id asc,c4.order_no asc,c4.room_id asc";

        $sth = $this->db->prepare($sql, $types, MDB2_PREPARE_RESULT);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }
        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['key'] = $row['class_id'] . "_" . $row['atrb_id'] . "_" . $row['dept_id'] . "_" . $row['room_id'];
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * lists_accept_by_date_count
     * @param type $cond
     * @return type
     * @throws Exception
     */
    public function lists_accept_by_date_count($cond)
    {

        $sql_status = "AND trn.status in (-3, 0, 2, 3)";
        $types = array("integer");
        $data = array('id' => $cond['id']);

        if ($cond['list_type'] === 'report') {

            switch ($cond['srh_work']) {
                case 'before':
                    $join = "LEFT JOIN ldr_trn_inside_before_work rp ON trn.training_id = rp.training_id AND NOT rp.del_flg";
                    break;
                case 'after':
                    $join = "LEFT JOIN ldr_trn_inside_after_work rp ON trn.training_id = rp.training_id AND NOT rp.del_flg";
                    break;
                case 'looking_back':
                    $join = "LEFT JOIN ldr_trn_inside_check rp ON trn.training_id = rp.training_id AND NOT rp.del_flg";
                    break;
            }

            if ($cond['srh_report'] === 'reported') {
                $sql_status .= " AND rp.status = 1";
            }
            else if ($cond['srh_report'] === 'nonreport') {
                $sql_status .= " AND COALESCE(rp.status,0) = 0";
            }
        }

        $sql = "
            SELECT
                COUNT(*)
            FROM ldr_trn_inside trn
            $join
            WHERE
              NOT trn.del_flg
              AND trn.inside_date_id = :id
              $sql_status
        ";
        $count = $this->db->extended->getOne($sql, null, $data, $types);
        if (PEAR::isError($count)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $count->getDebugInfo());
        }

        return $count;
    }

    /**
     * 申請したidの職員一覧
     * @param type $data
     * @return type
     */
    public function lists_accept_by_date($cond, $st = null, $pageCount = null)
    {
        $types = array("integer");
        $data = array('id' => $cond['id']);

        // ステータス
        switch ($cond['srh_status']) {
            // 承認待ち
            case '1':
                $sql_status = " AND trn.status = 1";
                break;
            // 受付待ち
            case '2':
                $sql_status = " AND trn.status = 2";
                break;
            // 受付済み
            case '3':
                $sql_status = " AND trn.status = 3";
                break;
            // 取り下げ
            case '-3':
                $sql_status = " AND trn.status = -3";
                break;
            default:
                $sql_status = " AND trn.status in (-3, 0, 2, 3)";
                break;
        }

        // 出欠
        switch ($cond['srh_attendance']) {
            // 出席
            case '1':
                $sql_status .= " AND trn.attendance = 1";
                break;
            // 欠席
            case '2':
                $sql_status .= " AND trn.attendance = 2";
                break;
            // 未設定
            case '0':
                $sql_status .= " AND trn.status = 3 AND (trn.attendance = 0 OR trn.attendance IS NULL)";
                break;
            default:
                break;
        }

        // 組織1,2
        if (!empty($cond['srh_class1'])) {
            list($data['class_id'], $data['atrb_id']) = explode("_", $cond['srh_class1']);
            $sql_status .= " AND e.emp_class = :class_id AND e.emp_attribute = :atrb_id";
            $types[] = "integer";
            $types[] = "integer";
        }

        // 組織3,4
        if (!empty($cond['srh_class3'])) {
            list($data['dept_id'], $data['room_id']) = explode("_", $cond['srh_class3']);
            $sql_status .= " AND e.emp_dept = :dept_id AND COALESCE(e.emp_room,0) = COALESCE(:room_id,0)";
            $types[] = "integer";
            $types[] = "integer";
        }

        $sql = "
            SELECT
                trn.training_id,
                trn.inside_date_id,
                trn.emp_id,
                e.emp_personal_id,
                trn.status,
                trn.attendance,
                e.emp_lt_nm || ' ' || e.emp_ft_nm as name,
                $status
                $report
                $update
                trn.created_on,
                $this->class_sql,
                class_nm,
                atrb_nm,
                dept_nm,
                room_nm,
                trn.updated_on
            FROM ldr_trn_inside trn
            JOIN empmst e USING(emp_id)
            JOIN classmst c1 ON c1.class_id = e.emp_class
            JOIN atrbmst c2 ON c2.class_id = e.emp_class AND c2.atrb_id = e.emp_attribute AND NOT atrb_del_flg
            JOIN deptmst c3 ON c3.atrb_id = e.emp_attribute AND c3.dept_id = e.emp_dept AND NOT dept_del_flg
            LEFT JOIN classroom c4 ON c4.dept_id = e.emp_dept AND(c4.room_id = e.emp_room OR c4.room_id IS NULL) AND NOT room_del_flg
            $join
            WHERE
                NOT trn.del_flg
                AND trn.inside_date_id = :id
                $sql_status
            ORDER BY c1.order_no asc,c1.class_id asc,c2.order_no asc,c2.atrb_id asc,c3.order_no asc,c3.dept_id asc,c4.order_no asc,c4.room_id asc,e.emp_personal_id asc
            ";

        if (isset($st) && isset($pageCount)) {
            $this->db->setLimit($pageCount, $st);
        }

        $sth = $this->db->prepare($sql, $types, MDB2_PREPARE_RESULT);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }
        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['status_text'] = Ldr_Const::get_training_status($row['status']);
            if ($cond['srh_work'] === 'looking_back') {
                $row['form'] = unserialize($row['report']);
            }
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * lists_accept_by_contents_count
     * @param type $cond
     * @return type
     * @throws Exception
     */
    public function lists_accept_by_contents_count($cond)
    {
        $sql_status = "AND trn.status in (-3, 0, 2, 3)";
        $types = array("integer");
        $data = array('id' => $cond['id']);

        if ($cond['list_type'] === 'report') {

            switch ($cond['srh_work']) {
                case 'before':
                    $join = "LEFT JOIN ldr_trn_inside_before_work rp ON trn.training_id = rp.training_id AND NOT rp.del_flg";
                    break;
                case 'after':
                    $join = "LEFT JOIN ldr_trn_inside_after_work rp ON trn.training_id = rp.training_id AND NOT rp.del_flg";
                    break;
                case 'looking_back':
                    $join = "LEFT JOIN ldr_trn_inside_check rp ON trn.training_id = rp.training_id AND NOT rp.del_flg";
                    break;
            }

            if ($cond['srh_report'] === 'reported') {
                $sql_status .= " AND rp.status = 1";
            }
            else if ($cond['srh_report'] === 'nonreport') {
                $sql_status .= " AND COALESCE(rp.status,0) = 0";
            }
        }

        $sql = "
            SELECT
                COUNT(*)
            FROM ldr_trn_inside trn
            JOIN ldr_mst_inside_date mst USING (inside_date_id)
            $join
            WHERE
              NOT trn.del_flg
              AND mst.inside_contents_id = :id
              $sql_status
        ";
        $count = $this->db->extended->getOne($sql, null, $data, $types);
        if (PEAR::isError($count)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $count->getDebugInfo());
        }

        return $count;
    }

    /**
     * 申請したidの職員一覧
     * @param type $data
     * @return type
     */
    public function lists_accept_by_contents($cond, $st = null, $pageCount = null)
    {

        $sql_status = "AND trn.status in (-3, 0, 2, 3)";
        $types = array("integer");
        $data = array('id' => $cond['id']);

        // list_type==='report' は、課題、振り返り
        if ($cond['list_type'] === 'report') {

            switch ($cond['srh_work']) {
                case 'before':
                    $fields = "
                        rp.status as r_status,
                        rp.work as report,
                        rp.updated_on as update,
                    ";
                    $join = "LEFT JOIN ldr_trn_inside_before_work rp ON trn.training_id = rp.training_id AND NOT rp.del_flg";
                    break;
                case 'after':
                    $fields = "
                        rp.status as r_status,
                        rp.work as report,
                        rp.updated_on as update,
                    ";
                    $join = "LEFT JOIN ldr_trn_inside_after_work rp ON trn.training_id = rp.training_id AND NOT rp.del_flg";
                    break;
                case 'looking_back':
                    $fields = "
                        rp.status as r_status,
                        rp.data as report,
                        rp.updated_on as update,
                    ";
                    $join = "LEFT JOIN ldr_trn_inside_check rp ON trn.training_id = rp.training_id AND NOT rp.del_flg";
                    break;
                default:
                    break;
            }
            if ($cond['srh_report'] === 'reported') {
                $sql_status .= " AND rp.status = 1";
            }
            else if ($cond['srh_report'] === 'nonreport') {
                $sql_status .= " AND COALESCE(rp.status,0) = 0";
            }
        }

        // 除外するdate_id
        if ($cond['without_date_id']) {
            $sql_status = " AND trn.inside_date_id <> :date_id";
            $data['date_id'] = $cond['without_date_id'];
            $types[] = 'integer';
        }

        $sql = "
            SELECT
                trn.training_id,
                trn.inside_date_id,
                trn.emp_id,
                e.emp_personal_id,
                trn.status,
                trn.attendance,
                e.emp_lt_nm || ' ' || e.emp_ft_nm as name,
                mst.training_date,
                mst.training_start_time,
                mst.training_last_time,
                $fields
                trn.created_on,
                $this->class_sql,
                class_nm,
                atrb_nm,
                dept_nm,
                room_nm,
                trn.updated_on
            FROM ldr_trn_inside trn
            JOIN ldr_mst_inside_date mst USING (inside_date_id)
            JOIN empmst e USING(emp_id)
            JOIN classmst c1 ON c1.class_id = e.emp_class
            JOIN atrbmst c2 ON c2.class_id = e.emp_class AND c2.atrb_id = e.emp_attribute AND NOT atrb_del_flg
            JOIN deptmst c3 ON c3.atrb_id = e.emp_attribute AND c3.dept_id = e.emp_dept AND NOT dept_del_flg
            LEFT JOIN classroom c4 ON c4.dept_id = e.emp_dept AND(c4.room_id = e.emp_room OR c4.room_id IS NULL) AND NOT room_del_flg
            $join
            WHERE
                NOT trn.del_flg
                AND mst.inside_contents_id = :id
                $sql_status
            ORDER BY c1.order_no asc,c1.class_id asc,c2.order_no asc,c2.atrb_id asc,c3.order_no asc,c3.dept_id asc,c4.order_no asc,c4.room_id asc,e.emp_personal_id asc
            ";

        if (isset($st) && isset($pageCount)) {
            $this->db->setLimit($pageCount, $st);
        }

        $sth = $this->db->prepare($sql, $types, MDB2_PREPARE_RESULT);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }
        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['status_text'] = Ldr_Const::get_training_status($row['status']);
            if ($cond['srh_work'] === 'looking_back') {
                $row['form'] = unserialize($row['report']);
            }
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 受付の保存
     * @param type $data
     */
    public function save_accept($data)
    {
        $ary2 = array();
        $ary3 = array();
        $ary3c = array();
        foreach ($data['training_id'] as $key => $value) {
            switch ($value) {
                case "2":
                    $ary2[] = $key;
                    break;
                case "3":
                    $ary3[] = $key;
                    break;
                case "-3":
                    $ary3c[] = $key;
                    break;
            }
        }
        if (!empty($ary2)) {
            $this->status_change($ary2, "2");
        }
        if (!empty($ary3)) {
            $this->status_change($ary3, "3");
        }
        if (!empty($ary3c)) {
            $this->status_change($ary3c, "-3");
        }

        $this->log->log("受付しました", "training_id=完了[" . implode(" ", $ary3) . "] 取下[" . implode(" ", $ary3c) . "] 未設定[" . implode(" ", $ary2) . "]");
    }

    /**
     * 課題の保存
     * @param array $data
     * @throws Exception
     */
    public function save_work($data)
    {
        $data['emp_id'] = $this->emp_id;
        if ($data['mode'] === 'before') {
            $table = "ldr_trn_inside_before_work";
        }
        else if ($data['mode'] === 'after') {
            $table = "ldr_trn_inside_after_work";
        }
        else {
            throw new Exception(__METHOD__ . " METHOD error");
        }

        $this->db->beginNestedTransaction();

        // INSERT
        if ($data['wk_status'] === "") {
            $sth = $this->db->prepare("INSERT INTO $table (training_id, work, status) VALUES (:id, :work, :status)", array('integer', 'text', 'integer'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($data);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
            }
            $sth->free();
        }
        else {
            $sth = $this->db->prepare("UPDATE $table SET work= :work ,status= :status WHERE training_id= :id", array('text', 'integer', 'integer'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($data);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
            }
            $sth->free();
        }

        $this->db->completeNestedTransaction();

        // Log
        $this->log->log("課題を保存しました", "training_id={$data['id']} status={$data['status']} ");
    }

    /**
     * 振り返りの保存
     * @param array $data
     * @throws Exception
     */
    public function save_check($data)
    {
        $dt = array(
            'id' => $data['id'],
            'emp_id' => $this->emp_id,
            'data' => serialize($data['form']),
            'status' => $data['status'],
        );

        $this->db->beginNestedTransaction();

        // INSERT
        if ($data['check_status'] === "") {
            $sth = $this->db->prepare("INSERT INTO ldr_trn_inside_check (training_id, data, status) VALUES (:id, :data, :status)", array('integer', 'text', 'integer'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($dt);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
            }
            $sth->free();
        }
        else {
            $sth = $this->db->prepare("UPDATE ldr_trn_inside_check SET data= :data ,status= :status WHERE training_id= :id", array('text', 'integer', 'integer'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($dt);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
            }
            $sth->free();
        }

        // 研修データ
        $train = $this->find($data['id']);

        // ステータスを完了
        $this->status_change($data['id'], '0');

        // 欠席じゃないとき
        // 一時保存じゃないときは出席処理を実施
        if ($train['attendance'] !== '2' and $data['status'] !== 0) {
            // 出席にする
            $this->attendance_change($data['id'], '1');
            $train['attendance'] = 1;

            // 様式2-IIに追加
            $this->save_form22($data, $this->emp_id);
        }

        $this->db->completeNestedTransaction();

        // Log
        $this->log->log("振り返りを保存しました", "training_id={$data['id']} status={$data['status']} attendance={$train['attendance']}");
    }

    /**
     * 様式2-2への更新
     * @param type $data
     */
    private function save_form22($data, $emp_id, $report = "1", $training_id = null)
    {
        $f22 = new Ldr_Form22($emp_id);
        if (empty($this->hospital_name)) {
            $conf = new Ldr_Config();
            $this->hospital_name = $conf->hospital_name();
        }

        $f22_data = array(
            "flag" => "1",
            "period" => ($data['type'] === '1' ? $data["training_date"] : date('Y-m-d')),
            "title" => $data["title"],
            "training_id" => empty($training_id) ? $data["training_id"] : $training_id,
            "organizer" => $this->hospital_name,
        );
        if ($report !== '1') {
            // 念のため消す
            $f22->delete_training($f22_data['flag'], $f22_data['training_id']);
        }
        else {
            $f22->save_training($f22_data);
        }
    }

    /**
     * 出欠
     * @param type $id
     * @param type $attendance
     * @throws Exception
     */
    private function attendance_change($id, $attendance)
    {
        $this->db->beginNestedTransaction();
        if (is_array($id)) {
            $holder = implode(',', array_fill(0, count($id), '?'));
            $sql_cond = " training_id IN($holder) ";
            $data = array_merge(array($attendance), $id);
            $types = array_merge(array('integer'), array_fill(0, count($id), 'integer'));
        }
        else {
            $sql_cond = "training_id= ?";
            $types = array('integer', 'integer');
            $data = array($attendance, $id);
        }

        $sql = "UPDATE ldr_trn_inside
                SET
                    attendance = ?
                WHERE
                    $sql_cond
                    AND NOT del_flg";
        $sth = $this->db->prepare($sql, $types, MDB2_PREPARE_MANIP);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }

        $sth->free();

        $this->db->completeNestedTransaction();
    }

    /**
     * 出欠の保存
     * @param type $data
     */
    public function save_attendance($data)
    {
        $ary1 = array();
        $ary2 = array();
        $ary0 = array();
        foreach ($data['training_id'] as $key => $value) {
            switch ($value) {
                case "1":
                    //出席
                    $ary1[] = $key;
                    break;
                case "2":
                    //欠席
                    $ary2[] = $key;
                    break;
                case "0":
                    //後回し
                    $ary0[] = $key;
                    break;
            }

            $this->save_form22($data, $data['emp_id'][$key], $value, $key);
        }
        if (!empty($ary1)) {
            $this->attendance_change($ary1, "1");
            $this->status_change($ary1, "0");
        }
        if (!empty($ary2)) {
            $this->attendance_change($ary2, "2");
            $this->status_change($ary2, "0");
        }
        if (!empty($ary0)) {
            $this->attendance_change($ary0, "0");
            $this->status_change($ary0, "3");
        }

        $this->log->log("出欠を設定しました", "training_id=完了[" . implode(" ", $ary1) . "] 取下[" . implode(" ", $ary2) . "] 未設定[" . implode(" ", $ary0) . "]");

        // 追加の参加者
        if (!empty($data['new_emp_id'])) {
            $id = explode('_', $data['id']);
            foreach ($data['new_emp_id'] as $emp_id) {
                $save = array(
                    'emp_id' => $emp_id,
                    'status' => 3,
                    'inside_date_id' => $id[1],
                );
                if ($data['moved'][$emp_id] !== 0) {
                    $save['training_id'] = $data['moved'][$emp_id];
                }
                $this->save($save);
            }
        }
    }

    /**
     * 講座一覧件数
     * @param type $cond
     * @return type
     * @throws Exception
     */
    public function count_course($cond = null)
    {
        return $this->mst->count($cond);
    }

    /**
     * 講座一覧
     * @param type $st
     * @param type $pageCount
     * @param type $cond
     * @return type
     * @throws Exception
     */
    public function lists_course($st = null, $pageCount = null, $cond = null)
    {
        $list = $this->mst->lists($st, $pageCount, $cond);
        foreach ($list as $key => $value) {
            $dt = $this->find_course_date($value['inside_id']);
            $list[$key]['course'] = $dt;
        }
        return $list;
    }

    /**
     * 申請済み職員一覧
     * @param type $data
     * @return type
     * @throws Exception
     */
    public function lists_applied($data)
    {

        $sql = "SELECT
                    trn.status,
                    e.emp_id,
                    e.emp_lt_nm || ' ' || e.emp_ft_nm AS name,
                    ldr.level,
                    c1.class_id,
                    c2.atrb_id,
                    c3.dept_id,
                    c4.room_id,
                    {$this->class_sql},
                    class_nm,
                    atrb_nm,
                    dept_nm,
                    room_nm
                FROM ldr_trn_inside trn
                JOIN empmst e USING(emp_id)
                JOIN classmst c1 ON c1.class_id = e.emp_class
                JOIN atrbmst c2 ON c2.class_id = e.emp_class AND c2.atrb_id = e.emp_attribute AND NOT atrb_del_flg
                JOIN deptmst c3 ON c3.atrb_id = e.emp_attribute AND c3.dept_id = e.emp_dept AND NOT dept_del_flg
                LEFT JOIN classroom c4 ON c4.dept_id = e.emp_dept AND(c4.room_id = e.emp_room OR c4.room_id IS NULL) AND NOT room_del_flg
                LEFT JOIN ldr_employee ldr ON ldr.emp_id = e.emp_id
                WHERE
                NOT trn.del_flg
                AND trn.inside_date_id = :inside_date_id
                AND trn.status >= '0'
                ORDER BY c1.class_id,c2.atrb_id,c3.dept_id,c4.room_id,e.emp_id
        ";
        $sth = $this->db->prepare($sql, array('integer'), MDB2_PREPARE_RESULT);

        $res = $sth->execute($data);

        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['status_text'] = Ldr_Const::get_training_status($row['status']);
            $lists[] = $row;
        }
        return $lists;
    }

    /**
     * 受付件数（ステータス毎）
     * @return Array
     * @throws Exception
     */
    public function count_accept_status()
    {
        if (empty($this->emp_id)) {
            throw new Exception(__METHOD__ . " METHOD error: ");
        }

        $sql = "SELECT
                (SELECT COUNT(*)
                    FROM ldr_trn_inside trn
                    JOIN ldr_mst_inside_date d USING (inside_date_id)
                    JOIN ldr_mst_inside i USING (inside_id)
                    JOIN ldr_mst_inside_contents c USING (inside_contents_id)
                    JOIN empmst e USING(emp_id)
                    JOIN ldr_approver app
                        ON app.emp_id = :emp_id
                        AND app.class_id = e.emp_class
                        AND app.atrb_id = e.emp_attribute
                        AND app.dept_id = e.emp_dept
                        AND COALESCE(app.room_id,0) = COALESCE(e.emp_room,0)
                    WHERE trn.status=1
                      AND NOT trn.del_flg
                      AND NOT d.del_flg
                      AND NOT c.del_flg
                      AND NOT i.del_flg
                ) AS st1_count,
                (SELECT COUNT(*)
                    FROM ldr_trn_inside trn
                    JOIN ldr_mst_inside_date d USING(inside_date_id)
                    JOIN ldr_mst_inside i USING (inside_id)
                    JOIN ldr_mst_inside_contents c USING (inside_contents_id)
                    WHERE trn.status=2
                      AND NOT trn.del_flg
                      AND NOT d.del_flg
                      AND NOT c.del_flg
                      AND NOT i.del_flg
                      AND (
                            EXISTS (SELECT * FROM ldr_mst_inside_planner p WHERE p.inside_id = d.inside_id AND emp_id = :emp_id)
                            OR EXISTS (SELECT * FROM ldr_permission_inside WHERE emp_id = :emp_id)
                          )
                ) AS st2_count";
        $row = $this->db->extended->getRow($sql, null, array('emp_id' => $this->emp_id), array('text'), MDB2_FETCHMODE_ASSOC);
        if (PEAR::isError($row)) {
            throw new Exception(__METHOD__ . " ERROR: " . $row->getDebugInfo());
        }

        return $row;
    }

}
