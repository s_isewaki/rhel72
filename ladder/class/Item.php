<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'Cmx/Model/SystemConfig.php';
require_once 'ladder/class/Log.php';

class Ldr_Item extends Model
{

    var $log;

    /**
     * 識別子一覧
     * 　第1階層の配列のキーは名称識別子
     * 　第2階層の配列のキーはテーブルのフィールド名
     * @var type
     */
    private $types = array(
        'form_21' => array(
            'title' => array('type' => '21title', 'form' => '様式2-I', 'field' => '研修名', 'table' => 'ldr_form2_1'),
            'organizer' => array('type' => '21organizer', 'form' => '様式2-I', 'field' => '主催', 'table' => 'ldr_form2_1'),
        ),
        'form_22' => array(
            'title' => array('type' => '22title', 'form' => '様式2-II', 'field' => '研修名', 'table' => 'ldr_form2_2'),
            'organizer' => array('type' => '22organizer', 'form' => '様式2-II', 'field' => '主催', 'table' => 'ldr_form2_2'),
            'category' => array('type' => '22category', 'form' => '様式2-II', 'field' => 'カテゴリ', 'table' => 'ldr_form2_2'),
        ),
        'form_3' => array(
            'title' => array('type' => '3title', 'form' => '様式3', 'field' => '学会名', 'table' => 'ldr_form3'),
            'organizer' => array('type' => '3organizer', 'form' => '様式3', 'field' => '主催', 'table' => 'ldr_form3'),
        ),
        'form_presentation' => array(
            'title' => array('type' => 'presentation_title', 'form' => '学会発表記録', 'field' => '学会名', 'table' => 'ldr_form_presentation'),
        ),
        'form_51' => array(
            'class' => array('type' => '51class', 'form' => '様式5-I', 'field' => '所属部署', 'table' => 'ldr_form5_1'),
            'role' => array('type' => '51role', 'form' => '様式5-I', 'field' => '役割', 'table' => 'ldr_form5_1'),
        ),
        'form_52' => array(
            'subject' => array('type' => '52subject', 'form' => '様式5-II', 'field' => '対象', 'table' => 'ldr_form5_2'),
        ),
        'form_53' => array(
            'title' => array('type' => '53title', 'form' => '様式5-III', 'field' => '名称', 'table' => 'ldr_form5_3'),
        ),
        'form_54' => array(
            'title' => array('type' => '54title', 'form' => '様式5-IV', 'field' => '名称', 'table' => 'ldr_form5_4'),
        ),
        'form_carrier' => array(
            'institution' => array('type' => 'carrier_institution', 'form' => '他院看護経験', 'field' => '施設名', 'table' => 'ldr_form_carrier'),
            'detail' => array('type' => 'carrier_detail', 'form' => '他院看護経験', 'field' => '職務内容', 'table' => 'ldr_form_carrier'),
        ),
        'form_transfer' => array(
            'transfer' => array('type' => 'transfer_transfer', 'form' => '院内異動履歴', 'field' => '異動先', 'table' => 'ldr_form_transfer'),
        ),
        'form_article' => array(
            'title' => array('type' => 'article_title', 'form' => '学会掲載記録', 'field' => '掲載誌', 'table' => 'ldr_form_article'),
        ),
    );
    private $fields;

    /**
     * コンストラクタ
     */
    public function __construct()
    {
        parent::connectDB();
        $this->log = new Ldr_log('item');
        $this->set_fields();
    }

    /**
     * 名称取得
     * @param Integer $id
     * @return Array
     */
    public function typeahead($type, $query)
    {
        if (empty($type)) {
            return array();
        }

        $query = $this->search_convert($query);

        $sql = "
            SELECT
                inner_id as id,
                name as value
            FROM ldr_item
            WHERE
                NOT del_flg
                AND type = :type
                AND (search_name LIKE :query OR search_kana LIKE :query)
            ORDER BY order_no, search_name, search_kana
        ";

        $sth = $this->db->prepare($sql, array('integer', 'text', 'text'), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array('type' => $type, 'query' => "%$query%"));

        if (PEAR::isError($res)) {
            throw new Exception('ldr_item typeahead ERROR: ' . $res->getDebugInfo());
        }
        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[] = $row;
        }
        return $list;
    }

    /**
     * 一覧データ取得
     * @param Integer $id
     * @return Array
     */
    public function lists($type)
    {
        if (empty($type)) {
            return array();
        }

        list($table, $field) = $this->fields[$type];

        $sql = "
            SELECT
                i.*,
                (SELECT COUNT(*) FROM {$table} t WHERE i.name=t.{$field}) AS counts
            FROM ldr_item i
            WHERE
                NOT del_flg
                AND type = :type
            ORDER BY order_no, search_name, search_kana
        ";

        $sth = $this->db->prepare($sql, array('integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array('type' => $type));

        if (PEAR::isError($res)) {
            throw new Exception('ldr_item lists ERROR: ' . $res->getDebugInfo());
        }
        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[] = $row;
        }
        return $list;
    }

    /**
     * 記録データ取得
     * @param Integer $type
     * @return Array
     */
    public function lists_record($type)
    {
        if (empty($type)) {
            return array();
        }

        list($table, $field) = $this->fields[$type];

        $sql = "
            SELECT
                t.{$field} as name
            FROM {$table} t
            WHERE NOT del_flg
              AND t.{$field} IS NOT NULL
            GROUP BY t.{$field}
            ORDER BY COUNT(t.{$field}) DESC
        ";

        $res = $this->db->query($sql);

        if (PEAR::isError($res)) {
            throw new Exception('ldr_item lists_record ERROR: ' . $res->getDebugInfo());
        }
        $list = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $list[] = $row;
        }
        return $list;
    }

    /**
     * 検索用文字列変換
     * @param type $str
     */
    private function search_convert($str)
    {
        switch (mb_detect_encoding($str)) {
            case 'EUC-JP':
                return mb_convert_kana($str, 'asCKV', 'EUC-JP');

            case 'SJIS':
                return mb_convert_kana(from_sjis($str), 'asCKV', 'EUC-JP');

            case 'UTF-8':
                return mb_convert_kana(from_utf8($str), 'asCKV', 'EUC-JP');

            default:
                return $str;
        }
    }

    /**
     * 識別子から、テーブル、フィールドを取得できる配列を作成
     */
    private function set_fields()
    {
        $data = array();
        foreach ($this->types() as $fields) {
            foreach ($fields as $field => $row) {
                $data[$row['type']] = array($row['table'], $field);
            }
        }
        $this->fields = $data;
    }

    /**
     * 識別子一覧
     * @return type
     */
    public function types()
    {
        return $this->types;
    }

    /**
     * validate
     * @param Array $data
     * @return Array
     */
    public function validate($data)
    {
        $error = array();

        // 名称
        if (Validation::is_empty($data['name'])) {
            $error['name'][] = '名称が入力されていません';
        }
        elseif (mb_strlen($data['name'], 'EUC-JP') > 200) {
            $error['name'][] = '名称は200文字以内で入力してください';
        }

        // 名称かな
        if (Validation::is_empty($data['kana'])) {

        }
        elseif (mb_strlen($data['kana'], 'EUC-JP') > 200) {
            $error['kana'][] = '名称かなは200文字以内で入力してください';
        }

        return $error;
    }

    /**
     * csv用のテキストを作成
     * @return type
     */
    public function export_csv($type)
    {
        $list = $this->lists($type);
        $fp = fopen('php://temp', 'r+b');
        foreach ($list as $data) {
            fputcsv($fp, array(
                $data['name'],
                $data['kana'],
                $data['order_no'],
            ));
        }
        rewind($fp);

        $csv = "名称,名称かな,表示順\r\n";
        $csv .= str_replace(PHP_EOL, "\r\n", stream_get_contents($fp));

        // Log
        $this->log->log("CSVをエクスポートしました");

        return $csv;
    }

    /**
     * データのインポート
     * @param type $file
     * @return type
     * @throws Exception
     */
    public function import_csv($type, $file)
    {
        $fp = Ldr_Util::file_to_euc($file['tmp_name']);
        $line = 0;
        $cnt = 0;
        $error_all = array();
        $this->db->beginNestedTransaction();
        while ($row = fgetcsv($fp)) {
            $line++;
            $data = array('type' => $type);

            list($data['name'], $data['kana'], $data['order_no']) = $row;
            if ($data['name'] === '名称') {
                continue;
            }

            $er = $this->validate($data);

            foreach ($er as $tmp) {
                foreach ($tmp as $value) {
                    $error_all[] = $line . "行目： " . $value;
                }
            }
            if (empty($er)) {
                try {
                    $this->save($data);
                }
                catch (Exception $e) {
                    $error_all[] = $line . "行目： 更新エラー";
                    cmx_log($e->getMessage());
                }
            }
            $cnt++;
        }
        if (!empty($error_all)) {
            $this->db->failNestedTransaction();
            throw new Exception(implode(',', $error_all));
        }
        $this->db->completeNestedTransaction();

        // Log
        $this->log->log("{$cnt}件のデータをインポートしました");

        return $cnt . "件のデータをインポートしました";
    }

    /**
     * 記録取り込み
     * @param type $type
     * @return type
     * @throws Exception
     */
    public function import_record($type)
    {
        $records = $this->lists_record($type);

        foreach ($records as $row) {
            try {
                $row['type'] = $type;
                $this->save($row);
            }
            catch (Exception $e) {
                cmx_log($e->getMessage());
            }
        }


        // Log
        $this->log->log("記録を取り込みました: type={$type}");
    }

    /**
     * 保存
     * @param String $emp_id
     * @param Array $data
     */
    public function save($data)
    {
        $this->db->beginNestedTransaction();

        $data['kana'] = $data['kana'] ? $data['kana'] : null;
        $data['search_name'] = $this->search_convert($data['name']);
        $data['search_kana'] = $this->search_convert($data['kana']);
        $data['order_no'] = $data['order_no'] ? $data['order_no'] : null;

        // INSERT or UPDATE
        $sth = $this->db->prepare("SELECT update_ldr_item(:type, :name, :kana, :search_name, :search_kana, :order_no)", array('text', 'text', 'text', 'text', 'text', 'integer'), MDB2_PREPARE_MANIP);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }
        $sth->free();

        $this->db->completeNestedTransaction();

        // Log
        $this->log->log("保存しました", "type={$data['type']} name={$data['name']}");
    }


    /**
     * 更新
     * @param type $data
     * @throws Exception
     */
    public function update($data)
    {

        $data['search_name'] = $this->search_convert($data['name']);
        $data['search_kana'] = $this->search_convert($data['kana']);

        $this->db->beginNestedTransaction();
        $sth = $this->db->prepare("UPDATE ldr_item SET name= :name, kana= :kana, search_name= :search_name, search_kana= :search_kana WHERE inner_id= :id", array('text', 'text', 'text', 'text', 'integer'), MDB2_PREPARE_MANIP);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }
        $sth->free();
        $this->db->completeNestedTransaction();

        // Log
        $this->log->log("更新しました", "inner_id={$data['id']}");
    }

    /**
     * 削除
     * @param type $id
     * @throws Exception
     */
    public function remove($id)
    {
        $this->db->beginNestedTransaction();
        $sth = $this->db->prepare("DELETE FROM ldr_item WHERE inner_id= ?", array('integer'), MDB2_PREPARE_MANIP);
        $res = $sth->execute(array($id));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }
        $sth->free();
        $this->db->completeNestedTransaction();

        // Log
        $this->log->log("削除しました", "inner_id=$id");
    }

    /**
     * 並べ替え
     * @param type $data
     * @throws Exception
     */
    public function sort($data)
    {
        $this->db->beginNestedTransaction();
        $sth = $this->db->prepare("UPDATE ldr_item SET order_no= ? WHERE inner_id= ?", array('integer', 'integer'), MDB2_PREPARE_MANIP);
        $cnt = 1;
        foreach ($data['order'] as $id) {
            $res = $sth->execute(array($cnt++, $id));
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " id={$id} ERROR: " . $res->getDebugInfo());
            }
        }
        $sth->free();
        $this->db->completeNestedTransaction();

        // Log
        $this->log->log("並べ替えました", implode(" ", $data['order']));
    }

}
