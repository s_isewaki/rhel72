<?php

require_once 'ladder/class/Config.php';

class Ldr_Const
{

    /**
     * １ページ毎の表示数(デフォルト)
     */
    const SRH_PERPAGE = 10;

    /**
     * 検索ワードのデリミッタ
     */
    const SRH_DELIMITERS = "/[\s,;]+/";

    /**
     * 申請のステータス（文言）or変換リストを取得
     * @param type $status ステータス（数値）
     * @return type ステータスに対応した文言を返却する。
     *               ステータスを省略した場合は変換リストを返却する。
     */
    public static function get_apply_status($status = "array")
    {
        $conf = new Ldr_Config();
        $title = $conf->titles();
        $APPLAY_STATUS = array(
            '4' => $title['shi_befor'],
            '5' => $title['shi_pendigr'],
            '6' => $title['shi_remand'],
            '8' => $title['shi_appraisal'],
            '9' => $title['shi_appr_meet'],
            '10' => $title['shi_chk_esta'],
            '11' => $title['shi_chking'],
            '12' => $title['committee'],
            '0' => '終了',
            '-2' => $title['shi_cancel'],
        );

        if ($status === "array") {
            return $APPLAY_STATUS;
        }
        if (!array_key_exists($status, $APPLAY_STATUS)) {
            return "";
        }

        return $APPLAY_STATUS[$status];
    }

    /**
     * アラビア数値に変換
     */
    private static $ROMAN_NUM = array(
        '1' => 'I',
        '2' => 'II',
        '3' => 'III',
        '4' => 'IV',
        '5' => 'V',
        '6' => 'VI',
        '7' => 'VII',
        '8' => 'VIII',
        '9' => 'IX',
        '10' => 'X',
        '12' => 'I / II',
        '123' => 'I / II / III',
        '1234' => 'I / II / III / IV',
        '23' => 'II / III',
        '234' => 'II / III / IV',
        '2345' => 'II / III / IV / V',
        '34' => 'III / IV',
        '345' => 'III / IV / V',
        '45' => 'IV / V',
    );

    /**
     * アラビア→ローマ数値or変換リストを取得
     * @param type $arabic_num アラビア数値
     * @return type ステータスに対応した文言を返却する。
     *               ステータスを省略した場合は変換リストを返却する。
     */
    public static function get_roman_num($arabic_num = "array")
    {
        if ($arabic_num === "array") {
            return Ldr_Const::$ROMAN_NUM;
        }
        if (!array_key_exists($arabic_num, Ldr_Const::$ROMAN_NUM)) {
            return "";
        }
        return Ldr_Const::$ROMAN_NUM[$arabic_num];
    }

    /**
     * 院外研修:区分
     * @var type
     */
    private static $OUTSIDE_TYPE = array(
        '1' => '日程出張',
        '2' => '出張',
        '3' => 'その他',
    );

    /**
     * 院外研修:区分を文言に変換
     * @param type $type 区分
     * @return type ステータスに対応した文言を返却する。
     *               ステータスを省略した場合は変換リストを返却する。
     */
    public static function get_outside_type($type = "array")
    {
        if ($type === "array") {
            return Ldr_Const::$OUTSIDE_TYPE;
        }
        if (!array_key_exists($type, Ldr_Const::$OUTSIDE_TYPE)) {
            return "";
        }
        return Ldr_Const::$OUTSIDE_TYPE[$type];
    }

    /**
     * 研修ステータス文言
     */
    private static $TRAINING_STATUS = array(
        '1' => '承認待ち',
        '2' => '受付待ち',
        '3' => '出欠確認',
        '0' => '受講済み',
        '-1' => '申請取り下げ',
        '-2' => '承認取り下げ',
        '-3' => '受付取り下げ',
    );

    /**
     * 研修ステータスを文言に変換
     * @param type $type
     * @return string
     */
    public static function get_training_status($type = "array")
    {
        if ($type === "array") {
            return Ldr_Const::$TRAINING_STATUS;
        }
        if (!array_key_exists($type, Ldr_Const::$TRAINING_STATUS)) {
            return "";
        }
        return Ldr_Const::$TRAINING_STATUS[$type];
    }

    /**
     * 院内研修受付状態
     */
    private static $INSIDE_RECEPTION = array(
        '-2' => '準備中', //手動
        '-1' => '受付前',
        '0' => '受付中',
        '1' => '受付終了', // 手動
        '2' => '定員', // 定員
        '3' => '締切', // 期間
        '4' => '開催終了',
    );

    /**
     * 院内研修の受付状態を文言に変換
     * @param type $type
     * @return string
     */
    public static function get_inside_reception($type = "array")
    {
        if ($type === "array") {
            return Ldr_Const::$INSIDE_RECEPTION;
        }
        if (!array_key_exists($type, Ldr_Const::$INSIDE_RECEPTION)) {
            return "";
        }
        return Ldr_Const::$INSIDE_RECEPTION[$type];
    }

    /**
     * 記録の表示順デフォルト
     */
    private static $DEFAULT_SORTSET = array(
        'form_21',
        'form_22',
        'form_23',
        'form_3',
        'form_4',
        'form_51',
        'form_52',
        'form_53',
        'form_54',
        'form_carrier',
        'form_transfer',
        'form_presentaion',
        'form_article',
    );

    /**
     * 記録の表示順デフォルトを取得
     * @param type $type
     * @return string
     */
    public static function get_default_sortset()
    {
        return Ldr_Const::$DEFAULT_SORTSET;
    }

}
