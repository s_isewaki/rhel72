<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'ladder/class/Log.php';
require_once 'ladder/class/InsideMstCheck.php';
require_once 'ladder/class/Config.php';
require_once 'ladder/class/Classmst.php';

/**
 * 院内研修: 振り返り
 */
class Ldr_InsideCheck extends Model
{

    var $log;
    var $inside_contents_id;
    var $mst;
    var $form;
    var $count_form;
    var $view_class;
    var $class_sql;

    /**
     * コンストラクタ
     * @param type $emp_id 職員ID
     */
    public function Ldr_InsideCheck($inside_contents_id)
    {
        parent::connectDB();
        $this->inside_contents_id = $inside_contents_id;
        $this->log = new Ldr_log('inside');

        $this->mst = new Ldr_InsideMstCheck();
        $template = $this->mst->find($inside_contents_id);
        $this->form = $template['form'];
        $this->count_form = $this->count_form();

        $conf = new Ldr_Config();
        $this->view_class = $conf->view_class();
        $this->class_sql = Ldr_Util::class_full_name_sql($conf->view_class());
    }

    /**
     * 集計項目
     * @return string
     */
    private function count_form()
    {
        $form = $this->form;

        $form['sex'] = array('type' => 'sex', 'name' => '性別');
        $form['age5'] = array('type' => 'age', 'name' => '年齢');
        $form['level'] = array('type' => 'level', 'name' => '認定レベル');
        $form['level_age'] = array('type' => 'age', 'name' => '認定後経過年数');
        $form['license_age5'] = array('type' => 'age', 'name' => '免許取得経過年数');
        $form['nurse_age5'] = array('type' => 'age', 'name' => '実務経験年数');
        $form['hospital_age5'] = array('type' => 'age', 'name' => '当院在籍年数');
        $form['ward_age5'] = array('type' => 'age', 'name' => '部署在籍年数');
        $form['class'] = array('type' => 'class', 'name' => '部署');
        $form['status'] = array('type' => 'status', 'name' => '役職');
        $form['job'] = array('type' => 'job', 'name' => '職種');
        return $form;
    }

    /**
     * 振り返りを集計する
     */
    public function count()
    {
        // 振り返りDBの作成
        $checkdb = $this->create_check_db();

        $count = array();
        $emp_count = $this->count_emp($checkdb);

        foreach ($this->count_form as $key => $form) {

            switch ($form['type']) {
                case 'text':
                case 'textarea':
                    list($table, $total) = $this->count_text($checkdb, $key);
                    break;
                case 'radio':
                case 'select':
                case 'checkbox':
                    list($table, $total) = $this->count_select($checkdb, $key, $form);
                    break;
                case 'level':
                    list($table, $total) = $this->count_level($checkdb);
                    break;
                case 'sex':
                    list($table, $total) = $this->count_sex($checkdb);
                    break;
                case 'age':
                    list($table, $total) = $this->count_age($checkdb, $key);
                    break;
                case 'class':
                    list($table, $total) = $this->count_class($checkdb);
                    break;
                case 'job':
                    list($table, $total) = $this->count_job($checkdb);
                    break;
                case 'status':
                    list($table, $total) = $this->count_status($checkdb);
                    break;
            }

            $count[] = array(
                'key' => $key,
                'form' => $form,
                'table' => $table,
                'total' => $total,
                'count' => $emp_count,
            );
        }

        return $count;
    }

    /**
     * 振り返りDBの作成
     */
    private function create_check_db()
    {

        $dbfile = '/var/tmp/comedix-carrier-inside-check-' . $this->inside_contents_id . '.db';

        // DBファイルの存在確認
        $new_file = false;
        if (!file_exists($dbfile)) {
            $new_file = true;
        }

        // DBファイルのサイズ確認
        if (!$new_file && filesize($dbfile) === 0) {
            $new_file = true;
        }

        // SQLite DBのオープン
        if (!$db = new PDO("sqlite:$dbfile")) {
            throw new Exception(__METHOD__ . ' ERROR: SQLite3 Connection Failed.');
        }

        // 振り返りデータが新しければ、データの作成しなおし
        $check_date = $this->check_updated_on();
        $db_date = filemtime($dbfile);
        if ($new_file || $check_date > $db_date) {
            $db->beginTransaction();

            $schema = $this->checkdata_schema();

            if (!$new_file) {
                // DROP TABLE
                foreach ($schema['drop'] as $ddl) {
                    if (!$db->query($ddl)) {
                        $err = $db->errorInfo();
                        $db->rollBack();
                        throw new Exception(__METHOD__ . " ERROR: SQLite3 {$ddl}: " . $err[2]);
                    }
                }
            }

            // CREATE TABLE
            foreach ($schema['create'] as $ddl) {
                if (!$db->query($ddl)) {
                    $err = $db->errorInfo();
                    $db->rollBack();
                    throw new Exception(__METHOD__ . " ERROR: SQLite3 {$ddl}: " . $err[2]);
                }
            }
            // CREATE INDEX
            foreach ($schema['index'] as $ddl) {
                if (!$db->query($ddl)) {
                    $err = $db->errorInfo();
                    $db->rollBack();
                    throw new Exception(__METHOD__ . " ERROR: SQLite3 {$ddl}: " . $err[2]);
                }
            }

            // PREPARE
            $sth = array();
            foreach ($schema['insert'] as $key => $sql) {
                $sth[$key] = $db->prepare($sql);
            }

            // INSERT
            $emp_ids = array();
            $list = $this->lists_checkdata();
            foreach ($list as $row) {

                // emp_id重複対策
                if ($emp_ids[$row['emp_id']]) {
                    continue;
                }
                $emp_ids[$row['emp_id']] = 1;

                // bind
                foreach (array('emp_id', 'emp_personal_id', 'name', 'class_nm', 'atrb_nm', 'dept_nm', 'room_nm', 'job_nm', 'st_nm') as $bind) {
                    $sth['checkdata']->bindValue($bind, $row[$bind], PDO::PARAM_STR);
                }
                foreach (array('sex', 'age', 'age5', 'class_id', 'atrb_id', 'dept_id', 'room_id', 'job_id', 'st_id', 'level', 'level_age', 'nurse_age', 'nurse_age5', 'hospital_age', 'hospital_age5', 'ward_age', 'ward_age5', 'license_age', 'license_age5', 'c1_order', 'c2_order', 'c3_order', 'c4_order') as $bind) {
                    $sth['checkdata']->bindValue($bind, $row[$bind], PDO::PARAM_INT);
                }

                if (!$sth['checkdata']->execute()) {
                    $err = $db->errorInfo();
                    $db->rollBack();
                    throw new Exception(__METHOD__ . ' ERROR: SQLite3 INSERT checkdata: ' . $err[2]);
                }

                foreach ($this->form as $key => $form) {
                    // チェックボックス
                    if ($form['type'] === 'checkbox') {
                        foreach ($row['form'][$key]['value'] as $value) {

                            $sth[$key]->bindValue('emp_id', $row['emp_id'], PDO::PARAM_STR);
                            $sth[$key]->bindValue($key, $value, PDO::PARAM_STR);

                            if (!$sth[$key]->execute()) {
                                $err = $db->errorInfo();
                                $db->rollBack();
                                throw new Exception(__METHOD__ . " ERROR: SQLite3 INSERT {$key}: " . $err[2]);
                            }
                        }
                    }
                    // それ以外(テキスト、ラジオボタン、プルダウン)
                    else {
                        $sth[$key]->bindValue('emp_id', $row['emp_id'], PDO::PARAM_STR);
                        $sth[$key]->bindValue($key, $row['form'][$key]['value'], PDO::PARAM_STR);

                        if (!$sth[$key]->execute()) {
                            $err = $db->errorInfo();
                            $db->rollBack();
                            throw new Exception(__METHOD__ . " ERROR: SQLite3 INSERT {$key}: " . $err[2]);
                        }
                    }
                }
            }

            $db->commit();
        }

        return $db;
    }

    /**
     * 振り返りDBのオープン
     * @return \PDO
     * @throws Exception
     */
    private function open_check_db()
    {

        $dbfile = '/var/tmp/comedix-carrier-inside-check-' . $this->inside_contents_id . '.db';

        // DBファイルの存在確認
        if (!file_exists($dbfile)) {
            throw new Exception(__METHOD__ . ' ERROR: SQLite3 DB File Not Found.');
        }

        // SQLite DBのオープン
        if (!$db = new PDO("sqlite:$dbfile")) {
            throw new Exception(__METHOD__ . ' ERROR: SQLite3 Connection Failed.');
        }

        return $db;
    }

    /**
     * 振り返り集計テーブルのスキーマ
     */
    private function checkdata_schema()
    {
        $schema = array();

        // DROP TABLE
        $schema['drop'][] = "DROP TABLE IF EXISTS checkdata;";

        // CREATE TABLE
        $schema['create'][] = "
            CREATE TABLE checkdata (
                emp_id TEXT PRIMARY KEY,
                emp_personal_id TEXT,
                name BLOB,
                sex INTEGER,
                age INTEGER,
                age5 INTEGER,
                class_id INTEGER,
                class_nm BLOB,
                atrb_id INTEGER,
                atrb_nm BLOB,
                dept_id INTEGER,
                dept_nm BLOB,
                room_id INTEGER,
                room_nm BLOB,
                job_id INTEGER,
                job_nm BLOB,
                st_id INTEGER,
                st_nm BLOB,
                level INTEGER,
                level_age INTEGER,
                nurse_age INTEGER,
                nurse_age5 INTEGER,
                hospital_age INTEGER,
                hospital_age5 INTEGER,
                ward_age INTEGER,
                ward_age5 INTEGER,
                license_age INTEGER,
                license_age5 INTEGER,
                c1_order INTEGER,
                c2_order INTEGER,
                c3_order INTEGER,
                c4_order INTEGER
            );
        ";

        // CREATE INDEX
        $schema['index'][] = "CREATE INDEX idx_age5 ON checkdata (age5);";
        $schema['index'][] = "CREATE INDEX idx_class ON checkdata (class_id, atrb_id, dept_id, room_id);";
        $schema['index'][] = "CREATE INDEX idx_job ON checkdata (job_id);";
        $schema['index'][] = "CREATE INDEX idx_st ON checkdata (st_id);";
        $schema['index'][] = "CREATE INDEX idx_level ON checkdata (level);";
        $schema['index'][] = "CREATE INDEX idx_level_age ON checkdata (level_age);";
        $schema['index'][] = "CREATE INDEX idx_nurse_age5 ON checkdata (nurse_age5);";
        $schema['index'][] = "CREATE INDEX idx_hospital_age5 ON checkdata (hospital_age5);";
        $schema['index'][] = "CREATE INDEX idx_ward_age5 ON checkdata (ward_age5);";
        $schema['index'][] = "CREATE INDEX idx_license_age5 ON checkdata (license_age5);";
        $schema['index'][] = "CREATE INDEX idx_class_order ON checkdata (c1_order, c2_order, c3_order, c4_order);";

        // INSERT
        $schema['insert']['checkdata'] = "
            INSERT INTO checkdata VALUES (
                :emp_id ,
                :emp_personal_id,
                :name,
                :sex,
                :age,
                :age5,
                :class_id,
                :class_nm,
                :atrb_id,
                :atrb_nm,
                :dept_id,
                :dept_nm,
                :room_id,
                :room_nm,
                :job_id,
                :job_nm,
                :st_id,
                :st_nm,
                :level,
                :level_age,
                :nurse_age,
                :nurse_age5,
                :hospital_age,
                :hospital_age5,
                :ward_age,
                :ward_age5,
                :license_age,
                :license_age5,
                :c1_order,
                :c2_order,
                :c3_order,
                :c4_order
            );
        ";

        // テンプレートから振り返りフィールドを取得
        foreach ($this->form as $key => $form) {
            // DROP
            $schema['drop'][] = "DROP TABLE IF EXISTS {$key};";

            // チェックボックスの場合
            if ($form['type'] === 'checkbox') {
                // CREATE
                $schema['create'][] = "CREATE TABLE {$key} (emp_id TEXT, {$key} BLOB, PRIMARY KEY(emp_id, {$key}));";
            }
            // それ以外の場合
            else {
                // CREATE
                $schema['create'][] = "CREATE TABLE {$key} (emp_id TEXT PRIMARY KEY, {$key} BLOB);";
            }

            // INDEX
            $schema['index'][] = "CREATE INDEX idx_{$key} ON {$key} ({$key});";

            // INSERT
            $schema['insert'][$key] = "INSERT INTO {$key} VALUES (:emp_id, :{$key});";
        }

        return $schema;
    }

    /**
     * 最新の振り返り日時
     */
    private function check_updated_on()
    {
        $sql = "
            SELECT
                extract(epoch from ck.updated_on)
            FROM ldr_trn_inside_check ck
            JOIN ldr_trn_inside i USING (training_id)
            JOIN ldr_mst_inside_date d USING (inside_date_id)
            WHERE d.inside_contents_id = ?
            ORDER BY ck.updated_on DESC
            LIMIT 1
        ";

        $updated_on = $this->db->extended->getOne($sql, null, array($this->inside_contents_id), array('integer'));
        if (PEAR::isError($updated_on)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $updated_on->getDebugInfo());
        }

        return $updated_on;
    }

    /**
     * 振り返りデータ一覧
     */
    private function lists_checkdata()
    {

        $sql = "
            SELECT
                i.emp_id,
                emp_personal_id,
                emp_lt_nm || ' ' || emp_ft_nm as name,
                emp_sex as sex,
                extract(year from age(current_date, to_date(emp_birth, 'YYYYMMDD'))) as age,
                trunc(extract(year from age(current_date, to_date(emp_birth, 'YYYYMMDD'))) / 5) * 5 as age5,
                c1.class_id ,
                c1.order_no as c1_order,
                class_nm ,
                c2.atrb_id ,
                c2.order_no as c2_order,
                atrb_nm ,
                c3.dept_id ,
                c3.order_no as c3_order,
                dept_nm ,
                c4.room_id ,
                c4.order_no as c4_order,
                room_nm ,
                job_id ,
                job_nm ,
                st_id ,
                st_nm ,
                (SELECT MAX(level) FROM ldr_level WHERE emp_id=e.emp_id AND ladder_id=1) AS level ,
                (SELECT extract(year from age(current_date, date)) FROM ldr_level WHERE emp_id=e.emp_id AND ladder_id=1 AND level=(SELECT MAX(level) FROM ldr_level WHERE emp_id=e.emp_id AND ladder_id=1)) AS level_age ,
                CASE
                    WHEN nurse_date iS NULL THEN null
                    ELSE extract(year from age(current_date, nurse_date))
                END as nurse_age,
                trunc(extract(year from age(current_date, nurse_date)) / 5) * 5 as nurse_age5,
                extract(year from age(current_date, hospital_date)) as hospital_age,
                trunc(extract(year from age(current_date, hospital_date)) / 5) * 5 as hospital_age5,
                extract(year from age(current_date, ward_date)) as ward_age,
                trunc(extract(year from age(current_date, ward_date)) / 5) * 5 as ward_age5,
                extract(year from age(current_date, license_date)) as license_age,
                trunc(extract(year from age(current_date, license_date)) / 5) * 5 as license_age5,
                data as form_data
            FROM ldr_trn_inside_check ck
            JOIN ldr_trn_inside i USING (training_id)
            JOIN ldr_mst_inside_date d USING (inside_date_id)
            JOIN empmst e USING (emp_id)
            JOIN classmst c1 ON c1.class_id = e.emp_class
            JOIN atrbmst c2 ON c2.class_id = e.emp_class AND c2.atrb_id = e.emp_attribute AND NOT atrb_del_flg
            JOIN deptmst c3 ON c3.atrb_id = e.emp_attribute AND c3.dept_id = e.emp_dept AND NOT dept_del_flg
            LEFT JOIN classroom c4 ON c4.dept_id = e.emp_dept AND (c4.room_id = e.emp_room OR c4.room_id IS NULL) AND NOT room_del_flg
            JOIN jobmst job ON job.job_id = e.emp_job AND NOT job_del_flg
            JOIN stmst st ON st.st_id = e.emp_st AND NOT st_del_flg
            LEFT JOIN ldr_employee le USING (emp_id)
            WHERE d.inside_contents_id = ?
        ";

        $sth = $this->db->prepare($sql, array('integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array($this->inside_contents_id));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }
        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['form'] = unserialize($row['form_data']);
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 集計(選択項目)
     * @param type $db
     * @param type $key
     * @param type $form
     * @return type
     */
    private function count_select($db, $key, $form)
    {
        $sql = "SELECT {$key} as label, COUNT(*) as value FROM {$key} GROUP BY {$key}";

        $table = array();
        $hash = array();
        $total = 0;
        foreach ($db->query($sql) as $row) {
            $hash[$row['label']] = $row;
            $total += $row['value'];
        }

        foreach (explode("\n", $form['select']) as $label) {
            $label = rtrim($label);
            if (array_key_exists($label, $hash)) {
                $table[] = $hash[$label];
            }
            else {
                $table[] = array('label' => $label, 'value' => 0);
            }
        }

        return array($table, $total);
    }

    /**
     * 集計(入力項目)
     * @param type $db
     * @param type $key
     * @return type
     */
    private function count_text($db, $key)
    {
        $sql = "SELECT {$key} as label, COUNT(*) as value FROM {$key} GROUP BY {$key} ORDER BY value DESC, label";

        $table = array();
        $total = 0;
        foreach ($db->query($sql) as $row) {
            if (Validation::is_empty($row['label'])) {
                $row['label'] = '(未入力)';
            }
            $table[] = $row;
            $total += $row['value'];
        }

        return array($table, $total);
    }

    /**
     * 集計(レベル)
     * @param type $db
     * @param type $key
     * @return type
     */
    private function count_level($db)
    {
        $sql = "SELECT level as label, COUNT(*) as value FROM checkdata GROUP BY level ORDER BY label";

        $table = array();
        $total = 0;
        foreach ($db->query($sql) as $row) {
            if (Validation::is_empty($row['label'])) {
                $row['label'] = '(なし)';
            }
            else {
                $row['label'] = Ldr_Const::get_roman_num($row['label']);
            }
            $table[] = $row;
            $total += $row['value'];
        }

        return array($table, $total);
    }

    /**
     * 集計(性別)
     * @param type $db
     * @return type
     */
    private function count_sex($db)
    {
        $sql = "SELECT sex as label, COUNT(*) as value FROM checkdata GROUP BY sex ORDER BY sex";
        $labels = array('1' => '男', '2' => '女', '0' => '不明', '3' => '不明');
        $table = array();
        $total = 0;
        foreach ($db->query($sql) as $row) {
            $row['label'] = $labels[$row['label']];
            $table[] = $row;
            $total += $row['value'];
        }

        return array($table, $total);
    }

    /**
     * 集計(年齢)
     * @param type $db
     * @param type $key
     * @return type
     */
    private function count_age($db, $key)
    {
        $sql = "SELECT {$key} as label, COUNT(*) as value FROM checkdata GROUP BY {$key} ORDER BY label";

        $table = array();
        $total = 0;
        foreach ($db->query($sql) as $row) {
            if (is_null($row['label'])) {
                $row['label'] = '(未設定)';
            }
            else {
                if ($key === 'age5') {
                    $row['label'] = sprintf('%d〜%d歳', $row['label'], $row['label'] + 4);
                }
                else if ($key === 'level_age') {
                    $row['label'] .= '年';
                }
                else {
                    $row['label'] = sprintf('%d〜%d年', $row['label'], $row['label'] + 4);
                }
            }
            $table[] = $row;
            $total += $row['value'];
        }

        return array($table, $total);
    }

    /**
     * 集計(部署)
     * @param type $db
     * @param type $key
     * @return type
     */
    private function count_class($db)
    {
        $sql = "SELECT {$this->class_sql}, COUNT(*) as value FROM checkdata GROUP BY dept_id, room_id ORDER BY c1_order, c2_order, c3_order, c4_order";
        $table = array();
        $total = 0;
        foreach ($db->query($sql) as $row) {
            $row['label'] = $row['class_full_name'];
            unset($row['class_full_name']);
            $table[] = $row;
            $total += $row['value'];
        }

        return array($table, $total);
    }

    /**
     * 集計(職種)
     * @param type $db
     * @param type $key
     * @return type
     */
    private function count_job($db)
    {
        $sql = "SELECT job_nm as label, COUNT(*) as value FROM checkdata GROUP BY job_id ORDER BY value DESC, label";

        $table = array();
        $total = 0;
        foreach ($db->query($sql) as $row) {
            $table[] = $row;
            $total += $row['value'];
        }

        return array($table, $total);
    }

    /**
     * 集計(役職)
     * @param type $db
     * @param type $key
     * @return type
     */
    private function count_status($db)
    {
        $sql = "SELECT st_nm as label, COUNT(*) as value FROM checkdata GROUP BY st_id ORDER BY value DESC, label";

        $table = array();
        $total = 0;
        foreach ($db->query($sql) as $row) {
            $table[] = $row;
            $total += $row['value'];
        }

        return array($table, $total);
    }

    /**
     * 振り返り入力数
     * @param type $db
     * @return type
     */
    private function count_emp($db)
    {
        $sql = "SELECT COUNT(*) as count FROM checkdata";
        $sth = $db->query($sql);
        $row = $sth->fetch(PDO::FETCH_ASSOC);

        return $row['count'];
    }

    /**
     * チャートデータ
     * @param type $key
     * @return type
     */
    public function charts($key)
    {
        // 振り返りDBのオープン
        $checkdb = $this->open_check_db();

        $chart = array();
        $form = $this->count_form[$key];
        switch ($form['type']) {
            case 'text':
                list($table, $total) = $this->count_text($checkdb, $key);
                foreach ($table as $row) {
                    if ($row['value']) {
                        $chart[] = array('data' => array(array('0', $row['value'] + 0)), 'label' => $row['label']);
                    }
                }
                $option = $this->charts_pie_options();
                break;

            case 'radio':
            case 'select':
                list($table, $total) = $this->count_select($checkdb, $key, $form);
                foreach ($table as $row) {
                    if ($row['value']) {
                        $chart[] = array('data' => array(array('0', $row['value'] + 0)), 'label' => $row['label']);
                    }
                }
                $option = $this->charts_pie_options();
                break;

            case 'checkbox':
                list($table, $total) = $this->count_select($checkdb, $key, $form);
                $cnt = '0';
                $ticks = array();
                foreach ($table as $row) {
                    if ($row['value']) {
                        $ticks[] = array($cnt, mb_substr($row['label'], 0, 6));
                        $chart[] = array('data' => array(array($cnt++, $row['value'] + 0)), 'label' => $row['label']);
                    }
                }
                $option = $this->charts_bars_options($ticks);
                break;

            case 'level':
                list($table, $total) = $this->count_level($checkdb);
                foreach ($table as $row) {
                    if ($row['value']) {
                        $chart[] = array('data' => array(array('0', $row['value'] + 0)), 'label' => $row['label']);
                    }
                }
                $option = $this->charts_pie_options();
                break;

            case 'sex':
                list($table, $total) = $this->count_sex($checkdb);
                foreach ($table as $row) {
                    if ($row['value']) {
                        $chart[] = array('data' => array(array('0', $row['value'] + 0)), 'label' => $row['label']);
                    }
                }
                $option = $this->charts_pie_options();
                break;

            case 'age':
                list($table, $total) = $this->count_age($checkdb, $key);
                foreach ($table as $row) {
                    if ($row['value']) {
                        $chart[] = array('data' => array(array('0', $row['value'] + 0)), 'label' => $row['label']);
                    }
                }
                $option = $this->charts_pie_options();
                break;

            case 'class':
                list($table, $total) = $this->count_class($checkdb);
                $cnt = '0';
                $ticks = array();
                foreach ($table as $row) {
                    if ($row['value']) {
                        $ticks[] = array($cnt, $row['label']);
                        $chart[] = array('data' => array(array($cnt++, $row['value'] + 0)));
                    }
                }
                $option = $this->charts_bars_options($ticks);
                break;

            case 'job':
                list($table, $total) = $this->count_job($checkdb);
                foreach ($table as $row) {
                    if ($row['value']) {
                        $chart[] = array('data' => array(array('0', $row['value'] + 0)), 'label' => $row['label']);
                    }
                }
                $option = $this->charts_pie_options();
                break;

            case 'status':
                list($table, $total) = $this->count_status($checkdb);
                foreach ($table as $row) {
                    if ($row['value']) {
                        $chart[] = array('data' => array(array('0', $row['value'] + 0)), 'label' => $row['label']);
                    }
                }
                $option = $this->charts_pie_options();
                break;

            case 'textarea':
                return array();
        }

        return array('data' => $chart, 'option' => $option);
    }

    /**
     * 円グラフオプション(Flotr2)
     * @return type
     */
    private function charts_pie_options()
    {
        return array(
            'HtmlText' => false,
            'shadowSize' => 0,
            'resolution' => 1,
            'fontSize' => 7,
            'grid' => array(
                'verticalLines' => false,
                'horizontalLines' => false,
            ),
            'xaxis' => array('showLabels' => false),
            'yaxis' => array('showLabels' => false),
            'pie' => array(
                'show' => true,
                'explode' => 2,
                'startAngle' => 272 / 180 * pi()
            ),
            'mouse' => array('track' => false),
            'legend' => array(
                'show' => false,
            ),
        );
    }

    /**
     * 棒グラフオプション(Flotr2)
     * @param type $ticks
     * @return type
     */
    private function charts_bars_options($ticks = array())
    {
        return array(
            'HtmlText' => false,
            'shadowSize' => 0,
            'resolution' => 1,
            'fontSize' => 7,
            'grid' => array(
                'verticalLines' => false,
                'horizontalLines' => true,
            ),
            'xaxis' => array(
                'ticks' => $ticks,
                'labelsAngle' => 45,
                'min' => 0
            ),
            'yaxis' => array(
                'showLabels' => true,
                'min' => 0,
                'autoscaleMargin' => 1
            ),
            'bars' => array(
                'show' => true,
                'horizontal' => false,
                'barWidth' => 0.5
            ),
            'mouse' => array('track' => false),
            'legend' => array(
                'show' => false,
            ),
        );
    }

    /**
     * CSV出力
     * @return type
     */
    public function export_csv()
    {

        // 見出し
        $title = array(
            '職員ID',
            '職員名',
            '性別',
            '年齢',
            '年齢(5歳刻み)',
            '職種',
            '役職',
            '認定レベル',
            '認定後経過年数',
            '免許取得後経過年数',
            '免許取得後経過年数(5年刻み)',
            '経験年数',
            '経験年数(5年刻み)',
            '当院在籍年数',
            '当院在籍年数(5年刻み)',
            '部署在籍年数',
            '部署在籍年数(5年刻み)',
        );

        // 部署見出し
        $busho = array();
        $classmst = new Ldr_Classmst();
        if ($this->view_class <= 1) {
            $busho[] = $classmst->class_name();
        }
        if ($this->view_class <= 2) {
            $busho[] = $classmst->atrb_name();
        }
        if ($this->view_class <= 3) {
            $busho[] = $classmst->dept_name();
        }
        if ($classmst->class_count() === 4) {
            $busho[] = $classmst->room_name();
        }
        array_splice($title, 5, 0, $busho);

        // 振り返り見出し
        foreach ($this->form as $form) {
            if ($form['type'] === 'checkbox') {
                $sel = explode("\n", $form['select']);
                foreach ($sel as $item) {
                    $title[] = trim($item);
                }
            }
            else {
                $title[] = $form['name'];
            }
        }

        // データ
        $fp = fopen('php://temp', 'r+b');
        $list = $this->lists_checkdata();
        foreach ($list as $row) {
            $data = array(
                $row['emp_personal_id'],
                $row['name'],
                ($row['sex'] === '1' ? '男' : '女'),
                $row['age'],
                $row['age5'],
                $row['job_nm'],
                $row['st_nm'],
                $row['level'],
                $row['level_age'],
                $row['license_age'],
                $row['license_age5'],
                $row['nurse_age'],
                $row['nurse_age5'],
                $row['hospital_age'],
                $row['hospital_age5'],
                $row['ward_age'],
                $row['ward_age5'],
            );

            $busho = array();
            if ($this->view_class <= 1) {
                $busho[] = $row['class_nm'];
            }
            if ($this->view_class <= 2) {
                $busho[] = $row['atrb_nm'];
            }
            if ($this->view_class <= 3) {
                $busho[] = $row['dept_nm'];
            }
            if ($classmst->class_count() === 4) {
                $busho[] = $row['room_nm'];
            }
            array_splice($data, 5, 0, $busho);


            foreach ($this->form as $key => $form) {
                if ($form['type'] === 'checkbox') {
                    $sel = explode("\n", $form['select']);
                    foreach ($sel as $item) {
                        $data[] = in_array(trim($item), $row['form'][$key]['value']) ? 1 : 0;
                    }
                }
                else {
                    $data[] = $row['form'][$key]['value'];
                }
            }

            fputcsv($fp, $data);
        }
        rewind($fp);

        $csv = implode(",", $title) . "\r\n";
        $csv .= str_replace(PHP_EOL, "\r\n", stream_get_contents($fp));

        return $csv;
    }

}
