<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'ladder/class/Validation.php';
require_once 'ladder/class/Log.php';

class Ldr_Form22 extends Model
{

    var $emp_id;
    var $log;

    /**
     * コンストラクタ
     * @param type $emp_id 職員ID
     */
    public function Ldr_Form22($emp_id)
    {
        parent::connectDB();
        $this->emp_id = $emp_id;
        $this->log = new Ldr_log('form22');
    }

    /**
     * validate
     * @param Array $data
     * @return Array
     */
    public function validate($data)
    {
        $error = array();

        // 期間
        if (Validation::is_empty($data['period'])) {
            $error['period'][] = '年月日が入力されていません';
        }
        else {
            if (!Validation::is_date($data['period'])) {
                $error['period'][] = '年月日が正しくありません';
            }
        }
        if (Validation::is_empty($data['flag'])) {
            $error['flag'][] = '院内/院外がチェックされていません';
        }
        else {
            if (intval($data['flag']) !== 1 && intval($data['flag']) !== 2) {
                $error['flag'][] = '院内/院外が正しくありません';
            }
        }

        // 研修名
        if (Validation::is_empty($data['title'])) {
            $error['title'][] = '研修名が入力されていません';
        }
        elseif (mb_strlen($data['title'], 'EUC-JP') > 200) {
            $error['title'][] = '研修名は200文字以内で入力してください';
        }

        // 研修名
        if (Validation::is_empty($data['organizer'])) {
            $error['organizer'][] = '主催が入力されていません';
        }
        elseif (mb_strlen($data['organizer'], 'EUC-JP') > 200) {
            $error['organizer'][] = '主催は200文字以内で入力してください';
        }

        return $error;
    }

    /**
     * csv用のテキストを作成
     * @return type
     */
    public function export_csv()
    {
        $list = $this->lists();
        $fp = fopen('php://temp', 'r+b');
        foreach ($list as $data) {
            if (intval($data['flag']) === 1) {
                $data['flag'] = "院内";
            }
            else if (intval($data['flag']) === 2) {
                $data['flag'] = "院外";
            }
            fputcsv($fp, array(
                $data['period'],
                $data['flag'],
                $data['title'],
                $data['organizer'],
                $data['category'],
            ));
        }
        rewind($fp);

        $csv = "年月日,院内/院外,研修名,主催,カテゴリ\r\n";
        $csv .= str_replace(PHP_EOL, "\r\n", stream_get_contents($fp));

        // Log
        $this->log->log("CSVをエクスポートしました");

        return $csv;
    }

    /**
     * データのインポート
     * @param type $file
     * @return type
     * @throws Exception
     */
    public function import_csv($file)
    {
        $fp = Ldr_Util::file_to_euc($file['tmp_name']);
        $line = 0;
        $cnt = 0;
        $error_all = array();
        $this->db->beginNestedTransaction();
        while ($row = fgetcsv($fp)) {
            $line++;
            $data = array();
            list($data['period'], $data['flag'], $data['title'], $data['organizer']) = $row;
            if ($data['period'] === '年月日') {
                continue;
            }
            if ($data['flag'] === '院内') {
                $data['flag'] = 1;
            }
            else if ($data['flag'] === '院外') {
                $data['flag'] = 2;
            }
            else {
                $data['flag'] = 0;
            }


            $er = $this->validate($data);
            foreach ($er as $tmp) {
                foreach ($tmp as $value) {
                    $error_all[] = $line . "行目： " . $value;
                }
            }
            if (empty($er)) {
                try {
                    $this->save($data);
                }
                catch (Exception $e) {
                    $error_all[] = $line . "行目： 更新エラー";
                    cmx_log($e->getMessage());
                }
            }
            $cnt++;
        }
        if (!empty($error_all)) {
            $this->db->failNestedTransaction();
            throw new Exception(implode(',', $error_all));
        }
        $this->db->completeNestedTransaction();

        // Log
        $this->log->log("{$cnt}件のデータをインポートしました");

        return $cnt . "件のデータをインポートしました";
    }

    /**
     * 検索条件の作成
     * @param type $cond
     * @return string
     */
    protected function make_condition($cond = null)
    {
        $cond_ret = "";
        $types = array();
        $data = array();
        if (!empty($cond)) {
            if (!empty($cond['srh_keyword'])) {
                $keywords = preg_split(Ldr_Const::SRH_DELIMITERS, $cond['srh_keyword'], -1, PREG_SPLIT_NO_EMPTY);
                $cond_key.= "AND ( ";
                foreach ($keywords as $key => $value) {
                    if ($key != 0) {
                        $cond_key.= " AND ";
                    }

                    $label_key = "key" . $key;
                    $cond_key.= "(title || ' ' || organizer || ' ' || category) ILIKE :" . $label_key . " ";
                    $types[] = 'text';
                    $data[$label_key] = "%" . $value . "%";
                }
                $cond_key.= ") ";
                $cond_ret .= $cond_key;
            }
            if (!empty($cond['srh_flag'])) {
                $cond_ret .= " AND flag= :srh_flag ";
                $types[] = 'text';
                $data['srh_flag'] = $cond['srh_flag'];
            }
            if (!empty($cond['srh_start_date'])) {
                $cond_ret .= " AND ( :srh_start_date <= period) ";
                $types[] = 'text';
                $data['srh_start_date'] = $cond['srh_start_date'];
            }
            if (!empty($cond['srh_last_date'])) {
                $cond_ret .= " AND (period <= :srh_last_date ) ";
                $types[] = 'text';
                $data['srh_last_date'] = $cond['srh_last_date'];
            }
        }
        $ret['cond'] = preg_replace("/AND/", "WHERE", $cond_ret, 1);
        $ret['types'] = $types;
        $ret['data'] = $data;
        return $ret;
    }

    /**
     * データ数を取得します
     * @return type
     */
    public function count($cond = null)
    {
        $sql = "
            SELECT
              count(*)
            FROM
              (
                SELECT
                  inner_id
                  , period
                  , flag
                  , title
                  , organizer
                  , category
                  , training_id
                FROM
                  ldr_form2_2
                WHERE
                  emp_id = :emp_id
                  AND NOT del_flg
                UNION
                SELECT
                  test_id AS inner_id
                  , to_date(t.assembly_actual_date, 'YYYYMMDD') AS period
                  , 3
                  , f.name || ' ' || c.title AS title
                  , a.lecturer AS organizer
                  , NULL
                  , NULL
                FROM study_test_status t
                JOIN study_contents c ON t.test_id = c.id AND NOT c.delete_flg
                JOIN study_folder f ON c.folder_id = f.id AND NOT f.delete_flg
                JOIN study_assembly a ON t.test_id = a.id AND NOT a.delete_flg
                WHERE
                  emp_id = :emp_id
                  AND t.assembly_actual_status=1
              ) form
        ";
        $ret = $this->make_condition($cond);
        $sql.= $ret['cond'];
        $types = array_merge(array('integer', 'text'), $ret['types']);
        $data = array_merge(array('emp_id' => $this->emp_id), $ret['data']);
        $count = $this->db->extended->getOne($sql, null, $data, $types);
        if (PEAR::isError($count)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $count->getDebugInfo());
        }

        return $count;
    }

    /**
     * 一覧の取得
     * @param type $st
     * @param type $pageCount
     * @param type $cond
     * @return type
     */
    public function lists($st = null, $pageCount = null, $cond = null)
    {
        $sql = "
            SELECT
              inner_id
              , period
              , flag
              , title
              , organizer
              , category
              , training_id
              , level
            FROM
              (
                SELECT
                    f22.inner_id,
                    f22.period,
                    f22.flag,
                    f22.title,
                    f22.organizer,
                    f22.category,
                    f22.training_id,
                    ldr_mst_inside.level
                FROM ldr_form2_2 f22
                LEFT JOIN ldr_trn_inside USING (training_id)
                LEFT JOIN ldr_mst_inside_date USING (inside_date_id)
                LEFT JOIN ldr_mst_inside USING (inside_id)
                WHERE
                  f22.emp_id = :emp_id
                  AND NOT f22.del_flg
                UNION
                SELECT
                  test_id AS inner_id
                  , to_date(t.assembly_actual_date, 'YYYYMMDD') AS period
                  , 3
                  , f.name || ' ' || c.title AS title
                  , 'バリテス' AS organizer
                  , NULL
                  , NULL
                  , NULL
                FROM study_test_status t
                JOIN study_contents c ON t.test_id = c.id AND NOT c.delete_flg
                JOIN study_folder f ON c.folder_id = f.id AND NOT f.delete_flg
                WHERE
                  emp_id = :emp_id
                  AND t.assembly_actual_status=1
              ) form
        ";
        $ret = $this->make_condition($cond);
        $sql .= $ret['cond'];
        $cond['order_by'][] = "inner_id";
        $order = "";
        foreach ($cond['order_by'] as $value) {
            $order.= empty($order) ? "order by " : ", ";
            $order.= $value;
        }
        $sql .= $order;

        if (isset($st) && isset($pageCount)) {
            $this->db->setLimit($pageCount, $st);
        }

        $types = array_merge(array('integer', 'text'), $ret['types']);
        $data = array_merge(array('emp_id' => $this->emp_id), $ret['data']);
        $sth = $this->db->prepare($sql, $types, MDB2_PREPARE_RESULT);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['level_roman'] = Ldr_Const::get_roman_num($row['level']);
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 保存
     * @param String $emp_id
     * @param Array $data
     */
    public function save($data)
    {
        $data['emp_id'] = $this->emp_id;

        $this->db->beginNestedTransaction();

        // INSERT
        if (empty($data['inner_id'])) {
            $sth = $this->db->prepare("INSERT INTO ldr_form2_2 (emp_id, period, flag, title, organizer, category) VALUES (:emp_id, :period, :flag, :title, :organizer, :category)", array('text', 'text', 'integer', 'text', 'text', 'text'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($data);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
            }
            $sth->free();
            $data['inner_id'] = $this->db->lastInsertID('ldr_form2_2', 'inner_id');
        }

        // UPDATE
        else {
            $sth = $this->db->prepare("UPDATE ldr_form2_2 SET period= :period, flag= :flag ,title= :title, organizer= :organizer, category= :category WHERE inner_id= :inner_id AND emp_id = :emp_id", array('text', 'integer', 'text', 'text', 'text', 'text', 'integer', 'text'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($data);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
            }
            $sth->free();
        }

        $this->db->completeNestedTransaction();

        // Log
        $this->log->log("保存しました", "id={$data['inner_id']} title={$data['title']}");
    }

    /**
     * 研修から保存
     * @param array $data
     * @throws Exception
     */
    public function save_training($data)
    {
        $this->db->beginNestedTransaction();

        $data['emp_id'] = $this->emp_id;

        // UPDATE or INSERT
        $sth = $this->db->prepare("SELECT update_ldr_form2_2_training(:emp_id, :flag, :training_id, :period, :title, :organizer)", array('text', 'integer', 'integer', 'date', 'text', 'text'), MDB2_PREPARE_MANIP);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }
        $sth->free();

        $this->db->completeNestedTransaction();
    }

    /**
     * 削除
     * @param type $id     inner_id
     * @throws Exception
     */
    public function delete($id)
    {
        $this->db->beginNestedTransaction();
        $data = $this->find($id);
        if (isset($data)) {
            // UPDATE
            $data["del_flg"] = TRUE;

            $sth = $this->db->prepare("UPDATE ldr_form2_2 SET del_flg = true WHERE inner_id= :inner_id AND emp_id = :emp_id", array('boolean'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($data);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
            }
        }

        $sth->free();

        $this->db->completeNestedTransaction();
        // Log
        $this->log->log("削除しました", "id=$id");
    }

    /**
     * 削除（研修用）
     * flagとtraining_idから削除を行う
     * @param type $flag
     * @param type $training_id
     * @throws Exception
     */
    public function delete_training($flag, $training_id)
    {
        if (empty($training_id) || empty($flag)) {
            throw new Exception(__METHOD__ . " METHOD Param error.");
        }
        $this->db->beginNestedTransaction();

        // UPDATE
        $sth = $this->db->prepare("UPDATE ldr_form2_2 SET del_flg = true WHERE flag= :flag AND training_id = :training_id AND NOT del_flg", array('boolean'), MDB2_PREPARE_MANIP);
        $res = $sth->execute(array('flag' => $flag, 'training_id' => $training_id));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }

        $sth->free();

        $this->db->completeNestedTransaction();
    }

    /**
     * inner_idに該当するデータを取得
     * @param Integer $id
     * @return Array
     */
    public function find($id)
    {
        $sql = "SELECT inner_id, emp_id,period, flag, title, organizer, category, training_id FROM ldr_form2_2 WHERE inner_id= :inner_id AND emp_id = :emp_id AND NOT del_flg";
        $row = $this->db->extended->getRow(
            $sql, null, array('inner_id' => $id, 'emp_id' => $this->emp_id), array('integer', 'text'), MDB2_FETCHMODE_ASSOC
        );
        if (PEAR::isError($row)) {
            throw new Exception(__METHOD__ . " ERROR: " . $row->getDebugInfo());
        }
        return $row;
    }

    /**
     * 最終更新日の取得
     * @return type
     */
    public function last_updated()
    {
        $sql = "SELECT to_char( max(updated_on),'YYYY-MM-DD HH24:MI:SS') as updated_on FROM ldr_form2_2
                WHERE emp_id = :emp_id AND NOT del_flg";
        $row = $this->db->extended->getRow(
            $sql, null, array('emp_id' => $this->emp_id), array('text'), MDB2_FETCHMODE_ASSOC
        );
        if (empty($row)) {
            return null;
        }

        return $row['updated_on'];
    }

}
