<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'ladder/class/Validation.php';
require_once 'ladder/class/Apply.php';
require_once 'ladder/class/Log.php';

class Ldr_Report extends Model
{

    private $log;
    private $apply_id;
    private $guideline;

    /**
     * コンストラクタ
     * @param type $emp_id
     */
    public function __construct($apply_id, $guideline)
    {
        parent::connectDB();
        $this->apply_id = $apply_id;
        $this->guideline = $guideline;
        $this->log = new Ldr_log('report');
    }

    /**
     * reporter_validate
     * @param type $data
     * @return string
     */
    public function reporter_validate($data)
    {
        $error = array();
        if (Validation::is_empty($data['reporter'])) {
            $error['reporter'][] = 'レポート評価者が設定されていません';
        }
        return $error;
    }

    /**
     * 一件検索 & 複数件検索
     * @param type $id 申請ID
     * @return type
     * @throws Exception
     */
    public function reporter_find()
    {
        if (is_null($this->apply_id)) {
            return array();
        }

        $sql = "
            SELECT
                emp_id,
                e.emp_lt_nm || ' ' || e.emp_ft_nm as emp_name
            FROM ldr_reporter
            JOIN empmst e USING (emp_id)
            WHERE ldr_apply_id=:ldr_apply_id
        ";
        $sth = $this->db->prepare($sql, array('integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array('ldr_apply_id' => $this->apply_id));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }
        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[$row['emp_id']] = $row['emp_name'];
        }
        $sth->free();
        return $lists;
    }

    /**
     * 保存
     * @param Array $data
     */
    public function reporter_save($data)
    {
        $this->db->beginNestedTransaction();

        //同じidr_apply_idを消す。
        $sth = $this->db->prepare("DELETE FROM ldr_reporter WHERE ldr_apply_id = :id ", array('integer'), MDB2_PREPARE_MANIP);
        $res = $sth->execute($data);

        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception('ldr_reporter INSERT or UPDATE ERROR: ' . $res->getDebugInfo());
        }

        //同じidr_apply_idを追加する
        foreach ($data['reporter'] as $reporter) {
            $data = array('id' => $data['id'], 'reporter' => $reporter);
            $sth = $this->db->prepare("INSERT INTO ldr_reporter (ldr_apply_id, emp_id) VALUES (:id, :reporter) ", array('integer', 'text'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($data);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception('ldr_reporter INSERT or UPDATE ERROR: ' . $res->getDebugInfo());
            }
        }

        $this->db->completeNestedTransaction();
        $this->log->log("保存しました", "id={$data['id']}");
    }

    /**
     * 評価コメントの取得
     * @param type $id
     * @param type $level
     * @param type $apply_emp_id
     * @param type $accept_emp_id
     * @return type
     */
    public function assessment()
    {
        $sql = "
            SELECT
                ldr_report_comment.*,
                e.emp_lt_nm || ' ' || e.emp_ft_nm as emp_name
            FROM ldr_report_comment
            JOIN empmst e USING (emp_id)
            WHERE ldr_apply_id= :ldr_apply_id
        ";
        $row = $this->db->extended->getRow(
            $sql, null, array('ldr_apply_id' => $this->apply_id), array('integer'), MDB2_FETCHMODE_ASSOC
        );
        if (PEAR::isError($row)) {
            throw new Exception('ldr_report_assessment SELECT ERROR: ' . $row->getDebugInfo());
        }

        // list
        $row['list'] = $this->assessment_lists();

        return $row;
    }

    /**
     * 評価データの取得
     * @param type $id
     * @param type $level
     * @param type $apply_emp_id
     * @param type $accept_emp_id
     * @return type
     */
    private function assessment_lists()
    {
        $sql = "
            SELECT
                g.guideline_id,
                g.guideline_group1,
                g.guideline_group2,
                g.guideline,
                g.criterion,
                a.assessment as apply_assessment,
                g2.name as group2_name,
                (SELECT COUNT(*) FROM ldr_guideline gg2 WHERE g.guideline_group2=gg2.guideline_group2 AND NOT gg2.del_flg) AS group2_count,
                g2.comment as group2_comment
            FROM ldr_guideline g
            JOIN ldr_guideline_group1 g1 ON g.guideline_group1=g1.guideline_group1 AND NOT g1.del_flg
            JOIN ldr_guideline_group2 g2 ON g.guideline_group2=g2.guideline_group2 AND NOT g2.del_flg
            LEFT JOIN ldr_report_assessment a ON a.ldr_apply_id= :ldr_apply_id AND a.guideline_id=g.guideline_id
            WHERE NOT g.del_flg
              AND g.guideline_revision = :guideline
            ORDER BY g.order_no, g.guideline_id
            ";
        $sth = $this->db->prepare($sql, null, MDB2_PREPARE_RESULT);
        $res = $sth->execute(array(
            'ldr_apply_id' => $this->apply_id,
            'guideline' => $this->guideline,
            )
        );
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * レポート評価コメントの保存
     * @param Array $data
     */
    public function save_report_comment($data, $mode = null)
    {
        if ($mode === 'auto') {
            $data['comment'] = from_utf8($data['comment']);
        }

        $this->db->beginNestedTransaction();

        $sth = $this->db->prepare("SELECT update_ldr_report_comment(:id, :emp_id, :comment)", array('integer', 'text', 'text'), MDB2_PREPARE_MANIP);
        $res = $sth->execute(array_merge($data, array('emp_id' => $data['emp_id'])));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }
        $sth->free();

        $this->db->completeNestedTransaction();
    }

    /**
     * レポート評価コメント(admin)の保存
     * @param Array $data
     */
    public function save_report_admin_comment($data, $mode = null)
    {
        if ($mode === 'auto') {
            $data['admin_comment'] = from_utf8($data['admin_comment']);
        }

        $this->db->beginNestedTransaction();
        $sth = $this->db->prepare("UPDATE ldr_report_comment SET admin_comment = :admin_comment WHERE ldr_apply_id= :id", array('text', 'integer'), MDB2_PREPARE_MANIP);
        $res = $sth->execute(array_merge($data));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }
        $sth->free();

        $this->db->completeNestedTransaction();
    }

    /**
     * レポート評価の保存
     * @param type $data
     * @throws Exception
     */
    public function save_report_assessment($data, $mode = null)
    {
        $this->db->beginNestedTransaction();

        $sql_upd = 'SELECT update_ldr_report_assessment(:ldr_apply_id, :emp_id, :guideline_id, :assessment)';
        $sth_upd = $this->db->prepare($sql_upd, array('integer', 'text', 'integer', 'integer'), MDB2_PREPARE_MANIP);

        foreach ($data['assessment'] as $guideline_id => $assessment) {
            $res_upd = $sth_upd->execute(array(
                'ldr_apply_id' => $data['id'],
                'emp_id' => $data['emp_id'],
                'guideline_id' => $guideline_id,
                'assessment' => $assessment,
            ));
            if (PEAR::isError($res_upd)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " ERROR: " . $res_upd->getDebugInfo());
            }
        }

        $this->db->completeNestedTransaction();
    }

    /**
     * 評価完了の入力バリデーション
     */
    public function validate_report_assessment()
    {
        $error = array();
        $assessment = $this->assessment();

        // コメント
        if (empty($assessment['comment'])) {
            $error['assessment'][] = 'メッセージが入力されていません';
        }

        // 評価
        foreach ($assessment['list'] as $row) {
            if (is_null($row['apply_assessment'])) {
                $error['assessment'][] = "未評価の項目があります";
                break;
            }
        }

        return $error;
    }
    /**
     * 評価完了の入力バリデーション
     */
    public function validate_report_assessment_admin()
    {
        $error = array();
        $assessment = $this->assessment();

        // コメント
        if (empty($assessment['admin_comment'])) {
            $error['assessment'][] = 'メッセージが入力されていません';
        }

        return $error;
    }

    /**
     * 評価完了
     * @param type $id        申請ID
     * @param type $emp_id    職員ID
     * @throws Exception
     */
    public function assessment_completed()
    {
        $this->db->beginNestedTransaction();

        $sth = $this->db->prepare("UPDATE ldr_report_comment SET completed = true, updated_on = CURRENT_TIMESTAMP WHERE ldr_apply_id= :ldr_apply_id", array('integer'), MDB2_PREPARE_MANIP);
        $res = $sth->execute(array('ldr_apply_id' => $this->apply_id));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }
        $sth->free();
        $this->db->completeNestedTransaction();
    }

    /**
     * 評価確認完了
     * @param type $id        申請ID
     * @param type $emp_id    職員ID
     * @throws Exception
     */
    public function assessment_admin_completed()
    {
        $this->db->beginNestedTransaction();

        $sth = $this->db->prepare("UPDATE ldr_report_comment SET admin_completed = true WHERE ldr_apply_id= :ldr_apply_id", array('integer'), MDB2_PREPARE_MANIP);
        $res = $sth->execute(array('ldr_apply_id' => $this->apply_id));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }
        $sth->free();
        $this->db->completeNestedTransaction();
    }

}
