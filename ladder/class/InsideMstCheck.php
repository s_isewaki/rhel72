<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'ladder/class/Log.php';
require_once 'ladder/class/CheckTemplate.php';

/**
 * 振り返り
 */
class Ldr_InsideMstCheck extends Model
{

    var $log;
    var $template;

    /**
     * コンストラクタ
     */
    public function Ldr_InsideMstCheck()
    {
        parent::connectDB();
        $this->log = new Ldr_log('inside_check');
        $this->template = new Ldr_CheckTemplate();
    }

    /**
     * validate
     * @param type $data
     * @return string
     */
    public function validate($data)
    {

        $data['title'] = "タイトル";
        return $this->template->validate($data);
    }

    /**
     * 保存
     * @param array $data
     * @throws Exception
     */
    public function save($data)
    {
        $this->db->beginNestedTransaction();

        $data['form'] = serialize($data['form']);

        // UPDATE or INSERT
        $sth = $this->db->prepare("SELECT update_ldr_mst_inside_check (:id,:form)", array('integer', 'text'), MDB2_PREPARE_MANIP);
        $res = $sth->execute($data);
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception('update_ldr_check_template UPDATE or INSERT ERROR ' . $res->getDebugInfo());
        }
        $sth->free();
        $this->db->completeNestedTransaction();
        $this->log->log("保存しました", "tmpl_id={$data['id']}");
    }

    public function lists_template()
    {
        return $this->template->lists();
    }

    /**
     * 取得（テンプレート）
     * @param type $id
     * @return type
     */
    public function find_template($id)
    {
        return $this->template->find($id);
    }

    /**
     * 取得
     * @param type $id
     * @return type
     * @throws Exception
     */
    public function find($id)
    {
        if (is_null($id)) {
            return array();
        }

        $sql = "
            SELECT
                inside_contents_id,
                form,
                updated_on
            FROM ldr_mst_inside_check
            WHERE
                NOT del_flg
                AND inside_contents_id= :inside_contents_id
        ";
        $row = $this->db->extended->getRow(
            $sql, null, array('inside_contents_id' => $id), array('integer'), MDB2_FETCHMODE_ASSOC
        );
        if (PEAR::isError($row)) {
            throw new Exception('ldr_mst_inside_check SELECT ERROR: ' . $row->getDebugInfo());
        }

        $row['form'] = unserialize($row['form']);
        return $row;
    }

}
