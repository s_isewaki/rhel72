<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'ladder/class/Const.php';
require_once 'ladder/class/Validation.php';
require_once 'ladder/class/Log.php';

class Ldr_GuidelineGuideline extends Model
{

    var $log;

    /**
     * コンストラクタ
     */
    public function Ldr_GuidelineGuideline()
    {
        parent::connectDB();
        $this->log = new Ldr_log('guideline');
    }

    /**
     * validate
     * @param Array $data
     * @return Array
     */
    public function validate($data)
    {
        $error = array();

        // 版
        if (Validation::is_empty($data['guideline_revision'])) {
            $error['guideline_revision'][] = '版が選択されていません';
        }
        else {
            $rv = new Ldr_GuidelineRevision();
            $row = $rv->find($data['guideline_revision']);
            if (empty($row)) {
                $error['guideline_revision'][] = '不正な版が選択されています';
            }
        }

        // 領域
        if (Validation::is_empty($data['guideline_group1'])) {
            $error['guideline_group1'][] = '領域が選択されていません';
        }
        else {
            $g1 = new Ldr_GuidelineGroup1();
            $row = $g1->find($data['guideline_group1']);
            if (empty($row)) {
                $error['guideline_group1'][] = '不正な領域が選択されています';
            }
        }

        // 分類
        if (Validation::is_empty($data['guideline_group2'])) {
            $error['guideline_group2'][] = '分類が選択されていません';
        }
        else {
            $g2 = new Ldr_GuidelineGroup2();
            $row = $g2->find($data['guideline_group2']);
            if (empty($row)) {
                $error['guideline_group2'][] = '不正な分類が選択されています';
            }
        }

        // レベル
        if (Validation::is_empty($data['level'])) {
            $error['level'][] = 'レベルが選択されていません';
        }
        elseif (!Validation::is_level($data['level'])) {
            $error['level'][] = '不正なレベルが選択されています';
        }

        // 行動指標
        if (Validation::is_empty($data['guideline'])) {
            $error['guideline'][] = '行動指標が入力されていません';
        }

        return $error;
    }

    /**
     * 一覧の取得
     * @return Array
     */
    public function lists($cond)
    {
        $sql = "
            SELECT
                guideline_id,
                gl.guideline_revision,
                gl.guideline_group1,
                gl.guideline_group2,
                level,
                guideline,
                criterion,
                g1.name AS group1_name,
                g2.name AS group2_name,
                EXISTS (SELECT * from ldr_assessment a WHERE a.guideline_id = gl.guideline_id AND NOT a.del_flg) AS used
            FROM ldr_guideline gl
            JOIN ldr_guideline_group1 g1 USING (guideline_group1, guideline_revision)
            JOIN ldr_guideline_group2 g2 USING (guideline_group2, guideline_revision)
            WHERE
                NOT gl.del_flg
                AND guideline_revision = :guideline_revision
                AND gl.guideline_group1 = :guideline_group1
                AND level = :level
            ORDER BY g1.order_no, g2.order_no, gl.order_no, gl.guideline_id
        ";
        $sth = $this->db->prepare($sql, array('integer', 'integer', 'integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute($cond);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['level_roman'] = $row['level'] ? Ldr_Const::get_roman_num($row['level']) : 'なし';
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 保存
     * @param Array $data
     */
    public function save($data)
    {
        $this->db->beginNestedTransaction();

        // INSERT
        if (empty($data['id'])) {
            $sth = $this->db->prepare("INSERT INTO ldr_guideline (guideline_revision, guideline_group1, guideline_group2, level, guideline, criterion) VALUES (:guideline_revision, :guideline_group1, :guideline_group2, :level, :guideline, :criterion)", array('integer', 'integer', 'integer', 'integer', 'text', 'text'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($data);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
            }
            $sth->free();
            $data['id'] = $this->db->lastInsertID('ldr_guideline', 'guideline_id');
        }

        // UPDATE
        else {
            $sth = $this->db->prepare("UPDATE ldr_guideline SET guideline_revision= :guideline_revision, guideline_group1= :guideline_group1, guideline_group2= :guideline_group2, level= :level, guideline= :guideline, criterion= :criterion WHERE guideline_id= :id", array('integer', 'integer', 'integer', 'integer', 'text', 'text', 'integer'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($data);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " id={$data['id']} ERROR: " . $res->getDebugInfo());
            }
            $sth->free();
        }
        $this->db->completeNestedTransaction();

        // Log
        $this->log->log("行動指標を保存しました", "id={$data['id']} revison={$data['guideline_revision']} group1={$data['guideline_group1']} group2={$data['guideline_group2']} level={$data['level']} guideline={$data['guideline']}");
    }

    /**
     * 削除
     * @param type $id
     * @throws Exception
     */
    public function delete($id)
    {
        $data = $this->find($id);
        if (isset($data)) {
            $this->db->beginNestedTransaction();
            $sth = $this->db->prepare("UPDATE ldr_guideline SET del_flg = true WHERE guideline_id= ?", array('integer'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($id);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " id={$data['guideline_id']} ERROR: " . $res->getDebugInfo());
            }
            $this->db->completeNestedTransaction();
            $sth->free();
        }

        // Log
        $this->log->log("行動指標を削除しました", "id=$id");
    }

    /**
     * 1件取得
     * @param Integer $id
     * @return Array
     */
    public function find($id)
    {
        if (is_null($id)) {
            return array();
        }

        $sql = "SELECT guideline_id AS id, guideline_revision, guideline_group1, guideline_group2, level, guideline, criterion FROM ldr_guideline WHERE guideline_id= :guideline_id";
        $row = $this->db->extended->getRow(
            $sql, null, array('guideline_id' => $id), array('integer'), MDB2_FETCHMODE_ASSOC
        );
        if (empty($row)) {
            throw new Exception();
        }
        return $row;
    }

    /**
     * 並べ替え
     * @param type $data
     * @throws Exception
     */
    public function sort($data)
    {
        $this->db->beginNestedTransaction();
        $sth = $this->db->prepare("UPDATE ldr_guideline SET order_no= ? WHERE guideline_id= ?", array('integer', 'integer'), MDB2_PREPARE_MANIP);
        $cnt = 1;
        foreach ($data['order'] as $id) {
            $res = $sth->execute(array($cnt++, $id));
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " id={$id} ERROR: " . $res->getDebugInfo());
            }
        }
        $this->db->completeNestedTransaction();
        $sth->free();

        // Log
        $this->log->log("行動指標を並べ替えました", implode(" ", $data['order']));
    }

    /**
     * 行動指標全リスト(query)
     * @return type
     */
    function full_lists($revision, $query)
    {

        $query = from_utf8($query);

        $sql = "
            SELECT
                guideline_id as id,
                guideline as value
            FROM ldr_guideline gl
            JOIN ldr_guideline_group1 g1 USING (guideline_group1, guideline_revision)
            JOIN ldr_guideline_group2 g2 USING (guideline_group2, guideline_revision)
            WHERE
                NOT gl.del_flg
                AND guideline_revision = ?
                AND guideline LIKE ?
            ORDER BY g1.order_no, g2.order_no, gl.order_no, gl.guideline_id
        ";
        $sth = $this->db->prepare($sql, array('integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array($revision, "%$query%"));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 行動指標有無チェック
     * @param type $revision
     * @return boolean
     */
    public function is_lists($revision)
    {
        $count = $this->db->extended->getOne(
            'SELECT COUNT(*) FROM ldr_guideline WHERE NOT del_flg AND guideline_revision = ?', null, array($revision), array('integer')
        );

        if ($count > 0) {
            return true;
        }
        return false;
    }

    /**
     * CSV出力
     * @param type $revision
     * @return type
     * @throws Exception
     */
    public function export_csv($revision)
    {
        $sql = "
            SELECT
                *,
                g1.name as g1_name,
                g2.name as g2_name
            FROM ldr_guideline gl
            JOIN ldr_guideline_group1 g1 USING (guideline_group1, guideline_revision)
            JOIN ldr_guideline_group2 g2 USING (guideline_group2, guideline_revision)
            WHERE
                NOT gl.del_flg
                AND guideline_revision = ?
            ORDER BY gl.level, g1.order_no, g2.order_no, gl.order_no, gl.guideline_id
        ";
        $sth = $this->db->prepare($sql, array('integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array($revision));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $fp = fopen('php://temp', 'r+b');

        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            fputcsv($fp, array(
                $row['guideline_id'],
                $row['level'],
                $row['g1_name'],
                $row['g2_name'],
                $row['guideline'],
                $row['criterion'],
            ));
        }
        rewind($fp);

        $csv = "行動指標ID,レベル,領域,分類,行動指標,判断基準\r\n";
        $csv .= str_replace(PHP_EOL, "\r\n", stream_get_contents($fp));

        $sth->free();
        return $csv;
    }

}
