<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'ladder/class/Validation.php';
require_once 'ladder/class/Log.php';

class Ldr_GuidelineRevision extends Model
{

    var $log;

    /**
     * コンストラクタ
     */
    public function Ldr_GuidelineRevision()
    {
        parent::connectDB();
        $this->log = new Ldr_log('guideline');
    }

    /**
     * validate
     * @param Array $data
     * @return Array
     */
    public function validate($data)
    {
        $error = array();

        // 研修名
        if (Validation::is_empty($data['name'])) {
            $error['name'][] = '版名が入力されていません';
        }
        elseif (mb_strlen($data['name'], 'EUC-JP') > 200) {
            $error['name'][] = '版名は200文字以内で入力してください';
        }

        return $error;
    }

    /**
     * 一覧の取得
     * @return Array
     */
    public function lists()
    {
        $sql = "SELECT
                    guideline_revision,
                    name,
                    (SELECT COUNT(*) FROM ldr_guideline g WHERE g.guideline_revision=r.guideline_revision AND NOT del_flg) AS count
                FROM ldr_guideline_revision r
                WHERE NOT del_flg ORDER BY updated_on desc";
        $sth = $this->db->prepare($sql, array('integer', 'text'), MDB2_PREPARE_RESULT);
        $res = $sth->execute();
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 保存
     * @param Array $data
     */
    public function save($data)
    {
        $this->db->beginNestedTransaction();

        // INSERT
        if (empty($data['id'])) {
            $sth = $this->db->prepare("INSERT INTO ldr_guideline_revision (name) VALUES (:name)", array('text'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($data);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
            }
            $sth->free();
            $data['id'] = $this->db->lastInsertID('ldr_guideline_revision', 'guideline_revision');
        }

        // UPDATE
        else {
            $sth = $this->db->prepare("UPDATE ldr_guideline_revision SET name= :name WHERE guideline_revision= :id", array('text', 'integer'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($data);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . "id={$data['id']} ERROR: " . $res->getDebugInfo());
            }
            $sth->free();
        }
        $this->db->completeNestedTransaction();

        // Log
        $this->log->log("版を保存しました", "guideline_revision={$data['id']} name={$data['name']}");
    }

    /**
     * 削除
     * @param type $id
     * @throws Exception
     */
    public function delete($id)
    {
        $data = $this->find($id);
        if (isset($data)) {
            $this->db->beginNestedTransaction();
            $sth = $this->db->prepare("UPDATE ldr_guideline_revision SET del_flg = true WHERE guideline_revision= ?", array('integer'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($id);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . "guideline_revision={$data['guideline_revision']} ERROR: " . $res->getDebugInfo());
            }
            $this->db->completeNestedTransaction();
            $sth->free();

            // Log
            $this->log->log("版を削除しました", "guideline_revision=$id");
        }
    }

    /**
     * 1件取得
     * @param Integer $id
     * @return Array
     */
    public function find($id)
    {
        if (is_null($id)) {
            return array();
        }

        $sql = "SELECT guideline_revision AS id, name FROM ldr_guideline_revision WHERE guideline_revision= :guideline_revision";
        $row = $this->db->extended->getRow(
            $sql, null, array('guideline_revision' => $id), array('integer'), MDB2_FETCHMODE_ASSOC
        );
        if (PEAR::isError($row)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $row->getDebugInfo());
        }
        return $row;
    }

}
