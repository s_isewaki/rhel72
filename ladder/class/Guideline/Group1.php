<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'ladder/class/Validation.php';
require_once 'ladder/class/Log.php';

class Ldr_GuidelineGroup1 extends Model
{

    var $log;

    /**
     * コンストラクタ
     */
    public function Ldr_GuidelineGroup1()
    {
        parent::connectDB();
        $this->log = new Ldr_log('guideline');
    }

    /**
     * validate
     * @param Array $data
     * @return Array
     */
    public function validate($data)
    {
        $error = array();

        // 版
        if (Validation::is_empty($data['guideline_revision'])) {
            $error['guideline_revision'][] = '版が選択されていません';
        }
        else {
            $rv = new Ldr_GuidelineRevision();
            $row = $rv->find($data['guideline_revision']);
            if (empty($row)) {
                $error['guideline_revision'][] = '不正な版が選択されています';
            }
        }

        // 領域
        if (Validation::is_empty($data['name'])) {
            $error['name'][] = '領域名が入力されていません';
        }
        elseif (mb_strlen($data['name'], 'EUC-JP') > 200) {
            $error['name'][] = '領域名は200文字以内で入力してください';
        }

        // form7
        if (Validation::is_empty($data['form7'])) {
            $error['form7'][] = '様式７が設定されていません';
        }

        return $error;
    }

    /**
     * 一覧の取得
     * @return Array
     */
    public function lists($cond)
    {
        if ($cond['form7'] === true) {
            $and_form7 = 'AND form7';
        }

        $sql = "
            SELECT
                guideline_group1,
                guideline_revision,
                name,
                form7,
                comment,
                EXISTS (SELECT * FROM ldr_guideline_group2 g2 WHERE g2.guideline_group1=g1.guideline_group1 AND NOT g2.del_flg) AS used
            FROM ldr_guideline_group1 g1
            WHERE NOT del_flg
              AND guideline_revision = :guideline_revision
            $and_form7
            ORDER BY order_no, guideline_group1
            ";
        $sth = $this->db->prepare($sql, array('integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute($cond);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 保存
     * @param Array $data
     */
    public function save($data)
    {
        $this->db->beginNestedTransaction();

        // INSERT
        if (empty($data['id'])) {
            $sth = $this->db->prepare("INSERT INTO ldr_guideline_group1 (guideline_revision, name, form7, comment) VALUES (:guideline_revision, :name, :form7, :comment)", array('integer', 'text', 'boolean', 'text'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($data);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
            }
            $sth->free();
            $data['id'] = $this->db->lastInsertID('ldr_guideline_group1', 'guideline_group1');
        }

        // UPDATE
        else {
            $sth = $this->db->prepare("UPDATE ldr_guideline_group1 SET guideline_revision= :guideline_revision, name= :name, form7= :form7, comment= :comment WHERE guideline_group1= :id", array('integer', 'text', 'boolean', 'text', 'integer'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($data);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
            }
            $sth->free();
        }
        $this->db->completeNestedTransaction();

        // Log
        $this->log->log("領域を保存しました", "group1={$data['id']} name={$data['name']}");
    }

    /**
     * 削除
     * @param type $id
     * @throws Exception
     */
    public function delete($id)
    {
        $data = $this->find($id);
        if (isset($data)) {
            $this->db->beginNestedTransaction();
            $sth = $this->db->prepare("UPDATE ldr_guideline_group1 SET del_flg = true WHERE guideline_group1= ?", array('integer'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($id);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
            }
            $this->db->completeNestedTransaction();
            $sth->free();
        }

        // Log
        $this->log->log("領域を削除しました", "group1=$id");
    }

    /**
     * 1件取得
     * @param Integer $id
     * @return Array
     */
    public function find($id)
    {
        if (is_null($id)) {
            return array();
        }

        $sql = "SELECT guideline_group1 AS id, guideline_revision, name, form7, comment FROM ldr_guideline_group1 WHERE guideline_group1= :guideline_group1";
        $row = $this->db->extended->getRow(
            $sql, null, array('guideline_group1' => $id), array('integer'), MDB2_FETCHMODE_ASSOC
        );
        if (empty($row)) {
            throw new Exception();
        }
        return $row;
    }

    /**
     * 並べ替え
     * @param type $data
     * @throws Exception
     */
    public function sort($data)
    {
        $this->db->beginNestedTransaction();
        $sth = $this->db->prepare("UPDATE ldr_guideline_group1 SET order_no= ? WHERE guideline_group1= ?", array('integer', 'integer'), MDB2_PREPARE_MANIP);
        $cnt = 1;
        foreach ($data['order'] as $id) {
            $res = $sth->execute(array($cnt++, $id));
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " id={$id} ERROR: " . $res->getDebugInfo());
            }
        }
        $this->db->completeNestedTransaction();
        $sth->free();

        // Log
        $this->log->log("領域を並べ替えました", implode(" ", $data['order']));
    }

    /**
     * 検索して、無ければ作る
     * @param Array $data
     */
    public function find_or_create($name, $revision)
    {
        // 検索する
        list($id) = $this->db->extended->getRow('SELECT guideline_group1 FROM ldr_guideline_group1 WHERE guideline_revision=? AND name=?', array('integer', 'text'), array($revision, $name));
        if (PEAR::isError($id)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . ' ERROR: ' . $id->getDebugInfo());
        }
        if ($id) {
            return $id;
        }

        $this->db->beginNestedTransaction();

        // 無ければ作る
        $sth = $this->db->prepare("INSERT INTO ldr_guideline_group1 (guideline_revision, name) VALUES (?, ?)", array('integer', 'text'), MDB2_PREPARE_MANIP);
        $res = $sth->execute(array($revision, $name));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }
        $sth->free();
        $id = $this->db->lastInsertID('ldr_guideline_group1', 'guideline_group1');

        $this->db->completeNestedTransaction();

        // Log
        $this->log->log("領域を保存しました", "group1={$id} name={$name}");

        return $id;
    }

}
