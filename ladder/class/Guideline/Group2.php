<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'ladder/class/Validation.php';
require_once 'ladder/class/Log.php';

class Ldr_GuidelineGroup2 extends Model
{

    var $log;

    /**
     * コンストラクタ
     */
    public function Ldr_GuidelineGroup2()
    {
        parent::connectDB();
        $this->log = new Ldr_log('guideline');
    }

    /**
     * validate
     * @param Array $data
     * @return Array
     */
    public function validate($data)
    {
        $error = array();
        // 版
        if (Validation::is_empty($data['guideline_revision'])) {
            $error['guideline_revision'][] = '版が選択されていません';
        }
        else {
            $rv = new Ldr_GuidelineRevision();
            $row = $rv->find($data['guideline_revision']);
            if (empty($row)) {
                $error['guideline_revision'][] = '不正な版が選択されています';
            }
        }

        // 領域
        if (Validation::is_empty($data['guideline_group1'])) {
            $error['guideline_group1'][] = '領域が選択されていません';
        }
        else {
            $g1 = new Ldr_GuidelineGroup1();
            $fd = $g1->find($data['guideline_group1']);
            if (empty($fd)) {
                $error['guideline_group1'][] = '不正な領域が選択されています';
            }
        }

        // 分類名
        if (Validation::is_empty($data['name'])) {
            $error['name'][] = '分類名が入力されていません';
        }
        elseif (mb_strlen($data['name'], 'EUC-JP') > 200) {
            $error['name'][] = '分類名は200文字以内で入力してください';
        }

        return $error;
    }

    /**
     * 一覧の取得
     * @return Array
     */
    public function lists($cond)
    {
        if ($cond['guideline_revision']) {
            $condition .= ' AND g2.guideline_revision = :guideline_revision ';
        }
        else if ($cond['guideline_group1']) {
            $condition .= ' AND g2.guideline_group1 = :guideline_group1 ';
        }

        $sql = "
            SELECT
                guideline_group2,
                g2.name,
                g2.guideline_revision,
                g2.comment,
                guideline_group1,
                g1.name AS group1_name,
                EXISTS (SELECT * FROM ldr_guideline gl WHERE gl.guideline_group2 = g2.guideline_group2 AND NOT gl.del_flg) AS used
            FROM ldr_guideline_group2 g2
            JOIN ldr_guideline_group1 g1 USING (guideline_group1, guideline_revision)
            WHERE NOT g2.del_flg
            $condition
            ORDER BY g2.order_no, guideline_group2
        ";
        $sth = $this->db->prepare($sql, array('integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute($cond);
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }

        $lists = array();
        while ($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $row['comment'] = unserialize($row['comment']);
            $row['comment0'] = $row['comment'][0];
            $row['comment1'] = $row['comment'][1];
            $row['comment2'] = $row['comment'][2];
            $row['comment3'] = $row['comment'][3];
            $row['comment4'] = $row['comment'][4];
            $row['comment5'] = $row['comment'][5];
            $row['comment6'] = $row['comment'][6];
            $lists[] = $row;
        }
        $sth->free();
        return $lists;
    }

    /**
     * 保存
     * @param Array $data
     */
    public function save($data)
    {
        $this->db->beginNestedTransaction();

        $data['comment'] = serialize(array(
            0 => trim($data['comment0']),
            1 => trim($data['comment1']),
            2 => trim($data['comment2']),
            3 => trim($data['comment3']),
            4 => trim($data['comment4']),
            5 => trim($data['comment5']),
            6 => trim($data['comment6']),
        ));

        // INSERT
        if (empty($data['id'])) {
            $sth = $this->db->prepare("INSERT INTO ldr_guideline_group2 (guideline_revision, name, guideline_group1, comment) VALUES (:guideline_revision, :name, :guideline_group1, :comment)", array('integer', 'text', 'integer', 'text'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($data);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
            }
            $sth->free();
            $data['id'] = $this->db->lastInsertID('ldr_guideline_group2', 'guideline_group2');
        }

        // UPDATE
        else {
            $sth = $this->db->prepare("UPDATE ldr_guideline_group2 SET guideline_revision= :guideline_revision, name= :name, guideline_group1= :guideline_group1, comment= :comment WHERE guideline_group2= :id", array('integer', 'text', 'integer', 'text', 'integer'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($data);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " id={$data['id']} ERROR: " . $res->getDebugInfo());
            }
            $sth->free();
        }
        $this->db->completeNestedTransaction();

        // Log
        $this->log->log("分類を保存しました", "group2={$data['id']} group1={$data['guideline_group1']} name={$data['name']}");
    }

    /**
     * 削除
     * @param type $id
     * @throws Exception
     */
    public function delete($id)
    {
        $data = $this->find($id);
        if (isset($data)) {
            $this->db->beginNestedTransaction();
            $sth = $this->db->prepare("UPDATE ldr_guideline_group2 SET del_flg = true WHERE guideline_group2= ?", array('integer'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($id);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " gp2={$data['guideline_group2']} ERROR: " . $res->getDebugInfo());
            }
            $this->db->completeNestedTransaction();
            $sth->free();
        }

        // Log
        $this->log->log("分類を削除しました", "group2=$id");
    }

    /**
     * 1件取得
     * @param Integer $id
     * @return Array
     */
    public function find($id)
    {
        if (is_null($id)) {
            return array();
        }

        $sql = "SELECT guideline_group2 AS id, name, guideline_revision, guideline_group1, comment FROM ldr_guideline_group2 WHERE guideline_group2= :guideline_group2";
        $row = $this->db->extended->getRow(
            $sql, null, array('guideline_group2' => $id), array('integer'), MDB2_FETCHMODE_ASSOC
        );
        if (empty($row)) {
            throw new Exception();
        }

        $row['comment'] = unserialize($row['comment']);
        $row['comment0'] = $row['comment'][0];
        $row['comment1'] = $row['comment'][1];
        $row['comment2'] = $row['comment'][2];
        $row['comment3'] = $row['comment'][3];
        $row['comment4'] = $row['comment'][4];
        $row['comment5'] = $row['comment'][5];
        $row['comment6'] = $row['comment'][6];

        return $row;
    }

    /**
     * 並べ替え
     * @param type $data
     * @throws Exception
     */
    public function sort($data)
    {
        $this->db->beginNestedTransaction();
        $sth = $this->db->prepare("UPDATE ldr_guideline_group2 SET order_no= ? WHERE guideline_group2= ?", array('integer', 'integer'), MDB2_PREPARE_MANIP);
        $cnt = 1;
        foreach ($data['order'] as $id) {
            $res = $sth->execute(array($cnt++, $id));
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " id={$id} ERROR: " . $res->getDebugInfo());
            }
        }
        $this->db->completeNestedTransaction();
        $sth->free();

        // Log
        $this->log->log("分類を並べ替えました", implode(" ", $data['order']));
    }

    /**
     * 検索して、無ければ作る
     * @param Array $data
     */
    public function find_or_create($name, $revision, $group1)
    {
        // 検索する
        list($id) = $this->db->extended->getRow('SELECT guideline_group2 FROM ldr_guideline_group2 WHERE guideline_revision=? AND guideline_group1=? AND name=?', null, array($revision, $group1, $name));
        if (PEAR::isError($id)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . ' ERROR: ' . $id->getDebugInfo());
        }
        if ($id) {
            return $id;
        }

        $this->db->beginNestedTransaction();

        // 無ければ作る
        $sth = $this->db->prepare("INSERT INTO ldr_guideline_group2 (guideline_revision, guideline_group1, name) VALUES (?, ?, ?)", array('integer', 'integer', 'text'), MDB2_PREPARE_MANIP);
        $res = $sth->execute(array($revision, $group1, $name));
        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }
        $sth->free();
        $id = $this->db->lastInsertID('ldr_guideline_group2', 'guideline_group2');

        $this->db->completeNestedTransaction();

        // Log
        $this->log->log("領域を保存しました", "group1={$id} name={$name}");

        return $id;
    }

}
