<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'ladder/class/Log.php';

class Ldr_NextPlan extends Model
{
    var $emp_id;
    var $log;

    /**
     * コンストラクタ
     */
    public function Ldr_NextPlan($emp_id)
    {
        parent::connectDB();
        $this->emp_id = $emp_id;
        $this->log = new Ldr_log('next_plan');
    }

    /**
     * 職員の次年度計画取得
     * @param $emp_id, $year
     * @return Array
     */
    public function find($year)
    {
        $sql = "
            SELECT
                emp_id,
                year,
                role,
                special_skill,
                strengthen_skill,
                studying_skill,
                current_department,
                hospital_transfer,
                university_transfer,
                university_transfer_destination,
                etc,
                action,
                del_flg
            FROM ldr_next_plan
            WHERE
                NOT del_flg
                AND emp_id = :emp_id
                AND year = :year
        ";

        $sth = $this->db->prepare($sql, array('integer'), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array('emp_id' => $this->emp_id, 'year' => $year));
        if (PEAR::isError($res)) {
            throw new Exception(__METHOD__ . ' ERROR: ' . $res->getDebugInfo());
        }
        $list = $res->fetchRow(MDB2_FETCHMODE_ASSOC);

        $sth->free();
        return $list;
    }

    /**
     * validate_next_plan
     * @param type $data
     * @return string
     */
    public function validate_next_plan($data)
    {
        $error = array();
        // 年度
        if (Validation::is_empty($data['year'])) {
            $error['error'][] = '年度が指定されていません';
        }
        return $error;
    }

    /**
     * 次年度計画:保存
     * @param Array $data
     */
    public function save($data)
    {
        $data['emp_id'] = $this->emp_id;
        $data['university_transfer_destination'] = implode(",", $data['university_transfer_destination']);
        $sth = $this->db->prepare("SELECT update_ldr_next_plan(:emp_id, :year, :role, :special_skill, :strengthen_skill, :studying_skill, :current_department, :hospital_transfer, :university_transfer, :university_transfer_destination, :etc, :action)",
                            array('text', 'integer', 'text', 'text', 'text', 'text', 'text', 'text', 'text', 'text', 'text', 'text'), MDB2_PREPARE_MANIP);
        $res = $sth->execute($data);

        if (PEAR::isError($res)) {
            $this->db->failNestedTransaction();
            throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
        }

        $sth->free();
        $this->db->completeNestedTransaction();
        $this->log->log("次年度計画を保存しました", $this->emp_id);
    }

    public function year_lists()
    {
        $th_year = Ldr_Util::th_year();
        $sth = $this->db->prepare("SELECT year FROM ldr_next_plan WHERE NOT del_flg AND emp_id = :emp_id ORDER BY year DESC", array('text'), MDB2_PREPARE_RESULT);
        $res = $sth->execute(array('emp_id' => $this->emp_id));
        while($row = $res->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $years[] = $row['year'];
        }
        if (!in_array($th_year, $years)) {
            $years[] = Ldr_Util::th_year();
        }
        if (!in_array($th_year + 1, $years)) {
            $years[] = Ldr_Util::th_year() + 1;
        }
        rsort($years);
        return $years;
    }
}
