<?php

set_include_path('../');
ini_set('pcre.backtrack_limit', 10000000);

require_once 'Cmx.php';
require_once 'Cmx/View/Smarty.php';
require_once 'Cmx/Core/Utils.php';
require_once 'ladder/class/Const.php';
require_once 'ladder/class/Config.php';
require_once 'ladder/class/Session.php';
require_once 'ladder/class/Util.php';
require_once 'ladder/class/Log.php';
