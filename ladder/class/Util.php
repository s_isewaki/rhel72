<?php

require_once 'Cmx.php';
require_once 'ladder/class/Const.php';
require_once 'ladder/class/Config.php';

/**
 * キャリア開発ラダーUtilクラス
 */
class Ldr_Util
{

    /**
     * viewの共通情報を取得します
     * @param type $page              ページ識別子
     */
    public static function get_common_info($page, $option = null)
    {
        $sidebar = array(
            'apply' => 'apply',
            'form_1' => 'apply',
            'form_6' => 'apply',
            'form_7' => 'apply',
            'form_8' => 'apply',
            'form_9' => 'apply',
            'form_21' => 'record',
            'form_22' => 'record',
            'form_23' => 'record',
            'form_3' => 'record',
            'form_4' => 'record',
            'form_51' => 'record',
            'form_52' => 'record',
            'form_53' => 'record',
            'form_54' => 'record',
            'form_carrier' => 'record',
            'form_transfer' => 'record',
            'form_presentation' => 'record',
            'form_article' => 'record',
            'next_plan' => 'next_plan',
            'inside' => 'inside',
            'inside_course' => 'inside',
            'inside_calendar' => 'inside',
            'outside' => 'outside',
            'outside_course' => 'outside',
            'relief' => 'relief',
            'relief_course' => 'relief',
            'tapprove_inside' => 'tapprove',
            'tapprove_outside' => 'tapprove',
            'tapprove_relief' => 'tapprove',
            'taccept_inside' => 'taccept',
            'taccept_outside' => 'taccept',
            'taccept_relief' => 'taccept',
            'adm_config' => 'admin',
            'adm_employee' => 'admin',
            'adm_permission' => 'admin',
            'adm_analysis' => 'admin',
            'adm_log' => 'admin',
            'adm_guideline' => 'ldradmin',
            'adm_ladder' => 'ldradmin',
            'assessment' => 'assessment',
            'analysis_assessment' => 'analysis',
            'analysis_ladder' => 'analysis',
            'analysis_placement' => 'analysis',
            'adm_inside' => 'trnadmin',
            'adm_outside' => 'trnadmin',
            'adm_relief' => 'trnadmin',
            'check_template' => 'trnadmin',
            'adm_training' => 'trnadmin',
            'novice_assessment' => 'novice',
            'novice_facilitator' => 'novice',
            'novice_sheet' => 'novice',
            'novice_analysis' => 'novice',
            'adm_novice' => 'novadmin',
            'adm_novice_date' => 'novadmin',
            'adm_novice_facilitator' => 'novadmin',
            'adm_novice_guideline' => 'novadmin',
            'adm_novice_setting' => 'novadmin',
            'staff' => 'staff',
            'staff_levelup' => 'staff',
            'staff_certification' => 'staff',
            'adm_item_set' => 'item',
            'adm_item_mst' => 'item',
            'accept' => 'accept',
            'ladder_progress' => 'accept',
            'accept_objection' => 'accept',
        );

        $conf = new Ldr_Config();

        // 新人評価数字
        if ($conf->novice_number() === '1') {
            $nlv = array(0, 'I', 'II', 'III', 'IV');
        }
        else {
            $nlv = array(0, 1, 2, 3, 4);
        }

        return array(
            'referer_url' => Ldr_Util::get_referer_url($option['referer_url']),
            'move_pager' => '',
            'ldr_ticket' => Ldr_Util::make_ticket(),
            'roman_num' => Ldr_Const::get_roman_num(),
            'function' => $conf->functions(),
            'titles' => $conf->titles(),
            'items' => $conf->items(),
            'recordset' => $conf->recordset(),
            'hospital_type' => $conf->hospital_type(),
            "{$sidebar[$page]}_menu" => 'in',
            "{$page}_li" => 'active',
            "page_li" => $page,
            'nlv' => $nlv,
            'now' => date('Y-m-d'),
        );
    }

    /**
     * アップロードするファイルのチェック
     * @return string
     */
    public static function exist_upload_file()
    {
        $ret = array();
        if (array_key_exists("csvfile", $_FILES)) {
            if ($_FILES["csvfile"]["error"] !== 0 || $_FILES["csvfile"]["size"] <= 0) {
                $ret['error'] = true;
                $ret['msg'] = 'ファイルのアップロードに失敗しました';
            }
        }
        return $ret;
    }

    /**
     * 多重POST防止用ticketの作成
     * @return type
     */
    public static function make_ticket()
    {
        if (isset($_SESSION)) {
            // 多重POST判定用onetimepassの発行
            $ticket = md5(uniqid(rand(), true));
            $_SESSION['ldr_ticket'] = $ticket;
            unset($_POST['ldr_ticket']);
        }
        return $ticket;
    }

    /**
     * 戻るボタンのURLを取得する。
     * @return type           URL
     */
    public static function get_referer_url($url = null)
    {
        $referer_url = "dashboard.php";
        if (!empty($url)) {
            $referer_url = $url;
        }
        else if (isset($_SERVER['HTTP_REFERER'])) {

            $req_url = parse_url($_SERVER['REQUEST_URI']);
            $req_ary = explode("/", $req_url['path']);
            $req_tmp = end($req_ary);

            $ref_url = parse_url($_SERVER['HTTP_REFERER']);
            $ref_ary = explode("/", $ref_url['path']);
            $ref_tmp = end($ref_ary);

            if ($req_tmp === $ref_tmp) {
                if (isset($_POST['referer_url'])) {
                    $referer_url = $_POST['referer_url'];
                }
            }
            else {
                //$referer_url = $_SERVER['HTTP_REFERER'];
                $referer_url = $ref_tmp;
            }
        }
        $_POST['referer_url'] = null;
        return $referer_url;
    }

    /**
     * ファイルをEUCに変換
     * @param type $fileName
     * @return type
     */
    public static function file_to_euc($fileName)
    {
        setlocale(LC_ALL, 'ja_JP.EUC-JP');
        $fp = tmpfile();
        fwrite($fp, mb_convert_encoding(file_get_contents($fileName), 'eucjp-win', 'SJIS-Win'));
        rewind($fp);
        return $fp;
    }

    /**
     * ラダー権限チェック
     * @param type $emp
     */
    public static function qualify_ladder($emp)
    {
        if (!$emp->is_ladder()) {
            js_login_exit();
        }
    }

    /**
     * ラダー管理者権限チェック
     * @param type $emp
     */
    public static function qualify_admin($emp)
    {
        Ldr_Util::qualify_ladder($emp);

        if (!$emp->is_admin()) {
            header('Location: dashboard.php');
            exit;
        }
    }

    /**
     * 院内研修マスタ操作権限
     * @param type $emp
     */
    public static function qualify_mst_inside($emp)
    {
        Ldr_Util::qualify_ladder($emp);

        if (!$emp->is_admin() && !$emp->is_inside()) {
            header('Location: dashboard.php');
            exit;
        }
    }

    /**
     * 院外研修マスタ操作権限
     * @param type $emp
     */
    public static function qualify_mst_outside($emp)
    {
        Ldr_Util::qualify_ladder($emp);

        if (!$emp->is_admin() && !$emp->is_outside()) {
            header('Location: dashboard.php');
            exit;
        }
    }

    /**
     * 救護員研修マスタ操作権限
     * @param type $emp
     */
    public static function qualify_mst_relief($emp)
    {
        Ldr_Util::qualify_ladder($emp);

        if (!$emp->is_admin() && !$emp->is_relief()) {
            header('Location: dashboard.php');
            exit;
        }
    }

    /**
     * 分析権限チェック
     * 職員情報も同じ権限
     * @param type $emp
     */
    public static function qualify_analysis($emp)
    {
        Ldr_Util::qualify_ladder($emp);

        $conf = new Ldr_Config();
        $analysis_placement = $conf->analysis_placement();

        if ($analysis_placement !== 2) {
            if (!$emp->is_approver() && !$emp->is_analysis() && !$emp->committee() && !$emp->is_admin()) {
                header('Location: dashboard.php');
                exit;
            }
        }
        else {
            if (!$emp->is_analysis()) {
                header('Location: dashboard.php');
                exit;
            }
        }
    }

    /**
     * 認定委員会の権限チェック
     * @param type $emp
     */
    public static function qualify_committee($emp)
    {
        Ldr_Util::qualify_ladder($emp);

        if (!$emp->committee() && !$emp->is_admin()) {
            header('Location: dashboard.php');
            exit;
        }
    }

    /**
     * 救護員担当の権限チェック
     * @param type $emp
     */
    public static function qualify_relief($emp)
    {
        Ldr_Util::qualify_ladder($emp);

        if (!$emp->is_relief()) {
            header('Location: dashboard.php');
            exit;
        }
    }

    /**
     * 院外研修担当の権限チェック
     * @param type $emp
     */
    public static function qualify_outside($emp)
    {
        Ldr_Util::qualify_ladder($emp);

        if (!$emp->is_outside()) {
            header('Location: dashboard.php');
            exit;
        }
    }

    /**
     * 申請承認者担当の権限チェック
     * @param type $emp
     */
    public static function qualify_approver($emp)
    {
        Ldr_Util::qualify_ladder($emp);

        if (!$emp->is_approver()) {
            header('Location: dashboard.php');
            exit;
        }
    }

    /**
     * 申請承認者 & 管理者の権限チェック
     * @param type $emp
     */
    public static function qualify_approver_admin($emp)
    {
        Ldr_Util::qualify_ladder($emp);

        if (!$emp->is_approver() && !$emp->is_admin()) {
            header('Location: dashboard.php');
            exit;
        }
    }

    /**
     * 院内研修(企画担当)の権限チェック
     * @param type $emp
     */
    public static function qualify_inside($emp)
    {
        Ldr_Util::qualify_ladder($emp);

        if (!$emp->is_inside() && !$emp->is_inside_planner()) {
            header('Location: dashboard.php');
            exit;
        }
    }

    /**
     * 日付の差
     * @param type $date1
     * @param type $date2
     * @return type
     */
    public static function diff_day($date1, $date2)
    {
        $timestamp1 = strtotime($date1);
        $timestamp2 = strtotime($date2);

        $seconddiff = $timestamp2 - $timestamp1;
        $daydiff = floor($seconddiff / (60 * 60 * 24));

        return $daydiff;
    }

    /**
     * 年の差(年目)
     * ※ 同じ年なら１年目
     * @param type $date1
     * @param type $date2
     * @return type
     */
    public static function diff_th_year($date1, $date2)
    {
        $tmp1 = date("Ymd", strtotime($date1));
        $tmp2 = date("Ymd", strtotime($date2));
        $year = ceil(( ((int)$tmp2 + 1) - (int)$tmp1) / 10000);
        return $year;
    }

    /**
     * 年度の開始日を返却する
     * @return type
     */
    public static function start_th_year($year)
    {
        if (!is_numeric($year)) {
            return "";
        }
        return "$year-04-01";
    }

    /**
     * 年度の開始日を返却する
     * @return type
     */
    public static function end_th_year($year)
    {
        if (!is_numeric($year)) {
            return "";
        }
        $year++;
        return "$year-03-31";
    }

    /**
     * 現在の年度を取得
     * @return type
     */
    public static function th_year()
    {
        if (date('m') < 4) {
            return intval(date('Y')) - 1;
        }
        return date('Y');
    }

    /**
     * 部署名を'>'繋ぎで取得する
     * @param type $data
     * @param type $view_class
     * @return string
     */
    public static function class_full_name_sql($view_class)
    {
        $sql = "";
        if (empty($view_class) || $view_class === "1") {
            $sql .= "class_nm || ' > ' || atrb_nm || ' > ' || dept_nm";
        }
        else if ($view_class === "2") {
            $sql .= "atrb_nm || ' > ' || dept_nm";
        }
        else {
            $sql .= "dept_nm";
        }

        $sql.=" || CASE WHEN room_nm IS NOT NULL THEN ' > ' || room_nm ELSE '' END AS class_full_name";
        return $sql;
    }

    /**
     * 部署名を'>'繋ぎで取得する
     * @param type $data
     * @param type $view_class
     * @return string
     */
    public static function class_full_name($data, $view_class = null)
    {
        if (empty($view_class) || $view_class === "1") {
            $class_name .= $data['class_nm'] . " > " . $data['atrb_nm'] . " > ";
        }
        else if ($view_class <= "2") {
            $class_name .= $data['atrb_nm'] . " > ";
        }
        $class_name .= $data['dept_nm'];
        if (!empty($data['room_nm'])) {
            $class_name .= " > " . $data['room_nm'];
        }
        return $class_name;
    }

    /**
     * 評価者かどうか判定を行う
     * @param type $form1  form1情報
     * @param type $emp_id 対象の職員ID
     * @return boolean
     */
    public static function is_appraiser($form1, $emp_id)
    {
        foreach ($form1['apr_id'] as $value) {
            if ($value === $emp_id) {
                return true;
            }
        }
        return false;
    }

    /**
     * 締切日を取得
     * @param type $date
     * @return type
     */
    public static function outside_closing_date($date = null)
    {
        if (is_null($date)) {
            $date = date("Y-m");
        }
        $last_day = date("Y-m-t", strtotime("-2 month" . $date));
        $week = date("w", strtotime($last_day));
        if ($week === '3') {
            return $last_day;
        }
        else {
            return date("Y-m-d", strtotime("Wednesday last week " . $last_day));
        }
    }

    /**
     * 多重投稿防止の判定(チケットの使用)
     * @return boolean true:正常  false:POST済み
     */
    public static function use_tiket()
    {
        if (!isset($_SESSION) || $_SERVER["REQUEST_METHOD"] != "POST" || isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
            // チェック対象外
            $ret = true;
        }
        else if ($_SESSION['ldr_ticket'] === $_POST['ldr_ticket']) {
            unset($_SESSION['ldr_ticket']);
            $ret = true;
        }
        else {
            $ret = false;
            if (basename($_SERVER["HTTP_REFERER"]) === basename($_SERVER["PHP_SELF"])) {
                cmx_log(basename($_SERVER["PHP_SELF"]) . " multi post");
            }
        }

        return $ret;
    }

    /**
     * UTF8をEUCに変換
     * @param type $utf8 string or array
     * @return type
     */
    public static function from_utf8($utf8)
    {
        if (is_array($utf8)) {
            $ary = array();
            foreach ($utf8 as $key => $value) {
                $ary[$key] = from_utf8($value);
            }
            return $ary;
        }
        else {
            return from_utf8($utf8);
        }
    }

    /**
     * 院内研修の受付状態
     * @param type $data
     * @return int
     */
    public static function get_inside_reception($data)
    {

        $status = isset($data['d_status']) ? $data['d_status'] : $data['status'];
        $today = date('Y-m-d H:i:s');
        $trn_date = $data['training_date'] . " " . substr($data['training_last_time'], 0, 5);
        if ($data['type'] === '1' && $trn_date < $today) {
            // 開催終了
            return '4';
        }

        // 受付終了' (手動)
        if ($status === '0') {
            return '1';
        }
        // 準備中' (手動)
        if ($status === '2') {
            return '-2';
        }
        // 定員
        if ($data['type'] === '1' && isset($data['count']) && $data['overflow'] !== 't') {
            if ($data['count'] >= $data['maximum']) {
                return '2';
            }
        }

        // 期間
        if (substr($data['accept_date'], 0, 19) > $today) {
            // 準備中
            return '-1';
        }
        else if ($today > substr($data['deadline_date'], 0, 19)) {
            // 締切（期間）
            return '3';
        }

        // 受付
        return '0';
    }

    /**
     * 年月→年月日
     * @param type $data
     * @return string
     */
    public static function month_to_date($data)
    {
        if (empty($data)) {
            return $data;
        }
        $date = array();
        if (preg_match('/^(\d{4})(\d{2})(\d{2})$/', $data, $date) || preg_match('/^(\d+)[\-\.\/](\d+)[\-\.\/](\d+)$/', $data, $date)) {
            return "{$date[1]}-{$date[2]}-01";
        }
        else if (preg_match('/^(\d{4})(\d{2})$/', $data, $date) || preg_match('/^(\d+)[\-\.\/](\d+)$/', $data, $date)) {
            return "{$date[1]}-{$date[2]}-01";
        }

        return "";
    }

    /**
     * 年月日→年月
     * @param type $data
     * @return type
     */
    public static function date_to_month($data)
    {
        return empty($data) ? $data : date('Y-m', strtotime($data));
    }

    /**
     * array_map用:値をquoteで括って返す
     * @param type $s
     * @return type
     */
    public static function map_quote($s)
    {
        return sprintf("'%s'", pg_escape_string($s));
    }

    /**
     * セッション初期化
     * @param type $key
     */
    public static function session_init($key)
    {
        session_cache_limiter('');
        session_name($key);
        session_start();
    }

}
