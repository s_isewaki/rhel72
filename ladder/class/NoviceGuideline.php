<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'Cmx/Model/SystemConfig.php';
require_once 'ladder/class/NoviceGuideline/Guideline.php';
require_once 'ladder/class/NoviceGuideline/Revision.php';
require_once 'ladder/class/NoviceGuideline/Group1.php';
require_once 'ladder/class/NoviceGuideline/Group2.php';
require_once 'ladder/class/Log.php';

class Ldr_NoviceGuideline extends Model
{

    var $conf;
    var $gl;
    var $rv;
    var $g1;
    var $g2;
    var $log;

    /**
     * コンストラクタ
     */
    public function Ldr_NoviceGuideline()
    {
        $this->conf = new Cmx_SystemConfig();
        $this->gl = new Ldr_NoviceGuidelineGuideline();
        $this->rv = new Ldr_NoviceGuidelineRevision();
        $this->g1 = new Ldr_NoviceGuidelineGroup1();
        $this->g2 = new Ldr_NoviceGuidelineGroup2();
        $this->log = new Ldr_Log('novice_guideline');
    }

    /**
     * validate
     * @param type $type
     * @param type $data
     * @return type
     */
    public function validate($type, $data)
    {
        switch ($type) {
            case 'guideline':
                return $this->gl->validate($data);
                break;

            case 'revision':
                return $this->rv->validate($data);
                break;

            case 'group1':
                return $this->g1->validate($data);
                break;

            case 'group2':
                return $this->g2->validate($data);
                break;
        }
    }

    /**
     * 一覧の取得
     * @param type $type
     * @return type
     */
    public function lists($type, $data = null)
    {
        switch ($type) {
            case 'guideline':
                return $this->gl->lists($data);
                break;

            case 'revision':
                return $this->rv->lists();
                break;

            case 'group1':
                return $this->g1->lists($data);
                break;

            case 'group2':
                return $this->g2->lists($data);
                break;
        }
    }

    /**
     * 保存
     * @param type $type
     * @param type $data
     * @return type
     */
    public function save($type, $data)
    {
        switch ($type) {
            case 'guideline':
                return $this->gl->save($data);

            case 'revision':
                return $this->rv->save($data);

            case 'group1':
                return $this->g1->save($data);

            case 'group2':
                return $this->g2->save($data);
        }
    }

    /**
     * 削除
     * @param type $type
     * @param type $id
     * @return type
     */
    public function delete($type, $id)
    {
        switch ($type) {
            case 'guideline':
                return $this->gl->delete($id);
                break;

            case 'revision':
                return $this->rv->delete($id);
                break;

            case 'group1':
                return $this->g1->delete($id);
                break;

            case 'group2':
                return $this->g2->delete($id);
                break;
        }
    }

    /**
     * 1件取得
     * @param type $type
     * @param type $id
     * @return type
     */
    public function find($type, $id)
    {
        switch ($type) {
            case 'guideline':
                return $this->gl->find($id);
                break;

            case 'revision':
                return $this->rv->find($id);
                break;

            case 'group1':
                return $this->g1->find($id);
                break;

            case 'group2':
                return $this->g2->find($id);
                break;
        }
    }

    /**
     * 並べ替え
     * @param type $type
     * @param type $data
     * @return type
     */
    public function sort($type, $data)
    {
        switch ($type) {
            case 'guideline':
                return $this->gl->sort($data);
                break;

            case 'group1':
                return $this->g1->sort($data);
                break;

            case 'group2':
                return $this->g2->sort($data);
                break;

            default:
                return array();
                break;
        }
    }

    /**
     * 現在運用中の版数の取得、切り替え
     * @param type $current_revision
     * @return type
     */
    function current_revision($current_revision = null)
    {
        if (!empty($current_revision)) {
            $find = $this->rv->find($current_revision);
            if (!empty($find)) {
                $this->conf->set('ldr.novice_current_revision', $current_revision);

                // Log
                $this->log->log('版を切り替えました', "revision=$current_revision");

                return $current_revision;
            }
        }
        return $this->conf->get('ldr.novice_current_revision');
    }

}
