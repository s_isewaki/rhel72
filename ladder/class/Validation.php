<?php

/**
 * Validation
 */
class Validation
{

    /**
     * 日付のチェック
     * @param type $data     チェック対象データ
     */
    public static function is_date($data)
    {
        $date = array();
        if (preg_match('/^(\d{4})(\d{2})(\d{2})$/', $data, $date) or preg_match('/^(\d+)[\-\.\/](\d+)[\-\.\/](\d+)$/', $data, $date)) {
            return checkdate($date[2], $date[3], $date[1]);
        }
        return false;
    }

    /**
     * 年月のチェック
     * @param type $data
     * @return boolean
     */
    public static function is_month($data)
    {
        $date = array();
        if (preg_match('/^(\d{4})(\d{2})(\d{2})$/', $data, $date) or preg_match('/^(\d+)[\-\.\/](\d+)[\-\.\/](\d+)$/', $data, $date)) {
            return checkdate($date[2], $date[3], $date[1]);
        }
        else if (preg_match('/^(\d{4})(\d{2})$/', $data, $date) or preg_match('/^(\d+)[\-\.\/](\d+)$/', $data, $date)) {
            return checkdate($date[2], '01', $date[1]);
        }
        return false;
    }

    /**
     * 時間のチェック
     * @param type $data     チェック対象データ
     */
    public static function is_time($data)
    {
        $time = array();
        if (preg_match('/^(\d{2})(\d{2})$/', $data, $time) or preg_match('/^(\d+)[:\.](\d+)$/', $data, $time)) {
            if ($time[1] >= 0 and $time[1] <= 23 and $time[2] >= 0 and $time[2] <= 59) {
                return true;
            }
        }
        return false;
    }

    /**
     * 年のチェック
     * @param type $data     チェック対象データ
     */
    public static function is_year($data)
    {
        if (!is_numeric($data)) {
            return false;
        }
        $year = intval($data);
        if (1900 <= $year && $year <= 9999) {
            return true;
        }
        return false;
    }

    /**
     * 申請レベルのチェック
     * @param type $data     チェック対象データ
     */
    public static function is_level($data)
    {
        if (empty($data) && intval($data) !== 0) {
            return false;
        }
        $level = intval($data);
        if (0 <= $level and $level <= 6) {
            return true;
        }
        if ($level === 12 or $level === 123 or $level === 1234 or $level === 23 or $level === 234 or $level === 2345 or $level === 34 or $level === 345 or $level === 45) {
            return true;
        }
        return false;
    }

    /**
     * 未入力判定
     * @param type $data     チェック対象データ
     */
    public static function is_empty($data)
    {
        if (!isset($data)) {
            return true;
        }
        else if (gettype($data) !== 'string') {
            return false;
        }
        else if (str_replace('　', '', trim($data)) === '') {
            return true;
        }
        return false;
    }

}
