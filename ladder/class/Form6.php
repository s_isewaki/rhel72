<?php

require_once 'Cmx.php';
require_once 'Cmx/Core/Model.php';
require_once 'ladder/class/Validation.php';
require_once 'ladder/class/Apply.php';
require_once 'ladder/class/Log.php';

class Ldr_Form6 extends Model
{

    var $emp_id;
    var $log;

    /**
     * コンストラクタ
     * @param type $emp_id
     */
    public function Ldr_Form6($emp_id)
    {
        parent::connectDB();
        $this->emp_id = $emp_id;
        $this->log = new Ldr_log('form6');
    }

    /**
     * validate
     * @param type $data
     * @return string
     */
    public function validate($data)
    {
        $error = array();

        // 申請年月日
        if (Validation::is_empty($data['narrative'])) {
            $error['narrative'][] = '事例内容が入力されていません';
        }

        return $error;
    }

    /**
     * 1件取得
     * @param Integer $id
     * @return Array
     */
    public function find($id)
    {
        if (is_null($id)) {
            return array();
        }
        $sql = "SELECT ldr_apply_id as id, narrative, updated_on FROM ldr_form6 WHERE ldr_apply_id= :ldr_apply_id AND NOT del_flg ";
        $row = $this->db->extended->getRow(
            $sql, null, array('ldr_apply_id' => $id), array('integer'), MDB2_FETCHMODE_ASSOC
        );
        if (PEAR::isError($row)) {
            throw new Exception(__METHOD__ . " ERROR: " . $row->getDebugInfo());
        }
        return $row;
    }

    /**
     * 保存
     * @param Array $data
     */
    public function save($data, $mode)
    {
        $this->db->beginNestedTransaction();

        $row = $this->find($data['id']);

        // ajax経由のときは、UTF-8→EUC-JP変換をする
        if ($mode === 'auto') {
            $data['narrative'] = from_utf8($data['narrative']);
        }

        // INSERT
        if (empty($row)) {
            $sth = $this->db->prepare("INSERT INTO ldr_form6 (ldr_apply_id, narrative) VALUES (:id, :narrative) ", array('integer', 'text'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($data);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
            }
            $sth->free();
        }
        // UPDATE
        else {
            $updata = array_merge($row, $data);

            $sth = $this->db->prepare("UPDATE ldr_form6 SET narrative= :narrative WHERE ldr_apply_id= :id ", array('text', 'integer'), MDB2_PREPARE_MANIP);
            $res = $sth->execute($updata);
            if (PEAR::isError($res)) {
                $this->db->failNestedTransaction();
                throw new Exception(__METHOD__ . " ERROR: " . $res->getDebugInfo());
            }
            $sth->free();
        }
        $this->db->completeNestedTransaction();

        if ($mode !== 'auto') {
            $this->log->log("保存しました", "id={$data['id']}");
        }
    }

}
