<?php

require_once 'class/Base.php';
require_once 'class/AnalysisLadder.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('als_ladder');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_analysis($session->emp());

//----------------------------------------------------------
// オブジェクト
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');

//----------------------------------------------------------
// データ
//----------------------------------------------------------
$analysis = new Ldr_AnalysisLadder();
$cond = array_merge((array)$_SESSION['ANALYSYS_LADDER_POST'], (array)$_GET);
$lists = $analysis->emp_lists($cond);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
header('Cache-Control: no-cache, must-revalidate');
header("Cache-Control: post-check=0, pre-check=0", false);
header('Pragma: no-cache');
$view->assign('lists', $lists);
$view->display("modal/emp_list.tpl");
