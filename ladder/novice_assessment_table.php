<?php

require_once 'class/Base.php';
require_once 'class/NoviceAssessment.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}
$emp = $session->emp();

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_ladder($emp);

//----------------------------------------------------------
// オブジェクト
//----------------------------------------------------------
$as = new Ldr_NoviceAssessment($emp->emp_id());

//----------------------------------------------------------
// 版数
//----------------------------------------------------------
$current_revision = $as->current_revision();

//----------------------------------------------------------
// 集計
//----------------------------------------------------------
$table = $as->table($_GET['year']);
$date_list = $as->date_lists($_GET['year']);
$group1_list = $as->lists('group1', array('revision_id' => $current_revision));
foreach ($group1_list as $g1) {
    $group2_list[$g1['group1']] = $as->lists('group2', array('revision_id' => $current_revision, 'group1' => $g1['group1']));
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
header('Cache-Control: no-cache, must-revalidate');
header("Cache-Control: post-check=0, pre-check=0", false);
header('Pragma: no-cache');

$view = new Cmx_View('ladder/templates');
$view->assign('date', array_reverse($date_list));
$view->assign('group1', $group1_list);
$view->assign('group2', $group2_list);
$view->assign('data', $table);
$view->assign(Ldr_Util::get_common_info());
$view->display("novice_assessment_table.tpl");
