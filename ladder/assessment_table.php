<?php

require_once 'class/Base.php';
require_once 'class/Assessment.php';
require_once 'class/Ladder.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}
$emp = $session->emp();

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_ladder($emp);

//----------------------------------------------------------
// オブジェクト
//----------------------------------------------------------
$as = new Ldr_Assessment($emp->emp_id());
$view = new Cmx_View('ladder/templates');
$ladder = new Ldr_Ladder();

//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('assessment');

//----------------------------------------------------------
// ladder
//----------------------------------------------------------
$ladder_id = $_SESSION['ladder_id'];
$this_ladder = $ladder->find($ladder_id);
$revision = $this_ladder['guideline_revision'];
$assessment_select = $this_ladder['config']['select'];

//----------------------------------------------------------
// 集計
//----------------------------------------------------------
$list = $as->table($_GET['year'], $revision);
$table_label = $as->table_label($revision);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
header('Cache-Control: no-cache, must-revalidate');
header("Cache-Control: post-check=0, pre-check=0", false);
header('Pragma: no-cache');

$view->assign("roman_num", Ldr_Const::get_roman_num());
$view->assign("list", $list);
$view->assign("ldr", $this_ladder);
$view->assign("label", $table_label);
$view->display("assessment_table.tpl");
