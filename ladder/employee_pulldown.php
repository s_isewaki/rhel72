<?php

require_once 'class/Base.php';
require_once 'class/Classmst.php';
require_once 'class/Jobmst.php';
require_once 'class/Statusmst.php';

//----------------------------------------------------------
// オブジェクト
//----------------------------------------------------------
$class = new Ldr_Classmst();
$job = new Ldr_Jobmst();
$status = new Ldr_Statusmst();

//----------------------------------------------------------
// データ
//----------------------------------------------------------
switch ($_POST['mode']) {

    case 'class12':
        $list = $class->class12_lists();
        break;

    case 'class34':
        $list = $class->class34_lists($_POST['id']);
        break;

    case 'job':
        $list = $job->lists();
        break;

    case 'status':
        $list = $status->lists();
        break;

    default:
        js_error_exit();
        break;
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
header('Cache-Control: no-cache, must-revalidate');
header("Cache-Control: post-check=0, pre-check=0", false);
header('Pragma: no-cache');
print cmx_json_encode($list);
