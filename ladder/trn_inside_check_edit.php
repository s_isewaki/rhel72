<?php

require_once 'class/Base.php';
require_once 'class/Pager.php';
require_once 'class/Inside.php';
require_once 'class/InsideMstCheck.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}
//----------------------------------------------------------
// PHPセッション
//----------------------------------------------------------
Ldr_Util::session_init('inside_apply');

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_ladder($emp);

//----------------------------------------------------------
// object
//----------------------------------------------------------
$inside = new Ldr_Inside($emp->emp_id());
$effective_post = Ldr_Util::use_tiket();

//----------------------------------------------------------
// データの取得
//----------------------------------------------------------
$dt = $inside->find($_POST['id']);

//----------------------------------------------------------
// 一時保存
//----------------------------------------------------------
if ($_POST['btn_save'] === 'btn_save') {
    try {
        if ($effective_post) {
            $_POST['status'] = 0;
            $inside->save_check($_POST);
        }
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }
    $_SESSION['ldr_return'] = true;
    header('Location:' . Ldr_Util::get_referer_url());
    exit;
}

//----------------------------------------------------------
// 確定
//----------------------------------------------------------
else if ($_POST['btn_save'] === 'btn_completed') {
    // バリデーション
    $error = $inside->validate_check($dt['form'], $_POST);

    // エラーが無ければ保存
    if (empty($error)) {
        try {
            if ($effective_post) {
                $_POST['status'] = 1;
                $inside->save_check($_POST);
            }
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
        }
        //unset($_SESSION['ldr_return']); //確定の場合は1ページ目に戻す
        $_SESSION['ldr_return'] = true;
        header('Location:' . Ldr_Util::get_referer_url());
        exit;
    }
}

//----------------------------------------------------------
// VIEWデータ
//----------------------------------------------------------
if (empty($error)) {
    $fdat = array(
        'id' => $_POST['id'],
        'title' => "{$dt['title']} 第{$dt['number']}回 / 全{$dt['number_of']}回",
        'training_id' => $dt['training_id'],
        'training_date' => $dt['training_date'],
        'type' => $dt['type'],
        'check_status' => $dt['check_status'],
    );
}

$data = array(
    'trn' => $dt,
    'hd_title' => '院内研修 振り返り',
);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $session->emp());
$view->assign($dt);
$view->assign($data);
$view->assign(Ldr_Util::get_common_info('inside'));

if (empty($error)) {
    $view->display_with_fill('trn_inside_check_edit.tpl', array('fdat' => $fdat));
}
else {
    $view->assign('error', $error);
    $view->assign('form_data', $_POST['form']);
    $view->display_with_fill('trn_inside_check_edit.tpl', array('fdat' => $_POST));
}
