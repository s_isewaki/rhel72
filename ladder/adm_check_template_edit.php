<?php

require_once 'class/Base.php';
require_once 'class/CheckTemplate.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_mst_inside($session->emp());

//----------------------------------------------------------
// Obj
//----------------------------------------------------------
$check = new Ldr_CheckTemplate();

//----------------------------------------------------------
// 保存
//----------------------------------------------------------
if (isset($_POST['btn_save'])) {
    // バリデーション
    $error = $check->validate($_POST);

    // エラーが無ければ保存
    if (empty($error)) {
        try {
            $check->save($_POST);
        }
        catch (Exception $e) {
            cmx_log($e->getMessage());
            js_error_exit();
        }

        header('Location:' . 'adm_check_template.php');
        exit;
    }
}

//----------------------------------------------------------
// データの取得
//----------------------------------------------------------
if ($_POST['mode'] === 'edit') {
    $data = $check->find($_POST['id']);
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $session->emp());
$view->assign(Ldr_Util::get_common_info('check_template'));

if ($error) {
    $view->assign('error', $error);
    $view->assign('form', $_POST['form']);
    $view->display_with_fill('adm_check_template_edit.tpl', array('fdat' => $_POST));
}
else {
    $view->assign('form', $data['form']);
    $view->display_with_fill('adm_check_template_edit.tpl', array('fdat' => $data));
}