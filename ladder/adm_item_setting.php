<?php

require_once 'class/Base.php';
require_once 'class/Item.php';
require_once 'class/Config.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_committee($session->emp());

//----------------------------------------------------------
// オブジェクト
//----------------------------------------------------------
$item = new Ldr_Item();
$conf = new Ldr_Config();
$effective_post = Ldr_Util::use_tiket();

//----------------------------------------------------------
// 保存
//----------------------------------------------------------
if ($effective_post && isset($_POST['btn_save'])) {
    try {
        $conf->save_items($_POST);
        $completed = true;
    }
    catch (Exception $e) {
        cmx_log($e->getMessage());
        js_error_exit();
    }
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $session->emp());
$view->assign(Ldr_Util::get_common_info('adm_item_set'));
$view->assign('item', $item);
$view->assign('completed', $completed);
$view->display('adm_item_setting.tpl');
