<?php

require_once 'class/Base.php';
require_once 'class/CheckTemplate.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
Ldr_Util::qualify_mst_inside($session->emp());

//----------------------------------------------------------
// Obj
//----------------------------------------------------------
$check = new Ldr_CheckTemplate();

//----------------------------------------------------------
// データの取得
//----------------------------------------------------------
$data = $check->find($_POST['id']);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View('ladder/templates');
$view->assign('emp', $session->emp());
$view->assign(Ldr_Util::get_common_info('check_template'));

$view->assign($data);
$view->display('adm_check_template_preview.tpl');
