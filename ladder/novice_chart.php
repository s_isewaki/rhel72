<?php

require_once 'class/Base.php';
require_once 'class/Novice.php';
require_once 'class/NoviceAssessment.php';
require_once "libs/pChart2/class/pData.class.php";
require_once "libs/pChart2/class/pDraw.class.php";
require_once "libs/pChart2/class/pRadar.class.php";
require_once "libs/pChart2/class/pImage.class.php";

//----------------------------------------------------------
// get
//----------------------------------------------------------
$emp_id = $_GET['emp_id'];
$year = $_GET['year'];
$number = $_GET['number'];

//----------------------------------------------------------
// font
//----------------------------------------------------------
if (file_exists('/usr/share/fonts/ipa-gothic/ipag.ttf')) {
    $font = "/usr/share/fonts/ipa-gothic/ipag.ttf";
}
else if (file_exists('/usr/share/fonts/ipa-pgothic/ipagp.ttf')) {
    $font = "/usr/share/fonts/ipa-pgothic/ipagp.ttf";
}
else if (file_exists('/usr/share/fonts/japanese/TrueType/sazanami-gothic.ttf')) {
    $font = "/usr/share/fonts/japanese/TrueType/sazanami-gothic.ttf";
}
else if (file_exists('/usr/share/fonts/ja/TrueType/kochi-gothic-subst.ttf')) {
    $font = "/usr/share/fonts/ja/TrueType/kochi-gothic-subst.ttf";
}

//----------------------------------------------------------
// オブジェクト
//----------------------------------------------------------
$novice = new Ldr_Novice($emp_id);
$as = new Ldr_NoviceAssessment($emp_id);
$conf = new Ldr_Config();

//----------------------------------------------------------
// 版数
//----------------------------------------------------------
$current_revision = $as->current_revision();

//----------------------------------------------------------
// 一覧
//----------------------------------------------------------
$group1_list = $as->lists('group1', array('revision_id' => $current_revision));
foreach ($group1_list as $g1) {
    $group2_list[$g1['group1']] = $as->lists('group2', array('revision_id' => $current_revision, 'group1' => $g1['group1']));
}

//----------------------------------------------------------
// date
//----------------------------------------------------------
$date_list = $novice->date_lists($year);

//----------------------------------------------------------
// assessment
//----------------------------------------------------------
$assessment = $as->full_assessment($emp_id, $year);

//----------------------------------------------------------
// pData
//----------------------------------------------------------
$data = array();
$label = array();
$MyData = new pData();
foreach ($group1_list as $g1) {
    foreach ($group2_list[$g1['group1']] as $g2) {
        $label[] = to_utf8(mb_substr($g2['name'], 0, 10, 'EUC-JP'));

        $a = $f = 0;
        foreach ($assessment[$g1['group1']][$g2['group2']] as $asm) {
            $a += $asm[$number]['assessment'] ? 5 - $asm[$number]['assessment'] : '';
            $f += $asm[$number]['f_assessment'] ? 5 - $asm[$number]['f_assessment'] : '';
        }
        $data['a'][] = $a / ($g2['count'] * 4) * 100;
        $data['f'][] = $f / ($g2['count'] * 4) * 100;
    }
}

$MyData->addPoints($data['a'], "a{$number}");
$MyData->addPoints($data['f'], "f{$number}");
$MyData->setSerieDescription("a{$number}", to_utf8("{$number}回 自己"));
$MyData->setSerieDescription("f{$number}", to_utf8("{$number}回 他者"));

$MyData->addPoints($label, "Labels");
$MyData->setAbscissa("Labels");

$myPicture = new pImage(950, 500, $MyData);
if (!$_GET['antialias']) {
    $myPicture->Antialias = FALSE;
}

$Settings = array("StartR" => 255, "StartG" => 255, "StartB" => 255, "EndR" => 255, "EndG" => 255, "EndB" => 255, "Alpha" => 255);
$myPicture->drawGradientArea(10, 10, 940, 490, DIRECTION_VERTICAL, $Settings);

$myPicture->setFontProperties(array("FontName" => $font, "FontSize" => 8));

$SplitChart = new pRadar();

$myPicture->setGraphArea(10, 10, 940, 490);
$SplitChart->drawRadar($myPicture, $MyData, array("DrawPoly" => TRUE, "LabelPos" => RADAR_LABELS_HORIZONTAL, "Segments" => 10, "FixedMax" => 100, "AxisRotation" => -90));

$myPicture->setFontProperties(array("FontName" => $font, "FontSize" => 9));
$myPicture->drawLegend(20, 20, array("Style" => LEGEND_BOX, "Mode" => LEGEND_VERTICAL));

$myPicture->Stroke();
