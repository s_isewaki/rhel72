<?php

require_once 'class/Base.php';
require_once 'class/Apply.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Ldr_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
Ldr_Util::qualify_committee($emp);

//----------------------------------------------------------
// IDなければダッシュボード
//----------------------------------------------------------
if (empty($_GET['id'])) {
    header("Location: dashboard.php");
    exit;
}

//----------------------------------------------------------
// Object
//----------------------------------------------------------
$apply = new Ldr_Apply(array(
    'apply_emp_id' => $emp->emp_id(),
    'apply_id' => $_GET['id'],
    )
);

//----------------------------------------------------------
// 取り下げ復旧
//----------------------------------------------------------
try {
    $apply->withdraw_recover();
}
catch (Exception $e) {
    cmx_log($e->getMessage());
    js_error_exit();
}

header('Location:' . Ldr_Util::get_referer_url());
exit;
