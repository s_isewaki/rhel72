<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix メドレポート｜障害名選択</title>
<?
require_once("about_comedix.php");

$fname = $PHP_SELF;

$session = qualify_session($session, $fname);
if ($session == "0") {
    js_login_exit();
}

$checkauth = check_authority($session, 57, $fname);
if ($checkauth == "0") {
    js_login_exit();
}

$con = connect2db($fname);

$code1 = isset($_POST['code1']) ? $_POST['code1'] : '';

// 分類一覧を取得
$classifications = getDisorderClassifications($con, $fname);
if ($classifications === false) {
    pg_close($con);
    js_error_exit();
}

// 選択された分類(初期値は番号が一番小さいもの)に該当する障害名を取得
if (!$code1) {
    if ($classifications) {
        $code1 = $classifications[0]['code1'];
    }
}

$disorderNames = array();
if ($code1) {
    $disorderNames = getDisorderNames($con, $code1, $fname);
    if ($classifications === false) {
        pg_close($con);
        js_error_exit();
    }
}

pg_close($con);

/**
 * 分類一覧を取得
 * 
 * @param resource $con   データベース接続リソース
 * @param string   $fname ページ名
 * 
 * @return mixed 成功時:障害名一覧/失敗時:false
 */
function getDisorderClassifications($con, $fname) {
    $sql = "select * from sum_med_disorder1_mst";
    $cond = "where is_disabled = 0 order by sort_order";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        return false;
    }
    
    $data = pg_fetch_all($sel);
    if (!$data) {
        $data = array();
    }
    return $data;
}

/**
 * 障害名一覧を取得
 * 
 * @param resource $con   データベース接続リソース
 * @param string   $code1 分類コード
 * @param string   $fname ページ名
 * 
 * @return mixed 成功時:障害名一覧/失敗時:false
 */
function getDisorderNames($con, $code1, $fname) {
    $sql = "select * from sum_med_disorder2_mst";
    $cond = q("where code1 = '%s' and is_disabled = 0 order by sort_order", $code1);
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        return false;
    }
    
    $data = pg_fetch_all($sel);
    if (!$data) {
        $data = array();
    }
    return $data;
}
?>
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<script type="text/javascript" src="js/jquery/jquery-1.10.1.min.js"></script>
<script type="text/javascript" src="js/swapimg.js"></script>
<script type="text/javascript">
// 分類名選択時の処理
function selectClassification() {
 document.mainform.submit();
}

// 障害名選択時の処理
function selectDisorder(disorder) {
    if (opener && !opener.closed && opener.selectDisorder) {
        opener.selectDisorder(disorder);
    }
}

// セルハイライトON
function highlightCells(tr) {
    changeCellsColor(tr, '#fefc8f');
}

// セルハイライトOFF
function dehighlightCells(tr) {
    changeCellsColor(tr, '');
}

// セルの色を設定
function changeCellsColor(tr, color) {
    tr.style.backgroundColor = color;
}
</script>
</head>
<body style="background-color:#f4f4f4">
<div style="padding:4px">
<div>
  <table class="dialog_titletable"><tr>
    <th>障害名選択</th>
      <td><a href="javascript:window.close()"><img src="img/icon/close.gif" alt="閉じる"
        onmouseover="swapimg(this,'img/icon/closeo.gif');" onmouseout="swapimg(this,'img/icon/close.gif');" /></a></td>
  </tr></table>
</div>

<form name="mainform" action="summary_disorder_select.php" method="post">
<input type="hidden" name="session" value="<? echo $session; ?>">
<div style="margin-top: 10px; margin-bottom: 5px;">
分類
<select name="code1" onchange="selectClassification();">
<?
foreach ($classifications as $value) {
    $selected = ($value['code1'] == $code1) ? ' selected' : '';
    echo "<option value=\"{$value['code1']}\"{$selected}>" . h($value['caption']) . "</option>\n";
}
?>
</select>
</div>

<table class="prop_table" cellspacing="1">
<tr>
<th>障害名</th>
</tr>
<?
$line_script = 'onmouseover="highlightCells(this.parentNode);" onmouseout="dehighlightCells(this.parentNode);"';
foreach ($disorderNames as $value) {
    echo "<tr onclick=\"selectDisorder('" . h_jsparam($value['caption']) . "');\">\n";
    echo "<td style=\"cursor: pointer;\" $line_script>" . h($value['caption']) . "</td>\n";
    echo "</tr>\n";
}
?>
</table>
</form>
</div>
</body>
</html>
