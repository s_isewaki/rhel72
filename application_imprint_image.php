<?php

/*
 * 印影画像出力
 * パラメータ
 *  $session
 *  $emp_id
 *  $apv_flg 1:承認 2:否認 3:差戻し
 *  $imprint_flg 't':印影機能を利用する 'f','':印影機能を利用しない。既存の承認画像を表示。　※apv_flg=1の時、判定に使用
 *  $t ダミー※operaで画像再登録しても、キャッシュが利用されるらしく
 *             画像が変わらないためURLを変化させるため必要。呼出側で年月日時分秒を指定している。
 *  $approve_label 承認｜確認
 */
require_once("about_session.php");

//ページ名
$fname = $_SERVER['PHP_SELF'];

//セッションチェック
$session = qualify_session($_REQUEST['session'], $fname);
if ($session == "0") {
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script type='text/javascript'>showLoginPage(window);</script>");
    exit;
}

$emp_id = $_REQUEST['emp_id'];
$apv_flg = $_REQUEST['apv_flg'];
$imprint_flg = $_REQUEST['imprint_flg'];
$approve_label = $_REQUEST['approve_label'];

$dirname = "workflow/imprint/";

$img_file = "img/spacer.gif";

switch ($apv_flg) {
    case 1:
        if ($imprint_flg == "t") {
            $img_file1 = "$dirname$emp_id.gif";
            $img_file2 = "$dirname$emp_id.jpg";
            // 当該職員の画像が登録済みであれば出力
            if (is_file($img_file1)) {
                $img_file = $img_file1;
            }
            else if (is_file($img_file2)) {
                $img_file = $img_file2;
            }
        } else {
            $img_file = ($approve_label == "確認") ? "img/confirmed.gif" : "img/approved.gif";
        }

        break;
    case 2:
        $img_file = "img/approve_ng.gif";

        break;
    case 3:
        $img_file = "img/returned.gif";

        break;
}

// 運用ディレクトリ名を取得（末尾にスラッシュあり）
$scheme = ($_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
$dir = str_replace($_SERVER["DOCUMENT_ROOT"], "", $_SERVER["SCRIPT_FILENAME"]);
$dir2 = str_replace("application_imprint_image.php", "", $dir);
$url_base = $scheme . $_SERVER['HTTP_HOST'] . $dir2;

if (is_file($img_file)) {
    header("Location: {$url_base}{$img_file}?" . time());
    exit;
}

/// 未登録のため空白画像を出力する
header("Location: {$url_base}img/spacer.gif");
