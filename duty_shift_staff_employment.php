<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務シフト作成｜個人勤務条件設定</title>

<?
//ini_set("display_errors","1");
require_once("about_session.php");
require_once("show_class_name.ini");

require_once("duty_shift_common.ini");
require_once("duty_shift_user_tab_common.ini");
require_once("duty_shift_common_class.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
// セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
///-----------------------------------------------------------------------------
// 権限のチェック
///-----------------------------------------------------------------------------
//ユーザ画面用
$chk_flg = $obj->check_authority_user($session, $fname);
if ($chk_flg == "") {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
//管理画面用
$section_admin_auth = $obj->check_authority_Management($session, $fname);
///-----------------------------------------------------------------------------
//初期処理
///-----------------------------------------------------------------------------
	///-----------------------------------------------------------------------------
	// 勤務パターン情報を取得
	///-----------------------------------------------------------------------------
	$wktmgrp_array = $obj->get_wktmgrp_array();
	if (count($wktmgrp_array) <= 0) {
		$err_msg_1 = "勤務パターン情報(wktmgrp)が未設定です。管理者に連絡してください";
	}
	///-----------------------------------------------------------------------------
	//ＤＢ(empmst)より職員情報を取得
	///-----------------------------------------------------------------------------
	$staff_name = "";
	$data_emp = $obj->get_empmst_array($staff_id);
	if (count($data_emp) > 0) {
		$staff_name = $data_emp[0]["name"];
	}
	///-----------------------------------------------------------------------------
	//勤務シフトグループ情報取得
	///-----------------------------------------------------------------------------
	$wk_group = $obj->get_duty_shift_group_array($group_id, $data_emp, $wktmgrp_array);
	$pattern_id = "";
	if (count($wk_group) > 0) {
		$pattern_id = $wk_group[0]["pattern_id"];
	}
	///-----------------------------------------------------------------------------
	//ＤＢ(atdptn)より役職情報取得
	///-----------------------------------------------------------------------------
//	$data_atdptn = $obj->get_atdptn_array($pattern_id);
	$atdptn_array = $obj->get_atdptn_array($pattern_id);										// 識別コードを４桁に変更 20140125 START
	$data_atdptn = $obj->get_duty_shift_pattern_array($pattern_id, $atdptn_array);				// 識別コードを４桁に変更 20140125 END

//	$atdptn_cnt = count($data_atdptn);															// 出勤表の休暇種別等画面の非表示を確認 20140125 START
	$atdptn_cnt = 0;

	// 表示内容がオブジェクトによって異なる
	// ※勤務可能なシフト欄の個数を数える時は休暇をひとまとめとする
	// ※カウント処理と同時に非表示設定のメンテナンスを行う
	//   →非表示フラグ又は表示記号なしなら非表示とする

	// 休日をまとめるフラグ
	$hol_flg = false;

	for ($i=0; $i<count($data_atdptn);$i++) {
		// 非表示フラグメンテナンス 20140130 UPDATE
		if(($data_atdptn[$i]["reason_display_flag"] == "f") ||
			($data_atdptn[$i]["display_flag"] == "f") ||
			($data_atdptn[$i]["stamp_disp_flg"] == "f") ||
			($data_atdptn[$i]["font_name"] == "")) {
			$data_atdptn[$i]["reason_display_flag"] = "f";
		}

		if($data_atdptn[$i]["reason_display_flag"] == "f") {
			continue;
		}
		if($data_atdptn[$i]["atdptn_ptn_id"]==10) {
			if($hol_flg == false) {
				$atdptn_cnt += 1;
				$hol_flg = true;			// 休日を１回しか数えないため
			}
		} else {
			$atdptn_cnt += 1;
		}
	}																							// 出勤表の休暇種別等画面の非表示を確認 END
	///-----------------------------------------------------------------------------
	//勤務シフトスタッフ情報取得（個別勤務条件）
	///-----------------------------------------------------------------------------
	$staff_data = $obj->get_duty_shift_staff_employmen($staff_id);
	if (count($staff_data) > 0) {
		//データ設定
		$wage = $staff_data["wage"];						//支給区分
		$tmcd_group_id = $staff_data["tmcd_group_id"];		//出勤グループＩＤ
		$duty_form = $staff_data["duty_form"];				//勤務形態（1:常勤、2:非常勤）
		$other_post_flg = $staff_data["other_post_flg"];	//他部署兼務（１：有り）

		$duty_mon_day_flg = $staff_data["duty_mon_day_flg"];		//勤務日（月曜日）（１：有り）
		$duty_tue_day_flg = $staff_data["duty_tue_day_flg"];		//勤務日（火曜日）（１：有り）
		$duty_wed_day_flg = $staff_data["duty_wed_day_flg"];		//勤務日（水曜日）（１：有り）
		$duty_thurs_day_flg = $staff_data["duty_thurs_day_flg"];	//勤務日（木曜日）（１：有り）
		$duty_fri_day_flg = $staff_data["duty_fri_day_flg"];		//勤務日（金曜日）（１：有り）
		$duty_sat_day_flg = $staff_data["duty_sat_day_flg"];		//勤務日（土曜日）（１：有り）
		$duty_sun_day_flg = $staff_data["duty_sun_day_flg"];		//勤務日（日曜日）（１：有り）
		$duty_hol_day_flg = $staff_data["duty_hol_day_flg"];		//勤務日（祝日）（１：有り）

		$night_duty_flg = $staff_data["night_duty_flg"];	//夜勤の有無（１：有り、２：夜専）
		$night_staff_cnt = $staff_data["night_staff_cnt"];	//夜勤従事者
	}
//勤務形態にデフォルト設定を行わない ※一度も更新されていない場合画面上でどちらにもチェックをつけずに表示 20100729
	//if ($duty_form == "") {
	//	$duty_form = "1";
	//}


	///-----------------------------------------------------------------------------
	/// 勤務シフト上限日数、JavaScriptでテーブル展開する
	///-----------------------------------------------------------------------------
	$limit_array = array();
	$sql = "select emp_id,atdptn_id,limit_day_count from emp_shift_limit";
	$cond = "where emp_id = '$staff_id' ";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$num = pg_numrows($sel);
	for($i=0; $i<$num; $i++){
		$limit_array[$i]["emp_id"] = pg_result($sel,$i,"emp_id");
		$limit_array[$i]["atdptn_id"] = pg_result($sel,$i,"atdptn_id");
		$limit_array[$i]["limit_day_count"] = pg_result($sel,$i,"limit_day_count");
	}

	// 勤務パターンの背景色とフォント色を得る
/*
	$color_array = array();
	$sql  = "SELECT font_color_id,back_color_id FROM duty_shift_pattern ";
	foreach($data_atdptn as $num=>$v1){
		$wk_pattern_id = $v1['atdptn_ptn_id'];

		$wk_reason = $v1['reason'];					// 休暇種別を展開する 20140130

		$cond = "WHERE atdptn_ptn_id=$wk_pattern_id AND pattern_id=$pattern_id ";

		// 休暇種別を展開する START 20140130
		if($v1['reason'] != "") {
			$cond .= "AND reason='$wk_reason'";
		}
		// 休暇種別を展開する END

		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$color_array[$wk_pattern_id]['font_color'] = pg_result($sel,0,"font_color_id");
		$color_array[$wk_pattern_id]['back_color'] = pg_result($sel,0,"back_color_id");
	}
*/

	// // 識別コードを４桁に変更 20140125
//	$sql  = "SELECT atdptn_1,atdptn_2,atdptn_3,atdptn_4,atdptn_5,atdptn_6,atdptn_0,atdptn_7 FROM duty_shift_staff_fixed_atdptn ";
	$sql = "select ";
	$sql .= "atdptn_1,atdptn_2,atdptn_3,atdptn_4,atdptn_5,atdptn_6,atdptn_0,atdptn_7,";
	$sql .= "reason_1,reason_2,reason_3,reason_4,reason_5,reason_6,reason_0,reason_7 ";
	$sql .= "from duty_shift_staff_fixed_atdptn ";

	$cond = "WHERE emp_id='$staff_id' ";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	// 識別コードを４桁に変更 20140125 START
//	$check_week1 = pg_result($sel,0,"atdptn_1");
//	$check_week2 = pg_result($sel,0,"atdptn_2");
//	$check_week3 = pg_result($sel,0,"atdptn_3");
//	$check_week4 = pg_result($sel,0,"atdptn_4");
//	$check_week5 = pg_result($sel,0,"atdptn_5");
//	$check_week6 = pg_result($sel,0,"atdptn_6");
//	$check_week0 = pg_result($sel,0,"atdptn_0");
//	$check_week7 = pg_result($sel,0,"atdptn_7");

	// 日曜日をゼロとする（祝日は７）
	$check_week1 = convert_id( pg_result($sel,0,"atdptn_1"), pg_result($sel,0,"reason_1") );
	$check_week2 = convert_id( pg_result($sel,0,"atdptn_2"), pg_result($sel,0,"reason_2") );
	$check_week3 = convert_id( pg_result($sel,0,"atdptn_3"), pg_result($sel,0,"reason_3") );
	$check_week4 = convert_id( pg_result($sel,0,"atdptn_4"), pg_result($sel,0,"reason_4") );
	$check_week5 = convert_id( pg_result($sel,0,"atdptn_5"), pg_result($sel,0,"reason_5") );
	$check_week6 = convert_id( pg_result($sel,0,"atdptn_6"), pg_result($sel,0,"reason_6") );
	$check_week0 = convert_id( pg_result($sel,0,"atdptn_0"), pg_result($sel,0,"reason_0") );
	$check_week7 = convert_id( pg_result($sel,0,"atdptn_7"), pg_result($sel,0,"reason_7") );
	// 識別コードを４桁に変更 20140125 END

?>

<!-- ************************************************************************ -->
<!-- JavaScript -->
<!-- ************************************************************************ -->
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">

	///-----------------------------------------------------------------------------
	// private member
	///-----------------------------------------------------------------------------
	var row_count = 1;
	var atdptn_count = <?echo $atdptn_cnt?> ;
	var atdptn_id_list = new Array();
	var atdptn_name_list = new Array();
	var option_list ;

<?
	$c_idx = 0;
	for ($i=0; $i<count($data_atdptn);$i++){
		// 出勤表の休暇種別等画面の非表示を確認
		if($data_atdptn[$i]["reason_display_flag"] == "f") {
			continue;
		}
		$wk1=$data_atdptn[$i]["atdptn_ptn_id"];
		echo "\tatdptn_id_list[$c_idx]=$wk1;\n";
		$c_idx++;
	}

	$c_idx = 0;
	for ($i=0; $i<count($data_atdptn);$i++){
		// 出勤表の休暇種別等画面の非表示を確認
		if($data_atdptn[$i]["reason_display_flag"] == "f") {
			continue;
		}
		$wk2=$data_atdptn[$i]["atdptn_ptn_name"];
		if($data_atdptn[$i]["atdptn_ptn_id"] == 10 && $data_atdptn[$i]["reason"] != 34) {
			$wk2 .= "(".$data_atdptn[$i]["reason_name"].")";
		}
		echo "\tatdptn_name_list[$c_idx]=\"$wk2\";\n";
		$c_idx++;
	}
?>
	// php ここまで

	///-----------------------------------------------------------------------------
	// 更新
	///-----------------------------------------------------------------------------
	function editData() {
		///-----------------------------------------------------------------------------
		// 数値チェック
		///-----------------------------------------------------------------------------
		var wk = document.mainform.night_staff_cnt.value;
//	    if (wk.match(/[^0-9]/g)) {
	    if (isNaN(wk)){
	        alert("夜勤従事者への計上に数値以外が設定されています。");
	        return;
	    }
		///-----------------------------------------------------------------------------
		//勤務日チェック時の未設定チェック
		///-----------------------------------------------------------------------------
		var atdptn_cnt = document.mainform.atdptn_cnt.value;
		//月〜金曜日
		if ((document.mainform.duty_mon_day_flg.checked) ||
			(document.mainform.duty_tue_day_flg.checked) ||
			(document.mainform.duty_wed_day_flg.checked) ||
			(document.mainform.duty_thurs_day_flg.checked) ||
			(document.mainform.duty_fri_day_flg.checked)) {
			var wk_flg = ""
			for(i=0;i<atdptn_cnt;i++) {
				var wk_id = "week_flg[" + i + "]";
				if (document.mainform.elements[wk_id].checked) {
					wk_flg = "1";
				}
			}
			if (wk_flg == "") {
		        alert("月〜金曜日の勤務可能なシフトが未設定です。");
		        return;
			}
		}
		//土曜日
		if (document.mainform.duty_sat_day_flg.checked) {
			var wk_flg = ""
			for(i=0;i<atdptn_cnt;i++) {
				var wk_id = "sat_flg[" + i + "]";
				if (document.mainform.elements[wk_id].checked) {
					wk_flg = "1";
				}
			}
			if (wk_flg == "") {
		        alert("土曜日の勤務可能なシフトが未設定です。");
		        return;
			}
		}
		//日曜日
		if (document.mainform.duty_sun_day_flg.checked) {
			var wk_flg = ""
			for(i=0;i<atdptn_cnt;i++) {
				var wk_id = "sun_flg[" + i + "]";
				if (document.mainform.elements[wk_id].checked) {
					wk_flg = "1";
				}
			}
			if (wk_flg == "") {
		        alert("日曜日の勤務可能なシフトが未設定です。");
		        return;
			}
		}
		//祝日
		if (document.mainform.duty_hol_day_flg.checked) {
			var wk_flg = ""
			for(i=0;i<atdptn_cnt;i++) {
				var wk_id = "holiday_flg[" + i + "]";
				if (document.mainform.elements[wk_id].checked) {
					wk_flg = "1";
				}
			}
			if (wk_flg == "") {
				alert("祝日の勤務可能なシフトが未設定です。");
				return;
			}
		}
		// 月間勤務シフトの上限
		if ( document.mainform.shift_limit_count.value != "" ){
			// 項目多重定義と日数の未入力・数値チェック
			for( var i=0; i<document.mainform.shift_limit_count.value ; i++){ // 入力エラーの背景pinkを白でリセット
				document.getElementsByName( "limit_day_count[]" )[i].style.backgroundColor = "white";
			}
			var iname = new Array();
			var error_count=0;
			for( var i=0; i<document.mainform.shift_limit_count.value ; i++){
				iname[i] = document.getElementsByName( "select_atdptn[]" )[i].selectedIndex;
				var day = document.getElementsByName( "limit_day_count[]" )[i].value;
				var day_chk = Number(day) - 0;
				if( isNaN(day_chk) || day==""){
					error_count++;
					document.getElementsByName( "limit_day_count[]" )[i].style.backgroundColor = "pink";
				}
			}
			if( error_count != 0){
				var message = "月間勤務シフトの上限の日数に数値以外が入力されているか入力がありません。";
				alert( message );
				return ;
			}
			iname.sort();
			var j = document.mainform.shift_limit_count.value - 1;
			for( var i=0; i<j ; i++){
				if( iname[i] == iname[i+1] ){
					var message = "月間勤務シフトの上限の「"+atdptn_name_list[iname[i]] + "」が複数定義されています";
					alert( message );
					return ;
				}
			}
			// 日数ブランクチェック
		}
		///-----------------------------------------------------------------------------
		//更新処理
		///-----------------------------------------------------------------------------
//		if (confirm('「更新」します。よろしいですか？')) {
			document.mainform.action="duty_shift_staff_employment_update_exe.php"
			document.mainform.submit();
//		}
	}
	function AllBoxChecked( check ){
		var count;
		var atdptn_ctn = <? echo $atdptn_cnt;?>;
		week = new Array(<?echo "\"week_flg[0]\"";for($i=1; $i<$atdptn_cnt; $i++){echo ",\"week_flg[$i]\"";}?>);
		sat  = new Array(<?echo "\"sat_flg[0]\"";for($i=1; $i<$atdptn_cnt; $i++){echo ",\"sat_flg[$i]\"";}?>);
		sun  = new Array(<?echo "\"sun_flg[0]\"";for($i=1; $i<$atdptn_cnt; $i++){echo ",\"sun_flg[$i]\"";}?>);
		holi = new Array(<?echo "\"holiday_flg[0]\"";for($i=1; $i<$atdptn_cnt; $i++){echo ",\"holiday_flg[$i]\"";}?>);
		for(count = 0; count < atdptn_ctn; count++){
			document.mainform.elements[week[count]].checked = check;
			document.mainform.elements[sat[count]].checked = check;
			document.mainform.elements[sun[count]].checked = check;
			document.mainform.elements[holi[count]].checked = check;
		}
	}

	// 月間勤務シフトの上限・テーブル行の追加 duty_shift_auto.phpから流用
	function addShiftLimit( _atdptn_id , _limit_day ){ // _atdptn_id が0のときは追加ボタンが押されたとき。0でないときはDBデータを初期表示
		if ( row_count > atdptn_count ){
			alert ( "これ以上は追加できません" );
			return ;
		}
		var table = document.getElementById("shift_limit");
		var rows  = table.rows.length;
		var row   = table.insertRow(rows);
		var cell1 = row.insertCell(0);
		var cell2 = row.insertCell(1);
		var cell3 = row.insertCell(2);
		var cell4 = row.insertCell(3);
		cell1.setAttribute("align","right");
		var select_option = "";
		var selected = "";
		for(var i=0; i<atdptn_count; i++){
			if( atdptn_id_list[i] == _atdptn_id){
				selected = " selected";
			}else{
				selected = "";
			}
			var wk_name = atdptn_name_list[i];
			select_option = select_option + "<OPTION VALUE='" + atdptn_id_list[i] + "'" + selected + ">" + wk_name + "</OPTION>";
		}
		cell2.innerHTML = '<select name="select_atdptn[]">' + select_option + '</select>';
		cell3.innerHTML = '<input name="limit_day_count[]" type="text" size="2" maxlength="2" value="' + _limit_day + '">日';
		cell4.innerHTML = '&nbsp<button type="button" onclick="removeShiftLimitRow(this);">削除</button>';
		row_count++;
//		renumberShiftLimit();
		document.mainform.shift_limit_count.value = renumberShiftLimit();
	}
	// 月間勤務シフトの上限・テーブル行の削除 duty_shift_auto.phpから流用
	function removeShiftLimitRow(obj,table) {
		var tr = obj.parentNode.parentNode;
		tr.parentNode.deleteRow(tr.sectionRowIndex);
		document.mainform.shift_limit_count.value = renumberShiftLimit();
	}
	// 月間勤務シフトの上限・連番付与。中間行を削除しても追加しても左の番号は常に1,2,3,4,5... と連番を表示する
	// FireFoxでは正常に動作しない
	function renumberShiftLimit(){
		var table = document.getElementById("shift_limit");
		var editrows = table.rows;
		for( var i=1; i<editrows.length ; i++){
			editrows[i].cells[0].innerHTML = i;
		}
		var retval = i - 1;
		return retval;
	}

    //履歴子画面を開く
    function openHistWin(id) {

        wx = 500;
        wy = 500;
        dx = screen.width / 2 - (wx / 2);
        dy = screen.top;
        base = screen.height / 2 - (wy / 2);
        var url = '';
        if (id == 'df') {
            url = 'employee_cond_df_history_list.php';
        }
        else {
            url = 'employee_cond_op_history_list.php';
        }
        url += '?session=<?=$session?>';
        url += '&emp_id=<?=$staff_id?>';
        url += '&wherefrom=1';
        childwin = window.open(url, 'HistPopup', 'left='+dx+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
        childwin.focus();
    }
</script>

<!-- ************************************************************************ -->
<!-- HTML -->
<!-- ************************************************************************ -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<?
//$wk=$staff_data["pattern_id"];
//echo("pattern_id=$wk");
?>
	<!-- ------------------------------------------------------------------------ -->
	<!-- 画面遷移／タブ -->
	<!-- ------------------------------------------------------------------------ -->
	<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
	<?
		// 画面遷移
		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
		show_user_title($session, $section_admin_auth);			//duty_shift_common.ini
		echo("</table>\n");

		// タブ
		$arr_option = "&group_id=" . $group_id ."&staff_id=" . $staff_id;
		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
		show_user_tab_menuitem($session, $fname, $arr_option);	//duty_shift_user_tab_common.ini
		echo("</table>\n");

		// 下線
		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td bgcolor=\"#5279a5\"><img src=\"img/spacer.gif\" width\"1\" height=\"2\" alt=\"\"></td></tr></table><img src=\"img/spacer.gif\" width=\"1\" height=\"5\" alt=\"\"><br>\n");
	?>
	</td></tr></table>
	<!-- ------------------------------------------------------------------------ -->
	<!-- データ不備時 -->
	<!-- ------------------------------------------------------------------------ -->
	<?
		if ($err_msg_1 != "") {
			echo($err_msg_1);
			echo("<br>\n");
		} else {
	 ?>
	<!-- ------------------------------------------------------------------------ -->
	<!-- 個人名 -->
	<!-- ------------------------------------------------------------------------ -->
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<?
		echo("<tr height=\"22\"> \n");
		echo("<td align=\"left\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">&nbsp;&nbsp; \n");
		echo($staff_name);
		echo("</font></td> \n");
	?>
	</table>
	<!-- ------------------------------------------------------------------------ -->
	<!-- 個人勤務条件設定 -->
	<!-- ------------------------------------------------------------------------ -->
	<form name="mainform" method="post">
		<table width="800" border="0" cellspacing="0" cellpadding="2" class="list">
			<!-- 雇用・勤務形態 -->
			<tr height="22">
			<td width="18%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">雇用・勤務形態</font></td>
			<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
			<label><input type="radio" name="duty_form" value="1"<? if ($duty_form == "1") {echo(" checked");} ?> >常勤</label>
			<label><input type="radio" name="duty_form" value="3"<? if ($duty_form == "3") {echo(" checked");} ?> >短時間正職員</label>
			<label><input type="radio" name="duty_form" value="2"<? if ($duty_form == "2") {echo(" checked");} ?> >非常勤</label>
            &nbsp;<input type="button" value="履歴" onclick="openHistWin('df');">
			</font></td>
			<td width="16%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">他部署兼務</font></td>
			<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
			<label><input type="radio" name="other_post_flg" value="1"<? if ($other_post_flg == "1") {echo(" checked");} ?> >有り</label>
			<label><input type="radio" name="other_post_flg" value="" <? if ($other_post_flg == "") {echo(" checked");} ?> >無し</label>
            &nbsp;<input type="button" value="履歴" onclick="openHistWin('op');">
			</font></td>
			</tr>

			<!-- 勤務日 -->
			<tr height="22">
			<td width="" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務日</font></td>
			<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
			<label><input type="checkbox" name="duty_mon_day_flg" value="1"<? if ($duty_mon_day_flg == "1") {echo(" checked");} ?> >月</label>
			<label><input type="checkbox" name="duty_tue_day_flg" value="1"<? if ($duty_tue_day_flg == "1") {echo(" checked");} ?> >火</label>
			<label><input type="checkbox" name="duty_wed_day_flg" value="1"<? if ($duty_wed_day_flg == "1") {echo(" checked");} ?> >水</label>
			<label><input type="checkbox" name="duty_thurs_day_flg" value="1"<? if ($duty_thurs_day_flg == "1") {echo(" checked");} ?> >木</label>
			<label><input type="checkbox" name="duty_fri_day_flg" value="1"<? if ($duty_fri_day_flg == "1") {echo(" checked");} ?> >金</label>
			<label><input type="checkbox" name="duty_sat_day_flg" value="1"<? if ($duty_sat_day_flg == "1") {echo(" checked");} ?> >土</label>
			<label><input type="checkbox" name="duty_sun_day_flg" value="1"<? if ($duty_sun_day_flg == "1") {echo(" checked");} ?> >日</label>
			<label><input type="checkbox" name="duty_hol_day_flg" value="1"<? if ($duty_hol_day_flg == "1") {echo(" checked");} ?> >祝日</label>
			</font></td>
			</tr>

			<!-- 夜勤の有無 -->
			<tr height="22">
			<td width="" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">夜勤の有無</font></td>
			<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
			<label><input type="radio" name="night_duty_flg" value="1"<? if (($night_duty_flg == "1") || ($night_duty_flg == "")) {echo(" checked");} ?> >有り</label>
			<label><input type="radio" name="night_duty_flg" value="" <? if ($night_duty_flg == "") {echo(" checked");} ?> >無し</label>
			<label><input type="radio" name="night_duty_flg" value="2"<? if ($night_duty_flg == "2") {echo(" checked");} ?> >夜専</label>
</font>
<? /* 文言変更 20120312 */ ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="red">&nbsp;（届出書添付書類上は、このチェックに関わらず自動判定されます。夜勤時間数が１６時間を超えると夜勤有り、全ての勤務日の日勤時間が1時間未満の夜勤の場合は、夜専と判定します。）
</font>
			</font></td>
			</tr>

			<!-- 夜勤従事者への計上 -->
			<? /* 20100629 自動計算するため表示しない。既存分を残すためhiddenに出力
			<tr height="22">
			<td width="30%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">夜勤従事者への計上</font></td>
			<td colspan="3">
			<input type="text" name="night_staff_cnt" value="<? echo($night_staff_cnt); ?>" size="4" maxlength="4" style="ime-mode:inactive;">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">人</font>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="red">（常勤の場合は、届出書添付書類では、自動計算されます。非常勤の場合は、施設基準マスタで、月間の所定労働時間が登録されている場合は自動計算されます。）
</font>
			</td>
			</tr>
			*/ ?>
			<input type="hidden" name="night_staff_cnt" value="<? echo($night_staff_cnt); ?>">
		</table>
		<!-- ------------------------------------------------------------------------ -->
		<!-- 「更新」ボタン -->
		<!-- ------------------------------------------------------------------------ -->
		<table width="800" border="0" cellspacing="0" cellpadding="2">
		<tr height="22">
			<td align="right"><input type="button" value="更新" onclick="editData();" ></td>
		</tr>
		</table>
		<!-- ------------------------------------------------------------------------ -->
		<!-- 勤務可能なシフト -->
		<!-- ------------------------------------------------------------------------ -->
		<table width="800" border="0" cellspacing="0" cellpadding="2" class="list">
		<?
			///-----------------------------------------------------------------------------
			// 見出し
			///-----------------------------------------------------------------------------
			echo("<tr height=\"22\"> \n");
			$td_width = 120;	// 幅変更 20140125
			echo("<td width=\"$td_width\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"></font></td> \n");
			echo("<td width=\"$td_width\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">月〜金</font></td> \n");
			echo("<td width=\"$td_width\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">土</font></td> \n");
			echo("<td width=\"$td_width\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">日</font></td> \n");
			echo("<td width=\"$td_width\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">祝日</font></td> \n");
			echo("</tr> \n");
			///-----------------------------------------------------------------------------
			// データ
			///-----------------------------------------------------------------------------
			echo("<tr height=\"22\"> \n");
			echo("<td width=\"100\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">勤務可能なシフト</font><BR> \n");

			// 全て{ON|OFF} ボタン
			echo("<input type=\"button\" value=\"全てON\" onclick=\"AllBoxChecked(true);\"> \n");
			echo("<BR>");
			echo("<input type=\"button\" value=\"全てOFF\" onclick=\"AllBoxChecked(false);\"> \n");
			echo("</td> \n");

			//データ設定 月曜〜金曜
			echo("<td width=\"$td_width\" align=\"left\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"> \n");	// 幅変更 20140125
			// 休暇をまとめるためのフラグ
			$hol_flg = false;

			$c_idx = 0; //チェックボックスの位置、0始まり
			for ($i=0; $i<count($data_atdptn); $i++) {
				// 出勤表の休暇種別等画面の非表示を確認
				if($data_atdptn[$i]["reason_display_flag"] == "f") {
					continue;
				}
				$wk = "";
				$k =  $data_atdptn[$i]["atdptn_ptn_id"];
				$wk_name = $data_atdptn[$i]["atdptn_ptn_name"];

				// 休暇をまとめる
				if($k == 10) {
					if($hol_flg == false) {
						$wk_name .= "(公休)";
						$hol_flg = true;
						if (!array_key_exists("week_flg_$k", $staff_data) or $staff_data["week_flg_$k"] == "1") { $wk = "checked"; }
                        echo("<label><input type=\"checkbox\" name=\"week_flg[$c_idx]\" value=\"1\" $wk>$wk_name</label><br> \n");
					} else {
						continue;
					}
				} else {
					if (!array_key_exists("week_flg_$k", $staff_data) or $staff_data["week_flg_$k"] == "1") { $wk = "checked"; }
                    echo("<label><input type=\"checkbox\" name=\"week_flg[$c_idx]\" value=\"1\" $wk>$wk_name</label><br> \n");
				}
				$c_idx++;
			}
			echo("</font></td> \n");

			//データ設定 土曜
			echo("<td width=\"$td_width\" align=\"left\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"> \n");	// 幅変更 20140125
			// 休暇をまとめるためのフラグ
			$hol_flg = false;

			$c_idx = 0; //チェックボックスの位置、0始まり
			for ($i=0; $i<count($data_atdptn); $i++) {
				// 出勤表の休暇種別等画面の非表示を確認
				if($data_atdptn[$i]["reason_display_flag"] == "f") {
					continue;
				}
				$wk = "";
				$k =  $data_atdptn[$i]["atdptn_ptn_id"];
				$wk_name = $data_atdptn[$i]["atdptn_ptn_name"];

				// 休暇をまとめる
				if($k == 10) {
					if($hol_flg == false) {
						$wk_name .= "(公休)";
						$hol_flg = true;
						if (!array_key_exists("sat_flg_$k", $staff_data) or $staff_data["sat_flg_$k"] == "1") { $wk = "checked"; }
                        echo("<label><input type=\"checkbox\" name=\"sat_flg[$c_idx]\" value=\"1\" $wk>$wk_name</label><br> \n");
					} else {
						continue;
					}
				} else {
					if (!array_key_exists("sat_flg_$k", $staff_data) or $staff_data["sat_flg_$k"] == "1") { $wk = "checked"; }
                    echo("<label><input type=\"checkbox\" name=\"sat_flg[$c_idx]\" value=\"1\" $wk>$wk_name</label><br> \n");
				}
				$c_idx++;
			}
			echo("</font></td> \n");

			//データ設定 日曜
			echo("<td width=\"$td_width\" align=\"left\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"> \n");	// 幅変更 20140125
			// 休暇をまとめるためのフラグ
			$hol_flg = false;

			$c_idx = 0; //チェックボックスの位置、0始まり
			for ($i=0; $i<count($data_atdptn); $i++) {
				// 出勤表の休暇種別等画面の非表示を確認
				if($data_atdptn[$i]["reason_display_flag"] == "f") {
					continue;
				}
				$wk = "";
				$k =  $data_atdptn[$i]["atdptn_ptn_id"];
				$wk_name = $data_atdptn[$i]["atdptn_ptn_name"];

				// 休暇をまとめる
				if($k == 10) {
					if($hol_flg == false) {
						$wk_name .= "(公休)";
						$hol_flg = true;
						if (!array_key_exists("sun_flg_$k", $staff_data) or $staff_data["sun_flg_$k"] == "1") { $wk = "checked"; }
                        echo("<label><input type=\"checkbox\" name=\"sun_flg[$c_idx]\" value=\"1\" $wk>$wk_name</label><br> \n");
					} else {
						continue;
					}
				} else {
					if (!array_key_exists("sun_flg_$k", $staff_data) or $staff_data["sun_flg_$k"] == "1") { $wk = "checked"; }
                    echo("<label><input type=\"checkbox\" name=\"sun_flg[$c_idx]\" value=\"1\" $wk>$wk_name</label><br> \n");
				}
				$c_idx++;
			}

			echo("</font></td> \n");

			//データ設定 祝日
			echo("<td width=\"$td_width\" align=\"left\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"> \n");	// 幅変更 20140125
			// 休暇をまとめるためのフラグ
			$hol_flg = false;

			$c_idx = 0; //チェックボックスの位置、0始まり
			for ($i=0; $i<count($data_atdptn); $i++) {
				// 出勤表の休暇種別等画面の非表示を確認
				if($data_atdptn[$i]["reason_display_flag"] == "f") {
					continue;
				}
				$wk = "";
				$k =  $data_atdptn[$i]["atdptn_ptn_id"];
				$wk_name = $data_atdptn[$i]["atdptn_ptn_name"];

				// 休暇をまとめる
				if($k == 10) {
					if($hol_flg == false) {
						$wk_name .= "(公休)";
						$hol_flg = true;
						if (!array_key_exists("holiday_flg_$k", $staff_data) or $staff_data["holiday_flg_$k"] == "1") { $wk = "checked"; }
                        echo("<label><input type=\"checkbox\" name=\"holiday_flg[$c_idx]\" value=\"1\" $wk>$wk_name</label><br> \n");
					} else {
						continue;
					}
				} else {
					if (!array_key_exists("holiday_flg_$k", $staff_data) or $staff_data["holiday_flg_$k"] == "1") { $wk = "checked"; }
                    echo("<label><input type=\"checkbox\" name=\"holiday_flg[$c_idx]\" value=\"1\" $wk>$wk_name</label><br> \n");
				}
				$c_idx++;
			}

			echo("</font></td> \n");

			echo("</tr> \n");

		?>
		</table>

		<BR>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">個人定型勤務条件</font><BR>
		<table width="800" border="0" cellspacing="0" cellpadding="2" class="list">
		<tr height="22">
		<td width="120" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">月</font></td>
		<td width="120" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">火</font></td>
		<td width="120" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">水</font></td>
		<td width="120" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">木</font></td>
		<td width="120" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">金</font></td>
		<td width="120" align="center" bgcolor="#E0FFFF"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">土</font></td>
		<td width="120" align="center" bgcolor="#FFF0F5"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日</font></td>
		<td width="120" align="center" bgcolor="#C8FFD5"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">祝</font></td>
		</tr>
		<tr>
		<!-- $color_array は使わなくなった 20140130
		<td align="center"><select id="fixed_week1" name="fixed_week[1]"><? disp_atdptn($data_atdptn,1,$check_week1,$color_array); ?></select></td>
		<td align="center"><select id="fixed_week2" name="fixed_week[2]"><? disp_atdptn($data_atdptn,2,$check_week2,$color_array); ?></select></td>
		<td align="center"><select id="fixed_week3" name="fixed_week[3]"><? disp_atdptn($data_atdptn,3,$check_week3,$color_array); ?></select></td>
		<td align="center"><select id="fixed_week4" name="fixed_week[4]"><? disp_atdptn($data_atdptn,4,$check_week4,$color_array); ?></select></td>
		<td align="center"><select id="fixed_week5" name="fixed_week[5]"><? disp_atdptn($data_atdptn,5,$check_week5,$color_array); ?></select></td>
		<td align="center"><select id="fixed_week6" name="fixed_week[6]"><? disp_atdptn($data_atdptn,6,$check_week6,$color_array); ?></select></td>
		<td align="center"><select id="fixed_week7" name="fixed_week[0]"><? disp_atdptn($data_atdptn,8,$check_week0,$color_array); ?></select></td>
		<td align="center"><select id="fixed_week8" name="fixed_week[7]"><? disp_atdptn($data_atdptn,7,$check_week7,$color_array); ?></select></td>
		 -->
		<td align="center"><select id="fixed_week1" name="fixed_week[1]"><? disp_atdptn($data_atdptn,1,$check_week1); ?></select></td>
		<td align="center"><select id="fixed_week2" name="fixed_week[2]"><? disp_atdptn($data_atdptn,2,$check_week2); ?></select></td>
		<td align="center"><select id="fixed_week3" name="fixed_week[3]"><? disp_atdptn($data_atdptn,3,$check_week3); ?></select></td>
		<td align="center"><select id="fixed_week4" name="fixed_week[4]"><? disp_atdptn($data_atdptn,4,$check_week4); ?></select></td>
		<td align="center"><select id="fixed_week5" name="fixed_week[5]"><? disp_atdptn($data_atdptn,5,$check_week5); ?></select></td>
		<td align="center"><select id="fixed_week6" name="fixed_week[6]"><? disp_atdptn($data_atdptn,6,$check_week6); ?></select></td>
		<td align="center"><select id="fixed_week7" name="fixed_week[0]"><? disp_atdptn($data_atdptn,8,$check_week0); ?></select></td>
		<td align="center"><select id="fixed_week8" name="fixed_week[7]"><? disp_atdptn($data_atdptn,7,$check_week7); ?></select></td>
		</tr>
		</table>
		<!-- ------------------------------------------------------------------------ -->
		<!-- 「更新」ボタン -->
		<!-- ------------------------------------------------------------------------ -->
		<table width="800" border="0" cellspacing="0" cellpadding="2">
		<tr height="22">
			<td align="right"><input type="button" value="更新" onclick="editData();" ></td>
		</tr>
		</table>
<BR>
		<!-- ------------------------------------------------------------------------ -->
		<!-- シフト作成期間中の上限 -->
		<!-- ------------------------------------------------------------------------ -->
		<table width="700" border="0" cellspacing="0" cellpadding="2" class="list" id="shift_limit">
		<tr height="22">
		<td width="200" align="center" ><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">＜月間勤務シフトの上限＞</font></td>
		<td width="200" align="center" ><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">シフト</font></td>
		<td width="200" align="center" ><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">上限</font></td>
		<td width="100" align="left" ><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
			<input type="button" value="追加" onclick="addShiftLimit(0,0);">
		</font></td>
		</tr>

		<!-- 追加・削除は スクリプト-->
		</table>

		<!-- ------------------------------------------------------------------------ -->
		<!-- ＨＩＤＤＥＮ -->
		<!-- ------------------------------------------------------------------------ -->
		<input type="hidden" name="session" value="<? echo($session); ?>">
		<input type="hidden" name="group_id" value="<? echo($group_id); ?>">
		<input type="hidden" name="staff_id" value="<? echo($staff_id); ?>">
		<input type="hidden" name="atdptn_cnt" value="<? echo($atdptn_cnt); ?>">
		<input type="hidden" name="shift_limit_count" value="0">
		<?
			// 休暇をまとめるためのフラグ
			$hol_flg = false;

			for ($i=0; $i<count($data_atdptn); $i++) {
				// 出勤表の休暇種別等画面の非表示を確認
				if($data_atdptn[$i]["reason_display_flag"] == "f") {
					continue;
				}
				$k =  $data_atdptn[$i]["atdptn_ptn_id"];
//				echo("<input type=\"hidden\" name=\"atdptn_ptn_id[]\" value=\"$k\">\n");

				// 休暇をまとめる
				if($k == 10) {
					if($hol_flg == false) {
						echo("<input type=\"hidden\" name=\"atdptn_ptn_id[]\" value=\"$k\">\n");
						$hol_flg = true;
					} else {
						continue;
					}
				} else {
					echo("<input type=\"hidden\" name=\"atdptn_ptn_id[]\" value=\"$k\">\n");
				}
			}
		?>
	</form>

	<?
		}
	?>

</td>
</tr>
</table>
<?
if( count($limit_array) != 0 ){
	echo "<script type=\"text/javascript\" src=\"js/fontsize.js\"></script>\n";
	echo "<script type=\"text/javascript\">\n";
	echo "// 連続勤務シフトの上限対応\n";
	echo "// 最初に表示するときDBから読み取ったデータがある場合の表(TABLE)作成\n";
	for($ii=0;$ii < count($limit_array); $ii++){
		echo "addShiftLimit(".$limit_array[$ii]["atdptn_id"].",".$limit_array[$ii]["limit_day_count"].");\n";
	}
	echo "</script>\n";
}

//function disp_atdptn($data_atdptn, $week, $check_week, $color_array){
function disp_atdptn($data_atdptn, $week, $check_week){
// 個人定型勤務条件設定用のリスト
// 識別コードを４桁に変更 20140125
// $color_array は使わなくなった 20140130

	echo "<option value='' selected></option>\n";

	foreach($data_atdptn as $num=>$v1){

		// 出勤表の休暇種別等画面の非表示を確認
		if($v1["reason_display_flag"] == "f") {
			continue;
		}
		$wk = $v1['atdptn_ptn_name'];
		$id = $v1['atdptn_ptn_id'];
		$rid = $v1['reason'];
		$wk_id = convert_id($id, $rid);

//		if( $id==10 ){$wk=$wk.'(公休)';}			// 休暇を種別ごとに表示する

		// 休暇の種別を全て表示（個人定型勤務条件の選択リスト）
		if($id == 10) {
			if($v1["reason"] != 34) {				// 休暇（事由なし）
				$wk .= "(".$v1["reason_name"].")";
			}
		}

		$fcolor = $v1['font_color_id'];
		$bcolor = $v1['back_color_id'];

//		if ( $id == $check_week ){
		if ( $wk_id == $check_week ) {
			$checked = "selected";
		} else {
			$checked = "";
		}
		echo "<option style=\"background-color:$bcolor; color:$fcolor\" value=\"$wk_id\" $checked>$wk</option>\n";
	}
}

// 勤務シフトパターンコードと事由コードを編集
function convert_id($default_id, $reason_id) {

	$id = $default_id;
	$rid = $reason_id;

	// ※バージョンアップ時に必要なコード変換 20140125
	if(($id == 10) && ($rid == 0)) {
		$rid = 24;						// 休暇（公休）とする
	}

	$ret = sprintf("%02d", $id).sprintf("%02d", $rid);

	return $ret;
}

?>
</body>
<? pg_close($con); ?>
</html>

