<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("label_by_profile_type.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$auth_id = ($func == "bed") ? 14 : 22;
$checkauth = check_authority($session, $auth_id, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);
pg_query($con, "begin");

$profile_type = get_profile_type($con, $fname);
?>
<body>
<form name="items" method="post" action="patient_waiting_detail.php">
<input type="hidden" name="back" value="t">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="id" value="<? echo($id); ?>">
<input type="hidden" name="progress" value="<? echo($progress); ?>">
<input type="hidden" name="charge_emp_id" value="<? echo($charge_emp_id); ?>">
<input type="hidden" name="intro_inst_cd" value="<? echo($intro_inst_cd); ?>">
<input type="hidden" name="intro_sect_rireki" value="<? echo($intro_sect_rireki); ?>">
<input type="hidden" name="intro_sect_cd" value="<? echo($intro_sect_cd); ?>">
<input type="hidden" name="intro_sect_text" value="<? echo($intro_sect_text); ?>">
<input type="hidden" name="intro_doctor_no" value="<? echo($intro_doctor_no); ?>">
<input type="hidden" name="intro_doctor_text" value="<? echo($intro_doctor_text); ?>">
<input type="hidden" name="msw" value="<? echo($msw); ?>">
<input type="hidden" name="disease" value="<? echo($disease); ?>">
<input type="hidden" name="patho_from_year" value="<? echo($patho_from_year); ?>">
<input type="hidden" name="patho_from_month" value="<? echo($patho_from_month); ?>">
<input type="hidden" name="patho_from_day" value="<? echo($patho_from_day); ?>">
<input type="hidden" name="patho_to_year" value="<? echo($patho_to_year); ?>">
<input type="hidden" name="patho_to_month" value="<? echo($patho_to_month); ?>">
<input type="hidden" name="patho_to_day" value="<? echo($patho_to_day); ?>">
<input type="hidden" name="note" value="<? echo($note); ?>">
<input type="hidden" name="dementia" value="<? echo($dementia); ?>">
<input type="hidden" name="reception_year" value="<? echo($reception_year); ?>">
<input type="hidden" name="reception_month" value="<? echo($reception_month); ?>">
<input type="hidden" name="reception_day" value="<? echo($reception_day); ?>">
<input type="hidden" name="reception_hour" value="<? echo($reception_hour); ?>">
<input type="hidden" name="reception_min" value="<? echo($reception_min); ?>">
<input type="hidden" name="verified_year" value="<? echo($verified_year); ?>">
<input type="hidden" name="verified_month" value="<? echo($verified_month); ?>">
<input type="hidden" name="verified_day" value="<? echo($verified_day); ?>">
<input type="hidden" name="verified_hour" value="<? echo($verified_hour); ?>">
<input type="hidden" name="verified_min" value="<? echo($verified_min); ?>">
<input type="hidden" name="bed_type_cd" value="<? echo($bed_type_cd); ?>">
<input type="hidden" name="status1" value="<? echo($status1); ?>">
<input type="hidden" name="status2" value="<? echo($status2); ?>">
<input type="hidden" name="ccl_psn" value="<? echo($ccl_psn); ?>">
<input type="hidden" name="ccl_psn_dtl" value="<? echo($ccl_psn_dtl); ?>">
<input type="hidden" name="ccl_rsn" value="<? echo($ccl_rsn); ?>">
<input type="hidden" name="ccl_rsn_cd" value="<? echo($ccl_rsn_cd); ?>">
<input type="hidden" name="func" value="<? echo($func); ?>">
</body>
<?
// チェック情報を取得
$sql = "select * from bedcheckwait";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
for ($i = 1; $i <= 11; $i++) {
	$varname = "required$i";
	$$varname = pg_fetch_result($sel, 0, "required$i");
}

if ($charge_emp_id == "") {
	if ($required1 == "t") {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('当院担当者を選択してください。');</script>\n");
		echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
		exit;
	}

	$charge_emp_id = null;
}

if ($intro_inst_cd == "") {
	if ($required2 == "t") {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('紹介元医療機関を選択してください。');</script>\n");
		echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
		exit;
	}

	$intro_inst_cd = null;
}

if ($intro_sect_cd == "") {
	if ($required3 == "t" && $intro_sect_text == "") {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('診療科を入力してください。');</script>\n");
		echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
		exit;
	}

	$intro_sect_cd = null;
}

if (strlen($intro_sect_text) > 50) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('診療科名が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

if ($intro_doctor_no == "") {
	if ($required4 == "t" && $intro_doctor_text == "") {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('前医を入力してください。');</script>\n");
		echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
		exit;
	}

	$intro_doctor_no = null;
}

if (strlen($intro_doctor_text) > 50) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('前医名が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

if (strlen($msw) > 50) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('MSWが長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

if ($required11 == "t" && $msw == "") {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('MSWを入力してください。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

if (strlen($disease) > 120) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('傷病名が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

if ($required6 == "t" && $disease == "") {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('傷病名を入力してください。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

if (($patho_from_year == "-" && $patho_from_month == "-" && $patho_from_day == "-") || ($patho_from_year == "" && $patho_from_month == "" && $patho_from_day == "")) {
	$patho_from = null;
} else {
	if (!checkdate($patho_from_month, $patho_from_day, $patho_from_year)) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('発症日（From）が不正です。');</script>\n");
		echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
		exit;
	}
	$patho_from = "$patho_from_year$patho_from_month$patho_from_day";
}

if (($patho_to_year == "-" && $patho_to_month == "-" && $patho_to_day == "-") || ($patho_to_year == "" && $patho_to_month == "" && $patho_to_day == "")) {
	$patho_to = null;
} else {
	if (!checkdate($patho_to_month, $patho_to_day, $patho_to_year)) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('発症日（To）が不正です。');</script>\n");
		echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
		exit;
	}
	$patho_to = "$patho_to_year$patho_to_month$patho_to_day";
}

if (!is_null($patho_from) && !is_null($patho_to) && $patho_from > $patho_to) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('発症日の期間が不正です。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

if ($required7 == "t" && is_null($patho_from) && is_null($patho_to)) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('発症日を入力してください。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

if (strlen($note) > 150) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('備考（ADL等）が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

if ($required8 == "t" && $note == "") {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('備考（ADL等）を入力してください。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

if ($dementia == "") {
	if ($required5 == "t") {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('認知症の有無を選択してください。');</script>\n");
		echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
		exit;
	}

	$dementia = null;
}

if (!checkdate($reception_month, $reception_day, $reception_year)) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('受付日が不正です。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}
$reception_time = "$reception_year$reception_month$reception_day$reception_hour$reception_min";

$sql = "select * from ptwait";
$cond = "where ptif_id = '$pt_id' and reception_time = '$reception_time' and ptwait_id <> $id limit 1";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if (pg_num_rows($sel) > 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('受付日時が重複しています。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

if ($verified_year == "-" && $verified_month == "-" && $verified_day == "-" && $verified_hour == "-" && $verified_min == "-") {
	$verified_time = null;
} else {
	if (!checkdate($verified_month, $verified_day, $verified_year)) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('最終確認日が不正です。');</script>\n");
		echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
		exit;
	}

	if ($verified_hour == "-" || $verified_min == "-") {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('最終確認時刻が不正です。');</script>\n");
		echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
		exit;
	}

	$verified_time = "$verified_year$verified_month$verified_day$verified_hour$verified_min";
}

if (strlen($status1) > 150) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('調整状況1が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

if ($required9 == "t" && $status1 == "") {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('調整状況1を入力してください。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

if (strlen($status2) > 150) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('調整状況2が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

if ($required10 == "t" && $status2 == "") {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('調整状況2を入力してください。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

if (strlen($ccl_psn_dtl) > 50) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('キャンセル連絡者の詳細が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

if (strlen($ccl_rsn) > 200) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('キャンセル理由が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

// 待機データ更新
if ($intro_sect_rireki == "") $intro_sect_rireki = null;
if ($bed_type_rireki == "" || $bed_type_cd == "") {$bed_type_rireki = null;}
if ($ccl_psn == "") $ccl_psn = null;
if ($ccl_rsn_rireki == "" || $ccl_rsn_cd == "") {$ccl_rsn_rireki = null;}
$sql = "update ptwait set";
$set = array("reception_time", "progress", "charge_emp_id", "intro_inst_cd", "intro_sect_rireki", "intro_sect_cd", "intro_sect_text", "intro_doctor_no", "intro_doctor_text", "msw", "disease", "patho_from", "patho_to", "note", "dementia", "verified_time", "bed_type_cd", "status1", "status2", "ccl_psn", "ccl_psn_dtl", "ccl_rsn", "ccl_rsn_rireki", "ccl_rsn_cd");
$setvalue = array($reception_time, $progress, $charge_emp_id, $intro_inst_cd, $intro_sect_rireki, $intro_sect_cd, $intro_sect_text, $intro_doctor_no, $intro_doctor_text, $msw, $disease, $patho_from, $patho_to, $note, $dementia, $verified_time, $bed_type_cd, $status1, $status2, $ccl_psn, $ccl_psn_dtl, $ccl_rsn, $ccl_rsn_rireki, $ccl_rsn_cd);
$cond = "where ptwait_id = $id";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

pg_query($con, "commit");
pg_close($con);

echo("<script type=\"text/javascript\">opener.location.reload();</script>");
echo("<script type=\"text/javascript\">self.close();</script>");
