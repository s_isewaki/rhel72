<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務管理 | 統計資料 個人別残業統計</title>
<?php
require_once("about_authority.php");
require_once("about_session.php");
require_once("about_postgres.php");

require_once("show_class_name.ini");
require_once("referer_common.ini");
require_once("work_admin_menu_common.ini");
require_once("show_attendance_pattern.ini");
require_once("show_select_values.ini");
require_once("show_timecard_common.ini");
require_once("work_admin_timecard_common.php");
require_once("atdbk_menu_common.ini");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
//セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
// 勤務管理権限チェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0" && $mmode != "usr") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

///-----------------------------------------------------------------------------
//初期処理
///-----------------------------------------------------------------------------
	// 期間(個人別残業時間リスト)
	if ($start_yr == "") {
		$start_yr = date('Y');
	}
	if ($start_mon == "") {
		$start_mon = date('m');
	}
	// 期間
	if ($start_yr_3month == "") {
		$start_yr_3month = date('Y');
	}
	if ($start_mon_3month == "") {
		$start_mon = date('m');
	}

?>

<!-- ************************************************************************ -->
<!-- JavaScript -->
<!-- ************************************************************************ -->
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function overtimeSummaryManthly() {
	if (check_error("one")){
		return false;
	}
	document.mainform.action = 'work_admin_overtime_ranking_Monthly.php';
	document.mainform.submit();
}

function overtimeSummary2Manth() {
	if (check_error("two")){
		return false;
	}
	document.mainform.action = 'work_admin_overtime_ranking_2Monthly.php';
	document.mainform.submit();
}

function overtimeSummary3Manth() {
	if (check_error("three")){
		return false;
	}
	document.mainform.action = 'work_admin_overtime_ranking_3Monthly.php';
	document.mainform.submit();
}

function check_error(flag){

	if (flag === "one"){
		var day_one =  document.mainform.base_one.value;
		if (day_one.match(/[^0-9]+/)){
			alert("個人別残業時間統計\n時間には半角数字のみで入力してください。");
			return true;
		}	
	}

	if (flag === "three"){
		var day_three =  document.mainform.base_three.value;
		if (day_three.match(/[^0-9]+/)){
			alert("個人別残業時間統計（3ヶ月まとめ）\n時間には半角数字のみで入力してください。");
			return true;
		}
	}
	return false;
}
</script>

<!-- ************************************************************************ -->
<!-- HTML -->
<!-- ************************************************************************ -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($mmode == "usr") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><img src="img/icon/b07.gif" width="32" height="32" border="0" alt="出勤表"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>出勤表</b></a></font></td>
<? if ($checkauth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="work_admin_menu.php?session=<? echo($session); ?>&work=1"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<?
}
else {
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<!-- atdbk_timecard.phpを atdbk_timecard_shift.phpに変更 20130129 -->
<td width="32" height="32" class="spacing"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><img src="img/icon/b07.gif" width="32" height="32" border="0" alt="出勤表"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>出勤表</b></a> &gt; <a href="work_admin_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } ?>
	<!-- ------------------------------------------------------------------------ -->
	<!-- 画面遷移／タブ -->
	<!-- ------------------------------------------------------------------------ -->
	<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
	<?
	// メニュータブの表示
	if ($mmode != "usr") {
		show_work_admin_menuitem($session, $fname, "");
	}
	else {
		show_atdbk_menuitem($session, $fname, "");
	}
	// 下線
		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td bgcolor=\"#5279a5\"><img src=\"img/spacer.gif\" width\"1\" height=\"2\" alt=\"\"></td></tr></table><img src=\"img/spacer.gif\" width=\"1\" height=\"5\" alt=\"\"><br>\n");
	?>
	</td></tr></table>

	<?php
	show_work_admin_submenu_statiscal($session, $fname, "");
	?>
	<!-- ------------------------------------------------------------------------ -->
	<!-- 入力エリア -->
	<!-- ------------------------------------------------------------------------ -->
	<form name="mainform" method="post">
		<!-- ------------------------------------------------------------------------ -->
		<!-- 個人別残業時間リスト -->
		<!-- ------------------------------------------------------------------------ -->
		<table width="960" border="0" cellspacing="0" cellpadding="2" style="margin-top: 10px;">
		<tr height="22">
		<td width="540" align="left" nowrap><h5>個人別残業時間統計</h5>
		<?
		// 期間
		?>
		<select id='start_yr' name='start_yr'><?
		show_select_years(15, $start_yr, false, true);
		?></select>年
		<select id='start_mon' name='start_mon'><?
		show_select_months($start_mon, false);
		?></select>月分の残業時間合計が<input type="text" id="base_one" name="base_one" size="3" value="45" align="right" style="padding-left: 3px;ime-mode:inactive;" />時間を超えた職員を出力する。
		<!-- 表示ボタン -->
		<input type="button" value="CSV出力" onclick="return overtimeSummaryManthly();"></td>
		<td align="right" width="170">
		</td>
		</tr>
		</table>

		<!-- ------------------------------------------------------------------------ -->
		<!-- 個人別残業時間調査（2ヶ月まとめ） -->
		<!-- ------------------------------------------------------------------------ -->
		<table width="960" border="0" cellspacing="0" cellpadding="2" style="margin-top: 30px;">
		<tr height="22">
		<td width="540" align="left" nowrap><h5>個人別残業時間統計（2ヶ月まとめ）</h5>
		<?
		// 期間
		?>
		<select id='start_yr_2month' name='start_yr_2month'><?
		show_select_years(15, $start_yr, false, true);
		?></select>年
		<select id='start_mon_2month' name='start_mon_2month'><?
		show_select_months($start_mon, false);
		?></select>月以前2ヶ月の残業時間平均が<input type="text" id="base_two" name="base_two" size="3" value="45" align="right" style="padding-left: 3px;ime-mode:inactive;" />時間を超えた職員を出力する。
		<!-- 表示ボタン -->
		<input type="button" value="CSV出力" onclick="return overtimeSummary2Manth()"></td>
		<td align="right" width="170">
		</td>
		</tr>
		</table>
		
		<!-- ------------------------------------------------------------------------ -->
		<!-- 個人別残業時間調査（3ヶ月まとめ） -->
		<!-- ------------------------------------------------------------------------ -->
		<table width="960" border="0" cellspacing="0" cellpadding="2" style="margin-top: 30px;">
		<tr height="22">
		<td width="540" align="left" nowrap><h5>個人別残業時間統計（3ヶ月まとめ）</h5>
		<?
		// 期間
		?>
		<select id='start_yr_3month' name='start_yr_3month'><?
		show_select_years(15, $start_yr, false, true);
		?></select>年
		<select id='start_mon_3month' name='start_mon_3month'><?
		show_select_months($start_mon, false);
		?></select>月以前3ヶ月の残業時間平均が<input type="text" id="base_three" name="base_three" size="3" value="45" align="right" style="padding-left: 3px;ime-mode:inactive;" />時間を超えた職員を出力する。
		<!-- 表示ボタン -->
		<input type="button" value="CSV出力" onclick="return overtimeSummary3Manth()"></td>
		<td align="right" width="170">
		</td>
		</tr>
		</table>
<?
// 年月日
//
// 表示、検索後
if ($action == "表示") {}
?>
		<!-- ------------------------------------------------------------------------ -->
		<!-- ＨＩＤＤＥＮ -->
		<!-- ------------------------------------------------------------------------ -->
		<input type="hidden" name="session" value="<? echo($session); ?>">
		<input type="hidden" name="mmode" value="<?php echo($mmode); ?>">
<!-- 	    <input type="hidden" name="overtime_criteria" value=""> -->
	</form>
<!--form name="csv" method="get" target="download"-->
<!-- 	<form name="csv" method="get"> -->
<!-- 	  <input type="hidden" name="overtime_criteria" value=""> -->
<!-- 	</form> -->
<!-- 	<iframe name="download" width="0" height="0" frameborder="0"></iframe> -->

</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
