<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
//ファイルの読み込み
require_once("about_comedix.php");
require_once("about_validation.php");

//ページ名
$fname = $PHP_SELF;

//セッションチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//権限チェック
$dept = check_authority($session,79,$fname);
if($dept == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//ＤＢのコネクション作成
$con = connect2db($fname);

//入力チェック
if($hospital_name ==""){
	echo("<script language=\"javascript\">alert(\"病院名を入力してください\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}
if($hospital_directer ==""){
	echo("<script language=\"javascript\">alert(\"病院長名を入力してください\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}
if($hospital_bed ==""){
	echo("<script language=\"javascript\">alert(\"病床数を入力してください\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}
if($nursing_standard ==""){
	echo("<script language=\"javascript\">alert(\"入院基本料施設基準（一般病床）を入力してください\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}

// トランザクションの開始
pg_query($con, "begin");

// 新規病院ID採番
$sql  = "select max(hospital_id) as max from fplus_hospital_master";
$cond = "";
$hospital_id_sel = select_from_table($con,$sql,$cond,$fname);
$hospital_id = pg_result($hospital_id_sel,0,"max");
if($hospital_id == ""){
	$hospital_id = 1;
}else{
	$hospital_id = $hospital_id + 1;
}

// 更新履歴ID採番
$sql  = "select max(hospital_history_id) as max from fplus_hospital_history";
$cond = "";
$history_id_sel = select_from_table($con,$sql,$cond,$fname);
$history_id = pg_result($history_id_sel,0,"max");
if($history_id == ""){
	$history_id = 1;
}else{
	$history_id = $history_id + 1;
}

// 履歴年度（初期値の設定）
$fiscal_year = "0000";

if($class_id==""){

$class_id = NULL;

}
if($attribute_id==""){

$attribute_id = NULL;

}
if($dept_id==""){

$dept_id = NULL;

}
if($room_id==""){

$room_id = NULL;

}

//ＤＢへ病院情報を登録する
$sql = "insert into fplus_hospital_master (hospital_id, hospital_name, hospital_directer, class_id, attribute_id, dept_id, room_id, update_time, del_flg) values (";
$content = array($hospital_id, "$hospital_name", "$hospital_directer", $class_id, $attribute_id, $dept_id, $room_id, date("Y-m-d H:i:s"), "f");

$in_hospital = insert_into_table($con,$sql,$content,$fname);	//病院情報を挿入

if ($in_hospital == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

//ＤＢへ履歴情報を登録する
$sql = "insert into fplus_hospital_history (hospital_history_id, hospital_id, fiscal_year, hospital_bed, nursing_standard, closing_flg, update_time, del_flg) values (";
$content = array($history_id, $hospital_id, "$fiscal_year", "$hospital_bed", "$nursing_standard", 'f', date("Y-m-d H:i:s"), "f");

$in_history = insert_into_table($con,$sql,$content,$fname);	//履歴情報を挿入

if ($in_history == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// トランザクションのコミット
pg_query($con, "commit");

pg_close($con);

echo("<script type=\"text/javascript\">window.opener.location.href = 'fplusadm_hospital_mst.php?session=$session&hospital_id=$hospital_id';</script>");
echo("<script language=\"javascript\">self.close();</script>\n");
exit;

?>