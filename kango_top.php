<?php
require_once("about_comedix.php");
require_once("kango_common.ini");
require_once("show_select_values.ini");

//==============================
//前処理
//==============================
// ページ名
$fname = $_SERVER["PHP_SELF"];

// セッションのチェック
$session = qualify_session($_REQUEST["session"], $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限チェック
$checkauth = check_authority($session, $AUTH_NO_KANGO_USER, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// パラメータ
$is_postback = isset($_REQUEST["is_postback"]) ? $_REQUEST["is_postback"] : null;

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if ($con == "0") {
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//==============================
//ユーザーID
//==============================
$emp_id = get_emp_id($con, $session, $fname);


//==============================
//ポストバック時の処理
//==============================
if ($is_postback == "true") {
    if ($postback_mode == "bldg_changed") {
        $ward_cd = "";
        set_user_selected_bldg($con, $fname, $emp_id, $bldg_cd, $ward_cd);
    }
}

// 除外診療科
$out_sect_id_list = get_out_sect_id_list($con, $fname);

// 一般病床
$general_ward_divs = get_general_ward_divs($con, $fname);

//==============================
//棟-病棟-病室情報
//==============================
$bldg_ward_room_info = get_bldg_ward_room_info($con, $fname);

//==============================
//棟一覧を取得
//==============================
$bldg_list = null;
$bldg_list_cd = array();
foreach ($bldg_ward_room_info as $row) {
    if (!in_array($row['bldg_cd'], $bldg_list_cd)) {
        $bldg_list1 = null;
        $bldg_list1['bldg_cd'] = $row['bldg_cd'];
        $bldg_list1['bldg_name'] = $row['bldg_name'];
        $bldg_list[] = $bldg_list1;
        $bldg_list_cd[] = $row['bldg_cd'];
    }
}

if ($is_postback != "true") {
    $user_selected_bldg = get_user_selected_bldg($con, $fname, $emp_id);
    if ($user_selected_bldg != null) {
        $bldg_cd = $user_selected_bldg['bldg_cd'];
    }
    else {
        $bldg_cd = $bldg_list[0]['bldg_cd'];
    }
}

if ($bldg_cd == "") {
    echo("<script type=\"text/javascript\">alert('棟が登録されていません。');</script>");
    echo("<script type=\"text/javascript\">history.back();</script>");
    exit;
}


//==============================
//病棟一覧を取得
//==============================
$ward_list = null;
$ward_list_cd = array();
foreach ($bldg_ward_room_info as $row) {
    if ($row['bldg_cd'] == $bldg_cd) {
        if (!in_array($row['bldg_cd'] . '/' . $row['ward_cd'], $ward_list_cd)) {
            $ward_list1 = null;
            $ward_list1['ward_cd'] = $row['ward_cd'];
            $ward_list1['ward_name'] = $row['ward_name'];
            $ward_list[] = $ward_list1;
            $ward_list_cd[] = $row['bldg_cd'] . '/' . $row['ward_cd'];
        }
    }
}

//==============================
//日付、時間帯
//==============================
if ($is_postback != "true") {
    $target_date = date("Ymd");
    $target_time = get_now_time();

    $target_date_y = date("Y");
    $target_date_m = date("m");
    $target_date_d = date("d");
}
$target_date = $target_date_y . $target_date_m . $target_date_d;


//指定日、棟の患者の入棟情報
$all_inpt_info = get_all_inpt_info($con, $fname, $target_date, $target_date, "", false);
$wk_data = null;
foreach ($all_inpt_info as $all_inpt_info1) {
    if ($bldg_cd == $all_inpt_info1['bldg_cd']) {
        $wk_data[] = $all_inpt_info1;
    }
}
$bldg_date_all_inpt_info = $wk_data;


//==============================
//評価記録情報
//==============================
$sql = " select * from nlcs_eval_record where input_date = '{$target_date}' and input_time = {$target_time} and bldg_cd = {$bldg_cd} and not delete_flg";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$bldg_date_eval_record_info = pg_fetch_all_in_array($sel);



//==============================
//一覧表示情報
//==============================
$result_info_default = array(
    'ward_name' => '',
    'standard_name' => '',
    'pt_count1' => 0,
    'pt_count2' => 0,
    'state_0' => 0,
    'state_2' => 0,
    'state_3' => 0,
    'level_1' => 0,
    'level_2' => 0,
    'level_3' => 0,
    'level_4' => 0,
    'level_5' => 0,
    '7_1' => 0,
    'rate_7_1' => 0,
);

//全病棟
$ward_result_info_all = $result_info_default;
$ward_result_info_all['ward_name'] = "全病棟";
$ward_result_info_all['standard_name'] = "";
$ward_result_info_all['7_1'] = "";
$ward_result_info_all['rate_7_1'] = "";

//７対１基準病棟
$ward_result_info_7_1 = $result_info_default;
$ward_result_info_7_1['ward_name'] = "７対１基準病棟";
$ward_result_info_7_1['standard_name'] = "";

//10対１基準病棟
$ward_result_info_10_1 = $result_info_default;
$ward_result_info_10_1['ward_name'] = "10対１基準病棟";
$ward_result_info_10_1['standard_name'] = "";

//地域包括ケア病棟
$ward_result_info_chiiki = $result_info_default;
$ward_result_info_chiiki['ward_name'] = "地域包括ケア病棟";
$ward_result_info_chiiki['standard_name'] = "";

//各病棟
$ward_result_info = array();

//集計
foreach ($ward_list as $ward_list1) {
    //病棟コード
    $ward_cd = $ward_list1['ward_cd'];

    //病棟患者
    $ward_date_all_inpt_info = get_row_data_pg_fetch_all($bldg_date_all_inpt_info, 'ward_cd', $ward_cd);

    //入院患者数IIの算出
    $pt_count2 = 0;
    $pt_count3 = 0; // 地域包括ケア
    foreach ($ward_date_all_inpt_info as $ward_date_all_inpt_info1) {

        // 一般病床でなければカウントしない
        if (!in_array($ward_date_all_inpt_info1["ward_type"], $general_ward_divs)) {
            continue;
        }

        // 当日退院であればカウントしない
        if ($ward_date_all_inpt_info1["inpt_out_dt"] == $target_date && $ward_date_all_inpt_info1["inpt_in_dt"] != $target_date && $ward_date_all_inpt_info1["ref_table_name"] != "inptmove") {
            continue;
        }

        $pt_count3++;

        // 除外診療科であればカウントしない
        if (in_array($ward_date_all_inpt_info1["inpt_sect_id"], $out_sect_id_list)) {
            continue;
        }

        // 15歳以上でなければカウントしない
        $age = floor(($target_date - $ward_date_all_inpt_info1['birth']) / 10000);
        if ($age < 15) {
            continue;
        }

        $pt_count2++;
    }

    //病棟患者の評価記録
    //全体の記録のうち、病棟所属の患者の情報のみ抽出。
    $wk_data = array();

    foreach ($bldg_date_eval_record_info as $bldg_date_eval_record_info1) {
        $wk_pt_id = $bldg_date_eval_record_info1['pt_id'];
        if (get_row_data_pg_fetch_all($ward_date_all_inpt_info, 'ptif_id', $wk_pt_id) != null) {
            $wk_data[] = $bldg_date_eval_record_info1;
        }
    }
    $ward_date_eval_record_info = $wk_data;



    $ward_result = $result_info_default;

    //病棟名
    $ward_result['ward_name'] = $ward_list1['ward_name'];

    //施設基準名
    $ward_setting = get_ward_setting($con, $fname, $bldg_cd, $ward_cd);
    $standard_code = $ward_setting['standard_code'];
    $ward_result['standard_name'] = get_standard_name($con, $fname, $standard_code);

    //入院患者数
    $ward_result['pt_count1'] = count($ward_date_all_inpt_info);
    $ward_result['pt_count2'] = $pt_count2;
    $ward_result['pt_count3'] = $pt_count3;

    //未入力患者数
    $ward_result['state_0'] = count($ward_date_all_inpt_info) - count($ward_date_eval_record_info);

    //レベル、記録状態、７対１の集計
    foreach ($ward_date_eval_record_info as $ward_date_eval_record_info1) {
        $wk_pt_class_level = $ward_date_eval_record_info1['pt_class_level'];
        $wk_input_state = $ward_date_eval_record_info1['input_state'];
        $wk_pt_7_1_flg = $ward_date_eval_record_info1['pt_7_1_flg'];

        if ($wk_input_state == 1) {
            if ($wk_pt_class_level != "") {
                $ward_result['level_' . $wk_pt_class_level] ++;
            }
            if ($wk_pt_7_1_flg == "t") {
                $ward_result['7_1'] ++;
            }
        }
        elseif ($wk_input_state == 2 || $wk_input_state == 3) {
            $ward_result['state_' . $wk_input_state] ++;
        }
    }

    //７対１％
    if ($ward_result['pt_count2'] == 0) {
        $rate_7_1 = 0;
    }
    else {
        $rate_7_1 = 100 * $ward_result['7_1'] / $ward_result['pt_count2'];
    }
    $rate_7_1 = round($rate_7_1, 1); //小数点１桁で四捨五入
    $rate_7_1 = $rate_7_1 . "%";
    $ward_result['rate_7_1'] = $rate_7_1;


    //施設基準が７対１以外の場合は７対１情報をクリア
    if ($standard_code != 1 and $standard_code != 2 and $standard_code != 7) {
        $ward_result['7_1'] = "";
        $ward_result['rate_7_1'] = "";
    }

    $ward_result_info[$ward_cd] = $ward_result;

    //全病棟の集計
    $ward_result_info_all['pt_count1'] += $ward_result['pt_count1'];
    $ward_result_info_all['pt_count2'] += $ward_result['pt_count2'];
    $ward_result_info_all['state_0'] += $ward_result['state_0'];
    $ward_result_info_all['state_2'] += $ward_result['state_2'];
    $ward_result_info_all['state_3'] += $ward_result['state_3'];
    $ward_result_info_all['level_1'] += $ward_result['level_1'];
    $ward_result_info_all['level_2'] += $ward_result['level_2'];
    $ward_result_info_all['level_3'] += $ward_result['level_3'];
    $ward_result_info_all['level_4'] += $ward_result['level_4'];
    $ward_result_info_all['level_5'] += $ward_result['level_5'];

    //７対１基準病棟の集計
    if ($standard_code == 1) {
        $ward_result_info_7_1['pt_count1'] += $ward_result['pt_count1'];
        $ward_result_info_7_1['pt_count2'] += $ward_result['pt_count2'];
        $ward_result_info_7_1['state_0'] += $ward_result['state_0'];
        $ward_result_info_7_1['state_2'] += $ward_result['state_2'];
        $ward_result_info_7_1['state_3'] += $ward_result['state_3'];
        $ward_result_info_7_1['level_1'] += $ward_result['level_1'];
        $ward_result_info_7_1['level_2'] += $ward_result['level_2'];
        $ward_result_info_7_1['level_3'] += $ward_result['level_3'];
        $ward_result_info_7_1['level_4'] += $ward_result['level_4'];
        $ward_result_info_7_1['level_5'] += $ward_result['level_5'];
        $ward_result_info_7_1['7_1'] += $ward_result['7_1'];
    }

    //10対１基準病棟の集計
    if ($standard_code == 2) {
        $ward_result_info_10_1['pt_count1'] += $ward_result['pt_count1'];
        $ward_result_info_10_1['pt_count2'] += $ward_result['pt_count2'];
        $ward_result_info_10_1['state_0'] += $ward_result['state_0'];
        $ward_result_info_10_1['state_2'] += $ward_result['state_2'];
        $ward_result_info_10_1['state_3'] += $ward_result['state_3'];
        $ward_result_info_10_1['level_1'] += $ward_result['level_1'];
        $ward_result_info_10_1['level_2'] += $ward_result['level_2'];
        $ward_result_info_10_1['level_3'] += $ward_result['level_3'];
        $ward_result_info_10_1['level_4'] += $ward_result['level_4'];
        $ward_result_info_10_1['level_5'] += $ward_result['level_5'];
        $ward_result_info_10_1['7_1'] += $ward_result['7_1'];
    }

    //地域包括ケア病棟の集計
    if ($standard_code == 7) {
        $ward_result_info_chiiki['pt_count1'] += $ward_result['pt_count1'];
        $ward_result_info_chiiki['pt_count2'] += $ward_result['pt_count3'];
        $ward_result_info_chiiki['state_0'] += $ward_result['state_0'];
        $ward_result_info_chiiki['state_2'] += $ward_result['state_2'];
        $ward_result_info_chiiki['state_3'] += $ward_result['state_3'];
        $ward_result_info_chiiki['level_1'] += $ward_result['level_1'];
        $ward_result_info_chiiki['level_2'] += $ward_result['level_2'];
        $ward_result_info_chiiki['level_3'] += $ward_result['level_3'];
        $ward_result_info_chiiki['level_4'] += $ward_result['level_4'];
        $ward_result_info_chiiki['level_5'] += $ward_result['level_5'];
        $ward_result_info_chiiki['7_1'] += $ward_result['7_1'];
    }
}

//７対１基準病棟の７対１％
if ($ward_result_info_7_1['pt_count2'] == 0) {
    $ward_result_info_7_1['rate_7_1'] = 0;
}
else {
    $ward_result_info_7_1['rate_7_1'] = 100 * $ward_result_info_7_1['7_1'] / $ward_result_info_7_1['pt_count2'];
}
$ward_result_info_7_1['rate_7_1'] = round($ward_result_info_7_1['rate_7_1'], 1); //小数点１桁で四捨五入
$ward_result_info_7_1['rate_7_1'] = $ward_result_info_7_1['rate_7_1'] . "%";

//10対１基準病棟の10対１％
if ($ward_result_info_10_1['pt_count2'] == 0) {
    $ward_result_info_10_1['rate_7_1'] = 0;
}
else {
    $ward_result_info_10_1['rate_7_1'] = 100 * $ward_result_info_10_1['7_1'] / $ward_result_info_10_1['pt_count2'];
}
$ward_result_info_10_1['rate_7_1'] = round($ward_result_info_10_1['rate_7_1'], 1); //小数点１桁で四捨五入
$ward_result_info_10_1['rate_7_1'] = $ward_result_info_10_1['rate_7_1'] . "%";

//地域包括ケア病棟の％
if ($ward_result_info_chiiki['pt_count2'] == 0) {
    $ward_result_info_chiiki['rate_7_1'] = 0;
}
else {
    $ward_result_info_chiiki['rate_7_1'] = 100 * $ward_result_info_chiiki['7_1'] / $ward_result_info_chiiki['pt_count2'];
}
$ward_result_info_chiiki['rate_7_1'] = round($ward_result_info_chiiki['rate_7_1'], 1); //小数点１桁で四捨五入
$ward_result_info_chiiki['rate_7_1'] = $ward_result_info_chiiki['rate_7_1'] . "%";


//========================================================================================================================================================================================================
// HTML出力
//========================================================================================================================================================================================================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?eh($KANGO_TITLE);?> | お知らせ</title>


<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/popup.js"></script>
<script type="text/javascript">

function initPage()
{
}

function bldg_changed(bldg_cd)
{
    document.mainform.postback_mode.value = "bldg_changed";
    document.mainform.submit();
}

function date_time_changed()
{
    document.mainform.postback_mode.value = "date_time_changed";
    document.mainform.submit();
}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}

.list_in {border-collapse:collapse;}
.list_in td {border:#FFFFFF solid 0px;}
</style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">

<!-- ヘッダー START -->
<?
show_kango_header($session,$fname);
?>
<!-- ヘッダー END -->
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td>

<!-- 全体 START -->
<form name="mainform" action="<?=$fname?>" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="is_postback" value="true">
<input type="hidden" name="postback_mode" value="">





<!-- 表示条件 START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="250">

<!-- 棟選択 START -->
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
事業所（棟）：<select name="bldg_cd" onchange="bldg_changed(this.value)">
<?
foreach($bldg_list as $bldg_list1)
{
    $tmp_bldg_cd = $bldg_list1['bldg_cd'];
    $tmp_bldg_name = $bldg_list1['bldg_name'];
?>
<option value="<?=$tmp_bldg_cd?>" <?if($tmp_bldg_cd == $bldg_cd){echo("selected");}?>><?=h($tmp_bldg_name)?></option>
<?
}
?>
</select>
</font>
<!-- 棟選択 END -->

</td>

<td width="250">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">

<!-- 日付選択 START -->
日付：<select id='target_date_y' name='target_date_y' onchange="date_time_changed()">
<?
$date_y_min = 2008;
$date_y_max = date("Y");
for ($tmp_y = $date_y_min; $tmp_y <= $date_y_max; $tmp_y++)
{
    ?>
    <option value="<?=$tmp_y?>" <?if($tmp_y == $target_date_y){echo('selected');}?>><?=$tmp_y?></option>
    <?
}
?>
</select>年
<select id='target_date_m' name='target_date_m' onchange="date_time_changed()">
<?
for ($i = 1; $i <= 12; $i++) {
    if ($target_date_y == "2008" && $i <= 3) {
        continue;
    }
    if ($target_date_y == date("Y") && $i > date("m")) {
        continue;
    }
    $tmp_m = sprintf("%02d", $i);
?>
        <option value="<?=$tmp_m?>" <?if ($tmp_m == $target_date_m) {
        echo('selected');
    }?>><?=$tmp_m?></option>
    <?
}
?>
</select>月
<select id='target_date_d' name='target_date_d' onchange="date_time_changed()">
<?
show_select_days($target_date_d);
?>
</select>日
<!-- 日付選択 END -->




</font>
</td>


<td width="250">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">

<!-- 時間帯選択 START -->
時間帯：<select id='target_time' name='target_time' onchange="date_time_changed()">
<option value="1" <? if($target_time == 1){echo("selected");}?>>深夜帯</option>
<option value="2" <? if($target_time == 2){echo("selected");}?>>日勤帯</option>
<option value="3" <? if($target_time == 3){echo("selected");}?>>準夜帯</option>
</select>
<!-- 時間帯選択 END -->


</font>
</td>

<td>
&nbsp;
</td>

</tr>
</table>
<!-- 表示条件 END -->

</form>


<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td><img src="img/spacer.gif" width="1" height="5" alt=""></td></tr>
</table>



<!-- 機能タイトル START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="250">
<b>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
看護必要度
</font>
</b>
</td>
<td>
&nbsp;
</td>
</tr>
</table>
<!-- 機能タイトル END -->



<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td><img src="img/spacer.gif" width="1" height="5" alt=""></td></tr>
</table>



<!-- 病棟一覧 START -->
<table width='800' border='0' cellspacing='0' cellpadding='1' class="list">

<tr>
<td bgcolor='#bdd1e7' rowspan='2' align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>病棟</font></td>
<td bgcolor='#bdd1e7' rowspan='2' width='100' align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>入院基準</font></td>
<td bgcolor='#bdd1e7' rowspan='2' width='50' align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>入院<br>患者数I<br>(注1)</font></td>
<td bgcolor='#bdd1e7' rowspan='2' width='50' align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>入院<br>患者数II<br>(注2)</font></td>
<td bgcolor='#bdd1e7' style="padding:0px" rowspan='2' width='1'><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td bgcolor='#bdd1e7' colspan='5' align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>患者分類レベル</font></td>
<td bgcolor='#bdd1e7' style="padding:0px" rowspan='2' width='1'><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td bgcolor='#bdd1e7' colspan='3' align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>未確定</font></td>
<td bgcolor='#bdd1e7' style="padding:0px" rowspan='2' width='1'><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td bgcolor='#bdd1e7' colspan='2' align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>7対1/10対1<br>/地域包括ケア<br>条件患者</font></td>
</tr>
<tr>
<td bgcolor='#bdd1e7' width='40' align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>1</font></td>
<td bgcolor='#bdd1e7' width='40' align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>2</font></td>
<td bgcolor='#bdd1e7' width='40' align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>3</font></td>
<td bgcolor='#bdd1e7' width='40' align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>4</font></td>
<td bgcolor='#bdd1e7' width='40' align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>5</font></td>
<td bgcolor='#bdd1e7' width='40' align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>未入力</font></td>
<td bgcolor='#bdd1e7' width='40' align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>保留</font></td>
<td bgcolor='#bdd1e7' width='40' align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>不在</font></td>
<td bgcolor='#bdd1e7' width='40' align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>患者数</font></td>
<td bgcolor='#bdd1e7' width='40' align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>比率</font></td>
</tr>

<?
foreach($ward_list as $ward_list1)
{
    //病棟名
    $ward_cd = $ward_list1['ward_cd'];

    //記録集計結果
    $ward_result = $ward_result_info[$ward_cd];

    ?>
    <tr>
    <td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=h($ward_result['ward_name'])?></font></td>
    <td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=h($ward_result['standard_name'])?></font></td>
    <td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['pt_count1']?></font></td>
    <td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['pt_count2']?></font></td>
    <td style="padding:0px"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
    <td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['level_1']?></font></td>
    <td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['level_2']?></font></td>
    <td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['level_3']?></font></td>
    <td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['level_4']?></font></td>
    <td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['level_5']?></font></td>
    <td style="padding:0px"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
    <td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['state_0']?></font></td>
    <td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['state_2']?></font></td>
    <td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['state_3']?></font></td>
    <td style="padding:0px"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
    <td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['7_1']?></font></td>
    <td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['rate_7_1']?></font></td>
    </tr>
    <?
}
?>

<tr>
<td style="padding:0px"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td style="padding:0px"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td style="padding:0px"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td style="padding:0px"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td style="padding:0px"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td style="padding:0px"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td style="padding:0px"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td style="padding:0px"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td style="padding:0px"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td style="padding:0px"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td style="padding:0px"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td style="padding:0px"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td style="padding:0px"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td style="padding:0px"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td style="padding:0px"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td style="padding:0px"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td style="padding:0px"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>

<?
$ward_result = $ward_result_info_all;
?>
<tr>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=h($ward_result['ward_name'])?></font></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=h($ward_result['standard_name'])?></font></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['pt_count1']?></font></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['pt_count2']?></font></td>
<td style="padding:0px"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['level_1']?></font></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['level_2']?></font></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['level_3']?></font></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['level_4']?></font></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['level_5']?></font></td>
<td style="padding:0px"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['state_0']?></font></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['state_2']?></font></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['state_3']?></font></td>
<td style="padding:0px"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['7_1']?></font></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['rate_7_1']?></font></td>
</tr>

<?
$ward_result = $ward_result_info_7_1;
?>
<tr>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=h($ward_result['ward_name'])?></font></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=h($ward_result['standard_name'])?></font></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['pt_count1']?></font></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['pt_count2']?></font></td>
<td style="padding:0px"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['level_1']?></font></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['level_2']?></font></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['level_3']?></font></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['level_4']?></font></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['level_5']?></font></td>
<td style="padding:0px"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['state_0']?></font></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['state_2']?></font></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['state_3']?></font></td>
<td style="padding:0px"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['7_1']?></font></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['rate_7_1']?></font></td>
</tr>

<?
$ward_result = $ward_result_info_10_1;
?>
<tr>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=h($ward_result['ward_name'])?></font></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=h($ward_result['standard_name'])?></font></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['pt_count1']?></font></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['pt_count2']?></font></td>
<td style="padding:0px"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['level_1']?></font></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['level_2']?></font></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['level_3']?></font></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['level_4']?></font></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['level_5']?></font></td>
<td style="padding:0px"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['state_0']?></font></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['state_2']?></font></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['state_3']?></font></td>
<td style="padding:0px"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['7_1']?></font></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['rate_7_1']?></font></td>
</tr>

<?
$ward_result = $ward_result_info_chiiki;
?>
<tr>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=h($ward_result['ward_name'])?></font></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=h($ward_result['standard_name'])?></font></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['pt_count1']?></font></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['pt_count2']?></font></td>
<td style="padding:0px"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['level_1']?></font></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['level_2']?></font></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['level_3']?></font></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['level_4']?></font></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['level_5']?></font></td>
<td style="padding:0px"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['state_0']?></font></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['state_2']?></font></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['state_3']?></font></td>
<td style="padding:0px"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['7_1']?></font></td>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$ward_result['rate_7_1']?></font></td>
</tr>

</table>
<!-- 病棟一覧 END -->


<ol style="margin-left:0;padding-left:0;list-style-type:none;">
<li style="margin-left:0;padding-left:0;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">注1）全病棟区分、全診療科、15歳未満、当日退院分を含む人数です。</font></li>
<li style="margin-left:0;padding-left:0;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">注2）一般病床に入院中の患者で、産科と15歳未満の小児および当日退院患者を除外した人数です。一般病床と産科の設定は、管理画面から行ってください。</font></li>
<li style="margin-left:0;padding-left:0;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">地域包括ケア病棟の場合は、一般病床に入院中の患者で、当日退院患者数を除外した人数です。</font></li>
</ol>


<!-- 全体 END -->

</td>
</tr>
</table>
</body>
</html>
<?
pg_close($con);
?>
