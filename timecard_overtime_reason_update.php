<?php
require_once("about_comedix.php");

$fname = $PHP_SELF;

$reason_id = $_POST["addReasonId"];
$reason_name = $_POST["addReasonName"];
$noapply_flag = $_POST["addNoApplyFlag"];
$wktm_group = $_POST["addWktmGroup"];
$hide_flag = $_POST["addHideFlag"];

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 残業理由IDを採番
$sql = "select max(reason_id) from ovtmrsn";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$id = intval(pg_fetch_result($sel, 0, 0)) + 1;

// 残業理由の新規登録と更新を行う
$list_reason = array(); 
ksort($reason_id);
foreach ($reason_id as $key=> $item){
	
	$tmp_name = $reason_name[$key];
	$tmp_noapply = $noapply_flag[$key];
	$wktmgrp = (empty($wktm_group[$key])) ? NULL : $wktm_group[$key];
	$tmp_hide = $hide_flag[$key];
	
	if ($item == "new_reason_id" || empty($item)){
		// 残業理由を登録
		$sql = "insert into ovtmrsn (reason_id, reason, del_flg, over_no_apply_flag, tmcd_group_id, sort, hide_flag) values (";
		$wktmgrp = (empty($wktm_group[$key])) ? NULL : $wktm_group[$key];
		$content = array($id, pg_escape_string($tmp_name), "f", $tmp_noapply, $wktmgrp, $key+1, $tmp_hide);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$list_reason[$id] = $id;
		$id++;
	}else{
		// 残業理由を更新
		$sql = "update ovtmrsn set";
		$set = array("reason", "over_no_apply_flag", "tmcd_group_id", "sort", "hide_flag");
		
		$setvalue = array(pg_escape_string($tmp_name), $tmp_noapply, $wktmgrp, $key+1, $tmp_hide);
		$cond = "where reason_id = $item";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$list_reason[$item] = $item;
	}
}


// 削除された項目を洗い出す
$sql = "select reason_id from ovtmrsn";
if (!empty($list_reason)){
	$str_id = implode("','", $list_reason);
	$cond = "where reason_id not in ('$str_id') and del_flg <> 't'";
}else{
	$cond = "where del_flg <> 't'";
}
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

$del_reason = array();
while($row = pg_fetch_array($sel)){
	$del_reason[] = $row["reason_id"];
}
// 削除フラグ更新を行う
if (count($del_reason) > 0){
	$str_del_id = implode("','", $del_reason);
	
	$sql = "update ovtmrsn set";
	$set = array("del_flg");
	
	$setvalue = array('t');
	$cond = "where reason_id in ('$str_del_id')";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}


// データベース接続を閉じる
pg_close($con);

// 一覧画面に遷移
echo("<script type=\"text/javascript\">location.href = 'timecard_overtime_reason.php?session=$session';</script>");
?>
