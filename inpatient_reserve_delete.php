<?
require("about_authority.php");
require("about_session.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションの開始
pg_query($con, "begin");

foreach ($del_pt as $tmp_ptif_id) {

	// 入院予定情報の削除
	$sql = "delete from inptres";
	$cond = "where ptif_id = '$tmp_ptif_id'";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// 担当者情報の削除
	$sql = "delete from inptopres";
	$cond = "where ptif_id = '$tmp_ptif_id'";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// トランザクションのコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 入院予定患者一覧画面を再表示
echo("<script type=\"text/javascript\">location.href = 'inpatient_reserve_list.php?session=$session&view=$view&initial=$initial&bldgwd=$bldgwd&list_sect_id=$list_sect_id&list_year=$list_year&list_month=$list_month&list_day=$list_day&list_year2=$list_year2&list_month2=$list_month2&list_day2=$list_day2&order=$order&page=$page'</script>");
?>
