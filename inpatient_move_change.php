<?
require("about_session.php");
require("about_authority.php");
require("inpatient_common.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 移転日時の編集
$date = "$mv_yr$mv_mon$mv_day";
$time = sprintf("%02d%02d", $mv_hr, $mv_min);
list($bldg_cd, $ward_cd) = split("-", $ward);

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// 本人の最新転床にならない場合、システムエラーとする
$sql = "select move_dt, move_tm from inptmove";
$cond = "where ptif_id = '$pt_id' and move_cfm_flg = 't' and move_del_flg = 'f' and (move_dt > '$date' or (move_dt = '$date' and move_tm >= '$time')) order by move_dt desc, move_tm desc";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if (pg_num_rows($sel) > 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 移転元情報を取得
$sql = "select inpt_enti_id, bldg_cd, ward_cd, ptrm_room_no, inpt_bed_no, inpt_sect_id, dr_id, nurse_id from inptmst";
$cond = "where ptif_id = '$pt_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$from_enti_id = pg_fetch_result($sel, 0, "inpt_enti_id");
$from_bldg_cd = pg_fetch_result($sel, 0, "bldg_cd");
$from_ward_cd = pg_fetch_result($sel, 0, "ward_cd");
$from_ptrm_room_no = pg_fetch_result($sel, 0, "ptrm_room_no");
$from_bed_no = pg_fetch_result($sel, 0, "inpt_bed_no");
$from_sect_id = pg_fetch_result($sel, 0, "inpt_sect_id");
$from_dr_id = pg_fetch_result($sel, 0, "dr_id");
$from_nurse_id = pg_fetch_result($sel, 0, "nurse_id");

// 移転先情報を取得
$sql = "select ptif_id, inpt_sect_id, dr_id, nurse_id from inptmst";
$cond = "where inpt_enti_id = '$enti' and bldg_cd = '$bldg_cd' and ward_cd = '$ward_cd' and ptrm_room_no = '$ptrm' and inpt_bed_no = '$bed'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$target_pt_id = pg_fetch_result($sel, 0, "ptif_id");
$target_sect_id = pg_fetch_result($sel, 0, "inpt_sect_id");
$target_dr_id = pg_fetch_result($sel, 0, "dr_id");
$target_nurse_id = pg_fetch_result($sel, 0, "nurse_id");

// 交換対象患者の最新転床にならない場合、システムエラーとする
$sql = "select move_dt, move_tm from inptmove";
$cond = "where ptif_id = '$target_pt_id' and move_cfm_flg = 't' and move_del_flg = 'f' and (move_dt > '$date' or (move_dt = '$date' and move_tm >= '$time')) order by move_dt desc, move_tm desc";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if (pg_num_rows($sel) > 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 未確定の移転情報を削除
delete_move_reservation_data($con, $pt_id, $fname);
if (strcmp($pt_id, $target_pt_id) != 0) {
	delete_move_reservation_data($con, $target_pt_id, $fname);
}

// 入院状況レコードを更新
update_inpatient_condition($con, $pt_id, $date, $time, "3", $session, $fname);
if (strcmp($pt_id, $target_pt_id) != 0) {
	update_inpatient_condition($con, $target_pt_id, $date, $time, "3", $session, $fname);
}

// 移転情報を作成
create_move_data($con, $pt_id, $from_enti_id, $from_bldg_cd, $from_ward_cd, $from_ptrm_room_no, $from_bed_no, $from_sect_id, $from_dr_id, $from_nurse_id, $enti, $bldg_cd, $ward_cd, $ptrm, $bed, $sect, $doc, $nurse, $date, $time, $reason, $fname);
if (strcmp($pt_id, $target_pt_id) != 0) {
	create_move_data($con, $target_pt_id, $enti, $bldg_cd, $ward_cd, $ptrm, $bed, $sect, $doc, $nurse, $from_enti_id, $from_bldg_cd, $from_ward_cd, $from_ptrm_room_no, $from_bed_no, $target_sect_id, $target_dr_id, $target_nurse_id, $date, $time, "", $fname);
}

// 入院情報を更新
update_inpatient_data($con, $pt_id, $enti, $bldg_cd, $ward_cd, $ptrm, $bed, $sect, $doc, $nurse, $fname);
if (strcmp($pt_id, $target_pt_id) != 0) {
	update_inpatient_data($con, $target_pt_id, $from_enti_id, $from_bldg_cd, $from_ward_cd, $from_ptrm_room_no, $from_bed_no, $target_sect_id, $target_dr_id, $target_nurse_id, $fname);
}

// トランザクションをコミット
pg_query($con, "commit");
//pg_query($con, "rollback");

// データベース接続を閉じる
pg_close($con);

// 親画面をリフレッシュし、自画面を閉じる
echo("<script type=\"text/javascript\">opener.location.reload(); self.close();</script>\n");

// 未確定の移転情報を削除
function delete_move_reservation_data($con, $pt_id, $fname) {
	$sql = "select move_dt from inptmove";
	$cond = "where ptif_id = '$pt_id' and move_cfm_flg = 'f' and move_del_flg = 'f'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$sql = "delete from inptcond";
		$cond = "where ptif_id = '$pt_id' and date = '{$row["move_dt"]}' and hist_flg = 'f'";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
	$sql = "update inptmove set";
	$set = array("move_del_flg");
	$setvalue = array("t");
	$cond = "where ptif_id = '$pt_id' and move_cfm_flg = 'f' and move_del_flg = 'f'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// 移転情報を作成
function create_move_data($con, $pt_id, $from_enti_id, $from_bldg_cd, $from_ward_cd, $from_ptrm_room_no, $from_bed_no, $from_sect_id, $from_dr_id, $from_nurse_id, $to_enti_id, $to_bldg_cd, $to_ward_cd, $to_ptrm_room_no, $to_bed_no, $to_sect_id, $to_dr_id, $to_nurse_id, $date, $mv_tm, $reason, $fname) {
	$from_nurse_id = intval($from_nurse_id);
	$to_nurse_id = intval($to_nurse_id);

	$sql = "insert into inptmove (ptif_id, from_enti_id, from_bldg_cd, from_ward_cd, from_ptrm_room_no, from_bed_no, from_sect_id, from_dr_id, from_nurse_id, to_enti_id, to_bldg_cd, to_ward_cd, to_ptrm_room_no, to_bed_no, to_sect_id, to_dr_id, to_nurse_id, move_dt, move_tm, move_reason, move_cfm_flg, move_del_flg, updated) values (";
	$content = array($pt_id, $from_enti_id, $from_bldg_cd, $from_ward_cd, $from_ptrm_room_no, $from_bed_no, $from_sect_id, $from_dr_id, $from_nurse_id, $to_enti_id, $to_bldg_cd, $to_ward_cd, $to_ptrm_room_no, $to_bed_no, $to_sect_id, $to_dr_id, $to_nurse_id, $date, $mv_tm, $reason, "t", "f", date("YmdHis"));
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// 入院情報を更新
function update_inpatient_data($con, $pt_id, $enti, $bldg_cd, $ward_cd, $ptrm, $bed, $sect, $doc, $nurse, $fname) {
	$nurse = intval($nurse);

	$sql = "update inptmst set";
	$set = array("inpt_enti_id", "bldg_cd", "ward_cd", "ptrm_room_no", "inpt_bed_no", "inpt_sect_id", "dr_id", "nurse_id");
	$setvalue = array($enti, $bldg_cd, $ward_cd, $ptrm, $bed, $sect, $doc, $nurse);
	$cond = "where ptif_id = '$pt_id'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}
?>
