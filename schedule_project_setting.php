<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix スケジュール | 委員会・WG設定</title>
<?
require("about_session.php");
require("about_authority.php");
require("show_schedule_project_list.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 2, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = " where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

if ($schd_show_old_pjt == "") {

	// オプション設定情報を取得
	$sql = "select schd_show_old_pjt from option";
	$cond = "where emp_id = '$emp_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$schd_show_old_pjt = pg_fetch_result($sel, 0, "schd_show_old_pjt");
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function changeOption() {
	location.href = 'schedule_project_setting.php?session=<? echo($session); ?>&schd_show_old_pjt='.concat(document.pjt.schd_show_old_pjt.value);
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<form name="pjt" action="schedule_project_set.php" method="post">
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>委員会・WG設定</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="600" border="0" cellspacing="0" cellpadding="2">


<tr height="22">
	<td align="left">
		<input type="button" value="全チェック" onclick="onCheck();">
		<input type="button" value="全チェックOFF" onclick="offCheck();">
		<input type="button" value="担当委員会WGチェック" onclick="onMemberCheck();">
	</td>

<td align="right">
<select name="schd_show_old_pjt" onchange="changeOption();">
<option value="f"<? if ($schd_show_old_pjt == "f") {echo(" selected");} ?>>終了済みを表示しない
<option value="t"<? if ($schd_show_old_pjt == "t") {echo(" selected");} ?>>終了済みも表示する
</select>
</td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="26">
<td width="30" bgcolor="#ccffcc"></td>
<td width="230" bgcolor="#ccffcc"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">委員会名</font></td>
<td width="230" bgcolor="#ccffcc"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">WG名</font></td>
<td width="140" bgcolor="#ccffcc"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">責任者</font></td>
</tr>
<? $arr_member = show_project_list($con, $emp_id, $schd_show_old_pjt, $session, $fname); ?>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr>
<td align="right"><input type="submit" value="更新">

</td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">


<script type="text/javascript">
function onCheck()
{
	for(i = 0; i < document.pjt.elements.length; i++)
	{
		if(document.pjt.elements[i].name == "pjt_ids[]")
		{
			document.pjt.elements[i].checked = true;	
		}
	}
}

function offCheck()
{
	for(i = 0; i < document.pjt.elements.length; i++)
	{
		if(document.pjt.elements[i].name == "pjt_ids[]")
		{
			document.pjt.elements[i].checked = false;	
		}
	}
}

function onMemberCheck()
{
	
	for(i = 0; i < document.pjt.elements.length; i++)
	{
		if(document.pjt.elements[i].name == "pjt_ids[]")
		{
<?
			foreach ($arr_member as $mem_pjt_id) 
			{
?>			
				if(document.pjt.elements[i].value == "<?=$mem_pjt_id?>")
				{
					document.pjt.elements[i].checked = true;	
				}
<?			
			}
?>
		}
	}


}


</script>

</form>
</center>
</body>
<? pg_close($con); ?>
</html>
