<?
ini_set("max_execution_time", 0);

require_once("about_postgres.php");
require_once("show_class_name.ini");
require_once("get_values.ini");
require_once("passwd_change_common.ini");
require_once("employee_auth_common.php");
//XX require_once("community/mainfile.php");
require_once("webmail/config/config.php");
require_once("webmail_quota_functions.php");

$fname = $PHP_SELF;

// ファイルが正常にアップロードされたかどうかチェック
$uploaded = false;
if (array_key_exists("csvfile", $_FILES)) {
	if ($_FILES["csvfile"]["error"] == 0 && $_FILES["csvfile"]["size"] > 0) {
		$uploaded = true;
	}
}
if (!$uploaded) {
	echo("<script type=\"text/javascript\">alert('ファイルのアップロードに失敗しました。');</script>\n");
	echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register2.php?session=$session&result=f&encoding=$encoding';</script>");
	exit;
}

// 文字コードの設定
switch ($encoding) {
case "1":
	$file_encoding = "SJIS";
	break;
case "2":
	$file_encoding = "EUC-JP";
	break;
case "3":
	$file_encoding = "UTF-8";
	break;
default:
	exit;
}

// 各グループのIDを取得
$auth_group_id = $_POST["auth_group_id"];
$disp_group_id = $_POST["disp_group_id"];
$menu_group_id = $_POST["menu_group_id"];

// EUC-JPで扱えない文字列は「・」に変換するよう設定
mb_substitute_character(0x30FB);

// データベースに接続
$con = connect2db($fname);

// emp_idの取得
$new_emp_id = get_new_emp_id($con, $fname);

// トランザクションを開始
pg_query($con, "begin");

// 環境設定情報を取得
$sql = "select site_id, use_cyrus from config";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$site_id = pg_fetch_result($sel, 0, "site_id");
$use_cyrus = pg_fetch_result($sel, 0, "use_cyrus");

// 組織情報を取得
$arr_class_name = get_class_name_array($con, $fname);



// 表示グループの設定情報を取得
if($disp_group_id != ""){
	$sql = "select * from dispgroup";
	$cond = "where group_id = $disp_group_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
		exit;
	}

	$disp_groups = pg_fetch_assoc($sel);

}

// メニューグループの設定情報を取得
if($menu_group_id != ""){
	$sql = "select * from menugroup";
	$cond = "where group_id = $menu_group_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
		exit;
	}

	$menu_groups = pg_fetch_assoc($sel);

}

// パスワードの長さ
$pass_length = get_pass_length($con, $fname);

// カラム数
$column_count = 12 + intval($arr_class_name[4]);
if ($site_id != "") {
	$column_count++;
}

// CSVデータを配列に格納
$employees = array();
$employee_no = 1;
$lines = file($_FILES["csvfile"]["tmp_name"]);
foreach ($lines as $line) {

	// 文字コードを変換
	$line = trim(mb_convert_encoding($line, "EUC-JP", $file_encoding));

	// EOFを削除
	$line = str_replace(chr(0x1A), "", $line);

	// 空行は無視
	if ($line == "") {
		continue;
	}

	// カラム数チェック
	$employee_data = split(",", $line);
	if (count($employee_data) != $column_count) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('カラム数が不正です。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register2.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}

	// 登録値の編集
	$i = 0;
	$employee = array();
	$employee["personal_id"] = trim($employee_data[$i++]);
	$employee["lt_nm"] = trim($employee_data[$i++]);
	$employee["ft_nm"] = trim($employee_data[$i++]);
	$employee["lt_kana_nm"] = mb_convert_kana(trim($employee_data[$i++]), "HVc");
	$employee["ft_kana_nm"] = mb_convert_kana(trim($employee_data[$i++]), "HVc");
	$employee["id"] = trim($employee_data[$i++]);
	$employee["pass"] = trim($employee_data[$i++]);
	if ($site_id != "") {
		$employee["mail_id"] = trim($employee_data[$i++]);
	}
	$employee["sex"] = trim($employee_data[$i++]);
	$employee["birth"] = trim($employee_data[$i++]);
	$employee["entry"] = trim($employee_data[$i++]);
	$employee["cls_nm"] = trim($employee_data[$i++]);
	$employee["atrb_nm"] = trim($employee_data[$i++]);
	$employee["dept_nm"] = trim($employee_data[$i++]);
	if ($arr_class_name[4] == "4") {
		$employee["room_nm"] = trim($employee_data[$i++]);
	}
	$employee["status_nm"] = trim($employee_data[$i++]);
	$employee["job_nm"] = trim($employee_data[$i++]);

	$employees[$employee_no] = $employee;
	$employee_no++;
}

// 職員データを登録
foreach ($employees as $employee_no => $employee_data) {
	// emp_idの取得
	$new_emp_id = get_new_emp_id($con, $fname);

	insert_employee_data($con, $employee_no, $employee_data, $arr_class_name, $site_id, $session, $fname, $use_cyrus);

	// 権限グループの更新
//XX 	$con_mysql = mysql_connect(XOOPS_DB_HOST, XOOPS_DB_USER, XOOPS_DB_PASS);
//XX	auth_change_group_of_employee($con, $con_mysql, $new_emp_id, $auth_group_id, $fname);
	auth_change_group_of_employee($con, $new_emp_id, $auth_group_id, $fname);

	// 表示設定の更新（表示グループを設定している場合）
	if($disp_group_id != ""){

		// 表示設定更新
		$sql = "update option set";
		$set = array("default_page", "font_size", "top_mail_flg", "top_ext_flg", "top_event_flg", "top_schd_flg", "top_info_flg", "top_task_flg", "top_msg_flg", "top_aprv_flg", "top_schdsrch_flg", "top_lib_flg", "top_bbs_flg", "top_memo_flg", "top_link_flg", "top_intra_flg", "bed_info", "schd_type", "top_wic_flg", "top_fcl_flg", "top_inci_flg", "top_cas_flg", "top_fplus_flg", "top_jnl_flg", "top_free_text_flg", "top_manabu_flg", "top_workflow_flg", "top_ccusr1_flg", "disp_group_id");
		$setvalue = array($disp_groups["default_page"], $disp_groups["font_size"], $disp_groups["top_mail_flg"], $disp_groups["top_ext_flg"], $disp_groups["top_event_flg"], $disp_groups["top_schd_flg"], $disp_groups["top_info_flg"], $disp_groups["top_task_flg"], $disp_groups["top_msg_flg"], $disp_groups["top_aprv_flg"], $disp_groups["top_schdsrch_flg"], $disp_groups["top_lib_flg"], $disp_groups["top_bbs_flg"], $disp_groups["top_memo_flg"], $disp_groups["top_link_flg"], $disp_groups["top_intra_flg"], $disp_groups["bed_info"], $disp_groups["schd_type"], $disp_groups["top_wic_flg"], $disp_groups["top_fcl_flg"], $disp_groups["top_inci_flg"], $disp_groups["top_cas_flg"], $disp_groups["top_fplus_flg"], $disp_groups["top_jnl_flg"], $disp_groups["top_free_text_flg"], $disp_groups["top_manabu_flg"], $disp_groups["top_workflow_flg"], $disp_groups["top_ccusr1_flg"], $disp_group_id);
		$cond = "where emp_id = '$new_emp_id'";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// メニューグループを設定している場合、メニューを設定
	if($menu_group_id != ""){

		// サイドメニュー更新
		$sql = "update option set";
		$set = array("sidemenu_show_flg","sidemenu_position");
		$setvalue = array($menu_groups["sidemenu_show_flg"], $menu_groups["sidemenu_position"]);
		$cond = "where emp_id = '$new_emp_id'";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// メニュー設定
		for ($i = 1; $i <= 12; $i++){
			$menu_num = 'menu_id_'.$i;
			if($menu_groups[$menu_num] == "") break;
			$sql = "insert into headermenu(emp_id, menu_id, menu_order) values(";
			$content = array($new_emp_id, $menu_groups[$menu_num], $i);
			$ins = insert_into_table($con, $sql, $content, $fname);
			if ($ins == 0) {
				pg_query($con,"rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}

		// menu_group_idの登録
		$sql = "update option set";
		$set = array("menu_group_id");
		$setvalue = array($menu_group_id);
		$cond = "where emp_id = '$new_emp_id'";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
}

// ライセンスのチェック
require_once("license_check_common.php");
$result = check_license($con, $fname);
if (!$result["ok"]) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('{$result["message"]}');</script>\n");
	echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register2.php?session=$session&result=f&encoding=$encoding';</script>");
	exit;
}

// コミュニティサイトデータを登録
//XX $con_mysql = mysql_connect(XOOPS_DB_HOST, XOOPS_DB_USER, XOOPS_DB_PASS);
//XX mysql_select_db(XOOPS_DB_NAME, $con_mysql);
//XX foreach ($employees as $employee_data) {
//XX 	insert_community_data($con_mysql, $con, $employee_data, $domain);
//XX }


// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);
//XX mysql_close($con_mysql);

if ($use_cyrus == "t") {

	// メールアカウントの作成
	$dir = getcwd();
	foreach ($employees as $employee_data) {
		make_mail_account($employee_data, $site_id, $dir, $imapServerAddress);
	}
}

// 成功画面を表示
echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register2.php?session=$session&result=t&encoding=$encoding';</script>");

// 職員データを登録
function insert_employee_data($con, $employee_no, $employee_data, $arr_class_name, $site_id, $session, $fname, $use_cyrus) {
	global $pass_length;
	$personal_id = $employee_data["personal_id"];
	$lt_nm = $employee_data["lt_nm"];
	$ft_nm = $employee_data["ft_nm"];
	$lt_kana_nm = $employee_data["lt_kana_nm"];
	$ft_kana_nm = $employee_data["ft_kana_nm"];
	$id = $employee_data["id"];
	$pass = $employee_data["pass"];
	$mail_id = $employee_data["mail_id"];
	$sex = $employee_data["sex"];
	$birth = $employee_data["birth"];
	$entry = $employee_data["entry"];
	$cls_nm = $employee_data["cls_nm"];
	$atrb_nm = $employee_data["atrb_nm"];
	$dept_nm = $employee_data["dept_nm"];
	$room_nm = $employee_data["room_nm"];
	$status_nm = $employee_data["status_nm"];
	$job_nm = $employee_data["job_nm"];

	// 入力チェック
	if (strlen($personal_id) > 12) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('職員IDが長すぎます。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register2.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	$sql = "select count(*) from empmst";
	$cond = "where emp_personal_id = '$personal_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (intval(pg_fetch_result($sel, 0, 0)) > 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('指定された職員IDは既に使用されています。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register2.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if ($lt_nm == "") {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('苗字（漢字）を入力してください。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register2.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if (strlen($lt_nm) > 20) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('苗字（漢字）が長すぎます。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register2.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if ($ft_nm == "") {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('名前（漢字）を入力してください。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register2.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if (strlen($ft_nm) > 20) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('名前（漢字）が長すぎます。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register2.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if ($lt_kana_nm == "") {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('苗字（ひらがな）を入力してください。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register2.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if (strlen($lt_kana_nm) > 20) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('苗字（ひらがな）が長すぎます。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register2.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if ($ft_kana_nm == "") {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('名前（ひらがな）を入力してください。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register2.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if (strlen($ft_kana_nm) > 20) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('名前（ひらがな）が長すぎます。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register2.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if ($id == "") {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('ログインIDを入力してください。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register2.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if (strlen($id) > 20) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('ログインIDが長すぎます。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register2.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if ($site_id == "") {
		if (preg_match("/[^0-9a-z_-]/", $id) > 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('ログインIDに使える文字は半角の「0〜9」「a〜z」「_」「-」のみです。レコード番号：{$employee_no}');</script>\n");
			echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register2.php?session=$session&result=f&encoding=$encoding';</script>");
			exit;
		}
	} else {
		if (preg_match("/[^0-9a-zA-Z_-]/", $id) > 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('ログインIDに使える文字は半角の「0〜9」「a〜z」「A〜Z」「_」「-」のみです。レコード番号：{$employee_no}');</script>\n");
			echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register2.php?session=$session&result=f&encoding=$encoding';</script>");
			exit;
		}
	}
	if (substr($id, 0, 1) == "-") {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('ログインIDの先頭に「-」は使えません。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register2.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if ($site_id == "") {
		if ($id == "cyrus" || $id == "postmaster" || $id == "root") {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('「{$id}」はログインIDとして使用できません。レコード番号：{$employee_no}');</script>\n");
			echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register2.php?session=$session&result=f&encoding=$encoding';</script>");
			exit;
		}
	}
	$sql = "select count(*) from login";
	$cond = "where emp_login_id = '$id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (intval(pg_fetch_result($sel, 0, 0)) > 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('指定されたログインIDは既に使用されています。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register2.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if ($pass == "") {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('パスワードを入力してください。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register2.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if (strlen($pass) > $pass_length) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('パスワードが長すぎます。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register2.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if (preg_match("/[^0-9a-zA-Z_-]/", $pass) > 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('パスワードに使える文字は半角の「0〜9」「a〜z」「A〜Z」「_」「-」のみです。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register2.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if (substr($pass, 0, 1) == "-") {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('パスワードの先頭に「-」は使えません。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register2.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if ($id == $pass) {
		$sql = "select weak_pwd_flg from config";
		$cond = "";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		if (pg_fetch_result($sel, 0, "weak_pwd_flg") == "f") {
			echo("<script type=\"text/javascript\">alert('ログインIDと同じパスワードは使用できません。レコード番号：{$employee_no}');</script>\n");
			echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register2.php?session=$session&result=f&encoding=$encoding';</script>");
			exit;
		}
	}
	if ($site_id != "") {
		if ($mail_id == "") {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('メールIDを入力してください。レコード番号：{$employee_no}');</script>\n");
			echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register2.php?session=$session&result=f&encoding=$encoding';</script>");
			exit;
		}

		if (strlen($mail_id) > 20) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('メールIDが長すぎます。レコード番号：{$employee_no}');</script>\n");
			echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register2.php?session=$session&result=f&encoding=$encoding';</script>");
			exit;
		}

		if ($use_cyrus == "t") {
			if (preg_match("/[^0-9a-z_-]/", $mail_id) > 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\">alert('メールIDに使える文字は半角の「0〜9」「a〜z」「_」「-」のみです。レコード番号：{$employee_no}');</script>\n");
				echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register2.php?session=$session&result=f&encoding=$encoding';</script>");
				exit;
			}
		} else {
			if (preg_match("/[^0-9a-z_.-]/", $mail_id) > 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\">alert('メールIDに使える文字は半角の「0〜9」「a〜z」「_」「-」「.」のみです。レコード番号：{$employee_no}');</script>\n");
				echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register2.php?session=$session&result=f&encoding=$encoding';</script>");
				exit;
			}
		}

		if (substr($mail_id, 0, 1) == "-") {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('メールIDの先頭に「-」は使えません。レコード番号：{$employee_no}');</script>\n");
			echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register2.php?session=$session&result=f&encoding=$encoding';</script>");
			exit;
		}

		$sql = "select count(*) from login inner join authmst on login.emp_id = authmst.emp_id";
		$cond = "where login.emp_login_mail = '$mail_id' and authmst.emp_del_flg = 'f'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		if (intval(pg_fetch_result($sel, 0, 0)) > 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('指定されたメールIDは既に使用されています。レコード番号：{$employee_no}');</script>\n");
			echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register2.php?session=$session&result=f&encoding=$encoding';</script>");
			exit;
		}
	}
	if ($sex != "1" && $sex != "2") {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('性別が不正です。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register2.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if ($birth != "") {
		if (preg_match("/^\d{8}$/", $birth) == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('生年月日が不正です。レコード番号：{$employee_no}');</script>\n");
			echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register2.php?session=$session&result=f&encoding=$encoding';</script>");
			exit;
		} else {
			$birth_yr = substr($birth, 0, 4);
			$birth_mon = substr($birth, 4, 2);
			$birth_day = substr($birth, 6, 2);
			if (!checkdate($birth_mon, $birth_day, $birth_yr)) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\">alert('生年月日が不正です。レコード番号：{$employee_no}');</script>\n");
				echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register2.php?session=$session&result=f&encoding=$encoding';</script>");
				exit;
			}
		}
	}
	if ($entry != "") {
		if (preg_match("/^\d{8}$/", $entry) == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('入職日が不正です。レコード番号：{$employee_no}');</script>\n");
			echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register2.php?session=$session&result=f&encoding=$encoding';</script>");
			exit;
		} else {
			$entry_yr = substr($entry, 0, 4);
			$entry_mon = substr($entry, 4, 2);
			$entry_day = substr($entry, 6, 2);
			if (!checkdate($entry_mon, $entry_day, $entry_yr)) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\">alert('入職日が不正です。レコード番号：{$employee_no}');</script>\n");
				echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register2.php?session=$session&result=f&encoding=$encoding';</script>");
				exit;
			}
		}
	}

	// 部門IDの取得
	$sql = "select class_id from classmst";
	$cond = "where class_nm = '$cls_nm' and class_del_flg = 'f'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('指定された{$arr_class_name[0]}は登録されていません。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register2.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	$cls = pg_fetch_result($sel, 0, "class_id");

	// 課IDの取得
	$sql = "select atrb_id from atrbmst";
	$cond = "where class_id = $cls and atrb_nm = '$atrb_nm' and atrb_del_flg = 'f'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('指定された{$arr_class_name[1]}は登録されていません。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register2.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	$atrb = pg_fetch_result($sel, 0, "atrb_id");

	// 科IDの取得
	$sql = "select dept_id from deptmst";
	$cond = "where atrb_id = $atrb and dept_nm = '$dept_nm' and dept_del_flg = 'f'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('指定された{$arr_class_name[2]}は登録されていません。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register2.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	$dept = pg_fetch_result($sel, 0, "dept_id");

	// 室IDの取得
	if ($arr_class_name[4] == "4" && $room_nm != "") {
		$sql = "select room_id from classroom";
		$cond = "where dept_id = $dept and room_nm = '$room_nm' and room_del_flg = 'f'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		if (pg_num_rows($sel) == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('指定された{$arr_class_name[3]}は登録されていません。レコード番号：{$employee_no}');</script>\n");
			echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register2.php?session=$session&result=f&encoding=$encoding';</script>");
			exit;
		}
		$room = pg_fetch_result($sel, 0, "room_id");
	} else {
		$room = null;
	}

	// 役職IDの取得
	$sql = "select st_id from stmst";
	$cond = "where st_nm = '$status_nm' and st_del_flg = 'f'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('指定された役職({$status_nm})は登録されていません。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register2.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	$status = pg_fetch_result($sel, 0, "st_id");

	// 職種IDの取得
	$sql = "select job_id from jobmst";
	$cond = "where job_nm = '$job_nm' and job_del_flg = 'f'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('指定された職種({$job_nm})は登録されていません。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_bulk_register2.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	$job = pg_fetch_result($sel, 0, "job_id");

	// 登録値の編集
	$new_emp_id = get_new_emp_id($con, $fname);
	$keywd = get_keyword($lt_kana_nm);

	// empmstデータを追加
	$sql = "insert into empmst (emp_id, emp_personal_id, emp_class, emp_attribute, emp_dept, emp_room, emp_job, emp_st, emp_lt_nm, emp_ft_nm, emp_kn_lt_nm, emp_kn_ft_nm, emp_sex, emp_birth, emp_join, emp_keywd) values (";
	$content = array($new_emp_id, $personal_id, $cls, $atrb, $dept, $room, $job, $status, $lt_nm, $ft_nm, $lt_kana_nm, $ft_kana_nm, $sex, $birth, $entry, $keywd);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// loginデータを追加
	if ($site_id == "") {
		$sql = "insert into login (emp_id, emp_login_id, emp_login_pass, emp_login_flg) values (";
		$content = array($new_emp_id, $id, $pass, "t");
	} else {
		$sql = "insert into login (emp_id, emp_login_id, emp_login_pass, emp_login_flg, emp_login_mail) values (";
		$content = array($new_emp_id, $id, $pass, "t", $mail_id);
	}
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// authmstデータを追加
	$sql = "insert into authmst (emp_id, emp_pass_flg) values(";
	$content = array($new_emp_id, "t");
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// optionデータを追加
	$sql = "insert into option (emp_id, schedule1_default, schedule2_default) values (";
	$content = array($new_emp_id, "2", "2");
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// mygroupmstデータを追加
	$sql = "insert into mygroupmst (owner_id, mygroup_id, mygroup_nm) values(";
	$content = array($new_emp_id, 1, "マイグループ");
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con,"rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

//XX // コミュニティサイトデータを登録
//XX function insert_community_data($con_mysql, $con, $employee_data, $domain) {
//XX 	$lt_nm = $employee_data["lt_nm"];
//XX 	$ft_nm = $employee_data["ft_nm"];
//XX 	$id = $employee_data["id"];
//XX 	$pass = $employee_data["pass"];
//XX
//XX 	$sql = "insert into " . XOOPS_DB_PREFIX . "_users (name, uname, email, user_regdate, user_viewemail, pass, rank, level, timezone_offset, notify_method, user_mailok) values ('$lt_nm $ft_nm', '$id', '$id@$domain', " . time() . ", 1, '" . md5($pass) . "', 0, 1, '9.0', 2, 0);";
//XX 	if (!mysql_query($sql, $con_mysql)) {
//XX 		pg_query($con, "rollback");
//XX 		pg_close($con);
//XX 		mysql_close($con_mysql);
//XX 		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
//XX 		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
//XX 		exit;
//XX 	}
//XX
//XX 	$uid = mysql_insert_id($con_mysql);
//XX 	$sql = "insert into " . XOOPS_DB_PREFIX . "_groups_users_link (groupid, uid) values (2, $uid)";
//XX 	if (!mysql_query($sql, $con_mysql)) {
//XX 		pg_query($con, "rollback");
//XX 		pg_close($con);
//XX 		mysql_close($con_mysql);
//XX 		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
//XX 		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
//XX 		exit;
//XX 	}
//XX }

// メールアカウントを作成
function make_mail_account($employee_data, $site_id, $dir, $imapServerAddress) {
	$id = $employee_data["id"];
	$pass = $employee_data["pass"];
	$mail_id = ($site_id == "") ? "" : $employee_data["mail_id"];

	$account = ($site_id == "") ? $id : "{$mail_id}_{$site_id}";
	if (preg_match("/[a-z]/", $account) > 0) {
		if ($imapServerAddress == "localhost" || $imapServerAddress == "127.0.0.1") {
			exec("/usr/bin/perl $dir/mboxadm/addmailbox $account $pass 2>&1 1> /dev/null", $output, $exit_status);
			if ($exit_status !== 0) {
				exec("/usr/bin/expect -f $dir/expect/addmailbox.exp $account $pass");
			}
		} else {
			exec("/usr/bin/expect -f $dir/expect/rsyncaddmailbox.exp $account $pass $imapServerAddress $dir");
		}
	}

	webmail_quota_change_to_default($account);
}

// 職員IDを採番
function get_new_emp_id($con, $fname) {
	$sql = "select max(emp_id) from empmst";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$max = pg_fetch_result($sel, 0, "max");

	$val = substr($max, 4, 12) + 1;
	$yymm = date("y") . date("m");

	for ($i = 8; $i > strlen($val); $i--) {
		$zero .= "0";
	}

	return "$yymm$zero$val";
}
?>
