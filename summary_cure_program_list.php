<?
ob_start();
$fname = $_SERVER["PHP_SELF"];
require_once("about_comedix.php");
require_once("get_values.php");
require_once("show_sinryo_top_header.ini");
require_once("label_by_profile_type.ini");
require_once("sot_util.php");
require_once("summary_common.ini");
ob_end_clean();

$session = qualify_session($session,$fname);
$fplusadm_auth = check_authority($session, 59, $fname);
$con = @connect2db($fname);
if(!$session || !$fplusadm_auth || !$con){
    echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
    exit;
}

$mode = isset($_REQUEST["mode"]) ? $_REQUEST["mode"] : "";
$db_key = explode("_", @$_REQUEST["key"]);
$is_disabled = isset($_REQUEST["is_disabled"]) ? $_REQUEST["is_disabled"] : "";
$caption = isset($_REQUEST["caption"]) ? $_REQUEST["caption"] : "";
$caption = trim(mb_convert_kana($caption, "s", "EUC-JP"));
$keylen = count($db_key);

//==========================================================================
// 更新
//==========================================================================
if ($mode == "update"){
    $sql  = " update sum_med_cure_program_mst set";
    $sql .= " caption = '" . pg_escape_string($caption)."'";
    $sql .= ",is_disabled = " . (int)$is_disabled;
    $sql .= " where therapy_type = '{$db_key[0]}' and code = " . (int)$db_key[1];
    $upd = update_set_table($con, $sql, array(), null, "", $fname);
    pg_close($con);
    header("Location: summary_cure_program_list.php?session={$session}");
    die;
}

//==========================================================================
// 削除
//==========================================================================
if ($mode == "delete"){
    // トランザクションの開始
    pg_query($con, "begin");
    
    $sql = " delete from sum_med_cure_program_mst where therapy_type = '{$db_key[0]}' and code = " . (int)$db_key[1];
    $upd = update_set_table($con, $sql, array(), null, "", $fname);
    if ($upd == 0) {
        pg_query($con,"rollback");
        pg_close($con);
        js_error_exit();
    }
    
    // 目標グループマスタからもデータを削除
    $sql = "delete from sum_med_goal_group_mst where therapy_type = '{$db_key[0]}' and code = " . (int)$db_key[1];
    $upd = update_set_table($con, $sql, array(), null, "", $fname);
    if ($upd == 0) {
        pg_query($con,"rollback");
        pg_close($con);
        js_error_exit();
    }
    
    // トランザクションのコミット
    pg_query($con, "commit");
    
    pg_close($con);
    header("Location: summary_cure_program_list.php?session={$session}");
    die;
}

//==========================================================================
// 表示順変更
//==========================================================================
if ($mode == "up" || $mode == "down"){
    $sql =
        " select sort_order from sum_med_cure_program_mst".
        " where therapy_type = '{$db_key[0]}' and code = " . (int)$db_key[1];
    $sel = select_from_table($con, $sql, "", $fname);
    $cur_sort_order = pg_fetch_result($sel, 0, 0);

    $sql = " select * from sum_med_cure_program_mst";
    if ($mode == "up") $sql .= " where sort_order < ".$cur_sort_order;
    if ($mode == "down") $sql .= " where sort_order > ".$cur_sort_order;
    $sql .= " and therapy_type = '{$db_key[0]}'";
    if ($mode == "up") $sql .= " order by sort_order desc limit 1";
    if ($mode == "down") $sql .= " order by sort_order limit 1";

    $sel = select_from_table($con, $sql, "", $fname);
    $swap_row = pg_fetch_array($sel);
    if ($swap_row["sort_order"] != ""){

        $sql  = " update sum_med_cure_program_mst set";
        $sql .= " sort_order = " . $swap_row["sort_order"];
        $sql .= " where therapy_type = '{$db_key[0]}' and code = " . (int)$db_key[1];
        $upd = update_set_table($con, $sql, array(), null, "", $fname);

        $sql  = " update sum_med_cure_program_mst set";
        $sql .= " sort_order = " . $cur_sort_order;
        $sql .= " where therapy_type = '{$swap_row[0]}' and code = " . (int)$swap_row[1];
        $upd = update_set_table($con, $sql, array(), null, "", $fname);

        pg_close($con);
        header("Location: summary_cure_program_list.php?session={$session}");
        die;
    }
}

//==========================================================================
// 以下、通常処理
//==========================================================================
$font = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';

// 診療科権限を取得
$entity_admin_auth = check_authority($session, 28, $fname);
// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);
// 診療区分/記録区分
$summary_kubun_title = $_label_by_profile["SUMMARY_KUBUN"][$profile_type];
// メドレポート/ＰＯレポート(Problem Oriented)
$med_report_title = $_label_by_profile["MED_REPORT"][$profile_type];
// 診療科/担当部門
$section_title = $_label_by_profile["SECTION"][$profile_type];
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix マスターメンテナンス | マスタ管理</title>

<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.td_border { border:#5279a5 solid 1px; }
.subtitle { font-weight:bold; margin-top:10px }
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">

<? show_sinryo_top_header($session, $fname, "KANRI_MST"); ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- left -->
<td width="120" valign="top">
<table width="120" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width="118" valign="top">
<? echo summary_common_show_admin_left_menu($session, $entity_admin_auth, $section_title, $summary_kubun_title); ?>
</td>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
</td>
<!-- left -->
<!-- center -->
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->
<!-- right -->
<td valign="top">

<?
//==========================================================================
// マスタ管理メニュー
//==========================================================================
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<? summary_common_show_mst_tab_menu($session, '治療プロ'); ?>
<td>&nbsp;</td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr></table>


<?
//==========================================================================
// サブメニュー
//==========================================================================
?>
<table border="0" cellspacing="0" cellpadding="0" style="margin-top:8px">
    <tr height="22">
        <td width="100" align="center" bgcolor="#958f28">
            <a href="summary_cure_program_list.php?session=<? echo $session; ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>一覧/編集</b></font></a>
        </td>
        <td width="5">&nbsp;</td>
        <td width="100" align="center" bgcolor="#e8e4bd">
            <a href="summary_cure_program_regist.php?session=<? echo $session; ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>新規登録</b></font></a>
        </td>
        <td width="5">&nbsp;</td>
        <td></td>
    </tr>
</table>



<?
//==========================================================================
// 一覧
//==========================================================================
    $therapy_types = array('PT' => '理学療法の治療プログラム', 'OT' => '作業療法の治療プログラム', 'ST' => '言語聴覚療法の治療プログラム', 'PST' => '心理療法の治療プログラム');
    $data2 = array();
    $sql =
        " select *".
        " from sum_med_cure_program_mst".
        " order by therapy_type, sort_order";
    $sel = select_from_table($con, $sql, "", $fname);
    while ($row = pg_fetch_array($sel)) {
        $data[$row["therapy_type"]][$row["code"]] = array("caption"=>$row["caption"], "disabled"=>$row["is_disabled"]);
    }
?>


<form action="summary_cure_program_list.php" method="get" name="frm" style="margin-top:10px">
    <input type="hidden" name="session" value="<? echo $session; ?>">
    <input type="hidden" name="mode" value="">
    <input type="hidden" name="key" value="">
    <input type="hidden" name="is_disabled" value="">
    <input type="hidden" name="caption" value="">
</form>
<script type="text/javascript">
    function registData(mode, key){
        var keys = key.split("_");
        document.frm.mode.value = mode;
        document.frm.key.value = key;
        document.frm.caption.value = document.getElementById("data_"+key).value;
        document.frm.is_disabled.value =  document.getElementById("chk_"+key).checked ? "0" : "1";
        if (document.frm.caption.value==""){
            alert("表記文字を指定してください。");
            return;
        }
        document.frm.submit();
    }
</script>

<? foreach ($therapy_types as $type => $caption){ ?>
    <table border="0" cellspacing="0" cellpadding="2" style="margin-top:10px; border-collapse:collapse">
        <tr>
            <td class="td_border" style="width:80px; background-color:#e3ecf4" colspan="4">&nbsp;&nbsp;<? echo $font; ?><? echo $type; ?></font></td>
            <td class="td_border"><? echo $font; ?><? echo $caption; ?></font></td>
        </tr>
        <? foreach ($data[$type] as $code => $row){ ?>
            <? if ($code == "") continue; ?>
            <tr>
            <td style="width:30px">&nbsp;</td>
            <td class="td_border" style="width:60px; background-color:#e3ecf4" colspan="3">&nbsp;&nbsp;<? echo $font; ?><? echo $type; ?>-<? echo $code; ?></font></td>
            <td class="td_border"><? echo $font; ?>
                <input type="text" style="width:200px" name="data_<? echo $type; ?>_<? echo $code; ?>" id="data_<? echo $type; ?>_<? echo $code; ?>" value="<? echo h($row["caption"]); ?>">
                <input type="button" value="▲" onclick="registData('up','<? echo $type; ?>_<? echo $code; ?>')" />
                <input type="button" value="▼" onclick="registData('down','<? echo $type; ?>_<? echo $code; ?>')" />
                <label style="padding-left:10px"><input type="checkbox" value="1" id="chk_<? echo $type; ?>_<? echo $code; ?>" name="chk_<? echo $type; ?>_<? echo $code; ?>" <? echo $row["disabled"]?"":"checked"; ?> />表示</label>
                <input type="button" value="更新" onclick="registData('update','<? echo $type; ?>_<? echo $code; ?>')" />
                <input style="margin-left:10px" type="button" value="削除" onclick="if(!confirm('この操作で、目標グループからも削除されます。\n\n削除してよろしいですか？')) return; registData('delete','<? echo $type; ?>_<? echo $code; ?>')" />
            </font></td>
            </tr>
        <? } ?>
    </table>
<? } ?>

</td>
<!-- right -->
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
