<?php

// セッションの開始
session_start();

// ==================================================
// ファイルの読込み
// ==================================================
require_once('Cmx.php');
require_once('Cmx/View/Smarty.php');
require_once("hiyari_header_class.php");

require_once("about_authority.php");
require_once("about_session.php");
require_once("about_postgres.php");

require_once("hiyari_common.ini");
require_once("hiyari_post_select_box.ini");
require_once("hiyari_rp_report_list_dl.ini");
require_once("hiyari_report_class.php");
require_once("hiyari_mail.ini");
require_once("hiyari_mail_input.ini");
require_once("hiyari_wf_utils.php");
require_once("hiyari_auth_class.php");

require_once("get_values.php");
require_once('class/Cmx/Model/SystemConfig.php');
require_once("conf/conf.inf");

// ==================================================
// 初期処理
// ==================================================
// 画面名
$fname = $PHP_SELF;

// モード
$mode = $_POST["mode"];

// 日付データ取得
$this_year = date('Y');
$this_month = date('m');

// 小数点桁数
$decimal_num = 2;

// ==================================================
// 画面データ取得
// ==================================================
// 集計モード
$date_mode = $_POST["date_mode"];

// 集計年月
$date_year = $_POST["date_year"];
$date_month = $_POST["date_month"];
$date_term = $_POST["date_term"];
if ($date_year == "") {
    $date_year = $this_year;
}
if ($date_month == "") {
    $date_month = $this_month;
}
if ($date_term == "") {
    $date_term = 1;
}

// 副報告フラグ
$sub_report_use = $_POST["sub_report_use"];

// 所属データ(配列)
$belong_cnt = $_POST["belong_cnt"];
$belong_class = $_POST["belong_class"];
$belong_attribute = $_POST["belong_attribute"];
$belong_dept = $_POST["belong_dept"];
$belong_room = $_POST["belong_room"];

// ==================================================
// セッションのチェック
// qualify_session() from about_session.php
// ==================================================
$session = qualify_session($session, $fname);
if($session == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// ==================================================
// 権限チェック
// check_authority() from about_authority.php
// ==================================================
$summary = check_authority($session, 47, $fname);
if ($summary == "0") {
    showLoginPage();
    exit;
}

// ==================================================
// DBコネクション取得
// connect2db() from about_postgres.php
// ==================================================
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// ==================================================
// EXCELダウンロード
// ==================================================
if ($mode == "excel") {
    download_excel($con, $fname);
    die;
}

//==================================================
//デザイン（1=旧デザイン、2=新デザイン）
//==================================================
$sysConf = new Cmx_SystemConfig();
$design_mode = $sysConf->get('fantol.design.mode');

// ==================================================
// セクション名(部門・課・科・室)取得
// get_class_name_array() from show_class_name.ini
// ==================================================
$arr_class_name = get_class_name_array($con, $fname);
$class_cnt = $arr_class_name["class_cnt"];

// ==================================================
// 職員ID取得
// get_emp_id() from get_values.php
// ==================================================
$emp_id = get_emp_id($con, $session, $fname);

// 初期表示時
if ($mode == "") {
    // ==================================================
    // 職員情報取得
    // get_empmst() from get_values.php
    // ==================================================
    $emp_info = get_empmst($con, $emp_id, $fname);
    $_POST["belong_class"][0] = $emp_info[2];
    $_POST["belong_attribute"][0] = $emp_info[3];
    $_POST["belong_dept"][0] = $emp_info[4];
    $_POST["belong_room"][0] = $emp_info[33];
}

// ==================================================
// 表示
// ==================================================
$smarty = new Cmx_View();

$smarty->assign("session", $session);
$smarty->assign("session_name", session_name());
$smarty->assign("session_id", session_id());

//------------------------------------------------------------------------------
//ヘッダ用データ
//------------------------------------------------------------------------------
$header = new hiyari_header_class($session, $fname, $con);
$smarty->assign("INCIDENT_TITLE", $header->get_inci_title());
$smarty->assign("inci_auth_name", $header->get_inci_auth_name());
$smarty->assign("is_kanrisha", $header->is_kanrisha());
$smarty->assign("tab_info", $header->get_user_tab_info());

//------------------------------------------------------------------------------
//MENU:集計ダウンロード
//------------------------------------------------------------------------------
$stats_download_mode = defined("STATS_DOWNLOAD_MODE");
$smarty->assign("stats_download_mode", $stats_download_mode);

//------------------------------------------------------------------------------
//集計条件：集計モード
//------------------------------------------------------------------------------
$smarty->assign("date_mode", $date_mode);
if ($date_mode == "" || $date_mode == "report_date") {
    $report_date_str = "selected";
    $incident_date_str = "";
} else if ($date_mode == "incident_date") {
    $report_date_str = "";
    $incident_date_str = "selected";
}
$smarty->assign("report_date_str", $report_date_str);
$smarty->assign("incident_date_str", $incident_date_str);

//------------------------------------------------------------------------------
//集計条件：集計年月
//------------------------------------------------------------------------------
$smarty->assign("year_list", array_reverse(range(2004, $this_year)));
$smarty->assign("date_year", $date_year);
$smarty->assign("month_list", range(1,12));
$smarty->assign("date_month", $date_month);
$smarty->assign("date_term", $date_term);

//------------------------------------------------------------------------------
//集計条件：副報告
//------------------------------------------------------------------------------
$smarty->assign("sub_report_use", $sub_report_use);

//------------------------------------------------------------------------------
//集計条件：部署
//------------------------------------------------------------------------------
//-----------------
//部署階層の名前
//-----------------
$smarty->assign("arr_class_name", $arr_class_name);

//-----------------
//部署の選択肢
//-----------------
// 統計分析の所属参照範囲を取得
// get_stats_read_div() from hiyari_common.ini
$read_div = get_stats_read_div($session,$fname,$con);

// ログインユーザーの最優先権限を取得
// get_emp_priority_auth() from hiyari_auth_class.php
$priority_auth = get_emp_priority_auth($session, $fname, $con);

// 統計分析の利用制限の所属/担当範囲判定フラグを取得
// get_stats_read_auth_flg() from hiyari_common.ini
$read_auth_flg = get_stats_read_auth_flg($session,$fname,$con);

$post_select_data = get_post_select_data($con, $fname, $emp_id, $read_div, $priority_auth, $read_auth_flg);
$smarty->assign("post_select_data", $post_select_data);

//-----------------
//部署条件数と選択値
//-----------------
//部署条件数
$belong_cnt = $_POST["belong_cnt"];
if ($belong_cnt == "") {
    $belong_cnt = 1;
}

// 行追加
if ($_POST["mode"] == "add_belong") {
    $belong_cnt++;
}

// 行削除
if ($_POST["mode"] == "del_belong") {
    $tmp_class = array();
    $tmp_attribute = array();
    $tmp_dept = array();
    $tmp_room = array();

    for($i=0; $i<$belong_cnt; $i++) {
        if ($_POST["del_line_no"] != $i) {
            $tmp_class[] = $_POST["belong_class"][$i];
            $tmp_attribute[] = $_POST["belong_attribute"][$i];
            $tmp_dept[] = $_POST["belong_dept"][$i];
            if ($class_cnt == 4){
                $tmp_room[] = $_POST["belong_room"][$i];
            }
        }
    }

    $belong_cnt--;

    $belong_class = $tmp_class;
    $belong_attribute = $tmp_attribute;
    $belong_dept = $tmp_dept;
    if ($class_cnt == 4){
        $belong_room = $tmp_room;
    }
}

$smarty->assign("belong_cnt", $belong_cnt);
$smarty->assign("belong_class", $belong_class);
$smarty->assign("belong_attribute", $belong_attribute);
$smarty->assign("belong_dept", $belong_dept);
if ($class_cnt == 4){
    $smarty->assign("belong_room", $belong_room);
}

//--------------------
// 集計実行時の部署条件
//--------------------
if ($mode == "sum") {
    // 部門一覧取得
    // get_class_mst() from hiyari_common.ini
    $sel_class = get_class_mst($con, $fname);
    $sel_class_arr = pg_fetch_all($sel_class);

    // 課一覧取得
    // get_atrb_mst() from hiyari_common.ini
    $sel_atrb = get_atrb_mst($con, $fname);
    $sel_atrb_arr = pg_fetch_all($sel_atrb);

    // 科一覧取得
    // get_dept_mst() from hiyari_common.ini
    $sel_dept = get_dept_mst($con, $fname);
    $sel_dept_arr = pg_fetch_all($sel_dept);

    // 室一覧取得
    // get_room_mst() from hiyari_common.ini
    $sel_room = get_room_mst($con, $fname);
    $sel_room_arr = pg_fetch_all($sel_room);

    $belong_list = array();
    for ($i = 0; $i < $belong_cnt; $i++) {
        $belong = array();

        $wk_class = $belong_class[$i];
        $wk_attribute = $belong_attribute[$i];
        $wk_dept = $belong_dept[$i];
        $wk_room = $belong_room[$i];

        $wk_belong_selected = "";

        if ($wk_class != "") {
            foreach ($sel_class_arr as $row) {
                $tmp_class_id = $row["class_id"];
                $tmp_class_nm = $row["class_nm"];

                if ($wk_class == $tmp_class_id) {
                    $wk_belong_selected .= $tmp_class_nm;
                }
            }
        } else {
            $wk_belong_selected .= "全て";
        }
        if ($wk_attribute != "") {
            foreach ($sel_atrb_arr as $row) {
                $tmp_class_id = $row["class_id"];
                $tmp_atrb_id = $row["atrb_id"];
                $tmp_atrb_nm = $row["atrb_nm"];

                if ($wk_class == $tmp_class_id && $wk_attribute == $tmp_atrb_id) {
                    $wk_belong_selected .= "＞" . $tmp_atrb_nm;
                }
            }
        } else {
            $wk_belong_selected .= "＞全て";
        }
        if ($wk_dept != "") {
            foreach ($sel_dept_arr as $row) {
                $tmp_atrb_id = $row["atrb_id"];
                $tmp_dept_id = $row["dept_id"];
                $tmp_dept_nm = $row["dept_nm"];

                if ($wk_attribute == $tmp_atrb_id && $wk_dept == $tmp_dept_id) {
                    $wk_belong_selected .= "＞" . $tmp_dept_nm;
                }
            }
        } else {
            $wk_belong_selected .= "＞全て";
        }
        if ($wk_room != "") {
            foreach ($sel_room_arr as $row) {
                $tmp_dept_id = $row["dept_id"];
                $tmp_room_id = $row["room_id"];
                $tmp_room_nm = $row["room_nm"];

                if ($wk_dept == $tmp_dept_id && $wk_room == $tmp_room_id) {
                    $wk_belong_selected .= "＞" . $tmp_room_nm;
                }
            }
        } else {
            $wk_belong_selected .= "＞全て";
        }

        $belong['class'] = $wk_class;
        $belong['attribute'] = $wk_attribute;
        $belong['dept'] = $wk_dept;
        $belong['room'] = $wk_room;
        $belong['selected'] = $wk_belong_selected;

        $belong_list[] = $belong;
    }

    $smarty->assign("belong_list", $belong_list);
}

//------------------------------------------------------------------------------
//表示
//------------------------------------------------------------------------------
$smarty->display("hiyari_rm_stats_download2.tpl");

// DB接続の切断
pg_close($con);

/**
 * 指定ヶ月後の年月を取得する
 *
 * @param  string  $year  年
 * @param  string  $month 月
 * @param  string  $num   指定ヶ月
 * @return string         指定ヶ月後の年月
 */
function add_month($year, $month, $num)
{
    if ((int)$month + (int)$num > 12) {
        $month = ((int)$month + (int)$num) - 12;
        $year++;
    } else if ((int)$month + (int)$num <= 0) {
        $month = 12 + ((int)$month + (int)$num);
        $year--;
    } else {
        $month = (int)$month + (int)$num;
    }
    return $year . "/" . substr("00" . $month, -2);
}

/**
 * 集計対象の報告書IDを抽出する
 * 
 * @param  object $con   DBコネクション
 * @param  string $fname 画面名
 * @return array         報告書ID
 */
function get_report_id($con, $fname)
{
    $date_year      = $_POST["date_year"];         // 集計年
    $date_month     = $_POST["date_month"];        // 集計月
    $date_term      = $_POST["date_term"];         // 集計期間
    $date_mode      = $_POST["date_mode"];         // 集計モード
    $sub_report_use = $_POST["sub_report_use"];    // 副報告フラグ

    // 日付の検索範囲を取得
    $search_start_date = $date_year . "/" . $date_month;
    $search_end_date = add_month($date_year, $date_month, $date_term - 1);      // 月までの指定なので、-1して調整
    
    $sql  = "SELECT DISTINCT a.report_id ";
    $sql .= "FROM ";
    if ($sub_report_use == "true") {    // 副報告を含める場合は全てのレポートが対象
        $sql .= "inci_report a ";
    } else {
        $sql .= "(";
        $sql .= "SELECT data.* ";
        $sql .= "FROM inci_report data ";
        $sql .= "LEFT OUTER JOIN inci_report_link link ";
        $sql .= "ON data.report_id = link.report_id ";
        $sql .= "WHERE (link.main_flg = 't' OR link.main_flg IS Null) ";
        $sql .= ") a ";
    }
    
    $sql .= "WHERE NOT a.shitagaki_flg AND NOT a.del_flag AND NOT a.kill_flg ";
    if ($date_mode == "report_date") {
        $sql .= "AND a.registration_date BETWEEN '$search_start_date/00' AND '$search_end_date/99' ";
    } elseif ($date_mode == "incident_date") {
        $sql .= "AND a.incident_date BETWEEN '$search_start_date/00' AND '$search_end_date/99' ";
    }
    $sql .= "ORDER BY a.report_id DESC ";

    // SQL実行
    $sel = select_from_table($con, $sql, "", $fname);
    if ($sel == 0) {
        pg_close($con);
        showErrorPage();
        exit;
    }
    
    $arr_report_ids = array();
    while ($row = pg_fetch_assoc($sel)) {
        $arr_report_ids[] = $row["report_id"];
    }

    return $arr_report_ids;
}

/**
 * 項目定義配列を生成する
 * 
 * @param  object $con          DBコネクション
 * @param  string $fname        画面名
 * @param  array  $arr_grp_item 項目マスタ
 * @return array                項目定義
 */
function create_arr_report_materials($con, $fname, $arr_grp_item)
{
    $arr_report_materials = array();
    
    foreach ($arr_grp_item as $grp_item) {
        $grp_item_temp = array(
                "grp_code"       => $grp_item["grp_code"],
                "easy_item_code" => $grp_item["easy_item_code"],
                "easy_item_name" => $grp_item["easy_item_name"],
                "item_type"      => $grp_item["item_type"]
        );
    
        if ($grp_item["item_type"] == "select" || $grp_item["item_type"] == "checkbox" || $grp_item["item_type"] == "radio") {
            $easy = get_inci_report_materials($con, $fname, $grp_item["grp_code"], $grp_item["easy_item_code"]);
            $grp_item_temp["easy_info"] = $easy;
        }
        array_push($arr_report_materials, $grp_item_temp);
    }
    
    return $arr_report_materials;
}

/**
 * ダウンロードデータを作成
 * 
 * @param  object $con   DBコネクション
 * @param  string $fname 画面名
 * @return string        ダウンロードデータ
 */
function create_download_data($con, $fname)
{
    // 出力対象のグループコード
    $use_grp_code = array(
            "9100",    // 報告者職種
            "1100",    // 実施の有無と影響度
            "100",     // 発生日時
            "3020",    // 発見者職種
            "3050",    // 当事者職種
            "125",     // ヒヤリ・ハット分類
            "900",     // 概要
            "90",      // 患者影響レベル
            "1530"     // 教育研修への活用
    );
    
    // 選択項目名・形式マスタ取得
    if (!($arr_grp_item = get_inci_easyinput_item_mst($con, $fname, $use_grp_code))) {
        return "";
    }
    
    // 項目定義配列を生成する
    $arr_report_materials = create_arr_report_materials($con, $fname, $arr_grp_item);

    // レポートクラスのインスタンス化
    $rep_obj = new hiyari_report_class($con, $fname);
    
    // GRM(SM)フラグをセット
    $is_sm_emp_flg = is_sm_emp($session, $fname, $con);
    
    // 集計対象の報告書IDを抽出する
    if (!($arr_report_ids = get_report_id($con, $fname))) {
        return "";
    }
    
    // EXCELデータ作成
    $download_data = get_excel_data($con, $fname, implode(",", $arr_report_ids), $arr_report_materials, $use_grp_code, $rep_obj, $is_sm_emp_flg);
    
    return $download_data;
}

/**
 * EXCELダウンロード
 * (参考:hiyari_rp_report_list_excel.php)
 * 
 * @param object $con   DBコネクション
 * @param string $fname 画面名
 */
function download_excel($con, $fname)
{
    // ダウンロードデータを作成
    $download_data = create_download_data($con, $fname);
    
    $filename = "inci-data.xls";
    ob_clean();
    header("Content-Type: application/vnd.ms-excel");
    header('Content-Disposition: attachment; filename=' . $filename);
    header('Expires: 0');
    header('Cache-Control: must-revalidate, post-check=0,pre-check=0');
    header('Pragma: public');
    echo mb_convert_encoding($download_data, 'sjis', mb_internal_encoding());
    ob_end_flush();
}
?>