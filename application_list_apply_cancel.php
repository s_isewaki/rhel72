<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" action="application_apply_list.php" method="post">
<input type="hidden" name="session" value="<?php echo($session); ?>">
<input type="hidden" name="category" value="<?php echo($category); ?>">
<input type="hidden" name="workflow" value="<?php echo($workflow); ?>">
<input type="hidden" name="apply_title" value="<?php echo($apply_title); ?>">
<input type="hidden" name="approve_emp_nm" value="<?php echo($approve_emp_nm); ?>">
<input type="hidden" name="date_y1" value="<?php echo($date_y1); ?>">
<input type="hidden" name="date_m1" value="<?php echo($date_m1); ?>">
<input type="hidden" name="date_d1" value="<?php echo($date_d1); ?>">
<input type="hidden" name="date_y2" value="<?php echo($date_y2); ?>">
<input type="hidden" name="date_m2" value="<?php echo($date_m2); ?>">
<input type="hidden" name="date_d2" value="<?php echo($date_d2); ?>">
<input type="hidden" name="apply_stat" value="<?php echo($apply_stat); ?>">
</form>
<?php
require_once("about_session.php");
require_once("about_authority.php");
require_once("application_workflow_common_class.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 7, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

//勤怠申請機能を使用するかファイルで確認 
$kintai_flag = file_exists("kintai/common/mdb.php");
// 総合届け使用フラグ
$general_flag = file_exists("opt/general_flag");
// ----- メディアテック Start  -----
if (phpversion() >= '5.1' && $kintai_flag) {
    require_once 'kintai/common/mdb_wrapper.php';
    require_once 'kintai/common/mdb.php';
    MDBWrapper::init();
    $mdb = new MDB();
    $mdb->beginTransaction(); // トランザクション開始
}
// ----- メディアテック End -----

$obj = new application_workflow_common_class($con, $fname);

for ($i = 0, $j = count($apply_cancel_chk); $i < $j; $i++) {
	if ($apply_cancel_chk[$i] != "") {
		$apply_id = $apply_cancel_chk[$i];

		// 承認状況チェック
		$apv_cnt = $obj->get_applyapv_cnt($apply_id);
		if ($apv_cnt > 0) {
			$arr_apply_wkfwmst = $obj->get_apply_wkfwmst($apply_id);
			pg_query($con, "rollback");
			pg_close($con);
			$approve_label = ($arr_apply_wkfwmst[0]["approve_label"] != "2") ? "承認" : "確認";
			echo("<script type=\"text/javascript\">alert('他の{$approve_label}状況が変更されたため、申請取消ができません。');</script>");
			echo("<script type=\"text/javascript\">document.items.submit();</script>");
			exit;
		}

		// 申請論理削除
		$obj->update_delflg_all_apply($apply_id, "t");

        // ----- メディアテック Start -----
        if (phpversion() >= '5.1' && $kintai_flag) {
            // テンプレートタイプを取得
            require_once 'kintai/common/workflow_common_util.php';
            $template_type = WorkflowCommonUtil::get_template_type($apply_id);

            $class = null;
            switch ($template_type) {
            case "ovtm": // 時間外勤務命令の場合
                if (module_check(WORKFLOW_OVTM_MODULE, WORKFLOW_OVTM_CLASS)) {
                    $class = WORKFLOW_OVTM_CLASS;
                }
                break;
            case "hol": // 休暇申請の場合
                //総合届け
                if ($general_flag) {
                    if (module_check(WORKFLOW_HOL_MODULE, WORKFLOW_HOL_CLASS)) {
                        $class = WORKFLOW_HOL_CLASS;
                    }
                }
                //休暇申請
                else {
                    if (module_check(WORKFLOW_HOL2_MODULE, WORKFLOW_HOL2_CLASS)) {
                        $class = WORKFLOW_HOL2_CLASS;
                    }
                }
                break;
            case "travel": // 旅行命令
                if (module_check(WORKFLOW_TRVL_MODULE, WORKFLOW_TRVL_CLASS)) {
                    $class = WORKFLOW_TRVL_CLASS;
                }
                break;
            case "late": // 遅刻早退の場合
                if (module_check(WORKFLOW_LATE_MODULE, WORKFLOW_LATE_CLASS)) {
                    $class = WORKFLOW_LATE_CLASS;
                }
                break;
            }
            if (!empty($class)) {
                $module = new $class;
                $module->cancel_apply($apply_id);
            }
        }
		// ----- メディアテック End -----
	}
}

// ----- メディアテック Start -----
if (phpversion() >= '5.1' && $kintai_flag) {
    // MDBトランザクションをコミット
    $mdb->endTransaction();
}
// ----- メディアテック End -----
// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を切断
pg_close($con);

// 一覧画面に遷移
echo("<script type=\"text/javascript\">document.items.submit();</script>");

// ----- メディアテック Start -----
// モジュール・クラスの存在チェック
function module_check($module, $class) {
    if (file_exists(WORKFLOW_KINTAI_DIR."/".$module)) {
        require_once WORKFLOW_KINTAI_DIR."/".$module;
        return class_exists($class);
    }
    return false;
}
// ----- メディアテック End -----

?>
</body>
