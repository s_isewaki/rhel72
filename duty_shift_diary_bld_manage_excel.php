<?
///*****************************************************************************
// 勤務シフト作成 | シフト作成「ＥＸＣＥＬ」 
///*****************************************************************************

//ini_set("display_errors","1");

//EXCELデータ転送用コマンド開始
ob_start();

require_once("about_authority.php");
require_once("about_session.php");
require_once("about_postgres.php");

require_once("duty_shift_common.ini");
require_once("duty_shift_common_class.php");
require_once("duty_shift_diary_common.ini");
require_once("duty_shift_diary_bld_manage_common.php");

///-----------------------------------------------------------------------------
//画面名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;


///-----------------------------------------------------------------------------
//セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session,$fname);
if($session == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if($con == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);

///-----------------------------------------------------------------------------
// 権限のチェック
///-----------------------------------------------------------------------------
//ユーザ画面用
$chk_flg = $obj->check_authority_user($session, $fname);
if ($chk_flg == "") 
{
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
//管理画面用
$section_admin_auth = $obj->check_authority_Management($session, $fname);
///-----------------------------------------------------------------------------
//初期処理
///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
//初期値設定
///-----------------------------------------------------------------------------
$err_flg_1 = "";

///-----------------------------------------------------------------------------
//ＤＢ(wktmgrp)より勤務パターン情報を取得
///-----------------------------------------------------------------------------
//$data_emp = $obj->get_empmst_array("");
$data_emp = array();	//以下の処理で未使用のため
$data_wktmgrp = $obj->get_wktmgrp_array();

///-----------------------------------------------------------------------------
// グループ名を取得
///-----------------------------------------------------------------------------
$sql = "select group_name from duty_shift_group ";
$cond = "where group_id = '$group_id' ";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($this->_db_con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$group_name = "";
$num = pg_numrows($sel);
if ($num > 0) {
	$group_name = pg_result($sel,0,"group_name");
}



///-----------------------------------------------------------------------------
// ログインユーザの職員ID・氏名を取得
///-----------------------------------------------------------------------------
$sql = "select emp_id, emp_lt_nm, emp_ft_nm from empmst";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) 
{
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

$emp_id = pg_fetch_result($sel, 0, "emp_id");
$emp_name = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");


$arr_ya_ake_jokin_kango  = array();			//夜勤明け、常勤、看護師メンバー
$arr_ya_ake_hi_jokin_kango  = array();		//夜勤明け、非常勤、看護師メンバー

$arr_ya_ake_jokin_jun_kango  = array();		//夜勤明け、常勤、准看護師メンバー
$arr_ya_ake_hi_jokin_jun_kango  = array();	//夜勤明け、非常勤、准看護師メンバー

$arr_ya_ake_jokin_hojo  = array();			//夜勤明け、常勤、補助メンバー
$arr_ya_ake_hi_jokin_hojo  = array();		//夜勤明け、非常勤、補助メンバー

$arr_ya_iri_jokin_kango  = array();			//夜勤入り、常勤、看護師メンバー
$arr_ya_iri_hi_jokin_kango  = array();		//夜勤入り、非常勤、看護師メンバー

$arr_ya_iri_jokin_jun_kango  = array();		//夜勤入り、常勤、准看護師メンバー
$arr_ya_iri_hi_jokin_jun_kango  = array();	//夜勤入り、非常勤、准看護師メンバー

$arr_ya_iri_jokin_hojo  = array();			//夜勤入り、常勤、補助メンバー
$arr_ya_iri_hi_jokin_hojo  = array();		//夜勤入り、非常勤、補助メンバー

$arr_nikkin_jokin_kango  = array();			//日勤、常勤、看護師メンバー
$arr_nikkin_hi_jokin_kango  = array();		//日勤、非常勤、看護師メンバー

$arr_nikkin_jokin_jun_kango  = array();		//日勤、常勤、准看護師メンバー
$arr_nikkin_hi_jokin_jun_kango  = array();	//日勤、非常勤、准看護師メンバー

$arr_nikkin_jokin_hojo  = array();			//日勤、常勤、補助メンバー
$arr_nikkin_hi_jokin_hojo  = array();		//日勤、非常勤、補助メンバー

$arr_hankin_jokin_kango  = array();			//半勤、常勤、看護師メンバー
$arr_hankin_hi_jokin_kango  = array();		//半勤、非常勤、看護師メンバー

$arr_hankin_jokin_jun_kango  = array();		//半勤、常勤、准看護師メンバー
$arr_hankin_hi_jokin_jun_kango  = array();	//半勤、非常勤、准看護師メンバー

$arr_hankin_jokin_hojo  = array();			//半勤、常勤、補助メンバー
$arr_hankin_hi_jokin_hojo  = array();		//半勤、非常勤、補助メンバー

$arr_jokin_kokyu  = array();					//常勤、公休メンバー
$arr_jokin_nenkyu  = array();					//常勤、年休メンバー

$arr_hi_jokin_kokyu  = array();				//非常勤、公休メンバー
$arr_hi_jokin_nenkyu  = array();				//非常勤、年休メンバー

//その他 20130205
$arr_jokin_other  = array();					//常勤、その他メンバー
$arr_hi_jokin_other  = array();				//非常勤、その他メンバー


$wwwwwwk = $yyyymmdd;




//指定された日付、シフトグループで表示するデータを取得し、表示エリアごとに配列別で振り分ける
list($arr_ya_ake_jokin_kango, $arr_ya_ake_hi_jokin_kango,
        $arr_ya_ake_jokin_jun_kango,$arr_ya_ake_hi_jokin_jun_kango,
        $arr_ya_ake_jokin_hojo,$arr_ya_ake_hi_jokin_hojo,
        $arr_ya_iri_jokin_kango,$arr_ya_iri_hi_jokin_kango,
        $arr_ya_iri_jokin_jun_kango,$arr_ya_iri_hi_jokin_jun_kango,
        $arr_ya_iri_jokin_hojo,$arr_ya_iri_hi_jokin_hojo,
        $arr_nikkin_jokin_kango,$arr_nikkin_hi_jokin_kango,
        $arr_nikkin_jokin_jun_kango,$arr_nikkin_hi_jokin_jun_kango,
        $arr_nikkin_jokin_hojo,$arr_nikkin_hi_jokin_hojo,
        $arr_hankin_jokin_kango,$arr_hankin_hi_jokin_kango,
        $arr_hankin_jokin_jun_kango,$arr_hankin_hi_jokin_jun_kango,
        $arr_hankin_jokin_hojo,$arr_hankin_hi_jokin_hojo,
        $arr_jokin_kokyu,$arr_jokin_nenkyu,
        $arr_hi_jokin_kokyu,$arr_hi_jokin_nenkyu,
        $arr_jokin_other,$arr_hi_jokin_other		
        ) 
    =get_building_manage_diary_data($con,$yyyymmdd,$group_id,$fname);
	
//振り分けられたメンバーを使用して表示用データに編集する
$download_data = assemble_data($arr_ya_ake_jokin_kango, $arr_ya_ake_hi_jokin_kango,
		$arr_ya_ake_jokin_jun_kango,$arr_ya_ake_hi_jokin_jun_kango,
		$arr_ya_ake_jokin_hojo,$arr_ya_ake_hi_jokin_hojo,
		$arr_ya_iri_jokin_kango,$arr_ya_iri_hi_jokin_kango,
		$arr_ya_iri_jokin_jun_kango,$arr_ya_iri_hi_jokin_jun_kango,
		$arr_ya_iri_jokin_hojo,$arr_ya_iri_hi_jokin_hojo,
		$arr_nikkin_jokin_kango,$arr_nikkin_hi_jokin_kango,
		$arr_nikkin_jokin_jun_kango,$arr_nikkin_hi_jokin_jun_kango,
		$arr_nikkin_jokin_hojo,$arr_nikkin_hi_jokin_hojo,
		$arr_hankin_jokin_kango,$arr_hankin_hi_jokin_kango,
		$arr_hankin_jokin_jun_kango,$arr_hankin_hi_jokin_jun_kango,
		$arr_hankin_jokin_hojo,$arr_hankin_hi_jokin_hojo,
		$arr_jokin_kokyu,$arr_jokin_nenkyu,
		$arr_hi_jokin_kokyu,$arr_hi_jokin_nenkyu,
        $arr_jokin_other,$arr_hi_jokin_other,
		$yyyymmdd,$group_name);
	


///-----------------------------------------------------------------------------
//ファイル名作成
///-----------------------------------------------------------------------------
$filename = 'shift_'.$yyyymmdd.'.xls';
if (ereg(' MSIE ', $_SERVER["HTTP_USER_AGENT"])) {
	$encoding = 'sjis';
} else {
	$encoding = mb_http_output();   // Firefox
}
$filename = mb_convert_encoding(
		$filename,
		$encoding,
		mb_internal_encoding());



ob_clean();
header("Content-Type: application/vnd.ms-excel");
header('Content-Disposition: attachment; filename=' . $filename);
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0,pre-check=0');
header('Pragma: public');
echo mb_convert_encoding(nl2br($download_data), 'sjis', mb_internal_encoding());

ob_end_flush();

pg_close($con);


/**
 * assemble_data 振り分けられたメンバーを使用して表示用データに編集する
 *
 * @param mixed $?? 下記配列参照
 *	$arr_ya_ake_jokin_kango  = array();			//夜勤明け、常勤、看護師メンバー
 *	$arr_ya_ake_hi_jokin_kango  = array();		//夜勤明け、非常勤、看護師メンバー
 *	
 *	$arr_ya_ake_jokin_jun_kango  = array();		//夜勤明け、常勤、准看護師メンバー
 *	$arr_ya_ake_hi_jokin_jun_kango  = array();	//夜勤明け、非常勤、准看護師メンバー
 *	
 *	$arr_ya_ake_jokin_hojo  = array();			//夜勤明け、常勤、補助メンバー
 *	$arr_ya_ake_hi_jokin_hojo  = array();		//夜勤明け、非常勤、補助メンバー
 *	
 *	$arr_ya_iri_jokin_kango  = array();			//夜勤入り、常勤、看護師メンバー
 *	$arr_ya_iri_hi_jokin_kango  = array();		//夜勤入り、非常勤、看護師メンバー
 *	
 *	$arr_ya_iri_jokin_jun_kango  = array();		//夜勤入り、常勤、准看護師メンバー
 *	$arr_ya_iri_hi_jokin_jun_kango  = array();	//夜勤入り、非常勤、准看護師メンバー
 *	
 *	$arr_ya_iri_jokin_hojo  = array();			//夜勤入り、常勤、補助メンバー
 *	$arr_ya_iri_hi_jokin_hojo  = array();		//夜勤入り、非常勤、補助メンバー
 *
 *	$arr_nikkin_jokin_kango  = array();			//日勤、常勤、看護師メンバー
 *	$arr_nikkin_hi_jokin_kango  = array();		//日勤、非常勤、看護師メンバー
 *	
 *	$arr_nikkin_jokin_jun_kango  = array();		//日勤、常勤、准看護師メンバー
 *	$arr_nikkin_hi_jokin_jun_kango  = array();	//日勤、非常勤、准看護師メンバー
 *	
 *	$arr_nikkin_jokin_hojo  = array();			//日勤、常勤、補助メンバー
 *	$arr_nikkin_hi_jokin_hojo  = array();		//日勤、非常勤、補助メンバー
 *	
 *	$arr_hankin_jokin_kango  = array();			//半勤、常勤、看護師メンバー
 *	$arr_hankin_hi_jokin_kango  = array();		//半勤、非常勤、看護師メンバー
 *	
 *	$arr_hankin_jokin_jun_kango  = array();		//半勤、常勤、准看護師メンバー
 *	$arr_hankin_hi_jokin_jun_kango  = array();	//半勤、非常勤、准看護師メンバー
 *	
 *	$arr_hankin_jokin_hojo  = array();			//半勤、常勤、補助メンバー
 *	$arr_hankin_hi_jokin_hojo  = array();		//半勤、非常勤、補助メンバー
 *	
 *	$arr_jokin_kokyu  = array();				//常勤、公休メンバー
 *	$arr_jokin_nenkyu  = array();				//常勤、年休メンバー
 *	
 *	$arr_hi_jokin_kokyu  = array();				//非常勤、公休メンバー
 *	$arr_hi_jokin_nenkyu  = array();			//非常勤、年休メンバー
 *
 *  $arr_jokin_other  = array();				//常勤、その他メンバー
 *  $arr_hi_jokin_other  = array();				//非常勤、その他メンバー
 *
 *	$yyyymmdd	 								//指定された日付
 *	$group_name									//指定されたグループ名
 *
 * @return mixed data エクセルに表示するデータ
 */
function assemble_data($arr_ya_ake_jokin_kango, $arr_ya_ake_hi_jokin_kango,
	$arr_ya_ake_jokin_jun_kango,$arr_ya_ake_hi_jokin_jun_kango,
	$arr_ya_ake_jokin_hojo,$arr_ya_ake_hi_jokin_hojo,
	$arr_ya_iri_jokin_kango,$arr_ya_iri_hi_jokin_kango,
	$arr_ya_iri_jokin_jun_kango,$arr_ya_iri_hi_jokin_jun_kango,
	$arr_ya_iri_jokin_hojo,$arr_ya_iri_hi_jokin_hojo,
	$arr_nikkin_jokin_kango,$arr_nikkin_hi_jokin_kango,
	$arr_nikkin_jokin_jun_kango,$arr_nikkin_hi_jokin_jun_kango,
	$arr_nikkin_jokin_hojo,$arr_nikkin_hi_jokin_hojo,
	$arr_hankin_jokin_kango,$arr_hankin_hi_jokin_kango,
	$arr_hankin_jokin_jun_kango,$arr_hankin_hi_jokin_jun_kango,
	$arr_hankin_jokin_hojo,$arr_hankin_hi_jokin_hojo,
	$arr_jokin_kokyu,$arr_jokin_nenkyu,
	$arr_hi_jokin_kokyu,$arr_hi_jokin_nenkyu,
    $arr_jokin_other,$arr_hi_jokin_other,
	$yyyymmdd,$group_name) 
{
	//titleの配列はduty_shift_diary_bld_manage_common.phpへ移動
    global $arr_title; //"head1"=>"看護職員状況","head2-1"=>"夜勤明"等
	global $arr_title2; //"duty"=>"当直医師"等
    global $arr_title3; //"head1"=>"重症・要注意・手術・その他"等
	
	$data = "";
	// metaタグで文字コード設定必須（ない場合にはMacで文字化けするため）
	$data .= "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Shift_JIS\">";
	
	$data .= "<table border=\"0\" cellspacing=\"2\" cellpadding=\"2\">";
	$data .= "<tr align=\"right\"><td colspan=\"19\"><font size=\"4\">シフトグループ：".$group_name."　　".substr($yyyymmdd,0,4)."年".substr($yyyymmdd,4,2)."月".substr($yyyymmdd,6,2)."日</font></td></tr>";
	$data .= "<tr><td  colspan=\"19\" align=\"left\" nowrap><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".$arr_title2["duty"]." ".$arr_title2["internist"]."　　　　　　　　　　　　　　　".$arr_title2["surgeon"]."　　　　　　　　　　　　　　　".$arr_title2["charge"]."</font></td></tr>";
	$data .= "</table>";


	$data .= "<table border=\"1\" cellspacing=\"2\" cellpadding=\"2\">";
	$data .= "<tr align=\"center\">";
	$data .= "<td colspan=\"19\"><font size=\"4\">".$arr_title["head1"]."  </font></td>";
	$data .= "</tr>";	
	$data .= "<tr align=\"center\">";
	$data .= "<td rowspan=\"2\">　</td>";
	$data .= "<td colspan=\"3\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\">".$arr_title["head2-1"]."</font></td>";
	$data .= "<td colspan=\"3\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\">".$arr_title["head2-2"]."</font></td>";
	$data .= "<td colspan=\"3\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\">".$arr_title["head2-3"]."</font></td>";
	$data .= "<td colspan=\"3\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\">".$arr_title["head2-4"]."</font></td>";
	$data .= "<td>　</td>";
	$data .= "<td colspan=\"4\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\">".$arr_title["head2-5"]."</font></td>";
	$data .= "<td rowspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\">".$arr_title["gokei"]."</font></td>";
	$data .= "</tr>";
	$data .= "<tr align=\"center\">";

	

	//4回ループ…夜勤明、日勤、半勤、夜勤入のそれぞれに看准補をそれぞれぶら下げる 
	for ($i=0; $i<4 ;$i++) 
	{
			$data .= "<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\">".$arr_title["head3-1"]."</font></td>";
			$data .= "<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\">".$arr_title["head3-2"]."</font></td>";
			$data .= "<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\">".$arr_title["head3-3"]."</font></td>";
	}
		
	$data .= "<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\">".$arr_title["head3-4"]."</font></td>";
	$data .= "<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\">".$arr_title["head3-5"]."</font></td>";
	$data .= "<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\">".$arr_title["head3-6"]."</font></td>";
    $data .= "<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\">".$arr_title["head3-7"]."</font></td>";
	$data .= "<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\">".$arr_title["head3-4"]."</font></td>";
	$data .= "</tr>";
	
	$data .= "<tr align=\"left\" valign=\"top\">";
	$data .= "<td valign=\"middle\" align=\"middle\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">常勤</font></td>";
	

	$data .= "<td>";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//夜勤明け、常勤、看護師メンバー
	for ($j=0; $j<count($arr_ya_ake_jokin_kango); $j++) 
	{
		$data .=$arr_ya_ake_jokin_kango[$j]["emp_lt_nm"];
		$data .=$arr_ya_ake_jokin_kango[$j]["emp_ft_nm"];
		$data .="<br>";
	}
	$data .= "</font></td>";
	


	$data .= "<td>";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";

	//夜勤明け、常勤、准看護師メンバー
	for ($j=0; $j<count($arr_ya_ake_jokin_jun_kango); $j++) 
	{
		$data .= $arr_ya_ake_jokin_jun_kango[$j]["emp_lt_nm"];
		$data .= $arr_ya_ake_jokin_jun_kango[$j]["emp_ft_nm"];
		$data .= "<br>";
	}
	$data .= "</font></td>";
	
	

	$data .= "<td>";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";

	//夜勤明け、常勤、補助看護師メンバー
	for ($j=0; $j<count($arr_ya_ake_jokin_hojo); $j++) 
	{
		$data .= $arr_ya_ake_jokin_hojo[$j]["emp_lt_nm"];
		$data .= $arr_ya_ake_jokin_hojo[$j]["emp_ft_nm"];
		$data .= "<br>";
	}
	$data .= "</font></td>";
	

	$data .= "<td>";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//日勤、常勤、看護師メンバー
	for ($j=0; $j<count($arr_nikkin_jokin_kango); $j++) 
	{
		$data .= $arr_nikkin_jokin_kango[$j]["emp_lt_nm"];
		$data .= $arr_nikkin_jokin_kango[$j]["emp_ft_nm"];
		$data .= "<br>";
	}
	$data .= "</font></td>";
	
	$data .= "<td>";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//日勤、常勤、准看護師メンバー
	for ($j=0; $j<count($arr_nikkin_jokin_jun_kango); $j++) 
	{
		$data .= $arr_nikkin_jokin_jun_kango[$j]["emp_lt_nm"];
		$data .= $arr_nikkin_jokin_jun_kango[$j]["emp_ft_nm"];
		$data .= "<br>";
	}
	$data .= "</font></td>";
	

	$data .= "<td>";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//日勤、常勤、補助看護師メンバー
	for ($j=0; $j<count($arr_nikkin_jokin_hojo); $j++) 
	{
		$data .= $arr_nikkin_jokin_hojo[$j]["emp_lt_nm"];
		$data .= $arr_nikkin_jokin_hojo[$j]["emp_ft_nm"];
		$data .= "<br>";
	}
	$data .= "</font></td>";
	

	$data .= "<td>";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//半勤、常勤、看護師メンバー
	for ($j=0; $j<count($arr_hankin_jokin_kango); $j++) 
	{
		$data .= $arr_hankin_jokin_kango[$j]["emp_lt_nm"];
		$data .= $arr_hankin_jokin_kango[$j]["emp_ft_nm"];
		$data .= "<br>";
	}
	$data .= "</font></td>";
	

	$data .= "<td>";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//半勤、常勤、准看護師メンバー
	for ($j=0; $j<count($arr_hankin_jokin_jun_kango); $j++) 
	{
		$data .= $arr_hankin_jokin_jun_kango[$j]["emp_lt_nm"];
		$data .= $arr_hankin_jokin_jun_kango[$j]["emp_ft_nm"];
		$data .= "<br>";
	}
	$data .= "</font></td>";
	

	$data .= "<td>";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//半勤、常勤、補助看護師メンバー
	for ($j=0; $j<count($arr_hankin_jokin_hojo); $j++) 
	{
		$data .= $arr_hankin_jokin_hojo[$j]["emp_lt_nm"];
		$data .= $arr_hankin_jokin_hojo[$j]["emp_ft_nm"];
		$data .= "<br>";
	}
	$data .= "</font></td>";
	

	$data .= "<td>";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//夜勤入り、常勤、看護師メンバー
	for ($j=0; $j<count($arr_ya_iri_jokin_kango); $j++) 
	{
		$data .= $arr_ya_iri_jokin_kango[$j]["emp_lt_nm"];
		$data .= $arr_ya_iri_jokin_kango[$j]["emp_ft_nm"];
		$data .= "<br>";
	}
	$data .= "</font></td>";
	

	$data .= "<td>";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//夜勤入り、常勤、准看護師メンバー
	for ($j=0; $j<count($arr_ya_iri_jokin_jun_kango); $j++) 
	{
		$data .= $arr_ya_iri_jokin_jun_kango[$j]["emp_lt_nm"];
		$data .= $arr_ya_iri_jokin_jun_kango[$j]["emp_ft_nm"];
		$data .= "<br>";
	}
	$data .= "</font></td>";
	

	$data .= "<td>";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//夜勤入り、常勤、補助看護師メンバー
	for ($j=0; $j<count($arr_ya_iri_jokin_hojo); $j++) 
	{
		$data .= $arr_ya_iri_jokin_hojo[$j]["emp_lt_nm"];
		$data .= $arr_ya_iri_jokin_hojo[$j]["emp_ft_nm"];
		$data .= "<br>";
	}
	$data .= "</font></td>";
	

	$data .= "<td valign=\"middle\" align=\"middle\">";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//常勤　小計
	$jokin_total_count= count($arr_ya_ake_jokin_kango) + 
	count($arr_ya_ake_jokin_jun_kango) + 
	count($arr_ya_ake_jokin_hojo) + 
	count($arr_nikkin_jokin_kango) + 
	count($arr_nikkin_jokin_jun_kango) + 
	count($arr_nikkin_jokin_hojo) + 
	count($arr_hankin_jokin_kango) + 
	count($arr_hankin_jokin_jun_kango) + 
	count($arr_hankin_jokin_hojo) + 
	count($arr_ya_iri_jokin_kango) + 
	count($arr_ya_iri_jokin_jun_kango) + 
	count($arr_ya_iri_jokin_hojo) ;
	
	$data .= $jokin_total_count;
	$data .= "</font></td>";
	


	$data .= "<td>";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//常勤、公休メンバー
	for ($j=0; $j<count($arr_jokin_kokyu); $j++) 
	{
		$data .= $arr_jokin_kokyu[$j]["emp_lt_nm"];
		$data .= $arr_jokin_kokyu[$j]["emp_ft_nm"];
		$data .= "<br>";
	}
	$data .= "</font></td>";
	
	
	$data .= "<td>";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//常勤、年休メンバー
	for ($j=0; $j<count($arr_jokin_nenkyu); $j++) 
	{
		$data .= $arr_jokin_nenkyu[$j]["emp_lt_nm"];
		$data .= $arr_jokin_nenkyu[$j]["emp_ft_nm"];
		$data .= "<br>";
	}
	$data .= "</font></td>";
	

    //常勤、その他メンバー
    $data .= "<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
    for ($j=0; $j<count($arr_jokin_other); $j++) 
    {
        $data .= $arr_jokin_other[$j]["emp_lt_nm"];
        $data .= $arr_jokin_other[$j]["emp_ft_nm"];
        $data .= "<br>";
    }
    $data .= "</font></td>";
    
	$data .= "<td valign=\"middle\" align=\"middle\">";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//常勤　休暇小計
    $jokin_kyuka_total_count= count($arr_jokin_kokyu) + count($arr_jokin_nenkyu) + count($arr_jokin_other);
	$data .= $jokin_kyuka_total_count;
		
	$data .= "</font></td>";
	

	$data .= "<td valign=\"middle\" align=\"middle\">";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//常勤　合計
	$jokin_all_total_count= $jokin_total_count + $jokin_kyuka_total_count;
	$data .= $jokin_all_total_count;
	
	$data .= "</font></td>";
	
	$data .= "</tr>";
	

	$data .= "<tr  align=\"left\" valign=\"top\">";
	$data .= "<td valign=\"middle\" align=\"middle\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">非常勤</font></td>";
	
	
	$data .= "<td>";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//夜勤明け、非常勤、看護師メンバー
	for ($j=0; $j<count($arr_ya_ake_hi_jokin_kango); $j++) 
	{
		$data .= $arr_ya_ake_hi_jokin_kango[$j]["emp_lt_nm"];
		$data .= $arr_ya_ake_hi_jokin_kango[$j]["emp_ft_nm"];
		$data .= "<br>";
	}
	$data .= "</font></td>";
	

	$data .= "<td>";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//夜勤明け、非常勤、准看護師メンバー
	for ($j=0; $j<count($arr_ya_ake_hi_jokin_jun_kango); $j++) 
	{
		$data .= $arr_ya_ake_hi_jokin_jun_kango[$j]["emp_lt_nm"];
		$data .= $arr_ya_ake_hi_jokin_jun_kango[$j]["emp_ft_nm"];
		$data .= "<br>";
	}
	$data .= "</font></td>";
	

	$data .= "<td>";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//夜勤明け、非常勤、補助看護師メンバー
	for ($j=0; $j<count($arr_ya_ake_hi_jokin_hojo); $j++) 
	{
		$data .= $arr_ya_ake_hi_jokin_hojo[$j]["emp_lt_nm"];
		$data .= $arr_ya_ake_hi_jokin_hojo[$j]["emp_ft_nm"];
		$data .= "<br>";
	}
	$data .= "</font></td>";
	

	$data .= "<td>";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//日勤、非常勤、看護師メンバー
	for ($j=0; $j<count($arr_nikkin_hi_jokin_kango); $j++) 
	{
		$data .= $arr_nikkin_hi_jokin_kango[$j]["emp_lt_nm"];
		$data .= $arr_nikkin_hi_jokin_kango[$j]["emp_ft_nm"];
		$data .= "<br>";
	}
	$data .= "</font></td>";
	
	
	$data .= "<td>";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//日勤、非常勤、准看護師メンバー
	for ($j=0; $j<count($arr_nikkin_hi_jokin_jun_kango); $j++) 
	{
		$data .= $arr_nikkin_hi_jokin_jun_kango[$j]["emp_lt_nm"];
		$data .= $arr_nikkin_hi_jokin_jun_kango[$j]["emp_ft_nm"];
		$data .= "<br>";
	}
	$data .= "</font></td>";
	
	
	
	$data .= "<td>";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//日勤、非常勤、補助看護師メンバー
	for ($j=0; $j<count($arr_nikkin_hi_jokin_hojo); $j++) 
	{
		$data .= $arr_nikkin_hi_jokin_hojo[$j]["emp_lt_nm"];
		$data .= $arr_nikkin_hi_jokin_hojo[$j]["emp_ft_nm"];
		$data .= "<br>";
	}
	$data .= "</font></td>";
	


	$data .= "<td>";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//半勤、非常勤、看護師メンバー
	for ($j=0; $j<count($arr_hankin_hi_jokin_kango); $j++) 
	{
		$data .= $arr_hankin_hi_jokin_kango[$j]["emp_lt_nm"];
		$data .= $arr_hankin_hi_jokin_kango[$j]["emp_ft_nm"];
		$data .= "<br>";
	}
	$data .= "</font></td>";
	

	$data .= "<td>";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//半勤、非常勤、准看護師メンバー
	for ($j=0; $j<count($arr_hankin_hi_jokin_jun_kango); $j++) 
	{
		$data .= $arr_hankin_hi_jokin_jun_kango[$j]["emp_lt_nm"];
		$data .= $arr_hankin_hi_jokin_jun_kango[$j]["emp_ft_nm"];
		$data .= "<br>";
	}
	$data .= "</font></td>";
	

	$data .= "<td>";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//半勤、非常勤、補助看護師メンバー
	for ($j=0; $j<count($arr_hankin_hi_jokin_hojo); $j++) 
	{
		$data .= $arr_hankin_hi_jokin_hojo[$j]["emp_lt_nm"];
		$data .= $arr_hankin_hi_jokin_hojo[$j]["emp_ft_nm"];
		$data .= "<br>";
	}
	$data .= "</font></td>";
	


	$data .= "<td>";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//夜勤入り、非常勤、看護師メンバー
	for ($j=0; $j<count($arr_ya_iri_hi_jokin_kango); $j++) 
	{
		$data .= $arr_ya_iri_hi_jokin_kango[$j]["emp_lt_nm"];
		$data .= $arr_ya_iri_hi_jokin_kango[$j]["emp_ft_nm"];
		$data .= "<br>";
	}
	$data .= "</font></td>";
	

	$data .= "<td>";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//夜勤入り、非常勤、准看護師メンバー
	for ($j=0; $j<count($arr_ya_iri_hi_jokin_jun_kango); $j++) 
	{
		$data .= $arr_ya_iri_hi_jokin_jun_kango[$j]["emp_lt_nm"];
		$data .= $arr_ya_iri_hi_jokin_jun_kango[$j]["emp_ft_nm"];
		$data .= "<br>";
	}
	$data .= "</font></td>";
	
	
	$data .= "<td>";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//夜勤入り、非常勤、補助看護師メンバー
	for ($j=0; $j<count($arr_ya_iri_hi_jokin_hojo); $j++) 
	{
		$data .= $arr_ya_iri_hi_jokin_hojo[$j]["emp_lt_nm"];
		$data .= $arr_ya_iri_hi_jokin_hojo[$j]["emp_ft_nm"];
		$data .= "<br>";
	}
	$data .= "</font></td>";
	
	

	$data .= "<td valign=\"middle\" align=\"middle\">";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//常勤　小計
	$hi_jokin_total_count= count($arr_ya_ake_hi_jokin_kango) + 
	count($arr_ya_ake_hi_jokin_jun_kango) + 
	count($arr_ya_ake_hi_jokin_hojo) + 
	count($arr_nikkin_hi_jokin_kango) + 
	count($arr_nikkin_hi_jokin_jun_kango) + 
	count($arr_nikkin_hi_jokin_hojo) + 
	count($arr_hankin_hi_jokin_kango) + 
	count($arr_hankin_hi_jokin_jun_kango) + 
	count($arr_hankin_hi_jokin_hojo) + 
	count($arr_ya_iri_hi_jokin_kango) + 
	count($arr_ya_iri_hi_jokin_jun_kango) + 
	count($arr_ya_iri_hi_jokin_hojo) ;
	
	$data .= $hi_jokin_total_count;
	$data .= "</font></td>";
	



	$data .= "<td>";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//非常勤、公休メンバー
	for ($j=0; $j<count($arr_hi_jokin_kokyu); $j++) 
	{
		$data .= $arr_hi_jokin_kokyu[$j]["emp_lt_nm"];
		$data .= $arr_hi_jokin_kokyu[$j]["emp_ft_nm"];
		$data .= "<br>";
	}
	$data .= "</font></td>";
	

	$data .= "<td>";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//非常勤、年休メンバー
	for ($j=0; $j<count($arr_hi_jokin_nenkyu); $j++) 
	{
		$data .= $arr_hi_jokin_nenkyu[$j]["emp_lt_nm"];
		$data .= $arr_hi_jokin_nenkyu[$j]["emp_ft_nm"];
		$data .= "<br>";
	}
	$data .= "</font></td>";
	

    //常勤、その他メンバー
    $data .= "<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
    for ($j=0; $j<count($arr_hi_jokin_other); $j++) 
    {
        $data .= $arr_hi_jokin_other[$j]["emp_lt_nm"];
        $data .= $arr_hi_jokin_other[$j]["emp_ft_nm"];
        $data .= "<br>";
    }
    $data .= "</font></td>";
    
	$data .= "<td valign=\"middle\" align=\"middle\">";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//非常勤　休暇小計
    $hi_jokin_kyuka_total_count= count($arr_hi_jokin_kokyu) + count($arr_hi_jokin_nenkyu) + count($arr_hi_jokin_other);
		
	$data .= $hi_jokin_kyuka_total_count;
	$data .= "</font></td>";
	
	

	$data .= "<td valign=\"middle\" align=\"middle\">";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//非常勤　合計
	$hi_jokin_all_total_count= $hi_jokin_total_count + $hi_jokin_kyuka_total_count;
		
	$data .= $hi_jokin_all_total_count;
	$data .= "</font></td>";
	
	
	$data .= "</tr>";
	
	
	$data .= "<tr align=\"center\">";
	$data .= "<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">合計</font></td>";
	
	
	$data .= "<td>";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//夜勤明、看護師、合計
	$yakin_ake_kango_gokei= count($arr_ya_ake_jokin_kango) + count($arr_ya_ake_hi_jokin_kango);
		
	$data .= $yakin_ake_kango_gokei;
	$data .= "</font></td>";
	
	$data .= "<td>";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//夜勤明、准看護師、合計
	$yakin_ake_jun_kango_gokei= count($arr_ya_ake_jokin_jun_kango) + count($arr_ya_ake_hi_jokin_jun_kango);
		
	$data .= $yakin_ake_jun_kango_gokei;
	$data .= "</font></td>";
	
	
	$data .= "<td>";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//夜勤明、補助看護師、合計
	$yakin_ake_hojo_kango_gokei= count($arr_ya_ake_jokin_hojo) + count($arr_ya_ake_hi_jokin_hojo);
		
	$data .= $yakin_ake_hojo_kango_gokei;
	$data .= "</font></td>";
	

	$data .= "<td>";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//日勤、看護師、合計
	$nikkin_kango_gokei= count($arr_nikkin_jokin_kango) + count($arr_nikkin_hi_jokin_kango);
		
	$data .= $nikkin_kango_gokei;
	$data .= "</font></td>";
	

	$data .= "<td>";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//日勤、准看護師、合計
	$nikkin_jun_kango_gokei= count($arr_nikkin_jokin_jun_kango) + count($arr_nikkin_hi_jokin_jun_kango);
		
	$data .= $nikkin_jun_kango_gokei;
	$data .= "</font></td>";
	

	$data .= "<td>";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//日勤、補助看護師、合計
	$nikkin_hojo_kango_gokei= count($arr_nikkin_jokin_hojo) + count($arr_nikkin_hi_jokin_hojo);
		
	$data .= $nikkin_hojo_kango_gokei;
	$data .= "</font></td>";
	
	
	$data .= "<td>";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//半勤、看護師、合計
	$hankin_kango_gokei= count($arr_hankin_jokin_kango) + count($arr_hankin_hi_jokin_kango);
		
	$data .= $hankin_kango_gokei;
	$data .= "</font></td>";
	

	$data .= "<td>";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//半勤、准看護師、合計
	$hankin_jun_kango_gokei= count($arr_hankin_jokin_jun_kango) + count($arr_hankin_hi_jokin_jun_kango);
		
	$data .= $hankin_jun_kango_gokei;
	$data .= "</font></td>";
	

	$data .= "<td>";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//半勤、補助看護師、合計
	$hankin_hojo_kango_gokei= count($arr_hankin_jokin_hojo) + count($arr_hankin_hi_jokin_hojo);
		
	$data .= $hankin_hojo_kango_gokei;
	$data .= "</font></td>";
	

	$data .= "<td>";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//夜勤入、看護師、合計
	$yakin_iri_kango_gokei= count($arr_ya_iri_jokin_kango) + count($arr_ya_iri_hi_jokin_kango);
		
	$data .= $yakin_iri_kango_gokei;
	$data .= "</font></td>";
	

	$data .= "<td>";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//夜勤入、准看護師、合計
	$yakin_iri_jun_kango_gokei= count($arr_ya_iri_jokin_jun_kango) + count($arr_ya_iri_hi_jokin_jun_kango);
		
	$data .= $yakin_iri_jun_kango_gokei;
	$data .= "</font></td>";
	


	$data .= "<td>";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//夜勤入、補助看護師、合計
	$yakin_iri_hojo_kango_gokei= count($arr_ya_iri_jokin_hojo) + count($arr_ya_iri_hi_jokin_hojo);
		
	$data .= $yakin_iri_hojo_kango_gokei;
	$data .= "</font></td>";
	


	$data .= "<td>";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//常勤、非常勤　合計
	$joukin_hi_joukin_gokei= $jokin_total_count + $hi_jokin_total_count;
		
	$data .= $joukin_hi_joukin_gokei;
	$data .= "</font></td>";
	

	$data .= "<td>";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//常勤、非常勤　公休合計
	$kokyu_gokei= count($arr_jokin_kokyu) + count($arr_hi_jokin_kokyu);
		
	$data .= $kokyu_gokei;
	$data .= "</font></td>";
	

	$data .= "<td>";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//常勤、非常勤　年休合計
	$nenkyu_gokei= count($arr_jokin_nenkyu) + count($arr_hi_jokin_nenkyu);
		
	$data .= $nenkyu_gokei;
	$data .= "</font></td>";
	

    //<!-- その他行 -->
	$data .= "<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
    $other_gokei= count($arr_jokin_other) + count($arr_hi_jokin_other);
    $data .= $other_gokei;
    $data .= "</font></td>";
    
	$data .= "<td>";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
	//公休・年休合計
    $kokyu_nenkyu_gokei= $kokyu_gokei + $nenkyu_gokei + $other_gokei;
		
	$data .= $kokyu_nenkyu_gokei;
	$data .= "</font></td>";
	

	$data .= "<td>";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	
		//全合計
	$all_gokei= $jokin_all_total_count + $hi_jokin_all_total_count;
		
	$data .= $all_gokei;
	$data .= "</font></td>";
	
	
	$data .= "</tr>";
	$data .= "</table>";



	//<!-- ------------------------------------------------------------------------ -->
	//<!-- 表(重症・要注意・手術・その他以下) -->
	//<!-- ------------------------------------------------------------------------ -->
	$data .= "<table width=\"960\" border=\"1\" cellspacing=\"0\" cellpadding=\"2\" class=\"list\">";
			
	$data .= "<tr align=\"center\">";
	$data .= "<td colspan=\"19\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".$arr_title3["head1"]."</font></td>";
	$data .= "</tr>";
	$data .= "<tr align=\"center\">";
	$data .= "<td width=\"50\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".$arr_title3["head2-1"]."</font></td>";
	$data .= "<td width=\"50\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".$arr_title3["head2-2"]."</font></td>";
	$data .= "<td width=\"50\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".$arr_title3["head2-3"]."</font></td>";
	$data .= "<td width=\"150\" colspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".$arr_title3["head2-4"]."</font></td>";
	$data .= "<td width=\"50\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".$arr_title3["head2-5"]."</font></td>";
	$data .= "<td width=\"50\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".$arr_title3["head2-6"]."</font></td>";
	$data .= "<td width=\"150\" colspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".$arr_title3["head2-7"]."</font></td>";
	$data .= "<td width=\"400\" colspan=\"10\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".$arr_title3["head2-8"]."</font></td>";
	$data .= "</tr>";
			


	//13行ぶん空白行を表示する
	for ($bk_cnt=0; $bk_cnt<13; $bk_cnt++) 
	{

		$data .= "<tr align=\"center\">";
		$data .= "<td width=\"50\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><br></font></td>";
		$data .= "<td width=\"50\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"> </font></td>";
		$data .= "<td width=\"50\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"> </font></td>";
		$data .= "<td width=\"150\" colspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"> </font></td>";
		$data .= "<td width=\"50\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"> </font></td>";
		$data .= "<td width=\"50\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"> </font></td>";
		$data .= "<td width=\"150\" colspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"> </font></td>";
		$data .= "<td width=\"400\" colspan=\"10\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"> </font></td>";
		$data .= "</tr>";

	}
	$data .= "</table>";
			
			
		//<!-- ------------------------------------------------------------------------ -->
		//<!-- 表(巡視以下) -->
		//<!-- ------------------------------------------------------------------------ -->
		$data .= "<table width=\"960\" border=\"1\" cellspacing=\"0\" cellpadding=\"2\" class=\"list\">";

		$data .= "<tr align=\"center\">";
		$data .= "<td rowspan=\"3\"  width=\"30\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".$arr_title3["head3-1"]."</font></td>";
		$data .= "<td width=\"100\">";
		$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".$arr_title3["head3-2"]."</font>";
		$data .= "</td>";
	
		$data .= "<td align=\"left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\">0</font></td>";
		$data .= "<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\">1</font></td>";
		$data .= "<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\">2</font></td>";
		$data .= "<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\">3</font></td>";
		$data .= "<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\">4</font></td>";
		$data .= "<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\">5</font></td>";
		$data .= "<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\">6</font></td>";
		$data .= "<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\">7</font></td>";
		$data .= "<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\">8</font></td>";
		$data .= "<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\">9</font></td>";
		$data .= "<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\">10</font></td>";
		$data .= "<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\">11</font></td>";
		$data .= "</tr>";

		$data .= "<tr align=\"center\">";
		$data .= "<td width=\"100\">";
		$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".$arr_title3["head3-3"]."</font>";
		$data .= "</td>";
		$data .= "<td>　</td><td>　</td><td>　</td><td>　</td><td>　</td><td>　</td><td>　</td><td>　</td><td>　</td><td>　</td><td>　</td><td>　</td>";
		$data .= "</tr>";
		$data .= "<tr align=\"center\">";
		$data .= "<td width=\"100\">";
		$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".$arr_title3["head3-4"]."</font>";
		$data .= "</td>";
		$data .= "<td>　</td><td>　</td><td>　</td><td>　</td><td>　</td><td>　</td><td>　</td><td>　</td><td>　</td><td>　</td><td>　</td><td>　</td>";
		$data .= "</tr>";
		






	$data .= "<tr align=\"center\">";
	$data .= "<td rowspan=\"3\"  width=\"30\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".$arr_title3["head3-1"]."</font></td>";
	$data .= "<td width=\"100\">";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".$arr_title3["head3-2"]."</font>";
	$data .= "</td>";
	
	$data .= "<td align=\"left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\">12</font></td>";
	$data .= "<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\">13</font></td>";
	$data .= "<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\">14</font></td>";
	$data .= "<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\">15</font></td>";
	$data .= "<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\">16</font></td>";
	$data .= "<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\">17</font></td>";
	$data .= "<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\">18</font></td>";
	$data .= "<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\">19</font></td>";
	$data .= "<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\">20</font></td>";
	$data .= "<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\">21</font></td>";
	$data .= "<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\">22</font></td>";
	$data .= "<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\">23</font></td>";
	$data .= "</tr>";
	
	$data .= "<tr align=\"center\">";
	$data .= "<td width=\"100\">";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".$arr_title3["head3-3"]."</font>";
	$data .= "</td>";
	$data .= "<td>　</td>	<td>　</td><td>　</td><td>　</td><td>　</td><td>　</td><td>　</td><td>　</td><td>　</td><td>　</td><td>　</td><td>　</td>";
	$data .= "</tr>";
	$data .= "<tr align=\"center\">";
	$data .= "<td width=\"100\">";
	$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".$arr_title3["head3-4"]."</font>";
	$data .= "</td>";
	$data .= "<td>　</td><td>　</td><td>　</td><td>　</td><td>　</td><td>　</td><td>　</td><td>　</td><td>　</td><td>　</td><td>　</td><td>　</td>";
	$data .= "</tr>";
	





		
		
		
		
		
		$data .= "</tr>";
		$data .= "<tr align=\"left\" valign=\"top\">";
		$data .= "<td colspan=\"14\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".$arr_title3["head3-5"]."<br><br><br><br></font></td>";
		$data .= "</tr>";
		$data .= "</table>";
	
	return $data;

	
}


