<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_session.php");
require("about_authority.php");
require("show_date_navigation_year.ini");
require("menu_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 56, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 稼動状況統計設定権限の取得
$stat_setting_auth = check_authority($session, 52, $fname);

$link_id = $item_id;
list($item_id, $assist_id) = explode("_", $item_id);

// データベースに接続
$con = connect2db($fname);

// 管理項目名を取得
$sql = "select name from statitem";
$cond = "where statitem_id = $item_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$item_name = pg_fetch_result($sel, 0, "name");

// 補助項目の場合、補助項目情報を取得
if ($assist_id != "") {
	$sql = "select statassist_name, assist_type, calc_var1_type, calc_var1_const_id, calc_var1_item_id, calc_var1_assist_id, calc_operator, calc_var2_type, calc_var2_const_id, calc_var2_item_id, calc_var2_assist_id, calc_format from statassist";
	$cond = "where statitem_id = $item_id and statassist_id = $assist_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$assist_name = $row["statassist_name"];
		$assist_type = $row["assist_type"];
		$var1_type = $row["calc_var1_type"];
		$var1_const_id = $row["calc_var1_const_id"];
		$var1_item_id = $row["calc_var1_item_id"];
		$var1_assist_id = $row["calc_var1_assist_id"];
		$operator = $row["calc_operator"];
		$var2_type = $row["calc_var2_type"];
		$var2_const_id = $row["calc_var2_const_id"];
		$var2_item_id = $row["calc_var2_item_id"];
		$var2_assist_id = $row["calc_var2_assist_id"];
		$format = $row["calc_format"];
	}
}

// 定数一覧を取得
$sql = "select statconst_id, statconst_value from statconst";
$cond = "where del_flg = 'f' order by statconst_id";
$sel_const = select_from_table($con, $sql, $cond, $fname);
if ($sel_const == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$consts = array();
while ($row = pg_fetch_array($sel_const)) {
	$consts[$row["statconst_id"]] = $row["statconst_value"];
}

// 施設を取得
$sql = "select char(1) '1' as area_type, statfcl_id, name, area_id, (select order_no from area where area.area_id = statfcl.area_id) as order_no from statfcl where area_id is not null and del_flg = 'f' union select '2', statfcl_id, name, area_id, statfcl_id from statfcl where area_id is null and del_flg = 'f'";
$cond = "order by 1, order_no";
$sel_fcl = select_from_table($con, $sql, $cond, $fname);
if ($sel_fcl == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 当年の全統計値を取得
$selected_y = date("Y", $date);
$sql = "select statfcl_id, statitem_id, statassist_id, stat_date, stat_val from stat";
$cond = "where stat_date like '{$selected_y}____'";
$sel_stat = select_from_table($con, $sql, $cond, $fname);
if ($sel_stat == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$stats = array();
while ($row = pg_fetch_array($sel_stat)) {
	$tmp_fcl_id = $row["statfcl_id"];
	$tmp_item_id = $row["statitem_id"];
	$tmp_assist_id = $row["statassist_id"];
	$tmp_date = $row["stat_date"];
	$tmp_val = $row["stat_val"];

	if ($tmp_val === "") {
		continue;
	}

	if ($tmp_assist_id == "") {
		$tmp_id = $tmp_item_id;
	} else {
		$tmp_id = $tmp_item_id . "_" . $tmp_assist_id;
	}

	$tmp_month = intval(substr($tmp_date, 4, 2));
	$tmp_day = intval(substr($tmp_date, 6, 2));

	$stats[$tmp_fcl_id][$tmp_id][$tmp_month][$tmp_day] = $tmp_val;
}

// イントラメニュー情報を取得
$sql = "select menu1 from intramenu";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$menu1 = pg_fetch_result($sel, 0, "menu1");

$last_year = get_last_year($date);
$next_year = get_next_year($date);
$year = date("Y", $date);
$day = date("d", $date);
?>
<title>イントラネット | <? echo($menu1); ?></title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function changeDate(dt) {
	location.href = 'intra_stat_item_month.php?session=<? echo($session); ?>&item_id=<? echo($link_id); ?>&date=' + dt;
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-style:none;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="intra_menu.php?session=<? echo($session); ?>"><img src="img/icon/b38.gif" width="32" height="32" border="0" alt="イントラネット"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="intra_menu.php?session=<? echo($session); ?>"><b>イントラネット</b></a> &gt; <a href="intra_stat.php?session=<? echo($session); ?>"><b><? echo($menu1); ?></b></a></font></td>
<? if ($stat_setting_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="stat_master_menu.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="105" align="center" bgcolor="#bdd1e7"><a href="intra_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メインメニュー</font></a></td>
<td width="5">&nbsp;</td>
<td width="<? echo(get_tab_width($menu1)); ?>" align="center" bgcolor="#5279a5"><a href="intra_stat.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b><? echo($menu1); ?></b></font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="300"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="intra_stat_item_month.php?session=<? echo($session); ?>&item_id=<? echo($link_id); ?>&date=<? echo($last_year); ?>">&lt;前年</a>&nbsp;<select onchange="changeDate(this.value);" style="vertical-align:middle;"><? show_date_options_y($last_year, $date, $next_year, 2); ?></select>&nbsp;<a href="intra_stat_item_month.php?session=<? echo($session); ?>&item_id=<? echo($link_id); ?>&date=<? echo($next_year); ?>">翌年&gt;</a></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="intra_stat.php?session=<? echo($session); ?>&date=<? echo($date); ?>">日</a></font></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff" valign="top">
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b><? echo($item_name); ?><? if ($assist_id != "") { ?><br>&nbsp;&gt; <? echo($assist_name); ?><? } ?></b></font></td>
<?
for ($i = 1; $i <= 12; $i++) {
	$tmp_date = mktime(0, 0, 0, $i, $day, $year);
	echo("<td width=\"40\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"intra_stat_item_day.php?session=$session&item_id=$link_id&date=$tmp_date\">$i</a></font></td>\n");
}
?>
</tr>
<?
while ($row = pg_fetch_array($sel_fcl)) {
	$fcl_id = $row["statfcl_id"];
	$fcl_name = $row["name"];

	// 計算系の補助項目の場合
	if ($assist_type == "2") {

		// 項目1が定数の場合
		if ($var1_type == "1") {
			for ($i = 1; $i <= 12; $i++) {
				$var1_array[$i] = array_fill(1, 31, $consts[$var1_const_id]);
			}

		// 項目1が項目指定の場合
		} else {
			$id = $var1_item_id;
			if ($var1_assist_id != "") {
				$id .= "_$var1_assist_id";
			}
			$var1_array = $stats[$fcl_id][$id];
		}

		// 項目2が定数の場合
		if ($var2_type == "1") {
			for ($i = 1; $i <= 12; $i++) {
				$var2_array[$i] = array_fill(1, 31, $consts[$var2_const_id]);
			}

		// 項目2が項目指定の場合
		} else {
			$id = $var2_item_id;
			if ($var2_assist_id != "") {
				$id .= "_$var2_assist_id";
			}
			$var2_array = $stats[$fcl_id][$id];
		}

		// 計算処理
		$vals = array();
		for ($i = 1; $i <= 12; $i++) {
			$vals[$i] = array();

			for ($j = 1; $j <= 31; $j++) {
				if (!isset($var1_array[$i][$j]) || !isset($var2_array[$i][$j])) {
					continue;
				}

				$var1_val = floatval($var1_array[$i][$j]);
				$var2_val = floatval($var2_array[$i][$j]);

				switch ($operator) {
				case "1":  // ＋
					$vals[$i][$j] = $var1_val + $var2_val;
					break;
				case "2":  // −
					$vals[$i][$j] = $var1_val - $var2_val;
					break;
				case "3":  // ×
					$vals[$i][$j] = $var1_val * $var2_val;
					break;
				case "4":  // ÷
					if ($var2_val <> 0) {
						$vals[$i][$j] = $var1_val / $var2_val;
					}
					break;
				}
			}
		}
	}

	echo("<tr height=\"22\">\n");
	echo("<td rowspan=\"3\" width=\"150\" bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$fcl_name</font></td>\n");
	echo("<td width=\"50\" bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">平均</font></td>\n");
	for ($i = 1; $i <= 12; $i++) {
		echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");

		// 管理項目または入力系の補助項目の場合
		if ($assist_type != "2") {
			$stat_count = count($stats[$fcl_id][$link_id][$i]);
			if ($stat_count > 0) {
				echo(round((array_sum($stats[$fcl_id][$link_id][$i]) / $stat_count), 1));
			}
		} else {
			$stat_count = count($vals[$i]);
			if ($stat_count > 0) {
				$val = (array_sum($vals[$i]) / $stat_count * 100) / 100;

				// 実数表示の場合
				if ($format == "1") {
					echo round($val, 1);

				// %表示の場合
				} else {
					$val *= 100;
					echo round($val, 1) . "%";
				}
			}
		}

		echo("</font></td>");
	}
	echo("</tr>\n");
	echo("<tr height=\"22\">\n");
	echo("<td bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">最大</font></td>\n");
	for ($i = 1; $i <= 12; $i++) {
		echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");

		// 管理項目または入力系の補助項目の場合
		if ($assist_type != "2") {
			echo(max($stats[$fcl_id][$link_id][$i]));
		} else {
			$stat_count = count($vals[$i]);
			if ($stat_count > 0) {
				$val = max($vals[$i]);

				// 実数表示の場合
				if ($format == "1") {
					echo round($val, 1);

				// %表示の場合
				} else {
					$val *= 100;
					echo round($val, 1) . "%";
				}
			}
		}

		echo("</font></td>");
	}
	echo("</tr>\n");
	echo("<tr height=\"22\">\n");
	echo("<td bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">最小</font></td>\n");
	for ($i = 1; $i <= 12; $i++) {
		echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");

		// 管理項目または入力系の補助項目の場合
		if ($assist_type != "2") {
			echo(min($stats[$fcl_id][$link_id][$i]));
		} else {
			$stat_count = count($vals[$i]);
			if ($stat_count > 0) {
				$val = min($vals[$i]);

				// 実数表示の場合
				if ($format == "1") {
					echo round($val, 1);

				// %表示の場合
				} else {
					$val *= 100;
					echo round($val, 1) . "%";
				}
			}
		}

		echo("</font></td>");
	}
	echo("</tr>\n");
}
?>
</table>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
