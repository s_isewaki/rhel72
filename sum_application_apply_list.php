<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css"/>
<?
require_once("about_comedix.php");
require_once("get_values.ini");
require_once("show_select_values.ini");
require_once("yui_calendar_util.ini");
require_once("sum_application_workflow_common_class.php");
require_once("summary_common.ini");
require_once("sum_apply_generate_revision.ini");

//====================================================================
//セッションチェック、権限チェック、DB接続
//====================================================================
$fname = $PHP_SELF;
$session = qualify_session($session,$fname);
$summary_check = @check_authority($session,57,$fname);
$con = @connect2db($fname);
if(!$session || !$summary_check || !$con){
	echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
	exit;
}


//==============================
// ログ
//==============================
$emp_id = get_emp_id($con, $session, $fname);
summary_common_write_operate_log("access", $con, $fname, $session, $emp_id, "", "");


//====================================
// 検索条件初期化
//
// 送信日セット(本日日付から過去２週間分)
// 送信タブをクリックした場合のみ。
//====================================
if(@$apply_date_default == "on" || @$apply_date_default == "on2"){
	$today = date("Y/m/d", strtotime("today"));

	$arr_today = split("/", $today);
	$date_y2 = $arr_today[0];
	$date_m2 = $arr_today[1];
	$date_d2 = $arr_today[2];

	// ２週間前の日付取得
	$two_weeks_ago = date("Y/m/d",strtotime("-2 week" ,strtotime($today)));

	$arr_two_weeks_ago = split("/", $two_weeks_ago);
  $date_y1 = $arr_two_weeks_ago[0] ;
  $date_m1 = $arr_two_weeks_ago[1] ;
  $date_d1 = $arr_two_weeks_ago[2] ;
}



//--------------------------------------
// 同画面の「中止送信」「再送信」ボタン処理
//--------------------------------------
if (@$_REQUEST["update_apply_stat"] != "" && @$_REQUEST["apply_id_comma_list"] != ""){

	$apply_id_list = explode(",", $_REQUEST["apply_id_comma_list"]);
	if ($_REQUEST["update_apply_stat"]=="1" || $_REQUEST["update_apply_stat"]=="2"){
		for($i = 0; $i < count($apply_id_list); $i++){
			if (!$apply_id_list[$i]) continue;
			$apid = $apply_id_list[$i];

			$dest_apply_stat = "update_and_resend";

			// 中止送信の場合、中止可能かを確認
			if ($_REQUEST["update_apply_stat"] == "2"){
				$sql = "select apply_stat from sum_apply where apply_id = ".$apid;
				$sel = select_from_table($con, $sql, "", $fname);
				if ((int)@pg_fetch_result($sel, 0, "apply_stat") != 1) continue;
				$dest_apply_stat = "cyusi";
			}

			$sel_new_apply = select_from_table($con, "select * from sum_apply where apply_id = " . $apid, "", $fname);
			$summary_id = (int)@pg_fetch_result($sel_new_apply, 0, "summary_id");
			$wkfw_id =    (int)@pg_fetch_result($sel_new_apply, 0, "wkfw_id");
			$ptif_id =    @pg_fetch_result($sel_new_apply, 0, "ptif_id");
			$sel_summary = select_from_table($con, "select * from summary where summary_id = ".$summary_id." and ptif_id = '".$ptif_id."'", "", $fname);
			$summary_seq = (int)@pg_fetch_result($sel_summary, 0, "summary_seq");

			apply_generate_revision(
				$con, $fname, $dest_apply_stat, $apid, $emp_id,
				$summary_id, $summary_seq, $ptif_id, $wkfw_id,
				0, 0, "yes", "yes", "yes");
		}
	}
}

// 外部ファイルを読み込む
write_yui_calendar_use_file_read_0_12_2();

// カレンダー作成、関数出力
write_yui_calendar_script2(2);
?>

<script type="text/javascript">

function reload_page() {
	document.getElementById("refresh_wait_message").style.display = "";
	window.location.reload(true);
}

function getSelectedValue(sel) { return sel.options[sel.selectedIndex].value; }

function deleteAllOptions(box) { for (var i = box.length - 1; i >= 0; i--) box.options[i] = null; }

function popupDetail(apply_id){
	window.open(
		"sum_application_approve_detail.php?session=<?=$session?>&exec_mode=apply&apply_id="+apply_id,
		"newwin2",
		"width=1020,height=700,scrollbars=yes,resizable=yes"
	);
}

function addOption(box, value, text, selected) {
	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	if (selected == value) opt.selected = true;
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
}

function changeCellsColor(class_name, color) {
	var cells = document.getElementsByTagName('td');
	for (var i = 0, j = cells.length; i < j; i++) {
		if (cells[i].className != class_name) continue;
		cells[i].style.backgroundColor = color;
	}
}

var checkboxes = new Object();
function apply_change_status(apply_stat, caption) {
	var apply_id_comma_list= "";
	for (var apply_id in checkboxes){
		if (checkboxes[apply_id]) apply_id_comma_list += "," + apply_id;
	}

	if (apply_id_comma_list == "") { alert('「'+caption+'」するデータを選択してくだい。'); return; }
	if (!confirm(caption+'します。よろしいですか？')) return;

	document.apply.apply_id_comma_list.value = apply_id_comma_list.substring(1);
	document.apply.update_apply_stat.value = apply_stat;
	document.apply.submit();
}
</script>
</head>
<body onload="initcal();">

<? summary_common_show_sinryo_top_header($session, $fname, "送信一覧"); ?>

<? summary_common_show_main_tab_menu($con, $fname, "送信一覧", 1); ?>

<form name="apply" action="sum_application_apply_list.php?session=<?=$session?>" method="get">
<input type="submit" name="dummy" style="display:none" />
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="apply_id_comma_list" value="">
<input type="hidden" name="update_apply_stat" value="">

<table class="list_table" cellspacing="1">
<tr>
	<th width="10%" style="white-space:nowrap">診療記録区分</th>
	<td width="50%" colspan="3">
		<select name="sel_div_id">
			<option value="">すべて</option>
			<? $sel = select_from_table($con, "select * from divmst where div_del_flg = 'f' order by div_id", "", $fname); ?>
			<? while($row = @pg_fetch_array($sel)){ ?>
			<?   $selected = ($row["div_id"] == @$sel_div_id ? "selected" : ""); ?>
			<option value="<?=$row["div_id"]?>" <?=$selected?>><?=h($row["diag_div"])?></option>
			<? } ?>
		</select>
	</td>
	<th width="10%" style="white-space:nowrap">テンプレート名</th>
	<td width="30%" >
		<select name="sel_tmpl_id">
			<option value="">すべて</option>
			<? $sel = select_from_table($con, "select * from tmplmst order by tmpl_id", "", $fname); ?>
			<? while($row = @pg_fetch_array($sel)){ ?>
			<?   $selected = ($row["tmpl_id"] == @$sel_tmpl_id ? "selected" : ""); ?>
			<option value="<?=$row["tmpl_id"]?>" <?=$selected?>><?=h($row["tmpl_name"])?></option>
			<? } ?>
		</select>
	</td>
</tr>
<tr>
	<th>患者ID</th>
	<td><input type="text" size="20" maxlength="20" name="sel_ptif_id" value="<?=h(@$sel_ptif_id)?>"></td>
	<th style="white-space:nowrap">患者氏名</th>
	<td><input type="text" size="30" maxlength="40" name="sel_ptif_name" value="<?=h(@$sel_ptif_name)?>"></td>
	<th>受信者</th>
	<td><input type="text" size="30" maxlength="30" name="approve_emp_nm" value="<?=h(@$approve_emp_nm)?>"></td>
</tr>
<tr>
	<th>送信日</th>
	<td colspan="3">
		<table>
		<tr>
			<td>
				<select id="date_y1" name="date_y1"><? show_select_years(10, @$date_y1, true); ?></select>/<select id="date_m1" name="date_m1"><? show_select_months(@$date_m1, true); ?></select>/<select id="date_d1" name="date_d1"><? show_select_days(@$date_d1, true); ?></select>
			</td><td>
				<img src="img/calendar_link.gif" style="left:3px;z-index:1;cursor:pointer;" onclick="show_cal1();"/><br>
				<div id="cal1Container" style="position:absolute;display:none;z-index:10000;"></div>
			</td><td>
				&nbsp;〜<select id="date_y2" name="date_y2"><? show_select_years(10, $date_y2, true); ?></select>/<select id="date_m2" name="date_m2"><? show_select_months($date_m2, true); ?></select>/<select id="date_d2" name="date_d2"><? show_select_days($date_d2, true); ?></select>
			</td><td>
				<img src="img/calendar_link.gif" style="left:3px;z-index:1;cursor:pointer;" onclick="show_cal2()"/><br>
				<div id="cal2Container" style="position:absolute;display:none;z-index:10000;"></div>
			</td>
		</tr></table>
	</td>
	<th>送信状況</th>
	<td>
		<select name="apply_stat">
			<? $arr_nm = array("未送信", "中止送信", "送信済：すべて", "送信済：未受付あり", "送信済：未実施あり", "送信済：差戻しあり", "送信済：否認あり", "送信済：受信完了(未確認)", "送信済：受信完了(確認済)"); ?>
			<? $arr_id = array('0','2','1','non_accepted','non_operated','sasimodosi', 'hinin', 'complete', 'complete_checked'); ?>
			<option value="-">すべて</option>
			<? for($i=0;$i<count($arr_nm);$i++) { ?>
			<option value="<?=$arr_id[$i]?>" <?=(@$apply_stat==$arr_id[$i] ? "selected":"")?>><?=$arr_nm[$i]?></option>
			<? } ?>
		</select>
	</td>
</tr>
</table>

<div class="search_button_div" style="border-bottom:1px solid #fdfdfd">
	<input type="button" onclick="document.apply.submit();" value="検索"/>
</div>

<div style="background-color:#F4F4F4; padding-top:5px; padding-bottom:5px; border-top:1px solid #d8d8d8; text-align:right">
	<input type="button" onclick="apply_change_status('2','中止送信');" value="中止送信"/>
	<input type="button" onclick="apply_change_status('1', '再送信');" value="再送信"/>
</div>


<?


$list_sql  =
	" select".
	" wk.short_wkfw_name".
	",wk.wkfw_appr".
	",ap.apply_id".
	",ap.ptif_id".
	",ap.apply_date".
	",ap.apply_no".
	",ap.apply_stat".
	",ap.is_approve_complete".
	",ap.approve_complete_check_ymdhms".
	",ap.dairi_emp_id".
	",ap.emp_id".
	",ap.visibility_apv_order".
	",pt.ptif_sex".
	",pt.ptif_birth".
	",pt.ptif_lt_kaj_nm".
	",pt.ptif_ft_kaj_nm".
	",su.diag_div".
	",tm.tmpl_name".
	",sahs.hinin_count".
	",sast.sasimodosi_count".
	" from sum_apply ap".
	" inner join sum_wkfwmst wk on (wk.wkfw_id = ap.wkfw_id and wk.is_hide_apply_list = 0)".
	" inner join summary su on (su.summary_id = ap.summary_id and su.ptif_id = ap.ptif_id and su.emp_id = ap.emp_id)".
	" inner join divmst dv on (dv.diag_div = su.diag_div)".
	" left outer join ptifmst pt on (pt.ptif_id = su.ptif_id)".
	" left outer join tmplmst tm on (tm.tmpl_id = wk.tmpl_id)".
	" left outer join sum_applyapv apv on (ap.apply_id = apv.apply_id)".

	" left outer join (".
	"   select".
	"   apply_id".
	"  ,count(apply_id) as hinin_count".
	"   from sum_apply_hinin_sasimodosi".
	"   where hinin_or_sasimodosi = 'hinin'".
	"   and update_ymdhm is null".
	"   group by apply_id".
	" ) sahs on (sahs.apply_id = apv.apply_id)".

	" left outer join (".
	"   select".
	"   apply_id".
	"  ,count(apply_id) as sasimodosi_count".
	"   from sum_apply_sasimodosi_target".
	"   where update_ymdhm is null".
	"   group by apply_id".
	" ) sast on (sast.apply_id = apv.apply_id)".

	// ログイン者本人か、代行送信したか
	" where (ap.emp_id = '".pg_escape_string($emp_id)."' or (ap.dairi_emp_id = '".pg_escape_string($emp_id)."' and ap.dairi_kakunin_ymdhms is null))".
	" and ap.delete_flg = 'f'";

if (trim(@$sel_div_id) != "") { //診療記録区分
	$list_sql .= " and dv.div_id = ". (int)$sel_div_id;
}
if ((int)@$sel_tmpl_id > 0) { //テンプレート名
	$list_sql .= " and wk.tmpl_id = ". (int)$sel_tmpl_id;
}
if(trim(@$sel_ptif_id) != "") {// 患者ID
	$list_sql .= " and su.ptif_id = '" . pg_escape_string(trim($sel_ptif_id))."'";
}
if(trim(@$sel_ptif_name) != "") {// 患者名
	$list_sql .=
		" and (".
		" pt.ptif_lt_kaj_nm like '%" . pg_escape_string(trim($sel_ptif_name))."%'".
		" or pt.ptif_ft_kaj_nm like '%" . pg_escape_string(trim($sel_ptif_name))."%'".
		" or pt.ptif_lt_kana_nm like '%" . pg_escape_string(trim($sel_ptif_name))."%'".
		" or pt.ptif_ft_kana_nm like '%" . pg_escape_string(trim($sel_ptif_name))."%'".
		" )";
}
if(@$apply_stat == "0" || @$apply_stat == "1" || @$apply_stat == "2") {// 送信状況
	$list_sql .= " and ap.apply_stat = '$apply_stat'";
}
if (@$apply_stat == "non_accepted"){
	$list_sql .= " and ap.apply_stat = '1'";
	$list_sql .= " and (apv.apv_stat_operated_by_other is null or apv.apv_stat_operated_by_other = 0)";
	$list_sql .= " and (apv.apv_stat_accepted is null or apv.apv_stat_accepted = 0)";
	$list_sql .= " and (ap.is_approve_complete is null or ap.is_approve_complete = 0)";
}
if (@$apply_stat == "non_operated"){
	$list_sql .= " and ap.apply_stat = '1'";
	$list_sql .= " and apv.next_notice_recv_div = '2'";
	$list_sql .= " and (apv.apv_stat_operated is null or apv.apv_stat_operated = 0)";
	$list_sql .= " and (apv.apv_stat_operated_by_other is null or apv.apv_stat_operated_by_other = 0)";
	$list_sql .= " and (ap.is_approve_complete is null or ap.is_approve_complete = 0)";
}
if (@$apply_stat == "sasimodosi"){
	$list_sql .= " and ap.apply_stat = '1'";
	$list_sql .= " and sast.sasimodosi_count > 0";
}
if (@$apply_stat == "hinin"){
	$list_sql .= " and ap.apply_stat = '1'";
	$list_sql .= " and sahs.hinin_count > 0";
}
if (@$apply_stat == "complete"){
	$list_sql .= " and ap.is_approve_complete = 1";
	$list_sql .= " and ap.approve_complete_check_ymdhms is null";
}
if (@$apply_stat == "complete_checked"){
	$list_sql .= " and ap.is_approve_complete = 1";
	$list_sql .= " and ap.approve_complete_check_ymdhms is not null";
}



if(trim(@$approve_emp_nm) != "") { // 受信者
	$sql_emp_nm = pg_escape_string($approve_emp_nm);
	$list_sql .=
		" and apv.emp_id in (".
		"   select emp_id from empmst".
		"   where emp_lt_nm like '%".$sql_emp_nm."%'".
		"   or emp_ft_nm like '%".$sql_emp_nm."%'".
		"   or emp_kn_lt_nm like '%".$sql_emp_nm."%'".
		"   or emp_kn_ft_nm like '%".$sql_emp_nm."%'".
		" )";
}
if (strlen(@$date_y1.@$date_m1.@$date_d1) == 8) { // 送信日(from)
	$list_sql .= " and ap.apply_date >= '".$date_y1.$date_m1.$date_d1."'";
}
if (strlen(@$date_y2.@$date_m2.@$date_d2) == 8) { // 送信日(to)
	$nextday = "0".((int)$date_d2+1);
	$nextday = substr($nextday, strlen($nextday)-2, strlen($nextday));
	$list_sql .= " and ap.apply_date < '".$date_y2.$date_m2.$nextday."'";
}

$list_sql .=
	" group by".
	" wk.short_wkfw_name".
	",wk.wkfw_appr".
	",ap.apply_id".
	",ap.ptif_id".
	",ap.apply_date".
	",ap.apply_no".
	",ap.apply_stat".
	",ap.is_approve_complete".
	",ap.approve_complete_check_ymdhms".
	",ap.dairi_emp_id".
	",ap.emp_id".
	",ap.visibility_apv_order".
	",pt.ptif_sex".
	",pt.ptif_birth".
	",pt.ptif_lt_kaj_nm".
	",pt.ptif_ft_kaj_nm".
	",su.diag_div".
	",tm.tmpl_name".
	",sahs.hinin_count".
	",sast.sasimodosi_count";


$page = @$_REQUEST["page"];
if ($page == "") $page = 1;

// 件数取得
$sel = select_from_table($con, "select count(*) as cnt from ( " . $list_sql . ") d", "", $fname);
if ($sel == 0) {
	pg_close($con);
	summary_common_show_error_page_and_die();
}
$apply_list_count = pg_fetch_result($sel, 0, "cnt");

//一画面内の最大表示件数
$disp_max_page = 15;
$offset = ($page - 1) * $disp_max_page;

//最大ページ数
$page_max  = 1;
if($apply_list_count > 0) $page_max  = floor( ($apply_list_count-1) / $disp_max_page ) + 1;

if ($apply_list_count > 0) {
	show_page_area($page_max,$page);
?>

<input type="hidden" name="page" value="">
<table class="listp_table" cellspacing="1">
<tr>
<th width="2%" style="text-align:center; white-space:nowrap; padding:2px"><br/></th>
<th width="12%" style="text-align:center; white-space:nowrap; padding:2px">オーダ番号<br>(管理ＣＤ-連番)</th>
<th width="8%" style="text-align:center; white-space:nowrap; padding:2px">診療<br/>記録区分</th>
<th width="10%" style="text-align:center; white-space:nowrap; padding:2px">テンプレート名</th>
<th width="7%" style="text-align:center; white-space:nowrap; padding:2px">患者ID</th>
<th width="9%" style="text-align:center; white-space:nowrap; padding:2px">患者氏名</th>
<th width="12%" style="text-align:center; white-space:nowrap; padding:2px">生年月日</th>
<th width="3%" style="text-align:center; white-space:nowrap; padding:2px">性別</th>
<th width="12%" style="text-align:center; white-space:nowrap; padding:2px">受信者</th>
<th width="5%" style="text-align:center; white-space:nowrap; padding:2px">受信<BR>状況</th>
<th width="5%" style="text-align:center; white-space:nowrap; padding:2px">実施<BR>状況</th>
<th width="9%" style="text-align:center; white-space:nowrap; padding:2px">送信日</th>
<th width="6%" style="text-align:center; white-space:nowrap; padding:2px">送信<BR>状況</th>
</tr>
<?

	// 一覧データ取得
	$sel = select_from_table($con, $list_sql . " order by ap.apply_date desc, ap.apply_id desc offset $offset limit $disp_max_page", "", $fname);
	if ($sel == 0) {
		pg_close($con);
		summary_common_show_error_page_and_die();
	}
	$application_list = pg_fetch_all($sel);

	//----------------------------------------------
	// 送信ループ
	//----------------------------------------------
	$count = 0;
	$obj = new application_workflow_common_class($con, $fname);
	foreach($application_list as $row) {
		$order_bangou = $obj->generate_apply_no_for_display($row["short_wkfw_name"], $row["apply_date"], $row["apply_no"]);// オーダ番号
		$apply_date = preg_replace("/^(\d{4})(\d{2})(\d{2}).*$/", "$1/$2/$3", $row["apply_date"]);// 送信日

		// 送受信状況
		$apply_stat_nm = "";
		if ($row["apply_stat"] == '0') $apply_stat_nm = '未送信';
		if ($row["apply_stat"] == '1') $apply_stat_nm = '<span style="color:#880">送信済</span>';
		if ($row["apply_stat"] == '2') $apply_stat_nm = '<span style="color:#f05">中止送信</span>';
		if ($row["is_approve_complete"]){
			$apply_stat_nm = '<span style="color:#f05">受信完了</span>';
			 if ($row["approve_complete_check_ymdhms"]) $apply_stat_nm = '<span style="color:#880">受信完了</span>';
		}

		//   受信完了のときは薄い黄色
		$bgcolor = "#fff";
		if ($row["is_approve_complete"]){
			if (!$row["approve_complete_check_ymdhms"]) $bgcolor = "#ffd9e7"; // 受信完結だが最終未確認のときは赤
			else $bgcolor = "#ffffd0"; // 受信完結、確認済みは薄い黄色
		}
		if ($row["sasimodosi_count"]) $bgcolor = "#b1f8b7"; // 差戻しありのときは緑
		if ($row["hinin_count"]) $bgcolor = "#fcc6a5"; // 否認のときはオレンジ
		if ($row["apply_stat"]=="0") $bgcolor = "#e1e1e1"; // 未送信のときも灰色
		if ($row["apply_stat"]=="2") $bgcolor = "#e1e1e1"; // 中止送信のときは灰色

		//----------------------------------------------
		// 送信に対する受信一覧
		//----------------------------------------------
		$sql =
			" select apv.apply_id, apv.apv_order, apv.apv_fix_show_flg".
			",apv.apv_stat_accepted, apv.apv_stat_operated, apv.apv_stat_accepted_by_other, apv.apv_stat_operated_by_other".
			",apv.next_notice_div, apv.next_notice_recv_div".
			",emp.emp_lt_nm, emp.emp_ft_nm".
			",sahs.from_apply_stat_accepted, sahs.from_apply_stat_operated, sahs.hinin_or_sasimodosi".
			" from sum_applyapv apv".
			" left join empmst emp on (apv.emp_id = emp.emp_id)".
			" left join sum_apply_hinin_sasimodosi sahs on (".
			"   sahs.apply_id = apv.apply_id".
			"   and sahs.from_apv_order = apv.apv_order".
			"   and sahs.from_emp_id = apv.emp_id".
			"   and sahs.update_ymdhm is null".
			" )".
			" where apv.apply_id = ".$row["apply_id"]. " and apv.emp_id is not null".
			" order by apv.apv_order, apv.apv_sub_order asc";
		$tmp_arr_applyapv = select_from_table($con, $sql, "", $fname);
		$arr_applyapv = pg_fetch_all($tmp_arr_applyapv);
		$apv_cnt = count($arr_applyapv);


		$count++;
		$align = " align=\"center\"";
		$tags1 = " class=\"apply_list_".$count."\"".
						 " onmouseover=\"changeCellsColor(this.className, '#fefc8f');\"".
						 " onmouseout=\"changeCellsColor(this.className, '');\"";
		$tags  = $tags1 . ' onclick="popupDetail(\''.$row["apply_id"].'\');">';

		//----------------------------------------------
		// 権限並列分を1行にまとめる
		//----------------------------------------------
		$arr_applyapv = summarize_applyapv($arr_applyapv);

		ob_start("resetRowspan") ; // 後でrowspan値を再設定するので出力をバッファリングする
		//----------------------------------------------
		// 受信ループ
		//----------------------------------------------
		$row_count = 0 ;
		$comDisp = false ;
		for($idx=0; $idx<count($arr_applyapv); $idx++) {
			$apv = $arr_applyapv[$idx];

			// 受信状況
			$apv_stat_accepted_nm = "未受付";
			if ($apv["apv_stat_accepted"]==1) $apv_stat_accepted_nm = '<span style="color:#880">受付済</span>';
			if ($apv["from_apply_stat_accepted"] && $apv["hinin_or_sasimodosi"]=="sasimodosi") $apv_stat_accepted_nm = '<span style="color:#f05">差戻し</span>';
			if ($apv["from_apply_stat_accepted"] && $apv["hinin_or_sasimodosi"]=="hinin") $apv_stat_accepted_nm = '<span style="color:#f05">否認</span>';
			if ($apv_stat_accepted_nm == "未受付" && $apv["apv_stat_accepted_by_other"]) $apv_stat_accepted_nm = '<span style="color:#ba16be">他者済</span>';

			$apv_stat_operated_nm = "未実施";
			if ($apv["apv_stat_operated"]==1) $apv_stat_operated_nm = '<span style="color:#880">実施済</span>';
			if ($apv["from_apply_stat_operated"] && $apv["hinin_or_sasimodosi"]=="sasimodosi") $apv_stat_operated_nm = '<span style="color:#f05">差戻し</span>';
			if ($apv["from_apply_stat_operated"] && $apv["hinin_or_sasimodosi"]=="hinin") $apv_stat_operated_nm = '<span style="color:#f05">否認</span>';
			if ($apv_stat_operated_nm == "未実施" && $apv["apv_stat_operated_by_other"]) $apv_stat_operated_nm = '<span style="color:#ba16be">他者済</span>';
			if ($apv["next_notice_recv_div"] <> "2") $apv_stat_operated_nm = '-';

			$ptif_sex = "不明";
			if ($row["ptif_sex"] == "1") $ptif_sex = "男性";
			if ($row["ptif_sex"] == "2") $ptif_sex = "女性";

			$ptif_birth = "";
			$birth = $row["ptif_birth"];
			if (strlen($birth) == 8){
				$ptif_birth = substr($birth,0,4) . "年". substr($birth,4,2) . "月" . substr($birth,6,2) . "日";
			}
			$dairi_html = "";
			if ($row["dairi_emp_id"] == $emp_id && $row["dairi_emp_id"] != $row["emp_id"]){
				$sql = "select emp_lt_nm, emp_ft_nm from empmst where emp_id = '". pg_escape_string($row["emp_id"])."'";
				$sel_dairi = select_from_table($con, $sql, "", $fname);
				$dairi_html = '<span style="padding:0; background-color:#eff9fd; border:1px solid #9bc8ec;">送信</span>'.
					h(pg_fetch_result($sel_dairi, 0, "emp_lt_nm")." ".pg_fetch_result($sel_dairi, 0, "emp_ft_nm"))."<br/>";
			}

			echo('<tr style="background-color:'.$bgcolor."; cursor:pointer\">\n");
			if (!$comDisp) {
				echo("<td style=\"padding:1px\""."@rowspan@".$tags1."><input type=\"checkbox\" onclick=\"checkboxes['".$row["apply_id"]."'] = this.checked;\"></td>\n"); //選択
				echo("<td"."@rowspan@".$tags.$dairi_html.h($order_bangou)."</td>\n"); //オーダ番号
				echo("<td"."@rowspan@".$tags.h($row["diag_div"])."</td>\n"); //診療記録区分
				echo("<td"."@rowspan@".$tags.h($row["tmpl_name"])."</td>\n"); //テンプレート名
				echo("<td"."@rowspan@".$tags.$row["ptif_id"]."</td>\n"); //患者ID
				echo("<td"."@rowspan@".$tags.$row["ptif_lt_kaj_nm"].$row["ptif_ft_kaj_nm"]."</td>\n"); //患者氏名
				echo("<td"."@rowspan@".$tags.$ptif_birth."</td>\n"); //生年月日
				echo("<td"."@rowspan@".$tags.$ptif_sex."</td>\n"); //性別
			}
			if ($apv["summarized"]) {  // 複数行をまとめた場合
				echo("<td".$align.$tags."複数あり</td>\n"); //受信者
			} else {
				echo("<td".$align.$tags.h($apv["emp_lt_nm"]." ".$apv["emp_ft_nm"])."</td>\n"); //受信者
			}
			echo("<td".$align.$tags.@$apv_stat_accepted_nm."</td>\n"); //受信状況
			echo("<td".$align.$tags.@$apv_stat_operated_nm."</td>\n"); //実施状況
			if (!$comDisp) {
				echo("<td".$align."@rowspan@".$tags.$apply_date."</td>\n"); //送信日
				echo("<td".$align."@rowspan@".$tags.$apply_stat_nm."</td>\n");//送信状況
				$comDisp = true ;
			}
			echo("</tr>\n");
			$row_count++;
		}

		ob_end_flush();
	}
	echo "</table>";

	show_page_area($page_max,$page);
}
?>
<? function show_page_area($page_max,$page) { ?>
	<? if($page_max < 2) return; ?>
  <script type="text/javascript">
	  function page_change(page) { document.apply.page.value = page; document.apply.submit(); }
  </script>
  <table width="100%" style="margin-top:4px;"><tr><td>
    <table class="td_pad2px" style="color:#c0c0c0; white-space:nowrap"><tr>
	    <td><? if($page!=1){ ?><a href="javascript:page_change(1);"><? } ?>先頭へ<? if($page!=1){ ?></a><? } ?></td>
	    <td><? if($page!=1) { ?><a href="javascript:page_change(<?=$page-1?>);"><? } ?>前へ<? if($page!=1){ ?></a><? } ?></td>
	    <td>
				<? for($i=1;$i<=$page_max;$i++) { ?>
					<? if($i!=$page) { ?><a href="javascript:page_change(<?=$i?>);"><? } ?>[<?=$i?>]<? if($i!=$page) { ?></a><? } ?>
				<? } ?>
	    </td>
	    <td><? if($page!=$page_max){ ?><a href="javascript:page_change(<?=$page+1?>);"><? } ?>次へ<? if($page!=$page_max){ ?></a><? } ?></td>
    </tr></table>
  </td></tr></table>
<? } ?>

<?
function summarize_applyapv($arr_applyapv) {
	$new_applyapv = array();
	$parallel_orders = array();
	foreach ($arr_applyapv as $apv) {

		// 権限並列でない場合はまとめない
		if ($apv["next_notice_div"] != "3") {
			$new_applyapv[] = $apv;
			continue;
		}

		// まだまとめに着手していない階層の場合
		if (!isset($parallel_orders[$apv["apv_order"]])) {

			// まとめ先のインデックスを覚えておく
			$index = count($new_applyapv);
			$parallel_orders[$apv["apv_order"]] = $index;

			// まとめ行に当該受信者の情報を反映させる
			$new_applyapv[] = $apv;
			$new_applyapv[$index]["apv_stat_accepted_by_other"] = 0;  // 他者済は考慮しない
			$new_applyapv[$index]["apv_stat_operated_by_other"] = 0; // 他者済は考慮しない

		// まとめ済みの階層の場合、必要に応じてまとめ行に当該受信者の情報を反映させる
		} else {
			$index = $parallel_orders[$apv["apv_order"]];
			if ($apv["apv_stat_accepted"] == "1") $new_applyapv[$index]["apv_stat_accepted"] = "1";
			if ($apv["from_apply_stat_accepted"] == "1") $new_applyapv[$index]["from_apply_stat_accepted"] = "1";
			if ($apv["apv_stat_operated"] == "1") $new_applyapv[$index]["apv_stat_operated"] = "1";
			if ($apv["from_apply_stat_operated"] == "1") $new_applyapv[$index]["from_apply_stat_operated"] = "1";
			if ($apv["hinin_or_sasimodosi"] != "") $new_applyapv[$index]["hinin_or_sasimodosi"] = $apv["hinin_or_sasimodosi"];

			// 「複数あり」フラグは必ず立てる
			$new_applyapv[$index]["summarized"] = true;
		}
	}
	return $new_applyapv;
}

function resetRowspan($buf) {
	global $row_count;
	$rowspan = ($row_count > 1 ? " rowspan=\"".$row_count."\"" : "");
	return str_replace("@rowspan@", $rowspan, $buf) ;
}
?>

</form>

<table id="refresh_wait_message" style="position:absolute; width:100%; top:150px; display:none"><tr><td style="text-align:center">
	<span style="padding:20px; background-color:#f48d02; color:#fff; border:2px solid #eaef07; font-size:14px">
		画面を最新の状態に更新します。しばらくお待ちください。
	</span>
</td></tr></table>

</body>
<? pg_close($con); ?>
</html>

