<?/*

画面パラメータ
	$session
		セッションID
	$caller
		呼び出し元識別子
	$mst_cd
		マスタコード(必須)
	$mst_ver
		マスタバージョン
	$searched
		'true'の場合、検索値で検索済み。
	$search_input
		項目名検索初期値
	$single_select
		'true'なら1件で閉じる。'false'なら選択しても閉じない。
*/

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require_once("about_comedix.php");
require("label_by_profile_type.ini");

//ページ名
$fname = $PHP_SELF;

//==============================
//セッションのチェック
//==============================
$session = qualify_session($session,$fname);
if($session == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
// 権限チェック
//==============================
$summary = check_authority($session, 57, $fname);
if ($summary == "0")
{
	showLoginPage();
	exit;
}

//==============================
//DBコネクション
//==============================
$con = connect2db($fname);
if($con == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
// 組織タイプを取得
//==============================
$profile_type = get_profile_type($con, $fname);

//==============================
//マスタ名の取得
//==============================
$sql = " select mst_name from tmplitemmst where mst_cd = '$mst_cd'";
$sel = select_from_table($con,$sql,"",$fname);
if($sel == 0){
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$sel_num = pg_numrows($sel);
if($sel_num == 0)
{
	//指定されたマスタが存在しない場合の処理
	echo("<script type=\"text/javascript\">alert('マスタコード「".$mst_cd."」が正しくありません。');close();</script>");
}
$mst_name = pg_fetch_result($sel,0,'mst_name');

//==============================
//マスタバージョンの取得
//==============================


$sql = "select case when max(rireki_index) isnull then 1 else max(rireki_index)+1 end as rireki_index from tmplitemrireki where mst_cd = '$mst_cd'";
$sel = select_from_table($con,$sql,"",$fname);
if($sel == 0){
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$now_ver = pg_fetch_result($sel,0,'rireki_index');

if($mst_ver == "")
{
	$mst_ver = $now_ver;
}
$search_mst_ver = $mst_ver;
if($mst_ver_now == 'true')
{
	$search_mst_ver = $now_ver;
}

//==============================
//検索処理
//==============================


if($searched == 'true')
{
	if($search_mst_ver == $now_ver)
	{
		$sql = "select * from tmplitem where mst_cd = '$mst_cd' and disp_flg";
	}
	else
	{
		$sql = "select * from tmplitemrireki where mst_cd = '$mst_cd' and rireki_index = $search_mst_ver and disp_flg";
	}
	if($search_input != "")
	{
		$sql .= " and item_name like '%".pg_escape_string($search_input)."%'";
	}
	$sql .= " order by disp_order";
	
	//SQL実行
	$sel = select_from_table($con,$sql,"",$fname);
	if($sel == 0){
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$sel_num = pg_numrows($sel);
}
else
{
	$sel = 0;
	$sel_num = 0;
}


//==========================================================================================
//HTML出力
//==========================================================================================
?>
<title>CoMedix <?
// メドレポート/ＰＯレポート(Problem Oriented)
echo ($_label_by_profile["MED_REPORT"][$profile_type]); ?>｜<?=$mst_name?>選択</title>
<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript">

function load_action()
{
	document.main_form.search_input.focus();
}



//病名が選択されたときの処理を行います。
function select_item(item_cd,item_name,link_cd1,link_cd2,link_cd3)
{
	//呼び出し元画面に通知
	if(window.opener && !window.opener.closed && window.opener.call_back_tmpl_mst_select)
	{
		var result = new Object();
		result.mst_cd = '<?=$mst_cd?>';
		result.mst_ver = '<?=$search_mst_ver?>';
		result.item_cd = item_cd;
		result.item_name = item_name;
		result.link_cd1 = link_cd1;
		result.link_cd2 = link_cd2;
		result.link_cd3 = link_cd3;
		window.opener.call_back_tmpl_mst_select('<?=$caller?>',result);
	}

<?
if($single_select != 'true')
{
?>
//	alert(item_name+'を追加しました。');
<?
}
else//$single_select == 'true'
{
?>
	//自画面を終了
	window.close();
<?
}
?>
}


function highlightCells(class_name) {
	changeCellsColor(class_name, '#ffff66');
}
function dehighlightCells(class_name) {
	changeCellsColor(class_name, '');
}
function changeCellsColor(class_name, color) {
	var cells = document.getElementsByTagName('td');
	for (var i = 0, j = cells.length; i < j; i++) {
		if (cells[i].className != class_name) {
			continue;
		}
		cells[i].style.backgroundColor = color;
	}
}

</script>
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="load_action();">
<?


//==============================
//ヘッダー 出力
//==============================
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b><?=$mst_name?>選択</b></font></td>
<td>&nbsp</td>
<td width="10">&nbsp</td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" width="10" height="10" alt=""><br>
<?


//==============================
//入力フィールド 出力
//==============================
?>
<form name="main_form" action="summary_tmpl_mst_select.php" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="caller" value="<?=$caller?>">
<input type="hidden" name="mst_cd" value="<?=$mst_cd?>">
<input type="hidden" name="mst_ver" value="<?=$mst_ver?>">
<input type="hidden" name="searched" value="true">
<input type="hidden" name="single_select" value="<?=$single_select?>">
<table width="100%" border="0" cellspacing="2" cellpadding="0">
<tr><td>
	
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td align="left" width="50%">
		<table border="0" cellspacing="0" cellpadding="2" class="list">
		<tr>
		<td bgcolor="#ffffcc" align="right" width="40">
			<nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">名称</font></nobr>
		</td>
		<td align="left">
			<nobr>
			<input type="text" name="search_input" value="<?=h($search_input)?>" style="width:150px;ime-mode: active">
			<input type="submit" value="検索">
			</nobr>
		</td>
		</tr>
		</table>
	</td>
	<td align="right" nowrap>
<?
if($mst_ver != $now_ver)
{
?>
		<input type="checkbox" name="mst_ver_now" value="true" <?if($mst_ver_now == 'true'){?>checked<?}?> >
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">現在のマスタバージョンで検索&nbsp</font>
<?
}
?>
	</td>
	</tr>
	</table>

</td></tr>
<tr><td>
	<img src="img/spacer.gif" width="1" height="5" alt=""><br>
</td></tr>
<tr><td align="center">
	
	<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
	<!-- 一覧HEADER START -->
	<tr>
	<td bgcolor="#ffffcc" align="center" width="40"><nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">コード</font></nobr></td>
	<td bgcolor="#ffffcc" align="center"><nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">名称</font></nobr></td>
	<td bgcolor="#ffffcc" align="center" width="80"><nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">連携コード1</font></nobr></td>
	<td bgcolor="#ffffcc" align="center" width="80"><nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">連携コード2</font></nobr></td>
	<td bgcolor="#ffffcc" align="center" width="80"><nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">連携コード3</font></nobr></td>
	</tr>
	<!-- 一覧HEADER END -->
	<!-- 一覧DATA START -->
	<?
	$is_disp_over = false;
	for($i = 0; $i < $sel_num; $i++)
	{
		if($i >= 200)
		{
			$is_disp_over = true;
			break;
		}
		
		$item_cd    = pg_fetch_result($sel,$i,"item_cd");
		$item_name  = pg_fetch_result($sel,$i,"item_name");
		$disp_order = pg_fetch_result($sel,$i,"disp_order");
		$sort1      = pg_fetch_result($sel,$i,"sort1");
		$sort2      = pg_fetch_result($sel,$i,"sort2");
		$link_cd1   = pg_fetch_result($sel,$i,"link_cd1");
		$link_cd2   = pg_fetch_result($sel,$i,"link_cd2");
		$link_cd3   = pg_fetch_result($sel,$i,"link_cd3");
		
		
		$item_cd_js    = str_replace("'","''",$item_cd);
		$item_name_js  = str_replace("'","''",$item_name);
		$link_cd1_js   = str_replace("'","''",$link_cd1);
		$link_cd2_js   = str_replace("'","''",$link_cd2);
		$link_cd3_js   = str_replace("'","''",$link_cd3);
		
		$line_script = "";
		$line_script .= " class=\"list_line".$i."\"";
		$line_script .= " onmouseover=\"highlightCells(this.className);this.style.cursor = 'pointer';\"";
		$line_script .= " onmouseout=\"dehighlightCells(this.className);this.style.cursor = '';\"";
		$line_script .= " onclick=\"select_item('$item_cd_js','$item_name_js','$link_cd1_js','$link_cd2_js','$link_cd3_js')\"";
	?>
	<tr>
	<td <?=$line_script?> align="center"><nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=h($item_cd)?></font></nobr></td>
	<td <?=$line_script?> align="left"  ><nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=h($item_name)?></font></nobr></td>
	<td <?=$line_script?> align="center"><nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=h($link_cd1)?></font></nobr></td>
	<td <?=$line_script?> align="center"><nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=h($link_cd2)?></font></nobr></td>
	<td <?=$line_script?> align="center"><nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=h($link_cd3)?></font></nobr></td>
	</tr>
	<?
	}
	
	if($is_disp_over)
	{
		?>
		<tr><td colspan="6">
			<nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">200件以上データがあります。</font></nobr>
		</td></tr>
		<?
	}
	
	?>
	<!-- 一覧DATA END -->
	</table>
	
</td></tr>
</table>
</form>
<?


//==============================
//終了HTML 出力
//==============================
?>


</body>
</html>
<?

pg_close($con);
?>
