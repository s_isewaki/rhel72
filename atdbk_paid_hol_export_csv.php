<?php
ob_start();
//atdbk_paid_hol_export_csv.php:年休取得状況CSV
require_once("about_comedix.php");
require_once("timecard_paid_hol_hour_class.php");
require_once("date_utils.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

$start_date = $_POST["start_yr2"] . $_POST["start_mon2"] . $_POST["start_day2"];
$end_date = $_POST["end_yr2"] . $_POST["end_mon2"] . $_POST["end_day2"];

// 情報をCSV形式で取得
$csv = get_list_csv($con, $fname, $start_date, $end_date);

// データベース接続を閉じる
pg_close($con);

// CSVを出力
$file_name = "paid_hol_" . date("Ymd") . ".csv";
ob_clean();
header("Content-Disposition: attachment; filename=$file_name");
header("Content-Type: application/octet-stream; name=$file_name");
header("Content-Length: " . strlen($csv));
echo($csv);
ob_end_flush();

// 情報をCSV形式で取得
function get_list_csv($con, $fname, $start_date, $end_date) {
	
		$titles = array(
				"職員ID",
				"職員氏名",
				"組織1階層",
				"組織2階層",
				"組織3階層",
				"日付",
				"事由",
				"日数",
				"時間数",
				"申請状態",
				"申請日"
		);

    //時間有休
    $obj_hol_hour = new timecard_paid_hol_hour_class($con, $fname);
    $obj_hol_hour->select();

    //有休の事由のID、休暇日数を取得
    $sql = "select * from atdbk_reason_mst  ";
    $cond = "where kind_flag = '1' and display_flag = 't' order by sequence ";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($this->_db_con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $arr_holiday_count = array();
    $arr_reason_cond = array();
    $arr_half_reason_cond = array(); //時間有休で半日有休対応 20140514
    while ($row = pg_fetch_array($sel)) {
        $reason_id = $row["reason_id"];
        $arr_holiday_count[$reason_id] = $row["holiday_count"];
        $arr_reason_cond[] = "'$reason_id'";
        if ($row["holiday_count"] == 0.5) {
            $arr_half_reason_cond[] = "'$reason_id'";
        }
    }
    if (count($arr_reason_cond) > 0) {
        $cond_reason_str = join(",", $arr_reason_cond);
    }
    else {
        $cond_reason_str = "'1','2','3','37','38','39','44','55','57','58','62'";
    }
    if (count($arr_half_reason_cond) > 0) {
        $cond_half_reason_str = join(",", $arr_half_reason_cond);
    }
    else {
        $cond_half_reason_str = "'2','3','38','39','44','55','57','58','62'";
    }
	//移行データ
	$arr_migration_date = array();
	$next_year_date = date("Ymd", strtotime("+1 year", date_utils::to_timestamp_from_ymd($end_date)));
	$sql  = "select a.* from timecard_paid_hol_import a ";
	$cond  = "where a.data_migration_date >= '$start_date' and a.data_migration_date  <= '$next_year_date' ";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while($row = pg_fetch_array($sel)){
		$wk_emp_personal_id = $row["emp_personal_id"];
		$arr_add_date = explode("/", $row["last_add_date"]);
		$last_add_date = sprintf("%d%02d%02d", $arr_add_date[0], $arr_add_date[1], $arr_add_date[2]);
		$arr_migration_date["$wk_emp_personal_id"]["data_migration_date"] = $row["data_migration_date"]; 
		$arr_migration_date["$wk_emp_personal_id"]["last_add_date"] = $last_add_date; 
	}
	//時間有休移行データ確認
	$arr_mig_hour_date = array();
	$sql  = "select a.*, b.emp_id from timecard_paid_hol_hour_import a ";
	$sql  .= "left join empmst b on b.emp_personal_id = a.emp_personal_id ";
	$cond  = "where a.data_migration_date >= '$start_date' and a.data_migration_date  <= '$next_year_date' ";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while($row = pg_fetch_array($sel)){
		$wk_emp_personal_id = $row["emp_personal_id"];
		//前回付与日取得
    	$emppaid_info = $obj_hol_hour->get_last_emppaid_info($row["emp_id"], $row["data_migration_date"], "");
    	$last_add_date = $emppaid_info["year"].$emppaid_info["paid_hol_add_mmdd"];
		$arr_mig_hour_date["$wk_emp_personal_id"]["data_migration_date"] = $row["data_migration_date"]; 
		$arr_mig_hour_date["$wk_emp_personal_id"]["last_add_date"] = $last_add_date; 
	}
	//休暇申請kintai_hol,勤務実績atdbkrslt,時間有休timecard_paid_hol_hourからデータ取得
	// 総合届け使用フラグ
	$general_flag = file_exists("opt/general_flag");
	$start_date_nm = ($general_flag) ? "start_date1" : "start_date";
	$end_date_nm = ($general_flag) ? "end_date1" : "end_date";
	$hol_info = array();
	$sql  = "select hol.*, c.emp_personal_id, c.emp_lt_nm||' '||c.emp_ft_nm as emp_nm, d.class_nm, e.atrb_nm, f.dept_nm, g.default_name, g.display_name from ( ";
	$sql .= "select b.emp_id, b.$start_date_nm, b.$end_date_nm, b.use_hour, b.use_minute, b.pattern, b.reason, b.apply_date, b.apply_stat from kintai_hol b ";
	$sql .= "where ((b.$start_date_nm >= '$start_date' and b.$start_date_nm <= '$end_date') or (b.$end_date_nm >= '$start_date' and b.$end_date_nm <= '$end_date'))  and b.reason in ($cond_reason_str) ";
	$sql .= "union ";
	if ($general_flag) {
		for ($d_idx=2; $d_idx<=5; $d_idx++) {
			$start_date_nm2 = "start_date".$d_idx;
			$end_date_nm2 = "end_date".$d_idx;
			$sql .= "select b.emp_id, b.$start_date_nm2 as start_date1, b.$end_date_nm2 as end_date1, b.use_hour, b.use_minute, b.pattern, b.reason, b.apply_date, b.apply_stat from kintai_hol b ";
			$sql .= "where ((b.$start_date_nm2 >= '$start_date' and b.$start_date_nm2 <= '$end_date') or (b.$end_date_nm2 >= '$start_date' and b.$end_date_nm2 <= '$end_date'))  and b.reason in ($cond_reason_str) ";
			$sql .= "union ";
		}
	}
	$sql .= "select a.emp_id, a.date as start_date, a.date as end_date, '' as use_hour, '' as use_minute, a.pattern, a.reason, '' as apply_date, '' as apply_stat from atdbkrslt a ";
	$sql .= "where a.date >= '$start_date' and a.date <= '$end_date' and a.reason in ($cond_reason_str) and ";
	$sql .= "not exists (select * from kintai_hol b where b.emp_id = a.emp_id and ";
	$sql .= "( ";
	$sql .= " (b.$start_date_nm <= a.date and b.$end_date_nm >= a.date) ";
	//総合届の場合、2番目以降の重複も確認
	if ($general_flag) {
		for ($d_idx=2; $d_idx<=5; $d_idx++) {
			$start_date_nm2 = "start_date".$d_idx;
			$end_date_nm2 = "end_date".$d_idx;
			$sql .= " or (b.$start_date_nm2 <= a.date and b.$end_date_nm2 >= a.date) ";
		}
	}
	$sql .= " )";
	$sql .= " and b.reason in ($cond_reason_str) ) ";
	$sql .= "union ";
	$sql .= "select a.emp_id, a.start_date, a.start_date as end_date, a.use_hour, a.use_minute, '' as pattern, '' as reason, '' as apply_date, '' as apply_stat from timecard_paid_hol_hour a ";
	$sql .= "where a.start_date >= '$start_date' and a.start_date <= '$end_date' and ";
	$sql .= "not exists (select * from kintai_hol b where b.emp_id = a.emp_id and b.$start_date_nm <= a.start_date and b.$end_date_nm >= a.start_date) ";
	$sql .= ") hol ";
	$sql .= "left join empmst c on c.emp_id = hol.emp_id ";
	$sql .= "left join classmst d on c.emp_class = d.class_id ";
	$sql .= "left join atrbmst e on c.emp_attribute = e.atrb_id ";
	$sql .= "left join deptmst f on c.emp_dept = f.dept_id ";
	$sql .= "left join atdbk_reason_mst g on hol.reason = g.reason_id ";
	$cond  = "order by d.order_no, e.order_no, f.order_no, c.emp_personal_id, hol.$start_date_nm ";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$hol_num = pg_num_rows($sel);
	$i = 0;
	while($row = pg_fetch_array($sel)){
		//移行データ確認
		$wk_emp_personal_id = $row["emp_personal_id"];
		if ($arr_migration_date["$wk_emp_personal_id"]["data_migration_date"] != "") {
			//前回付与日から移行日の間の場合は出力しない
			if ($row["$end_date_nm"] >= $arr_migration_date["$wk_emp_personal_id"]["last_add_date"] &&
				$row["$end_date_nm"] <= $arr_migration_date["$wk_emp_personal_id"]["data_migration_date"]) {
				continue;
			}
		}
		//時間有休の場合
		if ($row["use_hour"] > 0 || $row["use_minute"] > 0 &&
			$arr_mig_hour_date["$wk_emp_personal_id"]["data_migration_date"] != "") {
			//前回付与日から移行日の間の場合は出力しない
			if ($row["$end_date_nm"] >= $arr_mig_hour_date["$wk_emp_personal_id"]["last_add_date"] &&
				$row["$end_date_nm"] <= $arr_mig_hour_date["$wk_emp_personal_id"]["data_migration_date"]) {
				continue;
			}
		}

		$hol_info[$i][] = $row["emp_personal_id"];
		$hol_info[$i][] = $row["emp_nm"];
		$hol_info[$i][] = $row["class_nm"];
		$hol_info[$i][] = $row["atrb_nm"];
		$hol_info[$i][] = $row["dept_nm"];
		$hol_info[$i][] = substr($row["$start_date_nm"], 0, 4)."/".substr($row["$start_date_nm"], 4, 2)."/".substr($row["$start_date_nm"], 6, 2);
		//事由
		if ($row["display_name"] != "") {
			$reason_str = $row["display_name"];
		}
		else {
			$reason_str = $row["default_name"];
		}
		$hol_info[$i][] = $reason_str;
		//日数
		$day_cnt = "";
		$wk_reason = $row["reason"];
		if ($wk_reason != "") {
			$day_diff = date_utils::get_time_difference($row["$end_date_nm"]."0000", $row["$start_date_nm"]."0000") / (24*60) + 1;
			if ($day_diff == 1) {
				//半日
				if (in_array($wk_reason, $arr_half_reason_cond)) {
					$day_cnt = 0.5;
				}
				//1日
				else {
					$day_cnt = 1;
				}
			}
			//2日以上はそのまま
			else {
				$day_cnt = $day_diff;
			}
		}
		//時間数
		$wk_time_cnt = "";
		if ($row["use_hour"] > 0 || $row["use_minute"] > 0) {
			$wk_time_cnt = intval($row["use_hour"]).":".$row["use_minute"];
            if ($day_cnt == 1) {
                $day_cnt = "";
            }
		}
		$hol_info[$i][] = $day_cnt;
		$hol_info[$i][] = $wk_time_cnt;
		//申請状態
		switch ($row["apply_stat"]) {
		case "0":
			$stat_str = "申請中";
			break;
		case "1":
			$stat_str = "承認済";
			break;
		default:
			$stat_str = ""; //未申請
		}
		$hol_info[$i][] = $stat_str;
		//申請日
		$wk_apply_date = "";
		if ($row["apply_date"] != "") {
			$wk_apply_date = substr($row["apply_date"], 0, 4)."/".substr($row["apply_date"], 4, 2)."/".substr($row["apply_date"], 6, 2);
		}
		$hol_info[$i][] = $wk_apply_date;
		$i++;
	}
	
	
	$item_num = count($titles);

	$buf = "";
	for ($j=0;$j<$item_num;$j++) {
		if ($j != 0) {
			$buf .= ",";
		}
		$buf .= $titles[$j];
	}
	$buf .= "\r\n";

	for ($i=0;$i<$hol_num;$i++){
		for ($j=0;$j<$item_num;$j++) {
			if ($j != 0) {
				$buf .= ",";
			}
			$buf .= $hol_info[$i][$j];
		}
		$buf .= "\r\n";
	}
	
	return mb_convert_encoding($buf, "SJIS", "EUC-JP");
}
?>
