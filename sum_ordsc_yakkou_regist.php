<?
ob_start();
$fname = $_SERVER["PHP_SELF"];
require_once("about_comedix.php");
require("get_values.php");
require("show_sinryo_top_header.ini");
require("label_by_profile_type.ini");
require_once("sot_util.php");
require("summary_common.ini");
ob_end_clean();

$session = qualify_session($session,$fname);
$fplusadm_auth = check_authority($session, 59, $fname);
$con = @connect2db($fname);
if(!$session || !$fplusadm_auth || !$con){
  echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
  exit;
}


$mode = @$_REQUEST["mode"];
$kaisou = (int)@$_REQUEST["kaisou"];
$code1 = $kaisou ==1 ? @$_REQUEST["code1"] : @$_REQUEST["code1_dropdown"];
$code2 = $kaisou ==2 ? @$_REQUEST["code2"] : @$_REQUEST["code2_dropdown"];
$code3 = @$_REQUEST["code3"];
$caption = @$_REQUEST["yakkou_caption"];
$yakkou_bunrui_cd = @$_REQUEST["yakkou_bunrui_cd"];

//==========================================================================
// 新規登録、登録後は一覧表へ
//==========================================================================
$errmsg = array();
if (@$_REQUEST["mode"] == "regist" && ($kaisou==1 || $kaisou==2 || $kaisou==3)){

	$sql  = " select count(*) as cnt from sum_med_yakkou_bunrui".$kaisou."_mst";
	$sql .= " where code1 = '".pg_escape_string($code1)."'";
	if ($kaisou > 1) $sql .= " and code2 = '" .pg_escape_string($code2)."'";
	if ($kaisou > 2) $sql .= " and code3 = '" .pg_escape_string($code3)."'";
	$sel = select_from_table($con, $sql, "", $fname);
	if (pg_fetch_result($sel, 0, 0)) {
		$errmsg[] = "指定した分類コードの組み合わせは、既に登録されています。";
	}

	if ($kaisou > 2 && $yakkou_bunrui_cd){
		$sql  = " select count(*) as cnt from sum_med_yakkou_bunrui".$kaisou."_mst";
		$sql .= " where yakkou_bunrui_cd = '".$yakkou_bunrui_cd."'";
		$sel = select_from_table($con, $sql, "", $fname);
		if (pg_fetch_result($sel, 0, 0)) {
			$errmsg[] = "指定した薬効分類コードは、既に登録されています。";
		}
	}

	if (!count($errmsg)){

		$sel = select_from_table($con, "select max(sort_order) as sort_order from sum_med_yakkou_bunrui".$kaisou."_mst", "", $fname);
		$sort_order = ((int)pg_fetch_result($sel, 0, 0)) + 1;

		$sql  = " insert into sum_med_yakkou_bunrui".$kaisou."_mst (";
		$sql .= " sort_order";
		$sql .= ",is_disabled";
		$sql .= ",caption";
		$sql .= ",code1";
		if ($kaisou > 1) $sql .= ",code2";
		if ($kaisou > 2) $sql .= ",code3";
		if ($kaisou > 2) $sql .= ",yakkou_bunrui_cd";
		$sql .= " ) values (";
		$sql .= " ".$sort_order;
		$sql .= ",0";
		$sql .= ",'". pg_escape_string($caption)."'";
		$sql .= ",'". pg_escape_string($code1)."'";
		if ($kaisou > 1) $sql .= ",'".pg_escape_string($code2)."'";
		if ($kaisou > 2) $sql .= ",'".pg_escape_string($code3)."'";
		if ($kaisou > 2) $sql .= ",'".pg_escape_string($yakkou_bunrui_cd)."'";
		$sql .= ")";
		$upd = update_set_table($con, $sql, array(), null, "", $fname);
		header("Location: sum_ordsc_yakkou_list.php?session=".$session);
		die;
	}
}


//==========================================================================
// 以下、通常処理
//==========================================================================
$font = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';


// 診療科権限を取得
$entity_admin_auth = check_authority($session, 28, $fname);
// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);
// 診療区分/記録区分
$summary_kubun_title = $_label_by_profile["SUMMARY_KUBUN"][$profile_type];
// メドレポート/ＰＯレポート(Problem Oriented)
$med_report_title = $_label_by_profile["MED_REPORT"][$profile_type];
// 診療科/担当部門
$section_title = $_label_by_profile["SECTION"][$profile_type];
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <? echo($med_report_title); ?>管理 | 処方・注射オーダ</title>

<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list1 {border-collapse:collapse;}
.list1 td { border:#5279a5 solid 1px; text-align:left; white-space:nowrap; background-color:#eee }
.list1 th {  border:#5279a5 solid 1px; text-align:left; font-weight:normal; white-space:nowrap; background-color:#e3ecf4; padding:3px }

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">

<? show_sinryo_top_header($session,$fname,"KANRI_ORDSC"); ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- left -->
<td width="120" valign="top">
<table width="120" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width="118" valign="top">
<?= summary_common_show_admin_left_menu($session, $entity_admin_auth, $section_title, $summary_kubun_title); ?>
</td>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
</td>
<!-- left -->
<!-- center -->
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->






<!-- right -->
<td valign="top">
<?//==========================================================================?>
<?// 処方・注射メニュー                                                       ?>
<?//==========================================================================?>
<?= summary_common_show_ordsc_right_menu($session, "薬効分類")?>

<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr></table>


<?//==========================================================================?>
<?// サブメニュー                                                             ?>
<?//==========================================================================?>
<table border="0" cellspacing="0" cellpadding="0" style="margin-top:8px">
	<tr height="22">
		<td width="100" align="center" bgcolor="#e8e4bd">
			<a href="sum_ordsc_yakkou_list.php?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>一覧/編集</b></font></a>
		</td>
		<td width="5">&nbsp;</td>
		<td width="100" align="center" bgcolor="#958f28">
			<a href="sum_ordsc_yakkou_regist.php?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>新規登録</b></font></a>
		</td>
		<td width="5">&nbsp;</td>
		<td></td>
	</tr>
</table>


<? if (count($errmsg)) { ?>
<?=$font?>
<div style="margin-top:10px">■エラーが発生しました。</div>
<div style="margin-top:4px; padding:8px; background-color:#ffbfd9"><?=implode($errmsg,"<br>")?></div>
</font>
<? } ?>

<?//==========================================================================?>
<?// 入力領域                                                                 ?>
<?//==========================================================================?>
<script type="text/javascript">
	function radioClicked(){
		var k2 = document.getElementById("kaisou2").checked;
		var k3 = document.getElementById("kaisou3").checked;
		document.getElementById("bunrui1_textbox_div").style.display = (k2 || k3) ? "none" : "";
		document.getElementById("bunrui1_dropdown_div").style.display = (k2 || k3) ? "" : "none";
		document.getElementById("bunrui2_dropdown_div").style.display = (k3) ? "" : "none";
		document.getElementById("bunrui2_textbox_div").style.display = (k2) ? "" : "none";
		document.getElementById("bunrui3_textbox_div").style.display = (k3) ? "" : "none";
	}

	function addErr(msg) { errmsg[errmsg.length] = msg; }
	errmsg = [];
	function checkAndSubmit(){
		errmsg = [];
		var k1 = document.getElementById("kaisou1").checked;
		var k2 = document.getElementById("kaisou2").checked;
		var k3 = document.getElementById("kaisou3").checked;
		var code1_dropdown = document.getElementById("code1_dropdown").selectedIndex;
		var code2_dropdown = document.getElementById("code2_dropdown").selectedIndex;

		var code1 = document.getElementById("code1").value;
		var code2 = document.getElementById("code2").value;
		var code3 = document.getElementById("code3").value;

		var ybcd = document.getElementById("yakkou_bunrui_cd").value;
		var yc = document.getElementById("yakkou_caption").value;

		if (!k1 && !k2 && !k3) addErr("分類ラジオボタンを指定してください。");
		if (k1 && code1=="") addErr("分類１コードを入力してください。");
		if ((k2 || k3) && !code1_dropdown) addErr("分類１ドロップダウンを指定してください。");
		if (k2 && code2=="") addErr("分類２コードを入力してください。");
		if (k3 && code2_dropdown<0) addErr("分類２ドロップダウンを指定してください。");
		if (k3 && code3=="") addErr("分類３コードを入力してください。");
		if (k3 && ybcd=="") addErr("薬効分類コードを指定してください。");
		if (yc=="") addErr("薬効分類名を指定してください。");

		if (errmsg.length){
			alert(errmsg.join("\n"));
			return;
		}
		document.frm.mode.value = "regist";
		document.frm.submit();
	}
</script>



<form action="sum_ordsc_yakkou_regist.php" method="get" name="frm" style="margin-top:10px">
	<input type="hidden" name="session" value="<?=$session ?>">
	<input type="hidden" name="mode" value="">

	<table border="0" cellspacing="0" cellpadding="2" style="margin-top:10px; border-collapse:collapse;" class="list1">
		<tr>
			<th><?=$font?><label><input type="radio" name="kaisou" value="1" <?=@$_REQUEST["kaisou"]==""||@$_REQUEST["kaisou"]=="1"?"checked":""?> id="kaisou1" onclick="radioClicked()">分類１</label></font></th>
			<td>
				<div id="bunrui1_dropdown_div" <?=@$_REQUEST["kaisou"]=="2" || @$_REQUEST["kaisou"]=="3" ? '' : 'style="display:none"'?>><?=$font?>
					<select name="code1_dropdown" id="code1_dropdown" onchange="document.frm.submit();">
						<option value=""></option>
						<?
							$sql = " select * from sum_med_yakkou_bunrui1_mst order by sort_order";
							$sel = select_from_table($con, $sql, "", $fname);
							while ($row = pg_fetch_array($sel)) {
								$selected = @$_REQUEST["code1_dropdown"]==$row["code1"] ? "selected" : "";
								echo '<option value="'.$row["code1"].'" '.$selected.'>'.h($row["caption"]).'</option>\n';
							}
						?>
					</select>
				</font></div>
				<div id="bunrui1_textbox_div" <?=@$_REQUEST["kaisou"]=="" || @$_REQUEST["kaisou"]=="1" ? '' : 'style="display:none"'?>><?=$font?>
					<input type="text" name="code1" id="code1" value="<?=h(@$_REQUEST["code1"])?>" style="width:30px" maxlength="1" autocomplete="off" />
				</font></div>
			</td>
		</tr>
		<tr>
			<th><?=$font?><label><input type="radio" name="kaisou" value="2" <?=@$_REQUEST["kaisou"]=="2"?"checked":""?> id="kaisou2" onclick="radioClicked()">分類２</label></font></th>
			<td>
				<div id="bunrui2_dropdown_div" <?=$_REQUEST["kaisou"]=="3" ? '' : 'style="display:none"'?>><?=$font?>
					<select name="code2_dropdown" id="code2_dropdown">
						<?
							$sql = " select * from sum_med_yakkou_bunrui2_mst where code1 = '".@$_REQUEST["code1_dropdown"]."' order by sort_order";
							$sel = select_from_table($con, $sql, "", $fname);
							while ($row = pg_fetch_array($sel)) {
								$selected = (@$_REQUEST["code1_dropdown"]==$row["code1"] && @$_REQUEST["code2_dropdown"]==$row["code2"]) ? "selected" : "";
								echo '<option value="'.h($row["code2"]).'" '.$selected.'>'.h($row["caption"]).'</option>\n';
							}
						?>
					</select>
				</font></div>
				<div id="bunrui2_textbox_div" <?=$_REQUEST["kaisou"]=="2" ? '' : 'style="display:none"'?>><?=$font?>
					<input type="text" name="code2" id="code2" value="<?=h(@$_REQUEST["code2"])?>" style="width:30px" maxlength="1" autocomplete="off" />
				</font></div>
			</td>
		</tr>
		<tr>
			<th><?=$font?><label><input type="radio" name="kaisou" value="3" <?=@$_REQUEST["kaisou"]=="3"?"checked":""?> id="kaisou3" onclick="radioClicked()">分類３</label></font></th>
			<td>
				<div id="bunrui3_textbox_div" <?=$_REQUEST["kaisou"]=="3" ? '' : 'style="display:none"'?>><?=$font?>
					<input type="text" name="code3" id="code3" value="<?=h(@$_REQUEST["code3"])?>" style="width:30px" maxlength="1" autocomplete="off" />
				</font></div>
			</td>
		</tr>
		<tr>
			<th><?=$font?>薬効分類コード</font></th>
			<td><input type="text" name="yakkou_bunrui_cd" id="yakkou_bunrui_cd"  value="<?=h(@$_REQUEST["yakkou_bunrui_cd"])?>" style="width:50px" maxlength="3" value="<?=@$_REQUEST["yakkou_bunrui_cd"]?>" autocomplete="off"></td>
		</tr>
		<tr>
			<th><?=$font?>薬効分類名</font></th>
			<td><input type="text" name="yakkou_caption" id="yakkou_caption" value="<?=h(@$_REQUEST["yakkou_caption"])?>" style="width:500px" maxlength="80" value="<?=@$_REQUEST["caption"]?>" autocomplete="off"></td>
		</tr>
	</table>

	<div style="width:400px; text-align:right; margin-top:30px">
		<input type="button" onclick="checkAndSubmit();" value="登録">
	</div>
</form>




</td>
<!-- right -->
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
