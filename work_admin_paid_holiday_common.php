<?php
require_once("timecard_bean.php");
require_once("timecard_paid_hol_hour_class.php");
require_once("calendar_name_class.php");

/**
 * 年休使用数取得
 *
 * @param object $con DBコネクション
 * @param string $fname 画面名
 * @param array $arr_data 職員情報
 * @param string $closing_paid_holiday 有休休暇の付与日
 * @param string $year 処理年度
 * @param string $criteria_months 基準月数 1:3ヶ月 2:6ヶ月
 * @param string $year_flg 年フラグ 有休使用数を取得する対象年 1:前年 2:当年
 * @param string $select_close_date 当年有休残締日（画面から指定）
 * @return array 年休使用数の配列 キー：職員ID
 *
 */
function get_nenkyu_siyo_su($con, $fname, $arr_data, $closing_paid_holiday, $year, $criteria_months, $year_flg, $select_close_date) {

	$arr_siyo_su = array();
	if (count($arr_data) == 0) {
		return $arr_siyo_su;
	}

	//職員と期間の条件を設定
	if ($year_flg == 1) { //前年分
		if ($closing_paid_holiday == "1") {
			$last_start_date = ($year-1)."0401";
			$start_date = $year."0401";
		} elseif ($closing_paid_holiday == "2") {
			$last_start_date = ($year-1)."0101";
			$start_date = $year."0101";
		}
	} else { //当年分
		if ($closing_paid_holiday == "1") {
			$last_start_date = ($year)."0401";
			$start_date = ($year+1)."0401";
		} elseif ($closing_paid_holiday == "2") {
			$last_start_date = ($year)."0101";
			$start_date = ($year+1)."0101";
		}
	}
	if ($closing_paid_holiday == "1" || $closing_paid_holiday == "2") {
		//有休残締日が範囲内の場合は、範囲終了に設定
		if ($last_start_date <= $select_close_date && $select_close_date < $start_date) {
			$wk_start_date = $select_close_date;
		} else {
			$wk_start_date = $start_date;
		}

		$cond_add = "";
		for ($i=0; $i<count($arr_data); $i++) {
			if ($cond_add != "") {
				$cond_add .= " or ";
			}
			$cond_add .= "emp_id = '".$arr_data[$i]["emp_id"]."'";
		}
		if ($cond_add != "") {
			$cond_add = "(".$cond_add.")";
		}
		$cond_kikan = " ($cond_add and date >= '$last_start_date' and date < '$wk_start_date') ";
	} else { // "3":採用日
		$wk_month = ($criteria_months == "1") ? "3" : "6";
		$cond_add = "";
		for ($i=0; $i<count($arr_data); $i++) {
			$wk_3month_later = "";
			//付与日あり
			$paid_hol_add_mmdd = $arr_data[$i]["paid_hol_add_mmdd"];
			if ($paid_hol_add_mmdd != "") {
				$last_start_date = ($year-1).$paid_hol_add_mmdd;
				$start_date = $year.$paid_hol_add_mmdd;
			}
			//付与日なし
			else {
				$tbl_id = $arr_data[$i]["paid_hol_tbl_id"];
				$date_chk = substr($arr_data[$i]["paid_hol_add_date"],0,4);
				if ($tbl_id == "" || $date_chk == 0) {
					continue;
				}
				// 3ヶ月基準
				if ($criteria_months == "1") {
					$year_after_join = $arr_data[$i]["year_after_join"];
					//入職後1年以上か確認
					if (substr($year_after_join, 0, 4) <= $year) {
						$last_start_date = ($year-1).substr($year_after_join, 4, 4);
						$start_date = $year.substr($year_after_join, 4, 4);
					}
					//以外
					else {
						$wk_3month_later = $arr_data[$i]["paid_hol_add_date"];
						$last_start_date = ($year-1).substr($wk_3month_later, 4, 4);
						$start_date = $year.substr($wk_3month_later, 4, 4);
					}
				}
				// 6ヶ月基準
				else {
					$wk_6month_later = $arr_data[$i]["paid_hol_add_date"];
					$last_start_date = ($year-1).substr($wk_6month_later, 4, 4);
					$start_date = $year.substr($wk_6month_later, 4, 4);
				}
			}
			//当年の場合
			if ($year_flg == 2) {
				//開始を設定し直し
				$last_start_date = $start_date;
				//年度がシステム年以降の場合、指定締日を範囲終了とする（過去の場合は1年後とする）
				if ($year >= date("Y")) {
					$wk_timestamp = date_utils::to_timestamp_from_ymd($select_close_date);
					$start_date = date("Ymd", strtotime("+1 day", $wk_timestamp));
				} else {
					//勤続3ヶ月後の付与日と同じ場合、終了を起算日の1年後とする
					if ($wk_3month_later == $last_start_date) {
						if ($arr_data[$i]["paid_hol_start_date"] != "") {
							$wk_emp_join = $arr_data[$i]["paid_hol_start_date"];
						} else {
							$wk_emp_join = $arr_data[$i]["emp_join"];
						}
						$wk_year = substr($wk_emp_join, 0, 4);
						$start_date = ($wk_year+1).substr($wk_emp_join, 4, 4);
					} else {
						$wk_year = substr($last_start_date, 0, 4);
						$start_date = ($wk_year+1).substr($last_start_date, 4, 4);
					}
				}
			}

			//移行日が範囲内の場合、開始とする
			$tmp_data_migration_date = $arr_data[$i]["data_migration_date"]; //移行日
			//範囲確認用
			$wk_year = substr($last_start_date, 0, 4);
			$wk_start_date = ($wk_year+1).substr($last_start_date, 4, 4);
			if ($last_start_date <= $tmp_data_migration_date && $tmp_data_migration_date < $wk_start_date) {
				$last_start_date = $tmp_data_migration_date;
			}
			//開始終了が逆の場合は不要
			if ($last_start_date > $start_date) {
				continue;
			}

			if ($cond_add != "") {
				$cond_add .= " or ";
			}
			$cond_add .= "(emp_id = '".$arr_data[$i]["emp_id"]."' and date >= '$last_start_date' and date < '$start_date') ";
		}
		if ($cond_add != "") {
			$cond_add = "(".$cond_add.")";
		}
		$cond_kikan = $cond_add;
	}

	//実績データから年休数を取得
	if ($cond_add != "") {

		//データ取得 reason:44半有半公は0.5日とする 55半夏半有 57半有半欠 58半特半有 62半正半有 2午前有休 3午後有休 38午前年休 39午後年休
		$sql = " select atdbkrslt.emp_id,sum(case when reason='2' or reason='3' or reason='38' or reason='39' or reason = '44' or reason='55' or reason='57' or reason='58' or reason='62' then 0.5 else 1 end) as cnt from atdbkrslt ";
		$sql .= " LEFT JOIN atdbk_reason_mst ON atdbkrslt.reason = atdbk_reason_mst.reason_id ";
		$sql .= " where ($cond_kikan) ";
        $sql .= " and ((reason='2' or reason='3' or reason='38' or reason ='39' or reason='44' or reason='55' or reason='57' or reason='58' or reason='62') or (pattern = '10' and reason='1') or (pattern = '10' and reason='37')) ";
		$sql .= " group by atdbkrslt.emp_id ";
		$cond = "";

		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

        $num = pg_numrows($sel);
		//-------------------------------------------------------------------
		//情報設定
		//-------------------------------------------------------------------
		for($i=0;$i<$num;$i++){

			$wk_emp_id = pg_result($sel,$i,"emp_id");
			$cnt = pg_result($sel,$i,"cnt");

			$arr_siyo_su["$wk_emp_id"] = $cnt;
		}
	}
	return $arr_siyo_su;
}

/**
 * 年休使用数取得、付与日を基準
 *
 * @param object $con DBコネクション
 * @param string $fname 画面名
 * @param array $arr_data 職員情報
 * @param string $closing_paid_holiday 有休休暇の付与日
 * @param string $year 処理年度
 * @param string $criteria_months 基準月数 1:3ヶ月 2:6ヶ月
 * @param string $year_flg 年フラグ 有休使用数を取得する対象年 1:前年 2:当年
 * @param string $select_close_date 有休残締日（画面から指定）
 * @param string $select_add_date 付与年月日（画面から指定）
 * @param string $arr_specified_time 所定時間情報
 * @param string $paid_hol_hour_flag 時間有休許可 "t":する "f":しない
 * @param array $arr_prev_days_second 直近1年の2件目のデータ
 * @return array 年休使用数の配列 キー：職員ID
 *
 */
function get_nenkyu_siyo_su2($con, $fname, $arr_data, $closing_paid_holiday, $year, $criteria_months, $year_flg, $select_close_date, $select_add_date, $arr_specified_time, $paid_hol_hour_flag, $arr_prev_days_second) {

	$arr_siyo_su = array();
	if (count($arr_data) == 0) {
		return $arr_siyo_su;
	}

	$cond_add = "";
	$cond_add_hour = ""; //時間有休用
	for ($i=0; $i<count($arr_data); $i++) {
		//付与日を基準とする
		$add_date = substr($arr_data[$i]["add_date"], 0, 4).substr($arr_data[$i]["add_date"], 5, 2).substr($arr_data[$i]["add_date"], 8, 2);
		if ($add_date == "") {
			continue;
		}

		$shimebi_flg = false; //期間の終了日を含めるため
		//前年の場合
		if ($year_flg == 1) {
			$start_date = $add_date;
            //直近1年に2件付与データがある場合の対応 20131030
            $wk_emp_id = $arr_data[$i]["emp_id"];
            if ($arr_prev_days_second[$wk_emp_id]["add_date"] != "") {
                $last_start_date = $arr_prev_days_second[$wk_emp_id]["add_date"];
            }
            else {
                $wk_last_year = intval(substr($add_date, 0, 4)) - 1;
                $last_start_date = $wk_last_year.substr($add_date, 4, 4);
            }
		}
		//当年の場合
		else {
			$last_start_date = $add_date;
			//付与年月日
			if ($select_add_date != "") {
				$wk_next_year = intval(substr($add_date, 0, 4)) + 1;
				$start_date = $wk_next_year.substr($add_date, 4, 4);
			} else {
				//有休締め日まで
				$start_date = $select_close_date;
				$shimebi_flg = true;
			}
		}
        $last_start_date_hour = $last_start_date;
		//移行日が範囲内の場合、開始とする
		$tmp_data_migration_date = $arr_data[$i]["data_migration_date"]; //移行日
		//前回付与日も範囲内か確認 20120710
		$tmp_last_add_date = $arr_data[$i]["last_add_date"];
		list($wk_import_add_year, $wk_month, $wk_day) = split("/", $tmp_last_add_date);
		$last_add_date = sprintf("%04d%02d%02d", $wk_import_add_year, $wk_month, $wk_day); //yyyymmdd形式
		//範囲確認用
		$wk_year = substr($last_start_date, 0, 4);
		$wk_start_date = ($wk_year+1).substr($last_start_date, 4, 4);
        if (($last_start_date <= $tmp_data_migration_date && $tmp_data_migration_date < $wk_start_date) &&
      ($last_start_date <= $last_add_date && $last_add_date < $wk_start_date)) {
			//20110414 移行日の翌日からとする
			$wk_timestamp = date_utils::to_timestamp_from_ymd($tmp_data_migration_date);
			$last_start_date = date("Ymd", strtotime("+1 day", $wk_timestamp));
		}
		//開始終了順の場合（逆の場合は除く）
		if ($last_start_date <= $start_date) {

		    if ($cond_add != "") {
			    $cond_add .= " or ";
		    }
		    $wk_fugou = ($shimebi_flg) ? "=" : "";
		    $cond_add .= "(emp_id = '".$arr_data[$i]["emp_id"]."' and date >= '$last_start_date' and date <$wk_fugou '$start_date') ";
        }

        //時間有休用移行日が範囲内の場合、開始とする
        $tmp_hol_hour_data_migration_date = $arr_data[$i]["hol_hour_data_migration_date"]; //移行日
        if ($last_start_date_hour <= $tmp_hol_hour_data_migration_date && $tmp_hol_hour_data_migration_date < $wk_start_date) {
            //移行日の翌日からとする
            $wk_timestamp = date_utils::to_timestamp_from_ymd($tmp_hol_hour_data_migration_date);
            $last_start_date_hour = date("Ymd", strtotime("+1 day", $wk_timestamp));
        }

		//開始終了順の場合（逆の場合は除く）
        if ($last_start_date_hour <= $start_date) {
            //時間有休用 $cond_add_hour
            if ($cond_add_hour != "") {
                $cond_add_hour .= " or ";
            }
            //時間有休用移行データ
            $wk_fugou = ($shimebi_flg) ? "=" : "";
            $cond_add_hour .= "(emp_id = '".$arr_data[$i]["emp_id"]."' and start_date >= '$last_start_date_hour' and start_date <$wk_fugou '$start_date') ";
        }
	}
	if ($cond_add != "") {
		$cond_add = "(".$cond_add.")";
	}
	$cond_kikan = $cond_add;

	//実績データから年休数を取得
	if ($cond_add != "") {

		//データ取得 reason:44半有半公は0.5日とする 55半夏半有 57半有半欠 58半特半有 62半正半有 2午前有休 3午後有休 38午前年休 39午後年休
		$sql = " select atdbkrslt.emp_id,sum(case when reason='2' or reason='3' or reason='38' or reason='39' or reason = '44' or reason='55' or reason='57' or reason='58' or reason='62' then 0.5 else 1 end) as cnt from atdbkrslt ";
		$sql .= " LEFT JOIN atdbk_reason_mst ON atdbkrslt.reason = atdbk_reason_mst.reason_id ";
		$sql .= " where ($cond_kikan) ";
        $sql .= " and ((reason='2' or reason='3' or reason='38' or reason ='39' or reason='44' or reason='55' or reason='57' or reason='58' or reason='62') or (pattern = '10' and reason='1') or (pattern = '10' and reason='37')) ";
		$sql .= " group by atdbkrslt.emp_id ";
		$cond = "";
        //echo("<!-- sql=$sql -->\n");

		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$num = pg_numrows($sel);
		//-------------------------------------------------------------------
		//情報設定
		//-------------------------------------------------------------------
		for($i=0;$i<$num;$i++){

			$wk_emp_id = pg_result($sel,$i,"emp_id");
			$cnt = pg_result($sel,$i,"cnt");

			$arr_siyo_su["$wk_emp_id"] = $cnt;
		}
	}
	//時間有休データから取得時間数を取得
	if ($paid_hol_hour_flag == "t" && $cond_add_hour != "") {
        $sql = "select emp_id, sum(to_number(use_hour, '99') * 60 + to_number(use_minute, '99')) as total_minute from timecard_paid_hol_hour";
		$cond = "where $cond_add_hour ";
		$cond .= " group by emp_id ";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$num = pg_numrows($sel);
		//-------------------------------------------------------------------
		//情報設定
		//-------------------------------------------------------------------
		for($i=0;$i<$num;$i++){

            //時間有休はキーを別にして設定 20120517
            $wk_emp_id = "hour_".pg_result($sel,$i,"emp_id");
			$total_minute = pg_result($sel,$i,"total_minute");

            $arr_siyo_su["$wk_emp_id"] = $total_minute;
        }


	}
	return $arr_siyo_su;
}

/**
 * 勤続年数取得
 *
 * @param mixed $start_date 開始年月日YYYYMMDD形式
 * @param mixed $end_date 終了年月日YYYYMMDD形式
 * @return mixed 勤続年数、YY/MM形式
 *
 */
function get_length_of_service($start_date, $end_date) {

	$year1 = intval(substr($start_date, 0, 4));
	$mon1 = intval(substr($start_date, 4, 2));
	$day1 = intval(substr($start_date, 6, 2));

	$year2 = intval(substr($end_date, 0, 4));
	$mon2 = intval(substr($end_date, 4, 2));
	$day2 = intval(substr($end_date, 6, 2));

	//年月日が正しくない場合、空白を返す
	if ($day1 == 0 || $day2 == 0) {
		return "";
	}

	$diff = ($year2 * 12 + $mon2) - ($year1 * 12 + $mon1);

	if ($day1 > $day2 && $diff > 0) {
		$diff--;
	}

	$year = intval($diff / 12);
	$mon = $diff % 12;

	$str = $year."/".$mon;

	return $str;
}


/**
 * 有休付与画面の検索条件を返す
 *
 * @param mixed $srch_name 職員名
 * @param mixed $cls 事務所
 * @param mixed $atrb 課
 * @param mixed $dept 科
 * @param mixed $duty_form_jokin 常勤
 * @param mixed $duty_form_hijokin 非常勤
 * @param mixed $group_id グループ
 * @param mixed $select_add_mon 付与日の月
 * @param mixed $select_add_day 付与日の日
 * @param mixed $year 年度
 * @param string $criteria_months 基準月数 1:3ヶ月 2:6ヶ月
 * @param mixed $select_add_yr 付与日の年 20110210追加
 * @param mixed $closing_paid_holiday 有休締め日 1:4/1, 2:1/1 3:採用日
 * @param mixed $select_close_date 有休残締め日
 * @param mixed $srch_id 職員ID
 * @return mixed 検索条件
 *
 */
function get_paid_srch_cond($srch_name, $cls, $atrb, $dept, $duty_form_jokin, $duty_form_hijokin, $group_id, $select_add_mon, $select_add_day, $year, $criteria_months, $select_add_yr, $closing_paid_holiday, $select_close_date, $srch_id) {

	$wk_month = ($criteria_months == "1") ? "3" : "6";

	$cond = "where exists (select * from authmst where authmst.emp_id = empmst.emp_id and authmst.emp_del_flg = 'f')";
	//検索条件　職員ID
	if ($srch_id != "") {
		$tmp_srch_id = str_replace(" ", "", $srch_id);
        $cond .= " and (empmst.emp_personal_id like '%$tmp_srch_id%')";
	}
    //検索条件　職員名
    if ($srch_name != "") {
        $tmp_srch_name = str_replace(" ", "", $srch_name);
        $cond .= " and (emp_lt_nm like '%$tmp_srch_name%' or emp_ft_nm like '%$tmp_srch_name%' or emp_kn_lt_nm like '%$tmp_srch_name%' or emp_kn_ft_nm like '%$tmp_srch_name%' or emp_lt_nm || emp_ft_nm like '%$tmp_srch_name%' or emp_kn_lt_nm || emp_kn_ft_nm like '%$tmp_srch_name%')";
    }
    //検索条件　属性：事務所
	if ($cls != "" && substr($cls, 0, 1) != "-") {
		$cond .= " and ((empmst.emp_class = $cls) or exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_class = $cls))";
	}
	//検索条件　属性：課
	if ($atrb != "" && substr($atrb, 0, 1) != "-") {
		$cond .= " and ((empmst.emp_attribute = $atrb) or exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_attribute = $atrb))";
	}
	//検索条件　属性：科
	if ($dept != "" && substr($dept, 0, 1) != "-") {
		$cond .= " and ((empmst.emp_dept = $dept) or exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_dept = $dept))";
	}
	//検索条件　雇用・勤務形態
	if ($duty_form_jokin != $duty_form_hijokin) {
		if ($duty_form_hijokin == "checked") {
			//非常勤
			$cond .= " and ";
		}
		else {
			//常勤
			$cond .= " and NOT ";
		}
		$cond .= "EXISTS(SELECT * FROM empcond WHERE empcond.emp_id = empmst.emp_id AND duty_form = 2)";
	}
	//検索条件　グループ
	if ($group_id != "" && $group_id != "-") {
		$cond .= " and exists (select *  from duty_shift_staff where empmst.emp_id = duty_shift_staff.emp_id and  group_id in (select group_id from duty_shift_group where pattern_id = $group_id))";
	}
	//付与年月日、有休残締め日のどちらもない場合は、以下の条件は不要
	if (($select_add_yr == "-" || $select_add_yr == "") &&
			($select_close_date == "---")) {
		return $cond;
	}

	//検索条件　付与日 月日、または、月
	//有給付与起算日を入職日より優先して判断
	if ($select_add_mon != "-" && $select_add_mon != ""
		/* && $closing_paid_holiday == "3" //採用日の場合 20110311 採用日以外も付与日指定可能とする 20110407 */
		) {
		$search_mmdd = "";
		if ($select_add_mon != "-" && $select_add_mon != "" && $select_add_day != "-" && $select_add_day != "") {
			$search_mmdd = $select_add_mon.$select_add_day;
			$compare_mmdd = "mmdd";
			$compare_len = 4;
		} elseif ($select_add_mon != "-" && $select_add_mon != "") {
			$search_mmdd = $select_add_mon;
			$compare_mmdd = "mm";
			$compare_len = 2;
		}
		//付与日がない場合、採用日が1年以内 20110407
		$wk_year_before = ($select_add_yr - 1).$select_add_mon;
		if ($select_add_day != "-" && $select_add_day != "") {
			$wk_year_before .= $select_add_day;
		} else {
			$wk_year_before .= "01";
		}
		//6ヶ月基準
		if ($criteria_months == "2") {
			//(付与日が登録済の場合、検索の月日 = 付与日) or
			//(付与日がない場合 and
			//((起算日 != '' and 検索の月日 = 起算日の月日) or
			//(起算日 = '' and 入職日 != '' and 検索の月日 = 入職日の月日)))
			$cond .= " and ";
			$cond .= "( ";
			$cond .= " (emppaid.paid_hol_add_mmdd is not null and emppaid.year = '$select_add_yr' and substr(emppaid.paid_hol_add_mmdd, 1, $compare_len) = '$search_mmdd'  ";	//付与日が登録済の場合
			$cond .= " ) or ";
			$cond .= " (emppaid.paid_hol_add_mmdd is null and substr(g.paid_hol_add_mmdd, 1, $compare_len) = '$search_mmdd' ";	//付与日がない場合、前回の付与日
			$cond .= " ) or ";
			$cond .= " (emppaid.paid_hol_add_mmdd is null and g.paid_hol_add_mmdd is null and";	//付与日がない場合
			$cond .= "  ( ";
			$cond .= "   (f.paid_hol_start_date is not null and f.paid_hol_start_date != '' and to_char(to_timestamp(f.paid_hol_start_date, 'yyyymmdd')  + '$wk_month months', '$compare_mmdd') = '$search_mmdd') or"; //起算日がある場合、6ヶ月後
			$cond .= "   ((f.paid_hol_start_date is null or f.paid_hol_start_date = '') and emp_join != '' and to_char(to_timestamp(emp_join, 'yyyymmdd')  + '$wk_month months', '$compare_mmdd') = '$search_mmdd')";	 //起算日がない場合、入職日の6ヶ月後
			$cond .= "  ) ";
			$cond .= " )";
			//6ヶ月基準時の10月から3月入職の検索条件を追加
			if ($select_add_mon == "04") {
				$wk_n_start = ($select_add_yr-1)."1001";
				$wk_n_end = $select_add_yr."0331";
				$cond .= "  or ( ";
				$cond .= " emp_join >= '$wk_n_start' and emp_join <= '$wk_n_end' ";
				$cond .= "  ) ";
			}
			//1月1日基準で、有給付与月が1月の場合
			if ($closing_paid_holiday == "2" && $select_add_mon == "01") {
				$cond .= "  or ( ";
				$cond .= " emp_join > '$wk_year_before' ";
				$cond .= "  ) ";
			}

			$cond .= ")";
			$cond .= " ";
		}
		//3ヶ月基準
		else {
			$wk_select_add_yr = $select_add_yr-1;
			//3ヶ月の場合
			$select_add_mon2 = $select_add_mon + 3;
			$wk_select_add_yr2 = $wk_select_add_yr;
			if ($select_add_mon2 > 12) {
				$select_add_mon2 -= 12;
				$wk_select_add_yr2++;
			}
			$search_mmdd2 = sprintf("%02d", $select_add_mon2);
			if ($select_add_day != "-" && $select_add_day != "") {
				$search_mmdd2 .= $select_add_day;
			}

			//(起算日 != '' and 検索の月日 = 起算日の月日) or
			//(起算日 = '' and 入職日 != '' and 検索の月日 = 入職日の月日)
			$cond .= " and ";
			$cond .= "(";
			$cond .= " (emppaid.paid_hol_add_mmdd is not null and emppaid.year = '$select_add_yr' and substr(emppaid.paid_hol_add_mmdd, 1, $compare_len) = '$search_mmdd'  ";	//付与日が登録済の場合
			$cond .= " ) or ";
			$cond .= " (emppaid.paid_hol_add_mmdd is null and ((g.service_mon != '3' and CAST(g.year AS varchar) = '$wk_select_add_yr' and substr(g.paid_hol_add_mmdd, 1, $compare_len) = '$search_mmdd') or ";	//付与日がない場合、前回の付与日
			$cond .= " (g.service_mon = '3' and CAST(g.year AS varchar) = '$wk_select_add_yr2' and substr(g.paid_hol_add_mmdd, 1, $compare_len) = '$search_mmdd2'))  ";	//3ヶ月の場合
			$cond .= " ) or ";
			$cond .= " (emppaid.paid_hol_add_mmdd is null and g.paid_hol_add_mmdd is null and";	//付与日がない場合
			$cond .= " (";
			$cond .= " (f.paid_hol_start_date is not null and f.paid_hol_start_date != '' and ";
			$cond .= "  (";
			$cond .= "   (";
			$cond .= "    to_char(to_timestamp(f.paid_hol_start_date, 'yyyymmdd')  + '12 months', 'yyyy') > '$select_add_yr' "; //1年未満
			$cond .= "    and to_char(to_timestamp(f.paid_hol_start_date, 'yyyymmdd')  + '$wk_month months', '$compare_mmdd') = '$search_mmdd' ";
			$cond .= "   ) or ";
			$cond .= "   (";
			$cond .= "    to_char(to_timestamp(f.paid_hol_start_date, 'yyyymmdd')  + '12 months', 'yyyy') <= '$select_add_yr' "; //1年以降
			$cond .= "    and substr(f.paid_hol_start_date, 5, $compare_len) = '$search_mmdd' ";
			$cond .= "   ) ";
			$cond .= "  ) ";
			$cond .= " ) or ";
			$cond .= " ((f.paid_hol_start_date is null or f.paid_hol_start_date = '') and emp_join != '' and ";
			$cond .= "  (";
			$cond .= "   (";
			$cond .= "    to_char(to_timestamp(emp_join, 'yyyymmdd')  + '12 months', 'yyyymm') > '$select_add_yr$select_add_mon' "; //1年未満
			$cond .= "    and to_char(to_timestamp(emp_join, 'yyyymmdd')  + '$wk_month months', '$compare_mmdd') = '$search_mmdd' ";
			$cond .= "   ) or ";
			$cond .= "   (";
			$cond .= "    to_char(to_timestamp(emp_join, 'yyyymmdd')  + '12 months', 'yyyymm') <= '$select_add_yr$select_add_mon' "; //1年以降
			$cond .= "    and substr(emp_join, 5, $compare_len) = '$search_mmdd' ";
			$cond .= "   ) ";
			$cond .= "  ) ";
			$cond .= " ) ";
			$cond .= " )";
			$cond .= " )";
			//3ヶ月基準時の1月から3月入職者の検索条件を追加
			if ($select_add_mon == "04") {
				$wk_n_start = $select_add_yr."0101";
				$wk_n_end = $select_add_yr."0331";
				$cond .= "  or ( ";
				$cond .= " emp_join >= '$wk_n_start' and emp_join <= '$wk_n_end' ";
				$cond .= "  ) ";
			}
			//1月1日基準で、有給付与月が1月の場合
			if ($closing_paid_holiday == "2" && $select_add_mon == "01") {
				$cond .= "  or ( ";
				$cond .= " emp_join > '$wk_year_before' ";
				$cond .= "  ) ";
			}
			$cond .= ")";

		}
		//付与日がない場合、採用日が1年以内 20110407
		$wk_year_before = ($select_add_yr - 1).$select_add_mon;
		if ($select_add_day != "-" && $select_add_day != "") {
			$wk_year_before .= $select_add_day;
		} else {
			$wk_year_before .= "01";
		}


		//3ヶ月基準時の1月から3月入職者の検索条件
		if ($criteria_months == "1") {
			$wk_n_start = $select_add_yr."0101";
		}
		//6ヶ月基準時の10月から3月入職の検索条件
		else {
			$wk_n_start = ($select_add_yr-1)."1001";
		}
		$wk_n_end = $select_add_yr."0331";

//今回データ有り、または
//（今回データなし、かつ、前回データあり、付与日が1年前か9ヶ月前）、または
//（今回データなし、かつ、前回データなし、入職日後3ヶ月か6ヶ月）
		$cond .= " and ";
		$cond .= " (emppaid.year != '' or emppaid.year is not null or ";
		$cond .= "  ( ";
		$cond .= "   emppaid.year is null and g.year != '' and g.year is not null "; //前回付与日の判断は上のSQLで追加されている
		$cond .= "  ) or ";
		$cond .= "  ( ";
		$cond .= "   emppaid.year is null and g.year is null and emp_join != '' and ";
		$cond .= "   emp_join > '$wk_year_before' and "; //1年未満
		$cond .= "   to_char(to_timestamp(emp_join, 'yyyymmdd')  + '$wk_month months', '$compare_mmdd') = '$search_mmdd' ";
		$cond .= "  ) ";
		//有給付与月が4月の場合、新入職者の条件追加
		if ($select_add_mon == "04") {
			$cond .= "  or ( ";
			$cond .= " emp_join >= '$wk_n_start' and emp_join <= '$wk_n_end' ";
			$cond .= "  ) ";
		}
		//1月1日基準で、有給付与月が1月の場合
		if ($closing_paid_holiday == "2" && $select_add_mon == "01") {
			$cond .= "  or ( ";
			$cond .= " emp_join > '$wk_year_before' ";
			$cond .= "  ) ";
		}
		$cond .= " ) ";


	}
	//付与年月日が未設定の場合、有休残締め日より前を条件とする
	else {
		//締め日が採用日
		if ($closing_paid_holiday == "3") {
			$cond .= " and (to_char(to_timestamp(case when f.paid_hol_start_date != '' then f.paid_hol_start_date else emp_join end, 'yyyymmdd')  + '".$wk_month." months', 'yyyymmdd') <= '$select_close_date' ) ";
		}
	}
	//年度確認、採用日が未設定、または、設定済の場合は処理年時点に在籍している
	$cond .= " and ";
	$cond .= "( ";
	$cond .= " (f.paid_hol_start_date is null and emp_join = '') or";
	$cond .= " (substr(case when f.paid_hol_start_date is not null then f.paid_hol_start_date else emp_join end, 1, 4) <= '$year') ";
	$cond .= ") ";

	return $cond;
}

/**
 * 有休付与画面用のSQL文を返す
 *
 * @param mixed $srch_name 職員名
 * @param mixed $cls 事務所
 * @param mixed $atrb 課
 * @param mixed $dept 科
 * @param mixed $duty_form_jokin 常勤
 * @param mixed $duty_form_hijokin 非常勤
 * @param mixed $group_id グループ
 * @param mixed $select_add_mon 付与日の月
 * @param mixed $select_add_day 付与日の日
 * @param mixed $year 年度
 * @param string $criteria_months 基準月数 1:3ヶ月 2:6ヶ月
 * @param mixed $select_add_yr 付与日の年 20110210追加
 * @param mixed $closing_paid_holiday 有休締め日 1:4/1, 2:1/1 3:採用日
 * @param mixed $select_close_yr 有休残締め日年
 * @param mixed $select_close_mon 月
 * @param mixed $select_close_day 日
 * @param mixed $sort ソート順
 * @return mixed 検索条件
 *
 */
function get_paid_srch_sql($srch_name, $cls, $atrb, $dept, $duty_form_jokin, $duty_form_hijokin, $group_id, $select_add_mon, $select_add_day, $year, $criteria_months, $select_add_yr, $closing_paid_holiday, $select_close_yr, $select_close_mon, $select_close_day, $sort) {

	$wk_month = ($criteria_months == "1") ? "3" : "6";

	//付与年月日未設定時、プルダウンの"-"を""にする
	if ($select_add_yr == "-") {
		$select_add_yr = "";
	}
	if ($select_add_mon == "-") {
		$select_add_mon = "";
	}
	if ($select_add_day == "-") {
		$select_add_day = "";
	}

	//有給付与起算日paid_hol_start_dateがある場合は入職日emp_joinより優先して判断する
	// empmst 職員マスタ
	// emppaid b 有休付与データ 直近1年のデータ、検索条件の付与日がある場合は付与日とあっているデータ
	// classmst c 所属マスタ1階層
	// atrbmst d 所属マスタ2階層
	// deptmst e 所属マスタ3階層
	// empcond f 勤務条件
	// emppaid g 前回有休付与データ
	// timecard_paid_hol_import h 有休残移行データ
	// emppaid i 基準月数3ヶ月の有休付与データ
	// emppaid k 最新の有休付与データ、検索条件の付与日・有休残締め日が未設定の場合使用
	// jobmst l 職種
	//

	//ソート順
	if ($sort == "") {
		$sort = "1";
	}
	//2件ある場合でも1件のみ表示するようにする
	switch ($sort) {
		case "1":
		case "2":
			$distinct = " distinct on (empmst.emp_personal_id)  ";
			break;
		case "3":
		case "4":
			$distinct = " distinct on (empmst.emp_kn_lt_nm, empmst.emp_kn_ft_nm, empmst.emp_personal_id) ";
			break;
		case "5":
		case "6":
            $distinct = " distinct on (c.order_no, d.order_no, e.order_no, empmst.emp_personal_id) ";
			break;
	}
	$sql = "select $distinct empmst.emp_id, empmst.emp_personal_id, emp_lt_nm, emp_ft_nm ";
	$sql .= " , c.class_nm, d.atrb_nm, e.dept_nm, emp_join, f.paid_hol_tbl_id, f.paid_hol_start_date, f.duty_form ";
	$sql .= " , l.job_nm ";
	//有休付与データ項目
	if ($select_add_yr != "" || $select_close_yr != "-") {
		$sql .= " , emppaid.days1, emppaid.days2, emppaid.adjust_day, g.days1 as prev_days1 ,g.days2 as prev_days2, g.adjust_day as prev_adjust_day ";
		$sql .= " , to_char(to_timestamp(case when f.paid_hol_start_date != '' then f.paid_hol_start_date else emp_join end, 'yyyymmdd')  + '".$wk_month." months', 'yyyymmdd') as paid_hol_add_date ";
		$sql .= " , to_char(to_timestamp(case when f.paid_hol_start_date != '' then f.paid_hol_start_date else emp_join end, 'yyyymmdd')  + '12 months', 'yyyymmdd') as year_after_join ";
		$sql .= " , emppaid.paid_hol_emp_join as reg_emp_join, emppaid.paid_hol_start_date as reg_start_date, emppaid.paid_hol_tbl_id as reg_tbl_id, emppaid.paid_hol_add_mmdd, emppaid.service_year, emppaid.service_mon ";
		$sql .= " , h.last_add_date, h.last_carry, h.last_add, h.last_use, h.last_remain, h.curr_carry, h.curr_add, h.curr_use, h.curr_remain, h.data_migration_date ";
        $sql .= " , emppaid.year as emppaid_year ";
		$sql .= " , i.year as mon3_year, i.paid_hol_add_mmdd as mon3_paid_hol_add_mmdd, i.days1 as mon3_days1 ,i.days2 as mon3_days2, i.adjust_day as mon3_adjust_day ";
		$sql .= " , g.paid_hol_tbl_id as prev_tbl_id, g.service_year as prev_service_year, g.service_mon as prev_service_mon, g.year as prev_year, g.paid_hol_add_mmdd as prev_paid_hol_add_mmdd ";
        $sql .= " , g.carry_time_minute as prev_carry_time_minute ";
        $sql .= " , m.last_carry as hol_hour_last_carry, m.last_use as hol_hour_last_use, m.curr_carry as hol_hour_curr_carry, m.curr_use as hol_hour_curr_use, m.data_migration_date as hol_hour_data_migration_date, emppaid.carry_time_minute as curr_carry_time_minute "; //時間有休 20120517
    } else {
		$sql .= " , k.year as last_add_year, k.paid_hol_add_mmdd as last_add_mmdd ";
	}

	//reg_* 有休付与テーブルに登録済のデータ
	$sql .= " from empmst ";

	//付与日優先
	if ($select_add_yr != "") {

		$search_mmdd = "";
		if ($select_add_mon != "" && $select_add_day != "") {
			$search_mmdd = $select_add_mon.$select_add_day;
			$compare_mmdd = "mmdd";
			$compare_len = 4;
		} elseif ($select_add_mon != "") {
			$search_mmdd = $select_add_mon;
			$compare_mmdd = "mm";
			$compare_len = 2;
		}
		//2件有り得るので1件に絞り込む 20110404
		$sql .= " left join (select distinct on (b.emp_id) b.* from emppaid b  ";
		$sql .= " where   ";
		$sql .= " CAST(b.year AS varchar) = '$select_add_yr' and substr(b.paid_hol_add_mmdd, 1, $compare_len) = '$search_mmdd' order by b.emp_id, b.year desc, b.paid_hol_add_mmdd desc ";
		$sql .= " ) emppaid on emppaid.emp_id = empmst.emp_id ";
	}
	elseif ($select_close_yr != "-") {
		$start_ymd = ($select_close_yr-1).$select_close_mon.$select_close_day;
		$end_ymd = $select_close_yr.$select_close_mon.$select_close_day;

		//2件有り得るので1件に絞り込む
		$sql .= " left join (select distinct on (j.emp_id, j.year, j.paid_hol_add_mmdd) j.* from emppaid j  ";
		$sql .= " where CAST(j.year AS varchar)||j.paid_hol_add_mmdd =  ";
		$sql .= " (select max(CAST(b.year AS varchar)||b.paid_hol_add_mmdd ) from emppaid b  ";
		$sql .= " where j.emp_id = b.emp_id  and  ";
		$sql .= " (((CAST(b.year AS varchar)||b.paid_hol_add_mmdd > '$start_ymd' and ";
		$sql .= " CAST(b.year AS varchar)||b.paid_hol_add_mmdd <= '$end_ymd') ";
		$sql .= " ))) order by j.emp_id, j.year desc, j.paid_hol_add_mmdd desc ";
		$sql .= " ) emppaid on emppaid.emp_id = empmst.emp_id ";

	}
	//年月日指定なし時、最新の付与年月日を取得
	else {
		$sql .= " left join (select distinct on (b.emp_id) b.* from emppaid b  ";
		$sql .= "  order by b.emp_id, b.year desc, b.paid_hol_add_mmdd desc ";
		$sql .= " ) k on k.emp_id = empmst.emp_id ";
	}
	$sql .= " left join classmst c on c.class_id = empmst.emp_class ";
	$sql .= " left join atrbmst d on d.atrb_id = empmst.emp_attribute ";
	$sql .= " left join deptmst e on e.dept_id = empmst.emp_dept ";
	$sql .= " left join empcond f on f.emp_id = empmst.emp_id ";
	$sql .= " ";
	//付与日優先
	if ($select_add_yr != "-" && $select_add_yr != "") {
		$wk_year_before = ($select_add_yr - 1).$select_add_mon;
		$select_add_ymd = $select_add_yr.$select_add_mon;
		if ($select_add_day != "-" && $select_add_day != "") {
			$wk_year_before .= $select_add_day;
			$select_add_ymd .= $select_add_day;
		} else {
			$wk_year_before .= "01";
			$select_add_ymd .= "01";
		}
		$sql .= " left join (select distinct on (j.emp_id) j.* from emppaid j where (CAST(j.year AS varchar)||j.paid_hol_add_mmdd >= '$wk_year_before' and ";
		$sql .= " CAST(j.year AS varchar)||j.paid_hol_add_mmdd < '$select_add_ymd')  order by j.emp_id, j.year desc, j.paid_hol_add_mmdd desc ";
		$sql .= " ) g on g.emp_id = empmst.emp_id ";


	}
	elseif ($select_close_yr != "-") {
		//年範囲調整 20110208
		if ($closing_paid_holiday != "3") { // 1:4/1, 2:1/1
			// 2年前まで、2件有り得るので1件に絞り込む 20110325
			$wk_mmdd = ($closing_paid_holiday == "1") ? "0401" : "0101";
			$start_ymd = ($year-2).$wk_mmdd;
			$end_ymd = $year.$wk_mmdd;
			$sql .= " left join (select distinct on (j.emp_id, j.year, j.paid_hol_add_mmdd) j.* from emppaid j  ";
			$sql .= " where CAST(j.year AS varchar)||j.paid_hol_add_mmdd =  ";
			$sql .= " (select max(CAST(b.year AS varchar)||b.paid_hol_add_mmdd ) from emppaid b  ";
			$sql .= " where j.emp_id = b.emp_id  and  ";
			$sql .= " CAST(b.year AS varchar)||b.paid_hol_add_mmdd >= '$start_ymd' and ";
			$sql .= " CAST(b.year AS varchar)||b.paid_hol_add_mmdd < '$end_ymd') order by j.emp_id, j.year desc, j.paid_hol_add_mmdd desc ";
			$sql .= " ) g on g.emp_id = empmst.emp_id ";
		}
		//3:採用日
		else {
			$start_ymd = ($select_close_yr-3).$select_close_mon.$select_close_day;
			$end_ymd = ($select_close_yr-1).$select_close_mon.$select_close_day;
			//2件有り得るので1件に絞り込む
			$sql .= " left join (select distinct on (j.emp_id, j.year, j.paid_hol_add_mmdd) j.* from emppaid j  ";
			$sql .= " where CAST(j.year AS varchar)||j.paid_hol_add_mmdd =  ";
			$sql .= " (select max(CAST(b.year AS varchar)||b.paid_hol_add_mmdd ) from emppaid b  ";
			$sql .= " where j.emp_id = b.emp_id  and  ";
			$sql .= " CAST(b.year AS varchar)||b.paid_hol_add_mmdd > '$start_ymd' and ";
			$sql .= " CAST(b.year AS varchar)||b.paid_hol_add_mmdd <= '$end_ymd') order by j.emp_id, j.year desc, j.paid_hol_add_mmdd desc ";
			$sql .= " ) g on g.emp_id = empmst.emp_id ";
		}
	}

	if ($select_add_yr != "" || $select_close_yr != "-") {
        //有休移行データ
		$sql .= " left join timecard_paid_hol_import h on h.emp_personal_id = empmst.emp_personal_id ";
		//基準月数3ヶ月の場合、初回の3ヶ月のデータを探す
		$sql .= " left join emppaid i on i.emp_id = empmst.emp_id and to_char(to_timestamp(case when f.paid_hol_start_date != '' then f.paid_hol_start_date else empmst.emp_join end, 'yyyymmdd')  + '3 months', 'yyyymmdd') = CAST(i.year AS varchar)||i.paid_hol_add_mmdd ";
        //時間有休移行データ 20120517
        $sql .= " left join timecard_paid_hol_hour_import m on m.emp_personal_id = empmst.emp_personal_id ";
    }
	$sql .= " left join jobmst l on l.job_id = empmst.emp_job ";

	return $sql;
}

/**
 * 職員一覧を表示
 *
 * @param object $con DBコネクション
 * @param string $fname 画面名
 * @param object $sel SQL結果
 * @param string $closing_paid_holiday 有休休暇の付与日
 * @param array $arr_paid_hol 有休付与日数表の配列
 * @param string $year 処理年度
 * @param string $criteria_months 基準月数 1:3ヶ月 2:6ヶ月
 * @param object $atdbk_common_class 出勤表関連共通クラス
 * @param string $select_close_date 当年有休残締日
 * @param string $sort ソート順
 * @param string $select_add_date 付与年月日
 * @param string $output_flg 1:web 2:excel 3:印刷
 * @return 有休数一覧の文字列データ、2:excel時データを返し、それ以外の1,3は、echo表示しデータを返さない。
 *
 */
function get_emp_list_data($con, $fname, $sel, $closing_paid_holiday, $arr_paid_hol, $year, $criteria_months, $atdbk_common_class, $select_close_date, $sort, $select_add_date, $output_flg) {
	if (!$sel) {
		return "";
	}

    //時間有休
    $obj_hol_hour = new timecard_paid_hol_hour_class($con, $fname);
    $obj_hol_hour->select();

    if ($obj_hol_hour->paid_hol_hour_flag == "t") {
        $data = get_emp_list_data2($con, $fname, $sel, $closing_paid_holiday, $arr_paid_hol, $year, $criteria_months, $atdbk_common_class, $select_close_date, $sort, $select_add_date, $output_flg, $obj_hol_hour);
        return $data;
    }

	//基準月数
	$wk_month = ($criteria_months == "1") ? "3" : "6";

	//一覧見出しの配列
	$arr_title = array(
			"職員ID", //0
			"職員氏名",
			"職種", //2:excelのみに出力
			"所属", //3
			"採用年月日", //4:採用年月日から15:調整日数までを印刷には出力しない
			"有休表", //5
			"付与年月日",
			"勤続<br>年数",
			"前年<br>繰越",
			"前年<br>付与",
            "前年<br>調整", //10
            "前年<br>取得", //11
			"前年<br>残",
			"当年<br>繰越",
			"当年<br>付与",
            "当年<br>調整", //15
			"当年<br>有休", //16
			"当年<br>取得",
			"当年<br>残",
			"消化率", //19:印刷には出力しない
			"消化率<br>当年分" //20:印刷には出力しない
			);

	if ($output_flg == "2") { //excel
		$font_size = "2";
		$font_class = "j12";
	} else {
		$font_size = "3";
		$font_class = "j12";
	}

	$data = "";

	if ($output_flg == "1") { //web
		$data .= "<table width=\"1040\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"list\">\n";
	} elseif ($output_flg == "2") { //excel
		$data .= "<table width=\"1040\" border=\"1\" cellspacing=\"0\" cellpadding=\"2\" class=\"list\">\n";
	} else {
		//印刷
		echo("<table width=\"680\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\n");
		echo("<tr height=\"22\">\n");
		$wk_close_date_str = substr($select_close_date, 0, 4)."年".substr($select_close_date, 4, 2)."月".substr($select_close_date, 6, 2)."日";
		echo("<td width=\"\" align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		if ($select_close_date != "---") {
			echo("当年有休残締日:{$wk_close_date_str}");
		}
		echo("</font></td>\n");
		echo("</tr>\n");
		echo("</table>\n");
		$data .= "<table width=\"680\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"list\">\n";
	}
	$data .= "<tr height=\"22\" bgcolor=\"#f6f9ff\">\n";
	$data .= "<td width=\"80\" align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
	if ($output_flg != "1") {
		$data .= "職員ID";
	} else {
		$arrow = ($sort == "1") ? "img/up.gif" : "img/down.gif";
		$data .= "<a href=\"javascript:set_sort('EMP_ID');\"><img src=\"{$arrow}\" alt=\"\" width=\"17\" height=\"17\" border=\"0\" style=\"vertical-align:middle;\">職員ID</a>";
	}
	$data .= "</font></td>\n";
	$data .= "<td width=\"140\" align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
	if ($output_flg != "1") {
		$data .= "職員氏名";
	} else {
		$arrow = ($sort == "3") ? "img/up.gif" : "img/down.gif";
		$data .= "<a href=\"javascript:set_sort('EMP_NAME');\"><img src=\"{$arrow}\" alt=\"\" width=\"17\" height=\"17\" border=\"0\" style=\"vertical-align:middle;\">職員氏名</a>";
	}
	$data .= "</font></td>\n";
	if ($output_flg == "2") { //Excelのみ
		$data .= "<td width=\"80\" align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
		$data .= "職種";
		$data .= "</font></td>\n";
	}
	$data .= "<td width=\"160\" align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
	$arrow = ($sort == "5") ? "img/up.gif" : "img/down.gif";
	if ($output_flg != "1") {
		$data .= "所属";
	} else {
		$data .= "<a href=\"javascript:set_sort('DEPT');\"><img src=\"{$arrow}\" alt=\"\" width=\"17\" height=\"17\" border=\"0\" style=\"vertical-align:middle;\">所属</a>";
	}
	$data .= "</font></td>\n";

	//見出し出力用の配列位置指定
	//印刷
	if ($output_flg == "3") {
		$start_pos = 16; //当年有休
		$end_pos = 18; //当年残
	}
	//印刷以外
	else {
		$start_pos = 4; //採用年月日
		$end_pos = 20; //有休消化率
	}
	for ($i=$start_pos; $i<=$end_pos; $i++) {
		$data .= "<td align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";

		//if ($output_flg == "2") { //もし折り返しを除く場合
		//	$str = str_replace("<br>", "", $arr_title[$i]);
		//} else {
		$str = $arr_title[$i];
		//}
		$data .= $str;
		$data .= "</font></td>\n";
	}
	$data .= "</tr>\n";

	if ($output_flg != "2") {
		echo($data);
		$data = "";
	}

	//配列に設定
	//年休使用数取得のための情報
	$arr_data = array();
	$arr_emp_idx = array();	// emp_idをキーとした配列、位置を格納する
	$i = 0;
	while ($row = pg_fetch_array($sel)) {
		$arr_data[$i] = $row;
		$wk_emp_id = $row["emp_id"];
		$arr_emp_idx["$wk_emp_id"] = $i;
		//付与日 add_dateを求めて配列に設定
		$tmp_emppaid_year = $row["emppaid_year"]; //年
		if ($row["paid_hol_add_mmdd"] != "") {
			$wk_mm = substr($row["paid_hol_add_mmdd"], 0, 2);
			$wk_dd = substr($row["paid_hol_add_mmdd"], 2, 2);
			$wk_add_mmdd = "$tmp_emppaid_year/$wk_mm/$wk_dd"; //年を追加表示 20110208
		} else {
			if ($select_add_date != "") {
				$wk_year = substr($select_add_date, 0, 4);
				$wk_select_date = $select_add_date;
			} else {
				$wk_year = substr($select_close_date, 0, 4);
				$wk_select_date = $select_close_date;
			}
			switch ($closing_paid_holiday) {
				case "1":
					//年度調整
					if ($select_add_date == "") {
						if (substr($select_close_date, 4, 4) < "0401") {
							$wk_year--;
						}
					}
					$wk_add_mmdd = "$wk_year/04/01";
					break;
				case "2":
					$wk_add_mmdd = "$wk_year/01/01";
					break;
				case "3":
					if ($row["emp_join"] > 0 || $row["paid_hol_start_date"] > 0) {
						//6ヶ月基準
						if ($criteria_months == "2" ) {
							//前回付与日がある場合 20110309
							if ($row["prev_paid_hol_add_mmdd"] > 0) {
								$wk_add_mmdd = $wk_year."/".substr($row["prev_paid_hol_add_mmdd"], 0, 2)."/".substr($row["prev_paid_hol_add_mmdd"], 2, 2);

							} else {
								// 付与月日が有休残締め月日より大きい場合、前年とする
								if ($select_add_date == "") {
									if (substr($row["paid_hol_add_date"], 4, 4) > substr($select_close_date, 4, 4)) {
										$wk_year--;
									}
								}
								$wk_add_mmdd = $wk_year."/".substr($row["paid_hol_add_date"], 4, 2)."/".substr($row["paid_hol_add_date"], 6, 2);
							}
						}
						//3ヶ月基準
						else {
							//前回付与日がある場合 20110309
							if ($row["prev_paid_hol_add_mmdd"] > 0) {
								if ($row["prev_service_mon"] != "3") {
									$wk_add_mmdd = $wk_year."/".substr($row["prev_paid_hol_add_mmdd"], 0, 2)."/".substr($row["prev_paid_hol_add_mmdd"], 2, 2);
								} else {

									$wk_add_mm = intval(substr($row["prev_paid_hol_add_mmdd"], 0, 2)) + 9;
									if ($wk_add_mm > 12) {
										$wk_add_mm -= 12;
									}
									$wk_add_mm = sprintf("%02d", $wk_add_mm);
									$wk_dd = substr($row["prev_paid_hol_add_mmdd"], 2, 2);
									/*
									//月を計算した後に、元の日付が月末日付を超えないか確認
									if ($wk_dd > 28) {
										$wk_max_day = days_in_month($wk_year, $wk_add_mm);
										if ($wk_dd > $wk_max_day) {
											$wk_dd = $wk_max_day;
										}
									}
									*/
									$wk_add_mmdd = $wk_year."/".$wk_add_mm."/".$wk_dd;
								}

							} else {
								$today = date("Ymd");
								$year_after_join = $row["year_after_join"];
								//入職後1年以上か確認、画面の付与年月日、有休残締め日の順で確認
								$wk_year_after_join = substr($year_after_join, 0, strlen($wk_select_date)); //付与日の年月対応、長さを合わせる
								if ($wk_year_after_join <= $wk_select_date) {
									// 付与月日が有休残締め月日より大きい場合、前年とする
									if ($select_add_date == "") {
										if (substr($row["year_after_join"], 4, 4) > substr($select_close_date, 4, 4)) {
											$wk_year--;
										}
									}
									$wk_add_mmdd = $wk_year."/".substr($row["year_after_join"], 4, 2)."/".substr($row["year_after_join"], 6, 2);
								} else {
									$wk_add_mmdd = substr($row["paid_hol_add_date"], 0, 4)."/".substr($row["paid_hol_add_date"], 4, 2)."/".substr($row["paid_hol_add_date"], 6, 2);
								}
							}

						}
					} else {
						$wk_add_mmdd = "";
					}
					break;
			}
		}

		$arr_data[$i]["add_date"] = $wk_add_mmdd;
		$i++;
	}

    //直近1年に2件付与データがある場合の対応 20131029
    $arr_prev_days_second = get_prev_days_second($con, $fname, $select_close_date);
    //有休残締め日と有休付与日が未設定の場合、年休使用数は不要
	if ($select_close_date != "---" || $select_add_date != "") {

		$arr_specified_time = array();
		if ($obj_hol_hour->paid_hol_hour_flag == "t") {
			//タイムカード情報の取得
			$timecard_bean = new timecard_bean();
			$timecard_bean->select($con, $fname);

			$emp_ids = array();
			for ($idx=0; $idx<count($arr_data); $idx++) {
				$emp_ids[$idx] = $arr_data[$idx]["emp_id"];
			}
			$day1_time = $obj_hol_hour->get_specified_time_calendar();
				//時間有休用データをまとめて取得
			$arr_specified_time = $obj_hol_hour->get_specified_time_emp($emp_ids, $day1_time);

		}

		//前年の年休使用数
        $arr_siyo_su = get_nenkyu_siyo_su2($con, $fname, $arr_data, $closing_paid_holiday, $year, $criteria_months, 1, "", "", $arr_specified_time, "f", $arr_prev_days_second);

		foreach ($arr_siyo_su as $wk_emp_id => $wk_siyo_su) {
			$wk_idx = $arr_emp_idx["$wk_emp_id"];
			$arr_data[$wk_idx]["siyo_su"] = $wk_siyo_su;
		}

		//当年の年休使用数
		$arr_siyo_su_curr = get_nenkyu_siyo_su2($con, $fname, $arr_data, $closing_paid_holiday, $year, $criteria_months, 2, $select_close_date, $select_add_date, $arr_specified_time, "f", array());

		foreach ($arr_siyo_su_curr as $wk_emp_id => $wk_siyo_su) {
			$wk_idx = $arr_emp_idx["$wk_emp_id"];
			$arr_data[$wk_idx]["siyo_su2"] = $wk_siyo_su;
		}
	}

	$emp_ids = "";
	$i = 0;
	foreach ($arr_data as $row) {
		$tmp_id = $row["emp_id"];
		if ($i > 0) {
			$emp_ids .= ",";
		}
		$emp_ids .= $tmp_id;
		$tmp_personal_id = $row["emp_personal_id"];
		$tmp_name = "{$row["emp_lt_nm"]} {$row["emp_ft_nm"]}";
		$tmp_days1 = $row["days1"];
		$tmp_days2 = $row["days2"];
		$tmp_days3 = $tmp_days1 + $tmp_days2;
		if ($tmp_days3 == 0) {
			$tmp_days3 = "";
		}
		//有休表
		// 前回付与データがある場合は、表名称を引き継ぐ 20110309
		if ($row["paid_hol_add_mmdd"] != "") {
			$paid_hol_tbl_id = $row["reg_tbl_id"];	//当年
		} elseif ($row["prev_tbl_id"] != "") {
			$paid_hol_tbl_id = $row["prev_tbl_id"]; //前年
		} else {
			$paid_hol_tbl_id = $row["paid_hol_tbl_id"]; //勤務条件
		}

        //直近1年に2件付与データがある場合の対応 20131029
        if ($arr_prev_days_second[$tmp_id]["prev_days1"] != "") {
            $tmp_prev_days1 = $arr_prev_days_second[$tmp_id]["prev_days1"];
        }
        else {
            $tmp_prev_days1 = $row["prev_days1"];
        }
        if ($arr_prev_days_second[$tmp_id]["prev_days2"] != "") {
            $tmp_prev_days2 = $arr_prev_days_second[$tmp_id]["prev_days2"];
        }
        else {
            $tmp_prev_days2 = $row["prev_days2"];
        }
        //$tmp_prev_days1 = $row["prev_days1"];
		//$tmp_prev_days2 = $row["prev_days2"];
		$tmp_siyo_su = $row["siyo_su"];
		$tmp_siyo_su2 = $row["siyo_su2"];
		$tmp_adjust_day = $row["adjust_day"];
		$tmp_prev_adjust_day = $row["prev_adjust_day"];
		//採用日等、職員マスタとは別の登録時点の情報
		$tmp_reg_emp_join = $row["reg_emp_join"]; //採用日
		$tmp_reg_start_date = $row["reg_start_date"]; //付与起算日
		$tmp_reg_tbl_id = $row["reg_tbl_id"]; //有休表
		$tmp_paid_hol_add_mmdd = $row["paid_hol_add_mmdd"]; //付与日
		$tmp_service_year = $row["service_year"]; //勤続年
		$tmp_service_mon = $row["service_mon"]; //勤続月
		$tmp_last_remain = $row["last_remain"]; //前年残（有給休暇残日数移行画面で設定）
		$tmp_last_use = $row["last_use"]; //前年使用
		$tmp_last_add_date = $row["last_add_date"]; //前回付与日
		$tmp_last_carry = $row["last_carry"]; //前年繰越
		$tmp_last_add = $row["last_add"]; //前年付与
		$tmp_curr_use = $row["curr_use"]; //当年使用
		$tmp_curr_carry = $row["curr_carry"]; //当年繰越
		$tmp_curr_add = $row["curr_add"]; //当年付与
		$tmp_curr_remain = $row["curr_remain"]; //当年残
		$tmp_data_migration_date = $row["data_migration_date"]; //移行日
		$tmp_emppaid_year = $row["emppaid_year"]; //年
		$tmp_mon3_year = $row["mon3_year"]; //3ヶ月時点データ、前回付与年
		$tmp_mon3_paid_hol_add_mmdd = $row["mon3_paid_hol_add_mmdd"]; //前回付与日
		$tmp_mon3_days1 = $row["mon3_days1"];
		$tmp_mon3_days2 = $row["mon3_days2"];
		$tmp_mon3_adjust_day = $row["mon3_adjust_day"];
		//3ヶ月基準、付与日が入職後1年で、3ヶ月時の付与データがある場合
		if ($closing_paid_holiday == "3" && $criteria_months == "1") {
			$wk_year_after_join = $row["year_after_join"];
			$wk_add_date = str_replace("/", "",$row["add_date"]);
			if ($wk_year_after_join == $wk_add_date &&
					$tmp_mon3_year != "") {
				$tmp_prev_days1 = $tmp_mon3_days1;
				$tmp_prev_days2 = $tmp_mon3_days2;
				$tmp_prev_adjust_day = $tmp_mon3_adjust_day;

			}
		}

		//移行データの前回付与日
		$import_last_add_date = "";
		if ($tmp_last_add_date != "") {
			$wk_ymd = split("/", $tmp_last_add_date);
			$import_last_add_date = $wk_ymd[0].sprintf("%02d%02d", $wk_ymd[1], $wk_ymd[2]);
		}

		$data .= "<tr height=\"22\">\n";
		//職員ID
		$data .= "<td width=\"80\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
		//当年、前年のデータがない場合は、情報を引き継ぐ
		if ($emppaid_year == "" && $prev_year == "") {
			$wk_paid_hol_tbl_id = $paid_hol_tbl_id;
			$wk_paid_hol_add_date = str_replace("/", "", $row["add_date"]);
		} else {
			$wk_paid_hol_tbl_id = "";
			$wk_paid_hol_add_date = "";
		}
		if ($output_flg == "1") {
			$data .= "<a href=\"javascript:void(0);\" onclick=\"openEdit('$tmp_id', '$wk_paid_hol_tbl_id', '$wk_paid_hol_add_date', '$wk_yy', '$wk_mm');\">";
		}
		$data .= "$tmp_personal_id";
		if ($output_flg == "1") {
			$data .= "</a>";
		}
		$data .= "</font></td>\n";
		//職員氏名
		$data .= "<td width=\"140\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">$tmp_name</font>\n";
		if ($output_flg == "1") {
			$data .= "<input type=\"hidden\" name=\"emp_name_{$i}\" value=\"$tmp_name\">";
		}
		$data .= "</td>\n";
		//職種
		if ($output_flg == "2") {
			$data .= "<td width=\"80\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
			$data .= $row["job_nm"];
			$data .= "</font></td>\n";
		}

		//所属
		if ($output_flg == "3") {
			$wk_width = "360";
		} else {
			$wk_width = "160";
		}
		$data .= "<td width=\"$wk_width\" align=\"left\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
		$data .= $row["class_nm"]."＞".$row["atrb_nm"]."＞".$row["dept_nm"];
		$data .= "</font></td>\n";

		//付与日
		$wk_add_mmdd = $arr_data[$i]["add_date"];

		//移行日との比較用に日付設定
		list($wk_add_yyyy, $wk_add_mm, $wk_add_dd) = split("/", $wk_add_mmdd);
		$wk_curr_add_date = $wk_add_yyyy.$wk_add_mm.$wk_add_dd;
		$wk_last_add_date = ($wk_add_yyyy-1).$wk_add_mm.$wk_add_dd; // 1年前まで 20120409
		$wk_next_add_date = ($wk_add_yyyy+1).$wk_add_mm.$wk_add_dd;

		//印刷以外の場合に出力
		if ($output_flg != "3") {

		//採用年月日
		$data .= "<td align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
		$wk_join = "";
		if ($row["emp_join"] > 0) {
			$wk_join = $row["emp_join"];
		}
		if ($wk_join != "") {
			$data .= substr($wk_join,0,4)."/".substr($wk_join,4,2)."/".substr($wk_join,6,2);
			if ($output_flg == "1") {
				$data .= "<input type=\"hidden\" id=\"join_{$i}\" name=\"join_{$i}\" value=\"$wk_join\">\n";
			}

		}
		$data .= "</font></td>\n";

		//有休表
		$data .= "<td align=\"center\" nowrap><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
		//表名称
		//名称変更 常勤 週ｎ日 20101117
		//$wk_paid_hol_tbl_id = ($row["paid_hol_add_mmdd"] != "") ? $row["reg_tbl_id"] : $paid_hol_tbl_id;
		// 前回付与データがある場合は、表名称を引き継ぐ 20110309
		$wk_tbl_id_name = $atdbk_common_class->get_paid_hol_tbl_id_name($paid_hol_tbl_id);
		$data .= $wk_tbl_id_name;
		if ($output_flg == "1") {
			$data .= "<input type=\"hidden\" name=\"tbl_id_{$i}\" id=\"tbl_id_{$i}\" value=\"{$paid_hol_tbl_id}\">";
		}
		$data .= "</font></td>\n";
		$data .= "<td align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";

		//日付指定がない場合
		if ($select_add_date == "" && $select_close_date == "---") {
			$paid_hol_tbl_id = "";
		}
		//有休表がなしの場合は表示しない、当年付与データがなしの条件追加 20110329
		if ($paid_hol_tbl_id != "" && $tmp_emppaid_year != "") {
			$data .= "$wk_add_mmdd";
			//db用編集
			if ($output_flg == "1") {
				list($wk_add_yyyy, $wk_add_mm, $wk_add_dd) = split("/", $wk_add_mmdd);
				$wk_add_mmdd2 = sprintf("%02d%02d", $wk_add_mm, $wk_add_dd);
				echo("<input type=\"hidden\" name=\"add_yyyy_{$i}\" id=\"add_yyyy_{$i}\" value=\"$wk_add_yyyy\">");
				echo("<input type=\"hidden\" name=\"add_mmdd_{$i}\" id=\"add_mmdd_{$i}\" value=\"$wk_add_mmdd2\">");
			}
		}
		else {
			//最新の付与年月日
			$wk_last_add_year = $arr_data[$i]["last_add_year"];
			$wk_last_add_mmdd = $row["last_add_mmdd"];
			if ($wk_last_add_year != "" && $wk_last_add_mmdd != "") {
				$wk_last_add_ymd = $wk_last_add_year."/".substr($wk_last_add_mmdd, 0, 2)."/".substr($wk_last_add_mmdd, 2, 2);
				$data .= $wk_last_add_ymd;
			}
		}
		$data .= "</font></td>\n";
		//勤続年数
		$data .= "<td align=\"center\" nowrap><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
		//起算日優先順、勤務条件起算日、職員情報採用日
		if ($row["paid_hol_start_date"] != "") {
			$wk_emp_join = $row["paid_hol_start_date"];
		} else {
			$wk_emp_join = $row["emp_join"];
		}
		//有休表がなしの場合は表示しない、当年付与データがなしの条件追加 20110329
		if ($paid_hol_tbl_id != "" && $tmp_emppaid_year != "") {
			//勤続年数が登録済の場合
			if ($row["service_year"] != "" || $row["service_mon"] != "") {
				$wk_yy = $row["service_year"];
				$wk_mm = $row["service_mon"];
			} else
				//前回付与日がある場合、勤続年数＝前回の勤続年数＋１ //4/1 1/1の場合、前回が１年以下の場合は１
				//20110310
				if ($row["prev_paid_hol_add_mmdd"] != "") {
					if ($closing_paid_holiday != "3") {
						$wk_yy = $row["prev_service_year"];
						$wk_mm = $row["prev_service_mon"];
						if (($wk_yy == 0) || ($wk_yy == 1 && $wk_mm == 0)) {
							$wk_yy = 1;
							$wk_mm = 0;
						} else {
							$wk_yy++;
							$wk_mm = 0; //月数は表示しない
						}
					} else {
						//前回が3ヶ月は1年とする
						if ($row["prev_service_year"] == 0 && $row["prev_service_mon"] == 3) {
							$wk_yy = 1;
							$wk_mm = 0;
						} else {
							$wk_yy = $row["prev_service_year"] + 1;
							$wk_mm = $row["prev_service_mon"];
						}
					}
				}
				else {
					list($wk_yyyy, $wk_mm, $wk_dd) = split("/", $wk_add_mmdd);
					//付与日が4/1,1/1の場合
					if (($closing_paid_holiday == "1" ||
								$closing_paid_holiday == "2")) {
						$wk_end_date = $wk_yyyy.sprintf("%02d%02d", $wk_mm, $wk_dd);
					} else {
						//年またがり確認、3ヶ月や6ヶ月の時、表示されない場合があるのを回避
						$wk_mmdd = sprintf("%02d%02d", $wk_mm, $wk_dd);
						$wk_end_date = $wk_yyyy.$wk_mmdd;
					}
					$len_service = get_length_of_service($wk_emp_join, $wk_end_date);
					list($wk_yy, $wk_mm) = split("/", $len_service);
				}

			$str = "";
			if ($wk_yy > 0) {
				$str .= $wk_yy."年";
				if ($output_flg != "2") {
					$str .= "<br>";
				}
			}
			if ($wk_mm > 0) {
				$str .= $wk_mm."ヶ月";
			}
			$data .= "$str\n";
			if ($output_flg == "1") {
				$data .= "<input type=\"hidden\" id=\"len_service_{$i}\" name=\"len_service_{$i}\" value=\"$len_service\">\n";
			}
		}
		$data .= "</font></td>\n";
		$data .= "<td align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
		//有休表がなしの場合は表示しない、当年付与データがなしの条件追加 20110329
		if ($paid_hol_tbl_id != "" && $tmp_emppaid_year != "") {
			//前年繰越
			//移行データがある場合
			if ($tmp_last_carry != "" &&
					$tmp_prev_days1 == "" &&
					$wk_curr_add_date <= $import_last_add_date &&
					$import_last_add_date < $wk_next_add_date) {
				$tmp_prev_days1 = $tmp_last_carry;
			}
			//前年に移行データがある場合、移行データの当年取得を設定
			elseif ($tmp_curr_carry != "" &&
					$tmp_prev_days1 == "" &&
					$wk_last_add_date <= $import_last_add_date &&
					$import_last_add_date < $wk_curr_add_date) {
				$tmp_prev_days1 = $tmp_curr_carry;
			}
			$data .= "$tmp_prev_days1\n";
		}
		$data .= "</font></td>\n";
		$data .= "<td align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
		//有休表がなしの場合は表示しない、当年付与データがなしの条件追加 20110329
		if ($paid_hol_tbl_id != "" && $tmp_emppaid_year != "") {
			//前年付与
			//移行データがある場合
			if ($tmp_last_add != "" &&
					$tmp_prev_days2 == "" &&
					$wk_curr_add_date <= $import_last_add_date &&
					$import_last_add_date < $wk_next_add_date) {
				$tmp_prev_days2 = $tmp_last_add;
			}
			//前年に移行データがある場合、移行データの当年付与を設定
			elseif ($tmp_curr_add != "" &&
					$tmp_prev_days2 == "" &&
					$wk_last_add_date <= $import_last_add_date &&
					$import_last_add_date < $wk_curr_add_date) {
				$tmp_prev_days2 = $tmp_curr_add;
			}
			$data .= "$tmp_prev_days2\n";
		}
		$data .= "</font></td>\n";
		//前年調整
        $data .= "<td align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
        if ($paid_hol_tbl_id != "" && $tmp_emppaid_year != "") {
            $data .= "$tmp_prev_adjust_day\n";
        }
        $data .= "</font></td>\n";
        $data .= "<td align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
		//有休表がなしの場合は表示しない、当年付与データがなしの条件追加 20110329
		if ($paid_hol_tbl_id != "" && $tmp_emppaid_year != "") {
			//前年取得
			//移行データがある場合
			if ($tmp_last_use != "" &&
					$wk_curr_add_date <= $import_last_add_date &&
					$import_last_add_date < $wk_next_add_date) {
				$tmp_siyo_su = $tmp_last_use;
			}
			//前年に移行データがある場合、移行データの当年取得を設定
			elseif ($tmp_curr_use != "" &&
					$wk_last_add_date <= $import_last_add_date &&
					$import_last_add_date < $wk_curr_add_date) {
				$tmp_siyo_su += $tmp_curr_use;
			}
			$data .= "$tmp_siyo_su\n";
		}
		$data .= "</font></td>\n";
		//残日数
		$wk_zan_su = $tmp_prev_days1 + $tmp_prev_days2 + $tmp_prev_adjust_day - $tmp_siyo_su;
		$data .= "<td align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
		//有休表がなしの場合は表示しない、当年付与データがなしの条件追加 20110329
		if ($paid_hol_tbl_id != "" && $tmp_emppaid_year != "") {
			$data .= "$wk_zan_su\n";
		}
		$data .= "</font></td>\n";
		//移行データがある場合
		if ($tmp_curr_carry != "" &&
				$tmp_days1 == "" &&
				$wk_curr_add_date <= $import_last_add_date &&
				$import_last_add_date < $wk_next_add_date) {
			$tmp_days1 = $tmp_curr_carry;
		} else {
			//繰越日数、未設定の場合か、前年に移行データがある場合
			if ($tmp_days1 == "" ||
					($tmp_curr_add != "" &&
						$tmp_days1 == "" &&
						$wk_last_add_date <= $import_last_add_date &&
						$import_last_add_date < $wk_curr_add_date)) {
				//前年残<=前年付与の場合、前年残 20110202
				if ($wk_zan_su <= $tmp_prev_days2) {
					$tmp_days1 = $wk_zan_su;
				}
				//上記以外は前年付与（前々年分を除く）
				else {
					$tmp_days1 = $tmp_prev_days2;
				}

			}
		}
		//マイナスの場合は、ゼロとする
		if ($tmp_days1 < 0) {
			$tmp_days1 = 0;
		}
		//有休表がなしの場合は表示しない、当年付与データがなしの条件追加 20110329
		if ($paid_hol_tbl_id == "" ||  $tmp_emppaid_year == "") {
			$tmp_days1 = "";
		}
		$data .= "<td align=\"center\">";
		$data .= "<font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">$tmp_days1</font>";
		$data .= "</td>\n";

		//勤続年数表の位置を取得
		$wk_kinzoku_id = 0;
		//20110310 前回付与日がある場合、上で計算した勤続年を使用する
		if ($row["prev_paid_hol_add_mmdd"] != "") {
			$wk_kinzoku_id = $wk_yy + 1; //配列が1始まりのため
			if ($wk_kinzoku_id > 8) {
				$wk_kinzoku_id = 8;
			}
		}
		else {
			if ($wk_add_mmdd != "" && $wk_emp_join > 0) {
				list($wk_add_yyyy, $wk_add_mm, $wk_add_dd) = split("/", $wk_add_mmdd);
				$wk_add_date = $wk_add_yyyy.sprintf("%02d%02d", $wk_add_mm, $wk_add_dd);

				//付与日が"3":採用日の場合
				if ($closing_paid_holiday == "3") {
					//SQLで取得した6ヶ月後の日が違う場合は、計算が合わなくなるため調整する。8/31が2/28,5/31が11/30等
					if ($criteria_months == "2" || ($criteria_months == "1" && substr($row["year_after_join"], 0, 4) > $wk_add_yyyy)) {
						if (substr($row["paid_hol_add_date"], 6, 2) != substr($wk_emp_join, 6, 2)) {
							$wk_emp_join = substr($wk_emp_join, 0, 6).substr($row["paid_hol_add_date"], 6, 2);

						}
					}
					$wk_join_timestamp = date_utils::to_timestamp_from_ymd($wk_emp_join);
					//6ヶ月基準
					if ($criteria_months == "2") {
                            $arr_kikan = array("+7 year $wk_month month", "+6 year $wk_month month", "+5 year $wk_month month", "+4 year $wk_month month", "+3 year $wk_month month", "+2 year $wk_month month", "+1 year $wk_month month", "+$wk_month month");
					}
					//3ヶ月基準
					else {
                            $arr_kikan = array("+7 year", "+6 year", "+5 year", "+4 year", "+3 year", "+2 year", "+1 year", "+$wk_month month");
					}
					//$tmp_days2 = "";
					//7年後から6ヶ月まで勤続の期間を確認
					for ($wk_idx=0; $wk_idx<8; $wk_idx++) {

						$wk_kikan = date("Ymd", strtotime($arr_kikan[$wk_idx], $wk_join_timestamp));
						if ($wk_add_date >= $wk_kikan) {
							$wk_kinzoku_id = 8 - $wk_idx;
							break;
						}
					}
					if ($wk_kinzoku_id <= 0) {
						$wk_kinzoku_id = 1;
					}
				}
				//付与日が"1":4/1、"2":1/1の場合
				else {
					//採用後、最初の付与日の年を基準に勤続表の位置を取得
					$wk_base_year = substr($wk_emp_join, 0, 4);
					$wk_base_add_date = $wk_base_year.sprintf("%02d%02d", $wk_add_mm, $wk_add_dd);
					//採用日と最初の付与日が違う場合、基準年を1プラスする
					if ($wk_emp_join != $wk_base_add_date) {
						$wk_base_year++;
					}
					$wk_kinzoku_id = $wk_yyyy - $wk_base_year + 1;
					if ($wk_kinzoku_id < 0) {
						$wk_kinzoku_id = 0;
					}
					if ($wk_kinzoku_id > 8) {
						$wk_kinzoku_id = 8;
					}
				}
			}
		}
		//付与日数
		//移行データがある場合
		if ($tmp_curr_add != "" &&
				$tmp_days2 == "" &&
				$wk_curr_add_date <= $import_last_add_date &&
				$import_last_add_date < $wk_next_add_date) {
			$tmp_days2 = $tmp_curr_add;
			//$tmp_adjust_day = "0"; //調整日数の設定不要 20110307

            //当年付与データがある場合でも、当年付与の数値が未設定はそのままとする 20120918
        //} else {
            // //有休表番号と勤続年数をもとに有休付与表から取得
            //if ($tmp_days2 == "") {
			//	if ($wk_kinzoku_id > 0) {
			//		$tmp_days2 = $arr_paid_hol[$paid_hol_tbl_id][$wk_kinzoku_id];
			//	} else {
			//		$tmp_days2 = "0";
			//	}
			//}
		}
		//有休表がなしの場合は表示しない、当年付与データがなしの条件追加 20110329
		if ($paid_hol_tbl_id == "" ||  $tmp_emppaid_year == "") {
			$tmp_days2 = "";
		}
		//付与日数
		$data .= "<td align=\"center\">";

		$data .= "<font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">$tmp_days2</font>";
		$data .= "</td>\n";
		//有休表がなしの場合は表示しない、当年付与データがなしの条件追加 20110329
		if ($paid_hol_tbl_id == "" ||  $tmp_emppaid_year == "") {
			$tmp_adjust_day = "";
		}
		//調整日数
		$data .= "<td align=\"center\">";
		$data .= "<font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">$tmp_adjust_day</font>";

		$data .= "</td>\n";

		}
		//有休日数
		$tmp_days3 = $tmp_days1 + $tmp_days2 + $tmp_adjust_day;
		//有休表がなしの場合は表示しない、当年付与データがなしの条件追加 20110329
		if ($paid_hol_tbl_id == "" ||  $tmp_emppaid_year == "") {
			$tmp_days3 = "";
		}
		//当年有休
		if ($output_flg == "3") {
			$wk_width = "32";
		} else {
			$wk_width = "";
		}
		$data .= "<td width=\"$wk_width\" align=\"center\">";
		$data .= "<font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">$tmp_days3</font>";
		$data .= "</td>\n";
		//当年使用日数
		$data .= "<td width=\"$wk_width\" align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
		//有休表がなしの場合は表示しない、当年付与データがなしの条件追加 20110329
		if ($paid_hol_tbl_id != "" && $tmp_emppaid_year != "") {
			if ($tmp_curr_use != "" &&
					$wk_curr_add_date <= $import_last_add_date &&
					$import_last_add_date < $wk_next_add_date) {
				$tmp_siyo_su2 += $tmp_curr_use;
				$data .= "$tmp_siyo_su2\n";
			} else {
				$data .= "$tmp_siyo_su2\n";
			}
		} else {
			$tmp_siyo_su2 = "0";
		}
		if ($output_flg == "1") {
			$data .= "<input type=\"hidden\" id=\"curr_use_{$i}\" name=\"curr_use_{$i}\" value=\"$tmp_siyo_su2\">\n";
		}
		$data .= "</font></td>\n";
		//当年残日数
		$data .= "<td width=\"$wk_width\" align=\"center\">";
		//有休表がなしの場合は表示しない、当年付与データがなしの条件追加 20110329
		if ($paid_hol_tbl_id != "" && $tmp_emppaid_year != "") {
			$wk_curr_zan_su = $tmp_days3 - $tmp_siyo_su2;
		} else {
			$wk_curr_zan_su = "";
		}
		$data .= "<font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">$wk_curr_zan_su</font>";
		$data .= "</td>\n";
		//有休消化率
		if ($output_flg != "3") { //印刷以外
			$data .= "<td align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
			if ($paid_hol_tbl_id != "" && $tmp_emppaid_year != "") {
				$tmp_ritsu = $tmp_siyo_su2 / $tmp_days3 * 100;
				$tmp_ritsu = sprintf("%.1f", $tmp_ritsu);
				$data .= $tmp_ritsu;
			}
			$data .= "</font></td>\n";
			//当年分
			$data .= "<td align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
			if ($paid_hol_tbl_id != "" && $tmp_emppaid_year != "") {
				$tmp_ritsu = $tmp_siyo_su2 / $tmp_days2 * 100;
				$tmp_ritsu = sprintf("%.1f", $tmp_ritsu);
				$data .= $tmp_ritsu;
			}
			$data .= "</font></td>\n";
		}
		$data .= "</tr>\n";
		$i++;

		if ($output_flg != "2") {
			echo($data);
			$data = "";
		}

	}

	$data .= "</table>\n";

	if ($output_flg != "2") { //excel以外
		$data .= "<input type=\"hidden\" name=\"data_cnt\" value=\"$i\">\n";
		$data .= "<input type=\"hidden\" name=\"emp_ids\" value=\"$emp_ids\">\n";
		echo($data);
		$data = "";
	}
	return $data;
}

/**
 * 職員一覧を表示（時間有休）
 *
 * @param object $con DBコネクション
 * @param string $fname 画面名
 * @param object $sel SQL結果
 * @param string $closing_paid_holiday 有休休暇の付与日
 * @param array $arr_paid_hol 有休付与日数表の配列
 * @param string $year 処理年度
 * @param string $criteria_months 基準月数 1:3ヶ月 2:6ヶ月
 * @param object $atdbk_common_class 出勤表関連共通クラス
 * @param string $select_close_date 当年有休残締日
 * @param string $sort ソート順
 * @param string $select_add_date 付与年月日
 * @param string $output_flg 1:web 2:excel 3:印刷
 * @param object $obj_hol_hour 時間有休クラス
 * @return 有休数一覧の文字列データ、2:excel時データを返し、それ以外の1,3は、echo表示しデータを返さない。
 *
 */
function get_emp_list_data2($con, $fname, $sel, $closing_paid_holiday, $arr_paid_hol, $year, $criteria_months, $atdbk_common_class, $select_close_date, $sort, $select_add_date, $output_flg, $obj_hol_hour) {
	if (!$sel) {
		return "";
	}
    //カレンダー名、所定労働時間履歴用
    $calendar_name_class = new calendar_name_class($con, $fname);

	//基準月数
	$wk_month = ($criteria_months == "1") ? "3" : "6";

	//一覧見出しの配列
	$arr_title = array(
			"職員ID", //0
			"職員氏名",
			"職種", //2:excelのみに出力
			"所属", //3
            "採用年月日", //4:採用年月日から22:時間計までを印刷には出力しない
			"有休表", //5
			"付与年月日",
			"勤続<br>年数",
            "日数",
            "時間",
            "前年<br>付与", //10
            "前年<br>調整", //11
            "日数",
            "時間",
            "日数",
            "時間", //15
            "日数",
            "時間",
            "当年<br>付与",
            "当年<br>調整",
            "上限日数", //20
            "単位時間",
            "時間計",
            "日数",
            "時間",
            "日数", //25
            "時間",
            "日数",
            "時間",
            "消化率", //29:印刷には出力しない
            "消化率<br>当年分", //30:印刷には出力しない
			);

	if ($output_flg == "2") { //excel
		$font_size = "2";
		$font_class = "j12";
	} else {
		$font_size = "3";
		$font_class = "j12";
	}

	$data = "";

	if ($output_flg == "1") { //web 1200
		$data .= "<table width=\"\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"list\">\n";
	} elseif ($output_flg == "2") { //excel
		$data .= "<table width=\"1040\" border=\"1\" cellspacing=\"0\" cellpadding=\"2\" class=\"list\">\n";
	} else {
		//印刷
		echo("<table width=\"700\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\n");
		echo("<tr height=\"22\">\n");
		$wk_close_date_str = substr($select_close_date, 0, 4)."年".substr($select_close_date, 4, 2)."月".substr($select_close_date, 6, 2)."日";
		echo("<td width=\"\" align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		if ($select_close_date != "---") {
			echo("当年有休残締日:{$wk_close_date_str}");
		}
		echo("</font></td>\n");
		echo("</tr>\n");
		echo("</table>\n");
		$data .= "<table width=\"700\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"list\">\n";
	}
    $data .= "";
    //1段目
    $data .= "<tr height=\"22\" bgcolor=\"#f6f9ff\">\n";
    switch($output_flg) {
        case "1":
            $wk_colspan = "7";
            break;
        case "2":
            $wk_colspan = "8";
            break;
        case "3":
            $wk_colspan = "3";
            break;
    }
    $data .= "<td colspan=\"{$wk_colspan}\"></td>\n";
    //印刷以外の場合に出力
    if ($output_flg != "3") {
        $data .= "<td colspan=\"2\" align=\"left\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
    $data .= "前年繰越";
    $data .= "</font></td>\n";
    $data .= "<td colspan=\"1\"></td>\n";
    $data .= "<td colspan=\"1\"></td>\n";
    $data .= "<td colspan=\"2\" align=\"left\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
    $data .= "前年取得";
    $data .= "</font></td>\n";
    $data .= "<td colspan=\"2\" align=\"left\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
    $data .= "前年残";
    $data .= "</font></td>\n";
    $data .= "<td colspan=\"2\" align=\"left\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
    $data .= "当年繰越";
    $data .= "</font></td>\n";
    $data .= "<td colspan=\"1\"></td>\n";
        $data .= "<td colspan=\"1\"></td>\n";
        $data .= "<td colspan=\"3\" align=\"left\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
    $data .= "時間有休";
    $data .= "</font></td>\n";
    }
    $data .= "<td colspan=\"2\" align=\"left\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
    $data .= "当年有休";
    $data .= "</font></td>\n";
    $data .= "<td colspan=\"2\" align=\"left\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
    $data .= "当年取得";
    $data .= "</font></td>\n";
    $data .= "<td colspan=\"2\" align=\"left\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
    $data .= "当年残";
    $data .= "</font></td>\n";
	//印刷以外の場合に出力
    if ($output_flg != "3") {
        $data .= "<td colspan=\"1\"></td>\n";
        $data .= "<td colspan=\"1\"></td>\n";
    }
    $data .= "</tr>\n";

	$data .= "<tr height=\"22\" bgcolor=\"#f6f9ff\">\n";
	$data .= "<td width=\"80\" align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
	if ($output_flg != "1") {
		$data .= "職員ID";
	} else {
		$arrow = ($sort == "1") ? "img/up.gif" : "img/down.gif";
		$data .= "<a href=\"javascript:set_sort('EMP_ID');\"><img src=\"{$arrow}\" alt=\"\" width=\"17\" height=\"17\" border=\"0\" style=\"vertical-align:middle;\">職員ID</a>";
	}
	$data .= "</font></td>\n";
	$data .= "<td width=\"140\" align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
	if ($output_flg != "1") {
		$data .= "職員氏名";
	} else {
		$arrow = ($sort == "3") ? "img/up.gif" : "img/down.gif";
		$data .= "<a href=\"javascript:set_sort('EMP_NAME');\"><img src=\"{$arrow}\" alt=\"\" width=\"17\" height=\"17\" border=\"0\" style=\"vertical-align:middle;\">職員氏名</a>";
	}
	$data .= "</font></td>\n";
	if ($output_flg == "2") { //Excelのみ
		$data .= "<td width=\"80\" align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
		$data .= "職種";
		$data .= "</font></td>\n";
	}
	$data .= "<td width=\"160\" align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
	$arrow = ($sort == "5") ? "img/up.gif" : "img/down.gif";
	if ($output_flg != "1") {
		$data .= "所属";
	} else {
		$data .= "<a href=\"javascript:set_sort('DEPT');\"><img src=\"{$arrow}\" alt=\"\" width=\"17\" height=\"17\" border=\"0\" style=\"vertical-align:middle;\">所属</a>";
	}
	$data .= "</font></td>\n";

	//見出し出力用の配列位置指定
	//印刷
	if ($output_flg == "3") {
        $start_pos = 23; //当年有休
		$end_pos = 28; //当年残
	}
	//印刷以外
	else {
		$start_pos = 4; //採用年月日
        $end_pos = 30; //有休消化率
	}
	for ($i=$start_pos; $i<=$end_pos; $i++) {
		$data .= "<td align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
		$str = $arr_title[$i];
		$data .= $str;
		$data .= "</font></td>\n";
	}
	$data .= "</tr>\n";

	if ($output_flg != "2") {
		echo($data);
		$data = "";
	}

	//配列に設定
	//年休使用数取得のための情報
	$arr_data = array();
	$arr_emp_idx = array();	// emp_idをキーとした配列、位置を格納する
	$i = 0;
	while ($row = pg_fetch_array($sel)) {
		$arr_data[$i] = $row;
		$wk_emp_id = $row["emp_id"];
		$arr_emp_idx["$wk_emp_id"] = $i;
		//付与日 add_dateを求めて配列に設定
		$tmp_emppaid_year = $row["emppaid_year"]; //年
		if ($row["paid_hol_add_mmdd"] != "") {
			$wk_mm = substr($row["paid_hol_add_mmdd"], 0, 2);
			$wk_dd = substr($row["paid_hol_add_mmdd"], 2, 2);
			$wk_add_mmdd = "$tmp_emppaid_year/$wk_mm/$wk_dd"; //年を追加表示 20110208
		} else {
			if ($select_add_date != "") {
				$wk_year = substr($select_add_date, 0, 4);
				$wk_select_date = $select_add_date;
			} else {
				$wk_year = substr($select_close_date, 0, 4);
				$wk_select_date = $select_close_date;
			}
			switch ($closing_paid_holiday) {
				case "1":
					//年度調整
					if ($select_add_date == "") {
						if (substr($select_close_date, 4, 4) < "0401") {
							$wk_year--;
						}
					}
					$wk_add_mmdd = "$wk_year/04/01";
					break;
				case "2":
					$wk_add_mmdd = "$wk_year/01/01";
					break;
				case "3":
					if ($row["emp_join"] > 0 || $row["paid_hol_start_date"] > 0) {
						//6ヶ月基準
						if ($criteria_months == "2" ) {
							//前回付与日がある場合 20110309
							if ($row["prev_paid_hol_add_mmdd"] > 0) {
								$wk_add_mmdd = $wk_year."/".substr($row["prev_paid_hol_add_mmdd"], 0, 2)."/".substr($row["prev_paid_hol_add_mmdd"], 2, 2);

							} else {
								// 付与月日が有休残締め月日より大きい場合、前年とする
								if ($select_add_date == "") {
									if (substr($row["paid_hol_add_date"], 4, 4) > substr($select_close_date, 4, 4)) {
										$wk_year--;
									}
								}
								$wk_add_mmdd = $wk_year."/".substr($row["paid_hol_add_date"], 4, 2)."/".substr($row["paid_hol_add_date"], 6, 2);
							}
						}
						//3ヶ月基準
						else {
							//前回付与日がある場合 20110309
							if ($row["prev_paid_hol_add_mmdd"] > 0) {
								if ($row["prev_service_mon"] != "3") {
									$wk_add_mmdd = $wk_year."/".substr($row["prev_paid_hol_add_mmdd"], 0, 2)."/".substr($row["prev_paid_hol_add_mmdd"], 2, 2);
								} else {

									$wk_add_mm = intval(substr($row["prev_paid_hol_add_mmdd"], 0, 2)) + 9;
									if ($wk_add_mm > 12) {
										$wk_add_mm -= 12;
									}
									$wk_add_mm = sprintf("%02d", $wk_add_mm);
									$wk_dd = substr($row["prev_paid_hol_add_mmdd"], 2, 2);
									/*
									//月を計算した後に、元の日付が月末日付を超えないか確認
									if ($wk_dd > 28) {
										$wk_max_day = days_in_month($wk_year, $wk_add_mm);
										if ($wk_dd > $wk_max_day) {
											$wk_dd = $wk_max_day;
										}
									}
									*/
									$wk_add_mmdd = $wk_year."/".$wk_add_mm."/".$wk_dd;
								}

							} else {
								$today = date("Ymd");
								$year_after_join = $row["year_after_join"];
								//入職後1年以上か確認、画面の付与年月日、有休残締め日の順で確認
								$wk_year_after_join = substr($year_after_join, 0, strlen($wk_select_date)); //付与日の年月対応、長さを合わせる
								if ($wk_year_after_join <= $wk_select_date) {
									// 付与月日が有休残締め月日より大きい場合、前年とする
									if ($select_add_date == "") {
										if (substr($row["year_after_join"], 4, 4) > substr($select_close_date, 4, 4)) {
											$wk_year--;
										}
									}
									$wk_add_mmdd = $wk_year."/".substr($row["year_after_join"], 4, 2)."/".substr($row["year_after_join"], 6, 2);
								} else {
									$wk_add_mmdd = substr($row["paid_hol_add_date"], 0, 4)."/".substr($row["paid_hol_add_date"], 4, 2)."/".substr($row["paid_hol_add_date"], 6, 2);
								}
							}

						}
					} else {
						$wk_add_mmdd = "";
					}
					break;
			}
		}

		$arr_data[$i]["add_date"] = $wk_add_mmdd;
		$i++;
	}

    //直近1年に2件付与データがある場合の対応 20131029
    $arr_prev_days_second = get_prev_days_second($con, $fname, $select_close_date);
    //有休残締め日と有休付与日が未設定の場合、年休使用数は不要
	if ($select_close_date != "---" || $select_add_date != "") {

		$arr_specified_time = array();
		if ($obj_hol_hour->paid_hol_hour_flag == "t") {
			//タイムカード情報の取得
			$timecard_bean = new timecard_bean();
			$timecard_bean->select($con, $fname);

			$emp_ids = array();
			for ($idx=0; $idx<count($arr_data); $idx++) {
				$emp_ids[$idx] = $arr_data[$idx]["emp_id"];
			}
			$day1_time = $obj_hol_hour->get_specified_time_calendar();
				//時間有休用データをまとめて取得
			$arr_specified_time = $obj_hol_hour->get_specified_time_emp($emp_ids, $day1_time);

		}

		//前年の年休使用数
        $arr_siyo_su = get_nenkyu_siyo_su2($con, $fname, $arr_data, $closing_paid_holiday, $year, $criteria_months, 1, "", "", $arr_specified_time, $obj_hol_hour->paid_hol_hour_flag, $arr_prev_days_second);

		foreach ($arr_siyo_su as $wk_emp_id => $wk_siyo_su) {
            if (substr($wk_emp_id, 0, 5) == "hour_") {
                $wk_key = substr($wk_emp_id, 5);
            }
            else {
                $wk_key = $wk_emp_id;
            }
            $wk_idx = $arr_emp_idx["$wk_key"];
            if (substr($wk_emp_id, 0, 5) == "hour_") {
                $arr_data[$wk_idx]["siyo_su_minute"] = $wk_siyo_su;
            }
            else {
                $arr_data[$wk_idx]["siyo_su"] = $wk_siyo_su;
            }
		}

		//当年の年休使用数
        $arr_siyo_su_curr = get_nenkyu_siyo_su2($con, $fname, $arr_data, $closing_paid_holiday, $year, $criteria_months, 2, $select_close_date, $select_add_date, $arr_specified_time, $obj_hol_hour->paid_hol_hour_flag, array());

		foreach ($arr_siyo_su_curr as $wk_emp_id => $wk_siyo_su) {
			//$wk_idx = $arr_emp_idx["$wk_emp_id"];
            if (substr($wk_emp_id, 0, 5) == "hour_") {
                $wk_key = substr($wk_emp_id, 5);
            }
            else {
                $wk_key = $wk_emp_id;
            }
            $wk_idx = $arr_emp_idx["$wk_key"];
            //$arr_data[$wk_idx]["siyo_su2"] = $wk_siyo_su;
            if (substr($wk_emp_id, 0, 5) == "hour_") {
                $arr_data[$wk_idx]["siyo_su2_minute"] = $wk_siyo_su;
            }
            else {
                $arr_data[$wk_idx]["siyo_su2"] = $wk_siyo_su;
            }
        }
	}

	$emp_ids = "";
	$i = 0;
	foreach ($arr_data as $row) {
		$tmp_id = $row["emp_id"];
		if ($i > 0) {
			$emp_ids .= ",";
		}
		$emp_ids .= $tmp_id;
		$tmp_personal_id = $row["emp_personal_id"];
		$tmp_name = "{$row["emp_lt_nm"]} {$row["emp_ft_nm"]}";
		$tmp_days1 = $row["days1"];
		$tmp_days2 = $row["days2"];
		$tmp_days3 = $tmp_days1 + $tmp_days2;
		if ($tmp_days3 == 0) {
			$tmp_days3 = "";
		}
		//有休表
		// 前回付与データがある場合は、表名称を引き継ぐ 20110309
		if ($row["paid_hol_add_mmdd"] != "") {
			$paid_hol_tbl_id = $row["reg_tbl_id"];	//当年
		} elseif ($row["prev_tbl_id"] != "") {
			$paid_hol_tbl_id = $row["prev_tbl_id"]; //前年
		} else {
			$paid_hol_tbl_id = $row["paid_hol_tbl_id"]; //勤務条件
		}

        $tmp_duty_form = $row["duty_form"];
        //直近1年に2件付与データがある場合の対応 20131029
        if ($arr_prev_days_second[$tmp_id]["prev_days1"] != "") {
            $tmp_prev_days1 = $arr_prev_days_second[$tmp_id]["prev_days1"];
        }
        else {
            $tmp_prev_days1 = $row["prev_days1"];
        }
        if ($arr_prev_days_second[$tmp_id]["prev_days2"] != "") {
            $tmp_prev_days2 = $arr_prev_days_second[$tmp_id]["prev_days2"];
        }
        else {
            $tmp_prev_days2 = $row["prev_days2"];
        }
		$tmp_siyo_su = $row["siyo_su"];
		$tmp_siyo_su2 = $row["siyo_su2"];
		$tmp_adjust_day = $row["adjust_day"];
		$tmp_prev_adjust_day = $row["prev_adjust_day"];
		//採用日等、職員マスタとは別の登録時点の情報
		$tmp_reg_emp_join = $row["reg_emp_join"]; //採用日
		$tmp_reg_start_date = $row["reg_start_date"]; //付与起算日
		$tmp_reg_tbl_id = $row["reg_tbl_id"]; //有休表
		$tmp_paid_hol_add_mmdd = $row["paid_hol_add_mmdd"]; //付与日
		$tmp_service_year = $row["service_year"]; //勤続年
		$tmp_service_mon = $row["service_mon"]; //勤続月
		$tmp_last_remain = $row["last_remain"]; //前年残（有給休暇残日数移行画面で設定）
		$tmp_last_use = $row["last_use"]; //前年使用
		$tmp_last_add_date = $row["last_add_date"]; //前回付与日
		$tmp_last_carry = $row["last_carry"]; //前年繰越
		$tmp_last_add = $row["last_add"]; //前年付与
		$tmp_curr_use = $row["curr_use"]; //当年使用
		$tmp_curr_carry = $row["curr_carry"]; //当年繰越
		$tmp_curr_add = $row["curr_add"]; //当年付与
		$tmp_curr_remain = $row["curr_remain"]; //当年残
		$tmp_data_migration_date = $row["data_migration_date"]; //移行日
		$tmp_emppaid_year = $row["emppaid_year"]; //年
		$tmp_mon3_year = $row["mon3_year"]; //3ヶ月時点データ、前回付与年
		$tmp_mon3_paid_hol_add_mmdd = $row["mon3_paid_hol_add_mmdd"]; //前回付与日
		$tmp_mon3_days1 = $row["mon3_days1"];
		$tmp_mon3_days2 = $row["mon3_days2"];
		$tmp_mon3_adjust_day = $row["mon3_adjust_day"];
		//3ヶ月基準、付与日が入職後1年で、3ヶ月時の付与データがある場合
		if ($closing_paid_holiday == "3" && $criteria_months == "1") {
			$wk_year_after_join = $row["year_after_join"];
			$wk_add_date = str_replace("/", "",$row["add_date"]);
			if ($wk_year_after_join == $wk_add_date &&
					$tmp_mon3_year != "") {
				$tmp_prev_days1 = $tmp_mon3_days1;
				$tmp_prev_days2 = $tmp_mon3_days2;
				$tmp_prev_adjust_day = $tmp_mon3_adjust_day;

			}
		}

		//移行データの前回付与日
		$import_last_add_date = "";
		if ($tmp_last_add_date != "") {
			$wk_ymd = split("/", $tmp_last_add_date);
			$import_last_add_date = $wk_ymd[0].sprintf("%02d%02d", $wk_ymd[1], $wk_ymd[2]);
		}
        //時間有休
        $tmp_prev_carry_time_minute = $row["prev_carry_time_minute"];
        $tmp_curr_carry_time_minute = $row["curr_carry_time_minute"];
        $tmp_hol_hour_last_carry = $row["hol_hour_last_carry"];
        $tmp_hol_hour_last_use = $row["hol_hour_last_use"];
        $tmp_hol_hour_curr_carry = $row["hol_hour_curr_carry"];
        $tmp_hol_hour_curr_use = $row["hol_hour_curr_use"];
        $tmp_hol_hour_data_migration_date = $row["hol_hour_data_migration_date"];

        $tmp_siyo_su_hour = $row["siyo_su_minute"] / 60;
        $tmp_siyo_su2_hour = $row["siyo_su2_minute"] / 60;

        $wk_specified_time = $arr_specified_time["$tmp_id"] / 60; //所定時間

		$data .= "<tr height=\"22\">\n";
		//職員ID
		$data .= "<td width=\"80\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
		//当年、前年のデータがない場合は、情報を引き継ぐ
		if ($emppaid_year == "" && $prev_year == "") {
			$wk_paid_hol_tbl_id = $paid_hol_tbl_id;
			$wk_paid_hol_add_date = str_replace("/", "", $row["add_date"]);
		} else {
			$wk_paid_hol_tbl_id = "";
			$wk_paid_hol_add_date = "";
		}
		if ($output_flg == "1") {
			$data .= "<a href=\"javascript:void(0);\" onclick=\"openEdit('$tmp_id', '$wk_paid_hol_tbl_id', '$wk_paid_hol_add_date', '$wk_yy', '$wk_mm');\">";
		}
		$data .= "$tmp_personal_id";
		if ($output_flg == "1") {
			$data .= "</a>";
		}
		$data .= "</font></td>\n";
		//職員氏名
		$data .= "<td width=\"140\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">$tmp_name</font>\n";
		if ($output_flg == "1") {
			$data .= "<input type=\"hidden\" name=\"emp_name_{$i}\" value=\"$tmp_name\">";
		}
		$data .= "</td>\n";
		//職種
		if ($output_flg == "2") {
			$data .= "<td width=\"80\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
			$data .= $row["job_nm"];
			$data .= "</font></td>\n";
		}

		//所属
		if ($output_flg == "3") {
			$wk_width = "280"; //old 360
		} else {
			$wk_width = "160";
		}
		$data .= "<td width=\"$wk_width\" align=\"left\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
		$data .= $row["class_nm"]."＞".$row["atrb_nm"]."＞".$row["dept_nm"];
		$data .= "</font></td>\n";

		//付与日
		$wk_add_mmdd = $arr_data[$i]["add_date"];

		//移行日との比較用に日付設定
		list($wk_add_yyyy, $wk_add_mm, $wk_add_dd) = split("/", $wk_add_mmdd);
		$wk_curr_add_date = $wk_add_yyyy.$wk_add_mm.$wk_add_dd;
		$wk_last_add_date = ($wk_add_yyyy-1).$wk_add_mm.$wk_add_dd; // 1年前まで 20120409
		$wk_next_add_date = ($wk_add_yyyy+1).$wk_add_mm.$wk_add_dd;

		//印刷以外の場合に出力
		if ($output_flg != "3") {

		//採用年月日
		$data .= "<td align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
		$wk_join = "";

		if ($row["emp_join"] > 0) {
			$wk_join = $row["emp_join"];
		}
		//}
		if ($wk_join != "") {
			$data .= substr($wk_join,0,4)."/".substr($wk_join,4,2)."/".substr($wk_join,6,2);
			if ($output_flg == "1") {
				$data .= "<input type=\"hidden\" id=\"join_{$i}\" name=\"join_{$i}\" value=\"$wk_join\">\n";
			}

		}
		$data .= "</font></td>\n";

		//有休表
		$data .= "<td align=\"center\" nowrap><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
		//表名称
		//名称変更 常勤 週ｎ日 20101117
		//$wk_paid_hol_tbl_id = ($row["paid_hol_add_mmdd"] != "") ? $row["reg_tbl_id"] : $paid_hol_tbl_id;
		// 前回付与データがある場合は、表名称を引き継ぐ 20110309
		$wk_tbl_id_name = $atdbk_common_class->get_paid_hol_tbl_id_name($paid_hol_tbl_id);
		$data .= $wk_tbl_id_name;
		if ($output_flg == "1") {
			$data .= "<input type=\"hidden\" name=\"tbl_id_{$i}\" id=\"tbl_id_{$i}\" value=\"{$paid_hol_tbl_id}\">";
		}
		$data .= "</font></td>\n";
		$data .= "<td align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";

		//日付指定がない場合
		if ($select_add_date == "" && $select_close_date == "---") {
			$paid_hol_tbl_id = "";
		}
		//$paid_hol_tbl_id = ($row["paid_hol_add_mmdd"] != "") ? $row["reg_tbl_id"] : $row["paid_hol_tbl_id"];
		//有休表がなしの場合は表示しない、当年付与データがなしの条件追加 20110329
		if ($paid_hol_tbl_id != "" && $tmp_emppaid_year != "") {
			$data .= "$wk_add_mmdd";
			//db用編集
			if ($output_flg == "1") {
				list($wk_add_yyyy, $wk_add_mm, $wk_add_dd) = split("/", $wk_add_mmdd);
				$wk_add_mmdd2 = sprintf("%02d%02d", $wk_add_mm, $wk_add_dd);
				echo("<input type=\"hidden\" name=\"add_yyyy_{$i}\" id=\"add_yyyy_{$i}\" value=\"$wk_add_yyyy\">");
				echo("<input type=\"hidden\" name=\"add_mmdd_{$i}\" id=\"add_mmdd_{$i}\" value=\"$wk_add_mmdd2\">");
			}
		}
		else {
			//最新の付与年月日
			$wk_last_add_year = $arr_data[$i]["last_add_year"];
			$wk_last_add_mmdd = $row["last_add_mmdd"];
			if ($wk_last_add_year != "" && $wk_last_add_mmdd != "") {
				$wk_last_add_ymd = $wk_last_add_year."/".substr($wk_last_add_mmdd, 0, 2)."/".substr($wk_last_add_mmdd, 2, 2);
				$data .= $wk_last_add_ymd;
			}
		}
		$data .= "</font></td>\n";
		//勤続年数
		$data .= "<td align=\"center\" nowrap><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
		//起算日優先順、勤務条件起算日、職員情報採用日
		if ($row["paid_hol_start_date"] != "") {
			$wk_emp_join = $row["paid_hol_start_date"];
		} else {
			$wk_emp_join = $row["emp_join"];
		}
		//有休表がなしの場合は表示しない、当年付与データがなしの条件追加 20110329
		if ($paid_hol_tbl_id != "" && $tmp_emppaid_year != "") {
			//勤続年数が登録済の場合
			if ($row["service_year"] != "" || $row["service_mon"] != "") {
				$wk_yy = $row["service_year"];
				$wk_mm = $row["service_mon"];
			} else
				//前回付与日がある場合、勤続年数＝前回の勤続年数＋１ //4/1 1/1の場合、前回が１年以下の場合は１
				//20110310
				if ($row["prev_paid_hol_add_mmdd"] != "") {
					if ($closing_paid_holiday != "3") {
						$wk_yy = $row["prev_service_year"];
						$wk_mm = $row["prev_service_mon"];
						if (($wk_yy == 0) || ($wk_yy == 1 && $wk_mm == 0)) {
							$wk_yy = 1;
							$wk_mm = 0;
						} else {
							$wk_yy++;
							$wk_mm = 0; //月数は表示しない
						}
					} else {
						//前回が3ヶ月は1年とする
						if ($row["prev_service_year"] == 0 && $row["prev_service_mon"] == 3) {
							$wk_yy = 1;
							$wk_mm = 0;
						} else {
							$wk_yy = $row["prev_service_year"] + 1;
							$wk_mm = $row["prev_service_mon"];
						}
					}
				}
				else {
					list($wk_yyyy, $wk_mm, $wk_dd) = split("/", $wk_add_mmdd);
					//付与日が4/1,1/1の場合
					if (($closing_paid_holiday == "1" ||
								$closing_paid_holiday == "2")) {
						$wk_end_date = $wk_yyyy.sprintf("%02d%02d", $wk_mm, $wk_dd);
					} else {
						//年またがり確認、3ヶ月や6ヶ月の時、表示されない場合があるのを回避
						$wk_mmdd = sprintf("%02d%02d", $wk_mm, $wk_dd);
						$wk_end_date = $wk_yyyy.$wk_mmdd;
					}
					$len_service = get_length_of_service($wk_emp_join, $wk_end_date);
					list($wk_yy, $wk_mm) = split("/", $len_service);
				}

			$str = "";
			if ($wk_yy > 0) {
				$str .= $wk_yy."年";
				if ($output_flg != "2") {
					$str .= "<br>";
				}
			}
			if ($wk_mm > 0) {
				$str .= $wk_mm."ヶ月";
			}
			$data .= "$str\n";
			if ($output_flg == "1") {
				$data .= "<input type=\"hidden\" id=\"len_service_{$i}\" name=\"len_service_{$i}\" value=\"$len_service\">\n";
			}
		}
		$data .= "</font></td>\n";
		$data .= "<td align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
		//有休表がなしの場合は表示しない、当年付与データがなしの条件追加 20110329
		if ($paid_hol_tbl_id != "" && $tmp_emppaid_year != "") {
			//前年繰越
			//移行データがある場合
			if ($tmp_last_carry != "" &&
					$tmp_prev_days1 == "" &&
					$wk_curr_add_date <= $import_last_add_date &&
					$import_last_add_date < $wk_next_add_date) {
				$tmp_prev_days1 = $tmp_last_carry;
			}
                //前年に移行データがある場合、移行データの当年繰越を設定
			elseif ($tmp_curr_carry != "" &&
					$tmp_prev_days1 == "" &&
					$wk_last_add_date <= $import_last_add_date &&
					$import_last_add_date < $wk_curr_add_date) {
				$tmp_prev_days1 = $tmp_curr_carry;
			}
                //前年繰越時間
                $wk_carry_time = $tmp_prev_carry_time_minute / 60;
                //未設定の時、移行データ確認
                if ($tmp_prev_carry_time_minute == "") {
                    //移行データがある場合
                    if ($tmp_hol_hour_last_carry != "" &&
                            $wk_curr_add_date <= $tmp_hol_hour_data_migration_date &&
                            $tmp_hol_hour_data_migration_date < $wk_next_add_date) {
                        //20141014 HH:MM形式対応
                        $wk_carry_time = $obj_hol_hour->hh_or_hhmm_to_minute($tmp_hol_hour_last_carry) / 60;
                    }
                    //前年に移行データがある場合、移行データの当年繰越を設定
                    elseif ($tmp_hol_hour_curr_carry != "" &&
                            $wk_last_add_date <= $tmp_hol_hour_data_migration_date &&
                            $tmp_hol_hour_data_migration_date < $wk_curr_add_date) {
                        //20141014 HH:MM形式対応
                        $wk_carry_time = $obj_hol_hour->hh_or_hhmm_to_minute($tmp_hol_hour_curr_carry) / 60;
                    }
                }
                //繰越方法
                if ($wk_carry_time > 0) {
                    switch ($obj_hol_hour->paid_hol_hour_carry_flg) {
                        case "2": //そのまま繰越
                            break;
                        case "3": //切捨て
                            $wk_carry_time = 0;
                            break;
                        case "4": //半日切上げ
                            $half_time_min = $wk_specified_time * 60 / 2;
                            //半日時間（分）
                            if ($wk_carry_time * 60 > $half_time_min) {
                                $tmp_prev_days1 += 1;
                                $wk_carry_time = 0;
                            }
                            else if ($wk_carry_time * 60 > 0) {
                                $tmp_prev_days1 += 0.5;
                                $wk_carry_time = 0;
                            }
                            break;
                        case "1": //切上げ
                        default:
                            $tmp_prev_days1 += 1;
                            $wk_carry_time = 0;
                            break;

                    }
                }
                $data .= "$tmp_prev_days1\n";
		}
		$data .= "</font></td>\n";

            //前年繰越時間
            $data .= "<td align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
            if ($paid_hol_tbl_id != "" && $tmp_emppaid_year != "") {
                if ($obj_hol_hour->paid_hol_hour_unit_time == "1") {
                    $wk_carry_hhmm = minute_to_hmm(round($wk_carry_time * 60));
                    $data .= $wk_carry_hhmm;
                }
                else {
                    $data .= $wk_carry_time;
                }
            }
            $data .= "</font></td>\n";

            $data .= "<td align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";

            //前年付与
            //有休表がなしの場合は表示しない、当年付与データがなしの条件追加
		if ($paid_hol_tbl_id != "" && $tmp_emppaid_year != "") {
			//移行データがある場合
			if ($tmp_last_add != "" &&
					$tmp_prev_days2 == "" &&
					$wk_curr_add_date <= $import_last_add_date &&
					$import_last_add_date < $wk_next_add_date) {
				$tmp_prev_days2 = $tmp_last_add;
			}
			//前年に移行データがある場合、移行データの当年付与を設定
			elseif ($tmp_curr_add != "" &&
					$tmp_prev_days2 == "" &&
					$wk_last_add_date <= $import_last_add_date &&
					$import_last_add_date < $wk_curr_add_date) {
				$tmp_prev_days2 = $tmp_curr_add;
			}
			$data .= "$tmp_prev_days2\n";
		}
		$data .= "</font></td>\n";
            //前年調整
            $data .= "<td align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
            if ($paid_hol_tbl_id != "" && $tmp_emppaid_year != "") {
                $data .= "$tmp_prev_adjust_day\n";
            }
            $data .= "</font></td>\n";
            //前年取得
            $data .= "<td align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
		//有休表がなしの場合は表示しない、当年付与データがなしの条件追加
		if ($paid_hol_tbl_id != "" && $tmp_emppaid_year != "") {
			//移行データがある場合
			if ($tmp_last_use != "" &&
					$wk_curr_add_date <= $import_last_add_date &&
					$import_last_add_date < $wk_next_add_date) {
				$tmp_siyo_su = $tmp_last_use;
			}
			//前年に移行データがある場合、移行データの当年取得を設定
			elseif ($tmp_curr_use != "" &&
					$wk_last_add_date <= $import_last_add_date &&
					$import_last_add_date < $wk_curr_add_date) {
				$tmp_siyo_su += $tmp_curr_use;
			}
			$data .= "$tmp_siyo_su\n";
		}
		$data .= "</font></td>\n";
            //前年取得時間
            $data .= "<td align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
            if ($paid_hol_tbl_id != "" && $tmp_emppaid_year != "") {
                //移行データがある場合
                if ($tmp_hol_hour_last_use != "" &&
                        $wk_curr_add_date <= $tmp_hol_hour_data_migration_date &&
                        $tmp_hol_hour_data_migration_date < $wk_next_add_date) {
                        //20141014 HH:MM形式対応
                    $tmp_siyo_su_hour = $obj_hol_hour->hh_or_hhmm_to_minute($tmp_hol_hour_last_use) / 60;
                }
                //前年に移行データがある場合、移行データの当年取得を設定
                elseif ($tmp_hol_hour_curr_use != "" &&
                        $wk_last_add_date <= $tmp_hol_hour_data_migration_date &&
                        $tmp_hol_hour_data_migration_date < $wk_curr_add_date) {
                        //20141014 HH:MM形式対応
                    $tmp_siyo_su_hour += $obj_hol_hour->hh_or_hhmm_to_minute($tmp_hol_hour_curr_use) / 60;
                }
                if ($obj_hol_hour->paid_hol_hour_unit_time == "1") {
                    $tmp_siyo_su_hhmm = minute_to_hmm(round($tmp_siyo_su_hour * 60));
                    $data .= $tmp_siyo_su_hhmm;
                }
                else {
                    $data .= $tmp_siyo_su_hour;
                }
            }

            $data .= "</font></td>\n";
            // 所定労働時間履歴 20121129
            if ($wk_add_mmdd != "") {
                list($wk_add_yyyy, $wk_add_mm, $wk_add_dd) = split("/", $wk_add_mmdd);
                $wk_add_date_spec = $wk_add_yyyy.sprintf("%02d%02d", $wk_add_mm, $wk_add_dd);
                $wk_emp_id = $row["emp_id"];

                $arr_timehist = $calendar_name_class->get_calendarname_time($wk_add_date_spec);
                $day1_time = $arr_timehist["day1_time"];
                $wk_specified_time = $obj_hol_hour->get_specified_time($wk_emp_id, $day1_time) / 60;
            }


            //前年残日数
            $wk_zan_su = $tmp_prev_days1 + $tmp_prev_days2 + $tmp_prev_adjust_day - $tmp_siyo_su;
            //前年残時間
            if ($wk_carry_time > $tmp_siyo_su_hour || $tmp_siyo_su_hour == 0) {
                $wk_zan_su_hour = $wk_carry_time - $tmp_siyo_su_hour;
            }
            else {
                //使用時間を引く時間の不足分を日（時間換算）から引く
                $wk_borrow_day = ceil(($tmp_siyo_su_hour - $wk_carry_time) / $wk_specified_time);

                $wk_zan_su_hour = ($wk_specified_time * $wk_borrow_day) + $wk_carry_time - $tmp_siyo_su_hour;
                $wk_zan_su -= $wk_borrow_day;
            }

            $data .= "<td align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
		//有休表がなしの場合は表示しない、当年付与データがなしの条件追加 20110329
		if ($paid_hol_tbl_id != "" && $tmp_emppaid_year != "") {
			$data .= "$wk_zan_su\n";
		}
		$data .= "</font></td>\n";
            //前年残時間
            $data .= "<td align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";

    		if ($paid_hol_tbl_id != "" && $tmp_emppaid_year != "") {
                if ($obj_hol_hour->paid_hol_hour_unit_time == "1") {
                    $wk_zan_su_hhmm = minute_to_hmm(round($wk_zan_su_hour * 60));
                    $data .= $wk_zan_su_hhmm;
                }
                else {
                    $data .= $wk_zan_su_hour;
                }
            }
            $data .= "</font></td>\n";
}

            //当年繰越
            //移行データがある場合
		if ($tmp_curr_carry != "" &&
				$tmp_days1 == "" &&
				$wk_curr_add_date <= $import_last_add_date &&
				$import_last_add_date < $wk_next_add_date) {
			$tmp_days1 = $tmp_curr_carry;
		} else {
			//繰越日数、未設定の場合か、前年に移行データがある場合
			if ($tmp_days1 == "" ||
					($tmp_curr_add != "" &&
						$tmp_days1 == "" &&
						$wk_last_add_date <= $import_last_add_date &&
						$import_last_add_date < $wk_curr_add_date)) {
				//前年残<=前年付与の場合、前年残
				if ($wk_zan_su <= $tmp_prev_days2) {
					$tmp_days1 = $wk_zan_su;
				}
				//上記以外は前年付与（前々年分を除く）
				else {
					$tmp_days1 = $tmp_prev_days2;
				}

			}
		}

        //移行データフラグ
        $hol_hour_migration_flg = false;
        //時間繰越項目が既に設定済みの場合
        if ($tmp_curr_carry_time_minute != "") {
            $wk_carry_time = $tmp_curr_carry_time_minute / 60;
        }
        else {
            //移行データがある場合
            if ($tmp_hol_hour_curr_carry != "" &&
                    $tmp_curr_carry_time_minute == "" &&
                    $wk_curr_add_date <= $tmp_hol_hour_data_migration_date &&
                    $tmp_hol_hour_data_migration_date < $wk_next_add_date) {
                        //20141014 HH:MM形式対応
                $wk_carry_time = $obj_hol_hour->hh_or_hhmm_to_minute($tmp_hol_hour_curr_carry) / 60;
                $hol_hour_migration_flg = true;
            }
            else {
                $wk_carry_time = $tmp_curr_carry_time_minute / 60;
            }
        }
        //日数換算
        if ($wk_carry_time > $wk_specified_time) {
            $wk_day = intval($wk_carry_time / $wk_specified_time);
            $tmp_days1 += $wk_day;
            $wk_carry_time -= ($wk_specified_time * $wk_day);
        }
            //繰越方法
        if ($wk_carry_time > 0) {
            switch ($obj_hol_hour->paid_hol_hour_carry_flg) {
                case "2": //そのまま繰越
                    break;
                case "3": //切捨て
                    $wk_carry_time = 0;
                    break;
                case "4": //半日切上げ
                    //半日時間（分）
                    $half_time_min = $wk_specified_time * 60 / 2;
                    if ($wk_carry_time * 60 > $half_time_min) {
                        $tmp_days1 += 1;
                        $wk_carry_time = 0;
                    }
                    else if ($wk_carry_time * 60 > 0) {
                        $tmp_days1 += 0.5;
                        $wk_carry_time = 0;
                    }
                    break;
                case "1": //切上げ
                default:
                    $tmp_days1 += 1;
                    $wk_carry_time = 0;
                    break;

            }
        }
        //移行データがない場合
        if ($hol_hour_migration_flg == false) {
            //前々年考慮
            if ($tmp_prev_days2 != "" &&
                $tmp_days1 > $tmp_prev_days2) {
                $tmp_days1 = $tmp_prev_days2; //付与日数
                $wk_carry_time = 0;
            }
        }


		//マイナスの場合は、ゼロとする
		if ($tmp_days1 < 0) {
			$tmp_days1 = 0;
		}
            //有休表がなしの場合は表示しない、当年付与データがなしの条件追加 20110329
		if ($paid_hol_tbl_id == "" ||  $tmp_emppaid_year == "") {
			$tmp_days1 = "";
		}
        //印刷以外の場合に出力
        if ($output_flg != "3") {

            $data .= "<td align=\"center\">";
            $data .= "<font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">$tmp_days1</font>";
            $data .= "</td>\n";

            //当年繰越時間
            $data .= "<td align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
            if ($paid_hol_tbl_id != "" && $tmp_emppaid_year != "") {
                if ($obj_hol_hour->paid_hol_hour_unit_time == "1") {
                    $wk_carry_hhmm = minute_to_hmm(round($wk_carry_time * 60));
                    $data .= $wk_carry_hhmm;
                }
                else {
                    $data .= $wk_carry_time;
                }
            }
            $data .= "</font></td>\n";

            //勤続年数表の位置を取得
            $wk_kinzoku_id = 0;
            //20110310 前回付与日がある場合、上で計算した勤続年を使用する
            if ($row["prev_paid_hol_add_mmdd"] != "") {
                $wk_kinzoku_id = $wk_yy + 1; //配列が1始まりのため
                if ($wk_kinzoku_id > 8) {
                    $wk_kinzoku_id = 8;
                }
            }
            else {
                if ($wk_add_mmdd != "" && $wk_emp_join > 0) {
                    list($wk_add_yyyy, $wk_add_mm, $wk_add_dd) = split("/", $wk_add_mmdd);
                    $wk_add_date = $wk_add_yyyy.sprintf("%02d%02d", $wk_add_mm, $wk_add_dd);

                    //付与日が"3":採用日の場合
                    if ($closing_paid_holiday == "3") {
                        //SQLで取得した6ヶ月後の日が違う場合は、計算が合わなくなるため調整する。8/31が2/28,5/31が11/30等
                        if ($criteria_months == "2" || ($criteria_months == "1" && substr($row["year_after_join"], 0, 4) > $wk_add_yyyy)) {
                            if (substr($row["paid_hol_add_date"], 6, 2) != substr($wk_emp_join, 6, 2)) {
                                $wk_emp_join = substr($wk_emp_join, 0, 6).substr($row["paid_hol_add_date"], 6, 2);

                            }
                        }
                        $wk_join_timestamp = date_utils::to_timestamp_from_ymd($wk_emp_join);
                        //6ヶ月基準
                        if ($criteria_months == "2") {
                            $arr_kikan = array("+7 year $wk_month month", "+6 year $wk_month month", "+5 year $wk_month month", "+4 year $wk_month month", "+3 year $wk_month month", "+2 year $wk_month month", "+1 year $wk_month month", "+$wk_month month");
                        }
                        //3ヶ月基準
                        else {
                            $arr_kikan = array("+7 year", "+6 year", "+5 year", "+4 year", "+3 year", "+2 year", "+1 year", "+$wk_month month");
                        }
                        //$tmp_days2 = "";
                        //7年後から6ヶ月まで勤続の期間を確認
                        for ($wk_idx=0; $wk_idx<8; $wk_idx++) {

                            $wk_kikan = date("Ymd", strtotime($arr_kikan[$wk_idx], $wk_join_timestamp));
                            if ($wk_add_date >= $wk_kikan) {
                                $wk_kinzoku_id = 8 - $wk_idx;
                                break;
                            }
                        }
                        if ($wk_kinzoku_id <= 0) {
                            $wk_kinzoku_id = 1;
                        }
                    }
                    //付与日が"1":4/1、"2":1/1の場合
                    else {
                        //採用後、最初の付与日の年を基準に勤続表の位置を取得
                        $wk_base_year = substr($wk_emp_join, 0, 4);
                        $wk_base_add_date = $wk_base_year.sprintf("%02d%02d", $wk_add_mm, $wk_add_dd);
                        //採用日と最初の付与日が違う場合、基準年を1プラスする
                        if ($wk_emp_join != $wk_base_add_date) {
                            $wk_base_year++;
                        }
                        $wk_kinzoku_id = $wk_yyyy - $wk_base_year + 1;
                        if ($wk_kinzoku_id < 0) {
                            $wk_kinzoku_id = 0;
                        }
                        if ($wk_kinzoku_id > 8) {
                            $wk_kinzoku_id = 8;
                        }
                    }
                }
            }
            //付与日数
            //移行データがある場合
            if ($tmp_curr_add != "" &&
                    $tmp_days2 == "" &&
                    $wk_curr_add_date <= $import_last_add_date &&
                    $import_last_add_date < $wk_next_add_date) {
                $tmp_days2 = $tmp_curr_add;

            } else {
                //有休表番号と勤続年数をもとに有休付与表から取得
                if ($tmp_days2 == "" && $tmp_emppaid_year == "") { //付与データがない場合、ある場合は設定しない 20120821
                    if ($wk_kinzoku_id > 0) {
                        $tmp_days2 = $arr_paid_hol[$paid_hol_tbl_id][$wk_kinzoku_id];
                    } else {
                        $tmp_days2 = "0";
                    }
                }
            }
            //有休表がなしの場合は表示しない、当年付与データがなしの条件追加 20110329
            if ($paid_hol_tbl_id == "" ||  $tmp_emppaid_year == "") {
                $tmp_days2 = "";
            }
            //付与日数
            $data .= "<td align=\"center\">";

            $data .= "<font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">$tmp_days2</font>";
            $data .= "</td>\n";
            //調整日数 20121102
            $data .= "<td align=\"center\">";
            $data .= "<font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">$tmp_adjust_day</font>";

            $data .= "</td>\n";
            //有休表がなしの場合は表示しない、当年付与データがなしの条件追加 20110329
            if ($paid_hol_tbl_id == "" ||  $tmp_emppaid_year == "") {
                $tmp_adjust_day = "";
            }
            //雇用・勤務形態別、時間有休許可確認
            $wk_disp_flg = false;
            if (($paid_hol_tbl_id != "" && $tmp_emppaid_year != "") &&
                    (($tmp_duty_form != "2" && $obj_hol_hour->paid_hol_hour_full_flag == "t") ||
                        ($tmp_duty_form == "2" && $obj_hol_hour->paid_hol_hour_part_flag == "t"))){
                $wk_disp_flg = true;
            }
            //時間有休
            $data .= "<td align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
            if ($wk_disp_flg && $obj_hol_hour->paid_hol_hour_max_flag == "t") {
                $data .= $obj_hol_hour->paid_hol_hour_max_day;
            }
            $data .= "</font></td>\n";
            //単位時間
            $data .= "<td align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
            if ($wk_disp_flg) {
                $data .= $wk_specified_time;
            }
            $data .= "</font></td>\n";
            //時間計
            $data .= "<td align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
            if ($wk_disp_flg && $obj_hol_hour->paid_hol_hour_max_flag == "t") {
                $wk_time_total = $obj_hol_hour->paid_hol_hour_max_day * $wk_specified_time;
                $data .= $wk_time_total;
            }
            $data .= "</font></td>\n";
        }
        //当年有休
		$tmp_days3 = $tmp_days1 + $tmp_days2 + $tmp_adjust_day;
		//有休表がなしの場合は表示しない、当年付与データがなしの条件追加 20110329
		if ($paid_hol_tbl_id == "" ||  $tmp_emppaid_year == "") {
			$tmp_days3 = "";
		}
        else {
            //繰越を設定
            $tmp_curr_hol_time = $wk_carry_time;
        }
		//当年有休
		if ($output_flg == "3") {
			$wk_width = "32";
		} else {
			$wk_width = "";
		}
		$data .= "<td width=\"$wk_width\" align=\"center\">";
		$data .= "<font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">$tmp_days3</font>";
		$data .= "</td>\n";
        //当年有休時間
        $data .= "<td align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
        if ($paid_hol_tbl_id != "" && $tmp_emppaid_year != "") {
            if ($obj_hol_hour->paid_hol_hour_unit_time == "1") {
                $tmp_curr_hol_time = minute_to_hmm(round($tmp_curr_hol_time * 60));
                $data .= $tmp_curr_hol_time;
            }
            else {
                $data .= $tmp_curr_hol_time;
            }
        }
        $data .= "</font></td>\n";

        //当年取得日数
		$data .= "<td width=\"$wk_width\" align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
		//有休表がなしの場合は表示しない、当年付与データがなしの条件追加 20110329
		if ($paid_hol_tbl_id != "" && $tmp_emppaid_year != "") {
			if ($tmp_curr_use != "" &&
					$wk_curr_add_date <= $import_last_add_date &&
					$import_last_add_date < $wk_next_add_date) {
				$tmp_siyo_su2 += $tmp_curr_use;
				$data .= "$tmp_siyo_su2\n";
			} else {
                if ($tmp_siyo_su2 == "") {
                    $tmp_siyo_su2 = "0";
                }
				$data .= "$tmp_siyo_su2\n";
			}
		} else {
			$tmp_siyo_su2 = "0";
		}
		if ($output_flg == "1") {
			$data .= "<input type=\"hidden\" id=\"curr_use_{$i}\" name=\"curr_use_{$i}\" value=\"$tmp_siyo_su2\">\n";
		}
		$data .= "</font></td>\n";
        //当年取得時間
        $data .= "<td align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
        //時間有休
        if ($paid_hol_tbl_id != "" && $tmp_emppaid_year != "") {
            //移行データがある場合、移行データの当年取得時間を加算
            if ($tmp_hol_hour_curr_use != "" &&
                    $wk_curr_add_date <= $tmp_hol_hour_data_migration_date &&
                    $tmp_hol_hour_data_migration_date < $wk_next_add_date) {
                        //20141014 HH:MM形式対応
                $tmp_siyo_su2_hour += $obj_hol_hour->hh_or_hhmm_to_minute($tmp_hol_hour_curr_use) / 60;
            }
            $wk_next_add_date2 = date("Ymd", strtotime("1 year",date_utils::to_timestamp_from_ymd($wk_next_add_date)));
            //移行データの前年取得がある場合
            if ($tmp_hol_hour_last_use != "" &&
                    $wk_next_add_date <= $tmp_hol_hour_data_migration_date &&
                    $tmp_hol_hour_data_migration_date < $wk_next_add_date2) {
                        //20141014 HH:MM形式対応
                $tmp_siyo_su2_hour = $obj_hol_hour->hh_or_hhmm_to_minute($tmp_hol_hour_last_use) / 60;
            }
            if ($obj_hol_hour->paid_hol_hour_unit_time == "1") {
                $tmp_siyo_su2_hhmm = minute_to_hmm(round($tmp_siyo_su2_hour * 60));
                $data .= $tmp_siyo_su2_hhmm;
            }
            else {
                $data .= $tmp_siyo_su2_hour;
            }
        }
        $data .= "</font></td>\n";

        //当年残日数
		$data .= "<td width=\"$wk_width\" align=\"center\">";
		//有休表がなしの場合は表示しない、当年付与データがなしの条件追加 20110329
        if ($paid_hol_tbl_id != "" && $tmp_emppaid_year != "") {
            $wk_curr_zan_su = $tmp_days3 - $tmp_siyo_su2;
            //時間数計算
            //取得数が少ない場合
            if ($tmp_curr_hol_time > $tmp_siyo_su2_hour) {
                $tmp_curr_zan_time = $tmp_curr_hol_time - $tmp_siyo_su2_hour;
            }
            //取得数が多い場合、日から減算
            else {

                // 所定労働時間履歴 20121130
                if ($wk_add_mmdd != "" && $output_flg == "3") {
                    list($wk_add_yyyy, $wk_add_mm, $wk_add_dd) = split("/", $wk_add_mmdd);
                    $wk_add_date_spec = $wk_add_yyyy.sprintf("%02d%02d", $wk_add_mm, $wk_add_dd);
                    $wk_emp_id = $row["emp_id"];

                    $arr_timehist = $calendar_name_class->get_calendarname_time($wk_add_date_spec);
                    $day1_time = $arr_timehist["day1_time"];
                    $wk_specified_time = $obj_hol_hour->get_specified_time($wk_emp_id, $day1_time) / 60;
                }
                $wk_borrow_day = ceil(($tmp_siyo_su2_hour - $tmp_curr_hol_time) / $wk_specified_time);
                $tmp_curr_zan_time = ($wk_specified_time * $wk_borrow_day) + $tmp_curr_hol_time - $tmp_siyo_su2_hour;
                $wk_curr_zan_su -= $wk_borrow_day;
            }
        } else {
			$wk_curr_zan_su = "";
		}
		$data .= "<font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">$wk_curr_zan_su</font>";
		$data .= "</td>\n";
        //当年残時間
        $data .= "<td align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
        if ($paid_hol_tbl_id != "" && $tmp_emppaid_year != "") {
            if ($obj_hol_hour->paid_hol_hour_unit_time == "1") {
                $tmp_curr_zan_hhmm = minute_to_hmm(round($tmp_curr_zan_time * 60));
                $data .= $tmp_curr_zan_hhmm;
            }
            else {
                $data .= $tmp_curr_zan_time;
            }
        }
        $data .= "</font></td>\n";
        //有休消化率
		if ($output_flg != "3") { //印刷以外
			$data .= "<td align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
			if ($paid_hol_tbl_id != "" && $tmp_emppaid_year != "") {
                //時間有休分を日数換算し足した後に計算
                $tmp_siyo_su2_kansan = ($tmp_siyo_su2_hour / $wk_specified_time);
                $tmp_siyo_su2_all = $tmp_siyo_su2 + $tmp_siyo_su2_kansan;

                $tmp_days3_kansan = ($tmp_curr_hol_time / $wk_specified_time);
                $tmp_days3_all = $tmp_days3 + $tmp_days3_kansan;

                $tmp_ritsu = $tmp_siyo_su2_all / $tmp_days3_all * 100;
				$tmp_ritsu = sprintf("%.1f", $tmp_ritsu);
				$data .= $tmp_ritsu;
			}
			$data .= "</font></td>\n";
			//当年分
			$data .= "<td align=\"center\"><font size=\"$font_size\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">";
			if ($paid_hol_tbl_id != "" && $tmp_emppaid_year != "") {
                //時間有休分を日数換算し足した後に計算
                $tmp_siyo_su2_kansan = ($tmp_siyo_su2_hour / $wk_specified_time);
                $tmp_siyo_su2_all = $tmp_siyo_su2 + $tmp_siyo_su2_kansan;

                $tmp_ritsu = $tmp_siyo_su2_all / $tmp_days2 * 100;
				$tmp_ritsu = sprintf("%.1f", $tmp_ritsu);
				$data .= $tmp_ritsu;
			}
			$data .= "</font></td>\n";
		}
		$data .= "</tr>\n";
		$i++;

		if ($output_flg != "2") {
			echo($data);
			$data = "";
		}

	}

	$data .= "</table>\n";

	if ($output_flg != "2") { //excel以外
		$data .= "<input type=\"hidden\" name=\"data_cnt\" value=\"$i\">\n";
		$data .= "<input type=\"hidden\" name=\"emp_ids\" value=\"$emp_ids\">\n";
		echo($data);
		$data = "";
	}
	return $data;
}

//日、時間の減算、時間有休処理用
function day_hour_subtract($day1, $hour1, $day2, $hour2, $specified_hour) {

    if ($hour1 > $hour2) {
        $wk_day = $day1 - $day2;
        $wk_hour = $hour1 - $hour2;
        //時間を日換算
        if ($wk_hour > $specified_hour) {
            $wk_carry_up_day = intval($wk_hour / $specified_hour);
            $wk_day += $wk_carry_up_day;
            $wk_hour = $wk_hour - ($specified_hour * $wk_carry_up_day);
        }
    }
    else {
        $wk_day = $day1 - $day2;
        $wk_borrow_day = ceil(($hour2 - $hour1) / $specified_hour);
        $wk_hour = ($specified_hour * $wk_borrow_day) + $hour1 - $hour2;
        $wk_day -= $wk_borrow_day;
    }
    //日と時間を、日換算
    if ($wk_hour > 0) {
        $wk_day_kansan = $wk_day + ($wk_hour / $specified_hour);
    }
    else {
        $wk_day_kansan = $wk_day;
    }

    return array($wk_day, $wk_hour, $wk_day_kansan);
}

//直近1年のうちに付与データが2件ある場合の前回分を取得 20131029
//キー[emp_id]["prev_days1" , "prev_days2", "add_date" ]、値に前回繰越と付与日数、付与日を入れた配列を返す
function get_prev_days_second($con, $fname, $select_close_date) {

    $arr_data = array();
    if ($select_close_date == "" || $select_close_date == "---") {
        return $arr_data;
    }

    $yyyy = substr($select_close_date, 0, 4);
    $mmdd = substr($select_close_date, 4, 4);

    $start_date = ($yyyy-1).$mmdd;
    $end_date = $select_close_date;
    //1年以内に付与データが2件以上ある職員のデータを取得、ただし、月日が空白、同じデータがある場合を除く
    $sql = "select distinct on (emp_id, year, paid_hol_add_mmdd) emp_id, year, paid_hol_add_mmdd, days1, days2 from emppaid";
    $cond = "where emp_id in (select a.emp_id from "
        ." ( "
        ." select b.emp_id,count(*) as cnt from  "
        ." ( select distinct emp_id, year, paid_hol_add_mmdd from emppaid where"
        ." CAST(year AS varchar)||paid_hol_add_mmdd > '$start_date' and "
        ." CAST(year AS varchar)||paid_hol_add_mmdd <= '$end_date' "
        ." and paid_hol_add_mmdd != '' and paid_hol_add_mmdd is not null "
        ." group by emp_id, year, paid_hol_add_mmdd order by emp_id, year, paid_hol_add_mmdd "
        ." ) b group by b.emp_id "
        ." ) a "
        ." where a.cnt > 1 "
        ." ) "
        ." and  paid_hol_add_mmdd != '' order by emp_id, year desc, paid_hol_add_mmdd desc";
	$sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }
    $arr_empdata = array();
    while ($row = pg_fetch_array($sel)) {
        $arr_empdata[ $row["emp_id"] ]["days1"][] = $row["days1"];
        $arr_empdata[ $row["emp_id"] ]["days2"][] = $row["days2"];
        $arr_empdata[ $row["emp_id"] ]["add_date"][] = $row["year"].$row["paid_hol_add_mmdd"];
    }
    foreach ($arr_empdata as $wk_emp_id => $wk_data) {
        $arr_data[$wk_emp_id]["prev_days1"] = $wk_data["days1"][1];
        $arr_data[$wk_emp_id]["prev_days2"] = $wk_data["days2"][1];
        $arr_data[$wk_emp_id]["add_date"] = $wk_data["add_date"][1];
    }

    return $arr_data;
}

?>