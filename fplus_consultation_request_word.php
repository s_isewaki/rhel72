<?php
require_once ( './libs/phpword/PHPWord.php' );
require_once ( './libs/phpword/PHPWord/IOFactory.php' );
require_once ( './libs/phpword/PHPWord/Writer/Word2007.php' );
// オブジェクト
$PHPWord = new PHPWord();


//====================================================================
// ヘッダ設定
//====================================================================
header("Content-Disposition: attachment; filename=fplus_medical_accident_info_".Date("Ymd_Hi").".docx;");
header("Content-Transfer-Encoding: binary");


// 日本語はUTF-8へ変換する。(mb_convert_encoding関数を使用)
// 変換しない場合は文書ファイルが壊れて生成される。

// デフォルトのフォント
$PHPWord->setDefaultFontName(mb_convert_encoding('ＭＳ 明朝','UTF-8','EUC-JP'));
$PHPWord->setDefaultFontSize(10.5);

// プロパティ
$properties = $PHPWord->getProperties();
$properties->setTitle(mb_convert_encoding('医療安全コンサルテーション依頼書','UTF-8','EUC-JP'));
$properties->setCreator(mb_convert_encoding('Medi-System','UTF-8','EUC-JP')); 
$properties->setSubject(mb_convert_encoding('','UTF-8','EUC-JP')); 
$properties->setCompany(mb_convert_encoding('（株）メディシステムソリューション','UTF-8','EUC-JP'));
$properties->setCategory(mb_convert_encoding('','UTF-8','EUC-JP'));
$properties->setDescription(mb_convert_encoding('','UTF-8','EUC-JP'));
$properties->setCreated(mktime((int)date("s"), (int)date("i"), (int)date("h"), (int)date("m"), (int)date("d"), (int)date("Y")));
$properties->setModified(mktime((int)date("s"), (int)date("i"), (int)date("h"), (int)date("m"), (int)date("d"), (int)date("Y")));
$properties->setKeywords(mb_convert_encoding('','UTF-8','EUC-JP'));
$properties->setDescription(mb_convert_encoding('','UTF-8','EUC-JP')); 
$properties->setLastModifiedBy(mb_convert_encoding('Medi-System','UTF-8','EUC-JP'));

// セクションとスタイル、単位に注意(1/144インチ)
$section = $PHPWord->createSection();
$sectionStyle = $section->getSettings();
$sectionStyle->setLandscape();
$sectionStyle->setPortrait();
$sectionStyle->setMarginLeft(1700);
$sectionStyle->setMarginRight(1700);
$sectionStyle->setMarginTop(1700);
$sectionStyle->setMarginBottom(1700);

//前文
$PHPWord->addFontStyle('intro_pStyle', array('size'=>10.5,'spacing'=>100));
$PHPWord->addParagraphStyle('intro_align', array('align'=>'left'));
$section->addText(mb_convert_encoding(mb_convert_encoding('○○病院',"sjis-win","eucJP-win"),"UTF-8","sjis-win"), 'intro_pStyle', 'intro_align');
$section->addText(mb_convert_encoding(mb_convert_encoding('■■ ■■ 様',"sjis-win","eucJP-win"),"UTF-8","sjis-win"), 'intro_pStyle', 'intro_align');
$section->addTextBreak(1);
$section->addText(mb_convert_encoding(' ＫＫＲグループ病院より依頼のありました下記の問題点に関して、ご意見をいただきますようお願い致します。尚、ご意見は2週間程度でいただけると幸いです。','UTF-8','EUC-JP'), 'intro_pStyle', 'intro_align');
$align = array('align'=>'right');
$section->addText(mb_convert_encoding(mb_convert_encoding('医療安全対策専門役 △△ △△',"sjis-win","eucJP-win"),"UTF-8","sjis-win"), 'intro_pStyle', $align);
$section->addTextBreak(1);

//タイトル
$PHPWord->addFontStyle('title_pStyle', array('size'=>14,'spacing'=>100));
$PHPWord->addParagraphStyle('title_align', array('align'=>'center'));

$section->addText(mb_convert_encoding('KKR医療安全コンサルテーション依頼書','UTF-8','EUC-JP'), 'title_pStyle', 'title_align');
$section->addTextBreak(1);
// 本文
$PHPWord->addFontStyle('contents_pStyle', array('size'=>10.5,'spacing'=>150));
$PHPWord->addParagraphStyle('contents_align', array('align'=>'left'));
// 表形式
// Define table style arrays
$styleTable = array('cellMarginLeft'=>80, 'cellMarginRight'=>80);
// Add table style
$PHPWord->addTableStyle('myOwnTableStyle', $styleTable);
// Add table
$table = $section->addTable('myOwnTableStyle');

// タイトル行
$table->addRow(400);
$LName=mb_convert_encoding('タイトル','UTF-8','EUC-JP');
$RName=mb_convert_encoding($data_title,'UTF-8','EUC-JP');
$styleCell = array('valign'=>'center','borderSize'=>6);
$table->addCell(1366,$styleCell)->addText($LName,'contents_pStyle','contents_align');;
$table->addCell(7330,$styleCell)->addText($RName,'contents_pStyle','contents_align');

// 依頼内容行
$table->addRow(400);
$LName=mb_convert_encoding('依頼内容','UTF-8','EUC-JP');
if($data_request_contents1[0]=="1"){
	$RName1=mb_convert_encoding(mb_convert_encoding('（○）�仝彊�究明のための助言',"sjis-win","eucJP-win"),"UTF-8","sjis-win");
}else{
	$RName1=mb_convert_encoding(mb_convert_encoding('（　）�仝彊�究明のための助言',"sjis-win","eucJP-win"),"UTF-8","sjis-win");
}
if($data_request_contents2[0]=="1"){
	$RName2=mb_convert_encoding(mb_convert_encoding('（○）��提供した医療の水準の評価',"sjis-win","eucJP-win"),"UTF-8","sjis-win");
}else{
	$RName2=mb_convert_encoding(mb_convert_encoding('（　）��提供した医療の水準の評価',"sjis-win","eucJP-win"),"UTF-8","sjis-win");
}
if($data_request_contents3[0]=="1"){
	$RName3=mb_convert_encoding(mb_convert_encoding("（○）��その他 [$data_request_contents3_text]","sjis-win","eucJP-win"),"UTF-8","sjis-win");
}else{
	$RName3=mb_convert_encoding(mb_convert_encoding('（　）��その他 [ ]',"sjis-win","eucJP-win"),"UTF-8","sjis-win");
}
$styleCell = array('valign'=>'top','borderSize'=>6);
$table->addCell(1366,$styleCell)->addText($LName,'contents_pStyle','contents_align');
$cell = $table->addCell(7330,$styleCell);
$cell->addText($RName1,'contents_pStyle','contents_align');
$cell->addText($RName2,'contents_pStyle','contents_align');
$cell->addText($RName3,'contents_pStyle','contents_align');

// 事例紹介行
$table->addRow(400);
$LName=mb_convert_encoding('事例紹介','UTF-8','EUC-JP');
$RName=mb_convert_encoding(mb_convert_encoding("※ 添付ファイルを参照下さい","sjis-win","eucJP-win"),"UTF-8","sjis-win");
$styleCell = array('valign'=>'center','borderSize'=>6);
$table->addCell(1366,$styleCell)->addText($LName,'contents_pStyle','contents_align');
$table->addCell(7330,$styleCell)->addText($RName,'contents_pStyle','contents_align');

// 問題点・疑問点の整理行
$table->addRow(400);
$styleCell = array('valign'=>'top','borderSize'=>6);
$LName=mb_convert_encoding('問題点・疑問点の整理','UTF-8','EUC-JP');
$RName=mb_convert_encoding('問題点1','UTF-8','EUC-JP');
$table->addCell(1366,$styleCell)->addText($LName,'contents_pStyle','contents_align');
$cell = $table->addCell(7330,$styleCell);
$cell->addText($RName,'contents_pStyle','contents_align');
// 入力データを改行で分割
$ary_first_problem = preg_split("/\n/", $data_first_problem);
for($i=0;$i<count($ary_first_problem);$i++){
	$RName=mb_convert_encoding("　$ary_first_problem[$i]",'UTF-8','EUC-JP');
	$cell->addText($RName,'contents_pStyle','contents_align');
}
$cell->addText();

for($i=2;$i<=$data_problem_count;$i++){
	if($i==2){
		$RName=mb_convert_encoding('問題点2','UTF-8','EUC-JP');
		$cell->addText($RName,'contents_pStyle','contents_align');
		// 入力データを改行で分割
		$ary_second_problem = preg_split("/\n/", $data_second_problem);
		for($j=0;$j<count($ary_second_problem);$j++){
			$RName=mb_convert_encoding("　$ary_second_problem[$j]",'UTF-8','EUC-JP');
			$cell->addText($RName,'contents_pStyle','contents_align');
		}
		$cell->addText();
	}
	if($i==3){
		$RName=mb_convert_encoding('問題点3','UTF-8','EUC-JP');
		$cell->addText($RName,'contents_pStyle','contents_align');
		// 入力データを改行で分割
		$ary_third_problem = preg_split("/\n/", $data_third_problem);
		for($j=0;$j<count($ary_third_problem);$j++){
			$RName=mb_convert_encoding("　$ary_third_problem[$j]",'UTF-8','EUC-JP');
			$cell->addText($RName,'contents_pStyle','contents_align');
		}
		$cell->addText();
	}
	if($i==4){
		$RName=mb_convert_encoding('問題点4','UTF-8','EUC-JP');
		$cell->addText($RName,'contents_pStyle','contents_align');
		// 入力データを改行で分割
		$ary_fourth_problem = preg_split("/\n/", $data_fourth_problem);
		for($j=0;$j<count($ary_fourth_problem);$j++){
			$RName=mb_convert_encoding("　$ary_fourth_problem[$j]",'UTF-8','EUC-JP');
			$cell->addText($RName,'contents_pStyle','contents_align');
		}
		$cell->addText();
	}
	if($i==5){
		$RName=mb_convert_encoding('問題点5','UTF-8','EUC-JP');
		$cell->addText($RName,'contents_pStyle','contents_align');
		// 入力データを改行で分割
		$ary_fifth_problem = preg_split("/\n/", $data_fifth_problem);
		for($j=0;$j<count($ary_fifth_problem);$j++){
			$RName=mb_convert_encoding("　$ary_fifth_problem[$j]",'UTF-8','EUC-JP');
			$cell->addText($RName,'contents_pStyle','contents_align');
		}
		$cell->addText();
	}
	if($i==6){
		$RName=mb_convert_encoding('問題点6','UTF-8','EUC-JP');
		$cell->addText($RName,'contents_pStyle','contents_align');
		// 入力データを改行で分割
		$ary_sixth_problem = preg_split("/\n/", $data_sixth_problem);
		for($j=0;$j<count($ary_sixth_problem);$j++){
			$RName=mb_convert_encoding("　$ary_sixth_problem[$j]",'UTF-8','EUC-JP');
			$cell->addText($RName,'contents_pStyle','contents_align');
		}
		$cell->addText();
	}
	if($i==7){
		$RName=mb_convert_encoding('問題点7','UTF-8','EUC-JP');
		$cell->addText($RName,'contents_pStyle','contents_align');
		// 入力データを改行で分割
		$ary_seventh_problem = preg_split("/\n/", $data_seventh_problem);
		for($j=0;$j<count($ary_seventh_problem);$j++){
			$RName=mb_convert_encoding("　$ary_seventh_problem[$j]",'UTF-8','EUC-JP');
			$cell->addText($RName,'contents_pStyle','contents_align');
		}
		$cell->addText();
	}
	if($i==8){
		$RName=mb_convert_encoding('問題点8','UTF-8','EUC-JP');
		$cell->addText($RName,'contents_pStyle','contents_align');
		// 入力データを改行で分割
		$ary_eighth_problem = preg_split("/\n/", $data_eighth_problem);
		for($j=0;$j<count($ary_eighth_problem);$j++){
			$RName=mb_convert_encoding("　$ary_eighth_problem[$j]",'UTF-8','EUC-JP');
			$cell->addText($RName,'contents_pStyle','contents_align');
		}
		$cell->addText();
	}
	if($i==9){
		$RName=mb_convert_encoding('問題点9','UTF-8','EUC-JP');
		$cell->addText($RName,'contents_pStyle','contents_align');
		// 入力データを改行で分割
		$ary_nineth_problem = preg_split("/\n/", $data_nineth_problem);
		for($j=0;$j<count($ary_nineth_problem);$j++){
			$RName=mb_convert_encoding("　$ary_nineth_problem[$j]",'UTF-8','EUC-JP');
			$cell->addText($RName,'contents_pStyle','contents_align');
		}
		$cell->addText();
	}
	if($i==10){
		$RName=mb_convert_encoding('問題点10','UTF-8','EUC-JP');
		$cell->addText($RName,'contents_pStyle','contents_align');
		// 入力データを改行で分割
		$ary_tenth_problem = preg_split("/\n/", $data_tenth_problem);
		for($j=0;$j<count($ary_tenth_problem);$j++){
			$RName=mb_convert_encoding("　$ary_tenth_problem[$j]",'UTF-8','EUC-JP');
			$cell->addText($RName,'contents_pStyle','contents_align');
		}
		$cell->addText();
	}
}

// 依頼内容に対する見解行
$table->addRow(1200);
$LName=mb_convert_encoding('依頼内容に対する見解','UTF-8','EUC-JP');
$RName=mb_convert_encoding('1についてのコンサルテーション内容','UTF-8','EUC-JP');
$styleCell = array('valign'=>'top','borderSize'=>6);
$table->addCell(1366,$styleCell)->addText($LName,'contents_pStyle','contents_align');
$cell = $table->addCell(7330,$styleCell);
$cell->addText($RName,'contents_pStyle','contents_align');
$cell->addText();
$cell->addText();
for($i=2;$i<=$data_problem_count;$i++){
	$RName=mb_convert_encoding("{$i}についてのコンサルテーション内容",'UTF-8','EUC-JP');
	$cell->addText($RName,'contents_pStyle','contents_align');
	$cell->addText();
	$cell->addText();
}

// 出力
$WORD_OUT =  PHPWord_IOFactory::createWriter($PHPWord,'WORD2007');
//$WORD_OUT->save('./fplus/workflow/tmp/test.docx');
$WORD_OUT->save('php://output');

exit;

php?>
