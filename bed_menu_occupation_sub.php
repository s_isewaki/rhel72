<?
session_start();
$_SESSION['bed_in_ret_url'] = "bed_menu_occupation_sub.php?bldg_cd=$bldg_cd&ward_cd=$ward_cd&session=$session";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 病床管理 | 病床利用状況</title>
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_occupation_detail.ini");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 病床管理権限チェック
$checkauth = check_authority($session, 14, $fname);
if($checkauth == "0"){
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 診療科登録権限を取得
$section_admin_auth = check_authority($session, 28, $fname);

// 病棟登録権限を取得
$ward_admin_auth = check_authority($session, 21, $fname);

// データベースに接続
$con = connect2db($fname);

// 棟名を取得
$sql = "select bldg_name from bldgmst";
$cond = "where bldg_cd = $bldg_cd";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$bldg_name = pg_fetch_result($sel, 0, "bldg_name");

if ($ward_cd != 0) {

	// 病棟名の取得
	$sql = "select ward_name from wdmst";
	$cond = "where bldg_cd = $bldg_cd and ward_cd = $ward_cd";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$ward_name = pg_fetch_result($sel, 0, "ward_name");
} else {
	$ward_name = "すべて";
}

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// 画面更新間隔の値を取得
$sql = "select refresh_sec from roomlayout";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if (pg_num_rows($sel) > 0) {
	$refresh_sec = pg_fetch_result($sel, 0, "refresh_sec");
} else {
	$refresh_sec = 300;
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/popup.js"></script>
<script type="text/javascript" src="js/yui/3.10.3/build/yui/yui-min.js"></script>
<script type="text/javascript">
function initPage() {
	setTimeout(function(){location.reload();}, <? echo($refresh_sec * 1000); ?>);

	YUI().use('dd-drag', 'dd-drop', 'dd-scroll', function(Y){
		var divs = document.getElementsByTagName('div');
		for (var i = 0, j = divs.length; i < j; i++) {
			switch (divs[i].className) {
			case 'used':
				new Y.DD.Drop({node: '#' + divs[i].id});

				var bed = new Y.DD.Drag({node: '#' + divs[i].id}).plug(Y.Plugin.DDWinScroll);
				bed.set('data', bed.get('node').getXY());
				bed.on('drag:start', function (e){
					var dragNode = this.get('node');
					dragNode.setStyle('backgroundColor', '#fefe83');
					dragNode.setStyle('zIndex', 999);
				});
				bed.on('drag:drophit', function(e){
					var pt_id = e.drag.get('node').get('id').split('_').slice(5).join('');
					var bed_info = e.drop.get('node').get('id').split('_');
					window.open('inpatient_move_reserve_register.php?session=<? echo($session); ?>&pt_id='.concat(pt_id).concat('&ward=').concat(bed_info[1]).concat('-').concat(bed_info[2]).concat('&ptrm=').concat(bed_info[3]).concat('&bed=').concat(bed_info[4]).concat('&direct=t'), 'newwin', 'width=900,height=640,scrollbars=yes');
				});
				bed.on('drag:end', function(e){
					var dragNode = this.get('node');
					dragNode.setXY(this.get('data'));
					dragNode.setStyle('backgroundColor', '');
					dragNode.setStyle('zIndex', 0);
				});
				break;

			case 'vacant':
				new Y.DD.Drop({node: '#' + divs[i].id});
				break;
			}
		}
	});
}

function popupPatientDetail(pt_info1, pt_info13, pt_info2, pt_info3, pt_info4, pt_info5, pt_info6, pt_info7, pt_info8, pt_info22, pt_info23, pt_info9, pt_info10, pt_info11, pt_info14, pt_info15, pt_info20, pt_info21, pt_info12, pt_info16, pt_info19, pt_info18, pt_info17, e) {
	popupDetailBlue(
		new Array(
			'患者氏名', pt_info1,
			'患者ID', pt_info13,
			'保険', pt_info2,
			'性別', pt_info3,
			'年齢', pt_info4,
			'診療科', pt_info5,
			'主治医', pt_info6,
			'移動', pt_info7,
			'観察', pt_info8,
			'面会', pt_info9,
			'外泊', pt_info10,
			'外出', pt_info11,
			'入院日', pt_info14,
			'在院日数', pt_info15,
			'回復期リハビリ算定期限分類', pt_info22,
			'回復期リハビリ算定期限日', pt_info23,
			'リハビリ算定期限分類', pt_info20,
			'リハビリ算定期限日', pt_info21,
			'退院予定日', pt_info12,
			'医療区分・ADL区分', pt_info16,
			'看護度分類', pt_info19,
			'個人情報に関する要望', pt_info18,
			'特記事項', pt_info17
		), 350, 100, e
	);
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
form {margin:0;}
table.submenu {border-collapse:collapse;}
table.submenu td {border:#5279a5 solid 1px;}
table.bed {border-collapse:collapse;}
table.bed td {border:solid #5279a5 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr height="30" bgcolor="#f6f9ff">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">事業所（棟）：<? echo($bldg_name); ?> 病棟：<? echo($ward_name); ?></font></td>
<td align="right"><input type="button" value="閉じる" onclick="window.close();"></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<?
if ($ward_cd == "0") {
	show_bldg_report($con, $bldg_cd, $fname);
	$ward_cd = "";
}

show_ward_layout($con, $bldg_cd, $ward_cd, $session, $fname);

if ($ward_cd != "") {
	show_ward_report($con, $bldg_cd, $ward_cd, $fname);
}
?>
</body>
<? pg_close($con); ?>
</html>
