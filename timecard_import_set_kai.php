<?
if ($ins_upd_flg == "1") {
	$wk_title_str = "登録";
} else {
	$wk_title_str = "更新";
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 出勤表 | 勤務シフト(快決シフト)種類変換設定<?=$wk_title_str?></title>
<?
//ini_set("display_errors","1");
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("duty_shift_common_class.php");
require_once("date_utils.php");
require_once("get_values.ini");
require_once("timecard_import_common.ini");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
// セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}


///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);

$wktmgrp_array = $obj->get_wktmgrp_array();

$data_atdptn = $obj->get_atdptn_array("", "1");
$reason_2_array = $obj->get_reason_2("1");

if ($no != "" && $back != "t") {
	$cnvdata = get_cnvdata_kai($con, $fname, $no);
	//登録の場合も、勤務パターングループを設定
	$tmcd_group_id = $cnvdata[0]["tmcd_group_id"];

	//更新の場合
	if ($ins_upd_flg == "2") {
		$absence_reason = $cnvdata[0]["absence_reason"];
		$duty_ptn = $cnvdata[0]["duty_ptn"];
		$atdptn_ptn_id = $cnvdata[0]["atdptn_ptn_id"];
		$reason_2 = $cnvdata[0]["reason"];
	}
}

?>

<!-- ************************************************************************ -->
<!-- JavaScript -->
<!-- ************************************************************************ -->
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
	///-----------------------------------------------------------------------------
	// 「更新」
	///-----------------------------------------------------------------------------
	function updateBtn() {
		if (document.mainform.absence_reason.value == '') {
			alert('不在理由を入力してください');
			return;
		}
		if (document.mainform.duty_ptn.value == '') {
			alert('勤務区分を入力してください');
			return;
		}

		document.mainform.action = "timecard_import_set_kai_update.php";
		document.mainform.submit();

	}

var ptns = new Array();
<?
$key_id = "";

for ($i=0; $i<count($data_atdptn); $i++) {
	//非表示を除く 20101007
	if ($data_atdptn[$i]["display_flag"] == "f") {
		continue;
	}
	$wk_group_id = $data_atdptn[$i]["group_id"];
	$wk_atdptn_id = $data_atdptn[$i]["id"];
	$wk_name = $data_atdptn[$i]["name"];
	if ($wk_group_id != $key_id) {
		echo("ptns[$wk_group_id] = new Array();\n");
	}
	echo("ptns[$wk_group_id][$wk_atdptn_id] = '$wk_name';\n");
	$key_id = $wk_group_id;
}
?>

function setPtnOptions(default_ptn) {
	deleteAllOptions(document.mainform.atdptn_ptn_id);

	addOption(document.mainform.atdptn_ptn_id, '', '未設定', default_ptn);
	var tmcd_group_id = document.mainform.tmcd_group_id.value;
	if (ptns[tmcd_group_id]) {
		for (var i in ptns[tmcd_group_id]) {
			addOption(document.mainform.atdptn_ptn_id, i, ptns[tmcd_group_id][i], default_ptn);
		}
	}

}
function addOption(box, value, text, selected) {
	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	if (selected == value) {
		opt.selected = true;
	}
	box.options[box.length] = opt;
	if (!box.multiple) {
		try {box.style.fontSize = 'auto';} catch (e) {}
		box.style.overflow = 'auto';
	}
}
function deleteAllOptions(box) {
	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}
}


</script>

<!-- ************************************************************************ -->
<!-- HTML -->
<!-- ************************************************************************ -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="setPtnOptions('<?=$atdptn_ptn_id?>');">
<center>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
		<tr height="32" bgcolor="#5279a5">
		<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>勤務シフト種類変換設定<?=$wk_title_str?></b></font></td>
		<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
		</tr>
	</table>

	<form name="mainform" method="post">

	<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr>
<td width="240" bgcolor="#f6f9ff" colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>勤務シフト（快決シフト）</b></font></td>
<td width="40" bgcolor="#f6f9ff" rowspan="2"><font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>&nbsp;</b></font></td>
<td width="160" bgcolor="#f6f9ff" rowspan="2"><font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>勤務パターン<br>グループ</b></font></td>
<td width="100" bgcolor="#f6f9ff" rowspan="2"><font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>勤務パターン</b></font></td>
<td width="100" bgcolor="#f6f9ff" rowspan="2"><font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>事由</b></font></td>
</tr>

<tr>
<td width="120" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>不在理由</b></font></td>
<td width="120" bgcolor="#f6f9ff"><font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>勤務区分</b></font></td>
</tr>

		<tr height="22">
		<td align="center"><input type="text" name="absence_reason" id="absence_reason" size="20" maxlength="20" value="<?=$absence_reason?>" style="ime-mode:inactive;"></td>
		<td align="center"><input type="text" name="duty_ptn" id="duty_ptn" size="20" maxlength="20" value="<?=$duty_ptn?>" style="ime-mode:inactive;"></td>
		<td align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><b>←→</b></font></td>
		<td align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
<?
//勤務パターングループ
	echo("　<select id=\"tmcd_group_id\" name=\"tmcd_group_id\" onchange=\"setPtnOptions('');\">");
	for ($i=0; $i<count($wktmgrp_array); $i++) {
		$wk_id = $wktmgrp_array[$i]["id"];
		$wk_name = $wktmgrp_array[$i]["name"];

		$selected = ($tmcd_group_id == $wk_id) ? " selected" : "";
		echo("<option value=\"$wk_id\" $selected>$wk_name</option>\n");
	}
	echo("</select>");

?>
		</font></td>
		<td align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
<?
//勤務パターン
	echo("　<select id=\"atdptn_ptn_id\" name=\"atdptn_ptn_id\">");
	echo("</select>");
?>
</font></td>
		<td align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
<?
//事由
	echo("　<select id=\"reason_2\" name=\"reason_2\">");
	$selected = ($reason_2 == "") ? " selected" : "";
	echo("<option value=\"\" $selected>未設定</option>");
	for ($i=0; $i<count($reason_2_array); $i++) {
		$wk_id = $reason_2_array[$i]["id"];
		$wk_name = $reason_2_array[$i]["name"];
		$selected = ((int)$reason_2 == $wk_id) ? " selected" : "";
		echo("<option value=\"$wk_id\" $selected>$wk_name</option>\n");
	}
	echo("</select>");
?>
</font></td>

		</tr>
	</table>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="100%" align="right">
			<input type="button" id="upd" name="upd" value="　　<?=$wk_title_str?>　　" onclick="updateBtn();">
		</td>
		</tr>
	</table>

	<input type="hidden" name="session" value="<?=$session?>">
	<input type="hidden" name="no" value="<?=$no?>">
	<input type="hidden" name="ins_upd_flg" value="<?=$ins_upd_flg?>">
	</form>
</center>
</body>
<? pg_close($con); ?>
</html>
