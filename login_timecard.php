<?
header("Cache-Control: no-cache");
header("Pragma: no-cache");
header("Expires: Thu, 01 Jan 1970 00:00:00 GMT");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix</title>
<?
require("about_postgres.php");
require("show_extension_from_login.ini");
require("show_allot_from_login.ini");
require("show_welfare_from_login.ini");
require("show_qa_from_login.ini");
require("show_stat_from_login.ini");
require("menu_news_from_login.ini");
require("show_reservation_from_login.ini");
require("show_library_from_login.ini");
require("show_ext_search_from_login.ini");
require("show_link_from_login.ini");
require("show_copyright.ini");

$fname = $PHP_SELF;

// データベースに接続
$con = connect2db($fname);

// 環境設定値を取得
$sql = "select * from config";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$interval = pg_fetch_result($sel, 0, "interval");
$login_page_reflesh = ($interval == "t") ? pg_fetch_result($sel, 0, "login_page_reflesh") * 1000 : 0;  // 更新間隔はミリ秒単位に変換
//$timecard_use = pg_fetch_result($sel, 0, "timecard_use");
$timecard_use = "t";
if ($timecard_use == "t") {
	$ret_btn_flg = pg_fetch_result($sel, 0, "ret_btn_flg");
	$timecard_btn_width = ($ret_btn_flg == "f") ? "640" : "700";
	$title = "";
} else {
	$ret_btn_flg = "f";
	$title = pg_fetch_result($sel, 0, "title");
}
/*
$login_pos = pg_fetch_result($sel, 0, "login_pos");
$extension_show_flg = pg_fetch_result($sel, 0, "extension_show_flg");
$allot_show_flg = pg_fetch_result($sel, 0, "allot_show_flg");
$welfare_show_flg = pg_fetch_result($sel, 0, "welfare_show_flg");
$qa_show_flg = pg_fetch_result($sel, 0, "qa_show_flg");
$stat_show_flg = pg_fetch_result($sel, 0, "stat_show_flg");
$info_show_flg = pg_fetch_result($sel, 0, "info_show_flg");
$rsv_show_flg = pg_fetch_result($sel, 0, "rsv_show_flg");
$lib_show_flg = pg_fetch_result($sel, 0, "lib_show_flg");
$ext_search_show_flg = pg_fetch_result($sel, 0, "ext_search_show_flg");
$link_show_flg = pg_fetch_result($sel, 0, "link_show_flg");
$login_color = pg_fetch_result($sel, 0, "login_color");
$tmcd_chg_flg = pg_fetch_result($sel, 0, "tmcd_chg_flg");
*/
$login_pos = "1";
$extension_show_flg = "f";
$allot_show_flg = "f";
$welfare_show_flg = "f";
$qa_show_flg = "f";
$stat_show_flg = "f";
$info_show_flg = pg_fetch_result($sel, 0, "info_show_flg");
$rsv_show_flg = "f";
$lib_show_flg = "f";
$ext_search_show_flg = "f";
$link_show_flg = "f";
$login_color = pg_fetch_result($sel, 0, "login_color");
$tmcd_chg_flg = "t";
$ic_read_flg = pg_fetch_result($sel, 0, "ic_read_flg");

// 文書管理の並び順は更新日時の降順がデフォルト
if ($doc_o == "") {$doc_o = "4";}

// 文書管理の表示件数は7件がデフォルト
if ($doc_limit == "") {$doc_limit = "7";}

// ログインフォームをヘッダに表示する場合、タイムカード機能は使用しない
if ($login_pos == "2") {$timecard_use = "f";}

$onload = "";
//if ($interval == "t" && $login_page_reflesh > 0) {
//	$onload .= "setReload();";
//}
$onload .= "initPage();";
if ($ext_search_show_flg == "t") {
	$onload .= "setAtrbOptions();";
}

if ($login_color == "1") {
	$block_colors["border"] = "#5279a5";
	$block_colors["header"] = "#bdd1e7";
	$block_colors["column"] = "#f6f9ff";
} else {
	$block_colors["border"] = "#0e9c55";
	$block_colors["header"] = "#ceefc2";
	$block_colors["column"] = "#f7fdf2";
}
//タイムカード設定情報
$sql = "select return_icon_flg from timecard";

$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if (pg_num_rows($sel) > 0) {
	//退勤復帰アイコンフラグ 1,"":退勤復帰 2:呼出出勤
	$return_icon_flg = pg_fetch_result($sel, 0, "return_icon_flg");
}
if ($return_icon_flg == "") {$return_icon_flg = "1";}

//アイコン情報
if ($return_icon_flg == "1") {
	$icon_o1_gif = "img/icon-o1.gif";
	$icon_o1_alt = "退復";
	$icon_o2_gif = "img/icon-o2.gif";
	$icon_o2_alt = "復退";
} else {
	$icon_o1_gif = "img/icon-yobishutsu.gif";
	$icon_o1_alt = "呼出";
	$icon_o2_gif = "img/icon-yobitai.gif";
	$icon_o2_alt = "呼退";
	
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
<script type="text/javascript">
<? /*
function rld() {
	location.reload();
}
function setReload() {
	setTimeout('rld()', <? echo($login_page_reflesh); ?>);
}
*/ ?>
function initPage() {
<? //フォーカスフラグ "t"or ""初期
if ($focus_flg != "f") {
?>
	if (document.loginform.id) {
		document.loginform.id.focus();
	}
<?
}
?>
}

function regTimecard(wherefrom) {
	document.loginform.wherefrom.value = wherefrom;
	document.loginform.action = 'clock_in_insert_from_login.php';
	document.loginform.submit();
}

function regPatternSelectTimecard() {
//	document.loginform.wherefrom.value = 1;
	document.loginform.action = 'clock_in_insert_from_login.php';
	document.loginform.submit();
}

function readFelica() {
	var felica_anchor = document.getElementById('felica_anchor');
	if (!felica_anchor) {
		alert('プラグインがインストールされていないため利用できません。');
		return;
	}
	felica_anchor.click();
	var idm = document.loginform.idm.value;

	new Ajax.Request(
		'idm_check.php',
		{
			method: 'post',
			parameters: $H({idm: idm}).toQueryString(),
			onComplete: function (req) {
				var result = eval('('  + req.responseText + ')');
				if (result.id != '') {
					document.loginform.id.value = result.id;
					document.loginform.pass.value = result.pass;
				} else {
					alert('職員情報の読み取りに失敗しました。');
				}
			}
		}
	);
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/layout.css">
<link rel="stylesheet" type="text/css" href="css/font.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-style:none;}
img.close {
	background-image:url("img/icon/plus.gif");
	vertical-align:middle;
}
img.open {
	background-image:url("img/icon/minus.gif");
	vertical-align:middle;
}
.block {border-collapse:collapse; border:<? echo($block_colors["border"]); ?> solid 1px;}
.block .header td {background-color:<? echo($block_colors["header"]); ?>}
.block .column td {background-color:<? echo($block_colors["column"]); ?>}
.block td, .block td .block td {border:<? echo($block_colors["border"]); ?> solid 1px;}
.block td td {border-style:none;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5"<? if ($onload != "") {echo(" onload=\"$onload\"");} ?>>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<form name="loginform" action="main_menu.php" method="post">
<!-- header start -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="200"><img src="img/main_title.gif" width="195" height="51" border="0"></td>
<? if ($login_pos == "2") { ?>
<td align="right">
<table border="0" cellspacing="0" cellpadding="0">
<tr height="40">
<td align="center" bgcolor="#f6f9ff" style="padding-left:25px;padding-right:25px;border:#5279a5 solid 1px;">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">ID：</font></td>
<td><input type="text" name="id" size="14" maxlength="20" style="ime-mode:inactive;" onFocus="document.loginform.focus_flg.value='t';" onBlur="document.loginform.focus_flg.value='f';"></td>
<td width="16"></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">パスワード：</font></td>
<td><input type="password" name="pass" size="14" maxlength="20" style="ime-mode:inactive;"></td>
<td width="16"></td>
<td><input type="submit" value="ログイン"></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
<? } ?>
<?
if ($timecard_use != "t") {
	$width = ($login_pos == "2") ? " width=\"260\"" : "";
	$timecard_link = ($tmcd_chg_flg == "t") ? "<a href=\"login_timecard.php\">タイムカード</a>" : "&nbsp;";
	$now = date("Y") . "年" . intval(date("m")) . "月" . intval(date("d")) . "日（" . get_weekday_string() . "） " . date("H") . "時" . date("i") . "分";
	echo("<td$width>\n");
	echo("<table width=\"100%\" height=\"40\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
	echo("<tr>\n");
	echo("<td align=\"right\" valign=\"top\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\">$timecard_link</font></td>\n");
	echo("</tr>");
	echo("<tr>\n");
	echo("<td align=\"right\" valign=\"bottom\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\">$now</font></td>\n");
	echo("</tr>\n");
	echo("</table>\n");
	echo("</td>\n");

/*
	if ($tmcd_chg_flg == "t") {
		echo("<a href=\"login_timecard.php\">タイムカード</a><br>");
	}
	echo($now);
*/
}
?>
<?
	$login_link = ($tmcd_chg_flg == "t") ? "<a href=\"login.php\">ログイン画面</a>" : "&nbsp;";
	echo("<td>\n");
	echo("<table width=\"100%\" height=\"40\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
	echo("<tr>\n");
	echo("<td align=\"right\" valign=\"top\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\">$login_link</font></td>\n");
	echo("</tr>");
	echo("</table>\n");
	echo("</td>\n");
?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td background="img/line_3.gif"><img src="img/spacer.gif" width="1" height="7" alt=""></td>
</tr>
</table>
<!-- header end -->
<? if ($timecard_use == "t") { ?>
<table align="center" width="700" border="0" cellspacing="0" cellpadding="0">
<tr>
<td height="10"></td>
</tr>
<tr>
<td height="240" align="center" valign="middle" id="bg-time" style="padding:2px 15px 15px 15px;">
<iframe name="clock" src="login_timecard_clock.php" height="210" width="600" scrolling="no" frameborder="0">
</iframe>

</td>
</tr>
</table>
<? } else if ($title != "") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td align="center" style="padding:50px 15px 40px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" color="#5279a5" style="font-size:80px;"><b><? echo($title); ?></b></font></td>
</tr>
</table>
<? } ?>
<? if ($login_pos == "1") { ?>
<table align="center" width="700" border="0" cellspacing="0" cellpadding="0">
<tr>
<td height="5"></td>
</tr>
<tr>
<td height="73" align="center" valign="middle" id="bg-id">
<table width="77%" border="0" cellspacing="0" cellpadding="0">
<tr>
<? if ($ic_read_flg == "t") { ?>
<td>
<input type="button" value="ICカード" onclick="readFelica();">
<span id="FeliCa">
<!--
<a id="felica_anchor" href="felica:">
<input type="hidden" name="idm" />
<polling target="cTafzB6PR9ZGl7MmdfBDm37wfzoBABhLg6mKuzDiXFAH" result="idm" />
</a>
-->
</span>
</td>
<td width="2"></td>
<? } ?>
<td align="right"><span id="font90w">ID：</span></td>
<td><input type="text" name="id" size="14" maxlength="20" style="font-size:1.3em;ime-mode:inactive;" onFocus="document.loginform.focus_flg.value='t';" onBlur="document.loginform.focus_flg.value='f';"></td>
<td width="2"></td>
<td align="right"><span id="font90w">パスワード：</span></td>
<td><input type="password" name="pass" size="14" maxlength="20" style="font-size:1.3em;ime-mode:inactive;"></td>
<!--
<td width="16"></td>
<td><a href="javascript:void(0);" onclick="document.loginform.submit();return false;" onmouseover="document.getElementById('loginimg').src = 'img/icon/login_2.gif';" onmouseout="document.getElementById('loginimg').src = 'img/icon/login_1.gif';" onfocus="document.getElementById('loginimg').src = 'img/icon/login_2.gif';" onblur="document.getElementById('loginimg').src = 'img/icon/login_1.gif';"><img id="loginimg" src="img/icon/login_1.gif" alt="ログイン" width="60" height="60" border="0"></a></td>
-->
</tr>
</table>
</td>
</tr>
</table>
<? } ?>
<input type="hidden" name="wherefrom" value="">
<input type="hidden" name="ret_timecard" value="true">
<input type="hidden" name="doc_o" value="<? echo($doc_o); ?>">
<input type="hidden" name="doc_limit" value="<? echo($doc_limit); ?>">
<input type="hidden" name="select_group_id"  value="">
<input type="hidden" name="select_atdptn_id" value="">
<input type="hidden" name="focus_flg" value="">
<input type="hidden" name="hol_reg_flg" value="">
<input type="hidden" name="conf_ok_flg" value="">
</form>
<? if ($timecard_use == "t") { ?>
<table align="center" width="<? echo($timecard_btn_width); ?>" border="0" cellspacing="0" cellpadding="0">
<tr>
<td height="5"></td>
</tr>
  <tr>
    <td height="99" width="700" align="center" id="bg-icon" nowrap>
    <a href="javascript:regTimecard(1);"><img src="img/icon-b1.gif" alt="出勤" width="95" height="75" border="0"/></a>　<a href="javascript:regTimecard(4);"><img src="img/icon-b2.gif" alt="退勤" width="95" height="75" border="0"/></a>　
<? if ($ret_btn_flg != "t") {
echo("　　　　　");
}
?>
<a href="javascript:regTimecard(2);"><img src="img/icon-g1.gif" alt="外出" width="95" height="75" border="0"/></a>　<a href="javascript:regTimecard(3);"><img src="img/icon-g2.gif" alt="復帰" width="95" height="75" border="0"/></a>
<? if ($ret_btn_flg == "t") { ?>
　<a href="javascript:regTimecard(5);"><img src="<?=$icon_o1_gif?>" alt="<?=$icon_o1_alt?>" width="95" height="75" border="0"/></a>　<a href="javascript:regTimecard(6);"><img src="<?=$icon_o2_gif?>" alt="<?=$icon_o2_alt?>" width="95" height="75" border="0"/></a>
<? } ?>
</tr>
</table>
<? } ?>
<table align="center" border="0" cellpadding="0" cellspacing="0">
<tr valign="top">
<td>
<?
if ($extension_show_flg == "t") {
	show_extension_block($con, $fname);
}
if ($allot_show_flg == "t") {
	show_allot_block($con, $fname);
}
if ($welfare_show_flg == "t") {
	show_welfare_block($con, $fname);
}
if ($qa_show_flg == "t") {
	show_qa_block($con, $qa_category_id, $question_id, $fname);
}
if ($stat_show_flg == "t") {
	show_stat_block($con, $stat_date, $fname);
}
if ($info_show_flg == "t") {
	show_menu_news($con, $fname);
}
if ($rsv_show_flg == "t") {
	show_reservation($con, $fname);
}
if ($lib_show_flg == "t") {
	show_library_list($con, $doc_o, $doc_limit, $fname);
}
?>
</td>
<? if ($ext_search_show_flg == "t" || $link_show_flg == "t") { ?>
<td><img src="img/spacer.gif" alt="" width="10" height="1"></td>
<td>
<?
if ($ext_search_show_flg == "t") {
	show_ext_search_block($con, $fname);
}
if ($link_show_flg == "t") {
	show_link_block($con, $fname);
}
?>
</td>
<? } ?>
</tr>
</table>
<!-- footer start -->
<img src="img/spacer.gif" width="1" height="50" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2"></td>
</tr>
<tr>
<td height="18" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_copyright($con, $fname); ?></font></td>
</tr>
</table>
<!-- footer end -->
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
function get_weekday_string() {
	$w = date("w");
	switch ($w) {
	case 1:
		return "月";
	case 2:
		return "火";
	case 3:
		return "水";
	case 4:
		return "木";
	case 5:
		return "金";
	case 6:
		return "土";
	case 0:
		return "日";
	}
}
?>
