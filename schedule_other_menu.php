<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix スケジュール | 公開スケジュール（日）</title>
<?
$fname = $PHP_SELF;

require("about_session.php");
require("about_authority.php");
require("time_check2.ini");
require("project_check2.ini");
require("show_date_navigation_day.ini");
require("holiday.php");
require("show_schedule_common.ini");
require("show_other_schedule_chart.ini");
require("get_values.ini");
require("label_by_profile_type.ini");
require("show_calendar_memo.ini");

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 2, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// タスク権限を取得
$task = check_authority($session, 30, $fname);
$task_flg = ($task != "0");

// 日付の設定
if ($date == "") {
    $date = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
}
$day = date("d", $date);
$month = date("m", $date);
$year = date("Y", $date);

// 「前月」「前日」「翌日」「翌月」のタイムスタンプを取得
$last_month = get_last_month($date);
$last_day = get_last_day($date);
$next_day = get_next_day($date);
$next_month = get_next_month($date);

// データベースに接続
$con = connect2db($fname);

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$e_id = pg_fetch_result($sel, 0, "emp_id");

// 院内行事チェックボックスの処理
if ($timegd == "on" || $timegd == "off") {
    time_update($con, $e_id, $timegd, $fname);
} else if ($timegd == "") {
    $timegd = time_check($con, $e_id, $fname);
}

// 委員会・WGチェックボックスの処理
if ($pjtgd == "on" || $pjtgd == "off") {
    project_update($con, $e_id, $pjtgd, $fname);
} else if ($pjtgd == "") {
    $pjtgd = project_check($con, $emp_id, $e_id, $fname);
}

// 当日の出勤パターンを取得
$schd_type_name = get_schd_name($con, $emp_id, $e_id, "$year$month$day", $fname);

// スケジュール更新可能フラグを取得
$updatable_flg = get_updatable_flg($con, $emp_id, $e_id, $fname);

// 時間指定のないスケジュールの情報を配列で取得
$timeless_schedules = get_timeless_schedules($con, $emp_id, $pjtgd, $timegd, "$year$month$day", $fname, $e_id);

// 時間指定のあるスケジュール情報を配列で取得
$normal_schedules = get_normal_schedules($con, $emp_id, $e_id, $pjtgd, $timegd, "$year$month$day", $fname, $e_id);

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/popup.js"></script>
<script type="text/javascript">
function showGuide() {
    var timegd = (document.timegd.timeguide.checked) ? 'on' : 'off';
    location.href = 'schedule_other_menu.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo($date); ?>&timegd=' + timegd;
}

function showProject() {
    var pjtgd = (document.timegd.pjtguide.checked) ? 'on' : 'off';
    location.href = 'schedule_other_menu.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo($date); ?>&pjtgd=' + pjtgd;
}

function openPrint() {
    window.open('schedule_print_other_menu.php?&session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo($date); ?>', 'newwin2', 'width=800,height=600,scrollbars=yes');
}

function changeDate(dt) {
    location.href = 'schedule_other_menu.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=' + dt;
}

function popupScheduleDetail(title, place, date, time, type, detail, e) {
    popupDetailBlue(
        new Array(
            'タイトル', title,
            '行先', place,
            '日付', date,
            '時刻', time,
            '種別', type,
            '詳細', detail
        ), 400, 80, e
    );
}

function popupEventDetail(fcl_name, evt_date, evt_time, evt_name, evt_content, evt_contact, e) {
    popupDetailBlue(
        new Array(
            '施設', fcl_name,
            '日付', evt_date,
            '時刻', evt_time,
            '行事名', evt_name,
            '詳細', evt_content,
            '連絡先', evt_contact
        ), 400, 80, e
    );
}

function highlightCells(class_name) {
    changeCellsColor(class_name, '#ffff66');
}

function dehighlightCells(class_name) {
    changeCellsColor(class_name, '');
}

function changeCellsColor(class_name, color) {
    var cells = document.getElementsByTagName('td');
    for (var i = 0, j = cells.length; i < j; i++) {
        if (cells[i].className != class_name) {
            continue;
        }
        cells[i].style.backgroundColor = color;
    }
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>公開スケジュール（日）</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<form name="timegd">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="80" align="center" bgcolor="#5279a5"><a href="schedule_other_menu.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>日</b></font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_other_week_menu.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">週</font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_other_month_menu.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">月</font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_other_year_menu.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo($year); ?>&s_date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">年</font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_other_list_upcoming.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一覧</font></a></td>
<? if ($task_flg) { ?>
<td width="5"></td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_other_task.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">公開タスク</font></a></td>
<? } ?>
<td width="43"></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b><? echo(get_emp_kanji_name($con, $emp_id, $fname)); ?></b></font></td>
<td align="right"><input type="button" value="印刷" onClick="openPrint();"></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td height="22" width="60%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="schedule_other_menu.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo($last_month); ?>">&lt;&lt;前月</a>&nbsp;<a href="schedule_other_menu.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo($last_day); ?>">&lt;前日</a>&nbsp;<select onchange="changeDate(this.options[this.selectedIndex].value);"><? show_date_options_d($date); ?></select>&nbsp;<a href="schedule_other_menu.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo($next_day); ?>">翌日&gt;</a>&nbsp;<a href="schedule_other_menu.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo($next_month); ?>">翌月&gt;</a></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($schd_type_name); ?></font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 院内行事/所内行事
echo ($_label_by_profile["EVENT"][$profile_type]); ?></font><input type="checkbox" name="timeguide" onClick="showGuide();"<? if ($timegd == "on") {echo(" checked");} ?>>&nbsp;<a href="javascript:void(0);" onclick="window.open('schedule_project_setting2.php?session=<? echo($session); ?>&target_emp_id=<? echo($emp_id); ?>', 'regpro', 'width=640,height=700,scrollbars=yes')"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">委員会・WG</font></a><input type="checkbox" name="pjtguide" onClick="showProject();"<? if ($pjtgd == "on") {echo(" checked");} ?>></td>
</tr>
</table>
<?
$start_date = date("Ymd", $date);
// カレンダーのメモを取得
$arr_calendar_memo = get_calendar_memo($con, $fname, $start_date, $start_date);

$holiday = ktHolidayName($date);
if ($holiday != "") {
    $holiday_bgcolor = "#fadede";
} else if ($arr_calendar_memo["{$start_date}_type"] == "5") {
    $holiday_bgcolor = "#defafa";
} else {
    $holiday_bgcolor = "#ffffff";
}

if ($arr_calendar_memo["$start_date"] != "") {
    if ($holiday == "") {
        $holiday = $arr_calendar_memo["$start_date"];
    } else {
        $holiday .= "&nbsp;".$arr_calendar_memo["$start_date"];
    }
}

if ($holiday != "") {
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td height="22" bgcolor="<?=$holiday_bgcolor?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="red"><? echo($holiday); ?></font></td>
</tr>
</table>
<? } ?>
<?
echo("<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"2\">\n");

// 時間指定なし行を表示
show_timeless_schedules($timeless_schedules, $normal_schedules, $updatable_flg, $session, true);

// 時間指定あり行を表示
show_normal_schedules($timeless_schedules, $normal_schedules, $emp_id, $updatable_flg, $session, true);

echo("</table>\n");
?>
</form>
<? /* 凡例非表示
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#bdd1e7"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#bdd1e7"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width=""><? show_schd_type_color_list($con, $fname); ?></td>
<td width="1" bgcolor="#bdd1e7"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#bdd1e7"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
*/ ?>
</center>
<? pg_close($con); ?>
</body>
</html>
