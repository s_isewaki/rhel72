<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務管理 | 時間有休データ移行</title>
<?
//ini_set("display_errors","1");
ini_set("max_execution_time", 0);
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_class_name.ini");
require_once("referer_common.ini");
require_once("work_admin_menu_common.ini");
require_once("timecard_import_common.ini");
require_once("show_select_values.ini");
require_once("timecard_import_menu_common.php");
require_once("atdbk_menu_common.ini");


$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// データベースに接続
$con = connect2db($fname);

// 遷移元の取得
$referer = get_referer($con, $session, "work", $fname);

//削除
if ($mode == "delete") {
    deleteImportSetData($con, $fname, $no);
}

$cnvdata = get_cnvdata($con, $fname, "");

//インポート
if ($mode == "import") {
    // ファイルが正常にアップロードされたかどうかチェック
    $uploaded = false;
    if (array_key_exists("csvfile", $_FILES)) {
        if ($_FILES["csvfile"]["error"] == 0 && $_FILES["csvfile"]["size"] > 0) {
            $uploaded = true;
        }
    }
    if (!$uploaded) {
        echo("<script type=\"text/javascript\">alert('ファイルのアップロードに失敗しました。');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'timecard_import_paid_hol.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }
    // 文字コードの設定
    switch ($encoding) {
        case "1":
            $file_encoding = "SJIS";
            break;
        case "2":
            $file_encoding = "EUC-JP";
            break;
        case "3":
            $file_encoding = "UTF-8";
            break;
        default:
            exit;
    }

    // EUC-JPで扱えない文字列は「・」に変換するよう設定
    mb_substitute_character(0x30FB);
    // トランザクションを開始
    pg_query($con, "begin");

    // CSVデータを配列に格納
    $shifts = array();
    $no = 0;

    // 先頭行フラグ
    $first_flg = true;

    $lines = file($_FILES["csvfile"]["tmp_name"]);
    foreach ($lines as $line) {

        // 文字コードを変換
        $line = trim(mb_convert_encoding($line, "EUC-JP", $file_encoding));

        // EOFを削除
        $line = str_replace(chr(0x1A), "", $line);

        // 先頭行の見出しを読み飛ばす
        if ($first_flg) {
            $first_flg = false;
            //1行目に「氏名」という文字があれば見出しと判断 20110729
            if (strpos($line, "氏名") !== false) {
                $no++;
                continue;
            }
        }

        // 空行は無視
        if ($line == "") {
            continue;
        }

        // カンマで分割し配列へ設定
        $shift_data = split(",", $line);

        //職員IDがない場合は無視
        if ($shift_data[0] == "") {
            continue;
        }

        $shifts[$no] = $shift_data;
        $wk_id = $shift_data[0];

        $no++;
    }

    //処理件数
    $reg_cnt = 0;
    $err_cnt = 0;

    //メッセージ
    $errmsgs = array();
    $errmsgs[] = "インポート処理開始<br>";

    $migration_date = $migration_yr.$migration_mon.$migration_day;
    foreach ($shifts as $no => $shift_data) {

        $rtn_info = update_shift_data($con, $fname, $session, $shift_data, $cnvdata, $migration_date);

        if ($rtn_info["rtncd"] == 0) {
            $reg_cnt++;
        } else {
            $arr_msg = $rtn_info["errmsg"];
            for ($i=0; $i<count($arr_msg); $i++) {
                $wk_no = $no+1;
                $errmsgs[] = "{$wk_no}行目：".$arr_msg[$i]."<br>\n";
            }
            $err_cnt++;
        }
    }

    // トランザクションをコミット
    pg_query($con, "commit");

    $errmsgs[] = "インポート処理終了<br>";
    $errmsgs[] = "正常登録：{$reg_cnt}件<br>";
    $errmsgs[] = "エラー　：{$err_cnt}件<br>";
}

if ($encoding == "") {
    $encoding = "1";
}
//移行日の初期設定
if ($migration_yr == "") {
    $migration_yr = date("Y");
    $migration_mon = date("m");
    $migration_day = date("d");
}

?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">

function checkForm(form) {
	if (form.csvfile.value == '') {
		alert('ファイルを選択してください。');
		return false;
	}
	document.mainform.mode.value = "import";
	return true;
}

//検索表示
function Search() {
	document.mainform.mode.value = "srch";
	document.mainform.submit();

}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse; border:#5279a5 solid 1px;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
.non_in_list {border-collapse:collapse;}
.non_in_list td {border:0px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="work_admin_menu.php?session=<? echo($session); ?>"><img src="img/icon/b23.gif" width="32" height="32" border="0" alt="勤務管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="work_admin_menu.php?session=<? echo($session); ?>"><b>勤務管理</b></a></font></td>
</tr>
</table>
<? } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><img src="img/icon/b07.gif" width="32" height="32" border="0" alt="出勤表"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>出勤表</b></a> &gt; <a href="work_admin_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } ?>
<?
// メニュータブ
show_work_admin_menuitem($session, $fname, "");
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
<tr>
<td>
<?
//インポートメニュータブ表示
show_timecard_import_menuitem($session,$fname);
?>
</td>
</tr>
<tr>
<td><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>

<img src="img/spacer.gif" width="1" height="2" alt=""><br>
<form id="mainform" name="mainform" action="timecard_import_paid_hol_hour.php" method="post" enctype="multipart/form-data" onsubmit="return checkForm(this);">
<font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">外部システムからの時間有休データインポート</font>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="140" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">CSVファイル</font></td>
<td><input name="csvfile" type="file" value="" size="50"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ファイルの文字コード</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="encoding" value="1"<? if ($encoding == "1") {echo(" checked");} ?>>Shift_JIS（Windows/Mac標準）
<input type="radio" name="encoding" value="2"<? if ($encoding == "2") {echo(" checked");} ?>>EUC-JP
<input type="radio" name="encoding" value="3"<? if ($encoding == "3") {echo(" checked");} ?>>UTF-8
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">移行日</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="migration_yr"><? show_select_years(10, $migration_yr, false, true); ?></select>/<select name="migration_mon"><? show_select_months($migration_mon, false) ?></select>/<select name="migration_day"><? show_select_days($migration_day, false); ?></select></font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<?
if ($mode == "import") {
?>
<font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録結果メッセージ<br>
    <?
    foreach ($errmsgs as $errmsg) {
        echo($errmsg);
    }
}
?>
</font>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:void(0);" onclick="document.getElementById('csv_fmt').style.display = (document.getElementById('csv_fmt').style.display == 'none') ? '' : 'none';"><b>CSVのフォーマット</b></a></font>
<div id="csv_fmt" style="display:none">
<dl>
<dt><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"></font></dt>
<dd><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">下記項目をカンマで区切り、レコードごとに改行してください。</font></dd>
<dd>
<ul>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">個人コード（職員ID）</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">氏名（確認用の参考項目です。データ処理には利用しません。）</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">前年繰越時間(HHまたはHH:MM形式)</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">前年取得時間(HHまたはHH:MM形式)</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">当年繰越時間(HHまたはHH:MM形式)</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">当年取得時間(HHまたはHH:MM形式)</font></li>
</ul>
<dl>
<dt><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">例</font></dt>
<dd><font size="3" face="ＭＳ ゴシック, Osaka" class="j12">000001,山田　太郎,7,33,16,24</font></dd>
<dd><font size="3" face="ＭＳ ゴシック, Osaka" class="j12">000008,佐藤　一郎,44,36,6:30,34:03</font></dd>
</dl>
</dd>
</dl>
</div>
<br>
<font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">

個人コード
<input type="text" name="search_emp_id" value="<?=$search_emp_id?>">
氏名
<input type="text" name="search_emp_name" value="<?=$search_emp_name?>">
</font>
<input type="button" value="検索" onClick="Search();">
<br>
<?
if ($mode == "srch") {
    $sql = "select a.*, b.emp_lt_nm, b.emp_ft_nm from timecard_paid_hol_hour_import a left join empmst b on b.emp_personal_id = a.emp_personal_id";
    $cond = "";
    $arr_cond = array();
    if ($search_emp_id != "") {
        $arr_cond[] = "(a.emp_personal_id like '%$search_emp_id%')";
    }
    if ($search_emp_name != "") {
        $wk_search_emp_name = str_replace(" ", "", $search_emp_name);
        $arr_cond[] = "(a.emp_name like '%$wk_search_emp_name%' or b.emp_lt_nm||b.emp_ft_nm like '%$wk_search_emp_name%' or b.emp_kn_lt_nm||b.emp_kn_ft_nm like '%$wk_search_emp_name%')";
    }
    if (count($arr_cond) > 0) {
        $cond .= "where ";
        $cond .= join(" and ", $arr_cond);
    }

    $cond .=" order by a.emp_personal_id";
    $sel=select_from_table($con,$sql,$cond,$fname);
    if($sel==0){
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }
    $num = pg_num_rows($sel);
    if ($num == 0) {
        echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
        echo("検索条件に合うデータはありませんでした。");
        echo("</font>");
    } else {
?>

<table width="670" border="0" cellspacing="0" cellpadding="2" class="list">

<tr>
<td width="80" bgcolor="#f6f9ff"><font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>個人コード</b></font></td>
<td width="140" bgcolor="#f6f9ff"><font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>氏名</b></font></td>
<td width="100" bgcolor="#f6f9ff"><font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>前年繰越時間</b></font></td>
<td width="100" bgcolor="#f6f9ff"><font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>前年取得時間</b></font></td>
<td width="100" bgcolor="#f6f9ff"><font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>当年繰越時間</b></font></td>
<td width="100" bgcolor="#f6f9ff"><font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>当年取得時間</b></font></td>
<td width="50" bgcolor="#f6f9ff"><font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>移行日</b></font></td>
</tr>
        <?
        // 表示処理
        //$data = pg_fetch_all($sel);

        for ($i=0; $i<$num; $i++) {
            $data = pg_fetch_array($sel, $i);
            echo("<tr>\n");
            for ($j=0; $j<7; $j++) {
                echo("<td><font id=\"atdbk\" size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
                //移行日編集
                if ($j==6) {
                    $wk_date = substr($data[$j], 0, 4)."/".substr($data[$j], 4, 2)."/".substr($data[$j], 6, 2);
                    echo($wk_date);
                }
                //氏名
                else if ($j==1) {
                    if ($data[$j] == "") {
                        $wk_emp_name = $data["emp_lt_nm"]." ".$data["emp_ft_nm"];
                    } else {
                        $wk_emp_name = $data[$j];
                    }
                    echo($wk_emp_name);
                }
                //移行日・氏名以外、データ表示
                else {
                    echo($data[$j]);
                }
                echo("</td>\n");
            }

            echo("</tr>\n");

        }
?>

</table>
        <?
    } //end if ($num == 0) else
} //end if ($mode == "srch")
?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="back" value="t">
<input type="hidden" name="mode" value="">
<input type="hidden" name="no" value="">

</form>
</td>
</tr>
</table>


</body>
<? pg_close($con); ?>
</html>

<?
//データを登録
function update_shift_data($con, $fname, $session, $shift_data, $cnvdata, $migration_date)
{

    $arr_msg = array();
    $rtn_info = array();
    $rtn_info["rtncd"] = 0;

    //入力データチェック
    //カラム数チェック
    $column_count = 6;
    if (count($shift_data) != $column_count) {
        $rtn_info["rtncd"] = 1;
        $arr_msg[] = "カラム数が不正です。".join(",", $shift_data);
        $rtn_info["errmsg"] = $arr_msg;
        return $rtn_info;
    }

    //データ確認
    $emp_personal_id = $shift_data[0];
    //職員ID確認
    $sql = "select a.emp_id from empmst a ";
    $cond = "where a.emp_personal_id = '$emp_personal_id' ";
    $sel = select_from_table($con,$sql,$cond,$fname);
    if($sel == 0){
        pg_query($con,"rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $cnt = pg_numrows($sel);
    if ($cnt == 0) {
        $rtn_info["rtncd"] = 1;
        $arr_msg[] = "職員($emp_personal_id {$shift_data[1]})がマスタに登録されていません。";
    } else {
        $wk_emp_id = pg_fetch_result($sel, 0, "emp_id");
    }
    // 前年繰越
    if ($shift_data[2] != "") {
        if (preg_match("/^[0-9]*$|^\d+\:?\d+$/", $shift_data[2]) == 0 || strlen($shift_data[2]) > 5) { //HH:MM形式も含む 20141014
            $rtn_info["rtncd"] = 1;
            $arr_msg[] = "前年繰越時間({$shift_data[2]})が正しくありません。";
        }
    }
    // 前年取得
    if ($shift_data[3] != "") {
        if (preg_match("/^[0-9]*$|^\d+\:?\d+$/", $shift_data[3]) == 0 || strlen($shift_data[3]) > 5) {
            $rtn_info["rtncd"] = 1;
            $arr_msg[] = "前年取得時間({$shift_data[3]})が正しくありません。";
        }
    }
    // 当年繰越
    if ($shift_data[4] != "") {
        if (preg_match("/^[0-9]*$|^\d+\:?\d+$/", $shift_data[4]) == 0 || strlen($shift_data[4]) > 5) {
            $rtn_info["rtncd"] = 1;
            $arr_msg[] = "当年繰越時間({$shift_data[4]})が正しくありません。";
        }
    }
    // 当年取得
    if ($shift_data[5] != "") {
        if (preg_match("/^[0-9]*$|^\d+\:?\d+$/", $shift_data[5]) == 0 || strlen($shift_data[5]) > 5) {
            $rtn_info["rtncd"] = 1;
            $arr_msg[] = "当年取得時間({$shift_data[5]})が正しくありません。";
        }
    }
    //エラー時
    if ($rtn_info["rtncd"] == 1) {
        $rtn_info["errmsg"] = $arr_msg;
        return $rtn_info;
    }

    $sql = "delete from timecard_paid_hol_hour_import ";
    $cond = "where emp_personal_id = '$emp_personal_id' ";
    $del = delete_from_table($con, $sql, $cond, $fname);
    if ($del == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    //追加
    $sql = "insert into timecard_paid_hol_hour_import (emp_personal_id, emp_name, last_carry, last_use, curr_carry, curr_use,  data_migration_date ";
    $sql .= ") values (";
    $shift_data[6] = $migration_date;
    $content = $shift_data;
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) {
        pg_query($con,"rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    return $rtn_info;
}


?>
