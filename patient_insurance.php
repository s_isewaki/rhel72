<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 患者管理｜健康保険</title>
<?
require('./about_session.php');
require('./about_authority.php');
require('./about_postgres.php');
require('./show_select_values.ini');
require('./get_values.ini');
require('./conf/sql.inf');
require('label_by_profile_type.ini');
require('show_select_years_wareki.ini');

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == '0') {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 患者管理各権限の取得
$ptreg   = check_authority($session, 22, $fname);
$ptif    = check_authority($session, 15, $fname);
$outreg  = check_authority($session, 35, $fname);
$inout   = check_authority($session, 33, $fname);
$disreg  = check_authority($session, 36, $fname);
$dishist = check_authority($session, 34, $fname);

// 患者参照権限のチェック
if ($ptif == '0') {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 診療科登録権限を取得
$section_admin_auth = check_authority($session, 28, $fname);

// 患者管理管理者権限を取得
$patient_admin_auth = check_authority($session, 77, $fname);

// DBに接続
$con = connect2db($fname);

$profile_type = get_profile_type($con, $fname);

// 登録種別判定(default == 'update')
$sql_type = 'update';

// 患者氏名取得
$cond = "where ptif_id = '" . $pt_id . "' and ptif_del_flg = 'f' order by ptif_id";
$sel  = select_from_table($con, $SQL85, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$pt_name     = pg_result($sel, 0, 'ptif_lt_kaj_nm') . ' ' . pg_result($sel, 0, 'ptif_ft_kaj_nm');

// 健康保険情報取得
$sql  = 'select * from ptinsurance';
$cond = "where ptif_id = '" . $pt_id . "'";
$sel  = select_from_table($con, $sql, $cond, $fname);
if (!pg_result($sel, 0, 'ptif_id')) {
    $sql_type = 'insert';
}

if ($sql_type == 'insert') {
    $isr_code  = '';
    $isr_id    = '';
    $relation  = '';
    $ch_rate   = '0';
    $kf_date   = '';
    $kf_date_y = '';
    $kf_date_m = '';
    $kf_date_d = '';
    $st_date   = '';
    $st_date_y = '';
    $st_date_m = '';
    $st_date_d = '';
    $wf1       = '1';
    $wf2       = '1';
    $psn_id    = '';
    $psn_nm    = '';
    $ch_psn_id = '';
    $rsp_id    = '';
}
if ($sql_type == 'update') {
    $isr_code  = trim(pg_result($sel, 0, 'insurance_code'));
    $isr_id    = trim(pg_result($sel, 0, 'insurance_id'));
    $relation  = trim(pg_result($sel, 0, 'relation'));
    $ch_rate   = trim(pg_result($sel, 0, 'charge_rate'));
    if ($ch_rate == '') {
        $ch_rate = '0';
    }
    $kf_date   = trim(pg_result($sel, 0, 'iss_date'));
    $kf_date_y = substr($kf_date, 0, 4);
    $kf_date_m = substr($kf_date, 4, 2);
    $kf_date_d = substr($kf_date, 6, 2);
    $st_date   = trim(pg_result($sel, 0, 'acq_date'));
    $st_date_y = substr($st_date, 0, 4);
    $st_date_m = substr($st_date, 4, 2);
    $st_date_d = substr($st_date, 6, 2);
    $wf1       = trim(pg_result($sel, 0, 'wareki_flg1'));
    $wf2       = trim(pg_result($sel, 0, 'wareki_flg2'));
    $psn_id    = trim(pg_result($sel, 0, 'isr_person_id'));
    $psn_nm    = trim(pg_result($sel, 0, 'isr_person_name'));
    $ch_psn_id = trim(pg_result($sel, 0, 'chr_person_id'));
    $rsp_id    = trim(pg_result($sel, 0, 'recipient_id'));
}

// 初期表示時は和暦
$wareki_flg1 = '1';
$wareki_flg2 = '1';

if ($wf1 != '' && $wf1 == '0') {
    $wareki_flg1 = '0';
}
if ($wf2 != '' && $wf2 == '0') {
    $wareki_flg2 = '0';
}

// 検索フォームからの引継ぎ値をurlencode
$url_key1 = urlencode($key1);
$url_key2 = urlencode($key2);

?>
<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript">
function wf_change1()
{
    if (document.patient.wareki_flg1.checked == true) {
        yr_change1('a');
        document.getElementById('kf_date_y_div').style.display  = 'none';
        document.getElementById('kf_date_wy_div').style.display = '';
    } else {
        document.getElementById('kf_date_y_div').style.display  = '';
        document.getElementById('kf_date_wy_div').style.display = 'none';
    }
}
function wf_change2()
{
    if (document.patient.wareki_flg2.checked == true) {
        yr_change1('b');
        document.getElementById('st_date_y_div').style.display  = 'none';
        document.getElementById('st_date_wy_div').style.display = '';
    } else {
        document.getElementById('st_date_y_div').style.display  = '';
        document.getElementById('st_date_wy_div').style.display = 'none';
    }
}
<?
// 改元データを取得
$era_data    = get_era_data();
$era_ymd_str = '';
$gengo_str   = '';
for ($i = 1; $i < count($era_data); ++$i) {
    if ($i > 1) {
        $era_ymd_str .= ',';
        $gengo_str   .= ',';
    }
    $split_data   = split(',', $era_data[$i]);
    $era_ymd_str .= "'" . $split_data[0] . "'";
    $gengo_str   .= "'" . $split_data[1] . "'";
}
?>
var ymd_arr = new Array(<?=$era_ymd_str?>);
var gengo_arr = new Array(<?=$gengo_str?>);

function yr_change1(str)
{
    if (str == 'a') {
        var year = document.patient.kf_date_y.value;
        var mon  = document.patient.kf_date_m.value;
        var day  = document.patient.kf_date_d.value;
        var ymd  = year + mon + day;

        find_idx = -1;
        for (var i = 0; i < ymd_arr.length; ++i) {
            if (year == ymd_arr[i].substring(0, 4)) {
                find_idx = i;
                break;
            }
        }

        document.patient.kf_date_wy.value = year;
        if (find_idx > -1 && ymd < ymd_arr[find_idx]) {
            document.patient.kf_date_wy.selectedIndex += 1;
        }
    }
    if (str == 'b') {
        var year = document.patient.st_date_y.value;
        var mon  = document.patient.st_date_m.value;
        var day  = document.patient.st_date_d.value;
        var ymd  = year + mon + day;

        find_idx = -1;
        for (var i = 0; i < ymd_arr.length; ++i) {
            if (year == ymd_arr[i].substring(0, 4)) {
                find_idx = i;
                break;
            }
        }

        document.patient.st_date_wy.value = year;
        if (find_idx > -1 && ymd < ymd_arr[find_idx]) {
            document.patient.st_date_wy.selectedIndex += 1;
        }
    }
}

function yr_change2(str)
{
    if (str == 'a') {
        document.patient.kf_date_y.value = document.patient.kf_date_wy.value;
    }
    if (str == 'b') {
        document.patient.st_date_y.value = document.patient.st_date_wy.value;
    }
}

function check_wareki1()
{
    if (document.patient.wareki_flg1.checked == false) {
        return true;
    }

    var year = document.patient.kf_date_y.value;
    var mon  = document.patient.kf_date_m.value;
    var day  = document.patient.kf_date_d.value;
    var ymd  = year + mon + day;

    find_idx = -1;
    for (var i = 0; i < ymd_arr.length; ++i) {
        ymp_tmp = ymd_arr[i].substring(0, 4);
        if (year == ymp_tmp) {
            find_idx = i;
            break;
        }
    }

    if (find_idx == -1) {
        return true;
    }

    if (ymd < ymd_arr[find_idx]) {
        gengo = gengo_arr[find_idx + 1];
    } else {
        gengo = gengo_arr[find_idx];
    }

    obj = document.getElementById('kf_date_wy');
    var wareki_tmp = obj.options[obj.selectedIndex].text;
    select_gengo   = wareki_tmp.substring(0, 2);
    if (select_gengo != gengo) {
        alert('年月日を正しく指定してください');
        return false;
    }
    return true;
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="wf_change1();wf_change2();">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr bgcolor="#f6f9ff">
    <td width="32" height="32" class="spacing"><a href="patient_info_menu.php?session=<? echo($session); ?>"><img src="img/icon/b16.gif" width="32" height="32" border="0" alt="<? echo($_label_by_profile["PATIENT_MANAGE"][$profile_type]); ?>"></a></td>
    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="patient_info_menu.php?session=<? echo($session); ?>"><b><? echo($_label_by_profile["PATIENT_MANAGE"][$profile_type]); ?></b></a></font></td>
<? if ($section_admin_auth == '1') { ?>
    <td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="entity_menu.php?session=<? echo($session); ?>&entity=2"><b>管理画面へ</b></a></font></td>
<? } else if ($patient_admin_auth == '1') { ?>
    <td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="bed_check_setting.php?session=<? echo($session); ?>&entity=2"><b>管理画面へ</b></a></font></td>
<? } ?>
  </tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr height="22">
    <td width="70" align="center" bgcolor="#bdd1e7"><a href="patient_info_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">検索</font></a></td>
<? if ($ptif == 1) { ?>
    <td width="5">&nbsp;</td>
    <td width="70" align="center" bgcolor="#bdd1e7"><a href="patient_detail.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&page=<? echo($page); ?>&key1=<? echo($key1); ?>&key2=<? echo($ekey2); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基本</font></a></td>
    <td width="5">&nbsp;</td>
    <td width="85" align="center" bgcolor="#5279a5"><a href="patient_insurance.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo($url_key1); ?>&key2=<? echo($url_key2); ?>&page=<? echo($page); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>健康保険</b></font></a></td>
    <td width="5">&nbsp;</td>
    <td width="95" align="center" bgcolor="#bdd1e7"><a href="patient_genogram.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">家族構成図</font></a></td>
    <td width="5">&nbsp;</td>
    <td width="95" align="center" bgcolor="#bdd1e7"><a href="patient_sub_setting.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo($url_key1); ?>&key2=<? echo($url_key2); ?>&page=<? echo($page); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基礎データ</font></a></td>
    <td width="5">&nbsp;</td>
    <td width="75" align="center" bgcolor="#bdd1e7"><a href="patient_contact_setting.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo($url_key1); ?>&key2=<? echo($url_key2); ?>&page=<? echo($page); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">連絡先</font></a></td>
    <td width="5">&nbsp;</td>
    <td width="70" align="center" bgcolor="#bdd1e7"><a href="patient_claim_setting.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo($url_key1); ?>&key2=<? echo($url_key2); ?>&page=<? echo($page); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">請求</font></a></td>
<? } ?>
<? if ($inout == 1) { ?>
    <td width="5">&nbsp;</td>
    <td width="110" align="center" bgcolor="#bdd1e7"><a href="inoutpatient_detail.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo($url_key1); ?>&key2=<? echo($url_key2); ?>&page=<? echo($page); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($_label_by_profile["IN_OUT"][$profile_type]); ?></font></a></td>
<? } ?>
<? if ($dishist == 1) { ?>
    <td width="5">&nbsp;</td>
    <td width="125" align="center" bgcolor="#bdd1e7"><a href="disease_detail.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo($url_key1); ?>&key2=<? echo($url_key2); ?>&page=<? echo($page); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">プロブレムリスト</font></a></td>
<? } ?>
<? if ($ptif == 1) { ?>
    <td width="5">&nbsp;</td>
    <td width="135" align="center" bgcolor="#bdd1e7"><a href="patient_karte.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo($url_key1); ?>&key2=<? echo($url_key2); ?>&page=<? echo($page); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($_label_by_profile["KARTE_NO"][$profile_type]); ?></font></a></td>
<? } ?>
    <td width="">&nbsp;</td>
  </tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
  </tr>
</table>

<img src="img/spacer.gif" alt="" width="1" height="5"><br>

<form action="patient_insurance_update.php" method="post" name="patient" onsubmit="return check_wareki1();">

<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
  <tr height="22">
    <td width="24%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($_label_by_profile['PATIENT_ID'][$profile_type]); ?></font></td>
    <td width="30%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($pt_id); ?></font></td>
    <td width="16%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($_label_by_profile['PATIENT_NAME'][$profile_type]); ?></font></td>
    <td width="30%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($pt_name); ?></font></td>
  </tr>
</table>

<img src="img/spacer.gif" alt="" width="1" height="5"><br>

<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
  <tr height="22">
    <td width="24%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">交付日</font></td>
    <td colspan="3">
      <span id="kf_date_y_div">
      <select name="kf_date_y" onchange="yr_change1('a');">
<? show_select_years(120, $kf_date_y, true);?>
      </select>
      </span>
      <span id="kf_date_wy_div">
      <select id="kf_date_wy" name="kf_date_wy" onchange="yr_change2('a');">
<? show_select_years_jp(120, $kf_date_y, '', '', true); ?>
      </select>
      </span>
      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">/</font>
      <select name="kf_date_m">
<? show_select_months($kf_date_m, true); ?>
      </select>
      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">/</font>
      <select name="kf_date_d">
<? show_select_days($kf_date_d, true); ?>
      </select>
      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
      <input type="checkbox" name="wareki_flg1" value="1" <? if ($wareki_flg1 == '1') { echo('checked'); }?> onclick="wf_change1();">和暦</font>
    </td>
  </tr>
  <tr height="22">
    <td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">記号・番号</font></td>
    <td colspan="3">
      <input name="isr_code" type="text" value="<? echo $isr_code; ?>" style="ime-mode:inactive;">
      &nbsp;&nbsp;
      <input name="isr_id" type="text" value="<? echo $isr_id; ?>" style="ime-mode:inactive;">
    </td>
  </tr>
  <tr height="22">
    <td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">続柄</font></td>
    <td colspan="3">
      <input name="relation" type="radio" value="1" <? if ($relation == '1') { echo('checked'); }?>>
      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">本人</font>
      &nbsp;&nbsp;
      <input name="relation" type="radio" value="2" <? if ($relation == '2') { echo('checked'); }?>>
      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">家族</font>
    </td>
  </tr>
  <tr height="22">
    <td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">自己負担割合</font></td>
    <td colspan="3">
      <select name="ch_rate">
        <option value="0" <? if ($ch_rate == '0') { echo('selected'); }?>>
        <option value="1" <? if ($ch_rate == '1') { echo('selected'); }?>>１割
        <option value="2" <? if ($ch_rate == '2') { echo('selected'); }?>>２割
        <option value="3" <? if ($ch_rate == '3') { echo('selected'); }?>>３割
      </select>
    </td>
  </tr>
  <tr height="22">
    <td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">資格取得日</font></td>
    <td colspan="3">
      <span id="st_date_y_div">
      <select name="st_date_y" onchange="yr_change1('b');">
<? show_select_years(120, $st_date_y, true);?>
      </select>
      </span>
      <span id="st_date_wy_div">
      <select id="st_date_wy" name="st_date_wy" onchange="yr_change2('b');">
<? show_select_years_jp(120, $st_date_y, '', '', true); ?>
      </select>
      </span>
      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">/</font>
      <select name="st_date_m">
<? show_select_months($st_date_m, true); ?>
      </select>
      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">/</font>
      <select name="st_date_d">
<? show_select_days($st_date_d, true); ?>
      </select>
      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
      <input type="checkbox" name="wareki_flg2" value="1" <? if ($wareki_flg2 == '1') { echo('checked'); }?> onclick="wf_change2();">和暦</font>
    </td>
  </tr>
  <tr height="22">
    <td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">保険者番号</font></td>
    <td colspan="3"><input type="text" name="psn_id" value="<?=$psn_id?>" maxlength="8" style="ime-mode:disabled;"></td>
  </tr>
  <tr height="22">
    <td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">保険者名称</font></td>
    <td colspan="3"><input type="text" name="psn_nm" value="<? echo $psn_nm; ?>" style="width:60%; ime-mode:active;"></td>
  </tr>
  <tr height="22">
    <td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">公費負担者番号</font></td>
    <td colspan="3"><input type="text" name="ch_psn_id" value="<? echo $ch_psn_id; ?>" maxlength="8" style="ime-mode:disabled;"></td>
  </tr>
  <tr height="22">
    <td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">受給者番号</font></td>
    <td colspan="3"><input type="text" name="rsp_id" value="<? echo $rsp_id; ?>" maxlength="7" style="ime-mode:disabled;"></td>
  </tr>
</table>

<? if ($ptreg == 1) { ?>
<table width="600" border="0" cellspacing="0" cellpadding="2">
  <tr height="22">
    <td align="right"><input type="submit" value="更新"></td>
  </tr>
</table>
<? } ?>

<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="pt_id" value="<? echo($pt_id); ?>">
<input type="hidden" name="key1" value="<? echo($key1); ?>">
<input type="hidden" name="key2" value="<? echo($key2); ?>">
<input type="hidden" name="page" value="<? echo($page); ?>">
<input type="hidden" name="sql_type" value="<? echo($sql_type); ?>">

</form>

</td>
</tr>
</table>

</body>
<? pg_close($con); ?>
</html>
