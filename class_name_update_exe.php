<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_authority.php");
require("about_session.php");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 組織登録権限チェック
$checkauth = check_authority($session, 23, $fname);
if ($auth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if ($class_nm == "") {
	echo("<script type=\"text/javascript\">alert('階層1を入力してください。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if (strlen($class_nm) > 8) {
	echo("<script type=\"text/javascript\">alert('階層1が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if ($atrb_nm == "") {
	echo("<script type=\"text/javascript\">alert('階層2を入力してください。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if (strlen($atrb_nm) > 8) {
	echo("<script type=\"text/javascript\">alert('階層2が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if ($dept_nm == "") {
	echo("<script type=\"text/javascript\">alert('階層3を入力してください。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if (strlen($dept_nm) > 8) {
	echo("<script type=\"text/javascript\">alert('階層3が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if ($class_cnt == 3) {
	$room_nm = null;
} else if ($class_cnt == 4) {
	if ($room_nm == "") {
		echo("<script type=\"text/javascript\">alert('階層4を入力してください。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}
	if (strlen($room_nm) > 8) {
		echo("<script type=\"text/javascript\">alert('階層4が長すぎます。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}
} else {
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 組織名テーブルを更新
$sql = "update classname set";
$set = array("class_nm", "atrb_nm", "dept_nm", "room_nm", "class_cnt");
$setvalue = array($class_nm, $atrb_nm, $dept_nm, $room_nm, $class_cnt);
$cond = "";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 3階層の場合、4階層目のデータを削除
if ($class_cnt == 3) {
	$sql = "delete from classroom";
	$cond = "";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$sql = "update empmst set";
	$set = array("emp_room");
	$setvalue = array(null);
	$cond = "";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$sql = "update duty_shift_sync_link set";
	$set = array("room_id");
	$setvalue = array(null);
	$cond = "";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// データベース接続を閉じる
pg_close($con);

// 組織名画面をリフレッシュ
echo("<script type=\"text/javascript\">location.href = 'class_name.php?session=$session';</script>");
?>
