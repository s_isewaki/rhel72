<?php
//====================================================================
// 勤怠管理
// SFC CSVデータ自動出力 php
//====================================================================
echo("SFC CSV Auto Output [START]\n");

// 引数の取得
$out_dir = $argv[1];	// 出力ファイル格納ディレクトリ
$sfc_set_dir = $argv[2];	// 変換用ファイル設定ディレクトリ
$sfc_group_code = $argv[3];	// 連携用病棟コード 'all' or '01,02'
$yyyymm = $argv[4];		// 年月

// ディレクトリの存在を確認
if ( !is_dir($out_dir) ) {
    echo "out_dir is not found: $out_dir\n" ;
    exit;
}
if ( $yyyymm == "" ) {
	 $yyyymm = date("Ym");
}
$duty_yyyy = substr($yyyymm, 0, 4);
$duty_mm = intval(substr($yyyymm, 4, 2));
// タイムレコーダ連携ライブラリの読み込み
ob_start();
require_once('about_postgres.php');
require_once("duty_shift_diary_sfc_class.php");
ob_end_clean();

//プログラム名
$fname = $argv[0];
//ＤＢのコネクション作成
$con = connect2db($fname);
//利用するCLASS
$obj_sfc = new duty_shift_diary_sfc_class($con, $fname);

//group_id、SFCグループコードを取得
$sql = "select a.* from duty_shift_group a ";
if ($sfc_group_code == "all") {
	$cond = "where a.sfc_group_code != '' order by a.sfc_group_code, a.order_no";
} else {
	$code_str = "";
	$arr_sfc_cd = split(",", $sfc_group_code);
	for ($i=0; $i<count($arr_sfc_cd); $i++) {
		if ($code_str != "") {
			$code_str .= ",";
		}
		$code_str .= "'".$arr_sfc_cd[$i]."'";
	}
    $cond = "where a.sfc_group_code in ($code_str) order by a.sfc_group_code, a.order_no";
}
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo "  db select error\n";
	exit;
}
$num = pg_numrows($sel);
if ($num == 0) {
	pg_close($con);
	echo "  not found sfc_group_code\n";
	exit;
}
$arr_group = array();
for ($i=0; $i<$num; $i++) {
	$arr_group[$i]["group_id"] = pg_fetch_result($sel, $i, "group_id");
	$arr_group[$i]["sfc_group_code"] = pg_fetch_result($sel, $i, "sfc_group_code");
}
//sfc情報、sfcコード毎にgroup_idが複数ある場合に対応
//sfcコード毎の件数確認
$sql = "select a.sfc_group_code, count(*) as cnt from duty_shift_group a ";
if ($sfc_group_code == "all") {
    $cond = "where a.sfc_group_code != '' ";
} else {
    $code_str = "";
    $arr_sfc_cd = split(",", $sfc_group_code);
    for ($i=0; $i<count($arr_sfc_cd); $i++) {
        if ($code_str != "") {
            $code_str .= ",";
        }
        $code_str .= "'".$arr_sfc_cd[$i]."'";
    }
    $cond = "where a.sfc_group_code in ($code_str) ";
}
$cond .= " group by a.sfc_group_code order by a.sfc_group_code";

$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo "  db select error\n";
    exit;
}
$num = pg_numrows($sel);
if ($num == 0) {
    pg_close($con);
    echo "  not found sfc_group_code\n";
    exit;
}
$group_idx = 0;
$arr_sfc_info = array();
for ($i=0; $i<$num; $i++) {
    $arr_sfc_info[$i]["sfc_group_code"] = pg_fetch_result($sel, $i, "sfc_group_code");
    $group_cnt = pg_fetch_result($sel, $i, "cnt");
    $arr_sfc_info[$i]["group_cnt"] = $group_cnt;
    //SFCコード毎のgroup_idを設定
    $group_info = array();
    $group_info_idx = 0;
    for ($j=$group_idx; $j<$group_idx+$group_cnt; $j++) {
        $group_info[$group_info_idx]["group_id"] = $arr_group[$j]["group_id"];
        $group_info_idx++;
    }
    $arr_sfc_info[$i]["group_info"] = $group_info;
    $group_idx += $group_cnt;
}

//変換用ファイルを取得する
$csv_file_name = $sfc_set_dir."/cmxsfc-convert.txt";
$arr_convert_id = $obj_sfc->get_convert_id($csv_file_name);

for ($i=0; $i<$num; $i++) {
    //グループが1件の場合
    if ($arr_sfc_info[$i]["group_cnt"] == 1) {
        $wk_group_id = $arr_sfc_info[$i]["group_info"][0]["group_id"];
        $group_array = $obj_sfc->get_group_info($wk_group_id);
        // シフトグループに所属する職員を取得
        $arr_emp_info = $obj_sfc->get_emp_info_for_sfc($wk_group_id, $duty_yyyy, $duty_mm, $group_array);
        
        // SFCデータを取得
        $arr_data = $obj_sfc->get_sfc_csv_data($wk_group_id, $duty_yyyy, $duty_mm, $arr_emp_info, $group_array, "1");
    }
    //グループが複数の場合
    else {
        
        $arr_emp_infos = array();
        $arr_datas = array();
        //グループ数繰り返し
        $wk_cnt = $arr_sfc_info[$i]["group_cnt"];
        for ($j=0; $j<$wk_cnt; $j++) {
            $wk_group_id = $arr_sfc_info[$i]["group_info"][$j]["group_id"];
            $group_array = $obj_sfc->get_group_info($wk_group_id);
            
            $arr_emp_infos[$j] = $obj_sfc->get_emp_info_for_sfc($wk_group_id, $duty_yyyy, $duty_mm, $group_array);
            $arr_datas[$j] = $obj_sfc->get_sfc_csv_data($wk_group_id, $duty_yyyy, $duty_mm, $arr_emp_infos[$j], $group_array, "1");
        }
        //複数配列からemp_personal_idの出現順に並べる
        $arr_emp_no = array(); //職員IDの位置
        $arr_emp_info = array();
        $emp_idx = 0;
        for ($j=0; $j<$wk_cnt; $j++) {
            
            $arr_emp_info_t = $arr_emp_infos[$j];
            for ($k=0; $k<count($arr_emp_info_t); $k++) {
                $emp_personal_id = $arr_emp_info_t[$k]["emp_personal_id"];
                if ($arr_emp_no[$emp_personal_id] == "") {
                    $arr_emp_info[$emp_idx] = $arr_emp_info_t[$k];
                    $arr_emp_no[$emp_personal_id] = $emp_idx + 1;
                    $emp_idx++;
                }
            }
        }
        //データ数確認、0:配列全てコピー,1以上:1日毎に確認後コピー
        $arr_data = array();
        for ($j=0; $j<$wk_cnt; $j++) {
            //
            $arr_data_t = $arr_datas[$j];
            //位置確認後に配列へ設定
            foreach ($arr_data_t as $t_idx => $data_t) {
                $wk_emp_personal_id = $arr_data_t[$t_idx]["emp_personal_id"];
                $wk_idx = $arr_emp_no[$wk_emp_personal_id] - 1;
                //件数確認
                if (count($arr_data[$wk_idx]) == 0) {
                    $arr_data[$wk_idx] = $arr_data_t[$t_idx];
                }
                else {
                    //日ごとのデータを確認して設定
                    for ($k=1; $k<=31; $k++) {
                        if (($arr_data[$wk_idx][$k] == "0000" ||
                             $arr_data[$wk_idx][$k] == "") &&
                                $arr_data_t[$t_idx][$k] != "0000") {
                            $arr_data[$wk_idx][$k] = $arr_data_t[$t_idx][$k];
                        }
                    }
                }
            }
        }
       
    }

	// 情報をCSV形式で取得
	$csv = get_list_csv($con, $session, $arr_emp_info, $arr_data, $arr_convert_id);
	// CSVを出力
	if ($csv != "") {
		$month = sprintf("%02d", $duty_mm);
        $file_name = "KINJ$duty_yyyy$month".$arr_sfc_info[$i]["sfc_group_code"].".txt";
		$fp = fopen($out_dir."/".$file_name, "w");
		if (!$fp) {
			pg_close($con);
			echo " fopen error\n";
			exit;
		} else {
			if(!fwrite($fp, $csv, 2000000)) {
				pg_close($con);
				fclose($fp);
				echo " fwrite error\n";
				exit;
			}
		}
		fclose($fp);
	}
	
}

//ＤＢのコネクションクローズ
pg_close($con);

echo("SFC CSV Auto Output [END]\n");

/**
 * 管理日誌（SFC形式）情報をCSV形式で取得
 *
 * @param mixed $con DBコネクション
 * @param mixed $session セッション
 * @param mixed $arr_emp_info 職員情報
 * @param mixed $arr_data 職員、日別のSFCコードの配列
 * @param mixed $arr_convert_id 職員ID変換用配列
 * @return mixed CSV形式の管理日誌（SFC形式）情報
 *
 */
function get_list_csv($con, $session, $arr_emp_info, $arr_data, $arr_convert_id) {

	$buf = "";

	for($i=0;$i<count($arr_emp_info);$i++){
		//職員ID
		//変換用ファイルにIDがある場合は、SFC職員コードを設定
		$wk_emp_id = $arr_emp_info[$i]["emp_personal_id"];
		if ($arr_convert_id[$wk_emp_id] != "") {
			$wk_emp_id = $arr_convert_id[$wk_emp_id];
		} else {
			//ない場合は、"99"+職員IDの下6桁
			$wk_right_id = substr("000000".$wk_emp_id, -6);
			$wk_emp_id = "99".$wk_right_id;
		}
		$buf .= $wk_emp_id;
		for ($j=1;$j<=31;$j++) {
			$sfc_code = $arr_data[$i][$j];
			if ($sfc_code == "") {
				$sfc_code = "0000";
			}
			$buf .= ",".$sfc_code;
		}
		$buf .= "\r\n";
	}
	return mb_convert_encoding($buf, "SJIS", "EUC-JP");

}
