<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix タイムカード | 代行入力</title>
<?
require_once("about_session.php");
require_once("about_postgres.php");
require_once("show_attendance_pattern.ini");
require_once("show_select_values.ini");
require_once("get_values.ini");
require_once("atdbk_common_class.php");
require_once("html_utils.php");
require_once("timecard_common_class.php");
require_once("get_values.ini");
require_once("show_timecard_common.ini");
require_once("timecard_bean.php");
require_once("timecard_paid_hol_hour_class.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 日付の設定
$year = substr($date, 0, 4);
$month = substr($date, 4, 2);
$day = substr($date, 6, 2);
$int_month = intval($month);
$int_day = intval($day);

// データベースに接続
$con = connect2db($fname);

//出勤表関連共通クラス
$atdbk_common_class = new atdbk_common_class($con, $fname);
//javascript出力
$atdbk_common_class->setJavascript();

$timecard_common_class = new timecard_common_class($con, $fname, $emp_id, $date, $date);

//タイムカード情報の取得
$timecard_bean = new timecard_bean();
$timecard_bean->select($con, $fname);

//時間有休
$obj_hol_hour = new timecard_paid_hol_hour_class($con, $fname);
$obj_hol_hour->select();

// 職員氏名を取得
$emp_name = get_emp_kanji_name($con, $emp_id, $fname);

// エラーで戻ったか判定
$err_back_page_flg = ($back == "t");

// 勤務日種別を取得
$sql = "select type from calendar";
$cond = "where date = '$date'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$type = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "type") : "";

// 勤務日種別名称の設定
if ($type == "") {
	$type_name = "";
} else if ($type == "1") {
	$type_name = "通常勤務日";
} else if ($type == "6") {
	$type_name = get_holiday_name($date);
} else if ($type == "7") {
	$type_name = "年末年始休暇";
} else {
	switch ($type) {
	case "2":
		$fld = "day4";
		$default = "勤務日1";
		break;
	case "3":
		$fld = "day5";
		$default = "勤務日2";
		break;
	case "4":
		$fld = "day2";
		$default = "法定休日";
		break;
	case "5":
		$fld = "day3";
		$default = "所定休日";
		break;
	}
	//振替休日
	if ($type == "4" && $holiday_name == "振替休日") {
		$type_name = $holiday_name;
	} else {
		$sql = "select $fld from calendarname";
		$cond = "";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		if (pg_num_rows($sel) > 0) {
			$type_name = pg_fetch_result($sel, 0, 0);
		} else {
			$type_name = $default;
		}
	}
}

// 手当情報取得
$arr_allowance = get_timecard_allowance($con, $fname);

if ($err_back_page_flg) {
	//登録エラーから戻った場合
	$allow_id = $allow_ids[0];
	//手当回数追加
	$allow_count = $allow_counts[0];
}
else {
	// 勤務実績を取得
	$sql = "select * from atdbkrslt";
	$cond = "where emp_id = '$emp_id' and date = '$date'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0) {
		$tmcd_group_id = pg_fetch_result($sel, 0, "tmcd_group_id");
		$pattern = pg_fetch_result($sel, 0, "pattern");
		$reason = pg_fetch_result($sel, 0, "reason");
		$start_time = pg_fetch_result($sel, 0, "start_time");
		$out_time = pg_fetch_result($sel, 0, "out_time");
		$ret_time = pg_fetch_result($sel, 0, "ret_time");
		$end_time = pg_fetch_result($sel, 0, "end_time");
		for ($i = 1; $i <= 10; $i++) {
			$start_var = "o_start_time$i";
			$end_var = "o_end_time$i";
			$$start_var = pg_fetch_result($sel, 0, "$start_var");
			$$end_var = pg_fetch_result($sel, 0, "$end_var");
		}
		$status = pg_fetch_result($sel, 0, "status");
		$allow_id = pg_fetch_result($sel, 0, "allow_id");
		$meeting_time = pg_fetch_result($sel, 0, "meeting_time");
		$night_duty = pg_fetch_result($sel, 0, "night_duty");
		$previous_day_flag = pg_fetch_result($sel, 0, "previous_day_flag");
		$next_day_flag = pg_fetch_result($sel, 0, "next_day_flag");
		$start_btn_time = pg_fetch_result($sel, 0, "start_btn_time");
		$end_btn_time = pg_fetch_result($sel, 0, "end_btn_time");
		//会議研修時間を開始終了時刻とする 20091008
		$meeting_start_time = pg_fetch_result($sel, 0, "meeting_start_time");
		$meeting_end_time = pg_fetch_result($sel, 0, "meeting_end_time");
		//手当回数、残業時刻追加
		$allow_count = pg_fetch_result($sel, 0, "allow_count");
		$over_start_time = pg_fetch_result($sel, 0, "over_start_time");
		$over_end_time = pg_fetch_result($sel, 0, "over_end_time");
		$over_start_next_day_flag = pg_fetch_result($sel, 0, "over_start_next_day_flag");
		$over_end_next_day_flag = pg_fetch_result($sel, 0, "over_end_next_day_flag");
		$over_start_time2 = pg_fetch_result($sel, 0, "over_start_time2");
		$over_end_time2 = pg_fetch_result($sel, 0, "over_end_time2");
		$over_start_next_day_flag2 = pg_fetch_result($sel, 0, "over_start_next_day_flag2");
		$over_end_next_day_flag2 = pg_fetch_result($sel, 0, "over_end_next_day_flag2");

		//時間有休データ取得 20111207
		if ($obj_hol_hour->paid_hol_hour_flag == "t") {
			$arr_hol_start_hour = $obj_hol_hour->get_paid_hol_hour($emp_id, $date);
			$paid_hol_start_hour = substr($arr_hol_start_hour["start_time"], 0, 2);
			$paid_hol_start_min = substr($arr_hol_start_hour["start_time"], 2, 2);
			$paid_hol_hour =  $arr_hol_start_hour["use_hour"];
			$paid_hol_min =  $arr_hol_start_hour["use_minute"];
		}
		
		$update_time = preg_replace("/(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/","$1年$2月$3日 $4:$5",pg_fetch_result($sel, 0, "update_time"));
		
		$update_emp_id = pg_fetch_result($sel, 0, "update_emp_id");
		//所属
		$update_org_name = get_orgname($con, $fname, $update_emp_id, $timecard_bean);
		if ($update_org_name != "") {
			$update_org_name = substr($update_org_name, 1);
			$update_org_name = str_replace(" ", " &gt; ", $update_org_name);
		}
		//更新者氏名
		$update_emp_name = get_emp_kanji_name($con, $update_emp_id, $fname);
	} else {
		$tmcd_group_id = "";
		$pattern = "";
		$reason = "";
		$start_time = "";
		$out_time = "";
		$ret_time = "";
		$end_time = "";
		for ($i = 1; $i <= 10; $i++) {
			$start_var = "o_start_time$i";
			$end_var = "o_end_time$i";
			$$start_var = "";
			$$end_var = "";
		}
		$status = "";
		$allow_id = "";
		$meeting_time = "";
		$night_duty = "";
		$previous_day_flag = 0;
		$next_day_flag = 0;
		$start_btn_time = "";
		$end_btn_time = "";
		$meeting_start_time = "";
		$meeting_end_time = "";
		//手当回数、残業時刻追加
		$allow_count = "1";
		$over_start_time = "";
		$over_end_time = "";
		$over_start_next_day_flag = "";
		$over_end_next_day_flag = "";
		$over_start_time2 = "";
		$over_end_time2 = "";
		$over_start_next_day_flag2 = "";
		$over_end_next_day_flag2 = "";

		$paid_hol_start_hour = "";
		$paid_hol_start_min = "";
		$paid_hol_hour =  "";
		$paid_hol_min =  "";
	}
}

//前日チェックがあるかフラグ
$previous_day_flag_checked = "";
if ($previous_day_flag == 1){
	$previous_day_flag_checked = "checked";
}

//翌日チェックがあるかフラグ
$next_day_flag_checked = "";
if ($next_day_flag == 1){
	$next_day_flag_checked = "checked";
}

if ($err_back_page_flg == false){
	// 時刻を時・分に分割
	list($start_hour, $start_min) = split_hm($start_time);
	list($out_hour, $out_min) = split_hm($out_time);
	list($ret_hour, $ret_min) = split_hm($ret_time);
	list($end_hour, $end_min) = split_hm($end_time);
	list($meeting_start_hour, $meeting_start_min) = split_hm($meeting_start_time);
	list($meeting_end_hour, $meeting_end_min) = split_hm($meeting_end_time);
	for ($i = 1; $i <= 10; $i++) {
		$start_time_var = "o_start_time$i";
		$start_hour_var = "o_start_hour$i";
		$start_min_var = "o_start_min$i";
		list($$start_hour_var, $$start_min_var) = split_hm($$start_time_var);
		$end_time_var = "o_end_time$i";
		$end_hour_var = "o_end_hour$i";
		$end_min_var = "o_end_min$i";
		list($$end_hour_var, $$end_min_var) = split_hm($$end_time_var);
	}

	if ($meeting_time != null ){
		$meeting_time_hh = substr($meeting_time, 0, 2);
		$meeting_time_mm = substr($meeting_time, 2, 2);
	}
	// 未設定時のデフォルト取得
	if ($over_start_time == "" || $over_end_time == "") {
		$arr_default_time = $timecard_common_class->get_default_overtime($start_time, $end_time, $next_day_flag, $tmcd_group_id, $pattern, $type, $previous_day_flag );
	}
	if ($over_start_time == "") {
		$over_start_hour = $arr_default_time["over_start_hour"];
		$over_start_min = $arr_default_time["over_start_min"];
		$over_start_next_day_flag = $arr_default_time["over_start_next_day_flag"];
	} else {
		list($over_start_hour, $over_start_min) = split_hm($over_start_time);
	}
	if ($over_end_time == "") {
		$over_end_hour = $arr_default_time["over_end_hour"];
		$over_end_min = $arr_default_time["over_end_min"];
		$over_end_next_day_flag = $arr_default_time["over_end_next_day_flag"];
	} else {
		list($over_end_hour, $over_end_min) = split_hm($over_end_time);
	}
	list($over_start_hour2, $over_start_min2) = split_hm($over_start_time2);
	list($over_end_hour2, $over_end_min2) = split_hm($over_end_time2);
}
list($start_btn_hour, $start_btn_min) = split_hm($start_btn_time);
list($end_btn_hour, $end_btn_min) = split_hm($end_btn_time);
//翌日フラグデフォルト設定
$over_start_next_day_flag_checked = "";
if ($over_start_next_day_flag == 1){
	$over_start_next_day_flag_checked = "checked";
}
$over_end_next_day_flag_checked = "";
if ($over_end_next_day_flag == 1){
	$over_end_next_day_flag_checked = "checked";
}
$over_start_next_day_flag2_checked = "";
if ($over_start_next_day_flag2 == 1){
	$over_start_next_day_flag2_checked = "checked";
}
$over_end_next_day_flag2_checked = "";
if ($over_end_next_day_flag2 == 1){
	$over_end_next_day_flag2_checked = "checked";
}
// 分の"--"を""とする
if ($over_start_min == "--") {
	$over_start_min = "";
}
if ($over_end_min == "--") {
	$over_end_min = "";
}
if ($over_start_min2 == "--") {
	$over_start_min2 = "";
}
if ($over_end_min2 == "--") {
	$over_end_min2 = "";
}

//出勤予定にグループが指定されていた場合、出勤予定のグループを優先する
if ($tmcd_group_id == null && strlen($tmcd_group_id) == 0 && $err_back_page_flg == false){
	$tmcd_group_id = get_timecard_group_id($con, $emp_id, $fname);
}

// 出勤パターン名を取得
$arr_attendance_pattern = $atdbk_common_class->get_pattern_array($tmcd_group_id);

// 「退勤後復帰」ボタン表示フラグを取得
$sql = "select ret_btn_flg from config";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$ret_btn_flg = pg_fetch_result($sel, 0, "ret_btn_flg");

// 退勤後復帰時刻表示フラグを設定
$ret_show_flg = ($ret_btn_flg == "t" || $o_start_hour1 != "");

// 当日の申請がないかチェック
$sql = "select count(*) from tmmdapply";
$cond = "where emp_id = '$emp_id' and target_date = '$date' and not delete_flg and re_apply_id is null ";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$apply_count = intval(pg_fetch_result($sel, 0, 0));
if ($apply_count == 0) {
	$sql = "select count(*) from ovtmapply";
	$cond = "where emp_id = '$emp_id' and target_date = '$date' and not delete_flg and re_apply_id is null ";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$apply_count = intval(pg_fetch_result($sel, 0, 0));
}

// 申請があれば出勤状況以外は変更不可とする
$disabled_flag = ($apply_count > 0);
$disabled = ($disabled_flag) ? " disabled" : "";

// 残業理由一覧を取得
$sql = "select reason_id, reason from ovtmrsn";
$cond = "where del_flg = 'f' order by reason_id";
$ovtmrsn_sel = select_from_table($con, $sql, $cond, $fname);
if ($ovtmrsn_sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 勤務条件テーブルより残業管理を取得
$sql = "select no_overtime from empcond";
$cond = "where emp_id = '$emp_id' ";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$no_overtime = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "no_overtime") : "";

//残業理由表示フラグ
if ((($over_start_hour != "" && $over_start_hour != "--") ||
			($over_start_min != "" && $over_start_min != "--") ||
			($over_end_hour != "" && $over_end_hour != "--") ||
			($over_end_min != "" && $over_end_min != "--") ||
			($over_start_hour2 != "" && $over_start_hour2 != "--") ||
			($over_start_min2 != "" && $over_start_min2 != "--") ||
			($over_end_hour2 != "" && $over_end_hour2 != "--") ||
			($over_end_min2 != "" && $over_end_min2 != "--") 
			)
		&& $timecard_bean->over_time_apply_type != "0"
		&& $no_overtime != "t" // 残業管理をする場合
	){
	$ovtm_rsn_id_disp = "";
	
	//エラーで再表示でない場合、残業申請情報を取得
	if ($err_back_page_flg == false){
		$sql = "select apply_id, reason_id, reason, reason_detail, apply_status from ovtmapply";
		$cond = "where emp_id = '$emp_id' and target_date = '$date' and delete_flg = 'f' order by apply_id desc limit 1";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		if (pg_num_rows($sel) > 0)
		{
			$apply_id = pg_fetch_result($sel, 0, "apply_id");
			$ovtm_reason_id = pg_fetch_result($sel, 0, "reason_id");
			$ovtm_reason = pg_fetch_result($sel, 0, "reason");
			//$ovtm_reason_detail = pg_fetch_result($sel, 0, "reason_detail");
			//$apply_status = pg_fetch_result($sel, 0, "apply_status");
		}
	}
}
else {
	$ovtm_rsn_id_disp = "none";
}

?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
<!--
window.resizeTo(640, 640);
var arr_atdptn_reason = new Array();
<?
//事由情報を出力
$arr_group_pattern_reason = $atdbk_common_class->get_pattern_reason_array("");
foreach ($arr_group_pattern_reason as $wk_tmcd_group_id => $arr_pattern_reason) {
	foreach ($arr_pattern_reason as $wk_atdptn_id => $wk_reason) {
		if ($wk_reason != "") {
			echo("arr_atdptn_reason['{$wk_tmcd_group_id}_{$wk_atdptn_id}'] = '$wk_reason';\n");
		}
	}
}
?>
function setReason(atdptn_id) {

	if (atdptn_id == '--') {
		document.mainform["reason"].value = '--';
		return;
	}
	wk_tmcd_group_id = document.mainform["list_tmcd_group_id"].value;
	wk_id = wk_tmcd_group_id+'_'+atdptn_id;
	if (arr_atdptn_reason[wk_id] == undefined) {
		document.mainform["reason"].value = '--';
		return;
	}
	document.mainform["reason"].value = arr_atdptn_reason[wk_id];
}
//残業理由その他表示
function setOvtmReasonDisabled() {
	if (document.getElementById('ovtm_reason')) {
		document.getElementById('ovtm_reason').style.display = (document.mainform.ovtm_reason_id.value == 'other') ? '' : 'none';
	}
}

//残業理由表示
function setOvtmReasonIdDisabled() {
	var disp = 'none';
	if (document.getElementById('over_start_hour').value != '--' ||
		document.getElementById('over_start_min').value != '--' ||
		document.getElementById('over_end_hour').value != '--' ||
		document.getElementById('over_end_min').value != '--' ||
		document.getElementById('over_start_hour2').value != '--' ||
		document.getElementById('over_start_min2').value != '--' ||
		document.getElementById('over_end_hour2').value != '--' ||
		document.getElementById('over_end_min2').value != '--') {
		disp = '';
	}
	document.getElementById('ovtm_rsn_id').style.display = disp;
	document.getElementById('ovtm_rsn_id2').style.display = disp;
}

//-->
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>タイムカード代行入力</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form name="mainform" action="atdbk_timecard_others_insert.php" method="post">
<? if ($success == "t") { ?>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録しました。</font></td>
</tr>
</table>
<? } ?>
<? if ($apply_count > 0) { ?>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="red">※申請情報が存在するため、出勤状況のみ変更可能です</font></td>
</tr>
</table>
<? } ?>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="180" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員氏名</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($emp_name); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日付</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($int_month); ?>月<? echo($int_day); ?>日&nbsp;<? echo(get_weekday(to_timestamp($date))); ?>曜</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">種別</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($type_name); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">グループ</font></td>
<td>
<?
	//グループコンボボックス出力
$atdbk_common_class->setGroupOption("tmcd_group_id", "list_tmcd_group_id", $tmcd_group_id, "pattern", ($disabled_flag == false));
?>
</td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務実績</font></td>
<td><select name="pattern"<? echo($disabled); ?> id="pattern" <? echo("onChange=\"setReason(this.value);\"");?>><? show_pattern_options($arr_attendance_pattern, $pattern); ?></select></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">事由</font></td>
<td><? $atdbk_common_class->set_reason_option("reason", $reason, ($disabled_flag == false)); ?></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($atdbk_common_class->duty_or_oncall_str); ?></font></td>
<td><select name="night_duty" <? echo($disabled); ?>><?show_night_duty_options($night_duty);?></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">手当</font></td>
<td><?show_allowance_list($allow_id, $arr_allowance, $disabled);?>
&nbsp;&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">回数</font>&nbsp;
<? /* 手当回数追加 20100114 */
show_allowance_count($allow_count, $disabled);
?>
</td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤時刻</font></td>
<td>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
		<select name="start_hour"<? echo($disabled); ?>><? show_hour_options_0_23($start_hour); ?></select>：<select name="start_min"<? echo($disabled); ?>><? show_min_options_00_59($start_min); ?></select>
		<label for="zenjitsu"><input type="checkbox" name="previous_day_flag" id="zenjitsu" <?=$previous_day_flag_checked ?> value="1" <?=$disabled ?>>前日</label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
打刻&nbsp;&nbsp;
<? 
$tmp_start_btn_time = "";
if ($start_btn_hour != "") {
	if ($start_btn_hour < 10) {
		$tmp_start_btn_time = "&nbsp;".(int)$start_btn_hour.":".$start_btn_min;
	} else {
		$tmp_start_btn_time = $start_btn_hour.":".$start_btn_min;
	}
}
echo($tmp_start_btn_time); ?>
	</font>
</td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退勤時刻</font></td>
<td>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
		<select name="end_hour"<? echo($disabled); ?>><? show_hour_options_0_23($end_hour); ?></select>：<select name="end_min"<? echo($disabled); ?>><? show_min_options_00_59($end_min); ?></select>
		<label for="yokujitsu"><input type="checkbox" name="next_day_flag" id="yokujitsu" <?=$next_day_flag_checked ?> value="1" <?=$disabled ?>>翌日</label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
打刻&nbsp;&nbsp;
<? 
$tmp_end_btn_time = "";
if ($end_btn_hour != "") {
	if ($end_btn_hour < 10) {
		$tmp_end_btn_time = "&nbsp;".(int)$end_btn_hour.":".$end_btn_min;
	} else {
		$tmp_end_btn_time = $end_btn_hour.":".$end_btn_min;
	}
}
echo($tmp_end_btn_time); ?>
	</font>
</td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">外出時刻</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="out_hour"<? echo($disabled); ?>><? show_hour_options_0_23($out_hour); ?></select>：<select name="out_min"<? echo($disabled); ?>><? show_min_options_00_59($out_min); ?></select> 〜 <select name="ret_hour"<? echo($disabled); ?>><? show_hour_options_0_23($ret_hour); ?></select>：<select name="ret_min"<? echo($disabled); ?>><? show_min_options_00_59($ret_min); ?></select></font></td>
</tr>
<? //残業時刻追加 ?>
<?
//残業理由表示用のJavascript関数呼出文字列
if ($timecard_bean->over_time_apply_type != "0" &&
		$no_overtime != "t") {
	$wk_onchange_str = "onChange=\"setOvtmReasonIdDisabled();\"";
}
else {
	$wk_onchange_str = "";
}

?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">残業時刻1</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="over_start_hour" id="over_start_hour" <? echo($wk_onchange_str); ?> <?=$disabled?>><? show_hour_options_0_23($over_start_hour); ?></select>：<select name="over_start_min" id="over_start_min" <? echo($wk_onchange_str); ?> <?=$disabled?>><? show_min_options_over_time($over_start_min, $timecard_bean->fraction_over_unit, true, true); ?></select>
<label for="yokujitsu2"><input type="checkbox" name="over_start_next_day_flag" id="yokujitsu2" <?=$over_start_next_day_flag_checked ?> value="1" <?=$disabled?>>翌日</label>
 〜 <select name="over_end_hour" id="over_end_hour" <? echo($wk_onchange_str); ?> <?=$disabled?>><? show_hour_options_0_23($over_end_hour); ?></select>：<select name="over_end_min" id="over_end_min" <? echo($wk_onchange_str); ?> <?=$disabled?>><? show_min_options_over_time($over_end_min, $timecard_bean->fraction_over_unit, true, false); ?></select>
<label for="yokujitsu3"><input type="checkbox" name="over_end_next_day_flag" id="yokujitsu3" <?=$over_end_next_day_flag_checked ?> value="1" <?=$disabled?>>翌日</label>
</font>
</td>
</tr>
<? //残業時刻2追加 20110621 ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">残業時刻2</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="over_start_hour2" id="over_start_hour2" <? echo($wk_onchange_str); ?>  <?=$disabled?>><? show_hour_options_0_23($over_start_hour2); ?></select>：<select name="over_start_min2" id="over_start_min2" <? echo($wk_onchange_str); ?>  <?=$disabled?>>
<?
// 残業時刻2 20110621
 show_min_options_over_time($over_start_min2, $timecard_bean->fraction_over_unit, true, true);?></select>
<label for="yokujitsu4"><input type="checkbox" name="over_start_next_day_flag2" id="yokujitsu4" <?=$over_start_next_day_flag2_checked ?> value="1"  <?=$disabled?>>翌日</label>
 〜 <select name="over_end_hour2" id="over_end_hour2" <? echo($wk_onchange_str); ?>  <?=$disabled?>><? show_hour_options_0_23($over_end_hour2); ?></select>：<select name="over_end_min2" id="over_end_min2" <? echo($wk_onchange_str); ?>  <?=$disabled?>><? show_min_options_over_time($over_end_min2, $timecard_bean->fraction_over_unit, true, false); ?></select>
<label for="yokujitsu5"><input type="checkbox" name="over_end_next_day_flag2" id="yokujitsu5" <?=$over_end_next_day_flag2_checked ?> value="1"  <?=$disabled?>>翌日</label>
</font>
</td>
</tr>
<? //時間有休追加 20111207 
$obj_hol_hour->show_input_item($paid_hol_start_hour, $paid_hol_start_min, $paid_hol_hour, $paid_hol_min);
?>
<?
if ($ret_show_flg) {
	$break_flg = false;
	$ret_str = ($timecard_bean->return_icon_flg != "2") ? "退勤後復帰" : "呼出勤務";
	for ($i = 1; $i <= 10; $i++) {
		if ($break_flg) {
			break;
		}

		$start_hour_var = "o_start_hour$i";
		$start_min_var = "o_start_min$i";
		$end_hour_var = "o_end_hour$i";
		$end_min_var = "o_end_min$i";

		echo("<tr height=\"22\">\n");
		echo("<td align=\"right\" bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$ret_str}時刻{$i}</font></td>\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><select name=\"$start_hour_var\"$disabled>");
		show_hour_options_0_23($$start_hour_var);
		echo("</select>：<select name=\"$start_min_var\"$disabled>");
		show_min_options_00_59($$start_min_var);
		echo("</select> 〜 <select name=\"$end_hour_var\"$disabled>");
		show_hour_options_0_23($$end_hour_var);
		echo("</select>：<select name=\"$end_min_var\"$disabled>");
		show_min_options_00_59($$end_min_var);
		echo("</select></font></td>\n");
		echo("</tr>\n");

		//エラーで戻った場合は次の値が空の場合ループを止める
		$err_start_hour_var = "o_start_hour".($i + 1);
		if ($err_back_page_flg && ($$err_start_hour_var == "" || $$err_start_hour_var == "--")) {
			$break_flg = true;
		}
		else if ($$start_hour_var == "" || $$start_hour_var == "--"){
			$break_flg = true;
		}
	}
}

?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">会議・研修・病棟外勤務</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
	<select name="meeting_start_hour" <?=$disabled?>><? show_hour_options_0_23($meeting_start_hour); ?></select>：<select name="meeting_start_min" <?=$disabled?>><? show_min_options_00_59($meeting_start_min); ?></select> 〜 <select name="meeting_end_hour" <?=$disabled?>><? show_hour_options_0_23($meeting_end_hour); ?></select>：<select name="meeting_end_min" <?=$disabled?>><? show_min_options_00_59($meeting_end_min); ?></select>
</font>
</td>
</tr>

<input type="hidden" name="meeting_time_hh" value="<?=$meeting_time_hh?>">
<input type="hidden" name="meeting_time_mm" value="<?=$meeting_time_mm?>">

<tr height="22" id="ovtm_rsn_id" style="display:<? echo($ovtm_rsn_id_disp); ?>;">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">残業理由</font></td>
<td id="ovtm_rsn_id2" style="display:<? echo($ovtm_rsn_id_disp); ?>;">
<select name="ovtm_reason_id" onchange="setOvtmReasonDisabled();"  <?=$disabled?>>
<?
while ($row = pg_fetch_array($ovtmrsn_sel)) {
	$tmp_reason_id = $row["reason_id"];
	$tmp_reason = $row["reason"];
	
	$selected = ($ovtm_reason_id == $tmp_reason_id) ? " selected" : "";
	
	echo("<option value=\"$tmp_reason_id\" $selected>$tmp_reason\n");
}
//理由が入力済みの場合、その他とする
$selected = ($ovtm_reason != "") ? " selected" : "";
$ovtm_reason_disp = ($ovtm_reason != "" || pg_num_rows($ovtmrsn_sel) == 0) ? "" : "none"; //残業理由が０件はその他の入力ができるようにする 20120125
?>
<option value="other" <? echo($selected); ?>>その他
</select><br>
<div id="ovtm_reason" style="display:<? echo($ovtm_reason_disp); ?>;"><input type="text" name="ovtm_reason" value="<? echo($ovtm_reason); ?>" size="50" maxlength="100" style="ime-mode:active;"  <?=$disabled?>></div>
</td>
</tr>


<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤状況</font></td>
<td><select name="status"><? show_status_options($status); ?></select></td>
</tr>
<? /*
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">最終更新日時</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$update_time?>
</font>
</td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">最終更新者所属</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$update_org_name?>
</font>
</td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">最終更新者</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$update_emp_name?>
</font>
</td>
</tr>
*/ ?>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="emp_id" value="<? echo($emp_id); ?>">
<input type="hidden" name="date" value="<? echo($date); ?>">
<input type="hidden" name="apply_count" value="<? echo($apply_count); ?>">
<input type="hidden" name="pre_end_time" value="<? echo("$end_hour$end_min"); ?>">
<input type="hidden" name="pre_next_day_flag" value="<? echo("$next_day_flag"); ?>">
<input type="hidden" name="pre_night_duty_flag" value="<? echo(($night_duty == 1)); ?>">
<?
if ($ret_show_flg || $disabled_flag) {
	for ($i = 1; $i <= 10; $i++) {
		$hour_var = "o_start_hour$i";
		$min_var = "o_start_min$i";
		echo("<input type=\"hidden\" name=\"pre_o_start_time$i\" value=\"${$hour_var}${$min_var}\">\n");
	}
}

if ($disabled_flag){
?>
<input type="hidden" name="tmcd_group_id" value="<?=$tmcd_group_id?>">
<input type="hidden" name="pattern" value="<?=$pattern?>">
<input type="hidden" name="reason" value="<?=$reason?>">
<input type="hidden" name="night_duty" value="<?=$night_duty?>">
<input type="hidden" name="allow_ids[]" value="<?=$allow_id?>">
<input type="hidden" name="allow_counts[]" value="<?=$allow_count?>">
<input type="hidden" name="start_hour" value="<?=$start_hour?>">
<input type="hidden" name="start_min" value="<?=$start_min?>">
<input type="hidden" name="out_hour" value="<?=$out_hour?>">
<input type="hidden" name="out_min" value="<?=$out_min?>">
<input type="hidden" name="ret_hour" value="<?=$ret_hour?>">
<input type="hidden" name="ret_min" value="<?=$ret_min?>">
<input type="hidden" name="end_hour" value="<?=$end_hour?>">
<input type="hidden" name="end_min" value="<?=$end_min?>">
<input type="hidden" name="over_start_hour" value="<?=$over_start_hour?>">
<input type="hidden" name="over_start_min" value="<?=$over_start_min?>">
<input type="hidden" name="over_start_next_day_flag" value="<?=$over_start_next_day_flag?>">
<input type="hidden" name="over_end_hour" value="<?=$over_end_hour?>">
<input type="hidden" name="over_end_min" value="<?=$over_end_min?>">
<input type="hidden" name="over_end_next_day_flag" value="<?=$over_end_next_day_flag?>">
<input type="hidden" name="meeting_start_hour" value="<?=$meeting_start_hour?>">
<input type="hidden" name="meeting_start_min" value="<?=$meeting_start_min?>">
<input type="hidden" name="meeting_end_hour" value="<?=$meeting_end_hour?>">
<input type="hidden" name="meeting_end_min" value="<?=$meeting_end_min?>">
    <?
	if ($ret_show_flg) {
		$break_flg = false;
		for ($i = 1; $i <= 10; $i++) {
			$start_hour_var = "o_start_hour$i";
			$start_min_var = "o_start_min$i";
			$end_hour_var = "o_end_hour$i";
			$end_min_var = "o_end_min$i";

			if ($$start_hour_var == "") {
				break;
			}

			echo("<input type=\"hidden\" name=\"".$start_hour_var."\"  value=\"".$$start_hour_var."\">\n");
			echo("<input type=\"hidden\" name=\"".$start_min_var."\"  value=\"".$$start_min_var."\">\n");
			echo("<input type=\"hidden\" name=\"".$end_hour_var."\"  value=\"".$$end_hour_var."\">\n");
			echo("<input type=\"hidden\" name=\"".$end_min_var."\"  value=\"".$$end_min_var."\">\n");
		}
	}

}
?>
<input type="hidden" name="start_btn_time" value="<? echo($start_btn_time); ?>">
<input type="hidden" name="end_btn_time" value="<? echo($end_btn_time); ?>">
</form>
</center>
</body>
<? pg_close($con); ?>
</html>
<?
/*
// 日付をタイムスタンプに変換
function to_timestamp($yyyymmdd) {
	$y = substr($yyyymmdd, 0, 4);
	$m = substr($yyyymmdd, 4, 2);
	$d = substr($yyyymmdd, 6, 2);
	return mktime(0, 0, 0, $m, $d, $y);
}

// タイムスタンプから曜日を返す
function get_weekday($date) {
	$wd = date("w", $date);
	switch ($wd) {
	case "0":
		return "日";
		break;
	case "1":
		return "月";
		break;
	case "2":
		return "火";
		break;
	case "3":
		return "水";
		break;
	case "4":
		return "木";
		break;
	case "5":
		return "金";
		break;
	case "6":
		return "土";
		break;
	}
}
*/
// 出勤予定オプションを出力
function show_pattern_options($arr_attendance_pattern, $selected_pattern) {
	echo("<option value=\"--\"");
	if ($pattern == "") {
		echo(" selected");
	}
	echo(">\n");
	foreach ($arr_attendance_pattern as $id => $val) {
		echo("<option value=\"$id\"");
		if ($id == $selected_pattern) {
			echo(" selected");
		}
		echo(">$val");
	}
}

// 時刻を時・分に分割
function split_hm($time) {
	if ($time != "") {
		return array(substr($time, 0, 2), substr($time, 2, 2));
	} else {
		return array("", "");
	}
}

// 出勤状況オプションを出力
function show_status_options($status) {
	$status_values = array("", "1", "2", "3", "4", "5");
	$status_names = array("", "勤務中", "外出中", "退勤済み", "休暇中", "欠勤");
	for ($i = 0; $i < count($status_values); $i++) {
		$status_value = $status_values[$i];
		$status_name = $status_names[$i];

		echo("<option value=\"$status_value\"");
		if ($status_value == $status) {
			echo(" selected");
		}
		echo(">$status_name");
	}
}

// 当直オプションを出力
function show_night_duty_options($night_duty) {
	$arr_night_duty_value = array("", "1", "2");
	$arr_night_duty_name = array("", "有り", "無し");
	for ($i = 0; $i < count($arr_night_duty_value); $i++) {
		$night_duty_value = $arr_night_duty_value[$i];
		$night_duty_name = $arr_night_duty_name[$i];

		echo("<option value=\"$night_duty_value\"");
		if ($night_duty_value == $night_duty) {
			echo(" selected");
		}
		echo(">$night_duty_name");
	}
}
?>
