<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" action="facility_category_admin_setting.php" method="post">
<input type="hidden" name="admin_dept_st_flg" value="<? echo($admin_dept_st_flg); ?>">
<input type="hidden" name="admin_dept_flg" value="<? echo($admin_dept_flg); ?>">
<input type="hidden" name="admin_class_src" value="<? echo($admin_class_src); ?>">
<input type="hidden" name="admin_atrb_src" value="<? echo($admin_atrb_src); ?>">
<?
if (is_array($hid_admin_dept)) {
	foreach ($hid_admin_dept as $tmp_dept_id) {
		echo("<input type=\"hidden\" name=\"admin_dept[]\" value=\"$tmp_dept_id\">\n");
	}
} else {
	$hid_admin_dept = array();
}
?>
<input type="hidden" name="admin_st_flg" value="<? echo($admin_st_flg); ?>">
<?
if (is_array($admin_st)) {
	foreach ($admin_st as $tmp_st_id) {
		echo("<input type=\"hidden\" name=\"admin_st[]\" value=\"$tmp_st_id\">\n");
	}
} else {
	$admin_st = array();
}
?>
<input type="hidden" name="target_id_list1" value="<? echo($target_id_list1); ?>">
<?
if ($target_id_list1 != "") {
	$hid_admin_emp = split(",", $target_id_list1);
} else {
	$hid_admin_emp = array();
}
?>

<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="cate_id" value="<? echo($cate_id); ?>">
<input type="hidden" name="back" value="t">
</form>
<?
require("about_session.php");
require("about_authority.php");
require_once("Cmx.php");
require_once("aclg_set.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 41, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// 入力チェック
if ((($admin_dept_flg != "1" && count($hid_admin_dept) == 0) || ($admin_st_flg != "1" && count($admin_st) == 0)) && count($hid_admin_emp) == 0) {
	echo("<script type=\"text/javascript\">alert('設定内容が不正です。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}

// 登録内容の編集
if ($admin_dept_st_flg != "t") {$admin_dept_st_flg = "f";}
if ($admin_dept_flg == "") {$admin_dept_flg = null;}
if ($admin_st_flg == "") {$admin_st_flg = null;}

// トランザクションを開始
pg_query($con, "begin");

// 施設・設備情報を更新
$sql = "update fclcate set";
$set = array("admin_dept_st_flg", "admin_dept_flg", "admin_st_flg");
$setvalue = array($admin_dept_st_flg, $admin_dept_flg, $admin_st_flg);
$cond = "where fclcate_id = $cate_id";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 施設・設備管理者部署情報を登録
$sql = "delete from fclcateadmindept";
$cond = "where fclcate_id = $cate_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
foreach ($hid_admin_dept as $tmp_val) {
	list($tmp_class_id, $tmp_atrb_id, $tmp_dept_id) = split("-", $tmp_val);
	$sql = "insert into fclcateadmindept (fclcate_id, class_id, atrb_id, dept_id) values (";
	$content = array($cate_id, $tmp_class_id, $tmp_atrb_id, $tmp_dept_id);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// 施設・設備管理者役職情報を登録
$sql = "delete from fclcateadminst";
$cond = "where fclcate_id = $cate_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
foreach ($admin_st as $tmp_st_id) {
	$sql = "insert into fclcateadminst (fclcate_id, st_id) values (";
	$content = array($cate_id, $tmp_st_id);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// 施設・設備管理者職員情報を登録
$sql = "delete from fclcateadmin";
$cond = "where fclcate_id = $cate_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
foreach ($hid_admin_emp as $tmp_emp_id) {
	$sql = "insert into fclcateadmin (fclcate_id, emp_id) values (";
	$content = array($cate_id, $tmp_emp_id);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 管理者設定画面を再表示
echo("<script type=\"text/javascript\">location.href = 'facility_category_admin_setting.php?session=$session&cate_id=$cate_id'</script>");
?>
