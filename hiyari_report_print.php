<?php
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");
require_once("hiyari_common.ini");
require_once("hiyari_reporting_common.ini");
require_once("hiyari_mail.ini");
require_once("Cmx.php");
require_once("aclg_set.php");

//==============================
//初期処理
//==============================
//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if ( $session == "0" ) {
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// 権限チェック
$summary = check_authority($session, 47, $fname);
if ($summary == "0") {
    showLoginPage();
    exit;
}

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if ( $con == "0" ) {
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con,$_GET);

//==============================
//ユーザーID取得
//==============================
$emp_id = get_emp_id($con,$session,$fname);

//==============================
//HTML出力
//==============================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$INCIDENT_TITLE?> | 報告書印刷</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script language="javascript">
function load_action() {
	//印刷して閉じる
	self.print();
	setTimeout('self.close()', 1000);
}	
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

.list {border-collapse:collapse;}
.list td {border:#35B341 solid 1px;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#35B341 solid 0px;}
table.block_in td td {border-width:1;}

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="load_action()">
    <form name="mainform" action="hiyari_rp_classification_folder_input.php" method="post">
        <input type="hidden" name="session" value="<?=$session?>">
        <input type="hidden" name="mode" value="<?=$mode?>">
        <input type="hidden" name="is_postback" value="true">
        <input type="hidden" name="folder_id" value="<?=$folder_id?>">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td>
                    <?php $patient_non_print2 = $patient_non_print == "true"; ?>
                    <?php $report_html = get_report_html_for_print($con, $fname, $report_id, $eis_id, $patient_non_print2, $session, $gamen_mode );//$eis_idは省略可能。?>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <?=$report_html?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
<?php pg_close($con); // DBコネクション終了 ?>

