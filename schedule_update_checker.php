<?php
require_once("about_comedix.php");
require_once("schedule_repeat_common.php");
require_once("schedule_common.ini");
require_once("webmail/config/config.php");
require_once("Cmx.php");
require_once("aclg_set.php");
require_once("Cmx/Model/SystemConfig.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 2, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);


// 入力チェック
$current_regex_encoding = mb_regex_encoding('EUC-JP');
if (mb_ereg_replace("[ 　]", "", $schd_title) === "") {
    echo("<script type=\"text/javascript\">alert('タイトルが入力されていません。');</script>");
    echo("<script type=\"text/javascript\">history.back();</script>");
    exit;
}
mb_regex_encoding($current_regex_encoding);
if (strlen($schd_title) > 100) {
    echo("<script type=\"text/javascript\">alert('タイトルが長すぎます。');</script>");
    echo("<script type=\"text/javascript\">history.back();</script>");
    exit;
}
if (strlen($schd_plc) > 50) {
    echo("<script type=\"text/javascript\">alert('行先（テキスト）が長すぎます。');</script>");
    echo("<script type=\"text/javascript\">history.back();</script>");
    exit;
}
if ($rptopt == "" || $rptopt == "1") {
    if (!checkdate($schd_start_mth, $schd_start_day, $schd_start_yrs)) {
        echo("<script type=\"text/javascript\">alert('日付が不正です。');</script>");
        echo("<script type=\"text/javascript\">history.back();</script>");
        exit;
    }
}
if ($timeless == "") {
    if ("$schd_start_hrs$schd_start_min" >= "$schd_dur_hrs$schd_dur_min") {
        echo("<script type=\"text/javascript\">alert('時刻が不正です。');</script>");
        echo("<script type=\"text/javascript\">history.back();</script>");
        exit;
    }
}
if ($target_id_list == "") {
    echo("<script type=\"text/javascript\">alert('登録対象者が選択されていません。');</script>");
    echo("<script type=\"text/javascript\">history.back();</script>");
    exit;
}

// アラーム
$alarm = 0;
$conf = new Cmx_SystemConfig();
$schedule_alarm = $conf->get('schedule.alarm');
$schedule_alarm_start = $conf->get('schedule.alarm.start');
$schedule_alarm_end = $conf->get('schedule.alarm.end');
if ($schedule_alarm) {
    if (preg_match("/\D/",$_POST["alarm_h"]) or preg_match("/\D/",$_POST["alarm_m"])) {
        echo("<script type=\"text/javascript\">alert('アラーム通知時間は数値で入力してください。');</script>");
        echo("<script type=\"text/javascript\">history.back();</script>");
        exit;
    }
    if ($schedule_alarm === "1") {
        if (is_numeric($_POST["alarm_h"])) {
            $alarm += $_POST["alarm_h"] * 60;
        }
        if (is_numeric($_POST["alarm_m"])) {
            $alarm += $_POST["alarm_m"];
        }
        if ($alarm and ($schedule_alarm_start > $alarm or $schedule_alarm_end < $alarm)) {
            echo("<script type=\"text/javascript\">alert('アラーム通知時間は設定可能範囲内で入力してください。');</script>");
            echo("<script type=\"text/javascript\">history.back();</script>");
            exit;
        }
    }
    else if ($schedule_alarm === "2") {
        if ($_POST["alarm_m"]) {
            $alarm = 10;
        }
    }
}

// 日付の設定
$date = ($rptopt == "" || $rptopt == "1") ? "$schd_start_yrs$schd_start_mth$schd_start_day" : "";

if ($schd_imprt != "2") {
    $pub_limit = null;
} else {
    if ($pub_limit == "d") {
        $pub_limit = "d" . $pub_limit_dept;
    } else if ($pub_limit == "g") {
        $pub_limit = $pub_limit_group_id;
    }

    if ($pub_limit == "") {
        $pub_limit = null;
    }
}

// トランザクションの開始
pg_query($con, "begin transaction");

// ログインユーザの職員IDを取得
$sql = "select emp_id, emp_lt_nm, emp_ft_nm, get_mail_login_id(emp_id) from empmst";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$login_emp_id = pg_fetch_result($sel, 0, "emp_id");
$emp_nm = pg_fetch_result($sel, 0, "emp_lt_nm") . pg_fetch_result($sel, 0, "emp_ft_nm");
$emp_login_id = pg_fetch_result($sel, 0, 3);

// 登録者が違う場合や承認の場合は、指定職員のデータのみ更新
//2012/12/19 承認処理判定を追加
if (($reg_emp_flg == "f") || ($submit == "承認") ) {
    $sync_emp_flg = false;
} else {
    $sync_emp_flg = true;
}
// 更新対象スケジュール一覧を取得
$upd_schedules = get_repeated_schedules($con, $schd_id, $original_timeless, $rptopt, $fname, $sync_emp_flg);

// 削除済みの場合
if (count($upd_schedules) == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\">alert('この予約は既に削除されています。');window.opener.location.reload();self.close();</script>");
    exit;
}
// 行先が未指定の場合DBにnull設定
if ($place_id == "") {
    $place_id = null;
}

if ($timeless != "t") {
    $timeless = "f";
}

$new_emp_id = array();
$old_emp_id = array();

//2012/12/19 承認処理判定を追加
if (($reg_emp_flg == "t") && ($submit != "承認")){
    // 更新前の職員ID
    $old_emp_id = get_emp_id_from_schedules($con, $schd_id, $original_timeless, $fname);

    // 登録対象職員を配列化
    if ($target_id_list != "") {
        $new_emp_id = array_merge($new_emp_id, split(",", $target_id_list));
    }
} else {
    // 登録者以外の場合,承認処理の場合
    $old_emp_id[] = $login_emp_id;
    $new_emp_id[] = $login_emp_id;
}

$emp_change_flg = false;
// 登録者の場合に、職員ID変更確認。(承認以外の場合も)
//2012/12/19 承認処理判定を追加
if (($reg_emp_flg == "t") && ($submit != "承認")){
    if (count($old_emp_id) == count($new_emp_id)) {
        sort($new_emp_id);

		//2012/12/19		
		//trac #191 $old_emp_idもsortしないと、$old_emp_id[$i] == $new_emp_id[$i]となることはほぼ皆無→sortする
		sort($old_emp_id);
		
		for ($i=0; $i<count($old_emp_id); $i++) {
            if ($old_emp_id[$i] != $new_emp_id[$i]) {
                $emp_change_flg = true;
                break;
            }
        }
    } else {
        $emp_change_flg = true;
    }
}

//承認依頼有無フラグ
$aprv_ctrl = $conf->get("schedule.aprv_ctrl");	

// 職員IDの変更がない場合
if ($emp_change_flg == false) {
// スケジュールを更新
    foreach ($upd_schedules as $tmp_schedule) {

        $tmp_schd_id = $tmp_schedule["id"];
        $tmp_timeless = $tmp_schedule["timeless"];
        $tmp_emp_id = $tmp_schedule["emp_id"];
        $tmp_date = $tmp_schedule["date"];
        $tmp_time = $tmp_schedule["time"];
        $tmp_schd_status = $tmp_schedule["approve_status"];

        // 時間指定の有無が変わらない場合
        if ($tmp_timeless == $timeless) {

            // スケジュール情報を更新
            $table_name = ($tmp_timeless != "t") ? "schdmst" : "schdmst2";
            $sql = "update $table_name set";
            $cond = "where schd_id = '$tmp_schd_id'";
            $set = array("schd_title", "schd_plc", "schd_imprt", "schd_detail", "schd_type", "schd_place_id", "marker", "alarm");
            $setvalue = array(p($schd_title), $schd_plc, $schd_imprt, p($schd_detail), $schd_type, $place_id, $marker, $alarm);
            if ($schd_imprt != "2" || $pub_limit != "nc") {
                array_push($set, "pub_limit");
                array_push($setvalue, $pub_limit);
            }
            if ($date != "") {
                array_push($set, "schd_start_date");
                array_push($setvalue, $date);
            }
            if ($tmp_timeless != "t") {
                array_push($set, "schd_start_time_v", "schd_dur_v");
                array_push($setvalue, "$schd_start_hrs$schd_start_min", "$schd_dur_hrs$schd_dur_min");
            }

            if ($submit == "承認") {
                array_push($set, "schd_status");
                array_push($setvalue, "3");
            } else if ($login_emp_id != $tmp_emp_id) {
                array_push($set, "schd_status");
                if ($timeless == "f") {
                    $new_time = "{$schd_start_hrs}:{$schd_start_min}〜{$schd_dur_hrs}:{$schd_dur_min}";
                } else {
                    $new_time = "時刻指定なし";
                }
                // 日時変更あり
                if (($date != "" && $date != str_replace("/", "", $tmp_date)) ||
                    $new_time != $tmp_time) {
						
					if($aprv_ctrl == "2")
					{
						//承認依頼しない(管理画面のその他タグで指定)
						array_push($setvalue, "3");
					}
					else
					{
						array_push($setvalue, "2");
					}
					
                } else {
                // 日時変更なし
                    array_push($setvalue, $tmp_schd_status);
                }
            }
            $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
            if ($upd == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }

        // 時間指定の有無が変わる場合
        } else {

            // 更新元のスケジュール情報を取得
            $table_name = ($tmp_timeless != "t") ? "schdmst" : "schdmst2";
            $sql = "select emp_id, schd_reg_id, schd_status, repeat_flg, reg_time, schd_start_date, alarm_confirm from $table_name";
            $cond = "where schd_id = '$tmp_schd_id'";
            $sel = select_from_table($con, $sql, $cond, $fname);
            if ($sel == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
            $emp_id = pg_fetch_result($sel, 0, "emp_id");
            $reg_emp_id = pg_fetch_result($sel, 0, "schd_reg_id");
            $schd_status = pg_fetch_result($sel, 0, "schd_status");
            $repeat_flg = pg_fetch_result($sel, 0, "repeat_flg");
            $reg_time = pg_fetch_result($sel, 0, "reg_time");
            $schd_start_date = pg_fetch_result($sel, 0, "schd_start_date");
            $alarm_confirm = pg_fetch_result($sel, 0, "alarm_confirm");

            // 更新元のスケジュール情報を削除
            $sql = "delete from $table_name";
            $cond = "where schd_id = '$tmp_schd_id'";
            $del = delete_from_table($con, $sql, $cond, $fname);
            if ($del == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }

            // スケジュールIDを採番
            $new_schd_id = get_schd_id($con, $fname, $timeless);
            // スケジュール情報を登録
            $tmp_date = ($date == "") ? $schd_start_date : $date;
            $new_table_name = ($timeless != "t") ? "schdmst" : "schdmst2";
            $columns = array("schd_id", "emp_id", "schd_title", "schd_plc", "schd_imprt", "schd_start_date", "schd_detail", "schd_type", "schd_place_id", "marker", "schd_reg_id", "repeat_flg", "reg_time", "alarm", "alarm_confirm");
            $content = array($new_schd_id, $emp_id, p($schd_title), $schd_plc, $schd_imprt, $tmp_date, p($schd_detail), $schd_type, $place_id, $marker, $reg_emp_id, $repeat_flg, $reg_time, $alarm, $alarm_confirm);
            if ($schd_imprt != "2" || $pub_limit != "nc") {
                array_push($columns, "pub_limit");
                array_push($content, $pub_limit);
            }
            if ($timeless != "t") {
                // 24:00対応
                $schd_dur = "$schd_dur_hrs:$schd_dur_min";
                if ($schd_dur == "24:00") {
                    $schd_dur = "23:59";
                }
                array_push($columns, "schd_start_time", "schd_dur", "schd_start_time_v", "schd_dur_v");
                array_push($content, "$schd_start_hrs:$schd_start_min", "$schd_dur", "$schd_start_hrs$schd_start_min", "$schd_dur_hrs$schd_dur_min");
            }
            if ($submit == "承認") {
                array_push($columns, "schd_status");
                array_push($content, "3");
            } else if ($login_emp_id != $tmp_emp_id) {
                array_push($columns, "schd_status");
				
				if($aprv_ctrl == "2")
				{
					//承認依頼しない(管理画面のその他タグで指定)
					array_push($content, "3");
				}
				else
				{
					array_push($content, "2");
				}
				
            } else {
                array_push($columns, "schd_status");
                array_push($content, $schd_status);
            }
            $sql = "insert into $new_table_name (" . join(", ", $columns) . ") values (";
            $ins = insert_into_table($con, $sql, $content, $fname);
            if ($ins == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
        }
    }

} else {
// 職員IDの変更がある場合
    // 更新元のスケジュール情報を取得
    $table_name = ($original_timeless != "t") ? "schdmst" : "schdmst2";
    $sql = "select emp_id, schd_reg_id, schd_status, repeat_flg, reg_time, schd_start_date, alarm_confirm ";
    if ($original_timeless != "t") {
        $sql .= " , schd_start_time_v, schd_dur_v ";
    }
    $sql .= " from $table_name";
    $cond = "where schd_id = '$schd_id'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
//  $emp_id = pg_fetch_result($sel, 0, "emp_id");
    $reg_emp_id = pg_fetch_result($sel, 0, "schd_reg_id");
    $schd_status = pg_fetch_result($sel, 0, "schd_status");
    $repeat_flg = pg_fetch_result($sel, 0, "repeat_flg");
    $reg_time = pg_fetch_result($sel, 0, "reg_time");
    $schd_start_date = pg_fetch_result($sel, 0, "schd_start_date");
    $alarm_confirm = pg_fetch_result($sel, 0, "alarm_confirm");
    if ($original_timeless != "t") {
        $schd_start_time_v = pg_fetch_result($sel, 0, "schd_start_time_v");
        $schd_dur_v = pg_fetch_result($sel, 0, "schd_dur_v");
        $schd_time = "$schd_start_time_v$schd_dur_v";
    }

// 日付変更
    if ($date != "") {
        $arr_date[] = $date;
    } else {
        $arr_date = get_date_from_schedules($con, $schd_id, $original_timeless, $rptopt, $fname);
    }

    $arr_schd_id = array();
    // 時刻指定有無
    $arr_timeless = array();
    // 承認情報
    $arr_schd_status = array();
    foreach ($upd_schedules as $tmp_schedule) {
        $tmp_key = str_replace("/", "-", $tmp_schedule["date"])."_".$tmp_schedule["emp_id"];
        $arr_schd_id["$tmp_key"] = $tmp_schedule["id"];
        $arr_timeless["$tmp_key"] = $tmp_schedule["timeless"];
        $arr_schd_status["$tmp_key"] = $tmp_schedule["approve_status"];
    }

    $new_table_name = ($timeless != "t") ? "schdmst" : "schdmst2";

    // 登録メール用
    $arr_add_emp_id = array();

    // 登録・更新処理
    foreach($arr_date as $tmp_date) {
        foreach($new_emp_id as $tmp_emp_id) {
            if ($date != "") {
                // 元の日付の情報を取得する
                $tmp_key = $schd_start_date."_".$tmp_emp_id;
            } else {
                $tmp_key = $tmp_date."_".$tmp_emp_id;
            }
            $tmp_schd_id = $arr_schd_id["$tmp_key"];
            $tmp_schd_status = $arr_schd_status["$tmp_key"];
            $tmp_timeless = $arr_timeless["$tmp_key"];

            // 時刻指定有無が有り、かつ、変わらない場合は更新する
            if ($tmp_timeless != "" && $tmp_timeless == $timeless) {
                $table_name = ($tmp_timeless != "t") ? "schdmst" : "schdmst2";
                $sql = "update $table_name set";
                $cond = "where schd_id = '$tmp_schd_id'";
                $set = array("schd_title", "schd_plc", "schd_imprt", "schd_detail", "schd_type", "schd_place_id", "marker", "alarm");
                $setvalue = array(p($schd_title), $schd_plc, $schd_imprt, p($schd_detail), $schd_type, $place_id, $marker, $alarm);
                if ($schd_imprt != "2" || $pub_limit != "nc") {
                    array_push($set, "pub_limit");
                    array_push($setvalue, $pub_limit);
                }
                if ($date != "") {
                    array_push($set, "schd_start_date");
                    array_push($setvalue, $tmp_date);
                }
                if ($tmp_timeless != "t") {
                    array_push($set, "schd_start_time_v", "schd_dur_v");
                    array_push($setvalue, "$schd_start_hrs$schd_start_min", "$schd_dur_hrs$schd_dur_min");
                    $tmp_time = "$schd_start_hrs$schd_start_min$schd_dur_hrs$schd_dur_min";
                }
                if ($login_emp_id != $tmp_emp_id) {
                    // 日時が変更された場合、未承認にする
                    if (($date != "" && $date != $schd_start_date) || ($tmp_timeless != "t" && $tmp_time != $schd_time)) {
                        array_push($set, "schd_status");
						
						if($aprv_ctrl == "2")
						{
							//承認依頼しない(管理画面のその他タグで指定)
							array_push($setvalue, "3");
						}
						else
						{
							array_push($setvalue, "2");
						}
                    }
                }
//echo("sql=".$sql);
                $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
                if ($upd == 0) {
                    pg_query($con, "rollback");
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }
            } else { // 追加登録
                // スケジュールIDを採番
                $new_schd_id = get_schd_id($con, $fname, $timeless);

                $columns = array("schd_id", "emp_id", "schd_title", "schd_plc", "schd_imprt", "schd_start_date", "schd_detail", "schd_type", "schd_place_id", "marker", "schd_reg_id", "repeat_flg", "reg_time", "alarm", "alarm_confirm");
                $content = array($new_schd_id, $tmp_emp_id, p($schd_title), $schd_plc, $schd_imprt, $tmp_date, p($schd_detail), $schd_type, $place_id, $marker, $reg_emp_id, $repeat_flg, $reg_time, $alarm, $alarm_confirm);
                if ($schd_imprt != "2" || $pub_limit != "nc") {
                    array_push($columns, "pub_limit");
                    array_push($content, $pub_limit);
                }
                if ($timeless != "t") {
                    // 24:00対応
                    $schd_dur = "$schd_dur_hrs:$schd_dur_min";
                    if ($schd_dur == "24:00") {
                        $schd_dur = "23:59";
                    }
                    array_push($columns, "schd_start_time", "schd_dur", "schd_start_time_v", "schd_dur_v");
                    array_push($content, "$schd_start_hrs:$schd_start_min", "$schd_dur", "$schd_start_hrs$schd_start_min", "$schd_dur_hrs$schd_dur_min");
                }

                if ($login_emp_id == $tmp_emp_id) {
                    array_push($columns, "schd_status");
                    array_push($content, "1");
                } else {
                    array_push($columns, "schd_status");
					
					if($aprv_ctrl == "2")
					{
						//承認依頼しない(管理画面のその他タグで指定)
						array_push($content, "3");
					}
					else
					{
						array_push($content, "2");//未承認
					}
                }

                $sql = "insert into $new_table_name (" . join(", ", $columns) . ") values (";
                $ins = insert_into_table($con, $sql, $content, $fname);
                if ($ins == 0) {
                    pg_query($con, "rollback");
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }
                // 登録メール用
                $arr_add_emp_id[] = $tmp_emp_id;
            }
        }
    }

    $schedules_for_del_mail = array();
    // 更新元のスケジュール情報を削除
    foreach ($upd_schedules as $tmp_schedule) {
        // 登録対象の職員以外を削除
        $tmp_emp_id = $tmp_schedule["emp_id"];
        if (in_array($tmp_emp_id, $new_emp_id) && ($tmp_schedule["timeless"] == $timeless)) { // 時刻指定が変更されていないことを条件に追加
            continue;
        }
        $tmp_table_name = ($tmp_schedule["timeless"] == "f") ? "schdmst" : "schdmst2";
        $tmp_schd_id = $tmp_schedule["id"];

        $sql = "delete from $tmp_table_name";
        $cond = "where schd_id = '$tmp_schd_id'";
        $del = delete_from_table($con, $sql, $cond, $fname);
        if ($del == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        // 削除メール用
        if ($tmp_schedule["approve_status"] == "3") {
            $schedules_for_del_mail[] = $tmp_schedule;
        }
    }

}

// トランザクションのコミット
pg_query($con, "commit");

// 行先一覧を配列に格納
if (($sendmail == "t" && count($arr_add_emp_id) > 0) ||
    (count($schedules_for_del_mail) > 0)) {
    $places = get_places($con, $fname);
}

if ($sendmail == "t") {
// 登録メール
    if (count($arr_add_emp_id) > 0) {
        $arr_add_emp_id = array_unique($arr_add_emp_id);
        $to_login_ids = array();
        foreach ($arr_add_emp_id as $tmp_emp_id) {
            // 自分以外の場合
            if ($tmp_emp_id != $login_emp_id) {
                $to_login_ids[] = get_mail_addr($con, $fname, $tmp_emp_id, $domain);
            }
        }
        $mail_schd_date_times = array();
        for ($i = 0, $j = count($arr_date); $i < $j; $i++) {
            $tmp_date = $arr_date[$i];
            $mail_schd_date = preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $tmp_date);
            if ($i == 0) {
                $mail_schd_time = ($timeless == "t") ? "時刻指定なし" : "{$schd_start_hrs}:{$schd_start_min}〜{$schd_dur_hrs}:{$schd_dur_min}";
            } else {
                $mail_schd_time = "　〃";
            }
            $mail_schd_date_times[] = "$mail_schd_date $mail_schd_time";
        }
        $mail_schd_date_time = implode("\n　　　　　", $mail_schd_date_times);
        $mail_schd_detail = str_replace("\n", "\n　　　　　", $schd_detail);
        $subject = "[CoMedix] スケジュール登録のお知らせ";

        $place_name = ($place_id == "0") ? "その他" : $places["$place_id"];
        if ($place_name != "") {
            $place_name .= " {$schd_plc}";
        } else {
            $place_name = "{$schd_plc}";
        }

        $message = "下記のスケジュールが登録されました。\n\n";
        $message .= "登録者　：{$emp_nm}\n";
        $message .= "タイトル：{$schd_title}\n";
        $message .= "行先　　：{$place_name}\n";
        $message .= "日時　　：{$mail_schd_date_time}\n";
        $message .= "詳細　　：{$mail_schd_detail}\n";
        $message = mb_convert_kana($message, "KV"); // 半角カナ変換
        $additional_headers = "From: ".mb_encode_mimeheader (mb_convert_encoding($emp_nm,"ISO-2022-JP","AUTO")) ." <$emp_login_id@$domain>";

        foreach ($to_login_ids as $to_login_id) {
            mb_send_mail($to_login_id, $subject, $message, $additional_headers);
        }

    }
}
// 削除メール
if (count($schedules_for_del_mail) > 0) {
    usort($schedules_for_del_mail, "sort_by_emp_id_datetime");

    $subject = "[CoMedix] スケジュール削除のお知らせ";
    $additional_headers = "From: ".mb_encode_mimeheader (mb_convert_encoding($emp_nm,"ISO-2022-JP","AUTO")) ." <$emp_login_id@$domain>";
    $message = "下記の承認済みスケジュールが削除されました。\n\n";
    $message .= "登録者　：{$emp_nm}\n";

    // 職員IDをキーとして処理
    $old_emp_id = "";

    foreach ($schedules_for_del_mail as $tmp_schedule) {

        $tmp_emp_id = $tmp_schedule["emp_id"];
        // 初回のみキー設定
        if ($old_emp_id == "") {
            $old_emp_id = $tmp_emp_id;
        }

        // 職員が変わった場合
        if ($old_emp_id != $tmp_emp_id) {
            // メールアドレス取得
            $to_addr = get_mail_addr($con, $fname, $old_emp_id, $domain);
            // メール送信
            mb_send_mail($to_addr, $subject, $message, $additional_headers);
            // キー設定
            $old_emp_id = $tmp_emp_id;
            // 初期化
            $message = "下記の承認済みスケジュールが削除されました。\n\n";
            $message .= "登録者　：{$emp_nm}\n";
        }

        $tmp_place_name = ($tmp_schedule["place_id"] == "0") ? "その他" : $places[$tmp_schedule["place_id"]];
        if ($tmp_place_name != "") {
            $tmp_place_name .= " {$tmp_schedule["place_detail"]}";
        } else {
            $tmp_place_name = "{$tmp_schedule["place_detail"]}";
        }
        $mail_schd_detail = str_replace("\n", "\n　　　　　", $tmp_schedule["detail"]);
        $message .= "-----------------------------------------------------\n";
        $message .= "タイトル：{$tmp_schedule["title"]}\n";
        $message .= "行先　　：{$tmp_place_name}\n";
        $message .= "日時　　：{$tmp_schedule["date"]} {$tmp_schedule["time"]}\n";
        $message .= "詳細　　：{$mail_schd_detail}\n";
        $message = mb_convert_kana($message, "KV"); // 半角カナ変換
    }

    // メールアドレス取得
    $to_addr = get_mail_addr($con, $fname, $old_emp_id, $domain);
    // メール送信
    mb_send_mail($to_addr, $subject, $message, $additional_headers);
}

// データベース接続を閉じる
pg_close($con);

// 親画面をリフレッシュし、自画面を閉じる
echo("<script type=\"text/javascript\">window.opener.location.reload();</script>");
echo("<script type=\"text/javascript\">self.close();</script>");

function sort_by_emp_id_datetime($schd1, $schd2) {
    if ($schd1["emp_id"] != $schd2["emp_id"]) {
        return strcmp($schd1["emp_id"], $schd2["emp_id"]);
    }
    if ($schd1["date"] != $schd2["date"]) {
        return strcmp($schd1["date"], $schd2["date"]);
    }
    if ($schd1["timeless"] != $schd2["timeless"]) {
        return strcmp($schd1["timeless"], $schd2["timeless"]) * -1;
    }
    if ($schd1["time"] != $schd2["time"]) {
        return strcmp($schd1["time"], $schd2["time"]);
    }
    return strcmp($schd1["id"], $schd2["id"]);
}

function get_mail_addr($con, $fname, $emp_id, $domain) {
    $sql = "select get_mail_login_id(emp_id) as mail_login_id, emp_lt_nm, emp_ft_nm from empmst";
    $cond = "where emp_id = '$emp_id'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $mail_addr = pg_fetch_result($sel, 0, "mail_login_id");
    $emp_nm = pg_fetch_result($sel, 0, "emp_lt_nm") . pg_fetch_result($sel, 0, "emp_ft_nm");
    $to_addr = mb_encode_mimeheader (mb_convert_encoding($emp_nm,"ISO-2022-JP","AUTO")) ." <$mail_addr@$domain>";
    return $to_addr;
}
