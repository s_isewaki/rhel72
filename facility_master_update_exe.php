<?
require_once("about_comedix.php");
?>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" action="facility_master_update.php" method="post">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="back" value="t">
<input type="hidden" name="cate_id" value="<? echo($cate_id); ?>">
<input type="hidden" name="old_cate_id" value="<? echo($old_cate_id); ?>">
<input type="hidden" name="facility_id" value="<? echo($facility_id); ?>">
<input type="hidden" name="facility_name" value="<? echo($facility_name); ?>">
<input type="hidden" name="remarks" value="<? echo($remarks); ?>">
<input type="hidden" name="auth_flg" value="<? echo($auth_flg); ?>">
<input type="hidden" name="has_admin_flg" value="<? echo($has_admin_flg); ?>">
<input type="hidden" name="show_type_flg" value="<? echo($show_type_flg); ?>">
<input type="hidden" name="show_repeat_flg" value="<? echo($show_repeat_flg); ?>">
<input type="hidden" name="show_timeless_flg" value="<? echo($show_timeless_flg); ?>">
<input type="hidden" name="content_type" value="<? echo($content_type); ?>">
<input type="hidden" name="tmpl_content" value="<?
$tmpl_content = eregi_replace("_(textarea)", "/\\1", $tmpl_content);
$tmpl_content = eregi_replace("_(script)", "/\\1", $tmpl_content);
echo(h($tmpl_content)); ?>">
<input type="hidden" name="detail" value="<? echo($detail); ?>">
<input type="hidden" name="presentation" value="<? echo($presentation); ?>">
<input type="hidden" name="capacity" value="<? echo($capacity); ?>">
<input type="hidden" name="start_date1" value="<? echo($start_date1); ?>">
<input type="hidden" name="start_date2" value="<? echo($start_date2); ?>">
<input type="hidden" name="start_date3" value="<? echo($start_date3); ?>">
<input type="hidden" name="end_date_flg" value="<? echo($end_date_flg); ?>">
<input type="hidden" name="end_date1" value="<? echo($end_date1); ?>">
<input type="hidden" name="end_date2" value="<? echo($end_date2); ?>">
<input type="hidden" name="end_date3" value="<? echo($end_date3); ?>">
<input type="hidden" name="end_month" value="<? echo($end_month); ?>">
</form>
<?
require("get_values.ini");
require_once("Cmx.php");
require_once("aclg_set.php");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 41, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// 入力チェック

//// カテゴリ

////// 未入力
if ($cate_id == "") {
	echo("<script type=\"text/javascript\">alert('カテゴリを選択してください。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}

//// 名称

////// 未入力
if ($facility_name == "") {
	echo("<script type=\"text/javascript\">alert('名称を入力してください。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}

////// 文字列長
if (strlen($facility_name) > 100) {
	echo("<script type=\"text/javascript\">alert('名称が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}

// 予約開始日妥当性チェック
$start_date = $start_date1.$start_date2.$start_date3;
$start_date = str_replace("-", "", $start_date);
if ($start_date != "" &&
	!checkdate($start_date2, $start_date3, $start_date1)) {
	echo("<script language=\"javascript\">alert(\"予約開始日が正しくありません\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}

// 日付指定の場合
if ($end_date_flg == "1") {
	// 予約期限日妥当性チェック
	$end_date = $end_date1.$end_date2.$end_date3;
	$end_date = str_replace("-", "", $end_date);
	if ($end_date != "" &&
		!checkdate($end_date2, $end_date3, $end_date1)) {
		echo("<script language=\"javascript\">alert(\"予約期限日が正しくありません\");</script>\n");
		echo("<script language=\"javascript\">history.back();</script>\n");
		exit;
	}
	// 開始日、期限日の関連チェック
	if ($start_date != "" && $end_date != "" && $start_date > $end_date) {
		echo("<script language=\"javascript\">alert(\"予約開始日は予約期限日以前にしてください\");</script>\n");
		echo("<script language=\"javascript\">history.back();</script>\n");
	}

	// 月をクリア
	$end_month = "";
} else {
// 期間指定の場合
	if ($end_month == "-") {
		echo("<script language=\"javascript\">alert(\"期間指定の場合は月数を指定してください\");</script>\n");
		echo("<script language=\"javascript\">history.back();</script>\n");
		exit;
	}
	// 予約期限日をクリア
	$end_date = "";
}
// 詳細入力ありの場合
if ($detail == "on") {
	$presentation = ($presentation == "t") ? "t" : "f";

	//// 収容人数の妥当性をチェック
	if ($capacity != "" && preg_match("/^[0-9]{1,4}$/", $capacity) == 0) {
		echo("<script type=\"text/javascript\">alert('収容人数は半角数字1〜4桁で入力してください。');</script>");
		echo("<script type=\"text/javascript\">document.items.submit();</script>");
		exit;
	}

// 詳細入力なしの場合
} else {
	$presentation = null;
	$capacity = null;
}

if ($content_type == "2" && strlen($tmpl_content) == 0) {
	echo("<script type=\"text/javascript\">alert('テンプレートを指定してください。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}

if ($content_type == "2" && strpos($tmpl_content, "<? // XML") === false) {
	echo("<script type=\"text/javascript\">alert('XML作成用コードがありません。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}

/*
if ($content_type == "2" && strpos($tmpl_content, "dump_mem") === false) {
	echo("<script type=\"text/javascript\">alert('テンプレートが最後まで設定されていません。確認してください。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
*/
// 内容チェック
$content_upper = strtoupper($tmpl_content);
$sqlchkflg = true;

$chkarray = array("INSERT", "UPDATE", "DELETE", "CONNECT2DB", "SELECT_FROM_TABLE", "PG_CONNECT", "PG_EXEC", "<FORM");
foreach($chkarray as $chkstr){
	if(!(strpos($content_upper, $chkstr) === false)){
		$sqlchkflg = false;
		break;
	}
}

if($sqlchkflg == false){
	echo("<script language=\"javascript\">alert('指定できない文字列があります($chkstr)。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}

// 'エスケープ
$tmpl_content_escape = pg_escape_string($tmpl_content);

$emp_id = get_emp_id($con, $session, $fname);

// トランザクションを開始
pg_query($con, "begin");

// 更新前の権限設定・管理者設定フラグを取得
$sql = "select auth_flg, has_admin_flg, fclhist_id from facility";
$cond = "where facility_id = $facility_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$pre_auth_flg = pg_fetch_result($sel, 0, "auth_flg");
$pre_has_admin_flg = pg_fetch_result($sel, 0, "has_admin_flg");
$fclhist_id = pg_fetch_result($sel, 0, "fclhist_id");

// テンプレートの変更確認
$hist_flg = false;
if ($fclhist_id == 0) {
	$fclhist_id = 1;
	$hist_flg = true;
} else {
	// テンプレート最新情報を取得
	$sql = "select fclhist_content_type, fclhist_content from fclhist";
	$cond = "where facility_id = $facility_id and fclhist_id = $fclhist_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$fclhist_content_type = pg_fetch_result($sel, 0, "fclhist_content_type");
	$fclhist_content = pg_fetch_result($sel, 0, "fclhist_content");
	// 入力内容と比較
	if ($content_type != $fclhist_content_type || $tmpl_content != $fclhist_content) {
	// 違う場合
		$fclhist_id++;
		$hist_flg = true;
	}
}

// 履歴レコード登録
if ($hist_flg == true) {
	$now = date("YmdHis");

	// 履歴レコード作成
	$sql = "insert into fclhist (facility_id, fclhist_id, fclhist_content_type, fclhist_content, fclhist_update_id, fclhist_up_date) values (";
	$content = array($facility_id, $fclhist_id, $content_type, $tmpl_content_escape, $emp_id, $now);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// 施設・設備レコードを更新
$sql = "update facility set";
$set = array("fclcate_id", "facility_name", "remarks", "presentation", "capacity", "auth_flg", "has_admin_flg", "fclhist_id", "show_type_flg", "show_repeat_flg", "start_date", "end_date_flg", "end_date", "end_month", "show_timeless_flg");
$setvalue = array($cate_id, $facility_name, $remarks, $presentation, $capacity, $auth_flg, $has_admin_flg, $fclhist_id, $show_type_flg, $show_repeat_flg, $start_date, $end_date_flg, $end_date, $end_month, $show_timeless_flg);
if ($pre_auth_flg != $auth_flg) {
	array_push($set, "ref_dept_st_flg", "ref_dept_flg", "ref_st_flg", "upd_dept_st_flg", "upd_dept_flg", "upd_st_flg");
	array_push($setvalue, "f", "1", "1", "f", "1", "1");
}
$cond = "where facility_id = $facility_id";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 権限設定「する」→「しない」の場合、予約可能職員情報を削除
if ($pre_auth_flg == "t" && $auth_flg != "t") {
	$sql = "delete from fclauth";
	$cond = "where facility_id = $facility_id";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

// 権限設定「しない」→「する」の場合、登録者を予約可能職員情報として登録
} else if ($pre_auth_flg != "t" && $auth_flg == "t") {
	$sql = "insert into fclauth (facility_id, emp_id) values (";
	$content = array($facility_id, $emp_id);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// 管理者設定「する」→「しない」の場合、施設・設備管理者情報を削除
if ($pre_has_admin_flg == "t" && $has_admin_flg != "t") {
	$sql = "delete from fcladmin";
	$cond = "where facility_id = $facility_id";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

// 管理者設定「しない」→「する」の場合、登録者を施設・設備管理者情報として登録
} else if ($pre_has_admin_flg != "t" && $has_admin_flg == "t") {
	$sql = "insert into fcladmin (facility_id, emp_id) values (";
	$content = array($facility_id, $emp_id);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// 予約データの設備カテゴリを更新
$sql = "update reservation set";
$set = array("fclcate_id");
$setvalue = array($cate_id);
$cond = "where facility_id = $facility_id";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$sql = "update reservation2 set";
$set = array("fclcate_id");
$setvalue = array($cate_id);
$cond = "where facility_id = $facility_id";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を切断
pg_close($con);

// 画面遷移
echo("<script type=\"text/javascript\">location.href = 'facility_master_menu.php?session=$session&opens=$cate_id';</script>");
?>
