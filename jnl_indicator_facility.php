<?php
include_once 'jnl_indicator.php';
class IndicatorFacility extends Indicator
{
    /**
     * @access protected
     */
    var $subCategoryArr = array(
        '0' => array(
            '1' => '�±�'
          , '2' => 'Ϸ��',
        ),
    );

    /**
     * @access protected
     */
    var $noArr = array(
        /* ---------- ������ �±� ---------- */
        '64' => array(
            'name'         => '���Է���(��������)',
            'file'         => 'jnl_application_indicator_monthly_report_according_to_day.php',
            'category'     => '0',
            'sub_category' => '1',
            'file_name'    => 'name_y_m',
        ),
        '74' => array(
            'name'         => '�±��̷�����񴵼Կ�����',
            'file'         => 'jnl_application_indicator_summary_of_number_of_monthly_report_patients_according_to_hospital.php',
            'category'     => '0',
            'sub_category' => '1',
            'file_name'    => 'name_y',
        ),
        '75' => array(
            'name'         => '�±��̷������(�԰ٷ��)',
            'file'         => 'jnl_application_indicator_hospital_monthly_report_facility.php',
            'category'     => '0',
            'sub_category' => '1',
            'file_name'    => 'name_y',
        ),
        '76' => array(
            'name'         => '������Ź԰�������',
            'file'         => 'jnl_application_indicator_outpatient_care_act_details_facility.php',
            'category'     => '0',
            'sub_category' => '1',
            'file_name'    => 'name_y_m',
        ),
        '77' => array(
            'name'         => '����������',
            'file'         => 'jnl_application_indicator_outpatient_daily_allowance_point_facility.php',
            'category'     => '0',
            'sub_category' => '1',
            'file_name'    => 'name_y_m',
        ),
        '78' => array(
            'name'         => '�������Ź԰�������',
            'file'         => 'jnl_application_indicator_inpatient_care_act_details_facility.php',
            'category'     => '0',
            'sub_category' => '1',
            'file_name'    => 'name_y_m',
        ),
        '79' => array(
            'name'         => '����������',
            'file'         => 'jnl_application_indicator_inpatient_daily_allowance_point_facility.php',
            'category'     => '0',
            'sub_category' => '1',
            'file_name'    => 'name_y_m',
        ),
        '80' => array(
            'name'         => '����������ɽ[���]',
            'file'         => 'jnl_application_indicator_medical_treatment_fee_generalization_table_medical_facility.php',
            'category'     => '0',
            'sub_category' => '1',
            'file_name'    => 'name_y_m',
        ),
        '81' => array(
            'name'         => '����������ɽ[����]',
            'file'         => 'jnl_application_indicator_medical_treatment_fee_generalization_table_dental_facility.php',
            'category'     => '0',
            'sub_category' => '1',
            'file_name'    => 'name_y_m',
        ),

        /* ---------- ������ �±�(����) ---------- */
        '306' => array(
            'name'         => '���Ẻ�ꡦ�������ʬ�Ϥ��к�',
            'file'         => 'jnl_indicator_return_analysis.php',
            'category'     => '0',
            'sub_category' => '1',
            'file_name'    => 'name_y_m',
        ),
        /* ---------- ������ Ϸ�� ---------- */
        '103' => array(
            'name'         => '���Է���(��������)',
            'file'         => 'jnl_application_indicator_mon_per_date_nurse_home.php',
            'category'     => '0',
            'sub_category' => '2',
            'file_name'    => 'name_y_m',
        ),
        '124' => array(
            'name'         => 'Ϸ���ֲû����ܡ�������Ӱ���',
            'file'         => 'jnl_application_indicator_mon_report_list_facility.php',
            'category'     => '0',
            'sub_category' => '2',
            'file_name'    => 'name_y',
        ),
        '125' => array(
            'name'         => '���������ٽ�',
            'file'         => 'jnl_application_indicator_medical_expense_detail_facility.php',
            'category'     => '0',
            'sub_category' => '2',
            'file_name'    => 'name_y_m',
        ),
        '126' => array(
            'name'         => 'ͽ�ɵ������������ٽ�',
            'file'         => 'jnl_application_indicator_medical_expense_detail_facility.php',
            'category'     => '0',
            'sub_category' => '2',
            'file_name'    => 'name_y_m',
        ),
    );

    /**
     * ���󥹥ȥ饯��
     * @param $arr
     */
    function IndicatorFacility($arr)
    {
        parent::Indicator($arr);
    }
}