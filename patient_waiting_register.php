<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
if ($path == "b" || $path == "c") {
	$func = "病床管理";
	$auth_no = 14;
} else {
	$func = "患者管理";
	$auth_no = 22;
}
?>
<title>CoMedix <? echo($func); ?>｜待機患者登録</title>
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_select_values.ini");
require_once("label_by_profile_type.ini");
require_once("show_select_years_wareki.ini");
require_once("inpatient_common.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, $auth_no, $fname);
if($checkauth == "0"){
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

$con = connect2db($fname);

$profile_type = get_profile_type($con, $fname);

// チェック情報を取得
$sql = "select * from bedcheckwait";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
for ($i = 1; $i <= 11; $i++) {
	$varname = "display$i";
	$$varname = pg_fetch_result($sel, 0, "display$i");
}

// 医療機関マスタを取得
$sql = "select a.mst_cd, a.mst_name, b.rireki_index from institemmst a left join (select mst_cd, max(rireki_index) as rireki_index from institemrireki group by mst_cd) b on b.mst_cd = a.mst_cd";
$cond = "order by a.mst_cd";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$intro_insts = array();
while ($row = pg_fetch_array($sel)) {
	$intro_insts[$row["mst_cd"]] = array(
		"name"   => $row["mst_name"],
		"rireki" => $row["rireki_index"],
		"sects"  => array()
	);
}

// 診療科マスタを取得（最新の履歴のみ）
$sql = "select a.* from institemrireki a inner join (select mst_cd, max(rireki_index) as rireki_index from institemrireki group by mst_cd) b on b.mst_cd = a.mst_cd and b.rireki_index = a.rireki_index";
$cond = "where a.disp_flg order by a.disp_order";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
while ($row = pg_fetch_array($sel)) {
	$intro_insts[$row["mst_cd"]]["sects"][$row["item_cd"]]["name"] = $row["item_name"];
	for ($i = 1; $i <= 10; $i++) {
		if ($row["doctor_name$i"] == "" || $row["doctor_hide$i"] == "t") {
			continue;
		}
		$intro_insts[$row["mst_cd"]]["sects"][$row["item_cd"]]["doctors"][$i] = $row["doctor_name$i"];
	}
}

// キャンセル理由マスタを取得
list($ccl_rsn_rireki, $ccl_rsn_master) = get_rireki_index_and_master($con, "A309", $fname);

// 病床区分マスタを取得
list($bed_type_rireki, $bed_type_master) = get_rireki_index_and_master($con, "A310", $fname);

// 初期表示時
if ($back != "t") {
	$wareki_flg = "t";

// ポストバック時
} else {
	if ($charge_emp_id != "") {
		$sql = "select emp_lt_nm, emp_ft_nm from empmst";
		$cond = "where emp_id = '$charge_emp_id'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$charge = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");
	}
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function wareki_flg_onchange() {
	if (document.patient.wareki_flg.checked == true) {
		birth_yr_onchange();
		document.getElementById('year_div').style.display = "none";
		document.getElementById('year_wareki_div').style.display = "";
	} else {
		document.getElementById('year_div').style.display = "";
		document.getElementById('year_wareki_div').style.display = "none";
	}
}

<?
// 改元データを取得
$era_data = get_era_data();
$era_ymd_str = "";
$gengo_str = "";
for ($i = 1; $i < count($era_data); $i++) {
	if ($i > 1) {
		$era_ymd_str .= ",";
		$gengo_str .= ",";
	}
	$split_data = split(",", $era_data[$i]);
	$era_ymd_str .= "'".$split_data[0]."'";
	$gengo_str .= "'".$split_data[1]."'";
}
?>
var ymd_arr = new Array(<?=$era_ymd_str?>);
var gengo_arr = new Array(<?=$gengo_str?>);

function birth_yr_onchange() {
	var year = document.patient.birth_yr.value;
	var mon = document.patient.birth_mon.value;
	var day = document.patient.birth_day.value;
	var ymd = year+mon+day;

	find_idx = -1;
	for (var i=0; i<ymd_arr.length; i++) {
		if (year == ymd_arr[i].substring(0,4)) {
			find_idx = i;
			break;
		}
	}

	document.patient.birth_yr2.value = year;
	if (find_idx > -1 && ymd < ymd_arr[find_idx]) {
		document.patient.birth_yr2.selectedIndex += 1;
	}
}

function birth_yr2_onchange() {
	document.patient.birth_yr.value = document.patient.birth_yr2.value;
}

function check_wareki() {

	if (document.patient.wareki_flg.checked == false) {
		return true;
	}

	var year = document.patient.birth_yr.value;
	var mon = document.patient.birth_mon.value;
	var day = document.patient.birth_day.value;
	var ymd = year+mon+day;

	find_idx = -1;
	for (var i=0; i<ymd_arr.length; i++) {
		ymp_tmp = ymd_arr[i].substring(0,4);
		if (year == ymp_tmp) {
			find_idx = i;
			break;
		}
	}

	if (find_idx == -1) {
		return true;
	}

	if (ymd < ymd_arr[find_idx]) {
		gengo = gengo_arr[find_idx + 1];
	} else {
		gengo = gengo_arr[find_idx];
	}

	obj = document.patient.birth_yr2;
	var wareki_tmp = obj.options[obj.selectedIndex].text;
	select_gengo = wareki_tmp.substring(0,2);
	if (select_gengo != gengo) {
		alert('年月日を正しく指定してください');
		return false;
	}
	return true;
}

function change_age() {
	var birth_year = document.patient.birth_yr.value;
	var birth_month = document.patient.birth_mon.value;
	var birth_day = document.patient.birth_day.value;

	var today = new Date();
	var this_year = today.getFullYear();
	var this_month = today.getMonth() + 1;
	var this_day = today.getDate();

	var age = this_year - birth_year;
	if (this_month < birth_month || (this_month == birth_month && this_day < birth_day)) {
		age--;
	}

	document.getElementById('age_label').innerHTML = age + '歳';
}

var childWindows = [];

function openPatientSearchWindow() {
	openChildWindow('patient_search_popup.php?session=<? echo($session); ?>&cond_id=' + document.patient.pt_id.value, 'psp', 'width=640,height=600,scrollbars=yes');
}

function openEmployeeList() {
	openChildWindow('emplist_popup.php?session=<? echo($session); ?>&mode=21&item_id=1', 'ep', 'width=720,height=600,scrollbars=yes,resizable=yes');
}

function openInstSearchWindow() {
	openChildWindow('bed_inst_search.php?session=<? echo($session); ?>', 'bis', 'width=640,height=480,scrollbars=yes');
}

function openDiseaseSearchWindow() {
	openChildWindow('summary_byoumei_select.php?session=<? echo($session); ?>&subsys=patient', 'sbs', 'width=1000,height=700,scrollbars=yes');
}

function add_target_list(item_id, emp_id, emp_name) {
	document.patient.charge.value = emp_name;
	document.patient.charge_emp_id.value = emp_id;
}

function clearCharge() {
	add_target_list(null, '', '');
}

function selectInst(cd) {
	switch (cd) {
<? foreach ($intro_insts as $tmp_inst_cd => $tmp_inst) { ?>
	case '<? echo($tmp_inst_cd); ?>':
		document.patient.intro_inst_cd.value = '<? echo($tmp_inst_cd); ?>';
		document.patient.intro_inst_name.value = '<? echo($tmp_inst["name"]); ?>';
		document.patient.intro_sect_rireki.value = '<? echo($tmp_inst["rireki"]); ?>';
		break;
<? } ?>
	}
	setIntroSectOptions();
}

function setIntroSectOptions(defaultSectCd, defaultDoctorNo) {
	if (!document.patient.intro_sect_cd) return;

	clearOptions(document.patient.intro_sect_cd);

	var instCd = document.patient.intro_inst_cd.value;
	if (!instCd) return;

	switch (instCd) {
<? foreach ($intro_insts as $tmp_inst_cd => $tmp_inst) { ?>
	case '<? echo($tmp_inst_cd); ?>':
<? foreach ($tmp_inst["sects"] as $tmp_sect_cd => $tmp_sect) { ?>
		addOption(document.patient.intro_sect_cd, '<? echo($tmp_sect_cd); ?>', '<? echo($tmp_sect["name"]); ?>', defaultSectCd);
<? } ?>
		break;
<? } ?>
	}

	setIntroDoctorOptions(defaultDoctorNo);
}

function setIntroDoctorOptions(defaultDoctorNo) {
	if (!document.patient.intro_doctor_no) return;

	clearOptions(document.patient.intro_doctor_no);

	var sectCd = document.patient.intro_sect_cd.value;
	if (!sectCd) return;

	var instCd = document.patient.intro_inst_cd.value;
<? foreach ($intro_insts as $tmp_inst_cd => $tmp_inst) { ?>
	<? foreach ($tmp_inst["sects"] as $tmp_sect_cd => $tmp_sect) { ?>
	if (instCd == '<? echo($tmp_inst_cd); ?>' && sectCd == '<? echo($tmp_sect_cd); ?>') {
		<? foreach ($tmp_sect["doctors"] as $tmp_doctor_no => $tmp_doctor_name) { ?>
			addOption(document.patient.intro_doctor_no, '<? echo($tmp_doctor_no); ?>', '<? echo($tmp_doctor_name); ?>', defaultDoctorNo);
		<? } ?>
	}
	<? } ?>
<? } ?>
}

function clearInst() {
	document.patient.intro_inst_cd.value = '';
	document.patient.intro_inst_name.value = '';
	document.patient.intro_sect_rireki.value = '';
	if (document.patient.intro_sect_cd) clearOptions(document.patient.intro_sect_cd);
	if (document.patient.intro_doctor_no) clearOptions(document.patient.intro_doctor_no);
}

function call_back_summary_byoumei_select(caller, result) {
	document.patient.disease.value = result.byoumei;
}

function openChildWindow(url, name, option) {
	var childWindow = window.open(url, name, option);
	childWindow.focus();
	childWindows.push(childWindow);
}

function closeChildWindows() {
	while (childWindows.length > 0) {
		var childWindow = childWindows.pop();
		if (!childWindow.closed) childWindow.close();
	}
}

function addOption(box, value, text, selected) {
	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	if (selected == value) opt.selected = true;
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
}

function clearOptions(box) {
	for (var i = box.length - 1; i > 0; i--) {
		box.options[i] = null;
	}
}

function initPage() {
	switch ('<? echo($intro_inst_cd); ?>') {
<? foreach ($intro_insts as $tmp_inst_cd => $tmp_inst) { ?>
	case '<? echo($tmp_inst_cd); ?>':
		if (document.patient.intro_inst_name) {
			document.patient.intro_inst_name.value = '<? echo($tmp_inst["name"]); ?>';
		}
		document.patient.intro_sect_rireki.value = '<? echo($tmp_inst["rireki"]); ?>';
		break;
<? } ?>
	}
	setIntroSectOptions('<? echo($intro_sect_cd); ?>', '<? echo($intro_doctor_no); ?>');
	setCancelDisabled();
}

function setCancelDisabled() {
	var disabled = !document.patient.progress[1].checked;
	document.patient.ccl_psn[0].disabled = disabled;
	document.patient.ccl_psn[1].disabled = disabled;
	document.patient.ccl_psn_dtl.disabled = disabled;
	document.patient.ccl_rsn_cd.disabled = disabled;
	document.patient.ccl_rsn.disabled = disabled;
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="initPage();wareki_flg_onchange();change_age();">
<center>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>待機患者登録</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="closeChildWindows();window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form action="patient_waiting_insert.php" method="post" name="patient" onsubmit="return check_wareki();">
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($_label_by_profile["PATIENT_ID"][$profile_type]); ?></font></td>
<td colspan="3">
<input name="pt_id" type="text" value="<? echo $pt_id; ?>" maxlength="12" style="ime-mode:inactive;">
<input type="button" value="検索" onclick="openPatientSearchWindow();">
</td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">姓（漢字）</font></td>
<td><input name="lt_nm" type="text" value="<? echo $lt_nm; ?>" maxlength="10" style="ime-mode:active;"></td>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">名（漢字）</font></td>
<td><input name="ft_nm" type="text" value="<? echo $ft_nm; ?>" maxlength="10" style="ime-mode:active;"></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">姓（かな）</font></td>
<td><input name="lt_kana_nm" type="text" value="<? echo $lt_kana_nm; ?>" maxlength="10" style="ime-mode:active;"></td>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">名（かな）</font></td>
<td><input name="ft_kana_nm" type="text" value="<? echo $ft_kana_nm; ?>" maxlength="10" style="ime-mode:active;"></td>
</tr>
<tr height="22">
<td width="20%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">生年月日</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<span id="year_div">
<select name="birth_yr" onchange="birth_yr_onchange();change_age();">
<? show_select_years(120, $birth_yr);?>
</select></span><span id="year_wareki_div">
<select name="birth_yr2" onchange="birth_yr2_onchange();change_age();">
<? show_select_years_jp(120, $birth_yr2, "", ""); ?>
</select></span>/<select name="birth_mon" onchange="change_age();"><? show_select_months($birth_mon); ?></select>/<select name="birth_day" onchange="change_age();"><? show_select_days($birth_day); ?></select>
<input type="checkbox" name="wareki_flg" value="t" <? if ($wareki_flg == "t") echo(" checked"); ?> onclick="wareki_flg_onchange();">和暦
</font></td>
<td width="16%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">年齢</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><span id="age_label"></span></font></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">性別</font></td>
<td><select name="sex">
<option value="0"<? if ($sex == "0") {echo " selected";} ?>>不明
<option value="1"<? if ($sex == "1") {echo " selected";} ?>>男性
<option value="2"<? if ($sex == "2") {echo " selected";} ?>>女性
</select></td>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職業</font></td>
<td><input type="text" name="job" value="<? echo $job; ?>" style="ime-mode:active;"></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">進捗</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="progress" value="1"<? if ($progress == "" || $progress == "1") {echo " checked";} ?> onclick="setCancelDisabled();">待機中
<input type="radio" name="progress" value="2"<? if ($progress == "2") {echo " checked";} ?> style="margin-left:10px;" onclick="setCancelDisabled();">待機キャンセル
<input type="radio" name="progress" value="5"<? if ($progress == "5") {echo " checked";} ?> style="margin-left:10px;" onclick="setCancelDisabled();">その他
<input type="radio" name="progress" value="3"<? if ($progress == "3") {echo " checked";} ?> style="margin-left:10px;" onclick="setCancelDisabled();">入院予定
<input type="radio" name="progress" value="4"<? if ($progress == "4") {echo " checked";} ?> style="margin-left:10px;" onclick="setCancelDisabled();">入院済み
</font></td>
</tr>
<? if ($display1 == "t") { ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">当院担当者</font></td>
<td colspan="3">
<input type="text" name="charge" value="<? echo($charge); ?>" size="30" disabled>
<input type="button" value="職員名簿" onclick="openEmployeeList();">
<input type="button" value="クリア" onclick="clearCharge();">
</td>
</tr>
<? } ?>
<? if ($display2 == "t") { ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">紹介元医療機関</font></td>
<td colspan="3">
<input type="text" name="intro_inst_name" value="" size="25" disabled>
<input type="button" value="検索" onclick="openInstSearchWindow();">
<input type="button" value="クリア" onclick="clearInst();">
</td>
</tr>
<? } ?>
<? if ($display3 == "t") { ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">診療科</font></td>
<td colspan="3">
<select name="intro_sect_cd" onchange="setIntroDoctorOptions();">
<option value="">　　　　　</option>
</select>
<input type="text" name="intro_sect_text" value="<? echo $intro_sect_text; ?>" style="ime-mode:active;">
</td>
</tr>
<? } ?>
<? if ($display4 == "t") { ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">前医</font></td>
<td colspan="3">
<select name="intro_doctor_no">
<option value="">　　　　　</option>
</select>
<input type="text" name="intro_doctor_text" value="<? echo $intro_doctor_text; ?>" style="ime-mode:active;">
</td>
</tr>
<? } ?>
<? if ($display11 == "t") { ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">MSW</font></td>
<td colspan="3"><input type="text" name="msw" value="<? echo $msw; ?>" style="ime-mode:active;"></td>
</tr>
<? } ?>
<? if ($display6 == "t") { ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">傷病名</font></td>
<td colspan="3">
<input type="text" name="disease" size="50" value="<? echo($disease); ?>" style="ime-mode:active;">
<input type="button" value="病名検索" onclick="openDiseaseSearchWindow();">
</td>
</tr>
<? } ?>
<? if ($display7 == "t") { ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発症日</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="patho_from_year">
<option value="-">
<? show_select_years_span(1960, date("Y") + 1, $patho_from_year, false); ?>
</select>/<select name="patho_from_month">
<? show_select_months($patho_from_month, true); ?>
</select>/<select name="patho_from_day">
<? show_select_days($patho_from_day, true); ?>
</select> 〜
<select name="patho_to_year">
<option value="-">
<? show_select_years_span(1960, date("Y") + 1, $patho_to_year, false); ?>
</select>/<select name="patho_to_month">
<? show_select_months($patho_to_month, true); ?>
</select>/<select name="patho_to_day">
<? show_select_days($patho_to_day, true); ?>
</select>
</font></td>
</tr>
<? } ?>
<? if ($display8 == "t") { ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">備考（ADL等）</font></td>
<td colspan="3"><input type="text" name="note" size="70" value="<? echo($note); ?>" style="ime-mode:active;"></td>
</tr>
<? } ?>
<? if ($display5 == "t") { ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">認知症の有無</font></td>
<td colspan="3"><select name="dementia">
<option value=""></option>
<option value="f"<? if ($dementia == "f") {echo(" selected");} ?>>無し</option>
<option value="t"<? if ($dementia == "t") {echo(" selected");} ?>>有り</option>
</select></td>
</tr>
<? } ?>
<?
if ($reception_year == "") $reception_year = date("Y");
if ($reception_month == "") $reception_month = date("m");
if ($reception_day == "") $reception_day = date("d");
if ($reception_hour == "") $reception_hour = date("H");
if ($reception_min == "") $reception_min = date("i");
?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">受付日時</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="reception_year">
<? show_select_years_span(1960, date("Y") + 1, $reception_year, false); ?>
</select>/<select name="reception_month">
<? show_select_months($reception_month); ?>
</select>/<select name="reception_day">
<? show_select_days($reception_day); ?>
</select>
<select name="reception_hour">
<? show_hour_options_0_23($reception_hour, false); ?>
</select>：<select name="reception_min">
<? show_min_options_00_59($reception_min, false); ?>
</select>
</font></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">最終確認日時</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="verified_year">
<option value="-"></option>
<? show_select_years_span(1960, date("Y") + 1, $verified_year, false); ?>
</select>/<select name="verified_month">
<option value="-"></option>
<? show_select_months($verified_month); ?>
</select>/<select name="verified_day">
<option value="-"></option>
<? show_select_days($verified_day); ?>
</select>
<select name="verified_hour">
<option value="-"></option>
<? show_hour_options_0_23($verified_hour, false); ?>
</select>：<select name="verified_min">
<option value="-"></option>
<? show_min_options_00_59($verified_min, false); ?>
</select>
</font></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床区分</font></td>
<td colspan="3">
<select name="bed_type_cd">
<option value=""<? if ($bed_type_cd == "") {echo " selected";} ?>>
<? foreach ($bed_type_master as $tmp_bed_type_cd => $tmp_bed_type_name) { ?>
<option value="<? echo($tmp_bed_type_cd); ?>"<? if ($tmp_bed_type_cd == $bed_type_cd) {echo " selected";} ?>><? echo($tmp_bed_type_name); ?>
<? } ?>
</select><br>
</td>
</tr>
<? if ($display9 == "t") { ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">調整状況1</font></td>
<td colspan="3"><input type="text" name="status1" value="<? echo($status1); ?>" size="70" style="ime-mode:active;"></td>
</tr>
<? } ?>
<? if ($display10 == "t") { ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">調整状況2</font></td>
<td colspan="3"><input type="text" name="status2" value="<? echo($status2); ?>" size="70" style="ime-mode:active;"></td>
</tr>
<? } ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">キャンセル連絡者</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="ccl_psn" value="1"<? if ($ccl_psn != "2") {echo " checked";} ?>>本人
<input type="radio" name="ccl_psn" value="2"<? if ($ccl_psn == "2") {echo " checked";} ?> style="margin-left:10px;">その他
&nbsp;詳細 <input type="text" name="ccl_psn_dtl" value="<? echo $ccl_psn_dtl; ?>" style="ime-mode:active;">
</font></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">キャンセル理由</font></td>
<td colspan="3">
<select name="ccl_rsn_cd">
<option value=""<? if ($ccl_rsn_cd == "") {echo " selected";} ?>>
<? foreach ($ccl_rsn_master as $tmp_ccl_rsn_cd => $tmp_ccl_rsn_name) { ?>
<option value="<? echo($tmp_ccl_rsn_cd); ?>"<? if ($tmp_ccl_rsn_cd == $ccl_rsn_cd) {echo " selected";} ?>><? echo($tmp_ccl_rsn_name); ?>
<? } ?>
</select><br>
<textarea cols="50" rows="3" wrap="virtual" name="ccl_rsn" style="ime-mode:active;"><? echo($ccl_rsn); ?></textarea>
</td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="charge_emp_id" value="<? echo($charge_emp_id); ?>">
<input type="hidden" name="intro_inst_cd" value="<? echo($intro_inst_cd); ?>">
<input type="hidden" name="intro_sect_rireki" value="<? echo($intro_sect_rireki); ?>">
<input type="hidden" name="bed_type_rireki" value="<? echo($bed_type_rireki); ?>">
<input type="hidden" name="ccl_rsn_rireki" value="<? echo($ccl_rsn_rireki); ?>">
<input type="hidden" name="path" value="<? echo($path); ?>">
<input type="hidden" name="list_year" value="<? echo($list_year); ?>">
<input type="hidden" name="list_month" value="<? echo($list_month); ?>">
<input type="hidden" name="list_day" value="<? echo($list_day); ?>">
<input type="hidden" name="list_year2" value="<? echo($list_year2); ?>">
<input type="hidden" name="list_month2" value="<? echo($list_month2); ?>">
<input type="hidden" name="list_day2" value="<? echo($list_day2); ?>">
</form>
</center>
</body>
<? pg_close($con); ?>
</html>
