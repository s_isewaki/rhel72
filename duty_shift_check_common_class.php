<?php
//************************************************************************
// 勤務シフト作成　勤務シフト作成画面
// エラーチェックＣＬＡＳＳ
//************************************************************************
require_once("about_postgres.php");
require_once("about_error.php");
require_once("duty_shift_auto_bunsan_common_class.php");
require_once("duty_shift_auto_need_class.php");
require_once("duty_shift_auto_pattern_class.php");
require_once("duty_shift_auto_hindo_class.php");
require_once("date_utils.php");
require_once("work_admin_timecard_check_class.php");
require_once("timecard_common_class.php");

class duty_shift_check_common_class
{
	/*************************************************************************/
	// コンストラクタ
	/*************************************************************************/
	function duty_shift_check_common_class($group_id, $con, $fname)
	{
		//自動シフトクラス
		$this->auto = new duty_shift_auto_bunsan_common_class($group_id, $con, $fname);
		
		// 呼び出し元ファイル名を保持
		$this->file_name = $fname;
		// DBコネクションを保持
		$this->_db_con = $con;
		
		// 勤務シフトグループID
		$this->group_id = $group_id;
		
		// 勤務グループの情報を取得する
		$this->group_table = $this->auto->duty_shift_group_table();
		
		// 勤務パターンの情報を取得する
		$this->atdptn_table = $this->auto->duty_shift_pattern_table();
		
		// 組み合わせ指定の情報を取得する
		$this->okptn_obj = new ok_pattern_class($group_id, $con, $fname);
		
		// 組み合わせ禁止の情報を取得する
		$this->ngptn_obj = new ng_pattern_class($group_id, $con, $fname);

		// エラー表示の最大件数
		$this->max_err_cnt = 23;
	}

	/*************************************************************************/
	// 列チェック
	// @param	$plan_array				勤務シフト情報
	// @param	$duty_y					表示年
	// @param	$duty_m					表示月
	// @param	$start_date				開始日
	// @param	$end_date				終了日
	//
	// @return	$ret	エラー情報
	/*************************************************************************/
	function check_columns(
		$plan_array,
		$duty_y,
		$duty_m,
		$start_date,
		$end_date
		)
	{
		///-----------------------------------------------------------------------------
		// 勤務スタッフの情報を取得する
		///-----------------------------------------------------------------------------
		$this->staff_table = $this->auto->duty_shift_staff_table($plan_array, $duty_y, $duty_m);
		
		///-----------------------------------------------------------------------------
		// 必要人数オブジェクト
		///-----------------------------------------------------------------------------
		$this->need_obj = new duty_shift_auto_need_class($this->group_id, $duty_y, $duty_m, $start_date, $end_date, $this->_db_con, $this->file_name);
		$date_table = $this->need_obj->get_date_table();
		
		///-----------------------------------------------------------------------------
		// plan_arrayからシフトデータを取得する
		///-----------------------------------------------------------------------------
		for ($i=0; $i<count($plan_array); $i++) {
			$emp_id = $plan_array[$i]["staff_id"];
			$k = 1;
			foreach ( $date_table as $date => $date_row ) {
				$this->staff_table[$emp_id]['atdbk'][$date]['pattern'] = $plan_array[$i]["atdptn_ptn_id_$k"];
				$this->staff_table[$emp_id]['atdbk'][$date]['reason'] = $plan_array[$i]["reason_$k"];
				$k++;
			}
		}
		$this->auto->staff_table = $this->staff_table;
		
		///-----------------------------------------------------------------------------
		// 同自休暇人数上限数生成。$dmyは捨てる
		///-----------------------------------------------------------------------------
		$dmy = $this->auto->auto_vac_set_get($date_table);
		
		///-----------------------------------------------------------------------------
		// ループ
		///-----------------------------------------------------------------------------
		$cnt = 1;
		$err = array();
		foreach ( $date_table as $date => $date_row ) {
			$err_cnt = 1;
			$err_msg = "";
			$err_abb = "";
			
			// すでに割り当てられているシフトの集計
			foreach ( $this->staff_table as $emp_id => $staff ) {
				if ( !empty($staff['atdbk'][$date]['pattern']) ) {
					$this->need_obj->minus($date, $staff['atdbk'][$date]['pattern'], $staff['job_id'], $staff['team_id'], $staff['sex'], $staff['st_id']);
				}
			}
			
			// 少ない場合
			if ($this->group_table["need_less_chk_flg"] == "t") {			
				$less_array = $this->need_obj->get_less_shift($date);
				foreach ($less_array as $r) {
					if ($this->max_err_cnt <= $err_cnt) {
						break;
					}
					$err_msg .= sprintf("(%02d)%s-%s：少ない %d人<br>", $err_cnt++, $r["job_name"], $r["ptn_name"], $r["cnt"]);
					$err_abb = empty($err_abb) ? "少" : $err_abb;
				}
			}
			// 多い場合
			if ($this->group_table["need_more_chk_flg"] == "t") {			
				$more_array = $this->need_obj->get_more_shift($date);
				foreach ($more_array as $r) {
					if ($this->max_err_cnt <= $err_cnt) {
						break;
					}
					$err_msg .= sprintf("(%02d)%s-%s：多い %d人<br>", $err_cnt++, $r["job_name"], $r["ptn_name"], $r["cnt"]);
					$err_abb = empty($err_abb) ? "多" : $err_abb;
				}
			}
			
			$vacation_counter = 0; // 当日休暇人数
			$sex_pattern = array();
			foreach ( $this->staff_table as $emp_id => $staff ) {
				$ptn = $staff['atdbk'][$date]['pattern'];
				
				// 性別固定チェック
				if ($this->group_table["sex_pattern"][$ptn]) {
					if (empty($sex_pattern[$ptn]['sex'])) {
						$sex_pattern[$ptn]['sex'] = $staff['sex'];
					}
					elseif ($sex_pattern[$ptn]['sex'] != $staff['sex']) {
						$sex_pattern[$ptn]['flg'] = true;
					}
					$sex_pattern[$ptn]['list'][] = "{$staff["emp_lt_nm"]} {$staff["emp_ft_nm"]}";
				}
				
				// 勤務条件・勤務日
				if ($this->max_err_cnt > $err_cnt and empty($staff['atdbk'][$date]['pattern']) and $this->auto->check_duty_day($date_row, $emp_id)) {
					$err_msg .= sprintf("(%02d)%s：勤務日<br>", $err_cnt++, "{$staff["emp_lt_nm"]} {$staff["emp_ft_nm"]}");
					$err_abb = empty($err_abb) ? "未" : $err_abb;
				}
				
				// 勤務条件・勤務不可日
				if ($this->max_err_cnt > $err_cnt and !empty($staff['atdbk'][$date]['pattern']) and $staff['atdbk'][$date]['pattern'] != "10" and ! $this->auto->check_duty_day($date_row, $emp_id)) {
					$err_msg .= sprintf("(%02d)%s：勤務不可日<br>", $err_cnt++, "{$staff["emp_lt_nm"]} {$staff["emp_ft_nm"]}");
					$err_abb = empty($err_abb) ? "有" : $err_abb;
				}
				
				// 勤務不可能なシフト
				if ($this->max_err_cnt > $err_cnt and ! $this->auto->check_duty_shift($date_row, $staff['atdbk'][$date]['pattern'], $emp_id)) {
					$err_msg .= sprintf("(%02d)%s：不可シフト<br>", $err_cnt++, "{$staff["emp_lt_nm"]} {$staff["emp_ft_nm"]}");
					$err_abb = empty($err_abb) ? "シ" : $err_abb;
				}
				
				// 休暇人数を数える
				if ( $ptn == 10 ){
					$vacation_counter++;
				}
			}
			
			// 当日同時休暇人数上限チェック
			$limit_vac_emp_count = $this->auto->auto_pvac_day_limit( $date );
			if ( $this->max_err_cnt > $err_cnt && $vacation_counter > $limit_vac_emp_count ){
				$err_msg .= sprintf( "(%02d)：同時休暇人数超過、上限 %d 人<br>" , $err_cnt++ , $limit_vac_emp_count );
				$err_abb = empty($err_abb) ? "超" : $err_abb;
			}
			
			// 性別固定
			foreach ($sex_pattern as $ptn => $r) {
				if ($r['flg']) {
					foreach ($r['list'] as $name) {
						$err_msg .= sprintf("(%02d)%s：性別固定シフト<br>", $err_cnt++, $name);
						$err_abb = empty($err_abb) ? "性" : $err_abb;
					}
				}
			}

			$err[$cnt] = array("msg" => $err_msg, "msg_abb" => $err_abb);
			$cnt++;
		}
		return $err;
	}

	/*************************************************************************/
	// 行チェック
	// @param	$plan_array				勤務シフト情報
	// @param	$duty_y					表示年
	// @param	$duty_m					表示月
	// @param	$start_date				開始日
	// @param	$end_date				終了日
	//
	// @return	$ret	エラー情報
	/*************************************************************************/
	function check_rows(
		$plan_array,
		$duty_y,
		$duty_m,
		$start_date,
		$end_date
		){
		///-----------------------------------------------------------------------------
		// 勤務スタッフの情報を取得する
		///-----------------------------------------------------------------------------
		$this->staff_table = $this->auto->duty_shift_staff_table($plan_array, $duty_y, $duty_m);
		
		///-----------------------------------------------------------------------------
		// 必要人数オブジェクト
		///-----------------------------------------------------------------------------
		$this->need_obj = new duty_shift_auto_need_class($this->group_id, $duty_y, $duty_m, $start_date, $end_date, $this->_db_con, $this->file_name);
		$date_table = $this->need_obj->get_date_table();
		
		///-----------------------------------------------------------------------------
		// plan_arrayからシフトデータを取得する
		///-----------------------------------------------------------------------------
		for ($i=0; $i<count($plan_array); $i++) {
			$emp_id = $plan_array[$i]["staff_id"];
			$k = 1;
			foreach ( $date_table as $date => $date_row ) {
				$this->staff_table[$emp_id]['atdbk'][$date]['pattern'] = $plan_array[$i]["atdptn_ptn_id_$k"];
				$this->staff_table[$emp_id]['atdbk'][$date]['reason'] = $plan_array[$i]["reason_$k"];
				$k++;
			}
		}
		$this->auto->staff_table = $this->staff_table;
		
		///-----------------------------------------------------------------------------
		// 当月公休日数 2012.4.18
		///-----------------------------------------------------------------------------
		$public_vac_day	= $this->auto->month_vacation_count($date_table , $duty_y,$duty_m);
		
		///-----------------------------------------------------------------------------
		// 4週8休：基準データ
		///-----------------------------------------------------------------------------
		if ($this->group_table["four_week_chk_flg"] == "t") {
			//基準日からの日数
			$diff_days = date_utils::get_time_difference($this->group_table["chk_start_date"]."0000", $start_date."0000") / (24 * 60);
			if ($start_date < $this->group_table["chk_start_date"]) {
				$diff_days = $diff_days * (-1);
			}
			
			//28日単位の回数
			$four_week_times = floor($diff_days / 28);
			
			//基準日から28日単位の開始日を取得
			$chk_start_date = substr(date_utils::add_day_ymdhi($this->group_table["chk_start_date"]."0000", $four_week_times * 28), 0, 8);
			
			//開始日が範囲内になるまで28日を足す
			while ($chk_start_date <= $start_date) {
				$chk_start_date = substr(date_utils::add_day_ymdhi($chk_start_date."0000", 28), 0, 8);
			}
			
			// 開始基準日以前はチェックしない
			if ($this->group_table["chk_start_date"] > $end_date) {
				$this->group_table["four_week_chk_flg"] = 'f';
			}
		}
		///-----------------------------------------------------------------------------
		// ループ
		///-----------------------------------------------------------------------------
		$cnt = 0;
		$err = array();
		foreach ( $this->staff_table as $emp_id => $staff ) {
			$err_cnt = 1;
			$err_msg = "";
			$err_abb = "";
			$pvac_count = 0;
			
			foreach ( $date_table as $date => $date_row ) {
				$ptn = $staff['atdbk'][$date]['pattern'];
				$reason = $staff['atdbk'][$date]['reason'];
				// 勤務条件・勤務日
				if ($this->max_err_cnt > $err_cnt and empty($ptn) and $this->auto->check_duty_day($date_row, $emp_id)) {
					$err_msg .= sprintf("(%02d)%d日：勤務日<br>", $err_cnt++, substr($date, 6,2));
					$err_abb = empty($err_abb) ? "未" : $err_abb;
				}
				
				// 勤務条件・勤務不可日
				if ($this->max_err_cnt > $err_cnt and !empty($ptn) and $ptn != "10" and ! $this->auto->check_duty_day($date_row, $emp_id)) {
					$err_msg .= sprintf("(%02d)%d日：勤務不可日<br>", $err_cnt++, substr($date, 6,2));
					$err_abb = empty($err_abb) ? "有" : $err_abb;
				}
				
				// 勤務不可能なシフト
				if ($this->max_err_cnt > $err_cnt and ! $this->auto->check_duty_shift($date_row, $ptn, $emp_id)) {
					$err_msg .= sprintf("(%02d)%d日：不可シフト<br>", $err_cnt++, substr($date, 6,2));
					$err_abb = empty($err_abb) ? "シ" : $err_abb;
				}
				// 連続勤務日数上限
				if ($this->max_err_cnt > $err_cnt and ! $this->auto->check_limit_day($date_row, $emp_id)) {
					$err_msg .= sprintf("(%02d)%d日：連続勤務日数上限<br>", $err_cnt++, substr($date, 6,2));
					$err_abb = empty($err_abb) ? "連" : $err_abb;

				}
				// 連続勤務シフト上限
				if ($this->max_err_cnt > $err_cnt and ! $this->auto->check_limit_shift($date_row, $ptn, $emp_id)) {
					$err_msg .= sprintf("(%02d)%d日：連続シフト<br>", $err_cnt++, substr($date, 6,2));
					$err_abb = empty($err_abb) ? "上" : $err_abb;
				}
				// 勤務シフト間隔
				if ($this->max_err_cnt > $err_cnt and ! $this->auto->check_interval_shift($date_row, $ptn, $emp_id)) {
					$err_msg .= sprintf("(%02d)%d日：勤務シフト間隔<br>", $err_cnt++, substr($date, 6,2));
					$err_abb = empty($err_abb) ? "間" : $err_abb;
				}
				// 組み合わせ禁止(前日)
				if ($this->max_err_cnt > $err_cnt and ! $this->auto->check_ng_prev_day($date_row, $ptn, $emp_id)) {
					$err_msg .= sprintf("(%02d)%d日：禁止組合せ<br>", $err_cnt++, substr($date, 6,2));
					$err_abb = empty($err_abb) ? "禁" : $err_abb;
				}
				// 組み合わせ禁止(翌日)
				if ($this->max_err_cnt > $err_cnt and ! $this->auto->check_ng_next_day($date_row, $ptn, $emp_id)) {
					$err_msg .= sprintf("(%02d)%d日：禁止組合せ<br>", $err_cnt++, substr($date, 6,2));
					$err_abb = empty($err_abb) ? "禁" : $err_abb;
				}
				// 組み合わせ(翌日)
				if ($this->max_err_cnt > $err_cnt and ! $this->auto->check_ok_next_day($date_row, $ptn, $emp_id)) {
					$err_msg .= sprintf("(%02d)%d日：組合せ指定<br>", $err_cnt++, substr($date, 6,2));
					$err_abb = empty($err_abb) ? "組" : $err_abb;
				}
				// 公休日数 2012.4.18
				if ($ptn == 10 && $reason == 24){
					$pvac_count++;
				}
			}
			
			// 4週8休のチェック
			if ($this->max_err_cnt > $err_cnt and $this->group_table["four_week_chk_flg"] == "t") {
				$fourweek = $this->auto->check_4week_day($chk_start_date, $emp_id, array());
				if (!$fourweek["status"]) {
					foreach ($fourweek["err"] as $r) {
						$err_msg .= sprintf("(%02d)%d/%d：休暇(%d),不足(%d)<br>", $err_cnt++, substr($r["date"],4,2), substr($r["date"],6,2), $r["max"], $r["max"]-$r["cnt"]);
						$err_abb = empty($err_abb) ? "休" : $err_abb;
					}
				}
			}
			
			// 公休日数超過 2012.4.18
			if ( $this->max_err_cnt > $err_cnt && $pvac_count > $public_vac_day ){
				$over_day = $pvac_count - $public_vac_day;
				$err_msg .= sprintf("(%02d)公休日数%2d日超過<br>", $err_cnt++, $over_day);
				$err_abb = empty($err_abb) ? "超" : $err_abb;
			}
			$err[$cnt] = array("msg" => $err_msg, "msg_abb" => $err_abb);
			$cnt++;
		}
		return $err;
	}
	
	/*************************************************************************/
	// 行チェック
	// @param	$plan_array				勤務シフト情報
	// @param	$chk_day_cnt
	// @param	$duty_m					表示月
	// @param	$start_date				開始日
	// @param	$days					日数
	//
	// @return	$ret	エラー情報
	/*************************************************************************/
	function check_shift_work(
						$seven_work_chk_flg,
						$part_week_chk_flg,
						$chk_plan_array,
						$chk_day_cnt,
						$start_date,
						$end_date,
						$draft_flag,
						$data_atdptn, 
						$pattern_id,
						$days
						)
	{
		
		//初期
		$emp_info = array();
		$arr_info = array();
		
		//対象職員ID取得
		$arr_id = array();
		foreach ($chk_plan_array as $item){
			array_push($arr_id, $item["staff_id"]);
		}
		if (count($arr_id) > 0){
			$ids = implode("','", $arr_id);
		}
		
		//職員条件を取得
		$emp_info = $this->get_empcond($ids);
		if ($seven_work_chk_flg == "t"){
			$arr_info = $this->check_continue_work($chk_plan_array, $arr_id, $emp_info, $chk_day_cnt, $start_date, $days);
		}
		
		if ($part_week_chk_flg == "t"){
			$arr_info = $this->check_week_worktime($chk_plan_array, $arr_id, $emp_info, $chk_day_cnt, 
												   $start_date, $end_date, $arr_info, $draft_flag, $data_atdptn, $pattern_id);
		}
		
		return $arr_info;
	}
	
	/*************************************************************************/
	// 行チェック
	// @param	$plan_array				勤務シフト情報
	// @param	$chk_day_cnt			
	// @param	$duty_m					表示月
	// @param	$start_date				開始日
	// @param	$days					日数
	//
	// @return	$ret	エラー情報
	/*************************************************************************/
	function check_continue_work(
								$chk_plan_array,
								$arr_id,
								$emp_info,
								$chk_day_cnt,
								$start_date,
								$days
								)
	{
		
		$arr_info = array();
		$arr_target = array("5"=>"5",		//特別休暇
							"22"=>"22",		//法定休暇
							"23"=>"23", 	//所定休暇
							"24"=>"24", 	//公休
							"31"=>"31", 	//夏季休暇
							"61"=>"61"		//年末年始休暇
							);
		
		//職員IDstaff_id, 職員名staff_name, 
		foreach ($chk_plan_array as $plan){
			
			$key = $plan["staff_id"];
			
			if (array_key_exists($key, $emp_info)){
				
				$tmp_date = $start_date;
				for($i = 1; $i <= $chk_day_cnt; $i++){
					$reason_id = ($plan["reason_2_" . $i] != "")? $plan["reason_2_" . $i] : $plan["reason_" . $i];
					$atdptn_id = $plan["atdptn_ptn_id_" . $i];
					
					if ((array_key_exists($reason_id, $arr_target)) || ($atdptn_id == "")){
						$count = 0;			
					}else{
						$count++;
						
						if ($count >= $days) {
							$st_date = $this->get_date($tmp_date, "-6");
							$st_date = date("Y年m月d日", strtotime($this->get_date($tmp_date, "-6")));
							$ed_date = date("Y年m月d日", strtotime($tmp_date));
							
							if ($arr_info[$key]){
								$arr_info[$key]["date"][] = array("during"=>$st_date . " - " . $ed_date, "msg"=>$days . "日連続勤務");
							}else{
								$arr_info[$key]["emp_id"] = $plan["staff_id"];
								$arr_info[$key]["emp_name"] = $plan["staff_name"];
								$arr_info[$key]["date"][] = array("during"=>$st_date . " - " . $ed_date, "msg"=>$days . "日連続勤務");
							}
						}
					}
					$tmp_date = $this->get_date($tmp_date, "+1");
				}
			}
			
		}
		return $arr_info;
	}
	
	/*************************************************************************/
	// 行チェック
	// @param	$plan_array				勤務シフト情報
	// @param	$chk_day_cnt
	// @param	$duty_m					表示月
	// @param	$start_date				開始日
	// @param	$days					日数
	//
	// @return	$ret	エラー情報
	/*************************************************************************/
	function check_week_worktime(
								$chk_plan_array,
								$arr_id,
								$emp_info,
								$chk_day_cnt,
								$start_date,
								$end_date,
								&$arr_info,
								$draft_flag,
								$data_atdptn, 
								$pattern_id
								)
	{
		$am_days = array("2"=>"2",		    //午前有給
						 "38"=>"38" 	    //午前年休
						);
		
		$pm_days = array("3"=>"3",		//午後有休
						 "39"=>"39" 	//午後年休
						);
		
		$half_days = array("44"=>"44", 	//半有半公
						   "55"=>"55",	//半夏半有
						   "57"=>"57", 	//半有半欠
						   "58"=>"58", 	//半特半有
						   "62"=>"62"	//半正半有
						  );
		
		$one_days = array("1"=>"1", 		//有休休暇 
						  "37"=>"37",		//年休
						 );
		
		$total_days = array("1"=>"1","2"=>"2","3"=>"3","37"=>"37","38"=>"38","39"=>"39",
							"44"=>"44","55"=>"55","57"=>"57","58"=>"58","62"=>"62");
		
		$calendar = $this->get_calendar($start_date, $end_date);
		$calendarname = $this->get_calendarname();
		
		//カレンダー所定労働時間
		$fix_day_time = date_utils::hi_to_minute($calendarname["day_time"]);
		$fix_am_time = date_utils::hi_to_minute($calendarname["am_time"]);
		$fix_pm_time = date_utils::hi_to_minute($calendarname["pm_time"]);
		
		//atdptn情報
		$arr_atdptn = array();
		foreach ($data_atdptn as $item){
			$key = $item["id"];
			$arr_atdptn[$key] = $item;
		}
		
		//最新パターン取得
		$arr_ptn = array();
		foreach ($chk_plan_array as $plan){
			$key = $plan["staff_id"];	//職員ID
			$tmp_date = $start_date;

			for($i = 1; $i <= $chk_day_cnt; $i++){
				$atdptn_id = $plan["atdptn_ptn_id_" . $i];
				
				if($atdptn_id != "") {
					$arr_ptn[$key][$tmp_date] =$atdptn_id;
				}
				$tmp_date = $this->get_date($tmp_date, "+1");
			}
		}
		
		$standard_time = $this->get_specified_worktime($arr_ptn, $arr_id, $start_date, $end_date, $draft_flag,$arr_atdptn, $pattern_id, $calendar);
		
		foreach ($chk_plan_array as $plan){
			$key = $plan["staff_id"];	//職員ID
			
			if ((array_key_exists($key, $emp_info)) && ($emp_info[$key]["duty_form"] == "2")){
				
				$specified = date_utils::hi_to_minute($emp_info[$key]["specified"]); //所定労働時間
				$am_time = date_utils::hi_to_minute($emp_info[$key]["am_time"]); 	 //午前所定労働時間
				$pm_time = date_utils::hi_to_minute($emp_info[$key]["pm_time"]); 	 //午後所定労働時間
				$week_time = date_utils::hi_to_minute($emp_info[$key]["week_time"]); //週所定労働時間
				
				$specified = ($specified == "") ? $fix_day_time : $specified;
				$am_time = ($am_time == "")? $fix_am_time : $am_time;
				$pm_time = ($pm_time == "")? $fix_pm_time : $pm_time;
				
				$tmp_date = $start_date;
				$minutes = 0;
				for($i = 1; $i <= $chk_day_cnt; $i++){
					$reason_id = ($plan["reason_2_" . $i] != "")? $plan["reason_2_" . $i] : $plan["reason_" . $i];
					$atdptn_id = $plan["atdptn_ptn_id_" . $i];
					
					$wd = date("w", to_timestamp($tmp_date));	//月曜日(1)から加算
					
					if ($wd == "1"){
						$st_date = date("Y年m月d日", strtotime($tmp_date));
						$ed_date = date("Y年m月d日", strtotime($this->get_date($tmp_date, "6")));
						$minutes = 0;
					}
					
					if (array_key_exists($reason_id, $total_days)){
						//半日午前休暇
						if (array_key_exists($reason_id, $am_days)){
							$minutes += $specified;
						}
						
						//半日午後休暇
						if (array_key_exists($reason_id, $pm_days)){
							$minutes += $specified;
						}
						
						//半日休暇
						if (array_key_exists($reason_id, $half_days)){
							$minutes += (int)($specified / 2);
						}
						
						//１日休暇
						if (array_key_exists($reason_id, $one_days)){
							$minutes += $specified;
						}
					}else{
						$minutes += $standard_time[$key][$tmp_date]["work_time"];
					}
					
					if(($week_time > 0) && ($minutes > $week_time)){
						if ($arr_info[$key]){
							$arr_info[$key]["date"][] = array("during"=>$st_date . " - " . $ed_date, "msg"=>"週間勤務時間超過"); 
						}else{
							$arr_info[$key]["emp_id"] = $plan["staff_id"];
							$arr_info[$key]["emp_name"] = $plan["staff_name"];
							$arr_info[$key]["date"][] = array("during"=>$st_date . " - " . $ed_date, "msg"=>"週間勤務時間超過"); 
						}
						
						$minutes = 0;
					}
						
					$tmp_date = $this->get_date($tmp_date, "+1");
				}
			}
		}
		
		return $arr_info;
	}
	
	function get_specified_worktime(
									$arr_ptn, 
									$emp_id_list,
									$start_date,
									$end_date,
									$draft_flag,
									$arr_atdptn, 
									$pattern_id,
									$calendar
									)
	{
	
		$cls_timecard = new check_timecard();
		$ids = implode("','", $emp_id_list);
		$emp_ids = "'" . $ids . "'";
		$timecard_common_class = new timecard_common_class($this->_db_con, $this->file_name, $emp_id_list, $start_date, $end_date);
		$officehours = $cls_timecard->get_officehours($this->_db_con, $this->file_name);
		$emp_officehours = $cls_timecard->get_emp_officehours($this->_db_con, $this->file_name, $emp_ids);
		
		$arr_specified = array();
		
		//登録前のデータ
		$tmp_group = $pattern_id;
		foreach ($arr_ptn as $key => $ptn){
			$tmp_id = $key;
			foreach ($ptn as $tmp_date=>$tmp_pattern) {
				if ($arr_atdptn[$tmp_pattern]){
					$tmp_flag = $arr_atdptn[$tmp_pattern]["empcond_officehours_flag"];
					$tmp_anday_flag = $arr_atdptn[$tmp_pattern]["previous_day_flag"];
					$tmp_24h_flag = $arr_atdptn[$tmp_pattern]["over_24hour_flag"];
					$tmp_type = $calendar[$tmp_date];
					if (($tmp_type != "2") && ($tmp_type != "3")){
						$tmp_type = "1";
					}
				}
				
				/* 所定労働時間ゲット */
				$fixedtime = $cls_timecard->get_fixedtime($tmp_flag, $tmp_id, $tmp_date, $emp_officehours,
						$officehours, $tmp_group, $tmp_pattern, $tmp_type, $tmp_anday_flag,
						$tmp_24h_flag, $timecard_common_class);
				$plan_time = date_utils::get_diff_minute($fixedtime["end2_date_time"],$fixedtime["start2_date_time"]);
					
				//所定休憩時間を取得
				$arr_time = $officehours[$tmp_group][$tmp_pattern][$tmp_type];
				$rest_time = date_utils::hi_to_minute($fixedtime["rest_end_time"]) - date_utils::hi_to_minute($fixedtime["rest_start_time"]);
					
				$work_time = $plan_time - $rest_time; //所定労働時間-所定休憩時間
					
				$arr_specified[$tmp_id][$tmp_date]["work_time"] = $work_time;
			}
			
		}
		return $arr_specified;
	}
	
	
	// 勤務条件テーブルより勤務形態（常勤、非常勤）、所定労働時間、半日午前、半日午後を取得
	function get_empcond($id){
		$sql = "select emp_id, duty_form, specified_time, am_time, pm_time, week_time from empcond";
		$cond = "where emp_id in ('$id')";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		$emp_info = array();
		while ($row = pg_fetch_array($sel)) {
			$emp_id = $row["emp_id"];
			$duty_form = $row["duty_form"];
			$specified_time = $row["specified_time"];
			$am_time = $row["am_time"];
			$pm_time = $row["pm_time"];
			$week_time = $row["week_time"];
			
			$emp_info[$emp_id] = array("emp_id"=>$emp_id,
										 "duty_form"=>$duty_form,
										 "specified"=>$specified_time,
										 "am_time"=>$am_time,
										 "pm_time"=>$pm_time, 
										 "week_time"=>$week_time);
		}
		return $emp_info;
	}
	
	//カレンダの所定労働時間を取得
	function get_calendarname(){
		$sql = "select day1_time, am_time, pm_time from calendarname";
		$sel = select_from_table($this->_db_con, $sql, "", $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		$arr_calendar = array();
		while ($row = pg_fetch_array($sel)) {
			$arr_calendar["day_time"] = $row["day1_time"];
			$arr_calendar["am_time"] = $row["am_time"];
			$arr_calendar["pm_time"] = $row["pm_time"];
		}
		
		return $arr_calendar;
	}
	
	//カレンダのtypeを取得
	function get_calendar($st_day, $ed_day){
		$sql = "select date, type from calendar";
		$cond = "where date >= '$st_day' and date <= '$ed_day'";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	
		$arr_calendar = array();
		while ($row = pg_fetch_array($sel)) {
			$tmp_date = $row["date"];
			$tmp_type = $row["type"];
			$arr_calendar[$tmp_date] = $tmp_type;
		}
	
		return $arr_calendar;
	}
	
	// 指定翌日日付をyyyymmdd形式で取得
	function get_date($yyyymmdd, $num) {
		$day = $num . " day";
		return date("Ymd", strtotime($day, $this->to_timestamp($yyyymmdd)));
	}
	
	// 日付をタイムスタンプに変換
	function to_timestamp($yyyymmdd) {
		$y = substr($yyyymmdd, 0, 4);
		$m = substr($yyyymmdd, 4, 2);
		$d = substr($yyyymmdd, 6, 2);
		return mktime(0, 0, 0, $m, $d, $y);
	}
}
?>
