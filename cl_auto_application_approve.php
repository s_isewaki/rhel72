<?php
//ini_set( 'display_errors', 1 );

require_once("cl_common_log_class.inc");
$log = new cl_common_log_class(basename(__FILE__));
$log->info(basename(__FILE__)." START");

require_once('MDB2.php');
require_once(dirname(__FILE__) . "/Cmx.php");

$log->debug('cl_auto_application_approve_model require開始',__FILE__,__LINE__);
require_once(dirname(__FILE__) . "/cl/model/workflow/cl_auto_application_approve_model.php");
$log->debug('cl_auto_application_approve_model require終了',__FILE__,__LINE__);

function auto_application_approve($mdb2, $apply_id, $login_emp_id){
	global $log;

	$log->debug('自動承認モデル呼出',__FILE__,__LINE__);
	$model = new cl_auto_application_approve_model($mdb2, $login_emp_id);

	$log->debug('非同期・同期受信テーブル更新',__FILE__,__LINE__);
	$model->applyasyncrecv_update($apply_id);

	$log->debug('承認テーブル更新',__FILE__,__LINE__);
	$model->applyapv_update($apply_id);

	$log->debug('申請テーブル更新',__FILE__,__LINE__);
	$model->apply_update($apply_id);

}

