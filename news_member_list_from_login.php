<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_postgres.php");
//require("about_session.php");
//require("about_authority.php");
require("news_common.ini");
require("show_news_member_list_from_login.ini");

$fname = $PHP_SELF;

/*
// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\"showLoginPage(window);</script>");
	exit;
}
*/
// 親画面で振り分け
switch ($from_page_id) {
case "1":  // お知らせ管理
	$auth_id = 24;
	$doc_title = "お知らせ管理";
	break;
case "2":  // お知らせ
	$auth_id = 46;
	$doc_title = "お知らせ";
	break;
default:
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\"showLoginPage(window);</script>");
	exit;
}

/*
// 権限のチェック
$checkauth = check_authority($session, $auth_id, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\"showLoginPage(window);</script>");
	exit;
}
:*/
// データベースに接続
$con = connect2db($fname);

// イニシャルのデフォルトは「あ行」とする
if ($initial == "") {$initial = "1";}

// 職種一覧を取得
$job_list = array();
$job_list["0"] = "すべて";
$job_list = array_merge($job_list, get_job_list($con, $fname));

// 職種未指定の場合、ログインユーザの職種をデフォルトとする
if ($job_id == "") {
	$sql = "select emp_job from empmst";
	$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\"showErrorPage(window);</script>");
		exit;
	}
	$job_id = pg_result($sel, 0, "emp_job");
}
?>
<title>CoMedix <? echo($doc_title); ?> | 名簿</title>
<script language="javascript">
function changeJob(jobID) {
	location.href = 'news_member_list_from_login.php?from_page_id=<? echo($from_page_id); ?>&job_id=' + jobID;
}

function selectEmp(empID, empName) {
	opener.document.mainform.emp_name.value = empName;
	opener.document.mainform.srch_emp_id.value = empID;
	self.close();
}
</script>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#5279a5">
<td width="520" height="22" class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>名簿</b></font></td>
<td width="80" align="center"><a href="javascript:window.close()"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff">■閉じる</font></a></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td height="22"><select name="job_id" onchange="changeJob(this.value);"><? show_options($job_list, $job_id); ?></select></td>
</tr>
<tr height="22" bgcolor="#f6f9ff">
<td><? show_news_initial_list($job_id, $initial, $from_page_id); ?></td>
</tr>
<tr height="22" bgcolor="#f6f9ff">
<td><? show_news_member_list($con, $job_id, $initial, $fname); ?></td>
</tr>
</table>
</center>
</body>
<? pg_close($con); ?>
</html>
