<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("kango_common.ini");
require_once("kango_search_bed.ini");
require_once("label_by_profile_type.ini");


//==============================
//前処理
//==============================

// ページ名
$fname = $_SERVER["PHP_SELF"];

// セッションのチェック
$session = qualify_session($_REQUEST["session"], $fname);
if ($session == "0")
{
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$checkauth = check_authority($session, $AUTH_NO_KANGO_USER, $fname);
if($checkauth == "0")
{
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// パラメータ
$is_postback = isset($_REQUEST["is_postback"]) ? $_REQUEST["is_postback"] : null;
$action = isset($_REQUEST["action"]) ? $_REQUEST["action"] : null;

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
//ユーザーID
//==============================
$emp_id = get_emp_id($con,$session,$fname);

//==============================
//ポストバック時の処理
//==============================
if($is_postback == "true")
{
	set_user_selected_bldg($con,$fname,$emp_id,$bldg_cd,$ward_cd);
}

// 棟一覧を取得
$sql = "select bldg_cd, bldg_name from bldgmst";
$cond = "where bldg_del_flg = 'f' order by bldg_cd";
$sel_bldg = select_from_table($con, $sql, $cond, $fname);
if ($sel_bldg == 0)
{
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$bldg_count = pg_num_rows($sel_bldg);

if($is_postback != "true")
{
	$user_selected_bldg = get_user_selected_bldg($con,$fname,$emp_id);
	if($user_selected_bldg != null)
	{
		$bldg_cd = $user_selected_bldg['bldg_cd'];
		$ward_cd = $user_selected_bldg['ward_cd'];
		$action = "表示";
	}
	else
	{
		$bldg_cd = pg_fetch_result($sel_bldg, 0, "bldg_cd");
		$ward_cd = "";
	}
}

if ($action == "表示") {

	// 画面更新間隔の値を取得
	$sql = "select refresh_sec from roomlayout";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0) {
		$refresh_sec = pg_fetch_result($sel, 0, "refresh_sec");
	} else {
		$refresh_sec = 300;
	}
}

// 病棟一覧の取得
$sql = "select bldg_cd, ward_cd, ward_name from wdmst";
$cond = "where ward_del_flg = 'f' order by bldg_cd, ward_cd";
$sel_ward = select_from_table($con, $sql, $cond, $fname);
if ($sel_ward == 0)
{
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 組織タイプを取得
//$profile_type = get_profile_type($con, $fname);
$profile_type = 1; //看護観察記録では「社会福祉法人対応」は不要。

// 患者/利用者
$patient_title = $_label_by_profile["PATIENT"][$profile_type];

// 病床検索/居室検索
$bed_search_title = $_label_by_profile["BED_SEARCH"][$profile_type];

// 診療科/担当部門
$section_title = $_label_by_profile["SECTION"][$profile_type];

// 主治医/担当者１
$main_doctor_title = $_label_by_profile["MAIN_DOCTOR"][$profile_type];

// 退院/退所
$out_title = $_label_by_profile["OUT"][$profile_type];

// 病棟/施設
$ward_title = $_label_by_profile["WARD"][$profile_type];


//==============================
//その他
//==============================

//画面名
$PAGE_TITLE = "病棟レイアウト表示";

//========================================================================================================================================================================================================
// HTML出力
//========================================================================================================================================================================================================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$KANGO_TITLE?> | <?=$PAGE_TITLE?></title>


<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/popup.js"></script>
<script type="text/javascript">

function initPage()
{
<? if ($action == "表示") { ?>
	setTimeout(function(){location.reload();}, <? echo($refresh_sec * 1000); ?>);
<? } ?>

	resetWardOptions('<?=$ward_cd?>');
}

function resetWardOptions(default_ward_cd)
{
	var bldg_cd = document.mainform.bldg_cd.value;

	deleteAllOptions(document.mainform.ward_cd);

	addOption(document.mainform.ward_cd, '', '　　　　　', default_ward_cd);
	addOption(document.mainform.ward_cd, '0', 'すべて', default_ward_cd);

	switch (bldg_cd)
	{
		<?
		$pre_bldg_cd = "";
		while ($row = pg_fetch_array($sel_ward))
		{
			$cur_bldg_cd = $row["bldg_cd"];

			if ($cur_bldg_cd != $pre_bldg_cd)
			{
				if ($pre_bldg_cd != "")
				{
					echo ("\t\tbreak;\n");
				}
				echo ("\tcase '$cur_bldg_cd':\n");
				$pre_bldg_cd = $cur_bldg_cd;
			}

			echo("\t\taddOption(document.mainform.ward_cd, '{$row["ward_cd"]}', '{$row["ward_name"]}', default_ward_cd);\n");
		}
		?>
	}
}

function deleteAllOptions(box)
{
	for (var i = box.length - 1; i >= 0; i--)
	{
		box.options[i] = null;
	}
}

function addOption(box, value, text, selected)
{
	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	if (selected == value)
	{
		opt.selected = true;
	}
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
}

function popupPatientDetail(pt_info1, pt_info13, pt_info2, pt_info3, pt_info4, pt_info5, pt_info6, pt_info7, pt_info8, pt_info22, pt_info23, pt_info9, pt_info10, pt_info11, pt_info14, pt_info15, pt_info20, pt_info21, pt_info12, pt_info16, pt_info19, pt_info24, pt_info18, pt_info17, e) {
	popupDetailBlue(
		new Array(
			'<?=$patient_title?>氏名', pt_info1,
			'<?=$patient_title?>ID', pt_info13,
			'保険', pt_info2,
			'性別', pt_info3,
			'年齢', pt_info4,
			'<?=$section_title?>', pt_info5,
			'<?=$main_doctor_title?>', pt_info6,
			'移動', pt_info7,
			'観察', pt_info8,
			'面会', pt_info9,
			'外泊', pt_info10,
			'外出', pt_info11,
			'<?=$_label_by_profile["IN"][$profile_type]?>日', pt_info14,
			'<?=$_label_by_profile["STAY_HOSPITAL"][$profile_type]?>日数', pt_info15,
			'回復期リハビリ算定期限分類', pt_info22,
			'回復期リハビリ算定期限日', pt_info23,
			'リハビリ算定期限分類', pt_info20,
			'リハビリ算定期限日', pt_info21,
			'<?=$out_title?>予定日', pt_info12,
			'医療区分・ADL区分', pt_info16,
			'看護度分類', pt_info19,
			'差額室料免除',pt_info24,
			'個人情報に関する要望', pt_info18,
			'特記事項', pt_info17
		), 350, 100, e
	);
}

function showDetail() {
	if (document.mainform.ward_cd.value == '') {
		return;
	}

	document.mainform.action.value = '表示';
	document.mainform.submit();
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.bed {border-collapse:collapse;}
table.bed td {border:solid #5279a5 1px;}

.list_in {border-collapse:collapse;}
.list_in td {border:#FFFFFF solid 0px;}
</style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<!-- ヘッダー START -->
<?
show_kango_header($session,$fname)
?>
<!-- ヘッダー END -->
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td>


<form name="mainform" action="kango_search_bed.php" method="get">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="is_postback" value="true">
<input type="hidden" name="action" value="">

<!-- 検索条件 START -->
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr height="30" bgcolor="#f6f9ff">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">事業所（棟）：
<select name="bldg_cd" onchange="resetWardOptions();">
<?
while ($row = pg_fetch_array($sel_bldg))
{
	echo("<option value=\"{$row["bldg_cd"]}\"");
	if ($row["bldg_cd"] == $bldg_cd)
	{
		echo(" selected");
	}
	echo(">{$row["bldg_name"]}\n");
}
?></select>
<?=$ward_title?>：<select name="ward_cd" onchange="showDetail();">
</select>
</font></td>
</tr>
</table>
<!-- 検索条件 END -->



<!-- 検索結果 START -->
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<?
if ($bldg_count == 0)
{
	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">\n");
	echo("<tr height=\"22\">\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">棟が登録されていません。</font></td>\n");
	echo("</tr>\n");
	echo("</table>\n");
}
else if ($action == "表示")
{
	//病棟指定  ：棟サマリ・病棟サマリを表示
	if ($ward_cd == "0") {
		show_bldg_report($con, $bldg_cd, $fname);
		$ward_cd = "";
	}

	show_ward_layout($con, $bldg_cd, $ward_cd, $session, $fname);

	if ($ward_cd != "") {
		show_ward_report($con, $bldg_cd, $ward_cd, $fname);
	}
}
?>
<!-- 検索結果 END -->


</form>


</td>
</tr>
</table>
</body>
</html>
<?
pg_close($con);
?>
