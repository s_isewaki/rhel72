<?
require ("about_session.php");
require ("about_authority.php");
require ("referer_common.ini");
require ("cl_workflow_common.ini");
require_once ("cl_common.ini");
require_once ("show_class_name.ini");
require_once ("cl_application_workflow_common_class.php");
require_once("cl_title_name.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo ("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo ("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// ワークフロー権限のチェック
$checkauth = check_authority($session, $CL_MANAGE_AUTH, $fname);
if ($checkauth == "0") {
	echo ("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo ("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

$obj = new cl_application_workflow_common_class($con, $fname);

//====================================
// 登録更新処理
//====================================
if ($mode == "REGIST") {
	// トランザクションを開始
	pg_query($con, "begin");

    $obj->delete_cl_committee();

    for($i=1; $i<=2; $i++)
    {
        $pjt_parent_id = "pjt_parent_id".$i;
        $pjt_child_id  = "pjt_child_id".$i;

        if($$pjt_parent_id != "")
        {
            $arr_pjt_parent_id = split(",", $$pjt_parent_id);
            $arr_pjt_child_id  = split(",", $$pjt_child_id);

            for($j=0; $j<count($arr_pjt_parent_id); $j++)
            {
                $arr = array(
                               "committee_div" => $i,
                               "parent_pjt_id" => $arr_pjt_parent_id[$j],
                               "child_pjt_id"  => $arr_pjt_child_id[$j],
                               "order_no"      => $j + 1
                             );

                $obj->regist_cl_committee($arr);
            }
        }
    }
    // トランザクションをコミット
    pg_query($con, "commit");
}


//====================================
// 初期処理
//====================================
$cl_title = cl_title_name();

$arr_cl_committee1 = $obj->get_cl_committee(1);
$arr_cl_committee2 = $obj->get_cl_committee(2);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title><?=$cl_title?> | 委員会設定</title>


<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">


function initPage()
{
<?
    for($i=1; $i<=2; $i++)
    {
    $arr_cl_committee = "arr_cl_committee".$i;

?>
    pjt_parent_id = '';
    pjt_child_id  = '';
    pjt_parent_nm = '';
    pjt_child_nm  = '';

    <?
    foreach ($$arr_cl_committee as $cl_committee)
    {
    ?>
    if(pjt_parent_id != "")
    {
        pjt_parent_id += ',';
        pjt_child_id  += ',';
        pjt_parent_nm += ',';
        pjt_child_nm  += ',';
    }
    pjt_parent_id += '<?=$cl_committee["parent_pjt_id"]?>';
    pjt_child_id  += '<?=$cl_committee["child_pjt_id"]?>';
    pjt_parent_nm += '<?=$cl_committee["parent_pjt_nm"]?>';
    pjt_child_nm  += '<?=$cl_committee["child_pjt_nm"]?>';

    <?
    }
    ?>
    document.getElementById('pjt_parent_id<?=$i?>').value = pjt_parent_id;
    document.getElementById('pjt_child_id<?=$i?>').value  = pjt_child_id;
    document.getElementById('pjt_parent_nm<?=$i?>').value = pjt_parent_nm;
    document.getElementById('pjt_child_nm<?=$i?>').value  = pjt_child_nm;
    set_project_area(<?=$i?>);
<?
    }
?>
}


var childwin = null;
function openProjectList(idx)
{
	dx = screen.width;
	dy = screen.top;
	base = 0;
	wx = 720;
	wy = 600;
	var url = './cl_workflow_project_list.php';
	url += '?session=<?=$session?>&idx=' + idx + '&add_flg=t';
	childwin = window.open(url, 'popup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
	childwin.focus();
}

function set_project_area(idx)
{
    var disp_area = "project" + idx;

    pjt_parent_id = document.getElementById('pjt_parent_id' + idx).value;
    pjt_child_id  = document.getElementById('pjt_child_id' + idx).value;
    pjt_parent_nm = document.getElementById('pjt_parent_nm' + idx).value;
    pjt_child_nm  = document.getElementById('pjt_child_nm' + idx).value;

    arr_pjt_parent_id = pjt_parent_id.split(',');
    arr_pjt_child_id  = pjt_child_id.split(',');
    arr_pjt_parent_nm = pjt_parent_nm.split(',');
    arr_pjt_child_nm  = pjt_child_nm.split(',');

    var disp_area_content = '';

    for(i=0; i<arr_pjt_parent_id.length; i++)
    {
        if(i > 0)
        {
            disp_area_content += ", ";
        }

        tmp_pjt_nm = arr_pjt_parent_nm[i];

        if(arr_pjt_child_id[i] != "")
        {
            tmp_pjt_nm = tmp_pjt_nm + '&nbsp;&gt;&nbsp;' + arr_pjt_child_nm[i];
        }

        disp_area_content += "<a href=\"javascript:delete_committee('" + arr_pjt_parent_id[i] + "', '" + arr_pjt_child_id[i] + "', '" + idx + "', '" + tmp_pjt_nm + "');\">";
        disp_area_content += tmp_pjt_nm;
        disp_area_content += "</a>";
    }
    document.getElementById(disp_area).innerHTML = disp_area_content;
}


function delete_committee(del_pjt_parent_id, del_pjt_child_id, idx, pjt_nm)
{
	if(confirm("「" + pjt_nm + "」を削除します。よろしいですか？"))
	{
        pjt_parent_id = document.getElementById('pjt_parent_id' + idx).value;
        pjt_child_id  = document.getElementById('pjt_child_id' + idx).value;
        pjt_parent_nm = document.getElementById('pjt_parent_nm' + idx).value;
        pjt_child_nm  = document.getElementById('pjt_child_nm' + idx).value;

        arr_pjt_parent_id = pjt_parent_id.split(',');
        arr_pjt_child_id  = pjt_child_id.split(',');
        arr_pjt_parent_nm = pjt_parent_nm.split(',');
        arr_pjt_child_nm  = pjt_child_nm.split(',');

        new_pjt_parent_id = '';
        new_pjt_child_id  = '';
        new_pjt_parent_nm = '';
        new_pjt_child_nm  = '';

        for(i=0; i<arr_pjt_parent_id.length; i++)
        {
            if(arr_pjt_parent_id[i] == del_pjt_parent_id && arr_pjt_child_id[i] == del_pjt_child_id)
            {
                continue
            }

            if(new_pjt_parent_id != "")
            {
                new_pjt_parent_id += ',';
                new_pjt_child_id  += ',';
                new_pjt_parent_nm += ',';
                new_pjt_child_nm  += ',';
            }
	
            new_pjt_parent_id += arr_pjt_parent_id[i];
            new_pjt_child_id  += arr_pjt_child_id[i];
            new_pjt_parent_nm += arr_pjt_parent_nm[i];
            new_pjt_child_nm  += arr_pjt_child_nm[i];
        }

        document.getElementById('pjt_parent_id' + idx).value = new_pjt_parent_id;
        document.getElementById('pjt_child_id' + idx).value = new_pjt_child_id;
        document.getElementById('pjt_parent_nm' + idx).value = new_pjt_parent_nm;
        document.getElementById('pjt_child_nm' + idx).value = new_pjt_child_nm;

        set_project_area(idx);
	}
}

function clear_committee(idx)
{
    document.getElementById('pjt_parent_id' + idx).value = '';
    document.getElementById('pjt_child_id' + idx).value = '';
    document.getElementById('pjt_parent_nm' + idx).value = '';
    document.getElementById('pjt_child_nm' + idx).value = '';
    document.getElementById('project' + idx).innerHTML = '';
}


function regist()
{
	if (confirm('登録します。よろしいですか？'))
	{
		document.wkfw.action="cl_workflow_committee_setting.php?session=<?=$session?>&mode=REGIST";
		document.wkfw.submit();
    }
}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;border:#A0D25A solid 1px;}
.list td {border:#A0D25A solid 1px;}

.non_in_list {border-collapse:collapse;}
.non_in_list td {border:0px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="ladder_menu.php?session=<? echo($session); ?>"><img src="img/icon/s42.gif" width="32" height="32" border="0" alt="<? echo($cl_title); ?>"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="ladder_menu.php?session=<? echo($session); ?>"><b><? echo($cl_title); ?></b></a> &gt; <a href="cl_workflow_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="ladder_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>

<?

show_wkfw_menuitem($session, $fname, "");
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#A0D25A"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
<tr>
<td><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>

<form name="wkfw" action="#" method="post">
<input type="hidden" name="session" value="<? echo($session); ?>">


<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>

<!-- left -->
<?

show_cl_option_menu($session, $fname);
?>
<!-- left -->
<!-- center -->
<td width="10"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->
<!-- right -->

<td width="75%" valign="top">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>委員会設定</b></font></td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">

<tr height="50">
<td width="180" bgcolor="#E5F6CD" align="center"> 
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>認定委員会</b></font><br>
<input type="button" value="一覧" style="width=5.5em;" onclick="openProjectList('1');"><br>
<input type="button" value="クリア" style="width=5.5em;" onclick="clear_committee('1');">
</td>
<td bgcolor="#FFFFFF">
<table width="100%" height="100%" border="0" cellspacing="3" cellpadding="0" class="non_in_list">
<tr>
<td style="border:#E5F6CD solid 1px;">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><span id="project1"></span></font>
</td>
</tr>
</table>
</td>
</tr>

<tr height="50">
<td width="180" bgcolor="#E5F6CD" align="center"> 
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>看護診断委員会</b></font><br>
<input type="button" value="一覧" style="width=5.5em;" onclick="openProjectList('2');"><br>
<input type="button" value="クリア" style="width=5.5em;" onclick="clear_committee('2');">
</td>
<td bgcolor="#FFFFFF">
<table width="100%" height="100%" border="0" cellspacing="3" cellpadding="0" class="non_in_list">
<tr>
<td style="border:#E5F6CD solid 1px;">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><span id="project2"></span></font>
</td>
</tr>
</table>
</td>
</tr>

</table>
<input type="hidden" name="pjt_parent_id1" value="" id="pjt_parent_id1">
<input type="hidden" name="pjt_child_id1" value="" id="pjt_child_id1">
<input type="hidden" name="pjt_parent_nm1" value="" id="pjt_parent_nm1">
<input type="hidden" name="pjt_child_nm1" value="" id="pjt_child_nm1">

<input type="hidden" name="pjt_parent_id2" value="" id="pjt_parent_id2">
<input type="hidden" name="pjt_child_id2" value="" id="pjt_child_id2">
<input type="hidden" name="pjt_parent_nm2" value="" id="pjt_parent_nm2">
<input type="hidden" name="pjt_child_nm2" value="" id="pjt_child_nm2">

<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td align="right"><input type="button" value="登録" onclick="regist();"></td>
</tr>
</table>

</td>

<!-- right -->
</tr>
</table>

</form>

</td>
</tr>
</table>
</body>
</html>

<? pg_close($con); ?>
