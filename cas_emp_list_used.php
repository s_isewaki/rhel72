<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("cas_common.ini");
require_once("cas_application_workflow_common_class.php");
require_once("get_cas_title_name.ini");

$fname = $PHP_SELF;

//======================================
// セッションのチェック
//======================================
$session = qualify_session($session, $fname);
if ($session == "0") {
	showLoginPage();
	exit;
}

//======================================
// データベースに接続
//======================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$obj = new cas_application_workflow_common_class($con, $fname);
// 呼び元画面が「病棟評価表」画面の場合
if($call_screen == "WARD_GRADE")
{
    $arr_empmst = $obj->get_empmst($session);
    $login_emp_id = $arr_empmst[0]["emp_id"];

    $employee_div = "";

    // 該当職員が「評価者」どうか判断する
    if($obj->is_grader($login_emp_id))
    {
        $employee_div = "GRADER";
    }
    // 該当職員が「師長」どうか判断する
    if($obj->is_nursing_commander($login_emp_id))
    {
        $employee_div = "NURSING_COMMANDER";
    }
    // 該当職員が「特定職員」どうか判断する
    if($obj->is_particular_employee($login_emp_id))
    {
        $employee_div = "PARTICULAR_EMPLOYEE";
    }
}

//======================================
// よく使う人情報取得
//======================================
$sql  = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond , $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

$sql   = "select wm_counter.used_emp_id as emp_id, get_emp_name(used_emp_id) as name, get_mail_login_id(used_emp_id) as loginid , get_emp_email(used_emp_id) as email, count_num , (select class_nm from classmst where class_id = empmst.emp_class) as class_nm, (select atrb_nm from atrbmst where atrb_id = empmst.emp_attribute) as atrb_nm, (select dept_nm from deptmst where dept_id = empmst.emp_dept) as dept_nm, (select room_nm from classroom where room_id = empmst.emp_room) as room_nm from wm_counter  inner join empmst on empmst.emp_id = wm_counter.used_emp_id ";
$cond  = "where exists (select * from authmst where authmst.emp_id = wm_counter.used_emp_id and authmst.emp_del_flg = 'f' $approve_cond) and wm_counter.emp_id = '$emp_id' ";
$cond .= $obj->get_sql_for_ward_grade($employee_div, $login_emp_id);
$cond .= "order by count_num desc limit 20";

$sel_emp = select_from_table($con, $sql, $cond, $fname);

if ($sel_emp == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
while ($row_emp = pg_fetch_array($sel_emp)) {
    $tmp_arr = array();
    $tmp_arr['name'] = $row_emp['name'];
    $tmp_arr['emp_id'] = $row_emp['emp_id'];

    $belong_to = $row_emp['class_nm'] . ' &gt; ' . $row_emp['atrb_nm'] . ' &gt; ' . $row_emp['dept_nm'];
    if ($row_emp['room_nm'] != '') {
        $belong_to .= ' &gt; ' . $row_emp['room_nm'];
    }
    $tmp_arr['belong_to'] = $belong_to;

    $employees[] = $tmp_arr;
}

$cas_title = get_cas_title_name();

//==============================
// HTML
//==============================
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title><?=$cas_title?> | 職員検索</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script src="js/prototype/dist/prototype.js"></script>
<script language="javascript">

// コピー処理
function copy_emp(emp_id, emp_nm)
{
    if(window.opener && !window.opener.closed && window.opener.add_target_list)
    {
        set_wm_counter(emp_id);
        window.opener.add_target_list('<?=$input_div?>', emp_id, emp_nm);
    }
}

// ajax 「よく使う人」カウントアップ処理
function set_wm_counter(ids)
{
	var url = 'cas_wm_counter.php';
	var params = $H({'session':'<?=$session?>','used_emp_ids':ids}).toQueryString();
	var myAjax = new Ajax.Request(
		url, 
		{
			method: 'post',
			postBody: params
		});
}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

<!--
.list {border-collapse:collapse;}
.list td {border:#35B341 solid 1px;}
img.close {
	background-image:url("img/icon/plus.gif");
	vertical-align:middle;
}
img.open {
	background-image:url("img/icon/minus.gif");
	vertical-align:middle;
// -->

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<form name="mainform" action="#" method="post">
<input type="hidden" name="input_div" value="<?=$input_div?>">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#35B341">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>職員検索</b></font></td>
<td>&nbsp</td>
<td width="10">&nbsp</td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" width="10" height="10" alt=""><br>

<?
// タブ表示
show_emp_list_header_tab($session, $fname, $input_div, $call_screen);
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
<tr>
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td>

<!-- ここから -->

<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#DFFFDC">
<td width="50"></td>
<td width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員氏名</font></td>
<td width="*"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属</font></td>
</tr>

<?
foreach ($employees as $employee) {
?>
<tr height="22" bgcolor="#FFFFFF">
<td align="center"><input type="button" value="コピー" onclick="copy_emp('<?=$employee['emp_id']?>', '<?=$employee['name']?>');"></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=htmlspecialchars($employee['name'])?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$employee['belong_to']?></font></td>
</tr>

<?
}
?>
</table>

<!-- ここまで -->

</td>
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>

</form>	
</body>
</html>




<?

//==============================
//データベース接続を閉じる
//==============================
pg_close($con);
?>
