<?
//ini_set( 'display_errors', 1 );
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("cl_application_category_list.ini");
require_once("cl_application_template.ini");
require_once("show_select_values.ini");
require_once("cl_yui_calendar_util.ini");
require_once("get_values.ini");
require_once("cl_application_draft_template.ini");
require_once("cl_application_workflow_common_class.php");
require_once("cl_application_common.ini");
require_once("cl_common.ini");
require_once("cl_title_name.ini");
require_once(dirname(__FILE__) . "/cl_common_log_class.inc");

$log = new cl_common_log_class(basename(__FILE__,'.php'));
$log->info(basename(__FILE__)." START");

//====================================
//画面名
//====================================
$fname = $PHP_SELF;
$log->debug('$fname='.$fname);

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	$log->error('セッションのチェックエラー');
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session, $CAS_MENU_AUTH, $fname);
if($wkfw=="0"){
	$log->error('権限チェックエラー');
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
// ワークフロー権限の取得
$workflow_auth = check_authority($session, $CL_MANAGE_AUTH, $fname);

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	$log->error('DBコネクション取得エラー');
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
// ログインユーザの職員IDを取得
//====================================
$sql = "select emp_id from session where session_id = '$session'";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	$log->error('ログインユーザの職員IDを取得エラー');
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");
$log->debug('$emp_id='.$emp_id,__FILE__,__LINE__);
$obj = new cl_application_workflow_common_class($con, $fname);

$cl_title = cl_title_name();
$log->debug('$cl_title='.$cl_title,__FILE__,__LINE__);

?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$cl_title?> | 申請</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<?

$log->debug('$wkfw_id='.$wkfw_id,__FILE__,__LINE__);
if($wkfw_id != "")
{
	//カレンダー外部ファイルの読み込み
	write_yui_calendar_use_file_read_0_12_2();

	$log->debug('$apply_id='.$apply_id,__FILE__,__LINE__);
	$log->debug('$mode='.$mode,__FILE__,__LINE__);
	if($apply_id == "")
	{
		// ワークフロー情報取得
		$arr_wkfwmst       = $obj->get_wkfwmst($wkfw_id);
		$wkfw_content      = $arr_wkfwmst[0]["wkfw_content"];
		$wkfw_content_type = $arr_wkfwmst[0]["wkfw_content_type"];
		$short_wkfw_name   = $arr_wkfwmst[0]["short_wkfw_name"];

		$wkfw_history_no = $obj->get_max_wkfw_history_no($wkfw_id, "ALIAS");

	}
	else
	{
		$arr_apply_wkfwmst = $obj->get_apply_wkfwmst($apply_id);
		$wkfw_content      = $arr_apply_wkfwmst[0]["wkfw_content"];
		$wkfw_content_type = $arr_apply_wkfwmst[0]["wkfw_content_type"];
		$apply_content     = $arr_apply_wkfwmst[0]["apply_content"];
		$wkfw_history_no   = $arr_apply_wkfwmst[0]["wkfw_history_no"];
		$short_wkfw_name   = $arr_apply_wkfwmst[0]["short_wkfw_name"];

		$arr_option = "";

	}
}


?>
<script type="text/javascript">

function reload_apply_page()
{
<?
    if($apply_id != "" && $wkfw_id != "")
    {
?>
	document.apply.action="ladder_menu.php?session=<?=$session?>&apply_id=<?=$apply_id?>&wkfw_id=<?=$wkfw_id?>";
<?
    }
    else
    {
?>
	document.apply.action="ladder_menu.php?session=<?=$session?>";
<?
    }
?>
	document.apply.submit();
}


function init() {

	if(window.start_auto_session_update)
    {
		start_auto_session_update();
	}

	if('<?=$back?>' == 't') {
<?

		$approve_num = $_POST[approve_num];
		for($i=1; $i<=$approve_num;$i++) {

			$regist_emp_id = "regist_emp_id$i";
			$radio_emp_id = "radio_emp_id$i";
			$approve_name = "approve_name$i";

?>
			var radio_emp_id = 'radio_emp_id' + <?=$i?>;
			if(document.getElementById('<?=$radio_emp_id?>')) {
				for(j=0; j<document.apply.elements['<?=$radio_emp_id?>'].length; j++) {
					if(document.apply.elements['<?=$radio_emp_id?>'][j].value == '<?=$_POST[$regist_emp_id];?>') {
						document.apply.elements['<?=$radio_emp_id?>'][j].checked = true;
					}
				}
				document.apply.elements['<?=$regist_emp_id?>'].value = '<?=$_POST[$regist_emp_id];?>';
			}
			if(document.getElementById('<?=$approve_name?>')) {
				document.apply.elements['<?=$approve_name?>'].value = '<?=$_POST[$approve_name];?>';
				document.apply.elements['<?=$regist_emp_id?>'].value = '<?=$_POST[$regist_emp_id];?>';
			}
<?
		}
?>

	}
}

function attachFile() {
	window.open('cl_apply_attach.php?session=<? echo($session); ?>', 'newwin', 'width=640,height=480,scrollbars=yes');
}

function detachFile(e) {
	if (e == undefined) {
		e = window.event;
	}

	var btn_id;
	if (e.target) {
		btn_id = e.target.getAttribute('id');
	} else {
		btn_id = e.srcElement.id;
	}
	var id = btn_id.replace('btn_', '');

	var p = document.getElementById('p_' + id);
	document.getElementById('attach').removeChild(p);
}


// 申請処理
function apply_regist(apv_num, precond_num) {

	// 申請書入力チェック
	var rtn=NewApl_Validate();
	if(rtn==false){
		return;
	}

	// 未入力チェック
	for(i=1; i<=precond_num; i++)
	{
		obj = 'precond_apply_id' + i;
		apply_id = document.apply.elements[obj].value;
		if(apply_id == "") {
			alert('承認確定の申請書を設定してくだい。');
			return;
		}
	}

	var apv_selected = false;
	var apv_all_selected = true;
	for(i=1; i<=apv_num; i++) {
		obj = 'regist_emp_id' + i;
		emp_id = document.apply.elements[obj].value;
		if (emp_id == "") {
			apv_all_selected = false;
		} else {
			apv_selected = true;
		}
	}
	if (!apv_selected) {
		alert('承認者を選択してください。');
		return;
	}
	if (!apv_all_selected && !confirm('指定されていない承認者がありますが、送信しますか？')) {
		return;
	}

	if (window.InputCheck) {
		if (!InputCheck()) {
			return;
		}
	}

	// 重複チェック
	dpl_flg = false;
	for(i=1; i<=apv_num; i++) {
		obj_src = 'regist_emp_id' + i;
		emp_id_src = document.apply.elements[obj_src].value;
		if (emp_id_src == '') continue;

		for(j=i+1; j<=apv_num; j++) {
			obj_dist = 'regist_emp_id' + j;
			emp_id_dist = document.apply.elements[obj_dist].value;
			if(emp_id_src == emp_id_dist) {
				dpl_flg = true;
			}
		}
	}

	if(dpl_flg)
	{
		if (!confirm('承認者が重複していますが、申請しますか？'))
		{
			return;
		}
	}
	document.apply.action="cl_application_submit.php";
	document.apply.mode.value = "apply_regist";
	document.apply.submit();

}

// 下書き保存
function draft_regist()
{
//	document.apply.action="cl_application_submit.php?draft=on";
	document.apply.action="cl_application_submit.php";
	document.apply.draft.value="on";
	document.apply.mode.value = "draft_regist";
	document.apply.submit();
}

function apply_printwin(id)
{


<?
	// 新規申請
	if($wkfw_id != "" && $apply_id == "")
	{
		?>
		//window.open('cl_application_template_print.php?session=<?=$session?>&fname=<?=$fname?>&wkfw_id=<?=$wkfw_id?>&achievement_order=<?=$achievement_order?>&back=f&mode=apply_print&simple=<?=$simple?>', 'apply_print', 'width=670,height=700,scrollbars=yes');

		window.open('', 'apply_print', 'width=670,height=700,scrollbars=yes');

		document.apply.action="cl_application_template_print.php";
		document.apply.target="apply_print";
		document.apply.mode.value = "apply_print";
		//session,wkfw_idはhidden初期設定値を使用
		document.apply.submit();

		document.apply.target="_self";
		<?
	}
	// 下書保存
	else if($wkfw_id != "" && $apply_id != "")
	{
		?>
		//window.open('cl_application_draft_template_print.php?session=<?=$session?>&fname=<?=$fname?>&target_apply_id=<?=$apply_id?>&achievement_order=<?=$achievement_order?>&back=f&mode=apply_print&simple=<?=$simple?>', 'apply_print', 'width=670,height=700,scrollbars=yes');

		window.open('', 'apply_print', 'width=670,height=700,scrollbars=yes');

		document.apply.action="cl_application_draft_template_print.php";
		document.apply.target="apply_print";
		document.apply.mode.value = "apply_print";
		//session,apply_idはhidden初期設定値を使用
		document.apply.submit();

		document.apply.target="_self";
		<?
	}
?>

//	document.apply.action="ladder_menu.php";
//	document.apply.mode.value = "apply_printwin";
//	if(id != "")
//	{
//		document.apply.apply_id.value = id;
//	}
//	document.apply.submit();

}


function apply_draft(id, apv_num, precond_num)
{
	// 申請書入力チェック
	var rtn=DraftApl_Validate();
	if(rtn==false){
		return;
	}


	// 未入力チェック
	for(i=1; i<=precond_num; i++)
	{
		obj = 'precond_apply_id' + i;
		apply_id = document.apply.elements[obj].value;
		if(apply_id == "") {
			alert('承認確定の申請書を設定してくだい。');
			return;
		}
	}

	var apv_selected = false;
	var apv_all_selected = true;
	for(i=1; i<=apv_num; i++) {
		obj = 'regist_emp_id' + i;
		emp_id = document.apply.elements[obj].value;
		if (emp_id == "") {
			apv_all_selected = false;
		} else {
			apv_selected = true;
		}
	}
	if (!apv_selected) {
		alert('承認者を選択してください。');
		return;
	}

	if (!apv_all_selected && !confirm('指定されていない承認者がありますが、送信しますか？')) {
		return;
	}

	if (window.InputCheck) {
		if (!InputCheck()) {
			return;
		}
	}

	// 重複チェック
	dpl_flg = false;
	for(i=1; i<=apv_num; i++) {
		obj_src = 'regist_emp_id' + i;
		emp_id_src = document.apply.elements[obj_src].value;
		if (emp_id_src == '') continue;

		for(j=i+1; j<=apv_num; j++) {
			obj_dist = 'regist_emp_id' + j;
			emp_id_dist = document.apply.elements[obj_dist].value;
			if(emp_id_src == emp_id_dist) {
				dpl_flg = true;
			}
		}
	}

	if(dpl_flg)
	{
		if (!confirm('承認者が重複していますが、申請しますか？'))
		{
			return;
		}
	}

//	document.apply.action="cl_application_draft_submit.php?apply_id=" + id;
	document.apply.action="cl_application_draft_submit.php";
	document.apply.apply_id.value=id;
	document.apply.mode.value = "apply_draft";

	//alert("apply_draft() END");

	document.apply.submit();

}

function draft_update(id)
{
//	document.apply.action="cl_application_draft_submit.php?apply_id=" + id + "&draft=on";
	document.apply.action="cl_application_draft_submit.php";
	document.apply.apply_id.value=id;
	document.apply.draft.value="on";
	document.apply.mode.value = "draft_update";
	document.apply.submit();
}

function draft_delete(id)
{
//	document.apply.action="cl_application_draft_delete.php?apply_id=" + id;
	document.apply.action="cl_application_draft_delete.php";
	document.apply.apply_id.value=id;
	document.apply.mode.value = "draft_delete";
	document.apply.submit();
}


// テキストエリア行数の自動拡張
var ResizingTextArea = Class.create();

ResizingTextArea.prototype = {
    defaultRows: 1,

    initialize: function(field)
    {
        this.defaultRows = Math.max(field.rows, 1);
        this.resizeNeeded = this.resizeNeeded.bindAsEventListener(this);
        Event.observe(field, "click", this.resizeNeeded);
        Event.observe(field, "keyup", this.resizeNeeded);
    },

    resizeNeeded: function(event)
    {
        var t = Event.element(event);
        var lines = t.value.split('\n');
        var newRows = lines.length + 1;
        var oldRows = t.rows;
        for (var i = 0; i < lines.length; i++)
        {
            var line = lines[i];
            if (line.length >= t.cols) newRows += Math.floor(line.length / t.cols);
        }
        if (newRows > t.rows) t.rows = newRows;
        if (newRows < t.rows) t.rows = Math.max(this.defaultRows, newRows);
    }
}

// 全テキストエリアの行数変更
function resizeAllTextArea() {
	var objs = document.getElementsByTagName('textarea');
	for (var i = 0, j = objs.length; i < j; i++) {
	    var t = objs[i];

		//不活性では無い場合はリサイズの対象外
	    if(! t.disabled)
	    {
		    continue;
		}

		var defaultRows = Math.max(t.rows, 1);
	    var lines = t.value.split('\n');
	    var newRows = lines.length + 1;
	    var oldRows = t.rows;
	    for (var k = 0; k < lines.length; k++)
	    {
	        var line = lines[k];
	        if (line.length >= t.cols) newRows += Math.floor(line.length / t.cols);
	    }
	    if (newRows > t.rows) t.rows = newRows;
	    if (newRows < t.rows) t.rows = Math.max(defaultRows, newRows);
	}
}
// 申請書　画面コントロール
function DisplayControl(wkfw_id,apply_id){
	//alert("wkfw_id=" + wkfw_id + " apply_id="+apply_id);

	// 申請画面初期表示
	if( wkfw_id == '' && apply_id == ''){
		// なにもしない
	}
	// 新規申請の場合
	if( wkfw_id != '' && apply_id == ''){
		// テンプレートに実装されている「DisplayControl_ApplyAdd()」を呼び出す。
		NewApl_Display();
	}
	// 新規申請(一時保存)の場合
	if( wkfw_id != '' && apply_id != ''){
		// テンプレートに実装されている「DisplayControl_ApplyEdit(apply_id)」を呼び出す。
		DraftApl_Display(apply_id);
	}
}

// 研修選択時　承認者欄　主催者表示
function SetApvEmpTraining_Opener(training_id){
	iDummy = 0;
	jQuery.getJSON(
		'cl/common/cl_apvemp_opener_json.php'		,	// データ取得スクリプト
		{training_id:training_id}					,	// パラメーター
		function( param , status ) {					// 画面表示処理

			if(status=="success"){
				/* 読み込んだデータで表示する */
				Display_ApvEmpTraining_Opener( param );
			}
			else{
				alert("承認者の取得に失敗しました。");
				showLoginPage(window);
			}
		}
	);
}

// 研修選択時　承認者欄　講師表示
function SetApvEmpTraining_Teacher(training_id){
	iDummy = 0;
	jQuery.getJSON(
		'cl/common/cl_apvemp_teacher_json.php'		,	// データ取得スクリプト
		{training_id:training_id}					,	// パラメーター
		function( param , status ) {					// 画面表示処理

			if(status=="success"){
				/* 読み込んだデータで表示する */
				Display_ApvEmpTraining_Teacher( param );
			}
			else{
				alert("承認者の取得に失敗しました。");
				showLoginPage(window);
			}
		}
	);
}

// 課題選択時　承認者欄　課題提出先表示
function SetApvEmpTheme_Present(theme_id){
	iDummy = 0;
	jQuery.getJSON(
		'cl/common/cl_apvemp_theme_present_json.php'		,	// データ取得スクリプト
		{theme_id:theme_id}									,	// パラメーター
		function( param , status ) {							// 画面表示処理

			if(status=="success"){
				/* 読み込んだデータで表示する */
				Display_ApvEmpTheme_Present( param );
			}
			else{
				alert("承認者の取得に失敗しました。");
				showLoginPage(window);
			}
		}
	);
}

// 2012/07/18 Yamagawa add(s)
/**
 * 審議会表示
 * @param object data 審議会
 */
// 2012/07/18 Yamagawa add(e)

/**
 * 主催者表示
 * @param object data 院内研修データ
 */
function Display_ApvEmpTraining_Opener(data){

	var pos = -1;
	var no = 0;

	var rows = document.getElementsByTagName('span');
	for (i = 0; i < rows.length; i++) {
		pos = rows[i].id.indexOf('apvopener_');
		if (pos == 0){
			no = rows[i].id.substr(10);
			rows[i].innerHTML = data.emp_lt_nm + ' ' + data.emp_ft_nm;
			document.getElementById('regist_emp_id' + no).value = data.training_opener;
			document.getElementById('st_div' + no).value = '5';
			document.getElementById('parent_pjt_id' + no).value = '';
			document.getElementById('child_pjt_id' + no).value = '';
		}

	}

}

/**
 * 講師表示
 * @param object data 院内研修データ
 */
function Display_ApvEmpTraining_Teacher(data){

	var html_build = '';
	var pos = -1;
	var no = 0;
	var empcnt = 0;
	var apvteacher_num = 0;
	var apvteacher_span_num = 0;
	var emp_single_id = '';
	var emp_single_nm = '';

	if (data.training_teacher5 != ''){
		empcnt++;
		emp_single_id = data.training_teacher5;
		emp_single_nm  = data.emp_lt_nm5;
		emp_single_nm += ' ';
		emp_single_nm += data.emp_ft_nm5;
	}
	if (data.training_teacher4 != ''){
		empcnt++;
		emp_single_id = data.training_teacher4;
		emp_single_nm  = data.emp_lt_nm4;
		emp_single_nm += ' ';
		emp_single_nm += data.emp_ft_nm4;
	}
	if (data.training_teacher3 != ''){
		empcnt++;
		emp_single_id = data.training_teacher3;
		emp_single_nm  = data.emp_lt_nm3;
		emp_single_nm += ' ';
		emp_single_nm += data.emp_ft_nm3;
	}
	if (data.training_teacher2 != ''){
		empcnt++;
		emp_single_id = data.training_teacher2;
		emp_single_nm  = data.emp_lt_nm2;
		emp_single_nm += ' ';
		emp_single_nm += data.emp_ft_nm2;
	}
	if (data.training_teacher1 != ''){
		empcnt++;
		emp_single_id = data.training_teacher1;
		emp_single_nm  = data.emp_lt_nm1;
		emp_single_nm += ' ';
		emp_single_nm += data.emp_ft_nm1;
	}

	var rows = document.getElementsByTagName('span');

	for (i = 0; i < rows.length; i++) {

		pos = rows[i].id.indexOf('apvteacher_');
		if (pos == 0){
			apvteacher_span_num++;
			no = rows[i].id.substr(11);
			if (empcnt == 0){
				html_build = '該当者なし';
			} else if(empcnt == 1){
				html_build = emp_single_nm;
				apvteacher_num += 1;
			} else {
				if (document.getElementById('multi_apv_flg' + no).value == 't') {
					html_build  = HTML_Build_ApvEmpTraining_Teacher(true ,no ,data.training_teacher1 ,data.emp_lt_nm1 ,data.emp_ft_nm1 ,true);
					html_build += HTML_Build_ApvEmpTraining_Teacher(true ,no ,data.training_teacher2 ,data.emp_lt_nm2 ,data.emp_ft_nm2 ,true);
					html_build += HTML_Build_ApvEmpTraining_Teacher(true ,no ,data.training_teacher3 ,data.emp_lt_nm3 ,data.emp_ft_nm3 ,true);
					html_build += HTML_Build_ApvEmpTraining_Teacher(true ,no ,data.training_teacher4 ,data.emp_lt_nm4 ,data.emp_ft_nm4 ,true);
					html_build += HTML_Build_ApvEmpTraining_Teacher(true ,no ,data.training_teacher5 ,data.emp_lt_nm5 ,data.emp_ft_nm5 ,true);
					apvteacher_num += empcnt;

				} else {
					html_build  = HTML_Build_ApvEmpTraining_Teacher(false ,no ,data.training_teacher1 ,data.emp_lt_nm1 ,data.emp_ft_nm1 ,true);
					html_build += HTML_Build_ApvEmpTraining_Teacher(false ,no ,data.training_teacher2 ,data.emp_lt_nm2 ,data.emp_ft_nm2 ,html_build == '');
					html_build += HTML_Build_ApvEmpTraining_Teacher(false ,no ,data.training_teacher3 ,data.emp_lt_nm3 ,data.emp_ft_nm3 ,html_build == '');
					html_build += HTML_Build_ApvEmpTraining_Teacher(false ,no ,data.training_teacher4 ,data.emp_lt_nm4 ,data.emp_ft_nm4 ,html_build == '');
					html_build += HTML_Build_ApvEmpTraining_Teacher(false ,no ,data.training_teacher5 ,data.emp_lt_nm5 ,data.emp_ft_nm5 ,html_build == '');
					apvteacher_num += 1;
				}
			}
			rows[i].innerHTML = html_build;
			document.getElementById('regist_emp_id' + no).value = emp_single_id;
			document.getElementById('st_div' + no).value = '6';
			document.getElementById('parent_pjt_id' + no).value = '';
			document.getElementById('child_pjt_id' + no).value = '';
			/*
			if (apvteacher_num > 0) {
				var approve_num_obj = document.getElementById('approve_num');
				var apvteacher_num_obj = document.getElementById('apvteacher_num');
				approve_num_obj.value = parseInt(approve_num_obj.value) - parseInt(apvteacher_num_obj.value) + apvteacher_num;
				apvteacher_num_obj.value = apvteacher_num;
				document.getElementById('approve_num_view').innerHTML = approve_num_obj.value;
			}
			*/
		}

	}

	if (apvteacher_num > 0) {
		var approve_num = parseInt(document.getElementById('approve_num').value);
		var apvteacher_num_obj = document.getElementById('apvteacher_num');
		approve_num = parseInt(approve_num) - apvteacher_span_num + apvteacher_num ;
		apvteacher_num_obj.value = empcnt;
		document.getElementById('approve_num_view').innerHTML = approve_num;
	}

}

function HTML_Build_ApvEmpTraining_Teacher(multi_apv_flg,no,emp_id,emp_lt_nm,emp_ft_nm,checkflg){

	var html_build = '';

	if (emp_id != '') {

		if (multi_apv_flg){
			html_build  = '<input type="checkbox" name="check_emp_id';
			html_build += no;
			html_build += '_';
			html_build += emp_id;
			html_build += '" id="check_emp_id';
			html_build += no;
			html_build += '_';
			html_build += emp_id;
			html_build += '" onclick="set_approve_num(this);';
			html_build += '" value="';
			html_build += emp_id;
		} else {
			html_build  = '<input type="radio" name="radio_emp_id';
			html_build += no;
			html_build += '" onclick="set_emp_st(this, ' + "'" + no + "','6','','');";
			html_build += '" value="';
			html_build += emp_id;
			html_build += ',講師';
		}

		if (checkflg) {
			html_build += '" checked>';
		} else {
			html_build += '" >';
		}

		html_build += emp_lt_nm;
		html_build += ' ';
		html_build += emp_ft_nm;
		html_build += '<br>';

	}

	return html_build;

}

/**
 * 課題提出先表示
 * @param object data 研修課題データ
 */
function Display_ApvEmpTheme_Present(data){

	var pos = -1;
	var no = 0;

	var rows = document.getElementsByTagName('span');
	for (i = 0; i < rows.length; i++) {

		pos = rows[i].id.indexOf('apvpresent_');
		if (pos == 0){
			no = rows[i].id.substr(11);
			rows[i].innerHTML = data.emp_lt_nm + ' ' + data.emp_ft_nm;
			document.getElementById('regist_emp_id' + no).value = data.theme_present;
			document.getElementById('st_div' + no).value = '7';
			document.getElementById('parent_pjt_id' + no).value = '';
			document.getElementById('child_pjt_id' + no).value = '';
		}

	}

}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">


.list {border-collapse:collapse;}
.list td {border:#A0D25A solid 1px;}
.list td td {border-width:0;}

table.block {border-collapse:collapse;}
table.block td {border:#A0D25A solid 1px;padding:1px;}
table.block td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#A0D25A solid 0px;}

table.block2 {border-collapse:collapse;}
table.block2 td {border:#A0D25A solid 1px;padding:1px;}
table.block2 td {border-width:1;}

table.block3 {border-collapse:collapse;}
table.block3 td {border:#A0D25A solid 0px;}
table.block3 td {border-width:0;}


p {margin:0;}
</style>

<script type="text/javascript">

//onload()時の処理
function do_onload()
{
	//初期処理
	init();

	//テンプレート表示コントロール
	DisplayControl('<?=$wkfw_id?>','<?=$apply_id?>');
}

//テンプレートの表示が完了した後の処理
function templateDispAfter()
{
	//テキストエリアのリサイズ処理
	resizeAllTextArea();
}

</script>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="do_onload()">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="ladder_menu.php?session=<? echo($session); ?>"><img src="img/icon/s42.gif" width="32" height="32" border="0" alt="<? echo($cl_title); ?>"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="ladder_menu.php?session=<? echo($session); ?>"><b><? echo($cl_title); ?></b></a> &gt; <a href="ladder_menu.php?session=<? echo($session); ?>"><b>申請</b></a></font></td>
<? if ($workflow_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="cl_workflow_menu.php?session=<? echo($session); ?>&workflow=1"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>

<?
show_applycation_menuitem($session,$fname,"");
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr valign="top">
<td bgcolor="#A0D25A"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>

<img src="img/spacer.gif" alt="" width="1" height="2"><br>



<?
if($wkfw_id != "")
{
?>
<!-- セッションタイムアウト防止 START -->
<SCRIPT type="text/javascript">
function start_auto_session_update()
{
    //10分(10*60*1000ms)後に開始
    setTimeout(auto_sesson_update,600000);
}
function auto_sesson_update()
{
    //セッション更新
    document.session_update_form.submit();

    //10分(10*60*1000ms)後に最呼び出し
    setTimeout(auto_sesson_update,600000);
}
</SCRIPT>
<form name="session_update_form" action="cl_session_update.php" method="post" target="session_update_frame">
<input type="hidden" name="session" value="<?=$session?>">
</form>
<iframe name="session_update_frame" width="0" height="0" frameborder="0"></iframe>
<!-- セッションタイムアウト防止 END -->
	<?
}
?>



<form name="apply" action="#" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="wkfw_id" value="<?=$wkfw_id?>">
<input type="hidden" name="wkfw_type" value="<?=$wkfw_type?>">
<input type="hidden" name="mode" value="">
<input type="hidden" name="simple" value="">
<input type="hidden" name="wkfw_history_no" value="<?=$wkfw_history_no?>">

<input type="hidden" id="short_wkfw_name" name="short_wkfw_name" value="<?=$short_wkfw_name?>">
<input type="hidden" id="achievement_order" name="achievement_order" value="<?=$achievement_order?>">
<input type="hidden" id="ward_dpl_chk" name="ward_dpl_chk" value="">
<input type="hidden" id="draft" name="draft" value="">
<input type="hidden" id="apply_id" name="apply_id" value="<?=$apply_id?>">


<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<table width="100%" border="0" cellspacing="2" cellpadding="2">
<tr valign="top">
<td width="25%">
<?
show_application_cate_list($con, $session, $fname, $wkfw_type, $wkfw_id, $apply_id);
?>
</td>
<td><img src="img/spacer.gif" width="5" height="2" alt=""></td>
<td width="75%">
<?
if($wkfw_id != "" && $apply_id == "")
{
    show_application_template($con, $session, $fname, $wkfw_id, $apply_title, $content, $file_id, $filename, $back, $mode);
}
else if($wkfw_id != "" && $apply_id != "")
{
	show_draft_template($con, $session, $fname, $apply_id, $apply_title, $content, $file_id, $filename, $back, $mode);
}
?>
</td>
</tr>
</table>

</td>
</tr>
</table>


</form>
<!-- 子画面(共通モジュール)を開くためのフォーム及びスクリプト START -->
<form id="common_module_call_form" name="common_module_call_form" method="post" action="cl_common_call.php" target="common_module_window"></form>
<script type="text/javascript">
//子画面(共通モジュール)を開きます。
//
//moduleName 子画面のモジュール名を指定します。
//params     子画面へ送信する専用パラメータを設定します。Objectを指定してください。Objectの属性名が送信パラメータ名となり、値がパラメータとなります。
//option     子画面を開くときのオプションを指定します。(JavaScriptのopen関数のoptionと同じです。)
function commonOpen(moduleName,params,option)
{
	//子画面(共通モジュール)送信用フォームタグ
	var f = document.getElementById("common_module_call_form");

	//共通パラメータ用のhiddenを作成、値を設定。
	var html = "";
	html = html + '<input type="hidden" name="common_module" value="' + moduleName + '">';
	html = html + '<input type="hidden" name="session"       value="<?=$session ?>">';

	//専用パラメータ用のhiddenをparamsから作成。
	for(var key in params)
	{
		html = html + '<input type="hidden" name="' + key + '" value="">';
	}
	f.innerHTML = html;

	//専用パラメータ用のhiddenにparamsから値を設定。
	for(var key in params)
	{
		var hidden = eval("f." + key);
		hidden.value = eval("params." + key);
	}

	//子画面を開く
	window.open('', 'common_module_window', option);

	//子画面へ送信
	f.submit();
}
</script>
<!-- 子画面(共通モジュール)を開くためのフォーム及びスクリプト END -->
</td>
</tr>
</table>
</body>
</html>
<?


// cl/template/tmpディレクトリ作成
$obj->create_wkfwtmp_directory();
// cl/apply/tmpディレクトリ作成
$obj->create_applytmp_directory();

pg_close($con);
$log->info(basename(__FILE__)." END");
?>

