<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" method="post" action="timecard_import_set_amano.php">
<input type="hidden" name="back" value="t">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="no" value="<? echo($no); ?>">
<input type="hidden" name="ins_upd_flg" value="<? echo($ins_upd_flg); ?>">
<input type="hidden" name="absence_reason" value="<? echo($absence_reason); ?>">
<input type="hidden" name="duty_ptn" value="<? echo($duty_ptn); ?>">
<input type="hidden" name="tmcd_group_id" value="<? echo($tmcd_group_id); ?>">
<input type="hidden" name="atdptn_ptn_id" value="<? echo($atdptn_ptn_id); ?>">
<input type="hidden" name="reason_2" value="<? echo($reason_2); ?>">
</form>
<?php
//ファイルの読み込み
require_once("about_comedix.php");

//ページ名
$fname = $PHP_SELF;

//セッションチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$disreg = check_authority($session, 42, $fname);
if ($ptreg == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$con = connect2db($fname);

//重複チェック
$wk_absence_reason = pg_escape_string($absence_reason);
$wk_duty_ptn = pg_escape_string($duty_ptn);

$sql = "select count(*) as cnt from timecard_import_set_amano";
$cond = "where absence_reason = '$wk_absence_reason' and duty_ptn = '$wk_duty_ptn' ";
if ($ins_upd_flg == "2") {
	$cond .= " and no != $no ";
}

$sel = select_from_table($con,$sql,$cond,$fname);
if($sel == 0){
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$cnt = pg_fetch_result($sel, 0, "cnt");

if ($cnt > 0) {
    echo("<script language=\"javascript\">alert(\"勤務区分と不在理由が同じデータがあります\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}


//----------Transaction begin----------


pg_exec($con,"begin transaction");

// 登録値の編集

if ($ins_upd_flg == "1") {
	//途中に追加するため番号をずらす
    $sql = "select max(no) from timecard_import_set_amano ";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$wk_no = intval(pg_fetch_result($sel, 0, 0));

	if ($no < $wk_no) {
        $sql = "update timecard_import_set_amano set no = no + 1 ";
		$set = array();
		$setvalue = array();
		$cond = "where no > $no";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_exec($con,"rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}
	//登録
    $sql = "insert into timecard_import_set_amano (no, absence_reason, duty_ptn, tmcd_group_id, atdptn_ptn_id, reason) values (";
	$content = array($no+1, $wk_absence_reason, $wk_duty_ptn, $tmcd_group_id, $atdptn_ptn_id, $reason_2);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

} else {
	//更新
    $sql = "update timecard_import_set_amano set";
	$set = array("absence_reason", "duty_ptn", "tmcd_group_id", "atdptn_ptn_id", "reason");
	$setvalue = array($wk_absence_reason, $wk_duty_ptn, $tmcd_group_id, $atdptn_ptn_id, $reason_2);
	$cond = "where no = $no";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_exec($con,"rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
}

pg_exec($con, "commit");
pg_close($con);

echo("<script language='javascript'>if(opener && !opener.closed && opener.reload_list){opener.reload_list();};self.close();</script>");
?>
</body>
