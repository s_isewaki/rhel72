<?php
require_once('fpdf153/mbfpdf.php');

/**
 * pdfに表出力を行うクラス
 */
class hiyari_pdf_export extends MBFPDF {
    var $page_width;        //用紙の幅
    var $page_height;       //用紙の高さ
    var $margin;            //マージン
    var $left;              //出力エリア左のX座標
    var $top;               //出力エリア上のY座標
    var $right;             //出力エリア右のX座標
    var $bottom;            //出力エリア下のY座標
    var $line_height;       //行の高さ
    var $page_title;        //ページタイトル
    var $cur_page;          //現在のページ番号
    var $num_pages;         //総ページ数

    /**
     * コンストラクタ
     *
     * @param string $orientation 用紙の向き（P:縦長、L:横長）
     * @param string $unit 単位（pt:point、mm:millimeter、cm:centimeter、in:inch）
     * @param integer $w 用紙幅（A4の場合210）
     * @param integer $h 用紙高さ（A4の場合297）
     * @param integer $m マージン
     * @param integer $line_height 行の高さ
     * @param boolean $auto_page_break_flg 自動改ページフラグ
     */
    function hiyari_pdf_export($orientation='P', $unit='mm', $w=210, $h=297, $m=10, $line_height=5, $auto_page_break_flg=true)
    {
        $this->FPDF($orientation, $unit, array($w, $h));
        $this->page_width = $w;
        $this->page_height = $h;
        $this->margin = $m;
        $this->left = $m;
        $this->top = $m;
        $this->right = $w - $m;
        $this->bottom = $h - $m - $line_height; //出力エリアの下限は、マージン+ページ番号の1行分
        $this->line_height = $line_height;        
        $this->SetAutoPageBreak($auto_page_break_flg, $m + $line_height);
    }

    /**
     * ページタイトル
     *
     * @param array $page_title ページヘッダに出力される文字列の配列
     */
    function SetPageTitle($page_title)
    {
        $this->page_title = $page_title;
    }

    /**
     * ページ番号情報の初期化
     *
     * @param array $num_pages 総ページ数
     */
    function ResetPages($num_pages)
    {
        $this->cur_page = 1;
        $this->num_pages = $num_pages;
    }

    /**
     * ヘッダー
     */
    function Header()
    {
        //ページタイトル
        if (!is_null($this->page_title)){
            foreach ($this->page_title as $title){
                $this->SetFont(PGOTHIC, '', 12);
                $this->Cell(0, $this->line_height, $title, 0, 1, 'C');  //幅、高さ、出力文字列、線(0=なし)、出力後の位置(1=次行)、整列(C=中央)
            }
            $this->top = $this->GetY();
        }
    }

    /**
     * フッター
     */
    function Footer()
    {
        //表の枠線
        $y = $this->GetY();
        $this->Line($this->left,  $this->top, $this->left,  $y);        //左側
        $this->Line($this->left,  $this->top, $this->right, $this->top);//上側
        $this->Line($this->right, $this->top, $this->right, $y);        //右側
        $this->Line($this->left,  $y,         $this->right, $y);        //下側

        //ページ番号
        $this->SetY($this->bottom);
        $this->SetFont(PGOTHIC, '', 10);
        if (is_null($this->cur_page) || is_null($this->num_pages)){
            $this->Cell(0, $this->line_height, $this->PageNo().'/{nb}', 0, 0, 'C');                 //幅：自動、高さ：10、線：なし、出力後の位置：右、整列：中央
        }
        else{
            $this->Cell(0, $this->line_height, $this->cur_page.'/'.$this->num_pages, 0, 0, 'C');    //幅、高さ、出力文字列、線(0=なし)、出力後の位置(1=次行)、整列(C=中央)
            $this->cur_page += 1;
        }
    }

    /**
     * タイトルと内容の2列の情報をテーブル形式で出力
     * 
     * @param array $list 出力するデータ配列
     * @param integer $title_width タイトル幅
     * @return integer 総ページ数
     */
    function Table($list, $title_width=70)
    {
        $this->SetFont(PGOTHIC, '', 10);
        $this->SetFillColor(200, 200, 200);

        //内容の幅とX座標
        $contents_width = $this->page_width - ($this->margin*2) - $title_width;
        $x = $this->left + $title_width;

        //出力
        foreach ($list as $data){
            $y = $this->GetY();

            //==================================================================
            //グループの終了
            //==================================================================
            if (isset($data['end_of_group'])){
                $this->Cell($title_width+$contents_width, 1, '', 1, 1, 'L', true);
            }
            
            //==================================================================
            //グループタイトルの場合
            //==================================================================
            else if ($data['is_group_title']){
                //グループタイトル出力後、すぐに改ページになってしまう場合は、先に改ページ
                if ( ($this->bottom - $y) < ($this->line_height * 2) ){
                    $this->AddPage();
                }
                
                //グループタイトル出力
                $this->Cell($title_width+$contents_width, $this->line_height, $data['title'], 'T', 1, 'L', true);
            }

            //==================================================================
            //通常データの場合
            //==================================================================
            else{
                //-----------------------------------
                //タイトル出力
                //-----------------------------------
                //タイトルがページ内に収まらない場合は改ページ
                //（タイトルの途中で改ページになった場合、内容の出力位置がタイトルと揃わなくなってしまうため。）
                $cnt = $this->GetLineCount($data['title'], $title_width);
                if ( ($this->bottom - $y) < ($this->line_height * $cnt) ){
                    $this->AddPage();
                    $y = $this->GetY();
                }

                $this->MultiCell($title_width, $this->line_height, $data['title'], 'TR');
                $bottom1 = $this->GetY();

                //-----------------------------------
                //内容出力
                //-----------------------------------
                $this->SetXY($x, $y);
                $this->MultiCell($contents_width, $this->line_height, $data['contents'], 'TL');
                $bottom2 = $this->GetY();

                //-----------------------------------
                //次の出力位置設定
                //-----------------------------------
                if ($bottom2 > $y){             //内容の途中で自動改ページされていない場合
                    if ($bottom2 < $bottom1){   //タイトルの方が内容より高さがある場合、タイトルの下側に設定
                        $this->SetY($bottom1);
                    }
                }
            }
        }

        return $this->PageNo();
    }

   /**
    * 文字列を最大幅内に表示するために必要な行数を取得
    *
    * @param string $str 文字列
    * @param integer $max_width 最大幅
    * @param integer $min_len 最小文字数
    * @return integer 行数
   */
   function GetLineCount($str, $max_width, $min_len=18)
    {
        $cnt = 1;

        if ($this->GetStringWidth($str) > $max_width){
            $next_str = $str;
            while (true){
                $len = mb_strlen($next_str);
                for ($i=$min_len-1; $i<=$len; $i++){
                    $temp = mb_substr($next_str, 0, $i);
                    if ($this->GetStringWidth($temp) > $max_width){
                        $cnt++;
                        break;
                    }
                }
                if ($i>=$len) break;
                $next_str = mb_substr($next_str, ($i-1));
            }
        }

        return $cnt;
    }
}