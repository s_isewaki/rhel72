<?

//======================================================================================================================================================
// 入力エラー時の送信フォーム
//======================================================================================================================================================
?>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" method="post" action="hiyari_el_folder_update.php">
<input type="hidden" name="folder_name" value="<? echo($folder_name); ?>">
<input type="hidden" name="archive" value="<? echo($archive); ?>">
<input type="hidden" name="category" value="<? echo($category); ?>">
<input type="hidden" name="cate_id" value="<? echo($cate_id); ?>">
<input type="hidden" name="folder_id" value="<? echo($folder_id); ?>">
<input type="hidden" name="ref_dept_st_flg" value="<? echo($ref_dept_st_flg); ?>">
<input type="hidden" name="ref_dept_flg" value="<? echo($ref_dept_flg); ?>">
<input type="hidden" name="ref_class_src" value="<? echo($ref_class_src); ?>">
<input type="hidden" name="ref_atrb_src" value="<? echo($ref_atrb_src); ?>">
<?
if (is_array($hid_ref_dept))
{
	foreach ($hid_ref_dept as $tmp_dept_id)
	{
		echo("<input type=\"hidden\" name=\"ref_dept[]\" value=\"$tmp_dept_id\">\n");
	}
}
else
{
	$hid_ref_dept = array();
}
?>
<input type="hidden" name="ref_st_flg" value="<? echo($ref_st_flg); ?>">
<?
if (is_array($ref_st))
{
	foreach ($ref_st as $tmp_st_id)
	{
		echo("<input type=\"hidden\" name=\"ref_st[]\" value=\"$tmp_st_id\">\n");
	}
}
else
{
	$ref_st = array();
}
?>
<input type="hidden" name="target_id_list1" value="<? echo($target_id_list1); ?>">
<input type="hidden" name="target_id_list2" value="<? echo($target_id_list2); ?>">
<?
if ($target_id_list1 != "")
{
	$hid_ref_emp = split(",", $target_id_list1);
}
else
{
	$hid_ref_emp = array();
}
?>
<input type="hidden" name="upd_dept_st_flg" value="<? echo($upd_dept_st_flg); ?>">
<input type="hidden" name="upd_dept_flg" value="<? echo($upd_dept_flg); ?>">
<input type="hidden" name="upd_class_src" value="<? echo($upd_class_src); ?>">
<input type="hidden" name="upd_atrb_src" value="<? echo($upd_atrb_src); ?>">
<?
if (is_array($hid_upd_dept))
{
	foreach ($hid_upd_dept as $tmp_dept_id)
	{
		echo("<input type=\"hidden\" name=\"upd_dept[]\" value=\"$tmp_dept_id\">\n");
	}
}
else
{
	$hid_upd_dept = array();
}
?>
<input type="hidden" name="upd_st_flg" value="<? echo($upd_st_flg); ?>">
<?
if (is_array($upd_st))
{
	foreach ($upd_st as $tmp_st_id)
	{
		echo("<input type=\"hidden\" name=\"upd_st[]\" value=\"$tmp_st_id\">\n");
	}
}
else
{
	$upd_st = array();
}
?>
<?
if ($target_id_list2 != "")
{
	$hid_upd_emp = split(",", $target_id_list2);
}
else
{
	$hid_upd_emp = array();
}
?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="path" value="<? echo($path); ?>">
<input type="hidden" name="ref_toggle_mode" value="<? echo($ref_toggle_mode); ?>">
<input type="hidden" name="upd_toggle_mode" value="<? echo($upd_toggle_mode); ?>">
<input type="hidden" name="back" value="t">
</form>
<?


//======================================================================================================================================================
// 初期処理
//======================================================================================================================================================
require_once("about_session.php");
require_once("about_authority.php");
require_once("hiyari_el_common.ini");
require_once("hiyari_common.ini");
require_once("Cmx.php");
require_once("aclg_set.php");

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$auth_id = ($path != "3") ? 47 : 48;
if (check_authority($session, $auth_id, $fname) == "0")
{
	showLoginPage();
	exit;
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

//======================================================================================================================================================
// 入力チェック
//======================================================================================================================================================

//フォルダ名必須チェック
if ($folder_name == "")
{
	echo("<script type=\"text/javascript\">alert('フォルダ名が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}

//フォルダ名長さチェック
if (strlen($folder_name) > 60)
{
	echo("<script type=\"text/javascript\">alert('フォルダ名が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}

//参照権限チェック:(部署指定ありで部署が指定されていない、もしくは役職指定ありで役職が指定されていない) かつ (個人指定されていない)場合
if ((($ref_dept_flg != "1" && count($hid_ref_dept) == 0) || ($ref_st_flg != "1" && count($ref_st) == 0)) && count($hid_ref_emp) == 0)
{
	echo("<script type=\"text/javascript\">alert('参照可能範囲が不正です。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}

//更新権限チェック:(部署指定ありで部署が指定されていない、もしくは役職指定ありで役職が指定されていない) かつ (個人指定されていない)場合
if ((($upd_dept_flg != "1" && count($hid_upd_dept) == 0) || ($upd_st_flg != "1" && count($upd_st) == 0)) && count($hid_upd_emp) == 0)
{
	echo("<script type=\"text/javascript\">alert('更新可能範囲が不正です。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}


//======================================================================================================================================================
// 画面パラメータ精査
//======================================================================================================================================================

if ($ref_dept_st_flg != "t") {$ref_dept_st_flg = "f";}
if ($ref_dept_flg == "") {$ref_dept_flg = null;}
if ($ref_st_flg == "") {$ref_st_flg = null;}
if ($upd_dept_st_flg != "t") {$upd_dept_st_flg = "f";}
if ($upd_dept_flg == "") {$upd_dept_flg = null;}
if ($upd_st_flg == "") {$upd_st_flg = null;}


//======================================================================================================================================================
// DB処理開始
//======================================================================================================================================================

// トランザクションを開始
pg_query($con, "begin");


//======================================================================================================================================================
// ログインユーザの職員情報を取得
//======================================================================================================================================================

$sql = "select emp_id, emp_class, emp_attribute, emp_dept, emp_st from empmst";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel_emp = select_from_table($con, $sql, $cond, $fname);
if ($sel_emp == 0)
{
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel_emp, 0, "emp_id");
$emp_class = pg_fetch_result($sel_emp, 0, "emp_class");
$emp_atrb = pg_fetch_result($sel_emp, 0, "emp_attribute");
$emp_dept = pg_fetch_result($sel_emp, 0, "emp_dept");
$emp_st = pg_fetch_result($sel_emp, 0, "emp_st");


//======================================================================================================================================================
// 更新権限確認
//======================================================================================================================================================

// 管理画面以外の場合
if ($path != "3")
{
	$upd_flg = get_upd_flg($con, $fname, $archive, $cate_id, $folder_id, $emp_id, $emp_class, $emp_atrb, $emp_dept, $emp_st);
	if ($upd_flg == false)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('更新権限がありません。');</script>");
		echo("<script type=\"text/javascript\">document.items.submit();</script>");
		exit;
	}
}


//======================================================================================================================================================
// フォルダ(カテゴリ)情報
//======================================================================================================================================================

// カテゴリを更新する場合
if ($folder_id == "")
{
	// カテゴリ名を更新
	$private_flg = null;//不使用
	$sql = "update el_cate set";
	$set = array("lib_cate_nm", "private_flg", "ref_dept_st_flg", "ref_dept_flg", "ref_st_flg", "upd_dept_st_flg", "upd_dept_flg", "upd_st_flg", "upd_time");
	$setvalue = array(pg_escape_string($folder_name), $private_flg, $ref_dept_st_flg, $ref_dept_flg, $ref_st_flg, $upd_dept_st_flg, $upd_dept_flg, $upd_st_flg, date("YmdHis"));
	$cond = "where lib_cate_id = $cate_id";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0)	
	{
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
}

// フォルダを更新する場合
else
{
	// フォルダ名を更新
	$private_flg = null;//不使用
	$sql = "update el_folder set";
	$set = array("folder_name", "private_flg", "ref_dept_st_flg", "ref_dept_flg", "ref_st_flg", "upd_dept_st_flg", "upd_dept_flg", "upd_st_flg", "upd_time");
	$setvalue = array(pg_escape_string($folder_name), $private_flg, $ref_dept_st_flg, $ref_dept_flg, $ref_st_flg, $upd_dept_st_flg, $upd_dept_flg, $upd_st_flg, date("YmdHis"));
	$cond = "where folder_id = $folder_id";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0)
	{
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}


//======================================================================================================================================================
// 権限情報
//======================================================================================================================================================
//(部署＋役職＋個人)×(参照＋更新)＝計６権限をデリートインサート　＋配下フォルダへの権限継承

// カテゴリを更新する場合
if ($folder_id == "")
{
	//対象テーブル
	$refdept_tblname = "el_cate_ref_dept";
	$refst_tblname = "el_cate_ref_st";
	$refemp_tblname = "el_cate_ref_emp";
	$upddept_tblname = "el_cate_upd_dept";
	$updst_tblname = "el_cate_upd_st";
	$updemp_tblname = "el_cate_upd_emp";
	//ID
	$id_name = "lib_cate_id";
	$lib_id = $cate_id;
}

// フォルダを更新する場合
else
{
	//対象テーブル
	$refdept_tblname = "el_folder_ref_dept";
	$refst_tblname = "el_folder_ref_st";
	$refemp_tblname = "el_folder_ref_emp";
	$upddept_tblname = "el_folder_upd_dept";
	$updst_tblname = "el_folder_upd_st";
	$updemp_tblname = "el_folder_upd_emp";
	//ID
	$id_name = "folder_id";
	$lib_id = $folder_id;
}

// 参照可能部署情報を登録
$sql = "delete from $refdept_tblname";
$cond = "where $id_name = $lib_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0)
{
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
foreach ($hid_ref_dept as $tmp_val)
{
	list($tmp_class_id, $tmp_atrb_id, $tmp_dept_id) = split("-", $tmp_val);
	$sql = "insert into $refdept_tblname ($id_name, class_id, atrb_id, dept_id) values (";
	$content = array($lib_id, $tmp_class_id, $tmp_atrb_id, $tmp_dept_id);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// 参照可能役職情報を登録
$sql = "delete from $refst_tblname";
$cond = "where $id_name = $lib_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0)
{
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
foreach ($ref_st as $tmp_st_id)
{
	$sql = "insert into $refst_tblname ($id_name, st_id) values (";
	$content = array($lib_id, $tmp_st_id);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// 参照可能職員情報を登録
$sql = "delete from $refemp_tblname";
$cond = "where $id_name = $lib_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0)
{
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
foreach ($hid_ref_emp as $tmp_emp_id)
{
	$sql = "insert into $refemp_tblname ($id_name, emp_id) values (";
	$content = array($lib_id, $tmp_emp_id);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// 更新可能部署情報を登録
$sql = "delete from $upddept_tblname";
$cond = "where $id_name = $lib_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0)
{
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
foreach ($hid_upd_dept as $tmp_val)
{
	list($tmp_class_id, $tmp_atrb_id, $tmp_dept_id) = split("-", $tmp_val);
	$sql = "insert into $upddept_tblname ($id_name, class_id, atrb_id, dept_id) values (";
	$content = array($lib_id, $tmp_class_id, $tmp_atrb_id, $tmp_dept_id);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// 更新可能役職情報を登録
$sql = "delete from $updst_tblname";
$cond = "where $id_name = $lib_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0)
{
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
foreach ($upd_st as $tmp_st_id)
{
	$sql = "insert into $updst_tblname ($id_name, st_id) values (";
	$content = array($lib_id, $tmp_st_id);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// 更新可能職員情報を登録
$sql = "delete from $updemp_tblname";
$cond = "where $id_name = $lib_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0)
{
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
foreach ($hid_upd_emp as $tmp_emp_id)
{
	$sql = "insert into $updemp_tblname ($id_name, emp_id) values (";
	$content = array($lib_id, $tmp_emp_id);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}


// 階層下のフォルダの権限更新
$tree = lib_get_folder_tree($con, $archive, $cate_id, $folder_id, $emp_id, $fname, null, '');
if (count($tree) > 0)
{
	update_folder_tree_flg($con, $fname, $tree, $ref_dept_st_flg, $ref_dept_flg, $ref_st_flg, $upd_dept_st_flg, $upd_dept_flg, $upd_st_flg, $hid_ref_dept, $ref_st, $hid_ref_emp, $hid_upd_dept, $upd_st, $hid_upd_emp);
}


//======================================================================================================================================================
// DB処理終了
//======================================================================================================================================================

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);


//======================================================================================================================================================
// 処理終了通知・画面クローズ
//======================================================================================================================================================

//※ユーザー画面・管理画面共通
?>
<script type="text/javascript">
if(window.opener && !window.opener.closed && window.opener.reload_page)
{
	window.opener.reload_page();
	window.close();
}
</script>
<?



//========================================================================================================================================================================================================
// 内部関数
//========================================================================================================================================================================================================


//階層下のフォルダの権限更新
function update_folder_tree_flg($con, $fname, $tree, $ref_dept_st_flg, $ref_dept_flg, $ref_st_flg, $upd_dept_st_flg, $upd_dept_flg, $upd_st_flg, $hid_ref_dept, $ref_st, $hid_ref_emp, $hid_upd_dept, $upd_st, $hid_upd_emp)
{
	foreach ($tree as $tmp_folder)
	{
	
		$tmp_type = $tmp_folder["type"];
		$tmp_id = $tmp_folder["id"];

		// フォルダの場合更新
		if ($tmp_type == "folder")
		{
			update_folder_flg($con, $fname, $tmp_id, $ref_dept_st_flg, $ref_dept_flg, $ref_st_flg, $upd_dept_st_flg, $upd_dept_flg, $upd_st_flg, $hid_ref_dept, $ref_st, $hid_ref_emp, $hid_upd_dept, $upd_st, $hid_upd_emp);
		}
		update_folder_tree_flg($con, $fname, $tmp_folder, $ref_dept_st_flg, $ref_dept_flg, $ref_st_flg, $upd_dept_st_flg, $upd_dept_flg, $upd_st_flg, $hid_ref_dept, $ref_st, $hid_ref_emp, $hid_upd_dept, $upd_st, $hid_upd_emp);
	}

}

function update_folder_flg($con, $fname, $folder_id, $ref_dept_st_flg, $ref_dept_flg, $ref_st_flg, $upd_dept_st_flg, $upd_dept_flg, $upd_st_flg, $hid_ref_dept, $ref_st, $hid_ref_emp, $hid_upd_dept, $upd_st, $hid_upd_emp)
{
	//==================================================
	//フォルダ更新
	//==================================================
	
	// フォルダを更新
	$sql = "update el_folder set";
	$set = array("ref_dept_st_flg", "ref_dept_flg", "ref_st_flg", "upd_dept_st_flg", "upd_dept_flg", "upd_st_flg");
	$setvalue = array($ref_dept_st_flg, $ref_dept_flg, $ref_st_flg, $upd_dept_st_flg, $upd_dept_flg, $upd_st_flg);
	$cond = "where folder_id = $folder_id";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0)
	{
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	//==================================================
	//権限更新
	//==================================================
	//(部署＋役職＋個人)×(参照＋更新)＝計６系統権限をデリートインサート
	
	// 参照可能部署情報を登録
	$sql = "delete from el_folder_ref_dept";
	$cond = "where folder_id = $folder_id";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	foreach ($hid_ref_dept as $tmp_val)
	{
		list($tmp_class_id, $tmp_atrb_id, $tmp_dept_id) = split("-", $tmp_val);
		$sql = "insert into el_folder_ref_dept (folder_id, class_id, atrb_id, dept_id) values (";
		$content = array($folder_id, $tmp_class_id, $tmp_atrb_id, $tmp_dept_id);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0)
		{
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 参照可能役職情報を登録
	$sql = "delete from el_folder_ref_st";
	$cond = "where folder_id = $folder_id";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	foreach ($ref_st as $tmp_st_id)
	{
		$sql = "insert into el_folder_ref_st (folder_id, st_id) values (";
		$content = array($folder_id, $tmp_st_id);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0)
		{
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 参照可能職員情報を登録
	$sql = "delete from el_folder_ref_emp";
	$cond = "where folder_id = $folder_id";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	foreach ($hid_ref_emp as $tmp_emp_id)
	{
		$sql = "insert into el_folder_ref_emp (folder_id, emp_id) values (";
		$content = array($folder_id, $tmp_emp_id);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0)
		{
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 更新可能部署情報を登録
	$sql = "delete from el_folder_upd_dept";
	$cond = "where folder_id = $folder_id";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	foreach ($hid_upd_dept as $tmp_val)
	{
		list($tmp_class_id, $tmp_atrb_id, $tmp_dept_id) = split("-", $tmp_val);
		$sql = "insert into el_folder_upd_dept (folder_id, class_id, atrb_id, dept_id) values (";
		$content = array($folder_id, $tmp_class_id, $tmp_atrb_id, $tmp_dept_id);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0)
		{
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 更新可能役職情報を登録
	$sql = "delete from el_folder_upd_st";
	$cond = "where folder_id = $folder_id";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	foreach ($upd_st as $tmp_st_id)
	{
		$sql = "insert into el_folder_upd_st (folder_id, st_id) values (";
		$content = array($folder_id, $tmp_st_id);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0)
		{
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 更新可能職員情報を登録
	$sql = "delete from el_folder_upd_emp";
	$cond = "where folder_id = $folder_id";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	foreach ($hid_upd_emp as $tmp_emp_id)
	{
		$sql = "insert into el_folder_upd_emp (folder_id, emp_id) values (";
		$content = array($folder_id, $tmp_emp_id);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
}

?>
