<?
require_once("about_session.php");
require_once("about_authority.php");
require_once('Cmx.php');
require_once('Cmx/Model/SystemConfig.php');

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 20, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

if (!is_array($issue_ids)) {
	echo("<script type=\"text/javascript\">alert('更新対象がありません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);
pg_query($con, "begin");

// 表示された端末認証履歴情報をいったん有効にする
$sql = "update client_auth set";
$set = array("disabled");
$setvalue = array("f");
$cond = "where issue_id in (" . implode(",", $issue_ids) .  ")";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 指定された端末認証履歴情報に無効にする
if (is_array($disabled_issue_ids)) {
	$sql = "update client_auth set";
	$set = array("disabled");
	$setvalue = array("t");
	$cond = "where issue_id in (" . implode(",", $disabled_issue_ids) .  ")";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

pg_query($con, "commit");
pg_close($con);

echo("<script type=\"text/javascript\">location.href = 'config_client_auth.php?session=$session&s_client_no=" . urlencode($s_client_no) . "&s_note=" . urlencode($s_note) . "&s_issuer_name=" . urlencode($s_issuer_name) . "&page=$page';</script>");
