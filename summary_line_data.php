<?
/*
画面パラメータ
	$session
		セッションID
	$mode
		モード regist:登録 update:更新
	$no
		番号 行の番号
*/

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_authority.php");
require("about_session.php");
require("about_postgres.php");
require("label_by_profile_type.ini");
require("./get_values.ini");
require_once("summary_write_tmpl_util.ini");
require_once("summary_tmpl_common.ini");

//ページ名
$fname = $PHP_SELF;

//==============================
//セッションのチェック
//==============================
$session = qualify_session($session,$fname);
if($session == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
// 権限チェック
//==============================
$summary = check_authority($session, 57, $fname);
if ($summary == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
//DBコネクション
//==============================
$con = connect2db($fname);
if($con == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
// 組織タイプを取得
//==============================
$profile_type = get_profile_type($con, $fname);


//==========================================================================================
//HTML出力
//==========================================================================================
?>
<title><?
// メドレポート/ＰＯレポート(Problem Oriented)
echo ($_label_by_profile["MED_REPORT"][$profile_type]); ?>｜入力</title>
<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
<script type="text/javascript">

function set_data(no) {

	if (window.opener && !window.opener.closed && window.opener.callback_set_data) {
		window.opener.callback_set_data(no, document.main_form.line_data.value);
	}
	window.close();
}

function delete_data(no) {

	if (window.opener && !window.opener.closed && window.opener.callback_delete_data) {
		window.opener.callback_delete_data(no);
	}
	window.close();
}

//定型文の設定
//
//引数
//  caller 呼び出し元識別名。１画面で複数呼び出しする場合はこの値で処理を分ける。
//	result.sentence 選択された定型文
function call_back_fixedform_select(caller, result) {

	var str = result.sentence.replace(/<br>/g, '\r\n');
	// 後ろへ連結する場合
	document.main_form.elements[caller].value += str;
	// 置き換えの場合
	//	document.main_form.elements[caller].value = str;
}

</script>
<?
//========================================
//FORMタグ外HTML出力
//========================================
tmpl_page_out_of_tmplform_html($session);
?>
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="<?
if ($mode == "update") {
//	echo("var str = window.opener.document.getElementById('{$item}').value;\n");
//	echo("var tmp_str = str.replace(/<br>/g, '\\r\\n');\n");
	echo("document.main_form.line_data.value = window.opener.document.getElementById('{$item}').value;\n");
}
?>document.main_form.line_data.focus();loadResizingTextArea();">
<?


//==============================
//ヘッダー 出力
//==============================
if ($mode == "regist") {
	$str = "登録";
} else {
	$str = "更新";
}

?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff">&nbsp;&nbsp;<b><?=$str?>画面</b></font></td>
<td>&nbsp</td>
<td width="10">&nbsp;</td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" width="10" height="10" alt=""><br>
<?


//==============================
//入力フィールド 出力
//==============================
?>
<form name="main_form" action="" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<center>
<table width="90%" border="0" cellspacing="2" cellpadding="0">
<tr>
<td align="left">&nbsp;
<? echo(" #".$no);?>
</td>
<td align="right">
<?
/**
 * 定型文選択画面用ボタンを出力します。
 * 
 * @param $caller 呼び出し元識別名。
 * @param $tmpl_param
 * @param $multi_flg 定型文画面のコピー時動作 "1":複数回コピー可でウィンドウは閉じない "":一回のみコピーでウィンドウを閉じる
 */
//sessionを$tmpl_paramに設定して渡す
$tmpl_form["session"] = $session;
write_fixedform_select_button("line_data", $tmpl_form, "");
echo("&nbsp;&nbsp;");
if ($mode == "update") {
?>
	<input type="button" value="削除" onClick="delete_data(<? echo($no);?>);">
<?
}
?>
<input type="button" value="<?=$str?>" onClick="set_data(<? echo($no);?>);">
</td></tr>
<tr><td colspan="2" align="center">

<textarea id="line_data" name="line_data" cols="80" rows="5" style="ime-mode: active;" class="ResizingTextArea"></textarea>

</td></tr>
</table>
</center>
</form>
<?


//==============================
//終了HTML 出力
//==============================
?>


</body>
</html>
<?

pg_close($con);
?>
