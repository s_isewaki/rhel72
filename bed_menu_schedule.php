<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 病床管理 | 入退院カレンダー | 概要</title>
<? require("about_session.php"); ?>
<? require("about_authority.php"); ?>
<? require("show_bed_schedule2.ini"); ?>
<? require("show_date_navigation_week.ini"); ?>
<?
// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 病床管理権限のチェック
$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 入退院予定表示用の日付が渡されていなければ求める
if ($bedschd_date == "") {
	$bedschd_date = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
}

// 診療科登録権限を取得
$section_admin_auth = check_authority($session, 28, $fname);

// 病棟登録権限を取得
$ward_admin_auth = check_authority($session, 21, $fname);

// データベースに接続
$con = connect2db($fname);

// オプション設定情報を取得
$sql = "select calendar_start1 from option";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$calendar_start = pg_fetch_result($sel, 0, "calendar_start1");

// 棟一覧を取得
$sql = "select bldg_cd, bldg_name from bldgmst";
$cond = "where bldg_del_flg = 'f' order by bldg_cd";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$buildings = array();
while ($row = pg_fetch_array($sel)) {
	$buildings[$row["bldg_cd"]] = array("name" => $row["bldg_name"], "wards" => array());
}

// 病棟一覧を取得
$sql = "select bldg_cd, ward_cd, ward_name from wdmst";
$cond = "where ward_del_flg = 'f' order by ward_cd";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
while ($row = pg_fetch_array($sel)) {
	$buildings[$row["bldg_cd"]]["wards"][$row["ward_cd"]] = $row["ward_name"];
}

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// 選択病棟を保存
if ($updopt == "1") {
	$sql = "delete from bedcalopt";
	$cond = "where emp_id = '$emp_id'";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	if ($calendar_bldg_cd == "") $calendar_bldg_cd = null;
	if ($calendar_ward_cd == "") $calendar_ward_cd = null;

	$sql = "insert into bedcalopt (emp_id, bldg_cd, ward_cd) values (";
	$content = array($emp_id, $calendar_bldg_cd, $calendar_ward_cd);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

// 選択病棟をロード
} else {
	$sql = "select bldg_cd, ward_cd from bedcalopt";
	$cond = "where emp_id = '$emp_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	if (pg_num_rows($sel) > 0) {
		$calendar_bldg_cd = pg_fetch_result($sel, 0, "bldg_cd");
		$calendar_ward_cd = pg_fetch_result($sel, 0, "ward_cd");
	}
}

// スタート曜日の設定
$start_wd = ($calendar_start == 1) ? 0 : 1;  // 0：日曜、1：月曜

// 当該週のスタート日を求める
$bedschd_start_day = get_start_day($bedschd_date, $start_wd);

// 「前月」「前日」「翌日」「翌月」のタイムスタンプを取得
$bedschd_last_month = get_last_month($bedschd_date);
$bedschd_last_week = get_last_week($bedschd_date);
$bedschd_next_week = get_next_week($bedschd_date);
$bedschd_next_month = get_next_month($bedschd_date);
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/prototype/prototype-1.7.1.js"></script>
<script type="text/javascript" src="js/yui/3.10.3/build/yui/yui-min.js"></script>
<script type="text/javascript">
function initPage() {
	prepareDragDrop();
}

function changeBuilding(bldg_cd) {
	location.href = 'bed_menu_schedule.php?session=<? echo($session); ?>&calendar_bldg_cd=' + bldg_cd + '&bedschd_date=<? echo($bedschd_date); ?>&updopt=1';
}

function changeWard(ward_cd) {
	location.href = 'bed_menu_schedule.php?session=<? echo($session); ?>&calendar_bldg_cd=<? echo($calendar_bldg_cd); ?>&calendar_ward_cd=' + ward_cd + '&bedschd_date=<? echo($bedschd_date); ?>&updopt=1';
}

function changeDate(dt) {
	location.href = 'bed_menu_schedule.php?session=<? echo($session); ?>&calendar_bldg_cd=<? echo($calendar_bldg_cd); ?>&calendar_ward_cd=<? echo($calendar_ward_cd); ?>&bedschd_date=' + dt;
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.submenu {border-collapse:collapse;}
table.submenu td {border:#5279a5 solid 1px;}
table.block {border-collapse:collapse;}
table.block td {border:#5279a5 solid 1px;}
table.block td td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="bed_menu.php?session=<? echo($session); ?>"><img src="img/icon/b17.gif" width="32" height="32" border="0" alt="病床管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="bed_menu.php?session=<? echo($session); ?>"><b>病床管理</b></a></font></td>
<? if ($section_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="entity_menu.php?session=<? echo($session); ?>&entity=1"><b>管理画面へ</b></a></font></td>
<? } else if ($ward_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="building_list.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="115" align="center" bgcolor="#5279a5"><a href="bed_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>病床管理トップ</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="ward_reference.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病棟照会</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="inpatient_reserve_register_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入退院登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="bedmake_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ベットメイク</font></a></td>
<td width="5">&nbsp;</td>
<td width="125" align="center" bgcolor="#bdd1e7"><a href="bed_rate_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床利用率管理</font></a>
</td>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#bdd1e7"><a href="bed_date_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">在院日数管理</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="bed_patient_export.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者データ抽出</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<!-- メニュー -->
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="submenu">
<tr height="22">
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu.php?session=<? echo($session); ?>">お知らせ</a></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_status.php?session=<? echo($session); ?>">病棟状況一覧表</a></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入退院カレンダー</font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_inpatient_search.php?session=<? echo($session); ?>">入院患者検索</a></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_vacant_search.php?session=<? echo($session); ?>">空床検索</a></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_occupation.php?session=<? echo($session); ?>">病床利用状況</a></font></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<!-- 入退院カレンダー -->
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="block">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr height="22" bgcolor="#bdd1e7">
<td width="45"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>概要</b></font></td>
<td width="90"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_schedule_in.php?session=<? echo($session); ?>">入院予定表</a></font></td>
<td width="90"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_schedule_out.php?session=<? echo($session); ?>">退院予定表</a></font></td>
<td width="150"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_schedule_report.php?session=<? echo($session); ?>">病棟別空床状況報告</a></font></td>
<td width="110"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_schedule_move.php?session=<? echo($session); ?>">院内移動状況</a></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_schedule_change.php?session=<? echo($session); ?>">転院者一覧</a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr height="22" bgcolor="#bdd1e7">
<td nowrap style="padding-right:15px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
事業所（棟）：<select name="calendar_bldg_cd" onchange="changeBuilding(this.options[this.selectedIndex].value);">
<option value="">すべて
<?
foreach ($buildings as $tmp_bldg_cd => $tmp_building) {
	echo("<option value=\"$tmp_bldg_cd\"");
	if ($tmp_bldg_cd == $calendar_bldg_cd) {echo(" selected");}
	echo(">{$tmp_building["name"]}");
}
?>
</select>
病棟：<select name="calendar_ward_cd" onchange="changeWard(this.options[this.selectedIndex].value);">
<option value="">すべて
<?
if ($calendar_bldg_cd != "") {
	foreach ($buildings[$calendar_bldg_cd]["wards"] as $tmp_ward_cd => $tmp_ward_name) {
		echo("<option value=\"$tmp_ward_cd\"");
		if ($tmp_ward_cd == $calendar_ward_cd) {echo(" selected");}
		echo(">$tmp_ward_name");
	}
}
?>
</select>
</font></td>
<td nowrap align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_schedule.php?session=<? echo($session); ?>&calendar_bldg_cd=<? echo($calendar_bldg_cd); ?>&calendar_ward_cd=<? echo($calendar_ward_cd); ?>&bedschd_date=<? echo($bedschd_last_month); ?>">&lt;&lt;前月</a>&nbsp;<a href="bed_menu_schedule.php?session=<? echo($session); ?>&calendar_bldg_cd=<? echo($calendar_bldg_cd); ?>&calendar_ward_cd=<? echo($calendar_ward_cd); ?>&bedschd_date=<? echo($bedschd_last_week); ?>">&lt;前週</a>&nbsp;<select onchange="changeDate(this.options[this.selectedIndex].value);"><? show_date_options_w($bedschd_date, $bedschd_start_day); ?></select>&nbsp;<a href="bed_menu_schedule.php?session=<? echo($session); ?>&calendar_bldg_cd=<? echo($calendar_bldg_cd); ?>&calendar_ward_cd=<? echo($calendar_ward_cd); ?>&bedschd_date=<? echo($bedschd_next_week); ?>">翌週&gt;</a>&nbsp;<a href="bed_menu_schedule.php?session=<? echo($session); ?>&calendar_bldg_cd=<? echo($calendar_bldg_cd); ?>&calendar_ward_cd=<? echo($calendar_ward_cd); ?>&bedschd_date=<? echo($bedschd_next_month); ?>">翌月&gt;&gt;</a></font></td>
</tr>
</table>
</td>
</tr>
</table>
<? show_bed_schedule($con, $bedschd_start_day, $session, $fname, $calendar_bldg_cd, $calendar_ward_cd); ?>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
