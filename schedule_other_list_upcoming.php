<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix スケジュール | 公開スケジュール（予定今後）</title>
<?
$fname=$PHP_SELF;
require("conf/sql.inf");						//sqlの一覧ファイルを読み込む
require_once("about_comedix.php");			//db処理のためのファイルを読み込む
require("show_select_values.ini");
require("get_values.ini");
require("time_check2.ini");
require("show_schedule_to_do2.ini");
require("project_check2.ini");
require("show_schedule_common.ini");
require("show_other_schedule_chart.ini");

$session = qualify_session($session,$fname);
if($session == "0"){
echo("<script type='text/javascript' src='js/showpage.js'></script>");
echo("<script language='javascript'>showLoginPage(window);</script>");
exit;
}
$empif = check_authority($session,2,$fname);
if($empif == "0"){
echo("<script type='text/javascript' src='js/showpage.js'></script>");
echo("<script language='javascript'>showLoginPage(window);</script>");
exit;
}

// タスク権限を取得
$task = check_authority($session, 30, $fname);
$task_flg = ($task != "0");

if(!isset($_REQUEST['date'])){
$date1 = mktime(0,0,0,date('m'), date('d'), date('Y'));
}else{
$date1 = $_REQUEST['date'];
}

$day = date('d', $date1);
$month = date('m', $date1);
$year = date('Y', $date1);
$todayday = date('d');
$todaymonth = date('m');
$todayyear = date('Y');

$con = connect2db($fname);

//--0219追加ここから---
$cond = " where session_id='$session'";
$sel = select_from_table($con,$SQL1,$cond,$fname);

if($sel == 0){
pg_close($con);
echo("<script type='text/javascript' src='js/showpage.js'></script>");
echo("<script language='javascript'>showErrorPage(window);</script>");
exit;
}

$e_id = pg_result($sel,0,"emp_id");

if($timegd == "on" || $timegd == "off"){
time_update($con,$e_id,$timegd,$fname);
}

if($timegd == ""){
$timegd = time_check($con,$e_id,$fname);
}
//--0219追加ここまで---
//--0511追加ここから-----
if($pjtgd == "on" || $pjtgd == "off"){
project_update($con,$e_id,$pjtgd,$fname);
}

if($pjtgd == ""){
$pjtgd = project_check($con,$emp_id,$e_id,$fname);
}

//--0511追加ここまで-----

echo("<script language=\"javascript\">\n");
echo("function showGuide(){\n");
echo("var ses = \"".$session."\";\n");
$dt = mktime(0,0,0,$month,$day,$year);
echo("var emp_id = \"".$emp_id."\";\n");
echo("var dt  = ".$dt.";\n");
echo("if(document.timegd.timeguide.checked == true){\n");
echo("var url = \"schedule_other_list_upcoming.php?session=\"+ses+\"&emp_id=\"+emp_id+\"&date=\"+dt+\"&timegd=on\";\n");
echo("location.href=url;\n");
echo("}else{\n");
echo("var url = \"schedule_other_list_upcoming.php?session=\"+ses+\"&emp_id=\"+emp_id+\"&date=\"+dt+\"&timegd=off\";\n");
echo("location.href=url;\n");
echo("}\n");
echo("}\n");

echo("function showProject(){\n");
echo("var ses = \"".$session."\";\n");
echo("var emp_id = \"".$emp_id."\";");
$dt = mktime(0,0,0,$month,$day,$year);
echo("var dt  = ".$dt.";\n");
echo("if(document.timegd.pjtguide.checked == true){\n");
echo("var url = \"schedule_other_list_upcoming.php?session=\"+ses+\"&emp_id=\"+emp_id+\"&date=\"+dt+\"&pjtgd=on\";\n");
echo("location.href=url;\n");
echo("}else{\n");
echo("var url = \"schedule_other_list_upcoming.php?session=\"+ses+\"&emp_id=\"+emp_id+\"&date=\"+dt+\"&pjtgd=off\";\n");
echo("location.href=url;\n");
echo("}\n");
echo("}\n");

echo("</script>\n");

// スケジュール更新可能フラグの設定
if ($e_id == $emp_id) {
	$schd_updatable = true;
} else {
	$sql = "select count(*) from mygroup";
	$cond = "where owner_id = '$e_id' and member_id = '$emp_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$schd_updatable = (pg_fetch_result($sel, 0, 0) == 1);
}

// オプション設定情報を取得
$sql = "select calendar_start1 from option";
$cond = "where emp_id = '$e_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$calendar_start = pg_fetch_result($sel, 0, "calendar_start1");
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>公開スケジュール（予定今後）</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<form name="timegd">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="80" height="22" align="center" bgcolor="#bdd1e7"><a href="schedule_other_menu.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo(mktime(0,0,0,$month,$day,$year)); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日</font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_other_week_menu.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo(mktime(0,0,0,$month,$day,$year)); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">週</font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_other_month_menu.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo(mktime(0,0,0,$month,$day,$year)); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">月</font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_other_year_menu.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo($year); ?>&s_date=<? echo(mktime(0,0,0,$month,$day,$year)); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">年</font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#5279a5"><a href="schedule_other_list_upcoming.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo(mktime(0,0,0,$month,$day,$year)); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>一覧</b></font></a></td>
<? if ($task_flg) { ?>
<td width="5"></td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_other_task.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo(mktime(0,0,0,$month,$day,$year)); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">公開タスク</font></a></td>
<? } ?>
<td width="43"></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b><? echo(get_emp_kanji_name($con,$emp_id,$fname)); ?></b></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="80" height="22" align="center"><a href="schedule_other_list_all.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo(mktime(0,0,0,$month,$day,$year)); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">予定一覧</font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>予定今後</b></font></td>
<td width="5"></td>
<td width="80" align="center"><a href="schedule_other_list_past.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo(mktime(0,0,0,$month,$day,$year)); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">予定過去</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="1" cellspacing="0" cellpadding="0">
<?
// スケジュール一覧を取得
$today = date("Y-m-d");
$sql = "select s.schd_id, s.schd_title, s.schd_type, s.schd_start_time, s.schd_dur, s.schd_start_date, s.schd_imprt, s.schd_status, s.schd_start_time_v, s.schd_dur_v from schdmst s ";
$sql .= get_other_schedule_sql_base($emp_id, $e_id) . " and s.schd_start_date >= '$today' ";
$sql .= "union select s.schd_id, s.schd_title, s.schd_type, null, null, s.schd_start_date, s.schd_imprt, s.schd_status, null, null from schdmst2 s ";
$sql .= get_other_schedule_sql_base($emp_id, $e_id) . " and s.schd_start_date >= '$today' ";
$sql .= "order by schd_start_date, schd_start_time_v";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// スケジュール一覧を表示
while ($row = pg_fetch_array($sel)) {
	$schd_id = $row["schd_id"];
	$schd_title = $row["schd_title"];
	$schd_type = $row["schd_type"];
	$schd_start_date = $row["schd_start_date"];
	$schd_start_time = substr($row["schd_start_time_v"], 0, 2).":".substr($row["schd_start_time_v"], 2, 2);
	$schd_dur = substr($row["schd_dur_v"], 0, 2).":".substr($row["schd_dur_v"], 2, 2);
	$schd_status = $row["schd_status"];
	$color = get_schedule_color($schd_type);
	$schd_imprt = $row["schd_imprt"];

	$tmp_date = mktime(0, 0, 0, substr($schd_start_date, 5, 2), substr($schd_start_date, 8, 2), substr($schd_start_date, 0, 4));
	if ($schd_start_time != ":" && $schd_dur != ":") {
		$timeless = "";
		$formatted_time = substr($schd_start_time, 0, 5) . "-" . substr($schd_dur, 0, 5);
	} else {
		$timeless = "t";
		$formatted_time = "指定なし";
	}
	$status = ($schd_status == "2") ? "[未承認]" : "";
	$detail_url = ($schd_updatable) ? "schedule_update.php" : "schedule_other_detail.php";

	echo("<tr height=\"22\" bgcolor=\"$color\">\n");
	echo("<td width=\"100\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"schedule_other_menu.php?session=$session&emp_id=$emp_id&date=$tmp_date\">$schd_start_date</a></font></td>\n");
	echo("<td width=\"100\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$formatted_time</font></td>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
	if ($schd_imprt == "3") {
		echo("予定あり");
	} else {
		echo("<a href=\"javascript:void(0);\" onclick=\"window.open('$detail_url?session=$session&date=$tmp_date&time=$schd_start_time&schd_id=$schd_id&timeless=$timeless', 'schddetail', 'width=640,height=560,scrollbars=yes');\">" . h("$schd_title$status") . "</a>");
	}
	echo("</font></td>\n");
	echo("<td width=\"50\" align=\"center\">");
	if ($schd_updatable) {
		$ret_url = urlencode("schedule_other_list_upcoming.php?session=$session&emp_id=$emp_id&date=$tmp_date");
		echo("<input type=\"button\" value=\"削除\" onclick=\"location.href = 'schedule_delete_checker.php?session=$session&schd_id=$schd_id&ret_url=$ret_url&timeless=$timeless&rptopt=1';\">");
	} else {
		echo("<input type=\"button\" value=\"削除\" disabled>");
	}
	echo("</td>\n");
	echo("</tr>\n");
}
?>
</table>
</form>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#bdd1e7"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#bdd1e7"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width=""><? show_schd_type_color_list($con, $fname); ?></td>
<td width="1" bgcolor="#bdd1e7"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#bdd1e7"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
</center>
</body>
<? pg_close($con); ?>
</html>
