<?
//======================================================================================================================================================
// 入力エラー時の送信フォーム
//======================================================================================================================================================
?>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" method="post" action="hiyari_el_update.php">
<input type="hidden" name="path" value="<? echo($path); ?>">
<input type="hidden" name="archive" value="<? echo($archive); ?>">
<input type="hidden" name="category" value="<? echo($category); ?>">
<input type="hidden" name="folder_id" value="<? echo($folder_id); ?>">
<input type="hidden" name="manage_history" value="<? echo($manage_history); ?>">
<input type="hidden" name="document_name" value="<? echo($document_name); ?>">
<input type="hidden" name="document_type" value="<? echo($document_type); ?>">
<input type="hidden" name="keywd" value="<? echo($keywd); ?>">
<input type="hidden" name="lib_no" value="<? echo($lib_no); ?>">
<input type="hidden" name="explain" value="<? echo($explain); ?>">
<input type="hidden" name="ref_dept_st_flg" value="<? echo($ref_dept_st_flg); ?>">
<input type="hidden" name="ref_dept_flg" value="<? echo($ref_dept_flg); ?>">
<input type="hidden" name="ref_class_src" value="<? echo($ref_class_src); ?>">
<input type="hidden" name="ref_atrb_src" value="<? echo($ref_atrb_src); ?>">
<?
if (is_array($hid_ref_dept))
{
	foreach ($hid_ref_dept as $tmp_dept_id)
	{
		echo("<input type=\"hidden\" name=\"ref_dept[]\" value=\"$tmp_dept_id\">\n");
	}
}
else
{
	$hid_ref_dept = array();
}
?>
<input type="hidden" name="ref_st_flg" value="<? echo($ref_st_flg); ?>">
<?
if (is_array($ref_st))
{
	foreach ($ref_st as $tmp_st_id)
	{
		echo("<input type=\"hidden\" name=\"ref_st[]\" value=\"$tmp_st_id\">\n");
	}
}
else
{
	$ref_st = array();
}
?>
<input type="hidden" name="target_id_list1" value="<? echo($target_id_list1); ?>">
<input type="hidden" name="target_id_list2" value="<? echo($target_id_list2); ?>">
<?
if ($target_id_list1 != "")
{
	$hid_ref_emp = split(",", $target_id_list1);
}
else
{
	$hid_ref_emp = array();
}
?>
<input type="hidden" name="upd_dept_st_flg" value="<? echo($upd_dept_st_flg); ?>">
<input type="hidden" name="upd_dept_flg" value="<? echo($upd_dept_flg); ?>">
<input type="hidden" name="upd_class_src" value="<? echo($upd_class_src); ?>">
<input type="hidden" name="upd_atrb_src" value="<? echo($upd_atrb_src); ?>">
<?
if (is_array($hid_upd_dept))
{
	foreach ($hid_upd_dept as $tmp_dept_id)
	{
		echo("<input type=\"hidden\" name=\"upd_dept[]\" value=\"$tmp_dept_id\">\n");
	}
}
else
{
	$hid_upd_dept = array();
}
?>
<input type="hidden" name="upd_st_flg" value="<? echo($upd_st_flg); ?>">
<?
if (is_array($upd_st))
{
	foreach ($upd_st as $tmp_st_id)
	{
		echo("<input type=\"hidden\" name=\"upd_st[]\" value=\"$tmp_st_id\">\n");
	}
}
else
{
	$upd_st = array();
}
?>
<?
if ($target_id_list2 != "")
{
	$hid_upd_emp = split(",", $target_id_list2);
}
else
{
	$hid_upd_emp = array();
}
?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="lib_id" value="<? echo($lib_id); ?>">
<input type="hidden" name="a" value="<? echo($a); ?>">
<input type="hidden" name="c" value="<? echo($c); ?>">
<input type="hidden" name="f" value="<? echo($f); ?>">
<input type="hidden" name="o" value="<? echo($o); ?>">
<input type="hidden" name="ref_toggle_mode" value="<? echo($ref_toggle_mode); ?>">
<input type="hidden" name="upd_toggle_mode" value="<? echo($upd_toggle_mode); ?>">
<input type="hidden" name="back" value="t">
</form>
<?


//======================================================================================================================================================
// 初期処理
//======================================================================================================================================================
require_once("about_session.php");
require_once("about_authority.php");
require_once("hiyari_el_common.ini");
require_once("hiyari_common.ini");
require_once("Cmx.php");
require_once("aclg_set.php");

//医療安全(保存先書庫)
$archive = 2;


//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$auth_id = ($path != "3") ? 47 : 48;
if (check_authority($session, $auth_id, $fname) == "0")
{
	showLoginPage();
	exit;
}

// データベースに接続
$con = connect2db($fname);
if($con == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);


//======================================================================================================================================================
// ファイルアップロードチェック
//======================================================================================================================================================
define(UPLOAD_ERR_OK, 0);
define(UPLOAD_ERR_INI_SIZE, 1);
define(UPLOAD_ERR_FORM_SIZE, 2);
define(UPLOAD_ERR_PARTIAL, 3);
define(UPLOAD_ERR_NO_FILE, 4);
$filename = $_FILES["upfile"]["name"];
if ($filename != "")
{
	switch ($_FILES["upfile"]["error"])
	{
		case UPLOAD_ERR_OK:
			break;
		case UPLOAD_ERR_INI_SIZE:
		case UPLOAD_ERR_FORM_SIZE:
			echo("<script type=\"text/javascript\">alert('ファイルサイズが大きすぎます。');</script>");
			echo("<script type=\"text/javascript\">document.items.submit();</script>");
			exit;
			break;
		case UPLOAD_ERR_PARTIAL:
		case UPLOAD_ERR_NO_FILE:
			echo("<script type=\"text/javascript\">alert('アップロードに失敗しました。再度実行してください。');</script>");
			echo("<script type=\"text/javascript\">document.items.submit();</script>");
			exit;
			break;
	}
}

//======================================================================================================================================================
// 入力チェック
//======================================================================================================================================================

//if ($archive == "")
//{
//	echo("<script type=\"text/javascript\">alert('書庫を選択してください。');</script>");
//	echo("<script type=\"text/javascript\">document.items.submit();</script>");
//	exit;
//}
if ($category == "" || $category == "0")
{
	echo("<script type=\"text/javascript\">alert('カテゴリを選択してください。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if ($document_name == "")
{
	echo("<script type=\"text/javascript\">alert('コンテンツ名が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if (strlen($document_name) > 100)
{
	echo("<script type=\"text/javascript\">alert('コンテンツ名が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if ($keywd == "")
{
	echo("<script type=\"text/javascript\">alert('キーワードが入力されていません。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if (strlen($keywd) > 100)
{
	echo("<script type=\"text/javascript\">alert('キーワードが長すぎます。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if (strlen($lib_no) > 50)
{
	echo("<script type=\"text/javascript\">alert('コンテンツ番号が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if (strlen($explain) > 200)
{
	echo("<script type=\"text/javascript\">alert('説明が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}

if ((($ref_dept_flg != "1" && count($hid_ref_dept) == 0) || ($ref_st_flg != "1" && count($ref_st) == 0)) && count($hid_ref_emp) == 0)
{
	echo("<script type=\"text/javascript\">alert('参照可能範囲が不正です。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if ((($upd_dept_flg != "1" && count($hid_upd_dept) == 0) || ($upd_st_flg != "1" && count($upd_st) == 0)) && count($hid_upd_emp) == 0)
{
	echo("<script type=\"text/javascript\">alert('更新可能範囲が不正です。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}

//======================================================================================================================================================
// 画面パラメータ精査
//======================================================================================================================================================

if ($filename != "")
{
	$extension = (strpos($filename, ".")) ? preg_replace("/^.*\.([^.]*)$/", "$1", $filename) : "txt";
}
if ($folder_id == "") {$folder_id = null;}
if ($ref_dept_st_flg != "t") {$ref_dept_st_flg = "f";}
if ($ref_dept_flg == "") {$ref_dept_flg = null;}
if ($ref_st_flg == "") {$ref_st_flg = null;}
if ($upd_dept_st_flg != "t") {$upd_dept_st_flg = "f";}
if ($upd_dept_flg == "") {$upd_dept_flg = null;}
if ($upd_st_flg == "") {$upd_st_flg = null;}


//======================================================================================================================================================
// DB処理開始
//======================================================================================================================================================



// トランザクションの開始
pg_query($con, "begin");








//======================================================================================================================================================
// ログインユーザの職員IDを取得
//======================================================================================================================================================
$emp_id = get_emp_id($con, $session, $fname);





//==============================
// コンテンツ情報を取得
//==============================
$sql = "select lib_archive, lib_cate_id, folder_id, lib_extension from el_info";
$cond = "where lib_id = $lib_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0)
{
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$pre_archive = pg_fetch_result($sel, 0, "lib_archive");
$pre_category = pg_fetch_result($sel, 0, "lib_cate_id");
$pre_folder_id = pg_fetch_result($sel, 0, "folder_id");
$pre_extension = pg_fetch_result($sel, 0, "lib_extension");


//======================================================================================================================================================
// コンテンツ情報
//======================================================================================================================================================

// 履歴管理をしない場合
if ($manage_history != "t")
{
	//==============================
	// コンテンツ情報を更新
	//==============================
	$show_login_flg = "f";//不使用
	$show_login_begin = null;//不使用
	$show_login_end = null;//不使用
	$private_flg = null;//不使用
	$sql = "update el_info set";
	$set = array("lib_archive", "lib_cate_id", "lib_nm", "lib_keyword", "lib_summary", "show_login_flg", "show_login_begin", "show_login_end", "folder_id", "lib_type", "private_flg", "ref_dept_st_flg", "ref_dept_flg", "ref_st_flg", "upd_dept_st_flg", "upd_dept_flg", "upd_st_flg", "lib_up_date", "lib_no");
	$setvalue = array($archive, $category, pg_escape_string($document_name), pg_escape_string($keywd), pg_escape_string($explain), $show_login_flg, $show_login_begin, $show_login_end, $folder_id, $document_type, $private_flg, $ref_dept_st_flg, $ref_dept_flg, $ref_st_flg, $upd_dept_st_flg, $upd_dept_flg, $upd_st_flg, date("YmdHis"), pg_escape_string($lib_no));
	if ($filename != "")
	{
		$set[] = "lib_extension";
		$setvalue[] = $extension;
	}
	$cond = "where lib_id = $lib_id";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

}
// 履歴管理をする場合
else
{
	$org_lib_id = $lib_id;

	//==============================
	// コンテンツIDの採番
	//==============================

/*
	$sql = "select max(lib_id) from el_info";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$lib_id = intval(pg_fetch_result($sel, 0, 0)) + 1;
*/

	//シーケンスで自動採番に変更
	$sql = "select nextval('inci_el_info_lib_id_seq') as lib_id";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$lib_id = intval(pg_fetch_result($sel, 0, 0));
	
	
	//==============================
	// コンテンツ情報を登録
	//==============================
	$show_login_flg = "f";//不使用
	$show_login_begin = null;//不使用
	$show_login_end = null;//不使用
	$private_flg = null;//不使用
	$sql = "insert into el_info (lib_archive, lib_id, lib_cate_id, lib_nm, lib_extension, lib_keyword, lib_summary, emp_id, lib_up_date, lib_delete_flag, show_login_flg, show_login_begin, show_login_end, folder_id, lib_type, private_flg, ref_dept_st_flg, ref_dept_flg, ref_st_flg, upd_dept_st_flg, upd_dept_flg, upd_st_flg, lib_no) values (";
	$content = array($archive, $lib_id, $category, pg_escape_string($document_name), $extension, pg_escape_string($keywd), pg_escape_string($explain), $emp_id, date("YmdHis"), "f", $show_login_flg, $show_login_begin, $show_login_end, $folder_id, $document_type, $private_flg, $ref_dept_st_flg, $ref_dept_flg, $ref_st_flg, $upd_dept_st_flg, $upd_dept_flg, $upd_st_flg, pg_escape_string($lib_no));
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	//==============================
	// コンテンツ参照ログを削除（念のため）
	//==============================
	$sql = "delete from el_reflog";
	$cond = "where lib_id = $lib_id";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	//==============================
	// 現在の版情報を取得
	//==============================
	$sql = "select base_lib_id, edition_no from el_edition";
	$cond = "where lib_id = $org_lib_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$base_lib_id = pg_fetch_result($sel, 0, "base_lib_id");
	$edition_no = intval(pg_fetch_result($sel, 0, "edition_no"));

	//==============================
	// 版情報を登録
	//==============================
	$sql = "insert into el_edition values (";
	$content = array($base_lib_id, $edition_no + 1, $lib_id);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

//======================================================================================================================================================
// 権限情報
//======================================================================================================================================================
//(部署＋役職＋個人)×(参照＋更新)＝計６権限をデリートインサート


// 参照可能部署情報をDELETE-INSERT
$sql = "delete from el_ref_dept";
$cond = "where lib_id = $lib_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0)
{
	pg_query($con,"rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
foreach ($hid_ref_dept as $tmp_val)
{
	list($tmp_class_id, $tmp_atrb_id, $tmp_dept_id) = split("-", $tmp_val);
	$sql = "insert into el_ref_dept (lib_id, class_id, atrb_id, dept_id) values (";
	$content = array($lib_id, $tmp_class_id, $tmp_atrb_id, $tmp_dept_id);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0)
	{
		pg_query($con,"rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// 参照可能役職情報をDELETE-INSERT
$sql = "delete from el_ref_st";
$cond = "where lib_id = $lib_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0)
{
	pg_query($con,"rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
foreach ($ref_st as $tmp_st_id)
{
	$sql = "insert into el_ref_st (lib_id, st_id) values (";
	$content = array($lib_id, $tmp_st_id);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0)
	{
		pg_query($con,"rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// 参照可能職員情報をDELETE-INSERT
$sql = "delete from el_ref_emp";
$cond = "where lib_id = $lib_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0)
{
	pg_query($con,"rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
foreach ($hid_ref_emp as $tmp_emp_id)
{
	$sql = "insert into el_ref_emp (lib_id, emp_id) values (";
	$content = array($lib_id, $tmp_emp_id);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0)
	{
		pg_query($con,"rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// 更新可能部署情報をDELETE-INSERT
$sql = "delete from el_upd_dept";
$cond = "where lib_id = $lib_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0)
{
	pg_query($con,"rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
foreach ($hid_upd_dept as $tmp_val)
{
	list($tmp_class_id, $tmp_atrb_id, $tmp_dept_id) = split("-", $tmp_val);
	$sql = "insert into el_upd_dept (lib_id, class_id, atrb_id, dept_id) values (";
	$content = array($lib_id, $tmp_class_id, $tmp_atrb_id, $tmp_dept_id);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0)
	{
		pg_query($con,"rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// 更新可能役職情報をDELETE-INSERT
$sql = "delete from el_upd_st";
$cond = "where lib_id = $lib_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0)
{
	pg_query($con,"rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
foreach ($upd_st as $tmp_st_id)
{
	$sql = "insert into el_upd_st (lib_id, st_id) values (";
	$content = array($lib_id, $tmp_st_id);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0)
	{
		pg_query($con,"rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// 更新可能職員情報をDELETE-INSERT
$sql = "delete from el_upd_emp";
$cond = "where lib_id = $lib_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0)
{
	pg_query($con,"rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
foreach ($hid_upd_emp as $tmp_emp_id)
{
	$sql = "insert into el_upd_emp (lib_id, emp_id) values (";
	$content = array($lib_id, $tmp_emp_id);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0)
	{
		pg_query($con,"rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}


//==============================
// フォルダ更新日時を更新
//==============================

//保存先のフォルダが変更された場合
if ($folder_id != $pre_folder_id)
{
	//保存元の階層下のフォルダの権限更新
	lib_update_folder_modified($con, $pre_category, $pre_folder_id, $emp_id, $fname);
}

//保存先の階層下のフォルダの権限更新
lib_update_folder_modified($con, $category, $folder_id, $emp_id, $fname);


//======================================================================================================================================================
// DB処理終了
//======================================================================================================================================================


// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を切断
pg_close($con);















//======================================================================================================================================================
// 物理フォルダ/ファイル作成削除
//======================================================================================================================================================

// ファイル保存先ディレクトリがなければ作成
create_cate_directory($category);


// 履歴管理をしない場合
if ($manage_history != "t")
{
	//ファイルに変更がなく、保存先が変更となった場合
	if ($filename == "" && ($archive != $pre_archive || $category != $pre_category))
	{
		//ファイルを移動
		$pre_cate_dir = get_cate_directory_path($pre_category);
		$cate_dir     = get_cate_directory_path($category);
		exec("mv {$pre_cate_dir}/document{$lib_id}.* {$cate_dir}");
	}
	//ファイルに変更があった場合
	else if ($filename != "")
	{
		//旧ファイルを削除
		$pre_cate_dir = get_cate_directory_path($pre_category);
		exec("rm -f {$pre_cate_dir}/document{$lib_id}.*");
		
		//ファイルコピー(保存)
		$lib_path = get_lib_path($category, $lib_id, $extension);
		copy($upfile, $lib_path);
	}

}
// 履歴管理をする場合
else
{
	//ファイルコピー(保存)
	$lib_path = get_lib_path($category, $lib_id, $extension);
	copy($upfile, $lib_path);
}


//======================================================================================================================================================
// 処理終了通知・画面クローズ
//======================================================================================================================================================

//※ユーザー画面・管理画面共通
?>
<script type="text/javascript">
if(window.opener && !window.opener.closed && window.opener.reload_page)
{
	window.opener.reload_page();
	window.close();
}
</script>
</body>
