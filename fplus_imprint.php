<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ファントルくん＋ ｜ 印影登録</title>
<?
require_once("about_comedix.php");
require_once("show_select_values.ini");
require_once("get_values.ini");
require_once("fplus_imprint_common.ini");
require_once("./conf/sql.inf");

require_once("fplus_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック
//====================================
$checkauth=check_authority($session,78,$fname);
if($checkauth=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
// 管理者権限の取得
$fplusadm_auth = check_authority($session, 79, $fname);

$con = connect2db($fname);

// 職員ID取得
$emp_id=get_emp_id($con,$session,$fname);


?>

<?
// 印影機能フラグ取得
$db_imprint_flg = get_imprint_flg($con, $emp_id, $fname);

$delbtn_flg = "f";

// 画像登録
if ($regist_flg == "1") {
	// ファイルアップロードチェック
	define(UPLOAD_ERR_OK, 0);
	define(UPLOAD_ERR_INI_SIZE, 1);
	define(UPLOAD_ERR_FORM_SIZE, 2);
	define(UPLOAD_ERR_PARTIAL, 3);
	define(UPLOAD_ERR_NO_FILE, 4);

	$filename = $_FILES["imprint"]["name"];

	if ($filename != "") {
		$delbtn_flg = "t";
		// gif, jpgチェック
		$pos = strrpos($filename, ".");
		$ext = "";
		if ($pos > 0 ) {
			$ext = substr($filename, $pos+1, 3);
			$ext = strtolower($ext);
		}

		if ($pos === false || ($ext != "gif" && $ext != "jpg")) {
			echo("<script language=\"javascript\">alert('拡張子はgifかjpgのファイルを指定してください。');</script>");
			echo("<script language=\"javascript\">history.back();</script>");
			exit;
		}

		switch ($_FILES["imprint"]["error"]) {
		case UPLOAD_ERR_INI_SIZE:
		case UPLOAD_ERR_FORM_SIZE:
			echo("<script language=\"javascript\">alert('ファイルサイズが大きすぎます。');</script>");
			echo("<script language=\"javascript\">history.back();</script>");
			exit;
		case UPLOAD_ERR_PARTIAL:
		case UPLOAD_ERR_NO_FILE:
			echo("<script language=\"javascript\">alert('アップロードに失敗しました。再度実行してください。');</script>");
			echo("<script language=\"javascript\">history.back();</script>");
			exit;
		}

		// ファイル保存用ディレクトリがなければ作成
		if (!is_dir("fplus")) {
			mkdir("fplus", 0755);
		}
		if (!is_dir("fplus/workflow")) {
			mkdir("fplus/workflow", 0755);
		}
		if (!is_dir("fplus/workflow/imprint")) {
			mkdir("fplus/workflow/imprint", 0755);
		}


		// 画像がある場合は削除
		$filename1 = "fplus/workflow/imprint/$emp_id.gif";
		$filename2 = "fplus/workflow/imprint/$emp_id.jpg";

		if (is_file($filename1)) {
			unlink($filename1);
		}
		if (is_file($filename2)) {
			unlink($filename2);
		}
		// アップロードされたファイルを保存
		$savefilename = "fplus/workflow/imprint/{$emp_id}.{$ext}";
		$ret = copy($_FILES["imprint"]["tmp_name"], $savefilename);

		if ($ret == false) {
			echo("<script language=\"javascript\">alert('ファイルのコピーに失敗しました。再度実行してください。');</script>");
			echo("<script language=\"javascript\">history.back();</script>");
			exit;
		}
	}

	// 印影機能フラグ登録
	$insert_update_flg = "insert";
	if ($db_imprint_flg != "") {
		$insert_update_flg = "update";
	}
	regist_imprint_flg($con, $fname, $emp_id, $imprint_flg, $insert_update_flg);
} else if ($regist_flg == "2") {
	$delbtn_flg = "f";
	// 画像がある場合は削除
	$filename1 = "fplus/workflow/imprint/$emp_id.gif";
	$filename2 = "fplus/workflow/imprint/$emp_id.jpg";

	if (is_file($filename1)) {
		unlink($filename1);
	}
	if (is_file($filename2)) {
		unlink($filename2);
	}
	// 印影機能レコード削除
	regist_imprint_flg($con, $fname, $emp_id, "", "delete");
}

if ($imprint_flg == "") {
	if ($db_imprint_flg != "") {
		$imprint_flg = $db_imprint_flg;
	} else {
		$imprint_flg = "f";
	}
}


?>


<script type="text/javascript">
function submitForm(flg) {
	document.wkfw.regist_flg.value = flg;
	document.wkfw.submit();
}

/*
function referTemplate() {
	window.open('fplusadm_template_refer.php?session=<? echo($session); ?>', 'newwin', 'width=640,height=480,scrollbars=yes');
}
*/
</script>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#E6B3D4 solid 1px;}
p {margin:0;}

table.block3 {border-collapse:collapse;}
table.block3 td {border:#E6B3D4 solid 0px;}
table.block3 td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="fplus_menu.php?session=<? echo($session); ?>"><img src="img/icon/b47.gif" width="32" height="32" border="0" alt="ファントルくん＋"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="fplus_menu.php?session=<? echo($session); ?>"><b>ファントルくん＋</b></a> &gt; <a href="fplus_imprint.php?session=<? echo($session); ?>"><b>印影登録</b></a></font></td>
<? if ($fplusadm_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="fplusadm_menu.php?session=<? echo($session); ?>&workflow=1"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>

<?
show_applycation_menuitem($session,$fname,"");
?>

</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#E6B3D4"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form name="wkfw" action="fplus_imprint.php" method="post" enctype="multipart/form-data">
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="160" align="right" bgcolor="#FFFBFF"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">印影機能を利用する</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="imprint_flg" value="t"<? if ($imprint_flg == "t") {echo(" checked");} ?>>はい
<input type="radio" name="imprint_flg" value="f"<? if ($imprint_flg == "f") {echo(" checked");} ?>>いいえ
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#FFFBFF"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">印影画像指定(Jpeg,gif)</font></td>
<td>
<input type="file" name="imprint" size="60">

<table border="0" cellspacing="0" cellpadding="1" class="block3">
<tr>
<td>
プレビュー
</td>
<td>
<?
// 画像がある場合は表示

$filename = "";
$filename1 = "fplus/workflow/imprint/$emp_id.gif";
$filename2 = "fplus/workflow/imprint/$emp_id.jpg";

if (is_file($filename1)) {
	$filename = $filename1;
} else if (is_file($filename2)) {
	$filename = $filename2;
}

if ($filename == "") { ?>
<img src="img/spacer.gif" width="48" height="48" border="1">
<? } else {
	// 印影表示関数
	show_imprint_image($session, $emp_id, 1, "t", "t");
	$delbtn_flg = "t";
} ?>

</td>
</tr>
</table>

</td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr>
<td align="right">
<?
if ($delbtn_flg == "t") {
?>
<input type="button" name="del" value="削除" onclick="if(confirm('削除します。よろしいですか？')){submitForm('2');}">
<? } ?>
<input type="button" name="regist" value="登録" onclick="submitForm('1');">
</td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="regist_flg" value="">
</form>
</td>
</tr>
</table>

</body>
<? pg_close($con); ?>

</html>
