<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 環境設定｜端末認証履歴</title>
<?
require_once("about_session.php");
require_once("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 20, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

define("ROWS_PER_PAGE", 50);
if ($page == "") $page = 1;

// データベースに接続
$con = connect2db($fname);

// 端末認証履歴情報を取得
$sql = "select ca.issue_id, ca.disabled, ca.client_no, ca.note, e.emp_lt_nm, e.emp_ft_nm, c.class_nm, a.atrb_nm, d.dept_nm, r.room_nm, ca.issue_datetime, ca.auth_key from client_auth ca inner join empmst e on ca.issuer_emp_id = e.emp_id inner join classmst c on e.emp_class = c.class_id inner join atrbmst a on e.emp_attribute = a.atrb_id inner join deptmst d on e.emp_dept = d.dept_id left join classroom r on e.emp_room = r.room_id";
$conds = array();
if ($s_client_no != "") {
	$tmp_s_client_no = mb_ereg_replace('[%_]', '\\\\0', $s_client_no);
	$conds[] = "ca.client_no like '%" . pg_escape_string($tmp_s_client_no) . "%'";
}
if ($s_note != "") {
	$tmp_s_note = mb_ereg_replace('[%_]', '\\\\0', $s_note);
	$conds[] = "ca.note like '%" . pg_escape_string($tmp_s_note) . "%'";
}
if ($s_issuer_name != "") {
	$tmp_s_issuer_name = mb_ereg_replace('[%_]', '\\\\0', $s_issuer_name);
	$tmp_s_issuer_name = mb_ereg_replace("[ 　]", "", $tmp_s_issuer_name);
	$conds[] = "e.emp_lt_nm || e.emp_ft_nm like '%" . pg_escape_string($tmp_s_issuer_name) . "%'";
}
$cond = (count($conds) > 0) ? ("where " . implode(" and ", $conds)) : "";
$cond .= " order by ca.issue_id desc";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 大きすぎるページ番号が指定された場合、最大ページの表示とする
$issue_count = pg_num_rows($sel);
$max_page = ($issue_count) ? ceil($issue_count / ROWS_PER_PAGE) : 1;
if ($page > $max_page) {
	$page = $max_page;
}

// 表示対象のデータを配列に格納する
$rows = array();
$start_index = ($page - 1) * ROWS_PER_PAGE;
$end_index = min($start_index + ROWS_PER_PAGE - 1, $issue_count - 1);

// 当該ページに表示するデータの最後のインデックス
for ($i = $start_index; $i <= $end_index; $i++) {
	$rows[] = pg_fetch_array($sel, $i);
}
unset($sel);
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function disableIssues() {
	if (confirm('無効状態を更新します。よろしいですか？')) {
		document.mainform.submit();
	}
}

function openRegisterWindow() {
	window.open('config_client_auth_register.php?session=<? echo($session); ?>', 'cliauth', 'width=640,height=480,scrollbars=yes');
}

function selectRow(row) {
	row.style.cursor = 'pointer';
	row.style.backgroundColor = '#ffff66';
}

function unselectRow(row) {
	row.style.cursor = '';
	row.style.backgroundColor = '';
}

function openDetail(issueId) {
	window.open('config_client_auth_detail.php?session=<? echo($session); ?>&issue_id=' + issueId, 'cliauth', 'width=640,height=480,scrollbars=yes');
}

function selectIssue(e) {
	if (e.stopPropagation) {
		e.stopPropagation();
	} else {
		event.cancelBubble = true;
	}
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-style:none;}
.list td .inner {border-collapse:collapse;}
.list td .inner td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="config_register.php?session=<? echo($session); ?>"><img src="img/icon/b28.gif" width="32" height="32" border="0" alt="環境設定"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="config_register.php?session=<? echo($session); ?>"><b>環境設定</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="110" align="center" bgcolor="#bdd1e7"><a href="config_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ログイン画面</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="config_sidemenu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メニュー</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="config_siteid.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">サイトID</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="config_log.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ログ</font></a></td>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#bdd1e7"><a href="config_server_information.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">サーバ情報</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#5279a5"><a href="config_client_auth.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>端末認証履歴</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="config_other.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">その他</font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>

<img src="img/spacer.gif" alt="" width="1" height="4"><br>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">注）発行履歴が存在している場合でも、各端末でcookieをクリアーされてしまうと、認証できなくなります。</font></td>
</tr>
</table>

<img src="img/spacer.gif" alt="" width="1" height="2"><br>

<form action="config_client_auth.php" method="get">
<table border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td bgcolor="#f6f9ff" width="80"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">端末番号</font></td>
<td style="padding-right:10px;"><input type="text" name="s_client_no" value="<? echo($s_client_no); ?>"></td>
<td bgcolor="#f6f9ff" width="80"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">備考</font></td>
<td style="padding-right:10px;"><input type="text" name="s_note" value="<? echo($s_note); ?>"></td>
<td bgcolor="#f6f9ff" width="80"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発行者</font></td>
<td style="padding-right:10px;"><input type="text" name="s_issuer_name" value="<? echo($s_issuer_name); ?>"></td>
<td style="padding-left:4px;border-top-style:none;border-right-style:none;border-bottom-style:none;"><input type="submit" value="検索"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>

<img src="img/spacer.gif" alt="" width="1" height="4"><br>

<form name="mainform" action="config_client_auth_disable.php" method="post">
<table width="700" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td>
<input type="button" value="無効更新" onclick="disableIssues();">
<input type="button" value="認証キー発行" onclick="openRegisterWindow();">
</td>
<? if ($issue_count > 0) { ?>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
$encoded_s_client_no = urlencode($s_client_no);
$encoded_s_note = urlencode($s_note);
$encoded_s_issuer_name = urlencode($s_issuer_name);

echo("ページ");
if ($page > 1) {
	echo("<a href=\"config_client_auth.php?session=$session&s_client_no=$encoded_s_client_no&s_note=$encoded_s_note&s_issuer_name=$encoded_s_issuer_name&page=" . ($page - 1) . "\">←</a>");
}
for ($i = 1; $i <= $max_page; $i++) {
	echo(" ");
	if ($i == $page) {
		echo("[$i]");
	} else {
		echo("<a href=\"config_client_auth.php?session=$session&s_client_no=$encoded_s_client_no&s_note=$encoded_s_note&s_issuer_name=$encoded_s_issuer_name&page=$i\">$i</a>");
	}
}
if ($page < $max_page) {
	echo(" <a href=\"config_client_auth.php?session=$session&s_client_no=$encoded_s_client_no&s_note=$encoded_s_note&s_issuer_name=$encoded_s_issuer_name&page=" . ($page + 1) . "\">→</a>");
}
?>
</font></td>
<? } ?>
</tr>
</table>

<img src="img/spacer.gif" alt="" width="1" height="4"><br>

<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff">
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">無効</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発行No</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">端末番号</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">備考</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発行者</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">部署</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発行日時</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">認証キー</font></td>
</tr>
<?
foreach ($rows as $row) {
	$tmp_issue_id = $row["issue_id"];
?>
<tr height="22" id="issue<? echo($tmp_issue_id); ?>" onmouseover="selectRow(this);" onmouseout="unselectRow(this);" onclick="openDetail('<? echo($tmp_issue_id); ?>');">
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="disabled_issue_ids[]" value="<? echo($tmp_issue_id); ?>"<? if ($row["disabled"] == "t") {echo(" checked");} ?> onclick="selectIssue(event);"></font><input type="hidden" name="issue_ids[]" value="<? echo($tmp_issue_id); ?>"></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_issue_id); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($row["client_no"]); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($row["note"]); ?></font></td>
<td nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($row["emp_lt_nm"] . " " . $row["emp_ft_nm"]); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(format_issuer_dept($row["class_nm"], $row["atrb_nm"], $row["dept_nm"], $row["room_nm"])); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(format_issue_datetime($row["issue_datetime"])); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($row["auth_key"]); ?></font></td>
</tr>
<?
}
?>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="s_client_no" value="<? echo($s_client_no); ?>">
<input type="hidden" name="s_note" value="<? echo($s_note); ?>">
<input type="hidden" name="s_issuer_name" value="<? echo($s_issuer_name); ?>">
<input type="hidden" name="page" value="<? echo($page); ?>">
</form>

</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
function format_issuer_dept($class_nm, $atrb_nm, $dept_nm, $room_nm) {
	$issuer_dept = $class_nm . "＞" . $atrb_nm . "＞" . $dept_nm;
	if (strlen($room_nm) > 0) $issuer_dept .= "＞" . $room_nm;
	return $issuer_dept;
}

function format_issue_datetime($datetime) {
	return preg_replace("/\A(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})\z/", "$1/$2/$3 $4:$5", $datetime);
}
