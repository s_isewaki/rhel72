<!-- ************************************************************************ -->
<!-- 出勤表｜勤務シフト希望「登録」 -->
<!-- ************************************************************************ -->

<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");

require_once("duty_shift_common_class.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
// セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
///-----------------------------------------------------------------------------
// トランザクションを開始
///-----------------------------------------------------------------------------
pg_query($con, "begin transaction");

///-----------------------------------------------------------------------------
//日編集
///-----------------------------------------------------------------------------
$start_date = $duty_start_date;
$end_date = $duty_end_date;
$calendar_array = $obj->get_calendar_array($start_date, $end_date);

for ($i=0; $i<count($calendar_array); $i++) {
	$duty_date[$i + 1] = $calendar_array[$i]["date"];
}
///-----------------------------------------------------------------------------
// 選択された勤務パターングループの勤務シフト情報を登録
///-----------------------------------------------------------------------------
for($i=0;$i<$data_cnt;$i++) {
	//応援情報
	$assist_str = "assist_group_$i";
	$arr_assist_group = split(",", $$assist_str);
	///-----------------------------------------------------------------------------
	//項目の配列編集
	///-----------------------------------------------------------------------------
	$emp_id = $staff_id[$i];
	///-----------------------------------------------------------------------------
	//指定職員ＩＤの下書きデータを削除
	///-----------------------------------------------------------------------------
	if ($staff_id[$i] == $up_staff_id) {
		$sql = "delete from duty_shift_plan_individual";
		$cond = "where emp_id = '$emp_id' ";
		$cond .= "and duty_date >= '$start_date' and duty_date <= '$end_date' ";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
	///-----------------------------------------------------------------------------
	//登録
	///-----------------------------------------------------------------------------
	for($k=1;$k<=$day_cnt;$k++) {
		///-----------------------------------------------------------------------------
		//項目の配列編集
		///-----------------------------------------------------------------------------
		$g_id = "group_id_$i" . "_" . $k;
		$ptn_id = "pattern_id_$i" . "_" . $k;
		$wk_date = $duty_date[$k];

		$atdptn_ptn_id = "";
		$reason = "";
		$wk = "atdptn_ptn_id_$i" . "_" . $k;
		if ($$wk != "") {
			if ((int)substr($$wk,0,2) > 0) {
				$atdptn_ptn_id = (int)substr($$wk,0,2);
			}
			if ((int)substr($$wk,2,2) > 0) {
				$reason = (int)substr($$wk,2,2);
			}
		}

		$reason_2 = "reason_2_$i" . "_" . $k;
		if ($$reason_2 != "") {
			$reason = $$reason_2;
		}
		///-----------------------------------------------------------------------------
		//データが存在する場合のみ登録
		///-----------------------------------------------------------------------------
		if (($group_id != "") &&
			($$ptn_id != "") &&
			($atdptn_ptn_id != "") &&
			($duty_date[$k] != "") &&
			($staff_id[$i] == $up_staff_id)) {
			///-----------------------------------------------------------------------------
			//ＳＱＬ
			///-----------------------------------------------------------------------------
			$sql = "insert into duty_shift_plan_individual (group_id, emp_id, duty_date, pattern_id, atdptn_ptn_id, reason";
			$sql .= ") values (";
			//グループID（応援追加対応）
			$wk_group_id = $arr_assist_group[$k-1];
			$content = array($wk_group_id, $emp_id, $wk_date, $$ptn_id, $atdptn_ptn_id, $reason);
			$ins = insert_into_table($con, $sql, $content, $fname);
			if ($ins == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}
}
///-----------------------------------------------------------------------------
// トランザクションをコミット
///-----------------------------------------------------------------------------
pg_query("commit");
///-----------------------------------------------------------------------------
// データベース接続を閉じる
///-----------------------------------------------------------------------------
pg_close($con);
///-----------------------------------------------------------------------------
// 遷移元画面を再表示
///-----------------------------------------------------------------------------
$wk = "atdbk_duty_shift.php?session=$session&group_id=$group_id&duty_yyyy=$duty_yyyy&duty_mm=$duty_mm&plan_results_flg=$plan_results_flg";
$wk .= "&cause_group_id=$group_id";
$wk .= "&cause_duty_yyyy=$duty_yyyy";
$wk .= "&cause_duty_mm=$duty_mm";
$wk .= "&stamp_flg=2"; //スタンプ非表示状態とする 20140204
echo("<script type=\"text/javascript\">location.href = '$wk';</script>");
?>

</HTML>

