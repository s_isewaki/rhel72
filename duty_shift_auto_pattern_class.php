<?php
//************************************************************************
// 勤務シフト作成　勤務シフト作成画面
// 自動シフト組み合わせパターンＣＬＡＳＳ
//************************************************************************
require_once("about_postgres.php");

class duty_shift_auto_pattern_class
{
	/*************************************************************************/
	// パターンテーブルの取得
	/*************************************************************************/
	function _create_pattern_table($group_id, $table_name)
	{
		//SQL実行
		$sql = "SELECT * FROM $table_name";
		$cond = "WHERE group_id='$group_id' ORDER BY no";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			echo("<!-- FROM $table_name -->");
			exit;
		}
		
		//fetch
		$next_array = array();
		$prev_array = array();
		while ($row = pg_fetch_assoc($sel)){
			$prev = sprintf("%d:%d", $row["today_atdptn_ptn_id"], $row["today_reason"]);
			$next = sprintf("%d:%d", $row["nextday_atdptn_ptn_id"], $row["nextday_reason"]);
			$next_array[ $prev ][] = $next;
			$prev_array[ $next ][] = $prev;
		}
		
		$this->_next_table = $next_array;
		$this->_prev_table = $prev_array;
	}
	
	/*************************************************************************/
	// 前日チェック
	/*************************************************************************/
	function is_prev_day($pattern, $reason, $prev_pattern, $prev_reason)
	{
		$next = sprintf("%d:%d", $pattern, $reason);
		$prev = sprintf("%d:%d", $prev_pattern, $prev_reason);
		if ( empty($this->_prev_table[$next]) ) {
			return false;
		}
		foreach ( $this->_prev_table[$next] as $check ) {
			if ( $check == $prev ) {
				return true;
			}
		}
		return false;
	}
	
	/*************************************************************************/
	// 翌日チェック
	/*************************************************************************/
	function is_next_day($pattern, $reason, $next_pattern, $next_reason)
	{
		$prev = sprintf("%d:%d", $pattern, $reason);
		$next = sprintf("%d:%d", $next_pattern, $next_reason);
		if ( empty($this->_next_table[$prev]) ) {
			return false;
		}
		foreach ( $this->_next_table[$prev] as $check ) {
			if ( $check == $next ) {
				return true;
			}
		}
		return false;
	}
	
	/*************************************************************************/
	// 翌日のパターンを取得
	/*************************************************************************/
	function get_next_pattern($pattern, $reason)
	{
		$prev = sprintf("%d:%d", $pattern, $reason);
		foreach ( $this->_next_table[$prev] as $check ) {
			if ( !empty($check) ) {
				list($next_pattern, $next_reason) = explode(":", $check);
				return array("pattern" => $next_pattern, "reason" => $next_reason);
			}
		}
		return array();
	}
	
}

class ok_pattern_class extends duty_shift_auto_pattern_class
{
	// コンストラクタ
	function ok_pattern_class($group_id, $con, $fname)
	{
		// 呼び出し元ファイル名を保持
		$this->file_name = $fname;
		// DBコネクションを保持
		$this->_db_con = $con;
		
		// パターンテーブル
		$this->_create_pattern_table($group_id, 'duty_shift_group_seq_pattern');
	}
}

class ng_pattern_class extends duty_shift_auto_pattern_class
{
	// コンストラクタ
	function ng_pattern_class($group_id, $con, $fname)
	{
		// 呼び出し元ファイル名を保持
		$this->file_name = $fname;
		// DBコネクションを保持
		$this->_db_con = $con;
		
		// パターンテーブル
		$this->_create_pattern_table($group_id, 'duty_shift_group_ng_pattern');
	}
}