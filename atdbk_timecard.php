<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 出勤表 | タイムカード入力</title>
<?
//ini_set("display_errors", "1");
require_once("about_comedix.php");
require_once("show_attendance_pattern.ini");
require_once("show_timecard_common.ini");
require_once("holiday.php");
require_once("atdbk_menu_common.ini");
require_once("atdbk_common_class.php");
require_once("timecard_common_class.php");
require_once("atdbk_duty_shift_common_class.php");
require_once("date_utils.php");
require_once("atdbk_workflow_common_class.php");
require_once("timecard_bean.php");
require_once("timecard_paid_hol_hour_class.php");
require_once("calendar_name_class.php");
require_once("atdbk_close_class.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 5, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 勤務管理権限の取得
$work_admin_auth = check_authority($session, 42, $fname);

// 表示モードのデフォルトは「実績表示」
if ($view == "") {
	$view = "1";
}

// データベースに接続
$con = connect2db($fname);

//出勤表関連共通クラス
$atdbk_common_class = new atdbk_common_class($con, $fname);
//javascript出力
$atdbk_common_class->setJavascript();

//ワークフロー関連共通クラス
$atdbk_workflow_common_class = new atdbk_workflow_common_class($con, $fname);

//時間有休
$obj_hol_hour = new timecard_paid_hol_hour_class($con, $fname);
$obj_hol_hour->select();
//カレンダー名、所定労働時間履歴用
$calendar_name_class = new calendar_name_class($con, $fname);

// 職員ID・氏名を取得
$sql = "select emp_id, emp_lt_nm, emp_ft_nm, emp_personal_id from empmst";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");
$emp_name = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");
$emp_personal_id = pg_fetch_result($sel, 0, "emp_personal_id");

//会議研修病棟外の列を常に表示とする 20120319
$meeting_display_flag = true;

// ログインユーザの出勤グループを取得
$user_tmcd_group_id = get_timecard_group_id($con, $emp_id, $fname);

// 出勤パターン名・事由名を配列で取得
//$arr_attendance_pattern = get_attendance_pattern_array($con, $fname, $user_tmcd_group_id);
//$arr_attendance_reason = get_attendance_reason_array();

// ************ oose add start *****************
// 勤務条件テーブルより勤務形態（常勤、非常勤）、残業管理を取得
$sql = "select duty_form, no_overtime from empcond";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$duty_form = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "duty_form") : "";
$no_overtime = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "no_overtime") : "";
// ************ oose add end *****************

//タイムカード情報の取得
$timecard_bean = new timecard_bean();
$timecard_bean->select($con, $fname);

$atdbk_close_class = new atdbk_close_class($con, $fname);
$closing = $atdbk_close_class->get_empcond_closing($emp_id);
$closing_month_flg = $atdbk_close_class->closing_month_flg;

// 締め日が登録済みの場合
if ($closing != "") {

	if ($yyyymm != "") {
		$year = substr($yyyymm, 0, 4);
		$month = intval(substr($yyyymm, 4, 2));
		$month_set_flg = true; //月が指定された場合
	} else {
		$year = date("Y");
		$month = date("n");
		$yyyymm = $year . date("m");
		$month_set_flg = false; //月が未指定の場合
	}

	// 開始日・締め日を算出
	$calced = false;
	switch ($closing) {
	case "1":  // 1日
		$closing_day = 1;
		break;
	case "2":  // 5日
		$closing_day = 5;
		break;
	case "3":  // 10日
		$closing_day = 10;
		break;
	case "4":  // 15日
		$closing_day = 15;
		break;
	case "5":  // 20日
		$closing_day = 20;
		break;
	case "6":  // 末日
		$start_year = $year;
		$start_month = $month;
		$start_day = 1;
		$end_year = $start_year;
		$end_month = $start_month;
		$end_day = days_in_month($end_year, $end_month);
		$calced = true;
		break;
	}
	if (!$calced) {
		$day = date("j");
		$start_day = $closing_day + 1;
		$end_day = $closing_day;

		//月が指定された場合
		if ($month_set_flg) {
			//当月〜翌月
			if ($closing_month_flg != "2") {
				$start_year = $year;
				$start_month = $month;

				$end_year = $year;
				$end_month = $month + 1;
				if ($end_month == 13) {
					$end_year++;
					$end_month = 1;
				}
			}
			//前月〜当月
			else {
				$start_year = $year;
				$start_month = $month - 1;
				if ($start_month == 0) {
					$start_year--;
					$start_month = 12;
				}

				$end_year = $year;
				$end_month = $month;
			}

		} else {
			if ($day <= $closing_day) {
				$start_year = $year;
				$start_month = $month - 1;
				if ($start_month == 0) {
					$start_year--;
					$start_month = 12;
				}

				$end_year = $year;
				$end_month = $month;
			} else {
				$start_year = $year;
				$start_month = $month;

				$end_year = $year;
				$end_month = $month + 1;
				if ($end_month == 13) {
					$end_year++;
					$end_month = 1;
				}
			}
		}
	}
	$start_date = sprintf("%04d%02d%02d", $start_year, $start_month, $start_day);
	$end_date = sprintf("%04d%02d%02d", $end_year, $end_month, $end_day);


//月度対応
	//月が未指定の場合
	if (!$month_set_flg) {
		if ($closing_month_flg != "2") {
			$year = $start_year;
			$month = $start_month;
		} else {
			$year = $end_year;
			$month = $end_month;
		}
		$yyyymm = sprintf("%04d%02d", $year, $month);
	}

//前月、翌月リンク用
	$last_month_y = $year;
	$last_month_m = $month - 1;
	if ($last_month_m == 0) {
		$last_month_y--;
		$last_month_m = 12;
	}
	$last_yyyymm = $last_month_y . sprintf("%02d", $last_month_m);

	$next_month_y = $year;
	$next_month_m = $month + 1;
	if ($next_month_m == 13) {
		$next_month_y++;
		$next_month_m = 1;
	}
	$next_yyyymm = $next_month_y . sprintf("%02d", $next_month_m);



	// 変則労働適用情報を配列で取得
	$arr_irrg_info = get_irrg_info_array($con, $emp_id, $fname);

	// 実績表示ボタン名
	$view_button = ($view == "1") ? "予実績表示" : "実績表示";

	// 対象年月の締め状況を取得
//	$tmp_yyyymm = substr($start_date, 0, 6);
//月度対応 20100415
	$tmp_yyyymm = $year.sprintf("%02d", $month);
	$sql = "select count(*) from atdbkclose";
	$cond = "where emp_id = '$emp_id' and yyyymm = '$tmp_yyyymm'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$atdbk_closed = (pg_fetch_result($sel, 0, 0) > 0);
/*
	// 締め済みの場合、締めデータから表示対象期間を取得
	if ($atdbk_closed) {
		$sql = "select min(date), max(date) from wktotald";
		$cond = "where emp_id = '$emp_id' and yyyymm = '$tmp_yyyymm'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$start_date = pg_fetch_result($sel, 0, 0);
		$end_date = pg_fetch_result($sel, 0, 1);

		$start_year = intval(substr($start_date, 0, 4));
		$start_month = intval(substr($start_date, 4, 2));
		$start_day = intval(substr($start_date, 6, 2));
		$end_year = intval(substr($end_date, 0, 4));
		$end_month = intval(substr($end_date, 4, 2));
		$end_day = intval(substr($end_date, 6, 2));
	}
*/
	// タイムカード設定情報を取得
	$sql = "select ovtmscr_tm, modify_flg, show_time_flg from timecard";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0) {
		$ovtmscr_tm = pg_fetch_result($sel, 0, "ovtmscr_tm");
		$modify_flg = pg_fetch_result($sel, 0, "modify_flg");
		$show_time_flg = pg_fetch_result($sel, 0, "show_time_flg");
	}
	if ($ovtmscr_tm == "") {$ovtmscr_tm = 0;}
	if ($modify_flg == "") {$modify_flg = "t";}
	if ($show_time_flg == "") {$show_time_flg = "t";}

	// 給与支給区分・祝日計算方法を取得
    $sql = "select wage, hol_minus, specified_time, am_time, pm_time from empcond";
	$cond = "where emp_id = '$emp_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0) {
		$wage = pg_fetch_result($sel, 0, "wage");
		$hol_minus = pg_fetch_result($sel, 0, "hol_minus");
        $paid_specified_time = pg_fetch_result($sel, 0, "specified_time");
        $am_time = pg_fetch_result($sel, 0, "am_time");
        $pm_time = pg_fetch_result($sel, 0, "pm_time");
    } else {
		$wage = "";
		$hol_minus = "";
        $paid_specified_time = "";
        $am_time = "";
        $pm_time = "";
    }
}
//タイムカード集計項目出力設定からデータ取得 20091104
$arr_total_flg = get_timecard_total_flg($con, $fname); //show_timecard_common.ini
$arr_total_id = get_timecard_total_id($con, $fname);; //出力項目のid

?>
<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript">
<!--
function changeView() {
	document.timecard.action = 'atdbk_timecard.php';
	document.timecard.submit();
}

function editTimecard() {
	document.timecard.action = 'atdbk_timecard_insert.php';
	document.timecard.submit();
}

function openPrintPage() {
	window.open('atdbk_timecard_print.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&yyyymm=<? echo("$yyyymm"); ?>&view=<? echo($view); ?>&wherefrom=1', 'newwin', 'width=840,height=700,scrollbars=yes');
}

function downloadCSV(type) {
	document.csv.action = 'atdbk_timecard_csv' + type + '.php';
	document.csv.submit();
}

function show_sub_window(url) {
//	var h = window.screen.availHeight;
//	var w = window.screen.availWidth;
	var h = '740';
	var w = '900';
	var option = "directories=no,location=no,menubar=no,resizable=no,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;
	window.open(url, 'approvewin',option);
}

var arr_atdptn_reason = new Array();
<?
//事由情報を出力
$arr_group_pattern_reason = $atdbk_common_class->get_pattern_reason_array("");
foreach ($arr_group_pattern_reason as $wk_tmcd_group_id => $arr_pattern_reason) {
	foreach ($arr_pattern_reason as $wk_atdptn_id => $reason) {
		if ($reason != "") {
			echo("arr_atdptn_reason['{$wk_tmcd_group_id}_{$wk_atdptn_id}'] = '$reason';\n");
		}
	}
}
?>
function setReason(pos, tmcd_group_id, atdptn_id) {

	if (atdptn_id == '--') {
		document.timecard["reason[]"][pos].value = '--';
		return;
	}
	wk_group_id = document.timecard["list_tmcd_group_id[]"][pos].value;
	wk_id = wk_group_id+'_'+atdptn_id;
	if (arr_atdptn_reason[wk_id] == undefined) {
		document.timecard["reason[]"][pos].value = '--';
		return;
	}
	document.timecard["reason[]"][pos].value = arr_atdptn_reason[wk_id];
}

//一括修正画面
function openTimecardAll() {
	base_left = 0;
	base = 0;
	wx = window.screen.availWidth;
	wy = window.screen.availHeight;
//	wx = 1000;
//	wy = 700;
	url = 'atdbk_timecard_all.php?session=<?=$session?>&yyyymm=<? echo($yyyymm); ?>&view=<? echo($view); ?>&check_flg=<? echo($check_flg); ?>';
	window.open(url, 'TimecardAll', 'left='+base_left+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
}
//チェック
function inputcheck() {
	document.timecard.check_flg.value = '1';
	view = (document.timecard.view.value == '1') ? '2' : '1';
	document.timecard.view.value = view;
	document.timecard.submit();

}


//-->
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
td.lbl {padding-right:1em;}
td.txt {padding-right:0.5em;}
td.tm {width:2.8em;text-align:right;}
td.mark {width:2.8em;text-align:center;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><img src="img/icon/b07.gif" width="32" height="32" border="0" alt="出勤表"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>出勤表</b></a></font></td>
<? if ($work_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="work_admin_menu.php?session=<? echo($session); ?>&work=1"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<?
// メニュー表示
show_atdbk_menuitem($session, $fname, "");
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<? if ($closing == "") {  // 締め日未登録の場合 ?>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイムカード締め日が登録されていません。管理者に連絡してください。</font></td>
</tr>
</table>
<? } else { ?>
<form name="timecard" method="post">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<?php
	// タイムカードタブ直下のメニュー表示
 	timecard_menu_view($con, $fname, $session, $yyyymm, "url_timecard");
?>

<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
<td height="22" width="360"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">対象期間&nbsp;&nbsp;<a href="atdbk_timecard.php?session=<? echo($session); ?>&yyyymm=<? echo($last_yyyymm); ?>&view=<? echo($view); ?>">&lt;前月</a>&nbsp;<?
    $str = show_yyyymm_menu($year, $month);
    echo $str;
?>
&nbsp;<a href="atdbk_timecard.php?session=<? echo($session); ?>&yyyymm=<? echo($next_yyyymm); ?>&view=<? echo($view); ?>">翌月&gt;</a></font>
&nbsp;
<input type="button" value="チェック" onclick="inputcheck();">

</td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">開始日&nbsp;&nbsp;<? echo("{$start_month}月{$start_day}日"); ?>&nbsp;&nbsp;〜&nbsp;&nbsp;締め日&nbsp;&nbsp;<? echo("{$end_month}月{$end_day}日"); ?></font></td>
</tr>
</table>
</td>
</tr>
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
<td height="22" width=""><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員ID&nbsp;<? echo($emp_personal_id); ?>&nbsp;&nbsp;職員氏名&nbsp;<? echo($emp_name);
// 勤務条件表示
$str_empcond = get_employee_empcond2($con, $fname, $emp_id);

$str_shiftname = get_shiftname($con, $fname, $atdbk_closed, $emp_id, $start_date, $end_date);
//所属表示
$str_orgname = get_orgname($con, $fname, $emp_id, $timecard_bean);
echo("　<b>（".$str_empcond."　".$str_shiftname."）$str_orgname</b>");
?></font></td>
<td align="right" width=""><input type="button" value="<? echo($view_button); ?>" onclick="changeView();">&nbsp;<input type="button" value="登録" onclick="editTimecard();"<? if ($atdbk_closed) {echo(" disabled");} ?>>&nbsp;<input type="button" value="印刷" onclick="openPrintPage();">&nbsp;<input type="button" value="打刻CSV出力" onclick="downloadCSV(1);"<? if ($view == "2") {echo(" disabled");} ?>>&nbsp;<input type="button" value="月集計CSV出力" onclick="downloadCSV(2);"<? if ($view == "2") {echo(" disabled");} ?>></td>
</tr>
</table>
</td>
</tr>
<tr>
<td>
<table width="100%" border="1" cellspacing="0" cellpadding="1">
<tr height="22">
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日付</font></td>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">曜日</font></td>
<?
	if ($timecard_bean->time_move_flag == "t"){
		if ($timecard_bean->zen_display_flag == "t"){
?>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">前</font></td>
<?
		}
?>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤</font></td>
<?
		if ($timecard_bean->yoku_display_flag == "t"){
?>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">翌</font></td>
<?
		}
?>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退勤</font></td>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務</font></td>
<? //遅刻早退追加 20100907 ?>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">遅刻</font></td>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">早退</font></td>
		<?
		if ($timecard_bean->over_disp_split_flg != "f"){
?>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">法内残</font></td>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">法外残</font></td>
<?
		} else {
?>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">残業</font></td>
<?
		}
	}
	if ($timecard_bean->type_display_flag == "t"){
?>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">種別</font></td>
<?
	}

	if ($timecard_bean->group_display_flag == "t"){
?>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">グループ</font></td>
<?
	}
?>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務実績</font></td>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">事由</font></td>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($atdbk_common_class->duty_or_oncall_str); ?></font></td>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">手当</font></td>
<?
	if ($timecard_bean->time_move_flag == "f" || empty($timecard_bean->time_move_flag)){
		if ($timecard_bean->zen_display_flag == "t"){
?>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">前</font></td>
<?
		}
?>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤</font></td>
<?
		if ($timecard_bean->yoku_display_flag == "t"){
?>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">翌</font></td>
<?
		}
?>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退勤</font></td>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務</font></td>
<? //遅刻早退追加 20100907 ?>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">遅刻</font></td>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">早退</font></td>
		<?
		if ($timecard_bean->over_disp_split_flg != "f"){
?>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">法内残</font></td>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">法外残</font></td>
<?
		} else {
?>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">残業</font></td>
<?
		}
	}
	//深勤を深残へ変更 20110126
?>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">深残</font></td>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">外出</font></td>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">休憩</font></td>
<?
	if ($timecard_bean->ret_display_flag == "t"){
		$ret_str = ($timecard_bean->return_icon_flg != "2") ? "退復" : "呼出";

?>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$ret_str?></font></td>
<?
	}
?>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日数</font></td>
<?
	if($meeting_display_flag){
?>
<td width="" align="center" rowspan="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">会議<br>研修(h)</font></td>
<?
	}
?>
</tr>
    <?
$sums = show_attendance_book($con, $emp_id, $start_date, $end_date, $view, $yyyymm, $fname, $session, $atdbk_closed, $ovtmscr_tm, $modify_flg, $arr_irrg_info, $show_time_flg, $wage, $hol_minus, $user_tmcd_group_id, $atdbk_common_class, $meeting_display_flag, $atdbk_workflow_common_class, $timecard_bean, $no_overtime, $closing, $closing_month_flg, $check_flg, $obj_hol_hour, $duty_form, $calendar_name_class, $paid_specified_time, $am_time, $pm_time); ?>
</table>
<img src="img/spacer.gif" width="1" height="2" alt=""><br>
</td>
</tr>
<tr>
<td>
<table width="100%" border="1" cellspacing="0" cellpadding="1">
<tr height="22">
<?
	//要勤務日数〜深夜勤務
	for ($i=0; $i<18; $i++) {
		if ($arr_total_flg[$i]["flg"] == "t") {
			echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
			echo($arr_total_flg[$i]["name"]);
			echo("</font></td>\n");
		}
	}
?>
</tr>
<tr height="22">
<?
	for ($i = 0; $i < 8; $i++) {
		if ($arr_total_flg[$i]["flg"] == "t") {
			echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($sums[$i]) . "</font></td>\n");
		}
	}
	//年残
	if ($arr_total_flg[8]["flg"] == "t") {
		echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($sums[35]) . "</font></td>\n");
	}
	for ($i = 8; $i < 14; $i++) {
		//年残分シフト
		$wk_i = $i + 1;
		if ($arr_total_flg[$wk_i]["flg"] == "t") {
			//遅刻2_10、早退2_11で1回以上あったら、赤で表示する 20101210
			if (($arr_total_flg[$wk_i]["id"] == "2_10" ||
					$arr_total_flg[$wk_i]["id"] == "2_11") && $sums[$i] > 0) {
				$wk_color = " color=\"#ff0000\"";
			} else {
				$wk_color = " color=\"#000000\"";
			}
			echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" {$wk_color}>" . nbsp($sums[$i]) . "</font></td>\n");
		}
	}
	// 支給時間
	if ($arr_total_flg[15]["flg"] == "t") {
		// 支給時間＝勤務時間＋有給取得日数×所定労働時間
		//		$paid_hol_days = substr($sums[7], 0, strlen($sums[7])-2); //"日"を除く
		// 有給取得日数を支給換算日数に変更 20090908
		//$paid_hol_days = $sums[34]; //支給換算日数
//		$paid_time = $atdbk_common_class->get_paid_time($sums[13], $paid_hol_days, $emp_id);
//半日有休対応 20110204
		//$paid_time = $atdbk_common_class->get_paid_time($sums[13], $paid_hol_days, $sums[38], $sums[39], $emp_id);
        $paid_time_total = date_utils::hi_to_minute($sums[13]) + $sums[41];
		//時間有休追加 20111207
		if ($obj_hol_hour->paid_hol_hour_flag == "t") {
			$paid_time_total += $sums[40];
		}
        $paid_time_total = ($paid_time_total == 0) ? "0:00" : minute_to_hmm($paid_time_total);
        echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($paid_time_total) . "</font></td>\n");
	}
	for ($i = 14; $i < 16; $i++) {
		//年残、支給時間分シフト
		$wk_i = $i + 2;
		if ($arr_total_flg[$wk_i]["flg"] == "t") {
			echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($sums[$i]) . "</font></td>\n");
		}
	}
?>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="2" alt=""><br>
</td>
</tr>
<?
// 事由ID、表示用名称、表示フラグの配列
$arr_reason_id = $atdbk_common_class->get_add_disp_reason_id();
$arr_reason_name = $atdbk_common_class->get_add_disp_reason_name2();
$arr_disp_flg = array();
$disp_cnt = 0;
for ($i=0; $i<count($arr_reason_id); $i++) {
	$tmp_display_flag = $atdbk_common_class->get_reason_display_flag($arr_reason_id[$i]);
	$arr_disp_flg[$i] = $tmp_display_flag;
}

?>
<tr>
<td>
<table width="100%" border="1" cellspacing="0" cellpadding="1">
<tr height="22">
<?
	//法定内勤務〜休日勤務
	for ($i=18; $i<22; $i++) {
		if ($arr_total_flg[$i]["flg"] == "t") {
			echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
			echo($arr_total_flg[$i]["name"]);
			echo("</font></td>\n");
		}
	}

	if (in_array("10_0", $arr_total_id)) {
		for ($i=0; $i<count($arr_reason_id); $i++) {
			if ($arr_disp_flg[$i] == 't') {
		?>
		<td width="" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$arr_reason_name[$i]?></font></td>
		<?
			}
		}
	}
?>
</tr>
<tr height="22">
<?
//法定内勤務、法定内残業、法定外残業、休日勤務
	for ($i = 28; $i < 32; $i++) {
		//集計項目情報の位置は18番目から
		$wk_i = $i - 10;
		if ($arr_total_flg[$wk_i]["flg"] == "t") {

			echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($sums[$i]) . "</font></td>\n");
		}
	}
	//休暇日数
	if (in_array("10_0", $arr_total_id)) {
		//年末年始追加対応 20090908
		for ($i = 16; $i < 31; $i++) {
			if ($arr_disp_flg[$i-16] == 't') {
				//法定所定追加 20110121
				if ($i==17) { //法定
					$wk_str = nbsp($sums[36]);
				} elseif ($i==18) { //所定
					$wk_str = nbsp($sums[37]);
				} elseif ($i>18 && $i<27) {
					$wk_str = nbsp($sums[$i-2]);
				} elseif ($i==27) { //年末年始
					$wk_str = nbsp($sums[33]);
				} elseif ($i>27) { //
					$wk_str = nbsp($sums[$i-3]);
				} else {
					$wk_str = nbsp($sums[$i]);
				}
				echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . $wk_str . "</font></td>\n");
			}
		}
	}
?>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="2" alt=""><br>
</td>
</tr>
<?
// 集計項目表示
	show_shift_summary2($con, $fname, $atdbk_closed, $emp_id, $start_date, $end_date, $arr_total_id);

?>
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
<td align="right"><input type="button" value="<? echo($view_button); ?>" onclick="changeView();">&nbsp;<input type="button" value="登録" onclick="editTimecard();"<? if ($atdbk_closed) {echo(" disabled");} ?>>&nbsp;<input type="button" value="印刷" onclick="openPrintPage();">&nbsp;<input type="button" value="打刻CSV出力" onclick="downloadCSV(1);"<? if ($view == "2") {echo(" disabled");} ?>>&nbsp;<input type="button" value="月集計CSV出力" onclick="downloadCSV(2);"<? if ($view == "2") {echo(" disabled");} ?>></td>
</tr>
</table>
</td>
</tr>

</table>



<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="emp_id" value="<? echo($emp_id); ?>">
<input type="hidden" name="yyyymm" value="<? echo($yyyymm); ?>">
<input type="hidden" name="view" value="<? if ($view == "1") {echo("2");} else {echo("1");} ?>">
<input type="hidden" name="wherefrom" value="1">
<input type="hidden" name="check_flg" value="">
</form>
<form name="csv" method="get" target="download">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="emp_id" value="<? echo($emp_id); ?>">
<input type="hidden" name="yyyymm" value="<? echo($yyyymm); ?>">
<input type="hidden" name="wherefrom" value="1">
</form>
<iframe name="download" width="0" height="0" frameborder="0"></iframe>
<? } ?>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
// 出勤予定を出力
function show_attendance_book($con, $emp_id, $start_date, $end_date, $view, $yyyymm, $fname, $session, $atdbk_closed, $ovtmscr_tm, $modify_flg, $arr_irrg_info, $show_time_flg, $wage, $hol_minus, $user_tmcd_group_id, $atdbk_common_class, $meeting_display_flag, $atdbk_workflow_common_class, $timecard_bean, $no_overtime, $closing, $closing_month_flg, $check_flg, $obj_hol_hour, $duty_form, $calendar_name_class, $empcond_specified_time, $empcond_am_time, $empcond_pm_time) {

	//休日出勤と判定する日設定取得 20091222
	$arr_timecard_holwk_day = get_timecard_holwk_day($con, $fname, $emp_id);

	//公休と判定する日設定 20100810
	$arr_timecard_holwk_day_std = get_timecard_holwk_day($con, $fname, "");

	//一括修正申請情報
	$arr_tmmdapplyall_status = $atdbk_workflow_common_class->get_tmmdapplyall_status($emp_id, $start_date, $end_date);

	// 集計結果格納用配列を初期化
	$sums = array_fill(0, 42, 0); // 法定所定追加 20110121
	// 手当情報取得
	$arr_allowance = get_timecard_allowance($con, $fname);
	// 端数処理情報を取得
	$sql = "select * from timecard";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0) {
		for ($i = 1; $i <= 5; $i++) {
			$var_name = "fraction$i";
			$var_name_min = "fraction{$i}_min";
			$$var_name = pg_fetch_result($sel, 0, "fraction$i");
			if ($i != 3) {
				$$var_name_min = pg_fetch_result($sel, 0, "fraction{$i}_min");
			}
		}
		for ($i = 1; $i <= 5; $i++) {
			$var_name = "others$i";
			$$var_name = pg_fetch_result($sel, 0, "others$i");
		}
		$rest1 = intval(pg_fetch_result($sel, 0, "rest1"));
		$rest2 = intval(pg_fetch_result($sel, 0, "rest2"));
		$rest3 = intval(pg_fetch_result($sel, 0, "rest3"));
		//時間給者の休憩時間追加 20100929
		$rest4 = intval(pg_fetch_result($sel, 0, "rest4"));
		$rest5 = intval(pg_fetch_result($sel, 0, "rest5"));
		$rest6 = intval(pg_fetch_result($sel, 0, "rest6"));
		$rest7 = intval(pg_fetch_result($sel, 0, "rest7"));
		// ************ oose add start *****************
		$timecard_modify_flg = pg_fetch_result($sel, 0, "modify_flg");
// ************ oose add end *****************
		$delay_overtime_flg = pg_fetch_result($sel, 0, "delay_overtime_flg");
		$early_leave_time_flg = pg_fetch_result($sel, 0, "early_leave_time_flg");
		//$overtime_40_flg = pg_fetch_result($sel, 0, "overtime_40_flg");
	} else {
		for ($i = 1; $i <= 5; $i++) {
			$var_name = "fraction$i";
			$var_name_min = "fraction{$i}_min";
			$$var_name = "";
			$$var_name_min = "";
		}
		for ($i = 1; $i <= 5; $i++) {
			$var_name = "others$i";
			$$var_name = "";
		}
		$rest1 = 0;
		$rest2 = 0;
		$rest3 = 0;
		//時間給者の休憩時間追加 20100929
		$rest4 = 0;
		$rest5 = 0;
		$rest6 = 0;
		$rest7 = 0;
		// ************ oose add start *****************
		$timecard_modify_flg = "";
// ************ oose add end *****************
		$delay_overtime_flg = "1";
		$early_leave_time_flg = "2";
	}
	//週40時間超過分は計算しないを固定とする 20100525
	$overtime_40_flg = "2";

	//集計処理の設定を「集計しない」の固定とする 20100910
	$others1 = "1";	//勤務時間内に外出した場合
	$others2 = "1";	//勤務時間外に外出した場合
	$others3 = "1";	//時間給者の休憩の集計
	$others4 = "1";	//遅刻・早退に休憩が含まれた場合
	$others5 = "2";	//早出残業時間の集計

	// 「退勤後復帰」ボタン表示フラグを取得
	$sql = "select ret_btn_flg from config";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rolllback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$ret_btn_flg = pg_fetch_result($sel, 0, "ret_btn_flg");

	// 全日付をループ
	$tmp_date = $start_date;
	$wd = date("w", to_timestamp($tmp_date));
	//（変則労働時間・週の場合、週の開始曜日からスタート）
	if ($arr_irrg_info["irrg_type"] == "1") {
		while (($arr_irrg_info["irrg_wd"] == "1" && $wd != 0) || ($arr_irrg_info["irrg_wd"] == "2" && $wd != 1)) {
			$tmp_date = last_date($tmp_date);
			$wd = date("w", to_timestamp($tmp_date));
		}
	} else {
	//変則労働時間・週以外の場合、日曜から
		while ($wd != 0) {
			$tmp_date = last_date($tmp_date);
			$wd = date("w", to_timestamp($tmp_date));
		}
	}
	//データ検索用開始日
	$wk_start_date = $tmp_date;

	$counter_for_week = 1;
	$holiday_count = 0;
// ************ oose add start *****************
	$date = getdate(); //現在日付の取得
	$now_yyyy = $date["year"];
	$now_mm = $date["mon"];
	$now_dd = $date["mday"];
	$now_date = sprintf("%04d%02d%02d", $now_yyyy, $now_mm,$now_dd);
// ************ oose add end *****************
	//グループ毎のテーブルから勤務時間を取得する
	$timecard_common_class = new timecard_common_class($con, $fname, $emp_id, $tmp_date, $end_date);

	//個人別所定時間を優先するための対応 20120309
	$arr_empcond_officehours_ptn = $timecard_common_class->get_empcond_officehours_pattern();
	$arr_weekday_flg = $timecard_common_class->get_arr_weekday_flag($wk_start_date, $end_date);
    $arr_empcond_officehours = $timecard_common_class->get_arr_empcond_officehours($emp_id, $wk_start_date); //個人別勤務時間帯履歴対応 20130220

	//所定労働時間を取得
	//$day1_time = $timecard_common_class->get_calendarname_day1_time();
    //所定労働時間履歴を使用 20121127
    if ($empcond_specified_time != "") {
        $paid_specified_time = $empcond_specified_time;
        $day1_time = $empcond_specified_time;
        $am_time = $empcond_am_time;
        $pm_time = $empcond_pm_time;
        $empcond_spec_time_flg = true;
    }
    else {
        $arr_timehist = $calendar_name_class->get_calendarname_time($start_date);
        $day1_time = $arr_timehist["day1_time"];
        $am_time = $arr_timehist["am_time"];
        $pm_time = $arr_timehist["pm_time"];
        $empcond_spec_time_flg = false;
    }

	//出・退勤時刻文字サイズ
	$time_font = "\"j12\"";
	if ($timecard_bean->time_big_font_flag == "t" && $show_time_flg == "t"){
		$time_font = "\"y16\"";
	}
	//翌月初日までデータを取得 20130308
	$wk_end_date_nextone = next_date($end_date);
	// 処理日付の勤務日種別を取得
	$arr_type = array();
	$sql = "select date, type from calendar";
	$cond = "where date >= '$wk_start_date' and date <= '$wk_end_date_nextone'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$date = $row["date"];
		$arr_type[$date] = $row["type"];
	}
// 予定データをまとめて取得
	$arr_atdbk = array();
	$sql =	"SELECT ".
				"atdbk.date, ".
				"atdbk.pattern, ".
				"atdbk.reason, ".
				"atdbk.night_duty, ".
				"atdbk.allow_id, ".
				"atdbk.prov_start_time, ".
				"atdbk.prov_end_time, ".
				"atdbk.tmcd_group_id, ".
				"atdptn.workday_count ".
			"FROM ".
				"atdbk ".
					"LEFT JOIN atdptn ON atdbk.tmcd_group_id = atdptn.group_id AND atdbk.pattern = CAST(atdptn.atdptn_id AS varchar) ";
	$cond = "where emp_id = '$emp_id' and ";
	$cond .= " date >= '$wk_start_date' and date <= '$end_date'";

	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$date = $row["date"];
		$arr_atdbk[$date]["tmcd_group_id"] = $row["tmcd_group_id"];
		$arr_atdbk[$date]["pattern"] = $row["pattern"];
		$arr_atdbk[$date]["reason"] = $row["reason"];
		$arr_atdbk[$date]["night_duty"] = $row["night_duty"];
		$arr_atdbk[$date]["allow_id"] = $row["allow_id"];
		$arr_atdbk[$date]["prov_start_time"] = $row["prov_start_time"];
		$arr_atdbk[$date]["prov_end_time"] = $row["prov_end_time"];
		$arr_atdbk[$date]["workday_count"] = $row["workday_count"];
	}
// 実績データをまとめて取得
	$arr_atdbkrslt = array();
	$sql =	"SELECT ".
				"atdbkrslt.*, ".
				"atdptn.workday_count, ".
				"atdptn.base_time, ".
				"atdptn.over_24hour_flag, ".
				"atdptn.out_time_nosubtract_flag, ".
				"atdptn.after_night_duty_flag, ".
                "atdptn.previous_day_flag as atdptn_previous_day_flag, ".
                "atdptn.no_count_flag, ".
                "tmmdapply.apply_id     AS tmmd_apply_id, ".
				"tmmdapply.apply_status AS tmmd_apply_status, ".
				"ovtmapply.apply_id     AS ovtm_apply_id, ".
				"ovtmapply.apply_status AS ovtm_apply_status, ".
				"rtnapply.apply_id      AS rtn_apply_id, ".
				"rtnapply.apply_status  AS rtn_apply_status ".
			"FROM ".
				"atdbkrslt ".
					"LEFT JOIN atdptn ON atdbkrslt.tmcd_group_id = atdptn.group_id AND atdbkrslt.pattern = CAST(atdptn.atdptn_id AS varchar) ".
					"LEFT JOIN tmmdapply ON atdbkrslt.emp_id = tmmdapply.emp_id AND CAST(atdbkrslt.date AS varchar) = tmmdapply.target_date AND tmmdapply.delete_flg = false AND tmmdapply.re_apply_id IS NULL ".
					"LEFT JOIN ovtmapply ON atdbkrslt.emp_id = ovtmapply.emp_id AND CAST(atdbkrslt.date AS varchar) = ovtmapply.target_date AND ovtmapply.delete_flg = false AND ovtmapply.re_apply_id IS NULL ".
					"LEFT JOIN rtnapply  ON atdbkrslt.emp_id = rtnapply.emp_id  AND CAST(atdbkrslt.date AS varchar) = rtnapply.target_date AND rtnapply.delete_flg = false AND rtnapply.re_apply_id IS NULL ";
	$cond = "where atdbkrslt.emp_id = '$emp_id' and ";
    $cond .= " atdbkrslt.date >= '$wk_start_date' and atdbkrslt.date <= '$wk_end_date_nextone'"; //20130308
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
    while ($row = pg_fetch_array($sel)) {
        $date = $row["date"];
        $arr_atdbkrslt[$date]["pattern"] = $row["pattern"];
        $arr_atdbkrslt[$date]["reason"] = $row["reason"];
        $arr_atdbkrslt[$date]["night_duty"] = $row["night_duty"];
        $arr_atdbkrslt[$date]["allow_id"] = $row["allow_id"];
        $arr_atdbkrslt[$date]["start_time"] = $row["start_time"];
        $arr_atdbkrslt[$date]["end_time"] = $row["end_time"];
        $arr_atdbkrslt[$date]["out_time"] = $row["out_time"];
        $arr_atdbkrslt[$date]["ret_time"] = $row["ret_time"];
        for ($i = 1; $i <= 10; $i++) {
            $start_var = "o_start_time$i";
            $end_var = "o_end_time$i";
            $arr_atdbkrslt[$date][$start_var] = $row[$start_var];
            $arr_atdbkrslt[$date][$end_var] = $row[$end_var];
        }
        $arr_atdbkrslt[$date]["tmcd_group_id"] = $row["tmcd_group_id"];
        $arr_atdbkrslt[$date]["meeting_time"] = $row["meeting_time"];
        $arr_atdbkrslt[$date]["workday_count"] = $row["workday_count"];
        $arr_atdbkrslt[$date]["previous_day_flag"] = $row["previous_day_flag"];
        $arr_atdbkrslt[$date]["next_day_flag"] = $row["next_day_flag"];
        $arr_atdbkrslt[$date]["tmmd_apply_id"] = $row["tmmd_apply_id"];
        $arr_atdbkrslt[$date]["tmmd_apply_status"] = $row["tmmd_apply_status"];
        $arr_atdbkrslt[$date]["ovtm_apply_id"] = $row["ovtm_apply_id"];
        $arr_atdbkrslt[$date]["ovtm_apply_status"] = $row["ovtm_apply_status"];
        $arr_atdbkrslt[$date]["rtn_apply_id"] = $row["rtn_apply_id"];
        $arr_atdbkrslt[$date]["rtn_apply_status"] = $row["rtn_apply_status"];
        $arr_atdbkrslt[$date]["meeting_start_time"] = $row["meeting_start_time"];
        $arr_atdbkrslt[$date]["meeting_end_time"] = $row["meeting_end_time"];
        $arr_atdbkrslt[$date]["base_time"] = $row["base_time"];
        //残業時刻追加 20100114
        $arr_atdbkrslt[$date]["allow_count"] = $row["allow_count"];
        $arr_atdbkrslt[$date]["over_start_time"] = $row["over_start_time"];
        $arr_atdbkrslt[$date]["over_end_time"] = $row["over_end_time"];
        $arr_atdbkrslt[$date]["over_start_next_day_flag"] = $row["over_start_next_day_flag"];
        $arr_atdbkrslt[$date]["over_end_next_day_flag"] = $row["over_end_next_day_flag"];
        //残業時刻2追加 20110616
        $arr_atdbkrslt[$date]["over_start_time2"] = $row["over_start_time2"];
        $arr_atdbkrslt[$date]["over_end_time2"] = $row["over_end_time2"];
        $arr_atdbkrslt[$date]["over_start_next_day_flag2"] = $row["over_start_next_day_flag2"];
        $arr_atdbkrslt[$date]["over_end_next_day_flag2"] = $row["over_end_next_day_flag2"];
        $arr_atdbkrslt[$date]["over_24hour_flag"] = $row["over_24hour_flag"];
        //法定内残業
        $arr_atdbkrslt[$date]["legal_in_over_time"] = $row["legal_in_over_time"];
        //早出残業 20100601
        $arr_atdbkrslt[$date]["early_over_time"] = $row["early_over_time"];
        //休憩時刻追加 20100921
        $arr_atdbkrslt[$date]["rest_start_time"] = $row["rest_start_time"];
        $arr_atdbkrslt[$date]["rest_end_time"] = $row["rest_end_time"];
        //外出時間を差し引かないフラグ 20110727
        $arr_atdbkrslt[$date]["out_time_nosubtract_flag"] = $row["out_time_nosubtract_flag"];
        //明け追加 20110819
        $arr_atdbkrslt[$date]["after_night_duty_flag"] = $row["after_night_duty_flag"];
        //時間帯画面の前日フラグ 20121120
        $arr_atdbkrslt[$date]["atdptn_previous_day_flag"] = $row["atdptn_previous_day_flag"];
        //勤務時間を計算しないフラグ 20130225
        $arr_atdbkrslt[$date]["no_count_flag"] = $row["no_count_flag"];
    }
	//時間有休追加 20111207
	if ($obj_hol_hour->paid_hol_hour_flag == "t") {
		//時間有休データをまとめて取得
		$arr_hol_hour_dat = $obj_hol_hour->get_paid_hol_hour_month($emp_id, $wk_start_date, $end_date);

	}
	//法定内勤務、法定内残業、法定外残業
	$hoteinai = 0;
	$hoteinai_zan = 0;
	$hoteigai = 0;
//	$arr_week_hoteigai = array(); //1週間分の法定外残業 debug

	//遅刻時間計 20100907
	$delay_time = 0;

	//早退時間計
	$early_leave_time = 0;

	//支給換算日数 20090908
	$paid_day_count = 0;

	$wk_pos = 0; //行位置
	//休日出勤テーブル 2008/10/23
	$arr_hol_work_date = array();
	// チェック対応 20110125
	$today = date("Ymd");
	while ($tmp_date <= $end_date) {
//echo("<br>tmp_date=$tmp_date"); //debug
		$tmp_year = intval(substr($tmp_date, 0, 4));
		$tmp_month = intval(substr($tmp_date, 4, 2));
		$tmp_day = intval(substr($tmp_date, 6, 2));

		if ($counter_for_week >= 8) {
			$counter_for_week = 1;
		}
		if ($counter_for_week == 1) {
			//前日までの計
			$last_day_total = 0;
			$work_time_for_week = 0;
			if ($arr_irrg_info["irrg_type"] == "1") {
				$holiday_count = 0;
			}
			//1日毎の法定外残を週単位に集計 20090708
			$wk_week_hoteigai = 0;
//			$arr_week_hoteigai = array();
		}
		$work_time = 0;
		$wk_return_time = 0;

		$time2 = "";

		$start_time_info = array();
		$end_time_info = array(); //退勤時刻情報 20100910

		$time3 = 0; //外出時間（所定時間内） 20100910
		$time4 = 0; //外出時間（所定時間外） 20100910
		$tmp_rest2 = 0; //休憩時間 20100910
		$time3_rest = 0; //休憩時間（所定時間内）20100910
		$time4_rest = 0; //休憩時間（所定時間外）20100910
		$arr_effective_time_wk = array(); //休憩を引く前の実働時間 20100915
		$arr_hol_time = array(); // 休日勤務情報 20101224

		$holiday_name = get_holiday_name($tmp_date);
		if ($holiday_name != "") {
			$holiday_count++;
		}
		// 処理日付の勤務日種別を取得
		$type = $arr_type[$tmp_date];

		// カレンダー名を取得
		$type_name = $atdbk_common_class->get_calendarname($type, $tmp_date);
		// 処理日付の勤務予定情報を取得
		$prov_tmcd_group_id = $arr_atdbk[$tmp_date]["tmcd_group_id"];
		$prov_pattern = $arr_atdbk[$tmp_date]["pattern"];
		$prov_reason = $arr_atdbk[$tmp_date]["reason"];
		$prov_night_duty = $arr_atdbk[$tmp_date]["night_duty"];
		$prov_allow_id = $arr_atdbk[$tmp_date]["allow_id"];

		$prov_start_time = $arr_atdbk[$tmp_date]["prov_start_time"];
		$prov_end_time = $arr_atdbk[$tmp_date]["prov_end_time"];
		$prov_start_time = ($prov_start_time != "") ? preg_replace("/(\d{2})(\d{2})/", "$1:$2", $prov_start_time) : "";
		$prov_end_time =  ($prov_end_time != "") ? preg_replace("/(\d{2})(\d{2})/", "$1:$2", $prov_end_time) : "";
		$prov_workday_count = $arr_atdbk[$tmp_date]["workday_count"];

		// 処理日付の勤務実績を取得
		$pattern = $arr_atdbkrslt[$tmp_date]["pattern"];
		$reason = $arr_atdbkrslt[$tmp_date]["reason"];
        $night_duty = $arr_atdbkrslt[$tmp_date]["night_duty"];
		$allow_id = $arr_atdbkrslt[$tmp_date]["allow_id"];
		$start_time = $arr_atdbkrslt[$tmp_date]["start_time"];
		$end_time = $arr_atdbkrslt[$tmp_date]["end_time"];
		$out_time = $arr_atdbkrslt[$tmp_date]["out_time"];
		$ret_time = $arr_atdbkrslt[$tmp_date]["ret_time"];

		for ($i = 1; $i <= 10; $i++) {
			$start_var = "o_start_time$i";
			$end_var = "o_end_time$i";
			$$start_var = $arr_atdbkrslt[$tmp_date]["$start_var"];
			$$end_var = $arr_atdbkrslt[$tmp_date]["$end_var"];

			if ($$start_var != "") {
				$$start_var = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $$start_var);
			}
			if ($$end_var != "") {
				$$end_var = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $$end_var);
			}
		}

		$tmcd_group_id = $arr_atdbkrslt[$tmp_date]["tmcd_group_id"];
		$meeting_time = $arr_atdbkrslt[$tmp_date]["meeting_time"];

		$workday_count = $arr_atdbkrslt[$tmp_date]["workday_count"];

		$previous_day_flag = $arr_atdbkrslt[$tmp_date]["previous_day_flag"];
        $next_day_flag = $arr_atdbkrslt[$tmp_date]["next_day_flag"];

		$tmmd_apply_id = $arr_atdbkrslt[$tmp_date]["tmmd_apply_id"];
		$tmmd_apply_status = $arr_atdbkrslt[$tmp_date]["tmmd_apply_status"];
		$ovtm_apply_id = $arr_atdbkrslt[$tmp_date]["ovtm_apply_id"];
		$ovtm_apply_status = $arr_atdbkrslt[$tmp_date]["ovtm_apply_status"];
		$rtn_apply_id = $arr_atdbkrslt[$tmp_date]["rtn_apply_id"];
		$rtn_apply_status = $arr_atdbkrslt[$tmp_date]["rtn_apply_status"];

		$meeting_start_time = $arr_atdbkrslt[$tmp_date]["meeting_start_time"];
		$meeting_end_time = $arr_atdbkrslt[$tmp_date]["meeting_end_time"];
		$base_time = $arr_atdbkrslt[$tmp_date]["base_time"];
		//法定外残業の基準時間
		if ($base_time == "") {
			$wk_base_time = 8 * 60;
		} else {
			$base_hour = intval(substr($base_time, 0, 2));
			$base_min = intval(substr($base_time, 2, 2));
			$wk_base_time = $base_hour * 60 + $base_min;
		}

		//hhmmをhh:mmに変換している
		if ($start_time != "") {
			$start_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $start_time);
		}
		if ($end_time != "") {
			$end_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $end_time);
		}
		//表示用に退避 20100802
		$disp_start_time = $start_time;
		$disp_end_time = $end_time;

		if ($out_time != "") {
			$out_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $out_time);
		}
		if ($ret_time != "") {
			$ret_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $ret_time);
		}
		if ($meeting_start_time != "") {
			$meeting_start_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $meeting_start_time);
		}
		if ($meeting_end_time != "") {
			$meeting_end_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $meeting_end_time);
		}
		if ($previous_day_flag == "") {
			$previous_day_flag = 0;
		}
		if ($next_day_flag == "") {
			$next_day_flag = 0;
		}
		//残業時刻追加 20100114
		$over_start_time = $arr_atdbkrslt[$tmp_date]["over_start_time"];
		$over_end_time = $arr_atdbkrslt[$tmp_date]["over_end_time"];
		$over_start_next_day_flag = $arr_atdbkrslt[$tmp_date]["over_start_next_day_flag"];
		$over_end_next_day_flag = $arr_atdbkrslt[$tmp_date]["over_end_next_day_flag"];
		if ($over_start_time != "") {
			$over_start_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $over_start_time);
		}
		if ($over_end_time != "") {
			$over_end_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $over_end_time);
		}
		//残業時刻2追加 20110616
		$over_start_time2 = $arr_atdbkrslt[$tmp_date]["over_start_time2"];
		$over_end_time2 = $arr_atdbkrslt[$tmp_date]["over_end_time2"];
		$over_start_next_day_flag2 = $arr_atdbkrslt[$tmp_date]["over_start_next_day_flag2"];
		$over_end_next_day_flag2 = $arr_atdbkrslt[$tmp_date]["over_end_next_day_flag2"];
		if ($over_start_time2 != "") {
			$over_start_time2 = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $over_start_time2);
		}
		if ($over_end_time2 != "") {
			$over_end_time2 = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $over_end_time2);
		}
		$over_24hour_flag = $arr_atdbkrslt[$tmp_date]["over_24hour_flag"];
		//法定内残業
		$legal_in_over_time = $arr_atdbkrslt[$tmp_date]["legal_in_over_time"];
		if ($legal_in_over_time != "") {
			$legal_in_over_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $legal_in_over_time);
		}
		//早出残業 20100601
		$early_over_time = $arr_atdbkrslt[$tmp_date]["early_over_time"];
		if ($early_over_time != "") {
			$early_over_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $early_over_time);
		}
		//休憩時刻追加 20100921
		$rest_start_time = $arr_atdbkrslt[$tmp_date]["rest_start_time"];
		$rest_end_time = $arr_atdbkrslt[$tmp_date]["rest_end_time"];
		if ($rest_start_time != "") {
			$rest_start_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $rest_start_time);
		}
		if ($rest_end_time != "") {
			$rest_end_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $rest_end_time);
		}
		//外出時間を差し引かないフラグ 20110727
		$out_time_nosubtract_flag = $arr_atdbkrslt[$tmp_date]["out_time_nosubtract_flag"];

		//明け追加 20110819
		$after_night_duty_flag = $arr_atdbkrslt[$tmp_date]["after_night_duty_flag"];
        //時間帯画面の前日フラグ 20121120
        $atdptn_previous_day_flag = $arr_atdbkrslt[$tmp_date]["atdptn_previous_day_flag"];
        //勤務時間を計算しないフラグ 20130225
        $no_count_flag = $arr_atdbkrslt[$tmp_date]["no_count_flag"];
        
		//出勤予定にグループの指定がない場合、ユーザに設定したグループを優先する
		if (empty($tmcd_group_id)){
			// 出勤パターン名を取得
			$tmcd_group_id = $user_tmcd_group_id;
		}

		$selectGroupId   = "tmcd_group_id_".$tmp_date;
		$selectPatternId = "pattern_".$tmp_date;

		//当直の有効判定
		$night_duty_flag = $timecard_common_class->is_valid_duty($night_duty, $tmp_date, $start_time, $previous_day_flag, $end_time, $next_day_flag, $tmcd_group_id);

		// 残業表示用に退勤時刻を退避
		$show_end_time = $end_time;
		$show_start_time = $start_time;
		$show_next_day_flag = $next_day_flag;	//残業時刻日またがり対応 20100114

		$work_workday_count = "";

		if ($tmp_date >= $start_date) {

			//公休か事由なし休暇で残業時刻がありの場合 20100802
//			if ($pattern == "10" && ($reason == "22" || $reason == "23" || $reason == "24" || $reason == "34" || $reason == "") && $over_start_time != "" && $over_end_time != "") {
			//事由に関わらず休暇で残業時刻がありの場合へ変更 20100916
			if (($pattern == "10" || $after_night_duty_flag == "1") && //明けを追加 20110819
				$over_start_time != "" && $over_end_time != "") {
				$legal_hol_over_flg = true;
				//残業時刻を設定
				$start_time = $over_start_time;
				$end_time = $over_end_time;

				// 法定外算出基準を0とする
				$wk_base_time = 0;
			} else {
				$legal_hol_over_flg = false;
			}
			// 出勤日数をカウント（条件：出勤と退勤の打刻あり、または、「明け」の場合）
			if (($start_time != "" && $end_time != "") || $after_night_duty_flag == "1") {

				//日またがり対応 20101027
				if ($start_time > $end_time &&
						$previous_day_flag == "0") {
					$next_day_flag = "1";
				}

				//休暇時は計算しない 20090806
				if ($pattern != "10") {
					$work_workday_count = $workday_count;
				}
				//当直時、当直日数を追加する
				if($night_duty_flag){
					//曜日に対応したworkday_countを取得する
					$work_workday_count = $work_workday_count + $timecard_common_class->get_night_workday_count($tmp_date, $end_time, $next_day_flag, $tmcd_group_id);
				}
				$sums[1] = $sums[1] + $work_workday_count;
				//休日出勤のカウント方法変更 20091222
                if (check_timecard_holwk(
                            $arr_timecard_holwk_day, $arr_type[$tmp_date], $pattern, $reason, "", $timecard_bean) == true) {
					$sums[2] += 1;
				}
			}

			// 事由ベースの日数計算
			$reason_day_count = $atdbk_common_class->get_reason_day_count($reason);
			//reason_day_countは支給換算日数となる 20090908
            $paid_time = 0;
            if ($reason_day_count > 0) {
				$am_hol_days = 0;
                $pm_hol_days = 0;
                $paid_hol_days = 0;
                //半日有休を区別して集計 20110204
				switch ($reason) {
					case "2": //午前有休
					case "38": //午前年休
					case "67": //午前特別
					case "50": //午前夏休
					case "69": //午前正休
						$sums[38] += $reason_day_count;
                        $am_hol_days = 0.5;
                        break;
					case "3": //午後有休
					case "39": //午後年休
					case "68": //午後特別
					case "51": //午後夏休
					case "70": //午後正休
						$sums[39] += $reason_day_count;
                        $pm_hol_days = 0.5;
                        break;
                    case "44":  // 半有半公
                    case "55":  // 半夏半有
                    case "57":  // 半有半欠
                    case "58":  // 半特半有
                    case "62":  // 半正半有
                        $sums[34] += $reason_day_count;
                        $paid_hol_days = 0.5;
                        break;
                    default: //上記以外
                        if ($pattern == "10") { //20140106 パターンが休暇の場合
                            $sums[34] += $reason_day_count;
                            $paid_hol_days = 1.0;
                        }
                }
                //個人別勤務時間帯にある場合、曜日別に所定時間を求める 20130220 start
                $wk_weekday = $arr_weekday_flg[$tmp_date];
                $arr_empcond_officehours = $timecard_common_class->get_empcond_officehours($wk_weekday, $tmp_date);
                if ($arr_empcond_officehours["part_specified_time"] != "") {
                    $paid_specified_time = $arr_empcond_officehours["part_specified_time"];
                    $am_time = "";
                    $pm_time = "";
                    $empcond_spec_time_flg = true;
                }
                //else { //20130521 勤務条件の所定時間を2番目に優先とする
                //    $empcond_spec_time_flg = false;
                //}
                //個人別勤務時間帯にある場合、曜日別に所定時間を求める 20130220 end
                //所定時間履歴を使用 20121127
                //勤務条件にない場合
                if ($empcond_spec_time_flg == false) {
                    $arr_timehist = $calendar_name_class->get_calendarname_time($tmp_date);
                    $paid_specified_time = $arr_timehist["day1_time"];
                    $am_time = $arr_timehist["am_time"];
                    $pm_time = $arr_timehist["pm_time"];
                }
                //半日有給
                $min = 0;
                //半日午前所定時間がある場合は計算し加算
                if ($am_time != "") {
                    $hour2 = (int)substr($am_time, 0, 2);
                    $min2 = $hour2 * 60 + (int)substr($am_time, 2, 3);
                    //日数分を加算する(0.5単位のため、1回分の計算として2倍する）
                    $min += $min2 * $am_hol_days * 2;
                }
                else {
                    //ない場合は、半日以外の所定に加算
                    $paid_hol_days += $am_hol_days;
                }
                //半日午後所定時間がある場合は計算し加算
                if ($pm_time != "") {
                    $hour2 = (int)substr($pm_time, 0, 2);
                    $min2 = $hour2 * 60 + (int)substr($pm_time, 2, 3);
                    //日数分を加算する(0.5単位のため、1回分の計算として2倍する）
                    $min += $min2 * $pm_hol_days * 2;
                }
                else {
                    //ない場合は、半日以外の所定に加算
                    $paid_hol_days += $pm_hol_days;
                }

                //半日以外の換算日数
                if ($paid_specified_time != "") {
                    //所定労働時間を分単位にする
                    $hour2 = (int)substr($paid_specified_time, 0, 2);
                    $min2 = $hour2 * 60 + (int)substr($paid_specified_time, 2, 3);
                    //日数分を加算する
                    $min += $min2 * $paid_hol_days;
                }
                $paid_time = $min;
                $sums[41] += $min;
            }
			//時間有休追加 20111207
			$paid_time_hour = 0;
			if ($obj_hol_hour->paid_hol_hour_flag == "t") {
                $paid_hol_min =  $arr_hol_hour_dat[$tmp_date]["calc_minute"];
				$paid_time_hour = $paid_hol_min;
				$sums[40] += $paid_hol_min; //時間有休計
			}

			switch ($reason) {
//			case "16":  // 休日出勤　変更 2008/10/23
//				$sums[2] += $reason_day_count;
//				break;
			case "14":  // 代替出勤
				$sums[3] += 1;
				break;
			case "15":  // 振替出勤
				$sums[4] += 1;
				break;
			case "65":  // 午前振出
			case "66":  // 午後振出
				$sums[4] += 0.5;
				break;
			case "4":   // 代替休暇
				$sums[5] += 1;
				break;
			case "18":  // 半前代替休
			case "19":  // 半後代替休
				$sums[5] += 0.5;
				break;
			case "17":  // 振替休暇
				$sums[6] += 1;
				break;
			case "20":  // 半前振替休
			case "21":  // 半後振替休
				$sums[6] += 0.5;
				break;
			case "1":  // 有休休暇
			case "37": // 年休
                if ($pattern == "10") { //20140106 パターンが休暇の場合
                    $sums[7] += 1;
                }
				break;
			case "2":  // 午前有休
			case "3":  // 午後有休
			case "38": // 午前年休
			case "39": // 午後年休
				$sums[7] += 0.5;
				break;
			case "57":  // 半有半欠
				$sums[7] += 0.5;
				$sums[8] += 0.5;
				break;
			case "6":  // 一般欠勤
			case "7":  // 病傷欠勤
				$sums[8] += 1;
				break;
			case "48": // 午前欠勤
			case "49": // 午後欠勤
				$sums[8] += 0.5;
				break;
			case "8":   // その他休
				$sums[9] += 1;
				break;
			case "52":  // 午前その他休
			case "53":  // 午後その他休
				$sums[9] += 0.5;
				break;
			case "24":  // 公休
			//case "22":  // 法定休暇
			//case "23":  // 所定休暇
			case "45":  // 希望(公休)
			case "46":  // 待機(公休)
			case "47":  // 管理当直前(公休)
				$sums[16] += 1;
				break;
			case "35":  // 午前公休
			case "36":  // 午後公休
				$sums[16] += 0.5;
				break;
			case "5":   // 特別休暇
				$sums[17] += 1;
				break;
			case "67":  // 午前特別
			case "68":  // 午後特別
				$sums[17] += 0.5;
				break;
			case "58":  // 半特半有
				$sums[17] += 0.5;
				$sums[7] += 0.5;
				break;
			case "59":  // 半特半公
				$sums[17] += 0.5;
				$sums[16] += 0.5;
				break;
			case "60":  // 半特半欠
				$sums[17] += 0.5;
				$sums[8] += 0.5;
				break;
			case "25":  // 産休
				$sums[18] += 1;
				break;
			case "26":  // 育児休業
				$sums[19] += 1;
				break;
			case "27":  // 介護休業
				$sums[20] += 1;
				break;
			case "28":  // 傷病休職
				$sums[21] += 1;
				break;
			case "29":  // 学業休職
				$sums[22] += 1;
				break;
			case "30":  // 忌引
				$sums[23] += 1;
				break;
			case "31":  // 夏休
				$sums[24] += 1;
				break;
			case "50":  // 午前夏休
			case "51":  // 午後夏休
				$sums[24] += 0.5;
				break;
			case "54":  // 半夏半公
				$sums[24] += 0.5;
				$sums[16] += 0.5;
				break;
			case "55":  // 半夏半有
				$sums[24] += 0.5;
				$sums[7] += 0.5;
				break;
			case "56":  // 半夏半欠
				$sums[24] += 0.5;
				$sums[8] += 0.5;
				break;
			case "72":  // 半夏半特
				$sums[24] += 0.5;
				$sums[17] += 0.5;
				break;
			case "61":  // 年末年始
				$sums[33] += 1;
				break;
			case "69":  // 午前正休
			case "70":  // 午後正休
				$sums[33] += 0.5;
				break;
			case "62":  // 半正半有
				$sums[33] += 0.5;
				$sums[7] += 0.5;
				break;
			case "63":  // 半正半公
				$sums[33] += 0.5;
				$sums[16] += 0.5;
				break;
			case "64":  // 半正半欠
				$sums[33] += 0.5;
				$sums[8] += 0.5;
				break;
			case "32":   // 結婚休
				$sums[25] += 1;
				break;
			case "40":   // リフレッシュ休暇
				$sums[26] += 1;
				break;
			case "42":   // 午前リフレッシュ休暇
			case "43":   // 午後リフレッシュ休暇
				$sums[26] += 0.5;
				break;
			case "41":  // 初盆休暇
				$sums[27] += 1;
				break;
			case "44":  // 半有半公
				$sums[7] += 0.5;
				$sums[16] += 0.5;
				break;
			}
			// 休日となる設定と事由があう場合に公休に計算する 20100810
			// 法定休日
			if ($arr_type[$tmp_date] == "4" && $arr_timecard_holwk_day_std[0]["legal_holiday"] == "t") {
				if ($reason == "22") {
					$sums[16] += 1;
				}
			}
			// 集計行に法定休日追加 20110121
			if ($reason == "22") {
				$sums[36] += 1;
			}
			// 所定休日
			if ($arr_type[$tmp_date] == "5" && $arr_timecard_holwk_day_std[0]["prescribed_holiday"] == "t") {
				if ($reason == "23") {
					$sums[16] += 1;
				}
			}
			// 集計行に所定休日追加 20110121
			if ($reason == "23") {
				$sums[37] += 1;
			}
			// 年末年始
			if ($arr_type[$tmp_date] == "7" && $arr_timecard_holwk_day_std[0]["newyear_holiday"] == "t") {
				if ($reason == "61") {
					$sums[16] += 1;
				}
				if ($reason == "62" || $reason == "63" || $reason == "64") {
					$sums[16] += 0.5;
				}
			}
		}

		// 各時間帯の開始時刻・終了時刻を変数に格納
//		$tmp_type = ($type == "4" || $type == "5") ? "1" : $type;
		//type:"2勤務日1,3勤務日2"以外は"1通常"とする。"4法定休日,5所定休日,6祝日,7年末年始休暇"
		$tmp_type = ($type != "2" && $type != "3") ? "1" : $type;
/*
		for ($i = 1; $i <= 5; $i++) {
			$var_name_s = "start$i";
			$var_name_e = "end$i";
			$$var_name_s = $officehours[$pattern][$tmp_type]["start$i"];
			$$var_name_e = $officehours[$pattern][$tmp_type]["end$i"];
		}
*/
		$start2 = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "start2" );
		$end2   = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "end2" );
		$start4 = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "start4" );
		$end4   = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "end4" );
		$start5 = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "start5" );
		$end5   = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "end5" );

        //個人別所定時間を優先する場合 20120309
		if ($arr_empcond_officehours_ptn[$tmcd_group_id][$pattern] == 1) {
			$wk_weekday = $arr_weekday_flg[$tmp_date];
            $arr_empcond_officehours = $timecard_common_class->get_empcond_officehours($wk_weekday, $tmp_date); //個人別勤務時間帯履歴対応 20130220
			if ($arr_empcond_officehours["officehours2_start"] != "") {
				$start2 = $arr_empcond_officehours["officehours2_start"];
				$end2 = $arr_empcond_officehours["officehours2_end"];
				$start4 = $arr_empcond_officehours["officehours4_start"];
				$end4 = $arr_empcond_officehours["officehours4_end"];
			}
		}

		// 所定労働時間帯が確定できない場合、予定時刻を所定労働時間帯として使用
		if (($start2 == "" || $end2 == "") && $prov_start_time != "" && $prov_end_time != "") {
			$start2 = $prov_start_time;
			$end2 = $prov_end_time;
		}
		$over_calc_flg = false;	//残業時刻 20100525
		//残業時刻追加 20100114
		//残業開始が予定終了時刻より前の場合に設定
		$over_end_date_time = "";
		if ($over_start_time != "") {
			//残業開始日時
			if ($over_start_next_day_flag == "1") {
				$over_start_date = next_date($tmp_date);
			} else {
				$over_start_date = $tmp_date;
			}

			$over_start_date_time = $over_start_date.$over_start_time;
			$over_calc_flg = true;
			//残業終了時刻 20100705
			$over_end_date = ($over_end_next_day_flag == "1") ? next_date($tmp_date) : $tmp_date;
			$over_end_date_time = $over_end_date.$over_end_time;
		}
		// 実働時間をクリア、出勤時刻＜退勤時刻でない場合に前日の残業時間が表示されないようにする。
		$effective_time = 0;
		/*
		// 勤務時間修正申請情報を取得
		$modify_info = get_modify_info($con, $emp_id, $tmp_date, $fname);
		$modify_link_type = $modify_info["link_type"];
		$modify_apply_id = $modify_info["apply_id"];

		// 残業情報を取得
		$overtime_info = get_overtime_info($con, $emp_id, $ovtmscr_tm, $tmp_date, $start_time, $end_time, $start2, $end2, $fname);
		$overtime_link_type = $overtime_info["link_type"];
		$overtime_apply_id = $overtime_info["apply_id"];

		// 退勤後復帰申請情報を取得
		$return_info = get_return_info($con, $emp_id, $tmp_date, $ret_btn_flg, $o_start_time1, $fname);
		$return_link_type = $return_info["link_type"];
		$return_apply_id = $return_info["apply_id"];
*/
		//一括修正のステータスがある場合は設定
		$tmp_status = $arr_tmmdapplyall_status[$tmp_date]["apply_status"];
		if ($tmmd_apply_status == "" && $tmp_status != "") {
			$tmmd_apply_status = $tmp_status;
			$tmmd_apply_id = $arr_tmmdapplyall_status[$tmp_date]["apply_id"];
		}
		//tmmd_apply_statusからlink_type取得
		$modify_link_type = $timecard_common_class->get_link_type_modify($tmmd_apply_status);
		$modify_apply_id  = $tmmd_apply_id;

		// 残業情報を取得
		//ovtm_apply_statusからlink_type取得
		$night_duty_time_array  = array();
		$duty_work_time         = 0;
		if ($night_duty_flag){
			//当直時間の取得
			$night_duty_time_array = $timecard_common_class->get_duty_time_array($tmp_date, $end_time, $next_day_flag, $tmcd_group_id);

			//日数換算の数値ｘ通常勤務日の労働時間の時間
			$duty_work_time = date_utils::hi_to_minute($day1_time) * $timecard_common_class->get_night_workday_count($tmp_date, $end_time, $next_day_flag, $tmcd_group_id);
		}
//echo("d=$duty_work_time"); //debug
		$overtime_link_type = $timecard_common_class->get_link_type_overtime($tmp_date, $start_time, $previous_day_flag, $end_time, $next_day_flag, $night_duty, $start2, $end2, $ovtm_apply_status, $tmcd_group_id, $over_start_time, $over_end_time, $early_over_time, $over_24hour_flag, $over_start_time2, $over_end_time2, $over_start_next_day_flag2, $over_end_next_day_flag2);
		$overtime_apply_id  = $ovtm_apply_id;

		//rtn_apply_statusからlink_type取得
		$return_link_type = $timecard_common_class->get_link_type_return($ret_btn_flg, $o_start_time1, $rtn_apply_status);
		$return_apply_id  = $rtn_apply_id;
/*
		// 時間帯データを配列に格納
		$arr_time2 = create_time_array($start2, $end2);  // 所定労働
		$arr_time4 = create_time_array($start4, $end4);  // 休憩
*/
		// 所定労働・休憩開始終了日時の取得
        // 所定開始時刻が前日の場合 20121120
        if ($atdptn_previous_day_flag == "1") {
            $tmp_prev_date = last_date($tmp_date);
			$start2_date_time = $tmp_prev_date.$start2;
			//24時間以上勤務の場合 20100203
			if ($over_24hour_flag == "1") {
				$tmp_prev_date = $tmp_date;
			}
			$end2_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_prev_date, $start2, $end2);
		} else {
			$start2_date_time = $tmp_date.$start2;
			//24時間以上勤務の場合 20100203
			if ($over_24hour_flag == "1") {
				$tmp_end_date = next_date($tmp_date);
			} else {
				$tmp_end_date = $tmp_date;
			}
			$end2_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_end_date, $start2, $end2);
		}

		//休憩時刻追加、入力された場合時間帯の設定の代わりに使用 20100921
		if ($rest_start_time != "" && $rest_end_time != "") {
			$start4 = $rest_start_time;
			$end4 = $rest_end_time;
		}
		$start4_date_time = $tmp_date.$start4;
		$end4_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_date, $start4, $end4);

//echo("k=$start4_date_time $end4_date_time"); //debug

		//外出復帰日時の取得
		$out_date_time = $tmp_date.$out_time;
		$ret_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_date, $out_time, $ret_time);

		// 時間帯データを配列に格納
		$arr_time2 = $timecard_common_class->get_minute_time_array($start2_date_time, $end2_date_time);  // 所定労働
//		$arr_time4 = $timecard_common_class->get_minute_time_array($start4_date_time, $end4_date_time);  // 休憩
		// 前日出勤フラグ、翌日退勤フラグを考慮した休憩時間配列を取得
		// 24時間超える場合の翌日の休憩は配列にいれない 20101027
		if ($start2 < $start4) {
			$arr_time4 = $timecard_common_class->get_minute_time_array2($start4_date_time, $end4_date_time, $previous_day_flag, "");  // 休憩
		} else {
			$arr_time4 = $timecard_common_class->get_minute_time_array2($start4_date_time, $end4_date_time, $previous_day_flag, $next_day_flag);  // 休憩
		}

		//時間帯情報 休憩、深夜、前日当日翌日分の開始終了時刻を保持 20110126
		if ($tmp_date >= $start_date) {
			$work_times_info = $timecard_common_class->get_work_times_info($tmp_date, $start4, $end4, $start5, $end5);
		}
		// 処理当日の所定労働時間を求める
		if ($holiday_name != "" && $hol_minus == "t") {  // 祝日かつ祝日減算設定
			$specified_time = 0;
			//		} else if ($wage == "4" && $others3 == "2") {  // 時間給者かつ休憩を実働加算 集計処理の設定を「集計しない」の固定とする 20100910
			//			$specified_time = count($arr_time2);
		} else {
			$specified_time = count(array_diff($arr_time2, $arr_time4));
		}

// 基準時間の計算を変更 2008/10/21
		// 変則労働期間が月でない場合、所定労働時間を基準時間としてカウント
//		if ($tmp_date >= $start_date && $reason != "16") {
//			if ($arr_irrg_info["irrg_type"] != "2") {
//				$sums[12] += $specified_time;
				//当直の場合稼働時間に日数換算の数値ｘ通常勤務日の労働時間の時間を追加
//				if ($night_duty_flag){
//					$sums[12] += $duty_work_time;
//				}
//			}
//		}

		// 出退勤とも打刻済みの場合
        //勤務時間を計算しないフラグが"1"以外 20130225
        if ($start_time != "" && $end_time != "" && $no_count_flag != "1") {

			$start_date_time = $timecard_common_class->get_timecard_start_date_time($tmp_date, $start_time, $previous_day_flag);
			$end_date_time   = $timecard_common_class->get_timecard_end_date_time($tmp_date, $end_time, $next_day_flag);

//echo("start_date_time=$start_date_time ");

			// ここまでの処理で所定労働時間帯が未確定の場合、勤務実績で上書き
/*
			if ($start2 == "" || $end2 == "") {
				$start2 = $start_time;
				$end2 = $end_time;
			}
*/
			if (empty($start2_date_time) || empty($end2_date_time)) {
				$start2_date_time  = $start_date_time;
				$end2_date_time    = $end_date_time;
			}

			$overtime_approve_flg = $timecard_common_class->get_overtime_approve_flg($overtime_link_type, $modify_flg, $modify_link_type);

			//未申請を表示しない設定追加 20100809
			//休暇残業の場合の残業時間計算 20101228 //明けを追加 20110819
			if (($pattern == "10" || $after_night_duty_flag == "1") && $over_start_time != "" && $over_end_time != "") {
				$diff_min_end_time = date_utils::get_diff_minute($over_end_date_time ,$over_start_date_time);
			}
			else {
				if ($end_date_time > $end2_date_time) {
					$diff_min_end_time = date_utils::get_diff_minute($end_date_time ,$end2_date_time);
				} else {
					$diff_min_end_time = "";
				}
			}
			// 残業管理をする場合
			if ($no_overtime != "t") {
				// 残業申請中の場合は定時退勤時刻を退勤時刻として計算
//				if (!($overtime_link_type == "0" || $overtime_link_type == "3" || $overtime_link_type == "4")) {
				// 残業未承認か確認
				if ($overtime_approve_flg == "1" || ($return_link_type != "0" && $return_link_type != "3")) {
					if (date_utils::to_timestamp_from_ymdhi($start2_date_time) > date_utils::to_timestamp_from_ymdhi($start_date_time)) { //所定より出勤が早い場合には所定にする 20100914 時刻に:があるため遅刻時間の表示されない不具合 20101005
						$start_date_time  = $start2_date_time; //出勤時間、早出残業の場合
					}
					if ($end_date_time > $end2_date_time) {
						$end_date_time = $end2_date_time;
					}
				}
			}
//            echo "overtime_approve_flg=$overtime_approve_flg return_link_type=$return_link_type start_date_time=$start_date_time start2_date_time=$start2_date_time<br>\n";
            
//echo("start_date_time=$start_date_time ");
			// 出勤〜退勤までの時間を配列に格納
			$arr_tmp_work_time = $timecard_common_class->get_minute_time_array($start_date_time, $end_date_time);
			//出勤同時刻退勤または、出勤時間＞退勤時間の場合は処理を行わない
			if (count($arr_tmp_work_time) > 0){
/*
				// 時間帯データを配列に格納
				switch ($reason) {
				case "2":  // 午前有休
				case "38": // 午前年休
					$arr_time2 = $timecard_common_class->get_minute_time_array($start4_date_time, $end2_date_time);  // 休憩後の所定労働
					break;
				case "3":  // 午後有休
				case "39": // 午後年休
					$arr_time2 = $timecard_common_class->get_minute_time_array($start2_date_time, $end4_date_time);  // 休憩前の所定労働
					break;
				default:
*/
					$arr_time2 = $timecard_common_class->get_minute_time_array($start2_date_time, $end2_date_time);  // 所定労働
//					break;
//				}
//echo("$tmp_date 1=$start2_date_time $end2_date_time"); //debug

				//休日で残業がある場合 20101028
				if ($pattern == "10" && $over_start_time != "" && $over_end_time != "") {
					//深夜残業の開始終了時間
					$start5 = "2200";
					$end5 = "0500";
				}
				//前日・翌日の深夜帯も追加する
				$arr_time5 = $timecard_common_class->get_late_night_shift_array($tmp_date, $start5, $end5);  // 深夜帯

				// 確定出勤時刻／遅刻回数を取得
				// 勤務パターンで時刻指定がない場合でも、端数処理する 20090605
				if ($start2 == "" && $end2 == "" && $fraction1 > "1") {
					//端数処理する分
					$moving_minutes = $timecard_common_class->get_moving_minutes($fraction1_min);
					//基準日時
					$tmp_office_start_time = substr($start_date_time, 0, 10) . "00";
					$fixed_start_time = date_utils::move_time($tmp_office_start_time, $start_date_time, $moving_minutes, $fraction1);
					$start_time_info = array();
				} else {
					$start_time_info = $timecard_common_class->get_start_time_info($arr_tmp_work_time, $arr_time2, $arr_time4, $fraction1, $fraction1_min, $fraction3, $others4, $others5);
					$fixed_start_time = $start_time_info["fixed_time"];
				}

				// 確定退勤時刻／早退回数を取得
				// 勤務パターンで時刻指定がない場合でも、端数処理する 20090605
				if ($start2 == "" && $end2 == "" && $fraction2 > "1") {
					//端数処理する分
					$moving_minutes = $timecard_common_class->get_moving_minutes($fraction2_min);
					//基準日時
					$tmp_office_start_time = substr($start_date_time, 0, 10) . "00";
					$fixed_end_time = date_utils::move_time($tmp_office_start_time, $end_date_time, $moving_minutes, $fraction2);
//echo("fixed_end_time=$fixed_end_time");
					$end_time_info = array();
				} else {
					//残業時刻が入っている場合は、端数処理しない 20100806
					$wk_over_end_date_time = str_replace(":", "", $over_end_date_time); //20111128
					if ($over_end_time != "" && $wk_over_end_date_time > $end2_date_time) { //時間が所定終了よりあとか確認20111128
						$fixed_end_time = $wk_over_end_date_time;
					} else {
						$end_time_info = $timecard_common_class->get_end_time_info($arr_tmp_work_time, $arr_time2, $arr_time4, $fraction2, $fraction2_min, $fraction3, $others4);
						$fixed_end_time = $end_time_info["fixed_time"];
					}
					//※残業申請未承認の場合と（残業申請不要かつ残業管理をする場合）に退勤時刻を所定にする 20100721
					//残業申請画面を表示しないは除く 20100811
					if ($timecard_bean->over_time_apply_type != "0") {
						if ($overtime_approve_flg == "1" || ($overtime_approve_flg == "0" && $no_overtime != "t")) {
							//退勤時刻が所定よりあとの場合設定 20101224
							if ($fixed_end_time > $end2_date_time) {
								$fixed_end_time = $end2_date_time;
							}
						}
					}
				}
				//時間有休がある場合は、遅刻早退を計算しない 20111207
				if ($obj_hol_hour->paid_hol_hour_flag == "t" && $paid_time_hour > 0) {
					//遅刻
					$tikoku_time = 0;
					$start_time_info["diff_minutes"] = 0;
					$start_time_info["diff_count"] = 0;
					//早退
					$sotai_time = 0;
					$end_time_info["diff_minutes"] = 0;
					$end_time_info["diff_count"] = 0;
				}
				if ($tmp_date >= $start_date) {
					//遅刻
					$sums[10] += $start_time_info["diff_count"];
					$delay_time += $start_time_info["diff_minutes"]; //遅刻時間 20100907
					//早退
					$sums[11] += $end_time_info["diff_count"];
					$early_leave_time += $end_time_info["diff_minutes"];
				}
//echo("fixed_start_time=$fixed_start_time fixed_end_time=$fixed_end_time");
				// 実働時間・所定労働時間を配列に格納（休憩を除く）・有休時間を算出

				switch ($reason) {
				case "2":  // 午前有休
				case "38": // 午前年休
						// 残業時間が正しく計算されないため、休憩終了時間設定をしない 20100330
						//if (in_array($fixed_start_time, $arr_time2)) {
						$tmp_start_time = $fixed_start_time;
					//} else {
//						$tmp_start_time = $end4;
						//	$tmp_start_time = $end4_date_time; //休憩終了時間
					//	}
					$tmp_end_time = $fixed_end_time;

//					$arr_effective_time = create_time_array($tmp_start_time, $tmp_end_time);
					$arr_effective_time = $timecard_common_class->get_minute_time_array($tmp_start_time, $tmp_end_time);

					$arr_specified_time = $arr_time2;

//					$paid_time = count(create_time_array($start2, $start4));
//					$paid_time = date_utils::get_time_difference($start2_date_time, $start4_date_time);

					//休憩を引く前の実働時間
					$arr_effective_time_wk = $arr_effective_time;
					break;
				case "3":  // 午後有休
				case "39": // 午後年休
					$tmp_start_time = $fixed_start_time;
						// 残業時間が正しく計算されないため、休憩開始時間設定をしない 20100330
						//if (in_array($fixed_end_time, $arr_time2)) {
						$tmp_end_time = $fixed_end_time;
					//} else {
//						$tmp_end_time = $start4;
						//	$tmp_end_time = $start4_date_time; //休憩開始時間
					//	}
//					$arr_effective_time = create_time_array($tmp_start_time, $tmp_end_time);
					$arr_effective_time = $timecard_common_class->get_minute_time_array($tmp_start_time, $tmp_end_time);

					$arr_specified_time = $arr_time2;

//					$paid_time = count(create_time_array($end4, $end2));
//					$paid_time = date_utils::get_time_difference($end4_date_time, $end2_date_time);

					//休憩を引く前の実働時間
					$arr_effective_time_wk = $arr_effective_time;
					break;
				default:
					$tmp_start_time = $fixed_start_time;
					$tmp_end_time = $fixed_end_time;
//					$arr_effective_time = create_time_array($tmp_start_time, $tmp_end_time);
					$arr_effective_time = $timecard_common_class->get_minute_time_array($tmp_start_time, $tmp_end_time);
					//休憩を引く前の実働時間
					$arr_effective_time_wk = $arr_effective_time;
					if ($wage != "4" || ($start4 != "" && $end4 != "")) { //休憩時刻がある場合を追加 20100925
							$arr_effective_time = array_diff($arr_effective_time, $arr_time4);
					}

					$arr_specified_time = array_diff($arr_time2, $arr_time4);

//					$paid_time = 0;

					break;
				}
				//残業時刻追加対応 20100114
				//予定終了時刻から残業開始時刻の間を除外する
				if ($over_start_time != "") {
					$wk_end_date_time = $end2_date_time;

					$arr_jogai = array();
					/* 20100713
					//基準日時
					$tmp_office_start_time = substr($end2_date_time, 0, 10) . "00";
					if ($fraction1 > "1") {
						//端数処理する分
						$moving_minutes = $timecard_common_class->get_moving_minutes($fraction1_min);
						$over_start_date_time = date_utils::move_time($tmp_office_start_time, $over_start_date_time, $moving_minutes, $fraction1);
					}
					if ($fraction2 > "1") {
						//端数処理する分
						$moving_minutes = $timecard_common_class->get_moving_minutes($fraction2_min);
						$wk_end_date_time = date_utils::move_time($tmp_office_start_time, $wk_end_date_time, $moving_minutes, $fraction2);
					}
					*/
					//残業開始時刻があとの場合
					if (date_utils::to_timestamp_from_ymdhi($over_start_date_time) > date_utils::to_timestamp_from_ymdhi($wk_end_date_time)) {
						//時間配列を取得する（休憩扱い）
						$arr_jogai = $timecard_common_class->get_minute_time_array($wk_end_date_time, $over_start_date_time);
						//休憩扱いの時間配列を除外する
						$arr_effective_time = array_diff($arr_effective_time, $arr_jogai);
					}

				}

				//当直の場合当直時間を計算から除外する
				if ($night_duty_flag){
					//稼働 - (当直 - 所定労働)
					$arr_effective_time = array_diff($arr_effective_time, array_diff($night_duty_time_array, $arr_time2));
				}
//$debug1 = count($arr_time2);
//echo("$tmp_date 1=$debug1"); //debug

				// 普外時間を算出（分単位）
				// 外出時間を差し引かないフラグ対応 20110727
				if ($out_time_nosubtract_flag != "1") {
					$arr_out_time = $timecard_common_class->get_minute_time_array($out_date_time, $ret_date_time);
					$time3 = count(array_intersect($arr_out_time, $arr_specified_time));

					// 残外時間を算出（分単位）
					$time4 = count(array_diff($arr_out_time, $arr_time2)); //$arr_specified_timeから変更、休憩も除くため 20100910
				}

				// 実働時間を算出（分単位）
				$effective_time = count($arr_effective_time);
//$debug1 = $effective_time; //debug
				$time3_rest = 0;
				$time4_rest = 0;
				if ($wage == "4" && $start4 == "" && $end4 == "") {  // 時間給かつ休憩時刻指定がない場合 20100925
					if ($effective_time > 1200) {  // 20時間を超える場合 20100929
						$effective_time -= $rest7;
						$time3_rest = 0;
						$time4_rest = $rest7;
					} else if ($effective_time > 720) {  // 12時間を超える場合
						$effective_time -= $rest6;
						$time3_rest = 0;
						$time4_rest = $rest6;
					} else if ($effective_time > 540) {  // 9時間を超える場合
						$effective_time -= $rest3;
						$time3_rest = 0;
						$time4_rest = $rest3;
					} else if ($effective_time > 480) {  // 8時間を超える場合
						$effective_time -= $rest2;
						$time3_rest = $rest2;
						$time4_rest = 0;
					} else if ($effective_time > 360) {  // 6時間を超える場合
						$effective_time -= $rest1;
						$time3_rest = $rest1;
						$time4_rest = 0;
					} else if ($effective_time > 240 && $start_time < "12:00") {  // 4時間超え午前開始の場合
						$effective_time -= $rest4;
						$time3_rest = $rest4;
						$time4_rest = 0;
					} else if ($effective_time > 240 && $start_time >= "12:00") {  // 4時間超え午後開始の場合
						$effective_time -= $rest5;
						$time3_rest = $rest5;
						$time4_rest = 0;
					}
					//休日残業で残業申請中は休憩を表示しない 20100916
					if ($legal_hol_over_flg /*&& $overtime_approve_flg == "1"*/) {
						$time3_rest = 0;
						$time4_rest = 0;
					}
					//休日で残業がない場合,出勤退勤がある場合 20100917
					if ($pattern == "10" && $over_start_time == "" && $over_end_time == "") {
						$time3_rest = 0;
						$time4_rest = 0;
					}
					//休日の時間＞平日時間の場合に休日勤務から休憩を減算
//					if ($arr_hol_time[3] > ($wk_effective_time - $arr_hol_time[3])) {
					//開始した日が休日の場合に休日勤務から休憩を減算 2008/10/23
					//if (($previous_day_flag == true && $arr_hol_time[0] > 0) ||
					//	($previous_day_flag == false && $arr_hol_time[1] > 0)) {
					//	$sums[31] = $sums[31] - $time3_rest - $time4_rest;
					//}
				}
				//古い仕様のため廃止 20100330
				/*
				// 代替出勤または振替出勤の場合、実働時間から所定労働時間を減算
				if ($reason == "14" || $reason == "15") {
					$effective_time -= count($arr_specified_time);
				}
				*/
// debug
//echo("effective_time=$effective_time  paid_time=$paid_time time3=$time3  time4=$time4<br>\n");
				// 稼働時間を算出（分単位）
//				$work_time = $effective_time + $paid_time - $time3 - $time4;
				// 半年休の時間をプラスしない 20100330
				$work_time = $effective_time - $time3 - $time4;
				//echo("$tmp_date 1=$debug1 e=$effective_time p=$paid_time 3=$time3 4=$time4 r=$rest3"); //debug
				//休暇時は計算しない 20090806 公休か事由なし休暇で残業時刻が設定済を条件追加 20100802
				if ($pattern == "10" && !$legal_hol_over_flg) {
					$work_time = 0;
				}

				// 退勤後復帰回数・時間を算出
				$return_count = 0;
				$return_time = 0;
				$return_late_time = 0; //20100715
                //申請状態 "0":申請不要 "3":承認済 //勤務時間を計算しないフラグ確認 20130225
                if (($return_link_type == "0" || $return_link_type == "3") && $no_count_flag != "1") {
					for ($i = 1; $i <= 10; $i++) {
						$start_var = "o_start_time$i";
						if ($$start_var == "") {
							break;
						}
						$return_count++;

						$end_var = "o_end_time$i";
						if ($$end_var == "") {
							break;
						}

						//端数処理 20090709
						if ($fraction1 > "1") {
							//端数処理する分
							$moving_minutes = $timecard_common_class->get_moving_minutes($fraction1_min);
							//開始日時
							$tmp_start_date_time = $timecard_common_class->get_schedule_end_date_time(substr($end_date_time,0,8), substr($end_date_time,8,4), $$start_var);
							//基準日時
							$tmp_base_date_time = substr($tmp_start_date_time,0,10)."00";
							$tmp_date_time = date_utils::move_time($tmp_base_date_time, $tmp_start_date_time, $moving_minutes, $fraction1);
							$tmp_ret_start_time = substr($tmp_date_time, 8, 4);
						} else {
							$tmp_ret_start_time = str_replace(":", "", $$start_var);
						}
						if ($fraction2 > "1") {
							//端数処理する分
							$moving_minutes = $timecard_common_class->get_moving_minutes($fraction2_min);
							//終了日時
							$tmp_end_date_time = $timecard_common_class->get_schedule_end_date_time(substr($end_date_time,0,8), substr($end_date_time,8,4), $$end_var);
							//基準日時
							$tmp_base_date_time = substr($tmp_end_date_time,0,10)."00";
							$tmp_date_time = date_utils::move_time($tmp_base_date_time, $tmp_end_date_time, $moving_minutes, $fraction2);

							$tmp_ret_end_time = substr($tmp_date_time, 8, 4);
						} else {
							$tmp_ret_end_time = str_replace(":", "", $$end_var);
						}
						$arr_ret = $timecard_common_class->get_return_array($end_date_time, $tmp_ret_start_time, $tmp_ret_end_time);

						//echo("$end_date_time $tmp_ret_start_time $tmp_ret_end_time "); //debug

						//					$arr_ret = $timecard_common_class->get_return_array($end_date_time, $$start_var, $$end_var);
						$return_time += count($arr_ret);

						// 月の深夜勤務時間に退勤後復帰の深夜分を追加
						//$return_late_time += count(array_intersect($arr_ret, $arr_time5)); //20100715
						//計算方法変更 20110126
						//深夜残業
						$wk_ret_start = $tmp_date.$tmp_ret_start_time;
						$wk_ret_end = $timecard_common_class->get_schedule_end_date_time($tmp_date, $tmp_ret_start_time, $tmp_ret_end_time);
						$wk_late_night = $timecard_common_class->get_late_night($work_times_info, $wk_ret_start,$wk_ret_end, "");
						$return_late_time += $wk_late_night;
						//当日が休日の場合、休日に合計。
						//休日判断変更（勤務パターンに関わらず、カレンダーと休日設定による） 20110228
						if ((check_timecard_holwk(
										$arr_timecard_holwk_day, $arr_type[$tmp_date], $pattern, $reason, "", $timecard_bean) == true) ||
								$reason == "16") {
							//当日の残業
							$today_over = $timecard_common_class->get_day_time($work_times_info, $wk_ret_start,$wk_ret_end, "1");
							//事由が普通残業の場合を除く 20110128
							//if ($reason != "71") {
								$sums[31] += $today_over;
							//}
						}
						//翌日の残業
						$next_date = next_date($tmp_date);
                        if (check_timecard_holwk(
                                    $arr_timecard_holwk_day, $arr_type[$next_date], $arr_atdbkrslt[$next_date]["pattern"], $arr_atdbkrslt[$next_date]["reason"], "1", $timecard_bean) == true) { //20130308
							$nextday_over = $timecard_common_class->get_day_time($work_times_info, $wk_ret_start,$wk_ret_end, "2");
							//事由が普通残業の場合を除く 20110128
							//if ($reason != "71") {
								$sums[31] += $nextday_over;
							//}
						}
					}
				}
				$wk_return_time = $return_time;
				//残業時刻が入力された場合、重複するため、退勤後復帰時間を加算しない。 20100714 不要 20100910
				//if ($over_calc_flg) {
				//	$wk_return_time = 0; //集計用 $return_time 表示用
				//	$return_late_time = 0; //20100715
				//}

				// 前日までの計 = 週計
				$last_day_total = $work_time_for_week;

				$work_time_for_week += $work_time;
				$work_time_for_week += $wk_return_time;

				if ($tmp_date < $start_date) {
					$tmp_date = next_date($tmp_date);
					$counter_for_week++;
					continue;
				}
				//$sums[13] += $work_time; // 20100910

				//当直の場合稼働時間に日数換算の数値ｘ通常勤務日の労働時間の時間を追加（当直の時間を除外して計算している為)古い仕様
				//if ($night_duty_flag){
				//	$sums[13] += $duty_work_time;
				//}

/* 残業時間の処理を変更 20090708
				// 残業時間を算出（分単位）
				if ($arr_irrg_info["irrg_type"] == "") {  // 変則労働適用外の場合

					$tmp_specified_time = $specified_time;
					// 遅刻時間を残業時間から減算しない場合（所定時間から引くことで合わせる）
					if ($delay_overtime_flg == "2") {
						$tmp_specified_time = $specified_time - $start_time_info["diff_minutes"];
					}

					// 日々の表示値は普外・残外の減算前
//					$time1 = ($effective_time > 480) ? $effective_time - 480 : 0;
					$time1 = 0;
					//休暇で、当直時は計算しない 20090703
					if ($pattern != "10" || $night_duty != "1") {
						if ($effective_time > $tmp_specified_time) {
							$time1 = $effective_time - $tmp_specified_time;
						} else {
							$time1 = 0;
						}
						$sums[14] += $time1;
						if ($others1 == "4") {
							$sums[14] -= $time3;
						}
						if ($others2 == "4") {
							$sums[14] -= $time4;
						}
					}
					$wk_hoteigai = $time1;
					$hoteigai += $wk_hoteigai;

				} else if ($arr_irrg_info["irrg_type"] == "1") {  // 変則労働期間・週の場合
					if ($counter_for_week == 7) {
						$tmp_irrg_minutes = $arr_irrg_info["irrg_minutes"];
						if ($hol_minus == "t") {
							$tmp_irrg_minutes -= (8 * 60 * $holiday_count);
						}
						$time1 = ($work_time_for_week > $tmp_irrg_minutes) ? $work_time_for_week - $tmp_irrg_minutes : 0;
						$sums[14] += $time1;
					} else {
						$time1 = "";
					}
				}
*/

				// 深夜勤務時間を算出（分単位）
				//残業開始、終了時刻がある場合 20101027
/*
				if ($over_start_time != "" && $over_end_time != "") {
					$arr_over_time = $timecard_common_class->get_minute_time_array($over_start_date_time,$over_end_date_time);
					$time2 = count(array_intersect($arr_over_time, $arr_time5));
				} else {
					$time2 = count(array_intersect($arr_effective_time, $arr_time5));
				}

*/
				$time2 = 0;
				//計算方法変更 20110126
				//残業承認時に計算、申請画面を表示しない場合は無条件に計算
				if (($timecard_bean->over_time_apply_type != "0" && $overtime_approve_flg == "2") ||
						$timecard_bean->over_time_apply_type == "0") {
					//残業時刻がない場合で、所定終了、退勤を比較後残業計算
					if ($over_start_time == "" || $over_end_time == "") {
                        //if ($end2_date_time < $fixed_end_time) { //退勤時刻を使用しない 20120406
						//	$over_start_date_time = $end2_date_time;
						//	$over_end_date_time = $fixed_end_time;
						//} else {
							$over_start_date_time = ""; //20101109
							$over_end_date_time = "";
						//}
					}
					//深夜残業
					$time2 = $timecard_common_class->get_late_night($work_times_info, $over_start_date_time,$over_end_date_time, "");
                    //休憩対応 20130827
                    if ($time2 > 0 && $start4 != "" && $end4 != "") {
                        $wk_rest_time = $timecard_common_class->get_intersect_times($over_start_date_time, $over_end_date_time, $start4_date_time, $end4_date_time);
                        if ($wk_rest_time > 0) {
                            $late_night_info = $timecard_common_class->get_late_night_shift_info($tmp_date, $start5, $end5);  // 深夜帯情報
                            //深夜時間帯か再確認
                            $wk_s1 = max($over_start_date_time, $start4_date_time);
                            $wk_e1 = min($over_end_date_time, $end4_date_time);
                            $wk_rest_time = $timecard_common_class->get_times_info_calc($late_night_info, $wk_s1, $wk_e1);
                            $time2 -= $wk_rest_time;
                        }
                    }
                    //休日残業
					//当日
                    $wk_hol_flg = check_timecard_holwk(
                            $arr_timecard_holwk_day, $arr_type[$tmp_date], $pattern, $reason, "", $timecard_bean); //休日フラグ 20110228
					if (($pattern != "10" && $wk_hol_flg == true) ||
							$reason == "16" ||
							($pattern == "10" && $over_start_time != "" && $over_end_time != "" &&
                                $overtime_approve_flg != "1" && (($reason != "71"
                                        && $reason != "24") || ($reason == "24" && $timecard_bean->no_hol_overtime_flg == "f"))) //休暇で残業時刻がある場合 20110228
						) {
						$today_over = $timecard_common_class->get_day_time($work_times_info, $over_start_date_time,$over_end_date_time, "1");
						//休憩時間
						if ($wage == "4" && $start4 == "" && $end4 == "") {  // 時間給かつ休憩時刻指定がない場合
							//休憩を減算
                                
                            //$today_over = $today_over - $time3_rest - $time4_rest; //コメント化 20130822
                            ;
						} else {
                            //休憩 20130822
                   			//出勤時刻と残業開始時刻が一致している場合、所定、遅刻、早退を計算せず、残業、休憩のみ計算する
                            if ($start2 != "" && $start2 == $over_start_time && $over_start_next_day_flag != "1") {//残業開始が翌日でないことを追加
                                $overtime_only_flg = true;
                            }
                            else {
                                $overtime_only_flg = false;
                            }
							//休憩時間確認
							if ($start4 != "" && $end4 != "") {
                                if ($legal_hol_over_flg || $overtime_only_flg) {
                                    
                                    $wk_kyukei = $timecard_common_class->get_intersect_times($over_start_date_time, $over_end_date_time, $start4_date_time, $end4_date_time);
                                    if ($wk_kyukei > 0) {
                                        //日勤時間帯か再確認
                                        $wk_s1 = max($over_start_date_time, $start4_date_time);
                                        $wk_e1 = min($over_end_date_time, $end4_date_time);
                                        $wk_rest_time = $timecard_common_class->get_day_time($work_times_info, $wk_s1,$wk_e1, "1");
                                        $today_over -= $wk_rest_time;
                                    }
                                }
							}
						}
						//事由が普通残業の場合を除く 20110128
						//if ($reason != "71") {
							$sums[31] += $today_over;
						//}
					}
					//翌日フラグ
					if ($next_day_flag == "1" || $over_end_next_day_flag == "1") {
						$next_date = next_date($tmp_date);
                        if (check_timecard_holwk(
                                    $arr_timecard_holwk_day, $arr_type[$next_date], $arr_atdbkrslt[$next_date]["pattern"], $arr_atdbkrslt[$next_date]["reason"], "1", $timecard_bean) == true) { //20130308
							$nextday_over = $timecard_common_class->get_day_time($work_times_info, $over_start_date_time,$over_end_date_time, "2");
							//事由が普通残業の場合を除く 20110128
							//if ($reason != "71") {
								$sums[31] += $nextday_over;
							//}
						}
					}

					//残業２
					if ($over_start_time2 != "" && $over_end_time2 != "") {
						//開始日時
						$over_start_date_time2 = ($over_start_next_day_flag2 == 1) ? next_date($tmp_date).$over_start_time2 : $tmp_date.$over_start_time2;
						//終了日時
						$over_end_date_time2 = ($over_end_next_day_flag2 == 1) ? next_date($tmp_date).$over_end_time2 : $tmp_date.$over_end_time2;
						//深夜時間帯の時間取得
						$over_time2_late_night = $timecard_common_class->get_late_night($work_times_info, $over_start_date_time2, $over_end_date_time2, "");
						$time2 += $over_time2_late_night;
					}

				}
				$time2 += $return_late_time; //20100715
				$sums[15] += $time2;


				// 月の稼働時間・残業時間に退勤後復帰分の残業時間を追加
				//$sums[13] += $wk_return_time; //20100714 20100910
//				$sums[14] += $return_time;

				// 表示値の編集
				$time1 = minute_to_hmm($time1);
				$time2 = minute_to_hmm($time2);
				// 普外−＞外出に変更 20090603
				//$time3 = minute_to_hmm($time3 + $time3_rest);
				$time_outtime = minute_to_hmm($time3);
				if ($time4 > 0) {
					$time_outtime .= "(".minute_to_hmm($time4).")";
				}
				// 残外−＞休憩に変更 20090603
				if ($wage == "4" && $start4 == "" && $end4 == "") {  // 時間給かつ休憩時刻指定がない場合 20100925
					$time_rest = minute_to_hmm($time3_rest);
					if ($time4_rest > 0) {
						$time_rest .= "(".minute_to_hmm($time4_rest).")";
					}
				} else {
                    // 実働と休憩の配列から休憩時間を算出
					// 所定労働内の休憩時間
					$tmp_rest1 = count(array_intersect($arr_time2, $arr_time4));
					// 実働時間内の休憩時間
					$tmp_rest2 = count(array_intersect($arr_effective_time_wk, $arr_time4));
					// 所定労働外（残業時間内）の休憩時間
					$tmp_rest3 = $tmp_rest2 - $tmp_rest1;
					$time_rest = minute_to_hmm($tmp_rest1);
					if ($tmp_rest3 > 0) {
						$time_rest .= "(".minute_to_hmm($tmp_rest3).")";
					}
					//休憩時間前後の遅刻早退対応 20100925
					if ($tmp_rest1 > $tmp_rest2) {
						$tmp_rest2 = $tmp_rest1;
						$tmp_rest1 = 0;
						$time_rest = "";
					}

				}
				$return_time = minute_to_hmm($return_time);
                // 回数追加 20121109
                if ($return_count != "") {
                    $return_time .= " ".$return_count;
                }

				// 出退勤時刻の表示設定
				$time_cell_class = "tm";
				if ($show_time_flg != "t") {
					$time_cell_class = "mark";
					$start_time = ($start_time_info["diff_count"]) ? "遅刻" : "○";
					$show_start_time = ($start_time_info["diff_count"]) ? "遅刻" : "○";
					$show_end_time = ($end_time_info["diff_count"]) ? "早退" : "○";
				}
			} else {
			// 出勤同時刻退勤または、出勤時間＞退勤時間の場合で
			// 月の開始日前は処理しない
				if ($tmp_date < $start_date) {
					$tmp_date = next_date($tmp_date);
					$counter_for_week++;
					continue;
				}
			}
		} else {
			$time1 = "";
			$time2 = "";
			$time3 = "";
			$time4 = "";
			$time_outtime = "";
			$time_rest = "";
			$return_time = "";

			// 出退勤時刻の表示設定
			$time_cell_class = "tm";
			if ($show_time_flg != "t") {
				$time_cell_class = "mark";

				$start_date_time = $timecard_common_class->get_timecard_start_date_time($tmp_date, $start_time, $previous_day_flag);
				$end_date_time   = $timecard_common_class->get_timecard_end_date_time($tmp_date, $end_time, $next_day_flag);

				if (empty($start2_date_time) || empty($end2_date_time)) {
					$start2_date_time  = $start_date_time;
					$end2_date_time    = $end_date_time;
				}

				switch ($reason) {
				case "2":  // 午前有休
				case "38": // 午前年休
					$arr_time2 = $timecard_common_class->get_minute_time_array($start4_date_time, $end2_date_time);  // 休憩後の所定労働
					break;
				case "3":  // 午後有休
				case "39": // 午後年休
					$arr_time2 = $timecard_common_class->get_minute_time_array($start2_date_time, $end4_date_time);  // 休憩前の所定労働
					break;
				default:

					$arr_time2 = $timecard_common_class->get_minute_time_array($start2_date_time, $end2_date_time);  // 所定労働
					break;
				}

				if ($start_time != "") {
//					if ($end2 == "") {
					if ($end2_date_time == "") {
						$start_time = "○";
						$show_start_time = "○";
					} else {
						$arr_tmp_work_time = $timecard_common_class->get_minute_time_array($start_date_time, $end2_date_time);
						$start_time_info = $timecard_common_class->get_start_time_info($arr_tmp_work_time, $arr_time2, $arr_time4, $fraction1, $fraction1_min, $fraction3, $others4, $others5);
						$start_time = ($start_time_info["diff_count"]) ? "遅刻" : "○";
						$show_start_time = ($start_time_info["diff_count"]) ? "遅刻" : "○";
					}
				} else if ($end_time != "") {
//					if ($start2 == "") {
					if ($start2_date_time == "") {
						$show_end_time = "○";
					} else {
						$arr_tmp_work_time = $timecard_common_class->get_minute_time_array($start2_date_time, $end_date_time);
						$end_time_info = $timecard_common_class->get_end_time_info($arr_tmp_work_time, $arr_time2, $arr_time4, $fraction2, $fraction2_min, $fraction3, $others4);
						$show_end_time = ($end_time_info["diff_count"]) ? "早退" : "○";
					}
				}
			}

			// 事由が「代替休暇」「振替休暇」の場合、
			// 所定労働時間を稼働時間にプラス
			// 「有給休暇」を除く $reason == "1" || 2008/10/14
			if ($reason == "4" || $reason == "17") {
				if ($tmp_date >= $start_date) {
					$sums[13] += $specified_time;
					$work_time_for_week += $specified_time;
				}
			}
			// 退勤後復帰回数を算出
			$return_count = 0;
			// 退勤後復帰時間を算出 20100413
			$return_time = 0;
            //申請状態 "0":申請不要 "3":承認済 ※未承認時は計算しない //勤務時間を計算しないフラグ確認 20130225
            if (($return_link_type == "0" || $return_link_type == "3") && $no_count_flag != "1") {
			for ($i = 1; $i <= 10; $i++) {
				$start_var = "o_start_time$i";
				if ($$start_var == "") {
					break;
				}
				$return_count++;

				$end_var = "o_end_time$i";
				if ($$end_var == "") {
					break;
				}

				//端数処理
				if ($fraction1 > "1") {
					//端数処理する分
					$moving_minutes = $timecard_common_class->get_moving_minutes($fraction1_min);
					//開始日時
					$tmp_start_date_time = $tmp_date.$$start_var;
					//基準日時
					$tmp_base_date_time = substr($tmp_start_date_time,0,10)."00";
					$tmp_date_time = date_utils::move_time($tmp_base_date_time, $tmp_start_date_time, $moving_minutes, $fraction1);
						$tmp_ret_start_time = str_replace(":", "",substr($tmp_date_time, 8, 5));
				} else {
					$tmp_ret_start_time = str_replace(":", "", $$start_var);
				}
				if ($fraction2 > "1") {
					//端数処理する分
					$moving_minutes = $timecard_common_class->get_moving_minutes($fraction2_min);
					//終了日時
					$tmp_end_date_time = $tmp_date.$$end_var;
					//基準日時
					$tmp_base_date_time = substr($tmp_end_date_time,0,10)."00";
					$tmp_date_time = date_utils::move_time($tmp_base_date_time, $tmp_end_date_time, $moving_minutes, $fraction2);

					$tmp_ret_end_time = str_replace(":", "",substr($tmp_date_time, 8, 5));
				} else {
					$tmp_ret_end_time = str_replace(":", "", $$end_var);
				}
					$arr_ret = $timecard_common_class->get_return_array($tmp_date.$tmp_ret_start_time, $tmp_ret_start_time, $tmp_ret_end_time);

				$return_time += count($arr_ret);

				//前日・翌日の深夜帯
				//$arr_time5 = $timecard_common_class->get_late_night_shift_array($tmp_date, $start5, $end5);  // 深夜帯
				// 月の深夜勤務時間に退勤後復帰の深夜分を追加
				//$sums[15] += count(array_intersect($arr_ret, $arr_time5)); //深夜残業対応 20110126
				}
			}
			$wk_return_time = $return_time;
			//分を時分に変換
			$return_time = minute_to_hmm($return_time);
            // 回数追加 20121109
            if ($return_count != "") {
                $return_time .= " ".$return_count;
            }

			// 前日までの計 = 週計
			$last_day_total = $work_time_for_week;

			$work_time_for_week += $work_time;
			$work_time_for_week += $wk_return_time;

			if ($tmp_date < $start_date) {
				$tmp_date = next_date($tmp_date);
				$counter_for_week++;
				continue;
			}
		}
		// 残業時間の処理を変更 20090629
		$wk_hoteigai = 0;
		$wk_hoteinai_zan = 0;
		$wk_hoteinai = 0;

		//日数換算
		$total_workday_count = "";
		if (($start_time != "" && $end_time != "") || $after_night_duty_flag == "1") //明けを追加 20110819
		{
			//休暇時は計算しない 20090806
			if ($pattern != "10") {
				if($workday_count != "")
				{
					$total_workday_count = $workday_count;
				}
			}

			if($night_duty_flag)
			{
				//曜日に対応したworkday_countを取得する
				$total_workday_count = $total_workday_count + $timecard_common_class->get_night_workday_count($tmp_date, $end_time, $next_day_flag, $tmcd_group_id);

			}
		}
		else if ($workday_count == 0){
			//日数換算に0が指定された場合のみ0を入れる
			$total_workday_count = $workday_count;
		}

		//残業時間 20100209 変更 20100910
		$wk_zangyo = 0;
		$kinmu_time = 0;
		//出勤時刻、退勤時刻が両方入っている場合に計算する 20101004
        //勤務時間を計算しないフラグが"1"以外 20130225
        if ($start_time != "" && $end_time != "" && $no_count_flag != "1") {
            //残業承認時に計算、申請画面を表示しない場合は無条件に計算
			if (($timecard_bean->over_time_apply_type != "0" && $overtime_approve_flg == "2") ||
					$timecard_bean->over_time_apply_type == "0"
				) {

				//残業開始、終了時刻がある場合
				if ($over_start_time != "" && $over_end_time != "") {
					$wk_zangyo = date_utils::get_time_difference($over_end_date_time,$over_start_date_time);
				//残業時刻未設定は計算しない 20110825
				//} else {
				//	//残業時刻がない場合で、所定終了、退勤を比較後残業計算 20100913
				//	if ($end2_date_time < $fixed_end_time) {
				//		$wk_zangyo = date_utils::get_time_difference($fixed_end_time,$end2_date_time);
				//	}
				}
				//残業２
				if ($over_start_time2 != "" && $over_end_time2 != "") {
					$over_start_date_time2 = $tmp_date.$over_start_time2;
					if ($over_start_next_day_flag2 == 1) {
						$over_start_date_time2 = next_date($tmp_date).$over_start_time2;
					}
					$over_end_date_time2 = $tmp_date.$over_end_time2;
					if ($over_end_next_day_flag2 == 1) {
						$over_end_date_time2 = next_date($tmp_date).$over_end_time2;
					}

					$wk_zangyo2 = date_utils::get_time_difference($over_end_date_time2,$over_start_date_time2);
					$wk_zangyo += $wk_zangyo2;
				}
				//早出残業
				if ($early_over_time != "") {
					$wk_early_over_time = hmm_to_minute($early_over_time);
					$wk_zangyo += $wk_early_over_time;
				}
			}
			//呼出
			$wk_zangyo += $wk_return_time;

            //コメント化 20130822
			//if ($wage == "4" && $start4 == "" && $end4 == "") {  // 時間給かつ休憩時刻指定がない場合 20100925
			//	//休日残業時は、休憩減算 20100916
			//	if ($legal_hol_over_flg == true) {
			//		if ($wk_zangyo >= ($time3_rest + $time4_rest)) {
			//			$wk_zangyo -= ($time3_rest + $time4_rest);
			//		}
			//	}
			//}
			//休日残業で休憩時刻がある場合 20100921
			if ($legal_hol_over_flg == true && $tmp_rest2 > 0) {
				$wk_zangyo -= $tmp_rest2;
			}

			//出勤時刻と残業開始時刻が一致している場合、所定、遅刻、早退を計算せず、残業、休憩のみ計算する 20100917
			if ($start2 != "" && $start2 == $over_start_time && $over_start_next_day_flag != "1") {//残業開始が翌日でないことを追加 20101027
				$overtime_only_flg = true;
				//遅刻
				$tikoku_time = 0;
				$start_time_info["diff_minutes"] = 0;
				//早退
				$sotai_time = 0;
				$end_time_info["diff_minutes"] = 0;
				//残業承認時に計算、申請画面を表示しない場合は無条件に計算 20100922
				if (($timecard_bean->over_time_apply_type != "0" && $overtime_approve_flg == "2") ||
						$timecard_bean->over_time_apply_type == "0"
					) {
					$wk_zangyo -= $tmp_rest2;
				} else {
					$tmp_rest2 = 0;
					$time_rest = "";
				}
			}
			else {
				$overtime_only_flg = false;
				//遅刻
				$tikoku_time = $start_time_info["diff_minutes"];
				//早退
				$sotai_time = $end_time_info["diff_minutes"];
			}
			$tikoku_time2 = 0;
			// 遅刻時間を残業時間から減算する場合
			if ($timecard_bean->delay_overtime_flg == "1") {
				if ($wk_zangyo >= $tikoku_time) { //20100913
					$wk_zangyo -= $tikoku_time;
				} else { // 残業時間より多い遅刻時間対応 20100925
					$tikoku_time2 = $tikoku_time - $wk_zangyo;
					$wk_zangyo = 0;
				}
			}

			//勤務時間＝所定労働時間−休憩時間−外出時間−早退−遅刻＋残業時間 20100907
			//　残業時間＝承認済み残業時間＋承認済み呼出し勤務時間
			//　承認済み残業時間＝早出残業時間＋勤務終了後残業時間
			//所定
			if ($pattern == "10" || //休暇は所定を0とする 20100916
					$overtime_only_flg == true ||	//残業のみ計算する 20100917
					$after_night_duty_flag == "1"  //明けの場合 20110819
				) {
				$shotei_time = 0;
			} elseif ($start2 == "" && $end2 == "") {  // 所定時刻指定がない場合、端数処理不具合対応 20101005
				$shotei_time = count($arr_effective_time_wk); //休憩を引く前の実働時間
			} else {
				$shotei_time = count($arr_time2);
			}
			//時間有休追加 20111207
			if ($obj_hol_hour->paid_hol_hour_flag == "t") {
				$shotei_time -= $paid_time_hour;
			}
			//休憩 $tmp_rest2 $time3_rest $time4_rest
			//外出 $time3 $time4
			$kinmu_time = $shotei_time - $time3 - $time4 + $wk_zangyo;
			// 遅刻時間を残業時間から減算しない場合
			if ($timecard_bean->delay_overtime_flg == "2") {
				$kinmu_time -= $tikoku_time;
			}
			// 遅刻時間を残業時間から減算する場合で、残業未承認の場合で遅刻がある場合は引く 20100914
			else {
				if ($overtime_approve_flg == "1") {
					$kinmu_time -= $tikoku_time;
				} else { //遅刻時間不具合対応 20101005
					// 残業時間より多い遅刻時間対応 20100925
					$kinmu_time -= $tikoku_time2;
				}
			}
			// 早退時間を残業時間から減算しない場合
			if ($timecard_bean->early_leave_time_flg == "2") {
				$kinmu_time -= $sotai_time;
			}
			//時間給者 20100915
			if ($wage == "4" && $start4 == "" && $end4 == "") {  // 時間給かつ休憩時刻指定がない場合 20100925
				//休日残業時は、休憩を減算しない 20100916
				//if ($legal_hol_over_flg != true) { //20130822
                    $kinmu_time -= ($time3_rest + $time4_rest);
                //}
			} else {
				if ($overtime_only_flg != true && $legal_hol_over_flg != true) {	//残業のみ計算する場合と休日残業の場合、残業から引くためここでは引かない 20100921
					//月給制日給制、実働時間内の休憩時間
					$kinmu_time -= $tmp_rest2;
				}
			}

			//法定内、法定外残業の計算
			if ($wk_zangyo > 0) {
				//法定内入力有無確認
				if ($legal_in_over_time != "") {
					$wk_hoteinai_zan = hmm_to_minute($legal_in_over_time);
					if ($wk_hoteinai_zan <= $wk_zangyo) {
						$wk_hoteigai = $wk_zangyo - $wk_hoteinai_zan;
					} else {
						$wk_hoteigai = 0;
					}

				} else {
					//法定外残業の基準時間を超える分を法定外残業とする
					if ($kinmu_time > $wk_base_time) {
						$wk_hoteigai = $kinmu_time - $wk_base_time;
					}
					//
					if ($wk_zangyo - $wk_hoteigai > 0) {
						$wk_hoteinai_zan = $wk_zangyo - $wk_hoteigai;
					} else {
						//マイナスになる場合
						$wk_hoteinai_zan = 0;
						$wk_hoteigai = $wk_zangyo;
					}
				}
			}
			//合計
			//法定内
			$wk_hoteinai = $kinmu_time - $wk_zangyo;
			$hoteinai += $wk_hoteinai;
			//法定内残業
			$hoteinai_zan += $wk_hoteinai_zan;
			//法定外残
			$hoteigai += $wk_hoteigai;
			//勤務時間
			$sums[13] += $kinmu_time;
			//残業時間
			$sums[14] += $wk_zangyo;
		}

//comment化
if ($dummy == "1") {
			//残業承認時に計算、申請画面を表示しない場合は無条件に計算 20100608
		if (($timecard_bean->over_time_apply_type != "0" && $overtime_approve_flg == "2") ||
				$timecard_bean->over_time_apply_type == "0"
				|| $wk_return_time > 0 // 退勤後復帰がある場合 20100714
				|| $legal_hol_over_flg) { // 公休か残業時刻ありを条件追加 20100802

			//日数換算別基準時間
			//休暇時は計算しない 20090806
			if ($pattern != "10" ||
					$legal_hol_over_flg) { // 公休で残業時刻ありを条件追加 20100802

				//法定内勤務、法定内残業、法定外残業
				// 前日までの累計 >= 40時間
				//法定外残業で週４０時間超過分 1:残業時間として計算するの場合
				if ($last_day_total >= 40 * 60 && $overtime_40_flg == "1") {
					//  法定外 += 稼動時間
					//下の処理で合計するためここはコメント化
					//			$hoteigai += $work_time + $wk_return_time;
				} else if ($work_time + $wk_return_time > 0){
					// 上記以外、かつ、 稼動時間がある場合

					//法定内勤務と法定外残業
					//法定内勤務のうち週40時間を超える分から法定外残業へ移動
					//wk法定外
					//残業時刻から計算 20100525
					if ($over_calc_flg) {
						/* 20100713
						// 端数処理する
						if ($fraction2 > "1") {
							//端数処理する分
							$moving_minutes = $timecard_common_class->get_moving_minutes($fraction2_min);
							//基準日時
							if ($end2_date_time != "") {
								$tmp_office_start_time = substr($end2_date_time, 0, 10) . "00";
							} else {
								$tmp_office_start_time = substr($start_date_time, 0, 10) . "00";
							}
							$tmp_over_end_date_time = date_utils::move_time($tmp_office_start_time, $over_end_date_time, $moving_minutes, $fraction2);
						} else {
*/
							$tmp_over_end_date_time = $over_end_date_time;
//						}
						$arr_over_time = $timecard_common_class->get_minute_time_array($over_start_date_time,$tmp_over_end_date_time);
						//20100712
						$wk_hoteigai = count(array_diff($arr_over_time, $arr_time4));
						$wk_hoteigai_org = $wk_hoteigai;
						if ($wk_hoteigai <= $effective_time) {
							$wk_hoteigai = $effective_time - $wk_base_time;
							if ($wk_hoteigai < 0) {
								$wk_hoteigai = 0;
							}
						}

						// 所定労働内の休憩時間 20100706
						$tmp_rest1 = count(array_intersect(array_diff($arr_time2, $arr_over_time), $arr_time4));
						// 所定労働外（残業時間内）の休憩時間
						$tmp_rest3 = count(array_intersect($arr_over_time, $arr_time4));
						$time_rest = minute_to_hmm($tmp_rest1);
						if ($tmp_rest3 > 0) {
							$time_rest .= "(".minute_to_hmm($tmp_rest3).")";
						}

					}else {
						$wk_hoteigai = $work_time + $wk_return_time - $wk_base_time;
					}
					if ($wk_hoteigai > 0) {
						//wk法定内 = (稼動時間 - wk法定外)
						$wk_hoteinai = ($work_time + $wk_return_time - $wk_hoteigai);

						// 40時間を超えた法定外を除外する
						if ($overtime_40_flg == "1") {
							$wk_jogai = $last_day_total + $work_time + $wk_return_time - (40 * 60);
							if ($wk_jogai > 0) {
								$wk_hoteigai -= $wk_jogai;
							}
						}

						//$arr_week_hoteigai[$counter_for_week] = $wk_hoteigai;
					} else {
						//wk法定内 = (稼動時間)
						$wk_hoteinai = ($work_time + $wk_return_time);

						//マイナスを0とする
						$wk_hoteigai = 0;
					}
					//echo("$tmp_date n=$wk_hoteinai g=$wk_hoteigai "); //debug

					//前日までの累計 ＋ wk法定内　＞ 40時間 の場合
					if ($last_day_total + $wk_hoteinai > 40 * 60) {
						if ($overtime_40_flg == "1") {
							$wk_hoteigai = 0;
							//法定内 += (40時間 - 前日までの累計) 20090722 変更
							$hoteinai += (40 * 60) - $last_day_total;
						} else {
							//法定内 += wk法定内 20090722 変更
							$hoteinai += $wk_hoteinai;
						}
					} else {
						//上記以外
						//法定内 += wk法定内
						$hoteinai += $wk_hoteinai;
						//echo("<br>2 $tmp_date hoteinai=$hoteinai");

					}

					//echo("$tmp_date w=$wk_week_hoteigai g=$wk_hoteigai "); //debug
					// 法定内残業算出の所定労働時間を勤務パターン毎の時間とする 20090715
					// 法定内残業、所定労働時間から8時間まで間を加算 時間帯の法定外残業の計算がある場合はその時間を基準にする
					// 稼動時間が所定労働時間より大きい場合
					//			$wk_minute = date_utils::hi_to_minute($day1_time); //カレンダーの所定労働時間（間違い）
					$wk_minute = $specified_time;
					// 時間帯の法定外残業の計算を使用する 480 -> $wk_base_time 20091218 20100713
					if (($wk_minute != 0 && $wk_minute < $wk_base_time && ($work_time + $wk_return_time) > $wk_minute) || $over_calc_flg) {

						//稼動時間が法定外残業の計算時間以上
						if (($work_time + $wk_return_time) >= $wk_base_time) {
							//基準法定内残業
							$wk_std_hoteinai_zan = $wk_base_time - $wk_minute;
						} else {
							//8時間未満の場合、稼動時間-所定労働時間
							$wk_std_hoteinai_zan = ($work_time + $wk_return_time) - $wk_minute;
							//20100713
							if ($over_calc_flg && $wk_hoteigai_org > ($wk_base_time - $specified_time)) {
								$wk_std_hoteinai_zan = $wk_hoteinai;
							}
						}

						//法定外残業で週４０時間超過分 1:残業時間として計算するの場合
						if ($overtime_40_flg == "1") {
							//前日までの累計 + 所定時間 ＜ 40時間 の場合
							if ($last_day_total + $wk_minute < 40 * 60) {
								//wk法定内残業 = 40時間 - (前日までの累計 + 所定時間)
								$wk_hoteinai_zan = 40 * 60 - ($last_day_total + $wk_minute);
								//wk法定内残業 > 基準法定内残業
								if ($wk_hoteinai_zan > $wk_std_hoteinai_zan) {
									// wk法定内残業 = 基準法定内残業
									$wk_hoteinai_zan = $wk_std_hoteinai_zan;
								}
								//法定内勤務 -= wk法定内残業
								$hoteinai -= $wk_hoteinai_zan;

								//法定内残業 += wk法定内残業
								$hoteinai_zan += $wk_hoteinai_zan;

								$sums[14] += $wk_hoteinai_zan; // 変更 20090722
							}
						} else {
							// wk法定内残業 = 基準法定内残業
							$wk_hoteinai_zan = $wk_std_hoteinai_zan;
							//法定内勤務 -= wk法定内残業
							$hoteinai -= $wk_hoteinai_zan;

							//法定内残業 += wk法定内残業
							$hoteinai_zan += $wk_hoteinai_zan;

							$sums[14] += $wk_hoteinai_zan; // 変更 20090722
						}
					}
					//echo("$tmp_date m=$wk_minute l=$last_day_total z=$wk_hoteinai_zan "); //debug
				}

			}

			//1日毎の法定外残を週単位に集計
			$wk_week_hoteigai += $wk_hoteigai;

			//法定外残業で週４０時間超過分 1:残業時間として計算するの場合
			if ($overtime_40_flg == "1") {
				if ($counter_for_week == 7) {
					if ($arr_irrg_info["irrg_type"] == "1") {
						$tmp_irrg_minutes = $arr_irrg_info["irrg_minutes"];
					} else {
						$tmp_irrg_minutes = 40 * 60; // 変則労働期間以外の場合40時間
					}
					if ($hol_minus == "t") {
						$tmp_irrg_minutes -= (8 * 60 * $holiday_count);
					}
					$time1 = ($work_time_for_week > $tmp_irrg_minutes) ? $work_time_for_week - $tmp_irrg_minutes : 0;
					//				$sums[14] += $wk_week_hoteigai;
					$wk_hoteigai = $time1 + $wk_week_hoteigai;
					$hoteigai += $wk_hoteigai;

				} else {
					$wk_hoteigai = "";
				}
				//20090722 前日までの累計+稼働時間 >= 40時間、かつ、半日の場合、所定時間を基準とする
				if ($last_day_total + $work_time + $wk_return_time >= 40 * 60 && $workday_count == 0.5) {
					//echo("$tmp_date m=$wk_minute l=$last_day_total z=$wk_hoteinai_zan "); //debug
					//			if ($workday_count == 0.5) {
					$wk_base_time = $specified_time;
				}
				$wk_hoteigai_yoteigai = $work_time + $wk_return_time - $wk_base_time;
				if ($wk_hoteigai_yoteigai > 0) {
					if ($others1 != "4") { //勤務時間内に外出した場合が、残業時間から減算するでない場合
						$wk_hoteigai_yoteigai += $time3;
					}
					if ($others2 != "4") { //勤務時間外に外出した場合が、残業時間から減算するでない場合
						$wk_hoteigai_yoteigai += $time4;
					}
					$sums[14] += $wk_hoteigai_yoteigai;
				}
			} else {

				$time1 = 0;
				// 出退勤とも打刻済みの場合
				if ($start_time != "" && $end_time != "") {
					//				if ($workday_count == 0.5) {
					//					$tmp_specified_time = $specified_time;
					//				} else {
					//					$tmp_specified_time = 480; //法定内8時間を所定として処理 20090717
					$tmp_specified_time = $wk_base_time; //日数換算対応 20090803
					//				}
					// 遅刻時間を残業時間から減算しない場合（所定時間から引くことで合わせる）
					if ($delay_overtime_flg == "2") {
						$tmp_specified_time = $tmp_specified_time - $start_time_info["diff_minutes"];
						if ($tmp_specified_time < 0) {
							$tmp_specified_time = 0;
						}
					}
					//休暇時は計算しない 20090806 公休か事由なし休暇で残業時刻が設定済を条件追加 20100802
					if ($pattern != "10" || $legal_hol_over_flg) {
					//残業時刻から計算 20100525
						if ($over_calc_flg) {
							//法定外 20100705
							$time1 = $wk_hoteigai;

						} else {
							if ($effective_time > $tmp_specified_time) {
								$time1 = $effective_time - $tmp_specified_time;
							} else {
								$time1 = $wk_hoteigai; //20100714
							}
						}


						if ($others1 == "4") { //勤務時間内に外出した場合
							if ($time1 > $time3) {
								$time1 -= $time3;
							}
						}
						if ($others2 == "4") { //勤務時間外に外出した場合
							if ($time1 > $time4) {
								$time1 -= $time4;
							}
						}
					}
				}
				$wk_hoteigai = $time1; //20100714

				$sums[14] += $wk_hoteigai;
				$hoteigai += $wk_hoteigai;

			}
			//法定内残業が入力された場合 20100531
			if ($legal_in_over_time != "") {
				//合計を戻す
				$sums[14] -= $wk_hoteinai_zan;
				$hoteinai_zan -= $wk_hoteinai_zan;
				$sums[14] -= $wk_hoteigai;
				$hoteigai -= $wk_hoteigai;

				$wk_zangyo = $wk_hoteinai_zan + $wk_hoteigai;

				//法定内残業と法定外残業に分ける
				$wk_hoteinai_zan = hmm_to_minute($legal_in_over_time);
				if ($wk_hoteinai_zan <= $wk_zangyo) {
					$wk_hoteigai = $wk_zangyo - $wk_hoteinai_zan;
				} else {
					$wk_hoteigai = 0;
				}
				//再度、合計する
				$sums[14] += $wk_hoteinai_zan;
				$hoteinai_zan += $wk_hoteinai_zan;
				$sums[14] += $wk_hoteigai;
				$hoteigai += $wk_hoteigai;

			}
			//早出残業が入力された場合 20100601
			if ($early_over_time != "") {
				//申請中の場合は計算しない	// 残業未承認か確認
				if (!($overtime_approve_flg == "1" || ($return_link_type != "0" && $return_link_type != "3"))) {

					$wk_early_over_time = hmm_to_minute($early_over_time);
					$work_time += $wk_early_over_time;
					$wk_zangyo += $wk_early_over_time;
					$wk_hoteinai_zan += $wk_early_over_time;
					//合計する
					$sums[13] += $wk_early_over_time;
					$sums[14] += $wk_early_over_time;
					$hoteinai_zan += $wk_early_over_time;
				}

			}
		}
} //comment化

		// 表示処理
		//表示用退避から設定 20100802
		$start_time = $disp_start_time;
		$end_time = $disp_end_time;

		$enc_cur_url = urlencode("$fname?session=$session&yyyymm=$yyyymm&view=$view");
		$rowspan = ($view == "1") ? "1" : "2";
		//残業申請状況の表示
		if ($timecard_bean->over_apply_disp_flg == "t" || $timecard_bean->over_apply_disp_flg == "1") { //20140226
			$hspacer = ($modify_link_type != "0" || ($view == "1" && ($overtime_link_type != "0" || $return_link_type != "0"))) ? "<br><font size=\"2\" class=\"j10\">&nbsp;</font>" : "";
		} else {
			$hspacer = "";
		}

		// チェック対応、ハイライト 20110125
		//　残業未申請、残業申請中、退復未申請（呼出し未申請）・申請中、修正申請中
		//　打刻漏れ、遅刻、早退がある行をハイライトする。
		if ($check_flg == "1" && (
					($overtime_link_type == "1" &&
						$timecard_bean->over_apply_disp_flg == "t" && //残業申請状況の表示がするの場合 20110805
						(
							($timecard_bean->over_yet_apply_disp_min != "" && //未申請を表示する設定確認
							$diff_min_end_time != "" &&
							$diff_min_end_time >= $timecard_bean->over_yet_apply_disp_min) ||
							($timecard_bean->over_yet_apply_disp_min == "" && //未申請を表示する設定の「分」が空白の場合 20110523
							$diff_min_end_time != "")
						) &&
						$no_overtime != "t" //残業管理をする場合
					) ||
					($overtime_link_type == "2" && $no_overtime != "t") ||
					$return_link_type == "1" ||
					$return_link_type == "2" ||
					$modify_link_type == "2" ||
					($tmp_date < $today &&
						($pattern != "10" && $workday_count > 0 &&
						 $after_night_duty_flag != "1") && //明けの場合を除く 20110819
						($start_time == "" || $end_time == "")
						) || //打刻漏れチェックは当日より前の場合、出勤退勤の確認
					($tmp_date == $today &&
						($pattern != "10" && $workday_count > 0 &&
						 $after_night_duty_flag != "1") && //明けの場合を除く 20110819
						($start_time == "")
						) || //打刻漏れチェックは当日の場合、出勤の確認
					$start_time_info["diff_minutes"] > 0 ||
					$end_time_info["diff_minutes"] > 0 ||
					($tmp_date <= $today &&
						($pattern == "") && //勤務パターン未設定
						($start_time == "" || $end_time == ""))
					|| (($pattern == "") && //時刻があるのに勤務パターン未設定 20120301
						($start_time != "" || $end_time != ""))
					|| ($pattern == "10" && //勤務パターンがあり、事由が未設定 20120301
						$reason == "")
                    || ($pattern == "10" && //勤務パターンが休暇で、出勤退勤時刻があるのに、残業時刻が入っていない場合 20120507
                        $start_time != "" && $end_time != "" &&
                        $over_start_time == "" && $over_end_time == "")
                    || ($tmp_date != $today && $start_time != "" && $end_time == "") //当日以外で出勤時刻のみ設定 20120720
                    || ($start_time == "" && $end_time != "") //退勤時刻のみ設定 20120720
                    ) ) {
			$bgcolor = "#ffff66";
		}
		// 背景色設定。法定、祝祭日を赤、所定を青にする
		else {
			$bgcolor = get_timecard_bgcolor($type);
		}
		// 2段表示時の背景。#f6f9ffから変更
		if ($bgcolor == "#ffffff") {
			$bgcolor2 = "#f6f9ff";
		} else {
			$bgcolor2 = $bgcolor;
		}
		// 締めデータがない場合
		if (!$atdbk_closed) {
			echo("<tr bgcolor=\"$bgcolor\">\n");
			echo("<td rowspan=\"$rowspan\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
			if ($modify_flg == "t") {
				echo("<a href=\"javascript:void(0);\" onclick=\"window.open('atdbk_timecard_edit.php?session=$session&emp_id=$emp_id&date=$tmp_date&yyyymm=$yyyymm&view=$view&wherefrom=1', 'newwin', 'width=640,height=600,scrollbars=yes');\">{$tmp_month}月{$tmp_day}日</a>$hspacer");
			} else {
				switch ($modify_link_type) {
				case "0":  // 未申請
						echo("<a href=\"javascript:void(0);\" onclick=\"window.open('atdbk_timecard_apply.php?session=$session&date=$tmp_date&yyyymm=$yyyymm&view=$view&wherefrom=1', 'newwin', 'width=640,height=700,scrollbars=yes');\">{$tmp_month}月{$tmp_day}日</a>$hspacer");
					break;
				case "2":  // 申請中

	                // 勤務時間修正申請承認者情報取得
	                $arr_tmmdaprv = $atdbk_workflow_common_class->get_approve_list($modify_apply_id, "TMMD");
	                // 承認がひとつでもあるかチェック
	                $apv_1_flg = $atdbk_workflow_common_class->get_apv_flg($arr_tmmdaprv);
	                // 一部承認
	                if($apv_1_flg)
	                {
	                    $apply_stat_nm = "一部承認";
	                }
	                else
	                {
	                    $apply_stat_nm = "修正申請中";
	                }

                        echo("<a href=\"javascript:void(0);\" onclick=\"window.open('timecard_modify_detail.php?session=$session&apply_id=$modify_apply_id&wherefrom=1&pnt_url=$enc_cur_url', 'newwin', 'width=900,height=740,scrollbars=yes');\">{$tmp_month}月{$tmp_day}日</a><br><font size=\"2\" color=\"red\" class=\"j10\">$apply_stat_nm</font>");
					break;
				case "3":  // 承認済
                        echo("<a href=\"javascript:void(0);\" onclick=\"window.open('timecard_modify_back.php?session=$session&apply_id=$modify_apply_id&wherefrom=1&pnt_url=$enc_cur_url', 'newwin', 'width=900,height=740,scrollbars=yes');\">{$tmp_month}月{$tmp_day}日</a><br><font size=\"2\" class=\"j10\">修正承認済</font>");
					break;
				case "4":  // 否認済
                        echo("<a href=\"javascript:void(0);\" onclick=\"window.open('timecard_modify_back.php?session=$session&apply_id=$modify_apply_id&wherefrom=1&pnt_url=$enc_cur_url', 'newwin', 'width=900,height=740,scrollbars=yes');\">{$tmp_month}月{$tmp_day}日</a><br><font size=\"2\" class=\"j10\">修正否認済</font>");
					break;
				case "5":  // 差戻済
                        echo("<a href=\"javascript:void(0);\" onclick=\"window.open('timecard_modify_back.php?session=$session&apply_id=$modify_apply_id&wherefrom=1&pnt_url=$enc_cur_url', 'newwin', 'width=900,height=740,scrollbars=yes');\">{$tmp_month}月{$tmp_day}日</a><br><font size=\"2\" color=\"red\" class=\"j10\">修正差戻済</font>");
					break;
				}
			}
			echo("</font><input type=\"hidden\" name=\"date[]\" value=\"$tmp_date\"></td>\n");

			//曜日
			echo("<td rowspan=\"$rowspan\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . get_weekday(to_timestamp($tmp_date)) . "$hspacer</font></td>\n");

			if ($timecard_bean->time_move_flag == "t" && $view != "2"){

				if ($timecard_bean->zen_display_flag == "t"){
					// 前日になる場合があるフラグ
					$previous_day_value = "";
					if ($previous_day_flag == 1){
						$previous_day_value = "前";
					}
					echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($previous_day_value) . "$hspacer</font></td>\n");
				}

				echo("<td class=\"$time_cell_class\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">" . nbsp(hhmm_to_hmm($show_start_time)) . "$hspacer</font></td>\n"); //20110513

				if ($timecard_bean->yoku_display_flag == "t"){

					// 翌
					$next_day_value = "";
					if ($show_next_day_flag == 1){	//残業時刻日またがり対応 20100114
						$next_day_value = "翌";
					}

					echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($next_day_value) . "$hspacer</font></td>\n");
				}

				echo("<td class=\"$time_cell_class\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">");
				//残業管理をしない場合の対応 2008/10/15
				if ($no_overtime == "t") {
					$overtime_link_type = "0"; //申請を表示しない
					$time1 = "";	// 残業時間を表示しない
					$wk_hoteinai_zan = "";
					$wk_hoteigai = "";

				}

				switch ($overtime_link_type) {
				case "0":  // 申請不要
					echo(nbsp(hhmm_to_hmm($show_end_time)) . $hspacer ."</font>");
					break;
				case "1":  // 未申請
						$apply_stat_nm = ($timecard_bean->over_apply_disp_flg == "t") ? "残業未申請" : "";
						//未申請を表示しない設定追加 20100809
						if ($timecard_bean->over_yet_apply_disp_min != "") {
							if ($diff_min_end_time != "" && $diff_min_end_time < $timecard_bean->over_yet_apply_disp_min) {
								$apply_stat_nm = "";
							}
						}
						echo ("<a href=\"javascript:void(0);\" onclick=\"window.open('overtime_apply.php?session=$session&date=$tmp_date&pnt_url=$enc_cur_url', 'newwin', 'width=640,height=600,scrollbars=yes');\">".nbsp(hhmm_to_hmm($show_end_time))."</a></font><br><font size=\"2\" class=\"j10\">$apply_stat_nm</font>");
					break;
				case "2":  // 申請中

					// 残業申請承認者情報取得
					$arr_ovtmaprv = $atdbk_workflow_common_class->get_approve_list($overtime_apply_id, "OVTM");
					// 承認がひとつでもあるかチェック
					$apv_1_flg = $atdbk_workflow_common_class->get_apv_flg($arr_ovtmaprv);
					//残業申請状況表示フラグ
						if ($timecard_bean->over_apply_disp_flg == "t" || $timecard_bean->over_apply_disp_flg == "1") { //20140226
							// 一部承認
							if ($apv_1_flg) {
								$apply_stat_nm = "一部承認";
							} else {
								$apply_stat_nm = "残業申請中";
							}
						} else {
							$apply_stat_nm = "";
						}

					echo ("<a href=\"javascript:void(0);\" onclick=\"show_sub_window('overtime_detail.php?session=$session&apply_id=$overtime_apply_id&pnt_url=$enc_cur_url');\">".nbsp(hhmm_to_hmm($show_end_time))."</a><br></font><font size=\"2\" color=\"red\" class=\"j10\">$apply_stat_nm</font>");
					break;
				case "3":  // 承認済
						$apply_stat_nm = ($timecard_bean->over_apply_disp_flg == "t" || $timecard_bean->over_apply_disp_flg == "1") ? "残業承認済" : "";

						echo ("<a href=\"javascript:void(0);\" onclick=\"show_sub_window('overtime_refer.php?session=$session&apply_id=$overtime_apply_id');\">".nbsp(hhmm_to_hmm($show_end_time))."</a></font><br><font size=\"2\" class=\"j10\">$apply_stat_nm</font>");
					break;
				case "4":  // 否認済
						$apply_stat_nm = ($timecard_bean->over_apply_disp_flg == "t" || $timecard_bean->over_apply_disp_flg == "1") ? "残業否認済" : "";
						echo ("<a href=\"javascript:void(0);\" onclick=\"show_sub_window('overtime_refer.php?session=$session&apply_id=$overtime_apply_id');\">".nbsp(hhmm_to_hmm($show_end_time))."</a></font><br><font size=\"2\" class=\"j10\">$apply_stat_nm</font>");
					break;
				case "5":  // 差戻済
						$apply_stat_nm = ($timecard_bean->over_apply_disp_flg == "t" || $timecard_bean->over_apply_disp_flg == "1") ? "残業差戻済" : "";
						echo ("<a href=\"javascript:void(0);\" onclick=\"show_sub_window('overtime_back.php?session=$session&apply_id=$overtime_apply_id&pnt_url=$enc_cur_url');\">".nbsp(hhmm_to_hmm($show_end_time))."</a></font><br><font size=\"2\" color=\"red\" class=\"j10\">$apply_stat_nm</font>");
					break;
				case "6":  // 残業申請不要
						$apply_stat_nm = ($timecard_bean->over_apply_disp_flg == "t" || $timecard_bean->over_apply_disp_flg == "1") ? "残業申請不要" : "";
						echo ("<a href=\"javascript:void(0);\" onclick=\"window.open('overtime_apply.php?session=$session&date=$tmp_date&pnt_url=$enc_cur_url', 'newwin', 'width=640,height=740,scrollbars=yes');\">".nbsp(hhmm_to_hmm($show_end_time))."</a></font><br><font size=\"2\" class=\"j10\">$apply_stat_nm</font>");
					break;
				}
				echo("</td>\n");
				//勤務
				echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($kinmu_time)) . "$hspacer</font></td>\n"); //$work_time + $wk_return_timeから変更 20100910
				//遅刻 20100907 赤色に変更 20100922
				echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"red\">" . nbsp(minute_to_hmm($start_time_info["diff_minutes"])) . "$hspacer</font></td>\n");
				//早退 20100907 赤色に変更 20100922
				echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"red\">" . nbsp(minute_to_hmm($end_time_info["diff_minutes"])) . "$hspacer</font></td>\n");
				if ($timecard_bean->over_disp_split_flg != "f"){
					//法内残
					echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($wk_hoteinai_zan)) . "$hspacer</font></td>\n");
					//法外残
					echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($wk_hoteigai)) . "$hspacer</font></td>\n");
				} else {
					//残業=法内残+法外残
					echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($wk_hoteinai_zan+$wk_hoteigai)) . "$hspacer</font></td>\n");
				}

			}

			if ($timecard_bean->time_move_flag != "t"){
				//種別
				if ($timecard_bean->type_display_flag == "t"){
					echo("<td rowspan=\"$rowspan\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($type_name) . "$hspacer</font></td>\n");
				}
			}

			// 予実績表示の場合
			if ($view == "2") {

				// 勤務日種別と出勤パターン（予定）から所定労働時間帯を取得
	//			$prov_start2 = $officehours[$prov_pattern][$tmp_type]["start2"];
	//			$prov_end2 = $officehours[$prov_pattern][$tmp_type]["end2"];
				$prov_start2 = $timecard_common_class->get_officehours_info( $prov_tmcd_group_id, $prov_pattern, $tmp_type, "start2" );
				$prov_end2   = $timecard_common_class->get_officehours_info( $prov_tmcd_group_id, $prov_pattern, $tmp_type, "end2" );

                //個人別所定時間を優先する場合 20120309
				if ($arr_empcond_officehours_ptn[$prov_tmcd_group_id][$prov_pattern] == 1) {
					$wk_weekday = $arr_weekday_flg[$tmp_date];
					$arr_empcond_officehours = $timecard_common_class->get_empcond_officehours($wk_weekday);
					if ($arr_empcond_officehours["officehours2_start"] != "") {
						$prov_start2 = $arr_empcond_officehours["officehours2_start"];
						$prov_end2 = $arr_empcond_officehours["officehours2_end"];
					}
				}

				// 所定労働時間が確定できたら予定時刻として使用
				if ($prov_start2 != "" && $prov_end2 != "") {
					$prov_start_time = $prov_start2;
					$prov_end_time = $prov_end2;
				}

				// 表示値の編集
				if ($prov_pattern != "") {
	//				$prov_pattern = $arr_attendance_pattern[$prov_pattern];
					$prov_pattern = $atdbk_common_class->get_pattern_name($prov_tmcd_group_id, $prov_pattern);
				}
				if ($prov_reason != "") {
	//				$prov_reason = $arr_attendance_reason[$prov_reason];
					$prov_reason = $atdbk_common_class->get_reason_name($prov_reason);
				}

				if($prov_night_duty == "1")
				{
					$prov_night_duty = "有り";
				}
				else if($prov_night_duty == "2")
				{
					$prov_night_duty = "無し";
				}

				$prov_allow_contents = "";
				if ($prov_allow_id != "")
				{
					foreach($arr_allowance as $allowance)
					{
						if($prov_allow_id == $allowance["allow_id"])
						{
							$prov_allow_contents = $allowance["allow_contents"];
							break;
						}
					}
				}

				//グループ名取得
				$prov_group_name = $atdbk_common_class->get_group_name($prov_tmcd_group_id);

				if($timecard_bean->time_move_flag == "t"){
					if ($timecard_bean->zen_display_flag == "t"){
						echo("<td bgcolor=\"$bgcolor2\" class=\"lbl\">&nbsp;</td>\n");
					}
					echo("<td bgcolor=\"$bgcolor2\" class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">" . nbsp(hhmm_to_hmm($prov_start_time)) . "</font></td>\n");
					if ($timecard_bean->yoku_display_flag == "t"){
						echo("<td bgcolor=\"$bgcolor2\" class=\"lbl\">&nbsp;</td>\n");
					}
					echo("<td bgcolor=\"$bgcolor2\" class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">" . nbsp(hhmm_to_hmm($prov_end_time)) . "</font></td>\n");
					echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
					//遅刻 20100907
					echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
					//早退 20100907
					echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
					//法定内残、法定外残
					echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
					if ($timecard_bean->over_disp_split_flg != "f"){
						echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
					}
				}

				if ($timecard_bean->time_move_flag == "t"){
					//種別
					if ($timecard_bean->type_display_flag == "t"){
						echo("<td rowspan=\"$rowspan\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($type_name) . "$hspacer</font></td>\n");
					}
				}

				if ($timecard_bean->group_display_flag == "t"){
					echo("<td height=\"22\" bgcolor=\"$bgcolor2\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($prov_group_name) . "</font></td>\n");
				}
				echo("<td height=\"22\" bgcolor=\"$bgcolor2\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($prov_pattern) . "</font></td>\n");
				echo("<td bgcolor=\"$bgcolor2\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($prov_reason) . "</font></td>\n");
				echo("<td bgcolor=\"$bgcolor2\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($prov_night_duty) . "</font></td>\n");
				echo("<td bgcolor=\"$bgcolor2\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($prov_allow_contents) . "</font></td>\n");
				if($timecard_bean->time_move_flag == "f"){
					if ($timecard_bean->zen_display_flag == "t"){
						echo("<td bgcolor=\"$bgcolor2\" class=\"lbl\">&nbsp;</td>\n");
					}
					echo("<td bgcolor=\"$bgcolor2\" class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">" . nbsp(hhmm_to_hmm($prov_start_time)) . "</font></td>\n");
					if ($timecard_bean->yoku_display_flag == "t"){
						echo("<td bgcolor=\"$bgcolor2\" class=\"lbl\">&nbsp;</td>\n");
					}
					echo("<td bgcolor=\"$bgcolor2\" class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">" . nbsp(hhmm_to_hmm($prov_end_time)) . "</font></td>\n");
					echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
					//遅刻 20100907
					echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
					//早退 20100907
					echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
					//法定内残、法定外残
					echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
					if ($timecard_bean->over_disp_split_flg != "f"){
						echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
					}
				}
				echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
				echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
				echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
				if ($timecard_bean->ret_display_flag == "t"){
					echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
				}
				echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
				if($meeting_display_flag){
					echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
				}
				echo("</tr>\n");
				echo("<tr bgcolor=\"$bgcolor\">\n");
			}

			if ($timecard_bean->time_move_flag == "t" && $view == "2"){

				if ($timecard_bean->zen_display_flag == "t"){
					// 前日になる場合があるフラグ
					$previous_day_value = "";
					if ($previous_day_flag == 1){
						$previous_day_value = "前";
					}
					echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($previous_day_value) . "$hspacer</font></td>\n");
				}

				echo("<td class=\"$time_cell_class\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">" . nbsp(hhmm_to_hmm($show_start_time)) . "$hspacer</font></td>\n"); //20110513

				if ($timecard_bean->yoku_display_flag == "t"){

					// 翌
					$next_day_value = "";
					if ($show_next_day_flag == 1){	//残業時刻日またがり対応 20100114
						$next_day_value = "翌";
					}

					echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($next_day_value) . "$hspacer</font></td>\n");
				}

				echo("<td class=\"$time_cell_class\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">");
//残業管理をしない場合の対応 2008/10/15
				if ($no_overtime == "t") {
					$overtime_link_type = "0"; //申請を表示しない
					$time1 = "";	// 残業時間を表示しない
					$wk_hoteinai_zan = "";
					$wk_hoteigai = "";
				}

				switch ($overtime_link_type) {
				case "0":  // 申請不要
					echo(nbsp(hhmm_to_hmm($show_end_time)) . $hspacer ."</font>");
					break;
				case "1":  // 未申請
						$apply_stat_nm = ($timecard_bean->over_apply_disp_flg == "t") ? "残業未申請" : "";
						//未申請を表示しない設定追加 20100809
						if ($timecard_bean->over_yet_apply_disp_min != "") {
							if ($diff_min_end_time != "" && $diff_min_end_time < $timecard_bean->over_yet_apply_disp_min) {
								$apply_stat_nm = "";
							}
						}
						echo ("<a href=\"javascript:void(0);\" onclick=\"window.open('overtime_apply.php?session=$session&date=$tmp_date&pnt_url=$enc_cur_url', 'newwin', 'width=640,height=600,scrollbars=yes');\">".nbsp(hhmm_to_hmm($show_end_time))."</a></font><br><font size=\"2\" class=\"j10\">$apply_stat_nm</font>");
						break;
					case "2":  // 申請中

					// 残業申請承認者情報取得
					$arr_ovtmaprv = $atdbk_workflow_common_class->get_approve_list($overtime_apply_id, "OVTM");
					// 承認がひとつでもあるかチェック
					$apv_1_flg = $atdbk_workflow_common_class->get_apv_flg($arr_ovtmaprv);
					//残業申請状況表示フラグ
					if ($timecard_bean->over_apply_disp_flg == "t" || $timecard_bean->over_apply_disp_flg == "1") { //20140226
						// 一部承認
						if ($apv_1_flg) {
							$apply_stat_nm = "一部承認";
						} else {
							$apply_stat_nm = "残業申請中";
						}
					} else {
						$apply_stat_nm = "";
					}

					echo ("<a href=\"javascript:void(0);\" onclick=\"show_sub_window('overtime_detail.php?session=$session&apply_id=$overtime_apply_id&pnt_url=$enc_cur_url');\">".nbsp(hhmm_to_hmm($show_end_time))."</a><br></font><font size=\"2\" color=\"red\" class=\"j10\">$apply_stat_nm</font>");
					break;
				case "3":  // 承認済
						$apply_stat_nm = ($timecard_bean->over_apply_disp_flg == "t" || $timecard_bean->over_apply_disp_flg == "1") ? "残業承認済" : "";
						echo ("<a href=\"javascript:void(0);\" onclick=\"show_sub_window('overtime_refer.php?session=$session&apply_id=$overtime_apply_id');\">".nbsp(hhmm_to_hmm($show_end_time))."</a></font><br><font size=\"2\" class=\"j10\">$apply_stat_nm</font>");
					break;
				case "4":  // 否認済
						$apply_stat_nm = ($timecard_bean->over_apply_disp_flg == "t" || $timecard_bean->over_apply_disp_flg == "1") ? "残業否認済" : "";
						echo ("<a href=\"javascript:void(0);\" onclick=\"show_sub_window('overtime_refer.php?session=$session&apply_id=$overtime_apply_id');\">".nbsp(hhmm_to_hmm($show_end_time))."</a></font><br><font size=\"2\" class=\"j10\">$apply_stat_nm</font>");
					break;
				case "5":  // 差戻済
						$apply_stat_nm = ($timecard_bean->over_apply_disp_flg == "t" || $timecard_bean->over_apply_disp_flg == "1") ? "残業差戻済" : "";
						echo ("<a href=\"javascript:void(0);\" onclick=\"show_sub_window('overtime_back.php?session=$session&apply_id=$overtime_apply_id&pnt_url=$enc_cur_url');\">".nbsp(hhmm_to_hmm($show_end_time))."</a></font><br><font size=\"2\" color=\"red\" class=\"j10\">$apply_stat_nm</font>");
					break;
				case "6":  // 残業申請不要
						$apply_stat_nm = ($timecard_bean->over_apply_disp_flg == "t" || $timecard_bean->over_apply_disp_flg == "1") ? "残業申請不要" : "";
						echo ("<a href=\"javascript:void(0);\" onclick=\"window.open('overtime_apply.php?session=$session&date=$tmp_date&pnt_url=$enc_cur_url', 'newwin', 'width=640,height=740,scrollbars=yes');\">".nbsp(hhmm_to_hmm($show_end_time))."</a></font><br><font size=\"2\" class=\"j10\">$apply_stat_nm</font>");
					break;
				}
				echo("</td>\n");
				//勤務
				echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($kinmu_time)) . "$hspacer</font></td>\n"); //$work_time + $wk_return_timeから変更 20100910
				//遅刻 20100907 赤色に変更 20100922
				echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"red\">" . nbsp(minute_to_hmm($start_time_info["diff_minutes"])) . "$hspacer</font></td>\n");
				//早退 20100907 赤色に変更 20100922
				echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"red\">" . nbsp(minute_to_hmm($end_time_info["diff_minutes"])) . "$hspacer</font></td>\n");
				if ($timecard_bean->over_disp_split_flg != "f"){
					//法内残
					echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($wk_hoteinai_zan)) . "$hspacer</font></td>\n");

					//法外残
					echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($wk_hoteigai)) . "$hspacer</font></td>\n");
				} else {
					//残業=法内残+法外残
					echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($wk_hoteinai_zan+$wk_hoteigai)) . "$hspacer</font></td>\n");
				}
			}

			if ($timecard_bean->time_move_flag == "t" && $view != "2"){
				//種別
				if ($timecard_bean->type_display_flag == "t"){
					echo("<td rowspan=\"$rowspan\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($type_name) . "$hspacer</font></td>\n");
				}
			}

		//残業申請状況の表示
		if ($timecard_bean->over_apply_disp_flg == "t" || $timecard_bean->over_apply_disp_flg == "1") {
			$hspacer = (($view == "1" && $modify_link_type != "0") || $overtime_link_type != "0" || $return_link_type != "0") ? "<br><font size=\"2\" class=\"j10\">&nbsp;</font>" : "";
		} else {
			$hspacer = "";
		}
	        // グループ
			if ($timecard_bean->group_display_flag == "t"){
				//グループのリスト取得
				$arr_timecard_group = $atdbk_common_class->get_group_array();
				echo("<td class=\"txt\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
				if (($timecard_modify_flg == "f") && ($now_date >= $tmp_date)) {
					echo("<input type=\"hidden\" name=\"list_tmcd_group_id[]\" value=\"$tmcd_group_id\"> \n");
					echo("<select name=\"\" id=\"$selectGroupId\" disabled>");
				} else {
					echo("<select name=\"list_tmcd_group_id[]\" id=\"$selectGroupId\" onChange=\"setAtdptn(this.value, '$selectPatternId')\">");
				}

				foreach ($arr_timecard_group as $group => $group_val) {
					echo("<option value=\"$group\"");
					if ($tmcd_group_id == $group) {
						echo(" selected");
					}
					echo(">$group_val");
				}
				echo("</select>");
				echo("$hspacer</font></td>\n");
			}
			else{
				echo("<input type=\"hidden\" name=\"list_tmcd_group_id[]\" value=\"$tmcd_group_id\"> \n");
			}

			// パターン
			// パターンのリスト取得
			$arr_attendance_pattern = $atdbk_common_class->get_pattern_array($tmcd_group_id);
			echo("<td height=\"22\" class=\"txt\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
	// ************ oose update start *****************
	//		echo("<select name=\"pattern[]\">");
			if (($timecard_modify_flg == "f") && ($now_date >= $tmp_date)) {
				echo("<input type=\"hidden\" name=\"pattern[]\" value=\"$pattern\"> \n");
				echo("<select name=\"\" id=\"$selectPatternId\" disabled>");
			} else {
				echo("<select name=\"pattern[]\" id=\"$selectPatternId\" onChange=\"setReason($wk_pos, $tmcd_group_id, this.value);\">");
			}
			$wk_pos++;
	// ************ oose update end *****************
			echo("<option value=\"--\">");
			foreach ($arr_attendance_pattern as $atdptn_id => $atdptn_val) {
				echo("<option value=\"$atdptn_id\"");
				if ($pattern == $atdptn_id) {
					echo(" selected");
				}
				echo(">$atdptn_val");
			}
			echo("</select>");
			echo("$hspacer</font></td>\n");

			// 事由
			echo("<td class=\"txt\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
	// ************ oose update start *****************
	//		echo("<select name=\"reason[]\">");
	/*		if (($timecard_modify_flg == "f") && ($now_date >= $tmp_date)) {
				echo("<input type=\"hidden\" name=\"reason[]\" value=\"$reason\"> \n");
				echo("<select name=\"\" disabled>");
			} else {
				echo("<select name=\"reason[]\">");
			}
	// ************ oose update end *****************
			echo("<option value=\"--\">");
			foreach ($arr_attendance_reason as $tmp_key => $tmp_val) {
				echo("<option value=\"$tmp_key\""); if ($reason == $tmp_key) {echo(" selected");} echo(">$tmp_val");
			}
			echo("</select>");
	*/

			$atdbk_common_class->set_reason_option("reason[]", $reason, (($timecard_modify_flg == "f") && ($now_date >= $tmp_date)) == false);
			echo("$hspacer</font></td>\n");


	        // 当直
			echo("<td class=\"txt\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">\n");
			if (($timecard_modify_flg == "f") && ($now_date >= $tmp_date)) {
				echo("<input type=\"hidden\" name=\"night_duty[]\" value=\"$night_duty\"> \n");
				echo("<select name=\"\" disabled>");
			} else {
				echo("<select name=\"night_duty[]\">");
			}
			echo("<option value=\"--\">");
			echo("<option value=\"1\""); if ($night_duty == "1") {echo(" selected");} echo(">有り");
			echo("<option value=\"2\""); if ($night_duty == "2") {echo(" selected");} echo(">無し");
			echo("</select>");
			echo("$hspacer</font></td>\n");

			echo("<td height=\"22\" class=\"txt\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
	// ************ oose update start *****************
	//		show_allowance_list($allow_id, $arr_allowance);
			if (($timecard_modify_flg == "f") && ($now_date >= $tmp_date)) {
				if ($allow_id == "") {
					$wk = "--";
				} else {
					$wk = $allow_id;
				}
				echo("<input type=\"hidden\" name=\"allow_ids[]\" value=\"$wk\"> \n");
				echo("<select name=\"\" disabled>");
			} else {
				echo("<select name=\"allow_ids[]\">");
			}
			echo("<option value=\"--\">");

			foreach($arr_allowance as $allowance)
			{
				$tmp_allow_id = $allowance["allow_id"];
				$tmp_allow_contents = $allowance["allow_contents"];
				$tmp_del_flg = $allowance["del_flg"];

				if($allow_id == $tmp_allow_id)
				{
					echo("<option value=\"$tmp_allow_id\"");
					echo(" selected");
					echo(">$tmp_allow_contents");
				}
				else
				{
					if($tmp_del_flg == 'f')
					{
						echo("<option value=\"$tmp_allow_id\"");
						echo(">$tmp_allow_contents");
					}
				}
			}

			echo("</select>");
	// ************ oose update end *****************
			echo("$hspacer</font></td>\n");

			if ($timecard_bean->time_move_flag == "f" || empty($timecard_bean->time_move_flag)){

				if ($timecard_bean->zen_display_flag == "t"){
					// 前日になる場合があるフラグ
					$previous_day_value = "";
					if ($previous_day_flag == 1){
						$previous_day_value = "前";
					}
					echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($previous_day_value) . "$hspacer</font></td>\n");
				}

				echo("<td class=\"$time_cell_class\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">" . nbsp(hhmm_to_hmm($show_start_time)) . "$hspacer</font></td>\n"); //20110513

				if ($timecard_bean->yoku_display_flag == "t"){

					// 翌
					$next_day_value = "";
	/*
				時間による判定からフラグによる判定に変更
				時間は参照しない
				if ($wk_start_time != "" && $wk_end_time != "" && (substr("0$wk_start_time", -4) > substr("0$wk_end_time", -4) && $previous_day_flag != "1")){
	*/
					if ($show_next_day_flag == 1){	//残業時刻日またがり対応 20100114
						$next_day_value = "翌";
					}

					echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($next_day_value) . "$hspacer</font></td>\n");
				}

				echo("<td class=\"$time_cell_class\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">");
//残業管理をしない場合の対応 2008/10/15
				if ($no_overtime == "t") {
					$overtime_link_type = "0"; //申請を表示しない
					$time1 = "";	// 残業時間を表示しない
					$wk_hoteinai_zan = "";
					$wk_hoteigai = "";
				}

				switch ($overtime_link_type) {
				case "0":  // 申請不要
					echo(nbsp(hhmm_to_hmm($show_end_time)) . $hspacer ."</font>");
					break;
				case "1":  // 未申請
						$apply_stat_nm = ($timecard_bean->over_apply_disp_flg == "t") ? "残業未申請" : "";
						//未申請を表示しない設定追加 20100809
						if ($timecard_bean->over_yet_apply_disp_min != "") {
							if ($diff_min_end_time != "" && $diff_min_end_time < $timecard_bean->over_yet_apply_disp_min) {
								$apply_stat_nm = "";
							}
						}
						echo ("<a href=\"javascript:void(0);\" onclick=\"window.open('overtime_apply.php?session=$session&date=$tmp_date&pnt_url=$enc_cur_url', 'newwin', 'width=640,height=600,scrollbars=yes');\">".nbsp(hhmm_to_hmm($show_end_time))."</a></font><br><font size=\"2\" class=\"j10\">$apply_stat_nm</font>");
						break;
					case "2":  // 申請中

					// 残業申請承認者情報取得
					$arr_ovtmaprv = $atdbk_workflow_common_class->get_approve_list($overtime_apply_id, "OVTM");
					// 承認がひとつでもあるかチェック
					$apv_1_flg = $atdbk_workflow_common_class->get_apv_flg($arr_ovtmaprv);
					//残業申請状況表示フラグ
					if ($timecard_bean->over_apply_disp_flg == "t" || $timecard_bean->over_apply_disp_flg == "1") {
						// 一部承認
						if ($apv_1_flg) {
							$apply_stat_nm = "一部承認";
						} else {
							$apply_stat_nm = "残業申請中";
						}
					} else {
						$apply_stat_nm = "";
					}

					echo ("<a href=\"javascript:void(0);\" onclick=\"show_sub_window('overtime_detail.php?session=$session&apply_id=$overtime_apply_id&pnt_url=$enc_cur_url');\">".nbsp(hhmm_to_hmm($show_end_time))."</a><br></font><font size=\"2\" color=\"red\" class=\"j10\">$apply_stat_nm</font>");
					break;
				case "3":  // 承認済
						$apply_stat_nm = ($timecard_bean->over_apply_disp_flg == "t" || $timecard_bean->over_apply_disp_flg == "1") ? "残業承認済" : "";
						echo ("<a href=\"javascript:void(0);\" onclick=\"show_sub_window('overtime_refer.php?session=$session&apply_id=$overtime_apply_id');\">".nbsp(hhmm_to_hmm($show_end_time))."</a></font><br><font size=\"2\" class=\"j10\">$apply_stat_nm</font>");
					break;
				case "4":  // 否認済
						$apply_stat_nm = ($timecard_bean->over_apply_disp_flg == "t" || $timecard_bean->over_apply_disp_flg == "1") ? "残業否認済" : "";
						echo ("<a href=\"javascript:void(0);\" onclick=\"show_sub_window('overtime_refer.php?session=$session&apply_id=$overtime_apply_id');\">".nbsp(hhmm_to_hmm($show_end_time))."</a></font><br><font size=\"2\" class=\"j10\">$apply_stat_nm</font>");
					break;
				case "5":  // 差戻済
						$apply_stat_nm = ($timecard_bean->over_apply_disp_flg == "t" || $timecard_bean->over_apply_disp_flg == "1") ? "残業差戻済" : "";
						echo ("<a href=\"javascript:void(0);\" onclick=\"show_sub_window('overtime_back.php?session=$session&apply_id=$overtime_apply_id&pnt_url=$enc_cur_url');\">".nbsp(hhmm_to_hmm($show_end_time))."</a></font><br><font size=\"2\" color=\"red\" class=\"j10\">$apply_stat_nm</font>");
					break;
				case "6":  // 残業申請不要
						$apply_stat_nm = ($timecard_bean->over_apply_disp_flg == "t" || $timecard_bean->over_apply_disp_flg == "1") ? "残業申請不要" : "";
						echo ("<a href=\"javascript:void(0);\" onclick=\"window.open('overtime_apply.php?session=$session&date=$tmp_date&pnt_url=$enc_cur_url', 'newwin', 'width=640,height=600,scrollbars=yes');\">".nbsp(hhmm_to_hmm($show_end_time))."</a></font><br><font size=\"2\" class=\"j10\">$apply_stat_nm</font>");
					break;
				}
				echo("</td>\n");
				//勤務
				echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($kinmu_time)) . "$hspacer</font></td>\n"); //$work_time + $wk_return_timeから変更 20100910
				//遅刻 20100907 赤色に変更 20100922
				echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"red\">" . nbsp(minute_to_hmm($start_time_info["diff_minutes"])) . "$hspacer</font></td>\n");
				//早退 20100907 赤色に変更 20100922
				echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"red\">" . nbsp(minute_to_hmm($end_time_info["diff_minutes"])) . "$hspacer</font></td>\n");
				if ($timecard_bean->over_disp_split_flg != "f"){
					//法内残
					echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($wk_hoteinai_zan)) . "$hspacer</font></td>\n");

					//法外残
					echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($wk_hoteigai)) . "$hspacer</font></td>\n");
				} else {
					//残業=法内残+法外残
					echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($wk_hoteinai_zan+$wk_hoteigai)) . "$hspacer</font></td>\n");
				}
			}

			//深勤
			echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($time2) . "$hspacer</font></td>\n");
			//普外−＞外出に変更 20090603
			echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($time_outtime) . "$hspacer</font></td>\n");
			//残外−＞休憩に変更 20090603
			echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($time_rest) . "$hspacer</font></td>\n");

			if ($timecard_bean->ret_display_flag == "t"){
				$ret1_str = ($timecard_bean->return_icon_flg != "2") ? "復帰" : "呼出";
				//回数（$return_count）を削除し、時間に変更 20090603
				echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
				switch ($return_link_type) {
				case "0":  // 申請不要
					echo(nbsp($return_time) . $hspacer);
					break;
				case "1":  // 未申請
						$apply_stat_nm = ($timecard_bean->over_apply_disp_flg == "t") ? "{$ret1_str}未申請" : "";
						echo("<a href=\"javascript:void(0);\" onclick=\"window.open('return_apply.php?session=$session&date=$tmp_date&pnt_url=$enc_cur_url', 'newwin', 'width=640,height=600,scrollbars=yes');\">" . nbsp($return_time) . "</a><br><font size=\"2\" class=\"j10\">$apply_stat_nm</font>");
					break;
				case "2":  // 申請中
					// 退勤後復帰申請承認者情報取得
					$arr_rtnaprv = $atdbk_workflow_common_class->get_approve_list($return_apply_id, "RTN");
					// 承認がひとつでもあるかチェック
					$apv_1_flg = $atdbk_workflow_common_class->get_apv_flg($arr_rtnaprv);
					//残業申請状況表示フラグ
					if ($timecard_bean->over_apply_disp_flg == "t" || $timecard_bean->over_apply_disp_flg == "1") {
						// 一部承認
						if($apv_1_flg)
						{
							$apply_stat_nm = "一部承認";
						}
						else
						{
								$apply_stat_nm = "{$ret1_str}申請中";
						}
					} else {
						$apply_stat_nm = "";
					}

					echo("<a href=\"javascript:void(0);\" onclick=\"show_sub_window('return_detail.php?session=$session&apply_id=$return_apply_id&pnt_url=$enc_cur_url', 'newwin', 'width=640,height=480,scrollbars=yes');\">" . nbsp($return_time) . "</a><br><font size=\"2\" color=\"red\" class=\"j10\">$apply_stat_nm</font>");
					break;
				case "3":  // 承認済
						$apply_stat_nm = ($timecard_bean->over_apply_disp_flg == "t" || $timecard_bean->over_apply_disp_flg == "1") ? "{$ret1_str}承認済" : "";
						echo("<a href=\"javascript:void(0);\" onclick=\"show_sub_window('return_refer.php?session=$session&apply_id=$return_apply_id', 'newwin', 'width=640,height=480,scrollbars=yes');\">" . nbsp($return_time) . "</a><br><font size=\"2\" class=\"j10\">$apply_stat_nm</font>");
					break;
				case "4":  // 否認済
						$apply_stat_nm = ($timecard_bean->over_apply_disp_flg == "t" || $timecard_bean->over_apply_disp_flg == "1") ? "{$ret1_str}否認済" : "";
						echo("<a href=\"javascript:void(0);\" onclick=\"show_sub_window('return_refer.php?session=$session&apply_id=$return_apply_id', 'newwin', 'width=640,height=480,scrollbars=yes');\">" . nbsp($return_time) . "</a><br><font size=\"2\" class=\"j10\">$apply_stat_nm</font>");
					break;
				case "5":  // 差戻済
						$apply_stat_nm = ($timecard_bean->over_apply_disp_flg == "t" || $timecard_bean->over_apply_disp_flg == "1") ? "{$ret1_str}差戻済" : "";
						echo("<a href=\"javascript:void(0);\" onclick=\"show_sub_window('return_back.php?session=$session&apply_id=$return_apply_id&pnt_url=$enc_cur_url', 'newwin', 'width=640,height=480,scrollbars=yes');\">" . nbsp($return_time) . "</a><br><font size=\"2\" color=\"red\" class=\"j10\">$apply_stat_nm</font>");
					break;
				}
				echo("</font></td>\n");
				//時間
//				echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($return_time) . "$hspacer</font></td>\n");
			}

			// 日数
			// 出勤・退勤時刻がある場合のみ表示する 20090702 //明けを追加 20110819
			if (!(($start_time != "" && $end_time != "") || $after_night_duty_flag == "1")) {
				$total_workday_count = "";
			}
			echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . $total_workday_count . "<br></font></td>\n");

			//会議研修
			if($meeting_display_flag){
				echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
				$meeting_time_hh = "";
				$meeting_time_mm = "";
				// 開始終了時刻から時間を計算する 20091008
				if ($meeting_start_time != "" && $meeting_end_time != "") {
					$tmp_meeting_start_date_time = $tmp_date.$meeting_start_time;
					if ($meeting_start_time <= $meeting_end_time) {
						$tmp_meeting_end_date_time = $tmp_date.$meeting_end_time;
					} else {
						// 日またがりの場合
						$tmp_meeting_end_date_time = next_date($tmp_date).$meeting_end_time;
					}
					$tmp_meeting_time = date_utils::get_time_difference($tmp_meeting_end_date_time, $tmp_meeting_start_date_time);
					$tmp_meeting_hhmm = $timecard_common_class->minute_to_h_hh($tmp_meeting_time);
					echo($tmp_meeting_hhmm);
				} else
				if ($meeting_time != null){
					$meeting_time_hh = (int)substr($meeting_time, 0, 2);
					$meeting_time_mm = (int)substr($meeting_time, 2, 4);
	/*
					if ($meeting_time_hh > 0){
						echo($meeting_time_hh."時間");
					}
					if ($meeting_time_mm > 0){
						echo($meeting_time_mm."分");
					}
	*/
					echo($meeting_time_hh);
					if($meeting_time_mm == 15)
					{
						echo(".25");
					}
					else if($meeting_time_mm == 30)
					{
						echo(".5");
					}
					else if($meeting_time_mm == 45)
					{
						echo(".75");
					}

				}
				echo("<br></font></td>\n");
			}

			echo("</tr>\n");
		}
		//締めの場合
		else {

		$time_cell_class = "tm";
		if ($show_time_flg != "t") {
			$time_cell_class = "mark";
			if ($start_time != "") {
				$start_time = ($late_flg) ? "遅刻" : "○";
			} else {
				$start_time = "";
			}
			if ($end_time != "") {
				$end_time = ($early_flg) ? "早退" : "○";
			} else {
				$end_time = "";
			}
		}

		//出・退勤時刻文字サイズ
		$time_font = "\"j12\"";
		if ($timecard_bean->time_big_font_flag == "t" && $show_time_flg == "t"){
			$time_font = "\"y16\"";
		}

		// 表示処理
		$rowspan = ($view == "1") ? "1" : "2";
		//残業申請状況の表示
		if ($timecard_bean->over_apply_disp_flg == "t" || $timecard_bean->over_apply_disp_flg == "1") {
			$hspacer = ($modify_apply_id != "" || ($view == "1" && ($overtime_apply_id != "" || $return_apply_id != ""))) ? "<br><font size=\"2\" class=\"j10\">&nbsp;</font>" : "";
		} else {
			$hspacer = "";
		}
			echo("<tr bgcolor=\"$bgcolor\">\n");
		//日付
		echo("<td rowspan=\"$rowspan\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		if ($modify_apply_id == "") {
			echo("{$tmp_month}月{$tmp_day}日{$hspacer}");
		} else {
			echo("<a href=\"javascript:void(0);\" onclick=\"window.open('timecard_modify_refer.php?session=$session&apply_id=$modify_apply_id', 'newwin', 'width=900,height=700,scrollbars=yes');\">{$tmp_month}月{$tmp_day}日</a><br><font size=\"2\" class=\"j10\">$modify_apply_status</font>");
		}
		echo("</font></td>\n");
		//曜日
		echo("<td rowspan=\"$rowspan\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . get_weekday(to_timestamp($tmp_date)) . "$hspacer</font></td>\n");

		if ($timecard_bean->time_move_flag == "t" && $view != "2"){

			if ($timecard_bean->zen_display_flag == "t"){

				// 前日になる場合があるフラグ
				$previous_day_value = "";
				if ($previous_day_flag == 1){
					$previous_day_value = "前";
				}
				echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($previous_day_value) . "$hspacer</font></td>\n");
			}

			echo("<td class=\"$time_cell_class\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">" . nbsp(hhmm_to_hmm($start_time)) . "$hspacer</font></td>\n");

			if ($timecard_bean->yoku_display_flag == "t"){
				// 翌
				$next_day_value = "";
				if ($show_next_day_flag == 1){	//残業時刻日またがり対応 20100114
					$next_day_value = "翌";
				}
				echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($next_day_value) . "$hspacer</font></td>\n");
			}

			//退勤
			echo("<td class=\"$time_cell_class\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">");
			if ($overtime_apply_id == "") {
				echo(nbsp(hhmm_to_hmm($end_time)) . $hspacer);
			} else {
				echo("<a href=\"javascript:void(0);\" onclick=\"show_sub_window('overtime_refer.php?session=$session&apply_id=$overtime_apply_id', 'newwin', 'width=640,height=480,scrollbars=yes');\">" . nbsp(hhmm_to_hmm($end_time)) . "</a><br><font size=\"2\" class=\"j10\">$overtime_apply_status</font>");
			}
			echo("</font></td>\n");
			//勤務
				echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($kinmu_time)) . "$hspacer</font></td>\n"); //$work_time + $wk_return_timeから変更 20100910
				//遅刻 20100907 赤色に変更 20100922
				echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"red\">" . nbsp(minute_to_hmm($start_time_info["diff_minutes"])) . "$hspacer</font></td>\n");
				//早退 20100907 赤色に変更 20100922
				echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"red\">" . nbsp(minute_to_hmm($end_time_info["diff_minutes"])) . "$hspacer</font></td>\n");
				if ($timecard_bean->over_disp_split_flg != "f"){
				//法内残
				echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($wk_hoteinai_zan)) . "$hspacer</font></td>\n");

				//法外残
				echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($wk_hoteigai)) . "$hspacer</font></td>\n");
			} else {
				//残業=法内残+法外残
				echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($wk_hoteinai_zan+$wk_hoteigai)) . "$hspacer</font></td>\n");
			}
		}

		if ($timecard_bean->time_move_flag != "t"){
			//種別
			if ($timecard_bean->type_display_flag == "t"){
				echo("<td rowspan=\"$rowspan\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($type_name) . "$hspacer</font></td>\n");
			}
		}

		// 予実績表示の場合
		if ($view == "2") {

			// 処理日付の勤務予定情報を取得
			$prov_tmcd_group_id = $arr_wktotalp[$tmp_date]["tmcd_group_id"];
			$prov_pattern = $arr_wktotalp[$tmp_date]["pattern"];
			$prov_reason = $arr_wktotalp[$tmp_date]["reason"];
            $prov_night_duty = $arr_wktotalp[$tmp_date]["night_duty"];
			$prov_allow_id = $arr_wktotalp[$tmp_date]["allow_id"];

			$prov_start_time = $arr_wktotalp[$tmp_date]["prov_start_time"];
			$prov_end_time = $arr_wktotalp[$tmp_date]["prov_end_time"];

			if ($prov_start_time != "") {
				$prov_start_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $prov_start_time);
			}
			if ($prov_end_time != "") {
				$prov_end_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $prov_end_time);
			}

			// 表示値の編集
			$prov_tmcd_group_name = $atdbk_common_class->get_group_name($prov_tmcd_group_id);
//			$prov_pattern = $arr_attendance_pattern[$prov_pattern];
			$prov_pattern = $atdbk_common_class->get_pattern_name($prov_tmcd_group_id, $prov_pattern);
			$prov_reason = $atdbk_common_class->get_reason_name($prov_reason);

            if($prov_night_duty == "1")
            {
                $prov_night_duty = "有り";
            }
            else if($prov_night_duty == "2")
            {
                $prov_night_duty = "無し";
            }

// ************ oose add start 2008/02/05 *****************
			$prov_allow_contents = "";
			foreach($arr_allowance as $allowance)
			{
				if($prov_allow_id == $allowance["allow_id"])
				{
					$prov_allow_contents = $allowance["allow_contents"];
					break;
				}
			}
// ************ oose add end 2008/02/05 *****************

			if($timecard_bean->time_move_flag == "t"){
				if ($timecard_bean->zen_display_flag == "t"){
					echo("<td bgcolor=\"$bgcolor2\" class=\"lbl\">&nbsp;</td>\n");
				}
				echo("<td align=\"right\" bgcolor=\"$bgcolor2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">" . nbsp(hhmm_to_hmm($prov_start_time)) . "</font></td>\n");
				if ($timecard_bean->yoku_display_flag == "t"){
					echo("<td bgcolor=\"$bgcolor2\" class=\"lbl\">&nbsp;</td>\n");
				}
				echo("<td align=\"right\" bgcolor=\"$bgcolor2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">" . nbsp(hhmm_to_hmm($prov_end_time)) . "</font></td>\n");
				echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
					//遅刻 20100907
					echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
					//早退 20100907
					echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
					//20100209
					echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
					if ($timecard_bean->over_disp_split_flg != "f"){
						echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
					}
				}

			if ($timecard_bean->time_move_flag == "t"){
				//種別
				if ($timecard_bean->type_display_flag == "t"){
					echo("<td rowspan=\"$rowspan\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($type_name) . "$hspacer</font></td>\n");
				}
			}

			if ($timecard_bean->group_display_flag == "t"){
				echo("<td height=\"22\" bgcolor=\"$bgcolor2\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($prov_tmcd_group_name) . "</font></td>\n");
			}
			echo("<td height=\"22\" bgcolor=\"$bgcolor2\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($prov_pattern) . "</font></td>\n");
			echo("<td bgcolor=\"$bgcolor2\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($prov_reason) . "</font></td>\n");
			echo("<td bgcolor=\"$bgcolor2\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($prov_night_duty) . "</font></td>\n");
// ************ oose add start 2008/02/05 *****************
			echo("<td bgcolor=\"$bgcolor2\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($prov_allow_contents) . "</font></td>\n");
// ************ oose add end 2008/02/05 *****************

			if($timecard_bean->time_move_flag == "f"){
				if ($timecard_bean->zen_display_flag == "t"){
					echo("<td bgcolor=\"$bgcolor2\" class=\"lbl\">&nbsp;</td>\n");
				}
				echo("<td align=\"right\" bgcolor=\"$bgcolor2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">" . nbsp(hhmm_to_hmm($prov_start_time)) . "</font></td>\n");
				if ($timecard_bean->yoku_display_flag == "t"){
					echo("<td bgcolor=\"$bgcolor2\" class=\"lbl\">&nbsp;</td>\n");
				}
				echo("<td align=\"right\" bgcolor=\"$bgcolor2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">" . nbsp(hhmm_to_hmm($prov_end_time)) . "</font></td>\n");
				echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
					//遅刻 20100907
					echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
					//早退 20100907
					echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
					//20100209
					echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
					if ($timecard_bean->over_disp_split_flg != "f"){
						echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
					}
				}
			echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
			echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
			echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");

			if ($timecard_bean->ret_display_flag == "t"){
				echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
			}
			echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
			//会議研修表示
			if($meeting_display_flag){
				echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
			}
			echo("</tr>\n");

				echo("<tr bgcolor=\"$bgcolor\">\n");
		}

		if ($timecard_bean->time_move_flag == "t" && $view == "2"){
			if ($timecard_bean->zen_display_flag == "t"){

				// 前日になる場合があるフラグ
				$previous_day_value = "";
				if ($previous_day_flag == 1){
					$previous_day_value = "前";
				}
				echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($previous_day_value) . "$hspacer</font></td>\n");
			}

			echo("<td class=\"$time_cell_class\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">" . nbsp(hhmm_to_hmm($start_time)) . "$hspacer</font></td>\n");

			if ($timecard_bean->yoku_display_flag == "t"){
				// 翌
				$next_day_value = "";
/*
				時間による判定からフラグによる判定に変更
				時間は参照しない
				if ($wk_start_time != "" && $wk_end_time != "" && (substr("0$wk_start_time", -4) > substr("0$wk_end_time", -4) && $previous_day_flag != "1"))
*/
				if ($show_next_day_flag == 1){	//残業時刻日またがり対応 20100114
					$next_day_value = "翌";
				}
				echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($next_day_value) . "$hspacer</font></td>\n");
			}

			//退勤
			echo("<td class=\"$time_cell_class\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">");
			if ($overtime_apply_id == "") {
				echo(nbsp(hhmm_to_hmm($end_time)) . $hspacer);
			} else {
				echo("<a href=\"javascript:void(0);\" onclick=\"show_sub_window('overtime_refer.php?session=$session&apply_id=$overtime_apply_id', 'newwin', 'width=640,height=480,scrollbars=yes');\">" . nbsp(hhmm_to_hmm($end_time)) . "</a><br><font size=\"2\" class=\"j10\">$overtime_apply_status</font>");
			}
			echo("</font></td>\n");
			//勤務
				echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($kinmu_time)) . "$hspacer</font></td>\n"); //$work_time + $wk_return_timeから変更 20100910
				//遅刻 20100907 赤色に変更 20100922
				echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"red\">" . nbsp(minute_to_hmm($start_time_info["diff_minutes"])) . "$hspacer</font></td>\n");
				//早退 20100907 赤色に変更 20100922
				echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"red\">" . nbsp(minute_to_hmm($end_time_info["diff_minutes"])) . "$hspacer</font></td>\n");
				if ($timecard_bean->over_disp_split_flg != "f"){
					//法内残
					echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($wk_hoteinai_zan)) . "$hspacer</font></td>\n");

					//法外残
					echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($wk_hoteigai)) . "$hspacer</font></td>\n");
				} else {
					//残業=法内残+法外残
					echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($wk_hoteinai_zan+$wk_hoteigai)) . "$hspacer</font></td>\n");
				}
			}

		//残業申請状況の表示
		if ($timecard_bean->over_apply_disp_flg == "t" || $timecard_bean->over_apply_disp_flg == "1") {
			$hspacer = (($view == "1" && $modify_apply_id != "") || $overtime_apply_id != "" || $return_apply_id != "") ? "<br><font size=\"2\" class=\"j10\">&nbsp;</font>" : "";
		} else {
			$hspacer = "";
		}

		if ($timecard_bean->time_move_flag == "t" && $view != "2"){
			//種別
			if ($timecard_bean->type_display_flag == "t"){
				echo("<td rowspan=\"$rowspan\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($type_name) . "$hspacer</font></td>\n");
			}
		}

		//グループ
		if ($timecard_bean->group_display_flag == "t"){
			echo("<td height=\"22\" class=\"txt\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
			echo(nbsp($atdbk_common_class->get_group_name($tmcd_group_id)));
			echo("$hspacer</font></td>\n");
		}

		//パターン
		echo("<td height=\"22\" class=\"txt\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
//		echo(nbsp($arr_attendance_pattern[$pattern]));
		echo(nbsp($atdbk_common_class->get_pattern_name($tmcd_group_id, $pattern)));
		echo("$hspacer</font></td>\n");
		echo("<td class=\"txt\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		echo(nbsp($atdbk_common_class->get_reason_name($reason)));
		echo("$hspacer</font></td>\n");

        $tmp_night_duty = "";
        if($night_duty == "1")
        {
            $tmp_night_duty = "有り";
        }
        else if($night_duty == "2")
        {
            $tmp_night_duty = "無し";
        }
	    echo("<td class=\"txt\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		echo(nbsp($tmp_night_duty));
	    echo("$hspacer</font></td>\n");

// ************ oose add start 2008/02/01 *****************
		//手当表示
		echo("<td class=\"txt\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		$allow_contents = "";
		foreach($arr_allowance as $allowance)
		{
			if($allow_id == $allowance["allow_id"])
			{
				$allow_contents = $allowance["allow_contents"];
				break;
			}
		}
		echo(nbsp($allow_contents));
		echo("$hspacer</font></td>\n");
// ************ oose add end 2008/02/01 *****************
/*
		$time_cell_class = "tm";
		if ($show_time_flg != "t") {
			$time_cell_class = "mark";
			if ($start_time != "") {
				$start_time = ($late_flg) ? "遅刻" : "○";
			} else {
				$start_time = "";
			}
			if ($end_time != "") {
				$end_time = ($early_flg) ? "早退" : "○";
			} else {
				$end_time = "";
			}
		}
*/
		if ($timecard_bean->time_move_flag == "f" || empty($timecard_bean->time_move_flag)){

			if ($timecard_bean->zen_display_flag == "t"){

				// 前日になる場合があるフラグ
				$previous_day_value = "";
				if ($previous_day_flag == 1){
					$previous_day_value = "前";
				}
				echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($previous_day_value) . "$hspacer</font></td>\n");
			}

			echo("<td class=\"$time_cell_class\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">" . nbsp(hhmm_to_hmm($start_time)) . "$hspacer</font></td>\n");

			if ($timecard_bean->yoku_display_flag == "t"){
				// 翌
				$next_day_value = "";
/*
				時間による判定からフラグによる判定に変更
				時間は参照しない
				if ($wk_start_time != "" && $wk_end_time != "" && (substr("0$wk_start_time", -4) > substr("0$wk_end_time", -4) && $previous_day_flag != "1"))
*/
				if ($show_next_day_flag == 1){	//残業時刻日またがり対応 20100114
					$next_day_value = "翌";
				}
				echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($next_day_value) . "$hspacer</font></td>\n");
			}

			//退勤
			echo("<td class=\"$time_cell_class\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">");
			if ($overtime_apply_id == "") {
				echo(nbsp(hhmm_to_hmm($end_time)) . $hspacer);
			} else {
				echo("<a href=\"javascript:void(0);\" onclick=\"show_sub_window('overtime_refer.php?session=$session&apply_id=$overtime_apply_id', 'newwin', 'width=640,height=480,scrollbars=yes');\">" . nbsp(hhmm_to_hmm($end_time)) . "</a><br><font size=\"2\" class=\"j10\">$overtime_apply_status</font>");
			}
			echo("</font></td>\n");
			//勤務
				echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($kinmu_time)) . "$hspacer</font></td>\n"); //$work_time + $wk_return_timeから変更 20100910
				//遅刻 20100907 赤色に変更 20100922
				echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"red\">" . nbsp(minute_to_hmm($start_time_info["diff_minutes"])) . "$hspacer</font></td>\n");
				//早退 20100907 赤色に変更 20100922
				echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"red\">" . nbsp(minute_to_hmm($end_time_info["diff_minutes"])) . "$hspacer</font></td>\n");
				if ($timecard_bean->over_disp_split_flg != "f"){
					//法内残
					echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($wk_hoteinai_zan)) . "$hspacer</font></td>\n");

					//法外残
					echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($wk_hoteigai)) . "$hspacer</font></td>\n");
				} else {
					//残業=法内残+法外残
					echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($wk_hoteinai_zan+$wk_hoteigai)) . "$hspacer</font></td>\n");
				}
			}

		//深勤
		echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($time2) . "$hspacer</font></td>\n");
		//普外−＞外出に変更 20090603
		echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($time_outtime) . "$hspacer</font></td>\n");
		//残外−＞休憩に変更 20090603
		echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($time_rest) . "$hspacer</font></td>\n");

		if ($timecard_bean->ret_display_flag == "t"){
				$ret1_str = ($timecard_bean->return_icon_flg != "2") ? "復帰" : "呼出";
				//回数（$return_count）を削除し、時間に変更 20090603
			echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
			if ($return_apply_id == "") {
				echo(nbsp($return_time) . $hspacer);
			} else {
					if ($rtn_apply_status != "" && ($timecard_bean->over_apply_disp_flg == "t" || $timecard_bean->over_apply_disp_flg == "1")) {
						$return_apply_status = ($rtn_apply_status == "1") ? "{$ret1_str}承認済" : "{$ret1_str}否認済";
					} else {
						$return_apply_status = "";
					}
					echo("<a href=\"javascript:void(0);\" onclick=\"window.open('return_refer.php?session=$session&apply_id=$return_apply_id', 'newwin', 'width=640,height=480,scrollbars=yes');\">" . nbsp($return_time) . "</a><br><font size=\"2\" class=\"j10\">$return_apply_status</font>");
			}
			echo("</font></td>\n");
			//時間
//			echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($return_time) . "$hspacer</font></td>\n");
		}
		//日数
		echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . $work_workday_count . "<br></font></td>\n");

		//会議研修表示
		if($meeting_display_flag){
			$meeting_time_hh = "";
			$meeting_time_mm = "";
			$meeting_time_hhmm = "";
			// 開始終了時刻から時間を計算する 20091008
			if ($meeting_start_time != "" && $meeting_end_time != "") {
				$tmp_meeting_start_date_time = $tmp_date.$meeting_start_time;
				if ($meeting_start_time <= $meeting_end_time) {
					$tmp_meeting_end_date_time = $tmp_date.$meeting_end_time;
				} else {
					// 日またがりの場合
					$tmp_meeting_end_date_time = next_date($tmp_date).$meeting_end_time;
				}
				$tmp_meeting_time = date_utils::get_time_difference($tmp_meeting_end_date_time, $tmp_meeting_start_date_time);
				$tmp_meeting_hhmm = $timecard_common_class->minute_to_h_hh($tmp_meeting_time);
				$meeting_time_hhmm = $tmp_meeting_hhmm;
			} else
			if ($meeting_time != null){
				$meeting_time_hh = (int)substr($meeting_time, 0, 2);
				$meeting_time_mm = (int)substr($meeting_time, 2, 4);
				$meeting_time_hhmm = $meeting_time_hh;

				if ($meeting_time_mm == 15)
				{
					$meeting_time_hhmm .= ".25";
				}
				else if($meeting_time_mm == 30)
				{
					$meeting_time_hhmm .= ".5";
				}
				else if($meeting_time_mm == 45)
				{
					$meeting_time_hhmm .= ".75";
				}
			}

			echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($meeting_time_hhmm) . "</font></td>\n");
		}

		echo("</tr>\n");
		}
//////////////////////////////////////////////////////////
//echo("<br>tmp_date=$tmp_date"); //debug
//echo(" work_time=$work_time"); //debug
//echo(" work_time_for_week=$work_time_for_week"); //debug
//echo(" last_day_total=$last_day_total"); //debug
//echo(" wk_return_time=$wk_return_time"); //debug
//echo(" work_workday_count=$work_workday_count"); //debug

//echo(" h $hoteinai:$hoteigai"); //debug
		//事由が休日出勤の場合 変更 2008/10/23
//		if ($reason == "16") {
//			$sums[31] += $work_time + $wk_return_time;
//		}

		$tmp_date = next_date($tmp_date);
		$counter_for_week++;
	}

// end of while ($tmp_date <= $end_date)

	// 早退時間を残業時間から減算する設定の場合、法定内残業、法定外残業の順に減算する
	if ($early_leave_time_flg == "1") {

		list($hoteinai_zan, $hoteigai) = $atdbk_common_class->get_overtime_early_leave($hoteinai_zan, $hoteigai, $early_leave_time);
		//残業時間 20100713
		$sums[14] = $hoteinai_zan + $hoteigai;
		/*
		//法定内時間がある場合
		if ($hoteinai > 0) {
			//減算
			$hoteinai_zan -= $early_leave_time;
		}
		//法定内時間がマイナスになる（残りがある）場合
		if ($hoteinai_zan < 0) {
			//法定外時間から減算
			$hoteigai += $hoteinai_zan;
			$hoteinai_zan = 0;
			//法定外時間がマイナスになる場合
			if ($hoteigai < 0) {
				//法定内時間に表示
				$hoteinai_zan = $hoteigai;
				$hoteigai = 0;
			}
		}
*/
	}
	// 要勤務日数を取得 20091222
	$wk_year = substr($yyyymm, 0, 4);
	$wk_mon = substr($yyyymm, 4, 2);
//	$sums[0] = get_you_kinmu_su($con, $fname, $wk_year, $wk_mon, $arr_timecard_holwk_day, $closing);
	//当月日数
	if ($closing_month_flg == "2") { //締め日の月度が終了日の月の場合
		$wk_year2 = substr($start_date, 0, 4);
		$wk_mon2 = substr($start_date, 4, 2);
	} else {
		$wk_year2 = $wk_year;
		$wk_mon2 = $wk_mon;
	}
	$days_in_month = days_in_month($wk_year2, $wk_mon2);
	//対象年月の公休数を取得 20100615
	$legal_hol_cnt = get_legal_hol_cnt_month($con, $fname, $wk_year, $wk_mon, $closing, $closing_month_flg, $tmcd_group_id);
	$sums[0] = $days_in_month - $legal_hol_cnt;

	// 休日出勤 2008/10/23
	// 休日出勤のカウント方法変更 20091127
//	$sums[2] = count($arr_hol_work_date);
	// 法定内 = 勤務時間 - 残業時間
	//$sums[28] = $hoteinai; //20100713
	$sums[28] = $sums[13] - $sums[14];
	// 法定内残業
	$sums[29] = $hoteinai_zan;
	// 法定外残業
	$sums[30] = $hoteigai;

	// 変則労働期間が月の場合
	if ($arr_irrg_info["irrg_type"] == "2") {

		// 所定労働時間を基準時間とする
		$tmp_irrg_minutes = $arr_irrg_info["irrg_minutes"];
		if ($hol_minus == "t") {
			$tmp_irrg_minutes -= (8 * 60 * $holiday_count);
		}
		$sums[12] = ($tmp_irrg_minutes > 0) ? $tmp_irrg_minutes : 0;

		// 稼働時間−基準時間を残業時間とする
		$sums[14] = $sums[13] - $sums[12];
	}
	else {
	// 基準時間の計算を変更 2008/10/21
	// 要勤務日数 × 所定労働時間
		$sums[12] = $sums[0] * date_utils::hi_to_minute($day1_time);
	}
// debug
//echo(" before 13=".$sums[13]);
	// 月集計値の稼働時間・残業時間・深夜勤務を端数処理
	for ($i = 13; $i <= 15; $i++) {
		if ($sums[$i] < 0) {
			$sums[$i] = 0;
		}
		$sums[$i] = round_time($sums[$i], $fraction4, $fraction4_min);
	}
//echo(" after 13=".$sums[13]);

	// 残業管理をしない場合、残業時間、法定内勤務、法定内残業、法定外残業を０とする
	if ($no_overtime == "t") {
		$sums[14] = 0;
		$sums[28] = 0;
		$sums[29] = 0;
		$sums[30] = 0;
	}

	//時間有休 20111207
	if ($obj_hol_hour->paid_hol_hour_flag == "t") {
		//１日分の所定労働時間(分)
		$specified_time_per_day = $obj_hol_hour->get_specified_time($emp_id, $day1_time);

	}
	// 月集計値を表示用に編集
	for ($i = 0; $i <= 9; $i++) {
		$sums[$i] .= "日";
	}
    //当月分の時間有休を有休年休欄に追加出力 20120604
    if ($obj_hol_hour->paid_hol_hour_flag == "t") {
        //移行データ
        if ($sums[40] > 0) {
            $wk_hour = $sums[40] / 60;
            $sums[7] .= "(".$wk_hour."時間)";
        }
    }
	for ($i = 10; $i <= 11; $i++) {
		$sums[$i] .= "回";
	}
	for ($i = 12; $i <= 15; $i++) {
		$sums[$i] = ($sums[$i] == 0) ? "0:00" : minute_to_hmm($sums[$i]);
	}
	for ($i = 16; $i <= 27; $i++) {
		$sums[$i] .= "日";
	}
	for ($i = 28; $i <= 31; $i++) {
		$sums[$i] = ($sums[$i] == 0) ? "0:00" : minute_to_hmm($sums[$i]);
	}
	//年末年始追加 20090908
	$sums[33] .= "日";
	//支給換算日数追加 20090908
	//$sums[34] = $paid_day_count; //半日有休対応 20110204

	//年休残追加 20100625
	$sums[35] = $timecard_common_class->get_nenkyu_zan($wk_year, $wk_mon, $emp_id, $timecard_bean->closing_paid_holiday, $start_date, $end_date); //."日";
	//時間有休 20111207
	if ($obj_hol_hour->paid_hol_hour_flag == "t") {
        //時間有休がある場合の年休残文字列取得 20120604
        $ret_str = $obj_hol_hour->get_nenkyu_zan_str($wk_year, $wk_mon, $emp_id, $timecard_bean->closing_paid_holiday, $timecard_bean->criteria_months, $start_date, $end_date, $specified_time_per_day, $sums[35], $duty_form, "", $timecard_common_class);
        $sums[35] = $ret_str;
    }
    else {
        $sums[35] .= "日";
    }

	//法定追加 20110121
	$sums[36] .= "日";
	//所定追加 20110121
	$sums[37] .= "日";

	//変数の解放
	unset($timecard_common_class);
	unset($arr_time2);
	unset($arr_time4);
/*
	// 締めデータがあるが追加事由分がない場合、追加事由分を設定
	if ($atdbk_closed) {
		//追加事由分がない場合
		if ($sums[32] != "") {
			for ($i = 16; $i <= 27; $i++) {
				$tmp_sums[$i] = $sums[$i];
			}
		}
		//法定内勤務がない場合
		if ($sums[28] != "") {
			for ($i = 28; $i <= 31; $i++) {
				$tmp_sums[$i] = $sums[$i];
			}
		}
		return $tmp_sums;
	}
*/
	return $sums;
}



?>
