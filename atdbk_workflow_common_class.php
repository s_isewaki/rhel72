<?php
//出勤表申請関連クラス
require_once("Cmx.php");
require_once('Cmx/Model/SystemConfig.php');
require_once("about_comedix.php");

class atdbk_workflow_common_class
{

	var $file_name;	// 呼び出し元ファイル名
	var $_db_con;	// DBコネクション
	
	var $check_skip_flg; //承認階層開始判定によるスキップフラグtrue/false

	/**
	 * コンストラクタ
	 * @param object $con DBコネクション
	 * @param string $fname 画面名
	 */
	function atdbk_workflow_common_class($con, $fname)
	{
		// 呼び出し元ファイル名を保持
		$this->file_name = $fname;
		// DBコネクションを保持
		$this->_db_con = $con;

		// system_config 20151118
		$conf = new Cmx_SystemConfig();
		// 承認階層開始判定
		$aprv_skip_flg = $conf->get('timecard.aprv_skip_flg');
        $this->check_skip_flg = ($aprv_skip_flg == "t");
	}

//-------------------------------------------------------------------------------------------------------------------------
// ワークフロー登録・更新・削除関連
//-------------------------------------------------------------------------------------------------------------------------

	// 承認者ワークフロー登録
	function regist_atdbk_wkfw($ovtm_wkfw_div, $time_wkfw_div, $rtn_wkfw_div)
	{

		$sql = "insert into atdbk_wkfw ";
		$sql .= "(ovtm_wkfw_div, time_wkfw_div, rtn_wkfw_div) values(";
		$content = array($ovtm_wkfw_div, $time_wkfw_div, $rtn_wkfw_div);

		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

    function get_atdbk_wkfw()
    {
        $sql  = "select * from atdbk_wkfw ";
        $cond = "";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

        $arr = array();
        while($row = pg_fetch_array($sel))
        {
             $arr = array(
                              "ovtm_wkfw_div" => $row["ovtm_wkfw_div"],
                              "ovtmcfm" => $row["ovtmcfm"],
                              "time_wkfw_div" => $row["time_wkfw_div"],
                              "timecfm" => $row["timecfm"],
                              "rtn_wkfw_div" => $row["rtn_wkfw_div"],
                              "rtncfm" => $row["rtncfm"],
                          );

        }
        return $arr;
    }

	// 承認者ワークフロー更新
    function update_atdbk_wkfw($arr)
    {
		$sql = "update atdbk_wkfw set";
		$set = array_keys($arr);
		$setvalue = array_values($arr);
        $cond = "";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
    }

	// 承認者管理登録
	function regist_atdbk_apvmng($apv_order, $approve_mng)
	{

		$target_class_div = $approve_mng["target_class_div"];
		$multi_apv_flg    = $approve_mng["multi_apv_flg"];
		$next_notice_div  = $approve_mng["next_notice_div"];
		$apv_div0_flg     = $approve_mng["apv_div0_flg"];
		$apv_div1_flg     = $approve_mng["apv_div1_flg"];
		$apv_div4_flg     = $approve_mng["apv_div4_flg"];
		$wkfw_div         = $approve_mng["wkfw_div"];

		$sql = "insert into atdbk_apvmng ";
		$sql .= "(apv_order, target_class_div, multi_apv_flg, next_notice_div, apv_div0_flg, apv_div1_flg, apv_div4_flg, wkfw_div) values(";
		$content = array($apv_order, $target_class_div, $multi_apv_flg, $next_notice_div, $apv_div0_flg, $apv_div1_flg, $apv_div4_flg, $wkfw_div);

		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
	
	// 部署役職指定登録
	function regist_atdbk_apvpstdtl($apv_order, $st_id, $st_div, $wkfw_div)
	{

		$sql = "insert into atdbk_apvpstdtl ";
		$sql .= "(apv_order, st_id, st_div, wkfw_div) values(";
		$content = array($apv_order, $st_id, $st_div, $wkfw_div);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
	
	// 職員指定登録
	function regist_atdbk_apvdtl($apv_order, $emp_id, $apv_sub_order, $wkfw_div)
	{

		$sql = "insert into atdbk_apvdtl ";
		$sql .= "(apv_order, emp_id, apv_sub_order, wkfw_div) values(";
		$content = array($apv_order, $emp_id, $apv_sub_order, $wkfw_div);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 部署役職(部署指定)
	function regist_atdbk_apvsectdtl($apv_order, $class_id, $atrb_id, $dept_id, $room_id, $wkfw_div)
	{

		$atrb_id = ($atrb_id == "") ? null : $atrb_id;
		$dept_id = ($dept_id == "") ? null : $dept_id;
		$room_id = ($room_id == "") ? null : $room_id;

		$sql = "insert into atdbk_apvsectdtl ";
		$sql .= "(apv_order, class_id, atrb_id, dept_id, room_id, wkfw_div) values(";
		$content = array($apv_order, $class_id, $atrb_id, $dept_id, $room_id, $wkfw_div);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
	
	// 承認者ワークフロー削除
	function delete_atdbk_wkfw()
	{

		$sql = "delete from atdbk_wkfw";
		$cond = "";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
	
	// 承認者管理削除
	function delete_atdbk_apvmng($wkfw_div)
	{
		$sql = "delete from atdbk_apvmng";
		$cond = "where wkfw_div = $wkfw_div";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 部署役職削除
	function delete_atdbk_pvpstdtl($wkfw_div)
	{
		$sql = "delete from atdbk_apvpstdtl";
		$cond = "where wkfw_div = $wkfw_div";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 職員指定削除
	function delete_atdbk_apvdtl($wkfw_div)
	{
		$sql = "delete from atdbk_apvdtl";
		$cond = "where wkfw_div = $wkfw_div";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 部署役職(部署指定)
	function delete_atdbk_apvsectdtl($wkfw_div)
	{
		$sql = "delete from atdbk_apvsectdtl";
		$cond = "where wkfw_div = $wkfw_div";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


//-------------------------------------------------------------------------------------------------------------------------
// 申請関連
//-------------------------------------------------------------------------------------------------------------------------

	// 職員の部署役職取得
	function get_emp_info($emp_id)
	{
		// 職員情報取得(複数部署役職対応)
		$sql  = "select emp_class, emp_attribute, emp_dept, emp_room, emp_st from empmst where emp_id = '$emp_id' ";
		$sql .= "union ";
		$sql .= "select emp_class, emp_attribute, emp_dept, emp_room, emp_st from concurrent where emp_id = '$emp_id'";

		$sel = select_from_table($this->_db_con, $sql, "", $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("emp_class" => $row["emp_class"], "emp_attribute" => $row["emp_attribute"], "emp_dept" => $row["emp_dept"], "emp_room" => $row["emp_room"], "emp_st" => $row["emp_st"]);
		}

		return $arr;
	}
	
	// 承認者管理取得
	function get_wkfwapvmng($wkfw_div)
	{
		$sql  = "select apv_order, target_class_div, multi_apv_flg, next_notice_div, apv_div0_flg, apv_div1_flg, apv_div4_flg ";
		$sql .= "from atdbk_apvmng ";
		$cond = "where wkfw_div = $wkfw_div order by apv_order";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("apv_order" => $row["apv_order"],
                            "target_class_div" => $row["target_class_div"],
                            "multi_apv_flg" => $row["multi_apv_flg"],
                            "next_notice_div" => $row["next_notice_div"],
							"apv_div0_flg" => $row["apv_div0_flg"],
							"apv_div1_flg" => $row["apv_div1_flg"],
							"apv_div4_flg" => $row["apv_div4_flg"]
                           );
		}
		return $arr;
	}

	// 部署役職指定情報取得
	function get_wkfwapvpstdtl($apv_order, $target_class_div, $emp_id, $wkfw_div)
	{
		$arr = array();

		// 申請者の部署役職取得
		$arr_emp_info = $this->get_emp_info($emp_id);

		if($target_class_div == "4")
		{
			$fourth_post_exist_flg = false;
			foreach($arr_emp_info as $emp_info)
			{
				$emp_room  = $emp_info["emp_room"];
				if($emp_room != "")
				{
					$fourth_post_exist_flg = true;		
					break;
				}
			}

			if(!$fourth_post_exist_flg)
			{
				return $arr;
			}
		}

		$sql  = "select ";
		$sql .= "emp.emp_id, empmst.emp_lt_nm, empmst.emp_ft_nm, empmst.emp_class, empmst.emp_attribute, empmst.emp_dept, empmst.emp_room, empmst.emp_st, stmst.st_nm ";
		$sql .= "from ";
    	$sql .= "(";
    	$sql .= "select emp_id from empmst where "; 

		// 「部署指定しない」以外
		if($target_class_div != 0)
		{
			$sql .= "( ";
			$idx = 0;
			foreach($arr_emp_info as $emp_info)
			{
				$emp_class     = $emp_info["emp_class"];
				$emp_attribute = $emp_info["emp_attribute"];
				$emp_dept      = $emp_info["emp_dept"];
				$emp_room      = $emp_info["emp_room"];

				if($target_class_div == "4")
				{
					if($emp_room == "")
					{
						continue;
					}
				}

				if($idx > 0)
				{
					$sql .= "or ";
				}

				if($target_class_div == "1")
				{
					$sql .= "emp_class = $emp_class ";
				}
				else if($target_class_div == "2")
				{
					$sql .= "emp_attribute = $emp_attribute ";
				}
				else if($target_class_div == "3")
				{
					$sql .= "emp_dept = $emp_dept ";
				}
				else if($target_class_div == "4")
				{
					$sql .= "emp_room = $emp_room ";
				}
				$idx++;
			}	
			$sql .= ") ";
			$sql .= "and ";
		}
		$sql .= "exists (select st_id from atdbk_apvpstdtl where empmst.emp_st = atdbk_apvpstdtl.st_id and apv_order = $apv_order and st_div = 0 and wkfw_div = $wkfw_div) ";
		$sql .= "union ";
		$sql .= "select emp_id from concurrent where ";

		// 「部署指定しない」以外
		if($target_class_div != 0)
		{
			$sql .= "( ";
			$idx = 0;
			foreach($arr_emp_info as $emp_info)
			{
				$emp_class     = $emp_info["emp_class"];
				$emp_attribute = $emp_info["emp_attribute"];
				$emp_dept      = $emp_info["emp_dept"];
				$emp_room      = $emp_info["emp_room"];

				if($target_class_div == "4")
				{
					if($emp_room == "")
					{
						continue;
					}
				}

				if($idx > 0)
				{
					$sql .= "or ";
				}

				if($target_class_div == "1")
				{
					$sql .= "emp_class = $emp_class ";
				}
				else if($target_class_div == "2")
				{
					$sql .= "emp_attribute = $emp_attribute ";
				}
				else if($target_class_div == "3")
				{
					$sql .= "emp_dept = $emp_dept ";
				}
				else if($target_class_div == "4")
				{
					$sql .= "emp_room = $emp_room ";
				}
				$idx++;
			}	
			$sql .= ") ";
			$sql .= "and ";
		}
		$sql .= "exists (select st_id from atdbk_apvpstdtl where concurrent.emp_st = atdbk_apvpstdtl.st_id and apv_order = $apv_order and st_div = 0 and wkfw_div = $wkfw_div) ";
		$sql .= ") emp ";
		$sql .= "inner join empmst on emp.emp_id = empmst.emp_id ";
		$sql .= "inner join authmst on emp.emp_id = authmst.emp_id and not authmst.emp_del_flg ";
		$sql .= "inner join stmst on empmst.emp_st = stmst.st_id and not stmst.st_del_flg ";
		$cond = "order by emp.emp_id asc ";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$idx = 1;
		while($row = pg_fetch_array($sel))
		{
			$emp_full_nm = $row["emp_lt_nm"]." ".$row["emp_ft_nm"];
			$arr[] = array("emp_id" => $row["emp_id"],
			                "emp_full_nm" => $emp_full_nm,
			                "st_nm" => $row["st_nm"],
   			                "apv_sub_order" => $idx
			               );		
			$idx++;
		}
		return $arr;
	}

	// 職員指定情報取得
	function get_wkfwapvdtl($apv_order, $wkfw_div)
	{
		$sql   = "select a.emp_id, a.apv_sub_order, b.emp_lt_nm, b.emp_ft_nm, d.st_nm from atdbk_apvdtl a ";
		$sql  .= "inner join empmst b on a.emp_id = b.emp_id ";
		$sql  .= "inner join authmst c on a.emp_id = c.emp_id ";
		$sql  .= "and c.emp_del_flg = 'f' ";
		$sql  .= "inner join stmst d on b.emp_st = d.st_id ";
		$cond  = "where a.apv_order = $apv_order and a.wkfw_div = $wkfw_div";
		$cond .= "order by apv_sub_order asc";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$emp_full_nm = $row["emp_lt_nm"]." ".$row["emp_ft_nm"];
			$arr[] = array("emp_id" => $row["emp_id"],
			                "emp_full_nm" => $emp_full_nm,
			                "st_nm" => $row["st_nm"],
			                "apv_sub_order" => $row["apv_sub_order"]
			               );
		}
		return $arr;
	}

	// 承認者情報取得
	function get_wkfwapv_info($emp_id, $wkfw_div, $wkfw_flg)
	{
		$arr = array();
		$arr_wkfwapvmng = array();
		//個人指定承認者が指定済の場合は、ワークフロー指定承認者を除く 20100928
	    $arr_personal_approve = $this->get_personal_approve($wkfw_div, $emp_id);
		if(count($arr_personal_approve) == 0)
		{
			$skip_count = 0; //スキップ数
	        if($wkfw_flg)
	        {
			    $arr_wkfwapvmng = $this->get_wkfwapvmng($wkfw_div);
	        }

	        foreach ($arr_wkfwapvmng as $apvmng_i => $apvmng) {
	            $last_order_flg = ($apvmng_i == (count($arr_wkfwapvmng) - 1));

				$apvmng["apv_setting_flg"] = "f";

				$arr_apv = array();			
				$multi_apv_flg = $apvmng["multi_apv_flg"];

				$apv_div0_flg  = $apvmng["apv_div0_flg"];
				$apv_div1_flg  = $apvmng["apv_div1_flg"];
				$apv_div4_flg  = $apvmng["apv_div4_flg"];

				if($apv_div0_flg == "f" && $apv_div1_flg == "f" && $apv_div4_flg == "f")
				{
					$apvmng["apv_setting_flg"] = "t";
				}

				// 部署役職(申請者所属)指定
				if($apv_div0_flg == "t")
				{
					$arr_wkfwapvpstdtl = $this->get_wkfwapvpstdtl($apvmng["apv_order"], $apvmng["target_class_div"], $emp_id, $wkfw_div);

					for($i=0; $i<count($arr_wkfwapvpstdtl); $i++)
					{
						$arr_wkfwapvpstdtl[$i]["apv_div"] = "0";
					}

					// マージ処理
					$arr_apv = $this->merge_arr_emp_info($arr_apv, $arr_wkfwapvpstdtl);

				}
				// 部署役職(部署指定)指定
				if($apv_div4_flg == "t")
				{

	  				$arr_apvpstdtl = $this->get_apvpstdtl($apvmng["apv_order"], "1", $wkfw_div);
					$st_sect_ids = "";
					foreach($arr_apvpstdtl as $apvpstdtl)
					{
						if($st_sect_ids != "")
						{
							$st_sect_ids .= ",";
						}
						$st_sect_ids .= $apvpstdtl["st_id"];
					}
					$arr_sect_dtl = $this->get_apvsectdtl($apvmng["apv_order"], $wkfw_div);
					$arr_post_sect = $this->get_emp_info_for_post_sect($arr_sect_dtl["class_id"], $arr_sect_dtl["atrb_id"], $arr_sect_dtl["dept_id"], $arr_sect_dtl["room_id"], $st_sect_ids);

					for($i=0; $i<count($arr_post_sect); $i++)
					{
						$arr_post_sect[$i]["apv_div"] = "4";
					}

					// マージ処理
					$arr_apv = $this->merge_arr_emp_info($arr_apv, $arr_post_sect);
				}
				// 職員指定
				if($apv_div1_flg == "t")
				{
					$arr_wkfwapvdtl = $this->get_wkfwapvdtl($apvmng["apv_order"], $wkfw_div);
					for($i=0; $i<count($arr_wkfwapvdtl); $i++)
					{
						$arr_wkfwapvdtl[$i]["apv_div"] = "1";
					}

					// マージ処理
					$arr_apv = $this->merge_arr_emp_info($arr_apv, $arr_wkfwapvdtl);
				}

				// 複数承認者「許可する」
				if($multi_apv_flg == "t")
				{
					if(count($arr_apv) > 0)
					{
						if(count($arr_apv) > 1)
						{
	                        // 自分が含まれる場合はスキップ 20151118
	                        if ($this->check_skip_flg) {
	                            foreach ($arr_apv as $apv) {
	                                if ($apv["emp_id"] === $emp_id) {
	                                    $arr = array();
	                                    if (!$last_order_flg) {
	                                    	$skip_count = $apvmng_i + 1;
	                                    	continue 2;
	                                    }
	                                }
	                            }
	                        }

							$apv_sub_order = 1;
							foreach($arr_apv as $apv)
							{
								$arr_emp = array();
								$apvmng["apv_sub_order"] = $apv_sub_order;
								$arr_emp[] = array(
	                                               "emp_id" => $apv["emp_id"], 
	                                               "emp_full_nm" => $apv["emp_full_nm"], 
	                                               "st_nm" => $apv["st_nm"], 
	                                               "apv_div" => $apv["apv_div"], 
	                                               "pjt_nm" => $apv["pjt_nm"],
	                                               "parent_pjt_id" => $apv["parent_pjt_id"],
	                                               "child_pjt_id" => $apv["child_pjt_id"]
	                                               );
								$apvmng["emp_infos"] = $arr_emp;

								if($apv["apv_div"] == "2")
								{
									$apvmng["apv_setting_flg"] = "t";
								}

								$arr[] = $apvmng;
								$apv_sub_order++;
							}
						}
						else
						{
	                        // 自分が含まれる場合はスキップ 20151118
	                        if ($this->check_skip_flg && $arr_apv[0]["emp_id"] === $emp_id) {
	                            $arr = array();
	                            if (!$last_order_flg)  {
                                	$skip_count = $apvmng_i + 1;
		                            continue;
	                            }
	                        }

							$arr_emp = array();
							$apvmng["apv_sub_order"] = "";
							$arr_emp[] = array(
	                                           "emp_id" => $arr_apv[0]["emp_id"], 
	                                           "emp_full_nm" => $arr_apv[0]["emp_full_nm"], 
	                                           "st_nm" => $arr_apv[0]["st_nm"],
	                                           "apv_div" => $arr_apv[0]["apv_div"], 
	                                           "pjt_nm" => $arr_apv[0]["pjt_nm"],
	                                           "parent_pjt_id" => $arr_apv[0]["parent_pjt_id"],
	                                           "child_pjt_id" => $arr_apv[0]["child_pjt_id"]
	                                           );
							$apvmng["emp_infos"] = $arr_emp;

							if($apv["apv_div"] == "2")
							{
								$apvmng["apv_setting_flg"] = "t";
							}

							$arr[] = $apvmng;
						}	
					}
					else
					{
						$apvmng["emp_infos"] = array();
						$arr[] = $apvmng;
					}
				}
				// 複数承認者「許可しない」
				else
				{
	                // 自分が含まれる場合はスキップ 20151118
	                if ($this->check_skip_flg) {
	                    foreach ($arr_apv as $apv) {
	                        if ($apv["emp_id"] === $emp_id) {
	                            $arr = array();
                                if (!$last_order_flg) {
                                	$skip_count = $apvmng_i + 1;
                                	continue 2;
                                }

	                        }
	                    }
	                }

					$apvmng["apv_sub_order"] = "";
					$apvmng["emp_infos"] = $arr_apv;
					$arr[] = $apvmng;
				}
			}
			//スキップがあったらapv_orderを振りなおす 20151118
			if ($skip_count > 0) {
				$arr[0]["skip_count"] = $skip_count; //スキップ数を配列先頭に設定
				$wk_idx = 0;
				$wk_key = "";
				$arr_order_tbl = array();
				foreach ($arr as $arr_dat) {
					$wk_apv_order = $arr_dat["apv_order"];
					if ($wk_apv_order != $wk_key) {
						$wk_idx++;
						$arr_order_tbl[$wk_apv_order] = $wk_idx;
						$wk_key = $wk_apv_order;
					}
				}
				$wk_idx = 0;
				foreach ($arr as $arr_dat) {
					$wk_key = $arr[$wk_idx]["apv_order"];
					$arr[$wk_idx]["apv_order"] = $arr_order_tbl[$wk_key];
					$wk_idx++;
				}
			}
		}
        // 残業申請、勤務時間修正申請の場合、個人指定承認者をワークフロー指定承認者の後に表示する。
        // 退勤後復帰申請は、ワークフロー指定承認者の後には表示しない。
		//if($wkfw_div == "1" || $wkfw_div == "2" || ($wkfw_div == "3" && !$wkfw_flg)) //退勤後復帰も同様の処理とする 20100928
        //{
	        $arr_personal_approve = $this->get_personal_approve($wkfw_div, $emp_id);
	        if(count($arr_personal_approve) != 0)
	        {
	            $apv_order = count($arr_wkfwapvmng) + 1;
	            
	            $apv_personal = array();
	            $apv_personal["apv_order"] = $apv_order;
	            $apv_personal["multi_apv_flg"] = "f";
	            $apv_personal["apv_setting_flg"] = "f";
	            $apv_personal["emp_infos"][0] = $arr_personal_approve;
	            $arr[] = $apv_personal;
	        }
        //}

		return $arr;
	}

    // 個人指定の承認者取得
    function get_personal_approve($wkfw_div, $emp_id)
    {

        $sql  = "select a.emp_id, a.emp_lt_nm, a.emp_ft_nm, b.st_nm ";
        $sql .= "from empmst a ";
        $sql .= "left join stmst b on a.emp_st = b.st_id ";

        // 残業申請
        if($wkfw_div == "1")
        {
            $cond = "where a.emp_id = (select overtime_id from tmcdwkfw where emp_id = '$emp_id')";
        }
        // 勤務時間修正申請
        else if($wkfw_div == "2")
        {
            $cond = "where a.emp_id = (select modify_id from tmcdwkfw where emp_id = '$emp_id')";
        }
        // 退勤後復帰申請
        else if($wkfw_div == "3")
        {
            $cond = "where a.emp_id = (select return_id from tmcdwkfw where emp_id = '$emp_id')";
        }

        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }

        $arr = array();
        if (pg_num_rows($sel) > 0)
        {
             $emp_id      = pg_fetch_result($sel, 0, "emp_id");
	         $emp_full_nm = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");
             $st_nm       = pg_fetch_result($sel, 0, "st_nm");
             $arr = array("emp_id" => $emp_id, "emp_full_nm" => $emp_full_nm, "st_nm" => $st_nm);
        }
        return $arr;
    }
	
	// 部署役職(部署指定)情報取得
	function get_apvsectdtl($apv_order, $wkfw_div)
	{
		$sql   = "select a.class_id, a.atrb_id, a.dept_id, a.room_id ";
		$sql  .= "from atdbk_apvsectdtl a ";
		$cond  = "where a.apv_order = $apv_order and a.wkfw_div = $wkfw_div";
		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$class_id = pg_fetch_result($sel, 0, "class_id");
		$atrb_id = pg_fetch_result($sel, 0, "atrb_id");
		$dept_id = pg_fetch_result($sel, 0, "dept_id");
		$room_id = pg_fetch_result($sel, 0, "room_id");

		return array("class_id" => $class_id, "atrb_id" => $atrb_id, "dept_id" => $dept_id, "room_id" => $room_id);
	}
	
	// 役職情報取得
	function get_apvpstdtl($apv_order, $st_div, $wkfw_div)
	{
		$sql   = "select st_id from atdbk_apvpstdtl ";
		$cond  = "where apv_order = $apv_order and st_div = $st_div and wkfw_div = $wkfw_div order by st_id";
		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("st_id" => $row["st_id"]);
		}

		return $arr;
	}
	
	// 職員情報取得(部署役職指定用)
	function get_emp_info_for_post_sect($class_id, $attribute_id, $dept_id, $room_id, $st_sect_id)
	{

		$sql  = "select a.emp_id, a.emp_lt_nm, a.emp_ft_nm, c.st_nm ";
		$sql .= "from ( ";
		$sql .= "select varchar(1) '1' as type, emp_id, emp_lt_nm, emp_ft_nm, emp_st from empmst ";

		if($class_id != "")
		{
			$sql .= "where emp_class = $class_id ";
		}

		if($attribute_id != "")
		{
			$sql .= "and emp_attribute = $attribute_id ";
		}

		if($dept_id != "")
		{
			$sql .= "and emp_dept = $dept_id ";
		}

		if($room_id != "")
		{
			$sql .= "and emp_room = $room_id ";
		}

		if($st_sect_id)
		{
			$sql .= "and emp_st in ($st_sect_id) ";
		}

		$sql .= "union ";
		$sql .= "select varchar(1) '2' as type, sub_a.emp_id, sub_b.emp_lt_nm, sub_b.emp_ft_nm, sub_a.emp_st from concurrent sub_a ";
		$sql .= "inner join empmst sub_b on sub_a.emp_id = sub_b.emp_id ";

		if($class_id != "")
		{
			$sql .= "where sub_a.emp_class = $class_id ";
		}

		if($attribute_id != "")
		{
			$sql .= "and sub_a.emp_attribute = $attribute_id ";
		}

		if($dept_id != "")
		{
			$sql .= "and sub_a.emp_dept = $dept_id ";
		}

		if($room_id != "")
		{
			$sql .= "and sub_a.emp_room = $room_id ";
		}

		if($st_sect_id)
		{
			$sql .= "and sub_a.emp_st in ($st_sect_id) ";
		}

		$sql .= ") a ";
		$sql .= "inner join authmst b on a.emp_id = b.emp_id and not b.emp_del_flg ";
		$sql .= "left join stmst c on a.emp_st = c.st_id and not c.st_del_flg ";
		$sql .= "order by a.emp_id asc, a.type asc";

		$sel = select_from_table($this->_db_con, $sql, "", $this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		$tmp_emp_id = "";
		$idx = 1;
		while($row = pg_fetch_array($sel))
		{
			if($tmp_emp_id == $row["emp_id"])
			{
				continue;
			}
			$emp_full_nm = $row["emp_lt_nm"]." ".$row["emp_ft_nm"];
			$arr[] = array("emp_id" => $row["emp_id"],
			                "emp_full_nm" => $emp_full_nm,
			                "st_nm" => $row["st_nm"],
			                "apv_sub_order" => $idx);

			$tmp_emp_id = $row["emp_id"];
			$idx++;
		}
		return $arr;
	}
	
	// マージ処理
	function merge_arr_emp_info($arr_all_emp_info, $arr_target_emp_info)
	{
		$arr_tmp_apv = array();
		foreach($arr_target_emp_info as $target_emp_info)
		{
			$dpl_flg = false;
			foreach($arr_all_emp_info as $all_emp_info)
			{
				if($target_emp_info["emp_id"] == $all_emp_info["emp_id"])
				{
					$dpl_flg = true;		
				}
			}
			if(!$dpl_flg)
			{
				$arr_tmp_apv[] = $target_emp_info;
			}
		}

		for($i=0; $i<count($arr_tmp_apv); $i++)
		{
			array_push($arr_all_emp_info, $arr_tmp_apv[$i]);
		}

		return $arr_all_emp_info;
	}
	
//-------------------------------------------------------------------------------------------------------------------------
// 申請更新関連(残業申請、勤務時間修正申請、退勤後復帰申請)
//-------------------------------------------------------------------------------------------------------------------------

	// 申請情報取得
	function get_apply_wkfwmst($apply_id, $div)
	{
		$sql  = "select a.*, b.emp_lt_nm, b.emp_ft_nm ";

        switch($div)
        {
            case "OVTM":
                $sql .= "from ovtmapply a ";        
                break;
            case "TMMD":
                $sql .= "from tmmdapply a ";        
                break;
            case "RTN":
                $sql .= "from rtnapply a ";        
                break;
        }

		$sql .= "inner join empmst b on a.emp_id = b.emp_id ";
		
		$cond = "where a.apply_id = $apply_id ";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_all($sel);
	}
	
	// 承認情報取得
	function get_applyapv($apply_id, $div)
	{
		$sql  = "select a.*, b.emp_lt_nm, b.emp_ft_nm, c.st_nm  ";

        switch($div)
        {
            case "OVTM":
                $sql .= "from ovtmaprv a ";        
                break;
            case "TMMD":
                $sql .= "from tmmdaprv a ";        
                break;
            case "RTN":
                $sql .= "from rtnaprv a ";        
                break;
        }

		$sql .= "left join empmst b on a.aprv_emp_id = b.emp_id ";
		$sql .= "left join stmst c on a.emp_st = c.st_id ";
		
		$cond = "where a.apply_id = $apply_id order by a.aprv_no, a.aprv_sub_no asc";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_all($sel);
	}
	
	// 承認者候補情報取得
	function get_applyapvemp($apply_id, $apv_order, $div)
	{
		$sql  = "select a.*, b.emp_lt_nm, b.emp_ft_nm, c.emp_del_flg, d.st_nm ";

        switch($div)
        {
            case "OVTM":
                $sql .= "from ovtmaprvemp a ";        
                break;
            case "TMMD":
                $sql .= "from tmmdaprvemp a ";        
                break;
            case "RTN":
                $sql .= "from rtnaprvemp a ";        
                break;
        }

		$sql .= "inner join empmst b on a.emp_id = b.emp_id ";
		$sql .= "inner join authmst c on b.emp_id = c.emp_id ";
		$sql .= "left join stmst d on b.emp_st = d.st_id ";

		$cond = "where a.apply_id = $apply_id and a.aprv_no = $apv_order order by a.person_no asc";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_all($sel);
	}
	
//-------------------------------------------------------------------------------------------------------------------------
// 承認処理関連(残業申請、勤務時間修正申請、退勤後復帰申請)
//-------------------------------------------------------------------------------------------------------------------------

	// 階層ごとの承認者情報取得
	function get_applyapv_per_hierarchy($apply_id, $apv_order, $div)
	{
        switch($div)
        {
            case "OVTM":
                $sql = "select * from ovtmaprv ";        
                break;
            case "TMMD":
                $sql = "select * from tmmdaprv ";        
                break;
            case "RTN":
                $sql = "select * from rtnaprv ";        
                break;
        }

		$cond = "where apply_id = $apply_id and aprv_no = $apv_order";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_all($sel);
	}
	
	// 最終承認階層番号取得
	function get_last_apv_order($apply_id, $div)
	{
        switch($div)
        {
            case "OVTM":
                $sql = "select max(aprv_no) as max from ovtmaprv ";
                break;
            case "TMMD":
                $sql = "select max(aprv_no) as max from tmmdaprv ";
                break;
            case "RTN":
                $sql = "select max(aprv_no) as max from rtnaprv ";
                break;
        }

		$cond ="where apply_id = $apply_id";
		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "max");
	}
	
	// 申請確定階層取得
	function get_cfm_order($apply_id, $div)
	{
        switch($div)
        {
            case "OVTM":
                $sql = "select ovtmcfm as cfm from ovtmapply ";
                break;
            case "TMMD":
                $sql = "select timecfm as cfm from tmmdapply ";
                break;
            case "RTN":
                $sql = "select rtncfm as cfm from rtnapply ";
                break;
        }

		$cond ="where apply_id = $apply_id";
		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "cfm");
	}
	
	// 最終承認階層番号取得
	function get_apply_status($apply_id, $div)
	{
        switch($div)
        {
            case "OVTM":
                $sql = "select apply_status from ovtmapply ";
                break;
            case "TMMD":
                $sql = "select apply_status from tmmdapply ";
                break;
            case "RTN":
                $sql = "select apply_status from rtnapply ";
                break;
        }

		$cond ="where apply_id = $apply_id";
		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "apply_status");
	}
	
	// 非同期・同期受信情報取得
	function get_applyasyncrecv($apply_id, $recv_apv_order, $recv_apv_sub_order, $send_apved_order, $div)
	{
        switch($div)
        {
            case "OVTM":
                $sql = "select * from ovtm_async_recv ";
                break;
            case "TMMD":
                $sql = "select * from tmmd_async_recv ";
                break;
            case "RTN":
                $sql = "select * from rtn_async_recv ";
                break;
        }

		$cond .= "where apply_id = $apply_id and ";
		$cond .= "recv_aprv_no = $recv_apv_order and ";
        if($recv_apv_sub_order != "")
		{
			$cond .= "recv_aprv_sub_no = $recv_apv_sub_order and ";
		}
		else
		{
			$cond .= "recv_aprv_sub_no is null and ";
		}
		$cond .= "send_aprved_no <= $send_apved_order ";
		$cond .= "order by send_aprved_no asc ";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("apply_id" => $row["apply_id"],
                            "send_apv_order" =>  $row["send_aprv_no"],
                            "send_apv_sub_order" =>  $row["send_aprv_sub_no"],
                            "recv_apv_order" =>  $row["recv_aprv_no"],
                            "recv_apv_sub_order" =>  $row["recv_aprv_sub_no"],
                            "send_apved_order" =>  $row["send_aprved_no"]
			               );	
		}
		return $arr;
	}
	
	// 承認情報更新
	function update_apvstat($apv_stat, $apv_date, $apv_comment, $apply_id, $apv_order, $apv_sub_order, $div)
	{
        switch($div)
        {
            case "OVTM":
                $sql = "update ovtmaprv set";
                break;
            case "TMMD":
                $sql = "update tmmdaprv set";
                break;
            case "RTN":
                $sql = "update rtnaprv set";
                break;
        }

		$set = array("aprv_status", "aprv_date", "aprv_comment");
		$setvalue = array($apv_stat, $apv_date, pg_escape_string($apv_comment));

		if($apv_sub_order != "")
		{		
			$cond = "where apply_id = $apply_id and aprv_no = $apv_order and aprv_sub_no = $apv_sub_order";
		}
		else
		{
			$cond = "where apply_id = $apply_id and aprv_no = $apv_order and aprv_sub_no is null";
		}

		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
	
	// 同一階層の承認者数取得
	function get_same_hierarchy_apvcnt($apply_id, $apv_order, $div)
	{
        switch($div)
        {
            case "OVTM":
                $sql = "select count(*) as cnt from ovtmaprv ";
                break;
            case "TMMD":
                $sql = "select count(*) as cnt from tmmdaprv ";
                break;
            case "RTN":
                $sql = "select count(*) as cnt from rtnaprv ";
                break;
        }

		$cond ="where apply_id = $apply_id and aprv_no = $apv_order";
		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_result($sel, 0, "cnt");
	}
	
	// 承認更新番号取得
	function get_max_send_apved_order($apply_id, $send_apv_order, $div)
	{
        switch($div)
        {
            case "OVTM":
                $sql = "select max(send_aprved_no) as max from ovtm_async_recv ";
                break;
            case "TMMD":
                $sql = "select max(send_aprved_no) as max from tmmd_async_recv ";
                break;
            case "RTN":
                $sql = "select max(send_aprved_no) as max from rtn_async_recv ";
                break;
        }

		$cond = "where apply_id = $apply_id and send_aprv_no = $send_apv_order"; 

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "max");
	}
	
	// 非同期・同期受信更新
	function update_apv_show_flg($apply_id, $send_apv_order, $send_apv_sub_order, $send_apved_order, $div)
	{
        switch($div)
        {
            case "OVTM":
                $sql = "update ovtm_async_recv set";
                break;
            case "TMMD":
                $sql = "update tmmd_async_recv set";
                break;
            case "RTN":
                $sql = "update rtn_async_recv set";
                break;
        }

		$set = array("apv_show_flg", "send_aprved_no");
		$setvalue = array("t", $send_apved_order);

		if($send_apv_sub_order != "")
		{
			$cond = "where apply_id = $apply_id and send_aprv_no = $send_apv_order and send_aprv_sub_no = $send_apv_sub_order";
		}
		else
		{
			$cond = "where apply_id = $apply_id and send_aprv_no = $send_apv_order and send_aprv_sub_no is null";
		}
		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}
	
	// 同一階層で指定した承認ステータス数取得
	function get_same_hierarchy_apvstatcnt($apply_id, $apv_order, $apv_stat, $div)
	{
        switch($div)
        {
            case "OVTM":
                $sql = "select count(*) as cnt from ovtmaprv ";
                break;
            case "TMMD":
                $sql = "select count(*) as cnt from tmmdaprv ";
                break;
            case "RTN":
                $sql = "select count(*) as cnt from rtnaprv ";
                break;
        }

		$cond = "where apply_id = $apply_id and aprv_no = $apv_order and aprv_status = '$apv_stat'";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "cnt");
	}	
	
	// 申請ステータス更新
	function update_applystat($apply_id, $apply_stat, $session, $div)
	{
        switch($div)
        {
            case "OVTM":
                $sql = "update ovtmapply set";
                break;
            case "TMMD":
                $sql = "update tmmdapply set";
                break;
            case "RTN":
                $sql = "update rtnapply set";
                break;
        }

		$set = array("apply_status");
		$setvalue = array($apply_stat);
		$cond = "where apply_id = $apply_id";	

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}
	
	// 承認テーブル更新処理（権限並列用）
	function update_applyapv_for_parallel($apply_id, $apv_order, $apv_date, $apv_stat, $div)
	{
        switch($div)
        {
            case "OVTM":
                $sql = "update ovtmaprv set";
                break;
            case "TMMD":
                $sql = "update tmmdaprv set";
                break;
            case "RTN":
                $sql = "update rtnaprv set";
                break;
        }

		$set = array("aprv_status", "other_apv_flg", "aprv_date");
		$setvalue = array($apv_stat, "t", $apv_date);
		$cond = "where apply_id = $apply_id and aprv_no = $apv_order and aprv_status = '0'";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}
	
	// 承認処理
	function  approve_application($apply_id, $apv_order, $apv_sub_order, $approve, $apv_comment, $next_notice_div, $session, $div)
	{
		// 承認ステータス更新
		$apv_date = date("YmdHi");
		$this->update_apvstat($approve, $apv_date, $apv_comment, $apply_id, $apv_order, $apv_sub_order, $div);
		
		// 稟議タイプのみ
		$same_hierarchy_apvcnt = $this->get_same_hierarchy_apvcnt($apply_id, $apv_order, $div);
	
		//残業時間の確定階層取得
		$cfm_order = $this->get_cfm_order($apply_id, $div);
		if($cfm_order == "")
		{
			$cfm_order = 1;
		}
		//スキップ対応 20151118
		if ($cfm_order > 1 && $this->check_skip_flg) {
			$arr_app_info = $this->get_apply_wkfwmst($apply_id, $div);
	        switch($div)
	        {
	            case "OVTM":
	                $wkfw_div= "1";
	                break;
	            case "TMMD":
	                $wkfw_div= "2";
	                break;
	            case "RTN":
	                $wkfw_div= "3";
	                break;
	        }

			$arr_wk_info = $this->get_wkfwapv_info($arr_app_info[0]["emp_id"], $wkfw_div, true);
			$skip_count = $arr_wk_info[0]["skip_count"];
			$cfm_order -= $skip_count;
			if ($cfm_order < 1) {
				$cfm_order = 1;
			}
		}
		
        //申請ステータス
        $apply_status = $this->get_apply_status($apply_id, $div);
        
		// 該当承認者の同一階層に他の承認者がいる場合(複数承認者)
		if($same_hierarchy_apvcnt > 1)
		{
			// 最終承認階層取得
			$last_apv_order = $this->get_last_apv_order($apply_id, $div);
		
			// 承認者の階層より後につづく階層がある場合
			if($apv_order < $last_apv_order)
			{
				// 承認者の階層が非同期指定の場合
				if($next_notice_div == "1")
				{
					switch($approve)
					{
						case "1":   // 承認
							// 非同期・同期受信テーブル更新
							$apved_order = $this->get_max_send_apved_order($apply_id, $apv_order, $div);
							$this->update_apv_show_flg($apply_id, $apv_order, $apv_sub_order, $apved_order+1, $div);
                            if($cfm_order == $apv_order && $apply_status == "0")
                            {
								// 申請ステータス更新
								$this->update_applystat($apply_id, "1", $session, $div);
                            }
							break;

						case "2":   // 否認
							$same_hierarchy_apvcnt = $this->get_same_hierarchy_apvcnt($apply_id, $apv_order, $div);
							$same_hierarchy_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "2", $div);

							// 全員が否認の場合
							if($same_hierarchy_apvcnt == $same_hierarchy_apvstatcnt)
							{
								if($apply_status == "0")
                            	{
									// 申請ステータス更新
									$this->update_applystat($apply_id, "2", $session, $div);
                            	}
							}
							// 同一階層で先に「承認」した人がいた場合
							$same_hierarchy_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "1", $div);
							if($same_hierarchy_apvstatcnt > 0)
							{
								// 非同期・同期受信テーブル更新
								$apved_order = $this->get_max_send_apved_order($apply_id, $apv_order, $div);
								$this->update_apv_show_flg($apply_id, $apv_order, $apv_sub_order, $apved_order+1, $div);
							}
							break;
						case "3":   // 差戻し

							// 同一階層で先に「承認」した人がいた場合
							$same_hierarchy_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "1", $div);
							if($same_hierarchy_apvstatcnt > 0)
							{
								// 非同期・同期受信テーブル更新
								$apved_order = $this->get_max_send_apved_order($apply_id, $apv_order, $div);
								$this->update_apv_show_flg($apply_id, $apv_order, $apv_sub_order, $apved_order+1, $div);
							}

							break;
						default:
							break;
					}

					// 未承認者がいない ＡＮＤ 承認が一人もいない ＡＮＤ 差戻しが一人でもいる
					$non_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "0", $div);
					$ok_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "1", $div);
					$bak_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "3", $div);
					if($non_apvstatcnt == 0 && $ok_apvstatcnt == 0 && $bak_apvstatcnt > 0)
					{
						if($apply_status == "0")
                        {
							// 申請ステータス更新
							$this->update_applystat($apply_id, "3", $session, $div);
                        }
					}
				}
				// 承認者の階層が同期指定の場合
				else if($next_notice_div == "2")
				{
					$same_hierarchy_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "0", $div);
					// 同一階層で未承認者がいない場合
					if($same_hierarchy_apvstatcnt == 0)
					{
						$same_hierarchy_apvcnt = $this->get_same_hierarchy_apvcnt($apply_id, $apv_order, $div);
						$ok_approvecnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "1", $div);
						$no_approvecnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "2", $div);
						$bak_approvecnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "3", $div);

						// 全員が承認の場合
						if($same_hierarchy_apvcnt == $ok_approvecnt)
						{
							// 非同期・同期受信テーブル更新
							$this->update_apv_show_flg($apply_id, $apv_order, "", 1, $div);
                            if($cfm_order == $apv_order && $apply_status == "0")
                            {
								// 申請ステータス更新
								$this->update_applystat($apply_id, "1", $session, $div);
                            }

						}

						// 否認があって差戻しがない場合、否認にする。
						if($no_approvecnt > 0 && $bak_approvecnt == 0)
						{
							if($apply_status == "0")
                            {
								// 申請ステータス更新
								$this->update_applystat($apply_id, "2", $session, $div);
                            }
						}

						// 差戻しが１つでもある場合
						if($bak_approvecnt > 0)
						{
							if($apply_status == "0")
                            {
								// 申請ステータス更新
								$this->update_applystat($apply_id, "3", $session, $div);
                            }
						}			
					}
				}
				// 権限並列指定の場合
				else if($next_notice_div == "3")
				{
					$non_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "0", $div);

					// 未承認者がいた場合
					if($non_apvstatcnt > 0)
					{
						switch($approve)
						{
							case "1":   // 承認

								// 非同期・同期受信テーブル更新
								$this->update_apv_show_flg($apply_id, $apv_order, "", 1, $div);

								// 他の承認者の承認ステータスが「未承認」の場合、「承認」に更新
								$this->update_applyapv_for_parallel($apply_id, $apv_order, $apv_date, "1", $div);

                                if($cfm_order == $apv_order && $apply_status == "0")
                                {
								    // 申請ステータス更新
								    $this->update_applystat($apply_id, "1", $session, $div);
                                }

								break;
							case "2":   // 否認

								// 他の承認者の承認ステータスが「未承認」の場合、「否認」に更新
								$this->update_applyapv_for_parallel($apply_id, $apv_order, $apv_date, "2", $div);
								if($apply_status == "0")
                            	{
									// 申請ステータス更新
									$this->update_applystat($apply_id, "2", $session, $div);
                            	}
								break;
							case "3":   // 差戻し

								// 他の承認者の承認ステータスが「未承認」の場合、「差戻し」に更新
								$this->update_applyapv_for_parallel($apply_id, $apv_order, $apv_date, "3", $div);
								if($apply_status == "0")
                            	{
									// 申請ステータス更新
									$this->update_applystat($apply_id, "3", $session, $div);
                            	}
								break;
							default:
								break;
						}
					}
				}		
			}
			// 承認者の階層より後につづく階層がない場合（最終階層）
			else if($apv_order == $last_apv_order)
			{
				// 承認者の階層が非同期指定の場合
				if($next_notice_div == "1")
				{
					$same_hierarchy_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "0", $div);
					// 同一階層で未承認者がいない場合
					if($same_hierarchy_apvstatcnt == 0)
					{
						// 承認が１つでもある場合
						$approvecnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "1", $div);
						if($approvecnt > 0)
						{
                            if($cfm_order == $apv_order && $apply_status == "0")
                            {
							    // 申請ステータス更新
							    $this->update_applystat($apply_id, "1", $session, $div);
                            }
						}
			
						// 全員が否認の場合
						$same_hierarchy_apvcnt = $this->get_same_hierarchy_apvcnt($apply_id, $apv_order, $div);
						$no_approvecnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "2", $div);
						if($same_hierarchy_apvcnt == $no_approvecnt)
						{
							// 申請ステータス更新
							$this->update_applystat($apply_id, "2", $session, $div);
						}
			
						// 承認がなく差戻しがある場合
						if($approvecnt == 0)
						{
							$bak_cnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "3", $div);
							if($bak_cnt > 0)
							{
								// 申請ステータス更新
								$this->update_applystat($apply_id, "3", $session, $div);
							}
						}
					}
				}
				// 承認者の階層が同期指定の場合
				else if($next_notice_div == "2")
				{
					$same_hierarchy_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "0", $div);
					// 同一階層で未承認者がいない場合
					if($same_hierarchy_apvstatcnt == 0)
					{
						$same_hierarchy_apvcnt = $this->get_same_hierarchy_apvcnt($apply_id, $apv_order, $div);
						$ok_approvecnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "1", $div);
						$no_approvecnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "2", $div);
						$bak_approvecnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "3", $div);

						// 全員が承認の場合
						if($same_hierarchy_apvcnt == $ok_approvecnt)
						{
                            if($cfm_order == $apv_order && $apply_status == "0")
                            {
							    // 申請ステータス更新
							    $this->update_applystat($apply_id, "1", $session, $div);
                            }
						}

						// 否認があって差戻しがない場合、否認にする。
						if($no_approvecnt > 0 && $bak_approvecnt == 0)
						{
							// 申請ステータス更新
							$this->update_applystat($apply_id, "2", $session, $div);
						}

						// 差戻しが１つでもある場合
						if($bak_approvecnt > 0)
						{
							// 申請ステータス更新
							$this->update_applystat($apply_id, "3", $session, $div);

						}			
					}
				}
				// 権限並列指定の場合
				else if($next_notice_div == "3")
				{
					$non_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "0", $div);

					// 未承認者がいた場合
					if($non_apvstatcnt > 0)
					{
						switch($approve)
						{
							case "1":   // 承認

								// 他の承認者の承認ステータスが「未承認」の場合、「承認」に更新
								$this->update_applyapv_for_parallel($apply_id, $apv_order, $apv_date, "1", $div);
								// 申請ステータス更新
								if($cfm_order == $apv_order && $apply_status == "0")
                                {
								    $this->update_applystat($apply_id, "1", $session, $div);
                                }    
								break; //承認不具合対応 20140306
                            case "2":   // 否認

								// 他の承認者の承認ステータスが「未承認」の場合、「否認」に更新
								$this->update_applyapv_for_parallel($apply_id, $apv_order, $apv_date, "2", $div);
								// 申請ステータス更新
								$this->update_applystat($apply_id, "2", $session, $div);
								break;
							case "3":   // 差戻し

								// 他の承認者の承認ステータスが「未承認」の場合、「差戻し」に更新
								$this->update_applyapv_for_parallel($apply_id, $apv_order, $apv_date, "3", $div);
								// 申請ステータス更新
								$this->update_applystat($apply_id, "3", $session, $div);
								break;
							default:
								break;
						}
					}
				}
			}
		}
		// 該当承認者の同一階層に他の承認者がいない場合(承認者一人)
		else if($same_hierarchy_apvcnt == 1)
		{
			switch($approve)
			{
				case "1":   // 承認
					// 最終承認階層取得
					$last_apv_order = $this->get_last_apv_order($apply_id, $div);
					if($last_apv_order == ""){
						$last_apv_order = 1;
					}
					if($apv_order == $last_apv_order)
					{
						// 申請ステータス更新
						$this->update_applystat($apply_id, "1", $session, $div);
					}
                    else
                    {
	                    if($cfm_order == $apv_order && $apply_status == "0")
	                    {
						    // 申請ステータス更新
						    $this->update_applystat($apply_id, "1", $session, $div);
	                    }
                    }

					break;
				case "2":   // 否認
					if($apply_status == "0")
                    {
						// 申請ステータス更新
						$this->update_applystat($apply_id, "2", $session, $div);
                    }
					break;
				case "3":   // 差戻し
					if($apply_status == "0")
                    {
						// 申請ステータス更新
						$this->update_applystat($apply_id, "3", $session, $div);
                    }
					break;
				default:
					break;

			}
		}
	}

//-------------------------------------------------------------------------------------------------------------------------
// その他
//-------------------------------------------------------------------------------------------------------------------------

	// 職員情報取得
	function get_empmst($session)
	{
		$sql  = "select * from empmst";
		$cond = "where emp_id in (select emp_id from session where session_id='$session')";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_all($sel);
	}

    // 残業申請、勤務時間修正申請、退勤後復帰申請の承認情報取得
    function get_approve_list($apply_id, $div)
    {
        switch($div)
        {
            case "OVTM":
                $sql = "select aprv_status from ovtmaprv ";
                break;
            case "TMMD":
                $sql = "select aprv_status from tmmdaprv ";
                break;
            case "RTN":
                $sql = "select aprv_status from rtnaprv ";
                break;
        }

	    $cond = "where apply_id = $apply_id ";
        $cond .= "order by aprv_no, aprv_sub_no asc ";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo ("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo ("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $arr = array ();
        while ($row = pg_fetch_array($sel)) {
            $arr[] = array ("apv_stat" => $row["aprv_status"]);
        }
        return $arr;
    }

    // 承認があるかチェック
    function get_apv_flg($arr)
    {
        for ($i = 0; $i < count($arr); $i ++) {
            if ($arr[$i]["apv_stat"] == "1") {
                return true;
            }
        }
        return false;
    }

    // 申請確定階層取得
    function get_apply_confirm_hierarchy($inputs)
    {
        $wkfw_approve_num = $inputs["wkfw_approve_num"];
        $upper_apv_order = "";
        $lower_apv_order = "";

        for($i=1; $i<=$inputs["approve_num"]; $i++)
        {
			$aprv_emp_id = $inputs["regist_emp_id$i"];
			$apv_order   = $inputs["apv_order$i"];		

            // 申請確定階層と同じ階層に承認者が存在する場合
            if($wkfw_approve_num == $apv_order)
            {
                if($aprv_emp_id != "")
                {
                    $confirm_hierarchy = $apv_order;
                    break;
                }
            }
            // 申請確定階層より上位階層に承認者が存在する場合
            else if($wkfw_approve_num > $apv_order)
            {
                if($aprv_emp_id != "")
                {
                    $upper_apv_order = $apv_order;
                }
            }
            // 申請確定階層より下位階層に承認者が存在する場合
            else if($wkfw_approve_num < $apv_order)
            {
                if($aprv_emp_id != "" && $lower_apv_order == "")
                {
                    $lower_apv_order = $apv_order;
                }
            }
        }
        if($confirm_hierarchy == "")
        {
            if($upper_apv_order != "")
            {
                $confirm_hierarchy = $upper_apv_order;
            }
            else if($upper_apv_order == "")
            {
                $confirm_hierarchy = $lower_apv_order;
            }
        }

        return $confirm_hierarchy;
    }

    // 該当者なしの階層を考慮した承認者情報を取得
    function get_applyapv_for_non_pertinence($arr_applyapv)
    {
   	    $arr = array();
        $approve_num = count($arr_applyapv);
	    $apply_id = $arr_applyapv[0]["apply_id"];
	    $max_aprv_no  = $arr_applyapv[$approve_num - 1]["aprv_no"];
	    for($i=1; $i<=$max_aprv_no; $i++)
	    {
	         $exist_flg = false;
	         for($j=0; $j<$approve_num; $j++)
	         {
	             if($i == $arr_applyapv[$j]["aprv_no"])
	             {
	                 $arr[] = $arr_applyapv[$j];
	                 $exist_flg = true;
	             }
	         }    
	         if(!$exist_flg)
	         {
	             $arr[] = array("apply_id" => $apply_id, "aprv_no" => $i);
	         }
	    }
        return $arr;
    }
	
	/**
	 * 残業申請IDを採番
	 *
	 * @return 残業の申請ID
	 *
	 */
	function get_ovtm_apply_id() {
		
		$sql = "select max(apply_id) from ovtmapply";
		$cond = "";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo ("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo ("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
		$apply_id = intval(pg_fetch_result($sel, 0, 0)) + 1;

		return $apply_id;
	}
	
	/**
	 * 残業の申請IDが存在するか確認
	 *
	 * @param string 職員ID
	 * @param string 日付
	 * @return 存在する場合には申請ID、存在しない場合は""
	 *
	 */
	function check_ovtm_apply_id($emp_id, $date) {
		
		$sql = "select apply_id from ovtmapply";
		$cond = "where emp_id = '$emp_id' and target_date = '$date' and delete_flg = 'f' order by apply_id desc limit 1";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo ("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo ("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$apply_id = "";
		if (pg_num_rows($sel) > 0)
		{
			$apply_id = pg_fetch_result($sel, 0, 0);
		}
		
		return $apply_id;
	}

	/**
	 * 指定された残業の申請IDデータを論理削除する
	 *
	 * @param mixed 残業の申請ID
	 * @return なし
	 *
	 */
	function delete_ovtm_apply_id($ovtm_apply_id) {
		
		// 残業申請承認論理削除
		$sql = "update ovtmaprv set";
		$set = array("delete_flg");
		$setvalue = array('t');
		$cond = "where apply_id = $ovtm_apply_id";
		
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		// 残業申請論理削除
		$sql = "update ovtmapply set";
		$set = array("delete_flg");
		$setvalue = array('t');
		$cond = "where apply_id = $ovtm_apply_id";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		// 承認候補を論理削除
		$sql = "update ovtmaprvemp set";
		$set = array("delete_flg");
		$setvalue = array('t');
		$cond = "where apply_id = $ovtm_apply_id";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		// 同期・非同期情報を論理削除
		$sql = "update ovtm_async_recv set";
		$set = array("delete_flg");
		$setvalue = array('t');
		$cond = "where apply_id = $ovtm_apply_id";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	/**
	 * 指定された残業のovtmapply以外の承認情報を論理削除する。申請不要更新用。
	 *
	 * @param mixed 残業の申請ID
	 * @return なし
	 *
	 */
	function delete_ovtm_aprv($ovtm_apply_id) {
		
		// 残業申請承認論理削除
		$sql = "update ovtmaprv set";
		$set = array("delete_flg");
		$setvalue = array('t');
		$cond = "where apply_id = $ovtm_apply_id";
		
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		// 承認候補を論理削除
		$sql = "update ovtmaprvemp set";
		$set = array("delete_flg");
		$setvalue = array('t');
		$cond = "where apply_id = $ovtm_apply_id";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		// 同期・非同期情報を論理削除
		$sql = "update ovtm_async_recv set";
		$set = array("delete_flg");
		$setvalue = array('t');
		$cond = "where apply_id = $ovtm_apply_id";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
	
	/**
	 * 指定された修正の申請IDデータを論理削除する
	 *
	 * @param mixed 修正の申請ID
	 * @return なし
	 *
	 */
	function delete_tmmd_apply_id($tmmd_apply_id) {
		
		// 修正申請承認論理削除
		$sql = "update tmmdaprv set";
		$set = array("delete_flg");
		$setvalue = array('t');
		$cond = "where apply_id = $tmmd_apply_id";
		
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		// 修正申請論理削除
		$sql = "update tmmdapply set";
		$set = array("delete_flg");
		$setvalue = array('t');
		$cond = "where apply_id = $tmmd_apply_id";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		// 承認候補を論理削除
		$sql = "update tmmdaprvemp set";
		$set = array("delete_flg");
		$setvalue = array('t');
		$cond = "where apply_id = $tmmd_apply_id";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		// 同期・非同期情報を論理削除
		$sql = "update tmmd_async_recv set";
		$set = array("delete_flg");
		$setvalue = array('t');
		$cond = "where apply_id = $tmmd_apply_id";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
	
	/**
	 * 残業申請状態一括更新
	 *
	 * @param mixed $ovtm_apply_id 申請ID
	 * @param mixed $apply_status 申請状態 1:承認 2:否認 3:差戻し
	 * @return なし
	 *
	 */
	function update_ovtm_status_all($ovtm_apply_id, $apply_status) {

		// 残業申請の状態更新
		$sql = "update ovtmapply set";
		//否認
		if ($apply_status == "2") {
			//理由IDもクリアする
			$set = array("apply_status", "reason_id");
			$setvalue = array($apply_status, null);
		} else {
			$set = array("apply_status");
			$setvalue = array($apply_status);
		}
			
		$cond = "where apply_id = $ovtm_apply_id";
		
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		// 残業申請承認更新
		$sql = "update ovtmaprv set";
		$set = array("aprv_status", "aprv_date");
		$setvalue = array($apply_status, date("YmdHi"));
		$cond = "where apply_id = $ovtm_apply_id";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
	}
	
	/**
	 * 修正申請状態一括更新
	 *
	 * @param mixed $tmmd_apply_id 申請ID
	 * @param mixed $apply_status 申請状態 1:承認 2:否認 3:差戻し
	 * @return なし
	 *
	 */
	function update_tmmd_status_all($tmmd_apply_id, $apply_status) {
		
		// 修正申請の状態更新
		$sql = "update tmmdapply set";
		//否認
		if ($apply_status == "2") {
			//理由IDもクリアする
			$set = array("apply_status", "reason_id");
			$setvalue = array($apply_status, null);
		} else {
			$set = array("apply_status");
			$setvalue = array($apply_status);
		}
		
		$cond = "where apply_id = $tmmd_apply_id";
		
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		// 修正申請承認更新
		$sql = "update tmmdaprv set";
		$set = array("aprv_status", "aprv_date");
		$setvalue = array($apply_status, date("YmdHi"));
		$cond = "where apply_id = $tmmd_apply_id";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
	}
	
	/**
	 * 呼出・退復申請状態一括更新
	 *
	 * @param mixed $rtn_apply_id 申請ID
	 * @param mixed $apply_status 申請状態 1:承認 2:否認 3:差戻し
	 * @return なし
	 *
	 */
	function update_rtn_status_all($rtn_apply_id, $apply_status) {
		
		// 呼出・退復申請の状態更新
		$sql = "update rtnapply set";
		//否認
		if ($apply_status == "2") {
			//理由IDもクリアする
			$set = array("apply_status", "reason_id");
			$setvalue = array($apply_status, null);
		} else {
			$set = array("apply_status");
			$setvalue = array($apply_status);
		}
		
		$cond = "where apply_id = $rtn_apply_id";
		
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		// 呼出・退復申請承認更新
		$sql = "update rtnaprv set";
		$set = array("aprv_status", "aprv_date");
		$setvalue = array($apply_status, date("YmdHi"));
		$cond = "where apply_id = $rtn_apply_id";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
	}

	/**
	 * 退勤後復帰申請IDを採番
	 *
	 * @return 退勤後復帰の申請ID
	 *
	 */
	function get_rtn_apply_id() {
		
		$sql = "select max(apply_id) from rtnapply";
		$cond = "";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo ("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo ("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$apply_id = intval(pg_fetch_result($sel, 0, 0)) + 1;
		
		return $apply_id;
	}
	
	/**
	 * 退勤後復帰の申請IDが存在するか確認
	 *
	 * @param string 職員ID
	 * @param string 日付
	 * @return 存在する場合には申請ID、存在しない場合は""
	 *
	 */
	function check_rtn_apply_id($emp_id, $date) {
		
		$sql = "select apply_id from rtnapply";
		$cond = "where emp_id = '$emp_id' and target_date = '$date' order by apply_id desc limit 1";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo ("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo ("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$apply_id = "";
		if (pg_num_rows($sel) > 0)
		{
			$apply_id = pg_fetch_result($sel, 0, 0);
		}
		
		return $apply_id;
	}
	
	/**
	 * 指定された退勤後復帰の申請IDデータを論理削除する
	 *
	 * @param mixed 退勤後復帰の申請ID
	 * @return なし
	 *
	 */
	function delete_rtn_apply_id($rtn_apply_id) {
		
		// 退勤後復帰申請承認論理削除
		$sql = "update rtnaprv set";
		$set = array("delete_flg");
		$setvalue = array('t');
		$cond = "where apply_id = $rtn_apply_id";
		
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		// 退勤後復帰申請論理削除
		$sql = "update rtnapply set";
		$set = array("delete_flg");
		$setvalue = array('t');
		$cond = "where apply_id = $rtn_apply_id";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		// 承認候補を論理削除
		$sql = "update rtnaprvemp set";
		$set = array("delete_flg");
		$setvalue = array('t');
		$cond = "where apply_id = $rtn_apply_id";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		// 同期・非同期情報を論理削除
		$sql = "update rtn_async_recv set";
		$set = array("delete_flg");
		$setvalue = array('t');
		$cond = "where apply_id = $rtn_apply_id";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}	

	//残業申請追加 $status指定
    function insert_ovtm_apply($emp_id, $tmp_date, $tmp_reason_id, $reason, $tmp_end_time, $tmp_next_day_flag, $tmp_start_time, $tmp_previous_day_flag, $tmp_over_start_time, $tmp_over_end_time, $tmp_o_s_next_day_flag, $tmp_o_e_next_day_flag, $status, $tmp_over_start_time2, $tmp_over_end_time2, $tmp_o_s_next_day_flag2, $tmp_o_e_next_day_flag2, $reason_detail, $arr_values, $maxline) {
		
		// 承認者ワークフロー情報取得
		$sql = "select * from atdbk_wkfw";
		$cond = "";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		$ovtm_wkfw_div = pg_fetch_result($sel, 0, "ovtm_wkfw_div");
		$wkfw_approve_num = pg_fetch_result($sel, 0, "ovtmcfm");
		
		$wkfw_flg = ($ovtm_wkfw_div != "1") ? false : true;
		// 承認者管理情報取得
		$arr_wkfwapv = $this->get_wkfwapv_info($emp_id, "1", $wkfw_flg);
		$approve_num = count($arr_wkfwapv);   // 承認者数
		
		
		for ($j=0; $j<$approve_num; $j++) {
			$i = $j + 1;
			$wkfwapv         = $arr_wkfwapv[$j];
			$emp_infos       = $wkfwapv["emp_infos"];
			
			$multi_apv_flg   = $wkfwapv["multi_apv_flg"];	//複数承認者
			$apv_order       = $wkfwapv["apv_order"];		//承認階層
			$apv_sub_order   = $wkfwapv["apv_sub_order"];	//承認順
			$next_notice_div = $wkfwapv["next_notice_div"]; //1:非同期、2同期
			
			$varname = "regist_emp_id$i";
			if (count($emp_infos)> 0) {
				$$varname = $emp_infos[0]["emp_id"];
			} else {
				$$varname = "";
			}
			$varname = "apv_order$i";
			$$varname = $apv_order;
			$varname = "apv_sub_order$i";
			$$varname = $apv_sub_order;
			$varname = "multi_apv_flg$i";
			$$varname = $multi_apv_flg;
			$varname = "next_notice_div$i";
			$$varname = $next_notice_div;
			
			$varname = "pst_approve_num$apv_order";
			$$varname = count($emp_infos);
			
			for($k=0; $k<count($emp_infos); $k++) {
				$varname = "pst_emp_id$apv_order";
				$varname .= "_".($k+1);
				$$varname = $emp_infos[$k]["emp_id"];
			}
			
		}
		// 申請確定情報取得
		if($wkfw_approve_num != "")
		{
			// 申請確定階層取得用配列
			$inputs = array();
			$inputs["wkfw_approve_num"] = $wkfw_approve_num;
			$inputs["approve_num"] = $approve_num;
			for ($i=1; $i<=$approve_num; $i++) {
				$varname = "regist_emp_id$i";
				$inputs["regist_emp_id$i"] = $$varname;
				$varname = "apv_order$i";
				$inputs["apv_order$i"] = $$varname;
			}
			$tmp_cfm = $this->get_apply_confirm_hierarchy($inputs);
		}
		else
		{
			$tmp_cfm = null;
		}
		
		//残業申請不要データを削除する
		$this->delete_no_apply_ovtm_data($emp_id, $tmp_date);
		
		//申請ID
		$apply_id = $this->get_ovtm_apply_id();
		//$reason = "";
		if ($tmp_cfm == "") {
			$tmp_cfm = null;
		}
		$sql = "insert into ovtmapply (apply_id, emp_id, target_date, reason_id, reason, apply_time, apply_status, ovtmcfm, delete_flg, end_time, next_day_flag, start_time, previous_day_flag, over_start_time, over_end_time, over_start_next_day_flag, over_end_next_day_flag, over_start_time2, over_end_time2, over_start_next_day_flag2, over_end_next_day_flag2, reason_detail ";

        //追加 20140623
        //$maxline = 5;
        for ($i=3; $i<=$maxline; $i++) {
            $sql .= ", over_start_time".$i;
            $sql .= ", over_end_time".$i;
            $sql .= ", over_start_next_day_flag".$i;
            $sql .= ", over_end_next_day_flag".$i;
        }
        for ($i=1; $i<=$maxline; $i++) {
            $sql .= ", rest_start_time".$i;
            $sql .= ", rest_end_time".$i;
            $sql .= ", rest_start_next_day_flag".$i;
            $sql .= ", rest_end_next_day_flag".$i;
        }
		//追加 20150107
        for ($i=2; $i<=$maxline; $i++) {
            $sql .= ", reason_id".$i;
            $sql .= ", reason".$i;
        }
        $sql .= ") values (";
		$content = array($apply_id, $emp_id, $tmp_date, $tmp_reason_id, pg_escape_string($reason), date("YmdHis"), $status, $tmp_cfm, 'f', $tmp_end_time, $tmp_next_day_flag, $tmp_start_time, $tmp_previous_day_flag, $tmp_over_start_time, $tmp_over_end_time, $tmp_o_s_next_day_flag, $tmp_o_e_next_day_flag, $tmp_over_start_time2, $tmp_over_end_time2, $tmp_o_s_next_day_flag2, $tmp_o_e_next_day_flag2, pg_escape_string($reason_detail));
        //追加
        for ($i=3; $i<=$maxline; $i++) {
            $s_t = "over_start_time".$i;
            $e_t = "over_end_time".$i;
            $s_f = "over_start_next_day_flag".$i;
            $e_f = "over_end_next_day_flag".$i;
            array_push($content, $arr_values[$s_t]);
            array_push($content, $arr_values[$e_t]);
            array_push($content, $arr_values[$s_f]);
            array_push($content, $arr_values[$e_f]);
        }    
        for ($i=1; $i<=$maxline; $i++) {
            $s_t = "rest_start_time".$i;
            $e_t = "rest_end_time".$i;
            $s_f = "rest_start_next_day_flag".$i;
            $e_f = "rest_end_next_day_flag".$i;
            array_push($content, $arr_values[$s_t]);
            array_push($content, $arr_values[$e_t]);
            array_push($content, $arr_values[$s_f]);
            array_push($content, $arr_values[$e_f]);
        }    
		//追加 20150107
        for ($i=2; $i<=$maxline; $i++) {
            $s_t = "over_start_time".$i;
            $reason_id_num = "ovtm_reason_id".$i;
            $reason_num = "ovtm_reason".$i;
			if ($arr_values[$s_t] != "") {
            	$reason_id_val = ($arr_values[$reason_id_num] == "other") ? null : $arr_values[$reason_id_num];
            	$reason_val = $arr_values[$reason_num];
            }
            else {
            	$reason_id_val = null;
            	$reason_val = null;
            }
            array_push($content, $reason_id_val);
            array_push($content, pg_escape_string($reason_val));
        }
        $ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		// 承認登録
		for($k=1; $k<=$approve_num; $k++)
		{
			$varname = "regist_emp_id$k";
			$aprv_emp_id = ($$varname == "") ? null : $$varname;
			
			$varname = "apv_order$k";
			$apv_order = ($$varname == "") ? null : $$varname;
			
			$varname = "apv_sub_order$k";
			$apv_sub_order = ($$varname == "") ? null : $$varname;
			
			$varname = "multi_apv_flg$k";
			$multi_apv_flg = ($$varname == "") ? "f" : $$varname;
			
			$varname = "next_notice_div$k";
			$next_notice_div = ($$varname == "") ? null : $$varname;
			
			if($aprv_emp_id != "")
			{
				// 役職も登録する
				$sql = "select emp_st from empmst ";
				$cond = "where emp_id = '$aprv_emp_id'";
				$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
				if ($sel == 0) {
					pg_query($this->_db_con, "rollback");
					pg_close($this->_db_con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
				$emp_st = pg_fetch_result($sel, 0, 0);
				
				// 承認者情報を登録
				$sql = "insert into ovtmaprv (apply_id, aprv_no, aprv_emp_id, aprv_status, aprv_sub_no, multi_apv_flg, next_notice_div, emp_st";
				//承認済にする場合は、日時も更新する
				if ($status == "1") {
					$sql .= ", aprv_date";
				}
				$sql .= ") values (";
				$content = array($apply_id, $apv_order, $aprv_emp_id, $status, $apv_sub_order, $multi_apv_flg, $next_notice_div, $emp_st);
				if ($status == "1") {
					array_push($content, date("YmdHi"));
				}
				$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
				if ($ins == 0) {
					pg_query($this->_db_con, "rollback");
					pg_close($this->_db_con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
			}
		}
		
		// 承認者候補登録
		for($k=1; $k <= $approve_num; $k++)
		{
			$varname = "apv_order$k";
			$apv_order = $$varname;

			$wkfwapv         = $arr_wkfwapv[$apv_order-1];
			$emp_infos       = $wkfwapv["emp_infos"];
			
			$multi_apv_flg   = $wkfwapv["multi_apv_flg"];	//複数承認者
			// 複数承認者を「許可しない」かつ、複数いる場合 20100414
			//※無条件の場合に、apply_id, aprv_no, person_noのキーの重複でDBエラーとなる
			if ($multi_apv_flg != 't' && count($emp_infos) > 1) { 
				
				
				$varname = "pst_approve_num$apv_order";
				$pst_approve_num = $$varname;
				
				for($j=1; $j<=$pst_approve_num; $j++)
				{
					$varname = "pst_emp_id$apv_order";
					$varname .= "_$j";
					$pst_emp_id = $$varname;
					
					$sql = "insert into ovtmaprvemp (apply_id, aprv_no, person_no, emp_id, delete_flg) values (";
					$content = array($apply_id, $apv_order, $j, $pst_emp_id, 'f');
					$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
					if ($ins == 0) {
						pg_query($this->_db_con, "rollback");
						pg_close($this->_db_con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}
				}
			}
		}
		
		// 非同期・同期受信登録
		$previous_apv_order = "";
		$arr_apv = array();
		$arr_apv_sub_order = array();
		for($k=1; $k <= $approve_num; $k++)
		{
			$varname = "apv_order$k";
			$apv_order = $$varname;
			
			$varname = "apv_sub_order$k";
			$apv_sub_order = ($$varname == "") ? null : $$varname;
			
			$varname = "multi_apv_flg$k";
			$multi_apv_flg = ($$varname == "") ? "f" : $$varname;
			
			$varname = "next_notice_div$k";
			$next_notice_div = ($$varname == "") ? null : $$varname;
			
			if($previous_apv_order != $apv_order)
			{
				$arr_apv_sub_order = array();
			}
			$arr_apv_sub_order[] = $apv_sub_order;
			$arr_apv[$apv_order] = array("multi_apv_flg" => $multi_apv_flg, "next_notice_div" => $next_notice_div, "apv_sub_order" => $arr_apv_sub_order);
			
			$previous_apv_order = $apv_order;
		}
		
		$arr_send_apv_sub_order = array();
		foreach($arr_apv as $apv_order => $apv_info)
		{
			$multi_apv_flg = $apv_info["multi_apv_flg"];
			$next_notice_div = $apv_info["next_notice_div"];
			$arr_apv_sub_order = $apv_info["apv_sub_order"];
			
			foreach($arr_send_apv_sub_order as $send_apv_order => $arr_send_apv_sub)
			{
				// 非同期通知
				if($arr_send_apv_sub != null)
				{
					foreach($arr_send_apv_sub as $send_apv_sub_order)
					{
						foreach($arr_apv_sub_order as $recv_apv_sub_order)
						{
							$sql = "insert into ovtm_async_recv (apply_id, send_aprv_no, send_aprv_sub_no, recv_aprv_no, recv_aprv_sub_no, send_aprved_no, apv_show_flg) values (";
							$content = array($apply_id, $send_apv_order, $send_apv_sub_order, $apv_order, $recv_apv_sub_order, null, "f");
							$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
							if ($ins == 0) {
								pg_query($this->_db_con, "rollback");
								pg_close($this->_db_con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}
						}
					}
				}
				// 同期通知
				else
				{
					foreach($arr_apv_sub_order as $recv_apv_sub_order)
					{
						$sql = "insert into ovtm_async_recv (apply_id, send_aprv_no, send_aprv_sub_no, recv_aprv_no, recv_aprv_sub_no, send_aprved_no, apv_show_flg) values (";
						$content = array($apply_id, $send_apv_order, null, $apv_order, $recv_apv_sub_order, null, "f");
						$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
						if ($ins == 0) {
							pg_query($this->_db_con, "rollback");
							pg_close($this->_db_con);
							echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
							echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
							exit;
						}
					}
				}
				
			}
			$arr_send_apv_sub_order = array();
			
			// 非同期通知の場合
			if($multi_apv_flg == "t" && $next_notice_div == "1" && count($arr_apv_sub_order) > 1)
			{
				$arr_send_apv_sub_order[$apv_order] = $arr_apv_sub_order;
			}
			// 同期通知または権限並列通知の場合
			else if($multi_apv_flg == "t" && $next_notice_div != "1" && count($arr_apv_sub_order) > 1)
			{
				$arr_send_apv_sub_order[$apv_order] = null;
			}
		}
		return $apply_id;
	}
	
	//呼出・退復申請追加 $status指定
	function insert_rtn_apply($emp_id, $tmp_date, $status, $tmp_o_start_time1, $tmp_o_end_time1, $tmp_o_start_time2, $tmp_o_end_time2, $tmp_o_start_time3, $tmp_o_end_time3) {
		
		// 承認者ワークフロー情報取得
		$sql = "select * from atdbk_wkfw";
		$cond = "";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		
		$rtn_wkfw_div = pg_fetch_result($sel, 0, "rtn_wkfw_div");
		$wkfw_approve_num = pg_fetch_result($sel, 0, "rtncfm");

		$wkfw_flg = ($rtn_wkfw_div != "1") ? false : true;
		
		$arr_wkfwapv = $this->get_wkfwapv_info($emp_id, "3", $wkfw_flg);
		$approve_num = count($arr_wkfwapv);   // 承認者数
		
		
		for ($j=0; $j<$approve_num; $j++) {
			$i = $j + 1;
			$wkfwapv         = $arr_wkfwapv[$j];
			$emp_infos       = $wkfwapv["emp_infos"];
			
			$multi_apv_flg   = $wkfwapv["multi_apv_flg"];	//複数承認者
			$apv_order       = $wkfwapv["apv_order"];		//承認階層
			$apv_sub_order   = $wkfwapv["apv_sub_order"];	//承認順
			$next_notice_div = $wkfwapv["next_notice_div"]; //1:非同期、2同期
			
			$varname = "regist_emp_id$i";
			if (count($emp_infos)> 0) {
				$$varname = $emp_infos[0]["emp_id"];
			} else {
				$$varname = "";
			}
			$varname = "apv_order$i";
			$$varname = $apv_order;
			$varname = "apv_sub_order$i";
			$$varname = $apv_sub_order;
			$varname = "multi_apv_flg$i";
			$$varname = $multi_apv_flg;
			$varname = "next_notice_div$i";
			$$varname = $next_notice_div;
			
			$varname = "pst_approve_num$apv_order";
			$$varname = count($emp_infos);
			
			for($k=0; $k<count($emp_infos); $k++) {
				$varname = "pst_emp_id$apv_order";
				$varname .= "_".($k+1);
				$$varname = $emp_infos[$k]["emp_id"];
			}
			
		}
		// 申請確定情報取得
		if($wkfw_approve_num != "")
		{
			// 申請確定階層取得用配列
			$inputs = array();
			$inputs["wkfw_approve_num"] = $wkfw_approve_num;
			$inputs["approve_num"] = $approve_num;
			for ($i=1; $i<=$approve_num; $i++) {
				$varname = "regist_emp_id$i";
				$inputs["regist_emp_id$i"] = $$varname;
				$varname = "apv_order$i";
				$inputs["apv_order$i"] = $$varname;
			}
			$tmp_cfm = $this->get_apply_confirm_hierarchy($inputs);
		}
		else
		{
			$tmp_cfm = null;
		}
		
		//申請ID
		$apply_id = $this->get_rtn_apply_id();
		$reason = "";
		if ($tmp_cfm == "") {
			$tmp_cfm = null;
		}
		$sql = "insert into rtnapply (apply_id, emp_id, target_date, reason_id, reason, apply_time, apply_status, rtncfm, delete_flg, o_start_time1, o_end_time1, o_start_time2, o_end_time2, o_start_time3, o_end_time3) values (";
		$content = array($apply_id, $emp_id, $tmp_date, null, $wk_reason, date("YmdHis"), $status, $tmp_cfm, "f", $tmp_o_start_time1, $tmp_o_end_time1, $tmp_o_start_time2, $tmp_o_end_time2, $tmp_o_start_time3, $tmp_o_end_time3);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		// 承認登録
		for($k=1; $k<=$approve_num; $k++)
		{
			$varname = "regist_emp_id$k";
			$aprv_emp_id = ($$varname == "") ? null : $$varname;
			
			$varname = "apv_order$k";
			$apv_order = ($$varname == "") ? null : $$varname;
			
			$varname = "apv_sub_order$k";
			$apv_sub_order = ($$varname == "") ? null : $$varname;
			
			$varname = "multi_apv_flg$k";
			$multi_apv_flg = ($$varname == "") ? "f" : $$varname;
			
			$varname = "next_notice_div$k";
			$next_notice_div = ($$varname == "") ? null : $$varname;
			
			if($aprv_emp_id != "")
			{
				// 役職も登録する
				$sql = "select emp_st from empmst ";
				$cond = "where emp_id = '$aprv_emp_id'";
				$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
				if ($sel == 0) {
					pg_query($this->_db_con, "rollback");
					pg_close($this->_db_con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
				$emp_st = pg_fetch_result($sel, 0, 0);
				
				// 承認者情報を登録
				$sql = "insert into rtnaprv (apply_id, aprv_no, aprv_emp_id, aprv_status, aprv_sub_no, multi_apv_flg, next_notice_div, emp_st";
				//承認済にする場合は、日時も更新する
				if ($status == "1") {
					$sql .= ", aprv_date";
				}
				$sql .= ") values (";
				$content = array($apply_id, $apv_order, $aprv_emp_id, $status, $apv_sub_order, $multi_apv_flg, $next_notice_div, $emp_st);
				if ($status == "1") {
					array_push($content, date("YmdHi"));
				}
				$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
				if ($ins == 0) {
					pg_query($this->_db_con, "rollback");
					pg_close($this->_db_con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
			}
		}
		
		// 承認者候補登録
		for($k=1; $k <= $approve_num; $k++)
		{
			$varname = "apv_order$k";
			$apv_order = $$varname;
			
			$wkfwapv         = $arr_wkfwapv[$apv_order-1];
			$emp_infos       = $wkfwapv["emp_infos"];
			
			$multi_apv_flg   = $wkfwapv["multi_apv_flg"];	//複数承認者
			// 複数承認者を「許可しない」かつ、複数いる場合 20100414
			//※無条件の場合に、apply_id, aprv_no, person_noのキーの重複でDBエラーとなる
			if ($multi_apv_flg != 't' && count($emp_infos) > 1) { 
				
				
				$varname = "pst_approve_num$apv_order";
				$pst_approve_num = $$varname;
				
				for($j=1; $j<=$pst_approve_num; $j++)
				{
					$varname = "pst_emp_id$apv_order";
					$varname .= "_$j";
					$pst_emp_id = $$varname;
					
					$sql = "insert into rtnaprvemp (apply_id, aprv_no, person_no, emp_id, delete_flg) values (";
					$content = array($apply_id, $apv_order, $j, $pst_emp_id, 'f');
					$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
					if ($ins == 0) {
						pg_query($this->_db_con, "rollback");
						pg_close($this->_db_con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}
				}
			}
		}
		
		// 非同期・同期受信登録
		$previous_apv_order = "";
		$arr_apv = array();
		$arr_apv_sub_order = array();
		for($k=1; $k <= $approve_num; $k++)
		{
			$varname = "apv_order$k";
			$apv_order = $$varname;
			
			$varname = "apv_sub_order$k";
			$apv_sub_order = ($$varname == "") ? null : $$varname;
			
			$varname = "multi_apv_flg$k";
			$multi_apv_flg = ($$varname == "") ? "f" : $$varname;
			
			$varname = "next_notice_div$k";
			$next_notice_div = ($$varname == "") ? null : $$varname;
			
			if($previous_apv_order != $apv_order)
			{
				$arr_apv_sub_order = array();
			}
			$arr_apv_sub_order[] = $apv_sub_order;
			$arr_apv[$apv_order] = array("multi_apv_flg" => $multi_apv_flg, "next_notice_div" => $next_notice_div, "apv_sub_order" => $arr_apv_sub_order);
			
			$previous_apv_order = $apv_order;
		}
		
		$arr_send_apv_sub_order = array();
		foreach($arr_apv as $apv_order => $apv_info)
		{
			$multi_apv_flg = $apv_info["multi_apv_flg"];
			$next_notice_div = $apv_info["next_notice_div"];
			$arr_apv_sub_order = $apv_info["apv_sub_order"];
			
			foreach($arr_send_apv_sub_order as $send_apv_order => $arr_send_apv_sub)
			{
				// 非同期通知
				if($arr_send_apv_sub != null)
				{
					foreach($arr_send_apv_sub as $send_apv_sub_order)
					{
						foreach($arr_apv_sub_order as $recv_apv_sub_order)
						{
							$sql = "insert into rtn_async_recv (apply_id, send_aprv_no, send_aprv_sub_no, recv_aprv_no, recv_aprv_sub_no, send_aprved_no, apv_show_flg) values (";
							$content = array($apply_id, $send_apv_order, $send_apv_sub_order, $apv_order, $recv_apv_sub_order, null, "f");
							$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
							if ($ins == 0) {
								pg_query($this->_db_con, "rollback");
								pg_close($this->_db_con);
								echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
								echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
								exit;
							}
						}
					}
				}
				// 同期通知
				else
				{
					foreach($arr_apv_sub_order as $recv_apv_sub_order)
					{
						$sql = "insert into rtn_async_recv (apply_id, send_aprv_no, send_aprv_sub_no, recv_aprv_no, recv_aprv_sub_no, send_aprved_no, apv_show_flg) values (";
						$content = array($apply_id, $send_apv_order, null, $apv_order, $recv_apv_sub_order, null, "f");
						$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
						if ($ins == 0) {
							pg_query($this->_db_con, "rollback");
							pg_close($this->_db_con);
							echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
							echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
							exit;
						}
					}
				}
				
			}
			$arr_send_apv_sub_order = array();
			
			// 非同期通知の場合
			if($multi_apv_flg == "t" && $next_notice_div == "1" && count($arr_apv_sub_order) > 1)
			{
				$arr_send_apv_sub_order[$apv_order] = $arr_apv_sub_order;
			}
			// 同期通知または権限並列通知の場合
			else if($multi_apv_flg == "t" && $next_notice_div != "1" && count($arr_apv_sub_order) > 1)
			{
				$arr_send_apv_sub_order[$apv_order] = null;
			}
		}
		return $apply_id;
	}
	
	
	/**
	 * 勤務実績の最終更新者、日時を更新する
	 *
	 * @param mixed $emp_id 職員ID
	 * @param mixed $target_date 対象日付
	 * @param mixed $login_emp_id 更新者職員ID
	 * @return なし
	 *
	 */
	function update_atdbkrslt_emp_id_time($emp_id, $target_date, $login_emp_id) {

		$sql = "update atdbkrslt set";
		$set = array("update_emp_id", "update_time");
		$setvalue = array($login_emp_id, date("YmdHis"));
		$cond = "where emp_id = '$emp_id' and date = '$target_date'";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
	}
	
	
	/**
	 * 一括修正申請情報を取得する
	 *
	 * @param mixed $emp_id 職員ID
	 * @param mixed $start_date 開始日
	 * @param mixed $end_date 終了日
	 * @return array 一括修正申請情報の配列
	 *
	 */
	function get_tmmdapplyall_status($emp_id, $start_date, $end_date) {
		$sql = "select a.apply_id, a.emp_id, a.target_date, a.a_start_time, a.a_end_time, b.apply_status from tmmdapplyall a left join tmmdapply b on a.apply_id = b.apply_id";
		$cond = "where a.emp_id = '$emp_id' and a.target_date >= '$start_date' and a.target_date <= '$end_date'";
		$sel = select_from_table($this->_db_con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$num = pg_numrows($sel);
		$arr_data = array();
		for ($i=0; $i<$num; $i++) {
			$tmp_date = pg_fetch_result($sel, $i, "target_date");
			$arr_data[$tmp_date]["apply_status"] = pg_fetch_result($sel, $i, "apply_status");
			$arr_data[$tmp_date]["apply_id"] = pg_fetch_result($sel, $i, "apply_id");
			$arr_data[$tmp_date]["a_start_time"] = pg_fetch_result($sel, $i, "a_start_time");
			$arr_data[$tmp_date]["a_end_time"] = pg_fetch_result($sel, $i, "a_end_time");
		}
		return $arr_data;
	}

		
	/**
	 * 残業申請不要データを削除する
	 *
	 * @param mixed $emp_id 職員ID
	 * @param mixed $date 対象日付
	 * @return なし
	 *
	 */
	function delete_no_apply_ovtm_data($emp_id, $date) {
		
		$sql = "select apply_id from ovtmapply";
		$cond = "where emp_id = '$emp_id' and target_date = '$date' and apply_status = '4'";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		if (pg_num_rows($sel) > 0)
		{
			$apply_id = pg_fetch_result($sel, 0, 0);
			//残業申請
			$sql = "delete from ovtmapply";
			$cond = "where apply_id = $apply_id";
			$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
			if ($del == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			//残業申請承認
			$sql = "delete from ovtmaprv";
			$cond = "where apply_id = $apply_id";
			$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
			if ($del == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			//承認候補
			$sql = "delete from ovtmaprvemp";
			$cond = "where apply_id = $apply_id";
			$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
			if ($del == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			//同期・非同期情報
			$sql = "delete from ovtm_async_recv";
			$cond = "where apply_id = $apply_id";
			$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
			if ($del == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}
}
?>
