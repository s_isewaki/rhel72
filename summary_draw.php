<?
/*

画面パラメータ
	$session
		セッションID
	$caller
		呼び出し元識別子
*/
if (@$caller=="") $caller = "";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<?
require("about_authority.php");
require("about_session.php");
require("about_postgres.php");
require("label_by_profile_type.ini");
require("./get_values.ini");
require_once("summary_common.ini");
require_once("get_menu_label.ini");

//ページ名
$fname = $PHP_SELF;

//==============================
//セッションのチェック
//==============================
$session = qualify_session($session,$fname);
if($session == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
// 権限チェック
//==============================
$summary = check_authority($session, 57, $fname);
if ($summary == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
//DBコネクション
//==============================
$con = connect2db($fname);
if($con == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
// 組織タイプを取得
//==============================
$profile_type = get_profile_type($con, $fname);

//利用者IDを取得
$emp_id = get_emp_id($con, $session, $fname);

//==========================================================================================
//HTML出力
//==========================================================================================
?>
<title>CoMedix <?=get_report_menu_label($con, $fname)?>｜描画画面</title>
<script src="js/prototype/dist/prototype.js"></script>
<script src="js/jquery.js"></script>


<script type="text/javascript">
//画像挿入時の処理を行います。
function cvsDataSend() {

	InputCheck();
	if(window.opener && !window.opener.closed && window.opener.call_back_draw) {

		var imgData = document.tmplform.canvas_data.value;
		var pdfData = document.tmplform.canvas_pdf.value;
		window.opener.call_back_draw(imgData, pdfData);
		window.close();
		return;

	}

}

</script>

<!-- 描画スタイル -->
<?

    //----------------------------------------------------------------
    // Canvas画像ファイル名セット
    //----------------------------------------------------------------
    $canvas_jpg_name = 'cpd_' . md5(rand()) . '.jpg';

    // Canvas位置微調整(for mac firefox)
    $user_agent  = $_SERVER['HTTP_USER_AGENT'];
//    $canvas_ch_d = '154px';
//    $canvas_ch_p = '188px';
    $canvas_ch_d = '85px';
//    $canvas_ch_p = '188px';

    if (preg_match('/macintosh/i', $user_agent) && preg_match('/firefox/i', $user_agent)) {
        $canvas_ch_d = '87px';
//        $canvas_ch_p = '190px';
    }
?>
<style type="text/css">
.pad2px { pading-left:2px; padding-right:2px; }

#canvas {
    background-color:#ffffff;
    position:absolute;
    margin-top:<?=@$canvas_ch_d?>;
//    margin-top:70px;
    margin-left:110px;
    top:0px;
    width:500px;
    height:500px;
    z-index:2;
    border:1px solid #aaaaaa;
}
#toolbar {
    margin-top:0px;
    margin-left:30px;
    width:53px;
    height:270px;
    padding:10px 5px 0px 8px;
    border:1px solid;
    border-top-color:threedhighlight;
    border-left:0;
    border-right-color:threedshadow;
    border-bottom-color:threedshadow;
}
#toolbar ul {
    margin:0;
    padding:0;
    list-style:none
}
#toolbar ul li {
    margin:0;
    padding:0;
}
#toolbar #buttons li {
    float:left;
    width:23px;
    height:23px;
    border:1px solid blue;
    border-top-color:threedhighlight;
    border-left-color:threedhighlight;
    border-right-color:threeddarkshadow;
    border-bottom-color:threeddarkshadow;
}
#toolbar #buttons li img {
    width:21px;
    height:21px;
    border:1px solid red;
    border-top-color:threedlightshadow;
    border-left-color:threedlightshadow;
    border-right-color:threedshadow;
    border-bottom-color:threedshadow;
}
#toolbar #buttons li.down, #toolbar #buttons li.sel {
    border-top-color:windowframe;
    border-left-color:windowframe;
    border-right-color:threedhighlight;
    border-bottom-color:threedhighlight;
}
#toolbar #buttons li.down img, #toolbar #buttons li.sel img {
    background-color:threedhighlight;
    padding-left:1px;
    padding-top:1px;
    width:21px;
    height:21px;
    border-top-color:threedshadow;
    border-left-color:threedshadow;
    border-right-color:threedlightshadow;
    border-bottom-color:threedlightshadow;
}
#toolbar #buttons li.down div {
    background-color:threedface;
}
#toolbar #buttons:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}
#toolbar #settings {
    padding:2px;
    border:1px solid;
    border-top-color:threedshadow;
    border-left-color:threedshadow;
    border-right-color:threedhighlight;
    border-bottom-color:threedhighlight;
    margin:7px;
    margin-left:2px;
    margin-top:10px;
    height:65px;
    width:42px;
}
#toolbar #settings > div {
    display:none;
}
#toolbar #settings div div {
    cursor:default;
    text-align:center;
    float:left;
    width:33%;
    width:12px;
    height:12px;
    margin-bottom:2px;
}
#toolbar #settings div .sel {
    background-color:highlight;
    color:highlighttext;
}
#toolbar #settings div#airbrush-settings {
    padding-top: 2px;
    margin-left: 2px;
    display: none;
}
#toolbar #settings div#brush-settings {
    margin-top:12px;
}
#toolbar #settings div#eraser-settings div {
    margin-left:12px;
    margin-bottom:0px;
    float:none;
}
#toolbar #colorbar {
    height:16px;
    margin-top:10px;
    margin-left:2px;
    width:48px;
}
#toolbar #colorbar ul {
    margin:0;
    padding:0;
    list-style:none;
}
#toolbar #colorbar ul li {
    margin:0;
    padding:0;
}
#toolbar #colorbar #colors {
    width:50px;
}
#toolbar #colorbar #colors li {
    float:left;
    height:14px;
    width:14px;
    border:1px solid red;
    border-top-color:threedlightshadow;
    border-left-color:threedlightshadow;
    border-right-color:threedshadow;
    border-bottom-color:threedshadow;
}
#toolbar #colorbar #colors li.sel {
    border-top-color:windowframe;
    border-left-color:windowframe;
    border-right-color:threedhighlight;
    border-bottom-color:threedhighlight;
}
#toolbar #colorbar label {
    margin-left:10px;
    white-space:nowrap;
}
#toolbar #colorbar #colors li div {
    margin:0;
    float:none;
    width:12px;
    height:12px;
    border:1px solid;
    border-top-color:windowframe;
    border-left-color:windowframe;
    border-right-color:threedlightshadow;
    border-bottom-color:threedlightshadow;
}
</style>

<!-- 描画スタイル -->
<script type="text/javascript">
// 描画Javascript
	var canvas, c;
function canvasLoad(){

	// set up canvas
	canvas = document.getElementById('canvas');

	if (canvas.getContext) {

		c = canvas.getContext('2d');
		c.globalAlpha = 1.0;
		// set up defaults
		c.tool = new tool.brush();
		c.lineWidth   = 5;
		c.lineCap     = 'round';
		c.strokeStyle = '#000';
		c.fillStyle   = '#000';
		c.tertStyle   = '#000';

		// set up events
		canvas.onmousedown = c_down;
		canvas.onmousemove = c_move;
		canvas.onmouseout  = c_out;
		canvas.onmouseup   = c_up;
	}


	callPng();
}
    function getxy(e, o)
    {
        if (c) {
            var bo = getpos(o);
            var x = e.clientX - bo.x;
            var y = e.clientY - bo.y;

            x += document.documentElement.scrollLeft;
            y += document.documentElement.scrollTop;
            return { x: x-.5, y: y-.5 };
        }
    }

    function getpos(o)
    {
        // gets position of object o
        var bo, x, y, b; x = y = 0;
        if (document.getBoxObjectFor) {    // moz
            bo = document.getBoxObjectFor(o);
            x = bo.x; y = bo.y;
        } else if (o.getBoundingClientRect) {    // ie (??)
            bo = o.getBoundingClientRect();
            x = bo.left; y = bo.top;
        } else {    // opera, safari etc
            while(o && o.nodeName != 'BODY') {
                x += o.offsetLeft;
                y += o.offsetTop;
                b = parseInt(document.defaultView.getComputedStyle(o,null).getPropertyValue('border-width'));
                if (b > 0) { x += b; y +=b; }
                o = o.offsetParent;
            }
        }
        return { x:x, y:y }
    }

    var tool = {

        _brushes: function() {

            this.down = function()
            {
                this.last         = null;
                this.cp           = null;
                this.lastcp       = null;
                this.disconnected = null;
                c.beginPath();

                this.sstart = this.last = { x:m.x, y:m.y }
                this.status = 1;
            }
            this.move = function(e)
            {
                if (this.disconnected) {
                    this.disconnected = null;
                    this.last = { x:m.x, y:m.y }
                } else {
                    this.draw();
                }
                c.moveTo(m.x, m.y);
            }
            this.up = function()
            {
                if (this.sstart && this.sstart.x == m.x && this.sstart.y == m.y) {
                    drawDot(m.x, m.y, c.lineWidth, c.strokeStyle);
                }
                this.sstart = null;
                this.status = 0;
            }
            this.draw = function()
            {
                c.lineTo(m.x, m.y);
                c.stroke();
                c.beginPath();

                this.last = { x:m.x, y:m.y }
            }
        },

        _stamp1: function() {

            this.down = function()
            {
                this.last         = null;
                this.cp           = null;
                this.lastcp       = null;
                this.disconnected = null;
                c.beginPath();

                this.sstart = this.last = { x:m.x, y:m.y }
                this.status = 1;

                var img = new Image();
                img.onload = function() {
                    var sa = { x:m.x, y:m.y }
                    var pngx = sa.x - 15;
                    var pngy = sa.y - 15;
                    c.drawImage(img, pngx, pngy);
                }

                img.src = 'canvas_painter/kyobu01_tp.png?' + new Date().getTime();
            }
            this.move = function(e) {}
            this.up = function() {}
            this.draw = function() {}
        },

        _stamp2: function() {

            this.down = function()
            {
                this.last         = null;
                this.cp           = null;
                this.lastcp       = null;
                this.disconnected = null;
                c.beginPath();

                this.sstart = this.last = { x:m.x, y:m.y }
                this.status = 1;

                var img = new Image();
                img.onload = function() {
                    var sa = { x:m.x, y:m.y }
                    var pngx = sa.x - 15;
                    var pngy = sa.y - 15;
                    c.drawImage(img, pngx, pngy);
                }
                img.src = 'canvas_painter/kyobu02_tp.png?' + new Date().getTime();
            }
            this.move = function(e) {}
            this.up = function() {}
            this.draw = function() {}
        },

        _texts: function() {

            this.down = function()
            {
                this.last         = null;
                this.cp           = null;
                this.lastcp       = null;
                this.disconnected = null;
                c.beginPath();

                this.sstart = this.last = { x:m.x, y:m.y }
                this.status = 1;
                c.font = "12px 'ＭＳ Ｐゴシック'";
                var tx_val = $('#pastext_val').val();
                var sa = { x:m.x, y:m.y }
                c.fillStyle = "black";
                c.fillText(tx_val, sa.x, sa.y);
            }
            this.move = function(e) {}
            this.up = function() {}
            this.draw = function() {}
        },

        _erasers: function() {

            this.down = function()
            {
                this.last         = null;
                this.cp           = null;
                this.lastcp       = null;
                this.disconnected = null;
                c.beginPath();

                this.sstart = this.last = { x:m.x, y:m.y }
                this.status = 1;
                var sa = { x:m.x, y:m.y }
                var crx = sa.x - (c.lineWidth / 2)
                var cry = sa.y - (c.lineWidth / 2)
                c.fillStyle = '#ffffff';
                c.fillRect(crx, cry, c.lineWidth, c.lineWidth);
            }
            this.move = function(e)
            {

                if (this.disconnected) {
                    this.disconnected = null;
                    this.last = { x:m.x, y:m.y }
                } else {
                    this.draw();
                }
                c.moveTo(m.x, m.y);
            }
            this.up = function()
            {
                this.sstart = null;
                this.status = 0;
            }
            this.draw = function()
            {
                c.stroke();
                c.beginPath();

                var sa = { x:m.x, y:m.y }
                var crx = sa.x - (c.lineWidth / 2)
                var cry = sa.y - (c.lineWidth / 2)

                c.fillStyle = '#ffffff';
                c.fillRect(crx, cry, c.lineWidth, c.lineWidth);
            }
        },

        stamp1: function()
        {
            this.name = 'stamp1';
            this.status = 0;
            this.inherit = tool._stamp1; this.inherit();
        },

        stamp2: function()
        {
            this.name = 'stamp2';
            this.status = 0;
            this.inherit = tool._stamp2; this.inherit();
        },

        text: function()
        {
            this.name = 'text';
            this.status = 0;
            this.inherit = tool._texts; this.inherit();
        },

        pencil: function()
        {
            this.name = 'pencil';
            this.status = 0;
            this.inherit = tool._brushes; this.inherit();

            c.lineCap = 'butt';
            c.lineWidth = 1;
        },

        brush: function()
        {
            this.name = 'brush';
            this.status = 0;
            this.inherit = tool._brushes; this.inherit();
        },

        eraser: function()
        {
            this.name = 'eraser';
            this.status = 0;
            this.inherit = tool._erasers; this.inherit();
        },

        t_base: function()
        {
            this.name = 't_base';
            this.status = 0;
        },

        l_base: function()
        {
            this.name = 'l_base';
            this.status = 0;
        },

        f_base: function()
        {
            this.name = 'f_base';
            this.status = 0;
        },

        j_base: function()
        {
            this.name = 'j_base';
            this.status = 0;
        },

        k_base: function()
        {
            this.name = 'k_base';
            this.status = 0;
        },

        airbrush: function()
        {
            this.name = 'airbrush';
            this.status = 0;
            c.lineCap = 'square';

            this.down = function()
            {
                this.drawing = setInterval('c.tool.draw()', 50);
                this.last = { x:m.x, y:m.y }
                this.lineCap = 'square';
                this.status = 1;
            }
            this.move = function(e)
            {
                this.last = { x:m.x, y:m.y }
            }
            this.up = function(e)
            {
                clearInterval(this.drawing);
                this.status = 0;
            }
            this.draw = function()
            {
                c.save();
                c.beginPath();
                c.arc(this.last.x, this.last.y, c.lineWidth*4, 0, Math.PI*2, true);
                c.clip();
                for (var i = c.lineWidth * 15; i > 0; i--) {
                    var rndx = c.tool.last.x + Math.round(Math.random() * (c.lineWidth * 8) - (c.lineWidth * 4));
                    var rndy = c.tool.last.y + Math.round(Math.random() * (c.lineWidth * 8) - (c.lineWidth * 4));
                    drawDot(rndx, rndy, 1, c.strokeStyle);
                }
                c.restore();
            }
        }
    }

    function c_down(e)
    {
        var source = e.currentTarget;
        m = getxy(e, canvas);
        c.tool.down(e);
        c.moveTo(m.x, m.y);

        return false;
    }

    function c_up(e)
    {
        m = getxy(e, canvas);
        e.stopPropagation();
        c.tool.up(e);
        return false;
    }

    function c_move(e)
    {
        m = getxy(e, canvas);
        e.stopPropagation();

        if (0 < c.tool.status) {
            c.tool.move(e);
        }
        return false;
    }

    function c_out(e)
    {
        if (c && (c.tool.name == 'pencil' || c.tool.name == 'eraser' || c.tool.name == 'brush') && c.tool.status == 1) {
            c.tool.disconnected = 1;
            m = getxy(e, canvas);
            c.tool.draw();
        }
    }

    function drawDot(x, y, size, col, trg)
    {
        x = Math.floor(x) + 1;
        y = Math.floor(y) + 1;

        if (x > 0 && y > 0) {
            if (!trg) { trg = c; }
            if (col || size) { var lastcol = trg.fillStyle; var lastsize = trg.lineWidth; }
            if (col)  { trg.fillStyle = col;  }
            if (size) { trg.lineWidth = size; }
            if (trg.lineCap == 'round') {
                trg.arc(x, y, trg.lineWidth / 2, 0, (Math.PI / 180) * 360, false);
                trg.fill();
            } else {
                var dotoffset = (trg.lineWidth > 1) ? trg.lineWidth / 2 : trg.lineWidth;
                trg.fillRect((x - dotoffset), (y - dotoffset), trg.lineWidth, trg.lineWidth);
            }
            if (col || size) { trg.fillStyle = lastcol; trg.lineWidth = lastsize; }
        }
    }

    function selCol(o, e)
    {
        col = (typeof(o) == 'string') ? o : o.style.backgroundColor;
        selCol2(col, e);
    }

    function selCol2(col, e)
    {
        c.strokeStyle = col;
        if (c.lastStrokeStyle) { c.lastStrokeStyle = col; }
        if (e) e.preventDefault();
    }

    function cb(o)
    {
        var colorsbtns = document.getElementById('colors').getElementsByTagName('li');
        for (var i = 0; i < colorsbtns.length; ++i) {
            if (colorsbtns[i].className == 'sel') {
                colorsbtns[i].className = '';
            }
        }
        o.className = 'sel';
    }

    function selTool(o)
    {
        c.tool.status = 0;
        canvas.style.display = '';
        var newtool = o.id;

        // button highlighting
        var toolbarbtns = document.getElementById('buttons').getElementsByTagName('li');
        for (var i = 0; i < toolbarbtns.length; ++i) {
            if (toolbarbtns[i].className == 'sel') {
                toolbarbtns[i].className = '';
            }
        }

        if (o.id != 't_base'
			&& o.id != 'l_base'
			&& o.id != 'f_base'
			&& o.id != 'j_base'
			&& o.id != 'k_base') {
            o.className = 'sel';
        }

        // reset color (after eraser and select)
        if (c.lastStrokeStyle) {
            selCol(c.lastStrokeStyle);
            c.lastStrokeStyle = null;
        }

        c.lastTool = c.tool.name;
        c.tool = new tool[newtool]();

        var txsw = c.tool.name;
        if (txsw == 'text') {
            $('#pastext').show();
        } else {
            $('#pastext').hide();
        }

        // settings panel switching
        var settingpanels = document.getElementById('settings').getElementsByTagName('div');
        for (var i = 0; i < settingpanels.length; ++i) {
            if (settingpanels[i].style.display == 'block') {
               settingpanels[i].style.display = 'none';
            }
        }
        if (document.getElementById(c.tool.name + '-settings')) {
            document.getElementById(c.tool.name + '-settings').style.display = 'block';
        }

		if (o.id == 't_base') {
			// 上書きかどうかをチェック
			if(document.getElementById('overwrite').value != o.id){
				cvsAllClear();
			}
            var img = new Image();
            img.onload = function() {
                c.drawImage(img, 75, 75);
            }
            img.src = 'canvas_painter/toubu_base_tp_350.png?' + new Date().getTime();
            resetCanvas();
			document.getElementById('overwrite').value = o.id;
        }
		if (o.id == 'l_base') {
			// 上書きかどうかをチェック
			if(document.getElementById('overwrite').value != o.id){
				cvsAllClear();
			}
            var img = new Image();
            img.onload = function() {
                c.drawImage(img, 75, 75);
            }
            img.src = 'canvas_painter/naishikyo_base_1_tp.png?' + new Date().getTime();
            resetCanvas();
			document.getElementById('overwrite').value = o.id;
        }
        if (o.id == 'f_base') {
			// 上書きかどうかをチェック
			if(document.getElementById('overwrite').value != o.id){
				cvsAllClear();
			}
            var img = new Image();
            img.onload = function() {
                c.drawImage(img, 75, 75);
            }
            img.src = 'canvas_painter/fukubu_base_tp.png?' + new Date().getTime();
            resetCanvas();
			document.getElementById('overwrite').value = o.id;
        }

        if (o.id == 'j_base') {
			// 上書きかどうかをチェック
			if(document.getElementById('overwrite').value != o.id){
				cvsAllClear();
			}
            var img = new Image();
            img.onload = function() {
                c.drawImage(img, 75, 75);
            }
            img.src = 'canvas_painter/jyokusou_base_tp.png?' + new Date().getTime();
            resetCanvas();
			document.getElementById('overwrite').value = o.id;
        }
        if (o.id == 'k_base') {
			// 上書きかどうかをチェック
			if(document.getElementById('overwrite').value != o.id){
				cvsAllClear();
			}
            var img = new Image();
            img.onload = function() {
                c.drawImage(img, 75, 75);
            }
            img.src = 'canvas_painter/kyobu_base_tp.png?' + new Date().getTime();
            resetCanvas();
			document.getElementById('overwrite').value = o.id;
        }

        if (o.id == 'eraser') {
            c.lineWidth = 7;
        }
        if (o.id == 'airbrush') {
            c.lineWidth = 1;
        }
        if (o.id == 'brush') {
            c.lineWidth = 5;
        }
    }

    function selSetting(o, sett)
    {
        c.tool.status = 0;

        if (document.getElementById(c.tool.name + '-settings')) {
            var settingbtns = document.getElementById(c.tool.name + '-settings').childNodes;
            for (var i = 0; i < settingbtns.length; ++i) {
                if (settingbtns[i].className == 'sel') {
                   settingbtns[i].className='';
                }
            }
            o.className = 'sel';
            eval(sett);
        }
    }

<? // 保存画像呼び出し ?>
    function callPng()
    {
		if(window.opener.document.getElementById('canvas_data').value != "") {

        	var img = new Image();
        	img.onload = function() {
            	c.drawImage(img, 0, 0);
			}
			img.src = window.opener.document.getElementById('canvas_data').value;
		}else{

			c.fillStyle = '#ffffff';
			c.fillRect(0, 0, 500, 500);
			var img = new Image();

			img.onload = function() {
				c.drawImage(img, 0, 0);
//            c.drawImage(img, 75, 75);
			}
//        img.src = 'canvas_painter/jyokusou_base_tp.png?' + new Date().getTime();
		}

    }

    function InputCheck()
    {
        saveCanvas();
        return true;
    }

    function saveCanvas()
    {
        var can = $('#canvas')[0];
        var d = can.toDataURL('image/jpeg').replace('image/jpeg', 'image/octet-stream');
        document.tmplform.canvas_data.value = d;	// 書き込み部分の保存

        var d_jpg = can.toDataURL('image/jpeg').replace('data:image/jpeg;base64,', '');
        document.tmplform.canvas_pdf.value = d_jpg;	// PDF画像作成用データ
    }

    function setText(tag, text)
    {
        if (tag != null && text != null) {
            try { tag.innerText   = text ; } catch(e) {}
            try { tag.textContent = text ; } catch(e) {}
        }
    }

    function cvsAllClear()
    {

		c.fillStyle = '#ffffff';
		c.fillRect(0, 0, 500, 500);

        resetCanvas();
    }

    function resetCanvas()
    {
        // toolbar button reset
        var tb1 = document.getElementById('buttons').getElementsByTagName('li');
        for (var i = 0; i < tb1.length; ++i) {
            tb1[i].className = '';
        }
        tb1[3].className = 'sel';

        // settings reset
        $('#brush-settings').show();
        $('#airbrush-settings').hide();
        $('#eraser-settings').hide();
        for (var i = 1; i < 7; ++i) {
            document.getElementById('bs1_' + i).className = '';
        }
        document.getElementById('bs1_2').className = 'sel';

        // color button reset
        var cb1 = document.getElementById('colors').getElementsByTagName('li');
        cb1[0].className = 'sel';
        cb1[1].className = '';
        cb1[2].className = '';

        // 'pastext' hide
        $('#pastext').hide();

        // toolbar default set
        c.tool = new tool.brush();
        c.lineWidth   = 5;
        c.lineCap     = 'round';
        c.strokeStyle = '#000';
        c.fillStyle   = '#000';
        c.tertStyle   = '#000';
    }

    function btnDown(o)
    {
        o.className = 'sel';
    }

    function btnUp(o)
    {
        o.className = '';

    }
</script>

</head>
<body onLoad="canvasLoad();" style="background-color:#ffc; background-image:url(img/tmpl_white_header.gif); background-repeat:repeat-x;">
<div style="padding:4px">

<!-- ヘッダ -->
<?= summary_common_show_dialog_header("描画画面"); ?>
</div>
<?
//==============================
//入力フィールド 出力
//==============================
?>
<!--form name="main_form" action="summary_fixedform_delete.php" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="caller" value="<?=$caller?>">
<input type="hidden" name="caller_type" value="">
<input type="hidden" name="multi_flg" value="<?=@$multi_flg?>">

<div class="search_button_div">
	<input type="button" onclick="show_regist_window();" value="追加"/>
	<input type="button" onclick="deleteFixedform();" value="削除"/>
</div>


</form-->

<div style="background-color:#ffc;">
<?
//==============================
//描画画面 出力
//==============================
?>
<?
//===============================================================
// ペイントツール＆キャンバス
//===============================================================
?>

<form name="tmplform">
<table border=0 style="width:650px; height:600px;background-color:#ffc;">
<tr style="height:50px;">
<td>
<div style="text-align:right;"><input id="insert" type="button" onClick="cvsDataSend();" value="挿入" style="align:right;margin-right:35px"></div>
</td>
</tr>
<tr><td>

<table border=0 style="width:650px; height:550px;">

<tr style="height:500px;">
<td>

<div id="toolbar" style="background:#ffffff;">
<ul id="buttons">
<li title="スタンプ１" onclick="selTool(this)" id="stamp1"   class=""><img src="canvas_painter/kyobu01_r.png"></li>
<li title="スタンプ２" onclick="selTool(this)" id="stamp2"   class=""><img src="canvas_painter/kyobu02_r.png"></li>
<li title="スプレー"   onclick="selTool(this)" id="airbrush" class=""><img src="canvas_painter/cpd_btn_airbrush.png"></li>
<li title="ライン"     onclick="selTool(this)" id="brush"    class="sel"><img src="canvas_painter/cpd_btn_lines.png"></li>
<li title="テキスト"   onclick="selTool(this)" id="text"     class=""><img src="canvas_painter/cpd_btn_text.png"></li>
<li title="消しゴム"   onclick="selTool(this)" id="eraser"   class=""><img src="canvas_painter/cpd_btn_eraser.png"></li>
<li title="頭部上書き" onclick="selTool(this)" id="t_base" onMouseDown="btnDown(this)" onMouseUp="btnUp(this)" class=""><img src="canvas_painter/cpd_btn_tbu_bg.png"></li>
<li title="内視鏡上書き" onclick="selTool(this)" id="l_base" onMouseDown="btnDown(this)" onMouseUp="btnUp(this)" class=""><img src="canvas_painter/cpd_btn_nsk_bg01.png"></li>
<li title="腹部上書き" onclick="selTool(this)" id="f_base" onMouseDown="btnDown(this)" onMouseUp="btnUp(this)" class=""><img src="canvas_painter/cpd_btn_fkb_bg.png"></li>
<li title="褥瘡上書き" onclick="selTool(this)" id="j_base" onMouseDown="btnDown(this)" onMouseUp="btnUp(this)" class=""><img src="canvas_painter/cpd_btn_jks_bg.png"></li>
<li title="胸部上書き" onclick="selTool(this)" id="k_base" onMouseDown="btnDown(this)" onMouseUp="btnUp(this)" class=""><img src="canvas_painter/kyobu07_r.png"></li>
<li title="全消し"     onclick="cvsAllClear();" id="dustbox" onMouseDown="btnDown(this)" onMouseUp="btnUp(this)" class=""><img src="canvas_painter/cpd_btn_dustbox.png"></li>
</ul>

<!-- 画像種類切り替え用 -->
<input id="overwrite" type="hidden" value="">
<!--  -->

	<div id="settings" style="background:#f8f8ff;">
    	<div id="brush-settings" style="display:block;">
      		<div style="padding:2px; font-size:110%; line-height: 6px;" id="bs1_1" onclick="selSetting(this, 'c.lineWidth=8;c.lineCap=\'round\'')">●</div>
      		<div style="font-size: 70%; line-height: 10px;" id="bs1_2" onclick="selSetting(this, 'c.lineWidth=5;c.lineCap=\'round\'')"class="sel">●</div>
      		<div style="font-size: 40%; line-height: 13px;" id="bs1_3" onclick="selSetting(this, 'c.lineWidth=2;c.lineCap=\'round\'')">●</div>
      		<br /><br />
      		<div style="padding:2px; font-size:110%; line-height: 6px;" id="bs1_4" onclick="selSetting(this, 'c.lineWidth=8;c.lineCap=\'square\'')">■</div>
      		<div style="font-size: 70%; line-height: 10px;" id="bs1_5" onclick="selSetting(this, 'c.lineWidth=5;c.lineCap=\'square\'')">■</div>
      		<div style="font-size: 40%; line-height: 13px;" id="bs1_6" onclick="selSetting(this, 'c.lineWidth=2;c.lineCap=\'square\'')">■</div>
    	</div>
    	<div id="airbrush-settings">
      	<img height="19" width="19" style="float: left;" src="canvas_painter/airbrush_s.gif" class="sel" onclick="selSetting(this, 'c.lineWidth=1;')">
      	<img height="19" width="19" style="float: left;" src="canvas_painter/airbrush_m.gif" onclick="selSetting(this, 'c.lineWidth=2;')"><br>
      	<img height="24" width="24" style="clear: left; margin-top: 6px; margin-left: 7px;" src="canvas_painter/airbrush_l.gif" onclick="selSetting(this, 'c.lineWidth=3;')">
    	</div>
		<div id="eraser-settings">
			<div style="padding:4px 2px 0px 1px; font-size: 50%; vartical-align:middle;" onclick="selSetting(this, 'c.lineWidth=3')">■</div>
			<div style="padding:1px 1px 2px 2px; font-size: 80%; vartical-align:middle;" onclick="selSetting(this, 'c.lineWidth=5')">■</div>
			<div style="padding:1px 2px 3px 2px; font-size: 100%; vartical-align:middle;" onclick="selSetting(this, 'c.lineWidth=7')" class="sel">■</div>
			<div style="padding:1px 2px 2px 1px; font-size: 120%; vartical-align:middle;" onclick="selSetting(this, 'c.lineWidth=12')">■</div>
    	</div>
	</div>
	<div id="colorbar">
	<ul id="colors">
	<li style="background-color:#000" onclick="cb(this)" class="sel" onmousedown="selCol(this, event)"><div></div></li>
	<li style="background-color:#F00" onclick="cb(this)" class=""    onmousedown="selCol(this, event)"><div></div></li>
	<li style="background-color:#00F" onclick="cb(this)" class=""    onmousedown="selCol(this, event)"><div></div></li>
    </ul>
	</div>
</div>
<input id="canvas_data" type="hidden" name="canvas_data" value="">
<input id="canvas_pdf" type="hidden" name="canvas_pdf" value="">
<canvas id="canvas" class="canvas" width="500" height="500" style="background-color:#ffffff;"></canvas>
  </td>
</tr>
<tr style="height:50px;">
<td>
<!--div id="pt_dummy" style="height:0px; margin-top:58px; display:block;"></div-->
<div id="pastext" style="width:438px; height:30px; margin:-5px 0px 8px 110px; display:none; background-color: #FFDCA2;">　貼付テキスト入力：
<input id="pastext_val" type="text" value="" style="width:300px; margin-top:4px;">
</div>
</td>
</tr>

</table>

</td></tr>
</table>
</form>
<?
//==============================
//終了HTML 出力
//==============================
?>

</div>
</body>
</html>

<?

pg_close($con);

?>
