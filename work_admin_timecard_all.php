<?
require_once('Cmx.php');
require_once('Cmx/Model/SystemConfig.php');
require_once("get_menu_label.ini");
// 勤務シフト作成
$shift_menu_label = get_shift_menu_label($con, $fname);

//勤務シフト作成より呼び出し 20130218
if ($wherefrom == "10") {$wherefrom = "7";}

// タイトル
switch ($wherefrom) {
	case "2":  // タイムカード修正画面より
		$doc_title = "勤務管理";
		break;
	case "7":  // 勤務シフト作成より
		$doc_title = $shift_menu_label;
		break;
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <? echo($doc_title); ?> | タイムカード一括修正</title>
<?
require_once("about_comedix.php");
require_once("show_attendance_pattern.ini");
require_once("show_timecard_common.ini");
require_once("referer_common.ini");
require_once("holiday.php");
require_once("work_admin_menu_common.ini");
require_once("atdbk_common_class.php");
require_once("timecard_common_class.php");
require_once("atdbk_duty_shift_common_class.php");
require_once("date_utils.php");
require_once("atdbk_workflow_common_class.php");
require_once("timecard_bean.php");
require_once("duty_shift_common_class.php");
require_once("duty_shift_time_common_class.php");
require_once("work_admin_timecard_common.php");
require_once("timecard_paid_hol_hour_class.php");
require_once("atdbk_info_common.php");
require_once("ovtm_class.php");
require_once("timecard_tantou_class.php");
require_once("get_values.ini");
require_once("atdbk_close_class.php");


$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
switch ($wherefrom) {
	case "2":  // タイムカード修正画面より
		// 勤務管理権限チェック
		$checkauth = check_authority($session, 42, $fname);
		if ($checkauth == "0" && $mmode != "usr") {
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showLoginPage(window);</script>");
			exit;
		}
		break;
	case "7":  // 勤務シフト作成の実績入力画面より
		$checkauth = check_authority($session, 69, $fname);
		if ($checkauth == "0") {
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
			exit;
		}
		break;
	default:
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
		exit;
		break;
}

// リンク出力用変数の設定
$url_srch_name = urlencode($srch_name);

// 表示モードのデフォルトは「実績表示」
if ($view == "") {
	$view = "1";
}

//
// データベースに接続
$con = connect2db($fname);
$ovtm_class = new ovtm_class($con, $fname);

//前、次の職員対応 20110124
$err_flg = "";
if ($emp_move_flg != "") {

	//勤務シフト作成からの場合
	if ($wherefrom == "7") {
		$arr_staff_id = split(",", $staff_ids);
		$staff_idx = "";
		$data_cnt = count($arr_staff_id);
		for ($i=0; $i<$data_cnt; $i++) {
			if ($emp_id == $arr_staff_id[$i]) {
				$staff_idx = $i;
				break;
			}
		}
		if (($emp_move_flg == "prev" && $staff_idx == 0) || //先頭で前の職員
			($emp_move_flg == "next" && $staff_idx == $data_cnt - 1)) { //最後で次の職員
			$err_flg = "1";
		} else {
			$move_idx = ($emp_move_flg == "next") ? $staff_idx + 1 : $staff_idx - 1;
			$move_emp_id = $arr_staff_id[$move_idx];
		}
	} else {
		//担当所属取得
		if ($mmode == "usr") {
			$login_emp_id = get_emp_id($con, $session, $fname);
			$timecard_tantou_class = new timecard_tantou_class($con, $fname);
			$timecard_tantou_branches = $timecard_tantou_class->get_tantou_branches($login_emp_id);
		}
		$move_emp_id = get_move_emp_id($con, $fname, $emp_move_flg, $emp_personal_id, $srch_name, $cls, $atrb, $dept, $duty_form_jokin, $duty_form_hijokin, $group_id, $shift_group_id, $csv_layout_id, $srch_id, $sus_flg);
	}
	if ($move_emp_id == "") {
		$err_flg = "1";
	} else {
		$emp_id = $move_emp_id;
	}
}

//出勤表関連共通クラス
$atdbk_common_class = new atdbk_common_class($con, $fname);
//javascript出力
$atdbk_common_class->setJavascript();

//ワークフロー関連共通クラス
$atdbk_workflow_common_class = new atdbk_workflow_common_class($con, $fname);

//時間有休
$obj_hol_hour = new timecard_paid_hol_hour_class($con, $fname);
$obj_hol_hour->select();


// 遷移元の取得
$referer = get_referer($con, $session, "work", $fname);

//会議研修病棟外の列を常に表示とする 20120319
$meeting_display_flag = true;

// ログインユーザの出勤グループを取得
$tmcd_group_id = get_timecard_group_id($con, $emp_id, $fname);

// ************ oose add start *****************
// 勤務条件テーブルより勤務形態（常勤、非常勤）、残業管理を取得
$sql = "select duty_form, no_overtime from empcond";
$cond = "where emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$duty_form = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "duty_form") : "";
$no_overtime = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "no_overtime") : "";
// ************ oose add end *****************

//タイムカード情報の取得
$timecard_bean = new timecard_bean();
$timecard_bean->select($con, $fname);

$atdbk_close_class = new atdbk_close_class($con, $fname);
$closing = $atdbk_close_class->get_empcond_closing($emp_id);
$closing_month_flg = $atdbk_close_class->closing_month_flg;

// 締め日が登録済みの場合
if ($closing != "") {

	if ($yyyymm != "") {
		$year = substr($yyyymm, 0, 4);
		$month = intval(substr($yyyymm, 4, 2));
		$month_set_flg = true; //月が指定された場合
	} else {
		$year = date("Y");
		$month = date("n");
		$yyyymm = $year . date("m");
		$month_set_flg = false; //月が未指定の場合
	}

	$last_month_y = $year;
	$last_month_m = $month - 1;
	if ($last_month_m == 0) {
		$last_month_y--;
		$last_month_m = 12;
	}
	$last_yyyymm = $last_month_y . sprintf("%02d", $last_month_m);

	$next_month_y = $year;
	$next_month_m = $month + 1;
	if ($next_month_m == 13) {
		$next_month_y++;
		$next_month_m = 1;
	}
	$next_yyyymm = $next_month_y . sprintf("%02d", $next_month_m);

	// 開始日・締め日を算出
	$calced = false;
	switch ($closing) {
		case "1":  // 1日
			$closing_day = 1;
			break;
		case "2":  // 5日
			$closing_day = 5;
			break;
		case "3":  // 10日
			$closing_day = 10;
			break;
		case "4":  // 15日
			$closing_day = 15;
			break;
		case "5":  // 20日
			$closing_day = 20;
			break;
		case "6":  // 末日
			$start_year = $year;
			$start_month = $month;
			$start_day = 1;
			$end_year = $start_year;
			$end_month = $start_month;
			$end_day = days_in_month($end_year, $end_month);
			$calced = true;
			break;
	}
	if (!$calced) {
		$day = date("j");
		$start_day = $closing_day + 1;
		$end_day = $closing_day;

		//月が指定された場合
		if ($month_set_flg) {
			//当月〜翌月
			if ($closing_month_flg != "2") {
				$start_year = $year;
				$start_month = $month;

				$end_year = $year;
				$end_month = $month + 1;
				if ($end_month == 13) {
					$end_year++;
					$end_month = 1;
				}
			}
			//前月〜当月
			else {
				$start_year = $year;
				$start_month = $month - 1;
				if ($start_month == 0) {
					$start_year--;
					$start_month = 12;
				}

				$end_year = $year;
				$end_month = $month;
			}

		} else {
			if ($day <= $closing_day) {
				$start_year = $year;
				$start_month = $month - 1;
				if ($start_month == 0) {
					$start_year--;
					$start_month = 12;
				}

				$end_year = $year;
				$end_month = $month;
			} else {
				$start_year = $year;
				$start_month = $month;

				$end_year = $year;
				$end_month = $month + 1;
				if ($end_month == 13) {
					$end_year++;
					$end_month = 1;
				}
			}
		}
	}
	$start_date = sprintf("%04d%02d%02d", $start_year, $start_month, $start_day);
	$end_date = sprintf("%04d%02d%02d", $end_year, $end_month, $end_day);

	// 職員氏名を取得
	$sql = "select emp_lt_nm, emp_ft_nm, emp_personal_id from empmst";
	$cond = "where emp_id = '$emp_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$emp_name = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");
	$emp_personal_id = pg_fetch_result($sel, 0, "emp_personal_id");

	// 変則労働適用情報を配列で取得
	$arr_irrg_info = get_irrg_info_array($con, $emp_id, $fname);

	// 実績表示ボタン名
	$view_button = ($view == "1") ? "予実績表示" : "実績表示";

	// 対象年月の締め状況を取得
	//$tmp_yyyymm = substr($start_date, 0, 6);
	//月度対応 20100415
	$tmp_yyyymm = $year.sprintf("%02d", $month);
	$sql = "select count(*) from atdbkclose";
	$cond = "where emp_id = '$emp_id' and yyyymm = '$tmp_yyyymm'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$atdbk_closed = (pg_fetch_result($sel, 0, 0) > 0);

	// 締め済みの場合、締めデータから表示対象期間を取得
	if ($atdbk_closed) {
		$sql = "select min(date), max(date) from wktotald";
		$cond = "where emp_id = '$emp_id' and yyyymm = '$tmp_yyyymm'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$start_date = pg_fetch_result($sel, 0, 0);
		$end_date = pg_fetch_result($sel, 0, 1);

		$start_year = intval(substr($start_date, 0, 4));
		$start_month = intval(substr($start_date, 4, 2));
		$start_day = intval(substr($start_date, 6, 2));
		$end_year = intval(substr($end_date, 0, 4));
		$end_month = intval(substr($end_date, 4, 2));
		$end_day = intval(substr($end_date, 6, 2));
	}

	// 残業申請画面を開く時間を取得
	$sql = "select ovtmscr_tm from timecard";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0) {
		$ovtmscr_tm = pg_fetch_result($sel, 0, "ovtmscr_tm");
	} else {
		$ovtmscr_tm = 0;
	}

	// 給与支給区分・祝日計算方法を取得
	$sql = "select wage, hol_minus from empcond";
	$cond = "where emp_id = '$emp_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0) {
		$wage = pg_fetch_result($sel, 0, "wage");
		$hol_minus = pg_fetch_result($sel, 0, "hol_minus");
	} else {
		$wage = "";
		$hol_minus = "";
	}
}
//タイムカード集計項目出力設定からデータ取得 20091104
$arr_total_flg = get_timecard_total_flg($con, $fname); //show_timecard_common.ini
$arr_total_id = get_timecard_total_id($con, $fname);; //出力項目のid

//再計算時、画面のデータを設定
$set_data = array();
if ($recomp_flg == "1") {
	for ($i = 0; $i < count($date); $i++) {
		$tmp_date = $date[$i];

		$set_data[$tmp_date]["tmcd_group_id"] = $list_tmcd_group_id[$i];
		$set_data[$tmp_date]["pattern"] = $pattern[$i];
		$set_data[$tmp_date]["reason"] = $reason[$i];
		$set_data[$tmp_date]["night_duty"] = $night_duty[$i];
		$set_data[$tmp_date]["allow_id"] = $allow_ids[$i];
		$set_data[$tmp_date]["allow_count"] = $allow_counts[$i];

		$varname = "prev_day_flag_".$i; //previous_day_flagから変更 20100910
		$set_data[$tmp_date]["previous_day_flag"] = ($$varname == 1) ? 1 : 0;
		$set_data[$tmp_date]["start_time"] = ($start_hours[$i] != "") ? sprintf("%02d%02d", $start_hours[$i], $start_mins[$i]) : "";
		$varname = "next_day_flag_".$i;
		$set_data[$tmp_date]["next_day_flag"] = ($$varname == 1) ? 1 : 0;
		$set_data[$tmp_date]["end_time"] = ($end_hours[$i] != "") ? sprintf("%02d%02d", $end_hours[$i], $end_mins[$i]) : "";
		$set_data[$tmp_date]["out_time"] = ($out_hours[$i] != "") ? sprintf("%02d%02d", $out_hours[$i], $out_mins[$i]) : "";
		$set_data[$tmp_date]["ret_time"] = ($ret_hours[$i] != "") ? sprintf("%02d%02d", $ret_hours[$i], $ret_mins[$i]) : "";
		$varname = "o_s_next_day_flag_".$i;
		$set_data[$tmp_date]["over_start_next_day_flag"] = ($$varname == 1) ? 1 : 0;
		$set_data[$tmp_date]["over_start_time"] = ($over_start_hours[$i] != "") ? sprintf("%02d%02d", $over_start_hours[$i], $over_start_mins[$i]) : "";
		$varname = "o_e_next_day_flag_".$i;
		$set_data[$tmp_date]["over_end_next_day_flag"] = ($$varname == 1) ? 1 : 0;
		$set_data[$tmp_date]["over_end_time"] = ($over_end_hours[$i] != "") ? sprintf("%02d%02d", $over_end_hours[$i], $over_end_mins[$i]) : "";
		//残業時刻2追加 20110616
		$varname = "o_s_next_day_flag2_".$i;
		$set_data[$tmp_date]["over_start_next_day_flag2"] = ($$varname == 1) ? 1 : 0;
		$set_data[$tmp_date]["over_start_time2"] = ($over_start_hours2[$i] != "") ? sprintf("%02d%02d", $over_start_hours2[$i], $over_start_mins2[$i]) : "";
		$varname = "o_e_next_day_flag2_".$i;
		$set_data[$tmp_date]["over_end_next_day_flag2"] = ($$varname == 1) ? 1 : 0;
		$set_data[$tmp_date]["over_end_time2"] = ($over_end_hours2[$i] != "") ? sprintf("%02d%02d", $over_end_hours2[$i], $over_end_mins2[$i]) : "";

		$set_data[$tmp_date]["legal_in_over_time"] = ($legal_in_over_hours[$i] != "") ? sprintf("%02d%02d", $legal_in_over_hours[$i], $legal_in_over_mins[$i]) : "";

		$set_data[$tmp_date]["early_over_time"] = ($early_over_hours[$i] != "") ? sprintf("%02d%02d", $early_over_hours[$i], $early_over_mins[$i]) : "";

		$varname = "over_no_apply_flag_".$i;
		$set_data[$tmp_date]["over_no_apply_flag"] = ($$varname == 1) ? 1 : 0;
		$set_data[$tmp_date]["ovtm_reason_id"] = ($reason_ids[$i] == "other") ? "0" : $reason_ids[$i];

		$set_data[$tmp_date]["o_start_time1"] = ($o_start_hour1s[$i] != "") ? sprintf("%02d%02d", $o_start_hour1s[$i], $o_start_min1s[$i]) : "";
		$set_data[$tmp_date]["o_end_time1"] = ($o_end_hour1s[$i] != "") ? sprintf("%02d%02d", $o_end_hour1s[$i], $o_end_min1s[$i]) : "";
		$set_data[$tmp_date]["o_start_time2"] = ($o_start_hour2s[$i] != "") ? sprintf("%02d%02d", $o_start_hour2s[$i], $o_start_min2s[$i]) : "";
		$set_data[$tmp_date]["o_end_time2"] = ($o_end_hour2s[$i] != "") ? sprintf("%02d%02d", $o_end_hour2s[$i], $o_end_min2s[$i]) : "";
		$set_data[$tmp_date]["o_start_time3"] = ($o_start_hour3s[$i] != "") ? sprintf("%02d%02d", $o_start_hour3s[$i], $o_start_min3s[$i]) : "";
		$set_data[$tmp_date]["o_end_time3"] = ($o_end_hour3s[$i] != "") ? sprintf("%02d%02d", $o_end_hour3s[$i], $o_end_min3s[$i]) : "";
		$set_data[$tmp_date]["meeting_start_time"] = ($m_start_hours[$i] != "") ? sprintf("%02d%02d", $m_start_hours[$i], $m_start_mins[$i]) : "";
		$set_data[$tmp_date]["meeting_end_time"] = ($m_end_hours[$i] != "") ? sprintf("%02d%02d", $m_end_hours[$i], $m_end_mins[$i]) : "";
		$set_data[$tmp_date]["ovtm_status"] = $ovtm_status[$i];
		//休憩時刻追加 20100921
		$set_data[$tmp_date]["rest_start_time"] = ($rest_start_hours[$i] != "") ? sprintf("%02d%02d", $rest_start_hours[$i], $rest_start_mins[$i]) : "";
		$set_data[$tmp_date]["rest_end_time"] = ($rest_end_hours[$i] != "") ? sprintf("%02d%02d", $rest_end_hours[$i], $rest_end_mins[$i]) : "";
	}
}

?>
<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript">
<!--
function changeView() {
<? //締め済の場合は画面データの設定不要
if (!$atdbk_closed) { ?>
	document.timecard.recomp_flg.value = '1';
<? } ?>
	document.timecard.view.value = (document.timecard.view.value == '1') ? '2' : '1';
	document.timecard.action = 'work_admin_timecard_all.php';
	document.timecard.submit();
}

function editTimecard() {
	document.timecard.action = 'atdbk_timecard_all_insert.php';
	document.timecard.submit();
}

function openPrintPage() {
	window.open('atdbk_timecard_print.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&yyyymm=<? echo("$yyyymm"); ?>&view=<? echo($view); ?>&wherefrom=2', 'newwin', 'width=840,height=700,scrollbars=yes');
}

function downloadCSV(type) {
	document.csv.action = 'atdbk_timecard_csv' + type + '.php';
	document.csv.submit();
}

//再計算
function recomp() {
	document.timecard.recomp_flg.value = '1';
	document.timecard.action = 'work_admin_timecard_all.php';
	document.timecard.submit();
}

//残業申請不要チェック時、残業時刻をクリアする
function over_no_apply(pos) {
	var flg_name = 'over_no_apply_flag_'+pos;
	var flg = document.timecard[flg_name].checked;

	if (flg) {
		flg_name = 'o_s_next_day_flag_'+pos;
		document.timecard[flg_name].checked = false;
		flg_name = 'o_e_next_day_flag_'+pos;
		document.timecard[flg_name].checked = false;
		document.timecard["over_start_hours[]"][pos].value = '';
		document.timecard["over_start_mins[]"][pos].value = '';
		document.timecard["over_end_hours[]"][pos].value = '';
		document.timecard["over_end_mins[]"][pos].value = '';
	}

}

var arr_atdptn_reason = new Array();
<?
//事由情報を出力
$arr_group_pattern_reason = $atdbk_common_class->get_pattern_reason_array("");
foreach ($arr_group_pattern_reason as $wk_tmcd_group_id => $arr_pattern_reason) {
	foreach ($arr_pattern_reason as $wk_atdptn_id => $reason) {
		if ($reason != "") {
			echo("arr_atdptn_reason['{$wk_tmcd_group_id}_{$wk_atdptn_id}'] = '$reason';\n");
		}
	}
}
?>
function setReason(pos, tmcd_group_id, atdptn_id) {

	if (atdptn_id == '--') {
		document.timecard["reason[]"][pos].value = '--';
		return;
	}
	wk_group_id = document.timecard["list_tmcd_group_id[]"][pos].value;
	wk_id = wk_group_id+'_'+atdptn_id;
	if (arr_atdptn_reason[wk_id] == undefined) {
		document.timecard["reason[]"][pos].value = '--';
		return;
	}
	document.timecard["reason[]"][pos].value = arr_atdptn_reason[wk_id];
}

///-----------------------------------------------------------------------------
//予定コピー
///-----------------------------------------------------------------------------
function planCopy() {
	if (confirm('「予定コピー」します。よろしいですか？')) {
		document.timecard.recomp_flg.value = '1';
		document.timecard.plan_copy_flg.value = "1";
		document.timecard.action="work_admin_timecard_all.php";
		document.timecard.submit();
	}
}

//前の職員、次の職員
function emp_move(move_flg) {
	document.timecard.emp_move_flg.value = move_flg;
<?
if ($wherefrom == "7") {
	//勤務シフトからの場合
?>
	//職員情報が未設定の場合に呼出元画面から取得
	if (document.timecard.staff_ids.value == '') {
		if(window.opener && !window.opener.closed && window.opener.document.mainform.data_cnt) {
			data_cnt = window.opener.document.mainform.data_cnt.value;
			staff_ids = '';
			for (i=0; i<data_cnt; i++) {
				wk1 = 'staff_id[' + i + ']';
				staff_id = window.opener.document.mainform.elements[wk1].value;
				if (i > 0) {
					staff_ids += ',';
				}
				staff_ids += staff_id;
			}

			document.timecard.staff_ids.value = staff_ids;
			document.timecard.data_cnt.value = data_cnt;
		} else {
			alert('職員情報を取得できません。もう一度開き直してください。');
			return false;
		}
	}
	<?
}
?>
	document.timecard.submit();

}
//チェック
function inputcheck() {
	document.timecard.check_flg.value = '1';
	document.timecard.submit();

}

var scroll_pos = 0;
//横スクロール
function scroll_region(flg) {

	var wk_rows = document.getElementById('tbl2').rows;
	var wk_cells = wk_rows[0].cells;
	//移動位置を決定するため列幅の計を設定
	var arr_width = new Array(6);
	var wk_width = 0;

<?
$pos1 = 2;
$pos2 = 4;
$pos3 = 8;
$pos4 = 11;
$pos5 = 14;
//法定内残業を表示しない設定の場合
if ($timecard_bean->all_legal_over_disp_flg == "f"){

	$pos2--;
	$pos3--;
	$pos4--;
	$pos5--;
}
//申請状態を表示しない設定の場合
if ($timecard_bean->all_apply_disp_flg == "f") {

	$pos2--;
	$pos3--;
	$pos4--;
	$pos5--;
}
?>

	arr_width[0] = 0; //残業１
	for (var i=0; i<wk_cells.length; i++) {
		wk_width += wk_cells[i].offsetWidth;
		if (i==<? echo($pos1); ?>) { //法定内残
			arr_width[1] = wk_width;
		} else if (i==<? echo($pos2); ?>) { //外出
			arr_width[2] = wk_width;
		} else if (i==<? echo($pos3); ?>) { //呼出
			arr_width[3] = wk_width;
		} else if (i==<? echo($pos4); ?>) { //当直
			arr_width[4] = wk_width;
		} else if (i==<? echo($pos5); ?>) { //病棟外
			arr_width[5] = wk_width;
		}
	}

	if (flg == 1) { //左
		scroll_pos--;
<?
//法定内残と申請状態の両方を表示しない場合で位置が2の時次へ
if ($timecard_bean->all_legal_over_disp_flg == "f" &&
		$timecard_bean->all_apply_disp_flg == "f"){
			?>
			if (scroll_pos == 2) {
				scroll_pos--;
			}
			<?
}
		?>
	} else { //右
		scroll_pos++;
<?
//法定内残と申請状態の両方を表示しない場合で位置が2の時次へ
if ($timecard_bean->all_legal_over_disp_flg == "f" &&
		$timecard_bean->all_apply_disp_flg == "f"){
			?>
			if (scroll_pos == 2) {
				scroll_pos++;
			}
	<?
}
		?>
	}
<?
//最大位置の設定
//呼出を表示しない場合の対応
$max_pos = ($timecard_bean->ret_display_flag == "t") ? 4 : 3;
	?>
	if (scroll_pos > <? echo($max_pos); ?>) {
		scroll_pos = 0;
	}
	if (scroll_pos < 0) {
		scroll_pos = <? echo($max_pos); ?>;
	}

	document.getElementById('region').scrollLeft = arr_width[scroll_pos];

}
//-->
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<?
$wk_color_tm1 = "#b8d175";
$wk_color_tm2 = "#f99570";
//枠用
// tl: top left ┏
// tc: top center ┯┯
// tr: top right ┓
// ml: middle left  ┃｜
// mc: middle center ｜｜
// mr: middle right ｜┃
// bl: bottom left ┗
// bc: bottom center ┷┷
// br: bottom right ┛
// 1:出勤、退勤時刻 2:残業時刻
?>
<style type="text/css">
td.lbl {padding-right:1em;}
td.txt {padding-right:0.5em;}
td.tm {width:2.8em;text-align:right;}
td.tl1 {border-style:solid; border-top-color:<? echo($wk_color_tm1); ?>; border-right-color:<? echo($wk_color_tm1); ?>; border-left-color:<? echo($wk_color_tm1); ?>; border-width:3px 2px 1px 3px;}
td.tc1 {border-style:solid; border-top-color:<? echo($wk_color_tm1); ?>; border-right-color:<? echo($wk_color_tm1); ?>; border-left-color:<? echo($wk_color_tm1); ?>; border-width:3px 2px 1px 1px;}
td.tr1 {border-style:solid; border-top-color:<? echo($wk_color_tm1); ?>; border-right-color:<? echo($wk_color_tm1); ?>; border-left-color:<? echo($wk_color_tm1); ?>; border-width:3px 3px 1px 1px;}
td.ml1 {border-style:solid; border-right-color:<? echo($wk_color_tm1); ?>; border-left-color:<? echo($wk_color_tm1); ?>; border-width:1px 2px 1px 3px;}
td.mc1 {border-style:solid; border-right-color:<? echo($wk_color_tm1); ?>; border-left-color:<? echo($wk_color_tm1); ?>; border-width:1px 2px 1px 1px;}
td.mr1 {border-style:solid; border-right-color:<? echo($wk_color_tm1); ?>; border-left-color:<? echo($wk_color_tm1); ?>; border-width:1px 3px 1px 1px;}
td.bl1 {border-style:solid; border-right-color:<? echo($wk_color_tm1); ?>; border-bottom-color:<? echo($wk_color_tm1); ?>; border-left-color:<? echo($wk_color_tm1); ?>; border-width:1px 2px 3px 3px;}
td.bc1 {border-style:solid; border-right-color:<? echo($wk_color_tm1); ?>; border-bottom-color:<? echo($wk_color_tm1); ?>; border-left-color:<? echo($wk_color_tm1); ?>; border-width:1px 2px 3px 1px;}
td.br1 {border-style:solid; border-right-color:<? echo($wk_color_tm1); ?>; border-bottom-color:<? echo($wk_color_tm1); ?>; border-left-color:<? echo($wk_color_tm1); ?>; border-width:1px 3px 3px 1px;}

td.tl2 {border-style:solid; border-color:<? echo($wk_color_tm2); ?>; border-width:3px 2px 2px 3px;}
td.tc2 {border-style:solid; border-color:<? echo($wk_color_tm2); ?>; border-width:3px 2px 2px 1px;}
td.tr2 {border-style:solid; border-color:<? echo($wk_color_tm2); ?>; border-width:3px 3px 2px 1px;}
td.ml2 {border-style:solid; border-right-color:<? echo($wk_color_tm2); ?>; border-left-color:<? echo($wk_color_tm2); ?>; border-width:1px 2px 1px 3px;}
td.mc2 {border-style:solid; border-right-color:<? echo($wk_color_tm2); ?>; border-left-color:<? echo($wk_color_tm2); ?>; border-width:1px 2px 1px 1px;}
td.mr2 {border-style:solid; border-right-color:<? echo($wk_color_tm2); ?>; border-left-color:<? echo($wk_color_tm2); ?>; border-width:1px 3px 1px 1px;}
td.bl2 {border-style:solid; border-right-color:<? echo($wk_color_tm2); ?>; border-bottom-color:<? echo($wk_color_tm2); ?>; border-left-color:<? echo($wk_color_tm2); ?>; border-width:1px 2px 3px 3px;}
td.bc2 {border-style:solid; border-right-color:<? echo($wk_color_tm2); ?>; border-bottom-color:<? echo($wk_color_tm2); ?>; border-left-color:<? echo($wk_color_tm2); ?>; border-width:1px 2px 3px 1px;}
td.br2 {border-style:solid; border-right-color:<? echo($wk_color_tm2); ?>; border-bottom-color:<? echo($wk_color_tm2); ?>; border-left-color:<? echo($wk_color_tm2); ?>; border-width:1px 3px 3px 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<? if ($closing == "") {  // 締め日未登録の場合 ?>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイムカード締め日が登録されていません。出退勤マスターメンテナンスの締め日画面で登録してください。</font></td>
</tr>
</table>
<? } else if ($wage == "") {  // 給与支給区分未登録の場合 ?>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">給与支給区分が登録されていません。職員登録の勤務条件画面で登録してください。</font></td>
</tr>
</table>
<? } else { ?>
<form name="timecard" method="post">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
	<? /*
	<table width="100%" border="0" cellspacing="0" cellpadding="1">
	<tr>
	<td height="22" width="260"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="work_admin_timecard.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&yyyymm=<? echo($yyyymm); ?>&view=<? echo($view); ?>&srch_name=<? echo($url_srch_name); ?>&cls=<? echo($cls); ?>&atrb=<? echo($atrb); ?>&dept=<? echo($dept); ?>&page=<? echo($page); ?>">日別修正</a>&nbsp;
	<b>一括修正</b>
	</font></td>
	</tr>
	</table>
*/ ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#5279a5">
<td width="100%" height="28" class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>一括修正</b></font></td>
<td width="28" bgcolor="#5279a5" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>

<?
//勤務シフト実績入力からの場合
	if ($wherefrom == "7") {
?>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
		<?
			//出勤簿を追加 20130129
        $url = "work_admin_timecard.php?session=$session&emp_id=$emp_id&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&page=$page&group_id=$group_id&shift_group_id=$shift_group_id&csv_layout_id=$csv_layout_id&yyyymm=$yyyymm&wherefrom=$wherefrom&staff_ids=$staff_ids&duty_form_jokin=$duty_form_jokin&duty_form_hijokin=$duty_form_hijokin&srch_id=$srch_id&sus_flg=$sus_flg";
        $url_shift = "work_admin_timecard_shift.php?session=$session&emp_id=$emp_id&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&page=$page&group_id=$group_id&shift_group_id=$shift_group_id&csv_layout_id=$csv_layout_id&yyyymm=$yyyymm&wherefrom=$wherefrom&staff_ids=$staff_ids&duty_form_jokin=$duty_form_jokin&duty_form_hijokin=$duty_form_hijokin&srch_id=$srch_id&sus_flg=$sus_flg";
?>
<td height="22" width="280"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<a href="<? echo($url_shift); ?>">出勤簿</a>&nbsp;
<a href="<? echo($url); ?>">日別修正</a>&nbsp;
		<?
        $url = "work_admin_timecard_a4.php?session=$session&emp_id=$emp_id&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&page=$page&group_id=$group_id&shift_group_id=$shift_group_id&csv_layout_id=$csv_layout_id&yyyymm=$yyyymm&wherefrom=$wherefrom&staff_ids=$staff_ids&duty_form_jokin=$duty_form_jokin&duty_form_hijokin=$duty_form_hijokin&srch_id=$srch_id&sus_flg=$sus_flg";
 ?>
<a href="<? echo($url); ?>">日別修正（A4横）</a>&nbsp;
	<b>一括修正</b>
<?php
    // opt以下のphpを表示するかどうか(申請用追加機能有無判定)
    $opt_display_flag = file_exists("opt/flag");
    if ( phpversion() >= "5.1"  && $opt_display_flag){
        require_once './kintai/common/master_util.php';
		if(MasterUtil::get_kintai_ptn() != 0){
                $url = "kintai_ovtmlist_adm.php?session=$session&emp_id=$emp_id&yyyymm=$yyyymm&view=$view&wherefrom=$wherefrom&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&page=$page&group_id=$group_id&shift_group_id=$shift_group_id&csv_layout_id=$csv_layout_id&check_flg=&staff_ids=$staff_ids&duty_form_jokin=$duty_form_jokin&duty_form_hijokin=$duty_form_hijokin&srch_id=$srch_id&duty_form_jokin=$duty_form_jokin&duty_form_hijokin=$duty_form_hijokin&srch_id=$srch_id&sus_flg=$sus_flg";
            echo "<a href='".$url."'>出勤簿（超勤区分）</a>";
                $url = "kintai_ovtmlist_detail_adm.php?session=$session&emp_id=$emp_id&yyyymm=$yyyymm&view=$view&wherefrom=$wherefrom&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&page=$page&group_id=$group_id&shift_group_id=$shift_group_id&csv_layout_id=$csv_layout_id&check_flg=$check_flg&staff_ids=$staff_ids&duty_form_jokin=$duty_form_jokin&duty_form_hijokin=$duty_form_hijokin&srch_id=$srch_id&sus_flg=$sus_flg";
            echo "&nbsp;&nbsp;<a href='".$url."'>超勤区分内訳</a>";
		}
	}
?>
</font></td>
</tr>
</table>
<? } ?>

<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
    <td height="22" width="560"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">対象期間&nbsp;&nbsp;<a href="work_admin_timecard_all.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&yyyymm=<? echo($last_yyyymm); ?>&view=<? echo($view); ?>&wherefrom=<? echo($wherefrom); ?>&group_id=<? echo($group_id); ?>&duty_yyyy=<? echo($duty_yyyy); ?>&duty_mm=<? echo($duty_mm); ?>&staff_ids=<? echo($staff_ids); ?>&srch_name=<? echo($url_srch_name); ?>&cls=<? echo($cls); ?>&atrb=<? echo($atrb); ?>&dept=<? echo($dept); ?>&shift_group_id=<? echo($shift_group_id); ?>&csv_layout_id=<? echo($csv_layout_id); ?>&duty_form_jokin=<? echo($duty_form_jokin); ?>&duty_form_hijokin=<? echo($duty_form_hijokin); ?>&srch_id=<? echo($srch_id); ?>&sus_flg=<? echo($sus_flg); ?>&mmode=<? echo($mmode); ?>">&lt;前月</a>&nbsp;<?
    $str = show_yyyymm_menu($year, $month);
    echo $str;
?>
&nbsp;<a href="work_admin_timecard_all.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&yyyymm=<? echo($next_yyyymm); ?>&view=<? echo($view); ?>&wherefrom=<? echo($wherefrom); ?>&group_id=<? echo($group_id); ?>&duty_yyyy=<? echo($duty_yyyy); ?>&duty_mm=<? echo($duty_mm); ?>&staff_ids=<? echo($staff_ids); ?>&srch_name=<? echo($url_srch_name); ?>&cls=<? echo($cls); ?>&atrb=<? echo($atrb); ?>&dept=<? echo($dept); ?>&shift_group_id=<? echo($shift_group_id); ?>&csv_layout_id=<? echo($csv_layout_id); ?>&duty_form_jokin=<? echo($duty_form_jokin); ?>&duty_form_hijokin=<? echo($duty_form_hijokin); ?>&srch_id=<? echo($srch_id); ?>&sus_flg=<? echo($sus_flg); ?>&mmode=<? echo($mmode); ?>">翌月&gt;</a></font>
&nbsp;
<input type="button" value="<前の職員" onclick="emp_move('prev');">
<input type="button" value="次の職員>" onclick="emp_move('next');">
&nbsp;
<input type="button" value="チェック" onclick="inputcheck();">

</td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">開始日&nbsp;&nbsp;<? echo("{$start_month}月{$start_day}日"); ?>&nbsp;&nbsp;〜&nbsp;&nbsp;締め日&nbsp;&nbsp;<? echo("{$end_month}月{$end_day}日"); ?></font></td>
</tr>
</table>
</td>
</tr>
<tr>
<td>
<table width="1000" border="0" cellspacing="0" cellpadding="1">
<tr>
<td height="22" width=""><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員ID&nbsp;
	<? echo($emp_personal_id); ?>&nbsp;&nbsp;職員氏名&nbsp;<? echo($emp_name);
	// 勤務条件表示
	$str_empcond = get_employee_empcond2($con, $fname, $emp_id);

	$str_shiftname = get_shiftname($con, $fname, $atdbk_closed, $emp_id, $start_date, $end_date);
	//所属表示
	$str_orgname = get_orgname($con, $fname, $emp_id, $timecard_bean);
	echo("　<b>（".$str_empcond."　".$str_shiftname."）$str_orgname</b>");
?></font></td>
<td align="right" width="">
<? if ($wherefrom == "7") { ?>
<input type="button" value="予定コピー" onclick="planCopy();"<? if ($atdbk_closed) {echo(" disabled");} ?>>&nbsp;
<? } ?>
<input type="button" value="<? echo($view_button); ?>" onclick="changeView();">&nbsp;
	<? /*
	<input type="button" value="EXCEL出力" onclick="downloadCSV(1);"<? if ($view == "2") {echo(" disabled");} ?> disabled>&nbsp;
	*/ ?>
<input type="button" value="再計算" onclick="recomp();"<? if ($atdbk_closed) {echo(" disabled");} ?>>&nbsp;
<input type="button" value="登録" onclick="editTimecard();"<? if ($atdbk_closed) {echo(" disabled");} ?>>&nbsp;

</td>
</tr>
</table>
</td>
</tr>
<tr>
<td>


<?
//テーブル用の変数に設定してから表示する
	$tbl1 = ""; //左側テーブル、日付〜退勤
	$tbl2 = ""; //中央テーブル、残業〜病棟外
	$tbl3 = ""; //右側テーブル、日付〜最終更新者

	//上側見出し
	$tbl1_hdr_t = "";
	$tbl1_hdr = "";
	//下側見出し
	$tbl1_hdr_b = "";

	$tbl1 .= "<table width=\"\" border=\"1\" cellspacing=\"0\" cellpadding=\"1\">\n";
	$tbl1_hdr_t .= "<tr height=\"46\">\n";
	$tbl1_hdr_b .= "<tr height=\"38\">\n";
	$tbl1_hdr .= "<td width=\"\" align=\"center\" rowspan=\"1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">日付</font></td>\n";
	$tbl1_hdr .= "<td width=\"\" align=\"center\" rowspan=\"1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">曜日</font></td>\n";
	if ($timecard_bean->type_display_flag == "t"){
		$tbl1_hdr .= "<td width=\"\" align=\"center\" rowspan=\"1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">種別</font></td>\n";
	}

	if ($timecard_bean->group_display_flag == "t"){
		$tbl1_hdr .= "<td width=\"\" align=\"center\" rowspan=\"1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">グループ</font></td>\n";
	}
	$tbl1_hdr .= "<td width=\"\" align=\"center\" rowspan=\"1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">勤務実績</font></td>\n";
	$tbl1_hdr .= "<td width=\"\" align=\"center\" rowspan=\"1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">事由</font></td>\n";

	$tbl1_hdr_b .= $tbl1_hdr;

	if ($timecard_bean->zen_display_flag == "t"){
		$tbl1_hdr .= "<td width=\"\" align=\"center\" rowspan=\"1\" class=\"tl1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">前</font></td>\n";
		$tbl1_hdr_b .= "<td width=\"\" align=\"center\" rowspan=\"1\" class=\"bl1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">前</font></td>\n";
		$wk_class = "tc1";
		$wk_class_b = "bc1";
	} else {
		//前の列がない場合、出勤時刻の列の左を太くする
		//$wk_left_width = "style=\"border-style:solid; border-left-width:3px\"";
		$wk_class = "tl1";
		$wk_class_b = "bl1";
	}
	$tbl1_hdr .= "<td width=\"\" align=\"center\" rowspan=\"1\" class=\"$wk_class\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">出勤</font></td>\n";
	$tbl1_hdr_b .= "<td width=\"\" align=\"center\" rowspan=\"1\" class=\"$wk_class_b\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">出勤</font></td>\n";
	if ($timecard_bean->yoku_display_flag == "t"){
		$tbl1_hdr .= "<td width=\"\" align=\"center\" rowspan=\"1\" class=\"tc1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">翌</font></td>\n";
		$tbl1_hdr_b .= "<td width=\"\" align=\"center\" rowspan=\"1\" class=\"bc1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">翌</font></td>\n";
	}
	$tbl1_hdr .= "<td width=\"\" align=\"center\" rowspan=\"1\" class=\"tr1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">退勤</font></td>\n";
	$tbl1_hdr_b .= "<td width=\"\" align=\"center\" rowspan=\"1\" class=\"br1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">退勤</font></td>\n";
	$tbl1_hdr .= "</tr>\n";
	$tbl1_hdr_t .= $tbl1_hdr;
	$tbl1 .= $tbl1_hdr_t;

	$tbl1_hdr_b .= "</tr>\n";


	$tbl2 .= "<table id=\"tbl2\" border=\"1\" cellspacing=\"0\" cellpadding=\"1\">\n";
	$tbl2 .= "<tr height=\"22\" bgcolor=\"#c2ceec\">\n";
	$tbl2 .= "<td width=\"\" align=\"center\" rowspan=\"1\" colspan=\"4\" class=\"tl2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">残業時刻１</font></td>\n";
	$tbl2 .= "<td width=\"\" align=\"center\" rowspan=\"1\" colspan=\"4\" class=\"tr2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">残業時刻２</font></td>\n";
	$tbl2 .= "<td width=\"\" align=\"center\" rowspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">理由</font></td>\n";
	if ($timecard_bean->all_legal_over_disp_flg != "f"){
		$tbl2 .= "<td width=\"\" align=\"center\" rowspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">法定内<br>残業</font></td>\n";
	}
	//残業申請状況の表示
	if ($timecard_bean->all_apply_disp_flg != "f") {
		$tbl2 .= "<td width=\"\" align=\"center\" rowspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">申請状態</font></td>\n";
	}
	$tbl2 .= "<td width=\"\" align=\"center\" rowspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">外出</font></td>\n";
	$tbl2 .= "<td width=\"\" align=\"center\" rowspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">戻り</font></td>\n";
	//休憩時刻追加 20100921
	$tbl2 .= "<td width=\"\" align=\"center\" rowspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">休憩開始</font></td>\n";
	$tbl2 .= "<td width=\"\" align=\"center\" rowspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">休憩終了</font></td>\n";
	if ($timecard_bean->ret_display_flag == "t"){
		$tbl2 .= "<td width=\"\" align=\"center\" rowspan=\"1\" colspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">呼出・退復１</font></td>\n";
		$tbl2 .= "<td width=\"\" align=\"center\" rowspan=\"1\" colspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">呼出・退復２</font></td>\n";
		$tbl2 .= "<td width=\"\" align=\"center\" rowspan=\"1\" colspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">呼出・退復３</font></td>\n";
	}
	//当直の表示
	if ($timecard_bean->all_duty_disp_flg != "f") {
		$tbl2 .= "<td width=\"\" align=\"center\" rowspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$atdbk_common_class->duty_or_oncall_str}</font></td>\n";
	}
	$tbl2 .= "<td width=\"\" align=\"center\" rowspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">手当</font></td>\n";
	$tbl2 .= "<td width=\"\" align=\"center\" rowspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">回数</font></td>\n";
	if($meeting_display_flag){
		$tbl2 .= "<td width=\"\" align=\"center\" rowspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">病棟外<br>開始</font></td>\n";
		$tbl2 .= "<td width=\"\" align=\"center\" rowspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">病棟外<br>終了</font></td>\n";
	}
	$tbl2 .= "</tr>\n";

	//2段目
	$tbl2 .= "<tr height=\"22\" bgcolor=\"#c2ceec\">\n";
	$tbl2 .= "<td width=\"\" align=\"center\" rowspan=\"1\" class=\"ml2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">翌</font></td>\n";
	$tbl2 .= "<td width=\"\" align=\"center\" rowspan=\"1\" class=\"mc2\" nowrap><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">残業開始</font></td>\n";
	$tbl2 .= "<td width=\"\" align=\"center\" rowspan=\"1\" class=\"mc2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">翌</font></td>\n";
	$tbl2 .= "<td width=\"\" align=\"center\" rowspan=\"1\" class=\"mc2\" nowrap><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">残業終了</font></td>\n";
	$tbl2 .= "<td width=\"\" align=\"center\" rowspan=\"1\" class=\"mc2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">翌</font></td>\n";
	$tbl2 .= "<td width=\"\" align=\"center\" rowspan=\"1\" class=\"mc2\" nowrap><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">残業開始</font></td>\n";
	$tbl2 .= "<td width=\"\" align=\"center\" rowspan=\"1\" class=\"mc2\" nowrap><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">翌</font></td>\n";
	$tbl2 .= "<td width=\"\" align=\"center\" rowspan=\"1\" class=\"mr2\" nowrap><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">残業終了</font></td>\n";
	if ($timecard_bean->ret_display_flag == "t"){
		$ret1_str = ($timecard_bean->return_icon_flg != "2") ? "退勤復帰" : "呼出出勤";
		$ret2_str = ($timecard_bean->return_icon_flg != "2") ? "復帰退勤" : "呼出退勤";
		$tbl2 .= "<td width=\"\" align=\"center\" rowspan=\"1\" nowrap><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$ret1_str</font></td>\n";
		$tbl2 .= "<td width=\"\" align=\"center\" rowspan=\"1\" nowrap><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$ret2_str</font></td>\n";
		$tbl2 .= "<td width=\"\" align=\"center\" rowspan=\"1\" nowrap><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$ret1_str</font></td>\n";
		$tbl2 .= "<td width=\"\" align=\"center\" rowspan=\"1\" nowrap><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$ret2_str</font></td>\n";
		$tbl2 .= "<td width=\"\" align=\"center\" rowspan=\"1\" nowrap><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$ret1_str</font></td>\n";
		$tbl2 .= "<td width=\"\" align=\"center\" rowspan=\"1\" nowrap><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$ret2_str</font></td>\n";
	}
	$tbl2 .= "</tr>\n";

	//下側見出し
	$tbl2_hdr_b = "<tr height=\"38\" bgcolor=\"#c2ceec\">\n";
	$tbl2_hdr_b .= "<td width=\"\" align=\"center\" rowspan=\"1\" class=\"bl2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">翌</font></td>\n";
	$tbl2_hdr_b .= "<td width=\"\" align=\"center\" rowspan=\"1\" class=\"bc2\" nowrap><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">残業開始</font></td>\n";
	$tbl2_hdr_b .= "<td width=\"\" align=\"center\" rowspan=\"1\" class=\"bc2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">翌</font></td>\n";
	$tbl2_hdr_b .= "<td width=\"\" align=\"center\" rowspan=\"1\" class=\"bc2\" nowrap><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">残業終了</font></td>\n";
	$tbl2_hdr_b .= "<td width=\"\" align=\"center\" rowspan=\"1\" class=\"bc2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">翌</font></td>\n";
	$tbl2_hdr_b .= "<td width=\"\" align=\"center\" rowspan=\"1\" class=\"bc2\" nowrap><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">残業開始</font></td>\n";
	$tbl2_hdr_b .= "<td width=\"\" align=\"center\" rowspan=\"1\" class=\"bc2\" nowrap><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">翌</font></td>\n";
	$tbl2_hdr_b .= "<td width=\"\" align=\"center\" rowspan=\"1\" class=\"br2\" nowrap><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">残業終了</font></td>\n";
	$tbl2_hdr_b .= "<td width=\"\" align=\"center\" rowspan=\"1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">理由</font></td>\n";
	if ($timecard_bean->all_legal_over_disp_flg != "f"){
		$tbl2_hdr_b .= "<td width=\"\" align=\"center\" rowspan=\"1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">法定内<br>残業</font></td>\n";
	}
	//残業申請状況の表示
	if ($timecard_bean->all_apply_disp_flg != "f") {
		$tbl2_hdr_b .= "<td width=\"\" align=\"center\" rowspan=\"1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">申請状態</font></td>\n";
	}
	$tbl2_hdr_b .= "<td width=\"\" align=\"center\" rowspan=\"1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">外出</font></td>\n";
	$tbl2_hdr_b .= "<td width=\"\" align=\"center\" rowspan=\"1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">戻り</font></td>\n";
	//休憩時刻追加 20100921
	$tbl2_hdr_b .= "<td width=\"\" align=\"center\" rowspan=\"1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">休憩開始</font></td>\n";
	$tbl2_hdr_b .= "<td width=\"\" align=\"center\" rowspan=\"1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">休憩終了</font></td>\n";
	if ($timecard_bean->ret_display_flag == "t"){
		$ret1_str = ($timecard_bean->return_icon_flg != "2") ? "退勤復帰" : "呼出出勤";
		$ret2_str = ($timecard_bean->return_icon_flg != "2") ? "復帰退勤" : "呼出退勤";
		$tbl2_hdr_b .= "<td width=\"\" align=\"center\" rowspan=\"1\" nowrap><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$ret1_str</font></td>\n";
		$tbl2_hdr_b .= "<td width=\"\" align=\"center\" rowspan=\"1\" nowrap><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$ret2_str</font></td>\n";
		$tbl2_hdr_b .= "<td width=\"\" align=\"center\" rowspan=\"1\" nowrap><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$ret1_str</font></td>\n";
		$tbl2_hdr_b .= "<td width=\"\" align=\"center\" rowspan=\"1\" nowrap><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$ret2_str</font></td>\n";
		$tbl2_hdr_b .= "<td width=\"\" align=\"center\" rowspan=\"1\" nowrap><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$ret1_str</font></td>\n";
		$tbl2_hdr_b .= "<td width=\"\" align=\"center\" rowspan=\"1\" nowrap><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$ret2_str</font></td>\n";
	}
	//当直の表示
	if ($timecard_bean->all_duty_disp_flg != "f") {
		$tbl2_hdr_b .= "<td width=\"\" align=\"center\" rowspan=\"1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$atdbk_common_class->duty_or_oncall_str}</font></td>\n";
	}
	$tbl2_hdr_b .= "<td width=\"\" align=\"center\" rowspan=\"1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">手当</font></td>\n";
	$tbl2_hdr_b .= "<td width=\"\" align=\"center\" rowspan=\"1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">回数</font></td>\n";
	if($meeting_display_flag){
		$tbl2_hdr_b .= "<td width=\"\" align=\"center\" rowspan=\"1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">病棟外<br>開始</font></td>\n";
		$tbl2_hdr_b .= "<td width=\"\" align=\"center\" rowspan=\"1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">病棟外<br>終了</font></td>\n";
	}
	$tbl2_hdr_b .= "</tr>\n";

	$tbl3_hdr = "";

	$tbl3 .= "<table width=\"960\" border=\"1\" cellspacing=\"0\" cellpadding=\"1\">\n";
	$tbl3 .= "<tr height=\"46\">\n";
	//日付曜日追加 20100907
	$tbl3_hdr .= "<td width=\"\" align=\"center\" rowspan=\"1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">日付</font></td>\n";
	$tbl3_hdr .= "<td width=\"\" align=\"center\" rowspan=\"1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">曜日</font></td>\n";
	$tbl3_hdr .= "<td width=\"\" align=\"center\" rowspan=\"1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">勤務</font></td>\n";
	 //遅刻早退追加 20100907
	$tbl3_hdr .= "<td width=\"\" align=\"center\" rowspan=\"1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">遅刻</font></td>\n";
	$tbl3_hdr .= "<td width=\"\" align=\"center\" rowspan=\"1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">早退</font></td>\n";
	$tbl3_hdr .= "<td width=\"\" align=\"center\" rowspan=\"1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">残業</font></td>\n";
	if ($timecard_bean->all_legal_over_disp_flg != "f"){
		$tbl3_hdr .= "<td width=\"\" align=\"center\" rowspan=\"1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">法内<br>残</font></td>\n";
		$tbl3_hdr .= "<td width=\"\" align=\"center\" rowspan=\"1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">法外<br>残</font></td>\n";
	}
	//深勤を深残へ変更 20110126
	$tbl3_hdr .= "<td width=\"\" align=\"center\" rowspan=\"1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">深残</font></td>\n";
	$tbl3_hdr .= "<td width=\"\" align=\"center\" rowspan=\"1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">早出<br>残</font></td>\n";
	$tbl3_hdr .= "<td width=\"\" align=\"center\" rowspan=\"1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">外出</font></td>\n";
	$tbl3_hdr .= "<td width=\"\" align=\"center\" rowspan=\"1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">休憩</font></td>\n";
	if ($timecard_bean->ret_display_flag == "t"){
		$ret_str = ($timecard_bean->return_icon_flg != "2") ? "退復" : "呼出";
$tbl3_hdr .= "<td width=\"64\" align=\"center\" rowspan=\"1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$ret_str</font></td>\n";
	}

	$tbl3_hdr .= "<td nowrap width=\"\" align=\"center\" rowspan=\"1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">日数</font></td>\n";
	if($meeting_display_flag){
		$tbl3_hdr .= "<td width=\"\" align=\"center\" rowspan=\"1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">病棟<br>外(h)</font></td>\n";
	}
	$tbl3_hdr .= "<td width=\"\" align=\"center\" rowspan=\"1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">最終更新日時</font></td>\n";
	$tbl3_hdr .= "<td width=\"\" align=\"center\" rowspan=\"1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">最終更新者</font></td>\n";
	$tbl3 .= $tbl3_hdr;
	$tbl3 .= "</tr>\n";

	//下側見出し
	$tbl3_hdr_b = "<tr height=\"38\">\n";
	$tbl3_hdr_b .= $tbl3_hdr;
	$tbl3_hdr_b .= "</tr>\n";


    
?>

<? $sums = show_attendance_book($con, $emp_id, $start_date, $end_date, $view, $yyyymm, $fname, $session, $atdbk_closed, $ovtmscr_tm, $arr_irrg_info, $wage, $hol_minus, $tmcd_group_id, $atdbk_common_class, $meeting_display_flag, $atdbk_workflow_common_class, $timecard_bean, $no_overtime, $closing, $recomp_flg, $set_data, $date, $plan_copy_flg, $check_flg, $obj_hol_hour, $ovtm_class); ?>

<?
	$tbl1 .= $sums["tbl1"];
	$tbl1 .= $tbl1_hdr_b;
	$tbl1 .= $sums["tbl1_total"];
	$tbl1 .= "</table>";
	$tbl2 .= $sums["tbl2"];
	$tbl2 .= $tbl2_hdr_b;
	$tbl2 .= $sums["tbl2_total"];
	$tbl2 .= "</table>";
	$tbl3 .= $sums["tbl3"];
	$tbl3 .= $tbl3_hdr_b;
	$tbl3 .= $sums["tbl3_total"];
	$tbl3 .= "</table>";
?>
<table cellpadding="0" cellspacing="0" border="0">
<tr>
<td></td>
<td align="left"><input type="button" value=" ＜ " onclick="scroll_region(1);return false;" style="width:40px; background-color:#c2ceec;"></td>
<td align="right"><input type="button" value=" ＞ " onclick="scroll_region(2);return false;" style="width:40px; background-color:#c2ceec;"></td>
<td></td>
</tr>
<tr>
	<td valign="top">
	<? echo($tbl1); ?>
</td>
	<td colspan="2" valign="top">
<div id="region" style="cursor:arrow; overflow-x:scroll; overflow-y:hidden; overflow:auto; border:#5279a5 solid 0px; width:460px">
	<? echo($tbl2); ?>
</div>
</td>
	<td valign="top">
	<? echo($tbl3); ?>
</td>
</tr>
<tr>
<td></td>
<td align="left"><input type="button" value=" ＜ " onclick="scroll_region(1);return false;" style="width:40px; background-color:#c2ceec;"></td>
<td align="right"><input type="button" value=" ＞ " onclick="scroll_region(2);return false;" style="width:40px; background-color:#c2ceec;"></td>
<td></td>
</tr>
</table>



<img src="img/spacer.gif" width="1" height="2" alt=""><br>
</td>
</tr>

<tr>
<td>
<table width="1000" border="0" cellspacing="0" cellpadding="1">
<tr>
<td align="right" width="">
<? if ($wherefrom == "7") { ?>
<input type="button" value="予定コピー" onclick="planCopy();"<? if ($atdbk_closed) {echo(" disabled");} ?>>&nbsp;
<? } ?>
<input type="button" value="<? echo($view_button); ?>" onclick="changeView();">&nbsp;
	<? /*
	<input type="button" value="EXCEL出力" onclick="downloadCSV(1);"<? if ($view == "2") {echo(" disabled");} ?> disabled>&nbsp;
	*/ ?>
<input type="button" value="再計算" onclick="recomp();"<? if ($atdbk_closed) {echo(" disabled");} ?>>&nbsp;
<input type="button" value="登録" onclick="editTimecard();"<? if ($atdbk_closed) {echo(" disabled");} ?>>&nbsp;
</td>
</tr>
</table>
</td>
</tr>

</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="emp_id" value="<? echo($emp_id); ?>">
<input type="hidden" name="yyyymm" value="<? echo($yyyymm); ?>">
<input type="hidden" name="view" value="<? echo($view); ?>">
<input type="hidden" name="wherefrom" value="<? echo($wherefrom); ?>">
<input type="hidden" name="srch_id" value="<? echo($srch_id); ?>">
<input type="hidden" name="srch_name" value="<? echo($srch_name); ?>">
<input type="hidden" name="cls" value="<? echo($cls); ?>">
<input type="hidden" name="atrb" value="<? echo($atrb); ?>">
<input type="hidden" name="dept" value="<? echo($dept); ?>">
<input type="hidden" name="page" value="<? echo($page); ?>">
<input type="hidden" name="recomp_flg" value="">
<input type="hidden" name="group_id" value="<? echo($group_id); ?>">
<input type="hidden" name="plan_copy_flg" value="">
<input type="hidden" name="duty_yyyy" value="<? echo($duty_yyyy); ?>">
<input type="hidden" name="duty_mm" value="<? echo($duty_mm); ?>">
<input type="hidden" name="shift_group_id" value="<? echo($shift_group_id); ?>">
<input type="hidden" name="csv_layout_id" value="<? echo($csv_layout_id); ?>">
<input type="hidden" name="emp_move_flg" value="">
<input type="hidden" name="emp_personal_id" value="<? echo($emp_personal_id); ?>">
<input type="hidden" name="staff_ids" value="<? echo($staff_ids); ?>">
<input type="hidden" name="data_cnt" value="<? echo($data_cnt); ?>">
<input type="hidden" name="check_flg" value="">
<input type="hidden" name="sus_flg" value="<?php echo $sus_flg; ?>">
<input type="hidden" name="mmode" value="<? echo($mmode); ?>">
</form>
<form name="csv" method="get" target="download">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="emp_id" value="<? echo($emp_id); ?>">
<input type="hidden" name="yyyymm" value="<? echo($yyyymm); ?>">
<input type="hidden" name="wherefrom" value="<? echo($wherefrom); ?>">
</form>
<iframe name="download" width="0" height="0" frameborder="0"></iframe>
<? } ?>
</td>
</tr>
</table>
<?
if ($err_flg == "1") {
	if ($emp_move_flg == "next") {
		$err_msg = "最後の職員です。";
	} else {
		$err_msg = "先頭の職員です。";
	}
	echo("<script type=\"text/javascript\">alert('$err_msg');</script>");
}
?>
</body>
<? pg_close($con); ?>
</html>
<?
// 出勤予定を出力
function show_attendance_book($con, $emp_id, $start_date, $end_date, $view, $yyyymm, $fname, $session, $atdbk_closed, $ovtmscr_tm, $arr_irrg_info, $wage, $hol_minus, $user_tmcd_group_id, $atdbk_common_class, $meeting_display_flag, $atdbk_workflow_common_class, $timecard_bean, $no_overtime, $closing, $recomp_flg, $set_data, $arr_date, $plan_copy_flg, $check_flg, $obj_hol_hour, $ovtm_class) {

	global $url_srch_name, $cls, $atrb, $dept, $page, $srch_id;

    $tbl1 = "";
	$tbl2 = "";
	$tbl3 = "";

	//一括修正申請情報
	$arr_tmmdapplyall_status = $atdbk_workflow_common_class->get_tmmdapplyall_status($emp_id, $start_date, $end_date);

	if ($plan_copy_flg == "1") {
		//会議・研修・病棟外時間情報取得
		$arr_meeting_time = $atdbk_common_class->get_atdbk_meeting_time();
	}
	//休日出勤と判定する日設定取得 20091222
	$arr_timecard_holwk_day = get_timecard_holwk_day($con, $fname, $emp_id);

	// 集計結果格納用配列を初期化
	$sums = array_fill(0, 34, 0);

	// 勤務日・休日の名称を取得
	$sql = "select day1_time, day2, day3, day4, day5 from calendarname";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0) {
		$day2 = pg_fetch_result($sel, 0, "day2");
		$day3 = pg_fetch_result($sel, 0, "day3");
		$day4 = pg_fetch_result($sel, 0, "day4");
		$day5 = pg_fetch_result($sel, 0, "day5");
		$day1_time = pg_fetch_result($sel, 0, "day1_time");
	}
	else{
		$day1_time = "";
	}
	if ($day2 == "") {
		$day2 = "法定休日";
	}
	if ($day3 == "") {
		$day3 = "所定休日";
	}
	if ($day4 == "") {
		$day4 = "勤務日1";
	}
	if ($day5 == "") {
		$day5 = "勤務日2";
	}

	// 手当情報取得
	$arr_allowance = get_timecard_allowance($con, $fname);

	// 理由情報取得
	$arr_ovtmrsn = get_ovtmrsn($con, $fname);

	// 端数処理情報を取得
	$sql = "select * from timecard";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0) {
		for ($i = 1; $i <= 5; $i++) {
			$var_name = "fraction$i";
			$var_name_min = "fraction{$i}_min";
			$$var_name = pg_fetch_result($sel, 0, "fraction$i");
			$$var_name_min = pg_fetch_result($sel, 0, "fraction{$i}_min");
		}
		for ($i = 1; $i <= 5; $i++) {
			$var_name = "others$i";
			$$var_name = pg_fetch_result($sel, 0, "others$i");
		}
		$rest1 = intval(pg_fetch_result($sel, 0, "rest1"));
		$rest2 = intval(pg_fetch_result($sel, 0, "rest2"));
		$rest3 = intval(pg_fetch_result($sel, 0, "rest3"));
		//時間給者の休憩時間追加 20100929
		$rest4 = intval(pg_fetch_result($sel, 0, "rest4"));
		$rest5 = intval(pg_fetch_result($sel, 0, "rest5"));
		$rest6 = intval(pg_fetch_result($sel, 0, "rest6"));
		$rest7 = intval(pg_fetch_result($sel, 0, "rest7"));

		$night_workday_count = pg_fetch_result($sel, 0, "night_workday_count");
		$modify_flg = pg_fetch_result($sel, 0, "modify_flg");
	} else {
		for ($i = 1; $i <= 5; $i++) {
			$var_name = "fraction$i";
			$var_name_min = "fraction{$i}_min";
			$$var_name = "";
			$$var_name_min = "";
		}
		for ($i = 1; $i <= 5; $i++) {
			$var_name = "others$i";
			$$var_name = "";
		}
		$rest1 = 0;
		$rest2 = 0;
		$rest3 = 0;
		//時間給者の休憩時間追加 20100929
		$rest4 = 0;
		$rest5 = 0;
		$rest6 = 0;
		$rest7 = 0;

		$night_workday_count = "";
		$modify_flg = "t";
	}
	//集計処理の設定を「集計しない」の固定とする 20100910
	$others1 = "1";	//勤務時間内に外出した場合
	$others2 = "1";	//勤務時間外に外出した場合
	$others3 = "1";	//時間給者の休憩の集計
	$others4 = "1";	//遅刻・早退に休憩が含まれた場合
	$others5 = "2";	//早出残業時間の集計

	// 「退勤後復帰」ボタン表示フラグを取得
	$sql = "select ret_btn_flg from config";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rolllback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$ret_btn_flg = pg_fetch_result($sel, 0, "ret_btn_flg");

	// 全日付をループ（変則労働時間・週の場合、週の開始曜日からスタート）
	$tmp_date = $start_date;
	$wd = date("w", to_timestamp($tmp_date));
	if ($arr_irrg_info["irrg_type"] == "1") {
		while (($arr_irrg_info["irrg_wd"] == "1" && $wd != 0) || ($arr_irrg_info["irrg_wd"] == "2" && $wd != 1)) {
			$tmp_date = last_date($tmp_date);
			$wd = date("w", to_timestamp($tmp_date));
		}
	} else {
		//変則労働時間・週以外の場合、日曜から
		while ($wd != 0) {
			$tmp_date = last_date($tmp_date);
			$wd = date("w", to_timestamp($tmp_date));
		}
	}
	//データ検索用開始日
	$wk_start_date = $tmp_date;

	//グループ毎のテーブルから勤務時間を取得する
	$timecard_common_class = new timecard_common_class($con, $fname, $emp_id, $tmp_date, $end_date);

    //個人別所定時間を優先するための対応 20120309
    $arr_empcond_officehours_ptn = $timecard_common_class->get_empcond_officehours_pattern();
    $arr_weekday_flg = $timecard_common_class->get_arr_weekday_flag($wk_start_date, $end_date);
    $arr_empcond_officehours = $timecard_common_class->get_arr_empcond_officehours($emp_id, $wk_start_date); //個人別勤務時間帯履歴対応 20130220

    //出・退勤時刻文字サイズ
	$time_font = "\"j12\"";
	if ($timecard_bean->time_big_font_flag == "t" /*&& $show_time_flg == "t"*/){
		$time_font = "\"y16\"";
	}
	// 処理日付の勤務日種別を取得
	$arr_type = array();
	$sql = "select date, type from calendar";
	$cond = "where date >= '$wk_start_date' and date <= '$end_date'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$date = $row["date"];
		$arr_type[$date] = $row["type"];
	}
	// 予定データをまとめて取得
	$arr_atdbk = array();
	$sql =	"SELECT ".
		"atdbk.date, ".
		"atdbk.pattern, ".
		"atdbk.reason, ".
		"atdbk.night_duty, ".
		"atdbk.allow_id, ".
		"atdbk.prov_start_time, ".
		"atdbk.prov_end_time, ".
		"atdbk.tmcd_group_id, ".
		"atdptn.workday_count ".
		"FROM ".
		"atdbk ".
		"LEFT JOIN atdptn ON atdbk.tmcd_group_id = atdptn.group_id AND atdbk.pattern = CAST(atdptn.atdptn_id AS varchar) ";
	$cond = "where emp_id = '$emp_id' and ";
	$cond .= " date >= '$wk_start_date' and date <= '$end_date'";

	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$date = $row["date"];
		$arr_atdbk[$date]["tmcd_group_id"] = $row["tmcd_group_id"];
		$arr_atdbk[$date]["pattern"] = $row["pattern"];
		$arr_atdbk[$date]["reason"] = $row["reason"];
		$arr_atdbk[$date]["night_duty"] = $row["night_duty"];
		$arr_atdbk[$date]["allow_id"] = $row["allow_id"];
		$arr_atdbk[$date]["prov_start_time"] = $row["prov_start_time"];
		$arr_atdbk[$date]["prov_end_time"] = $row["prov_end_time"];
		$arr_atdbk[$date]["workday_count"] = $row["workday_count"];
	}
	// 実績データをまとめて取得
	$arr_atdbkrslt = array();
	$sql =	"SELECT ".
		"atdbkrslt.*, ".
		"atdptn.workday_count, ".
		"atdptn.base_time, ".
		"atdptn.over_24hour_flag, ".
		"atdptn.out_time_nosubtract_flag, ".
		"atdptn.after_night_duty_flag, ".
        "atdptn.previous_day_flag as atdptn_previous_day_flag, ".
        "atdptn.no_count_flag, ".
        "tmmdapply.apply_id     AS tmmd_apply_id, ".
		"tmmdapply.apply_status AS tmmd_apply_status, ".
		"ovtmapply.apply_id     AS ovtm_apply_id, ".
		"ovtmapply.apply_status AS ovtm_apply_status, ".
		"ovtmapply.reason_id    AS ovtm_reason_id, ".
		"ovtmapply.reason       AS ovtm_reason, ".
		"rtnapply.apply_id      AS rtn_apply_id, ".
		"rtnapply.apply_status  AS rtn_apply_status, ".
		"empmst.emp_lt_nm||' '||empmst.emp_ft_nm  AS update_emp_name ".
		"FROM ".
		"atdbkrslt ".
		"LEFT JOIN atdptn ON atdbkrslt.tmcd_group_id = atdptn.group_id AND atdbkrslt.pattern = CAST(atdptn.atdptn_id AS varchar) ".
		"LEFT JOIN tmmdapply ON atdbkrslt.emp_id = tmmdapply.emp_id AND CAST(atdbkrslt.date AS varchar) = tmmdapply.target_date AND tmmdapply.delete_flg = false AND tmmdapply.re_apply_id IS NULL ".
		"LEFT JOIN ovtmapply ON atdbkrslt.emp_id = ovtmapply.emp_id AND CAST(atdbkrslt.date AS varchar) = ovtmapply.target_date AND ovtmapply.delete_flg = false AND ovtmapply.re_apply_id IS NULL ".
		"LEFT JOIN rtnapply  ON atdbkrslt.emp_id = rtnapply.emp_id  AND CAST(atdbkrslt.date AS varchar) = rtnapply.target_date AND rtnapply.delete_flg = false AND rtnapply.re_apply_id IS NULL ".
		"LEFT JOIN empmst ON atdbkrslt.update_emp_id = empmst.emp_id ";
	$cond = "where atdbkrslt.emp_id = '$emp_id' and ";
	$cond .= " atdbkrslt.date >= '$wk_start_date' and atdbkrslt.date <= '$end_date'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$date = $row["date"];
        //まとめて設定
        $arr_atdbkrslt[$date] = $row;
/*
		$arr_atdbkrslt[$date]["pattern"] = $row["pattern"];
		$arr_atdbkrslt[$date]["reason"] = $row["reason"];
		$arr_atdbkrslt[$date]["night_duty"] = $row["night_duty"];
		$arr_atdbkrslt[$date]["allow_id"] = $row["allow_id"];
		$arr_atdbkrslt[$date]["start_time"] = $row["start_time"];
		$arr_atdbkrslt[$date]["end_time"] = $row["end_time"];
		$arr_atdbkrslt[$date]["out_time"] = $row["out_time"];
		$arr_atdbkrslt[$date]["ret_time"] = $row["ret_time"];
		for ($i = 1; $i <= 10; $i++) {
			$start_var = "o_start_time$i";
			$end_var = "o_end_time$i";
			$arr_atdbkrslt[$date][$start_var] = $row[$start_var];
			$arr_atdbkrslt[$date][$end_var] = $row[$end_var];
		}
		$arr_atdbkrslt[$date]["tmcd_group_id"] = $row["tmcd_group_id"];
		$arr_atdbkrslt[$date]["meeting_time"] = $row["meeting_time"];
		$arr_atdbkrslt[$date]["workday_count"] = $row["workday_count"];
		$arr_atdbkrslt[$date]["previous_day_flag"] = $row["previous_day_flag"];
		$arr_atdbkrslt[$date]["next_day_flag"] = $row["next_day_flag"];
		$arr_atdbkrslt[$date]["tmmd_apply_id"] = $row["tmmd_apply_id"];
		$arr_atdbkrslt[$date]["tmmd_apply_status"] = $row["tmmd_apply_status"];
		$arr_atdbkrslt[$date]["ovtm_apply_id"] = $row["ovtm_apply_id"];
		$arr_atdbkrslt[$date]["ovtm_apply_status"] = $row["ovtm_apply_status"];
		$arr_atdbkrslt[$date]["ovtm_reason_id"] = $row["ovtm_reason_id"];
		$arr_atdbkrslt[$date]["ovtm_reason"] = $row["ovtm_reason"];
		$arr_atdbkrslt[$date]["rtn_apply_id"] = $row["rtn_apply_id"];
		$arr_atdbkrslt[$date]["rtn_apply_status"] = $row["rtn_apply_status"];
		$arr_atdbkrslt[$date]["meeting_start_time"] = $row["meeting_start_time"];
		$arr_atdbkrslt[$date]["meeting_end_time"] = $row["meeting_end_time"];
		$arr_atdbkrslt[$date]["base_time"] = $row["base_time"];
		//残業時刻追加 20100114
		$arr_atdbkrslt[$date]["allow_count"] = $row["allow_count"];
		$arr_atdbkrslt[$date]["over_start_time"] = $row["over_start_time"];
		$arr_atdbkrslt[$date]["over_end_time"] = $row["over_end_time"];
		$arr_atdbkrslt[$date]["over_start_next_day_flag"] = $row["over_start_next_day_flag"];
		$arr_atdbkrslt[$date]["over_end_next_day_flag"] = $row["over_end_next_day_flag"];
		//残業時刻2追加 20110616
		$arr_atdbkrslt[$date]["over_start_time2"] = $row["over_start_time2"];
		$arr_atdbkrslt[$date]["over_end_time2"] = $row["over_end_time2"];
		$arr_atdbkrslt[$date]["over_start_next_day_flag2"] = $row["over_start_next_day_flag2"];
		$arr_atdbkrslt[$date]["over_end_next_day_flag2"] = $row["over_end_next_day_flag2"];
		$arr_atdbkrslt[$date]["over_24hour_flag"] = $row["over_24hour_flag"];
		//法定内残業
		$arr_atdbkrslt[$date]["legal_in_over_time"] = $row["legal_in_over_time"];
		//早出残業
		$arr_atdbkrslt[$date]["early_over_time"] = $row["early_over_time"];

		$arr_atdbkrslt[$date]["update_time"] = $row["update_time"];
		$arr_atdbkrslt[$date]["update_emp_name"] = $row["update_emp_name"];
		//休憩時刻追加 20100921
		$arr_atdbkrslt[$date]["rest_start_time"] = $row["rest_start_time"];
		$arr_atdbkrslt[$date]["rest_end_time"] = $row["rest_end_time"];
		//外出時間を差し引かないフラグ 20110727
		$arr_atdbkrslt[$date]["out_time_nosubtract_flag"] = $row["out_time_nosubtract_flag"];
		//明け追加 20110819
		$arr_atdbkrslt[$date]["after_night_duty_flag"] = $row["after_night_duty_flag"];
        //時間帯画面の前日フラグ 20121120
        $arr_atdbkrslt[$date]["atdptn_previous_day_flag"] = $row["atdptn_previous_day_flag"];
        //勤務時間を計算しないフラグ 20130225
        $arr_atdbkrslt[$date]["no_count_flag"] = $row["no_count_flag"];
        */
    }
	//時間有休追加 20111207
	if ($obj_hol_hour->paid_hol_hour_flag == "t") {
		//時間有休データをまとめて取得
		$arr_hol_hour_dat = $obj_hol_hour->get_paid_hol_hour_month($emp_id, $wk_start_date, $end_date);

	}
	//再計算時、画面データを設定する
	if ($recomp_flg == "1") {
		for ($i = 0; $i < count($arr_date); $i++) {
			$wk_date = $arr_date[$i];
			$arr_atdbkrslt[$wk_date]["tmcd_group_id"] = $set_data[$wk_date]["tmcd_group_id"];
			$arr_atdbkrslt[$wk_date]["pattern"] = $set_data[$wk_date]["pattern"];
			$arr_atdbkrslt[$wk_date]["reason"] = $set_data[$wk_date]["reason"];
			$arr_atdbkrslt[$wk_date]["night_duty"] = $set_data[$wk_date]["night_duty"];
			$arr_atdbkrslt[$wk_date]["allow_id"] = $set_data[$wk_date]["allow_id"];
			$arr_atdbkrslt[$wk_date]["allow_count"] = $set_data[$wk_date]["allow_count"];
			$arr_atdbkrslt[$wk_date]["previous_day_flag"] = $set_data[$wk_date]["previous_day_flag"];
			$arr_atdbkrslt[$wk_date]["start_time"] = $set_data[$wk_date]["start_time"];
			$arr_atdbkrslt[$wk_date]["next_day_flag"] = $set_data[$wk_date]["next_day_flag"];
			$arr_atdbkrslt[$wk_date]["end_time"] = $set_data[$wk_date]["end_time"];
			$arr_atdbkrslt[$wk_date]["out_time"] = $set_data[$wk_date]["out_time"];
			$arr_atdbkrslt[$wk_date]["ret_time"] = $set_data[$wk_date]["ret_time"];
			$arr_atdbkrslt[$wk_date]["over_start_next_day_flag"] = $set_data[$wk_date]["over_start_next_day_flag"];
			$arr_atdbkrslt[$wk_date]["over_start_time"] = $set_data[$wk_date]["over_start_time"];
			$arr_atdbkrslt[$wk_date]["over_end_next_day_flag"] = $set_data[$wk_date]["over_end_next_day_flag"];
			$arr_atdbkrslt[$wk_date]["over_end_time"] = $set_data[$wk_date]["over_end_time"];
			//残業時刻2追加 20110616
			$arr_atdbkrslt[$wk_date]["over_start_next_day_flag2"] = $set_data[$wk_date]["over_start_next_day_flag2"];
			$arr_atdbkrslt[$wk_date]["over_start_time2"] = $set_data[$wk_date]["over_start_time2"];
			$arr_atdbkrslt[$wk_date]["over_end_next_day_flag2"] = $set_data[$wk_date]["over_end_next_day_flag2"];
			$arr_atdbkrslt[$wk_date]["over_end_time2"] = $set_data[$wk_date]["over_end_time2"];
			$arr_atdbkrslt[$wk_date]["ovtm_reason_id"] = $set_data[$wk_date]["ovtm_reason_id"];
			$arr_atdbkrslt[$wk_date]["o_start_time1"] = $set_data[$wk_date]["o_start_time1"];
			$arr_atdbkrslt[$wk_date]["o_end_time1"] = $set_data[$wk_date]["o_end_time1"];
			$arr_atdbkrslt[$wk_date]["o_start_time2"] = $set_data[$wk_date]["o_start_time2"];
			$arr_atdbkrslt[$wk_date]["o_end_time2"] = $set_data[$wk_date]["o_end_time2"];
			$arr_atdbkrslt[$wk_date]["o_start_time3"] = $set_data[$wk_date]["o_start_time3"];
			$arr_atdbkrslt[$wk_date]["o_end_time3"] = $set_data[$wk_date]["o_end_time3"];
			$arr_atdbkrslt[$wk_date]["meeting_start_time"] = $set_data[$wk_date]["meeting_start_time"];
			$arr_atdbkrslt[$wk_date]["meeting_end_time"] = $set_data[$wk_date]["meeting_end_time"];
			//法定内残業
			$arr_atdbkrslt[$wk_date]["legal_in_over_time"] = $set_data[$wk_date]["legal_in_over_time"];
			//早出残業
			$arr_atdbkrslt[$wk_date]["early_over_time"] = $set_data[$wk_date]["early_over_time"];
			//休憩時刻追加 20100921
			$arr_atdbkrslt[$wk_date]["rest_start_time"] = $set_data[$wk_date]["rest_start_time"];
			$arr_atdbkrslt[$wk_date]["rest_end_time"] = $set_data[$wk_date]["rest_end_time"];
		}
	}
	//予定コピー
	if ($plan_copy_flg == "1") {
		for ($i = 0; $i < count($arr_date); $i++) {
			$wk_date = $arr_date[$i];
			//勤務パターンをコピーする条件の時、グループもコピー
			if (($arr_atdbkrslt[$wk_date]["pattern"] == "" || $arr_atdbkrslt[$wk_date]["pattern"] == "--") && $arr_atdbk[$wk_date]["pattern"] != "") {
				$arr_atdbkrslt[$wk_date]["tmcd_group_id"] = $arr_atdbk[$wk_date]["tmcd_group_id"];
				$arr_atdbkrslt[$wk_date]["pattern"] = $arr_atdbk[$wk_date]["pattern"];
				$wk_copy_flg = true;
			} else {
				$wk_copy_flg = false;
			}
			//グループが未設定時はデフォルト設定
			if ($arr_atdbkrslt[$wk_date]["tmcd_group_id"] == "") {
				$arr_atdbkrslt[$wk_date]["tmcd_group_id"] = $user_tmcd_group_id;
			}
			if ($arr_atdbkrslt[$wk_date]["reason"] == "" || $arr_atdbkrslt[$wk_date]["reason"] == "--") {
				$arr_atdbkrslt[$wk_date]["reason"] = $arr_atdbk[$wk_date]["reason"];
			}
			if ($arr_atdbkrslt[$wk_date]["night_duty"] == "" || $arr_atdbkrslt[$wk_date]["night_duty"] == "--") {
				$arr_atdbkrslt[$wk_date]["night_duty"] = $arr_atdbk[$wk_date]["night_duty"];
			}
			if ($arr_atdbkrslt[$wk_date]["allow_id"] == "" || $arr_atdbkrslt[$wk_date]["allow_id"] == "--") {
				$arr_atdbkrslt[$wk_date]["allow_id"] = $arr_atdbk[$wk_date]["allow_id"];
			}
			//実績の時刻が未設定の場合
			if ($arr_atdbkrslt[$wk_date]["start_time"] == "" && $arr_atdbkrslt[$wk_date]["end_time"] == "") {
				//予定の時刻が設定済の場合
				if ($arr_atdbk[$wk_date]["prov_start_time"] != "" && $arr_atdbk[$wk_date]["prov_end_time"] != "") {
					if ($arr_atdbkrslt[$wk_date]["start_time"] == "") {
						$arr_atdbkrslt[$wk_date]["start_time"] = $arr_atdbk[$wk_date]["prov_start_time"];
					}
					if ($arr_atdbkrslt[$wk_date]["end_time"] == "") {
						$arr_atdbkrslt[$wk_date]["end_time"] = $arr_atdbk[$wk_date]["prov_end_time"];
					}
				}
				//予定の時刻がない場合、時間帯から設定
				else {
					//勤務パターンをコピーする場合以外は時刻設定しない
					if ($wk_copy_flg == false) {
						continue;
					}
					$tmcd_group_id = $arr_atdbkrslt[$wk_date]["tmcd_group_id"];
					$atdptn_ptn_id = $arr_atdbkrslt[$wk_date]["pattern"];
					if ($tmcd_group_id != "" &&
							$atdptn_ptn_id != "") {
						$type = $arr_type[$wk_date];
						$tmp_type = ($type != "2" && $type != "3") ? "1" : $type;

						$tmp_start_time = "";
						$tmp_end_time = "";
						//休日以外の場合に設定
						if ($atdptn_ptn_id != "10") {
							$tmp_start_time = $timecard_common_class->get_officehours_info( $tmcd_group_id, $atdptn_ptn_id, $tmp_type, "start2" );
							$tmp_end_time   = $timecard_common_class->get_officehours_info( $tmcd_group_id, $atdptn_ptn_id, $tmp_type, "end2" );
                            //個人別所定時間を優先する場合 20140318
                            if ($arr_empcond_officehours_ptn[$tmcd_group_id][$atdptn_ptn_id] == 1) {
                                $wk_weekday = $arr_weekday_flg[$wk_date];
                                $arr_empcond_officehours = $timecard_common_class->get_empcond_officehours($wk_weekday);
                                if ($arr_empcond_officehours["officehours2_start"] != "") {
                                    $tmp_start_time = $arr_empcond_officehours["officehours2_start"];
                                    $tmp_end_time = $arr_empcond_officehours["officehours2_end"];
                                }
                            }
                        }

						$arr_atdbkrslt[$wk_date]["start_time"] = $tmp_start_time;
						$arr_atdbkrslt[$wk_date]["end_time"] = $tmp_end_time;
						//会議・研修・病棟外
						if ($arr_atdbkrslt[$wk_date]["meeting_start_time"] == "" && $arr_atdbkrslt[$wk_date]["meeting_end_time"] == "") {
							$arr_atdbkrslt[$wk_date]["meeting_start_time"] = $arr_meeting_time[$tmcd_group_id][$atdptn_ptn_id]["meeting_start_time"];
							$arr_atdbkrslt[$wk_date]["meeting_end_time"] = $arr_meeting_time[$tmcd_group_id][$atdptn_ptn_id]["meeting_end_time"];
						}
					}
				}
			}
		}
	}

	$counter_for_week = 1;
	$holiday_count = 0;
	//法定内勤務、法定内残業、法定外残業
	$hoteinai = 0;
	$hoteinai_zan = 0;
	$hoteigai = 0;

	//遅刻時間計 20100907
	$delay_time = 0;

	//早退時間計
	$early_leave_time = 0;

	//支給換算日数 20090908
	$paid_day_count = 0;

	//合計、外出、休憩、退復、日数、病棟外、早出
	$out_time_total = 0;
	$rest_time_total = 0;
	$over_rest_time_total = 0; //残業休憩 20141209
	$return_time_total = 0;
	$meeting_time_total = 0;
	$early_time_total = 0;
    //呼出回数
    $return_count_total = 0;

	$wk_pos = 0; //行位置
	//休日出勤テーブル 2008/10/23
	$arr_hol_work_date = array();
	// チェック対応 20110125
	$today = date("Ymd");
	while ($tmp_date <= $end_date) {
		$tmp_year = intval(substr($tmp_date, 0, 4));
		$tmp_month = intval(substr($tmp_date, 4, 2));
		$tmp_day = intval(substr($tmp_date, 6, 2));

		if ($counter_for_week >= 8) {
			$counter_for_week = 1;
		}
		if ($counter_for_week == 1) {
			//前日までの計
			$last_day_total = 0;
			$work_time_for_week = 0;
			if ($arr_irrg_info["irrg_type"] == "1") {
				$holiday_count = 0;
			}
			//1日毎の法定外残を週単位に集計
			$wk_week_hoteigai = 0;
		}
		$work_time = 0;
		$wk_return_time = 0;

		$start_time_info = array();
		$end_time_info = array(); //退勤時刻情報 20100910

		$time3 = 0; //外出時間（所定時間内） 20100910
		$time4 = 0; //外出時間（所定時間外） 20100910
		$tmp_rest2 = 0; //休憩時間 20100910
		$time3_rest = 0; //休憩時間（所定時間内）20100910
		$time4_rest = 0; //休憩時間（所定時間外）20100910
		$arr_effective_time_wk = array(); //休憩を引く前の実働時間 20100915

		$holiday_name = get_holiday_name($tmp_date);
		if ($holiday_name != "") {
			$holiday_count++;
		}

		// 処理日付の勤務日種別を取得
		$type = $arr_type[$tmp_date];
		// カレンダー名を取得
		$type_name = $atdbk_common_class->get_calendarname($type, $tmp_date);

		// 処理日付の勤務予定情報を取得
		$prov_tmcd_group_id = $arr_atdbk[$tmp_date]["tmcd_group_id"];
		$prov_pattern = $arr_atdbk[$tmp_date]["pattern"];
		$prov_reason = $arr_atdbk[$tmp_date]["reason"];
		$prov_night_duty = $arr_atdbk[$tmp_date]["night_duty"];
		$prov_allow_id = $arr_atdbk[$tmp_date]["allow_id"];

		$prov_start_time = $arr_atdbk[$tmp_date]["prov_start_time"];
		$prov_end_time = $arr_atdbk[$tmp_date]["prov_end_time"];
		$prov_start_time = ($prov_start_time != "") ? preg_replace("/(\d{2})(\d{2})/", "$1:$2", $prov_start_time) : "";
		$prov_end_time =  ($prov_end_time != "") ? preg_replace("/(\d{2})(\d{2})/", "$1:$2", $prov_end_time) : "";
		$prov_workday_count = $arr_atdbk[$tmp_date]["workday_count"];

		// 処理日付の勤務実績を取得
		$pattern = $arr_atdbkrslt[$tmp_date]["pattern"];
		$reason = $arr_atdbkrslt[$tmp_date]["reason"];
		$night_duty = $arr_atdbkrslt[$tmp_date]["night_duty"];
		$allow_id = $arr_atdbkrslt[$tmp_date]["allow_id"];
		$start_time = $arr_atdbkrslt[$tmp_date]["start_time"];
		$end_time = $arr_atdbkrslt[$tmp_date]["end_time"];
		$out_time = $arr_atdbkrslt[$tmp_date]["out_time"];
		$ret_time = $arr_atdbkrslt[$tmp_date]["ret_time"];

		for ($i = 1; $i <= 10; $i++) {
			$start_var = "o_start_time$i";
			$end_var = "o_end_time$i";
			$$start_var = $arr_atdbkrslt[$tmp_date]["$start_var"];
			$$end_var = $arr_atdbkrslt[$tmp_date]["$end_var"];

			if ($$start_var != "") {
				$$start_var = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $$start_var);
			}
			if ($$end_var != "") {
				$$end_var = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $$end_var);
			}
		}

		$tmcd_group_id = $arr_atdbkrslt[$tmp_date]["tmcd_group_id"];
		$meeting_time = $arr_atdbkrslt[$tmp_date]["meeting_time"];

		$workday_count = $arr_atdbkrslt[$tmp_date]["workday_count"];

		$previous_day_flag = $arr_atdbkrslt[$tmp_date]["previous_day_flag"];
		$next_day_flag = $arr_atdbkrslt[$tmp_date]["next_day_flag"];

		$tmmd_apply_id = $arr_atdbkrslt[$tmp_date]["tmmd_apply_id"];
		$tmmd_apply_status = $arr_atdbkrslt[$tmp_date]["tmmd_apply_status"];
		$ovtm_apply_id = $arr_atdbkrslt[$tmp_date]["ovtm_apply_id"];
		$ovtm_apply_status = $arr_atdbkrslt[$tmp_date]["ovtm_apply_status"];
        //否認の場合は残業理由を未設定にする 20120723
        if ($ovtm_apply_status == "2") {
            $ovtm_reason = "";
            $ovtm_reason_id = "";
        }
        else {
            $ovtm_reason_id = $arr_atdbkrslt[$tmp_date]["ovtm_reason_id"];
            $ovtm_reason = $arr_atdbkrslt[$tmp_date]["ovtm_reason"];
        }
		$rtn_apply_id = $arr_atdbkrslt[$tmp_date]["rtn_apply_id"];
		$rtn_apply_status = $arr_atdbkrslt[$tmp_date]["rtn_apply_status"];

		$meeting_start_time = $arr_atdbkrslt[$tmp_date]["meeting_start_time"];
		$meeting_end_time = $arr_atdbkrslt[$tmp_date]["meeting_end_time"];
		$base_time = $arr_atdbkrslt[$tmp_date]["base_time"];
		//法定外残業の基準時間
		if ($base_time == "") {
			$wk_base_time = 8 * 60;
		} else {
			$base_hour = intval(substr($base_time, 0, 2));
			$base_min = intval(substr($base_time, 2, 2));
			$wk_base_time = $base_hour * 60 + $base_min;
		}
		//hhmmをhh:mmに変換している
		if ($start_time != "") {
			$start_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $start_time);
		}
		if ($end_time != "") {
			$end_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $end_time);
		}
		//表示用に退避 20100802
		$disp_start_time = $start_time;
		$disp_end_time = $end_time;

		if ($out_time != "") {
			$out_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $out_time);
		}
		if ($ret_time != "") {
			$ret_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $ret_time);
		}
		if ($meeting_start_time != "") {
			$meeting_start_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $meeting_start_time);
		}
		if ($meeting_end_time != "") {
			$meeting_end_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $meeting_end_time);
		}
		if ($previous_day_flag == "") {
			$previous_day_flag = 0;
		}
		if ($next_day_flag == "") {
			$next_day_flag = 0;
		}
		$allow_count = $arr_atdbkrslt[$tmp_date]["allow_count"];
		//残業時刻追加 20100114
		$over_start_time = $arr_atdbkrslt[$tmp_date]["over_start_time"];
		$over_end_time = $arr_atdbkrslt[$tmp_date]["over_end_time"];
		$over_start_next_day_flag = $arr_atdbkrslt[$tmp_date]["over_start_next_day_flag"];
		$over_end_next_day_flag = $arr_atdbkrslt[$tmp_date]["over_end_next_day_flag"];
		if ($over_start_time != "") {
			$over_start_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $over_start_time);
		}
		if ($over_end_time != "") {
			$over_end_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $over_end_time);
		}
		//残業時刻2追加 20110616
		$over_start_time2 = $arr_atdbkrslt[$tmp_date]["over_start_time2"];
		$over_end_time2 = $arr_atdbkrslt[$tmp_date]["over_end_time2"];
		$over_start_next_day_flag2 = $arr_atdbkrslt[$tmp_date]["over_start_next_day_flag2"];
		$over_end_next_day_flag2 = $arr_atdbkrslt[$tmp_date]["over_end_next_day_flag2"];
		if ($over_start_time2 != "") {
			$over_start_time2 = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $over_start_time2);
		}
		if ($over_end_time2 != "") {
			$over_end_time2 = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $over_end_time2);
		}
		$over_24hour_flag = $arr_atdbkrslt[$tmp_date]["over_24hour_flag"];
		//法定内残業
		$legal_in_over_time = $arr_atdbkrslt[$tmp_date]["legal_in_over_time"];
		if ($legal_in_over_time != "") {
			$legal_in_over_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $legal_in_over_time);
		}
		//早出残業
		$early_over_time = $arr_atdbkrslt[$tmp_date]["early_over_time"];
		if ($early_over_time != "") {
			$early_over_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $early_over_time);
		}
		//休憩時刻追加 20100921
		$rest_start_time = $arr_atdbkrslt[$tmp_date]["rest_start_time"];
		$rest_end_time = $arr_atdbkrslt[$tmp_date]["rest_end_time"];
		if ($rest_start_time != "") {
			$rest_start_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $rest_start_time);
		}
		if ($rest_end_time != "") {
			$rest_end_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $rest_end_time);
		}
		//外出時間を差し引かないフラグ 20110727
		$out_time_nosubtract_flag = $arr_atdbkrslt[$tmp_date]["out_time_nosubtract_flag"];
		//明け追加 20110819
		$after_night_duty_flag = $arr_atdbkrslt[$tmp_date]["after_night_duty_flag"];
        //時間帯画面の前日フラグ 20121120
        $atdptn_previous_day_flag = $arr_atdbkrslt[$tmp_date]["atdptn_previous_day_flag"];
        //勤務時間を計算しないフラグ 20130225
        $no_count_flag = $arr_atdbkrslt[$tmp_date]["no_count_flag"];
        
		//最終更新日時・更新者
		if ($arr_atdbkrslt[$tmp_date]["update_time"] != "") {
			$update_time = preg_replace("/(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/","$1年$2月$3日 $4:$5",$arr_atdbkrslt[$tmp_date]["update_time"]);
		} else {
			$update_time = "&nbsp;";
		}
		if ($arr_atdbkrslt[$tmp_date]["update_emp_name"] != "") {
			$update_emp_name = $arr_atdbkrslt[$tmp_date]["update_emp_name"];
		} else {
			$update_emp_name = "&nbsp;";
		}

		//出勤予定にグループが指定されていた場合、出勤予定のグループを優先する
		if ($tmcd_group_id == null && strlen($tmcd_group_id) == 0){
			// 出勤パターン名を取得
			$tmcd_group_id = $user_tmcd_group_id;
		}

		$selectGroupId   = "tmcd_group_id_".$tmp_date;
		$selectPatternId = "pattern_".$tmp_date;

		//当直の有効判定
		$night_duty_flag = $timecard_common_class->is_valid_duty($night_duty, $tmp_date, $start_time, $previous_day_flag, $end_time, $next_day_flag, $tmcd_group_id);

		// 残業表示用に退勤時刻を退避
		$show_end_time = $end_time;
		$show_next_day_flag = $next_day_flag;	//残業時刻日またがり対応 20100114

		$rslt_workday_count = "";

		if ($tmp_date >= $start_date) {

			//公休か事由なし休暇で残業時刻がありの場合 20100802
//			if ($pattern == "10" && ($reason == "22" || $reason == "23" || $reason == "24" || $reason == "34" || $reason == "") && $over_start_time != "" && $over_end_time != "") {
			//事由に関わらず休暇で残業時刻がありの場合へ変更 20100916
			if (($pattern == "10" || $after_night_duty_flag == "1") && //明けを追加 20110819
				$over_start_time != "" && $over_end_time != "") {
				$legal_hol_over_flg = true;
				//出勤退勤時刻が未設定の場合、残業時刻を設定
//				$start_time = ($start_time == "") ? $over_start_time : $start_time;
//				$end_time = ($end_time == "") ? $over_end_time : $end_time;
				$start_time = $over_start_time;
				$end_time = $over_end_time;

				// 20100802
				$wk_base_time = 0;
			} else {
				$legal_hol_over_flg = false;
			}

			// 出勤日数をカウント（条件：出勤と退勤の打刻あり、または、「明け」の場合）
			if (($start_time != "" && $end_time != "") || $after_night_duty_flag == "1") {

				/*
				//残業終了時刻がある場合は、退勤時刻へ設定する 20100114
				if ($over_end_time != "") {
					//日またがり対応
					if ($over_end_next_day_flag == "1" ||
							($start_time > $over_end_time &&
								$previous_day_flag == "0")) {
						$next_day_flag = "1";
					} else {
						//残業終了時刻が当日の場合
						$next_day_flag = "0";
					}
					//$end_time = $over_end_time; //早退時対応 20100913
				} else {
					//日またがり対応
					if ($start_time > $end_time &&
							$previous_day_flag == "0") {
						$next_day_flag = "1";
					}
				}
				*/
				//日またがり対応 20101027
				if ($start_time > $end_time &&
						$previous_day_flag == "0") {
					$next_day_flag = "1";
				}

				//休暇時は計算しない 20090806
				if ($pattern != "10") {
					$rslt_workday_count = $workday_count;
				}
				//当直時、当直日数を追加する
				if($night_duty_flag){

					//通常・土・祝日で追加する日数を変更
					//曜日に対応したworkday_countを取得する
					$rslt_workday_count = $rslt_workday_count + $timecard_common_class->get_night_workday_count($tmp_date, $end_time, $next_day_flag, $tmcd_group_id);
				}
				$sums[1] = $sums[1] + $rslt_workday_count;

				//休日出勤のカウント方法変更 20091222
				if (check_timecard_holwk(
							$arr_timecard_holwk_day, $arr_type[$tmp_date], $pattern, $reason) == true) {
					$sums[2] += 1;
				}
			}

			// 事由ベースの日数計算
			$reason_day_count = $atdbk_common_class->get_reason_day_count($reason);
			//reason_day_countは支給換算日数となる 20090908
			if ($reason != "") {
				$paid_day_count += $reason_day_count;
			}
			//時間有休追加 20111207
			$paid_time_hour = 0;
			if ($obj_hol_hour->paid_hol_hour_flag == "t") {
				$paid_hol_min =  $arr_hol_hour_dat[$tmp_date]["calc_minute"];
				$paid_time_hour = $paid_hol_min;
			}
		}

		// 各時間帯の開始時刻・終了時刻を変数に格納
		//		$tmp_type = ($type == "4" || $type == "5") ? "1" : $type;
		//type:"2勤務日1,3勤務日2"以外は"1通常"とする。"4法定休日,5所定休日,6祝日,7年末年始休暇"
		$tmp_type = ($type != "2" && $type != "3") ? "1" : $type;
		$start2 = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "start2" ); //所定開始
		$end2 =   $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "end2" ); //所定終了
		$start4 = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "start4" ); //休憩開始
		$end4 =   $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "end4" ); //休憩終了
		$start5 = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "start5" ); //深夜開始
		$end5 =   $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "end5" ); //深夜終了

        //個人別所定時間を優先する場合 20120309
        if ($arr_empcond_officehours_ptn[$tmcd_group_id][$pattern] == 1) {
            $wk_weekday = $arr_weekday_flg[$tmp_date];
            $arr_empcond_officehours = $timecard_common_class->get_empcond_officehours($wk_weekday, $tmp_date); //個人別勤務時間帯履歴対応 20130220
            if ($arr_empcond_officehours["officehours2_start"] != "") {
                $start2 = $arr_empcond_officehours["officehours2_start"];
                $end2 = $arr_empcond_officehours["officehours2_end"];
                $start4 = $arr_empcond_officehours["officehours4_start"];
                $end4 = $arr_empcond_officehours["officehours4_end"];
            }
        }

        // 所定労働時間帯が確定できない場合、予定時刻を所定労働時間帯として使用
		if (($start2 == "" || $end2 == "") && $prov_start_time != "" && $prov_end_time != "") {
			$start2 = $prov_start_time;
			$end2 = $prov_end_time;
		}
		$over_calc_flg = false;	//残業時刻 20100525
		//残業時刻追加 20100114
		//残業開始が予定終了時刻より前の場合に設定
		if ($over_start_time != "") {
			//残業開始日時
			if ($over_start_next_day_flag == "1") {
				$over_start_date = next_date($tmp_date);
			} else {
				$over_start_date = $tmp_date;
			}

			$over_start_date_time = $over_start_date.$over_start_time;
			$over_calc_flg = true;
			//残業終了時刻 20100705
			$over_end_date = ($over_end_next_day_flag == "1") ? next_date($tmp_date) : $tmp_date;
			$over_end_date_time = $over_end_date.$over_end_time;
		}
		// 実働時間をクリア、出勤時刻＜退勤時刻でない場合に前日の残業時間が表示されないようにする。
		$effective_time = 0;

		//一括修正のステータスがある場合は設定
		$tmp_status = $arr_tmmdapplyall_status[$tmp_date]["apply_status"];
		if ($tmmd_apply_status == "" && $tmp_status != "") {
			$tmmd_apply_status = $tmp_status;
			$tmmd_apply_id = $arr_tmmdapplyall_status[$tmp_date]["apply_id"];
		}
		// 勤務時間修正申請情報を取得
		//tmmd_apply_statusからlink_type取得
		$modify_link_type = $timecard_common_class->get_link_type_modify($tmmd_apply_status);
		$modify_apply_id  = $tmmd_apply_id;

		// 残業情報を取得
		//ovtm_apply_statusからlink_type取得
		$night_duty_time_array  = array();
		$duty_work_time         = 0;
		if ($night_duty_flag){
			//当直時間の取得
			$night_duty_time_array = $timecard_common_class->get_duty_time_array($tmp_date, $end_time, $next_day_flag, $tmcd_group_id);

			//日数換算の数値ｘ通常勤務日の労働時間の時間
			$duty_work_time = date_utils::hi_to_minute($day1_time) * $timecard_common_class->get_night_workday_count($tmp_date, $end_time, $next_day_flag, $tmcd_group_id);
		}
		//申請状態
		$overtime_link_type = $timecard_common_class->get_link_type_overtime($tmp_date, $start_time, $previous_day_flag, $end_time, $next_day_flag, $night_duty, $start2, $end2, $ovtm_apply_status, $tmcd_group_id, $over_start_time, $over_end_time, $early_over_time, $over_24hour_flag, $over_start_time2, $over_end_time2, $over_start_next_day_flag2, $over_end_next_day_flag2);
		$overtime_apply_id  = $ovtm_apply_id;

		//test残業時刻がなく状態が未申請の場合は、残業申請状態クリア 20101004
		//if ($over_start_time == "" && $over_end_time == "" && $overtime_link_type == "1") {
		//	$overtime_link_type = "0";
		//}

		// 退勤後復帰申請情報を取得
		//rtn_apply_statusからlink_type取得
		$return_link_type = $timecard_common_class->get_link_type_return($ret_btn_flg, $o_start_time1, $rtn_apply_status);
		$return_apply_id  = $rtn_apply_id;

		// 所定労働・休憩開始終了日時の取得
        // 所定開始時刻が前日の場合 20121120
        if ($atdptn_previous_day_flag == "1") {
            $tmp_prev_date = last_date($tmp_date);
			$start2_date_time = $tmp_prev_date.$start2;
			//24時間以上勤務の場合 20100203
			if ($over_24hour_flag == "1") {
				$tmp_prev_date = $tmp_date;
			}
			$end2_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_prev_date, $start2, $end2);
		} else {
			$start2_date_time = $tmp_date.$start2;
			$start2_date_time = $tmp_date.$start2;
			//24時間以上勤務の場合 20100203
			if ($over_24hour_flag == "1") {
				$tmp_end_date = next_date($tmp_date);
			} else {
				$tmp_end_date = $tmp_date;
			}
			$end2_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_end_date, $start2, $end2);
		}

		//休憩時刻追加、入力された場合時間帯の設定の代わりに使用 20100921
		if ($rest_start_time != "" && $rest_end_time != "") {
			$start4 = $rest_start_time;
			$end4 = $rest_end_time;
		}
		//休憩時刻の日またがり確認 20141104
        $wk_start2 = ($start2 != "") ? $start2 : $start_time; //所定時刻なしの休憩時間計算不具合対応 20141128
		$start4_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_date, $wk_start2, $start4);
		$end4_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_date, $wk_start2, $end4);
		//echo(" date_time=$start2_date_time, $end2_date_time");

		//外出復帰日時の取得
		$out_date_time = $tmp_date.$out_time;
		$ret_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_date, $out_time, $ret_time);

		// 時間帯データを配列に格納
		$arr_time2 = $timecard_common_class->get_minute_time_array($start2_date_time, $end2_date_time);  // 所定労働
		// 前日出勤フラグ、翌日退勤フラグを考慮した休憩時間配列を取得
		// 24時間超える場合の翌日の休憩は配列にいれない 20101027
		if ($start2 < $start4) {
			$arr_time4 = $timecard_common_class->get_minute_time_array2($start4_date_time, $end4_date_time, $previous_day_flag, "");  // 休憩
		} else {
			$arr_time4 = $timecard_common_class->get_minute_time_array2($start4_date_time, $end4_date_time, $previous_day_flag, $next_day_flag);  // 休憩
		}

		//時間帯情報 休憩、深夜、前日当日翌日分の開始終了時刻を保持 20110126
		if ($tmp_date >= $start_date) {
			$work_times_info = $timecard_common_class->get_work_times_info($tmp_date, $start4, $end4, $start5, $end5);
		}
		// 処理当日の所定労働時間を求める
		if ($holiday_name != "" && $hol_minus == "t") {  // 祝日かつ祝日減算設定
			$specified_time = 0;
			//		} else if ($wage == "4" && $others3 == "2") {  // 時間給者かつ休憩を実働加算 集計処理の設定を「集計しない」の固定とする 20100910
//			$specified_time = count($arr_time2);
		} else {
			$specified_time = count(array_diff($arr_time2, $arr_time4));
		}

		// 出退勤とも打刻済みの場合
        //勤務時間を計算しないフラグが"1"以外 20130225
        if ($start_time != "" && $end_time != "" && $no_count_flag != "1") {
            
			//早出残業が入力された場合、出勤時刻を開始予定時刻にする
			if ($early_over_time != "") {
				// 出勤が予定より早い場合、開始予定時刻にする 20100910
				if ($start_time < $start2) {
					$wk_start_time = $start2;
				} else {
					$wk_start_time = $start_time;
				}
			} else {
				$wk_start_time = $start_time;
			}
			$start_date_time = $timecard_common_class->get_timecard_start_date_time($tmp_date, $wk_start_time, $previous_day_flag);
			$end_date_time   = $timecard_common_class->get_timecard_end_date_time($tmp_date, $end_time, $next_day_flag);
			// ここまでの処理で所定労働時間帯が未確定の場合、勤務実績で上書き
			if (empty($start2_date_time) || empty($end2_date_time)) {
				$start2_date_time = $start_date_time;
				$end2_date_time    = $end_date_time;
			}

			$overtime_approve_flg = $timecard_common_class->get_overtime_approve_flg($overtime_link_type, $modify_flg, $modify_link_type);
			//未申請を表示しない設定追加
			//休暇残業の場合の残業時間計算 //明けを追加 20110819
			if (($pattern == "10" || $after_night_duty_flag == "1") && $over_start_time != "" && $over_end_time != "") {
				$diff_min_end_time = date_utils::get_diff_minute($over_end_date_time ,$over_start_date_time);
			}
			else {
				if ($end_date_time > $end2_date_time) {
					$diff_min_end_time = date_utils::get_diff_minute($end_date_time ,$end2_date_time);
				} else {
					$diff_min_end_time = "";
				}
			}
			// 残業管理をする場合
			if ($no_overtime != "t") {
				// 残業未承認か確認
				if ($overtime_approve_flg == "1" || ($return_link_type != "0" && $return_link_type != "3")) {
					if (date_utils::to_timestamp_from_ymdhi($start2_date_time) > date_utils::to_timestamp_from_ymdhi($start_date_time)) { //所定より出勤が早い場合には所定にする 20100914 時刻に:があるため遅刻時間の表示されない不具合 20101005
						$start_date_time  = $start2_date_time; //出勤時間、早出残業の場合
					}
					if ($end_date_time > $end2_date_time) {
						$end_date_time = $end2_date_time;
					}
				}
			}

			// 出勤〜退勤までの時間を配列に格納
			$arr_tmp_work_time = $timecard_common_class->get_minute_time_array($start_date_time, $end_date_time);

			//出勤同時刻退勤または、出勤時間＞退勤時間の場合は処理を行わない
			if (count($arr_tmp_work_time) > 0){
				// 時間帯データを配列に格納
				$arr_time2 = $timecard_common_class->get_minute_time_array($start2_date_time, $end2_date_time);  // 所定労働
				//if (count($arr_time2) == 0) {
				//	$tmp_start2_date_time = date_utils::add_minute_ymdhi($end2_date_time, -1);
				//	$arr_time2 = $timecard_common_class->get_minute_time_array($tmp_start2_date_time, $end2_date_time);  // 所定労働
				//}
				//休日で残業がある場合 20101028
				if ($pattern == "10" && $over_start_time != "" && $over_end_time != "") {
					//深夜残業の開始終了時間
					$start5 = "2200";
					$end5 = "0500";
				}
				//前日・翌日の深夜帯も追加する
				$arr_time5 = $timecard_common_class->get_late_night_shift_array($tmp_date, $start5, $end5);  // 深夜帯

				// 確定出勤時刻／遅刻回数を取得
				// 勤務パターンで時刻指定がない場合でも、端数処理する 20090605
				if ($start2 == "" && $end2 == "" && $fraction1 > "1") {
					//端数処理する分
					$moving_minutes = $timecard_common_class->get_moving_minutes($fraction1_min);
					//基準日時
					$tmp_office_start_time = substr($start_date_time, 0, 10) . "00";
					$fixed_start_time = date_utils::move_time($tmp_office_start_time, $start_date_time, $moving_minutes, $fraction1);
					$start_time_info = array();
				} else {
					$start_time_info = $timecard_common_class->get_start_time_info($arr_tmp_work_time, $arr_time2, $arr_time4, $fraction1, $fraction1_min, $fraction3, $others4, $others5);
					$fixed_start_time = $start_time_info["fixed_time"];
				}

				// 確定退勤時刻／早退回数を取得
				// 勤務パターンで時刻指定がない場合でも、端数処理する 20090605
				if ($start2 == "" && $end2 == "" && $fraction2 > "1") {
					//端数処理する分
					$moving_minutes = $timecard_common_class->get_moving_minutes($fraction2_min);
					//基準日時
					$tmp_office_start_time = substr($start_date_time, 0, 10) . "00";
					$fixed_end_time = date_utils::move_time($tmp_office_start_time, $end_date_time, $moving_minutes, $fraction2);
					//echo("fixed_end_time=$fixed_end_time");
					$end_time_info = array();
				} else {
					//残業時刻が入っている場合は、端数処理しない 20100806
					$wk_over_end_date_time = str_replace(":", "", $over_end_date_time); //20111128
                    if ($over_end_time != "" && $wk_over_end_date_time > $end2_date_time && $end_date_time >= $end2_date_time) { //時間が所定終了よりあとか確認20111128 //早退の場合を除く条件追加 20140902
						$fixed_end_time = $wk_over_end_date_time;
					} else {
						$end_time_info = $timecard_common_class->get_end_time_info($arr_tmp_work_time, $arr_time2, $arr_time4, $fraction2, $fraction2_min, $fraction3, $others4);
						$fixed_end_time = $end_time_info["fixed_time"];
					}
					//※残業申請未承認の場合と（残業申請不要かつ残業管理をする場合）に退勤時刻を所定にする 20100721
					//残業申請画面を表示しないは除く 20100811
					if ($timecard_bean->over_time_apply_type != "0") {
						if ($overtime_approve_flg == "1" || ($overtime_approve_flg == "0" && $no_overtime != "t")) {
							//退勤時刻が所定よりあとの場合設定 20101224
							if ($fixed_end_time > $end2_date_time) {
								$fixed_end_time = $end2_date_time;
							}
						}
					}
				}
				//時間有休がある場合は、遅刻早退を計算しない 20111207
				if ($obj_hol_hour->paid_hol_hour_flag == "t" && $paid_time_hour > 0) {
					//時間有休と相殺する場合
                    if ($ovtm_class->sousai_sinai_flg != "t") {
                        //遅刻
                        $tikoku_time = 0;
                        $start_time_info["diff_minutes"] = 0;
                        $start_time_info["diff_count"] = 0;
                        //早退
                        $sotai_time = 0;
                        $end_time_info["diff_minutes"] = 0;
                        $end_time_info["diff_count"] = 0;
                    }
				}
				//前日出勤の場合、遅刻は発生しない
				if ($tmp_date >= $start_date && $previous_day_flag == false) {
					$sums[10] += $start_time_info["diff_count"];
					$delay_time += $start_time_info["diff_minutes"]; //遅刻時間 20100907
				}
				if ($tmp_date >= $start_date) {
					//早退
					$sums[11] += $end_time_info["diff_count"];
					$early_leave_time += $end_time_info["diff_minutes"];
				}

				// 実働時間・所定労働時間を配列に格納（休憩を除く）・有休時間を算出
				switch ($reason) {
					case "2":  // 午前有休
					case "38": // 午前年休
						// 残業時間が正しく計算されないため、休憩終了時間設定をしない 20100330
						//if (in_array($fixed_start_time, $arr_time2)) {
							$tmp_start_time = $fixed_start_time;
						//} else {
						//	$tmp_start_time = $end4_date_time;
						//}
						$tmp_end_time = $fixed_end_time;
						$arr_effective_time = $timecard_common_class->get_minute_time_array($tmp_start_time, $tmp_end_time);

						$arr_specified_time = $arr_time2;

						//$paid_time = date_utils::get_time_difference($start2_date_time, $start4_date_time);

						//休憩を引く前の実働時間
						$arr_effective_time_wk = $arr_effective_time;
						break;
					case "3":  // 午後有休
					case "39": // 午後年休
						$tmp_start_time = $fixed_start_time;
						// 残業時間が正しく計算されないため、休憩開始時間設定をしない 20100330
						//if (in_array($fixed_end_time, $arr_time2)) {
							$tmp_end_time = $fixed_end_time;
						//} else {
						//	$tmp_end_time = $start4_date_time;
						//}
						$arr_effective_time = $timecard_common_class->get_minute_time_array($tmp_start_time, $tmp_end_time);

						$arr_specified_time = $arr_time2;

						//$paid_time = date_utils::get_time_difference($end4_date_time, $end2_date_time);

						//休憩を引く前の実働時間
						$arr_effective_time_wk = $arr_effective_time;
						break;
					default:
						$tmp_start_time = $fixed_start_time;
						$tmp_end_time = $fixed_end_time;
						$arr_effective_time = $timecard_common_class->get_minute_time_array($tmp_start_time, $tmp_end_time);
						//休憩を引く前の実働時間
						$arr_effective_time_wk = $arr_effective_time;
						if ($wage != "4" || ($start4 != "" && $end4 != "")) { //休憩時刻がある場合を追加 20100925
							$arr_effective_time = array_diff($arr_effective_time, $arr_time4);
						}

						$arr_specified_time = array_diff($arr_time2, $arr_time4);

						//$paid_time = 0;

						break;
				}
				//残業時刻追加対応 20100114
				//予定終了時刻から残業開始時刻の間を除外する
				if ($over_start_time != "") {
					$wk_end_date_time = $end2_date_time;

					$arr_jogai = array();
					/* 20100713
					//基準日時
					$tmp_office_start_time = substr($end2_date_time, 0, 10) . "00";
					if ($fraction1 > "1") {
						//端数処理する分
						$moving_minutes = $timecard_common_class->get_moving_minutes($fraction1_min);
						$over_start_date_time = date_utils::move_time($tmp_office_start_time, $over_start_date_time, $moving_minutes, $fraction1);
					}
					if ($fraction2 > "1") {
						//端数処理する分
						$moving_minutes = $timecard_common_class->get_moving_minutes($fraction2_min);
						$wk_end_date_time = date_utils::move_time($tmp_office_start_time, $wk_end_date_time, $moving_minutes, $fraction2);
					}
					*/
					//残業開始時刻があとの場合
					if (date_utils::to_timestamp_from_ymdhi($over_start_date_time) > date_utils::to_timestamp_from_ymdhi($wk_end_date_time)) {
						//時間配列を取得する（休憩扱い）
						$arr_jogai = $timecard_common_class->get_minute_time_array($wk_end_date_time, $over_start_date_time);
						//休憩扱いの時間配列を除外する
						$arr_effective_time = array_diff($arr_effective_time, $arr_jogai);
					}

				}

				//休日勤務時間（分）と休日出勤を取得 2008/10/23
				if ($tmp_date >= $start_date) {
					//休暇時は計算しない 20090806
					if ($pattern != "10") {
						$arr_hol_time = $timecard_common_class->get_holiday_work($arr_type, $tmp_date, $arr_effective_time, $start_time, $previous_day_flag, $end_time, $next_day_flag, $reason);
						//$sums[31] += $arr_hol_time[3]; //20110126

						// 休日出勤した日付を配列へ設定
						for ($i=4; $i<7; $i++) {
							if ($arr_hol_time[$i] != "") {
								$arr_hol_work_date[$arr_hol_time[$i]] = 1;
							}
						}
					}
				}
				//当直の場合当直時間を計算から除外する
				if ($night_duty_flag){
					//稼働 - (当直 - 所定労働)
					$arr_effective_time = array_diff($arr_effective_time, array_diff($night_duty_time_array, $arr_time2));
				}

				// 普外時間を算出（分単位）
				// 外出時間を差し引かないフラグ対応 20110727
				if ($out_time_nosubtract_flag != "1") {
					$arr_out_time = $timecard_common_class->get_minute_time_array($out_date_time, $ret_date_time);
					$time3 = count(array_intersect($arr_out_time, $arr_specified_time));

					// 残外時間を算出（分単位）
					$time4 = count(array_diff($arr_out_time, $arr_time2)); //$arr_specified_timeから変更、休憩も除くため 20100910
				}
				// 実働時間を算出（分単位）
				$effective_time = count($arr_effective_time);
				$time3_rest = 0;
				$time4_rest = 0;
				if ($wage == "4" && $start4 == "" && $end4 == "") {  // 時間給かつ休憩時刻指定がない場合 20100925
					if ($effective_time > 1200) {  // 20時間を超える場合 20100929
						$effective_time -= $rest7;
						$time3_rest = 0;
						$time4_rest = $rest7;
					} else if ($effective_time > 720) {  // 12時間を超える場合
						$effective_time -= $rest6;
						$time3_rest = 0;
						$time4_rest = $rest6;
					} else if ($effective_time > 540) {  // 9時間を超える場合
						$effective_time -= $rest3;
						$time3_rest = 0;
						$time4_rest = $rest3;
					} else if ($effective_time > 480) {  // 8時間を超える場合
						$effective_time -= $rest2;
						$time3_rest = $rest2;
						$time4_rest = 0;
					} else if ($effective_time > 360) {  // 6時間を超える場合
						$effective_time -= $rest1;
						$time3_rest = $rest1;
						$time4_rest = 0;
					} else if ($effective_time > 240 && $start_time < "12:00") {  // 4時間超え午前開始の場合
						$effective_time -= $rest4;
						$time3_rest = $rest4;
						$time4_rest = 0;
					} else if ($effective_time > 240 && $start_time >= "12:00") {  // 4時間超え午後開始の場合
						$effective_time -= $rest5;
						$time3_rest = $rest5;
						$time4_rest = 0;
					}
					//休日残業で残業申請中は休憩を表示しない 20100916
					if ($legal_hol_over_flg /*&& $overtime_approve_flg == "1"*/) {
						$time3_rest = 0;
						$time4_rest = 0;
					}
					//休日で残業がない場合,出勤退勤がある場合 20100917
					if ($pattern == "10" && $over_start_time == "" && $over_end_time == "") {
						$time3_rest = 0;
						$time4_rest = 0;
					}
                    //開始した日が休日の場合に休日勤務から休憩を減算 2008/10/23
					//if (($previous_day_flag == true && $arr_hol_time[0] > 0) ||
					//		($previous_day_flag == false && $arr_hol_time[1] > 0)) {
					//	$sums[31] = $sums[31] - $time3_rest - $time4_rest;
					//}
				}

				//古い仕様のため廃止 20100330
				/*
				// 代替出勤または振替出勤の場合、実働時間から所定労働時間を減算
				if ($reason == "14" || $reason == "15") {
					$effective_time -= count($arr_specified_time);
				}
				*/
				// 稼働時間を算出（分単位）
				//$work_time = $effective_time + $paid_time - $time3 - $time4;
				// 半年休の時間をプラスしない 20100330
				$work_time = $effective_time - $time3 - $time4;
				//休暇時は計算しない 20090806 公休か事由なし休暇で残業時刻が設定済を条件追加 20100802
				if ($pattern == "10" && !$legal_hol_over_flg) {
					$work_time = 0;
				}

				// 退勤後復帰回数・時間を算出
				$return_count = 0;
				$return_time = 0;
				$return_late_time = 0; //20100715
                //申請状態 "0":申請不要 "3":承認済 //勤務時間を計算しないフラグ確認 20130225
                if (($return_link_type == "0" || $return_link_type == "3") && $no_count_flag != "1") {
					for ($i = 1; $i <= 10; $i++) {
						$start_var = "o_start_time$i";
						if ($$start_var == "") {
							break;
						}
						$return_count++;

						$end_var = "o_end_time$i";
						if ($$end_var == "") {
							break;
						}

						//端数処理 20090709
						if ($fraction1 > "1") {
							//端数処理する分
							$moving_minutes = $timecard_common_class->get_moving_minutes($fraction1_min);
							//開始日時
							$tmp_start_date_time = $timecard_common_class->get_schedule_end_date_time(substr($end_date_time,0,8), substr($end_date_time,8,4), $$start_var);
							//基準日時
							$tmp_base_date_time = substr($tmp_start_date_time,0,10)."00";
							$tmp_date_time = date_utils::move_time($tmp_base_date_time, $tmp_start_date_time, $moving_minutes, $fraction1);
							$tmp_ret_start_time = substr($tmp_date_time, 8, 4);
						} else {
							$tmp_ret_start_time = str_replace(":", "", $$start_var);
						}
						if ($fraction2 > "1") {
							//端数処理する分
							$moving_minutes = $timecard_common_class->get_moving_minutes($fraction2_min);
							//終了日時
							$tmp_end_date_time = $timecard_common_class->get_schedule_end_date_time(substr($end_date_time,0,8), substr($end_date_time,8,4), $$end_var);
							//基準日時
							$tmp_base_date_time = substr($tmp_end_date_time,0,10)."00";
							$tmp_date_time = date_utils::move_time($tmp_base_date_time, $tmp_end_date_time, $moving_minutes, $fraction2);

							$tmp_ret_end_time = substr($tmp_date_time, 8, 4);
						} else {
							$tmp_ret_end_time = str_replace(":", "", $$end_var);
						}
						$arr_ret = $timecard_common_class->get_return_array($end_date_time, $tmp_ret_start_time, $tmp_ret_end_time);

						//					$arr_ret = $timecard_common_class->get_return_array($end_date_time, $$start_var, $$end_var);
						$return_time += count($arr_ret);

						// 月の深夜勤務時間に退勤後復帰の深夜分を追加
						//$return_late_time += count(array_intersect($arr_ret, $arr_time5)); //20100715
						//計算方法変更 20110126
						//深夜残業
						$wk_ret_start = $tmp_date.$tmp_ret_start_time;
						$wk_ret_end = $timecard_common_class->get_schedule_end_date_time($tmp_date, $tmp_ret_start_time, $tmp_ret_end_time);
						$wk_late_night = $timecard_common_class->get_late_night($work_times_info, $wk_ret_start,$wk_ret_end, "");
						$return_late_time += $wk_late_night;
						//当日が休日の場合、休日に合計。
						if (($pattern != "10" && check_timecard_holwk(
										$arr_timecard_holwk_day, $arr_type[$tmp_date], $pattern, $reason, "") == true) ||
								$reason == "16" ||
								$pattern == "10") {
							//当日の残業
							$today_over = $timecard_common_class->get_day_time($work_times_info, $wk_ret_start,$wk_ret_end, "1");
							$sums[31] += $today_over;
						}
						//翌日の残業
						$next_date = next_date($tmp_date);
						if (check_timecard_holwk(
									$arr_timecard_holwk_day, $arr_type[$next_date], $pattern, $reason, "1") == true) {
							$nextday_over = $timecard_common_class->get_day_time($work_times_info, $wk_ret_start,$wk_ret_end, "2");
							$sums[31] += $nextday_over;
						}
					}
				}

				$wk_return_time = $return_time;
				//残業時刻が入力された場合、重複するため、退勤後復帰時間を加算しない。 20100714 不要 20100910
				//if ($over_calc_flg) {
				//	$wk_return_time = 0; //集計用 $return_time 表示用
				//	$return_late_time = 0; //20100715
				//}
				$return_time_total += $return_time; // 20100721

				// 前日までの計 = 週計
				$last_day_total = $work_time_for_week;
				$work_time_for_week += $work_time;
				$work_time_for_week += $wk_return_time;
				if ($tmp_date < $start_date) {
					$tmp_date = next_date($tmp_date);
					$counter_for_week++;
					continue;
				}
				//$sums[13] += $work_time; // 20100910

				//当直の場合稼働時間に日数換算の数値ｘ通常勤務日の労働時間の時間を追加（当直の時間を除外して計算している為)
				//if ($night_duty_flag){
				//	$sums[13] += $duty_work_time;
				//}

				// 深夜勤務時間を算出（分単位）
				$time2 = 0;
				//計算方法変更 20110126
				//残業承認時に計算、申請画面を表示しない場合は無条件に計算
				if (($timecard_bean->over_time_apply_type != "0" && $overtime_approve_flg == "2") ||
						$timecard_bean->over_time_apply_type == "0") {
					//残業時刻がない場合で、所定終了、退勤を比較後残業計算
					if ($over_start_time == "" || $over_end_time == "") {
                        //if ($end2_date_time < $fixed_end_time) { //退勤時刻を使用しない 20120406
						//	$over_start_date_time = $end2_date_time;
						//	$over_end_date_time = $fixed_end_time;
						//} else {
							$over_start_date_time = ""; //20101109
							$over_end_date_time = "";
						//}
					}
                    //残業時間データ
                    $next_date = next_date($tmp_date); //20140303 残業時間集計不具合対応、翌日が休暇の場合
                    //休憩除外 20140702
                    $rest_start_time1 = $arr_atdbkrslt[$tmp_date]["rest_start_time1"];
                    $rest_end_time1 = $arr_atdbkrslt[$tmp_date]["rest_end_time1"];
                    if ($rest_start_time1 != "" && $rest_end_time1 != "") {
                        $rest_start_date1 = ($arr_atdbkrslt[$tmp_date]["rest_start_next_day_flag1"] == 1) ? next_date($tmp_date) :$tmp_date;
                        $rest_end_date1 = ($arr_atdbkrslt[$tmp_date]["rest_end_next_day_flag1"] == 1) ? next_date($tmp_date) :$tmp_date;
                        $rest_start_date_time1 = $rest_start_date1.$rest_start_time1;
                        $rest_end_date_time1 = $rest_end_date1.$rest_end_time1;
                    }
                    else {
                        $rest_start_date_time1 = "";
                        $rest_end_date_time1 = "";
                    }
                    $arr_overtime_data = get_overtime_data($work_times_info, $tmp_date, $pattern, $reason, $arr_timecard_holwk_day, $arr_type, $timecard_common_class, $over_start_date_time, $over_end_date_time, $arr_atdbkrslt[$next_date]["pattern"], $arr_atdbkrslt[$next_date]["reason"], $timecard_bean, $ovtm_class, $rest_start_date_time1, $rest_end_date_time1, $end2_date_time); //20130308
                    //深夜残業
                    $time2 = $arr_overtime_data["late_night"] + $arr_overtime_data["hol_late_night"];
                    /*
					$time2 = $timecard_common_class->get_late_night($work_times_info, $over_start_date_time,$over_end_date_time, "");
                    //休憩対応 20130827
                    if ($time2 > 0 && $start4 != "" && $end4 != "") {
                        $wk_rest_time = $timecard_common_class->get_intersect_times($over_start_date_time, $over_end_date_time, $start4_date_time, $end4_date_time);
                        if ($wk_rest_time > 0) {
                            $late_night_info = $timecard_common_class->get_late_night_shift_info($tmp_date, $start5, $end5);  // 深夜帯情報
                            //深夜時間帯か再確認
                            $wk_s1 = max($over_start_date_time, $start4_date_time);
                            $wk_e1 = min($over_end_date_time, $end4_date_time);
                            $wk_rest_time = $timecard_common_class->get_times_info_calc($late_night_info, $wk_s1, $wk_e1);
                            $time2 -= $wk_rest_time;
                        }
                    }
*/  
/*
                    //残業２
					if ($over_start_time2 != "" && $over_end_time2 != "") {
						//開始日時
						$over_start_date_time2 = ($over_start_next_day_flag2 == 1) ? next_date($tmp_date).$over_start_time2 : $tmp_date.$over_start_time2;
						//終了日時
						$over_end_date_time2 = ($over_end_next_day_flag2 == 1) ? next_date($tmp_date).$over_end_time2 : $tmp_date.$over_end_time2;
						//深夜時間帯の時間取得
						$over_time2_late_night = $timecard_common_class->get_late_night($work_times_info, $over_start_date_time2, $over_end_date_time2, "");
						$time2 += $over_time2_late_night;
					}
                    */
                    //残業３−５を追加、２−５の繰り返し処理とする
					//残業２
                    for ($idx=2; $idx<=5; $idx++) {
                        $s_t = "over_start_time".$idx;
                        $e_t = "over_end_time".$idx;
                        $s_f = "over_start_next_day_flag".$idx;
                        $e_f = "over_end_next_day_flag".$idx;
                        $over_start_time_value = $arr_atdbkrslt[$tmp_date][$s_t];
                        $over_end_time_value = $arr_atdbkrslt[$tmp_date][$e_t];
                        $over_start_next_day_flag_value = $arr_atdbkrslt[$tmp_date][$s_f];
                        $over_end_next_day_flag_value = $arr_atdbkrslt[$tmp_date][$e_f];
                        
                        if ($over_start_time_value != "" && $over_end_time_value != "") {
                            //開始日時
                            $over_start_date_time_value = ($over_start_next_day_flag_value == 1) ? next_date($tmp_date).$over_start_time_value : $tmp_date.$over_start_time_value;
                            //終了日時
                            $over_end_date_time_value = ($over_end_next_day_flag_value == 1) ? next_date($tmp_date).$over_end_time_value : $tmp_date.$over_end_time_value;
                            $rs_t = "rest_start_time".$idx;
                            $re_t = "rest_end_time".$idx;
                            $rs_f = "rest_start_next_day_flag".$idx;
                            $re_f = "rest_end_next_day_flag".$idx;
                            $rest_start_time_value = $arr_atdbkrslt[$tmp_date][$rs_t];
                            $rest_end_time_value = $arr_atdbkrslt[$tmp_date][$re_t];
                            $rest_start_next_day_flag_value = $arr_atdbkrslt[$tmp_date][$rs_f];
                            $rest_end_next_day_flag_value = $arr_atdbkrslt[$tmp_date][$re_f];
                            
                            //残業時間深夜内休憩
                            $overtime_rest_night = 0;
                            
                            if ($rest_start_time_value != "" && $rest_end_time_value != "") {
                                $rest_start_date_value = ($arr_atdbkrslt[$tmp_date][$rs_f] == 1) ? next_date($tmp_date) :$tmp_date;
                                $rest_end_date_value = ($arr_atdbkrslt[$tmp_date][$re_f] == 1) ? next_date($tmp_date) :$tmp_date;
                                $rest_start_date_time_value = $rest_start_date_value.$rest_start_time_value;
                                $rest_end_date_time_value = $rest_end_date_value.$rest_end_time_value;
                            }
                            else {
                                $rest_start_date_time_value = "";
                                $rest_end_date_time_value = "";
                            }
                            
                            //echo "$tmp_date over_start_date_time_value=$over_start_date_time_value  over_end_date_time_value=$over_end_date_time_value rest_start_date_time_value=$rest_start_date_time_value rest_end_date_time_value=$rest_end_date_time_value <br>\n";
                            //残業時間データ
                            $arr_overtime_data2 = get_overtime_data($work_times_info, $tmp_date, $pattern, $reason, $arr_timecard_holwk_day, $arr_type, $timecard_common_class, $over_start_date_time_value, $over_end_date_time_value, $arr_atdbkrslt[$next_date]["pattern"], $arr_atdbkrslt[$next_date]["reason"], $timecard_bean, $ovtm_class, $rest_start_date_time_value, $rest_end_date_time_value, ""); //20130308
                            
                            //普通残業
                            $normal_over += $arr_overtime_data2["normal_over"];
                            //深夜
                            $late_night += $arr_overtime_data2["late_night"];
                            //休日残業
                            $hol_over += $arr_overtime_data2["hol_over"];
                            //休日深夜
                            $hol_late_night += $arr_overtime_data2["hol_late_night"];
                            
                            //深夜時間帯の時間取得
                            $over_time2_late_night_old = $timecard_common_class->get_late_night($work_times_info, $over_start_date_time_value, $over_end_date_time_value, "");
                            $over_time2_late_night = $arr_overtime_data2["late_night"] + $arr_overtime_data2["hol_late_night"];
                            //                            echo "$tmp_date over_time2_late_night_old=$over_time2_late_night_old  over_time2_late_night=$over_time2_late_night <br>\n";
                            $time2 += $over_time2_late_night;
                        }
                    }
				}
				$time2 += $return_late_time; //20100715
				$sums[15] += $time2;


				// 月の稼働時間・残業時間に退勤後復帰分の残業時間を追加
				//$sums[13] += $wk_return_time; //20100714 20100910
				//				$sums[14] += $return_time;

				// 表示値の編集
				$time1 = minute_to_hmm($time1);
				$time2 = minute_to_hmm($time2);
				// 普外−＞外出に変更 20090603
				//$time3 = minute_to_hmm($time3 + $time3_rest);
				$time_outtime = minute_to_hmm($time3);
				if ($time4 > 0) {
					$time_outtime .= "(".minute_to_hmm($time4).")";
				}
				// 残外−＞休憩に変更 20090603
				if ($wage == "4" && $start4 == "" && $end4 == "") {  // 時間給かつ休憩時刻指定がない場合 20100925
					$time_rest = minute_to_hmm($time3_rest);
					$rest_time_total += $time3_rest;
					if ($time4_rest > 0) {
						$time_rest .= "(".minute_to_hmm($time4_rest).")";
						$rest_time_total += $time4_rest;
					}
				} else /*if ($wage == "1" || $wage == "3")*/ {
					// 実働と休憩の配列から休憩時間を算出
					// 所定労働内の休憩時間
					$tmp_rest1 = count(array_intersect($arr_time2, $arr_time4));
					// 実働時間内の休憩時間
                    $tmp_rest2 = count(array_intersect($arr_effective_time_wk, $arr_time4));
					// 所定労働外（残業時間内）の休憩時間
					$tmp_rest3 = $tmp_rest2 - $tmp_rest1;
                    $time_rest = minute_to_hmm($tmp_rest1);
                    if ($tmp_rest3 > 0) {
						$time_rest .= "(".minute_to_hmm($tmp_rest3).")";
						$rest_time_total += $tmp_rest3;
					}
					//休憩時間前後の遅刻早退対応 20100925
					if ($tmp_rest1 > $tmp_rest2) {
						$tmp_rest2 = $tmp_rest1;
						$tmp_rest1 = 0;
						$time_rest = "";
					}
					$rest_time_total += $tmp_rest1;
                    
//				} else {
//					$time_rest = "";
				}
				$return_time = minute_to_hmm($return_time);
                // 回数追加 20121109
                if ($return_count != "") {
                    $return_time .= " ".$return_count;
                    $return_count_total += $return_count;
                }
            } else {
				// 出勤同時刻退勤または、出勤時間＞退勤時間の場合で
				// 月の開始日前は処理しない
				if ($tmp_date < $start_date) {
					$tmp_date = next_date($tmp_date);
					$counter_for_week++;
					continue;
				}
			}
		} else {
			$time1 = "";
			$time2 = "";
			$time3 = "";
			$time4 = "";
			$time_outtime = "";
			$time_rest = "";
			$return_time = "";

			// 事由が「代替休暇」「振替休暇」の場合、
			// 所定労働時間を稼働時間にプラス
			// 「有給休暇」を除く $reason == "1" || 2008/10/14
			if ($reason == "4" || $reason == "17") {
				if ($tmp_date >= $start_date) {
					$sums[13] += $specified_time;
					$work_time_for_week += $specified_time;
				}
			}
			// 退勤後復帰回数を算出
			$return_count = 0;
			// 退勤後復帰時間を算出 20100413
			$return_time = 0;
            //申請状態 "0":申請不要 "3":承認済 ※未承認時は計算しない //勤務時間を計算しないフラグ確認 20130225
			if (($return_link_type == "0" || $return_link_type == "3") && $no_count_flag != "1") {
				for ($i = 1; $i <= 10; $i++) {
					$start_var = "o_start_time$i";
					if ($$start_var == "") {
						break;
					}
					$return_count++;

					$end_var = "o_end_time$i";
					if ($$end_var == "") {
						break;
					}
					//端数処理
					if ($fraction1 > "1") {
						//端数処理する分
						$moving_minutes = $timecard_common_class->get_moving_minutes($fraction1_min);
						//開始日時
						$tmp_start_date_time = $tmp_date.$$start_var;
						//基準日時
						$tmp_base_date_time = substr($tmp_start_date_time,0,10)."00";
						$tmp_date_time = date_utils::move_time($tmp_base_date_time, $tmp_start_date_time, $moving_minutes, $fraction1);
						$tmp_ret_start_time = str_replace(":", "",substr($tmp_date_time, 8, 5));
					} else {
						$tmp_ret_start_time = str_replace(":", "", $$start_var);
					}
					if ($fraction2 > "1") {
						//端数処理する分
						$moving_minutes = $timecard_common_class->get_moving_minutes($fraction2_min);
						//終了日時
						$tmp_end_date_time = $tmp_date.$$end_var;
						//基準日時
						$tmp_base_date_time = substr($tmp_end_date_time,0,10)."00";
						$tmp_date_time = date_utils::move_time($tmp_base_date_time, $tmp_end_date_time, $moving_minutes, $fraction2);

						$tmp_ret_end_time = str_replace(":", "",substr($tmp_date_time, 8, 5));
					} else {
						$tmp_ret_end_time = str_replace(":", "", $$end_var);
					}
					$arr_ret = $timecard_common_class->get_return_array($tmp_date.$tmp_ret_start_time, $tmp_ret_start_time, $tmp_ret_end_time);

					$return_time += count($arr_ret);

					//前日・翌日の深夜帯
					//$arr_time5 = $timecard_common_class->get_late_night_shift_array($tmp_date, $start5, $end5);  // 深夜帯
					// 月の深夜勤務時間に退勤後復帰の深夜分を追加
					//$sums[15] += count(array_intersect($arr_ret, $arr_time5));
				}
			}
			$wk_return_time = $return_time;
			//分を時分に変換
			$return_time = minute_to_hmm($return_time);
            // 回数追加 20121109
            if ($return_count != "") {
                $return_time .= " ".$return_count;
                $return_count_total += $return_count;
            }

			// 前日までの計 = 週計
			$last_day_total = $work_time_for_week;

			$work_time_for_week += $work_time;
			$work_time_for_week += $wk_return_time;

			if ($tmp_date < $start_date) {
				$tmp_date = next_date($tmp_date);
				$counter_for_week++;
				continue;
			}
		}

		$wk_hoteigai = 0;
		$wk_hoteinai_zan = 0;
		$wk_hoteinai = 0;

		//日数換算
		$total_workday_count = "";
		if (($start_time != "" && $end_time != "") || $after_night_duty_flag == "1") //明けを追加 20110819
		{
			//休暇時は計算しない 20090806
			if ($pattern != "10") {
				if($workday_count != "")
				{
					$total_workday_count = $workday_count;
				}
			}

			if($night_duty_flag)
			{
				//曜日に対応したworkday_countを取得する
				$total_workday_count = $total_workday_count + $timecard_common_class->get_night_workday_count($tmp_date, $end_time, $next_day_flag, $tmcd_group_id);

			}
		}
		else if ($workday_count == 0){
			//日数換算に0が指定された場合のみ0を入れる
			$total_workday_count = $workday_count;
		}

		//残業時間 20100209 変更 20100910
		$wk_zangyo = 0;
		$kinmu_time = 0;
		$over_rest_time_day = 0; //残業休憩日毎 20141209
		//出勤時刻、退勤時刻が両方入っている場合に計算する 20101004
        //勤務時間を計算しないフラグが"1"以外 20130225
        if ($start_time != "" && $end_time != "" && $no_count_flag != "1") {
            //残業承認時に計算、申請画面を表示しない場合は無条件に計算
			if (($timecard_bean->over_time_apply_type != "0" && $overtime_approve_flg == "2") ||
					$timecard_bean->over_time_apply_type == "0"
				) {

				//残業開始、終了時刻がある場合
				if ($over_start_time != "" && $over_end_time != "") {
					$wk_zangyo = date_utils::get_time_difference($over_end_date_time,$over_start_date_time);
                    //休憩除外 20140702
                    $rest_start_time1 = $arr_atdbkrslt[$tmp_date]["rest_start_time1"];
                    $rest_end_time1 = $arr_atdbkrslt[$tmp_date]["rest_end_time1"];
                    if ($rest_start_time1 != "" && $rest_end_time1 != "") {
                        $rest_start_date1 = ($arr_atdbkrslt[$tmp_date]["rest_start_next_day_flag1"] == 1) ? next_date($tmp_date) :$tmp_date;
                        $rest_end_date1 = ($arr_atdbkrslt[$tmp_date]["rest_end_next_day_flag1"] == 1) ? next_date($tmp_date) :$tmp_date;
                        $wk_rest_time1 = date_utils::get_time_difference($rest_end_date1.$rest_end_time1,$rest_start_date1.$rest_start_time1);
                        $wk_zangyo -= $wk_rest_time1;
                        //残業休憩日毎 20141209
                        $over_rest_time_day += $wk_rest_time1;
                    }
                    //残業時刻未設定は計算しない 20110825
				//} else {
				//	//残業時刻がない場合で、所定終了、退勤を比較後残業計算 20100913
				//	if ($end2_date_time < $fixed_end_time) {
				//		$wk_zangyo = date_utils::get_time_difference($fixed_end_time,$end2_date_time);
				//	}
				}
                //残業３−５を追加、２−５の繰り返し処理とする
                //残業２
                for ($idx=2; $idx<=5; $idx++) {
                    $s_t = "over_start_time".$idx;
                    $e_t = "over_end_time".$idx;
                    $s_f = "over_start_next_day_flag".$idx;
                    $e_f = "over_end_next_day_flag".$idx;
                    $over_start_time_value = $arr_atdbkrslt[$tmp_date][$s_t];
                    $over_end_time_value = $arr_atdbkrslt[$tmp_date][$e_t];
                    $over_start_next_day_flag_value = $arr_atdbkrslt[$tmp_date][$s_f];
                    $over_end_next_day_flag_value = $arr_atdbkrslt[$tmp_date][$e_f];
                    if ($over_start_time_value != "" && $over_end_time_value != "") {
                        //開始日時
                        $over_start_date_time_value = ($over_start_next_day_flag_value == 1) ? next_date($tmp_date).$over_start_time_value : $tmp_date.$over_start_time_value;
                        //終了日時
                        $over_end_date_time_value = ($over_end_next_day_flag_value == 1) ? next_date($tmp_date).$over_end_time_value : $tmp_date.$over_end_time_value;
                        $wk_over_time2 = date_utils::get_time_difference($over_end_date_time_value, $over_start_date_time_value);
                        //休憩除外 20140702
                        $rs_t = "rest_start_time".$idx;
                        $re_t = "rest_end_time".$idx;
                        $rs_f = "rest_start_next_day_flag".$idx;
                        $re_f = "rest_end_next_day_flag".$idx;
                        
                        $rest_start_time_value = $arr_atdbkrslt[$tmp_date][$rs_t];
                        $rest_end_time_value = $arr_atdbkrslt[$tmp_date][$re_t];
                        $rest_start_next_day_flag_value = $arr_atdbkrslt[$tmp_date][$rs_f];
                        $rest_end_next_day_flag_value = $arr_atdbkrslt[$tmp_date][$re_f];
                        if ($rest_start_time_value != "" && $rest_end_time_value != "") {
                            
                            $rest_start_date_value = ($arr_atdbkrslt[$tmp_date][$rs_f] == 1) ? next_date($tmp_date) :$tmp_date;
                            $rest_end_date_value = ($arr_atdbkrslt[$tmp_date][$re_f] == 1) ? next_date($tmp_date) :$tmp_date;
                            $rest_start_date_time_value = $rest_start_date_value.$rest_start_time_value;
                            $rest_end_date_time_value = $rest_end_date_value.$rest_end_time_value;
                            $wk_rest_over_time2 = date_utils::get_time_difference($rest_end_date_time_value, $rest_start_date_time_value);
                            $wk_over_time2 -= $wk_rest_over_time2;
	                        //残業休憩日毎 20141209
	                        $over_rest_time_day += $wk_rest_over_time2;
                        }
                        //echo "ov2 $tmp_date over_start_time_value=$over_start_time_value wk_over_time2=$wk_over_time2<br>";
                        $wk_zangyo += $wk_over_time2;
                        //休憩時間確認
                        //if ($start4 != "" && $end4 != "") {
                        //	$wk_kyukei = $timecard_common_class->get_intersect_times($over_start_date_time2, $over_end_date_time2, $start4_date_time, $end4_date_time);
                        //	$wk_zangyo -= $wk_kyukei;
                        
                        //}
                    }
                }
                /*
                //残業２
				if ($over_start_time2 != "" && $over_end_time2 != "") {
					$over_start_date_time2 = $tmp_date.$over_start_time2;
					if ($over_start_next_day_flag2 == 1) {
						$over_start_date_time2 = next_date($tmp_date).$over_start_time2;
					}
					$over_end_date_time2 = $tmp_date.$over_end_time2;
					if ($over_end_next_day_flag2 == 1) {
						$over_end_date_time2 = next_date($tmp_date).$over_end_time2;
					}

					$wk_zangyo2 = date_utils::get_time_difference($over_end_date_time2,$over_start_date_time2);
					$wk_zangyo += $wk_zangyo2;
				}
                */
				//早出残業
				if ($early_over_time != "") {
					$wk_early_over_time = hmm_to_minute($early_over_time);
					$wk_zangyo += $wk_early_over_time;
					$early_time_total += $wk_early_over_time;
				}
			}
			//呼出
			$wk_zangyo += $wk_return_time;

            //減算しない 20130822
			//if ($wage == "4" && $start4 == "" && $end4 == "") {  // 時間給かつ休憩時刻指定がない場合 20100925
			//	//休日残業時は、休憩減算 20100916
			//	if ($legal_hol_over_flg == true) {
			//		if ($wk_zangyo >= ($time3_rest + $time4_rest)) {
			//			$wk_zangyo -= ($time3_rest + $time4_rest);
			//		}
			//	}
			//}
			//休日残業で休憩時刻がある場合 20100921
            if (($legal_hol_over_flg == true)&& $tmp_rest2 > 0) {
				$wk_zangyo -= $tmp_rest2;
			}
			//出勤時刻と残業開始時刻が一致している場合、所定、遅刻、早退を計算せず、残業、休憩のみ計算する 20100917
			if ($start2 != "" && $start2 == $over_start_time && $over_start_next_day_flag != "1") {//残業開始が翌日でないことを追加 20101027
				$overtime_only_flg = true;
				//遅刻
				$tikoku_time = 0;
				$start_time_info["diff_minutes"] = 0;
				//早退
				$sotai_time = 0;
				$end_time_info["diff_minutes"] = 0;
				//残業承認時に計算、申請画面を表示しない場合は無条件に計算 20100922
				if (($timecard_bean->over_time_apply_type != "0" && $overtime_approve_flg == "2") ||
						$timecard_bean->over_time_apply_type == "0"
					) {
					$wk_zangyo -= $tmp_rest2;
				} else {
					$tmp_rest2 = 0;
					$time_rest = "";
				}
			} else {
				$overtime_only_flg = false;
                $paid_hol_min =  $arr_hol_hour_dat[$tmp_date]["calc_minute"];
                //時間有休と相殺しない場合で時間有休ある場合 20140807
                if ($ovtm_class->sousai_sinai_flg == "t" && $paid_hol_min != "") {
                    //遅刻
                    $tikoku_time = 0;
                    //早退
                    $sotai_time = 0;
                }
                else {
                    //遅刻
                    $tikoku_time = $start_time_info["diff_minutes"];
                    //早退
                    $sotai_time = $end_time_info["diff_minutes"];
                }
			}
			$tikoku_time2 = 0;
			// 遅刻時間を残業時間から減算する場合
			if ($timecard_bean->delay_overtime_flg == "1") {
				if ($wk_zangyo >= $tikoku_time) { //20100913
					$wk_zangyo -= $tikoku_time;

				} else { // 残業時間より多い遅刻時間対応 20100925
					$tikoku_time2 = $tikoku_time - $wk_zangyo;
					$wk_zangyo = 0;
				}
			}

			//勤務時間＝所定労働時間−休憩時間−外出時間−早退−遅刻＋残業時間 20100907
			//　残業時間＝承認済み残業時間＋承認済み呼出し勤務時間
			//　承認済み残業時間＝早出残業時間＋勤務終了後残業時間
			//所定
			if ($pattern == "10" || //休暇は所定を0とする 20100916
					$overtime_only_flg == true ||	//残業のみ計算する 20100917
					$after_night_duty_flag == "1"  //明けの場合 20110819
				) {
				$shotei_time = 0;
//			} elseif ($wage == "4" && $start4 == "" && $end4 == "") {  // 時間給かつ休憩時刻指定がない場合 20100925 不具合対応 20101014
//				$shotei_time = count($arr_effective_time_wk); //休憩を引く前の実働時間
			} elseif ($start2 == "" && $end2 == "") {  // 所定時刻指定がない場合、端数処理不具合対応 20101005
				$shotei_time = count($arr_effective_time_wk); //休憩を引く前の実働時間
			} else {
				$shotei_time = count($arr_time2);
			}
            $paid_hol_min =  $arr_hol_hour_dat[$tmp_date]["calc_minute"];
            if ($obj_hol_hour->paid_hol_hour_flag == "t" && $paid_hol_min != "") {
                //時間有休と相殺しない場合
                if ($ovtm_class->sousai_sinai_flg == "t") {
                    //遅刻早退が時間有休より多い場合、その分は勤務時間から減算されるようにする
                    if ($start_time_info["diff_minutes"] != "") {
                        if ($start_time_info["diff_minutes"] >= $paid_hol_min) {
                            $shotei_time -= $start_time_info["diff_minutes"];
                        }
                        else {
                            $shotei_time -= $paid_hol_min;
                        }
                    }
                    if ($end_time_info["diff_minutes"] != "") {
                        if ($end_time_info["diff_minutes"] >= $paid_hol_min) {
                            $shotei_time -= $end_time_info["diff_minutes"];
                        }
                        else {
                            $shotei_time -= $paid_hol_min;
                        }
                    }
                }
                else {   
                    $shotei_time -= $paid_hol_min;
                }
            }
            //休憩 $tmp_rest2 $time3_rest $time4_rest
			//外出 $time3 $time4
			$kinmu_time = $shotei_time - $time3 - $time4 + $wk_zangyo;
			// 遅刻時間を残業時間から減算しない場合
			if ($timecard_bean->delay_overtime_flg == "2") {
				$kinmu_time -= $tikoku_time;
			}
			// 遅刻時間を残業時間から減算する場合で、残業未承認の場合で遅刻がある場合は引く 20100914
			else {
				if ($overtime_approve_flg == "1") {
					$kinmu_time -= $tikoku_time;
				} else { //遅刻時間不具合対応 20101005
					// 残業時間より多い遅刻時間対応 20100925
					$kinmu_time -= $tikoku_time2;
				}
			}
			// 早退時間を残業時間から減算しない場合
			if ($timecard_bean->early_leave_time_flg == "2") {
				$kinmu_time -= $sotai_time;
			}
			//時間給者 20100915
			if ($wage == "4" && $start4 == "" && $end4 == "") {  // 時間給かつ休憩時刻指定がない場合 20100925
				//休日残業時は、休憩を減算しない 20100916
                //if ($legal_hol_over_flg != true) { //コメント化20130822
					$kinmu_time -= ($time3_rest + $time4_rest);
				//}
			} else {
				if ($overtime_only_flg != true && $legal_hol_over_flg != true) {	//残業のみ計算する場合と休日残業の場合、残業から引くためここでは引かない 20100921
					//月給制日給制、実働時間内の休憩時間
					$kinmu_time -= $tmp_rest2;
				}
			}

			//法定内、法定外残業の計算
			if ($wk_zangyo > 0) {
				//法定内入力有無確認
				if ($legal_in_over_time != "") {
					$wk_hoteinai_zan = hmm_to_minute($legal_in_over_time);
					if ($wk_hoteinai_zan <= $wk_zangyo) {
						$wk_hoteigai = $wk_zangyo - $wk_hoteinai_zan;
					} else {
						$wk_hoteigai = 0;
					}

				} else {
					//法定外残業の基準時間を超える分を法定外残業とする
					if ($kinmu_time > $wk_base_time) {
						$wk_hoteigai = $kinmu_time - $wk_base_time;
					}
					//
					if ($wk_zangyo - $wk_hoteigai > 0) {
						$wk_hoteinai_zan = $wk_zangyo - $wk_hoteigai;
					} else {
						//マイナスになる場合
						$wk_hoteinai_zan = 0;
						$wk_hoteigai = $wk_zangyo;
					}
				}
			}
			//合計
			//法定内
			$wk_hoteinai = $kinmu_time - $wk_zangyo;
			$hoteinai += $wk_hoteinai;
			//法定内残業
			$hoteinai_zan += $wk_hoteinai_zan;
			//法定外残
			$hoteigai += $wk_hoteigai;
			//勤務時間
			$sums[13] += $kinmu_time;
			//残業時間
			$sums[14] += $wk_zangyo;
			//残業休憩 20141209
			$over_rest_time_total += $over_rest_time_day;
		}
//comment化
if ($dummy == "1") {
		 //残業承認時に計算、申請画面を表示しない場合は無条件に計算 20100608
		if (($timecard_bean->over_time_apply_type != "0" && $overtime_approve_flg == "2") ||
				$timecard_bean->over_time_apply_type == "0"
				|| $wk_return_time > 0 // 退勤後復帰がある場合 20100714
				|| $legal_hol_over_flg) { // 公休か残業時刻ありを条件追加 20100802
			//休暇時は計算しない 20090806
			if ($pattern != "10" ||
					$legal_hol_over_flg) { // 公休で残業時刻ありを条件追加 20100802

				// 計算用の稼働時間
				//			$work_time_calc = $work_time;
				// 遅刻時間を残業時間から減算しない場合、計算用の稼働時間に
				//			if ($delay_overtime_flg == "2") {
				//				$work_time_calc = $work_time + $start_time_info["diff_minutes"];
				//			}

				//法定内勤務、法定内残業、法定外残業
				// 前日までの累計 >= 40時間
				//法定外残業で週４０時間超過分 1:残業時間として計算するの場合
				if ($last_day_total >= 40 * 60 && $timecard_bean->overtime_40_flg == "1") {
					//  法定外 += 稼動時間
					//下の処理で合計するためここはコメント化
					//			$hoteigai += $work_time + $wk_return_time;
				} else if ($work_time + $wk_return_time > 0){
					// 上記以外、かつ、 稼動時間がある場合

					//法定内勤務と法定外残業
					//法定内勤務のうち週40時間を超える分から法定外残業へ移動
					//wk法定外
					//残業時刻から計算 20100525
					if ($over_calc_flg) {
						/* 20100713
						// 端数処理する
						if ($fraction2 > "1") {
							//端数処理する分
							$moving_minutes = $timecard_common_class->get_moving_minutes($fraction2_min);
							//基準日時
							if ($end2_date_time != "") {
								$tmp_office_start_time = substr($end2_date_time, 0, 10) . "00";
							} else {
								$tmp_office_start_time = substr($start_date_time, 0, 10) . "00";
							}
							$tmp_over_end_date_time = date_utils::move_time($tmp_office_start_time, $over_end_date_time, $moving_minutes, $fraction2);
						} else {
							*/
							$tmp_over_end_date_time = $over_end_date_time;
						//}
						$arr_over_time = $timecard_common_class->get_minute_time_array($over_start_date_time,$tmp_over_end_date_time);
						//20100712
						$wk_hoteigai = count(array_diff($arr_over_time, $arr_time4));
						$wk_hoteigai_org = $wk_hoteigai;
						if ($wk_hoteigai <= $effective_time) {
							$wk_hoteigai = $effective_time - $wk_base_time;
							if ($wk_hoteigai < 0) {
								$wk_hoteigai = 0;
							}
						}
						//法定内残業分を除く 20100712 20100714
						//$wk_jitsudo = ($effective_time > $wk_base_time) ? $wk_base_time : $effective_time;
						//$wk_hoteinai_bun = $wk_jitsudo - $specified_time;
						//if ($wk_hoteinai_bun > 0 && $wk_hoteigai > $wk_hoteinai_bun) {
						//	$wk_hoteigai -= $wk_hoteinai_bun;
						//}
						// 所定労働内の休憩時間 20100706
						$tmp_rest1 = count(array_intersect(array_diff($arr_time2, $arr_over_time), $arr_time4));
						// 所定労働外（残業時間内）の休憩時間
						$tmp_rest3 = count(array_intersect($arr_over_time, $arr_time4));
						$time_rest = minute_to_hmm($tmp_rest1);
						if ($tmp_rest3 > 0) {
							$time_rest .= "(".minute_to_hmm($tmp_rest3).")";
						}

					}else {
						$wk_hoteigai = $work_time + $wk_return_time - $wk_base_time;
					}
					if ($wk_hoteigai > 0) {
						//wk法定内 = (稼動時間 - wk法定外)
						$wk_hoteinai = ($work_time + $wk_return_time - $wk_hoteigai);

						// 40時間を超えた法定外を除外する
						if ($timecard_bean->overtime_40_flg == "1") {
							$wk_jogai = $last_day_total + $work_time + $wk_return_time - (40 * 60);
							if ($wk_jogai > 0) {
								$wk_hoteigai -= $wk_jogai;
							}
						}

						//$arr_week_hoteigai[$counter_for_week] = $wk_hoteigai;
					} else {
						//wk法定内 = (稼動時間)
						$wk_hoteinai = ($work_time + $wk_return_time);

						//マイナスを0とする
						$wk_hoteigai = 0;
					}
					//echo("$tmp_date n=$wk_hoteinai g=$wk_hoteigai "); //debug
					//前日までの累計 ＋ wk法定内　＞ 40時間 の場合
					if ($last_day_total + $wk_hoteinai > 40 * 60) {
						if ($timecard_bean->overtime_40_flg == "1") {
							$wk_hoteigai = 0;
							//法定内 += (40時間 - 前日までの累計) 20090722 変更
							$hoteinai += (40 * 60) - $last_day_total;
						} else {
							//法定内 += wk法定内 20090722 変更
							$hoteinai += $wk_hoteinai;
						}


						//$arr_week_hoteigai[$counter_for_week] = $wk_hoteigai;
					} else {
						//上記以外
						//法定内 += wk法定内
						$hoteinai += $wk_hoteinai;
						//echo("<br>2 $tmp_date hoteinai=$hoteinai");

					}

					//echo("$tmp_date w=$wk_week_hoteigai g=$wk_hoteigai "); //debug
					// 法定内残業、所定労働時間から8時間まで間を加算
					// 稼動時間が所定労働時間より大きい場合
					//			$wk_minute = date_utils::hi_to_minute($day1_time);
					$wk_minute = $specified_time;
					// 時間帯の法定外残業の計算を使用する 480 -> $wk_base_time 20091218 20100713
					if (($wk_minute != 0 && $wk_minute < $wk_base_time && ($work_time + $wk_return_time) > $wk_minute) || $over_calc_flg) {

						//稼動時間が法定外残業の計算時間以上
						if (($work_time + $wk_return_time) >= $wk_base_time) {
							//基準法定内残業
							$wk_std_hoteinai_zan = $wk_base_time - $wk_minute;
						} else {
							//8時間未満の場合、稼動時間-所定労働時間
							$wk_std_hoteinai_zan = ($work_time + $wk_return_time) - $wk_minute;
							//debug
							if ($wk_std_hoteinai_zan < 0) {
								$wk_std_hoteinai_zan = 0;
							}
							//20100713
							//if ($over_calc_flg && $wk_hoteigai_org > ($wk_base_time - $specified_time)) {
							//	$wk_std_hoteinai_zan = $wk_hoteinai;
							//}
						}

						//法定外残業で週４０時間超過分 1:残業時間として計算するの場合
						if ($timecard_bean->overtime_40_flg == "1") {
							//前日までの累計 + 所定時間 ＜ 40時間 の場合
							if ($last_day_total + $wk_minute < 40 * 60) {
								//wk法定内残業 = 40時間 - (前日までの累計 + 所定時間)
								$wk_hoteinai_zan = 40 * 60 - ($last_day_total + $wk_minute);
								//wk法定内残業 > 基準法定内残業
								if ($wk_hoteinai_zan > $wk_std_hoteinai_zan) {
									// wk法定内残業 = 基準法定内残業
									$wk_hoteinai_zan = $wk_std_hoteinai_zan;
								}
								//法定内勤務 -= wk法定内残業
								$hoteinai -= $wk_hoteinai_zan;

								//法定内残業 += wk法定内残業
								$hoteinai_zan += $wk_hoteinai_zan;

								$sums[14] += $wk_hoteinai_zan; // 変更 20090722
								$wk_zangyo += $wk_hoteinai_zan;
							}
						} else {
							// wk法定内残業 = 基準法定内残業
							$wk_hoteinai_zan = $wk_std_hoteinai_zan;
							//法定内勤務 -= wk法定内残業
							$hoteinai -= $wk_hoteinai_zan;

							//法定内残業 += wk法定内残業
							$hoteinai_zan += $wk_hoteinai_zan;

							$sums[14] += $wk_hoteinai_zan; // 変更 20090722
							$wk_zangyo += $wk_hoteinai_zan;
						}
					}
					//echo("$tmp_date m=$wk_minute l=$last_day_total z=$wk_hoteinai_zan "); //debug
				}

			}

			//1日毎の法定外残を週単位に集計
			$wk_week_hoteigai += $wk_hoteigai;


			//法定外残業で週４０時間超過分 1:残業時間として計算するの場合
			if ($timecard_bean->overtime_40_flg == "1") {
				if ($counter_for_week == 7) {
					if ($arr_irrg_info["irrg_type"] == "1") {
						$tmp_irrg_minutes = $arr_irrg_info["irrg_minutes"];
					} else {
						$tmp_irrg_minutes = 40 * 60; // 変則労働期間以外の場合40時間
					}
					if ($hol_minus == "t") {
						$tmp_irrg_minutes -= (8 * 60 * $holiday_count);
					}
					$time1 = ($work_time_for_week > $tmp_irrg_minutes) ? $work_time_for_week - $tmp_irrg_minutes : 0;
					//				$sums[14] += $wk_week_hoteigai;
					$wk_hoteigai = $time1 + $wk_week_hoteigai;
					$hoteigai += $wk_hoteigai;

				} else {
					$wk_hoteigai = "";
				}
				//20090722 前日までの累計+稼働時間 >= 40時間、かつ、半日の場合、所定時間を基準とする
				if ($last_day_total + $work_time + $wk_return_time >= 40 * 60 && $workday_count == 0.5) {
					$wk_base_time = $specified_time;
				}
				$wk_hoteigai_yoteigai = $work_time + $wk_return_time - $wk_base_time;
				if ($wk_hoteigai_yoteigai > 0) {
					if ($others1 != "4") { //勤務時間内に外出した場合が、残業時間から減算するでない場合
						$wk_hoteigai_yoteigai += $time3;
					}
					if ($others2 != "4") { //勤務時間外に外出した場合が、残業時間から減算するでない場合
						$wk_hoteigai_yoteigai += $time4;
					}
					$sums[14] += $wk_hoteigai_yoteigai;
					$wk_zangyo += $wk_hoteigai_yoteigai;
				}
			} else {

				$time1 = 0;
				// 出退勤とも打刻済みの場合
				if ($start_time != "" && $end_time != "") {
					//				$tmp_specified_time = $specified_time;
					//				$tmp_specified_time = 480; //法定内8時間を所定として処理 20090717
					$tmp_specified_time = $wk_base_time; //日数換算対応 20090803
					// 遅刻時間を残業時間から減算しない場合（所定時間から引くことで合わせる）
					if ($timecard_bean->delay_overtime_flg == "2") {
						$tmp_specified_time = $tmp_specified_time - $start_time_info["diff_minutes"];
						if ($tmp_specified_time < 0) {
							$tmp_specified_time = 0;
						}
					}
					//休暇時は計算しない 20090806 公休か事由なし休暇で残業時刻が設定済を条件追加 20100802
					if ($pattern != "10" || $legal_hol_over_flg) {
						//残業時刻から計算 20100525
						if ($over_calc_flg) {
							//法定外 20100705
							$time1 = $wk_hoteigai;
							// 遅刻時間を残業時間から減算しない場合 20100910
							if ($timecard_bean->delay_overtime_flg == "2") {
								$time1 += $start_time_info["diff_minutes"];
							}
						} else {
							if ($effective_time > $tmp_specified_time) {
								$time1 = $effective_time - $tmp_specified_time;
							} else {
								$time1 = $wk_hoteigai; //20100714
							}
						}

						//集計処理の設定を「集計しない」の固定とする 20100910
						/*
						if ($others1 == "4") { //勤務時間内に外出した場合
							if ($time1 > $time3) {
								$time1 -= $time3;
							}
						}
						if ($others2 == "4") { //勤務時間外に外出した場合
							if ($time1 > $time4) {
								$time1 -= $time4;
							}
						}
						*/
					}
				}
				$wk_hoteigai = $time1; //20100714

				$sums[14] += $wk_hoteigai;
				$hoteigai += $wk_hoteigai;

				$wk_zangyo += $wk_hoteigai;
			}
			//法定内残業が入力された場合 20100531
			if ($legal_in_over_time != "") {
				//合計を戻す
				$sums[14] -= $wk_hoteinai_zan;
				$hoteinai_zan -= $wk_hoteinai_zan;
				$sums[14] -= $wk_hoteigai;
				$hoteigai -= $wk_hoteigai;

				$wk_hoteinai_zan = hmm_to_minute($legal_in_over_time);
				if ($wk_hoteinai_zan <= $wk_zangyo) {
					$wk_hoteigai = $wk_zangyo - $wk_hoteinai_zan;
				} else {
					$wk_hoteigai = 0;
				}
				//再度、合計する
				$sums[14] += $wk_hoteinai_zan;
				$hoteinai_zan += $wk_hoteinai_zan;
				$sums[14] += $wk_hoteigai;
				$hoteigai += $wk_hoteigai;

			}
			//早出残業が入力された場合 20100601
			if ($early_over_time != "") {
				//申請中の場合は計算しない	// 残業未承認か確認
				if (!($overtime_approve_flg == "1" || ($return_link_type != "0" && $return_link_type != "3"))) {

					$wk_early_over_time = hmm_to_minute($early_over_time);
					$work_time += $wk_early_over_time;
					$wk_zangyo += $wk_early_over_time;
					$wk_hoteinai_zan += $wk_early_over_time;
					//合計する
					$sums[13] += $wk_early_over_time;
					$sums[14] += $wk_early_over_time;
					$hoteinai_zan += $wk_early_over_time;
				}

			}
		}
} //comment化
		$time_cell_class = "tm";
		// 表示処理
		//表示用退避から設定 20100802
		$start_time = $disp_start_time;
		$end_time = $disp_end_time;

        //$enc_cur_url = urlencode("$fname?session=$session&emp_id=$emp_id&date=$tmp_date&yyyymm=$yyyymm&view=$view&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&page=$page&srch_id=$srch_id");
		$rowspan = ($view == "1") ? "1" : "2";
		$hspacer = "";
		// チェック対応、ハイライト 20110125
		//　残業未申請、残業申請中、退復未申請（呼出し未申請）・申請中、修正申請中
		//　打刻漏れ、遅刻、早退がある行をハイライトする。
		if ($check_flg == "1" && (
					($overtime_link_type == "1" &&
						$timecard_bean->over_apply_disp_flg == "t" && //残業申請状況の表示がするの場合 20110805
						(
							($timecard_bean->over_yet_apply_disp_min != "" && //未申請を表示する設定確認
							$diff_min_end_time != "" &&
							$diff_min_end_time >= $timecard_bean->over_yet_apply_disp_min)  ||
							($timecard_bean->over_yet_apply_disp_min == "" && //未申請を表示する設定の「分」が空白の場合 20110523
							$diff_min_end_time != "")) &&
						$no_overtime != "t" //残業管理をする場合
					) ||
					($overtime_link_type == "2" && $no_overtime != "t") ||
					$return_link_type == "1" ||
					$return_link_type == "2" ||
					$modify_link_type == "2" ||
					($tmp_date < $today &&
						($pattern != "10" && $workday_count > 0 &&
							$after_night_duty_flag != "1") && //明けの場合を除く 20110819
						($start_time == "" || $end_time == "") &&
						($o_start_time1 == "") //退復なし
						) || //打刻漏れチェックは当日より前の場合、出勤退勤の確認
					($tmp_date == $today &&
						($pattern != "10" && $workday_count > 0 &&
							$after_night_duty_flag != "1") && //明けの場合を除く 20110819
						($start_time == "") &&
						($o_start_time1 == "") //退復なし
						) || //打刻漏れチェックは当日の場合、出勤の確認
					$start_time_info["diff_minutes"] > 0 ||
					$end_time_info["diff_minutes"] > 0 ||
					($tmp_date <= $today &&
						($pattern == "") && //勤務パターン未設定
						($start_time == "" || $end_time == ""))
					|| (($pattern == "") && //時刻があるのに勤務パターン未設定 20120301
						($start_time != "" || $end_time != ""))
					|| ($pattern == "10" && //勤務パターンがあり、事由が未設定 20120301
						$reason == "")
                    || ($pattern == "10" && //勤務パターンが休暇で、出勤退勤時刻があるのに、残業時刻が入っていない場合 20120507
                        $start_time != "" && $end_time != "" &&
                        $over_start_time == "" && $over_end_time == "")
                    || ($tmp_date != $today && $start_time != "" && $end_time == "") //当日以外で出勤時刻のみ設定 20120720
                    || ($start_time == "" && $end_time != "") //退勤時刻のみ設定 20120720
                    ) ) {
			$bgcolor = "#ffff66";
		}
		// 背景色設定。法定、祝祭日を赤、所定を青にする
		else {
			$bgcolor = get_timecard_bgcolor($type);
		}
		// 2段表示時の背景。#f6f9ffから変更
		if ($bgcolor == "#ffffff") {
			$bgcolor2 = "#f6f9ff";
		} else {
			$bgcolor2 = $bgcolor;
		}
		// 締めデータがない場合
		//if (!$atdbk_closed) {
		if (true) {//常にこちらで表示。確認
			$tbl1 .= "<tr bgcolor=\"$bgcolor\">\n";
			$tbl1 .= "<td nowrap rowspan=\"$rowspan\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
			$tbl1 .= "{$tmp_month}月{$tmp_day}日";
			$tbl1 .= "</font><input type=\"hidden\" name=\"date[]\" value=\"$tmp_date\"></td>\n";
			//曜日
			$tbl1 .= "<td nowrap rowspan=\"$rowspan\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . get_weekday(to_timestamp($tmp_date)) . "$hspacer</font></td>\n";

			//種別
			if ($timecard_bean->type_display_flag == "t"){
				$tbl1 .= "<td nowrap rowspan=\"$rowspan\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($type_name) . "$hspacer</font></td>\n";
			}

			// 予実績表示の場合
			if ($view == "2") {

				// 勤務日種別と出勤パターン（予定）から所定労働時間帯を取得
				$prov_start2 = $timecard_common_class->get_officehours_info( $prov_tmcd_group_id, $prov_pattern, $tmp_type, "start2" );
				$prov_end2   = $timecard_common_class->get_officehours_info( $prov_tmcd_group_id, $prov_pattern, $tmp_type, "end2" );

                //個人別所定時間を優先する場合 20140318
                if ($arr_empcond_officehours_ptn[$prov_tmcd_group_id][$prov_pattern] == 1) {
                    $wk_weekday = $arr_weekday_flg[$tmp_date];
                    $arr_empcond_officehours = $timecard_common_class->get_empcond_officehours($wk_weekday);
                    if ($arr_empcond_officehours["officehours2_start"] != "") {
                        $prov_start2 = $arr_empcond_officehours["officehours2_start"];
                        $prov_end2 = $arr_empcond_officehours["officehours2_end"];
                    }
                }
                // 所定労働時間が確定できたら予定時刻として使用
				if ($prov_start2 != "" && $prov_end2 != "") {
					$prov_start_time = $prov_start2;
					$prov_end_time = $prov_end2;
				}

                //個人別所定時間を優先する場合 20120309
                if ($arr_empcond_officehours_ptn[$prov_tmcd_group_id][$prov_pattern] == 1) {
                    $wk_weekday = $arr_weekday_flg[$tmp_date];
                    $arr_empcond_officehours = $timecard_common_class->get_empcond_officehours($wk_weekday);
                    if ($arr_empcond_officehours["officehours2_start"] != "") {
                        $prov_start2 = $arr_empcond_officehours["officehours2_start"];
                        $prov_end2 = $arr_empcond_officehours["officehours2_end"];
                    }
                }

                // 表示値の編集
				if ($prov_pattern != "") {
					$prov_pattern = $atdbk_common_class->get_pattern_name($prov_tmcd_group_id, $prov_pattern);
				}
				if ($prov_reason != "") {
					$prov_reason = $atdbk_common_class->get_reason_name($prov_reason);
				}

				if($prov_night_duty == "1")
				{
					$prov_night_duty = "有り";
				}
				else if($prov_night_duty == "2")
				{
					$prov_night_duty = "無し";
				}

				$prov_allow_contents = "";
				if ($prov_allow_id != "")
				{
					foreach($arr_allowance as $allowance)
					{
						if($prov_allow_id == $allowance["allow_id"])
						{
							$prov_allow_contents = $allowance["allow_contents"];
							break;
						}
					}
				}

				//グループ名取得
				$prov_group_name = $atdbk_common_class->get_group_name($prov_tmcd_group_id);

				if ($timecard_bean->group_display_flag == "t"){
					$tbl1 .= "<td height=\"27\" bgcolor=\"$bgcolor2\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($prov_group_name) . "</font></td>\n";
				}

				$tbl1 .= "<td height=\"27\" bgcolor=\"$bgcolor2\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($prov_pattern) . "</font></td>\n";
				$tbl1 .= "<td bgcolor=\"$bgcolor2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($prov_reason) . "</font></td>\n";
				//前
				if ($timecard_bean->zen_display_flag == "t"){
					$tbl1 .= "<td bgcolor=\"$bgcolor2\" class=\"ml1\">&nbsp;</td>\n";
					$wk_class = "mc1";
				} else {
					//前の列がない場合、出勤時刻の列の左を太くする
					$wk_class = "ml1";
				}
				$tbl1 .= "<td bgcolor=\"$bgcolor2\" class=\"$wk_class\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">" . nbsp(hhmm_to_hmm($prov_start_time)) . "</font></td>\n";
				//翌
				if ($timecard_bean->yoku_display_flag == "t"){
					$tbl1 .= "<td bgcolor=\"$bgcolor2\" class=\"mc1\">&nbsp;</td>\n";
				}
				$tbl1 .= "<td bgcolor=\"$bgcolor2\" class=\"mr1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">" . nbsp(hhmm_to_hmm($prov_end_time)) . "</font></td>\n";
				$tbl1 .= "</tr>\n";
				$tbl1 .= "<tr bgcolor=\"$bgcolor\">\n";

				$tbl2 .= "<tr bgcolor=\"$bgcolor\" height=\"27\">\n";
				//残業
				$tbl2 .= "<td bgcolor=\"$bgcolor2\" class=\"ml2\">&nbsp;</td>\n";
				$tbl2 .= "<td bgcolor=\"$bgcolor2\" class=\"mc2\">&nbsp;</td>\n";
				$tbl2 .= "<td bgcolor=\"$bgcolor2\" class=\"mc2\">&nbsp;</td>\n";
				$tbl2 .= "<td bgcolor=\"$bgcolor2\" class=\"mc2\">&nbsp;</td>\n";
				//残業2
				$tbl2 .= "<td bgcolor=\"$bgcolor2\" class=\"mc2\">&nbsp;</td>\n";
				$tbl2 .= "<td bgcolor=\"$bgcolor2\" class=\"mc2\">&nbsp;</td>\n";
				$tbl2 .= "<td bgcolor=\"$bgcolor2\" class=\"mc2\">&nbsp;</td>\n";
				$tbl2 .= "<td bgcolor=\"$bgcolor2\" class=\"mr2\">&nbsp;</td>\n";
				//理由
				$tbl2 .= "<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n";
				//法定内残業
				if ($timecard_bean->all_legal_over_disp_flg != "f"){
					$tbl2 .= "<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n";
				}
				//残業申請状況の表示
				if ($timecard_bean->all_apply_disp_flg != "f") {
					//申請状態
					$tbl2 .= "<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n";
				}
				//外出
				$tbl2 .= "<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n";
				$tbl2 .= "<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n";
				//休憩時刻追加 20100921
				$tbl2 .= "<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n";
				$tbl2 .= "<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n";
				//復退
				if ($timecard_bean->ret_display_flag == "t"){
					$tbl2 .= "<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n";
					$tbl2 .= "<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n";
					$tbl2 .= "<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n";
					$tbl2 .= "<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n";
					$tbl2 .= "<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n";
					$tbl2 .= "<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n";
				}
				//当直
				if ($timecard_bean->all_duty_disp_flg != "f") {
					$tbl2 .= "<td bgcolor=\"$bgcolor2\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($prov_night_duty) . "</font></td>\n";
				}
				//手当
				$tbl2 .= "<td bgcolor=\"$bgcolor2\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($prov_allow_contents) . "</font></td>\n";
				//手当回数
				$tbl2 .= "<td bgcolor=\"$bgcolor2\" class=\"lbl\">&nbsp;</td>\n";
				//病棟外
				if($meeting_display_flag){
					$tbl2 .= "<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n";
					$tbl2 .= "<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n";
				}
				$tbl2 .= "</tr>\n";

				$tbl3 .= "<tr bgcolor=\"$bgcolor\">\n";
				//日付 20100907
				$tbl3 .= "<td rowspan=\"$rowspan\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
				$tbl3 .= "{$tmp_month}月{$tmp_day}日";
				$tbl3 .= "</font></td>\n";
				//曜日 20100907
				$tbl3 .= "<td rowspan=\"$rowspan\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . get_weekday(to_timestamp($tmp_date)) . "$hspacer</font></td>\n";
				//勤務
				$tbl3 .= "<td bgcolor=\"$bgcolor2\" class=\"tm\" height=\"27\">&nbsp;</td>\n";
				//遅刻 20100907
				$tbl3 .= "<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n";
				//早退 20100907
				$tbl3 .= "<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n";
				//残業
				$tbl3 .= "<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n";
				//法定内残、法定外残
				if ($timecard_bean->all_legal_over_disp_flg != "f"){
					$tbl3 .= "<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n";
					$tbl3 .= "<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n";
				}
				//深夜
				$tbl3 .= "<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n";
				//早出残業
				$tbl3 .= "<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n";
				$tbl3 .= "<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n";
				$tbl3 .= "<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n";
				if ($timecard_bean->ret_display_flag == "t"){
					$tbl3 .= "<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n";
				}
				$tbl3 .= "<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n";
				if($meeting_display_flag){
					$tbl3 .= "<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n";
				}

				$tbl3 .= "<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n";
				$tbl3 .= "<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n";

				$tbl3 .= "</tr>\n";

				//$tbl3 .= "<tr bgcolor=\"$bgcolor\">\n";
			} //view=="2" 予実績表示

			$hspacer = "";

			//グループ、パターンのリスト取得
			if ($timecard_bean->group_display_flag == "t"){
				//グループのリスト取得
				$arr_timecard_group = $atdbk_common_class->get_group_array();
				$tbl1 .= "<td class=\"txt\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
				$tbl1 .= "<select name=\"list_tmcd_group_id[]\" id=\"$selectGroupId\" onChange=\"setAtdptn(this.value, '$selectPatternId')\">";

				foreach ($arr_timecard_group as $group => $group_val) {
					$tbl1 .= "<option value=\"$group\"";
					if ($tmcd_group_id == $group) {
						$tbl1 .= " selected";
					}
					$tbl1 .= ">$group_val";
				}
				$tbl1 .= "</select>";
				$tbl1 .= "$hspacer</font></td>\n";
			}
			else{
				$tbl1 .= "<input type=\"hidden\" name=\"list_tmcd_group_id[]\" value=\"$tmcd_group_id\"> \n";
			}

			$arr_attendance_pattern = $atdbk_common_class->get_pattern_array($tmcd_group_id);
			$tbl1 .= "<td height=\"30\" class=\"txt\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
			$tbl1 .= "<select name=\"pattern[]\" id=\"$selectPatternId\" onChange=\"setReason($wk_pos, $tmcd_group_id, this.value);\">";
			$tbl1 .= "<option value=\"--\">";
			foreach ($arr_attendance_pattern as $atdptn_id => $atdptn_val) {
				$tbl1 .= "<option value=\"$atdptn_id\"";
				if ($pattern == $atdptn_id) {
					$tbl1 .= " selected";
				}
				$tbl1 .= ">$atdptn_val";
			}
			$tbl1 .= "</select>";
			$tbl1 .= "$hspacer</font></td>\n";


			// 事由
			$tbl1 .= "<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
			$tbl1 .= $atdbk_common_class->set_reason_option("reason[]", $reason, true, true);
			$tbl1 .= "$hspacer</font></td>\n";

			$wk_left = "ml1";
			$wk_right = "mr1";
			if ($timecard_bean->zen_display_flag == "t"){
				$tbl1 .= "<td align=\"center\" class=\"$wk_left\">";
				// 前日になる場合があるフラグ
				$previous_day_flag_checked = ($previous_day_flag == 1) ? " checked" : "";
				$tbl1 .= "<input type=\"checkbox\" name=\"prev_day_flag_{$wk_pos}\" value=\"1\" $previous_day_flag_checked>";
				$tbl1 .= "</td>";
				$wk_left = "mc1";
			}

			$tbl1 .= "<td nowrap class=\"$wk_left\">\n";
			list($wk_start_hour, $wk_start_min) = split(":", hhmm_to_hmm($start_time));
			$tbl1 .= "<input type=\"text\" name=\"start_hours[]\" value=\"$wk_start_hour\" size=\"2\" maxlength=\"2\" style=\"width:26px\">:";
			$tbl1 .= "<input type=\"text\" name=\"start_mins[]\" value=\"$wk_start_min\" size=\"2\" maxlength=\"2\" style=\"width:26px\">\n";
			$tbl1 .= "</td>\n";

			if ($timecard_bean->yoku_display_flag == "t"){
				$tbl1 .= "<td align=\"center\" class=\"$wk_left\">";
				// 翌日フラグ
				$next_day_flag_checked = ($show_next_day_flag == 1) ? " checked" : "";
				$tbl1 .= "<input type=\"checkbox\" name=\"next_day_flag_{$wk_pos}\" value=\"1\" $next_day_flag_checked>";
				$tbl1 .= "</td>";
			}

			$tbl1 .= "<td nowrap class=\"$wk_right\">\n";
			list($wk_end_hour, $wk_end_min) = split(":", hhmm_to_hmm($show_end_time));

			$tbl1 .= "<input type=\"text\" name=\"end_hours[]\" value=\"$wk_end_hour\" size=\"2\" maxlength=\"2\" style=\"width:26px\">:";
			$tbl1 .= "<input type=\"text\" name=\"end_mins[]\" value=\"$wk_end_min\" size=\"2\" maxlength=\"2\" style=\"width:26px\">\n";
			//残業管理をしない場合の対応 2008/10/15
			if ($no_overtime == "t") {
				$overtime_link_type = "0"; //申請を表示しない
				$time1 = "";	// 残業時間を表示しない
				$wk_hoteinai_zan = "";
				$wk_hoteigai = "";
				$wk_zangyo = ""; //20100721
			}
			$tbl1 .= "</font></td>\n";
			$tbl1 .= "</tr>\n";

			$wk_left = "ml2";
			$wk_center = "mc2";
			$wk_right = "mr2";
			$tbl2 .= "<tr bgcolor=\"$bgcolor\" height=\"30\">\n";
			//残業開始 翌日フラグ
			$tbl2 .= "<td align=\"center\" class=\"$wk_left\" style=\"border-left-width:3px\">";
			$over_start_next_day_flag_checked = ($over_start_next_day_flag == 1) ? " checked" : "";
			$tbl2 .= "<input type=\"checkbox\" name=\"o_s_next_day_flag_{$wk_pos}\" value=\"1\" $over_start_next_day_flag_checked>";
			$tbl2 .= "</td>";
			//残業開始
			$tbl2 .= "<td nowrap class=\"$wk_center\">\n";
			list($wk_over_start_hour, $wk_over_start_min) = split(":", hhmm_to_hmm($over_start_time));
			$tbl2 .= "<input type=\"text\" name=\"over_start_hours[]\" value=\"$wk_over_start_hour\" size=\"2\" maxlength=\"2\" style=\"width:26px\">:";
			$tbl2 .= "<input type=\"text\" name=\"over_start_mins[]\" value=\"$wk_over_start_min\" size=\"2\" maxlength=\"2\" style=\"width:26px\">\n";
			$tbl2 .= "</td>\n";
			//残業終了 翌日フラグ
			$tbl2 .= "<td align=\"center\" class=\"$wk_center\">";
			$over_end_next_day_flag_checked = ($over_end_next_day_flag == 1) ? " checked" : "";
			$tbl2 .= "<input type=\"checkbox\" name=\"o_e_next_day_flag_{$wk_pos}\" value=\"1\" $over_end_next_day_flag_checked>";
			$tbl2 .= "</td>";
			//残業終了
			$tbl2 .= "<td nowrap class=\"$wk_center\">\n";
			list($wk_over_end_hour, $wk_over_end_min) = split(":", hhmm_to_hmm($over_end_time));
			$tbl2 .= "<input type=\"text\" name=\"over_end_hours[]\" value=\"$wk_over_end_hour\" size=\"2\" maxlength=\"2\" style=\"width:26px\">:";
			$tbl2 .= "<input type=\"text\" name=\"over_end_mins[]\" value=\"$wk_over_end_min\" size=\"2\" maxlength=\"2\" style=\"width:26px\">\n";
			$tbl2 .= "</td>\n";
			//残業開始2 翌日フラグ
			$tbl2 .= "<td align=\"center\" class=\"$wk_center\">";
			$over_start_next_day_flag2_checked = ($over_start_next_day_flag2 == 1) ? " checked" : "";
			$tbl2 .= "<input type=\"checkbox\" name=\"o_s_next_day_flag2_{$wk_pos}\" value=\"1\" $over_start_next_day_flag2_checked>";
			$tbl2 .= "</td>";
			//残業開始2
			$tbl2 .= "<td nowrap class=\"$wk_center\">\n";
			list($wk_over_start_hour2, $wk_over_start_min2) = split(":", hhmm_to_hmm($over_start_time2));
			$tbl2 .= "<input type=\"text\" name=\"over_start_hours2[]\" value=\"$wk_over_start_hour2\" size=\"2\" maxlength=\"2\" style=\"width:26px\">:";
			$tbl2 .= "<input type=\"text\" name=\"over_start_mins2[]\" value=\"$wk_over_start_min2\" size=\"2\" maxlength=\"2\" style=\"width:26px\">\n";
			$tbl2 .= "</td>\n";
			//残業終了2 翌日フラグ
			$tbl2 .= "<td align=\"center\" class=\"$wk_center\">";
			$over_end_next_day_flag2_checked = ($over_end_next_day_flag2 == 1) ? " checked" : "";
			$tbl2 .= "<input type=\"checkbox\" name=\"o_e_next_day_flag2_{$wk_pos}\" value=\"1\" $over_end_next_day_flag2_checked>";
			$tbl2 .= "</td>";
			//残業終了2
			$tbl2 .= "<td nowrap class=\"$wk_right\">\n";
			list($wk_over_end_hour2, $wk_over_end_min2) = split(":", hhmm_to_hmm($over_end_time2));
			$tbl2 .= "<input type=\"text\" name=\"over_end_hours2[]\" value=\"$wk_over_end_hour2\" size=\"2\" maxlength=\"2\" style=\"width:26px\">:";
			$tbl2 .= "<input type=\"text\" name=\"over_end_mins2[]\" value=\"$wk_over_end_min2\" size=\"2\" maxlength=\"2\" style=\"width:26px\">\n";
			$tbl2 .= "</td>\n";
			//理由
			//残業情報の理由が設定済の場合、その他0
			$wk_reason_id = ($ovtm_reason != "") ? "0" : $ovtm_reason_id;
			$tbl2 .= "<td nowrap>\n";
			$tbl2 .= show_ovtmrsn($wk_reason_id, $arr_ovtmrsn, true, false, true);
			$tbl2 .= "</td>\n";
			if ($timecard_bean->all_legal_over_disp_flg != "f"){
				//法定内残業
				list($legal_in_over_hour, $legal_in_over_min) = split(":", hhmm_to_hmm($legal_in_over_time));
				$tbl2 .= "<td nowrap>\n";
				$tbl2 .= "<input type=\"text\" name=\"legal_in_over_hours[]\" value=\"$legal_in_over_hour\" size=\"2\" maxlength=\"2\" style=\"width:26px\">:";
				$tbl2 .= "<input type=\"text\" name=\"legal_in_over_mins[]\" value=\"$legal_in_over_min\" size=\"2\" maxlength=\"2\" style=\"width:26px\">\n";
				$tbl2 .= "</td>\n";
			}
			//残業申請状況の表示
			if ($timecard_bean->all_apply_disp_flg != "f") {
				//申請状態
				$tbl2 .= "<td nowrap>\n";
				//optionにvalue:5以上は、再表示用
				$disp_status = ($recomp_flg == "1") ? $set_data[$tmp_date]["ovtm_status"] : "";
				$tbl2 .= "<select name=\"ovtm_status[]\" >";
				switch ($overtime_link_type) {
					case "0":
						$tbl2 .= "<option value=\"\">　　　　　　　　</option>";
						$selected = ($disp_status == "4") ? " selected" : "";
						$tbl2 .= "<option value=\"4\" $selected>残業申請不要</option>";
						$selected = ($disp_status == "1") ? " selected" : "";
						$tbl2 .= "<option value=\"1\" $selected>残業承認済</option>";
						$selected = ($disp_status == "2") ? " selected" : "";
						$tbl2 .= "<option value=\"2\" $selected>残業否認済</option>";
						break;
					case "1":
						$selected = ($disp_status == "5") ? " selected" : "";
						$tbl2 .= "<option value=\"5\" $selected>残業未申請</option>";
						$selected = ($disp_status == "4") ? " selected" : "";
						$tbl2 .= "<option value=\"4\" $selected>残業申請不要</option>";
						$selected = ($disp_status == "1") ? " selected" : "";
						$tbl2 .= "<option value=\"1\" $selected>残業承認済</option>";
						$selected = ($disp_status == "2") ? " selected" : "";
						$tbl2 .= "<option value=\"2\" $selected>残業否認済</option>";
						break;
					case "2":
						$selected = ($disp_status == "6") ? " selected" : "";
						$tbl2 .= "<option value=\"6\" $selected>残業申請中</option>";
						$selected = ($disp_status == "4") ? " selected" : "";
						$tbl2 .= "<option value=\"4\" $selected>残業申請不要</option>";
						$selected = ($disp_status == "1") ? " selected" : "";
						$tbl2 .= "<option value=\"1\" $selected>残業承認済</option>";
						$selected = ($disp_status == "2") ? " selected" : "";
						$tbl2 .= "<option value=\"2\" $selected>残業否認済</option>";
						$selected = ($disp_status == "3") ? " selected" : "";
						$tbl2 .= "<option value=\"3\" $selected>残業差戻済</option>";
						break;
					case "6":
						$tbl2 .= "<option value=\"\">残業申請不要</option>";
						$selected = ($disp_status == "1") ? " selected" : "";
						$tbl2 .= "<option value=\"1\" $selected>残業承認済</option>";
						$selected = ($disp_status == "2") ? " selected" : "";
						$tbl2 .= "<option value=\"2\" $selected>残業否認済</option>";
						break;
					case "3":
						$tbl2 .= "<option value=\"\">残業承認済</option>";
						break;
					case "4":
						$tbl2 .= "<option value=\"\">残業否認済</option>";
						break;
					case "5":
						$tbl2 .= "<option value=\"\">残業差戻済</option>";
						break;
				}
				$tbl2 .= "</select>";
				$tbl2 .= "</td>\n";
			}
			//外出
			$tbl2 .= "<td nowrap>\n";
			list($wk_out_hour, $wk_out_min) = split(":", hhmm_to_hmm($out_time));
			$tbl2 .= "<input type=\"text\" name=\"out_hours[]\" value=\"$wk_out_hour\" size=\"2\" maxlength=\"2\" style=\"width:26px\">:";
			$tbl2 .= "<input type=\"text\" name=\"out_mins[]\" value=\"$wk_out_min\" size=\"2\" maxlength=\"2\" style=\"width:26px\">\n";
			$tbl2 .= "</td>\n";
			//戻り
			$tbl2 .= "<td nowrap>\n";
			list($wk_ret_hour, $wk_ret_min) = split(":", hhmm_to_hmm($ret_time));
			$tbl2 .= "<input type=\"text\" name=\"ret_hours[]\" value=\"$wk_ret_hour\" size=\"2\" maxlength=\"2\" style=\"width:26px\">:";
			$tbl2 .= "<input type=\"text\" name=\"ret_mins[]\" value=\"$wk_ret_min\" size=\"2\" maxlength=\"2\" style=\"width:26px\">\n";
			$tbl2 .= "</td>\n";
			//休憩時刻追加 20100921
			//休憩開始
			$tbl2 .= "<td nowrap>\n";
			list($wk_rest_start_hour, $wk_rest_start_min) = split(":", hhmm_to_hmm($rest_start_time));
			$tbl2 .= "<input type=\"text\" name=\"rest_start_hours[]\" value=\"$wk_rest_start_hour\" size=\"2\" maxlength=\"2\" style=\"width:26px\">:";
			$tbl2 .= "<input type=\"text\" name=\"rest_start_mins[]\" value=\"$wk_rest_start_min\" size=\"2\" maxlength=\"2\" style=\"width:26px\">\n";
			$tbl2 .= "</td>\n";
			//休憩終了
			$tbl2 .= "<td nowrap>\n";
			list($wk_rest_end_hour, $wk_rest_end_min) = split(":", hhmm_to_hmm($rest_end_time));
			$tbl2 .= "<input type=\"text\" name=\"rest_end_hours[]\" value=\"$wk_rest_end_hour\" size=\"2\" maxlength=\"2\" style=\"width:26px\">:";
			$tbl2 .= "<input type=\"text\" name=\"rest_end_mins[]\" value=\"$wk_rest_end_min\" size=\"2\" maxlength=\"2\" style=\"width:26px\">\n";
			$tbl2 .= "</td>\n";
			//退勤復帰
			if ($timecard_bean->ret_display_flag == "t"){
				$tbl2 .= "<td nowrap>\n";
				list($wk_o_start_hour1, $wk_o_start_min1) = split(":", hhmm_to_hmm($o_start_time1));
				$tbl2 .= "<input type=\"text\" name=\"o_start_hour1s[]\" value=\"$wk_o_start_hour1\" size=\"2\" maxlength=\"2\" style=\"width:26px\">:";
				$tbl2 .= "<input type=\"text\" name=\"o_start_min1s[]\" value=\"$wk_o_start_min1\" size=\"2\" maxlength=\"2\" style=\"width:26px\">\n";
				$tbl2 .= "</td>\n";
				//復帰退勤
				$tbl2 .= "<td nowrap>\n";
				list($wk_o_end_hour1, $wk_o_end_min1) = split(":", hhmm_to_hmm($o_end_time1));
				$tbl2 .= "<input type=\"text\" name=\"o_end_hour1s[]\" value=\"$wk_o_end_hour1\" size=\"2\" maxlength=\"2\" style=\"width:26px\">:";
				$tbl2 .= "<input type=\"text\" name=\"o_end_min1s[]\" value=\"$wk_o_end_min1\" size=\"2\" maxlength=\"2\" style=\"width:26px\">\n";
				$tbl2 .= "</td>\n";
				//退勤復帰
				$tbl2 .= "<td nowrap>\n";
				list($wk_o_start_hour2, $wk_o_start_min2) = split(":", hhmm_to_hmm($o_start_time2));
				$tbl2 .= "<input type=\"text\" name=\"o_start_hour2s[]\" value=\"$wk_o_start_hour2\" size=\"2\" maxlength=\"2\" style=\"width:26px\">:";
				$tbl2 .= "<input type=\"text\" name=\"o_start_min2s[]\" value=\"$wk_o_start_min2\" size=\"2\" maxlength=\"2\" style=\"width:26px\">\n";
				$tbl2 .= "</td>\n";
				//復帰退勤
				$tbl2 .= "<td nowrap>\n";
				list($wk_o_end_hour2, $wk_o_end_min2) = split(":", hhmm_to_hmm($o_end_time2));
				$tbl2 .= "<input type=\"text\" name=\"o_end_hour2s[]\" value=\"$wk_o_end_hour2\" size=\"2\" maxlength=\"2\" style=\"width:26px\">:";
				$tbl2 .= "<input type=\"text\" name=\"o_end_min2s[]\" value=\"$wk_o_end_min2\" size=\"2\" maxlength=\"2\" style=\"width:26px\">\n";
				$tbl2 .= "</td>\n";
				//退勤復帰
				$tbl2 .= "<td nowrap>\n";
				list($wk_o_start_hour3, $wk_o_start_min3) = split(":", hhmm_to_hmm($o_start_time3));
				$tbl2 .= "<input type=\"text\" name=\"o_start_hour3s[]\" value=\"$wk_o_start_hour3\" size=\"2\" maxlength=\"2\" style=\"width:26px\">:";
				$tbl2 .= "<input type=\"text\" name=\"o_start_min3s[]\" value=\"$wk_o_start_min3\" size=\"2\" maxlength=\"2\" style=\"width:26px\">\n";
				$tbl2 .= "</td>\n";
				//復帰退勤
				$tbl2 .= "<td nowrap>\n";
				list($wk_o_end_hour3, $wk_o_end_min3) = split(":", hhmm_to_hmm($o_end_time3));
				$tbl2 .= "<input type=\"text\" name=\"o_end_hour3s[]\" value=\"$wk_o_end_hour3\" size=\"2\" maxlength=\"2\" style=\"width:26px\">:";
				$tbl2 .= "<input type=\"text\" name=\"o_end_min3s[]\" value=\"$wk_o_end_min3\" size=\"2\" maxlength=\"2\" style=\"width:26px\">\n";
				$tbl2 .= "</td>\n";
			}
			// 当直
			if ($timecard_bean->all_duty_disp_flg != "f") {
				$tbl2 .= "<td class=\"txt\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">\n";
				$tbl2 .= "<select name=\"night_duty[]\">";
				$tbl2 .= "<option value=\"--\">";
				$tbl2 .= "<option value=\"1\""; if ($night_duty == "1") {$tbl2 .= " selected";} $tbl2 .= ">有り";
				$tbl2 .= "<option value=\"2\""; if ($night_duty == "2") {$tbl2 .= " selected";} $tbl2 .= ">無し";
				$tbl2 .= "</select>";
				$tbl2 .= "$hspacer</font></td>\n";
			}

			$tbl2 .= "<td height=\"27\" class=\"txt\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
			$tbl2 .= show_allowance_list($allow_id, $arr_allowance, "", true);
			$tbl2 .= "$hspacer</font></td>\n";

			//手当回数
			$tbl2 .= "<td align=\"center\" class=\"txt\">";
			$tbl2 .= show_allowance_count($allow_count, "", true);
			$tbl2 .= "</td>\n";

			if($meeting_display_flag){
				//病棟外開始
				$tbl2 .= "<td nowrap>\n";
				list($wk_m_start_hour, $wk_m_start_min) = split(":", hhmm_to_hmm($meeting_start_time));
				$tbl2 .= "<input type=\"text\" name=\"m_start_hours[]\" value=\"$wk_m_start_hour\" size=\"2\" maxlength=\"2\" style=\"width:26px\">:";
				$tbl2 .= "<input type=\"text\" name=\"m_start_mins[]\" value=\"$wk_m_start_min\" size=\"2\" maxlength=\"2\" style=\"width:26px\">\n";
				$tbl2 .= "</td>\n";
				//病棟外終了
				$tbl2 .= "<td nowrap>\n";
				list($wk_m_end_hour, $wk_m_end_min) = split(":", hhmm_to_hmm($meeting_end_time));
				$tbl2 .= "<input type=\"text\" name=\"m_end_hours[]\" value=\"$wk_m_end_hour\" size=\"2\" maxlength=\"2\" style=\"width:26px\">:";
				$tbl2 .= "<input type=\"text\" name=\"m_end_mins[]\" value=\"$wk_m_end_min\" size=\"2\" maxlength=\"2\" style=\"width:26px\">\n";
				$tbl2 .= "</td>\n";
			}
			$tbl2 .= "</tr>\n";

			$tbl3 .= "<tr bgcolor=\"$bgcolor\" height=\"30\">\n";
			if ($view == "1") {
				//日付 20100907
				$tbl3 .= "<td rowspan=\"$rowspan\" class=\"lbl\" nowrap><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
				$tbl3 .= "{$tmp_month}月{$tmp_day}日";
				$tbl3 .= "</font></td>\n";
				//曜日 20100907
				$tbl3 .= "<td rowspan=\"$rowspan\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . get_weekday(to_timestamp($tmp_date)) . "$hspacer</font></td>\n";
			}
			//勤務
			$tbl3 .= "<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($kinmu_time)) . "$hspacer</font></td>\n"; //$work_time + $wk_return_timeから変更 20100910
			//遅刻 20100907 赤色に変更 20100922
			$tbl3 .= "<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"red\">" . nbsp(minute_to_hmm($start_time_info["diff_minutes"])) . "$hspacer</font></td>\n";
			//早退 20100907 赤色に変更 20100922
			$tbl3 .= "<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"red\">" . nbsp(minute_to_hmm($end_time_info["diff_minutes"])) . "$hspacer</font></td>\n";
			//残業
			$tbl3 .= "<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($wk_zangyo)) . "$hspacer</font></td>\n";
			if ($timecard_bean->all_legal_over_disp_flg != "f"){
				//法内残
				$tbl3 .= "<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($wk_hoteinai_zan)) . "$hspacer</font></td>\n";
				//法外残
				$tbl3 .= "<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($wk_hoteigai)) . "$hspacer</font></td>\n";
			}

			//深勤
			$tbl3 .= "<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($time2) . "$hspacer</font></td>\n";
			//早出残業
			list($early_over_hour, $early_over_min) = split(":", hhmm_to_hmm($early_over_time));
			$tbl3 .= "<td nowrap><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">\n";
			$wk_early_over_time = hhmm_to_hmm($early_over_time);
			$tbl3 .= nbsp($wk_early_over_time) . $hspacer;
			$tbl3 .= "<input type=\"hidden\" name=\"early_over_hours[]\" value=\"$early_over_hour\">\n";
			$tbl3 .= "<input type=\"hidden\" name=\"early_over_mins[]\" value=\"$early_over_min\">\n";
			//$tbl3 .= "<input type=\"text\" name=\"early_over_hours[]\" value=\"$early_over_hour\" size=\"2\" maxlength=\"2\">:";
			//$tbl3 .= "<input type=\"text\" name=\"early_over_mins[]\" value=\"$early_over_min\" size=\"2\" maxlength=\"2\">\n";
			$tbl3 .= "</font></td>\n";
			//普外−＞外出に変更 20090603
			$tbl3 .= "<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($time_outtime) . "$hspacer</font></td>\n";
			//残外−＞休憩に変更 20090603
			//休憩、残業休憩追加 20141209
			$wk_time_rest = hmm_to_minute($time_rest) + $over_rest_time_day;
			$time_rest = minute_to_hmm($wk_time_rest);
			$tbl3 .= "<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($time_rest) . "$hspacer</font></td>\n";

			if ($timecard_bean->ret_display_flag == "t"){
				//$ret3_str = ($timecard_bean->return_icon_flg != "2") ? "復帰" : "呼出";
				$ret3_str = "";
				//回数（$return_count）を削除し、時間に変更 20090604
				$tbl3 .= "<td align=\"center\" width=\"64\" nowrap><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";

				switch ($return_link_type) {
					case "0":  // 申請不要
						$tbl3 .= nbsp($return_time) . $hspacer;
						break;
					case "1":  // 未申請
						$apply_stat_nm = ($timecard_bean->over_apply_disp_flg == "t") ? "{$ret3_str}未申請" : "";
						$tbl3 .= nbsp($return_time) . "<font size=\"2\" class=\"j10\">$apply_stat_nm</font>";
						break;
					case "2":  // 申請中

						// 退勤後復帰申請承認者情報取得
						$arr_rtnaprv = $atdbk_workflow_common_class->get_approve_list($return_apply_id, "RTN");
						// 承認がひとつでもあるかチェック
						$apv_1_flg = $atdbk_workflow_common_class->get_apv_flg($arr_rtnaprv);
						//残業申請状況表示フラグ
						if ($timecard_bean->over_apply_disp_flg == "t" || $timecard_bean->over_apply_disp_flg == "1") {//20140226
							// 一部承認
							if($apv_1_flg)
							{
								$apply_stat_nm = "一部承認";
							}
							else
							{
								$apply_stat_nm = "{$ret3_str}申請中";
							}
						} else {
							$apply_stat_nm = "";
						}

						$tbl3 .= nbsp($return_time) . "<font size=\"2\" color=\"red\" class=\"j10\">$apply_stat_nm</font>";
						break;
					case "3":  // 承認済
						$apply_stat_nm = ($timecard_bean->over_apply_disp_flg == "t" || $timecard_bean->over_apply_disp_flg == "1") ? "{$ret3_str}承認済" : "";
						$tbl3 .= nbsp($return_time) . "<font size=\"2\" class=\"j10\">$apply_stat_nm</font>";
						break;
					case "4":  // 否認済
						$apply_stat_nm = ($timecard_bean->over_apply_disp_flg == "t" || $timecard_bean->over_apply_disp_flg == "1") ? "{$ret3_str}否認済" : "";
						$tbl3 .= nbsp($return_time) . "<font size=\"2\" class=\"j10\">$apply_stat_nm</font>";
						break;
					case "5":  // 差戻済
						$apply_stat_nm = ($timecard_bean->over_apply_disp_flg == "t" || $timecard_bean->over_apply_disp_flg == "1") ? "{$ret3_str}差戻済" : "";
						$tbl3 .= nbsp($return_time) . "<font size=\"2\" color=\"red\" class=\"j10\">$apply_stat_nm</font>";
						break;
				}
				$tbl3 .= "</font></td>\n";
				//時間
				//				$tbl3 .= "<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($return_time) . "$hspacer</font></td>\n";
			}

			// 日数
			/*
						$total_workday_count = "";
						if ($start_time != "")
						{
							if($workday_count != "")
							{
								$total_workday_count = $workday_count;
							}

							if($night_duty_flag)
							{
								//曜日に対応したworkday_countを取得する
								$total_workday_count = $total_workday_count + $timecard_common_class->get_night_workday_count($tmp_date, $end_time, $next_day_flag, $tmcd_group_id);
							}
						}
						else if ($workday_count == 0){
							//日数換算に0が指定された場合のみ0を入れる
							$total_workday_count = $workday_count;
						}
			*/
			// 出勤・退勤時刻がある場合のみ表示する 20090702 //明けを追加 20110819
			if (!(($start_time != "" && $end_time != "") || $after_night_duty_flag == "1")) {
				$total_workday_count = "";
			}
			$tbl3 .= "<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . $total_workday_count . "<br></font></td>\n";

			//会議研修
			if($meeting_display_flag){
				$tbl3 .= "<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
				$meeting_time_hh = "";
				$meeting_time_mm = "";
				$tmp_meeting_time = 0;
				// 開始終了時刻から時間を計算する 20091008
				if ($meeting_start_time != "" && $meeting_end_time != "") {
					$tmp_meeting_start_date_time = $tmp_date.$meeting_start_time;
					if ($meeting_start_time <= $meeting_end_time) {
						$tmp_meeting_end_date_time = $tmp_date.$meeting_end_time;
					} else {
						// 日またがりの場合
						$tmp_meeting_end_date_time = next_date($tmp_date).$meeting_end_time;
					}
					$tmp_meeting_time = date_utils::get_time_difference($tmp_meeting_end_date_time, $tmp_meeting_start_date_time);
					$tmp_meeting_hhmm = $timecard_common_class->minute_to_h_hh($tmp_meeting_time);
					$tbl3 .= $tmp_meeting_hhmm;
				} else
					if ($meeting_time != null){
						$meeting_time_hh = (int)substr($meeting_time, 0, 2);
						$meeting_time_mm = (int)substr($meeting_time, 2, 4);

						$tbl3 .= $meeting_time_hh;
						$tmp_meeting_time = $meeting_time_hh*60;

						if ($meeting_time_mm == 15)
						{
							$tbl3 .= ".25";
							$tmp_meeting_time += 15;
						}
						else if($meeting_time_mm == 30)
						{
							$tbl3 .= ".5";
							$tmp_meeting_time += 30;
						}
						else if($meeting_time_mm == 45)
						{
							$tbl3 .= ".75";
							$tmp_meeting_time += 45;
						}

					}
				$tbl3 .= "<br></font></td>\n";
				$meeting_time_total += $tmp_meeting_time;
			}

			//最終更新日時・更新者
			$tbl3 .= "<td bgcolor=\"$bgcolor\" nowrap><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$update_time</font></td>\n";
			$tbl3 .= "<td bgcolor=\"$bgcolor\" nowrap><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$update_emp_name</font></td>\n";

			$tbl3 .= "</tr>\n";
		}
		//締めの場合
		else {
			echo("<tr bgcolor=\"$bgcolor\">\n");
			echo("<td rowspan=\"$rowspan\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
			echo("{$tmp_month}月{$tmp_day}日");
/*
			if ($modify_apply_id == "") {
				echo("{$tmp_month}月{$tmp_day}日{$hspacer}");
			} else {
				echo("{$tmp_month}月{$tmp_day}日<br><font size=\"2\" class=\"j10\">$modify_apply_status</font>");
			}
			echo("</font></td>\n");
*/
			//曜日
			echo("<td rowspan=\"$rowspan\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . get_weekday(to_timestamp($tmp_date)) . "$hspacer</font></td>\n");

			//種別
			if ($timecard_bean->type_display_flag == "t"){
				echo("<td rowspan=\"$rowspan\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($type_name) . "$hspacer</font></td>\n");
			}

			// 予実績表示の場合
			if ($view == "2") {

				// 処理日付の勤務予定情報を取得
				$prov_tmcd_group_id = $arr_wktotalp[$tmp_date]["tmcd_group_id"];
				$prov_pattern = $arr_wktotalp[$tmp_date]["pattern"];
				$prov_reason = $arr_wktotalp[$tmp_date]["reason"];
				$prov_night_duty = $arr_wktotalp[$tmp_date]["night_duty"];
				$prov_allow_id = $arr_wktotalp[$tmp_date]["allow_id"];	//手当

				$prov_start_time = $arr_wktotalp[$tmp_date]["prov_start_time"];
				$prov_end_time = $arr_wktotalp[$tmp_date]["prov_end_time"];

				if ($prov_start_time != "") {
					$prov_start_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $prov_start_time);
				}
				if ($prov_end_time != "") {
					$prov_end_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $prov_end_time);
				}

				// 表示値の編集
				$prov_tmcd_group_name = $atdbk_common_class->get_group_name($prov_tmcd_group_id);
				$prov_pattern = $atdbk_common_class->get_pattern_name($prov_tmcd_group_id, $prov_pattern);
				$prov_reason = $atdbk_common_class->get_reason_name($prov_reason);

				if($prov_night_duty == "1")
				{
					$prov_night_duty = "有り";
				}
				else if($prov_night_duty == "2")
				{
					$prov_night_duty = "無し";
				}

				// ************ oose add start 2008/02/05 *****************
				$prov_allow_contents = "";
				foreach($arr_allowance as $allowance)
				{
					if($prov_allow_id == $allowance["allow_id"])
					{
						$prov_allow_contents = $allowance["allow_contents"];
						break;
					}
				}
				// ************ oose add end 2008/02/05 *****************

				if ($timecard_bean->group_display_flag == "t"){
					echo("<td height=\"22\" bgcolor=\"$bgcolor2\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($prov_tmcd_group_name) . "</font></td>\n");
				}
				echo("<td height=\"22\" bgcolor=\"$bgcolor2\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($prov_pattern) . "</font></td>\n");
				echo("<td bgcolor=\"$bgcolor2\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($prov_reason) . "</font></td>\n");
				//前
				if ($timecard_bean->zen_display_flag == "t"){
					echo("<td bgcolor=\"$bgcolor2\" class=\"ml1\">&nbsp;</td>\n");
					$wk_class = "mc1";
				} else {
					//前の列がない場合、出勤時刻の列の左を太くする
					$wk_class = "ml1";
				}
				echo("<td align=\"right\" bgcolor=\"$bgcolor2\" class=\"$wk_class\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">" . nbsp(hhmm_to_hmm($prov_start_time)) . "</font></td>\n");
				//翌
				if ($timecard_bean->yoku_display_flag == "t"){
					echo("<td bgcolor=\"$bgcolor2\" class=\"mc1\">&nbsp;</td>\n");
				}
				echo("<td align=\"right\" bgcolor=\"$bgcolor2\" class=\"mr1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">" . nbsp(hhmm_to_hmm($prov_end_time)) . "</font></td>\n");
				//残業
				echo("<td bgcolor=\"$bgcolor2\" class=\"ml2\">&nbsp;</td>\n");
				echo("<td bgcolor=\"$bgcolor2\" class=\"mc2\">&nbsp;</td>\n");
				echo("<td bgcolor=\"$bgcolor2\" class=\"mc2\">&nbsp;</td>\n");
				echo("<td bgcolor=\"$bgcolor2\" class=\"mc2\">&nbsp;</td>\n");
				//残業2
				echo("<td bgcolor=\"$bgcolor2\" class=\"mc2\">&nbsp;</td>\n");
				echo("<td bgcolor=\"$bgcolor2\" class=\"mc2\">&nbsp;</td>\n");
				echo("<td bgcolor=\"$bgcolor2\" class=\"mc2\">&nbsp;</td>\n");
				echo("<td bgcolor=\"$bgcolor2\" class=\"mr2\">&nbsp;</td>\n");
				//理由
				echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
				if ($timecard_bean->all_legal_over_disp_flg != "f"){
					//法定内残業
					echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
				}
				//残業申請状況の表示
				if ($timecard_bean->all_apply_disp_flg != "f") {
					//申請状態
					echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
				}
				//外出
				echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
				echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
				//休憩時刻追加 20100921
				echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
				echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
				//復退
				if ($timecard_bean->ret_display_flag == "t"){
					echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
					echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
					echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
					echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
					echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
					echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
				}
				//当直
				if ($timecard_bean->all_duty_disp_flg != "f") {
					echo("<td bgcolor=\"$bgcolor2\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($prov_night_duty) . "</font></td>\n");
				}
				//手当
				echo("<td bgcolor=\"$bgcolor2\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($prov_allow_contents) . "</font></td>\n");
				//手当回数
				echo("<td bgcolor=\"$bgcolor2\" class=\"lbl\">&nbsp;</td>\n");
				//病棟外
				if($meeting_display_flag){
					echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
					echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
				}
				//日付 20100907
				echo("<td rowspan=\"$rowspan\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
				echo("{$tmp_month}月{$tmp_day}日");
				echo("</font></td>\n");
				//曜日 20100907
				echo("<td rowspan=\"$rowspan\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . get_weekday(to_timestamp($tmp_date)) . "$hspacer</font></td>\n");
				//勤務
				echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
				//遅刻 20100907
				echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
				//早退 20100907
				echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
				//残業
				echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");

				//法定内残、法定外残 20100209
				if ($timecard_bean->all_legal_over_disp_flg != "f"){
					echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
					echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
				}

				//深夜
				echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
				//早出残業
				echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
				echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
				echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
				//退復
				if ($timecard_bean->ret_display_flag == "t"){
					echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
				}
				echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
				//会議研修表示
				if($meeting_display_flag){
					echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
				}

				echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");
				echo("<td bgcolor=\"$bgcolor2\" class=\"tm\">&nbsp;</td>\n");

				echo("</tr>\n");

				echo("<tr bgcolor=\"$bgcolor\">\n");
			}

//			$hspacer = (($view == "1" && $modify_apply_id != "") || $overtime_apply_id != "" || $return_apply_id != "") ? "<br><font size=\"2\" class=\"j10\">&nbsp;</font>" : "";
			$hspacer = "";

			//グループ
			if ($timecard_bean->group_display_flag == "t"){
				echo("<td height=\"22\" class=\"txt\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
				echo(nbsp($atdbk_common_class->get_group_name($tmcd_group_id)));
				echo("$hspacer</font></td>\n");
			}

			//パターン
			echo("<td height=\"22\" class=\"txt\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
			//		echo(nbsp($arr_attendance_pattern[$pattern]));
			echo(nbsp($atdbk_common_class->get_pattern_name($tmcd_group_id, $pattern)));
			echo("$hspacer</font></td>\n");
			echo("<td class=\"txt\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
			echo(nbsp($atdbk_common_class->get_reason_name($reason)));
			echo("$hspacer</font></td>\n");

			$tmp_night_duty = "";
			if($night_duty == "1")
			{
				$tmp_night_duty = "有り";
			}
			else if($night_duty == "2")
			{
				$tmp_night_duty = "無し";
			}
			$wk_left = ($tmp_date == $end_date) ? "bl1" : "ml1";
			$wk_right = ($tmp_date == $end_date) ? "br1" : "mr1";
			if ($timecard_bean->zen_display_flag == "t"){

				echo("<td align=\"center\" class=\"$wk_left\">");
				// 前日になる場合があるフラグ
				$previous_day_value = "";
				if ($previous_day_flag == 1){
					$previous_day_value = "前";
				}
				echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($previous_day_value) . "$hspacer</font></td>\n");
				$wk_left = ($tmp_date == $end_date) ? "bc1" : "mc1";
			}

			echo("<td align=\"center\" class=\"$wk_left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">" . nbsp(hhmm_to_hmm($start_time)) . "$hspacer</font></td>\n");

			//middle,center or bottom,center
			$wk_left = ($tmp_date == $end_date) ? "bc1" : "mc1";
			if ($timecard_bean->yoku_display_flag == "t"){
				// 翌
				$next_day_value = "";
				if ($show_next_day_flag == 1){	//残業時刻日またがり対応 20100114
					$next_day_value = "翌";
				}
				echo("<td align=\"center\" class=\"$wk_left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($next_day_value) . "$hspacer</font></td>\n");
			}

			//退勤
			echo("<td align=\"center\" class=\"$wk_right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">");
			if ($overtime_apply_id == "") {
				echo(nbsp(hhmm_to_hmm($end_time)) . $hspacer);
			} else {
				echo(nbsp(hhmm_to_hmm($end_time)) . "<br><font size=\"2\" class=\"j10\">$overtime_apply_status</font>");
			}
			echo("</font></td>\n");

			$wk_left = ($tmp_date == $end_date) ? "bl2" : "ml2";
			$wk_center = ($tmp_date == $end_date) ? "bc2" : "mc2";
			$wk_right = ($tmp_date == $end_date) ? "br2" : "mr2";
			//残業開始 翌日フラグ
			$over_start_next_day_value = ($over_start_next_day_flag == 1) ? "翌" : "";
			echo("<td align=\"center\" class=\"$wk_left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($over_start_next_day_value) . "$hspacer</font></td>\n");
			//残業開始
			echo("<td align=\"center\" class=\"$wk_center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">");
			echo(nbsp(hhmm_to_hmm($over_start_time)) . $hspacer);
			echo("</font></td>\n");
			//残業終了 翌日フラグ
			$over_end_next_day_value = ($over_end_next_day_flag == 1) ? "翌" : "";
			echo("<td align=\"center\" class=\"$wk_center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($over_end_next_day_value) . "$hspacer</font></td>\n");
			//残業終了
			echo("<td align=\"center\" class=\"$wk_center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">");
			echo(nbsp(hhmm_to_hmm($over_end_time)) . $hspacer);
			echo("</font></td>\n");
			//残業開始2 翌日フラグ
			$over_start_next_day_value2 = ($over_start_next_day_flag2 == 1) ? "翌" : "";
			echo("<td align=\"center\" class=\"$wk_center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($over_start_next_day_value2) . "$hspacer</font></td>\n");
			//残業開始2
			echo("<td align=\"center\" class=\"$wk_center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">");
			echo(nbsp(hhmm_to_hmm($over_start_time2)) . $hspacer);
			echo("</font></td>\n");
			//残業終了2 翌日フラグ
			$over_end_next_day_value2 = ($over_end_next_day_flag2 == 1) ? "翌" : "";
			echo("<td align=\"center\" class=\"$wk_center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($over_end_next_day_value2) . "$hspacer</font></td>\n");
			//残業終了2
			echo("<td align=\"center\" class=\"$wk_right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">");
			echo(nbsp(hhmm_to_hmm($over_end_time2)) . $hspacer);
			echo("</font></td>\n");
			//理由
			//残業情報の理由が設定済の場合、その他
			$wk_reason_name = "";
			if ($ovtm_reason != "") {
				$wk_reason_name = "その他";
			} else {
				if ($ovtm_reason_id != "") {
					foreach($arr_ovtmrsn as $ovtmrsn) {
						if ($ovtmrsn["reason_id"] == $ovtm_reason_id) {
							$wk_reason_name = $ovtmrsn["reason"];
							break;
						}
					}
				}
			}
			if ($timecard_bean->all_legal_over_disp_flg != "f"){
				//法定内残業
				echo("<td class=\"$time_cell_class\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">");
				echo(nbsp(hhmm_to_hmm($legal_in_over_time)) . $hspacer);
				echo("</font></td>\n");
			}
			//残業申請状況の表示
			if ($timecard_bean->all_apply_disp_flg != "f") {
				echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($wk_reason_name) . "$hspacer</font></td>\n");
				//申請状態
				$apply_stat_nm = "&nbsp;";
				switch ($overtime_link_type) {
					case "0":  // 申請不要
						$apply_stat_nm = "&nbsp;";
						break;
					case "1":  // 未申請
						$apply_stat_nm = "残業未申請";
						break;
					case "2":  // 申請中

						// 残業申請承認者情報取得
						$arr_ovtmaprv = $atdbk_workflow_common_class->get_approve_list($overtime_apply_id, "OVTM");
						// 承認がひとつでもあるかチェック
						$apv_1_flg = $atdbk_workflow_common_class->get_apv_flg($arr_ovtmaprv);
						// 一部承認
						if ($apv_1_flg) {
							$apply_stat_nm = "一部承認";
						} else {
							$apply_stat_nm = "残業申請中";
						}

						break;
					case "3":  // 承認済
						$apply_stat_nm = "残業承認済";
						break;
					case "4":  // 否認済
						$apply_stat_nm = "残業否認済";
						break;
					case "5":  // 差戻済
						$apply_stat_nm = "残業差戻済";
						break;
					case "6":  // 残業申請不要
						$apply_stat_nm = "残業申請不要";
						break;
				}
				echo("<td align=\"center\" nowrap><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$apply_stat_nm");

				echo("</font></td>\n");
			}
			//外出
			echo("<td class=\"$time_cell_class\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">");
			echo(nbsp(hhmm_to_hmm($out_time)) . $hspacer);
			echo("</font></td>\n");
			//戻り
			echo("<td class=\"$time_cell_class\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">");
			echo(nbsp(hhmm_to_hmm($ret_time)) . $hspacer);
			echo("</font></td>\n");
			//休憩時刻追加 20100921
			//休憩開始
			echo("<td class=\"$time_cell_class\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">");
			echo(nbsp(hhmm_to_hmm($rest_start_time)) . $hspacer);
			echo("</font></td>\n");
			//休憩終了
			echo("<td class=\"$time_cell_class\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">");
			echo(nbsp(hhmm_to_hmm($rest_end_time)) . $hspacer);
			echo("</font></td>\n");
			//退勤復帰
			if ($timecard_bean->ret_display_flag == "t"){
				echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(hhmm_to_hmm($o_start_time1)) . "$hspacer</font></td>\n");
				//復帰退勤
				echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(hhmm_to_hmm($o_end_time1)) . "$hspacer</font></td>\n");
				//退勤復帰
				echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(hhmm_to_hmm($o_start_time2)) . "$hspacer</font></td>\n");
				//復帰退勤
				echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(hhmm_to_hmm($o_end_time2)) . "$hspacer</font></td>\n");
				//退勤復帰
				echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(hhmm_to_hmm($o_start_time3)) . "$hspacer</font></td>\n");
				//復帰退勤
				echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(hhmm_to_hmm($o_end_time3)) . "$hspacer</font></td>\n");
			}
			//当直
			if ($timecard_bean->all_duty_disp_flg != "f") {
				echo("<td class=\"txt\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
				echo(nbsp($tmp_night_duty));
				echo("$hspacer</font></td>\n");
			}

			//手当表示
			echo("<td class=\"txt\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
			$allow_contents = "";
			foreach($arr_allowance as $allowance)
			{
				if($allow_id == $allowance["allow_id"])
				{
					$allow_contents = $allowance["allow_contents"];
					break;
				}
			}
			echo(nbsp($allow_contents));
			echo("$hspacer</font></td>\n");
			//手当回数
			echo("<td align=\"center\" class=\"txt\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
			if ($allow_contents != "") {
				echo($allow_count);
			} else {
				echo("&nbsp;");
			}
			echo("</font></td>\n");

			if($meeting_display_flag){
				//病棟外開始
				echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(hhmm_to_hmm($meeting_start_time)) . "$hspacer</font></td>\n");
				//病棟外終了
				echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(hhmm_to_hmm($meeting_end_time)) . "$hspacer</font></td>\n");
			}
			if ($view == "1") {
				//日付 20100907
				echo("<td rowspan=\"$rowspan\" class=\"lbl\" nowrap><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
				echo("{$tmp_month}月{$tmp_day}日");
				echo("</font></td>\n");
				//曜日 20100907
				echo("<td rowspan=\"$rowspan\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . get_weekday(to_timestamp($tmp_date)) . "$hspacer</font></td>\n");
			}
			//勤務
			echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($kinmu_time)) . "$hspacer</font></td>\n"); //$work_time + $wk_return_timeから変更 20100910
			//遅刻 20100907 赤色に変更 20100922
			echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"red\">" . nbsp(minute_to_hmm($start_time_info["diff_minutes"])) . "$hspacer</font></td>\n");
			//早退 20100907 赤色に変更 20100922
			echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"red\">" . nbsp(minute_to_hmm($end_time_info["diff_minutes"])) . "$hspacer</font></td>\n");
			//残業
			echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($wk_zangyo)) . "$hspacer</font></td>\n");
			if ($timecard_bean->all_legal_over_disp_flg != "f"){
				//法内残
				echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($wk_hoteinai_zan)) . "$hspacer</font></td>\n");
				//法外残
				echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp(minute_to_hmm($wk_hoteigai)) . "$hspacer</font></td>\n");
			}

			//深勤
			echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($time2) . "$hspacer</font></td>\n");
			//早出残業
			echo("<td class=\"$time_cell_class\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=".$time_font.">");
			echo(nbsp(hhmm_to_hmm($early_over_time)) . $hspacer);
			echo("</font></td>\n");
			//普外−＞外出に変更 20090603
			echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($time_outtime) . "$hspacer</font></td>\n");
			//残外−＞休憩に変更 20090603
			echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($time_rest) . "$hspacer</font></td>\n");

			if ($timecard_bean->ret_display_flag == "t"){
				$ret3_str = ($timecard_bean->return_icon_flg != "2") ? "復帰" : "呼出";
				//回数（$return_count）を削除し、時間に変更 20090603
				echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
				if ($return_apply_id == "") {
					echo(nbsp($return_time) . $hspacer);
				} else {
					if ($rtn_apply_status != "" && ($timecard_bean->over_apply_disp_flg == "t" || $timecard_bean->over_apply_disp_flg == "1")) {
						$return_apply_status = ($rtn_apply_status == "1") ? "{$ret3_str}承認済" : "{$ret3_str}否認済";
					} else {
						$return_apply_status = "";
					}
					echo(nbsp($return_time) . "<font size=\"2\" class=\"j10\">$return_apply_status</font>");
				}
				echo("</font></td>\n");
				//時間
				//			echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($return_time) . "$hspacer</font></td>\n");
			}
			//日数
			echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . $rslt_workday_count . "<br></font></td>\n");

			//会議研修表示
			if($meeting_display_flag){
				echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
				$meeting_time_hh = "";
				$meeting_time_mm = "";
				$tmp_meeting_time = 0;
				// 開始終了時刻から時間を計算する 20091008
				if ($meeting_start_time != "" && $meeting_end_time != "") {
					$tmp_meeting_start_date_time = $tmp_date.$meeting_start_time;
					if ($meeting_start_time <= $meeting_end_time) {
						$tmp_meeting_end_date_time = $tmp_date.$meeting_end_time;
					} else {
						// 日またがりの場合
						$tmp_meeting_end_date_time = next_date($tmp_date).$meeting_end_time;
					}
					$tmp_meeting_time = date_utils::get_time_difference($tmp_meeting_end_date_time, $tmp_meeting_start_date_time);
					$tmp_meeting_hhmm = $timecard_common_class->minute_to_h_hh($tmp_meeting_time);
					echo($tmp_meeting_hhmm);
				} else
					if ($meeting_time != null){
						$meeting_time_hh = (int)substr($meeting_time, 0, 2);
						$meeting_time_mm = (int)substr($meeting_time, 2, 4);

						echo($meeting_time_hh);
						$tmp_meeting_time = $meeting_time_hh*60;
						$tmp_meeting_time += $meeting_time_mm;
						if($meeting_time_mm == 15)
						{
							echo(".25");
						}
						else if($meeting_time_mm == 30)
						{
							echo(".5");
						}
						else if($meeting_time_mm == 45)
						{
							echo(".75");
						}

					}
				echo("&nbsp;</font></td>\n");
				$meeting_time_total += $tmp_meeting_time;
			}

			//最終更新日時・更新者
			echo("<td bgcolor=\"$bgcolor\" nowrap><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$update_time</font></td>\n");
			echo("<td bgcolor=\"$bgcolor\" nowrap><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$update_emp_name</font></td>\n");

			echo("</tr>\n");
		}
		//外出の合計
		$out_time_total += $time3;
		$out_time_total += $time4;


		//事由が休日出勤の場合
		//		if ($reason == "16") {
		//			$sums[31] += $work_time + $wk_return_time;
		//		}

		$tmp_date = next_date($tmp_date);
		$counter_for_week++;
		$wk_pos++;
	} // end of while

	// 早退時間を残業時間から減算する設定の場合、法定内残業、法定外残業の順に減算する
	if ($timecard_bean->early_leave_time_flg == "1") {

		list($hoteinai_zan, $hoteigai) = $atdbk_common_class->get_overtime_early_leave($hoteinai_zan, $hoteigai, $early_leave_time);
		//残業時間 20100713
		$sums[14] = $hoteinai_zan + $hoteigai;
	}
	// 要勤務日数を取得 20091222
	$wk_year = substr($yyyymm, 0, 4);
	$wk_mon = substr($yyyymm, 4, 2);
	$sums[0] = get_you_kinmu_su($con, $fname, $wk_year, $wk_mon, $arr_timecard_holwk_day, $closing);

	// 休日出勤 2008/10/23
	// 休日出勤のカウント方法変更 20091127
	//	$sums[2] = count($arr_hol_work_date);
	// 法定内 = 勤務時間 - 残業時間 20100713
	//$sums[28] = $hoteinai; //20100713
	$sums[28] = $sums[13] - $sums[14];
	// 法定内残業
	$sums[29] = $hoteinai_zan;
	// 法定外残業
	$sums[30] = $hoteigai;

	// 変則労働期間が月の場合
	if ($arr_irrg_info["irrg_type"] == "2") {

		// 所定労働時間を基準時間とする
		$tmp_irrg_minutes = $arr_irrg_info["irrg_minutes"];
		if ($hol_minus == "t") {
			$tmp_irrg_minutes -= (8 * 60 * $holiday_count);
		}
		$sums[12] = ($tmp_irrg_minutes > 0) ? $tmp_irrg_minutes : 0;

		// 稼働時間−基準時間を残業時間とする
		$sums[14] = $sums[13] - $sums[12];
	}
	else {
		// 基準時間の計算を変更 2008/10/21
		// 要勤務日数 × 所定労働時間
		$sums[12] = $sums[0] * date_utils::hi_to_minute($day1_time);
	}

	// 月集計値の稼働時間・残業時間・深夜勤務を端数処理
	for ($i = 13; $i <= 15; $i++) {
		if ($sums[$i] < 0) {
			$sums[$i] = 0;
		}
		$sums[$i] = round_time($sums[$i], $fraction4, $fraction4_min);
	}

	// 残業管理をしない場合、残業時間、法定内勤務、法定内残業、法定外残業を０とする
	if ($no_overtime == "t") {
		$sums[14] = 0;
		$sums[28] = 0;
		$sums[29] = 0;
		$sums[30] = 0;
	}

	// 月集計値を表示用に編集
	for ($i = 0; $i <= 9; $i++) {
		$sums[$i] .= "日";
	}
	for ($i = 10; $i <= 11; $i++) {
		$sums[$i] .= "回";
	}
	for ($i = 12; $i <= 15; $i++) {
		$sums[$i] = ($sums[$i] == 0) ? "0:00" : minute_to_hmm($sums[$i]);
	}
	for ($i = 16; $i <= 27; $i++) {
		$sums[$i] .= "日";
	}
	for ($i = 28; $i <= 31; $i++) {
		$sums[$i] = ($sums[$i] == 0) ? "0:00" : minute_to_hmm($sums[$i]);
	}
	//年末年始追加 20090908
	$sums[33] .= "日";
	//支給換算日数追加 20090908
	$sums[34] = $paid_day_count;

	$wk_colspan = 37;	//※残業申請不要分ユーザ画面より1つ少ない 20100921
	$wk_colspan1 = 10;
	$wk_colspan2 = 25;
	if ($timecard_bean->group_display_flag != "t"){
		$wk_colspan1--;
	}
	if ($timecard_bean->type_display_flag != "t"){
		$wk_colspan1--;
	}
	if ($timecard_bean->zen_display_flag != "t"){
		$wk_colspan1--;
	}
	if ($timecard_bean->yoku_display_flag != "t"){
		$wk_colspan1--;
	}
	if ($timecard_bean->ret_display_flag != "t"){
		$wk_colspan2 -= 6;
	}
	//病棟外
	if($meeting_display_flag != "t"){
		$wk_colspan2 -= 2;
	}
	//残業申請状況の表示
	if ($timecard_bean->all_apply_disp_flg == "f") {
		$wk_colspan2--;
	}
	//当直の表示
	if ($timecard_bean->all_duty_disp_flg == "f") {
		$wk_colspan2--;
	}
	//法定内残業用
	if ($timecard_bean->all_legal_over_disp_flg != "f"){
		$wk_colspan2++;
	}
	//合計表示、一括修正画面に追加
	$tbl1_total = "<tr >\n";
	$tbl1_total .= "<td colspan=\"{$wk_colspan1}\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	$tbl1_total .= "合計";
	$tbl1_total .= "</font></td>\n";

	$tbl2_total = "<tr bgcolor=\"#c2ceec\">\n";
	$tbl2_total .= "<td colspan=\"{$wk_colspan2}\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	$tbl2_total .= "&nbsp;";
	$tbl2_total .= "</font></td>\n";
	$tbl2_total .= "</tr>\n";

	$tbl3_total = "<tr >\n";
	$tbl3_total .= "<td colspan=\"2\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	$tbl3_total .= "&nbsp;";
	$tbl3_total .= "</font></td>\n";
	//勤務
	$tbl3_total .= "<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	$tbl3_total .= nbsp($sums[13]);
	$tbl3_total .= "</font></td>\n";
	//遅刻 20100907
	$tbl3_total .= "<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	$delay_time = ($delay_time == 0) ? "0:00" : minute_to_hmm($delay_time);
	$tbl3_total .= $delay_time;
	$tbl3_total .= "</font></td>\n";
	//早退 20100907
	$tbl3_total .= "<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	$early_leave_time = ($early_leave_time == 0) ? "0:00" : minute_to_hmm($early_leave_time);
	$tbl3_total .= $early_leave_time;
	$tbl3_total .= "</font></td>\n";
	//残業
	$tbl3_total .= "<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	$tbl3_total .= nbsp($sums[14]);
	$tbl3_total .= "</font></td>\n";
	if ($timecard_bean->all_legal_over_disp_flg != "f"){
		//法内残
		$tbl3_total .= "<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
		$tbl3_total .= nbsp($sums[29]);
		$tbl3_total .= "</font></td>\n";
		//法外残
		$tbl3_total .= "<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
		$tbl3_total .= nbsp($sums[30]);
		$tbl3_total .= "</font></td>\n";
	}
	//深夜勤務
	$tbl3_total .= "<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	$tbl3_total .= nbsp($sums[15]);
	$tbl3_total .= "</font></td>\n";
	//早出
	$tbl3_total .= "<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	$early_time_total = ($early_time_total == 0) ? "0:00" : minute_to_hmm($early_time_total);
	$tbl3_total .= $early_time_total;
	$tbl3_total .= "</font></td>\n";
	//外出
	$out_time_total = ($out_time_total == 0) ? "0:00" : minute_to_hmm($out_time_total);
	$tbl3_total .= "<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	$tbl3_total .= $out_time_total;
	$tbl3_total .= "</font></td>\n";
	//休憩に残業休憩も加算 20141209
	$rest_time_total = ($rest_time_total+$over_rest_time_total == 0) ? "0:00" : minute_to_hmm($rest_time_total+$over_rest_time_total);
	$tbl3_total .= "<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	$tbl3_total .= $rest_time_total;
	$tbl3_total .= "</font></td>\n";
	//退復
	if ($timecard_bean->ret_display_flag == "t"){
		$return_time_total = ($return_time_total == 0) ? "0:00" : minute_to_hmm($return_time_total);
        //呼出回数
        if ($return_count_total != 0) {
            $return_time_total .= " ".$return_count_total;
        }
        $tbl3_total .= "<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
		$tbl3_total .= $return_time_total;
		$tbl3_total .= "</font></td>\n";
	}
	//日数
	$tbl3_total .= "<td align=\"right\" nowrap width=\"32\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	$tbl3_total .= $sums[1];
	$tbl3_total .= "</font></td>\n";
	//病棟外
	if($meeting_display_flag){
		$meeting_time_total = $timecard_common_class->minute_to_h_hh($meeting_time_total);
		$tbl3_total .= "<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
		$tbl3_total .= $meeting_time_total;
		$tbl3_total .= "</font></td>\n";
	}

	$tbl3_total .= "<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	$tbl3_total .= "&nbsp;</font></td>\n";
	$tbl3_total .= "<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
	$tbl3_total .= "&nbsp;</font></td>\n";

	$tbl3_total .= "</tr>\n";

	$sums["tbl1"] = $tbl1;
	$sums["tbl2"] = $tbl2;
	$sums["tbl3"] = $tbl3;
	//合計行
	$sums["tbl1_total"] = $tbl1_total;
	$sums["tbl2_total"] = $tbl2_total;
	$sums["tbl3_total"] = $tbl3_total;

	return $sums;
}

?>
