<?php
// 出張旅費
// オプション設定・更新
require_once("about_comedix.php");

$fname = $_SERVER["PHP_SELF"];

// セッションと権限のチェック
$session = qualify_session($_REQUEST["session"], $fname);
$checkauth = check_authority($session, 3, $fname);
if ($session == "0" or $checkauth == "0") {
	js_login_exit();
}

// 入力項目の確認
if (empty($_REQUEST["long_daytrip_percentage"])) {
	js_alert_exit('「長期日帰出張の日当」が入力されていません。');
}
if (!is_numeric($_REQUEST["long_daytrip_percentage"])) {
	js_alert_exit('「長期日帰出張の日当」に数字以外が入力されています。');
}
if (empty($_REQUEST["long_stay_percentage"])) {
	js_alert_exit('「長期宿泊出張の日当」が入力されていません。');
}
if (!is_numeric($_REQUEST["long_stay_percentage"])) {
	js_alert_exit('「長期宿泊出張の日当」に数字以外が入力されています。');
}
if (empty($_REQUEST["long_hotel_percentage"])) {
	js_alert_exit('「長期宿泊出張の宿泊料」が入力されていません。');
}
if (!is_numeric($_REQUEST["long_hotel_percentage"])) {
	js_alert_exit('「長期宿泊出張の宿泊料」に数字以外が入力されています。');
}
if (empty($_REQUEST["oil_price"])) {
	js_alert_exit('「病院のガソリン購入額」が入力されていません。');
}
if (!is_numeric($_REQUEST["oil_price"])) {
	js_alert_exit('「病院のガソリン購入額」に数字以外が入力されています。');
}
if (!empty($_REQUEST["oil_price2"]) and !is_numeric($_REQUEST["oil_price2"])) {
	js_alert_exit('「病院のガソリン購入額2」に数字以外が入力されています。');
}
if (!empty($_REQUEST["oil_price2"]) and empty($_REQUEST["oil_price2_date"])) {
	js_alert_exit('「病院のガソリン購入額2」の日付が選択されていません。');
}
if (!empty($_REQUEST["oil_price3"]) and !is_numeric($_REQUEST["oil_price3"])) {
	js_alert_exit('「病院のガソリン購入額3」に数字以外が入力されています。');
}
if (!empty($_REQUEST["oil_price3"]) and empty($_REQUEST["oil_price3_date"])) {
	js_alert_exit('「病院のガソリン購入額3」の日付が選択されていません。');
}
if (empty($_REQUEST["oil_percentage"]) and $_REQUEST["oil_percentage"] !== '0') {
	js_alert_exit('「燃料費1リットル当りの金額」が入力されていません。');
}
if (!is_numeric($_REQUEST["oil_percentage"])) {
	js_alert_exit('「燃料費1リットル当りの金額」に数字以外が入力されています。');
}
if (empty($_REQUEST["normal_car_distance"])) {
	js_alert_exit('「車両区分による走行距離：普通車」が入力されていません。');
}
if (!is_numeric($_REQUEST["normal_car_distance"])) {
	js_alert_exit('「車両区分による走行距離：普通車」に数字以外が入力されています。');
}
if (empty($_REQUEST["small_car_distance"])) {
	js_alert_exit('「車両区分による走行距離：軽自動車」が入力されていません。');
}
if (!is_numeric($_REQUEST["small_car_distance"])) {
	js_alert_exit('「車両区分による走行距離：軽自動車」に数字以外が入力されています。');
}

$preticket_mail = @$_REQUEST["preticket_mail"] ? $_REQUEST["preticket_mail"] : array();

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// 登録されているかチェック
$sql = "SELECT * FROM trip_option";
$sel = select_from_table($con,$sql,"",$fname);
if ($sel == 0) {
	js_error_exit();
}

// UPDATE	
if (pg_num_rows($sel) > 0) {
	$set = array(
			"long_daytrip_percentage" => !empty($_REQUEST["long_daytrip_percentage"]) ? p($_REQUEST["long_daytrip_percentage"]) : 0,
			"long_stay_percentage" => !empty($_REQUEST["long_stay_percentage"]) ? p($_REQUEST["long_stay_percentage"]) : 0,
			"long_hotel_percentage" => !empty($_REQUEST["long_hotel_percentage"]) ? p($_REQUEST["long_hotel_percentage"]) : 0,
			"oil_price" => !empty($_REQUEST["oil_price"]) ? p($_REQUEST["oil_price"]) : 0,
			"oil_price2" => !empty($_REQUEST["oil_price2"]) ? p($_REQUEST["oil_price2"]) : 0,
			"oil_price3" => !empty($_REQUEST["oil_price3"]) ? p($_REQUEST["oil_price3"]) : 0,
			"oil_price2_date" => !empty($_REQUEST["oil_price2_date"]) ? p($_REQUEST["oil_price2_date"]) : "",
			"oil_price3_date" => !empty($_REQUEST["oil_price3_date"]) ? p($_REQUEST["oil_price3_date"]) : "",
			"oil_percentage" => !empty($_REQUEST["oil_percentage"]) ? p($_REQUEST["oil_percentage"]) : 0,
			"normal_car_distance" => !empty($_REQUEST["normal_car_distance"]) ? p($_REQUEST["normal_car_distance"]) : 0,
			"small_car_distance" => !empty($_REQUEST["small_car_distance"]) ? p($_REQUEST["small_car_distance"]) : 0,
			"updated_on" => date('Y-m-d H:i:s')
			);
	$sql = "UPDATE trip_option SET ";
	$upd = update_set_table($con, $sql, array_keys($set), array_values($set), "", $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		js_error_exit();
	}
}

// INSERT
else {
	$sql = "INSERT INTO trip_option values (";
	$content = array(
			!empty($_REQUEST["long_daytrip_percentage"]) ? p($_REQUEST["long_daytrip_percentage"]) : 0,
			!empty($_REQUEST["long_stay_percentage"]) ? p($_REQUEST["long_stay_percentage"]) : 0,
			!empty($_REQUEST["long_hotel_percentage"]) ? p($_REQUEST["long_hotel_percentage"]) : 0,
			!empty($_REQUEST["oil_price"]) ? p($_REQUEST["oil_price"]) : 0,
			!empty($_REQUEST["oil_price2"]) ? p($_REQUEST["oil_price2"]) : 0,
			!empty($_REQUEST["oil_price3"]) ? p($_REQUEST["oil_price3"]) : 0,
			!empty($_REQUEST["oil_price2_date"]) ? p($_REQUEST["oil_price2_date"]) : 0,
			!empty($_REQUEST["oil_price3_date"]) ? p($_REQUEST["oil_price3_date"]) : 0,
			!empty($_REQUEST["oil_percentage"]) ? p($_REQUEST["oil_percentage"]) : 0,
			!empty($_REQUEST["normal_car_distance"]) ? p($_REQUEST["normal_car_distance"]) : 0,
			!empty($_REQUEST["small_car_distance"]) ? p($_REQUEST["small_car_distance"]) : 0,
			date('Y-m-d H:i:s')
			);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		js_error_exit();
	}
}

// 前渡しチケットメール
$del = delete_from_table($con, "DELETE FROM trip_preticket_mail", "", $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}
foreach ($preticket_mail as $emp_id) {
	$ins = insert_into_table($con, "INSERT INTO trip_preticket_mail values (", array($emp_id), $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		js_error_exit();
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

?>
<script type="text/javascript">
location.href = 'workflow_trip_option.php?session=<?=$session?>';
</script>
