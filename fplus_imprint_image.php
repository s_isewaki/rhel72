<?
/*
 * 印影画像出力
 * パラメータ
 *  $session
 *  $emp_id
 *  $apv_flg 1:承認 2:否認 3:差戻し
 *  $imprint_flg 't':印影機能を利用する 'f','':印影機能を利用しない。既存の承認画像を表示。　※apv_flg=1の時、判定に使用
 *  $t ダミー※operaで画像再登録しても、キャッシュが利用されるらしく
 *             画像が変わらないためURLを変化させるため必要。呼出側で年月日時分秒を指定している。
 *  $approve_label 承認｜確認
 */
require_once("about_comedix.php");

//ページ名
$fname = $PHP_SELF;

//セッションチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$dirname = "fplus/workflow/imprint/";

$img_file = "img/spacer.gif";

switch ($apv_flg) {
case 1:
	if ($imprint_flg == "t") {
		$img_file1 = "$dirname$emp_id.gif";
		$img_file2 = "$dirname$emp_id.jpg";
// 当該職員の画像が登録済みであれば出力
		if (is_file($img_file1)) {
			$img_file = $img_file1;
		} else if (is_file($img_file2)) {
			$img_file = $img_file2;
		}
	} else {
		$img_file = ($approve_label == "確認") ? "img/confirmed.gif" : "img/accepted.gif";
	}

	break;
case 2:
	$img_file = "img/accept_ng.gif";

	break;
case 3:
	$img_file = "img/returned.gif";

	break;
}

// 運用ディレクトリ名を取得（末尾にスラッシュあり）
$scheme = ($_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
$dir = str_replace($_SERVER["DOCUMENT_ROOT"], "", $_SERVER["SCRIPT_FILENAME"]);
$dir = str_replace("fplus_imprint_image.php", "", $dir);
$url_base = $scheme . $_SERVER['HTTP_HOST'] . $dir;

if (is_file($img_file)) {
	header("Location: $url_base$img_file");
	exit;
}

/// 未登録のため空白画像を出力する
header("Location: {$url_base}img/spacer.gif");
