<?php
require_once("Cmx.php");
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("show_application_category_list.ini");
require_once("application_template.ini");
require_once("show_select_values.ini");
require_once("yui_calendar_util.ini");
require_once("get_values.ini");
require_once("application_draft_template.ini");
require_once("application_workflow_common_class.php");
require_once("application_common.ini");
require_once("get_values_for_template.ini");
require_once("library_common.php");
require_once("aclg_set.php");
//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,7,$fname);
if($wkfw=="0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}
// ワークフロー権限の取得
$workflow_auth = check_authority($session, 3, $fname);

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}
// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

$obj = new application_workflow_common_class($con, $fname);
//総合届フラグ
$general_flag = file_exists("opt/general_flag");
?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 決裁・申請 | 申請</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
<script type="text/javascript" src="js/domready.js"></script>
<script type="text/javascript" src="js/atdbk_close_stat.js?<?php echo time(); ?>"></script>
<?php
if($wkfw_id != "")
{

    if($apply_id == "")
    {
        // ワークフロー情報取得
        $arr_wkfwmst       = $obj->get_wkfwmst($wkfw_id);
        $wkfw_content      = $arr_wkfwmst[0]["wkfw_content"];
        $wkfw_content_type = $arr_wkfwmst[0]["wkfw_content_type"];
        $approve_label     = $arr_wkfwmst[0]["approve_label"];

        // 本文形式タイプのデフォルトを「テキスト」とする
        if ($wkfw_content_type == "") {$wkfw_content_type = "1";}

        // 本文形式タイプのデフォルトを「テンプレート」の場合
        if($wkfw_content_type == "2")
        {
            $wkfw_history_no = $obj->get_max_wkfw_history_no($wkfw_id);
        }

        $num = 0;
        $pos = 0;
        while (1) {
            $pos = strpos($wkfw_content, 'show_cal', $pos);
            if ($pos === false) {
                break;
            } else {
                $num++;
            }
            $pos++;
        }

        // 外部ファイルを読み込む（$num==0でも読み込んでください）
        write_yui_calendar_use_file_read_0_12_2();
        // カレンダー作成、関数出力
        write_yui_calendar_script2($num);

        if ($mode == "apply_printwin") {

        //---------テンプレートの場合XML形式のテキスト$contentを作成---------

            if ($wkfw_content_type == "2") {
                // XML変換用コードの保存
                $wkfw_content_xml = ereg_replace("^.*<\? // XML", "<? // XML", $wkfw_content);
                $savexmlfilename = "workflow/tmp/{$session}_x.php";
                $fp = fopen($savexmlfilename, "w");

                if (!$fp) {
                    echo("<script language='javascript'>alert('一時ファイルがオープンできません。再度、印刷してください。$savexmlfilename');</script>");
//                  echo("<script language='javascript'>history.back();</script>");
                    $print_failed = true;
                }
                if(!fwrite($fp, $wkfw_content_xml, 2000000)) {
                    fclose($fp);
                    echo("<script language='javascript'>alert('一時ファイルに書込みできません。再度、印刷してください。');</script>");
//                  echo("<script language='javascript'>history.back();</script>");
                    $print_failed = true;
                }
                fclose($fp);

                include( $savexmlfilename );

            }
            $savexmlfilename = "workflow/tmp/{$session}_d.php";
            $fp = fopen($savexmlfilename, "w");

            if (!$fp) {
                echo("<script language='javascript'>alert('一時ファイルがオープンできません。再度、印刷してください。$savexmlfilename');</script>");
//              echo("<script language='javascript'>history.back();</script>");
                $print_failed = true;
            }

            if ($content == "") {
                $content = " ";
            }
            if(!fwrite($fp, $content, 2000000)) {
                fclose($fp);
                echo("<script language='javascript'>alert('一時ファイルに書込みできません。再度、印刷してください。');</script>");
//              echo("<script language='javascript'>history.back();</script>");
                $print_failed = true;
            }

            fclose($fp);
        }
    }
    else
    {
        $arr_apply_wkfwmst = $obj->get_apply_wkfwmst($apply_id);
        $wkfw_content      = $arr_apply_wkfwmst[0]["wkfw_content"];
        $wkfw_content_type = $arr_apply_wkfwmst[0]["wkfw_content_type"];
        $apply_content     = $arr_apply_wkfwmst[0]["apply_content"];
        $wkfw_history_no   = $arr_apply_wkfwmst[0]["wkfw_history_no"];
        $approve_label     = $arr_apply_wkfwmst[0]["approve_label"];

        // ワークフロー情報取得
        /*
        $sel_apply_wkfwmst = search_apply_wkfwmst($con, $fname, $target_apply_id);
        $wkfw_content = pg_fetch_result($sel_apply_wkfwmst, 0, "wkfw_content");
        $wkfw_content_type = pg_fetch_result($sel_apply_wkfwmst, 0, "wkfw_content_type");
        $apply_content = pg_fetch_result($sel_apply_wkfwmst, 0, "apply_content");
        */
        // 本文形式タイプのデフォルトを「テキスト」とする
        if ($wkfw_content_type == "") {$wkfw_content_type = "1";}

        // 形式をテキストからテンプレートに変更した場合の古いデータ対応
        // wkfw_content_typeは2:テンプレートだが、登録データがXMLでない場合、1:テキストとして処理する
        if ($wkfw_content_type == "2") {
            if (strpos($apply_content, "<?xml") === false) {
                $wkfw_content_type = "1";
            }
        }

        if($wkfw_history_no != "")
        {
            $arr_wkfw_template_history = $obj->get_wkfw_template_history($wkfw_id, $wkfw_history_no);
            $wkfw_content = $arr_wkfw_template_history["wkfw_content"];
        }

        $num = 0;
        if ($wkfw_content_type == "2") {
            $pos = 0;
            while (1) {
                $pos = strpos($wkfw_content, 'show_cal', $pos);
                if ($pos === false) {
                    break;
                } else {
                    $num++;
                }
                $pos++;
            }
        }

        // 外部ファイルを読み込む（$num==0でも読み込んでください）
        write_yui_calendar_use_file_read_0_12_2();
        // カレンダー作成、関数出力
        write_yui_calendar_script2($num);

        if ($mode == "apply_printwin") {

        //---------テンプレートの場合XML形式のテキスト$contentを作成---------

            if ($wkfw_content_type == "2") {
                // XML変換用コードの保存
                $wkfw_content_xml = ereg_replace("^.*<\? // XML", "<? // XML", $wkfw_content);
                $savexmlfilename = "workflow/tmp/{$session}_x.php";
                $fp = fopen($savexmlfilename, "w");

                if (!$fp) {
                    echo("<script language='javascript'>alert('一時ファイルがオープンできません。再度、印刷してください。$savexmlfilename');</script>");
//                  echo("<script language='javascript'>history.back();</script>");
                    $print_failed = true;
                }
                if(!fwrite($fp, $wkfw_content_xml, 2000000)) {
                    fclose($fp);
                    echo("<script language='javascript'>alert('一時ファイルに書込みできません。再度、印刷してください。');</script>");
//                  echo("<script language='javascript'>history.back();</script>");
                    $print_failed = true;
                }
                fclose($fp);

                include( $savexmlfilename );

            }
            $savexmlfilename = "workflow/tmp/{$session}_d.php";
            $fp = fopen($savexmlfilename, "w");

            if (!$fp) {
                echo("<script language='javascript'>alert('一時ファイルがオープンできません。再度、印刷してください。$savexmlfilename');</script>");
//              echo("<script language='javascript'>history.back();</script>");
                $print_failed = true;
            }

            if ($content == "") {
                $content = " ";
            }
            if(!fwrite($fp, $content, 2000000)) {
                fclose($fp);
                echo("<script language='javascript'>alert('一時ファイルに書込みできません。再度、印刷してください。');</script>");
//              echo("<script language='javascript'>history.back();</script>");
                $print_failed = true;
            }
            fclose($fp);
        }
    }
    // 合議者（事前登録職員）
    $councli_emp_list = $obj->get_council_list_name($wkfw_id,"ALIAS");
    $council_emp_id = "";
    $council_emp_nm = "";
    $council_emp_type = ""; // 事前登録職員に'1'を設定する。クリアボタンを押しても消せないようにするため
    foreach($councli_emp_list as $wkfw_council)
    {
	if($council_emp_id != "")
	{
	    $council_emp_id .= ",";
	    $council_emp_nm .= ", ";
	    $council_emp_type .= ",";
	}
	$council_emp_id .= $wkfw_council["emp_id"];
	$council_emp_nm .= $wkfw_council["emp_name"];
	$council_emp_type .= '1';
    }

    if ( $apply_id != "" ){ // 下書きを読み出すとき
	// 合議者（申請時に追加した職員）
	$councli_emp_list = $obj->get_apply_council_namelist2($apply_id);
	$council_emp_id2 = array();
	$council_emp_nm2 = array();
	$council_emp_tp2 = array();
	$cnt=0;
	foreach($councli_emp_list as $wkfw_council)
	{
		$council_emp_id2[$cnt] = $wkfw_council["emp_id"];
		$council_emp_nm2[$cnt] = $wkfw_council["name"];
		$council_emp_tp2[$cnt] = "2"; // 削除可
		$cnt++;
	}
    }

	$council_ret = $obj->get_council_list($wkfw_id, "ALIAS");
	$consultation_set_flag = $council_ret[1];
}

$approve_label = ($approve_label != "2") ? "承認" : "確認";

?>
<script type="text/javascript">
var _session = '<?php echo $session; ?>';
var close_stat = "0";
function init() {


    if('<?=$back?>' == 't') {
<?php

        $approve_num = $_POST[approve_num];
        for($i=1; $i<=$approve_num;$i++) {

            $regist_emp_id = "regist_emp_id$i";
            $radio_emp_id = "radio_emp_id$i";
            $approve_name = "approve_name$i";

?>
            var radio_emp_id = 'radio_emp_id' + <?=$i?>;
            if(document.getElementById('<?=$radio_emp_id?>')) {
                for(j=0; j<document.apply.elements['<?=$radio_emp_id?>'].length; j++) {
                    if(document.apply.elements['<?=$radio_emp_id?>'][j].value == '<?=$_POST[$regist_emp_id];?>') {
                        document.apply.elements['<?=$radio_emp_id?>'][j].checked = true;
                    }
                }
                document.apply.elements['<?=$regist_emp_id?>'].value = '<?=$_POST[$regist_emp_id];?>';
            }
            if(document.getElementById('<?=$approve_name?>')) {
                document.apply.elements['<?=$approve_name?>'].value = '<?=$_POST[$approve_name];?>';
                document.apply.elements['<?=$regist_emp_id?>'].value = '<?=$_POST[$regist_emp_id];?>';
            }
<?php
        }
?>
    }
}

function attachFile() {
    window.open('apply_attach.php?session=<?php echo($session); ?>', 'newwin', 'width=640,height=480,scrollbars=yes');
}

function detachFile(e) {
    if (e == undefined) {
        e = window.event;
    }

    var btn_id;
    if (e.target) {
        btn_id = e.target.getAttribute('id');
    } else {
        btn_id = e.srcElement.id;
    }
    var id = btn_id.replace('btn_', '');

    var p = document.getElementById('p_' + id);
    document.getElementById('attach').removeChild(p);
}

// 申請処理
function apply_regist(apv_num, precond_num) {
<?php
//総合届の場合は締めチェックを除く
if (!$general_flag) {
?>
	if (document.apply.elements['kintai_template_type']) {
	    var kintai_template_type = document.apply.elements['kintai_template_type'].value;
	    if (kintai_template_type == 'hol'){
		    var apply_emp_id = document.apply.elements['apply_emp_id'].value;
		    var chkdate1 = document.apply.elements['date_y2'].value +
						   document.apply.elements['date_m2'].value +
						   document.apply.elements['date_d2'].value;
		    var chkdate2 = document.apply.elements['date_y3'].value +
						   document.apply.elements['date_m3'].value +
						   document.apply.elements['date_d3'].value;
		    get_close_stat_ajax(apply_emp_id, chkdate1, chkdate2);
		    if (close_stat == "1") {
				alert('出勤表の勤務時間集計画面で締め済みのため申請できません。');
				return false;
			}
		}
	}
<?php } ?>
    // 未入力チェック
    for(i=1; i<=precond_num; i++)
    {
        obj = 'precond_apply_id' + i;
        apply_id = document.apply.elements[obj].value;
        if(apply_id == "") {
            alert('<?php echo($approve_label); ?>確定の申請書を設定してくだい。');
            return;
        }
    }

    var apv_selected = false;
    for (var i = 1; i <= apv_num; i++) {
        // 承認者番号（i）は階層番号とは異なるものだが、とりあえずこれでradioを取得する
        var radios = document.apply.elements['radio_emp_id' + i];
        if (radios) {
            var radio_selected = false;
            for (var j = 0, k = radios.length; j < k; j++) {
                if (radios[j].checked) {
                    apv_selected = true;
                    radio_selected = true;
                    break;
                }
            }
            if (!radio_selected) {
                alert(i + '階層目の<?php echo($approve_label); ?>者が指定されていないため送信できません。');
                return;
            }
        }
        if (!apv_selected && document.apply.elements['regist_emp_id' + i].value != '') {
            apv_selected = true;
        }
    }
    if (!apv_selected) {
        alert('<?php echo($approve_label); ?>者が指定されていないため送信できません。');
        return;
    }

    if (window.InputCheck) {
        if (!InputCheck()) {
            return;
        }
    }

    // 重複チェック
    dpl_flg = false;
    for(i=1; i<=apv_num; i++) {
        obj_src = 'regist_emp_id' + i;
        emp_id_src = document.apply.elements[obj_src].value;
        if (emp_id_src == '') continue;

        for(j=i+1; j<=apv_num; j++) {
            obj_dist = 'regist_emp_id' + j;
            emp_id_dist = document.apply.elements[obj_dist].value;
            if(emp_id_src == emp_id_dist) {
                dpl_flg = true;
            }
        }
    }

    if(dpl_flg)
    {
        if (!confirm('<?php echo($approve_label); ?>者が重複していますが、申請しますか？'))
        {
                return;
        }
    }
    document.apply.action="application_submit.php";
    document.apply.submit();

}

// 下書き保存
function draft_regist()
{
    document.apply.action="application_submit.php?draft=on";
    document.apply.submit();
}

function apply_printwin(id) {
    document.apply.mode.value = "apply_printwin";
    if(id == "")
    {
        document.apply.action="application_menu.php";
    }
    else
    {
        document.apply.action="application_menu.php?apply_id=" + id;
    }
    var default_target = document.apply.target;
    document.apply.target = 'printframe';
    document.apply.submit();
    document.apply.target = default_target;
}


function apply_draft(id, apv_num, precond_num)
{

    // 未入力チェック
    for(i=1; i<=precond_num; i++)
    {
        obj = 'precond_apply_id' + i;
        apply_id = document.apply.elements[obj].value;
        if(apply_id == "") {
            alert('<?php echo($approve_label); ?>確定の申請書を設定してくだい。');
            return;
        }
    }

    var apv_selected = false;
    for (var i = 1; i <= apv_num; i++) {
        // 承認者番号（i）は階層番号とは異なるものだが、とりあえずこれでradioを取得する
        var radios = document.apply.elements['radio_emp_id' + i];
        if (radios) {
            var radio_selected = false;
            for (var j = 0, k = radios.length; j < k; j++) {
                if (radios[j].checked) {
                    apv_selected = true;
                    radio_selected = true;
                    break;
                }
            }
            if (!radio_selected) {
                alert(i + '階層目の<?php echo($approve_label); ?>者が指定されていないため送信できません。');
                return;
            }
        }
        if (!apv_selected && document.apply.elements['regist_emp_id' + i].value != '') {
            apv_selected = true;
        }
    }
    if (!apv_selected) {
        alert('<?php echo($approve_label); ?>者が指定されていないため送信できません。');
        return;
    }

    if (window.InputCheck) {
        if (!InputCheck()) {
            return;
        }
    }

    // 重複チェック
    dpl_flg = false;
    for(i=1; i<=apv_num; i++) {
        obj_src = 'regist_emp_id' + i;
        emp_id_src = document.apply.elements[obj_src].value;
        if (emp_id_src == '') continue;

        for(j=i+1; j<=apv_num; j++) {
            obj_dist = 'regist_emp_id' + j;
            emp_id_dist = document.apply.elements[obj_dist].value;

            if(emp_id_src == emp_id_dist) {
                dpl_flg = true;
            }
        }
    }

    if(dpl_flg)
    {
        if (!confirm('<?php echo($approve_label); ?>者が重複していますが、申請しますか？'))
        {
                return;
        }
    }

    document.apply.action="application_draft_submit.php?apply_id=" + id;
    document.apply.submit();

}

function draft_update(id)
{
    document.apply.action="application_draft_submit.php?apply_id=" + id + "&draft=on";
    document.apply.submit();
}

function draft_delete(id)
{
    document.apply.action="application_draft_delete.php?apply_id=" + id;
    document.apply.submit();
}


// テキストエリア行数の自動拡張
var ResizingTextArea = Class.create();

ResizingTextArea.prototype = {
    initialize: function(field)
    {
        this.resizeNeeded = this.resizeNeeded.bindAsEventListener(this);
        Event.observe(field, "click", this.resizeNeeded);
        Event.observe(field, "keyup", this.resizeNeeded);
    },

    resizeNeeded: function(event)
    {
        var t = Event.element(event);
        t.style.height = '';
        if (t.scrollHeight > t.clientHeight) {
            t.style.height = (t.scrollHeight + 5) + 'px';
        }
    }
}

// 全テキストエリアの行数変更
function resizeAllTextArea() {
    var objs = document.getElementsByTagName('textarea');
    for (var i = 0, j = objs.length; i < j; i++) {
        var t = objs[i];
        if (t.scrollHeight > t.clientHeight) {
            t.style.height = (t.scrollHeight + 5) + 'px';
        }
    }
}
// 合議者対応。application_template.iniの「申請者以外の結果通知」から流用
// 職員リストへ職員追加
// application_template.iniのadd_target_listから呼び出される
function council_add_target_list(emp_id, emp_nm) {
	var emp_ids = emp_id.split(", ");
	var emp_nms = emp_nm.split(", ");

	var existed_emp_ids = document.getElementById('council_emp_id').value;
	if (existed_emp_ids.length == 0) {
		existed_emp_ids = new Array();
	} else {
		existed_emp_ids = existed_emp_ids.split(",");
	}
	var existed_emp_nms = document.getElementById('council_emp_nm').value;
	if (existed_emp_nms.length == 0) {
		existed_emp_nms = new Array();
	} else {
		existed_emp_nms = existed_emp_nms.split(",");
	}
	var existed_emp_tps = document.getElementById('council_emp_tp').value;
	if (existed_emp_tps.length == 0) {
		existed_emp_tps = new Array();
	} else {
		existed_emp_tps = existed_emp_tps.split(",");
	}

	for (var i = 0, j = emp_ids.length; i < j; i++) {
		var dpl_flg = false;
		for (var m = 0, n = existed_emp_ids.length; m < n; m++) {
			if (emp_ids[i] == existed_emp_ids[m]) {
				dpl_flg = true;
				break;
			}
		}
		if (dpl_flg) {
			continue;
		}
		existed_emp_ids.push(emp_ids[i]);
		existed_emp_nms.push(emp_nms[i]);
		existed_emp_tps.push("2");
	}

	existed_emp_ids = existed_emp_ids.join(",");
	existed_emp_nms = existed_emp_nms.join(",");
	existed_emp_tps = existed_emp_tps.join(",");

	document.getElementById('council_emp_id').value = existed_emp_ids;
	document.getElementById('council_emp_nm').value = existed_emp_nms;
	document.getElementById('council_emp_tp').value = existed_emp_tps;

	council_set_disp_area(existed_emp_ids, existed_emp_nms, existed_emp_tps);
}
// 合議者対応、追加職員を表示
function council_set_disp_area(emp_id, emp_nm, emp_type) {
    disp_area = "disp_council_list";

    arr_emp_id = emp_id.split(",");
    arr_emp_nm = emp_nm.split(",");
    arr_emp_type = emp_type.split(",");

    disp_area_content = "";
    for(i=0; i<arr_emp_id.length; i++)
    {
	if(i > 0)
	{
	    disp_area_content += ', ';
	}

	if(arr_emp_type[i] == '2')
	{
	    disp_area_content += "<a href=\"javascript:council_delete_emp('" + arr_emp_id[i] + "', '" + arr_emp_nm[i] + "');\">";
	    disp_area_content += arr_emp_nm[i];
	    disp_area_content += "</a>";
	}
	else
	{
	    disp_area_content += arr_emp_nm[i];
	}
    }
    document.getElementById(disp_area).innerHTML = disp_area_content;
}

// 合議者対応、職員を選んで削除
function council_delete_emp(del_emp_id, del_emp_nm){
    if(confirm("「" + del_emp_nm + "」さんを削除します。よろしいですか？"))
    {
	emp_ids = document.getElementById('council_emp_id').value;
	emp_nms = document.getElementById('council_emp_nm').value;
	emp_tps = document.getElementById('council_emp_tp').value;

	new_emp_ids = "";
	new_emp_nms = "";
	new_emp_types = "";

	cnt = 0;
	if(emp_ids != "") {

	    arr_emp_id = emp_ids.split(",");
	    arr_emp_nm = emp_nms.split(",");
	    arr_emp_tp = emp_tps.split(",");

	    for(i=0; i<arr_emp_id.length; i++)
	    {
		if(arr_emp_id[i] == del_emp_id)
		{
		    continue;
		}
		else
		{
		    if(cnt > 0)
		    {
			new_emp_ids += ",";
			new_emp_nms += ", ";
			new_emp_types += ",";
		    }
		    new_emp_ids += arr_emp_id[i];
		    new_emp_nms += arr_emp_nm[i];
		    new_emp_types += arr_emp_tp[i];
		    cnt++;
		}
	    }
	}
	document.getElementById('council_emp_id').value = new_emp_ids;
	document.getElementById('council_emp_nm').value = new_emp_nms;
	document.getElementById('council_emp_tp').value = new_emp_types;
	council_set_disp_area(new_emp_ids, new_emp_nms, new_emp_types);
    }

}
// 合議者対応、職員を全クリア。但し事前登録職員は消えない
function council_clear_emp(){
    council_emp_id = document.getElementById('council_emp_id').value;
    council_emp_nm = document.getElementById('council_emp_nm').value;
    council_emp_tp = document.getElementById('council_emp_tp').value;

    arr_council_emp_id = council_emp_id.split(",");
    arr_council_emp_nm = council_emp_nm.split(",");
    arr_council_emp_tp = council_emp_tp.split(",");

    tmp_emp_id = '';
    tmp_emp_nm = '';
    tmp_emp_tp = '';

    for(i=0; i<arr_council_emp_id.length; i++)
    {
	if(arr_council_emp_tp[i] != 2)
	{
	    if(tmp_emp_id != "")
	    {
		tmp_emp_id += ",";
		tmp_emp_nm += ", ";
		tmp_emp_tp += ",";
	    }

	    tmp_emp_id += arr_council_emp_id[i]
	    tmp_emp_nm += arr_council_emp_nm[i];
	    tmp_emp_tp += arr_council_emp_tp[i];
	}
    }

    document.getElementById('disp_council_list').innerHTML = tmp_emp_nm;
    document.getElementById('council_emp_id').value = tmp_emp_id;
    document.getElementById('council_emp_nm').value = tmp_emp_nm;
    document.getElementById('council_emp_tp').value = tmp_emp_tp;
}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">


.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}

table.block {border-collapse:collapse;}
table.block td {border:#5279a5 solid 1px;padding:1px;}
table.block td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#5279a5 solid 0px;}

table.block2 {border-collapse:collapse;}
table.block2 td {border:#5279a5 solid 1px;padding:1px;}
table.block2 td {border-width:1;}

table.block3 {border-collapse:collapse;}
table.block3 td {border:#5279a5 solid 0px;}
table.block3 td {border-width:0;}


p {margin:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="init();
<?php if ($wkfw_id != "") { ?>
initcal();
<?php } ?>
<?php
if ($mode == "apply_printwin" && !$print_failed)
{
	$precond_titles = "";
	foreach ($_REQUEST as $key => $val) {
		if (strpos($key, "precond_title") === 0) {
			$precond_titles .= "&$key=" . urlencode($val);
		}
	}

if($wkfw_id != "" && $apply_id == "")
{
?>
window.open('application_template_print.php?session=<?=$session?>&fname=<?=$fname?>&wkfw_id=<?=$wkfw_id?>&back=f&mode=apply_print<?=$precond_titles?>', 'apply_print', 'width=770,height=700,scrollbars=yes');
<?php
}
else if($wkfw_id != "" && $apply_id != "")
{
?>
window.open('application_draft_template_print.php?session=<?=$session?>&fname=<?=$fname?>&target_apply_id=<?=$apply_id?>&back=f&mode=apply_print<?=$precond_titles?>', 'apply_print', 'width=770,height=700,scrollbars=yes');
<?php
}
?>

<?php
}
?>
if (window.load_action){load_action();};
if (window.OnloadSub) { OnloadSub(); };resizeAllTextArea();if (window.refreshApproveOrders) {refreshApproveOrders();}">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="application_menu.php?session=<?php echo($session); ?>"><img src="img/icon/b08.gif" width="32" height="32" border="0" alt="決裁・申請"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="application_menu.php?session=<?php echo($session); ?>"><b>決裁・申請</b></a> &gt; <a href="application_menu.php?session=<?php echo($session); ?>"><b>申請</b></a></font></td>
<?php if ($workflow_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="workflow_menu.php?session=<?php echo($session); ?>&workflow=1"><b>管理画面へ</b></a></font></td>
<?php } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<?php show_applycation_menuitem($session,$fname,""); ?>

</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>

<form name="apply" action="#" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="wkfw_id" value="<?=$wkfw_id?>">
<input type="hidden" name="wkfw_type" value="<?=$wkfw_type?>">
<input type="hidden" name="mode" value="">
<input type="hidden" name="wkfw_history_no" value="<?=$wkfw_history_no?>">

<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td valign="top" width="25%">
<?php show_application_cate_list($con, $session, $fname, $wkfw_type, $wkfw_id, $apply_id); ?>
</td>
<td><img src="img/spacer.gif" width="5" height="2" alt=""></td>
<td valign="top" width="75%">
<?php
if($wkfw_id != "" && $apply_id == "")
{
    show_application_template($con, $session, $fname, $wkfw_id, $apply_title, $content, $file_id, $filename, $back, $mode);
}
else if($wkfw_id != "" && $apply_id != "")
{
    show_draft_template($con, $session, $fname, $apply_id, $apply_title, $content, $file_id, $filename, $back, $mode);
}
?>
</td>
</tr>
</table>
<!--/form-->
<?php if ($wkfw_id != "" && $consultation_set_flag == "on") { ?>
<!--form name="council"-->
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">合議者</font>&nbsp
<input type="button" value="職員名簿" style="margin-left:2em;width: 5.5em;" onclick="openEmployeeList('20', '');">
<input type="button" value=" クリア " style="margin-left:2em;width: 5.5em;" onclick="council_clear_emp();">
<!--[if IE]>
<table width="690" border="0" cellspacing="0" cellpadding="2" class="list">
<![endif]-->
<!--[if ! IE]>-->
<table width="795" border="0" cellspacing="0" cellpadding="2" class="list">
<!--<![endif]-->
    <col class="col1" width="680"/>
<tr>
</tr>
<tr><td height="40"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><span id="disp_council_list"></span></font></td></tr>
</table>
<input type="hidden" name="council_emp_id" id="council_emp_id" value="">
<input type="hidden" name="council_emp_nm" id="council_emp_nm" value="">
<input type="hidden" name="council_emp_tp" id="council_emp_tp" value="">
<script type="text/javascript">
document.getElementById("council_emp_id").value = '<?=$council_emp_id?>';
document.getElementById("council_emp_nm").value = "<?=$council_emp_nm?>";
document.getElementById("council_emp_tp").value = '<?=$council_emp_type?>';
document.getElementById("disp_council_list").innerHTML = "<?php echo $council_emp_nm; ?>";
<?php for($i=0; $i<count($council_emp_id2); $i++ ) { ?>
var emp_id = '<?php echo $council_emp_id2[$i]; ?>';
var emp_nm = '<?php echo $council_emp_nm2[$i]; ?>';
var type   = '<?php echo $council_emp_tp2[$i]; ?>';
council_add_target_list(emp_id, emp_nm, type);
<?php } ?>
</script>
<?php } ?>
</td>
</tr>
</table>
</form>
<iframe name="printframe" width="0" height="0" frameborder="0"></iframe>
</body>
</html>
<?php
// workflow/tmpディレクトリ作成
$obj->create_wkfwtmp_directory();
// apply/tmpディレクトリ作成
$obj->create_applytmp_directory();
pg_close($con);
