<?
require_once("Cmx.php");
require_once("aclg_set.php");
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");
require_once("about_postgres.php");
require_once("hiyari_common.ini");
require_once("hiyari_reporting_common.ini");
require_once("hiyari_mail.ini");
require_once("show_class_name.ini");

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//====================================
// 権限チェック
//====================================
$auth_chk = check_authority($session, 48, $fname);
if ($auth_chk == '0') {
    showLoginPage();
    exit;
}


//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

//====================================
// 画面遷移パラメータ(担当者権限)取得
// (SM・RM・RA・SD・MD・HD)
//====================================
$auth = $_GET["auth"];
//指定されていない場合は先頭の権限
if($auth == "")
{
    $auth_list = get_inci_mst($con, $fname);
    foreach($auth_list as $auth_list_key => $auth_list_value)
    {
        if(is_usable_auth($auth_list_key,$fname,$con))
        {
            $auth = $auth_list_key;
            break;
        }
    }
}


//====================================
// 処理分岐
//====================================
$mode = $_POST["mode"];
switch ($mode) {
    case "regist":
        // 登録処理
        regist($con, $fname, $auth, $_POST);
        break;
    default:
        break;
}


//====================================
// 初期化処理
//====================================

// 組織階層情報取得
$arr_class_name = get_class_name_array($con, $fname);

// 権限名取得
$arr_auth_name = sel_auth_name($con, $fname);



// 【役職】決裁者情報取得
$decide_use_flg = "f";
$decide_target_class = "1";
$decide_rm_flg = "f";
$decide_ra_flg = "f";
$decide_md_flg = "f";
$decide_sd_flg = "f";
$decide_hd_flg = "f";
$decide_arr_post = array();
if($auth == 'RM' || $auth == 'RA' || $auth == 'SD' || $auth == 'MD' || $auth == 'HD') {
    $sel_post = search_inci_auth_emp_post($con, $fname, $auth, '決裁者');
    $cnt=0;
    while ($row_post = pg_fetch_array($sel_post)) {

        if($cnt == 0) {
            $decide_use_flg = $row_post["use_flg"];
            $decide_target_class = $row_post["target_class"];
            $decide_rm_flg = $row_post["rm_flg"];
            $decide_ra_flg = $row_post["ra_flg"];
            $decide_md_flg = $row_post["md_flg"];
            $decide_sd_flg = $row_post["sd_flg"];
            $decide_hd_flg = $row_post["hd_flg"];
        }
        array_push($decide_arr_post, $row_post["emp_st"]);
        $cnt++;
    }
}

// 【役職】審議者情報取得
$discuss_use_flg = "f";
$discuss_target_class = "1";
$discuss_rm_flg = "f";
$discuss_ra_flg = "f";
$discuss_sd_flg = "f";
$discuss_arr_post = array();
if($auth == 'RM' || $auth == 'RA' || $auth == 'SD') {
    $sel_post = search_inci_auth_emp_post($con, $fname, $auth, '審議者');
    $cnt=0;
    while ($row_post = pg_fetch_array($sel_post)) {

        if($cnt == 0) {
            $discuss_use_flg = $row_post["use_flg"];
            $discuss_target_class = $row_post["target_class"];
            $discuss_rm_flg = $row_post["rm_flg"];
            $discuss_ra_flg = $row_post["ra_flg"];
            $discuss_sd_flg = $row_post["sd_flg"];
        }
        array_push($discuss_arr_post, $row_post["emp_st"]);
        $cnt++;
    }
}

// 【職員】決裁者情報取得
$arr_decide_target = search_inci_auth_emp($con, $fname, $auth, '決裁者');
// 【職員】審議者情報取得
if($auth == 'SM' || $auth == 'RM' || $auth == 'RA' || $auth == 'SD') {//MD,HD以外
    $arr_discuss_target = search_inci_auth_emp($con, $fname, $auth, '審議者');
}






// 役職マスタ情報取得(リスト項目の値取得)
$arr_manager_post = array();
$sql = "select st_id,st_nm from stmst ";
$cond = "where st_del_flg = 'f' order by order_no";
$sel = select_from_table($con,$sql,$cond,$fname);       //役職一覧を取得
if($sel == 0){
    pg_close($con);
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showErrorPage(window);</script>");
    exit;
}
while ($row = pg_fetch_array($sel)) {

    $tmp_st_id = $row["st_id"];
    $tmp_st_nm = $row["st_nm"];
    array_push($arr_manager_post, array("id" => $tmp_st_id, "name" => $tmp_st_nm));
}



// 代行者機能利用設定テーブル情報取得
if($auth == "SM" || $auth == "RM" || $auth == "RA" || $auth == "SD") {
    $agent_use_flg = search_inci_agent_use($con, $fname, $auth);
}
?>


<?
//====================================
// HTML
//====================================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$INCIDENT_TITLE?> | 担当者設定</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script language="javascript">

function initPage() {

//役職指定なしに対するdisable制御
<? if($auth == 'RM' || $auth == 'RA' || $auth == 'SD' || $auth == 'MD' || $auth == 'HD') {?>

    //役職指定なしの場合(決裁者に対する処理)
    <? if($decide_use_flg == 'f') { ?>

        //部署指定をdisableにし、先頭を選択
        for (var i = 0; i < document.mainform.decide_target_class.length; i++) {
            document.mainform.decide_target_class[i].disabled = true;
        }
        document.mainform.decide_target_class[0].checked = true;

        //役職指定をdisableにする
        for (var i = 0; i < document.mainform.decide_mng_post.length; i++) {
            document.mainform.decide_mng_post[i].disabled = true;
        }

        //安全管理役職指定をdisableにする
        <? if($auth == 'RM') { ?>
        for (var i = 0; i < document.mainform.decide_rm_flg.length; i++) {
            if(document.mainform.decide_rm_flg[i].type == "checkbox") {
                document.mainform.decide_rm_flg[i].disabled = true;
            }
        }
        <?}?>
        <? if($auth == 'RA') { ?>
        for (var i = 0; i < document.mainform.decide_ra_flg.length; i++) {
            if(document.mainform.decide_ra_flg[i].type == "checkbox") {
                document.mainform.decide_ra_flg[i].disabled = true;
            }
        }
        <?}?>
        <? if($auth == 'SD') { ?>
        for (var i = 0; i < document.mainform.decide_sd_flg.length; i++) {
            if(document.mainform.decide_sd_flg[i].type == "checkbox") {
                document.mainform.decide_sd_flg[i].disabled = true;
            }
        }
        <?}?>
        <? if($auth == 'MD') { ?>
        for (var i = 0; i < document.mainform.decide_md_flg.length; i++) {
            if(document.mainform.decide_md_flg[i].type == "checkbox") {
                document.mainform.decide_md_flg[i].disabled = true;
            }
        }
        <?}?>
        <? if($auth == 'HD') { ?>
        for (var i = 0; i < document.mainform.decide_hd_flg.length; i++) {
            if(document.mainform.decide_hd_flg[i].type == "checkbox") {
                document.mainform.decide_hd_flg[i].disabled = true;
            }
        }
        <?}?>

    <? } ?>


    //役職指定なしの場合(審議者に対する処理)
    <? if($auth != 'MD' && $auth != 'HD') { ?>
    <? if($discuss_use_flg == 'f') { ?>

        //部署指定をdisableにし、先頭を選択
        for (var i = 0; i < document.mainform.discuss_target_class.length; i++) {
            document.mainform.discuss_target_class[i].disabled = true;
        }
        document.mainform.discuss_target_class[0].checked = true;

        //役職指定をdisableにする
        for (var i = 0; i < document.mainform.discuss_mng_post.length; i++) {
            document.mainform.discuss_mng_post[i].disabled = true;
        }

        //安全管理役職指定をdisableにする
        <? if($auth == 'RM') { ?>
        for (var i = 0; i < document.mainform.discuss_rm_flg.length; i++) {
            if(document.mainform.discuss_rm_flg[i].type == "checkbox") {
                document.mainform.discuss_rm_flg[i].disabled = true;
            }
        }
        <?}?>
        <? if($auth == 'RA') { ?>
        for (var i = 0; i < document.mainform.discuss_ra_flg.length; i++) {
            if(document.mainform.discuss_ra_flg[i].type == "checkbox") {
                document.mainform.discuss_ra_flg[i].disabled = true;
            }
        }
        <?}?>
        <? if($auth == 'SD') { ?>
        for (var i = 0; i < document.mainform.discuss_sd_flg.length; i++) {
            if(document.mainform.discuss_sd_flg[i].type == "checkbox") {
                document.mainform.discuss_sd_flg[i].disabled = true;
            }
        }
        <?}?>
    <?}?>
    <?}?>

<? } ?>

//職員指定の初期値を設定
    //決裁者職員
    <?foreach($arr_decide_target as $id => $name)
    {
        if($disp_area_1 != "")
        {
            $disp_area_1  .= ",";
            $decide_ids   .= ",";
            $decide_names .= ",";
        }
        $disp_area_1  .= "<a href=\"javascript:delete_emp(\\'{$id}\\', \\'{$name}\\', \\'1\\');\">{$name}</a>";
        $decide_ids   .= $id;
        $decide_names .= $name;
    }
    ?>
    if(document.getElementById('disp_area_1'))
    {
        document.getElementById('disp_area_1').innerHTML   = '<?=$disp_area_1?>';
        document.getElementById('disp_area_ids_1').value   = '<?=$decide_ids?>';
        document.getElementById('disp_area_names_1').value = '<?=$decide_names?>';
    }
    //審議者職員
    <?foreach($arr_discuss_target as $id => $name)
    {
        if($disp_area_2 != "")
        {
            $disp_area_2   .= ",";
            $discuss_ids   .= ",";
            $discuss_names .= ",";
        }
        $disp_area_2   .= "<a href=\"javascript:delete_emp(\\'{$id}\\', \\'{$name}\\', \\'2\\');\">{$name}</a>";
        $discuss_ids   .= $id;
        $discuss_names .= $name;
    }
    ?>
    if(document.getElementById('disp_area_2'))
    {
        document.getElementById('disp_area_2').innerHTML   = '<?=$disp_area_2?>';
        document.getElementById('disp_area_ids_2').value   = '<?=$discuss_ids?>';
        document.getElementById('disp_area_names_2').value = '<?=$discuss_names?>';
    }

}


function delete_emp(del_emp_id, del_emp_nm, input_div)
{
    if(confirm("「" + del_emp_nm + "」さんを削除します。よろしいですか？"))
    {
        ids   = "disp_area_ids_" + input_div;
        names = "disp_area_names_" + input_div;

        emp_ids = document.getElementById(ids).value;
        emp_nms = document.getElementById(names).value;

        new_emp_ids = "";
        new_emp_nms = "";

        cnt = 0;
        if(emp_ids != "") {

            arr_emp_id = emp_ids.split(",");
            arr_emp_nm = emp_nms.split(",");

            for(i=0; i<arr_emp_id.length; i++)
            {
                if(arr_emp_id[i] == del_emp_id)
                {
                    continue;
                }
                else
                {
                    if(cnt > 0)
                    {
                        new_emp_ids += ",";
                        new_emp_nms += ",";
                    }
                    new_emp_ids += arr_emp_id[i];
                    new_emp_nms += arr_emp_nm[i];
                    cnt++;
                }
            }
        }
        document.getElementById(ids).value = new_emp_ids;
        document.getElementById(names).value = new_emp_nms;
        set_disp_area(new_emp_ids, new_emp_nms, input_div);
    }
}

//子画面から呼ばれる関数
function add_emp_list(input_emp_id, input_emp_nm, input_div)
{
    ids   = "disp_area_ids_" + input_div;
    names = "disp_area_names_" + input_div;

    //既存入力されている職員
    emp_ids = document.getElementById(ids).value;
    emp_nms = document.getElementById(names).value;


    //子画面から入力された職員
    arr_input_emp_id   = input_emp_id.split(", ");
    arr_input_emp_name = input_emp_nm.split(", ");

    tmp_ids   = "";
    tmp_names = "";
    if(emp_ids != "")
    {
        arr_emp_id = emp_ids.split(",");

　      for(i=0; i<arr_input_emp_id.length; i++)
        {
            dpl_flg = false;
            for(j=0; j<arr_emp_id.length; j++)
            {
                // 重複した場合
                if(arr_input_emp_id[i] == arr_emp_id[j])
                {
                    dpl_flg = true;
                }
            }
            if(!dpl_flg)
            {
                if(tmp_ids != "")
                {
                    tmp_ids   += ",";
                    tmp_names += ",";
                }
                tmp_ids   += arr_input_emp_id[i];
                tmp_names += arr_input_emp_name[i];
            }
        }
    }
    else
    {
        for(i=0; i<arr_input_emp_id.length; i++)
        {
            if(tmp_ids != "")
            {
                tmp_ids   += ",";
                tmp_names += ",";
            }
            tmp_ids   += arr_input_emp_id[i];
            tmp_names += arr_input_emp_name[i];
        }
    }

    if(emp_ids != "" && tmp_ids != "")
    {
        emp_ids += ",";
    }
    emp_ids += tmp_ids;
    document.getElementById(ids).value = emp_ids;
    if(emp_nms != ""  && tmp_names != "") {
        emp_nms += ",";
    }
    emp_nms += tmp_names;
    document.getElementById(names).value = emp_nms;

    set_disp_area(emp_ids, emp_nms, input_div);
}


function set_disp_area(emp_id, emp_nm, input_div) {

    disp_area = "disp_area_" + input_div;

    arr_emp_id = emp_id.split(",");
    arr_emp_nm = emp_nm.split(",");
    disp_area_content = "";
    for(i=0; i<arr_emp_id.length; i++)
    {
        if(i > 0)
        {
            disp_area_content += ',';
        }

        disp_area_content += "<a href=\"javascript:delete_emp('" + arr_emp_id[i] + "', '" + arr_emp_nm[i] + "', '" + input_div + "');\">";
        disp_area_content += arr_emp_nm[i];
        disp_area_content += "</a>";
    }
    document.getElementById(disp_area).innerHTML = disp_area_content;
}


function regist_manager(){

    // 職員対象リストの重複チェック
    if (chk_dpl_auth() == false) {
        return;
    }
<?if($auth == 'RM' || $auth == 'RA' || $auth == 'SD' || $auth == 'MD' || $auth == 'HD') {?>
    // 「申請者の所属する部署」ラジオボタン必須チェック
    if(chk_target_class() == false) {
        return;
    }
    // 「役職」チェックボックス必須チェック
    if(chk_mng_post() == false) {
        return;
    }
<?}?>

    document.mainform.mode.value = "regist";
    document.mainform.action="hiyari_charge_register.php?session=<?=$session?>&auth=<?=$auth?>";
    document.mainform.submit();
}

function chk_dpl_auth()
{
    // 職員対象リストの重複チェック
    arr_total = new Array();
    retFlg = true;

    // 決裁者職員
    arr_decide_target_id = new Array();
    if(document.getElementById('disp_area_ids_1'))
    {
        val = document.getElementById('disp_area_ids_1').value;
        arr_decide_target_id = to_array(val);
    }

    // 審議者職員
    arr_discuss_target_id = new Array();
    if(document.getElementById('disp_area_ids_2'))
    {
        val = document.getElementById('disp_area_ids_2').value;
        arr_discuss_target_id = to_array(val);
    }

    if (arr_decide_target_id != null && arr_decide_target_id.length > 0) {
        arr_total = arr_total.concat(arr_decide_target_id);
    }
    if (arr_discuss_target_id != null && arr_discuss_target_id.length > 0) {
        arr_total = arr_total.concat(arr_discuss_target_id);
    }

    for (var i = 0; i < arr_total.length && retFlg; i++)
    {
        for (var j = 0; j < arr_total.length; j++)
        {
            if (i != j && arr_total[i] == arr_total[j])
            {
                retFlg = false;
                break;
            }
        }
    }

    if(retFlg == false)
    {
        alert('決裁者・審議者の中で重複した名称が入力されています。');
    }

    return retFlg;
}



function chk_target_class()
{
    // 「申請者の所属する部署」ラジオボタン必須チェック
    decide_use_flg = "t";
    discuss_use_flg = "t";
    decide_val = "";
    discuss_val = "";

    //決済者の部署指定の有無を取得
    for (var i = 0; i < document.mainform.decide_use_flg.length; i++) {
        if(document.mainform.decide_use_flg[i].type == "checkbox") {
            if(document.mainform.decide_use_flg[i].checked) {
                decide_use_flg = "f";
            }
        }
    }

<?if($auth == 'RM' || $auth == 'RA' || $auth == 'SD') {?>
    //審議者の部署指定の有無を取得
    for (var i = 0; i < document.mainform.discuss_use_flg.length; i++) {
        if(document.mainform.discuss_use_flg[i].type == "checkbox") {
            if(document.mainform.discuss_use_flg[i].checked) {
                discuss_use_flg = "f";
            }
        }
    }
<?}?>

    //決裁者の部署値
    for (var i = 0; i < document.mainform.decide_target_class.length; i++) {
        if(document.mainform.decide_target_class[i].checked) {
            decide_val = document.mainform.decide_target_class[i].value;
        }
    }
<?if($auth == 'RM' || $auth == 'RA' || $auth == 'SD') {?>
    //審議者の部署値
    for (var i = 0; i < document.mainform.discuss_target_class.length; i++) {
        if(document.mainform.discuss_target_class[i].checked) {
            discuss_val = document.mainform.discuss_target_class[i].value;
        }
    }
<?}?>
    if(decide_use_flg == 't' && decide_val == "") {
        alert('決裁者の「部署」を指定してください');
        return false;
    }
<?if($auth == 'RM' || $auth == 'RA' || $auth == 'SD') {?>
    if(discuss_use_flg == 't' && discuss_val == "") {
        alert('審議者の「部署」を指定してください');
        return false;
    }
<?}?>

<?if($auth == 'RM' || $auth == 'RA' || $auth == 'SD') {?>
    if(decide_use_flg == 't' && discuss_use_flg == "t" && decide_val != discuss_val) {
        alert('決裁者と審議者の「部署」の指定を合わせてください');
        return false;
    }
<?}?>
    return true;
}

function chk_mng_post()
{
    // 「役職」チェックボックス必須チェック
    decide_use_flg = "t";
    discuss_use_flg = "t";
    decide_chk_flg = 'f';
    discuss_chk_flg = 'f';

    //決済者の部署指定の有無を取得
    for (var i = 0; i < document.mainform.decide_use_flg.length; i++) {
        if(document.mainform.decide_use_flg[i].type == "checkbox") {
            if(document.mainform.decide_use_flg[i].checked) {
                decide_use_flg = "f";
            }
        }
    }

    //役職(安全管理役職を含む)の指定が行われているかを取得
    for (var i = 0; i < document.mainform.decide_mng_post.length; i++) {
        if(document.mainform.decide_mng_post[i].checked) {
            decide_chk_flg = 't';
            break;
        }
    }
<?if($auth == 'RM'){?>
    for (var i = 0; i < document.mainform.decide_rm_flg.length; i++) {
        if(document.mainform.decide_rm_flg[i].type == "checkbox") {
            if(document.mainform.decide_rm_flg[i].checked) {
                decide_chk_flg = 't';
            }
        }
    }
<?}?>
<?if($auth == 'RA'){?>
    for (var i = 0; i < document.mainform.decide_ra_flg.length; i++) {
        if(document.mainform.decide_ra_flg[i].type == "checkbox") {
            if(document.mainform.decide_ra_flg[i].checked) {
                decide_chk_flg = 't';
            }
        }
    }
<?}?>
<?if($auth == 'SD'){?>
    for (var i = 0; i < document.mainform.decide_sd_flg.length; i++) {
        if(document.mainform.decide_sd_flg[i].type == "checkbox") {
            if(document.mainform.decide_sd_flg[i].checked) {
                decide_chk_flg = 't';
            }
        }
    }
<?}?>
<?if($auth == 'MD'){?>
    for (var i = 0; i < document.mainform.decide_md_flg.length; i++) {
        if(document.mainform.decide_md_flg[i].type == "checkbox") {
            if(document.mainform.decide_md_flg[i].checked) {
                decide_chk_flg = 't';
            }
        }
    }
<?}?>
<?if($auth == 'HD'){?>
    for (var i = 0; i < document.mainform.decide_hd_flg.length; i++) {
        if(document.mainform.decide_hd_flg[i].type == "checkbox") {
            if(document.mainform.decide_hd_flg[i].checked) {
                decide_chk_flg = 't';
            }
        }
    }
<?}?>


<?if($auth == 'RM' || $auth == 'RA' || $auth == 'SD'){?>
    //審議者の部署指定の有無を取得
    for (var i = 0; i < document.mainform.discuss_use_flg.length; i++) {
        if(document.mainform.discuss_use_flg[i].type == "checkbox") {
            if(document.mainform.discuss_use_flg[i].checked) {
                discuss_use_flg = "f";
            }
        }
    }

    //役職(安全管理役職を含む)の指定が行われているかを取得
    for (var i = 0; i < document.mainform.discuss_mng_post.length; i++) {
        if(document.mainform.discuss_mng_post[i].checked) {
            discuss_chk_flg = 't';
            break;
        }
    }
<?if($auth == 'RM'){?>
    for (var i = 0; i < document.mainform.discuss_rm_flg.length; i++) {
        if(document.mainform.discuss_rm_flg[i].type == "checkbox") {
            if(document.mainform.discuss_rm_flg[i].checked) {
                discuss_chk_flg = 't';
            }
        }
    }
<?}?>
<?if($auth == 'RA'){?>
    for (var i = 0; i < document.mainform.discuss_ra_flg.length; i++) {
        if(document.mainform.discuss_ra_flg[i].type == "checkbox") {
            if(document.mainform.discuss_ra_flg[i].checked) {
                discuss_chk_flg = 't';
            }
        }
    }
<?}?>
<?if($auth == 'SD'){?>
    for (var i = 0; i < document.mainform.discuss_sd_flg.length; i++) {
        if(document.mainform.discuss_sd_flg[i].type == "checkbox") {
            if(document.mainform.discuss_sd_flg[i].checked) {
                discuss_chk_flg = 't';
            }
        }
    }
<?}?>
<?}?>



    if(decide_use_flg == 't' && decide_chk_flg == 'f') {
        alert('決裁者の「役職」を指定してください');
        return false;
    }
<?if($auth == 'RM' || $auth == 'RA' || $auth == 'SD'){?>
    if(discuss_use_flg == 't' && discuss_chk_flg == 'f') {
        alert('審議者の「役職」を指定してください');
        return false;
    }
<?}?>
    return true;
}



function to_array(str)
{
    if ( str != null && str.length > 0 ) {
        return str.split(",");
    } else {
        return null;
    }
}


function do_change(obj, div) {

    if (obj.checked) {
        obj.value = 'f';
        flg = true;
    } else {
        obj.value = 't';
        flg = false;
    }

    var target_class = document.mainform.elements[ div + '_target_class' ];
    for (var i = 0; i < target_class.length; i++) {
        target_class[i].disabled = flg;
    }

    var mng_post = document.mainform.elements[ div + '_mng_post' ];
    for (var i = 0; i < mng_post.length; i++) {
        mng_post[i].disabled = flg;
    }

<?if($auth == 'RM') {?>
    var rm_flg = document.mainform.elements[ div + '_rm_flg' ];
    for (var i = 0; i < rm_flg.length; i++) {
        if(rm_flg[i].type == "checkbox") {
            rm_flg[i].disabled = flg;
        }
    }
<?}?>
<?if($auth == 'RA') {?>
    var ra_flg = document.mainform.elements[ div + '_ra_flg' ];
    for (var i = 0; i < ra_flg.length; i++) {
        if(ra_flg[i].type == "checkbox") {
            ra_flg[i].disabled = flg;
        }
    }
<?}?>
<?if($auth == 'SD') {?>
    var sd_flg = document.mainform.elements[ div + '_sd_flg' ];
    for (var i = 0; i < sd_flg.length; i++) {
        if(sd_flg[i].type == "checkbox") {
            sd_flg[i].disabled = flg;
        }
    }
<?}?>
<?if($auth == 'MD') {?>
    var md_flg = document.mainform.elements[ div + '_md_flg' ];
    for (var i = 0; i < md_flg.length; i++) {
        if(md_flg[i].type == "checkbox") {
            md_flg[i].disabled = flg;
        }
    }
<?}?>
<?if($auth == 'HD') {?>
    var hd_flg = document.mainform.elements[ div + '_hd_flg' ];
    for (var i = 0; i < hd_flg.length; i++) {
        if(hd_flg[i].type == "checkbox") {
            hd_flg[i].disabled = flg;
        }
    }
<?}?>

}

function call_emp_search(input_div)
{
    var childwin = null;

    dx = screen.availWidth - 10;
    dy = screen.top;
    base = 0;
    wx = 720;
    wy = 600;
    var url = 'hiyari_emp_list.php';
    url += '?session=<?=$session?>&input_div=' + input_div;
    childwin = window.open(url, 'emplist', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

    childwin.focus();
}

function emp_clear(div)
{
    if(document.getElementById('disp_area_' + div))
    {
        document.getElementById('disp_area_' + div).innerHTML   = '';
        document.getElementById('disp_area_ids_' + div).value   = '';
        document.getElementById('disp_area_names_' + div).value = '';
    }

}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
<!--
.list {border-collapse:collapse;}
.list td {border:#35B341 solid 1px;}

table.block {border-collapse:collapse;}
table.block td {border:#35B341 solid 1px;padding:1px;}
table.block td td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#35B341 solid 0px;}
table.block_in td td {border-width:1;}

.non_in_list {border-collapse:collapse;}
.non_in_list td {border:0px;}
// -->
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<form name="mainform" action="hiyari_charge_register.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<!-- ヘッダー -->
<?
show_hiyari_header($session,$fname,true);
?>
<!-- ヘッダー -->

<img src="img/spacer.gif" width="1" height="5" alt=""><br>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
<tr>
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td>

<!-- 権限マスタ表示 -->
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="200" valign="top" bgcolor="#D5F4D8">
<table width="200" border="0" cellspacing="10" cellpadding="0">
<tr>
<td>
<?foreach($arr_auth_name as $key => $val) { ?>

<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tr>
<td width="5"><img src="img/i_corner1.gif" width="5" height="5"></td>
<td background="img/i_up.gif"><img src="img/spacer.gif" width="1" height="5" alt=""></td>
<td width="5"><img src="img/i_corner2.gif" width="5" height="5"></td>
</tr>
<tr>
<td background="img/i_left.gif"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<td bgcolor="#E5F8E1" align="center">
<nobr>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
<a href="hiyari_charge_register.php?session=<?=$session?>&auth=<?=$key?>" <? if($auth==$key) { ?>style="background-color:#35B341;color:#FFFFFF;padding:2px;"<? } ?>><?=$val?></a>
</font>
</nobr>
<br>
</td>
<td background="img/i_right.gif"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/i_corner3.gif" width="5" height="5"></td>
<td background="img/i_down.gif"><img src="img/spacer.gif" width="1" height="5" alt=""></td>
<td><img src="img/i_corner4.gif" width="5" height="5"></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<?}?>
</td>
</tr>
</table>
</td>


<td width="10" bgcolor="#F5FFE5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<? /*決裁者の設定*/ ?>
        <td valign="middle" bgcolor="#F5FFE5">

            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr bgcolor="#FFDDFD">
                    <td height="100%" align="center"><font size="4" face="ＭＳ Ｐゴシック, Osaka"><b><?=($arr_auth_name[$auth])?></b></font></td>
                </tr>
            </table>

            <table cellspacing="0" cellpadding="0" border="0">
                <tr><td><img src="img/spacer.gif" width="1" height="5" alt=""></td></tr>
            </table>

            <table width="20%" border="0" cellspacing="0" cellpadding="0"  class="list">
                <tr height="22">
                    <td bgcolor="#DFFFDC" align="center" nowrap>
                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">■決裁者の設定</font>
                    </td>
                </tr>
            </table>

            <table cellspacing="0" cellpadding="0" border="0">
                <tr><td><img src="img/spacer.gif" width="1" height="5" alt=""></td></tr>
            </table>

            <table width="100%" border='0' cellspacing='0' cellpadding='0' class="block">
            <? if($auth == 'RM' || $auth == 'RA' || $auth == 'SD' || $auth == 'MD' || $auth == 'HD') { ?>
                <tr>
                    <td rowspan="2" width="20%" height="100" cellspacing="0" cellpadding="0" border="0" bgcolor="#DFFFDC" align="center">
                        <table border='0' cellspacing='0' cellpadding='0' class="block_in">
                            <tr>
                                <td align="center" nowrap>
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">部署・役職</font><BR>
                                    <input type="hidden" name="decide_use_flg" value="t">
                                    <input type="checkbox" name="decide_use_flg" id="decide_use_flg" value="f" <? if ($decide_use_flg == "f") {echo(" checked");} ?> onclick="do_change(this,'decide');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">指定しない</font>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="35%">
                        <table width="100%" height="100%" cellspacing="1" cellpadding="1" border="0" bgcolor="#DFFFDC">
                            <tr>
                                <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">部署</font></td>
                            </tr>
                        </table>
                    </td>
                    <td width="45%">
                        <table width="100%" height="100%" cellspacing="1" cellpadding="1" border="0" bgcolor="#DFFFDC">
                            <tr>
                                <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">役職</font></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr bgcolor="#FFFFFF">
                    <td>
                        <table border='0' cellspacing='0' cellpadding='0'>
                            <tr>
                                <td nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請者の所属する</font></td>
                                <td>
                                    <table>
                                        <? for($i=0; $i<$arr_class_name["class_cnt"]; $i++) {?>
                                        <tr><td nowrap><input type="radio" name="decide_target_class" value="<?echo($i+1);?>" <? if ($decide_target_class == $i+1) {echo(" checked");} ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$arr_class_name[$i]?></td></tr>
                                        <? } ?>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table border='0' cellspacing='0' cellpadding='0'>
                            <tr>
                                <td nowrap>
                                    <?for($i=0; $i<count($arr_manager_post); $i++) {
                                        $chk_flg = false;
                                        $st_id = $arr_manager_post[$i]["id"];
                                        $st_nm = $arr_manager_post[$i]["name"];

                                        for($j=0; $j<count($decide_arr_post); $j++) {
                                            $emp_st = $decide_arr_post[$j];
                                            if($st_id == $emp_st) {
                                                $chk_flg = true;
                                                break;
                                            }
                                        }
                                    ?>
                                    <input type="checkbox" name="decide_mng_post[]" id="decide_mng_post" value="<?echo($st_id);?>" <?if($chk_flg){echo(" checked");} ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?echo($st_nm);?></font><BR>
                                    <?}?>

                                    <input type="hidden" name="decide_rm_flg" value="f">
                                    <?if($auth == 'RM') {?>
                                    <input type="checkbox" name="decide_rm_flg" id="decide_rm_flg" value="t" <? if ($decide_rm_flg == "t") {echo(" checked");} ?> onclick="javascript:this.value = (this.checked)? 't': 'f';">
                                    <?}?>

                                    <input type="hidden" name="decide_ra_flg" value="f">
                                    <?if($auth == 'RA') {?>
                                    <input type="checkbox" name="decide_ra_flg" id="decide_ra_flg" value="t" <? if ($decide_ra_flg == "t") {echo(" checked");} ?> onclick="javascript:this.value = (this.checked)? 't': 'f';">
                                    <?}?>

                                    <input type="hidden" name="decide_sd_flg" value="f">
                                    <?if($auth == 'SD') {?>
                                    <input type="checkbox" name="decide_sd_flg" id="decide_sd_flg" value="t" <? if ($decide_sd_flg == "t") {echo(" checked");} ?> onclick="javascript:this.value = (this.checked)? 't': 'f';">
                                    <?}?>

                                    <input type="hidden" name="decide_md_flg" value="f">
                                    <?if($auth == 'MD') {?>
                                    <input type="checkbox" name="decide_md_flg" id="decide_md_flg" value="t" <? if ($decide_md_flg == "t") {echo(" checked");} ?> onclick="javascript:this.value = (this.checked)? 't': 'f';">
                                    <?}?>

                                    <input type="hidden" name="decide_hd_flg" value="f">
                                    <?if($auth == 'HD') {?>
                                    <input type="checkbox" name="decide_hd_flg" id="decide_hd_flg" value="t" <? if ($decide_hd_flg == "t") {echo(" checked");} ?> onclick="javascript:this.value = (this.checked)? 't': 'f';">
                                    <?}?>

                                    <?if($auth == 'RM' || $auth == 'RA' || $auth == 'SD' || $auth == 'MD' || $auth == 'HD') {?>
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$arr_auth_name[$auth]?></font>&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#FF0000">(<?=$arr_auth_name[$auth]?>登録画面で担当者を指定)</font><BR>
                                    <?}?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            <? } ?>
                <tr height="100">
                <td width="20%" bgcolor="#DFFFDC" align="center">
                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員</font><br>
                <input type="button" value="職員名簿" onclick="call_emp_search('1');"><br>
                <input type="button" value="クリア" onclick="emp_clear('1');">
                </td>
                <td colspan="2" bgcolor="#FFFFFF">
                <table width="100%" height="100%" border="0" cellspacing="3" cellpadding="0" class="non_in_list">
                <tr>
                <td style="border:#35B341 solid 1px;">
                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><span id="disp_area_1"></span></font>
                </td>
                </tr>
                </table>
                </td>
                </tr>
                <input type="hidden" name="disp_area_ids_1" value="" id="disp_area_ids_1">
                <input type="hidden" name="disp_area_names_1" value="" id="disp_area_names_1">
            </table>

<? /*審議者の設定*/ ?>
<? if($auth == 'SM' || $auth == 'RM' ||  $auth == 'RA' || $auth == 'SD') { ?>

            <table cellspacing="0" cellpadding="0" border="0">
                <tr><td><img src="img/spacer.gif" width="1" height="5" alt=""></td></tr>
            </table>

            <table width="20%" border="0" cellspacing="0" cellpadding="0"  class="list">
                <tr height="22">
                    <td bgcolor="#DFFFDC" align="center" nowrap>
                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">■審議者の設定</font>
                    </td>
                </tr>
            </table>

            <table cellspacing="0" cellpadding="0" border="0">
                <tr><td><img src="img/spacer.gif" width="1" height="5" alt=""></td></tr>
            </table>

            <table width="100%" border='0' cellspacing='0' cellpadding='0' class="block">
            <? if($auth == 'RM' || $auth == 'RA' || $auth == 'SD') { ?>
                <tr>
                    <td rowspan="2" height="100" cellspacing="0" cellpadding="0" border="0" bgcolor="#DFFFDC" align="center">
                        <table border='0' cellspacing='0' cellpadding='0' class="block_in">
                            <tr>
                                <td align="center" nowrap>
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">部署・役職</font><BR>
                                    <input type="hidden" name="discuss_use_flg" value="t">
                                    <input type="checkbox" name="discuss_use_flg" id="discuss_use_flg" value="f" <? if ($discuss_use_flg == "f") {echo(" checked");} ?> onclick="do_change(this,'discuss');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">指定しない</font>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="35%">
                        <table width="100%" height="100%" cellspacing="1" cellpadding="1" border="0" bgcolor="#DFFFDC">
                            <tr>
                                <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">部署</font></td>
                            </tr>
                        </table>
                    </td>
                    <td width="45%">
                        <table width="100%" height="100%" cellspacing="1" cellpadding="1" border="0" bgcolor="#DFFFDC">
                            <tr>
                                <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">役職</font></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr bgcolor="#FFFFFF">
                    <td>
                        <table border='0' cellspacing='0' cellpadding='0'>
                            <tr>
                                <td nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請者の所属する</font></td>
                                <td>
                                    <table>
                                    <? for($i=0; $i<$arr_class_name["class_cnt"]; $i++) {?>
                                    <tr><td nowrap><input type="radio" name="discuss_target_class" value="<?echo($i+1);?>" <? if ($discuss_target_class == $i+1) {echo(" checked");} ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$arr_class_name[$i]?></td></tr>
                                    <? } ?>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table border='0' cellspacing='0' cellpadding='0'>
                            <tr>
                                <td nowrap>
                                    <?for($i=0; $i<count($arr_manager_post); $i++) {
                                        $chk_flg = false;
                                        $st_id = $arr_manager_post[$i]["id"];
                                        $st_nm = $arr_manager_post[$i]["name"];

                                        for($j=0; $j<count($discuss_arr_post); $j++) {
                                            $emp_st = $discuss_arr_post[$j];
                                            if($st_id == $emp_st) {
                                                $chk_flg = true;
                                                break;
                                            }
                                        }
                                    ?>
                                    <input type="checkbox" name="discuss_mng_post[]" id="discuss_mng_post" value="<?echo($st_id);?>" <?if($chk_flg){?>checked<?}?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?echo($st_nm);?></font><BR>
                                    <?}?>

                                    <input type="hidden" name="discuss_rm_flg" value="f">
                                    <?if($auth == 'RM') {?>
                                    <input type="checkbox" name="discuss_rm_flg" id="discuss_rm_flg" value="t" <? if ($discuss_rm_flg == "t") {echo(" checked");} ?> onclick="javascript:this.value = (this.checked)? 't': 'f';">
                                    <?}?>

                                    <input type="hidden" name="discuss_ra_flg" value="f">
                                    <?if($auth == 'RA') {?>
                                    <input type="checkbox" name="discuss_ra_flg" id="discuss_ra_flg" value="t" <? if ($discuss_ra_flg == "t") {echo(" checked");} ?> onclick="javascript:this.value = (this.checked)? 't': 'f';">
                                    <?}?>

                                    <input type="hidden" name="discuss_sd_flg" value="f">
                                    <?if($auth == 'SD') {?>
                                    <input type="checkbox" name="discuss_sd_flg" id="discuss_sd_flg" value="t" <? if ($discuss_sd_flg == "t") {echo(" checked");} ?> onclick="javascript:this.value = (this.checked)? 't': 'f';">
                                    <?}?>

                                    <?if($auth == 'RM' || $auth == 'RA' || $auth == 'SD') {?>
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$arr_auth_name[$auth]?></font>&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#FF0000">(<?=$arr_auth_name[$auth]?>登録画面で担当者を指定)</font><BR>
                                    <?}?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            <? } ?>
                <tr height="100">
                <td width="20%" bgcolor="#DFFFDC" align="center">
                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員</font><br>
                <input type="button" value="職員名簿" onclick="call_emp_search('2');"><br>
                <input type="button" value="クリア" onclick="emp_clear('2');">
                </td>
                <td colspan="2" bgcolor="#FFFFFF">
                <table width="100%" height="100%" border="0" cellspacing="3" cellpadding="0" class="non_in_list">
                <tr>
                <td style="border:#35B341 solid 1px;">
                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><span id="disp_area_2"></span></font>
                </td>
                </tr>
                </table>
                </td>
                </tr>
                <input type="hidden" name="disp_area_ids_2" value="" id="disp_area_ids_2">
                <input type="hidden" name="disp_area_names_2" value="" id="disp_area_names_2">
            </table>
<? } ?>

<? /*代行者の設定*/ ?>
<? if($auth == 'SM' || $auth == 'RM' || $auth == 'RA' || $auth == 'SD') { ?>
            <table cellspacing="0" cellpadding="0" border="0">
                <tr><td><img src="img/spacer.gif" width="1" height="5" alt=""></td></tr>
            </table>

            <table width="20%" border="0" cellspacing="0" cellpadding="0"  class="list">
                <tr height="22">
                    <td bgcolor="#DFFFDC" align="center" nowrap>
                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">■代行者の設定</font>
                    </td>
                </tr>
            </table>

            <table cellspacing="0" cellpadding="0" border="0">
                <tr><td><img src="img/spacer.gif" width="1" height="5" alt=""></td></tr>
            </table>

            <table width="100%" border='0' cellspacing='0' cellpadding='0'>
            <tr>
                <td nowrap>
                <input type="radio" name="agent_use_flg" value="t" <?if($agent_use_flg == "t"){?> checked<?}?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">代行者設定を可能にする</font>&nbsp;
                <input type="radio" name="agent_use_flg" value="f" <?if($agent_use_flg == "f"){?> checked<?}?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">代行者設定を不可にする</font>
                </td>
            </tr>
            </table>
<?}?>
            <table cellspacing="0" cellpadding="0" border="0">
                <tr><td><img src="img/spacer.gif" width="1" height="5" alt=""></td></tr>
            </table>

            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <td align="right">
                        <input type="button" value="登録" onclick="regist_manager();">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>


</td>
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>

<table cellspacing="0" cellpadding="0" border="0">
<tr><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
</table>

<!-- フッター START -->
<?
show_hiyari_footer($session,$fname,true);
?>
<!-- フッター END -->



<input type="hidden" name="mode" value="">
<input type="hidden" name="auth" value="<?=$auth?>">

</td>
</tr>
</table>
</form>
</body>
<? pg_close($con); ?>
</html>
<?


//====================================
// 権限名取得
//====================================
function sel_auth_name($con, $fname) {

    $arr_auth_name = array();

    $sql = "select auth, auth_name from inci_auth_mst where use_flg order by auth_rank asc";
    $cond = "";
    $sel = select_from_table($con, $sql, $cond, $fname);

    if($sel == 0){
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }

    while($row = pg_fetch_array($sel)) {

        $id = $row["auth"];
        $name = h($row["auth_name"]);
        $arr_auth_name[$id] = $name;
    }
    return $arr_auth_name;
}

//====================================
// 担当者テーブル情報取得
//====================================
function search_inci_auth_emp($con, $fname, $auth, $auth_part) {

    $arr_target = array();

    $sql = "select " .
                "a.emp_id, " .
                "b.emp_name " .
            "from " .
                "inci_auth_emp a, " .
                "(select emp_id, emp_lt_nm || ' ' || emp_ft_nm as emp_name from empmst) b ";
    $cond = "where a.emp_id = b.emp_id and auth = '$auth' and a.auth_part = '$auth_part' order by emp_id";
    $sel_target = select_from_table($con, $sql, $cond, $fname);
    if($sel_target == 0){
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }

    while ($row_target = pg_fetch_array($sel_target)) {

        $tmp_target_id = $row_target["emp_id"];
        $tmp_target_nm = $row_target["emp_name"];
        $arr_target[$tmp_target_id] = $tmp_target_nm;
    }
    return $arr_target;

}

//====================================
// 担当者部署指定テーブル情報取得
//====================================
function search_inci_auth_emp_post($con, $fname, $auth, $auth_part) {

    $sql = "select ".
                "a.*, ".
                "b.emp_st ".
            "from ".
                "inci_auth_emp_post a ".
                "left outer join inci_auth_emp_st b on ".
                "a.auth = b.auth and  a.auth_part = b.auth_part ";
    $cond = "where a.auth = '$auth' and a.auth_part = '$auth_part'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if($sel == 0){
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }
    return $sel;

}

//====================================
// 代行者機能利用設定テーブル情報取得
//====================================
function search_inci_agent_use($con, $fname, $auth) {

    $sql = "select * from inci_agent_use";
    $cond = "where auth = '$auth'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if($sel == 0){
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }

    $agent_use_flg = pg_fetch_result($sel,0,"agent_use_flg");
    return $agent_use_flg;
}

//====================================
// 登録処理
//====================================
function regist($con,  $fname, $auth, $inputs) {

    $decide_target_id_list = $inputs['disp_area_ids_1'];
    $discuss_target_id_list = $inputs['disp_area_ids_2'];
    $arr_decide_emp_id = array();
    $arr_discuss_emp_id = array();

    $decide_use_flg = $inputs['decide_use_flg'];
    $discuss_use_flg = $inputs['discuss_use_flg'];

    $decide_rm_flg = $inputs['decide_rm_flg'];
    $discuss_rm_flg = $inputs['discuss_rm_flg'];

    $decide_ra_flg = $inputs['decide_ra_flg'];
    $discuss_ra_flg = $inputs['discuss_ra_flg'];

    $decide_sd_flg = $inputs['decide_sd_flg'];
    $discuss_sd_flg = $inputs['discuss_sd_flg'];

    $decide_md_flg = $inputs['decide_md_flg'];

    $decide_hd_flg = $inputs['decide_hd_flg'];

    $decide_target_class = $inputs['decide_target_class'];
    $discuss_target_class = $inputs['discuss_target_class'];

    $decide_mng_post = $inputs['decide_mng_post'];
    $discuss_mng_post = $inputs['discuss_mng_post'];

    // トランザクションの開始
    pg_query($con, "begin transaction");

    if ($decide_target_id_list != "") {
        $arr_decide_emp_id = array_merge($arr_decide_emp_id, split(",", $decide_target_id_list));
    }

    if ($discuss_target_id_list != "") {
        $arr_discuss_emp_id = array_merge($arr_discuss_emp_id, split(",", $discuss_target_id_list));
    }


    regist_inci_auth_emp($con, $fname, $arr_decide_emp_id, $auth, '決裁者');
    if($auth == 'SM' || $auth == 'RM' || $auth == 'RA' || $auth == 'SD') {
        regist_inci_auth_emp($con, $fname, $arr_discuss_emp_id, $auth, '審議者');
    }

    if($auth == 'RM' || $auth == 'RA' || $auth == 'SD' || $auth == 'MD' || $auth == 'HD') {
        regist_inci_auth_emp_post($con, $fname, $auth, '決裁者', $decide_use_flg, $decide_target_class, $decide_mng_post, $decide_rm_flg, $decide_ra_flg, $decide_sd_flg, $decide_md_flg, $decide_hd_flg);
        if($auth != 'MD' && $auth != 'HD') {
            regist_inci_auth_emp_post($con, $fname, $auth, '審議者', $discuss_use_flg, $discuss_target_class, $discuss_mng_post, $discuss_rm_flg, $discuss_ra_flg, $discuss_sd_flg, "f");
        }
    }

    if($auth == 'SM' || $auth == 'RM' || $auth == 'RA' || $auth == 'SD') {
        $agent_use_flg = $_POST['agent_use_flg'];
        update_inci_agent_use($con, $fname, $auth, $agent_use_flg);
    }

    //報告ファイル権限設定／統計分析権限設定との同期
    $agreement_target_class = "";
    if($decide_use_flg == "t")
    {
        $agreement_target_class = $decide_target_class;
    }
    elseif($discuss_use_flg == "t")
    {
        $agreement_target_class = $discuss_target_class;
    }
    if($agreement_target_class != "")
    {
        $sql = "select * from inci_auth_case where auth = '$auth';";
        $sel = select_from_table($con, $sql, "", $fname);
        if($sel == 0){
            pg_close($con);
            echo("<script type='text/javascript' src='./js/showpage.js'></script>");
            echo("<script language='javascript'>showErrorPage(window);</script>");
            exit;
        }
        $report_db_use_flg = pg_fetch_result($sel,0,"report_db_use_flg");
        $report_db_read_div = pg_fetch_result($sel,0,"report_db_read_div");
        $report_db_read_auth_flg = pg_fetch_result($sel,0,"report_db_read_auth_flg");
        $report_db_update_flg = pg_fetch_result($sel,0,"report_db_update_flg");
        $report_db_update_div = pg_fetch_result($sel,0,"report_db_update_div");
        $stats_use_flg = pg_fetch_result($sel,0,"stats_use_flg");
        $stats_read_div = pg_fetch_result($sel,0,"stats_read_div");
        $stats_read_auth_flg = pg_fetch_result($sel,0,"stats_read_auth_flg");

        $set = array();
        $setvalue = array();

        $inci_auth_case_report_db_update = false;
//      if($report_db_use_flg == "t" && $report_db_read_div != 0 && $report_db_read_div != $agreement_target_class)
        if($report_db_read_auth_flg == "t" && $report_db_read_div != $agreement_target_class)
        {
            $inci_auth_case_report_db_update = true;
            array_push($set,"report_db_read_div");
            array_push($setvalue,$agreement_target_class);
        }
        $inci_auth_case_report_db2_update = false;
        if($report_db_update_flg == "t" && $report_db_update_div != 0 && $report_db_update_div != $agreement_target_class)
        {
            $inci_auth_case_report_db2_update = true;
            array_push($set,"report_db_update_div");
            array_push($setvalue,$agreement_target_class);
        }
        $inci_auth_case_stats_update = false;
//      if($stats_use_flg == "t" && $stats_read_div != 0 && $stats_read_div != $agreement_target_class)
        if($stats_read_auth_flg == "t" && $stats_read_div != $agreement_target_class)
        {
            $inci_auth_case_stats_update = true;
            array_push($set,"stats_read_div");
            array_push($setvalue,$agreement_target_class);
        }

        if($inci_auth_case_report_db_update || $inci_auth_case_stats_update || $inci_auth_case_report_db2_update)
        {
            $sql = "update inci_auth_case set";
            $cond = "where auth = '$auth'";
            $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
            if ($upd == 0) {
                pg_exec($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
        }
    }













    // トランザクションをコミット
    pg_query($con, "commit");
}
//====================================
// 担当者テーブル登録処理
//====================================
function regist_inci_auth_emp($con, $fname, $arr_emp_id, $auth, $auth_part_div) {

    $sql = "delete from inci_auth_emp";
    $cond = "where auth = '$auth' and auth_part = '$auth_part_div'";
    $del = delete_from_table($con, $sql, $cond, $fname);
    if ($del == 0) {
        pg_exec($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    foreach($arr_emp_id as $key) {

        $sql = "insert into inci_auth_emp (auth, auth_part, emp_id) values (";
        $content = array($auth, $auth_part_div, $key);
        $ins = insert_into_table($con, $sql, $content, $fname);
        if ($ins == 0) {
            pg_exec($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }
}

//====================================
// 担当者部署指定テーブル登録処理
//====================================
function regist_inci_auth_emp_post($con, $fname, $auth, $auth_part_div, $use_flg, $target_class, $arr_emp_st, $rm_flg, $ra_flg, $sd_flg, $md_flg, $hd_flg) {

    // 担当者部署指定テーブル削除
    $sql = "delete from inci_auth_emp_post";
    $cond = "where auth = '$auth' and auth_part = '$auth_part_div'";
    $del = delete_from_table($con, $sql, $cond, $fname);
    if ($del == 0) {
        pg_exec($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    // 担当者役職指定テーブル削除
    $sql = "delete from inci_auth_emp_st";
    $cond = "where auth = '$auth' and auth_part = '$auth_part_div'";
    $del = delete_from_table($con, $sql, $cond, $fname);
    if ($del == 0) {
        pg_exec($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    // 担当者部署指定テーブル登録
    // 全役職指定フラグ(all_st_flg)は、FALSE固定
    $sql = "insert into inci_auth_emp_post (auth, auth_part, use_flg, target_class, all_st_flg, rm_flg, ra_flg, md_flg, sd_flg, hd_flg) values (";
    $content = array($auth, $auth_part_div, $use_flg, $target_class, 'f', $rm_flg, $ra_flg, $md_flg, $sd_flg, $hd_flg);
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) {
        pg_exec($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    // 担当者部署指定テーブル登録
    if($use_flg == 't') {
        foreach($arr_emp_st as $val) {
            $sql = "insert into inci_auth_emp_st (auth, auth_part, emp_st) values (";
            $content = array($auth, $auth_part_div, $val);
            $ins = insert_into_table($con, $sql, $content, $fname);
            if ($ins == 0) {
                pg_exec($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
        }
    }
}

//=================================
// 代行者機能利用設定テーブル更新
//=================================
function update_inci_agent_use($con, $fname, $auth, $agent_use_flg) {
    $sql = "update inci_agent_use set";
    $set = array("agent_use_flg");
    $setvalue = array(pg_escape_string($agent_use_flg));
    $cond = "where auth = '$auth'";
    $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);

    if ($upd == 0) {
        pg_exec($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}
//=================================
// 部門オプションを出力
//=================================
function show_class_options($arr_class, $class) {
    echo("<option value=\"-\">----------");
    foreach ($arr_class as $tmp_class_id => $arr) {
        $tmp_class_name = $arr["name"];
        echo("<option value=\"$tmp_class_id\"");
        if ($class === $tmp_class_id) {
            echo(" selected");
        }
        echo(">$tmp_class_name\n");
    }
}



?>
