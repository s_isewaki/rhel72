<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 設備予約 | 週詳細（印刷）</title>
<?
$fname = $PHP_SELF;
//ini_set("display_errors","1");

require_once("about_comedix.php");
require("show_facility.ini");
require("facility_common.ini");
require("facility_week_chart_common.ini");
require("holiday.php");
require("show_calendar_memo.ini");

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 11, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 施設・設備登録権限を取得
$fcl_admin_auth = check_authority($session, 41, $fname);

// 表示範囲が渡されていない場合は「全て」に設定
if ($range == "") {
    $range = "all";
}

// タイムスタンプが渡されていない場合は当日日付のタイムスタンプを取得
if ($date == "") {
    $date = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
}

// 日付の設定
$day = date("d", $date);
$month = date("m", $date);
$year = date("Y", $date);

// データベースに接続
$con = connect2db($fname);

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// 参照／予約／管理可能なカテゴリ・設備一覧を配列で取得
$categories = get_referable_categories($con, $emp_id, $fname);

// カテゴリ未選択の場合、最初のカテゴリが選択されたものとする
if ($cate_id == "") {
    if ($range != "all") {
        list($cate_id, $fcl_id) = explode("-", $range);
    }
    if ($cate_id == "") {
        $cate_id = (count($categories) > 0) ? strval(key($categories)) : "";
    }
}

// 設備未選択の場合、最初の設備が選択されたものとする
if ($cate_id != "" && $fcl_id == "") {
    $facilities = $categories[$cate_id]["facilities"];
    $fcl_id = (count($facilities) > 0) ? strval(key($facilities)) : "";
}

// 選択されたカテゴリの患者情報連携フラグを取得
if ($cate_id != "") {
    $pt_flg = ($categories[$cate_id]["pt_flg"] == "t");
} else {
    $pt_flg = false;
}

// 選択された設備の管理者権限を取得
if ($fcl_id != "") {
    $fcl_admin_flg = $categories[$cate_id]["facilities"][$fcl_id]["admin_flg"];
} else {
    $fcl_admin_flg = false;
}

// 選択されたカテゴリの週表示に予定内容を表示するを取得
if ($cate_id != "") {
    $show_content = $categories[$cate_id]["show_content"];
} else {
    $show_content = "f";
}

// 他画面の表示範囲に選択内容を反映
if ($range = "all") {
    if ($cate_id != "") {
        $range = $cate_id;
    }
    if ($fcl_id != "") {
        $range .= "-{$fcl_id}";
    }
}

// オプション情報を取得
$default_info = get_default_info($con, $session, $fname);
$default_url = $default_info["url"];
$calendar_start = $default_info["calendar_start"];

// 表示対象週のスタート日のタイムスタンプを取得
$start_date = get_start_date_timestamp($con, $date, $emp_id, $fname);

// 表示対象週の7日分の日付を配列で取得
$dates = get_dates_in_week($start_date);

// 時間枠の設定
$base_min = 30;
if ($cate_id != "") {
    $base_min = $categories[$cate_id]["base_min"];
}

// 時刻指定なしの予約情報を配列で取得
$timeless_reservations = get_timeless_reservations($con, $cate_id, $fcl_id, $dates, $base_min, $fcl_admin_flg, $emp_id, $pt_flg, $fname);

// 予約情報を配列で取得
$reservations = get_reservations_for_detail_print($con, $cate_id, $fcl_id, $dates, $base_min, $fcl_admin_flg, $emp_id, $pt_flg, $fname);

// スタート曜日の設定
$start_wd = ($calendar_start == "1") ? 0 : 1;  // 0：日曜、1：月曜

// 処理週のスタート日のタイムスタンプを求める
$start_day = get_start_day($date, $start_wd);

// 前月・前週・翌週・翌月のタイプスタンプを変数に格納
$last_month = get_last_month($date);
$last_week = strtotime("-7 days", $date);
$next_week = strtotime("+7 days", $date);
$next_month = get_next_month($date, $start_wd);
?>
<script type="text/javascript" src="js/fontsize.js"></script>
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
@media print {
    .noprint {visibility:hidden;}
}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="self.print();self.close();">
<center>
<form name="view" action="facility_week_chart.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr bgcolor="#f6f9ff">
<td height="30"><span class="noprint"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示範囲&nbsp;&nbsp;<? show_category_string($categories, $cate_id); ?></font></span><img src="img/spacer.gif" width="160" height="1" alt=""><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><? show_date_string($date, $start_day, $session, $range); ?></font></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="range" value="<? echo($range); ?>">
<input type="hidden" name="date" value="<? echo($date); ?>">
</form>
<? show_facilities($categories, $cate_id, $fcl_id, $range, $date, $session); ?>
<?
if ($fcl_id != "") {
    echo("<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"2\">\n");

    $start_date = date("Ymd", $start_day);
    $end_date = date("Ymd", strtotime("+6 days", $start_day));
    // カレンダーのメモを取得
    $arr_calendar_memo = get_calendar_memo($con, $fname, $start_date, $end_date);
    // 見出しデータを取得
    list($arr_title, $arr_tag) = get_template_title($con, $session, $fname, $fcl_id, $end_date);
    // 見出し行を表示
    show_title($arr_title);

    show_reservations_week($con, $dates, $arr_calendar_memo, $timeless_reservations, $reservations, $arr_tag);
    echo("</table>\n");
} else {
    echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\n");
    echo("<tr height=\"22\">\n");
    echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">表示可能な設備がありません。</font></td>\n");
    echo("</tr>\n");
    echo("</table>\n");
}
?>
</center>
</body>
<? pg_close($con); ?>
</html>
<?
// 見出し行を表示
function show_title($arr_title) {

    echo("<tr>\n");
    echo("<td><br></td>\n");
    echo("<td width=\"70\" style=\"border-right-style:none;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">開始時刻</font></td>\n");
    echo("<td width=\"80\" style=\"border-left-style:none;border-right-style:none;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">タイトル</font></td>\n");
    for ($i=0,$j=count($arr_title); $i<$j; $i++) {
        $style_right = ($i < $j-1) ? "border-right-style:none;" : "";
        echo("<td style=\"border-left-style:none;$style_right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$arr_title[$i]</font></td>\n");
    }
    echo("</tr>\n");
}

// 指定日付以前のうちで最新のテンプレート履歴情報を取得
// facility_common.iniのget_fclhist()を参考にしている
function get_fclhist_with_date($con, $fname, $facility_id, $date) {

    $sql = "select * from fclhist";
    $cond = "where facility_id = $facility_id and fclhist_up_date <= '$date'";
    $cond .= " order by fclhist_id desc limit 1";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    $fclhist_info = array();

    if (pg_num_rows($sel) == 0) {
        $fclhist_info["fclhist_id"] = "0";
        $fclhist_info["fclhist_content_type"] = "1";
    } else {
        $fclhist_info = pg_fetch_array($sel);
    }

    return $fclhist_info;
}

// テンプレートの見出しとタグを取得する
function get_template_title($con, $session, $fname, $facility_id, $end_date) {

    $arr_title = array();
    $arr_tag = array();
    // 表示範囲終了日までの最新情報を取得
    $fclhist_info = get_fclhist_with_date($con, $fname, $facility_id, $end_date);
    if ($fclhist_info["fclhist_content_type"] == "2") {

        // ワークフロー情報取得
        $tmpl_content = $fclhist_info["fclhist_content"];
        $tmpl_content_xml = preg_replace("/^.*<\? \/\/ XML/s", "<? // XML", $tmpl_content);

        // タイトルを取得
        list($arr_title, $arr_tag) = get_title_from_php($tmpl_content_xml);
    }
    return array($arr_title, $arr_tag);
}

// XML作成用PHPからタイトルの配列を取得、タグも同時取得
function get_title_from_php($content) {

    $arr_title = array();
    $arr_tag = array();

    if (strlen($content) == 0 ){
        return array($arr_title, $arr_tag);
    }

    // 行毎の配列
    $arr_line = explode("\n", $content);

    // "区切りで探す
    $idx = 0;

    for ($i=0,$max_cnt=count($arr_line); $i<$max_cnt; $i++) {
        $tag = "";
        $title = "";
        $line = $arr_line[$i];
        if (strpos($line, "create_element") > 0 && strpos($line, "apply") > 0) {
            continue;
        }
        if (strpos($line, "create_element") > 0) {
            $tmp_data = explode("\"", $line);
            $tag = $tmp_data[1];
            for ($j=$i+1; $j<$max_cnt; $j++) {
                $i++;
                $line = $arr_line[$j];
                if (strpos($line, "set_attribute") > 0 && strpos($line, "title") > 0) {
                    $tmp_data = explode("\"", $line);
                    $title = $tmp_data[3];
                    break;
                }
            }
            if ($title == "") {
                $title = $tag;
            }
            $arr_title[$idx] = $title;
            $arr_tag[$idx] = $tag;
            $idx++;
        }
    }

        return array($arr_title, $arr_tag);
}

// 予約を表示
function show_reservations_week($con, $dates, $arr_calendar_memo, $timeless_reservations, $reservations, $arr_tag) {
    $today = date("Ymd");

    foreach ($dates as $date) {
        $timestamp = to_timestamp($date);
        $month = date("m", $timestamp);
        $day = date("d", $timestamp);
        $weekday_info = get_weekday_info($timestamp);
        $holiday = ktHolidayName($timestamp);
        if ($date == $today) {
            $bgcolor = "#ccffcc";
        } else if ($holiday != "") {
            $bgcolor = "#fadede";
        } else if ($arr_calendar_memo["{$date}_type"] == "5") {
            $bgcolor = "#defafa";
        } else {
            $bgcolor = $weekday_info["color"];
        }
        // カレンダーのメモがある場合は設定する
        if ($arr_calendar_memo["$date"] != "") {
            if ($holiday == "") {
                $holiday = $arr_calendar_memo["$date"];
            } else {
                $holiday .= "<br>".$arr_calendar_memo["$date"];
            }
        }
        // 日ごとのデータ数確認
        $rowspan = count($timeless_reservations[$date]) + count($reservations[$date]);
        if ($rowspan == 0) {
            $rowspan = 1;
        }
        $td_height = intval(100 / $rowspan)."%";
        echo("<tr height=\"40\">\n");
        echo("<td rowspan=\"$rowspan\" width=\"48\" nowrap align=\"center\" valign=\"top\" bgcolor=\"$bgcolor\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><b>{$day}日<br>{$weekday_info["label"]}曜日</b></font><br>");
        if ($holiday != "") {
            echo("<font color=\"red\" class=\"j12\">$holiday</font>");
        }
        echo("</td>\n");
        // 日ごとのデータを表示、時刻指定なし、あり
        $tmp_reservations = $reservations[$date];
        $tmp_timeless_reservations = $timeless_reservations[$date];
        $idx = 0;
        $max_cnt = count($tmp_timeless_reservations) + count($tmp_reservations);
        if ($max_cnt == 0) {
            $colspan = count($arr_tag) + 2;
            echo("<td colspan=\"$colspan\">&nbsp;</td>\n");
        }
        // 時刻指定なし
        foreach ($tmp_timeless_reservations as $reservation) {

            if ($idx > 0) {
                echo("<tr>\n");
            }
            $arr_data = array();
            if ($reservation["fclhist_content_type"] == "2") {
                $arr_data = get_data_from_xml($reservation["content"]);
            }

            echo("<td height=\"$td_height\" style=\"border-right-style:none;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">&nbsp;</font></td>\n");
            echo("<td width=\"80\" style=\"border-left-style:none;border-right-style:none;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . h($reservation["title"]) . "</font></td>\n");
            // テンプレートの各データ表示
            for ($i=0,$j=count($arr_tag); $i<$j; $i++) {
                $tag = $arr_tag[$i];
                $style_right = ($i < $j-1) ? "border-right-style:none;" : "";
                $nowrap = ($tag == "Doctor") ? " nowrap" : "";
                if ($arr_data[$tag] != "") {
                    $tmp_data = h($arr_data[$tag]);
                    $tmp_data = str_replace("\n", "<br>", $tmp_data);
                    echo("<td style=\"border-left-style:none;$style_right\"$nowrap><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$tmp_data}</font></td>\n");
                } else {
                    echo("<td style=\"border-left-style:none;$style_right\"$nowrap><br></td>\n");
                }
            }
            if ($idx < $max_cnt - 1) {
                echo("</tr>\n");
            }
            $idx++;
        }
        // 時刻指定あり
        foreach ($tmp_reservations as $reservation) {

            if ($idx > 0) {
                echo("<tr>\n");
            }
            $arr_data = array();
            if ($reservation["fclhist_content_type"] == "2") {
                $arr_data = get_data_from_xml($reservation["content"]);
            }

            $formatted_start_time = preg_replace("/^(\d{2})(\d{2})$/", "$1:$2", $reservation["start_time"]);
            echo("<td height=\"$td_height\" style=\"border-right-style:none;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$formatted_start_time}</font></td>\n");
            echo("<td width=\"80\" style=\"border-left-style:none;border-right-style:none;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . h($reservation["title"]) . "</font></td>\n");
            // テンプレートの各データ表示
            for ($i=0,$j=count($arr_tag); $i<$j; $i++) {
                $tag = $arr_tag[$i];
                $style_right = ($i < $j-1) ? "border-right-style:none;" : "";
                $nowrap = ($tag == "Doctor") ? " nowrap" : "";
                if ($arr_data[$tag] != "") {
                    $tmp_data = h($arr_data[$tag]);
                    $tmp_data = str_replace("\n", "<br>", $tmp_data);
                    echo("<td style=\"border-left-style:none;$style_right\"$nowrap><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$tmp_data}</font></td>\n");
                } else {
                    echo("<td style=\"border-left-style:none;$style_right\"$nowrap><br></td>\n");
                }
            }
            if ($idx < $max_cnt - 1) {
                echo("</tr>\n");
            }
            $idx++;
        }

        echo("</tr>\n");
    }
}

// 予約情報を配列で取得
// facility_week_chart_common.iniのget_reservations関数の後半の時系列化処理をしないデータを返す
function get_reservations_for_detail_print($con, $cate_id, $fcl_id, $dates, $base_min, $fcl_admin_flg, $emp_id, $pt_flg, $fname) {
    if ($cate_id == "" || $fcl_id == "") {
        return;
    }

    if (!is_array($dates)) {
        $dates = array($dates);
    }

    // タイムチャート表示時間を配列で取得
    $time_span = get_time_span($con, $cate_id, $fname);
    $chart_start_hhmm = sprintf("%02d%02d", intval($time_span[0] / 60), ($time_span[0] % 60));
    $chart_end_hhmm = sprintf("%02d%02d", intval($time_span[1] / 60), ($time_span[1] % 60));

    $reservations = array();
    foreach ($dates as $date) {
        $reservations[$date] = array();

        // 処理日の予約情報を格納
        $sql = "select r.*, c.fclcate_name, f.facility_name, re.emp_lt_nm || ' ' || re.emp_ft_nm as reg_emp_nm, ue.emp_lt_nm || ' ' || ue.emp_ft_nm as upd_emp_nm, t1.type_name as type1_nm, t2.type_name as type2_nm, t3.type_name as type3_nm, fclhist.fclhist_content_type from reservation r inner join fclcate c on c.fclcate_id = r.fclcate_id inner join facility f on f.facility_id = r.facility_id inner join empmst re on re.emp_id = r.emp_id left join empmst ue on ue.emp_id = r.upd_emp_id left join rsvtype t1 on t1.type_id = r.type1 left join rsvtype t2 on t2.type_id = r.type2 left join rsvtype t3 on t3.type_id = r.type3 left join fclhist on fclhist.facility_id = r.facility_id and fclhist.fclhist_id = r.fclhist_id";
        $cond = "where r.facility_id = $fcl_id and r.date = '$date' and r.reservation_del_flg = 'f' order by r.start_time";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        if (pg_num_rows($sel) > 0) {
            $tmp_reservations = pg_fetch_all($sel);
            foreach ($tmp_reservations as $i => $reservation) {
                $tmp_reservation = $reservation;
                $tmp_reservation["show_start_time"] = ($chart_start_hhmm <= $reservation["start_time"]) ? $reservation["start_time"] : $chart_start_hhmm;
                $tmp_reservation["show_end_time"] = ($reservation["end_time"] <= $chart_end_hhmm) ? $reservation["end_time"] : $chart_end_hhmm;
                $tmp_reservation["updatable"] = ($fcl_admin_flg || ($reservation["emp_id"] == $emp_id));
                $tmp_types = array();
                if ($reservation["type1_nm"] != "") {$tmp_types[] = $reservation["type1_nm"];}
                if ($reservation["type2_nm"] != "") {$tmp_types[] = $reservation["type2_nm"];}
                if ($reservation["type3_nm"] != "") {$tmp_types[] = $reservation["type3_nm"];}
                $tmp_reservation["type_nm"] = join(", ", $tmp_types);
                if ($pt_flg) {
                    $tmp_reservation["patients"] = $reservation["pt_name"];
                }
                $tmp_reservations[$i] = $tmp_reservation;
            }
            $reservations[$date] = array_merge($reservations[$date], $tmp_reservations);
        }

    }

    return $reservations;
}

// XMLのデータから各項目のデータを取得する。タグをキーとした配列データを返す
function get_data_from_xml($content) {

    $arr_data = array();

    if (strlen($content) == 0 ){
        return $arr_data;
    }

    $utf_content = mb_convert_encoding(mb_convert_encoding($content,"sjis-win","eucJP-win"),"UTF-8","sjis-win");
    $utf_content = str_replace("EUC-JP", "UTF-8", $utf_content);
    $utf_content = preg_replace("/[\x01-\x08\x0b\x0c\x0e-\x1f\x7f]+/","",$utf_content);
    $utf_content = str_replace("\0","",$utf_content);
    $doc = domxml_open_mem($utf_content);
    $root = $doc->get_elements_by_tagname('apply');
    $node_array = $root[0]->child_nodes();

    $old_tagname = "";
    foreach ($node_array as $child) {
        if ($child->node_type() == XML_ELEMENT_NODE) {
            $itemvalue = mb_convert_encoding($child->get_content(), "eucJP-win", "UTF-8");
            $type = $child->get_attribute("type");
            // \、"をエスケープする
            if ($type != "checkbox" && $type != "radio") {
                $itemvalue = str_replace("\\", "\\\\", $itemvalue);
                $itemvalue = str_replace("\"", "\\\"", $itemvalue);
            }

            $itemvalue = str_replace("\r\n", "\n", $itemvalue);

            if ($itemvalue != "") {
                // checkboxで同じタグの場合、続けて表示
                if ($type == "checkbox" && $child->tagname == $old_tagname) {
                    $arr_data[$child->tagname] .= "、".$itemvalue;
                } else {
                    $arr_data[$child->tagname] = $itemvalue;
                }
                $first_flg = false;
            }
            $old_tagname = $child->tagname;
        }
    }

    return $arr_data;
}

// 表示範囲を出力
function show_category_string($categories, $cate_id) {
    if (count($categories) == 0) {
        echo("（未登録）");
        return;
    }

    foreach ($categories as $tmp_category_id => $tmp_category) {
        if ($tmp_category_id != $cate_id) {
            continue;
        }
        echo("【{$tmp_category["name"]}】");
        break;
    }
}

// 選択日付を出力
function show_date_string($date, $start_day, $session, $range) {
    $tmp_start_day = date("Y/m/d", $start_day);
    $tmp_end_day = date("Y/m/d", strtotime("+6 days", $start_day));
    echo("【{$tmp_start_day}〜{$tmp_end_day}】");
}

// 設備リストを出力
function show_facilities($categories, $cate_id, $fcl_id, $range, $date, $session) {
    if (count($categories) == 0) {
        return;
    }
    if (count($categories[$cate_id]["facilities"]) == 0) {
        return;
    }

    echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" style=\"margin-top:5px;\">\n");
    echo("<tr>\n");

    foreach ($categories[$cate_id]["facilities"] as $tmp_fcl_id => $tmp_facility) {
        if ($tmp_fcl_id != $fcl_id) {
            continue;
        }
        echo("<td style=\"padding-right:15px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
        echo("<b>");
        echo($tmp_facility["name"]);
        echo("</b>");
        echo("</font></td>\n");
        break;
    }

    echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">\n");
    $time_str = date("Y")."/".date("m")."/".date("d")." ".date("H").":".date("i");
    echo("作成日時：$time_str\n");
    echo("</font></td>\n");
    echo("</tr>\n");
    echo("</table>\n");
}
