<?

require_once("Cmx.php");
require_once("aclg_set.php");
require_once("about_comedix.php");

$fname = $_SERVER['PHP_SELF'];
$notice_name = $_REQUEST['notice_name'];

// セッションのチェック
$session = qualify_session($_REQUEST['session'], $fname);
if ($session == "0") {
    js_login_exit();
}

// 権限のチェック
$checkauth = check_authority($session, 24, $fname);
if ($checkauth == "0") {
    js_login_exit();
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"], $_POST, $con);

// 入力チェック
if ($notice_name == "") {
    pg_close($con);
    echo("<script type=\"text/javascript\">alert('通達区分が入力されていません。');</script>");
    echo("<script type=\"text/javascript\">history.back();</script>");
    exit;
}
if (strlen($notice_name) > 30) {
    pg_close($con);
    echo("<script type=\"text/javascript\">alert('通達区分が長すぎます。');</script>");
    echo("<script type=\"text/javascript\">history.back();</script>");
    exit;
}

// トランザクションを開始
pg_query($con, "begin");

// 通達区分IDの採番
$sql = "select coalesce(max(notice_id) + 1, 1) from newsnotice";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$notice_id = pg_fetch_result($sel, 0, 0);

// 通達区分の登録
$sql = "insert into newsnotice (notice_id, notice_name) values (";
$content = array($notice_id, p($notice_name));
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 通達区分一覧画面に遷移
echo("<script type=\"text/javascript\">location.href = 'news_notice_list.php?session=$session';</script>");
