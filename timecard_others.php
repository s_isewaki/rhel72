<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務管理 | その他</title>
<?
require_once("Cmx.php");
require_once('Cmx/Model/SystemConfig.php');
require_once("about_comedix.php");
require_once("referer_common.ini");
require_once("work_admin_menu_common.ini");
require_once("html_utils.php");
require_once("show_timecard_common.ini");
require_once("timecard_paid_hol_hour_class.php");
require_once("atdbk_menu_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

//時間有休
$obj_hol_hour = new timecard_paid_hol_hour_class($con, $fname);
$obj_hol_hour->select();

// 遷移元の取得
$referer = get_referer($con, $session, "work", $fname);

// その他設定を取得
$sql = "select * from timecard";

$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if (pg_num_rows($sel) > 0) {
	$others1 = pg_fetch_result($sel, 0, "others1");
	$others2 = pg_fetch_result($sel, 0, "others2");
	$others3 = pg_fetch_result($sel, 0, "others3");
	$others4 = pg_fetch_result($sel, 0, "others4");
	$irrg_time_w = pg_fetch_result($sel, 0, "irrg_time_w");
	$irrg_time_wd = pg_fetch_result($sel, 0, "irrg_time_wd");
	$irrg_time_m = pg_fetch_result($sel, 0, "irrg_time_m");
	//時間給者の休憩時間追加 20100929
	for ($i = 1; $i <= 7; $i++) {
		$var_name = "rest$i";
		$$var_name = pg_fetch_result($sel, 0, "rest$i");
	}
	$modify_flg = pg_fetch_result($sel, 0, "modify_flg");
	$modify_plan_flg = pg_fetch_result($sel, 0, "modify_plan_flg"); //本人による修正（予定） 20140226
	$ovtmscr_tm = pg_fetch_result($sel, 0, "ovtmscr_tm");
	$over_time_apply_type = pg_fetch_result($sel, 0, "over_time_apply_type");
	$show_time_flg = pg_fetch_result($sel, 0, "show_time_flg");
	$time_move_flag      = pg_fetch_result($sel, 0, "time_move_flag");
	$type_display_flag   = pg_fetch_result($sel, 0, "type_display_flag");
	$group_display_flag  = pg_fetch_result($sel, 0, "group_display_flag");
	$zen_display_flag    = pg_fetch_result($sel, 0, "zen_display_flag");
	$yoku_display_flag   = pg_fetch_result($sel, 0, "yoku_display_flag");
	$ret_display_flag    = pg_fetch_result($sel, 0, "ret_display_flag");
	$time_big_font_flag  = pg_fetch_result($sel, 0, "time_big_font_flag");
	$return_flg = pg_fetch_result($sel, 0, "return_flg");
	$others5 = pg_fetch_result($sel, 0, "others5");
	$delay_overtime_flg = pg_fetch_result($sel, 0, "delay_overtime_flg");
	$early_leave_time_flg = pg_fetch_result($sel, 0, "early_leave_time_flg");
	$overtime_40_flg = pg_fetch_result($sel, 0, "overtime_40_flg");
	$timecard_title = pg_fetch_result($sel, 0, "timecard_title");
	$timecard_appr_disp_flg = pg_fetch_result($sel, 0, "timecard_appr_disp_flg");

	//残業申請判定
	$overtime_apply_checked         = "";
	$overtime_after_apply_checked   = "";
	$overtime_not_apply_checked     = "";
	if ($over_time_apply_type == 1){
		$overtime_apply_checked = " checked";
	}
	else if($over_time_apply_type == 2){
		$overtime_after_apply_checked = " checked";
	}
	else{
		$overtime_not_apply_checked = " checked";
	}
	//所属の表示
	$class_disp_flg = pg_fetch_result($sel, 0, "class_disp_flg");
	$atrb_disp_flg = pg_fetch_result($sel, 0, "atrb_disp_flg");
	$dept_disp_flg = pg_fetch_result($sel, 0, "dept_disp_flg");
	$room_disp_flg = pg_fetch_result($sel, 0, "room_disp_flg");

	$arr_org_disp_flg = array();
	$arr_org_disp_flg[0] = $class_disp_flg;
	$arr_org_disp_flg[1] = $atrb_disp_flg;
	$arr_org_disp_flg[2] = $dept_disp_flg;
	$arr_org_disp_flg[3] = $room_disp_flg;

	//残業申請状況の表示フラグ
	$over_apply_disp_flg = pg_fetch_result($sel, 0, "over_apply_disp_flg");
	//退勤復帰アイコンフラグ 1,"":退勤復帰 2:呼出出勤
	$return_icon_flg = pg_fetch_result($sel, 0, "return_icon_flg");
	//残業を法定内・外で分割フラグ
	$over_disp_split_flg = pg_fetch_result($sel, 0, "over_disp_split_flg");

    //月間残業時間60時間超計算 20130405
    $over_disp_sixty_flg = pg_fetch_result($sel, 0, "over_disp_sixty_flg");

    //週40時間残業を適用する 20130425
    $over_disp_forty_flg = pg_fetch_result($sel, 0, "over_disp_forty_flg");
    $over_disp_forty_week = pg_fetch_result($sel, 0, "over_disp_forty_week"); //起算曜日
    $over_forty_persons_full = pg_fetch_result($sel, 0, "over_forty_persons_full"); //対象者
    $over_forty_persons_short = pg_fetch_result($sel, 0, "over_forty_persons_short");
    $over_forty_persons_part = pg_fetch_result($sel, 0, "over_forty_persons_part");

    //一括修正画面の表示設定
	//申請状態
	$all_apply_disp_flg = pg_fetch_result($sel, 0, "all_apply_disp_flg");
	//当直
	$all_duty_disp_flg = pg_fetch_result($sel, 0, "all_duty_disp_flg");
	//法定内・法定外残業
	$all_legal_over_disp_flg = pg_fetch_result($sel, 0, "all_legal_over_disp_flg");
	//残業未申請を表示しない時間（分）
	$over_yet_apply_disp_min = pg_fetch_result($sel, 0, "over_yet_apply_disp_min");
	//メニュー表示設定
	$workflow_menu_flg = pg_fetch_result($sel, 0, "workflow_menu_flg");
	$shift_menu_flg = pg_fetch_result($sel, 0, "shift_menu_flg");
	$shift_refer_menu_flg = pg_fetch_result($sel, 0, "shift_refer_menu_flg");
	$shift_list_menu_flg = pg_fetch_result($sel, 0, "shift_list_menu_flg");
	$shift_duty_menu_flg = pg_fetch_result($sel, 0, "shift_duty_menu_flg");

	//年次有給休暇簿表示設定 20131112 YOKOBORI
	$year_paid_hol_menu_flg = pg_fetch_result($sel, 0, "year_paid_hol_menu_flg");

	//メニュー表示、最初の画面表示 20130129
	$arr_menu_view = array();
	$arr_menu_view[1] = pg_fetch_result($sel, 0, "menu_view1");
	$arr_menu_view[2] = pg_fetch_result($sel, 0, "menu_view2");
	$arr_menu_view[3] = pg_fetch_result($sel, 0, "menu_view3");
	$arr_menu_view[4] = pg_fetch_result($sel, 0, "menu_view4");
	$arr_menu_view[5] = pg_fetch_result($sel, 0, "menu_view5");
	$arr_menu_view[6] = pg_fetch_result($sel, 0, "menu_view6");
	$menu_view_default = pg_fetch_result($sel, 0, "menu_view_default");

	// 出勤簿タイトル 20130129
	$timecard_title2 = pg_fetch_result($sel, 0, "timecard_title2");
}
else {
	//集計処理の設定を「集計しない」の固定とする 20100910
	$others1 = "1";	//勤務時間内に外出した場合
	$others2 = "1";	//勤務時間外に外出した場合
	$others3 = "1";	//時間給者の休憩の集計
	$others4 = "1";	//遅刻・早退に休憩が含まれた場合
	$others5 = "2";	//早出残業時間の集計
}
// デフォルト値の設定
if ($modify_flg == "") {$modify_flg = "t";}
if ($show_time_flg == "") {$show_time_flg = "t";}
if ($return_flg == "") {$return_flg = "f";}
if ($timecard_title == "") {$timecard_title = "タイムカード";}
if ($timecard_title2 == "") {$timecard_title2 = "出勤簿";}
if ($timecard_appr_disp_flg == "") {$timecard_appr_disp_flg = "f";}
if ($st_name_cnt == "") { $st_name_cnt = 0; }
if ($del_st_name_idx == "") { $del_st_name_idx = -1; }
if ($over_apply_disp_flg == "") {$over_apply_disp_flg = "t";}
if ($return_icon_flg == "") {$return_icon_flg = "1";}
if ($over_disp_split_flg == "") {$over_disp_split_flg = "t";}
if ($over_disp_sixty_flg == "") {$over_disp_sixty_flg = "t";}
if ($over_disp_forty_flg == "") {$over_disp_forty_flg = "f";}
if ($over_disp_forty_week == "") {$over_disp_forty_week = 0;}
if ($over_forty_persons_full == "") {$over_forty_persons_full = "f";}
if ($over_forty_persons_short == "") {$over_forty_persons_short = "f";}
if ($over_forty_persons_part == "") {$over_forty_persons_part = "f";}
if ($all_apply_disp_flg == "") {$all_apply_disp_flg = "t";}
if ($all_duty_disp_flg == "") {$all_duty_disp_flg = "t";}
if ($all_legal_over_disp_flg == "") {$all_legal_over_disp_flg = "t";}
if ($workflow_menu_flg == "") { $workflow_menu_flg = "t"; }
if ($shift_menu_flg == "") { $shift_menu_flg = "t"; }
if ($shift_refer_menu_flg == "") { $shift_refer_menu_flg = "t"; }
if ($shift_list_menu_flg == "") { $shift_list_menu_flg = "t"; }
if ($shift_duty_menu_flg == "") { $shift_duty_menu_flg = "t"; }
if ($year_paid_hol_menu_flg == "") { $year_paid_hol_menu_flg = "f"; }
if ($menu_view_default == "" ) { $menu_view_default = 1; }

// 環境設定より退勤後復帰ボタン表示フラグを取得
$sql = "select ret_btn_flg from config";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$ret_btn_flg = (pg_fetch_result($sel, 0, "ret_btn_flg") == "t");

	///-----------------------------------------------------------------------------
	// 決裁欄役職情報を取得
	///-----------------------------------------------------------------------------

	$st_name_array = array();
	$wk_array = array();
	if ($reload_flg == "") {
		$wk_st_name_array = get_timecard_st_name_array($con, $fname);
		$st_name1 = $wk_st_name_array[0]["st_name"];
		$st_name2 = $wk_st_name_array[1]["st_name"];
		$st_name3 = $wk_st_name_array[2]["st_name"];

		for ($i=3; $i<count($wk_st_name_array); $i++) {
			$st_name_array[$i-3]["st_name"] = $wk_st_name_array[$i]["st_name"];
		}

		$st_name_cnt = count($st_name_array);
	} else {
		$m=0;
		for ($i=0; $i<$st_name_cnt; $i++) {
			//削除データ以外を設定
			if ($i != (int)$del_st_name_idx) {
				$varname = "st_name".($i+4);
				$wk_array[$m]["st_name"] = $$varname;
				$m++;
			}
		}
		//追加
		if ($add_st_name_idx != ""){
			$wk_array[$m]["st_name"] = "";
		}
		//再表示用データ入れ替え
		$st_name_array = $wk_array;
		$st_name_cnt = count($st_name_array);

		$add_st_name_idx = "";
		$del_st_name_idx = -1;

	}

	if ($st_name1 == "" && $st_name2 == "" && $st_name3 == "") {
		$st_name1 = "院長";
		$st_name2 = "看護部長";
		$st_name3 = "師長";
	}

//集計項目情報
$arr_total_flg = get_timecard_total_flg($con, $fname);

// 組織階層情報取得
$arr_org_level = array ();
$level_cnt = 0;
$sql = "select class_nm, atrb_nm, dept_nm, room_nm, class_cnt from classname ";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo ("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo ("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$arr_org_level[0] = pg_result($sel, 0, "class_nm");
$arr_org_level[1] = pg_result($sel, 0, "atrb_nm");
$arr_org_level[2] = pg_result($sel, 0, "dept_nm");
$arr_org_level[3] = pg_result($sel, 0, "room_nm");
$level_cnt = pg_result($sel, 0, "class_cnt");

// opt以下のphpを表示するかどうか(申請用追加機能有無判定)
$opt_display_flag = file_exists("opt/flag");

// system_config
$conf = new Cmx_SystemConfig();

// 残業追加タブを表示する
$ovtm_tab_flg = $conf->get('timecard.ovtm_tab_flg');
//時間をキーボードで入力する
$keyboard_input_flg = $conf->get('timecard.keyboard_input_flg');
//残業１の時間をデフォルト表示しない
$ovtm1_no_default_flg = $conf->get('timecard.ovtm1_no_default_flg');
//残業に休憩時間を入力する
$ovtm_rest_disp_flg = $conf->get('timecard.ovtm_rest_disp_flg');
//労働基準法の休憩時間が取れていない場合は警告する
$rest_check_flg = $conf->get('timecard.rest_check_flg');
//残業申請、タイムカード修正申請画面に当月残業累計時間を表示する
$ovtm_total_disp_flg = $conf->get('timecard.ovtm_total_disp_flg');
//36協定の残業限度時間をこえたら警告する
$ovtm_limit_check_flg = $conf->get('timecard.ovtm_limit_check_flg');
//残業限度時間　月
$ovtm_limit_month = $conf->get('timecard.ovtm_limit_month');
//残業限度時間　年
//$ovtm_limit_year = $conf->get('timecard.ovtm_limit_year');
//1日5回までの出退勤を取り込む
$import_5time_flg = $conf->get('timecard.import_5time_flg');
//ｎ時までの打刻は前日の残業とする
$import_base_time = $conf->get('timecard.import_base_time');
//公休以外の日から連続した、残業は休日残業としない
$no_nextday_hol_ovtm_flg = $conf->get('timecard.no_nextday_hol_ovtm_flg');
//タイムカード修正画面で休憩時間を表示する
$rest_disp_flg = $conf->get('timecard.rest_disp_flg');
//時間有休を遅刻早退と相殺しない
$sousai_sinai_flg = $conf->get('timecard.sousai_sinai_flg');
//遅刻早退があった日に残業した場合の法定内残業を法定外残業に集計する t:する "",f:しない 20141024
//設定の文言としては「遅刻早退の時間を法定内残業、法定外残業の計算に影響させない」"t":影響させない
$delay_overtime_inout_flg = $conf->get('timecard.delay_overtime_inout_flg');

//時間帯画面の設定追加 20141111
$officehours_min_unit = $conf->get('timecard.officehours_min_unit');
if ($officehours_min_unit == "") {
	$officehours_min_unit = 5;
}
//残業時刻2以降にも理由を入力する設定追加 20141226
$ovtm_reason2_input_flg = $conf->get('timecard.ovtm_reason2_input_flg');
//ユーザ側の入力画面で表示しない項目 20150109
$duty_input_nodisp_flg = $conf->get('timecard.duty_input_nodisp_flg');
$allowance_input_nodisp_flg = $conf->get('timecard.allowance_input_nodisp_flg');
$rest_input_nodisp_flg = $conf->get('timecard.rest_input_nodisp_flg');
$out_input_nodisp_flg = $conf->get('timecard.out_input_nodisp_flg');
$hol_hour_input_nodisp_flg = $conf->get('timecard.hol_hour_input_nodisp_flg');
$meeting_input_nodisp_flg = $conf->get('timecard.meeting_input_nodisp_flg');
//手当、会議研修、欠勤の表示 20150113
$list_allowance_disp_flg = $conf->get('timecard.list_allowance_disp_flg');
$list_meeting_disp_flg = $conf->get('timecard.list_meeting_disp_flg');
$list_absence_disp_flg = $conf->get('timecard.list_absence_disp_flg');
//月間残業時間45時間超計算
$over_disp_fortyfive_flg = $conf->get('timecard.over_disp_fortyfive_flg');
if ($over_disp_fortyfive_flg == "") {$over_disp_fortyfive_flg = "f";}
//当直・待機
$duty_or_oncall_flg = $conf->get('timecard.duty_or_oncall_flg');
if ($duty_or_oncall_flg == "") {$duty_or_oncall_flg = "1";}
$duty_str = ($duty_or_oncall_flg == "1") ? "当直" : "待機";
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
	///-----------------------------------------------------------------------------
	// 決裁欄役職
	///-----------------------------------------------------------------------------
	// 追加
	function addStName(idx)
	{
		//行追加
		document.update.del_st_name_idx.value = "";
		document.update.add_st_name_idx.value = idx;
		document.update.action="timecard_others.php#print_setting";
		document.update.submit();
	}
	// 削除
	function delStName(idx)
	{
		//行削除
		document.update.del_st_name_idx.value = idx;
		document.update.add_st_name_idx.value = "";
		document.update.action="timecard_others.php#print_setting";
		document.update.submit();
	}

    function chkOvtmLimit(obj) {

        if (obj.checked) {
            document.getElementById('ovtm_limit_month').disabled = false;
            //document.getElementById('ovtm_limit_year').disabled = false;
        }
        else {
            document.getElementById('ovtm_limit_month').disabled = 'disabled';
            //document.getElementById('ovtm_limit_year').disabled = 'disabled';
        }
    }
    function chkImport5time(obj) {

        if (obj.checked) {
            document.getElementById('import_base_time').disabled = false;
        }
        else {
            document.getElementById('import_base_time').disabled = 'disabled';
        }
    }
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.list {border-collapse:collapse;}
table.list td {border:#5279a5 solid 1px;}
table.block_in {border-collapse:collapse;}
table.block_in td {border:#5279a5 solid 0px;}
table.block_in td td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="work_admin_menu.php?session=<? echo($session); ?>"><img src="img/icon/b23.gif" width="32" height="32" border="0" alt="勤務管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="work_admin_menu.php?session=<? echo($session); ?>"><b>勤務管理</b></a></font></td>
</tr>
</table>
<? } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<!-- atdbk_timecard.phpを atdbk_timecard_shift.phpに変更 20130129 -->
<td width="32" height="32" class="spacing"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><img src="img/icon/b07.gif" width="32" height="32" border="0" alt="出勤表"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>出勤表</b></a> &gt; <a href="work_admin_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } ?>

<?
// メニュータブ
show_work_admin_menuitem($session, $fname, "");
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<form name="update" action="timecard_others_update_exe.php" method="post">
<table width="700" border="0" cellspacing="0" cellpadding="2">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>集計処理</b></font></td>
<td align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<table width="700" border="0" cellspacing="0" cellpadding="2" class="list">
<input type="hidden" name="others1" value="<?=$others1?>">
<input type="hidden" name="others2" value="<?=$others2?>">
<input type="hidden" name="others3" value="<?=$others3?>">
<input type="hidden" name="others4" value="<?=$others4?>">
<input type="hidden" name="others5" value="<?=$others5?>">
<?
/*
	//集計処理の設定を「集計しない」の固定とする 20100910
<tr height="22">
<td width="260" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務時間内に外出した場合</font></td>
<td><select name="others1"><option value="--"><option value="1"<? if ($others1 == "1") {echo(" selected");} ?>>集計しない<option value="2"<? if ($others1 == "2") {echo(" selected");} ?>>遅刻時間に加算する<option value="3"<? if ($others1 == "3") {echo(" selected");} ?>>早退時間に加算する<option value="4"<? if ($others1 == "4") {echo(" selected");} ?>>残業時間から減算する</select></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務時間外に外出した場合</font></td>
<td><select name="others2"><option value="--"><option value="1"<? if ($others2 == "1") {echo(" selected");} ?>>集計しない<option value="2"<? if ($others2 == "2") {echo(" selected");} ?>>遅刻時間に加算する<option value="3"<? if ($others2 == "3") {echo(" selected");} ?>>早退時間に加算する<option value="4"<? if ($others2 == "4") {echo(" selected");} ?>>残業時間から減算する</select></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時間給者の休憩の集計</font></td>
<td><select name="others3"><option value="--"><option value="1"<? if ($others3 == "1") {echo(" selected");} ?>>集計しない<option value="2"<? if ($others3 == "2") {echo(" selected");} ?>>実働時間に加算する</select></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">遅刻・早退に休憩が含まれた場合</font></td>
<td><select name="others4"><option value="--"><option value="1"<? if ($others4 == "1") {echo(" selected");} ?>>集計しない<option value="2"<? if ($others4 == "2") {echo(" selected");} ?>>遅刻・早退時間に加算する</select></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">早出残業時間の集計</font></td>
<td><select name="others5"><option value="--"><option value="1"<? if ($others5 == "1") {echo(" selected");} ?>>残業時間に加算する<option value="2"<? if ($others5 == "2") {echo(" selected");} ?>>集計しない</select></td>
</tr>
*/
?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">遅刻した時間の集計</font></td>
<td><select name="delay_overtime_flg"><option value="1"<? if ($delay_overtime_flg == "1") {echo(" selected");} ?>>残業時間から減算する<option value="2"<? if ($delay_overtime_flg == "2") {echo(" selected");} ?>>残業時間から減算しない</select></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">早退した時間の集計</font></td>
<td><select name="early_leave_time_flg"><option value="1"<? if ($early_leave_time_flg == "1") {echo(" selected");} ?>>残業時間から減算する<option value="2"<? if ($early_leave_time_flg == "2") {echo(" selected");} ?>>残業時間から減算しない</select></td>
</tr>
<? /* 20100525 非表示とする、計算しない固定とする
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務時間の週４０時間超過分</font></td>
<td><select name="overtime_40_flg"><option value="1"<? if ($overtime_40_flg == "1") {echo(" selected");} ?>>法定外残業時間として計算する<option value="2"<? if ($overtime_40_flg == "2") {echo(" selected");} ?>>法定外残業時間として計算しない</select></td>
</tr>
*/
?>
<input type="hidden" name="overtime_40_flg" value="2">
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="700" border="0" cellspacing="0" cellpadding="2">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>変則労働適用時の所定労働時間</b></font></td>
</tr>
</table>
<table width="700" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="115" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">週</font></td>
<td width="140"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="text" name="irrg_time_w" value="<? echo($irrg_time_w); ?>" style="ime-mode:inactive;" size="5" maxlength="2">時間</font></td>
<td width="115" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">開始曜日</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<label><input type="radio" name="irrg_time_wd" value="1"<? if ($irrg_time_wd == "1") {echo(" checked");} ?>>日</label>
<label><input type="radio" name="irrg_time_wd" value="2"<? if ($irrg_time_wd == "2") {echo(" checked");} ?>>月</label>
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">月</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="text" name="irrg_time_m" value="<? echo($irrg_time_m); ?>" style="ime-mode:inactive;" size="5" maxlength="3">時間</font></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="700" border="0" cellspacing="0" cellpadding="2">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>時間給者の休憩時間</b></font></td>
</tr>
</table>
<table width="700" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td align="right" width="260" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">4時間を超えた場合(午前開始)</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="text" name="rest4" value="<? echo($rest4); ?>" style="ime-mode:inactive;" size="3" maxlength="2">分</font></td>
</tr>
<tr height="22">
<td align="right" width="260" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">4時間を超えた場合(午後開始)</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="text" name="rest5" value="<? echo($rest5); ?>" style="ime-mode:inactive;" size="3" maxlength="2">分</font></td>
</tr>
<tr height="22">
<td align="right" width="260" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">6時間を超えた場合</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="text" name="rest1" value="<? echo($rest1); ?>" style="ime-mode:inactive;" size="3" maxlength="2">分</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">8時間を超えた場合</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="text" name="rest2" value="<? echo($rest2); ?>" style="ime-mode:inactive;" size="3" maxlength="2">分</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">9時間を超えた場合</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="text" name="rest3" value="<? echo($rest3); ?>" style="ime-mode:inactive;" size="3" maxlength="2">分</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">12時間を超えた場合</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="text" name="rest6" value="<? echo($rest6); ?>" style="ime-mode:inactive;" size="3" maxlength="3">分</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">20時間を超えた場合</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="text" name="rest7" value="<? echo($rest7); ?>" style="ime-mode:inactive;" size="3" maxlength="3">分</font></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="700" border="0" cellspacing="0" cellpadding="2">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>本人による修正</b></font></td>
</tr>
</table>
<table width="700" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td align="right" width="260"  bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤予定表</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<label><input type="radio" name="modify_plan_flg" value="t"<? if ($modify_plan_flg == "t") {echo(" checked");} ?>>可</label>
<label><input type="radio" name="modify_plan_flg" value="f"<? if ($modify_plan_flg == "f") {echo(" checked");} ?>>不可</label>
</font></td>
</tr>
<tr height="22">
<td align="right" width="260"  bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイムカード入力</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<label><input type="radio" name="modify_flg" value="t"<? if ($modify_flg == "t") {echo(" checked");} ?>>可</label>
<label><input type="radio" name="modify_flg" value="f"<? if ($modify_flg == "f") {echo(" checked");} ?>>不可</label>
</font></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="700" border="0" cellspacing="0" cellpadding="2">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>タイムカード表示</b></font></td>
<td align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<table width="700" border="0" cellspacing="0" cellpadding="2" class="list">
	<!-- メニュー表示、デフォルトの設定 20130129 -->
	<tr height="22">
		<td align="right" width="260"  bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メニュー表示</font></td>
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">

				<label><input type="checkbox" name="menu_view1" value="t" <?php if($arr_menu_view[1] == "t") {echo (" checked");} ?>>出勤簿</label>
				<label><input type="checkbox" name="menu_view2" value="t" <?php if($arr_menu_view[2] == "t") {echo (" checked");} ?>>日別修正</label>
				<label><input type="checkbox" name="menu_view3" value="t" <?php if($arr_menu_view[3] == "t") {echo (" checked");} ?>>日別修正(A4横)</label>
				<label><input type="checkbox" name="menu_view4" value="t" <?php if($arr_menu_view[4] == "t") {echo (" checked");} ?>>一括修正</label>
<?php
if ($opt_display_flag) {
?>
				<label><input type="checkbox" name="menu_view5" value="t" <?php if($arr_menu_view[5] == "t") {echo (" checked");} ?>>出勤簿（超勤区分）</label>
				<label><input type="checkbox" name="menu_view6" value="t" <?php if($arr_menu_view[6] == "t") {echo (" checked");} ?>>超勤区分内訳</label>

<?php } ?>
			</font>
		</td>
	</tr>
	<tr height="22">
		<td align="right" width="260"  bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">最初画面の表示</font></td>
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<select name="menu_view_default" width = "100">
					<option value="1" <?php if($menu_view_default == "1") {echo (" selected");} ?>>出勤簿</option>
					<option value="2" <?php if($menu_view_default == "2") {echo (" selected");} ?>>日別修正</option>
					<option value="3" <?php if($menu_view_default == "3") {echo (" selected");} ?>>日別修正(A4横)</option>
<?php
if ($opt_display_flag) {
?>
					<option value="5" <?php if($menu_view_default == "5") {echo (" selected");} ?>>出勤簿（超勤区分）</option>
					<option value="6" <?php if($menu_view_default == "6") {echo (" selected");} ?>>超勤区分内訳</option>
<?php } ?>
				</select>
			</font>
		</td>
	</tr>

	<tr height="22">
		<td align="right" width="260"  bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤時刻・退勤時刻を表示する</font></td>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
		<label><input type="radio" name="show_time_flg" value="t"<? if ($show_time_flg == "t") {echo(" checked");} ?>>する</label>
		<label><input type="radio" name="show_time_flg" value="f"<? if ($show_time_flg == "f") {echo(" checked");} ?>>しない（打刻済みは「○」と表示されます）</label>
		</font></td>
	</tr>
	<tr height="22">
		<td align="right" width="260"  bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤、退勤時刻の列を曜日の次に表示する</font></td>
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<label><input type="radio" name="time_move_flag" value="t"<? if ($time_move_flag == "t") {echo(" checked");} ?>>する</label>
				<label><input type="radio" name="time_move_flag" value="f"<? if ($time_move_flag == "f") {echo(" checked");} ?>>しない</label>
			</font>
		</td>
	</tr>
	<tr height="22">
		<td align="right" width="260"  bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日付種別を表示する</font></td>
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<label><input type="radio" name="type_display_flag" value="t"<? if ($type_display_flag == "t") {echo(" checked");} ?>>する</label>
				<label><input type="radio" name="type_display_flag" value="f"<? if ($type_display_flag == "f") {echo(" checked");} ?>>しない</label>
			</font>
		</td>
	</tr>
	<tr height="22">
		<td align="right" width="260"  bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務シフトグループを表示する</font></td>
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<label><input type="radio" name="group_display_flag" value="t"<? if ($group_display_flag == "t") {echo(" checked");} ?>>する</label>
				<label><input type="radio" name="group_display_flag" value="f"<? if ($group_display_flag == "f") {echo(" checked");} ?>>しない</label>
			</font>
		</td>
	</tr>
	<tr height="22">
		<td align="right" width="260"  bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">前日出勤の「前」を表示する</font></td>
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<label><input type="radio" name="zen_display_flag" value="t"<? if ($zen_display_flag == "t") {echo(" checked");} ?>>する</label>
				<label><input type="radio" name="zen_display_flag" value="f"<? if ($zen_display_flag == "f") {echo(" checked");} ?>>しない</label>
			</font>
		</td>
	</tr>
	<tr height="22">
		<td align="right" width="260"  bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">翌日退勤の「翌」を表示する</font></td>
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<label><input type="radio" name="yoku_display_flag" value="t"<? if ($yoku_display_flag == "t") {echo(" checked");} ?>>する</label>
				<label><input type="radio" name="yoku_display_flag" value="f"<? if ($yoku_display_flag == "f") {echo(" checked");} ?>>しない</label>
			</font>
		</td>
	</tr>
	<tr height="22">
		<td align="right" width="260"  bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退勤後復帰情報を表示する</font></td>
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<label><input type="radio" name="ret_display_flag" value="t"<? if ($ret_display_flag == "t") {echo(" checked");} ?>>する</label>
				<label><input type="radio" name="ret_display_flag" value="f"<? if ($ret_display_flag == "f") {echo(" checked");} ?>>しない</label>
			</font>
		</td>
	</tr>
	<tr height="22">
		<td align="right" width="260"  bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">手当を表示する<br>※「出勤簿」画面にのみ有効</font></td>
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<label><input type="radio" name="list_allowance_disp_flg" value="t"<? if ($list_allowance_disp_flg != "f") {echo(" checked");} ?>>する</label>
				<label><input type="radio" name="list_allowance_disp_flg" value="f"<? if ($list_allowance_disp_flg == "f") {echo(" checked");} ?>>しない</label>
			</font>
		</td>
	</tr>
	<tr height="22">
		<td align="right" width="260"  bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">会議研修を表示する<br>※「出勤簿」画面にのみ有効</font></td>
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<label><input type="radio" name="list_meeting_disp_flg" value="t"<? if ($list_meeting_disp_flg != "f") {echo(" checked");} ?>>する</label>
				<label><input type="radio" name="list_meeting_disp_flg" value="f"<? if ($list_meeting_disp_flg == "f") {echo(" checked");} ?>>しない</label>
			</font>
		</td>
	</tr>
	<tr height="22">
		<td align="right" width="260"  bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">欠勤を表示する<br>※「出勤簿」画面にのみ有効</font></td>
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<label><input type="radio" name="list_absence_disp_flg" value="t"<? if ($list_absence_disp_flg == "t") {echo(" checked");} ?>>する</label>
				<label><input type="radio" name="list_absence_disp_flg" value="f"<? if ($list_absence_disp_flg != "t") {echo(" checked");} ?>>しない</label>
			</font>
		</td>
	</tr>
	<tr height="22">
		<td align="right" width="260"  bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">打刻時刻の文字サイズ</font></td>
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<label><input type="radio" name="time_big_font_flag" value="t"<? if ($time_big_font_flag == "t") {echo(" checked");} ?>>大</label>
				<label><input type="radio" name="time_big_font_flag" value="f"<? if ($time_big_font_flag == "f") {echo(" checked");} ?>>標準</label>
			</font>
		</td>
	</tr>
	<tr height="22">
		<td align="right" width="260"  bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属の表示</font></td>
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
//所属の表示、階層別のチェックボックス
for ($i=0; $i<$level_cnt; $i++) {
	$checked = ($arr_org_disp_flg[$i] == "t") ? " checked" : "";
	echo("<label><input type=\"checkbox\" name=\"org_disp_flg_$i\" value=\"t\" $checked>".$arr_org_level[$i]);
	echo("</label>&nbsp;\n");
}
?>
			</font>
		</td>
	</tr>
	<tr height="22">
		<td align="right" width="260"  bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">残業申請状況の表示</font></td>
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<label><input type="radio" name="over_apply_disp_flg" value="t"<? if ($over_apply_disp_flg == "t") {echo(" checked");} ?>>する（<input type="text" name="over_yet_apply_disp_min" value="<? echo($over_yet_apply_disp_min); ?>" style="ime-mode:inactive;" size="2" maxlength="2">分未満は、残業未申請を表示しない。）</label><br>
				<label><input type="radio" name="over_apply_disp_flg" value="1"<? if ($over_apply_disp_flg == "1") {echo(" checked");} ?>>残業未申請以外表示する</label><br>
				<label><input type="radio" name="over_apply_disp_flg" value="f"<? if ($over_apply_disp_flg == "f") {echo(" checked");} ?>>しない</label>
				</font>
		</td>
	</tr>
	<tr height="22">
		<td align="right" width="260"  bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">残業を法定内・外で分割</font></td>
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<label><input type="radio" name="over_disp_split_flg" value="t"<? if ($over_disp_split_flg == "t") {echo(" checked");} ?>>する</label>
				<label><input type="radio" name="over_disp_split_flg" value="f"<? if ($over_disp_split_flg == "f") {echo(" checked");} ?>>しない</label>
			</font>
		</td>
	</tr>
	<tr height="22">
		<td align="right" width="260"  bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">月間残業時間45時間超計算<br>※「出勤簿」画面にのみ有効</font></td>
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<label><input type="radio" name="over_disp_fortyfive_flg" value="t"<? if ($over_disp_fortyfive_flg == "t") {echo(" checked");} ?>>する</label>
				<label><input type="radio" name="over_disp_fortyfive_flg" value="f"<? if ($over_disp_fortyfive_flg == "f") {echo(" checked");} ?>>しない</label>
			</font>
		</td>
	</tr>
	<tr height="22">
		<td align="right" width="260"  bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">月間残業時間60時間超計算<br>※「出勤簿」画面にのみ有効</font></td>
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<label><input type="radio" name="over_disp_sixty_flg" value="t"<? if ($over_disp_sixty_flg == "t") {echo(" checked");} ?>>する</label>
				<label><input type="radio" name="over_disp_sixty_flg" value="f"<? if ($over_disp_sixty_flg == "f") {echo(" checked");} ?>>しない</label>
			</font>
		</td>
	</tr>
	<tr height="22">
		<td align="right" width="260"  bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">週40時間残業を適用する<br>※「出勤簿」画面にのみ有効</font></td>
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<label><input type="radio" name="over_disp_forty_flg" value="t"<? if ($over_disp_forty_flg == "t") {echo(" checked");} ?>>する </label><label><input type="radio" name="over_disp_forty_flg" value="f"<? if ($over_disp_forty_flg == "f") {echo(" checked");} ?>>しない</label> <br>起算曜日：
				<label><input type="radio" name="over_disp_forty_week" value="0"<? if ($over_disp_forty_week == "0") {echo(" checked");} ?>>日曜 </label><label><input type="radio" name="over_disp_forty_week" value="1"<? if ($over_disp_forty_week == "1") {echo(" checked");} ?>>月曜</label> <br>対象者：&nbsp;&nbsp;&nbsp;
				<label><input type="checkbox" name="over_forty_persons_full" value="t"<? if ($over_forty_persons_full == "t") { echo(" checked"); } ?>>常勤</label>
				<label><input type="checkbox" name="over_forty_persons_short" value="t"<? if ($over_forty_persons_short == "t") { echo(" checked"); } ?>>短時間正職員</label>
				<label><input type="checkbox" name="over_forty_persons_part" value="t"<? if ($over_forty_persons_part == "t") { echo(" checked"); } ?>>非常勤</label>
			</font>
		</td>
	</tr>
<?php /* 時間有休、外出を追加 20141203 */ ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">遅刻早退、時間有休、外出の時間を法定内残業、<br>法定外残業の計算に影響させない</font></td>
<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<label><input type="checkbox" name="delay_overtime_inout_flg" value="t"<? if ($delay_overtime_inout_flg == "t") {echo(" checked");} ?>>影響させない</label>
			</font>
</td>
</tr>


</table>
<? /* 時間有休 20111207 */ ?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="700" border="0" cellspacing="0" cellpadding="2">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>有給休暇の時間単位取得</b></font></td>
<td align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<table width="700" border="0" cellspacing="0" cellpadding="2" class="list">
	<tr height="22">
		<td align="right" width="260"  bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">有給休暇の時間単位取得</font></td>
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<label><input type="radio" name="paid_hol_hour_flag" value="t"<? if ($obj_hol_hour->paid_hol_hour_flag == "t") {echo(" checked");} ?>>許可する</label>
				<label><input type="radio" name="paid_hol_hour_flag" value="f"<? if ($obj_hol_hour->paid_hol_hour_flag == "f") {echo(" checked");} ?>>許可しない</label>
			</font>
		</td>
	</tr>
	<tr height="22">
		<td align="right" width="260"  bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時間有休の対象者</font></td>
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<label><input type="checkbox" name="paid_hol_hour_full_flag" value="t"<? if ($obj_hol_hour->paid_hol_hour_full_flag == "t") {echo(" checked");} ?>>常勤</label>
				<label><input type="checkbox" name="paid_hol_hour_part_flag" value="t"<? if ($obj_hol_hour->paid_hol_hour_part_flag == "t") {echo(" checked");} ?>>非常勤</label>
			</font>
		</td>
	</tr>
	<tr height="22">
		<td align="right" width="260"  bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">年間の時間有休の取得日数</font></td>
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<label><input type="radio" name="paid_hol_hour_max_flag" value="t"<? if ($obj_hol_hour->paid_hol_hour_max_flag == "t") {echo(" checked");} ?>>上限あり</label>
				<select name="paid_hol_hour_max_day">
<?
for ($i=1; $i<=5; $i++) {
	echo("<option value=\"{$i}\"");
	if ($obj_hol_hour->paid_hol_hour_max_day == $i) {
		echo(" selected");
	}
	echo(">{$i}</option>\n");

}
				?>
				</select>
				<label><input type="radio" name="paid_hol_hour_max_flag" value="f"<? if ($obj_hol_hour->paid_hol_hour_max_flag == "f") {echo(" checked");} ?>>上限なし</label>
			</font>
		</td>
	</tr>
	<tr height="22">
		<td align="right" width="260"  bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時間有休の取得時間単位</font></td>
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<select name="paid_hol_hour_unit_time">
<option value="1"<? if ($obj_hol_hour->paid_hol_hour_unit_time == "1") {echo(" selected");} ?>>1分</option>
<option value="15"<? if ($obj_hol_hour->paid_hol_hour_unit_time == "15") {echo(" selected");} ?>>15分</option>
<option value="30"<? if ($obj_hol_hour->paid_hol_hour_unit_time == "30") {echo(" selected");} ?>>30分</option>
<option value="60"<? if ($obj_hol_hour->paid_hol_hour_unit_time == "60") {echo(" selected");} ?>>1時間</option>
<option value="120"<? if ($obj_hol_hour->paid_hol_hour_unit_time == "120") {echo(" selected");} ?>>2時間</option>
<option value="180"<? if ($obj_hol_hour->paid_hol_hour_unit_time == "180") {echo(" selected");} ?>>3時間</option>
				</select>
			</font>
		</td>
	</tr>
    <? /* 繰越方法 20120514 */ ?>
	<tr height="22">
		<td align="right" width="260"  bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">繰越方法</font></td>
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<label><input type="radio" name="paid_hol_hour_carry_flg" value="1"<? if ($obj_hol_hour->paid_hol_hour_carry_flg == "1") {echo(" checked");} ?>>残有休時間を日数換算して繰り越す、端数は切り上げる</label><br>
				<label><input type="radio" name="paid_hol_hour_carry_flg" value="2"<? if ($obj_hol_hour->paid_hol_hour_carry_flg == "2") {echo(" checked");} ?>>残有休時間を日数換算して繰り越す、端数は、そのまま繰り越す</label><br>
				<label><input type="radio" name="paid_hol_hour_carry_flg" value="3"<? if ($obj_hol_hour->paid_hol_hour_carry_flg == "3") {echo(" checked");} ?>>残有休時間を日数換算して繰り越す、端数は切り捨てる</label><br>
				<label><input type="radio" name="paid_hol_hour_carry_flg" value="4"<? if ($obj_hol_hour->paid_hol_hour_carry_flg == "4") {echo(" checked");} ?>>残有休時間を半日単位で換算して繰り越す、端数は切り上げる</label>
			</font>
		</td>
	</tr>

    <? /* 出勤簿表示 20140801 */ ?>
	<tr height="22">
		<td align="right" width="260"  bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤簿表示</font></td>
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<label><input type="checkbox" name="sousai_sinai_flg" id="sousai_sinai_flg" value="t"<? if ($sousai_sinai_flg == "t") { echo(" checked"); } ?>>時間有休を遅刻早退と相殺しない</label>
			</font>
		</td>
	</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="700" border="0" cellspacing="0" cellpadding="2">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>（集計項目）</b></font></td>
</tr>
</table>
<table width="700" border="0" cellspacing="0" cellpadding="2" class="list">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
for ($i=0; $i<count($arr_total_flg); $i++) {

	$checked = ($arr_total_flg[$i]["flg"] == "t") ? " checked" : "";
	$wk_name = str_replace("<br>", "", $arr_total_flg[$i]["name"]);
	if ($duty_or_oncall_flg == "2") {
		$wk_name = str_replace("当直", "待機", $wk_name);
	}
	echo("<label><input type=\"checkbox\" name=\"total_".$i."\" value=\"".$arr_total_flg[$i]["id"]."\" $checked>".$wk_name."</label>&nbsp;\n");

}
?>
</font></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<a name="print_setting"></a>
<table width="700" border="0" cellspacing="0" cellpadding="2">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>タイムカード印刷</b></font></td>
</tr>
</table>
<table width="700" border="0" cellspacing="0" cellpadding="2" class="list">
	<tr height="22">
		<td align="right" width="260"  bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイムカードタイトル</font></td>
		<td><input type="text" name="timecard_title" value="<? echo($timecard_title); ?>" size="40" maxlength="40" style="ime-mode:active;"></td>
	</tr>
	<tr height="22">
		<td align="right" width="260"  bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤簿タイトル</font></td>
		<td><input type="text" name="timecard_title2" value="<? echo($timecard_title2); ?>" size="40" maxlength="40" style="ime-mode:active;"></td>
	</tr>
	<tr height="22">
		<td align="right" width="260"  bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">決裁欄役職（左から）</font></td>
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<label><input type="radio" name="timecard_appr_disp_flg" value="f"<? if ($timecard_appr_disp_flg == "f") {echo(" checked");} ?>>表示しない</label>
				<label><input type="radio" name="timecard_appr_disp_flg" value="t"<? if ($timecard_appr_disp_flg == "t") {echo(" checked");} ?>>表示する</label>
			</font>
		</td>
	</tr>
	<tr height="22">
		<td colspan="2">
			<table width="250" border="0" cellspacing="0" cellpadding="2" class="block_in">
			<tr height="22">
			<td width="180"><input type="text" id="st_name1" name="st_name1" size="30" maxlength="30" value="<?=$st_name1?>" style="ime-mode:active;"></td>
			<td width="40"></td>
			</tr>
			<tr height="22">
			<td width="180"><input type="text" id="st_name2" name="st_name2" size="30" maxlength="30" value="<?=$st_name2?>" style="ime-mode:active;"></td>
			<td width="40"></td>
			</tr>
			<tr height="22">
			<td width="180" ><input type="text" id="st_name3" name="st_name3" size="30" maxlength="30" value="<?=$st_name3?>" style="ime-mode:active;"></td>
			<td width="80" align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
			<?
				echo("<input type=\"button\" value=\"追加\" onclick=\"addStName(-1);\"> \n");
			?>
			</font></td>
			</tr>
			<!-- ------------------------------------------------------------------------ -->
			<!-- データ -->
			<!-- ------------------------------------------------------------------------ -->
			<?

			for($i=0; $i<$st_name_cnt;$i++){
				echo("<tr height=\"22\"> \n");
				//ＮＯ
				$wk = $i + 4;

				//役職
				echo("<td width=\"180\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"> \n");
				echo("<input type=\"text\" id=\"st_name$wk\" name=\"st_name$wk\" size=\"30\" maxlength=\"30\" value=\"".$st_name_array[$i]["st_name"]."\" style=\"ime-mode:active;\">\n");
				echo("</font></td> \n");

				//削除
				echo("<td width=\"40\" align=\"left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"> \n");
				echo("<input type=\"button\" value=\"削除\" onclick=\"delStName($i);\"> \n");
				echo("</font></td> \n");
				echo("</tr> \n");
			}

			?>
			</table>
		</td>
	</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="700" border="0" cellspacing="0" cellpadding="2">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>残業申請</b></font></td>
<td align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<table width="700" border="0" cellspacing="0" cellpadding="2" class="list">
	<tr height="22">
		<td width="115" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請画面</font></td>
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<label><input type="radio" name="over_time_apply_type" value="1"<?=$overtime_apply_checked ?>>
				退勤時、残業時間が<input type="text" name="ovtmscr_tm" value="<? echo($ovtmscr_tm); ?>" size="4" maxlength="3" style="ime-mode:inactive;">分超えていたら、残業申請画面を表示する</label><br>
				<label><input type="radio" name="over_time_apply_type" value="2"<?=$overtime_after_apply_checked ?>>
				残業の当日事前申請または、事後申請する</label><br>
				<label><input type="radio" name="over_time_apply_type" value="0"<?=$overtime_not_apply_checked ?>>
				残業申請画面を表示しない</label>
			</font>
		</td>
	</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="700" border="0" cellspacing="0" cellpadding="2">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>退勤後復帰申請・呼出出勤申請</b></font></td>
</tr>
</table>
<table width="700" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="260" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示選択</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<label><input type="radio" name="return_icon_flg" value="1"<? if ($return_icon_flg == "1") {echo(" checked");} ?><? if (!$ret_btn_flg) {echo(" disabled");} ?>>退勤後復帰、復帰後退勤</label><br>
<label><input type="radio" name="return_icon_flg" value="2"<? if ($return_icon_flg == "2") {echo(" checked");} ?><? if (!$ret_btn_flg) {echo(" disabled");} ?>>呼出出勤、呼出退勤</label>
</font></td>
</tr>
<tr height="22">
<td width="260" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退勤後復帰申請画面を表示する</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<label><input type="radio" name="return_flg" value="t"<? if ($return_flg == "t") {echo(" checked");} ?><? if (!$ret_btn_flg) {echo(" disabled");} ?>>する</label>
<label><input type="radio" name="return_flg" value="f"<? if ($return_flg == "f") {echo(" checked");} ?><? if (!$ret_btn_flg) {echo(" disabled");} ?>>しない</label>
</font></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="700" border="0" cellspacing="0" cellpadding="2">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>当直・待機</b></font></td>
</tr>
</table>
<table width="700" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="260" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示選択</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<label><input type="radio" name="duty_or_oncall_flg" value="1"<? if ($duty_or_oncall_flg == "1") {echo(" checked");} ?>>当直</label>
<label><input type="radio" name="duty_or_oncall_flg" value="2"<? if ($duty_or_oncall_flg == "2") {echo(" checked");} ?>>待機</label>
</font></td>
</tr>
<tr height="22">
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="700" border="0" cellspacing="0" cellpadding="2">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>一括修正画面の表示設定</b></font></td>
</tr>
</table>
<table width="700" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<label><input type="checkbox" name="all_apply_disp_flg" value="t"<? if ($all_apply_disp_flg == "t") { echo(" checked"); } ?>>申請状態</label>
<label><input type="checkbox" name="all_duty_disp_flg" value="t"<? if ($all_duty_disp_flg == "t") { echo(" checked"); } ?>><? echo($duty_str); ?></label>
<label><input type="checkbox" name="all_legal_over_disp_flg" value="t"<? if ($all_legal_over_disp_flg == "t") { echo(" checked"); } ?>>法定内・法定外残業</label>
</font></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="700" border="0" cellspacing="0" cellpadding="2">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>メニュー表示設定</b></font></td>
</tr>
</table>
<table width="700" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<label><input type="checkbox" name="workflow_menu_flg" value="t"<? if ($workflow_menu_flg == "t") { echo(" checked"); } ?>>承認者設定</label>
<label><input type="checkbox" name="shift_menu_flg" value="t"<? if ($shift_menu_flg == "t") { echo(" checked"); } ?>>勤務シフト希望</label>
<label><input type="checkbox" name="shift_refer_menu_flg" value="t"<? if ($shift_refer_menu_flg == "t") { echo(" checked"); } ?>>勤務シフト参照</label>
<label><input type="checkbox" name="shift_list_menu_flg" value="t"<? if ($shift_list_menu_flg == "t") { echo(" checked"); } ?>>勤務表印刷</label>
<label><input type="checkbox" name="shift_duty_menu_flg" value="t"<? if ($shift_duty_menu_flg == "t") { echo(" checked"); } ?>><? echo($duty_str); ?>予定表参照</label>
<label><input type="checkbox" name="year_paid_hol_menu_flg" value="t"<? if ($year_paid_hol_menu_flg == "t") { echo(" checked"); } ?>>年次有給休暇簿</label>
</font></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="700" border="0" cellspacing="0" cellpadding="2">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>時間帯画面の設定</b></font></td>
<td align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<table width="700" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="260" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所定時刻の分の単位</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<label><input type="radio" name="officehours_min_unit" value="1"<? if ($officehours_min_unit == "1") {echo(" checked");} ?>>1分</label>&nbsp;
<label><input type="radio" name="officehours_min_unit" value="5"<? if ($officehours_min_unit == "5") {echo(" checked");} ?>>5分</label>
</font></td>
</tr>
</table>

<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="700" border="0" cellspacing="0" cellpadding="2">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>残業申請、タイムカード修正のオプション設定</b></font></td>
</tr>
</table>
<table width="700" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<label><input type="checkbox" name="ovtm_tab_flg" id="ovtm_tab_flg" value="t"<? if ($ovtm_tab_flg == "t") { echo(" checked"); } ?>>残業追加タブを表示する</label>
</font></td>
</tr>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<label><input type="checkbox" name="keyboard_input_flg" id="keyboard_input_flg" value="t"<? if ($keyboard_input_flg == "t") { echo(" checked"); } ?>>時間をキーボードで入力する</label>
</font></td>
</tr>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<label><input type="checkbox" name="ovtm1_no_default_flg" id="ovtm1_no_default_flg" value="t"<? if ($ovtm1_no_default_flg == "t") { echo(" checked"); } ?>>残業１の時間をデフォルト表示しない</label>
</font></td>
</tr>
<?
$disabled = "";
if ($ovtm_rest_disp_flg != "t") {
    $disabled = " disabled";
}

?>
<tr height="22">
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<label><input type="checkbox" name="ovtm_rest_disp_flg" id="ovtm_rest_disp_flg" value="t"<? if ($ovtm_rest_disp_flg == "t") { echo(" checked"); } ?> onclick="if (this.checked) { document.getElementById('rest_check_flg').disabled=false; } else { document.getElementById('rest_check_flg').disabled='disabled'; }">残業に休憩時間を入力する</label>
（
</font>
<label><input type="checkbox" name="rest_check_flg" id="rest_check_flg" value="t"<? if ($rest_check_flg == "t") { echo(" checked"); } ?> <? echo $disabled; ?>>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10">
労働基準法の休憩時間が取れていない場合は警告する
</font>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j9">
（6時間を超えて45分。8時間を超えて60分）
</font>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
）
</font>
</label>
</td>
</tr>
<tr height="22">
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<label><input type="checkbox" name="ovtm_reason2_input_flg" id="ovtm_reason2_input_flg" value="t"<? if ($ovtm_reason2_input_flg == "t") { echo(" checked"); } ?>>残業時刻2以降にも理由を入力する</label>
</font>
</td>
</tr>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<label><input type="checkbox" name="ovtm_total_disp_flg" id="ovtm_total_disp_flg" value="t"<? if ($ovtm_total_disp_flg == "t") { echo(" checked"); } ?>>残業申請、タイムカード修正申請画面に当月残業累計時間を表示する</label>
</font></td>
</tr>
<?
//サブ項目を無効化する
$disabled = "";
if ($ovtm_limit_check_flg != "t") {
    $disabled = " disabled";
}

?>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<label><input type="checkbox" name="ovtm_limit_check_flg" id="ovtm_limit_check_flg" value="t"<? if ($ovtm_limit_check_flg == "t") { echo(" checked"); } ?> onclick="chkOvtmLimit(this);">残業申請時に36協定の月単位残業限度時間を超えたら警告する</label><br>
　　残業限度時間　月単位
<input type="text" name="ovtm_limit_month" id="ovtm_limit_month" value="<? echo $ovtm_limit_month; ?>" style="ime-mode:inactive;" size="2" maxlength="2" <? echo $disabled; ?>>
時間
<? /*
年単位
<input type="text" name="ovtm_limit_year" id="ovtm_limit_year" value="<? echo $ovtm_limit_year; ?>" size="3" maxlength="3" <? echo $disabled; ?>>
時間
*/ ?>
</font></td>
</tr>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<label><input type="checkbox" name="import_5time_flg" id="import_5time_flg" value="t"<? if ($import_5time_flg == "t") { echo(" checked"); } ?> onclick="chkImport5time(this);">タイムレコーダーから1日5回までの出勤と退勤を取込む</label><br>
<?
//サブ項目を無効化する
$disabled = "";
if ($import_5time_flg != "t") {
    $disabled = " disabled";
}

?>

<input type="text" name="import_base_time" id="import_base_time" value="<? echo $import_base_time; ?>" style="ime-mode:inactive;" size="2" maxlength="2" <? echo $disabled; ?>>
時までの打刻は前日の残業とする
</font></td>
</tr>
<? /*
*/ ?>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<label><input type="checkbox" name="no_nextday_hol_ovtm_flg" id="no_nextday_hol_ovtm_flg" value="t"<? if ($no_nextday_hol_ovtm_flg == "t") { echo(" checked"); } ?>>公休以外の日から連続した、残業は休日残業としない</label>
</font></td>
</tr>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<label><input type="checkbox" name="rest_disp_flg" id="rest_disp_flg" value="t"<? if ($rest_disp_flg == "t") { echo(" checked"); } ?>>タイムカード修正画面で所定の休憩時間を表示する</label>
</font></td>
</tr>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
ユーザ側の入力画面で表示しない項目<br>
<label><input type="checkbox" name="duty_input_nodisp_flg" id="duty_input_nodisp_flg" value="t"<? if ($duty_input_nodisp_flg == "t") { echo(" checked"); } ?>><? echo($duty_str); ?></label>&nbsp;
<label><input type="checkbox" name="allowance_input_nodisp_flg" id="allowance_input_nodisp_flg" value="t"<? if ($allowance_input_nodisp_flg == "t") { echo(" checked"); } ?>>手当</label>
<label><input type="checkbox" name="out_input_nodisp_flg" id="out_input_nodisp_flg" value="t"<? if ($out_input_nodisp_flg == "t") { echo(" checked"); } ?>>外出時刻</label>
<label><input type="checkbox" name="hol_hour_input_nodisp_flg" id="hol_hour_input_nodisp_flg" value="t"<? if ($hol_hour_input_nodisp_flg == "t") { echo(" checked"); } ?>>時間有休</label>
<label><input type="checkbox" name="meeting_input_nodisp_flg" id="meeting_input_nodisp_flg" value="t"<? if ($meeting_input_nodisp_flg == "t") { echo(" checked"); } ?>>会議・研修・病棟外勤務</label>
</font></td>
</tr>
</table>

<table width="700" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="reload_flg" value="1">
<input type="hidden" name="st_name_cnt" value="<? echo($st_name_cnt); ?>">
<input type="hidden" name="del_st_name_idx" value="<? echo($del_st_name_idx); ?>">
<input type="hidden" name="add_st_name_idx" value="<? echo($add_st_name_idx); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
