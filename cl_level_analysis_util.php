<?
//ini_set("display_errors","1");
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("cl_common.ini");
require_once("cl_common_log_class.inc");
require_once("Cmx.php");
require_once("MDB2.php");

$log = new cl_common_log_class(basename(__FILE__));

$log->info(basename(__FILE__)." START");

define("CATEGORY_LADDER_INFO_NM","ラダー情報");
define("CATEGORY_PERSONNEL_AFFAIRS_INFO_NM","ラダー・人事情報");

define("CATEGORY_LADDER_INFO_ID","1");
define("CATEGORY_PERSONNEL_AFFAIRS_INFO_ID","2");

define("ITEM_UNIT_EMP_CNT_NM","部署別人数");
define("ITEM_UNIT_YEAR_EMP_CNT_NM","部署年度別人数");
define("ITEM_UNIT_RECOGNITION_YEAR_EMP_CNT_NM","部署認定年度別人数");
define("ITEM_RECOGNITION_EMP_CNT_NM","認定後経過年数別人数");
define("ITEM_UNIT_AVERAGE_EVALUATION_POINT_NM","部署別平均評価点数");
define("ITEM_YEAR_AVERAGE_EVALUATION_POINT_NM","年度別平均評価点数");
define("ITEM_LICENSE_ELAPSED_YEAR_EMP_CNT_NM","看護師免許取得経過年人数");
define("ITEM_TENURE_EMP_CNT_NM","在職年数別人数");
define("ITEM_ASSIGNMENT_YEAR_EMP_CNT_NM","配属年数別人数");
define("ITEM_GRADUATION_EMP_CNT_NM","卒業学校別人数");
define("ITEM_GRADUATION_ELAPSED_YEAR_EMP_CNT_NM","卒業経過年数別人数");

define("ITEM_UNIT_EMP_CNT_ID","1");
define("ITEM_UNIT_YEAR_EMP_CNT_ID","2");
define("ITEM_UNIT_RECOGNITION_YEAR_EMP_CNT_ID","3");
define("ITEM_RECOGNITION_EMP_CNT_ID","4");
define("ITEM_UNIT_AVERAGE_EVALUATION_POINT_ID","5");
define("ITEM_YEAR_AVERAGE_EVALUATION_POINT_ID","6");
define("ITEM_LICENSE_ELAPSED_YEAR_EMP_CNT_ID","7");
define("ITEM_TENURE_EMP_CNT_ID","8");
define("ITEM_ASSIGNMENT_YEAR_EMP_CNT_ID","9");
define("ITEM_GRADUATION_EMP_CNT_ID","10");
define("ITEM_GRADUATION_ELAPSED_YEAR_EMP_CNT_ID","11");

function get_class_division(&$mdb2, $login_emp_id) {

	global $log;

	require_once("cl/model/search/cl_level_analysis_model.php");
	$level_analysis_model = new cl_level_analysis_model(&$mdb2,$login_emp_id);
	$log->debug("レベル分析用DBA取得",__FILE__,__LINE__);

	require_once("cl/model/master/cl_mst_nurse_education_committee_model.php");
	$education_committee_model = new cl_mst_nurse_education_committee_model(&$mdb2,$login_emp_id);
	$log->debug("看護教育委員会DBA取得",__FILE__,__LINE__);

	require_once("cl/model/master/cl_mst_setting_council_model.php");
	$council_model = new cl_mst_setting_council_model(&$mdb2,$login_emp_id);
	$log->debug("審議会DBA取得",__FILE__,__LINE__);

	require_once("cl/model/master/cl_mst_nurse_manager_st_model.php");
	$manager_st_model = new cl_mst_nurse_manager_st_model(&$mdb2,$login_emp_id);
	$log->debug("所属長DBA取得",__FILE__,__LINE__);

	require_once("cl/model/master/cl_mst_supervisor_st_model.php");
	$supervisor_st_model = new cl_mst_supervisor_st_model(&$mdb2,$login_emp_id);
	$log->debug("看護部長DBA取得",__FILE__,__LINE__);

	// 看護教育委員会チェック
	$education_committee = $education_committee_model->check_education_committee($login_emp_id);

	// ログインユーザーが看護教育委員会でない場合
	if (count($education_committee) == 0){
		// 審議会チェック
		$council = $council_model->check_council($login_emp_id);

		// ログインユーザーが看護教育委員会・審議会で無い場合
		if (count($council) == 0){
			// 所属長(役職)チェック
			$supervisor = $supervisor_st_model->check_supervisor($login_emp_id);

			if (count($supervisor) > 0){
				// 所属長(部署)取得
				$supervisor_class = $level_analysis_model->get_supervisor_class();
			}

			// 看護部長(役職)チェック
			$nurse_manager = $manager_st_model->check_nurse_manager($login_emp_id);

			if (count($nurse_manager) > 0){
				// 看護部長(部署)取得
				$nurse_manager_class = $level_analysis_model->get_nurse_manager_class();
			}

			if (count($supervisor) == 0 && count($nurse_manager) == 0){
				// ログイン不可ユーザーなので、ここに分岐されることは無い
			} else {
				if(count($supervisor) == 0){
					$class_division = $nurse_manager_class['class_division'];
				} else if(count($nurse_manager) == 0){
					$class_division = $supervisor_class['class_division'];
				} else {
					if ($nurse_manager_class['class_division'] > $supervisor_class['class_division']){
						$class_division = $supervisor_class['class_division'];
					} else {
						$class_division = $nurse_manager_class['class_division'];
					}
				}
			}
		} else {
			$class_division = 0;
		}
	} else {
		$class_division = 0;
	}

	return $class_division;
}

function get_analysis_data(&$mdb2, $login_emp_id) {

	global $log;

	require_once("cl/model/search/cl_level_analysis_model.php");
	$level_analysis_model = new cl_level_analysis_model(&$mdb2,$login_emp_id);
	$log->debug("レベル分析用DBA取得",__FILE__,__LINE__);

	$class_division = get_class_division(&$mdb2, $login_emp_id);

	$data = array();
	$row_count = 0;
	$column_count = 0;

	// 値配置用配列
	$data_place_row_1_key = array();
	$data_place_row_2_key = array();
	$data_place_column_1_key = array();
	$data_place_column_2_key = array();
	$data_place_row = array();
	$data_place_column = array();

	// グラフ用配列
	// 縦軸ラベル
	$vertical_options = array();
	// 横軸ラベル
	$horizontal_options = array();
	// 値
	$cells = array();
	// 行合計
	$vertical_sums = array();
	// 列合計
	$horizontal_sums = array();

	// 横軸ヘッダー行書き込み
	switch($_POST['analysis_items']) {

		case ITEM_UNIT_EMP_CNT_ID:
		case ITEM_GRADUATION_EMP_CNT_ID:

			if ($_POST['analysis_items'] == ITEM_UNIT_EMP_CNT_ID) {
				$set_value = '所属部署';
			} else if ($_POST['analysis_items'] == ITEM_GRADUATION_EMP_CNT_ID) {
				$set_value = '卒業学校';
			}
			$data[$row_count][$column_count] = set_cell_data($set_value,'','',true,false,'left');
			$column_count++;

			if ($_POST['level'] == '') {
				$data[$row_count][$column_count] = set_cell_data(get_level_disp(0),'','',true,false,'left');
				array_push($data_place_column_1_key,0);
				array_push($horizontal_options ,array('easy_code' => 0,'easy_name' => get_level_disp(0)));
				$column_count++;
				$data[$row_count][$column_count] = set_cell_data(get_level_disp(1),'','',true,false,'left');
				array_push($data_place_column_1_key,1);
				array_push($horizontal_options ,array('easy_code' => 1,'easy_name' => get_level_disp(1)));
				$column_count++;
				$data[$row_count][$column_count] = set_cell_data(get_level_disp(2),'','',true,false,'left');
				array_push($data_place_column_1_key,2);
				array_push($horizontal_options ,array('easy_code' => 2,'easy_name' => get_level_disp(2)));
				$column_count++;
				$data[$row_count][$column_count] = set_cell_data(get_level_disp(3),'','',true,false,'left');
				array_push($data_place_column_1_key,3);
				array_push($horizontal_options ,array('easy_code' => 3,'easy_name' => get_level_disp(3)));
				$column_count++;
				$data[$row_count][$column_count] = set_cell_data(get_level_disp(4),'','',true,false,'left');
				array_push($data_place_column_1_key,4);
				array_push($horizontal_options ,array('easy_code' => 4,'easy_name' => get_level_disp(4)));
				$column_count++;
				$data[$row_count][$column_count] = set_cell_data(get_level_disp(5),'','',true,false,'left');
				array_push($data_place_column_1_key,5);
				array_push($horizontal_options ,array('easy_code' => 5,'easy_name' => get_level_disp(5)));
			} else {
				array_push($data_place_column_1_key,$_POST['level']);
				$data[$row_count][$column_count] = set_cell_data(get_level_disp($_POST['level']),'','',true,false,'left');
				array_push($horizontal_options ,array('easy_code' => $_POST['level'],'easy_name' => get_level_disp($_POST['level'])));
			}

			break;

		case ITEM_UNIT_YEAR_EMP_CNT_ID:
		case ITEM_UNIT_RECOGNITION_YEAR_EMP_CNT_ID:
		case ITEM_YEAR_AVERAGE_EVALUATION_POINT_ID:

			$data[$row_count][$column_count] = set_cell_data('','','',true,false,'');
			$column_count++;
			$data[$row_count][$column_count] = set_cell_data('','','',true,false,'');
			$column_count++;

			if ($_POST['analysis_items'] == ITEM_UNIT_YEAR_EMP_CNT_ID) {
				$set_value_1 = '年度';
				$set_value_2 = '部署';
				$set_value_3 = 'レベル';
			} else if ($_POST['analysis_items'] == ITEM_UNIT_RECOGNITION_YEAR_EMP_CNT_ID) {
				$set_value_1 = '認定年度';
				$set_value_2 = '部署';
				$set_value_3 = 'レベル';
			} else if ($_POST['analysis_items'] == ITEM_YEAR_AVERAGE_EVALUATION_POINT_ID) {
				$set_value_1 = '評価年度';
				$set_value_2 = 'レベル';
				$set_value_3 = 'カテゴリ';
			}

			$year_cnt = $_POST['year_to'] - $_POST['year_from'] + 1;

			if ($year_cnt == 1) {
				$set_colspan = '';
			} else {
				$set_colspan = $year_cnt;
			}

			$data[$row_count][$column_count] = set_cell_data($set_value_1,$set_colspan,'',true,false,'left');

			$row_count++;
			$column_count = 0;

			$data[$row_count][$column_count] = set_cell_data($set_value_2,'','',true,false,'left');
			$column_count++;

			$data[$row_count][$column_count] = set_cell_data($set_value_3,'','',true,false,'left');
			$column_count++;

			for($i = 0; $i < $year_cnt; $i++) {
				$data[$row_count][$column_count] = set_cell_data($_POST['year_from'] + $i,'','',true,false,'right');
				array_push($data_place_column_1_key,$_POST['year_from'] + $i);
				array_push($horizontal_options ,array('easy_code' => $_POST['year_from'] + $i,'easy_name' => $_POST['year_from'] + $i));
				$column_count++;
			}

			break;

		case ITEM_RECOGNITION_EMP_CNT_ID:
		case ITEM_LICENSE_ELAPSED_YEAR_EMP_CNT_ID:
		case ITEM_TENURE_EMP_CNT_ID:
		case ITEM_ASSIGNMENT_YEAR_EMP_CNT_ID:
		case ITEM_GRADUATION_ELAPSED_YEAR_EMP_CNT_ID:

			$data[$row_count][$column_count] = set_cell_data('','','',true,false,'');
			$column_count++;
			$data[$row_count][$column_count] = set_cell_data('','','',true,false,'');
			$column_count++;

			if ($_POST['analysis_items'] == ITEM_RECOGNITION_EMP_CNT_ID) {
				$set_value_1 = '認定後経過年数';
				$set_value_2 = '部署';
				$set_value_3 = 'レベル';
			} else if ($_POST['analysis_items'] == ITEM_LICENSE_ELAPSED_YEAR_EMP_CNT_ID) {
				$set_value_1 = '免許取得経過年数';
				$set_value_2 = '部署';
				$set_value_3 = 'レベル';
			} else if ($_POST['analysis_items'] == ITEM_TENURE_EMP_CNT_ID) {
				$set_value_1 = '在職年数';
				$set_value_2 = '部署';
				$set_value_3 = 'レベル';
			} else if ($_POST['analysis_items'] == ITEM_ASSIGNMENT_YEAR_EMP_CNT_ID) {
				$set_value_1 = '配属年数';
				$set_value_2 = '部署';
				$set_value_3 = 'レベル';
			} else if ($_POST['analysis_items'] == ITEM_GRADUATION_ELAPSED_YEAR_EMP_CNT_ID) {
				$set_value_1 = '卒業後経過年数';
				$set_value_2 = '卒業学校';
				$set_value_3 = 'レベル';
			}

			$data[$row_count][$column_count] = set_cell_data($set_value_1, 8,'',true,false,'left');

			$row_count++;
			$column_count = 0;

			$data[$row_count][$column_count] = set_cell_data($set_value_2,'','',true,false,'left');
			$column_count++;
			$data[$row_count][$column_count] = set_cell_data($set_value_3,'','',true,false,'left');
			$column_count++;
			$data[$row_count][$column_count] = set_cell_data('１年未満','','',true,false,'left');
			array_push($horizontal_options ,array('easy_code' => 0,'easy_name' => '１年未満'));
			$column_count++;
			$data[$row_count][$column_count] = set_cell_data('１年以上２年未満','','',true,false,'left');
			array_push($horizontal_options ,array('easy_code' => 1,'easy_name' => '１年以上２年未満'));
			$column_count++;
			$data[$row_count][$column_count] = set_cell_data('２年以上３年未満','','',true,false,'left');
			array_push($horizontal_options ,array('easy_code' => 2,'easy_name' => '２年以上３年未満'));
			$column_count++;
			$data[$row_count][$column_count] = set_cell_data('３年以上４年未満','','',true,false,'left');
			array_push($horizontal_options ,array('easy_code' => 3,'easy_name' => '３年以上４年未満'));
			$column_count++;
			$data[$row_count][$column_count] = set_cell_data('４年以上５年未満','','',true,false,'left');
			array_push($horizontal_options ,array('easy_code' => 4,'easy_name' => '４年以上５年未満'));
			$column_count++;
			$data[$row_count][$column_count] = set_cell_data('５年以上８年未満','','',true,false,'left');
			array_push($horizontal_options ,array('easy_code' => 5,'easy_name' => '５年以上８年未満'));
			$column_count++;
			$data[$row_count][$column_count] = set_cell_data('８年以上１０年未満','','',true,false,'left');
			array_push($horizontal_options ,array('easy_code' => 8,'easy_name' => '８年以上１０年未満'));
			$column_count++;
			$data[$row_count][$column_count] = set_cell_data('１０年以上','','',true,false,'left');
			array_push($horizontal_options ,array('easy_code' => 10,'easy_name' => '１０年以上'));

			break;

		case ITEM_UNIT_AVERAGE_EVALUATION_POINT_ID:

			$data[0][0] = set_cell_data('','','',true,false,'');
			$data[1][0] = set_cell_data('部署','','',true,false,'left');

			$header_data_category = $level_analysis_model->get_category($_POST['level']);
			$column_count = 1;

			if (count($header_data_category) > 0) {
				$level_key = $header_data_category[0]['level'];
				$level_count = 1;
				$level_colspan = 0;
				for ($i = 0; $i < count($header_data_category); $i++) {
					array_push($horizontal_options ,array('easy_code' => $header_data_category[$i]['level'].'-'.$header_data_category[$i]['category_id'],'easy_name' => get_level_disp($header_data_category[$i]['level']).' '.$header_data_category[$i]['category_name']));
					if ($level_key == $header_data_category[$i]['level']) {
						$level_colspan++;
					} else {
						$level_colspan++;
						$data[1][$column_count] = set_cell_data('合計','','',true,false,'left');
						array_push($data_place_column_1_key,$level_key);
						array_push($data_place_column_2_key,'total');
						$column_count++;
						$data[0][$level_count] = set_cell_data(get_level_disp($level_key),$level_colspan,'',true,false,'left');
						$level_key = $header_data_category[$i]['level'];
						$level_count++;
						$level_colspan = 1;
					}
					$data[1][$column_count] = set_cell_data($header_data_category[$i]['category_name'],'','',true,false,'left');
					array_push($data_place_column_1_key,$level_key);
					array_push($data_place_column_2_key,$header_data_category[$i]['category_id']);
					$column_count++;
				}
				$level_colspan++;
				$data[1][$column_count] = set_cell_data('合計','','',true,false,'left');
				array_push($data_place_column_2_key,'total');
				$column_count++;
				$data[0][$level_count] = set_cell_data(get_level_disp($level_key),$level_colspan,'',true,false,'left');
				array_push($data_place_column_1_key,$level_key);
			}

			$row_count = 1;
			break;
	}

	$row_count++;
	$column_count = 0;

	$arr_class_condition = array();
	$arr_atrb_condition = array();
	$arr_dept_condition = array();
	$arr_room_condition = array();

	for($i = 0; $i < count($_POST['class_id']); $i++) {
		if (
			$_POST['class_id'][$i] != ''
			|| $_POST['atrb_id'][$i] != ''
			|| $_POST['dept_id'][$i] != ''
			|| $_POST['room_id'][$i] != ''
		) {
			array_push($arr_class_condition, $_POST['class_id'][$i]);
			array_push($arr_atrb_condition, $_POST['atrb_id'][$i]);
			array_push($arr_dept_condition, $_POST['dept_id'][$i]);
			array_push($arr_room_condition, $_POST['room_id'][$i]);
		} else {
			// 全て未選択の場合、全所属対象となる為、条件削除
			$arr_class_condition = array();
			$arr_atrb_condition = array();
			$arr_dept_condition = array();
			$arr_room_condition = array();
			break;
		}
	}

	// 縦軸ヘッダー書き込み
	switch($_POST['analysis_items']) {

		case ITEM_UNIT_EMP_CNT_ID:
		case ITEM_UNIT_AVERAGE_EVALUATION_POINT_ID:
			// 2012/07/11 Yamagawa upd(s)
			/*
			$header_data_atrb = $level_analysis_model->get_atrb(
				$arr_class_condition
				,$arr_atrb_condition
				,$class_division
			);

			for ($i = 0; $i <= count($header_data_atrb); $i++) {
				if ($i == count($header_data_atrb)) {
					$atrb_nm = '合計';
					$atrb_id = 'total';
				} else {
					$atrb_nm = $header_data_atrb[$i]['atrb_nm'];
					$atrb_id = $header_data_atrb[$i]['atrb_id'];
					array_push($vertical_options ,array('easy_code' => $atrb_id,'easy_name' => $atrb_nm));
				}
				$data[$row_count][0] = set_cell_data($atrb_nm,'','',false,false,'left');
				array_push($data_place_row_1_key,$atrb_id);
				$row_count++;
			}
			*/
			$header_data_dept = $level_analysis_model->get_dept(
				$arr_class_condition
				,$arr_atrb_condition
				,$arr_dept_condition
				,$class_division
			);

			for ($i = 0; $i <= count($header_data_dept); $i++) {
				if ($i == count($header_data_dept)) {
					$dept_nm = '合計';
					$dept_id = 'total';
				} else {
					$dept_nm = $header_data_dept[$i]['dept_nm'];
					$dept_id = $header_data_dept[$i]['dept_id'];
					array_push($vertical_options ,array('easy_code' => $dept_id,'easy_name' => $dept_nm));
				}
				$data[$row_count][0] = set_cell_data($dept_nm,'','',false,false,'left');
				array_push($data_place_row_1_key,$dept_id);
				$row_count++;
			}
			// 2012/07/11 Yamagawa upd(e)

			break;

		case ITEM_UNIT_YEAR_EMP_CNT_ID:
		case ITEM_UNIT_RECOGNITION_YEAR_EMP_CNT_ID:
		case ITEM_RECOGNITION_EMP_CNT_ID:
		case ITEM_LICENSE_ELAPSED_YEAR_EMP_CNT_ID:
		case ITEM_TENURE_EMP_CNT_ID:
		case ITEM_ASSIGNMENT_YEAR_EMP_CNT_ID:

			// 2012/07/11 Yamagawa upd(s)
			/*
			$header_data_atrb = $level_analysis_model->get_atrb(
				$arr_class_condition
				,$arr_atrb_condition
				,$class_division
			);

			if (in_array($_POST['analysis_items'],array(ITEM_UNIT_RECOGNITION_YEAR_EMP_CNT_ID,ITEM_RECOGNITION_EMP_CNT_ID))) {
				$bol_zero = false;
			} else {
				$bol_zero = true;
			}
			$header_data_level = get_level($_POST['level'],$bol_zero);

			for ($i = 0; $i <= count($header_data_atrb); $i++) {
				if ($i == count($header_data_atrb)) {
					$atrb_nm = '合計';
					$atrb_id = 'total';
				} else {
					$atrb_nm = $header_data_atrb[$i]['atrb_nm'];
					$atrb_id = $header_data_atrb[$i]['atrb_id'];
				}
				$data[$row_count][0] = set_cell_data($atrb_nm,'',count($header_data_level),false,false,'left');
				for ($j = 0; $j < count($header_data_level); $j++) {
					array_push($data_place_row_1_key,$atrb_id);
					array_push($data_place_row_2_key,$header_data_level[$j]['level']);
					if ($atrb_id != 'total') {
						array_push($vertical_options ,array('easy_code' => $atrb_id.'-'.$header_data_level[$j]['level'],'easy_name' => $atrb_nm.' '.$header_data_level[$j]['level_disp']));
					}
					if ($j == 0) {
						$data[$row_count][1] = set_cell_data($header_data_level[$j]['level_disp'],'','',false,false,'left');
					} else {
						$data[$row_count][0] = set_cell_data($header_data_level[$j]['level_disp'],'','',false,false,'left');
					}
					$row_count++;
				}
			}
			*/
			$header_data_dept = $level_analysis_model->get_dept(
				$arr_class_condition
				,$arr_atrb_condition
				,$arr_dept_condition
				,$class_division
			);

			if (in_array($_POST['analysis_items'],array(ITEM_UNIT_RECOGNITION_YEAR_EMP_CNT_ID,ITEM_RECOGNITION_EMP_CNT_ID))) {
				$bol_zero = false;
			} else {
				$bol_zero = true;
			}
			$header_data_level = get_level($_POST['level'],$bol_zero);

			for ($i = 0; $i <= count($header_data_dept); $i++) {
				if ($i == count($header_data_dept)) {
					$dept_nm = '合計';
					$dept_id = 'total';
				} else {
					$dept_nm = $header_data_dept[$i]['dept_nm'];
					$dept_id = $header_data_dept[$i]['dept_id'];
				}
				$data[$row_count][0] = set_cell_data($dept_nm,'',count($header_data_level),false,false,'left');
				for ($j = 0; $j < count($header_data_level); $j++) {
					array_push($data_place_row_1_key,$dept_id);
					array_push($data_place_row_2_key,$header_data_level[$j]['level']);
					if ($dept_id != 'total') {
						array_push($vertical_options ,array('easy_code' => $dept_id.'-'.$header_data_level[$j]['level'],'easy_name' => $dept_nm.' '.$header_data_level[$j]['level_disp']));
					}
					if ($j == 0) {
						$data[$row_count][1] = set_cell_data($header_data_level[$j]['level_disp'],'','',false,false,'left');
					} else {
						$data[$row_count][0] = set_cell_data($header_data_level[$j]['level_disp'],'','',false,false,'left');
					}
					$row_count++;
				}
			}
			// 2012/07/11 Yamagawa upd(e)

			break;

		case ITEM_YEAR_AVERAGE_EVALUATION_POINT_ID;
			$header_data_category = $level_analysis_model->get_category($_POST['level']);

			if (count($header_data_category) > 0) {
				$level_key = $header_data_category[0]['level'];
				$level_rowspan = 0;
				$level_start = $row_count;
				for ($i = 0; $i < count($header_data_category); $i++) {
					array_push($vertical_options ,array('easy_code' => $header_data_category[$i]['level'].'-'.$header_data_category[$i]['category_id'],'easy_name' => get_level_disp($header_data_category[$i]['level']).' '.$header_data_category[$i]['category_name']));
					if ($level_key == $header_data_category[$i]['level']) {
						$level_rowspan++;
						if ($i == 0) {
							$data[$row_count][1] = set_cell_data($header_data_category[$i]['category_name'],'','',false,false,'left');
						} else {
							$data[$row_count][0] = set_cell_data($header_data_category[$i]['category_name'],'','',false,false,'left');
						}
						array_push($data_place_row_1_key,$level_key);
						array_push($data_place_row_2_key,$header_data_category[$i]['category_id']);
					} else {
						$level_rowspan++;
						$data[$row_count][0] = set_cell_data('合計','','',false,false,'left');
						array_push($data_place_row_1_key,$level_key);
						array_push($data_place_row_2_key,'total');
						$row_count++;
						$data[$level_start][0] = set_cell_data(get_level_disp($level_key),'',$level_rowspan,false,false,'left');
						$level_key = $header_data_category[$i]['level'];
						$level_start = $row_count;
						$level_rowspan = 1;
						$data[$row_count][1] = set_cell_data($header_data_category[$i]['category_name'],'','',false,false,'left');
						array_push($data_place_row_1_key,$level_key);
						array_push($data_place_row_2_key,$header_data_category[$i]['category_id']);
					}
					$row_count++;
				}
				$level_rowspan++;
				$data[$row_count][0] = set_cell_data('合計','','',false,false,'left');
				array_push($data_place_row_1_key,$level_key);
				array_push($data_place_row_2_key,'total');
				$row_count++;
				$data[$level_start][0] = set_cell_data(get_level_disp($level_key),'',$level_rowspan,false,false,'left');
			}

			break;

		case ITEM_GRADUATION_EMP_CNT_ID:
			$header_data_school = $level_analysis_model->get_school();

			for ($i = 0; $i <= count($header_data_school); $i++) {
				array_push($vertical_options ,array('easy_code' => $header_data_school[$i]['school_name'],'easy_name' => $header_data_school[$i]['school_name']));
				if ($i == count($header_data_school)) {
					$school_name = '合計';
					$school_id = 'total';
				} else {
					$school_name = $header_data_school[$i]['school_name'];
					$school_id = $header_data_school[$i]['school_name'];
				}
				$data[$row_count][0] = set_cell_data($school_name,'','',false,false,'left');
				array_push($data_place_row_1_key,$school_id);
				$row_count++;
			}

			break;

		case ITEM_GRADUATION_ELAPSED_YEAR_EMP_CNT_ID:
			$header_data_school = $level_analysis_model->get_school();
			$header_data_level = get_level($_POST['level'],true);

			for ($i = 0; $i <= count($header_data_school); $i++) {
				if ($i == count($header_data_school)) {
					$school_name = '合計';
					$school_id = 'total';
				} else {
					$school_name = $header_data_school[$i]['school_name'];
					$school_id = $header_data_school[$i]['school_name'];
				}
				$data[$row_count][0] = set_cell_data($school_name,'',count($header_data_level),false,false,'left');
				for ($j = 0; $j < count($header_data_level); $j++) {
					if ($school_id != 'total') {
						array_push($vertical_options ,array('easy_code' => $header_data_school[$i]['school_name'].'-'.$header_data_level[$j]['level'],'easy_name' => $header_data_school[$i]['school_name'].' '.$header_data_level[$j]['level_disp']));
					}
					if ($j == 0) {
						$data[$row_count][1] = set_cell_data($header_data_level[$j]['level_disp'],'','',false,false,'left');
					} else {
						$data[$row_count][0] = set_cell_data($header_data_level[$j]['level_disp'],'','',false,false,'left');
					}
					array_push($data_place_row_1_key,$school_id);
					array_push($data_place_row_2_key,$header_data_level[$j]['level']);
					$row_count++;
				}
			}
			break;
	}

	// 集計結果取得
	switch($_POST['analysis_items']) {

		case ITEM_UNIT_EMP_CNT_ID:

			$analysis_data = $level_analysis_model->get_item_unit_emp_cnt(
				$_POST['job']
				, $_POST['level']
				, $arr_class_condition
				, $arr_atrb_condition
				, $arr_dept_condition
				, $arr_room_condition
				, $_POST['year_from']
				, $_POST['year_to']
				, $class_division
				, 'analysis'
				, ''
				, ''
				, ''
				, ''
			);
			break;

		case ITEM_UNIT_YEAR_EMP_CNT_ID:

			$analysis_data = get_item_unit_year_emp_cnt(
				&$mdb2
				, $login_emp_id
				, $_POST['job']
				, $_POST['level']
				, $arr_class_condition
				, $arr_atrb_condition
				, $arr_dept_condition
				, $arr_room_condition
				, $_POST['year_from']
				, $_POST['year_to']
				, $class_division
				, 'analysis'
				, ''
				, ''
				, ''
				, ''
			);
			break;

		case ITEM_UNIT_RECOGNITION_YEAR_EMP_CNT_ID:

			$analysis_data = $level_analysis_model->get_item_unit_recognition_year_emp_cnt(
				$_POST['job']
				, $_POST['level']
				, $arr_class_condition
				, $arr_atrb_condition
				, $arr_dept_condition
				, $arr_room_condition
				, $_POST['year_from']
				, $_POST['year_to']
				, $class_division
				, 'analysis'
				, ''
				, ''
				, ''
				, ''
			);
			break;

		case ITEM_RECOGNITION_EMP_CNT_ID:

			$analysis_data = $level_analysis_model->get_item_recognition_emp_cnt(
				$_POST['job']
				, $_POST['level']
				, $arr_class_condition
				, $arr_atrb_condition
				, $arr_dept_condition
				, $arr_room_condition
				, $_POST['year_from']
				, $_POST['year_to']
				, $class_division
				, 'analysis'
				, ''
				, ''
				, ''
				, ''
			);
			break;

		case ITEM_UNIT_AVERAGE_EVALUATION_POINT_ID:

			$analysis_data = $level_analysis_model->get_item_unit_average_evaluation_point(
				$_POST['job']
				, $_POST['level']
				, $arr_class_condition
				, $arr_atrb_condition
				, $arr_dept_condition
				, $arr_room_condition
				, $_POST['year_from']
				, $_POST['year_to']
				, $class_division
				, 'analysis'
				, ''
				, ''
				, ''
				, ''
			);
			break;

		case ITEM_YEAR_AVERAGE_EVALUATION_POINT_ID:

			$analysis_data = $level_analysis_model->get_item_year_average_evaluation_point(
				$_POST['job']
				, $_POST['level']
				, $arr_class_condition
				, $arr_atrb_condition
				, $arr_dept_condition
				, $arr_room_condition
				, $_POST['year_from']
				, $_POST['year_to']
				, $class_division
				, 'analysis'
				, ''
				, ''
				, ''
				, ''
			);
			break;

		case ITEM_LICENSE_ELAPSED_YEAR_EMP_CNT_ID:

			$analysis_data = $level_analysis_model->get_item_license_elapsed_year_emp_cnt(
				$_POST['job']
				, $_POST['level']
				, $arr_class_condition
				, $arr_atrb_condition
				, $arr_dept_condition
				, $arr_room_condition
				, $_POST['year_from']
				, $_POST['year_to']
				, $class_division
				, 'analysis'
				, ''
				, ''
				, ''
				, ''
			);
			break;

		case ITEM_TENURE_EMP_CNT_ID:

			$analysis_data = $level_analysis_model->get_item_tenure_emp_cnt(
				$_POST['job']
				, $_POST['level']
				, $arr_class_condition
				, $arr_atrb_condition
				, $arr_dept_condition
				, $arr_room_condition
				, $_POST['year_from']
				, $_POST['year_to']
				, $class_division
				, 'analysis'
				, ''
				, ''
				, ''
				, ''
			);
			break;

		case ITEM_ASSIGNMENT_YEAR_EMP_CNT_ID:

			$analysis_data = $level_analysis_model->get_item_assignment_year_emp_cnt(
				$_POST['job']
				, $_POST['level']
				, $arr_class_condition
				, $arr_atrb_condition
				, $arr_dept_condition
				, $arr_room_condition
				, $_POST['year_from']
				, $_POST['year_to']
				, $class_division
				, 'analysis'
				, ''
				, ''
				, ''
				, ''
			);
			break;

		case ITEM_GRADUATION_EMP_CNT_ID:

			$analysis_data = $level_analysis_model->get_item_graduation_emp_cnt(
				$_POST['job']
				, $_POST['level']
				, $arr_class_condition
				, $arr_atrb_condition
				, $arr_dept_condition
				, $arr_room_condition
				, $_POST['year_from']
				, $_POST['year_to']
				, $class_division
				, 'analysis'
				, ''
				, ''
				, ''
				, ''
			);
			break;

		case ITEM_GRADUATION_ELAPSED_YEAR_EMP_CNT_ID:

			$analysis_data = $level_analysis_model->get_item_graduation_elapsed_year_emp_cnt(
				$_POST['job']
				, $_POST['level']
				, $arr_class_condition
				, $arr_atrb_condition
				, $arr_dept_condition
				, $arr_room_condition
				, $_POST['year_from']
				, $_POST['year_to']
				, $class_division
				, 'analysis'
				, ''
				, ''
				, ''
				, ''
			);
			break;

	}

	// 集計結果書込
	switch($_POST['analysis_items']) {

		case ITEM_UNIT_EMP_CNT_ID:
		case ITEM_UNIT_YEAR_EMP_CNT_ID:
		case ITEM_UNIT_RECOGNITION_YEAR_EMP_CNT_ID:
		case ITEM_UNIT_AVERAGE_EVALUATION_POINT_ID:
		case ITEM_YEAR_AVERAGE_EVALUATION_POINT_ID:
		case ITEM_GRADUATION_EMP_CNT_ID:

			if ($_POST['analysis_items'] == ITEM_UNIT_EMP_CNT_ID) {
				$start_row = 1;
				$start_column = 1;
			} else if ($_POST['analysis_items'] == ITEM_UNIT_YEAR_EMP_CNT_ID) {
				$start_row = 2;
				$start_column = 2;
			} else if ($_POST['analysis_items'] == ITEM_UNIT_RECOGNITION_YEAR_EMP_CNT_ID) {
				$start_row = 2;
				$start_column = 2;
			} else if ($_POST['analysis_items'] == ITEM_UNIT_AVERAGE_EVALUATION_POINT_ID) {
				$start_row = 2;
				$start_column = 1;
			} else if ($_POST['analysis_items'] == ITEM_YEAR_AVERAGE_EVALUATION_POINT_ID) {
				$start_row = 2;
				$start_column = 2;
			} else if ($_POST['analysis_items'] == ITEM_GRADUATION_EMP_CNT_ID) {
				$start_row = 1;
				$start_column = 1;
			}

			$break_key = '';
			for ($i = 0; $i < count($data_place_row_1_key); $i++) {
				if (
					count($data_place_row_2_key) > 0
					&& $break_key == $data_place_row_1_key[$i]
				) {
					$start_column_wk = $start_column - 1;
				} else {
					$start_column_wk = $start_column;
					$break_key = $data_place_row_1_key[$i];
				}
				for ($j = 0; $j < count($data_place_column_1_key); $j++) {
					$row_index = $start_row + $i;
					$column_index = $start_column_wk + $j;
					if (
						count($data_place_row_2_key) > 0
						&& count($data_place_column_2_key) > 0
					) {

						$row_1_key = $data_place_row_1_key[$i];
						$row_2_key = $data_place_row_2_key[$i];
						$column_1_key = $data_place_column_1_key[$j];
						$column_2_key = $data_place_column_2_key[$j];

					} else if (count($data_place_row_2_key) > 0) {

						$row_1_key = $data_place_row_1_key[$i];
						$row_2_key = $data_place_row_2_key[$i];
						$column_1_key = $data_place_column_1_key[$j];
						$column_2_key = 0;

					} else if (count($data_place_column_2_key) > 0) {

						$row_1_key = $data_place_row_1_key[$i];
						$row_2_key = 0;
						$column_1_key = $data_place_column_1_key[$j];
						$column_2_key = $data_place_column_2_key[$j];

					} else {

						$row_1_key = $data_place_row_1_key[$i];
						$row_2_key = 0;
						$column_1_key = $data_place_column_1_key[$j];
						$column_2_key = 0;

					}

					$data_place_row
						[$row_1_key]
						[$row_2_key]
						[$column_1_key]
						[$column_2_key]
					 = $row_index;

					$data_place_column
						[$row_1_key]
						[$row_2_key]
						[$column_1_key]
						[$column_2_key]
					 = $column_index;

					if (
						in_array(
							$_POST['analysis_items']
							,array(
								ITEM_UNIT_AVERAGE_EVALUATION_POINT_ID
								,ITEM_YEAR_AVERAGE_EVALUATION_POINT_ID
							)
						)
					) {
						$default_value = '';
					} else {
						$default_value = 0;
					}

					$data
						[$row_index]
						[$column_index]
					 = set_cell_data(
					 	$default_value
					 	,''
					 	,''
					 	,false
					 	,true
					 	,'right'
					 	,$row_1_key
					 	,$row_2_key
					 	,$column_1_key
					 	,$column_2_key
					 );
				}
			}

			// キー項目・合計算出フラグの設定
			if ($_POST['analysis_items'] == ITEM_UNIT_EMP_CNT_ID) {
				// 2012/07/11 Yamagawa upd(s)
				//$row_1_key_name = 'emp_attribute';
				$row_1_key_name = 'emp_dept';
				// 2012/07/11 Yamagawa upd(e)
				$column_1_key_name = 'now_level';
				$value_name = 'emp_cnt';
				$row_1_total = true;
				$row_2_total = false;
				$column_1_total = false;
				$column_2_total = false;
			} else if ($_POST['analysis_items'] == ITEM_UNIT_YEAR_EMP_CNT_ID) {
				// 2012/07/11 Yamagawa upd(s)
				//$row_1_key_name = 'atrb_id';
				$row_1_key_name = 'dept_id';
				// 2012/07/11 Yamagawa upd(e)
				$row_2_key_name = 'level';
				$column_1_key_name = 'year';
				$value_name = 'count';
				$row_1_total = true;
				$row_2_total = false;
				$column_1_total = false;
				$column_2_total = false;
			} else if ($_POST['analysis_items'] == ITEM_UNIT_RECOGNITION_YEAR_EMP_CNT_ID) {
				// 2012/07/11 Yamagawa upd(s)
				//$row_1_key_name = 'emp_attribute';
				$row_1_key_name = 'emp_dept';
				// 2012/07/11 Yamagawa upd(e)
				$row_2_key_name = 'level';
				$column_1_key_name = 'get_year';
				$value_name = 'cnt';
				$row_1_total = true;
				$row_2_total = false;
				$column_1_total = false;
				$column_2_total = false;
			} else if ($_POST['analysis_items'] == ITEM_UNIT_AVERAGE_EVALUATION_POINT_ID) {
				// 2012/07/11 Yamagawa upd(s)
				//$row_1_key_name = 'emp_attribute';
				$row_1_key_name = 'emp_dept';
				// 2012/07/11 Yamagawa upd(e)
				$column_1_key_name = 'level';
				$column_2_key_name = 'category_id';
				// 2012/08/08 Yamagawa add(s)
				$value_name = 'value';
				// 2012/08/08 Yamagawa add(e)
				$row_1_total = false;
				$row_2_total = false;
				$column_1_total = false;
				$column_2_total = false;
			} else if ($_POST['analysis_items'] == ITEM_YEAR_AVERAGE_EVALUATION_POINT_ID) {
				$row_1_key_name = 'level';
				$row_2_key_name = 'category_id';
				$column_1_key_name = 'eval_year';
				// 2012/08/08 Yamagawa add(s)
				$value_name = 'value';
				// 2012/08/08 Yamagawa add(e)
				$row_1_total = false;
				$row_2_total = false;
				$column_1_total = false;
				$column_2_total = false;
			} else if ($_POST['analysis_items'] == ITEM_GRADUATION_EMP_CNT_ID) {
				$row_1_key_name = 'school_name';
				$column_1_key_name = 'now_level';
				$value_name = 'emp_cnt';
				$row_1_total = true;
				$row_2_total = false;
				$column_1_total = false;
				$column_2_total = false;
			}

			for ($i = 0; $i < count($analysis_data); $i++) {
				$row_1_key_index = $analysis_data[$i][$row_1_key_name];
				$column_1_key_index = $analysis_data[$i][$column_1_key_name];
				$set_value = $analysis_data[$i][$value_name];

				if ($row_2_key_name == '') {
					$row_2_key_index = 0;
				} else {
					$row_2_key_index = $analysis_data[$i][$row_2_key_name];
				}

				if ($column_2_key_name == '') {
					$column_2_key_index = 0;
				} else {
					$column_2_key_index = $analysis_data[$i][$column_2_key_name];
				}

				$data
					[$data_place_row
						[$row_1_key_index]
						[$row_2_key_index]
						[$column_1_key_index]
						[$column_2_key_index]
					]
					[$data_place_column
						[$row_1_key_index]
						[$row_2_key_index]
						[$column_1_key_index]
						[$column_2_key_index]
					] = set_cell_data(
						$set_value
						,''
						,''
						,false
						,true
						,'right'
						,$row_1_key_index
						,$row_2_key_index
						,$column_1_key_index
						,$column_2_key_index
					);

				if ($row_1_total) {
					$data
						[$data_place_row
							['total']
							[$row_2_key_index]
							[$column_1_key_index]
							[$column_2_key_index]
						]
						[$data_place_column
							['total']
							[$row_2_key_index]
							[$column_1_key_index]
							[$column_2_key_index]
						]
						['value'] += $set_value;
				}

				if ($row_2_total) {
					$data
						[$data_place_row
							[$row_1_key_index]
							['total']
							[$column_1_key_index]
							[$column_2_key_index]
						]
						[$data_place_column
							[$row_1_key_index]
							['total']
							[$column_1_key_index]
							[$column_2_key_index]
						]
						['value'] += $set_value;
				}

				if ($column_1_total) {
					$data
						[$data_place_row
							[$row_1_key_index]
							[$row_2_key_index]
							['total']
							[$column_2_key_index]
						]
						[$data_place_column
							[$row_1_key_index]
							[$row_2_key_index]
							['total']
							[$column_2_key_index]
						]
						['value'] += $set_value;
				}

				if ($column_2_total) {
					$data
						[$data_place_row
							[$row_1_key_index]
							[$row_2_key_index]
							[$column_1_key_index]
							['total']
						]
						[$data_place_column
							[$row_1_key_index]
							[$row_2_key_index]
							[$column_1_key_index]
							['total']
						]
						['value'] += $set_value;
				}

				if ($row_1_total && $row_2_total) {
					$data
						[$data_place_row
							['total']
							['total']
							[$column_1_key_index]
							[$column_2_key_index]
						]
						[$data_place_column
							['total']
							['total']
							[$column_1_key_index]
							[$column_2_key_index]
						]
						['value'] += $set_value;
				}

				if ($row_1_total && $column_1_total) {
					$data
						[$data_place_row
							['total']
							[$row_2_key_index]
							['total']
							[$column_2_key_index]
						]
						[$data_place_column
							['total']
							[$row_2_key_index]
							['total']
							[$column_2_key_index]
						]
						['value'] += $set_value;
				}

				if ($row_1_total && $column_2_total) {
					$data
						[$data_place_row
							['total']
							[$row_2_key_index]
							[$column_1_key_index]
							['total']
						]
						[$data_place_column
							['total']
							[$row_2_key_index]
							[$column_1_key_index]
							['total']
						]
						['value'] += $set_value;
				}

				if ($row_2_total && $column_1_total) {
					$data
						[$data_place_row
							[$row_1_key_index]
							['total']
							['total']
							[$column_2_key_index]
						]
						[$data_place_column
							[$row_1_key_index]
							['total']
							['total']
							[$column_2_key_index]
						]
						['value'] += $set_value;
				}

				if ($row_2_total && $column_2_total) {
					$data
						[$data_place_row
							[$row_1_key_index]
							['total']
							[$column_1_key_index]
							['total']
						]
						[$data_place_column
							[$row_1_key_index]
							['total']
							[$column_1_key_index]
							['total']
						]
						['value'] += $set_value;
				}

				if ($column_1_total && $column_2_total) {
					$data
						[$data_place_row
							[$row_1_key_index]
							[$row_2_key_index]
							['total']
							['total']
						]
						[$data_place_column
							[$row_1_key_index]
							[$row_2_key_index]
							['total']
							['total']
						]
						['value'] += $set_value;
				}

				if ($row_1_total && $row_2_total && $column_1_total) {
					$data
						[$data_place_row
							['total']
							['total']
							['total']
							[$column_2_key_index]
						]
						[$data_place_column
							['total']
							['total']
							['total']
							[$column_2_key_index]
						]
						['value'] += $set_value;
				}

				if ($row_1_total && $row_2_total && $column_2_total) {
					$data
						[$data_place_row
							['total']
							['total']
							[$column_1_key_index]
							['total']
						]
						[$data_place_column
							['total']
							['total']
							[$column_1_key_index]
							['total']
						]
						['value'] += $set_value;
				}

				if ($row_1_total && $column_1_total && $column_2_total) {
					$data
						[$data_place_row
							['total']
							[$row_2_key_index]
							['total']
							['total']
						]
						[$data_place_column
							['total']
							[$row_2_key_index]
							['total']
							['total']
						]
						['value'] += $set_value;
				}

				if ($row_2_total && $column_1_total && $column_2_total) {
					$data
						[$data_place_row
							[$row_1_key_index]
							['total']
							['total']
							['total']
						]
						[$data_place_column
							[$row_1_key_index]
							['total']
							['total']
							['total']
						]
						['value'] += $set_value;
				}

				if ($row_1_total && $row_2_total && $column_1_total && $column_2_total) {
					$data
						[$data_place_row
							['total']
							['total']
							['total']
							['total']
						]
						[$data_place_column
							['total']
							['total']
							['total']
							['total']
						]
						['value'] += $set_value;
				}

			}
			break;

		case ITEM_RECOGNITION_EMP_CNT_ID:
		case ITEM_LICENSE_ELAPSED_YEAR_EMP_CNT_ID:
		case ITEM_TENURE_EMP_CNT_ID:
		case ITEM_ASSIGNMENT_YEAR_EMP_CNT_ID:
		case ITEM_GRADUATION_ELAPSED_YEAR_EMP_CNT_ID:

			if (in_array($_POST['analysis_items'],array(ITEM_UNIT_RECOGNITION_YEAR_EMP_CNT_ID,ITEM_RECOGNITION_EMP_CNT_ID))) {
				$bol_zero = false;
			} else {
				$bol_zero = true;
			}

			for ($i = 0; $i < count($data_place_row_1_key); $i++) {
				$row_index = 2 + $i;
				$row_1_key = $data_place_row_1_key[$i];
				$row_2_key = $data_place_row_2_key[$i];
				$data_place_row
					[$data_place_row_1_key
						[$i]
					]
					[$data_place_row_2_key
						[$i]
					] = $row_index;

				for ($j = 0; $j < 8; $j++) {
					if (
						$_POST['level'] == ''
						&& (
							(
								$bol_zero
								&& $data_place_row_2_key[$i] != 0
							)
							|| (
								!$bol_zero
								&& $data_place_row_2_key[$i] != 1
							)
						)
					) {
						$column_index = 1 + $j;
					} else {
						$column_index = 2 + $j;
					}
					$data
						[$row_index]
						[$column_index]
					 = set_cell_data(
					 	0
					 	,''
					 	,''
					 	,false
					 	,true
					 	,'right'
					 	,$row_1_key
					 	,$row_2_key
					 	,$j
					 	,'');
				}
			}

			if ($_POST['analysis_items'] == ITEM_RECOGNITION_EMP_CNT_ID) {
				// 2012/07/11 Yamagawa upd(s)
				//$row_1_key_name = 'emp_attribute';
				$row_1_key_name = 'emp_dept';
				// 2012/07/11 Yamagawa upd(e)
				$row_2_key_name = 'now_level';
			} else if ($_POST['analysis_items'] == ITEM_LICENSE_ELAPSED_YEAR_EMP_CNT_ID) {
				// 2012/07/11 Yamagawa upd(s)
				//$row_1_key_name = 'emp_attribute';
				$row_1_key_name = 'emp_dept';
				// 2012/07/11 Yamagawa upd(e)
				$row_2_key_name = 'now_level';
			} else if ($_POST['analysis_items'] == ITEM_TENURE_EMP_CNT_ID) {
				// 2012/07/11 Yamagawa upd(s)
				//$row_1_key_name = 'emp_attribute';
				$row_1_key_name = 'emp_dept';
				// 2012/07/11 Yamagawa upd(e)
				$row_2_key_name = 'now_level';
			} else if ($_POST['analysis_items'] == ITEM_ASSIGNMENT_YEAR_EMP_CNT_ID) {
				// 2012/07/11 Yamagawa upd(s)
				//$row_1_key_name = 'emp_attribute';
				$row_1_key_name = 'emp_dept';
				// 2012/07/11 Yamagawa upd(e)
				$row_2_key_name = 'now_level';
			} else if ($_POST['analysis_items'] == ITEM_GRADUATION_ELAPSED_YEAR_EMP_CNT_ID) {
				$row_1_key_name = 'school_name';
				$row_2_key_name = 'now_level';
			}

			for($i = 0; $i < count($analysis_data); $i++) {

				$row_1_key_index = $analysis_data[$i][$row_1_key_name];
				$row_2_key_index = $analysis_data[$i][$row_2_key_name];

				if (
					$_POST['level'] == ''
					&& (
						(
							$bol_zero
							&& $row_2_key_index != 0
						)
						|| (
							!$bol_zero
							&& $row_2_key_index != 1
						)
					)
				) {
					$column_count = 1;
				} else {
					$column_count = 2;
				}

				$data
					[$data_place_row
						[$row_1_key_index]
						[$row_2_key_index]
					]
					[$column_count]
				 = set_cell_data(
				 	$analysis_data
				 		[$i]
				 		['year_0_cnt']
				 	,''
				 	,''
				 	,false
				 	,true
				 	,'right'
				 	,$row_1_key_index
				 	,$row_2_key_index
				 	,0
				 	,'');

				$data
					[$data_place_row
						['total']
						[$row_2_key_index]
					]
					[$column_count]
					['value'] += $analysis_data[$i]['year_0_cnt'];
				$column_count++;

				$data
					[$data_place_row
						[$row_1_key_index]
						[$row_2_key_index]
					]
					[$column_count]
				 = set_cell_data(
				 	$analysis_data
				 		[$i]
				 		['year_1_cnt']
				 	,''
				 	,''
				 	,false
				 	,true
				 	,'right'
				 	,$row_1_key_index
				 	,$row_2_key_index
				 	,1
				 	,'');

				$data
					[$data_place_row
						['total']
						[$row_2_key_index]
					]
					[$column_count]
					['value'] += $analysis_data[$i]['year_1_cnt'];
				$column_count++;

				$data
					[$data_place_row
						[$row_1_key_index]
						[$row_2_key_index]
					]
					[$column_count]
				 = set_cell_data(
				 	$analysis_data
				 		[$i]
				 		['year_2_cnt']
				 	,''
				 	,''
				 	,false
				 	,true
				 	,'right'
				 	,$row_1_key_index
				 	,$row_2_key_index
				 	,2
				 	,'');

				$data
					[$data_place_row
						['total']
						[$row_2_key_index]
					]
					[$column_count]
					['value'] += $analysis_data[$i]['year_2_cnt'];
				$column_count++;

				$data
					[$data_place_row
						[$row_1_key_index]
						[$row_2_key_index]
					]
					[$column_count]
				 = set_cell_data(
				 	$analysis_data
				 		[$i]
				 		['year_3_cnt']
				 	,''
				 	,''
				 	,false
				 	,true
				 	,'right'
				 	,$row_1_key_index
				 	,$row_2_key_index
				 	,3
				 	,'');

				$data
					[$data_place_row
						['total']
						[$row_2_key_index]
					]
					[$column_count]
					['value'] += $analysis_data[$i]['year_3_cnt'];
				$column_count++;

				$data
					[$data_place_row
						[$row_1_key_index]
						[$row_2_key_index]
					]
					[$column_count]
				 = set_cell_data(
				 	$analysis_data
				 		[$i]
				 		['year_4_cnt']
				 	,''
				 	,''
				 	,false
				 	,true
				 	,'right'
				 	,$row_1_key_index
				 	,$row_2_key_index
				 	,4
				 	,'');

				$data
					[$data_place_row
						['total']
						[$row_2_key_index]
					]
					[$column_count]
					['value'] += $analysis_data[$i]['year_4_cnt'];
				$column_count++;

				$data
					[$data_place_row
						[$row_1_key_index]
						[$row_2_key_index]
					]
					[$column_count]
				 = set_cell_data(
				 	$analysis_data
				 		[$i]
				 		['year_5_7_cnt']
				 	,''
				 	,''
				 	,false
				 	,true
				 	,'right'
				 	,$row_1_key_index
				 	,$row_2_key_index
				 	,5
				 	,'');

				$data
					[$data_place_row
						['total']
						[$row_2_key_index]
					]
					[$column_count]
					['value'] += $analysis_data[$i]['year_5_7_cnt'];
				$column_count++;

				$data
					[$data_place_row
						[$row_1_key_index]
						[$row_2_key_index]
					]
					[$column_count]
				 = set_cell_data(
				 	$analysis_data
				 		[$i]
				 		['year_8_9_cnt']
				 	,''
				 	,''
				 	,false
				 	,true
				 	,'right'
				 	,$row_1_key_index
				 	,$row_2_key_index
				 	,6
				 	,'');

				$data
					[$data_place_row
						['total']
						[$row_2_key_index]
					]
					[$column_count]
					['value'] += $analysis_data[$i]['year_8_9_cnt'];
				$column_count++;

				$data
					[$data_place_row
						[$row_1_key_index]
						[$row_2_key_index]
					]
					[$column_count]
				 = set_cell_data(
				 	$analysis_data
				 		[$i]
				 		['year_10_cnt']
				 	,''
				 	,''
				 	,false
				 	,true
				 	,'right'
				 	,$row_1_key_index
				 	,$row_2_key_index
				 	,7
				 	,'');

				$data
					[$data_place_row
						['total']
						[$row_2_key_index]
					]
					[$column_count]
					['value'] += $analysis_data[$i]['year_10_cnt'];
				$column_count++;

			}

			break;

	}

	for ($row_index = 0; $row_index < count($data); $row_index++) {

		$row = array();
		$vertical_sum = 0;
		$horizontal_index = 0;
		$bol_rowadd = false;

		for ($column_index = 0; $column_index < count($data[$row_index]); $column_index++) {

			if (
				!$data[$row_index][$column_index]['bolemplist']
				|| $data[$row_index][$column_index]['row_1_key'] === 'total'
				|| $data[$row_index][$column_index]['row_2_key'] === 'total'
				|| $data[$row_index][$column_index]['column_1_key'] === 'total'
				|| $data[$row_index][$column_index]['column_2_key'] === 'total'
			) {
			} else {
				array_push($row,$data[$row_index][$column_index]['value']);
				$vertical_sum += $data[$row_index][$column_index]['value'];
				if ($horizontal_sums[$horizontal_index]) {
					$horizontal_sums[$horizontal_index] +=  $data[$row_index][$column_index]['value'];
				} else {
					$horizontal_sums[$horizontal_index] =  $data[$row_index][$column_index]['value'];
				}
				$horizontal_index++;
				$bol_rowadd = true;
			}

		}

		if ($bol_rowadd) {
			array_push($cells, $row);
			array_push($vertical_sums, $vertical_sum);
		}

	}

	$_SESSION['stats_result_info']['report_stats_data']['vertical_options'] = $vertical_options;
	$_SESSION['stats_result_info']['report_stats_data']['horizontal_options'] = $horizontal_options;
	$_SESSION['stats_result_info']['report_stats_data']['stats_data_list']['cells'] = $cells;
	$_SESSION['stats_result_info']['report_stats_data']['stats_data_list']['vertical_sums'] = $vertical_sums;
	$_SESSION['stats_result_info']['report_stats_data']['stats_data_list']['horizontal_sums'] = $horizontal_sums;

	return $data;
}

function get_emp_list(&$mdb2, $login_emp_id) {

	global $log;

	require_once("cl/model/search/cl_level_analysis_model.php");
	$level_analysis_model = new cl_level_analysis_model(&$mdb2,$login_emp_id);
	$log->debug("レベル分析用DBA取得",__FILE__,__LINE__);

	$class_division = get_class_division(&$mdb2, $login_emp_id);

	$class_id = explode(',' ,$_POST['class_id']);
	$atrb_id = explode(',' ,$_POST['atrb_id']);
	$dept_id = explode(',' ,$_POST['dept_id']);
	$room_id = explode(',' ,$_POST['room_id']);

	$arr_class_condition = array();
	$arr_atrb_condition = array();
	$arr_dept_condition = array();
	$arr_room_condition = array();

	for($i = 0; $i < $_POST['affiliation_row_count']; $i++) {
		if (
			$class_id[$i] != ''
			|| $atrb_id[$i] != ''
			|| $dept_id[$i] != ''
			|| $room_id[$i] != ''
		) {
			array_push($arr_class_condition, $class_id[$i]);
			array_push($arr_atrb_condition, $atrb_id[$i]);
			array_push($arr_dept_condition, $dept_id[$i]);
			array_push($arr_room_condition, $room_id[$i]);
		} else {
			// 全て未選択の場合、全所属対象となる為、条件削除
			$arr_class_condition = array();
			$arr_atrb_condition = array();
			$arr_dept_condition = array();
			$arr_room_condition = array();
			break;
		}
	}

	// 対象職員取得
	switch ($_REQUEST['analysis_items']) {

		case ITEM_UNIT_EMP_CNT_ID:

			$data_emp_list = $level_analysis_model->get_item_unit_emp_cnt(
				$_POST['job']
				, $_POST['level']
				, $arr_class_condition
				, $arr_atrb_condition
				, $arr_dept_condition
				, $arr_room_condition
				, $_POST['year_from']
				, $_POST['year_to']
				, $class_division
				, 'emp_list'
				, $_POST['row_1_key']
				, $_POST['row_2_key']
				, $_POST['column_1_key']
				, $_POST['column_2_key']
			);
			break;

		case ITEM_UNIT_YEAR_EMP_CNT_ID:

			$data_emp_list = get_item_unit_year_emp_cnt(
				&$mdb2
				, $login_emp_id
				, $_POST['job']
				, $_POST['level']
				, $arr_class_condition
				, $arr_atrb_condition
				, $arr_dept_condition
				, $arr_room_condition
				, $_POST['year_from']
				, $_POST['year_to']
				, $class_division
				, 'emp_list'
				, $_POST['row_1_key']
				, $_POST['row_2_key']
				, $_POST['column_1_key']
				, $_POST['column_2_key']
			);
			break;

		case ITEM_UNIT_RECOGNITION_YEAR_EMP_CNT_ID:

			$data_emp_list = $level_analysis_model->get_item_unit_recognition_year_emp_cnt(
				$_POST['job']
				, $_POST['level']
				, $arr_class_condition
				, $arr_atrb_condition
				, $arr_dept_condition
				, $arr_room_condition
				, $_POST['year_from']
				, $_POST['year_to']
				, $class_division
				, 'emp_list'
				, $_POST['row_1_key']
				, $_POST['row_2_key']
				, $_POST['column_1_key']
				, $_POST['column_2_key']
			);
			break;

		case ITEM_RECOGNITION_EMP_CNT_ID:

			$data_emp_list = $level_analysis_model->get_item_recognition_emp_cnt(
				$_POST['job']
				, $_POST['level']
				, $arr_class_condition
				, $arr_atrb_condition
				, $arr_dept_condition
				, $arr_room_condition
				, $_POST['year_from']
				, $_POST['year_to']
				, $class_division
				, 'emp_list'
				, $_POST['row_1_key']
				, $_POST['row_2_key']
				, $_POST['column_1_key']
				, $_POST['column_2_key']
			);
			break;

		case ITEM_UNIT_AVERAGE_EVALUATION_POINT_ID:

			$data_emp_list = $level_analysis_model->get_item_unit_average_evaluation_point(
				$_POST['job']
				, $_POST['level']
				, $arr_class_condition
				, $arr_atrb_condition
				, $arr_dept_condition
				, $arr_room_condition
				, $_POST['year_from']
				, $_POST['year_to']
				, $class_division
				, 'emp_list'
				, $_POST['row_1_key']
				, $_POST['row_2_key']
				, $_POST['column_1_key']
				, $_POST['column_2_key']
			);
			break;

		case ITEM_YEAR_AVERAGE_EVALUATION_POINT_ID:

			$data_emp_list = $level_analysis_model->get_item_year_average_evaluation_point(
				$_POST['job']
				, $_POST['level']
				, $arr_class_condition
				, $arr_atrb_condition
				, $arr_dept_condition
				, $arr_room_condition
				, $_POST['year_from']
				, $_POST['year_to']
				, $class_division
				, 'emp_list'
				, $_POST['row_1_key']
				, $_POST['row_2_key']
				, $_POST['column_1_key']
				, $_POST['column_2_key']
			);
			break;

		case ITEM_LICENSE_ELAPSED_YEAR_EMP_CNT_ID:

			$data_emp_list = $level_analysis_model->get_item_license_elapsed_year_emp_cnt(
				$_POST['job']
				, $_POST['level']
				, $arr_class_condition
				, $arr_atrb_condition
				, $arr_dept_condition
				, $arr_room_condition
				, $_POST['year_from']
				, $_POST['year_to']
				, $class_division
				, 'emp_list'
				, $_POST['row_1_key']
				, $_POST['row_2_key']
				, $_POST['column_1_key']
				, $_POST['column_2_key']
			);
			break;

		case ITEM_TENURE_EMP_CNT_ID:

			$data_emp_list = $level_analysis_model->get_item_tenure_emp_cnt(
				$_POST['job']
				, $_POST['level']
				, $arr_class_condition
				, $arr_atrb_condition
				, $arr_dept_condition
				, $arr_room_condition
				, $_POST['year_from']
				, $_POST['year_to']
				, $class_division
				, 'emp_list'
				, $_POST['row_1_key']
				, $_POST['row_2_key']
				, $_POST['column_1_key']
				, $_POST['column_2_key']
			);
			break;

		case ITEM_ASSIGNMENT_YEAR_EMP_CNT_ID:

			$data_emp_list = $level_analysis_model->get_item_assignment_year_emp_cnt(
				$_POST['job']
				, $_POST['level']
				, $arr_class_condition
				, $arr_atrb_condition
				, $arr_dept_condition
				, $arr_room_condition
				, $_POST['year_from']
				, $_POST['year_to']
				, $class_division
				, 'emp_list'
				, $_POST['row_1_key']
				, $_POST['row_2_key']
				, $_POST['column_1_key']
				, $_POST['column_2_key']
			);
			break;

		case ITEM_GRADUATION_EMP_CNT_ID:

			$data_emp_list = $level_analysis_model->get_item_graduation_emp_cnt(
				$_POST['job']
				, $_POST['level']
				, $arr_class_condition
				, $arr_atrb_condition
				, $arr_dept_condition
				, $arr_room_condition
				, $_POST['year_from']
				, $_POST['year_to']
				, $class_division
				, 'emp_list'
				, $_POST['row_1_key']
				, $_POST['row_2_key']
				, $_POST['column_1_key']
				, $_POST['column_2_key']
			);
			break;

		case ITEM_GRADUATION_ELAPSED_YEAR_EMP_CNT_ID:

			$data_emp_list = $level_analysis_model->get_item_graduation_elapsed_year_emp_cnt(
				$_POST['job']
				, $_POST['level']
				, $arr_class_condition
				, $arr_atrb_condition
				, $arr_dept_condition
				, $arr_room_condition
				, $_POST['year_from']
				, $_POST['year_to']
				, $class_division
				, 'emp_list'
				, $_POST['row_1_key']
				, $_POST['row_2_key']
				, $_POST['column_1_key']
				, $_POST['column_2_key']
			);
			break;

	}

	// 職員分ループ
	for ($i = 0; $i < count($data_emp_list); $i++) {

		// 行番号
		$data_emp_list[$i]['no'] = $i + 1;

		// 職員氏名
		$data_emp_list[$i]['emp_name']  = '';
		$data_emp_list[$i]['emp_name'] .= $data_emp_list[$i]['emp_lt_nm'];
		$data_emp_list[$i]['emp_name'] .= ' ';
		$data_emp_list[$i]['emp_name'] .= $data_emp_list[$i]['emp_ft_nm'];

		// 所属
		$data_emp_list[$i]['affiliation']  = '';
		$data_emp_list[$i]['affiliation'] .= $data_emp_list[$i]['class_nm'];
		$data_emp_list[$i]['affiliation'] .= ' > ';
		$data_emp_list[$i]['affiliation'] .= $data_emp_list[$i]['atrb_nm'];
		$data_emp_list[$i]['affiliation'] .= ' > ';
		$data_emp_list[$i]['affiliation'] .= $data_emp_list[$i]['dept_nm'];
		if ($data_emp_list[$i]['room_nm'] != '') {
			$data_emp_list[$i]['affiliation'] .= ' > ';
			$data_emp_list[$i]['affiliation'] .= $data_emp_list[$i]['room_nm'];
		}

	}

	return $data_emp_list;

}

function get_item_unit_year_emp_cnt(
	&$mdb2
	, $login_emp_id
	, $job
	, $level
	, $arr_class
	, $arr_atrb
	, $arr_dept
	, $arr_room
	, $year_from
	, $year_to
	, $class_division
	, $mode
	, $row_1_key
	, $row_2_key
	, $column_1_key
	, $column_2_key) {

	global $log;

	require_once("cl/model/search/cl_level_analysis_model.php");
	$level_analysis_model = new cl_level_analysis_model(&$mdb2,$login_emp_id);
	$log->debug("レベル分析用DBA取得",__FILE__,__LINE__);

	// 現在所属・職種・レベル認定年度・入職年度取得
	$empdata = $level_analysis_model->get_emp_item_unit_year_emp_cnt(
		$job
		, $level
		, $arr_class
		, $arr_atrb
		, $arr_dept
		, $arr_room
		, $year_from
		, $year_to
		, $class_division
		, $mode
		, $row_1_key
		, $row_2_key
		, $column_1_key
		, $column_2_key);

	//$log->debug("empdata：".print_r($empdata, true));

	// 所属変更履歴取得
	$classdata = $level_analysis_model->get_class_history_item_unit_year_emp_cnt(
		$job
		, $level
		, $arr_class
		, $arr_atrb
		, $arr_dept
		, $arr_room
		, $year_from
		, $year_to
		, $class_division
		, $mode
		, $row_1_key
		, $row_2_key
		, $column_1_key
		, $column_2_key);

	//$log->debug("classdata：".print_r($classdata, true));

	// 職種変更履歴取得
	if ($job != '') {
		$jobdata = $level_analysis_model->get_job_history_item_unit_year_emp_cnt(
			$job
			, $level
			, $arr_class
			, $arr_atrb
			, $arr_dept
			, $arr_room
			, $year_from
			, $year_to
			, $class_division
			, $mode
			, $row_1_key
			, $row_2_key
			, $column_1_key
			, $column_2_key);
	}

	//$log->debug("jobdata：".print_r($jobdata, true));

	// 対象職員分ループ
	for ($emp_idx = 0; $emp_idx < count($empdata); $emp_idx++) {

		// 職員ID
		$emp_id = $empdata[$emp_idx]['emp_id'];

		// 対象年度分ループ
		for ($year = $_POST['year_from']; $year <= $_POST['year_to']; $year++) {

			// 職員IDをセット
			$wkdata[$emp_id][$year]['emp_id'] = $emp_id;

			// 年度をセット
			$wkdata[$emp_id][$year]['year'] = $year;

			// 現所属先をセット
			$wkdata[$emp_id][$year]['class_id'] = $empdata[$emp_idx]['emp_class'];
			$wkdata[$emp_id][$year]['atrb_id'] = $empdata[$emp_idx]['emp_attribute'];
			$wkdata[$emp_id][$year]['dept_id'] = $empdata[$emp_idx]['emp_dept'];
			$wkdata[$emp_id][$year]['room_id'] = $empdata[$emp_idx]['emp_room'];

			// 現職種をセット
			$wkdata[$emp_id][$year]['job_id'] = $empdata[$emp_idx]['emp_job'];

			// 表示フラグ（入職年度）をセット
			$wkdata[$emp_id][$year]['disp_flg'] = $empdata[$emp_idx]['join_year'];

			// 対象年度をセット
			$wkdata[$emp_id][$year]['retire_year'] = $empdata[$emp_idx]['retire_year'];

			// 部署有無フラグをセット
			// 2012/07/11 Yamagawa upd(s)
			//$wkdata[$emp_id][$year]['atrb_exists'] = $empdata[$emp_idx]['atrb_exists'];
			$wkdata[$emp_id][$year]['dept_exists'] = $empdata[$emp_idx]['dept_exists'];
			// 2012/07/11 Yamagawa upd(e)

			// 指定所属内フラグをセット
			$wkdata[$emp_id][$year]['affiliation'] = $empdata[$emp_idx]['affiliation'];

			// 参照可能範囲内フラグをセット
			$wkdata[$emp_id][$year]['login_reference'] = $empdata[$emp_idx]['login_reference'];

			if ($mode == 'emp_list') {
				// 職員名・所属先名をセット
				$wkdata[$emp_id][$year]['emp_lt_nm'] = $empdata[$emp_idx]['emp_lt_nm'];
				$wkdata[$emp_id][$year]['emp_ft_nm'] = $empdata[$emp_idx]['emp_ft_nm'];
				$wkdata[$emp_id][$year]['class_nm'] = $empdata[$emp_idx]['class_nm'];
				$wkdata[$emp_id][$year]['atrb_nm'] = $empdata[$emp_idx]['atrb_nm'];
				$wkdata[$emp_id][$year]['dept_nm'] = $empdata[$emp_idx]['dept_nm'];
				$wkdata[$emp_id][$year]['room_nm'] = $empdata[$emp_idx]['room_nm'];
			}

			// 対象年度別のレベルを判定する
			if (
				$empdata[$emp_idx]['get_level5_year'] != ''
				&& $empdata[$emp_idx]['get_level5_year'] <= $year
			) {
				$wkdata[$emp_id][$year]['level'] = 5;
			} else if (
				$empdata[$emp_idx]['get_level4_year'] != ''
				&& $empdata[$emp_idx]['get_level4_year'] <= $year
			) {
				$wkdata[$emp_id][$year]['level'] = 4;
			} else if (
				$empdata[$emp_idx]['get_level3_year'] != ''
				&& $empdata[$emp_idx]['get_level3_year'] <= $year
			) {
				$wkdata[$emp_id][$year]['level'] = 3;
			} else if (
				$empdata[$emp_idx]['get_level2_year'] != ''
				&& $empdata[$emp_idx]['get_level2_year'] <= $year
			) {
				$wkdata[$emp_id][$year]['level'] = 2;
			} else if (
				$empdata[$emp_idx]['get_level1_year'] != ''
				&& $empdata[$emp_idx]['get_level1_year'] <= $year
			) {
				$wkdata[$emp_id][$year]['level'] = 1;
			} else {
				$wkdata[$emp_id][$year]['level'] = 0;
			}
		}

	}

	// 所属変更履歴分ループ
	// 2012/08/31 Yamagawa add(s)
	$emp_first_classdata = array();
	// 2012/08/31 Yamagawa add(e)
	for ($class_idx = 0; $class_idx < count($classdata); $class_idx++) {

		// キーブレイク：職員ID
		if ($emp_id != $classdata[$class_idx]['emp_id']) {

			// 年度（前行）が対象年度を上回っている場合
			if ($last_classdata['histyear'] > $_POST['year_from']) {

				// 対象年度分まで、所属先（前行）をセットする
				for ($year = $_POST['year_from']; $year < $last_classdata['histyear']; $year++) {
					$wkdata[$emp_id][$year]['class_id'] = $last_classdata['class_id'];
					$wkdata[$emp_id][$year]['atrb_id'] = $last_classdata['atrb_id'];
					$wkdata[$emp_id][$year]['dept_id'] = $last_classdata['dept_id'];
					$wkdata[$emp_id][$year]['room_id'] = $last_classdata['room_id'];
					// 2012/07/11 Yamagawa upd(s)
					//$wkdata[$emp_id][$year]['atrb_exists'] = $last_classdata['atrb_exists'];
					$wkdata[$emp_id][$year]['dept_exists'] = $last_classdata['dept_exists'];
					// 2012/07/11 Yamagawa upd(e)
					$wkdata[$emp_id][$year]['affiliation'] = $last_classdata['affiliation'];
					$wkdata[$emp_id][$year]['login_reference'] = $last_classdata['login_reference'];
				}

			}

			// キー入れ替え：職員ID
			$emp_id = $classdata[$class_idx]['emp_id'];

		// 年度（参照行）が対象年度未満の場合
		} else if ($classdata[$class_idx]['histyear'] < $_POST['year_from']) {

			// 対象年度分まで、所属先（前行）をセットする
			for ($year = $_POST['year_from']; $year < $last_classdata['histyear']; $year++) {
				$wkdata[$emp_id][$year]['class_id'] = $last_classdata['class_id'];
				$wkdata[$emp_id][$year]['atrb_id'] = $last_classdata['atrb_id'];
				$wkdata[$emp_id][$year]['dept_id'] = $last_classdata['dept_id'];
				$wkdata[$emp_id][$year]['room_id'] = $last_classdata['room_id'];
				// 2012/07/11 Yamagawa upd(s)
				//$wkdata[$emp_id][$year]['atrb_exists'] = $last_classdata['atrb_exists'];
				$wkdata[$emp_id][$year]['dept_exists'] = $last_classdata['dept_exists'];
				// 2012/07/11 Yamagawa upd(e)
				$wkdata[$emp_id][$year]['affiliation'] = $last_classdata['affiliation'];
				$wkdata[$emp_id][$year]['login_reference'] = $last_classdata['login_reference'];

				// 表示フラグをセット
				$wkdata[$emp_id][$year]['disp_flg'] = $last_classdata['histyear'];
			}

		} else {

			// 年度（前行）と年度（参照行）が２年以上期間がある場合
			if ($last_classdata['histyear'] - 1 != $classdata[$class_idx]['histyear']) {

				// 年度（参照行）の前年まで、所属先（前行）をセットする
				for ($year = $last_classdata['histyear']; $year > $classdata[$class_idx]['histyear']; $year--) {
					$wkdata[$emp_id][$year]['class_id'] = $last_classdata['class_id'];
					$wkdata[$emp_id][$year]['atrb_id'] = $last_classdata['atrb_id'];
					$wkdata[$emp_id][$year]['dept_id'] = $last_classdata['dept_id'];
					$wkdata[$emp_id][$year]['room_id'] = $last_classdata['room_id'];
					// 2012/07/11 Yamagawa upd(s)
					//$wkdata[$emp_id][$year]['atrb_exists'] = $last_classdata['atrb_exists'];
					$wkdata[$emp_id][$year]['dept_exists'] = $last_classdata['dept_exists'];
					// 2012/07/11 Yamagawa upd(e)
					$wkdata[$emp_id][$year]['affiliation'] = $last_classdata['affiliation'];
					$wkdata[$emp_id][$year]['login_reference'] = $last_classdata['login_reference'];

					// 表示フラグをセット
					$wkdata[$emp_id][$year]['disp_flg'] = $last_classdata['histyear'];
				}

			}

			// 年度
			$year = $classdata[$class_idx]['histyear'];

			// 参照行の所属をセットする
			$wkdata[$emp_id][$year]['class_id'] = $classdata[$class_idx]['class_id'];
			$wkdata[$emp_id][$year]['atrb_id'] = $classdata[$class_idx]['atrb_id'];
			$wkdata[$emp_id][$year]['dept_id'] = $classdata[$class_idx]['dept_id'];
			$wkdata[$emp_id][$year]['room_id'] = $classdata[$class_idx]['room_id'];
			// 2012/07/11 Yamagawa upd(s)
			//$wkdata[$emp_id][$year]['atrb_exists'] = $classdata[$class_idx]['atrb_exists'];
			$wkdata[$emp_id][$year]['dept_exists'] = $classdata[$class_idx]['dept_exists'];
			// 2012/07/11 Yamagawa upd(e)
			$wkdata[$emp_id][$year]['affiliation'] = $classdata[$class_idx]['affiliation'];
			$wkdata[$emp_id][$year]['login_reference'] = $classdata[$class_idx]['login_reference'];

			// 表示フラグをセット
			$wkdata[$emp_id][$year]['disp_flg'] = $classdata[$class_idx]['histyear'];

		}

		// 参照データを保持
		$last_classdata = $classdata[$class_idx];

	}

	// 2012/08/31 Yamagawa add(s)
	if ($last_classdata['histyear'] > $_POST['year_from']) {

		// 対象年度分まで、所属先（前行）をセットする
		for ($year = $_POST['year_from']; $year < $last_classdata['histyear']; $year++) {
			$wkdata[$emp_id][$year]['class_id'] = $last_classdata['class_id'];
			$wkdata[$emp_id][$year]['atrb_id'] = $last_classdata['atrb_id'];
			$wkdata[$emp_id][$year]['dept_id'] = $last_classdata['dept_id'];
			$wkdata[$emp_id][$year]['room_id'] = $last_classdata['room_id'];
			$wkdata[$emp_id][$year]['dept_exists'] = $last_classdata['dept_exists'];
			$wkdata[$emp_id][$year]['affiliation'] = $last_classdata['affiliation'];
			$wkdata[$emp_id][$year]['login_reference'] = $last_classdata['login_reference'];
		}

	}
	// 2012/08/31 Yamagawa add(e)

	// 職種変更履歴分ループ
	for ($job_idx = 0; $job_idx < count($jobdata); $job_idx++) {

		// キーブレイク：職員ID
		if ($emp_id != $jobdata[$job_idx]['emp_id']) {

			// 年度（前行）が対象年度を上回っている場合
			if ($last_jobdata['histyear'] > $_POST['year_from']) {

				// 対象年度分まで、職種（前行）をセットする
				for ($year = $_POST['year_from']; $year < $last_jobdata['histyear']; $year++) {
					$wkdata[$emp_id][$year]['job_id'] = $last_jobdata['job_id'];
				}

			}

			// キー入れ替え：職員ID
			$emp_id = $jobdata[$job_idx]['emp_id'];

		// 年度（参照行）が対象年度未満の場合
		} else if ($jobdata[$job_idx]['histyear'] < $_POST['year_from']) {

			// 対象年度分まで、職種（前行）をセットする
			for ($year = $_POST['year_from']; $year < $last_jobdata['histyear']; $year++) {
				$wkdata[$emp_id][$year]['job_id'] = $last_jobdata['job_id'];

				// 表示フラグをセット
				$wkdata[$emp_id][$year]['disp_flg'] = $last_jobdata['histyear'];
			}

		} else {

			// 年度（前行）と年度（参照行）が２年以上期間がある場合
			if ($last_jobdata['histyear'] - 1 != $jobdata[$job_idx]['histyear']) {

				// 年度（参照行）の前年まで、職種（前行）をセットする
				for ($year = $last_jobdata['histyear']; $year > $jobdata[$job_idx]['histyear']; $year--) {
					$wkdata[$emp_id][$year]['job_id'] = $last_jobdata['job_id'];

					// 表示フラグをセット
					$wkdata[$emp_id][$year]['disp_flg'] = $last_jobdata['histyear'];
				}

			}

			// 年度
			$year = $jobdata[$job_idx]['histyear'];

			// 参照行の職種をセットする
			$wkdata[$emp_id][$year]['job_id'] = $jobdata[$job_idx]['job_id'];

			// 表示フラグをセット
			$wkdata[$emp_id][$year]['disp_flg'] = $jobdata[$job_idx]['histyear'];

		}

		// 参照データを保持
		$last_jobdata = $jobdata[$job_idx];

	}

	// キーインデックス用
	$key_index = array();
	$key_value = 0;

	foreach ($wkdata as $wkemp) {

		foreach($wkemp as $wkyear) {

			// 退職年度を上回った場合
			if (
				$wkyear['retire_year'] != ''
				&& $wkyear['retire_year'] < $wkyear['year']
			) {
				continue;
			}

			// 表示フラグがNULL
			if ($wkyear['disp_flg'] == '') {
				continue;
			}

			// 対象の部署マスタが存在しない場合
			// 2012/07/11 Yamagawa upd(s)
			//if ($wkyear['atrb_exists'] == 'f') {
			if ($wkyear['dept_exists'] == 'f') {
			// 2012/07/11 Yamagawa upd(e)
				continue;
			}

			// 職種選択　かつ　不一致の場合
			if (
				$job != ''
				&& $job != $wkyear['job_id']
			) {
				continue;
			}

			// レベル選択　かつ　不一致の場合
			if (
				$level != ''
				&& $level != $wkyear['level']
			) {
				continue;
			}

			// 指定した所属外の場合
			if ($wkyear['affiliation'] == 'f') {
				continue;
			}

			// ログインユーザーの参照可能範囲外の場合
			if ($wkyear['login_reference'] == 'f') {
				continue;
			}

			// キー項目の取得
			// 2012/07/11 Yamagawa upd(s)
			//$atrb_id = $wkyear['atrb_id'];
			$dept_id = $wkyear['dept_id'];
			// 2012/07/11 Yamagawa upd(e)
			$wklevel = $wkyear['level'];
			$year = $wkyear['year'];

			if ($mode == 'analysis') {

				// インデックスが未登録の場合
				// 2012/07/11 Yamagawa upd(s)
				/*
				if (
					$key_index[$atrb_id][$wklevel][$year]
					|| $key_index[$atrb_id][$wklevel][$year] === 0
				) {

					// 明細欄の値をインクリメント
					$analysis_data[$key_index[$atrb_id][$wklevel][$year]]['count']++;

				} else {

					// 明細欄のキー値をセット
					$analysis_data[$key_value]['atrb_id'] = $atrb_id;
					$analysis_data[$key_value]['level'] = $wklevel;
					$analysis_data[$key_value]['year'] = $year;

					// 明細欄の値をインクリメント
					$analysis_data[$key_value]['count'] = 1;

					// 明細欄のインデックスを登録
					$key_index[$atrb_id][$wklevel][$year] = $key_value;
					$key_value++;

				}
				*/
				if (
					$key_index[$dept_id][$wklevel][$year]
					|| $key_index[$dept_id][$wklevel][$year] === 0
				) {

					// 明細欄の値をインクリメント
					$analysis_data[$key_index[$dept_id][$wklevel][$year]]['count']++;

				} else {

					// 明細欄のキー値をセット
					$analysis_data[$key_value]['dept_id'] = $dept_id;
					$analysis_data[$key_value]['level'] = $wklevel;
					$analysis_data[$key_value]['year'] = $year;

					// 明細欄の値をインクリメント
					$analysis_data[$key_value]['count'] = 1;

					// 明細欄のインデックスを登録
					$key_index[$dept_id][$wklevel][$year] = $key_value;
					$key_value++;

				}
				// 2012/07/11 Yamagawa upd(e)

			} else if ($mode == 'emp_list') {

				// 2012/07/11 Yamagawa upd(s)
				/*
				if (
					$row_1_key != $atrb_id
					&& $row_1_key != 'total'
				) {
				*/
				if (
					$row_1_key != $dept_id
					&& $row_1_key != 'total'
				) {
				// 2012/07/11 Yamagawa upd(e)
					continue;
				}

				if ($row_2_key != $wklevel) {
					continue;
				}

				if ($column_1_key != $year) {
					continue;
				}

				$analysis_data[$key_value]['emp_lt_nm'] = $wkyear['emp_lt_nm'];
				$analysis_data[$key_value]['emp_ft_nm'] = $wkyear['emp_ft_nm'];
				$analysis_data[$key_value]['class_nm'] = $wkyear['class_nm'];
				$analysis_data[$key_value]['atrb_nm'] = $wkyear['atrb_nm'];
				$analysis_data[$key_value]['dept_nm'] = $wkyear['dept_nm'];
				$analysis_data[$key_value]['room_nm'] = $wkyear['room_nm'];
				$key_value++;

			}

		}
	}

	return $analysis_data;
}

function set_cell_data(
	$value
	, $colspan
	, $rowspan
	, $bolheader
	, $bolemplist
	, $align
	, $row_1_key
	, $row_2_key
	, $column_1_key
	, $column_2_key
){

	$cell_data = array();
	$cell_data['value'] = $value;
	$cell_data['colspan'] = $colspan;
	$cell_data['rowspan'] = $rowspan;
	$cell_data['bolheader'] = $bolheader;
	$cell_data['bolemplist'] = $bolemplist;
	if ($align == '') {
		$cell_data['align'] = 'left';
	} else {
		$cell_data['align'] = $align;
	}
	$cell_data['row_1_key'] = $row_1_key;
	$cell_data['row_2_key'] = $row_2_key;
	$cell_data['column_1_key'] = $column_1_key;
	$cell_data['column_2_key'] = $column_2_key;
	return $cell_data;

}

function get_level($level ,$bol_zero) {

	$arr = array();
	$idx = 0;
	if ($level == '') {
		if ($bol_zero) {
			$arr[$idx]['level_disp'] = 'レベル0';
			$arr[$idx]['level'] = 0;
			$idx++;
		}
		$arr[$idx]['level_disp'] = 'レベルI';
		$arr[$idx]['level'] = 1;
		$idx++;
		$arr[$idx]['level_disp'] = 'レベルII';
		$arr[$idx]['level'] = 2;
		$idx++;
		$arr[$idx]['level_disp'] = 'レベルIII';
		$arr[$idx]['level'] = 3;
		$idx++;
		$arr[$idx]['level_disp'] = 'レベルIV';
		$arr[$idx]['level'] = 4;
		$idx++;
		$arr[$idx]['level_disp'] = 'レベルV';
		$arr[$idx]['level'] = 5;
	} else {
		switch ($level) {
			case 1:
				$level_disp = 'レベルI';
				break;
			case 2:
				$level_disp = 'レベルII';
				break;
			case 3:
				$level_disp = 'レベルIII';
				break;
			case 4:
				$level_disp = 'レベルIV';
				break;
			case 5:
				$level_disp = 'レベルV';
				break;
		}
		$arr[$idx]['level_disp'] = $level_disp;
		$arr[$idx]['level'] = $level;
	}
	return $arr;
}

function get_level_disp($level) {

	$disp = '';

	switch($level) {
		case 0:
			$disp = 'レベル0';
			break;
		case 1:
			$disp = 'レベルI';
			break;
		case 2:
			$disp = 'レベルII';
			break;
		case 3:
			$disp = 'レベルIII';
			break;
		case 4:
			$disp = 'レベルIV';
			break;
		case 5:
			$disp = 'レベルV';
			break;
	}

	return $disp;

}


$log->info(basename(__FILE__)." END");
