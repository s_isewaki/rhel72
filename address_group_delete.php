<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require_once("about_comedix.php");

$fname = $PHP_SELF;

$session = qualify_session($session, $fname);
if ($session == "0") {
    js_login_exit();
}

$checkauth = check_authority($session, 9, $fname);
if ($checkauth == "0") {
    js_login_exit();
}

$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
    js_error_exit();
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// チェックされたメール送信グループをループ
foreach ($group_ids as $tmp_group_id) {

	// 所属職員情報を削除
	$sql = "delete from adbkgrp";
	$cond = "where id = $tmp_group_id";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
	    js_error_exit();
	}
}

// トランザクションをコミット
pg_query($con, "commit");

pg_close($con);

// メール送信グループ画面をリフレッシュ
echo("<script type=\"text/javascript\">location.href = 'address_group.php?session=$session';</script>");
