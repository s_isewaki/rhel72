<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 職員登録 | 権限グループ</title>
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once('Cmx.php');
require_once('Cmx/Model/SystemConfig.php');
require_once("label_by_profile_type.ini");
require_once("get_cas_title_name.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 19, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// データベースに接続
$con = connect2db($fname);

// グループ一覧を配列に格納
$sql = "select group_id, group_nm from authgroup";
$cond = "order by group_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$groups = array();
while ($row = pg_fetch_array($sel)) {
    $groups[$row["group_id"]] = $row["group_nm"];
}
if ($group_id == "") {
    $group_id = key($groups);
}


// 権限グループを表示する場合
if ($group_id != "") {

    // 表示対象の権限グループ情報を取得
    $sql = "select * from authgroup";
    $cond = "where group_id = $group_id";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $authinfo = pg_fetch_array($sel);

    // ライセンス情報の取得
    $sql = "select * from license";
    $cond = "";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    for ($i = 1; $i <= 60; $i++) { // 2014/2/2 19から60に変更
        $var_name = "func$i";
        $$var_name = pg_fetch_result($sel, 0, "lcs_func$i");
    }

    // 組織タイプを取得
    $profile_type = get_profile_type($con, $fname);

    // イントラメニュー名を取得
    $sql = "select * from intramenu";
    $cond = "";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $intra_menu1 = pg_fetch_result($sel, 0, "menu1");
    $intra_menu2_4 = pg_fetch_result($sel, 0, "menu2_4");
    $intra_menu3 = pg_fetch_result($sel, 0, "menu3");

    // 社会福祉法人の場合「当直・外来分担表」を「当直分担表」とする
    if ($profile_type == PROFILE_TYPE_WELFARE) {
        $intra_menu2_4 = str_replace("・外来", "", $intra_menu2_4);
    }
}

require_once("get_menu_label.ini");
$report_menu_label = get_report_menu_label($con, $fname);
$shift_menu_label = get_shift_menu_label($con, $fname);

// system_config
$conf = new Cmx_SystemConfig();

//権限グループの初期設定を取得する
$default_group_id = $conf->get("default_set.group_id");
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function initPage() {
    setPtRefAuth();
    setPtOutRefAuth();
    setPtDisRefAuth();
    setIntraAuth();
    setQAAuth();
//XX    setCmtAuth();
    setEmpRefAuth();
    setAmbAuth();
    setWardAuth();
    setInciAuth();
    setFPlusAuth();
    setManabuAuth();
    setMedAuth();
    setKViewAuth();
    setSchdAuth();
    setFclAuth();
    setLinkAuth();
    setBbsAuth();
    setAdbkAuth();
    setWebmlAuth();
    setShiftAuth();
    setShift2Auth();
    setCasAuth();
    setJinjiAuth();
    setLadderAuth();
    setCareerAuth();
    setJnlAuth();
    setPjtAuth();
    setNlcsAuth();
    setCAuthAuth();
    <? if ($func31=="t") { ?>setCCUsr1Auth();<? } ?>
}

function setPtRefAuth() {
    setSubBox(document.employee.ptif, document.employee.ptreg.checked, '<? echo($func1); ?>');
}

function setPtOutRefAuth() {
    setSubBox(document.employee.inout, document.employee.outreg.checked, '<? echo($func1); ?>');
}

function setPtDisRefAuth() {
    setSubBox(document.employee.dishist, document.employee.disreg.checked, '<? echo($func1); ?>');
}

function setIntraAuth() {
    var check_flag = (
        document.employee.ymstat.checked ||
        document.employee.intram.checked ||
        document.employee.stat.checked ||
        document.employee.allot.checked ||
        document.employee.life.checked
    );
    setSubBox(document.employee.intra, check_flag, '<? echo($func4); ?>');
}

function setQAAuth() {
    setSubBox(document.employee.qa, document.employee.qaadm.checked, 't');
}

//XX function setCmtAuth() {
//XX     setSubBox(document.employee.cmt, document.employee.cmtadm.checked, 't');
//XX }

function setEmpRefAuth() {
    setSubBox(document.employee.empif, document.employee.reg.checked, 't');
}

function setAmbAuth() {
    setSubBox(document.employee.amb, document.employee.ambadm.checked, '<? echo($func15); ?>');
}

function setWardAuth() {
    if (document.employee.ward_reg) {
        setSubBox(document.employee.ward, document.employee.ward_reg.checked, '<? echo($func2); ?>');
    }
}

function setInciAuth() {
    setSubBox(document.employee.inci, document.employee.rm.checked, '<? echo($func5); ?>');
}

function setFPlusAuth() {
    setSubBox(document.employee.fplus, document.employee.fplusadm.checked, '<? echo($func13); ?>');
}

function setManabuAuth() {
    setSubBox(document.employee.manabu, document.employee.manabuadm.checked, '<? echo($func12); ?>');
}

function setMedAuth() {
    setSubBox(document.employee.med, document.employee.medadm.checked, '<? echo($func6); ?>');
}

function setKViewAuth() {
    setSubBox(document.employee.kview, document.employee.kviewadm.checked, '<? echo($func11); ?>');
}

function setSchdAuth() {
    setSubBox(document.employee.schd, document.employee.schdplc.checked, 't');
}

function setFclAuth() {
    setSubBox(document.employee.resv, document.employee.fcl.checked, 't');
}

function setLinkAuth() {
    setSubBox(document.employee.link, document.employee.linkadm.checked, 't');
}

function setBbsAuth() {
    setSubBox(document.employee.bbs, document.employee.bbsadm.checked, 't');
}

function setAdbkAuth() {
    setSubBox(document.employee.adbk, document.employee.adbkadm.checked, 't');
}

function setWebmlAuth() {
    setSubBox(document.employee.webml, document.employee.webmladm.checked, 't');
}

function setShiftAuth() {
    setSubBox(document.employee.shift, document.employee.shiftadm.checked, '<? echo($func9); ?>');
}

function setShift2Auth() {
    setSubBox(document.employee.shift2, document.employee.shift2adm.checked, '<? echo($func18); ?>');
}

function setCasAuth() {
    setSubBox(document.employee.cas, document.employee.casadm.checked, '<? echo($func8); ?>');
}

function setJinjiAuth() {
    setSubBox(document.employee.jinji, document.employee.jinjiadm.checked, '<? echo($func16); ?>');
}

function setLadderAuth() {
    setSubBox(document.employee.ladder, document.employee.ladderadm.checked, '<? echo($func17); ?>');
}

function setCareerAuth() {
    setSubBox(document.employee.career, document.employee.careeradm.checked, '<? echo($func19); ?>');
}

function setJnlAuth() {
    setSubBox(document.employee.jnl, document.employee.jnladm.checked, '<? echo($func14); ?>');
}

function setNlcsAuth() {
    setSubBox(document.employee.nlcs, document.employee.nlcsadm.checked, '<? echo($func10); ?>');
}

function setPjtAuth() {
    setSubBox(document.employee.pjt, document.employee.pjtadm.checked, 't');
}

function setCAuthAuth() {
    setSubBox(document.employee.pass_flg, document.employee.cauth.checked, 't');
}

function setCCUsr1Auth() {
    setSubBox(document.employee.ccusr1, document.employee.ccadm1.checked, '<? echo($func31); ?>');
}

function setSubBox(sub_box, check_flag, sub_box_checkable) {
    if (check_flag) {
        sub_box.checked = true;
        sub_box.disabled = true;
    } else {
        sub_box.disabled = (sub_box_checkable != 't');
    }
}

function registerGroup() {
    window.open('employee_auth_group_register.php?session=<? echo($session); ?>', 'newwin', 'width=640,height=480,scrollbars=yes');
}

function deleteGroup() {
    var selected = false;
    if (document.delform.elements['group_ids[]']) {
        if (document.delform.elements['group_ids[]'].length) {
            for (var i = 0, j = document.delform.elements['group_ids[]'].length; i < j; i++) {
                if (document.delform.elements['group_ids[]'][i].checked) {
                    selected = true;
                    break;
                }
            }
        } else if (document.delform.elements['group_ids[]'].checked) {
            selected = true;
        }
    }

    if (!selected) {
        alert('削除対象が選択されていません。');
        return;
    }

    if (confirm('削除します。よろしいですか？')) {
        document.delform.submit();
    }
}

function default_set() {
        document.defaultform.submit();
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-style:none;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"<? if ($group_id != "") {echo(' onload="initPage();"');} ?>>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="employee_info_menu.php?session=<? echo($session); ?>"><img src="img/icon/b26.gif" width="32" height="32" border="0" alt="職員登録"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="employee_info_menu.php?session=<? echo($session); ?>"><b>職員登録</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_info_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">検索</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">新規登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_bulk_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="140" align="center" bgcolor="#bdd1e7"><a href="employee_db_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員登録用DB操作</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#5279a5"><a href="employee_auth_group.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>権限グループ</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_display_group.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示グループ</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_menu_group.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メニューグループ</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_auth_bulk_setting.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括設定</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_auth_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限管理</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_field_option.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">項目管理</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr valign="top">
<td width="15%">
<form name="delform" action="employee_auth_group_delete.php">
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
権限グループ名<br>
<input type="button" value="作成" onclick="registerGroup();">
<input type="button" value="削除" onclick="deleteGroup();">
</font></td>
</tr>
<tr height="100" valign="top">
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<?
foreach ($groups as $tmp_group_id => $tmp_group_nm) {
    echo("<tr height=\"22\" valign=\"middle\">\n");
    echo("<td width=\"1\"><input type=\"checkbox\" name=\"group_ids[]\" value=\"$tmp_group_id\"></td>\n");
    if ($tmp_group_id == $group_id) {
        echo("<td style=\"padding-left:3px;background-color:#ffff66;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_group_nm</font></td>\n");
    } else {
        echo("<td style=\"padding-left:3px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"employee_auth_group.php?session=$session&group_id=$tmp_group_id\">$tmp_group_nm</a></font></td>\n");
    }
    echo("</tr>\n");
}
?>
</table>
</td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="default_group_id" value="<? echo($default_group_id); ?>">
</form>


<img src="img/spacer.gif" width="1" height="8" alt=""><br>
<form name="defaultform" action="employee_auth_group_default.php">
    <table border="0" cellspacing="0" cellpadding="2" class="list">
        <tr height="22">
            <td width="15%">
                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">初期権限グループ<input type="button" value="保存" onclick="default_set();"><br>
                <select name="group_id">
                    <option value="">　　　　　</option>
<?
foreach ($groups as $tmp_group_id => $tmp_group_nm) {
    echo("<option value=\"$tmp_group_id\"");
    if ($tmp_group_id == $default_group_id) {
        echo(" selected");
    }
    echo(">$tmp_group_nm\n");
}
                        ?>
                </select>
                </font>
                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="red"><br>※保存された権限グループは<br>「職員登録＞新規登録」画面で初期表示されます</font>
            </td>
        </tr>
    </table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>


</td>
<td><img src="img/spacer.gif" alt="" width="5" height="1"></td>
<td>
<? if ($group_id != "") { ?>
<form name="employee" action="employee_auth_group_update_exe.php">
<table width="50%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限グループ名</font></td>
<td><input type="text" name="group_nm" value="<? echo($groups[$group_id]); ?>" size="25" style="ime-mode:active;"></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="10"><br>
<table border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>基本機能</b></font></td>
</tr>
</table>
<table width="50%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff">
<td></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ユーザ機能</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">管理者機能</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ウェブメール</font></td>
<td align="center"><input name="webml" type="checkbox" value="t"<? if ($authinfo["webml_flg"] == "t") {echo(" checked");} ?>></td>
<td align="center"><input name="webmladm" type="checkbox" value="t"<? if ($authinfo["webmladm_flg"] == "t") {echo(" checked");} ?> onclick="setWebmlAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">スケジュール</font></td>
<td align="center"><input name="schd" type="checkbox" value="t"<? if ($authinfo["schd_flg"] == "t") {echo(" checked");} ?>></td>
<td align="center"><input name="schdplc" type="checkbox" value="t"<? if ($authinfo["schdplc_flg"] == "t") {echo(" checked");} ?> onclick="setSchdAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">委員会・WG</font></td>
<td align="center"><input name="pjt" type="checkbox" value="t"<? if ($authinfo["pjt_flg"] == "t") {echo(" checked");} ?>></td>
<td align="center"><input name="pjtadm" type="checkbox" value="t"<? if ($authinfo["pjtadm_flg"] == "t") {echo(" checked");} ?> onclick="setPjtAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ・回覧板</font></td>
<td align="center"><input name="newsuser" type="checkbox" value="t"<? if ($authinfo["newsuser_flg"] == "t") {echo(" checked");} ?>></td>
<td align="center"><input name="news" type="checkbox" value="t"<? if ($authinfo["news_flg"] == "t") {echo(" checked");} ?>></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タスク</font></td>
<td align="center"><input name="work" type="checkbox" value="t"<? if ($authinfo["work_flg"] == "t") {echo(" checked");} ?>></td>
<td></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">伝言メモ</font></td>
<td align="center"><input name="phone" type="checkbox" value="t"<? if ($authinfo["phone_flg"] == "t") {echo(" checked");} ?>></td>
<td></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">設備予約</font></td>
<td align="center"><input name="resv" type="checkbox" value="t"<? if ($authinfo["resv_flg"] == "t") {echo(" checked");} ?>></td>
<td align="center"><input name="fcl" type="checkbox" value="t"<? if ($authinfo["fcl_flg"] == "t") {echo(" checked");} ?> onclick="setFclAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤表／勤務管理</font></td>
<td align="center"><input name="attd" type="checkbox" value="t"<? if ($authinfo["attdcd_flg"] == "t") {echo(" checked");} ?>></td>
<td align="center"><input name="wkadm" type="checkbox" value="t"<? if ($authinfo["wkadm_flg"] == "t") {echo(" checked");} ?>></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">決裁・申請／ワークフロー</font></td>
<td align="center"><input name="aprv_use" type="checkbox" value="t"<? if ($authinfo["aprv_flg"] == "t") {echo(" checked");} ?>></td>
<td align="center"><input name="wkflw" type="checkbox" value="t"<? if ($authinfo["wkflw_flg"] == "t") {echo(" checked");} ?>></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ネットカンファレンス</font></td>
<td align="center"><input name="conf" type="checkbox" value="t"<? if ($authinfo["conf_flg"] == "t") {echo(" checked");} ?>></td>
<td></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">掲示板・電子会議室</font></td>
<td align="center"><input name="bbs" type="checkbox" value="t"<? if ($authinfo["bbs_flg"] == "t") {echo(" checked");} ?>></td>
<td align="center"><input name="bbsadm" type="checkbox" value="t"<? if ($authinfo["bbsadm_flg"] == "t") {echo(" checked");} ?> onclick="setBbsAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">Q&amp;A</font></td>
<td align="center"><input name="qa" type="checkbox" value="t"<? if ($authinfo["qa_flg"] == "t") {echo(" checked");} ?>></td>
<td align="center"><input name="qaadm" type="checkbox" value="t"<? if ($authinfo["qaadm_flg"] == "t") {echo(" checked");} ?> onclick="setQAAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文書管理</font></td>
<td align="center"><input name="lib" type="checkbox" value="t"<? if ($authinfo["lib_flg"] == "t") {echo(" checked");} ?>></td>
<td align="center"><input name="libadm" type="checkbox" value="t"<? if ($authinfo["libadm_flg"] == "t") {echo(" checked");} ?>></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">アドレス帳</font></td>
<td align="center"><input name="adbk" type="checkbox" value="t"<? if ($authinfo["adbk_flg"] == "t") {echo(" checked");} ?>></td>
<td align="center"><input name="adbkadm" type="checkbox" value="t"<? if ($authinfo["adbkadm_flg"] == "t") {echo(" checked");} ?> onclick="setAdbkAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">内線電話帳</font></td>
<td align="center"><input name="ext" type="checkbox" value="t"<? if ($authinfo["ext_flg"] == "t") {echo(" checked");} ?>></td>
<td align="center"><input name="extadm" type="checkbox" value="t"<? if ($authinfo["extadm_flg"] == "t") {echo(" checked");} ?>></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">リンクライブラリ</font></td>
<td align="center"><input name="link" type="checkbox" value="t"<? if ($authinfo["link_flg"] == "t") {echo(" checked");} ?>></td>
<td align="center"><input name="linkadm" type="checkbox" value="t"<? if ($authinfo["linkadm_flg"] == "t") {echo(" checked");} ?> onclick="setLinkAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">パスワード変更</font></td>
<td align="center"><input name="pass_flg" type="checkbox" value="t"<? if ($authinfo["pass_flg"] == "t") {echo(" checked");} ?>></td>
<td></td>
</tr>
<!--tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">コミュニティサイト</font></td>
<td align="center"><input name="cmt" type="checkbox" value="t"<? if ($authinfo["cmt_flg"] == "t") {echo(" checked");} ?>></td>
<td align="center"><input name="cmtadm" type="checkbox" value="t"<? if ($authinfo["cmtadm_flg"] == "t") {echo(" checked");} ?> onclick="setCmtAuth();"></td>
</tr-->
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">備忘録</font></td>
<td align="center"><input name="memo" type="checkbox" value="t"<? if ($authinfo["memo_flg"] == "t") {echo(" checked");} ?>></td>
<td></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">端末認証</font></td>
<td align="center"><input name="cauth" type="checkbox" value="t"<? if ($authinfo["cauth_flg"] == "t") {echo(" checked");} ?> onclick="setCAuthAuth();"></td>
<td></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="10"><br>
<table border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>管理機能</b></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff">
<td></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ユーザ機能</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">管理者機能</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員参照</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">組織プロフィール</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">組織</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職種</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">役職</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カレンダー</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 院内行事登録／法人内行事登録
echo($_label_by_profile["EVENT_REG"][$profile_type]);
?></font></td>
<td></td>
<td align="center"><input name="tmgd" type="checkbox" value="t"<? if ($authinfo["tmgd_flg"] == "t") {echo(" checked");} ?>></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員登録</font></td>
<td></td>
<td align="center"><input name="reg" type="checkbox" value="t"<? if ($authinfo["reg_flg"] == "t") {echo(" checked");} ?> onclick="setEmpRefAuth();"></td>
<td align="center"><input name="empif" type="checkbox" value="t"<? if ($authinfo["empif_flg"] == "t") {echo(" checked");} ?>></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">マスターメンテナンス</font></td>
<td></td>
<td></td>
<td></td>
<td align="center"><input name="hspprf" type="checkbox" value="t"<? if ($authinfo["hspprf_flg"] == "t") {echo(" checked");} ?>></td>
<td align="center"><input name="dept_reg" type="checkbox" value="t"<? if ($authinfo["dept_flg"] == "t") {echo(" checked");} ?>></td>
<td align="center"><input name="job_flg" type="checkbox" value="t"<? if ($authinfo["job_flg"] == "t") {echo(" checked");} ?>></td>
<td align="center"><input name="status_flg" type="checkbox" value="t"<? if ($authinfo["status_flg"] == "t") {echo(" checked");} ?>></td>
<td align="center"><input name="attditem" type="checkbox" value="t"<? if ($authinfo["attditem_flg"] == "t") {echo(" checked");} ?>></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">環境設定</font></td>
<td></td>
<td align="center"><input name="config" type="checkbox" value="t"<? if ($authinfo["config_flg"] == "t") {echo(" checked");} ?>></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ライセンス管理</font></td>
<td></td>
<td align="center"><input name="lcs" type="checkbox" value="t"<? if ($authinfo["lcs_flg"] == "t") {echo(" checked");} ?>></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">アクセスログ</font></td>
<td></td>
<td align="center"><input name="accesslog" type="checkbox" value="t"<? if ($authinfo["accesslog_flg"] == "t") {echo(" checked");} ?>></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="10"><br>
<table border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>業務機能</b></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff">
<td></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 患者登録／利用者登録
echo($_label_by_profile["PATIENT_REG"][$profile_type]);
?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 患者基本情報／利用者基本情報
echo($_label_by_profile["PATIENT_INFO"][$profile_type]);
?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 来院登録／来所登録
echo($_label_by_profile["OUT_REG"][$profile_type]);
?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 入院来院履歴／入所来所履歴
echo($_label_by_profile["IN_OUT"][$profile_type]);
?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">プロブレムリスト</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">プロブレム</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">管理者権限</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 患者管理／利用者管理
echo($_label_by_profile["PATIENT_MANAGE"][$profile_type]);
?></font></td>
<td align="center"><input name="ptreg" type="checkbox" value="t"<? if ($authinfo["ptreg_flg"] == "t") {echo(" checked");} ?><? if ($func1 == "f") {echo(" disabled");} ?> onclick="setPtRefAuth();"></td>
<td align="center"><input name="ptif" type="checkbox" value="t"<? if ($authinfo["ptif_flg"] == "t") {echo(" checked");} ?><? if ($func1 == "f") {echo(" disabled");} ?>></td>
<td align="center"><input name="outreg" type="checkbox" value="t"<? if ($authinfo["outreg_flg"] == "t") {echo(" checked");} ?><? if ($func1 == "f") {echo(" disabled");} ?> onclick="setPtOutRefAuth();"></td>
<td align="center"><input name="inout" type="checkbox" value="t"<? if ($authinfo["inout_flg"] == "t") {echo(" checked");} ?><? if ($func1 == "f") {echo(" disabled");} ?>></td>
<td align="center"><input name="disreg" type="checkbox" value="t"<? if ($authinfo["disreg_flg"] == "t") {echo(" checked");} ?><? if ($func1 == "f") {echo(" disabled");} ?> onclick="setPtDisRefAuth();"></td>
<td align="center"><input name="dishist" type="checkbox" value="t"<? if ($authinfo["dishist_flg"] == "t") {echo(" checked");} ?><? if ($func1 == "f") {echo(" disabled");} ?>></td>
<td align="center"><input name="ptadm" type="checkbox" value="t"<? if ($authinfo["ptadm_flg"] == "t") {echo(" checked");} ?><? if ($func1 == "f") {echo(" disabled");} ?>></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="8"><br>
<table width="50%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff">
<td></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ユーザ機能</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">管理者機能</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">検索ちゃん</font></td>
<td align="center"><input name="search" type="checkbox" value="t"<? if ($authinfo["search_flg"] == "t") {echo(" checked");} ?><? if ($func7 == "f") {echo(" disabled");} ?>></td>
<td></td>
</tr>



<? if ($func31=="t") { ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">スタッフ・ポートフォリオ</font></td>
<td align="center"><input name="ccusr1" type="checkbox" value="t"<? if ($authinfo["ccusr1_flg"] == "t") {echo(" checked");} ?><? if ($func31 == "f") {echo(" disabled");} ?>></td>
<td align="center">
<input name="ccadm1" type="checkbox" value="t"<? if ($authinfo["ccadm1_flg"] == "t") {echo(" checked");} ?><? if ($func31 == "f") {echo(" disabled");} ?> onclick="setCCUsr1Auth();">
</td>
</tr>
<? } ?>




<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">看護支援</font></td>
<td align="center"><input name="amb" type="checkbox" value="t"<? if ($authinfo["amb_flg"] == "t") {echo(" checked");} ?><? if ($func15 == "f") {echo(" disabled");} ?>></td>
<td align="center"><input name="ambadm" type="checkbox" value="t"<? if ($authinfo["ambadm_flg"] == "t") {echo(" checked");} ?><? if ($func15 == "f") {echo(" disabled");} ?> onclick="setAmbAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 病床管理
echo($_label_by_profile["BED_MANAGE"][$profile_type]);
?></font></td>
<td align="center"><input name="ward" type="checkbox" value="t"<? if ($authinfo["ward_flg"] == "t") {echo(" checked");} ?><? if ($func2 == "f") {echo(" disabled");} ?>></td>
<td align="center">
<? if ($profile_type != "2") { ?>
<input name="ward_reg" type="checkbox" value="t"<? if ($authinfo["ward_reg_flg"] == "t") {echo(" checked");} ?><? if ($func2 == "f") {echo(" disabled");} ?> onclick="setWardAuth();">
<? } ?>
</td>
</tr>
<? if ($profile_type != "2") { ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 診療科／担当部門
echo($_label_by_profile["SECTION"][$profile_type]);
?></font></td>
<td></td>
<td align="center">
<input name="entity_flg" type="checkbox" value="t"<? if ($authinfo["entity_flg"] == "t") {echo(" checked");} ?><? if ($func2 == "f" && $func6 == "f") {echo(" disabled");} ?>>
</td>
</tr>
<? } ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">看護観察記録</font></td>
<td align="center"><input name="nlcs" type="checkbox" value="t"<? if ($authinfo["nlcs_flg"] == "t") {echo(" checked");} ?><? if ($func10 == "f") {echo(" disabled");} ?>></td>
<td align="center"><input name="nlcsadm" type="checkbox" value="t"<? if ($authinfo["nlcsadm_flg"] == "t") {echo(" checked");} ?><? if ($func10 == "f") {echo(" disabled");} ?> onclick="setNlcsAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ファントルくん</font></td>
<td align="center"><input name="inci" type="checkbox" value="t"<? if ($authinfo["inci_flg"] == "t") {echo(" checked");} ?><? if ($func5 == "f") {echo(" disabled");} ?>></td>
<td align="center"><input name="rm" type="checkbox" value="t"<? if ($authinfo["rm_flg"] == "t") {echo(" checked");} ?><? if ($func5 == "f") {echo(" disabled");} ?> onclick="setInciAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ファントルくん＋</font></td>
<td align="center"><input name="fplus" type="checkbox" value="t"<? if ($authinfo["fplus_flg"] == "t") {echo(" checked");} ?><? if ($func13 == "f") {echo(" disabled");} ?>></td>
<td align="center"><input name="fplusadm" type="checkbox" value="t"<? if ($authinfo["fplusadm_flg"] == "t") {echo(" checked");} ?><? if ($func13 == "f") {echo(" disabled");} ?> onclick="setFPlusAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">バリテス</font></td>
<td align="center"><input name="manabu" type="checkbox" value="t"<? if ($authinfo["manabu_flg"] == "t") {echo(" checked");} ?><? if ($func12 == "f") {echo(" disabled");} ?>></td>
<td align="center"><input name="manabuadm" type="checkbox" value="t"<? if ($authinfo["manabuadm_flg"] == "t") {echo(" checked");} ?><? if ($func12 == "f") {echo(" disabled");} ?> onclick="setManabuAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// メドレポート／POレポート
echo($report_menu_label);
?></font></td>
<td align="center"><input name="med" type="checkbox" value="t"<? if ($authinfo["med_flg"] == "t") {echo(" checked");} ?><? if ($func6 == "f") {echo(" disabled");} ?>></td>
<td align="center"><input name="medadm" type="checkbox" value="t"<? if ($authinfo["medadm_flg"] == "t") {echo(" checked");} ?><? if ($func6 == "f") {echo(" disabled");} ?> onclick="setMedAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カルテビューワー</font></td>
<td align="center"><input name="kview" type="checkbox" value="t"<? if ($authinfo["kview_flg"] == "t") {echo(" checked");} ?><? if ($func11 == "f") {echo(" disabled");} ?>></td>
<td align="center"><input name="kviewadm" type="checkbox" value="t"<? if ($authinfo["kviewadm_flg"] == "t") {echo(" checked");} ?><? if ($func11 == "f") {echo(" disabled");} ?> onclick="setKViewAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">経営支援</font></td>
<td align="center"><input name="biz" type="checkbox" value="t"<? if ($authinfo["biz_flg"] == "t") {echo(" checked");} ?><? if ($func3 == "f") {echo(" disabled");} ?>></td>
<td></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 勤務シフト作成
echo($shift_menu_label);
?></font></td>
<td align="center"><input name="shift" type="checkbox" value="t"<? if ($authinfo["shift_flg"] == "t") {echo(" checked");} ?><? if ($func9 == "f") {echo(" disabled");} ?>></td>
<td align="center"><input name="shiftadm" type="checkbox" value="t"<? if ($authinfo["shiftadm_flg"] == "t") {echo(" checked");} ?><? if ($func9 == "f") {echo(" disabled");} ?> onclick="setShiftAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務表</font></td>
<td align="center"><input name="shift2" type="checkbox" value="t"<? if ($authinfo["shift2_flg"] == "t") {echo(" checked");} ?><? if ($func18 == "f") {echo(" disabled");} ?>></td>
<td align="center"><input name="shift2adm" type="checkbox" value="t"<? if ($authinfo["shift2adm_flg"] == "t") {echo(" checked");} ?><? if ($func18 == "f") {echo(" disabled");} ?> onclick="setShift2Auth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// クリニカルラダー／CAS
echo(get_cas_title_name());
?></font></td>
<td align="center"><input name="cas" type="checkbox" value="t"<? if ($authinfo["cas_flg"] == "t") {echo(" checked");} ?><? if ($func8 == "f") {echo(" disabled");} ?>></td>
<td align="center"><input name="casadm" type="checkbox" value="t"<? if ($authinfo["casadm_flg"] == "t") {echo(" checked");} ?><? if ($func8 == "f") {echo(" disabled");} ?> onclick="setCasAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">人事管理</font></td>
<td align="center"><input name="jinji" type="checkbox" value="t"<? if ($authinfo["jinji_flg"] == "t") {echo(" checked");} ?><? if ($func16 == "f") {echo(" disabled");} ?>></td>
<td align="center"><input name="jinjiadm" type="checkbox" value="t"<? if ($authinfo["jinjiadm_flg"] == "t") {echo(" checked");} ?><? if ($func16 == "f") {echo(" disabled");} ?> onclick="setJinjiAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">クリニカルラダー</font></td>
<td align="center"><input name="ladder" type="checkbox" value="t"<? if ($authinfo["ladder_flg"] == "t") {echo(" checked");} ?><? if ($func17 == "f") {echo(" disabled");} ?>></td>
<td align="center"><input name="ladderadm" type="checkbox" value="t"<? if ($authinfo["ladderadm_flg"] == "t") {echo(" checked");} ?><? if ($func17 == "f") {echo(" disabled");} ?> onclick="setLadderAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">キャリア開発ラダー</font></td>
<td align="center"><input name="career" type="checkbox" value="t"<? if ($authinfo["career_flg"] == "t") {echo(" checked");} ?><? if ($func19 == "f") {echo(" disabled");} ?>></td>
<td align="center"><input name="careeradm" type="checkbox" value="t"<? if ($authinfo["careeradm_flg"] == "t") {echo(" checked");} ?><? if ($func19 == "f") {echo(" disabled");} ?> onclick="setCareerAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日報・月報</font></td>
<td align="center"><input name="jnl" type="checkbox" value="t"<? if ($authinfo["jnl_flg"] == "t") {echo(" checked");} ?><? if ($func14 == "f") {echo(" disabled");} ?>></td>
<td align="center"><input name="jnladm" type="checkbox" value="t"<? if ($authinfo["jnladm_flg"] == "t") {echo(" checked");} ?><? if ($func14 == "f") {echo(" disabled");} ?> onclick="setJnlAuth();"></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="8"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff">
<td></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ユーザ機能</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">月間・年間<? echo($intra_menu1); ?>参照</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">イントラメニュー</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 稼働状況統計
echo($intra_menu1);
?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 当直・外来分担表
echo($intra_menu2_4);
?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 私たちの生活サポート
echo($intra_menu3);
?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">イントラネット</font></td>
<td align="center"><input name="intra" type="checkbox" value="t"<? if ($authinfo["intra_flg"] == "t") {echo(" checked");} ?><? if ($func4 == "f") {echo(" disabled");} ?>></td>
<td align="center"><input type="checkbox" name="ymstat" value="t"<? if ($authinfo["ymstat_flg"] == "t") {echo(" checked");} ?><? if ($func4 == "f") {echo(" disabled");} ?> onclick="setIntraAuth();"></td>
<td align="center"><input name="intram" type="checkbox" value="t"<? if ($authinfo["intram_flg"] == "t") {echo(" checked");} ?><? if ($func4 == "f") {echo(" disabled");} ?> onclick="setIntraAuth();"></td>
<td align="center"><input name="stat" type="checkbox" value="t"<? if ($authinfo["stat_flg"] == "t") {echo(" checked");} ?><? if ($func4 == "f") {echo(" disabled");} ?> onclick="setIntraAuth();"></td>
<td align="center"><input name="allot" type="checkbox" value="t"<? if ($authinfo["allot_flg"] == "t") {echo(" checked");} ?><? if ($func4 == "f") {echo(" disabled");} ?> onclick="setIntraAuth();"></td>
<td align="center"><input name="life" type="checkbox" value="t"<? if ($authinfo["life_flg"] == "t") {echo(" checked");} ?><? if ($func4 == "f") {echo(" disabled");} ?> onclick="setIntraAuth();"></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="更新"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="group_id" value="<? echo($group_id); ?>">
</form>
<? } ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
