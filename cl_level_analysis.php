<?
//ini_set("display_errors","1");
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("show_select_values.ini");
require_once("cl_application_common.ini");
require_once("cl_common.ini");
require_once("cl_application_workflow_common_class.php");
require_once("cl_level_analysis_util.php");
require_once("cl_title_name.ini");
require_once("cl_common_log_class.inc");
require_once("cl_smarty_setting.ini");
require_once("Cmx.php");
require_once("MDB2.php");

$log = new cl_common_log_class(basename(__FILE__));

$log->info(basename(__FILE__)." START");

$smarty = cl_get_smarty_obj();
$smarty->template_dir = dirname(__FILE__) . "/cl/view";

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,$CAS_MENU_AUTH,$fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
// ワークフロー権限の取得
$workflow_auth = check_authority($session, $CL_MANAGE_AUTH, $fname);

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$obj = new cl_application_workflow_common_class($con, $fname);

$cl_title = cl_title_name();

/**
 * アプリケーションメニューHTML取得
 */
$aplyctn_mnitm_str=get_applycation_menuitem($session,$fname);

/**
 * 志願申請、評価申請用の志願者取得（申請者名）
 */
$arr_emp = $obj->get_empmst($session);
$emp_id  = $arr_emp["0"]["emp_id"];

/**
 * ラダーレベル
 */
$option_level = get_select_levels($_POST['level']);

/**
 * 分析項目（第一階層）
 */
$option_analysis_category = get_select_analysis_category($_POST['analysis_category']);

/**
 * 分析項目（第二階層）
 */
if ($_POST['analysis_category'] != "") {
	$item_display = true;
	$option_analysis_items = get_select_analysis_item($_POST['analysis_category'], $_POST['analysis_items']);
} else {
	$item_display = false;
}

/**
 * 分析項目（年度）
 */
if ($_POST['analysis_category'] != "") {
	switch($_POST['analysis_items']){
		case ITEM_UNIT_YEAR_EMP_CNT_ID:
		case ITEM_UNIT_RECOGNITION_YEAR_EMP_CNT_ID:
		case ITEM_YEAR_AVERAGE_EVALUATION_POINT_ID:
			$year_from_display = true;
			$option_year_from = cl_get_select_years($_POST['year_from'],true);
			break;
		case ITEM_UNIT_AVERAGE_EVALUATION_POINT_ID:
			$year_from_display = true;
			$option_year_from = cl_get_select_years($_POST['year_from'],false);
			if ($_POST['year_from'] == '') {
				$year_from_view = date(Y);
			}
			break;
		default:
			$year_from_display = false;
			break;
	}
} else {
	$year_from_display = false;
}

if (
	$_POST['analysis_category'] != ""
	&& in_array(
		$_POST['analysis_items']
		,array(
			ITEM_UNIT_YEAR_EMP_CNT_ID
			,ITEM_UNIT_RECOGNITION_YEAR_EMP_CNT_ID
			,ITEM_YEAR_AVERAGE_EVALUATION_POINT_ID
		)
	)
) {
	$year_to_display = true;
	$option_year_to = cl_get_select_years($_POST['year_to'],false);
	if ($_POST['year_to'] == '') {
		$year_to_view = date(Y);
	}
} else {
	$year_to_display = false;
}

$date_update_flg = $_POST['date_update_flg'];
$emp_date = $_POST['emp_date'];

/**
 * Javascriptコード生成（動的オプション項目変更用）
 */
$js_str = get_js_str($arr_wkfwcatemst,$workflow);

/**
 * データベースクローズ
 */
pg_close($con);

//##########################################################################
// データベース接続オブジェクト取得
//##########################################################################
$log->debug("MDB2オブジェクト取得開始",__FILE__,__LINE__);
$mdb2 = MDB2::connect(CMX_DB_DSN);
if (PEAR::isError($mdb2)) {
	$log->error("MDB2オブジェクト取得エラー：".$mdb2->getDebugInfo(),__FILE__,__LINE__);
}
$log->debug("MDB2オブジェクト取得終了",__FILE__,__LINE__);

require_once("cl/model/search/jobmst_model.php");
$jobmst_model = new jobmst_model($mdb2,$emp_id);
$log->debug("役職マスタ用DBA取得",__FILE__,__LINE__);

// 2012/08/31 Yamagawa upd(s)
//$arr_job = $jobmst_model->getList();
$arr_job = $jobmst_model->getList_nurse();
// 2012/08/31 Yamagawa upd(e)
$option_job = get_select_job($arr_job, $_POST['job']);

require_once(dirname(__FILE__) . "/cl/model/search/classname_model.php");
$classname_model = new classname_model($mdb2, $emp_id);
$log->debug("組織名称DBA取得",__FILE__,__LINE__);

require_once("cl/model/search/classmst_model.php");
$classmst_model = new classmst_model($mdb2,$emp_id);
$log->debug("施設マスタ用DBA取得",__FILE__,__LINE__);

require_once("cl/model/search/atrbmst_model.php");
$atrbmst_model = new atrbmst_model($mdb2,$emp_id);
$log->debug("部署マスタ用DBA取得",__FILE__,__LINE__);

require_once("cl/model/search/deptmst_model.php");
$deptmst_model = new deptmst_model($mdb2,$emp_id);
$log->debug("課・科マスタ用DBA取得",__FILE__,__LINE__);

require_once("cl/model/search/classroom_model.php");
$classroom_model = new classroom_model($mdb2,$emp_id);
$log->debug("室マスタ用DBA取得",__FILE__,__LINE__);

$class_division = get_class_division($mdb2, $emp_id);

// 組織階層取得
$arr_classname = $classname_model->getClassname();
$class_cnt = $arr_classname['class_cnt'];

if ($class_division == 0){

	// 全施設取得
	$arr_class = $classmst_model->getList();

	// 全部署取得
	$arr_atrb = $atrbmst_model->getList();

	// 全課・科取得
	$arr_dept = $deptmst_model->getList();

	if ($class_cnt >= 4) {
		// 全室取得
		$arr_room = $classroom_model->getList();
	}

} else {

	// 施設取得
	$arr_class = $classmst_model->getList_add_conditions_emp_id($emp_id);

	// 部署取得
	$arr_atrb = $atrbmst_model->getList_add_conditions_emp_id($class_division, $emp_id);

	// 課・課取得
	$arr_dept = $deptmst_model->getList_add_conditions_emp_id($class_division, $emp_id);

	if ($class_cnt >= 4) {
		// 全室取得
		$arr_room = $classroom_model->getList_add_conditions_emp_id($class_division, $emp_id);
	}
}

if ($_POST['affiliation_row_count'] == '') {

	$affiliation[0]['rowno'] = 0;
	$affiliation[0]['option_class'] = get_select_class($arr_class,'');
	$affiliation[0]['class_id_view'] = '全て';
	$affiliation[0]['option_atrb'] = get_select_atrb(array(),'');
	$affiliation[0]['atrb_id_view'] = '全て';
	$affiliation[0]['option_dept'] = get_select_dept(array(),'');
	$affiliation[0]['dept_id_view'] = '全て';
	if ($class_cnt >= 4) {
		$affiliation[0]['option_room'] = get_select_room(array(),'');
		$affiliation[0]['room_id_view'] = '全て';
	}
	$affiliation_row_count = 1;

} else {

	// 削除された分のインデックスを詰める
	for ($i = 0; $i < $_POST['affiliation_row_max_count'] - $_POST['affiliation_row_count']; $i++){
		for ($j = $_POST['affiliation_row_max_count'] - 1; $j > 0; $j--){
			if ($class_id[$j - 1] == '') {
				$class_id[$j - 1] = $class_id[$j];
				$class_id_view[$j - 1] = $class_id_view[$j];
				$atrb_id[$j - 1] = $atrb_id[$j];
				$atrb_id_view[$j - 1] = $atrb_id_view[$j];
				$dept_id[$j - 1] = $dept_id[$j];
				$dept_id_view[$j - 1] = $dept_id_view[$j];
				$room_id[$j - 1] = $room_id[$j];
				$room_id_view[$j - 1] = $room_id_view[$j];
				break;
			}
		}
	}

	// 所属行分ループ
	for ($i = 0; $i < $_POST['affiliation_row_count']; $i++){

		$affiliation[$i]['rowno'] = $i;

		// 施設検索
		$affiliation[$i]['option_class'] = get_select_class($arr_class,$class_id[$i]);
		$affiliation[$i]['class_id_view'] = $class_id_view[$i];

		if ($class_id[$i] == ''){
			$affiliation[$i]['option_atrb'] = get_select_atrb(array(),'');
			$affiliation[$i]['atrb_id_view'] = '全て';
			$atrb_id[$i] = '';
		} else {
			// 施設検索時、部署検索
			$arr_atrb_wk = extraction_atrb_options($arr_atrb ,$class_id[$i]);
			$affiliation[$i]['option_atrb'] = get_select_atrb($arr_atrb_wk,$atrb_id[$i]);
			$affiliation[$i]['atrb_id_view'] = $atrb_id_view[$i];
			// 2012/07/12 Yamagawa add(s)
			$bol_exists = false;
			foreach ($arr_atrb_wk as $atrb_wk) {
				if ($atrb_wk['atrb_id'] == $atrb_id[$i]) {
					$bol_exists = true;
					break;
				}
			}

			if (!$bol_exists) {
				$atrb_id[$i] = '';
			}
			// 2012/07/12 Yamagawa add(e)
		}

		if ($atrb_id[$i] == ''){
			$affiliation[$i]['option_dept'] = get_select_dept(array(),'');
			$affiliation[$i]['dept_id_view'] = '全て';
			$dept_id[$i] == '';
		} else {
			// 部署選択時、課・科検索
			$arr_dept_wk = extraction_dept_options($arr_dept ,$atrb_id[$i]);
			$affiliation[$i]['option_dept'] = get_select_dept($arr_dept_wk,$dept_id[$i]);
			$affiliation[$i]['dept_id_view'] = $dept_id_view[$i];
			// 2012/07/12 Yamagawa add(s)
			$bol_exists = false;
			foreach ($arr_dept_wk as $dept_wk) {
				if ($dept_wk['dept_id'] == $dept_id[$i]) {
					$bol_exists = true;
					break;
				}
			}

			if (!$bol_exists) {
				$dept_id[$i] = '';
			}
			// 2012/07/12 Yamagawa add(e)
		}

		if ($class_cnt >= 4) {
			if ($dept_id[$i] == ''){
				$affiliation[$i]['option_room'] = get_select_room(array(),'');
				$affiliation[$i]['room_id_view'] = '全て';
			} else {
				// 課・科選択時、室を検索
				$arr_room_wk = extraction_room_options($arr_room ,$dept_id[$i]);
				$affiliation[$i]['option_room'] = get_select_room($arr_room_wk,$room_id[$i]);
				$affiliation[$i]['room_id_view'] = $room_id_view[$i];
			}
		}
	}

	$affiliation_row_count = $_POST['affiliation_row_count'];

	if ($_POST['mode'] == 'add_affiliation'){
		$affiliation[$affiliation_row_count]['rowno'] = $affiliation_row_count;
		$affiliation[$affiliation_row_count]['option_class'] = get_select_class($arr_class,'');
		$affiliation[$affiliation_row_count]['class_id_view'] = '全て';
		$affiliation[$affiliation_row_count]['option_atrb'] = get_select_atrb(array(),'');
		$affiliation[$affiliation_row_count]['atrb_id_view'] = '全て';
		$affiliation[$affiliation_row_count]['option_dept'] = get_select_dept(array(),'');
		$affiliation[$affiliation_row_count]['dept_id_view'] = '全て';
		if ($class_cnt >= 4) {
			$affiliation[$affiliation_row_count]['option_room'] = get_select_room(array(),'');
			$affiliation[$affiliation_row_count]['room_id_view'] = '全て';
		}
		$affiliation_row_count++;
	}
}

$affiliation_row_max_count = $affiliation_row_count;

if ($_POST['mode'] == 'analysis') {
	$data = get_analysis_data($mdb2, $emp_id);
	$log->debug("data_start");
	$log->debug(print_r($data,true));
	$log->debug("data_end");
}

// DB切断
$mdb2->disconnect();
$log->debug("DB切断",__FILE__,__LINE__);

/**
 * テンプレートマッピング
 */

$smarty->assign( 'cl_title'						, $cl_title						);
$smarty->assign( 'session'						, $session						);
$smarty->assign( 'login_emp_id'					, $emp_id						);
$smarty->assign( 'yui_cal_part'					, $yui_cal_part					);
$smarty->assign( 'workflow_auth'				, $workflow_auth				);
$smarty->assign( 'aplyctn_mnitm_str'			, $aplyctn_mnitm_str			);
$smarty->assign( 'js_str'						, $js_str						);

$smarty->assign( 'option_job'					, $option_job					);
$smarty->assign( 'option_level'					, $option_level					);
$smarty->assign( 'option_analysis_category'		, $option_analysis_category		);
$smarty->assign( 'item_display'					, $item_display					);
$smarty->assign( 'option_analysis_items'		, $option_analysis_items		);
$smarty->assign( 'year_from_display'			, $year_from_display			);
$smarty->assign( 'option_year_from'				, $option_year_from				);
$smarty->assign( 'year_to_display'				, $year_to_display				);
$smarty->assign( 'option_year_to'				, $option_year_to				);

$smarty->assign( 'job_view'						, $job_view						);
$smarty->assign( 'level_view'					, $level_view					);
$smarty->assign( 'analysis_category_view'		, $analysis_category_view		);
$smarty->assign( 'analysis_items_view'			, $analysis_items_view			);
$smarty->assign( 'year_from_view'				, $year_from_view				);
$smarty->assign( 'year_to_view'					, $year_to_view					);

$smarty->assign( 'class_cnt'					, $class_cnt					);
$smarty->assign( 'affiliation'					, $affiliation					);
$smarty->assign( 'affiliation_row_count'		, $affiliation_row_count		);
$smarty->assign( 'affiliation_row_max_count'	, $affiliation_row_max_count	);
$smarty->assign( 'data'							, $data							);

$smarty->assign( 'date_update_flg'				, $date_update_flg				);
$smarty->assign( 'emp_date'						, $emp_date						);

$log->debug("テンプレートマッピング",__FILE__,__LINE__);

/**
 * テンプレート出力
 */
$log->debug("テンプレート：".basename($_SERVER['PHP_SELF'],'.php').".tpl",__FILE__,__LINE__);
$smarty->display(basename($_SERVER['PHP_SELF'],'.php').".tpl");
$log->debug("テンプレート出力",__FILE__,__LINE__);

if ($date_update_flg == 'update'){
	echo("<script language=\"javascript\">document.ladder_form.date_update_flg.value = '';</script>");
	echo("<script language=\"javascript\">output_to_print();</script>");
}

/**
 * Javascript文字列取得
 */
function get_js_str($arr_wkfwcatemst,$workflow)
{
	$str_buff[]  = "";
	foreach ($arr_wkfwcatemst as $tmp_cate_id => $arr) {
		$str_buff[] .= "\n\t\tif (cate_id == '".$tmp_cate_id."') {\n";


		 foreach($arr["wkfw"] as $tmp_wkfw_id => $tmp_wkfw_nm){
			$tmp_wkfw_nm = htmlspecialchars($tmp_wkfw_nm, ENT_QUOTES);
			$tmp_wkfw_nm = str_replace("&#039;", "\'", $tmp_wkfw_nm);
			$tmp_wkfw_nm = str_replace("&quot;", "\"", $tmp_wkfw_nm);

			$str_buff[] .= "\t\t\taddOption(obj_wkfw, '".$tmp_wkfw_id."', '".$tmp_wkfw_nm."',  '".$workflow."');\n";

		}

		$str_buff[] .= "\t\t}\n";
	}

	return implode('', $str_buff);
}

/**
 * 年オプションHTML取得
 */
function cl_get_select_years($date, $bolblank)
{
	ob_start();
	show_select_years(10, $date, $bolblank);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

/**
 * 職種オプションHTML取得
 */
function get_select_job($arr_job, $job_id){
	ob_start();
	show_job_options($arr_job, $job_id);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

function show_job_options($arr_job, $job_id){
	echo("<option value=\"\">");
	for($i=0;$i<count($arr_job);$i++) {
		echo("<option value=\"".$arr_job[$i]['job_id']."\"");
		if($job_id == $arr_job[$i]['job_id']) {
			echo(" selected");
		}
		echo(">".$arr_job[$i]['job_nm']."\n");
	}
}

/**
 * 施設オプションHTML取得
 */
function get_select_class($arr_class ,$class_id){
	ob_start();
	show_class_options($arr_class ,$class_id);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

function show_class_options($arr_class, $class_id){
	echo("<option value=\"\">全て\n");
	for($i=0;$i<count($arr_class);$i++) {
		echo("<option value=\"".$arr_class[$i]['class_id']."\"");
		if($class_id == $arr_class[$i]['class_id']) {
			echo(" selected");
		}
		echo(">".$arr_class[$i]['class_nm']."\n");
	}
}

/**
 * 施設選択時JS取得
 */
/*
function get_class_options_change($arr_atrb,$bol_room){
	ob_start();
	show_class_options_change($arr_atrb, $bol_room);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

function show_class_options_change($arr_atrb, $bol_room){

	if ($bol_room) {
		echo("function class_change(class_obj,atrb_obj,dept_obj,room_obj){");
	} else {
		echo("function class_change(class_obj,atrb_obj,dept_obj){");
	}

	echo("deleteAllOptionsPost(atrb_obj);");
	echo("deleteAllOptionsPost(dept_obj);");

	if ($bol_room) {
		echo("deleteAllOptionsPost(room_obj);");
	}

	for($i=0;$i<count($arr_atrb);$i++){
		echo("if (class_obj.value == '".$arr_atrb[$i]['class_id']."') {");
		echo("addOptionPost(atrb_obj,'".$arr_atrb[$i]['atrb_id']."','".$arr_atrb[$i]['atrb_nm']."',false);");
		echo("}");
	}

	echo("}");
}
*/

/**
 * 所属オプションHTML取得
 */
function get_select_atrb($arr_atrb ,$atrb_id){
	ob_start();
	show_atrb_options($arr_atrb ,$atrb_id);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

function show_atrb_options($arr_atrb, $atrb_id){
	echo("<option value=\"\">全て\n");
	for($i=0;$i<count($arr_atrb);$i++) {
		echo("<option value=\"".$arr_atrb[$i]['atrb_id']."\"");
		if($atrb_id == $arr_atrb[$i]['atrb_id']) {
			echo(" selected");
		}
		echo(">".$arr_atrb[$i]['atrb_nm']."\n");
	}
}

function extraction_atrb_options($p_arr_atrb, $class_id) {

	$arr_atrb = array();
	for ($i = 0; $i < count($p_arr_atrb); $i++) {
		if ($class_id == $p_arr_atrb[$i]['class_id']) {
			array_push($arr_atrb ,$p_arr_atrb[$i]);
		}
	}
	return $arr_atrb;

}

/**
 * 所属選択時JS取得
 */
/*
function get_atrb_options_change($arr_dept, $bol_room){
	ob_start();
	show_atrb_options_change($arr_dept, $bol_room);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

function show_atrb_options_change($arr_dept, $bol_room){

	if ($bol_room) {
		echo("function atrb_change(atrb_obj,dept_obj,room_obj){");
	} else {
		echo("function atrb_change(atrb_obj,dept_obj){");
	}

	echo("deleteAllOptionsPost(dept_obj);");

	if ($bol_room) {
		echo("deleteAllOptionsPost(room_obj);");
	}

	for($i=0;$i<count($arr_dept);$i++){
		echo("if (atrb_obj.value == '".$arr_dept[$i]['atrb_id']."') {");
		echo("addOptionPost(dept_obj,'".$arr_dept[$i]['dept_id']."','".$arr_dept[$i]['dept_nm']."',false);");
		echo("}");
	}

	echo("}");
}
*/

/**
 * 課・科オプションHTML取得
 */
function get_select_dept($arr_dept ,$dept_id){
	ob_start();
	show_dept_options($arr_dept ,$dept_id);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

function show_dept_options($arr_dept, $dept_id){
	echo("");
	echo("<option value=\"\">全て\n");
	for($i=0;$i<count($arr_dept);$i++) {
		echo("<option value=\"".$arr_dept[$i]['dept_id']."\"");
		if($dept_id == $arr_dept[$i]['dept_id']) {
			echo(" selected");
		}
		echo(">".$arr_dept[$i]['dept_nm']."\n");
	}
}

function extraction_dept_options($p_arr_dept, $atrb_id) {

	$arr_dept = array();
	for ($i = 0; $i < count($p_arr_dept); $i++) {
		if ($atrb_id == $p_arr_dept[$i]['atrb_id']) {
			array_push($arr_dept ,$p_arr_dept[$i]);
		}
	}
	return $arr_dept;

}

/**
 * 課・科選択時JS取得
 */
/*
function get_dept_options_change($arr_room){
	ob_start();
	show_dept_options_change($arr_room);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

function show_dept_options_change($arr_room){
	echo("function dept_change(dept_obj,room_obj){");
	echo("deleteAllOptionsPost(room_obj);");
	for($i=0;$i<count($arr_room);$i++){
		echo("if (dept_obj.value == '".$arr_room[$i]['dept_id']."') {");
		echo("addOptionPost(room_obj,'".$arr_room[$i]['room_id']."','".$arr_room[$i]['room_nm']."',false);");
		echo("}");
	}
	echo("}");
}
*/

/**
 * 室オプションHTML取得
 */
function get_select_room($arr_room ,$room_id){
	ob_start();
	show_room_options($arr_room ,$room_id);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

function show_room_options($arr_room, $room_id){
	echo("<option value=\"\">全て\n");
	for($i=0;$i<count($arr_room);$i++) {
		echo("<option value=\"".$arr_room[$i]['room_id']."\"");
		if($room_id == $arr_room[$i]['room_id']) {
			echo(" selected");
		}
		echo(">".$arr_room[$i]['room_nm']."\n");
	}
}

function extraction_room_options($p_arr_room, $dept_id) {

	$arr_room = array();
	for ($i = 0; $i < count($p_arr_room); $i++) {
		if ($dept_id == $p_arr_room[$i]['dept_id']) {
			array_push($arr_room ,$p_arr_room[$i]);
		}
	}
	return $arr_room;

}

/**
 * 分析項目（第一階層）HTML取得
 */
function get_select_analysis_category($category){
	ob_start();
	show_analysis_category($category);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

function show_analysis_category($category){

	$arr_categorys_nm = array(
		""
		,CATEGORY_LADDER_INFO_NM
		,CATEGORY_PERSONNEL_AFFAIRS_INFO_NM
	);

	$arr_categorys_id = array(
		""
		,CATEGORY_LADDER_INFO_ID
		,CATEGORY_PERSONNEL_AFFAIRS_INFO_ID
	);

	for($i=0;$i<count($arr_categorys_nm);$i++) {

		echo("<option value=\"$arr_categorys_id[$i]\"");
		if($category == $arr_categorys_id[$i]) {
			echo(" selected");
		}
		echo(">$arr_categorys_nm[$i]\n");
	}

}

/**
 * 分析項目（第一階層）HTML取得
 */
function get_select_analysis_item($category ,$item){
	ob_start();
	show_analysis_item($category ,$item);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

function show_analysis_item($category ,$item){

	switch($category){

		case CATEGORY_LADDER_INFO_ID:

			$arr_items_nm = array(
				""
				,ITEM_UNIT_EMP_CNT_NM
				,ITEM_UNIT_YEAR_EMP_CNT_NM
				,ITEM_UNIT_RECOGNITION_YEAR_EMP_CNT_NM
				,ITEM_RECOGNITION_EMP_CNT_NM
				,ITEM_UNIT_AVERAGE_EVALUATION_POINT_NM
				,ITEM_YEAR_AVERAGE_EVALUATION_POINT_NM
			);

			$arr_items_id = array(
				""
				,ITEM_UNIT_EMP_CNT_ID
				,ITEM_UNIT_YEAR_EMP_CNT_ID
				,ITEM_UNIT_RECOGNITION_YEAR_EMP_CNT_ID
				,ITEM_RECOGNITION_EMP_CNT_ID
				,ITEM_UNIT_AVERAGE_EVALUATION_POINT_ID
				,ITEM_YEAR_AVERAGE_EVALUATION_POINT_ID
			);

			break;

		case CATEGORY_PERSONNEL_AFFAIRS_INFO_ID:

			$arr_items_nm = array(
				""
				,ITEM_LICENSE_ELAPSED_YEAR_EMP_CNT_NM
				,ITEM_TENURE_EMP_CNT_NM
				,ITEM_ASSIGNMENT_YEAR_EMP_CNT_NM
				,ITEM_GRADUATION_EMP_CNT_NM
				,ITEM_GRADUATION_ELAPSED_YEAR_EMP_CNT_NM
			);

			$arr_items_id = array(
				""
				,ITEM_LICENSE_ELAPSED_YEAR_EMP_CNT_ID
				,ITEM_TENURE_EMP_CNT_ID
				,ITEM_ASSIGNMENT_YEAR_EMP_CNT_ID
				,ITEM_GRADUATION_EMP_CNT_ID
				,ITEM_GRADUATION_ELAPSED_YEAR_EMP_CNT_ID
			);

			break;

	}

	for($i=0;$i<count($arr_items_nm);$i++) {

		echo("<option value=\"$arr_items_id[$i]\"");
		if($item == $arr_items_id[$i]) {
			echo(" selected");
		}
		echo(">$arr_items_nm[$i]\n");
	}

}

/**
 * アプリケーションメニューHTML取得
 */
function get_applycation_menuitem($session,$fname)
{
	ob_start();
	show_applycation_menuitem($session,$fname,"");
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}
/**
 * 必須レベルオプションHTML取得
 */
function get_select_levels($level){

	ob_start();
	show_levels_options($level);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;

}

function show_levels_options($level) {

	$arr_levels_nm = array("","I","II","III","IV","V");
	$arr_levels_id = array("","1","2","3","4","5");

	for($i=0;$i<count($arr_levels_nm);$i++) {

		echo("<option value=\"$arr_levels_id[$i]\"");
		if($level == $arr_levels_id[$i]) {
			echo(" selected");
		}
		echo(">$arr_levels_nm[$i]\n");
	}
}

$log->info(basename(__FILE__)." END");
$log->shutdown();
