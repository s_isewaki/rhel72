<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
//ファイルの読み込み
require("about_postgres.php");
require("about_session.php");
require("about_authority.php");
require("./get_values.ini");



//ページ名
$fname = $PHP_SELF;

//==============================
//セッションチェック
//==============================
$session = qualify_session($session,$fname);
if($session == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
//権限チェック
//==============================
$dept = check_authority($session,57,$fname);
if($dept == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
// 画面パラメータの整理
//==============================
if($mode != "insert_select" && $mode != "update")
{
	$mode = "insert";
}

//==============================
//ＤＢのコネクション作成(トランザクション開始)
//==============================
$con = connect2db($fname);
pg_query($con, "begin transaction");

//==============================
//送信データをＤＢデータに変換
//==============================
$ptif_id_sql = $pt_id;
$problem_id_sql = $problem_id;//新規の場合は後で採番する。
$problem_list_no_sql = $problem_list_no;
$create_date_sql = $create_date_year.$create_date_month.$create_date_day;
$problem_sql = pg_escape_string($problem);
$emp_id_sql = $emp_id;
$problem_class_sql = $problem_class;
$problem_kind_sql = $problem_kind;
if(@$disease_name_flg=="t"){
	$disease_name_flg_sql = "t";
} else {
	$disease_name_flg_sql = "f";
}
$start_date_sql = $start_date_year.$start_date_month.$start_date_day;
if($start_date_sql == "---")
{
	$start_date_sql = "";
}
$end_date_sql = $end_date_year.$end_date_month.$end_date_day;
if($end_date_sql == "---")
{
	$end_date_sql = "";
}
$memo_sql = pg_escape_string($memo);
if($other_editable=="t")
{
	$other_editable_sql = "t";
}
else
{
	$other_editable_sql = "f";
}
$icd10_sql = pg_escape_string($icd10);
$byoumei_code_sql = pg_escape_string($byoumei_code);


//echo($ptif_id_sql."/".$problem_id_sql."/".$problem_list_no_sql."/".$create_date_sql."/".$problem_sql."/".$emp_id_sql."/".$problem_class_sql."/".$problem_kind_sql."/".$disease_name_flg_sql."/".$start_date_sql."/".$end_date_sql."/".$memo_sql);
//exit;

//==============================
//入力チェック
//==============================
if($problem_sql == "")
{
	echo("<script language=\"javascript\">alert(\"プロブレムを入力してください\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}

// 出現期日
if($start_date_sql == "") {
    echo("<script language=\"javascript\">alert(\"出現期日を入力してください\");</script>\n");
    echo("<script language=\"javascript\">history.back();</script>\n");
    exit;
} elseif (!isValidDateInputPattern($start_date_year, $start_date_month, $start_date_day)) {
    $message = "出現期日には、年月日を全て入力するか年のみを入力、または年月を入力してください";
    echo("<script language=\"javascript\">alert(\"{$message}\");</script>\n");
    echo("<script language=\"javascript\">history.back();</script>\n");
    exit;
}

if (strlen($start_date_sql) == 8    // 日付の妥当性チェック(日付が全て入力されている場合のみ)
        && !checkdate($start_date_month, $start_date_day, $start_date_year)) {
    echo("<script language=\"javascript\">alert(\"出現期日が正しくありません\");</script>\n");
    echo("<script language=\"javascript\">history.back();</script>\n");
    exit;
}

// 解決日
if ($end_date_sql != "" && !isValidDateInputPattern($end_date_year, $end_date_month, $end_date_day)) {
    $message = "解決日には、年月日を全て入力するか年のみを入力、または年月を入力してください";
    echo("<script language=\"javascript\">alert(\"{$message}\");</script>\n");
    echo("<script language=\"javascript\">history.back();</script>\n");
    exit;
}

if (strlen($end_date_sql) == 8    // 日付の妥当性チェック(日付が全て入力されている場合のみ)
        && !checkdate($end_date_month, $end_date_day, $end_date_year)) {
    echo("<script language=\"javascript\">alert(\"解決日が正しくありません\");</script>\n");
    echo("<script language=\"javascript\">history.back();</script>\n");
    exit;
}

// 出現期日と解決日の大小チェック(日付が全て入力されている場合のみ)
if (strlen($start_date_sql) == 8 && strlen($end_date_sql) == 8) {
    if (strtotime($start_date_sql) > strtotime($end_date_sql)) {
        echo("<script language=\"javascript\">alert(\"解決日が出現期日より前の日付です\");</script>\n");
        echo("<script language=\"javascript\">history.back();</script>\n");
        exit;
    }
}

if($problem_list_no_sql == "")
{
	echo("<script language=\"javascript\">alert(\"番号を入力してください\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}
if(ereg("[^0-9]", $problem_list_no_sql))
{
	echo("<script language=\"javascript\">alert(\"番号は数値を入力してください\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}


//==============================
//更新前のデータ取得 (更新時)
//==============================
if($mode == "update")
{
	$tbl = "select * from summary_problem_list ";
	$cond = "where ptif_id = '$pt_id' and problem_id = '$problem_id'";
	$sel = select_from_table($con,$tbl,$cond,$fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$old_db_record = pg_fetch_all($sel);
	if(!$old_db_record)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$old_db_record = $old_db_record[0];
}



//==============================
//番号重複チェック
//==============================
$tbl = "select count(*) from summary_problem_list ";
$cond = "where (ptif_id = '$pt_id') and (problem_list_no = '$problem_list_no_sql')";
if($mode == "update")
{
	$cond .= " and not (problem_id = '$problem_id')";
}
$sel = select_from_table($con,$tbl,$cond,$fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$num=pg_result($sel,0,"count");
if($num > 0)
{
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script language=\"javascript\">alert(\"番号が登録済みです\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}

//==============================
//プロブレム重複チェック
//==============================
$tbl = "select count(*) from summary_problem_list ";
$cond = "where (ptif_id = '$pt_id') and (problem = '$problem_sql')";
if($mode == "update")
{
	$cond .= " and not (problem_id = '$problem_id')";
}
$sel = select_from_table($con,$tbl,$cond,$fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$num=pg_result($sel,0,"count");
if($num > 0)
{
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script language=\"javascript\">alert(\"同一プロブレム名があります\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}

//==============================
//プロブレム使用中チェック (更新時)
//==============================
if($mode == "update")
{
	$tbl = "select count(*) from summary ";
	$cond = "where (ptif_id = '$pt_id') and (problem_id = '$problem_id')";
	$sel = select_from_table($con,$tbl,$cond,$fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$num=pg_result($sel,0,"count");
	if($num > 0)
	{
		//プロブレムが使用されている場合
		//番号は更新不可能。
		if($old_db_record["problem_list_no"] != $problem_list_no)
		{
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script language=\"javascript\">alert(\"このプロブレムは使用されているため番号は変更できません\");</script>\n");
			echo("<script language=\"javascript\">history.back();</script>\n");
			exit;
		}

		/*
		$tbl = "select count(*) from summary_problem_list ";
		$cond = "where (ptif_id = '$pt_id') and (problem_id = '$problem_id')"
		       ." and not (problem_list_no = '$problem_list_no_sql')";
		$sel = select_from_table($con,$tbl,$cond,$fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$num=pg_result($sel,0,"count");
		if($num > 0)
		{
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script language=\"javascript\">alert(\"このプロブレムは使用されているため番号は変更できません\");</script>\n");
			echo("<script language=\"javascript\">history.back();</script>\n");
			exit;
		}
		*/
	}
}

//==========================================================================================
//プロブレムリストテーブルに登録 (新規)
//==========================================================================================

if($mode == "insert" || $mode == "insert_select")
{
	//==============================
	//$problem_idを採番
	//==============================
	$tbl = "select max(problem_id) from summary_problem_list";
	$cond = "where (ptif_id = '$pt_id')";
	$maxsel = select_from_table($con,$tbl,$cond,$fname);
	if($maxsel == 0){
		$problem_id=1;
	}else{
		$problem_id=pg_result($maxsel,0,"max") + 1;
	}
	$problem_id_sql = $problem_id;

	//==============================
	//DB登録
	//==============================
	$content = array($ptif_id_sql,$problem_id_sql,$problem_list_no_sql,$create_date_sql,$problem_sql,$emp_id_sql,$problem_class_sql,$problem_kind_sql,$disease_name_flg_sql,$start_date_sql,$end_date_sql,$memo_sql,$other_editable_sql,$icd10_sql,$byoumei_code_sql);
	$intbl = "insert into summary_problem_list (ptif_id,problem_id,problem_list_no,create_date,problem,emp_id,problem_class,problem_kind,disease_name_flg,start_date,end_date,memo,other_editable,icd10,byoumei_code) values(";
	$in = insert_into_table($con,$intbl,$content,$fname);
	if($in == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
}

//==========================================================================================
//プロブレムリストテーブルを更新 (更新)
//==========================================================================================
else//($mode == "update")
{
	//==============================
	//DB更新
	//==============================
	//※"create_date","emp_id"は更新しない。
	$sql = "update summary_problem_list set";
	$set = array("problem_list_no","problem","problem_class","problem_kind","disease_name_flg","start_date","end_date","memo","other_editable","icd10","byoumei_code");
	$setvalue = array($problem_list_no_sql,$problem_sql,$problem_class_sql,$problem_kind_sql,$disease_name_flg_sql,$start_date_sql,$end_date_sql,$memo_sql,$other_editable_sql,$icd10_sql,$byoumei_code_sql);
	$cond = "where ptif_id = '$pt_id' and problem_id = '$problem_id'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if($upd == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

}


//==========================================================================================
//プロブレム名の変更を診療記録に反映 (更新)
//==========================================================================================
//診療記録に反映する必要性について
//プロブレム名を変えて、診療記録を変えないと、診療記録の更新画面でプロブレムをリストから表示できなくなる。

if($mode == "update")
{
	//番号、もしくはプロブレム名が変更の場合(※番号が変更になるケースは現状ない。)
	if($old_db_record["problem"] != $problem || $old_db_record["problem_list_no"] != $problem_list_no)
	{
		//==============================
		//診療記録プロブレムの作成
		//==============================
		$summary_problem = "#".$problem_list_no." ".$problem;
		$summary_problem_sql = pg_escape_string($summary_problem);

		//==============================
		//DB更新
		//==============================
		$sql = "update summary set";
		$set = array("problem");
		$setvalue = array($summary_problem_sql);
		$cond = "where ptif_id = '$pt_id' and problem_id = '$problem_id'";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if($upd == 0)
		{
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}
}

//==========================================================================================
//プロブレムリスト履歴テーブルに追加登録
//==========================================================================================

//==============================
//rireki_indexを採番
//==============================
$tbl = "select max(rireki_index) from summary_problem_list_rireki";
$cond = "where (ptif_id = '$pt_id') and (problem_id = '$problem_id')";
$maxsel = select_from_table($con,$tbl,$cond,$fname);
if($maxsel == 0){
	$rireki_index=1;
}else{
	$rireki_index=pg_result($maxsel,0,"max") + 1;
}
$rireki_index_sql = $rireki_index;

//==============================
//DB登録
//==============================

$date1 = mktime(0,0,0,date('m'), date('d'), date('Y'));
$now_date_year = date('Y', $date1);
$now_date_month = date('m', $date1);
$now_date_day = date('d', $date1);
$update_date = $now_date_year.$now_date_month.$now_date_day;
$update_date_sql = $update_date;

$update_emp_id = get_emp_id($con, $session, $fname);
$update_emp_id_sql = $update_emp_id;

$content = array($ptif_id_sql,$problem_id_sql,$rireki_index_sql,$problem_list_no_sql,$update_date_sql,$problem_sql,$update_emp_id_sql,$problem_class_sql,$problem_kind_sql,$disease_name_flg_sql,$start_date_sql,$end_date_sql,$memo_sql,$other_editable_sql,$icd10_sql,$byoumei_code_sql);
$intbl = "insert into summary_problem_list_rireki (ptif_id,problem_id,rireki_index,problem_list_no,update_date,problem,update_emp_id,problem_class,problem_kind,disease_name_flg,start_date,end_date,memo,other_editable,icd10,byoumei_code) values(";
$in = insert_into_table($con,$intbl,$content,$fname);
if($in == 0)
{
	pg_query($con, "rollback");
	pg_close($con);
//	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
//	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

//==============================
// 医学的領域の問題をすべて抽出
//==============================
$sql =
 " select * from summary_problem_list".
 " where ptif_id = '".pg_escape_string($pt_id)."'".
 " and problem_kind like '医学的領域の問題'".
 " order by problem_list_no";
$selall = select_from_table($con,$sql,"",$fname);


//==============================
// ＤＢコネクション切断(トランザクション終了)
//==============================
pg_query($con, "commit");
pg_close($con);

/**
 * 日付の入力パターンチェック
 * 
 * 「年月日」、「年」のみ、「年月」の入力パターンはOK。他はNG。
 * 
 * @param string $year  年
 * @param string $month 月
 * @param string $day   日
 * 
 * @return boolean ture:OK/false:NG
 */
function isValidDateInputPattern($year, $month, $day)
{
    if ($year == '-') {
        return false;
    }

    if ($month == '-' && $day != '-') {
        return false;
    }

    return true;
}
?>

<? //============================== ?>
<? // 処理完了JavaScript出力        ?>
<? //============================== ?>
<script type="text/javascript">
	var mode = "<?=$mode?>";

	// 全データを返す（tmpl_NyuinjiByoureki.php用）
	if (window.opener && window.opener.problem_list_all_selected_call_back) {
		var datalist = [];
		<? while($row = pg_fetch_array($selall)) { ?>
			datalist.push({
				"problem_id":"<?=str_replace('"','\"',$row["problem_id"])?>",
				"problem_list_no":"'<?=str_replace('"','\"',$row["problem_list_no"])?>",
				"problem":"<?=str_replace('"','\"',$row["problem"])?>",
				"icd10":"<?=str_replace('"','\"',$row["icd10"])?>",
				"byoumei_code":"<?=str_replace('"','\"',$row["byoumei_code"])?>",
				"start_date":"<?=str_replace('"','\"',$row["start_date"])?>"
			});
		<? } ?>
		window.opener.problem_list_all_selected_call_back(datalist, true);
	}
	else if (mode=="insert" || mode=="update") {
		window.opener.location.reload();
	}
	else {
		if (window.opener && window.opener.problem_list_selected_call_back) {
			window.opener.problem_list_selected_call_back('<?=$problem_id?>','<?=$problem_list_no?>','<?=$problem?>','<?=$start_date?>');
		}
	}
	window.close();
</script>